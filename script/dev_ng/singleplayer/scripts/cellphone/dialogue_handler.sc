USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_audio.sch"
USING "commands_player.sch"
USING "commands_task.sch"
USING "dialogue_public.sch"
USING "dialogue_private.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "cellphone_movement.sch"
USING "script_player.sch"
USING "finance_control_public.sch"
USING "comms_control_public.sch"
USING "net_comms_public.sch"
USING "ply_to_ply_calls.sch"
USING "flow_public_game.sch"


                                  
       
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      SCRIPT NAME     :   dialogue_handler.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Controls dialogue automation - acting as a bridge between text
//                          file and native script command. 
//                          Controls mobile phone and normal conversation.
//
//                          Used in conjunction with dialogue_public and dialogue_private                          
//                          along with cellphone header files.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





#if IS_DEBUG_BUILD
    
    //BOOL Test_Conversation_State = FALSE

    INT texttestint = 0
    INT MPtexttestint = 0

    INT DebugPreviousLineInt

    TEXT_LABEL_23 DebugCurrentLabel

    BOOL b_PlayingRandomLine = FALSE

  

#endif


BOOL ConversationBuilt = FALSE, b_ScaleformCallButtonsSetUp = FALSE
BOOL IsConversationPaused = FALSE, b_NeedtoMovePhoneDownForAcceptedJob = FALSE

BOOL bool_BlockLoadFailed = FALSE

CONST_INT const_PreloadMaxTime 5000//0//10000 //Reinstate the actual timeout values once commands actually work.

INT i_PreloadTimeout = 5000//0//10000 // - Reinstate the actual timeout values once commands actually work.

INT i_PreloadRemoteRingTimeout = 5000//0//10000   // - Reinstate the actual timeout values once commands actually work.


INT i_FaceToFacePreloadStartTime, i_FaceToFacePreloadCurrentTime


TEXT_LABEL_23 audioTextLabel, randomAudioTextLabel
TEXT_LABEL_63 audioFilename


TEXT_LABEL_23 SpeakListenLabel
STRING SpeakListenString 


TEXT_LABEL_23 MasterInterruptibleAnimTriggeredLabel
STRING MasterInterruptibleString, MasterAnimTriggeredString, MasterPlaceholderString

BOOL MasterInterruptibleBool, MasterAnimTriggeredBool, MasterPlaceholderBool



TEXT_LABEL_23 LineFlagsLabel
STRING LineFlagsString





//INT FirstSpeakerNumber, SecondSpeakerNumber

INT mpSegmentIndex = 0



//Forced Answer Delay Timers.
INT ForceAnswer_StartTime, ForceAnswer_CurrentTime
TIME_DATATYPE Net_ForceAnswer_StartTime

CONST_INT ForceAnswer_DelayTime 2000


//Incoming call Auto-Hangup Timers.
INT IncomingAutoHangUp_StartTime, IncomingAutoHangUp_CurrentTime
TIME_DATATYPE Net_IncomingAutoHangUp_StartTime

CONST_INT IncomingAutoHangUp_ConstTime 10000


INT Failsafe_StartTime, Failsafe_CurrentTime//PlayState_StartTime
TIME_DATATYPE Net_Failsafe_StartTime


INT Buffer_StartTime, Buffer_CurrentTime , HidePhone_StartTime, HidePhone_CurrentTime
TIME_DATATYPE Net_Buffer_StartTime, Net_HidePhone_StartTime

INT i_TimeDifference, i_ForceAnswerTimeDiff, i_AutoHangupTimeDiff


STRING TempYesRoots[constMaxMultiparts]
STRING TempYesSpecifics[constMaxMultiparts]

STRING TempNoRoots[constMaxMultiparts]
STRING TempNoSpecifics[constMaxMultiparts]


BOOL dh_dpad_scroll_pause_cued = FALSE
SCALEFORM_RETURN_INDEX ServiceChoice_ReturnedSFIndex











//Stock Market manipulation vars
//INT i_dh_Provider_Ident

//BSMF_TYPES bsaq_mf_RejectDecision


PROC MovePhoneToWaistForFirstPersonDecision()

    //If hud movement is hidden, we need to put the phone at waist height, away from the ear to show prop UI.
    IF ShouldFirstPersonPhoneHudMovementBeHidden() //Having this check in all gameplay camera as player can transition to first person when decision screen is up.
    //Reincluded for 2038700

        #if IS_DEBUG_BUILD
            cdPrintstring ("DIALOG_HAND - Phone moved to waist height for First Person response call.")
            cdPrintnl()
        #endif

        IF NOT IS_PED_INJURED (PLAYER_PED_ID())

            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //Only let this happen on foot. See 2059927

                IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927

                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)
                                           
                        Remove_Cellphone_From_Ear()
                        //Reincluded for 2038700  
                                                                                                                
                    ENDIF

                ENDIF

            ENDIF

        ENDIF

    ENDIF

ENDPROC





PROC MovePhoneToEarForFirstPersonDecision()

    //If hud movement is hidden, we need to put the phone at waist height, away from the ear to show prop UI.
    IF ShouldFirstPersonPhoneHudMovementBeHidden() //Having this check in all gameplay camera as player can transition to first person when decision screen is up.
    //Reincluded for 2038700

        #if IS_DEBUG_BUILD
            cdPrintstring ("DIALOG_HAND - Phone moved back to ear height for First Person response call.")
            cdPrintnl()
        #endif

        IF NOT IS_PED_INJURED (PLAYER_PED_ID())

            IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())  //Only let this happen on foot. See 2059927

                IF NOT (g_Use_Prologue_Cellphone) // Removed this for the prologue mission as that phone is only used as a detonator: See bug 1101927

                    IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR)

                        Put_Cellphone_To_Ear()
                        //Reincluded for 2038700 
                        
                    ENDIF  
                                                                                                                         
                ENDIF

            ENDIF

        ENDIF

    ENDIF

ENDPROC











PROC Check_for_911_List_Navigation()

                            
    IF dh_dpad_scroll_pause_cued
    
        IF TIMERA() > 50

            dh_dpad_scroll_pause_cued = FALSE

        ENDIF

    ENDIF




    IF dh_dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_BACKWARD)
        
            /* List cursor not required here. Kept for reference.
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF   
            */

            Call_Scaleform_Input_Keypress_Up()

            dh_dpad_scroll_pause_cued = TRUE

            SETTIMERA (0)


        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))
        OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_SCROLL_FORWARD)

            /*
            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfEntriesInList 

                ListCursorIndex = 0

            ENDIF
            */

            
            Call_Scaleform_Input_Keypress_Down()

            dh_dpad_scroll_pause_cued = TRUE
            
            SETTIMERA (0)

           
            
        ENDIF

    
    ENDIF

            
ENDPROC

















//These may need network functionality built in with new net timer structs if we go ahead with this:
PROC Setup_HidePhoneTrial()

    IF NETWORK_IS_GAME_IN_PROGRESS()
    
        Net_HidePhone_StartTime = GET_NETWORK_TIME()

    ELSE
    
        HidePhone_StartTime  = GET_GAME_TIMER()

    ENDIF


ENDPROC





FUNC BOOL Has_HidePhoneTrialTimerElapsed()


    
    IF NETWORK_IS_GAME_IN_PROGRESS()

        i_TimeDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_HidePhone_StartTime)

    ELSE

        HidePhone_CurrentTime = GET_GAME_TIMER()

        i_TimeDifference = HidePhone_CurrentTime - HidePhone_StartTime

    ENDIF
    

    /* Removing this block for Next Gen as vanishing the phone causes great confusion in first person in SP. - defaulting to returning FALSE.

    IF i_TimeDifference > 5500 //Requested by Leslie, #1263498. Was originally 1500 ms now 5500ms.


        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_HAND - HidePhone timer elapsed - returning TRUE in 3rd person mode.")
            cdPrintnl()

        #endif
            

        RETURN TRUE
    
    ELSE

        RETURN FALSE

    ENDIF

    */
    

    RETURN FALSE



ENDFUNC










PROC Perform_dh_StockMarketUpdate()

/*
    i_dh_Provider_Ident = Get_Cellphone_Provider()

    
    SWITCH i_dh_Provider_Ident

        CASE PROVIDER_BADGER

            bsaq_mf_RejectDecision = BSMF_CALLREFUSED_FOR_BDG
      
        BREAK

        CASE PROVIDER_WHIZ

            bsaq_mf_RejectDecision = BSMF_CALLREFUSED_FOR_WIZ
       
        BREAK

        CASE PROVIDER_TINKLE

            bsaq_mf_RejectDecision = BSMF_CALLREFUSED_FOR_TNK

        BREAK

        DEFAULT //Default to badger.

            bsaq_mf_RejectDecision = BSMF_CALLREFUSED_FOR_BDG
         
        BREAK


    ENDSWITCH


    */
    ////BAWSAQ_INCREMENT_MODIFIER (bsaq_mf_RejectDecision, 1)

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_FH - Performing call refused stock update.")
        cdPrintnl()
     
    #endif
    */


ENDPROC





























//This decides which state the cellphone should return to after a call has been made
//Update_Scaleform_VSE from cellphone_private will then make that change

PROC BeforeCallStateHandler()




    IF g_Cellphone.PhoneDS = PDS_DISABLED
    OR g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY
    
        #if IS_DEBUG_BUILD
            cdPrintstring ("DIALOG_HAND - BeforeCallStateHandler says phone has been set to disabled - not assigning new value to BeforeCallPhoneDS. Exiting handler proc")     
            cdPrintnl ()
        #endif

        EXIT

    ENDIF




    SWITCH BeforeCallPhoneDS

     
        CASE PDS_MINIMAL
            BeforeCallPhoneDS = PDS_MAXIMUM
        BREAK


        
        //CRITICAL! If a phonecall comes in and the cellphone was already on the homescreen, this controls what happens when the phonecall terminates...
        CASE PDS_MAXIMUM
            
         
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

                BeforeCallPhoneDS = PDS_MAXIMUM

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

      
            ELSE

  
                BeforeCallPhoneDS = PDS_AWAY  //This is slightly weird, but done by request.

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - BeforeCallStateHandler says bcstate was maximum, no return to hscreen bit set in PAUSE_ANSWERING_TIME, so setting to AWAY. #1119893")
                    cdPrintnl()
                #endif
    

            ENDIF

            //Additional information:
            //TODO 686980, the Trackify sequence, requested that the phone returns to the homescreen after the call has delivered the coords. The bit can be set via
            //public function SET_FINISHED_CALL_RETURNS_TO_HOMESCREEN()
            //Check notes for bug 246816 that were made in cellphone flashhand on 13.10.11
            //This will also put the phone away if it is already up and an incoming call is rejected. Can put a check in if necessary.

        BREAK
   

        //This should never run now unless game runs slow...
        CASE PDS_RUNNINGAPP


            //Put in this failsafe to make sure any apps left hanging get cleaned up via abnormal exit check. Bug 100374 fired this and it looked 
            //like BeforeCallPhoneDS being set to PDS_MAXIMUM, meant appMPJoblist was in limbo. Set BeforeCallPhoneDS to PDS_AWAY which is safer. 

            //Was
            //BeforeCallPhoneDS = PDS_MAXIMUM
            BeforeCallPhoneDS = PDS_AWAY     

            
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - BeforeCallStateHandler says bcstate was PDS_RUNNINGAPP just prior to call! Now switching it to PDS_AWAY. Hanging app should self terminate via abnormal exit.")
                cdPrintnl()
            #endif


            //SCRIPT_ASSERT ("Please add this bug for Steve Taylor.")

            //This got fired by bug 82144.


            //Update 03.09.10
            //Had a chat with Keith and we agree that incoming calls will not be allowed if the phone is in a running app state:
            //Two main reasons. 1. If the player is pissing around on the phone internet, stock market or camera you don't want hassled with a call.
            //2. It's a bit dangerous and the app writers would need to interrogate a global to ensure their scaleform slots are refilled.

            //BeforeCallPhoneDS = PDS_RUNNINGAPP //Trial run... 02.09.10
            //Will need to have some way of storing the scaleform display_view state so we can return to that.

        BREAK
        
    

        //CRITICAL! If a phonecall comes in and the cellphone was NOT in use at the time, this controls what happens when the phonecall terminates.
        CASE PDS_AWAY
       
            IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

                BeforeCallPhoneDS = PDS_MAXIMUM

                CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)


         
            ELSE

                BeforeCallPhoneDS = PDS_AWAY

            ENDIF

            //Additional information:
            //TODO 686980, the Trackify sequence, requested that the phone returns to the homescreen after the call has delivered the coords. The bit can be set via
            //public function SET_FINISHED_CALL_RETURNS_TO_HOMESCREEN ()
            //Check notes for bug 246816 that were made in cellphone flashhand on 13.10.11
            //This will also put the phone away if it is already up and an incoming call is rejected. Can put a check in if necessary.


        BREAK



        DEFAULT //Added for bug 147217
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - BeforeCallStateHandler says bcstate was peculiar just prior to call! Setting bcstate to PDS_AWAY as a precaution.")
                cdPrintnl()
            #endif



            BeforeCallPhoneDS = PDS_AWAY
    
        BREAK


       
    ENDSWITCH

    
ENDPROC







PROC GetSpeakerListenerLabel()
    
    SpeakListenLabel = g_ConversationData.ConversationSegmentToGrab
    SpeakListenLabel += "SL"



    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Trying to find SL label of passed conversation ")
        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
        cdPrintnl()

    #endif



    IF DOES_TEXT_LABEL_EXIST (SpeakListenLabel)

        
        //SpeakListenString = GET_STRING_FROM_TEXT_FILE (SpeakListenLabel)
        SpeakListenString = GET_FILENAME_FOR_AUDIO_CONVERSATION (SpeakListenLabel)
        
        IF IS_STRING_NULL (SpeakListenString) 
        
            #IF IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Speaker Listener string has null value. See Steve T")

            #endif

        ELSE


            #IF IS_DEBUG_BUILD
                 cdPrintstring("DIALOG_HAND - Found SL Label for dialogue. Details are ")
                 cdPrintstring(SpeakListenString)
                 cdPrintnl()
            #endif

        
        ENDIF

    ELSE
        
        #IF IS_DEBUG_BUILD

            
            cdPrintstring("DIALOG_HAND - WARNING! Could not find SL Label for dialogue. Conversation or phonecall will be blank and terminate quickly.")
            cdPrintnl()



            CASSERTLN(DEBUG_CELL_DIALOG, "DIALOG_HAND - Speaker Listener label for conversation root \"", SpeakListenLabel, "\" does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC






PROC GetMasterInterruptibleAnimTriggeredLabel()
    
    MasterInterruptibleAnimTriggeredLabel = g_ConversationData.ConversationSegmentToGrab
    MasterInterruptibleAnimTriggeredLabel += "IF"



    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Trying to find Master Interruptible Flag label of passed conversation ")
        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
        cdPrintnl()

    #endif



    IF DOES_TEXT_LABEL_EXIST (MasterInterruptibleAnimTriggeredLabel)

        
        //MasterInterruptibleString = GET_STRING_FROM_TEXT_FILE (MasterInterruptibleAnimTriggeredLabel)
        //MasterInterruptibleString = GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel)    //Was used when only Interruptible flag was present in label's string contents.
        MasterInterruptibleString  = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 0, 1)

        IF IS_STRING_NULL (MasterInterruptibleString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("MasterInterruptible string has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                 cdPrintstring("DIALOG_HAND - Grabbed Master Interruptible character from string for dialogue. Details are ")
                 cdPrintstring(MasterInterruptibleString)
                 cdPrintnl()
            #endif
     
        ENDIF




        MasterAnimTriggeredString = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 1, 2)


        IF IS_STRING_NULL (MasterAnimTriggeredString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("MasterAnimTriggeredString has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                 cdPrintstring("DIALOG_HAND - Grabbed Master AnimTriggered character from string for dialogue. Details are ")
                 cdPrintstring(MasterAnimTriggeredString)
                 cdPrintnl()
            #endif
        
        ENDIF
        



        MasterPlaceholderString = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 2, 3)


        IF IS_STRING_NULL (MasterPlaceholderString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("MasterPlaceholderString has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                 cdPrintstring("DIALOG_HAND - Grabbed Master Placeholder character from string for dialogue. Details are ")
                 cdPrintstring(MasterPlaceholderString)
                 cdPrintnl()
            #endif
        
        ENDIF






    ELSE
        
        #if IS_DEBUG_BUILD

            SCRIPT_ASSERT ("DIALOG_HAND - Master InterruptibleAnimTriggered label for this conversation root does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC





    


PROC GetLineFlagsLabel()
    
    LineFlagsLabel = g_ConversationData.ConversationSegmentToGrab
    LineFlagsLabel += "LF"



    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Trying to find Line Flags label of passed conversation ")
        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
        cdPrintnl()

    #endif



    IF DOES_TEXT_LABEL_EXIST (LineFlagsLabel)

        
        //LineFlagsString = GET_STRING_FROM_TEXT_FILE (LineFlagsLabel)
        LineFlagsString = GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel)
        
        IF IS_STRING_NULL (LineFlagsString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Line Flags string has null value. See Steve T")

            #endif

        ELSE


            #if IS_DEBUG_BUILD
                 cdPrintstring("DIALOG_HAND - Found Line Flags Label for dialogue. Details are ")
                 cdPrintstring(LineFlagsString)
                 cdPrintnl()
            #endif

        
        ENDIF

    ELSE
        
        #if IS_DEBUG_BUILD

            SCRIPT_ASSERT ("DIALOG_HAND - Line Flags label for this conversation root does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC








PROC GetSpeakerListenerLabelForThisMultipart()
    
    SpeakListenLabel = g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex]
    SpeakListenLabel += "SL"

    IF DOES_TEXT_LABEL_EXIST (SpeakListenLabel)

        
        //SpeakListenString = GET_STRING_FROM_TEXT_FILE (SpeakListenLabel)
        SpeakListenString = GET_FILENAME_FOR_AUDIO_CONVERSATION (SpeakListenLabel)


        
        IF IS_STRING_NULL (SpeakListenString) 
        
            #IF IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Speaker Listener string has null value. See Steve T")

            #endif

        ELSE

            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("DIALOG_HAND - SL Label for multipart dialogue is ")
                cdPrintstring(SpeakListenString)
                cdPrintstring(" for segment ")
                cdPrintint(mpSegmentIndex)
                cdPrintnl()
            #endif

        
        ENDIF

    ELSE
        
        #IF IS_DEBUG_BUILD

            SCRIPT_ASSERT ("DIALOG_HAND - Speaker Listener label for this multipart conversation root does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC








PROC GetMasterInterruptibleAnimTriggeredLabelForThisMultipart()
    
    MasterInterruptibleAnimTriggeredLabel = g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex]
    MasterInterruptibleAnimTriggeredLabel += "IF"

    IF DOES_TEXT_LABEL_EXIST (MasterInterruptibleAnimTriggeredLabel)

        
        //MasterInterruptibleString = GET_STRING_FROM_TEXT_FILE (MasterInterruptibleAnimTriggeredLabel)
        //MasterInterruptibleString = GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel)   //Was used when only Interruptible flag was present in label's string contents.

        MasterInterruptibleString  = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 0, 1)


        
        IF IS_STRING_NULL (MasterInterruptibleString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Master Interruptible string has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                cdPrintstring("DIALOG_HAND - Grabbed Master Interruptible character from string for multipart dialogue. Details are ")
                cdPrintstring(MasterInterruptibleString)
                cdPrintnl()
                cdPrintstring(" for segment ")
                cdPrintint(mpSegmentIndex)
                cdPrintnl()
            #endif

        
        ENDIF




        MasterAnimTriggeredString  = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 1, 2)


        IF IS_STRING_NULL (MasterAnimTriggeredString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Master AnimTriggered string has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                cdPrintstring("DIALOG_HAND - Grabbed Master AnimTriggered character from string for multipart dialogue. Details are ")
                cdPrintstring(MasterAnimTriggeredString)
                cdPrintnl()
                cdPrintstring(" for segment ")
                cdPrintint(mpSegmentIndex)
                cdPrintnl()
            #endif

        
        ENDIF
        



        MasterPlaceholderString  = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 2, 3)


        IF IS_STRING_NULL (MasterPlaceholderString) 
        
            #if IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Master Placeholder string has null value. See Steve T")

            #endif

        ELSE

            #if IS_DEBUG_BUILD
                cdPrintstring("DIALOG_HAND - Grabbed Master Placeholder character from string for multipart dialogue. Details are ")
                cdPrintstring(MasterPlaceholderString)
                cdPrintnl()
                cdPrintstring(" for segment ")
                cdPrintint(mpSegmentIndex)
                cdPrintnl()
            #endif

        
        ENDIF





    ELSE
        
        #if IS_DEBUG_BUILD

            SCRIPT_ASSERT ("DIALOG_HAND - Master InterruptibleAnimTriggered label for this multipart conversation root does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC








           

PROC GetLineFlagsLabelForThisMultipart()
    
    LineFlagsLabel = g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex]
    LineFlagsLabel += "LF"

    IF DOES_TEXT_LABEL_EXIST (LineFlagsLabel)

        
        //LineFlagsString = GET_STRING_FROM_TEXT_FILE (LineFlagsLabel)
        LineFlagsString = GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel)


        
        IF IS_STRING_NULL (LineFlagsString) 
        
            #IF IS_DEBUG_BUILD

                 SCRIPT_ASSERT ("Line Flags string has null value. See Steve T")

            #endif

        ELSE

            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("DIALOG_HAND - Line Flags Label for multipart dialogue is ")
                cdPrintstring(LineFlagsString)
                cdPrintstring(" for segment ")
                cdPrintint(mpSegmentIndex)
                cdPrintnl()
            #endif

        
        ENDIF

    ELSE
        
        #IF IS_DEBUG_BUILD

            SCRIPT_ASSERT ("DIALOG_HAND - Line Flags label for this multipart conversation root does not exist. Check americandialogue.txt to see if the root is present and you've made the gxt.")

        #endif

    ENDIF

ENDPROC
























PROC AddPedsDataForConversation()


    //Need to get rid of all ped struct data here... 


    INT arrayindex

    FOR arrayindex = 0 TO (constMaxNum_Conversers - 1)  //Subtract one to prevent array overrun


        //Would need to think about having 0, 1 and 2 as defaults for player speaker...

        #if IS_DEBUG_BUILD
            
            cdPRINTNL()
            cdPRINTSTRING ("Checking Standard Ped Struct Index ")
            cdPRINTINT (arrayIndex)
            cdPRINTNL()

        #endif


        IF g_ConversationPedsStruct.PedInfo[arrayindex].ActiveInConversation = TRUE


           
                IF DOES_ENTITY_EXIST (g_ConversationPedsStruct.PedInfo[arrayindex].Index)
                OR g_ConversationPedsStruct.PedInfo[arrayindex].Index = NULL

                    ADD_PED_TO_CONVERSATION (arrayindex, g_ConversationPedsStruct.PedInfo[arrayindex].Index, g_ConversationPedsStruct.PedInfo[arrayindex].VoiceId)
                
                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - ADD_PED_FOR_CONVERSATION called for index: ")
                        cdPrintint (arrayindex)
                                

                        cdPrintstring(" with voice ")
                        cdPrintstring(g_ConversationPedsStruct.PedInfo[arrayindex].VoiceID)
                        cdPrintstring(" and ped index ")
                        cdPrintint (NATIVE_TO_INT (g_ConversationPedsStruct.PedInfo[arrayindex].Index))
                    
                        cdPrintnl()

                        IF IS_ENTITY_DEAD (g_ConversationPedsStruct.PedInfo[arrayindex].Index)
                            
                            cdPrintstring ("DIALOG_HAND - WARNING! Ped above is NULL or recently deceased!")
                            cdPrintnl()
             
                        ENDIF

                    #endif


                ELSE
                    
                    #if IS_DEBUG_BUILD

                        cdPrintnl()                    
                        cdPrintstring ("DIALOG_HAND - DANGER! Ped included in local peds struct at array position ")
                        cdPrintint (arrayindex)
                        cdPrintstring (" is no longer valid! NOT adding to native conversation data.")
                        cdPrintnl()
                        cdPrintstring ("Active involvement by this ped will cause missing voicename assert! ")
                        cdPrintnl()
             
                
                    #endif

                ENDIF
                              

                IF g_ConversationPedsStruct.NullPed_Number_for_VoicePlacement = arrayindex
                    
                    IF g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.x <> 0.0
                    AND g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.y <> 0.0
                    AND g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.z <> 0.0

                        WAIT (0)

                        SET_POSITION_FOR_NULL_CONV_PED (g_ConversationPedsStruct.NullPed_Number_for_VoicePlacement, g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement)
                        
                        #if IS_DEBUG_BUILD     
                            cdPrintnl()                    
                            cdPrintstring ("DIALOG_HAND - Setting remote voice location for NULL ped number ")
                            cdPrintint (arrayindex)
                            cdPrintnl()
                            printfloat(g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.x)
                            cdPrintnl()
                            printfloat(g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.y)
                            cdPrintnl()
                            printfloat(g_ConversationPedsStruct.NullPed_Vector_for_VoicePlacement.z)
                            cdPrintnl()
                            
                        #endif

                    ENDIF

                ENDIF


                /*
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - ADD_PED_FOR_CONVERSATION called for index: ")
                    cdPrintint (arrayindex)
                            

                    cdPrintstring(" with voice ")
                    cdPrintstring(g_ConversationPedsStruct.PedInfo[arrayindex].VoiceID)
                    cdPrintstring(" and ped index ")
                    cdPrintint (NATIVE_TO_INT (g_ConversationPedsStruct.PedInfo[arrayindex].Index))
                
                    cdPrintnl()

                #endif



                #if IS_DEBUG_BUILD

                    //DOES_ENTITY_EXIST - see email from Matthew / Graeme. 
                
                    IF IS_ENTITY_DEAD (g_ConversationPedsStruct.PedInfo[arrayindex].Index)
                            
                        cdPrintstring ("DIALOG_HAND - WARNING! Ped above is NULL or recently deceased!")
                        cdPrintnl()
             
                    ENDIF

                #endif   
                */


         

        ENDIF


    ENDFOR



    
    #if USE_TU_CHANGES
    IF g_B_ContentCreationPeds_Should_be_added_for_Dialogue = TRUE

        
        g_B_ContentCreationPeds_Should_be_added_for_Dialogue = FALSE
        
        
        arrayIndex = 0

        FOR arrayindex = 0 TO (constMaxExtraContentCreationNum_Conversers - 1)  //Subtract one to prevent array overrun



        #if IS_DEBUG_BUILD
            
            cdPRINTNL()
            cdPRINTSTRING ("Now checking Extra ContentCreation Ped Struct Index ")
            cdPRINTINT (arrayIndex)
            cdPRINTNL()

        #endif


            IF g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].ActiveInConversation = TRUE


           
                IF DOES_ENTITY_EXIST (g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].Index)
                OR g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].Index = NULL


                    ADD_PED_TO_CONVERSATION (g_ContentCreationConversationPedsStruct.PedInfo[arrayIndex].AddPedNativeNumber, g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].Index, g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].VoiceId)
    

                    /*Data example for reference
                    g_ContentCreationConversationPedsStruct.PedInfo[3].AddPedNativeNumber = 20
                    g_ContentCreationConversationPedsStruct.PedInfo[3].VoiceID = "STEVE"
                    g_ContentCreationConversationPedsStruct.PedInfo[3].Index = NULL
                    g_ContentCreationConversationPedsStruct.PedInfo[3].ActiveInConversation = TRUE
                    */

                    #if IS_DEBUG_BUILD
            
                        cdprintstring ("Adding above ped to conversation via native as speaker number ")
                        cdprintint (g_ContentCreationConversationPedsStruct.PedInfo[arrayIndex].AddPedNativeNumber)
                        cdPrintstring (" with voice ")
                        cdPrintstring (g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].VoiceID)
                        cdPrintstring (" and ped index ")
                        cdPrintint (NATIVE_TO_INT (g_ContentCreationConversationPedsStruct.PedInfo[arrayindex].Index))

                        cdprintnl()
                        cdprintnl()

                    #endif


                ENDIF

            ENDIF

        ENDFOR


    ENDIF
    #endif


ENDPROC





















FUNC BOOL GetMasterInterruptibleIntFromString_and_Convert_to_Bool() 

    STRING singleCharacter

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 0, 1)


    
    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Master Interruptible character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()
    
    #endif
    */
    



    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        
        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master Interruptible returns FALSE")
            cdPrintnl()
        #endif
        */
        

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master Interruptible returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF


ENDFUNC






FUNC BOOL GetMasterAnimTriggeredIntFromString_and_Convert_to_Bool() 


    STRING singleCharacter

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 1, 2)


    
    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Master AnimTriggered character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()
    
    #endif
    */



    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master AnimTriggered returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master AnimTriggered returns TRUE")
            cdPrintnl()
        #endif
        */
        

        RETURN TRUE

    ENDIF


ENDFUNC




















FUNC BOOL GetMasterPlaceholderIntFromString_and_Convert_to_Bool() 


    STRING singleCharacter

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (MasterInterruptibleAnimTriggeredLabel), 2, 3)


    
    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Master Placeholder character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()
    
    #endif
    */



    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master Placeholder returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Master Placeholder returns TRUE")
            cdPrintnl()
        #endif
        */
        

        RETURN TRUE

    ENDIF


ENDFUNC























FUNC INT GetSpeakerIntFromString (INT iSLmanipulator)


    STRING singleCharacter


    //singleCharacter = GET_STRING_FROM_STRING (GET_STRING_FROM_TEXT_FILE (SpeakListenLabel), (iSLmanipulator * 3), ((iSLmanipulator * 3)+ 1))


    

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (SpeakListenLabel), (iSLmanipulator * 3), ((iSLmanipulator * 3)+ 1))


    
    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Speaker character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()
    
    #endif
    */



    RETURN ConvertSingleCharacter(singleCharacter)
    


ENDFUNC









FUNC INT GetListenerIntFromString (INT iSLmanipulator)


    STRING singleCharacter


    //singleCharacter = GET_STRING_FROM_STRING (GET_STRING_FROM_TEXT_FILE (SpeakListenLabel), ((iSLmanipulator * 3) + 1), ((iSLmanipulator * 3)+ 2))
    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (SpeakListenLabel), ((iSLmanipulator * 3) + 1), ((iSLmanipulator * 3)+ 2))

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Listener character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()

    #endif
    */




    RETURN ConvertSingleCharacter(singleCharacter)
        


ENDFUNC






FUNC INT GetVolumeTypeIntFromString (INT iSLmanipulator)


    STRING singleCharacter


    //singleCharacter = GET_STRING_FROM_STRING (GET_STRING_FROM_TEXT_FILE (SpeakListenLabel), ((iSLmanipulator * 3) + 2), ((iSLmanipulator * 3)+ 3))
    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (SpeakListenLabel), ((iSLmanipulator * 3) + 2), ((iSLmanipulator * 3)+ 3))

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted VolumeType character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()

    #endif
    */


    RETURN ConvertSingleCharacter(singleCharacter)
    


ENDFUNC








FUNC BOOL GetLineInterruptibleIntFromString_and_Convert_to_Bool(INT iSLmanipulator)

    
    STRING singleCharacter

    
    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), (iSLmanipulator * 7), ((iSLmanipulator * 7)+ 1))

    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Line Interruptible returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Line Interruptible returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF


ENDFUNC

        
            





FUNC BOOL GetDucksRadioInt_and_Convert_to_Bool(INT iSLmanipulator)

    
    STRING singleCharacter

   
    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 1), ((iSLmanipulator * 7)+ 2))
    

    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Ducks Radio returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Ducks Radio returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF

ENDFUNC








FUNC BOOL GetDucksScoreInt_and_Convert_to_Bool(INT iSLmanipulator)


    STRING singleCharacter
    

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 2), ((iSLmanipulator * 7)+ 3))
    
    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Ducks Score returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Ducks Score returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF


ENDFUNC







FUNC eAUDIBILITY GetAudibilityEnumFromString (INT iSLmanipulator)


    STRING singleCharacter


    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 3), ((iSLmanipulator * 7)+ 4))

    /*
    #if IS_DEBUG_BUILD

        cdPrintstring("DIALOG_HAND - Comparing extracted Audibility character: ")
        cdPrintstring(singleCharacter)
        cdPrintnl()

    #endif
    */
    

    /* Code Enum for reference.
    ENUM eAUDIBILITY
    AUD_AUDIBILITY_NORMAL = 0,
    AUD_AUDIBILITY_CLEAR,
    AUD_AUDIBILITY_CRITICAL,
    AUD_AUDIBILITY_LEAD_IN
    ENDENUM


    Nick Warseck in bug 548445
    D* Ambient -> AUD_AUDIBILITY_NORMAL   4
    D* Normal/Clear -> AUD_AUDIBILITY_CLEAR   0, 1
    D* Critical -> AUD_AUDIBILITY_CRITICAL 2
    D* Lead In -> AUD_AUDIBILITY_LEAD_IN 3


    Dialogue star exports the audibility flags as follows:

    Normal -> 0 (Default)
    Clear -> 1
    Critical -> 2
    Lead In -> 3 
    Ambient -> 4


    */



    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        RETURN AUD_AUDIBILITY_CLEAR

    ENDIF

        
    IF ARE_STRINGS_EQUAL (singleCharacter, "1")

        RETURN AUD_AUDIBILITY_CLEAR

    ENDIF


    IF ARE_STRINGS_EQUAL (singleCharacter, "2")

        RETURN AUD_AUDIBILITY_CRITICAL

    ENDIF

        
    IF ARE_STRINGS_EQUAL (singleCharacter, "3")

        RETURN AUD_AUDIBILITY_LEAD_IN

    ENDIF



    IF ARE_STRINGS_EQUAL (singleCharacter, "4")

        RETURN AUD_AUDIBILITY_NORMAL //This maps to ambient at the code side now.

    ENDIF




    #if IS_DEBUG_BUILD

        PRINTSTRING ("DIALOG_HAND - GetAudibilityEnumFromString returned weird character!")
        PRINTNL()

    #endif


    RETURN AUD_AUDIBILITY_NORMAL


ENDFUNC






FUNC BOOL GetHeadSetInt_and_Convert_to_Bool(INT iSLmanipulator)


    STRING singleCharacter
    

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 4), ((iSLmanipulator * 7)+ 5))
    
    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Headset flag returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("Headset Flag returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF


ENDFUNC









FUNC BOOL GetDoNotInterruptForSpecAbInt_and_Convert_to_Bool(INT iSLmanipulator)


    STRING singleCharacter
    

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 5), ((iSLmanipulator * 7)+ 6))
    
    IF ARE_STRINGS_EQUAL (singleCharacter, "0")

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("DoNotInterruptForSpecAb flag returns FALSE")
            cdPrintnl()
        #endif
        */

        RETURN FALSE

    ELSE

        /*
        #if IS_DEBUG_BUILD
            cdPrintstring("HDoNotInterruptForSpecAb Flag returns TRUE")
            cdPrintnl()
        #endif
        */

        RETURN TRUE

    ENDIF


ENDFUNC









FUNC BOOL GetIsPadSpeakerRouteInt_and_Convert_to_Bool(INT iSLmanipulator)


    STRING singleCharacter
    

    singleCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (LineFlagsLabel), ((iSLmanipulator * 7) + 6), ((iSLmanipulator * 7) + 7))
    
    IF ARE_STRINGS_EQUAL (singleCharacter, "0")


        #if IS_DEBUG_BUILD
            cdPrintstring("IsPadSpeakerRoute flag returns FALSE")
            cdPrintnl()
        #endif


        RETURN FALSE

    ELSE


        #if IS_DEBUG_BUILD
            cdPrintstring("IsPadSpeakerRoute flag returns TRUE")
            cdPrintnl()
        #endif


        RETURN TRUE

    ENDIF


ENDFUNC




FUNC BOOL IsProblemTextBlock(INT iHash)		
	SWITCH iHash
		CASE HASH("APCUTAU")
		CASE HASH("APAIR")
		CASE HASH("APCALAU")
		CASE HASH("PBPAU")   // Fix for 2663027
		CASE HASH("PBBAU")   // Fix for url:bugstar:5512044 - Ignorable assert - Error: false: GXT2 File missing: language:/PBBAU.gxt2  ...from Updating script dialogue_handler
		CASE HASH("EXPOWAU") // Fix for 2743250
		CASE HASH("EXCALAU") // Fix for 2798553
		CASE HASH("EXCPAAU") // Fix for 2802174 and 2802201
		CASE HASH("EXHELAU") // Fix for 2845076
		CASE HASH("BPLESAU") // Fix for 3011433
		CASE HASH("BPMALAU") // Fix for 3011433
		CASE HASH("LOWREAU") // Fix for 3081309
		CASE HASH("IMTRAAU") // Fix for 3127894
		CASE HASH("IMAMPAU") // Fix for 3162461
		CASE HASH("IMWEDAU") // Found this lurking in netDLC for imp exp - Steve T. Pre-emptive strike.
		CASE HASH("IMBUSAU") // Found this lurking in netDLC for imp exp - Steve T. Pre-emptive strike.
		CASE HASH("IMROCAU") // Found this lurking in netDLC for imp exp - Steve T. Pre-emptive strike.
		CASE HASH("IMFLIAU") // Fix for 3145260
		CASE HASH("IMPAAUD") // url:bugstar:3198023 - Assert - X:\gta5\src\dev_ng\game\VS_Project4_lib\_Unity/../../text/TextFile.cpp(761): Error: false: GXT2 File missing: language:/IMPAAUD.gxt2  ...from Updating script dialogue_handler
		CASE HASH("IMDUNAU") // url:bugstar:3206729 - svm_dune4: Error: false: GXT2 File missing: language:/IMDUNAU.gxt2
		CASE HASH("IMRUIAU") // url:bugstar:3228057 - SVM_ruiner2: ignorable assert on approaching the Cargobobs at LSIA: TextFile.cpp(761): Error: false: GXT2 File missing: language:/IMRUIAU.gxt2  ...from Updating script dialogue_handler
		CASE HASH("IMMECAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMARMAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMBLZAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMPHAAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMTECAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMVOLAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("IMWASAU") // url:bugstar:3230819 ##### "Here's the list of Import Export text blocks that will have likely have caused and might still be the cause of problems. All need added to the exception list that CL refers to"
		CASE HASH("GNRCAUD") // url:bugstar:3451025 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text block - GNRCAUD  - to the dialogue handler's script.
		CASE HASH("GNRWEAU") // url:bugstar:3519014 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text block - GNRWEAU  - to the dialogue handler's script.
		CASE HASH("GRAPCAU") // url:bugstar:3493669 - WVM - APC - Ignorable assert -  Error: false: GXT2 File missing: language:/GRAPCAU.gxt2  ...from Updating script dialogue_handler
		CASE HASH("GRHALAU") // url:bugstar:3485852 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the GRHALAU dialogue text blocks to the dialogue handler's script.
		CASE HASH("GRDUNAU") // url:bugstar:3485852 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the GRDUNAU dialogue text blocks to the dialogue handler's script.
		CASE HASH("GRMOCAU") // url:bugstar:3579632 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text blocks to the dialogue handler's script.
		CASE HASH("GROPPAU") // url:bugstar:3579632 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text blocks to the dialogue handler's script.
		CASE HASH("GROP2AU") // url:bugstar:3579632 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text blocks to the dialogue handler's script.
		CASE HASH("GRTAMAU") // url:bugstar:3579632 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text blocks to the dialogue handler's script.
		CASE HASH("GRTRSAU") // url:bugstar:3579632 - New gunrununning placeholder dialogue has been added to the DLC pack. Please add the the dialogue text blocks to the dialogue handler's script.
		CASE HASH("SMGCAUD") // url:bugstar:3770997 - Smuggler - Please hookup placeholder Steal Mission Opening Calls.
		CASE HASH("SMGFZAU") // url:bugstar:3858891 - Please add the dialogue text block: SMGFZAU.
		CASE HASH("IAAFIAU") // url:bugstar:4007713 - New dialogue block for The IAA Job
		CASE HASH("IAAMOAU") // url:bugstar:3973502 - New dialogue block for IAA - Morgue
		CASE HASH("IAASFAU") // url:bugstar:4007713 - New dialogue block for IAA - Server Farm
		CASE HASH("IAADEAU") // url:bugstar:4007713 - New dialogue block for IAA - De-lorean
		CASE HASH("SILPRAU") // url:bugstar:4007713 - New dialogue block for Silo - Predator
		CASE HASH("SUBBMAU") // url:bugstar:4007713 - New dialogue block for Submarine - Ballistic Missile
		CASE HASH("SUBFIAU") // url:bugstar:4007713 - New dialogue block for The Submarine Job
		CASE HASH("SUBFOAU") // url:bugstar:4007713 - New dialogue block for Submarine - Foundry
		CASE HASH("SUBOSAU") // url:bugstar:4007713 - New dialogue block for Submarine - Osprey
		CASE HASH("SUBCAAU") // url:bugstar:4007713 - New dialogue block for Submarine - Sub Car
		CASE HASH("XMFMAUD") // url:bugstar:4015563 - Please hookup placeholder prep mission opening calls
		CASE HASH("TUFIN")   // url:bugstar:4019749 - The Fleeca Job - Ignorable assert fires during hacking minigame: Error: false: GXT2 File missing: language:/TUFIN.gxt2  ...from Updating script dialogue_handler
		CASE HASH("SUBRIAU") // url:bugstar:4040362 - New dialogue block for Submarine - Riot Van
		CASE HASH("SILBCAU") // url:bugstar:4067663 - New dialogue block for Silo - BC Custom
		CASE HASH("SILSPAU") // url:bugstar:4070434 - New dialogue block for Silo - Spy Plane
		CASE HASH("SILTAAU") // url:bugstar:4072085 - Silo - Stealth Tank: Can we please add the following Dialogue ID for this mission
		CASE HASH("SILFIAU") // url:bugstar:4104357 - Ignorable Assert - Error: false: GXT2 File missing: language:/SILFIAU.gxt2  ...from Updating script dialogue_handler
		CASE HASH("H2CUTAU") // url:bugstar:4104357 - url:bugstar:4175933 - Need new text block added as an exception to dialogue handler - H2CUTAU
		CASE HASH("BATFMAU") // url:bugstar:4550948 - New Battle dialogue has been added for the DLC pack. Please add the dialogue text block: BATFMAU - to the dialogue handler's script.
		CASE HASH("LAZFMAU") // url:bugstar:4819364 - New Lazlow dialogue has been added for the mpBattle DLC pack. Please add the dialogue text block: LAZFMAU - to the dialogue handler's script.
		CASE HASH("PAIFMAU")
		CASE HASH("ARINTAU") // url:bugstar:5439378 - New Arena dialogue has been added for the mpChristmas2018 DLC pack. Please add the dialogue text block: ARINTAU - to the dialogue handler's script.
		CASE HASH("ARANNAU") // url:bugstar:5439386 - The announcer dialogue has been added for the mpChristmas2018 DLC pack. Please add the dialogue text block: ARANNAU - to the dialogue handler's script.
		CASE HASH("ARASAU")  // url:bugstar:5486686 - An additional announcer dialogue sheet has been added for the mpChristmas2018 DLC pack. Please add the dialogue text block: ARASAU - to the dialogue handler's script.
		CASE HASH("CATRKAU")  // url:bugstar:5698088 - Added new placeholder dialogue to the Casino DLC pack. Please add the text block: CATRKAU, to the dialogue handler's script.
		CASE HASH("CAGTAU")  // url:bugstar:5721157 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAGTAU, to the dialogue handler's script.
		CASE HASH("CTOMAU")  // url:bugstar:5721159 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CTOMAU, to the dialogue handler's script.
		CASE HASH("CASPAUD")	// url:bugstar:5742710 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CASPAUD, to the dialogue handler's script.		
		CASE HASH("CAS1AUD")	// url:bugstar:5748012 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS1AUD, to the dialogue handler's script.
		CASE HASH("CAS2AUD")	// url:bugstar:5748013 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS2AUD, to the dialogue handler's script.
		CASE HASH("CAS3AUD")	// url:bugstar:5748015 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS3AUD, to the dialogue handler's script.
		CASE HASH("CAS4AUD")	// url:bugstar:5748016 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS4AUD, to the dialogue handler's script.
		CASE HASH("CAS5AUD")	// url:bugstar:5748017 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS5AUD, to the dialogue handler's script.
		CASE HASH("CAS6AUD")	// url:bugstar:5748018 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CAS6AUD, to the dialogue handler's script.
		CASE HASH("CASUIAU")	// url:bugstar:5799646 - Added new placeholder dialogue to the mpVinewood DLC pack. Please add the text block: CASUIAU, to the dialogue handler's script.
		CASE HASH("CASINAU")	// url:bugstar:5808064 - Added new dialogue to the mpVinewood DLC pack. Please add the text block: CASINAU, to the dialogue handler's script.
		CASE HASH("CAPAAUD")	// url:bugstar:5813778 - Added new dialogue to the mpVinewood DLC pack. Please add the text block: CAPAAUD, to the dialogue handler's script.
		CASE HASH("CASBKAU")	// url:bugstar:5838151 - Added new dialogue to the mpVinewood DLC pack. Please add the text block: CASBKAU , to the dialogue handler's script.
		CASE HASH("HS3FAAU")	// url:bugstar:6047746 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3FAAU , to the dialogue handler's script.
		CASE HASH("HS3PRAU")	// url:bugstar:6047747 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3PRAU , to the dialogue handler's script.
		CASE HASH("HS3CWAU")	// url:bugstar:6047877 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3CWAU , to the dialogue handler's script.
		CASE HASH("HS3LEAU")	// url:bugstar:6047878 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3LEAU , to the dialogue handler's script.
		CASE HASH("HS3SPAU")	// url:bugstar:6047879 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3SPAU , to the dialogue handler's script.
		CASE HASH("HS3CEAU")	// url:bugstar:6047880 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3CEAU , to the dialogue handler's script.
		CASE HASH("HS3REAU")	// url:bugstar:6051374 - Added new placeholder dialogue to the mpHeist3 DLC pack. Please add the text block: HS3REAU, to the dialogue handler's script.
		CASE HASH("HS3FIAU")	// url:bugstar:6047876 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3FIAU, to the dialogue handler's script.	
		CASE HASH("HS3MNAU")	// url:bugstar:6068035 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3MNAU, to the dialogue handler's script.
		CASE HASH("HS3VNAU")	// url:bugstar:6073013 - Added new dialogue to the mpHeist3 DLC pack. Please add the text block: HS3VNAU, to the dialogue handler's script.
		CASE HASH("CCYCAUD")	// url:bugstar:6359673 - Added new placeholder dialogue to the Cops and Crooks DLC pack. Please add the text block: CCYCAUD, to the dialogue handler's script.
			RETURN TRUE
	ENDSWITCH
	
	SWITCH iHash
		CASE HASH("H4AEXAU")	// url:bugstar:6595456 - Added new placeholder dialogue to the Heist 4 DLC pack. Please add the text block: H4AEXAU, to the dialogue handler's script.
		CASE HASH("HS4PAAU")	// url:bugstar:6660115 - Added new placeholder dialogue to the mpHeist4 DLC pack. Please add the text block: HS4PAAU, to the dialogue handler's script.
		CASE HASH("HS4C1AU")	// url:bugstar:6660116 - Added new placeholder dialogue to the mpHeist4 DLC pack. Please add the text block: HS4C1AU, to the dialogue handler's script.
		CASE HASH("HS4C2AU")	// url:bugstar:6660118 - Added new placeholder dialogue to the mpHeist4 DLC pack. Please add the text block: HS4C2AU, to the dialogue handler's script.
		CASE HASH("HS4EDAU")	// url:bugstar:6660119 - Added new placeholder dialogue to the mpHeist4 DLC pack. Please add the text block: HS4EDAU, to the dialogue handler's script.
		CASE HASH("HS4BPAU")	// url:bugstar:6669084 - Added new placeholder dialogue to the Heist 4 DLC pack. Please add the text block: HS4BPAU, to the dialogue handler's script.
		CASE HASH("HS4MOAU")	// url:bugstar:6687579 - Added new placeholder dialogue to the Heist 4 DLC pack. Please add the text block: HS4MOAU, to the dialogue handler's script.
		CASE HASH("HS4PIAU")	// url:bugstar:6693854 - Added new dialogue to the Heist 4 DLC pack. Please add the text block: HS4PIAU, to the dialogue handler's script.
		CASE HASH("HS4FIAU")	// url:bugstar:6693856 - Added new dialogue to the Heist 4 DLC pack. Please add the text block: HS4FIAU, to the dialogue handler's script.
		CASE HASH("H4SCPAU")
		CASE HASH("HS4FAUD")
		CASE HASH("HS4CSAU")	// url:bugstar:6720283 - Added new dialogue to the Heist 4 DLC pack. Please add the text block: HS4CSAU, to the dialogue handler's script.
		CASE HASH("HS4PSAU")	// url:bugstar:6774265 - Added new dialogue to the Heist 4 DLC pack. Please add the text block: HS4PSAU, to the dialogue handler's script.
		CASE HASH("TNSSAUD")	// url:bugstar:7007515 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNSSAUD, to the dialogue handler's script.
		CASE HASH("TNMMAUD")	// url:bugstar:7007518 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNMMAUD, to the dialogue handler's script.
		CASE HASH("TNRPAUD")	// url:bugstar:7007523 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNRPAUD, to the dialogue handler's script.
		CASE HASH("TNFUDAU")	// url:bugstar:7007527 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFUDAU, to the dialogue handler's script.		
		CASE HASH("TNFMCAU")	// url:bugstar:7007529 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFMCAU, to the dialogue handler's script.
		CASE HASH("TNFIAAU")	// url:bugstar:7007540 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFIAAU, to the dialogue handler's script.
		CASE HASH("TNFFTAU")	// url:bugstar:7007535 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFFTAU, to the dialogue handler's script.
		CASE HASH("TNFBOAU")	// url:bugstar:7007537 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFBOAU, to the dialogue handler's script.
		CASE HASH("TNFFLAU")	// url:bugstar:7007532 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFFLAU, to the dialogue handler's script.
		CASE HASH("TNFBKAU")	// url:bugstar:7007547 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFBKAU, to the dialogue handler's script.
		CASE HASH("TNFMHAU")	// url:bugstar:7007543 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFMHAU, to the dialogue handler's script.
		CASE HASH("TNFBBAU")	// url:bugstar:7007553 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNFBBAU, to the dialogue handler's script.
		CASE HASH("TNCUTAU")	// url:bugstar:7030925 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNCUTAU, to the dialogue handler's script.
		CASE HASH("TNHEAUD")	// url:bugstar:7007521 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNHEAUD, to the dialogue handler's script.
		CASE HASH("TNARAUD")	// url:bugstar:6990238 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TNARAUD, to the dialogue handler's script.
		CASE HASH("TUMSAUD")	// url:bugstar:7091664 - Added new placeholder dialogue to the Tuner DLC pack. Please add the text block: TUMSAUD, to the dialogue handler's script.
		CASE HASH("TNCSBAU")	// url:bugstar:7079163 - Added new dialogue to the Tuner MP DLC pack. Please add the text block: TNCSBAU to the dialogue handler's script.
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE HASH("G9HAAUD")	// url:bugstar:7177533 - Added new placeholder dialogue to the Gen9 DLC pack. Please add the text block: G9HAAUD, to the dialogue handler's script.
		#ENDIF
		CASE HASH("LOWCAAU")
		CASE HASH("LCAU")
		#IF FEATURE_FIXER
		CASE HASH("FXDL0AU")	// url:bugstar:7244232 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL0AU to the dialogue handler's script.
		CASE HASH("FXDL1AU")	// url:bugstar:7244236 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL1AU to the dialogue handler's script.
		CASE HASH("FXDL2AU")	// url:bugstar:7244237 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL2AU to the dialogue handler's script.
		CASE HASH("FXDL3AU")	// url:bugstar:7244243 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL3AU to the dialogue handler's script.
		CASE HASH("FXDL4AU")	// url:bugstar:7244244 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL4AU to the dialogue handler's script.
		CASE HASH("FXDL5AU")	// url:bugstar:7244246 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXDL5AU to the dialogue handler's script.
		CASE HASH("FXFLAUD")	// url:bugstar:7244249 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXFLAUD to the dialogue handler's script.
		CASE HASH("FXFRAUD")	// url:bugstar:7244255 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXFRAUD to the dialogue handler's script.
		CASE HASH("FXIMAUD")	// url:bugstar:7244256 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXIMAUD to the dialogue handler's script.
		CASE HASH("FXLMAUD")	// url:bugstar:7244258 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXLMAUD to the dialogue handler's script.
		CASE HASH("FXBAAUD")	// url:bugstar:7244261 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXBAAUD to the dialogue handler's script.
		CASE HASH("FXFAAUD")	// url:bugstar:7244264 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXFAAUD to the dialogue handler's script.
		CASE HASH("FXCUTAU")	// url:bugstar:7266172 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXCUTAU to the dialogue handler's script.
		CASE HASH("FXIGAUD")	// url:bugstar:7275259 - Added new placeholder dialogue to the Fixer DLC pack. Please add the text block: FXIGAUD to the dialogue handler's script.
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE HASH("SM2AGAU")	// url:bugstar:7634306 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2AGAU, to the dialogue handler's script.
		CASE HASH("SM2MAAU")	// url:bugstar:7634307 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2MAAU, to the dialogue handler's script.
		CASE HASH("SM2TOAU")	// url:bugstar:7634308 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2TOAU, to the dialogue handler's script.
		CASE HASH("SM2WBAU")	// url:bugstar:7634310 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2WBAU, to the dialogue handler's script.
		CASE HASH("SM2YOAU")	// url:bugstar:7634312 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2YOAU, to the dialogue handler's script.
		CASE HASH("SM2ULAU")	// url:bugstar:7634504 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2ULAU, to the dialogue handler's script.
		CASE HASH("SM2U1AU")	// url:bugstar:7656134 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U1AU, to the dialogue handler's script.
		CASE HASH("SM2U2AU")	// url:bugstar:7656135 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U2AU, to the dialogue handler's script.
		CASE HASH("SM2U3AU")	// url:bugstar:7656136 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U3AU, to the dialogue handler's script.
		CASE HASH("SM2U4AU")	// url:bugstar:7656139 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U4AU, to the dialogue handler's script.
		CASE HASH("SM2U5AU")	// url:bugstar:7656140 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U5AU, to the dialogue handler's script.
		CASE HASH("SM2U6AU")	// url:bugstar:7656141 - Added new placeholder dialogue to the MpSum2 DLC pack. Please add the text block: SM2U6AU, to the dialogue handler's script.
		#ENDIF		
		#IF FEATURE_DLC_2_2022
		CASE HASH("XM31AUD")	// 	url:bugstar:7954504 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM31AUD, to the dialogue handler's script.
		CASE HASH("XM32AUD")	// 	url:bugstar:7954508 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM32AUD, to the dialogue handler's script.
		CASE HASH("XM33AUD")	// 	url:bugstar:7954510 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM33AUD, to the dialogue handler's script.
		CASE HASH("XM34AUD")	// 	url:bugstar:7954513 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM34AUD, to the dialogue handler's script.
		CASE HASH("XM35AUD")	// 	url:bugstar:7954517 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM35AUD, to the dialogue handler's script.
		CASE HASH("XM36AUD")	// 	url:bugstar:7954518 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM36AUD, to the dialogue handler's script.
		CASE HASH("XM37AUD")	// 	url:bugstar:7954520 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM37AUD, to the dialogue handler's script.
		CASE HASH("XM38AUD")	// 	url:bugstar:7954525 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM38AUD, to the dialogue handler's script.
		CASE HASH("XM39AUD")	// 	url:bugstar:7954526 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM39AUD, to the dialogue handler's script.
		CASE HASH("XM310AU")	// 	url:bugstar:7954527 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM310AU, to the dialogue handler's script.
		CASE HASH("XM311AU")	// 	url:bugstar:7954528 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM311AU, to the dialogue handler's script.
		CASE HASH("XM3IGAU")	// 	url:bugstar:7954532 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3IGAU, to the dialogue handler's script.
		CASE HASH("XM3LAAU")	//	url:bugstar:7972651 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3LAAU, to the dialogue handler's script.
		CASE HASH("XM3DXAU")	//	url:bugstar:7972657 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3DXAU, to the dialogue handler's script.
		CASE HASH("XM3GVAU")	//	url:bugstar:7972659 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3GVAU, to the dialogue handler's script.
		CASE HASH("XM3LRAU")	//	url:bugstar:7972666 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3LRAU, to the dialogue handler's script.
		CASE HASH("XM3LDAU")	//	url:bugstar:7972701 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3LDAU, to the dialogue handler's script.
		CASE HASH("XM3RNAU")	//	url:bugstar:7972759 - Added new placeholder dialogue to the mpChristmas3 DLC pack. Please add the text block: XM3RNAU, to the dialogue handler's script.
		#ENDIF
			RETURN TRUE
	ENDSWITCH
	
	#IF FEATURE_COPS_N_CROOKS
	SWITCH iHash
		CASE HASH("CCBSAUD")	// url:bugstar:6359672 - Added new placeholder dialogue to the Cops and Crooks DLC pack. Please add the text block: CCBSAUD, to the dialogue handler's script.
		CASE HASH("CCDFAUD")	// url:bugstar:6359676 - Added new placeholder dialogue to the Cops and Crooks DLC pack. Please add the text block: CCDFAUD, to the dialogue handler's script.
		CASE HASH("CCDPAUD")	// url:bugstar:6359677 - Added new placeholder dialogue to the Cops and Crooks DLC pack. Please add the text block: CCDPAUD, to the dialogue handler's script.
		CASE HASH("CCBNKAU")	// url:bugstar:6398915 - Added new dialogue to the Cops and Crooks DLC pack. Please add the text block: CCBNKAU, to the dialogue handler's script.
			RETURN TRUE
	ENDSWITCH	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(g_tl31_Block_DiagDebug)
		AND NOT ARE_STRINGS_EQUAL(g_tl31_Block_DiagDebug, "null")
			PRINTLN("[ConversationDebug] - iHash: ", iHash, " g_ConversationDebugCustomBlock as hash: ", GET_HASH_KEY(g_tl31_Block_DiagDebug))
			
			IF iHash = GET_HASH_KEY(g_tl31_Block_DiagDebug)
				PRINTLN("[ConversationDebug] - g_ConversationDebugCustomBlock is problem text = TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
		
	RETURN FALSE
ENDFUNC



PROC CommonExtractSpeakersListeners()


    INT textLabelIncrement = 1
    INT textLabelSequentialSuffix = 1
    INT labelIndex = 0
    
    ConversationBuilt = FALSE
    IsConversationPaused = FALSE

    INT SpeakerNumber, ListenerNumber, VolumeType

      
    BOOL LineInterruptibleBool, DucksRadioBool, DucksScoreBool, HeadSetBool, DoNotInterruptForSpecAbBool, IsPadSpeakerRoute
    eAUDIBILITY AudibilityEnum


    bool_BlockLoadFailed = FALSE

    /*
    IF NOT DOES_TEXT_BLOCK_EXIST (g_BlockToLoad)

            IF NOT IS_CURRENT_MISSION_DLC() //Must be an original GTA5 mission, use standard dialogue slot.

                #if IS_DEBUG_BUILD
                    
                    cdPrintstring ("DIALOG_HAND - Jan 14 NO BLOCK FOUND IN GXT WITH NAME PASSED! Conversation will be empty.")
                    cdPrintnl()

                    cdPrintstring ("DIALOG_HAND - Block passed was ")
                    cdPrintstring (g_BlockToLoad)
                    cdPrintnl()

                    cdPrintstring ("DIALOG_HAND - Conversation root was ")
                    cdPrintstring (g_ConversationData.ConversationSegmentToGrab)
                    cdPrintnl()



                    SCRIPT_ASSERT ("Conversation text block requested not valid in gxt. Check TTY for details. Please pass bug to owner of mission.")

               #endif   
        

               bool_BlockLoadFailed = TRUE

           ENDIF


    ENDIF
    */




    IF DOES_TEXT_BLOCK_EXIST (g_BlockToLoad)


        //1972746 - CT_AUD block causes the vast majority of problems when used in DLC missions. This is a legacy block so we will force this to look at american.rpf
        //whenever it has been requested as a general rule.
        IF ARE_STRINGS_EQUAL (g_BlockToLoad, "CT_AUD")


            g_ForceDLC_DialogueRequest = TRUE
        
    
            #if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND - WARNING! Passed text block is non-dlc use. Forcing load attempt from standard rpf. Ignoring any DLC flags.")
                cdPrintstring (g_BlockToLoad)
                cdPrintnl()
            #endif

        ENDIF




        #if IS_DEBUG_BUILD
            cdPrintstring ("DIALOG_HAND - Jan 14 DLC Compatibility version. Loading text block: ")
            cdPrintstring (g_BlockToLoad)
            cdPrintnl()
        #endif   


        INT i_ForceOrbisDurangoToUseDLCSlot = 0 //If this remains at zero, then we cannot be in Next Gen.


        IF IS_PLAYSTATION_PLATFORM()  
        OR IS_XBOX_PLATFORM()

            #if USE_TU_CHANGES

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. Next gen version detected and running TU changes")
                    cdPrintnl()
                #endif

            #endif



                //We were forcing all Next Gen build text, including assassination DLC to use legacy regular dialogue slot
                //loading rather than looking for it with the REQUEST_ADDITIONAL_TEXT_FOR_DLC command. Dave Watson's C and C work
                //needed to bypass this check on NG, so if he sets g_bUse_MP_DLC_Dialogue to TRUE, then it will look at the DLC slots.

                //If we're in last gen, i_ForceOrbisDurangoToUseDLCSlot remains at 0 and is ignored.
                //If i_ForceOrbisDurangoToUseDLCSlot = 1 then MP_DLC_Dialogue is required, look at DLC slots.
                //If i_ForceOrbisDurangoToUseDLCSlot = 2 then we're in NG but still want to use the regular slots.



            IF g_bUse_MP_DLC_Dialogue = TRUE //Fix for Dave W. Next gen C and C dialogue not working on PS4.

                i_ForceOrbisDurangoToUseDLCSlot = 1

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. b_ForceOrbisDurangoToUseDLCSlot set to 1 due to g_bUse_MP_DLC_Dialogue being TRUE")
                    cdPrintnl()
                #endif



            ELSE

                i_ForceOrbisDurangoToUseDLCSlot = 2

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. i_ForceOrbisDurangoToUseDLCSlot set to 2 due to g_bUse_MP_DLC_Dialogue being FALSE")
                    cdPrintnl()
                #endif


            ENDIF

        ENDIF
        
        #if USE_TU_CHANGES
        
            BOOL b_UseRegularAdditionalText = FALSE         //Default to looking for DLC dialogue in TU
            
            IF g_bInMultiplayer
            OR IS_FAKE_MULTIPLAYER_MODE_SET()
            
                IF g_bUse_MP_DLC_Dialogue = FALSE
                
                    b_UseRegularAdditionalText = TRUE       //If we are in MP and the use_MP_DLC_dialogue flag is not set, assume this is regular text.
                    
                    #if IS_DEBUG_BUILD
                    
                        cdPrintnl()
                        cdPrintstring ("FEB 14 - Text Block Check. MP check forced b_UseRegularAdditionalText to TRUE.")
                        cdPrintnl()
                        
                    #endif
                    
                ENDIF
                
            ELSE
            
                IF (NOT IS_CURRENT_MISSION_DLC())
                
                    b_UseRegularAdditionalText = TRUE       //If we are in SP and are not running a DLC mission, assume this is regular text.
                    
                    #if IS_DEBUG_BUILD
                    
                        cdPrintnl()
                        cdPrintstring ("FEB 14 - Text Block Check. SP check forced b_UseRegularAdditionalText to TRUE.")
                        cdPrintnl()
                        
                    #endif
                    
                ENDIF
                
            ENDIF
            
        #endif


        //Fix for 2641906 Part 1. If problematic Apartments blocks are detected, ignore any rpf pointing flags and default the system to grab the block from the DLC rpf.
		IF IsProblemTextBlock(GET_HASH_KEY(g_BlockToLoad))


			b_UseRegularAdditionalText = FALSE
			g_ForceDLC_DialogueRequest = FALSE
			i_ForceOrbisDurangoToUseDLCSlot = 1
			
			#if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND Jan 14 - 2641906- Apartments or DLC text block detected. Grabbing from DLC rpf regardless of any flag states.")
            	cdPrintnl()
            #endif
		ENDIF




        //If a text block does not load, then dialogue handler will remain stuck here...

        IF (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) < (ENUM_TO_INT(CONV_PRIORITY_VERY_LOW)) //This means the text has been passed with an ambient priority level.
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_MP_SPECIAL))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE))


            #if USE_TU_CHANGES
                IF b_UseRegularAdditionalText = TRUE //Must be an original GTA5 mission or we are in MP and use_MP_DLC flag is not set, use standard dialogue slot.
                OR g_ForceDLC_DialogueRequest = TRUE
                OR i_ForceOrbisDurangoToUseDLCSlot = 2 //We must be in NG if this is greater than zero but the MP_DLC flag has not been set. If in NG, default text to look at standard slots.        
            #endif
                    
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND Jan 14 - Cellphone call or conversation text about to load into ambient dialogue text slot")
                        cdPrintnl()
                    #endif


                    REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for additional cellphone call or conversation text to load into ambient dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE


            #if USE_TU_CHANGES
            
                ELSE

                    #if IS_DEBUG_BUILD
                        cdPrintstring("DLC dialogue detected - Attempting to load block into DLC ambient slot ")
                        cdPrintnl()
                    #endif

                    REQUEST_ADDITIONAL_TEXT_FOR_DLC (g_BlockToLoad, DLC_AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (DLC_AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for additional cellphone call or conversation text to load into DLC ambient dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE

                ENDIF
                

            #endif
                    


        ELSE

            #if USE_TU_CHANGES
                IF b_UseRegularAdditionalText = TRUE //Must be an original GTA5 mission or we are in MP and use_MP_DLC flag is not set, use standard dialogue slot.
                OR g_ForceDLC_DialogueRequest = TRUE
                OR i_ForceOrbisDurangoToUseDLCSlot = 2 //We must be in NG if this is greater than zero but the MP_DLC flag has not been set. If in NG default text to look at standard slots.        
            #endif

                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND Jan 14 - Cellphone call or conversation text about to load into mission dialogue text slot")
                        cdPrintnl()
                    #endif


                    REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for additional cellphone call or conversation text to load into mission dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE                    


            
            #if USE_TU_CHANGES

                ELSE

                    #if IS_DEBUG_BUILD
                        cdPrintstring("DLC dialogue detected - Attempting to load block into DLC mission slot")
                        cdPrintnl()
                    #endif

                  	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_mission_controller_2020")) = 0
	                    REQUEST_ADDITIONAL_TEXT_FOR_DLC (g_BlockToLoad, DLC_MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
	                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (DLC_MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
	                        WAIT(0)
	                        #if IS_DEBUG_BUILD
	                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for additional cellphone call or conversation text to load into DLC mission dialogue text slot")
	                            cdPrintnl()
	                        #endif
	                    ENDWHILE
	                ENDIF
                ENDIF

            #endif
     


        ENDIF


    

    ELSE

           #if IS_DEBUG_BUILD
                
                cdPrintstring ("DIALOG_HAND - NO BLOCK FOUND IN GXT WITH NAME PASSED! Conversation will be empty.")
                cdPrintnl()

                cdPrintstring ("DIALOG_HAND - Block passed was ")
                cdPrintstring (g_BlockToLoad)
                cdPrintnl()

                cdPrintstring ("DIALOG_HAND - Conversation root was ")
                cdPrintstring (g_ConversationData.ConversationSegmentToGrab)
                cdPrintnl()



                SCRIPT_ASSERT ("Conversation text block requested not valid in gxt. Check TTY for details. Please pass bug to owner of mission.")

           #endif   
    

            bool_BlockLoadFailed = TRUE


        EXIT

    ENDIF
    


    CREATE_NEW_SCRIPTED_CONVERSATION()


    GetSpeakerListenerLabel ()

    GetMasterInterruptibleAnimTriggeredLabel ()

    GetLineFlagsLabel ()

    
  


        


    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Building conversation from root: ")
        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
        cdPrintnl()

        b_PlayingRandomLine = FALSE


    #endif





    AddPedsDataForConversation ()
      
                            
                                                                                                                       
    g_DialogueAdjustment = 0 //default this so that normal sequential play has a zero value regardless.


    WHILE labelIndex < constConversationLabels
    AND ConversationBuilt = FALSE

    


        randomAudioTextLabel = g_ConversationData.ConversationSegmentToGrab
        randomaudioTextLabel += "A"
       
   
        g_ConversationLabels[labelIndex] = g_ConversationData.ConversationSegmentToGrab

        


        g_ConversationLabels[labelIndex] += "_"
        g_ConversationLabels[labelIndex] += textLabelSequentialSuffix

    

        audioTextLabel = g_ConversationLabels[labelIndex]
        audioTextLabel += "A"




                                                                                                                           
        IF DOES_TEXT_LABEL_EXIST (audioTextLabel)//(g_ConversationLabels[labelIndex])
                                                                                                     
       
            //audioFilename = GET_STRING_FROM_TEXT_FILE(audioTextLabel)

            audioFilename = GET_FILENAME_FOR_AUDIO_CONVERSATION (audioTextLabel)
            
            //Get corresponding Speaker, Listener number from SL label to include in ADD_LINE_TO_CONVERSATION
            //Speaker Ints are present in the 1st, 4th, 7th, 10th integers in the SL string. As the label index starts at zero we can use 
            //the following formula to correctly extract the Speaker int for each line.  You don't need to add 1, because the GET_NTH function
            //returns zero for the first character.
            //Accordingly, Listener Ints will be, 2nd, 5th, 8th and the Volumetype Int is the 3rd, 6th and 9th 

               







            SpeakerNumber = GetSpeakerIntFromString (labelIndex)
            //SpeakerNumber = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, (labelIndex * 3))
           
            IF SpeakerNumber = 9
    
                #if USE_TU_CHANGES
                IF g_B_ContentCreationPeds_Should_be_added_for_Dialogue = FALSE
                #endif

                #if IS_DEBUG_BUILD

                    SCRIPT_ASSERT("9 is reserved for special listener audio context but speaker declared as 9. See Steve T.")                 
                        
                #endif
                
                #if USE_TU_CHANGES
                ENDIF
                #endif

                #if IS_DEBUG_BUILD

                    cdPrintstring("DIALOG_HAND - WARNING Speaker number set to 9! May cause problems! ")                 
                    cdPrintnl()

                #endif


            ENDIF


           
           
            ListenerNumber = GetListenerIntFromString (labelIndex)
            //ListenerNumber = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, ((labelIndex * 3) + 1))
            
            
            VolumeType = GetVolumeTypeIntFromString (labelIndex)
            //VolumeType = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, ((labelIndex * 3) + 2))


            MasterInterruptibleBool = GetMasterInterruptibleIntFromString_and_Convert_to_Bool()

            MasterAnimTriggeredBool = GetMasterAnimTriggeredIntFromString_and_Convert_to_Bool()
            
            MasterPlaceholderBool = GetMasterPlaceholderIntFromString_and_Convert_to_Bool()


            LineInterruptibleBool = GetLineInterruptibleIntFromString_and_Convert_to_Bool(labelIndex)
            
            DucksRadioBool = GetDucksRadioInt_and_Convert_to_Bool(labelIndex)

            DucksScoreBool = GetDucksScoreInt_and_Convert_to_Bool(labelIndex)
            
            AudibilityEnum = GetAudibilityEnumFromString (labelIndex)

            HeadSetBool = GetHeadSetInt_and_Convert_to_Bool(labelIndex)

            DoNotInterruptForSpecAbBool = GetDoNotInterruptForSpecAbInt_and_Convert_to_Bool(labelIndex)
            
            IsPadSpeakerRoute = GetIsPadSpeakerRouteInt_and_Convert_to_Bool(labelIndex)







                

            IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[labelIndex])


                IF g_PlayingSingleLine = FALSE  //Play this conversation in sequential order.
                AND g_PlayingFromLine = FALSE

                    IF SpeakerNumber > 36 //0-9, A-Z
                    OR ListenerNumber > 36
                                         
                        #IF IS_DEBUG_BUILD

                            SCRIPT_ASSERT("Speaker or Listener value larger than 36 is invalid!")                 
                        
                        #endif
                    
                    ELSE

                        #if IS_DEBUG_BUILD


                            cdPrintstring("DIALOG_HAND - Found this label in text files:")
                            cdPrintstring(g_ConversationLabels[labelIndex])
                            cdPrintstring("DIALOG_HAND - Adding line to conv. with speaker value ")                 
                            cdPrintint(SpeakerNumber)
                            cdPrintstring(" and listener number ")                 
                            cdPrintint(ListenerNumber)
                            cdPrintstring(" and volume type ")                 
                            cdPrintint(VolumeType)
                            cdPrintString(" to CLindex ")
                            cdPrintint(labelIndex)
                            cdPrintnl()

                            IF LineInterruptibleBool = TRUE
                                cdPrintstring("   LineInt:1 ")
                            ELSE
                                cdPrintstring("   LineInt:0 ")
                            ENDIF
                           
                            IF DucksRadioBool = TRUE
                                cdPrintstring(" DucksRadio:1 ")
                            ELSE
                                cdPrintstring(" DucksRadio:0 ")
                            ENDIF
                           
                            IF DucksScoreBool = TRUE
                                cdPrintstring(" DucksScore:1 ")
                            ELSE
                                cdPrintstring(" DucksScore:0 ")
                            ENDIF
                        

                            cdPrintstring(" Audibility:")
                            cdPrintInt (ENUM_TO_INT (AudibilityEnum))


                            IF HeadSetBool = TRUE
                                cdPrintString(" HeadSetInt:1 ")
                            ELSE
                                cdPrintString(" HeadSetInt:0 ")
                            ENDIF


                            IF DoNotInterruptForSpecAbBool = TRUE
                                cdPrintString(" DoNotInterruptForSpecAbInt:1 ")
                            ELSE
                                cdPrintString(" DoNotInterruptForSpecAbInt:0 ")
                            ENDIF
                            
                            IF IsPadSpeakerRoute = TRUE
                                cdPrintString(" IsPadSpeakerRouteInt:1 ")
                            ELSE
                                cdPrintString(" IsPadSpeakerRouteInt:0 ")
                            ENDIF






                            cdPrintnl()

                        #endif

                    ENDIF


                    //This needs changed to support speakers , listeners greater than 9 by code. AZSL work. Done.
         
                    ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, g_ConversationLabels[labelIndex], ListenerNumber, VolumeType, FALSE, 
                                                LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool,
                                                IsPadSpeakerRoute)


                    //PRINTSTRING("Adding line 1")
                    //PRINTNL()


                ELSE

                    //If we want to play a single line, only add the line which specifically matches the label passed in to g_SpecificLabel

                    IF ARE_STRINGS_EQUAL (g_ConversationLabels[labelIndex], g_SpecificLabel)

                   
                        IF SpeakerNumber > 36//9 
                        OR ListenerNumber > 36//9
                            
                            #if IS_DEBUG_BUILD

                                SCRIPT_ASSERT("Single line play - Speaker or Listener value larger than 36 is invalid!")                 
                            
                            #endif
                        
                        ELSE

                            #if IS_DEBUG_BUILD

                                cdPrintstring("DIALOG_HAND - Found this label in text files:")
                                cdPrintstring(g_ConversationLabels[labelIndex])

                                cdPrintstring("DIALOG_HAND - Adding single line or resumption line to conversation with speaker value ")                 
                                cdPrintint(SpeakerNumber)
                                cdPrintstring(" and listener value ")                 
                                cdPrintint(ListenerNumber)
                                cdPrintstring(" and volume type ")                 
                                cdPrintint(VolumeType)
                                cdPrintString(" to CLindex ")
                                cdPrintint(labelIndex)

                                cdPrintnl()

                            #endif

                        ENDIF
                        
                        ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, g_ConversationLabels[labelIndex], ListenerNumber, VolumeType, FALSE,
                                                    LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool, 
                                                    IsPadSpeakerRoute)

                        g_DialogueAdjustment = labelIndex


                        //PRINTSTRING("AdjustmentIndex is...  ")
                        //PRINTINT(g_DialogueAdjustment)
                        //PRINTNL()
                        
                        //PRINTSTRING("Adding line 2")
                        //PRINTNL()




                        IF g_PlayingFromLine = TRUE

                            g_PlayingFromLine = FALSE //Starting label has been identified, safe to add rest of labels.

                        ENDIF

                    ENDIF

                ENDIF


            ELSE  //if the audio label exists but the subtitle label does not then it must be a SFX pause...   NOT allowing script to play single line sfx pauses at this point...

                
                
                IF g_PlayingFromLine = FALSE
                AND g_PlayingSingleLine = FALSE  // Don't include SFX or audio pauses in conversation array
                
                    //This used to work by passing in a dummy "SFX" subtitle but now code magic knows when a line is an sfx line, so won't display the subtitle.
                    ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, audioFilename, ListenerNumber, VolumeType, FALSE,
                                                LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool, 
                                                IsPadSpeakerRoute)

                    //PRINTSTRING("Adding line 3")
                    //PRINTNL()

                    
                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND - SFX or pause label is ")
                        cdPrintstring(audioFilename)
                        cdPrintstring("DIALOG_HAND - Adding SFX or audio pause to CL index ")
                        cdPrintint(labelIndex)
                        cdPrintnl()

                    #endif
                    

                ENDIF



            ENDIF

            //Increment suffix by label increment
            textLabelSequentialSuffix = textLabelSequentialSuffix + textLabelIncrement


  
        ELSE


            //Check for random conversation here
            
            IF DOES_TEXT_LABEL_EXIST (randomAudioTextLabel)

           
                //audioFilename = GET_STRING_FROM_TEXT_FILE(randomAudioTextLabel)

                audioFilename = GET_FILENAME_FOR_AUDIO_CONVERSATION (randomAudioTextLabel)

                SpeakerNumber = GetSpeakerIntFromString (labelIndex)

                ListenerNumber = GetListenerIntFromString (labelIndex)
                        
                VolumeType = GetVolumeTypeIntFromString (labelIndex)


                MasterInterruptibleBool = GetMasterInterruptibleIntFromString_and_Convert_to_Bool()

                MasterAnimTriggeredBool = GetMasterAnimTriggeredIntFromString_and_Convert_to_Bool()
                
                MasterPlaceholderBool = GetMasterPlaceholderIntFromString_and_Convert_to_Bool()


                LineInterruptibleBool = GetLineInterruptibleIntFromString_and_Convert_to_Bool(labelIndex)
            
                DucksRadioBool = GetDucksRadioInt_and_Convert_to_Bool(labelIndex)

                DucksScoreBool = GetDucksScoreInt_and_Convert_to_Bool(labelIndex)

                AudibilityEnum = GetAudibilityEnumFromString (labelIndex)

                HeadSetBool = GetHeadSetInt_and_Convert_to_Bool(labelIndex)

                DoNotInterruptForSpecAbBool = GetDoNotInterruptForSpecAbInt_and_Convert_to_Bool(labelIndex)
                
                IsPadSpeakerRoute = GetIsPadSpeakerRouteInt_and_Convert_to_Bool(labelIndex)


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND DESIGNER DEBUG - Found audio label for random conversation: ")
                    cdPrintstring(randomAudioTextLabel)
                    cdPrintstring(" with filename: ")
                    cdPrintstring(audioFilename)
                    cdPrintstring(" Root: ")
                    cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
                    cdPrintnl()
                    cdPrintstring("Speaker value ")
                    cdPrintint(SpeakerNumber)
                    cdPrintstring(" and listener value ")                 
                    cdPrintint(ListenerNumber)

                    cdPrintstring(" and volume type ")                 
                    cdPrintint(VolumeType)

                    cdPrintnl()
                
                    b_PlayingRandomLine = TRUE

                #endif
                

              
                ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, g_ConversationData.ConversationSegmentToGrab, ListenerNumber, VolumeType, TRUE,
                                            LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool,
                                            IsPadSpeakerRoute)

                    //PRINTSTRING("Adding line 4")
                    //PRINTNL()

           
            
            ENDIF


            //g_ConversationLabels[labelIndex] = "END"

            ConversationBuilt = TRUE
    
        ENDIF

        labelIndex ++

            IF labelIndex = constConversationLabels

                #if IS_DEBUG_BUILD
                    SCRIPT_ASSERT ("DIALOG_HAND - More potential labels in this conversation than allowed for in global conversation label array. See Steve T.")
                #endif

            ENDIF


    ENDWHILE


ENDPROC


























PROC BuildFaceToFaceConversation() //CONV_STATE_BUILDING



    CommonExtractSpeakersListeners()


    #if IS_DEBUG_BUILD
        
        cdPrintnl()
        cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - Built face to face conversation OK. Now playing: ")
        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)

        cdPrintnl()

        IF MasterAnimTriggeredBool 

            cdPrintnl()
            cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - This Conversation has been passed as ANIM TRIGGERED!")
            cdPrintnl()

        ELSE
        
            cdPrintnl()
            cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - This Conversation is not anim triggered.")
            cdPrintnl()

        ENDIF


    #endif


      
    SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM (MasterAnimTriggeredBool) //Here? #1130859

    SET_CONVERSATION_AUDIO_PLACEHOLDER (MasterPlaceholderBool)

    IF g_IsThisFaceToFacePreloaded


        #if IS_DEBUG_BUILD  
                                                                            
            cdPrintnl()              
            cdPrintstring("DIALOG_HAND - Face to face conversation fired using native PRELOAD...")
            cdPrintnl()
    
        #endif


        PRELOAD_SCRIPT_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen, g_ConversationData.g_CloneConversation, MasterInterruptibleBool) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.
        
        i_FaceToFacePreloadStartTime = GET_GAME_TIMER()


    ELSE

        #if IS_DEBUG_BUILD  
                                                                            
            cdPrintnl()              
            cdPrintstring("DIALOG_HAND - Face to face conversation fired using native START...")
            cdPrintnl()
    
        #endif


        START_SCRIPT_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen, g_ConversationData.g_CloneConversation, MasterInterruptibleBool) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.

    ENDIF
   



    g_ConversationStatus = CONV_STATE_PLAYING



ENDPROC













PROC InitialiseMultipartConversation() //CONV_STATE_BUILDING


    
        //Legacy routine pre- dlc.
        //If a text block does not load, then dialogue handler will remain stuck here...
        /*
        IF (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) < (ENUM_TO_INT(CONV_PRIORITY_VERY_LOW)) //This means the text has been passed with an ambient priority level.
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_MP_SPECIAL))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE))


            REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
            WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                WAIT(0)
                #if IS_DEBUG_BUILD
                    cdPrintstring ("DIALOG_HAND - Waiting for additional cellphone call or conversation text to load into ambient dialogue text slot")
                    cdPrintnl()
                #endif
            ENDWHILE

        ELSE

            REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
            WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                WAIT(0)
                #if IS_DEBUG_BUILD
                    cdPrintstring ("DIALOG_HAND - Waiting for additional cellphone call or conversation text to load into mission dialogue text slot")
                    cdPrintnl()
                #endif
            ENDWHILE

        ENDIF
        */



        INT i_ForceOrbisDurangoToUseDLCSlot = 0 //If this remains at zero, then we cannot be in Next Gen.


        IF IS_PLAYSTATION_PLATFORM()  
        OR IS_XBOX_PLATFORM()

            #if USE_TU_CHANGES

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. Next gen version detected and running TU changes")
                    cdPrintnl()
                #endif


                //We were forcing all Next Gen build text, including assassination DLC to use legacy regular dialogue slot
                //loading rather than looking for it with the REQUEST_ADDITIONAL_TEXT_FOR_DLC command. Dave Watson's C and C work
                //needed to bypass this check on NG, so if he sets g_bUse_MP_DLC_Dialogue to TRUE, then it will look at the DLC slots.

                //If we're in last gen, i_ForceOrbisDurangoToUseDLCSlot remains at 0 and is ignored.
                //If i_ForceOrbisDurangoToUseDLCSlot = 1 then MP_DLC_Dialogue is required, look at DLC slots.
                //If i_ForceOrbisDurangoToUseDLCSlot = 2 then we're in NG but still want to use the regular slots.


            #endif


            IF g_bUse_MP_DLC_Dialogue = TRUE //Fix for Dave W. Next gen C and C dialogue not working on PS4.

                i_ForceOrbisDurangoToUseDLCSlot = 1

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. Multipart b_ForceOrbisDurangoToUseDLCSlot set to 1 due to g_bUse_MP_DLC_Dialogue being TRUE")
                    cdPrintnl()
                #endif



            ELSE

                i_ForceOrbisDurangoToUseDLCSlot = 2

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("FEB 14 - Dialogue Handler. Multipart i_ForceOrbisDurangoToUseDLCSlot set to 2 due to g_bUse_MP_DLC_Dialogue being FALSE")
                    cdPrintnl()
                #endif


            ENDIF

        ENDIF






        #if USE_TU_CHANGES

            BOOL b_UseRegularAdditionalText = FALSE         //Default to looking for DLC dialogue in TU
        

            IF g_bInMultiplayer
            
                IF g_bUse_MP_DLC_Dialogue = FALSE

                    b_UseRegularAdditionalText = TRUE       //If we are in MP and the use_MP_DLC_dialogue flag is not set, assume this is regular text.
             
                
                    #if IS_DEBUG_BUILD
            
                        cdPrintnl()
                        cdPrintstring ("FEB 14 - Text Block Check. Multipart MP check forced b_UseRegularAdditionalText to TRUE.")
                        cdPrintnl()                   

                    #endif

                
                ENDIF

            ELSE
            
                IF (NOT IS_CURRENT_MISSION_DLC())

                    b_UseRegularAdditionalText = TRUE       //If we are in SP and are not running a DLC mission, assume this is regular text.

                
                    #if IS_DEBUG_BUILD
            
                        cdPrintnl()
                        cdPrintstring ("FEB 14 - Text Block Check. Multipart SP check forced b_UseRegularAdditionalText to TRUE.")
                        cdPrintnl()

                    #endif
                
                
                ENDIF
        
            ENDIF

    
        #endif





        IF DOES_TEXT_BLOCK_EXIST (g_BlockToLoad)



            //1972746 - CT_AUD block causes the vast majority of problems when used in DLC missions. This is a legacy block so we will force this to look at american.rpf
            //whenever it has been requested as a general rule.
            IF ARE_STRINGS_EQUAL (g_BlockToLoad, "CT_AUD")


                g_ForceDLC_DialogueRequest = TRUE
        
    
                #if IS_DEBUG_BUILD
                    cdPrintstring ("DIALOG_HAND - WARNING! Passed multipart text block is non-dlc use. Forcing load attempt from standard rpf. Ignoring any DLC flags.")
                    cdPrintstring (g_BlockToLoad)
                    cdPrintnl()
                #endif

            ENDIF




            #if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND - July 14 Multipart text block found!")
                cdPrintstring (g_BlockToLoad)
                cdPrintnl()
            #endif
            
        ELSE 

            #if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND - TROUBLE! July 14 Multipart text block MISSING! This will cause a load failure.")
                cdPrintstring (g_BlockToLoad)
                cdPrintnl()
            #endif

        ENDIF



		//Fix for 2641906- Part 2. If problematic Apartments blocks are detected, ignore any rpf pointing flags and default the system to grab the block from the DLC rpf.
		IF IsProblemTextBlock(GET_HASH_KEY(g_BlockToLoad))
		
			b_UseRegularAdditionalText = FALSE
			g_ForceDLC_DialogueRequest = FALSE
			i_ForceOrbisDurangoToUseDLCSlot = 1
			
			#if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND Jan 14 - 2641906- Apartments or DLC text block detected. Grabbing from DLC rpf regardless of any flag states.")
            	cdPrintnl()
            #endif
		ENDIF




        //If a text block does not load, then dialogue handler will remain stuck here...

        IF (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) < (ENUM_TO_INT(CONV_PRIORITY_VERY_LOW)) //This means the text has been passed with an ambient priority level.
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_MP_SPECIAL))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT))
        OR (ENUM_TO_INT(g_CurrentlyPlayingConvPriority)) = (ENUM_TO_INT(CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT_BYPASS_SLEEPMODE))


            #if USE_TU_CHANGES
                IF b_UseRegularAdditionalText = TRUE //Must be an original GTA5 mission or we are in MP and use_MP_DLC flag is not set, use standard dialogue slot.
                OR g_ForceDLC_DialogueRequest = TRUE
                OR i_ForceOrbisDurangoToUseDLCSlot = 2 //We must be in NG if this is greater than zero but the MP_DLC flag has not been set. If in NG default text to look at standard slots.        
             #endif
                    
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND Jan 14 - Cellphone call or conversation text about to load into ambient dialogue text slot")
                        cdPrintnl()
                    #endif


                    REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for multipart additional cellphone call or conversation text to load into ambient dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE


            #if USE_TU_CHANGES
            
                ELSE
              
                    #if IS_DEBUG_BUILD
                        cdPrintstring("DLC dialogue detected - Attempting to load block into DLC ambient slot ")
                        cdPrintnl()
                    #endif

                    REQUEST_ADDITIONAL_TEXT_FOR_DLC (g_BlockToLoad, DLC_AMBIENT_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (DLC_AMBIENT_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for multipart additional cellphone call or conversation text to load into DLC ambient dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE

                ENDIF
                

            #endif
                    


        ELSE

            #if USE_TU_CHANGES
                IF b_UseRegularAdditionalText = TRUE //Must be an original GTA5 mission or we are in MP and use_MP_DLC flag is not set, use standard dialogue slot.
                OR g_ForceDLC_DialogueRequest = TRUE
                OR i_ForceOrbisDurangoToUseDLCSlot = 2 //We must be in NG if this is greater than zero but the MP_DLC flag has not been set. If in NG default text to look at standard slots.        
            #endif

                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND Jan 14 - Cellphone call or conversation text about to load into mission dialogue text slot")
                        cdPrintnl()
                    #endif


                    REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for multipart additional cellphone call or conversation text to load into mission dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE                    


            
            #if USE_TU_CHANGES

                ELSE

                    #if IS_DEBUG_BUILD
                        cdPrintstring("DLC dialogue detected - Attempting to load block into DLC mission slot")
                        cdPrintnl()
                    #endif

                  
                    REQUEST_ADDITIONAL_TEXT_FOR_DLC (g_BlockToLoad, DLC_MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (DLC_MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
                        WAIT(0)
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND Jan 14 - Waiting for multipart additional cellphone call or conversation text to load into DLC mission dialogue text slot")
                            cdPrintnl()
                        #endif
                    ENDWHILE
                
                ENDIF

            #endif
     


        ENDIF












    CREATE_NEW_SCRIPTED_CONVERSATION()

    
   
    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Building multipart conversation from first root: ")
        cdPrintstring(g_ConversationData.MultipartSegmentToGrab[0]) 
        cdPrintnl()
    #endif



    AddPedsDataForConversation ()



ENDPROC










PROC BuildMultipartConversation()


    mpSegmentIndex = 0


    INT textLabelIncrement = 1
    INT textLabelSequentialSuffix = 1
    INT labelIndex = 0

    ConversationBuilt = FALSE
    IsConversationPaused = FALSE

    INT SpeakerNumber, ListenerNumber, VolumeType

    BOOL LineInterruptibleBool, DucksRadioBool, DucksScoreBool, HeadSetBool, DoNotInterruptForSpecAbBool, IsPadSpeakerRoute
    eAUDIBILITY AudibilityEnum




    WHILE mpSegmentIndex < g_TotalMultiparts


            


       
        GetSpeakerListenerLabelForThisMultipart()

        GetMasterInterruptibleAnimTriggeredLabelForThisMultipart()

        GetLineFlagsLabelForThisMultipart()

                                                                                                                               
        INT SLCounter = 0


        WHILE labelIndex < constConversationLabels
        AND ConversationBuilt = FALSE

        
               

            randomAudioTextLabel = g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex]
            randomaudioTextLabel += "A"

       
            g_ConversationLabels[labelIndex] = g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex]

            //For multi-part conversations we would need to change the second parameter for each sequential label in our global conversation data array
            //that we'd construct in the public multi-part function.
            //We'd also need to have a global array of single specific lines.
            //Finally we'd need a wrap in here to tell the proc that it is indeed doing a multi-part conversation, we'd stop it by checking for a null value
            //or, perhaps, safer... the number of lines you want to add.
            


            g_ConversationLabels[labelIndex] += "_"
            g_ConversationLabels[labelIndex] += textLabelSequentialSuffix

        

            audioTextLabel = g_ConversationLabels[labelIndex]
            audioTextLabel += "A"


                                                                                                                               
            IF DOES_TEXT_LABEL_EXIST (audioTextLabel)//(g_ConversationLabels[labelIndex])
                                                                                                         
           
                //audioFilename = GET_STRING_FROM_TEXT_FILE(audioTextLabel)

                audioFilename = GET_FILENAME_FOR_AUDIO_CONVERSATION(audioTextLabel)

     
                //Get corresponding Speaker, Listener number from SL label to include in ADD_LINE_TO_CONVERSATION
                //Speaker Ints are present in the 1st, 4th, 7th, 10th integers in the SL string. As the label index starts at zero we can use 
                //the following formula to correctly extract the Speaker int for each line.  You don't need to add 1, because the GET_NTH function
                //returns zero for the first character.
                //Accordingly, Listener Ints will be, 2nd, 5th, 8th and the Volumetype Int is the 3rd, 6th and 9th 

                   

                SpeakerNumber = GetSpeakerIntFromString (SLCounter)
                //SpeakerNumber = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, (SLCounter * 3))
                
                
                ListenerNumber = GetListenerIntFromString (SLCounter)
                //ListenerNumber = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, ((SLCounter * 3) + 1))
                
                
                VolumeType = GetVolumeTypeIntFromString (SLCounter)
                //VolumeType = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, ((SLCounter * 3) + 2))


                MasterInterruptibleBool = GetMasterInterruptibleIntFromString_and_Convert_to_Bool()

                MasterAnimTriggeredBool = GetMasterAnimTriggeredIntFromString_and_Convert_to_Bool()

                MasterPlaceholderBool = GetMasterPlaceholderIntFromString_and_Convert_to_Bool()



                LineInterruptibleBool = GetLineInterruptibleIntFromString_and_Convert_to_Bool(SLCounter)
            
                DucksRadioBool = GetDucksRadioInt_and_Convert_to_Bool(SLCounter)

                DucksScoreBool = GetDucksScoreInt_and_Convert_to_Bool(SLCounter)
                    
                AudibilityEnum = GetAudibilityEnumFromString (SLCounter)

                HeadSetBool = GetHeadSetInt_and_Convert_to_Bool(SLCounter)

                DoNotInterruptForSpecAbBool = GetDoNotInterruptForSpecAbInt_and_Convert_to_Bool(SLCounter)
                
                IsPadSpeakerRoute = GetIsPadSpeakerRouteInt_and_Convert_to_Bool(SLCounter)



                IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[labelIndex])


          
                        //If we want to play a single line, only add the line which specifically matches the label passed in to g_SpecificLabel

                        IF ARE_STRINGS_EQUAL (g_ConversationLabels[labelIndex], g_ConversationData.MultipartSpecificLabel[mpSegmentIndex])

                       
                            IF SpeakerNumber > 36 //9 
                            OR ListenerNumber > 36 //9
                                
                                #IF IS_DEBUG_BUILD

                                    SCRIPT_ASSERT("Single line play - Speaker or Listener value invalid!")                 
                                
                                #endif
                            
                            ELSE

                                #if IS_DEBUG_BUILD
                                    
                                    cdPrintstring("DIALOG_HAND - Found this label in text files:")
                                    cdPrintstring(g_ConversationLabels[labelIndex])

                                    cdPrintstring("DIALOG_HAND - Adding single line only to conversation with speaker value ")                 
                                    cdPrintint(SpeakerNumber)
                                    cdPrintstring(" and listener value ")                 
                                    cdPrintint(ListenerNumber)
                                    cdPrintstring(" and volume type ")                 
                                    cdPrintint(VolumeType)
                                    cdPrintnl()

                                #endif

                            ENDIF
                            
                            ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, g_ConversationLabels[labelIndex], ListenerNumber, VolumeType, FALSE,
                                                        LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool,
                                                        IsPadSpeakerRoute)

                            //PRINTSTRING("Adding line 5")
                            //PRINTNL()


                        ENDIF

                    


                ELSE  //if the audio label exists but the subtitle label does not then it must be a SFX pause...   NOT allowing script to play single line sfx pauses at this point...

                    
                    //Removing this! Too many SFX pauses can screw up multipart conversations>
                    //ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, audioFilename, ListenerNumber, VolumeType, FALSE,
                    //                            LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool, IsPadSpeakerRoute)



                    //PRINTSTRING("Adding line 6")
                    //PRINTNL()


                ENDIF

                //Increment suffix by label increment
                textLabelSequentialSuffix = textLabelSequentialSuffix + textLabelIncrement


      
            ELSE


                //Check for random conversation here
            
                IF DOES_TEXT_LABEL_EXIST (randomAudioTextLabel)

               
                    //audioFilename = GET_STRING_FROM_TEXT_FILE(randomAudioTextLabel)

                    audioFilename = GET_FILENAME_FOR_AUDIO_CONVERSATION (randomAudioTextLabel)

                    SpeakerNumber = GetSpeakerIntFromString (0)

                    ListenerNumber = GetListenerIntFromString (0)
                            
                    VolumeType = GetVolumeTypeIntFromString (0)


                    MasterInterruptibleBool = GetMasterInterruptibleIntFromString_and_Convert_to_Bool()

                    MasterAnimTriggeredBool = GetMasterAnimTriggeredIntFromString_and_Convert_to_Bool()
                    
                    MasterPlaceholderBool = GetMasterPlaceholderIntFromString_and_Convert_to_Bool()



                    LineInterruptibleBool = GetLineInterruptibleIntFromString_and_Convert_to_Bool(0)
            
                    DucksRadioBool = GetDucksRadioInt_and_Convert_to_Bool(0)

                    DucksScoreBool = GetDucksScoreInt_and_Convert_to_Bool(0)

                    AudibilityEnum = GetAudibilityEnumFromString (0)

                    HeadSetBool = GetHeadSetInt_and_Convert_to_Bool(0)
                
                    DoNotInterruptForSpecAbBool = GetDoNotInterruptForSpecAbInt_and_Convert_to_Bool(0)
                    
                    IsPadSpeakerRoute = GetIsPadSpeakerRouteInt_and_Convert_to_Bool(0)



                    #IF IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND DESIGNER DEBUG - Found audio label for random conversation: ")
                        cdPrintstring(randomAudioTextLabel)
                        cdPrintstring(" with filename: ")
                        cdPrintstring(audioFilename)
                        cdPrintstring(" Root: ")
                        cdPrintstring(g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex])
                        cdPrintstring(" and volume type ")                 
                        cdPrintint(VolumeType)

                        cdPrintnl()
                    #endif
                    

                  
                    ADD_LINE_TO_CONVERSATION (SpeakerNumber, audioFilename, g_ConversationData.MultipartSegmentToGrab[mpSegmentIndex], ListenerNumber, VolumeType, TRUE,
                                                LineInterruptibleBool, DucksRadioBool, DucksScoreBool, AudibilityEnum, HeadSetBool, DoNotInterruptForSpecAbBool,
                                                IsPadSpeakerRoute)

                    //PRINTSTRING("Adding line 7")
                    //PRINTNL()

           
                    g_ConversationLabels[labelIndex] = "END"

                    ConversationBuilt = TRUE



        
                ELSE


                        g_ConversationLabels[labelIndex] = "END"

                        ConversationBuilt = TRUE


                ENDIF
        
            ENDIF

         

            labelIndex ++

            SLCounter ++

                IF labelIndex = constConversationLabels

                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT ("More potential labels in this conversation than allowed for in global conversation label array. See Steve T.")
                    #endif

                ENDIF


        ENDWHILE


       
    mpSegmentIndex ++



        //Reset these two for next time around...
        ConversationBuilt = FALSE
        textLabelSequentialSuffix = 1
        SLCounter = 0

        labelIndex = 0 




    ENDWHILE



    IF g_IsThisConversationForPhone = FALSE // this isn't a phonecall, so go ahead and play the conversation
    
        
        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - Built multipart face to face conversation OK. First root was: ")
            cdPrintstring (g_ConversationData.MultipartSegmentToGrab[0])
            cdPrintnl()


            IF MasterAnimTriggeredBool 

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - This Conversation has been passed as ANIM TRIGGERED!")
                cdPrintnl()

            ELSE
        
                cdPrintnl()
                cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - This Conversation is not anim triggered.")
                cdPrintnl()

            ENDIF

        #endif


        
          

        SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM (MasterAnimTriggeredBool) //Here? #1130859
        
        SET_CONVERSATION_AUDIO_PLACEHOLDER (MasterPlaceholderBool)
        
        START_SCRIPT_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen, g_ConversationData.g_CloneConversation, MasterInterruptibleBool) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.

        
      

        
        g_ConversationStatus = CONV_STATE_PLAYING
    
    ELSE

        #if IS_DEBUG_BUILD
        
            cdPrintnl()
            cdPrintstring ("DIALOG_HAND DESIGNER_DEBUG - Built multipart phonecall OK. First root was: ")
            cdPrintstring (g_ConversationData.MultipartSegmentToGrab[0])
            cdPrintnl()

        #endif

    ENDIF






ENDPROC














PROC BuildCellphoneConversation()  //Uses new audio paradigm that essentially mimics face-to-face conversation apart from the playing segment.


    


 

    //Bug 352962 necessitated this addition, although the issue was dormant for movie fails.

    //If the fail / interrupt flag has been fired for any reason, give flashhand time to fully clean up before resetting the flag.
    WHILE g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE
    AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) > 0

        WAIT(0)

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - Waiting on flashhand to terminate before resetting interrupt_fail flag..")
            cdPrintnl()

        #endif

    ENDWHILE

    //The fail / interrupt flag needs reset to prevent further checks below bombing out immediately. Cellphone_Flashhand resets to FALSE, but there is a small
    //chance a call could come in before flashhand does this.
    g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE




    //If Disable_Hangup has been set for this call, we need to let this script know that it should check for any change of this state later...
    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_UPDATE_NEG_BUTTON_PRESENCE)

    ELSE
        
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_UPDATE_NEG_BUTTON_PRESENCE)

    ENDIF






    g_Attempted_Call_Aborted = FALSE


    IF g_TotalMultiparts = 0

        IF NOT (g_IsThisAnMPChatCall) //We do not need to build a conversation if we are merely going through the system to initiate a chat call between two human players.

            CommonExtractSpeakersListeners()
        
        ENDIF

    ELSE
      
        BuildMultipartConversation()

    ENDIF
    

    #if IS_DEBUG_BUILD

        
        IF g_IsThisAnMPChatCall

            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - This phonecall is an MP Chat call. No conversation built.")
            cdPrintnl()
        
        ELSE

            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - Built cellphone conversation OK.")
            cdPrintnl()

        ENDIF   


    #endif


    

    //New Preload Work - critical!
    g_b_PreloadOngoing = FALSE

    IF g_UpcomingConversationInboundForCellphone

            
        IF g_CellphonePromptResponse = RESPONSE_STORE_EMPTY
            

            IF g_IsThisAnMPChatCall = FALSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - This is not a chat call, preload of conversation underway...")
                    cdPrintnl()

                #endif
             
                i_PreloadTimeout = const_PreloadMaxTime
                    
                PRELOAD_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen) 
       
            ELSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - NO Preload applicable. This is a human to human chat call.")
                    cdPrintnl()

                #endif

                i_PreloadTimeout = 0 //Deliberate - this means the timeout will fire immediately for the chat call.


            ENDIF


        ELSE
        
            
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - NO Preload available. This is a response or dynamic call branch")
                cdPrintnl()

            #endif

            i_PreloadTimeout = 0 //Deliberate - this means the timeout will fire immediately for a branch forcing it to play immediately. These can't be preloaded.

        ENDIF


        g_b_PreloadOngoing = TRUE
        SETTIMERB(0)

  
               
        WHILE g_b_PreloadOngoing = TRUE
        AND g_ConversationStatus = CONV_STATE_BUILDING //Need to make sure a kill, which sets CONV_STATE_FINISHED will break out of this also. NEEDS CHECKED WHEN PRELOAD WORKS!
        AND TIMERB() < i_PreloadTimeout

            WAIT (0)
        
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND - Preload 5 ongoing.")
                        cdPrintnl()
                    #endif




            
                    IF NOT IS_PLAYER_PLAYING (PLAYER_ID())  //New check for GET_IS_PRELOADED_CONVERSATION_READY() work. 1
                    
                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring ("DIALOG_HAND - Player died during preload of incoming call...")
                            cdPrintnl()

                        #endif

                        g_b_PreloadOngoing = FALSE
                        
                        STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...

                        g_ConversationStatus = CONV_STATE_FINISHED

                    ENDIF




            //IF Preload has finished via check to new native command then break out of this clause.
            //via g_b_PreloadOngoing = FALSE

            IF IS_MOBILE_PHONE_CALL_ONGOING()
                IF GET_IS_PRELOADED_CONVERSATION_READY() //Script is checking that a preloaded conversation is ready when no conversation is preloading.
                

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Success! Preload of incoming call has returned ready...")
                        cdPrintnl()

                    #endif



                    g_b_PreloadOngoing = FALSE  


                ELSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Waiting on Preload of incoming call to return READY...")
                        cdPrintnl()

                    #endif
                        
                ENDIF
            ENDIF

        ENDWHILE


        IF TIMERB() > i_PreloadTimeout

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - Preload Breakout - Preload Timeout elapsed!")
                cdPrintnl()

            #endif
            
        ENDIF



        IF g_ConversationStatus <> CONV_STATE_BUILDING

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - Preload Breakout - Conversation State was no longer BUILDING!")
                cdPrintnl()

            #endif
            
        ENDIF



        IF g_b_PreloadOngoing = FALSE   //Will either be set by the Preload finishing or the phone being taken out during the preload and starting the conversation immediately for safety's sake.
            
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - Preload Breakout - PreloadOngoing bool was set to FALSE. Preload ready or may have been the result of cellphone controller take out. Check for warning.")
                cdPrintnl()

            #endif

        ENDIF

    ENDIF


    g_b_PreloadOngoing = FALSE //Failsafe.




    



    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("DIALOG_HAND - Preload interval concluded.")
        cdPrintnl()

    #endif



    //End of Preload Work main block.



    IF g_Cellphone.PhoneDS = PDS_DISABLED
    OR g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY
    //OR g_Cellphone.PhoneDS > PDS_MAXIMUM   //Added for Preload Work so we abort the attempted call if the phone starts running an app during the preload interval.
    OR g_ConversationStatus <> CONV_STATE_BUILDING //Phonecall might have been killed during preload...
    OR g_Phone_Blocked_While_Moving_Down = TRUE
        
        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_HAND - Trying to ring or use phone for call, but phone is disabled, moving downscreen or began running app during preload. Stopping conversation.")
            cdPrintnl()

        #endif


        STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...


        g_ConversationStatus = CONV_STATE_FINISHED


    ELSE

        


        //Wait for preload to finish here for incoming calls? Outgoing calls can check preload has finished during pause for answering time.



        //Perhaps move this to just after new phonecall returns true in case anyone checks immediately next frame.
        IF g_IsThisAnMPJobOffer
        
            #if IS_DEBUG_BUILD
                cdPrintstring("DIALOG_HAND - Resetting job offer response store and move down bool.")
                cdPrintnl()
            #endif

            g_CellphoneJobOfferResponse = RESPONSE_STORE_EMPTY

            b_NeedtoMovePhoneDownForAcceptedJob = FALSE


        ENDIF




        IF g_UpcomingConversationInboundForCellphone = FALSE   //Player must have dialled... go ahead and play the call ( need timer for believable answering and check for actual call data )


            //Only do this once so that branching phone calls only keep the initial state..
            IF g_HasStateBeenPreserved = FALSE

                BeforeCallPhoneDS = g_Cellphone.PhoneDS //keep a copy of the previous phone state
                g_HasStateBeenPreserved = TRUE

            ENDIF



            b_ScaleformCallButtonsSetUp = FALSE  //Critical - lets PlayCellphoneConversation or PlayCellphoneJobOfferConversation set up scaleform screens.
            
            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - Setting call screen set up to FALSE from BuildCellphoneConversation")
                cdPrintnl()

            #endif




            IF g_CellphonePromptResponse = RESPONSE_STORE_EMPTY  //Dynamic Work... Need to prevent answering time pause for dynamic phonecall branch.
         
                
                PRELOAD_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen)

                WAIT(0)                
                
                g_ConversationStatus = CONV_STATE_PAUSE_ANSWERING_TIME


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - Conversation status set to PAUSE_ANSWERING_TIME. Preloading conversation for outgoing call.")
                    cdPrintnl()
                
                #endif
                
                
                g_b_PreloadOngoing = TRUE  //Initiates check in procedure PauseForContactAnsweringTime()
                SETTIMERB(0) //Set up safety timeout for PauseForContactAnsweringTime()
                
                //This kicks off the remote ring tone in Cellphone Flashhand
                SET_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)
                


                SETTIMERA(0)

            ELSE

                //If the response store isn't empty it means that this is a branch of a conversation, so it's safe to go ahead and set the PLAY state
                //as no pause for other character answering time is necessary.

                g_b_PreloadOngoing = FALSE

                g_ConversationStatus = CONV_STATE_PLAYING
            
            ENDIF          









            g_InboundCallWaitAccRej = CALL_NONE_WAITING






            
            IF g_ShouldPhoneBeForcedOnScreen = TRUE //We may need to force the phone onto screen for cutscenes etc.
                


                IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND - Cellphone was in ATTEMPTING TO CALL CONTACT state during call process, but call has been passed with ForceOnScreen. Phone takeout likely duplicated.")
                        cdPrintnl()

                    #endif 

                    //Perhaps just put an ongoing call assignment here and update vse, rather than go through PDS_TAKEOUT to prevent multiple phone takeout anims.
                    //The benefit of leaving is that it's easy to find incorrect force answers from other scripts. Re-appraise after Dec. 2011 milestone.


    
                ENDIF



                IF NOT (g_Cellphone.PhoneDS = PDS_ONGOING_CALL) //Prevent "with replies" call taking out phone twice...

                    IF g_Cellphone.PhoneDS > PDS_TAKEOUT //ShouldBeForcedOnscreen naughtily used but phone already onscreen.

                            //# 2027104
                            //This mimics the Important section! of cellphone controller, line 3705ish but makes sure not to go through the takeout routine again
                            //which has the capacity to stop the cellphone moving up the hud for a call in first person. 
                            //If people could be trusted to use g_ShouldPhoneBeForcedOnScreen correctly, this wouldn't be needed. But they can't. So it is.      
                        
                            //Critical fix for tricky first person issue where bad use of ShouldBeForced can screw up the phone position when it shows up on the hud.   
                            IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE

                                //Critical!
                                g_Cellphone.PhoneDS = PDS_ONGOING_CALL  //This will let buildcellphone within dialogue handler continue, as its "looped while" pause can now pass through.
                                            
                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintstring("STATE ASSIGNMENT 736. Dialogue Handler assigns failsafe PDS_ONGOING_CALL")
                                    cdPrintnl()   
                            

                             
                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - Dialogue Handler has set PDS to failsafe ONGOING_CALL state.")
                    
                                #endif 


                                Update_Scaleform_VSE ()

                            ENDIF



                    ELSE

                        g_Cellphone.PhoneDS = PDS_TAKEOUT //Forces cellphone into hand onscreen from cellphone_controller.sc
                                                      //tell cellphone_controller.sc to take the phone out and launch cellphone_flashhand.sc along with scaleform movies.


                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("STATE ASSIGNMENT 60. Dialogue_Handler assigns PDS_TAKEOUT new")
                            cdPrintnl()   
                        #endif



                        //Now moved after the flashhand check. Occasionally the phone was not getting put to the player's ear. The wait 0 seems to help this.
                        //Put_Cellphone_To_Ear()


                   ENDIF


                ELSE


                    Update_Scaleform_VSE() //We need to refresh the callscreen here so that a player_call_char_with_replies has its question label removed when the yes/no branch plays.
               

                ENDIF




           
            ELSE  //Phone assumed as already onscreen

                //Need an artificial delay here for other char's answering time...?


                WHILE g_Cellphone.PhoneDS = PDS_TAKEOUT  //even though PDS was not AWAY, there would be a conceivable but tiny chance that it was in TAKEOUT, so we wait
                    WAIT (0)                             //until this is resolved from cellphone controller when it sets it to MAXIMUM.  
                    
                    
                    IF g_b_PreloadOngoing = TRUE
                            
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("DIALOG_HAND - Preload 2 ongoing.")
                            cdPrintnl()
                        #endif

                        IF IS_MOBILE_PHONE_CALL_ONGOING()
                            IF GET_IS_PRELOADED_CONVERSATION_READY() //Script is checking that a preloaded conversation is ready when no conversation is preloading.

                                #if IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring ("DIALOG_HAND - WHILE g_Cellphone.PhoneDS = PDS_TAKEOUT reporting Preload has returned READY...")
                                    cdPrintnl()

                                #endif

                                g_b_PreloadOngoing = FALSE

                            ENDIF
                        ENDIF
    
                    ENDIF
             
                    
                                
                ENDWHILE

                
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND - Cellphone was in PDS_MAXIMUM state during outgoing call process, dialogue handler altering to PDS_ONGOING_CALL.")
                        cdPrintnl()

                    #endif 

                ENDIF


                
                IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND - Cellphone was in ATTEMPTING TO CALL CONTACT state during call process, dialogue handler altering to PDS_ONGOING_CALL.")
                        cdPrintnl()

                    #endif 
    
                ENDIF




                IF g_Cellphone.PhoneDS = PDS_AWAY

                    STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...

                    g_ConversationStatus = CONV_STATE_FINISHED

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Call was not passed with force onscreen and phone has been put away or was not originally present. Aborting call.")
                        cdPrintnl()

                    #endif 

                    g_Attempted_Call_Aborted = TRUE

                    EXIT

                ENDIF





            
                g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 61. Dialogue_Handler assigns PDS_ONGOING_CALL")
                    cdPrintnl()   
                #endif

              

    

                
                Update_Scaleform_VSE()  //Will fire off an SF error here, if the phone is not onscreen at all. Check if the phone is onscreen before updating VSE?
                //Might need to play anims here for taking phone out within onscreen check ... as this would normally be done by PDS_TAKEOUT but we're bypassing that

                //e.g IF PHONE_NOT_ONSCREEN?
                //Play takeout of pocket anim ( put up to ear is done later )
                
                //IF PHONE ONSCREEN 
                //update VSE as this may be a response to IS_CALLING_CONTACT from the flow.


            ENDIF
    



   
            //Delay Fix here
            //See Bug 139248
            
            IF NETWORK_IS_GAME_IN_PROGRESS()

                Net_Failsafe_StartTime = GET_NETWORK_TIME()

            ELSE

                Failsafe_StartTime = GET_GAME_TIMER()

            ENDIF


            WHILE g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //if the phone was not already present onscreen, we know that the movies have been checked from cellphone_controller.
            AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
          
            
        

                WAIT(0)
        
                    
                //Think about having a check for PDS < TAKEOUT here for bug 295259. We would set the autoplay to fail instantly in that case to pass through the loop.
                //The phone should at least be in takeout state unless something really weird has happened with timings.
        
        

                IF g_b_PreloadOngoing = TRUE
                        

                    #if IS_DEBUG_BUILD
                        cdPrintstring ("DIALOG_HAND - Preload 1 ongoing.")
                        cdPrintnl()
                    #endif

                    IF IS_MOBILE_PHONE_CALL_ONGOING()
                        IF GET_IS_PRELOADED_CONVERSATION_READY()

                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - <> PDS_ONGOING_CALL wait reporting Preload has returned READY...")
                                cdPrintnl()

                            #endif

                            g_b_PreloadOngoing = FALSE

                        ENDIF
                    ENDIF
                    
    
                ENDIF



        
                
                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Dialogue Handler is waiting on PDS to be set to ONGOING_CALL for outgoing call.")
                    cdPrintnl()

                #endif 

                IF NETWORK_IS_GAME_IN_PROGRESS()
                
                    i_TimeDifference = GET_TIME_DIFFERENCE (GET_NETWORK_TIME(), Net_Failsafe_StartTime)
                
                ELSE

                    Failsafe_CurrentTime = GET_GAME_TIMER()
                    i_TimeDifference = Failsafe_CurrentTime - Failsafe_StartTime
                
                ENDIF


                    
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0  //Trial addition for 246816

                    IF i_TimeDifference > 15000 // 15 seconds timeout

                        g_LastInboundCallRejected = TRUE

                        g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE 

             
                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("DIALOG_HAND - Dialogue Handler timed out during ONGOING_CALL wait for outgoing call. Queued conversation terminated.")
                            cdPrintnl()

                        #endif

                    ENDIF

                ENDIF

            
            ENDWHILE
            


            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR) //Put the cellphone to the player's ear if not already there.
            
                IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE //Added for 2038700, this delays player_call_char_cellphone call from putting the phone to ear
                                                                       //It will go to ear on foot when the cellphone conversation starts from PlayCellphoneConversation()
                    Put_Cellphone_To_Ear()
            
                ENDIF

            ENDIF


         
            IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE  //don't play conversation if ongoing_call check has timed out due to scaleform.
            
                #if IS_DEBUG_BUILD

                    IF g_TotalMultiParts < 2

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Built mobile phone call: ")
                        cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
                        cdPrintnl()

                    ELSE

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Built multipart mobile phone call with first root: ")
                        cdPrintstring(g_ConversationData.MultipartSegmentToGrab[0])
                        cdPrintnl()

                    ENDIF
            
                #endif 
        



                //If the response store isn't empty it means that this is a branch of a conversation, so it's safe to go ahead and play
                //as no pause for other character answering time is necessary.
                IF NOT (g_CellphonePromptResponse = RESPONSE_STORE_EMPTY)

                    g_PrepareCallForDynamicBranch = FALSE


                    #if IS_DEBUG_BUILD
            
                        cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 02")
                        cdPrintnl()

                    #endif


                
                    Set_SF_Buttons_SuppText_For_ConnectedCall()
                    TitleUpdate_Add_Hide_Phone_Soft_Key()

                    IF NOT (g_IsThisAnMPChatCall)

                       IF g_CellphonePromptResponse = RESPONSE_STORE_EMPTY

                            
                            #if IS_DEBUG_BUILD

                                cdPrintstring ("DIALOG_HAND - Using START_PRELOADED_CONVERSATION 1 as response store was empty.")
                                cdPrintnl()

                            #endif


                            START_PRELOADED_CONVERSATION()

                        ELSE


                            #if IS_DEBUG_BUILD

                                cdPrintstring ("DIALOG_HAND - Using START_SCRIPT_PHONE_CONVERSATION 1 as this was a response or dynamic branch.")
                                cdPrintnl()

                            #endif


                            START_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.
                    
                        ENDIF
                        

                    ELSE


                        #if IS_DEBUG_BUILD

                            cdPrintstring ("DIALOG_HAND - Not starting SCRIPT_PHONE_CONVERSATION 1 as g_IsThisAnMPChatCall is returning TRUE")
                            cdPrintnl()

                        #endif
                    

                    ENDIF

                    Setup_HidePhoneTrial()
                    
                         
                ENDIF    



                      

            ELSE  


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Ongoing call wait for outgoing call timed out, so hanging up phone.")
            
                #endif 

                    
                KILL_ANY_CONVERSATION() //Trial replacement for 246816. Safer to just clear conversation rather than interfere with phone.
                g_Attempted_Call_Aborted = TRUE

                //HANG_UP_AND_PUT_AWAY_PHONE()


            ENDIF


            //perhaps put return true here...



        ELSE


            g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED // Tell cellphone_flashhand.sc that a call is waiting on a flash response


            //Let cellphone controller know that we want to go through interference if player is in a vehicle before phone rings.
            
            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                    IF g_Cellphone.PhoneDS = PDS_AWAY
                    
                        //PLAY_AUDIO_EVENT("MOBILE_PRERING")  //Too risky now we're preloading.
            
                        //SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)  //Too risky now we're preloading.
                    
                    ENDIF

                ELSE
                
                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)
                    
                ENDIF
            ENDIF 
            
                     


            IF g_HasStateBeenPreserved = FALSE

                BeforeCallPhoneDS = g_Cellphone.PhoneDS //keep a copy of the previous phone state
                g_HasStateBeenPreserved = TRUE

            ENDIF


            IF g_Cellphone.PhoneDS = PDS_AWAY

                g_Cellphone.PhoneDS = PDS_TAKEOUT  //As a call is inbound we need to tell cellphone_controller.sc to take the phone out and launch cellphone_flashhand.sc
            
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 62. Dialogue_Handler assigns PDS_TAKEOUT")
                    cdPrintnl()   
                #endif

                

            ELSE

    
                WHILE g_Cellphone.PhoneDS = PDS_TAKEOUT  //even though PDS was not AWAY, there would be a conceivable but tiny chance that it was in TAKEOUT, so we wait
                    WAIT (0)                             //until this is resolved from cellphone controller when it sets it to MAXIMUM.              
                ENDWHILE

                
                IF g_Cellphone.PhoneDS = PDS_MAXIMUM

                    #IF IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("DIALOG_HAND - Cellphone was in PDS_MAXIMUM state during incoming call process, dialogue handler altering to PDS_ONGOING_CALL.")
        
                    #endif 

                ENDIF                     
            
                g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 64. Dialogue_Handler assigns PDS_ONGOING_CALL")
                    cdPrintnl()   
                #endif



                Update_Scaleform_VSE()
            
            ENDIF
         

            
       

                    
            

            //Delay Fix here
            //See Bug 139248
            IF NETWORK_IS_GAME_IN_PROGRESS()

                Net_Failsafe_StartTime = GET_NETWORK_TIME()

            ELSE
                
                Failsafe_StartTime = GET_GAME_TIMER()
            
            ENDIF






            WHILE g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //if the phone was not already present onscreen, we know that the movies have been checked from cellphone_controller.
            AND g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE
           
                
                WAIT (0)

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Dialogue Handler is waiting on PDS to be set to ONGOING_CALL for inbound call.")
                    cdPrintnl()

                #endif 

                IF NETWORK_IS_GAME_IN_PROGRESS()

                    i_TimeDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_Failsafe_StartTime)
                
                ELSE
                
                    Failsafe_CurrentTime = GET_GAME_TIMER()
                    i_TimeDifference = Failsafe_CurrentTime - Failsafe_StartTime

                ENDIF



                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 //Trial addition for 246816

                    IF i_TimeDifference > 20000 // 20 seconds timeout
  
                        g_LastInboundCallRejected = TRUE

                        g_B_External_Interrupt_Autoplay_MovieFail_Flag = TRUE

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("DIALOG_HAND - Dialogue Handler timed out during ONGOING_CALL wait for inbound call. Queued conversation terminated.")
                            cdPrintnl()
                            cdPrintstring("DIALOG_HAND - Timeout reason - Cellphone_flashhand failed to launch.")
                            cdPrintnl()

                        #endif

                        //Failsafe.
                        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PLAY_INTERFERENCE_AUDIO)


                    ENDIF

                ENDIF

               

            ENDWHILE


       
              




            IF g_B_External_Interrupt_Autoplay_MovieFail_Flag = FALSE //don't play conversation if ongoing_call check has timed out due to scaleform.

                g_ConversationStatus = CONV_STATE_WAITING_ON_PLAYER_ACCREJ  //Tell the main enum switcher that we need to keep running the accept / reject procedure below
                                                                            //until the player picks up or rejects the call.
            
            ELSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Ongoing call wait for inbound call timed out, so hanging up phone.")
            
                #endif 

              
                KILL_ANY_CONVERSATION() //Trial replacement for 246816. Safer to just clear conversation rather than interfere with phone.
                g_Attempted_Call_Aborted = TRUE

                //HANG_UP_AND_PUT_AWAY_PHONE()
                

            ENDIF

  

            //For bug 123203, makes sure to use network timer for MP.
 
            IF NETWORK_IS_GAME_IN_PROGRESS() 

                Net_ForceAnswer_StartTime = GET_NETWORK_TIME()

                Net_IncomingAutoHangUp_StartTime = GET_NETWORK_TIME()


            ELSE
            
                ForceAnswer_StartTime = GET_GAME_TIMER()

                IncomingAutoHangUp_StartTime = GET_GAME_TIMER()

            ENDIF







            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintstring("DIALOG_HAND - Resetting ForceAnswer_StartTime and AutoHangUpStartTime.")
                cdPrintnl()

                cdPrintstring ("DIALOG_HAND -  SP ForceAnswer_StartTime is ")
                cdPrintint (ForceAnswer_StartTime)
                cdPrintnl()           


                cdPrintstring ("DIALOG_HAND -  SP IncomingAutoHangUp_StartTime is ")
                cdPrintint (IncomingAutoHangUp_StartTime)
                cdPrintnl()
                cdPrintnl()


            #endif 


            //Need to check for multiple iterations of the same script loading, other apps that may be running, exiting. Best to put in another display state (CALL_INBOUND) and wrap other scripts
            //navigation / selection essentially pausing with that...

        ENDIF


    ENDIF







ENDPROC














//This used the previous audio code system of playing mobile phone calls that meant only only two speakers could be involved.
//Kept for reference.

//  PROC Legacy_BuildCellphoneConversation()  //Just does phone calls at this point...    CONV_STATE_BUILDING
//
//      
//
//
//
//      INT textLabelIncrement = 1
//      INT textLabelSequentialSuffix = 1
//      INT labelIndex = 0
//      
//      ConversationBuilt = FALSE
//      IsConversationPaused = FALSE
//
//      INT SpeakerNumber //ListenerInt
//   
//
//     
//      REQUEST_ADDITIONAL_TEXT (g_BlockToLoad, MISSION_DIALOGUE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
//      
//      WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (MISSION_DIALOGUE_TEXT_SLOT)  //the permanent global from the temporary holder.
//          WAIT(0)
//      
//          #IF IS_DEBUG_BUILD
//              cdPrintstring ("Waiting for additional cellphone call dialogue text to load...")
//              cdPrintnl()
//          #endif
//
//      ENDWHILE
//
//     
//
//
//     
//
//
//      CREATE_NEW_MOBILE_PHONE_CALL ()
//
//
//      GetSpeakerListenerLabel ()
//
//
//
//      //AddPedsDataForConversation ()  This will need modified to work with phonecalls!
//
//      FirstSpeakerNumber = GetSpeakerIntFromString (labelIndex)
//
//      SecondSpeakerNumber = GetListenerIntFromString (labelIndex)
//
//
//
//
//
//      WHILE labelIndex < constConversationLabels
//      AND ConversationBuilt = FALSE
//
//          //DISPLAY_TEXT (0.41, 0.63 , g_ConversationLabels[labelIndex])
//
//
//          g_ConversationLabels[labelIndex] = g_ConversationData.ConversationSegmentToGrab
//          g_ConversationLabels[labelIndex] += "_"
//          g_ConversationLabels[labelIndex] += textLabelSequentialSuffix
//
//
//
//          audioTextLabel = g_ConversationLabels[labelIndex]
//          audioTextLabel += "A"
//
//
//
//          IF DOES_TEXT_LABEL_EXIST (audioTextLabel)//(g_ConversationLabels[labelIndex])
//
//              audioFilename = GET_STRING_FROM_TEXT_FILE(audioTextLabel)
//
//              //Need to use GET_NTH_INTEGER_IN_STRING for speaker listener stuff...
//
//              //Get corresponding Speaker number from SL label to include in ADD_LINE_TO_MOBILE_PHONE_CALL
//              //Speaker Ints are present in the 1st, 4th, 7th, 10th integers in the SL string. As the label index starts at zero we can use 
//              //the following formula to correctly extract the Speaker int for each line.  You don't need to add 1, because the GET_NTH function
//              //returns zero for the first character.
//
//                 
//
//              
//              //Better if this worked the same way as normal face to face conversations.
//
//              
//              SpeakerNumber = GetSpeakerIntFromString (labelIndex)
//
//              //SpeakerNumber = GET_NTH_INTEGER_IN_STRING(SpeakListenLabel, (labelIndex * 3))
//
//
//
//              IF SpeakerNumber > 36//9 
//                  
//                  #IF IS_DEBUG_BUILD
//
//                      SCRIPT_ASSERT("Speaker value invalid!")                 
//                  
//                  #endif
//              
//              ELSE
//
//                  #IF IS_DEBUG_BUILD
//
//                      cdPrintstring("Adding line to mobile phone call with speaker value ")                 
//                      cdPrintint(SpeakerNumber)
//                      cdPrintnl()
//
//                  #endif
//
//              ENDIF
//
//
//                                                                             
//              IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[labelIndex])
//
//                                                                     
//                  ADD_LINE_TO_MOBILE_PHONE_CALL (SpeakerNumber, audioFilename, g_ConversationLabels[labelIndex])
//
//              ELSE  //if the audio label exists but the subtitle label does not then it must be a SFX pause...
//
//                 ADD_LINE_TO_MOBILE_PHONE_CALL (SpeakerNumber, audioFilename, "STRING")
//
//              ENDIF
//              
//
//              /*
//              cdPrintstring("Voice at 0 is ")
//              cdPrintstring(g_ConversationPedsStruct.PedInfo[0].VoiceID)
//              cdPrintint (NATIVE_TO_INT (g_ConversationPedsStruct.PedInfo[0].Index))
//              cdPrintnl()
//              
//
//              cdPrintstring("Voice at 2 is ")
//              cdPrintstring(g_ConversationPedsStruct.PedInfo[2].VoiceID)
//              cdPrintint (NATIVE_TO_INT (g_ConversationPedsStruct.PedInfo[2].Index))
//              cdPrintnl()
//
//                                                                  
//              cdPrintstring("Voice at 1 is ")
//              cdPrintstring(g_ConversationPedsStruct.PedInfo[1].VoiceID)
//              cdPrintint (NATIVE_TO_INT (g_ConversationPedsStruct.PedInfo[1].Index))
//              cdPrintnl()
//              */
//
//
//       
//
//
//              //Increment suffix by label increment
//              textLabelSequentialSuffix = textLabelSequentialSuffix + textLabelIncrement
//
//          
//
//
//          ELSE
//
//              g_ConversationLabels[labelIndex] = "END"
//
//              ConversationBuilt = TRUE
//      
//          ENDIF
//
//          labelIndex ++
//
//              IF labelIndex = constConversationLabels
//
//                  #IF IS_DEBUG_BUILD
//                      SCRIPT_ASSERT("More potential labels in this conversation than allowed for in global conversation label array. See Steve T.")
//                  #endif
//
//              ENDIF
//
//
//      ENDWHILE
//
//
//     
//
//      #IF IS_DEBUG_BUILD
//
//          cdPrintnl()
//          cdPrintstring ("Built cellphone conversation OK. Now playing...")
//          cdPrintnl()
//
//      #endif
//
//
//      
//
//      IF g_Cellphone.PhoneDS = PDS_DISABLED
//          
//          #IF IS_DEBUG_BUILD
//
//              cdPrintstring ("Trying to ring or use phone, but phone is disabled")
//              cdPrintnl()
//
//          #endif
//
//          g_ConversationStatus = CONV_STATE_FINISHED
//
//
//      ELSE
//
//
//
//          IF g_IsThisAnMPJobOffer
//          
//              #IF IS_DEBUG_BUILD
//                  cdPrintstring("Resetting job offer response store.")
//                  cdPrintnl()
//              #endif
//
//              g_CellphoneJobOfferResponse = RESPONSE_STORE_EMPTY
//
//          ENDIF
//
//
//
//
//          IF g_UpcomingConversationInboundForCellphone = FALSE   //Player must have dialled... go ahead and play the call ( need timer for believable answering and check for actual call data )
//
//
//              //Only do this once so that branching phone calls only keep the initial state..
//              IF g_HasStateBeenPreserved = FALSE
//
//                  BeforeCallPhoneDS = g_Cellphone.PhoneDS //keep a copy of the previous phone state
//                  g_HasStateBeenPreserved = TRUE
//
//              ENDIF
//
//
//              g_ConversationStatus = CONV_STATE_PLAYING //this should be done before setting the phone to the ear / playing the call so that any brief delay
//                                                        //doesn't allow another call to interrupt.
//
//
//              g_InboundCallWaitAccRej = CALL_NONE_WAITING
//
//
//
//
//              
//              IF g_ShouldPhoneBeForcedOnScreen = TRUE //We may need to force the phone onto screen for cutscenes etc.
//                  
//                  g_Cellphone.PhoneDS = PDS_TAKEOUT //Forces cellphone into hand onscreen from cellphone_controller.sc
//                                                    //tell cellphone_controller.sc to take the phone out and launch cellphone_flashhand.sc along with scaleform movies.
//             
//                  Put_Cellphone_To_Ear()
//             
//             
//              ELSE
//
//                  //Need an artificial delay here for other char's answering time...
//                  
//                
//
//                  g_Cellphone.PhoneDS = PDS_ONGOING_CALL
//
//
//
//                  
//                  Update_Scaleform_VSE()  //Will fire off an SF error here, if the phone is not onscreen at all. Check if the phone is onscreen before updating VSE?
//                  //Might need to play anims here for taking phone out within onscreen check ... as this would normally be done by PDS_TAKEOUT but we're bypassing that
//
//                  //e.g IF PHONE_NOT_ONSCREEN?
//                  //Play takeout of pocket anim ( put up to ear is done later )
//                  
//                  //IF PHONE ONSCREEN 
//                  //update VSE as this may be a response to IS_CALLING_CONTACT from the flow.
//
//
//              ENDIF
//      
//
//
//
//     
//
//
//              //Need to put pause here for outbound phone calls - if not handled by pickup routines of other scripts.
//
//
//              //IMPORTANT! We need this to work differently for multiple speakers!
//
//              //This will only handle zero and one as speakers at present!
//              
//              //g_ConversationPedsStruct.PedInfo[SpeakerNumber].Index
//              //e.g START_MOBILE_PHONE_CALL (PLAYER_PED_ID(), "HARRY", dummyConvPed1, "VAUGHN", g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen)
//
//              //Maybe check for contact involved in call and get the voice name from that? New command needed for conference call!
//
//
//
//              #IF IS_DEBUG_BUILD
//
//                  cdPrintnl()
//                  cdPrintstring("Starting mobile phone call with first speaker int of ")
//                  cdPrintint(FirstSpeakerNumber)
//                  cdPrintstring(" and second speaker number ")
//                  cdPrintint(SecondSpeakerNumber)
//                  cdPrintnl()
//
//              #endif 
//      
//
//              START_MOBILE_PHONE_CALL (g_ConversationPedsStruct.PedInfo[FirstSpeakerNumber].Index, g_ConversationPedsStruct.PedInfo[FirstSpeakerNumber].VoiceID, g_ConversationPedsStruct.PedInfo[SecondSpeakerNumber].Index, g_ConversationPedsStruct.PedInfo[SecondSpeakerNumber].VoiceID, g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen)
//
//    
//              //START_SCRIPT_PHONE_CONVERSATION(TRUE, TRUE)
//
//
//
//              //Tell Andrew's finance scripts that a phone conversation has been triggered
//              INFORM_BAWSAQ_OF_EVENT (BS_EVENT_PHONECALL_TRIGGERED)
//
//
//
//              //perhaps put return true here...
//
//
//
//          ELSE
//
//
//              g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED // Tell cellphone_flashhand.sc that a call is waiting on a flash response
//
//
//
//              IF g_HasStateBeenPreserved = FALSE
//
//                  BeforeCallPhoneDS = g_Cellphone.PhoneDS //keep a copy of the previous phone state
//                  g_HasStateBeenPreserved = TRUE
//
//              ENDIF
//
//
//              IF g_Cellphone.PhoneDS =  PDS_AWAY
//
//                  g_Cellphone.PhoneDS = PDS_TAKEOUT  //As a call is inbound we need to tell cellphone_controller.sc to take the phone out and launch cellphone_flashhand.sc
//              
//              ELSE
//              
//                  g_Cellphone.PhoneDS = PDS_ONGOING_CALL
//
//
//                  Update_Scaleform_VSE()
//              
//              ENDIF
//           
//
//              
//         
//
//
//              //WHILE g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED //Replace this section by injecting another interim state CONV_WAITING_ON_PLAYER_ACCREJ  
//                                                                            //that then loops a proc which checks for acceptance then triggers CONV_STATE_PLAYING
//
//              //WAIT(0)                                                     //Now done. Left in comments for reference.
//              
//              
//              g_ConversationStatus = CONV_STATE_WAITING_ON_PLAYER_ACCREJ  //Tell the main enum switcher that we need to keep running the accept / reject procedure below
//                                                                          //until the player picks up or rejects the call.
//
//
//    
//
//              //For bug 123203, it looks as if these aren't getting reset?
//   
//              IF NETWORK_IS_GAME_IN_PROGRESS() 
//
//                  GET_NETWORK_TIMER (ForceAnswer_StartTime)
//
//                  GET_NETWORK_TIMER (IncomingAutoHangUp_StartTime)
//
//
//              ELSE
//              
//                  ForceAnswer_StartTime = GET_GAME_TIMER()
//
//                  IncomingAutoHangUp_StartTime = GET_GAME_TIMER()
//
//              ENDIF
//
//
//
//
//
//
//
//              #IF IS_DEBUG_BUILD
//
//                  cdPrintnl()
//                  cdPrintstring("Resetting ForceAnswer_StartTime and AutoHangUpStartTime.")
//                  cdPrintnl()
//
//              #endif 
//
//
//
//
//
//              //ENDWHILE
//              
//              
//
//
//              //Need to check for multiple iterations of the same script loading, other apps that may be running, exiting. Best to put in another display state (CALL_INBOUND) and wrap other scripts
//              //navigation / selection essentially pausing with that...
//
//          ENDIF
//
//
//      ENDIF
//
//
//
//
//
//
//
//  ENDPROC
//















PROC PauseForContactAnsweringTime()

      
       IF g_b_PreloadOngoing = TRUE  //Highly likely that the preload has finished before this point.
                    
            #if IS_DEBUG_BUILD
                cdPrintstring ("DIALOG_HAND - Preload 3 ongoing.")
                cdPrintnl()
            #endif

            IF IS_MOBILE_PHONE_CALL_ONGOING()
                IF GET_IS_PRELOADED_CONVERSATION_READY() 

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - PauseForContactAnsweringTime reporting Preload has returned READY...")
                        cdPrintnl()

                    #endif

                    g_b_PreloadOngoing = FALSE

                ELSE

                     #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - PauseForContactAnsweringTime waiting on Preload to return READY...")
                        cdPrintnl()

                    #endif

                ENDIF
            ENDIF
            

                     IF NOT IS_PLAYER_PLAYING (PLAYER_ID())  //New check for GET_IS_PRELOADED_CONVERSATION_READY() work. 2
                    
                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring ("DIALOG_HAND - Player died during preload of outgoing call...")
                            cdPrintnl()

                        #endif

                        g_b_PreloadOngoing = FALSE
                        
                        STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...

                        g_ConversationStatus = CONV_STATE_FINISHED

                    ENDIF





        ENDIF


        IF TIMERB() > i_PreloadRemoteRingTimeout
                

            IF g_b_PreloadOngoing = TRUE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - PauseForContactAnsweringTime Preload timeout fired!")
                    cdPrintnl()

                #endif


                g_b_PreloadOngoing = FALSE

            ENDIF

        ENDIF 
        


        IF TIMERA() < g_Outgoing_Call_Pause_Answering_Time
        OR g_b_PreLoadOngoing = TRUE
         

            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Call hung up but phone not necessarily put away so we call normal cleanup...
                            
                
                IF IS_PLAYER_PLAYING (PLAYER_ID())
                    IF IS_PLAYER_CONTROL_ON (PLAYER_ID())


                       IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Could reuse this even though this is an outgoing call... These are likely to all be cutscenes though.


                           #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Can't hang up phone during Pause for Answering Time. This has been disabled for this call by mission script.")
                                cdPrintnl()
                
                           #endif

                        

                       ELSE

                           g_InputButtonJustPressed = TRUE

                            
                  
                            Play_Select_Beep()


                            STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation.  As this is a phonecall then a hang up mid-sentence will cut any speaker off at any point.

                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Phone hungup during Pause for Answering Time pre-conversation via joypad")
                                cdPrintnl()
                
                            #endif

                            g_LastCellphoneCallInterrupted = TRUE


                            
                            g_ConversationStatus = CONV_STATE_FINISHED


                            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)


                            IF NOT g_ShouldPhoneBeForcedOnScreen //Fix for 1291246

                                SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)  //Flag beforecallstatehandler that we need to return to the homescreen
                                                                                                                    //for consistency's sake. See ##1119893
                            ENDIF
                            
                            
                            BeforeCallStateHandler ()

                            
                         
                            g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made.
                      
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 268. Dialogue_Handler assigns BeforeCallDS")
                                cdPrintnl()   
                            #endif             
                          
                            Update_Scaleform_VSE ()

                        ENDIF


                    ENDIF
                ENDIF



             ENDIF


       ELSE


                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - Conversation status set to PLAYING, now PAUSE_ANSWERING_TIME has concluded.")
                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 03")
                    cdPrintnl()

                
                #endif

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)



                g_ConversationStatus = CONV_STATE_PLAYING

                g_PrepareCallForDynamicBranch = FALSE

               


                Set_SF_Buttons_SuppText_For_ConnectedCall()
                TitleUpdate_Add_Hide_Phone_Soft_Key()

                IF NOT (g_IsThisAnMPChatCall)


                    IF g_CellphonePromptResponse = RESPONSE_STORE_EMPTY

                        
                        #if IS_DEBUG_BUILD

                            cdPrintstring ("DIALOG_HAND - Using START_PRELOADED_CONVERSATION 2 as response store was empty.")
                            cdPrintstring ("DIALOG_HAND - Also checking preload start prevention bit")
                            cdPrintnl()

                        #endif

                        IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START) //Should only be used by Alwyn's timetable switch!

                            START_PRELOADED_CONVERSATION()

                        ELSE

                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - NOT starting START_PRELOADED_CONVERSATION 2 segment as PREVENT bit was set!")
                                cdPrintnl()
                                cdPrintnl()

                            #endif
                        

                        ENDIF

    
                        //Critical. Make sure this does not persist for other calls.
                        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_PREVENT_PRELOADED_PHONECALL_START)



                    ELSE


                        #if IS_DEBUG_BUILD

                            cdPrintstring ("DIALOG_HAND - Using START_SCRIPT_PHONE_CONVERSATION 2 as this was a response or dynamic branch.")
                            cdPrintnl()

                        #endif


                        START_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.
                
                    ENDIF


                    
                ELSE

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("DIALOG_HAND - Not starting SCRIPT_PHONE_CONVERSATION 2 as g_IsThisAnMPChatCall is returning TRUE")
                        cdPrintnl()

                    #endif

                ENDIF

                Setup_HidePhoneTrial()


      ENDIF





ENDPROC












PROC WaitForPlayerToAcceptRejectCall()


//g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED when we first run this proc. It will keep running until the player accepts or rejects the call and the state of the
//global is changed or if we force the player to accept the call via g_ForcePlayerAnswer
       
    

    IF NETWORK_IS_GAME_IN_PROGRESS() 

        i_ForceAnswerTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_ForceAnswer_StartTime)

        i_AutoHangupTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_IncomingAutoHangUp_StartTime)

    ELSE
            
        ForceAnswer_CurrentTime = GET_GAME_TIMER()
        i_ForceAnswerTimeDiff = ForceAnswer_CurrentTime - ForceAnswer_StartTime

        IncomingAutoHangUp_CurrentTime = GET_GAME_TIMER()
        i_AutoHangupTimeDiff = IncomingAutoHangup_CurrentTime - IncomingAutoHangup_StartTime

    ENDIF



    
    #if IS_DEBUG_BUILD 
        IF g_b_DebugPhoneTimerDisplay
        
            IF NETWORK_IS_GAME_IN_PROGRESS() 
                DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, 0.1, "STRING", "Net Game Timer will be wrong now. It uses new datatype.")
            ELSE
                DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, 0.1, "STRING", "Normal Game Timer")
            ENDIF


            DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.2, "STRING", "ForceAnswer Start")
            DISPLAY_TEXT_WITH_NUMBER (0.70, 0.20, "CELL_500", ForceAnswer_StartTime)

            DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.3, "STRING", "ForceAnswer Curr")
            DISPLAY_TEXT_WITH_NUMBER (0.70, 0.30, "CELL_500", ForceAnswer_CurrentTime)


            DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.6, "STRING", "IncomingAHU Start")
            DISPLAY_TEXT_WITH_NUMBER (0.70, 0.6, "CELL_500", IncomingAutoHangUp_StartTime)

            DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.7, "STRING", "IncomingAHU Curr")
            DISPLAY_TEXT_WITH_NUMBER (0.70, 0.7, "CELL_500", IncomingAutoHangUp_CurrentTime)
        
        ENDIF
    #endif




    //If the call has been created with a "force answer" function and the answer delay has elapsed, make the player accept the inbound call.
    IF g_ForcePlayerAnswer = TRUE 
    //OR g_IsThisAnMpJobOffer = TRUE //This was removed at the request of bug 990782. Originally, Job Offers auto picked up at the request of Les.
        

        IF ShouldFirstPersonPhoneHudMovementBeHidden()


            IF i_ForceAnswerTimeDiff > (ForceAnswer_DelayTime + 2000)
                
                IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //Potential fix for #1505412

                    IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
                        //IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)

                            
                            Play_Select_Beep()
              
                            g_InboundCallWaitAccRej = CALL_ACCEPTED


                            #if IS_DEBUG_BUILD 

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - ForcePlayerAnswer First Person adjusted time has elapsed. Status set to CALL_ACCEPTED.")
                                cdPrintnl()       
                                cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 0412")
                                cdPrintnl()
                
                            #endif

                            //Trial Removal of DISPLAY VIEW 4 call.
                            Set_SF_Buttons_SuppText_For_ConnectedCall()
                            TitleUpdate_Add_Hide_Phone_Soft_Key()

                        //ENDIF
                    ENDIF

                ENDIF

            ENDIF


        ELSE

                IF i_ForceAnswerTimeDiff > ForceAnswer_DelayTime
                    
                    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //Potential fix for #1505412

                        IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
                            //IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)

                                
                                Play_Select_Beep()
                  
                                g_InboundCallWaitAccRej = CALL_ACCEPTED


                                #if IS_DEBUG_BUILD 

                                    cdPrintnl()
                                    cdPrintstring ("DIALOG_HAND - ForcePlayerAnswer time has elapsed. Status set to CALL_ACCEPTED.")
                                    cdPrintnl()       
                                    cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 04")
                                    cdPrintnl()
                    
                                #endif

                                //Trial Removal of DISPLAY VIEW 4 call.
                                Set_SF_Buttons_SuppText_For_ConnectedCall()
                                TitleUpdate_Add_Hide_Phone_Soft_Key()

                            //ENDIF
                        ENDIF

                    ENDIF

                ENDIF

        ENDIF

    ENDIF
 
      


    //If the player does not accept or reject the call within the const AutoHangUp time, the caller will hang up and the call will be classed as rejected - and interrupted.
    IF i_AutoHangupTimeDiff > IncomingAutoHangUp_ConstTime


        #if IS_DEBUG_BUILD

            cdPrintnl ()
            cdPrintstring ("DIALOG_HAND - AutoHangupTime has elapsed - Player has not actively accepted or rejected call. Call will be set to rejected automatically.")
            cdPrintnl ()


            cdPrintstring ("DIALOG_HAND -  AutoHangupStart time is ")
            cdPrintint (IncomingAutoHangUp_StartTime)
            cdPrintnl()           


            cdPrintstring ("DIALOG_HAND -  AutoHangupCurrent time is ")
            cdPrintint (IncomingAutoHangUp_CurrentTime)
            cdPrintnl()
            cdPrintnl()



        #endif

        g_InboundCallWaitAccRej = CALL_REJECTED


    ENDIF




    IF g_InboundCallWaitAccRej = CALL_ACCEPTED



        b_ScaleformCallButtonsSetUp = FALSE

        #IF IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - Setting call screen setup to FALSE after g_InboundCallWaitAccRej set to CALL_ACCEPTED")
            cdPrintnl()
        
        #endif


        g_ConversationStatus = CONV_STATE_PLAYING   //this should be done before setting the phone to the ear / playing the call so that any brief anim / task delay
                                                    //doesn't allow another call to interrupt.

        
        Put_Cellphone_To_Ear()

  
        #if IS_DEBUG_BUILD
            IF g_TotalMultiparts < 2

                cdPrintnl()
                cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Starting mobile phone call: ")
                cdPrintstring(g_ConversationData.ConversationSegmentToGrab)
                        
                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 06")
                cdPrintnl()


            ELSE

                cdPrintnl()
                cdPrintstring("DIALOG_HAND DESIGNER_DEBUG - Starting multipart mobile phone call with first root: ")
                cdPrintstring(g_ConversationData.MultipartSegmentToGrab[0])
                cdPrintnl()
                cdPrintstring ("DIALOG_HAND - SuppTextButtonCall 07")
                cdPrintnl()


            ENDIF


        #endif 
    



        //START_MOBILE_PHONE_CALL (g_ConversationPedsStruct.PedInfo[FirstSpeakerNumber].Index, g_ConversationPedsStruct.PedInfo[FirstSpeakerNumber].VoiceID, g_ConversationPedsStruct.PedInfo[SecondSpeakerNumber].Index, g_ConversationPedsStruct.PedInfo[SecondSpeakerNumber].VoiceID, g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen)
           
        g_PrepareCallForDynamicBranch = FALSE

        //Play the phone call using new audio command...

        Set_SF_Buttons_SuppText_For_ConnectedCall()
        TitleUpdate_Add_Hide_Phone_Soft_Key()



        IF NOT (g_IsThisAnMPChatCall)

            IF g_CellphonePromptResponse = RESPONSE_STORE_EMPTY

                
                #if IS_DEBUG_BUILD

                    cdPrintstring ("DIALOG_HAND - Using START_PRELOADED_CONVERSATION 3 as response store was empty.")
                    cdPrintnl()

                #endif


                START_PRELOADED_CONVERSATION()

            ELSE


                #if IS_DEBUG_BUILD

                    cdPrintstring ("DIALOG_HAND - Using START_SCRIPT_PHONE_CONVERSATION 3 as this was a response or dynamic branch.")
                    cdPrintnl()

                #endif


                START_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.
        
            ENDIF

            //START_SCRIPT_PHONE_CONVERSATION (g_ConversationData.g_DisplaySubtitles, g_ConversationData.g_AddToBriefScreen) //Display or Not Display subtitles and Add or Don't Add to brief screen the dialogue lines.

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintstring ("DIALOG_HAND - Not starting SCRIPT_PHONE_CONVERSATION 3 as g_IsThisAnMPChatCall is returning TRUE")
                cdPrintnl()

            #endif

        ENDIF
        
        Setup_HidePhoneTrial()



        //Legacy
        //Tell Andrew's finance scripts that a phone conversation has been triggered.
        //INFORM_BAWSAQ_OF_EVENT (BS_EVENT_PHONECALL_TRIGGERED)


        g_LastInboundCallRejected = FALSE

    ENDIF
   
       

    //By checking the force answer global here,  we can make sure the call can never be rejected during a force answer by manipulating this segment
    //and in cellphone flashhand.sc
    IF g_ForcePlayerAnswer = FALSE
    
    
        IF g_InboundCallWaitAccRej = CALL_REJECTED

            
            IF g_IsThisAnMPChatCall //Work for 1602347
            
            
                SCRIPT_END_A_VOICE_SESSION()

                HANG_UP_AND_PUT_AWAY_PHONE()

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Chat Call rejected. Ending voice session.")
                    cdPrintnl()

                #endif 


            ELSE

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - Call rejected. It was not a force answer call.")
                    cdPrintnl()

                #endif 


                Perform_dh_StockMarketUpdate()


                //call must have been rejected, set state to finish and cleanup call.

                g_LastInboundCallRejected = TRUE


                STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...



                g_ConversationStatus = CONV_STATE_FINISHED


                
                BeforeCallStateHandler ()

                
                //Scaleform check...Probably not required here.
                g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 65. Dialogue_Handler assigns BeforeCallDS")
                    cdPrintnl()   
                #endif


           

                Update_Scaleform_VSE ()

            ENDIF



        ENDIF


    ELSE

         IF g_InboundCallWaitAccRej = CALL_REJECTED

                #IF IS_DEBUG_BUILD
                    
                    SCRIPT_ASSERT ("Asserts unrelated to this script have caused NETWORK_TIMER to reject call on client unexpectedly. Cleaning up call.") 
                    cdPrintnl()
                    cdPrintstring("DIALOG_HAND - A force answer call has somehow reached CALL_REJECTED status. Let Steve T or Keith know. Please save logs.")
                    cdPrintnl()


                    g_LastInboundCallRejected = TRUE


                    STOP_SCRIPTED_CONVERSATION (FALSE) //Clear up preload / don't finish last line of conversation as this is a call...


                    g_ConversationStatus = CONV_STATE_FINISHED


                    
                    BeforeCallStateHandler ()

                    
                    //Scaleform check...Probably not required here.
                    g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 66. Dialogue_Handler assigns BeforeCallDS")
                        cdPrintnl()   
                    #endif              

                    Update_Scaleform_VSE ()



                #endif 
        
         ENDIF

    ENDIF




ENDPROC

                       









PROC PlayFaceToFaceConversation()


 
	#IF IS_DEBUG_BUILD
	
		IF g_bEnableConversationMute
			
            cdPrintnl()
            cdPrintstring ("DIALOG_PRIV - g_bEnableConversationMute debug is ENABLED. Can't play conversations (PlayFaceToFaceConversation)!")
            cdPrintnl()

			EXIT
			
		ENDIF
	
	#ENDIF

    //We'd need to check for conversations being paused, in this segment.

    //#IF IS_DEBUG_BUILD
        //CheckForLineSkip () //from dialogue_private.sch  //No line skip for face-to-face conversations.
    //#endif



    IF IS_PLAYER_PLAYING (PLAYER_ID())

        //IF IS_ENTITY_IN_WATER (PLAYER_PED_ID())
        IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())  //Complaints about conversations being killed in water. Compromise is fully submerged... #1061632

            IF g_AllowUnderwaterDialogue = FALSE
                                    
                KillFaceToFaceConversationImmediately()

                #if IS_DEBUG_BUILD  
                                                                                        
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Player ped swimming underwater - Killing face to face conversation immediately. Use ALLOW_DIALOGUE_IN_WATER if you need to bypass this.")
                    cdPrintnl()
                
                #endif

            ENDIF

        ENDIF
    ENDIF





    IF bool_BlockLoadFailed = TRUE

        KillFaceToFaceConversationImmediately()

        #if IS_DEBUG_BUILD  
                                                                                
            cdPrintnl()              
            cdPrintstring("DIALOG_HAND - Block Load failed - Killing face to face conversation immediately")
            cdPrintnl()
        
        #endif

        KillFaceToFaceConversationImmediately()

    ENDIF


    IF g_IsThisFaceToFacePreloaded = FALSE


        IF IsConversationPaused = FALSE
        
            IF g_ConversationPaused = TRUE

                PAUSE_SCRIPTED_CONVERSATION (TRUE) //finish last line
                IsConversationPaused = TRUE

                #IF IS_DEBUG_BUILD  
                                                                                    
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Face to face conversation paused.")
                    cdPrintnl()
            
                #endif


            ENDIF
        
        ELSE //Conversation not in paused state.
                    
            IF g_ConversationPaused = FALSE

                RESTART_SCRIPTED_CONVERSATION()
                IsConversationPaused = FALSE

                #if IS_DEBUG_BUILD  
                                                                                    
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Face to face conversation restarted.")
                    cdPrintnl()
            
                #endif


            ENDIF
      
        ENDIF  
        

    ELSE

    
        #if IS_DEBUG_BUILD  
            
            IF g_b_DisplayConversationToggle      
                                                                            
                cdPrintnl()              
                cdPrintstring("DIALOG_HAND - PlayFaceToFaceConversation() holding preloaded conversation.")
                cdPrintnl()

            ENDIF
    
        #endif
    

        IF g_i_PreloadAutoPlayTime > 0


            i_FaceToFacePreloadCurrentTime = GET_GAME_TIMER()

            IF (i_FaceToFacePreloadCurrentTime - i_FaceToFacePreloadStartTime) > g_i_PreloadAutoPlayTime

                START_PRELOADED_CONVERSATION()

                g_IsThisFaceToFacePreloaded = FALSE //Make sure dialogue handler will now allow the conversation to be paused.
                     
                #if IS_DEBUG_BUILD  
                                                                                        
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - PreloadAutoPlayTime has expired. Starting preloaded conversation.")
                    cdPrintnl()

                #endif

            ENDIF
        
        ENDIF
         

    ENDIF    





    IF g_ConversationPaused = FALSE

        IF NOT IS_SCRIPTED_CONVERSATION_ONGOING() 

            #IF IS_DEBUG_BUILD  
                                                                                
                cdPrintnl()              
                cdPrintstring("DIALOG_HAND - Face to face conversation finished.")
                cdPrintnl()
        
            #endif
    
        
                     
            g_ConversationStatus = CONV_STATE_FINISHED

        ENDIF


        //Possibly add ELSE timeout clause here for task 167661?


    ENDIF


ENDPROC



















PROC PlayCellphoneConversation()


    IF IS_MOBILE_PHONE_CALL_ONGOING()
            
        IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PHONE_UP_TO_EAR) //Put the cellphone to the player's ear if not already there.
        
                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                    IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                        IF g_bInMultiplayer = FALSE

                            #if IS_DEBUG_BUILD

                                cdPrintstring ("DIALOG_HAND - Phone call still ongoing, player not in vehicle. Putting cellphone to ear! TODO 1888348")
                                cdPrintnl()

                            #endif

                            Put_Cellphone_To_Ear()

                        ENDIF
                        

                    ENDIF
                ENDIF

        
        ELSE


                IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
                    IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())

                        IF g_bInMultiplayer = FALSE

                            #if IS_DEBUG_BUILD

                                cdPrintstring ("DIALOG_HAND - Phone call still ongoing, player now in vehicle. Removing cellphone from  ear! TODO 2003415")
                                cdPrintnl()

                            #endif

                            Remove_Cellphone_From_Ear()

                        ENDIF
                        

                    ENDIF
                ENDIF

        
        ENDIF



         /* Test routine. Doing this looks bad.

            IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())
            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

                IF g_bInMultiplayer = FALSE

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("DIALOG_HAND - Phone call still ongoing, player now in vehicle FIRST person. Removing cellphone from  ear! TODO 2003415")
                        cdPrintnl()

                    #endif

                    TASK_USE_MOBILE_PHONE (PLAYER_PED_ID(), FALSE)

                ENDIF
                

            ENDIF
            ENDIF

        */








        
    ENDIF



     //Was disabled pending audio fix of 1280098


    //For 1609922 in MP, this is getting removed on a trial basis.
    IF g_bInMultiplayer = FALSE

        CheckForLineSkip () //from dialogue_private.sch  //allowing line skip in release mode...
  
    ENDIF


    IF bool_BlockLoadFailed = TRUE

        #if IS_DEBUG_BUILD
            
            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - Conversation block for call failed to load, hanging up phone!")
            cdPrintnl()

        #endif



        HANG_UP_AND_PUT_AWAY_PHONE()

    ENDIF






    IF IsConversationPaused = FALSE
    
        IF g_ConversationPaused = TRUE


            IF g_PauseRestartHasComeFromDedicatedCellphoneFunction = TRUE

                PAUSE_SCRIPTED_CONVERSATION (TRUE) //finish last line
                IsConversationPaused = TRUE

                g_PauseRestartHasComeFromDedicatedCellphoneFunction = FALSE

                #if IS_DEBUG_BUILD  
                                                                                    
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Cellphone conversation paused.")
                    cdPrintnl()
            
                #endif

            ENDIF

        ENDIF
    
    ELSE //Conversation not in paused state.
                
        IF g_ConversationPaused = FALSE

            IF g_PauseRestartHasComeFromDedicatedCellphoneFunction = TRUE

                RESTART_SCRIPTED_CONVERSATION()
                IsConversationPaused = FALSE

                g_PauseRestartHasComeFromDedicatedCellphoneFunction = FALSE

                #if IS_DEBUG_BUILD  
                                                                                    
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Cellphone conversation restarted.")
                    cdPrintnl()
            
                #endif

            ENDIF


        ENDIF
  
    ENDIF 













    //Don't auto-hide phone if this call requires a response or an answerphone message is playing...

    IF g_CallRequiringPlayerResponse = FALSE 
        
        IF NOT (g_IsThisAnMPChatCall) //Requested by Conor so that a chat call always stays up...

            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_THIS_IS_AN_ANSWERPHONE_CALL) //If this is an answerphone call, don't auto-hide phone. 

                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) 

                    IF Has_HidePhoneTrialTimerElapsed() //Move phone downscreen after a short time after a call reaches CONNECTED status...

                        IF g_Cellphone.PhoneDS > PDS_MAXIMUM //Make sure the phone can only move offscreen if a call is ongoing

                            //Might have to have a check in here to not put the phone away if it's on the last couple of lines? Polish.
                            
                            //Set appropriate bits...
                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE) //Let flashhand know transit routine should commence...
                            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) //we don't want to go through this routine more than once in one call...


                            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)
                            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

                            #if IS_DEBUG_BUILD
                    
                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Has_HidePhoneTrialTimerElapsed, setting vanish bits...")
                                cdPrintnl()

                            #endif

                        ENDIF

                    ENDIF

                ENDIF

            ENDIF

        
        ELSE //Hands Free Chat call functionality for 1566736 - moved to Cellphone Flashhand as the initiator of the call isn't actually running a conversation here!
        
        
            /*
            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_THIS_IS_AN_ANSWERPHONE_CALL) //If this is an answerphone call, don't auto-hide phone. 

                IF NOT IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) 

                    //IF Has_HidePhoneTrialTimerElasped() //Move phone downscreen after a short time after a call reaches CONNECTED status...
                    IF Is_Player_On_Voice_Chat_Phonecall()

                        
                        IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDING_CELLPHONE_CAM_PIC) //don't check for this button if we are sending a cellphone pic.
                       
                        
                                
                            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Enter new number

                            
                            
                                IF g_Cellphone.PhoneDS > PDS_MAXIMUM //Make sure the phone can only move offscreen if a call is ongoing

                                    //Might have to have a check in here to not put the phone away if it's on the last couple of lines? Polish.
                                    
                                    //Set appropriate bits...
                                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE) //Let flashhand know transit routine should commence...
                                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) //we don't want to go through this routine more than once in one call...


                                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)
                                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

                                    #if IS_DEBUG_BUILD
                            
                                        cdPrintnl()
                                        cdPrintstring ("DIALOG_HAND - Button press detected to go hands free on Chat Call. Setting vanish bits...")
                                        cdPrintnl()

                                    #endif

                                ENDIF



                            ENDIF


                        ENDIF

                    ENDIF

                ENDIF

            ENDIF
            */
        
        
        
        
        ENDIF

    ENDIF
    








    //If no phone call is ongoing AND the chat call flag has not been set...
    IF (IS_MOBILE_PHONE_CALL_ONGOING() = FALSE) //i.e a phone call has just finished and we're checking if it requires a response...
    AND (g_IsThisAnMPChatCall = FALSE ) //This will allow a chat call to remain in "playing" state.

        

        IF g_CallRequiringPlayerResponse = TRUE

            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_WITH_REPLIES_WAITING_ON_USER_INPUT) 

        ELSE

            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_WITH_REPLIES_WAITING_ON_USER_INPUT)

        ENDIF


        
        IF g_PrepareCallForDynamicBranch  //Possibly change this to a while check...

            IF g_CallRequiringPlayerResponse = FALSE   
        
                g_ConversationData.ConversationSegmentToGrab = g_DynamicBranch_Root
                g_BlockToLoad = g_DynamicBlockToLoad
                //g_PrepareCallForDynamicBranch = FALSE //This needs to be done right before conversation starts to play as it can effect answering time pauses otherwise.


                g_UpcomingConversationInboundForCellphone = FALSE

                g_CellphonePromptResponse = RESPONSE_DYNAMIC_BRANCH //Re-use existing infrastructure to prevent "answering time" pause kicking in.


                //Set_SF_Buttons_to_OngoingCall()




                ClearConversationData() //Clear the old conversation data and get ready to build the next branch which I'm treating as an entirely new conversation.
                            


                EXIT //Need to exit the procedure here so that CONV_STATE_BUILDING, set in ClearConversationData is not superceded by the assignment to FINISHED at the 
                     //true end of the call.

            ELSE

                //N Multipart with response could be built here?



                #if IS_DEBUG_BUILD
                    
                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - Warning! Dynamic branch has been set on _WITH_REPLIES_CALL!")
                    cdPrintnl()

                #endif

            ENDIF

        ENDIF 





       IF NOT g_IsEmergencyServicesCall //Emergency service call will now use a scaleform list as requested by 1015536. For neatness and flexibility, doing this in the else branch.
        


            WHILE g_CallRequiringPlayerResponse = TRUE //This phone call requires a yes / no response from the player. Let's wait until he responds...

                WAIT(0)
            

                //Dynamic Work...





                //Takes care of bug 113398 whereupon the phone was waiting for a response but the MP guys were disabling it.
                IF g_Cellphone.PhoneDS < PDS_MAXIMUM

                    g_CallRequiringPlayerResponse = FALSE

                    #if IS_DEBUG_BUILD
                        
                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Cellphone call was waiting on a player response but it appears that the phone has been disabled or forced away by an incident.")
                        cdPrintnl()

                    #endif
                    
                ELSE



                        IF b_ScaleformCallButtonsSetUp = FALSE

          
                            MovePhoneToWaistForFirstPersonDecision()



                            //All subsequent g_IsEmergencyServicesCall cannot possibly return true here due to the envelope added for bug 1015536, but keeping the logic
                            //in case minds are changed and we go back to the soft key method.



                            IF g_IsEmergencyServicesCall //If this is an emergency services call, display choice of ambulance / fire / police

                                
                                b_ScaleformCallButtonsSetUp = TRUE

                                
                                g_TellFlashHandToDisplayEmergencyServices = TRUE

                                  
                                //WIPBmark
                                //Replace "outgoing_call" descriptor with question and update buttons for emergency services.
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(4)),
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                    g_QuestionGodLabel,
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].label, 

                                    //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                    g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                    

                                
                                

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)

                                #if IS_DEBUG_BUILD
                                    
                                    cdPrintstring ("DIALOGUE_HAND - Update_Scaleform_VSE called - 301")
                                    cdPrintnl ()
                                    cdPrintstring ("Display View 4 - Dialogue Handler 1")
                                    cdPrintnl()
        
                                #endif



                       

                                //g_SF_ButtonsSizeX = 0.20 //demo hack

                                //Altered transparency here for badger from opaque to transparent
                                //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //0 - transparent, 0 - opaque

                                

                                //Monochrome
                                /*
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_604") //"Police" Positive

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_603") //"Fire" - Negative

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 1,
                                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_602") //"Paramedic" - Other
                                */

                                
                                //Badger - Select Back combo
                                IF g_b_ToggleButtonLabels

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                        17, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_604") //"Police" - Positive

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                        18, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_602") //"Paramedic" - Other
                                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                        19, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_603") //"Fire" - Negative

                                ELSE


                                    //Force labels on regardless as a temporary measure until representative soft key icons are available.
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                        17, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Police" - Positive

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                        18, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Paramedic" - Other
                                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                        19, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Fire" - Negative


                                    
                                    /* Check back in to not display button labels.
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Police" - Positive

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Paramedic" - Other
                                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                        2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Fire" - Negative

                                    */

                                ENDIF

                    







                            
                            ELSE //otherwise display standard yes / no


                                b_ScaleformCallButtonsSetUp = TRUE


                                g_TellFlashHandToDisplayYN = TRUE 
                                
                                IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 
                            
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(4)),
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_QuestionGodLabel, "CELL_300",
                                        g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel, "CELL_195" )
                                    
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)


                                    #if IS_DEBUG_BUILD

                                        cdPrintstring ("DIALOGUE_HAND - Update_Scaleform_VSE called - 302")
                                        cdPrintnl ()
                                        cdPrintstring ("Display View 4 - Dialogue Handler 2")
                                        cdPrintnl()

                                    #endif


                                ELSE


                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(4)), 
                                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                                        g_QuestionGodLabel,
                                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].label, 

                                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                                    
                                    
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)

                                    #if IS_DEBUG_BUILD

                                        cdPrintstring ("DIALOGUE_HAND - Update_Scaleform_VSE called - 303")
                                        cdPrintnl ()
                                        cdPrintstring ("Display View 4 - Dialogue Handler 3")
                                        cdPrintnl()

                                    #endif



                                ENDIF

                                
                                //g_SF_ButtonsSizeX = 0.20 //demo hack
                                //Altered transparency here for badger from opaque to transparent
                                //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //opaque


                                 
                                //Monochrome                
                                /*
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" Positive

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative

                                //This button should not be present on screen, so must be switched off via method second int parameter.
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
                                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
                                */

                            
                                //Badger - Yes, No combo
                                IF g_b_ToggleButtonLabels
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                        13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" - Positive

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                        14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative

                                ELSE
                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                        13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

                                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                                        14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                                ENDIF

                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)





                            ENDIF



                        ENDIF





                         //MP response work...

               
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Fix for revert_tues response...  
                        #if IS_DEBUG_BUILD
                            OR g_bFlowAutoplayInProgress = TRUE
                        #endif
              
               

                            MovePhoneToEarForFirstPersonDecision()


                            
                            g_SF_ButtonsSizeX = 0.19
                            
                            g_CallRequiringPlayerResponse = FALSE
                          
                      
                            Play_Select_Beep()


                            IF g_IsEmergencyServicesCall


                                //Was.. g_ConversationData.ConversationSegmentToGrab = g_AmbulanceSegmentLabel //ready to play next branch, so safe to copy across temp holder as initial question has concluded.            
                                g_ConversationData.ConversationSegmentToGrab = g_PoliceSegmentLabel                  


                                #IF IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - Selected Police at service prompt...")
                                    cdPrintnl()
                                
                                #endif

                             
                                g_CellphonePromptResponse = RESPONDED_POLICE


                                    


                            ELSE

                            
                                IF g_TotalReplyMultiparts > 0  //Is this a multipart reply?
                                 

                                    g_b_Is_MultipartRepliesInProgress = TRUE //This is only set to false by CommonDialogueCleanup.

                                    g_ConversationStatus = CONV_STATE_FREE  //Set this so that CheckCurrentConversationStatusForMultipart does not return false. 
                                                                            //Note, we are not running CommonDialogueCleanup by setting this. That runs when CONV_STATE_FINISHED is set.

                                    
                         
                                    g_Outgoing_Call_Pause_Answering_Time = 0 //we don't want any "picking up" pause, the contact has already answered.

                                    

                                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_UNIVERSAL_REPLIES_FOR_N_LINES_HAS_BEEN_SET)


                           
                                        INT tempYesInt = 0

                                        WHILE tempYesInt < (g_i_Total_UniversalYesReplies + 1) 
                                        
                                            TempYesRoots[tempYesInt] = CONVERT_TEXT_LABEL_TO_STRING(g_Multipart_Universal_YesRoots[tempYesInt])
                                            TempYesSpecifics[tempYesInt] = CONVERT_TEXT_LABEL_TO_STRING(g_Multipart_Universal_YesSpecifics[tempYesInt])

                                            tempYesInt ++
                                        ENDWHILE
                                


                                        PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES (g_i_Total_UniversalYesReplies,
                                                            g_ConversationPedsStruct, g_TheContactInvolvedinCall, g_BlockToLoad,
                                                            TempYesRoots, TempYesSpecifics, CONV_PRIORITY_VERY_HIGH, FALSE)





                                    ELSE    //Consider consolidating this two-line specific stuff into above when I've more time... Need to get Friends script to act if so.


                                        //Continue to support Friend activity specific two line replies.
                                        IF g_TotalReplyMultiparts = 2 
                                         //Only required to have a 2-line reply at present. Variety can be increased and the call type switched depending on g_TotalReplyMultiparts value.
                                        PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES (g_ConversationPedsStruct, g_TheContactInvolvedinCall, g_BlockToLoad,  
                                                                                    
                                                    g_Multipart_YesSegmentLabel[0], g_Multipart_YesSpecificLabel[0], 
                                                                                          
                                                    g_Multipart_YesSegmentLabel[1], g_Multipart_YesSpecificLabel[1],                                      
                                                                                 
                                                    CONV_PRIORITY_VERY_HIGH, FALSE) 

                                        ENDIF


                                    ENDIF







                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected YES at prompt to cue multipart reply")                                
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_YES



                                    g_TellFlashHandToDisplayYN = FALSE
                                    g_TellFlashHandToDisplayEmergencyServices = FALSE

                                    g_UpcomingConversationInboundForCellphone = FALSE


                                    Set_SF_Buttons_to_OngoingCall()




                                    EXIT

     



                                ELSE


                                    g_ConversationData.ConversationSegmentToGrab = g_YesSegmentLabel //ready to play next branch, so safe to copy across temp holder as initial question has concluded.            
                                  
                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected Yes at prompt...")
                                        cdPrintnl()
                                    
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_YES


                                ENDIF

                            ENDIF



                            g_TellFlashHandToDisplayYN = FALSE
                            g_TellFlashHandToDisplayEmergencyServices = FALSE

                            g_UpcomingConversationInboundForCellphone = FALSE


                            Set_SF_Buttons_to_OngoingCall()


                            //Pass in response to global...
                            ClearConversationData() //Clear the old conversation data and get ready to build the next branch which I'm treating as an entirely new conversation.
                            


                            EXIT //Need to exit the procedure here so that CONV_STATE_BUILDING, set in ClearConversationData is not superceded by the assignment to FINISHED at the 
                                 //true end of the call.

                        ENDIF



                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT)) //Should be Macro!
                        OR IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_FORCE_CALL_WITH_REPLIES_NEGATIVE) 

                            
                            MovePhoneToEarForFirstPersonDecision()

                                        
                            g_SF_ButtonsSizeX = 0.19


                            g_CallRequiringPlayerResponse = FALSE
                            

                            Play_Select_Beep()



                            IF g_IsEmergencyServicesCall


                                g_ConversationData.ConversationSegmentToGrab = g_FireSegmentLabel //ready to play next branch, so safe to copy across temp holder as initial question has concluded.            
                              
                                #IF IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - Selected Fire at service prompt...")
                                    cdPrintnl()
                                
                                #endif

                                g_CellphonePromptResponse = RESPONDED_FIRE



                            ELSE


                                IF g_TotalReplyMultiparts > 0  //Is this a multipart reply?
        
                                 

                                    g_b_Is_MultipartRepliesInProgress = TRUE //This is only set to false by CommonDialogueCleanup.

                                    g_ConversationStatus = CONV_STATE_FREE  //Set this so that CheckCurrentConversationStatusForMultipart does not return false. 
                                                                            //Note, we are not running CommonDialogueCleanup by setting this. That runs when CONV_STATE_FINISHED is set.

                                    
                         
                                    g_Outgoing_Call_Pause_Answering_Time = 0 //we don't want any "picking up" pause, the contact has already answered.

                                    

                                    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_UNIVERSAL_REPLIES_FOR_N_LINES_HAS_BEEN_SET)


                              
                                        INT tempNoInt = 0

                                        WHILE tempNoInt < (g_i_Total_UniversalNoReplies + 1) 
                                        
                                            TempNoRoots[tempNoInt] = CONVERT_TEXT_LABEL_TO_STRING(g_Multipart_Universal_NoRoots[tempNoInt])
                                            TempNoSpecifics[tempNoInt] = CONVERT_TEXT_LABEL_TO_STRING(g_Multipart_Universal_NoSpecifics[tempNoInt])

                                            tempNoInt ++
                                        ENDWHILE
                                


                                        PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES (g_i_Total_UniversalNoReplies,
                                                            g_ConversationPedsStruct, g_TheContactInvolvedinCall, g_BlockToLoad,
                                                            TempNoRoots, TempNoSpecifics, CONV_PRIORITY_VERY_HIGH, FALSE)





                                    ELSE  //Consider consolidating this two-line specific stuff into above when I've more time...   Need to get Friends script to act if so.


                                        //Continue to support Friend activity specific two line replies.
                                        IF g_TotalReplyMultiparts = 2 
                                         //Only required to have a 2-line reply at present. Variety can be increased and the call type switched depending on g_TotalReplyMultiparts value.
                                        PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES (g_ConversationPedsStruct, g_TheContactInvolvedinCall, g_BlockToLoad,  
                                                                                    
                                                    g_Multipart_NoSegmentLabel[0], g_Multipart_NoSpecificLabel[0], 
                                                                                          
                                                    g_Multipart_NoSegmentLabel[1], g_Multipart_NoSpecificLabel[1],                                      
                                                                                 
                                                    CONV_PRIORITY_VERY_HIGH, FALSE) 

                                        ENDIF


                                    ENDIF















                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected NO at prompt to cue multipart reply.")
                                        cdPrintnl()
                                    
                                    #endif


                                    g_CellphonePromptResponse = RESPONDED_NO


                                    g_TellFlashHandToDisplayYN = FALSE
                                    g_TellFlashHandToDisplayEmergencyServices = FALSE

                                    g_UpcomingConversationInboundForCellphone = FALSE


                                    Set_SF_Buttons_to_OngoingCall()




                                    EXIT


                                ELSE


                                    g_ConversationData.ConversationSegmentToGrab = g_NoSegmentLabel //ready to play next branch, so safe to copy across temp holder as initial question has concluded.            
                                  
                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected NO at prompt...")
                                        cdPrintnl()
                                    
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_NO


                                ENDIF

                            ENDIF

                                        
                            g_TellFlashHandToDisplayYN = FALSE
                            g_TellFlashHandToDisplayEmergencyServices = FALSE

                            g_UpcomingConversationInboundForCellphone = FALSE


                            Set_SF_Buttons_to_OngoingCall()



                            //Pass in response to global...
                            ClearConversationData() //Clear the old conversation data and get ready to build the next branch which I'm treating as an entirely new conversation.
                            


                            
                            EXIT //Need to exit the procedure here so that CONV_STATE_BUILDING, set in ClearConversationData is not superceded by the assignment to FINISHED at the 
                                 //true end of the call.

                        ENDIF  
                 





                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //revert_tues
                  

                            IF g_IsEmergencyServicesCall

                                g_SF_ButtonsSizeX = 0.19
                                

                                g_CallRequiringPlayerResponse = FALSE
                                

                                Play_Select_Beep()


                                MovePhoneToEarForFirstPersonDecision()

                          

                                //Was.. g_ConversationData.ConversationSegmentToGrab = g_PoliceSegmentLabel //ready to play next branch, so safe to copy across temp holder as initial question has concluded.            
                                g_ConversationData.ConversationSegmentToGrab = g_AmbulanceSegmentLabel

                                #IF IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - Selected Paramedic at service prompt...")
                                    cdPrintnl()
                                
                                #endif

                               
                                g_CellphonePromptResponse = RESPONDED_AMBULANCE



                                g_TellFlashHandToDisplayYN = FALSE
                                g_TellFlashHandToDisplayEmergencyServices = FALSE

                                g_UpcomingConversationInboundForCellphone = FALSE


                                Set_SF_Buttons_to_OngoingCall()




                                //Pass in response to global...
                                ClearConversationData() //Clear the old conversation data and get ready to build the next branch which I'm treating as an entirely new conversation.
                                


                                

                                
                                EXIT //Need to exit the procedure here so that CONV_STATE_BUILDING, set in ClearConversationData is not superceded by the assignment to FINISHED at the 
                                     //true end of the call.





                            ELSE

                              
                                #IF IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - This button selection has no function in yes/no response call.")
                                    cdPrintnl()
                                
                                #endif

                          
                            ENDIF

                                        
              
                        ENDIF  



                ENDIF // < PDS_MAXIMUM check.


            ENDWHILE






                
        ELSE  //g_IsEmergencyServicesCall must be true... 1015536
                

            WHILE g_CallRequiringPlayerResponse

                WAIT(0)


                //Takes care of bug 113398 whereupon the phone was waiting for a response but the MP guys were disabling it.
                IF g_Cellphone.PhoneDS < PDS_MAXIMUM

                    g_CallRequiringPlayerResponse = FALSE

                    #if IS_DEBUG_BUILD
                        
                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Cellphone call was waiting on a player response in Emergency Services call but it appears that the phone has been disabled or forced away by an incident.")
                        cdPrintnl()

                    #endif
                    
                ELSE


                        IF b_ScaleformCallButtonsSetUp = FALSE
                            
                            
                            MovePhoneToWaistForFirstPersonDecision()

                                                       
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "SET_DATA_SLOT_EMPTY", 18) 


                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(0)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_603") //Fire Department

                       
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(1)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_602") //Paramedic

                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(18)), (TO_FLOAT(2)),
                                        (TO_FLOAT(0)),INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_604") // 'Ra Polis.



                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 18) //Set enum state to simple list.
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_601") //Which Service?






                            //Add in Select button for service list.
                            IF g_b_ToggleButtonLabels
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_205") //"Select" - Positive
                             
                            ELSE
                                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"SELECT" - Positive

                            ENDIF


                
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other

                            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                        
                            dh_dpad_scroll_pause_cued = FALSE

                            b_ScaleformCallButtonsSetUp = TRUE


                        ENDIF
                                



                        Check_for_911_List_Navigation()




                        
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))  
                        #if IS_DEBUG_BUILD
                            OR g_bFlowAutoplayInProgress = TRUE
                        #endif

                            
                      

                            MovePhoneToEarForFirstPersonDecision()



                            g_CallRequiringPlayerResponse = FALSE
                            

                            Play_Select_Beep()


                            INT i_ChosenService

                     
                            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
                            ServiceChoice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


        
                            WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (ServiceChoice_ReturnedSFIndex)
                            
                                WAIT (0)

                                #if IS_DEBUG_BUILD
                                    cdPrintstring("DIALOG_HAND - Waiting on return value from scaleform. 601")
                                    cdPrintnl()
                                #endif
                                
                            ENDWHILE


                            i_ChosenService = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (ServiceChoice_ReturnedSFIndex)

                            
                                #if IS_DEBUG_BUILD
                                    cdPrintstring("DIALOG_HAND - Scaleform return int from Choose service was ")
                                    cdPrintint(i_ChosenService)
                                    cdPrintnl()
                                #endif




                            SWITCH i_ChosenService

                                CASE 0

                                    g_ConversationData.ConversationSegmentToGrab = g_FireSegmentLabel        
                                  
                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected Fire at service prompt...")
                                        cdPrintnl()
                                    
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_FIRE

                                BREAK



                                CASE 1

                                    g_ConversationData.ConversationSegmentToGrab = g_AmbulanceSegmentLabel        
                                  
                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected Paramedic at service prompt...")
                                        cdPrintnl()
                                    
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_AMBULANCE

                                BREAK



                                CASE 2

                                    g_ConversationData.ConversationSegmentToGrab = g_PoliceSegmentLabel        
                                  
                                    #if IS_DEBUG_BUILD

                                        cdPrintnl()
                                        cdPrintstring("DIALOG_HAND - Selected Police at service prompt...")
                                        cdPrintnl()
                                    
                                    #endif

                                    g_CellphonePromptResponse = RESPONDED_POLICE

                                BREAK




                            ENDSWITCH





                            g_TellFlashHandToDisplayYN = FALSE
                            g_TellFlashHandToDisplayEmergencyServices = FALSE

                            g_UpcomingConversationInboundForCellphone = FALSE


                            Set_SF_Buttons_to_OngoingCall()



                            //Pass in response to global...
                            ClearConversationData() //Clear the old conversation data and get ready to build the next branch which I'm treating as an entirely new conversation.
                            


                            
                            EXIT //Need to exit the procedure here so that CONV_STATE_BUILDING, set in ClearConversationData is not superseded by the assignment to FINISHED at the 
                                 //true end of the call.





                        ENDIF







                        ///This segment will enable the phone to be hung up during the 911 service selection segment,
                        
                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))  
                        #if IS_DEBUG_BUILD
                            OR g_bFlowAutoplayInProgress = TRUE
                        #endif

                            g_CallRequiringPlayerResponse = FALSE

                            Play_Select_Beep()

                            #if IS_DEBUG_BUILD
                        
                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Hung up during service selection in new format 911 call.")
                                cdPrintnl()

                            #endif

                        ENDIF
                        




                ENDIF

            ENDWHILE


        ENDIF //g_IsEmergencyServicesCall check 1015536
        













       
        #if IS_DEBUG_BUILD  
                                                                                
            cdPrintnl()              
            cdPrintstring("DIALOG_HAND - Call finished.")
            cdPrintnl()
        
        #endif
    



        
        IF g_Non_Essential_Call_Aborted_and_CS_Already_Free = FALSE //If this has been set to true, then we know the conversation has already been stopped
                                                                    //due to a non-essential call operation.

            STOP_SCRIPTED_CONVERSATION (FALSE) //failsafe to make sure conversation is terminated for 167661.

            SETTIMERA(0)

            WHILE TIMERA() < 900
            AND g_Cellphone.PhoneDS = PDS_ONGOING_CALL

                WAIT(0)

                #if IS_DEBUG_BUILD
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Waiting for approx 1 second after finished call before playing Hang Up sfx.")
                    cdPrintnl()
                #endif

            ENDWHILE


                     
            g_ConversationStatus = CONV_STATE_FINISHED

        ENDIF
            

		IF NOT g_bMissionDoNotHangUpFromDialogueHandler
	       
			PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)
			
		ELSE 
		
            #if IS_DEBUG_BUILD
                cdPrintnl()              
                cdPrintstring("DIALOG_HAND - Not playing hang up SFX because g_bMissionDoNotHangUpFromDialogueHandler = TRUE")
                cdPrintnl()
            #endif
			
		ENDIF



        //May need to add specific check for player proactively calling contact here if any timing issue occurs.


        BeforeCallStateHandler ()


        //Scaleform check
        g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 67. Dialogue_Handler assigns BeforeCallDS")
            cdPrintnl()   
        #endif




        

        //If this fires off the assert that means the call has malfunctioned - probably because of a dead ped involved in the phonecall.

        Update_Scaleform_VSE ()


  
    ELSE


        
        IF g_Phone_Active_but_Hidden = FALSE //Don't let player hang up phone during a conversation if it is currently hidden for a hotswap

            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Call hung up but phone not necessarily put away so we call normal cleanup...
                            
                
                IF IS_PLAYER_PLAYING (PLAYER_ID())
                    IF IS_PLAYER_CONTROL_ON (PLAYER_ID())


                       IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)


                           #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Can't hang up phone as this has been disabled for this call by mission script.")
                                cdPrintnl()
                
                           #endif

                        

                       ELSE

                           g_InputButtonJustPressed = TRUE

                            
                            //Removed on a trial basis 17.11.10 - a tap on the joypad will now hang up the call. Phone will be moved away because of BeforeCallStateHandler settings.
                            /*
                            WHILE IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //revert_tues  
                                WAIT(0)
                            ENDWHILE
                            */

                            Play_Select_Beep()


                            STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation.  As this is a phonecall then a hang up mid-sentence will cut any speaker off at any point.

                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring ("DIALOG_HAND - Phone hungup during conversation via joypad")
                                cdPrintnl()
                
                            #endif

                            g_LastCellphoneCallInterrupted = TRUE

                            g_LastCallHungupDuringCallViaJoypad = TRUE


                            
                            g_ConversationStatus = CONV_STATE_FINISHED


                            //May need to add specific check for player proactively calling contact here if any timing issue occurs.
                            
                            
                            
                            BeforeCallStateHandler ()

                            
                            //Critical!
                            //Scaleform Check

                            g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made.
                      
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 68. Dialogue_Handler assigns BeforeCallDS")
                                cdPrintnl()   
                            #endif             
                          
                            Update_Scaleform_VSE ()

                        ENDIF


                    ENDIF
                ENDIF

                



            ENDIF

        ENDIF
        
                                                        
    ENDIF


ENDPROC






PROC MovePhoneOffScreenForAcceptedJobOffer()
    FLOAT fComplete = MovePhoneToDestination(g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio], g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio], g_3dPhoneStartRotVec, g_3dPhoneRotVec, 350.0, FALSE)

    IF fComplete >= 1.0
    
        //CRITICAL!
        IF g_b_RadarWasAlreadyHidden = FALSE //Phone wasn't already hidden, safe to turn back on.

            DISPLAY_RADAR (TRUE) //switch on radar!
        
        ENDIF
        
        //Reset this critical var to false to stop this proc looping.
        b_NeedtoMovePhoneDownForAcceptedJob = FALSE
    ENDIF
ENDPROC





PROC PlayCellphoneJobOfferConversation()



    //Was disabled pending audio fix of 1280098

    //For 1609922 in MP, this is getting removed on a trial basis.
    IF g_bInMultiplayer = FALSE
    
        CheckForLineSkip () //from dialogue_private.sch  ////allowing line skip in release mode...
    
    ENDIF

    IF bool_BlockLoadFailed = TRUE

        #if IS_DEBUG_BUILD
            
            cdPrintnl()
            cdPrintstring ("DIALOG_HAND - Conversation block for job offer call failed to load, hanging up phone!")
            cdPrintnl()

        #endif



        HANG_UP_AND_PUT_AWAY_PHONE()

    ENDIF


                     

            IF b_ScaleformCallButtonsSetUp = FALSE

                b_ScaleformCallButtonsSetUp = TRUE

                g_TellFlashHandToDisplayYN = TRUE 
                

                IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 
            
      


                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(4)),
                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, g_QuestionGodLabel, "CELL_300", "CELL_195", "CELL_195" )


                    
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("DIALOGUE_HAND - Update_Scaleform_VSE called - 305")
                        cdPrintnl ()
                        cdPrintstring ("Display View 4 - Dialogue Handler 4")
                        cdPrintnl()

                    #endif



                ELSE


                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(4)), 
                        INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                        g_QuestionGodLabel,
                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].label, 

                        //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                        g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                    
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 4)

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("DIALOGUE_HAND - Update_Scaleform_VSE called - 309")
                        cdPrintnl ()
                        cdPrintstring ("Display View 4 - Dialogue Handler 5")
                        cdPrintnl()

                    #endif


                ENDIF

                
                g_SF_ButtonsSizeX = 0.20 //demo hack
                //Altered transparency here for badger from opaque to transparent
                //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieButtonsIndex, "SET_BACKGROUND_VISIBILITY", 0) //opaque


                 
                //Monochrome 
                /*               
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative

                //This button should not be present on screen, so must be switched off via method second int parameter.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0,
                INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_250") //"Error!" - Other
                */


                //Badger - Yes, No combo
                IF g_b_ToggleButtonLabels
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative

                ELSE
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                        13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive

                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                        14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                    1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

            ENDIF








        

        



        IF g_CallRequiringPlayerResponse = TRUE //This phone call requires a yes / no response from the player. Let's wait until he responds...

                   


            //Takes care of bug 113398 whereupon the phone was waiting for a response but the MP guys were disabling it.
            IF g_Cellphone.PhoneDS < PDS_MAXIMUM

                g_CallRequiringPlayerResponse = FALSE

                #IF IS_DEBUG_BUILD
                    
                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - Cellphone call was waiting on a player response but it appears that the phone has been disabled or forced away by an incident.")
                    cdPrintnl()

                #endif

                //Respond No?

                
            ELSE

                         
                

                    IF g_3DPhoneNeedsMovedUp = FALSE //Fix for 188709

                        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))  
                        #if IS_DEBUG_BUILD
                            OR g_bFlowAutoplayInProgress = TRUE
                        #endif
                     
                            
                            g_SF_ButtonsSizeX = 0.19
                            
                            g_CallRequiringPlayerResponse = FALSE
                          
                      
                            Play_Select_Beep()

            

                              
                                #if IS_DEBUG_BUILD

                                    cdPrintnl()
                                    cdPrintstring("DIALOG_HAND - Selected Yes at job offer prompt... removing phone but continuing conversation.")
                                    cdPrintnl()
                                
                                #endif

                                g_CellphoneJobOfferResponse = RESPONDED_YES


                                b_NeedtoMovePhoneDownForAcceptedJob = TRUE

                          

                       
                        ENDIF

                    ENDIF
                    



                    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NEGATIVE_INPUT))
                   
                    
        
                                      
                        g_SF_ButtonsSizeX = 0.19


                        g_CallRequiringPlayerResponse = FALSE
                        

                        Play_Select_Beep()



                
                          
                            #IF IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring("DIALOG_HAND - Selected NO at job offer prompt...")
                                cdPrintnl()
                            
                            #endif

                            g_CellphoneJobOfferResponse = RESPONDED_NO

                       
                    ENDIF  
             



                   


            ENDIF // < PDS_MAXIMUM check.


        ELSE




            


            //Bear in mind that Keith calls this immediately after getting the response: CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT(). This may need to be delayed until
            //he is ready to cue a new job offer if problems occur.

            IF g_CellphoneJobOfferResponse = RESPONDED_NO


       
                #if IS_DEBUG_BUILD  
                                                                                        
                    cdPrintnl()              
                    cdPrintstring("DIALOG_HAND - Job call finished by player response. Terminating conversation.")
                    cdPrintnl()
                
                #endif
            

                IF g_Non_Essential_Call_Aborted_and_CS_Already_Free = FALSE //If this has been set to true, then we know the conversation has already been stopped
                                                                            //due to a non-essential call operation.



                    STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation.  As this is a phonecall then a hang up mid-sentence will cut any speaker off at any point.
                    
                                 
                    g_ConversationStatus = CONV_STATE_FINISHED

                ENDIF


                //May need to add specific check for player proactively calling contact here if any timing issue occurs.


                BeforeCallStateHandler ()


                //Scaleform check
                g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 69. Dialogue_Handler assigns BeforeCallDS")
                    cdPrintnl()   
                #endif


                //If this fires off the assert that means the call has malfunctioned - probably because of a dead ped involved in the phonecall.
                Update_Scaleform_VSE ()



            ELSE     //responded yes...



                IF NOT IS_MOBILE_PHONE_CALL_ONGOING()


                    b_NeedtoMovePhoneDownForAcceptedJob = FALSE

                    #if IS_DEBUG_BUILD  
                                                                                            
                        cdPrintnl()              
                        cdPrintstring("DIALOG_HAND - Player responded to job offer, conversation has naturally ended.")
                        cdPrintnl()
                    
                    #endif
                
                    STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation.  As this is a phonecall then a hang up mid-sentence will cut any speaker off at any point.
                    
                                 
                    g_ConversationStatus = CONV_STATE_FINISHED


                    PLAY_SOUND_FRONTEND (-1, "Hang_Up", g_Owner_Soundset)


                    BeforeCallStateHandler ()


                    //Scaleform check
                    g_Cellphone.PhoneDS = BeforeCallPhoneDS //set the phone back to whatever state it was before any call was made

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 70. Dialogue_Handler assigns BeforeCallDS")
                        cdPrintnl()   
                    #endif



                    //If this fires off the assert that means the call has malfunctioned - probably because of a dead ped involved in the phonecall.
                    Update_Scaleform_VSE ()


                    
                       


                ENDIF






            ENDIF




  
    ENDIF


        


ENDPROC

































#if IS_DEBUG_BUILD

PROC DisplayOnscreenConversationInfo()

       
                                                            
    DebugPreviousLineInt = ((GET_CURRENT_SCRIPTED_CONVERSATION_LINE() - 1) + g_DialogueAdjustment)

    IF DebugPreviousLineInt > -1
        
        format_medium_ostext (255, 255, 0, 100) //yellow
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.73, 0.70, "STRING", "Previous Label:")


        format_medium_ostext (255, 255, 0, 100) //yellow
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.8, 0.70, "STRING", g_ConversationLabels[DebugPreviousLineInt])

        DRAW_RECT(0.815, 0.710, 0.172, 0.028, 0, 0, 0, 125)


    ENDIF
    
    
    

    DebugCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()


        IF b_PlayingRandomLine = TRUE

            format_medium_ostext (0, 255, 255, 255) //blue
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.63, 0.77, "STRING", "Random Line Root:")

            format_medium_ostext (0, 255, 255, 255) //blue
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.73, 0.77, "STRING", g_ConversationData.ConversationSegmentToGrab)

            DRAW_RECT(0.731, 0.779, 0.214, 0.031, 0, 0, 0, 125)


        ELSE
            
            IF NOT ARE_STRINGS_EQUAL (DebugCurrentLabel, "NULL")

                format_medium_ostext (0, 255, 0, 255) //green
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.63, 0.77, "STRING", "Current Label:")

                format_medium_ostext (0, 255, 0, 255) //green
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.77, "STRING", DebugCurrentLabel)

                DRAW_RECT(0.710, 0.779, 0.172, 0.028, 0, 0, 0, 125)


            ENDIF

        ENDIF


                                                                                                   

 ENDPROC







PROC DisplayOnscreenProceduralConversationInfo()

       
                                                            
    DebugPreviousLineInt = ((GET_CURRENT_UNRESOLVED_SCRIPTED_CONVERSATION_LINE() - 1) + g_DialogueAdjustment)

    IF DebugPreviousLineInt > -1
        
        format_medium_ostext (255, 255, 0, 100) //yellow
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.70, 0.70, "STRING", "Previous PRC Label:")


        format_medium_ostext (255, 255, 0, 100) //yellow
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.8, 0.70, "STRING", g_ConversationLabels[DebugPreviousLineInt])

        DRAW_RECT(0.815, 0.710, 0.222, 0.028, 0, 0, 0, 125)


    ENDIF
    
    
    

    DebugCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_PROCEDURAL_CONVERSATION_LABEL()


        IF b_PlayingRandomLine = TRUE

            format_medium_ostext (0, 255, 255, 255) //blue
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.63, 0.77, "STRING", "Random Line Root:")

            format_medium_ostext (0, 255, 255, 255) //blue
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.73, 0.77, "STRING", g_ConversationData.ConversationSegmentToGrab)

            DRAW_RECT(0.731, 0.779, 0.254, 0.031, 0, 0, 0, 125)


        ELSE
            
            IF NOT ARE_STRINGS_EQUAL (DebugCurrentLabel, "NULL")

                format_medium_ostext (0, 255, 0, 255) //green
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.60, 0.77, "STRING", "PRC Current Label:")

                format_medium_ostext (0, 255, 0, 255) //green
                DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.77, "STRING", DebugCurrentLabel)

                DRAW_RECT(0.710, 0.779, 0.222, 0.028, 0, 0, 0, 125)


            ENDIF

        ENDIF


                                                                                                   

 ENDPROC











 #endif















SCRIPT



    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()



     
    
    WHILE TRUE

        WAIT(0)
       






        //Check if there has been any change in the "can this call be hung up" stakes from its original "no, it can't" status.
        IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_UPDATE_NEG_BUTTON_PRESENCE)

            IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Disable_Hangup must have been cleared by external script...
        

                //Only re-add end call button if phonecall is ongoing to prevent overwriting other buttons e.g "yes", "no" during a a call intermission.

                IF IS_MOBILE_PHONE_CALL_ONGOING()
                
                    //Re-add the end call button...
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_202") //"END CALL" - Negative

                        IF g_Use_Prologue_Cellphone //No End Call in prologue regardless of labels. #844402
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                        ENDIF



                    ELSE
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            6, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"END CALL" - Negative
                    ENDIF


                    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_UPDATE_NEG_BUTTON_PRESENCE) //Don't want to check inside this loop for the remainder of this call...

                ENDIF
                
            ENDIF

        ENDIF






        IF IS_BIT_SET (BitSet_DialogueHandler, g_BS_STANDARD_CONVERSATION_BUFFER_ACTIVE)

            IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //don't try and play if we are finishing last line of killed conversation...


                IF NOT IS_BIT_SET (BitSet_DialogueHandler, g_BS_STANDARD_CONVERSATION_BUFFER_TIMER_BEGUN)

                    SET_BIT (BitSet_DialogueHandler, g_BS_STANDARD_CONVERSATION_BUFFER_TIMER_BEGUN)

                    IF NETWORK_IS_GAME_IN_PROGRESS()

                        Net_Buffer_StartTime = GET_NETWORK_TIME()
                    
                    ELSE
                    
                        Buffer_StartTime = GET_GAME_TIMER()
                    
                    ENDIF

                ENDIF


                IF NETWORK_IS_GAME_IN_PROGRESS()
                    
                    i_TimeDifference = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Net_Buffer_StartTime)

                ELSE

                    Buffer_CurrentTime = GET_GAME_TIMER()
                    i_TimeDifference = Buffer_CurrentTime - Buffer_StartTime
                
                ENDIF


                IF i_TimeDifference < 3000
           
                    //Note how we are still using the holders here. 
                    IF CREATE_CONVERSATION ( g_ConversationPedsHolderStruct, g_BlockToLoadHolder, g_BufferSegmentToGrab, g_BufferConvPriority, g_AssignDisplaySubtitlesBuffer, g_AssignAddtoBriefScreenBuffer)

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring ("DIALOG_HAND - Buffered Create_Conversation has returned true.")
                            cdPrintnl()
                        
                        #endif


                        BitSet_DialogueHandler = 0 //Buffer conversation has passed back true, no longer need to poll for a free system. Reset all bits.

                    

                    ELSE

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring ("DIALOG_HAND - Trying to play buffered standard conversation, but Create_Conversation has returned false this frame.")
                            cdPrintnl()
                        
                        #endif


                    ENDIF

                ELSE

                    BitSet_DialogueHandler = 0 //Buffer conversation has passed back true, no longer need to poll for a free system. Reset all bits.

        
                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Buffer Time Limit expired! Trying to play buffered standard conversation, but time limit has elapsed.")
                        cdPrintnl()
                    
                    #endif

                    SCRIPT_ASSERT ("Buffered conversation has not cued 3 seconds after conclusion of last conv. Please bug the owner of this mission with console log.")


                ENDIF


            ENDIF
        ENDIF



        IF NOT IS_PLAYER_PLAYING (PLAYER_ID()) 
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
		AND NOT CONTENT_IS_USING_ARENA()
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_mission_controller_2020")) = 0 // This chunk of logic prevented Spectators from heaing dialogue.
		#IF FEATURE_COPS_N_CROOKS
		AND NOT (IS_FREEMODE_ARCADE() AND GET_ARCADE_MODE() = ARC_COPS_CROOKS)
		#ENDIF
            IF g_ConversationStatus <> CONV_STATE_FREE

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 


                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring ("DIALOG_HAND - Player not playing! CONV_STATE not free. Finishing last line?")
                        cdPrintnl()
                    
                    #endif

                    KillAllConversations ()  //Failsafe that will kill any active conversations after phone has been put away and player has died.

                ENDIF

            ENDIF
        ENDIF


        IF b_NeedtoMovePhoneDownForAcceptedJob = TRUE

            MovePhoneOffScreenForAcceptedJobOffer()

        ENDIF



        SWITCH g_ConversationStatus

            CASE CONV_STATE_FREE

                //#IF IS_DEBUG_BUILD

                    //cdPrintstring ("No ongoing conversation.")
                    //cdPrintnl()

                //#endif

            BREAK



            CASE CONV_STATE_BUILDING

                #if IS_DEBUG_BUILD

                    cdPrintnl()
                    cdPrintstring ("DIALOG_HAND - Conversation state has been set to CONV_STATE_BUILDING.")
                    cdPrintnl()

                #endif

                
                IF g_IsThisConversationForPhone //if the conversation is a phonecall, build that specifically...

                    IF g_TotalMultiparts = 0
                         
                        BuildCellphoneConversation()

                    ELSE

                        InitialiseMultipartConversation()

                        BuildCellphoneConversation()

                    ENDIF

                ELSE
                
                    IF g_TotalMultiparts = 0

                        BuildFaceToFaceConversation() //...otherwise it must be a face to face conversation.

                    ELSE
                        
                        //Multipart work
                        InitialiseMultipartConversation()
                        BuildMultipartConversation()

                    ENDIF


                ENDIF
                    
           
            BREAK



            CASE CONV_STATE_WAITING_ON_PLAYER_ACCREJ
    
                WaitForPlayerToAcceptRejectCall()
                
                
                #if IS_DEBUG_BUILD

                    //cdPrintstring ("Waiting for call to be accepted or rejected by player.")
                    //cdPrintnl()

                #endif

            BREAK





            CASE CONV_STATE_PAUSE_ANSWERING_TIME

                PauseForContactAnsweringTime()

            BREAK





            
            CASE CONV_STATE_PLAYING

             

                IF g_IsThisConversationForPhone //if the conversation is a phonecall, play it using specific routine.

                    IF g_IsThisAnMPJobOffer

                        PlayCellphoneJobOfferConversation()

                    ELSE

                        PlayCellphoneConversation()

                        

                        /*
                        #if IS_DEBUG_BUILD

                            IF g_IsThisAnMPChatCall = FALSE

                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.44, "STRING", "CONV_STATE_PLAYING_NORMAL")

                            ELSE

                                DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.44, "STRING", "CONV_STATE_PLAYING_CHATCALL")

                            ENDIF

                        #endif
                        */
                        


                    ENDIF

                ELSE

                    PlayFaceToFaceConversation()

                ENDIF




                #if IS_DEBUG_BUILD

                    IF g_b_DisplayConversationToggle

                        DisplayOnscreenConversationInfo()

                    ENDIF


                    IF g_b_DisplayProceduralConversationToggle

                        DisplayOnscreenProceduralConversationInfo()

                    ENDIF

                #endif


            BREAK
        

            


            CASE CONV_STATE_HANGUPAWAY

                                
                #if IS_DEBUG_BUILD

                    cdPrintstring ("DIALOG_HAND - Player HUNG UP and put away phone by script or DISABLED CELLPHONE, HUAcleanup called from dialogue_private.")
                    cdPrintnl()

                #endif

                ScriptHungupAwayCleanup()

            BREAK





            CASE CONV_STATE_FINISHED
               
            
                #if IS_DEBUG_BUILD

                    cdPrintstring ("DIALOG_HAND - Conversation has finished and ConversationCleanup called from dialogue_private.")
                    cdPrintnl()

                #endif

                ConversationCleanup()


            BREAK




            DEFAULT
                
                #IF IS_DEBUG_BUILD
               
                    SCRIPT_ASSERT ("DIALOG_HAND - Conversation status in a right funny state - see Steve T and make him cry.")

                #endif

            BREAK

        ENDSWITCH


                                                   


        

                                                  
                                                    //Test Block
                                                    
                                                    #IF IS_DEBUG_BUILD
                                                    
                                                    
                                                        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8)
                                                            // ...yes

                                                            WHILE IS_KEYBOARD_KEY_PRESSED(KEY_8)
                                                              WAIT (0)
                                                            ENDWHILE

                                                                                                             
                                                            SWITCH texttestint

                                                                /*
                                                                ENUM eTextMessageIconStyle

                                                                    ICON_STANDARD_TEXTMSG,
                                                                    ICON_REPLAY_TEXTMSG,
                                                                    ICON_SHITSKIP_TEXTMSG

                                                                ENDENUM
                                                                */


                                                                CASE 0


																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO0", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO1", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO2", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO3", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO4", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO5", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO6", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO7", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO8", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO9", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO10", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO11", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "VEX_PM_PHOTO12", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++


                                                                    ENDIF

																	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "VEX_PM_PHOTO13", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        //texttestint++

                                                                    ENDIF
																	
																	
																	
																	
                                                                    //Hyperlink Test!
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "JOSH1_TXT", TXTMSG_LOCKED, TXTMSG_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        texttestint++


                                                                    ENDIF 
                                                                    */


                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "JOSH1_TXT", TXTMSG_LOCKED, TXTMSG_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        texttestint++


                                                                    ENDIF 
                                                                    */


                                                                    /*
                                                                     g_DailyObjectivesList[0] = "CELL_3"
                                                                     g_DailyObjectivesList[1] = "CELL_4"
                                                                     g_DailyObjectivesList[2] = "CELL_5"

                                                                        

                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_CLTEST2", TXTMSG_LOCKED,
                                                                        "NULL", 3, "Verylongtextsendername23456", DAILYOBJ_LIST_COMPONENT) 

                                                                         texttestint ++
                                                                    ENDIF
                                                                    */

                                                                    
                                                                    //Testing the condensed font tag.

																	/*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "LR_PIC_TXT4", TXTMSG_LOCKED, TXTMSG_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        texttestint ++


                                                                    ENDIF
																	*/


                                                                     /*
                                                                    {Test - do not translate please}
                                                                    [CELL_2001]
                                                                    SN: ~a~ will pay $~1~ for this job.

                                                                    {Test - do not translate please}
                                                                    [CELL_2002]
                                                                    S: It's ~a~. I need help! This is a very long text message. It is very long in order to test the limits for TODO 960642. It has been proposed that there should a 200 character limit. ~nrt~So we shall see how that goes.~nrt~

                                                                    {Test - do not translate please}
                                                                    [CELL_2003]
                                                                    N: Need $~1~? Read on.


                                                                    {Test - do not translate please}
                                                                    [CELL_2004]
                                                                    2S: ~a~ wants to talk to ~a~ right now!


                                                                    {Test - do not translate please}
                                                                    [CELL_2005]
                                                                    3S: ~a~ and ~a~ are going to beat the hell outta ~a~!
                                                                    */





                                                                    //STRING_COMPONENT,
                                                                    //NUMBER_COMPONENT,
                                                                    //STRING_AND_NUMBER_COMPONENT,


                                                                    //Tested with Custom Sender - OK! Test_Sender2 with custom first string and custom second string
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2004", EMAIL_UNLOCKED,
                                                                        "FirstString!", -99, "Test_Sender2", STRING_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_REPLY_IS_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER, 1, "SecondString!" ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */
                                               



                                                                    //Tested with Custom Sender  OK! - Three custom strings!
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2005", EMAIL_UNLOCKED,
                                                                        "FirstString!", -99, "Test_Sender2", STRING_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_REPLY_IS_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER, 2, "SecondString!", "ThirdString" ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */


                                                                    //Tested with CHAR_FRANKLIN - OK!  String Component. Charsheet Sender.
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2002", EMAIL_UNLOCKED,
                                                                        "FirstEString!", -99, "TestESender2", STRING_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */



                                                                    //Tested with CHAR_TREVOR -  OK! Number Component. Charsheet Sender.
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_TREVOR, "CELL_2003", EMAIL_UNLOCKED,
                                                                        "FirstEString!", 87, "TestESender2", NUMBER_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_REPLY_IS_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */



                                                                    //Tested with CHAR_DEVIN - OK! String and number component. Charsheet Sender.
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_DEVIN, "CELL_2001", EMAIL_UNLOCKED,
                                                                        "FirstString!", 88, "TestESender2", STRING_AND_NUMBER_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */


                                                                    //Tested with custom Sender - OK! String and number component. Custom Sender.
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2001", EMAIL_UNLOCKED,
                                                                        "FirstString!", 88, "TestESender2", STRING_AND_NUMBER_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */




                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2002", EMAIL_UNLOCKED,
                                                                        "FirstEString!", -99, "TestESender2", STRING_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */
















                                                                                                                                      
                                                                    /*
                                                                    IF SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (CHAR_JIMMY, SILENT_TREVOR, "TXT_102", TXTMSG_LOCKED, TXTMSG_CRITICAL, TXTMSG_DO_NOT_AUTO_UNLOCK,
                                                                        REPLY_IS_REQUIRED, ICON_REPLAY_TEXTMSG, FALSE)
                                                                        texttestint++
                                                                    ENDIF
                                                                    */


                                                                    //SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_LESTER, "TXT_102", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL)

                                                                    //texttestint++


                                                                    //TXT_107


                                                                    //IF SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (CHAR_LESTER, SILENT_FRANKLIN,"ASS1_MIS",TXTMSG_UNLOCKED,TXTMSG_NOT_CRITICAL,TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,ICON_STANDARD_TEXTMSG,FALSE,NO_BARTER_REQUIRED,CAN_CALL_SENDER)


                                                                         //texttestint --
                                                                    //ENDIF





                              








                                                                    //WAIT (0)
                                                                    /*
                                                                    IF SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (CHAR_LESTER, SILENT_FRANKLIN,"ASS1_MIS",TXTMSG_UNLOCKED,TXTMSG_NOT_CRITICAL,TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,ICON_STANDARD_TEXTMSG,FALSE,NO_BARTER_REQUIRED,CAN_CALL_SENDER)


                                                                         //texttestint --
                                                                    ENDIF
                                                                    */



                                                                     //July 2013            
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (CHAR_LESTER, "SOL2_PASS", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        texttestint++


                                                                    ENDIF 
                                                                    */
                                                                    

                                                                    //New email tests.
                                                                    
                                                                    /*                                                                
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "SXT_INF_STP", EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ,
                                                                    MPE_REPLY_IS_REQUIRED)


                                                                      
                                                                        cdPrintnl()
                                                                        cdPrintstring("testing new email 1")
                                                                        cdPrintnl()
                                                                        texttestint++


                                                                    ENDIF                                                                    
                                                                    */


                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2001", TXTMSG_LOCKED,
                                                                        "DaveTest", 1974, "TestSender2", STRING_AND_NUMBER_COMPONENT, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ,  
                                                                        REPLY_IS_REQUIRED, ICON_STANDARD_TEXTMSG, FALSE)
           
 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_LESTER, "CELL_2002", TXTMSG_LOCKED,
                                                                        "CarName", -99, "TestSender2", STRING_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

    
                                                                    /* Broken!
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP", TXTMSG_UNLOCKED, 
                                                                    "DM_TI0314", NO_INT_SUBSTRING_COMPONENT_VALUE, "", STRING_COMPONENT)

                                                                        texttestint++

                                                                    ENDIF
                                                                    */





                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_BARRY, "CELL_2002", TXTMSG_LOCKED,
                                                                        "Stevie", -99, "TestSender2", STRING_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                    
                                                                    /*        
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "SXT_JUL_1ST", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_DO_NOT_AUTO_UNLOCK, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                       


                                                                    ENDIF


                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "SXT_NIK_1ST", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_DO_NOT_AUTO_UNLOCK, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                        texttestint ++

                                                                    ENDIF
                                                                    */


                                                                    /*Working.
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2002", TXTMSG_LOCKED,
                                                                        "Stevie", -99, "TestSender2", STRING_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                
                                                                BREAK


                              

                                                                CASE 1

                                                                    //SXT_JUL_1ST
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_MICHAEL, "CELL_2001", TXTMSG_LOCKED,
                                                                        "Stretch", -99, "TestSender4", STRING_COMPONENT) 

                                                                    //IF SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (CHAR_LESTER, SILENT_FRANKLIN,"ASS1_MIS",TXTMSG_UNLOCKED,TXTMSG_NOT_CRITICAL,TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,ICON_STANDARD_TEXTMSG,FALSE,NO_BARTER_REQUIRED,CAN_CALL_SENDER)


                                                                         texttestint --
                                                                    ENDIF
                                                                    */
                                                                                                                                          
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_JIMMY, "LR_PIC_TXT5", TXTMSG_LOCKED, TXTMSG_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
                                                                    
                                                                        texttestint --


                                                                    ENDIF

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (CHAR_BIKESITE_PAMC, "MGTNS_U", TXTMSG_LOCKED, TXTMSG_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, REPLY_IS_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                        texttestint ++


                                                                    ENDIF 
                                                                    */
                                                                    
                                                                    /*                                                                
                                                                    g_CarListMessageDecision = UPDATED_CAR_LIST
                                                                    
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_ABIGAIL, "CELL_CLTEST2", TXTMSG_LOCKED,
                                                                        "NULL", 8, "TestSender1", CAR_LIST_COMPONENT) 

                                                                         texttestint --
                                                                    ENDIF
                                                                    */

                                                                    
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "TXT_104", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, 
                                                                        NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CANNOT_CALL_SENDER)
                                                                        texttestint++
                                                                    ENDIF   
                                                                    */









                                                                    //Email test.

                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "CELL_EMAIL1", EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_DO_NOT_AUTO_UNLOCK,
                                                                        MPE_REPLY_IS_REQUIRED)


                                                                      
                                                                        cdPrintnl()
                                                                        cdPrintstring("testing new email with reply required DL 1")
                                                                        cdPrintnl()
                                                                        texttestint++


                                                                    ENDIF  

                                                                    */









                                                                    //1 string.
                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2002", EMAIL_UNLOCKED,
                                                                        "FirstEString!", -99, "TestESender2", STRING_COMPONENT, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, MPE_NO_BARTER_REQUIRED, MPE_CANNOT_CALL_SENDER ) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                           


                                                                    //NO_CHARACTER



                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2005", TXTMSG_UNLOCKED,
                                                                        "", -99, "TestSender2", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, REPLY_IS_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CAN_CALL_SENDER, 2, "SecondString", "ThirdString") 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */
                                                                                              

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "MIS_CUST_TXT2", TXTMSG_UNLOCKED,
                                                                        "", -99, "KM_Sender2", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_DO_NOT_AUTO_UNLOCK, REPLY_IS_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CAN_CALL_SENDER, 1, "This is a very long SecondString") 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */


                                                                    /*
                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "SXT_NIK_STP", EMAIL_LOCKED, EMAIL_CRITICAL, EMAIL_DO_NOT_AUTO_UNLOCK,
                                                                    MPE_REPLY_IS_REQUIRED, ICON_REPLAY_TEXTMSG)


                                                                      
                                                                        cdPrintnl()
                                                                        cdPrintstring("testing new email 2")
                                                                        cdPrintnl()
                                                                        texttestint--


                                                                    ENDIF                                                                    
                                                                    */






                                                                    
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "CARAPP_NEW", TXTMSG_UNLOCKED, TXTMSG_CRITICAL)
                                                                       
                                                                        texttestint++
                                                                    ENDIF 
                                                                    */

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "CELL_2001", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL)
                                                                       
                                                                        texttestint++
                                                                    ENDIF
                                                                    */

//                                                                    
//                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_MP_ARMY_CONTACT, "CELL_2001", TXTMSG_UNLOCKED,
//                                                                        "DaveTest", 1974, "TestSender2", STRING_AND_NUMBER_COMPONENT)
//           
// 
//
//                                                                         texttestint++
//                                                                    ENDIF
//                                                                                                                                               
                                                                BREAK



                                                                /*
                                                                CASE 1
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "TXT_102", TXTMSG_LOCKED, TXTMSG_CRITICAL)
                                                                        texttestint++
                                                                    ENDIF   
                                                                BREAK
                                                                */

                                                                CASE 2


                                                                    IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "CELL_EMAIL2", EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_DO_NOT_AUTO_UNLOCK,
                                                                    MPE_NO_REPLY_REQUIRED)


                                                                      
                                                                        cdPrintnl()
                                                                        cdPrintstring("testing new email DLNJan")
                                                                        cdPrintnl()
                                                                        texttestint--

                                                                    ENDIF



                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_DEVIN, "STOCK_TEST", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                        texttestint = 0


                                                                    ENDIF
                                                                    */
                                                                    


                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_HAO, "SXT_NIK_NEED", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                        texttestint ++


                                                                    ENDIF
                                                                    */

                                                                    /*
                                                                    IF SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (CHAR_TANISHA, SILENT_TREVOR, "TXT_103", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_DO_NOT_AUTO_UNLOCK,
                                                                        REPLY_IS_REQUIRED, ICON_REPLAY_TEXTMSG, FALSE)
                                                                        texttestint++
                                                                    ENDIF
                                                                    */

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2004", TXTMSG_UNLOCKED,
                                                                        "FirstString", -99, "TestSender2", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CAN_CALL_SENDER, 1, "SecondString") 

                                                                         texttestint++
                                                                    ENDIF




                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_TREVOR, "CELL_CLTEST1", TXTMSG_LOCKED,
                                                                        "NULL", 9, "TestSender1", CAR_LIST_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */
                                                                    

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (CHAR_MICHAEL, "TXT_106", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                    NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG, FALSE)
                                                                    
                                                                        texttestint++


                                                                    ENDIF 
                                                                    */




                                                                    //SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER (CHAR_FRANKLIN, CHAR_LESTER, "TXT_102", TXTMSG_LOCKED, TXTMSG_CRITICAL)



                   
                                                                    

//                                                                    IF SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (CHAR_LESTER, "TXT_107", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL,
//                                                       `             
//                                                                        TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
//                                                                    
//                                                                        texttestint++
//
//
//                                                                    ENDIF 

                                                                    //SCRIPT_ASSERT("text")

                                                                    //REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(CT_END_OF_MISSION, BIT_TREVOR, CHAR_JOSH, "TXT_108", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, 10000)



                                                                    /* October 2012
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_FRANKLIN, "CELL_2002", TXTMSG_LOCKED,
                                                                        "Stevie", -99, "TestSender2", STRING_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */


                                                                   
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (CHAR_TREVOR, "TXT_105", TXTMSG_UNLOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                    */

                                                                BREAK


                                                                CASE 3
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_HITCHER_GIRL,  "TXT_103", TXTMSG_LOCKED, TXTMSG_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, REPLY_IS_REQUIRED, ICON_REPLAY_TEXTMSG)
                                                                        texttestint++
                                                                    ENDIF
                                                                    */
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2001", TXTMSG_LOCKED,
                                                                        "DaveTest", 1974, "TestSender2", STRING_AND_NUMBER_COMPONENT)
           
 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_LESTER, "CELL_2005", TXTMSG_UNLOCKED,
                                                                        "FirstString", -99, "TestSender2", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CAN_CALL_SENDER, 2, "SecondString", "ThirdString") 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_TREVOR, "SXT_CHA_NEED", TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL,
                                                                        TXTMSG_DO_NOT_AUTO_UNLOCK, NO_REPLY_REQUIRED, ICON_SHITSKIP_TEXTMSG)
                                                                    
                                                                        texttestint ++


                                                                    ENDIF








                                                                BREAK


                                                                CASE 4
                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_LESTER,  "STNTP_U", TXTMSG_LOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                    */
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (NO_CHARACTER, "CELL_2005", TXTMSG_UNLOCKED,
                                                                        "FirstString!", -99, "TestSenderName", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED,
                                                                        ICON_STANDARD_TEXTMSG, TRUE, NO_BARTER_REQUIRED, CAN_CALL_SENDER, 2, "Number Two", "Number Three") 

                                                                         texttestint++
                                                                    ENDIF


                                                                    /*
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (CHAR_LESTER, "CELL_CLTEST1", TXTMSG_LOCKED,
                                                                        "NULL", 9, "TestSender1", CAR_LIST_COMPONENT) 

                                                                         texttestint++
                                                                    ENDIF
                                                                    */

                                                                BREAK
                                                                
                                                      


                                                                CASE 5
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_RON, "TXT_106", TXTMSG_LOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK




                                                                
                                                                CASE 6
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_STEVE,  "TXT_106", TXTMSG_LOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK


                                                                CASE 7
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_WADE,  "TXT_101", TXTMSG_LOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK


                                                                CASE 8
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_LESTER,  "TXT_102", TXTMSG_UNLOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK



                                                                CASE 9
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL,  "TXT_105", TXTMSG_UNLOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK

                                                                
                                                                CASE 10                                        
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_FRANKLIN, "TXT_106", TXTMSG_UNLOCKED)
                                                                        texttestint = 0
                                                                    ENDIF
                                                                BREAK


                                                                CASE 11
                                                                    
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_TREVOR,  "TXT_103", TXTMSG_UNLOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK


                                                                CASE 12
                                                                    IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_LESTER,  "TXT_104", TXTMSG_UNLOCKED)
                                                                        texttestint++
                                                                    ENDIF
                                                                BREAK

                                                                /*
                                                                [TXT_101:TMSG]
                                                                This is an example text message.


                                                                [TXT_102:TMSG]
                                                                Hey, how are you doing? Thought I'd drop you a text!


                                                                [TXT_103:TMSG]
                                                                Get lost, you bawbag! If I ever catch you round here again!


                                                                [TXT_104:TMSG]
                                                                What's happening? Fancy meeting up?                                                                                                      


                                                                [TXT_105:TMSG]
                                                                Do you want to replay this mission?


                                                                [TXT_106:TMSG]
                                                                Boo! Hope you got that job done okay! Give me a call soon.
                                                                */



                                                            ENDSWITCH

                                                                                                                      
                                                            IF texttestint > 12

                                                                texttestint = 0

                                                            ENDIF

                                                            
                                                            
                                                            //HIDE_PHONE_FOR_HOTSWAP (TRUE)

                                                            //CHANGE_CALLING_SCREEN_CONTACT_NAMES (CHAR_TREVOR)
                                                            
                                                            //Put_Cellphone_To_Ear()
                        
                                                            //SET_PHONE_TO_SLEEP(TRUE)
                                                                                                                                 
                                                        ENDIF



                                                        
                                                        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
                                                            // ...yes

                                                            WHILE IS_KEYBOARD_KEY_PRESSED(KEY_9)
                                                              WAIT (0)
                                                            ENDWHILE

                                                            //SET_PHONE_TO_SLEEP (TRUE)
                                                            //HANG_UP_AND_PUT_AWAY_PHONE()

                                                            //KILL_FACE_TO_FACE_CONVERSATION()

                                                            //g_ConversationPaused = TRUE //Pause conversation
                                                                        
                                                            //PAUSE_SCRIPTED_CONVERSATION (FALSE)

                                                            //PAUSE_FACE_TO_FACE_CONVERSATION (FALSE)

                                                            //MAKE_CONTACT_ENTRY_PRIORITY (CHAR_MICHAEL)
                                                            //MAKE_CONTACT_ENTRY_PRIORITY (CHAR_TREVOR)

                                                            //g_TextMessage[g_CurrentNumberOfTextsStored].TxtMsgTimeSent.TxtMsgHours = 12

                                                            //TxtMsgComparisonTime = g_TextMessage.TxtMsgTimeSent

                                                            //REQUEST_ADDITIONAL_TEXT ("TMSG", PHONE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
                                                            //WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (PHONE_TEXT_SLOT)  //the permanent global from the temporary holder.
                                                                //WAIT(0)
                                                            //ENDWHILE

                                                            //CREATE_CONVERSATION (MyLocalPedStruct, "S1M2AUD", "S1M2_IE", CONV_PRIORITY_VERY_HIGH) //This priority should default to medium as a precaution.
                                                            
                                                            
                                                            //CREATE_MOBILE_PHONE(MOBILE_1)


                                                            SWITCH MPtexttestint


                                                                CASE 0
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_MICHAEL, "MPJ_100", JOB_AVAILABLE)
                                                                        
                                                                        //cdPrintnl()
                                                                        //cdPrintstring("Testing MPJ 101")
                                                                        //cdPrintnl()
                                                                        MPtexttestint++


                                                                    //ENDIF
                                                                
                                                                BREAK

                                                                
                                                                CASE 1
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_FRANKLIN, "MPJ_101", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 2
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_TREVOR, "MPJ_102", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK


                                                                CASE 3
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_MICHAEL, "MPJ_103", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 4
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_LESTER, "MPJ_104", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK



                                                                CASE 5
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_FRANKLIN, "MPJ_105", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 6
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_MICHAEL, "MPJ_106", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK


                                                                CASE 7
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_TREVOR, "MPJ_107", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 8
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_FRANKLIN, "MPJ_108", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK


                                                                CASE 9
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_LESTER, "MPJ_109", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 10
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_MICHAEL, "MPJ_110", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK


                                                                CASE 11
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_TREVOR, "MPJ_111", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF   
                                                                BREAK


                                                                CASE 12
                                                                    //IF SEND_NEW_MULTIPLAYER_JOB (CHAR_FRANKLIN, "MPJ_112", JOB_AVAILABLE)
                                                                        MPtexttestint++
                                                                    //ENDIF
                                                                BREAK


                                                                /*
                                                                [TXT_101:TMSG]
                                                                This is an example text message.


                                                                [TXT_102:TMSG]
                                                                Hey, how are you doing? Thought I'd drop you a text!


                                                                [TXT_103:TMSG]
                                                                Get lost, you bawbag! If I ever catch you round here again!


                                                                [TXT_104:TMSG]
                                                                What's happening? Fancy meeting up?                                                                                                      


                                                                [TXT_105:TMSG]
                                                                Do you want to replay this mission?


                                                                [TXT_106:TMSG]
                                                                Boo! Hope you got that job done okay! Give me a call soon.
                                                                */



                                                            ENDSWITCH
                                                            

                                                                                                                      
                                                            IF MPtexttestint > 12

                                                                MPtexttestint = 0

                                                            ENDIF

                                                            
                                                            
                                                            //HIDE_PHONE_FOR_HOTSWAP (TRUE)

                                                            //CHANGE_CALLING_SCREEN_CONTACT_NAMES (CHAR_TREVOR)
                                                            
                                                            //Put_Cellphone_To_Ear()
                        
                                                            //SET_PHONE_TO_SLEEP(TRUE)
                                                                                                                                 
                                                        ENDIF


































                                                                                                                                  

                                                        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)


                                            
                                                            




                                                            // ...yes

                                                     
                                                            //SET_PHONE_TO_SLEEP (TRUE)
                                                            //HANG_UP_AND_PUT_AWAY_PHONE()

                                                            //KILL_FACE_TO_FACE_CONVERSATION()

                                                            //g_ConversationPaused = FALSE //Restart conversation

                                                            //PAUSE_FACE_TO_FACE_CONVERSATION (TRUE)
                                                                        
                                                            //PAUSE_SCRIPTED_CONVERSATION (FALSE)

                        
                                                            //MAKE_CONTACT_ENTRY_ALPHA (CHAR_MICHAEL)
                                                            //MAKE_CONTACT_ENTRY_ALPHA (CHAR_TREVOR)

                                                                        
                                                            //DISPLAY_TEXT (0.5, 0.5, "TXT_101")
                                                             //HIDE_PHONE_FOR_HOTSWAP (FALSE)


                                                            //Remove_Cellphone_From_Ear()

                                                            //CHAR_CALL_PLAYER_CELLPHONE (CHAR_LESTER, "EMSAUD", "EM_CALLQ", CONV_PRIORITY_VERY_HIGH)


                                                            //PLAYER_CALL_EMERGENCY_SERVICES (CHAR_CALL911, "EMSAUD", "EM_CALLQ", CONV_PRIORITY_VERY_HIGH, "CELL_601", "EM_CALLA", "EM_CALLF", "EM_CALLP")
                                         


                                                            //SET_PHONE_TO_SLEEP(TRUE)

                                                           
                                                            //ADD_CONTACT_TO_PHONEBOOK (CHAR_DETONATEPHONE, ALL_OWNERS_BOOKS, TRUE)

                                                            //DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER() 

                                                            //DELETE_ALL_LOCKED_TEXT_MESSAGES_FROM_CURRENT_PLAYER()
                                                              
                                                            //UNLOCK_TEXT_MESSAGE_BY_LABEL ("TXT_101")  
                                                                                                                                 
                                                        ENDIF

                                                        



                                                        /*

                                                        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
                                                            // ...yes

                                                            WHILE IS_KEYBOARD_KEY_PRESSED(KEY_9)
                                                              WAIT (0)
                                                            ENDWHILE


                                                            CHAR_CALL_PLAYER_CELLPHONE(CHAR_LESTER, "H1AUD", "HST1_CALL3AY", CONV_PRIORITY_VERY_HIGH, DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)

                                                            //CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES(CHAR_ARIANO, "HST1_CALL3A", CONV_PRIORITY_VERY_HIGH, "CALL_H0_1", "HST1_CALL3AY", "HST1_CALL3AN")

                                                            //CREATE_CONVERSATION ("S1M2AUD", "S1M2_IE", CONV_PRIORITY_VERY_HIGH) //This priority should default to medium as a precaution.
                                                            
                                                            //PLAY_SINGLE_LINE_FROM_CONVERSATION ("S1M2AUD", "S1M2_IE_3", CONV_PRIORITY_VERY_HIGH) //I'm with Lorenzo..

                                                            //DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER ("TXT_101")


                                                        ENDIF



                                                        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0)
                                                            // ...yes

                                                            WHILE IS_KEYBOARD_KEY_PRESSED(KEY_0)
                                                              WAIT (0)
                                                            ENDWHILE


                                                            //CHAR_CALL_PLAYER_CELLPHONE(CHAR_LESTER, "HST1_CALL3AY", CONV_PRIORITY_VERY_HIGH)

                                                            //WHILE NOT CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES(CHAR_ARIANO, "HST1_CALL3A", CONV_PRIORITY_MEDIUM, "CALL_H0_1", "HST1_CALL3AY", "HST1_CALL3AN")

                                                                //CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES_FORCE_ANSWER (CHAR_ARIANO, "H1AUD", "HST1_CALL3A", CONV_PRIORITY_MEDIUM, "CALL_H0_1", "HST1_CALL3AY", "HST1_CALL3AN")
                                                                
                                                                PLAYER_MAKE_CONFERENCE_CALL (CHAR_ARIANO, CHAR_MICHAEL, "H1AUD", "HST1_CALL5A", CONV_PRIORITY_VERY_HIGH)

                                                                //PLAYER_CALL_CHAR_CELLPHONE_WITH_REPLIES (CHAR_ARIANO, "H1AUD", "HST1_CALL3A", CONV_PRIORITY_MEDIUM, "CALL_H0_1", "HST1_CALL3AY", "HST1_CALL3AN")

                                                                //START_SCRIPT_CONVERSATION (FALSE, FALSE)

                                                            //ENDWHILE

                                                        ENDIF
                                                        */

                                                      
                                                            
                                                     

                                                        
                                                        
                                                        /*          
                                                        IF CHECK_RESPONSE_TO_CELLPHONE_PROMPT() = RESPONDED_YES
                                                        ENDIF





                                                        SWITCH CHECK_RESPONSE_TO_CELLPHONE_PROMPT()
                                                        
                                                              CASE RESPONSE_STORE_EMPTY
                                                                cdPrintstring("empty answer store")
                                                                cdPrintnl()
                                                              BREAK

                                                              CASE RESPONDED_YES
                                                                cdPrintstring("YES answer stored")
                                                                cdPrintnl()
                                                              BREAK

                                                              CASE RESPONDED_NO
                                                                cdPrintstring("NO answer stored")
                                                                cdPrintnl()
                                                              BREAK



                                                        ENDSWITCH
                                                        */

                                                        /*
                                                        IF IS_CALLING_CONTACT (CHAR_LESTER)

                                                            cdPrintstring("Calling Lester")
                                                            cdPrintnl()

                                                        ENDIF


                                                        IF IS_CALLING_CONTACT (CHAR_ARIANO)

                                                            cdPrintstring("Calling Ariano")
                                                            cdPrintnl()

                                                        ENDIF


                                                        IF IS_CALLING_CONTACT (CHAR_MICHAEL)

                                                            cdPrintstring("Calling Michael")
                                                            cdPrintnl()

                                                        ENDIF



                                                        IF IS_CALLING_CONTACT (CHAR_TREVOR)

                                                            cdPrintstring("Calling Trevor")
                                                            cdPrintnl()

                                                        ENDIF
                                                        */


                                                                                                                                         
                                                    //ENDIF
                                                    //End of Test block
                                                    #endif
                                                    






        

        
             

            
    ENDWHILE
    
    
ENDSCRIPT


