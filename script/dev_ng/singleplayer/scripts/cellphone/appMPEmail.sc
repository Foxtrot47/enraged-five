USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "stack_sizes.sch"
USING "MP_globals_COMMON_TU.sch"
USING "cellphone_movement.sch"




  
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   AppMPEmail.sc 
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Takes the array of MP Emails stored in the global array
//                          and places them in the scaleform slots in chronological order.
//                          
//                          Original email bug was 1627004 
//                
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************






INT MessageListSlot[MAX_MP_EMAILS]

INT NumberOfMessagesInList = 0
INT NumberOfFlowMessagesInList = 0

INT ListCursorIndex = 0
INT i_FullListSlotToReturnTo = 0
INT SingleMessageIndex, MessageReadDecider

BOOL ThisMessageContainsHyperlink = FALSE


BOOL dpad_scroll_pause_cued = FALSE

BOOL b_SafetyValveFired = FALSE
INT sf_TempInt = 0


BOOL b_RepeatButtonLimiter = FALSE
INT RepeatButtonLimiter_StartTime, RepeatButtonLimiter_CurrentTime

SCALEFORM_RETURN_INDEX Choice_ReturnedSFIndex


CONST_INT c_NumberOfTXD_Messages 59

TEXT_LABEL_63 CurrentEmailMessageTXD
TEXT_LABEL_23 EmailMessagesThatRequireTXDs_LABEL[c_NumberOfTXD_Messages]
TEXT_LABEL_63 EmailMessagesThatRequireTXDs_STRING[c_NumberOfTXD_Messages]

BOOL b_TXD_Load_Required = FALSE



VECTOR v_3dEmailDesiredPosition, v_3dEmailOriginalPosition


INT i_LatestPosixTime //Holds a copy of the freshest marketing email's posix time which we can use to update the stat once the message has actually been read.

//Would check the email banner ad into as requested in url:bugstar:2551529 to determine which sender name to use.
//STRING ThisSenderName = "TestSenderString"//Usually Rockstar Games Social Club


//enumPhonebookPresence PersonalPhonebookToCheck  //we need to check which character we are using and store which phonebook we should base the contacts list on.


#if IS_DEBUG_BUILD

    INT MessagesToDraw = 0




    FLOAT listOriginX = 0.3//0.72
    FLOAT listOriginY = 0.42


    FLOAT drawItemX = 0.0
    FLOAT drawItemY = 0.0

#endif








//Connection length and answerphone block - now moved to cellphone_globals.sch
/*
INT Connection_EngagedPauseLength = 4000
INT Connection_AnsMsgPauseLength = 7000
STRING answerPhoneBlock = "H1AUD"
*/





//New work to retrieve SC Emails from server for marketing purposes. Requested by 2301166
BOOL b_Priority_RetrievalOngoing = FALSE
BOOL b_Priority_SC_Retrieval_Succeeded = FALSE
eEMAIL_RETRIEVAL_STATUS Priority_SC_CurrentRetrievalStatus 


INT i_Num_of_Marketing_Emails_Retrieved = 0


//Struct used to receive email data from a single ID 
scrEmail_Data  single_email_Received_Data






//New work to retrieve SC Emails from server for marketing purposes. Requested by 2301166

PROC Retrieve_Priority_SC_Marketing_Emails()


/*
    ENUM LANGUAGE_TYPE
    LANGUAGE_UNDEFINED = -1,
    LANGUAGE_ENGLISH = 0,
    LANGUAGE_FRENCH,
    LANGUAGE_GERMAN,
    LANGUAGE_ITALIAN,
    LANGUAGE_SPANISH,
    LANGUAGE_PORTUGUESE,
    LANGUAGE_POLISH,
    LANGUAGE_RUSSIAN,
    LANGUAGE_KOREAN,
    LANGUAGE_CHINESE,
    LANGUAGE_JAPANESE,
    LANGUAGE_MEXICAN
    ENDENUM


    "gta5mkt_en", "gta5mkt_fr", "gta5mkt_ge", "gta5mkt_it", "gta5mkt_sp", "gta5mkt_pt", "gta5mkt_pl", "gta5mkt_ru", "gta5mkt_ko", "gta5mkt_ch", "gta5mkt_ja", "gta5mkt_me".
	
	"gta5mkt_cn" added in bug 3790944 for Simplified Chinese support.


 */

    //2563312  - We need to support localisation tags now.
    SWITCH GET_CURRENT_LANGUAGE()


        CASE LANGUAGE_UNDEFINED
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_en")
        BREAK

        CASE LANGUAGE_ENGLISH
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_en")
        BREAK

        CASE LANGUAGE_FRENCH
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_fr")
        BREAK

        CASE LANGUAGE_GERMAN
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ge")
        BREAK

        CASE LANGUAGE_ITALIAN
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_it")
        BREAK

        CASE LANGUAGE_SPANISH
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_sp")
        BREAK

        CASE LANGUAGE_PORTUGUESE
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_pt")
        BREAK

        CASE LANGUAGE_POLISH
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_pl")
        BREAK

        CASE LANGUAGE_RUSSIAN
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ru")
        BREAK

        CASE LANGUAGE_KOREAN
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ko")
        BREAK

        CASE LANGUAGE_CHINESE
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ch")
        BREAK

        CASE LANGUAGE_JAPANESE
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_ja")
        BREAK

        CASE LANGUAGE_MEXICAN
            SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_me")
        BREAK
		
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			 SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5mkt_cn")	//New tag provided in bug 3790944.
		BREAK

    ENDSWITCH
    
    //Now replaced by local language tag above.
    //SC_EMAIL_SET_CURRENT_EMAIL_TAG("gta5marketing")  //Critical! Specify that we only want to retrieve marketing tagged emails rather than Eyefind messages.
    
    
    SC_EMAIL_RETRIEVE_EMAILS (0, 6)


    WAIT (0)

    b_Priority_RetrievalOngoing = TRUE
    b_Priority_SC_Retrieval_Succeeded = FALSE
    


   
    
    WHILE b_Priority_RetrievalOngoing
    AND g_Cellphone.PhoneDS > PDS_AWAY

        WAIT(0)


        Priority_SC_CurrentRetrievalStatus = SC_EMAIL_GET_RETRIEVAL_STATUS()

            


        IF CHECK_FOR_APPLICATION_EXIT() //will only terminate if this script is in PDS_RUNNINGAPP rather than PDS_COMPLEXAPP state.
        
            #if IS_DEBUG_BUILD
                cdPrintstring("AppMPEmail - Check_For_Application_Exit returning TRUE during marketing retrieval process. Will cue PDS_AWAY")
                cdPrintstring("STATE ASSIGNMENT 2488969693. AppMPEmail assigns PDS_AWAY")
                cdPrintnl()
            #endif



            g_Cellphone.PhoneDS = PDS_AWAY
            //Cleanup_and_Terminate()


            b_Priority_RetrievalOngoing = FALSE
            b_Priority_SC_Retrieval_Succeeded = FALSE


        ENDIF




        SWITCH Priority_SC_CurrentRetrievalStatus


            
            //Retrieval Processed Finished, set EmailState to next section.   THIS IS NOT GETTING HIT FROM CODE.
            CASE EMAIL_RETRIEVAL_SUCCEEDED

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appMPemail - Finished retrieving Marketing email batch.")
                    cdPrintnl()
                #endif

                b_Priority_RetrievalOngoing = FALSE
                b_Priority_SC_Retrieval_Succeeded = TRUE

            BREAK



            CASE EMAIL_RETRIEVAL_NONE

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appMPemail - MARKETING EMAIL RETRIEVAL NONE - Should not see this spam repeatedly! Report to Steve T! Retrieved ")
                    cdPrintnl()
                    cdPrintint(SC_EMAIL_GET_NUM_RETRIEVED_EMAILS())
                #endif

                b_Priority_RetrievalOngoing = FALSE
                b_Priority_SC_Retrieval_Succeeded = TRUE

            BREAK



            CASE EMAIL_RETRIEVAL_PENDING

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appMPemail - MARKETING EMAIL RETRIEVAL PENDING...")
                    cdPrintnl()
                #endif

            BREAK



            CASE EMAIL_RETRIEVAL_CANCELLED

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appMPemail - MARKETING EMAIL RETRIEVAL CANCELLED!")
                    cdPrintnl()
                #endif

                b_Priority_RetrievalOngoing = FALSE
                b_Priority_SC_Retrieval_Succeeded = FALSE

            BREAK



            //Failed! Bail out!
            CASE EMAIL_RETRIEVAL_FAILED

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("appMPemail - MARKETING EMAIL RETRIEVAL FAILED!")
                    cdPrintnl()
                #endif

                b_Priority_RetrievalOngoing = FALSE
                b_Priority_SC_Retrieval_Succeeded = FALSE

            BREAK






        ENDSWITCH


    ENDWHILE



    IF b_Priority_SC_Retrieval_Succeeded

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("appMPemail - Social Club Marketing email retrieval SUCCESS! Marketing emails will be present in the first few positions of the inbox")
            cdPrintnl()
        #endif


        i_Num_of_Marketing_Emails_Retrieved = SC_EMAIL_GET_NUM_RETRIEVED_EMAILS()


    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("appMPemail - Social Club Marketing email retrieval did not succeed. No marketing emails will be present in the inbox.")
            cdPrintnl()
        #endif


        i_Num_of_Marketing_Emails_Retrieved = 0


    ENDIF

        

ENDPROC










PROC Set_Up_Messages_TXD_Relationship()

    

    EmailMessagesThatRequireTXDs_LABEL[0] = "SXT_JUL_1ST" 
    EmailMessagesThatRequireTXDs_STRING[0] = "05_a_sext_stripperJuliet" 

    EmailMessagesThatRequireTXDs_LABEL[1] = "SXT_JUL_2ND" 
    EmailMessagesThatRequireTXDs_STRING[1] = "05_b_sext_stripperJuliet" 

    EmailMessagesThatRequireTXDs_LABEL[2] = "SXT_JUL_NEED" 
    EmailMessagesThatRequireTXDs_STRING[2] = "05_c_sext_stripperJuliet" 

    EmailMessagesThatRequireTXDs_LABEL[3] = "MAPEMAIL_TXT" 
    EmailMessagesThatRequireTXDs_STRING[3] = "NHP_PHOTO_19" 

    EmailMessagesThatRequireTXDs_LABEL[4] = "SXT_NIK_2ND" 
    EmailMessagesThatRequireTXDs_STRING[4] = "06_b_sext_stripperNikki" 

    EmailMessagesThatRequireTXDs_LABEL[5] = "SXT_NIK_NEED" 
    EmailMessagesThatRequireTXDs_STRING[5] = "06_c_sext_stripperNikki" 

    EmailMessagesThatRequireTXDs_LABEL[6] = "SXT_SAP_1ST" 
    EmailMessagesThatRequireTXDs_STRING[6] = "08_a_sext_stripperSapphire" 

    EmailMessagesThatRequireTXDs_LABEL[7] = "SXT_SAP_2ND" 
    EmailMessagesThatRequireTXDs_STRING[7] = "08_b_sext_stripperSapphire" 

    EmailMessagesThatRequireTXDs_LABEL[8] = "SXT_SAP_NEED" 
    EmailMessagesThatRequireTXDs_STRING[8] = "08_c_sext_stripperSapphire" 


    EmailMessagesThatRequireTXDs_LABEL[9] = "SXT_INF_1ST" 
    EmailMessagesThatRequireTXDs_STRING[9] = "04_a_sext_stripperInfernus" 

    EmailMessagesThatRequireTXDs_LABEL[10] = "SXT_INF_2ND" 
    EmailMessagesThatRequireTXDs_STRING[10] = "04_b_sext_stripperInfernus" 

    EmailMessagesThatRequireTXDs_LABEL[11] = "SXT_INF_NEED" 
    EmailMessagesThatRequireTXDs_STRING[11] = "04_c_sext_stripperInfernus" 

    EmailMessagesThatRequireTXDs_LABEL[12] = "SXT_TXI_1ST" 
    EmailMessagesThatRequireTXDs_STRING[12] = "11_a_sext_taxiLiz" 

    EmailMessagesThatRequireTXDs_LABEL[13] = "SXT_TXI_2ND" 
    EmailMessagesThatRequireTXDs_STRING[13] = "11_b_sext_taxiLiz" 

    EmailMessagesThatRequireTXDs_LABEL[14] = "SXT_TXI_NEED" 
    EmailMessagesThatRequireTXDs_STRING[14] = "11_c_sext_taxiLiz" 

    EmailMessagesThatRequireTXDs_LABEL[15] = "SXT_HCH_1ST" 
    EmailMessagesThatRequireTXDs_STRING[15] = "10_a_sext_hitcherGirl" 

    EmailMessagesThatRequireTXDs_LABEL[16] = "SXT_HCH_2ND" 
    EmailMessagesThatRequireTXDs_STRING[16] = "10_b_sext_hitcherGirl" 

    EmailMessagesThatRequireTXDs_LABEL[17] = "SXT_HCH_NEED" 
    EmailMessagesThatRequireTXDs_STRING[17] = "10_c_sext_hitcherGirl" 

    EmailMessagesThatRequireTXDs_LABEL[18] = "SOL2_PASS" 
    EmailMessagesThatRequireTXDs_STRING[18] = "executiveproducer" 

    EmailMessagesThatRequireTXDs_LABEL[19] = "MAPEMAIL_TXT1" 
    EmailMessagesThatRequireTXDs_STRING[19] = "NHP_PHOTO_18" 
	
	EmailMessagesThatRequireTXDs_LABEL[20] = "MAPEMAIL_TXT2" 
    EmailMessagesThatRequireTXDs_STRING[20] = "NHP_PHOTO_11" 
	
	EmailMessagesThatRequireTXDs_LABEL[21] = "MAPEMAIL_TXT3" 
    EmailMessagesThatRequireTXDs_STRING[21] = "NHP_PHOTO_13" 
	
	EmailMessagesThatRequireTXDs_LABEL[22] = "MAPEMAIL_TXT4" 
    EmailMessagesThatRequireTXDs_STRING[22] = "NHP_PHOTO_5" 
	
    EmailMessagesThatRequireTXDs_LABEL[23] = "MAPEMAIL_TXT5" 
    EmailMessagesThatRequireTXDs_STRING[23] = "NHP_PHOTO_12" 
	
	 EmailMessagesThatRequireTXDs_LABEL[24] = "MAPEMAIL_TXT6" 
    EmailMessagesThatRequireTXDs_STRING[24] = "NHP_PHOTO_3" 
	
	EmailMessagesThatRequireTXDs_LABEL[25] = "MAPEMAIL_TXT7" 
    EmailMessagesThatRequireTXDs_STRING[25] = "NHP_PHOTO_1" 
	
	EmailMessagesThatRequireTXDs_LABEL[26] = "MAPEMAIL_TXT8" 
    EmailMessagesThatRequireTXDs_STRING[26] = "NHP_PHOTO_10" 
	
	EmailMessagesThatRequireTXDs_LABEL[27] = "MAPEMAIL_TXT9" 
    EmailMessagesThatRequireTXDs_STRING[27] = "NHP_PHOTO_6" 
	
	EmailMessagesThatRequireTXDs_LABEL[28] = "MAPEMAIL_TXT10" 
    EmailMessagesThatRequireTXDs_STRING[28] = "NHP_PHOTO_20" 
	
	 EmailMessagesThatRequireTXDs_LABEL[29] = "MAPEMAIL_TXT11" 
    EmailMessagesThatRequireTXDs_STRING[29] = "NHP_PHOTO_7" 
	
	EmailMessagesThatRequireTXDs_LABEL[30] = "MAPEMAIL_TXT12" 
    EmailMessagesThatRequireTXDs_STRING[30] = "NHP_PHOTO_14" 
	
	EmailMessagesThatRequireTXDs_LABEL[31] = "MAPEMAIL_TXT13" 
    EmailMessagesThatRequireTXDs_STRING[31] = "NHP_PHOTO_4" 
   
	EmailMessagesThatRequireTXDs_LABEL[32] = "MAPEMAIL_TXT14" 
    EmailMessagesThatRequireTXDs_STRING[32] = "NHP_PHOTO_17" 
	
	EmailMessagesThatRequireTXDs_LABEL[33] = "MAPEMAIL_TXT15" 
    EmailMessagesThatRequireTXDs_STRING[33] = "NHP_PHOTO_2" 
	
    EmailMessagesThatRequireTXDs_LABEL[34] = "MAPEMAIL_TXT16" 
    EmailMessagesThatRequireTXDs_STRING[34] = "NHP_PHOTO_15" 
   
	EmailMessagesThatRequireTXDs_LABEL[35] = "MAPEMAIL_TXT17" 
    EmailMessagesThatRequireTXDs_STRING[35] = "NHP_PHOTO_8" 
	
	EmailMessagesThatRequireTXDs_LABEL[36] = "MAPEMAIL_TXT18" 
    EmailMessagesThatRequireTXDs_STRING[36] = "NHP_PHOTO_9" 
	
	EmailMessagesThatRequireTXDs_LABEL[37] = "MAPEMAIL_TXT19" 
    EmailMessagesThatRequireTXDs_STRING[37] = "NHP_PHOTO_16" 
	
	EmailMessagesThatRequireTXDs_LABEL[38] = "MC_EMAIL_0" 
    EmailMessagesThatRequireTXDs_STRING[38] = "NHP_prep_mines" 
	
	EmailMessagesThatRequireTXDs_LABEL[39] = "BONEPIC1_TXT" 
    EmailMessagesThatRequireTXDs_STRING[39] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[40] = "BONEPIC2_TXT" 
    EmailMessagesThatRequireTXDs_STRING[40] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[41] = "BONEPIC3_TXT" 
    EmailMessagesThatRequireTXDs_STRING[41] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[42] = "BONEPIC4_TXT" 
    EmailMessagesThatRequireTXDs_STRING[42] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[43] = "BONEPIC5_TXT" 
    EmailMessagesThatRequireTXDs_STRING[43] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[44] = "BONEPIC6_TXT" 
    EmailMessagesThatRequireTXDs_STRING[44] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[45] = "BONEPIC7_TXT" 
    EmailMessagesThatRequireTXDs_STRING[45] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[46] = "BONEPIC8_TXT" 
    EmailMessagesThatRequireTXDs_STRING[46] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[47] = "BONEPIC9_TXT" 
    EmailMessagesThatRequireTXDs_STRING[47] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[48] = "BONEPIC10_TXT" 
    EmailMessagesThatRequireTXDs_STRING[48] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[49] = "BONEPIC11_TXT" 
    EmailMessagesThatRequireTXDs_STRING[49] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[50] = "BONEPIC12_TXT" 
    EmailMessagesThatRequireTXDs_STRING[50] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[51] = "BONEPIC13_TXT" 
    EmailMessagesThatRequireTXDs_STRING[51] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[52] = "BONEPIC14_TXT" 
    EmailMessagesThatRequireTXDs_STRING[52] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[53] = "BONEPIC15_TXT" 
    EmailMessagesThatRequireTXDs_STRING[53] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[54] = "BONEPIC16_TXT" 
    EmailMessagesThatRequireTXDs_STRING[54] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[55] = "BONEPIC17_TXT" 
    EmailMessagesThatRequireTXDs_STRING[55] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[56] = "BONEPIC18_TXT" 
    EmailMessagesThatRequireTXDs_STRING[56] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[57] = "BONEPIC19_TXT" 
    EmailMessagesThatRequireTXDs_STRING[57] = "" 
	
	EmailMessagesThatRequireTXDs_LABEL[58] = "BONEPIC20_TXT" 
    EmailMessagesThatRequireTXDs_STRING[58] = "" 
	
	
    
	
ENDPROC




 











PROC Cleanup_and_Terminate()

	//Potential fix for 3059265. If we're not in first person, make sure that on exit of this application the phone re-establishes its zenith position on the screen.
	
	IF ShouldFirstPersonPhoneHudMovementBeHidden() = FALSE //This app exists in MP only, so as the phone is not in hand in MP this should always be false. Hud movement is never hidden in MP.
		
        SET_MOBILE_PHONE_POSITION (g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio]) //Zenith of normal phone movement.
		
	 	#if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING(" AppMPEmail - resetting phone position to EndVec in Cleanup_and_Terminate.")
            PRINTNL()
        #endif
		
    ENDIF


    //Tricky timing fix for Bug 110732 - if this doesn't work, replace with direct reference to movie load state
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 
	
        HANG_UP_AND_PUT_AWAY_PHONE()    //Flashhand cannot exit if an application thread has launched but hasn't been cleaned up so this is a weird timing bug.
                                        
    ENDIF


    
    IF b_TXD_Load_Required
    
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED (CurrentEmailMessageTXD)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING(" AppMPEmail - Terminating App. Removing streamed texture dictionary...")
            PRINTNL()
        #endif

    ENDIF

    //Critical!
    g_b_Rotate3dPhoneLandscape = FALSE
    g_b_Rotate3dPhonePortrait = FALSE

    TERMINATE_THIS_THREAD()

ENDPROC






PROC RotatePhoneToWidescreen()
    FLOAT fComplete = MovePhoneToDestination( v_3dEmailOriginalPosition, v_3dEmailDesiredPosition, g_3dPhoneStartRotVec, <<-90, 0, 90.0>>, 350.0, FALSE)


        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING("AppMPEmail - Rotating phone to widescreen from AppMPEmail")
            PRINTNL()
        #endif

    
    // scale the phone up just a bit so that email is readable
    // note that subtitles were already overlapping, so might as well go big (or go home)
    SET_MOBILE_PHONE_SCALE(500 + 75*fComplete)
    IF fComplete >= 1.0
        g_b_Rotate3dPhoneLandscape = FALSE
        s_iTimeStampStart = 0
    ENDIF
ENDPROC



PROC RotatePhoneToPortrait_and_Exit()
    FLOAT fComplete = MovePhoneToDestination( v_3dEmailDesiredPosition, v_3dEmailOriginalPosition, <<-90, 0, 90.0>>, g_3dPhoneStartRotVec, 350.0, FALSE)
    

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING("AppMPEmail - Rotating phone to portrait from AppMPEmail")
            PRINTNL()
        #endif


    // scale the phone up just a bit so that email is readable
    SET_MOBILE_PHONE_SCALE(500 + 75*(1.0-fComplete))
    IF fComplete >= 1.0
        g_b_Rotate3dPhonePortrait = FALSE
        Cleanup_and_Terminate()
        s_iTimeStampStart = 0
    ENDIF
ENDPROC






PROC Reinitialise_Text_Main_Screen()



    //Now includes second display_view parameter to jump back to last list position after single message view exit.

    //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE
    IF g_LastEmailSentMustBeRead = FALSE

       

        IF i_FullListSlotToReturnTo < 0

            i_FullListSlotToReturnTo = 0

        ENDIF


        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_INBOX")


    
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 8, TO_FLOAT(i_FullListSlotToReturnTo))
   




        #if IS_DEBUG_BUILD

            PRINTSTRING ("AppMPEmail - FullListSlotToReturnTo is ")
            PRINTINT (i_FullListSlotToReturnTo)
            PRINTNL()       


        #endif



        //Badger
        IF g_b_ToggleButtonLabels

            IF NumberOfMessagesInList > 0 //only display "Options" button if there is an actual text or texts in the list.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_214") //"Options" - Positive
            ELSE
               LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                    2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ENDIF


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative

        ELSE
            IF NumberOfMessagesInList > 0 //only display "Options" button if there is an actual text or texts in the list.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ELSE
               LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                2, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Options" - Positive
            ENDIF
            

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"BACK" - Negative
        ENDIF


        //1636892 - Display soft key to show that Contacts App can be opened from within Text Message App in MP only.

        IF g_bInMultiplayer

            //If I allow auto_launch of contacts in the future using the g_LaunchContactsFromTextMessage system then re-establish this button. It displays a wee envelope with an arrow. 
            
            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
            //    11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Contacts" - Other
            //CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


            //Let's just switch it off in the meantime.
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)


        ELSE

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
            CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

        ENDIF

    ELSE


        #if IS_DEBUG_BUILD

            cdPrintstring("AppMPEmail - Jumping straight to single message view as g_LastEmailSentMustBeRead is TRUE")
            cdPrintnl()

        #endif
        
        i_FullListSlotToReturnTo = 0


    ENDIF








ENDPROC





 FUNC BOOL IS_BOUNTY_HUNTER_MESSAGE(STRING sMessage)
	SWITCH GET_HASH_KEY(sMessage)
		
		CASE HASH ("BONEPIC1_TXT")
		CASE HASH ("BONEPIC2_TXT")
		CASE HASH ("BONEPIC3_TXT")
		CASE HASH ("BONEPIC4_TXT")
		CASE HASH ("BONEPIC5_TXT")
		CASE HASH ("BONEPIC6_TXT")
		CASE HASH ("BONEPIC7_TXT")
		CASE HASH ("BONEPIC8_TXT")
		CASE HASH ("BONEPIC9_TXT")
		CASE HASH ("BONEPIC10_TXT")
		CASE HASH ("BONEPIC11_TXT")
		CASE HASH ("BONEPIC12_TXT")
		CASE HASH ("BONEPIC13_TXT")
		CASE HASH ("BONEPIC14_TXT")
		CASE HASH ("BONEPIC15_TXT")
		CASE HASH ("BONEPIC16_TXT")
		CASE HASH ("BONEPIC17_TXT")
		CASE HASH ("BONEPIC18_TXT")
		CASE HASH ("BONEPIC19_TXT")
		CASE HASH ("BONEPIC20_TXT")


			RETURN TRUE			
	ENDSWITCH
	RETURN FALSE
ENDFUNC


PROC Place_Existing_Messages_In_Slots()


    NumberOfMessagesInList = 0 //reset number of placed messages before filling list.
	
	NumberOfFlowMessagesInList = 0 //reset number of placed messages before filling list.

    //Display dummy "text list empty" message in slot 1 in lieu of any real texts overwriting.  Removed as Eddie has a general solution #1351002
    /*
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(6)), (TO_FLOAT(0)), 12, 34,
        INVALID_SCALEFORM_PARAM, "CELL_230", "CELL_231")
    */


    Get_Cellphone_Owner()


    BOOL message_in_chrono_order[MAX_MP_EMAILS]

    INT slotIndex = 0 //,  scaleform_SlotIndex = 0

    INT whichRetrievedEmail = 0
      
	  
	  
	   //Set up ordinary flow emails from the last known slot index.                                                     
    WHILE slotIndex < (MAX_MP_EMAILS - 1)


        //Tricky timing fix for Bug 110732
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

            Cleanup_and_Terminate ()    

        ENDIF



        INT comparisonIndex = 0

        INT top_time_comparison = (MAX_MP_EMAILS - 1) //Dummy position



        //Create an impossibly earlier time and date as the top comparison in the last, "dummy position" of the text message array.
        g_EmailMessage[top_time_comparison].EmailTimeSent.EmailSecs = -1 
        g_EmailMessage[top_time_comparison].EmailTimeSent.EmailMins = 0
        g_EmailMessage[top_time_comparison].EmailTimeSent.EmailHours = 0
        g_EmailMessage[top_time_comparison].EmailTimeSent.EmailDay = 0
        g_EmailMessage[top_time_comparison].EmailTimeSent.EmailYear = 0

        



        WHILE comparisonIndex < (MAX_MP_EMAILS)


            //Tricky timing fix for Bug 110732
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

                Cleanup_and_Terminate ()    

            ENDIF

                //IF (INT_TO_ENUM (enumCharacterList, comparisonIndex )) <> g_Cellphone.PhoneOwner //Don't let the character include a contact to himself.

                    IF message_in_chrono_order[comparisonIndex] = FALSE

                        IF g_EmailMessage[comparisonIndex].EmailLockStatus <> Email_EMPTY //make sure empty text messages aren't counted in this list.  


                            //As we're checking this, we don't have to specifically check the SP or MP portions. No SP messages should be resident in MP portion
                            IF g_EmailMessage[comparisonIndex].PhonePresence[g_Cellphone.PhoneOwner] = TRUE //check filter for this player character
                                                                    
                                IF Email_IsFirstTimeNewerThanSecondTime (g_EmailMessage[comparisonIndex].EmailTimeSent,  g_EmailMessage[top_time_comparison].EmailTimeSent)
                            
                                    top_time_comparison = comparisonIndex                   

                                    IF g_EmailMessage[top_time_comparison].EmailReadStatus = UNREAD_MPEMAIL

                                        MessageReadDecider = 0  //Unread messages use icon 33 - see badger wiki.

                                        
                                    ELSE
                                    
                                        MessageReadDecider = 1  //Read messages use icon 34 - see badger wiki.


                                    ENDIF



                                                                           
                                                             
                                                             
                                                                              
                                                             
                                    //Comment back into to display results of sorting algorithm.
                                    /*                                                             
                                    #if IS_DEBUG_BUILD
                                        
                                        PRINTNL()
                                        PRINTSTRING("Text message details")
                                        PRINTNL()
                                        PRINTSTRING("SlotIndex ")
                                        PRINTINT(slotIndex)
                                        PRINTNL()
                                        PRINTSTRING(g_EmailMessage[top_time_comparison].EmailLabel)
                                        PRINTNL()
                                        PRINTSTRING("end")
                                        PRINTNL()
                                    
                                    #endif
                                    */

                                                   
                                ENDIF

                            ENDIF

                        ENDIF

                    ENDIF
               //ENDIF

            //ENDIF

            comparisonIndex ++

        ENDWHILE

        
    
        MessageListSlot[slotIndex] = top_time_comparison
        message_in_chrono_order[top_time_comparison] = TRUE








        //Place the result of the sort in the scaleform slot.

        IF g_EmailMessage[top_time_comparison].EmailLockStatus <> Email_EMPTY //make sure empty text messages aren't counted in this list.  


                       //As we're checking this, we don't have to specifically check the SP or MP portions. No SP messages should be resident in MP portion
                       IF g_EmailMessage[top_time_comparison].PhonePresence[g_Cellphone.PhoneOwner] = TRUE //check filter for this player character






    /*
    param 1 = View state enum
    param 2 = Slot number
    param 3 = Icon type 0 = unread, 1 = read, 2 = needs reply
    param 4 = Boolean - Has attachment icon
    param 5 = String - Senders email address
    param 6 = String - Subject 
    */
                                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotIndex)
                                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_EmailMessage[top_time_comparison].EmailTimeSent.EmailHours)
                                    //SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_EmailMessage[top_time_comparison].EmailTimeSent.EmailMins)
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MessageReadDecider)


                                    //Set up attachment icon
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
                                    //End of attachment icon


                            
                                    //Set up "From Sender Address"
                                    IF g_EmailMessage[top_time_comparison].EmailSender = NO_CHARACTER
                                        
                                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_EmailMessage[top_time_comparison].EmailSenderStringComponent)
                                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_2000") //~a~

                                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSenderStringComponent)
                                        
                                        END_TEXT_COMMAND_SCALEFORM_STRING()

                                    ELSE
                
                                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sCharacterSheetAll[g_EmailMessage[top_time_comparison].EmailSender].label)
                                        
                                    ENDIF
                                    //End of "From Sender Address"

                          


                                    //Set up "Subject"
                                    TEXT_LABEL_63 SubjectCheck_Label = g_EmailMessage[top_time_comparison].EmailLabel
									
									IF 	IS_BOUNTY_HUNTER_MESSAGE(g_EmailMessage[top_time_comparison].EmailLabel)
										INT numberofcurrentkills = GET_PACKED_STAT_INT(PACKED_MP_RDR_BOUNTY_HUNTER_NUMBER_OF_BOUNTIES_COMPLETED) 
										SWITCH numberofcurrentkills
										
											CASE 0 SubjectCheck_Label =  "BONEMAIL_TXT_SUB" BREAK
											CASE 1 SubjectCheck_Label =  "BONEMAIL2_TXT_SUB" BREAK
											CASE 2 SubjectCheck_Label =  "BONEMAIL3_TXT_SUB" BREAK
											CASE 3 SubjectCheck_Label =  "BONEMAIL4_TXT_SUB" BREAK
											CASE 4 SubjectCheck_Label =  "BONEMAIL5_TXT_SUB" BREAK
										
										ENDSWITCH
									ELSE
                                    	SubjectCheck_Label += "_SUB"
									ENDIF
                                    
                                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (SubjectCheck_Label)


                                    
                                    /*
                                    SWITCH g_EmailMessage[top_time_comparison].EmailSpecialComponents

                                        CASE NO_SPECIAL_COMPONENTS

                                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING (g_EmailMessage[top_time_comparison].EmailLabel)

                                        BREAK
                                    
                                
                                        CASE STRING_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_EmailMessage[top_time_comparison].EmailLabel)
                                        
                                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailStringComponent)

        
                                            //Check if any additional strings are required...
                                            IF g_EmailMessage[top_time_comparison].EmailNumberOfAdditionalStrings = 1
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailSecondStringComponent, "NULL"))

                                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSecondStringComponent)   

                                            ENDIF


                                            IF g_EmailMessage[top_time_comparison].EmailNumberOfAdditionalStrings = 2
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailSecondStringComponent, "NULL"))
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailThirdStringComponent, "NULL"))
                                                IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                ENDIF
                                                IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[top_time_comparison].EmailThirdStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[top_time_comparison].EmailThirdStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailThirdStringComponent)        
                                                ENDIF
                                            ENDIF

                                
                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK


                                        CASE NUMBER_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_EmailMessage[top_time_comparison].EmailLabel)
                                        
                                            ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[top_time_comparison].EmailNumberComponent)

                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK



                                        CASE STRING_AND_NUMBER_COMPONENT

                                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING (g_EmailMessage[top_time_comparison].EmailLabel)
                                        
                                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailStringComponent)
                                            ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[top_time_comparison].EmailNumberComponent)


                                            //Check if any additional strings are required...
                                            IF g_EmailMessage[top_time_comparison].EmailNumberOfAdditionalStrings = 1
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailSecondStringComponent, "NULL"))

                                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSecondStringComponent)   

                                            ENDIF


                                            IF g_EmailMessage[top_time_comparison].EmailNumberOfAdditionalStrings = 2
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailSecondStringComponent, "NULL"))
                                            AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[top_time_comparison].EmailThirdStringComponent, "NULL"))

                                                //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailThirdStringComponent)        
                                                IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailSecondStringComponent)
                                                ENDIF
                                                IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[top_time_comparison].EmailThirdStringComponent)
                                                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[top_time_comparison].EmailThirdStringComponent)
                                                ELSE
                                                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[top_time_comparison].EmailThirdStringComponent)        
                                                ENDIF
                                            ENDIF



                                            END_TEXT_COMMAND_SCALEFORM_STRING()

                                        BREAK



                                        CASE CAR_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK




                                        CASE SUPERAUTOS_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE LEGENDARY_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE PEDALMETAL_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE WARSTOCK_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE ELITAS_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                        CASE DOCKTEASE_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK

                                        
                                        CASE DAILYOBJ_LIST_COMPONENT
                                        
                                            Create_Car_List_String_For_Scaleform(top_time_comparison)

                                        BREAK


                                    ENDSWITCH
                                    */
                                    //End of "Subject""


                  
                                    


                                    
                                END_SCALEFORM_MOVIE_METHOD ()

                                //New work to retrieve SC Emails from server for marketing purposes. Requested by 2301166
                                //Adding the incrementation here for safety.

                                NumberOfMessagesInList ++ 
								
								NumberOfFlowMessagesInList ++

                                #if IS_DEBUG_BUILD
                                    cdPrintnl()
                                    cdPrintnl()
                                    cdPrintstring("AppMPEmail - 3 - NumberOfMessagesInList incremented after filling scaleform slot ")
                                    cdPrintint(slotIndex)
                                    cdPrintnl()
                                #endif




                    ENDIF

        ENDIF

        slotIndex ++



    ENDWHILE   


    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("AppMPEmail - PlaceExistingMessages concluded.")
        cdPrintnl()   
    #endif
	  
	  
	  
	  
	  
	  
	  slotIndex = NumberOfMessagesInList 
	  

    
    											//This section will display all Eyefind emails from any sender in the retrieved batch.
    											BOOL b_EmailAlreadyPlaced[10]
												BOOL b_EmailPlacedSuccessfullyPlacedThisLoop = FALSE
												#if IS_DEBUG_BUILD
												UGC_DATE MktSentTimeStamp
												UGC_DATE CurrentTimeCheck
												#endif

																				    WHILE slotIndex < (i_Num_of_Marketing_Emails_Retrieved + NumberOfMessagesInList)

																				        whichRetrievedEmail = 0
																				        b_EmailPlacedSuccessfullyPlacedThisLoop = FALSE


																				        WHILE whichRetrievedEmail < i_Num_of_Marketing_Emails_Retrieved
																				        AND b_EmailPlacedSuccessfullyPlacedThisLoop = FALSE
																				         
																				            #if IS_DEBUG_BUILD

																				                cdPrintstring("AppMPEmail - Trying to place filtered Marketing email ")
																				                cdPrintint(whichRetrievedEmail)
																				                cdPrintstring(" in slot ")
																				                cdPrintint(slotIndex)
																				                cdPrintnl()

																				            #endif


																				            SC_EMAIL_GET_EMAIL_AT_INDEX (whichRetrievedEmail, single_email_Received_Data)

																				            //Returned struct for reference.
																				            /*
																				            STRUCT scrEmail_Data
																				                INT                 Id
																				                INT                 CreatePosixTime
																				                TEXT_LABEL_31       SenderName   //This is being used for the custom vendor name
																				                GAMER_HANDLE        SenderHandle
																				                TEXT_LABEL_63       Subject[2]
																				                TEXT_LABEL_63       Contents[16]
																				                INT                 Image
																				            ENDSTRUCT
																				            */
																				        
																				            //INFO: Retrieve a hash for a gamer handle
																				            //NATIVE FUNC INT NETWORK_HASH_FROM_GAMER_HANDLE(GAMER_HANDLE& gamerHandle) = "0x89963a9e50eaa50f"

																				            //IF ARE_STRINGS_EQUAL (single_email_Received_Data.SenderName, "SteveT7RocksnPT")   //SteveT7RocksnPT is primary test, stevet2RSN is secondary test account.
																				            //OR NETWORK_HASH_FROM_GAMER_HANDLE(single_email_Received_Data.SenderHandle) = -1573246464 //Xbox One
																				            //OR NETWORK_HASH_FROM_GAMER_HANDLE(single_email_Received_Data.SenderHandle) = -1117039173 //PC

																				                IF b_EmailAlreadyPlaced[whichRetrievedEmail] = FALSE

																				                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

																				                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
																				                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(slotIndex )
																				                        
																										//Linking the unread email in the first slot, i,e the latest email to the posix stat controlled global. See bug 2614094.
																										IF slotIndex = 0 
																											IF g_NumberOfUnreadMarketingEmailsThisSession = 0 //Latest email in slot zero has been read. 
																												SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)//(MessageReadDecider) //0 puts dot beside email, 1 leaves clear indicating that it has been read.
																											ELSE
																												SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
																											ENDIF
																										ELSE
																											SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) //0 puts dot beside left hand side of email in list view, 1 leaves clear. Only put dot beside first, latest email.
																										ENDIF
																										

																				                        //Set up attachment icon
																				                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
																				                        //End of attachment icon


																				                
																				                        //Set up "From Sender Address"

																				                        //Would check the email banner ad int here as requested in url:bugstar:2551529 to determine which sender name to use.
																				                        /*
																				                        IF NumberOfMessagesInList = 0 
																				                            ThisSenderName = "Dynasty 8"
																				                        ENDIF
																				                        
																				                        IF NumberOfMessagesInList = 1 
																				                            ThisSenderName = "Sender 2"
																				                        ENDIF

																				                        IF NumberOfMessagesInList > 1 
																				                            ThisSenderName = "Sender 3"
																				                        ENDIF
																				                        */
																				                                       
																				                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_2000") //~a~

																				                            //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME ("Legendary Motorsport Long Name") // 2614045 Put this in for testing a name longer than sixteen characters.
																				                            //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (ThisSenderName) //2517261 requested this string be used for testing.
																				                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.SenderName)
																				                            
																				                        END_TEXT_COMMAND_SCALEFORM_STRING()

																				                                     

																				                        //Set up "Subject"

																				                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_EMAIL_SUBJ") //~a~

																				                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Subject[0])
																				                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Subject[1])
																				                    
																				                        END_TEXT_COMMAND_SCALEFORM_STRING()


																				                    END_SCALEFORM_MOVIE_METHOD()


																				                    b_EmailAlreadyPlaced[whichRetrievedEmail] = TRUE


																				                    MessageListSlot[slotIndex] = whichRetrievedEmail


																				                    b_EmailPlacedSuccessfullyPlacedThisLoop = TRUE


																				                    NumberOfMessagesInList ++ 



																				                    #if IS_DEBUG_BUILD

																				                        cdPrintstring("AppMPEmail - Placed filtered Marketing email ")
																				                        cdPrintint(whichRetrievedEmail)
																				                        cdPrintstring(" in slot ")
																				                        cdPrintint(slotIndex)
																				                        cdPrintstring(" with posix ")
																				                        cdPrintint(single_email_Received_Data.CreatePosixTime)
																				                        cdPrintnl()

																				                        cdPrintstring(" with image header selection ")
																				                        cdPrintint(single_email_Received_Data.Image)
																				                        cdPrintnl()


																				                        cdPrintstring ("Number of messages in list is at ")
																				                        cdPrintint(NumberOfMessagesInList)
																				                        cdPrintnl()



																				                        CONVERT_POSIX_TIME (single_email_Received_Data.CreatePosixTime, MktSentTimeStamp)

																				                        cdPrintnl()
																				                        cdPrintstring("Email Sent time details are ")

																				                        cdPrintint(MktSentTimeStamp.nYear)
																				                        cdPrintnl()
																				                        cdPrintint(MktSentTimeStamp.nMonth)
																				                        cdPrintnl()
																				                        cdPrintint(MktSentTimeStamp.nDay)
																				                        cdPrintnl()
																				                        cdPrintint(MktSentTimeStamp.nHour)
																				                        cdPrintnl()
																				                        cdPrintint(MktSentTimeStamp.nMinute)
																				                        cdPrintnl()
																				                        cdPrintint(MktSentTimeStamp.nSecond)
																				                        cdPrintnl()
																				                        cdPrintnl()

																				                        
																				                        
																				                        GET_POSIX_TIME(CurrentTimeCheck.nYear, CurrentTimeCheck.nMonth, CurrentTimeCheck.nDay, CurrentTimeCheck.nHour, CurrentTimeCheck.nMinute, CurrentTimeCheck.nSecond)

																				                        cdPrintnl()
																				                        cdPrintstring("Current time details are ")

																				                        cdPrintint(CurrentTimeCheck.nYear)
																				                        cdPrintnl()
																				                        cdPrintint(CurrentTimeCheck.nMonth)
																				                        cdPrintnl()
																				                        cdPrintint(CurrentTimeCheck.nDay)
																				                        cdPrintnl()
																				                        cdPrintint(CurrentTimeCheck.nHour)
																				                        cdPrintnl()
																				                        cdPrintint(CurrentTimeCheck.nMinute)
																				                        cdPrintnl()
																				                        cdPrintint(CurrentTimeCheck.nSecond)
																				                        cdPrintnl()
																				                        cdPrintnl()

																				                        //2539286 - Above work helps support expiration dates if it cannot be done at the server side. If a message is sent with an expiry date,
																				                        //I'd just compare that posix with the current time. Existing "Sent" posix time used above as a reference to the mechanics involved.
																				                        //If we need to then compare the dates for expiry purposes e.g  then we could use and adapted FUNC BOOL IsFirstTimeNewerThanSecondTime (structTxtMsgTimeSent FirstTimeToCompare, structTxtMsgTimeSent SecondTimeToCompare)



																				                     #endif

																				                    //Keep a copy of the latest received posix to easily update the last read posix stat if the first email is actually read.
																				                    IF whichRetrievedEmail = 0

																				                        #if IS_DEBUG_BUILD
																				                            cdPrintstring("Keeping record of latest posix ")
																				                            cdPrintint(single_email_Received_Data.CreatePosixTime)
																				                        #endif

																				                        i_LatestPosixTime = single_email_Received_Data.CreatePosixTime

																				                    ENDIF


																				                ENDIF

																				            //ENDIF

																				            whichRetrievedEmail ++

																				        ENDWHILE



																				        slotIndex ++

																				    ENDWHILE

    //End of section that will display all Eyefind emails from any sender in the retrieved batch.







      
  
   



    //Potential addition for scaleform value not returning.
    //b_RepeatButtonLimiter = FALSE
    //RepeatButtonLimiter_StartTime = GET_GAME_TIMER()

     
ENDPROC








#IF IS_DEBUG_BUILD
PROC Draw_SingleMessageOptions_HelpText() 

    format_medium_ostext (0, 128, 255, 255) //Light blue //CALL
    //DISPLAY_TEXT (0.750, 0.390, "CELL_201")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.05), (g_SF_PhonePosY + 0.16), "CELL_201")



    //Need to grey out any locked messages as these cannot be deleted by the player.
    IF g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_LOCKED
        format_medium_ostext (255, 255, 255, 55) //Greyed out //DELETE
    ELSE
        format_medium_ostext (0, 255, 0, 255) //Green //DELETE
    ENDIF
    
    
    //DISPLAY_TEXT (0.790, 0.390, "CELL_216")
    DISPLAY_TEXT ((g_SF_PhonePosX - 0.02), (g_SF_PhonePosY + 0.16), "CELL_216")






    format_medium_ostext (255, 0, 0, 255) //Solid Red  //BACK
    //DISPLAY_TEXT (0.847, 0.390, "CELL_206")
    DISPLAY_TEXT ((g_SF_PhonePosX + 0.02), (g_SF_PhonePosY + 0.16), "CELL_206")


ENDPROC
#endif








PROC Display_Selected_Message()


    #IF IS_DEBUG_BUILD  
    
    IF g_DoDebugTempDraw_Phone = TRUE

    
        drawItemX = ListOriginX
        drawItemY = ListOriginY 

                

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        highlight_Item()
           
        DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_EmailMessage[SingleMessageIndex].EmailSender].label) //draw the name of the contact by referring to the character's text label. 
        
        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)

        
        //Make sure we display a leading zero if the minutes are less than 10.
        IF g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailMins < 10
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailHours, g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailMins)
        ELSE
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailHours, g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailMins)
        ENDIF

        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)
        //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailDay, g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMonth, g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailYear)
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailDay, g_EmailMessage[SingleMessageIndex].EmailTimeSent.EmailMonth)


        #IF IS_DEBUG_BUILD
            //Display the text label for debug purposes
            IF g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_LOCKED
                format_medium_ostext (255, 0, 0, 95) //Semi transparent red
            ELSE
                format_medium_ostext (0, 255, 0, 95) //Semi transparent green
            ENDIF
            DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_EmailMessage[SingleMessageIndex].EmailLabel)
        #endif

        drawItemX = ListOriginX

        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        drawItemY = drawItemY + 0.02

        highlight_Item()

        SET_TEXT_WRAP (0.0, 0.93)
        DISPLAY_TEXT (drawItemX, drawItemY, g_EmailMessage[SingleMessageIndex].EmailLabel) //the contexts of the text itself

    ENDIF

    #endif
              



    //Delete this text message if not locked...

    IF g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED
        
        IF g_InputButtonJustPressed = FALSE
        IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_DELETE_OPTION_INPUT)) //Delete action - delete this single text message
        
           
            Play_Back_Beep()

            g_InputButtonJustPressed = TRUE

    


            Get_Cellphone_Owner()
                    

            IF g_EmailMessage[SingleMessageIndex].EmailDeletionMode = MPE_DELETE_FROM_ALL_CHARACTERS

                //If en masse deletion is tagged for this message then we want to remove the message from all SP player character filters...
                g_EmailMessage[SingleMessageIndex].PhonePresence[0] = FALSE
                g_EmailMessage[SingleMessageIndex].PhonePresence[1] = FALSE
                g_EmailMessage[SingleMessageIndex].PhonePresence[2] = FALSE

            ELSE

                //...otherwise just remove from the player character currently in use.
                g_EmailMessage[SingleMessageIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE


            ENDIF


            IF IS_TEXTMSG_PRESENT_IN_ANY_FILTER (SingleMessageIndex)
                
                
                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Joypad removed message from current player filter only. Current player Int is ")
                    PRINTINT (ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    PRINTNL ()

                #endif
                    

            ELSE

                //We only want to mark the array index as empty if the message is not present in any character's text message filter.

                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_EMPTY
                g_EmailMessage[SingleMessageIndex].EmailReadStatus = UNREAD_MPEMAIL


                #IF IS_DEBUG_BUILD

                    PRINTSTRING ("Joypad removed message no longer in any player filter. Deleted message from list at array position ")
                    PRINTINT (SingleMessageIndex)
                    PRINTNL ()

                #endif
                    
          
            ENDIF


            //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
            THEFEED_REMOVE_ITEM (g_EmailMessage[SingleMessageIndex].EmailFeedEntryId)






            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)


            Place_Existing_Messages_In_Slots() //Update the slot list to reflect removal of deleted message.


       

            //For any list navigation cursor positions greater than zero, i.e the top position, we need to jump one up the list 
            //as deletion has occurred.  This allows display_view in "Reinitialise_Text_Main_Screen" to restore last list position.
            
            IF ListCursorIndex > 0
                ListCursorIndex --
            ENDIF 
           


            Reinitialise_Text_Main_Screen()



            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.


                g_CellPhone.PhoneDS = PDS_RUNNINGAPP
                g_DoOptionsForSingleMessage= FALSE //Return to full display list.


                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 24. AppMPEmail assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            ENDIF

        
      
        ENDIF
        ENDIF

     
     ELSE
     
        //Hyperlinked messages should always be sent as LOCKED and DO_NOT_AUTO_UNLOCK
        IF g_InputButtonJustPressed = FALSE
        IF IS_CONTROL_PRESSED (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) 


            IF ThisMessageContainsHyperlink = TRUE
        
            
                //If this message has a hyperlink attached to it, exit and signal that appInternet should launch. 
                //Hyperlinked messages cannot be deleted by the user at this point as they share the same option button.

                IF g_CellPhone.PhoneDS > PDS_AWAY

                    g_BrowserLinkToLaunch = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label)
					
					
					IF ARE_STRINGS_EQUAL ( g_BrowserLinkToLaunch, "HARDCODED_4207156") //This hardcoded string MUST match whatever the email hyperlink contains in the text file for bug 4207156
					
						g_CellPhone.PhoneDS = PDS_AWAY
					
						//TODO: Need flag to launch Kevin's start menu for bug 4207156
						g_blaunch_criminal_starter_pack = TRUE
									
					ELSE

	                    g_BrowserStartState = SBSS_Launch_Link

	                
	                    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_APPINTERNET_ON_RETURN_TO_PDS_MAXIMUM) //Force Check_For_App_Selection to trigger and launch appInternet automatically.
	                
	                    g_CellPhone.PhoneDS = PDS_MAXIMUM
					
					ENDIF
                    
                ENDIF

                
                Cleanup_and_Terminate() //Make sure this script terminates immediately.

   
            ENDIF

        ENDIF
        ENDIF

     ENDIF
                                                                                                                                                                                        



    IF g_EmailMessage[SingleMessageIndex].EmailBarterStatus = MPE_BARTER_IS_REQUIRED
        
        IF g_InputButtonJustPressed = FALSE
            IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) //Special option - square

                Play_Select_Beep()
            
                g_InputButtonJustPressed = TRUE

                g_EmailMessage[SingleMessageIndex].EmailReplyStatus = MPE_REPLIED_BARTER

                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED

                g_EmailMessage[SingleMessageIndex].EmailBarterStatus = MPE_NO_BARTER_REQUIRED

                CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



                g_DoOptionsForSingleMessage= FALSE


                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 25. AppMPEmail assigns PDS_RUNNINGAPP")
                        cdPrintnl()   
                    #endif

                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)


                Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.

                
                Reinitialise_Text_Main_Screen()




            ENDIF
        ENDIF

    ENDIF



    //Call the sender of the message....
    IF g_InputButtonJustPressed = FALSE
    AND b_RepeatButtonLimiter = TRUE
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - call the sender of the displayed single message.




                     
            g_InputButtonJustPressed = TRUE
        



            //If a reply is required for this message, entering a positive input means a "yes" reply has been given.
            IF g_EmailMessage[SingleMessageIndex].EmailReplyStatus > MPE_NO_REPLY_REQUIRED


                Play_Select_Beep() //#1591713 - Moved in here so it only beeps when an option is available.


                g_EmailMessage[SingleMessageIndex].EmailReplyStatus = MPE_REPLIED_YES

                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED //This should be done by flow - probably!


                g_DoOptionsForSingleMessage= FALSE


                IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                    #if IS_DEBUG_BUILD
                        cdPrintnl()
                        cdPrintstring("STATE ASSIGNMENT 26. AppMPEmail assigns PDS_RUNNINGAPP")
                        cdPrintnl()   
                    #endif

                ENDIF

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)


                Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.  

                
                Reinitialise_Text_Main_Screen()

                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 8) //Set enum state and header to MESSAGE LIST
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_INBOX")


                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1, -1, -1, -1, "CELL_214") //"Options" Positive

                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1, -1, -1, -1, "CELL_206") //"Back" - Negative

                //Make sure this is off in this particular display view   - note that the second int method param is 0 for off.
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 0, -1, -1, -1, "CELL_250") //"Error!" - Other
                */
                



             
            ELSE //"Call" option must have been picked. Only take action if this text message allows the sender call option to be visible and active...


                IF g_EmailMessage[SingleMessageIndex].EmailCanCallSenderStatus = MPE_CAN_CALL_SENDER


                    Play_Select_Beep() //#1591713 - Moved in here so it only beeps when an option is available.


                    //This needs to be done before setting the phone state to make sure any residual contact does not return TRUE inadvertently - see bug 375586
                    //Can't assign directly to chosen name as IS_CALLING_CONTACT may grab before appContacts initialises. Needs to be done after appContacts has started.
                    g_TheContactInvolvedinCall = CHAR_BLANK_ENTRY           


                    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                    
                        g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT    //Setting this here will ensure the scaleform methods below take precedence above the creation of the contacts list itself.
                                                                                //when appContacts is launched. See that script for more info.
                        Put_Cellphone_To_Ear()

                        #if IS_DEBUG_BUILD
                            cdPrintnl()
                            cdPrintstring("STATE ASSIGNMENT 27. AppMPEmail assigns PDS_ATTEMPTING_TO_CALL_CONTACT")
                            cdPrintnl()   
                        #endif
       

                    ENDIF


                    //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
                    request_load_scriptstring ("appContacts")

                    Application_Thread = START_NEW_SCRIPT ("appContacts", CONTACTS_APP_STACK_SIZE)


                    #if IS_DEBUG_BUILD
                        cdPrintstring("AppMPEmail -  Launching appCONTACTS with contacts stack size")
                        cdPrintnl()
                    #endif


                    SET_SCRIPT_AS_NO_LONGER_NEEDED ("appContacts")


                    


                    //Tell the global used by appContacts the sender name of this message which can then be transposed to the character sheet.
                    g_TheContactInvolvedinCall = (g_EmailMessage[SingleMessageIndex].EmailSender)





                    IF GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(g_TheContactInvolvedinCall, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER 

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                            g_CharacterSheetNonSaved[g_TheContactInvolvedinCall].phonebookNumberLabel,
                            "CELL_300",
                            "CELL_211",
                            "CELL_195")   

                    ELSE


                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_DATA_SLOT", (TO_FLOAT(4)), (TO_FLOAT(0)), (TO_FLOAT(3)),
                            INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM,
                           	g_sCharacterSheetAll[g_TheContactInvolvedinCall].label,
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].picture,
                            "CELL_211",

                            //Replaced .role with .label  as this is no longer displayed -  Steve T. 14/11/12. Done in a effort to reduce global usage.
                            g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)

                    
                    ENDIF


                                                                          
                    Update_Scaleform_VSE ()



                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppMPEmail - Attempting to call contact with label: ")
                        cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
                        cdPrintstring (" which is charsheet entry ")
                        cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
                        cdPrintnl()

                    #endif





                    //Experiment with ordering of this section if I can nail down that pesky text bug. Perhaps the change-over to appContacts which also uses the phone text slot interferes
                    //with the contents of that slot when it is being used to show the text messages. Store the text messages in another text slot instead?


                
                    Cleanup_and_Terminate()

                ENDIF


            ENDIF


        ENDIF
    ENDIF

  




ENDPROC







#IF IS_DEBUG_BUILD
PROC Display_Message_List()


    INT drawIndex = 0
 

    drawItemX = ListOriginX
    drawItemY = ListOriginY 


    //DRAW_RECT (0.824, 0.650, 0.238, 0.60, 0, 0, 0, 155)

            
    MessagesToDraw = (NumberOfMessagesInList) 

    //DRAW_RECT (0.86, 0.50, 0.1, 0.2, 0, 0, 0, 165)
    
    WHILE drawIndex < MessagesToDraw

     
        format_medium_ostext (255, 255, 255, 205) //Semi transparent white

        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF

            
        DISPLAY_TEXT (drawItemX, drawItemY, g_sCharacterSheetAll[g_EmailMessage[MessageListSlot[drawIndex]].EmailSender].label) //draw the name of the contact by referring to the character's text label. 
        
        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)

        
        //Make sure we display a leading zero if the minutes are less than 10.
        IF g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMins < 10
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_506", g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailHours, g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMins)
        ELSE
            DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_503", g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailHours, g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMins)
        ENDIF

        drawItemX = drawItemX + 0.07
        format_medium_ostext (255, 255, 255, 205)
        //DISPLAY_TEXT_WITH_3_NUMBERS (drawItemX, drawItemY, "CELL_504", g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailDay, g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMonth, g_TextMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailYear)
        DISPLAY_TEXT_WITH_2_NUMBERS (drawItemX, drawItemY, "CELL_505", g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailDay, g_EmailMessage[MessageListSlot[drawIndex]].EmailTimeSent.EmailMonth)


        #IF IS_DEBUG_BUILD
            //Display the text label for debug purposes
            IF g_EmailMessage[MessageListSlot[drawIndex]].EmailLockStatus = Email_LOCKED
                format_medium_ostext (255, 0, 0, 95) //Semi transparent red
            ELSE
                format_medium_ostext (0, 255, 0, 95) //Semi transparent green
            ENDIF
            DISPLAY_TEXT_WITH_LITERAL_STRING (drawItemX + 0.03, drawItemY, "STRING", g_EmailMessage[MessageListSlot[drawIndex]].EmailLabel)
        #endif

        drawItemX = ListOriginX

        format_medium_ostext (255, 255, 255, 205) //Semi transparent white
        IF drawIndex = ListCursorIndex
            highlight_Item()
        ENDIF

        drawItemY = drawItemY + 0.02

        SET_TEXT_WRAP (0.0, 0.93)
        DISPLAY_TEXT (drawItemX, drawItemY, g_EmailMessage[MessageListSlot[drawIndex]].EmailLabel) //the contexts of the text itself
              
        drawItemY = drawItemY + 0.07 //Draw the next name slightly down the y-axis
   
        drawIndex ++
    ENDWHILE
    


ENDPROC
#endif








PROC Check_for_Single_Message_List_Navigation()

    IF dpad_scroll_pause_cued
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF
    ENDIF

    // PC Scrollwheel support
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
       
            Call_Scaleform_Input_Keypress_Up()
       
        ENDIF
        

        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
          
            Call_Scaleform_Input_Keypress_Down()

        ENDIF
    
    ENDIF



    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
                  
            Call_Scaleform_Input_Keypress_Up()
             
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)

        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            Call_Scaleform_Input_Keypress_Down()
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)
          
        ENDIF

    
    ENDIF

 ENDPROC





PROC Check_for_List_Navigation()

                            
    IF dpad_scroll_pause_cued
        IF TIMERA() > 50

            dpad_scroll_pause_cued = FALSE

        ENDIF
    ENDIF

    // PC Scrollwheel support
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
    
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_BACKWARD))
        
            IF ListCursorIndex > 0
                ListCursorIndex --

                Call_Scaleform_Input_Keypress_Up()

            ENDIF   
        
        
        ENDIF
        
        IF Is_Phone_Control_Just_Pressed( FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION,INPUT_CELLPHONE_SCROLL_FORWARD))
        
            ListCursorIndex ++
            
            IF ListCursorIndex = NumberOfMessagesInList 
            
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintnl()
                cdPrintstring("AppMPEmail - 1 - ListCursorIndex has been reset to zero!")
                cdPrintnl()
            #endif

                ListCursorIndex = 0
            ENDIF
            
            Call_Scaleform_Input_Keypress_Down()

        ENDIF
    
    ENDIF


    IF dpad_scroll_pause_cued = FALSE

        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_UP_INPUT))
        
            IF ListCursorIndex > 0
                ListCursorIndex --

                Call_Scaleform_Input_Keypress_Up()

            ENDIF   
        
             
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)

        ENDIF       
        
        
        IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_NAV_DOWN_INPUT))

            ListCursorIndex ++

                 
            IF ListCursorIndex = NumberOfMessagesInList
            
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintnl()
                cdPrintstring("AppMPEmail - 2 - ListCursorIndex has been reset to zero!")
                cdPrintnl()
            #endif
 

                ListCursorIndex = 0

            ENDIF

           
            Call_Scaleform_Input_Keypress_Down()
            
            dpad_scroll_pause_cued = TRUE
            SETTIMERA(0)
          
        ENDIF

    
    ENDIF

            
ENDPROC



                                 



PROC Check_if_TXD_Load_is_Required()


    IF b_TXD_Load_Required //Get rid of any existing texture dictionaries so they don't stack up during repeated viewings.
    
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED (CurrentEmailMessageTXD)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING("AppMPEmail - Check_if_TXD_Load_is_Required. Removing streamed texture dictionary...")
            PRINTNL()
        #endif


    ENDIF


    b_TXD_Load_Required = FALSE //Reset before checking if the text label of the chosen text message requires a texture dictionary to be streamed in.



    //Image Header work - 2496045
    //Load in default image as a test.
 /*
    CurrentEmailMessageTXD = "05_b_sext_stripperJuliet" 

    b_TXD_Load_Required = TRUE


    #if IS_DEBUG_BUILD
        cdPrintstring("AppMPEmail - Requesting marketing email txd..")
        cdPrintnl()
    #endif


    REQUEST_STREAMED_TEXTURE_DICT(CurrentEmailMessageTXD)

    WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED(CurrentEmailMessageTXD)            
        WAIT (100)
        #if IS_DEBUG_BUILD
            cdPrintstring("AppMPEmail - Waiting on email TXD to load...")
            cdPrintnl()
    ENDWHILE
*/

    //Unreferenced bypass.
	/*
    IF ARE_STRINGS_EQUAL(g_EmailMessage[1].EmailLabel, EmailMessagesThatRequireTXDs_LABEL[1])
        EmailMessagesThatRequireTXDs_STRING[1] = CurrentEmailMessageTXD
        IF ARE_STRINGS_EQUAL (EmailMessagesThatRequireTXDs_STRING[1],CurrentEmailMessageTXD)
        ENDIF
        //Do nothing.
    ENDIF
*/


    
    IF g_EmailMessage[SingleMessageIndex].EmailLockStatus <> Email_EMPTY

        INT i_Count = 0

        WHILE i_Count < c_NumberOfTXD_Messages

            IF ARE_STRINGS_EQUAL(g_EmailMessage[SingleMessageIndex].EmailLabel, EmailMessagesThatRequireTXDs_LABEL[i_Count])


				IF IS_BOUNTY_HUNTER_MESSAGE( EmailMessagesThatRequireTXDs_LABEL[i_Count])
					CurrentEmailMessageTXD = "BAT_CASE_"
					CurrentEmailMessageTXD += g_bountyhunter_random_character 
					PRINTLN("[KW BOUNTYHUNTER]  g_bountyhunter_random_character = ",g_bountyhunter_random_character)
				ELSE
                	CurrentEmailMessageTXD = EmailMessagesThatRequireTXDs_STRING[i_Count] //If the labels match, get corresponding string from the partner array.
				ENDIF
				
                b_TXD_Load_Required = TRUE


                 

                REQUEST_STREAMED_TEXTURE_DICT(CurrentEmailMessageTXD)

          
                WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED(CurrentEmailMessageTXD)            
                    WAIT (100)
                    PRINTSTRING("AppMPEmail - Waiting on TXD to load...")
                    PRINTNL()
                ENDWHILE



                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Found Message that requires TXD at position ")
                    PRINTINT (i_Count)
                    PRINTNL()

                #endif

            ENDIF

            i_Count++

        ENDWHILE

    ENDIF
    

ENDPROC








PROC Check_For_Message_Selection() //Again, could pass stuff in here for context checking depending on screen. Will check all flash stuff.


    /*
    #if IS_DEBUG_BUILD 

        DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.15, "STRING", "List Cursor")

        DISPLAY_TEXT_WITH_NUMBER (0.20, 0.20, "CELL_500", ListCursorIndex)


		DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.35, "STRING", "NumTot")

        DISPLAY_TEXT_WITH_NUMBER (0.20, 0.40, "CELL_500", NumberOfMessagesInList)

		DISPLAY_TEXT_WITH_LITERAL_STRING (0.20, 0.45, "STRING", "NumFlow")

        DISPLAY_TEXT_WITH_NUMBER (0.20, 0.50, "CELL_500", NumberOfFlowMessagesInList)

    #endif
    */


    IF g_InputButtonJustPressed = FALSE

    IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT)) //Positive action - user opted to display single message options.   
    //OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
    OR g_LastEmailSentMustBeRead = TRUE
    OR g_ShouldForceSelectionOfLatestAppItem = TRUE // Temp removal for 3021274 - When using quick launch for a flow email the app could get stuck because the flow email is not in position zero.
    
        //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE
        IF g_LastEmailSentMustBeRead = FALSE
        AND g_ShouldForceSelectionOfLatestAppItem = FALSE

            Play_Select_Beep()  //therefore must have been a control press... avoid double beep. # 1593340
        
        ENDIF
        
        g_InputButtonJustPressed = TRUE
        

        b_RepeatButtonLimiter = FALSE


        //1636892 Make sure to switch this off initially when moving to single message view to get rid of open contact option in MP. It can be replaced later by other soft keys.
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
            11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



        
        
        #if IS_DEBUG_BUILD
            
            IF ListCursorIndex < 0
            
                SCRIPT_ASSERT ("ListCursorIndex less than zero error! Please bug Steve Taylor.")
                PRINTSTRING ("ListCursorIndex is less than zero! It is ")
                PRINTINT (ListCursorIndex)
                PRINTNL()

                cdPrintstring ("Warning - AppMPemail - ListCursorIndex is less than zero! It is ")
                cdPrintint (ListCursorIndex)
                cdPrintnl()

                                                              
            ELSE                      
                                 
                PRINTSTRING ("Array Index occupying this slot ")
                PRINTINT (MessageListSlot[ListCursorIndex])
                PRINTNL()

                cdPrintstring ("AppMPemail - Array Index occupying this slot  ")
                cdPrintint (MessageListSlot[ListCursorIndex])
                cdPrintnl()

            
            ENDIF

        #endif

           
        SingleMessageIndex = MessageListSlot[ListCursorIndex]





		//Trial change for 3025612
        //IF ListCursorIndex < i_Num_of_Marketing_Emails_Retrieved 	//This will need changed to a specific number of Eyefind marketing emails added if we need to support flow emails also.
                                                                 									//As it stands, there are no flow emails being sent, so anything that appears can be safely assumed to be and Eyefind email.
																									
		IF ListCursorIndex = NumberOfFlowMessagesInList  //We need to make sure to take different action on Social Club marketing emails if the list cursor is pointing to an index that houses a marketing message.
		OR ListCursorIndex > NumberOfFlowMessagesInList
																									
																									
            #if IS_DEBUG_BUILD
                cdPrintstring("AppMPemail - identifying selection of Marketing Email from message list ")
                cdPrintnl()  
                cdPrintint(i_Num_of_Marketing_Emails_Retrieved)
                cdPrintnl()
                cdPrintstring("STATE ASSIGNMENT 2936767933. AppMPEmail assigns PDS_COMPLEXAPP")
                cdPrintnl()   
            #endif

            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_COMPLEXAPP

                g_DoOptionsForSingleMessage= TRUE


                i_FullListSlotToReturnTo = ListCursorIndex


                //If we're including all senders from Eyefind messaging in the list use this one:
                //SC_EMAIL_GET_EMAIL_AT_INDEX (ListCursorIndex, single_email_Received_Data)

                //If we're just including those sent from Social Club sent to Eyefind then use this:
                SC_EMAIL_GET_EMAIL_AT_INDEX (MessageListSlot[ListCursorIndex], single_email_Received_Data)

            ENDIF


        
                    SETTIMERB(0)

                    b_SafetyValveFired = FALSE



                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


                    WHILE (NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                    AND (b_SafetyValveFired = FALSE)

                        WAIT (0)
                        
                        #if IS_DEBUG_BUILD

                            cdPrintstring("AppMPEmail - Waiting on return value from scaleform. ")
                            cdPrintstring( "Choice_ReturnedSFIndex is: ")
                            PRINTINT (NATIVE_TO_INT(Choice_ReturnedSFIndex))
                            cdPrintnl()
                            

                            IF NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                                cdPrintnl()
                                cdPrintString("Return value still not ready...")
                                cdPrintnl()
                            ENDIF


                        #endif

                        IF TIMERB() > 2000 
                            b_SafetyValveFired = TRUE
                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring("AppMPEmail - Waiting on return value from scaleform SAFETY VALVE fired!")
                                cdPrintnl()
                           
                            #endif

                        ENDIF

                    ENDWHILE



                    Check_if_TXD_Load_is_Required()



                    //Fill out the scaleform single message screen with the chosen marketing email...
                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)



                        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_1") //This doesn't display? See Eddie.


                        //To Address param...
       
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_TO_FIELD") //~a~
                            
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))

                        END_TEXT_COMMAND_SCALEFORM_STRING()

                
                        //Would check the email banner ad int here as requested in url:bugstar:2551529 to determine which sender name to use.
                        //ThisSenderName = "Dynasty 8"

                        //From Address
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_FROM_FIELD") //~a~

                                //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (ThisSenderName) //2517261 requested this string be used.
                                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.SenderName)

                        END_TEXT_COMMAND_SCALEFORM_STRING()
                
                        //No image built in.
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_EMAIL_BCON") //~a~

                        
                    
                        //Image Header work - 2496045 - put either the header or footer in and remove _bcon above. 
                        //BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_EMAIL_IMGH")  //Header Image.
                        //BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_EMAIL_IMGF")  //Footer Image.

                            //Using this one as a test.  This is embedded with the IMGH or IMGF text labels. We need to make sure we load the correct texture.
                            //EmailMessagesThatRequireTXDs_LABEL[1] = "SXT_JUL_2ND" 
                            //EmailMessagesThatRequireTXDs_STRING[1] = "05_b_sext_stripperJuliet" 


            
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[0])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[1])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[2])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[3])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[4])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[5])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[6])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[7])
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[8])
                            
                            //Image Header work - 2496045
                            //Too many substrings if we add this one and an image header.
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.Contents[9])
                                                       
                        END_TEXT_COMMAND_SCALEFORM_STRING()
                        
                
                
                
                        //Sign off
                        //Would check the email banner ad int here as requested in url:bugstar:2551529 to determine which sender name to use.
                        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_2000") //~a~

                            //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (ThisSenderName) //2517261 requested this string be used.
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (single_email_Received_Data.SenderName)

                        END_TEXT_COMMAND_SCALEFORM_STRING()

                        //Problems with this.
                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(CurrentEmailMessageTXD)
                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)

                        //But this works and will put an Elitas banner above the "To:" segment!
                        //Requested new banner header ads in bug 2550957 
                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Tourist_Info")
                        //SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)

                        // 2551529 - Display the vendor header image. Here's the list of numbers we are working from:
                        /*  2: EmailAds_Dock_Tease

                            3: EmailAds_Warstock

                            4: EmailAds_Legendary_Motorsport
                        
   5: EmailAds_LS_Customs
                        
   6: EmailAds_Elitas_Travel
                        
   7: EmailAds_LS_Tourist_Info

                            8: emailads_dynasty8

                            9: emailads_bennys
                        
   10: emailads_ammunation
                        
   11: emailads_SSSA
                        
   12: emailads_maze_bank
                        
   13: emailads_shark
                        
   14: emailads_RP 
                        
                            DebugCellphoneGui can be used as a test harness. Increment via RAG - Cellphone Scaleform selection as a placeholder incrementer via cellphone_controller
                        */

                        #if IS_DEBUG_BUILD
                            cdPrintstring("appMPEmail - Displaying single marketing message. Received Image decider INT is ")
                            cdPrintint(single_email_Received_Data.Image)
                            cdPrintnl()
                        #endif



                        SWITCH single_email_Received_Data.Image//Can use DebugCellphoneGui here to test...

                            CASE 0 //This is currently less than the default value of DebugCellphoneGui - don't include a header image
                                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Tourist_Info")												
                            BREAK


                            CASE 1 //This is currently the default value of DebugCellphoneGui - don't include a header image
                                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("Elistas")
                            BREAK

                            CASE 2
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Dock_Tease")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK
                            
                            CASE 3
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Warstock")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 4
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Legendary_Motorsport")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 5
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Customs")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 6
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Elitas_Travel")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 7
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Tourist_Info")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 8
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_dynasty8")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 9
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_bennys")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 10
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_ammunation")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 11
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_SSSA") //Southern Super Autos
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 12
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_maze_bank")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 13
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_shark")
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                            CASE 14
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_RP") //RP experience points.
                                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
                            BREAK

                        ENDSWITCH
                       
    

                    END_SCALEFORM_MOVIE_METHOD ()


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                    //Switch off positive button for Marketing emails.
                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other



                    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 9) //Set enum state and header to SINGLE MESSAGE



                    //IF ListCursorIndex = 0 //If the player selected the top item that assume all messages have been read...
                    IF ListCursorIndex = NumberOfFlowMessagesInList  //See 3025612. We need to make sure to take different action on Social Club marketing emails if the list cursor is pointing to an index that houses a marketing message. This fixes the unread badge indicator.
               

                        IF IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION) 


                            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION)

                            #if IS_DEBUG_BUILD
                                cdPrintstring("AppMPEmail - Email selection was list cursor zero - g_BSTU_MARKETING_EVENT_CUED_THIS_SESSION was set. Clearing bit!")
                                cdPrintnl()
                            #endif

                        ENDIF


                        IF g_NumberOfUnreadMarketingEmailsThisSession > 0

                            #if IS_DEBUG_BUILD
                                cdPrintstring("AppMPEmail - Email selection was list cursor zero - resetting unread total to 0.")
                                cdPrintnl()
                                cdPrintstring("Updating profile stat with latest posix ")
                                cdPrintint(i_LatestPosixTime)
                            #endif

                     
                            SET_MP_INT_CHARACTER_STAT (MP_STAT_LAST_READ_EMAIL_POSIX, i_LatestPosixTime)
                   
                            IF g_NumberOfUnreadMarketingEmailsThisSession > 0
                                g_NumberOfUnreadMarketingEmailsThisSession = 0
                            ENDIF

                        ENDIF
                    
                    ENDIF              


            EXIT

        ENDIF











        //Only proceed to single message display and options if the index points to a full slot.
        IF g_EmailMessage[SingleMessageIndex].EmailLockStatus <> Email_EMPTY




            IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                g_Cellphone.PhoneDS = PDS_COMPLEXAPP


            
                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 29. AppMPEmail assigns PDS_COMPLEXAPP")
                    cdPrintnl()   
                #endif

            ENDIF





      
            //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
            IF g_LastEmailSentMustBeRead = TRUE
            OR g_ShouldForceSelectionOfLatestAppItem = TRUE //Temp removal for 3021274 - When using quick launch for a flow email the app could get stuck because the flow email is not in position zero.

                SingleMessageIndex = MessageListSlot[0] //make sure we pick the top slot for a critical message or the most recent force selection from elongated d-pad up press.
            
                i_FullListSlotToReturnTo = 0


                //IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
                IF g_LastEmailSentMustBeRead = TRUE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("AppMPEmail - Last message sent must be read was TRUE, so setting slot selection to zero.")
                        cdPrintnl()
                   
                    #endif

                ELSE

                    #if IS_DEBUG_BUILD

                        cdPrintnl()
                        cdPrintstring("AppMPEmail - g_ShouldForceSelectionOfLatestAppItem was TRUE, so setting slot selection to zero.")
                        cdPrintnl()
               
                    #endif

                ENDIF

                g_ShouldForceSelectionOfLatestAppItem = FALSE //Critical!


            ELSE

                    SETTIMERB(0)

                    b_SafetyValveFired = FALSE



                    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

                    Choice_ReturnedSFIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()


                    WHILE (NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                    AND (b_SafetyValveFired = FALSE)

                        WAIT (0)
                        
                        #if IS_DEBUG_BUILD

                            cdPrintstring("AppMPEmail - Waiting on return value from scaleform. ")
                            cdPrintstring( "Choice_ReturnedSFIndex is: ")
                            PRINTINT (NATIVE_TO_INT(Choice_ReturnedSFIndex))
                            cdPrintnl()
                            

                            IF NOT (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (Choice_ReturnedSFIndex))
                                cdPrintnl()
                                cdPrintString("Return value still not ready...")
                                cdPrintnl()
                            ENDIF


                        #endif

                        IF TIMERB() > 2000 
                            b_SafetyValveFired = TRUE
                            #if IS_DEBUG_BUILD

                                cdPrintnl()
                                cdPrintstring("AppMPEmail - Waiting on return value from scaleform SAFETY VALVE fired!")
                                cdPrintnl()
                           
                            #endif
                        ENDIF

                    ENDWHILE

            
                    IF b_SafetyValveFired = TRUE

                        sf_TempInt = 0

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("AppMPEmail - Safety valve fired setting slot selection to zero.")
                            cdPrintnl()
                       
                        #endif

                    ELSE

                        sf_TempInt = (GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (Choice_ReturnedSFIndex))
                    

                        #if IS_DEBUG_BUILD

                            cdPrintnl()
                            cdPrintstring("AppMPEmail - sf_TempInt is ")
                            cdPrintInt(sf_TempInt)
                            cdPrintnl()
                       
                        #endif


                    ENDIF





                    IF sf_TempInt < 0
                           
                        #if IS_DEBUG_BUILD

                            cdPrintstring("AppMPEmail - Choice_ReturnedSFIndex is less than zero for some reason! Setting to zero as safeguard.")
                            cdPrintnl()

                        #endif

                        sf_TempInt = 0

                    ENDIF


                    //Scaleform slot return looks faulty at Eddie's side. It seems to return zero regardless on the first selection after you've gone back and restored the last position after reading an email.
                    //See bug 1727361
                    
                    //SingleMessageIndex = MessageListSlot[sf_TempInt]

                    //i_FullListSlotToReturnTo = sf_TempInt

                
                    SingleMessageIndex = MessageListSlot[ListCursorIndex]

                    i_FullListSlotToReturnTo = ListCursorIndex





            ENDIF




            //Was originally here, put scaleform not returning pish means I don't even check for a return value if the last message was critical...
            /*
            IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = TRUE
                SingleMessageIndex = MessageListSlot[0] //make sure we pick the top slot for a critical message
            
                i_FullListSlotToReturnTo = 0

            ENDIF   
            */
            


            //Read - Unread status. Alter unread status here for selected message. Any message that reaches single message display is marked as read.

            g_EmailMessage[SingleMessageIndex].EmailReadStatus = READ_MPEMAIL






                //Reset hyperlink buffer and local bool to "empty" before testing for hyperlink availability.
                ThisMessageContainsHyperlink = FALSE
                g_HyperLink_Buffered_Label = "NO_HYPERLINK"


                TEXT_LABEL_63 HyperlinkCheck_Label = g_EmailMessage[SingleMessageIndex].EmailLabel

                HyperlinkCheck_Label += "_LINK"



                IF DOES_TEXT_LABEL_EXIST (HyperlinkCheck_Label)

                    g_HyperLink_Buffered_Label = HyperlinkCheck_Label


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppMPEmail - Found valid hyperlink label for selected textmessage. Updating buffer and locking message!")
                        cdPrintnl()
                        cdPrintstring ("AppMPEmail - Hyperlink label contents are ")
                        cdPrintstring (GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label))
                        cdPrintnl()

                    #endif

                    
                    ThisMessageContainsHyperlink = TRUE


                    //Update centre soft key with 16 - internet world icon.
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_267") //"Link" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                
                    g_EmailMessage[SingleMessageIndex].EmailAutoUnlockStatus = Email_DO_NOT_AUTO_UNLOCK

                    g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_LOCKED

                ENDIF















            IF g_EmailMessage[SingleMessageIndex].EmailAutoUnlockStatus = Email_AUTO_UNLOCK_AFTER_READ //automatically unlock if set.
                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED
            ENDIF

    

            Check_if_TXD_Load_is_Required()


            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)


            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_1") //This doesn't display? See Eddie.


            //To Address param...
       
            BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_TO_FIELD") //~a~
                
                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(PLAYER_ID()))

            END_TEXT_COMMAND_SCALEFORM_STRING()










            //From Address param...
            IF g_EmailMessage[SingleMessageIndex].EmailSender = NO_CHARACTER
                                            
                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_EmailMessage[SingleMessageIndex].EmailSenderStringComponent)

                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_FROM_FIELD") //~a~
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSenderStringComponent)

                END_TEXT_COMMAND_SCALEFORM_STRING()

            ELSE
                
                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_FROM_FIELD") //~a~

                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (g_sCharacterSheetAll[g_EmailMessage[SingleMessageIndex].EmailSender].label)
                
                END_TEXT_COMMAND_SCALEFORM_STRING()

            ENDIF
            //End of From Address




 


            SWITCH g_EmailMessage[SingleMessageIndex].EmailSpecialComponents

                CASE NO_SPECIAL_COMPONENTS

                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_EmailMessage[SingleMessageIndex].EmailLabel)

                BREAK
            
        
                CASE STRING_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_EmailMessage[SingleMessageIndex].EmailLabel)
                
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailStringComponent)
        
                    //Check if any additional strings are required...
                    IF g_EmailMessage[SingleMessageIndex].EmailNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)   

                    ENDIF


                    IF g_EmailMessage[SingleMessageIndex].EmailNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent, "NULL"))

                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                       // ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)        
                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)        
                        ENDIF
                    ENDIF


                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK


                CASE NUMBER_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_EmailMessage[SingleMessageIndex].EmailLabel)
                
                    ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[SingleMessageIndex].EmailNumberComponent)

                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK



                CASE STRING_AND_NUMBER_COMPONENT

                    BEGIN_TEXT_COMMAND_SCALEFORM_STRING(g_EmailMessage[SingleMessageIndex].EmailLabel)
                
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailStringComponent)
                    ADD_TEXT_COMPONENT_INTEGER (g_EmailMessage[SingleMessageIndex].EmailNumberComponent)

                    //Check if any additional strings are required...
                    IF g_EmailMessage[SingleMessageIndex].EmailNumberOfAdditionalStrings = 1
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent, "NULL"))

                        ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)   

                    ENDIF


                    IF g_EmailMessage[SingleMessageIndex].EmailNumberOfAdditionalStrings = 2
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent, "NULL"))
                    AND NOT (ARE_STRINGS_EQUAL ( g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent, "NULL"))

                        //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                        //ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)        

                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSecondStringComponent)
                        ENDIF
                        IF DOES_TEXT_LABEL_EXIST(g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)
                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)
                        ELSE
                            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailThirdStringComponent)        
                        ENDIF

                    ENDIF



                    END_TEXT_COMMAND_SCALEFORM_STRING()

                BREAK


                CASE CAR_LIST_COMPONENT
                                            
                     Create_Car_List_String_For_Scaleform (SingleMessageIndex)

                BREAK



                CASE SUPERAUTOS_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE LEGENDARY_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE PEDALMETAL_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE WARSTOCK_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE ELITAS_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE DOCKTEASE_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK


                CASE DAILYOBJ_LIST_COMPONENT
                
                    Create_Car_List_String_For_Scaleform(SingleMessageIndex)

                BREAK

            ENDSWITCH
            



            //Sign off Param... reusing "from address" here...
            IF g_EmailMessage[SingleMessageIndex].EmailSender = NO_CHARACTER
                                            
                //SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_EmailMessage[SingleMessageIndex].EmailSenderStringComponent)

                BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CELL_2000") //~a~
                    
                    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_EmailMessage[SingleMessageIndex].EmailSenderStringComponent)

                END_TEXT_COMMAND_SCALEFORM_STRING()

            ELSE

                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(g_sCharacterSheetAll[g_EmailMessage[SingleMessageIndex].EmailSender].label)

            ENDIF

			//Trial for adding Maze Bank banner to the top of the email.
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_MAZE_MPEMAIL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_maze_bank")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF


			//Add Warstock banner if the email originates from them.
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_MILSITE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Warstock")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF


			//Add Arena banner if the email originates from them.
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_ARENA
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_arena")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF


			//Add Casino banner if the email originates from them.
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_CASINO_MPEMAIL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_diamond")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF
			
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_LS_CAR_MEET_MPEMAIL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("emailads_ls_car_meet")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF
			
			#IF FEATURE_DLC_1_2022
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_LUXURY_AUTOS_MPEMAIL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("SUM2_EMAIL_LUXURY_AUTOS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF
			
			IF g_EmailMessage[SingleMessageIndex].EmailSender = CHAR_PREMIUM_DELUXE_MPEMAIL
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("SUM2_EMAIL_PREMIUM_DELUXE")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			ENDIF
			#ENDIF


            END_SCALEFORM_MOVIE_METHOD ()























            g_DoOptionsForSingleMessage= TRUE


            //LEGACY_SCALEFORM_MOVIE_METHOD_WITH_STRING (SF_MovieIndex, "SET_HEADER", "CELL_INBOX") //"Texts"


            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 9) //Set enum state and header to SINGLE MESSAGE
     

            //Viewed message in single message view. Ok to remove from feed. See bug 986286 
            THEFEED_REMOVE_ITEM (g_EmailMessage[SingleMessageIndex].EmailFeedEntryId)


            
            //Set up specific buttons for those text messages which require a reply...
            IF g_EmailMessage[SingleMessageIndex].EmailReplyStatus > MPE_NO_REPLY_REQUIRED
             
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"Yes"
                */

                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_212") //"YES" - Positive
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                            13, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"YES" - Positive
                    ENDIF


            ELSE
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 1, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" Positive       
                */  
                    
            
                    //Badger

                    //If this text message has been set up to allow the sender to be called, the default setting, then display the call button..
                    IF g_EmailMessage[SingleMessageIndex].EmailCanCallSenderStatus = MPE_CAN_CALL_SENDER

                        IF g_b_ToggleButtonLabels
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //"CALL" - Positive
                        ELSE 
                            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"CALL" - Positive
                        ENDIF

                    ELSE //Switch positive soft key off...
            
                         LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_POS, 0,
                            1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                    ENDIF

                    
            ENDIF



            IF g_EmailMessage[SingleMessageIndex].EmailReplyStatus > MPE_NO_REPLY_REQUIRED
            
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1,
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"No"
                */

                IF ARE_STRINGS_EQUAL (g_EmailMessage[SingleMessageIndex].EmailLabel, "CELL_FINV") //Special mp race invite does not allow "no" response.
                      
                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 0,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                    ENDIF

                ELSE

                    //Badger

                    //This sets the NO button for an email that requires a reply. Keith requested that rather than having an explict no, we just use the back button.
                    //The delete button can effectively act as a "no". See  GTA5 DLC bug 1727773

                    //Display "no" soft key
                    /*
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_213") //"NO" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            14, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"NO" - Negative
                    ENDIF
                    */


                    //So we just use the "back" function instead
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)//"BACK" - Negative
                    ENDIF






                ENDIF


            ELSE

                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 2, 1, 
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative
                */

                //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_206") //"BACK" - Negative
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_NEG, 1,
                            4, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)//"BACK" - Negative
                    ENDIF

                

            ENDIF            








            















            //Make sure that the delete option is only available if the text message concerned is in unlocked state. Check second int parameter after method name.
            //Hyperlinked messages must be locked due to button conflicts.
            IF g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_LOCKED 

           
                /* Trial move above - see 764336
                //Reset hyperlink buffer and local bool to "empty" before testing for hyperlink availability.
                ThisMessageContainsHyperlink = FALSE
                g_HyperLink_Buffered_Label = "NO_HYPERLINK"


                TEXT_LABEL_63 HyperlinkCheck_Label = g_EmailMessage[SingleMessageIndex].EmailLabel

                HyperlinkCheck_Label += "_LINK"



                IF DOES_TEXT_LABEL_EXIST (HyperlinkCheck_Label)

                    g_HyperLink_Buffered_Label = HyperlinkCheck_Label


                    #if IS_DEBUG_BUILD

                        cdPrintstring ("AppMPEmail - Found valid hyperlink label for selected textmessage. Updating buffer.")
                        cdPrintnl()
                        cdPrintstring ("AppMPEmail - Hyperlink label contents are ")
                        cdPrintstring (GET_FILENAME_FOR_AUDIO_CONVERSATION(g_HyperLink_Buffered_Label))
                        cdPrintnl()

                    #endif

                    
                    ThisMessageContainsHyperlink = TRUE


                    //Update centre soft key with 16 - internet world icon.
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_267") //"Link" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            16, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)



                ENDIF
                */
                    



                //Check is this is a barter message. Hyperlinked messages should not be barter messages also.
                IF g_EmailMessage[SingleMessageIndex].EmailBarterStatus =  MPE_BARTER_IS_REQUIRED
            

                    ThisMessageContainsHyperlink = FALSE //failsafe to make sure barter takes precedence if text labels are screwy.


                    //Update centre soft key with 11 - temp Barter icon.
                
                    //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_264") //"Barter" - Other
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            11, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) 
                    ENDIF

                    SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                ELSE

                    IF ThisMessageContainsHyperlink = FALSE //make sure hyperlink soft key decision isn't reversed if this isn't a barter message.

                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 0,
                        1, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM) //"Error!" - Other
                        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)

                    ENDIF

                ENDIF



            
            ELSE
            
                //Monochrome
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieButtonsIndex, "SET_SOFT_KEYS", 3, 1, 
                    INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_216") //"DELETE" - Other
                */

                  //Badger
                    IF g_b_ToggleButtonLabels
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            12, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_216") //"DELETE" - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                    ELSE 
                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                            12, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM)//"DELETE" - Other
                        SET_BIT (BitSet_CellphoneDisplay, g_BS_OTHER_OPTION_IS_DISPLAYED)
                    ENDIF


            
            
            ENDIF


            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE           
            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE
            g_LastEmailSentMustBeRead = FALSE

        
        ELSE   //IF g_EmailMessage[SingleMessageIndex].EmailLockStatus <> Email_EMPTY check way above!

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING("AppMPEmail WARNING! - g_EmailMessage[SingleMessageIndex].EmailLockStatus was EMPTY")
                PRINTNL()
                PRINTSTRING("AppMPEmail WARNING! - but g_LastEmailSentMustBeRead may have been TRUE. Setting to false for safety. See bug 1557146")
                PRINTNL()

            #endif
            
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE           
            //g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead = FALSE
            g_LastEmailSentMustBeRead = FALSE

            
    
            //Used to just do this... put phone away instantly.
            //g_Cellphone.PhoneDS = PDS_AWAY


            //If the fix causes problems then could conceivably mimic the check for application routine. See bug 1725903
            
            //g_TerminateApp = TRUE Include this, then a copy of the section from cellphone_flashhand which updates VSE.

            //g_b_Rotate3dPhonePortrait = TRUE //will trigger a clean up and terminate after rotation has finished.



        ENDIF



        ENDIF





        IF g_LaunchContactsFromTextMessage = FALSE   //1636892
        
            IF g_bInMultiplayer

                
                //If I allow auto_launch of contacts in the future using the g_LaunchContactsFromTextMessage system then re-establish the section between the comment stars.
                //Do NOT re-include the Cleanup. We'll also need to rotate back to portrait before setting g_LaunchContactsFromTextMessage to true.


                /*

                //Trial for 1687668
                SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, (INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)))


                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_SPECIAL_OPTION_INPUT)) 

                    #if IS_DEBUG_BUILD

                        cdPrintstring("AppMPEmail - g_LaunchContactsFromTextMessage has been set to TRUE. Will cleanup text message script accordingly!")
                        cdPrintnl()

                    #endif


                    g_InputButtonJustPressed = TRUE //This will let Cellphone_Flashhand know that we want to launch the contacts app immediately after exiting

                    
                    
                    //Would need to rotate here!

                    
                    g_LaunchContactsFromTextMessage = TRUE

                    //Cleanup_and_Terminate() //Don't need this as cellphone_flashhand's CHECK_FOR_APPLICATION_EXIT routines detect g_LaunchContactsFromTextMessage returning true.


                ENDIF

                */

            ENDIF

        ENDIF






   
    ENDIF //End of g_InputButtonJustPressed = FALSE condition check








ENDPROC







PROC DisplaySingleMessage_and_Options()


    Display_Selected_Message()
                   

    #IF IS_DEBUG_BUILD
            
        IF g_DoDebugTempDraw_Phone = TRUE

            Draw_SingleMessageOptions_HelpText()
    
        ENDIF

    #endif


ENDPROC





PROC Update_Message_List()


  
    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE



  


    Play_Back_Beep()

    g_InputButtonJustPressed = TRUE


    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)

    Place_Existing_Messages_In_Slots() //Update the slot list to reflect removal of deleted message.

    
    Reinitialise_Text_Main_Screen()


    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_CellPhone.PhoneDS = PDS_RUNNINGAPP
        g_DoOptionsForSingleMessage= FALSE //Return to full display list.


        
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 30. AppMPEmail assigns PDS_RUNNINGAPP")
            cdPrintnl()   
        #endif

    ENDIF



ENDPROC







SCRIPT



    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


    /*
    REQUEST_ADDITIONAL_TEXT ("TMSG", PHONE_TEXT_SLOT) //Load in additional text block that has been passed in by the requesting script and copied into 
    WHILE NOT HAS_ADDITIONAL_TEXT_LOADED (PHONE_TEXT_SLOT)  //the permanent global from the temporary holder.
        WAIT(0)
    ENDWHILE
    */


    
    //Tricky timing fix for Bug 110732
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

        Cleanup_and_Terminate ()    

    ENDIF



    Set_Up_Messages_TXD_Relationship()







    //Important! Make sure full display list is defaulted to on startup of this script.
    g_DoOptionsForSingleMessage= FALSE //Return to full display list.


    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LaunchToTextMessageScreen = FALSE



    g_DisplayNewSideTaskSignifier = FALSE








    //New work to retrieve SC Emails from server for marketing purposes. Requested by 2301166
    Retrieve_Priority_SC_Marketing_Emails()










    

    
    LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)

  
    Place_Existing_Messages_In_Slots()


    Reinitialise_Text_Main_Screen()
    
  


    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_Cellphone.PhoneDS = PDS_RUNNINGAPP //Failsafe - makes sure that the cellphone is initialised into running state. 

            
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 31. AppMPEmail assigns PDS_RUNNINGAPP")
            cdPrintnl()   
        #endif

    ENDIF




    RepeatButtonLimiter_StartTime = GET_GAME_TIMER()
    



    Previous_SelectedAppCursorPos_pageOne = 0  //Make sure that on return to the homescreen from a dpad_up activatable feed selection, the app cursor is over email on the homescreen.



    GET_MOBILE_PHONE_POSITION (v_3dEmailOriginalPosition)
    
    v_3dEmailDesiredPosition = v_3dEmailOriginalPosition
    // original value was -6. Text-readable size is -10.
    v_3dEmailDesiredPosition.x -= 10.0 //Specifies the size of the left adjustment to make sure the entire body text is visible in landscape mode.

    v_3dEmailDesiredPosition.y += 20.0 //Fixes TRC bombshell with new anims by making sure the landscape phone lies above any long or four line subtitles - in German!




    g_b_Rotate3dPhonePortrait = FALSE //Failsafe.

    g_b_Rotate3dPhoneLandscape = TRUE //Force the phone to rotate into landscape mode.


    WHILE TRUE

        WAIT(0)
        
        #if IS_DEBUG_BUILD
            //DISPLAY_TEXT_WITH_NUMBER (0.70, 0.35, "CELL_500", ListCursorIndex)
            //DISPLAY_TEXT_WITH_NUMBER (0.7, 0.4, "CELL_500", NumberOfMessagesInList)    
        #endif

        IF g_b_Rotate3dPhoneLandscape = FALSE
        AND g_b_Rotate3dPhonePortrait = TRUE

            RotatePhoneToPortrait_and_Exit()

        ENDIF


  
        IF g_b_Rotate3dPhoneLandscape = TRUE
        AND g_b_Rotate3dPhonePortrait = FALSE

            RotatePhoneToWidescreen()

        ENDIF



        


        IF b_RepeatButtonLimiter = FALSE

            RepeatButtonLimiter_CurrentTime = GET_GAME_TIMER()


            IF RepeatButtonLimiter_CurrentTime - RepeatButtonLimiter_StartTime > 500


                b_RepeatButtonLimiter = TRUE
    
            ENDIF   
        ENDIF




        


   
        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.
        AND g_b_Rotate3dPhoneLandscape = FALSE
        AND g_b_Rotate3dPhonePortrait = FALSE

            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP

                    IF g_DoOptionsForSingleMessage= FALSE //shouldn't need to check this providing PDS_COMPLEXAPP state has been set.

                        #IF IS_DEBUG_BUILD
                            IF g_DoDebugTempDraw_Phone = TRUE

                                Display_Message_List()
                                
                            ENDIF
                        #endif
                                
                                             
                        IF g_LaunchContactsFromTextMessage = FALSE

                            Check_for_List_Navigation()                                      
                                              
                            Check_For_Message_Selection()

                        ENDIF
                        

                    ENDIF

                BREAK



                CASE PDS_COMPLEXAPP
                    
                    IF g_DoOptionsForSingleMessage = TRUE

                        DisplaySingleMessage_and_Options()

                        Check_for_Single_Message_List_Navigation()      //Was removed at one time .See bug 956980. 
                                                                        //Then, navigation not needed in single message view, now in because of long text messages.

                    ENDIF
                                            
                BREAK



                DEFAULT

                    
                    #IF IS_DEBUG_BUILD

                        PRINTSTRING ("AppMPEmail in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK


            ENDSWITCH

     


            



            
            
            //Only check for this if not in full list display mode

            IF g_DoOptionsForSingleMessage = FALSE

                IF CHECK_FOR_APPLICATION_EXIT() //will only terminate if this script is in PDS_RUNNINGAPP rather than PDS_COMPLEXAPP state.
                
                        #if IS_DEBUG_BUILD
                            cdPrintstring("AppMPEmail - Check_For_Application_Exit returning TRUE. Will exit.")
                            cdPrintnl()
                        #endif

            

                    g_b_Rotate3dPhonePortrait = TRUE //will trigger a clean up and terminate after rotation has finished.
                    //Cleanup_and_Terminate()

                ENDIF

            ELSE
                

                //IF g_InputButtonJustPressed = FALSE  //nasty timing issue if this check is left in...
                IF Is_Phone_Control_Just_Pressed (FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_GO_BACK_INPUT)) //Go back action - user opted to return to full text message list from single message display.
                OR IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW)                    


                    IF ARE_STRINGS_EQUAL (g_EmailMessage[SingleMessageIndex].EmailLabel, "CELL_FINV") //Special mp race invite.
                      
                              
                        #if IS_DEBUG_BUILD
                            cdPrintstring("AppMPEmail - Not taking action on back button press as reply is forced for CELL_FINV text. See bug 923977")
                            cdPrintnl()
                        #endif


                    ELSE

                        WAIT(0) //Make sure the JUST_PRESSED check cannot return true to any subesquent checks until a second press is made.

                        
                        //See bug 892983. If MP wants to delete an invite text after a response window timeouts, setting this bit allows them to force the text message app to return
                        //to the full message view. Provided they have deleted the text message before calling the public function to set this bit, the full list view will be updated to
                        //reflect the removal of the text.
                        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW) 

                        Play_Back_Beep()

                        g_InputButtonJustPressed = TRUE

                        g_DoOptionsForSingleMessage= FALSE

                        
                        //See bug 316474
                        IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

                            g_Cellphone.PhoneDS = PDS_RUNNINGAPP

            
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("STATE ASSIGNMENT 32. AppMPEmail assigns PDS_RUNNINGAPP")
                                cdPrintnl()   
                            #endif

                        ENDIF

                        //Fix for bug 110732
                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

                            Cleanup_and_Terminate()
                                            
                        ENDIF



                        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 8)


                        Place_Existing_Messages_In_Slots()  //repopulate slots after possible deletion.


                        Reinitialise_Text_Main_Screen()

                        //CRITICAL!
                        //This section modifies what happens to the single message status after the back button has been pressed from single message view.


                        IF g_EmailMessage[SingleMessageIndex].EmailReplyStatus > MPE_NO_REPLY_REQUIRED

                            IF g_EmailMessage[SingleMessageIndex].EmailBarterStatus = MPE_BARTER_IS_REQUIRED

                                
                                #if IS_DEBUG_BUILD
                                    cdPrintstring("AppMPEmail - NO reply set for Barter text message")
                                    cdPrintnl()
                                #endif

                                
                                //Currently the same behaviour as a standard replay message. A reply is forced, in this case "no". The message can then be deleted by the user 
                                //after unlocking the message.
                                
                                g_EmailMessage[SingleMessageIndex].EmailReplyStatus = MPE_REPLIED_NO

                                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED
                                

                            ELSE

                                //Keith has requested that no reply action is taken for emails at present. See bug 1727773
                                //g_EmailMessage[SingleMessageIndex].EmailReplyStatus = MPE_REPLIED_NO

                                g_EmailMessage[SingleMessageIndex].EmailLockStatus = Email_UNLOCKED //This should be done by flow - probably!

                            ENDIF


                        ENDIF

                    ENDIF


                ENDIF //End of phone control just pressed check. 
                


               


            ENDIF
  
        ELSE


            //Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
            //return to the main menu post-phonecall.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.

            //BeforeCallPhoneDS = PDS_MAXIMUM //After a call has finished the phone display state alters to whatever it was before the call came through.
                                            //We don't really want to go back to the "calling contact" screen after an answerphone message so we set it to PDS_MAXIMUM
                                            //here. We could just as easily put the phone completely away if need be.
                                            //NB - This also means that when browsing the contacts list, if a calls comes through and it terminates then we default
                                            //back to MAXIMUM mode not browsing the contacts list.

            //TERMINATE_THIS_THREAD() //Likewise, having this here will make sure this script is terminated should any call come through... 
          

        ENDIF


        //Fix for bug 110732
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 

            Cleanup_and_Terminate()    //Flashhand cannot exit if an application thread has launched but hasn't been cleaned up so this is a weird timing bug.
                                        
        ENDIF



        IF CHECK_FOR_ABNORMAL_EXIT()

           Cleanup_and_Terminate()

        ENDIF



        #if IS_DEBUG_BUILD

            //draw_debug_screen_grid()

            //cdPrintstring(GET_PLAYER_NAME(PLAYER_ID()))
            //cdPrintnl()


        #endif



        
            
    ENDWHILE
    
    
ENDSCRIPT


