USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "cellphone_public.sch"
USING "cellphone_private.sch"
USING "stack_sizes.sch"



       






// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   appBlank.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Template script for each app to be launched from cellphone.
//                          
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************






PROC Cleanup_and_Terminate()

    //Make sure you terminate any audio! 


    TERMINATE_THIS_THREAD ()


ENDPROC





SCRIPT

    //Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    WHILE TRUE

        WAIT(0)
        
     
     

        IF g_Cellphone.PhoneDS <> PDS_ONGOING_CALL //i.e a phonecall is not attempting to get through to the player.


            SWITCH g_Cellphone.PhoneDS
             
            
                CASE PDS_RUNNINGAPP

                    //Example placement of procedures and funcs.

                    //Display_Message_List()
                    //Check_for_List_Navigation()                                      
                    //Check_For_Message_Selection()
               
                BREAK



         
                DEFAULT

                    #IF IS_DEBUG_BUILD

                        PRINTSTRING("App Blank in default state. Should terminate if exit check in place.")
                        PRINTNL()

                    #endif

                BREAK





            ENDSWITCH

     
            
            
            //This will automatically exit the cellphone if the back button is pressed. If you move two screens deep into your app, then you must set your app state,
            //g_Cellphone.PhoneDS to PDS_COMPLEXAPP so this doesn't return true.
            IF CHECK_FOR_APPLICATION_EXIT()

                Cleanup_and_Terminate()

            ENDIF

       
        ELSE

            //Update: Incoming calls can't currently go through to the phone whilst an app is running. If this changes, we'll need to reinstate it.

            //Reinstate the block below to make the phone terminate this script when a phonecall comes through so that the phone will
            //return to the main menu post-phonecall.

            //This is an important section! It tells this script what to do when a call, be it answerphone or other phonecall, comes through.

            //BeforeCallPhoneDS = PDS_MAXIMUM
                    

            //Cleanup_and_Terminate()

        ENDIF



        //Critical! Do not remove. This must run every frame as it handles bad things such as the player dying when the phone is active.
        IF CHECK_ABNORMAL_EXIT()

            Cleanup_and_Terminate()

        ENDIF



        
            
    ENDWHILE
    
    
ENDSCRIPT


