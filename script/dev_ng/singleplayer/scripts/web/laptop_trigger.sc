// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "script_player.sch"
USING "brains.sch"
USING "commands_pad.sch"
USING "website_public.sch"
USING "heading_push_trigger_public.sch"
USING "context_control_public.sch"
USING "net_simple_interior.sch"
USING "rc_helper_functions.sch"

// Variables
INT myContextIntention = NEW_CONTEXT_INTENTION
BOOL bOnCallCleared = FALSE
BOOL bOnAMission	= FALSE
BOOL bGangMemberLF	= FALSE
BOOL bPlayedLoginSound = FALSE

SCRIPT_TIMER stMissionCDTimer
SCRIPT_TIMER stWHSyncSceneTimer
SCRIPT_TIMER stWHWarpOutForSellMissionStart

OBJECT_INDEX oLaptop
	
INT iLaptopSynchSceneStage = 0
INT iLaptopSynchSceneID
VECTOR vSynchSceneOffset = <<0,0,0>>
VECTOR vSynchSceneOrigin
TEXT_LABEL tPreviousIdleAnim

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID d_wLaptopWidget
	BOOL d_bLaptopTestAnims
#ENDIF

/// PURPOSE:
///    Checks distance between two specified entities
FUNC BOOL ATM_RANGE_CHECK(ENTITY_INDEX a, ENTITY_INDEX b)
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(a),GET_ENTITY_COORDS(b)) < 2.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if we are currently showing any securo laptop help text
FUNC BOOL IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_OFF_LAP_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WHPRIVSESLAP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WHSECUROBLCK")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SECINPUTTREGLAP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIKERWHINPUT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIKERWHBLCKA")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIKERWHBLCKB")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WHBIKERBLCK")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIKERWHBLCKC")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BIKERWHBLCKD")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENAWHINPUT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    General cleanup
PROC CLEANUP()
	IF myContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(myContextIntention)
	ENDIF
	
	IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
		CLEAR_HELP()
	ENDIF
	
	g_bLaunchedMissionFrmLaptop 		= FALSE
	g_bWarehouseLaptopContextTriggered 	= FALSE
ENDPROC

ENUM eATM_ANIM_SET
	eAnimSet_enter = 0,		// enter
//	eAnimSet_base,			// base
	eAnimSet_idle,			// idle_a [3:idle_b, 4:idle_c, 5:idle_d]
	eAnimSet_exit			// exit
ENDENUM

FUNC BOOL Get_Laptop_Anim_Dict_Names(eATM_ANIM_SET eAnimset, TEXT_LABEL_63 &tLaptopAnimDict)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		tLaptopAnimDict = ""
		RETURN FALSE
	ENDIF
	IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		tLaptopAnimDict = ""
		RETURN FALSE
	ENDIF
	IF NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Get_Laptop_Anim_Dict_Names not in warehouse")
		tLaptopAnimDict = ""
		RETURN FALSE
	ENDIF
	
	UNUSED_PARAMETER(eAnimset)
	tLaptopAnimDict = "anim@amb@warehouse@laptop@"
	RETURN TRUE
	
ENDFUNC

PROC Control_Synch_Scene(OBJECT_INDEX oIn)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	IF NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oIn)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tAtmEnterAnimDict, tAtmIdleAnimDict, tAtmExitAnimDict
	
	Get_Laptop_Anim_Dict_Names(eAnimSet_enter, tAtmEnterAnimDict)
	Get_Laptop_Anim_Dict_Names(eAnimSet_idle, tAtmIdleAnimDict)
	Get_Laptop_Anim_Dict_Names(eAnimSet_exit, tAtmExitAnimDict)
	
	REQUEST_ANIM_DICT(tAtmExitAnimDict)
	REQUEST_ANIM_DICT(tAtmEnterAnimDict)
	
	STRING sBase = "idle_a"		//"base"
	
	INT iMaxIdleAnims = 1
	IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
		iMaxIdleAnims = 1
	ENDIF
	
	INT iLocalLaptopSynchSceneID
	
	CONST_INT iCONST_STAGE_0_walk		0
	CONST_INT iCONST_STAGE_1_enter		1
	CONST_INT iCONST_STAGE_3_idle		3
	
	SWITCH iLaptopSynchSceneStage
		CASE iCONST_STAGE_0_walk
		
			bPlayedLoginSound = FALSE
		
			DRAW_DEBUG_TEXT("walk", GET_ENTITY_COORDS(PLAYER_PED_ID()), 064, 256, 064, 064)//////green
			
			IF HAS_ANIM_DICT_LOADED(tAtmEnterAnimDict)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
				AND HAS_ANIM_DICT_LOADED(tAtmEnterAnimDict)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
						DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
						iLaptopSynchSceneID = -1
					ENDIF
					iLaptopSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSynchSceneOrigin, 
										GET_ENTITY_ROTATION(oIn),DEFAULT,FALSE,FALSE)
										
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
							tAtmEnterAnimDict, "enter",
							SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)

					NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
					
					REMOVE_ANIM_DICT(tAtmEnterAnimDict)
					
					iLaptopSynchSceneStage = iCONST_STAGE_1_enter
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_STAGE_1_enter
			DRAW_DEBUG_TEXT("enter", GET_ENTITY_COORDS(PLAYER_PED_ID()), 064, 256, 064, 064)//////green
			
			REQUEST_ANIM_DICT(tAtmIdleAnimDict)
			
			iLocalLaptopSynchSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLaptopSynchSceneID)
	
			IF NOT bPlayedLoginSound
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalLaptopSynchSceneID) >= 0.93
					PLAY_SOUND_FRONTEND(-1, "Login", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
					
					bPlayedLoginSound = TRUE
				ENDIF
			ENDIF
			
			IF HAS_ANIM_DICT_LOADED(tAtmIdleAnimDict)
			AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalLaptopSynchSceneID)
			OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalLaptopSynchSceneID) >= 0.96)
				INT iRandom
				iRandom = GET_RANDOM_INT_IN_RANGE(0,iMaxIdleAnims)
				
				IF (ARE_STRINGS_EQUAL(tPreviousIdleAnim, "idle_a") AND (iRandom = 0))
				OR (ARE_STRINGS_EQUAL(tPreviousIdleAnim, "idle_b") AND (iRandom = 1))
				OR (ARE_STRINGS_EQUAL(tPreviousIdleAnim, "idle_c") AND (iRandom = 2))
				OR (ARE_STRINGS_EQUAL(tPreviousIdleAnim, "idle_d") AND (iRandom = 3))
					iRandom++
					
					IF iRandom >= iMaxIdleAnims
						iRandom = 0
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
					DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
					iLaptopSynchSceneID = -1
				ENDIF
				iLaptopSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSynchSceneOrigin, 
									GET_ENTITY_ROTATION(oIn),DEFAULT,FALSE,TRUE)
				
				SWITCH iRandom
					CASE 0
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
								tAtmIdleAnimDict, "idle_a",
								SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
						NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
						tPreviousIdleAnim = "idle_a"
						
						CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene idle_a")
						iLaptopSynchSceneStage = iCONST_STAGE_3_idle
					BREAK
					CASE 1
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
								tAtmIdleAnimDict, "idle_b",
								SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
						NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
						tPreviousIdleAnim = "idle_b"
						
						CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene idle_b")
						iLaptopSynchSceneStage = iCONST_STAGE_3_idle
					BREAK
					CASE 2
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
								tAtmIdleAnimDict, "idle_c",
								SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
						NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
						tPreviousIdleAnim = "idle_c"
						
						CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene idle_c")
						iLaptopSynchSceneStage = iCONST_STAGE_3_idle
					BREAK
					CASE 3
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
								tAtmIdleAnimDict, "idle_d",
								SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
						NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
						tPreviousIdleAnim = "idle_d"
						
						CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene idle_d")
						iLaptopSynchSceneStage = iCONST_STAGE_3_idle
					BREAK
					
					DEFAULT
						tPreviousIdleAnim = "idle_XXX"
						
						CASSERTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene idle_XXX_", iRandom)
						SCRIPT_ASSERT("idle_XXX")
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE iCONST_STAGE_3_idle
				
			bPlayedLoginSound = FALSE
			
			IF   IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tAtmIdleAnimDict, "idle_a", ANIM_SYNCED_SCENE)
				DRAW_DEBUG_TEXT("idle_a", GET_ENTITY_COORDS(PLAYER_PED_ID()), 256, 064, 064, 064)//////red
			ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tAtmIdleAnimDict, "idle_b", ANIM_SYNCED_SCENE)
				DRAW_DEBUG_TEXT("idle_b", GET_ENTITY_COORDS(PLAYER_PED_ID()), 256, 064, 064, 064)//////red
			ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tAtmIdleAnimDict, "idle_c", ANIM_SYNCED_SCENE)
				DRAW_DEBUG_TEXT("idle_c", GET_ENTITY_COORDS(PLAYER_PED_ID()), 256, 064, 064, 064)//////red
			ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tAtmIdleAnimDict, "idle_d", ANIM_SYNCED_SCENE)
				DRAW_DEBUG_TEXT("idle_d", GET_ENTITY_COORDS(PLAYER_PED_ID()), 256, 064, 064, 064)//////red
			ELSE
				DRAW_DEBUG_TEXT("idle_XXX", GET_ENTITY_COORDS(PLAYER_PED_ID()), 256, 064, 064, 064)//////red
			ENDIF
			
			iLocalLaptopSynchSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLaptopSynchSceneID)

			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
			AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalLaptopSynchSceneID)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
					DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
					iLaptopSynchSceneID = -1
				ENDIF
				iLaptopSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSynchSceneOrigin, 
									GET_ENTITY_ROTATION(oIn),DEFAULT,FALSE,TRUE)
				iLocalLaptopSynchSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLaptopSynchSceneID)
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalLaptopSynchSceneID)
					SET_SYNCHRONIZED_SCENE_PHASE(iLocalLaptopSynchSceneID, 0.0)
				ENDIF
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
						tAtmIdleAnimDict,	//tAtmBaseAnimDict,
						sBase,
						SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
				
				CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene \"", tAtmIdleAnimDict, "\" - exit from idle anim")
				iLaptopSynchSceneStage = iCONST_STAGE_3_idle
				
				EXIT
			ENDIF
			
			IF HAS_ANIM_DICT_LOADED(tAtmIdleAnimDict)	//tAtmBaseAnimDict)
			AND (NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalLaptopSynchSceneID)
			OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalLaptopSynchSceneID) >= 0.99)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
					DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
					iLaptopSynchSceneID = -1
				ENDIF
				iLaptopSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSynchSceneOrigin, 
									GET_ENTITY_ROTATION(oIn),DEFAULT,FALSE,TRUE)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
						tAtmIdleAnimDict,	//tAtmBaseAnimDict,
						sBase,
						SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
				
				CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: Control_Synch_Scene \"", tAtmIdleAnimDict, "\" - timeout of idle anim")
				iLaptopSynchSceneStage = iCONST_STAGE_3_idle
				
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF d_bLaptopTestAnims
		iLocalLaptopSynchSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLaptopSynchSceneID)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalLaptopSynchSceneID)
			vSynchSceneOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oIn, vSynchSceneOffset)
			SET_SYNCHRONIZED_SCENE_ORIGIN(iLocalLaptopSynchSceneID, vSynchSceneOrigin, GET_ENTITY_ROTATION(oIn))
		ENDIF
	ENDIF
	#ENDIF
	
ENDPROC

FUNC BOOL IS_SECUROSERV_COMPUTER()
	IF DOES_ENTITY_EXIST(oLaptop)
		IF  GET_ENTITY_MODEL(oLaptop) = INT_TO_ENUM(MODEL_NAMES, HASH("EX_PROP_MONITOR_01_EX"))
		OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SECUROSERV_LAPTOP()
	IF DOES_ENTITY_EXIST(oLaptop)
		IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if this is the clubhouse or factory laptop 
FUNC BOOL IS_BIKER_LAPTOP()
	IF DOES_ENTITY_EXIST(oLaptop)
		IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
		OR IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_IE_LAPTOP()
	IF DOES_ENTITY_EXIST(oLaptop)
	AND IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_LAPTOP()
	IF DOES_ENTITY_EXIST(oLaptop)
	AND IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
	
	PROC SETUP_LAPTOP_DEBUG_WIDGETS()
		
		IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			d_wLaptopWidget = START_WIDGET_GROUP("Laptop Trigger (IN WAREHOUSE)")
		ELIF IS_PLAYER_IN_FACTORY(PLAYER_ID())
			d_wLaptopWidget = START_WIDGET_GROUP("Laptop Trigger (IN FACTORY)")
		ELIF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
			d_wLaptopWidget = START_WIDGET_GROUP("Laptop Trigger (IN ARENA_GARAGE)")
		ELSE
			d_wLaptopWidget = START_WIDGET_GROUP("Laptop Trigger")
		ENDIF
		
			ADD_WIDGET_BOOL("d_bLaptopTestAnims", d_bLaptopTestAnims)
		STOP_WIDGET_GROUP()
	ENDPROC

	PROC MAINTAIN_LAPTOP_DEBUG_WIDGETS(OBJECT_INDEX oIn)
		IF d_bLaptopTestAnims
		
			TEXT_LABEL_63 tLaptopAnimDict
			Get_Laptop_Anim_Dict_Names(eAnimSet_enter, tLaptopAnimDict)
			
			SET_CURRENT_WIDGET_GROUP(d_wLaptopWidget)
			WIDGET_GROUP_ID d_wLaptopSubwidget = START_WIDGET_GROUP("d_bLaptopTestAnims")
				ADD_WIDGET_VECTOR_SLIDER("vSynchSceneOffset", vSynchSceneOffset, -2.0, 2.0, 0.001)
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(d_wLaptopWidget)
			
			WHILE d_bLaptopTestAnims
				Control_Synch_Scene(oIn)
				
				WAIT(0)
			ENDWHILE
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
				DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
			ENDIF
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			DELETE_WIDGET_GROUP(d_wLaptopSubwidget)
		ENDIF
	ENDPROC
#ENDIF

FUNC BOOL IS_PLAYER_ON_MISSION_THAT_SHOULD_BLOCK_LAPTOP()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_ISLAND_HEIST_PREP
			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<1155.0, -3200.515, -40.050>>, <<1176.790, -3189.532, -34.785>>)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_TUNER_ROBBERY
			IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(),<<-1470.8796, -528.8513, 75.0839>>, <<-1472.6295, -526.5934, 73.3136>>)
				RETURN TRUE
			ELIF IS_ENTITY_IN_AREA(PLAYER_PED_ID(),<<1008.4672, -3167.9465, -37.5246>>, <<1006.9028, -3171.1516, -39.6192>>)
				RETURN TRUE
			ENDIF
		ENDIF

	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LAPTOP_ACCESSIBLE()
	IF GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_SIMEON_SHOWROOM_OFFICE_COMPUTER_UNAVAILABLE()
		RETURN FALSE
	ENDIF
	#ENDIF // #IF FEATURE_DLC_1_2022
		
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
		RETURN GET_ARCADE_MODE() = ARC_NULL
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_ON_MISSION_THAT_SHOULD_BLOCK_LAPTOP()
		RETURN FALSE
	ENDIF

	IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		RETURN TRUE
	ELIF GB_GET_NUM_BOSSES_IN_SESSION() < GB_GET_MAX_NUM_GANGS()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_CHECKS_AND_CREATE_GANG(GB_GANG_TYPE eGangType, BOOL bAlreadyABoss)
	IF NOT bAlreadyABoss
	AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND GB_GET_NUM_BOSSES_IN_SESSION() < GB_GET_MAX_NUM_GANGS()
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
		GB_BOSS_CREATE_GANG(DEFAULT, eGangType)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the context intention or help text for clubhouses
PROC SELECT_CONTEXT_INTENTION_FOR_BIKER_COMPUTER(BOOL bGangVIP)
	IF (IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_ID()) OR IS_LOCAL_PLAYER_IN_FACROTY_THEY_OWN())
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT g_bLaunchedMissionFrmLaptop
	AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), GET_ENTITY_HEADING(oLaptop), 90)
		IF NETWORK_IS_ACTIVITY_SESSION()
			PRINT_HELP_FOREVER("WHSECUROBLCK")
		ELIF MISSION_PC_DID_I_JOIN_A_PRIVATE_SESSION()
			IF IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_ID())
				PRINT_HELP_FOREVER("BIKERWHBLCKC")
			ELSE
				PRINT_HELP_FOREVER("BIKERWHBLCKD")
			ENDIF
		ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		
			IF HAS_NET_TIMER_STARTED(stMissionCDTimer)
			AND HAS_NET_TIMER_EXPIRED(stMissionCDTimer, 10000, TRUE)
			OR NOT bOnAMission
				IF GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
				OR g_bLaunchedMissionFrmLaptop = TRUE
					bOnAMission = TRUE
					IF NOT g_bLaunchedMissionFrmLaptop
						PRINT_HELP_FOREVER("WHSECUROBLCK")
					ENDIF
				ELSE
					IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
						CLEAR_HELP(TRUE)
					ENDIF
					bOnAMission = FALSE
					RESET_NET_TIMER(stMissionCDTimer)
					REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "BIKERWHINPUT")
				ENDIF
			ELIF bOnAMission
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_FOREVER("WHSECUROBLCK")
				ENDIF
				
				IF NOT GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
					bOnAMission = FALSE
					CLEAR_HELP(TRUE)
				ENDIF
			ENDIF
				
		ELIF NOT bGangVIP
		AND GB_GET_NUM_BOSSES_IN_SESSION() < GB_GET_MAX_NUM_GANGS()
			REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "BIKERWHBLCKA")
		ELSE
			PRINT_HELP_FOREVER("BIKERWHBLCKB")
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 10 = 0
				IF NETWORK_IS_IN_MP_CUTSCENE()
					CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA2] Laptop not activating due to NETWORK_IS_IN_MP_CUTSCENE = TRUE")
				ENDIF
				
				IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
					CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA2] Laptop not activating due to IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR = TRUE")
				ENDIF
				
				IF g_bLaunchedMissionFrmLaptop
					CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA2] Laptop not activating due to g_bLaunchedMissionFrmLaptop = TRUE")
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the context intention or help text for clubhouses
PROC SELECT_CONTEXT_INTENTION_FOR_IE_COMPUTER(BOOL bGangVIP)	
	IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND GET_IE_GARAGE_PLAYER_IS_IN(PLAYER_ID()) = GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT g_bLaunchedMissionFrmLaptop
	
		IF MISSION_PC_DID_I_JOIN_A_PRIVATE_SESSION()
			PRINT_HELP_FOREVER("WHPRIVSESLAP")									
		ELIF NOT bGangVIP
			IF GB_GET_NUM_BOSSES_IN_SESSION() >= GB_GET_MAX_NUM_GANGS()
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROMAXVIP")
			ELIF GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "PIM_EXECH0x1")
			ELIF NETWORK_IS_ACTIVITY_SESSION()
				PRINT_HELP_FOREVER("WHSECUROBLCK")
			ELSE
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "SECINPUTTREGLAP")
			ENDIF
		//We shouldn't allow access to the computer for players who are in a motorcycle club
		ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
			PRINT_HELP_FOREVER("WHBIKERBLCK")
		ELSE
			IF HAS_NET_TIMER_STARTED(stMissionCDTimer)
			AND HAS_NET_TIMER_EXPIRED(stMissionCDTimer, 10000, TRUE)
			OR NOT bOnAMission
				IF GB_IS_PLAYER_ON_VEHICLE_EXPORT_MISSION(PLAYER_ID())
				OR g_bLaunchedMissionFrmLaptop = TRUE
					bOnAMission = TRUE
					IF NOT g_bLaunchedMissionFrmLaptop
						PRINT_HELP_FOREVER("WHSECUROBLCK")
					ENDIF
				ELSE
					IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
						CLEAR_HELP()
					ENDIF
					bOnAMission = FALSE
					RESET_NET_TIMER(stMissionCDTimer)
					REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROINPUT")
				ENDIF
			ELIF bOnAMission
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_FOREVER("WHSECUROBLCK")
				ENDIF
				
				IF NOT GB_IS_PLAYER_ON_VEHICLE_EXPORT_MISSION(PLAYER_ID())
					CLEAR_HELP(TRUE)
					bOnAMission = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Sets the context intention or help text for arena garage
PROC SELECT_CONTEXT_INTENTION_FOR_ARENA_COMPUTER(BOOL bGangVIP)	
	IF IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT g_bLaunchedMissionFrmLaptop
		REGISTER_CONTEXT_INTENTION(myContextIntention, CP_LOW_PRIORITY, "ARENAWHINPUT")
		UNUSED_PARAMETER(bGangVIP)
		
//		IF MISSION_PC_DID_I_JOIN_A_PRIVATE_SESSION()
//			PRINT_HELP_FOREVER("WHPRIVSESLAP")									
//		ELIF NOT bGangVIP
//			IF GB_GET_NUM_BOSSES_IN_SESSION() >= GB_GET_MAX_NUM_GANGS()
//				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROMAXVIP")
//			ELIF GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
//				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "PIM_EXECH0x1")
//			ELIF NETWORK_IS_ACTIVITY_SESSION()
//				PRINT_HELP_FOREVER("WHSECUROBLCK")
//			ELSE
//				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "SECINPUTTREGLAP")
//			ENDIF
//		//We shouldn't allow access to the computer for players who are in a motorcycle club
//		ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
//			PRINT_HELP_FOREVER("WHBIKERBLCK")
//		ELSE
//			IF HAS_NET_TIMER_STARTED(stMissionCDTimer)
//			AND HAS_NET_TIMER_EXPIRED(stMissionCDTimer, 10000, TRUE)
//			OR NOT bOnAMission
//				IF GB_IS_PLAYER_ON_VEHICLE_EXPORT_MISSION(PLAYER_ID())
//				OR g_bLaunchedMissionFrmLaptop = TRUE
//					bOnAMission = TRUE
//					IF NOT g_bLaunchedMissionFrmLaptop
//						PRINT_HELP_FOREVER("WHSECUROBLCK")
//					ENDIF
//				ELSE
//					IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
//						CLEAR_HELP()
//					ENDIF
//					bOnAMission = FALSE
//					RESET_NET_TIMER(stMissionCDTimer)
//					REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROINPUT")
//				ENDIF
//			ELIF bOnAMission
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					PRINT_HELP_FOREVER("WHSECUROBLCK")
//				ENDIF
//				
//				IF NOT GB_IS_PLAYER_ON_VEHICLE_EXPORT_MISSION(PLAYER_ID())
//					CLEAR_HELP(TRUE)
//					bOnAMission = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Sets the context intention or help text for warehouses
PROC SELECT_CONTEXT_INTENTION_FOR_WAREHOUSE(BOOL bGangVIP)
	IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT g_bLaunchedMissionFrmLaptop
	
		IF MISSION_PC_DID_I_JOIN_A_PRIVATE_SESSION()
			PRINT_HELP_FOREVER("WHPRIVSESLAP")									
		ELIF NOT bGangVIP
			IF GB_GET_NUM_BOSSES_IN_SESSION() >= GB_GET_MAX_NUM_GANGS()
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROMAXVIP")
			ELIF GB_IS_VIP_GAMEPLAY_DISABLED_FOR_PLAYER(PLAYER_ID())
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "PIM_EXECH0x1")
			ELIF NETWORK_IS_ACTIVITY_SESSION()
				PRINT_HELP_FOREVER("WHSECUROBLCK")
			ELSE
				REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "SECINPUTTREGLAP")
			ENDIF
		//We shouldn't allow access to the computer for players who are in a motorcycle club
		ELIF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
			PRINT_HELP_FOREVER("WHBIKERBLCK")
		ELSE
			IF HAS_NET_TIMER_STARTED(stMissionCDTimer)
			AND HAS_NET_TIMER_EXPIRED(stMissionCDTimer, 10000, TRUE)
			OR NOT bOnAMission
				IF GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
				OR g_bLaunchedMissionFrmLaptop = TRUE
					bOnAMission = TRUE
					IF NOT g_bLaunchedMissionFrmLaptop
						PRINT_HELP_FOREVER("WHSECUROBLCK")
					ENDIF
				ELSE
					IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
						CLEAR_HELP()
					ENDIF
					bOnAMission = FALSE
					RESET_NET_TIMER(stMissionCDTimer)
					REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "WHSECUROINPUT")
				ENDIF
			ELIF bOnAMission
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_FOREVER("WHSECUROBLCK")
				ENDIF
				
				IF NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
					CLEAR_HELP(TRUE)
					bOnAMission = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC RUN_EXIT_SYNC_SCENE()
	IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(oLaptop)
		
		TEXT_LABEL_63 tLaptopAnimDict
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> Player scenario cancel out")
			
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())//1388194
				IF (IS_PED_USING_ANY_SCENARIO(PLAYER_PED_ID()))
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(PLAYER_PED_ID())
					IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					
				ENDIF
				
				IF Get_Laptop_Anim_Dict_Names(eAnimSet_idle, tLaptopAnimDict)
					REMOVE_ANIM_DICT(tLaptopAnimDict)
				ENDIF
				
				IF Get_Laptop_Anim_Dict_Names(eAnimSet_exit, tLaptopAnimDict)
					IF iLaptopSynchSceneStage != 0
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iLaptopSynchSceneID)
							DETACH_SYNCHRONIZED_SCENE(iLaptopSynchSceneID)
							iLaptopSynchSceneID = -1
						ENDIF
						iLaptopSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vSynchSceneOrigin, GET_ENTITY_ROTATION(oLaptop), DEFAULT,
								FALSE, 		// SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iLaptopSynchSceneID, TRUE)
								FALSE)		// SET_SYNCHRONIZED_SCENE_LOOPED(iLaptopSynchSceneID, FALSE)
						
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), iLaptopSynchSceneID,
								tLaptopAnimDict, "exit",
								NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
					    NETWORK_START_SYNCHRONISED_SCENE(iLaptopSynchSceneID)
						
						REMOVE_ANIM_DICT(tLaptopAnimDict)
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> iLaptopSynchSceneStage = ", iLaptopSynchSceneStage)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> not Get_Laptop_Anim_Dict_Names()")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> player is dead [3]")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> player is dead [2]")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> player is dead or dying [1]")
	ENDIF
	
	g_bWarehouseLaptopContextTriggered = FALSE
ENDPROC

PROC RUN_ENTRY_SYNC_SCENE()
	g_bWarehouseLaptopContextTriggered = TRUE
	
	TEXT_LABEL_63 tLaptopAnimDict

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
		VECTOR vTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oLaptop, <<0.0328,-0.8,0.3>>)
		BOOL bInPos = FALSE
		
		RESET_NET_TIMER(stWHSyncSceneTimer)
		
		IF Get_Laptop_Anim_Dict_Names(eAnimSet_enter, tLaptopAnimDict)
			WHILE NOT bInPos 
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				IF NOT HAS_NET_TIMER_STARTED(stWHSyncSceneTimer)
					START_NET_TIMER(stWHSyncSceneTimer)
				ELIF HAS_NET_TIMER_EXPIRED(stWHSyncSceneTimer, 4000)
					bInPos = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[Laptop] WH Laptop in position timer expierd! Moving on")
				ENDIF
				
				IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTargetPos, 0.05)
				AND NOT IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), GET_ENTITY_HEADING(oLaptop), 5)
					IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTargetPos, PEDMOVEBLENDRATIO_WALK, 5000, GET_ENTITY_HEADING(oLaptop),DEFAULT_NAVMESH_RADIUS)
					ENDIF
				ELSE
					bInPos = TRUE
				ENDIF
				
				DRAW_DEBUG_SPHERE(vTargetPos, 0.1,								255,000,000, 255)
				DRAW_DEBUG_LINE(vTargetPos, GET_ENTITY_COORDS(oLaptop),			000,255,000, 255)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTargetPos,	000,000,255, 255)
				
				WAIT(0)
			ENDWHILE
			
			RESET_NET_TIMER(stWHSyncSceneTimer)
			iLaptopSynchSceneStage = 0									
		ENDIF
	ENDIF
	
	WHILE iLaptopSynchSceneStage != 3
		Control_Synch_Scene(oLaptop)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
		// Wait until the laptop cannot trigger again until moving on..
		WAIT(0)
	ENDWHILE
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_CANT_USE_LAPTOP_REASON(BOOL SkipAmbient, VECTOR vtrig, VECTOR vLocateSize)
	IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
		BOOL bReasonFound = FALSE
		
		IF SkipAmbient
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to SkipAmbient = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF g_bInRadioLocate
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] g_bInRadioLocate = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_PHONE_ONSCREEN()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_PHONE_ONSCREEN = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_PED_IN_COVER(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_PED_IN_COVER = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF Is_Player_Timetable_Scene_In_Progress()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to Is_Player_Timetable_Scene_In_Progress = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vtrig, vLocateSize, FALSE,TRUE, TM_ON_FOOT)
		//	CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_ENTITY_AT_COORD = FALSE")
			bReasonFound = TRUE
		ENDIF
		
		IF NOT (GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_ROOM_KEY_FROM_ENTITY(oLaptop))
		//	CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to mismatched roomkeys (player:", GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()), " != laptop:", GET_ROOM_KEY_FROM_ENTITY(oLaptop), ")")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to GLOBAL_SPEC_BS_USING_MP_RADIO bit is set")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_PLAYER_DRIVING_ANY_VEHICLE()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_PLAYER_DRIVING_ANY_VEHICLE")
			bReasonFound = TRUE
		ENDIF
		
		IF MP_FORCE_TERMINATE_INTERNET_ACTIVE()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to MP_FORCE_TERMINATE_INTERNET_ACTIVE = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF SP_FORCE_TERMINATE_INTERNET_ACTIVE()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to SP_FORCE_TERMINATE_INTERNET_ACTIVE = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_DISABLE_LAPTOPS) AND NETWORK_IS_ACTIVITY_SESSION())
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to ciMISSION_DISABLE_LAPTOPS = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_PLAYER_ON_BOSSVBOSS_DM() AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG() // 2622149
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_PLAYER_ON_BOSSVBOSS_DM = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF NETWORK_IS_IN_MP_CUTSCENE()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to NETWORK_IS_IN_MP_CUTSCENE = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF g_bIEAppVisible
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to g_bIEAppVisible = TRUE")
			bReasonFound = TRUE
		ENDIF
		
		IF IS_AMBIENT_OBJECT_INTERNET_DISABLED()
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to IS_AMBIENT_OBJECT_INTERNET_DISABLED()")
			bReasonFound = TRUE
		ENDIF
		
		//
		IF NOT bReasonFound
			CDEBUG1LN(DEBUG_AMBIENT, "[LPT_NA] Laptop not activating due to unknown reason")
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IN_RANGE_OF_AIRCRAFT_CARRIER_LAPTOP()
	IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_SUPER_BUSINESS_BATTLES 
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<3084.5112, -4686.6641, 26.2522>>, 50.0)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LAPTOP_SCRIPT_MODEL_LAUNCHED_MANUALLY(MODEL_NAMES eModel)
	IF eModel = PROP_LAPTOP_LESTER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LAPTOP_OUT_OF_RANGE()
	IF DOES_ENTITY_EXIST(oLaptop)
		MODEL_NAMES modnam = GET_ENTITY_MODEL(oLaptop)
		
		IF IS_LAPTOP_SCRIPT_MODEL_LAUNCHED_MANUALLY(modnam)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vLaptopCoords = GET_ENTITY_COORDS(oLaptop)
				
				RETURN VDIST2(vPlayerCoords, vLaptopCoords) > 10000 //(100 * 100)
			ENDIF
		ELSE
			RETURN NOT IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oLaptop)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT (OBJECT_INDEX oIn)

	oLaptop = oIn
	
	IF NOT DOES_ENTITY_EXIST(oLaptop)
		CPRINTLN(DEBUG_AMBIENT, "Laptop object passed to atm_trigger script doesn't exist. Terminating.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED()	
		CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		CPRINTLN(DEBUG_AMBIENT, "Laptop object cleaned up as the player isn't playing as a story character.")
		CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	#IF USE_CLF_DLC
	IF g_bLoadedClifford
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("IAA_Drone")) > 0
			CPRINTLN(DEBUG_AMBIENT, "Laptop object cleaned up as IAA Drone is running.")
			CLEANUP()
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
	#ENDIF
	
	IF g_BlipSystemPrologueMode
		CLEANUP()
		TERMINATE_THIS_THREAD()
	ENDIF

	IF g_bLaptopMissionSuppressed
		TERMINATE_THIS_THREAD()
	ENDIF
	
	BOOL bDisabledMode = FALSE
	//INT iRenderIndex = 0
	STRING sRenderTarget = "tvscreen"

	PED_INDEX tmpArray[25]
	BOOL      bCheck = TRUE
	
	// Trigger	
	VECTOR    vtrig = GET_ENTITY_COORDS(oIn,TRUE) - (GET_ENTITY_FORWARD_VECTOR(oIn)* 0.6)
	vtrig.z = vtrig.z + 0.5
		
	vSynchSceneOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oIn, vSynchSceneOffset)
	
	CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: : vSynchSceneOrigin: ", vSynchSceneOrigin)
	
	//bInPropertyOwnerNotPaidLastUtilityBill = TRUE
	
	#IF IS_DEBUG_BUILD
	SETUP_LAPTOP_DEBUG_WIDGETS()
	#ENDIF
	
	g_bLaunchedMissionFrmMCT = FALSE
	g_bLaunchedMissionFrmLaptop = FALSE
	g_bLaunchedPreSellVehModFromIEApp = FALSE
		
	// Each frame check player range
	WHILE bCheck

		// Check for laptop being destroyed
		IF DOES_ENTITY_EXIST(oLaptop)
		
			IF bOnAMission
				IF NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
					IF NOT HAS_NET_TIMER_STARTED(stMissionCDTimer)
						START_NET_TIMER(stMissionCDTimer, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF g_bLaunchedMissionFrmLaptop
			AND (IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			OR IS_PLAYER_IN_FACTORY(PLAYER_ID()))
				
				IF NOT HAS_NET_TIMER_STARTED(stWHWarpOutForSellMissionStart)
					START_NET_TIMER(stWHWarpOutForSellMissionStart)
					CDEBUG1LN(DEBUG_AMBIENT, "laptop_trigger g_bLaunchedMissionFrmLaptop timer started")
				ELIF HAS_NET_TIMER_EXPIRED(stWHWarpOutForSellMissionStart, 10000)
					RESET_NET_TIMER(stWHWarpOutForSellMissionStart)
					g_bLaunchedMissionFrmLaptop = FALSE
					CDEBUG1LN(DEBUG_AMBIENT, "laptop_trigger calling DISABLE_CELLPHONE_THIS_FRAME_ONLY clearing flag as the timer has expired")
				ENDIF
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			ENDIF		
		
			IF bInPropertyOwnerNotPaidLastUtilityBill
				IF NOT bDisabledMode
	
					MODEL_NAMES modnam = GET_ENTITY_MODEL(oLaptop)
				
					IF modnam = PROP_LAPTOP_01A

						//PROP_LAPTOP_LESTER2
				
						CREATE_MODEL_HIDE(GET_ENTITY_COORDS(oIn), 1.00, PROP_LAPTOP_01A, FALSE) 
						VECTOR rot = GET_ENTITY_ROTATION(oLaptop)
			            oLaptop = CREATE_OBJECT(PROP_LAPTOP_LESTER2, GET_ENTITY_COORDS(oLaptop),FALSE)
						SET_ENTITY_ROTATION(oLaptop, rot)
						bDisabledMode = TRUE
						
						/*
						IF REGISTER_NAMED_RENDERTARGET(sRenderTarget)
							
							LINK_NAMED_RENDERTARGET(PROP_LAPTOP_LESTER2)
						
							IF IS_NAMED_RENDERTARGET_LINKED(PROP_LAPTOP_LESTER2)
								
								iRenderIndex = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
								PRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Got laptop render target")
							ELSE
									IF IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
										RELEASE_NAMED_RENDERTARGET(sRenderTarget)
									ENDIF
								PRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Cannot link render target")
							ENDIF
					
							
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Cannot get render target")
						#ENDIF
						ENDIF
						*/
					
					
					
					ENDIF
				ENDIF
			ENDIF
		
		
		
			IF HAS_OBJECT_BEEN_BROKEN(oLaptop)
				CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Laptop broken, destroying trigger...")
				CLEANUP()
				TERMINATE_THIS_THREAD()
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(oLaptop)
		AND (NOT IS_CUTSCENE_PLAYING())
		AND IS_PLAYER_PLAYING(GET_PLAYER_INDEX())
		

			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(oLaptop)

				/*
				// Check for tutorial display bAtmFirstTimeFlowHelpShown
				IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE()  
					IF NOT g_savedGlobals.sFinanceData.bAtmFirstTimeFlowHelpShown	
						IF NOT  IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_FLOW_HELP_MESSAGE_QUEUED("ATM_1TM_TUT")
								//ATM_1TM_TUT
								ADD_HELP_TO_FLOW_QUEUE("ATM_1TM_TUT",FHP_HIGH)	// Priority kicked up to high to avoid delay issues
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ATM_1TM_TUT")
						g_savedGlobals.sFinanceData.bAtmFirstTimeFlowHelpShown	= TRUE
					ENDIF
				ENDIF
				*/
				
				//pushData.triggererEntity = INT_TO_NATIVE ( ENTITY_INDEX, NATIVE_TO_INT (PLAYER_PED_ID()))

				// Check for peds nearby that are playing the ambient atm set since lester doesn't use a scanario
				BOOL SkipAmbient = FALSE
				INT npeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(),tmpArray)
				INT i = 0
					
				REPEAT npeds i
					// Atm male
					// GET_ENTITY_ANIM_CURRENT_TIME()
					IF NOT IS_ENTITY_DEAD(tmpArray[i])
						IF IS_ENTITY_PLAYING_ANIM(tmpArray[i],"MP_COMMON_MISS","HACK_INTRO")
							SkipAmbient = TRUE
							
						ELIF IS_ENTITY_PLAYING_ANIM(tmpArray[i],"MP_COMMON_MISS","HACK_LOOP")
							SkipAmbient = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF g_bSuppressLaptopContextIntention
					SkipAmbient = TRUE
				ENDIF
				
				//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),in_coords.vec_coord[0], << LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY >>, TRUE,TRUE, TM_ON_FOOT)
				VECTOR vLocateSize = << 0.6, 0.6, 0.8 >>
				IF IS_SECUROSERV_COMPUTER()
				OR IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
				OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
				OR IS_IE_LAPTOP()
					vLocateSize = << 1.0, 1.0, 0.9 >>
				ELIF IS_ARENA_LAPTOP()
					vLocateSize = << 0.75, 0.55, 1.25 >>
				ENDIF
				
				//Checks used to determine if the player has changed their gang status whilst the script is running
				BOOL bGangVIP = GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				
				BOOL bHeadingAcceptable = TRUE
				
				IF IS_ARENA_LAPTOP()
				OR IS_SECUROSERV_LAPTOP()
					bHeadingAcceptable = IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), GET_ENTITY_HEADING(oLaptop), 90)
				ENDIF
					
				IF (NOT SkipAmbient)
				AND NOT g_bInRadioLocate
				AND (NOT IS_PHONE_ONSCREEN())
				AND NOT IS_PED_IN_COVER(PLAYER_PED_ID())
				AND (NOT (Is_Player_Timetable_Scene_In_Progress()))
				AND (IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vtrig, vLocateSize, FALSE,TRUE, TM_ON_FOOT))
				AND	(GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_ROOM_KEY_FROM_ENTITY(oLaptop))
				AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)					 // MP radio is a near a laptop, so block it when using
				AND (NOT IS_PLAYER_DRIVING_ANY_VEHICLE())
				AND NOT (g_bInMultiplayer AND MP_FORCE_TERMINATE_INTERNET_ACTIVE())
				AND NOT (NOT g_bInMultiplayer AND SP_FORCE_TERMINATE_INTERNET_ACTIVE())
				AND NOT (g_bInMultiplayer AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_DISABLE_LAPTOPS) AND NETWORK_IS_ACTIVITY_SESSION())
				AND NOT (g_bInMultiplayer AND IS_PLAYER_ON_BOSSVBOSS_DM() AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()) // 2622149
				AND NOT (g_bInMultiplayer AND GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = CSV_HEISTERS) // 5652299
				AND NOT NETWORK_IS_IN_MP_CUTSCENE()
				AND NOT g_bIEAppVisible
				AND NOT IN_RANGE_OF_AIRCRAFT_CARRIER_LAPTOP()
				#IF FEATURE_COPS_N_CROOKS
				AND (NOT IS_FREEMODE_ARCADE() OR GET_ARCADE_MODE() = ARC_NULL)
				#ENDIF
				#IF FEATURE_DLC_1_2022
				AND NOT IS_SIMEON_SHOWROOM_OFFICE_COMPUTER_UNAVAILABLE()
				#ENDIF // #IF FEATURE_DLC_1_2022
				AND bHeadingAcceptable
				AND NOT IS_PLAYER_ON_MISSION_THAT_SHOULD_BLOCK_LAPTOP()
				AND NOT IS_AMBIENT_OBJECT_INTERNET_DISABLED()	
					IF NOT IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					AND NOT IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID()) 
						//Check if the players gang status has changed
						IF bGangMemberLF != bGangVIP
							IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
							OR IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_ID())
							OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
								IF myContextIntention != NEW_CONTEXT_INTENTION
									RELEASE_CONTEXT_INTENTION(myContextIntention)
								ENDIF
								
								IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
									CLEAR_HELP(TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						//Update the gang status
						bGangMemberLF = bGangVIP
						
						//We are close enough so display help or register the relevant context intention
						IF myContextIntention = NEW_CONTEXT_INTENTION
							
							IF bDisabledMode
								REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "MPLA_BILL")
							ELSE
								IF IS_SECUROSERV_COMPUTER()								
									SELECT_CONTEXT_INTENTION_FOR_WAREHOUSE(bGangVIP)
								ELIF IS_BIKER_LAPTOP()
									SELECT_CONTEXT_INTENTION_FOR_BIKER_COMPUTER(bGangVIP)
								ELIF IS_IE_LAPTOP()
									SELECT_CONTEXT_INTENTION_FOR_IE_COMPUTER(bGangVIP)
								ELIF IS_ARENA_LAPTOP()
									SELECT_CONTEXT_INTENTION_FOR_ARENA_COMPUTER(bGangVIP)
								ELSE
									REGISTER_CONTEXT_INTENTION(myContextIntention, CP_MINIMUM_PRIORITY, "BROWSEINPUTTRIG")									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF myContextIntention != NEW_CONTEXT_INTENTION
					AND HAS_CONTEXT_BUTTON_TRIGGERED(myContextIntention)
					AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
					AND IS_LAPTOP_ACCESSIBLE()
						
						IF myContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(myContextIntention)
						ENDIF
						
						IF NOT bDisabledMode
							
							//Run the entry to idle animations
							IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_FACROTY_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
								RUN_ENTRY_SYNC_SCENE()
							ENDIF
							
							//Create a gang if necessary & start the relevant browser
							IF IS_SECUROSERV_COMPUTER()							
								RUN_CHECKS_AND_CREATE_GANG(GT_VIP, bGangVIP)
								START_BROWSER(DEFAULT, FALSE, eSecuroServ)
							ELIF IS_IE_LAPTOP()
								RUN_CHECKS_AND_CREATE_GANG(GT_VIP, bGangVIP)
								START_BROWSER(DEFAULT, FALSE, eIEApp)
							ELIF IS_BIKER_LAPTOP()
								RUN_CHECKS_AND_CREATE_GANG(GT_BIKER, bGangVIP)
								START_BROWSER(DEFAULT, FALSE, eBikerApp)
							ELIF IS_ARENA_LAPTOP()
								START_BROWSER(SBSS_ArenaWar, FALSE, eAppInternet)
							ELSE
								START_BROWSER(DEFAULT, FALSE, eAppInternet)
							ENDIF
							
							IF NOT bOnCallCleared
								SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
								bOnCallCleared = TRUE
								CDEBUG1LN(DEBUG_AMBIENT, "laptop_trigger: LAPTOP: bOnCallCleared = TRUE")
							ENDIF
							
							BOOL waitingForBrowser = TRUE
							
							//This additional wait time allows the securoserv and biker apps to launch
							//this prevents the exit anim playing too early
							IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_FACROTY_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
								REINIT_NET_TIMER(stWHSyncSceneTimer)
								
								WHILE NOT HAS_NET_TIMER_EXPIRED(stWHSyncSceneTimer, 500)
									DISABLE_CELLPHONE_THIS_FRAME_ONLY()
									WAIT(0)
								ENDWHILE
								
								RESET_NET_TIMER(stWHSyncSceneTimer)
							ENDIF
							
							WHILE waitingForBrowser
								
								IF NOT DOES_ENTITY_EXIST(oLaptop)
								OR NOT (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(oLaptop,TRUE), << 1.5, 1.5, 4 >>, FALSE,TRUE, TM_ON_FOOT))
									// Check for player leaving the trigger zone
									g_bBrowserQuitMessage = TRUE
								ENDIF
								
								IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
								OR IS_LOCAL_PLAYER_IN_FACROTY_THEY_OWN()
								OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
								OR IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
									IF NOT IS_BROWSER_OPEN()
									AND NOT g_bIEAppVisible
										waitingForBrowser = FALSE
									ENDIF								
								ELSE
									IF IS_BROWSER_OPEN() OR g_bBrowserQuitMessage
										waitingForBrowser = FALSE
									ENDIF
								ENDIF
								
								IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
									DISABLE_CELLPHONE_THIS_FRAME_ONLY()
								ENDIF
								// Wait until the laptop cannot trigger again until moving on..
								WAIT(0)
							ENDWHILE
							
							IF IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_FACROTY_THEY_OWN()
							OR IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
								RUN_EXIT_SYNC_SCENE()
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: <EXIT_LAPTOP> is_player_in_warehouse = false")
							ENDIF
							
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
							RESET_NET_TIMER(stWHWarpOutForSellMissionStart)
							
							bOnCallCleared = FALSE
							CDEBUG1LN(DEBUG_AMBIENT, "laptop_trigger: LAPTOP: bOnCallCleared = FALSE")
						ENDIF
					ELIF myContextIntention != NEW_CONTEXT_INTENTION
					AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
					AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
					AND IS_LAPTOP_ACCESSIBLE()
					AND IS_LOCAL_PLAYER_IN_ARENA_GARAGE_THEY_OWN()
						
						IF myContextIntention != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(myContextIntention)
							DISABLE_INTERACTION_MENU()
						ENDIF
						
						//Run the entry to idle animations
						RUN_ENTRY_SYNC_SCENE()
						
						//Start the menu
						SET_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCH_AC_UNLOCKS_MENU)
						
						WHILE IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCH_AC_UNLOCKS_MENU)
							WAIT(0)
						ENDWHILE
						
						RUN_EXIT_SYNC_SCENE()
						ENABLE_INTERACTION_MENU()
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()	
					ENDIF
				
				ELSE
					IF myContextIntention != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(myContextIntention)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF GET_FRAME_COUNT() % 10 = 0
							DEBUG_PRINT_CANT_USE_LAPTOP_REASON(SkipAmbient, vtrig, vLocateSize)
						ENDIF
					#ENDIF
					
					IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
					OR IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_ID())
						IF IS_WAREHOUSE_LAPTOP_HELP_DISPLAYING()
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					// If out of range then check if far enough away to finish this script
					IF DOES_ENTITY_EXIST(oIn)
						
						IF IS_LAPTOP_OUT_OF_RANGE()
							CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Laptop script out of range...")
							bCheck = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF myContextIntention != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(myContextIntention)
				ENDIF
			ENDIF
		ELSE
			IF myContextIntention != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(myContextIntention)
			ENDIF
			bCheck = FALSE
			
			#IF IS_DEBUG_BUILD

				IF IS_ENTITY_DEAD(PLAYER_PED_ID())
					CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: laptop quitting because IS_ENTITY_DEAD(PLAYER_PED_ID())")
				ENDIF
				IF NOT DOES_ENTITY_EXIST(oLaptop)
					CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: laptop quitting because NOT DOES_ENTITY_EXIST(oLaptop)")
				ENDIF
				IF (IS_CUTSCENE_PLAYING())
					CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: laptop quitting because (IS_CUTSCENE_PLAYING())")
				ENDIF
				IF NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())
					CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: laptop quitting because NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())")
				ENDIF

			#ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_LAPTOP_DEBUG_WIDGETS(oIn)
		#ENDIF
		
		WAIT(0)
	ENDWHILE
	
	IF bDisabledMode
      IF IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
            RELEASE_NAMED_RENDERTARGET(sRenderTarget)
      ENDIF
	ENDIF
	CPRINTLN(DEBUG_AMBIENT, "laptop_trigger: : Laptop script bailed...")

ENDSCRIPT
