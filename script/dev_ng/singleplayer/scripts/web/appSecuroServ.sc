USING "rage_builtins.sch"
USING "globals.sch"

USING "cellphone_public.sch"
USING "commands_zone.sch"

USING "website_private.sch"
USING "net_realty_warehouse.sch"
USING "net_gang_boss.sch"
USING "net_simple_interior_ie_garage.sch"
USING "net_realty_vehicle_garage.sch"
USING "net_ie_dropoff_public.sch"
USING "SVM_MISSION_FLOW.sch"
USING "net_script_tunables_new.sch"
USING "gb_vehicle_export_freemode_header.sch"

SCALEFORM_INDEX mov
SCALEFORM_RETURN_INDEX currentSelectionReturnIndex, currentWarehouseReturnIndex, currentCurrentScreenReturnIndex, buyWindowOpenReturnIndex

/////////////////////////////
///    Consts
///    
//Page ID's
CONST_INT ciLoginPageID				0
CONST_INT ciWarehouseStatsPageID	1
CONST_INT ciWarehouseMapPageID		2
CONST_INT ciBranchPageID			3
CONST_INT ciVehicleWHStatsPageID	4
CONST_INT ciVehicleWHMapPageID		5
CONST_INT ciSVMPageID				6

//Bit set consts
CONST_INT SEC_BS_DISPLAY_OVERLAY_WARNING			0
CONST_INT SEC_BS_DISPLAY_OVERLAY_CONFIRM			1
CONST_INT SEC_BS_PRUCHASE_IE_WH_SELECTED			2
CONST_INT SEC_BS_LAUNCH_STEAL_MIS_SELECTED			3
CONST_INT SEC_BS_IE_TRANSACTION_FAILED				4
CONST_INT SEC_BS_OVERLAY_ACTIVE						5
CONST_INT SEC_BS_PROPERTY_PURCHASED_THIS_RUN		6	//Set to true if we've purchased anything during this run of the script
CONST_INT SEC_BS_PROCESSING_BASKET					7 				
CONST_INT SEC_BS_PROCESSING_CONTRA_BASKET_FAILED	8 
CONST_INT SEC_BS_PROCESSING_WH_BASKET_DONE			9
CONST_INT SEC_BS_BUY_WAREHOUSE_CONFIRMED			10		
CONST_INT SEC_BS_SELL_CONTRABAND_CONFIRMED			11
CONST_INT SEC_BS_CHECK_PAGE_ID						12
CONST_INT SEC_BS_CURRENTLY_INSIDE_WAREHOUSE			13
CONST_INT SEC_BS_CURRENTLY_PLAYING_TUTORIAL			14
CONST_INT SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP	15
CONST_INT SEC_BS_SIDE_PANEL_SHOWING					16
CONST_INT SEC_BS_CHECK_SIDE_BAR						17
CONST_INT SEC_BS_PURCHASE_FAILED_NO_MONEY			18
CONST_INT SEC_BS_JUST_PURCHASED_WAREHOUSE			19
CONST_INT SEC_BS_WAITING_FOR_BUTTON_RETURN_VALUE	20
CONST_INT SEC_BS_SHOW_CASH_CHANGE_HUD				21
CONST_INT SEC_BS_DISPLAYING_LOGIN_PAGE				22
CONST_INT SEC_BS_DISPLAYING_PURCHASE_WH_TUT			23
CONST_INT SEC_BS_DIALOGUE_TUTORIAL_COMPLETE			24
CONST_INT SEC_BS_HELP_TEXT_TUTORIAL_COMPLETE		25
CONST_INT SEC_BS_DELAY_APP_EXIT_FOR_CONVERSATION	26
CONST_INT SEC_BS_BUY_MISSION_ON_COOLDOWN			27
CONST_INT SEC_BS_SELL_MISSION_ON_COOLDOWN			28
CONST_INT SEC_BS_BUY_COOLDOWN_HELP_PLAYED			29
CONST_INT SEC_BS_SELL_COOLDOWN_HELP_PLAYED			30
CONST_INT SEC_BS_COOLDOWN_HELP_DISPLAYING			31
//End of Bit set 1
CONST_INT SEC_BS2_SPECIAL_ITEM						0
CONST_INT SEC_BS2_PA_IS_MALE						1
CONST_INT SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES	2
CONST_INT SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF	3	//Set to true to make sure we don't close the overlay screen after the browser nag screen is closed
CONST_INT SEC_BS2_DONE_MP_INTRO_HELP_1				4
CONST_INT SEC_BS2_DONE_MP_INTRO_HELP_2				5
CONST_INT SEC_BS2_DONE_MP_INTRO_HELP_3				6
//End of bit set 2

ENUM enumItemConsumed
	inputConsumed_0 = 0,
	inputConsumed_1,
	
	inputConsumed_BUY_WAREHOUSE = 3007,
	inputConsumed_BUY_SMALL_SHIPMENT = 3008,
	inputConsumed_BUY_MEDIUM_SHIPMENT = 3009,
	inputConsumed_BUY_LARGE_SHIPMENT = 3010,
	inputConsumed_SELL = 3011,
	inputConsumed_CONFIRM = 3012,	//Overlay screen confirm
	
	inputConsumed_STEAL_VEHICLE = 4001,
	inputConsumed_VEH_STAT_ACCEPT = 4003, //Accept button on the vehicle stat screen overlay
	inputConsumed_VEH_STAT_CANCEL = 4004, //Cancel button on the vehicle stat screen overlay
	
	inputConsumed_BUY_VEH_WH = 5005,
	inputConsumed_MAP_STEAL_VEHICLE = 5006,
	inputConsumed_VEH_MAP_ACCEPT = 5007,	//Accept button on the map screen overlay
	inputConsumed_VEH_MAP_CANCEL = 5008,	//Cancel button on the map screen overlay
	
	inputConsumed_SVM_MAP_ACCEPT = 6002,	//Accept button on the special vehicle screen
	inputConsumed_SVM_MAP_CANCEL = 6003,	//Cancel button on the special vehicle screen
	inputConsumed_SVM_LAUNCH	 = 6999,	//Any of the special vehilce options
	
	inputConsumed_END
ENDENUM
enumItemConsumed currentButtonPressStage = inputConsumed_1

/////////////////////////////
///    Variables
///    
INT iBS, iBS2
INT iCurrentSelectedWarehouse = -1
CONTRABAND_SIZE eCurrentSelectedShipmentSize = CONTRABAND_INVALID

//Contraband details at the time of opening the app
CONTRABAND_TYPE securoContraType = CONTRABAND_TYPE_INVALID

structPedsForConversation tutPedStruct

//Cooldown help text vars
INT	iWarehouseSellCooldownBS	= 0
INT	iCooldownExpierdCheckFC		= 0
INT iJustPurchasedWHID			= 0
INT	iTutorialDialogueID			= -1
INT iProcessingBasketStage 		= SHOP_BASKET_STAGE_ADD
INT iMissionUnavailableReason 	= -1
INT iCurrentlyInsideWarehouse 	= ciW_Invalid
INT iCurrentPageID				= ciLoginPageID
INT iPlayerLoop					= 0
INT iTutorialHelpTime			= 0
INT iCratesToSell				= 0
INT iContrabandBuyerID			= 0
INT iCooldownTimeLastUpdate		= 0	//The cooldown time we sent to SF last update

INT iLeftX, iLeftY
INT iRightX, iRightY

//Used for the replace warehouse menu when trading in
INT iWHOrder[ciMaxOwnedWarehouses]

FLOAT iCursorX = 0.0
FLOAT iCursorY = 0.0

FLOAT iCursorXLast = 0.0
FLOAT iCursorYLast = 0.0

//Used to check if the player has moved too far from the laptop object
VECTOR vPlayerCoords
//Used to maintain a list of organisations for the securoserv app
INT iActiveUsers[MAX_NUM_GANGS]
//Needed so we can remove this user from the list if they leave the game
TEXT_LABEL_63 tlUserList[MAX_NUM_GANGS]
//Used for the tutorial
SCRIPT_TIMER sTutorialTimer
//USed to block the cash hud when we don't need it
SCRIPT_TIMER stCashChangeHUDTimer

/////////////////////////////
///    Variables for the IE parts of the SF movie
/// 
//An offset value to make sure we give SF unique ID's for the IE warehouses vs the old warehouses
INT iWarehouseOffset 				= (ciMaxWarehouses + 1)
INT iSVMToLaunch					= -1					//The special vehicle mission we have selected
INT iIETutorialBS1					= -1
INT iIETutorialBS2					= -1

FMMC_PLANNING_MISSION_HEADER_DESCRIPTIONS sSVMDescriptions
IE_GARAGE_INTERIORS eSelectedInterior = IE_INTERIOR_BASIC

#IF IS_DEBUG_BUILD
BOOL bDrawDebugStuff = FALSE
#ENDIF

ENUM SEC_OVERLAY_PROMPT	
	SEC_OVERLAY_NO_VEH_FOUND = 0,	
	SEC_OVERLAY_WHOUSE_FULL,
	SEC_OVERLAY_GOON_ANIMAL,
	SEC_OVERLAY_GOON_GAMBLING
ENDENUM

PROC CLEANUP_SECUROSERVE(BOOL bCloseHubApp = FALSE)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> cleanup \"", GET_THIS_SCRIPT_NAME(), "\"")
	
	SET_BROWSER_OPEN(FALSE)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	DISABLE_CELLPHONE(FALSE)
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		g_bSecuroDelayOfficeChairExit = TRUE
		SET_AUDIO_SCRIPT_CLEANUP_TIME(1000)
		PLAY_SOUND_FRONTEND(-1, "Logout", "GTAO_Exec_SecuroServ_Computer_Sounds")
	ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	STOP_AUDIO_SCENE("GTAO_Computer_Screen_Active_Scene")
	
	Unpause_Objective_Text()
	
	THEFEED_RESUME()
	
	SET_USE_DLC_DIALOGUE(FALSE)
	
	IF IS_PC_VERSION()
		SET_MULTIHEAD_SAFE(FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
	AND NOT USE_SERVER_TRANSACTIONS()
		REQUEST_SAVE(SSR_REASON_PURCHASE_PROPERTY, STAT_SAVETYPE_END_SHOPPING)
	ENDIF
	
	CLEAR_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
	CLEAR_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_CONFIRM)
	
	IF bCloseHubApp
	AND WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
		g_sBusAppManagement.bCloseHubApp = TRUE
	ENDIF
	
	g_sBusAppManagement.iRenderHandShakeFC = -1
	
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC INT GET_WAREHOUSE_INDEX_FROM_SCALEFORM_INDEX(INT iScaleformID)
	RETURN (iScaleformID+1)
ENDFUNC

FUNC INT GET_SCALEFORM_INDEX_FROM_WAREHOUSE_INDEX(INT iWarehouseID)
	RETURN (iWarehouseID-1)
ENDFUNC

FUNC BOOL ARE_SELL_MISSIONS_DISABLED()
	RETURN g_sMPTunables.bexec_disable_sell_missions
ENDFUNC

/// PURPOSE:
///    Grabs the name of our gang. If we're checking the local player we need to 
///    avoid the restricted account checks: 2770221
FUNC STRING GET_PLAYER_ORGANISATION_NAME(PLAYER_INDEX piPlayer)

	IF piPlayer = PLAYER_ID()
		PLAYER_INDEX playerID = GB_GET_THIS_PLAYER_GANG_BOSS(piPlayer)
		IF playerID != INVALID_PLAYER_INDEX()
			STRING stReturn = GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(playerID)].sMagnateGangBossData.tlGangName)
			IF IS_STRING_NULL_OR_EMPTY(stReturn)
				RETURN GET_DEFAULT_GANG_NAME(playerID)
			ELSE
				RETURN stReturn
			ENDIF
		ENDIF
	ENDIF
	
	RETURN GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(piPlayer)
ENDFUNC

FUNC STRING GET_SPECIAL_ITEM_TXD_LABEL(INT iWarehouse)
	CONTRABAND_SPECIAL_ITEM eSpecialItem = GET_FIRST_SPECIAL_ITEM_IN_WAREHOUSE(iWarehouse)
	
	SWITCH eSpecialItem
		CASE CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN	"ah_sell_crt_2"
		CASE CONTRABAND_ITEM_FILM_REEL				RETURN	"ah_sell_crt_3"
		CASE CONTRABAND_ITEM_ORNAMENTAL_EGG			RETURN	"ah_sell_crt_4"
		CASE CONTRABAND_ITEM_GOLDEN_MINIGUN 		RETURN	"ah_sell_crt_5"
		CASE CONTRABAND_ITEM_SASQUATCH_HIDE			RETURN	"ah_sell_crt_6"
		CASE CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN	"ah_sell_crt_7"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC UPDATE_IE_MISSION_COOLDOWN()
	
	//Get the time remaining (In seconds)
	INT iCurrentTimeRemaining 	= (GB_GET_MISSION_COOLDOWN_TIME_REMAINING(VEV_EYE_IN_THE_SKY) + 1)
	INT iTimeToPassScaleform	= -1
	
	IF GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_EYE_IN_THE_SKY)
		IF iCooldownTimeLastUpdate != 0
			iTimeToPassScaleform 	= 0
			iCooldownTimeLastUpdate	= 0
		ENDIF
	ELIF iCooldownTimeLastUpdate != iCurrentTimeRemaining
		iTimeToPassScaleform 	= iCurrentTimeRemaining
		iCooldownTimeLastUpdate	= iCurrentTimeRemaining
	ENDIF
	
	IF iTimeToPassScaleform != -1
		CDEBUG2LN(DEBUG_INTERNET, "<SECURO> UPDATE_IE_MISSION_COOLDOWN sending uptated time of: ", iTimeToPassScaleform)
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "UPDATE_COOLDOWN")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeToPassScaleform)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

FUNC STRING GET_SEC_OVERLAY_PROMPT(SEC_OVERLAY_PROMPT sPrompt)
	SWITCH sPrompt
		CASE SEC_OVERLAY_NO_VEH_FOUND 	RETURN "SEC_MISS_L_F"
		CASE SEC_OVERLAY_WHOUSE_FULL	RETURN "SEC_WHOUSE_FULL"
		CASE SEC_OVERLAY_GOON_ANIMAL	RETURN "GENERAL_MLF_G2"
		CASE SEC_OVERLAY_GOON_GAMBLING	RETURN "GENERAL_MLF_G5"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_SVM_MISSION_BE_ADDED_TO_SELECTION(INT iMission)
	
//	#IF IS_DEBUG_BUILD
//		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ImpExpFlowPlayVariations")
//			RETURN TRUE
//		ENDIF
//	#ENDIF
		
	#IF FEATURE_CASINO_HEIST
	IF WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	UNUSED_PARAMETER(iMission)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_DESCRIPTION_FOR_SVM(INT iMissionDecHash)
	BOOL bDescriptionReady = TRUE
	CACHED_MISSION_DESCRIPTION_LOAD_VARS sMissionDescLoadVars
	
	IF iMissionDecHash != 0
		WHILE NOT REQUEST_AND_LOAD_CACHED_DESCRIPTION_FOR_HEIST(iMissionDecHash, sMissionDescLoadVars)
			PRINTLN("<SECURO> DOWNLOAD_PLANNING_MISSION_HEADER_DATA - GET_DESCRIPTION_FOR_SVM - IS_HEIST_DESCRIPTION_READY - Waiting on sub HASH: ", iMissionDecHash)
			WAIT(0)
		ENDWHILE
		
		IF NOT sMissionDescLoadVars.bSucess	
			bDescriptionReady = FALSE
		ENDIF
	ENDIF
	
	IF bDescriptionReady
		PRINTLN("<SECURO> [BDEC] * DOWNLOAD_PLANNING_MISSION_HEADER_DATA - GET_DESCRIPTION_FOR_SVM - All description strings are available; beginning dump: ")
		STRING sText
		INT iStartCharacterIndex, iStringLengthInCharacters, iArrayIndex, i
		BOOL bFinishedCopying
		
		//Clear the previously saved data
		REPEAT FMMC_NUM_LABELS_FOR_DESCRIPTION i
			sSVMDescriptions.tl63MissionDecription[i] = ""
		ENDREPEAT
		
		//Get the cached description:
		sText = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(iMissionDecHash, INVITE_MAX_DESCRIPTION_LENGTH)	
		iStringLengthInCharacters = GET_LENGTH_OF_LITERAL_STRING(sText)
		PRINTLN("<SECURO> [BDEC] DOWNLOAD_PLANNING_MISSION_HEADER_DATA - GET_DESCRIPTION_FOR_PLANNING_MISSION_HEADER_DATA - [", sText, "] - iStringLengthInCharacters = ", iStringLengthInCharacters)
		//Copy this string into chunks using a byte limit
		WHILE NOT bFinishedCopying
			//If we've got to the end of the string then we're done
			IF iStartCharacterIndex >= iStringLengthInCharacters
				PRINTLN("<SECURO> [BDEC] DOWNLOAD_PLANNING_MISSION_HEADER_DATA - GET_DESCRIPTION_FOR_PLANNING_MISSION_HEADER_DATA - IS_HEIST_DESCRIPTION_READY = ", sSVMDescriptions.tl63MissionDecription[0], sSVMDescriptions.tl63MissionDecription[1], sSVMDescriptions.tl63MissionDecription[2], sSVMDescriptions.tl63MissionDecription[3], 
																				sSVMDescriptions.tl63MissionDecription[4], sSVMDescriptions.tl63MissionDecription[5], sSVMDescriptions.tl63MissionDecription[6], sSVMDescriptions.tl63MissionDecription[7])
				bFinishedCopying = TRUE
			ELSE
				//If we're over the array size then we done
				IF iArrayIndex >= FMMC_NUM_LABELS_FOR_DESCRIPTION
					PRINTLN("<SECURO> bFinishedCopying (iArrayIndex >= FMMC_NUM_LABELS_FOR_DESCRIPTION)")
					bFinishedCopying = TRUE
				ELSE
					//Get the next chunk
					sSVMDescriptions.tl63MissionDecription[iArrayIndex] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(sText, iStartCharacterIndex, iStringLengthInCharacters, 63)
					//Print
					PRINTLN("<SECURO> [BDEC]DOWNLOAD_PLANNING_MISSION_HEADER_DATA - GET_DESCRIPTION_FOR_PLANNING_MISSION_HEADER_DATA - g_HeistPrePlanningClient.sPlanningData.sMissionData.tl63MissionDecription[", iArrayIndex, "] = ", sSVMDescriptions.tl63MissionDecription[iArrayIndex])
					//Get the length of the chunk that we've just grabbed so we can move start position
					iStartCharacterIndex += GET_LENGTH_OF_LITERAL_STRING(sSVMDescriptions.tl63MissionDecription[iArrayIndex])
					PRINTLN("iStartCharacterIndex is now ", iStartCharacterIndex)
					//Move to the next array
					iArrayIndex++
					PRINTLN("<SECURO> iArrayIndex is now ", iArrayIndex)
				ENDIF
			ENDIF
		ENDWHILE
	ENDIF
	
	RETURN bDescriptionReady
ENDFUNC

PROC POPULATE_SPECIAL_VEH_DATA(INT iMissionID, INT iVehicleMissionsCompleted, BOOL bFallback = FALSE)
	INT i
	INT iSpecialVehID 					= iMissionID
	INT iCountOfMissionsToUnock			= GET_SVM_UNLOCK_VALUE(iMissionID)
	INT iArrayPos 						= SVM_FLOW_GET_NEXT_AVAILABLE_MISSION_CONTENT_ARRAY_POS(iMissionID)
	FLOAT fMissionCashMultiplier		= 0.0
	FLOAT fMissionRPMultiplier			= 0.0
	TEXT_LABEL tlMissionName			= GET_SVM_MISSION_NAME(iMissionID)
	BOOL bMissionUnlocked				= (iVehicleMissionsCompleted >= iCountOfMissionsToUnock)
	
	IF bMissionUnlocked
	AND AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		bMissionUnlocked = FALSE
	ENDIF
	
	STRING sTextureHandle
	SCRIPT_TIMER stDescriptionLoadTO
	
	IF bFallback
		sTextureHandle = GET_SVM_SCALEFORM_TXD(iMissionID)
		CDEBUG1LN(DEBUG_INTERNET, "[SECURO] POPULATE_SPECIAL_VEH_DATA - bFallback = ", BOOL_TO_INT(bFallback), " , using alternate SVM image.")
	ELSE
		sTextureHandle = TEXT_LABEL_TO_STRING(g_txtSVMThumbnails[iMissionID])
	ENDIF

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SvmFlowAlwaysAvailable")
		bMissionUnlocked =  TRUE
	ENDIF
	#ENDIF
	
	IF bMissionUnlocked
		iCountOfMissionsToUnock = 0
	ENDIF
	
	//If we have a valid mision request the description
	IF iArrayPos != -1 
		IF NOT IS_CLOUD_DOWN_CLOUD_LOADER()
			WHILE NOT GET_DESCRIPTION_FOR_SVM(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].iMissionDecHash)
			
				IF NOT HAS_NET_TIMER_STARTED(stDescriptionLoadTO)
					START_NET_TIMER(stDescriptionLoadTO)
				ELIF HAS_NET_TIMER_EXPIRED(stDescriptionLoadTO, 750)
					iArrayPos = -1 
					CASSERTLN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA Description failed to load! using fallback")
					BREAKLOOP
				ENDIF
				
				CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA waiting on descriptions to load using GET_DESCRIPTION_FOR_SVM")
				WAIT(0)
			ENDWHILE
			
			IF iArrayPos != -1
				GET_XP_MULTIPLIER_AND_CASH_MULTIPLIER_BASED_ON_MISSION_TYPE(fMissionRPMultiplier, fMissionCashMultiplier, FMMC_TYPE_MISSION, FMMC_TYPE_MISSION_CONTACT, DEFAULT, DEFAULT, GET_HASH_KEY(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iArrayPos].tlName))
			ENDIF
		
			IF fMissionRPMultiplier = 1.0
				fMissionRPMultiplier = 0.0
			ELSE
				CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA Mission RP multiplier set to: ", fMissionRPMultiplier)
			ENDIF
			IF fMissionCashMultiplier = 1.0
				fMissionCashMultiplier = 0.0
			ELSE
				CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA Mission cash multiplier set to: ", fMissionCashMultiplier)
			ENDIF
		ELSE
			iArrayPos = -1
			CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA cloud down. Using fallback")
		ENDIF
	ELSE
		CASSERTLN(DEBUG_INTERNET, "<SECURO> POPULATE_SPECIAL_VEH_DATA Description failed to load because iArrayPos is -1")
	ENDIF
	
	//Documentation here: https://hub.take2games.com/display/RSGGTAV/SecuroServ+App
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_SPECIAL_VEHICLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpecialVehID) 					// Mission ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlMissionName) 					// Mission/Vehicle name
		
		IF iArrayPos != -1 	
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
				//Add the text description for the mission grabbed above
				REPEAT FMMC_NUM_LABELS_FOR_DESCRIPTION i
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sSVMDescriptions.tl63MissionDecription[i])
				ENDREPEAT
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			//Fallback on the old descriptions
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("") 		// Fallback Mission description
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureHandle)	// TXD reference
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCountOfMissionsToUnock)			// Count of regular missions to unlock this SVM
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bMissionUnlocked)					// If we've unlocked the mission or not
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(FLOOR(fMissionCashMultiplier))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(FLOOR(fMissionRPMultiplier))
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    eIEWarehouse - The warehouse we check to see if is on sale
///    eIEWarehouseInterior - The interior we check to see if is on sale
///    bIncludeWHCost - Pass true if the cost of the WH shuld be added
///    bsalePrice - Pass true if you need the sale price of the vehicle warehouse
/// RETURNS:
///    The price of the interior passed incluing sale discounts and office base price if required
FUNC INT GET_IE_WH_COST_WITH_INTERIOR(IMPORT_EXPORT_GARAGES eIEWarehouse, IE_GARAGE_INTERIORS eIEWarehouseInterior, BOOL bIncludeWHCost, BOOl bsalePrice)
	INT iTotalCost
	
	BOOL bOwnedWH = (eIEWarehouse = GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()))
	
	IF bsalePrice
		IF (IS_IE_GARAGE_ON_SALE(eIEWarehouse) & bIncludeWHCost)
		OR IS_IE_GARAGE_INTERIOR_ON_SALE(eIEWarehouseInterior, bOwnedWH)
			iTotalCost += GET_IE_GARAGE_INTERIOR_COST(eIEWarehouseInterior, bOwnedWH)
			
			IF bIncludeWHCost
				iTotalCost += GET_IE_GARAGE_COST(eIEWarehouse)
			ENDIF
		ELSE
			iTotalCost = -1
		ENDIF
	ELSE
		IF bIncludeWHCost 
			IF IS_IE_GARAGE_ON_SALE(eIEWarehouse)
				iTotalCost += GET_IE_GARAGE_BASE_COST(eIEWarehouse)
			ELSE
				iTotalCost += GET_IE_GARAGE_COST(eIEWarehouse)
			ENDIF
		ENDIF		
		
		IF IS_IE_GARAGE_INTERIOR_ON_SALE(eIEWarehouseInterior, bOwnedWH)
			iTotalCost += GET_IE_GARAGE_INTERIOR_BASE_COST(eIEWarehouseInterior, bOwnedWH)
		ELSE
			iTotalCost += GET_IE_GARAGE_INTERIOR_COST(eIEWarehouseInterior, bOwnedWH)
		ENDIF
	ENDIF
	
	RETURN iTotalCost
ENDFUNC

/// PURPOSE:
///    Returns the minimum sale value of all vehicles in a players vehicle warehouse
FUNC INT GET_IE_WAREHOUSE_VEHICLE_STOCK_VALUE()
	INT i, iTotalValue
	
	//Send data for the players owned vehicles
	REPEAT ciMAX_IE_OWNED_VEHICLES i
		IE_VEHICLE_ENUM eTempVeh = INT_TO_ENUM(IE_VEHICLE_ENUM, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iWarehouseVehicles[i])

		IF ENUM_TO_INT(eTempVeh) > ciIE_COVER_VEHICLE_INDEX
			eTempVeh = INT_TO_ENUM(IE_VEHICLE_ENUM, (ENUM_TO_INT(eTempVeh) - ciIE_COVER_VEHICLE_INDEX))
		ENDIF
		
		IF eTempVeh > IE_VEH_INVALID
		AND eTempVeh < IE_VEHICLE_COUNT
			iTotalValue += GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eTempVeh), IE_DROPOFF_PRIVATE_BUYER_1, FALSE)
		ENDIF	
	ENDREPEAT
	
	RETURN iTotalValue
ENDFUNC

PROC POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(IMPORT_EXPORT_GARAGES eIEWarehouse, INT iCollectedVehCount)

	IF eIEWarehouse = IE_GARAGE_STRAWBERRY
		EXIT
	ENDIF
		
	VECTOR vCoord	= GET_IE_GARAGE_MAP_MIDPOINT(GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(eIEWarehouse))
	BOOL bIsOwned	= (GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()) = eIEWarehouse)
	INT iVehWHID 	= (ENUM_TO_INT(eIEWarehouse) + iWarehouseOffset)
	
	INT iWHCostWithBasicInterior		= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_BASIC, !bIsOwned, FALSE)
	INT iWHSaleCostWithBasicInterior	= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_BASIC, !bIsOwned, TRUE)
	INT iWHCostWithUrbanInterior		= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_URBAN, !bIsOwned, FALSE)
	INT iWHSaleCostWithUrbanInterior	= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_URBAN, !bIsOwned, TRUE)
	INT iWHCostWithBrandedInterior		= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_BRANDED, !bIsOwned, FALSE)
	INT iWHSaleCostWithBrandedInterior	= GET_IE_WH_COST_WITH_INTERIOR(eIEWarehouse, IE_INTERIOR_BRANDED, !bIsOwned, TRUE)
	
	TEXT_LABEL tlName			= GET_IE_GARAGE_NAME_LABEL(eIEWarehouse)
	TEXT_LABEL locationLabel 	= GET_NAME_OF_ZONE(vCoord)
	TEXT_LABEL descriptionLabel = GET_IE_GARAGE_DESCRIPTION(eIEWarehouse)
	TEXT_LABEL txdLabel 		= GET_IE_GARAGE_TXD(eIEWarehouse)
	INT iVehicleCapacity		= 40
	INT iNumVehiclesStored		= iCollectedVehCount
	INT iValueOfVehiclesStored	= 0
	INT iOwnedInterior			= -1
	FLOAT fStealCooldown		= 0.0
	
	IF NOT bIsOwned
		iNumVehiclesStored = 0
	ENDIF
	
	IF eIEWarehouse = IE_GARAGE_LISA
	OR eIEWarehouse = IE_GARAGE_LISA2
		locationLabel = "WH_LISA_1"
	ENDIF
	
	IF bIsOwned
		iOwnedInterior = ENUM_TO_INT(GET_PLAYERS_OWND_IE_WAREHOUSE_INTERIOR(PLAYER_ID()))		
		
		//The basic interior has a 0 or $125000 cost depending on if we're renovating or not
		iWHCostWithBasicInterior = GET_IE_GARAGE_INTERIOR_BASE_COST(IE_INTERIOR_BASIC, TRUE)
		
		//Get the value of all owned vehicles
		iValueOfVehiclesStored = GET_IE_WAREHOUSE_VEHICLE_STOCK_VALUE()
		
		CPRINTLN(DEBUG_INTERNET, "<SECURO> POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN Passing owned interior: ", iOwnedInterior, " value of owned vehicles: ", iValueOfVehiclesStored)
	ENDIF
	
	IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_EYE_IN_THE_SKY)
		fStealCooldown = TO_FLOAT(GB_GET_MISSION_COOLDOWN_TIME_REMAINING(VEV_EYE_IN_THE_SKY))
		CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN Cooldown timer running. iTimeRemaining = ", fStealCooldown)
	ENDIF
	
	//Documentation here: https://hub.take2games.com/display/RSGGTAV/SecuroServ+App
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_VEHICLE_WAREHOUSE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehWHID) 							// Vehicle WH ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vCoord.x) 						// X position in world coordinates
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vCoord.y) 						// Y position in world coordinates
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHCostWithBasicInterior) 			// Purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHSaleCostWithBasicInterior) 		// On sale purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHCostWithUrbanInterior) 			// Purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHSaleCostWithUrbanInterior) 		// On sale purchase cost	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHCostWithBrandedInterior) 		// Purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWHSaleCostWithBrandedInterior) 	// On sale purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlName) 						// Warehouse name text label 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(locationLabel) 					// Warehouse location text label 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(descriptionLabel)				// Warehouse description text label 		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(txdLabel) 				// Warehouse image txd reference		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bIsOwned) 						// Is warehouse owned by player?
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehicleCapacity)					// Warehouse capacity		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumVehiclesStored) 				// Number of vehicles currently stored		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iValueOfVehiclesStored) 				// Lifetime earnings
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fStealCooldown)					// Steal cooldown time remaining
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iOwnedInterior) 					// Lifetime earnings
	END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

PROC POPULATE_MP_SECUROSERV_SITE_PIN(INT iWarehouse)

	TEXT_LABEL nameLabel 			= GET_WAREHOUSE_NAME(iWarehouse)
	VECTOR vCoord 					= GET_WAREHOUSE_COORDS(iWarehouse)
	TEXT_LABEL locationLabel 		= GET_NAME_OF_ZONE(vCoord)
	TEXT_LABEL descriptionLabel 	= GET_WAREHOUSE_DESCRIPTION(iWarehouse)
	TEXT_LABEL txdLabel 			= GET_WAREHOUSE_TXD(iWarehouse)
	INT purchaseCost 				= GET_WAREHOUSE_COST(iWarehouse)
	BOOL isOwned 					= DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
	EWarehouseSize eSize 			= GET_WAREHOUSE_SIZE(iWarehouse)
	INT capacity 					= GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
	INT amountStored 				= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
	INT iSellValue		 			= 0
	INT buyTimeRem					= GET_CONTRABAND_BUY_COOLDOWN(iWarehouse)
	INT iWarehouseSlot				= -1
	INT iSalePrice					= -1
	INT iSellTimeRem				= -1
	
	//If necessary grab the time remaining from the sell mission tuneable timer
	IF buyTimeRem <= 0
		IF NOT GB_HAS_BUY_MISSION_TIMER_EXPIRED()
			buyTimeRem = g_sMPTunables.iexec_buy_fail_cooldown - ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), g_sGbWorkCoolDownVars.gbBuyCooldownTimer.Timer))
		ENDIF
	ENDIF
	
	IF buyTimeRem > 0
		SET_BIT(iBS, SEC_BS_BUY_MISSION_ON_COOLDOWN)
		SET_BIT(iWarehouseSellCooldownBS, iWarehouse)
	ENDIF
	
	IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
		iWarehouseSlot 	= GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
		iSellValue 		= (GB_GET_PER_CRATE_CASH_REWARD(amountStored) * amountStored) + GB_CALCULATE_SPECIAL_ITEM_REWARD(iWarehouse)
	ENDIF
	
	//url:bugstar:2803928
	IF iWarehouse = ciW_LSIA_1
	OR iWarehouse = ciW_LSIA_2
		locationLabel = "WH_LISA_1"
	ENDIF
	
	IF IS_WAREHOUSE_ON_SALE(iWarehouse)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Warehouse on sale: ", iWarehouse)
		iSalePrice		= purchaseCost
		purchaseCost 	= GET_WAREHOUSE_BASE_COST(iWarehouse)		
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_WAREHOUSE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SCALEFORM_INDEX_FROM_WAREHOUSE_INDEX(iWarehouse)) 		// Warehouse ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vCoord.x) 			// X position in world coordinates
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vCoord.y) 			// Y position in world coordinates
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(purchaseCost) 			// Purchase cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(nameLabel) 			// Warehouse name text label 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(locationLabel) 		// Warehouse location text label 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(descriptionLabel)	// Warehouse description text label 
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(txdLabel) 	// Warehouse image txd reference
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(isOwned) 				// Is warehouse owned by player?
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSize))	// Warehouse size - 0:Small, 1:Medium, 2:Large
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(capacity) 				// Warehouse capacity in units of contraband
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(amountStored) 			// How many units of contraband the player currently has in the warehouse
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)			// Value of the contraband currently stored in the warehouse.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellTimeRem)			// if > 0 this adds a timer to the Sell contraband buttons
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(buyTimeRem)			// if > 0 this adds a timer to the buy buttons
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWarehouseSlot)		// if > -1 this controls the order in which owned warehouses are added to the graph
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSalePrice)			// if > -1 this tells securoServ that the item is on sale
	END_SCALEFORM_MOVIE_METHOD()
	
	TEXT_LABEL_23 tlContrabandType = GET_WAREHOUSE_CONTRABAND_TYPE_LABEL_FOR_SECURO_APP(g_eContrabandType)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_WAREHOUSE_SHIPMENTS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SCALEFORM_INDEX_FROM_WAREHOUSE_INDEX(iWarehouse)) 	// Warehouse ID
		
		INT iSmallShipmentCost 	= GET_CONTRABAND_SHIPMENT_COST(CONTRABAND_SMALL)
		INT iMediumShipmentCost = GET_CONTRABAND_SHIPMENT_COST(CONTRABAND_MEDIUM)
		INT iLargeShipmentCost 	= GET_CONTRABAND_SHIPMENT_COST(CONTRABAND_LARGE)
		INT iSmallShipmentSize 	= ENUM_TO_INT(CONTRABAND_SMALL)
		INT iMedShipmentSize 	= ENUM_TO_INT(CONTRABAND_MEDIUM)
		
		INT iSmallShipmentSaleCost	= -1
		INT iMediumShipmentSaleCost = -1
		INT iLargeShipmentSaleCost	= -1
	
		IF IS_CONTRABAND_SHIPMENT_ON_SALE(CONTRABAND_MEDIUM)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Medi Contraband shipment on sale")
			iMediumShipmentSaleCost	= iMediumShipmentCost
			iMediumShipmentCost 	= GET_CONTRABAND_BASE_SHIPMENT_COST(CONTRABAND_MEDIUM)		
		ENDIF
	
		IF IS_CONTRABAND_SHIPMENT_ON_SALE(CONTRABAND_LARGE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "LARGE Contraband shipment on sale")
			iLargeShipmentSaleCost	= iLargeShipmentCost
			iLargeShipmentCost 		= GET_CONTRABAND_BASE_SHIPMENT_COST(CONTRABAND_LARGE)		
		ENDIF
		
		IF IS_BIT_SET(iBS2, SEC_BS2_SPECIAL_ITEM)
			CONTRABAND_SPECIAL_ITEM eItem = GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(securoContraType)
			IF IS_SPECIAL_CONTRABAND_ITEM_ON_SALE(eItem)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Special Contraband shipment on sale")
				iSmallShipmentSaleCost 	= GET_SPECIAL_CONTRABAND_ITEM_COST(eItem)
				iSmallShipmentCost		= GET_SPECIAL_CONTRABAND_ITEM_BASE_COST(eItem)
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Special Contraband shipment available")
				iSmallShipmentCost = GET_SPECIAL_CONTRABAND_ITEM_COST(eItem)
			ENDIF
		ELIF IS_CONTRABAND_SHIPMENT_ON_SALE(CONTRABAND_SMALL)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Small Contraband shipment on sale")
			iSmallShipmentSaleCost	= iSmallShipmentCost
			iSmallShipmentCost 		= GET_CONTRABAND_BASE_SHIPMENT_COST(CONTRABAND_SMALL)
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_PLAYER_ON_MP_INTRO()
			//Remove the smaller shipment sizes as they would all be free
			iSmallShipmentSize	= -1
			iMedShipmentSize	= -1
			
			//Set the price to free
			iLargeShipmentSaleCost	= -1
			iLargeShipmentCost 		= 0	
		ENDIF
		#ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSmallShipmentSize)						// smallShipmentSize	int	How many units of contraband are in a small shipment for this warehouse. -1 is used to indicate that this shipment size isn't available.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSmallShipmentCost) 						// smallShipmentCost	int	How much a small shipment costs to purchase.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlContrabandType) 						// smallShipmentType	string	A description of what the small shipment is (eg "guns", "jewels" etc)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMedShipmentSize)							// mediumShipmentSize	int	How many units of contraband are in a medium shipment for this warehouse. -1 is used to indicate that this shipment size isn't available.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMediumShipmentCost) 						// mediumShipmentCost	int	How much a medium shipment costs to purchase.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")										// No longer used (Empty string requested)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(CONTRABAND_LARGE))				// largeShipmentSize	int	How many units of contraband are in a large shipment for this warehouse. -1 is used to indicate that this shipment size isn't available.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLargeShipmentCost) 						// largeShipmentCost	int	How much a large shipment costs to purchase.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(IS_BIT_SET(iBS2, SEC_BS2_SPECIAL_ITEM))	// Is special item active for the one crate option
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSmallShipmentSaleCost) 					// Small shipment sale cost		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMediumShipmentSaleCost) 					// Medium shipment sale cost
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLargeShipmentSaleCost) 					// Large shipment salecost
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL SHOULD_WAREHOUSE_PIN_BE_ADDED_TO_MAP(INT iWarehouse)
	IF NOT IS_WAREHOUSE_DISABLED_BY_TUNEABLE(iWarehouse)
	#IF FEATURE_CASINO_HEIST
	AND NOT WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
	#ENDIF
		IF GET_COUNT_OF_WAREHOUSES_OWNED() < ciMaxOwnedWarehouses
		OR GET_WAREHOUSE_SIZE(iWarehouse) = eWarehouseLarge
			RETURN TRUE
		ELIF GET_WAREHOUSE_SIZE(iWarehouse) = eWarehouseMedium
		AND GET_SMALLEST_OWNED_WAREHOUSE() = eWarehouseMedium
			RETURN TRUE
		ELIF GET_SMALLEST_OWNED_WAREHOUSE() = eWarehouseSmall
			RETURN TRUE
		ENDIF
	ELIF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_INTERNET, "[WHOUSE] SHOULD_WAREHOUSE_PIN_BE_ADDED_TO_MAP RETURNING FALSE for warehouse: ", iWarehouse)
	RETURN FALSE
ENDFUNC

PROC GET_CONTRABAND_BUYER_INFO(INT &iSellVal, TEXT_LABEL_63 &sBuyerOrg, TEXT_LABEL_63 &sQuantity, INT ibuyerNo, INT amountStored)
	
	BOOL bPlural
	INT iSpecialCount = GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iCurrentlyInsideWarehouse)
	INT NonSpecialContraTotal = amountStored - iSpecialCount
	
	IF NOT IS_WAREHOUSE_EMPTY(iCurrentlyInsideWarehouse)
	AND (ibuyerNo = 0
	OR NonSpecialContraTotal >= 2 AND ibuyerNo < 2
	OR NonSpecialContraTotal > 2 AND ibuyerNo <= 2
	OR ibuyerNo = 3 AND DOES_WAREHOUSE_CONTAIN_A_SPECIAL_ITEM(iCurrentlyInsideWarehouse))
	AND (amountStored != iSpecialCount OR ibuyerNo = 3)
	
		INT iItemCount 
		INT iSaveSlot 	= GET_SAVE_SLOT_FOR_WAREHOUSE(iCurrentlyInsideWarehouse)
		
		sQuantity = ""
		
		//Because we're just setting the values, satisfy the compiler :(
		UNUSED_PARAMETER(sBuyerOrg)
		UNUSED_PARAMETER(sQuantity)
		
		SWITCH ibuyerNo
			CASE 0
				iItemCount = ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER1_AMOUNT) / 100))
				iSellVal 	= GB_GET_PER_CRATE_CASH_REWARD(iItemCount) * iItemCount
				sBuyerOrg 	= GET_CONTRABAND_BUYER_COMPANY(g_iContrabandBuyerOneID[iSaveSlot])
				sQuantity	= iItemCount
				
				bPlural = (iItemCount > 1)
			BREAK
			CASE 1
				iItemCount = ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER2_AMOUNT) / 100))
				
				iSellVal 	= GB_GET_PER_CRATE_CASH_REWARD(iItemCount) * iItemCount
				sBuyerOrg 	= GET_CONTRABAND_BUYER_COMPANY(g_iContrabandBuyerTwoID[iSaveSlot])
				sQuantity	= iItemCount
				
				bPlural = (iItemCount > 1)
			BREAK
			CASE 2
				iItemCount = ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER3_AMOUNT) / 100))
				
				iSellVal 	= GB_GET_PER_CRATE_CASH_REWARD(iItemCount) * iItemCount
				sBuyerOrg 	= GET_CONTRABAND_BUYER_COMPANY(g_iContrabandBuyerThreeID[iSaveSlot])
				sQuantity	= iItemCount
				
				bPlural = (iItemCount > 1)
			BREAK
			CASE 3
				iSellVal 	= GB_CALCULATE_SPECIAL_ITEM_REWARD(iCurrentlyInsideWarehouse)
				sBuyerOrg 	= GET_CONTRABAND_BUYER_COMPANY(g_iContrabandBuyerFourID[iSaveSlot])
			BREAK
		ENDSWITCH
		
		IF g_sMPTunables.benable_control_payout_Executive_Office_SpecialCargo
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RECEIVED_BOOSTED_EXEC_SPECIAL_CARGO_SELL_PAYOUT)
				PRINTLN("<SECURO>[BOOSTED_PAYOUTS] Giving booseted payout for exex special cargo. From: ", iSellVal, " to: ", (iSellVal * g_sMPTunables.icash_control_payout_Executive_Office_SpecialCargo))
				iSellVal *= g_sMPTunables.icash_control_payout_Executive_Office_SpecialCargo
			ENDIF
		ENDIF
		
		IF ibuyerNo = 3
			sQuantity += GET_FILENAME_FOR_AUDIO_CONVERSATION("CBUYERCRS")
		ELSE
			IF bPlural
				sQuantity += GET_FILENAME_FOR_AUDIO_CONVERSATION("CBUYERCRP")
			ELSE
				sQuantity += GET_FILENAME_FOR_AUDIO_CONVERSATION("CBUYERCR")
			ENDIF
		ENDIF
	ELSE
		iSellVal 	= -1
		sBuyerOrg 	= ""
		sQuantity	= ""
	ENDIF
ENDPROC

FUNC VECTOR GET_OFFICE_FRONT_DOOR_POS(INT iCurrentProperty)
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1		RETURN <<-1582.5367, -556.0219, 33.9540>>
		CASE PROPERTY_OFFICE_2_BASE	RETURN <<-1373.8700, -502.7115, 32.1574>>
		CASE PROPERTY_OFFICE_3		RETURN <<-117.5296, -605.7157, 35.2857>>
		CASE PROPERTY_OFFICE_4		RETURN <<-74.7579, -818.4601, 325.1753>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC APP_SECURO_SHOW_PAGE(INT iPageID)
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "showScreen")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPageID)	
	END_SCALEFORM_MOVIE_METHOD()
	
	iCurrentPageID = iPageID
	PRINTLN("<SECURO> APP_SECURO_SHOW_PAGE - Open page: ", iPageID)
ENDPROC

PROC POPULATE_MP_SECUROSERV_SITE_PIN_MAP(BOOL bCanAccessApp)
	
	securoContraType 	= g_eContrabandType
	IF IS_SPECIAL_CONTRABAND_ITEM_ACTIVE()
		SET_BIT(iBS2, SEC_BS2_SPECIAL_ITEM)
	ENDIF
	
	IF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		TEXT_LABEL nameLabel 		= GET_WAREHOUSE_NAME(iCurrentlyInsideWarehouse)
		VECTOR vCoord 				= GET_WAREHOUSE_COORDS(iCurrentlyInsideWarehouse)
		TEXT_LABEL locationLabel 	= GET_NAME_OF_ZONE(vCoord)
		TEXT_LABEL txdLabel 		= ""
		TEXT_LABEL_63 sBuyerOrg		= ""
		TEXT_LABEL_63 sQuantity		= ""
		EWarehouseSize eSize 		= GET_WAREHOUSE_SIZE(iCurrentlyInsideWarehouse)
		INT capacity 				= GET_WAREHOUSE_MAX_CAPACITY(iCurrentlyInsideWarehouse)
		INT amountStored 			= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iCurrentlyInsideWarehouse)
		INT iSellValue		 		= -1
		INT iSpecialItemCount		= GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iCurrentlyInsideWarehouse)
		
		//Grab this time here to make sure all timers are consistent
		INT iSellTimeRem 	= GET_CONTRABAND_SELL_COOLDOWN(iCurrentlyInsideWarehouse)
		
		//If necessary grab the time remaining from the sell mission tuneable timer
		IF iSellTimeRem <= 0
			IF NOT GB_HAS_SELL_MISSION_TIMER_EXPIRED()
				iSellTimeRem = g_sMPTunables.iexec_sell_fail_cooldown - ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), g_sGbWorkCoolDownVars.gbSellCooldownTimer.Timer))
			ENDIF
		ENDIF
		
		IF iSellTimeRem > 0
			SET_BIT(iBS, SEC_BS_SELL_MISSION_ON_COOLDOWN)
		ENDIF
		
		IF DOES_WAREHOUSE_CONTAIN_A_SPECIAL_ITEM(iCurrentlyInsideWarehouse)
			txdLabel = GET_SPECIAL_ITEM_TXD_LABEL(iCurrentlyInsideWarehouse)
		ENDIF
		
		GET_CONTRABAND_BUYER_INFO(iSellValue, sBuyerOrg, sQuantity, 0, amountStored)
		
		CDEBUG1LN(DEBUG_INTERNET, "iCurrentlyInsideWarehouse: ", iCurrentlyInsideWarehouse, " nameLabel: ", nameLabel, " locationLabel: ", locationLabel, " txdLabel: ", txdLabel, " eSize: ", eSize, " capacity: ", capacity, " amountStored: ", amountStored )
		
		INT iSellMissionsComplete 		= GET_SELL_MISSION_COMPLETED_COUNT()
		INT iSellCompletionPercentage 	= FLOOR(CLAMP((TO_FLOAT(iSellMissionsComplete) /  TO_FLOAT(GET_SELL_MISSION_UNDERTAKEN_COUNT())) * 100, 0, 100))
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_PLAYER_DATA")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_NAME(PLAYER_ID()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_ORGANISATION_NAME(PLAYER_ID()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iSellCompletionPercentage))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellMissionsComplete)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_LIFETIME_CONTRABAND_EARNINGS())
		END_SCALEFORM_MOVIE_METHOD()
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_BUYER_DATA")
			//Buyer 1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBuyerOrg)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sQuantity)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)
			
			//Buyer 2
			GET_CONTRABAND_BUYER_INFO(iSellValue, sBuyerOrg, sQuantity, 1,amountStored)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBuyerOrg)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sQuantity)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)
			
			//Buyer 3
			GET_CONTRABAND_BUYER_INFO(iSellValue, sBuyerOrg, sQuantity, 2, amountStored)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBuyerOrg)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sQuantity)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)
			
			//Buyer 4
			GET_CONTRABAND_BUYER_INFO(iSellValue, sBuyerOrg, sQuantity, 3, amountStored)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sBuyerOrg)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sQuantity)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)			
		END_SCALEFORM_MOVIE_METHOD()		
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_WAREHOUSE_DATA")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(nameLabel)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(locationLabel)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(txdLabel)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eSize))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(capacity)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(amountStored)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellValue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpecialItemCount)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellTimeRem)
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		// Set the login username and the flag to indicate whether they can access the app or not
		/*
		SET_PLAYER_DATA
		gamerTag				string	The username displayed in the login panel.
		gamerHasAccess			bool	Flag to determine whether the map screen (true) or the access denied overlay (false) is shown after pressing the log in button.
		totalEarnings			float	How much money the player has made from their warehouses.
		collectionsCompleted	int		How many collections the player has completed.
		collectionSuccessRate	float	Percentage of collections that were successful.
		salesCompleted			int		How many sales the player has completed.
		salesSuccessRate 		float	Percentage of sales that were successful.
		organisation			string	The player's organisation name.
		numVehiclesStolen		int		How many vehicles the player has stolen.
		stealVehiclesSuccess	float	Percentage of successful vehicle steals.
		vehiclesExported		int		How many vehicles the player has successfully exported.
		exportVehiclesSuccess	float	Percentage of successful vehicle exports.
		totalVehicleEarnings	int		How much money the player has made from exporting vehicles.
		bCanAccessSVMMissions	bool	Informs SF as to if the player has unlocked the SVM missions
		vOfficePosition.x		float	Players x pos in world coords
		vOfficePosition.y		float	Players y pos in world coords
		*/
		
		VECTOR vOfficePosition 			= GET_OFFICE_FRONT_DOOR_POS(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0))
		
		INT iBuyMissionsComplete 		= GET_BUY_MISSION_COMPLETED_COUNT()
		INT iSellMissionsComplete 		= GET_SELL_MISSION_COMPLETED_COUNT()
		
		INT iBuyCompletionPercentage	= FLOOR(CLAMP((TO_FLOAT(iBuyMissionsComplete) /  TO_FLOAT(GET_BUY_MISSION_UNDERTAKEN_COUNT())) * 100, 0, 100))
		INT iSellCompletionPercentage 	= FLOOR(CLAMP((TO_FLOAT(iSellMissionsComplete) /  TO_FLOAT(GET_SELL_MISSION_UNDERTAKEN_COUNT())) * 100, 0, 100))

		INT iNumVehStolen 				= GET_TOTAL_NUM_VEHICLES_STOLEN()
		INT iNumVehExported				= GET_TOTAL_NUM_EXPORT_MISSIONS_COMPLETED()
		INT iTotalExportEarnings 		= GET_TOTAL_EXPORT_EARNINGS()
		
		INT iStealCompletionPercentage 	= FLOOR(CLAMP((TO_FLOAT(iNumVehStolen) / TO_FLOAT(GET_TOTAL_NUM_STEAL_MISSIONS_STARTED())) * 100, 0, 100))
		INT iExportCompletionPercentage = FLOOR(CLAMP((TO_FLOAT(GET_TOTAL_NUM_EXPORT_MISSIONS_COMPLETED()) / TO_FLOAT(GET_TOTAL_NUM_EXPORT_MISSIONS_STARTED())) * 100, 0, 100))
		
		INT iSVMUnlockCount				= GET_MINIMUM_STOLEN_VEH_COUNT_TO_UNLOCK_SVM()
		
		BOOL bCanAccessSVMMissions		= (iNumVehStolen >= iSVMUnlockCount)
		
		CDEBUG1LN(DEBUG_INTERNET, "<SECURO> POPULATE_MP_SECUROSERV_SITE_PIN_MAP: Minimum stolen vehicles to unlock SVM section: ", iSVMUnlockCount, " iNumVehStolen: ", iNumVehStolen)
		
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_SvmFlowAlwaysAvailable")
				bCanAccessSVMMissions = TRUE
			ENDIF
		#ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_PLAYER_DATA")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_NAME(PLAYER_ID()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanAccessApp)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(GET_LIFETIME_CONTRABAND_EARNINGS()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBuyMissionsComplete)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iBuyCompletionPercentage))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSellMissionsComplete)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iSellCompletionPercentage))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_ORGANISATION_NAME(PLAYER_ID()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumVehStolen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iStealCompletionPercentage))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumVehExported)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iExportCompletionPercentage))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalExportEarnings)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanAccessSVMMissions)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vOfficePosition.x)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vOfficePosition.y)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			#IF FEATURE_GEN9_EXCLUSIVE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(!IS_PLAYER_ON_MP_INTRO())
			#ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		
		INT iWarehouse
		FOR iWarehouse = 1 TO ciMaxWarehouses
			IF SHOULD_WAREHOUSE_PIN_BE_ADDED_TO_MAP(iWarehouse)
				POPULATE_MP_SECUROSERV_SITE_PIN(iWarehouse)
			ENDIF
		ENDFOR
		
		//Populate the map with the IE warehouses
		INT iIEWarehouse, iMission
		INT iCollectedVehCount = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal
		
		FOR iIEWarehouse = 1 TO (ENUM_TO_INT(IE_GARAGE_UPPER_LIMIT) -1)
			#IF FEATURE_CASINO_HEIST
			IF NOT WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
			OR ENUM_TO_INT(GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())) = iIEWarehouse
			#ENDIF
				POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iIEWarehouse), iCollectedVehCount)
			#IF FEATURE_CASINO_HEIST
			ENDIF
			#ENDIF
		ENDFOR
		
		REPEAT ciMAX_AT_FLOW_MISSIONS iMission
			IF SHOULD_SVM_MISSION_BE_ADDED_TO_SELECTION(iMission)
				POPULATE_SPECIAL_VEH_DATA(iMission, iNumVehStolen, g_bSVMThumbnailFallback)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Initalises the iActiveUsers array to -1
PROC INIT_ACTIVE_USER_LIST()
	INT i = 0
	
	REPEAT GB_GET_MAX_NUM_GANGS() i
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[securo] INIT_ACTIVE_USER_LIST Setting value: ", i, " of app active user list to -1")
		iActiveUsers[i] = -1
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks a local array for the current player in the loop (iPlayerLoop)
FUNC BOOL IS_PLAYER_IN_ACTIVE_GANG_LIST()
	INT i = 0
	
	REPEAT GB_GET_MAX_NUM_GANGS() i
		IF iActiveUsers[i] = iPlayerLoop
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Adds a player to the local array and calls the relevant scaleform method
PROC ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST(PLAYER_INDEX piPlayer, INT iPlayerID)
	INT i = 0
	STRING sPlayerName = GET_PLAYER_NAME(piPlayer)
	
	IF ARE_STRINGS_EQUAL(sPlayerName, "**Invalid**")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SECURO] ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST. tried to add **Invalid** player to list")
		EXIT
	ENDIF
	
	INT iIter
	INT iMaxNumGoons = GB_MAX_GANG_GOONS_EXEC
	TEXT_LABEL_63 tlGoons[GB_MAX_GANG_GOONS_EXEC]
	
	REPEAT iMaxNumGoons iIter
		PLAYER_INDEX piGoon = GlobalplayerBD_FM_3[iPlayerID].sMagnateGangBossData.GangMembers[iIter]
		
		IF piGoon != INVALID_PLAYER_INDEX()
			tlGoons[iIter] = GET_PLAYER_NAME(piGoon)
		ELSE
			tlGoons[iIter] = ""
		ENDIF
	ENDREPEAT
	
	//Make sure there are no spaces in the list
	REPEAT iMaxNumGoons iIter
		IF IS_STRING_NULL_OR_EMPTY(tlGoons[iIter])
			IF iIter != iMaxNumGoons
				INT j
				
				FOR j = iIter TO (iMaxNumGoons -1)
					IF NOT IS_STRING_NULL_OR_EMPTY(tlGoons[j])
						tlGoons[iIter] 	= tlGoons[j]
						tlGoons[j] 		= ""
						j 				= (iMaxNumGoons -1)
						
						CDEBUG1LN(DEBUG_INTERNET, "ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST Updating list order Empty player slot: ", iIter, " Moving Goon: ", j, " down the list")
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDREPEAT
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_ACTIVE_USER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sPlayerName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(GET_PLAYER_ORGANISATION_NAME(piPlayer))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlGoons[0])
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlGoons[1])
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlGoons[2])
	END_SCALEFORM_MOVIE_METHOD()
	
	REPEAT GB_GET_MAX_NUM_GANGS() i
		IF iActiveUsers[i] 	= -1
			iActiveUsers[i] = iPlayerID
			tlUserList[i] 	= sPlayerName
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[SECURO] ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST. Adding: ", tlUserList[i], " to slot: ", i)
			EXIT
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[SECURO] ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST. We have more players to add than GB_GET_MAX_NUM_GANGS: ", GB_GET_MAX_NUM_GANGS())
ENDPROC

/// PURPOSE:
///    Removes a player from the local array and calls the relevant scaleform method
PROC REMOVE_PLAYER_FROM_ACTIVE_ORGANISATION_LIST()
	INT i = 0
	INT iIndex = -1
	
	REPEAT GB_GET_MAX_NUM_GANGS() i
		IF iActiveUsers[i] = iPlayerLoop
			iActiveUsers[i] = -1
			iIndex = i
		ENDIF
	ENDREPEAT
	
	IF iIndex != -1
	AND iIndex < GB_GET_MAX_NUM_GANGS()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SECURO] REMOVE_PLAYER_FROM_ACTIVE_ORGANISATION_LIST. index: ", iIndex)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[SECURO] REMOVE_PLAYER_FROM_ACTIVE_ORGANISATION_LIST. removing: ", tlUserList[iIndex])
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "REMOVE_ACTIVE_USER")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlUserList[iIndex])
		END_SCALEFORM_MOVIE_METHOD()
		tlUserList[iIndex] = ""
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[SECURO] REMOVE_PLAYER_FROM_ACTIVE_ORGANISATION_LIST. Can't find player to remove: ", iIndex)
	ENDIF
ENDPROC

/// PURPOSE: One player per frame loop to check gang status
///   Maintains the list of organizations to be displayed in the app
PROC MAINTAIN_ACTIVE_ORGANIZATION_LIST()
	PLAYER_INDEX pPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
	
	IF NETWORK_IS_PLAYER_ACTIVE(pPlayer)
	AND GB_IS_PLAYER_BOSS_OF_A_GANG(pPlayer)
	AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(pPlayer)
	//AND NOT GB_ARE_PLAYER_BOSS_PRIVILIGES_SUSPENDED_BD(PLAYER_ID())
		IF NOT IS_PLAYER_IN_ACTIVE_GANG_LIST()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[securo] Adding player: ", iPlayerLoop, " to app active user list")
			ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST(pPlayer, iPlayerLoop)
		ENDIF
	ELSE
		IF IS_PLAYER_IN_ACTIVE_GANG_LIST()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[securo] Removing player: ", iPlayerLoop, " from app active user list")
			REMOVE_PLAYER_FROM_ACTIVE_ORGANISATION_LIST()
		ENDIF
	ENDIF
	
	iPlayerLoop ++
	
	IF iPlayerLoop = NUM_NETWORK_PLAYERS
		iPlayerLoop = 0
	ENDIF	
ENDPROC

PROC SECUROSERVE_CONTROL_DISABLE()
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

	IF (NOT IS_SYSTEM_UI_BEING_DISPLAYED())
	AND (NOT IS_PAUSE_MENU_ACTIVE())
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
ENDPROC

PROC RENDER_SECUROSERVE()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SECUROSERVE_CONTROL_DISABLE()
	
	IF IS_BIT_SET(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	ENDIF
	
	THEFEED_HIDE_THIS_FRAME()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_PAUSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,255)
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	
	g_sBusAppManagement.iRenderHandShakeFC = GET_FRAME_COUNT()
ENDPROC

FUNC BOOL IS_MAP_SIDEBAR_OPEN()
	RETURN IS_BIT_SET(iBS, SEC_BS_SIDE_PANEL_SHOWING)
ENDFUNC

PROC CLEAR_OVERLAY_WARNING_BS()
	IF IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
		CLEAR_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
	ENDIF
	
	IF IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_CONFIRM)
		CLEAR_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_CONFIRM)
	ENDIF
ENDPROC

PROC CLOSE_OVERLAY_SCREEN(BOOL bPlaySound = FALSE, BOOL bClearIEPurAndStealBitSet = FALSE)
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "HIDE_OVERLAY")						
	END_SCALEFORM_MOVIE_METHOD()
	CLEAR_OVERLAY_WARNING_BS()
	
	IF bPlaySound
		PLAY_SOUND_FRONTEND(-1, "Popup_Cancel", "GTAO_Exec_SecuroServ_Computer_Sounds" )
	ENDIF
	
	IF bClearIEPurAndStealBitSet
		IF IS_BIT_SET(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			CDEBUG1LN(DEBUG_INTERNET, "<SECURO> CLOSE_OVERLAY_SCREEN clearing bit SEC_BS_LAUNCH_STEAL_MIS_SELECTED")
			CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
		ENDIF
		
		IF IS_BIT_SET(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
			CDEBUG1LN(DEBUG_INTERNET, "<SECURO> CLOSE_OVERLAY_SCREEN clearing bit SEC_BS_PRUCHASE_IE_WH_SELECTED")
			CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
		ENDIF
	ENDIF
	
	CLEAR_BIT(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
	CLEAR_BIT(iBS, SEC_BS_OVERLAY_ACTIVE)
ENDPROC

/// PURPOSE:
///    Shows a popup message over the top of the current screen
/// PARAMS:
///    sTitleBar 			- Top bar textlable
///    sDescription 		- Description text label
///    sButton1 			- Left side button text label (Moves to center if "" is passed for sButton2)
///    sButton2 			- Pass "" to show no button. Right side button text label
///    bSuccess 			- True = Green False = Red overlay screen
///    bInteriorsOverlay 	- If true brings up the interior overlay instead of the standard screen
///    iAdditionalINT 		- Set to a value !-1 to add an additional INT to the overlay description
PROC START_OVERLAY_SCREEN(STRING sTitleBar, STRING sDescription, String sButton1, String sButton2, BOOL bSuccess , BOOL bInteriorsOverlay = FALSE, INT iAdditionalINT = -1 )
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SHOW_OVERLAY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitleBar)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sDescription)
			IF iadditionalINT != -1
				ADD_TEXT_COMPONENT_INTEGER(iadditionalINT)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sButton1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sButton2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSuccess)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bInteriorsOverlay)
	END_SCALEFORM_MOVIE_METHOD()
	
	SET_BIT(iBS, SEC_BS_OVERLAY_ACTIVE)
ENDPROC

PROC PASS_DPAD_INPUTS_TO_SCALEFORM()
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LEFT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RIGHT)))
	ENDIF
ENDPROC

PROC PASS_ANALOGUE_STICK_INPUTS_TO_SF(BOOL BPassInLeftStickInput, BOOL BPassInRightStickInput)
	IF BPassInLeftStickInput
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		INT iLeftXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		INT iLeftYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		IF (iLeftXNew != 127) OR (iLeftX != 127) OR (iLeftYNew != 127) OR (iLeftY != 127)
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_ANALOG_STICK_INPUT")
				iLeftX = iLeftXNew
				iLeftY = iLeftYNew
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftX)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftY)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
	IF BPassInRightStickInput
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		INT iRightXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		INT iRightYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		IF (iRightXNew != 127) OR (iRightX != 127) OR (iRightYNew != 127) OR (iRightY != 127)
			BOOL bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN))
		
			IF NOT bUsingScrollWheel
				bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP))
			ENDIF
			
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_ANALOG_STICK_INPUT")
				iRightX = iRightXNew
				iRightY = iRightYNew
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightX)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightY)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUsingScrollWheel)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

PROC PASS_CURSOR_INPUT_TO_SF()
	iCursorX = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	iCursorY = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
				
	IF iCursorX != iCursorXLast
	OR iCursorY != iCursorYLast
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_MOUSE_INPUT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iCursorX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iCursorY)
		END_SCALEFORM_MOVIE_METHOD()
		
		iCursorXLast = iCursorX
		iCursorYLast = iCursorY			
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_ACCEPT)))
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		IF iCurrentPageID != ciVehicleWHStatsPageID
		AND iCurrentPageID != ciSVMPageID
		OR NOT IS_BIT_SET(iBS, SEC_BS_OVERLAY_ACTIVE)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
		ENDIF
		IF iCurrentPageID = ciBranchPageID
		OR iCurrentPageID = ciLoginPageID
			CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, "EXITING WEBSITE")
			CLEANUP_SECUROSERVE()
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		IF IS_BIT_SET(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
			CLEAR_BIT(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
		ELIF IS_BIT_SET(iBS, SEC_BS_OVERLAY_ACTIVE)
			CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, " and bOverlayActive Closing overlay")
			CLOSE_OVERLAY_SCREEN(TRUE, TRUE)
		ELIF IS_MAP_SIDEBAR_OPEN()
			CLEAR_BIT(iBS, SEC_BS_SIDE_PANEL_SHOWING)
		ENDIF
	ENDIF
ENDPROC

PROC PASS_WAREHOUSE_INPUTS_TO_SCALEFORM()
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
		CLEANUP_SECUROSERVE()
	ENDIF
	
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
	AND NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, " EXITING WEBSITE for Triangle press")
		CLEANUP_SECUROSERVE()
	ENDIF
	
	PASS_DPAD_INPUTS_TO_SCALEFORM()
		
	// Mouse cursor control
	IF IS_USING_CURSOR(FRONTEND_CONTROL)
		PASS_CURSOR_INPUT_TO_SF()
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			CLEANUP_SECUROSERVE()
		ENDIF
	ELSE
		PASS_ANALOGUE_STICK_INPUTS_TO_SF(TRUE, TRUE)
	ENDIF
ENDPROC

PROC PASS_INPUTS_TO_SCALEFORM()
		
	IF g_bBrowserGoToStoreTrigger
		CPRINTLN(DEBUG_INTERNET, "APP_SECURO g_bBrowserGoToStoreTrigger()")
		
		IF g_bBrowserNagScreenState 
			CPRINTLN(DEBUG_INTERNET, "APP_SECURO g_bBrowserNagScreenState()")
			
		   IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		   OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_ACCEPT) AND NOT IS_WARNING_MESSAGE_ACTIVE())
				
				g_bBrowserNagScreenState 	= FALSE
				g_bBrowserGoToStoreTrigger 	= FALSE
				CLEAR_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
				
				SET_LAST_VIEWED_SHOP_ITEM(g_iBrowserGoToStoreItemHash, g_iBrowserGoToStoreItemPrice, 0)
				OPEN_COMMERCE_STORE("", "", SPL_PHONE)
				
				WHILE IS_COMMERCE_STORE_OPEN()
					RENDER_SECUROSERVE()
					WAIT(0)
				ENDWHILE
		   ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
		   OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL) AND NOT IS_WARNING_MESSAGE_ACTIVE())
				CPRINTLN(DEBUG_INTERNET, "APP_SECURO canceled, bailing")
				
				PLAY_SOUND_FRONTEND(-1, "BACK","HUD_FRONTEND_DEFAULT_SOUNDSET") 
				
				g_bBrowserNagScreenState 	= FALSE
				g_bBrowserGoToStoreTrigger 	= FALSE
				CLEAR_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
				
				SET_BIT(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
		   ENDIF
		ENDIF
		EXIT
	ENDIF

	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		IF iCurrentPageID = ciVehicleWHMapPageID
			IF DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
				IF IS_BIT_SET(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
					CLEAR_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				ELSE
					SET_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				ENDIF
			ENDIF
		ENDIF
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LB)))
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		IF iCurrentPageID = ciVehicleWHMapPageID
			IF DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
				IF IS_BIT_SET(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
					CLEAR_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				ELSE
					SET_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				ENDIF
			ENDIF
		ENDIF
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RB)))
	ENDIF
	// NB: Using IS_CONTROL_PRESSED for continuous zooming out on map.
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LT)))
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LT)))
	ENDIF
	// NB: Using IS_CONTROL_PRESSED for continuous zooming in on map.
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RT)))
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RT)))
	ENDIF
	
	PASS_DPAD_INPUTS_TO_SCALEFORM()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		IF IS_BIT_SET(iBS, SEC_BS_CHECK_PAGE_ID)
			CPRINTLN(DEBUG_INTERNET,"<SECURO> Accept cancel input for SEC_BS_CHECK_PAGE_ID")
		ELSE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
		ENDIF
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF IS_BIT_SET(iBS, SEC_BS_CHECK_PAGE_ID)
			CPRINTLN(DEBUG_INTERNET,"<SECURO> Reject cancel input for SEC_BS_CHECK_PAGE_ID")
		ELSE
			#IF FEATURE_CASINO_HEIST
			IF WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
			AND g_sBusAppManagement.bRunningPrimaryApp
			AND NOT IS_BIT_SET(iBS, SEC_BS_OVERLAY_ACTIVE)
				IF iCurrentPageID = ciVehicleWHStatsPageID
				OR iCurrentPageID = ciWarehouseStatsPageID
					CPRINTLN(DEBUG_INTERNET,"<SECURO> Opened from hub app: CURRENT PAGE ID is:", iCurrentPageID, " EXITING WEBSITE")
					CLEANUP_SECUROSERVE()
				ENDIF
			ENDIF
			#ENDIF
			
			IF iCurrentPageID != ciVehicleWHStatsPageID
			AND iCurrentPageID != ciSVMPageID
			OR NOT IS_BIT_SET(iBS, SEC_BS_OVERLAY_ACTIVE)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
			ENDIF
			
			IF iCurrentPageID = ciBranchPageID
			OR iCurrentPageID = ciLoginPageID
				CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, " EXITING WEBSITE")
				CLEANUP_SECUROSERVE()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF IS_BIT_SET(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
			CLEAR_BIT(iBS2, SEC_BS2_FINISHED_SHOWING_NAG_SCREEN_LF)
		ELIF IS_BIT_SET(iBS, SEC_BS_OVERLAY_ACTIVE)
			CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, " and bOverlayActive Closing overlay")
			CLOSE_OVERLAY_SCREEN(TRUE, TRUE)
		ELIF IS_MAP_SIDEBAR_OPEN()
			CLEAR_BIT(iBS, SEC_BS_SIDE_PANEL_SHOWING)
		ENDIF
		
		IF IS_BIT_SET(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
			CLEAR_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
		ENDIF
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_X)))
	ENDIF
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
		ENDIF
	ENDIF
		
	// Mouse cursor control
	IF IS_USING_CURSOR(FRONTEND_CONTROL)	
	
		PASS_CURSOR_INPUT_TO_SF()
		
		IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_ACCEPT)))
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_CANCEL)))
			ENDIF
		ENDIF
		IF iCurrentPageID = ciSVMPageID
			//This allows us to have different functionality for a zoom in/out map vs a scrolling list like the SVM page
			PASS_ANALOGUE_STICK_INPUTS_TO_SF(FALSE, TRUE)
		ELSE
			// NB: Using IS_CONTROL_PRESSED for continuous zooming out on map.
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_SCROLL_UP)))
			ENDIF
			// NB: Using IS_CONTROL_PRESSED for continuous zooming in on map.
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_SCROLL_DOWN)))
			ENDIF
		ENDIF
	ELSE
		PASS_ANALOGUE_STICK_INPUTS_TO_SF(TRUE, TRUE)
	ENDIF
	
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
	AND NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CPRINTLN(DEBUG_INTERNET,"<SECURO> CURRENT PAGE ID is:", iCurrentPageID, " EXITING WEBSITE for Triangle press")
		CLEANUP_SECUROSERVE()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		CLEANUP_SECUROSERVE()
	ENDIF
	#ENDIF
ENDPROC

PROC ADD_WAREHOUSE_MENU_HELP_KEY_INPUT(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iNewWarehouse)
	REMOVE_MENU_HELP_KEYS()
	IF GET_WAREHOUSE_ID_FOR_SAVE_SLOT(control.iCurrentVertSel) <= 0
	OR ARE_WAREHOUSES_SUITABLE_FOR_TRADE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(control.iCurrentVertSel), iNewWarehouse)
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
	ENDIF
	ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
	//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
ENDPROC

PROC SETUP_REPLACE_WAREHOUSE_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iNewWarehouse)
	INT iMenuCounter
	INT i
	INT iOwnedCount = GET_COUNT_OF_WAREHOUSES_OWNED()
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	
		REPEAT ciMaxOwnedWarehouses i
			iWHOrder[i] = iOwnedCount
		ENDREPEAT
	
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_WHOUSE_0")
		INT iWarehouse
		FOR i = (ciMaxOwnedWarehouses - 1) TO 0 STEP -1
			iWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(i)
			
			CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_WAREHOUSE_MENU: GET_OWNED_WAREHOUSE(",i,") = ",iWarehouse)
			IF iWarehouse > ciW_Invalid
				BOOL bIsSelectable = ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iWarehouse, iNewWarehouse)
				
				ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,bIsSelectable)
				// B* 2750666
				IF IS_LANGUAGE_NON_ROMANIC()
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_WAREHOUSE_NAME(iWarehouse)))
				ELSE
					ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_WAREHOUSE_NAME(iWarehouse)), FALSE, TRUE)
				ENDIF
				
				ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,bIsSelectable)
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_WAREHOUSE_VALUE_FOR_SALE(iWarehouse))
				
				control.iSlotIDS[i] 	= iWarehouse
				iWHOrder[iMenuCounter] 	= i
				iMenuCounter++
			ELSE
				IF i > ciW_Invalid
					IF GET_WAREHOUSE_ID_FOR_SAVE_SLOT(i-1) > 0
						ADD_MENU_ITEM_TEXT(iMenuCounter,"REP_WHOUSE_1",0)
						control.iSlotIDS[i] 	= iWarehouse
						iWHOrder[iMenuCounter] 	= i
						iMenuCounter++
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.icurrentVertSel)
		
		//Once
		ADD_WAREHOUSE_MENU_HELP_KEY_INPUT(control, iNewWarehouse)
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_WAREHOUSE_MENU: setup menu")
ENDPROC

PROC REPLACE_WAREHOUSE_WARNING_MESSAGE_WEB(INT iValue, BOOL bSmallerGarage)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
	IF bSmallerGarage
		pBodyTextLabel = ("BRDISWARE")				//"You already own a warehouse, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ELSE
		pBodyTextLabel = ("BRDISWAREB1")			//"You already own a warehouse, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ENDIF
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF
	
	CDEBUG1LN(DEBUG_INTERNET, "REPLACE_WAREHOUSE_WARNING_MESSAGE_WEB(pHeaderTextLabel:\"",
			pHeaderTextLabel, "\", pBodyTextLabel:\"",
			pBodyTextLabel, "\", pBodySubTextLabel:\"",
			pBodySubTextLabel, "\", NumberToInsert:\"",
			NumberToInsert, "\", pFirstSubStringTextLabel:\"",
			pFirstSubStringTextLabel, "\")")
	SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
			pBodyTextLabel,
			FE_WARNING_OKCANCEL,
			pBodySubTextLabel,
			TRUE,
			NumberToInsert,
			WARNING_MESSAGE_DEFAULT,
			pFirstSubStringTextLabel)
ENDPROC

PROC HANDLE_REPLACE_WAREHOUSE_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingWarehouseID)
	//item description
	BOOL bSmallerGarage
	INT iCurrentSelectedWHSlot		= iWHOrder[control.iCurrentVertSel]
	INT iOwnedWarehouseID 			= GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iCurrentSelectedWHSlot)
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		
		INT iTradeIn = GET_WAREHOUSE_VALUE_FOR_SALE(iOwnedWarehouseID)
		REPLACE_WAREHOUSE_WARNING_MESSAGE_WEB(iTradeIn, bSmallerGarage)
		
	ELSE
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELIF iOwnedWarehouseID > 0
		AND NOT ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iOwnedWarehouseID, iPurchasingWarehouseID)
			IF GET_WAREHOUSE_SIZE(iOwnedWarehouseID) > GET_WAREHOUSE_SIZE(iPurchasingWarehouseID)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_5a",1000)
			ELIF NOT IS_WAREHOUSE_EMPTY(iOwnedWarehouseID)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_5b",1000)
			ELIF iOwnedWarehouseID = iPurchasingWarehouseID
				SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_5c",1000)
			#IF FEATURE_DLC_1_2022
			ELIF IS_WAREHOUSE_PED_SOURCING_CARGO(iOwnedWarehouseID)	
				SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_5d",1000)
			#ENDIF
			ELSE
				CASSERTLN(DEBUG_INTERNET, "<SECURO> HANDLE_REPLACE_WAREHOUSE_MENU_DESCRIPTION: unknown reason why warehouse not suitable to trade ", iOwnedWarehouseID, " for ", iPurchasingWarehouseID, " in selection ", control.iCurrentVertSel, "!")
				SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_5",1000)
			ENDIF
		ELIF iOwnedWarehouseID > 0
		AND ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iOwnedWarehouseID, iPurchasingWarehouseID)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_2",100)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_WHOUSE_3",100)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL RUN_REPLACE_WAREHOUSE_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control,INT &iResultSlot,INT iPurchasingWarehouseID)
	
	bool g_bAbortWarehouseMenus = FALSE
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_PLAYER_IN_CORONA()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR g_bMissionEnding
	OR g_bAbortWarehouseMenus
	OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		IF g_bAbortWarehouseMenus
			CPRINTLN(DEBUG_INTERNET, "RUN_REPLACE_WAREHOUSE_MENU: aborting menu. g_bAbortWarehouseMenus = TRUE")
		ENDIF
		iResultSlot = -1
		CPRINTLN(DEBUG_INTERNET, "RUN_REPLACE_WAREHOUSE_MENU - NETWORK_IS_GAME_IN_PROGRESS = FALSE - returning slot -1")
		CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
		RETURN(TRUE)
	ENDIF
	INT propval = GET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(iPurchasingWarehouseID) //*g_sMPTunables.fPropertyMultiplier)

	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()

	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK2)
	
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		
		IF NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		ENDIF
		
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		SET_MOUSE_CURSOR_THIS_FRAME()		
	ENDIF
	
	SET_BIT(control.iBS,REP_VEH_PROP_BS_USING)
	IF LOAD_MENU_ASSETS()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_SETUP)
				SETUP_REPLACE_WAREHOUSE_MENU(control, iPurchasingWarehouseID)
				SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
				HANDLE_REPLACE_WAREHOUSE_MENU_DESCRIPTION(control, iPurchasingWarehouseID)
				IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					DRAW_MENU()
				ENDIF
			ELSE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT g_sTransitionSessionData.sEndReserve.bTranCleanUpEndReservingSlot
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						INT iCurrentSelectedWHSlot	= iWHOrder[control.iCurrentVertSel]
						INT iOwnedWarehouseID 		= GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iCurrentSelectedWHSlot)
						INT iOwnedWarehouseCost 	= 0
						
						IF iOwnedWarehouseID > ciW_Invalid
							iOwnedWarehouseCost = GET_WAREHOUSE_VALUE_FOR_SALE(iOwnedWarehouseID)
						ENDIF
						
						IF iOwnedWarehouseID <= ciW_Invalid
						OR ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iOwnedWarehouseID, iPurchasingWarehouseID)
							IF IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()
							OR (IS_MENU_CURSOR_ACCEPT_PRESSED() AND (g_iMenuCursorItem = control.iCurrentVertSel))
							OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
								PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Success", "GTAO_Exec_SecuroServ_Computer_Sounds")
								
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_INTERNET, "<SECURO> control.iCurrentVertSel ", control.iCurrentVertSel, " ownedWarehouse: ", iOwnedWarehouseID)
									CPRINTLN(DEBUG_INTERNET, "<SECURO> propVal = ",propval)
									
									IF iOwnedWarehouseID > 0
									AND ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iOwnedWarehouseID, iPurchasingWarehouseID)
										CPRINTLN(DEBUG_INTERNET, "<SECURO> trade in = ",iOwnedWarehouseCost)
									ENDIF
								#ENDIF
								
								INT iSpendValue = (propval - iOwnedWarehouseCost)
								
								IF iSpendValue <= 0
								OR NETWORK_CAN_SPEND_MONEY(iSpendValue, FALSE, TRUE, FALSE)
									IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
									AND iOwnedWarehouseID > ciW_Invalid
										CLEANUP_MENU_ASSETS()
										SET_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
										RETURN FALSE
									ELSE
										iResultSlot = iCurrentSelectedWHSlot
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
										CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
										RETURN TRUE
									ENDIF
								ELSE
									CPRINTLN(DEBUG_INTERNET, "<SECURO> NETWORK_CAN_SPEND_MONEY returned false")
									SET_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
								ENDIF
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
							ENDIF
						ELSE
							//not suitable for trade...
						ENDIF
					ELSE 
						IF NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						IF IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()
						OR (IS_MENU_CURSOR_CANCEL_PRESSED() AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
							PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
							IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								CPRINTLN(DEBUG_INTERNET, "RUN_REPLACE_WAREHOUSE_MENU player exited menu")
								CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
								iResultSlot = -1
								RETURN TRUE
							ELSE
								//REMOVE_MENU_HELP_KEYS()
								//ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "BB_SELECT")
								//ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "BB_BACK")
								//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
							ENDIF
							
							SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_MENU_CURSOR_SCROLL_UP_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								control.iCurrentVertSel--
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_UP)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								control.iCurrentVertSel++
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_DOWN)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
							ENDIF
						ENDIF
					ENDIF
					
					// handle clicking on a new highlighted option
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						IF (g_iMenuCursorItem != control.iCurrentVertSel)
							control.iCurrentVertSel = g_iMenuCursorItem
							SET_CURRENT_MENU_ITEM(control.icurrentVertSel)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
						ENDIF
					ENDIF
					
					IF control.iCurrentVertSel >= control.iMaxVertSel
						control.iCurrentVertSel = 0
					ENDIF
					IF Control.iCurrentVertSel < 0
						control.iCurrentVertSel = control.iMaxVertSel-1
					ENDIF
					//PRINTLN("control.iCurrentVertSel = ",control.iCurrentVertSel)
					HANDLE_REPLACE_WAREHOUSE_MENU_DESCRIPTION(control,iPurchasingWarehouseID)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						DRAW_MENU()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESSING_SECUROSERVE_WAREHOUSE_BASKET(INT &iProcessSuccess, INT iPrice, INT iStatValue, SHOP_ITEM_CATEGORIES eCategory, 
													ITEM_ACTION_TYPES eAction, INT iItemId, INT iInventoryKey, INT iSellingPrice = 0, 
													INT iSellingItemId = 0, INT iInteriorID = 0, INT iInteriorInventoryKey = 0, INT iInteriorPrice = 0)
	
	INT iTempPrice
	
	IF IS_BIT_SET(iBS, SEC_BS_PROCESSING_BASKET)
		SWITCH iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				// Override the players cash.
				iTempPrice = (iPrice - iSellingPrice)
				
				IF iTempPrice > 0
				AND NOT NETWORK_CAN_SPEND_MONEY(iTempPrice, FALSE, TRUE, FALSE)
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					CASSERTLN(DEBUG_INTERNET, "[BASKET] PROCESSING_SECUROSERVE_WAREHOUSE_BASKET - Player can't afford this item! iTempPrice= ", iTempPrice)
					RETURN TRUE
				ENDIF
				
				CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding warehouse to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iPrice:$", iPrice, ", iItemId:", iItemId, ", iInventoryKey:", iInventoryKey)
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
				
					
					IF iSellingItemId = HASH("WH_INDEX_0_t0_v0")
					OR iSellingItemId = HASH("IE_WH_INDEX_0_t0_v0")
						SCRIPT_ASSERT("PROCESSING_SECUROSERVE_WAREHOUSE_BASKET - You are trading in an invalid warehouse!")
					ENDIF
				
					// Warehouse we are trading in.
					IF iSellingItemId != 0
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding trade warehouse to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iSellingPrice:$", iSellingPrice)
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iSellingItemId, eAction, 1, iSellingPrice, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
							//
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Failed to add selling item to basket")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF iInteriorID != 0
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding trade warehouse to basket eCategory: CATEGORY_INVENTORY_WAREHOUSE_INTERIOR, eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iSellingPrice:$", iSellingPrice)
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_WAREHOUSE_INTERIOR, iInteriorID, eAction, 1, iInteriorPrice, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInteriorInventoryKey)
							//
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Failed to add interior item to basket")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket checkout started")							
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to start basket checkout")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to add item to basket")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, success!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, failed!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS
				CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD				
				USE_FAKE_MP_CASH(FALSE)
				
				iProcessSuccess = 2
				RETURN FALSE
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 0
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF

	IF iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		CPRINTLN(DEBUG_INTERNET, "[BASKET] - was FORCE_CLOSE_BROWSER called to termiate the transaction? Set success to 0")
		
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		iProcessSuccess = 0
		RETURN FALSE
	ENDIF

	iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	iProcessSuccess = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL BUY_MP_WAREHOUSE_FROM_SECUROSERV(INT iWarehouseID, INT &iResultSlot)
	REPLACE_MP_VEH_OR_PROP_MENU replaceMenu
	iResultSlot = -1	//, i
	
	IF g_sMPTunables.bexec_disable_warehouse_purchase
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: purchase failed, g_sMPTunables.bexec_disable_warehouse_purchase is true")
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		RETURN FALSE
	ENDIF
	
	IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouseID)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: purchase invalid, we already own warehouse: ", iWarehouseID)
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_15 tl_15WarehouseName = GET_WAREHOUSE_NAME(iWarehouseID)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: trying to purchase warehouse #", iWarehouseID, " \"", tl_15WarehouseName, "\"")
	
	INT iMaxTradeIn = 0
	INT propval = CEIL(TO_FLOAT(GET_WAREHOUSE_COST(iWarehouseID))*g_sMPTunables.fPropertyMultiplier)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: purchase valid, attempting index #", iWarehouseID)
	INT minPendingTransactionValue
	INT finalPendingTransactionValue
	minPendingTransactionValue = propval - iMaxTradeIn
	
	INT iOldWarehouse = 0
	
	//is it a debit or refund
	IF minPendingTransactionValue >0
		//do they have enough money
		IF NOT NETWORK_CAN_SPEND_MONEY(minPendingTransactionValue, FALSE, TRUE, FALSE)		
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: purchase failed, player doesn't have sufficient cash NETWORK_CAN_SPEND_MONEY returned false")
			TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15WarehouseName), minPendingTransactionValue)
			
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
			SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
			RETURN FALSE
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: min pending- $", minPendingTransactionValue, "  MAX trade val- $",iMaxTradeIn)
	
	IF DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE()
		WHILE NOT RUN_REPLACE_WAREHOUSE_MENU(replaceMenu,iResultSlot,iWarehouseID)
			DISABLE_SELECTOR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			WAIT(0)
			g_sMenuData.bKeepPhoneForNextDrawMenuCall = TRUE
		ENDWHILE
		
		DISABLE_SELECTOR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		WAIT(0)
		g_sMenuData.bKeepPhoneForNextDrawMenuCall = TRUE
	ELSE
		iResultSlot = 0
	ENDIF
	
	INT iResultSlotCost = 0
	IF iResultSlot = -1
	//	g_iPendingBrowserTimer = g_iBrowserTimer
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		CPRINTLN(DEBUG_INTERNET, "<SECURO> Player backed out of warehouse selection")
		RETURN FALSE
	ELSE
		
		iOldWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iResultSlot)
		IF (iOldWarehouse <= ciW_Invalid)
		OR (iOldWarehouse > ciMaxWarehouses)
			iResultSlotCost = ciW_Invalid
		ELSE
			iResultSlotCost = GET_WAREHOUSE_VALUE_FOR_SALE(iOldWarehouse)
		ENDIF
		
		finalPendingTransactionValue = propval - iResultSlotCost
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TRADE> Player selected WH: ", iOldWarehouse, " for trade. price before trade: ", propval, " Old WH trade val: ", iResultSlotCost, " Final trans value: ", finalPendingTransactionValue)
	ENDIF
	
	// Re-check if player can afford based on selected trade-in value
	IF finalPendingTransactionValue > 0
		//do they have enough money		
		IF NOT NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue, FALSE, TRUE, FALSE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: purchase failed, player failed on command NETWORK_CAN_SPEND_MONEY")
			TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15WarehouseName), finalPendingTransactionValue)
			
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
			SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		SET_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_WAREHOUSE_KEY_FOR_CATALOGUE(iWarehouseID)
		INT iInventoryKey = GET_WAREHOUSE_INVENTORY_KEY_FOR_CATALOGUE(iResultSlot)
		INT iSellingItemId = 0
		IF GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iResultSlot) != ciW_Invalid
			iSellingItemId = GET_WAREHOUSE_KEY_FOR_CATALOGUE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iResultSlot))
		ENDIF
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedWarehouseIndex = tl_15WarehouseName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedWarehouseIndex)
			CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: label for last clicked warehouse index #", iWarehouseID, " is empty!")
		ELSE
			//CDEBUG1LN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: label for last clicked warehouse index #", iWarehouseID, " is \"", tlLastClickedWarehouseIndex, "\" (iReplaceWarehouse: ", iReplaceWarehouse, ")")
		ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		///////////////////////////////////////////
		///   TRANSACTION FOR WAREHOUSE
		WHILE PROCESSING_SECUROSERVE_WAREHOUSE_BASKET(iProcessSuccess, propval, iResultSlot, CATEGORY_INVENTORY_WAREHOUSE, NET_SHOP_ACTION_BUY_WAREHOUSE, iItemId, iInventoryKey, iResultSlotCost, iSellingItemId)
			RENDER_SECUROSERVE()
			WAIT(0)
		ENDWHILE
		RENDER_SECUROSERVE()
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: success!!")
			BREAK
			DEFAULT
				CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: final:$", finalPendingTransactionValue)
	
	SPEND_OFFICE_AND_WAREHOUSE data
	
	data.m_Location				= iWarehouseID
	data.m_LocationAmount		= finalPendingTransactionValue
	data.m_SmallWarehouses		= GET_COUNT_OF_WAREHOUSES_OWNED_BY_SIZE(eWarehouseSmall, PLAYER_ID())
	data.m_MediumWarehouses		= GET_COUNT_OF_WAREHOUSES_OWNED_BY_SIZE(eWarehouseMedium, PLAYER_ID())
	data.m_LargeWarehouses		= GET_COUNT_OF_WAREHOUSES_OWNED_BY_SIZE(eWarehouseLarge, PLAYER_ID())
	data.m_WarehouseSize 		= ENUM_TO_INT(GET_WAREHOUSE_SIZE(iWarehouseID))
	
	SET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(iResultSlot, PropVal)
	
	IF finalPendingTransactionValue >= 0
	AND (finalPendingTransactionValue = 0 
	OR (finalPendingTransactionValue > 0 AND (USE_SERVER_TRANSACTIONS() OR NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue,FALSE,TRUE,FALSE))))
		IF finalPendingTransactionValue > 0
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, finalPendingTransactionValue)
		ENDIF
		iOldWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iResultSlot)
		IF (iOldWarehouse <= 0)
		OR (iOldWarehouse > ciMaxWarehouses)
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			NETWORK_SPENT_PURCHASE_WAREHOUSE_PROPERTY(propval, data, FALSE, TRUE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_WAREHOUSE_FROM_SECUROSERV: Bought warehouse ", tl_15WarehouseName, " for $", finalPendingTransactionValue)
		ELIF finalPendingTransactionValue > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			CDEBUG1LN(DEBUG_SAFEHOUSE, ",SECURO_TRADE> Trading with cost: ", finalPendingTransactionValue, " Sending telemtry cost as: ", data.m_LocationAmount)
			NETWORK_SPENT_UPGRADE_WAREHOUSE_PROPERTY(finalPendingTransactionValue, data, FALSE, TRUE)
		ENDIF
		
		IF USE_SERVER_TRANSACTIONS()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV(IMPORT_EXPORT_GARAGES eIEWarehouseID)
	
	INT iOwnedIEWarehouseValue
	IMPORT_EXPORT_GARAGES eOwnedIEWH = GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())
	
	INT iInteriorItemCost 		= GET_IE_GARAGE_INTERIOR_COST(eSelectedInterior, FALSE)
	INT	iInteriorInventoryKey 	= HASH("MP_STAT_OWNED_IE_WAREHOUSE_VAR_v0")
	INT	iInteriorItemHash		= GET_IE_GARAGE_INTERIOR_KEY_FOR_CATALOGUE(eSelectedInterior, FALSE)
	
	IF iInteriorItemCost = 0
		//The basic interior is free when first purchasing a WH
		//Setting the item hash to 0 stops it from being added to the transaction basket
		iInteriorInventoryKey = 0
		iInteriorItemHash = 0
	ENDIF
	
	IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), eIEWarehouseID)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: purchase invalid, we already own warehouse: ", eIEWarehouseID)
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_15 tl_15WarehouseName = GET_IE_GARAGE_NAME_LABEL(eIEWarehouseID)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: trying to purchase warehouse #", eIEWarehouseID, " \"", tl_15WarehouseName, "\"")
	
	INT iMaxTradeIn = 0
	INT propval = CEIL(TO_FLOAT(GET_IE_GARAGE_COST(eIEWarehouseID))*g_sMPTunables.fPropertyMultiplier)
	
	propval += iInteriorItemCost
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: purchase valid, attempting index #", eIEWarehouseID)
	INT minPendingTransactionValue
	INT finalPendingTransactionValue
	minPendingTransactionValue = propval - iMaxTradeIn
	
	//is it a debit or refund
	IF eOwnedIEWH = IE_GARAGE_INVALID
		IF minPendingTransactionValue > 0
			//do they have enough money			
			IF NOT NETWORK_CAN_SPEND_MONEY(minPendingTransactionValue,FALSE,TRUE,FALSE)
				CPRINTLN(DEBUG_INTERNET, "<SECURO>BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: purchase failed, player doesn't have sufficient cash")
				TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15WarehouseName), minPendingTransactionValue)
				
				SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				RETURN FALSE
			ENDIF
		ENDIF
		
		finalPendingTransactionValue = propval
	ELSE
		iOwnedIEWarehouseValue = GET_IE_GARAGE_TRADE_PRICE(eOwnedIEWH)
		
		finalPendingTransactionValue = propval - iOwnedIEWarehouseValue
		CPRINTLN(DEBUG_INTERNET, "<SECURO><SECURO_TRADE> Player selected factory: ", eOwnedIEWH, " for trade. price before trade: ", propval, " Old Factory trade val: ", iOwnedIEWarehouseValue, " Final trans value: ", finalPendingTransactionValue)
	
		// Re-check if player can afford based on selected trade-in value
		IF finalPendingTransactionValue > 0
			IF NOT NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue, FALSE, TRUE, FALSE)
				CPRINTLN(DEBUG_INTERNET, "<SECURO>BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: purchase failed, player failed on command NETWORK_CAN_SPEND_MONEY")
				TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15WarehouseName), minPendingTransactionValue)
					
				SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		SET_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_IE_WAREHOUSE_KEY_FOR_CATALOGUE(eIEWarehouseID)
		INT iInventoryKey = GET_IE_WAREHOUSE_INVENTORY_KEY_FOR_CATALOGUE()
		INT iSellingItemId = 0
		IF eOwnedIEWH != IE_GARAGE_INVALID
			iSellingItemId = GET_IE_WAREHOUSE_KEY_FOR_CATALOGUE(eOwnedIEWH)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedWarehouseIndex = tl_15WarehouseName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedWarehouseIndex)
			CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: label for last clicked warehouse index #", eIEWarehouseID, " is empty!")
		ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		///////////////////////////////////////////
		///   TRANSACTION FOR WAREHOUSE
		WHILE PROCESSING_SECUROSERVE_WAREHOUSE_BASKET(iProcessSuccess, (propval - iInteriorItemCost), 0, CATEGORY_INVENTORY_WAREHOUSE, NET_SHOP_ACTION_BUY_WAREHOUSE, iItemId, iInventoryKey, iOwnedIEWarehouseValue, iSellingItemId, iInteriorItemHash, iInteriorInventoryKey, iInteriorItemCost)
			RENDER_SECUROSERVE()
			WAIT(0)
		ENDWHILE
		RENDER_SECUROSERVE()
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: success!!")
			BREAK
			DEFAULT
				CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: final:$", finalPendingTransactionValue)
	
	SPEND_OFFICE_AND_WAREHOUSE data
	SPEND_BUSINESS_PROPERTY data2
	
	data.m_Location				= GET_HASH_KEY(GET_IE_GARAGE_NAME_LABEL(eIEWarehouseID))
	data.m_LocationAmount		= finalPendingTransactionValue
	data.m_Style				= GET_HASH_KEY(GET_IE_GARAGE_INTERIOR_STRING(eSelectedInterior))
	data.m_StyleAmount			= iInteriorItemCost
	data2.m_businessID 			= data.m_Location
	data2.m_businessUpgradeType = data.m_Style
	
	IF data.m_StyleAmount > 0
	AND (eOwnedIEWH = IE_GARAGE_INVALID OR eOwnedIEWH = IE_GARAGE_UPPER_LIMIT)
		//This makes sure the total price in the telemetry data matches the price charged
		//This is only necessary when using NETWORK_SPENT_PURCHASE_IMPEXP_WAREHOUSE_PROPERTY
		data.m_LocationAmount -= data.m_StyleAmount
	ENDIF
	
	SET_IE_GARAGE_PAID_FOR_PURCHASE_PRICE(propval - iInteriorItemCost)
	
	//Check if we're trading of just buying
	IF eOwnedIEWH = IE_GARAGE_INVALID
	OR eOwnedIEWH = IE_GARAGE_UPPER_LIMIT
		IF USE_SERVER_TRANSACTIONS()
			CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
			NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
		ENDIF
		
		NETWORK_SPENT_PURCHASE_IMPEXP_WAREHOUSE_PROPERTY(propval, data, FALSE, TRUE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: Bought vehicle warehouse ", tl_15WarehouseName, " for $", finalPendingTransactionValue)
	ELSE
		IF iOwnedIEWarehouseValue > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			CPRINTLN(DEBUG_INTERNET, "<SECURO><SECURO_TRADE> BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV: Trading the vehicle warehouse is earning the player: ", iOwnedIEWarehouseValue)
			NETWORK_EARN_FROM_PROPERTY(iOwnedIEWarehouseValue, GET_HASH_KEY(GET_IE_GARAGE_NAME_LABEL(eOwnedIEWH)))
		ENDIF
		
		IF propval > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOPS, "<SECURO><SECURO_TRADE> Trading with cost: ", finalPendingTransactionValue, " Sending telemtry cost as: ", propval)
			NETWORK_SPENT_TRADE_IMPEXP_WAREHOUSE_PROPERTY(propval, data2, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESSING_SECUROSERVE_WAREHOUSE_INTERIOR_BASKET(INT &iProcessSuccess, INT iPrice, INT iStatValue, SHOP_ITEM_CATEGORIES eCategory, 
																ITEM_ACTION_TYPES eAction, INT iItemId, INT iInventoryKey)
	IF IS_BIT_SET(iBS, SEC_BS_PROCESSING_BASKET)
		SWITCH iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				
				IF iPrice > 0
				AND NOT NETWORK_CAN_SPEND_MONEY(iPrice, FALSE, TRUE, FALSE)
					CASSERTLN(DEBUG_INTERNET, "[BASKET] PROCESSING_SECUROSERVE_WAREHOUSE_INTERIOR_BASKET - Player can't afford this item! iPrice= $", iPrice)
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					RETURN TRUE
				ENDIF
				
				CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding warehouse interior to basket eCategory: CATEGORY_INVENTORY_WAREHOUSE_INTERIOR, eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction))
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
					IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket checkout started")							
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to start basket checkout")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[BASKET] - Failed to add interior item to basket")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					RETURN TRUE
				ENDIF
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, success!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, failed!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS
				CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 2
				RETURN FALSE
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 0
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF

	IF iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		CPRINTLN(DEBUG_INTERNET, "[BASKET] - was FORCE_CLOSE_BROWSER called to termiate the transaction? Set success to 0")
		
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		iProcessSuccess = 0
		RETURN FALSE
	ENDIF

	iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	iProcessSuccess = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL BUY_MP_IE_WAREHOUSE_INTERIOR(IMPORT_EXPORT_GARAGES eIEWarehouseID, IE_GARAGE_INTERIORS eInterior)
	IMPORT_EXPORT_GARAGES eOwnedIEWH = GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())
	
	INT iInteriorItemCost 		= GET_IE_GARAGE_INTERIOR_COST(eInterior, TRUE)
	INT	iInteriorInventoryKey 	= HASH("MP_STAT_OWNED_IE_WAREHOUSE_VAR_v0")
	INT	iInteriorItemHash		= GET_IE_GARAGE_INTERIOR_KEY_FOR_CATALOGUE(eInterior, TRUE)
	
	IF eIEWarehouseID != eOwnedIEWH
		CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR attempting interior purchase for an IE warehouse we don't own")
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		RETURN FALSE
	ENDIF
	
	IF iInteriorItemCost > 0 
	AND NOT NETWORK_CAN_SPEND_MONEY(iInteriorItemCost, FALSE, TRUE, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO>BUY_MP_IE_WAREHOUSE_INTERIOR: purchase failed, player doesn't have sufficient cash. Interior cost:  ", iInteriorItemCost)
		TRIGGER_BROWSER_NAG_SCREEN(iInteriorItemHash, iInteriorItemCost)
			
		SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
		RETURN FALSE
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		SET_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		iProcessingBasketStage 	= SHOP_BASKET_STAGE_ADD
		
		INT iProcessSuccess = -1
		///////////////////////////////////////////
		///   TRANSACTION FOR WAREHOUSE INTERIOR
		WHILE PROCESSING_SECUROSERVE_WAREHOUSE_INTERIOR_BASKET(iProcessSuccess, iInteriorItemCost, 1, CATEGORY_INVENTORY_WAREHOUSE_INTERIOR, NET_SHOP_ACTION_BUY_WAREHOUSE, iInteriorItemHash, iInteriorInventoryKey)
			RENDER_SECUROSERVE()
			WAIT(0)
		ENDWHILE
		RENDER_SECUROSERVE()
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR: success!!")
			BREAK
			DEFAULT
				CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	IF iInteriorItemCost > 0
		SPEND_OFFICE_AND_WAREHOUSE data
		
		data.m_Location 		= GET_HASH_KEY(GET_IE_GARAGE_NAME_LABEL(eIEWarehouseID))
		data.m_LocationAmount 	= 0
		data.m_Style			= GET_HASH_KEY(GET_IE_GARAGE_INTERIOR_STRING(eSelectedInterior))
		data.m_StyleAmount	 	= iInteriorItemCost
		
		IF USE_SERVER_TRANSACTIONS()
			CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
			NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
		ENDIF
		
		PRINTLN("<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR - item cost: ",iInteriorItemCost)
		PRINTLN("<SECURO> BUY_MP_IE_WAREHOUSE_INTERIOR - Call NETWORK_SPENT_UPGRADE_IMPEXP_WAREHOUSE_PROPERTY")
		NETWORK_SPENT_UPGRADE_IMPEXP_WAREHOUSE_PROPERTY(iInteriorItemCost, data, FALSE, TRUE)
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BUY_MP_CONTRABAND_FROM_SECUROSERV(INT iWarehouseID, CONTRABAND_SIZE ShipmentSize)
	INT iResultSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouseID)
	
	TEXT_LABEL_15 tl_15ContrabandName = GET_WAREHOUSE_NAME(iWarehouseID)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: trying to purchase CONTRABAND #", iWarehouseID, " \"", tl_15ContrabandName, "\"")
	
	BOOL bSpeialItemSelected = FALSE
	INT iMaxTradeIn = 0
	INT propval = GET_CONTRABAND_SHIPMENT_COST(ShipmentSize)
	
	IF IS_BIT_SET(iBS2, SEC_BS2_SPECIAL_ITEM)
	AND ShipmentSize = CONTRABAND_SMALL
		propval = GET_SPECIAL_CONTRABAND_ITEM_COST(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(securoContraType))
		bSpeialItemSelected = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase valid, attempting index #", iWarehouseID)
	INT minPendingTransactionValue
	INT finalPendingTransactionValue
	minPendingTransactionValue = propval - iMaxTradeIn
	
	//is it a debit or refund
	IF minPendingTransactionValue > 0
		//do they have enough money	
		IF NOT NETWORK_CAN_SPEND_MONEY(minPendingTransactionValue,FALSE,TRUE,FALSE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, player doesn't have sufficient cash")
			TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15ContrabandName), minPendingTransactionValue)
			
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
			SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
			RETURN FALSE
		ENDIF
	ENDIF

	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: min pending- $", minPendingTransactionValue, "  MAX trade val- $",iMaxTradeIn)
	
	finalPendingTransactionValue = propval //- GET_VALUE_OF_CURRENTLY_OWNED_CONTRABAND(iResultSlot)
	
	// Re-check if player can afford based on selected trade-in value
	IF finalPendingTransactionValue > 0		
		IF NOT NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue, FALSE, TRUE, FALSE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, player failed on command NETWORK_CAN_SPEND_MONEY")
			TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15ContrabandName), finalPendingTransactionValue)
			
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
			SET_BIT(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_BUY, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, mission not available")
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
		iMissionUnavailableReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_BUY, FALSE)
		RETURN FALSE
	ENDIF
	
	// Check we're not taking part in something
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, player critical to job")
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
		iMissionUnavailableReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_BUY, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT GB_HAS_BUY_MISSION_TIMER_EXPIRED()
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, GB_HAS_BUY_MISSION_TIMER_EXPIRED is false")
		iMissionUnavailableReason = GB_MU_REASON_ON_COOLDOWN
		RETURN FALSE
	ENDIF
	
	IF ARE_SECUROSERV_BUY_MISSIONS_DISABLED()
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, ARE_BUY_MISSIONS_DISABLED is true")
		iMissionUnavailableReason = GB_MU_REASON_UNSUITABLE_SESSION
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, NETWORK_IS_ACTIVITY_SESSION is true")
		RETURN FALSE
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
	
		/*
		BuyContrabandMission – 
                This is used to fill/set the CATEGORY_INVENTORY_CONTRABAND_MISSION
                It deducts money for the ‘purchasing’ of the mission
                Basket will be < CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION, price>
                Validation must make sure the player owns the warehouse this CATEGORY_INVENTORY_CONTRABAND_MISSION is associated with

		*/
	
		SET_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(TRUE, ENUM_TO_INT(ShipmentSize), bSpeialItemSelected, ENUM_TO_INT(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(securoContraType)))
		INT iInventoryKey = GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(iResultSlot)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedCONTRABANDIndex = tl_15ContrabandName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedCONTRABANDIndex)
			CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: label for last clicked CONTRABAND index #", iWarehouseID, " is empty!")
		ELSE
			//CDEBUG1LN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: label for last clicked CONTRABAND index #", iWarehouseID, " is \"", tlLastClickedCONTRABANDIndex, "\" (iReplaceCONTRABAND: ", iReplaceCONTRABAND, ")")
		ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		BOOL bProcessingBasket = IS_BIT_SET(iBS, SEC_BS_PROCESSING_BASKET)
		///////////////////////////////////////////
		///   TRANSACTION FOR CONTRABAND
		WHILE PROCESSING_SECUROSERVE_CONTRABAND_BASKET(iProcessSuccess, propval, iResultSlot, CATEGORY_INVENTORY_CONTRABAND_MISSION, NET_SHOP_ACTION_BUY_CONTRABAND_MISSION, iItemId, iInventoryKey, bProcessingBasket, iProcessingBasketStage)
			RENDER_SECUROSERVE()
			WAIT(0)
		ENDWHILE
		
		IF NOT bProcessingBasket
			CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		ENDIF
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: success!!")
				
				// Track this so we can add contraband if the mision was successful.
				SET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY(iItemId)
			BREAK
			DEFAULT
				CASSERTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: final:$", finalPendingTransactionValue)
	
	IF finalPendingTransactionValue >= 0
	AND (finalPendingTransactionValue = 0 
	OR (finalPendingTransactionValue > 0 AND (USE_SERVER_TRANSACTIONS() OR NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue,FALSE,TRUE,FALSE))))
		IF finalPendingTransactionValue > 0
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, finalPendingTransactionValue)
		ENDIF
	/*	IF GET_VALUE_OF_CURRENTLY_OWNED_CONTRABAND(iResultSlot) > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			NETWORK_EARN_FROM_CONTRABAND(GET_VALUE_OF_CURRENTLY_OWNED_CONTRABAND(iResultSlot), GET_HASH_KEY(tl_15ContrabandName))
		ENDIF */
		
		IF USE_SERVER_TRANSACTIONS()
			CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
			NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
		ENDIF
		
		NETWORK_BUY_CONTRABAND_MISSION(propval, iWarehouseID, GET_HASH_KEY(tl_15ContrabandName), FALSE, TRUE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: Bought contraband ", tl_15ContrabandName, " for $", finalPendingTransactionValue)
		
		IF USE_SERVER_TRANSACTIONS()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		ENDIF
	ELSE 
		IF finalPendingTransactionValue < 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			NETWORK_BUY_PROPERTY(propval,GET_HASH_KEY(tl_15ContrabandName),FALSE,TRUE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> BUY_MP_CONTRABAND_FROM_SECUROSERV: Earning from proprety: $", -finalPendingTransactionValue)
			
			IF USE_SERVER_TRANSACTIONS()
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			ENDIF
		ELSE	
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
			RETURN FALSE
		ENDIF
	ENDIF
	 
	RETURN TRUE
ENDFUNC

FUNC BOOL SELL_MP_CONTRABAND_FROM_SECUROSERV(INT iWarehouseID)
	
	INT iResultSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouseID)
	TEXT_LABEL_15 tl_15ContrabandName = GET_WAREHOUSE_NAME(iWarehouseID)
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: trying to sell contraband for warehouse #", iWarehouseID, " \"", tl_15ContrabandName, "\", in slot ", iResultSlot)
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_SELL, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, mission not available")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: purchase failed, player is critical")
		RETURN FALSE
	ENDIF
	
	IF IS_WAREHOUSE_EMPTY(iWarehouseID)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: warehouse is empty")
		RETURN FALSE
	ENDIF
	
	IF NOT GB_HAS_SELL_MISSION_TIMER_EXPIRED()
			CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: GB_HAS_SELL_MISSION_TIMER_EXPIRED false")
		RETURN FALSE
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
	
		/*
		BuyContrabandMission – 
                This is used to fill/set the CATEGORY_INVENTORY_CONTRABAND_MISSION
                It deducts money for the ‘purchasing’ of the mission
                Basket will be < CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION, price>
                Validation must make sure the player owns the warehouse this CATEGORY_INVENTORY_CONTRABAND_MISSION is associated with

		*/
	
		SET_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(FALSE, -1)
		INT iInventoryKey = GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(iResultSlot)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedCONTRABANDIndex = tl_15ContrabandName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedCONTRABANDIndex)
			CASSERTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: label for last clicked CONTRABAND index #", iWarehouseID, " is empty!")
		ELSE
			//CDEBUG1LN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: label for last clicked CONTRABAND index #", iWarehouseID, " is \"", tlLastClickedCONTRABANDIndex, "\" (iReplaceCONTRABAND: ", iReplaceCONTRABAND, ")")
		ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		BOOL bProcessingBasket = IS_BIT_SET(iBS, SEC_BS_PROCESSING_BASKET)
		///////////////////////////////////////////
		///   TRANSACTION FOR CONTRABAND
		WHILE PROCESSING_SECUROSERVE_CONTRABAND_BASKET(iProcessSuccess, 0, iResultSlot, CATEGORY_INVENTORY_CONTRABAND_MISSION, NET_SHOP_ACTION_BUY_CONTRABAND_MISSION, iItemId, iInventoryKey, bProcessingBasket, iProcessingBasketStage)
			RENDER_SECUROSERVE()
			WAIT(0)
		ENDWHILE
		
		IF NOT bProcessingBasket
			CLEAR_BIT(iBS, SEC_BS_PROCESSING_BASKET)
		ENDIF
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: success!!")
			BREAK
			DEFAULT
				CASSERTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
		NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
	ENDIF
	
	NETWORK_BUY_CONTRABAND_MISSION(0, iWarehouseID, GET_HASH_KEY(tl_15ContrabandName), FALSE, TRUE)
	CPRINTLN(DEBUG_INTERNET, "<SECURO> SELL_MP_CONTRABAND_FROM_SECUROSERV: Bought contraband ", tl_15ContrabandName)
	
	IF USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL LAUNCH_MP_VEH_STEAL_FROM_SECUROSERV()
	CPRINTLN(DEBUG_INTERNET, "<SECURO> LAUNCH_MP_VEH_STEAL_FROM_SECUROSERV: trying to steal vehicle")
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_VEHICLE_EXPORT_BUY , FALSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> LAUNCH_MP_VEH_STEAL_FROM_SECUROSERV: purchase failed, mission not available")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<SECURO> LAUNCH_MP_VEH_STEAL_FROM_SECUROSERV: purchase failed, player is critical")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns a string to give mission unavailable feedback
/// PARAMS:
///    bBuyMission - Is this a buy or sell mission
FUNC STRING GET_MISSION_UNAVAILABLE_REASON_STRING(BOOL bBuyMission, INT iWarehouseID)
	CDEBUG1LN(DEBUG_INTERNET, "Returning mission unavailable reason for: ", iMissionUnavailableReason, " buy mission? ", bBuyMission)
	
	IF NOT GB_HAS_SELL_MISSION_TIMER_EXPIRED()
	OR NOT GB_HAS_BUY_MISSION_TIMER_EXPIRED()
		RETURN "WH_MFREASON_9"
	ENDIF
	
	SWITCH iMissionUnavailableReason
		CASE GB_MU_REASON_ONLY_GANG_PLAYERS			RETURN "WH_MFREASON_0"
		CASE GB_MU_REASON_SESSION_MISSION_ACTIVE	RETURN "WH_MFREASON_1"
		CASE GB_MU_REASON_NO_OTHER_BOSSES			RETURN "WH_MFREASON_2"
		CASE GB_MU_REASON_NOT_ENOUGH_GOONS			RETURN "WH_MFREASON_3"
		CASE GB_MU_REASON_TOO_MANY_INSTANCES		RETURN "WH_MFREASON_4"
		CASE GB_MU_REASON_BOSS_CRITICAL_TO_FME		RETURN "WH_MFREASON_5"
		CASE GB_MU_REASON_GOON_CRITICAL_TO_FME		RETURN "WH_MFREASON_6"
		CASE GB_MU_REASON_GOON_FM_EVENT_ACTIVE		RETURN "WH_MFREASON_6"
		CASE GB_MU_REASON_INVITE_SENT				RETURN "WH_MFREASON_8"
		CASE GB_MU_REASON_BOTH_SENT_BVB_INVITE		RETURN "WH_MFREASON_8"
		CASE GB_MU_REASON_ON_COOLDOWN				RETURN "WH_MFREASON_9"
		CASE GB_MU_REASON_UNSUITABLE_SESSION		RETURN "WH_MFREASON_12"
		CASE GB_MU_REASON_GOON_GAMBLING				RETURN "GENERAL_MLF_G5"
		CASE GB_MU_REASON_GOON_IS_ANIMAL			RETURN "GENERAL_MLF_G2"
	ENDSWITCH
	
	//Critical to job so cant launch a mission
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	OR NETWORK_IS_ACTIVITY_SESSION()
		RETURN "WH_MFREASON_10"
	ENDIF
	
	IF bBuyMission
		RETURN "WHOUSE_PURCHF"
	ENDIF
	
	IF IS_WAREHOUSE_EMPTY(iWarehouseID)
		RETURN "MP_WH_SELL_FE"
	ENDIF
	
	RETURN "MP_WH_SELL_F"
ENDFUNC

PROC RUN_INITIAL_CONTRABAND_SELL_CHECKS(INT iWarehouse)
	IF ARE_SELL_MISSIONS_DISABLED()
		START_OVERLAY_SCREEN("MP_WH_SELL", "WH_MFREASON_12", "WHOUSE_CONF", "", FALSE)					//FAIL: Tuneables have disabled sell missions
		iCratesToSell 						= 0
		iContrabandBuyerID 					= 0
		SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
	ELIF IS_WAREHOUSE_EMPTY(iWarehouse)																	//FAIL: Empty WH
		START_OVERLAY_SCREEN("MP_WH_SELL", "MP_WH_SELL_FE", "WHOUSE_CONF", "", FALSE)
		iCratesToSell 						= 0
		iContrabandBuyerID 					= 0
		SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
	ELIF IS_CONTRABAND_SELL_COOLDOWN_ACTIVE(iWarehouse)													//FAIL: Sell cooldown active
	OR NOT GB_HAS_SELL_MISSION_TIMER_EXPIRED()
		START_OVERLAY_SCREEN("CONTRA_SALEF", "WH_MFREASON_9", "WHOUSE_CONF", "", FALSE)
		iCratesToSell 						= 0
		iContrabandBuyerID 					= 0
		SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
	ELIF NETWORK_IS_ACTIVITY_SESSION()																	//FAIL: Not in freemode
		START_OVERLAY_SCREEN("CONTRA_SALEF", "WH_MFREASON_10", "WHOUSE_CONF", "", FALSE)
		iCratesToSell 						= 0
		iContrabandBuyerID 					= 0
		SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
	ELIF iCratesToSell >= g_sMPTunables.iexec_sell_3rd_brickade_threshold								//PASS: Mission may require 3 vehicles
		SET_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
		START_OVERLAY_SCREEN("MP_WH_SALER", "MP_WH_SELL_W2", "WHOUSE_CONT", "WHOUSE_CANC", TRUE)
	ELIF iCratesToSell >= g_sMPTunables.iexec_sell_2nd_brickade_threshold								//PASS: Mission may require 2 vehicles
	OR iCratesToSell >=  g_sMPTunables.iexec_sell_2nd_cuban_threshold
		SET_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
		START_OVERLAY_SCREEN("MP_WH_SALER", "MP_WH_SELL_W", "WHOUSE_CONT", "WHOUSE_CANC", TRUE)
	ELIF iContrabandBuyerID = ciContraSellMissionBuyer_4												//PASS: Special item confirm
		SET_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
		
		STRING sOverlayTitle	= "MP_WH_SELLS"		//Plural
		STRING sOverlayDesc		= "MP_WH_SELL_D"
		
		IF iCratesToSell = 1
			sOverlayTitle		= "MP_WH_SELLSS"	//Single crate
			sOverlayDesc		= "MP_WH_SELL_DS"
		ENDIF
		
		START_OVERLAY_SCREEN(sOverlayTitle, sOverlayDesc, "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
	ELSE																								//PASS: Start mission confirm
		SET_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
		#IF FEATURE_DLC_1_2022
		IF DID_I_JOIN_A_PRIVATE_SESSION()
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_APP_SELL_GOODS_HELP_TEXT_EXEC_CARGO)
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("APP_SG_HELP")
					PRINT_HELP("APP_SG_HELP")
				ENDIF
				SET_PACKED_STAT_BOOL (PACKED_MP_BOOL_APP_SELL_GOODS_HELP_TEXT_EXEC_CARGO, TRUE)
			ENDIF
		ENDIF
		#ENDIF
		START_OVERLAY_SCREEN("MP_WH_SELL", "MP_WH_SELL_C", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
	ENDIF
	
	currentButtonPressStage 	= inputConsumed_0		
	iMissionUnavailableReason 	= -1
ENDPROC

/// PURPOSE:
///    Prints a ticker message each time the player purchases a new warehouse of a size they haven't previously purchased
PROC RUN_CHECK_FOR_NEW_ITEM_TICKER(INT iWarehouse)
	EWarehouseSize eWHSize = GET_WAREHOUSE_SIZE(iWarehouse)
	STRING stickerString = ""
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_CHECK_FOR_NEW_ITEM_TICKER: Checking for WH: ", iWarehouse, " Do we own one of this size: ", DOES_LOCAL_PLAYER_OWN_WAREHOUSE_OF_SIZE(eWHSize, iWarehouse))
	
	IF NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE_OF_SIZE(eWHSize, iWarehouse)
		INT iStat = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT2)
		INT iBitToCheck = 0
		
		IF eWHSize = eWarehouseSmall
			iBitToCheck 	= biFmGb_Help2_HasCashNoOffice0
			stickerString 	= "WHPURCH_TICK_1"
		ELIF eWHSize = eWarehouseMedium
			iBitToCheck 	= biFmGb_Help2_HasCashNoOffice1
			stickerString 	= "WHPURCH_TICK_2"
		ELSE
			iBitToCheck 	= biFmGb_Help2_HasCashNoOffice2
			stickerString 	= "WHPURCH_TICK_3"
		ENDIF
		
		IF NOT IS_BIT_SET(iStat, iBitToCheck)
			PRINT_TICKER(stickerString)
			SET_BIT(iStat, iBitToCheck)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT2, iStat) 
		ENDIF
	ENDIF
ENDPROC

FUNC INT CALCULATE_CONTRABAND_CRATES_TO_SELL(INT iSellButtonSelected)
	INT iamountStored 			= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iCurrentlyInsideWarehouse)
	INT iSpecialCount 			= GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iCurrentlyInsideWarehouse)
	INT NonSpecialContraTotal 	= iamountStored - iSpecialCount
	
	IF iSellButtonSelected = 1
		RETURN ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER1_AMOUNT) / 100))
	ELIF iSellButtonSelected = 2
		RETURN ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER2_AMOUNT) / 100))
	ELIF iSellButtonSelected = 3
		RETURN ROUND(NonSpecialContraTotal * (TO_FLOAT(g_sMPTunables.iEXEC_SELL_BUYER3_AMOUNT) / 100))
	ELIF iSellButtonSelected = 4
		RETURN iSpecialCount
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Handles inputs for the smaller version of the SecuroServ app used inside warehouses
PROC CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP()	
	IF currentButtonPressStage = inputConsumed_0
	OR currentButtonPressStage = inputConsumed_1
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SELECTION")
			currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			currentButtonPressStage 	= inputConsumed_0
		ENDIF	
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		PLAY_SOUND_FRONTEND(-1, "Mouse_Click", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
	ENDIF
	
	IF (currentButtonPressStage = inputConsumed_0)
	AND IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentSelectionReturnIndex)
		INT iButtonIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentSelectionReturnIndex)
		IF iButtonIndex > 0
		AND iButtonIndex < 5
			iContrabandBuyerID 	= iButtonIndex
			iCratesToSell 		= CALCULATE_CONTRABAND_CRATES_TO_SELL(iButtonIndex)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP [MAP] Sell Contraband in \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentlyInsideWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentlyInsideWarehouse), "...")
			currentButtonPressStage = inputConsumed_SELL
			PLAY_SOUND_FRONTEND(-1, "Sell", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
		ELIF iButtonIndex = 5
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP [MAP] Sell Contraband in \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentlyInsideWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentlyInsideWarehouse), "...")
			IF IS_BIT_SET(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
				currentButtonPressStage 			= inputConsumed_1
				CLEAR_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
				CLOSE_OVERLAY_SCREEN()
				PLAY_SOUND_FRONTEND(-1, "Error", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
			ELSE
				currentButtonPressStage = inputConsumed_CONFIRM
				PLAY_SOUND_FRONTEND(-1, "Confirm", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
			ENDIF
		ELIF iButtonIndex = 6
			CLOSE_OVERLAY_SCREEN()
			PLAY_SOUND_FRONTEND(-1, "Cancel", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
			
			iCratesToSell 						= 0
			iContrabandBuyerID 					= 0
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP overlay screen cancel button pressed ")
		ELSE
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP unknown button index ", iButtonIndex, ", warehouseID: ", iCurrentlyInsideWarehouse)
		ENDIF
	ENDIF
	
	IF currentButtonPressStage = inputConsumed_SELL
		RUN_INITIAL_CONTRABAND_SELL_CHECKS(iCurrentlyInsideWarehouse)
	ENDIF
	
	IF currentButtonPressStage = inputConsumed_CONFIRM
		IF NOT SELL_MP_CONTRABAND_FROM_SECUROSERV(iCurrentlyInsideWarehouse)
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Cannot launch sell mission for warehouse: ", iCurrentlyInsideWarehouse)
			
			iCratesToSell 				= 0
			iContrabandBuyerID 			= 0
			iMissionUnavailableReason 	= GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_SELL, FALSE)
			STRING sSellFailReason 		= GET_MISSION_UNAVAILABLE_REASON_STRING(FALSE, iCurrentlyInsideWarehouse)			
			START_OVERLAY_SCREEN("MP_WH_SELL", sSellFailReason, "WHOUSE_CONF", "", FALSE)
			PLAY_SOUND_FRONTEND(-1, "Error", "GTAO_Exec_SecuroServ_Warehouse_PC_Sounds")
			
			SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
		ELSE
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting contraband sell mission for warehouse: ", iCurrentlyInsideWarehouse)
			GB_BOSS_REQUEST_CONTRABAND_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_GB_CONTRABAND_SELL, iCurrentlyInsideWarehouse)
			
			CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCHED_CARGO_MISSION_FROM_HACKER_TRUCK)
			g_iContraSellMissionCratesToSell	= iCratesToSell
			g_iContraSellMissionBuyerID 		= iContrabandBuyerID
			g_bLaunchedMissionFrmLaptop			= TRUE
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Launching contraband sell mission for: ", g_iContraSellMissionCratesToSell, " crates to buyer: ", g_iContraSellMissionBuyerID)
			
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				g_sBusAppManagement.iMissionLaunched = FMMC_TYPE_GB_CONTRABAND_SELL
			ENDIF
			
			// Exit
			CLEANUP_SECUROSERVE()
		ENDIF
		
		currentButtonPressStage = inputConsumed_1
	ENDIF
ENDPROC

/// PURPOSE:
///    Deals with any input on the original warehouse pages
PROC HANDLE_SECURO_WAREHOUSE_INPUTS(enumItemConsumed &ecurrentButtonPressStage)
	// Buy Warehouse
	IF (eCurrentButtonPressStage = inputConsumed_BUY_WAREHOUSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting warehouse purchase. Runing confirm screen overlay")
		START_OVERLAY_SCREEN("WHOUSE_PURCH", "WHOUSE_PURCHQ", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
		eCurrentButtonPressStage 	= inputConsumed_0
		iMissionUnavailableReason 	= -1
		SET_BIT(iBS, SEC_BS_BUY_WAREHOUSE_CONFIRMED)
	ENDIF
	
	// Run the confirm overlay for the purchase of contraband.
	IF (eCurrentButtonPressStage = inputConsumed_BUY_SMALL_SHIPMENT)
	OR (eCurrentButtonPressStage = inputConsumed_BUY_MEDIUM_SHIPMENT)
	OR (eCurrentButtonPressStage = inputConsumed_BUY_LARGE_SHIPMENT)
		IF IS_CONTRABAND_BUY_COOLDOWN_ACTIVE(iCurrentSelectedWarehouse)
			START_OVERLAY_SCREEN("CONTRA_PURFA", "WH_MFREASON_11", "WHOUSE_CONF", "", FALSE)
			SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
		ELSE
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting contraband buy mission for warehouse. Runing confirm screen overlay")
			TEXT_LABEL_15 tlContraSize
			
			tlContraSize = BUILD_BUY_MISSION_CONFIRM_STRING(securoContraType, eCurrentSelectedShipmentSize, IS_SPECIAL_CONTRABAND_ITEM_ACTIVE())
			
			START_OVERLAY_SCREEN("WHOUSE_SHI", tlContraSize, "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
		ENDIF
		
		eCurrentButtonPressStage = inputConsumed_0
	ENDIF
	
	//Confirm purchase of warehouse or contraband
	IF (eCurrentButtonPressStage = inputConsumed_CONFIRM)
		//Buy a warehouse
		IF IS_BIT_SET(iBS, SEC_BS_BUY_WAREHOUSE_CONFIRMED)
			CLOSE_OVERLAY_SCREEN()
			INT iWarehouseID = iCurrentSelectedWarehouse
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Buying \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouseID), "\" $", GET_WAREHOUSE_COST(iWarehouseID))
			
			INT iResultSlot = -1
			IF BUY_MP_WAREHOUSE_FROM_SECUROSERV(iWarehouseID, iResultSlot)
				// Buy warehouse.
				START_OVERLAY_SCREEN("WHOUSE_PURCH", "WHOUSE_PURCHD", "WHOUSE_CONF", "", TRUE)
				
				// If warehouse is successfully purchased then resend the data for that warehouse now with the 'isOwned' param changed.			
				INT iOldWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iResultSlot)
				IF (iOldWarehouse <= 0)
					SET_WAREHOUSE_AS_PURCHASED(iWarehouseID)
					POPULATE_MP_SECUROSERV_SITE_PIN(iWarehouseID)
					REQUEST_NET_SPEND_COMMON_DATA_UPDATE()
				ELSE
					TRADE_IN_WAREHOUSE(iOldWarehouse, iWarehouseID)
					POPULATE_MP_SECUROSERV_SITE_PIN(iOldWarehouse)
					POPULATE_MP_SECUROSERV_SITE_PIN(iWarehouseID)
				ENDIF
				iJustPurchasedWHID = iWarehouseID
				SET_BIT(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)				
				SET_BIT(iBS, SEC_BS_JUST_PURCHASED_WAREHOUSE)
				SET_BIT(iBS, SEC_BS_DISPLAYING_PURCHASE_WH_TUT)
				SET_BIT(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
				RUN_CHECK_FOR_NEW_ITEM_TICKER(iWarehouseID)
			ELSE
				// Buy failed
				STRING sFailReason = "WHOUSE_PURCHF"
				IF IS_BIT_SET(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
					sFailReason = "WHSE_PURCHF_NM"
				ELIF g_sMPTunables.bexec_disable_warehouse_purchase
					sFailReason = "WHSE_PURCHF_TU"
				ENDIF
				START_OVERLAY_SCREEN("WHOUSE_PURFA", sFailReason, "WHOUSE_CONF", "", FALSE)
			ENDIF
			
			iCurrentSelectedWarehouse 		= -1
			eCurrentSelectedShipmentSize 	= CONTRABAND_INVALID
			eCurrentButtonPressStage 		= inputConsumed_1
			SET_BIT(iBS, SEC_BS_PROCESSING_WH_BASKET_DONE)
			CLEAR_BIT(iBS, SEC_BS_BUY_WAREHOUSE_CONFIRMED)
		ELIF IS_BIT_SET(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
			IF SELL_MP_CONTRABAND_FROM_SECUROSERV(iCurrentSelectedWarehouse)
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting contraband sell mission for warehouse: ", iCurrentSelectedWarehouse)
				g_bSecuroQuickExitOfficeChair = TRUE
				GB_BOSS_REQUEST_CONTRABAND_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_GB_CONTRABAND_SELL, iCurrentSelectedWarehouse)
				
				CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCHED_CARGO_MISSION_FROM_HACKER_TRUCK)
				g_iContraSellMissionCratesToSell	= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iCurrentSelectedWarehouse)
				g_iContraSellMissionBuyerID 		= ciContraSellMissionBuyer_1
				
				IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					g_sBusAppManagement.iMissionLaunched = FMMC_TYPE_GB_CONTRABAND_SELL
				ENDIF
				
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Launching contraband sell mission for: ", g_iContraSellMissionCratesToSell, " crates to buyer: ", g_iContraSellMissionBuyerID)
				
				// Exit
				CLEANUP_SECUROSERVE(TRUE)
			ELSE
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Cannot launch sell mission for warehouse: ", iCurrentSelectedWarehouse)
				
				iMissionUnavailableReason 	= GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_GB_CONTRABAND_SELL, FALSE)
				STRING sSellFailReason 		= GET_MISSION_UNAVAILABLE_REASON_STRING(FALSE, iCurrentSelectedWarehouse)
				
				START_OVERLAY_SCREEN("MP_WH_SELL", sSellFailReason, "WHOUSE_CONF", "", FALSE)
				
				SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
				iCurrentSelectedWarehouse 			= -1
				eCurrentButtonPressStage 			= inputConsumed_1
				CLEAR_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
			ENDIF
			
			eCurrentButtonPressStage = inputConsumed_1
		//Else buy contraband for a warehouse	
		ELSE
			INT iWarehouseID = iCurrentSelectedWarehouse
			
			IF iWarehouseID > ciW_Invalid
			AND BUY_MP_CONTRABAND_FROM_SECUROSERV(iWarehouseID, eCurrentSelectedShipmentSize)			
			
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Buying shipment of ", eCurrentSelectedShipmentSize, " units from \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouseID), "\" $", GET_CONTRABAND_SHIPMENT_COST(eCurrentSelectedShipmentSize))
				// Start the mission - bit of a fudge for now we should probably add a central controller for this?
				BOOL bActivateSpecialItemMission = FALSE
				//Only start a special item mission if the user clicked the 1 crate option
				g_bSecuroQuickExitOfficeChair = TRUE
				IF IS_BIT_SET(iBS2, SEC_BS2_SPECIAL_ITEM)
				AND eCurrentSelectedShipmentSize = CONTRABAND_SMALL
					bActivateSpecialItemMission = TRUE
				ENDIF
				
				INT iFMMCTypeChosen
				
				CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCHED_CARGO_MISSION_FROM_HACKER_TRUCK)
				iFMMCTypeChosen = REQUEST_LAUNCH_ANY_SOURCE_CARGO_MISSION(iWarehouseID, eCurrentSelectedShipmentSize, securoContraType, bActivateSpecialItemMission)
				
				IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					g_sBusAppManagement.iMissionLaunched = iFMMCTypeChosen
				ENDIF
				
				SET_BIT(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
				
				//Exit
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_START_MIS)
				AND NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
					iTutorialDialogueID = 4
					SET_BIT(iBS, SEC_BS_DELAY_APP_EXIT_FOR_CONVERSATION)
				ELSE
					CLEANUP_SECUROSERVE(TRUE)
				ENDIF
			ELSE
				// Purchase Failed
				STRING sFailReason = GET_MISSION_UNAVAILABLE_REASON_STRING(TRUE, iWarehouseID)
				START_OVERLAY_SCREEN("WHOUSE_PURFA", sFailReason, "WHOUSE_CONF", "", FALSE)
				SET_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
			ENDIF
			
			iCurrentSelectedWarehouse 	= -1
			eCurrentButtonPressStage 	= inputConsumed_1
		ENDIF
	ENDIF
	
	IF(eCurrentButtonPressStage = inputConsumed_SELL)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting warehouse purchase. Runing confirm screen overlay")
		RUN_INITIAL_CONTRABAND_SELL_CHECKS(iCurrentSelectedWarehouse)
	ENDIF
ENDPROC

FUNC BOOL LAUNCH_IE_STEAL_MISSION(SEC_OVERLAY_PROMPT &iPrompt)

	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal >= ciMAX_IE_OWNED_VEHICLES
		iPrompt = SEC_OVERLAY_WHOUSE_FULL
		RETURN FALSE
	ENDIF
	
	INT iFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_VEHICLE_EXPORT_BUY, FALSE)
	
	IF iFailReason = GB_MU_REASON_GOON_IS_ANIMAL
		iPrompt = SEC_OVERLAY_GOON_ANIMAL
		RETURN FALSE
	ELIF iFailReason = GB_MU_REASON_GOON_GAMBLING
		iPrompt = SEC_OVERLAY_GOON_GAMBLING
		RETURN FALSE
	ENDIF

	//Check we are about to launch a mission with a valid vehicle that we don't own
	CHECK_IE_VEH_STEAL_LIST_FOR_OWNED_VEHICLES()
	
	IF GET_ACTIVE_IE_STEAL_MISSION_VEHICLE() = IE_VEH_INVALID
		//Shuffle the list
		SHUFFLE_IE_VEHICLE_LIST_FOR_STEAL_MISSIONS()
		//Make sure we don't start at a point in the list with an empty slot
		CHECK_IE_VEH_STEAL_LIST_FOR_OWNED_VEHICLES(TRUE)
	ENDIF
	
	CDEBUG1LN(DEBUG_INTERNET, "<SECURO>[Mission_Launch] LAUNCH_IE_STEAL_MISSION Requesting Mission: FMMC_TYPE_VEHICLE_EXPORT_BUY",
									" With vehicle: ", GET_STRING_FROM_IE_VEHICLE_ENUM(GET_ACTIVE_IE_STEAL_MISSION_VEHICLE()))
	
	IF GET_ACTIVE_IE_STEAL_MISSION_VEHICLE() != IE_VEH_INVALID
	AND LAUNCH_MP_VEH_STEAL_FROM_SECUROSERV()
	AND IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(VEHICLE_EXPORT_DROPOFF_GB_WAREHOUSE)
		//Get the mission vehicle
		IE_VEHICLE_ENUM eMissionVeh = GET_ACTIVE_IE_STEAL_MISSION_VEHICLE()
		//Launch the mission
		GB_BOSS_REQUEST_VEHICLE_EXPORT_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_VEHICLE_EXPORT_BUY, eMissionVeh, IE_VEH_INVALID, IE_VEH_INVALID, IE_VEH_INVALID)
		//Set the active vehicle for the next steal mission
		INCREMENT_ACTIVE_IE_STEAL_MISSION_VEHICLE()
		//Quick exit the office chair
		g_bSecuroQuickExitOfficeChair = TRUE
		//Clear telemetry
		CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCHED_VEH_EXP_MISSION_FROM_HACKER_TRUCK)
		
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			g_sBusAppManagement.iMissionLaunched = FMMC_TYPE_VEHICLE_EXPORT_BUY
		ENDIF
		
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
			IF GET_ACTIVE_IE_STEAL_MISSION_VEHICLE() = IE_VEH_INVALID
				CASSERTLN(DEBUG_INTERNET, "<SECURO> LAUNCH_IE_STEAL_MISSION Still trying to launch a mission with an invalid vehicle. This will not work!")
			ENDIF
		#ENDIF
		CDEBUG1LN(DEBUG_INTERNET, "<SECURO>[Mission_Launch] LAUNCH_IE_STEAL_MISSION Couldn't activate warehouse dropoff!") 
	ENDIF
	
	iPrompt = SEC_OVERLAY_NO_VEH_FOUND
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_IE_WH_PURCHASE_COST(IMPORT_EXPORT_GARAGES eVehicleWH, IE_GARAGE_INTERIORS eWarehouseInterior, BOOL bRenovating)
	INT iTotalCost
	BOOL bPurchasingvehicleWH 	= (eVehicleWH != GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()))
	BOOL bTradingInExisitingWH	= (DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID()) AND bPurchasingvehicleWH)
	
	IF bPurchasingvehicleWH
	OR bTradingInExisitingWH
		iTotalCost += GET_IE_GARAGE_COST(eVehicleWH)
		
		IF bTradingInExisitingWH
			iTotalCost -= GET_IE_GARAGE_TRADE_PRICE(GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	//We always need to add the cost of a new interior
	iTotalCost += GET_IE_GARAGE_INTERIOR_COST(eWarehouseInterior, bRenovating)
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> GET_IE_WH_PURCHASE_COST: Total cost: ", iTotalCost, " bPurchasingvehicleWH ",bPurchasingvehicleWH, " bTradingInExisitingWH ", bTradingInExisitingWH)
	
	RETURN iTotalCost
ENDFUNC

FUNC BOOL CAN_PLAYER_AFFORD_IE_WH_PURCHASE(IMPORT_EXPORT_GARAGES eVehicleWH, IE_GARAGE_INTERIORS eWarehouseInterior, BOOL bRenovating)
	
	INT iTotalCost = GET_IE_WH_PURCHASE_COST(eVehicleWH, eWarehouseInterior, bRenovating)
	
	IF iTotalCost > 0 
	AND NOT NETWORK_CAN_SPEND_MONEY(iTotalCost, FALSE, TRUE, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> CAN_PLAYER_AFFORD_IE_WH_PURCHASE: purchase failed, player doesn't have sufficient cash. Total cost: ", iTotalCost)
		INT iItemHash = GET_HASH_KEY(GET_IE_GARAGE_NAME_LABEL(eVehicleWH))
		TRIGGER_BROWSER_NAG_SCREEN(iItemHash, iTotalCost)		
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> CAN_PLAYER_AFFORD_IE_WH_PURCHASE: purchase can go ahead player has sufficient cash. Total cost: ", iTotalCost)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Deals with any input on the IE vehicle warehouse pages
PROC HANDLE_SECURO_IE_WAREHOUSE_INPUTS(enumItemConsumed &eCurrentButtonPressStage)

	INT iIEWarehouseID 						= ((iCurrentSelectedWarehouse - iWarehouseOffset) - 1)
	INT iCollectedVehCount 					= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal
	IMPORT_EXPORT_GARAGES eSelectedWH		= INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iIEWarehouseID)
	IMPORT_EXPORT_GARAGES eOwnedWH 			= GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())
	IE_GARAGE_INTERIORS eOwnedWHInterior 	= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.eWarehouseStyle
	STRING sFailReason
	
	SWITCH eCurrentButtonPressStage
		//Steal vehicle buttons on the map and stats page
		CASE inputConsumed_STEAL_VEHICLE
		CASE inputConsumed_MAP_STEAL_VEHICLE
			IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_EYE_IN_THE_SKY)
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting IE steal mission. Cooldown timer stilll running. Runing confirm screen overlay")
				START_OVERLAY_SCREEN("SEC_VEH_STEAL", "SEC_V_STEAL_F", "WHOUSE_CONF", "", FALSE)
				CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			ELSE
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting IE steal mission. Runing confirm screen overlay")
				START_OVERLAY_SCREEN("SEC_VEH_STEAL", "SEC_VEH_STEALQ", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
				SET_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			ENDIF
			
			eCurrentButtonPressStage = inputConsumed_1				
			CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
		BREAK
		
		//Cancel button on the stats page overlay
		CASE inputConsumed_VEH_STAT_CANCEL
			CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
			eCurrentButtonPressStage = inputConsumed_1
		BREAK
		
		//Buy vehicle warehouse button
		CASE inputConsumed_BUY_VEH_WH			
			eCurrentButtonPressStage = inputConsumed_1			
			CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			SET_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
			
			IF eSelectedWH = eOwnedWH
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting IE warehouse interior purchase. Runing confirm screen overlay")
				START_OVERLAY_SCREEN("IEWHINT_PURCH", "IEWHINT_PURCHQ", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
			ELIF DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting IE warehouse trade. Runing confirm trade screen overlay")
				START_OVERLAY_SCREEN("IEWHOUSE_PURCH", "IE_WH_TRADE", "WHOUSE_CONF", "WHOUSE_CANC", TRUE, FALSE, GET_IE_GARAGE_TRADE_PRICE(GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())))
			ELSE
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting IE warehouse purchase. Runing confirm screen overlay")
				START_OVERLAY_SCREEN("IEWHOUSE_PURCH", "IEWHOUSE_PURCHQ", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
			ENDIF
		BREAK
		
		//Confirm button on the map page overlay screen
		CASE inputConsumed_VEH_MAP_ACCEPT
		CASE inputConsumed_VEH_STAT_ACCEPT
			IF IS_BIT_SET(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
				
				IF NOT CAN_PLAYER_AFFORD_IE_WH_PURCHASE(eSelectedWH, eSelectedInterior, (eSelectedWH = eOwnedWH))
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS CAN_PLAYER_AFFORD_IE_WH_PURCHASE returned false.")
					IF eSelectedWH = eOwnedWH
						START_OVERLAY_SCREEN("WHOUSE_PURFA", "IEWH_PURCHF_NM", "WHOUSE_CONF", "", FALSE)
					ELSE
						START_OVERLAY_SCREEN("WHOUSE_PURFA", "WHSE_PURCHF_NM", "WHOUSE_CONF", "", FALSE)
					ENDIF
					SET_BIT(iBS, SEC_BS_IE_TRANSACTION_FAILED)
				ELIF eSelectedWH = eOwnedWH
										
					//Selecting a new interior
					IF eOwnedWHInterior = eSelectedInterior
					
						//We already own this interior
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requested purchase of an interior we already own")
						START_OVERLAY_SCREEN("WHOUSE_PURFA", "IEWH_INT_PURCHF", "WHOUSE_CONF", "", FALSE)
						
					ELIF BUY_MP_IE_WAREHOUSE_INTERIOR(eSelectedWH, eSelectedInterior)
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Completed purchase of IE garage interior rennovation to: ", eSelectedInterior, ". Did not select the underground garage for purchase")
						START_OVERLAY_SCREEN("IEWHOUSE_PURCH", "IEWHINT_PURCHD", "WHOUSE_CONF", "", TRUE)
						
						//Set the interior variation
						SET_OWND_IE_WAREHOUSE_INTERIOR(eSelectedInterior)
						eSelectedInterior = IE_INTERIOR_BASIC
						
						//Send the data back to SF
						POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(eSelectedWH, iCollectedVehCount)
						
						//Request a save
						SET_BIT(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
						SET_BIT(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
					ELSE
						// Buy failed
						sFailReason = "WHOUSE_PURCHF"
						IF IS_BIT_SET(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
							sFailReason = "WHSE_PURCHF_NM"
						ENDIF
						START_OVERLAY_SCREEN("WHOUSE_PURFA", sFailReason, "WHOUSE_CONF", "", FALSE)
						
						SET_BIT(iBS, SEC_BS_IE_TRANSACTION_FAILED)
					ENDIF
				ELIF BUY_MP_IE_WAREHOUSE_FROM_SECUROSERV(eSelectedWH)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Completed purchase of IE garage: ", iIEWarehouseID, " with interior: ", eSelectedInterior)
					START_OVERLAY_SCREEN("IEWHOUSE_PURCH", "IEWHOUSE_PURCHD", "WHOUSE_CONF", "", TRUE)
					
					IF DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
						//Trade our existing IE warehouse
						SET_IE_GARAGE_AS_OWNED(INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iIEWarehouseID))
						SET_OWND_IE_WAREHOUSE_INTERIOR(eSelectedInterior)
						POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(eOwnedWH, iCollectedVehCount)
						POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(eSelectedWH, iCollectedVehCount)	
					ELSE
						//Buying our first IE warehouse
						SET_IE_GARAGE_AS_OWNED(INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iIEWarehouseID))
						SET_OWND_IE_WAREHOUSE_INTERIOR(eSelectedInterior)
						POPULATE_MP_SECUROSERV_SITE_VEH_WH_PIN(eSelectedWH, iCollectedVehCount)	
						REQUEST_NET_SPEND_COMMON_DATA_UPDATE()
					ENDIF
					
					//Set the underground garage as owned
					SET_IE_WAREHOUSE_UNDERGROUND_GAR_AS_OWNED()
					eSelectedInterior = IE_INTERIOR_BASIC
					
					//Request a save
					SET_BIT(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
					SET_BIT(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
				ELSE
					// Buy failed
					sFailReason = "WHOUSE_PURCHF"
					IF IS_BIT_SET(iBS, SEC_BS_PURCHASE_FAILED_NO_MONEY)
						sFailReason = "WHSE_PURCHF_NM"
//					ELIF g_sMPTunables.bexec_disable_warehouse_purchase
//						sFailReason = "WHSE_PURCHF_TU"
					ENDIF
					START_OVERLAY_SCREEN("WHOUSE_PURFA", sFailReason, "WHOUSE_CONF", "", FALSE)
					
					SET_BIT(iBS, SEC_BS_IE_TRANSACTION_FAILED)
				ENDIF					
									
				CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
				
			ELIF IS_BIT_SET(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_IE_WAREHOUSE_INPUTS Requesting steal vehicle mission launch")
				SEC_OVERLAY_PROMPT iPrompt
				
				IF LAUNCH_IE_STEAL_MISSION(iPrompt)
					INCREMENT_TOTAL_NUM_STEAL_MISSIONS_STARTED()
					SET_SHARD_SHOULD_BE_HELD_UP_FOR_OFFICE_SEAT_ANIM(TRUE)
					SET_PLAYER_IS_ON_IE_VEHICLE_STEAL_MISSION(TRUE)
					CLEANUP_SECUROSERVE(TRUE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
					START_OVERLAY_SCREEN("SEC_VEH_STEAL", GET_SEC_OVERLAY_PROMPT(iPrompt), "WHOUSE_CONF", "", FALSE)
				ENDIF
				
				CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			ENDIF
			
			eCurrentButtonPressStage = inputConsumed_1
		BREAK
		
		//Cancel button on the map page confirm screen overlay
		CASE inputConsumed_VEH_MAP_CANCEL
			CLEAR_BIT(iBS, SEC_BS_LAUNCH_STEAL_MIS_SELECTED)
			CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
			eCurrentButtonPressStage = inputConsumed_1
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_SECURO_SVM_INPUTS(enumItemConsumed &eCurrentButtonPressStage)

	INT iCountOfMissionsToUnock	= (4 * (iSVMToLaunch + 1))
	BOOL bMissionUnlocked		= FALSE

	SWITCH eCurrentButtonPressStage
		//Confirm button on the map page overlay screen
		CASE inputConsumed_SVM_MAP_ACCEPT
			IF iSVMToLaunch < 0
			OR iSVMToLaunch > ciMAX_AT_FLOW_MISSIONS
				IF NOT IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Requesting IE SVM mission (2) with invalid ID: ", iSVMToLaunch, ". Runing confirm screen overlay")
					PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
					START_OVERLAY_SCREEN("SVM_MIS_TI", "SVM_MIS_FL", "WHOUSE_CONF", "", FALSE)
					
					CDEBUG1LN(DEBUG_INTERNET, "HANDLE_SECURO_SVM_INPUTS - inputConsumed_SVM_MAP_ACCEPT: Setting bit SVM_BS_DISPLAY_OVERLAY_WARNING")
					SET_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Confirmed request launching SVM mission: ", iSVMToLaunch)
				g_bLaunchedMissionFrmLaptop = TRUE
				SVM_FLOW_LAUNCH_NEXT_AVAILABLE_MISSION(g_sTransitionSessionData.ciCam, iSVMToLaunch, ciFLOW_MISSION_TYPE_SVM)
				CLEANUP_SECUROSERVE()
			ENDIF
		BREAK
		//Cancel button on the map page confirm screen overlay
		CASE inputConsumed_SVM_MAP_CANCEL
			eCurrentButtonPressStage = inputConsumed_1
		BREAK
		//SVM mission selection
		CASE inputConsumed_SVM_LAUNCH		
			
			IF iCurrentPageID != ciSVMPageID
				CASSERTLN(DEBUG_INTERNET, "<SECURO> HANDLE_SECURO_SVM_INPUTS Invalid page id for SVM input. Page: ", iCurrentPageID, " SVM mission ID: ", iSVMToLaunch)
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				eCurrentButtonPressStage = inputConsumed_1
				EXIT
			ENDIF
			
			bMissionUnlocked = (GET_MP_INT_CHARACTER_STAT(MP_STAT_AT_FLOW_IMPEXP_NUM) >= iCountOfMissionsToUnock)
			
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_SvmFlowAlwaysAvailable")
				bMissionUnlocked =  TRUE
			ENDIF
			#ENDIF

			IF iSVMToLaunch < 0
			OR iSVMToLaunch > ciMAX_AT_FLOW_MISSIONS
				IF NOT IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Requesting IE SVM mission (2) with invalid ID: ", iSVMToLaunch, ". Runing confirm screen overlay")
					PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
					START_OVERLAY_SCREEN("SVM_MIS_TI", "SVM_MIS_FL", "", "WHOUSE_CONF", FALSE)
					
					CDEBUG1LN(DEBUG_INTERNET, "HANDLE_SECURO_SVM_INPUTS - inputConsumed_SVM_LAUNCH: Setting bit SVM_BS_DISPLAY_OVERLAY_WARNING")
					SET_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
				ENDIF
				
				iSVMToLaunch = -1
			ELIF NOT bMissionUnlocked
				IF NOT IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Requesting IE SVM mission which we have not unlocked. ID: ", iSVMToLaunch)
					PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
					START_OVERLAY_SCREEN("SVM_MIS_TI", "SVM_MIS_FNA", "", "WHOUSE_CONF", FALSE)
					
					CDEBUG1LN(DEBUG_INTERNET, "HANDLE_SECURO_SVM_INPUTS - inputConsumed_SVM_LAUNCH: Setting bit SVM_BS_DISPLAY_OVERLAY_WARNING - Mission not unlocked.")
					SET_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
				ENDIF
				
				iSVMToLaunch = -1
			ELIF SVM_FLOW_GET_NEXT_AVAILABLE_MISSION_CONTENT_ARRAY_POS(iSVMToLaunch, ciFLOW_MISSION_TYPE_SVM) = -1
			OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
				IF NOT IS_BIT_SET(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Requesting IE SVM mission which is not in the game. ID: ", iSVMToLaunch)
					PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
					START_OVERLAY_SCREEN("SVM_MIS_TI", "SVM_MIS_FL", "", "WHOUSE_CONF", FALSE)
					
					SET_BIT(iBS, SEC_BS_DISPLAY_OVERLAY_WARNING)
				ENDIF
				
				iSVMToLaunch = -1
			ELSE
				CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS HANDLE_SECURO_SVM_INPUTS Confirmed request launching SVM mission: ", iSVMToLaunch)
				START_OVERLAY_SCREEN("SVM_MIS_TI", "SVM_MIS_L", "WHOUSE_CONF", "WHOUSE_CANC", TRUE)
				PLAY_SOUND_FRONTEND(-1, "Navigate", "GTAO_Exec_SecuroServ_Computer_Sounds")
			ENDIF
			
			currentButtonPressStage = inputConsumed_1
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_ACCEPT_BUTTON_INPUT_JUST_PRESSED()	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) 
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CANCEL_BUTTON_INPUT_JUST_PRESSED()	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) 
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CHECKS_FOR_CURRENT_SCREEN_ID()
	IF NOT IS_BIT_SET(iBS, SEC_BS_CHECK_PAGE_ID)
		IF IS_ACCEPT_BUTTON_INPUT_JUST_PRESSED()
		OR IS_CANCEL_BUTTON_INPUT_JUST_PRESSED()
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SCREEN_ID")
			currentCurrentScreenReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			SET_BIT(iBS, SEC_BS_CHECK_PAGE_ID)
		ENDIF
	ELIF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentCurrentScreenReturnIndex)
		iCurrentPageID = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentCurrentScreenReturnIndex)
		CPRINTLN(DEBUG_INTERNET,"<SECURO> MAINTAIN_CHECKS_FOR_CURRENT_SCREEN_ID CURRENT PAGE ID:", iCurrentPageID)
		CLEAR_BIT(iBS, SEC_BS_CHECK_PAGE_ID)
	ENDIF
ENDPROC

PROC MAINTAIN_CHECKS_FOR_CURRENT_SELECTION_ID()
	IF currentButtonPressStage = inputConsumed_0
	OR currentButtonPressStage = inputConsumed_1		
		IF IS_ACCEPT_BUTTON_INPUT_JUST_PRESSED()
			IF NOT IS_BIT_SET(iBS, SEC_BS_WAITING_FOR_BUTTON_RETURN_VALUE)
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SELECTION")
				currentSelectionReturnIndex 	= END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_SELECTED_WAREHOUSE_ID")
				currentWarehouseReturnIndex 	= END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
				
				SET_BIT(iBS, SEC_BS_WAITING_FOR_BUTTON_RETURN_VALUE)
			ENDIF
			
			SET_BIT(iBS, SEC_BS_CHECK_SIDE_BAR)
			currentButtonPressStage = inputConsumed_0
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_CHECKS_FOR_IS_WAREHOUSE_PANEL_SHOWING()
	IF iCurrentPageID = ciWarehouseMapPageID
	OR iCurrentPageID = ciVehicleWHMapPageID
		IF IS_BIT_SET(iBS, SEC_BS_CHECK_SIDE_BAR)
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "IS_WAREHOUSE_PANEL_SHOWING")
			buyWindowOpenReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			CLEAR_BIT(iBS, SEC_BS_CHECK_SIDE_BAR)
		ELIF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(buyWindowOpenReturnIndex)
			IF GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_BOOL(buyWindowOpenReturnIndex)
				SET_BIT(iBS, SEC_BS_SIDE_PANEL_SHOWING)
			ELSE
				CLEAR_BIT(iBS, SEC_BS_SIDE_PANEL_SHOWING)
			ENDIF
			CLEAR_BIT(iBS, SEC_BS_CHECK_SIDE_BAR)
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_BUTTON_PRESS()
	
	#IF IS_DEBUG_BUILD
	INT iStrOffset = 0, iColumn = 0
	INT iRed, iGreen, iBlue, iAlpha
	TEXT_LABEL_63 str1 = "", str2 = ""
	str1  = "CHECK_BUTTON_PRESS stage:"
	str1 += ENUM_TO_INT(currentButtonPressStage)
	#ENDIF
	
	MAINTAIN_CHECKS_FOR_CURRENT_SCREEN_ID()
	MAINTAIN_CHECKS_FOR_CURRENT_SELECTION_ID()
	MAINTAIN_CHECKS_FOR_IS_WAREHOUSE_PANEL_SHOWING()
	
	IF (currentButtonPressStage = inputConsumed_0)
		IF (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentSelectionReturnIndex) AND IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentWarehouseReturnIndex))
			INT iButtonIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentSelectionReturnIndex)
			
			CLEAR_BIT(iBS, SEC_BS_WAITING_FOR_BUTTON_RETURN_VALUE)

			// You can find the id of the warehouse in question with this...
			iCurrentSelectedWarehouse = GET_WAREHOUSE_INDEX_FROM_SCALEFORM_INDEX(GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentWarehouseReturnIndex))
			
			CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [TOP]: ", iButtonIndex)
			
			//Login Screen
			CONST_INT iCONST_Button_Index_1001	1001 //Log In button on log in panel
			CONST_INT iCONST_Button_Index_1002	1002 //Quit button on access denied popup
			//Stats Screen
			CONST_INT iCONST_Button_Index_2001	2001 //Warehouse map button
			CONST_INT iCONST_Button_Index_2002	2002 //Buy button in column 1 of the bar graph
			CONST_INT iCONST_Button_Index_2003	2003 //Buy button in column 2 of the bar graph
			CONST_INT iCONST_Button_Index_2004	2004 //Buy button in column 3 of the bar graph
			CONST_INT iCONST_Button_Index_2005	2005 //Buy button in column 4 of the bar graph
			CONST_INT iCONST_Button_Index_2006	2006 //Buy button in column 5 of the bar graph
			//Map Screen
			CONST_INT iCONST_Button_Index_3001	3001 //Zoom In button
			CONST_INT iCONST_Button_Index_3002	3002 //Zoom Out button
			CONST_INT iCONST_Button_Index_3003	3003 //Filter All button
			CONST_INT iCONST_Button_Index_3004	3004 //Filter Small button
			CONST_INT iCONST_Button_Index_3005	3005 //Filter Medium button
			CONST_INT iCONST_Button_Index_3006	3006 //Filter Large button
			CONST_INT iCONST_Button_Index_3007	3007 //Buy Warehouse button
			CONST_INT iCONST_Button_Index_3008	3008 //Buy Small Shipment button
			CONST_INT iCONST_Button_Index_3009	3009 //Buy Medium Shipment button
			CONST_INT iCONST_Button_Index_3010	3010 //Buy Large Shipment button
			CONST_INT iCONST_Button_Index_3011	3011 //Sell Contraband Button
			CONST_INT iCONST_Button_Index_3012	3012 //Set Waypoint button on purchase success popup
			CONST_INT iCONST_Button_Index_3013	3013 //Close button on purchase success popup
			//Vehicle stats screen
			CONST_INT iCONST_Button_Index_4001	4001 //Steal Vehicle
			CONST_INT iCONST_Button_Index_4002	4002 //Show Vehicle Map Screen
			CONST_INT iCONST_Button_Index_4003	4003 //Accept button on map screen popup/overlay
			CONST_INT iCONST_Button_Index_4004	4004 //Cancel button on map screen popup/overlay
			
			CONST_INT iCONST_Button_Index_4015	4015 //IE warehouse Interior Style 0
			CONST_INT iCONST_Button_Index_4016	4016 //IE warehouse Interior Style 1
			CONST_INT iCONST_Button_Index_4017	4017 //IE warehouse Interior Style 2
			CONST_INT iCONST_Button_Index_4018	4018 //IE warehouse Purchase Interior / Basement renovation
			CONST_INT iCONST_Button_Index_4019	4019 //IE warehouse Cancel Interior Overlay
			CONST_INT iCONST_Button_Index_4023	4023 //IE warehouse Show Interior Overlay
			
			//Vehicle Map
			CONST_INT iCONST_Button_Index_5003 	5003 //IE WH map page - Filter button: All
			CONST_INT iCONST_Button_Index_5004 	5004 //IE WH map page - Filter button: Owned
			CONST_INT iCONST_Button_Index_5005 	5005 //Buy Vehicle Warehouse
			CONST_INT iCONST_Button_Index_5006	5006 //Steal Vehicle
			CONST_INT iCONST_Button_Index_5007	5007 //Accept button on map screen popup/overlay
			CONST_INT iCONST_Button_Index_5008	5008 //Cancel button on map screen popup/overlay
			CONST_INT iCONST_Button_Index_5010	5010 //Buy IE WH (Brings up the interiors overlay)
			CONST_INT iCONST_Button_Index_5011	5011 //Renovate IE WH (Brings up the interiors overlay)
			CONST_INT iCONST_Button_Index_5012	5012 //IE warehouse Interior 1
			CONST_INT iCONST_Button_Index_5013	5013 //IE warehouse Interior 2
			CONST_INT iCONST_Button_Index_5014	5014 //IE warehouse Interior 3
			CONST_INT iCONST_Button_Index_5015	5015 //IE warehouse Interior overlay cancel button
			//Special Vehicle page
			CONST_INT iCONST_Button_Index_6002	6002 //Accept button on map screen popup/overlay
			CONST_INT iCONST_Button_Index_6003	6003 //Cancel button on map screen popup/overlay	
			
			CONST_INT iCONST_SVM_Button_Index_0	0 	 //Special vehicle mission 1
			CONST_INT iCONST_SVM_Button_Index_1	1 	 //Special vehicle mission 2
			CONST_INT iCONST_SVM_Button_Index_2	2 	 //Special vehicle mission 3
			CONST_INT iCONST_SVM_Button_Index_3	3 	 //Special vehicle mission 4
			CONST_INT iCONST_SVM_Button_Index_4	4 	 //Special vehicle mission 5
			CONST_INT iCONST_SVM_Button_Index_5	5 	 //Special vehicle mission 6
			CONST_INT iCONST_SVM_Button_Index_6	6 	 //Special vehicle mission 7
			CONST_INT iCONST_SVM_Button_Index_7	7 	 //Special vehicle mission 8
			
			SWITCH iButtonIndex
				/////////////////////////////////////////////
				///    	LOGIN SCREEN
				///    	
				CASE iCONST_Button_Index_1001	//Log In button on log in panel
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [LOGIN] Log In")
				BREAK
				CASE iCONST_Button_Index_1002	//Quit button on access denied popup
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [LOGIN] Quit")
					//CLEANUP_SECUROSERVE()
				BREAK
				
				/////////////////////////////////////////////
				///    	STATS SCREEN
				///
				CASE iCONST_Button_Index_2001	//Warehouse map button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Warehouse map")
				BREAK
				CASE iCONST_Button_Index_2002	//Buy button in column 1 of the bar graph
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Buy in column 1")
				BREAK
				CASE iCONST_Button_Index_2003	//Buy button in column 2 of the bar graph
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Buy in column 2")
				BREAK
				CASE iCONST_Button_Index_2004	//Buy button in column 3 of the bar graph
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Buy in column 3")
				BREAK
				CASE iCONST_Button_Index_2005	//Buy button in column 4 of the bar graph
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Buy in column 4")
				BREAK
				CASE iCONST_Button_Index_2006	//Buy button in column 5 of the bar graph
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Buy in column 5")
				BREAK
				
				/////////////////////////////////////////////
				///    	MAP SCREEN
				///    
				CASE iCONST_Button_Index_3001	//Zoom In button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Zoom In")
				BREAK
				CASE iCONST_Button_Index_3002	//Zoom Out button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Zoom Out")
				BREAK
				CASE iCONST_Button_Index_3003	//Filter All button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Filter All")
				BREAK
				CASE iCONST_Button_Index_3004	//Filter Small button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Filter Small")
				BREAK
				CASE iCONST_Button_Index_3005	//Filter Medium button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Filter Medium")
				BREAK
				CASE iCONST_Button_Index_3006	//Filter Large button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Filter Large")
				BREAK
				CASE iCONST_Button_Index_3007	//Buy Warehouse button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Buy \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentSelectedWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentSelectedWarehouse), "...")
					currentButtonPressStage = inputConsumed_BUY_WAREHOUSE
				BREAK
				CASE iCONST_Button_Index_3008	//Buy Small Shipment button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Buy small shipment from \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentSelectedWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentSelectedWarehouse), "...")
					eCurrentSelectedShipmentSize = CONTRABAND_SMALL
					currentButtonPressStage = inputConsumed_BUY_SMALL_SHIPMENT
				BREAK
				CASE iCONST_Button_Index_3009	//Buy Medium Shipment button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Buy medium shipment from \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentSelectedWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentSelectedWarehouse), "...")
					eCurrentSelectedShipmentSize = CONTRABAND_MEDIUM
					currentButtonPressStage = inputConsumed_BUY_MEDIUM_SHIPMENT
				BREAK
				CASE iCONST_Button_Index_3010	//Buy Large Shipment button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Buy large shipment from \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentSelectedWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentSelectedWarehouse), "...")
					eCurrentSelectedShipmentSize = CONTRABAND_LARGE
					currentButtonPressStage = inputConsumed_BUY_LARGE_SHIPMENT
				BREAK
				CASE iCONST_Button_Index_3011	//Sell Contraband Button
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Sell Contraband in \"", DEBUG_GET_STRING_FOR_WAREHOUSE(iCurrentSelectedWarehouse), "\" $", GET_WAREHOUSE_COST(iCurrentSelectedWarehouse), "...")
					currentButtonPressStage = inputConsumed_SELL
				BREAK
				CASE iCONST_Button_Index_3012	//Confirm button (Can add whatever text we need to this)
					IF NOT IS_BIT_SET(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
					AND NOT IS_BIT_SET(iBS, SEC_BS_PROCESSING_WH_BASKET_DONE)
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Going to confirm purchase stage")
						currentButtonPressStage = inputConsumed_CONFIRM
					ELSE
						CLEAR_BIT(iBS, SEC_BS_PROCESSING_WH_BASKET_DONE)
						CLEAR_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
						iCurrentSelectedWarehouse 			= -1
						currentButtonPressStage 			= inputConsumed_1
						CLOSE_OVERLAY_SCREEN()
					ENDIF
					
					CLEAR_BIT(iBS, SEC_BS_JUST_PURCHASED_WAREHOUSE)
					
					IF NOT IS_BIT_SET(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
						PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Success", "GTAO_Exec_SecuroServ_Computer_Sounds")
					ENDIF
					
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [OVERLAY] inputConsumed_CONFIRM")
				BREAK
				CASE iCONST_Button_Index_3013	//Close button on purchase success popup
					iCurrentSelectedWarehouse 			= -1
					currentButtonPressStage 			= inputConsumed_1
					CLEAR_BIT(iBS, SEC_BS_PROCESSING_CONTRA_BASKET_FAILED)
					CLEAR_BIT(iBS, SEC_BS_SELL_CONTRABAND_CONFIRMED)
					CLEAR_BIT(iBS, SEC_BS_BUY_WAREHOUSE_CONFIRMED)
					CLOSE_OVERLAY_SCREEN()
					PLAY_SOUND_FRONTEND(-1, "Popup_Cancel", "GTAO_Exec_SecuroServ_Computer_Sounds")
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Close")
				BREAK
				/////////////////////////////////////////////
				///    	Vehicle stats screen
				///   
				CASE iCONST_Button_Index_4001 //Steal Vehicle
					currentButtonPressStage = inputConsumed_STEAL_VEHICLE
					PLAY_SOUND_FRONTEND(-1, "Sell", "GTAO_Exec_SecuroServ_Computer_Sounds" )
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Steal vehicle")
				BREAK
				CASE iCONST_Button_Index_4002 //Show Vehicle Map Screen
					SET_BIT(iBS, SEC_BS_SIDE_PANEL_SHOWING)
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] Show vehicle map screen")
				BREAK
				CASE iCONST_Button_Index_4003 //Accept button on map screen popup/overlay
					currentButtonPressStage = inputConsumed_VEH_STAT_ACCEPT
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] overlay scrren accept")
					
					IF NOT IS_BIT_SET(iBS, SEC_BS_IE_TRANSACTION_FAILED)
						PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Success", "GTAO_Exec_SecuroServ_Computer_Sounds")
					ENDIF
					
					CLEAR_BIT(iBS, SEC_BS_IE_TRANSACTION_FAILED)
					
					CLOSE_OVERLAY_SCREEN()
				BREAK
				CASE iCONST_Button_Index_4004 //Cancel button on map screen popup/overlay
					currentButtonPressStage = inputConsumed_VEH_STAT_CANCEL
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [STATS] overlay scrren cancel")
					PLAY_SOUND_FRONTEND(-1, "Popup_Cancel", "GTAO_Exec_SecuroServ_Computer_Sounds")
					CLOSE_OVERLAY_SCREEN()
				BREAK
				
				/////////////////////////////////////////////
				///    	Vehicle map
				///
				CASE iCONST_Button_Index_5003 //IE WH map page - Filter button: All
					SET_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				BREAK
				CASE iCONST_Button_Index_5004 //IE WH map page - Filter button: Owned
					CLEAR_BIT(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
				BREAK
				CASE iCONST_Button_Index_5005 //Buy Vehicle Warehouse
				CASE iCONST_Button_Index_4018
					currentButtonPressStage = inputConsumed_BUY_VEH_WH
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Buy vehicle WH")
				BREAK
				CASE iCONST_Button_Index_5006 //Steal Vehicle
					currentButtonPressStage = inputConsumed_MAP_STEAL_VEHICLE
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] Steal vehicle")
				BREAK
				CASE iCONST_Button_Index_5007 //Accept button on map screen popup/overlay
					currentButtonPressStage = inputConsumed_VEH_MAP_ACCEPT
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] overlay scrren accept")
					
					IF NOT IS_BIT_SET(iBS, SEC_BS_IE_TRANSACTION_FAILED)
						PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Success", "GTAO_Exec_SecuroServ_Computer_Sounds")
					ENDIF
					
					CLEAR_BIT(iBS, SEC_BS_IE_TRANSACTION_FAILED)					
					CLOSE_OVERLAY_SCREEN()
				BREAK
				CASE iCONST_Button_Index_5008 //Cancel button on map screen popup/overlay
					currentButtonPressStage = inputConsumed_VEH_MAP_CANCEL
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] overlay scrren cancel")
					PLAY_SOUND_FRONTEND(-1, "Popup_Cancel", "GTAO_Exec_SecuroServ_Computer_Sounds")
					CLOSE_OVERLAY_SCREEN()
				BREAK
				CASE iCONST_Button_Index_5010 //Buy IE WH (Brings up the interiors overlay)
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal != 0
					AND GET_MP_INT_CHARACTER_STAT(MP_STAT_OWNED_IE_WAREHOUSE) != 0
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS Requesting IE warehouse purchase when our warehouse isn't empty. Runing confirm screen overlay")
						START_OVERLAY_SCREEN("WHOUSE_PURFA", "SEC_WHOUSE_NEMP", "WHOUSE_CONF", "", FALSE)
						CLEAR_BIT(iBS, SEC_BS_PRUCHASE_IE_WH_SELECTED)
					ELSE
						eSelectedInterior = IE_INTERIOR_BASIC
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH_MAP] Buy IE WH")
						START_OVERLAY_SCREEN("IEWH_SEL_INTT", "IEWH_SEL_INTD", "WHOUSE_CONF", "WHOUSE_CANC", TRUE, TRUE)
					ENDIF
				BREAK
				CASE iCONST_Button_Index_5011 //Renovate IE WH (Brings up the interiors overlay)
				CASE iCONST_Button_Index_4023
					eSelectedInterior 			= GET_PLAYERS_OWND_IE_WAREHOUSE_INTERIOR(PLAYER_ID())
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH_MAP] Renovate IE WH")
					START_OVERLAY_SCREEN("IEWH_SEL_INTT", "IEWH_SEL_INTD", "WHOUSE_CONF", "WHOUSE_CANC", TRUE, TRUE)
				BREAK
				CASE iCONST_Button_Index_5012
				CASE iCONST_Button_Index_4015
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH] Interior 1")
					eSelectedInterior = IE_INTERIOR_BASIC
				BREAK
				CASE iCONST_Button_Index_5013
				CASE iCONST_Button_Index_4016
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH] Interior 2")
					eSelectedInterior = IE_INTERIOR_URBAN
				BREAK
				CASE iCONST_Button_Index_5014
				CASE iCONST_Button_Index_4017
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH] Interior 3")
					eSelectedInterior = IE_INTERIOR_BRANDED
				BREAK
				CASE iCONST_Button_Index_5015
				CASE iCONST_Button_Index_4019
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [IE_WH] Cancel button")
					eSelectedInterior = IE_INTERIOR_BASIC
					CLOSE_OVERLAY_SCREEN()
				BREAK
				
				////////////////////////////////////////////
				///    	Special vehicle missions
				///    	
				CASE iCONST_Button_Index_6002	 //Accept button on map screen popup/overlay
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] SVM confirm")
					currentButtonPressStage = inputConsumed_SVM_MAP_ACCEPT
					
					CLOSE_OVERLAY_SCREEN()
				BREAK
				
				CASE iCONST_Button_Index_6003	 //Cancel button on map screen popup/overlay
					currentButtonPressStage = inputConsumed_SVM_MAP_CANCEL
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] SVM cancel")
					PLAY_SOUND_FRONTEND(-1, "Popup_Cancel", "GTAO_Exec_SecuroServ_Computer_Sounds")
					CLOSE_OVERLAY_SCREEN()
				BREAK
				
				CASE iCONST_SVM_Button_Index_0	 //Special vehicle mission 1
				CASE iCONST_SVM_Button_Index_1	 //Special vehicle mission 2
				CASE iCONST_SVM_Button_Index_2	 //Special vehicle mission 3
				CASE iCONST_SVM_Button_Index_3	 //Special vehicle mission 4
				CASE iCONST_SVM_Button_Index_4	 //Special vehicle mission 5
				CASE iCONST_SVM_Button_Index_5	 //Special vehicle mission 6
				CASE iCONST_SVM_Button_Index_6	 //Special vehicle mission 7
				CASE iCONST_SVM_Button_Index_7	 //Special vehicle mission 8
					IF iCurrentPageID = ciSVMPageID
						CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS [MAP] SVM screen selected vehicle: ", iButtonIndex)
						currentButtonPressStage = inputConsumed_SVM_LAUNCH
						iSVMToLaunch 			= iButtonIndex
					ENDIF
				BREAK
				DEFAULT
					CPRINTLN(DEBUG_INTERNET, "<SECURO> CHECK_BUTTON_PRESS unknown button index ", iButtonIndex, ", warehouseID: ", iCurrentSelectedWarehouse)
				BREAK
			ENDSWITCH
		ELSE
			#IF IS_DEBUG_BUILD
				str2 = "selection and warehouse index not ready..."
			#ENDIF
		ENDIF
	ENDIF

	//Handle the control input receivied above
	HANDLE_SECURO_SVM_INPUTS(currentButtonPressStage)
	HANDLE_SECURO_WAREHOUSE_INPUTS(currentButtonPressStage)
	HANDLE_SECURO_IE_WAREHOUSE_INPUTS(currentButtonPressStage)	
	
	#IF IS_DEBUG_BUILD
	IF bDrawDebugStuff
		GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str1, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.1,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
		
		IF NOT IS_STRING_NULL_OR_EMPTY(str2)
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_2D(str2, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.1,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
		ENDIF
		
		INT iIter
		FOR iIter = 0 TO (ciMaxOwnedWarehouses-1)
			INT iWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iIter)
					
			STRING sWarehouse
			TEXT_LABEL_31 tlCurrentContrabandTotal
			TEXT_LABEL_15 tlCurrentSlot = "Slot "
			tlCurrentSlot += iIter
			
			IF iWarehouse > 0
				//Prevent asserts when we don't own a warehouse in this slot
				sWarehouse = DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouse)
				tlCurrentContrabandTotal 	= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
				tlCurrentContrabandTotal	+= " / "
				tlCurrentContrabandTotal	+= GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
				tlCurrentContrabandTotal 	+= " Units"
			ELSE
				sWarehouse = DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouse)
				tlCurrentContrabandTotal = "0 Units"
			ENDIF
			
			FLOAT fColumn = TO_FLOAT(iColumn)
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_2D(tlCurrentSlot, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.1,0.0,0>>*fColumn), iRed, iGreen, iBlue, iAlpha)	fColumn += 0.5
		
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_2D(sWarehouse, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.1,0.0,0>>*fColumn), iRed, iGreen, iBlue, iAlpha)	fColumn += 1.5
			
//			IF iWarehouse > -1
//			AND IS_WAREHOUSE_FULL(iWarehouse)
//				SET_TEXT_COLOUR(255, 0, 0, 255)
//			ENDIF
			GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, iStrOffset), iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_2D(tlCurrentContrabandTotal, <<0.3,0.2,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.1,0.0,0>>*fColumn), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
		ENDFOR
	ENDIF
	#ENDIF
ENDPROC

PROC SECURO_PRINT_TUTORIAL_HELP(STRING sStringToDisplay, INT HelpTime)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(sStringToDisplay)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, HelpTime)
ENDPROC

/// PURPOSE:
///    IF the sidebar is open this will return the warehouse being looked at
FUNC INT GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR()	
	RETURN iCurrentSelectedWarehouse
ENDFUNC

/// PURPOSE:
///    IF the sidebar is open this will return true if the player
///    owns the displayed warehouse
FUNC BOOL DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
	IF GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR() > ciW_Invalid
	AND GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR() < iWarehouseOffset
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the conditions are met to show the special item sidebar
FUNC BOOL SHOULD_DISPLAY_SPECIAL_ITEM_TUTORIAL()

	IF iCurrentPageID != ciWarehouseMapPageID
		RETURN FALSE
	ENDIF
	
	INT iWarehouse = GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR()
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_HELP_TEXT_TUTORIAL_COMPLETE)
	AND iWarehouse > ciW_Invalid
	AND IS_MAP_SIDEBAR_OPEN()
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
	AND g_bSpecialItemActive
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_LIN_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_MAP_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_PURCH_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_BUY_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SUM_TUT)
	//AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_START_MIS)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_NU_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_WH_OLD_USER_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_STAT_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_PUR_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_RENO_TUT)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SVM_TUT)
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED TRUE")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED FALSE")
	RETURN FALSE
ENDFUNC

FUNC STRING SECURO_TUT_GET_ROOT_LABEL(INT iTutorialID)
	IF IS_BIT_SET(iBS2, SEC_BS2_PA_IS_MALE)
		SWITCH iTutorialID
			CASE 0	RETURN "EXCPA_LOGINM"	//Login
			CASE 1	RETURN "EXCPA_MAPM"		//Map Page
			CASE 2	RETURN "EXCPA_WAREM"	//Confirm Warehouse Purchase
			CASE 3	RETURN "EXCPA_BUYINM"	//Buying Contraband Stock
			CASE 4	RETURN "EXCPA_STOCKM"	//Confirm Stock Purchase
			CASE 5	RETURN "EXCPA_SUMMM"	//Stats Page
			CASE 6	RETURN "IMPA_TUT1M"		//3 option page (existing user)
			CASE 7	RETURN "IMPA_TUT1BM"	//3 option page	(New User)
			CASE 8	RETURN "IMPA_TUT2M"		//Special cargo page (existing user)
			CASE 9	RETURN "IMPA_TUT3M"		//Special vehicle work page
			CASE 10	RETURN "IMPA_TUT4M"		//Vehicle cargo page
			CASE 11	RETURN "IMPA_TUT5M"		//First purchase of a vehicle WH
			CASE 12	RETURN "IMPA_TUT6M"		//First entry to Site once we have purcased a vehicle WH
			CASE 13 RETURN "IMPA_TUT7M"		//Renovate reminder
		ENDSWITCH
	ELSE
		SWITCH iTutorialID
			CASE 0	RETURN "EXCPA_LOGINF"
			CASE 1	RETURN "EXCPA_MAPF"
			CASE 2	RETURN "EXCPA_WAREF"
			CASE 3	RETURN "EXCPA_BUYINF"
			CASE 4	RETURN "EXCPA_STOCKF"
			CASE 5	RETURN "EXCPA_SUMMF"
			CASE 6	RETURN "IMPA_TUT1F"
			CASE 7	RETURN "IMPA_TUT1BF"
			CASE 8	RETURN "IMPA_TUT2F"
			CASE 9	RETURN "IMPA_TUT3F"
			CASE 10	RETURN "IMPA_TUT4F"
			CASE 11	RETURN "IMPA_TUT5F"
			CASE 12	RETURN "IMPA_TUT6F"
			CASE 13	RETURN "IMPA_TUT7F"
		ENDSWITCH
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "SECURO_TUT_GET_ROOT_LABEL: Invalid tutorial ID: ", iTutorialID)	
	RETURN "EXCPA_LOGINM"
ENDFUNC

PROC SECURO_START_TUTORIAL_DIALOGUE(INT iTutorialID)
	STRING sRootID = "EXCPAAU"
	
	IF iTutorialID > 5
		sRootID = "IMPAAUD"
	ENDIF
	
	IF CREATE_CONVERSATION(tutPedStruct, sRootID, SECURO_TUT_GET_ROOT_LABEL(iTutorialID), CONV_PRIORITY_HIGH)
		CPRINTLN(DEBUG_INTERNET, "<SECURO> Created tutorial converstion: ", iTutorialID)
		iTutorialDialogueID	= -1
	ENDIF
ENDPROC

PROC SECURO_RUN_TUTORIAL_DIALOGUE()
	//Start the dialogue
	IF iTutorialDialogueID != -1
		SECURO_START_TUTORIAL_DIALOGUE(iTutorialDialogueID)
	ENDIF
ENDPROC

PROC RUN_SECURO_APP_DIALOGUE_TUTORIAL()

	BOOL bStartNewDialogue
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_BIT_SET(iBS, SEC_BS_DISPLAYING_LOGIN_PAGE)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_LIN_TUT)
			//Login page
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 6750
			iTutorialDialogueID	= 0
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_LIN_TUT, TRUE)
		ELIF iCurrentPageID = ciWarehouseMapPageID
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_MAP_TUT)
			//Map Page
			IF NOT DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE() //Dont trigger dialogue if player already owns a warehouse B*7218537
				bStartNewDialogue	= TRUE
				iTutorialHelpTime 	= 8000
				iTutorialDialogueID	= 1
			ENDIF
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_MAP_TUT, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_WH_OLD_USER_TUT, TRUE)
		ELIF IS_BIT_SET(iBS, SEC_BS_DISPLAYING_PURCHASE_WH_TUT)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_PURCH_TUT)
			//Successful WH purchase
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 6000
			iTutorialDialogueID	= 2
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_PURCH_TUT, TRUE)
		ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_BUY_TUT)
		AND (DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
		OR IS_MAP_SIDEBAR_OPEN() AND iJustPurchasedWHID = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(0) AND iJustPurchasedWHID != ciW_Invalid AND iCurrentSelectedWarehouse = -1) 
			//Buy Contraband tutorial
			iJustPurchasedWHID 	= ciW_Invalid
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 9000
			iTutorialDialogueID	= 3
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_BUY_TUT, TRUE)
		ELIF iCurrentPageID = ciWarehouseStatsPageID
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SUM_TUT)
			//Stats page
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 6000
			iTutorialDialogueID	= 5
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SUM_TUT, TRUE)
			
		ELIF iCurrentPageID = ciBranchPageID
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_TUT)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_NU_TUT)
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_MAP_TUT)
				//Branching page - Existing user
				bStartNewDialogue	= TRUE
				iTutorialHelpTime 	= 10000
				iTutorialDialogueID	= 6
			ELSE
				//Branching page - New user
				bStartNewDialogue	= TRUE
				iTutorialHelpTime 	= 10000
				iTutorialDialogueID	= 7
			ENDIF
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_TUT, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_NEWF_NU_TUT, TRUE)
			
		ELIF iCurrentPageID = ciSVMPageID
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SVM_TUT)
			//SVM page - all users
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 4000
			iTutorialDialogueID	= 9
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SVM_TUT, TRUE)
			
		ELIF iCurrentPageID = ciVehicleWHMapPageID
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_TUT)
			//Vehicle WH map page - all users
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 4000
			iTutorialDialogueID	= 10
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_TUT, TRUE)
			
		ELIF iCurrentPageID = ciVehicleWHMapPageID
		AND IS_BIT_SET(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_PUR_TUT)
			//Purchased WH - all users
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 4000
			iTutorialDialogueID	= 11
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_PUR_TUT, TRUE)
		
		ELIF iCurrentPageID = ciVehicleWHStatsPageID
		AND NOT IS_BIT_SET(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_STAT_TUT)
			//Purchased WH - after closing app and loging back in
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 4000
			iTutorialDialogueID	= 12
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_VEH_WH_STAT_TUT, TRUE)
			
		ELIF iCurrentPageID = ciVehicleWHStatsPageID
		AND NOT IS_BIT_SET(iBS, SEC_BS_PROPERTY_PURCHASED_THIS_RUN)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_RENO_TUT)
			//Renovate WH - after closing app and loging back in 
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 4000
			iTutorialDialogueID	= 13
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_RENO_TUT, TRUE)
			
		ELIF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_MAP_TUT)
		AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_SUM_TUT)
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_WH_OLD_USER_TUT)
			IF iCurrentPageID = ciWarehouseStatsPageID
			OR iCurrentPageID = ciWarehouseMapPageID
				//Special cargo page - Existing user
				bStartNewDialogue	= TRUE
				iTutorialHelpTime 	= 4000
				iTutorialDialogueID	= 8
				
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_WH_OLD_USER_TUT, TRUE)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF bStartNewDialogue 
		SET_BIT(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)		
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_DIALOGUE_TUTORIAL Running tutorial for: ", iTutorialHelpTime, " seconds. Tutorial ID: ", iTutorialDialogueID)
	ELIF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
		IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
			START_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_DIALOGUE_TUTORIAL Starting timer time: ", iTutorialHelpTime, " Tutorial ID: ", iTutorialDialogueID)
		ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
			iTutorialHelpTime = 0
			RESET_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_DIALOGUE_TUTORIAL Starting timer expired time: ", iTutorialHelpTime, " Tutorial ID: ", iTutorialDialogueID)
			
			//Once all the tutorial are complete we no longer need this function
			IF HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
				SET_BIT(iBS, SEC_BS_DIALOGUE_TUTORIAL_COMPLETE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RUN_WAREHOUSE_APP_TUTORIAL()

	STRING sHelp
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
	AND NOT IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
		IF GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_1) < 3
		AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_1))
		
			sHelp 				= "WH_TUT_1"									//First message
			iTutorialHelpTime 	= 5500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_1))
			
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_1)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_1, iNumTimes)
			
		ELIF GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_2) < 3
		AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_2))
		
			sHelp 				= "WH_TUT_2"									//Second message
			iTutorialHelpTime 	= 5500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_2))
			
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_2)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_2, iNumTimes)
			
		ELIF GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_3) < 3
		AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_3))
		
			sHelp 				= "WH_TUT_3"									//Third Message
			iTutorialHelpTime 	= 5500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_3))
			
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_3)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_3, iNumTimes)
			
		ELIF GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_SPECIAL) < 3
		AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_SPEC))
		AND DOES_WAREHOUSE_CONTAIN_A_SPECIAL_ITEM(iCurrentlyInsideWarehouse)
		
			sHelp 				= "WH_TUT_SPEC"									//Special message
			iTutorialHelpTime 	= 5500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_WH_TUT_SPEC))
			
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_SPECIAL)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_WH_TUTORIAL_SPECIAL, iNumTimes)
			
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sHelp)
	AND NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
		//Display the relevant help text
		SECURO_PRINT_TUTORIAL_HELP(sHelp, iTutorialHelpTime)
		SET_BIT(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
		
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_WAREHOUSE_APP_TUTORIAL Printing: sHelp: ", sHelp, " time: ", iTutorialHelpTime)
	ELIF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
		IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
			START_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_WAREHOUSE_APP_TUTORIAL Starting timer for: ", sHelp, " time: ", iTutorialHelpTime)
		ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
			iTutorialHelpTime 			= 0
			RESET_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_WAREHOUSE_APP_TUTORIAL Starting timer expired for: ", sHelp, " time: ", iTutorialHelpTime)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_SECURO_HELP_TUTORIALS_BEEN_COMPLETED()
	IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP) 				>= 3	//Map page buy first WH
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_WH) 			>= 3	//Buy WH
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP_OWNED_WH) 		>= 3	//Map page Green icons
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_CONTRABAND) 	>= 3	//Buy Contraband tutorial
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SUMMARY) 			>= 3	//Summary
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SPECIAL_ITEM) 		>= 3	//Special
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE_2)	
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SOURCE_VEH)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SUM_PAGE)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SOURCE_VEH_CD)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SVM_PAGE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWEBSITE_TUTORIAL_BS_INDEX eTutorialID)
	
	INT iTutorialID = ENUM_TO_INT(eTutorialID)
	
	SWITCH eTutorialID
		CASE eWTBS_SECURO_MAP_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP) < 3					//Map page buy first WH
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_SECURO_WH_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_WH) < 3					//Buy WH
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_SECURO_PURCH_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP_OWNED_WH) < 3			//Map page Green icons
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_SECURO_BUY_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_CONTRABAND) < 3			//Buy Contraband tutorial
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS,iTutorialID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_SECURO_SUM_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SUMMARY) < 3				//Summary
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_SECURO_SPEC_TUT
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SPECIAL_ITEM) < 3			//Special
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND SHOULD_DISPLAY_SPECIAL_ITEM_TUTORIAL()
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT1
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND IS_BIT_SET(iBS2, SEC_BS2_VEH_WH_MAP_SHOWING_ALL_WAREHOUSES)
			AND iCurrentPageID = ciVehicleWHMapPageID
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT2
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE_2)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT3))
			AND iCurrentPageID = ciVehicleWHMapPageID
			AND NOT IS_MAP_SIDEBAR_OPEN()
			AND DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT3
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SOURCE_VEH)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND iCurrentPageID = ciVehicleWHMapPageID
			AND IS_MAP_SIDEBAR_OPEN()
			AND DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT4
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SUM_PAGE)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND iCurrentPageID = ciVehicleWHStatsPageID
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT5
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SOURCE_VEH_CD)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_EYE_IN_THE_SKY)
				IF iCurrentPageID = ciVehicleWHStatsPageID
				OR iCurrentPageID = ciVehicleWHMapPageID
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE eWTBS_IE_SEC_TUT6
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SVM_PAGE)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND iCurrentPageID = ciSVMPageID
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_IE_WEBSITE_TUTORIAL_BS()
	iIETutorialBS1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_IMP_EXP_COMP_TUTORIALS1)
	iIETutorialBS2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_IMP_EXP_COMP_TUTORIALS2)
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
PROC CLEAR_DISPLAYED_TUTORIAL_HELP()
	CLEAR_HELP()
	CLEAR_BIT(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
	iTutorialHelpTime = 0
	RESET_NET_TIMER(sTutorialTimer)
ENDPROC

PROC RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL()
	
	STRING sHelp
	
	IF IS_PLAYER_ON_MP_INTRO()	
		IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		AND NOT IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
			SWITCH iCurrentPageID
				CASE ciBranchPageID
					IF NOT IS_BIT_SET(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_1)
						sHelp 				= "G9_SEC_TUT_1"					//Login Page
						iTutorialHelpTime 	= 10000
						SET_BIT(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_1)
					ENDIF
				BREAK
				CASE ciWarehouseStatsPageID
					IF NOT IS_BIT_SET(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_2)
						sHelp 				= "G9_SEC_TUT_2"					//Stats Page
						iTutorialHelpTime 	= 20000
						SET_BIT(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_2)
					ENDIF
				BREAK
				CASE ciWarehouseMapPageID
					IF NOT IS_BIT_SET(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_3)
					AND NOT IS_MAP_SIDEBAR_OPEN()
						IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
							START_NET_TIMER(sTutorialTimer)
							CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL Starting timer to delay warehouse prompt")
						ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, 500)
							sHelp 				= "G9_SEC_TUT_3"					//Warehouse Page
							iTutorialHelpTime 	= 10000
							SET_BIT(iBS2, SEC_BS2_DONE_MP_INTRO_HELP_3)
							RESET_NET_TIMER(sTutorialTimer)
						ENDIF
					ELSE
						RESET_NET_TIMER(sTutorialTimer)
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			SWITCH iCurrentPageID
				CASE ciBranchPageID
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_3")
						CLEAR_DISPLAYED_TUTORIAL_HELP()
					ENDIF
				BREAK
				CASE ciWarehouseStatsPageID
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_1")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_3")
						CLEAR_DISPLAYED_TUTORIAL_HELP()
					ENDIF
				BREAK
				CASE ciWarehouseMapPageID
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_1")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_2")
					OR IS_MAP_SIDEBAR_OPEN()
						CLEAR_DISPLAYED_TUTORIAL_HELP()
					ENDIF
				BREAK
				DEFAULT
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_1")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("G9_SEC_TUT_3")
						CLEAR_DISPLAYED_TUTORIAL_HELP()
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sHelp)
	AND NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		//Display the relevant help text
		SECURO_PRINT_TUTORIAL_HELP(sHelp, iTutorialHelpTime)
		SET_BIT(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL Printing: sHelp: ", sHelp, " time: ", iTutorialHelpTime)
	ELIF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
			START_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL Starting timer for: ", sHelp, " time: ", iTutorialHelpTime)
		ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
			iTutorialHelpTime 				= 0
			RESET_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL Starting timer expired for: ", sHelp, " time: ", iTutorialHelpTime)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC RUN_SECURO_APP_HELP_TEXT_TUTORIAL()

	STRING sHelp
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
	AND NOT IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
		IF iCurrentPageID = ciWarehouseMapPageID
		AND SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_MAP_TUT) 
		
			sHelp 				= "SECURO_MAP_TUT"						//Map Page
			iTutorialHelpTime 	= 8000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_MAP_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP, iNumTimes)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_WH_TUT) 
		AND IS_MAP_SIDEBAR_OPEN()
		AND NOT DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
		
			sHelp 				= "SECURO_WH_TUT"						//Buy WH
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_WH_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_WH)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_WH, iNumTimes)
			
		ELIF iCurrentPageID = ciWarehouseMapPageID
		AND DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE()
		AND NOT IS_BIT_SET(iBS, SEC_BS_JUST_PURCHASED_WAREHOUSE)
		AND SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_PURCH_TUT)
		
			sHelp 				= "SECURO_PURCH_TUT"					//Map page Green icons
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_PURCH_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP_OWNED_WH)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_MAP_OWNED_WH, iNumTimes)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_BUY_TUT) 
		AND DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
		AND IS_MAP_SIDEBAR_OPEN()
		
			sHelp 				= "SECURO_BUY_TUT"						//Buy Contraband tutorial
			iTutorialHelpTime 	= 9000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_BUY_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_CONTRABAND)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_BUY_CONTRABAND, iNumTimes)
			
		ELIF iCurrentPageID = ciWarehouseStatsPageID
		AND SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_SUM_TUT)
		
			sHelp 				= "SECURO_SUM_TUT"						//Stats page
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_SUM_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SUMMARY)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SUMMARY, iNumTimes)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_SECURO_SPEC_TUT)
		
			sHelp 				= "SECURO_SPEC_TUT"						//OOOHHHH a special item!
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_SECURO_SPEC_TUT))
			INT iNumTimes = GET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SPECIAL_ITEM)
			iNumTimes ++
			SET_PACKED_STAT_INT(PACKED_MP_INT_SECURO_TUT_SPECIAL_ITEM, iNumTimes)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT1)
		
			sHelp 				= "IE_SEC_TUT1"						//Vehicle WH map page 1
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT1))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT2)
		
			sHelp 				= "IE_SEC_TUT2"						//Vehicle WH map page 2
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT2))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_MAP_PAGE_2)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT3)
		
			sHelp 				= "IE_SEC_TUT3"						//Vehicle WH Source vehicle button
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT3))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SOURCE_VEH)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT4)
		
			sHelp 				= "IE_SEC_TUT4"						//Vehicle WH summary page
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT4))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SUM_PAGE)
		
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT5)
		
			sHelp 				= "IE_SEC_TUT5"						//Vehicle WH Steal mission on CD
			iTutorialHelpTime 	= 6000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT5))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_VWH_SOURCE_VEH_CD)
			
		ELIF SHOULD_SECURO_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_IE_SEC_TUT6)
		
			sHelp 				= "IE_SEC_TUT6"						//SVM page
			iTutorialHelpTime 	= 9000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_IE_SEC_TUT6))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_SVM_PAGE)
			
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sHelp)
	AND NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		//Display the relevant help text
		SECURO_PRINT_TUTORIAL_HELP(sHelp, iTutorialHelpTime)
		SET_BIT(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		
		CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_TUTORIAL Printing: sHelp: ", sHelp, " time: ", iTutorialHelpTime)
	ELIF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
			START_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_TUTORIAL Starting timer for: ", sHelp, " time: ", iTutorialHelpTime)
		ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
			iTutorialHelpTime 				= 0
			RESET_NET_TIMER(sTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<SECURO_TUT> RUN_SECURO_APP_HELP_TEXT_TUTORIAL Starting timer expired for: ", sHelp, " time: ", iTutorialHelpTime)
			
			IF HAVE_ALL_SECURO_HELP_TUTORIALS_BEEN_COMPLETED()
				SET_BIT(iBS, SEC_BS_HELP_TEXT_TUTORIAL_COMPLETE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RENDER_NAG_SCREEN()
	CPRINTLN(DEBUG_INTERNET, "APP_SECURO RENDER_NAG_SCREEN()")
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	FE_WARNING_FLAGS iButtonFlagBitfield = FE_WARNING_OKCANCEL
	STRING pBodySubTextLabel = NULL
	
	pHeaderTextLabel = ("BRSCRWTEX")			//title
	IF NOT HAS_ENTERED_OFFLINE_SAVE_FM()
		SWITCH g_eBrosNagReason
			CASE NSR_CASH
				pBodyTextLabel = ("BRDISTEX")	//warning
			BREAK
			CASE NSR_DLC
				pBodyTextLabel = ("BRDISDLC")	//warning
			BREAK
		ENDSWITCH
		pBodySubTextLabel = ("BRSHETEX")		//warning
	ELSE
		SWITCH g_eBrosNagReason
			CASE NSR_CASH
				pBodyTextLabel = ("BRDISTES")	//warning
			BREAK
			CASE NSR_DLC
				pBodyTextLabel = ("BRDISDLC")	//warning
			BREAK
		ENDSWITCH
		pBodySubTextLabel = ("BRSHETES")		//warning
	ENDIF
	SET_WARNING_MESSAGE_WITH_HEADER(pHeaderTextLabel, pBodyTextLabel, iButtonFlagBitfield, pBodySubTextLabel)
	
	g_sBusAppManagement.iRenderHandShakeFC = GET_FRAME_COUNT()
ENDPROC

PROC SECURO_SETUP_TUTORIAL_DIALOGUE()
	SET_USE_DLC_DIALOGUE(TRUE)
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_PERSONNEL, -1) = 1
		SET_BIT(iBS2, SEC_BS2_PA_IS_MALE)
	ENDIF
	
	IF IS_BIT_SET(iBS2, SEC_BS2_PA_IS_MALE)
		ADD_PED_FOR_DIALOGUE(tutPedStruct, 3, NULL, "EXECPA_MALE")
	ELSE
		ADD_PED_FOR_DIALOGUE(tutPedStruct, 2, NULL, "EXECPA_FEMALE")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<SECURO> SECURO_SETUP_TUTORIAL_DIALOGUE Setup PA voice. MALE? ", IS_BIT_SET(iBS2, SEC_BS2_PA_IS_MALE))
ENDPROC

PROC RUN_CHECK_ON_ACTIVE_BUY_MISSION_COOLDOWNS()
	IF iCooldownExpierdCheckFC = 180
		INT iLoop
		REPEAT ciMaxOwnedWarehouses iLoop
			INT iWH = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iLoop)
			
			IF iWH != ciW_Invalid
			AND IS_BIT_SET(iWarehouseSellCooldownBS, iWH)
				INT buyTimeRem = GET_CONTRABAND_BUY_COOLDOWN(iWH)

				//If necessary grab the time remaining from the sell mission tuneable timer
				IF buyTimeRem <= 0
					IF NOT GB_HAS_BUY_MISSION_TIMER_EXPIRED()
						buyTimeRem = g_sMPTunables.iexec_buy_fail_cooldown - ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), g_sGbWorkCoolDownVars.gbBuyCooldownTimer.Timer))
					ENDIF
				ENDIF
				
				IF buyTimeRem <= 0
					CLEAR_BIT(iWarehouseSellCooldownBS, iWH)
				ENDIF
			ENDIF
		ENDREPEAT
		iCooldownExpierdCheckFC = -1
	ENDIF
	
	iCooldownExpierdCheckFC ++
ENDPROC

PROC RUN_MISSION_COOLDOWN_HELP_TEXT()

	IF IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
		IF NOT HAS_NET_TIMER_STARTED(sTutorialTimer)
			START_NET_TIMER(sTutorialTimer)
		ELIF HAS_NET_TIMER_EXPIRED(sTutorialTimer, 5000)			
			CLEAR_BIT(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
			RESET_NET_TIMER(sTutorialTimer)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_PLAYING_TUTORIAL)
		AND NOT IS_BIT_SET(iBS, SEC_BS_SELL_COOLDOWN_HELP_PLAYED)
		AND IS_BIT_SET(iBS, SEC_BS_SELL_MISSION_ON_COOLDOWN)
			SECURO_PRINT_TUTORIAL_HELP("SECURO_SELL_CD", 5000)
			SET_BIT(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
			SET_BIT(iBS, SEC_BS_SELL_COOLDOWN_HELP_PLAYED)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		AND NOT IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
			
			RUN_CHECK_ON_ACTIVE_BUY_MISSION_COOLDOWNS()
			
			IF IS_MAP_SIDEBAR_OPEN()
				IF NOT IS_BIT_SET(iBS, SEC_BS_BUY_COOLDOWN_HELP_PLAYED)
				AND IS_BIT_SET(iBS, SEC_BS_BUY_MISSION_ON_COOLDOWN)
				AND DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
					IF IS_BIT_SET(iWarehouseSellCooldownBS, GET_WAREHOUSE_ID_FOR_OPEN_BUY_SIDE_BAR())
						SECURO_PRINT_TUTORIAL_HELP("SECURO_BUY_CD", 5000)
						SET_BIT(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
						SET_BIT(iBS, SEC_BS_BUY_COOLDOWN_HELP_PLAYED)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iBS, SEC_BS_SELL_COOLDOWN_HELP_PLAYED)
				AND NOT IS_BIT_SET(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
				AND IS_BIT_SET(iBS, SEC_BS_SELL_MISSION_ON_COOLDOWN)
				AND DOES_PLAYER_OWN_WAREHOUSE_OPEN_IN_SIDEBAR()
					SECURO_PRINT_TUTORIAL_HELP("SECURO_SELL_CD", 5000)
					SET_BIT(iBS, SEC_BS_COOLDOWN_HELP_DISPLAYING)
					SET_BIT(iBS, SEC_BS_SELL_COOLDOWN_HELP_PLAYED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT
	CPRINTLN(DEBUG_INTERNET, "<SECURO> started \"", GET_THIS_SCRIPT_NAME(), "\"")
	
	iBS = 0
	
	IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	#IF FEATURE_CASINO_HEIST
	OR WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
	AND g_sBusAppManagement.bRunningPrimaryApp = FALSE
	#ENDIF
		SET_BIT(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
	ENDIF
	
	IF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		mov = REQUEST_SCALEFORM_MOVIE("warehouse")
	ELSE
		mov = REQUEST_SCALEFORM_MOVIE("securoserv")
		g_bSecuroDelayOfficeChairExit = TRUE
		g_bSecuroQuickExitOfficeChair = FALSE
	ENDIF
		
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	DISABLE_CELLPHONE(TRUE)
	
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
		WAIT(0)
	ENDWHILE
	
	IF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		iCurrentlyInsideWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
		#IF FEATURE_CASINO_HEIST
		IF WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
			iCurrentlyInsideWarehouse = g_sBusAppManagement.iPropertyID
		ENDIF
		#ENDIF
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
		POPULATE_MP_SECUROSERV_SITE_PIN_MAP(TRUE)
	ELIF IS_PROPERTY_OFFICE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	#IF FEATURE_CASINO_HEIST
	OR WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
	#ENDIF
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
			POPULATE_MP_SECUROSERV_SITE_PIN_MAP(TRUE)
		ELSE
			POPULATE_MP_SECUROSERV_SITE_PIN_MAP(FALSE)
		ENDIF
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
	AND g_sBusAppManagement.bRunningPrimaryApp
		IF g_sBusAppManagement.bSecuroSpecCargo
			APP_SECURO_SHOW_PAGE(ciWarehouseStatsPageID)
		ELSE
			APP_SECURO_SHOW_PAGE(ciVehicleWHStatsPageID)
		ENDIF
	ENDIF
	#ENDIF
	
	START_AUDIO_SCENE("GTAO_Computer_Screen_Active_Scene")
	
	SET_BIT(iBS, SEC_BS_DISPLAYING_LOGIN_PAGE)	
	SET_BROWSER_OPEN(TRUE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	Pause_Objective_Text()
	
	IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
		INIT_ACTIVE_USER_LIST()
		
		IF NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
		AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		//AND NOT GB_ARE_PLAYER_BOSS_PRIVILIGES_SUSPENDED_BD(PLAYER_ID())
			ADD_PLAYER_TO_ACTIVE_ORGANISATION_LIST(PLAYER_ID(), NATIVE_TO_INT(PLAYER_ID()))
		ENDIF
	ENDIF
	
	IF HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
		SET_BIT(iBS, SEC_BS_DIALOGUE_TUTORIAL_COMPLETE)
	ENDIF
	IF HAVE_ALL_SECURO_HELP_TUTORIALS_BEEN_COMPLETED()
		SET_BIT(iBS, SEC_BS_HELP_TEXT_TUTORIAL_COMPLETE)
	ENDIF
		
	SECURO_SETUP_TUTORIAL_DIALOGUE()
	SET_SHARD_SHOULD_BE_HELD_UP_FOR_OFFICE_SEAT_ANIM(FALSE)
	
	THEFEED_PAUSE()
	
	IF IS_PC_VERSION()
		SET_MULTIHEAD_SAFE(TRUE, TRUE, TRUE, TRUE)
	ENDIF
	
	UPDATE_IE_WEBSITE_TUTORIAL_BS()
	SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARKNETACCESSED, TRUE)
	#ENDIF
		
	/// main update and render loop
	WHILE IS_BROWSER_OPEN()
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vPlayerCoords) > 5.0
			// Exit
			CPRINTLN(DEBUG_INTERNET, "<SECURO> Shutting down! Player too far from the coords")	
			CLEANUP_SECUROSERVE()
		ELIF IS_PAUSE_MENU_ACTIVE_EX()
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			CPRINTLN(DEBUG_INTERNET, "<SECURO> Shutting down! Player opend the pause menu")	
			CLEANUP_SECUROSERVE()
		ELIF IS_SYSTEM_UI_BEING_DISPLAYED()
			CPRINTLN(DEBUG_INTERNET, "<SECURO> Shutting down! IS_SYSTEM_UI_BEING_DISPLAYED")
			CLEANUP_SECUROSERVE(TRUE)
		ENDIF
		
		IF IS_BIT_SET(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
			IF NOT HAS_NET_TIMER_STARTED(stCashChangeHUDTimer)
				START_NET_TIMER(stCashChangeHUDTimer)
			ELIF HAS_NET_TIMER_EXPIRED(stCashChangeHUDTimer, 4000)
				RESET_NET_TIMER(stCashChangeHUDTimer)
				CLEAR_BIT(iBS, SEC_BS_SHOW_CASH_CHANGE_HUD)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iBS, SEC_BS_DELAY_APP_EXIT_FOR_CONVERSATION)		
			IF g_bBrowserGoToStoreTrigger
				g_bBrowserNagScreenState = TRUE
				RENDER_NAG_SCREEN()
			ELSE
				RENDER_SECUROSERVE()
			ENDIF
			
			UPDATE_IE_MISSION_COOLDOWN()
			RUN_MISSION_COOLDOWN_HELP_TEXT()
			
			IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
				IF NOT IS_BIT_SET(iBS, SEC_BS_DIALOGUE_TUTORIAL_COMPLETE)
					RUN_SECURO_APP_DIALOGUE_TUTORIAL()
					SECURO_RUN_TUTORIAL_DIALOGUE()
					#IF FEATURE_GEN9_EXCLUSIVE
					RUN_SECURO_APP_HELP_TEXT_INTRO_MISSION_TUTORIAL()
					#ENDIF
				ELIF NOT IS_BIT_SET(iBS, SEC_BS_HELP_TEXT_TUTORIAL_COMPLETE)
					RUN_SECURO_APP_HELP_TEXT_TUTORIAL()
				ENDIF
			ELSE
				IF NOT IS_WAREHOUSE_EMPTY(iCurrentlyInsideWarehouse)
					RUN_WAREHOUSE_APP_TUTORIAL()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
				PASS_WAREHOUSE_INPUTS_TO_SCALEFORM()
				CHECK_BUTTON_PRESS_FOR_WAREHOUSE_APP()
			ELSE
				PASS_INPUTS_TO_SCALEFORM()
				CHECK_BUTTON_PRESS()
			ENDIF
			
			IF NOT IS_BIT_SET(iBS, SEC_BS_CURRENTLY_INSIDE_WAREHOUSE)
				MAINTAIN_ACTIVE_ORGANIZATION_LIST()
			ENDIF
		ELSE
			IF iTutorialDialogueID = -1
			OR IS_SCRIPTED_CONVERSATION_ONGOING()
				CPRINTLN(DEBUG_INTERNET, "<SECURO> Shutting down! buy mission dialoge has started!")	
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SECURO_START_MIS, TRUE)
				CLEANUP_SECUROSERVE(TRUE)
			ENDIF
			
			CPRINTLN(DEBUG_INTERNET, "<SECURO> Shutting down! Waiting for the buy misssion started dialogue to begin")	
			SECURO_RUN_TUTORIAL_DIALOGUE()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	// Script should never reach here. Always terminate with cleanup function.
	CASSERTLN(DEBUG_INTERNET, "<SECURO> \"", GET_THIS_SCRIPT_NAME(), "\" should never reach here. Always terminate with cleanup function.")
ENDSCRIPT
