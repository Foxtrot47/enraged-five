USING "rage_builtins.sch"
USING "globals.sch"

USING "cellphone_public.sch"
USING "commands_zone.sch"

USING "website_private.sch"
USING "net_realty_warehouse.sch"
USING "net_gang_boss.sch"
USING "net_realty_vehicle_garage.sch"
USING "SVM_MISSION_FLOW.sch"
USING "net_ie_dropoff_public.sch"
USING "gb_vehicle_export_freemode_header.sch"

//***********Script CONSTS***********
CONST_INT ciMaxSelectableCars				(ciMaxIEVehicleModels - 1)
CONST_INT ciMaxSubMenuVehicles 				10
CONST_INT ciMaxVehicleCollections			10
CONST_INT ciMaxSelectableMissionVehicles 	4
CONST_INT ciVehicleSelectionOffset			10000
CONST_INT ciCollectionSelectionOffset		18000

CONST_INT ciPageID_VehicleSelection			0
CONST_INT ciPageID_Collection				1
CONST_INT ciPageID_Buyers					2

CONST_INT ciFailReasonCriticalToJob 		13
CONST_INT ciFailReasonNotBoss 				14
CONST_INT ciFailReasonBikerBoss				15
CONST_INT ciFailReasonNoWH					16
CONST_INT ciFailReasonTooFewPlayers			17
CONST_INT ciFailReasonCloudSaveFail			18
CONST_INT ciFailReasonGoonGambling			19
CONST_INT ciFailReasonGoonAnimal			20

CONST_INT ciVehicleCategoryStandard			0
CONST_INT ciVehicleCategoryMid				1
CONST_INT ciVehicleCategoryTop				2

//Bit set consts
CONST_INT AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP	0
CONST_INT AH_BS_HELP_TEXT_TUTORIAL_COMPLETE			1
CONST_INT AH_BS_CURRENTLY_PLAYING_TUTORIAL			2
CONST_INT AH_BS_DIALOGUE_TUTORIAL_COMPLETE			3
CONST_INT AH_BS_TUTORIAL_1_PLAYED_THIS_BOOT			4
CONST_INT AH_BS_TUTORIAL_2_PLAYED_THIS_BOOT			5
CONST_INT AH_BS_TUTORIAL_3_PLAYED_THIS_BOOT			6

ENUM APP_STAGES
	eAppStage_SelectingOptions,
	eAppStage_WaitingForVehicleMod,
	eAppStage_LaunchingMission
ENDENUM

ENUM IE_VEH_BUYER
	IE_BUYER_PRIVATE,
	IE_BUYER_SHOWROOM,
	IE_BUYER_SPECIALIST
ENDENUM

ENUM enumItemConsumed
	inputConsumed_0 = 1000000,
	inputConsumed_1 = 2000000,
	
	inputConsumed_OVERLAY_CONFIRM 		= 101,		//OVERLAY_ACCEPT_BUTTON:Number 
	inputConsumed_OVERLAY_CANCEL 		= 102,		//OVERLAY_CANCEL_BUTTON
	inputConsumed_EXPORT_VEHICLES		= 1002,		//Export button for the selected vehicles
	inputConsumed_BUYER_0			 	= 1007,
	inputConsumed_BUYER_1 				= 1008,
	inputConsumed_BUYER_2 				= 1009,
	
	inputConsumed_END					= 3000000
ENDENUM

//***********Script variables***********
BOOL bWaitingForCarmodEventToProcess 	= FALSE
BOOL bConfirmMissionLaunch				= FALSE
INT iMissionUnavailableReason 			= -1
INT iCurrentTabID						= ciPageID_VehicleSelection
INT iBS

//Set to a garage ID if we're currently inside an IE warehouse
IMPORT_EXPORT_GARAGES eIEGarageID = IE_GARAGE_INVALID

//The four vehicles we have selected for a sell mission
INT iSelectedMissionVehicles[ciMaxSelectableMissionVehicles]
//The four vehicles we have selected when using the vehicle collection option
INT iSelectedCollectionVehicles[ciMaxSelectableMissionVehicles]

//Players assigned to each of the 4 mission vehicle slots 
PLAYER_INDEX piAssignedPlayers[ciMaxSelectableMissionVehicles]

//Selected mission drop off point
VEHICLE_EXPORT_DROPOFF_TYPE eDropOffLocation = VEHICLE_EXPORT_DROPOFF_INVALID

//The vehicle collection we have selected if any
IE_VEHICLE_SET_ID eSelectedVehicleCollection = IE_VEH_SET_INVALID

//Scaleform movie
SCALEFORM_INDEX siMovie
SCALEFORM_RETURN_INDEX currentSelectionReturnIndex
SCALEFORM_RETURN_INDEX currentTabReturnIndex

//Input variables
enumItemConsumed currentButtonPressStage = inputConsumed_1

FLOAT iCursorX = 0.0
FLOAT iCursorY = 0.0

FLOAT iCursorXLast = 0.0
FLOAT iCursorYLast = 0.0

INT iLeftX, iLeftY
INT iRightX, iRightY
INT iIETutorialBS1 		= 0
INT iIETutorialBS2 		= 0
INT iTutorialHelpTime 	= 0
INT iTutorialDialogueID = -1

BOOL bOverlayActive = FALSE

APP_STAGES eAppStage = eAppStage_SelectingOptions
structPedsForConversation tutPedStruct

SCRIPT_TIMER stModShopTimeOut
SCRIPT_TIMER stTutorialTimer

//#IF IS_DEBUG_BUILD
//STRUCT IE_APP_DEBUG_WIDGET_VARS
//	BOOL bSelectBuyMissionVehicle			= FALSE
//	BOOL bSetIEVehicleAsOwned				= FALSE
//	BOOL bClearOwnedIEVehicles				= FALSE
//	
//	INT iVehicleSlider = 1
//	INT iStealMisVehSlider = 1
//ENDSTRUCT

//IE_APP_DEBUG_WIDGET_VARS eLocalWidgetVars
//#ENDIF

//***********Script functions***********
PROC CLEANUP_IE_APP(BOOL bDontCleanupBrowserFlag = FALSE, BOOL bEnablePlayerControl = TRUE)

	g_bIEAppVisible = bDontCleanupBrowserFlag
	
	DISABLE_CELLPHONE(FALSE)
	
	Unpause_Objective_Text()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siMovie)
	
	IF bEnablePlayerControl
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL)
		STOP_AUDIO_SCENE("GTAO_Computer_Screen_Active_Scene")
	ENDIF
	
	IF IS_PC_VERSION()
	AND IS_MULTIHEAD_FADE_UP()
		SET_MULTIHEAD_SAFE(FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	THEFEED_RESUME()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SET_CURRENT_PAGE_ID(INT iPageID)
	IF iCurrentTabID != iPageID
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_CURRENT_PAGE_ID: Moving from page ", iCurrentTabID, " To: ", iPageID)
		iCurrentTabID = iPageID
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_SELECTED_VEHICLE_SET()
	RETURN eSelectedVehicleCollection != IE_VEH_SET_INVALID
ENDFUNC

/// PURPOSE:
///    A count of the local players gang members who are insied the local players IE garage
FUNC INT GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE()
	INT i, iCount
	
	IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		iCount ++
	ENDIF
	
	REPEAT GB_MAX_GANG_GOONS i
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()
			IF IS_PLAYER_IN_IE_GARAGE(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i])
			AND IS_NET_PLAYER_OK(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i], TRUE, TRUE)
			AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i])
				iCount ++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Returns the count of vehicles we are trying to sell 
FUNC INT GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(BOOL bVehCollection)
	INT i, iCount
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF NOT bVehCollection
			IF iSelectedMissionVehicles[i] != 0
				iCount ++
			ENDIF
		ELSE
			IF iSelectedCollectionVehicles[i] != 0
				iCount ++
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Returns a string to give mission unavailable feedback
FUNC TEXT_LABEL_15 GET_MISSION_UNAVAILABLE_REASON_TEXT_LABEL(BOOL bSellingVehSet)
	
	TEXT_LABEL_15 tlReturnLabel
	
	SWITCH iMissionUnavailableReason
		CASE GB_MU_REASON_UNSUITABLE_SESSION		tlReturnLabel =  "AH_SES"			RETURN tlReturnLabel
		CASE ciFailReasonCriticalToJob				tlReturnLabel =  "AH_CRIT"			RETURN tlReturnLabel
		CASE ciFailReasonNotBoss					tlReturnLabel =  "AH_NBOSS"			RETURN tlReturnLabel
		CASE ciFailReasonBikerBoss					tlReturnLabel =  "AH_NBOSS" 		RETURN tlReturnLabel
		CASE ciFailReasonCloudSaveFail				tlReturnLabel =  "AH_SAVING_DOWN" 	RETURN tlReturnLabel
		CASE ciFailReasonTooFewPlayers
			tlReturnLabel = "AH_NEPLYRS"
			tlReturnLabel += GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(bSellingVehSet)
			RETURN tlReturnLabel
		CASE ciFailReasonGoonAnimal
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				tlReturnLabel = "GENERAL_MLF_G1"
			ELIF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
				tlReturnLabel = "GENERAL_MLF_G2"
			ELSE
				tlReturnLabel = "GENERAL_MLF_G3"
			ENDIF
			
			RETURN tlReturnLabel
		CASE ciFailReasonGoonGambling
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				tlReturnLabel = "GENERAL_MLF_G4"
			ELIF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
				tlReturnLabel = "GENERAL_MLF_G5"
			ELSE
				tlReturnLabel = "GENERAL_MLF_G6"
			ENDIF
			
			RETURN tlReturnLabel
		BREAK
	ENDSWITCH
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		tlReturnLabel = "AH_SES" RETURN tlReturnLabel
	ENDIF
	
	tlReturnLabel = "AH_SELL_NA" RETURN tlReturnLabel
ENDFUNC

/// PURPOSE:
///    Returns true if we need to block a mission launch because we can't save at the moment
FUNC BOOL BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES()
	IF g_bAllowSaveTroubleIESellMissionBloker
	AND IS_SAVING_HAVING_TROUBLE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the mission unavailable value
FUNC BOOL CAN_PLAYER_LAUNCH_MISSION(BOOL bSellingVehSet, BOOL bCheckAssignedPlayers = FALSE)
	//Check we own an IE WH
	IF NOT DOES_PLAYER_OWN_AN_IE_GARAGE(PLAYER_ID())
	#IF IS_DEBUG_BUILD
	AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowRemoteMissionLaunch")
	#ENDIF
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, no owned IE WH")
		iMissionUnavailableReason = ciFailReasonNoWH
		RETURN FALSE
	ENDIF
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_VEHICLE_EXPORT_SELL, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, mission unavailable")
		iMissionUnavailableReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_VEHICLE_EXPORT_SELL, FALSE)
		
		IF iMissionUnavailableReason = GB_MU_REASON_GOON_GAMBLING
			iMissionUnavailableReason = ciFailReasonGoonGambling
		ELIF iMissionUnavailableReason = GB_MU_REASON_GOON_IS_ANIMAL
			iMissionUnavailableReason = ciFailReasonGoonAnimal
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
	#IF IS_DEBUG_BUILD
	AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_AllowRemoteMissionLaunch")
	#ENDIF
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, GB_IS_PLAYER_BOSS_OF_A_GANG is false")
		iMissionUnavailableReason = ciFailReasonNotBoss
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG is true")
		iMissionUnavailableReason = ciFailReasonBikerBoss
		RETURN FALSE
	ENDIF
	
	// Check we're not taking part in something
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, player critical to job")
		iMissionUnavailableReason = ciFailReasonCriticalToJob
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, NETWORK_IS_ACTIVITY_SESSION is true")
		iMissionUnavailableReason = GB_MU_REASON_UNSUITABLE_SESSION
		RETURN FALSE
	ENDIF
	
	IF GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE() < GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(bSellingVehSet)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, Too few players in the building")
		iMissionUnavailableReason = ciFailReasonTooFewPlayers
		RETURN FALSE
	ENDIF
	
	IF BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES()
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed due to save failure")
		iMissionUnavailableReason = ciFailReasonCloudSaveFail
		RETURN FALSE
	ENDIF
	
	//Checks all assigned players to make sure we still have the players assigned to a vehicle
	IF bCheckAssignedPlayers
		INT i
		REPEAT MAX_NUMBER_OF_IE_PLAYERS i
			PLAYER_INDEX piPlayerToCheck = INT_TO_NATIVE(PLAYER_INDEX, g_sIEVehicleSetupStruct[i].iPlayerIndex)
			
			IF piPlayerToCheck != INVALID_PLAYER_INDEX()
				IF NOT NETWORK_IS_PLAYER_CONNECTED(piPlayerToCheck)
				OR NOT IS_NET_PLAYER_OK(piPlayerToCheck)
				OR NOT IS_PLAYER_IN_IE_GARAGE(piPlayerToCheck)
				OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(piPlayerToCheck)
					IF g_sIEVehicleSetupStruct[i].iIEPlayerVehicle = 0
					OR g_sIEVehicleSetupStruct[i].iIEPlayerVehicle = -1
						CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launching, Assigned player is not ok or not in IE WH: ", GET_PLAYER_NAME(piPlayerToCheck), " Continuing launch as they were not assigned a vehicle.")
					ELSE
						CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_LAUNCH_MISSION: mission launch failed, Assigned player is not ok: ", GET_PLAYER_NAME(piPlayerToCheck))
						iMissionUnavailableReason = ciFailReasonTooFewPlayers
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC IE_APP_CONTROL_DISABLE()
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

	IF (NOT IS_SYSTEM_UI_BEING_DISPLAYED())
	AND (NOT IS_PAUSE_MENU_ACTIVE())
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Fills a slot with a player ID to be used when launching the mission
PROC ASSIGN_GANG_MEMBER_TO_SLOT(INT iSlot, PLAYER_INDEX piGangMember)
	IF piGangMember = INVALID_PLAYER_INDEX()
		CASSERTLN(DEBUG_INTERNET, "ASSIGN_GANG_MEMBER_TO_SLOT: Trying to assign an invalid player to slot: ", iSlot)
	ENDIF
	
	piAssignedPlayers[iSlot] = piGangMember
ENDPROC

/// PURPOSE:
///    Clears the list of players selected for the mission (Sell missions)
PROC REMOVE_ALL_GANG_MEMBERS_FROM_VEH_SLOT()
	INT i
	REPEAT ciMaxSelectableMissionVehicles i
		piAssignedPlayers[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Returns a count of the gang members we have assigned to vehicles 
FUNC INT GET_COUNT_OF_ASSIGNED_GANG_MEMBERS()
	INT i, iCount
	REPEAT ciMaxSelectableMissionVehicles i
		IF piAssignedPlayers[i] != INVALID_PLAYER_INDEX()
			iCount ++
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Checks if we have already assigned the specified player to a vehicle
FUNC BOOL IS_GANG_MEMBER_ASSIGNED_TO_VEHICLE(PLAYER_INDEX piGangMember)
	INT i
	REPEAT ciMaxSelectableMissionVehicles i
		IF piAssignedPlayers[i] = piGangMember
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loops through the local players gang members to find any players we have not yet assigned to a vehicle
PROC BUILD_LIST_OF_AVAILABLE_GANG_MEMBERS(PLAYER_INDEX &piGang[])
	INT i, iSlotToAssign
	INT iMaximumPlayers = ciMaxSelectableMissionVehicles
	
	IF NOT IS_GANG_MEMBER_ASSIGNED_TO_VEHICLE(PLAYER_ID())
		piGang[iSlotToAssign] = PLAYER_ID()
		iSlotToAssign ++
		iMaximumPlayers --
	ENDIF
	
	REPEAT iMaximumPlayers i
		IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i] != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i], TRUE, TRUE)
		AND NOT IS_GANG_MEMBER_ASSIGNED_TO_VEHICLE(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i])
		AND IS_PLAYER_IN_IE_GARAGE(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i])
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i])
			piGang[iSlotToAssign] = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangMembers[i]
			iSlotToAssign ++
		ELSE
			piGang[iSlotToAssign] = INVALID_PLAYER_INDEX()
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_ALL_SELECTED_VEHICLES(BOOL bVehCollection, BOOL bSendToSF = FALSE)
	INT i
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF NOT bVehCollection
			iSelectedMissionVehicles[i] = 0
		ELSE
			iSelectedCollectionVehicles[i] = 0
		ENDIF
	ENDREPEAT
	
	IF bSendToSF
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(eSelectedVehicleCollection)))
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> REMOVE_ALL_SELECTED_VEHICLES Removed all vehicles")
ENDPROC

/// PURPOSE:
///    Returns a count of unassigned gang members
FUNC INT GET_COUNT_OF_GANG_MEMBERS_TO_ASSIGN()
	INT iAssigned = GET_COUNT_OF_ASSIGNED_GANG_MEMBERS()
	INT iGangSize = GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE()
	
	RETURN (iGangSize - iAssigned)
ENDFUNC

/// PURPOSE:
///    Adds the specified vehicle to the selected vehicle array.
///    This array is used when launching a mission
PROC ADD_SCALEFORM_SELECTED_VEHICLE(INT iVehicleID, BOOL bVehCollection)
	INT i
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF bVehCollection
			IF iSelectedCollectionVehicles[i] = 0
				iSelectedCollectionVehicles[i] = iVehicleID
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> ADD_SCALEFORM_SELECTED_VEHICLE added vehicle to collection array ", iVehicleID)
				EXIT
			ENDIF
		ELSE
			IF iSelectedMissionVehicles[i] = 0
				iSelectedMissionVehicles[i] = iVehicleID
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> ADD_SCALEFORM_SELECTED_VEHICLE added vehicle ", iVehicleID)
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
	
	CASSERTLN(DEBUG_INTERNET, "ADD_SCALEFORM_SELECTED_VEHICLE no free vehicle slots available!")
ENDPROC

/// PURPOSE:
///    Removes the specified vehicle from the selected vehicle array. Shuffles the vehicle to the end of the list first.
///    This array is used when launching a mission
///    Does not update the SF movie like: REMOVE_LAST_SF_SELECTED_VEHICLE
PROC REMOVE_SCALEFORM_SELECTED_VEHICLE(INT iVehicleID)
	INT i
	
	IF iVehicleID > ciVehicleSelectionOffset
		iVehicleID -= ciVehicleSelectionOffset
	ENDIF
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF i = (ciMaxSelectableMissionVehicles - 1)
			//Remove this vehicle
			iSelectedMissionVehicles[i] = 0
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> REMOVE_SCALEFORM_SELECTED_VEHICLE (1) Removed vehicle ", iVehicleID)
		ELIF iSelectedMissionVehicles[i] = iVehicleID
			IF iSelectedMissionVehicles[(i + 1)] != 0
				//Shuffle this one up the list to be removed at the end
				iSelectedMissionVehicles[i] = iSelectedMissionVehicles[(i + 1)]
				iSelectedMissionVehicles[(i + 1)] = iVehicleID
			ELSE
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> REMOVE_SCALEFORM_SELECTED_VEHICLE (2) Removed vehicle ", iVehicleID)
				iSelectedMissionVehicles[i] = 0
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Looks for the specified vehicle in the selected vehicle array
FUNC BOOL IS_VEHICLE_SELECTED(INT iVehicleID, BOOL bVehCollection)
	INT i
	INT iVehicleIdToCheck = iVehicleID
	
	//Check for the offset we apply when a vehicle is in the selection window
	IF iVehicleID > ciVehicleSelectionOffset
		iVehicleIdToCheck -= ciVehicleSelectionOffset
	ENDIF
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF bVehCollection
			IF iSelectedCollectionVehicles[i] = iVehicleIdToCheck
				RETURN TRUE
			ENDIF
		ELSE
			IF iSelectedMissionVehicles[i] = iVehicleIdToCheck
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Version of the function above to be used with the new SF movie.
///    Assigns the players in order of the gang members array. Always boss first.
PROC ASSIGN_GANG_MEMBERS_TO_MISSION_VEHICLES(BOOL bVehCollection)
	INT i
	PLAYER_INDEX piUnassignedPlayers[ciMaxSelectableMissionVehicles]
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF NOT bVehCollection
		AND iSelectedMissionVehicles[i] != 0
		OR bVehCollection
		AND iSelectedCollectionVehicles[i] != 0
			IF i = 0
				//Assign the boss to the first vehicle
				piAssignedPlayers[i] = PLAYER_ID()
			ELSE
				//Assign any remaining gang members so they launch the mission correctly
				BUILD_LIST_OF_AVAILABLE_GANG_MEMBERS(piUnassignedPlayers)
				piAssignedPlayers[i] = piUnassignedPlayers[0]
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Assigns the (up to) 4 vehicles from the specified collection to the mission vehicles list
PROC ASSIGN_COLLECTION_VEHICLES(IE_VEHICLE_SET_ID eCollectionID)
	IE_VEHICLE_SET_DATA eCollectionData
	
	FILL_IE_VEHICLE_SET_STRUCT(eCollectionData, eCollectionID)
	
	INT i
	
	REPEAT ciMaxSelectableMissionVehicles i
		ADD_SCALEFORM_SELECTED_VEHICLE(ENUM_TO_INT(eCollectionData.eCollectionVehicles[i]), TRUE)
	ENDREPEAT
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> ASSIGN_COLLECTION_VEHICLES added collection: ", eCollectionID)
ENDPROC

/// PURPOSE:
///    Removes the last selected vehicle from the array and sends a call to update the UI
PROC REMOVE_LAST_SF_SELECTED_VEHICLE()
	INT i
	
	FOR i = (ciMaxSelectableMissionVehicles -1) TO 0 STEP -1
		IF iSelectedMissionVehicles[i] != 0
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> REMOVE_LAST_SF_SELECTED_VEHICLE Removed vehicle ", iSelectedMissionVehicles[i])
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(iSelectedMissionVehicles[i]))
			iSelectedMissionVehicles[i] = 0
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

FUNC INT GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF eDropOff, BOOL bModCostOnly = FALSE)
	INT i, iVehicleValue
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF iSelectedMissionVehicles[i] != 0
			IE_VEHICLE_ENUM eVehicle = INT_TO_ENUM(IE_VEHICLE_ENUM, iSelectedMissionVehicles[i])
			iVehicleValue += GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle), eDropOff, bModCostOnly)
		ENDIF
	ENDREPEAT
	
	RETURN iVehicleValue
ENDFUNC

/// PURPOSE:
///    Shows a popup message over the top of the current screen
/// PARAMS:
///    sTitleBar - Top bar textlable
///    sDescription - Description text label
///    sButton1 - Left side button text label (Moves to center if "" is passed for sButton2
///    sButton2 - Pass "" to show no button. Right side button text label
PROC START_OVERLAY_SCREEN(STRING sTitleBar, STRING sDescription, String sButton1, String sButton2, INT iAdditionalINT = -1, INT iSecondAdditionalINT = -1)
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_OVERLAY")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitleBar)
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sDescription)
			IF iAdditionalINT != -1
				ADD_TEXT_COMPONENT_INTEGER(iadditionalINT)
			ENDIF
			IF iSecondAdditionalINT != -1
				ADD_TEXT_COMPONENT_INTEGER(iSecondAdditionalINT)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sButton1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sButton2)		
	END_SCALEFORM_MOVIE_METHOD()
	
	bOverlayActive = TRUE
ENDPROC

PROC CLOSE_OVERLAY_SCREEN()
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "HIDE_OVERLAY")						
	END_SCALEFORM_MOVIE_METHOD()
	
	bOverlayActive = FALSE
	
	IF bConfirmMissionLaunch
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CLOSE_OVERLAY_SCREEN setting bConfirmMissionLaunch to FALSE")
		bConfirmMissionLaunch = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_IE_VEH_BUYER_DISABLED(IE_VEH_BUYER eBuyerID)

	IF BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES()
		RETURN TRUE
	ENDIF
	
	IF eBuyerID = IE_BUYER_PRIVATE
		RETURN g_sMPTunables.bIMPEXP_SELL_DISABLE_BUYER1
	ELIF eBuyerID = IE_BUYER_SHOWROOM		
		RETURN g_sMPTunables.bIMPEXP_SELL_DISABLE_BUYER2
	ELIF eBuyerID = IE_BUYER_SPECIALIST
		RETURN g_sMPTunables.bIMPEXP_SELL_DISABLE_BUYER3
	ENDIF
	
	RETURN FALSE	
ENDFUNC

PROC SHOW_EXPORT_BUYERS_SCREEN()
	INT iPrivateBuyerOffer 		= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_PRIVATE_BUYER_1)
	INT iShowroomOffer			= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_SHOWROOM_1)
	INT iSpecialistOffer		= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_GANGSTER_1)
	INT iPrivateBuyerModCost 	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_PRIVATE_BUYER_1, TRUE)
	INT iShowroomBuyerModCost	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_SHOWROOM_1, TRUE)
	INT iSpecialistBuyerModCost	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_GANGSTER_1, TRUE)
	
	BOOL bCanAffordPrivate 		= TRUE
	BOOL bCanAffordShowroom		= TRUE
	BOOl bCanAffordSpecialist 	= TRUE
	
	IF iPrivateBuyerModCost > 0
	AND NOT NETWORK_CAN_SPEND_MONEY(iPrivateBuyerModCost, FALSE, TRUE, FALSE)
	OR IS_IE_VEH_BUYER_DISABLED(IE_BUYER_PRIVATE)
		bCanAffordPrivate = FALSE
	ENDIF
	
	IF iShowroomBuyerModCost > 0
	AND NOT NETWORK_CAN_SPEND_MONEY(iShowroomBuyerModCost, FALSE, TRUE, FALSE)
	OR IS_IE_VEH_BUYER_DISABLED(IE_BUYER_SHOWROOM)
		bCanAffordShowroom = FALSE
	ENDIF
	
	IF iSpecialistBuyerModCost > 0
	AND NOT NETWORK_CAN_SPEND_MONEY(iSpecialistBuyerModCost, FALSE, TRUE, FALSE)
	OR IS_IE_VEH_BUYER_DISABLED(IE_BUYER_SPECIALIST)
		bCanAffordSpecialist = FALSE
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_BUYERS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPrivateBuyerModCost)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iShowroomBuyerModCost)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpecialistBuyerModCost)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPrivateBuyerOffer)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iShowroomOffer)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpecialistOffer)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanAffordPrivate)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanAffordShowroom)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCanAffordSpecialist)
	END_SCALEFORM_MOVIE_METHOD()
	
	SET_CURRENT_PAGE_ID(ciPageID_Buyers)
ENDPROC

/// PURPOSE:
///    Sends data about a specific IE vehilce to SF
PROC PASS_VEHICLE_DATA_TO_SCALEFORM(IE_VEHICLE_ENUM eVehicle, BOOL bForceSelected = FALSE)
	
	INT iVehicleSaleValue 		= GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle), IE_DROPOFF_PRIVATE_BUYER_1)
	INT iVehicleCollectionValue = GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle), IE_DROPOFF_GANGSTER_1, FALSE, FALSE, TRUE)	
	INT iVehMarketValue			= GET_VEHICLE_EXPORT_VEHICLE_BASE_VALUE(GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle), IE_DROPOFF_PRIVATE_BUYER_1, FALSE, TRUE)
	INT iVehicleCategory		= ciVehicleCategoryStandard
	
	IF iVehicleSaleValue >= g_sMPTunables.iIMPEXP_SELL_BUYER1_OFFER_HARD
		iVehicleCategory = ciVehicleCategoryTop
	ELIF iVehicleSaleValue >= g_sMPTunables.iIMPEXP_SELL_BUYER1_OFFER_MED
		iVehicleCategory = ciVehicleCategoryMid
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "UPDATE_VEHICLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eVehicle)) 	// Vehicle ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehicleSaleValue) 		// Vehicle value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehicleCollectionValue) 	// Vehicle value when selling it as part of a collection
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bForceSelected)			// in case we need to force select a certain car
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) 					// Do we own it? (passing true because this is setup to only call this method for vehicles we own)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehicleCategory)			// Standard, mid range or top class?
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehMarketValue)			// The vehicles market value
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> PASS_VEHICLE_DATA_TO_SCALEFORM passing owned vehicle: ", eVehicle, " value: ", iVehicleSaleValue, " iVehicleCollectionValue: ", iVehicleCollectionValue, " iVehMarketValue: ", iVehMarketValue)
ENDPROC

/// PURPOSE:
///    Passes data about the vehicle groups to SF
PROC PASS_COLLECTION_DATA_TO_SCALEFORM(INT iVehicleCollectionID)
	
	IF IS_VEH_SET_DISABLED(INT_TO_ENUM(IE_VEHICLE_SET_ID, iVehicleCollectionID))
		CDEBUG1LN(DEBUG_INTERNET, "IS_VEH_SET_DISABLED true vor set: ", iVehicleCollectionID)
		EXIT
	ENDIF
		
	INT iCollectionSaleValue = GET_IE_VEH_SET_BONUS_SELL_REWARD(INT_TO_ENUM(IE_VEHICLE_SET_ID, iVehicleCollectionID), 1)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "UPDATE_COLLECTION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVehicleCollectionID) 	// Vehicle collection ID
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCollectionSaleValue)	// Collection value
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) 				// Is this collection of vehicles disabled
	END_SCALEFORM_MOVIE_METHOD()
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> PASS_COLLECTION_DATA_TO_SCALEFORM passing vehicle collection data: ", iVehicleCollectionID, " value: ", iCollectionSaleValue)
ENDPROC

PROC SHOW_EXPORT_VEHICLE_SELECTION_SCREEN()
	BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SHOW_EXPORT_SCREEN")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC RUN_CANCEL_PRESS_CHECKS()
	IF bOverlayActive
		CLOSE_OVERLAY_SCREEN()
	ELIF iCurrentTabID = ciPageID_Buyers
		SET_CURRENT_PAGE_ID(ciPageID_VehicleSelection)
	ELIF HAS_PLAYER_SELECTED_VEHICLE_SET()
	AND iCurrentTabID = ciPageID_Collection
		REMOVE_ALL_SELECTED_VEHICLES(TRUE, TRUE)
		eSelectedVehicleCollection = IE_VEH_SET_INVALID
	ELIF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) > 0
	AND iCurrentTabID = ciPageID_VehicleSelection
		REMOVE_LAST_SF_SELECTED_VEHICLE()
	ELSE
		CPRINTLN(DEBUG_INTERNET,"<APP_IE> EXITING WEBSITE")
		CLEANUP_IE_APP()
	ENDIF
ENDPROC

PROC PASS_DPAD_INPUTS_TO_SCALEFORM()
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LEFT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RIGHT)))
	ENDIF
ENDPROC

PROC PASS_ANALOGUE_STICK_INPUTS_TO_SF(BOOL BPassInLeftStickInput, BOOL BPassInRightStickInput)
	IF BPassInLeftStickInput
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		INT iLeftXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		INT iLeftYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		IF (iLeftXNew != 127) OR (iLeftX != 127) OR (iLeftYNew != 127) OR (iLeftY != 127)
			BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_ANALOG_STICK_INPUT")
				iLeftX = iLeftXNew
				iLeftY = iLeftYNew
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftX)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftY)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
	IF BPassInRightStickInput
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		INT iRightXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		INT iRightYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		IF (iRightXNew != 127) OR (iRightX != 127) OR (iRightYNew != 127) OR (iRightY != 127)
			BOOL bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN))
		
			IF NOT bUsingScrollWheel
				bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP))
			ENDIF
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_ANALOG_STICK_INPUT")
				iRightX = iRightXNew
				iRightY = iRightYNew
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightX)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightY)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUsingScrollWheel)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
ENDPROC

PROC PASS_CURSOR_INPUT_TO_SF()
	iCursorX = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	iCursorY = GET_DISABLED_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
				
	IF iCursorX != iCursorXLast
	OR iCursorY != iCursorYLast
		
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_MOUSE_INPUT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iCursorX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(iCursorY)
		END_SCALEFORM_MOVIE_METHOD()
		
		iCursorXLast = iCursorX
		iCursorYLast = iCursorY			
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_ACCEPT)))
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
		RUN_CANCEL_PRESS_CHECKS()
	ENDIF
ENDPROC

PROC PASS_INPUTS_TO_SCALEFORM()

	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		IF NOT bOverlayActive
			SET_CURRENT_PAGE_ID(ciPageID_VehicleSelection)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LB)))
		ENDIF
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		IF NOT bOverlayActive
			SET_CURRENT_PAGE_ID(ciPageID_Collection)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RB)))
		ENDIF
	ENDIF
	
	// NB: Using IS_CONTROL_PRESSED for continuous zooming out on map.
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LT)))
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LT)))
	ENDIF
	// NB: Using IS_CONTROL_PRESSED for continuous zooming in on map.
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RT)))
	ENDIF
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RT)))
	ENDIF
	
	PASS_DPAD_INPUTS_TO_SCALEFORM()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CPRINTLN(DEBUG_INTERNET,"<APP_IE> PASS_INPUTS_TO_SCALEFORM INPUT_FRONTEND_CANCEL")
		RUN_CANCEL_PRESS_CHECKS()
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_X)))
	ENDIF
		
	// Mouse cursor control
	IF IS_USING_CURSOR(FRONTEND_CONTROL)	
	
		PASS_CURSOR_INPUT_TO_SF()
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_ACCEPT)))
		ENDIF
	
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_CURSOR_CANCEL)))
		ENDIF
		
		//Usng analogue stic input for mouse scroll
		PASS_ANALOGUE_STICK_INPUTS_TO_SF(FALSE, TRUE)
	ELSE
		PASS_ANALOGUE_STICK_INPUTS_TO_SF(TRUE, TRUE)
	ENDIF
	
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
	AND NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CPRINTLN(DEBUG_INTERNET,"<APP_IE> EXITING WEBSITE for Triangle press")
		CLEANUP_IE_APP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		CLEANUP_IE_APP()
	ENDIF
	#ENDIF
ENDPROC

PROC RENDER_IE_APP()	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	IE_APP_CONTROL_DISABLE()
	
	IF g_bInMultiplayer
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)//1544427
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	ENDIF
	
	THEFEED_HIDE_THIS_FRAME()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_PAUSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(siMovie,255,255,255,255)
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	
ENDPROC

/// PURPOSE:
///    Sends the owned vehicle and collection data to SF
PROC SEND_INITIAL_DATA_TO_SCALEFORM()
	
	INT i
	
	//Send data for the players owned vehicles
	REPEAT ciMAX_IE_OWNED_VEHICLES i
		IE_VEHICLE_ENUM eTempVeh = INT_TO_ENUM(IE_VEHICLE_ENUM, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iWarehouseVehicles[i])

		IF ENUM_TO_INT(eTempVeh) > ciIE_COVER_VEHICLE_INDEX
			eTempVeh = INT_TO_ENUM(IE_VEHICLE_ENUM, (ENUM_TO_INT(eTempVeh) - ciIE_COVER_VEHICLE_INDEX))
		ENDIF
		
		IF eTempVeh > IE_VEH_INVALID
		AND eTempVeh < IE_VEHICLE_COUNT
			PASS_VEHICLE_DATA_TO_SCALEFORM(eTempVeh)
		ENDIF		
	ENDREPEAT
	
	//Send data for the value of vehicle collections
	FOR i = ENUM_TO_INT(IE_VEH_SET_METAL) TO (ENUM_TO_INT(IE_VEH_SET_MAX) - 1)
		PASS_COLLECTION_DATA_TO_SCALEFORM(i)
	ENDFOR
	
	IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_SELL)
		INT iTimeRemaining = GB_GET_MISSION_COOLDOWN_TIME_REMAINING(VEV_SELL)
		CDEBUG1LN(DEBUG_INTERNET, "<APP_IE> SEND_INITIAL_DATA_TO_SCALEFORM Cooldown timer running. iTimeRemaining = ", iTimeRemaining)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "SET_COOLDOWN")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimeRemaining)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	SHOW_EXPORT_VEHICLE_SELECTION_SCREEN()
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> SEND_INITIAL_DATA_TO_SCALEFORM initial vehicle and collection data sent")
ENDPROC

/// PURPOSE:
///    Set's the global data and broadcasts an event to allow either vehicle modding to begin or the mission to starts
/// PARAMS:
///    bSellingVehCollection - True if we want to sell a collection of 2 or more specific vehicles
PROC SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS(BOOL bSellingVehCollection = FALSE)

	INT i
	PLAYER_INDEX piUnassignedPlayers[ciMaxSelectableMissionVehicles]
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF NOT bSellingVehCollection
		AND iSelectedMissionVehicles[i] != 0
		OR bSellingVehCollection
		AND iSelectedCollectionVehicles[i] != 0
			INT iVehicleToAssign = iSelectedMissionVehicles[i]
			
			IF bSellingVehCollection
				iVehicleToAssign = iSelectedCollectionVehicles[i]
			ENDIF
			
			//Assign the slected player to this vehicle for the carmodshop script to use
			g_sIEVehicleSetupStruct[i].iIEPlayerVehicle = iVehicleToAssign
			g_sIEVehicleSetupStruct[i].iPlayerIndex = NATIVE_TO_INT(piAssignedPlayers[i])			
		ELSE
			//Assign any remaining gang members so they launch the mission correctly
			//This happens when the boss selects less vehicles than gang members available
			BUILD_LIST_OF_AVAILABLE_GANG_MEMBERS(piUnassignedPlayers)
			
			g_sIEVehicleSetupStruct[i].iIEPlayerVehicle = -1
			g_sIEVehicleSetupStruct[i].iPlayerIndex 	= NATIVE_TO_INT(piUnassignedPlayers[0])
			piAssignedPlayers[i] 						= piUnassignedPlayers[0]			
		ENDIF
		
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS: slot : ", i, " vehicle: ", g_sIEVehicleSetupStruct[i].iIEPlayerVehicle, " Player ID: ", g_sIEVehicleSetupStruct[i].iPlayerIndex)
	ENDREPEAT
	
	IF eDropOffLocation != VEHICLE_EXPORT_DROPOFF_BUYER_PRIVATE
	AND NOT bSellingVehCollection
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_ANY_CONVERSATION()
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS: Mechaninc still speaking killing conversation for vehicle modding to begin")
		ENDIF
		
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS: Options selcted for buyer: ", eDropOffLocation, " Waiting for modding session to begin.")
		SET_PLAYER_MISSION_PERSONAL_CAR_MOD_START(TRUE)
		bWaitingForCarmodEventToProcess = TRUE
	ELSE
		SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION (PLAYER_ID(), TRUE)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS: Options selected for private buyer moving on to eAppStage_LaunchingMission")
		eAppStage = eAppStage_LaunchingMission
	ENDIF
	
	IF bSellingVehCollection
		SET_PLAYER_STARTED_VEH_SET_SELL_MISSION(eSelectedVehicleCollection)
	ELSE
		CLEAR_IE_SELL_VEHICLE_SET_DATA()
	ENDIF
	
	SET_OFFICE_PERSONAL_CAR_MOD_IE_MISSION_FAILED(FALSE)
	
	//Broadcast this so the all relevant players know which vehicle to mod
	BROADCAST_START_PRE_SELL_VEHICLE_MOD(piAssignedPlayers[0], piAssignedPlayers[1], piAssignedPlayers[2], piAssignedPlayers[3], 
												g_sIEVehicleSetupStruct[0].iIEPlayerVehicle, g_sIEVehicleSetupStruct[1].iIEPlayerVehicle, 
												g_sIEVehicleSetupStruct[2].iIEPlayerVehicle, g_sIEVehicleSetupStruct[3].iIEPlayerVehicle, eDropOffLocation, bSellingVehCollection)
	
	g_bLaunchedPreSellVehModFromIEApp = TRUE
ENDPROC

PROC RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES(IE_VEHICLE_ENUM &eVehiclesToRemove[ciMaxSelectableMissionVehicles])
	INT i
	INT iSlots[ciMaxSelectableMissionVehicles]
	INT iVehicles[ciMaxSelectableMissionVehicles]
	CONTRABAND_TRANSACTION_STATE eResult
	
	REPEAT ciMaxSelectableMissionVehicles i
		IF eVehiclesToRemove[i] = IE_VEH_INVALID
			iVehicles[i] 	= 0
			iSlots[i]		= -1
		ELSE
			iVehicles[i] 	= 0
			iSlots[i]		= GET_SAVE_SLOT_FOR_IE_VEHICLE(eVehiclesToRemove[i])
		ENDIF
		
		CPRINTLN(DEBUG_INTERNET, "<APP_IE>[Mission_Launch] RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES Transaction setup. slot: ", i, " Remove Vehicle ", eVehiclesToRemove[i], " from save slot: ", iSlots[i])
	ENDREPEAT
	
	IF USE_SERVER_TRANSACTIONS()
	
		CPRINTLN(DEBUG_INTERNET, "<APP_IE>[Mission_Launch] RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES starting transaction...")
		
		WHILE NOT PROCESS_TRANSACTION_FOR_UPDATE_IE_VEHICLE_SLOT(iSlots, iVehicles, eResult)
			WAIT(0)
		ENDWHILE
		
		IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
			REMOVE_VEHICLE_FROM_IE_WAREHOUSE(eVehiclesToRemove)
		ELSE
			g_bIEAppFailedRemoveVehicles = TRUE
			CASSERTLN(DEBUG_INTERNET, "RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES transaction failed to remove vehicles")
		ENDIF
	ELSE
		REMOVE_VEHICLE_FROM_IE_WAREHOUSE(eVehiclesToRemove)
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE>[Mission_Launch] RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES Vehicles removed. Moving on")
ENDPROC

PROC LAUNCH_MISSION()
	INT i
	IE_VEHICLE_ENUM eMissionVehicles[ciMaxSelectableMissionVehicles]
	
	BOOL bSellingVehSet = IS_LOCAL_PLAYER_SELLING_VEHICLE_SET()
	CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_LAUNCHED_VEH_EXP_MISSION_FROM_HACKER_TRUCK)
	
	//Get the four vehicles we need for the mission
	REPEAT ciMaxSelectableMissionVehicles i
		IF bSellingVehSet
			eMissionVehicles[i] = INT_TO_ENUM(IE_VEHICLE_ENUM, iSelectedCollectionVehicles[i])
		ELSE
			eMissionVehicles[i] = INT_TO_ENUM(IE_VEHICLE_ENUM, iSelectedMissionVehicles[i])
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_INTERNET, "<APP_IE>[Mission_Launch] LAUNCH_MISSION Requesting Mission: FMMC_TYPE_VEHICLE_EXPORT_SELL with vehicles: ", 
		GET_SELECTED_IE_VEHICLE_STRING(eMissionVehicles[0]), " & ",
		GET_SELECTED_IE_VEHICLE_STRING(eMissionVehicles[1]), " & ", 
		GET_SELECTED_IE_VEHICLE_STRING(eMissionVehicles[2]), " & ", 
		GET_SELECTED_IE_VEHICLE_STRING(eMissionVehicles[3]), " Dropoff location: ", eDropOffLocation, " Vehicle count: ", GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(bSellingVehSet))
	#ENDIF
	
	IF eDropOffLocation = VEHICLE_EXPORT_DROPOFF_BUYER_PRIVATE
	OR IS_LOCAL_PLAYER_SELLING_VEHICLE_SET()
		//Private buyers or when we sell a vehicle collection do not require us to mod any vehicles so set this here before the mission launch
		SET_PLAYER_MISSION_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION (PLAYER_ID(), TRUE)
	ENDIF
	
	IF IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(eDropOffLocation, GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(bSellingVehSet), bSellingVehSet)
		GB_BOSS_REQUEST_VEHICLE_EXPORT_MISSION_LAUNCH_FROM_SERVER(FMMC_TYPE_VEHICLE_EXPORT_SELL, eMissionVehicles[0], eMissionVehicles[1], eMissionVehicles[2], eMissionVehicles[3])
		
		//Run the transaction to remove the mission vehicles
		RUN_TRANSACTION_TO_REMOVE_IE_VEHICLES(eMissionVehicles)
		
		// Reset existing dropoffs
		IF g_bIEAppFailedRemoveVehicles
			REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[i] = IE_DROPOFF_INVALID
				#IF IS_DEBUG_BUILD
					PRINTLN("<APP_IE>[Mission_Launch] LAUNCH_MISSION - Invalidating existing dropoffs!")
				#ENDIF
			ENDREPEAT		
		ELSE
			//Set the stat
			INCREMENT_TOTAL_NUM_EXPORT_MISSIONS_STARTED()
		ENDIF
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<APP_IE>[Mission_Launch] LAUNCH_MISSION Couldn't activate dropoffs!") 
	ENDIF
ENDPROC

FUNC STRING GET_CONFIRM_SELL_DESCRIPTION(BOOL bPrivateBuyer, BOOL bCantAffordMods = FALSE, INT iPrivateBuyerCost = -1, BOOL bBuyerDisabled = FALSE)
	
	IF bBuyerDisabled
		RETURN "AH_BUYER_D6"
	ELIF bCantAffordMods
		IF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) = 1
			RETURN "AH_BUYER_NM2"
		ELSE
			RETURN "AH_BUYER_NM1"
		ENDIF
	ELIF bPrivateBuyer
		IF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) = 1
			IF iPrivateBuyerCost > 0
				RETURN "AH_BUYER_D4"
			ELSE
				RETURN "AH_BUYER_D2"
			ENDIF
		ELIF iPrivateBuyerCost > 0
			RETURN "AH_BUYER_D5"
		ENDIF
	ELSE
		IF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) = 1
			RETURN "AH_BUYER_D4"
		ELSE
			RETURN "AH_BUYER_D5"
		ENDIF
	ENDIF
	
	RETURN "AH_BUYER_D1"
ENDFUNC

/// PURPOSE:
///    Returns a string based on the number of vehicles in a collection
FUNC STRING GET_COLLECT_MORE_VEHICLES_MESSAGE(IE_VEHICLE_SET_ID eCollectionID)
	INT iCollectionVehicleCount = GET_IE_VEH_SET_VEHICLE_COUNT(eCollectionID)
	
	SWITCH iCollectionVehicleCount
		CASE 2	RETURN "AH_COL_2VEH"
		CASE 3	RETURN "AH_COL_3VEH"
		CASE 4 	RETURN "AH_COL_4VEH"
	ENDSWITCH
	
	RETURN "AH_COL_2VEH"
ENDFUNC

FUNC STRING GET_IE_MISSION_CD_OVERLAY_TEXT()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal > 1
		RETURN "AH_COOLDOWN2"
	ENDIF
	
	RETURN "AH_COOLDOWN"
ENDFUNC

FUNC BOOL CAN_PLAYER_AFFORD_VEHICLE_MODS(IE_VEH_BUYER eBuyerID)
	INT iCost
	
	IF eBuyerID = IE_BUYER_PRIVATE
		iCost = GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_PRIVATE_BUYER_1, TRUE)
	ELIF eBuyerID = IE_BUYER_SHOWROOM
		iCost = GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_SHOWROOM_1, TRUE)
	ELIF eBuyerID = IE_BUYER_SPECIALIST
		iCost = GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_GANGSTER_1, TRUE)
	ENDIF
	
	IF iCost > 0 
	AND NOT NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> CAN_PLAYER_AFFORD_IE_WH_PURCHASE: purchase failed, player doesn't have sufficient cash. Total cost: ", iCost)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Deals with button input from the player
PROC HANDLE_APP_SELECTION(enumItemConsumed &eConsumedInput)

	IF eConsumedInput < inputConsumed_0
		
		IF BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES()
			IF eConsumedInput = inputConsumed_BUYER_0
			OR eConsumedInput = inputConsumed_BUYER_1
			OR eConsumedInput = inputConsumed_BUYER_2
				CDEBUG1LN(DEBUG_INTERNET, "<APP_IE> HANDLE_APP_SELECTION BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES is true and we've selected a buyer. Blocking mission launch")
				START_OVERLAY_SCREEN("AH_BUYER_T", "AH_SAVING_DOWN", "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
				
				eConsumedInput = inputConsumed_1
				EXIT
			ENDIF
		ENDIF
		
		IF eConsumedInput = inputConsumed_OVERLAY_CONFIRM
			IF bConfirmMissionLaunch
			
				BOOL bLaunchVehSetMission = HAS_PLAYER_SELECTED_VEHICLE_SET()
				IF iCurrentTabID != ciPageID_Collection
					bLaunchVehSetMission = FALSE
				ENDIF
			
				IF CAN_PLAYER_LAUNCH_MISSION(bLaunchVehSetMission)
					//Assign players to vehicles
					ASSIGN_GANG_MEMBERS_TO_MISSION_VEHICLES(bLaunchVehSetMission)
					//Setup the data and start the next stage
					SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS(bLaunchVehSetMission)
					
					CLOSE_OVERLAY_SCREEN()
				ELSE
					
					TEXT_LABEL_15 tlFailReason = GET_MISSION_UNAVAILABLE_REASON_TEXT_LABEL(bLaunchVehSetMission)
					
					CLOSE_OVERLAY_SCREEN()
					
					CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
					IF iMissionUnavailableReason = ciFailReasonTooFewPlayers
					AND NOT bLaunchVehSetMission
						INT i, iNumVehiclesToRemove
						iNumVehiclesToRemove = (GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(bLaunchVehSetMission) - GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE())
						
						REPEAT iNumVehiclesToRemove i
							REMOVE_LAST_SF_SELECTED_VEHICLE()
						ENDREPEAT
					ENDIF					
					
					START_OVERLAY_SCREEN("AH_BUYER_T", tlFailReason, "OR_OVRLY_OK", "")
				ENDIF
			ELSE
				CLOSE_OVERLAY_SCREEN()
			ENDIF
		ELIF eConsumedInput = inputConsumed_OVERLAY_CANCEL
		
			CLOSE_OVERLAY_SCREEN()
			
		ELIF eConsumedInput = inputConsumed_EXPORT_VEHICLES
		
			IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_SELL)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_IE_MISSION_CD_OVERLAY_TEXT(), "OR_OVRLY_OK", "")
			#IF FEATURE_CASINO_HEIST
			ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				START_OVERLAY_SCREEN("AH_VEH_SEL_T", "AHA_ARCADE_BLCK", "OR_OVRLY_OK", "")
			#ENDIF
			ELIF iSelectedMissionVehicles[0] = 0 AND iCurrentTabID = ciPageID_VehicleSelection
			OR iSelectedCollectionVehicles[0] = 0 AND iCurrentTabID = ciPageID_Collection
				//No vehicles selected
				START_OVERLAY_SCREEN("AH_VEH_SEL_T", "AH_VEH_SEL_D", "OR_OVRLY_OK", "")
			ELIF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				//Not a boss
				START_OVERLAY_SCREEN("AH_BUYER_T", "AH_NBOSS", "OR_OVRLY_OK", "")
			ELIF HAS_PLAYER_SELECTED_VEHICLE_SET()
			AND iCurrentTabID = ciPageID_Collection
				IF BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES()
					CDEBUG1LN(DEBUG_INTERNET, "<APP_IE> HANDLE_APP_SELECTION BLOCK_MISSION_LAUNCH_FOR_SAVE_ISSUES is true and we've selected a collection. Blocking mission launch")
					START_OVERLAY_SCREEN("AH_BUYER_T", "AH_SAVING_DOWN", "WHOUSE_CONF", "")
				ELSE
					//Vehicle collection selected. No need to select a buyer for this
					START_OVERLAY_SCREEN("AH_BUYER_T", "AH_BUYER_D3", "WHOUSE_CONF", "WHOUSE_CANC")
					eDropOffLocation = VEHICLE_EXPORT_DROPOFF_BUYER_GANGSTER
					bConfirmMissionLaunch = TRUE
				ENDIF
			ELSE
				SHOW_EXPORT_BUYERS_SCREEN()
			ENDIF
			
		ELIF eConsumedInput = inputConsumed_BUYER_0
			IF IS_IE_VEH_BUYER_DISABLED(IE_BUYER_PRIVATE)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE, DEFAULT, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ELIF CAN_PLAYER_AFFORD_VEHICLE_MODS(IE_BUYER_PRIVATE)
			
				INT iPrivateBuyerModCost 	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_PRIVATE_BUYER_1, TRUE)
				INT iPrivateBuyerReward		= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_PRIVATE_BUYER_1)
				
				IF iPrivateBuyerModCost <= 0
					iPrivateBuyerModCost = -1
				ENDIF
				
				//Buyer 0 confirm message
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, FALSE, iPrivateBuyerModCost), "WHOUSE_CONF", "WHOUSE_CANC", iPrivateBuyerModCost, iPrivateBuyerReward)
				eDropOffLocation = VEHICLE_EXPORT_DROPOFF_BUYER_PRIVATE
				bConfirmMissionLaunch = TRUE
			ELSE
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ENDIF
		ELIF eConsumedInput = inputConsumed_BUYER_1
			IF IS_IE_VEH_BUYER_DISABLED(IE_BUYER_SHOWROOM)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE, DEFAULT, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ELIF CAN_PLAYER_AFFORD_VEHICLE_MODS(IE_BUYER_SHOWROOM)
				
				INT iShowroomBuyerModCost 	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_SHOWROOM_1, TRUE)
				INT iShowroomBuyerReward	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_SHOWROOM_1)
				
				//Buyer 1 confirm message
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(FALSE), "WHOUSE_CONF", "WHOUSE_CANC", iShowroomBuyerModCost, iShowroomBuyerReward)
				eDropOffLocation = VEHICLE_EXPORT_DROPOFF_BUYER_SHOWROOM
				bConfirmMissionLaunch = TRUE
			ELSE
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ENDIF
		ELIF eConsumedInput = inputConsumed_BUYER_2
			IF IS_IE_VEH_BUYER_DISABLED(IE_BUYER_SPECIALIST)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE, DEFAULT, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ELIF CAN_PLAYER_AFFORD_VEHICLE_MODS(IE_BUYER_SPECIALIST)
			
				INT iSpecialistBuyerModCost	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_GANGSTER_1, TRUE)
				INT iSpecialistBuyerReward	= GET_EXPORT_VALUE_OF_SELECTED_MISSION_VEHICLES(IE_DROPOFF_GANGSTER_1)
				
				//Buyer 2 confirm message
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(FALSE), "WHOUSE_CONF", "WHOUSE_CANC", iSpecialistBuyerModCost, iSpecialistBuyerReward)
				eDropOffLocation = VEHICLE_EXPORT_DROPOFF_BUYER_GANGSTER
				bConfirmMissionLaunch = TRUE
			ELSE
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_CONFIRM_SELL_DESCRIPTION(TRUE, TRUE), "WHOUSE_CONF", "")
				bConfirmMissionLaunch = FALSE
			ENDIF
		ENDIF
				
		eConsumedInput = inputConsumed_1
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the button presed is a vehicle or a vehicle plus the export window offset
FUNC BOOL HAS_VEHICLE_BUTTON_BEEN_SELECTED(INT iButtonPressID)

	INT iLowestButtonID 	= ENUM_TO_INT(IE_VEH_INVALID)
	INT iHighestButtonID 	= ENUM_TO_INT(IE_VEHICLE_COUNT)
	
	//Check for the vehicles in the bottom selection window
	IF iButtonPressID > iLowestButtonID
	AND iButtonPressID < iHighestButtonID
	//Check for the vehicles + the offset. (Corresponds to the vehicles in the export selection window)
	OR iButtonPressID > (iLowestButtonID + ciVehicleSelectionOffset)
	AND iButtonPressID < (iHighestButtonID + ciVehicleSelectionOffset)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the button presed is a vehicle collections or a vehicle plus the export window offset
FUNC BOOL HAS_VEHICLE_SET_BUTTON_BEEN_SELECTED(INT iButtonPressID)

	INT iLowestButtonID 	= ENUM_TO_INT(IE_VEH_SET_METAL)
	INT iHighestButtonID 	= ENUM_TO_INT(IE_VEH_SET_MAX)
	
	//Check for the vehicles in the bottom selection window
	IF iButtonPressID >= iLowestButtonID
	AND iButtonPressID < iHighestButtonID
	//Check for the vehicles + the offset. (Corresponds to the vehicles in the export selection window)
	OR iButtonPressID > (iLowestButtonID + ciCollectionSelectionOffset)
	AND iButtonPressID < (iHighestButtonID + ciCollectionSelectionOffset)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_BUTTON_PRESS()

	IF currentButtonPressStage = inputConsumed_1
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
			BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "GET_CURRENT_SELECTION")			
			currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siMovie, "GET_CURRENT_TAB_ID")	
			currentTabReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			currentButtonPressStage = inputConsumed_0
		ENDIF
	ENDIF
	
	IF (currentButtonPressStage = inputConsumed_0)
	AND (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentSelectionReturnIndex) AND IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentTabReturnIndex))
	
		iCurrentTabID = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentTabReturnIndex)
		
		INT 	iButtonIndex 		= GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentSelectionReturnIndex)
		BOOL 	bSendInputBackToSF 	= TRUE
		BOOL	bSetPageID			= TRUE
		
		CONST_INT iCONST_Button_Index_101 	101		//OVERLAY_ACCEPT_BUTTON:Number 
		CONST_INT iCONST_Button_Index_102	102		//OVERLAY_CANCEL_BUTTON
		CONST_INT iCONST_Button_Index_1000	1000	//Export Tab
		CONST_INT iCONST_Button_Index_1001	1001	//Collections Tab
		CONST_INT iCONST_Button_Index_1002	1002	//Export button for the selected vehicles
		CONST_INT iCONST_Button_Index_1007	1007	//Buyer 1
		CONST_INT iCONST_Button_Index_1008 	1008	//Buyer 2
		CONST_INT iCONST_Button_Index_1009	1009	//Buyer 3
					
		currentButtonPressStage = INT_TO_ENUM(enumItemConsumed, iButtonIndex)
		
		//Individual vehicles
		IF HAS_VEHICLE_BUTTON_BEEN_SELECTED(iButtonIndex)
			
			IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_SELL)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_IE_MISSION_CD_OVERLAY_TEXT(), "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			#IF FEATURE_CASINO_HEIST
			ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				START_OVERLAY_SCREEN("AH_VEH_SEL_T", "AHA_ARCADE_BLCK", "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			#ENDIF
			ELIF NOT IS_VEHICLE_SELECTED(iButtonIndex, FALSE)
				IF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) = ciMaxSelectableMissionVehicles
					START_OVERLAY_SCREEN("AH_BUYER_T", "AH_MAX_PLAYERD", "OR_OVRLY_OK", "")
					bSendInputBackToSF = FALSE
				ELIF GET_COUNT_OF_VEHICLES_SELCTED_FOR_SELL_MISSION(FALSE) < GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE()
					ADD_SCALEFORM_SELECTED_VEHICLE(iButtonIndex, FALSE)
				ELSE
					START_OVERLAY_SCREEN("AH_BUYER_T", "AH_NE_PLAYERD", "OR_OVRLY_OK", "")
					bSendInputBackToSF = FALSE
				ENDIF				
			ELSE
				IF iButtonIndex > ciVehicleSelectionOffset
					iButtonIndex -= ciVehicleSelectionOffset
				ENDIF
				
				REMOVE_SCALEFORM_SELECTED_VEHICLE(iButtonIndex)
			ENDIF
			
			//Do nothing until the player selects the export button
			currentButtonPressStage = inputConsumed_1
		//vehicle collections (ID 201 - 210)
		ELIF HAS_VEHICLE_SET_BUTTON_BEEN_SELECTED(iButtonIndex)
			IE_VEHICLE_SET_ID eCollection = INT_TO_ENUM(IE_VEHICLE_SET_ID, iButtonIndex)
			IF NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_SELL)
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_IE_MISSION_CD_OVERLAY_TEXT(), "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			#IF FEATURE_CASINO_HEIST
			ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				START_OVERLAY_SCREEN("AH_VEH_SEL_T", "AHA_ARCADE_BLCK", "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			#ENDIF
			ELIF NOT DOES_PLAYER_OWN_ALL_VEHICLES_IN_A_SET(eCollection)
				//Collect more vehicles message
				START_OVERLAY_SCREEN("AH_BUYER_T", GET_COLLECT_MORE_VEHICLES_MESSAGE(eCollection), "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			ELIF GET_COUNT_OF_GANG_MEMBERS_INSIDE_IE_GARAGE() < GET_IE_VEH_SET_VEHICLE_COUNT(eCollection)
				//Not enough players to launch a mission
				START_OVERLAY_SCREEN("AH_BUYER_T", "AH_COL_PLAYERD", "OR_OVRLY_OK", "")
				bSendInputBackToSF = FALSE
			ELIF eSelectedVehicleCollection = eCollection
				//We already have a collection selected so deselect this
				eSelectedVehicleCollection = IE_VEH_SET_INVALID
				REMOVE_ALL_SELECTED_VEHICLES(TRUE)
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Deselecting collection ", eCollection)
			ELIF HAS_PLAYER_SELECTED_VEHICLE_SET()
				//We have selected a different collection				
				REMOVE_ALL_SELECTED_VEHICLES(TRUE)				//Remove the old vehicles
				//Tell Scaleform to remove the old collection
				CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(eSelectedVehicleCollection)))
				eSelectedVehicleCollection = eCollection		//Select the new collection
				ASSIGN_COLLECTION_VEHICLES(eCollection)			//Assign the vehicles
				
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Selecting collection ", eCollection)
			ELSE
				eSelectedVehicleCollection = eCollection
				ASSIGN_COLLECTION_VEHICLES(eCollection)
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Selecting collection ", eCollection)
			ENDIF
			
			//Do nothing until the player selects the export button
			currentButtonPressStage = inputConsumed_1
		ELSE			
			SWITCH iButtonIndex
				CASE iCONST_Button_Index_101 	//OVERLAY_ACCEPT_BUTTON
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS OVERLAY_ACCEPT_BUTTON clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_102	//OVERLAY_CANCEL_BUTTON
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS OVERLAY_CANCEL_BUTTON clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1002	//Export button for the selected vehicles
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Export button clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1007	//Buyer 1
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Buyer 1 clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1008 	//Buyer 2
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Buyer 2 clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1009 	//Buyer 3
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Buyer 3 clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1000	//Export Tab
					bSetPageID = FALSE
					SET_CURRENT_PAGE_ID(ciPageID_VehicleSelection)
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Export tab button clicked ", iButtonIndex)
				BREAK
				CASE iCONST_Button_Index_1001	//Collections Tab
					bSetPageID = FALSE
					SET_CURRENT_PAGE_ID(ciPageID_Collection)
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Collections tab button clicked ", iButtonIndex)
				BREAK
				DEFAULT
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS unhandled button index ", iButtonIndex)
				BREAK
			ENDSWITCH
		ENDIF
		
		IF bSendInputBackToSF
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> CHECK_BUTTON_PRESS Sending input value back to SF ", iButtonIndex, " Current tab: ", iCurrentTabID)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siMovie, "SET_INPUT_EVENT", TO_FLOAT(iButtonIndex))
		ENDIF
		
		IF bSetPageID
			SET_CURRENT_PAGE_ID(iCurrentTabID)
		ENDIF
	ENDIF

	//Handle the control input receivied above
	HANDLE_APP_SELECTION(currentButtonPressStage)
ENDPROC

FUNC BOOL HAVE_ALL_IEAPP_HELP_TUTORIALS_BEEN_COMPLETED()
	IF HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_VEH_SELECT)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE)	
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE_2)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE_2)
	AND HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_EXPORT_COOLDOWN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWEBSITE_TUTORIAL_BS_INDEX eTutorialID)

	INT iTutorialID = ENUM_TO_INT(eTutorialID)
	
	SWITCH eTutorialID
		CASE eWTBS_AH_VEH_SELECT
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_VEH_SELECT)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_1_PLAYED_THIS_BOOT)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_BUYER_PAGE
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_2_PLAYED_THIS_BOOT)
			AND iCurrentTabID = ciPageID_Buyers
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_BUYER_PAGE_2
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE_2)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_2_PLAYED_THIS_BOOT)
			AND iCurrentTabID = ciPageID_Buyers
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_COLLECTION_PAGE
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_3_PLAYED_THIS_BOOT)
			AND iCurrentTabID = ciPageID_Collection
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_COLLECTION_PAGE_2
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE_2)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_3_PLAYED_THIS_BOOT)
			AND iCurrentTabID = ciPageID_Collection
			AND DOES_LOCAL_PLAYER_HAVE_ALL_VEHICLES_IN_ANY_SET()
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_EXPORT_COOLDOWN
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_EXPORT_COOLDOWN)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT GB_HAS_IE_MISSION_COOLDOWN_TIMER_EXPIRED(VEV_SELL)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eWTBS_AH_BUYER_PAGE_3
			IF NOT HAS_IE_WEBSITE_TUT_BEEN_DONE(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE_3)
			AND NOT IS_BIT_SET(g_iWebsiteTutorialDisplayedBS, iTutorialID)
			AND NOT IS_BIT_SET(iBS, AH_BS_TUTORIAL_2_PLAYED_THIS_BOOT)
			AND iCurrentTabID = ciPageID_Buyers
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_IE_WEBSITE_TUTORIAL_BS()
	iIETutorialBS1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_IMP_EXP_COMP_TUTORIALS1)
	iIETutorialBS2 = GET_MP_INT_CHARACTER_STAT(MP_STAT_IMP_EXP_COMP_TUTORIALS2)
ENDPROC

PROC IEAPP_PRINT_TUTORIAL_HELP(STRING sStringToDisplay, INT HelpTime)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(sStringToDisplay)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, HelpTime)
ENDPROC

PROC RUN_IEAPP_HELP_TEXT_TUTORIAL()

	STRING sHelp	
	
	IF NOT IS_BIT_SET(iBS, AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
	AND NOT IS_BIT_SET(iBS, AH_BS_CURRENTLY_PLAYING_TUTORIAL)
		IF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_VEH_SELECT)
		
			sHelp 				= "IE_SEC_TUT7"						//Landing page (Veh selection)
			iTutorialHelpTime 	= 9000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_VEH_SELECT))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_VEH_SELECT)
			
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_BUYER_PAGE)
		
			sHelp 				= "IE_SEC_TUT8"						//Buyer page
			iTutorialHelpTime 	= 9500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_BUYER_PAGE))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE)
			
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_BUYER_PAGE_2)
		
			sHelp 				= "IE_SEC_TUT13"					//Buyer page 2
			iTutorialHelpTime 	= 10000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_BUYER_PAGE_2))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE_3)
			
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_COLLECTION_PAGE)
		
			sHelp 				= "IE_SEC_TUT9"						//Buyer page 2
			iTutorialHelpTime 	= 10000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_COLLECTION_PAGE))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_BUYER_PAGE_2)
			
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_COLLECTION_PAGE_2)
		
			sHelp 				= "IE_SEC_TUT10"					//Vehicle collections page
			iTutorialHelpTime 	= 8500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_COLLECTION_PAGE_2))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE)
		
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_EXPORT_COOLDOWN)
		
			sHelp 				= "IE_SEC_TUT11"					//We have completed a vehicle collection
			iTutorialHelpTime 	= 8500
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_EXPORT_COOLDOWN))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_COLLECTION_PAGE_2)
			
		ELIF SHOULD_IEAPP_HELP_TUTORIAL_BE_DISPLAYED(eWTBS_AH_BUYER_PAGE_3)
		
			sHelp 				= "IE_SEC_TUT12"					//Mission CD active
			iTutorialHelpTime 	= 8000
			SET_BIT(g_iWebsiteTutorialDisplayedBS, ENUM_TO_INT(eWTBS_AH_BUYER_PAGE_3))
			INCREMENT_NUM_TIMES_IE_WEB_TUT_HAS_PLAYED(iIETutorialBS1, iIETutorialBS2, IE_WEB_TUT_AH_EXPORT_COOLDOWN)
			
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sHelp)
	AND NOT IS_BIT_SET(iBS, AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		//Display the relevant help text
		IEAPP_PRINT_TUTORIAL_HELP(sHelp, iTutorialHelpTime)
		SET_BIT(iBS, AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		
		CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> RUN_IEAPP_HELP_TEXT_TUTORIAL Printing: sHelp: ", sHelp, " time: ", iTutorialHelpTime)
	ELIF IS_BIT_SET(iBS, AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
		IF NOT HAS_NET_TIMER_STARTED(stTutorialTimer)
			START_NET_TIMER(stTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> RUN_IEAPP_HELP_TEXT_TUTORIAL Starting timer for: ", sHelp, " time: ", iTutorialHelpTime)
		ELIF HAS_NET_TIMER_EXPIRED(stTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, AH_BS_CURRENTLY_DISPLAYING_TUTORIAL_HELP)
			iTutorialHelpTime = 0
			RESET_NET_TIMER(stTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> RUN_IEAPP_HELP_TEXT_TUTORIAL Starting timer expired for: ", sHelp, " time: ", iTutorialHelpTime)
			
			IF HAVE_ALL_IEAPP_HELP_TUTORIALS_BEEN_COMPLETED()
				SET_BIT(iBS, AH_BS_HELP_TEXT_TUTORIAL_COMPLETE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_1)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_2)
	AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_3)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED TRUE")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED FALSE")
	RETURN FALSE
ENDFUNC

FUNC STRING APPIE_TUT_GET_ROOT_LABEL(INT iTutorialID)
	SWITCH iTutorialID
		CASE 0	RETURN "IMMEC_TUT1"	//First login
		CASE 1	RETURN "IMMEC_TUT2"	//Buyer page
		CASE 2	RETURN "IMMEC_TUT3"	//Collections page
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "APPIE_TUT_GET_ROOT_LABEL: Invalid tutorial ID: ", iTutorialID)	
	RETURN "IMMEC_TUT1"
ENDFUNC

PROC APPIE_START_TUTORIAL_DIALOGUE(INT iTutorialID)	
	IF CREATE_CONVERSATION(tutPedStruct, "IMMECAU", APPIE_TUT_GET_ROOT_LABEL(iTutorialID), CONV_PRIORITY_HIGH)
		CPRINTLN(DEBUG_INTERNET, "<APP_IE> Created tutorial converstion: ", iTutorialID)
		iTutorialDialogueID	= -1
	ENDIF
ENDPROC

PROC APPIE_RUN_TUTORIAL_DIALOGUE()
	//Start the dialogue
	IF iTutorialDialogueID != -1
		APPIE_START_TUTORIAL_DIALOGUE(iTutorialDialogueID)
	ENDIF
ENDPROC

PROC APPIE_SELECT_APP_DIALOGUE_TUTORIAL()

	BOOL bStartNewDialogue
	
	IF NOT IS_BIT_SET(iBS, AH_BS_CURRENTLY_PLAYING_TUTORIAL)
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_1)
			//First login
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 6750
			iTutorialDialogueID	= 0
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_1, TRUE)
			SET_BIT(iBS, AH_BS_TUTORIAL_1_PLAYED_THIS_BOOT)
			
		ELIF iCurrentTabID = ciPageID_Buyers
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_2)
			//Buyers Page
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 8000
			iTutorialDialogueID	= 1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_2, TRUE)
			SET_BIT(iBS, AH_BS_TUTORIAL_2_PLAYED_THIS_BOOT)
			
		ELIF iCurrentTabID = ciPageID_Collection
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_3)
			//Collections page
			bStartNewDialogue	= TRUE
			iTutorialHelpTime 	= 6000
			iTutorialDialogueID	= 2
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AH_AUTO_TUT_3, TRUE)
			SET_BIT(iBS, AH_BS_TUTORIAL_3_PLAYED_THIS_BOOT)
		ENDIF
	ENDIF
	
	IF bStartNewDialogue 
		SET_BIT(iBS, AH_BS_CURRENTLY_PLAYING_TUTORIAL)		
		CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> APPIE_SELECT_APP_DIALOGUE_TUTORIAL Running tutorial for: ", iTutorialHelpTime, " seconds. Tutorial ID: ", iTutorialDialogueID)
	ELIF IS_BIT_SET(iBS, AH_BS_CURRENTLY_PLAYING_TUTORIAL)
		IF NOT HAS_NET_TIMER_STARTED(stTutorialTimer)
			START_NET_TIMER(stTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> APPIE_SELECT_APP_DIALOGUE_TUTORIAL Starting timer time: ", iTutorialHelpTime, " Tutorial ID: ", iTutorialDialogueID)
		ELIF HAS_NET_TIMER_EXPIRED(stTutorialTimer, iTutorialHelpTime)
			CLEAR_BIT(iBS, AH_BS_CURRENTLY_PLAYING_TUTORIAL)
			iTutorialHelpTime = 0
			RESET_NET_TIMER(stTutorialTimer)
			CPRINTLN(DEBUG_INTERNET, "<APP_IE_TUT> APPIE_SELECT_APP_DIALOGUE_TUTORIAL Starting timer expired time: ", iTutorialHelpTime, " Tutorial ID: ", iTutorialDialogueID)
			
			//Once all the tutorial are complete we no longer need this function
			IF HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
				SET_BIT(iBS, AH_BS_DIALOGUE_TUTORIAL_COMPLETE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC APPIE_SETUP_TUTORIAL_DIALOGUE()
	SET_USE_DLC_DIALOGUE(TRUE)
	
	ADD_PED_FOR_DIALOGUE(tutPedStruct, 2, NULL, "SECUROMECH")
	
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> APPIE_SETUP_TUTORIAL_DIALOGUE ped added for dialogue")
ENDPROC

#IF IS_DEBUG_BUILD
//PROC SETUP_PRIVATE_IE_APP_WIDGETS(IE_APP_DEBUG_WIDGET_VARS &debug_vars)	
//	START_WIDGET_GROUP("IE App")		
//		ADD_WIDGET_INT_SLIDER("IE Vehicle to set as owned", debug_vars.iVehicleSlider, 1, (ENUM_TO_INT(IE_VEHICLE_COUNT) - 1), 1)
//		ADD_WIDGET_BOOL("Set IE vehicle as owned", debug_vars.bSetIEVehicleAsOwned)
//		
//		ADD_WIDGET_INT_SLIDER("IE Vehicle to set for steal mission", debug_vars.iStealMisVehSlider, 1, (ENUM_TO_INT(IE_VEHICLE_COUNT) - 1), 1)
//		ADD_WIDGET_BOOL("Set steal mission vehicle", debug_vars.bSelectBuyMissionVehicle)
//		
//		ADD_WIDGET_BOOL("Remove all owned IE vehicles", debug_vars.bClearOwnedIEVehicles)
//		
//		INT i
//		
//		START_WIDGET_GROUP("IE vehicle lookup")
//			FOR i = 1 TO (ENUM_TO_INT(IE_VEHICLE_COUNT) - 1)
//				TEXT_LABEL_63 tlVehicle = i
//				tlVehicle += ": "
//				tlVehicle += GET_STRING_FROM_IE_VEHICLE_ENUM(INT_TO_ENUM (IE_VEHICLE_ENUM, i))
//				ADD_WIDGET_STRING(tlVehicle)
//			ENDFOR
//		STOP_WIDGET_GROUP()
//	STOP_WIDGET_GROUP()
//ENDPROC

//PROC UPDATE_DEBUG_WIDGETS(IE_APP_DEBUG_WIDGET_VARS &debug_vars)
//	IF debug_vars.bSelectBuyMissionVehicle
//		g_eIEStealMissionVehList[g_iIEStealMissionActiveVehicle] = INT_TO_ENUM(IE_VEHICLE_ENUM, debug_vars.iStealMisVehSlider)
//		debug_vars.bSelectBuyMissionVehicle = FALSE
//	ENDIF
//	IF debug_vars.bSetIEVehicleAsOwned
//		
//		IF debug_vars.iVehicleSlider != 0
//			IE_VEHICLE_ENUM eVehicle = INT_TO_ENUM(IE_VEHICLE_ENUM, debug_vars.iVehicleSlider)
//			MODEL_NAMES eModel = GET_MODEL_NAME_FROM_IE_VEHICLE(eVehicle)
//			BROADCAST_PLAYER_DELIVERED_VEHICLE(PLAYER_ID(), PLAYER_ID(), eModel, debug_vars.iVehicleSlider, eVehicle, 0, 0, GET_IE_DROPOFF_FROM_IMPORT_EXPORT_WAREHOUSE(GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID())))
//			FILL_OWNED_VEHICLE_LIST()
//			FILL_VEHICLE_LIST()
//		ENDIF
//		
//		debug_vars.bSetIEVehicleAsOwned = FALSE
//	ENDIF
//	IF debug_vars.bClearOwnedIEVehicles
//		INT i
//		
//		REPEAT ciMAX_IE_OWNED_VEHICLES i
//			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iWarehouseVehicles[i] = 0
//			SET_MP_INT_CHARACTER_STAT(GET_IE_OWNED_WAREHOUSE_VEHICLE_STAT(i), 0)
//			SET_PACKED_STAT_INT(GET_STAT_ENUM_FOR_IE_VEHICLE_HISTORY_LIST(i), 0)
//			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.iVehicleTotal = 0
//		ENDREPEAT
//		
//		FILL_OWNED_VEHICLE_LIST()
//		
//		debug_vars.bClearOwnedIEVehicles = FALSE
//	ENDIF
//ENDPROC
#ENDIF //IS_DEBUG_BILD
 //FEATURE_IMPORT_EXPORT

//***********SCRIPT***********
SCRIPT
	CPRINTLN(DEBUG_INTERNET, "<APP_IE> started \"", GET_THIS_SCRIPT_NAME(), "\"")
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("appImportExport")) > 1
	
		CPRINTLN(DEBUG_SAFEHOUSE, "<APP_IE> appImportExport script already running! cleaning up.")
		CLEANUP_IE_APP(TRUE, FALSE)
		
	ENDIF
	
	siMovie = REQUEST_SCALEFORM_MOVIE("IMPORT_EXPORT_WAREHOUSE")
			
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	DISABLE_CELLPHONE(TRUE)
	
	Pause_Objective_Text()
	THEFEED_PAUSE()
	
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(siMovie))
		WAIT(0)
	ENDWHILE
	
	g_bIEAppVisible = TRUE
	g_bIEAppFailedRemoveVehicles = FALSE
	
	IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		eIEGarageID = GET_IE_GARAGE_PLAYER_IS_IN(PLAYER_ID())
		UNUSED_PARAMETER(eIEGarageID)
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	SETUP_PRIVATE_IE_APP_WIDGETS(eLocalWidgetVars)
//	#ENDIF

	g_bLaunchedPreSellVehModFromIEApp = FALSE
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	REMOVE_ALL_GANG_MEMBERS_FROM_VEH_SLOT()
	
	SEND_INITIAL_DATA_TO_SCALEFORM()
	
	IF IS_PC_VERSION()
		SET_MULTIHEAD_SAFE(TRUE, TRUE, TRUE, TRUE)
	ENDIF
	
	UPDATE_IE_WEBSITE_TUTORIAL_BS()
	
	IF HAVE_ALL_IEAPP_HELP_TUTORIALS_BEEN_COMPLETED()
		SET_BIT(iBS, AH_BS_HELP_TEXT_TUTORIAL_COMPLETE)
	ENDIF
	
	IF HAVE_ALL_DIALOGUE_TUTORIALS_BEEN_COMPLETED()
		SET_BIT(iBS, AH_BS_DIALOGUE_TUTORIAL_COMPLETE)
	ENDIF
	
	APPIE_SETUP_TUTORIAL_DIALOGUE()
	START_AUDIO_SCENE("GTAO_Computer_Screen_Active_Scene")
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARKNETACCESSED, TRUE)
	#ENDIF
	
	/// main update and render loop
	WHILE g_bIEAppVisible
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> Shutting down! Player too far from the coords")	
			CLEANUP_IE_APP()
		ELIF IS_PAUSE_MENU_ACTIVE()
			IF eAppStage = eAppStage_SelectingOptions
				CPRINTLN(DEBUG_INTERNET, "<APP_IE> Shutting down! Player opend the pause menu")	
				CLEANUP_IE_APP()
			ENDIF
		ELIF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> Shutting down! No network game is currently active")	
			CLEANUP_IE_APP()
		ELIF IS_ENTITY_DEAD(PLAYER_PED_ID())
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> Shutting down! Player is injured or dead")	
			CLEANUP_IE_APP()
		ELIF NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		#IF FEATURE_CASINO_HEIST
		AND NOT WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
		#ENDIF
			CPRINTLN(DEBUG_INTERNET, "<APP_IE> Shutting down! Player is not inside IE warehouse")
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IEAPPINPUTTRIG")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IEAPPINPUTTRPC")
				CLEAR_HELP()
			ENDIF
			
			CLEANUP_IE_APP()
		ENDIF
		
		SWITCH eAppStage
			CASE eAppStage_SelectingOptions
				
				RENDER_IE_APP()
				
				IF NOT IS_BIT_SET(iBS, AH_BS_DIALOGUE_TUTORIAL_COMPLETE)
					APPIE_SELECT_APP_DIALOGUE_TUTORIAL()
					APPIE_RUN_TUTORIAL_DIALOGUE()
				ENDIF
				
				IF NOT IS_BIT_SET(iBS, AH_BS_HELP_TEXT_TUTORIAL_COMPLETE)
					RUN_IEAPP_HELP_TEXT_TUTORIAL()
				ENDIF
				
				//This flag is set when this script has sent the event to start vehicle moddification
				//We have to wait for the Modshop script to process this event for all (up to 4) players before we continue.
				IF NOT bWaitingForCarmodEventToProcess
					#IF IS_DEBUG_BUILD
						//UPDATE_DEBUG_WIDGETS(eLocalWidgetVars)
					#ENDIF
					
					PASS_INPUTS_TO_SCALEFORM()
					CHECK_BUTTON_PRESS()
				ELSE
					IF IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID())
					AND NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
						CPRINTLN(DEBUG_INTERNET, "<APP_IE> SET_DATA_FOR_PRE_MISSION_LAUNCH_MODS: Options selected moving on to eAppStage_WaitingForVehicleMod")
						eAppStage = eAppStage_WaitingForVehicleMod
						
						IF IS_PC_VERSION()
							SET_MULTIHEAD_SAFE(FALSE, TRUE, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eAppStage_WaitingForVehicleMod
				Unpause_Objective_Text()
				
				IF NOT HAS_NET_TIMER_STARTED(stModShopTimeOut)
					START_NET_TIMER(stModShopTimeOut)
				ELIF HAS_NET_TIMER_EXPIRED(stModShopTimeOut, 301000)
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> STAGE: eAppStage_WaitingForVehicleMod 5 minute timer expired sutting down!")
					g_bLaunchedPreSellVehModFromIEApp = FALSE
					CLEANUP_IE_APP()
				ENDIF
				
				IF IS_PERSONAL_CAR_MOD_IS_READY_TO_START_MISSION(PLAYER_ID())
					eAppStage = eAppStage_LaunchingMission
					RESET_NET_TIMER(stModShopTimeOut)
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> STAGE: eAppStage_WaitingForVehicleMod Modding finished, launching mission")
				ELIF NOT IS_MISSION_PERSONAL_CAR_MOD_STARTED(PLAYER_ID())
					CPRINTLN(DEBUG_INTERNET, "<APP_IE> STAGE: eAppStage_WaitingForVehicleMod IS_MISSION_PERSONAL_CAR_MOD_STARTED = FALSE sutting down!")
					g_bLaunchedPreSellVehModFromIEApp = FALSE
					CLEANUP_IE_APP()
				ENDIF
			BREAK
			CASE eAppStage_LaunchingMission
				IF CAN_PLAYER_LAUNCH_MISSION(IS_LOCAL_PLAYER_SELLING_VEHICLE_SET(), TRUE)
					LAUNCH_MISSION()
					WHILE IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
						WAIT(0)
					ENDWHILE
					CLEANUP_IE_APP(DEFAULT, FALSE)
				ELSE
					CDEBUG1LN(DEBUG_INTERNET, "<APP_IE> app in stage eAppStage_LaunchingMission but CAN_PLAYER_LAUNCH_MISSION returned false!")
					IF iMissionUnavailableReason = ciFailReasonTooFewPlayers
						g_iFailedToLaunchIEVehicleSellMission = 0
					ELSE
						g_iFailedToLaunchIEVehicleSellMission = 1
					ENDIF
				ENDIF
				
				CLEANUP_IE_APP()
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
	
	// Script should never reach here. Always terminate with cleanup function.
	CASSERTLN(DEBUG_INTERNET, "<APP_IE> \"", GET_THIS_SCRIPT_NAME(), "\" should never reach here. Always terminate with cleanup function.")
ENDSCRIPT

