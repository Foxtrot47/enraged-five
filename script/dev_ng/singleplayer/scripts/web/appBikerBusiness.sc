USING "rage_builtins.sch"
USING "globals.sch"

USING "cellphone_public.sch"
USING "commands_zone.sch"

USING "website_private.sch"
USING "net_realty_warehouse.sch"
USING "net_gang_boss.sch"
USING "net_realty_biker_warehouse.sch"

//***********Script CONSTS***********

CONST_INT ciFailReasonCriticalToJob 		13
CONST_INT ciFailReasonNotBoss 				14
CONST_INT ciINPUT_KEYBOARD_DOWN				300
CONST_INT ciINPUT_KEYBOARD_UP				301

//App page ID's
CONST_INT ciUnknownPage		-1
CONST_INT ciBusinessesPage 	1
CONST_INT ciSummaryPage		2
CONST_INT ciPurchasePage	3
CONST_INT ciMangementPage	4
CONST_INT ciSplashPage		5

//Transaction fail consts
CONST_INT ciFailReasonFactoryEmpty 		0
CONST_INT ciFailReasonNotEnoughMoney 	1
CONST_INT ciFailReasonFAOwned			2
CONST_INT ciFailReasonTransFail			3
CONST_INT ciFailReasonMissionNotAvail	4
CONST_INT ciFailReasonFailedTrade		5

ENUM BIKER_MENU_STAGE
	eSelectFactoryType,		//Buy Facrtory or Buy Mission
	eSelectFactoryOptions,	//Stop production and sell factory options
	eSelectFactoryUpgrades,
	eSelectFactory,
	eConfirm
ENDENUM

//***********Script variables***********
VECTOR vPlayerCoords

BOOL bRequestSave				= FALSE
BOOL bInputConsumed 			= TRUE
BOOL bCheckPageID				= TRUE
BOOL bGoHome					= FALSE
BOOL bOverlayScreenActive 		= FALSE
BOOL bDisplayedBuyHelpText		= FALSE
BOOL bCursorInputProcessed 		= FALSE
BOOL bWaitingForSelectionReturn = FALSE

INT iLastHiglightedButton		= -1
INT iLastButtonAction			= 0
INT iCurrentPageID				= ciUnknownPage
INT iLaunchedMissionID			= -1
INT iMaterialsTotalLastRefresh	= 0
INT iUpgradeTransactionState	= GENERIC_TRANSACTION_STATE_DEFAULT

FACTORY_ID			eCurrentFactoryID	= FACTORY_ID_INVALID
FACTORY_UPGRADE_ID 	eJustPurchasedUpgrade		= UPGRADE_ID_INVALID	//Used to delay the app exit for url:bugstar:3015579

SCRIPT_TIMER stExitDelay
SCRIPT_TIMER stMissionStartTImer	//Timer to check a mission is taking to long to launch

structPedsForConversation tutPedStruct

SCALEFORM_INDEX mov
SCALEFORM_RETURN_INDEX currentSelectionReturnIndex
SCALEFORM_RETURN_INDEX currentRolloverReturnIndex
SCALEFORM_RETURN_INDEX currentBusinessReturnIndex
SCALEFORM_RETURN_INDEX CurrentScreenReturnIndex

//Control input variables
INT iLeftX
INT iLeftY
INT iRightX
INT iRightY

//Transaction variables
BOOL 	bProcessingBasket 		= FALSE
INT 	iProcessingBasketStage 	= SHOP_BASKET_STAGE_ADD
INT 	iTransactionFailReason 	= -1

CONTRABAND_TRANSACTION_STATE eTransactionState

//***********Script functions***********
PROC CLEANUP_BIKER_APP(BOOL bDontCleanupBrowserFlag = FALSE)
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	
	SET_BROWSER_OPEN(bDontCleanupBrowserFlag)
	
	DISABLE_CELLPHONE(FALSE)
	
	Unpause_Objective_Text()
	
	SET_AUDIO_SCRIPT_CLEANUP_TIME(1000)
	PLAY_SOUND_FRONTEND(-1, "Exit", "DLC_Biker_Computer_Sounds")
	
	IF IS_PC_VERSION()
		SET_MULTIHEAD_SAFE(FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	IF NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL)
	AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_23 tlConvRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(tlConvRoot, "BPLES_TUT1")
			SET_PACKED_STAT_BOOL((PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL), FALSE)
			KILL_ANY_CONVERSATION()
		ENDIF
	ENDIF
	
	THEFEED_RESUME()
	
	IF bRequestSave
	AND NOT USE_SERVER_TRANSACTIONS()
		//Request a save
		//IF NETWORK_IS_ACTIVITY_SESSION()
		//	STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
		//ENDIF

		//REQUEST_SAVE(STAT_SAVETYPE_END_SHOPPING)
		g_b_BikeAppStatChangeMajor = TRUE
		PRINTLN("[SAVECOOLDOWN] RUN_BIKER_APP_SAVE_COOLDOWN - g_b_BikeAppStatChangeMajor - CLEANUP_BIKER_APP ")
	ENDIF
	
	g_sBusAppManagement.iRenderHandShakeFC = -1
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC APP_CONTROL_DISABLE()
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

	IF (NOT IS_SYSTEM_UI_BEING_DISPLAYED())
	AND (NOT IS_PAUSE_MENU_ACTIVE())
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
ENDPROC

PROC RENDER_BIKER_APP()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	APP_CONTROL_DISABLE()
	
	IF g_bInMultiplayer
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)//1544427
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	ENDIF
	
	THEFEED_HIDE_THIS_FRAME()
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_PAUSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_RECT_FROM_CORNER(0.0, 0.0, 1.0, 1.0, 0, 0, 0, 255)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov,255,255,255,255)
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	
	g_sBusAppManagement.iRenderHandShakeFC = GET_FRAME_COUNT()
ENDPROC

/// PURPOSE:
///    Checks if we own the property and if it has been disabled for any reason
FUNC BOOL IS_FACTORY_AVAILABLE_TO_BUY(FACTORY_ID eFactoryID)
	
	IF IS_FACTORY_PURCHASE_DISABLED(eFactoryID)
	OR DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactoryID)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_CURRENTLY_IN_FACTORY()
	RETURN eCurrentFactoryID != FACTORY_ID_INVALID
ENDFUNC

FUNC BOOL HAS_PLAYER_SETUP_THIS_FACTORY()
	RETURN HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(PLAYER_ID(), eCurrentFactoryID)
ENDFUNC

FUNC BOOL SHOULD_BUY_SUPPLIES_HELP_BE_DISPLAYED()
	IF NOT bDisplayedBuyHelpText
	AND GET_PACKED_STAT_INT(PACKED_MP_INT_OPEN_ROAD_BUY_TUTORIAL) < 3
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the mission unavailable value
FUNC BOOL CAN_PLAYER_LAUNCH_MISSION(INT iMissionToLaunch)
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), iMissionToLaunch, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> CHECK_IF_PLAYER_CAN_DO_MISSION: purchase failed, mission unavailable")
		//iMissionUnavailableReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), iMissionToLaunch, FALSE)
		RETURN FALSE
	ENDIF
	
	IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> CHECK_IF_PLAYER_CAN_DO_MISSION: purchase failed, GB_IS_PLAYER_BOSS_OF_A_GANG is false")
		//iMissionUnavailableReason = ciFailReasonNotBoss
		RETURN FALSE
	ENDIF
	
	// Check we're not taking part in some other mission
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> CHECK_IF_PLAYER_CAN_DO_MISSION: purchase failed, player critical to job")
		//iMissionUnavailableReason = ciFailReasonCriticalToJob
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> CHECK_IF_PLAYER_CAN_DO_MISSION: purchase failed, NETWORK_IS_ACTIVITY_SESSION is true")
		//iMissionUnavailableReason = GB_MU_REASON_UNSUITABLE_SESSION
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESSING_APP_FACTORY_BASKET(INT &iProcessSuccess, INT iPrice, INT iStatValue, SHOP_ITEM_CATEGORIES eCategory, ITEM_ACTION_TYPES eAction, INT iItemId, INT iInventoryKey, INT iSellingPrice = 0, INT iSellingItemId = 0)
	UNUSED_PARAMETER(iTransactionFailReason)
	
	INT iTempPrice = (iPrice - iSellingPrice)
	
	IF bProcessingBasket
		SWITCH iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				
				IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
					PRINTLN("[BASKET] PROCESSING_APP_FACTORY_BASKET - Waiting for existing transaction")
					RETURN TRUE
				ENDIF
				
				// Check the players cash.
				IF iTempPrice > 0
				AND NOT NETWORK_CAN_SPEND_MONEY(iTempPrice, FALSE, TRUE, FALSE)
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					CASSERTLN(DEBUG_INTERNET, "[BASKET] PROCESSING_APP_FACTORY_BASKET - Player can't afford this item! iTempPrice=", iTempPrice)
					RETURN TRUE
				ENDIF
				
				CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding factory to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iPrice:$", iPrice, ", iItemId:", iItemId, ", iInventoryKey:", iInventoryKey)
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
				
					
					IF iSellingItemId = HASH("WH_INDEX_0_t0_v0")
						SCRIPT_ASSERT("PROCESSING_APP_FACTORY_BASKET - You are trading in an invalid factory!")
					ENDIF
				
					// factory we are trading in.
					IF iSellingItemId != 0
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding trade factory to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iSellingPrice:$", iSellingPrice)
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iSellingItemId, eAction, 1, iSellingPrice, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
							//
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Failed to add selling item to basket")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket checkout started")							
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to start basket checkout")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to add item to basket")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, success!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, failed!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS
				//DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) -- we delete this later
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
//				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 2
				RETURN FALSE
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 0
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF

	IF iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		CPRINTLN(DEBUG_INTERNET, "[BASKET] - was FORCE_CLOSE_BROWSER called to termiate the transaction? Set success to 0")
		
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		iProcessSuccess = 0
		RETURN FALSE
	ENDIF

	iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	iProcessSuccess = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL BUY_MP_FACTORY_FROM_OPENROAD(FACTORY_ID eFactoryId, INT &iResultSlot, FACTORY_ID eTradeInFactory = FACTORY_ID_INVALID)
	
	INT minPendingTransactionValue, finalPendingTransactionValue, iOwnedFactoryValue	
	
	IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactoryId)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: purchase invalid, we already own factory: ", eFactoryId)
		PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
		
		iTransactionFailReason = ciFailReasonFAOwned
		
		RETURN FALSE
	ENDIF
	
	TEXT_LABEL_15 tl_15FactoryName = GET_FACTORY_NAME_FROM_ID(eFactoryId)
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: trying to purchase factory #", eFactoryId, " \"", tl_15FactoryName, "\"")
	
	INT propval = CEIL(TO_FLOAT(GET_FACTORY_PRICE(eFactoryId))*g_sMPTunables.fPropertyMultiplier)
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: purchase valid, attempting index #", eFactoryId)
	
	minPendingTransactionValue = propval
	
		BOOL bShouldBeFree = SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryID)
		
		IF bShouldBeFree
			propval = 0
			minPendingTransactionValue = 0
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD - Factory ID: ", eFactoryID, " is free")
		ENDIF
	
	//is it a debit or refund
	IF eTradeInFactory = FACTORY_ID_INVALID
	OR eTradeInFactory = FACTORY_ID_MAX
		IF minPendingTransactionValue > 0
			//do they have enough money
			IF NOT NETWORK_CAN_SPEND_MONEY(minPendingTransactionValue,FALSE,TRUE,FALSE)
				CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: purchase failed, player doesn't have sufficient cash")
				//TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15FactoryName), minPendingTransactionValue)
				
				iTransactionFailReason = ciFailReasonNotEnoughMoney
				
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		iResultSlot = GET_SAVE_SLOT_FOR_FACTORY(eTradeInFactory)
		
		IF iResultSlot = -1
		//	g_iPendingBrowserTimer = g_iBrowserTimer
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: Player backed out of factory selection")
			
			iTransactionFailReason = ciFailReasonFailedTrade
			
			RETURN FALSE
		ELSE
			iOwnedFactoryValue = GET_FACTORY_SALE_VALUE(eTradeInFactory)
			
			finalPendingTransactionValue = propval - iOwnedFactoryValue
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER><BIKER_TRADE> BUY_MP_FACTORY_FROM_OPENROAD: Player selected factory: ", eTradeInFactory, " for trade. price before trade: ", propval, " Old Factory trade val: ", iOwnedFactoryValue, " Final trans value: ", finalPendingTransactionValue)
		ENDIF
	
		// Re-check if player can afford based on selected trade-in value
		IF finalPendingTransactionValue > 0
			//do they have enough money			
			IF NOT NETWORK_CAN_SPEND_MONEY(finalPendingTransactionValue, FALSE, TRUE, FALSE)
				CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: purchase failed, player failed on command NETWORK_CAN_SPEND_MONEY")
				TRIGGER_BROWSER_NAG_SCREEN(GET_HASH_KEY(tl_15FactoryName), finalPendingTransactionValue)
				
				iTransactionFailReason = ciFailReasonNotEnoughMoney
				
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		bProcessingBasket 		= TRUE
		iProcessingBasketStage 	= SHOP_BASKET_STAGE_ADD
		INT iItemId 			= GET_FACTORY_KEY_FOR_CATALOGUE(eFactoryId)
		INT iInventoryKey 		= GET_FACTORY_INVENTORY_KEY_FOR_CATALOGUE(iResultSlot)
		INT iSellingItemId 		= 0
		
			IF bShouldBeFree
				iItemId = GET_FACTORY_KEY_FOR_CATALOGUE(eFactoryId, TRUE)
			ENDIF
		
		IF eTradeInFactory != FACTORY_ID_INVALID
		AND eTradeInFactory != FACTORY_ID_MAX
			BOOL bStarterPackItem = FALSE
			
				//This was a premium edition purchase so we need to make sure they don't get any trade price for this factory
				IF SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eTradeInFactory)
					bStarterPackItem = TRUE
				ENDIF
			
			iSellingItemId = GET_FACTORY_KEY_FOR_CATALOGUE(eTradeInFactory, bStarterPackItem)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_15 tlLastClickedfactoryIndex = tl_15FactoryName
			IF IS_STRING_NULL_OR_EMPTY(tlLastClickedfactoryIndex)
				CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: label for last clicked factory index #", eFactoryId, " is empty!")
			ELSE
				//CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: label for last clicked factory index #", eFactoryId, " is \"", tlLastClickedfactoryIndex, "\" (iReplacefactory: ", iReplacefactory, ")")
			ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		///////////////////////////////////////////
		///   TRANSACTION FOR factory
		WHILE PROCESSING_APP_FACTORY_BASKET(iProcessSuccess, propval, iResultSlot, CATEGORY_INVENTORY_WAREHOUSE, NET_SHOP_ACTION_BUY_WAREHOUSE, iItemId, iInventoryKey, iOwnedFactoryValue, iSellingItemId)
			RENDER_BIKER_APP()
			WAIT(0)
		ENDWHILE
		
		RENDER_BIKER_APP()
		
		SWITCH iProcessSuccess
			CASE 0
				CWARNINGLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: failed to process transaction")
				PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
				
				iTransactionFailReason = ciFailReasonTransFail
				
				RETURN FALSE
			BREAK
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: success!!")
			BREAK
			DEFAULT
				
				iTransactionFailReason = ciFailReasonTransFail
				CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
				RETURN FALSE
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: final:$", finalPendingTransactionValue)
	
	SPEND_BUSINESS_PROPERTY data
	
	//Assign the telemetry data
	data.m_businessID			= GET_HASH_KEY(GET_FACTORY_NAME_FROM_ID(eFactoryId))
	data.m_businessType			= ENUM_TO_INT(GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryId))
	data.m_businessUpgradeType	= -1
	
	//Increment the spending stat
	IF finalPendingTransactionValue > 0
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, finalPendingTransactionValue)
	ENDIF
	
	eTradeInFactory = GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iResultSlot)
	
	SET_FACTORY_PAID_FOR_PURCHASE_PRICE(iResultSlot, propval)
	
	//Check if we're trading of just buying
	IF (eTradeInFactory = FACTORY_ID_INVALID)
	OR (eTradeInFactory = FACTORY_ID_MAX)
		IF USE_SERVER_TRANSACTIONS()
			CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
			NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
		ENDIF
		
		NETWORK_SPENT_PURCHASE_BUSINESS_PROPERTY(propval, data, FALSE, TRUE)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_FACTORY_FROM_OPENROAD: Bought Factory ", tl_15FactoryName, " for $", finalPendingTransactionValue)
	ELSE
		IF iOwnedFactoryValue > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER><BIKER_TRADE> BUY_MP_FACTORY_FROM_OPENROAD: Trading the business is earning the player: ", (iOwnedFactoryValue * -1))
			NETWORK_EARN_FROM_PROPERTY(iOwnedFactoryValue, GET_HASH_KEY(GET_FACTORY_NAME_FROM_ID(eTradeInFactory)))
		ENDIF
		
		IF propval > 0
			IF USE_SERVER_TRANSACTIONS()
				CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOPS, "<APP_BIKER><BIKER_TRADE> BUY_MP_FACTORY_FROM_OPENROAD: Trading with cost: ", finalPendingTransactionValue, " Sending telemtry cost as: ", propval)
			NETWORK_SPENT_TRADE_BUSINESS_PROPERTY(propval, data, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL SELL_MP_CONTRABAND_FROM_OPENROAD(FACTORY_ID eFactoryID)
	
	INT iResultSlot = GET_SAVE_SLOT_FOR_FACTORY(eFactoryID)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl_15ContrabandName = GET_FACTORY_NAME_FROM_ID(eFactoryID)
	#ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: trying to sell contraband for factory #", eFactoryID, " \"", tl_15ContrabandName, "\", in slot ", iResultSlot)
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_SELL, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: purchase failed, mission not available")
		
		iTransactionFailReason = ciFailReasonMissionNotAvail
		
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: purchase failed, player is critical")
		
		iTransactionFailReason = ciFailReasonCriticalToJob
		
		RETURN FALSE
	ENDIF
	
	IF IS_FACTORY_EMPTY(eFactoryID)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: factory is empty")
		
		iTransactionFailReason = ciFailReasonFactoryEmpty
		
		RETURN FALSE
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
	
		/*
		BuyContrabandMission Ã¢â‚¬â€œ 
                This is used to fill/set the CATEGORY_INVENTORY_CONTRABAND_MISSION
                It deducts money for the Ã¢â‚¬ËœpurchasingÃ¢â‚¬â„¢ of the mission
                Basket will be < CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION, price>
                Validation must make sure the player owns the factory this CATEGORY_INVENTORY_CONTRABAND_MISSION is associated with

		*/
	
		bProcessingBasket = TRUE
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_FACTORY_MISSION_KEY_FOR_CATALOGUE(TRUE)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedCONTRABANDIndex = tl_15ContrabandName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedCONTRABANDIndex)
			CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: label for last clicked CONTRABAND index #", eFactoryID, " is empty!")
		ELSE
			//CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: label for last clicked CONTRABAND index #", eFactoryID, " is \"", tlLastClickedCONTRABANDIndex, "\" (iReplaceCONTRABAND: ", iReplaceCONTRABAND, ")")
		ENDIF
		#ENDIF
		
		///////////////////////////////////////////
		///   TRANSACTION FOR CONTRABAND
		WHILE NOT PROCESS_TRANSACTION_FOR_CONTRABAND_MISSION(iResultSlot, iItemId, eTransactionState)
			WAIT(0)
			RENDER_BIKER_APP()
		ENDWHILE
		
		RENDER_BIKER_APP()
		
		IF eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
			CWARNINGLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: failed to process transaction")
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
			
			iTransactionFailReason = ciFailReasonMissionNotAvail
			
			RETURN FALSE
		ELIF eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
		
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: success!!")
			SET_LAST_PURCHASED_FACTORY_MISSION_CATALOGUE_KEY(iItemId)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BUY_MP_SUPPLIES_FROM_OPENROAD(FACTORY_ID eFactoryID)
	
	INT iResultSlot = GET_SAVE_SLOT_FOR_FACTORY(eFactoryID)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl_15ContrabandName = GET_FACTORY_NAME_FROM_ID(eFactoryID)
	#ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: trying to buy contraband for factory #", eFactoryID, " \"", tl_15ContrabandName, "\", in slot ", iResultSlot)
	
	// Check that mission is available
	IF NOT GB_IS_BOSS_MISSION_AVAILABLE(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_SELL, FALSE)
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: purchase failed, mission not available")
		
		iTransactionFailReason = ciFailReasonMissionNotAvail
		
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: purchase failed, player is critical")
		
		iTransactionFailReason = ciFailReasonCriticalToJob
		
		RETURN FALSE
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
	
		/*
		BuyContrabandMission Ã¢â‚¬â€œ 
                This is used to fill/set the CATEGORY_INVENTORY_CONTRABAND_MISSION
                It deducts money for the Ã¢â‚¬ËœpurchasingÃ¢â‚¬â„¢ of the mission
                Basket will be < CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION, price>
                Validation must make sure the player owns the factory this CATEGORY_INVENTORY_CONTRABAND_MISSION is associated with

		*/
	
		bProcessingBasket = TRUE
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		INT iItemId = GET_FACTORY_MISSION_KEY_FOR_CATALOGUE(FALSE)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedCONTRABANDIndex = tl_15ContrabandName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedCONTRABANDIndex)
			CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: label for last clicked CONTRABAND index #", eFactoryID, " is empty!")
		ELSE
			//CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> SELL_MP_CONTRABAND_FROM_APP: label for last clicked CONTRABAND index #", eFactoryID, " is \"", tlLastClickedCONTRABANDIndex, "\" (iReplaceCONTRABAND: ", iReplaceCONTRABAND, ")")
		ENDIF
		#ENDIF
		
		///////////////////////////////////////////
		///   TRANSACTION FOR CONTRABAND
		WHILE NOT PROCESS_TRANSACTION_FOR_CONTRABAND_MISSION(iResultSlot, iItemId, eTransactionState)
			WAIT(0)
			RENDER_BIKER_APP()
		ENDWHILE
		
		RENDER_BIKER_APP()
		
		IF eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
			CWARNINGLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: failed to process transaction")
			PLAY_SOUND_FRONTEND(-1, "Popup_Confirm_Fail", "GTAO_Exec_SecuroServ_Computer_Sounds") 
			
			iTransactionFailReason = ciFailReasonMissionNotAvail
			
			RETURN FALSE
		ELIF eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
		
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: success!!")
			SET_LAST_PURCHASED_FACTORY_MISSION_CATALOGUE_KEY(iItemId)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BUY_MP_SUPPLIES_FROM_OPENROAD: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_TRANSACTION_FAIL_REASON(INT iFailReason)
	SWITCH iFailReason
		CASE ciFailReasonFactoryEmpty 		RETURN "BKR_TF_R1"
		CASE ciFailReasonNotEnoughMoney 	RETURN "BKR_TF_R2"
		CASE ciFailReasonCriticalToJob		RETURN "BKR_TF_R3"
		CASE ciFailReasonFAOwned			RETURN "BKR_TF_R4"
		CASE ciFailReasonTransFail			RETURN "BKR_TF_R5"
		CASE ciFailReasonMissionNotAvail	RETURN "BKR_TF_R6"
	ENDSWITCH
	
	RETURN "BKR_TF_R1"
ENDFUNC

PROC LAUNCH_MISSION(INT iMissionToLaunch, FACTORY_ID eFactory, BIKER_SELL_LOCALITY eLocalSellMission = SELL_LOCALITY_INVALID)
	IF CAN_PLAYER_LAUNCH_MISSION(iMissionToLaunch)
		CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> LAUNCH_MISSION of type: ", iMissionToLaunch, " for Factory: ", eFactory)
		GB_BOSS_REQUEST_ILLICIT_GOODS_MISSION_LAUNCH_FROM_SERVER(iMissionToLaunch, eFactory, eLocalSellMission)
		IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			g_bLaunchedMissionFrmMCT = TRUE
		ENDIF
		g_sBIK_Telemetry_data.m_FromHackerTruck = 0
		iLaunchedMissionID = iMissionToLaunch
		g_bLaunchedMissionFrmLaptop = TRUE
	ENDIF
ENDPROC

PROC LAUNCH_SETUP_MISSION(FACTORY_ID eFactory)
	IF CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_BUY)
		CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> LAUNCH_SETUP_MISSION for Factory: ", eFactory)
		GB_BOSS_REQUEST_BIKER_BUSINESS_SETUP_MISSION_LAUNCH_FROM_SERVER(eFactory)
		g_sBIK_Telemetry_data.m_FromHackerTruck = 0
		g_bLaunchedMissionFrmLaptop = TRUE
	ENDIF
ENDPROC

FUNC BOOL RUN_TRANSACTION_FOR_FACTORY_UPGRADE(FACTORY_UPGRADE_ID eUpgrade)

	BOOL bSuccess = TRUE
	BOOL bDeleteTransaction = TRUE
	
	INT 			iItemCost 		= GET_FACTORY_UPGRADE_COST(eUpgrade, eCurrentFactoryID)
	INT 			iUpgradeID 		= ENUM_TO_INT(eUpgrade) 
	INT 			iSaveSlot		= GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
	INT				iInventoryKey 	= GET_FACTORY_MOD_INVENTORY_KEY_FOR_CATALOGUE(iSaveSlot, (iUpgradeID + 1))	//+ 1 because 0 will give us the setup mission key		
	FACTORY_TYPE 	eFactoryType	= GET_GOODS_CATEGORY_FROM_FACTORY_ID(eCurrentFactoryID)
	INT				iItemHash		= GET_FACTORY_MOD_KEY_FOR_CATALOGUE(eFactoryType, (iUpgradeID + 1), 1)

	CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> RUN_TRANSACTION_FOR_FACTORY_UPGRADE: Purchase upgrade: ", eUpgrade, " Inventory Key: ", iInventoryKey, " Item Hash: ", iItemHash, " Factory type: ", eFactoryType)
	
	IF USE_SERVER_TRANSACTIONS()
	
		bSuccess = FALSE	
		
		WHILE NOT PROCESS_GENERIC_ITEM_TRANSACTION(CATEGORY_INVENTORY_WAREHOUSE_INTERIOR, NET_SHOP_ACTION_BUY_WAREHOUSE, iItemHash, 1, iItemCost, iUpgradeTransactionState, iInventoryKey)
			WAIT(0)
			RENDER_BIKER_APP()
			PRINTLN("RUN_TRANSACTION_FOR_FACTORY_UPGRADE - Processing transaction")
			
			IF iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_PENDING
			AND GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
				iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_FAILED
				CDEBUG1LN(DEBUG_SAFEHOUSE, "RUN_TRANSACTION_FOR_FACTORY_UPGRADE - Transaction failed due to index = -1. Unable to purchase upgrade: ", eUpgrade)
				bDeleteTransaction = FALSE
				BREAKLOOP
			ENDIF
		ENDWHILE
		
		RENDER_BIKER_APP()
		
		IF (iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS)
			PRINTLN("RUN_TRANSACTION_FOR_FACTORY_UPGRADE - Transaction success")
			bSuccess = TRUE
		ELSE
			PRINTLN("RUN_TRANSACTION_FOR_FACTORY_UPGRADE - Transaction fail")
		ENDIF
		
		iUpgradeTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	IF bSuccess
		
		IF iItemCost > 0
			SPEND_BUSINESS_PROPERTY data
			
			data.m_businessID 			= GET_HASH_KEY(GET_FACTORY_NAME_FROM_ID(eCurrentFactoryID))
			data.m_businessType			= ENUM_TO_INT(eFactoryType)
			data.m_businessUpgradeType	= ENUM_TO_INT(eUpgrade)
			
			PRINTLN("RUN_TRANSACTION_FOR_FACTORY_UPGRADE - item cost: ",iItemCost)
			PRINTLN("RUN_TRANSACTION_FOR_FACTORY_UPGRADE - Call NETWORK_SPENT_UPGRADE_BUSINESS_PROPERTY")
			NETWORK_SPENT_UPGRADE_BUSINESS_PROPERTY(iItemCost,data, FALSE, TRUE)
		ENDIF
		
		//Set the flag to request a save when we leave the app
		bRequestSave = TRUE
		
		IF USE_SERVER_TRANSACTIONS()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF bDeleteTransaction
	AND USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_CEASE_PRODUCTION_TRANSACTION()
	INT iSaveSlot 	= GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
	
	IF USE_SERVER_TRANSACTIONS()
	AND GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), eCurrentFactoryID) > 0
		INT iMissionKey = GET_FACTORY_MISSION_KEY_FOR_CATALOGUE(TRUE)
		
		bProcessingBasket = TRUE
		
		WHILE NOT PROCESS_TRANSACTION_FOR_CONTRABAND_MISSION(iSaveSlot, iMissionKey, eTransactionState)
			WAIT(0)
			RENDER_BIKER_APP()
		ENDWHILE
		
		RENDER_BIKER_APP()
		
		IF eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
			eTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			INT iProductTotal = GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), eCurrentFactoryID)
			
			WHILE NOT PROCESS_TRANSACTION_FOR_REMOVE_PRODUCT(iSaveSlot, iProductTotal, 0, REMOVE_CONTRA_ATTACKED, eTransactionState)
				WAIT(0)
				RENDER_BIKER_APP()
			ENDWHILE
			
			SET_LAST_PURCHASED_FACTORY_MISSION_CATALOGUE_KEY(iMissionKey)
			RENDER_BIKER_APP()
			
			IF eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
				CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> Transaction Success! stopping production at factory: ", eCurrentFactoryID)
				CEASE_FACTORY_PRODUCTION(PLAYER_ID(), eCurrentFactoryID, TRUE, FALSE, FALSE)
				INCREMENT_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, FALSE, FALSE)
			ELSE
				CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> Transaction failed to remove product from business: ", eCurrentFactoryID)
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> Transaction failed to add mission to inventory")
		ENDIF
		
	ELSE
		CEASE_FACTORY_PRODUCTION(PLAYER_ID(), eCurrentFactoryID, TRUE, FALSE, FALSE)
		INCREMENT_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, FALSE, FALSE)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_FACTORY_STATUS_STRING(FACTORY_STATUS eStatus)
	SWITCH eStatus
		CASE unowned			RETURN "unowned"
		CASE active				RETURN "active"
		CASE PlayerSuspended	RETURN "Player Suspended"
		CASE suspended 			RETURN "suspended"
		CASE NotSetup			RETURN "Not Setup"
	ENDSWITCH
	
	RETURN "Status unknown!"
ENDFUNC
#ENDIF

FUNC INT GET_PERCENTAGE_AS_INT(INT a, INT b)
	RETURN CEIL((TO_FLOAT(a)/TO_FLOAT(b)) * 100)
ENDFUNC

/// PURPOSE:
///    Gets the product total as a percentage of the capacity for the given factory
FUNC INT GET_PRODUCT_TOTAL_PERCENTAGE(FACTORY_ID eFactoryID)
	INT iProductCapacity				= GET_FACTORY_PRODUCT_CAPACITY(eFactoryID)
	INT iStockCount						= GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(),eFactoryID)
	
	// Calculate the Capacity and Supply meters based on percentages
	FLOAT fProductQuantityPercent = (TO_FLOAT(iStockCount)/TO_FLOAT(iProductCapacity))*100
	
	RETURN ROUND(fProductQuantityPercent)
ENDFUNC

PROC ADD_FACTORY_TO_SCALEFORM_MOVIE(INT iFactoryID)
	
	FACTORY_ID eFactoryID = INT_TO_ENUM(FACTORY_ID, iFactoryID)
	
	IF IS_FACTORY_OF_TYPE_WEAPONS(eFactoryID)
		EXIT
	ENDIF
	
	BOOL bShouldBeFree = SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryID)
				 
	FACTORY_TYPE eFactoryType			= GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	FACTORY_STATUS eFactoryStatus		= GET_FACTORY_STATUS(eFactoryID)
	BOOL bCompletedSetup				= FALSE
	STRING sLocation					= GET_NAME_OF_ZONE(GET_FACTORY_DROP_OFF_COORDS(eFactoryID))
	TEXT_LABEL_15 tlDescription			= GET_FACTORY_DESCRIPTION_FROM_ID(eFactoryID, eFactoryType)
	INT iActiveChSlot					= GET_ACTIVE_CHARACTER_SLOT()
	INT iSaveSlot						= 0
	INT iPrice							= GET_FACTORY_PRICE(eFactoryID)
	INT iSalePrice						= -1
	INT iProductCapacity				= GET_FACTORY_PRODUCT_CAPACITY(eFactoryID)
	INT iStockCount						= GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(),eFactoryID)
	INT iStockValue						= GET_FACTORY_TOTAL_PRODUCT_VALUE(PLAYER_ID(), eFactoryID, iStockCount, TRUE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
	INT iSuppliesLevel					= 0
	INT iTotalSales 					= 0
	INT iTimesRaided					= 0
	INT iSuccessfulSales				= 0
	INT iSuccessfulResupplies			= 0
	INT iResupplyCost					= 0
	INT iResupplySaleCost				= -1
	INT iSuccessfulSellLS				= 0 		//Sell missions Los Santos
	INT iSuccessfulSellBC				= 0 		//Sell mission Blaine County
	INT iProductionStopCountResupply	= 0
	INT iProductionStopCountFull		= 0
	BOOL bResupplyButtonEnabled 		= FALSE
		
	IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactoryID)
		iSaveSlot 						= GET_SAVE_SLOT_FOR_FACTORY(eFactoryID)
		iSuccessfulResupplies			= GET_PERCENTAGE_AS_INT(GET_BIKER_RESUPPLY_COMPLETE_TOTAL_FOR_FACTORY(iActiveChSlot, iSaveSlot), GET_BIKER_RESUPPLY_UNDERTAKEN_TOTAL_FOR_FACTORY(iActiveChSlot, iSaveSlot))
		iSuccessfulSellLS				= GET_PERCENTAGE_AS_INT(GET_BIKER_SELL_COMPLETE_FOR_LS(iActiveChSlot, iSaveSlot), GET_BIKER_SELL_UNDERTAKEN_FOR_LS(iActiveChSlot, iSaveSlot))
		iSuccessfulSellBC				= GET_PERCENTAGE_AS_INT(GET_BIKER_SELL_COMPLETE_FOR_BC(iActiveChSlot, iSaveSlot), GET_BIKER_SELL_UNDERTAKEN_FOR_BC(iActiveChSlot, iSaveSlot))
		iSuccessfulSales				= GET_BIKER_SELL_MISSION_COMPLETE_FACTORY_TOTAL(iActiveChSlot, iSaveSlot)
		iTotalSales						= GET_BIKER_MISSION_EARNINGS(eFactoryID)
		iTimesRaided					= GET_COUNT_OF_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, FALSE, TRUE)
		iProductionStopCountResupply 	= GET_COUNT_OF_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, FALSE, FALSE)
		iProductionStopCountFull		= GET_COUNT_OF_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, TRUE, TRUE)
		iSuppliesLevel 					= g_iMaterialsTotalForFactory[iSaveSlot]
		iResupplyCost					= GET_FACTORY_PAID_RESUPPLY_COST(FALSE, iSuppliesLevel)
		
		bCompletedSetup = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage > 0
		
		IF bCompletedSetup
		OR CAN_PLAYER_LAUNCH_RESUPPLY_MISSION(eFactoryID)
			bResupplyButtonEnabled = TRUE
		ENDIF
	ENDIF
	
	// Calculate the Capacity and Supply meters based on percentages
	FLOAT fProductQuantityPercent = (TO_FLOAT(iStockCount)/TO_FLOAT(iProductCapacity))*100
	INT iStockLevel = ROUND(fProductQuantityPercent)
	
	IF iSuppliesLevel < 0
		iSuppliesLevel = 0
	ENDIF
	
	IF IS_FACTORY_ON_SALE(eFactoryID)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<APP_BIKER> Factory on sale: ", eFactoryID)
		iSalePrice 	= iPrice
		iPrice 		= GET_FACTORY_BASE_PRICE(eFactoryID)
	ENDIF
	
	IF IS_FACTORY_PAID_RESUPPLY_ON_SALE(iSuppliesLevel)
		iResupplySaleCost 	= iResupplyCost
		iResupplyCost		= GET_FACTORY_PAID_RESUPPLY_COST(TRUE, iSuppliesLevel)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<APP_BIKER> Paid resupply on sale. cost: ", iResupplyCost)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<APP_BIKER> Paid resupply not on sale. cost: ", iResupplyCost)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> ADD_FACTORY_TO_SCALEFORM_MOVIE(1): ",iFactoryID, " Name: ", GET_FACTORY_NAME_FOR_DEBUG(eFactoryID))
	CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> ADD_FACTORY_TO_SCALEFORM_MOVIE(2): status: ", DEBUG_GET_FACTORY_STATUS_STRING(eFactoryStatus), " price: ", iPrice,
								" stockLevel: ", iStockLevel, " stockValue: ", iStockValue, " suppliesLevel: ", iSuppliesLevel, 
								" Setup: ", HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(PLAYER_ID(), eCurrentFactoryID))
	#ENDIF
	
	iMaterialsTotalLastRefresh = iSuppliesLevel
	
		IF bShouldBeFree
			PRINTLN("<APP_BIKER> ADD_FACTORY_TO_SCALEFORM_MOVIE - Factory: ", iFactoryID, " is free to the player")
			iPrice = 0
			iSalePrice = -1
		ENDIF
											
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_BUSINESS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)					//	Unique ID representing the business.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eFactoryType))		//	What kind of business it is: FAKE ID(0), WEED(1), CASH (2), METH (3), CRACK (4).
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlDescription)		//	The description text labels are the same as the TXD reference
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sLocation)			// 	Area description of where the business is located, eg Strawberry.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(tlDescription)		//	Longer description of the business.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eFactoryStatus))	// 	Current business status:
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPrice)						//	How much the business costs to buy at full price.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSalePrice)					//	How much the business costs to buy at sale price. Pass in -1 if there is no sale price.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStockLevel)					//	How much stock is currently held in the business as a percentage (ie range 0-100).
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStockValue)					//	How much the stock is worth, in dollars.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuppliesLevel)				//	Current levels of supplies as a percentage (ie range 0-100).
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCompletedSetup)				//	True if the resupply button can be enabled
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalSales)					// 	How much money has been made through sales by this business.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTimesRaided)					//	How many times this business has been raided.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuccessfulSales)				//	How many sales this business has made.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bResupplyButtonEnabled)		//	If false, the management screen's setup/resupply button is greyed out but still clickable.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iResupplyCost)					//	Price displayed on the buy supplies button on the management page.				
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iResupplySaleCost)				//	SALE Price displayed on the buy supplies button on the management page.	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShouldBeFree)				//	Should this be free via the starter pack?
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_BUSINESS_STATS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)					//	Unique ID representing the business.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuccessfulResupplies)			//	Percentage of resupplies that were successful.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuccessfulSellLS)				//	Percentage of sales in Los Santos that were successful.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSuccessfulSellBC)				//	Percentage of sales in Blaine County that were successful.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iProductionStopCountResupply)	//	Number of times production has ceased due to running out of supplies.
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iProductionStopCountFull)		//	Number of times production has ceased due to running out of space.
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_START_PRODUCTION_STATUS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bCompletedSetup)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_PRODUCT_BUYER(INT iFactoryID, INT iBuyerID, STRING buyerName, STRING amount, INT price, BOOL bAvailable)
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_BUSINESS_BUYER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBuyerID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(buyerName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(amount)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(price)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_BUSINESS_BUYER_STATUS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBuyerID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC ADD_FACTORY_UPGRADE(INT iFactoryID, INT index, STRING description, INT price, INT iSalePrice, BOOL bUpgradeOwned, STRING sTXD)

	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "ADD_BUSINESS_UPGRADE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(description)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(price)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSalePrice)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_BUSINESS_UPGRADE_STATUS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFactoryID)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(index)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUpgradeOwned)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SEND_PLAYER_DATA(BOOL bInsideFactory)

	STRING sMCName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(PLAYER_ID())
	
	IF IS_STRING_NULL_OR_EMPTY(sMCName)
		sMCName = GET_DEFAULT_BIKER_GANG_NAME()
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_PLAYER_DATA")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sMCName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bInsideFactory)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC GET_PRODUCT_BUYER_DETAILS(BOOL bCityBuyer, TEXT_LABEL_15 &sBuyer, TEXT_LABEL_15 &sBuyerDetails)
	IF bCityBuyer
		sBuyer 			= "OR_GOODS_BYR2"
		sBuyerDetails	= "OR_BYR_DETAILS2"
	ELSE
		sBuyer 			= "OR_GOODS_BYR1"
		sBuyerDetails	= "OR_BYR_DETAILS1"
	ENDIF
ENDPROC

PROC SEND_INITIAL_DATA_TO_SCALEFORM()

	// Populate with business data.
	// If you pass in true to SEND_PLAYER_DATA, the scaleform movie will only show the management screen
	
	IF IS_PLAYER_CURRENTLY_IN_FACTORY()
		
		CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> **** BEGIN SEND_INITIAL_DATA_TO_SCALEFORM - Factory ****")
		
		SEND_PLAYER_DATA(TRUE)
		
		INT iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
		INT iFactoryID = ENUM_TO_INT(eCurrentFactoryID)
		FACTORY_TYPE eFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eCurrentFactoryID)
		
		ADD_FACTORY_TO_SCALEFORM_MOVIE(iFactoryID)
		
		//Pass the info for sell missions
		IF NOT IS_FACTORY_EMPTY(eCurrentFactoryID)
			INT iInitialStockVal 	= GET_FACTORY_TOTAL_PRODUCT_VALUE(PLAYER_ID(), eCurrentFactoryID, GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(),eCurrentFactoryID), TRUE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			FLOAT fStockVal 		= (TO_FLOAT(iInitialStockVal) * g_sMPTunables.fBiker_Sell_Product_Local_Reward_Modifier)			
			INT iFinalSaleValue 	= ROUND(fStockVal)
			
			TEXT_LABEL_15 sBuyerLocation, sBuyerName
			
			//Buyser one is always the closer (If we're in the city or docks then los_santos)
			GET_PRODUCT_BUYER_DETAILS(IS_CLOSEST_BUYER_IN_LOS_SANTOS(eCurrentFactoryID), sBuyerName, sBuyerLocation) 
			ADD_PRODUCT_BUYER(iFactoryID, 1, sBuyerLocation, sBuyerName, iFinalSaleValue, TRUE)
			
			//Buyer two is always further away
			GET_PRODUCT_BUYER_DETAILS(NOT IS_CLOSEST_BUYER_IN_LOS_SANTOS(eCurrentFactoryID), sBuyerName, sBuyerLocation) 
			fStockVal 		= (TO_FLOAT(iInitialStockVal) * g_sMPTunables.fBiker_Sell_Product_Far_Reward_Modifier)			
			iFinalSaleValue = ROUND(fStockVal)
			ADD_PRODUCT_BUYER(iFactoryID, 2, sBuyerLocation, sBuyerName, iFinalSaleValue, TRUE)
		ENDIF
		
		INT iIter
		
		//Pass scaleform details about the upgrades
		REPEAT ENUM_TO_INT(UPGRADE_ID_MAX) iIter
			FACTORY_UPGRADE_ID 	eUpgrade 			= INT_TO_ENUM(FACTORY_UPGRADE_ID, iIter)
			BOOL 				bIsUpgradeAvailable = TRUE
			TEXT_LABEL_15		tlUpgradeTXD		= GET_FACTORY_UPGRADE_TXD(eFactoryType, eUpgrade)
			
			IF DOES_PLAYER_OWN_FACTORY_UPGRADE(PLAYER_ID(), eCurrentFactoryID, eUpgrade)
			OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage <= 0
				bIsUpgradeAvailable = FALSE
			ENDIF
			
			//Get the base price
			INT iUpgradeCost 		= GET_FACTORY_UPGRADE_COST(eUpgrade, eCurrentFactoryID)
			INT iUpgradeSalePrice	= -1
			
			//Get the sale price if the item is on sale
			IF IS_FACTORY_UPGRADE_ON_SALE(eCurrentFactoryID, eUpgrade)
			AND bIsUpgradeAvailable
				CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> Upgrade on sale: ", eUpgrade)
				iUpgradeSalePrice 	= iUpgradeCost
				iUpgradeCost		= GET_FACTORY_UPGRADE_BASE_COST(eUpgrade, eCurrentFactoryID)
			ENDIF
			
			CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> SEND_INITIAL_DATA_TO_SCALEFORM adding upgrade: ", eUpgrade, " cost: ", GET_FACTORY_UPGRADE_COST(eUpgrade, eCurrentFactoryID))
			ADD_FACTORY_UPGRADE(iFactoryID, (iIter + 1), GET_FACTORY_UPGRADE_NAME(eUpgrade), iUpgradeCost, iUpgradeSalePrice, bIsUpgradeAvailable, tlUpgradeTXD)
		ENDREPEAT
		
		CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> **** END SEND_INITIAL_DATA_TO_SCALEFORM ****")
	ELSE
		CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> **** BEGIN SEND_INITIAL_DATA_TO_SCALEFORM - clubhouse ****")
		
		SEND_PLAYER_DATA(FALSE)
		
		INT i
		FOR i = 1 TO (ENUM_TO_INT(FACTORY_ID_MAX) -1)
			ADD_FACTORY_TO_SCALEFORM_MOVIE(i)
		ENDFOR
		CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> **** END SEND_INITIAL_DATA_TO_SCALEFORM ****")
	ENDIF
ENDPROC

PROC PASS_INPUTS_TO_SCALEFORM()

	bCursorInputProcessed = FALSE
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LB)))
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RB)))
	ENDIF
	
	IF IS_USING_CURSOR(FRONTEND_CONTROL)		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(ciINPUT_KEYBOARD_UP)))
		ENDIF
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(ciINPUT_KEYBOARD_DOWN)))
		ENDIF
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(ciINPUT_KEYBOARD_DOWN)))
		ENDIF
		
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_RELEASE_EVENT", TO_FLOAT(ENUM_TO_INT(ciINPUT_KEYBOARD_UP)))
		ENDIF
	ELSE
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
			bCursorInputProcessed = TRUE
		ENDIF
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
			bCursorInputProcessed = TRUE
		ENDIF
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LEFT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RIGHT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
		IF iCurrentPageID = ciBusinessesPage
		OR iCurrentPageID = ciUnknownPage
		OR iCurrentPageID = ciMangementPage
		OR iCurrentPageID = ciSplashPage
			IF NOT bOverlayScreenActive
				CLEANUP_BIKER_APP()
			ELSE
				CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> [APP_CLOSE] 2 Back pressed not closing the app. page: ", iCurrentPageID, " overlay: ", bOverlayScreenActive)
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_INTERNET, "<APP_BIKER> [APP_CLOSE] Back pressed not closing the app. page: ", iCurrentPageID, " overlay: ", bOverlayScreenActive)
		ENDIF
		
		IF bOverlayScreenActive
			bOverlayScreenActive = FALSE
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "Click_Back", "DLC_Biker_Computer_Sounds")
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_X)))
	ENDIF
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_Y)))
	ENDIF	
	
	IF IS_USING_CURSOR(FRONTEND_CONTROL)
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_MOUSE_INPUT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y))
			
			bCursorInputProcessed = TRUE
		END_SCALEFORM_MOVIE_METHOD()
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_ACCEPT)))
		ENDIf
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(mov, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_CANCEL)))
			IF iCurrentPageID = ciBusinessesPage
			OR iCurrentPageID = ciUnknownPage
			OR iCurrentPageID = ciMangementPage
				IF NOT bOverlayScreenActive
					CLEANUP_BIKER_APP()
				ENDIF
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "Click_Back", "DLC_Biker_Computer_Sounds")
		ENDIf
		
	ELSE
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		INT iLeftXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		INT iLeftYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		IF (iLeftXNew != 127) OR (iLeftX != 127) OR (iLeftYNew != 127) OR (iLeftY != 127)
			BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_ANALOG_STICK_INPUT")
				iLeftX = iLeftXNew
				iLeftY = iLeftYNew
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftX)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftY)
			END_SCALEFORM_MOVIE_METHOD()
			
			bCursorInputProcessed = TRUE
		ENDIF
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
	INT iRightXNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
	INT iRightYNew = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
	IF (iRightXNew != 127) OR (iRightX != 127) OR (iRightYNew != 127) OR (iRightY != 127)
		BOOL bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN))
		
		IF NOT bUsingScrollWheel
			bUsingScrollWheel = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP))
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_ANALOG_STICK_INPUT")
			iRightX = iRightXNew
			iRightY = iRightYNew
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightY)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUsingScrollWheel)
		END_SCALEFORM_MOVIE_METHOD()
		
		IF NOT bUsingScrollWheel
			bCursorInputProcessed = TRUE
		ENDIF
	ENDIF
	
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y))
	AND NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		CPRINTLN(DEBUG_INTERNET,"<APP_BIKER> CURRENT PAGE ID is:", iCurrentPageID, " EXITING WEBSITE for Triangle press")
		CLEANUP_BIKER_APP()
	ENDIF
	
	#IF IS_DEBUG_BUILD		
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		CLEANUP_BIKER_APP()
	ENDIF	
	#ENDIF
	
ENDPROC

PROC SHOW_OVERLAY(INT ibuttonAction, STRING message, STRING confirmLabel, STRING cancelLabel, INT iadditionalINT = -1, BOOL bPlayFailSound = FALSE)
	iLastButtonAction = ibuttonAction
	BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SHOW_OVERLAY")
	
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(message)
			IF iadditionalINT != -1
				ADD_TEXT_COMPONENT_INTEGER(iadditionalINT)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(confirmLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(cancellabel)
	END_SCALEFORM_MOVIE_METHOD()
	
	IF bPlayFailSound
		PLAY_SOUND_FRONTEND(-1, "Click_Fail", "DLC_Biker_Computer_Sounds")
	ENDIF
	
	bOverlayScreenActive = TRUE
ENDPROC

PROC PURCHASE_BUSINESS(INT id)
	FACTORY_ID eFactoryID = INT_TO_ENUM(FACTORY_ID, id)
	
	CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> BUY_BIKER_FACTORY Buying factory: ", id, " Begin transaction.")
	
	INT iResultSlot,i
	
	REPEAT ciMAX_OWNED_FACTORIES i 
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[i].eFactoryID = FACTORY_ID_INVALID
			iResultSlot = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF BUY_MP_FACTORY_FROM_OPENROAD(eFactoryID, iResultSlot)
		SET_FACTORY_AS_OWNED(eFactoryID)
	
		// Update scaleform with the new businees status.
		ADD_FACTORY_TO_SCALEFORM_MOVIE(id)
		
		// Set a flag so that closing the overlay forces the site to display the homepage
		bGoHome = TRUE
		
		//Clear the help call bits
		GB_CLEAR_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_GET_BUSINESS_0)
		GB_CLEAR_LOCAL_BIKER_CALL_BIT(eGB_LOCAL_BIKER_CALL_BITSET_NOTIFICATION_GET_BUSINESS_1)
		
		START_FACTORY_DEFEND_MISSION_TIMER(GET_SAVE_SLOT_FOR_FACTORY(INT_TO_ENUM(FACTORY_ID, id)))
		SAVE_FACTORY_DEFEND_TIMER_STATS()
		
		SET_PACKED_STAT_BOOL(GET_STAT_ENUM_FOR_FACTORY_SETUP_DELAY_TIMER_SLOT(iResultSlot), FALSE)
		
		CLEAR_FACTORY_STAFF_ARRIVED_TEXT_MESSAGES(iResultSlot)
		
		CLEAR_FACTORY_PRODUCTION_LJT_CALL_TRIGGERS(iResultSlot)
		
		REQUEST_NET_SPEND_COMMON_DATA_UPDATE()
		
		//Set the flag to request a save when we leave the app
		bRequestSave = TRUE
		
		// Notify the player.
		SHOW_OVERLAY(2001, "OR_PUR_SUC", "OR_OVRLY_OK", "")
	ELSE
		SHOW_OVERLAY(0, "BKR_TF_R5", "OR_OVRLY_OK", "")
	ENDIF
ENDPROC

PROC TRADE_BUINESS(FACTORY_ID eNewFactory)
	FACTORY_TYPE eOldFactoryType 	= GET_GOODS_CATEGORY_FROM_FACTORY_ID(eNewFactory)	
	FACTORY_ID eOldFactory			= GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), eOldFactoryType)
	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> TRADE_BUINESS Trading factory: ", eOldFactory," ", GET_FACTORY_NAME_FOR_DEBUG(eOldFactory), " for Factory: ", eNewFactory, " ", GET_FACTORY_NAME_FOR_DEBUG(eNewFactory))
	#ENDIF	
		
	//Perform the trade
	INT iResultSlot
	IF BUY_MP_FACTORY_FROM_OPENROAD(eNewFactory, iResultSlot, eOldFactory)
		TRADE_BIKER_FACTORY(eNewFactory, eOldFactory)	
	
		// Update scaleform with the new businees status.
		ADD_FACTORY_TO_SCALEFORM_MOVIE(ENUM_TO_INT(eOldFactory))
		ADD_FACTORY_TO_SCALEFORM_MOVIE(ENUM_TO_INT(eNewFactory))
		
		// Set a flag so that closing the overlay forces the site to display the homepage
		bGoHome = TRUE
		
		//Set the flag to request a save when we leave the app
		bRequestSave = TRUE
		
		// Notify the player.
		SHOW_OVERLAY(2001, "OR_PUR_SUC", "OR_OVRLY_OK", "")
	ELSE
		SHOW_OVERLAY(0, "BKR_TF_R5", "OR_OVRLY_OK", "")
	ENDIF
ENDPROC

/// PURPOSE:
///    Uses the factory type of the given factory to calculate the trade value 
FUNC INT GET_TRADE_IN_VALUE_OF_FACTORY(INT iFactoryID)
	FACTORY_ID eFactoryID 			= INT_TO_ENUM(FACTORY_ID, iFactoryID)
	FACTORY_TYPE eOldFactoryType 	= GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	FACTORY_ID eOldFactory 			= GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), eOldFactoryType)			
	
	RETURN GET_FACTORY_SALE_VALUE(eOldFactory)
ENDFUNC

PROC PURCHASE_FACTORY_UPGRADE(FACTORY_UPGRADE_ID eUpgrade)

	PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")

	CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> PURCHASE_FACTORY_UPGRADE: ", GET_FACTORY_UPGRADE_NAME(eUpgrade), " for business: ", eCurrentFactoryID)
	
	SET_LOCAL_PLAYER_OWN_FACTORY_UPGRADE(eCurrentFactoryID, eUpgrade, TRUE)
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SEND_INITIAL_DATA_TO_SCALEFORM()
	ELSE
		eJustPurchasedUpgrade = eUpgrade
	ENDIF
	#ENDIF
	#IF NOT FEATURE_CASINO_HEIST
		eJustPurchasedUpgrade = eUpgrade
	#ENDIF
ENDPROC

INT iAssignedTransactionSlot 	= -1
INT iTransactionID				= -1

FUNC BOOL WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL()
	
	IF NOT USE_SERVER_TRANSACTIONS()
		RETURN TRUE
	ENDIF
	
	STRUCT_SHOP_TRANSACTION_EVENT Event
	SCRIPT_TIMER stTransactionTimeOut
	
	//Clear any old data
	iAssignedTransactionSlot	= -1
	iTransactionID				= -1
	
	INT iScriptTransactionIndex
	INT iCount
	
	//First find the transaction relevant to us
	REPEAT SHOPPING_TRANSACTION_MAX_NUMBER iScriptTransactionIndex	
		IF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionService = SERVICE_SPEND_BUSINESS						
		
			PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - Trans slot: ", iScriptTransactionIndex," is currently running the transaction for bunker")
			iAssignedTransactionSlot = iScriptTransactionIndex
			
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF iAssignedTransactionSlot = -1
		ASSERTLN("<APP_BIKER> Failed to determine transaction slot for resupply transaction")
		WAIT(0)
		RENDER_BIKER_APP()
		RETURN FALSE
	ENDIF
	
	WHILE NOT HAS_NET_TIMER_EXPIRED(stTransactionTimeOut, 15000)
		IF iTransactionID = -1
			IF g_cashTransactionData[iAssignedTransactionSlot].eEssentialData.iTransactionId != NET_SHOP_INVALID_ID					
				iTransactionID = g_cashTransactionData[iAssignedTransactionSlot].eEssentialData.iTransactionId
				PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - Awaiting transaction result. transaction ID assigned: ", iTransactionID)
			ENDIF
		ENDIF
		
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
			IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SHOP_TRANSACTION
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
					
					IF Event.m_Id = iTransactionID
					AND Event.m_Id != -1
						IF Event.m_ResultCode = 0
							PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - Transaction success!")
							RETURN TRUE
						ELSE
							PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - Transaction failed!")
							RETURN FALSE
						ENDIF
					ELSE
						PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - Event non matching transaction ID. Event: ", Event.m_Id, " stored ID: ", iTransactionID)
					ENDIF						
				ENDIF
			ENDIF
		ENDREPEAT
		
		WAIT(0)
		RENDER_BIKER_APP()
	ENDWHILE
	
	WAIT(0)
	RENDER_BIKER_APP()
	
	PRINTLN("<APP_BIKER> WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL - timer expired with no transaction event!")
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_1_2022
PROC DISPLAY_FIRST_PRIVATE_GOODS_SELL_HELP_TEXT()
	IF DID_I_JOIN_A_PRIVATE_SESSION()
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_APP_SELL_GOODS_HELP_TEXT_BIKER)
			IF NOT IS_THIS_PRINT_BEING_DISPLAYED("APP_SG_HELP")
				PRINT_HELP("APP_SG_HELP")
			ENDIF
			SET_PACKED_STAT_BOOL (PACKED_MP_BOOL_APP_SELL_GOODS_HELP_TEXT_BIKER, TRUE)
		ENDIF
	ENDIF
ENDPROC
#ENDIF //#IF FEATURE_DLC_1_2022

PROC HANDLE_OVERLAY_RESPONSE(BOOL accepted, INT iBusinessID)

	CALL_SCALEFORM_MOVIE_METHOD(mov, "HIDE_OVERLAY")
	
	bOverlayScreenActive = FALSE
	
	IF accepted
		
		INT iSaveSlot
		INT iLaunchFailReason
		
		SWITCH iLastButtonAction
			
			// Buy Business
			CASE 2001
				IF (bGoHome)
					CALL_SCALEFORM_MOVIE_METHOD(mov, "SHOW_HOMEPAGE")
					bGoHome = FALSE
				ELIF NOT IS_FACTORY_PURCHASE_DISABLED(INT_TO_ENUM(FACTORY_ID, iBusinessID))
					PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
					IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), GET_GOODS_CATEGORY_FROM_FACTORY_ID(INT_TO_ENUM(FACTORY_ID, iBusinessID)))
						TRADE_BUINESS(INT_TO_ENUM(FACTORY_ID, iBusinessID))
						UPDATE_CEASE_PRODUCTION_STAT(iSaveSlot, FALSE)
					ELSE
						PURCHASE_BUSINESS(iBusinessID)
					ENDIF
				ENDIF
			BREAK
			
			//Confirm Business trade
			CASE 1001
				SHOW_OVERLAY(2001, "OR_BUY_SAV", "WHOUSE_CONF", "WHOUSE_CANC", GET_TRADE_IN_VALUE_OF_FACTORY(iBusinessID))
			BREAK
			
			//Confirm sell local
			CASE 1002
				IF NOT CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_CONTRABAND_SELL)
					IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
						iLaunchFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_SELL, FALSE)
					ENDIF
					
					IF iLaunchFailReason = GB_MU_REASON_GOON_IS_ANIMAL					
						SHOW_OVERLAY(0, "GENERAL_MLF_G1", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF iLaunchFailReason = GB_MU_REASON_GOON_GAMBLING
						SHOW_OVERLAY(0, "GENERAL_MLF_G4", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELSE
						SHOW_OVERLAY(0, "OR_MIS_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ENDIF
				ELSE
					#IF FEATURE_DLC_1_2022
					DISPLAY_FIRST_PRIVATE_GOODS_SELL_HELP_TEXT()
					#ENDIF //#IF FEATURE_DLC_1_2022
					SHOW_OVERLAY(3011, "OR_SELL_GD", "WHOUSE_CONF", "WHOUSE_CANC")
				ENDIF
			BREAK
			
			//Confirm sell long distance
			CASE 1003				
				IF NOT CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_CONTRABAND_SELL)
					IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
						iLaunchFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_SELL, FALSE)
					ENDIF
						
					IF iLaunchFailReason = GB_MU_REASON_GOON_IS_ANIMAL					
						SHOW_OVERLAY(0, "GENERAL_MLF_G1", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF iLaunchFailReason = GB_MU_REASON_GOON_GAMBLING
						SHOW_OVERLAY(0, "GENERAL_MLF_G4", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELSE
						SHOW_OVERLAY(0, "OR_MIS_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ENDIF
				ELSE
					#IF FEATURE_DLC_1_2022
					DISPLAY_FIRST_PRIVATE_GOODS_SELL_HELP_TEXT()
					#ENDIF //#IF FEATURE_DLC_1_2022
					SHOW_OVERLAY(3012, "OR_SELL_GD", "WHOUSE_CONF", "WHOUSE_CANC")
				ENDIF
			BREAK
			
			// Setup Business
			CASE 3003
				IF BUY_MP_SUPPLIES_FROM_OPENROAD(eCurrentFactoryID)
					PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
					LAUNCH_SETUP_MISSION(eCurrentFactoryID)
					INCREMENT_BIKER_BUY_MISSION_UNDERTAKEN_COUNT(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID))
				ENDIF
			BREAK
			
			// Cease Production
			CASE 3005
				PLAY_SOUND_FRONTEND(-1, "Business_Shutdown", "DLC_Biker_Computer_Sounds")
			
				iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)							
				CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> Cease production at business: ", eCurrentFactoryID, " in slot: ",  iSaveSlot)
				DO_CEASE_PRODUCTION_TRANSACTION()	
				UPDATE_CEASE_PRODUCTION_STAT(iSaveSlot, TRUE)
				//Refresh the current factory data
				SEND_INITIAL_DATA_TO_SCALEFORM()
				
					IF DOES_PLAYER_OWN_A_BUSINESS_HUB(PLAYER_ID())
						BUSINESS_TYPE eType
						eType = GET_BUSINESS_TYPE_FROM_FACTORY_TYPE(GET_GOODS_CATEGORY_FROM_FACTORY_ID(eCurrentFactoryID))
						IF DOES_BUSINESS_TYPE_HAVE_STAFF_MEMBER_ASSIGNED(eType)
							INT iStaffID
							iStaffID = GET_STAFF_MEMBER_ASSIGNED_TO_BUSINESS(eType)
							CLEAR_HUB_STAFF_MEMBER_ASSIGNMENT(iStaffID)
							
							SET_BIT(g_iHubAppFactoryShutDownHelp, iSaveSlot)
						ENDIF
					ENDIF
			BREAK
			
			// Start Production
			CASE 3006
				PLAY_SOUND_FRONTEND(-1, "Business_Restart", "DLC_Biker_Computer_Sounds")
				
				iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)							
				CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> Begin production at business: ", eCurrentFactoryID, " in slot: ",  iSaveSlot)
				START_BIKER_DELAY_PRODUCTION_TIMER(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID))
				//Refresh the current factory data
				SEND_INITIAL_DATA_TO_SCALEFORM()
				UPDATE_CEASE_PRODUCTION_STAT(iSaveSlot, FALSE)
			BREAK
			
			// Purchase Upgrade #1
			CASE 3008
				IF IS_FACTROY_UPGRADE_AVAILABLE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 0), eCurrentFactoryID)
					IF RUN_TRANSACTION_FOR_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 0))
						PURCHASE_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 0))
					ENDIF
				ENDIF
			BREAK
			
			// Purchase Upgrade #2
			CASE 3009
				IF IS_FACTROY_UPGRADE_AVAILABLE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 1), eCurrentFactoryID)
					IF RUN_TRANSACTION_FOR_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 1))
						PURCHASE_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 1))
						SEND_INITIAL_DATA_TO_SCALEFORM()
						SHOW_OVERLAY(0, "OR_SUS_P_STAF", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ENDIF
				ENDIF
			BREAK
			
			// Purchase Upgrade #3
			CASE 3010
				IF IS_FACTROY_UPGRADE_AVAILABLE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 2),eCurrentFactoryID)
					IF RUN_TRANSACTION_FOR_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 2))
						PURCHASE_FACTORY_UPGRADE(INT_TO_ENUM(FACTORY_UPGRADE_ID, 2))
					ENDIF
				ENDIF
			BREAK
			
			// Sell #1
			CASE 3011
				IF SELL_MP_CONTRABAND_FROM_OPENROAD(eCurrentFactoryID)
					PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
					
					UPDATE_FACTORY_PRODUCTION_ACTIVE_STATE(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID), !g_sMPtunables.bStopFactoryProductionOnMissions)
					LAUNCH_MISSION(FMMC_TYPE_BIKER_CONTRABAND_SELL, eCurrentFactoryID, SELL_LOCALITY_NEAR)
					INCREMENT_BIKER_SELL_MISSION_COUNT(eCurrentFactoryID, SELL_LOCALITY_NEAR)
				ELSE					
					SHOW_OVERLAY(0, GET_TRANSACTION_FAIL_REASON(iTransactionFailReason),"OR_OVRLY_OK", "", DEFAULT, TRUE)
				ENDIF
			BREAK
			
			// Sell #2
			CASE 3012
				IF SELL_MP_CONTRABAND_FROM_OPENROAD(eCurrentFactoryID)
					PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
					
					UPDATE_FACTORY_PRODUCTION_ACTIVE_STATE(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID), !g_sMPtunables.bStopFactoryProductionOnMissions)
					LAUNCH_MISSION(FMMC_TYPE_BIKER_CONTRABAND_SELL, eCurrentFactoryID, SELL_LOCALITY_FAR)
					INCREMENT_BIKER_SELL_MISSION_COUNT(eCurrentFactoryID, SELL_LOCALITY_FAR)
				ELSE
					SHOW_OVERLAY(0, GET_TRANSACTION_FAIL_REASON(iTransactionFailReason),"OR_OVRLY_OK", "", DEFAULT, TRUE)
				ENDIF
			BREAK
			
			//Pay for supplies
			CASE 3014
				PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
				
				TRIGGER_PAYMENT_FOR_FACTORY_SUPPLIES(eCurrentFactoryID)
				
				iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
				
				IF WAS_PAID_RESUPPLY_TRANSACTION_SUCCESSFUL()
					START_PAID_RESUPPLY_TIMER(iSaveSlot)
					SHOW_OVERLAY(0, "OR_RSP_PAID", "OR_OVRLY_OK", "")
					//Refresh the current factory data
					SEND_INITIAL_DATA_TO_SCALEFORM()
					//Show help
					CLEAR_HELP(TRUE)
					IF IS_FACTORY_EMPTY(eCurrentFactoryID)
						BEGIN_TEXT_COMMAND_DISPLAY_HELP("OR_PSUP_OTW0")
					ELSE
						BEGIN_TEXT_COMMAND_DISPLAY_HELP("OR_PSUP_OTW1")
					ENDIF
					END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, 8000)
				ELSE
					SHOW_OVERLAY(0, "UA_PSUP_FAIL", "OR_OVRLY_OK", "", DEFAULT, TRUE)
				ENDIF
			BREAK
			// Resupply mission
			CASE 3015
				IF BUY_MP_SUPPLIES_FROM_OPENROAD(eCurrentFactoryID)
					g_iResupplyingFactory = ENUM_TO_INT(eCurrentFactoryID)
					PLAY_SOUND_FRONTEND(-1, "Click_Special", "DLC_Biker_Computer_Sounds")
					LAUNCH_MISSION(FMMC_TYPE_BIKER_BUY, eCurrentFactoryID)
					UPDATE_FACTORY_PRODUCTION_ACTIVE_STATE(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID), !g_sMPtunables.bStopFactoryProductionOnMissions)
					INCREMENT_BIKER_BUY_MISSION_UNDERTAKEN_COUNT(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID))
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a sell mission is available and shows the relevant overlay screen
PROC RUN_INITIAL_SELL_MISSION_CHECKS(INT iButtonIndex, BOOL bSellingLongDistance)
//	IF ARE_SELL_MISSIONS_DISABLED()
//	
//		START_OVERLAY_SCREEN("MP_WH_SELL", "WH_MFREASON_12", "WHOUSE_CONF", "", FALSE)						//FAIL: Tuneables have disabled sell missions
		
	IF IS_FACTORY_EMPTY(eCurrentFactoryID)															//FAIL: Empty Factory
		SHOW_OVERLAY(0, "OR_BUS_EMPTY", "OR_OVRLY_OK", "", DEFAULT, TRUE)

	ELIF NOT CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_CONTRABAND_SELL)										//FAIL: Not available
		INT iLaunchFailReason
		
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			iLaunchFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_BIKER_CONTRABAND_SELL, FALSE)
		ENDIF
			
		IF iLaunchFailReason = GB_MU_REASON_GOON_IS_ANIMAL					
			SHOW_OVERLAY(0, "GENERAL_MLF_G1", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ELIF iLaunchFailReason = GB_MU_REASON_GOON_GAMBLING
			SHOW_OVERLAY(0, "GENERAL_MLF_G4", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ELSE
			SHOW_OVERLAY(0, "OR_MIS_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ENDIF
	ELIF GET_PRODUCT_TOTAL_PERCENTAGE(eCurrentFactoryID) > 25
		IF bSellingLongDistance
			SHOW_OVERLAY(1003, "OR_SELL_GDB", "OR_OVRLY_OK", "")
		ELSE
			SHOW_OVERLAY(1002, "OR_SELL_GDB", "OR_OVRLY_OK", "")
		ENDIF
	ELSE		
		SHOW_OVERLAY(iButtonIndex, "OR_SELL_GD", "WHOUSE_CONF", "WHOUSE_CANC")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if a setup or resupply mission is available and shows the relevant overlay screen
PROC RUN_INITIAL_BUY_MISSION_CHECKS(INT iButtonIndex)
	
//	IF ARE_RESUPPLY_MISSIONS_DISABLED()															//FAIL: Tuneables have disabled sell missions
//		START_OVERLAY_SCREEN("MP_WH_SELL", "WH_MFREASON_12", "WHOUSE_CONF", "", FALSE)		
	IF g_iMaterialsTotalForFactory[GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)] >= g_sMPTunables.iBIKER_RESUPPLY_LAUNCH_SUPPLIES_THRESHOLD   //FAIL: We hav 100% supplies already
		SHOW_OVERLAY(0, "OR_MIS_NA_B", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF IS_PAID_RESUPPLY_TIMER_RUNNING(GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID))	//FAIL: We have already paid for a resupply
		SHOW_OVERLAY(0, "OR_MIS_NA_C", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF NOT CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_BUY)										//FAIL: Not available
		INT iLaunchFailReason
		
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
		AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
			iLaunchFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_BIKER_BUY, FALSE)
		ENDIF
			
		IF iLaunchFailReason = GB_MU_REASON_GOON_IS_ANIMAL					
			SHOW_OVERLAY(0, "GENERAL_MLF_G1", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ELIF iLaunchFailReason = GB_MU_REASON_GOON_GAMBLING
			SHOW_OVERLAY(0, "GENERAL_MLF_G4", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ELSE
			SHOW_OVERLAY(0, "OR_MIS_NA_B", "OR_OVRLY_OK", "", DEFAULT, TRUE)
		ENDIF
		
		iLastButtonAction = 0
	ELSE
		SHOW_OVERLAY(iButtonIndex, "OR_STEAL_SUP", "WHOUSE_CONF", "WHOUSE_CANC")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if an upgrade is available to purchase and shows the relevant overlay screen
PROC RUN_INITIAL_UPGRADE_PURCHASE_CHECKS(FACTORY_UPGRADE_ID eUpgrade, INT iButtonIndex)

	INT iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
	
	IF NOT IS_FACTORY_SLOT_VALID(iSaveSlot)
		SHOW_OVERLAY(0, "OR_UPGR_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage <= 0
		SHOW_OVERLAY(0, "OR_UPGR_NS", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF IS_FACTORY_UPGRADE_DISABLED(eUpgrade, eCurrentFactoryID)
		SHOW_OVERLAY(0, "OR_UPGR_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF DOES_PLAYER_OWN_FACTORY_UPGRADE(PLAYER_ID(), eCurrentFactoryID, eUpgrade)
		SHOW_OVERLAY(0, "OR_UPGR_OWN", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ELIF NETWORK_CAN_SPEND_MONEY(GET_FACTORY_UPGRADE_COST(eUpgrade, eCurrentFactoryID), FALSE, TRUE, FALSE)
		SHOW_OVERLAY(iButtonIndex, "OR_BUY_UPGR", "WHOUSE_CONF", "WHOUSE_CANC")
	ELSE
		SHOW_OVERLAY(0, "OR_UPG_NO_MONEY", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the factory is available for purchase and determines if we should trade in an existing one
PROC RUN_INIITIAL_FACTORY_PURCHASE_CHECKS(INT iBusinessID, INT iButtonIndex)
	FACTORY_ID eFactoryID = INT_TO_ENUM(FACTORY_ID, iBusinessID)
	
	BOOL bShouldBeFree = SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryID)
	
	INT iPurchasePrice = GET_FACTORY_PRICE(eFactoryID)
	
	IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID))
		FACTORY_ID eOldFactory = GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID))
		
		iPurchasePrice -= GET_FACTORY_SALE_VALUE(eOldFactory)
	ENDIF
	
		IF bShouldBeFree
			PRINTLN("<APP_BIKER> RUN_INIITIAL_FACTORY_PURCHASE_CHECKS - Player is purchasing a free factory with ID: ", eFactoryID)
			iPurchasePrice = 0
		ENDIF
	
	IF iPurchasePrice > 0
	AND NOT NETWORK_CAN_SPEND_MONEY(iPurchasePrice, FALSE, TRUE, FALSE)
		SHOW_OVERLAY(0, "OR_BUS_AFF", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ElIF IS_FACTORY_AVAILABLE_TO_BUY(eFactoryID)
		IF DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_ID(), GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID))						
			SHOW_OVERLAY(1001, "OR_BUY_BUSB", "WHOUSE_CONF", "WHOUSE_CANC")
		ELSE
			SHOW_OVERLAY(iButtonIndex, "OR_BUY_BUS", "WHOUSE_CONF", "WHOUSE_CANC")
		ENDIF
	ELSE
		SHOW_OVERLAY(0, "OR_BUS_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
	ENDIF
ENDPROC
	
/// PURPOSE:
///    Handles the response to buttons being triggered from the scaleform movie
PROC CHECK_BUTTON_PRESS()

	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SELECTION")
		currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_SELECTED_BUSINESS_ID")
		currentBusinessReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SCREEN_ID")			
		CurrentScreenReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		bCheckPageID 	= FALSE
		bInputConsumed 	= FALSE
	ENDIF
	
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))	
	AND bCheckPageID
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_SCREEN_ID")
		CurrentScreenReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		bCheckPageID = FALSE
	ENDIF
	
	IF (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(CurrentScreenReturnIndex))
	AND !bCheckPageID
		iCurrentPageID = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(CurrentScreenReturnIndex)
		CPRINTLN(DEBUG_INTERNET,"<APP_BIKER> CURRENT PAGE ID:", iCurrentPageID)
		bCheckPageID = TRUE
	ENDIF
	
	IF bCursorInputProcessed
	AND NOT bWaitingForSelectionReturn
		BEGIN_SCALEFORM_MOVIE_METHOD(mov, "GET_CURRENT_ROLLOVER")
		currentRolloverReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
		bWaitingForSelectionReturn = TRUE
	ENDIF
	
	IF bWaitingForSelectionReturn
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentRolloverReturnIndex)
			iLastHiglightedButton = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentRolloverReturnIndex)
			bWaitingForSelectionReturn = FALSE
		ENDIF
	ENDIF
	
	IF iLastHiglightedButton = 3014
	AND SHOULD_BUY_SUPPLIES_HELP_BE_DISPLAYED()
		BEGIN_TEXT_COMMAND_DISPLAY_HELP("OR_RSP_TUT")
		END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE, 8000)
		
		INT iNewTotal = (GET_PACKED_STAT_INT(PACKED_MP_INT_OPEN_ROAD_BUY_TUTORIAL) + 1)
		
		SET_PACKED_STAT_INT(PACKED_MP_INT_OPEN_ROAD_BUY_TUTORIAL, iNewTotal)
		
		bDisplayedBuyHelpText = TRUE
	ENDIF
	
	IF (NOT bInputConsumed)
		IF (IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentSelectionReturnIndex) AND IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(currentBusinessReturnIndex))
			INT iButtonIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentSelectionReturnIndex)
			INT iBusinessID = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(currentBusinessReturnIndex)
			
			INT iSaveSlot = -1
			
			IF eCurrentFactoryID != FACTORY_ID_INVALID
				iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
			ENDIF
			
			CPRINTLN(DEBUG_INTERNET,"<APP_BIKER> CURRENT PAGE iButtonIndex:", iButtonIndex)
			
			SWITCH iButtonIndex
			
				// Generic Overlay Accept
				CASE 101
					HANDLE_OVERLAY_RESPONSE(TRUE, iBusinessID)
				BREAK
				
				// Generic Overlay Cancel
				CASE 102
					HANDLE_OVERLAY_RESPONSE(FALSE, iBusinessID)
					PLAY_SOUND_FRONTEND(-1, "Click_Cancel", "DLC_Biker_Computer_Sounds")
				BREAK
				
				// Buy Business
				CASE 2001
					RUN_INIITIAL_FACTORY_PURCHASE_CHECKS(iBusinessID, iButtonIndex)
				BREAK
				
				CASE 3001					
					IF g_iMaterialsTotalForFactory[iSaveSlot] >= g_sMPTunables.iBIKER_RESUPPLY_LAUNCH_SUPPLIES_THRESHOLD
						SHOW_OVERLAY(0, "OR_MIS_NA_B", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF IS_PAID_RESUPPLY_TIMER_RUNNING(iSaveSlot)
						SHOW_OVERLAY(0, "OR_MIS_NA_C", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ENDIF
				BREAK
				
				//Upgrade button in the sidebar
				CASE 3002
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage <= 0
						SHOW_OVERLAY(0, "OR_UPGR_NS", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF GET_FACTORY_STATUS(eCurrentFactoryID) = PlayerSuspended
						SHOW_OVERLAY(0, "OR_UPGR_RES", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ENDIF
				BREAK
				
				// Setup Business
				CASE 3003
					IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
						SHOW_OVERLAY(0, "BKR_TF_R3", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF NOT CAN_PLAYER_LAUNCH_MISSION(FMMC_TYPE_BIKER_BUY)										//FAIL: Not available
						INT iLaunchFailReason
						
						IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
						AND NOT GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
							iLaunchFailReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_BIKER_BUY, FALSE)
						ENDIF
							
						IF iLaunchFailReason = GB_MU_REASON_GOON_IS_ANIMAL					
							SHOW_OVERLAY(0, "GENERAL_MLF_G1", "OR_OVRLY_OK", "", DEFAULT, TRUE)
						ELIF iLaunchFailReason = GB_MU_REASON_GOON_GAMBLING
							SHOW_OVERLAY(0, "GENERAL_MLF_G4", "OR_OVRLY_OK", "", DEFAULT, TRUE)
						ELSE
							SHOW_OVERLAY(0, "BKR_TF_R6", "OR_OVRLY_OK", "", DEFAULT, TRUE)
						ENDIF
					ELSE
						SHOW_OVERLAY(iButtonIndex, "OR_BUS_SETUP", "WHOUSE_CONF", "WHOUSE_CANC")
					ENDIF
				BREAK
				
				// Cease Production
				CASE 3005
					IF IS_PAID_RESUPPLY_TIMER_RUNNING(iSaveSlot)
						SHOW_OVERLAY(0, "UA_CANT_SDOWN", "OR_OVRLY_OK", "", -1, TRUE)
					ELSE
						SHOW_OVERLAY(iButtonIndex, "OR_STOP_PROD", "WHOUSE_CONF", "WHOUSE_CANC")
					ENDIF
				BREAK
				
				// Start Production
				CASE 3006
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage <= 0
						SHOW_OVERLAY(0, "OR_BS_NOT_SETUP", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF g_iMaterialsTotalForFactory[iSaveSlot] <= 0
						SHOW_OVERLAY(iButtonIndex, "OR_START_PRD_NS", "WHOUSE_CONF", "WHOUSE_CANC")
					ELSE
						SHOW_OVERLAY(iButtonIndex, "OR_START_PROD", "WHOUSE_CONF", "WHOUSE_CANC")
					ENDIF
				BREAK
				
				// Upgrade buttons 1-3
				CASE 3008
				CASE 3009
				CASE 3010
					FACTORY_UPGRADE_ID eUpgrade
					eUpgrade = INT_TO_ENUM(FACTORY_UPGRADE_ID, (iButtonIndex - 3008))
					RUN_INITIAL_UPGRADE_PURCHASE_CHECKS(eUpgrade, iButtonIndex)					
				BREAK
				
				// Sell buttons 1-3
				CASE 3011
				CASE 3012
				CASE 3013
					//Launches the relevant overlay screen
					RUN_INITIAL_SELL_MISSION_CHECKS(iButtonIndex, iButtonIndex = 3012)
				BREAK
				
				// Purchase Business supplies
				CASE 3014
					INT iMaterialsTotal, iCost
					iMaterialsTotal = g_iMaterialsTotalForFactory[iSaveSlot]
					iCost 			= GET_FACTORY_PAID_RESUPPLY_COST(FALSE, iMaterialsTotal)
					
					IF g_sMPTunables.bBIKER_DISABLE_PURCHASE_SUPPLIES
						SHOW_OVERLAY(0, "OR_BS_PAY_NA", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF g_iMaterialsTotalForFactory[iSaveSlot] >= g_sMPTunables.iBIKER_RESUPPLY_LAUNCH_SUPPLIES_THRESHOLD
						SHOW_OVERLAY(0, "OR_MIS_NA_B", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF IS_PAID_RESUPPLY_TIMER_RUNNING(iSaveSlot)
						SHOW_OVERLAY(0, "OR_MIS_NA_C", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELIF NOT NETWORK_CAN_SPEND_MONEY(iCost, FALSE, TRUE, FALSE)
						SHOW_OVERLAY(0, "OR_BS_PAY_NM", "OR_OVRLY_OK", "", DEFAULT, TRUE)
					ELSE
						SHOW_OVERLAY(iButtonIndex, "OR_BUY_SUP", "WHOUSE_CONF", "WHOUSE_CANC", iCost)
					ENDIF
				BREAK
				
				// Steal Business supplies
				CASE 3015
					RUN_INITIAL_BUY_MISSION_CHECKS(iButtonIndex)
				BREAK
				
			ENDSWITCH
			bInputConsumed = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL BIKER_START_TUTORIAL_DIALOGUE()
	RETURN CREATE_CONVERSATION(tutPedStruct, "BPLESAU", "BPLES_TUT1", CONV_PRIORITY_HIGH)
ENDFUNC

PROC BIKER_SETUP_TUTORIAL_DIALOGUE()
	SET_USE_DLC_DIALOGUE(TRUE)
	
	ADD_PED_FOR_DIALOGUE(tutPedStruct, 2, NULL, "LJT")
	
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> BIKER_SETUP_TUTORIAL_DIALOGUE Setup Lester voice.")
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Adds debug information on screen to show the status of the GET_CURRENT_ROLLOVER sscaleform method
PROC DISPLAY_CURRENT_SELECTION_DEBUG()
	TEXT_LABEL_63 tl
	
	tl = "Highlighted button: "
	tl += iLastHiglightedButton
	
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(0, 191, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.007, 0.007, "STRING", tl)
	
	TEXT_LABEL_63 tl2	
	tl2 = "Cursor input: "
	tl2 += BOOL_TO_INT(bCursorInputProcessed)
	
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(0, 191, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.007, 0.025, "STRING", tl2)
	
	TEXT_LABEL_63 tl3	
	tl3 = "Waiting for return: "
	tl3 += BOOL_TO_INT(bWaitingForSelectionReturn)
	
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(0, 191, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.007, 0.042, "STRING", tl3)
ENDPROC
#ENDIF

FUNC BOOL IS_LOACL_PLAYER_LEAVING_PROPERTY_FOR_MISSION_START()
	IF g_bLaunchedMissionFrmLaptop 
	OR HAS_NET_TIMER_STARTED(stExitDelay)
	OR HAS_NET_TIMER_STARTED(stMissionStartTImer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

 //FEATURE_BIKER

//***********SCRIPT functions***********
SCRIPT
	CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> started \"", GET_THIS_SCRIPT_NAME(), "\"")
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("appBikerBusiness")) > 1
	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<APP_BIKER> appBiker script already running! cleaning up.")
		CLEANUP_BIKER_APP(TRUE)
	ENDIF
	
	mov = REQUEST_SCALEFORM_MOVIE("BIKER_BUSINESSES")
	
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
		WAIT(0)
	ENDWHILE
		
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	DISABLE_CELLPHONE(TRUE)
	
	Pause_Objective_Text()	
	THEFEED_PAUSE()
	
	SET_BROWSER_OPEN(TRUE)
	
	IF IS_PLAYER_IN_FACTORY(PLAYER_ID())
		eCurrentFactoryID = GET_FACTORY_PLAYER_IS_IN(PLAYER_ID())
		CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> Running from inside Business: ", eCurrentFactoryID)
	#IF FEATURE_CASINO_HEIST
	ELIF WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
		eCurrentFactoryID = INT_TO_ENUM(FACTORY_ID, g_sBusAppManagement.iPropertyID)
		CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> Running from inside Arcade basement. Selected business: ", eCurrentFactoryID)
	#ENDIF
	ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		CDEBUG1LN(DEBUG_INTERNET,"<APP_BIKER> Running from biker clubhouse")
	ENDIF
	
	SEND_INITIAL_DATA_TO_SCALEFORM()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ELSE
		CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> Script startup. Player ped does not exist or is dead!")
	ENDIF
	
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL)
		BIKER_SETUP_TUTORIAL_DIALOGUE()
	ENDIF
	
	IF IS_PC_VERSION()
		SET_MULTIHEAD_SAFE(TRUE, TRUE, TRUE, TRUE)
	ENDIF
	
	IF eCurrentFactoryID != FACTORY_ID_INVALID
		INT iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
		
		IF iSaveSlot != -1
			IF GET_PRODUCT_TOTAL_FOR_FACTORY(PLAYER_ID(), eCurrentFactoryID) = GET_FACTORY_PRODUCT_CAPACITY(eCurrentFactoryID)
				IF NOT IS_BIT_SET(g_iFactoryStoppedProduction, iSaveSlot)
					INCREMENT_TIMES_FACTORY_PRODUCTION_HALTED(iSaveSlot, TRUE, FALSE)
					SET_BIT(g_iFactoryStoppedProduction, iSaveSlot)
				ENDIF
			ELSE
				CLEAR_BIT(g_iFactoryStoppedProduction, iSaveSlot)
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DARKNETACCESSED, TRUE)
	#ENDIF
	
	/// main update and render loop
	WHILE IS_BROWSER_OPEN()
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_EXITING_PROPERTY)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vPlayerCoords) > 5.0
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> Shutting down! Player too far from the computer")	
			CLEANUP_BIKER_APP()
		ELIF IS_PAUSE_MENU_ACTIVE()
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> Shutting down! Player opend the pause menu")	
			CLEANUP_BIKER_APP()
		ELIF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> Shutting down! No network game is currently active")	
			CLEANUP_BIKER_APP()
		ELIF IS_PED_INJURED(PLAYER_PED_ID())
			CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> Shutting down! Player is injured or dead")	
			CLEANUP_BIKER_APP()
		ENDIF
		
		BOOL bCheckPlayerOwnedFactory = TRUE
		
		#IF FEATURE_GEN9_EXCLUSIVE
		bCheckPlayerOwnedFactory = FALSE
		#ENDIF
		
		IF (DOES_PLAYER_OWN_A_FACTORY(PLAYER_ID()) AND bCheckPlayerOwnedFactory)
		OR NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL)
		AND IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_ID())
		AND BIKER_START_TUTORIAL_DIALOGUE()
		AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL)
			#IF IS_DEBUG_BUILD
			IF NOT DOES_PLAYER_OWN_A_FACTORY(PLAYER_ID())
			OR NOT bCheckPlayerOwnedFactory
				CPRINTLN(DEBUG_INTERNET, "<APP_BIKER> Created Lester tutorial conversation.")
			ENDIF
			#ENDIF
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_OPEN_ROAD_TUTORIAL, TRUE)
		ENDIF
		
		RENDER_BIKER_APP()
		PASS_INPUTS_TO_SCALEFORM()
		
		IF eCurrentFactoryID != FACTORY_ID_INVALID
			INT iSaveSlot = GET_SAVE_SLOT_FOR_FACTORY(eCurrentFactoryID)
			
			IF iSaveSlot > -1
			AND iSaveSlot < ciMAX_OWNED_FACTORIES
			AND iMaterialsTotalLastRefresh != g_iMaterialsTotalForFactory[iSaveSlot]
				SEND_INITIAL_DATA_TO_SCALEFORM()
			ENDIF
		ENDIF
		
		IF eJustPurchasedUpgrade = UPGRADE_ID_INVALID
		AND NOT IS_LOACL_PLAYER_LEAVING_PROPERTY_FOR_MISSION_START()
			CHECK_BUTTON_PRESS()
		ELSE
			IF iLaunchedMissionID != FMMC_TYPE_BIKER_CONTRABAND_SELL
				//for: url:bugstar:3015579
				IF NOT HAS_NET_TIMER_STARTED(stExitDelay)
					START_NET_TIMER(stExitDelay)
					CDEBUG1LN(DEBUG_PROPERTY, "<APP_BIKER> Started exit delay timer for upgrade purchae, setup mission or resupply mission")
				ELIF HAS_NET_TIMER_EXPIRED(stExitDelay, 200)
					IF NOT g_bLaunchedMissionFrmLaptop
						SET_PLAYER_HAS_JUST_PURCHASED_FACTORY_UPGRADE(PLAYER_ID(), eCurrentFactoryID, eJustPurchasedUpgrade, TRUE)
						g_bLaunchedMissionFrmLaptop = TRUE
						CDEBUG1LN(DEBUG_PROPERTY, "<APP_BIKER> exit delay timer expired for upgrade purchae, setup mission or resupply mission")
						CLEANUP_BIKER_APP()
					ELIF NOT g_SimpleInteriorData.bTriggerExit
						CDEBUG1LN(DEBUG_PROPERTY, "<APP_BIKER> Triggering simple interior exit")
						TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(stMissionStartTImer)
					START_NET_TIMER(stMissionStartTImer)
					DO_SCREEN_FADE_OUT(800)
					CDEBUG1LN(DEBUG_PROPERTY, "<APP_BIKER> Start timer and do screen fade for sell mission launch")
				ELIF NOT g_SimpleInteriorData.bTriggerExit
					IF IS_BIT_SET(g_iSellBitSet, ciGB_BIKER_SELL_SERV_INIT_DONE)
						TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
						CDEBUG1LN(DEBUG_PROPERTY, "<APP_BIKER> Sell mission ready triggering simple interior exit")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(stMissionStartTImer, 25000)
							CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> SELL_MISSION LAUNCH: Mission took > 15 seconds to initalise! Triggering simple interior exit")
							TRIGGER_EXIT_FROM_SIMPLE_INTERIOR_NOW()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	// Script should never reach here. Always terminate with cleanup function.
	CASSERTLN(DEBUG_INTERNET, "<APP_BIKER> \"", GET_THIS_SCRIPT_NAME(), "\" should never reach here. Always terminate with cleanup function.")
ENDSCRIPT

