USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "cellphone_public.sch"
USING "context_control_public.sch"
//USING "flow_help_public.sch"
USING "net_stat_tracking.sch"

USING "freemode_events_header.sch"

/*    
  
. Have an overall controller that is constantly running in the background
. Scripts register with the controller when their object is available to be triggered
. Registration includes a priority and a help message string
. When L1 is pressed the controller lets an object trigger
. The object informs controller when it is no longer available to be triggered
. Include some debug output so we can get a handle on issues that crop up
. Be wary of scripts that register but forget to unregister
. Provide helper functions like, "is player heading within this angle of this vector"




*/

ENUM CONTEXT_CONTROLLER_STATE
	WAITING_FOR_BUTTON_PRESS,
	//WAITING_FOR_HELD_COUNTER,
	BUTTON_PRESSED_WAITING_FOR_HANDLING,
	BUTTON_HANDLING_IN_PROGRESS
ENDENUM





BOOL bPrintanHalp = FALSE
BOOL bPrintanSubMode = FALSE
BOOL bPrintPlayerName
TEXT_LABEL tPrintanLabel
TEXT_LABEL_63 tPrintanSub
INT iPrintanStyle

BOOL bMPAtmStartGamertagFromHandle = FALSE

INT iGetNameState = 0
INT iAtmRequestId = -1

PROC GHETTO_HELP_PRINTAN()

	IF NOT bPrintanHalp
		EXIT
	ENDIF
	
	BOOL bPlaySound = TRUE
	
		IF (iPrintanStyle = ENUM_TO_INT(CS_EVENTS))
			bPlaySound = FALSE
		ENDIF
	
	IF bPrintanSubMode
		IF bPrintPlayerName
			ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE) // 2823310
		ENDIF
		BEGIN_TEXT_COMMAND_DISPLAY_HELP(tPrintanLabel)
		IF bPrintPlayerName
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tPrintanSub)
		ELSE
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tPrintanSub)
		ENDIF
		END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, bPlaySound,50)
		
		//PRINTLN("GHETTO CONTEXT HELP PRINTING SUBSTRING")
	ELSE
		BEGIN_TEXT_COMMAND_DISPLAY_HELP(tPrintanLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
		END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, bPlaySound,50)
		
		//PRINTLN("GHETTO CONTEXT HELP PRINTING")
	ENDIF
	
	// Set the style
	SWITCH iPrintanStyle
		CASE 0 // CS_DEFAULT
			
		BREAK
		CASE 1 // CS_EVENTS
			SET_FREEMODE_EVENT_HELP_BACKGROUND(FALSE)
		BREAK
		CASE 2 // CS_GANG_BOSS
			GB_SET_GANG_BOSS_HELP_BACKGROUND(FALSE)
		BREAK
	ENDSWITCH

ENDPROC

PROC PRINT_GHETTO_HELP(TEXT_LABEL lbl, INT contextStyle)
	tPrintanLabel = lbl
	bPrintanHalp = TRUE
	bPrintanSubMode = FALSE
	iPrintanStyle = contextStyle
	PRINTLN("GHETTO CONTEXT HELP SHOW")
ENDPROC

PROC PRINT_GHETTO_HELP_WITH_SUBSTRINGS(TEXT_LABEL lbl, TEXT_LABEL_63 adit, INT contextStyle, BOOL isPlayerName)
	tPrintanLabel = lbl
	tPrintanSub = adit
	bPrintanHalp = TRUE
	bPrintanSubMode = TRUE
	iPrintanStyle = contextStyle
	bPrintPlayerName = isPlayerName
	CPRINTLN(DEBUG_AMBIENT, "PRINT_GHETTO_HELP_WITH_SUBSTRINGS tPrintanSub = ", tPrintanSub, ", bPrintPlayerName = ", bPrintPlayerName)
ENDPROC

PROC CLEAR_GHETTO_HELP()
	If g_iCurrentlyDisplayingContextINDEX != -1 OR bPrintanHalp
	bPrintanHalp = FALSE
	g_iCurrentlyDisplayingContextINDEX = -1
	PRINTLN("GHETTO CONTEXT HELP CLEARED")
	ENDIF
ENDPROC
 

// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()

	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC CHECK_CONTEXT_ENTRIES_FOR_DEAD_SCRIPTS(INT &topPrioIndex)
	INT i = 0

	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed AND g_IntentionList[i].bActive
		
			IF g_IntentionList[i].RegisteredBy != NULL
			
					IF NOT IS_THREAD_ACTIVE(g_IntentionList[i].RegisteredBy) 
						PRINTLN("CHECK_CONTEXT_ENTRIES_FOR_DEAD_SCRIPTS removing trigger due to script end")
						IF topPrioIndex > -1
						
							CLEAR_GHETTO_HELP()
							
							//IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(topPrioIndex,TRUE)
							//	//PRINTLN("Context controller clearing help")
							//	//CLEAR_HELP()
							//	//WAIT(50)
							//ENDIF
							//this is now supported functionality because I'm tired of getting bugs about this
							//SCRIPT_ASSERT("CHECK_CONTEXT_ENTRIES_FOR_DEAD_SCRIPTS: active context intention belonging to dead script had priority! This is ignorable")
							PRINTLN("CHECK_CONTEXT_ENTRIES_FOR_DEAD_SCRIPTS removed the top priority index ")
						ENDIF
						
						
						
						g_IntentionList[i].bUsed = FALSE 
						g_IntentionList[i].RegisteredBy = NULL
						g_IntentionList[i].bUsed = FALSE
						g_IntentionList[i].bActive = FALSE
						g_IntentionList[i].bAccepted = FALSE
						g_IntentionList[i].iPriority = 0
						g_IntentionList[i].bToBeDeleted = FALSE
						g_IntentionList[i].bNoString = FALSE
						g_IntentionList[i].iStyle = 0
						
									
									
						topPrioIndex = -1

					ENDIF
				
			ENDIF
		ENDIF
		
	ENDREPEAT
ENDPROC





FUNC INT GET_HIGHEST_USED_INDEX()

	INT i = 0
	INT iTopPrio = -1
	INT iTopPrioIndex = -1
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed
			//IF (NOT g_IntentionList[i].bToBeDeleted) AND (NOT g_IntentionList[i].)
				IF g_IntentionList[i].iPriority > iTopPrio
					iTopPrioIndex = i
					iTopPrio = g_IntentionList[i].iPriority
				ENDIF
			//ENDIF
		ENDIF
	ENDREPEAT
	RETURN iTopPrioIndex
ENDFUNC

//INT iCurrentlyDisplayingContext = -1

PROC ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
	CLEAR_GHETTO_HELP()
	g_iCurrentlyDisplayingContextINDEX = -1
	/*
	IF g_iCurrentlyDisplayingContextINDEX > -1
		PRINTLN("Context controller trying to remove currently displayed help with array index ",g_iCurrentlyDisplayingContextINDEX)
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_iCurrentlyDisplayingContextINDEX,TRUE)

			PRINTLN("Context controller cleared help for index ",g_iCurrentlyDisplayingContextINDEX)
	
			CLEAR_GHETTO_HELP()
		ELSE
			PRINTLN("Context controller detected that ID ",g_iCurrentlyDisplayingContextINDEX, " was not displaying!")
		ENDIF	
		g_iCurrentlyDisplayingContextINDEX = -1
	ENDIF
	*/

ENDPROC

FUNC BOOL CAN_SHOW_MY_HELP(INT myHelpIndex)

	//IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)
	//	IF NOT IS_FLOW_HELP_QUEUE_EMPTY()
	//		RETURN FALSE	
	//	ENDIF
	//ENDIF

	if g_bTriggerSceneBlockHelp
	and g_bTriggerSceneActive
		CPRINTLN(DEBUG_FLOW_HELP, "Could not start help text because the player is in 7.5m of mission trigger blip.")
		RETURN FALSE
	endif



	IF myHelpIndex > 0
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(myHelpIndex,TRUE) OR bPrintanHalp
			//PRINTLN("CAN_SHOW_MY_HELP: Context can override it's own messages")
			RETURN TRUE //can override itself
		ENDIF
	ENDIF

	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED() 
	AND (NOT HAS_SCRIPT_HIDDEN_HELP_THIS_FRAME())
		
		INT i
		REPEAT MAX_CONTEXT_INTENTION i
			IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(i, TRUE)
				PRINTLN("CAN_SHOW_MY_HELP: Context can override other context intention messages")
				RETURN TRUE //can override other context help text
			ENDIF
		ENDREPEAT
		
		PRINTLN("Context help cannot display because another help is on screen, if this is spamming and no help is visible it means IS_HELP_MESSAGE_BEING_DISPLAYED is broken, bug 'Default UI Code' about this")
		RETURN FALSE //not allowed to override other help messages
	ENDIF
	
	
	RETURN TRUE

ENDFUNC



PROC REMOVE_CONTEXT_ENTRIES_PENDING_DELETE()
	INT i = 0
	REPEAT MAX_CONTEXT_INTENTION i
	
			
			IF g_IntentionList[i].bToBeDeleted
			
				IF i = g_iCurrentlyDisplayingContextINDEX
					ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()	
				ENDIF

				g_IntentionList[i].RegisteredBy = NULL
				g_IntentionList[i].bUsed = FALSE
				g_IntentionList[i].bActive = FALSE
				g_IntentionList[i].bAccepted = FALSE
				g_IntentionList[i].iPriority = 0
				g_IntentionList[i].bToBeDeleted = FALSE
				g_IntentionList[i].bNoString = FALSE
				g_IntentionList[i].iID = -1
				g_IntentionList[i].iStyle = 0
				
				PRINTLN("REMOVE_CONTEXT_ENTRIES_PENDING_DELETE: Deleted context entry ", i)
			ENDIF
	ENDREPEAT
ENDPROC


FUNC INT COUNT_ACTIVE_CONTEXT_ENTRIES()//only call in handling mode
	INT i = 0
	INT c = 0
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed = TRUE
		IF g_IntentionList[i].bActive = FALSE
			++c
		ENDIF
		ENDIF
	ENDREPEAT
	
	IF c = 0
		PRINTLN("Context controller detected no entries active in handling mode, attempt dropback and help clear")
		
		ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()

	ENDIF
	
	RETURN c
ENDFUNC





FUNC BOOL IS_CONTEXT_INTENTION_ACCEPTED(int h)

	IF h < 0
		RETURN FALSE
	ENDIF

	IF g_IntentionList[h].bAccepted
		RETURN TRUE
	ENDIF
	RETURN FALSE 
ENDFUNC



PROC RESET_CONTEXT_SYSTEM_GLOBALS()
	g_bSuppressContextHelpDuringHandling = FALSE
	g_bSuppressContextActionsThisFrame = FALSE
	b_SuppressContextHelpNextUpdate = FALSE
	g_iContextIntentionIDGenerator = 1
	g_iCurrentlyDisplayingContextINDEX = -1
	INT afdg = 0
	REPEAT MAX_CONTEXT_INTENTION afdg
		g_IntentionList[afdg].bUsed = FALSE
	ENDREPEAT
ENDPROC



PROC ResetATMNameVars()
	g_iMPAtmLogEntryPendingIndex = -1
	bMPAtmStartGamertagFromHandle = FALSE
	iAtmRequestId = -1
	iGetNameState = 0
	PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: ResetATMNameVars called")
ENDPROC

/// PURPOSE:
///    Due to 1627644 this had to happen in another script than main
PROC DO_CHECK_FOR_ATM_NAME_PROCESS()

    IF g_iMPAtmLogEntryPendingIndex != -1
        //Text lable
        //g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] =
        
        GAMER_HANDLE hGamer[1]
        hGamer[0].Data1 = g_iMPAtmLogHandle[0] 
        hGamer[0].Data2 = g_iMPAtmLogHandle[1] 
        hGamer[0].Data3 = g_iMPAtmLogHandle[2] 
        hGamer[0].Data4 = g_iMPAtmLogHandle[3] 
        hGamer[0].Data5 = g_iMPAtmLogHandle[4] 
        hGamer[0].Data6 = g_iMPAtmLogHandle[5] 
        hGamer[0].Data7 = g_iMPAtmLogHandle[6] 
        hGamer[0].Data8 = g_iMPAtmLogHandle[7] 
        hGamer[0].Data9 = g_iMPAtmLogHandle[8] 
        hGamer[0].Data10 = g_iMPAtmLogHandle[9] 
        hGamer[0].Data11 = g_iMPAtmLogHandle[10]   
        hGamer[0].Data12 = g_iMPAtmLogHandle[11]   
        hGamer[0].Data13 = g_iMPAtmLogHandle[12]
		
        IF IS_XBOX360_VERSION()
		OR IS_XBOX_PLATFORM()
            IF NOT bMPAtmStartGamertagFromHandle
				IF NETWORK_GAMERTAG_FROM_HANDLE_START(hGamer[0])
					bMPAtmStartGamertagFromHandle = TRUE
				ENDIF
			ENDIF
            
			IF bMPAtmStartGamertagFromHandle
                IF NETWORK_GAMERTAG_FROM_HANDLE_PENDING()
					PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Waiting for network handle")
				ELSE
					IF NETWORK_GAMERTAG_FROM_HANDLE_SUCCEEDED()
                    	g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] = NETWORK_GET_GAMERTAG_FROM_HANDLE(hGamer[0])
						PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Got XBL gamer handle, string in from handle '",NETWORK_GET_GAMERTAG_FROM_HANDLE(hGamer[0]),"' stored string '",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] ,"' to ", g_iMPAtmLogEntryPendingIndex)
						
	                #IF IS_DEBUG_BUILD
	                ELSE
	                    PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Failed to set log event player name. NETWORK_GAMERTAG_FROM_HANDLE_SUCCEEDED false")
	                #ENDIF
	                ENDIF
					
					ResetATMNameVars()
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
            	PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Failed to set log event player name. NETWORK_GAMERTAG_FROM_HANDLE_START false")
        	#ENDIF
			ENDIF
        ELIF IS_PS3_VERSION()
		OR IS_PLAYSTATION_PLATFORM()
            IF IS_GAMER_HANDLE_VALID(hGamer[0])
			
				TEXT_LABEL_63 tlGamerNames[1]			
			
				SWITCH iGetNameState
					CASE 0
						iAtmRequestId = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(hGamer, 1)						
						IF iAtmRequestId >= 0
							iGetNameState += 1
							PRINTLN("[CORONA] MAINTAIN_LAST_GANG_OPS_HEIST_PLAYERS - lastGangOpsHeistPlayers.iGamertagRequestID = ", lastGangOpsHeistPlayers.iGamertagRequestID, ". Progressing to pending (1).")
						ENDIF						
					BREAK
					
					CASE 1
						INT iNameState
						// Grab our status for pending gamertags / names
						iNameState = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(iAtmRequestId, tlGamerNames, 1)
						
						SWITCH iNameState
							// Fail
							CASE -1
								ResetATMNameVars()								
								PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: PSN - failed")
							BREAK
							
							// Success
							CASE 0
                				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] = tlGamerNames[0]
								PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Got PSN gamer handle, string in from handle '", tlGamerNames[0],"' stored string '",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] ,"' to ", g_iMPAtmLogEntryPendingIndex)							
								ResetATMNameVars()								
							BREAK
							
							// Pending
							CASE 1
								#IF IS_DEBUG_BUILD
								IF GET_GAME_TIMER() % 1000 < 75
									PRINTLN("[CORONA] DO_CHECK_FOR_ATM_NAME_PROCESS - Pending for g_iMPAtmLogEntryPendingIndex ", g_iMPAtmLogEntryPendingIndex, " - NETWORK_GET_DISPLAYNAMES_FROM_HANDLES() = 1")
								ENDIF
								#ENDIF
							BREAK
						ENDSWITCH					
					BREAK
				ENDSWITCH
            ELSE
			
				ResetATMNameVars()
				
				PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: PSN - IS_GAMER_HANDLE_VALID = FALSE")
			ENDIF
        ELSE
            IF IS_GAMER_HANDLE_VALID(hGamer[0]) 
                g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] = NETWORK_MEMBER_ID_FROM_GAMER_HANDLE(hGamer[0])
				PRINTLN("DO_CHECK_FOR_ATM_NAME_PROCESS: Got PC gamer handle, string in from handle '",NETWORK_MEMBER_ID_FROM_GAMER_HANDLE(hGamer[0]),"' stored string '",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedATM.LogSourceNames[g_iMPAtmLogEntryPendingIndex] ,"' to ", g_iMPAtmLogEntryPendingIndex)
            ENDIF
			
			ResetATMNameVars()
        ENDIF
    ENDIF

ENDPROC

PROC MINUS_MONEY_FIX()// to fix players already afflicted by 1643668 after title update
	BOOL bFixed = FALSE
	INT i = 0
	
	REPEAT MAX_BANK_ACCOUNTS i
		IF g_BankAccounts[i].iBalance < 0
			g_BankAccounts[i].iBalance = 0
			bFixed = TRUE
		ENDIF	
	ENDREPEAT

	IF bFixed
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_MICHAEL)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_FRANKLIN)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_TREVOR)
	ENDIF
	
ENDPROC


PROC DO_CHECK_FOR_BROWSER_SWITCH_BACK_FROM_SHOP()
    IF (NOT IS_PLAYER_SWITCH_IN_PROGRESS()) AND (NOT IS_BROWSER_OPEN())
        IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN(FALSE)
            PRINTLN("Freemode restoring shop state triggered")
            IF NOT IS_STRING_NULL_OR_EMPTY(g_sTransitionSessionData.sEndReserve.stMPBrowserFromShopSite)
                PRINTLN("Freemode e starting web browser triggered")
                CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
                START_BROWSER(SBSS_SHOP_RESTORE)
            ENDIF
        ENDIF
    ENDIF
ENDPROC


SCRIPT 

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controller")) > 1
		PRINTLN("Context controller detected duplicate of itself, bailing.")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// This script needs to cleanup only when the game moves from SP to MP
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	CONTEXT_CONTROLLER_STATE currentState = WAITING_FOR_BUTTON_PRESS
	CONTEXT_CONTROLLER_STATE lastState = currentState
	SETTIMERA(0)
	g_ContextResetForced = FALSE


	RESET_CONTEXT_SYSTEM_GLOBALS()


	//start listening for left hold

	g_strContextButton = "CC_SUBSTR"//PAD_DPAD_RIGHT
	
	//if no responses have been awarded access
		//if there are any registered listeners
			//parse for priority and award response to highest
			//while response is allowable 
				//show help text for relevant response	
				
	INT highestUsed = -1
	INT lastHighestUsed = -1
	

	
	BOOL bIDisabledHeadlights = FALSE
	BOOL bIDisabledRoof = FALSE
	
	BOOL bRefireStopper = FALSE
	
	
	INT waitingOnID = -1
	BOOL mpmodeDetection = g_bInMultiplayer
	
	WHILE TRUE
	
		
		//atm name processing trigger
		IF g_bInMultiplayer
    		DO_CHECK_FOR_ATM_NAME_PROCESS()
		ELSE
			MINUS_MONEY_FIX()//1643668
		ENDIF
		
		
		DO_CHECK_FOR_BROWSER_SWITCH_BACK_FROM_SHOP()
	
	
		#IF IS_DEBUG_BUILD
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controller")) > 1
			SCRIPT_ASSERT("Multiple versions of context controller detected running at once! Please pass this bug and log to Default Levels.")
		ENDIF
		#ENDIF
	
		IF g_bInMultiplayer != mpmodeDetection 
			PRINTLN("Context controller: detected mp flag changed purging tracking gobals")
			ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
			RESET_CONTEXT_SYSTEM_GLOBALS()
			highestUsed = -1
			mpmodeDetection = g_bInMultiplayer
			
			WHILE IS_PLAYER_SWITCH_IN_PROGRESS()
				PRINTLN("Context controller: waiting for switch after mp mode change to finish")
				WAIT(2000)
			ENDWHILE
		ENDIF
				
		IF b_SuppressContextHelpNextUpdate 
			IF g_iCurrentlyDisplayingContextINDEX > -1	
			
			
				//check for refresh
				IF (NOT g_IntentionList[g_iCurrentlyDisplayingContextINDEX].bNoString) 
					IF NOT IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_iCurrentlyDisplayingContextINDEX,TRUE)//check for display with raw index
						g_iCurrentlyDisplayingContextINDEX = -1
						PRINTLN("context controller: detected current displayed help as not displayed")
					ENDIF
				ENDIF									
				//check for change
				
			ENDIF
		ENDIF
		
		IF (CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS())
			highestUsed = -1
			ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
		ENDIF
		
		//maintain help display
		IF highestUsed > -1 
		
			IF NOT b_SuppressContextHelpNextUpdate
				//attempt to show the correct help
				IF g_iCurrentlyDisplayingContextINDEX != highestUsed  
				
				IF CAN_SHOW_MY_HELP(g_iCurrentlyDisplayingContextINDEX)
					ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
					
						IF (NOT g_IntentionList[highestUsed].bAccepted)
						AND (g_IntentionList[highestUsed].bUsed)
							//Show the help
							IF NOT CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS()
								IF (NOT g_IntentionList[highestUsed].bToBeDeleted)
									IF (NOT g_IntentionList[highestUsed].bNoString) 
										
										IF NOT g_IntentionList[highestUsed].bAdditionalHelp
											//PRINT_HELP_FOREVER_WITH_STRING(g_IntentionList[highestUsed].helpLabel, g_strContextButton)											
											PRINTLN("Context controller displaying help for index: ", highestUsed, " with PRINT_HELP_WITH_STRING, high used index ",highestUsed,"  previous context ", g_iCurrentlyDisplayingContextINDEX)
											PRINT_GHETTO_HELP(g_IntentionList[highestUsed].helpLabel, g_IntentionList[highestUsed].iStyle)
										ELSE
										/*
											BEGIN_TEXT_COMMAND_DISPLAY_HELP(g_IntentionList[highestUsed].helpLabel)
											ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
											ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_IntentionList[highestUsed].additionalHelpLabel)
											END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, TRUE)
										*/
											PRINTLN("Context controller displaying help for index: ", highestUsed, " with BEGIN_TEXT_COMMAND_DISPLAY_HELP,  high used index ",highestUsed,"  previous context ", g_iCurrentlyDisplayingContextINDEX)
											PRINT_GHETTO_HELP_WITH_SUBSTRINGS(g_IntentionList[highestUsed].helpLabel,g_IntentionList[highestUsed].additionalHelpLabel, g_IntentionList[highestUsed].iStyle, g_IntentionList[highestUsed].subStringIsPlayerName)
										ENDIF
										g_iCurrentlyDisplayingContextINDEX = highestUsed
//									#IF IS_DEBUG_BUILD
//									ELSE
//										PRINTLN("Context controller: No context help because no string setting")
//									#ENDIF
									ENDIF
//								#IF IS_DEBUG_BUILD
//								ELSE
//									PRINTLN("Context controller: No context help because to be deleted")
//								#ENDIF
								ENDIF
//							#IF IS_DEBUG_BUILD
//							ELSE
//								PRINTLN("Context controller: No context help because suppression conditions")
//							#ENDIF
							ENDIF
							
						//#IF IS_DEBUG_BUILD
						//ELSE
							//PRINTLN("Context controller: No context help because not used or already accepted")
						//#ENDIF
						ENDIF
						
//					#IF IS_DEBUG_BUILD
//					ELSE
//						PRINTLN("Context controller: No context help because can show help failed")
//					#ENDIF
					ENDIF
				ENDIF
			ELSE//help is suppressed 
				ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
			ENDIF
		ELSE
			ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
		ENDIF
		
		//maintain button state
		SWITCH currentState
			CASE WAITING_FOR_BUTTON_PRESS
				//PRINTLN("WAITING_FOR_BUTTON_PRESS")
				//PRINTLN("context controller(WAITING_FOR_BUTTON_PRESS)")
				highestUsed = GET_HIGHEST_USED_INDEX()
				IF IS_CONTROL_RELEASED(FRONTEND_CONTROL,INPUT_CONTEXT)
					bRefireStopper = TRUE
				ENDIF						
				IF NOT CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS()
					IF highestUsed > -1 AND bRefireStopper 
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
							g_IntentionList[highestUsed].bActive = TRUE
							g_IntentionList[highestUsed].bPickedUp = FALSE//used to make "just" mode work
							waitingOnID = g_IntentionList[highestUsed].iID
							currentState = BUTTON_PRESSED_WAITING_FOR_HANDLING
							g_bSuppressContextHelpDuringHandling = FALSE
						ENDIF
					ENDIF
				ELSE
					highestUsed = -1 //suppressed, remove 
					
				ENDIF
				//Context controller now suppresses headlights and car roof 
				//actions while a context press is available
				IF (lastHighestUsed != highestUsed)
					//BOOL bIDisabledHeadlights = FALSE
					//BOOL bIDisabledRoof = FALSE
					IF highestUsed != -1
						IF NOT IS_PED_DEAD_OR_DYING(GET_PLAYER_PED(GET_PLAYER_INDEX()))
							IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(GET_PLAYER_INDEX()))
								IF IS_CONTROL_ENABLED(PLAYER_CONTROL, INPUT_VEH_ROOF)
									bIDisabledRoof = TRUE
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
								ENDIF
								IF IS_CONTROL_ENABLED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
									bIDisabledHeadlights = TRUE
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF bIDisabledRoof
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
							bIDisabledRoof = FALSE
						ENDIF
						IF bIDisabledHeadlights
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
							bIDisabledHeadlights = FALSE
						ENDIF
					ENDIF
					lastHighestUsed = highestUsed
				ENDIF
			BREAK

			CASE BUTTON_PRESSED_WAITING_FOR_HANDLING
				//PRINTLN("BUTTON_PRESSED_WAITING_FOR_HANDLING")
				IF highestUsed < 0 OR COUNT_ACTIVE_CONTEXT_ENTRIES() = 0//emergency dropback
					PRINTLN("context controller(BUTTON_PRESSED_WAITING_FOR_HANDLING) emergency dropback!") 
					currentState = WAITING_FOR_BUTTON_PRESS
				ELSE
					IF TIMERA() > 5000
						//Timed out  
						//clear and delete focused since the script that registered it
						//apparently doesn't give a crap
						g_IntentionList[highestUsed].bToBeDeleted = TRUE
						PRINTLN("context_controller: a registered context didn't respond in time, deleting and rechecking.")
						//drop back to state
						currentState = WAITING_FOR_BUTTON_PRESS
					ELSE
						//Has the highestUsed been triggered?
						PRINTLN("context controller(BUTTON_PRESSED_WAITING_FOR_HANDLING) checking for accept")
						IF IS_CONTEXT_INTENTION_ACCEPTED(highestUsed)
							currentState = BUTTON_HANDLING_IN_PROGRESS
							
						ENDIF
					ENDIF
					IF NOT (waitingOnID = g_IntentionList[highestUsed].iID)
							PRINTLN("BUTTON_PRESSED_WAITING_FOR_HANDLING waitingOnID is ", waitingOnID, " highestUsed is ",highestUsed, " this index has an ID of ", g_IntentionList[highestUsed].iID)
							///SCRIPT_ASSERT("context_controller: In state BUTTON_PRESSED_WAITING_FOR_HANDLING id of waiting context changed! Please pass this bug AND log to Default Levels., thankyou.")
							currentState = WAITING_FOR_BUTTON_PRESS
					ENDIF
					IF g_IntentionList[highestUsed].bToBeDeleted OR g_IntentionList[highestUsed].bUsed = FALSE //it was externally deleted
						PRINTLN("context controller(BUTTON_PRESSED_WAITING_FOR_HANDLING) fell back to due to index toDelete flag set or use trigger")	
						currentState = WAITING_FOR_BUTTON_PRESS //fallback
					ENDIF
				ENDIF

			BREAK

			CASE BUTTON_HANDLING_IN_PROGRESS
				IF g_bSuppressContextHelpDuringHandling
					ATTEMPT_REMOVE_CURRENTLY_DISPLAYED_HELP()
				ENDIF
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
					IF highestUsed > -1 
						g_IntentionList[highestUsed].bPickedUp = FALSE//used to make "just" mode work
					ENDIF
				
				ENDIF

				IF(g_ContextResetForced)
					//bail
					g_ContextResetForced = FALSE
					currentState = WAITING_FOR_BUTTON_PRESS
				ELSE
				
					IF highestUsed > -1

						IF NOT (g_IntentionList[highestUsed].bActive 
								OR g_IntentionList[highestUsed].bToBeDeleted)
							//bail
							g_ContextResetForced = FALSE
						
							
							highestUsed = -1 //force a reselect
						ENDIF
					ENDIF
				ENDIF
				IF highestUsed > -1
				IF g_IntentionList[highestUsed].bToBeDeleted 
				OR g_IntentionList[highestUsed].bUsed = FALSE //it was externally deleted
					PRINTLN("context controller(BUTTON_HANDLING_IN_PROGRESS) fell back to due to index toDelete flag set or use trigger")	
			
					highestUsed = -1
				ENDIF
				ENDIF
				
				IF highestUsed = -1
					PRINTLN("context controller(BUTTON_HANDLING_IN_PROGRESS) fell back to due to highest used being -1")	
					currentState = WAITING_FOR_BUTTON_PRESS //fallback
					bRefireStopper = FALSE
				ENDIF
			BREAK
			
			
		ENDSWITCH



		//suppress stuff that should be suppressed if any context is active
		IF highestUsed > -1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
		ENDIF

		
		IF currentState != lastState
			PRINTLN("context_controller: state transition (", lastState, " -> ", currentState, ") high index = ", highestUsed)
		
			lastState = currentState
			//reset anything that shouldn't carry over states
			SETTIMERA(0)
			bRefireStopper = FALSE	
		ENDIF
		
		
		
		//prune intentions that are pending deletion
		REMOVE_CONTEXT_ENTRIES_PENDING_DELETE()
		//check for dead scripts
		CHECK_CONTEXT_ENTRIES_FOR_DEAD_SCRIPTS(highestUsed)

		g_bSuppressContextActionsThisFrame = FALSE
		b_SuppressContextHelpNextUpdate = FALSE
		
		GHETTO_HELP_PRINTAN()
		WAIT(0)//there can only be one wait
	ENDWHILE
				
				
	
				
				


ENDSCRIPT








