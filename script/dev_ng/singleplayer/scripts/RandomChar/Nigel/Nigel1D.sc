
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "script_blip.sch"
USING "script_camera.sch"
USING "randomChar_public.sch"
USING "dialogue_public.sch"
USING "CompletionPercentage_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "RC_Threat_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Area_public.sch"
USING "initial_scenes_nigel.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	: Nigel 1 D
//		AUTHOR			: Ben Hinchliffe/Ian Gander
//		DESCRIPTION		: Player has to chase a celebrity golfer and steal his golf club
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// The Random Character - sRCLauncherDataLocal.pedID[0]
g_structRCScriptArgs sRCLauncherDataLocal

// Mission stages
ENUM MISSION_STAGE
	//MAIN MISSION FLOW
	MS_INTRO_PHONECALL = 0,
	MS_GO_TO_THE_GOLF_CLUB,
	MS_GO_TO_CELEBRITY,
	MS_GET_THE_CLUB,
	MS_CHASE_THE_GOLFER,
	MS_CHASE_IN_CITY,
	MS_COLLECT_GOLF_CLUB,
	MS_END_PHONECALL,
	MS_MISSION_FAILED,
	
	//SUB STAGES / RETURNS
	MS_LOSE_SECURITY,
	MS_LOSE_THE_COPS
	
ENDENUM

// Sub-stages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Fail reasons
ENUM FAIL_REASON
	FAIL_DEFAULT,
	FAIL_LION_ESCAPED,
	FAIL_DID_NOT_LOCATE_LION,
	FAIL_LOST_GOLF_CLUB,
	FAIL_CLUB_DESTROYED
ENDENUM

//Golfer States
ENUM GOLFER_STATE
	GS_WAIT_FOR_PLAYER,
	GS_PLAYER_IN_OUTFIT,
	GS_ANGRY_AT_PLAYER,
	GS_GET_INTO_VEHICLE,
	GS_FLEE_FROM_PLAYER_IN_BUGGY,
	GS_CRASH_INTO_LAKE,
	GS_FLEE_IN_CITY,
	GS_CRASHED,
	GS_FLEEING_ON_FOOT
ENDENUM

//Celebrity Personal Security states
ENUM SECURITY_STATE
	SS_GUARD_AREA,
	SS_GO_TO_PLAYER,
	SS_WARN_PLAYER,
	SS_AIM_AT_PLAYER,
	SS_ATTACK_PLAYER,
	SS_STANDDOWN,
	SS_FLEEING_AREA
ENDENUM

//Golf course security
ENUM COURSE_SECURITY
	CS_GET_IN_POSITION,
	CS_WAIT_AT_LOCATION,
	CS_PLAYER_WARNED,
	CS_CHAT_TO_PLAYER,
	CS_CALL_POLICE,
	CS_TURN_TO_FACE_BUGGY,
	CS_AVOID_BUGGY,
	CS_FLEE_FROM_PLAYER,
	CS_ATTACK_PLAYER
ENDENUM

//Consierge Ped state
ENUM CONCIERGE_STATE
	CS_STAND_AND_WAIT,
	CS_FLEEING_PLAYER
ENDENUM

//Ambient golfer state
ENUM AMBIENT_STATE
	AS_PLAYGOLF,
	AS_ALERTED,
	AS_GO_TO_FLEE_POS,
	AS_FLEE,
	AS_DEAD
ENDENUM

ENUM OUTFIT_STATE
	OS_WAIT_FOR_PLAYER,
	OS_DETECTED,
	OS_DO_CAMERA_UP,
	OS_CHANGE_OUTFIT,
	OS_DO_CAMERA_DOWN,
	OS_OUTFIT_CHANGED,
	OS_DISABLED
ENDENUM

ENUM WANTED_STATE
	WS_NOT_WANTED,
	WS_WAITING_FOR_WANTED,
	WS_WANTED
ENDENUM

ENUM PLAYER_TASE_STATE
	TS_NOT_TASED,
	TS_TASED
ENDENUM

ENUM CELEB_SWING_DIALOGUE
	SWING_TRIGGER,
	SWING_SPEAKING,
	SWING_WAIT
ENDENUM

// MISSION PED
STRUCT MISSION_PED
	PED_INDEX mPed
	VECTOR vStartPos
	VECTOR vFleePos
	FLOAT fStartHeading
	MODEL_NAMES mModel
	BOOL	bImDead
	BOOL	bImAttacked
	BLIP_INDEX	mBlip
	STRING	mStartAnim
ENDSTRUCT

// MISSION VEHICLE
STRUCT MISSION_VEHICLE
	VEHICLE_INDEX mVehicle
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT

// MISSION PROPS
STRUCT MISSION_PROP
	OBJECT_INDEX mObject
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT

STRUCT MISSION_PICKUP
	PICKUP_INDEX index
	OBJECT_INDEX objIndex
	BLIP_INDEX blip
	VECTOR pos //temporary until set blip for pickup works correctly
ENDSTRUCT

// ENUMS
MISSION_STAGE eMissionStage = MS_GO_TO_THE_GOLF_CLUB
GOLFER_STATE eGolferState = GS_WAIT_FOR_PLAYER
//MISSION_STAGE eReturnStage = MS_INTRO_PHONECALL
SUB_STAGE eSubStage = SS_SETUP
 
// STAGE SKIPPING
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 8
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	BOOL bDebug_PrintToTTY = TRUE
	WIDGET_GROUP_ID widgetGroup
#ENDIF

MISSION_STAGE eTargetStage 	// now used in release build too for checkpoint stuff
BOOL bFinishedSkipping  	// now used in release build too for checkpoint stuff

//----------------------
//	CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP			0
CONST_INT CP_LOCATED_STANKY			1
CONST_INT CP_COMPLETE				2

CONST_INT Z_SKIP_AFTER_MOCAP		1
CONST_INT Z_SKIP_LOCATED_STANKY		3 // the number passed in to the z skip function to get to the final stage
CONST_INT Z_SKIP_ENDPHONECALL		7 

// CONVERSATIONS
structPedsForConversation sDialogue  

//BLIPS
BLIP_INDEX	biMissionBlip
BLIP_INDEX	biConciergeBlip

//CAMERAS
CAMERA_INDEX	ciOutfitTransitionIn
CAMERA_INDEX	ciOutfitTransitionOut
CAMERA_INDEX	ciOutfitTransitionHelper

//BOOLS

BOOL		bDropGolfBag				//Used to check if golf bag needs detaching from buggy
BOOL		bSecurityCreated			//Security for the Celebrity created
BOOL		bCourseSecurityLoaded		//Security around edge of golf course
BOOL		bAmbientGolfersCreated		//Ambient golfers around Celeb
BOOL		bAmbientChaseGolfersCreated	//Ambient golfers during the celebrity buggy chase
BOOL		bPlayerApproached			//Used for Celeb security	
BOOL		bPlayerReturned
BOOL		bShouldAttackPlayer			//Used to tell security if they should attack or not
BOOL		bWarned						//Player has been warned about cars and guns
BOOL		bHelpTextDisplayed
BOOL		bCelebrityCreated	
BOOL		bFriendIsFleeing
BOOL		bSecuritySaidCannotDrive
BOOL		bSecurityAttack
BOOL		bWarnedAboutLeaving			//This is used for leaving lion / golfclubs
BOOL		bConversationActive 		//Deals with if conversations are active
BOOL		bConciergeSceneCreated 		
BOOL		bMainObjectiveDisplayed 	//Has the main objective been displayed // Resets every main objective change.
BOOL		bClubCollectedByPlayer
BOOL		bHasGoneOppositeWay			//Used to see if Celeb golfer has gone alternative route at start of chase
BOOL		bPlayerHasBeenClose			//Used to see if the player has been near the golf club pickup yet
//BOOL		bReturnObjectiveDisplayed 	// Has return objective been displayed // Resets every return objective change.
BOOL		bHasChanged 				//Keeping track of Trevor changing outfit

BOOL		bFostenburgFleeCheck = FALSE
BOOL 		bDonePoliceReport = FALSE

BOOL		bCheckStatFour = TRUE
BOOL		bAllowPlayerToStealClubFromBag = TRUE

BOOL		bHasAcquiredGolfOutfit = FALSE

BOOL 		bHasAddedHeadshot = FALSE
INT			iHeadshotTimer

INT 		iNigelCallDelayTimer

// Replay stuff
BOOL		bReplayCelebDeadCheck = FALSE

// Audio mix group vars
BOOL		bIsInCaddy = FALSE
VEHICLE_INDEX	viStoredCaddyIdx

//STRINGS
FAIL_REASON 		eFailReason									//FAIL REASON THAT GETS DISPLAYED

//CONST_INT
CONST_INT			NUM_OF_CONC_PEDS			4				//NUMBER OF CONCERIGE PEDS AT FRONT OF GOLF CLUB
CONST_INT			NUM_OF_SECURITY				3				//NUMBER OF CELEBRITY PERSONAL SECURITY
CONST_INT			NUM_OF_COURSE_SECURITY		3				//NUMBER OF GOLF COURSE SECURTIY
CONST_INT			NUM_OF_AMB_GOLFERS			6				//NUMBER OF AMBIENT GOLFERS PLAYING GOLF
CONST_INT			NUM_OF_AMB_CLUBS			6				//NUMBER OF GOLF CLUBS
CONST_INT			NUM_OF_AMB_CHASE_GOLFERS	2				//NUMBER OF AMBIENT GOLFERS PRESENT DURING BUGGY CHASE
CONST_INT			NUM_OF_TRANS_BUGGIES		9				//NUMBER OF GOLF BUGGIES USED THROUGHOUT
CONST_INT			NUM_OF_AMB_BUGGIES			2				//NUMBER OF AMBIENT BUGGIES
CONST_INT			DROP_CLUB_HEALTH			600				//Health at which the golf clubs will drop from Celebrity golf buggy
CONST_INT			STUCK_COUNT					30				//COUNTER USED TO DETERMINE IF CELEBRITY GOLF BUGGY IS STUCK

// Const Ints for entity IDs in arrays
CONST_INT			BUGGY_CLUBFRONT1			0
CONST_INT			BUGGY_CLUBFRONT2			1
CONST_INT			BUGGY_PATH1					2
CONST_INT			BUGGY_PATH2					3
CONST_INT			BUGGY_BEHINDCELEB			4
CONST_INT			BUGGY_NEXTTOCELEB			5
CONST_INT			BUGGY_NORTHCARPARK			6
CONST_INT			BUGGY_AMBIENTPED1			7
CONST_INT			BUGGY_AMBIENTPED2			8

CONST_INT			CELEBSEC_WEST				0
CONST_INT			CELEBSEC_EAST				1
CONST_INT			CELEBSEC_NORTH				2

CONST_INT			COURSESEC_ENTRANCE			0
CONST_INT			COURSESEC_ROADENTRY			1
CONST_INT			COURSESEC_CARPARK			2

CONST_INT			CONSIERGE_FRONT				0
CONST_INT			CONSIERGE_DRIVER			1
CONST_INT			CONSIERGE_RIGHTDOOR			2
CONST_INT			CONSIERGE_LEFTDOOR			3

CONST_INT			AMBIENTPED_HOLE4WATCH1		0
CONST_INT			AMBIENTPED_HOLE4WATCH2		1
CONST_INT			AMBIENTPED_NORTHLAKE		2
CONST_INT			AMBIENTPED_HOLE4PLAY		3
CONST_INT			AMBIENTPED_NEARBANKER		4
CONST_INT			AMBIENTPED_INBUNKER			5

CONST_INT			AMBIENTCHASEPED_ONGREEN		0
CONST_INT			AMBIENTCHASEPED_HOLE7SIGN	1


//CONST FLOATS
CONST_FLOAT			CF_CAR_STOPPING_DIS			8.0		//Distance car will stop at hotel entrance
CONST_FLOAT			CF_TALK_DIS					3.0			//Distance conserige will start talking to player
CONST_FLOAT			ALERT_DIS					11.0		//Distance celebrity is alerted to player
CONST_FLOAT			GOLFER_ALERT_DIS			20.0		//Distance ambeint golfers will spot player with a gun
CONST_FLOAT			LOSING_RANGE				65.0		//Range player is warned about losing
CONST_FLOAT			LOST_RANGE					140.0		//Range player loses celebrity / golf clubs
CONST_FLOAT			SLOW_DOWN_RANGE				20.0		//Range to slow down celebrity - Rubber banding
CONST_FLOAT			FIND_DIS					50.0
CONST_FLOAT			TRAFFIC_MULTIPLIER			0.75

//INTS
INT					iBuggieStuckCounter						//Used to determine if the celebrity buggy is stuck
INT 				iScaredConvoCounter
INT 				iChaseConvoCounter
INT					iFostLineTimer
INT					iGuardWarnTimer							//Used to give the player some warning if they are seen holding a weapon
INT					iGuardTaserTimer						//Used to stop the guards tasing the player over and over again 
INT					iHitDialogueTimer						//Used to make sure the celeb doesn't spam "you hit me" dialogue
INT					iScaredDialogueTimer					//Used to make sure celeb doesn't spam scared dialogue
INT					iChaseDialogueTimer						//Used to make sure celeb doesn't spam chase dialogue
INT					iSecurityWarnTimer						//Used for warnings for the celeb security
INT					iCelebWarnTimer							//Used for warnings for the celeb
INT					iClubPickupAudioID						//Used to play pickup audio

//
VECTOR 		vStopCarLocationOne 	=		<< -1304.7542, -38.3551, 47.0814 >>			//LOCATION 1 TO STOP PLAYER CAR IF NEAR GOLF COURSE SECURITY
VECTOR		vStopCarLocationTwo		=		<< -1340.5546, 17.6527, 51.7458 >>			//LOCATION 2 TO STOP PLAYER CAR IF NEAR GOLF COURSE SECURITY
VECTOR		vStopCarLocationThree	=		<< -1388.4355, 138.2263, 55.0734 >>			//LOCATION 3 TO STOP PLAYER CAR IF NEAR GOLF COURSE SECURITY
VECTOR		vAttackedPosition															//UPDATED OFF ATTACKED GOLFER POSITION

//ARRAYS

//PEDS
MISSION_PED			ConciergePed[NUM_OF_CONC_PEDS]						//CONSEIRGE PEDS
MISSION_PED			CelebrityGolfer										//CELEBRITY GOLFER - GLEN STANKY
MISSION_PED			CelebrityFriend										//CELEBRITY GOLFERS FRIEND
MISSION_PED			CelebSecurity[NUM_OF_SECURITY]						//CELEBRITY PERSONAL SECURITY
MISSION_PED			CourseSecurity[NUM_OF_COURSE_SECURITY]				//GOLF COURSE SECURITY
MISSION_PED			AmbientGolfer[NUM_OF_AMB_GOLFERS]					//AMBIENT GOLFERS
MISSION_PED			AmbientChaseGolfer[NUM_OF_AMB_CHASE_GOLFERS]		//AMBIENT GOLFERS DURING BUGGY CHASE

//States
SECURITY_STATE		eSecurityState[NUM_OF_SECURITY]						//PERSONAL SECURITY STATE
AMBIENT_STATE		eAmbientState[NUM_OF_AMB_GOLFERS]					//AMBIENT GOLFER STATE
AMBIENT_STATE		eChaseGolferState[NUM_OF_AMB_CHASE_GOLFERS]			//AMBIENT GOLFERS DURING CHASE STATE
COURSE_SECURITY		eCourseSecurity[NUM_OF_COURSE_SECURITY]				//GOLF COURSE SECURITY STATE
	
//CONCIERGE STATES
CONCIERGE_STATE 	eConsiergeState[NUM_OF_CONC_PEDS]					//CONCIERGE STATE

//OUTFIT CHANGING STATE
OUTFIT_STATE		eOutfitState										//OUTFIT STATE

//WANTED TRACKING STATE
WANTED_STATE		eWantedState										//WANTED STATE

//TASED TRACKING STATE
PLAYER_TASE_STATE	eTaseState = TS_NOT_TASED

//CELEB SPEECH STATE
CELEB_SWING_DIALOGUE eSwingSpeech = SWING_TRIGGER

//VEHICLES
MISSION_VEHICLE		GolfBuggieTransport[NUM_OF_TRANS_BUGGIES]			//GOLF BUGGY'S DURING MISSION
MISSION_VEHICLE		CelebrityBuggy										//CELEBRITY'S PERSONAL GOLF BUGGY
MISSION_VEHICLE		AmbientGolfBuggy[NUM_OF_AMB_BUGGIES]				//AMBIENT GOLF BUGGYS DURING CHASE
MISSION_VEHICLE		ReplayVehicle											//SPECIAL BUGGY GIVEN IF PLAYER REPLAYS MISSION AFTER FAILING

//Relationship groups
REL_GROUP_HASH 		relHatePlayer

//Sequences
SEQUENCE_INDEX		siCelebSecurity
SEQUENCE_INDEX		siCelebrity

//PROPS
OBJECT_INDEX		oiGolfBag											//GOLF BAG ATTACHED TO BACK OF CELEBRITY GOLF BUGGY

SCENARIO_BLOCKING_INDEX	sbiGolfer
SCENARIO_BLOCKING_INDEX	sbiConc
INT iNavBlocker = -1
  
//MODELS
MODEL_NAMES 		CelebrityModel	= U_M_M_MARKFOST					//CELEBRITY MODEL

//pickups
MISSION_PICKUP 		golfClubPickup										//GOLF CLUB PICKUP

//Props
MISSION_PROP		AmbientClubs[NUM_OF_AMB_CLUBS]						//CLUBS GIVEN TO AMBIENT GOLFERS
MISSION_PROP		CelebFriendClub										//CELEBRITY FRIENDS CLUB

//Sequences

TEST_POLY			tpGolfCourse										//AREA TO DETERMINE IF PLAYER HAS ENTERED GOLF COURSE
TEST_POLY			tpGolfCourseExpanded
TEST_POLY			tpLeftSideOfCourse									//AREA TO DETERMINE WHICH SIDE PLAYER IS ATTACKING CELEBRITY FROM

//INIT ARRAYS
/// PURPOSE:
///    Loads all the inital arrays - PED LOCATIONS etc. 
PROC INIT_ARRAYS
	
	//Celebrity
	CelebrityGolfer.vStartPos = << -1096.8550, 67.6858, 52.9520 >>
	CelebrityGolfer.fStartHeading = 112.0
	CelebrityGolfer.mModel = CelebrityModel
	
	
	//Celebrity chase vehicle
	CelebrityBuggy.vStartPos = << -1099.9100, 61.2608, 52.8124 >>
	CelebrityBuggy.fStartHeading = 243.0
	CelebrityBuggy.mModel = caddy
	
	//Celerbrity Friend
	CelebrityFriend.vStartPos = << -1096.2576, 69.7230, 53.0107 >>
	CelebrityFriend.fStartHeading = 163.0
	CelebrityFriend.mModel = A_M_Y_Golfer_01
	
	//Golf club for celeb Friend
	CelebFriendClub.vStartPos = CelebrityFriend.vStartPos
	CelebFriendClub.mModel = prop_golf_wood_01
	
	///Buggies parked at front of the club
	GolfBuggieTransport[BUGGY_CLUBFRONT1].vStartPos = << -1370.1924, 61.6015, 52.4847 >>
	GolfBuggieTransport[BUGGY_CLUBFRONT1].fStartHeading = 206.0
	GolfBuggieTransport[BUGGY_CLUBFRONT1].mModel = caddy
	GolfBuggieTransport[BUGGY_CLUBFRONT2].vStartPos = << -1373.8676, 65.9104, 52.5826 >>
	GolfBuggieTransport[BUGGY_CLUBFRONT2].fStartHeading = 224.0
	GolfBuggieTransport[BUGGY_CLUBFRONT2].mModel = caddy
	
	//Buggies parked near path
	GolfBuggieTransport[BUGGY_PATH1].vStartPos =	<< -1302.4132, -6.6432, 48.8636 >>
	GolfBuggieTransport[BUGGY_PATH1].fStartHeading = 20.0
	GolfBuggieTransport[BUGGY_PATH1].mModel = caddy
	GolfBuggieTransport[BUGGY_PATH2].vStartPos = << -1328.5121, 26.7404, 52.4943 >>
	GolfBuggieTransport[BUGGY_PATH2].fStartHeading = 272.0
	GolfBuggieTransport[BUGGY_PATH2].mModel = caddy
	
	//Buggies near celebrity
	GolfBuggieTransport[BUGGY_BEHINDCELEB].vStartPos =	<< -1102.3347, 75.7757, 53.2591 >>
	GolfBuggieTransport[BUGGY_BEHINDCELEB].fStartHeading = 293.0
	GolfBuggieTransport[BUGGY_BEHINDCELEB].mModel = caddy
	
	//One directly behind celebs buggy, directly adjacent to celeb
	GolfBuggieTransport[BUGGY_NEXTTOCELEB].vStartPos =	<< -1103.0232, 65.6267, 53.0560 >>
	GolfBuggieTransport[BUGGY_NEXTTOCELEB].fStartHeading = 203.0
	GolfBuggieTransport[BUGGY_NEXTTOCELEB].mModel = caddy
	//
	
	//Top north side car park
	GolfBuggieTransport[BUGGY_NORTHCARPARK].vStartPos =	<< -1356.70, 145.34, 55.67 >>
	GolfBuggieTransport[BUGGY_NORTHCARPARK].fStartHeading = 187.0
	GolfBuggieTransport[BUGGY_NORTHCARPARK].mModel = caddy
	
	//Near Ambient Golfers
	GolfBuggieTransport[BUGGY_AMBIENTPED1].vStartPos =	<< -1158.52, 68.38, 55.97 >>
	GolfBuggieTransport[BUGGY_AMBIENTPED1].fStartHeading = 226.84
	GolfBuggieTransport[BUGGY_AMBIENTPED1].mModel = caddy
	
	GolfBuggieTransport[BUGGY_AMBIENTPED2].vStartPos =	<< -1165.3975, 72.9072, 55.6258 >>
	GolfBuggieTransport[BUGGY_AMBIENTPED2].fStartHeading = 238.0
	GolfBuggieTransport[BUGGY_AMBIENTPED2].mModel = caddy
	
	//Buggy used for replay
	ReplayVehicle.vStartPos = << -1191.22, 36.83, 52.03 >>
	ReplayVehicle.fStartHeading = -77.1337
	ReplayVehicle.mModel = caddy
	
	//Security PEDS
	CelebSecurity[CELEBSEC_WEST].vStartPos = << -1105.6224, 66.4609, 53.0700 >>
	CelebSecurity[CELEBSEC_WEST].fStartHeading = 118.0
	CelebSecurity[CELEBSEC_WEST].mModel = S_M_M_HighSec_01
	CelebSecurity[CELEBSEC_EAST].vStartPos = << -1090.0902, 64.3905, 52.5607 >>
	CelebSecurity[CELEBSEC_EAST].fStartHeading = 238.0
	CelebSecurity[CELEBSEC_EAST].mModel = S_M_M_HighSec_01
	CelebSecurity[CELEBSEC_NORTH].vStartPos = << -1093.7360, 73.8568, 53.0388 >>
	CelebSecurity[CELEBSEC_NORTH].fStartHeading = 319.0
	CelebSecurity[CELEBSEC_NORTH].mModel = S_M_M_HighSec_01
	
	
	//Golf course security peds - Warn player about bringing vehicle onto course etc. 
	CourseSecurity[COURSESEC_ENTRANCE].vStartPos = << -1298.49, -23.89, 47.99 >>
	CourseSecurity[COURSESEC_ENTRANCE].fStartHeading = 126.55
	CourseSecurity[COURSESEC_ENTRANCE].vFleePos = << -1314.0590, -35.6361, 48.1836 >>
	CourseSecurity[COURSESEC_ENTRANCE].mModel = S_M_M_Security_01
	CourseSecurity[COURSESEC_ROADENTRY].vStartPos =  << -1344.5045, 18.8674, 51.9921 >>
	CourseSecurity[COURSESEC_ROADENTRY].fStartHeading = 205.0
	CourseSecurity[COURSESEC_ROADENTRY].vFleePos = << -1344.40, 18.11, 52.19 >>
	CourseSecurity[COURSESEC_ROADENTRY].mModel = S_M_M_Security_01
	CourseSecurity[COURSESEC_CARPARK].vStartPos = << -1376.71, 140.20, 55.25 >>
	CourseSecurity[COURSESEC_CARPARK].fStartHeading = 99.0
	CourseSecurity[COURSESEC_CARPARK].vFleePos = << -1375.5769, 131.1931, 55.3238 >>
	CourseSecurity[COURSESEC_CARPARK].mModel = S_M_M_Security_01
	
	
	//Front Concierge PEDS
	ConciergePed[CONSIERGE_FRONT].vStartPos = << -1377.18, 55.39, 53.70 >>
	ConciergePed[CONSIERGE_FRONT].fStartHeading = 101.0
	ConciergePed[CONSIERGE_FRONT].mModel = S_M_Y_Valet_01
	//Driver of buggy
	ConciergePed[CONSIERGE_DRIVER].vStartPos = << -1369.3046, 52.1333, 52.4768 >>
	ConciergePed[CONSIERGE_DRIVER].fStartHeading = 20.0
	ConciergePed[CONSIERGE_DRIVER].mModel = S_M_Y_Valet_01
	
	ConciergePed[CONSIERGE_RIGHTDOOR].vStartPos = << -1366.5809, 54.0400, 52.4633 >>
	ConciergePed[CONSIERGE_RIGHTDOOR].fStartHeading = 94.0
	ConciergePed[CONSIERGE_RIGHTDOOR].mModel = S_M_Y_Valet_01
	
	ConciergePed[CONSIERGE_LEFTDOOR].vStartPos = << -1367.6914, 60.2214, 52.4635 >>
	ConciergePed[CONSIERGE_LEFTDOOR].fStartHeading = 126.0
	ConciergePed[CONSIERGE_LEFTDOOR].mModel = S_M_Y_Valet_01
	
	//Ambient golf peds on course
	AmbientGolfer[AMBIENTPED_HOLE4WATCH1].vStartPos = << -1161.1089, 72.2742, 55.6518 >>
	AmbientGolfer[AMBIENTPED_HOLE4WATCH1].fStartHeading = 328.4240
	AmbientGolfer[AMBIENTPED_HOLE4WATCH1].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_HOLE4WATCH1].mStartAnim = "idle_b"
	AmbientGolfer[AMBIENTPED_HOLE4WATCH2].vStartPos = << -1159.8032, 71.7432, 55.6988 >>
	AmbientGolfer[AMBIENTPED_HOLE4WATCH2].fStartHeading = 344.2791
	AmbientGolfer[AMBIENTPED_HOLE4WATCH2].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_HOLE4WATCH2].mStartAnim = "idle_c"
	AmbientGolfer[AMBIENTPED_NORTHLAKE].vStartPos = << -1143.1727, 112.0022, 58.1151 >>
	AmbientGolfer[AMBIENTPED_NORTHLAKE].fStartHeading = 109.0
	AmbientGolfer[AMBIENTPED_NORTHLAKE].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_NORTHLAKE].mStartAnim = "wood_idle_low_b"
	AmbientGolfer[AMBIENTPED_HOLE4PLAY].vStartPos = << -1157.1703, 79.4863, 56.7109 >>
	AmbientGolfer[AMBIENTPED_HOLE4PLAY].fStartHeading = 159.0
	AmbientGolfer[AMBIENTPED_HOLE4PLAY].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_HOLE4PLAY].mStartAnim = "wedge_idle_high_c"
	AmbientGolfer[AMBIENTPED_NEARBANKER].vStartPos = << -1060.1726, 79.7097, 51.0781 >>
	AmbientGolfer[AMBIENTPED_NEARBANKER].fStartHeading = 222.0
	AmbientGolfer[AMBIENTPED_NEARBANKER].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_NEARBANKER].mStartAnim = "wedge_idle_high_b"
	AmbientGolfer[AMBIENTPED_INBUNKER].vStartPos = << -1025.1515, 8.6190, 48.7672 >>
	AmbientGolfer[AMBIENTPED_INBUNKER].fStartHeading = 133.0
	AmbientGolfer[AMBIENTPED_INBUNKER].mModel = A_M_Y_Golfer_01
	AmbientGolfer[AMBIENTPED_INBUNKER].mStartAnim = "putt_idle_high_b"
	
	//Golf clubs for ambeint golfes
	AmbientClubs[AMBIENTPED_HOLE4WATCH1].vStartPos = AmbientGolfer[AMBIENTPED_HOLE4WATCH1].vStartPos
	AmbientClubs[AMBIENTPED_HOLE4WATCH1].mModel = PROP_GOLF_PUTTER_01
	AmbientClubs[AMBIENTPED_HOLE4WATCH2].vStartPos = AmbientGolfer[AMBIENTPED_HOLE4WATCH2].vStartPos
	AmbientClubs[AMBIENTPED_HOLE4WATCH2].mModel = PROP_GOLF_PUTTER_01
	AmbientClubs[AMBIENTPED_NORTHLAKE].vStartPos = AmbientGolfer[AMBIENTPED_NORTHLAKE].vStartPos
	AmbientClubs[AMBIENTPED_NORTHLAKE].mModel = prop_golf_wood_01
	AmbientClubs[AMBIENTPED_HOLE4PLAY].vStartPos = AmbientGolfer[AMBIENTPED_HOLE4PLAY].vStartPos
	AmbientClubs[AMBIENTPED_HOLE4PLAY].mModel = prop_golf_wood_01
	AmbientClubs[AMBIENTPED_NEARBANKER].vStartPos = AmbientGolfer[AMBIENTPED_NEARBANKER].vStartPos
	AmbientClubs[AMBIENTPED_NEARBANKER].mModel = PROP_GOLF_IRON_01
	AmbientClubs[AMBIENTPED_INBUNKER].vStartPos = AmbientGolfer[AMBIENTPED_INBUNKER].vStartPos
	AmbientClubs[AMBIENTPED_INBUNKER].mModel = PROP_GOLF_PUTTER_01
	
	//Ambient golf PEDS used during celebrity buggy chase 
	AmbientChaseGolfer[AMBIENTCHASEPED_ONGREEN].vStartPos = << -1096.2777, -117.3159, 40.5370 >>
	AmbientChaseGolfer[AMBIENTCHASEPED_ONGREEN].vFleePos = << -1095.01, -56.45, 43.83 >>
	AmbientChaseGolfer[AMBIENTCHASEPED_ONGREEN].fStartHeading = 343.0
	AmbientChaseGolfer[AMBIENTCHASEPED_ONGREEN].mModel =  A_M_Y_Golfer_01
	AmbientChaseGolfer[AMBIENTCHASEPED_HOLE7SIGN].vStartPos = << -1115.5482, -104.4307, 40.8407 >>
	AmbientChaseGolfer[AMBIENTCHASEPED_HOLE7SIGN].vFleePos = << -1139.6190, -114.8868, 40.0996 >>
	AmbientChaseGolfer[AMBIENTCHASEPED_HOLE7SIGN].fStartHeading = 45.0
	AmbientChaseGolfer[AMBIENTCHASEPED_HOLE7SIGN].mModel =  A_M_Y_Golfer_01
	
	//Ambient golf buggies used during celebrity buggy chase
	AmbientGolfBuggy[AMBIENTCHASEPED_ONGREEN].vStartPos = << -1100.8177, -103.5910, 40.8740 >>
	AmbientGolfBuggy[AMBIENTCHASEPED_ONGREEN].fStartHeading = 99.0
	AmbientGolfBuggy[AMBIENTCHASEPED_ONGREEN].mModel = caddy
	AmbientGolfBuggy[AMBIENTCHASEPED_HOLE7SIGN].vStartPos = << -1114.0878, -97.6378, 41.0175 >>
	AmbientGolfBuggy[AMBIENTCHASEPED_HOLE7SIGN].fStartHeading = 233.0
	AmbientGolfBuggy[AMBIENTCHASEPED_HOLE7SIGN].mModel = caddy

		
ENDPROC
  
// ---------------------------
//  FUNCTIONS
// ---------------------------

/// PURPOSE:
///    Sets all models as no longer needed, removes all anim dicts, removes all vehicle recordings etc.
PROC UnloadEverything()
	
	INT i
	
	///PEDS
	
	FOR i = 0 to NUM_OF_CONC_PEDS - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(ConciergePed[i].mModel)
	ENDFOR
	
	FOR i = 0 to NUM_OF_COURSE_SECURITY - 1 
		SET_MODEL_AS_NO_LONGER_NEEDED(CourseSecurity[i].mModel)
	ENDFOR
	
	FOR i = 0 to NUM_OF_SECURITY - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(celebSecurity[i].mModel)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_GOLFERS - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(AmbientGolfer[i].mModel)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_CHASE_GOLFERS - 1 
		SET_MODEL_AS_NO_LONGER_NEEDED(AmbientChaseGolfer[i].mModel)
	ENDFOR
	
	SET_MODEL_AS_NO_LONGER_NEEDED(CelebrityModel)
	
	///

	//VEHICLES
	FOR i = 0 to NUM_OF_TRANS_BUGGIES - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(GolfBuggieTransport[i].mModel)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_BUGGIES - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(AmbientGolfBuggy[i].mModel)
	ENDFOR
	
	///
	
	//PROPS
	FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(AmbientClubs[i].mModel)
	ENDFOR
	///
	
		
ENDPROC

/// PURPOSE:
///    Makes the PED passed in flee from the player
/// PARAMS:
///    mPedFlee - Makes this ped flee from the player. 
PROC MAKE_PED_FLEE(PED_INDEX mPedFlee, BOOL OnFoot = FALSE)

	IF IS_PED_UNINJURED(mPedFlee)
		IF OnFoot = FALSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_MAINTAIN_MIN_DISTANCE_TO_TARGET, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_CAN_USE_FRUSTRATED_ADVANCE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_LEAVE_VEHICLES, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_DISABLE_HANDS_UP, TRUE) 
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			TASK_SMART_FLEE_PED(mPedFlee, PLAYER_PED_ID(), 200, -1,FALSE)	
			SET_PED_KEEP_TASK(mPedFlee, TRUE)
		ELSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_MAINTAIN_MIN_DISTANCE_TO_TARGET, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_CAN_USE_FRUSTRATED_ADVANCE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee, CA_LEAVE_VEHICLES, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_FORCE_EXIT_VEHICLE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_DISABLE_HANDS_UP, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			OPEN_SEQUENCE_TASK(siCelebrity)
//				IF IS_PED_IN_ANY_VEHICLE(mPedFlee)
//					TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
//				ENDIF
				//TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mPedFlee, <<-1.5, 5, 0>>), PEDMOVEBLENDRATIO_SPRINT)
				TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1,TRUE)	
			CLOSE_SEQUENCE_TASK(siCelebrity)
			TASK_PERFORM_SEQUENCE(mPedFlee, siCelebrity)
			CLEAR_SEQUENCE_TASK(siCelebrity)
			SET_PED_KEEP_TASK(mPedFlee, TRUE)
		ENDIF
	ENDIF
ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

/// PURPOSE: Cleanup that needs to be done as soon as mission is passed or failed. Blip removal etc
PROC MissionCleanup(bool bClearText = TRUE)
	IF bClearText = TRUE // clear all text
		CLEAR_PRINTS()
	ENDIF
	CLEAR_HELP(TRUE)
ENDPROC

//PURPOSE: DEBUG - print string into the TTY
PROC DEBUG_PRINT(STRING s)

	#IF IS_DEBUG_BUILD
		IF bDebug_PrintToTTY
			CPRINTLN(DEBUG_MISSION, s)
		ENDIF 
	#ENDIF
	
	// Stop release compile error
	s = s
ENDPROC

//PURPOSE: DEBUG - print int and string into the TTY
PROC DEBUG_PRINT_INT(STRING s, INT i)

	#IF IS_DEBUG_BUILD
		IF bDebug_PrintToTTY
			CPRINTLN(DEBUG_MISSION, s, i)
		ENDIF 
	#ENDIF
	
	// Stop release compile error
	s = s
	i = i
ENDPROC

//PURPOSE: DEBUG - print int and string into the TTY
PROC DEBUG_PRINT_FLOAT(STRING s, FLOAT f)

	#IF IS_DEBUG_BUILD
		IF bDebug_PrintToTTY
			CPRINTLN(DEBUG_MISSION, s, f)
		ENDIF 
	#ENDIF
	
	// Stop release compile error
	s = s
	f = f
	
ENDPROC


/// PURPOSE:
///    Very efficient test for if two entities are with fRange metres of each other
///    Will be more efficient than doing IS_ENTITY_AT_ENTITY()
///    NOTE: I am explicitly NOT doing a dead check for this because I need the coords of a DEAD ped!
/// PARAMS:
///    e1 - First entity - must be the dead one!
///    e2 - Second entity
///    fRange - Test if the entities are this close to each other
FUNC BOOL IS_ENTITY_IN_RANGE_ENTITY_DEAD(ENTITY_INDEX e1, ENTITY_INDEX e2, FLOAT fRange)
	RETURN ( VDIST2(GET_ENTITY_COORDS(e1, FALSE), GET_ENTITY_COORDS(e2)) <= fRange*fRange )
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup(BOOL bClearText = TRUE)
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	MissionCleanup(bClearText)
	
	UnloadEverything()
	
	RESET_MISSION_STATS_ENTITY_WATCH()
	
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1D_SCENE")
		STOP_AUDIO_SCENE("NIGEL_1D_SCENE")
	ENDIF
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	// mark peds and vehicles as no longer needed here:
	INT i

	///PEDS
	
	FOR i = 0 to NUM_OF_CONC_PEDS - 1
		SAFE_RELEASE_PED(ConciergePed[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_COURSE_SECURITY - 1 
		SAFE_RELEASE_PED(CourseSecurity[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_SECURITY - 1
		SAFE_RELEASE_PED(celebSecurity[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_GOLFERS - 1
		SAFE_RELEASE_PED(AmbientGolfer[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_CHASE_GOLFERS - 1 
		SAFE_RELEASE_PED(AmbientChaseGolfer[i].mPed)
	ENDFOR
	
	SAFE_RELEASE_PED(CelebrityFriend.mPed)
	
	SAFE_RELEASE_PED(CelebrityGolfer.mPed)

	//VEHICLES
	FOR i = 0 to NUM_OF_TRANS_BUGGIES - 1
		SAFE_RELEASE_VEHICLE(GolfBuggieTransport[i].mVehicle)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_BUGGIES - 1
		SAFE_RELEASE_VEHICLE(AmbientGolfBuggy[i].mVehicle)
	ENDFOR
	
	SAFE_RELEASE_VEHICLE(CelebrityBuggy.mVehicle)
	
	///
	
	//PROPS
	FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
		SAFE_RELEASE_OBJECT(AmbientClubs[i].mObject)
	ENDFOR
	
	SET_BUILDING_STATE(BUILDINGNAME_SB_N1D_GOLFER, BUILDINGSTATE_DESTROYED)
	g_bBuildingScenarioBlocksUpdateThisFrame = TRUE
	
	WAIT(0)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiGolfer)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiConc)
	
	IF iNavBlocker > -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker)
	ENDIF
	
	REMOVE_ANIM_DICT("rcmnigel1d")

	REMOVE_ANIM_DICT("mini@golf")
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE) //Cleanup the scene created by the launcher
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	MissionCleanup()
	
	// Set the outfit as acquired (so it appears in player's wardrobe)
	IF bHasAcquiredGolfOutfit = TRUE
		SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_TREVOR), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, TRUE)
	ENDIF			

	Random_Character_Passed(CP_RAND_C_NIG1D)
	Script_Cleanup()
ENDPROC

/// PURPOSE: Sets the new mission stage and initialises the substate
PROC SetStage(MISSION_STAGE eNewStage)
	eMissionStage = eNewStage // Setup new mission state
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///  Sets the fail reason and mission stage to be fail fade out. 
PROC SET_FAIL_REASON(FAIL_REASON eNewFailReason)

	eFailReason = eNewFailReason
	SETSTAGE(MS_MISSION_FAILED)

ENDPROC

/// PURPOSE: Resets the gameplay camera behind the player
PROC ResetCamBehindPlayer()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
ENDPROC

/// PURPOSE:
///    Enable the ability to change into the golf outfit during the mission
PROC ACTIVATE_OUTFIT_CHANGE(BOOL bForceActivate = FALSE)
	IF eOutfitState != OS_OUTFIT_CHANGED // Don't re-activate the outfit change if it has already been used
	OR bForceActivate = TRUE
		IF NOT DOES_BLIP_EXIST(biConciergeBlip)
			biConciergeBlip = CREATE_BLIP_FOR_COORD(<< -1377.18, 55.39, 53.70  >>)
			SET_BLIP_SPRITE(biConciergeBlip, RADAR_TRACE_GOLF)
		ENDIF
		eOutfitState = OS_WAIT_FOR_PLAYER // Reset the outfit change manager
	ENDIF
ENDPROC

/// PURPOSE:
///    Disable the ability to change into the golf outfit during the mission
PROC DEACTIVATE_OUTFIT_CHANGE()
	SAFE_REMOVE_BLIP(biConciergeBlip)
	eOutfitState = OS_DISABLED // Disable the outfit change manager
ENDPROC

/// PURPOSE:
///    Manage the process of changing outfits when the player goes to the Golf icon during the mission
PROC HANDLE_OUTFIT_CHANGE()
	SWITCH eOutfitState
		CASE OS_WAIT_FOR_PLAYER
			// wait for player to enter concierge area
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1377.18, 55.39, 53.70>>, <<2,2,2>>, FALSE, TRUE, TM_ON_FOOT)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1379.153687,57.951008,52.190113>>, <<-1378.735352,53.172840,54.940182>>, 4.000000)
						eOutfitState = OS_DETECTED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE OS_DETECTED
			// Create in-transition cams
			IF NOT DOES_CAM_EXIST(ciOutfitTransitionHelper)
				ciOutfitTransitionHelper = GET_GAME_CAMERA_COPY()
			ENDIF
			IF NOT DOES_CAM_EXIST(ciOutfitTransitionIn)
				ciOutfitTransitionIn = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SPLINE_DEFAULT, << -1409.4524, 53.5135, 78.0868 >>,  << -26.7543, 0.0000, -85.6976 >>, 50)	
				SET_CAM_SPLINE_DURATION(ciOutfitTransitionIn , 1500*3)
				ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(ciOutfitTransitionIn, ciOutfitTransitionHelper, 1500)
				ADD_CAM_SPLINE_NODE(ciOutfitTransitionIn, << -1382.7190, 55.2160, 58.0408 >>, << -15.6873, 0.0000, -88.6059 >>, 1500)
				ADD_CAM_SPLINE_NODE(ciOutfitTransitionIn, << -1409.4524, 53.5135, 78.0868 >>,  << -26.7543, 0.0000, -85.6976 >>, 1500)
			ENDIF
			// turn off controls + radar
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			DISPLAY_RADAR(FALSE)
			// move Trev towards the doors to hide him while we change his outfit
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1368.3302, 56.4194, 52.4637>>, PEDMOVEBLENDRATIO_WALK)
			ENDIF
			eOutfitState = OS_DO_CAMERA_UP
		BREAK
		CASE OS_DO_CAMERA_UP
			// Move camera up into position
			IF NOT IS_CAM_ACTIVE(ciOutfitTransitionHelper)
				IF NOT IS_CAM_ACTIVE(ciOutfitTransitionIn)
					SET_CAM_ACTIVE(ciOutfitTransitionHelper, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, TRUE)
				ENDIF
			ELIF NOT IS_CAM_ACTIVE(ciOutfitTransitionIn) 
				SET_CAM_ACTIVE(ciOutfitTransitionIn, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, TRUE)
				eOutfitState = OS_CHANGE_OUTFIT
			ENDIF
		BREAK
		CASE OS_CHANGE_OUTFIT
			// Change the outfit
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1368.3302, 56.4194, 52.4637>>, <<2,2,2>>, FALSE, TRUE, TM_ON_FOOT)
				WAIT(2000)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, FALSE)
				// Do this when this function has updated!!
				SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
				
				// set outfit acquried flag
				bHasAcquiredGolfOutfit = TRUE
				
				// Move Trev back towards the concierge
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1373.09, 55.61, 52.71>>, PEDMOVEBLENDRATIO_WALK) 
					// Make the player go back to a coord of my choice because it's safer
				ENDIF
				WAIT(500)
				eOutfitState = OS_DO_CAMERA_DOWN
			ENDIF
		BREAK
		CASE OS_DO_CAMERA_DOWN
			// Move camera back down to player level using a new camera to avoid the transition going through the clubhouse mesh	
			IF NOT DOES_CAM_EXIST(ciOutfitTransitionOut) 
				ciOutfitTransitionOut = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SPLINE_DEFAULT,<< -1373.6809, 57.0584, 54.9717 >>, << -10.6657, -0.0000, 91.5916 >>, 50)	
				SET_CAM_SPLINE_DURATION(ciOutfitTransitionOut , 4500)
				ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(ciOutfitTransitionOut, ciOutfitTransitionIn, 1500)
				ADD_CAM_SPLINE_NODE(ciOutfitTransitionOut, << -1382.7190, 55.2160, 58.0408 >>, << -15.6873, 0.0000, -88.6059 >>, 1500)
				ADD_CAM_SPLINE_NODE(ciOutfitTransitionOut, << -1373.6809, 57.0584, 54.9717 >>, << -10.6657, -0.0000, 91.5916 >>, 1500)
			ENDIF
			
			IF NOT IS_CAM_ACTIVE(ciOutfitTransitionOut) 
				RENDER_SCRIPT_CAMS(TRUE, TRUE)
				SET_CAM_ACTIVE(ciOutfitTransitionOut, TRUE)
				WAIT(3500)
				eOutfitState = OS_OUTFIT_CHANGED
			ENDIF
			
			IF IS_CAM_ACTIVE(ciOutfitTransitionHelper)
				SET_CAM_ACTIVE(ciOutfitTransitionHelper, FALSE)
			ENDIF
			
			IF IS_CAM_ACTIVE(ciOutfitTransitionIn) 
				SET_CAM_ACTIVE(ciOutfitTransitionIn, FALSE)
			ENDIF

		BREAK
		CASE OS_OUTFIT_CHANGED
			// Set the transition camera to inactive and remove the blip
			IF DOES_CAM_EXIST(ciOutfitTransitionOut)
				IF IS_CAM_ACTIVE(ciOutfitTransitionOut) 
					RENDER_SCRIPT_CAMS(FALSE, TRUE)
					SET_CAM_ACTIVE(ciOutfitTransitionOut, FALSE)
					WAIT(2000)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					DISPLAY_RADAR(TRUE)
					DESTROY_CAM(ciOutfitTransitionHelper)
					DESTROY_CAM(ciOutfitTransitionIn)
					DESTROY_CAM(ciOutfitTransitionOut)
				ENDIF
			ENDIF
			SAFE_REMOVE_BLIP(biConciergeBlip)
		BREAK
		CASE OS_DISABLED
			// Do nothing
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: creates ped and sets heading
PROC CreateMissionPed(MISSION_PED &mMissionPed, BOOL bUnloadModel = TRUE)
	if HAS_MODEL_LOADED(mMissionPed.mModel)
		mMissionPed.mPed = CREATE_PED(PEDTYPE_MISSION, mMissionPed.mModel, mMissionPed.vStartPos)
		if IS_ENTITY_ALIVE(mMissionPed.mPed)
			SET_ENTITY_HEADING(mMissionPed.mPed, mMissionPed.fStartHeading)
		ENDIF
		if bUnloadModel = TRUE
			SET_MODEL_AS_NO_LONGER_NEEDED(mMissionPed.mModel)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: creates vehicle and sets heading and colours
PROC CreateMissionVehicle(MISSION_VEHICLE &mMissionVehicle, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionVehicle.mModel)
		mMissionVehicle.mVehicle = CREATE_VEHICLE(mMissionVehicle.mModel, mMissionVehicle.vStartPos, mMissionVehicle.fStartHeading)
	ENDIF

	if bRemoveModel = TRUE
		SET_MODEL_AS_NO_LONGER_NEEDED(mMissionVehicle.mModel)
	ENDIF
ENDPROC

/// PURPOSE: creates props and sets heading
PROC CreateMissionProp(MISSION_PROP &mMissionProp, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionProp.mModel)
		mMissionProp.mObject = CREATE_OBJECT(mMissionProp.mModel, mMissionProp.vStartPos)
		if DOES_ENTITY_EXIST(mMissionProp.mObject)
			SET_ENTITY_HEADING(mMissionProp.mObject,mMissionProp.fStartHeading)
		ENDIF
	ENDIF

	if bRemoveModel = TRUE
		SET_MODEL_AS_NO_LONGER_NEEDED(mMissionProp.mModel)
	ENDIF
ENDPROC

FUNC BOOL IS_PED_INSIDE_SOFT_BOUNDARY(PED_INDEX pedIndex)
	//DISPLAY_POLY2(tpGolfCourseExpanded)
	IF IS_POINT_IN_POLY_2D(tpGolfCourseExpanded, GET_ENTITY_COORDS(pedIndex, FALSE))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if a PED is inside the golf course area
/// PARAMS:
///    pedIndex - The ped passing in
/// RETURNS:
///    True if they are inside the area, false other wise
FUNC BOOL IS_PED_INSIDE_GOLF_AREA(PED_INDEX pedIndex)
	//DISPLAY_POLY2(tpGolfCourse)
	IF IS_POINT_IN_POLY_2D(tpGolfCourse, GET_ENTITY_COORDS(pedIndex))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_ABANDONMENT()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF NOT IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
			IF NOT IS_PED_INSIDE_SOFT_BOUNDARY(PLAYER_PED_ID())
				SET_FAIL_REASON(FAIL_LION_ESCAPED)
			ELSE
				//UPDATE_CHASE_BLIP(biMissionBlip, CelebrityGolfer.mPed, LOST_RANGE, DEFAULT, TRUE)
				SET_BLIP_FLASHES(biMissionBlip,TRUE)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(biMissionBlip)
				DEBUG_PRINT("Recreated mission blip")
				biMissionBlip = CREATE_PED_BLIP(CelebrityGolfer.mPed,TRUE,FALSE,BLIPPRIORITY_MED)
			ELSE
				SET_BLIP_FLASHES(biMissionBlip,FALSE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks to see if the player is unarmed - Including objects
///    Used to see if PEDS should attack player
/// RETURNS:
///    TRUE - If player is unarmed, false otherwise
FUNC BOOL IS_PLAYER_UNARMED()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED 
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT //CHECK FOR MOBILE PHONE AS THIS WILL TRIGGER
		// Allow the player to hold a golf club without reactions, because... it's a golf club
		IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_GOLFCLUB
			RETURN FALSE
		ENDIF
		// Player is armed with something, but we don't want to return true when we're in a vehicle because the weapon isn't visible and it doesn't make sense
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		// If the player's shooting in a vehicle, then the weapon is visible and we should return false
		ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///   Checks to see if the player is using any sniper rifle
/// RETURNS:
///   True  - If the player is using a sniper rifle, fasle otherwise
FUNC BOOL IS_PLAYER_USING_SNIPER_RIFLE()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_SNIPERRIFLE 
	OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_HEAVYSNIPER 
	OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_REMOTESNIPER 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if the player is on the left side of Glen as they approach him
/// RETURNS:
///    True if the player is on the left side, false otherwise
FUNC BOOL IS_PLAYER_AT_LEFT_SIDE()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_POINT_IN_POLY_2D(tpLeftSideOfCourse, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE


ENDFUNC

/// PURPOSE:
///    Custom flee proc for the ambient golfers to make them drop their clubs when they flee
/// PARAMS:
///    iPedNum - The ped array ID we're making flee
PROC MAKE_AMBIENT_GOLF_PED_FLEE(int iPedNum)
	IF DOES_ENTITY_EXIST(AmbientClubs[iPedNum].mObject)
		IF IS_ENTITY_ATTACHED(AmbientClubs[iPedNum].mObject)
			DETACH_ENTITY(AmbientClubs[iPedNum].mObject)
		ENDIF
	ENDIF
	MAKE_PED_FLEE(AmbientGolfer[iPedNum].mPed,TRUE)
ENDPROC

PROC MAKE_ALL_AMBIENT_GOLFERS_FLEE()
	int i
	FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
		IF IS_ENTITY_ALIVE(AmbientGolfer[i].mPed)
			MAKE_AMBIENT_GOLF_PED_FLEE(i)
			eAmbientState[i] = AS_FLEE
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Checks if any of the celeb scene peds have died or if the player is doing anything dodgy to anyone/thing in the scene
/// RETURNS:
///    TRUE if any of the peds are dead, the player's bumped into any of them, has jacked any of the buggies or any of the peds go to ragdoll
///    FALSE otherwise
FUNC BOOL HAS_PLAYER_MUCKED_ABOUT()

	INT i
	FOR i = 0 TO NUM_OF_SECURITY - 1
		IF NOT IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
		OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), CelebSecurity[i].mPed)
		OR IS_PED_RAGDOLL(CelebSecurity[i].mPed)
			DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Player bumped celeb security with vehicle OR celeb security dead")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	IF NOT IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
	OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), CelebrityGolfer.mPed)
	OR IS_PED_RAGDOLL(CelebrityGolfer.mPed)
		DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Celeb ragdoll OR player bumped celeb with vehicle OR celeb isn't alive")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(CelebrityFriend.mPed)
	OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), CelebrityFriend.mPed)
	OR IS_PED_RAGDOLL(CelebrityFriend.mPed)
		DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Celeb friend ragdoll OR player bumped friend with vehicle OR friend isn't alive")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CelebrityBuggy.mVehicle)
			DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Player in celeb's golf cart")
			RETURN TRUE
		ENDIF
	ELSE
		// The player must've destroyed the buggy, which is a big no-no#
		DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Celeb golf cart not OK, player destroyed it?")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_OK(GolfBuggieTransport[BUGGY_BEHINDCELEB].mVehicle)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GolfBuggieTransport[BUGGY_BEHINDCELEB].mVehicle)
			DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Player in golf cart behind celeb")
			RETURN TRUE
		ENDIF
	ELSE
		// The player must've destroyed the buggy, which is a big no-no
		DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Golf cart behind celeb not OK, player destroyed it?")
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_OK(GolfBuggieTransport[BUGGY_NEXTTOCELEB].mVehicle)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GolfBuggieTransport[BUGGY_NEXTTOCELEB].mVehicle)
			DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Player in golf cart next to celeb")
			RETURN TRUE
		ENDIF
	ELSE
		// The player must've destroyed the buggy, which is a big no-no
		DEBUG_PRINT("HAS_PLAYER_MUCKED_ABOUT(): Golf cart next to celeb not OK, player destroyed it?")
		RETURN TRUE
	ENDIF
	
	// The player is behaving!
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///  Makes security PEDS attack player OR go to alert if the player isn't close enough to start the chase
///  Also makes Celebrity start to Flee in vehicle
PROC MAKE_SECURITY_ATTACK()

	IF bSecurityAttack = FALSE
	
		INT i
		
		FOR i = 0 TO NUM_OF_SECURITY - 1
			IF IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
				CLEAR_PED_TASKS(CelebSecurity[i].mPed)
				OPEN_SEQUENCE_TASK(siCelebSecurity)
					TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(250, 600))
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(siCelebSecurity)
				TASK_PERFORM_SEQUENCE(CelebSecurity[i].mPed, siCelebSecurity)
				CLEAR_SEQUENCE_TASK(siCelebSecurity)
				SET_PED_COMBAT_ATTRIBUTES(CelebSecurity[i].mPed,CA_DISABLE_SECONDARY_TARGET,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(CelebSecurity[i].mPed, TRUE)
				SET_PED_KEEP_TASK(CelebSecurity[i].mPed,TRUE)
				SET_PED_PATH_PREFER_TO_AVOID_WATER(CelebSecurity[i].mPed, TRUE)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relHatePlayer,RELGROUPHASH_PLAYER)
				eSecurityState[i] = SS_ATTACK_PLAYER
				IF NOT DOES_BLIP_EXIST(CelebSecurity[i].mBlip)
					CelebSecurity[i].mBlip = CREATE_PED_BLIP(CelebSecurity[i].mPed,FALSE,FALSE,BLIPPRIORITY_MED)
				ENDIF
				DEBUG_PRINT_INT("Attack player, security guard: ", i)
			ENDIF
		ENDFOR
		
		IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),CelebrityBuggy.mVehicle)
				IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
					IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						OPEN_SEQUENCE_TASK(siCelebSecurity)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 500)
							TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(250, 500))
							TASK_ENTER_VEHICLE(NULL,CelebrityBuggy.mVehicle,-1,VS_DRIVER)
						CLOSE_SEQUENCE_TASK(siCelebSecurity)
						TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed, siCelebSecurity)
						CLEAR_SEQUENCE_TASK(siCelebSecurity)
						DEBUG_PRINT("Making Glen get into vehicle through MAKE_SECURITY_ATTACK()")
						eGolferState = GS_GET_INTO_VEHICLE
					ENDIF
				ENDIF
			ELSE
				MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
				eGolferState = GS_FLEEING_ON_FOOT
			ENDIF
		ELSE
			MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
		ENDIF
		
		IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
			SET_ENTITY_LOAD_COLLISION_FLAG(CelebrityFriend.mPed,TRUE)
		ENDIF
		MAKE_PED_FLEE(CelebrityFriend.mPed,TRUE)
		
		MAKE_ALL_AMBIENT_GOLFERS_FLEE()
		
		IF eMissionStage <= MS_GET_THE_CLUB
		AND IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
		// Only do this if we're pre-chase, as we want to leave the club attached to the bag otherwise
		// This really only ought to happen during a skip anyway
			//RE-ATTACH GOLF CLUB TO NEW OFFSET SO IT LOOKS OK WHEN HE IS RUNNING
			ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
			
			IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
				//RE-ATTACH GOLF CLUB TO NEW OFFSET SO IT LOOKS OK WHEN HE IS RUNNING
				ATTACH_ENTITY_TO_ENTITY(CelebFriendClub.mObject,CelebrityFriend.mPed,GET_PED_BONE_INDEX(CelebrityFriend.mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
			ENDIF
		ELSE
			IF IS_ENTITY_ATTACHED(golfClubPickup.objIndex)
				DETACH_ENTITY(golfClubPickup.objIndex)
			ENDIF
		ENDIF
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(NI1D_GOLF_CLUB_STOLEN_IN_TIME)
		
		bSecurityAttack = TRUE
		
	ENDIF

	
ENDPROC

/// PURPOSE:
///    Makes all security guards alerted (AIM_AT_PLAYER) and looking at the player if the mission stage is earlier
///    than get the club OR if the player is >80m away
///    Otherwise, make them attack instead
PROC MAKE_SECURITY_ALERT()
	
	IF DOES_ENTITY_EXIST(CelebrityGolfer.mPed)
		IF eMissionStage < MS_GET_THE_CLUB
		OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebrityGolfer.mPed) > 100
			INT i
			
			FOR i = 0 TO NUM_OF_SECURITY - 1
				IF IS_PED_UNINJURED(CelebSecurity[i].mPed)
					SET_ENTITY_LOAD_COLLISION_FLAG(CelebSecurity[i].mPed,TRUE)
					// Make the east celeb security look like he's doing more than the others because of #1093056
					IF i = CELEBSEC_EAST
						IF DOES_ENTITY_EXIST(CelebrityGolfer.mPed)
							TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[i].mPed, CelebrityGolfer.mPed)
							TASK_GO_TO_ENTITY(CelebSecurity[i].mPed, CelebrityGolfer.mPed, DEFAULT, 6.0)
						ENDIF
					ELSE
						TASK_LOOK_AT_ENTITY(CelebSecurity[i].mPed, PLAYER_PED_ID(), -1)
					ENDIF					
					SET_CURRENT_PED_WEAPON(CelebSecurity[i].mPed, WEAPONTYPE_PISTOL, TRUE)
					SET_PED_USING_ACTION_MODE(CelebSecurity[i].mPed, TRUE, -1, "DEFAULT_ACTION")
					DEBUG_PRINT_INT("Alerted, security guard: ", i)
				ENDIF
				eSecurityState[i] = SS_AIM_AT_PLAYER
			ENDFOR
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				IF NOT IsPedPerformingTask(CelebrityGolfer.mPed, SCRIPT_TASK_COWER)
					CLEAR_PED_TASKS(CelebrityGolfer.mPed)
					TASK_COWER(CelebrityGolfer.mPed)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
				CLEAR_PED_TASKS(CelebrityFriend.mPed)
				IF IS_ENTITY_ATTACHED(CelebFriendClub.mObject)
					DETACH_ENTITY(CelebFriendClub.mObject)
				ENDIF
				TASK_SMART_FLEE_PED(CelebrityFriend.mPed, PLAYER_PED_ID(), 300, -1, TRUE)
				SAFE_RELEASE_PED(CelebrityFriend.mPed)
				bFriendIsFleeing = TRUE
			ENDIF
			
			bShouldAttackPlayer = TRUE
			
			// Possibly do other golfer stuff here, anims etc
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(NI1D_GOLF_CLUB_STOLEN_IN_TIME)
			
		ELSE
			// Guards are already aiming or worse, so make them attack if possible
			DEBUG_PRINT("Alert called, but guards need to attack instead")
			MAKE_SECURITY_ATTACK()
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Monitors and adjusts chase speed
PROC Monitor_Golf_Chase_Speed()
	
	IF eMissionStage < MS_COLLECT_GOLF_CLUB
		IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
					IF bHasGoneOppositeWay = FALSE
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,"NIGEL1DGOLF",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
						ENDIF
					ELSE // IF THE GOLF PLAYER HAS GONE OTHER WAY AT START OF CHASE, KEEP CHECKING UNTIL HE HAS FINISH AND PUT HIM BACK ON ORIGINAL ROUTE.
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
							bHasGoneOppositeWay = FALSE //IF HE HAS FINISH ALTERNATIVE ROUTE, SET THIS TO FALSE SO IT PLAYS ORIGINAL
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
ENDPROC

/// PURPOSE:
///    Plays a line of dialogue. Updates each hit. 
PROC PlayHitDialogue()

	IF NOT IS_MESSAGE_BEING_DISPLAYED()
	OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF (GET_GAME_TIMER() - iHitDialogueTimer) >= 6000
				ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
				IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", CONV_PRIORITY_MEDIUM)
					iHitDialogueTimer = GET_GAME_TIMER()
					iScaredDialogueTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks to see if the celebrity buggy has become stuck
/// RETURNS:
///    True - If the buggy has become stuck, false otherwise
FUNC BOOL Is_Buggy_Stuck()

	IF GET_ENTITY_SPEED(CelebrityBuggy.mVehicle) < 0.15
	AND IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
		++iBuggieStuckCounter
		IF iBuggieStuckCounter > STUCK_COUNT
			VEHICLE_WAYPOINT_PLAYBACK_PAUSE(CelebrityBuggy.mVehicle)
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
			AND IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				BRING_VEHICLE_TO_HALT(CelebrityBuggy.mVehicle, 2, 3)
				IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
					CLEAR_PED_TASKS(CelebrityGolfer.mPed)
//					IS_VEHICLE_DRIVEABLE()
//					// Sometimes the celebrity doesn't get out of the buggy when told to here, so he drives off with the clubs still attached!
//					// We can't have that, so let's make absolutely 100% certain he does get out...
//					WHILE GET_SCRIPT_TASK_STATUS(CelebrityGolfer.mPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
//						IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
//							TASK_LEAVE_ANY_VEHICLE(CelebrityGolfer.mPed, 0, ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
//						ENDIF
//						WAIT(0)
//					ENDWHILE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
					PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_GIVEUP", "NIG1D_GIVEUP_1", CONV_PRIORITY_MEDIUM)
					DEBUG_PRINT("Celeb vehicle stuck")
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		iBuggieStuckCounter = 0
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Monitors the golf course chase
/// RETURNS:
///    Returns true:
///    If the golf buggy gets wrecked.
///    If golf buggy gets stuck.
///    If Celebrity gets out of buggy.
///    If buggy health gets below DROP_CLUB_HEALTH.
///    FALSE otherwise.
FUNC BOOL Monitor_GolfCourse_Chase()
	
	IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
		
		IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			//Update chase hint cam
			CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, CelebrityBuggy.mVehicle)
			
			IF Is_Buggy_Stuck()
			OR IS_VEHICLE_TYRE_BURST(CelebrityBuggy.mVehicle, SC_WHEEL_CAR_FRONT_LEFT)
			OR IS_VEHICLE_TYRE_BURST(CelebrityBuggy.mVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
						REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF")
						REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF2")
					ENDIF
				ENDIF
				CLEAR_PED_TASKS(CelebrityGolfer.mPed)
				MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
				DEBUG_PRINT("*** Celeb cart stuck or a front wheel is burst - chase monitor returning True")
				RETURN TRUE
			ELSE
				Monitor_Golf_Chase_Speed()
			ENDIF
			
			IF GET_ENTITY_HEALTH(CelebrityBuggy.mVehicle) < DROP_CLUB_HEALTH
				bDropGolfBag = TRUE
				DEBUG_PRINT("*** Celeb cart below drop club threshold - chase monitor returning True")
				RETURN TRUE
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
				DEBUG_PRINT("*** Celeb no longer in vehicle - chase monitor returning True")
				RETURN TRUE
			ENDIF
			
				//DEALS WITH PLAYING DIALOGUE FOR DAMAGE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CelebrityBuggy.mVehicle,PLAYER_PED_ID(),TRUE)
			AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(CelebrityBuggy.mVehicle,WEAPONTYPE_INVALID,GENERALWEAPON_TYPE_ANYWEAPON)
				PlayHitDialogue()
				DEBUG_PRINT_INT("VEHICLE HEALTH IS ",GET_ENTITY_HEALTH(CelebrityBuggy.mVehicle) )
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(CelebrityBuggy.mVehicle)
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CelebrityBuggy.mVehicle,PLAYER_PED_ID())
					DEBUG_PRINT_INT("VEHICLE HEALTH IS ",GET_ENTITY_HEALTH(CelebrityBuggy.mVehicle) )
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(CelebrityBuggy.mVehicle)
				ENDIF
			ENDIF
		
		ELSE
			DEBUG_PRINT("Celeb dead - returning true")
			RETURN TRUE
		ENDIF
		
	ELSE // The celeb's buggy has wrecked/destroyed before any of the above checks returned true - end the chase early
		
		DEBUG_PRINT("*** Celeb cart wrecked/not OK - chase monitor returning True")
		MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Monitors and adjusts chase speed for in the city
PROC Monitor_City_Chase_Speed()
	
	IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<SLOW_DOWN_RANGE,SLOW_DOWN_RANGE,SLOW_DOWN_RANGE>>)
			SET_DRIVE_TASK_MAX_CRUISE_SPEED(CelebrityGolfer.mPed,0.3)
		ELSE
			SET_DRIVE_TASK_MAX_CRUISE_SPEED(CelebrityGolfer.mPed,0)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Monitors the chase through the city
/// RETURNS:
///    TRUE:
///    If golf buggie is wrecked.
///    If golf buggy is stuck.
///    If golf buggy health is below DROP_CLUB_HEALTH - 100.
///    If celebrity is not in the vehicle. 
///    FLASE - otherwise.
FUNC BOOL Monitor_City_Chase()

	IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
		IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			//Update chase hint cam
			CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, CelebrityBuggy.mVehicle)
			
			//Monitor chase speed
			IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
				Monitor_City_Chase_Speed()
			ENDIF
			
			IF Is_Buggy_Stuck()
			OR IS_VEHICLE_TYRE_BURST(CelebrityBuggy.mVehicle, SC_WHEEL_CAR_FRONT_LEFT)
			OR IS_VEHICLE_TYRE_BURST(CelebrityBuggy.mVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
				DEBUG_PRINT("Buggy stuck or front tyres burst in city - returning true")
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
					REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF")
					REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF2")
				ENDIF
				CLEAR_PED_TASKS(CelebrityGolfer.mPed)
				MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
				RETURN TRUE
			ENDIF
			
			IF GET_ENTITY_HEALTH(CelebrityBuggy.mVehicle) < (DROP_CLUB_HEALTH - 100)
				DEBUG_PRINT("Buggy health below threshold - returning true")
				bDropGolfBag = TRUE
				RETURN TRUE
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
				DEBUG_PRINT("Celeb not in buggy while monitoring chase - returning true")
				RETURN TRUE
			ENDIF
		ELSE
			DEBUG_PRINT("Celeb dead - returning true")
			RETURN TRUE
		ENDIF
	ELSE
		DEBUG_PRINT("Buggy somehow not ok - returning true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles Celebrity chase colour dialogue - Tries to plays a conversation until all have been said
PROC CelebrityChaseDialogue()
	
	// Only do this if we're still chasing the guy
	IF eMissionStage = MS_CHASE_THE_GOLFER
	OR eMissionStage =  MS_CHASE_IN_CITY
		IF iChaseConvoCounter < 5
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF (GET_GAME_TIMER() - iChaseDialogueTimer) > 15000
						//DEBUG_PRINT_INT("Timer is now: ", (GET_GAME_TIMER() - iChaseDialogueTimer))
						IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
							IF GET_DISTANCE_BETWEEN_PEDS(CelebrityGolfer.mPed, PLAYER_PED_ID()) < 40
								ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
								ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID() , "TREVOR")
								SWITCH iChaseConvoCounter
									CASE 0
										IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_CHASE1", CONV_PRIORITY_HIGH)
											DEBUG_PRINT("Chase conv 1 started...")
											iChaseDialogueTimer = GET_GAME_TIMER()
											iChaseConvoCounter++
										ENDIF
									BREAK
									CASE 1
										IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_CHASE2", CONV_PRIORITY_HIGH)
											DEBUG_PRINT("Chase conv 2 started...")
											iChaseDialogueTimer = GET_GAME_TIMER()
											iChaseConvoCounter++
										ENDIF
									BREAK
									CASE 2
										IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_CHASE3", CONV_PRIORITY_HIGH)
											DEBUG_PRINT("Chase conv 3 started...")
											iChaseDialogueTimer = GET_GAME_TIMER()
											iChaseConvoCounter++
										ENDIF
									BREAK
									// Only do the last two if we reach the city chase
									CASE 3
										IF eMissionStage =  MS_CHASE_IN_CITY
											IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_CHASE4", CONV_PRIORITY_HIGH)
												DEBUG_PRINT("Chase conv 4 started...")
												iChaseDialogueTimer = GET_GAME_TIMER()
												iChaseConvoCounter++
											ENDIF
										ENDIF
									BREAK
									CASE 4
										IF eMissionStage =  MS_CHASE_IN_CITY
											IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_CHASE5", CONV_PRIORITY_HIGH)
												DEBUG_PRINT("Chase conv 5 started...")
												iChaseDialogueTimer = GET_GAME_TIMER()
												iChaseConvoCounter++
											ENDIF
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
/// Handles Celebrity scared dialogue - Will play if bullet is in area around celebrity.
PROC CelebrityScaredDialogue()

	IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(CelebrityGolfer.mPed),20.0)
		IF NOT IS_MESSAGE_BEING_DISPLAYED()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF (GET_GAME_TIMER() - iScaredDialogueTimer) >= 3000
					SWITCH iScaredConvoCounter
					
						CASE 0 
							
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", "NIG1D_HIT_7", CONV_PRIORITY_MEDIUM)		
						
						BREAK
						
						CASE 1
							
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", "NIG1D_HIT_8", CONV_PRIORITY_MEDIUM)		
						
						BREAK
						
						CASE 2
							
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", "NIG1D_HIT_9", CONV_PRIORITY_MEDIUM)		
						
						BREAK
						
						CASE 3 
							
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", "NIG1D_HIT_10", CONV_PRIORITY_MEDIUM)		
						
						BREAK
					
					
					ENDSWITCH
					iScaredConvoCounter++
					iHitDialogueTimer = GET_GAME_TIMER()
					iScaredDialogueTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///  Checks if player has failed for not catching up with Celebrity
PROC DISTANCE_FAIL_CHECKS()
		
		
		//USED FOR RETURN TO CLUB CHECKS - DISTANCE_FAIL_CHECKS()
		IF bPlayerHasBeenClose = FALSE
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex) AND eMissionStage = MS_COLLECT_GOLF_CLUB
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), golfClubPickup.objIndex, << 25.0, 25.0, 25.0 >>)
					bPlayerHasBeenClose = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF bWarnedAboutLeaving = FALSE
				IF eGolferState = GS_CRASHED OR eMissionStage = MS_COLLECT_GOLF_CLUB
					IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
						IF bPlayerHasBeenClose = TRUE // IF PLAYER HAS BEEN CLOSE TO CLUB WE CAN TELL HIM TO RETURN TO IT
							IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),golfClubPickup.objIndex,<<LOSING_RANGE,LOSING_RANGE,LOSING_RANGE>>)
								PRINT_NOW("N1D_RETPICKUP",DEFAULT_GOD_TEXT_TIME,5)
								bWarnedAboutLeaving = TRUE
							ENDIF
						ENDIF
					ENDIF
				// Don't need to warn the player about getting too far away from Stanky via God Text anymore
//				ELSE
//					IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
//						IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<LOSING_RANGE,LOSING_RANGE,LOSING_RANGE>>)
//						AND NOT IS_ENTITY_AT_COORD(CelebrityGolfer.mPed,CelebrityGolfer.vStartPos,<<20.0,20.0,20.0>>)
//							PRINT_NOW("N1D_DONTLOSE",DEFAULT_GOD_TEXT_TIME,5)
//							bWarnedAboutLeaving = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ELSE 
				IF eGolferState = GS_CRASHED OR eMissionStage = MS_COLLECT_GOLF_CLUB
					IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
						IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),golfClubPickup.objIndex,<<LOST_RANGE,LOST_RANGE,LOST_RANGE>>)
							SET_FAIL_REASON(FAIL_LOST_GOLF_CLUB)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF eMissionStage < MS_COLLECT_GOLF_CLUB // Only do this fail check while the chase is ongoing - don't care about Glen if the golf club is around
				IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<LOST_RANGE,LOST_RANGE,LOST_RANGE>>)
					AND NOT CAN_PED_SEE_PED(PLAYER_PED_ID(), CelebrityGolfer.mPed)
						SET_FAIL_REASON(FAIL_LION_ESCAPED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
ENDPROC

PROC HANDLE_CELEB_SWING_DIALOGUE()

	SWITCH eSwingSpeech
		CASE SWING_TRIGGER
			IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CelebrityGolfer.mPed, 35.0)
				AND eSecurityState[CELEBSEC_WEST] != SS_STANDDOWN
				AND eSecurityState[CELEBSEC_EAST] != SS_STANDDOWN
				AND eSecurityState[CELEBSEC_NORTH] != SS_STANDDOWN // Don't play the dialogue if security are going into standdown because of the outfit
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
						IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SWING", CONV_PRIORITY_HIGH)
							eSwingSpeech = SWING_SPEAKING
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SWING_SPEAKING
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iFostLineTimer = GET_GAME_TIMER()
				eSwingSpeech = SWING_WAIT
			ENDIF
		BREAK
		CASE SWING_WAIT
			IF (GET_GAME_TIMER() - iFostLineTimer) > 4500
				eSwingSpeech = SWING_TRIGGER
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
/// Puts course security back in initial state so they can react to golf buggy chase
/// Also sets bHelpTextDisplayed = TRUE - Stops help text displaying.
PROC RESET_COURSE_SECURITY()
	
	DEBUG_PRINT("RESET COURSE SECURITY")
	
	bHelpTextDisplayed = TRUE
	
	INT i
	
	FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
		IF IS_ENTITY_ALIVE(CourseSecurity[i].mPed)
			eCourseSecurity[i] = CS_WAIT_AT_LOCATION
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE:
///  Deals with the celebrities behaviours
PROC Monitor_Celebrity()
	

	IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
		SWITCH eGolferState
			
			CASE GS_WAIT_FOR_PLAYER
			
				HANDLE_CELEB_SWING_DIALOGUE() // Handle the dialogue
				
				//CELEB THINKS ITS DEBT COLLECTORS AND RUNS
				IF HAS_PLAYER_THREATENED_PED(CelebrityGolfer.mPed)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					
//						ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
//						ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
//						PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_FLEE", "NIG1D_FLEE_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
						
						//RE-ATTACH GOLF CLUB TO NEW OFFSET SO IT LOOKS OK WHEN HE IS RUNNING
						ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
						
						IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
						 	IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
								IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
									TASK_ENTER_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,-1,VS_DRIVER)
								ENDIF
							ENDIF
						ENDIF
						
						DEBUG_PRINT("GS_WAIT_FOR_PLAYER, player threatened Glen; attacking...")
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[CELEBSEC_WEST].mPed, "Nigel1DCelebSecurity")
						IF IS_PLAYER_USING_SNIPER_RIFLE()
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_6", CONV_PRIORITY_MEDIUM)
						ELSE
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_5", CONV_PRIORITY_MEDIUM)
						ENDIF
						
						MAKE_SECURITY_ATTACK()

						eGolferState = GS_GET_INTO_VEHICLE
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() // Kill the conversation ASAP, everyone needs to react
					ENDIF
				ELSE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
						IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF) // CHECK PLAYERS OUTFIT - IS IT GOLFERS?
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_FLEE", "NIG1D_FLEE_1", CONV_PRIORITY_MEDIUM)
							
							IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
							 	IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
									IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
										TASK_ENTER_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,-1,VS_DRIVER)
									ENDIF
								ENDIF
							ENDIF
							
							DEBUG_PRINT("GS_WAIT_FOR_PLAYER, player got too close to Glen (alert_dis); attacking...")
							MAKE_SECURITY_ATTACK()
							
							//CHANGES GOLF CLUB ORIENTATIONS TO LOOK CORRECT IN HAND
								ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
							//
							
							eGolferState = GS_GET_INTO_VEHICLE
						ELSE
							iCelebWarnTimer = GET_GAME_TIMER()
							iFostLineTimer = GET_GAME_TIMER()
							eGolferState = GS_PLAYER_IN_OUTFIT
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			
			CASE GS_PLAYER_IN_OUTFIT
			
				IF HAS_PLAYER_THREATENED_PED(CelebrityGolfer.mPed)
				OR bPlayerReturned = TRUE
					DEBUG_PRINT("GS_PLAYER_IN_OUTFIT, player threatened Glen or returned a second time; attacking...")
					MAKE_SECURITY_ATTACK()
					
				ELSE
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
						ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
						OPEN_SEQUENCE_TASK(siCelebrity)
							TASK_ACHIEVE_HEADING(NULL, 112.0)
							TASK_PLAY_ANIM(NULL,"rcmnigel1d","swing_a_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
							TASK_PLAY_ANIM(NULL,"rcmnigel1d","swing_b_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
							SET_SEQUENCE_TO_REPEAT(siCelebrity,REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(siCelebrity)
						TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed, siCelebrity)
						CLEAR_SEQUENCE_TASK(siCelebrity)
						IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
							TASK_CLEAR_LOOK_AT(CelebrityFriend.mPed)
						ENDIF
						bPlayerReturned = TRUE
						int i
						FOR i = 0 TO NUM_OF_SECURITY - 1
							eSecurityState[i] = SS_GUARD_AREA
						ENDFOR
						eGolferState = GS_WAIT_FOR_PLAYER
					ELSE
						// Check for bumping
						IF (GET_GAME_TIMER() - iFostLineTimer) >= 2500
							IF IS_ENTITY_TOUCHING_ENTITY(CelebrityGolfer.mPed, PLAYER_PED_ID())
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
									ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
									IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_BUMPF", CONV_PRIORITY_MEDIUM)
										iFostLineTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					AND IS_VEHICLE_OK(GolfBuggieTransport[BUGGY_NEXTTOCELEB].mVehicle)
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),CelebrityBuggy.mVehicle)
						OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(),GolfBuggieTransport[BUGGY_NEXTTOCELEB].mVehicle)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_ANY_CONVERSATION()
							ENDIF
							iSecurityWarnTimer = GET_GAME_TIMER() //SET TIMER, USED INSIDE SECURITY STATE
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_BANK", "NIG1D_BANK_1", CONV_PRIORITY_MEDIUM)
							OPEN_SEQUENCE_TASK(siCelebrity)
								TASK_PLAY_ANIM(NULL,"rcmnigel1d","thanks_male_03",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
							CLOSE_SEQUENCE_TASK(siCelebrity)
							TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed,siCelebrity)
							CLEAR_SEQUENCE_TASK(siCelebrity)
							eGolferState = GS_ANGRY_AT_PLAYER
						ENDIF
					ENDIF
					IF (GET_GAME_TIMER() - iCelebWarnTimer) >= 15000 //PLAYER HAS BEEN IN AREA TOO LONG, CELEB GETS PISSED OFF
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
							ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_BANK", "NIG1D_BANK_2", CONV_PRIORITY_MEDIUM)
							OPEN_SEQUENCE_TASK(siCelebrity)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
							CLOSE_SEQUENCE_TASK(siCelebrity)
							TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed,siCelebrity)
							CLEAR_SEQUENCE_TASK(siCelebrity)
							IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
								ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
							ENDIF
							iSecurityWarnTimer = GET_GAME_TIMER() //SET TIMER, USED INSIDE SECURITY STATE
							eSecurityState[CELEBSEC_WEST] = SS_GO_TO_PLAYER //MAKES SECURITY NEAR BUGGY REACT
							eGolferState = GS_ANGRY_AT_PLAYER
						ENDIF
					ENDIF
				ENDIF
			
			BREAK
			
			CASE GS_ANGRY_AT_PLAYER
				IF HAS_PLAYER_THREATENED_PED(CelebrityGolfer.mPed)
					DEBUG_PRINT("GS_ANGRY_AT_PLAYER, player threatened Glen; attacking...")
					MAKE_SECURITY_ATTACK()
				
				ENDIF
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
					ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
					OPEN_SEQUENCE_TASK(siCelebrity)
						TASK_TURN_PED_TO_FACE_COORD(NULL, <<-1094.57, 60.98, 52.67>>)
						TASK_PLAY_ANIM(NULL,"mini@golf","iron_idle_high_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
						TASK_PLAY_ANIM(NULL,"mini@golf","iron_swing_intro_high",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
						TASK_PLAY_ANIM(NULL,"mini@golf","iron_swing_action_high",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
						TASK_PLAY_ANIM(NULL,"mini@golf","swing_outro",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
						TASK_PLAY_ANIM(NULL,"mini@golf","swing_react_bad_02",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
						SET_SEQUENCE_TO_REPEAT(siCelebrity,REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(siCelebrity)
					TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed, siCelebrity)
					CLEAR_SEQUENCE_TASK(siCelebrity)
					IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
						TASK_CLEAR_LOOK_AT(CelebrityFriend.mPed)
					ENDIF
					bPlayerReturned = TRUE
					int i
					FOR i = 0 TO NUM_OF_SECURITY - 1
						eSecurityState[i] = SS_GUARD_AREA
					ENDFOR
					eGolferState = GS_WAIT_FOR_PLAYER
				ELSE
					// Check for bumping
					IF (GET_GAME_TIMER() - iFostLineTimer) >= 2500
						IF IS_ENTITY_TOUCHING_ENTITY(CelebrityGolfer.mPed, PLAYER_PED_ID())
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
								ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
								IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_BUMPF", CONV_PRIORITY_MEDIUM)
									iFostLineTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GS_GET_INTO_VEHICLE
			
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				AND GET_DISTANCE_BETWEEN_ENTITIES(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle) < 10
				AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),CelebrityBuggy.mVehicle)
				AND NOT IS_ENTITY_UPSIDEDOWN(CelebrityBuggy.mVehicle)
					IF IS_PED_SITTING_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
						ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
						PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_FLEE", "NIG1D_FLEE_3", CONV_PRIORITY_MEDIUM)
						IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
							IF NOT IS_PLAYER_AT_LEFT_SIDE()
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,"NIGEL1DGOLF",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT|EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
							ELSE
								bHasGoneOppositeWay = TRUE
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,"NIGEL1DGOLF2",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT|EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
							ENDIF
						ENDIF
						eGolferState = GS_FLEE_FROM_PLAYER_IN_BUGGY
					ENDIF
				ELSE
					MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
					eGolferState = GS_FLEEING_ON_FOOT
				ENDIF
				RESET_COURSE_SECURITY()
			BREAK
			
			CASE GS_FLEE_FROM_PLAYER_IN_BUGGY //STAYS IN HERE WHILE FLEEING FROM PLAYER
				
				CelebrityChaseDialogue()
				CelebrityScaredDialogue()
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						IF eMissionStage <>  MS_CHASE_THE_GOLFER //NEED TO MAKE SURE CELEBRITY CONTINUES TO FLEE IF OUTSIDE THIS STATE
							IF GET_VEHICLE_WAYPOINT_PROGRESS(CelebrityBuggy.mVehicle) >= 200
								MAKE_PED_FLEE(CelebrityGolfer.mPed)
								eGolferState = GS_FLEE_IN_CITY
							ENDIF
							IF Is_Buggy_Stuck()
								eGolferState = GS_CRASHED
							ENDIF
						ENDIF
						//FLEEING
					ELSE
						eGolferState = GS_CRASHED
					ENDIF
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),CelebrityBuggy.mVehicle, TRUE)
						eGolferState = GS_CRASHED
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GS_CRASH_INTO_LAKE
			
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						eGolferState = GS_CRASHED
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GS_FLEE_IN_CITY
				
				CelebrityChaseDialogue()
				CelebrityScaredDialogue()
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
					OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(),CelebrityBuggy.mVehicle, TRUE)
						eGolferState = GS_CRASHED
					ENDIF
				ENDIF
				
			BREAK
			
			CASE GS_CRASHED
				//CELEB HAS CRASHED AND IS ON FOOT - CREATE PICKUP
				DEBUG_PRINT("Celeb crashed or forced out - quitting chase")
				IF eMissionStage < MS_COLLECT_GOLF_CLUB // Only set the stage to collect the golf club if we haven't passed it yet
					PRINT_NOW("N1D_COLLECT",DEFAULT_GOD_TEXT_TIME,5) // Collect the ~g~golf club. //
					SetStage(MS_COLLECT_GOLF_CLUB)
				ENDIF
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
						REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF")
						REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF2")
					ENDIF
				ENDIF
				MAKE_PED_FLEE(CelebrityGolfer.mPed,TRUE)
				eGolferState = GS_FLEEING_ON_FOOT
			BREAK
			
			CASE GS_FLEEING_ON_FOOT
				//CELEB IS FLEEING ON FOOT
			BREAK
			
		ENDSWITCH
	
	ELSE
		IF NOT bReplayCelebDeadCheck
			DEBUG_PRINT("bReplayCelebDeadCheck tripped, recording last 2 seconds")
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 2.0, REPLAY_IMPORTANCE_LOWEST) // Get the last two seconds of celeb's life to capture death
			bReplayCelebDeadCheck = TRUE
		ENDIF
		IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLD_KILLED_MARK))
			SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLD_KILLED_MARK))
			CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLD_KILLED_MARK) set")	
		ENDIF
	ENDIF
		
ENDPROC



/// PURPOSE:
///    Makes sure concierge models have loded
/// PARAMS:
///    bWaitForLoad - If true, will wait for all models to be loaded
/// RETURNS:
///    TRUE if all mdoels loaded, false otherwise. 
FUNC BOOL HAS_CONCIERGE_SCENE_LOADED(BOOL bWaitForLoad = FALSE)
	
	INT i
	
	FOR i = 0 TO NUM_OF_CONC_PEDS - 1
		
		REQUEST_MODEL(ConciergePed[i].mModel)
		
	ENDFOR
	
	FOR i = 0 TO NUM_OF_TRANS_BUGGIES - 1
	
		REQUEST_MODEL(GolfBuggieTransport[i].mModel)
		
	ENDFOR
	
	IF bWaitForLoad = FALSE
         // HAVE ALL PED CONCIEGRES LOADED
        FOR i = 0 TO NUM_OF_CONC_PEDS - 1
            IF NOT HAS_MODEL_LOADED(ConciergePed[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		 // HAVE ALL BUGGIES LOADED
		FOR i = 0 TO NUM_OF_TRANS_BUGGIES - 1
           	IF NOT HAS_MODEL_LOADED(GolfBuggieTransport[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DTRANS")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF2")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DCRASH")
			RETURN FALSE
		ENDIF 

	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED CONCIEGRES LOADED
            FOR i = 0 TO NUM_OF_CONC_PEDS - 1
                IF NOT HAS_MODEL_LOADED(ConciergePed[i].mModel)
                    bEverythingLoaded = FALSE
                ENDIF
            ENDFOR
			
			 // HAVE ALL BUGGIES LOADED
            FOR i = 0 TO NUM_OF_TRANS_BUGGIES - 1
                IF NOT HAS_MODEL_LOADED(GolfBuggieTransport[i].mModel)
                    bEverythingLoaded = FALSE
                ENDIF
            ENDFOR
			
			IF NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DTRANS")
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF")
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF2")
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DCRASH")
				 bEverythingLoaded = FALSE
			ENDIF
	

        ENDWHILE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///  Makes all consierge peds flee the player
PROC CONSIERGE_FLEE_PLAYER()
	
	INT i
	
	FOR i = 0 TO NUM_OF_CONC_PEDS - 1
		MAKE_PED_FLEE(ConciergePed[i].mPed,TRUE)
	ENDFOR
	

ENDPROC


/// PURPOSE:
/// Monitors individual conseirge peds.
/// Checks if player has threatend them or shot in area. 
PROC Monitor_Consierge(INT iPedNum)

	SWITCH eConsiergeState[iPedNum]
	
		CASE CS_STAND_AND_WAIT
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ConciergePed[iPedNum].mPed, <<6,6,6>>, FALSE, TRUE, TM_ON_FOOT)
				IF GET_SCRIPT_TASK_STATUS(ConciergePed[iPedNum].mPed, script_task_look_at_entity) != PERFORMING_TASK
					TASK_LOOK_AT_ENTITY(ConciergePed[iPedNum].mPed, PLAYER_PED_ID(), 500, SLF_EXTEND_PITCH_LIMIT|SLF_EXTEND_YAW_LIMIT)
				ENDIF
			ENDIF
			IF HAS_PLAYER_THREATENED_PED(ConciergePed[iPedNum].mPed)
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF iPedNum = 0
							ADD_PED_FOR_DIALOGUE(sDialogue, 7, ConciergePed[iPedNum].mPed , "CONCIERGE")
							ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_FLEE", "NIG1D_FLEE_9", CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
				DEACTIVATE_OUTFIT_CHANGE()
				CONSIERGE_FLEE_PLAYER()
				eConsiergeState[iPedNum] = CS_FLEEING_PLAYER
			ELIF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(ConciergePed[iPedNum].mPed),30.0)
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF iPedNum = 0
							ADD_PED_FOR_DIALOGUE(sDialogue, 7, ConciergePed[iPedNum].mPed , "CONCIERGE")
							ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
							PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_FLEE", "NIG1D_FLEE_7", CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
				DEACTIVATE_OUTFIT_CHANGE()
				CONSIERGE_FLEE_PLAYER()
				eConsiergeState[iPedNum] = CS_FLEEING_PLAYER
			ENDIF
		BREAK
		
		CASE CS_FLEEING_PLAYER
		BREAK
		
	ENDSWITCH

		
ENDPROC

/// PURPOSE:
///   Monitors the consierge scene, player will fail if he attacks them before finding golfer.
PROC Monitor_Consierge_Scene()

	INT i
	
	FOR i = 0 TO NUM_OF_CONC_PEDS - 1
		IF IS_ENTITY_ALIVE(ConciergePed[i].mPed)
			Monitor_Consierge(i)
		ENDIF
	ENDFOR


ENDPROC

/// PURPOSE:
///  Handles concierge scene - Loads models, checks they are loaded.
///  Also checks if everything is ok once created.
PROC HANDLE_CONCIERGE_SCENE()

	INT i
  
  	IF bConciergeSceneCreated = FALSE
  
   		IF HAS_CONCIERGE_SCENE_LOADED()
		
			DEBUG_PRINT("Concierge scene loaded, creating now...")
		
	      	// CREATE WORKERS
	      	FOR i = 0 TO NUM_OF_CONC_PEDS -1
	            CreateMissionPed(ConciergePed[i],FALSE)
	            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ConciergePed[i].mPed,TRUE) 
	      	ENDFOR
	      
	      	//CREATE BUGGY
//	     	FOR i = 0 TO NUM_OF_TRANS_BUGGIES -1
//	       		CreateMissionVehicle(GolfBuggieTransport[i],FALSE)
//				SET_VEHICLE_MODEL_IS_SUPPRESSED(GolfBuggieTransport[i].mModel,TRUE)
//			ENDFOR
			
			//PUT A PED IN THE VEHICLE
			CreateMissionVehicle(GolfBuggieTransport[0],FALSE)
			SET_PED_INTO_VEHICLE(ConciergePed[1].mPed,GolfBuggieTransport[0].mVehicle,VS_DRIVER)
			SET_PED_CAN_BE_DRAGGED_OUT(ConciergePed[1].mPed,FALSE)

	      	bConciergeSceneCreated = TRUE
			
			//Unload PED models
			FOR i = 0 TO NUM_OF_CONC_PEDS -1
	            SET_MODEL_AS_NO_LONGER_NEEDED(ConciergePed[i].mModel)
	      	ENDFOR
			
			//Unload VEHICLE models
			FOR i = 0 TO NUM_OF_TRANS_BUGGIES -1
	       		SET_MODEL_AS_NO_LONGER_NEEDED(GolfBuggieTransport[i].mModel)
			ENDFOR

    	ENDIF
  	ELSE
  		Monitor_Consierge_Scene()
  	ENDIF
	
ENDPROC

/// PURPOSE:
///    Requests the models for the Celeb security.
/// PARAMS:
///    bWaitForLoad - Will force it to wait until the models have been loaded.
/// RETURNS:
///    True - If all models have loaded, false otherwise. 
FUNC BOOL HAS_SECURITY_LOADED(BOOL bWaitForLoad = FALSE)


	INT i
	
	FOR i = 0 TO NUM_OF_SECURITY - 1
	
		REQUEST_MODEL(CelebSecurity[i].mModel)
		
	ENDFOR
	

	
	IF bWaitForLoad = FALSE
     
	 
        FOR i = 0 TO NUM_OF_SECURITY - 1
            IF NOT HAS_MODEL_LOADED(CelebSecurity[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR

	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED CONCIEGRES LOADED
            FOR i = 0 TO NUM_OF_SECURITY - 1
            	IF NOT HAS_MODEL_LOADED(CelebSecurity[i].mModel)
                	bEverythingLoaded = FALSE
           		ENDIF
       	 	ENDFOR
	

        ENDWHILE
	ENDIF
	
	RETURN TRUE


ENDFUNC

/// PURPOSE:
///    Checks if player is near security - Used when trying to escape them
/// RETURNS:
///    True - If player is away from security, false otherwise. 
FUNC BOOL IS_PLAYER_NEAR_SECURITY()

	INT i
	
	FOR i = 0 TO  NUM_OF_SECURITY -1
		IF IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[i].mPed,<<70.0,70.0,70.0>>)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Checks if all the security are dead - Used when trying to escape them
/// RETURNS:
///    True - If all security are dead, false otherwise
FUNC BOOL ARE_GUARDS_DEAD()

	INT i
	
	FOR i = 0 TO  NUM_OF_SECURITY -1
		IF CelebSecurity[i].bImDead = FALSE
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Continually check the conditions for this mission's FOUR! stat and update if needed
PROC CHECK_STATS_GOALS()

	IF bCheckStatFour
		IF NOT IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
		AND ARE_GUARDS_DEAD()
			DEBUG_PRINT("Player achieved FOUR!")
			bCheckStatFour = FALSE
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1D_KILLED_STANKY_AND_GUARDS)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///  Controls the behaviours of each Celeb security ped
/// PARAMS:
///    iPedNum - The ped we want to check
PROC Monitor_Security_Ped(INT iPedNum)

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(CelebSecurity[iPedNum].mPed)
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebSecurity[iPedNum].mPed) < 20
			IF NOT IS_PED_HEADTRACKING_PED(CelebSecurity[iPedNum].mPed, PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(CelebSecurity[iPedNum].mPed, PLAYER_PED_ID(), 1000)
			ENDIF
		ENDIF
	ENDIF

	SWITCH eSecurityState[iPedNum]
	
		CASE SS_GUARD_AREA
		
			IF HAS_PLAYER_THREATENED_PED(CelebSecurity[iPedNum].mPed)
			OR HAS_PED_RECEIVED_EVENT(CelebSecurity[iPedNum].mPed, EVENT_POTENTIAL_GET_RUN_OVER)
				IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed, 110)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF IS_PLAYER_USING_SNIPER_RIFLE() //CHECK IF PLAYER IS TRYING TO SNIPE
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
						PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_6", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
					DEBUG_PRINT("SS_GUARD_AREA, player threatened Celeb security; attacking...")
					MAKE_SECURITY_ATTACK()
					
				ENDIF
			ELIF IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				IF IS_ENTITY_ON_SCREEN(CelebSecurity[iPedNum].mPed)
				AND CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
				AND GET_DISTANCE_BETWEEN_ENTITIES(CelebSecurity[iPedNum].mPed, PLAYER_PED_ID()) < 50
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
					PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_6", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
					DEBUG_PRINT("SS_GUARD_AREA, player tried sniping Celeb security but was spotted; attacking...")
					MAKE_SECURITY_ATTACK()
				ENDIF
			ELIF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),CelebSecurity[iPedNum].mPed)
				IF IS_PLAYER_USING_SNIPER_RIFLE() //CHECK IF PLAYER IS TRYING TO SNIPE
					IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_6", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							DEBUG_PRINT("SS_GUARD_AREA, player aimed Sniper Rifle when security could see them; attacking...")
							MAKE_SECURITY_ATTACK()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //PLAYER IS NOT IN A VEHICLE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<25.0,25.0,25.0>>)
					 	IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF)// CHECK PLAYERS OUTFIT - IF NOT DO BELOW
							IF NOT IS_PLAYER_UNARMED()
								IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
								AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<18.0,18.0,18.0>>)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_5", CONV_PRIORITY_MEDIUM)
										DEBUG_PRINT("SS_GUARD_AREA, player armed, not wearing outfit, guard has LOS or player within 18m; attacking...")
										MAKE_SECURITY_ATTACK()	
									ENDIF
								ENDIF
							ELSE
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<18.0,18.0,18.0>>)
									IF bPlayerApproached = FALSE //ONLY WANT THIS EVENT TO HAPPEN ONCE SO NEED TO SET BOOL
										ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_1", CONV_PRIORITY_MEDIUM)
												OPEN_SEQUENCE_TASK(siCelebSecurity)
													TASK_PLAY_ANIM(NULL,"rcmnigel1d","thanks_male_03",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
													TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
												CLOSE_SEQUENCE_TASK(siCelebSecurity)
												TASK_PERFORM_SEQUENCE(CelebSecurity[iPedNum].mPed,siCelebSecurity)
												CLEAR_SEQUENCE_TASK(siCelebSecurity)
												iSecurityWarnTimer = GET_GAME_TIMER()
												bPlayerApproached = TRUE
												eSecurityState[iPedNum] = SS_GO_TO_PLAYER
											ENDIF
										ELSE // Play the line without subtitles if we must so the guard state progresses
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
												OPEN_SEQUENCE_TASK(siCelebSecurity)
													TASK_PLAY_ANIM(NULL,"rcmnigel1d","thanks_male_03",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
													TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
												CLOSE_SEQUENCE_TASK(siCelebSecurity)
												TASK_PERFORM_SEQUENCE(CelebSecurity[iPedNum].mPed,siCelebSecurity)
												CLEAR_SEQUENCE_TASK(siCelebSecurity)
												iSecurityWarnTimer = GET_GAME_TIMER()
												bPlayerApproached = TRUE
												eSecurityState[iPedNum] = SS_GO_TO_PLAYER
											ENDIF
										ENDIF
									ELSE
										IF bShouldAttackPlayer = TRUE
											IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<12.0,12.0,12.0>>)
												IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												ENDIF
												ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
													DEBUG_PRINT("SS_GUARD_AREA, non-outfit, player came back to area after being warned; attacking")
													MAKE_SECURITY_ATTACK()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE //PLAYER IS IN A GOLFERS OUTFIT - DO THIS STUFF
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<18.0,18.0,18.0>>)
								IF bPlayerApproached = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND bConversationActive = FALSE
										IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
											TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
										ENDIF
										IF IS_PED_UNINJURED(CelebrityFriend.mPed)
											TASK_LOOK_AT_ENTITY(CelebrityFriend.mPed, PLAYER_PED_ID(), -1)
										ENDIF
										IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
											TASK_LOOK_AT_ENTITY(CelebrityGolfer.mPed, PLAYER_PED_ID(), -1)
											OPEN_SEQUENCE_TASK(siCelebrity)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(), 1500)
												TASK_PLAY_ANIM(NULL, "rcmnigel1d", "idle_c", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
											CLOSE_SEQUENCE_TASK(siCelebrity)
											TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed, siCelebrity)
											CLEAR_SEQUENCE_TASK(siCelebrity)
//											IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
//												ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.0,0.0,0.0>>,<<0,0,0>>)
//											ENDIF
										ENDIF
										ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
										ADD_PED_FOR_DIALOGUE(sDialogue, 2,PLAYER_PED_ID(), "TREVOR")
										ADD_PED_FOR_DIALOGUE(sDialogue, 6,CelebrityGolfer.mPed, "FOSTENBURG")
										IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_OUTFIT", CONV_PRIORITY_MEDIUM)
											bPlayerApproached = TRUE
											int i
											FOR i = 0 TO NUM_OF_SECURITY - 1
												eSecurityState[i] = SS_STANDDOWN
											ENDFOR
										ENDIF
									ENDIF
								ELSE //ONCE THIS LOOPS BACK AROUND PLAYER WILL BE SHOT FOR RETURNING AGAIN
									IF bShouldAttackPlayer = TRUE
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<12.0,12.0,12.0>>)
											IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
												DEBUG_PRINT("SS_GUARD_AREA, outfit, player came back to area after being warned; attacking")
												MAKE_SECURITY_ATTACK()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE //PLAYER IS IN A VEHICLE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<50.0,50.0,50.0>>)
						IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel) //CHECK TO SEE IF VEHICLE IS A GOLF BUGGY - IF NOT DO BELOW
							IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
									DEBUG_PRINT("SS_GUARD_AREA, player 50m away from group in non-buggy vehicle; attacking")
									MAKE_SECURITY_ATTACK()
								ENDIF
							ENDIF
						ELSE // VEHICLE MODEL IS A GOLF BUGGY
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<25.0,25.0,25.0>>)
								IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 15.0
									DEBUG_PRINT_FLOAT("Going too fast, going to trigger aggro: ", GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
									IF HAS_PED_RECEIVED_EVENT(CelebSecurity[iPedNum].mPed, EVENT_POTENTIAL_GET_RUN_OVER)
									OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<6.0,6.0,6.0>>)
										IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
											IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
												DEBUG_PRINT("SS_GUARD_AREA, player went too fast too close to security; attacking")
												MAKE_SECURITY_ATTACK()
											ENDIF
										ENDIF
									ENDIF
								ELSE 
									IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
										ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
										// Because this line can be triggered very easily and we only want to play it once, we'll track whether it's been said through a bool
										// Otherwise, all three security peds say it one after the other and it's totally weird
										IF bSecuritySaidCannotDrive = FALSE
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_7", CONV_PRIORITY_MEDIUM)
												DEBUG_PRINT_INT("'You can't drive through here buddy said by ped id: ", iPedNum)
												IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
													TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
												ENDIF
												bSecuritySaidCannotDrive = TRUE
												iSecurityWarnTimer = GET_GAME_TIMER()
												eSecurityState[iPedNum] = SS_WARN_PLAYER
											ENDIF
										ELSE
											IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
												TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
											ENDIF
											bSecuritySaidCannotDrive = TRUE
											iSecurityWarnTimer = GET_GAME_TIMER()
											eSecurityState[iPedNum] = SS_WARN_PLAYER
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_GO_TO_PLAYER
			
			IF NOT IS_PLAYER_UNARMED()
				IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebSecurity[iPedNum].mPed) < 80
						DEBUG_PRINT("SS_GO_TO_PLAYER, player armed, guard has LOS; attacking...")
						MAKE_SECURITY_ATTACK()
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<9.0,9.0,9.0>>) //PLAYER HAS GOT CLOSER, JUMP INTO LAST WARN STATE
				AND bShouldAttackPlayer = TRUE
					ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_3", CONV_PRIORITY_MEDIUM)
						IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
							TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
						ENDIF
						iSecurityWarnTimer = GET_GAME_TIMER()
						eSecurityState[iPedNum] = SS_AIM_AT_PLAYER
					ENDIF
				ELSE //PLAYER HAS LEFT OR STAYED AT SAME DISTANCE
					IF (GET_GAME_TIMER() - iSecurityWarnTimer) >= 8000
						//PLAYER HAS STAYED AT SAME DISTANCE
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>)		
							ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_2", CONV_PRIORITY_MEDIUM)
								OPEN_SEQUENCE_TASK(siCelebSecurity)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 7000)
									TASK_PLAY_ANIM(NULL,"rcmnigel1d","thanks_male_05",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT)
								CLOSE_SEQUENCE_TASK(siCelebSecurity)
								TASK_PERFORM_SEQUENCE(CelebSecurity[iPedNum].mPed,siCelebSecurity)
								CLEAR_SEQUENCE_TASK(siCelebSecurity)
								iSecurityWarnTimer = GET_GAME_TIMER()
								eSecurityState[iPedNum] = SS_WARN_PLAYER
								DEBUG_PRINT("Guard: going into SS_WARN_PLAYER ")
							ENDIF
						ELSE
							//WAIT AND DO NOTHING - PLAYER LEFT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK

		CASE SS_WARN_PLAYER
		
			IF NOT IS_PLAYER_UNARMED()
				IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebSecurity[iPedNum].mPed) < 80
						DEBUG_PRINT("SS_WARN_PLAYER, player armed, guard has LOS; attacking...")
						MAKE_SECURITY_ATTACK()
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF)// CHECK PLAYERS OUTFIT
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<4.0,4.0,4.0>>) //PLAYER HAS GOT CLOSER, JUMP INTO LAST WARN STATE
					OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<8.0,8.0,8.0>>)
					AND bShouldAttackPlayer = TRUE
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_3", CONV_PRIORITY_MEDIUM)
							IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
								TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
							ENDIF
							iSecurityWarnTimer = GET_GAME_TIMER()
							eSecurityState[iPedNum] = SS_AIM_AT_PLAYER
						ENDIF
						DEBUG_PRINT("SS_WARN_PLAYER, player got too close, going to aim...")
					ELSE
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<9.0,9.0,9.0>>)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //PLAYER IS IN A VEHICLE
								IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 15.0 //Player is above 'acceptable' speed, go to Attack
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									DEBUG_PRINT_FLOAT("SS_WARN_PLAYER, going too fast: ", GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
									ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
									PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
									DEBUG_PRINT("SS_WARN_PLAYER, player in vehicle going too fast within 9m of guard; attacking...")
									MAKE_SECURITY_ATTACK()
								ENDIF
							ENDIF
						ENDIF
						IF (GET_GAME_TIMER() - iSecurityWarnTimer) >= 7000
							//PLAYER HAS STAYED AT SAME DISTANCE
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>)
								ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_3", CONV_PRIORITY_MEDIUM)
									IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
										TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
									ENDIF
									iSecurityWarnTimer = GET_GAME_TIMER()
									eSecurityState[iPedNum] = SS_AIM_AT_PLAYER
									DEBUG_PRINT("SS_WARN_PLAYER, player took too long, going to aim...")
								ENDIF
							ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebSecurity[iPedNum].mPed) > 20.0
								TASK_ACHIEVE_HEADING(CelebSecurity[iPedNum].mPed, CelebSecurity[iPedNum].fStartHeading)
								TASK_CLEAR_LOOK_AT(CelebSecurity[iPedNum].mPed)
								DEBUG_PRINT("SS_WARN_PLAYER, player >20m away, returning to SS_GUARD_AREA...")
								eSecurityState[iPedNum] = SS_GUARD_AREA
								//WAIT AND DO NOTHING - PLAYER LEFT
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Player is in golfer outfit
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<18.0,18.0,18.0>>)
						IF bPlayerApproached = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND bConversationActive = FALSE
								IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
									TASK_TURN_PED_TO_FACE_ENTITY(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID(),-1)
								ENDIF
								IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
									TASK_TURN_PED_TO_FACE_ENTITY(CelebrityGolfer.mPed,PLAYER_PED_ID(),-1)
								ENDIF
								ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
								ADD_PED_FOR_DIALOGUE(sDialogue, 2,PLAYER_PED_ID(), "TREVOR")
								ADD_PED_FOR_DIALOGUE(sDialogue, 6,CelebrityGolfer.mPed, "FOSTENBURG")
								IF CREATE_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_OUTFIT", CONV_PRIORITY_MEDIUM)
									bPlayerApproached = TRUE
									// Make all peds go into standdown
									int i
									FOR i = 0 TO NUM_OF_SECURITY - 1
										eSecurityState[i] = SS_STANDDOWN
									ENDFOR
								ENDIF
							ENDIF
						ELSE //ONCE THIS LOOPS BACK AROUND PLAYER WILL BE SHOT FOR RETURNING AGAIN
							IF bShouldAttackPlayer = TRUE
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<12.0,12.0,12.0>>)
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
										DEBUG_PRINT("SS_WARN_PLAYER, outfit worn, player came back to area after being warned; attacking")
										MAKE_SECURITY_ATTACK()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE SS_AIM_AT_PLAYER
		
			IF NOT IS_PLAYER_UNARMED()
				IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CelebSecurity[iPedNum].mPed) < 80
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						DEBUG_PRINT("SS_AIM_AT_PLAYER, player armed, guard has LOS; attacking...")
						IF bSecurityAttack = FALSE
							MAKE_SECURITY_ATTACK()
						ELSE
							// Has this guard got stuck here in Aim after the rest have been set to attack?
							CLEAR_PED_TASKS(CelebSecurity[iPedNum].mPed)
							OPEN_SEQUENCE_TASK(siCelebSecurity)
								TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(5, 200))
								TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
							CLOSE_SEQUENCE_TASK(siCelebSecurity)
							TASK_PERFORM_SEQUENCE(CelebSecurity[iPedNum].mPed, siCelebSecurity)
							CLEAR_SEQUENCE_TASK(siCelebSecurity)
							SET_PED_COMBAT_ATTRIBUTES(CelebSecurity[iPedNum].mPed,CA_DISABLE_SECONDARY_TARGET,TRUE)
							SET_ENTITY_LOAD_COLLISION_FLAG(CelebSecurity[iPedNum].mPed, TRUE)
							SET_PED_KEEP_TASK(CelebSecurity[iPedNum].mPed,TRUE)
							SET_PED_PATH_PREFER_TO_AVOID_WATER(CelebSecurity[iPedNum].mPed, TRUE)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relHatePlayer,RELGROUPHASH_PLAYER)
							eSecurityState[iPedNum] = SS_ATTACK_PLAYER
							IF NOT DOES_BLIP_EXIST(CelebSecurity[iPedNum].mBlip)
								CelebSecurity[iPedNum].mBlip = CREATE_PED_BLIP(CelebSecurity[iPedNum].mPed,FALSE,FALSE,BLIPPRIORITY_MED)
							ENDIF
							DEBUG_PRINT_INT("Attack player, FORCE security guard: ", iPedNum)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(CelebSecurity[iPedNum].mPed), 20, FALSE)
				AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
					PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
					DEBUG_PRINT("SS_AIM_AT_PLAYER, bullet detected within 20m of a ped, attacking...")
					MAKE_SECURITY_ATTACK()
				ENDIF
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<7.0,7.0,5.0>>) //PLAYER HAS GOT CLOSER, TAKE HIM DOWN
				OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-1097.28, 68.05, 52.99>>, 10.0)
				AND bShouldAttackPlayer = TRUE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
					PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
					DEBUG_PRINT("SS_AIM_AT_PLAYER, player got too close, attacking...")
					MAKE_SECURITY_ATTACK()
				ELSE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //PLAYER IS IN A VEHICLE
							IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 15.0 //Player is above 'acceptable' speed, go to Attack
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								DEBUG_PRINT_FLOAT("SS_AIM_AT_PLAYER, going too fast: ", GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
								ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
								PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
								DEBUG_PRINT("SS_AIM_AT_PLAYER, player in vehicle above acceptable speed, within 20m; attacking...")
								MAKE_SECURITY_ATTACK()
							ENDIF
						ENDIF
					ENDIF
					IF (GET_GAME_TIMER() - iSecurityWarnTimer) >= 10000
						IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
							IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<25.0,25.0,25.0>>)
							AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebrityGolfer.mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
							//PLAYER HAS LEFT AREA
								IF IS_PED_UNINJURED(CelebSecurity[iPedNum].mPed)
									CLEAR_PED_TASKS_IMMEDIATELY(CelebSecurity[iPedNum].mPed)
									TASK_GO_STRAIGHT_TO_COORD(CelebSecurity[iPedNum].mPed,CelebSecurity[iPedNum].vStartPos,3.0,-1,CelebSecurity[iPedNum].fStartHeading)
								ENDIF
								bShouldAttackPlayer = TRUE // SHOULD NOW ATTACK PLAYER IF HE RETURNS AGAIN
								eSecurityState[iPedNum] = SS_GUARD_AREA
							ELSE
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>) //PLAYER HAS STAYED AT SAME DISTANCE
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CelebSecurity[iPedNum].mPed,<<9.0,9.0,9.0>>) //PLAYER HAS GOT CLOSER
									OR (GET_GAME_TIMER() - iSecurityWarnTimer) >= 12000
										//PLAYER STAYED IN AREA - ATTACK
										IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
										ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
										PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
										DEBUG_PRINT("SS_AIM_AT_PLAYER, player took too long and didn't move away, attacking...")
										MAKE_SECURITY_ATTACK()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ATTACK_PLAYER
			IF GET_DISTANCE_BETWEEN_ENTITIES(CelebSecurity[iPedNum].mPed,PLAYER_PED_ID()) > 120
			AND (IS_PED_IN_ANY_VEHICLE(CelebrityGolfer.mPed) OR IS_ENTITY_ALIVE(CelebrityGolfer.mPed))
				IF DOES_BLIP_EXIST(CelebSecurity[iPedNum].mBlip) 
					REMOVE_BLIP(CelebSecurity[iPedNum].mBlip)
					MAKE_PED_FLEE(CelebSecurity[iPedNum].mPed,TRUE)
					eSecurityState[iPedNum] = SS_FLEEING_AREA
				ENDIF
			ELSE
				// Keep checking for the current celeb NOT doing a sequence (i.e. not attacking) and retask if required
				IF NOT IsPedPerformingTask(CelebSecurity[iPedNum].mPed, SCRIPT_TASK_PERFORM_SEQUENCE)
					OPEN_SEQUENCE_TASK(siCelebSecurity)
						TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(5, 200))
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(siCelebSecurity)
					TASK_PERFORM_SEQUENCE(CelebSecurity[iPedNum].mPed, siCelebSecurity)
					CLEAR_SEQUENCE_TASK(siCelebSecurity)
					
					SET_PED_COMBAT_ATTRIBUTES(CelebSecurity[iPedNum].mPed,CA_DISABLE_SECONDARY_TARGET,TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(CelebSecurity[iPedNum].mPed, TRUE)
					SET_PED_KEEP_TASK(CelebSecurity[iPedNum].mPed,TRUE)
					SET_PED_PATH_PREFER_TO_AVOID_WATER(CelebSecurity[iPedNum].mPed, TRUE)
				ENDIF
			ENDIF
			//ATTACKING PLAYER
		BREAK
		
		CASE SS_STANDDOWN
		 	bShouldAttackPlayer = FALSE
			IF HAS_PLAYER_THREATENED_PED(CelebSecurity[iPedNum].mPed)
			OR HAS_PED_RECEIVED_EVENT(CelebSecurity[iPedNum].mPed, EVENT_POTENTIAL_GET_RUN_OVER)
				DEBUG_PRINT("SS_STANDDOWN, player threatened Glen OR guard thought he'd get run over; attacking...")
				MAKE_SECURITY_ATTACK()
			ELIF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),CelebSecurity[iPedNum].mPed)
				IF IS_PLAYER_USING_SNIPER_RIFLE()
					IF CAN_PED_SEE_PLAYER(CelebSecurity[iPedNum].mPed)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[iPedNum].mPed, "Nigel1DCelebSecurity")
						PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_6", CONV_PRIORITY_MEDIUM)
						DEBUG_PRINT("SS_STANDDOWN, player aiming Sniper Rifle, guard has LOS; attacking...")
						MAKE_SECURITY_ATTACK()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_FLEEING_AREA
			//FLEEING THE AREA / PLAYER
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///  Checks the security is alive - Then monitors and maintains each security PEDS behaviours
PROC Monitor_Security()

	INT i
	
	FOR i = 0 TO NUM_OF_SECURITY - 1
		IF IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
			// Wide-ranging 'if any of these peds die, just fucking do this' check
			IF bSecurityAttack = FALSE
			AND bShouldAttackPlayer = FALSE
				IF HAS_PLAYER_MUCKED_ABOUT()
					DEBUG_PRINT("Monitor_Security, player has 'mucked about' with the celeb scene; attacking...")
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1098.8646, 69.9792, 53.6198>>) < 45
						ADD_PED_FOR_DIALOGUE(sDialogue, 7,CelebSecurity[i].mPed, "Nigel1DCelebSecurity")
						PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_SECUR", "NIG1D_SECUR_4", CONV_PRIORITY_MEDIUM)
						MAKE_SECURITY_ATTACK()
					ELSE
						MAKE_SECURITY_ALERT()
					ENDIF					
				ENDIF
			ENDIF
			Monitor_Security_Ped(i)
		ELSE
			IF DOES_BLIP_EXIST(CelebSecurity[i].mBlip) 
				REMOVE_BLIP(CelebSecurity[i].mBlip)
			ENDIF
			IF DOES_ENTITY_EXIST(CelebSecurity[i].mPed)
				IF IS_ENTITY_DEAD(CelebSecurity[i].mPed)
					CelebSecurity[i].bImDead = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

/// PURPOSE:
///  Makes sure Celebrity Security models have loaded, then creates them.
///  Will then monitor them once created
PROC HANDLE_CELEB_SECURITY()

	INT i
	
	IF bSecurityCreated = FALSE
	
		IF HAS_SECURITY_LOADED()
		
			DEBUG_PRINT("Celeb security loaded, creating now...")
		
			FOR i = 0 TO NUM_OF_SECURITY - 1
	            CreateMissionPed(CelebSecurity[i],FALSE)
	            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CelebSecurity[i].mPed,TRUE) 
				GIVE_WEAPON_TO_PED(CelebSecurity[i].mPed,WEAPONTYPE_PISTOL,-1)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(CelebSecurity[i].mPed,TRUE)
				SET_PED_CAN_BE_TARGETTED(CelebSecurity[i].mPed,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(CelebSecurity[i].mPed,relHatePlayer)
				SET_PED_DIES_WHEN_INJURED(CelebSecurity[i].mPed,TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(CelebSecurity[i].mModel, TRUE)
	      	ENDFOR
			
			bSecurityCreated = TRUE
		
		ENDIF
	
	ELSE
		Monitor_Security()
	ENDIF

ENDPROC

/// PURPOSE:
///    Requests the models for the golf course security
/// PARAMS:
///    bWaitForLoad - Will force it to wait and load them
/// RETURNS:
///    True - If the models have loaded, false otherwise
FUNC BOOL HAS_COURSE_SECURITY_LOADED(BOOL bWaitForLoad = FALSE)

	INT i
	
	FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
	
		REQUEST_MODEL(CourseSecurity[i].mModel)
		
	ENDFOR
	
	IF bWaitForLoad = FALSE
     
	 
        FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
            IF NOT HAS_MODEL_LOADED(CourseSecurity[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR

	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED CONCIEGRES LOADED
            FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
            	IF NOT HAS_MODEL_LOADED(CourseSecurity[i].mModel)
                	bEverythingLoaded = FALSE
           		ENDIF
       	 	ENDFOR
	

        ENDWHILE
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    If the player gets into a vehicle, check if it's a Caddy (or Caddy2) and add it to the audio mix if so
///    If player exits a vehicle, remove the index
PROC HANDLE_CADDY_AUDIO_MIX()
	
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1D_SCENE")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF NOT bIsInCaddy
				IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE), CADDY)
				OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE), CADDY2)
					viStoredCaddyIdx = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(viStoredCaddyIdx,"NIGEL_1D_GOLF_CART_B_TREVOR")
					DEBUG_PRINT("Adding current player Caddy to audio mix...")
					bIsInCaddy = TRUE
				ELSE
					DEBUG_PRINT("Player is NOT in a Caddy - not adding to audio, but stopping checking model...")
					bIsInCaddy = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bIsInCaddy
				IF DOES_ENTITY_EXIST(viStoredCaddyIdx)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viStoredCaddyIdx)
					DEBUG_PRINT("Removing player Caddy from audio mix")
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viStoredCaddyIdx)
				ENDIF
				bIsInCaddy = FALSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Check for the player driving a vehicle other than a golf cart in the course, and give them a wanted level
///    The security guards also check for this themselves, but this will cover the whole course so the player can't get away with it
///    This also manages the 'wanted' state machine generally
PROC CHECK_FOR_DRIVING_IN_COURSE()

	SWITCH eWantedState
		CASE WS_NOT_WANTED
			IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
					IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel)
						DEBUG_PRINT("Setting wanted level for driving in the course!")
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						eWantedState = WS_WAITING_FOR_WANTED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE WS_WAITING_FOR_WANTED
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				eWantedState = WS_WANTED
			ENDIF
		BREAK
		
		CASE WS_WANTED
			IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
					IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel)
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
          				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
						SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF
				ENDIF
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				DEBUG_PRINT("Resetting wanted level state")
				eWantedState = WS_NOT_WANTED
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Display help text warnings about weapons and vehicles being prohibited when the player is in the right area
PROC HELP_TEXT_WARNINGS()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationOne,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationTwo,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationThree,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CLEAR_HELP()
			PRINT_HELP("N1D_HELP1") // Weapons and vehicles are prohibited on the golf course.
			bHelpTextDisplayed = TRUE
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Monitors the golf course security deals with their behaviours
/// PARAMS:
///    iPedNum - The ped we are checking
PROC Monitor_Course_Security(INT iPedNum)

	IF eGolferState <> GS_FLEE_FROM_PLAYER_IN_BUGGY
	AND eGolferState <> GS_FLEEING_ON_FOOT
	AND eMissionStage <> MS_LOSE_THE_COPS
	AND eMissionStage <> MS_LOSE_SECURITY
	
		SWITCH eCourseSecurity[iPedNum]
			
			CASE CS_GET_IN_POSITION
				IF iPedNum = COURSESEC_ENTRANCE
					TASK_FOLLOW_NAV_MESH_TO_COORD(CourseSecurity[iPedNum].mPed,<< -1297.0706, -31.0982, 47.7225 >>,1.0, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS,
						ENAV_DEFAULT, CourseSecurity[iPedNum].fStartHeading)
					eCourseSecurity[iPedNum] = CS_WAIT_AT_LOCATION
				ELSE
					eCourseSecurity[iPedNum] = CS_WAIT_AT_LOCATION
				ENDIF
			BREAK
			
			CASE CS_WAIT_AT_LOCATION
			
				IF IS_PED_UNINJURED(CourseSecurity[iPedNum].mPed)
				
					IF (HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
					AND CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed) )
					OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
						IF NOT IS_PLAYER_UNARMED()
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
							eWantedState = WS_WAITING_FOR_WANTED
							MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed)
							eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
						ELSE
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),1)
							eWantedState = WS_WAITING_FOR_WANTED
							GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
							TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
							SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
							eCourseSecurity[iPedNum] = CS_CALL_POLICE
						ENDIF
					ELSE
						IF bWarned = FALSE
							//WARN ABOUT CAR
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CourseSecurity[iPedNum].mPed,<<30.0,30.0,30.0>>)
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
								AND NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel)
									IF IS_VEHICLE_OK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationOne,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
										OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationTwo,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
										OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vStopCarLocationThree,<<CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS,CF_CAR_STOPPING_DIS>>)
											IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  WAITING_TO_START_TASK
											AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_VEHICLE) <>  PERFORMING_TASK
												ADD_PED_FOR_DIALOGUE(sDialogue, 5,CourseSecurity[iPedNum].mPed, "NIGEL1DCONSIERGE")
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
													TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),10000)
													GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
													DEBUG_PRINT("Security dialogue: Trevor warned about vehicles")
													bWarned = TRUE
													iGuardWarnTimer = GET_GAME_TIMER()
													eCourseSecurity[iPedNum] = CS_PLAYER_WARNED
												ENDIF
											ENDIF
										ENDIF
									ENDIF	
								ELSE // If player isn't in a car but is armed, warn about guns
									IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),CourseSecurity[iPedNum].mPed,<<10.0,10.0,10.0>>)
										IF NOT IS_PLAYER_UNARMED()
											IF CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed)
												ADD_PED_FOR_DIALOGUE(sDialogue, 5,CourseSecurity[iPedNum].mPed, "NIGEL1DCONSIERGE")
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
													CLEAR_PED_TASKS(CourseSecurity[iPedNum].mPed)
													TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
													TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),10000)
													GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
													DEBUG_PRINT("Security dialogue: Trevor warned about weapons")
													bWarned = TRUE
													iGuardWarnTimer = GET_GAME_TIMER()
													eCourseSecurity[iPedNum] = CS_PLAYER_WARNED
												ENDIF
											ELSE
												// Player is on phone or involved in a higher priority conversation
												TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
											ENDIF
										ELSE // If player isn't armed or in a car and isn't wearing the golf outfit, tell them about the golf outfit
											ADD_PED_FOR_DIALOGUE(sDialogue, 5,CourseSecurity[iPedNum].mPed, "NIGEL1DCONSIERGE")
											IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF)
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
													CLEAR_PED_TASKS(CourseSecurity[iPedNum].mPed)
													TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
													TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),2000)
													DEBUG_PRINT("Security dialogue: Trevor told about golf outfit")
													eCourseSecurity[iPedNum] = CS_CHAT_TO_PLAYER
												ELSE
													// Player is on phone or involved in a higher priority conversation
													TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
												ENDIF
											ELSE
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_4", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
													CLEAR_PED_TASKS(CourseSecurity[iPedNum].mPed)
													TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
													TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),2000)
													DEBUG_PRINT("Security dialogue: Trevor welcomed to club")
													eCourseSecurity[iPedNum] = CS_CHAT_TO_PLAYER
												ELSE
													// Player is on phone or involved in a higher priority conversation
													TASK_LOOK_AT_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(), 7000)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF		
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE CS_PLAYER_WARNED
			
				IF (HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
				AND CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed) )
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
					IF NOT IS_PLAYER_UNARMED()
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed)
						eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
					ELSE
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
						SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
						eCourseSecurity[iPedNum] = CS_CALL_POLICE
					ENDIF
				ELSE
					IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
							IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel)
								IF (GET_GAME_TIMER() - iGuardWarnTimer) > 15000 // Give player 15 seconds to get out of the vehicle/drive it away
									SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
									eWantedState = WS_WAITING_FOR_WANTED
									TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
									SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
									eCourseSecurity[iPedNum] = CS_CALL_POLICE
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PLAYER_UNARMED()
								IF (GET_GAME_TIMER() - iGuardWarnTimer) > 7000 // Give player 7 seconds to put weapon away
									SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
									eWantedState = WS_WAITING_FOR_WANTED
									TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
									SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
									eCourseSecurity[iPedNum] = CS_CALL_POLICE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
			BREAK
		
			CASE CS_CHAT_TO_PLAYER
				IF (HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
				AND CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed) )
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
					IF NOT IS_PLAYER_UNARMED()
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed)
						eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
					ELSE
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
						TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
						SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
						eCourseSecurity[iPedNum] = CS_CALL_POLICE
					ENDIF
				ELSE
					IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
							IF NOT IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),CelebrityBuggy.mModel)
								ADD_PED_FOR_DIALOGUE(sDialogue, 5,CourseSecurity[iPedNum].mPed, "NIGEL1DCONSIERGE")
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
									TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),10000)
									GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
									bWarned = TRUE
									iGuardWarnTimer = GET_GAME_TIMER()
									eCourseSecurity[iPedNum] = CS_PLAYER_WARNED
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PLAYER_UNARMED()
								IF CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed)
									ADD_PED_FOR_DIALOGUE(sDialogue, 5,CourseSecurity[iPedNum].mPed, "NIGEL1DCONSIERGE")
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_WARN", "NIG1D_WARN_2", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
										TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID(),10000)
										GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
										bWarned = TRUE
										iGuardWarnTimer = GET_GAME_TIMER()
										eCourseSecurity[iPedNum] = CS_PLAYER_WARNED
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TASK_ACHIEVE_HEADING(CourseSecurity[iPedNum].mPed, CourseSecurity[iPedNum].fStartHeading)
					ENDIF
				ENDIF
			BREAK
			
			CASE CS_CALL_POLICE
				IF (HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
				AND CAN_PED_SEE_PLAYER(CourseSecurity[iPedNum].mPed) )
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
					IF NOT IS_PLAYER_UNARMED()
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed)
						eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
					ELSE
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
						eWantedState = WS_WAITING_FOR_WANTED
						GIVE_WEAPON_TO_PED(CourseSecurity[iPedNum].mPed,WEAPONTYPE_STUNGUN,-1, TRUE)
						TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
						SET_PED_CAN_BE_TARGETTED(CourseSecurity[iPedNum].mPed,TRUE)
						eCourseSecurity[iPedNum] = CS_ATTACK_PLAYER
					ENDIF
				ENDIF
			BREAK
			
			CASE CS_ATTACK_PLAYER
				//ATTACKING PLAYER
				// Once the player has been tased, stop the course security from firing
				SWITCH eTaseState
					CASE TS_NOT_TASED
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
							DEBUG_PRINT("Player tased! Calming the tasers down...")
							iGuardTaserTimer = GET_GAME_TIMER()
							INT i
							for i = 0 TO NUM_OF_COURSE_SECURITY - 1
								IF IS_ENTITY_ALIVE(CourseSecurity[i].mPed)
									IF eCourseSecurity[i] = CS_ATTACK_PLAYER
										CLEAR_PED_TASKS(CourseSecurity[i].mPed)
										TASK_AIM_GUN_AT_ENTITY(CourseSecurity[i].mPed, PLAYER_PED_ID(), -1, TRUE)
									ENDIF
								ENDIF
							ENDFOR
							CLEAR_ENTITY_LAST_WEAPON_DAMAGE(PLAYER_PED_ID())
							eTaseState = TS_TASED
						ENDIF
					BREAK
					CASE TS_TASED
						IF (GET_GAME_TIMER() - iGuardTaserTimer) > 7000
							DEBUG_PRINT("Tase timer expired, let the tasing begin once more")
							INT i
							for i = 0 TO NUM_OF_COURSE_SECURITY - 1
								IF IS_ENTITY_ALIVE(CourseSecurity[i].mPed)
									IF eCourseSecurity[i] = CS_ATTACK_PLAYER
										CLEAR_PED_TASKS(CourseSecurity[i].mPed)
										TASK_COMBAT_PED(CourseSecurity[iPedNum].mPed,PLAYER_PED_ID())
										//SET_PED_SHOOT_RATE(CourseSecurity[i].mPed, 70)
									ENDIF
								ENDIF
							ENDFOR
							eTaseState = TS_NOT_TASED
						ENDIF
					BREAK
				ENDSWITCH
				IF NOT IS_PLAYER_UNARMED()
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
					eWantedState = WS_WAITING_FOR_WANTED
					MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed)
					eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
				ENDIF
			BREAK
			
			CASE CS_FLEE_FROM_PLAYER
				//FLEEING FROM PLAYER
			BREAK

			
		ENDSWITCH
	ELSE
		SWITCH eCourseSecurity[iPedNum]
		
			CASE CS_WAIT_AT_LOCATION
				IF IS_ENTITY_AT_ENTITY(CelebrityBuggy.mVehicle,CourseSecurity[iPedNum].mPed,<<35.0,35.0,35.0>>)
				OR HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
					TASK_TURN_PED_TO_FACE_ENTITY(CourseSecurity[iPedNum].mPed,CelebrityBuggy.mVehicle,-1)
					eCourseSecurity[iPedNum] = CS_TURN_TO_FACE_BUGGY
				ENDIF
			BREAK
			
			CASE CS_TURN_TO_FACE_BUGGY
				IF IS_ENTITY_AT_ENTITY(CelebrityBuggy.mVehicle,CourseSecurity[iPedNum].mPed,<<20.0,20.0,20.0>>)
				OR HAS_PLAYER_THREATENED_PED(CourseSecurity[iPedNum].mPed)
					CLEAR_PED_TASKS(CourseSecurity[iPedNum].mPed)
					TASK_FOLLOW_NAV_MESH_TO_COORD(CourseSecurity[iPedNum].mPed,CourseSecurity[iPedNum].vFleePos,3.0,DEFAULT_TIME_BEFORE_WARP)
					eCourseSecurity[iPedNum] = CS_AVOID_BUGGY
				ENDIF
			BREAK
			
			CASE CS_AVOID_BUGGY
				IF IS_ENTITY_AT_COORD(CourseSecurity[iPedNum].mPed,CourseSecurity[iPedNum].vFleePos,<<2.0,2.0,2.0>>)
					MAKE_PED_FLEE(CourseSecurity[iPedNum].mPed,TRUE)
					eCourseSecurity[iPedNum] = CS_FLEE_FROM_PLAYER
				ENDIF
				//FLEEING FROM BUGGY
			BREAK
			
			CASE CS_FLEE_FROM_PLAYER
				//FLEEING FROM PLAYER
			BREAK
		ENDSWITCH
		
	ENDIF

ENDPROC

/// PURPOSE:
///  Checks if models have loaded for golf course security then creates them.
PROC HANDLE_COURSE_SECURITY()

	INT i
	
	IF bCourseSecurityLoaded = FALSE
		IF HAS_COURSE_SECURITY_LOADED()
		
			DEBUG_PRINT("Course security loaded, creating now...")
		
			FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
	            CreateMissionPed(CourseSecurity[i],FALSE)
				SET_PED_CAN_BE_TARGETTED(CourseSecurity[i].mPed,TRUE)
	            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CourseSecurity[i].mPed,TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(CourseSecurity[i].mModel, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_MAINTAIN_MIN_DISTANCE_TO_TARGET, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_USE_COVER, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_DISABLE_SECONDARY_TARGET, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_AGGRESSIVE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(CourseSecurity[i].mPed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
				SET_PED_COMBAT_RANGE(CourseSecurity[i].mPed, CR_NEAR)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(CourseSecurity[i].mPed, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(CourseSecurity[i].mPed,relHatePlayer)
	      	ENDFOR
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,relHatePlayer, RELGROUPHASH_COP)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_COP, relHatePlayer)
			
			bCourseSecurityLoaded = TRUE
			
		ENDIF
	ELSE
		FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
			IF IS_ENTITY_ALIVE(CourseSecurity[i].mPed)
				Monitor_Course_Security(i)
			ENDIF
		ENDFOR
		IF bHelpTextDisplayed = FALSE
			HELP_TEXT_WARNINGS()
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///  Handles the celebrities Friend - Makes him flee etc.
PROC HANDLE_CELEB_FRIEND()
	
	IF bFriendIsFleeing = FALSE // IF THE FRIEND IS NOT FLEEING, CHECK HIM.
		IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
			IF HAS_PLAYER_THREATENED_PED(CelebrityFriend.mPed)
				IF IS_ENTITY_ALIVE(CelebrityFriend.mPed)
					SET_ENTITY_LOAD_COLLISION_FLAG(CelebrityFriend.mPed,TRUE)
				ENDIF
				MAKE_PED_FLEE(CelebrityFriend.mPed,TRUE)
				bFriendIsFleeing = TRUE
			ENDIF	
		ENDIF
	ENDIF


ENDPROC

/// PURPOSE:
///  Checks to see if another golfer is close to help if player attacks. 
///  If a golfer is close to the one being attacked, he will join in the fight.
PROC CHECK_IF_HELP_IS_CLOSE()

	INT i
	
	FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
		IF IS_ENTITY_ALIVE(AmbientGolfer[i].mPed)
			IF AmbientGolfer[i].bImAttacked = FALSE
				IF IS_ENTITY_AT_COORD(AmbientGolfer[i].mPed,vAttackedPosition,<<20.0,20.0,20.0>>)
					SET_PED_CAN_BE_TARGETTED(AmbientGolfer[i].mPed,TRUE)
					TASK_COMBAT_PED(AmbientGolfer[i].mPed,PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(AmbientClubs[i].mObject)
						IF IS_ENTITY_ATTACHED(AmbientClubs[i].mObject)
							DETACH_ENTITY(AmbientClubs[i].mObject)
						ENDIF
					ENDIF
					eAmbientState[i] = AS_ALERTED
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	
ENDPROC


PROC MAKE_GOLFERS_CLOSE_TO_DEAD_PED_REACT(INT iPedNum)
	
	IF DOES_ENTITY_EXIST(AmbientGolfer[iPedNum].mPed)
		int i
		FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
			IF IS_ENTITY_ALIVE(AmbientGolfer[i].mPed)
				IF IS_ENTITY_IN_RANGE_ENTITY_DEAD(AmbientGolfer[iPedNum].mPed, AmbientGolfer[i].mPed, 20)
					IF eAmbientState[i] <> AS_FLEE
						DEBUG_PRINT_INT("Making this Ambient Golfer flee from dead ped: ", i)
						MAKE_AMBIENT_GOLF_PED_FLEE(i)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

FUNC BOOL CHECK_AMBIENT_GOLF_DEAD(INT iPedNum)

	IF DOES_ENTITY_EXIST(AmbientGolfer[iPedNum].mPed)
		IF IS_ENTITY_DEAD(AmbientGolfer[iPedNum].mPed)
			IF DOES_ENTITY_EXIST(AmbientClubs[iPedNum].mObject)
				IF IS_ENTITY_ATTACHED(AmbientClubs[iPedNum].mObject)
					DETACH_ENTITY(AmbientClubs[iPedNum].mObject)
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Handles ambient golfers behaviours
/// PARAMS:
///    iPedNum - The golfer we are checking
PROC Monitor_Ambient_Golfers(INT iPedNum)
	
	SWITCH eAmbientState[iPedNum]
	
		CASE AS_PLAYGOLF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(AmbientGolfer[iPedNum].mPed),50.0,FALSE)
					MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_SHOUTED)
					ENDIF
					DEBUG_PRINT("AS_PLAYGOLF, bullet in area of ambient golfer (no vehicle); alerted...")
					MAKE_SECURITY_ALERT()
					eAmbientState[iPedNum] = AS_ALERTED
				ELSE
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),AmbientGolfer[iPedNum].mPed,<<GOLFER_ALERT_DIS,GOLFER_ALERT_DIS,GOLFER_ALERT_DIS>>)
						IF HAS_PLAYER_THREATENED_PED(AmbientGolfer[iPedNum].mPed) 
							IF NOT IS_PLAYER_UNARMED()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_MESSAGE_BEING_DISPLAYED()
									PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_SHOUTED)
								ENDIF
								MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
								DEBUG_PRINT("AS_PLAYGOLF, player threatened ambient golfer; attacking...")
								MAKE_SECURITY_ALERT()
								eAmbientState[iPedNum] = AS_ALERTED
							ELSE	
								vAttackedPosition = GET_ENTITY_COORDS(AmbientGolfer[iPedNum].mPed)
								AmbientGolfer[iPedNum].bImAttacked = TRUE
								CHECK_IF_HELP_IS_CLOSE()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_MESSAGE_BEING_DISPLAYED()
									PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_FUCK_YOU", SPEECH_PARAMS_SHOUTED)
									DEBUG_PRINT("Played aggro line from ambient golfer...")
								ENDIF
								SET_PED_CAN_BE_TARGETTED(AmbientGolfer[iPedNum].mPed,TRUE)
								TASK_COMBAT_PED(AmbientGolfer[iPedNum].mPed,PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(AmbientClubs[iPedNum].mObject)
									IF IS_ENTITY_ATTACHED(AmbientClubs[iPedNum].mObject)
										DETACH_ENTITY(AmbientClubs[iPedNum].mObject)
									ENDIF
								ENDIF
								eAmbientState[iPedNum] = AS_ALERTED
							ENDIF
						ELSE
							IF CAN_PED_SEE_PLAYER(AmbientGolfer[iPedNum].mPed)
								IF NOT IS_PLAYER_UNARMED()
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND NOT IS_MESSAGE_BEING_DISPLAYED()
										PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_SHOUTED)
									ENDIF
									MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
									DEBUG_PRINT("AS_PLAYGOLF, player armed and ambient golfer has LOS; attacking...")
									MAKE_SECURITY_ALERT()
									eAmbientState[iPedNum] = AS_ALERTED
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 10.0
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),AmbientGolfer[iPedNum].mPed,<<ALERT_DIS,ALERT_DIS,ALERT_DIS>>)
					AND HAS_PED_RECEIVED_EVENT(AmbientGolfer[iPedNum].mPed, EVENT_POTENTIAL_GET_RUN_OVER)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(AmbientGolfer[iPedNum].mPed)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_CURSE_MED", SPEECH_PARAMS_SHOUTED)
						ENDIF
						DEBUG_PRINT_INT("This ambient golfer nearly got run over and is now attacking player: ", iPedNum)
						SET_PED_CAN_BE_TARGETTED(AmbientGolfer[iPedNum].mPed,TRUE)
						TASK_COMBAT_PED(AmbientGolfer[iPedNum].mPed,PLAYER_PED_ID())
						CHECK_IF_HELP_IS_CLOSE()
						eAmbientState[iPedNum] = AS_ALERTED
					ENDIF
				ELIF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(AmbientGolfer[iPedNum].mPed),50.0)
					MAKE_PED_FLEE(AmbientGolfer[iPedNum].mPed,TRUE)
					MAKE_GOLFERS_CLOSE_TO_DEAD_PED_REACT(iPedNum)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_SHOUTED)
					ENDIF
					DEBUG_PRINT("AS_PLAYGOLF, bullet in area of ambient golfer (in vehicle); attacking...")
					MAKE_SECURITY_ALERT()
					eAmbientState[iPedNum] = AS_ALERTED
				ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(AmbientGolfer[iPedNum].mPed)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(AmbientGolfer[iPedNum].mPed, PLAYER_PED_ID(), TRUE)
					DEBUG_PRINT("Player damaged an ambient golfer with their vehicle (or another vehicle damaged them?)")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_FUCK_YOU", SPEECH_PARAMS_SHOUTED)
					ENDIF
					MAKE_GOLFERS_CLOSE_TO_DEAD_PED_REACT(iPedNum)
					MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
					eAmbientState[iPedNum] = AS_FLEE
				ENDIF
			ENDIF
		BREAK
	
		CASE AS_ALERTED
			IF HAS_PLAYER_THREATENED_PED(AmbientGolfer[iPedNum].mPed)
			AND NOT IS_PLAYER_UNARMED()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					ADD_PED_FOR_DIALOGUE(sDialogue, 8,AmbientGolfer[iPedNum].mPed, "NIGEL1DAMBIENTGOLFER1")
					PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_CURSE_MED", SPEECH_PARAMS_SHOUTED)
				ENDIF
				DEBUG_PRINT("AS_ALERTED, player threatened ambient golfer and was armed; attacking...")
				MAKE_SECURITY_ALERT()
				MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
				eAmbientState[iPedNum] = AS_FLEE
			ENDIF
		BREAK
		
		CASE AS_FLEE
			IF NOT IsPedPerformingTask(AmbientGolfer[iPedNum].mPed, SCRIPT_TASK_SMART_FLEE_PED)
				MAKE_AMBIENT_GOLF_PED_FLEE(iPedNum)
			ENDIF
			SAFE_RELEASE_PED(AmbientGolfer[iPedNum].mPed)
		BREAK
		
		CASE AS_DEAD
		BREAK
	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks to see if Ambient golfer models have loaded
/// PARAMS:
///    bWaitForLoad - Will wait until they have loaded
/// RETURNS:
///    True - If models have been loaded, false otherwise
FUNC BOOL HAVE_AMBIENT_GOLFERS_LOADED(BOOL bWaitForLoad = FALSE)

	INT i
	
	FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
	
		REQUEST_MODEL(AmbientGolfer[i].mModel)
		
	ENDFOR
	
	FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
	
		REQUEST_MODEL(AmbientClubs[i].mModel)
		
	ENDFOR
	

	
	IF bWaitForLoad = FALSE
     
	 
        FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
            IF NOT HAS_MODEL_LOADED(AmbientGolfer[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
			IF NOT HAS_MODEL_LOADED(AmbientClubs[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR

	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED CONCIEGRES LOADED
            FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
            	IF NOT HAS_MODEL_LOADED(AmbientGolfer[i].mModel)
                	bEverythingLoaded = FALSE
           		ENDIF
       	 	ENDFOR
			
			FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
				IF NOT HAS_MODEL_LOADED(AmbientClubs[i].mModel)
					bEverythingLoaded = FALSE
				ENDIF
			ENDFOR
	
        ENDWHILE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
/// Checks if ambeint golfer models have been loaded if so creates the golfer PEDS.
/// Will then start checking them. 
PROC HANDLE_AMBIENT_GOLFERS()

	INT i
	
	IF bAmbientGolfersCreated = FALSE
		IF HAVE_AMBIENT_GOLFERS_LOADED()
		
			FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
	            CreateMissionPed(AmbientGolfer[i],FALSE)
	            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AmbientGolfer[i].mPed,TRUE) 
				SET_PED_CAN_BE_TARGETTED(AmbientGolfer[i].mPed,TRUE)
	      	ENDFOR
			
			FOR i = 0 TO NUM_OF_AMB_CLUBS - 1
				CreateMissionProp(AmbientClubs[i],FALSE)			
				ATTACH_ENTITY_TO_ENTITY(AmbientClubs[i].mObject,AmbientGolfer[i].mPed,GET_PED_BONE_INDEX(AmbientGolfer[i].mPed,BONETAG_PH_R_HAND),<<0.06,0.04,0.0>>,<<-90,180,0>>)
	      	ENDFOR
			
			//SPECIAL ATTACH CASES FOR GOLFERS WITH ANIMS
			ATTACH_ENTITY_TO_ENTITY(AmbientClubs[AMBIENTPED_HOLE4WATCH1].mObject,AmbientGolfer[AMBIENTPED_HOLE4WATCH1].mPed,GET_PED_BONE_INDEX(AmbientGolfer[AMBIENTPED_HOLE4WATCH1].mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
			ATTACH_ENTITY_TO_ENTITY(AmbientClubs[AMBIENTPED_HOLE4WATCH2].mObject,AmbientGolfer[AMBIENTPED_HOLE4WATCH2].mPed,GET_PED_BONE_INDEX(AmbientGolfer[AMBIENTPED_HOLE4WATCH2].mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
			ATTACH_ENTITY_TO_ENTITY(AmbientClubs[AMBIENTPED_NORTHLAKE].mObject,AmbientGolfer[AMBIENTPED_NORTHLAKE].mPed,GET_PED_BONE_INDEX(AmbientGolfer[AMBIENTPED_NORTHLAKE].mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.05>>,<<0, -10, -90>>)
			ATTACH_ENTITY_TO_ENTITY(AmbientClubs[AMBIENTPED_HOLE4PLAY].mObject,AmbientGolfer[AMBIENTPED_HOLE4PLAY].mPed,GET_PED_BONE_INDEX(AmbientGolfer[AMBIENTPED_HOLE4PLAY].mPed,BONETAG_PH_R_HAND),<<-0.01, 0.0, 0.0>>,<<10, 0, -90>>)
			
			FOR i = 0 TO 1 
				IF IS_PED_UNINJURED(AmbientGolfer[i].mPed)
					TASK_PLAY_ANIM(AmbientGolfer[i].mPed,"rcmnigel1d",AmbientGolfer[i].mStartAnim,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				ENDIF
			ENDFOR
			
			FOR i = 2 TO 3 
				IF IS_PED_UNINJURED(AmbientGolfer[i].mPed)
					TASK_PLAY_ANIM(AmbientGolfer[i].mPed,"mini@golf",AmbientGolfer[i].mStartAnim,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				ENDIF
			ENDFOR
			
			bAmbientGolfersCreated = TRUE
			
		ENDIF
	ELSE
		FOR i = 0 TO NUM_OF_AMB_GOLFERS - 1
		
			IF CHECK_AMBIENT_GOLF_DEAD(i)
				IF eAmbientState[i] <> AS_DEAD
					eAmbientState[i] = AS_DEAD
					DEBUG_PRINT_INT("This Ambient Golfer is now dead: ", i)
					MAKE_GOLFERS_CLOSE_TO_DEAD_PED_REACT(i)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(AmbientGolfer[i].mPed)
				Monitor_Ambient_Golfers(i)
			ENDIF
		ENDFOR
	ENDIF

ENDPROC

/// PURPOSE:
/// Monitors chase golfer peds, handles thier behaviours etc. 
PROC Monitor_Chase_Golfers(INT iPedNum)
	
	SWITCH eChaseGolferState[iPedNum]
	
		CASE AS_PLAYGOLF
	
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
					IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						IF IS_ENTITY_AT_ENTITY(CelebrityGolfer.mPed,AmbientChaseGolfer[iPedNum].mPed,<<62.0,62.0,62.0>>)
							IF IS_PED_UNINJURED(AmbientChaseGolfer[iPedNum].mPed)
								TASK_TURN_PED_TO_FACE_ENTITY(AmbientChaseGolfer[iPedNum].mPed,CelebrityGolfer.mPed,-1)
								eChaseGolferState[iPedNum] = AS_ALERTED
							ENDIF
						ELIF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(AmbientChaseGolfer[iPedNum].mPed),15.0,TRUE)
							ADD_PED_FOR_DIALOGUE(sDialogue, 8, AmbientChaseGolfer[iPedNum].mPed , "NIGEL1DAMBIENTGOLFER1")
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), AmbientChaseGolfer[iPedNum].mPed) < 45
							// Only play dialogue if the player is close enough to hear it
								PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_CURSE_MED", SPEECH_PARAMS_SHOUTED) // "What the fuck?"
							ENDIF
							MAKE_PED_FLEE(AmbientChaseGolfer[iPedNum].mPed,FALSE)
							eChaseGolferState[iPedNum] = AS_FLEE
						ENDIF
					ELSE
						IF HAS_PLAYER_THREATENED_PED(AmbientChaseGolfer[iPedNum].mPed)
						OR IS_ENTITY_AT_ENTITY(CelebrityGolfer.mPed,AmbientChaseGolfer[iPedNum].mPed,<<30.0,30.0,30.0>>)
							MAKE_PED_FLEE(AmbientChaseGolfer[iPedNum].mPed,FALSE)
							eChaseGolferState[iPedNum] = AS_FLEE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	
		BREAK
	
		CASE AS_ALERTED	
			IF IS_ENTITY_AT_ENTITY(CelebrityGolfer.mPed,AmbientChaseGolfer[iPedNum].mPed,<<38.0,38.0,38.0>>)
				IF IS_PED_UNINJURED(AmbientChaseGolfer[iPedNum].mPed)
					ADD_PED_FOR_DIALOGUE(sDialogue, 8, AmbientChaseGolfer[iPedNum].mPed , "NIGEL1DAMBIENTGOLFER1")
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), AmbientChaseGolfer[iPedNum].mPed) < 45
					// Only play dialogue if the player is close enough to hear it
						PLAY_PED_AMBIENT_SPEECH(AmbientGolfer[iPedNum].mPed, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_SHOUTED) // "You god damn maniac!"
					ENDIF
					CLEAR_PED_TASKS_IMMEDIATELY(AmbientChaseGolfer[iPedNum].mPed)
					TASK_GO_STRAIGHT_TO_COORD(AmbientChaseGolfer[iPedNum].mPed,AmbientChaseGolfer[iPedNum].vFleePos,3.0,-1)
					SET_PED_KEEP_TASK(AmbientChaseGolfer[iPedNum].mPed, TRUE)
					eChaseGolferState[iPedNum] = AS_GO_TO_FLEE_POS
				ENDIF
			ENDIF
		BREAK	
		
		CASE AS_GO_TO_FLEE_POS
			IF IS_ENTITY_AT_COORD(AmbientChaseGolfer[iPedNum].mPed,AmbientChaseGolfer[iPedNum].vFleePos,<<2.0,2.0,2.0>>)
				MAKE_PED_FLEE(AmbientChaseGolfer[iPedNum].mPed,FALSE)
				eChaseGolferState[iPedNum] = AS_FLEE
			ENDIF
		BREAK
		
		CASE AS_FLEE
			//FLEEING FROM SOMETHING
		BREAK
	
	ENDSWITCH	

ENDPROC

/// PURPOSE:
///    Checks to see if Ambient golfer models have loaded
/// PARAMS:
///    bWaitForLoad - Will wait until they have loaded
/// RETURNS:
///    True - If models have been loaded, false otherwise
FUNC BOOL HAVE_AMBIENT_CHASE_GOLFERS_LOADED(BOOL bWaitForLoad = FALSE)

	INT i
	
	FOR i = 0 TO NUM_OF_AMB_CHASE_GOLFERS - 1
	
		REQUEST_MODEL(AmbientChaseGolfer[i].mModel)
		
	ENDFOR
	
	FOR i = 0 TO NUM_OF_AMB_BUGGIES - 1
	
		REQUEST_MODEL(AmbientGolfBuggy[i].mModel)
		
	ENDFOR
	

	
	IF bWaitForLoad = FALSE
     
	 
        FOR i = 0 TO NUM_OF_AMB_CHASE_GOLFERS - 1
            IF NOT HAS_MODEL_LOADED(AmbientChaseGolfer[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		 FOR i = 0 TO NUM_OF_AMB_BUGGIES - 1
            IF NOT HAS_MODEL_LOADED(AmbientGolfBuggy[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR


	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED LOADED
            FOR i = 0 TO NUM_OF_AMB_CHASE_GOLFERS - 1
            	IF NOT HAS_MODEL_LOADED(AmbientChaseGolfer[i].mModel)
                	bEverythingLoaded = FALSE
           		ENDIF
       	 	ENDFOR
			
			//HAVE BUGGIES LOADED
			FOR i = 0 TO NUM_OF_AMB_BUGGIES - 1
            	IF NOT HAS_MODEL_LOADED(AmbientGolfBuggy[i].mModel)
                	bEverythingLoaded = FALSE
           		ENDIF
       	 	ENDFOR
	
        ENDWHILE
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
/// Handles golfers that are present during the chase with the celebrity. 
PROC HANDLE_CHASE_AMBIENT_GOLFERS()
	
	INT i 
	
	IF bAmbientChaseGolfersCreated = FALSE
	
		IF HAVE_AMBIENT_CHASE_GOLFERS_LOADED()
		
			FOR i = 0 TO NUM_OF_AMB_CHASE_GOLFERS - 1
	            CreateMissionPed(AmbientChaseGolfer[i],FALSE)
	            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(AmbientChaseGolfer[i].mPed,TRUE) 
				SET_PED_CAN_BE_TARGETTED(AmbientChaseGolfer[i].mPed,FALSE)
	      	ENDFOR
			
			FOR i = 0 TO NUM_OF_AMB_BUGGIES - 1
				CreateMissionVehicle(AmbientGolfBuggy[i],FALSE) //KEEP GOLF BUGGY IN MEMORY FOR WHOLE MISSION
				SET_VEHICLE_MODEL_IS_SUPPRESSED(AmbientGolfBuggy[i].mModel,TRUE)
       	 	ENDFOR 
			
			bAmbientChaseGolfersCreated = TRUE
			
		ENDIF
	ELSE
		FOR i = 0 TO NUM_OF_AMB_CHASE_GOLFERS - 1
			IF IS_ENTITY_ALIVE(AmbientChaseGolfer[i].mPed)
				Monitor_Chase_Golfers(i)
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Loads celebrity models, makes sure they are loaded.
/// PARAMS:
///    bWaitForLoad - Makes sure the models are loaded by waiting
/// RETURNS:
///   	True - If models have been loaded, false otherwise 
FUNC BOOL HAS_CELEBRITY_LOADED(BOOL bWaitForLoad = FALSE)
	
	
	REQUEST_MODEL(CelebrityModel) 
	
	REQUEST_MODEL(A_M_Y_Golfer_01)
	
	REQUEST_MODEL(CelebrityBuggy.mModel)
	
	REQUEST_MODEL(CelebFriendClub.mModel)
	
	IF bWaitForLoad = FALSE
         
		IF NOT HAS_MODEL_LOADED(CelebrityModel)
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(A_M_Y_Golfer_01)
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(CelebrityBuggy.mModel)
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(CelebFriendClub.mModel)
			RETURN FALSE
		ENDIF
        
	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL PED CONCIEGRES LOADED
          	IF NOT HAS_MODEL_LOADED(CelebrityModel)
                bEverythingLoaded = FALSE
     		ENDIF
			
			IF NOT HAS_MODEL_LOADED(A_M_Y_Golfer_01)
				bEverythingLoaded = FALSE
			ENDIF
			
			IF NOT HAS_MODEL_LOADED(CelebrityBuggy.mModel)
                bEverythingLoaded = FALSE
     		ENDIF
			
			IF NOT HAS_MODEL_LOADED(CelebFriendClub.mModel)
                bEverythingLoaded = FALSE
     		ENDIF
	

        ENDWHILE
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Handles creating the celebrity and pickup on back of golf buggie
PROC CREATE_CELEBRITY()

	WHILE bCelebrityCreated = FALSE
		IF HAS_CELEBRITY_LOADED()
		
			//CREATE GOLFER
			CreateMissionPed(CelebrityGolfer,FALSE)
			SET_PED_PROP_INDEX(CelebrityGolfer.mPed, ANCHOR_HEAD, 0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CelebrityGolfer.mPed,TRUE)
			SET_PED_CAN_BE_TARGETTED(CelebrityGolfer.mPed,FALSE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(CelebrityGolfer.mPed,TRUE)
			SET_ENTITY_LOAD_COLLISION_FLAG(CelebrityGolfer.mPed,TRUE)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(CelebrityGolfer.mPed)
			
			//CREATE CELEBRITY Friend
			CreateMissionPed(CelebrityFriend,FALSE)
			
			//CREATE FriendS CLUB
			CreateMissionProp(CelebFriendClub,FALSE)
			ATTACH_ENTITY_TO_ENTITY(CelebFriendClub.mObject,CelebrityFriend.mPed,GET_PED_BONE_INDEX(CelebrityFriend.mPed,BONETAG_PH_R_HAND),<<0.0,0.0,0.0>>,<<0,0,0>>)
			TASK_PLAY_ANIM(CelebrityFriend.mPed,"rcmnigel1d","idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CelebrityFriend.mPed,TRUE)
			SET_PED_CAN_BE_TARGETTED(CelebrityFriend.mPed,FALSE)
					
			//CREATE GOLF BAG - ATTACH TO BUGGIE
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				golfClubPickup.pos = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(CelebrityGolfer.mPed))
			ENDIF
			oiGolfBag = CREATE_OBJECT(PROP_GOLF_BAG_01, golfClubPickup.pos)
			CreateMissionVehicle(CelebrityBuggy,FALSE)
			IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1D_SCENE")
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(CelebrityBuggy.mVehicle, "NIGEL_1D_GOLF_CART_A")
				ENDIF
			ENDIF
			SET_VEHICLE_ENGINE_ON(CelebrityBuggy.mVehicle,TRUE,TRUE)
			SET_ENTITY_HEALTH(CelebrityBuggy.mVehicle,1200)
			ATTACH_ENTITY_TO_ENTITY(oiGolfBag,CelebrityBuggy.mVehicle,0,<<0.2,-1.3,0.5>>,<<0,0,0>>)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(CelebrityBuggy.mModel,TRUE)
			
			//CREATE GOLF CLUB - ATTACH TO BAG
			golfClubPickup.objIndex = CREATE_OBJECT(PROP_GOLF_WOOD_01, golfClubPickup.pos)
			//SET_ENTITY_INVINCIBLE(golfClubPickup.objIndex,TRUE)
			ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,CelebrityGolfer.mPed,GET_PED_BONE_INDEX(CelebrityGolfer.mPed,BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
			//Unload models
			SET_MODEL_AS_NO_LONGER_NEEDED(CelebrityGolfer.mModel) 
			
			OPEN_SEQUENCE_TASK(siCelebrity)
				TASK_ACHIEVE_HEADING(NULL, 112.0)
				TASK_PLAY_ANIM(NULL,"rcmnigel1d","swing_a_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
				TASK_PLAY_ANIM(NULL,"rcmnigel1d","swing_b_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
				SET_SEQUENCE_TO_REPEAT(siCelebrity,REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(siCelebrity)
				TASK_PERFORM_SEQUENCE(CelebrityGolfer.mPed,siCelebrity)
			CLEAR_SEQUENCE_TASK(siCelebrity)
			
			// Create buggies
			CreateMissionVehicle(GolfBuggieTransport[BUGGY_BEHINDCELEB],FALSE)
			CreateMissionVehicle(GolfBuggieTransport[BUGGY_NEXTTOCELEB],FALSE)
			
			DEBUG_PRINT("Celeb created!")
			bCelebrityCreated = TRUE
			
		ELSE
			DEBUG_PRINT("Waiting for celeb assets...")
		ENDIF
		WAIT(0)
	ENDWHILE
	
	
ENDPROC

/// PURPOSE:
/// Detaches golf bag from buggy if needed.
/// Adds blip to golf club.
PROC CREATE_GOLF_PICKUP()

	IF bDropGolfBag = TRUE
		IF IS_ENTITY_ALIVE(oiGolfBag)
			IF IS_ENTITY_ATTACHED(oiGolfBag)
				DETACH_ENTITY(oiGolfBag)
			ENDIF
		ENDIF
		ADD_PED_FOR_DIALOGUE(sDialogue, 6, CelebrityGolfer.mPed , "FOSTENBURG")
		ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
		PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "NIG1DAU", "NIG1D_HIT", "NIG1D_HIT_11", CONV_PRIORITY_MEDIUM)
	ENDIF
	
	IF DOES_ENTITY_EXIST(CelebrityGolfer.mPed)
	AND IS_ENTITY_ALIVE(golfClubPickup.objIndex)
		IF IS_ENTITY_ATTACHED_TO_ENTITY(golfClubPickup.objIndex, CelebrityGolfer.mPed)
			DETACH_ENTITY(golfClubPickup.objIndex)
		ENDIF
	ENDIF
	
	SAFE_REMOVE_BLIP(biMissionBlip)
	
	IF DOES_ENTITY_EXIST(golfClubPickup.objIndex)
		biMissionBlip = ADD_BLIP_FOR_ENTITY(golfClubPickup.objIndex)
		SET_BLIP_COLOUR(biMissionBlip, BLIP_COLOUR_GREEN)
	ENDIF
	
ENDPROC


PROC HANDLE_POLICE_REPORT()
	
	IF bDonePoliceReport = FALSE
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_NIGEL_1D_01", 0.0)
			DEBUG_PRINT("*** Played special Nigel 1D police report ***")
			bDonePoliceReport = TRUE
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE: initialises mission variables such as bools, ped states, area checks and requests anims
PROC InitVariables()
	PRINTSTRING("init variables ************************")

	//INIT ARRAYS
	INIT_ARRAYS()
	
	//GOLFER
	
	eGolferState = GS_WAIT_FOR_PLAYER
	eWantedState = WS_NOT_WANTED
	
	INT i
	
	FOR i = 0 TO NUM_OF_COURSE_SECURITY - 1
		eCourseSecurity[i] = CS_GET_IN_POSITION
	ENDFOR
	
	FOR i = 0 TO NUM_OF_SECURITY - 1
		eSecurityState[i] = SS_GUARD_AREA
	ENDFOR
	
	//Counters used for conversations
	iScaredConvoCounter = 0
	iChaseConvoCounter = 0
	
	iClubPickupAudioID = GET_SOUND_ID()
	
	// Commented out until audio mix makes it into a build
	IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_1D_SCENE")
		START_AUDIO_SCENE("NIGEL_1D_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1D_SCENE")
		IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(CelebrityBuggy.mVehicle, "NIGEL_1D_GOLF_CART_A")
		ENDIF
	ENDIF
	
	//SET BOOLS
	bDropGolfBag 			 	= FALSE		 				
	bCourseSecurityLoaded		= FALSE		
	bAmbientGolfersCreated		= FALSE		
	bPlayerApproached			= FALSE
	bSecuritySaidCannotDrive	= FALSE
	bShouldAttackPlayer			= FALSE
	bWarned						= FALSE			
	bHelpTextDisplayed			= FALSE	
	bFriendIsFleeing			= FALSE
	bWarnedAboutLeaving			= FALSE	
	bConversationActive 		= FALSE		
	bConciergeSceneCreated		= FALSE 		
	bMainObjectiveDisplayed 	= FALSE
	bAmbientChaseGolfersCreated = FALSE
	bSecurityAttack				= FALSE
	bHasChanged					= GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
	
	//TEMP SET PLAYERS CLOTHS TO BE GOLFER - USE THIS TO SET GOLF CLOTHS ON
	//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, FALSE)
	
	ADD_RELATIONSHIP_GROUP("SecurityGroup", relHatePlayer)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,relHatePlayer, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_COP, relHatePlayer)
	
	REQUEST_WAYPOINT_RECORDING("NIGEL1DTRANS")
	REQUEST_WAYPOINT_RECORDING("NIGEL1DGOLF")
	REQUEST_WAYPOINT_RECORDING("NIGEL1DGOLF2")
	REQUEST_WAYPOINT_RECORDING("NIGEL1DCRASH")
	REQUEST_ANIM_DICT("rcmnigel1d")
	REQUEST_ANIM_DICT("mini@golf")
	REQUEST_ADDITIONAL_TEXT("NIGEL1D",MISSION_TEXT_SLOT)
	REQUEST_MODEL(caddy)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ANIM_DICT_LOADED("rcmnigel1d")
	OR NOT HAS_ANIM_DICT_LOADED("mini@golf")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DTRANS")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF2")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DCRASH")
	OR NOT HAS_MODEL_LOADED(caddy)
		WAIT(0)
	ENDWHILE
	
	//CreateMissionVehicle(GolfBuggieTransport[BUGGY_BEHINDCELEB],FALSE)
	//CreateMissionVehicle(GolfBuggieTransport[BUGGY_NEXTTOCELEB],FALSE)
	
	
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(TRAFFIC_MULTIPLIER)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,SPC_LEAVE_CAMERA_CONTROL_ON)
	
	sbiGolfer = ADD_SCENARIO_BLOCKING_AREA(<<-1122.2018, 48.5724, 51.4652>>, <<-1076.2333, 92.1041, 60.0617>>)
	SET_PED_NON_CREATION_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>)
	
	sbiConc = ADD_SCENARIO_BLOCKING_AREA(<<-1379.33, 45.13, 50.68>>, <<-1368.01, 69.83, 55.90>>)
	
	IF iNavBlocker = -1
		iNavBlocker = ADD_NAVMESH_BLOCKING_OBJECT(<<-1375.08, 55.55, 52.93>>, <<4, 6, 4>>, 355)
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL,TREVOR_BOOK, FALSE)
	
	// Temp: Fix for B*545798
	IF IS_PHONE_ONSCREEN()
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	
	OPEN_TEST_POLY(tpGolfCourse)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1313.7141, -27.2202, 48.0314 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1294.0658, -27.0136, 47.5724 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1069.8435, -143.7690, 39.7493 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -925.2529, -89.9079, 38.3457 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1010.9485, 40.0643, 49.9277 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1122.8375, 233.7037, 64.7587 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1282.5781, 188.8798, 59.2769 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1328.1532, 192.0178, 57.8136 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1386.75, 167.32, 57.11 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1411.50, 109.64, 51.49 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1399.63, -10.83, 51.93 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1359.54, 18.93, 52.52 >>)
		ADD_TEST_POLY_VERT(tpGolfCourse, << -1342.27, 16.92, 52.16 >>)
	CLOSE_TEST_POLY(tpGolfCourse)
	
	COPY_EXPANDED_POLY(tpGolfCourseExpanded, tpGolfCourse, 100)
	
	OPEN_TEST_POLY(tpLeftSideOfCourse)
		ADD_TEST_POLY_VERT(tpLeftSideOfCourse,  << -1100.1997, 77.1233, 53.2703 >>)
		ADD_TEST_POLY_VERT(tpLeftSideOfCourse, << -1040.6903, 97.3963, 51.9067 >>)
		ADD_TEST_POLY_VERT(tpLeftSideOfCourse, << -977.9333, -5.5166, 45.6548 >>)
		ADD_TEST_POLY_VERT(tpLeftSideOfCourse, << -1039.6870, -35.8720, 45.4867 >>)
		ADD_TEST_POLY_VERT(tpLeftSideOfCourse, << -1062.1901, -2.8635, 49.6013 >>)
	CLOSE_TEST_POLY(tpLeftSideOfCourse)
	
	bFinishedSkipping = TRUE
	
	// other requests here
	#IF IS_DEBUG_BUILD // stage skipping
	
		mSkipMenu[0].sTxtLabel = "MS_GO_TO_THE_GOLF_CLUB" 
		mSkipMenu[1].sTxtLabel = "MS_GO_TO_CELEBRITY"
		mSkipMenu[2].sTxtLabel = "MS_GET_THE_CLUB"
		mSkipMenu[3].sTxtLabel = "MS_CHASE_THE_GOLFER"
		mSkipMenu[4].sTxtLabel = "MS_CHASE_IN_CITY"
		mSkipMenu[5].sTxtLabel = "MS_COLLECT_GOLF_CLUB"
		mSkipMenu[6].sTxtLabel = "MS_END_PHONECALL"
		mSkipMenu[7].sTxtLabel = "*** Debug: Swap between default and golf outfit"
		
	#ENDIF
	

ENDPROC
/// PURPOSE:
///    Deletes everything regardless
PROC DELETE_EVERYTHING()

	// DELETE PEDS
	SAFE_DELETE_PED(CelebrityGolfer.mPed)

	SAFE_DELETE_PED(CelebrityFriend.mPed)
	

	INT i
	
	FOR i = 0 to NUM_OF_CONC_PEDS - 1
		SAFE_DELETE_PED(ConciergePed[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_GOLFERS - 1
		SAFE_DELETE_PED(AmbientGolfer[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_CHASE_GOLFERS - 1 
		SAFE_DELETE_PED(AmbientChaseGolfer[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_SECURITY - 1 
		SAFE_DELETE_PED(CelebSecurity[i].mPed)
	ENDFOR
	
	FOR i = 0 to NUM_OF_COURSE_SECURITY - 1 
		SAFE_DELETE_PED(CourseSecurity[i].mPed)
	ENDFOR
	
	// DELETE VEHICLES
	
	FOR i = 0 to NUM_OF_TRANS_BUGGIES - 1
		SAFE_DELETE_VEHICLE(GolfBuggieTransport[i].mVehicle)
	ENDFOR
	
	FOR i = 0 to NUM_OF_AMB_BUGGIES - 1
		SAFE_DELETE_VEHICLE(AmbientGolfBuggy[i].mVehicle)
	ENDFOR
	
	SAFE_DELETE_VEHICLE(CelebrityBuggy.mVehicle)
	
	//DELETE PROPS
	SAFE_DELETE_OBJECT(golfClubPickup.objIndex)
	
	SAFE_DELETE_OBJECT(oiGolfBag)
	
	SAFE_DELETE_OBJECT(CelebFriendClub.mObject)
	
	FOR i = 0 to NUM_OF_AMB_CLUBS - 1
		SAFE_DELETE_OBJECT(AmbientClubs[i].mObject)
	ENDFOR
	
	IF DOES_PICKUP_EXIST(golfClubPickup.index)
		REMOVE_PICKUP(golfClubPickup.index)
	ENDIF
	
	//REMOVE BLIPS
	
	// Main mission blip
	SAFE_REMOVE_BLIP(biMissionBlip)
	
	//Security Blips
	FOR i = 0 TO NUM_OF_SECURITY - 1
		SAFE_REMOVE_BLIP(CelebSecurity[i].mBlip)
	ENDFOR
	
	REMOVE_RELATIONSHIP_GROUP(relHatePlayer)
	
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)
	
ENDPROC


/// PURPOSE: Deletes all entities and unloads everything
PROC ResetMission()
	
	
	CLEAR_PRINTS()
	
	DELETE_EVERYTHING()
	
	//Set player wanted level to 0
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
	eWantedState = WS_NOT_WANTED

	UnloadEverything()
	
	//REMOVE WAYPOINT RECORDINGS
	IF GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DTRANS")
		REMOVE_WAYPOINT_RECORDING("NIGEL1DTRANS")
	ENDIF
	IF GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF")
		REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF")
	ENDIF
	IF GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DGOLF2")
		REMOVE_WAYPOINT_RECORDING("NIGEL1DGOLF2")
	ENDIF
	IF GET_IS_WAYPOINT_RECORDING_LOADED("NIGEL1DCRASH")
		REMOVE_WAYPOINT_RECORDING("NIGEL1DCRASH")
	ENDIF
	
	bCelebrityCreated = FALSE
	bSecurityCreated = FALSE
	
	InitVariables()
	
	// put peds etc in start positions
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1330.1176, -72.3159, 48.1812 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),326.0)
	ENDIF
	
	SetStage(MS_GO_TO_THE_GOLF_CLUB)
	
ENDPROC

/// PURPOSE:
///    Z-menu debug option to quickly swap between the Golf outfit and normal clothes if desired
PROC SWAP_OUTFIT()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF)
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
			ACTIVATE_OUTFIT_CHANGE(TRUE)
		ELSE
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_GOLF, FALSE)
			DEACTIVATE_OUTFIT_CHANGE()
			//SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Skips the current stage
PROC SkipStage()
	
	// stop player and buddy from doing whatever they were doing before skip
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	SWITCH eMissionStage
		
		CASE MS_INTRO_PHONECALL
			DEBUG_PRINT("In INTRO_PHONECALL stage skip")
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					KILL_PHONE_CONVERSATION()
				ENDIF
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE MS_GO_TO_THE_GOLF_CLUB
			DEBUG_PRINT("In GO_TO_THE_GOLF_CLUB stage skip")
			IF HAS_CONCIERGE_SCENE_LOADED(TRUE) 
			AND HAS_SECURITY_LOADED(TRUE)
			AND HAS_COURSE_SECURITY_LOADED(TRUE)
				IF NOT IS_REPLAY_BEING_SET_UP()
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1291.5958, -12.9214, 48.5137>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(),303.114)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MS_GO_TO_CELEBRITY
			DEBUG_PRINT("Skipping MS_GO_TO_CELBRITY...")
			bPlayerReturned = FALSE
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1190.22, 36.83, 52.03 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),299.0)	
				ENDIF
			ENDIF
//			IF IS_REPLAY_IN_PROGRESS()
//				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
//				//AND IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT()
//					REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
//					WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
//						WAIT(0)
//					ENDWHILE
//					IF NOT IS_VEHICLE_OK(ReplayVehicle.mVehicle)
//						CREATE_VEHICLE_FOR_REPLAY(ReplayVehicle.mVehicle, ReplayVehicle.vStartPos, ReplayVehicle.fStartHeading, true, FALSE, FALSE, true, TRUE, CADDY)
//						DEBUG_PRINT("Creating replay vehicle for replay")
//					ENDIF
//					IF NOT IS_REPLAY_BEING_SET_UP()
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ReplayVehicle.mVehicle)
//						SET_VEHICLE_ON_GROUND_PROPERLY(ReplayVehicle.mVehicle)
//					ENDIF
//				ELSE
//					IF NOT IS_VEHICLE_OK(ReplayVehicle.mVehicle)
//						// Create the default Caddy replay vehicle
//						ReplayVehicle.mVehicle = CREATE_VEHICLE(ReplayVehicle.mModel, ReplayVehicle.vStartPos, ReplayVehicle.fStartHeading)
//						DEBUG_PRINT("Creating buggy for replay")
//						IF NOT IS_REPLAY_BEING_SET_UP()
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ReplayVehicle.mVehicle)
//							SET_VEHICLE_ON_GROUND_PROPERLY(ReplayVehicle.mVehicle)
//						ENDIF
//					ELSE
//						IF NOT IS_REPLAY_BEING_SET_UP()
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), ReplayVehicle.mVehicle)
//							SET_VEHICLE_ON_GROUND_PROPERLY(ReplayVehicle.mVehicle)
//							DEBUG_PRINT("Setting ped into replay buggy")
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
		CASE MS_GET_THE_CLUB
			DEBUG_PRINT("In GO_TO_CELBRITY stage skip")
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1102.4547, 73.6150, 53.2039 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),238.0)	
				ENDIF
				IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
					IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
						IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
							SET_PED_INTO_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,VS_DRIVER)	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE MS_CHASE_THE_GOLFER
			DEBUG_PRINT("Skipping MS_CHASE_THE_GOLFER...")
			//CELEB VEHICLE
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				//STOP VEHICLE RECORDING
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(CelebrityBuggy.mVehicle)
					VEHICLE_WAYPOINT_PLAYBACK_PAUSE(CelebrityBuggy.mVehicle)
				ENDIF
				SET_ENTITY_COORDS(CelebrityBuggy.mVehicle,<< -1246.9563, -89.9753, 42.9040 >>)
				SET_ENTITY_HEADING(CelebrityBuggy.mVehicle,238.0)
				IF IS_PED_UNINJURED(CelebrityGolfer.mPed)
					IF NOT IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						SET_PED_INTO_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle,VS_DRIVER)	
					ENDIF
				ENDIF
			
			ENDIF
			
			//PLAYER VEHICLE
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF IS_VEHICLE_OK(GolfBuggieTransport[4].mVehicle)
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),GolfBuggieTransport[4].mVehicle)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),GolfBuggieTransport[4].mVehicle,VS_DRIVER)
						ENDIF
					ENDIF
					SET_ENTITY_COORDS(GolfBuggieTransport[4].mVehicle,<< -1273.7362, -75.3014, 44.6289 >>)
					SET_ENTITY_HEADING(GolfBuggieTransport[4].mVehicle,242.0)
				ELSE
					DEBUG_PRINT("Skip buggy isn't okay, probably going to fail")
				ENDIF
			ENDIF
			
			bClubCollectedByPlayer = FALSE // If we're skipping, ensure we don't assume the club has been collected
			
			eSubStage = SS_CLEANUP
			
		BREAK
		
		CASE MS_CHASE_IN_CITY
			DEBUG_PRINT("Skipping MS_CHASE_IN_CITY...")
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				SET_ENTITY_HEALTH(CelebrityBuggy.mVehicle,450)
				DEBUG_PRINT("SKIPPED CITY CHASE SETTING HEALTH TO 450 ")
			ENDIF
			
			IF IS_ENTITY_ALIVE(oiGolfBag)
				IF IS_ENTITY_ATTACHED(oiGolfBag)
					DETACH_ENTITY(oiGolfBag)
				ENDIF
			ENDIF
			
			SAFE_DELETE_PED(CelebrityGolfer.mPed)
			bCelebrityCreated = FALSE
			bClubCollectedByPlayer = FALSE // If we're skipping, ensure we don't assume the club has been collected
			SAFE_DELETE_VEHICLE(CelebrityBuggy.mVehicle)
			
			WAIT(2000) // Give the game time for the bag and Trevor to hit the ground after being detached
			
			eSubStage = SS_CLEANUP
			
		BREAK
		
		CASE MS_COLLECT_GOLF_CLUB
			DEBUG_PRINT("Skipping MS_COLLECT_GOLF_CLUB...")
			IF NOT IS_REPLAY_BEING_SET_UP()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(),GET_ENTITY_COORDS(oiGolfBag)+<<0,1.5,0>>)
				ENDIF
			ENDIF
			
			bClubCollectedByPlayer = TRUE
			eSubStage = SS_CLEANUP
			
		BREAK
		
		CASE MS_END_PHONECALL
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					KILL_PHONE_CONVERSATION()
				ENDIF
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF	
		BREAK
		
		//OPTIONAL STAGES
		CASE MS_LOSE_SECURITY
			INT i
			FOR i = 0 TO NUM_OF_SECURITY - 1
				IF IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
					SET_ENTITY_HEALTH(CelebSecurity[i].mPed,0)
				ENDIF
			ENDFOR
		BREAK
				
		CASE MS_LOSE_THE_COPS
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			ENDIF
		BREAK
		
		
	ENDSWITCH	
ENDPROC

/// PURPOSE: Jumps to the stage selected
PROC JumpToStage(MISSION_STAGE eNewStage)

	if eMissionStage = eNewStage  // skip current stage
		bFinishedSkipping = TRUE
		IF eMissionStage = MS_GET_THE_CLUB
			// Print the goal text when skipping to this stage, so it comes up in the replay
			PRINT_NOW("N1D_TAKE",DEFAULT_GOD_TEXT_TIME,5)	// Steal the ~g~golf club.
		ENDIF
		IF IS_REPLAY_BEING_SET_UP()
			IF GET_REPLAY_MID_MISSION_STAGE() = CP_LOCATED_STANKY
				END_REPLAY_SETUP(ReplayVehicle.mVehicle)
			ELSE
				END_REPLAY_SETUP()
			ENDIF
		ENDIF
		RC_END_Z_SKIP()
	ELSE
		DEBUG_PRINT("Still skipping...")
		SkipStage() 
	ENDIF
ENDPROC
	
#IF IS_DEBUG_BUILD
	/// PURPOSE: Goes back to the previous stage
      PROC PreviousStage()
            int iNewStage
            
		
            SWITCH eMissionStage            
            
				CASE MS_CHASE_IN_CITY
					 iNewStage = ENUM_TO_INT(MS_CHASE_THE_GOLFER)
				BREAK
				
				CASE MS_LOSE_SECURITY
					 iNewStage = ENUM_TO_INT(MS_COLLECT_GOLF_CLUB)
				BREAK
				
				CASE MS_LOSE_THE_COPS
					 iNewStage = ENUM_TO_INT(MS_COLLECT_GOLF_CLUB)
				BREAK
				
				CASE MS_END_PHONECALL
					 iNewStage = ENUM_TO_INT(MS_COLLECT_GOLF_CLUB)
				BREAK
					
                 // !! all other cases just go to the previous state in the list
                DEFAULT
                    iNewStage = ENUM_TO_INT(eMissionStage)-1 
                BREAK
				
            ENDSWITCH
			
            if iNewStage >-1 
                //we can skip to a previous stage
                eTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
				RC_START_Z_SKIP()
                ResetMission()
                bFinishedSkipping = FALSE
                JumpToStage(eTargetStage)
            ENDIF
      ENDPROC

#ENDIF

/// PURPOSE:
///    Starts a Z skip
/// PARAMS:
///    iTargetStage - 
PROC Do_Z_Skip(INT iTargetStage, BOOL bResetMission = FALSE)

	RC_START_Z_SKIP()
	
	IF bResetMission = TRUE
		ResetMission()
	ENDIF
	
	eTargetStage = INT_TO_ENUM(MISSION_STAGE, iTargetStage)
	IF eTargetStage = MS_GET_THE_CLUB
		DEBUG_PRINT("We are trying to skip to Get The Club...")
	ELIF eTargetStage = MS_GO_TO_THE_GOLF_CLUB
		DEBUG_PRINT("We are trying to skip to Go to Golf Club...")
	ENDIF
	bFinishedSkipping = FALSE
	JumpToStage(eTargetStage)
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		int iNewStage

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) // Check for Pass
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)) // Check for Fail
			WAIT_FOR_CUTSCENE_TO_STOP()
			SET_FAIL_REASON(FAIL_DEFAULT)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) // Check for Skip forward
			SkipStage()
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) // Check for Skip backwards
			PreviousStage()
		ENDIF
		
		if LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage) // Check for jumping to stage
			IF iNewStage = 7
				SWAP_OUTFIT()
			ELSE
				Do_Z_Skip(iNewStage+1, TRUE)
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

// --------------------------------------------------------------
//     STAGE FUNCTIONS
// --------------------------------------------------------------
/// PURPOSE:
///    Main mission function for the intro phonecall stage
PROC INTRO_PHONECALL()
	
	SWITCH eSubStage
		CASE SS_SETUP
		
			DEBUG_PRINT("ENTERING PHONE CALL 3 ")
			
			ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(sDialogue, 3, NULL, "NIGEL")
			ADD_PED_FOR_DIALOGUE(sDialogue, 4, NULL, "MRSTHORNHILL")
			
			IF bFinishedSkipping = TRUE
				//backup screen fade in	NEEDED for mission replay
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
					
			bConversationActive = FALSE
			
			eSubStage = SS_UPDATE
		BREAK
		CASE SS_UPDATE
			IF NOT bConversationActive
				IF PLAYER_CALL_CHAR_CELLPHONE(sDialogue, CHAR_NIGEL, "NIG1DAU", "NIG1D_INTRO", CONV_PRIORITY_VERY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 8.0, REPLAY_IMPORTANCE_LOW)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					bConversationActive = TRUE
				ENDIF
			ELSE
				IF bCelebrityCreated = FALSE //CREATE CELEBRITY BEFORE PROGRESSING
					CREATE_CELEBRITY()
				ELSE
					//HANDLE_COURSE_SECURITY()
					HANDLE_CONCIERGE_SCENE()
					HANDLE_CELEB_SECURITY()
					Monitor_Celebrity()
					HANDLE_AMBIENT_GOLFERS()
					HANDLE_CELEB_FRIEND()
					// CHECK_FOR_DRIVING_IN_COURSE() // Check for this now in case the player drives onto the course during the intro call
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_CELLPHONE_CALL_FINISHED()
						IF HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
						AND bSecurityAttack = FALSE
							// If the phone has been forced away because the player has rushed to the celeb and they're being attacked by his security, we'll just skip it
							bConversationActive = FALSE
						ELSE
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
		
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_PHONE_CONVERSATION()
			ENDIF
			REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)	//Nigel
			REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	//Mrs Thornton	
			bConversationActive = FALSE
			
			IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID()) // If the player has reached the club by the time the phone call ends, skip to "find the celeb"
				IF bCelebrityCreated = FALSE //CREATE CELEBRITY BEFORE PROGRESSING
					CREATE_CELEBRITY()
				ELSE
					SetStage(MS_GO_TO_CELEBRITY)
				ENDIF
			ELSE
				SetStage(MS_GO_TO_THE_GOLF_CLUB)
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Deals with approaching the front of the club house
PROC GO_TO_THE_GOLF_CLUB()
	
	SWITCH eSubStage
		CASE SS_SETUP
			DEBUG_PRINT("ENTERING GO TO GOLF CLUB ")
			IF bMainObjectiveDisplayed = FALSE
				//PRINT_NOW("N1D_CLUB",DEFAULT_GOD_TEXT_TIME,5)	// Go to the golf club ~y~entrance. //
				PRINT_NOW("N1D_TAKE",DEFAULT_GOD_TEXT_TIME,5)	// Steal the ~g~golf club.
				bMainObjectiveDisplayed = TRUE
			ENDIF
			SAFE_REMOVE_BLIP(biMissionBlip)
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				biMissionBlip = CREATE_PED_BLIP(CelebrityGolfer.mPed,TRUE,FALSE,BLIPPRIORITY_MED)
			ELSE
				DEBUG_PRINT("Glen was not alive when trying to create the blip in GO_TO_THE_GOLF_CLUB() init")
				CREATE_CELEBRITY()
				biMissionBlip = CREATE_PED_BLIP(CelebrityGolfer.mPed,TRUE,FALSE,BLIPPRIORITY_MED)
			ENDIF
			ACTIVATE_OUTFIT_CHANGE()
			DEBUG_PRINT("ENTERING GO TO GOLF CLUB LOOP ")
			
			// If repeat play is active and we're not skipping anymore, fade in
			IF IS_REPEAT_PLAY_ACTIVE()
			AND bFinishedSkipping
				RC_END_Z_SKIP()
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE

			IF bCelebrityCreated = FALSE //CREATE CELEBRITY BEFORE PROGRESSING
				DEBUG_PRINT("Glen created in GO_TO_THE_GOLF_CLUB() SS_UPDATE")
				CREATE_CELEBRITY()
			ELSE
				HANDLE_ABANDONMENT()
				HANDLE_AMBIENT_GOLFERS()
				HANDLE_CELEB_FRIEND()
				HANDLE_CONCIERGE_SCENE()
				HANDLE_CELEB_SECURITY()
				//HANDLE_COURSE_SECURITY()
				Monitor_Celebrity()
				HANDLE_OUTFIT_CHANGE()

				IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
					IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID()) // HAS PLAYER MADE IT INTO GOLF COURSE AREA
						eSubStage = SS_CLEANUP		
					ENDIF
				ELSE
					eSubStage = SS_CLEANUP	
				ENDIF
				
			ENDIF		
		

		BREAK
		
		CASE SS_CLEANUP
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				SetStage(MS_GO_TO_CELEBRITY) //GO TO THE CELEBRITY
			ELSE
				bMainObjectiveDisplayed = FALSE
				SetStage(MS_COLLECT_GOLF_CLUB) //SKIP TO THIS STAGE IF PLAYER KILLED CELEBRITY
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///   Handles getting into the golf buggie after PED has spoken. 
PROC GO_TO_CELEBRITY()
	
	SWITCH eSubStage
		CASE SS_SETUP
		
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 8.0, REPLAY_IMPORTANCE_LOW)
		
			DEBUG_PRINT("ENTERING GO TO CELEBRITY ")
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("N1D_TAKE",DEFAULT_GOD_TEXT_TIME,5)	// Steal the golf club
				bMainObjectiveDisplayed = TRUE
			ENDIF
			SAFE_REMOVE_BLIP(biMissionBlip)
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				biMissionBlip = CREATE_PED_BLIP(CelebrityGolfer.mPed,TRUE,FALSE,BLIPPRIORITY_MED)
			ELSE
				SCRIPT_ASSERT("CelebrityGolfer.mPed not alive in GO_TO_CELEBRITY() setup?")
			ENDIF
			eSubStage = SS_UPDATE
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			
		BREAK
		
		CASE SS_UPDATE
		
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			HANDLE_ABANDONMENT()
			HANDLE_CELEB_FRIEND()
			//HANDLE_COURSE_SECURITY()
			HANDLE_CONCIERGE_SCENE()
			HANDLE_AMBIENT_GOLFERS()
			HANDLE_CELEB_SECURITY()
			Monitor_Celebrity()
			HANDLE_OUTFIT_CHANGE()
			// CHECK_FOR_DRIVING_IN_COURSE()
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CelebrityGolfer.mPed, 50)
				OR eGolferState = GS_GET_INTO_VEHICLE // IS CELEBRITY INSIDE GOLF BUGGY
					eSubStage = SS_CLEANUP
				ELIF IS_REPLAY_BEING_SET_UP()
					eSubStage = SS_CLEANUP
				ELIF bFinishedSkipping = FALSE
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CelebrityGolfer.mPed, 150)
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ELSE
				eSubStage = SS_CLEANUP
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP
			
			bConversationActive = FALSE
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_LOCATED_STANKY, "MS_GET_THE_CLUB", TRUE) // LOCATED CELEBRITY, SET CHECKPOINT
			ELSE
				// If the player isn't in a vehicle now, don't store one
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_LOCATED_STANKY, "MS_GET_THE_CLUB", TRUE, DEFAULT, NULL, FALSE) 
			ENDIF
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				SetStage(MS_GET_THE_CLUB) // GET THE GOLF CLUB
			ELSE
				DETACH_ENTITY(golfClubPickup.objIndex)
				bMainObjectiveDisplayed = FALSE
				SetStage(MS_COLLECT_GOLF_CLUB) // IF CELEBRITY IS DEAD SKIP TO THIS STAGE
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///   Handles the journey to the golf player
PROC GET_THE_CLUB()
	
	SWITCH eSubStage
		CASE SS_SETUP	
			
			DEBUG_PRINT("ENTERING GET THE CLUB 1 ")
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("N1D_TAKE",DEFAULT_GOD_TEXT_TIME,5)	// Steal the ~g~golf club.
				bMainObjectiveDisplayed = TRUE
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			SAFE_REMOVE_BLIP(biMissionBlip)
			biMissionBlip = CREATE_PED_BLIP(CelebrityGolfer.mPed,TRUE,FALSE,BLIPPRIORITY_MED)

			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
		
			//DISPLAY_POLY2(tpLeftSideOfCourse)//DRAW THIS FOR DEBUG PURPOSES
			HANDLE_ABANDONMENT()
			HANDLE_CELEB_FRIEND()
			HANDLE_AMBIENT_GOLFERS()
			HANDLE_CELEB_SECURITY()
			HANDLE_CONCIERGE_SCENE()
			Monitor_Celebrity()
			HANDLE_OUTFIT_CHANGE()
			CHECK_STATS_GOALS()
			HANDLE_POLICE_REPORT()
			// CHECK_FOR_DRIVING_IN_COURSE()
			
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			
			IF NOT IS_ENTITY_ALIVE(golfClubPickup.objIndex)
				CLEAR_PRINTS()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF
				bConversationActive = FALSE
				SAFE_REMOVE_BLIP(biMissionBlip)
				SET_FAIL_REASON(FAIL_CLUB_DESTROYED) //PLAYER DESTROYED THE CLUB
			ENDIF
			
			// If we're still cycling through the stages for shitskipping, then just pass over this
			IF IS_REPLAY_BEING_SET_UP()
				eSubStage = SS_CLEANUP
			ENDIF

			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				IF IS_PED_FLEEING(CelebrityGolfer.mPed)
					bFostenburgFleeCheck = TRUE
				ENDIF
				IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
					IF IS_PED_IN_VEHICLE(CelebrityGolfer.mPed,CelebrityBuggy.mVehicle)
						ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,oiGolfBag,0,<<0.10,0.0,-0.25>>,<<-5,155,0>>) // REMOVE GOLF CLUB FROM HAND AND ATTACH TO GOLF BAG ON BUGGY
						eSubStage = SS_CLEANUP
					ENDIF
				ELSE
					//IF BUGGY IS DESTROYED, DETACH THE GOLF BAG
					IF IS_ENTITY_ALIVE(oiGolfBag)
						IF IS_ENTITY_ATTACHED(oiGolfBag)
							DETACH_ENTITY(oiGolfBag)
						ENDIF
					ENDIF
				ENDIF
			ELSE 
				eSubStage = SS_CLEANUP
			ENDIF
			
			IF bFostenburgFleeCheck = TRUE
				IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
					IF IS_PED_RAGDOLL(CelebrityGolfer.mPed)
						IF IS_ENTITY_ATTACHED(golfClubPickup.objIndex)
							DETACH_ENTITY(golfClubPickup.objIndex)
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		
		BREAK
		
		CASE SS_CLEANUP
			
			CLEAR_PRINTS()
			
			IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			AND NOT bFostenburgFleeCheck
				SetStage(MS_CHASE_THE_GOLFER) // CHASE THE CELEBRITY
			ELSE
				bMainObjectiveDisplayed = FALSE
				SetStage(MS_COLLECT_GOLF_CLUB) //IF CELEBRITY IS DEAD COLLECT CLUB
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///   Handles the player chasing the golf player
PROC CHASE_THE_GOLFER()

	SWITCH eSubStage
		CASE SS_SETUP
		
			DEBUG_PRINT("CHASE THE GOLFER ")
			iHitDialogueTimer = GET_GAME_TIMER() //USED FOR SINGLE HIT BUGGY DIALOGUE FROM GOLFER
			iScaredDialogueTimer = GET_GAME_TIMER() //USED FOR SINGLE SCARED LINES OF DIALOGUE
			iChaseDialogueTimer = GET_GAME_TIMER()
			//biMissionBlip = CREATE_VEHICLE_BLIP(CelebrityBuggy.mVehicle,FALSE,BLIPPRIORITY_MED)
			SET_BLIP_SCALE(biMissionBlip,1.0)
			SAFE_RELEASE_VEHICLE(GolfBuggieTransport[BUGGY_PATH2].mVehicle) // Releasing this vehicle as it's slightly in the way of Glen's path out of the course
			
			// Ensure the golf bag and the pickup are both attached to the buggy, in case we've skipped and this hasn't been done properly
			IF IS_ENTITY_ALIVE(oiGolfBag)
			AND IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF NOT IS_ENTITY_ATTACHED(oiGolfBag)
					DEBUG_PRINT("Reattaching bag to buggy!?")
					ATTACH_ENTITY_TO_ENTITY(oiGolfBag,CelebrityBuggy.mVehicle,0,<<0.2,-1.3,0.5>>,<<0,0,0>>)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
			AND IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			AND IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF NOT IS_ENTITY_ATTACHED(golfClubPickup.objIndex)
					DEBUG_PRINT("Reattaching club to bag!?")
					ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,oiGolfBag,0,<<0.10,0.0,-0.25>>,<<-5,155,0>>)
				ELIF IS_ENTITY_ATTACHED_TO_ENTITY(golfClubPickup.objIndex, CelebrityGolfer.mPed)
					DEBUG_PRINT("Glen still holding club, reattach to bag")
					DETACH_ENTITY(golfClubPickup.objIndex)
					ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,oiGolfBag,0,<<0.10,0.0,-0.25>>,<<-5,155,0>>)
				ENDIF
			ENDIF
			
			iBuggieStuckCounter = 0 // Reset for the chase
			
			DEACTIVATE_OUTFIT_CHANGE() // The mission can't be affected by the outfit change now, so disable it
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
		
			HANDLE_CELEB_FRIEND()
			Monitor_Celebrity()
			HANDLE_CELEB_SECURITY()
			//HANDLE_COURSE_SECURITY()
			DISTANCE_FAIL_CHECKS()
			HANDLE_AMBIENT_GOLFERS()
			HANDLE_CHASE_AMBIENT_GOLFERS()
			CHECK_STATS_GOALS()
			HANDLE_POLICE_REPORT()
			// CHECK_FOR_DRIVING_IN_COURSE()
			
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			
			UPDATE_CHASE_BLIP(biMissionBlip, CelebrityGolfer.mPed, LOST_RANGE)
			
			IF NOT IS_ENTITY_ALIVE(golfClubPickup.objIndex)
				CLEAR_PRINTS()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF
				bConversationActive = FALSE
				SAFE_REMOVE_BLIP(biMissionBlip)
				SET_FAIL_REASON(FAIL_CLUB_DESTROYED) //PLAYER DESTROYED THE CLUB
			ENDIF

			IF Monitor_GolfCourse_Chase() OR NOT IS_ENTITY_ALIVE(CelebrityGolfer.mPed) // SOMETHING HAS HAPPENED TO BUGGY OR CELEB IS DEAD.
				PRINT_NOW("N1D_COLLECT",DEFAULT_GOD_TEXT_TIME,5) // Collect the ~g~golf club. //
				SetStage(MS_COLLECT_GOLF_CLUB) // COLLECT THE GOLF CLUB
			ELSE
				IF bAllowPlayerToStealClubFromBag
					// If the player is on foot and gets close enough to the club sitting in the back, Trevor just 'yoinks' it out
					IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
					AND IS_ENTITY_ALIVE(oiGolfBag)
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF IS_ENTITY_ATTACHED_TO_ENTITY(golfClubPickup.objIndex, oiGolfBag)
								IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), golfClubPickup.objIndex, << 0.8, 0.8, 2.0 >>, FALSE, TRUE, TM_ON_FOOT)
									PLAY_SOUND_FROM_ENTITY(iClubPickupAudioID, "COLLECT_IN_BAG", golfClubPickup.objIndex, "NIGEL_1D_SOUNDSET")
									SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 128) // SHAKE PAD FOR PICKUP
									bClubCollectedByPlayer = TRUE
									eSubStage = SS_CLEANUP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_VEHICLE_WAYPOINT_TARGET_POINT(CelebrityBuggy.mVehicle) >= 200 // CELEB HAS MADE IT OUT OF GOLF COURSE INTO CITY
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
		
			
		BREAK
		
		CASE SS_CLEANUP
			
			CLEAR_PRINTS()
			
			IF bClubCollectedByPlayer
				bConversationActive = FALSE
				IF DOES_ENTITY_EXIST(golfClubPickup.objIndex)
					DELETE_OBJECT(golfClubPickup.objIndex) //DELETE THE CLUB PLAYER HAS COLLECTED
				ENDIF
				SAFE_REMOVE_BLIP(biMissionBlip)
			
				IF NOT ARE_GUARDS_DEAD() AND NOT IS_PLAYER_NEAR_SECURITY()
					bMainObjectiveDisplayed = FALSE
					SetStage(MS_LOSE_SECURITY) // LOSE THE SECURITY IF PLAYER IS NEAR THEM
				ELSE
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
						bMainObjectiveDisplayed = FALSE
						SetStage(MS_LOSE_THE_COPS) // LOSE COPS IF PLAYER HAS WANTED LEVEL
					ELSE
						SetStage(MS_END_PHONECALL) // END PHONECALL IF NOT THE ABOVE TWO STATES
					ENDIF
				ENDIF
			ELSE
				SetStage(MS_CHASE_IN_CITY) // CHASE CELEB IN CITY 
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///   Handles the player chasing the golf player in the city
///    Kicks in once waypoint recording has ended.
PROC CHASE_IN_CITY()

	SWITCH eSubStage
		CASE SS_SETUP
		
			eGolferState = GS_FLEE_IN_CITY // SET THE CELEB STATE TO FLEE IN THE CITY
			
			MAKE_PED_FLEE(CelebrityGolfer.mPed) // SET HIM TO FLEE THE PLAYER
		
			IF IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF GET_ENTITY_HEALTH(CelebrityBuggy.mVehicle) > 600
					SET_ENTITY_HEALTH(CelebrityBuggy.mVehicle,600) // LOWER VEHICLES HEALTH SO IT IS MORE LIKELY TO DROP GOLF CLUB
				ENDIF
			ENDIF
			
			// Ensure the golf bag and the pickup are both attached to the buggy, in case we've skipped and this hasn't been done properly
			IF IS_ENTITY_ALIVE(oiGolfBag)
				IF NOT IS_ENTITY_ATTACHED(oiGolfBag)
					DEBUG_PRINT("Reattaching bag to buggy!?")
					ATTACH_ENTITY_TO_ENTITY(oiGolfBag,CelebrityBuggy.mVehicle,0,<<0.2,-1.3,0.5>>,<<0,0,0>>)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
			AND IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
				IF NOT IS_ENTITY_ATTACHED(golfClubPickup.objIndex)
					DEBUG_PRINT("Reattaching club to bag!?")
					ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,oiGolfBag,0,<<0.10,0.0,-0.25>>,<<-5,155,0>>)
				ELIF IS_ENTITY_ATTACHED_TO_ENTITY(golfClubPickup.objIndex, CelebrityGolfer.mPed)
					DEBUG_PRINT("Glen still holding club, reattach to bag")
					DETACH_ENTITY(golfClubPickup.objIndex)
					ATTACH_ENTITY_TO_ENTITY(golfClubPickup.objIndex,oiGolfBag,0,<<0.10,0.0,-0.25>>,<<-5,155,0>>)
				ENDIF
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			DEBUG_PRINT("ENTERING CHASE IN CITY ")
			SAFE_REMOVE_BLIP(biMissionBlip)
			biMissionBlip = CREATE_VEHICLE_BLIP(CelebrityBuggy.mVehicle,FALSE,BLIPPRIORITY_MED)
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
		
			Monitor_Celebrity()
			DISTANCE_FAIL_CHECKS()
			UPDATE_CHASE_BLIP(biMissionBlip, CelebrityBuggy.mVehicle, LOST_RANGE)
			CHECK_STATS_GOALS()
			// CHECK_FOR_DRIVING_IN_COURSE()
			
			IF bAllowPlayerToStealClubFromBag
				// If the player is on foot and gets close enough to the club sitting in the back, Trevor just 'yoinks' it out
				IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
				AND IS_ENTITY_ALIVE(oiGolfBag)
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_ATTACHED_TO_ENTITY(golfClubPickup.objIndex, oiGolfBag)
							IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), golfClubPickup.objIndex, << 0.8, 0.8, 2.0 >>, FALSE, TRUE, TM_ON_FOOT)
								PLAY_SOUND_FROM_ENTITY(iClubPickupAudioID, "COLLECT_IN_BAG", golfClubPickup.objIndex, "NIGEL_1D_SOUNDSET")
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 128) // SHAKE PAD FOR PICKUP
								bClubCollectedByPlayer = TRUE
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF Monitor_City_Chase()
				eSubStage = SS_CLEANUP
			ENDIF
			
					
		BREAK
		
		CASE SS_CLEANUP
			
			IF bClubCollectedByPlayer
				CLEAR_PRINTS()
				bConversationActive = FALSE
				IF DOES_ENTITY_EXIST(golfClubPickup.objIndex)
					DELETE_OBJECT(golfClubPickup.objIndex) //DELETE THE CLUB PLAYER HAS COLLECTED
				ENDIF
				SAFE_REMOVE_BLIP(biMissionBlip)
			
				IF NOT ARE_GUARDS_DEAD() AND NOT IS_PLAYER_NEAR_SECURITY()
					bMainObjectiveDisplayed = FALSE
					SetStage(MS_LOSE_SECURITY) // LOSE THE SECURITY IF PLAYER IS NEAR THEM
				ELSE
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
						bMainObjectiveDisplayed = FALSE
						SetStage(MS_LOSE_THE_COPS) // LOSE COPS IF PLAYER HAS WANTED LEVEL
					ELSE
						SetStage(MS_END_PHONECALL) // END PHONECALL IF NOT THE ABOVE TWO STATES
					ENDIF
				ENDIF
			ELSE
				CLEAR_PRINTS()
				bMainObjectiveDisplayed = FALSE
				SetStage(MS_COLLECT_GOLF_CLUB) // COLLECT THE GOLF CLUB
			ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///     Handles player collecting the golf club
PROC COLLECT_GOLF_CLUB()

	SWITCH eSubStage
		CASE SS_SETUP
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF
			
			bWarnedAboutLeaving = FALSE //RESETS THIS BOOL SO IT CAN BE USED FOR LEAVING THE PICKUP
			CREATE_GOLF_PICKUP()
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex) // NEED TO CHECK THIS INCASE PLAYER BLOWS IT UP
				IF bMainObjectiveDisplayed = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
						PRINT_NOW("N1D_COLLECT",DEFAULT_GOD_TEXT_TIME,5) // Collect the ~g~golf club. //
						bMainObjectiveDisplayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOW)
			
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct) // Ensure we get the cinematic cam back now the chase has ended
			DEBUG_PRINT("ENTERING COLLECT GOLF CLUB ")			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			Monitor_Celebrity()
			//HANDLE_AMBIENT_GOLFERS()
			HANDLE_CELEB_SECURITY()
			//HANDLE_COURSE_SECURITY()
			DISTANCE_FAIL_CHECKS()
			HANDLE_CHASE_AMBIENT_GOLFERS()
			CHECK_STATS_GOALS()
			IF bDonePoliceReport = FALSE
				IF IS_PED_INSIDE_GOLF_AREA(PLAYER_PED_ID())
					HANDLE_POLICE_REPORT()
				ENDIF
			ENDIF
			
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			
			// Check this again, in case there was a conversation ongoing when we were in the setup state just now
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex)
				IF bMainObjectiveDisplayed = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
						PRINT_NOW("N1D_COLLECT",DEFAULT_GOD_TEXT_TIME,5) // Collect the ~g~golf club. //
						bMainObjectiveDisplayed = TRUE
					ENDIF
				ENDIF
			ENDIF

			//BAG WILL DETACH FROM GOLF BUGGY IF IT GETS DESTROYED
			IF NOT IS_VEHICLE_OK(CelebrityBuggy.mVehicle)
				IF IS_ENTITY_ALIVE(oiGolfBag)
					IF IS_ENTITY_ATTACHED(oiGolfBag)
						DETACH_ENTITY(oiGolfBag)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(golfClubPickup.objIndex) // GOLF CLUB IS ALIVE
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), golfClubPickup.objIndex, << 0.8, 0.8, 2.0 >>, FALSE, TRUE, TM_ON_FOOT) // IF PLAYER IS AT GOLF CLUB THEN IT IS COLLECTED
					IF IS_ENTITY_ATTACHED_TO_ANY_OBJECT(golfClubPickup.objIndex)
						DEBUG_PRINT("Golf club attached to bag - correct?")
						PLAY_SOUND_FROM_ENTITY(iClubPickupAudioID, "COLLECT_IN_BAG", golfClubPickup.objIndex, "NIGEL_1D_SOUNDSET")
						// Play sound "COLLECT_IN_BAG"
					ELSE
						DEBUG_PRINT("Golf club NOT attached to bag - correct?")
						PLAY_SOUND_FROM_ENTITY(iClubPickupAudioID, "COLLECT_OUT_BAG", golfClubPickup.objIndex, "NIGEL_1D_SOUNDSET")
						// Play sound "COLLECT_OUT_BAG"
					ENDIF
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 128) // SHAKE PAD FOR PICKUP
					bClubCollectedByPlayer = TRUE
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					eSubStage = SS_CLEANUP
				ELSE
//					IF IS_ENTITY_ATTACHED(golfClubPickup.objIndex)
//						RENDER_FAKE_PICKUP_GLOW((GET_ENTITY_COORDS(golfClubPickup.objIndex)+ <<0,0,1.25>>), 5) // IF PLAYER HAS NOT PICKED UP, RENDER PICKUP GLOW FROM CLUB
//					ELSE
//						RENDER_FAKE_PICKUP_GLOW((GET_ENTITY_COORDS(golfClubPickup.objIndex)+ <<0,0,0.15>>), 5) // IF PLAYER HAS NOT PICKED UP, RENDER PICKUP GLOW FROM CLUB
//					ENDIF
					
				ENDIF
			ELSE //CLUB HAS BEEN DESTROYED
				eSubStage = SS_CLEANUP
			ENDIF
	
		BREAK
		
		CASE SS_CLEANUP
			
			IF bClubCollectedByPlayer = TRUE //PLAYER COLLECTED THE CLUB TO GET INTO HERE
				CLEAR_PRINTS()
				bConversationActive = FALSE
				IF DOES_ENTITY_EXIST(golfClubPickup.objIndex)
					DELETE_OBJECT(golfClubPickup.objIndex) //DELETE THE CLUB PLAYER HAS COLLECTED
				ENDIF
				SAFE_REMOVE_BLIP(biMissionBlip)
			
				IF NOT ARE_GUARDS_DEAD() AND NOT IS_PLAYER_NEAR_SECURITY()
					bMainObjectiveDisplayed = FALSE
					SetStage(MS_LOSE_SECURITY) // LOSE THE SECURITY IF PLAYER IS NEAR THEM
				ELSE
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
						bMainObjectiveDisplayed = FALSE
						SetStage(MS_LOSE_THE_COPS) // LOSE COPS IF PLAYER HAS WANTED LEVEL
					ELSE
						SetStage(MS_END_PHONECALL) // END PHONECALL IF NOT THE ABOVE TWO STATES
					ENDIF
				ENDIF
			ELSE //PLAYER DID NOT COLLECT THE CLUB
				CLEAR_PRINTS()
				bConversationActive = FALSE
				IF DOES_ENTITY_EXIST(golfClubPickup.objIndex)
					DELETE_OBJECT(golfClubPickup.objIndex)	//DELETE CLUB IT WAS DESTROYED
				ENDIF
				SAFE_REMOVE_BLIP(biMissionBlip)
				SET_FAIL_REASON(FAIL_CLUB_DESTROYED) //PLAYER DESTROYED THE CLUB
			ENDIF
		
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///  Handles losing the security if player is near them once he has the golf pickup
PROC LOSE_SECURITY()

	SWITCH eSubStage
		CASE SS_SETUP
			
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("N1D_LOSESEC",DEFAULT_GOD_TEXT_TIME,5) // Escape the ~r~security //
				bMainObjectiveDisplayed = TRUE
			ENDIF
			
			INT i
			FOR i = 0 TO NUM_OF_SECURITY - 1 //BLIP ALL THE SECURITY FOR CELEBRITY
				IF DOES_BLIP_EXIST(CelebSecurity[i].mBlip)
					SET_BLIP_SCALE(CelebSecurity[i].mBlip,0.7)
				ENDIF
			ENDFOR
			
			DEBUG_PRINT("ENTERING LOSE THE SECURITY ")			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			
			IF NOT ARE_GUARDS_DEAD() AND NOT IS_PLAYER_NEAR_SECURITY()
				HANDLE_CELEB_SECURITY()
			ELSE
				eSubStage = SS_CLEANUP // IF PLAYER HAS KILLED SECURITY OR EVADED THEM
			ENDIF
			
			Monitor_Celebrity()
			//HANDLE_AMBIENT_GOLFERS()
			//HANDLE_COURSE_SECURITY()
			HANDLE_CHASE_AMBIENT_GOLFERS()
			CHECK_STATS_GOALS()
			
		BREAK
		
		CASE SS_CLEANUP

			FOR i = 0 TO NUM_OF_SECURITY - 1
				MAKE_PED_FLEE(CelebSecurity[i].mPed)
				SAFE_REMOVE_BLIP(CelebSecurity[i].mBlip) //REMOVE SECURITY BLIPS IF VALID
			ENDFOR
			CHECK_STATS_GOALS() // Check this again - the bImDead flags should all be set now if the player killed all the guards
			CLEAR_PRINTS()
			bConversationActive = FALSE
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
				bMainObjectiveDisplayed = FALSE
				SetStage(MS_LOSE_THE_COPS) // LOSE COPS IF PLAYER HAS WANTED LEVEL
			ELSE
				SetStage(MS_END_PHONECALL) // ENDPHONE CALL IF PLAYER IS NOT WANTED
			ENDIF
		
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Main mission function for losing the cops if a wanted level is gained
PROC LOSE_THE_COPS()

	SWITCH eSubStage
		CASE SS_SETUP
			
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("N1D_WANTED",DEFAULT_GOD_TEXT_TIME,5) // Lose the cops. //
				bMainObjectiveDisplayed = TRUE
			ENDIF
			
			INT i
			
			FOR i = 0 TO NUM_OF_SECURITY - 1
				MAKE_PED_FLEE(CelebSecurity[i].mPed)
				SAFE_REMOVE_BLIP(CelebSecurity[i].mBlip)
			ENDFOR
			
			DEBUG_PRINT("ENTERING LOSE THE COPS")			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			Monitor_Celebrity()
			//HANDLE_AMBIENT_GOLFERS()
			//HANDLE_COURSE_SECURITY()
			HANDLE_CELEB_SECURITY() // Keep updating their states if they're dead
			HANDLE_CHASE_AMBIENT_GOLFERS()
			CHECK_STATS_GOALS()
			
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0) // PLAYER NEEDS TO LOSE THE COPS
				eSubStage = SS_CLEANUP
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP

			CLEAR_PRINTS()
			bConversationActive = FALSE
			SetStage(MS_END_PHONECALL)	// ENDPHONE CALL OF MISSION
		
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Plays the phone call at the end of the mission when player has the golf club
PROC END_PHONECALL()

	SWITCH eSubStage	
		CASE SS_SETUP
			
			DEBUG_PRINT("ENTERING END PHONECALL")	
			
			iNigelCallDelayTimer = GET_GAME_TIMER()
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				
				//REMOVE SECURITY BLIPS AND MAKE PEDS FLEE INCASE
				INT i
				
				FOR i = 0 TO NUM_OF_SECURITY - 1
					MAKE_PED_FLEE(CelebSecurity[i].mPed)
					SAFE_REMOVE_BLIP(CelebSecurity[i].mBlip)
				ENDFOR
				//
				
				SAFE_REMOVE_BLIP(biMissionBlip)
				
				//CLEAR PEDS AND ADD THEM AGAIN TO ENSURE PHONECALL WORKS
				REMOVE_PED_FOR_DIALOGUE(sDialogue, 2)
				REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)
				REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)
				ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(sDialogue, 3, NULL, "NIGEL")
				ADD_PED_FOR_DIALOGUE(sDialogue, 4, NULL, "MRSTHORNHILL")
				//
				
				eSubStage = SS_UPDATE
				
				DEBUG_PRINT("ENTERING END PHONECALL LOOP")
				
			ENDIF
			
		BREAK
		CASE SS_UPDATE	
			
			Monitor_Celebrity() // Carry on monitoring the Celeb so he runs away appropriately, etc
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				IF NOT bConversationActive
					IF (GET_GAME_TIMER() - iNigelCallDelayTimer) > 3000
						IF PLAYER_CALL_CHAR_CELLPHONE(sDialogue, CHAR_NIGEL, "NIG1DAU", "NIG1D_OUTRO", CONV_PRIORITY_VERY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOW)
							bConversationActive = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF HAS_CELLPHONE_CALL_FINISHED()
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
		
			DEBUG_PRINT("CLEANUP END PHONECALL")
		
			CLEAR_PRINTS()
			Script_Passed()	// MISSION COMPLETE
			
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///  Waits for the screen to fade out, then updates the fail reason for the mission    
PROC FAIL_WAIT_FOR_FADE()
	
	SWITCH eSubStage
	
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			// remove blips
			SAFE_REMOVE_BLIP(biMissionBlip)

			STRING sFailReason
			
			SWITCH eFailReason // print fail reason
			
				CASE FAIL_DEFAULT // no fail reason to display
				BREAK
				
				CASE FAIL_LION_ESCAPED
					sFailReason = "N1D_ESCAPE"				//~r~Lion Oaks escaped. 
				BREAK
				
				CASE FAIL_DID_NOT_LOCATE_LION
					sFailReason = "N1D_NOLOCATE"			//~r~Failed to locate Lion Oaks.
				BREAK
				
				CASE FAIL_CLUB_DESTROYED
					sFailReason = "N1D_CLUBDES"			//~r~Failed to locate Lion Oaks.
				BREAK

				CASE FAIL_LOST_GOLF_CLUB
					sFailReason = "N1D_CLUBLOST"			//~r~The golf club was lost.
				BREAK
			ENDSWITCH
			
			IF eFailReason = FAIL_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				// MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< -1327.8684, -92.8358, 48.5237 >>, 345.2175)
				// SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-1324.834, -90.740, 48.783>>, -0.454195)
				
				DELETE_EVERYTHING()				
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					KILL_ANY_CONVERSATION()
				ENDIF
				MissionCleanup()
				Script_Cleanup(FALSE)
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
	//OR IS_REPEAT_PLAY_ACTIVE()
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_1D(sRCLauncherDataLocal, tpGolfCourse)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
		DEBUG_PRINT("Set up script for replay")
	ENDIF
	
	// Set script indices to launcher-generated ones
	CelebrityGolfer.mPed = sRCLauncherDataLocal.pedID[0]
	CelebrityFriend.mPed = sRCLauncherDataLocal.pedID[1]
	CelebSecurity[CELEBSEC_WEST].mPed = sRCLauncherDataLocal.pedID[2]
	CelebSecurity[CELEBSEC_EAST].mPed = sRCLauncherDataLocal.pedID[3]
	CelebSecurity[CELEBSEC_NORTH].mPed = sRCLauncherDataLocal.pedID[4]
	
	golfClubPickup.objIndex = sRCLauncherDataLocal.objID[0]
	CelebFriendClub.mObject = sRCLauncherDataLocal.objID[1]
	oiGolfBag = sRCLauncherDataLocal.objID[2]
	
	CelebrityBuggy.mVehicle = sRCLauncherDataLocal.vehID[0]
	GolfBuggieTransport[BUGGY_BEHINDCELEB].mVehicle = sRCLauncherDataLocal.vehID[1]
	GolfBuggieTransport[BUGGY_NEXTTOCELEB].mVehicle = sRCLauncherDataLocal.vehID[2]
	
	ADD_VEHICLE_UPSIDEDOWN_CHECK(CelebrityBuggy.mVehicle)
	
	bCelebrityCreated = TRUE
	bSecurityCreated = TRUE
	
	iHeadshotTimer = GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup= START_WIDGET_GROUP("Nigel 1D widgets")
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)        
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	CLEAR_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLD_KILLED_MARK))	// ensure this bit is initially clear when starting the mission
	
	InitVariables() // initialise everything
	
	int i
	for i = 0 TO COUNT_OF(CelebSecurity)-1
		IF IS_ENTITY_ALIVE(CelebSecurity[i].mPed)
			SET_PED_RELATIONSHIP_GROUP_HASH(CelebSecurity[i].mPed,relHatePlayer)
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(CelebSecurity[i].mPed,FALSE, relHatePlayer)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relHatePlayer,RELGROUPHASH_PLAYER)
		ENDIF
	ENDFOR
	
	IF IS_NEXT_WEATHER_TYPE("THUNDER")
	OR IS_PREV_WEATHER_TYPE("THUNDER")
		SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST", 25.0)
	ENDIF

	// handle replay checkpoints
	IF Is_Replay_In_Progress()
	OR IS_REPEAT_PLAY_ACTIVE()
		
		INT iReplayStage
		
		// If it's repeat play, start from the beginning of the mission, otherwise start at the checkpoint
		IF IS_REPEAT_PLAY_ACTIVE()
		AND NOT Is_Replay_In_Progress() // You can replay repeat plays...
			iReplayStage = CP_AFTER_MOCAP
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1291.5958, -12.9214, 48.5137>>, 294.7073)
		ELSE
			iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		ENDIF
		
		DEBUG_PRINT_INT("Replay stage grabbed: ", iReplayStage)
		
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF
		
		SWITCH iReplayStage
		
			CASE CP_AFTER_MOCAP
				DEBUG_PRINT_INT("Replay skipping to stage ", Z_SKIP_AFTER_MOCAP)
				// Move player so skip works correctly if player dies inside course
				START_REPLAY_SETUP(<<-1291.5958, -12.9214, 48.5137>>, 294.7073)
				Do_Z_Skip(Z_SKIP_AFTER_MOCAP)	 // skip the mocap intro
			BREAK
			
			CASE CP_LOCATED_STANKY
				DEBUG_PRINT_INT("Replay skipping to stage ", Z_SKIP_LOCATED_STANKY)
				START_REPLAY_SETUP(<<-1190.22, 36.83, 52.03>>, 299.0)
				Do_Z_Skip(Z_SKIP_LOCATED_STANKY)	 // skip to get golf club
			BREAK
			
			CASE CP_COMPLETE
				DEBUG_PRINT_INT("Replay skipping to stage ", Z_SKIP_ENDPHONECALL)
				START_REPLAY_SETUP(<<-1324.8539, 60.2491, 52.5400>>, 94.6913)
				Do_Z_Skip(Z_SKIP_ENDPHONECALL)	 // skip to end of mission phonecall
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
            BREAK
		ENDSWITCH
	ENDIF
				
	WHILE(TRUE) // Loop within here until the mission passes or fails
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_VSM")
		
		WAIT(0)

		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Check for the player getting into a Caddy to add it to the audio mix
		//HANDLE_CADDY_AUDIO_MIX()
		
		// Because the stat system isn't ready as soon as the mission starts, wait a couple of seconds and then try adding Mark to headshot watch
		IF IS_ENTITY_ALIVE(CelebrityGolfer.mPed)
			IF (GET_GAME_TIMER() - iHeadshotTimer) > 2000
				IF NOT bHasAddedHeadshot
					INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(CelebrityGolfer.mPed)
					DEBUG_PRINT("Added Mark to headshot watch")
					bHasAddedHeadshot = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF eMissionStage = MS_MISSION_FAILED
			FAIL_WAIT_FOR_FADE()
		ELSE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SWITCH eMissionStage // MAIN LOOP
					
					//MAIN STATES
					CASE MS_INTRO_PHONECALL
						INTRO_PHONECALL()
					BREAK
					
					CASE MS_GO_TO_THE_GOLF_CLUB
						GO_TO_THE_GOLF_CLUB()
					BREAK
					
					CASE MS_GO_TO_CELEBRITY
						GO_TO_CELEBRITY()
					BREAK
					
					CASE MS_GET_THE_CLUB
						GET_THE_CLUB()
					BREAK
					
					CASE MS_CHASE_THE_GOLFER
						CHASE_THE_GOLFER()
					BREAK
					
					CASE MS_CHASE_IN_CITY
						CHASE_IN_CITY()
					BREAK
					
					CASE MS_COLLECT_GOLF_CLUB
						COLLECT_GOLF_CLUB()
					BREAK
					
					CASE MS_LOSE_SECURITY
						LOSE_SECURITY()
					BREAK
					
					CASE MS_LOSE_THE_COPS
						LOSE_THE_COPS()
					BREAK
					
					CASE MS_END_PHONECALL
						END_PHONECALL()
					BREAK

					//RETURN STATES

				ENDSWITCH
			ELSE
				SET_FAIL_REASON(FAIL_DEFAULT)
			ENDIF
			
			if bFinishedSkipping = FALSE
				JumpToStage(eTargetStage) // still skipping stages
			ENDIF
		
			#IF IS_DEBUG_BUILD // STAGE SKIPPING
				if bFinishedSkipping = TRUE
					DEBUG_Check_Debug_Keys() // not skipping stages, check for debug keys
				ENDIF
			#ENDIF
		ENDIF
	ENDWHILE

ENDSCRIPT

