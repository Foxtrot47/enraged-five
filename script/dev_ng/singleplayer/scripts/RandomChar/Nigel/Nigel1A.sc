// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Nigel1A.sc
//		AUTHOR			:	Ahron Mason
//		DESCRIPTION		:	Trevor steals gold tooth from musician for Nigel and Mrs Thornhill.
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "event_public.sch"
USING "initial_scenes_Nigel.sch"
USING "rgeneral_include.sch"
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:ENUMS:
//-------------------------------------------------------------------------------------------------------------------------------------------------

// Mission stages
ENUM MISSION_STAGE
	MISSION_STAGE_ENTER_THE_MUSIC_CLUB,
	MISSION_STAGE_FIND_CELEB,
	MISSION_STAGE_KNOCK_OUT_TOOTH,
	MISSION_STAGE_COLLECT_TOOTH,
	MISSION_STAGE_LEAVE_THE_AREA,
	MISSION_STAGE_OUTRO_PHONECALL,
	MISSION_STAGE_MISSION_PASSED,
	//Additional stages
	MISSION_STAGE_LOSE_THE_COPS,
	MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE,
	//debug stages
	MISSION_STAGE_DEBUG_SETUP
ENDENUM

/// each mission stage has uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

//fail conditions
ENUM FAILED_REASON_ENUM
	FAILED_DEFAULT,
	FAILED_MUSICIAN_KILLED
ENDENUM

//used to update mission peds behaviour
ENUM N1A_PED_AI
	N1A_PED_AI_SETUP_RELAXED,
	N1A_PED_AI_STATE_RELAXED,
	N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL,
	N1A_PED_AI_SETUP_ROUTINE,
	N1A_PED_AI_STATE_ROUTINE,
	N1A_PED_AI_SETUP_AWARE_OF_PLAYER,
	N1A_PED_AI_STATE_AWARE_OF_PLAYER,
	N1A_PED_AI_SETUP_MOVE_TOWARDS_PLAYER,
	N1A_PED_AI_STATE_MOVE_TOWARDS_PLAYER,
	N1A_PED_AI_SETUP_MELEE_ATTACK,
	N1A_PED_AI_STATE_MELEE_ATTACK,
	N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP,
	N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP,
	N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS,
	N1A_PED_AI_STATE_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS,
	N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS,
	N1A_PED_AI_SETUP_REACHED_PERIMETER,
	N1A_PED_AI_STATE_REACHED_PERIMETER,
	N1A_PED_AI_SETUP_OUT_OF_REACH,
	N1A_PED_AI_STATE_OUT_OF_REACH,
	N1A_PED_AI_SETUP_FLEE,
	N1A_PED_AI_STATE_FLEE,
	N1A_PED_AI_SETUP_FLEE_TO_VEHICLE,
	N1A_PED_AI_STATE_FLEE_TO_VEHICLE,
	N1A_PED_AI_SETUP_FLEE_IN_VEHICLE,
	N1A_PED_AI_STATE_FLEE_IN_VEHICLE,
	N1A_PED_AI_SETUP_SURRENDERED,
	N1A_PED_AI_STATE_SURRENDERED,
	N1A_PED_AI_SETUP_FRIGHTENED,
	N1A_PED_AI_STATE_FRIGHTENED,
	N1A_PED_AI_SETUP_AIM_GUN_AND_ADVANCE,
	N1A_PED_AI_STATE_AIM_GUN_AND_ADVANCE,
	N1A_PED_AI_SETUP_GUN_COMBAT,
	N1A_PED_AI_STATE_GUN_COMBAT
ENDENUM

//used to update the state of the musician
ENUM N1A_MUSICIAN_STATE
	N1A_MUSICIAN_STATE_RELAXED,
	N1A_MUSICIAN_STATE_BRAWLING,
	N1A_MUSICIAN_STATE_SURRENDERED,
	N1A_MUSICIAN_STATE_KNOCKED_DOWN,
	N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT,
	N1A_MUSICIAN_STATE_FLEE,
	N1A_MUSICIAN_STATE_DEAD
ENDENUM

/// PURPOSE: the different rooms within the rock club interior
ENUM N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM
	N1A_RCR_MAIN_ROOM = 0,
	N1A_RCR_MAIN_ENTRANCE,
	N1A_RCR_MAIN_ENTRANCE_TO_MAIN_ROOM,
	N1A_RCR_BACK_ROOM_CORRIDOR_TO_BASEMENT,
	N1A_RCR_LANDING_OUTSIDE_BASEMENT,
	N1A_RCR_BASEMENT,
	N1A_RCR_CLOAK_ROOM,
	N1A_RCR_CORRIDOR_TO_UPSTAIRS,
	N1A_RCR_LANDING_UPSTAIRS,
	N1A_RCR_ROOM_UPSTAIRS,
	N1A_RCR_REAR_ENTRANCE_BOTTOM_STAIRS,
	N1A_RCR_REAR_ENTRANCE_TOP_STAIRS,
	
	N1A_RCR_INVALID,	
	N1A_MAX_NUM_ROCK_CLUB_ROOMS
ENDENUM

/// PURPOSE: used to update a ped's movement state - for use with detection system
ENUM N1A_PED_MOVEMENT_STATE
	N1A_PEDMOVEMENTSTATE_COVER,
	N1A_PEDMOVEMENTSTATE_STEALTH,
	N1A_PEDMOVEMENTSTATE_RUNNING,
	N1A_PEDMOVEMENTSTATE_SPRINTING,
	N1A_PEDMOVEMENTSTATE_WALKING,
	N1A_PEDMOVEMENTSTATE_IN_VEHICLE,
	N1A_PEDMOVEMENTSTATE_DEFAULT
ENDENUM

/// PURPOSE: contains all of the door's which I need to control in script
ENUM N1A_MISSION_CONTROLLED_DOORS_ENUM
	N1A_MCD_INT_DD_TO_MAIN_ROOM_01 = 0,
	N1A_MCD_INT_DD_TO_MAIN_ROOM_02,
	N1A_MCD_INT_ENTRANCE_STAFF_AREA_01,	
	N1A_MCD_INT_TO_REAR_ENTRANCE_01,
	N1A_MCD_INT_TO_BASEMENT_01,
	N1A_MCD_INT_BASEMENT_01,
	N1A_MAX_MISSION_CONTROLLED_DOORS
ENDENUM

/// PURPOSE: list of animations the club peds have
ENUM N1A_CLUB_PED_ANIM_TYPE
	N1A_CPAT_BASE_ANIM,
	N1A_CPAT_IDLE_A_ANIM,
	N1A_CPAT_IDLE_B_ANIM,
	N1A_CPAT_BUMP_ANIM,
	N1A_CPAT_OUTRO_ANIM,
	N1A_MAX_CLUB_PED_ANIM_TYPE
ENDENUM

/// PURPOSE: list of animations Willy and the groupie
ENUM N1A_Willy_GROUPIE_ANIM_TYPE
	N1A_WGAT_BASE_ANIM,
	N1A_WGAT_BASE_2_ANIM,
	N1A_WGAT_THIS_IS_AWKWARD,
	N1A_WGAT_YOU_KNOW,
	N1A_WGAT_EXIT,
	N1A_MAX_Willy_GROUPIE_ANIM_TYPE
ENDENUM

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:STRUCTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: used by scripted peds in the mission - musician
///    AI uses N1A_PED_AI to set behaviours
STRUCT STRUCT_MISSION_PED
	AI_BLIP_STRUCT 	blipAIStruct
	BLIP_INDEX		blipIndex
	PED_INDEX 		pedIndex
	N1A_PED_AI		AI
	MODEL_NAMES		mnModel
	VECTOR			vPosition
	FLOAT			fHeading
	INT				iTimer
	INT				iAiDelayTimer
	INT				iFrameCountLastSeenPlayer
ENDSTRUCT

/// PURPOSE: used by scripted peds
STRUCT STRUCT_SIMPLE_MISSION_PED
	PED_INDEX 		pedIndex
	MODEL_NAMES		mnModel
	VECTOR			vPosition
	FLOAT			fHeading
ENDSTRUCT

/// PURPOSE: used by the pickups in the mission
STRUCT STRUCT_MISSION_PICKUP
	PICKUP_INDEX	index
	MODEL_NAMES		mnModel
	BLIP_INDEX		blipIndex
	VECTOR 			vPosition
	PICKUP_TYPE 	type = PICKUP_CUSTOM_SCRIPT
	INT 			iPlacementFlags
	//VECTOR 			vRotation	//not currently in use
	//FLOAT 			fHeading
ENDSTRUCT

/// PURPOSE: used by scripted vehicles in the mission - musician's car
STRUCT STRUCT_MISSION_VEHICLE
	VEHICLE_INDEX 	vehIndex
	MODEL_NAMES		mnModel
	VECTOR			vPosition
	FLOAT			fHeading
ENDSTRUCT

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:CONSTANTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

CONST_FLOAT FLEE_PED_MOVE_RATE_OVERRIDE_SPEED_UP							1.15		// musician flee speed, faster than player so he has more chance of escape
CONST_FLOAT DIST_PLAYER_CLASSED_AS_COLLECTED_TOOTH_PICKUP					0.75			// used to fake the tooth object acting like a pickup
CONST_FLOAT Z_DIFFERENCE_COMBAT_TASK_STOPS_WORKING							0.25		// height diff that combat breaks - detect for this and change the peds AI
CONST_INT DELAY_FOR_MISSION_PASSED											2500		// don't pass the mission straight after the outro phonecall
CONST_INT TIME_DELAY_FOR_OUTRO_PHONECALL									2500		// don't have the player call Nigel straight away
CONST_INT TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE							2500		// dialogue delay time
CONST_INT CP_NIGEL1A_ENTER_THE_MUSIC_CLUB									0			// Initial mission replay checkpoint Note: this doesn't get set by the mission, but is used to skip over the phonecall
CONST_INT CP_NIGEL1A_LEAVE_THE_AREA											1			// 1st mission replay checkpoint
CONST_INT CP_NIGEL1A_MISSION_PASSED											2			// 2nd mission replay checkpoint - used by shitskip
CONST_INT Z_SKIP_ENTER_THE_MUSIC_CLUB										0			// z skip stage
CONST_INT Z_SKIP_FIND_CELEB													1			// z skip stage
CONST_INT Z_SKIP_KNOCK_OUT_TOOTH											2			// z skip stage
CONST_INT Z_SKIP_COLLECT_TOOTH												3			// z skip stage
CONST_INT Z_SKIP_LEAVE_THE_AREA												4			// z skip stage
CONST_INT Z_SKIP_OUTRO_PHONECALL											5			// z skip stage
CONST_INT Z_SKIP_MISSION_PASSED												6
CONST_INT TOTAL_PLAYER_LEFT_MUSICIAN_BRAWL_DIALOGUE							3			// defines max convos/lines for specified dialogue 
CONST_INT TOTAL_PLAYER_RETURNED_MUSICIAN_BRAWL_DIALOGUE						3			// defines max convos/lines for specified dialogue 
CONST_INT TOTAL_PLAYER_OUT_OF_REACH_MUSICIAN_BRAWL_DIALOGUE					3			// defines max convos/lines for specified dialogue 
CONST_INT CLUB_PED_GROUPIE_UPSTAIRS_01										0			// array index ID for groupie upstairs 2
CONST_INT CLUB_PED_GROUPIE_UPSTAIRS_02										1			// array index ID for groupie upstairs 1
CONST_INT CLUB_PED_ROADIE													2			// array index ID for roadie 1
CONST_INT CLUB_PED_BAND_MANAGER												3			// array index ID for manager
CONST_INT GROUPIE_WILLYS_PIECE												4			// array index ID for Willy's bird
CONST_INT MAX_NUM_CLUB_PEDS													5			// defines max number of club peds in array
CONST_INT CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD							10			// grace period in frame count for can ped see ped tests
CONST_INT NIG1A_PLAYER_LEFT_THE_CLUB_DIST									60			// dist player has to reach from the club to be classed as left the area
CONST_INT NIG1A_PLAYER_CLASSED_AS_PURSUING_Willy_DIST						50			// if player is with in this dist from Willy, he's classed as pursuing him in the scripted wanted level checks
CONST_INT NIG1A_TIME_DELAY_WANTED_FOR_WEAPON_IN_CLUB						10000		// if player spotted with weapon in the club - gets scripte wanted level after this delay
CONST_INT NIG1A_TIME_DELAY_WANTED_FOR_WILLY_ASSAULT							5000		// if player assault's Willy - gets scripte wanted level after this delay

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:VARIABLES
//-------------------------------------------------------------------------------------------------------------------------------------------------

MISSION_STAGE eMissionStage	= MISSION_STAGE_ENTER_THE_MUSIC_CLUB	// MISSION_STAGE_DEBUG_SETUP //	MISSION_STAGE_ENTER_THE_MUSIC_CLUB //SHOULD THESE GET INITIALISED IN INIT_VARIABLES INSTEAD, SO THEY GET REST WHEN Z SKIPPING ETC?
MISSION_STAGE eMissionSkipTargetStage							 // Used in mission checkpoint setup and debug stage skipping
SUB_STAGE eSubStage = SS_SETUP									 // SHOULD THESE GET INITIALISED IN INIT_VARIABLES INSTEAD, SO THEY GET REST WHEN Z SKIPPING ETC?
g_structRCScriptArgs sRCLauncherDataLocal
#IF IS_DEBUG_BUILD	// stage skipping
	CONST_INT MAX_SKIP_MENU_LENGTH 7
    MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF
BLIP_INDEX blipMusicClub
BOOL bDoneObjective_LoseWantedLevel
BOOL bDoneDialogue_BandSayWhereWillyIs
BOOL bDoneDialogue_MusicianBeginFlee
BOOL bDoneDialogue_MusicianKnockedDown
BOOL bDoneDialogue_MusicianLosesTooth
BOOL bDoneDialogue_MusicianSpottedPlayer
BOOL bDoneDialogue_MusicianStartBrawl
BOOL bDoneDialogue_MusicianSurrendered
BOOL bDoneDialogue_MusicianShotAtByPlayer
BOOL bDoneDialogue_PlayerReturnsTofightMusician
BOOL bDoneDialogue_TrevorCommentSneakingInBackdoor
BOOL bDoneDialogue_TrevorKilledWilly
BOOL bDoneDialogue_TrevorLeaveTheAreaComment
BOOL bDoneDialogue_MusicianShoutSecurity
BOOL bDonedialogue_ClubPedsReactThreatened
BOOL bDoneObjective_BeatUpMusician
BOOL bDoneObjective_CollectTooth
BOOL bDoneObjective_LeaveTheArea
BOOL bDoneObjective_EnterClub
BOOL bFinishedStageSkipping	= TRUE	//used to determine if we are mission replay checkpoint skipping or debug skipping
BOOL bLoadedWorldForStageSkipping = FALSE	// flag to say if we haveloaded the world around the player when stage skipping
BOOL bHasPlayerDamagedMusician
BOOL bHasPlayerScarredClubPedsUpstairs
BOOL bHasOutroPhonecallSucceeded
BOOL bHasWillySeenPlayerAttackHisGroupie
BOOL bPausedDialogue_MusicianSpottedPlayer 
BOOL bPausedDialogue_AmbientUpstairs
BOOL bPausedDialogue_AmbientWillyGroupie
BOOL bPlayerUsedWeaponToDamageWilly
BOOL bHasPlayerReceivedScriptedWantedLevel
BOOL bShouldPlayerGetWantedLevelForWeaponInClub
BOOL bShouldPlayerGetWantedLevelAfterAttackingWilly
BOOL bSetMissionControlledDoorsInitialStates[N1A_MAX_MISSION_CONTROLLED_DOORS]
BOOL bSetExitSceneForWillyAndGroupie
FAILED_REASON_ENUM eN1A_MissionFailedReason
INT iClubPedID_ToUpdateThisFrameCounter
INT iDialogueCounter_PlayerLeftMusicianInBrawl
INT iDialogueCounter_PlayerOutOfReachMusicianInBrawl
INT iDialogueCounter_PlayerReturnedMusicianInBrawl
INT iMissionControlledDoors[N1A_MAX_MISSION_CONTROLLED_DOORS]
INT iRockClubRoom[N1A_MAX_NUM_ROCK_CLUB_ROOMS]
INT iTimer_DelayForOutroPhonecall
INT iTimer_BackupKnockOutToothTrigger
INT iTimer_PlayerReceivesScriptedWantedLevel
INT iCounter_AmbientDialogueUpstairs
INT iCounter_AmbientDialogueWillyGroupie
INT iSyncedSceneID_ClubPedUpstairsScenes[N1A_MAX_CLUB_PED_ANIM_TYPE]
INT iSyncedSceneID_WillyGroupieScenes[N1A_MAX_Willy_GROUPIE_ANIM_TYPE]
//INT iUpdateClubPeds_ThisFrameCounter
INTERIOR_INSTANCE_INDEX RockClubInteriorIndex
FLOAT fHeading_PlayerMissionStart					// heading player gets set to when skipping back to the start of the the mission
N1A_MUSICIAN_STATE eMusicianState
N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM ePlayerInsideClubStatus
REL_GROUP_HASH relGroupPlayerEnemy //for hate relationship
REL_GROUP_HASH relGroupUnagressive
SCENARIO_BLOCKING_INDEX scenarioBlockingAreaRockClub
SEQUENCE_INDEX sequenceAI 
STRING sNigel1A_DialogueRoot_MusicianSpotsTrevor = "NULL"
STRING sNigel1A_Dialogue_TextBlockName = "NIG1AAU"
structPedsForConversation sDialogue
STRUCT_MISSION_PED sClubPed[MAX_NUM_CLUB_PEDS]
STRUCT_MISSION_PED sWillyPed
STRUCT_MISSION_PICKUP mpToothPickup
STRUCT_MISSION_VEHICLE sMusicianVehicle
TEXT_LABEL_23 tlPausedDialogue_MusicianSpottedPlayer = ""
TEXT_LABEL_23 tlDialogueLinePaused_UpstairsAmbient = ""
TEXT_LABEL_23 tlDialogueLinePaused_WillyGroupieAmbient = ""
TEXT_LABEL_31 tlAnimDict_ClubPedsUpstairs = "rcmnigel1a_band_groupies"
TEXT_LABEL_31 tlAnimDict_WillyMocappedAnims = "rcmnigel1aig_1"
TEXT_LABEL_31 tlAnimDict_WillyMocappedHurtAnims = "rcmnigel1aig_3"
VECTOR vClearZone_RockClub_Max
VECTOR vClearZone_RockClub_Min
VECTOR vMusicClub_CentrePos
VECTOR vPlayerPos
VECTOR vSyncedScenePosition_ClubPedsUpstairs
VECTOR vSyncedSceneRotation_ClubPedsUpstairs
VECTOR vSyncedScenePosition_WillyGroupie
VECTOR vSyncedSceneRotation_WillyGroupie
VECTOR vPos_PlayerMissionStart						// pos player gets warped to when skipping back to the start of the the mission
VEHICLE_INDEX vehIndex_MissionReplayRestore

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:DEBUG FUNCS / PROCS / WIDGETS
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintMissionInfoToTTY					= TRUE	// needs to be FALSE for submit!!
	BOOL bDebug_PrintPlayerRoomKey						= FALSE
	BOOL bDebug_PrintPlayerExclusionAreacheckMainRoom 	= FALSE
	BOOL bDebug_DoPlayerStartedBrawlChecks 				= FALSE	
	BOOL bDebug_DoShapeTestPlayertoMusician				= FALSE
	BOOL bDebug_PrintCurrentMusicianState				= FALSE
	/// PURPOSE:
	/// My debug mission widget groups, which get created in RAG->SCRIPT
	PROC SETUP_MISSION_WIDGET()
		widgetGroup = START_WIDGET_GROUP("NIGEL 1A WIDGETS")	
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintMissionInfoToTTY)	
			ADD_WIDGET_BOOL("bDebug_PrintPlayerRoomKey", bDebug_PrintPlayerRoomKey)
			ADD_WIDGET_BOOL("bDebug_PrintPlayerExclusionAreacheckMainRoom", bDebug_PrintPlayerExclusionAreacheckMainRoom)
			ADD_WIDGET_BOOL("Do Start Brawling with Musician checks", bDebug_DoPlayerStartedBrawlChecks)
			ADD_WIDGET_BOOL("bDebug_DoShapeTestPlayertoMusician", bDebug_DoShapeTestPlayertoMusician)
			ADD_WIDGET_BOOL("bDebug_PrintCurrentMusicianState : ", bDebug_PrintCurrentMusicianState)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	/// PURPOSE:
	/// updates my mission widgets, based off RAG input
	PROC MAINTAIN_MISSION_WIDGETS()
		IF bDebug_PrintPlayerRoomKey
			INT iRoomKey
			iRoomKey = GET_KEY_FOR_ENTITY_IN_ROOM(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG ** ", " bDebug_PrintPlayerRoomKey = ", iRoomKey, " ePlayerInsideClubStatus = ", ePlayerInsideClubStatus, " frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
		IF bDebug_PrintPlayerExclusionAreacheckMainRoom
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-559.14031, 279.951287, 79.676308>>, <<-567.432556,280.602701,84.226013>>, 6.60000)	//exclusion zone near the main entrance door
				VECTOR vTempPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG ** ", " bDebug_PrintPlayerExclusionAreacheckMainRoom = return FALSE", " ePlayerInsideClubStatus = ", ePlayerInsideClubStatus, "vPlayerPos = ", vTempPos, " frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
		IF bDebug_DoPlayerStartedBrawlChecks
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sWillyPed.pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG CHECK HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER TARGETTED PED") ENDIF #ENDIF
					DRAW_RECT(0.15, 0.1, 0.1, 0.1, 0, 0, 100, 100)	//BLUE
				ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
					//CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sWillyPed.pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG CHECK HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER DAMAGED PED") ENDIF #ENDIF
					DRAW_RECT(0.25, 0.1, 0.1, 0.1, 100, 0, 0, 100)	//RED 
				ELIF IS_ENTITY_TOUCHING_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG CHECK HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER touching PED") ENDIF #ENDIF
					DRAW_RECT(0.35, 0.1, 0.1, 0.1, 255, 216, 0, 100)	//YELLOW 
				ENDIF
			ENDIF
		ENDIF
		IF bDebug_DoShapeTestPlayertoMusician
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				IF CAN_PED_SEE_PED(sWillyPed.pedIndex, PLAYER_PED_ID())
					DRAW_RECT(0.90, 0.85, 0.05, 0.05, 0, 255, 0, 150)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " bDebug_DoShapeTestPlayertoMusician - CAN_PED_SEE_PED - TRUE") ENDIF #ENDIF
				ELSE
					DRAW_RECT(0.90, 0.85, 0.05, 0.05, 255, 0, 0, 150)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " bDebug_DoShapeTestPlayertoMusician - CAN_PED_SEE_PED - FALSE") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
		/*
		// debug fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRESSED KEY 0") ENDIF #ENDIF
	    	IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				SET_ENTITY_HEALTH(sWillyPed.pedIndex, 0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - KILLED sWillyPed ") ENDIF #ENDIF
			ENDIF
	    ENDIF	*/	
		IF bDebug_PrintCurrentMusicianState
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " bDebug_PrintCurrentMusicianState = ", eMusicianState) ENDIF #ENDIF
		ENDIF
	ENDPROC

	/// PURPOSE:
	/// removes my debug mission widget group
	PROC CLEANUP_MISSION_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    remove object from the game
/// PARAMS:
///    objectIndex - object to remove
///    bDelete - if true delete it, else set as no longer needed
PROC SAFE_REMOVE_OBJECT(OBJECT_INDEX &objectIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_OBJECT(objectIndex)
	ELSE
		SAFE_RELEASE_OBJECT(objectIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes vehicle from the world
/// PARAMS:
///    vehIndex - vehicle to be removed
///    bDelete - if true AND player isn't inside it delete it, else set as no longer needed
PROC SAFE_REMOVE_VEHICLE(VEHICLE_INDEX &vehIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_VEHICLE(vehIndex)
	ELSE
		SAFE_RELEASE_VEHICLE(vehIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes ped from the game
/// PARAMS:
///    ped - ped to be removed
///    bDelete - if true delete it, else set as no longer needed
///    bDetach - if true ped will be detached if attached, and no in a vehicle or getting into a vehicle
PROC SAFE_REMOVE_PED(PED_INDEX &ped, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_PED(ped)
	ELSE
		SAFE_RELEASE_PED(ped)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a ped to a new location
/// PARAMS:
///    pedIndex - ped to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the ped at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
///    bForcePedAiAndAnimUpdate - if true FORCE_PED_AI_AND_ANIMATION_UPDATE is applied on the ped
PROC SAFE_TELEPORT_PED(PED_INDEX pedIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bForcePedAiAndAnimUpdate = FALSE)
	SAFE_TELEPORT_ENTITY(pedIndex, vPosition, fHeading, bSnapToGround)
	IF bForcePedAiAndAnimUpdate
		IF IS_ENTITY_ALIVE(pedIndex)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a vehicle to a new location
/// PARAMS:
///    vehIndex - vehicle to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the vehicle at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
PROC SAFE_TELEPORT_VEHICLE(VEHICLE_INDEX &vehIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bKeepTasks = FALSE)
	IF IS_ENTITY_ALIVE(vehIndex)
		IF bSnapToGround = TRUE
			BOOL groundCheckOk = FALSE
			FLOAT fNewZ = 0.0
			groundCheckOk = GET_GROUND_Z_FOR_3D_COORD(vPosition, fNewZ)			
			IF (groundCheckOk)
				vPosition.z = fNewZ
			ENDIF
		ENDIF
		SET_ENTITY_COORDS(vehIndex, vPosition, FALSE, bKeepTasks)
		SET_ENTITY_HEADING(vehIndex, fHeading)
		IF bSnapToGround
			SET_VEHICLE_ON_GROUND_PROPERLY(vehIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleports a ped out of their vehicle.
///    NOTE: proc waits until GET_SAFE_COORD_FOR_PED returns true
/// PARAMS:
///    pedIndex - ped to Teleport
PROC SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PED_INDEX pedIndex)
	VECTOR vRespotCoords = << 0.0, 0.0, 0.0 >>
	FLOAT fRespotHeading = 0.0	
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_IN_ANY_VEHICLE(pedIndex)				
			vRespotCoords = GET_ENTITY_COORDS(pedIndex)			
			WHILE NOT GET_SAFE_COORD_FOR_PED(vRespotCoords, FALSE, vRespotCoords)
				WAIT(0)
				vRespotCoords.x += 2.0
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE - looking for safe coords") ENDIF #ENDIF
			ENDWHILE			
			fRespotHeading = GET_ENTITY_HEADING(pedIndex)			
			SAFE_TELEPORT_PED(pedIndex, vRespotCoords, fRespotHeading, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a blip of the correct size and colour for an object being used as a pickup
/// PARAMS:
///    vPos - where to create the blip
///    bBlipPriority - priority of the blip
///    bDisplayRoute - do we want to add a route for this blip?
/// RETURNS:
///   the blip index of the new blip (null if blip was not created
FUNC BLIP_INDEX CREATE_OBJECT_PICKUP_BLIP(ENTITY_INDEX EntityIndex, BLIP_PRIORITY bBlipPriority=BLIPPRIORITY_MED)
	BLIP_INDEX bRetBlip = NULL
	
	IF IS_ENTITY_ALIVE(EntityIndex)
		bRetBlip = ADD_BLIP_FOR_ENTITY(EntityIndex)
		IF DOES_BLIP_EXIST(bRetBlip)
			SET_BLIP_PRIORITY(bRetBlip,bBlipPriority)
			SET_BLIP_SCALE(bRetBlip, BLIP_SIZE_PICKUP)
		ENDIF
	ENDIF
	RETURN bRetBlip
ENDFUNC

/// PURPOSE:
///   Sets entity to face an entity using SET_ENTITY_HEADING_FACE_ENTITY
///   but also performs alive checks on the entities
/// PARAMS:
///    ent - entity to turn
///    targent - entity to face
PROC SAFE_SET_ENTITY_HEADING_FACE_ENTITY(ENTITY_INDEX ent, ENTITY_INDEX targent)
	IF IS_ENTITY_ALIVE(ent)
	AND IS_ENTITY_ALIVE(targent)
		SET_ENTITY_HEADING_FACE_ENTITY(ent, targent)
	ENDIF
ENDPROC

/// PURPOSE:
///    Compares two headings
/// PARAMS:
///    f1 - first heading
///    f2 - second heading
///    fTolerance - max difference allowed in the headings
/// RETURNS:
///    TRUE if the difference between the headings is less than or equal to the tolerance
FUNC BOOL IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(FLOAT f1, FLOAT f2, FLOAT fTolerance)
	IF (ABSF(f1 - f2) <= fTolerance)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE - test returned true") ENDIF #ENDIF
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
///    and are within the z threshold
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
///    fThresholdZ - max z different between the two positions
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D_WITH_DIFFERENT_Z_THRESHOLD(VECTOR v1, VECTOR v2, FLOAT fRange, FLOAT fThresholdZ)
	VECTOR vDiff = v2 - v1
	IF ((vDiff.z * vDiff.z) <= (fThresholdZ * fThresholdZ))
		IF ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check is the specified time has passed using specified timer.
/// PARAMS:
///    iTimeAmount - Time to check.
///    iTimer - Timer to use.
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimer, INT iTimeAmount)
	RETURN (GET_GAME_TIMER() - iTimer) > iTimeAmount
ENDFUNC

/// PURPOSE:
///    Check is the specified frame count has passed using specified timer.
/// PARAMS:
///    iFrameCounterTimer - Frame Count to check.
///    iFrameAmount - Frame Count to check against.
/// RETURNS:
///    True is the specified amount of iFrameAmount has passed for the specified iFrameCounterTimer.
FUNC BOOL HAS_FRAME_COUNTER_PASSED(INT iFrameCounterTimer, INT iFrameAmount)
	RETURN (GET_FRAME_COUNT() - iFrameCounterTimer) > iFrameAmount
ENDFUNC

/// PURPOSE:
///    check if the player in a vehicle has potentially just ran over the specified ped
/// PARAMS:
///    ped - ped to check
///    vPlayerPosition - player coords
/// RETURNS:
///    TRUE if he's may have ran over him recently
FUNC BOOL HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED(PED_INDEX pedIndex, VECTOR vPlayerPosition)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
		AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
			IF IS_PED_RAGDOLL(pedIndex)
				IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPosition, 3.5)
				OR IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED - returning true") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    convert a TEXT_LABEL into type STRING
/// PARAMS:
///    tlTextLabel - the text label to convert
/// RETURNS:
///    STRING
FUNC STRING N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(STRING tlTextLabel)
	RETURN(tlTextLabel)
ENDFUNC

/// PURPOSE:
///    Register a new door with the door system
///    NOTE: code taken from Lester1A.sc
/// PARAMS:
///    eDoor - The specific door to register taken from N1A_MISSION_CONTROLLED_DOORS_ENUM
///    mnModel - the door's model name 
///    vPos - the door's position
PROC REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MISSION_CONTROLLED_DOORS_ENUM eDoor, MODEL_NAMES mnModel, VECTOR vPos)
	TEXT_LABEL_23	str_hash = "N1A_"
	str_hash += ENUM_TO_INT(eDoor)
	iMissionControlledDoors[eDoor] = GET_HASH_KEY(str_hash)
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])		
		ADD_DOOR_TO_SYSTEM(iMissionControlledDoors[eDoor], mnModel, vPos, FALSE, TRUE)	// note: useOldOverrides parameter must be set to FALSE! Default is TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REGISTER_NEW_MISSION_CONTROLLED_DOOR - door added to the system : ", str_hash, " hash key : ", iMissionControlledDoors[eDoor], " at pos : ", vPos) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the state of a mission controlled door which has been registered with the door system
/// PARAMS:
///    eDoor - specific door to set state taken from N1A_MISSION_CONTROLLED_DOORS_ENUM
///    eDoorState - the new door state
///    bFlushState - this should only be set true for the last command called on a door in the single frame, BUT also this should be the last call to update a door so set to TRUE by default
/// RETURNS:
///    True if door state was successfully set
FUNC BOOL SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MISSION_CONTROLLED_DOORS_ENUM eDoor, DOOR_STATE_ENUM eDoorState, BOOL bFlushState = TRUE)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		DOOR_SYSTEM_SET_DOOR_STATE(iMissionControlledDoors[eDoor], eDoorState, FALSE, bFlushState)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_CONTROLLED_DOOR_STATE - RETURN true for door :", eDoor, " set state : ", eDoorState, " bFlushState = ", bFlushState) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Set the open ratio and hold open flag on a mission controlled door which has been registered with the door system
/// PARAMS:
///    eDoor - specific door to set state taken from N1A_MISSION_CONTROLLED_DOORS_ENUM
///    fOpenRatio - 0.0 = shut, 1.0 = fully open
///    bSetHoldOpen - if the door is to hold it's open state
///    bFlushState - this should only be set true for the last command called on a door in the single frame.
/// RETURNS:
///    True if door open state was successfully set
FUNC BOOL SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MISSION_CONTROLLED_DOORS_ENUM eDoor, FLOAT fOpenRatio = 0.0, BOOL bSetHoldOpen = FALSE, BOOL bFlushState = FALSE)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])		
		DOOR_SYSTEM_SET_HOLD_OPEN(iMissionControlledDoors[eDoor], bSetHoldOpen)
		DOOR_SYSTEM_SET_OPEN_RATIO(iMissionControlledDoors[eDoor], fOpenRatio, FALSE, bFlushState)		
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO - RETURN true for door : ", eDoor, " ratio : ", fOpenRatio, " bSetHoldOpen : ", bSetHoldOpen, " bFlushState = ", bFlushState) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the door returns DOOR_SYSTEM_GET_IS_PHYSICS_LOADED true
/// PARAMS:
///    eDoor - specific door to check
/// RETURNS:
///    TRUE if DOOR_SYSTEM_GET_IS_PHYSICS_LOADED returns true and door is registered with the system
FUNC BOOL IS_MISSION_CONTROLLED_DOOR_PHYSICS_LOADED(N1A_MISSION_CONTROLLED_DOORS_ENUM eDoor)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		//IF DOOR_SYSTEM_GET_IS_PHYSICS_LOADED(iMissionControlledDoors[eDoor])
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_MISSION_CONTROLLED_DOOR_PHYSICS_LOADED - RETURN true for door : ", eDoor) ENDIF #ENDIF
			RETURN TRUE
		//ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    trigger ambient speech using PLAY_PED_AMBIENT_SPEECH_WITH_VOICE
///    handles toggling STOP_PED_SPEAKING
/// PARAMS:
///    pedIndex - 
///    Context - 
///    VoiceName - 
///    Params - 
PROC NIG1A_PLAY_TRIGGER_AMBIENT_SPEECH(PED_INDEX pedIndex ,STRING Context, STRING VoiceName, SPEECH_PARAMS Params = SPEECH_PARAMS_ADD_BLIP)
	IF IS_PED_UNINJURED(pedIndex)
		BOOL bTemp = IS_AMBIENT_SPEECH_DISABLED(pedIndex)
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, FALSE)
		ENDIF
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedIndex, Context, VoiceName, Params)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIG1A_PLAY_TRIGGER_AMBIENT_SPEECH - Context : ", Context, " VoiceName : ", VoiceName) ENDIF #ENDIF
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC
							

/// PURPOSE:
///    task Willy and his groupie to use a specific sync scene anim
/// PARAMS:
///    eWhichAnim - which animation they should be using
PROC SET_WILLY_GROUPIE_SYNCED_ANIM(N1A_Willy_GROUPIE_ANIM_TYPE eWhichAnim)	

	INT i
	BOOL bLoopedScene = FALSE
	BOOL bHoldLastFrameScene = FALSE
	BOOL bCanInterruptBaseScene = FALSE
	BOOL bForceSceneStart = FALSE
	FLOAT fBlendInDelta = NORMAL_BLEND_IN
	TEXT_LABEL_23 tlAnimNameWilly, tlAnimNameGroupie
	
	SWITCH eWhichAnim
		CASE N1A_WGAT_BASE_ANIM
			bLoopedScene = TRUE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = FALSE
			tlAnimNameWilly = "BASE_Willie"
			tlAnimNameGroupie = "BASE_GIRL"
		BREAK
		CASE N1A_WGAT_BASE_2_ANIM
			bLoopedScene = TRUE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = FALSE
			tlAnimNameWilly = "BASE_02_Willie"
			tlAnimNameGroupie = "BASE_02_GIRL"
		BREAK
		CASE N1A_WGAT_THIS_IS_AWKWARD
			bLoopedScene = FALSE
			bHoldLastFrameScene = TRUE
			bCanInterruptBaseScene = TRUE
			fBlendInDelta = SLOW_BLEND_IN
			tlAnimNameWilly = "THIS_IS_AWKWARD_Willie"
			tlAnimNameGroupie = "THIS_IS_AWKWARD_GIRL"
		BREAK
		CASE N1A_WGAT_YOU_KNOW
			bLoopedScene = FALSE
			bHoldLastFrameScene = TRUE
			bCanInterruptBaseScene = TRUE
			fBlendInDelta = SLOW_BLEND_IN
			tlAnimNameWilly = "YOU_KNOW_Willie"
			tlAnimNameGroupie = "YOU_KNOW_GIRL"
		BREAK
		CASE N1A_WGAT_EXIT
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = TRUE
			bForceSceneStart = TRUE
			fBlendInDelta = SLOW_BLEND_IN
			tlAnimNameWilly = "EXIT_Willie"
			tlAnimNameGroupie = "EXIT_GIRL"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1A.sc : SET_WILLY_GROUPIE_SYNCED_ANIM invalid parameter eWhichAnim")
		BREAK
	ENDSWITCH
	
	// if the synched scene isn't allowed to interupt the base anim, exit early if the base anim scene is running
	IF NOT bCanInterruptBaseScene
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_BASE_ANIM])
		OR IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_BASE_2_ANIM])
			EXIT
		ELSE
			CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - claims sync scenes aren't running to exit early : tlAnimNameWilly = ", tlAnimNameWilly)
		ENDIF
	ENDIF
	
	// don't allow the synched scene to stomp on other scenes (excluding base)
	IF NOT bForceSceneStart
		FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_Willy_GROUPIE_ANIM_TYPE) - 1)
			IF i != ENUM_TO_INT(N1A_WGAT_BASE_ANIM)
			AND i != ENUM_TO_INT(N1A_WGAT_BASE_2_ANIM)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[i])
					// allow interupt if scene is about to finish
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncedSceneID_WillyGroupieScenes[i]) < 0.95
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[eWhichAnim])
	
		iSyncedSceneID_WillyGroupieScenes[eWhichAnim] = CREATE_SYNCHRONIZED_SCENE(vSyncedScenePosition_WillyGroupie, vSyncedSceneRotation_WillyGroupie)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedSceneID_WillyGroupieScenes[eWhichAnim], bLoopedScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedSceneID_WillyGroupieScenes[eWhichAnim], bHoldLastFrameScene)
		
		// task the peds
		IF IS_PED_UNINJURED(sWillyPed.pedIndex)
			IF IS_ENTITY_PLAYING_ANIM(sWillyPed.pedIndex, tlAnimDict_WillyMocappedAnims, tlAnimNameWilly)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - Willy already playing anim : ", tlAnimNameWilly) ENDIF #ENDIF
			ELSE
				IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sWillyPed.pedIndex, NORMAL_BLEND_OUT, FALSE)
				ENDIF
				TASK_SYNCHRONIZED_SCENE(sWillyPed.pedIndex, iSyncedSceneID_WillyGroupieScenes[eWhichAnim], tlAnimDict_WillyMocappedAnims, tlAnimNameWilly, fBlendInDelta, NORMAL_BLEND_OUT,
						SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS) // can use SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION since he ragdoll continuously
				// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
				IF bFinishedStageSkipping
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sWillyPed.pedIndex)
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - Willy's set anim : ", tlAnimNameWilly, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)
			IF IS_ENTITY_PLAYING_ANIM(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, tlAnimDict_WillyMocappedAnims, tlAnimNameGroupie)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - groupie already playing anim : ", tlAnimNameGroupie) ENDIF #ENDIF
			ELSE
				IF IsPedPerformingTask(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, NORMAL_BLEND_OUT, FALSE)
				ENDIF
				TASK_SYNCHRONIZED_SCENE(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, iSyncedSceneID_WillyGroupieScenes[eWhichAnim], tlAnimDict_WillyMocappedAnims, tlAnimNameGroupie, fBlendInDelta, NORMAL_BLEND_OUT,
						SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS)	// can use SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION since he ragdoll continuously
				// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
				IF bFinishedStageSkipping
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - groupie set anim : ", tlAnimNameGroupie, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_WILLY_GROUPIE_SYNCED_ANIM - skipped, scene in question already running ", eWhichAnim) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    if Willy / groupie has finished their exit animation reaction animation.  Handles triggering and checking it's finished
/// PARAMS:
///    bCheckWillyStatus - if TRUE returns Willy's status, if FALSE returns Groupies status
/// RETURNS:
///    TRUE if the specfic ped has finished their surrender reaction anim
FUNC BOOL HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM(BOOL bCheckWillyStatus)
	
	// decide which exit scene to play - if celeb is in synced scene both will need exit
	IF NOT bSetExitSceneForWillyAndGroupie			
		SET_WILLY_GROUPIE_SYNCED_ANIM(N1A_WGAT_EXIT)
		bSetExitSceneForWillyAndGroupie = TRUE
	ELSE
		// if both scenes aren't running check ped has finished sync scene task
		PED_INDEX pedToTest = sClubPed[GROUPIE_WILLYS_PIECE].pedIndex
		IF bCheckWillyStatus
			pedToTest = sWillyPed.pedIndex
		ENDIF
		IF IS_PED_UNINJURED(pedToTest)
			IF NOT IsPedPerformingTask(pedToTest, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM - return TRUE not got active task with para bCheckWillyStatus : ", bCheckWillyStatus) ENDIF #ENDIF
				RETURN TRUE
			ELIF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_EXIT])
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM - return TRUE sync scene not running with para bCheckWillyStatus : ", bCheckWillyStatus) ENDIF #ENDIF
				RETURN TRUE
			ELSE			
				// B*1516076 - Allow Willy to break out ealier
				IF bCheckWillyStatus
					IF (GET_SYNCHRONIZED_SCENE_PHASE(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_EXIT]) > 0.85)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM - return TRUE early exit in anim bCheckWillyStatus : ", bCheckWillyStatus) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
				// speed up the scene's rate
				IF (GET_SYNCHRONIZED_SCENE_RATE(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_EXIT]) < 1.25)	
					SET_SYNCHRONIZED_SCENE_RATE(iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_EXIT], 1.25)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM - updating scene rate 1.25 this frame") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    get the anim string for the specified ped's anim type
/// PARAMS:
///    iClubPedIndex - the specific ped
///    eWhichAnim - the anim type
/// RETURNS:
///    TEXT_LABEL_23 name of the anim
FUNC TEXT_LABEL_23 GET_CLUB_PED_ANIM_NAME(INT iClubPedIndex, N1A_CLUB_PED_ANIM_TYPE eWhichAnim)

	TEXT_LABEL_23 tlAnimName = ""
	
	
	SWITCH eWhichAnim
		CASE N1A_CPAT_BASE_ANIM
			tlAnimName = "BASE_"
		BREAK
		CASE N1A_CPAT_BUMP_ANIM
			// these two peds only have a valid bump anim
			IF iClubPedIndex != CLUB_PED_BAND_MANAGER
			AND iClubPedIndex != CLUB_PED_GROUPIE_UPSTAIRS_02
			
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_CLUB_PED_ANIM_NAME - RETURN TEXT_LABEL_23 ", tlAnimName, " early no valid anim for ped ID : ", iClubPedIndex) ENDIF #ENDIF
				RETURN tlAnimName
			ELSE
				tlAnimName = "BUMP_"
			ENDIF
		BREAK
		CASE N1A_CPAT_IDLE_A_ANIM
			tlAnimName = "IDLE_A_"
		BREAK
		CASE N1A_CPAT_IDLE_B_ANIM
			tlAnimName = "IDLE_B_"
		BREAK
		CASE N1A_CPAT_OUTRO_ANIM
			tlAnimName = "OUTRO_"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1A.sc : GET_CLUB_PED_ANIM_NAME invalid parameter eWhichAnim")
		BREAK
	ENDSWITCH
	
	TEXT_LABEL_23 tlPedExtention = ""
	
	SWITCH iClubPedIndex
		CASE CLUB_PED_BAND_MANAGER
			tlPedExtention = "M1"
		BREAK
		CASE CLUB_PED_ROADIE
			tlPedExtention = "M2"
		BREAK
		CASE CLUB_PED_GROUPIE_UPSTAIRS_01
			tlPedExtention = "F2"
		BREAK
		CASE CLUB_PED_GROUPIE_UPSTAIRS_02
			tlPedExtention = "F1"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1A.sc : GET_CLUB_PED_ANIM_NAME invalid parameter iClubPedIndex")
		BREAK
	ENDSWITCH
	
	tlAnimName += tlPedExtention	
	
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_CLUB_PED_ANIM_NAME - RETURN TEXT_LABEL_23 ", tlAnimName, " for ped ID : ", iClubPedIndex) ENDIF #ENDIF
	
	RETURN tlAnimName
ENDFUNC

/// PURPOSE:
///    task the club peds upstairs to use the specific anim
/// PARAMS:
///    iClubPedIndex - the specific ped
///    eWhichAnim - which animation they should be using
PROC SET_SPECIFIC_CLUB_PED_UPSTAIRS_SYNCED_ANIM(INT iClubPedIndex, INT iSyncedSceneID, N1A_CLUB_PED_ANIM_TYPE eWhichAnim, FLOAT fBlendInDelta = NORMAL_BLEND_IN)
	
	// These two don't use the synced anims
	IF iClubPedIndex != GROUPIE_WILLYS_PIECE
		IF IS_PED_UNINJURED(sClubPed[iClubPedIndex].pedIndex)
			TEXT_LABEL_23 tlSyncedAnimName = GET_CLUB_PED_ANIM_NAME(iClubPedIndex, eWhichAnim)
			STRING sSyncedAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedAnimName)

			IF IS_ENTITY_PLAYING_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sSyncedAnimName)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM - already playing anim : ", sSyncedAnimName, " for ped ID :", iClubPedIndex) ENDIF #ENDIF
			ELSE
				IF IsPedPerformingTask(sClubPed[iClubPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sClubPed[iClubPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)
				ENDIF
				TASK_SYNCHRONIZED_SCENE(sClubPed[iClubPedIndex].pedIndex, iSyncedSceneID, tlAnimDict_ClubPedsUpstairs, sSyncedAnimName, fBlendInDelta, NORMAL_BLEND_OUT,
						SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS)
				// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
				IF bFinishedStageSkipping
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sClubPed[iClubPedIndex].pedIndex)
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM - set anim : ", sSyncedAnimName, " for ped ID :", iClubPedIndex) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    task the club peds upstairs to use the specific anim
/// PARAMS:
///    eWhichAnim - which animation they should be using
PROC SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(N1A_CLUB_PED_ANIM_TYPE eWhichAnim)	

	INT i
	BOOL bLoopedScene = FALSE
	BOOL bHoldLastFrameScene = FALSE
	BOOL bCanInterruptBaseScene = FALSE
	BOOL bForceSceneStart = FALSE
	FLOAT fBlendInDelta = NORMAL_BLEND_IN
	
	SWITCH eWhichAnim
		CASE N1A_CPAT_BASE_ANIM
			bLoopedScene = TRUE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = FALSE
		BREAK
		CASE N1A_CPAT_BUMP_ANIM
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = TRUE
		BREAK
		CASE N1A_CPAT_IDLE_A_ANIM
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = TRUE
		BREAK
		CASE N1A_CPAT_IDLE_B_ANIM
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = TRUE
			bForceSceneStart = TRUE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		CASE N1A_CPAT_OUTRO_ANIM
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			bCanInterruptBaseScene = TRUE
			bForceSceneStart = TRUE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1A.sc : GET_CLUB_PED_ANIM_NAME invalid parameter eWhichAnim")
		BREAK
	ENDSWITCH
	
	// if the synched scene isn't allowd to interupt the base anim, exit early if the base anim scene is running
	IF NOT bCanInterruptBaseScene
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM])
			EXIT
		ENDIF
	ENDIF
	// don't allow the synched scene to stomp one other scenes (excluding base)
	IF NOT bForceSceneStart
		FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_CLUB_PED_ANIM_TYPE) - 1)
			IF i != ENUM_TO_INT(N1A_CPAT_BASE_ANIM)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_ClubPedUpstairsScenes[i])
					// allow interupt if scene is about to finish
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncedSceneID_ClubPedUpstairsScenes[i]) < 0.95
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID_WillyGroupieScenes[eWhichAnim])
		iSyncedSceneID_ClubPedUpstairsScenes[eWhichAnim] = CREATE_SYNCHRONIZED_SCENE(vSyncedScenePosition_ClubPedsUpstairs, vSyncedSceneRotation_ClubPedsUpstairs)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedSceneID_ClubPedUpstairsScenes[eWhichAnim], bLoopedScene)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedSceneID_ClubPedUpstairsScenes[eWhichAnim], bHoldLastFrameScene)
		
		FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
			IF i != GROUPIE_WILLYS_PIECE
				SET_SPECIFIC_CLUB_PED_UPSTAIRS_SYNCED_ANIM(i, iSyncedSceneID_ClubPedUpstairsScenes[eWhichAnim], eWhichAnim, fBlendInDelta)
			ENDIF
		ENDFOR
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM - skipped as sync in question already running: ", eWhichAnim) ENDIF #ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    task the club peds upstairs to use their bumped anim
/// PARAMS:
///    iClubPedIndex - the specific ped
PROC SET_SPECIFIC_CLUB_PED_UPSTAIRS_BUMPED_ANIM(INT iClubPedIndex)

	// Only these two peds have bumped anims
	IF iClubPedIndex = CLUB_PED_BAND_MANAGER
	OR iClubPedIndex = CLUB_PED_GROUPIE_UPSTAIRS_02
		IF IS_PED_UNINJURED(sClubPed[iClubPedIndex].pedIndex)		
			TEXT_LABEL_23 tlBumpedAnimName = GET_CLUB_PED_ANIM_NAME(iClubPedIndex, N1A_CPAT_BUMP_ANIM)
			STRING sBumpedAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlBumpedAnimName)
					
			IF NOT IS_ENTITY_PLAYING_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sBumpedAnimName)
				IF IsPedPerformingTask(sClubPed[iClubPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sClubPed[iClubPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)
				ENDIF
				TASK_PLAY_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sBumpedAnimName, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
				IF bFinishedStageSkipping
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sClubPed[iClubPedIndex].pedIndex)
				ENDIF				
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_SPECIFIC_CLUB_PED_UPSTAIRS_BUMPED_ANIM - set anim ", sBumpedAnimName, " for ped ID :", iClubPedIndex) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    if the specific ped has finished their synced scene break out animation.  Handles triggering and checking it's finished
/// PARAMS:
///    iClubPedIndex - specific ped to check
/// RETURNS:
///    TRUE if the specfic ped has finished their synced scene break out animation
FUNC BOOL HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM(INT iClubPedIndex)
	IF IS_PED_UNINJURED(sClubPed[iClubPedIndex].pedIndex)
	
		// these pair don't have exit anims
		IF iClubPedIndex = GROUPIE_WILLYS_PIECE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM - return TRUE skipped for iClubPedIndex :", iClubPedIndex) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		
		TEXT_LABEL_23 tlBreakOutAnimName = GET_CLUB_PED_ANIM_NAME(iClubPedIndex, N1A_CPAT_OUTRO_ANIM)
		STRING sBreakOutAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlBreakOutAnimName)
		
		IF IsPedPerformingTask(sClubPed[iClubPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
			STOP_SYNCHRONIZED_ENTITY_ANIM(sClubPed[iClubPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)			
			TASK_PLAY_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sBreakOutAnimName, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
			// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
			IF bFinishedStageSkipping
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sClubPed[iClubPedIndex].pedIndex)
			ENDIF				
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM - set exit anim : ", sBreakOutAnimName, " from sync scene task iClubPedIndex :", iClubPedIndex) ENDIF #ENDIF
			
		// test for the female finishing her reaction animation
		ELIF NOT IS_ENTITY_PLAYING_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sBreakOutAnimName)
		
			//check these pair aren't playing their bumped anim
			IF iClubPedIndex = CLUB_PED_BAND_MANAGER
			OR iClubPedIndex = CLUB_PED_GROUPIE_UPSTAIRS_02
			
				TEXT_LABEL_23 tlBumpAnimName = GET_CLUB_PED_ANIM_NAME(iClubPedIndex, N1A_CPAT_OUTRO_ANIM)
				STRING sBumpAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlBumpAnimName)
		
				IF IS_ENTITY_PLAYING_ANIM(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, sBumpAnimName)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM - waiting for bump anim to finish anim : ", sBumpAnimName, " iClubPedIndex :", iClubPedIndex) ENDIF #ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM - return TRUE for iClubPedIndex :", iClubPedIndex) ENDIF #ENDIF
			RETURN TRUE
		ELSE
			//SET_ENTITY_ANIM_SPEED(sClubPed[iClubPedIndex].pedIndex, tlAnimDict_ClubPedsUpstairs, GET_CLUB_PED_ANIM_NAME(iClubPedIndex, N1A_CPAT_OUTRO_ANIM), 1.2)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles setting the club doors' initial states using the DOOR_SYSTEM_GET_IS_PHYSICS_LOADED check
PROC MANAGE_SET_CLUB_DOORS_INITIAL_STATES()
	INT i
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_MISSION_CONTROLLED_DOORS) - 1)
		IF bSetMissionControlledDoorsInitialStates[i] = FALSE
			IF IS_MISSION_CONTROLLED_DOOR_PHYSICS_LOADED(INT_TO_ENUM(N1A_MISSION_CONTROLLED_DOORS_ENUM, i))
				SWITCH INT_TO_ENUM(N1A_MISSION_CONTROLLED_DOORS_ENUM, i)
					CASE N1A_MCD_INT_DD_TO_MAIN_ROOM_01
						// double doors leading from the main entrance corridor into the main room	
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, 1.0, TRUE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
					BREAK
					CASE N1A_MCD_INT_DD_TO_MAIN_ROOM_02
						// double doors leading from the main entrance corridor into the main room	
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, 1.0, TRUE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
					BREAK
					CASE N1A_MCD_INT_ENTRANCE_STAFF_AREA_01
						// the door leads to the staff area at the main entrance where they'd take tickets / cash
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, 0.0, FALSE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)
					BREAK
					CASE N1A_MCD_INT_TO_REAR_ENTRANCE_01
						// the door which leads up the stairs to the rear entrance	
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_REAR_ENTRANCE_01, 0.0, FALSE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
					BREAK
					CASE N1A_MCD_INT_TO_BASEMENT_01
						// the door which leads down to the basement area	
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_BASEMENT_01, 0.0, FALSE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)
					BREAK
					CASE N1A_MCD_INT_BASEMENT_01
						// the door for the basement room	
						SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_BASEMENT_01, 0.0, FALSE, FALSE)
						//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
						SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)
					BREAK
					DEFAULT
						SCRIPT_ASSERT("NIGEL 1A : MANAGE_SET_CLUB_DOORS_INITIAL_STATES - invalid door value detected for N1A_MISSION_CONTROLLED_DOORS_ENUM")
					BREAK
				ENDSWITCH	
				bSetMissionControlledDoorsInitialStates[i] = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_SET_CLUB_DOORS_INITIAL_STATES - set this frame : ", GET_FRAME_COUNT(), " for door : ", i) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDFOR
	/*
	// NOTE: two doors for the room upstairs aren't currently tagged up as doors.
	
		///Some info from David to try out - 
	// Set Ratios first with no flush then call set state with DOORSTATE_FORCE_CLOSED_THIS_FRAME  and flush state to true
	// then immediately calling set state with DOORSTATE_FORCE_OPEN_THIS_FRAME
	// also setting flush state to true this should first snap the door into position then immediately open it.
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, 1.0, TRUE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, 1.0, TRUE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_REAR_ENTRANCE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_BASEMENT_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_BASEMENT_01, 0.0, FALSE, FALSE)
	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	WAIT(0)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	*/
	/*	
	// double doors leading from the main entrance corridor into the main room	
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, 1.0, TRUE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
		
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, 1.0, TRUE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	
	// the door leads to the staff area at the main entrance where they'd take tickets / cash
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, 0.0, FALSE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	
	// the door which leads up the stairs to the rear entrance	
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_REAR_ENTRANCE_01, 0.0, FALSE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	
	// NOTE: two doors for the room upstairs aren't currently tagged up as doors.
	
	// the door which leads down to the basement area	
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_BASEMENT_01, 0.0, FALSE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	
	// the door for the basement room	
	//SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_BASEMENT_01, 0.0, FALSE, FALSE)
	//SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	*/
	
ENDPROC

/// PURPOSE:
///    deals with pinningthe hospital interior to memory and setting the interior active
/// PARAMS:
///    bDoHasAssetsLoadedCheck - if true game waits on IS_INTERIOR_READY check
FUNC BOOL IS_MUSIC_CLUB_INTERIOR_LOADED(BOOL bRequestInterior = TRUE)
	// distance from venue check?		
	IF NOT IS_INTERIOR_READY(RockClubInteriorIndex)	
		IF bRequestInterior
			PIN_INTERIOR_IN_MEMORY(RockClubInteriorIndex)
		ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_MUSIC_CLUB_INTERIOR_LOADED - returning FALSE") ENDIF #ENDIF
		RETURN FALSE
	ELSE
		SET_INTERIOR_ACTIVE(RockClubInteriorIndex, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_MUSIC_CLUB_INTERIOR_LOADED - returning TRUE") ENDIF #ENDIF
		//bDone_rock_clubInterior_PinnedInMemory = TRUE
		RETURN TRUE
	ENDIF
ENDFUNC

/// PURPOSE:
///    check if the specified position is inside the club using 
///    IS_COLLISION_MARKED_OUTSIDE check
/// PARAMS:
///    vPos - the position to test
/// RETURNS:
///    TRUE if the position is inside the music club interior
FUNC BOOL IS_POSITION_INSIDE_MUSIC_CLUB_INTERIOR(VECTOR vPos)
	IF NOT IS_COLLISION_MARKED_OUTSIDE(vPos)
		INTERIOR_INSTANCE_INDEX interiorInstanceIndex = GET_INTERIOR_FROM_COLLISION(vPos)
		IF interiorInstanceIndex = RockClubInteriorIndex
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_POSITION_INSIDE_MUSIC_CLUB_INTERIOR - return TRUE - for pos : ", vPos) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if an entity is inside the rock club interior
/// PARAMS:
///    entityIndex - entity to test
///    eRoomToCheck - the specific room to test
///    bCheckEntityIsAlive - check the entity is alive first
/// RETURNS:
///    true if the entity is inside the rock club interior
FUNC BOOL IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(ENTITY_INDEX entityIndex, N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM eRoomToCheck, BOOL bCheckEntityIsAlive = TRUE)
	IF bCheckEntityIsAlive
		IF NOT IS_ENTITY_ALIVE(entityIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR - return FALSE entityIndex is not alive") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	VECTOR vPos = GET_ENTITY_COORDS(entityIndex)
	IF (GET_INTERIOR_AT_COORDS(vPos) = RockClubInteriorIndex)	//unfortunately this check on its own can return true if stood outside interior closeby		
		INT iHaskKey_PedCurrentRoom		
		iHaskKey_PedCurrentRoom = GET_KEY_FOR_ENTITY_IN_ROOM(entityIndex)
		IF iHaskKey_PedCurrentRoom = iRockClubRoom[eRoomToCheck]
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR - return TRUE - entityIndex in room ID : ", eRoomToCheck) ENDIF #ENDIF
			RETURN TRUE
		ENDIF		
	ENDIF
	IF eRoomToCheck = N1A_RCR_INVALID
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR - return TRUE - entityIndex status is N1A_RCR_INVALID ID : ", eRoomToCheck) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if an entity is inside the rock club interior
/// PARAMS:
///    entityIndex - entity to test
///    bCheckEntityIsAlive - check the ped isn't dead before proceeding NOTE: GET_KEY_FOR_ENTITY_IN_ROOM(pedIndex) requires entity alive check!
/// RETURNS:
///    true if the entity is inside the rock club interior
FUNC BOOL IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(ENTITY_INDEX entityIndex, BOOL bCheckEntityIsAlive = TRUE)
	//GET_KEY_FOR_ENTITY_IN_ROOM(pedIndex) requires entity alive check
	IF bCheckEntityIsAlive
		IF IS_ENTITY_ALIVE(entityIndex)
			N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM eRoomToCheck
			INT i, iHaskKey_PedCurrentRoom		
			iHaskKey_PedCurrentRoom = GET_KEY_FOR_ENTITY_IN_ROOM(entityIndex)			
			FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_NUM_ROCK_CLUB_ROOMS) - 1)			
				eRoomToCheck = INT_TO_ENUM(N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM, i)
				IF eRoomToCheck <> N1A_RCR_INVALID	// don't perform the check if it's got to the invalid room
					IF iHaskKey_PedCurrentRoom = iRockClubRoom[eRoomToCheck]
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR - return TRUE - entityIndex in room ID : ", i) ENDIF #ENDIF
						RETURN TRUE
					ENDIF	
				ENDIF
			ENDFOR
		ENDIF
	ELSE
		// check is returning inaccurate results, not as reliable as the GET_KEY_FOR_ENTITY_IN_ROOM
		VECTOR vPos = GET_ENTITY_COORDS(entityIndex, FALSE)
		IF (GET_INTERIOR_AT_COORDS(vPos) = RockClubInteriorIndex)	//unfortunately this check on its own can return true if stood outside interior closeby		
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR - inside interior but room ID doesn't match") ENDIF #ENDIF
			RETURN TRUE
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if an entity is inside the rock club interior
/// PARAMS:
///    entityIndex - entity to test
///    bCheckEntityIsAlive - check the entity is alive first
/// RETURNS:
///    true if the entity is inside the rock club interior
FUNC N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM GET_PED_INSIDE_MUSIC_CLUB_STATUS(ENTITY_INDEX entityIndex, BOOL bCheckEntityIsAlive = TRUE)
	IF bCheckEntityIsAlive
		IF NOT IS_ENTITY_ALIVE(entityIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM - return N1A_RCR_INVALID entityIndex is not alive") ENDIF #ENDIF
			RETURN N1A_RCR_INVALID
		ENDIF
	ENDIF
	VECTOR vPos = GET_ENTITY_COORDS(entityIndex)
	IF (GET_INTERIOR_AT_COORDS(vPos) = RockClubInteriorIndex)	//unfortunately this check on its own can return true if stood outside interior closeby		
		INT iHaskKey_PedCurrentRoom		
		iHaskKey_PedCurrentRoom = GET_KEY_FOR_ENTITY_IN_ROOM(entityIndex)
		N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM eRoomToCheck
		INT i
		FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_NUM_ROCK_CLUB_ROOMS) - 1)			
			eRoomToCheck = INT_TO_ENUM(N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM, i)
			IF eRoomToCheck <> N1A_RCR_INVALID	// don't perform the check if it's got to the invalid room
				IF iHaskKey_PedCurrentRoom = iRockClubRoom[eRoomToCheck]
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM - return room ID : ", i) ENDIF #ENDIF
					RETURN eRoomToCheck
				ENDIF	
			ENDIF
		ENDFOR
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM - inside interior but room ID doesn't match") ENDIF #ENDIF
	ENDIF
	RETURN N1A_RCR_INVALID
ENDFUNC

/// PURPOSE:
///    B*1544838 - need to toggle audio scene on and off when yoou enter / leave the club
///    also needs to not be active whilst in a vehicle in the club (rare but possible)
/// PARAMS:
///    ePlayerInsideClubStatus - eLocal_PlayerInsideClubStatus
PROC UPDATE_AUDIO_SCENE(N1A_ROCK_CLUB_INTERIOR_ROOM_ENUM eLocal_PlayerInsideClubStatus)
	
	// Outside the club
	IF eLocal_PlayerInsideClubStatus = N1A_RCR_INVALID	// player still in the club
	OR eLocal_PlayerInsideClubStatus = N1A_MAX_NUM_ROCK_CLUB_ROOMS
		IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1A_SCENE")
			STOP_AUDIO_SCENE("NIGEL_1A_SCENE")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_AUDIO_SCENE - STOP_AUDIO_SCENE - NIGEL_1A_SCENE outside club") ENDIF #ENDIF
		ENDIF
	//inside the club
	ELSE
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
			IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_1A_SCENE")				
				START_AUDIO_SCENE("NIGEL_1A_SCENE")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_AUDIO_SCENE - START_AUDIO_SCENE - NIGEL_1A_SCENE - in club") ENDIF #ENDIF
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1A_SCENE")				
				STOP_AUDIO_SCENE("NIGEL_1A_SCENE")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_AUDIO_SCENE - STOP_AUDIO_SCENE - NIGEL_1A_SCENE in club in veh") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
/// PARAMS:
///    sConversationRoot - the conversation root to test
///    bReturnTrueIfStringIsNull - checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT can return "NULL" the first few frames after CREATE_CONVERSATION has returned true
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a root matching the passed in string or "NULL" if bReturnTrueIfNULL
FUNC BOOL IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(STRING sConversationRoot, BOOL bReturnTrueIfStringIsNull = TRUE)
	TEXT_LABEL_23 tlTempRoot		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
		//if current root matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempRoot, sConversationRoot)
			RETURN TRUE
		ENDIF		
		IF bReturnTrueIfStringIsNull
			//if current root matches null, return true
			//because if the correct root isn't returned the first frame or so after CREATE_CONVERSATION has return true.
			IF ARE_STRINGS_EQUAL(tlTempRoot, "NULL")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
///    cloneConversation - ?
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) != 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_IN()	// problems with convo's starting during fade out
		IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - Which line of dialogue to start from
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1A_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) != 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_IN()	// problems with convo's starting during fade out
		IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using PLAY_SINGLE_LINE_FROM_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - the specific line we want to play
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen 
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1A_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) != 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF IS_SCREEN_FADED_IN()	// problems with convo's starting during fade out
		IF PLAY_SINGLE_LINE_FROM_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    will return the enumSubtitlesState needed to allow a dialogue line to play
///    without subtitles if a message is being displayed
/// RETURNS:
///    enumSubtitlesState
FUNC enumSubtitlesState GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()

	IF IS_MESSAGE_BEING_DISPLAYED()
		RETURN DO_NOT_DISPLAY_SUBTITLES
	ENDIF
	
	RETURN DISPLAY_SUBTITLES
ENDFUNC

/// PURPOSE:
///    Check the specified ped is in the current conversation
/// PARAMS:
///    pedIndex - ped to test
/// RETURNS:
///    True if ped is alive and IS_PED_IN_CURRENT_CONVERSATION
FUNC BOOL NIG1A_IS_PED_INVOLVED_IN_CURRENT_CONVERSATION(PED_INDEX pedIndex)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_IN_CURRENT_CONVERSATION(pedIndex)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIG1A_IS_PED_INVOLVED_IN_CURRENT_CONVERSATION - return true, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    kill's any ongoing ambient dialogue between Willy and groupie and stores the conversation progress so it can be retriggered
/// PARAMS:
///    bFinishCurrentLine - if we shoud use KILL_FACE_TO_FACE_CONVERSATION() or KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
/// RETURNS:
///    true if the convo was killed and stored
FUNC BOOL KILL_AND_STORE_CURRENT_Willy_AND_GROUPIE_AMBIENT_DIALOGUE(BOOL bFinishCurrentLine = TRUE)
	IF NOT bPausedDialogue_AmbientWillyGroupie
		IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL1", FALSE)	//only  the ambient between Willy and groupie
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL2", FALSE)	//only  the ambient between Willy and groupie
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL3", FALSE)	//only  the ambient between Willy and groupie
			// Bug fix - could return dialogue as playing in first frame even though the line is invalid, so wait for line to be valid
			TEXT_LABEL_23 tl23CurrentDialogueLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "KILL_AND_STORE_CURRENT_Willy_AND_GROUPIE_AMBIENT_DIALOGUE - line : ", tl23CurrentDialogueLine) ENDIF #ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(tl23CurrentDialogueLine)
				bPausedDialogue_AmbientWillyGroupie = TRUE
				IF iCounter_AmbientDialogueWillyGroupie > 0
					iCounter_AmbientDialogueWillyGroupie--	// ready to retrigger last dialogue
				ENDIF 
				tlDialogueLinePaused_WillyGroupieAmbient = tl23CurrentDialogueLine
				IF bFinishCurrentLine
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "KILL_AND_STORE_CURRENT_Willy_AND_GROUPIE_AMBIENT_DIALOGUE - return TRUE, finish current line : ",
																					bFinishCurrentLine, " line : ", tlDialogueLinePaused_WillyGroupieAmbient) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE msStage)
	emissionStage = msStage
	eSubStage = SS_SETUP
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:MISSION INIT STUFF
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    General setup for the mission which effects the world
PROC SETUP_WORLD_STATE_FOR_MISSION()
	
	// Enabled by the initial scene in normal flow to prevent player seeing a odd door flash as it regens
	// only do it here for replays / z skips
	IF IS_REPLAY_IN_PROGRESS()
	OR NOT bFinishedStageSkipping
		SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
		CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)")
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	SET_CREATE_RANDOM_COPS(FALSE)
	
	// Remove stuff we don't want around the carpark - vehicles, peds, objects, blood decals
	SET_PED_NON_CREATION_AREA(vClearZone_RockClub_Min, vClearZone_RockClub_Max)
	scenarioBlockingAreaRockClub = ADD_SCENARIO_BLOCKING_AREA(vClearZone_RockClub_Min, vClearZone_RockClub_Max)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", FALSE)		// Stop ambient security spawning
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_CLIPBOARD", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)	// Stop truck pulling out in the car park
	
	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_DOORMAN_01, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_BOUNCER_01, TRUE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_RockClub_Min, vClearZone_RockClub_Max, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sMusicianVehicle.mnModel, TRUE)
	
	// only do it here for replays / z skips
	// clear areas which would normally get missed since they occur in the launcher's SETUP_AREA_FOR_MISSION
	IF IS_REPLAY_IN_PROGRESS()
	OR IS_REPEAT_PLAY_ACTIVE()
	OR IS_REPLAY_BEING_SET_UP()
	OR NOT bFinishedStageSkipping
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearZone_RockClub_Min, vClearZone_RockClub_Max)
		CLEAR_AREA(vMusicClub_CentrePos, 15.0, TRUE)
		CLEAR_AREA(<< -573.39404, 289.36960, 79.06918 >>, 15.0, TRUE)	// extra clear area for bottom room in the club			
		CLEAR_AREA(<< -554.17, 307.68, 82.84 >>, 2.0, TRUE)		
		CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - simulated SETUP_AREA_FOR_MISSION for replay / skip")
	ENDIF
	
	// exterior doors to the club are handed by Kenneth's building controller
	SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_UNLOCKED)	//DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_UNLOCKED)	//DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
	CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - set club exteior doors state - DOORSTATE_FORCE_UNLOCKED_THIS_FRAME")
																					
	///Some info from David to try out - 
	// Set Ratios first with no flush then call set state with DOORSTATE_FORCE_CLOSED_THIS_FRAME  and flush state to true
	// then immediately calling set state with DOORSTATE_FORCE_OPEN_THIS_FRAME
	// also setting flush state to true this should first snap the door into position then immediately open it.
	
	INT i 
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_MISSION_CONTROLLED_DOORS) - 1)
		bSetMissionControlledDoorsInitialStates[i] = TRUE
	ENDFOR
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, 1.0, TRUE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, 1.0, TRUE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_REAR_ENTRANCE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_TO_BASEMENT_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1A_MCD_INT_BASEMENT_01, 0.0, FALSE, FALSE)
	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_CLOSED_THIS_FRAME, TRUE)	
	WAIT(0)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_REAR_ENTRANCE_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)	
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_TO_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)		
	SET_MISSION_CONTROLLED_DOOR_STATE(N1A_MCD_INT_BASEMENT_01, DOORSTATE_FORCE_OPEN_THIS_FRAME, TRUE)
ENDPROC

/// PURPOSE:
///    requests and checks if the groupies' models are loaded
/// RETURNS:
///    TRUE if all of the groupies' models loaded
FUNC BOOL HAVE_CLUB_PED_MODELS_LOADED()
	INT i
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		REQUEST_MODEL(sClubPed[i].mnModel)
	ENDFOR
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		IF NOT HAS_MODEL_LOADED(sClubPed[i].mnModel)
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests and checks to see if the assets are loaded for the start of the mission
/// RETURNS:
///    TRUE if all the assets have loaded
FUNC BOOL HAVE_ASSETS_FOR_MISSION_INIT_LOADED()
	REQUEST_MODEL(mpToothPickup.mnModel)	
	REQUEST_MODEL(sWillyPed.mnModel)	
	REQUEST_MODEL(sMusicianVehicle.mnModel)
	REQUEST_ANIM_DICT("rcmnigel1a")		
	REQUEST_ANIM_DICT(tlAnimDict_ClubPedsUpstairs)	
	REQUEST_ANIM_DICT(tlAnimDict_WillyMocappedAnims)	
	REQUEST_ADDITIONAL_TEXT("NIGEL1A", MISSION_TEXT_SLOT)		
	IF HAS_MODEL_LOADED(sWillyPed.mnModel)	
	AND HAS_MODEL_LOADED(sMusicianVehicle.mnModel)	
	AND HAS_ANIM_DICT_LOADED("rcmnigel1a")
	AND HAS_ANIM_DICT_LOADED(tlAnimDict_ClubPedsUpstairs)	
	AND HAS_ANIM_DICT_LOADED(tlAnimDict_WillyMocappedAnims)	
	AND HAVE_CLUB_PED_MODELS_LOADED()
	AND IS_MUSIC_CLUB_INTERIOR_LOADED()
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAVE_ASSETS_FOR_MISSION_INIT_LOADED - return TRUE") ENDIF #ENDIF
		RETURN TRUE	
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAVE_ASSETS_FOR_MISSION_INIT_LOADED - return FALSE") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests and checks to see if the assets are loaded for the collext tooth stage
/// RETURNS:
///    TRUE if all the assets have loaded
FUNC BOOL HAVE_ASSETS_LOADED_FOR_COLLECT_TOOTH_STAGE()
	REQUEST_MODEL(mpToothPickup.mnModel)
	REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1A_TOOTH")
	//REQUEST_ANIM_DICT("pickup_object")	// B*1533138 - tooth pickup anim
	IF HAS_MODEL_LOADED(mpToothPickup.mnModel)
	AND REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1A_TOOTH")	
	//AND HAS_ANIM_DICT_LOADED("pickup_object")		// B*1533138 - tooth pickup anim
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAVE_ASSETS_LOADED_FOR_COLLECT_TOOTH_STAGE - return TRUE") ENDIF #ENDIF
		RETURN TRUE	
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAVE_ASSETS_LOADED_FOR_COLLECT_TOOTH_STAGE - return FALSE") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    reset's the STRUCT_MISSION_PED's iTimer and iFrameCountLastSeenPlayer
PROC RESET_MISSION_PED_TIMERS()
	INT i
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		sClubPed[i].iTimer = 0
		sClubPed[i].iAiDelayTimer = 0
		sClubPed[i].iFrameCountLastSeenPlayer = 0
		RESET_THREAT_SLOT_FOR_ENTITIES(PLAYER_PED_ID(), sClubPed[i].pedIndex)
	ENDFOR
	sWillyPed.iTimer = 0
	sWillyPed.iAiDelayTimer = 0
	sWillyPed.iFrameCountLastSeenPlayer = 0	
	RESET_THREAT_SLOT_FOR_ENTITIES(PLAYER_PED_ID(), sWillyPed.pedIndex)
ENDPROC

/// PURPOSE:
///    initialises all the mission variables
PROC SET_MISSION_VARIABLES()
	INT i
	bDoneObjective_LoseWantedLevel						= FALSE
	bDoneDialogue_BandSayWhereWillyIs					= FALSE
	bDoneDialogue_MusicianBeginFlee						= FALSE
	bDoneDialogue_MusicianKnockedDown					= FALSE
	bDoneDialogue_MusicianLosesTooth					= FALSE
	bDoneDialogue_MusicianSpottedPlayer					= FALSE
	bDoneDialogue_MusicianStartBrawl					= FALSE
	bDoneDialogue_MusicianSurrendered					= FALSE
	bDoneDialogue_MusicianShotAtByPlayer				= FALSE
	bDoneDialogue_PlayerReturnsTofightMusician			= TRUE
	bDoneDialogue_TrevorCommentSneakingInBackdoor		= FALSE
	bDoneDialogue_TrevorKilledWilly						= FALSE
	bDoneDialogue_TrevorLeaveTheAreaComment				= FALSE
	bDoneDialogue_MusicianShoutSecurity					= FALSE
	bDonedialogue_ClubPedsReactThreatened				= FALSE
	bDoneObjective_BeatUpMusician						= FALSE
	bDoneObjective_CollectTooth							= FALSE
	bDoneObjective_LeaveTheArea							= FALSE
	bDoneObjective_EnterClub							= FALSE
	bHasPlayerDamagedMusician							= FALSE
	bHasPlayerScarredClubPedsUpstairs					= FALSE
	bHasOutroPhonecallSucceeded							= FALSE
	bHasWillySeenPlayerAttackHisGroupie					= FALSE
	bPausedDialogue_MusicianSpottedPlayer 				= FALSE
	bPausedDialogue_AmbientUpstairs						= FALSE
	bPausedDialogue_AmbientWillyGroupie 				= FALSE
	bPlayerUsedWeaponToDamageWilly						= FALSE
	bHasPlayerReceivedScriptedWantedLevel				= FALSE
	bShouldPlayerGetWantedLevelForWeaponInClub			= FALSE
	bShouldPlayerGetWantedLevelAfterAttackingWilly		= FALSE
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_MISSION_CONTROLLED_DOORS) - 1)
		bSetMissionControlledDoorsInitialStates[i] = FALSE
	ENDFOR
	bSetExitSceneForWillyAndGroupie						= FALSE
	iClubPedID_ToUpdateThisFrameCounter					= 0
	iDialogueCounter_PlayerLeftMusicianInBrawl			= 0
	iDialogueCounter_PlayerOutOfReachMusicianInBrawl	= 0
	iDialogueCounter_PlayerReturnedMusicianInBrawl		= 0
	iTimer_BackupKnockOutToothTrigger					= 0
	iTimer_DelayForOutroPhonecall						= 0
	iTimer_PlayerReceivesScriptedWantedLevel			= 0
	iCounter_AmbientDialogueUpstairs					= 0
	iCounter_AmbientDialogueWillyGroupie				= 0
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_CLUB_PED_ANIM_TYPE) - 1)
		iSyncedSceneID_ClubPedUpstairsScenes[i] = -1
	ENDFOR
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_Willy_GROUPIE_ANIM_TYPE) - 1)
		iSyncedSceneID_WillyGroupieScenes[i] = -1
	ENDFOR
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherDataLocal.iSyncSceneIndex)
		iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_BASE_2_ANIM] = sRCLauncherDataLocal.iSyncSceneIndex
		CPRINTLN(DEBUG_MISSION, " Update sync scene handle from TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE : sRCLauncherDataLocal.iSyncSceneIndex = ", sRCLauncherDataLocal.iSyncSceneIndex,
							" iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_BASE_2_ANIM] = ", iSyncedSceneID_WillyGroupieScenes[N1A_WGAT_BASE_2_ANIM])
	ENDIF							
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_MISSION_CONTROLLED_DOORS) - 1)
		iMissionControlledDoors[i] = 0
	ENDFOR
	//setup the hash keys for the rock club interior's rooms
	iRockClubRoom[N1A_RCR_MAIN_ROOM]						= GET_HASH_KEY("V_64_Dance")			// main room
	iRockClubRoom[N1A_RCR_MAIN_ENTRANCE]					= GET_HASH_KEY("V_64_Entry_Trans")		// main entrance
	iRockClubRoom[N1A_RCR_MAIN_ENTRANCE_TO_MAIN_ROOM]		= GET_HASH_KEY("V_64_Entry")			// main entrance leading to main room
	iRockClubRoom[N1A_RCR_BACK_ROOM_CORRIDOR_TO_BASEMENT]	= GET_HASH_KEY("V_64_Back")				// upper corridor and stairs leading to basement?
	iRockClubRoom[N1A_RCR_LANDING_OUTSIDE_BASEMENT] 		= GET_HASH_KEY("V_64_Back_Lower")		// landing area outside basement room
	iRockClubRoom[N1A_RCR_BASEMENT]							= GET_HASH_KEY("V_64_Base2")			// basement
	iRockClubRoom[N1A_RCR_CLOAK_ROOM]						= GET_HASH_KEY("V_64_Cloak")			// cloak room front entrance
	iRockClubRoom[N1A_RCR_CORRIDOR_TO_UPSTAIRS]				= GET_HASH_KEY("V_64_Side")				// corridor leading upstairs?
	iRockClubRoom[N1A_RCR_LANDING_UPSTAIRS]					= GET_HASH_KEY("V_64_Side_Upper")		// landing area upstairs
	iRockClubRoom[N1A_RCR_ROOM_UPSTAIRS]					= GET_HASH_KEY("V_64_Upper")			// upstairs room
	iRockClubRoom[N1A_RCR_REAR_ENTRANCE_BOTTOM_STAIRS]		= GET_HASH_KEY("V_64_Rear")				// rear entrance (bottom of stairs)
	iRockClubRoom[N1A_RCR_REAR_ENTRANCE_TOP_STAIRS]			= GET_HASH_KEY("V_64_Rear_Trans")		// rear entrance (up stairs)		
	iDialogueCounter_PlayerLeftMusicianInBrawl				= 0
	iDialogueCounter_PlayerReturnedMusicianInBrawl			= 0
	iDialogueCounter_PlayerOutOfReachMusicianInBrawl		= 0
	fHeading_PlayerMissionStart								= 341.4680
	eN1A_MissionFailedReason								= FAILED_DEFAULT
	tlPausedDialogue_MusicianSpottedPlayer					= ""
	//vCarPark_CentrePos									= << -554.90, 303.18, 82.24 >>
	vMusicClub_CentrePos 									= << -558.2858, 284.4073, 81.1764 >>
	vClearZone_RockClub_Min									= << -577.27405, 270.65054, 77.0 >>
	vClearZone_RockClub_Max									= << -543.17926, 315.29727, 94.0 >>
	vSyncedScenePosition_ClubPedsUpstairs					= << -559.9, 289.366, 84.376 >>
	vSyncedSceneRotation_ClubPedsUpstairs					= << 0.0, 0.0, -6.0 >>
	vSyncedScenePosition_WillyGroupie						= << -552.180, 285.476, 81.976 >> //remember update initial scene if changed! // original prop pos << -552.126, 285.476, 81.976 >>
	vSyncedSceneRotation_WillyGroupie						= << 0.0, 0.0, 79.5 >> //remember update initial scene if changed! // original prop rot << 0.0, 0.0, 80.5 >>
	vPos_PlayerMissionStart									= <<-573.0122, 244.7840, 81.9076>>
	
	//the band
	sWillyPed.mnModel 										= U_M_M_WILLYFIST	// needs to swtich over to U_M_M_WillyFist once it makes it into model_names enum
	sWillyPed.vPosition 									= << -552.66, 287.75, 82.18 >>
	sWillyPed.fHeading 										= 123.75
	// groupie in main room with Willy
	sClubPed[GROUPIE_WILLYS_PIECE].mnModel 					= A_F_Y_BEVHILLS_03
	sClubPed[GROUPIE_WILLYS_PIECE].vPosition 				= << -553.46, 287.34, 82.18 >>
	sClubPed[GROUPIE_WILLYS_PIECE].fHeading 				= -65.86
	// groupie upstairs stood against pillar
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].mnModel 			= A_F_Y_BEVHILLS_03
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].vPosition 		= << -559.94, 285.99, 85.38 >>
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].fHeading 		= -59.06
	// groupie upstairs stood chatting to band member
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].mnModel 			= A_F_Y_BEVHILLS_03
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].vPosition 		= << -558.87, 289.79, 85.38 >>
	sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].fHeading 		= 162.44
	sClubPed[CLUB_PED_BAND_MANAGER].mnModel 				= A_M_Y_VINEWOOD_04
	sClubPed[CLUB_PED_BAND_MANAGER].vPosition 				= << -559.00, 286.09, 85.38 >>
	sClubPed[CLUB_PED_BAND_MANAGER].fHeading 				= 65.92
	sClubPed[CLUB_PED_ROADIE].mnModel 						= A_M_Y_GAY_01	
	sClubPed[CLUB_PED_ROADIE].vPosition 					= << -558.84, 288.97, 85.38 >>
	sClubPed[CLUB_PED_ROADIE].fHeading 						= -7.72
	RESET_MISSION_PED_TIMERS()
	RESET_THREAT_SHAPETESTS()
	// Willy's car
	sMusicianVehicle.mnModel 								= GAUNTLET
	sMusicianVehicle.vPosition 								= << -553.55, 308.61, 82.81 >>
	sMusicianVehicle.fHeading 								= 202.78
	mpToothPickup.mnModel 									= PROP_LD_GOLD_TOOTH
	mpToothPickup.type										= PICKUP_CUSTOM_SCRIPT
	mpToothPickup.vPosition 								= << -554.23, 300.55, 82.19 >> //set an initial pos
	//mpToothPickup.fHeading 									= 0.0
	mpToothPickup.iPlacementFlags = 0
	SET_BIT(mpToothPickup.iPlacementFlags, enum_to_int(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(mpToothPickup.iPlacementFlags, enum_to_int(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	SET_BIT(mpToothPickup.iPlacementFlags, enum_to_int(PLACEMENT_FLAG_FIXED))
	
	eMusicianState 											= N1A_MUSICIAN_STATE_RELAXED
	ePlayerInsideClubStatus									= N1A_RCR_INVALID
	ADD_RELATIONSHIP_GROUP("N1A_PLAYER_HATE_GROUP", relGroupPlayerEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupPlayerEnemy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupPlayerEnemy)	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupPlayerEnemy, RELGROUPHASH_COP)		// make the cops work with the celebs instead of fight them
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relGroupPlayerEnemy)
	ADD_RELATIONSHIP_GROUP("N1A_UNAGRESSIVE", relGroupUnagressive)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupUnagressive, relGroupPlayerEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupPlayerEnemy, relGroupUnagressive)	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupUnagressive, RELGROUPHASH_COP)		// make the cops work with the celebs instead of fight them
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relGroupUnagressive)
	RockClubInteriorIndex = GET_INTERIOR_AT_COORDS(<< -555.5934, 285.7738, 81.1763 >>)	// middle of main room
	
	// register all the doors here to allow at least a frames wait until i try to control them (wait for assets to load acheives this)	
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_DD_TO_MAIN_ROOM_01, V_ILEV_ROC_DOOR1_L, << -561.35, 278.58, 83.13 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_DD_TO_MAIN_ROOM_02, V_ILEV_ROC_DOOR1_R, << -559.55, 278.42, 83.13 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_ENTRANCE_STAFF_AREA_01, V_ILEV_ROC_DOOR3, << -568.88, 281.11, 83.13 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_TO_REAR_ENTRANCE_01, V_ILEV_SS_DOOR5_R, << -567.94, 291.93, 85.52 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_TO_BASEMENT_01, V_ILEV_ROC_DOOR2, << -560.24, 293.01, 82.33 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1A_MCD_INT_BASEMENT_01, V_ILEV_ROC_DOOR2, << -569.80, 293.77, 79.33 >>)
ENDPROC

/// PURPOSE:
///    Creates Willy's vehicle.  Always use this Function when creating it so it remains consistent.
/// PARAMS:
///    mVehicle - vehicle index to use
///    vPos - where to create it
///    fHeading - heading to use
///    bWaitForLoad - wait for the model to load?
/// RETURNS:
///    TRUE if vehicle created, FALSE otherwise
FUNC BOOL CREATE_MUSICIAN_VEHICLE(VEHICLE_INDEX &mVehicle, MODEL_NAMES mnModel, VECTOR vPos, FLOAT fHeading, BOOL bWaitForLoad = TRUE) 
	
	REQUEST_MODEL(mnModel)	
	IF bWaitForLoad = TRUE
		WHILE NOT HAS_MODEL_LOADED(mnModel)
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT HAS_MODEL_LOADED(mnModel)
			RETURN FALSE // model not loaded, can't create vehicle
		ENDIF
	ENDIF
	
	CREATE_SCENE_VEHICLE(mVehicle, mnModel, vPos, fHeading)
	IF IS_ENTITY_ALIVE(mVehicle)
		SET_VEHICLE_COLOUR_COMBINATION(mVehicle, 1)
		SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_UNLOCKED)
		SET_CAN_AUTO_VAULT_ON_ENTITY(mVehicle, FALSE)
	ENDIF	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnModel)
	RETURN TRUE
ENDFUNC 

//PURPOSE: Creates and sets up all the entities needed at the start of the mission
PROC SETUP_SCENE_FOR_START_OF_MISSION()
	INT i
	
	// Musician's vehicle	
	// Assign musican's vehicle over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
		sMusicianVehicle.vehIndex = sRCLauncherDataLocal.vehID[0]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - musician vehicle passed over from launcher")
	ELSE
		CREATE_MUSICIAN_VEHICLE(sMusicianVehicle.vehIndex, sMusicianVehicle.mnModel, sMusicianVehicle.vPosition, sMusicianVehicle.fHeading)
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - musician vehicle created in script, not from launcher")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sMusicianVehicle.mnModel)
	
	// Setup Willy
	// Assign Willy over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		sWillyPed.pedIndex = sRCLauncherDataLocal.pedID[0]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - Willy ped passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(sWillyPed.pedIndex)
			sWillyPed.pedIndex = CREATE_PED(PEDTYPE_MISSION, sWillyPed.mnModel, sWillyPed.vPosition, sWillyPed.fHeading)
			CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - Willy ped created in script")
		ENDIF	
	ENDIF
	IF IS_PED_UNINJURED(sWillyPed.pedIndex)	
		SET_PED_CAN_BE_TARGETTED(sWillyPed.pedIndex, TRUE)	
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sWillyPed.pedIndex, TRUE)		
		SET_PED_DIES_WHEN_INJURED(sWillyPed.pedIndex, TRUE)
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sWillyPed.pedIndex, TRUE)		
		SET_PED_CONFIG_FLAG(sWillyPed.pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_CONFIG_FLAG(sWillyPed.pedIndex, PCF_AllowMissionPedToUseInjuredMovement, TRUE) // make Willy use the injuted animations when TASK_SMART_FLEE_PED is applied
		STOP_PED_SPEAKING(sWillyPed.pedIndex, TRUE)		
		SET_PED_LOD_MULTIPLIER(sWillyPed.pedIndex, 4.0)				// B*1529730 - stop Willy big hair
		//RETAIN_ENTITY_IN_INTERIOR(sWillyPed.pedIndex, RockClubInteriorIndex)	// don't do this for now as it's making the peds invisible inside the interior!!!				
		IF IS_PED_IN_GROUP(sWillyPed.pedIndex)
			REMOVE_PED_FROM_GROUP(sWillyPed.pedIndex)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sWillyPed.pedIndex, relGroupPlayerEnemy)				
		SET_PED_TO_INFORM_RESPECTED_FRIENDS(sWillyPed.pedIndex, 10.0, 5)
		SET_PED_DEFAULT_COMPONENT_VARIATION(sWillyPed.pedIndex)
		SET_ENTITY_HEALTH(sWillyPed.pedIndex, 250)	//set higher to stop issue of Musician dying from ragdoll on beaten up
		SET_PED_COMBAT_MOVEMENT(sWillyPed.pedIndex, CM_DEFENSIVE)
		SET_ENTITY_LOAD_COLLISION_FLAG(sWillyPed.pedIndex, TRUE)	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
		REQUEST_ANIM_DICT(tlAnimDict_WillyMocappedAnims)
		IF HAS_ANIM_DICT_LOADED(tlAnimDict_WillyMocappedAnims)
			SET_PED_ALTERNATE_MOVEMENT_ANIM(sWillyPed.pedIndex, AAT_IDLE, tlAnimDict_WillyMocappedAnims, "base")
			SET_PED_ALTERNATE_MOVEMENT_ANIM(sWillyPed.pedIndex, AAT_WALK, tlAnimDict_WillyMocappedAnims, "walk")
			SET_PED_ALTERNATE_MOVEMENT_ANIM(sWillyPed.pedIndex, AAT_RUN, tlAnimDict_WillyMocappedAnims, "run")
		ENDIF
		sWillyPed.AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
		sWillyPed.iTimer = 0
		sWillyPed.iAiDelayTimer = 0
		sWillyPed.iFrameCountLastSeenPlayer = 0
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL tDebugName = "Willy:"
			SET_PED_NAME_DEBUG(sWillyPed.pedIndex, tDebugName)
		#ENDIF				
	ENDIF	
	SET_MODEL_AS_NO_LONGER_NEEDED(sWillyPed.mnModel)	
	
	// Assign Willy Groupie over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[1])
		sClubPed[GROUPIE_WILLYS_PIECE].pedIndex = sRCLauncherDataLocal.pedID[1]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - Willy's groupie ped passed over from launcher")
	ENDIF	
	// Assign Groupie's upstairs over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[2])
		sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].pedIndex = sRCLauncherDataLocal.pedID[2]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - CLUB_PED_GROUPIE_UPSTAIRS_01 ped passed over from launcher")
	ENDIF
	// Assign Groupie's upstairs over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[3])
		sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].pedIndex = sRCLauncherDataLocal.pedID[3]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - CLUB_PED_GROUPIE_UPSTAIRS_02 ped passed over from launcher")
	ENDIF
	// Assign Band Manage upstairs over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[4])
		sClubPed[CLUB_PED_BAND_MANAGER].pedIndex = sRCLauncherDataLocal.pedID[4]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - CLUB_PED_BAND_MANAGER ped passed over from launcher")
	ENDIF
	// Assign Roadie upstairs over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[5])
		sClubPed[CLUB_PED_ROADIE].pedIndex = sRCLauncherDataLocal.pedID[5]
		CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - CLUB_PED_ROADIE ped passed over from launcher")
	ENDIF
	
	// Create the club peds - groupies, rest of band if they don't already exist
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		IF NOT DOES_ENTITY_EXIST(sClubPed[i].pedIndex)
			sClubPed[i].pedIndex = CREATE_PED(PEDTYPE_MISSION, sClubPed[i].mnModel, sClubPed[i].vPosition, sClubPed[i].fHeading)
			CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - sClubPed[", i, "].pedIndex created in script")
		ENDIF
	ENDFOR
	
	// create the synced scene the club peds will use	
	iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM] = CREATE_SYNCHRONIZED_SCENE(vSyncedScenePosition_ClubPedsUpstairs, vSyncedSceneRotation_ClubPedsUpstairs)
	SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], TRUE)
	SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], FALSE)
	STRING sSyncedSceneAnimName = ""
	TEXT_LABEL_23 tlSyncedSceneAnimName = ""
	
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		IF IS_PED_UNINJURED(sClubPed[i].pedIndex)	
			SET_PED_DIES_WHEN_INJURED(sClubPed[i].pedIndex, TRUE)		
			SET_PED_CONFIG_FLAG(sClubPed[i].pedIndex, PCF_DontInfluenceWantedLevel, TRUE)
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sClubPed[i].pedIndex, TRUE)	
			//RETAIN_ENTITY_IN_INTERIOR(sClubPed[i].pedIndex, RockClubInteriorIndex)	// don't do this for now as it's making the peds invisible inside the interior!!!
			IF IS_PED_IN_GROUP(sClubPed[i].pedIndex)
				REMOVE_PED_FROM_GROUP(sClubPed[i].pedIndex)
			ENDIF
			SET_PED_RELATIONSHIP_GROUP_HASH(sClubPed[i].pedIndex, relGroupUnagressive)
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(sClubPed[i].pedIndex, 10.0, 5)	
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_DISABLE_HANDS_UP, FALSE)
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_USE_VEHICLE, FALSE)
			//SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_PREFER_PAVEMENTS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(sClubPed[i].pedIndex, FA_CAN_SCREAM, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sClubPed[i].pedIndex, CA_ALWAYS_FIGHT, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sClubPed[i].pedIndex, CA_ALWAYS_FLEE, TRUE)	
			SET_PED_COMBAT_MOVEMENT(sClubPed[i].pedIndex, CM_DEFENSIVE)
			SWITCH i
				CASE CLUB_PED_BAND_MANAGER
					SET_PED_CAN_BE_TARGETTED(sClubPed[i].pedIndex, TRUE)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										
					tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
					sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
					CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - task sync scene : ", sSyncedSceneAnimName, " for ped ID : ", i)
					TASK_SYNCHRONIZED_SCENE(sClubPed[i].pedIndex, iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
										SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS)
					sClubPed[i].AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
				CASE CLUB_PED_ROADIE
					SET_PED_CAN_BE_TARGETTED(sClubPed[i].pedIndex, TRUE)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 3, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
					
					tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
					sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
					CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - task sync scene : ", sSyncedSceneAnimName, " for ped ID : ", i)
					TASK_SYNCHRONIZED_SCENE(sClubPed[i].pedIndex, iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
										SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					sClubPed[i].AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
				CASE GROUPIE_WILLYS_PIECE
					SET_PED_CAN_BE_TARGETTED(sClubPed[i].pedIndex, FALSE)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,7), 0, 1, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
					
					sClubPed[i].AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
					
					// set the base by default if it's not runnings
					SET_WILLY_GROUPIE_SYNCED_ANIM(N1A_WGAT_BASE_2_ANIM)
				BREAK
				CASE CLUB_PED_GROUPIE_UPSTAIRS_01
					SET_PED_CAN_BE_TARGETTED(sClubPed[i].pedIndex, FALSE)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,7), 1, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
					
					tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
					sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
					CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - task sync scene : ", sSyncedSceneAnimName, " for ped ID : ", i)
					TASK_SYNCHRONIZED_SCENE(sClubPed[i].pedIndex, iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
										SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					sClubPed[i].AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
				CASE CLUB_PED_GROUPIE_UPSTAIRS_02
					SET_PED_CAN_BE_TARGETTED(sClubPed[i].pedIndex, FALSE)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,7), 1, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(sClubPed[i].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
					
					tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
					sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
					CPRINTLN(DEBUG_MISSION, "SETUP_SCENE_FOR_START_OF_MISSION - task sync scene : ", sSyncedSceneAnimName, " for ped ID : ", i)
					TASK_SYNCHRONIZED_SCENE(sClubPed[i].pedIndex, iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
										SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					sClubPed[i].AI = N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
			ENDSWITCH
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sClubPed[i].pedIndex, TRUE)
			sClubPed[i].iTimer = 0
			sClubPed[i].iAiDelayTimer = 0
			sClubPed[i].iFrameCountLastSeenPlayer = 0
			#IF IS_DEBUG_BUILD
				TEXT_LABEL tDebugName = "ClubPed: "
				tDebugName += i
				SET_PED_NAME_DEBUG(sClubPed[i].pedIndex, tDebugName)
			#ENDIF			
		ENDIF	
		SET_MODEL_AS_NO_LONGER_NEEDED(sClubPed[i].mnModel)
	ENDFOR
ENDPROC

//PURPOSE: Initialization of the mission
PROC INIT_MISSION()	
	
	SET_MISSION_VARIABLES()
	//INFORM_MISSION_STATS_OF_MISSION_START_NIGEL_1_A()
	
	#IF IS_DEBUG_BUILD	
		
		// This needs setting up first to ensure debug print can be displayed when loading assets
		SETUP_MISSION_WIDGET()	
		
		// Stage skipping   
	    mSkipMenu[Z_SKIP_ENTER_THE_MUSIC_CLUB].sTxtLabel = "Enter the club"                
	    mSkipMenu[Z_SKIP_FIND_CELEB].sTxtLabel = "Find Willy" 
	    mSkipMenu[Z_SKIP_KNOCK_OUT_TOOTH].sTxtLabel = "Beat up Willy"
		mSkipMenu[Z_SKIP_COLLECT_TOOTH].sTxtLabel = "Collect Willy's tooth" 
		mSkipMenu[Z_SKIP_LEAVE_THE_AREA].sTxtLabel = "Leave the area" 
		mSkipMenu[Z_SKIP_OUTRO_PHONECALL].sTxtLabel = "Outro Phonecall" 
		mSkipMenu[Z_SKIP_MISSION_PASSED].sTxtLabel = "Mission Passed"
	#ENDIF
	
	RC_CLEANUP_LAUNCHER()	// clean's up the launcher's version of SETUP_AREA_FOR_MISSION - blocking areas
	SETUP_WORLD_STATE_FOR_MISSION()	// needs to be no wait between mission trigger and resetup of scenario blocking area and ped non creation zone
	
	WHILE NOT HAVE_ASSETS_FOR_MISSION_INIT_LOADED()
		WAIT(0)
	ENDWHILE
	
	REGISTER_SCRIPT_WITH_AUDIO()
	ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK, FALSE)
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	CLEAR_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLA_KILLED_WILLIE))	// ensure this bit is initially clear when starting the mission
	
	SETUP_SCENE_FOR_START_OF_MISSION()
ENDPROC

/// PURPOSE:
///    Removes all the assets loaded by the mission, used in mission cleanup and reset mission functions.
/// PARAMS:
///    bClearTextSlots - if TRUE, the MISSION_TEXT_SLOT will be cleared
PROC UNLOAD_ALL_MISSION_ASSETS(BOOL bClearTextSlots = TRUE)
	INT i
	REMOVE_ANIM_DICT("rcmnigel1a")
	REMOVE_ANIM_DICT(tlAnimDict_ClubPedsUpstairs)
	REMOVE_ANIM_DICT(tlAnimDict_WillyMocappedAnims)	
	REMOVE_ANIM_DICT(tlAnimDict_WillyMocappedHurtAnims)
	//REMOVE_ANIM_DICT("pickup_object")	// B*1533138 - tooth pickup anim
	RELEASE_SCRIPT_AUDIO_BANK()
	UNREGISTER_SCRIPT_WITH_AUDIO()	
	SET_MODEL_AS_NO_LONGER_NEEDED(sMusicianVehicle.mnModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(mpToothPickup.mnModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(sWillyPed.mnModel)
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		SET_MODEL_AS_NO_LONGER_NEEDED(sClubPed[i].mnModel)
	ENDFOR
	IF IS_INTERIOR_READY(RockClubInteriorIndex)
		SET_INTERIOR_ACTIVE(RockClubInteriorIndex, FALSE)
		UNPIN_INTERIOR(RockClubInteriorIndex)
	ENDIF
	IF bClearTextSlots
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all of the blips used in the mission, checking if they exist.
PROC DELETE_ALL_MISSION_BLIPS()
	SAFE_REMOVE_BLIP(blipMusicClub)
	SAFE_REMOVE_BLIP(sWillyPed.blipIndex)
	SAFE_REMOVE_BLIP(mpToothPickup.blipIndex)
ENDPROC

/// PURPOSE:
///    Deletes all of the mission entities checking if they exist
///    Used in Mission Failed, when faded out.
/// PARAMS:
///    bDelete - if true all entities will be deleted, else released
PROC CLEANUP_ALL_MISSION_ENTITIES(BOOL bDelete = FALSE)
	INT i
	SAFE_REMOVE_PICKUP(mpToothPickup.index)
	IF IS_PED_UNINJURED(sWillyPed.pedIndex)
		SET_PED_KEEP_TASK(sWillyPed.pedIndex, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(sWillyPed.pedIndex, FALSE)
		STOP_PED_SPEAKING(sWillyPed.pedIndex, FALSE)
	ENDIF
	SAFE_REMOVE_PED(sWillyPed.pedIndex, bDelete)	
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		IF IS_PED_UNINJURED(sClubPed[i].pedIndex)
			SET_PED_KEEP_TASK(sClubPed[i].pedIndex, TRUE)
		ENDIF
		SAFE_REMOVE_PED(sClubPed[i].pedIndex, bDelete)
	ENDFOR
	IF IS_VEHICLE_OK(sMusicianVehicle.vehIndex)
		SET_CAN_AUTO_VAULT_ON_ENTITY(sMusicianVehicle.vehIndex, TRUE)
		SET_CAN_CLIMB_ON_ENTITY(sMusicianVehicle.vehIndex, TRUE)
	ENDIF
	SAFE_REMOVE_VEHICLE(sMusicianVehicle.vehIndex, bDelete)
	SAFE_REMOVE_VEHICLE(vehIndex_MissionReplayRestore, bDelete)
ENDPROC

/// PURPOSE:
///    Mission cleanup
/// PARAMS:
///    bDeleteAll - if TRUE all entities are deleted, else released
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC MISSION_CLEANUP(BOOL bDeleteAll = FALSE, BOOL bClearTextSlots = TRUE)
	INT i
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(TRUE)
	ENDIF
	KILL_ANY_CONVERSATION()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	DISABLE_CELLPHONE(FALSE)
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	//RENDER_SCRIPT_CAMS(FALSE, FALSE) causes camera to snap on mission passed
	SET_TIME_SCALE(1)
	HIDE_ACTIVE_PHONE(FALSE)
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingAreaRockClub)
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", TRUE)		// Stop ambient security spawning
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_CLIPBOARD", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)	// Stop truck pulling out in the car park
	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_DOORMAN_01, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_BOUNCER_01, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_RockClub_Min, vClearZone_RockClub_Max, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sMusicianVehicle.mnModel, FALSE)
	
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 2)		// "TREVOR"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)		// "NIGEL"	 / "GROUPIE1"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "MRSTHORNHILL"	/ "GROUPIE2"	
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 5)		// "BANDLEADER"
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	WAIT_FOR_CUTSCENE_TO_STOP()
	
	// Reset the exterior doors to locked to the club are handed by Kenneth's building controller
	//SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
	//SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
	
	// clear anything blocking the exterior doors
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-564.67102, 275.48468, 82.01414>>, <<-564.32562, 278.74033, 84.25044>>, 2.1, <<-564.61438, 274.18298, 82.01967>>, 86)
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-561.86169, 294.47070, 86.49855>>, <<-562.15326, 291.70157, 88.57632>>, 1.5, <<-554.92700, 294.07227, 84.36716>>, -95)
	
	SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_LOCKED)	// DOORSTATE_LOCKED
	SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_LOCKED)	// DOORSTATE_LOCKED
	CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set club exteior doors state - DOORSTATE_FORCE_LOCKED_THIS_FRAME")
	
	//Enable the interior needs to be disabled in mission_cleanup
	SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_ROCKCLUB, TRUE)
	CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, TRUE)")
	
	// remove mission controlled doors from system
	FOR i = 0 TO (ENUM_TO_INT(N1A_MAX_MISSION_CONTROLLED_DOORS) - 1)
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[i])
			REMOVE_DOOR_FROM_SYSTEM(iMissionControlledDoors[i])
			CPRINTLN(DEBUG_MISSION, " MISSION_CLEANUP - ", " REMOVE_DOOR_FROM_SYSTEM : ", iMissionControlledDoors[i])
		ENDIF
	ENDFOR
	IF IS_PLAYER_PLAYING(PLAYER_ID())		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	REMOVE_RELATIONSHIP_GROUP(relGroupPlayerEnemy)
	REMOVE_RELATIONSHIP_GROUP(relGroupUnagressive)	
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1A_SCENE")
		STOP_AUDIO_SCENE("NIGEL_1A_SCENE")
	ENDIF
	SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)	// B*1516072 - allow convo during ragdoll
	RESET_THREAT_SHAPETESTS()
	DELETE_ALL_MISSION_BLIPS()	
	CLEANUP_ALL_MISSION_ENTITIES(bDeleteAll)	
	UNLOAD_ALL_MISSION_ASSETS(bClearTextSlots)	
	#IF IS_DEBUG_BUILD
		CLEANUP_MISSION_WIDGETS()
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - done")
ENDPROC

/// PURPOSE:
///    Handles call to MISSION_CLEANUP and terminates the thread
PROC Script_Cleanup()	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MISSION_CLEANUP()	
	ENDIF	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)	
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Mission Passed
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Script_Passed()")
	SAFE_FADE_SCREEN_IN_FROM_BLACK()
	Random_Character_Passed(CP_RAND_C_NIG1A)
	Script_Cleanup()	
ENDPROC

/// PURPOSE:
///    updates the reason for mission failed
///    in order of most important fail reason so if multiple fails conditions have been set, we use the most important
PROC UPDATE_FAIL_REASON()

	// allow player to kill Willy once his tooth has been knocked out.
	IF eMissionStage < MISSION_STAGE_COLLECT_TOOTH
		IF DOES_ENTITY_EXIST(sWillyPed.pedIndex)
			IF IS_ENTITY_DEAD(sWillyPed.pedIndex)
			OR IS_PED_INJURED(sWillyPed.pedIndex)
				eN1A_MissionFailedReason = FAILED_MUSICIAN_KILLED
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_FAIL_REASON - MUSICIAN KILLED") ENDIF #ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has failed
PROC MISSION_FAILED_CHECKS()

	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED
			UPDATE_FAIL_REASON()
			IF eN1A_MissionFailedReason <> FAILED_DEFAULT
				SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    get the given ped's current movement state
///    NOTE: for use with the ped detection system
/// PARAMS:
///    pedIndex - ped to get update for 
///    bDoDeadCheck - check ped is alive first
/// RETURNS:
///    N1A_PED_MOVEMENT_STATE for the ped
FUNC N1A_PED_MOVEMENT_STATE GET_UPDATED_PED_MOVEMENT_STATE(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)

	N1A_PED_MOVEMENT_STATE eCurrent_PedMovementState
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_DEFAULT
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() return N1A_PEDMOVEMENTSTATE_DEFAULT ped not alive") ENDIF #ENDIF
			RETURN eCurrent_PedMovementState
		ENDIF
	ENDIF	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_IN_VEHICLE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_IN_VEHICLE") ENDIF #ENDIF
	ELIF GET_PED_STEALTH_MOVEMENT(pedIndex)
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_STEALTH
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_STEALTH") ENDIF #ENDIF
	ELIF IS_PED_IN_COVER(pedIndex)
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_COVER
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_COVER") ENDIF #ENDIF
	ELIF IS_PED_WALKING(pedIndex)
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_WALKING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_WALKING") ENDIF #ENDIF
	ELIF IS_PED_RUNNING(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_RUNNING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_RUNNING") ENDIF #ENDIF
	ELIF IS_PED_SPRINTING(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_SPRINTING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_SPRINTING") ENDIF #ENDIF
	ELSE
		eCurrent_PedMovementState = N1A_PEDMOVEMENTSTATE_DEFAULT
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1A_PEDMOVEMENTSTATE_DEFAULT") ENDIF #ENDIF		
	ENDIF
	RETURN eCurrent_PedMovementState
ENDFUNC

/// PURPOSE:
///    make sure the generated spawn pos for the tooth pickup isn't in an unaccessible area
///    if it is update the spawn pos to a safe hard coded position
/// PARAMS:
///    vCoords - the position to check
///    fDistPlayerClearOfPickupCoord - added to the constant DIST_PLAYER_CLASSED_AS_COLLECTED_TOOTH_PICKUP to check that the spawn point is not within this range from the player
/// RETURNS:
///    TRUE if original position doesn't need updating or unsafe position was successfully updated
FUNC BOOL UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA(VECTOR &vCoords, FLOAT fDistPlayerClearOfPickupCoord = 0.25)
	
	VECTOR vTempCoords
	INT i, iVariations
	BOOL bCoordsNeedUpdating = FALSE
	
	// north side of the stage encasing the speakers to the front and rear
	IF IS_POINT_IN_ANGLED_AREA(vCoords, <<-552.339050,288.716888,79.176384>>, <<-549.225220,288.553375,86.378616>>, 5.500000)
		bCoordsNeedUpdating = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA - coords in area 1 : vCoords = ", vCoords, " FrameCount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	
	// south side of the stage encasing the speakers to the front and rear
	ELIF IS_POINT_IN_ANGLED_AREA(vCoords, <<-552.902100,281.694794,79.976616>>, <<-549.659912,281.181702,89.677162>>, 4.000000)
		bCoordsNeedUpdating = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA - coords in area 2 : vCoords = ", vCoords, " FrameCount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ENDIF
	
	// get a hard corded spawn pos if coords need updating
	IF bCoordsNeedUpdating
		i = 0
		iVariations = 12	
		REPEAT iVariations i
			SWITCH i
				CASE 0	vTempCoords = << -552.70709, 290.01120, 81.2 >>			BREAK	// north corner near stage
				CASE 1	vTempCoords = << -552.81635, 287.68204, 81.2 >>			BREAK	// running South from corner near stage
				CASE 2	vTempCoords = << -553.06049, 285.44385, 81.2 >>			BREAK	// running South from corner near stage
				CASE 3	vTempCoords = << -553.43890, 282.57767, 81.2 >>			BREAK	// running South from corner near stage
				CASE 4	vTempCoords = << -553.91620, 279.92017, 81.2 >>			BREAK	// running South from corner near stage
				CASE 5	vTempCoords = << -555.19159, 290.48895, 81.2 >>			BREAK	// North middle of the room
				CASE 6	vTempCoords = << -555.47510, 287.19907, 81.2 >>			BREAK	// running south middle of the room
				CASE 7	vTempCoords = << -555.20837, 284.90021, 81.2 >>			BREAK	// running south middle of the room
				CASE 8	vTempCoords = << -555.44061, 282.68533, 81.2 >>			BREAK	// running south middle of the room
				CASE 9	vTempCoords = << -555.85413, 280.33383, 81.2 >>			BREAK	// running south middle of the room
				// two back up points quiet far from the stage
				CASE 10	vTempCoords = << -557.79437, 289.95535, 81.2 >>			BREAK	// way from stage north point
				CASE 11	vTempCoords = << -558.29779, 284.79138, 81.2 >>			BREAK	// way from stage south point
			ENDSWITCH
			
			IF (VDIST(vPlayerPos, vCoords) > (DIST_PLAYER_CLASSED_AS_COLLECTED_TOOTH_PICKUP + fDistPlayerClearOfPickupCoord))
				vCoords = vTempCoords
				CPRINTLN(DEBUG_MISSION, "UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA - updated vCoords = ", vCoords, " FrameCount : ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
		ENDREPEAT		
	ELSE
		CPRINTLN(DEBUG_MISSION, "UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA - return TRUE update not needed, original coords were safe vCoords = ", vCoords, " FrameCount : ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA - return FALSE failed to gen new coords. FrameCount : ", GET_FRAME_COUNT())
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Retrieve a suitable place to spawn the tooth pickup.
///    should be close to the musician
///    but not under the player as we don't want him instantly collecting it
/// PARAMS:
///    vCoords - the vector which will be used for the pickup placement
///    fDistPlayerClearOfPickupCoord - added to the constant DIST_PLAYER_CLASSED_AS_COLLECTED_TOOTH_PICKUP to check that the spawn point is not within this range from the player
/// RETURNS:
///    the Position to spawn the pickup
FUNC BOOL GET_TOOTH_PICKUP_COORDS(VECTOR &vCoords, FLOAT fDistPlayerClearOfPickupCoord = 1.00)	// B*1533138 - pushed out range for tooth spawn
	vCoords = << 0.0, 0.0, 0.0 >>	//reset the passed in vector 
	IF DOES_ENTITY_EXIST(sWillyPed.pedIndex)
		vCoords = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(sWillyPed.pedIndex, FALSE), 1.5, 4.0)			
		//check to see that a valid coord was returned by GET_SAFE_PICKUP_COORDS
		IF NOT ARE_VECTORS_EQUAL(vCoords, << 0.0, 0.0, 0.0 >>)
			// make sure the tooth is somewhere accessible if not values are updated to a hard coded safe position
			IF UPDATE_GENERATED_TOOTH_SPAWN_POS_IF_IN_UNSAFE_AREA(vCoords, fDistPlayerClearOfPickupCoord)
				//Check the player isn't on top off the Position
				IF (VDIST(vPlayerPos, vCoords) > (DIST_PLAYER_CLASSED_AS_COLLECTED_TOOTH_PICKUP + fDistPlayerClearOfPickupCoord))		
					CPRINTLN(DEBUG_MISSION, "GET_TOOTH_PICKUP_COORDS - RETURN TRUE - coords = ", vCoords, " FrameCount : ", GET_FRAME_COUNT())
					RETURN TRUE				
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	vCoords = << 0.0, 0.0, 0.0 >>
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_TOOTH_PICKUP_COORDS - RETURN FALSE - unable to find suitable coord FrameCount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check the difference between two coord's Z to see if they are more than the Threshold
///    To be used when deciding when to stop the combat task, as combat doesn't handle player standing slightly higher ground, e.g. on prop / vehicle
/// PARAMS:
///    v1 - first entity coords
///    v2 - second entity coords
///    fThreshold - the difference the Z coords has to be greater than
/// RETURNS:
///    TRUE if z difference is greater than fThreshold
FUNC BOOL IS_DIFFERENCE_IN_Z_COORDS_MORE_THAN_THRESHOLD(VECTOR v1, VECTOR v2, FLOAT fThreshold = Z_DIFFERENCE_COMBAT_TASK_STOPS_WORKING)
	FLOAT fDiff_Zcoords
	fDiff_Zcoords = v1.Z
	fDiff_Zcoords -= v2.Z
	fDiff_Zcoords = ABSF(fDiff_Zcoords)
	IF fDiff_Zcoords > fThreshold
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   if the player hangs around the club or is pursuing Willy, give player a wanted level after period of time 
PROC MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
	IF bShouldPlayerGetWantedLevelForWeaponInClub
	OR bShouldPlayerGetWantedLevelAfterAttackingWilly
		IF NOT bHasPlayerReceivedScriptedWantedLevel
			INT iTimeTillScriptedWantedLevel = 0
			IF bShouldPlayerGetWantedLevelAfterAttackingWilly
				iTimeTillScriptedWantedLevel = NIG1A_TIME_DELAY_WANTED_FOR_WILLY_ASSAULT	// 5 sec delay for
			ELIF bShouldPlayerGetWantedLevelForWeaponInClub
				iTimeTillScriptedWantedLevel = NIG1A_TIME_DELAY_WANTED_FOR_WEAPON_IN_CLUB	// 10 sec delay for
			ENDIF
			IF iTimeTillScriptedWantedLevel != 0	// check we do want a wanted level to be applied
				IF HAS_TIME_PASSED(iTimer_PlayerReceivesScriptedWantedLevel, iTimeTillScriptedWantedLevel)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					IF bShouldPlayerGetWantedLevelAfterAttackingWilly
						IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMusicClub_CentrePos, NIG1A_PLAYER_LEFT_THE_CLUB_DIST)	// says club name so only do this if he's within range
							PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_NIGEL_1A_01", 0.0)	// B*1551720 - Willy assault mentioned
						ENDIF
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL - PLAY_POLICE_REPORT(SCRIPTED_SCANNER_REPORT_NIGEL_1A_01, 0.0)") ENDIF #ENDIF
					ENDIF
					bHasPlayerReceivedScriptedWantedLevel = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL - bHasPlayerReceivedScriptedWantedLevel set TRUE") ENDIF #ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				// don't drop wanted level if inside the club or with in the leave area dist
				IF ePlayerInsideClubStatus <> N1A_RCR_INVALID
				AND ePlayerInsideClubStatus <> N1A_MAX_NUM_ROCK_CLUB_ROOMS
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL - player still in the club - wanted level kept on") ENDIF #ENDIF
				ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMusicClub_CentrePos, NIG1A_PLAYER_LEFT_THE_CLUB_DIST)
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL - player still in vicinity of the club - wanted level kept on") ENDIF #ENDIF
				//ELIF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, NIG1A_PLAYER_CLASSED_AS_PURSUING_Willy_DIST)
				//	bSetWantedLevel = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the CAN_PED_SEE_PED check and sets the specified FrameCounter if it returned TRUE
///    counter is used as a grace period for checking the ped has seen the player, as CAN_PED_SEE_PED takes several frames to return
/// PARAMS:
///    pedIndex - the ped to test against the player
///    iFrameCountLastSeenPlayer - the ped's last seen player counter
///    fOverride_PedViewCone - the ped's viewing cone - 170 is the default value
PROC UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(PED_INDEX pedIndex, INT &iFrameCountLastSeenPlayer, FLOAT fOverride_PedViewCone = 170.0)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF CAN_PED_SEE_PED(pedIndex, PLAYER_PED_ID(), fOverride_PedViewCone)
			iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER - CAN_PED_SEE_PED returned TRUE so set frame count to : ", iFrameCountLastSeenPlayer) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC
	
/// PURPOSE:
///    checks if the player's current weapon is lethal
/// PARAMS:
///    bIncludeProjectilesCheck - if true, checks for grenades,stickybombs etc
///    bIncludeLeathalMeleeWeapons - if true check includes knife, hammer, crowbar etc
///    bIncludeStunGunAsLethal - added parameter as we now want pool peds to surrender to stun gun attack
/// RETURNS:
///    true - if lethal
FUNC BOOL IS_PLAYER_CURRENT_WEAPON_LETHAL(BOOL bIncludeProjectilesCheck = TRUE, BOOL bIncludeLeathalMeleeWeapons = FALSE, BOOL bIncludeStunGunAsLethal = TRUE)	
	//different dialogue based on player weapon
	WEAPON_TYPE currentPlayerWeapon
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
	SWITCH currentPlayerWeapon
		CASE WEAPONTYPE_PISTOL
			FALLTHRU
		CASE WEAPONTYPE_COMBATPISTOL
			FALLTHRU
		CASE WEAPONTYPE_APPISTOL
			FALLTHRU
		CASE WEAPONTYPE_MICROSMG
			FALLTHRU
		CASE WEAPONTYPE_DLC_SNSPISTOL
			FALLTHRU
		CASE WEAPONTYPE_SMG
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTRIFLE
			FALLTHRU
		CASE WEAPONTYPE_CARBINERIFLE
			FALLTHRU
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			FALLTHRU
		CASE WEAPONTYPE_ADVANCEDRIFLE
			FALLTHRU
		CASE WEAPONTYPE_MG
			FALLTHRU
		CASE WEAPONTYPE_COMBATMG
			FALLTHRU
		CASE WEAPONTYPE_PUMPSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SNIPERRIFLE
			FALLTHRU
		CASE WEAPONTYPE_HEAVYSNIPER
			FALLTHRU
		CASE WEAPONTYPE_REMOTESNIPER
			FALLTHRU
		CASE WEAPONTYPE_GRENADELAUNCHER
			FALLTHRU
		CASE WEAPONTYPE_RPG
			FALLTHRU
		CASE WEAPONTYPE_MINIGUN
			RETURN TRUE
	ENDSWITCH	
	IF bIncludeProjectilesCheck
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_GRENADE
				FALLTHRU
			CASE WEAPONTYPE_STICKYBOMB
				FALLTHRU
			CASE WEAPONTYPE_MOLOTOV
				RETURN TRUE
		ENDSWITCH
	ENDIF
	IF bIncludeLeathalMeleeWeapons
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_KNIFE
				FALLTHRU
			CASE WEAPONTYPE_HAMMER
				FALLTHRU
			CASE WEAPONTYPE_CROWBAR
				FALLTHRU
			CASE WEAPONTYPE_DLC_BOTTLE
				FALLTHRU
		ENDSWITCH
	ENDIF
	IF bIncludeStunGunAsLethal
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check to see if the player has started fighting the ped
/// PARAMS:	
///    pedIndex - Ped who player is starting fight with
///    iFrameCounter_PedLastSeePlayer - the frame CAN_PED_SEE_PED last returned true for the pedIndex
///    bTestPlayerInMeleeCombat if TRUE IS_PED_IN_MELEE_COMBAT check is performed along with dist and see check
///    fPlayerDetectionDist - additional dist cap
/// RETURNS:
///    True if the player is targetting the ped, or has damaged the ped
FUNC BOOL HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(PED_INDEX pedIndex, INT iFrameCounter_PedLastSeePlayer, BOOL bTestPlayerInMeleeCombat = TRUE, FLOAT fPlayerDetectionDist = 10.0)
	IF IS_PED_UNINJURED(pedIndex)
		IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
		OR (bTestPlayerInMeleeCombat AND IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID()))
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fPlayerDetectionDist)	//and is with in the range
				IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER TARGETTED PED return TRUE : iFrameCounter_PedLastSeePlayer = ", iFrameCounter_PedLastSeePlayer, " Frame Count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					RETURN TRUE
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED - HAS_FRAME_COUNTER_PASSED return FALSE - iFrameCounter_PedLastSeePlayer = ", iFrameCounter_PedLastSeePlayer, " Frame Count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
			//CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER DAMAGED PED") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER touching PED") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player has a weapon and is intimidating a ped enough that they should react
/// PARAMS:
///    pedIndex - Ped who the player is intimidating
///    iFrameCounter_PedLastSeePlayer - the frame count for the last time the pedIndex could see the player
///    bWeaponDrawInClubClassedAsIntimidating - player seen in club with weapon draw will return true
///    fPlayerDetectionDist - Player is automatically classed as intimidating when shooting or targetting within this range
///    fBulletRadiusCheck - Used for IS_BULLET_IN_AREA check around pedIndex
///    fProjectileRadiusCheck - Used for IS_PROJECTILE_IN_AREA check around pedIndex
/// RETURNS:
///    True if bullet is in area around ped, if player is shooting, if player is targetting or free aiming at ped in view or if damaged.
FUNC BOOL IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(PED_INDEX pedIndex, INT iFrameCounter_PedLastSeePlayer, BOOL bWeaponDrawInClubClassedAsIntimidating = FALSE, FLOAT fPlayerDetectionDist = 50.0, FLOAT fBulletRadiusCheck = 8.0, FLOAT fProjectileRadiusCheck = 15.0)
	IF IS_PED_UNINJURED(pedIndex)	
		IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
			VECTOR vTemp_PedPos = GET_ENTITY_COORDS(pedIndex)		
			IF IS_BULLET_IN_AREA(vTemp_PedPos, fBulletRadiusCheck, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - bullet in area around ped : ", "return TRUE") ENDIF #ENDIF
				RETURN TRUE
			ENDIF		
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fPlayerDetectionDist)	//and is with in the range
				IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
					IF bWeaponDrawInClubClassedAsIntimidating
						IF ePlayerInsideClubStatus <> N1A_RCR_INVALID	// make sure player is in the club first
						AND ePlayerInsideClubStatus <> N1A_MAX_NUM_ROCK_CLUB_ROOMS
							// B*1511108 - try to ensure weapon is visible
							IF IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
							OR IS_PED_RELOADING(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - player weapon seen in club : ", "return TRUE") ENDIF #ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - playing free aiming or targeting ped : ", "return TRUE") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
					IF IS_PED_SHOOTING(PLAYER_PED_ID())	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - player shooting with weapon : ", "return TRUE") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
					IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - player touching ped with weapon drawn : ", "return TRUE") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - ped can't see player : ", "return FALSE") ENDIF #ENDIF
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - player not in range of ped : ", "return FALSE") ENDIF #ENDIF
			ENDIF				
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - ped damaged by player with weapon : ", "return TRUE") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF			
			VECTOR vMin, vMax // is one of the player's projectiles nearby?
			vMin = vTemp_PedPos
			vMax = vMin
			vMin.x= vMin.x - fProjectileRadiusCheck
			vMin.y = vMin.y -fProjectileRadiusCheck
			vMin.z = vMin.z - fProjectileRadiusCheck
			vMax.x = vMax.x + fProjectileRadiusCheck
			vMax.y = vMax.y + fProjectileRadiusCheck
			vMax.z = vMax.z + fProjectileRadiusCheck
			IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
				// if the ped is in the club, only check for projectiles in the club
				IF IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(pedIndex)
					VECTOR vProjectilePos
					IF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_GRENADE, vProjectilePos)
					ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_SMOKEGRENADE, vProjectilePos)
					ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_STICKYBOMB, vProjectilePos)
					ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_MOLOTOV, vProjectilePos)
						// check the projectile is inside the club
						IF IS_POSITION_INSIDE_MUSIC_CLUB_INTERIOR(vProjectilePos)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - projectile in area around ped IN CLUB : ", "return TRUE") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF	
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - projectile in area around ped : ", "return TRUE") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED - player current weapon not leathal : ", "return FALSE") ENDIF #ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Checks if the player is intimidating a ped enough that they should react
///    NOTE: doesn't take into account player weapon
/// PARAMS:
///    pedIndex - Ped who the player is intimidating
///    iFrameCounter_PedLastSeePlayer - the frame count for the last time the pedIndex could see the player
///    bIncludeBumpedCheck - if true func will return TRUE for player bumping into the ped
///    fPlayerDetectionDist - Player is automatically classed as intimidating when shooting or targetting within this range
///    fBulletRadiusCheck - Used for IS_BULLET_IN_AREA check around pedIndex
///    fProjectileRadiusCheck - Used for IS_PROJECTILE_IN_AREA check around pedIndex
/// RETURNS:
///    True if bullet is in area around ped, if player is shooting, if player is targetting or free aiming at ped in view or if damaged.
FUNC BOOL IS_PLAYER_INTIMIDATE_SPECIFIC_PED(PED_INDEX pedIndex, INT iFrameCounter_PedLastSeePlayer, BOOL bIncludeBumpedCheck = TRUE, FLOAT fPlayerDetectionDist = 20.0, FLOAT fBulletRadiusCheck = 8.0, FLOAT fProjectileRadiusCheck = 15.0)
	IF IS_PED_UNINJURED(pedIndex)	
		VECTOR vTemp_PedPos = GET_ENTITY_COORDS(pedIndex)	
		IF IS_BULLET_IN_AREA(vTemp_PedPos, fBulletRadiusCheck, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - bullet in area around ped") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fPlayerDetectionDist)	//and is with in the range
			IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
				IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
					// B*1511108 - try to ensure weapon is visible
					IF IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
					OR IS_PED_RELOADING(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing has leathal weapon in view") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing free aiming or targeting ped") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_PED_SHOOTING(PLAYER_PED_ID())	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player shooting with weapon") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
				OR IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player seen in combat or performing stealth kill") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - ped damaged by player with weapon") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF bIncludeBumpedCheck
			IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())
				IF IS_PED_RAGDOLL(pedIndex)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED check - PLAYER touching PED and ped is ragdoll") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		VECTOR vMin, vMax // is one of the player's projectiles nearby?
		vMin = vTemp_PedPos
		vMax = vMin
		vMin.x= vMin.x - fProjectileRadiusCheck
		vMin.y = vMin.y -fProjectileRadiusCheck
		vMin.z = vMin.z - fProjectileRadiusCheck
		vMax.x = vMax.x + fProjectileRadiusCheck
		vMax.y = vMax.y + fProjectileRadiusCheck
		vMax.z = vMax.z + fProjectileRadiusCheck
		IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
			// if the ped is in the club, only check for projectiles in the club
			IF IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(pedIndex)
				VECTOR vProjectilePos
				IF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_GRENADE, vProjectilePos)
				ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_SMOKEGRENADE, vProjectilePos)
				ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_STICKYBOMB, vProjectilePos)
				ELIF GET_COORDS_OF_PROJECTILE_TYPE_IN_AREA(vMin, vMax, WEAPONTYPE_MOLOTOV, vProjectilePos)
					// check the projectile is inside the club
					IF IS_POSITION_INSIDE_MUSIC_CLUB_INTERIOR(vProjectilePos)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - projectile in area around ped IN CLUB : ", "return TRUE") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF	
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - projectile in area around ped : ", "return TRUE") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    check for player bumping into a ped
/// PARAMS:
///    pedIndex - specific ped
/// RETURNS:
///    if player is touching ped and ped is ragdoll
FUNC BOOL IS_PLAYER_BUMPING_INTO_PED(PED_INDEX pedIndex)
	IF IS_PED_UNINJURED(pedIndex)
		IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())
			IF IS_PED_RAGDOLL(pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_BUMPING_INTO_PED check - return TRUE ragdoll and touching this frame : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		IF HAS_PED_RECEIVED_EVENT(pedIndex, EVENT_PED_COLLISION_WITH_PLAYER)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_BUMPING_INTO_PED check - return TRUE collision event this frame : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check for player bumping into one of the club peds
///    different check for sync scene peds who need to play an anim
/// PARAMS:
///    pedIndex - specific ped
/// RETURNS:
///    TRUE if bumped
FUNC BOOL IS_PLAYER_BUMPING_INTO_CLUB_PED(INT iClubPedIndex)
	IF iClubPedIndex = CLUB_PED_BAND_MANAGER
	OR iClubPedIndex = CLUB_PED_GROUPIE_UPSTAIRS_02
		IF IS_PED_UNINJURED(sClubPed[iClubPedIndex].pedIndex)
			IF IS_ENTITY_TOUCHING_ENTITY(sClubPed[iClubPedIndex].pedIndex, PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_BUMPING_INTO_CLUB_PED check - return TRUE for ped using sync scene ID : ", iClubPedIndex, " this frame : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PLAYER_BUMPING_INTO_PED(sClubPed[iClubPedIndex].pedIndex)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_BUMPING_INTO_CLUB_PED check - return TRUE for ped ID : ", iClubPedIndex, " this frame : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the pedIndex's health is equal to or below the threshold
/// PARAMS:
///    pedIndex - the ped to test
///    bLastHitMustBeMelee - only returns TRUE if the last register damage was melee 
///    iHealthThreshold - the health threshold
/// RETURNS:
///    true if the ped's health is less than or equal to the threshold, and bLastDamageMustBeMelee if TRUE
FUNC BOOL HAS_PLAYER_BEATEN_UP_SPECIFIC_PED(PED_INDEX pedIndex, BOOL bLastDamageMustBeMelee = FALSE, INT iHealthThreshold = 180)
	IF IS_PED_UNINJURED(pedIndex)
		IF (GET_ENTITY_HEALTH(pedIndex) <= iHealthThreshold)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
				IF bLastDamageMustBeMelee
					IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYMELEE)
					OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_UNARMED)	// may not need this test
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
						CLEAR_PED_LAST_WEAPON_DAMAGE(pedIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_BEATEN_UP_SPECIFIC_PED : return TRUE - bLastDamageMustBeMelee = ", bLastDamageMustBeMelee) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_BEATEN_UP_SPECIFIC_PED : return TRUE - bLastDamageMustBeMelee = ", bLastDamageMustBeMelee) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
	CLEAR_PED_LAST_WEAPON_DAMAGE(pedIndex)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if Willy has called for Security or said this surrender dialogue
/// RETURNS:
///    TRUE if Willy has shouted for security
FUNC BOOL HAS_Willy_CALLED_FOR_SECURITY()
	IF bDoneDialogue_MusicianShoutSecurity
	OR bDoneDialogue_MusicianSurrendered
		IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_M1")	// Security! Where's security!?
		AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MSU2")	// Oh no don't hurt me, don't hurt me!  What do you want?	
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_Willy_CALLED_FOR_SECURITY or said surrender dialogue - return TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    make player head track Willy prior to it all kicking off when he's in the same room as Willy
///    for B*1519694
PROC HANDLE_PLAYER_HEADTRACKING_WILLY_PRE_BRAWL()

	IF eMusicianState = N1A_MUSICIAN_STATE_RELAXED
		IF IS_ENTITY_ALIVE(sWillyPed.pedIndex)
			IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM			
				IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sWillyPed.pedIndex)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sWillyPed.pedIndex, -1)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HANDLE_PLAYER_HEADTRACKING_WILLY_PRE_BRAWL - task applied") ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sWillyPed.pedIndex)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HANDLE_PLAYER_HEADTRACKING_WILLY_PRE_BRAWL - task cleared") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    had to move this here since skip has to call it earlier
PROC SET_WILLY_AI_SETUP_FLEE_TASK()
	IF IS_PED_UNINJURED(sWillyPed.pedIndex)
		CLEAR_PED_TASKS(sWillyPed.pedIndex)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sWillyPed.pedIndex, TRUE)	// B*1543302 - ensure this is renabled
		SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_DISABLE_HANDS_UP, FALSE)					
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_USE_VEHICLE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_PREFER_PAVEMENTS, TRUE)
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_USE_COVER, FALSE)
		SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
		SET_PED_MAX_MOVE_BLEND_RATIO(sWillyPed.pedIndex, PEDMOVEBLENDRATIO_RUN)	
		IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
			STOP_SYNCHRONIZED_ENTITY_ANIM(sWillyPed.pedIndex, FAST_BLEND_OUT, TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE - Musician sync scene stopped") ENDIF #ENDIF
		ENDIF
		OPEN_SEQUENCE_TASK(sequenceAI)
			IF IS_PED_IN_ANY_VEHICLE(sWillyPed.pedIndex)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE - Musician told to exit vehicle") ENDIF #ENDIF
			ENDIF
			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150.0, -1, TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE - Musician setup to flee on foot") ENDIF #ENDIF
		CLOSE_SEQUENCE_TASK(sequenceAI)					
		TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
		CLEAR_SEQUENCE_TASK(sequenceAI)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the behaviour of the musician ped
PROC UPDATE_MUSICIAN_PED_AI()
	IF IS_PED_UNINJURED(sWillyPed.pedIndex)
		SET_PED_RESET_FLAG(sWillyPed.pedIndex, PRF_PreventAllMeleeTakedowns, TRUE)	// must be called everyframe to prevent takedown and takedown failed
		IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	//having to test this because changes to tasks breaks the execution/takedown moves
			SWITCH (sWillyPed.AI)
				CASE N1A_PED_AI_SETUP_RELAXED											
					sWillyPed.AI = N1A_PED_AI_STATE_RELAXED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ set - ", "N1A_PED_AI_SETUP_RELAXED") ENDIF #ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_RELAXED
					//currently an idle state
					IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 8.0)	// rougly width of the pavement
						//IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)	// this doesn't work as it's a secondary task
						IF NOT IS_PED_HEADTRACKING_PED(sWillyPed.pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID(), -1)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ set look at player -	N1A_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						ENDIF
					ELSE
						//IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)	// this doesn't work as it's a secondary task
						IF IS_PED_HEADTRACKING_PED(sWillyPed.pedIndex, PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(sWillyPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ Clear look at player	 - N1A_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
				CASE N1A_PED_AI_SETUP_MELEE_ATTACK
					IF HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM(TRUE)
						//IF HAS_TIME_PASSED(sWillyPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(250, 500))	// small delay in his reaction
							IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sWillyPed.pedIndex, FAST_BLEND_OUT, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_SURRENDERED - Musician sync scene stopped") ENDIF #ENDIF
							ENDIF
							//CLEAR_PED_TASKS(sWillyPed.pedIndex)	
							SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, TRUE)
							TASK_COMBAT_PED(sWillyPed.pedIndex, PLAYER_PED_ID())
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)						
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MELEE_ATTACK") ENDIF #ENDIF
							sWillyPed.AI = N1A_PED_AI_STATE_MELEE_ATTACK
						//ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MELEE_ATTACK - waiting on exit sync scene") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_MELEE_ATTACK
					/*IF NOT IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex, N1A_RCR_MAIN_ROOM, FALSE)
						//if the Musician isn't inside the area - seen when trying to navigate to player who was on higher ground
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MELEE_ATTACK - done - musician left area") ENDIF #ENDIF
						sWillyPed.iAiDelayTimer = (GET_GAME_TIMER() - 9000)	// ensure the task reapply has no delay
						sWillyPed.AI = N1A_PED_AI_SETUP_REACHED_PERIMETER
					ELIF IS_DIFFERENCE_IN_Z_COORDS_MORE_THAN_THRESHOLD(GET_ENTITY_COORDS(sWillyPed.pedIndex), vPlayerPos, Z_DIFFERENCE_COMBAT_TASK_STOPS_WORKING)
						//if the Musician isn't inside the area - seen when trying to navigate to player who was on higher ground
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MELEE_ATTACK done - Player to high to reach") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_OUT_OF_REACH
					ELSE*/
						IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_COMBAT)
	                    	// PED HAS LOST HIS TASK, RESET IT
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_MELEE_ATTACK - done") ENDIF #ENDIF
	                   		sWillyPed.AI = N1A_PED_AI_SETUP_MELEE_ATTACK
	                  	ENDIF 
					//ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP
					// B*1340051 - went from flee to fall over back to flee before hand to mouth anim
					IF IS_PED_FLEEING(sWillyPed.pedIndex)
					OR IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SMART_FLEE_PED)
						CLEAR_PED_TASKS(sWillyPed.pedIndex)
					ENDIF
					REQUEST_ANIM_DICT(tlAnimDict_WillyMocappedHurtAnims)	// early request for knocked out anims - N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS
					//CLEAR_PED_TASKS_IMMEDIATELY(sWillyPed.pedIndex)	//we do this because CLEAR_PED_TASKS doesn't always switch the task straight away, e.g. when brawling					
					SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_CAN_RAGDOLL(sWillyPed.pedIndex, TRUE)					
					IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						SET_PED_TO_RAGDOLL(sWillyPed.pedIndex, 1000, 1000, TASK_RELAX)
						//VECTOR vTempPos
						//vTempPos = GET_ENTITY_COORDS(sWillyPed.pedIndex)
						//SET_PED_TO_RAGDOLL_WITH_FALL(sWillyPed.pedIndex, 5000, 8000, TYPE_DOWN_STAIRS, GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vTempPos.Z, << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)	
					ENDIF					
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP - done") ENDIF #ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
					sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP
				BREAK				
				CASE N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP
					IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~	N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP - done") ENDIF #ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
						sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS
					ENDIF
					//now switching him to flee behaviour instead
					/*
					IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP						
						IF GET_SCRIPT_TASK_STATUS(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
		                AND GET_SCRIPT_TASK_STATUS(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
		                	OPEN_SEQUENCE_TASK(sequenceAI)
								TASK_COWER(NULL, -1)
							CLOSE_SEQUENCE_TASK(sequenceAI)							
							TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
							CLEAR_SEQUENCE_TASK(sequenceAI)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP - done") ENDIF #ENDIF
		              	ENDIF
					ENDIF
					*/
				BREAK
				CASE N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS
					REQUEST_ANIM_DICT(tlAnimDict_WillyMocappedHurtAnims)
					IF HAS_ANIM_DICT_LOADED(tlAnimDict_WillyMocappedHurtAnims)
						IF NOT IS_PED_GETTING_UP(sWillyPed.pedIndex)
						AND NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						AND NOT IS_PED_BEING_STUNNED(sWillyPed.pedIndex)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sWillyPed.pedIndex, TRUE)	// B*1543302 - renable this once he's up
							TASK_CLEAR_LOOK_AT(sWillyPed.pedIndex)
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sWillyPed.pedIndex, FALSE)	// B* 1492058 look at makes anim clip
							OPEN_SEQUENCE_TASK(sequenceAI)
								/*IF NOT IS_PED_FACING_PED(sWillyPed.pedIndex, PLAYER_PED_ID(), 120)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)
								ENDIF*/ // just play the anim once stood up
								TASK_PLAY_ANIM(NULL, tlAnimDict_WillyMocappedHurtAnims, "enter_willie", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1)//, AF_UPPERBODY | AF_SECONDARY)
								TASK_PLAY_ANIM(NULL, tlAnimDict_WillyMocappedHurtAnims, "base_willie", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 2000) //, AF_UPPERBODY | AF_SECONDARY)
								TASK_PLAY_ANIM(NULL, tlAnimDict_WillyMocappedHurtAnims, "exit_willie", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1) //, AF_UPPERBODY | AF_SECONDARY)
							CLOSE_SEQUENCE_TASK(sequenceAI)
							TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
							CLEAR_SEQUENCE_TASK(sequenceAI)
							REMOVE_ANIM_DICT(tlAnimDict_WillyMocappedHurtAnims)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
							sWillyPed.iTimer = GET_GAME_TIMER()
							sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS - done") ENDIF #ENDIF
							sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS
					IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS -> N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(sWillyPed.pedIndex, tlAnimDict_WillyMocappedHurtAnims, "enter_willie")
							SET_ENTITY_ANIM_SPEED(sWillyPed.pedIndex, tlAnimDict_WillyMocappedHurtAnims, "enter_willie", 1.4)
						ELIF IS_ENTITY_PLAYING_ANIM(sWillyPed.pedIndex, tlAnimDict_WillyMocappedHurtAnims, "exit_willie")
							SET_ENTITY_ANIM_SPEED(sWillyPed.pedIndex, tlAnimDict_WillyMocappedHurtAnims, "exit_willie", 1.2)
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS
					IF NOT IS_PED_HEADTRACKING_PED(sWillyPed.pedIndex, PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID(), -1)
						SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sWillyPed.pedIndex, TRUE)	// turn back on B* 1492058 look at makes anim clip
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ set look at player -	N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS") ENDIF #ENDIF
					ENDIF
					IF NOT IS_PED_FACING_PED(sWillyPed.pedIndex, PLAYER_PED_ID(), 90)
						IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							TASK_TURN_PED_TO_FACE_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID(), -1)
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_REACHED_PERIMETER										
					OPEN_SEQUENCE_TASK(sequenceAI)
						IF (VDIST(GET_ENTITY_COORDS(sWillyPed.pedIndex), vMusicClub_CentrePos) >= 10.0)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vMusicClub_CentrePos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 2.0)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_REACHED_PERIMETER - return to area then anims") ENDIF #ENDIF
						ELSE
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1)	//done to balance the amount of tasks in the sequence as i test that to see when we reach the last turn task
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_REACHED_PERIMETER - just play anims") ENDIF #ENDIF
						ENDIF
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 0)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(1000, 2000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(sequenceAI)												
					TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
					CLEAR_SEQUENCE_TASK(sequenceAI)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
					sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_REACHED_PERIMETER - done") ENDIF #ENDIF
					sWillyPed.AI = N1A_PED_AI_STATE_REACHED_PERIMETER
				BREAK
				CASE N1A_PED_AI_STATE_REACHED_PERIMETER
					IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM
						IF IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex, N1A_RCR_MAIN_ROOM, FALSE)
							IF sWillyPed.iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_REACHED_PERIMETER - done") ENDIF #ENDIF
								sWillyPed.AI = N1A_PED_AI_SETUP_MELEE_ATTACK
								EXIT
							ENDIF
						ENDIF
					ENDIF
					IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_REACHED_PERIMETER - reapply") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_REACHED_PERIMETER
					ELIF GET_SEQUENCE_PROGRESS(sWillyPed.pedIndex) >= 4	// final turn to task
						IF HAS_TIME_PASSED(sWillyPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(4000, 8000))
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_REACHED_PERIMETER - reapply") ENDIF #ENDIF
							sWillyPed.AI = N1A_PED_AI_SETUP_REACHED_PERIMETER
						ENDIF
					ELSE
						sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_OUT_OF_REACH									
					OPEN_SEQUENCE_TASK(sequenceAI)
						INT iRandomAnimSelector
						iRandomAnimSelector = GET_RANDOM_INT_IN_RANGE(0, 3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 0)
						SWITCH iRandomAnimSelector
							CASE 0
								TASK_PLAY_ANIM(NULL, "rcmnigel1a", "threaten", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
							BREAK
							CASE 1
								TASK_PLAY_ANIM(NULL, "rcmnigel1a", "anger_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
							BREAK
							CASE 2					
								TASK_PLAY_ANIM(NULL, "rcmnigel1a", "screw_you", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
							BREAK
						ENDSWITCH
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE(1000, 2000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(sequenceAI)							
					//this needs to go here other wise I recieve (i think)
					//Error: [script] Verifyf(iSequenceTaskID==CTaskSequences::ms_iActiveSequence
					//FAILED: SCRIPT: Script Name = Nigel1A : Program Counter = 19467: SET_SEQUENCE_TO_REPEAT - Using wrong sequence ID (from Updating script nigel1a)
				//	CLEAR_PED_TASKS_IMMEDIATELY(sWillyPed.pedIndex)	//we do this because CLEAR_PED_TASKS doesn't always switch the task straight away, e.g. when brawling							
					TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
					CLEAR_SEQUENCE_TASK(sequenceAI)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)	
					sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_OUT_OF_REACH - done") ENDIF #ENDIF
					sWillyPed.AI = N1A_PED_AI_STATE_OUT_OF_REACH
				BREAK
				CASE N1A_PED_AI_STATE_OUT_OF_REACH
					IF NOT IS_DIFFERENCE_IN_Z_COORDS_MORE_THAN_THRESHOLD(GET_ENTITY_COORDS(sWillyPed.pedIndex), vPlayerPos, Z_DIFFERENCE_COMBAT_TASK_STOPS_WORKING)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_OUT_OF_REACH - done") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_MELEE_ATTACK
					ELSE
						IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_OUT_OF_REACH - reapply") ENDIF #ENDIF
							sWillyPed.AI = N1A_PED_AI_SETUP_OUT_OF_REACH
						ELIF GET_SEQUENCE_PROGRESS(sWillyPed.pedIndex) >= 3	// final turn to task
							IF HAS_TIME_PASSED(sWillyPed.iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(4000, 8000))
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_OUT_OF_REACH - reapply") ENDIF #ENDIF
								sWillyPed.AI = N1A_PED_AI_SETUP_OUT_OF_REACH
							ENDIF
						ELSE
							sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_FLEE
					IF HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM(TRUE)
						SET_WILLY_AI_SETUP_FLEE_TASK()	// had to put in proc so skip can call it too			
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_STATE_FLEE
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE waiting on exit sync anim") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_FLEE
					SET_PED_MAX_MOVE_BLEND_RATIO(sWillyPed.pedIndex, PEDMOVEBLENDRATIO_RUN)	// see B*1071719 - need to be called each frame otherwise it resets.
					IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_Ai_STATE_FLEE - going to N1A_PED_AI_SETUP_FLEE") ENDIF #ENDIF
	                    sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_FLEE_TO_VEHICLE
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_DISABLE_HANDS_UP, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_PREFER_PAVEMENTS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sWillyPed.pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
							
					// check Willy should still try to reach his car
					IF IS_VEHICLE_OK(sMusicianVehicle.vehIndex)																//if Musician vehicle is ok to use
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMusicianVehicle.vehIndex)	
						AND IS_ENTITY_IN_RANGE_COORDS(sMusicianVehicle.vehIndex, sMusicianVehicle.vPosition, 3.0)			//if Musician vehicle pos hasn't changed much
						AND IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(GET_ENTITY_HEADING(sMusicianVehicle.vehIndex), sMusicianVehicle.fHeading, 20.0)	//if Musician vehicle heading hasn't changed much	//if Player isn't already in the vehicle
							IF IS_PED_IN_VEHICLE(sWillyPed.pedIndex, sMusicianVehicle.vehIndex)						//if Musician isn't already in the vehicle
							AND NOT IS_PED_BEING_JACKED(sWillyPed.pedIndex)	//if Musician isn't being jacked.
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_TO_VEHICLE going to N1A_PED_AI_STATE_FLEE_TO_VEHICLE task skipped as in the vehicle") ENDIF #ENDIF
								sWillyPed.AI = N1A_PED_AI_STATE_FLEE_TO_VEHICLE
							ELSE
								CLEAR_PED_TASKS(sWillyPed.pedIndex)							
								TASK_ENTER_VEHICLE(sWillyPed.pedIndex, sMusicianVehicle.vehIndex, 25000, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)	// increased DEFAULT_TIME_BEFORE_WARP since he has a fair way to run
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)						
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_TO_VEHICLE going to N1A_PED_AI_STATE_FLEE_TO_VEHICLE") ENDIF #ENDIF
								sWillyPed.AI = N1A_PED_AI_STATE_FLEE_TO_VEHICLE
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_TO_VEHICLE going to N1A_PED_AI_SETUP_FLEE") ENDIF #ENDIF
							sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_TO_VEHICLE going to N1A_PED_AI_SETUP_FLEE") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_FLEE_TO_VEHICLE
					IF NOT IS_VEHICLE_OK(sMusicianVehicle.vehIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_TO_VEHICLE - going to N1A_PED_AI_SETUP_FLEE - car not ok") ENDIF #ENDIF
                    	sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					ELIF (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sMusicianVehicle.vehIndex)
					OR IS_PED_BEING_JACKED(sWillyPed.pedIndex)	
					OR NOT IS_ENTITY_IN_RANGE_COORDS(sMusicianVehicle.vehIndex, sMusicianVehicle.vPosition, 3.0)
					OR NOT IS_DIFFERENCE_IN_HEADINGS_LESS_THAN_OR_EQUAL_TO_THE_TOLERANCE(GET_ENTITY_HEADING(sMusicianVehicle.vehIndex), sMusicianVehicle.fHeading, 20.0))
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_TO_VEHICLE - going to N1A_PED_AI_SETUP_FLEE - reason to abort") ENDIF #ENDIF
                    	sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					ELIF IS_PED_SITTING_IN_VEHICLE(sWillyPed.pedIndex, sMusicianVehicle.vehIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_TO_VEHICLE - going to N1A_PED_AI_SETUP_FLEE_IN_VEHICLE - reason to abort") ENDIF #ENDIF
                    	sWillyPed.AI = N1A_PED_AI_SETUP_FLEE_IN_VEHICLE
					ELSE
						IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_ENTER_VEHICLE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_TO_VEHICLE - going to N1A_PED_AI_SETUP_FLEE_TO_VEHICLE") ENDIF #ENDIF
		                    sWillyPed.AI = N1A_PED_AI_SETUP_FLEE_TO_VEHICLE
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_FLEE_IN_VEHICLE
					IF IS_VEHICLE_OK(sMusicianVehicle.vehIndex)																		//if Musician vehicle is ok to use											//if Player isn't already in the vehicle
					AND IS_PED_SITTING_IN_VEHICLE(sWillyPed.pedIndex, sMusicianVehicle.vehIndex)								//if Musician isn't already in the vehicle
						CLEAR_PED_TASKS(sWillyPed.pedIndex)							
						OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, sMusicianVehicle.vehIndex, << -533.7831, 317.3689, 82.0616 >>, 60.0, DRIVINGSTYLE_ACCURATE, sMusicianVehicle.mnModel, DRIVINGMODE_AVOIDCARS_RECKLESS, 8.0, -1.0)
							TASK_VEHICLE_MISSION_PED_TARGET(NULL, sMusicianVehicle.vehIndex, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 500.0, -1.0)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE_IN_VEHICLE - Musician setup to flee in his car") ENDIF #ENDIF
						CLOSE_SEQUENCE_TASK(sequenceAI)					
						TASK_PERFORM_SEQUENCE(sWillyPed.pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)						
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE_IN_VEHICLE going to N1A_PED_AI_STATE_FLEE_IN_VEHICLE") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_STATE_FLEE_IN_VEHICLE
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE_IN_VEHICLE going to N1A_PED_AI_SETUP_FLEE") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_FLEE_IN_VEHICLE
					IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_FLEE_IN_VEHICLE - going to N1A_PED_AI_SETUP_FLEE_IN_VEHICLE") ENDIF #ENDIF
	                    sWillyPed.AI = N1A_PED_AI_SETUP_FLEE_IN_VEHICLE
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_SURRENDERED
					IF HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM(TRUE)
						IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						AND NOT IS_PED_GETTING_UP(sWillyPed.pedIndex)
						AND NOT IS_PED_BEING_STUNNED(sWillyPed.pedIndex)
							IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sWillyPed.pedIndex, FAST_BLEND_OUT, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_SURRENDERED - Musician sync scene stopped") ENDIF #ENDIF
							ENDIF
							SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
							TASK_LOOK_AT_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID(), -1)
							TASK_HANDS_UP(sWillyPed.pedIndex, -1, PLAYER_PED_ID(), -1)											
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_SURRENDERED - done: framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							IF IS_PED_IN_COMBAT(sWillyPed.pedIndex)					
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_SURRENDERED - musician in combat **: framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
							sWillyPed.AI = N1A_PED_AI_STATE_SURRENDERED
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_SURRENDERED - waiting on exit anim sync anim : framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_SURRENDERED			
					IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_HANDS_UP)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_SURRENDERED - done : framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	                	sWillyPed.AI = N1A_PED_AI_SETUP_SURRENDERED						
	              	ENDIF
					IF IS_PED_IN_COMBAT(sWillyPed.pedIndex)					
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_SURRENDERED - musician in combat **: framecount = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_SETUP_FRIGHTENED	
					CLEAR_PED_TASKS(sWillyPed.pedIndex)
					SET_PED_COMBAT_ATTRIBUTES(sWillyPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)									
					TASK_COWER(sWillyPed.pedIndex, -1)	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sWillyPed.pedIndex, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FRIGHTENED done") ENDIF #ENDIF
					sWillyPed.AI = N1A_PED_AI_STATE_FRIGHTENED
				BREAK
				CASE N1A_PED_AI_STATE_FRIGHTENED
					IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
						IF NOT IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_COWER)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_STATE_FRIGHTENED done") ENDIF #ENDIF
	                    	sWillyPed.AI = N1A_PED_AI_SETUP_FRIGHTENED
	                  	ENDIF
					ENDIF
				BREAK				
				DEFAULT
					SCRIPT_ASSERT("NIGEL1A - invalid N1A_PED_AI")
				BREAK				
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if the specified peds is a club ped
/// PARAMS:
///    pedIndex - ped to test
/// RETURNS:
///    TRUE if pedIndex is a club ped - groupie or band
FUNC BOOL IS_PED_A_CLUB_PED(PED_INDEX &pedIndex)
	INT i
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		IF pedIndex = sClubPed[i].pedIndex
			RETURN TRUE
		ENDIF	
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check the musician has seen the player attack a specific ped
/// PARAMS:
///    pedIndexVictim - ped to check for attacked
/// RETURNS:
///    true if musician saw player attack pedIndexVictim
FUNC BOOL HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED(PED_INDEX pedIndexVictim)
	IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		INT i = 0
		PED_INDEX mPed
		IF Has_Ped_Been_Killed()
			BOOL bMusicianSeenPlayerKillPed = FALSE
			i = 0
			REPEAT Get_Number_Of_Ped_Killed_Events() i
				IF DOES_ENTITY_EXIST(Get_Index_Of_Killed_Ped(i))
	            	mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Killed_Ped(i))
					IF mPed = pedIndexVictim
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 0 : pedvictim is one of Get_Number_Of_Ped_Killed_Events") ENDIF #ENDIF
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 1 : pedvictim has been damaged by player") ENDIF #ENDIF
							IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM
							OR sWillyPed.iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 2 : ", "musician can see ped or player in main room ") ENDIF #ENDIF
								bMusicianSeenPlayerKillPed = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			IF bMusicianSeenPlayerKillPed
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 4 : killed ", " returning TRUE") ENDIF #ENDIF
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF
		IF Has_Ped_Been_Injured()
			BOOL bMusicianSeenPlayerInjurePed = FALSE
			i = 0
			REPEAT Get_Number_Of_Ped_Injured_Events() i
				IF DOES_ENTITY_EXIST(Get_Index_Of_Injured_Ped(i))
	            	mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Injured_Ped(i))
					IF mPed = pedIndexVictim
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 5 :  pedvictim is one of Has_Ped_Been_Injured - ") ENDIF #ENDIF
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 6 :, pedvictim has been damaged by player") ENDIF #ENDIF
							IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM
							OR sWillyPed.iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 7 : ", "musician can see ped or player in main room ") ENDIF #ENDIF
								bMusicianSeenPlayerInjurePed = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			IF bMusicianSeenPlayerInjurePed
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED - 8 : injured ", " returning TRUE") ENDIF #ENDIF
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF 
		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    update the ai of the specified club ped
/// PARAMS:
///    iClubPedID - the array index of the ped to update
PROC UPDATE_STANDARD_CLUB_PED_AI(INT iClubPedID)
	IF IS_PED_UNINJURED(sClubPed[iClubPedID].pedIndex)
		IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	//having to test this because changes to tasks breaks the execution/takedown moves
			SWITCH (sClubPed[iClubPedID].AI)
				CASE N1A_PED_AI_SETUP_RELAXED						
				BREAK
				CASE N1A_PED_AI_STATE_RELAXED
					IF HAS_TIME_PASSED(sClubPed[iClubPedID].iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(0, 2500))
						IF NOT IS_PED_CLOSE_TO_IDEAL_HEADING(sClubPed[iClubPedID].pedIndex, sClubPed[iClubPedID].fHeading, 30)
							IF NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_ACHIEVE_HEADING)
								TASK_ACHIEVE_HEADING(sClubPed[iClubPedID].pedIndex, sClubPed[iClubPedID].fHeading)
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_RELAXED - ", " set TASK_ACHIEVE_HEADING ", " PED ID:", iClubPedID) ENDIF #ENDIF
							ENDIF
						ELSE
							SWITCH iClubPedID
								CASE CLUB_PED_GROUPIE_UPSTAIRS_02
									IF NOT IS_PED_USING_SCENARIO(sClubPed[iClubPedID].pedIndex, "WORLD_HUMAN_STAND_IMPATIENT")
										TASK_START_SCENARIO_IN_PLACE(sClubPed[iClubPedID].pedIndex, "WORLD_HUMAN_STAND_IMPATIENT", -1, FALSE)
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_RELAXED - ", " set use scenario WORLD_HUMAN_STAND_IMPATIENT", " PED ID:", iClubPedID) ENDIF #ENDIF
									ENDIF
								BREAK
								CASE GROUPIE_WILLYS_PIECE
									FALLTHRU
								CASE CLUB_PED_ROADIE
									FALLTHRU	
								CASE CLUB_PED_GROUPIE_UPSTAIRS_01
									IF NOT IS_PED_USING_SCENARIO(sClubPed[iClubPedID].pedIndex, "WORLD_HUMAN_HANG_OUT_STREET")
										TASK_START_SCENARIO_IN_PLACE(sClubPed[iClubPedID].pedIndex, "WORLD_HUMAN_HANG_OUT_STREET", -1, FALSE)
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_RELAXED - ", " set use scenario WORLD_HUMAN_HANG_OUT_STREET", " PED ID:", iClubPedID) ENDIF #ENDIF
									ENDIF	
								BREAK
							ENDSWITCH	
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_RANGE_COORDS(sClubPed[iClubPedID].pedIndex, vPlayerPos, 7.0)	// rougly width of the pavement
					OR (GET_PED_INSIDE_MUSIC_CLUB_STATUS(sClubPed[iClubPedID].pedIndex, FALSE) = ePlayerInsideClubStatus)	// if player is in the same room
						//IF NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)	// this doesn't work as it's a secondary task
						IF NOT IS_PED_HEADTRACKING_PED(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), -1)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_RELAXED -  set look at player", " PED ID:", iClubPedID) ENDIF #ENDIF
						ENDIF
					ELSE
						//IF  IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)	// this doesn't work as it's a secondary task
						IF IS_PED_HEADTRACKING_PED(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID())
							TASK_CLEAR_LOOK_AT(sClubPed[iClubPedID].pedIndex)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_RELAXED - Clear look at player", " PED ID:", iClubPedID) ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_UNDER_SYNCED_SCENE_CONTROL
				BREAK
				CASE N1A_PED_AI_SETUP_AWARE_OF_PLAYER					
					IF HAS_TIME_PASSED(sClubPed[iClubPedID].iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(0, 2000))
						IF IS_PED_USING_ANY_SCENARIO(sClubPed[iClubPedID].pedIndex)
							//CLEAR_PED_TASKS(sClubPed[iClubPedID].pedIndex)
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sClubPed[iClubPedID].pedIndex)
						ENDIF
						// don't make all peds turn to the player - bug fix 1042210
						SWITCH iClubPedID
							CASE CLUB_PED_BAND_MANAGER
								FALLTHRU
							CASE CLUB_PED_GROUPIE_UPSTAIRS_02
								TASK_LOOK_AT_ENTITY(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), -1)
							BREAK
							DEFAULT
								TASK_LOOK_AT_ENTITY(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), -1)
								TASK_TURN_PED_TO_FACE_ENTITY(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), -1)
							BREAK
						ENDSWITCH
						sClubPed[iClubPedID].AI = N1A_PED_AI_STATE_AWARE_OF_PLAYER
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_SETUP_AWARE_OF_PLAYER - set", " PED ID:", iClubPedID) ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_AWARE_OF_PLAYER						
					// don't make all peds turn to the player - bug fix 1042210
					SWITCH iClubPedID
						CASE CLUB_PED_BAND_MANAGER
							FALLTHRU
						CASE CLUB_PED_GROUPIE_UPSTAIRS_02
							IF NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)
								sClubPed[iClubPedID].iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure a task reapply will go through first attempt
								sClubPed[iClubPedID].AI = N1A_PED_AI_SETUP_AWARE_OF_PLAYER
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_AWARE_OF_PLAYER - reapply", " PED ID:", iClubPedID) ENDIF #ENDIF
							ENDIF
						BREAK
						DEFAULT
							IF NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								sClubPed[iClubPedID].iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure a task reapply will go through first attempt
								sClubPed[iClubPedID].AI = N1A_PED_AI_SETUP_AWARE_OF_PLAYER
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_AWARE_OF_PLAYER - reapply", " PED ID:", iClubPedID) ENDIF #ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE N1A_PED_AI_SETUP_FLEE	
					INT IMinDelayTime, iMaxDelayTime
					IMinDelayTime = 0
					iMaxDelayTime = 1200
					IF iClubPedID = GROUPIE_WILLYS_PIECE		// make Willy's bird react quicker than the other groupies
						// delay groupie fleeing whilst she plays here
						IF NOT HAS_Willy_AND_GROUPIE_FINISHED_EXIT_SYNCED_ANIM(FALSE)						
							IMinDelayTime = 50000
							iMaxDelayTime = 90000
							sClubPed[iClubPedID].iAiDelayTimer = GET_GAME_TIMER()
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_SETUP_FLEE - delay for sync anim Willy's piece ", " PED ID:", iClubPedID, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELSE
							iMaxDelayTime = 0
						ENDIF
					ENDIF
					IF HAS_TIME_PASSED(sClubPed[iClubPedID].iAiDelayTimer, GET_RANDOM_INT_IN_RANGE(IMinDelayTime, iMaxDelayTime))
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_DISABLE_HANDS_UP, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_USE_VEHICLE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_PREFER_PAVEMENTS, TRUE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_USE_COVER, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, FA_CAN_SCREAM, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, CA_ALWAYS_FIGHT, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sClubPed[iClubPedID].pedIndex, CA_ALWAYS_FLEE, TRUE)
						IF iClubPedID != CLUB_PED_BAND_MANAGER	// this guy has react dialogue so don't interrupt
							STOP_PED_SPEAKING(sClubPed[iClubPedID].pedIndex, FALSE)	//added to make them scream // hopefully?
						ENDIF
						IF HAS_CLUB_PED_FINISHED_SYNCED_SCENE_BREAK_OUT_ANIM(iClubPedID)
							IF IS_PED_USING_ANY_SCENARIO(sClubPed[iClubPedID].pedIndex)
								//CLEAR_PED_TASKS(sClubPed[iClubPedID].pedIndex)
								SET_PED_PANIC_EXIT_SCENARIO(sClubPed[iClubPedID].pedIndex, vPlayerPos)
							ENDIF
							
							SWITCH iClubPedID
								CASE CLUB_PED_BAND_MANAGER
									//TASK_HANDS_UP(sClubPed[iClubPedID].pedIndex, -1, PLAYER_PED_ID(), -1)
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - hands up - set", " PED ID:", iClubPedID) ENDIF #ENDIF
								BREAK
								CASE CLUB_PED_GROUPIE_UPSTAIRS_01
									FALLTHRU
								CASE CLUB_PED_GROUPIE_UPSTAIRS_02
									//TASK_COWER(sClubPed[iClubPedID].pedIndex, -1)
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - cower - set", " PED ID:", iClubPedID) ENDIF #ENDIF
								BREAK
								DEFAULT
									//TASK_SMART_FLEE_PED(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), 150, -1)
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - flee - set", " PED ID:", iClubPedID) ENDIF #ENDIF
								BREAK
							ENDSWITCH
							TASK_SMART_FLEE_PED(sClubPed[iClubPedID].pedIndex, PLAYER_PED_ID(), 150, -1)
							SET_PED_KEEP_TASK(sClubPed[iClubPedID].pedIndex, TRUE)
							sClubPed[iClubPedID].AI = N1A_PED_AI_STATE_FLEE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_SETUP_FLEE - set", " PED ID:", iClubPedID, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_SETUP_FLEE - waiting for reaction anim", " PED ID:", iClubPedID) ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_SETUP_FLEE - delaying flee this frame : ", GET_FRAME_COUNT(), " PED ID:", iClubPedID) ENDIF #ENDIF
					ENDIF
				BREAK
				CASE N1A_PED_AI_STATE_FLEE	
					IF IS_PED_ACTIVE_IN_SCENARIO(sClubPed[iClubPedID].pedIndex)
					OR NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_SMART_FLEE_PED)
					AND NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_COWER)
					AND NOT IsPedPerformingTask(sClubPed[iClubPedID].pedIndex, SCRIPT_TASK_HANDS_UP)
					AND NOT IS_PED_FLEEING(sClubPed[iClubPedID].pedIndex)
						sClubPed[iClubPedID].iAiDelayTimer = (GET_GAME_TIMER() - 5000)	// ensure a task reapply will go through first attempt
						sClubPed[iClubPedID].AI = N1A_PED_AI_SETUP_FLEE
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_STANDARD_CLUB_PED_AI - N1A_PED_AI_STATE_FLEE - reapply", " PED ID:", iClubPedID) ENDIF #ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   kill's any ongoing ambient dialogue upstairs and stores the conversation progress so it can be retriggered 
FUNC BOOL KILL_AND_STORE_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(BOOL bFinishCurrentLine = TRUE)
	IF NOT bPausedDialogue_AmbientUpstairs
		IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN1", FALSE)	//only  the ambient upstairs dialogue
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN2", FALSE)	//only  the ambient upstairs dialogue
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN3", FALSE)	//only  the ambient upstairs dialogue
			// Bug fix - could return dialogue as playing in first frame even though the line is invalid, so wait for line to be valid
			TEXT_LABEL_23 tl23CurrentDialogueLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			IF NOT IS_STRING_NULL_OR_EMPTY(tl23CurrentDialogueLine)
				bPausedDialogue_AmbientUpstairs = TRUE
				IF iCounter_AmbientDialogueUpstairs > 0
					iCounter_AmbientDialogueUpstairs--	// ready to retrigger last dialogue
				ENDIF 
				tlDialogueLinePaused_UpstairsAmbient = tl23CurrentDialogueLine
				IF bFinishCurrentLine
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - Return TRUE killed ambient upstairs dialogue, finish current line : ",
																					bFinishCurrentLine, " line : ", tlDialogueLinePaused_UpstairsAmbient) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///   kill's any ongoing ambient dialogue upstairs
FUNC BOOL KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(BOOL bFinishCurrentLine = TRUE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN1", FALSE)	//only  the ambient upstairs dialogue
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN2", FALSE)	//only  the ambient upstairs dialogue
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_BAN3", FALSE)	//only  the ambient upstairs dialogue
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_IDLE1", FALSE)	//only  the ambient upstairs dialogue
			IF bFinishCurrentLine
				KILL_FACE_TO_FACE_CONVERSATION()
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS - Return TRUE killed ambient upstairs dialogue") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles triggering ambient dialogue upstairs in the club between the band manager, roadie and groupies
/// PARAMS:
///    iTimer_DialogueTriggerDelay - timer used to delay the dialogue triggering
/// RETURNS:
///    TRUE if a dialogue exchange is triggered
FUNC BOOL TRIGGER_AMBIENT_DIALOGUE_UPSTAIRS(INT &iTimer_DialogueTriggerDelay)

	N1A_CLUB_PED_ANIM_TYPE eClubPedAnims = N1A_CPAT_BASE_ANIM
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT bPausedDialogue_AmbientUpstairs	// restart paused dialogue straight away
			iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
		ENDIF
	ELSE
		INT iLocal_TotaldialogueEzchanges = 4
		TEXT_LABEL_23 tlCurrentDialogueRoot = ""
		IF iCounter_AmbientDialogueUpstairs < iLocal_TotaldialogueEzchanges
			INT iTimeDelay_DialogueTrigger = GET_RANDOM_INT_IN_RANGE(1000, 2000)
			// no delay for first dialogue exchange or if we are restarting an existing exchange
			IF iCounter_AmbientDialogueUpstairs = 0
			OR bPausedDialogue_AmbientUpstairs
				iTimeDelay_DialogueTrigger = 100	// Bug fix - straight away causes isssues with security triggering it's dialogue since it gets called after this each from, so setup cancel occurs over and over
			ENDIF
			IF HAS_TIME_PASSED(iTimer_DialogueTriggerDelay, iTimeDelay_DialogueTrigger)
				SWITCH iCounter_AmbientDialogueUpstairs
					CASE 0
						tlCurrentDialogueRoot = "NIG1A_BAN1"	// Manager, roadie and groupies banter upstairs - 1
						// I can't believe we're partying with Love Fist's manager. That's so cool.
						// Some of the shit I've seen, love, it's unimaginable.  
						// If the Jezz and the guys weren't so into yoga and probiotics, we'd all be dead.
						// I burned through the last of my dopamine in '08. The doctors say I'll probably never laugh again.
						// That's so awesome.  
						eClubPedAnims = N1A_CPAT_IDLE_A_ANIM
					BREAK
					CASE 1
						tlCurrentDialogueRoot = "NIG1A_BAN2"	// Manager, roadie and groupies banter upstairs - 2
						// I've been on the road with the Fist since 2003. I'm heavily involved in the creative.
						// So they are working on new material? 
						// No, still doing covers of the eighties stuff.   
						// Clear The Custard is, like, my favorite album ever. When I tell my friends I'm hot for Love Fist they're all like ew, 
						// that's like making out with your grandfather, and I'm like yeah, if my grandfather, like, totally grew his hair and wore leather pants.
						// I don't care what people say. Willy is still hot, even after the hip replacement.  
						eClubPedAnims = N1A_CPAT_IDLE_A_ANIM
					BREAK
					CASE 2
						tlCurrentDialogueRoot = "NIG1A_BAN3"	// Manager, roadie and groupies banter upstairs - 3
						// Is it true that Willy wears women's knickers? Are they really all bisexual?  
						// We're British, love, we make up sexuality as we go along.  
						// Woah, I am majorly stoned. Like, seriously.  
						// You want level that out with some H? Or a line of K? Get you nice and fluffy.  
						// Until you've been in the K-hole, taking in the A-hole, you haven't lived.
						// You guys are so funny!  
						eClubPedAnims = N1A_CPAT_IDLE_A_ANIM
					BREAK
					CASE 3
						tlCurrentDialogueRoot = "NIG1A_IDLE1"	// They run out of idle banter
						// Everyone went so quiet!
						// Just enjoy the high.
						eClubPedAnims = N1A_CPAT_IDLE_B_ANIM
					BREAK
				ENDSWITCH
				IF bPausedDialogue_AmbientUpstairs
					IF NIG1A_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlCurrentDialogueRoot, tlDialogueLinePaused_UpstairsAmbient, CONV_PRIORITY_MEDIUM)
						bPausedDialogue_AmbientUpstairs = FALSE
						iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
						iCounter_AmbientDialogueUpstairs++
						SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(eClubPedAnims)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_AMBIENT_DIALOGUE_UPSTAIRS - restarted paused dialogue root : ", tlCurrentDialogueRoot, " : line : ", tlDialogueLinePaused_UpstairsAmbient) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlCurrentDialogueRoot, CONV_PRIORITY_MEDIUM)
						bPausedDialogue_AmbientUpstairs = FALSE
						iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
						iCounter_AmbientDialogueUpstairs++
						SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(eClubPedAnims)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_AMBIENT_DIALOGUE_UPSTAIRS - triggered dialogue root : ", tlCurrentDialogueRoot) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	// set the base by default if it's not runnings
	SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(N1A_CPAT_BASE_ANIM)
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    attempt to trigger a dialogue line for player bumping into a club peds
/// PARAMS:
///    iClubPedIndex - specific club ped
/// RETURNS:
///    TRUE if convo was successfully triggered
FUNC BOOL TRIGGER_PLAYER_BUMPED_DIALOGUE_FOR_CLUB_PEDS(INT iClubPedIndex)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		STRING sNigel1a_Dialogue_BumpedRoot = "NULL"
		SWITCH iClubPedIndex
			CASE CLUB_PED_BAND_MANAGER
				sNigel1a_Dialogue_BumpedRoot = "NIG1A_BUMPM"
				// Hey, don't be a twat.
				// Hey, no need, dude.				
			BREAK
			CASE CLUB_PED_ROADIE
				sNigel1a_Dialogue_BumpedRoot = "NIG1A_BUMPR"
				// That's not cool.  
				// You're ruining the vibe, man.  
			BREAK
			CASE CLUB_PED_GROUPIE_UPSTAIRS_01
				sNigel1a_Dialogue_BumpedRoot = "NIG1A_BUMPG1"
				// Trippy.  
				// You're weird.  
			BREAK
			CASE CLUB_PED_GROUPIE_UPSTAIRS_02
				sNigel1a_Dialogue_BumpedRoot = "NIG1A_BUMPG2"
				// Personal space, man.
				// Woah, take it easy.  
			BREAK
		ENDSWITCH
		IF NOT IS_STRING_NULL_OR_EMPTY(sNigel1a_Dialogue_BumpedRoot)
			IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, sNigel1a_Dialogue_BumpedRoot, CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_PLAYER_BUMPED_DIALOGUE_FOR_CLUB_PEDS - return TRUE : iClubPedIndex = ", iClubPedIndex) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    controls club peds behaviour
PROC MANAGE_CLUB_PEDS()
	INT i 
	FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sClubPed[i].pedIndex, sClubPed[i].iFrameCountLastSeenPlayer)
	ENDFOR
	// Willy's bird only
	IF sClubPed[GROUPIE_WILLYS_PIECE].AI < N1A_PED_AI_SETUP_FLEE
		IF eMusicianState <> N1A_MUSICIAN_STATE_RELAXED
		OR IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, sClubPed[GROUPIE_WILLYS_PIECE].iFrameCountLastSeenPlayer)
		OR (ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM AND IS_PED_IN_COMBAT(PLAYER_PED_ID()))	// additional checks to make her flee as soon as the player is fighting in the room
			sClubPed[GROUPIE_WILLYS_PIECE].AI = N1A_PED_AI_SETUP_FLEE
			MAKE_PED_SCREAM(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)	// B*1516087 - make some peds scream
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", GROUPIE_WILLYS_PIECE) ENDIF #ENDIF
		ELIF bDoneDialogue_MusicianSpottedPlayer
			// needs revising for sync scene stuff
			//IF sClubPed[GROUPIE_WILLYS_PIECE].AI = N1A_PED_AI_STATE_RELAXED
			//	sClubPed[GROUPIE_WILLYS_PIECE].AI = N1A_PED_AI_SETUP_AWARE_OF_PLAYER
			//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - ped AI set to N1A_PED_AI_SETUP_AWARE_OF_PLAYER : ", " PED ID:", GROUPIE_WILLYS_PIECE) ENDIF #ENDIF
			//ENDIF
		ENDIF				
	ENDIF
	// reset of club peds who are in the room upstairs
	IF NOT bHasPlayerScarredClubPedsUpstairs
		IF ePlayerInsideClubStatus = N1A_RCR_ROOM_UPSTAIRS	// if the player enters the room the peds are in.
			FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
				IF i <> GROUPIE_WILLYS_PIECE	// don't do this for Willy's birb
					IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_B1")	//bDoneDialogue_BandSayWhereWillyIs
						IF sClubPed[i].AI = N1A_PED_AI_STATE_RELAXED
							sClubPed[i].iAiDelayTimer = GET_GAME_TIMER()
							sClubPed[i].AI = N1A_PED_AI_SETUP_AWARE_OF_PLAYER
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - ped AI set to N1A_PED_AI_SETUP_AWARE_OF_PLAYER : ", " PED ID:", i) ENDIF #ENDIF
						ENDIF
						sClubPed[CLUB_PED_BAND_MANAGER].iTimer = GET_GAME_TIMER()	// used to trigger ambient dialogue
					ELSE
						IF sClubPed[i].AI = N1A_PED_AI_SETUP_AWARE_OF_PLAYER
						OR sClubPed[i].AI = N1A_PED_AI_STATE_AWARE_OF_PLAYER
							sClubPed[i].iAiDelayTimer = GET_GAME_TIMER()
							sClubPed[i].AI = N1A_PED_AI_STATE_RELAXED
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - ped AI set to N1A_MUSICIAN_STATE_RELAXED : ", " PED ID:", i) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_B1")	//bDoneDialogue_BandSayWhereWillyIs
				TRIGGER_AMBIENT_DIALOGUE_UPSTAIRS(sClubPed[CLUB_PED_BAND_MANAGER].iTimer)	// timer used to delay dialogue
			ELSE
				SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(N1A_CPAT_BASE_ANIM)
			ENDIF
		ELIF ePlayerInsideClubStatus = N1A_RCR_LANDING_UPSTAIRS	// if the player is stood upstairs on the landing area
			TRIGGER_AMBIENT_DIALOGUE_UPSTAIRS(sClubPed[CLUB_PED_BAND_MANAGER].iTimer)	// timer used to delay dialogue
		ELSE
			KILL_AND_STORE_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS()
		ENDIF	
		/*IF ePlayerInsideClubStatus = N1A_RCR_ROOM_UPSTAIRS	// added to the IS_PLAYER_INTIMIDATE_SPECIFIC_PED checks
			IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
			OR IS_PED_IN_COMBAT(PLAYER_PED_ID())	// is player scrapping in the room)
				bHasPlayerScarredClubPedsUpstairs = TRUE
				KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
				sClubPed[i].iAiDelayTimer = 0	// ensure the ai will trigger straight away
				sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - combat or stealth kill - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
			ENDIF
		ENDIF*/
		FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
			IF i <> GROUPIE_WILLYS_PIECE
				IF sClubPed[i].AI <> N1A_PED_AI_SETUP_FLEE
				AND sClubPed[i].AI <> N1A_PED_AI_STATE_FLEE
					IF eMusicianState != N1A_MUSICIAN_STATE_RELAXED
						bHasPlayerScarredClubPedsUpstairs = TRUE
						KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
						sClubPed[i].iAiDelayTimer = GET_GAME_TIMER()
						sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
						MAKE_PED_SCREAM(sClubPed[i].pedIndex)	// B*1516087 - make some peds scream
						FORCE_PED_PANIC_WALLA()					// B*1516087 - make some peds scream
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - Willy has kicked off - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
					ELIF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sClubPed[i].pedIndex, sClubPed[i].iFrameCountLastSeenPlayer, FALSE)
						bHasPlayerScarredClubPedsUpstairs = TRUE
						KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
						sClubPed[i].iAiDelayTimer = 0	// ensure the ai will trigger straight away
						sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
						MAKE_PED_SCREAM(sClubPed[i].pedIndex)	// B*1516087 - make some peds scream
						FORCE_PED_PANIC_WALLA()					// B*1516087 - make some peds scream
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - intimidated - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
					ELIF ePlayerInsideClubStatus <> N1A_RCR_INVALID	// need this check as player could probably shoot from outside the club and pass the bullet in area test
					AND IS_ANYONE_SHOOTING_NEAR_PED(sClubPed[i].pedIndex)
						KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
						bHasPlayerScarredClubPedsUpstairs = TRUE
						sClubPed[i].iAiDelayTimer = 0	// ensure the ai will trigger straight away
						sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
						MAKE_PED_SCREAM(sClubPed[i].pedIndex)	// B*1516087 - make some peds scream
						FORCE_PED_PANIC_WALLA()					// B*1516087 - make some peds scream
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - shot near - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
					ELIF (ePlayerInsideClubStatus = N1A_RCR_ROOM_UPSTAIRS AND IS_PED_IN_COMBAT(PLAYER_PED_ID()))	// additional checks to make her flee as soon as the player is fighting in the room
					OR IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex, N1A_RCR_ROOM_UPSTAIRS)	
						KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
						bHasPlayerScarredClubPedsUpstairs = TRUE
						sClubPed[i].iAiDelayTimer = GET_GAME_TIMER()
						sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
						MAKE_PED_SCREAM(sClubPed[i].pedIndex)	// B*1516087 - make some peds scream
						FORCE_PED_PANIC_WALLA()					// B*1516087 - make some peds scream
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - Willy, guard, or player fighting in room - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
					ENDIF
					// trigger bumped dialogue if possible
					IF IS_PLAYER_BUMPING_INTO_CLUB_PED(i)
						KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
						TRIGGER_PLAYER_BUMPED_DIALOGUE_FOR_CLUB_PEDS(i)
						SET_SPECIFIC_CLUB_PED_UPSTAIRS_BUMPED_ANIM(i)
						bHasPlayerScarredClubPedsUpstairs = TRUE
						sClubPed[i].iAiDelayTimer = 0	// ensure the ai will trigger straight away
						sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - intimidated - ped AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		// if the player has scarred the club peds upstairs make them all flee
		FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
			IF i != GROUPIE_WILLYS_PIECE
				IF sClubPed[i].AI != N1A_PED_AI_SETUP_FLEE
				AND sClubPed[i].AI != N1A_PED_AI_STATE_FLEE
					sClubPed[i].iAiDelayTimer = GET_GAME_TIMER()
					sClubPed[i].AI = N1A_PED_AI_SETUP_FLEE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - bHasPlayerScarredClubPedsUpstairs - ped FORCED AI set to N1A_PED_AI_SETUP_FLEE : ", " PED ID:", i) ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDFOR
		// reaction dialogue from band manager - only play if player is in the room
		IF NOT bDonedialogue_ClubPedsReactThreatened
			KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)
			IF IS_PED_UNINJURED(sClubPed[CLUB_PED_BAND_MANAGER].pedIndex)
				IF IS_ENTITY_IN_RANGE_COORDS(sClubPed[CLUB_PED_BAND_MANAGER].pedIndex, vPlayerPos, 20.0)
					TEXT_LABEL_23 tlTempReactDialogueRoot
					IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
					// B*1511108 - try to ensure weapon is visible
					AND (IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
					OR IS_PED_RELOADING(PLAYER_PED_ID()))
						// He's tooled up, look out!    
						tlTempReactDialogueRoot = "NIG1A_B2"
					ELSE
						// Shit! Security!  
						tlTempReactDialogueRoot = "NIG1A_B3"
					ENDIF
					IF ePlayerInsideClubStatus = N1A_RCR_ROOM_UPSTAIRS
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlTempReactDialogueRoot, CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
							bDonedialogue_ClubPedsReactThreatened = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - done -  bDonedialogue_ClubPedsReactThreatened root", tlTempReactDialogueRoot) ENDIF #ENDIF
						ENDIF
					ELIF ePlayerInsideClubStatus = N1A_RCR_LANDING_UPSTAIRS
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlTempReactDialogueRoot, CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
								bDonedialogue_ClubPedsReactThreatened = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - done -  bDonedialogue_ClubPedsReactThreatened root", tlTempReactDialogueRoot) ENDIF #ENDIF
							ENDIF
						ENDIF					
					ENDIF
				ENDIF
			ELSE
				bDonedialogue_ClubPedsReactThreatened = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - bDonedialogue_ClubPedsReactThreatened skipped ped dead") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	//IF (GET_GAME_TIMER() - iUpdateClubPeds_ThisFrameCounter > 6)
		UPDATE_STANDARD_CLUB_PED_AI(iClubPedID_ToUpdateThisFrameCounter)
		//iUpdateClubPeds_ThisFrameCounter = GET_GAME_TIMER()
		IF (iClubPedID_ToUpdateThisFrameCounter >= (MAX_NUM_CLUB_PEDS- 1))
			iClubPedID_ToUpdateThisFrameCounter = 0
		ELSE
			iClubPedID_ToUpdateThisFrameCounter++
		ENDIF
	//ENDIF
ENDPROC

/// PURPOSE:
///   kill's any ongoing ambient dialogue upstairs
FUNC BOOL KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_WILLY(BOOL bFinishCurrentLine = TRUE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL1", FALSE)	//only the ambient dialogue between Willy and the groupie
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL2", FALSE)	//only the ambient dialogue between Willy and the groupie
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_WILL3", FALSE)	//only the ambient dialogue between Willy and the groupie
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_IDLE2", FALSE)	//only the ambient dialogue between Willy and the groupie
			IF bFinishCurrentLine
				KILL_FACE_TO_FACE_CONVERSATION()
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_Willy - Return TRUE killed ambient ialogue") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles triggering ambient dialogue between Willy and groupie in the main room
///    4 different conversations to trigger...the can be restarted too
/// PARAMS:
///    iTimer_DialogueTriggerDelay - timer used to delay the dialogue triggering
/// RETURNS:
///    TRUE if a dialogue exchange is triggered
FUNC BOOL TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE(INT &iTimer_DialogueTriggerDelay)

	// B*1516098 - for some reason this dialogue is stubborn and doesn't stop when skipping, so making sure if won't trigger if skipping
	IF bFinishedStageSkipping
	
		N1A_Willy_GROUPIE_ANIM_TYPE eWillyGroupieSyncAnims = N1A_WGAT_BASE_2_ANIM	// groupie has arms up
		
		// B*1991867 - if we are leaving scene 3 need to use different base anim
		IF iCounter_AmbientDialogueWillyGroupie = 3
			eWillyGroupieSyncAnims = N1A_WGAT_BASE_ANIM	// groupie has arms down
			CPRINTLN(DEBUG_MISSION, "TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE : set eWillyGroupieSyncAnims = N1A_WGAT_BASE_ANIM")
		ENDIF
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bPausedDialogue_AmbientWillyGroupie	// restart paused dialogue straight away
				iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
				
				// check for reason to pause the dialogue if the player leaves the area - could also be paused by other triggered dialogue
				IF ePlayerInsideClubStatus != N1A_RCR_MAIN_ROOM
				AND IS_ENTITY_OCCLUDED(sWillyPed.pedIndex)
					IF KILL_AND_STORE_CURRENT_Willy_AND_GROUPIE_AMBIENT_DIALOGUE()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE - killed dialogue as player not in room and Willy isn't on screen") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM
				INT iLocal_TotalDialogueExchanges = 4
				TEXT_LABEL_23 tlCurrentDialogueRoot = ""
				IF iCounter_AmbientDialogueWillyGroupie < iLocal_TotalDialogueExchanges
					INT iTimeDelay_DialogueTrigger = GET_RANDOM_INT_IN_RANGE(1000, 2000)
					// B*1991867 - bigger delay time needed between these convo blends
					IF iCounter_AmbientDialogueWillyGroupie = 3
						iTimeDelay_DialogueTrigger = 2500
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE - &&&&&&") ENDIF #ENDIF
					ENDIF
					// no delay for first dialogue exchange or if we are restarting an existing exchange
					IF bPausedDialogue_AmbientWillyGroupie
						iTimeDelay_DialogueTrigger = 100	// Bug fix - straight away causes isssues with security triggering it's dialogue since it gets called after this each from, so setup cancel occurs over and over
					ENDIF
					IF HAS_TIME_PASSED(iTimer_DialogueTriggerDelay, iTimeDelay_DialogueTrigger)
						SWITCH iCounter_AmbientDialogueWillyGroupie
							CASE 0
								tlCurrentDialogueRoot = "NIG1A_WILL1"	// Willy banters with a young groupie in the main room - v1
								// I bet I could hit your G sharp every time.
								// You guys were amazing tonight.
								// Aye, I know.  
								// Chin Stainer is like my ultimate break-up song.  
								// Oh my God, that track is so layered.
								// It's like a Renaissance painting... It's like underneath those knobs and titties it's real art, you know what I mean?
							BREAK
							CASE 1
								tlCurrentDialogueRoot = "NIG1A_WILL2"	// Willy banters with a young groupie in the main room - v2
								// Have you got some any chewing gum or benzos? 
								// Oh, no I don't, I'm sorry. Have the others left already?
								// Aye, they went back to hotel. Percy forgot to take his statins, 
								// and Jezz's prostate is giving him jip.
								// Is it true that you guys trashed the suite?
								// Nah, that's just a PR thing. I mean I still get wrecked every night, but it doesn't define me.  
							BREAK
							CASE 2
								tlCurrentDialogueRoot = "NIG1A_WILL3"	// Willy banters with a young groupie in the main room - v3
								// You know if these pants could talk, I'd have to kill them.  
								// I know every word to Dangerous Bastard.  
								// We did this one gig we did here in the eighties, so manycunts OD'd, they had to officially call it genocide. 
								// I wish I'd been born to see that.  
								// Wait. How old are you, sweetheart?
								// I'm twenty. I mean, like, twenty two.
								// Can't be too careful. So many celebs in Britain have been done for jailbait these days.  
								eWillyGroupieSyncAnims = N1A_WGAT_YOU_KNOW
							BREAK
							CASE 3
								tlCurrentDialogueRoot = "NIG1A_IDLE2"	// They run out of idle banter
								// This is pure awkward. I've run out of things to say, 
								// normally the ladies are like gobbling by now.
								// That's fine, I don't mind just watching you.
								eWillyGroupieSyncAnims = N1A_WGAT_THIS_IS_AWKWARD
							BREAK
						ENDSWITCH
						IF IS_PED_UNINJURED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)
							IF bPausedDialogue_AmbientWillyGroupie
								IF NIG1A_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlCurrentDialogueRoot, tlDialogueLinePaused_WillyGroupieAmbient, CONV_PRIORITY_MEDIUM)
									bPausedDialogue_AmbientWillyGroupie = FALSE
									iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
									iCounter_AmbientDialogueWillyGroupie++	
									// trigger the sync anim to match
									SET_WILLY_GROUPIE_SYNCED_ANIM(eWillyGroupieSyncAnims)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE - restarted paused dialogue root : ", tlCurrentDialogueRoot, " : line : ", tlDialogueLinePaused_WillyGroupieAmbient) ENDIF #ENDIF
									RETURN TRUE
								ENDIF
							ELSE
								IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, tlCurrentDialogueRoot, CONV_PRIORITY_MEDIUM)
									bPausedDialogue_AmbientWillyGroupie = FALSE
									iTimer_DialogueTriggerDelay = GET_GAME_TIMER()
									SET_WILLY_GROUPIE_SYNCED_ANIM(eWillyGroupieSyncAnims)
									iCounter_AmbientDialogueWillyGroupie++
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE - triggered dialogue root : ", tlCurrentDialogueRoot) ENDIF #ENDIF
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		// set the base by default if it's not runnings
		SET_WILLY_GROUPIE_SYNCED_ANIM(eWillyGroupieSyncAnims)	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Manages the interaction with the musician
PROC MANAGE_WILLY()
	IF IS_PED_UNINJURED(sWillyPed.pedIndex)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sWillyPed.pedIndex, sWillyPed.iFrameCountLastSeenPlayer)
		SWITCH eMusicianState
			CASE N1A_MUSICIAN_STATE_RELAXED
				IF NOT bDoneDialogue_MusicianSpottedPlayer
					TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE(sWillyPed.iTimer)
					IF ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM
						IF IS_PED_UNINJURED(sWillyPed.pedIndex)
						AND (VDIST(vPlayerPos, GET_ENTITY_COORDS(sWillyPed.pedIndex)) <= 5.5)	
							KILL_AND_STORE_CURRENT_Willy_AND_GROUPIE_AMBIENT_DIALOGUE(FALSE)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF												
							//Restart the spotted on foot convo, if it was paused
							IF bPausedDialogue_MusicianSpottedPlayer
								IF IS_PLAYER_CURRENT_WEAPON_LETHAL()	//when restarting the convo, if Trevor now has a leathal weapon and kick back out so the leathal weapon dialogue plays instead
								AND NOT ARE_STRINGS_EQUAL(sNigel1A_DialogueRoot_MusicianSpotsTrevor, "NIG1A_MSP3")	// trevor had leathal weapon when musician spotted him so don't start brawling whilst weapon out
									bPausedDialogue_MusicianSpottedPlayer = FALSE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESTARTED ON FOOT DIALOGUE cancelled as player now has leathal weapon out	**********") ENDIF #ENDIF
								ELSE
									IF NIG1A_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, sNigel1A_DialogueRoot_MusicianSpotsTrevor, tlPausedDialogue_MusicianSpottedPlayer, CONV_PRIORITY_MEDIUM)
										bPausedDialogue_MusicianSpottedPlayer = FALSE
										bDoneDialogue_MusicianSpottedPlayer = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESTARTED ON FOOT DIALOGUE	**********") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
									IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MSP3", CONV_PRIORITY_MEDIUM) //)										
										// Don't come near me with that, you prick!
										// Is that any way to speak to your biggest fan? Smile, Willy.
										sNigel1A_DialogueRoot_MusicianSpotsTrevor = "NIG1A_MSP3"	// used when choosing which dialogue to restart
										bDoneDialogue_MusicianSpottedPlayer = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATED ON FOOT DIALOGUE - TREVOR WITH LEATHAL WEAPON	**********") ENDIF #ENDIF
									ENDIF
								ELSE	
									IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MSP1", CONV_PRIORITY_MEDIUM)										
										// Hey, no photographs mate, alright.
										// That's okay, I want something more personal.
										// Piss off, you twat, before I boot your balls.
										
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.5, REPLAY_IMPORTANCE_LOWEST) 
										
										sNigel1A_DialogueRoot_MusicianSpotsTrevor = "NIG1A_MSP1"	// used when choosing which dialogue to restart
										bDoneDialogue_MusicianSpottedPlayer = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATED ON FOOT DIALOGUE -TREVOR WITH NON LEATHAL WEAPON  **********") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//pause the conversation if the player leaves the area so we can resume when he returns
					IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sNigel1A_DialogueRoot_MusicianSpotsTrevor)	//only restart on foot dialogue
						IF ePlayerInsideClubStatus != N1A_RCR_MAIN_ROOM
							// Bug fix - could return dialogue as playing in first frame even though the line is invalid, so wait for line to be valid
							TEXT_LABEL_23 tl23CurrentDialogueLine
							tl23CurrentDialogueLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							IF NOT IS_STRING_NULL_OR_EMPTY(tl23CurrentDialogueLine)
								bPausedDialogue_MusicianSpottedPlayer = TRUE
								bDoneDialogue_MusicianSpottedPlayer = FALSE
								tlPausedDialogue_MusicianSpottedPlayer = tl23CurrentDialogueLine
								KILL_FACE_TO_FACE_CONVERSATION()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STORED ON FOOT DIALOGUE FOR RESUME	**********") ENDIF #ENDIF
							ENDIF
						ENDIF
					ELSE
						// resume if player leaves the area
						//IF ePlayerInsideClubStatus != N1A_RCR_MAIN_ROOM
						//	TRIGGER_WILLY_AND_GROUPIE_AMBIENT_DIALOGUE(sWillyPed.iTimer)
						//ENDIF
					ENDIF
				ENDIF
				// delay Willy attacking if cause for reaction isn't directly attacking him (B*1072081 - delay Willy till groupie is clear)
				bHasWillySeenPlayerAttackHisGroupie = HAS_MUSICIAN_SEEN_PLAYER_ATTACKED_SPECIFIC_PED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)
				IF bHasWillySeenPlayerAttackHisGroupie
				OR HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(sWillyPed.pedIndex, sWillyPed.iFrameCountLastSeenPlayer)			
				OR (ePlayerInsideClubStatus = N1A_RCR_MAIN_ROOM AND IS_PED_IN_COMBAT(PLAYER_PED_ID()))	// additional checks to make her fight if player is scrapping close by
					sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
					sWillyPed.AI = N1A_PED_AI_SETUP_MELEE_ATTACK
					sWillyPed.iTimer = GET_GAME_TIMER()
					IF NOT bHasPlayerDamagedMusician	// used to trigger specific dialogue were Willy calls out to security
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
							bHasPlayerDamagedMusician = TRUE
							bDoneDialogue_MusicianStartBrawl = TRUE	// if player has damaged Willy skip over the initial dialogue exchange, so he call for security straight away
							sWillyPed.iAiDelayTimer = (GET_GAME_TIMER() - 1000)	// no delay if attack Willy directly
							sWillyPed.iTimer = (GET_GAME_TIMER() - 1000)			// no delay if attack Willy directly
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bHasPlayerDamagedMusician set TRUE") ENDIF #ENDIF
						ENDIF
					ENDIF
					KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_Willy(FALSE)
					IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sNigel1A_DialogueRoot_MusicianSpotsTrevor)	//only restart on foot dialogue
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID())	// begin tracking the player's damage during the brawl with Willy
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID())") ENDIF #ENDIF
					eMusicianState = N1A_MUSICIAN_STATE_BRAWLING
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING from N1A_MUSICIAN_STATE_RELAXED because player threaten Willy, attakced bird, is in combat in the room or the guard in the room has kicked off") ENDIF #ENDIF				
				ENDIF
				IF IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sWillyPed.pedIndex, sWillyPed.iFrameCountLastSeenPlayer, TRUE, 10.0)	// overriding the dection dist so he doesn't spot player on the stairs with a gun
				OR ePlayerInsideClubStatus <> N1A_RCR_INVALID	// need this check as player could probably shoot from outside the club and pass the bullet in area test
				AND IS_ANYONE_SHOOTING_NEAR_PED(sWillyPed.pedIndex)
					sWillyPed.AI = N1A_PED_AI_SETUP_SURRENDERED
					sWillyPed.iAiDelayTimer = GET_GAME_TIMER()
					sWillyPed.iTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Player Intimidated Musician, IS_PLAYER_INTIMIDATING_SPECIFIC_PED() returned true") ENDIF #ENDIF
					//Used to detect player hurting him at this point
					IF IS_PED_UNINJURED(sWillyPed.pedIndex)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sWillyPed.pedIndex)
						CLEAR_PED_LAST_WEAPON_DAMAGE(sWillyPed.pedIndex)
						CLEAR_PED_LAST_DAMAGE_BONE(sWillyPed.pedIndex)
					ENDIF
					KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_Willy(FALSE)
					IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sNigel1A_DialogueRoot_MusicianSpotsTrevor)	//only restart on foot dialogue
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					eMusicianState = N1A_MUSICIAN_STATE_SURRENDERED
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_SURRENDERED from N1A_MUSICIAN_STATE_BRAWLING because player intimidated ped") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE N1A_MUSICIAN_STATE_BRAWLING
				IF NOT bHasPlayerDamagedMusician	// used to trigger specific dialogue were Willy calls out to security
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
						bHasPlayerDamagedMusician = TRUE
						bDoneDialogue_MusicianStartBrawl = TRUE	// if player has damaged Willy skip over the initial dialogue exchange, so he call for security straight away
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bHasPlayerDamagedMusician set TRUE") ENDIF #ENDIF
					ENDIF
				ENDIF
				IF NOT bDoneDialogue_MusicianStartBrawl
					//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//ENDIF
					KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_Willy(FALSE)
					
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_SD3")	// You fucking prick.  Come here.
					AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_SD5") //  Hey, hey! Intruder!
						IF HAS_TIME_PASSED(sWillyPed.iTimer, 500)
						AND sWillyPed.AI = N1A_PED_AI_STATE_MELEE_ATTACK
							// only say it when he can see the player
							IF NOT HAS_FRAME_COUNTER_PASSED(sWillyPed.iFrameCountLastSeenPlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
								IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MSP4", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
									// You want a go, do you?!	
									SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
									bDoneDialogue_MusicianStartBrawl = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING done bDoneDialogue_MusicianStartBrawl  trevor brawl dialogue") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bDoneDialogue_MusicianShoutSecurity
						IF bHasPlayerDamagedMusician
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_Willy(FALSE)
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_M1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Security! Where's security!?	
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
								bDoneDialogue_MusicianShoutSecurity = TRUE
							ENDIF
						ENDIF
					ENDIF
					// Additional dialogue during the brawl
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						sWillyPed.iTimer = GET_GAME_TIMER()				
					ELSE
						IF HAS_TIME_PASSED(sWillyPed.iTimer, (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE + GET_RANDOM_INT_IN_RANGE(0, 1000)))							
							IF sWillyPed.AI = N1A_PED_AI_SETUP_MELEE_ATTACK	
							OR sWillyPed.AI = N1A_PED_AI_STATE_MELEE_ATTACK
								IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 8.0)
									IF NOT bDoneDialogue_PlayerReturnsTofightMusician
										IF iDialogueCounter_PlayerReturnedMusicianInBrawl < TOTAL_PLAYER_RETURNED_MUSICIAN_BRAWL_DIALOGUE
											IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MP3", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())  // ) 
												// Are you back for your Glasgow kiss?  
												// I've about enough of you.
												// You must be a glutton for punishment.
												iDialogueCounter_PlayerReturnedMusicianInBrawl++
												sWillyPed.iTimer = GET_GAME_TIMER()
												bDoneDialogue_PlayerReturnsTofightMusician = TRUE
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING done dialogue - player returns to brawl") ENDIF #ENDIF
											ENDIF
										ELSE
											bDoneDialogue_PlayerReturnsTofightMusician = TRUE
										ENDIF										
									ELSE
										// longer wait time for the random brawl lines
										IF HAS_TIME_PASSED(sWillyPed.iTimer, (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE + GET_RANDOM_INT_IN_RANGE(15000, 5000)))
											IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_BRAWL", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())  // ) 
												// You think you're hard, do you?  
												// Come on, then!
												// Have a go!
												// I'll do you in!  
												// You fucking prick!  
												// You look like you escaped from the loony bin!
												// You don't know the half of me!  
												// I grew up on a council estate!
												// I'll cave your heed in!
												sWillyPed.iTimer = GET_GAME_TIMER()
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING done dialogue - brawling") ENDIF #ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF sWillyPed.AI = N1A_PED_AI_SETUP_REACHED_PERIMETER	
							OR sWillyPed.AI = N1A_PED_AI_STATE_REACHED_PERIMETER
							OR (ePlayerInsideClubStatus = N1A_RCR_INVALID
							AND NOT IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 12.0))	// if player is running away
								IF iDialogueCounter_PlayerLeftMusicianInBrawl < TOTAL_PLAYER_LEFT_MUSICIAN_BRAWL_DIALOGUE
									IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 50.0)	//don't do the dialogue if Trevor is far away.
										IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MP1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()) //) 
											// That's right, run away, you pussy!  
											// See if you can find your bollocks on the way out!
											// See, you mess with Willie, you get fucked!
											iDialogueCounter_PlayerLeftMusicianInBrawl++
											sWillyPed.iTimer = GET_GAME_TIMER()
											bDoneDialogue_PlayerReturnsTofightMusician = FALSE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING done dialogue - player left area") ENDIF #ENDIF
										ENDIF
									ENDIF
								ELSE
									bDoneDialogue_PlayerReturnsTofightMusician = FALSE
								ENDIF								
							ELIF sWillyPed.AI = N1A_PED_AI_SETUP_OUT_OF_REACH
							OR sWillyPed.AI = N1A_PED_AI_STATE_OUT_OF_REACH
								IF iDialogueCounter_PlayerOutOfReachMusicianInBrawl < TOTAL_PLAYER_OUT_OF_REACH_MUSICIAN_BRAWL_DIALOGUE
									IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MP2", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()) //) 
										// Come here and fight like a man.
										// What are you hiding over there for?  
										// Let's go, you pansy! I'm right here!   
										iDialogueCounter_PlayerOutOfReachMusicianInBrawl++
										sWillyPed.iTimer = GET_GAME_TIMER()
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1A_MUSICIAN_STATE_BRAWLING done dialogue - player out of reach") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF					
					ENDIF
					IF HAS_PLAYER_BEATEN_UP_SPECIFIC_PED(sWillyPed.pedIndex, TRUE)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TOOTH IS TO BE KNOCKED OUT - PLAYER BEAT UP THE MUSICIAN") ENDIF #ENDIF
						sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP
						// temp health boost until his tooth gets knocked out
						IF (GET_ENTITY_HEALTH(sWillyPed.pedIndex) < 180.0)
							SET_ENTITY_HEALTH(sWillyPed.pedIndex, 180)
							CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN: player beat up Willy - give temp health boost to 180 for ragdoll and tooth knockout +-+-")
						ENDIF
						iTimer_BackupKnockOutToothTrigger = GET_GAME_TIMER()	//used for knock out tooth backup if IS_PED_RAGOLL keeps returning true for too long
						sWillyPed.iTimer = GET_GAME_TIMER()
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)	// end tracking the player's damage
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)") ENDIF #ENDIF
						eMusicianState = N1A_MUSICIAN_STATE_KNOCKED_DOWN
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_KNOCKED_DOWN from N1A_MUSICIAN_STATE_BRAWLING because player BEAT UP MUSICIAN") ENDIF #ENDIF
					ENDIF
				ENDIF			
				IF IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sWillyPed.pedIndex, sWillyPed.iFrameCountLastSeenPlayer, TRUE)	
				OR ePlayerInsideClubStatus <> N1A_RCR_INVALID	// need this check as player could probably shoot from outside the club and pass the bullet in area test
				AND IS_ANYONE_SHOOTING_NEAR_PED(sWillyPed.pedIndex)
					sWillyPed.AI = N1A_PED_AI_SETUP_SURRENDERED
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Player Intimidated Musician, IS_PLAYER_INTIMIDATING_SPECIFIC_PED() returned true") ENDIF #ENDIF
					//Used to detect player hurting him at this point
					IF IS_PED_UNINJURED(sWillyPed.pedIndex)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sWillyPed.pedIndex)
						CLEAR_PED_LAST_WEAPON_DAMAGE(sWillyPed.pedIndex)
						CLEAR_PED_LAST_DAMAGE_BONE(sWillyPed.pedIndex)
					ENDIF
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)	// end tracking the player's damage
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)") ENDIF #ENDIF
					eMusicianState = N1A_MUSICIAN_STATE_SURRENDERED
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_SURRENDERED from N1A_MUSICIAN_STATE_BRAWLING because player intimidated ped") ENDIF #ENDIF
				ENDIF				
			BREAK
			CASE N1A_MUSICIAN_STATE_SURRENDERED
				IF NOT bDoneDialogue_MusicianSurrendered
					IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 15.0)
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_SD7")	// "Fuck, it's not worth this.  Call the Police."
						AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MSP3")	// Willy spot player with weapon dialogue
							// wait until he can see the player
							IF NOT HAS_FRAME_COUNTER_PASSED(sWillyPed.iFrameCountLastSeenPlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
								IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MSU2", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
									// Okay, okay, calm the fuck down! Just tell me what you want.  	
									SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
									sWillyPed.iTimer = GET_GAME_TIMER()
									bDoneDialogue_MusicianSurrendered = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - bDoneDialogue_MusicianSurrendered - done") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// make Willy flee as part of bug 1050128 - imran request, not hooked up yet as its hard to hit him if he's fleeing
					IF sWillyPed.AI = N1A_PED_AI_SETUP_SURRENDERED
					OR sWillyPed.AI = N1A_PED_AI_STATE_SURRENDERED
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MSU2")	// // Oh no don't hurt me, don't hurt me!  What do you want?				
							sWillyPed.iTimer = GET_GAME_TIMER()
						ELIF HAS_TIME_PASSED(sWillyPed.iTimer, 3500)
							IF sWillyPed.AI = N1A_PED_AI_STATE_SURRENDERED	// ensures exit sync anim can playout
								sWillyPed.AI = N1A_PED_AI_SETUP_FLEE	// new make him flee shortly after surrender
							ENDIF
						ENDIF
					ELIF sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
					OR sWillyPed.AI = N1A_PED_AI_STATE_FLEE
						//additional flee dialogue
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							sWillyPed.iTimer = GET_GAME_TIMER()
						ELSE
							IF HAS_TIME_PASSED(sWillyPed.iTimer, (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE + GET_RANDOM_INT_IN_RANGE(0, 2500)))
								IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 25.0)
									// B*1354912 - Willy shouted from outside interior	- allow dialogue if both are outside interior or both in the same room in the interior
									IF NOT IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex)
									AND ePlayerInsideClubStatus = N1A_RCR_INVALID
									OR IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex, ePlayerInsideClubStatus)
										IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_FLEE", CONV_PRIORITY_MEDIUM)
											// You stay the hell away from me!
											// Somebody stop that nutter!  
											// He's trying to kill me! I'm a musical icon!  
											// I bet Crow doesn't have to deal with this shit!  
											sWillyPed.iTimer = GET_GAME_TIMER()
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_SURRENDERED - done random flee dialogue : NIG1A_FLEE") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE()
				ENDIF
				IF bPlayerUsedWeaponToDamageWilly
					IF NOT bDoneDialogue_MusicianShotAtByPlayer
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MCSG", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Argh! Fuck me, that hurts! 
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
								bDoneDialogue_MusicianShotAtByPlayer = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - bDoneDialogue_MusicianShotAtByPlayer - done") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
					IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
						IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_UNARMED)
						OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYMELEE)
						OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(sWillyPed.pedIndex, WEAPONTYPE_STUNGUN)
							sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP
							iTimer_BackupKnockOutToothTrigger = GET_GAME_TIMER()	//used for knock out tooth backup if IS_PED_RAGOLL keeps returning true for too long
							eMusicianState = N1A_MUSICIAN_STATE_KNOCKED_DOWN
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_KNOCKED_DOWN from N1A_MUSICIAN_STATE_SURRENDERED because player standard damaged musician") ENDIF #ENDIF
						ELSE
							IF NOT IS_PED_BEING_STUNNED(sWillyPed.pedIndex)	// don't do this whilst he's currently taking damage from being stunned.
								// if the player shoots Willy with a leath weapon in a critical area, kill him
								IF IS_PLAYER_CURRENT_WEAPON_LETHAL(TRUE, FALSE, FALSE)													
									PED_BONETAG tempBoneTag
									IF GET_PED_LAST_DAMAGE_BONE(sWillyPed.pedIndex, tempBoneTag)
										SWITCH tempBoneTag
											CASE BONETAG_PELVIS
											FALLTHRU
											CASE BONETAG_SPINE
											FALLTHRU
											CASE BONETAG_SPINE1
											FALLTHRU
											CASE BONETAG_SPINE2
											FALLTHRU
											CASE BONETAG_SPINE3
											FALLTHRU
											CASE BONETAG_NECK
											FALLTHRU
											CASE BONETAG_HEAD
												SET_ENTITY_HEALTH(sWillyPed.pedIndex, 0)
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MUSICIAN KILLED - player shot vital area") ENDIF #ENDIF
											BREAK
											DEFAULT
											BREAK
										ENDSWITCH
									ENDIF
								ENDIF
							ENDIF
							bPlayerUsedWeaponToDamageWilly = TRUE	// used to trigger additional dialogue
							sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP
							iTimer_BackupKnockOutToothTrigger = GET_GAME_TIMER()	//used for knock out tooth backup if IS_PED_RAGOLL keeps returning true for too long
							eMusicianState = N1A_MUSICIAN_STATE_KNOCKED_DOWN
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_KNOCKED_DOWN from N1A_MUSICIAN_STATE_SURRENDERED because player weapon damaged musician") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF	
				// if he ragdolls from player running him over trigger the tooth knockout
				IF HAS_PLAYER_IN_VEHICLE_POTENTIALLY_JUST_RUN_OVER_PED(sWillyPed.pedIndex, vPlayerPos)
					sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_BEATEN_UP
					iTimer_BackupKnockOutToothTrigger = GET_GAME_TIMER()	//used for knock out tooth backup if IS_PED_RAGOLL keeps returning true for too long
					eMusicianState = N1A_MUSICIAN_STATE_KNOCKED_DOWN
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_KNOCKED_DOWN from N1A_MUSICIAN_STATE_SURRENDERED because player ran over musician") ENDIF #ENDIF
				ENDIF
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sWillyPed.pedIndex)
				CLEAR_PED_LAST_WEAPON_DAMAGE(sWillyPed.pedIndex)
				IF NOT bShouldPlayerGetWantedLevelForWeaponInClub
					// player should only get a wanted level if he was inside the club
					IF ePlayerInsideClubStatus != N1A_RCR_INVALID
						IF iTimer_PlayerReceivesScriptedWantedLevel = 0
							iTimer_PlayerReceivesScriptedWantedLevel = GET_GAME_TIMER()	// used to give the player a scripted wanted level if is using a weapon in the club
						ENDIF
						bShouldPlayerGetWantedLevelForWeaponInClub = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN bShouldPlayerGetWantedLevelForWeaponInClub = TRUE") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE N1A_MUSICIAN_STATE_KNOCKED_DOWN
				IF NOT bDoneDialogue_MusicianKnockedDown
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					KILL_ANY_ONGOING_CURRENT_AMBIENT_DIALOGUE_WILLY(FALSE)
					IF bPlayerUsedWeaponToDamageWilly
						IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MCSG", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// Argh! Fuck me, that hurts! 
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
							bDoneDialogue_MusicianKnockedDown = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - bDoneDialogue_MusicianKnockedDown - done weapon damaged line NIG1A_MCSG") ENDIF #ENDIF
						ENDIF
					ELSE
						IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MKD", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// Argh!
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
							bDoneDialogue_MusicianKnockedDown = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - bDoneDialogue_MusicianKnockedDown - done standard damaged line NIG1A_MCSG") ENDIF #ENDIF
						ENDIF	
					ENDIF
				ENDIF	
				//wait for Musician to stop falling before delay for the tooth getting knocked out
				IF NOT IS_PED_RAGDOLL(sWillyPed.pedIndex)
				OR HAS_TIME_PASSED(iTimer_BackupKnockOutToothTrigger, 2000)	//backup timer to knock the tooth out if ragdoll isn't ending quick enough
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sWillyPed.pedIndex, FALSE)
					eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT from N1A_MUSICIAN_STATE_KNOCKED_DOWN because ragdoll ended or times out") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT
				
			BREAK		
			CASE N1A_MUSICIAN_STATE_FLEE
				//additional flee dialogue
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					sWillyPed.iTimer = GET_GAME_TIMER()
				ELSE
					IF HAS_TIME_PASSED(sWillyPed.iTimer, (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE + GET_RANDOM_INT_IN_RANGE(0, 2500)))
						IF IS_ENTITY_IN_RANGE_COORDS(sWillyPed.pedIndex, vPlayerPos, 25.0)
							// B*1354912 - Willy shouted from outside interior	- allow dialogue if both are outside interior or both in the same room in the interior
							IF NOT IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex)
							AND ePlayerInsideClubStatus = N1A_RCR_INVALID
							OR IS_ENTITY_IN_SPECIFIC_ROOM_INSIDE_MUSIC_CLUB_INTERIOR(sWillyPed.pedIndex, ePlayerInsideClubStatus)
								IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_FLEE", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
									// You stay the hell away from me!
									// Somebody stop that nutter!  
									// He's trying to kill me! I'm a musical icon!  
									// I bet Crow doesn't have to deal with this shit!  
									sWillyPed.iTimer = GET_GAME_TIMER()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - N1A_MUSICIAN_STATE_FLEE - done random flee dialogue : NIG1A_FLEE") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK		
		ENDSWITCH	
		UPDATE_MUSICIAN_PED_AI()
	ELSE
		IF eMusicianState <> N1A_MUSICIAN_STATE_DEAD
			IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLA_KILLED_WILLIE))
				SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLA_KILLED_WILLIE))
				CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLA_KILLED_WILLIE) set")		
			ENDIF
			eMusicianState = N1A_MUSICIAN_STATE_DEAD
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - eMusicianState = N1A_MUSICIAN_STATE_DEAD") ENDIF #ENDIF
		ENDIF
		IF NOT bDoneDialogue_TrevorKilledWilly
			IF DOES_ENTITY_EXIST(sWillyPed.pedIndex)		
				VECTOR vDeadPos = GET_ENTITY_COORDS(sWillyPed.pedIndex, FALSE)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDeadPos, 15.0)	// only play if player was close to the body
						IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_TC3", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()) //)
							// Oops, sorry Love Fist fans.  
							bDoneDialogue_TrevorKilledWilly = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE MUSICIAN - bDoneDialogue_TrevorKilledWilly - TRUE", "***") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
  
/// PURPOSE:
///    Used by checkpoints, Z skip, P skip and J skip to setup a skip to a certain stage in the mission.
PROC SKIP_STAGE()	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		KILL_ANY_CONVERSATION()
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		CLEAR_PRINTS()	
		SWITCH eMissionStage
			CASE MISSION_STAGE_ENTER_THE_MUSIC_CLUB
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe guard that the SS_SETUP has already had chance to run.
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-563.0730, 292.5788, 86.5763>>, 79.2186)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_FIND_CELEB	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 25.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_ENTER_THE_MUSIC_CLUB : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_ENTER_THE_MUSIC_CLUB : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_FIND_CELEB
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe guard that the SS_SETUP has already had chance to run.
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-556.3416, 283.2208, 81.1763>>, 328.4892, FALSE, TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_KNOCK_OUT_TOOTH	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 25.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_FIND_CELEB : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					RESET_MISSION_PED_TIMERS()
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_FIND_CELEB : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_KNOCK_OUT_TOOTH
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					// stay where they were if he wasn't beaten up already
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF sWillyPed.AI < N1A_PED_AI_STATE_MUSICIAN_BEATEN_UP
							SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-554.1479, 287.9976, 81.1763>>, 152.3380, FALSE, TRUE)		
						ENDIF
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_COLLECT_TOOTH	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_FIND_CELEB : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					bSetExitSceneForWillyAndGroupie = TRUE	// B*1516098 skip the sync scene exit anim!
					IF IS_PED_UNINJURED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)	
						// having issues with it not stopping instantly, so added this extra clear
						IF IsPedPerformingTask(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
							STOP_SYNCHRONIZED_ENTITY_ANIM(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, INSTANT_BLEND_OUT, TRUE)
						ENDIF													
						FREEZE_ENTITY_POSITION(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, FALSE)
						CLEAR_PED_TASKS_IMMEDIATELY(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)	
						SAFE_SET_ENTITY_HEADING_FACE_ENTITY(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, PLAYER_PED_ID())
						sClubPed[GROUPIE_WILLYS_PIECE].AI = N1A_PED_AI_SETUP_FLEE
					ENDIF
					IF IS_PED_UNINJURED(sWillyPed.pedIndex)									
						FREEZE_ENTITY_POSITION(sWillyPed.pedIndex, FALSE)	
						// having issues with it not stopping instantly, so added this extra clear
						IF IsPedPerformingTask(sWillyPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
							STOP_SYNCHRONIZED_ENTITY_ANIM(sWillyPed.pedIndex, INSTANT_BLEND_OUT, TRUE)
						ENDIF
						CLEAR_PED_TASKS_IMMEDIATELY(sWillyPed.pedIndex)
						SAFE_TELEPORT_PED(sWillyPed.pedIndex, <<-554.01, 286.01, 81.1762>>, 27.18, FALSE, FALSE)		
						SAFE_SET_ENTITY_HEADING_FACE_ENTITY(sWillyPed.pedIndex, PLAYER_PED_ID())
						SET_PED_CAN_RAGDOLL(sWillyPed.pedIndex, TRUE)					
						IF (GET_ENTITY_HEALTH(sWillyPed.pedIndex) >= 137.5)	// fatigued threshold used to be 165
							SET_ENTITY_HEALTH(sWillyPed.pedIndex, 135)
							CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", " set Willy health 135 +-+-")							
						ENDIF
					ENDIF
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_SET_ENTITY_HEADING_FACE_ENTITY(PLAYER_PED_ID(), sWillyPed.pedIndex)						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
					ENDIF
					bDoneDialogue_MusicianStartBrawl = TRUE						
					sWillyPed.AI = N1A_PED_AI_SETUP_MUSICIAN_TOOTH_KNOCKED_OUT_ANIMS	
					eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT					
					RESET_MISSION_PED_TIMERS()
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_KNOCK_OUT_TOOTH : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_COLLECT_TOOTH
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.	
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF DOES_PICKUP_EXIST(mpToothPickup.index)
							IF DOES_PICKUP_OBJECT_EXIST(mpToothPickup.index)
								VECTOR vTempWarpPos
								FLOAT fTempHeading
								vTempWarpPos = GET_PICKUP_COORDS(mpToothPickup.index)
								vTempWarpPos.z += 1.5	// add leeway on this Z, since GET_GROUND_Z_FOR_3D_COORD get's the lowest ground point below the value.
								IF GET_GROUND_Z_FOR_3D_COORD(vTempWarpPos, vTempWarpPos.z)
									vTempWarpPos.z += 0.25	// add a bit extra to help to the player falling through the floor
									//IF eMissionSkipTargetStage = MISSION_STAGE_LEAVE_THE_AREA	// if the target stage is the next stage load the scene otherwise don't bother
									//	WAIT_FOR_WORLD_TO_LOAD(vTempWarpPos, 25.0)
									//ENDIF
									//SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
									//SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_COLLECT_TOOTH : ", " player warp pos set to : ", vTempWarpPos, " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
									fTempHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
									SAFE_TELEPORT_PED(PLAYER_PED_ID(), vTempWarpPos, fTempHeading, FALSE, FALSE)
									SAFE_SET_ENTITY_HEADING_FACE_ENTITY(PLAYER_PED_ID(), sWillyPed.pedIndex)
									SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
									SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
									WAIT(0)	//used to stop player seeing stuff snap into position							
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_COLLECT_TOOTH : ", "aiting for ground z for warp pos", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					SAFE_REMOVE_PICKUP(mpToothPickup.index)
					IF NOT IS_REPLAY_BEING_SET_UP()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_LEAVE_THE_AREA	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_COLLECT_TOOTH : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					INT i
					FOR i = 0 TO MAX_NUM_CLUB_PEDS - 1
						SAFE_REMOVE_PED(sClubPed[i].pedIndex, TRUE)	// get rid of peds to help Willy pathing out club quickly
					ENDFOR
					IF sWillyPed.AI != N1A_PED_AI_STATE_FLEE
						SET_WILLY_AI_SETUP_FLEE_TASK()	// had to put in proc so skip can call it too	
						sWillyPed.AI = N1A_PED_AI_STATE_FLEE		// just flee straight away: sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_MUSICIAN_PED_AI ~~~ ", "N1A_PED_AI_SETUP_FLEE by skip stage") ENDIF #ENDIF
					ENDIF
					eMusicianState = N1A_MUSICIAN_STATE_FLEE	// just flee straight away: eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT				
					RESET_MISSION_PED_TIMERS()
					eSubStage = SS_CLEANUP	// jump straight to clean up from here, since the position i teleport the player at the tooth could be too far away to register after I've updated the x to ensure the player doesn't drop through the floor 
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_COLLECT_TOOTH : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_LEAVE_THE_AREA
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -462.2575, -156.7884, 37.0458 >>, 112.5754, FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_OUTRO_PHONECALL	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_LEAVE_THE_AREA : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF							
					RESET_MISSION_PED_TIMERS()
					WAIT(0)
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_LEAVE_THE_AREA : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_OUTRO_PHONECALL
				IF IS_PHONE_ONSCREEN(FALSE)
					HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
				ENDIF
				IF NOT bHasOutroPhonecallSucceeded
					eSubStage = SS_CLEANUP 						
				ELSE
					//needed to stop the outgoing phonecalls!!!
					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_PHONE_CONVERSATION()
						KILL_ANY_CONVERSATION()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_OUTRO_PHONECALL : ", "killing ongoing convo", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						WAIT(0)
					ENDWHILE					
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_OUTRO_PHONECALL : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			BREAK
			CASE MISSION_STAGE_LOSE_THE_COPS
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_LOSE_THE_COPS : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK	
			DEFAULT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
/// PARAMS:
///    eNewStage - stage to jump to
PROC JUMP_TO_STAGE(MISSION_STAGE eNewStage)	
	IF eMissionStage = eNewStage	// end skip stage
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
		ENDIF
		IF eMissionStage = MISSION_STAGE_LEAVE_THE_AREA	// additional setup which must come after END_REPLAY_SETUP()
			WAIT(500)	// B*1544280 - delay for Willy slow flee task start
			RESET_MISSION_PED_TIMERS()
			iTimer_PlayerReceivesScriptedWantedLevel = (GET_GAME_TIMER() - (NIG1A_TIME_DELAY_WANTED_FOR_WILLY_ASSAULT - 1000))	// B*1553964 - ensure player gets wanted rating straight away			
		ENDIF
		RC_END_Z_SKIP()		
	    bFinishedStageSkipping = TRUE
		bLoadedWorldForStageSkipping = FALSE
		// ensure we are fully faded in if we have skipped to the mission passed stage, since the mission passed GUI doesn't display if not (seems to need a frame wait too)
		IF eMissionStage = MISSION_STAGE_MISSION_PASSED
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
	ELSE
	    SKIP_STAGE() 
	ENDIF	
ENDPROC

/// PURPOSE: 
///     Reset the mission, cleanups the current state and set's the mission up again
///     USED by the mission replay checkpoint setup and Debug skips
PROC RESET_MISSION()
	MISSION_CLEANUP(TRUE, FALSE)		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		IF NOT IS_REPLAY_BEING_SET_UP()
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//put player near start location facing towards club
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
		ENDIF
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	/*//set the initial scene back up
	eInitialSceneStage = IS_REQUEST_SCENE
	WHILE NOT SetupScene_NIGEL_1A(sRCLauncherDataLocal)	
		CPRINTLN(DEBUG_MISSION, " RESET_MISSION - waiting on SetupScene_NIGEL_1A")
		WAIT(0)
	ENDWHILE
	RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	SETUP_AREA_FOR_MISSION(RC_NIGEL_1A, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
	*/
	//re do mission initialization
	INIT_MISSION()	
	SET_STAGE(MISSION_STAGE_ENTER_THE_MUSIC_CLUB)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Perform a Z skip.  Used by the mission checkpoints and the debug Z skip function
/// PARAMS:
///    iNewStage - Mission stage we want to skip to
///    bResetMission - used when we go backwards in mission flow.  If false we also don't stop the active cutscene in RC_START_Z_SKIP, instead handled in SKIP_STAGE to fix bug 1006740
PROC DO_Z_SKIP(INT iNewStage, BOOL bResetMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP with parameters  - iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	RC_START_Z_SKIP(bResetMission)
	IF bResetMission
		RESET_MISSION()
	ENDIF
	eMissionSkipTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
	bFinishedStageSkipping = FALSE
	IF IS_REPLAY_BEING_SET_UP()
		bLoadedWorldForStageSkipping = TRUE
	ELSE
		bLoadedWorldForStageSkipping = FALSE
	ENDIF
	// load world for the mission start area if we are resetting the mission to the intro mocap.  Moved here from script skip stage to fix bug 1006740 - mocap exit states not getting set as game is waiting on world to load before getting to check				
	// basically if you press CROSS to confirm which stage in the z menu, it skipped the mocap but couldn't sent exit states as it was waiting for world to load first.					
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF eMissionSkipTargetStage = MISSION_STAGE_ENTER_THE_MUSIC_CLUB
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//put player near start location facing towards club
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				bLoadedWorldForStageSkipping = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP -  LOADED WORLD ready at mission start area framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP : iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	JUMP_TO_STAGE(eMissionSkipTargetStage)
ENDPROC
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//	DEBUG - J,P and Z skip stuff
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for S, F, J and P skips
	PROC DEBUG_Check_Debug_Keys()
	    INT iNewStage
	    // Check for Pass
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
	    	WAIT_FOR_CUTSCENE_TO_STOP()	
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				IF IS_PED_IN_MELEE_COMBAT(sWillyPed.pedIndex)	// clear ped tasks doesn't work on a ped in combat :(
					CLEAR_PED_TASKS_IMMEDIATELY(sWillyPed.pedIndex)
				ELSE
					CLEAR_PED_TASKS(sWillyPed.pedIndex)
				ENDIF
				SET_PED_KEEP_TASK(sWillyPed.pedIndex, FALSE)
				sWillyPed.AI = N1A_PED_AI_SETUP_RELAXED
			ENDIF
			// respot player outside club so doors can get locked
			IF ePlayerInsideClubStatus != N1A_RCR_INVALID
			AND ePlayerInsideClubStatus != N1A_MAX_NUM_ROCK_CLUB_ROOMS
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-571.1343, 271.7024, 81.9336>>, 132.6751)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				CPRINTLN(DEBUG_MISSION, "SKIP - player in club interior, warp outside setup")
			ENDIF
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "S SKIP") ENDIF #ENDIF
	        Script_Passed()
	    ENDIF
	    // Check for Fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
	    	WAIT_FOR_CUTSCENE_TO_STOP()			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "F SKIP") ENDIF #ENDIF
		    SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
	    ENDIF	    
	    // Check for Skip forward
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))		
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_OUTRO_PHONECALL)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage) + 1
				BREAK
			ENDSWITCH
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "J SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
			DO_Z_SKIP(iNewStage, FALSE)	//perform a Z skip to the next stage, without the mission reset
	    ENDIF	    
	    // Check for Skip backwards
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_LEAVE_THE_AREA)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage)-1
				BREAK
			ENDSWITCH		
		    IF iNewStage > -1
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "P SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ENDIF
	    ENDIF	    
	    // Z skip menu
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage)
			// if we are skipping forward in the mission stages, just J skip rather than a full mission reset
			IF (eMissionStage = MISSION_STAGE_LOSE_THE_COPS)	// additional stages dealth seperately since they sit at the end of the MISSION_STAGE enum
				IF iNewStage = ENUM_TO_INT(MISSION_STAGE_OUTRO_PHONECALL)	// trying to z skip past lose cops so don't reset mission
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, FALSE)
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, TRUE)
				ENDIF
			ELIF (iNewStage <= ENUM_TO_INT(eMissionStage))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, FALSE)
			ENDIF
	    ENDIF
	ENDPROC
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:MISSION STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------	

/// PURPOSE:
///    Trevor has to get into the music club
PROC ENTER_THE_MUSIC_CLUB()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())	
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP	
			IF IS_PED_UNINJURED(sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, 4, sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_01].pedIndex, "Nigel1AGroupie01")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", 4, "  Voice ID = Nigel1AGroupie01 ", "ped ID = ", CLUB_PED_GROUPIE_UPSTAIRS_01) ENDIF #ENDIF
			ENDIF	
			IF IS_PED_UNINJURED(sClubPed[CLUB_PED_BAND_MANAGER].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, 5, sClubPed[CLUB_PED_BAND_MANAGER].pedIndex, "Nigel1AManager")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", 5, "  Voice ID = Nigel1AManager ", "ped ID = ", CLUB_PED_BAND_MANAGER) ENDIF #ENDIF
			ENDIF			
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, 8, sWillyPed.pedIndex, "Willy")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", 8, "  Voice ID = Willy ") ENDIF #ENDIF
			ENDIF
			IF IS_PED_UNINJURED(sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, ConvertSingleCharacter("A"), sClubPed[CLUB_PED_GROUPIE_UPSTAIRS_02].pedIndex, "Nigel1AGroupie02")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", "A", "  Voice ID = Nigel1AGroupie02 ", "ped ID = ", CLUB_PED_GROUPIE_UPSTAIRS_02) ENDIF #ENDIF
			ENDIF			
			IF IS_PED_UNINJURED(sClubPed[CLUB_PED_ROADIE].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, ConvertSingleCharacter("B"), sClubPed[CLUB_PED_ROADIE].pedIndex, "Nigel1ARoadie")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", "B", "  Voice ID = Nigel1ARoadie ", "ped ID = ", CLUB_PED_ROADIE) ENDIF #ENDIF
			ENDIF
			IF IS_PED_UNINJURED(sClubPed[GROUPIE_WILLYS_PIECE].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, ConvertSingleCharacter("C"), sClubPed[GROUPIE_WILLYS_PIECE].pedIndex, "Nigel1AGroupie03")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ADD_PED_FOR_DIALOGUE : ", "speakerId = ", "C", "  Voice ID = Nigel1AGroupie03 ", "ped ID = ", GROUPIE_WILLYS_PIECE) ENDIF #ENDIF
			ENDIF
			IF bFinishedStageSkipping
				SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, NEEDED for repeat play purposes.
			ENDIF
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "ENTER_THE_MUSIC_CLUB - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			MANAGE_WILLY()		
			MANAGE_CLUB_PEDS()
			IF ePlayerInsideClubStatus <> N1A_RCR_INVALID //IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(PLAYER_PED_ID())
			AND ePlayerInsideClubStatus <> N1A_MAX_NUM_ROCK_CLUB_ROOMS
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ENTER_THE_MUSIC_CLUB - player inside club - eSubStage = SS_CLEANUP") ENDIF #ENDIF
				eSubStage = SS_CLEANUP
			ELSE
				IF NOT bDoneObjective_EnterClub					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_LOW)
					PRINT_NOW("NIG1A_OBJ_01", DEFAULT_GOD_TEXT_TIME, 0)	// Enter the ~y~club~s~ and find Willy.~s~
					IF NOT DOES_BLIP_EXIST(blipMusicClub)
						blipMusicClub = CREATE_COORD_BLIP(vMusicClub_CentrePos)
					ENDIF
					bDoneObjective_EnterClub = TRUE
				ENDIF
			ENDIF			
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			MANAGE_WILLY()			
			MANAGE_CLUB_PEDS()
			SAFE_REMOVE_BLIP(blipMusicClub)
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_01")	
				CLEAR_THIS_PRINT("NIG1A_OBJ_01")	// Enter the ~y~club~s~ and find Willy.~s~
			ENDIF
			SET_STAGE(MISSION_STAGE_FIND_CELEB)
			CPRINTLN(DEBUG_MISSION, "ENTER_THE_MUSIC_CLUB - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Trevor has to go to the parking lot, around the back of the rock club
PROC STAGE_FIND_CELEB()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())	
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
	MANAGE_WILLY()
	MANAGE_CLUB_PEDS()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP			
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			IF ePlayerInsideClubStatus != N1A_RCR_INVALID	// IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(PLAYER_PED_ID(), FALSE)
			AND ePlayerInsideClubStatus != N1A_MAX_NUM_ROCK_CLUB_ROOMS
				SAFE_REMOVE_BLIP(blipMusicClub)
			ELSE
				IF NOT DOES_BLIP_EXIST(blipMusicClub)	// re-blip the club if the player isn't inside it.
					blipMusicClub = CREATE_COORD_BLIP(vMusicClub_CentrePos)
				ENDIF
			ENDIF
			HANDLE_PLAYER_HEADTRACKING_WILLY_PRE_BRAWL()
			// Trevor learns of Willy' location from the rest of the band
			IF NOT bDoneDialogue_BandSayWhereWillyIs
				IF NOT bHasPlayerScarredClubPedsUpstairs
					IF ePlayerInsideClubStatus = N1A_RCR_ROOM_UPSTAIRS
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_AND_STORE_CURRENT_AMBIENT_DIALOGUE_UPSTAIRS(FALSE)							
						ENDIF			
						IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_B1", CONV_PRIORITY_MEDIUM)
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_LOW)
							// Okay, which one of you cupcakes is Willy?
							// Hey, who the fuck are you?  
							// His dentist.
							// Willy's still downstairs chatting up some bird, man.
							// His dentist comes to him, that is so cool.  
							SET_CLUB_PEDS_UPSTAIRS_SYNCED_ANIM(N1A_CPAT_IDLE_B_ANIM)
							bDoneDialogue_BandSayWhereWillyIs = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bHasPlayerScarredClubPedsUpstairs
					IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_B1")	//bDoneDialogue_BandSayWhereWillyIs
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - player scarred them whilst they were saying where Willy is.  So cancelled blipping him") ENDIF #ENDIF
					ENDIF
				ELSE
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_B1")	//bDoneDialogue_BandSayWhereWillyIs
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - eSubStage = SS_CLEANUP - peds upstairs told Trevor where Willy is") ENDIF #ENDIF
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1A_ENTOURAGE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1A_ENTOURAGE)") ENDIF #ENDIF		
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF
			// if Trevor has spotted Willy
			IF bDoneDialogue_MusicianSpottedPlayer
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sNigel1A_DialogueRoot_MusicianSpotsTrevor)	//bDoneDialogue_BandSayWhereWillyIs
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - eSubStage = SS_CLEANUP - Trevor spoke to Willy") ENDIF #ENDIF	
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
			// sneaking in back door dialogue
			IF NOT bDoneDialogue_TrevorCommentSneakingInBackdoor
				IF ePlayerInsideClubStatus = N1A_RCR_REAR_ENTRANCE_TOP_STAIRS
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
						IF GET_UPDATED_PED_MOVEMENT_STATE(PLAYER_PED_ID()) != N1A_PEDMOVEMENTSTATE_STEALTH
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_TBE1", CONV_PRIORITY_MEDIUM) //)
								// Tradesmen's entrance, don't mind if I do.  
								bDoneDialogue_TrevorCommentSneakingInBackdoor = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneDialogue_TrevorCommentSneakingInBackdoor - TRUE", "***") ENDIF #ENDIF
							ENDIF
						ELSE
							bDoneDialogue_TrevorCommentSneakingInBackdoor = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneDialogue_TrevorCommentSneakingInBackdoor - TRUE skipped over, player in stealth", "***") ENDIF #ENDIF
						ENDIF
					ENDIF
				ELSE
					bDoneDialogue_TrevorCommentSneakingInBackdoor = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneDialogue_TrevorCommentSneakingInBackdoor - TRUE skipped over, player in wrong area", "***") ENDIF #ENDIF
				ENDIF
			ENDIF
			// has Willy kicked off with the player
			IF eMusicianState != N1A_MUSICIAN_STATE_RELAXED	// bug fix B*1060738
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - eSubStage = SS_CLEANUP - Willy not in relaxed state") ENDIF #ENDIF
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_01")	
				CLEAR_THIS_PRINT("NIG1A_OBJ_01")	// Enter the ~y~club~s~ and find Willy.~s~
			ENDIF
			bDoneDialogue_TrevorCommentSneakingInBackdoor = TRUE // Set this dialogue to true as we don't want it playing out after this point
			SAFE_REMOVE_BLIP(blipMusicClub)
			SET_STAGE(MISSION_STAGE_KNOCK_OUT_TOOTH)
			CPRINTLN(DEBUG_MISSION, "STAGE_FIND_CELEB - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC	

/// PURPOSE:
///    Trevor is told to Beat up Willy
PROC STAGE_KNOCK_OUT_TOOTH()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())	
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
	MANAGE_WILLY()	
	MANAGE_CLUB_PEDS()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP				
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				IF NOT DOES_BLIP_EXIST(sWillyPed.blipIndex)
					sWillyPed.blipIndex = CREATE_PED_BLIP(sWillyPed.pedIndex, TRUE, FALSE)
				ENDIF				
			ENDIF
			HAVE_ASSETS_LOADED_FOR_COLLECT_TOOTH_STAGE()	// start requesting these ready
			iTimer_BackupKnockOutToothTrigger = GET_GAME_TIMER()
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE		
			//knock out the tooth when the musician state reaches 
			IF eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_LOW)
				eSubStage = SS_CLEANUP
				CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - SS_UPDATE - Willy i knocked down state")
			ELSE
				IF NOT bDoneObjective_BeatUpMusician
					IF bDoneDialogue_MusicianSpottedPlayer
					AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sNigel1A_DialogueRoot_MusicianSpotsTrevor)
					OR eMusicianState != N1A_MUSICIAN_STATE_RELAXED	// bug fix B*1060738
						// B*1386580 - Beat up Willy objective overridden by dialogue. (delay if a convo is being built)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_NOW("NIG1A_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)	// Beat up ~r~Willy.~s~		
							bDoneObjective_BeatUpMusician = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - objective displayed (no convo ongoing or queued), frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELSE
							IF IS_SCRIPTED_CONVERSATION_ONGOING()
								IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MSP4")	// Right, I'll take you on!
								AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MSU2")	// Oh no don't hurt me, don't hurt me!  What do you want?									
									PRINT_NOW("NIG1A_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1)	// Beat up ~r~Willy.~s~		
									bDoneObjective_BeatUpMusician = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - objective displayed (interupt ongoing convo) frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - objective DELAYED for building / queues convo frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				HANDLE_PLAYER_HEADTRACKING_WILLY_PRE_BRAWL()
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sWillyPed.pedIndex)
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			SAFE_REMOVE_BLIP(sWillyPed.blipIndex)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_02")	
				CLEAR_THIS_PRINT("NIG1A_OBJ_02")	// Beat up ~r~Willy.~s~
			ENDIF
			// B*1563437 - don't lower his health until it doesn't matter if he dies
			IF IS_PED_UNINJURED(sWillyPed.pedIndex)
				IF (GET_ENTITY_HEALTH(sWillyPed.pedIndex) >= 137.5)	// fatigued threshold used to be 135
					SET_ENTITY_HEALTH(sWillyPed.pedIndex, 135)
					CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - SS_CLEANUP : lower Willy health 135 for flee +-+-")
				ENDIF
			ENDIF
			SET_STAGE(MISSION_STAGE_COLLECT_TOOTH)
			CPRINTLN(DEBUG_MISSION, "STAGE_KNOCK_OUT_TOOTH - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC	

/// PURPOSE:
///    Willy's tooth falls out and the player must collect it
PROC STAGE_COLLECT_TOOTH()

	VECTOR vTempSoundPos
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())	
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	// if the player hangs around the club or is pursuing Willy, give player a wanted level after period of time
	MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
	MANAGE_WILLY()	
	MANAGE_CLUB_PEDS()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			IF HAVE_ASSETS_LOADED_FOR_COLLECT_TOOTH_STAGE()				
				IF NOT DOES_PICKUP_EXIST(mpToothPickup.index)
					IF GET_TOOTH_PICKUP_COORDS(mpToothPickup.vPosition)					
						mpToothPickup.index = CREATE_PICKUP(mpToothPickup.type, mpToothPickup.vPosition, mpToothPickup.iPlacementFlags, -1, TRUE, mpToothPickup.mnModel)
						CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - tooth pickup created")
						IF DOES_PICKUP_EXIST(mpToothPickup.index)
							IF DOES_PICKUP_OBJECT_EXIST(mpToothPickup.index)						
										
								//tooth sound sfx
								IF REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1A_TOOTH")								
									vTempSoundPos = GET_PICKUP_COORDS(mpToothPickup.index)
									vTempSoundPos.z += 0.35	// make this a little higher to ensure the pos is above ground level
									PLAY_SOUND_FROM_COORD(-1, "TOOTH_PING", vTempSoundPos, "NIGEL_1A_SOUNDSET")
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "sfx for knocked out tooth played ~~~~~~~") ENDIF #ENDIF
								ELSE
									CPRINTLN(DEBUG_MISSION, "sfx for knocked out tooth NOT loaded in time ~~~~~~~")
								ENDIF			
								//setup tooth pickup blip
								IF NOT DOES_BLIP_EXIST(mpToothPickup.blipIndex)
									mpToothPickup.blipIndex = CREATE_BLIP_FOR_PICKUP(mpToothPickup.index)
								ENDIF
								IF DOES_BLIP_EXIST(mpToothPickup.blipIndex)
									//SET_BLIP_COLOUR(mpToothPickup.blipIndex, BLIP_COLOUR_GREEN)
									SET_BLIP_NAME_FROM_TEXT_FILE(mpToothPickup.blipIndex, "NIG1A_TP_BLIP")	//Tooth
								ENDIF
							ELSE
								CPRINTLN(DEBUG_MISSION, " : STAGE_COLLECT_TOOTH : DOES_PICKUP_OBJECT_EXIST return false after CREATE_PICKUP!")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_MISSION, " : STAGE_COLLECT_TOOTH : DOES_PICKUP_OBJECT_EXIST return false after CREATE_PICKUP!")
						ENDIF	
						SET_MODEL_AS_NO_LONGER_NEEDED(mpToothPickup.mnModel)
						IF NOT bShouldPlayerGetWantedLevelAfterAttackingWilly
							// check the scripted wanted level timer hasn't already been set for other reasons
							IF iTimer_PlayerReceivesScriptedWantedLevel = 0
								iTimer_PlayerReceivesScriptedWantedLevel = GET_GAME_TIMER()	// used to give the player a scripted wanted level if is using a weapon in the club								
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - done -  setup wanted level delay for staying in club") ENDIF #ENDIF
							ENDIF
							bShouldPlayerGetWantedLevelAfterAttackingWilly = TRUE
						ENDIF
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 0, REPLAY_IMPORTANCE_LOWEST)	// B*1841676 - record knockout blow to Willy
						eSubStage = SS_UPDATE
						CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - SS_SETUP done")
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			IF NOT bDoneDialogue_MusicianLosesTooth
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MCS", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
						// Aagh, my tooth. You bastard!
						bDoneDialogue_MusicianLosesTooth = TRUE
					ENDIF
				ELSE
					bDoneDialogue_MusicianLosesTooth = TRUE	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - skipped over bDoneDialogue_MusicianLosesTooth as Willy is Dead") ENDIF #ENDIF
				ENDIF
			ELIF NOT bDoneObjective_CollectTooth
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MCS")	// bDoneDialogue_MusicianLosesTooth				
					PRINT_NOW("NIG1A_OBJ_03", DEFAULT_GOD_TEXT_TIME, 1)	// Collect Willy's ~g~gold tooth.~s~
					bDoneObjective_CollectTooth = TRUE
				ENDIF			
			ELIF NOT bDoneDialogue_MusicianBeginFlee	
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
					IF sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MCS")	// bDoneDialogue_MusicianLosesTooth
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_ML", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()) // no subtitles for bug fix 1042232
								// You are out of your fucking mind!
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
								IF eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT
									sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
									eMusicianState = N1A_MUSICIAN_STATE_FLEE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH : ", "N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT - eMusicianState = N1A_MUSICIAN_STATE_FLEE") ENDIF #ENDIF
								ENDIF
								bDoneDialogue_MusicianBeginFlee = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bDoneDialogue_MusicianBeginFlee = TRUE	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - skipped over bDoneDialogue_MusicianBeginFlee as Willy is Dead") ENDIF #ENDIF
				ENDIF
			ENDIF
			//Check for player collecting the tooth
			IF HAS_PICKUP_BEEN_COLLECTED(mpToothPickup.index)
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_LOW)
				//SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 250)
				//vTempSoundPos = GET_ENTITY_COORDS(mpToothPickup.index)
				//vTempSoundPos.z += 0.35	// make this a little higher to ensure the pos is above ground level
				//PLAY_SOUND_FROM_COORD(-1, "PICK_UP_DEFAULT", vTempSoundPos, "HUD_FRONTEND_CUSTOM_SOUNDSET")
				
				/*// B*1533138 - tooth pickup anim
				IF IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
				AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				AND NOT IS_PED_IN_COMBAT(PLAYER_PED_ID())
				AND NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
				AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "pickup_object", "pickup_low", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT | AF_ABORT_ON_WEAPON_DAMAGE | AF_EXIT_AFTER_INTERRUPTED)
				ELSE
					CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - SS_UPDATE - anim skipped")
				ENDIF*/
				
				CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - SS_UPDATE - tooth collected ~~~~*****+++++----$$$~~~")
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_03")	
				CLEAR_THIS_PRINT("NIG1A_OBJ_03")	// Collect Willy's ~g~gold tooth.~s~
			ENDIF
			bDoneObjective_CollectTooth = TRUE
			SAFE_REMOVE_BLIP(mpToothPickup.blipIndex)
			SET_MODEL_AS_NO_LONGER_NEEDED(mpToothPickup.mnModel)
			//REMOVE_ANIM_DICT("pickup_object")	// B*1533138 - tooth pickup anim
			SAFE_REMOVE_PICKUP(mpToothPickup.index)			
			SET_STAGE(MISSION_STAGE_LEAVE_THE_AREA)
			CPRINTLN(DEBUG_MISSION, "STAGE_COLLECT_TOOTH - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC	

/// PURPOSE:
///    Player must leave the area
PROC STAGE_LEAVE_THE_AREA()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	// if the player hangs around the club or is pursuing Willy, give player a wanted level after period of time
	MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
	MANAGE_WILLY()		
	MANAGE_CLUB_PEDS()
	INT i = 0
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_NIGEL1A_LEAVE_THE_AREA, "Leave the area.", TRUE) // 2nd Mission replay checkpoint
			eSubStage = SS_UPDATE	
			CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE				
			IF NOT bDoneDialogue_MusicianLosesTooth
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_MCS", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
						//Fuck, I've lost a tooth.  You bastard.
						bDoneDialogue_MusicianLosesTooth = TRUE
					ENDIF	
				ELSE
					bDoneDialogue_MusicianLosesTooth = TRUE	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - skipped over bDoneDialogue_MusicianLosesTooth as Willy is Dead") ENDIF #ENDIF
				ENDIF
			ELIF NOT bDoneDialogue_MusicianBeginFlee
				IF IS_PED_UNINJURED(sWillyPed.pedIndex)
					IF sWillyPed.AI = N1A_PED_AI_STATE_MUSICIAN_FINISHED_TOOTH_OUT_ANIMS
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_MCS")	// bDoneDialogue_MusicianLosesTooth
							IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_ML", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())	// no subtitles for bug fix 1042232
								// You are out of your fucking mind!
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE)	// B*1516072 - allow convo during ragdoll
								IF eMusicianState = N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT
									sWillyPed.AI = N1A_PED_AI_SETUP_FLEE
									eMusicianState = N1A_MUSICIAN_STATE_FLEE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA : ", "N1A_MUSICIAN_STATE_TOOTH_KNOCKED_OUT - eMusicianState = N1A_MUSICIAN_STATE_FLEE") ENDIF #ENDIF
								ENDIF
								bDoneDialogue_MusicianBeginFlee = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bDoneDialogue_MusicianBeginFlee = TRUE	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - skipped over bDoneDialogue_MusicianBeginFlee as Willy is Dead") ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMusicClub_CentrePos, NIG1A_PLAYER_LEFT_THE_CLUB_DIST)
					IF NOT bDoneObjective_LeaveTheArea
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_NOW("NIG1A_OBJ_04", 10000, 0)	// Leave the area.~s~	B*1583984 increase display time from DEFAULT_GOD_TEXT_TIME (7500)
							bDoneObjective_LeaveTheArea = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - objective displayed (no convo ongoing or queued), frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELSE
							IF IS_SCRIPTED_CONVERSATION_ONGOING()
								// B*1772114 - this convo can stomp on the objective so wait for it to finish if playing
								IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1A_ML")	// bDoneDialogue_MusicianBeginFlee
									PRINT_NOW("NIG1A_OBJ_04", 10000, 0)	// Leave the area.~s~	B*1583984 increase display time from DEFAULT_GOD_TEXT_TIME (7500)
									bDoneObjective_LeaveTheArea = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - objective displayed (interupt ongoing convo) frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - objective DELAYED for ongoing convo NIG1A_ML frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - objective DELAYED for building / queues convo frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF						
					ELSE
						IF NOT bDoneDialogue_TrevorLeaveTheAreaComment
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()									
								IF NOT IS_ENTITY_INSIDE_MUSIC_CLUB_INTERIOR(PLAYER_PED_ID(), FALSE)
									IF NIG1A_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sNigel1A_Dialogue_TextBlockName, "NIG1A_TLA", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()) //)
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
										// I'd say my work is done here.  
										bDoneDialogue_TrevorLeaveTheAreaComment = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TREVOR LEAVE THE AREA DIALOGUE", "***") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	// wait for current dialogue to finish first
						CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - player left the area")
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF						
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_04")
				CLEAR_THIS_PRINT("NIG1A_OBJ_04") // Leave the area.~s~
			ENDIF
			FOR i = 0 TO (MAX_NUM_CLUB_PEDS - 1)
				SAFE_REMOVE_PED(sClubPed[i].pedIndex)
			ENDFOR	
			SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)	// B*1516072 - allow convo during ragdoll
			//Player has to lose the cops if he has a wanted level
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
			ELSE
				SET_STAGE(MISSION_STAGE_OUTRO_PHONECALL)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "STAGE_LEAVE_THE_AREA - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Additional stage which monitors the player losing his wanted level
PROC STAGE_LOSE_THE_COPS()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	MANAGE_WILLY()		
	MANAGE_CLUB_PEDS()
	
	SWITCH eSubStage		
		//	------------------------------------------
		CASE SS_SETUP
			MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
			bDoneObjective_LoseWantedLevel = FALSE	// lose cops objective should display everytime
			eSubStage = SS_UPDATE			
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE			
			// if the player hangs around the club or is pursuing Willy, give player a wanted level after period of time
			MONITOR_PLAYER_SCRIPTED_WANTED_LEVEL()
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_05")
					CLEAR_THIS_PRINT("NIG1A_OBJ_05")	//Lose the cops.
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	// wait for current dialogue to finish first
					eSubStage = SS_CLEANUP
				ENDIF
			ELSE
				IF NOT bDoneObjective_LoseWantedLevel						
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_05")
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "lose cops objective displayed ^^^^^^ +++++ ^^^^^") ENDIF #ENDIF
						PRINT_NOW("NIG1A_OBJ_05", DEFAULT_GOD_TEXT_TIME, 0)	// Lose the cops.
						bDoneObjective_LoseWantedLevel = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1A_OBJ_05")
				CLEAR_THIS_PRINT("NIG1A_OBJ_05")	//Lose the cops.
			ENDIF
			SET_STAGE(MISSION_STAGE_OUTRO_PHONECALL)
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_CLEANUP done")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Trveor calls Nigel, to inform him of the successful mission
PROC STAGE_OUTRO_PHONECALL()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	MANAGE_WILLY()	
	MANAGE_CLUB_PEDS()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_PHONE_CONVERSATION()
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			ADD_PED_FOR_DIALOGUE(sDialogue, 3, NULL, "NIGEL")
			//ADD_PED_FOR_DIALOGUE(sDialogue, 4, NULL, "MRSTHORNHILL")			
			iTimer_DelayForOutroPhonecall = GET_GAME_TIMER()			
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_SETUP done")	
		BREAK
		//	------------------------------------------
		CASE SS_UPDATE	
			IF NOT bHasOutroPhonecallSucceeded
				// Delay the phonecall for polish purposes
				IF HAS_TIME_PASSED(iTimer_DelayForOutroPhonecall, TIME_DELAY_FOR_OUTRO_PHONECALL)
					IF PLAYER_CALL_CHAR_CELLPHONE(sDialogue, CHAR_NIGEL, sNigel1A_Dialogue_TextBlockName, "NIG1A_CP2", CONV_PRIORITY_CELLPHONE)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_LOW) 
						// Guess what, you crazy English fruitcake.
						// Oh, I'm dreadful at guessing, aren't I Mrs Thornhill?  
						// No, she can't hear me, she's washing out a nappy from Samantha Muldoon's dustbin.    
						// One gold tooth, courtesy of Love Fist, complete with complimentary DNA.
						// Marvelous, marvelous! Jock, I knew you wouldn't let us down!  
						// Too-da-loo.  
						bHasOutroPhonecallSucceeded = TRUE
					ENDIF
				ELSE
					// player gets wanted before phonecall triggers
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	// player gets wanted rating		
						//Player already wanted so go to lose the cops stage
						CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_UPDATE player has a wanted level revert back to MISSION_STAGE_LOSE_THE_COPS")
						SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_CELLPHONE_CALL_FINISHED()
						IF HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()         // If the cellphone was in use and the player receives an injury or starts ragdolling then,
							bHasOutroPhonecallSucceeded = FALSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - cellphone convo was interrupted so bHasOutroPhonecallSucceeded * set FALSE") ENDIF #ENDIF
						ELSE
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_PHONE_CONVERSATION()
			ENDIF
			KILL_ANY_CONVERSATION()
			SET_STAGE(MISSION_STAGE_MISSION_PASSED)
			CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_CLEANUP done")
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Mission has already failed at this point, but this handles
///    having to update the fail reason if the player does something more
///    serious during the fade.
PROC MISSION_FAILED_WAIT_FOR_FADE()

	STRING sFailReason = NULL
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ePlayerInsideClubStatus = GET_PED_INSIDE_MUSIC_CLUB_STATUS(PLAYER_PED_ID(), FALSE)
	UPDATE_AUDIO_SCENE(ePlayerInsideClubStatus)
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF			
			DELETE_ALL_MISSION_BLIPS()			
			SWITCH eN1A_MissionFailedReason	//Set the fail reason string
				CASE FAILED_MUSICIAN_KILLED
					sFailReason = "NIG1A_F_01"		// ~r~Willy died.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: MUSICIAN KILLED") ENDIF #ENDIF
				BREAK
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Generic") ENDIF #ENDIF
				BREAK		
			ENDSWITCH	
			//Check if fail reason needs to be displayed
			IF eN1A_MissionFailedReason = FAILED_DEFAULT
				RANDOM_CHARACTER_FAILED()
			ELSE
				RANDOM_CHARACTER_FAILED_WITH_REASON(sFailReason)
			ENDIF	
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP done")
		BREAK
		//	------------------------------------------
		CASE SS_UPDATE		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				// get the player out of the club
				IF ePlayerInsideClubStatus != N1A_RCR_INVALID
				AND ePlayerInsideClubStatus != N1A_MAX_NUM_ROCK_CLUB_ROOMS
					CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - player in club interior, warp setup")
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-571.1343, 271.7024, 81.9336>>, 132.6751)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-575.5230, 269.2646, 81.6734>>, 83.1558)
				ENDIF
				CLEANUP_ALL_MISSION_ENTITIES(TRUE)						
				CLEAR_AREA(vMusicClub_CentrePos, 45.0, TRUE)			
				CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - SS_CLEANUP done")
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK		
	ENDSWITCH
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	PROC DEBUG_SETUP()
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		INT i
		SWITCH eSubStage
			CASE SS_SETUP
				bDebug_PrintMissionInfoToTTY				= TRUE	// turn on mission tty
				eSubStage = SS_UPDATE
			BREAK 
			CASE SS_UPDATE
				i = 1
				IF IS_ENTITY_ALIVE(sClubPed[i].pedIndex)
					IF CAN_PED_SEE_PED(sClubPed[i].pedIndex, PLAYER_PED_ID(), 170)
						sClubPed[i].iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLUB_PEDS - CAN_PED_SEE_PED returned TRUE for ped ID = ", i, "  at frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				UPDATE_STANDARD_CLUB_PED_AI(i)
			BREAK
			CASE SS_CLEANUP
			BREAK
		ENDSWITCH
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)	
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)	
	// take ownership from the initial scene (needs to happen before launcher terminates) must check is running, could get here from replay and not be setup
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherDataLocal.iSyncSceneIndex)
		TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.iSyncSceneIndex)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	/*IF IS_REPLAY_IN_PROGRESS() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
	  	//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_1A(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " IS_REPLAY_IN_PROGRESS - waiting on SetupScene_NIGEL_1A")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_NIGEL_1A, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
		g_bSceneAutoTrigger = FALSE
	ENDIF*/
	
	INIT_MISSION()
				
	IF IS_REPLAY_IN_PROGRESS()			// handle replay checkpoints
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF
		
		SWITCH iReplayStage	
			CASE CP_NIGEL1A_ENTER_THE_MUSIC_CLUB
				START_REPLAY_SETUP(vPos_PlayerMissionStart, fHeading_PlayerMissionStart)
				CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, << -590.5827, 250.1880, 81.2377 >>, 266.6357, FALSE, FALSE, FALSE, FALSE, FALSE)
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "locate musician in progress")
				DO_Z_SKIP(Z_SKIP_ENTER_THE_MUSIC_CLUB)				
			BREAK
			CASE CP_NIGEL1A_LEAVE_THE_AREA
				START_REPLAY_SETUP(<<-554.1479, 287.9976, 81.1763>>, 152.3380)
				CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, << -590.5827, 250.1880, 81.2377 >>, 266.6357, FALSE, FALSE, FALSE, FALSE, FALSE)	// set this to the parking lot once possible
				bDoneDialogue_MusicianLosesTooth = TRUE	//set this dialogue to have already played, because they want the skip to be at the objective instead
				bDoneDialogue_MusicianBeginFlee = TRUE // skip this just flee
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "leave the area in progress")
				DO_Z_SKIP(Z_SKIP_LEAVE_THE_AREA)
			BREAK
			CASE CP_NIGEL1A_MISSION_PASSED
				START_REPLAY_SETUP(<<-462.2627, -156.7760, 37.0557>>, 112.4431)
				DO_Z_SKIP(Z_SKIP_MISSION_PASSED)	 
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "mission passed in progress")
			BREAK
			DEFAULT
				SCRIPT_ASSERT("RC - Nigel 1A - replay checkpoint - invalid")
			BREAK
		ENDSWITCH
	ELIF IS_REPEAT_PLAY_ACTIVE()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//setup player in spot which matches start of the mission
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			CPRINTLN(DEBUG_MISSION, "IS_REPEAT_PLAY_ACTIVE -  set player coords for repeat play ", vPos_PlayerMissionStart, " framecount : ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	
	WHILE(TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_VSW")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)		
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		
			MISSION_FAILED_CHECKS()	
			MANAGE_SET_CLUB_DOORS_INITIAL_STATES()
			
			// Fix B*1121745 - swap over to call everyframe commands
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			SWITCH eMissionStage
				CASE MISSION_STAGE_ENTER_THE_MUSIC_CLUB
					ENTER_THE_MUSIC_CLUB()
				BREAK
				CASE MISSION_STAGE_FIND_CELEB
					STAGE_FIND_CELEB()
				BREAK				
				CASE MISSION_STAGE_KNOCK_OUT_TOOTH
					STAGE_KNOCK_OUT_TOOTH()
				BREAK				
				CASE MISSION_STAGE_COLLECT_TOOTH
					STAGE_COLLECT_TOOTH()
				BREAK				
				CASE MISSION_STAGE_LEAVE_THE_AREA
					STAGE_LEAVE_THE_AREA()
				BREAK				
				CASE MISSION_STAGE_LOSE_THE_COPS
					STAGE_LOSE_THE_COPS()
				BREAK				
				CASE MISSION_STAGE_OUTRO_PHONECALL
					STAGE_OUTRO_PHONECALL()
				BREAK
				CASE MISSION_STAGE_MISSION_PASSED
					Script_Passed()
				BREAK
				CASE MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
					MISSION_FAILED_WAIT_FOR_FADE()
				BREAK					
					CASE MISSION_STAGE_DEBUG_SETUP		// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_SETUP()
						#ENDIF
					BREAK				
			ENDSWITCH			
		ENDIF
		
		// if we are skipping through the mission stages, for a checkpoint / debug skip		
		IF bFinishedStageSkipping = FALSE		
			JUMP_TO_STAGE(eMissionSkipTargetStage)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_MISSION_WIDGETS()	
		  	IF bFinishedStageSkipping = TRUE		// not skipping stages, check for debug keys as long as we aren't in fail state				
				IF eMissionStage <> MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
		        	DEBUG_Check_Debug_Keys()
				ENDIF
			ENDIF
        #ENDIF	
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
