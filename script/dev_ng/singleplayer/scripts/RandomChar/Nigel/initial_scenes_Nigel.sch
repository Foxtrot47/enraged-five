USING "RC_Helper_Functions.sch"
USING "RC_Launcher_public.sch"

// Initial scene setup
ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

// Timed sound event progression enum - may get moved to general RC launcher later
ENUM BOOT_EVENT
	BE_INIT,
	BE_WAITING,
	BE_PLAYING
ENDENUM

// Struct for timed sound event vars
STRUCT BOOT_EVENT_VARS
	BOOT_EVENT timedSound
	INT iSoundID
	INT iThumpTimer
	INT iThumpDelay
	INT iBounceLimit
	INT iBounceCount
	INT iBounceTimer
	INT iBounceDelay
	INT iBounceForce
	FLOAT fBounceOffSet
ENDSTRUCT

MODEL_NAMES 	  		mMaleContactModel        = GET_NPC_PED_MODEL(CHAR_NIGEL)
MODEL_NAMES 	  		mFemaleContactModel      = GET_NPC_PED_MODEL(CHAR_MRS_THORNHILL)
MODEL_NAMES				mAlDiNapoliModel		 = U_M_M_ALDINAPOLI
MODEL_NAMES 	  		mContactVehicleModel     = PREMIER
RC_LAUNCHER_ANIMS		extraAnims

SCENARIO_BLOCKING_INDEX RCMScenarioBlockArea

/// PURPOSE:
///    Blocks the ped scenarios at Nigel's start
FUNC SCENARIO_BLOCKING_INDEX Nigel1_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<-1212.16, 634.82, 138.50>>, <<-1010.08, 830.94, 180.47>>)
ENDFUNC

/// PURPOSE:
///    Stops peds spawning at Nigel's start
PROC SET_NIGEL1_PED_NON_CREATION_AREA()
	SET_PED_NON_CREATION_AREA(<<-1112.99, 774.12, 151.77>>, <<-1077.99, 853.65, 187.66>>)
ENDPROC

/// PURPOSE:
///    Setup common place to grab his ped model since it's now in use in launcher and mission
/// RETURNS:
///    Ai Di Napoli's MODEL_NAMES
FUNC MODEL_NAMES GET_AL_DI_NAPOLI_PED_MODEL()
	RETURN U_M_M_ALDINAPOLI
ENDFUNC

/// PURPOSE:
///    Setup common place to grab his scene handle for Nigel 2 intro mocap since it's now in use in launcher and mission
/// RETURNS:
///    Ai Di Napoli's scene handle
FUNC STRING GET_AL_DI_NAPOLI_SCENE_HANDLE()
	RETURN "Al_DiNapoli"
ENDFUNC


/// PURPOSE: 
///    Sets road areas and vehicle gens around the Nigel 1 cutscene area
/// PARAMS:
///    bSet - whether to enable or disable the settings
PROC ENABLE_VEHICLES_NEAR_NIGEL1(BOOL bSet)
	
	VECTOR vMin = <<-1212.16394, 634.82306, 100.50546>>
	VECTOR vMax = <<-1010.08270, 830.94873, 250.47050>>
	VECTOR v1 = vMin
	VECTOR v2 = vMax
	
	SET_ROADS_IN_ANGLED_AREA(v1, v2, 25.000000, FALSE, bSet)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, bSet)
	
	IF NOT bSet
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vMin, vMax)
		CLEAR_AREA(<<-1100.50, 789.8, 164.34>>, 40.0, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    ensure's the initial scene's sync scene is cleaned up prior to peds being released
/// PARAMS:
///    sRCLauncherData -Random char mission data needed to get reference to entities 
PROC CLEANUP_NIGEL1A_SYNC_SCENE(g_structRCScriptArgs& sRCLauncherData)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherData.iSyncSceneIndex)
		// Willy
		IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
			IF IsPedPerformingTask(sRCLauncherData.pedID[0], SCRIPT_TASK_SYNCHRONIZED_SCENE)
				CLEAR_PED_TASKS(sRCLauncherData.pedID[0])
			ENDIF
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.pedID[0], INSTANT_BLEND_OUT, TRUE)
		ENDIF
		// Groupie
		IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
			IF IsPedPerformingTask(sRCLauncherData.pedID[1], SCRIPT_TASK_SYNCHRONIZED_SCENE)
				CLEAR_PED_TASKS(sRCLauncherData.pedID[1])
			ENDIF
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.pedID[1], INSTANT_BLEND_OUT, TRUE)
		ENDIF
		CPRINTLN(DEBUG_RANDOM_CHAR, GET_THIS_SCRIPT_NAME(), ": CLEANUP_NIGEL1A_SYNC_SCENE : sync scene cleaned up sRCLauncherData.iSyncSceneIndex = ", sRCLauncherData.iSyncSceneIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    ensure's the initial scene's sync scene is cleaned up prior to peds being released
/// PARAMS:
///    sRCLauncherData -Random char mission data needed to get reference to entities 
PROC CLEANUP_NIGEL1B_SYNC_SCENE(g_structRCScriptArgs& sRCLauncherData)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherData.iSyncSceneIndex)
		IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
			IF IsPedPerformingTask(sRCLauncherData.pedID[1], SCRIPT_TASK_SYNCHRONIZED_SCENE)
				CLEAR_PED_TASKS(sRCLauncherData.pedID[1])
			ENDIF
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.pedID[1], INSTANT_BLEND_OUT, TRUE)
		ENDIF
		CPRINTLN(DEBUG_RANDOM_CHAR, GET_THIS_SCRIPT_NAME(), ": CLEANUP_NIGEL1B_SYNC_SCENE : sync scene cleaned up sRCLauncherData.iSyncSceneIndex = ", sRCLauncherData.iSyncSceneIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles area clearance for initial scenes where needed
PROC SETUP_AREA_FOR_MISSION(g_eRC_MissionIDs eMissionID, BOOL bEnable)
	
	SWITCH eMissionID
		CASE RC_NIGEL_1
			
			IF bEnable
				// Scenarios
				RCMScenarioBlockArea = Nigel1_Scenario_Blocker()
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", FALSE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_CLIPBOARD", FALSE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
				
				// Peds
				SET_NIGEL1_PED_NON_CREATION_AREA()
				CLEAR_AREA_OF_PEDS(<<-1100.50, 789.8, 164.34>>, 20.0)
				
				// Vehicles
				ENABLE_VEHICLES_NEAR_NIGEL1(FALSE)			
			ELSE
				// Restore everything to normal
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				CLEAR_PED_NON_CREATION_AREA()
				ENABLE_VEHICLES_NEAR_NIGEL1(TRUE)
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1A		// ensure any values changes are updated in the mission script to keep in sync SETUP_WORLD_STATE_FOR_MISSION
			IF bEnable
				
				// Scenarios
				RCMScenarioBlockArea = ADD_SCENARIO_BLOCKING_AREA(<< -577.27405, 270.65054, 77.0 >>, <<-543.17926, 315.29727, 94.0>>)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", FALSE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_CLIPBOARD", FALSE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
				
				// Peds
				SET_PED_NON_CREATION_AREA(<< -577.27405, 270.65054, 77.0 >>, <<-543.17926, 315.29727, 94.0>>)
				SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_DOORMAN_01, TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(S_M_M_BOUNCER_01, TRUE)
				CLEAR_AREA_OF_PEDS(<< -558.2858, 284.4073, 81.1764 >>, 15.0)
				CLEAR_AREA_OF_PEDS(<< -573.39404, 289.36960, 79.06918 >>, 15.0)	// extra clear area for bottom room in the club				
	
				// Vehicles
				CLEAR_AREA(<< -554.17, 307.68, 82.84 >>, 2.0, TRUE)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -577.27405, 270.65054, 77.0 >>, <<-543.17926, 315.29727, 94.0>>, FALSE)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -577.27405, 270.65054, 77.0 >>, <<-543.17926, 315.29727, 94.0>>)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
			ELSE
				// Restore everything to normal
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_GUARD_STAND", TRUE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_CLIPBOARD", TRUE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", TRUE)
				
				CLEAR_PED_NON_CREATION_AREA()
				SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_DOORMAN_01, FALSE)
				SET_PED_MODEL_IS_SUPPRESSED(S_M_M_BOUNCER_01, FALSE)
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -577.27405, 270.65054, 77.0 >>, <<-543.17926, 315.29727, 94.0>>, TRUE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, FALSE)
			ENDIF
		BREAK

		CASE RC_NIGEL_1B	// ensure any values changes are updated in the mission script to keep in sync
			IF bEnable
				// Scenarios
				RCMScenarioBlockArea = ADD_SCENARIO_BLOCKING_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>)	
				
				// Peds
				SET_PED_NON_CREATION_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>)
			
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>, FALSE)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>)
				
				CLEAR_AREA(<< -1035.73, 365.44, 68.91 >>, 35.0, TRUE)	// extended to fix bug 1042629 - Blood stain persisted after retry
				CLEAR_AREA_OF_VEHICLES(<< -1035.73, 365.44, 68.91 >>, 27.0, FALSE)
				CLEAR_AREA_OF_PEDS(<< -1035.73, 365.44, 68.91 >>, 27.0)	
			ELSE
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				CLEAR_PED_NON_CREATION_AREA()
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1073.19, 342.05, 63.328316 >>, << -966.24, 411.05, 84.820435 >>, TRUE)
			ENDIF
		BREAK
		
		CASE RC_NIGEL_1C
		BREAK
		
		CASE RC_NIGEL_1D
			IF bEnable
				RCMScenarioBlockArea = ADD_SCENARIO_BLOCKING_AREA(<<-1122.2018, 48.5724, 51.4652>>, <<-1076.2333, 92.1041, 60.0617>>)
				SET_PED_NON_CREATION_AREA(<<-1122.2018, 48.5724, 51.4652>>, <<-1076.2333, 92.1041, 60.0617>>)
				CLEAR_AREA(<<-1096.8550, 67.6858, 52.9520>>, 15, TRUE)
			ELSE
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				CLEAR_PED_NON_CREATION_AREA()
			ENDIF
		BREAK
		
		CASE RC_NIGEL_2
			IF bEnable
				// Scenarios
				RCMScenarioBlockArea = ADD_SCENARIO_BLOCKING_AREA(<<-1319.56628, -650.07697, 23.53715>>, <<-1301.78918, -634.07208, 38.18122>>)
				
				// Peds
				SET_PED_NON_CREATION_AREA(<<-1319.56628, -650.07697, 23.53715>>, <<-1301.78918, -634.07208, 38.18122>>)
			ELSE
				// Restore everything to normal
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				
				CLEAR_PED_NON_CREATION_AREA()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    get Nigel's vehicle's model name
///    Used in script to re request the model
/// RETURNS:
///    Nigel's vehicle's MODEL_NAMES
FUNC MODEL_NAMES GET_NIGEL_VEHICLE_MODEL()
	RETURN mContactVehicleModel
ENDFUNC

/// PURPOSE:
///    Creates Nigel's vehicle.  Always use this Function when creating it so it remains consistent.
/// PARAMS:
///    mVehicle - vehicle index to use
///    vPos - where to create it
///    fHeading - heading to use
///    bLockDoors - lock the doors on creation?
///    bWaitForLoad - wait for the model to load?
/// RETURNS:
///    TRUE if vehicle created, FALSE otherwise
FUNC BOOL CREATE_NIGEL_VEHICLE(VEHICLE_INDEX &mVehicle, VECTOR vPos, FLOAT fHeading, BOOL bLockDoors = FALSE, BOOL bWaitForLoad = TRUE) 
	
	REQUEST_MODEL(mContactVehicleModel)	
	IF bWaitForLoad = TRUE
		WHILE NOT HAS_MODEL_LOADED(mContactVehicleModel)
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT HAS_MODEL_LOADED(mContactVehicleModel)
			RETURN FALSE // model not loaded, can't create vehicle
		ENDIF
	ENDIF
	
	CREATE_SCENE_VEHICLE(mVehicle, mContactVehicleModel, vPos, fHeading)
	IF IS_ENTITY_ALIVE(mVehicle)
		SET_VEHICLE_COLOURS(mVehicle, 65, 0) //light blue
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mVehicle, SC_DOOR_BOOT, FALSE)
		SET_VEHICLE_NUMBER_PLATE_TEXT(mVehicle, "28BNT310")
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(mVehicle, 0)
		IF bLockDoors = TRUE
			SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_CANNOT_ENTER)
		ENDIF
	ENDIF	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mContactVehicleModel)
	RETURN TRUE
ENDFUNC 

/// PURPOSE:
///    Creates Al in position as if he is in the boot
/// RETURNS:
///    TRUE when ped has spawned "in boot"
FUNC BOOL SPAWN_AL_IN_TRUNK(PED_INDEX &pedID, VEHICLE_INDEX &vehID)
	IF DOES_ENTITY_EXIST(vehID)
	AND HAS_MODEL_LOADED(mAlDiNapoliModel)
		pedID = CREATE_PED(PEDTYPE_MISSION, U_M_M_ALDINAPOLI, << -59.7094, -1330.1289, 32.1963 >>)
		SET_PED_NAME_DEBUG(pedID, "NI3_LNCH_CELEB")
		SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 0, 0) // Change to 1,0 once he is skinned
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, TRUE)
			
		// Get car boot positions
		VECTOR vCarBoot
		vCarBoot = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID, <<-0.2000, -1.8000, -1.0000>>)
		VECTOR vCarBootAngles
		FLOAT fCarBootHeading
		fCarBootHeading = -90 + GET_ENTITY_HEADING(vehID)
		vCarBootAngles = << 0.0, 0.0, fCarBootHeading>>
			
		// Put ped in boot
		SET_ENTITY_COORDS_NO_OFFSET(pedID, vCarBoot)
		SET_ENTITY_ROTATION(pedID, vCarBootAngles)
		ATTACH_ENTITY_TO_ENTITY(pedID, vehID, 0, << 0.2, -1.8000, 0.0 >>, << 0, 0, 90 >>, FALSE)
		SET_ENTITY_VISIBLE(pedID, FALSE)
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This is used in the setup for Nigel 3 - makes the car boot thump and wobble as if Al is in it
PROC BOOT_THUMP_CYCLE(BOOT_EVENT_VARS &beV, VEHICLE_INDEX &vehID, PED_INDEX &pedID)

	IF IS_ENTITY_ALIVE(vehID)
		SWITCH beV.timedSound
		
			CASE BE_INIT
				// Once the sound has loaded, OK to proceed
				IF REQUEST_SCRIPT_AUDIO_BANK("CAR_TRUNK_THUMPS")
					beV.iSoundID = GET_SOUND_ID()
					beV.iThumpTimer = GET_GAME_TIMER()
					beV.iThumpDelay = GET_RANDOM_INT_IN_RANGE(6000,12000)
					beV.timedSound = BE_WAITING
				ENDIF
			BREAK
			
			CASE BE_WAITING
				// Wait to play a batch of thumps
				IF GET_GAME_TIMER() - beV.iThumpTimer > beV.iThumpDelay
					CPRINTLN(DEBUG_MISSION, "launcher_Nigel: BOOT_THUMP_CYCLE playing trunk thump sounds")
					PLAY_SOUND_FROM_COORD(beV.iSoundID, "TRUNK_THUMPS", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehID, << 0.0, -2.2, 0.5 >>)) // Play at offset to fix B*1098735
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedID, "NIGE3_CXAA", "DINAPOLI", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_STANDARD))
					beV.iBounceLimit = GET_RANDOM_INT_IN_RANGE(3,7)
					beV.iBounceCount = 0
					beV.iBounceTimer = GET_GAME_TIMER()
					beV.iBounceDelay = GET_RANDOM_INT_IN_RANGE(100,250)
					beV.timedSound = BE_PLAYING
				ENDIF
				
			BREAK
			
			CASE BE_PLAYING
				// Wait for triggering
				IF GET_GAME_TIMER() - beV.iBounceTimer > beV.iBounceDelay
					beV.iBounceForce = GET_RANDOM_INT_IN_RANGE(-200,-120)
					beV.fBounceOffSet = 0.5*TO_FLOAT(GET_RANDOM_INT_IN_RANGE(-2,3))
					APPLY_FORCE_TO_ENTITY(vehID, APPLY_TYPE_IMPULSE, << 0.0, 0.0, beV.iBounceForce >>, << bev.fBounceOffSet, -1.5, 0.0 >>, 0, TRUE, TRUE, FALSE, TRUE)
					beV.iBounceCount++
					IF beV.iBounceCount < beV.iBounceLimit
						beV.iBounceTimer = GET_GAME_TIMER()
						beV.iBounceDelay = GET_RANDOM_INT_IN_RANGE(150,280)
					ELSE
						beV.iThumpTimer = GET_GAME_TIMER()
						beV.iThumpDelay = GET_RANDOM_INT_IN_RANGE(6000,12000)
						beV.timedSound = BE_WAITING
					ENDIF
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF
	
ENDPROC

/// PURPOSE: 
///    Setup scene for Nigel 1
FUNC BOOL SetupScene_NIGEL_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT_MALE  	0
	CONST_INT  MODEL_CONTACT_FEMALE  1
	CONST_INT MODEL_CONTACT_CAR  	2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	INT         iSyncScene
	BOOL        bCreatedScene
	
	// Assign model names
	mModel[MODEL_CONTACT_MALE] = mMaleContactModel
	mModel[MODEL_CONTACT_FEMALE] = mFemaleContactModel
	mModel[MODEL_CONTACT_CAR]  = mContactVehicleModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
		
		CASE IS_REQUEST_SCENE
	
			// Setup specific launcher data
			sRCLauncherData.triggerType 			= RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene 			= "NMT_1_RCM"
			sRCLauncherData.bPedsCritical 			= TRUE
			sRCLauncherData.bVehsCritical = TRUE

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmnigel1", "base_nigel", "base_nigel")
			SETUP_LAUNCHER_ANIMS(extraAnims, "rcmnigel1", "base_mrs_thornhill", "base_mrs_thornhill")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE

			// Wait for assets
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE	
		
			// Create Nigel
			IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_NIGEL, <<-1100.50, 789.8, 164.34>>, 1.82, "RC: NIGEL")
				bCreatedScene = FALSE
			ENDIF
			
			// Create Mrs Thornhill
			IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[1], CHAR_MRS_THORNHILL, <<-1099.35,789.15,164.47>>, 0.0, "RC: MRS THORNHILL")
				bCreatedScene = FALSE
			ENDIF
			
			// Create Nigel's hire car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])				
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mContactVehicleModel, <<-1077.9590, 793.9361, 164.6374>>, 172.8563)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 65, 0) //light blue
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherData.vehID[0], SC_DOOR_BOOT, FALSE)
					SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[0], "28BNT310")
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[0], 0)
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_CAN_DEFORM_WHEELS(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherData.vehID[0], FALSE)
				ENDIF
			ENDIF
			
			// Ready to apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Setup synchronised scene
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0]) AND IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])	
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[1], TRUE)
				iSyncScene = CREATE_SYNCHRONIZED_SCENE(<<-1100.55786, 789.88617, 163.32849>>, <<0,0,0>>)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSyncScene, "rcmnigel1", "base_nigel", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[1], iSyncScene, "rcmnigel1", "base_mrs_thornhill", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			ENDIF

			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
	 
			// Setup area around cutscene
			SETUP_AREA_FOR_MISSION(RC_NIGEL_1, TRUE)
			
			// Scene is good to go!
			RETURN TRUE
		
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Setup scene for Nigel 1A
FUNC BOOL SetupScene_NIGEL_1A(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_PED_WILLY			0
	CONST_INT MODEL_PED_GROUPIE_1		1
	CONST_INT MODEL_PED_GROUPIE_2		2
	CONST_INT MODEL_PED_GROUPIE_3		3
	CONST_INT MODEL_PED_MANAGER			4
	CONST_INT MODEL_PED_ROADIE			5
	
	CONST_INT MODEL_VEHICLE_CARPARK		6
	
	// Variables
	MODEL_NAMES mModel[7] 
	INT         iCount

	// Assign model names
	mModel[MODEL_PED_WILLY] 		= U_M_M_WILLYFIST
	mModel[MODEL_PED_GROUPIE_1] 	= A_F_Y_BEVHILLS_03
	mModel[MODEL_PED_GROUPIE_2] 	= A_F_Y_BEVHILLS_03
	mModel[MODEL_PED_GROUPIE_3] 	= A_F_Y_BEVHILLS_03
	mModel[MODEL_PED_MANAGER] 		= A_M_Y_VINEWOOD_04
	mModel[MODEL_PED_ROADIE] 		= A_M_Y_GAY_01

	mModel[MODEL_VEHICLE_CARPARK] 	= GAUNTLET

	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS	
			sRCLauncherData.triggerLocate[0] = << -565.514587, 269.613922, 81.020805 >>
			sRCLauncherData.triggerLocate[1] = << -562.23370, 306.53821, 94.132584 >> 
			sRCLauncherData.triggerWidth = 30.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
	
			// B*1470358 - enable interior for the mission (gets cleaned up in launcher's cleanup if the mission wasn't launched
			SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)	
			PRINT_LAUNCHER_DEBUG(" SetupScene_NIGEL_1A - SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)	")
			
			// Set the dictionary in order for the launcher to remove the 
			// anims if the script has to terminate
			sRCLauncherData.sAnims.sDictionary = "rcmnigel1aig_1"
			
			sRCLauncherData.iSyncSceneIndex = -1	// initialise to -1
			
			// Setup area around cutscene
			SETUP_AREA_FOR_MISSION(RC_NIGEL_1A, TRUE)
			
			// B*1285146 - depopulates interior and regenerates it's content
			INTERIOR_INSTANCE_INDEX tempClubInterior
			tempClubInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -555.5934, 285.7738, 81.1763 >>, "v_rockclub")	
			IF IS_VALID_INTERIOR(tempClubInterior)
				REFRESH_INTERIOR(tempClubInterior)
			ENDIF
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			REQUEST_ANIM_DICT(sRCLauncherData.sAnims.sDictionary)
		
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			// Wait for assets
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sRCLauncherData.sAnims.sDictionary)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Create Willy
			CREATE_SCENE_PED(sRCLauncherData.pedID[0], mModel[MODEL_PED_WILLY], << -552.66, 287.75, 82.18 >>, 123.75)

			// Create Groupie
			CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_PED_GROUPIE_1], << -553.46, 287.34, 82.18 >>, -65.86)
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
				SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[1], FALSE)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,7), 0, 1, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			ENDIF
			
			// Set Willy and Groupie's sync scene running
			sRCLauncherData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<< -552.180, 285.476, 81.976 >>, << 0.0, 0.0, 79.5 >>)
			SET_SYNCHRONIZED_SCENE_LOOPED(sRCLauncherData.iSyncSceneIndex, TRUE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sRCLauncherData.iSyncSceneIndex, FALSE)
				
			// Willy task
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], sRCLauncherData.iSyncSceneIndex, sRCLauncherData.sAnims.sDictionary, "BASE_02_Willie", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
														SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS) 	// can use SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION since he ragdoll continuously	
			ENDIF
			// Groupie task
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[1], sRCLauncherData.iSyncSceneIndex, sRCLauncherData.sAnims.sDictionary, "BASE_02_GIRL", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
														SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS) 	// can use SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION since he ragdoll continuously
			ENDIF
			
			// Create peds upstairs in the club
			CREATE_SCENE_PED(sRCLauncherData.pedID[2], mModel[MODEL_PED_GROUPIE_2], << -559.94, 285.99, 85.38 >>, -59.06)
			CREATE_SCENE_PED(sRCLauncherData.pedID[3], mModel[MODEL_PED_GROUPIE_3], << -558.87, 289.79, 85.38 >>, 162.44)
			CREATE_SCENE_PED(sRCLauncherData.pedID[4], mModel[MODEL_PED_MANAGER], << -559.00, 286.09, 85.38 >>, 65.92)
			CREATE_SCENE_PED(sRCLauncherData.pedID[5], mModel[MODEL_PED_ROADIE], << -558.84, 288.97, 85.38 >>, -7.72)

			// TODO: add second sync scene index to struct
			/*iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM] = CREATE_SYNCHRONIZED_SCENE(vSyncedScenePosition_ClubPedsUpstairs, vSyncedSceneRotation_ClubPedsUpstairs)
			SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], TRUE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], FALSE)*/
	
			// MODEL_PED_GROUPIE_2 aka CLUB_PED_GROUPIE_UPSTAIRS_01 in mission script
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[2]) 
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,7), 1, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				
				/*tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
				sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[2], iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
									SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)*/
			ENDIF
			// MODEL_PED_GROUPIE_3 aka CLUB_PED_GROUPIE_UPSTAIRS_02 in mission script
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[3])  
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,7), 1, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				
				/*tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
				sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[3], iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
									SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)*/
			ENDIF						
			// MODEL_PED_MANAGER aka CLUB_PED_BAND_MANAGER in mission script
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[4])
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[4], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[4], INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[4], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[4], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[4], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
									
				/*tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
				sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[4], iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
									SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS)*/
			ENDIF
			// MODEL_PED_ROADIE aka CLUB_PED_ROADIE in mission script
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[5])
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[5], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[5], INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[5], INT_TO_ENUM(PED_COMPONENT,3), 0, 3, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[5], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[5], INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
				
				/*tlSyncedSceneAnimName = GET_CLUB_PED_ANIM_NAME(i, N1A_CPAT_BASE_ANIM)
				sSyncedSceneAnimName = N1A_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedSceneAnimName)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[5], iSyncedSceneID_ClubPedUpstairsScenes[N1A_CPAT_BASE_ANIM], tlAnimDict_ClubPedsUpstairs, sSyncedSceneAnimName, INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
									SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)*/
			ENDIF		
	
			// Create vehicle in carpark
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_VEHICLE_CARPARK], << -553.55, 308.61, 82.81 >>, 202.78)
			IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_UNLOCKED)
				SET_CAN_AUTO_VAULT_ON_ENTITY(sRCLauncherData.vehID[0], FALSE)
			ENDIF

			// Release assets
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Animations
			REMOVE_ANIM_DICT(sRCLauncherData.sAnims.sDictionary)
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Nigel 1B
FUNC BOOL SetupScene_NIGEL_1B(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_PED_MALE_CELEB		0
	CONST_INT MODEL_PED_FEMALE			1
	CONST_INT MODEL_PED_GARDENER		2
	CONST_INT MODEL_VEHICLE_GARDENER	3
	CONST_INT MODEL_VEHICLE_CELEB		4
	CONST_INT MODEL_PICKUP_CLOTHES		5
	
	// Variables
	MODEL_NAMES mModel[6]
	INT         iCount
	STRING 		sGardenerAnimDict = "rcmnigel1b"
	
	// Assign model names
	mModel[MODEL_PED_MALE_CELEB] 	= IG_TYLERDIX
	mModel[MODEL_PED_FEMALE] 		= A_F_Y_BEACH_01
	mModel[MODEL_PED_GARDENER] 		= S_M_M_GARDENER_01
	mModel[MODEL_VEHICLE_GARDENER] 	= SPEEDO
	mModel[MODEL_VEHICLE_CELEB] 	= COMET2
	mModel[MODEL_PICKUP_CLOTHES]	= Prop_nigel_bag_pickup // PROP_BEACH_BAG_01B

	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// had to split this into mutliple area checks - this is the majority section covering poolside
			// Gardener area is cover in ARE_RC_TRIGGER_CONDITIONS_MET
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS	
			sRCLauncherData.triggerLocate[0] = <<-1065.120239,366.343781,97.591011>>
			sRCLauncherData.triggerLocate[1] = <<-1020.774536,364.759552,65.238777>>
			sRCLauncherData.triggerWidth = 40.0
			sRCLauncherData.bAllowVehicleActivation = TRUE	
			
			// Set the dictionary in order for the launcher to remove the 
			// anims if the script has to terminate
			sRCLauncherData.sAnims.sDictionary = "rcmnigel1bnmt_1b"

			// Clear area for scene
			SETUP_AREA_FOR_MISSION(RC_NIGEL_1B, TRUE)
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT(sRCLauncherData.sAnims.sDictionary)
			REQUEST_ANIM_DICT(sGardenerAnimDict)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			// Wait for assets...
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sRCLauncherData.sAnims.sDictionary)
			OR NOT HAS_ANIM_DICT_LOADED(sGardenerAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
					
			// Create mission vehicles (created before peds since gardener needs to look in his vehicle)
			// Gardener's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_VEHICLE_GARDENER], << -1010.97, 359.91, 70.65 >>, 331.46)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
					SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 97, 97)
					SET_VEHICLE_DIRT_LEVEL(sRCLauncherData.vehID[0], 13.5)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT, FALSE)
					SET_VEHICLE_DOOR_OPEN(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT, FALSE, TRUE)	// Ste says door needs to be open before setting ratio
					SET_VEHICLE_DOOR_CONTROL(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sRCLauncherData.vehID[0])
					// Allow frame for set vehicle on ground to go through and door open request
					RETURN FALSE
				ENDIF
			ENDIF
			
			// B*1567157 - wait for the door to be open otherwise it can push the gardener out of position
			IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
				FLOAT fOpenRatio
				fOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT)
				IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT)
				AND (fOpenRatio < 0.8)	//secondary check to see if door is open
					//SET_VEHICLE_DOOR_OPEN(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT)	// swapped for below command due to bug*896419 - Mission doesn't progress to initial phone call
					SET_VEHICLE_DOOR_CONTROL(sRCLauncherData.vehID[0], SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)
					CPRINTLN(DEBUG_RANDOM_CHAR, GET_THIS_SCRIPT_NAME(), ": ", "waiting for gardener's van door to set open, current ratio is : ", fOpenRatio)
				ELSE
					eInitialSceneStage = IS_COMPLETE_SCENE
					CPRINTLN(DEBUG_RANDOM_CHAR, GET_THIS_SCRIPT_NAME(), ": ", "gardener's van door is open", fOpenRatio)
				ENDIF
			ENDIF
		BREAK
			
		CASE IS_COMPLETE_SCENE
			
			// Setup Gardener's van's anim
			IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
				IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherData.vehID[0], sGardenerAnimDict, "idle_speedo")
					IF HAS_ANIM_DICT_LOADED(sGardenerAnimDict)
						TASK_VEHICLE_PLAY_ANIM(sRCLauncherData.vehID[0], sGardenerAnimDict, "idle_speedo")
						// Have to wait to ensure anim is playing for the door opening to place the gardener looking in the back
						RETURN FALSE
					ELSE
						REQUEST_ANIM_DICT(sGardenerAnimDict)	// back as - should be loaded already in IS_WAIR_FOR_SCENE
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF

	
			// Celeb's vehicle
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_VEHICLE_CELEB], << -1018.26, 357.76, 70.20 >>, 339.52)
			IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[1])
				SET_VEHICLE_COLOURS(sRCLauncherData.vehID[1], 27, 27)	//red		
			ENDIF
	
			// Create Tyler Dixon
			CREATE_SCENE_PED(sRCLauncherData.pedID[0], mModel[MODEL_PED_MALE_CELEB], << -1054.70, 355.94, 68.35 >>, 23.67)
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
				SET_PED_DIES_IN_WATER(sRCLauncherData.pedID[0], FALSE)
				SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.pedID[0], << -1054.70, 355.94, 68.35 >>) 
				SET_ENTITY_HEADING(sRCLauncherData.pedID[0], 23.67) 
				// Set behaviour up
				//TASK_STAND_STILL(sRCLauncherData.pedID[0], -1)
				TASK_PLAY_ANIM(sRCLauncherData.pedID[0], sGardenerAnimDict, "Swimming_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
			ENDIF
			
			// Create Tyler's girlfriend
			CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_PED_FEMALE], << -1058.43, 362.78, 69.86 >>, 130.45)
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)		
				SET_PED_DIES_IN_WATER(sRCLauncherData.pedID[1], FALSE)

				sRCLauncherData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<< -1059.000, 360.976, 69.0 >>, << -0.000, 0.000, -1.080 >>)
				SET_SYNCHRONIZED_SCENE_LOOPED(sRCLauncherData.iSyncSceneIndex, TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sRCLauncherData.iSyncSceneIndex, FALSE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[1], sRCLauncherData.iSyncSceneIndex, sRCLauncherData.sAnims.sDictionary, "base_girl", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
														SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)	
			ENDIF
	
			// Create Gardener
			CREATE_SCENE_PED(sRCLauncherData.pedID[2], mModel[MODEL_PED_GARDENER], << -1012.57, 357.48, 70.62 >>, -28.55)
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[2])
				
				SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[2])
				SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.pedID[2], << -1012.57, 357.48, 70.62 >>)				
				SET_ENTITY_HEALTH(sRCLauncherData.pedID[2], 160)
				
				// set the ped's inital behaviour (looking in the back of the van)
				IF HAS_ANIM_DICT_LOADED(sGardenerAnimDict)
					TASK_PLAY_ANIM(sRCLauncherData.pedID[2], sGardenerAnimDict, "idle_gardener", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS) // B*1544302 - AF_USE_KINEMATIC_PHYSICS
				ENDIF
			ENDIF
	
			// Create clothes item pickup
			CREATE_SCENE_PROP(sRCLauncherData.objID[0], mModel[MODEL_PICKUP_CLOTHES], << -1050.03, 368.95, 69.29 >>, -11.27)
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[0])
				SET_ENTITY_ROTATION(sRCLauncherData.objID[0], << 0.00, 0.00, -11.27 >>)
				SET_ENTITY_CAN_BE_DAMAGED(sRCLauncherData.objID[0], FALSE)
				SET_ENTITY_INVINCIBLE(sRCLauncherData.objID[0], TRUE)
				SET_CAN_AUTO_VAULT_ON_ENTITY(sRCLauncherData.objID[0], FALSE)
				SET_CAN_CLIMB_ON_ENTITY(sRCLauncherData.objID[0], FALSE)	
				/* B*1528443 - don't do this until mission script kicks in as prop can drop below ground
				ACTIVATE_PHYSICS(sRCLauncherData.objID[0])
				SET_ENTITY_HAS_GRAVITY(sRCLauncherData.objID[0], TRUE) */
			ENDIF
			
			// Release assets
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
	
			// Animations
			REMOVE_ANIM_DICT(sRCLauncherData.sAnims.sDictionary)
			REMOVE_ANIM_DICT(sGardenerAnimDict)	// also used by the van anim
	
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH

	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Nigel 1C
FUNC BOOL SetupScene_NIGEL_1C(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_MILAN 0
	CONST_INT MODEL_DEXIE 1
	
	// Variables
	MODEL_NAMES	mModel[2]
	INT         iCount
	STRING 		ANIMS_KERRY = "rcmnigel1cnmt_1c"

	// Assign model names
	mModel[MODEL_MILAN] = IG_KERRYMCINTOSH
	mModel[MODEL_DEXIE] = A_C_ROTTWEILER
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS	
			sRCLauncherData.triggerLocate[0] = << -650.555847, -217.062653, 35.554352 >>
			sRCLauncherData.triggerLocate[1] = << -601.409851, -300.388702, 64.245743 >> 
			sRCLauncherData.triggerWidth = 35.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT(ANIMS_KERRY)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			// Load assets
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(ANIMS_KERRY)
				RETURN FALSE
			ENDIF
	
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE

			// Create Kerry McIntosh
			sRCLauncherData.pedID[0] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_MILAN], <<-626.87, -268.85, 38>>, 121.0) 
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], PED_COMP_HEAD, 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], PED_COMP_HAIR, 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], PED_COMP_TORSO, 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], PED_COMP_LEG, 0, 0, 0) //(lowr)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherData.pedID[0], CA_ALWAYS_FLEE, TRUE)
			SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherData.pedID[0], TRUE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherData.pedID[0], FALSE)
			STOP_PED_SPEAKING(sRCLauncherData.pedID[0], TRUE)

			// Setup anims
			SEQUENCE_INDEX siKerry
			OPEN_SEQUENCE_TASK(siKerry)
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "base")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "dexy_stay_there")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "base")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "price_tag")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "base")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "such_a_good")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "base")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "this_looks")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "base")
				TASK_PLAY_ANIM(NULL, ANIMS_KERRY, "who_would")
				SET_SEQUENCE_TO_REPEAT(siKerry, REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(siKerry)
			TASK_PERFORM_SEQUENCE(sRCLauncherData.pedID[0], siKerry)
			CLEAR_SEQUENCE_TASK(siKerry)

			// Create Dexie
			sRCLauncherData.pedID[1] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_DEXIE], <<-627.40, -267.12, 38.23>>, -59.95) 
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_TORSO, 0, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_LEG, 0, 1, 0) //(lowr)
			SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[1], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherData.pedID[1], TRUE)

			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
		
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Nigel 1D
FUNC BOOL SetupScene_NIGEL_1D(g_structRCScriptArgs& sRCLauncherData, TEST_POLY& pMissionTrigger)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT	MODEL_SECURITY    0
	CONST_INT	MODEL_CELEB_CLUB  1
	CONST_INT	MODEL_FRIEND_CLUB 2
	CONST_INT	MODEL_CELEB	  	  3
	CONST_INT	MODEL_FRIEND      4
	CONST_INT	MODEL_CADDY       5
	CONST_INT	MODEL_BAG         6
	
	// Variables
	MODEL_NAMES    mModel[7]
	STRING 		   sMiniGolfAnims = "mini@golf"
	INT            iCount
	
	// Assign model names
	mModel[MODEL_SECURITY]    = S_M_M_HighSec_01
	mModel[MODEL_CELEB_CLUB]  = prop_golf_wood_01
	mModel[MODEL_FRIEND_CLUB] = prop_golf_wood_01
	mModel[MODEL_CELEB]       = U_M_M_MARKFOST
	mModel[MODEL_FRIEND]      = A_M_Y_Golfer_01
	mModel[MODEL_CADDY]       = CADDY
	mModel[MODEL_BAG]         = PROP_GOLF_BAG_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
		
			// Setup specific launcher data
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.sAnims.sDictionary = "rcmnigel1d"
	
			// Setup trigger polygon
			OPEN_TEST_POLY(pMissionTrigger)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1313.7141, -27.2202, 48.0314 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1294.0658, -27.0136, 47.5724 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1069.8435, -143.7690, 39.7493 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -925.2529, -89.9079, 38.3457 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1010.9485, 40.0643, 49.9277 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1122.8375, 233.7037, 64.7587 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1282.5781, 188.8798, 59.2769 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1328.1532, 192.0178, 57.8136 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1386.75, 167.32, 57.11 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1411.50, 109.64, 51.49 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1379.52, 111.05, 54.28 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1371.77, 24.07, 52.88 >>)
				ADD_TEST_POLY_VERT(pMissionTrigger, << -1342.27, 16.92, 52.16 >>)
			CLOSE_TEST_POLY(pMissionTrigger)
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT(sRCLauncherData.sAnims.sDictionary)
			REQUEST_ANIM_DICT(sMiniGolfAnims)
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE
			
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sRCLauncherData.sAnims.sDictionary)
			OR NOT HAS_ANIM_DICT_LOADED(sMiniGolfAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		
		CASE IS_CREATE_SCENE

			// Setup scene
			SETUP_AREA_FOR_MISSION(RC_NIGEL_1D, TRUE)
	
			// Create Mark Fostenburg
			sRCLauncherData.pedID[0] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_CELEB], <<-1096.8550, 67.6858, 52.9520>>, 112.0)
			SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_HEAD, 0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0],TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherData.pedID[0],TRUE)
			SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherData.pedID[0],TRUE)
			SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[0],TRUE)
	
			// Create and attach golf club
			sRCLauncherData.objID[0] = CREATE_OBJECT(mModel[MODEL_CELEB_CLUB], <<-1096.8550, 67.6858, 52.9520>>)
			ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[0],sRCLauncherData.pedID[0],GET_PED_BONE_INDEX(sRCLauncherData.pedID[0],BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
			
			// Apply animation task
			SEQUENCE_INDEX siCelebrity
			OPEN_SEQUENCE_TASK(siCelebrity)
				TASK_PLAY_ANIM(NULL,sRCLauncherData.sAnims.sDictionary,"swing_a_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
				TASK_PLAY_ANIM(NULL,sRCLauncherData.sAnims.sDictionary,"swing_b_mark",NORMAL_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT)
				SET_SEQUENCE_TO_REPEAT(siCelebrity,REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(siCelebrity)
			TASK_PERFORM_SEQUENCE(sRCLauncherData.pedID[0], siCelebrity)
			CLEAR_SEQUENCE_TASK(siCelebrity)

			// Create friend
			sRCLauncherData.pedID[1] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_FRIEND], <<-1096.2576, 69.7230, 53.0107>>, 163.0)
			TASK_PLAY_ANIM(sRCLauncherData.pedID[1],sRCLauncherData.sAnims.sDictionary,"idle_a_friend_watching",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[1],TRUE)
	
			// Attach golf club to friend's hand
			sRCLauncherData.objID[1] = CREATE_OBJECT(mModel[MODEL_FRIEND_CLUB], <<-1096.2576, 69.7230, 53.0107>>)
			ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[1],sRCLauncherData.pedID[1],GET_PED_BONE_INDEX(sRCLauncherData.pedID[1],BONETAG_PH_R_HAND),<<0.0, 0.0, 0.0>>,<<0, 0, 0>>)
	
			// Create security guards
			sRCLauncherData.pedID[2] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_SECURITY], <<-1105.6224, 66.4609, 53.0700>>, 118.0)
			sRCLauncherData.pedID[3] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_SECURITY], <<-1090.0902, 64.3905, 52.5607>>, 238.0)
			sRCLauncherData.pedID[4] = CREATE_PED(PEDTYPE_MISSION, mModel[MODEL_SECURITY], <<-1093.7360, 73.8568, 53.0388>>, 319.0)

			// Setup guards
			FOR iCount = 2 TO 4
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[iCount],TRUE) 
				GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[iCount],WEAPONTYPE_PISTOL,-1)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherData.pedID[iCount],TRUE)
				SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[iCount],TRUE)
				SET_PED_DIES_WHEN_INJURED(sRCLauncherData.pedID[iCount],TRUE)
				SET_PED_PATH_PREFER_TO_AVOID_WATER(sRCLauncherData.pedID[iCount], TRUE)
				SET_PED_MODEL_IS_SUPPRESSED(mModel[MODEL_SECURITY], TRUE)
			ENDFOR
	
			// Create Fostenburg's caddy
			sRCLauncherData.vehID[0] = CREATE_VEHICLE(mModel[MODEL_CADDY], <<-1099.9100, 61.2608, 52.8124 >>, 243.0)
			SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[0])
			SET_VEHICLE_ENGINE_ON(sRCLauncherData.vehID[0],TRUE,TRUE)
			SET_ENTITY_HEALTH(sRCLauncherData.vehID[0],1200)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_CADDY],TRUE)
	
			// Attach gold bag to caddy
			sRCLauncherData.objID[2] = CREATE_OBJECT(mModel[MODEL_BAG], <<-1099.9100, 63.2608, 52.8124 >>)
			ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[2], sRCLauncherData.vehID[0],0,<<0.2,-1.3,0.5>>,<<0,0,0>>)
	
			// Create additional caddies
			sRCLauncherData.vehID[1] = CREATE_VEHICLE(mModel[MODEL_CADDY], <<-1102.3347, 75.7757, 53.2591 >>, 293.0)
			sRCLauncherData.vehID[2] = CREATE_VEHICLE(mModel[MODEL_CADDY], <<-1103.0232, 65.6267, 53.0560 >>, 203.0)
			
			// Ensure they are set on the ground properly
			FOR iCount = 1 TO 2
				SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[iCount])
			ENDFOR
	
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Nigel 2
FUNC BOOL SetupScene_NIGEL_2(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT_MALE  	0
	CONST_INT MODEL_CONTACT_CAR  	1
	CONST_INT MODEL_TARGET_CAR  	2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT_MALE] = mMaleContactModel
	mModel[MODEL_CONTACT_CAR] = mContactVehicleModel
	mModel[MODEL_TARGET_CAR] = DUBSTA
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 9.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "NMT_2_RCM"

			// Setup area
			SETUP_AREA_FOR_MISSION(RC_NIGEL_2, TRUE)
			
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmnigel2", "base_nigel", "base_nigel")
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR

			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK		
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
		
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE	
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
		
			// Create Nigel
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_NIGEL, << -1310.65, -640.17, 26.52 >> , 153.43, "RC NIGEL")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Nigel's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mContactVehicleModel, <<-1304.01, -644.58, 25.91>>, 127.67)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 65, 0) //light blue
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherData.vehID[0], SC_DOOR_BOOT, FALSE)
					SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[0], "28BNT310")
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[0], 0)
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_CANNOT_ENTER)
				ENDIF
			ENDIF
			
			// Create Al's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_TARGET_CAR], << -1290.73, -634.603, 26.1004 >> , 126.961)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[1])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[1], 0, 0)  //black
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[1], VEHICLELOCK_CANNOT_ENTER)
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
			
		CASE IS_COMPLETE_SCENE
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Nigel 3
FUNC BOOL SetupScene_NIGEL_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT_MALE  	0
	CONST_INT MODEL_CELEB_TRUNK		1
	CONST_INT MODEL_CONTACT_CAR  	2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT_MALE] = mMaleContactModel
	mModel[MODEL_CELEB_TRUNK]  = mAlDiNapoliModel
	mModel[MODEL_CONTACT_CAR]  = mContactVehicleModel
		
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "NMT_3_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmnigel3", "base")
			
			// Wait for assets
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
		
			// Create Nigel
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_NIGEL, << -44.60, -1289.86, 29.16 >>, 22.9, "RC NIGEL")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Nigel's hire car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])				
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mContactVehicleModel, << -39.9688, -1285.9615, 28.3505 >>,  181.02)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 65, 0) //light blue
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherData.vehID[0], SC_DOOR_BOOT, FALSE)
					SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[0], "28BNT310")
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[0], 0)
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_CANNOT_ENTER)
					SET_VEHICLE_CAN_DEFORM_WHEELS(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherData.vehID[0], FALSE)
				ENDIF
			ENDIF
							
			// Create Al DiNapoli in car trunk
			IF NOT SPAWN_AL_IN_TRUNK(sRCLauncherData.pedID[1], sRCLauncherData.vehID[0])
				bCreatedScene = FALSE
			ENDIF
		
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
			
		CASE IS_COMPLETE_SCENE
		
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
