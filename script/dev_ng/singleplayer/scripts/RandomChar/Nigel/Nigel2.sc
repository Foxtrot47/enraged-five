// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Nigel2.sc
//		AUTHOR			:	Ahron Mason
//		DESCRIPTION		:	Trevor helps Nigel and Mrs Thornhill kidnap a celebrity
//							Car chase
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "building_control_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "event_public.sch"
USING "initial_scenes_Nigel.sch"	//needed for call to SetupScene_NIGEL_2 to setup scene again when z / p skipping back to start of the mission
USING "taxi_functions.sch"
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//												 CONSTANTS FOR UBER PLAYBACK
//USING "traffic_default_values.sch" // this must be included before traffic.sch NOTE: when using uber, revert to below Constants instead
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      85
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       70    
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    70 
// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           15  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         11                            
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            10
USING "traffic.sch"

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:ENUMS:
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Mission stages
ENUM MISSION_STAGE
	MISSION_STAGE_INTRO_MOCAP_SCENE,
	MISSION_STAGE_CHASE_TARGET_IN_VEHICLE,	
	MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE,
	MISSION_STAGE_END_CUTSCENE_MOCAP,	
	MISSION_STAGE_MISSION_PASSED,			
	//Additional stages
	MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE,
	//debug stages
	MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE,
	MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE,
	MISSION_STAGE_DEBUG,
	MISSION_STAGE_DEBUG_HOPSITAL,
	MISSION_STAGE_DEBUG_HOPSITAL_DOORS
ENDENUM

/// PURPOSE: each mission stage uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

/// PURPOSE: cutscene stages
ENUM CUTSCENE_ENUM
	CE_START_CUTSCENE,
	CE_CAMERA_1,
	CE_CAMERA_2,
	CE_CAMERA_3,
	CE_CAMERA_4,
	CE_CAMERA_5,
	CE_SKIPPED_SCENE
ENDENUM

/// PURPOSE: fail conditions
ENUM FAILED_REASON_ENUM
	FAILED_DEFAULT = 0,
	FAILED_TARGET_DIED,
	FAILED_TARGET_ESCAPED,
	FAILED_NIGEL_VEHICLE_WRECKED,
	FAILED_NIGEL_ATTACKED,
	FAILED_NIGEL_DIED,	
	FAILED_MRS_THORNHILL_ATTACKED,
	FAILED_MRS_THORNHILL_DIED,	
	FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND,
	FAILED_LEFT_NIGEL_BEHIND,
	FAILED_LEFT_MRS_T_BEHIND
ENDENUM

/// PURPOSE: used to update the scripted peds on the chase routes behaviour
ENUM SCRIPTED_PED_STATE
	SPS_INIT,
	SPS_BEGIN_ROUTINE,
	SPS_AWARE_OF_TARGET,
	SPS_AWARE_OF_PLAYER,
	SPS_REACT_TO_TARGET,
	SPS_REACT_TO_PLAYER,
	SPS_READY_FOR_CLEANUP,
	SPS_CLEANED_UP
ENDENUM

/// PURPOSE: state machine for handling the interior for the chase
ENUM HOSPITAL_INTERIOR_STATE
	HIS_SET_INTERIOR_IPLS_FOR_CHASE,
	HIS_PIN_INTERIOR_IN_MEMORY,
	HIS_SET_ENTITY_SET_FOR_CHASE,
	HIS_REFRESH_INTERIOR,
	//HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR,
	//HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR,
	//HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR,
	HIS_READY,
	HIS_END
ENDENUM

/// PURPOSE: used for hospital door object state machine
ENUM HOSPITAL_DOORS_OBJECT_STATE
	HDO_CREATE,
	HDO_WAIT_FOR_DELETE,
	HDO_END
ENDENUM

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:STRUCTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: used by main scripted vehicles in the mission
STRUCT STRUCT_MISSION_VEHICLE
	VEHICLE_INDEX	vehicle
	MODEL_NAMES		model
	BLIP_INDEX		blip
	VECTOR			vSpawn
	FLOAT			fSpawnHeading
ENDSTRUCT

/// PURPOSE: used by all the main scripted peds in the mission
STRUCT STRUCT_MISSION_PED
	PED_INDEX 		ped
	MODEL_NAMES		model
	BLIP_INDEX		blip
	VECTOR			vSpawn
	FLOAT			fSpawnHeading
ENDSTRUCT

/// PURPOSE: used by the scripted peds who are seen along the chase route
STRUCT STRUCT_SIMPLE_MISSION_PED
	PED_INDEX 				ped
	SCRIPTED_PED_STATE		eState = SPS_INIT
ENDSTRUCT

/// PURPOSE: used by the scripted vehicle who are seen along the chase route
STRUCT STRUCT_SIMPLE_MISSION_VEHICLE
	VEHICLE_INDEX	vehicle
	MODEL_NAMES		model
ENDSTRUCT

STRUCT STRUCT_MISSION_OBJECT
	OBJECT_INDEX objectIndex
	VECTOR vPos
	VECTOR vRot	
	MODEL_NAMES modelName = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:CONSTANTS
//-------------------------------------------------------------------------------------------------------------------------------------------------
CONST_FLOAT NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE 					0.7			// targets blip flashes when he gets further than this percentage away to NIGEL2_TARGET_ESCAPED_DISTANCE
CONST_FLOAT NIGEL2_TARGET_ESCAPED_DISTANCE 						200.0		// fail distance for target escaping
CONST_FLOAT NIGEL2_PLAYER_CLOSE_TO_TARGET_CRASHED				25.0		// dist check player to target for going to park alongside Di Napoli stage
CONST_FLOAT PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE			100.0		// fail distance for leaving nigel and mrs t behind
CONST_FLOAT PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE			60.0		// fail warning distance for leaving nigel and mrs t behind
CONST_FLOAT RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE			30.0		// reset fail distance message for leaving nigel and mrs t behind
CONST_FLOAT NIGEL2_START_AREA_PEDS_TRIGGER_TIME 				500.0		// point in the uber playback we setup the ambient scripted peds aroudn the chase start area
CONST_FLOAT NIGEL2_HOSPITAL_PEDS_TRIGGER_TIME 					111161.0	// point in the uber playback we setup the ambient scripted peds in the hospital
CONST_FLOAT NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT			114436.0	// point in the uber playback we play the rayfire hospital doors anim
CONST_FLOAT NIGEL2_HOSPITAL_WALL_SMASH_01_TRIGGER_POINT			117166.0	// point in the uber playback we trigger wall smash 01 sfx
CONST_FLOAT NIGEL2_HOSPITAL_WALL_SMASH_02_TRIGGER_POINT			117736.0	// point in the uber playback we trigger wall smash 02 sfx
CONST_FLOAT NIGEL2_HOSPITAL_WALL_SMASH_03_TRIGGER_POINT			118496.0	// point in the uber playback we trigger wall smash 03 sfx
CONST_FLOAT NIGEL2_HOSPITAL_WINDOW_SMASH_TRIGGER_POINT			120986.0	// point in the uber playback we trigger window smash sfx
CONST_FLOAT NIGEL2_HOSPITAL_CRASH_SCREAMS_01_TRIGGER_POINT		114936.0	// point in the uber playback we trigger scream 01 sfx
CONST_FLOAT NIGEL2_HOSPITAL_CRASH_SCREAMS_02_TRIGGER_POINT		118186.0	// point in the uber playback we trigger scream 02 sfx
CONST_FLOAT NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT			124136.0	// point in the uber playback we apply damage to the target vehicle for smashing into the wall at the crash site
CONST_FLOAT NIGEL2_DEFAULT_UBER_PLAYBACK_SPEED					1.0			// can be tweaked to change the overal speed of the uber playback
CONST_FLOAT NIGEL2_UBER_TIME_STEP_01							43.5		// time to change veh rec start times to accomidate change rec for tunnel section
CONST_FLOAT NIGEL2_UBER_TIME_STEP_02							78.5		// time to change veh rec start times to accomidate change rec for cross roads
CONST_FLOAT NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_01				41011.0		// playeback time in original rec first step occured
CONST_FLOAT NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_02				77018.5		// playeback time in original rec first step occured
CONST_INT NIGEL2_TOTAL_DIALOGUE_LINES_GET_IN_NIGEL_CAR			2			// max dialogue lines / convos for initial state get in car	
CONST_INT NIGEL2_TOTAL_IMPORTANT_DIALOGUE_DURING_CHASE			8			// max dialogue lines / convos during car chase which are important
CONST_INT NIGEL2_TOTAL_CATCH_UP_DIALOGUE_DURING_CHASE			3			// max dialogue lines / convos for catch up dialogue	
CONST_INT NIGEL2_TOTAL_BANTER_DIALOGUE_DURING_CHASE				7			// max dialogue lines / convos for banter during chase
CONST_INT NIGEL2_MISSION_VEHICLES_HEALTH						2000		// initial vehicle health set on Nigel and target's vehicle
CONST_INT NIGEL2_TOTAL_UBER_VEHICLE_HORNS_TO_PROCESS			12			// horns done in script
CONST_INT NIGEL2_TOTAL_START_AREA_SCRIPTED_VEHICLES				3			// scripted vehicles at the start area, not part of the uber recording
CONST_INT TOTAL_HOSPITAL_PEDS									9			// scripted peds at the hospital
CONST_INT TOTAL_START_AREA_PEDS									4			// scripted peds at the start area
CONST_INT CP_NIGEL2_START_CHASE									0			// 1st mission replay checkpoint
CONST_INT CP_NIGEL2_MISSION_PASSED								1			// 2nd mission replay checkpoint - used by shitskip
CONST_INT Z_SKIP_INTRO_MOCAP									0
CONST_INT Z_SKIP_START_CHASE									1
CONST_INT Z_SKIP_PARK_CAR_NEXT_TO_TARGET						2
CONST_INT Z_SKIP_OUTRO_CUTSCENE									3
CONST_INT Z_SKIP_MISSION_PASSED									4
CONST_INT NIGEL2_MUSIC_EVENT_START								0
CONST_INT NIGEL2_MUSIC_EVENT_CAR								1
CONST_INT NIGEL2_MUSIC_EVENT_JUMP								2
CONST_INT NIGEL2_MUSIC_EVENT_STOP								3
CONST_INT NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE		5

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:VARIABLES:
//-------------------------------------------------------------------------------------------------------------------------------------------------

MISSION_STAGE eMissionStage 	= MISSION_STAGE_INTRO_MOCAP_SCENE // MISSION_STAGE_INTRO_MOCAP_SCENE //  MISSION_STAGE_INTRO_MOCAP_SCENE   MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE    MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE    MISSION_STAGE_DEBUG  MISSION_STAGE_DEBUG_HOPSITAL
MISSION_STAGE eMissionSkipTargetStage					//used in mission checkpoint setup and debug stage skipping
SUB_STAGE eSubStage 			= SS_SETUP			
g_structRCScriptArgs sRCLauncherDataLocal
#IF IS_DEBUG_BUILD//stage skipping
      CONST_INT MAX_SKIP_MENU_LENGTH 5
      MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF
BOOL bFinishedStageSkipping	= TRUE //used to determine if we are mission replay checkpoint skipping or debug skipping
BOOL bLoadedWorldForStageSkipping = FALSE	// flag to say if we haveloaded the world around the player when stage skipping
BOOL bDoneDialogue_BanterChase[NIGEL2_TOTAL_BANTER_DIALOGUE_DURING_CHASE]
BOOL bDoneDialogue_CatchUp[NIGEL2_TOTAL_CATCH_UP_DIALOGUE_DURING_CHASE]
BOOL bDoneDialogue_WrongWaySpecialFail
BOOL bDoneDialogue_Chase[NIGEL2_TOTAL_IMPORTANT_DIALOGUE_DURING_CHASE]
BOOL bDoneDialogue_InitialInstruction
BOOL bDoneDialogue_GetInNigelsVehicle[NIGEL2_TOTAL_DIALOGUE_LINES_GET_IN_NIGEL_CAR]
BOOL bDoneDialogue_WarnRamming
BOOL bDoneDialogue_TrevorGetsWantedLevel
BOOL bDoneDialogue_CopsSeenChasing
BOOL bDoneDialogue_DrivingOnWrongSideRoad
BOOL bDoneDialogue_UpsideDownOrAirbourne
BOOL bDoneObjective_GetBackInVehicle
BOOL bDoneObjective_Initial_GetInNigelsVehicle
//BOOL bDoneObjective_WaitForNigelAndMrsT
BOOL bDoneObjective_WarnLeavingNigelAndMrsT
BOOL bDoneCleanup_JumpOutHospitalWindow
BOOL bDoneOneTime_JumpOutHospitalWindow
BOOL bDone_KillConvoForOutTheVehicleDialogue
BOOL bDone_PlayerInNigelVehicle_FirstTime
BOOL bDone_RequestHospitalAudio
BOOL bDone_StartLocationVehicleHorn
BOOL bDone_TargetStartChaseRoute
BOOL bDone_TurnEngineOnForPlayer
BOOL bDone_UberRecordingCleanupForChaseEnd
BOOL bDone_UpdateTrashTruckPedModel
BOOL bDone_UpdateBMXPedModel
BOOL bDone_UberVehicleHorn[NIGEL2_TOTAL_UBER_VEHICLE_HORNS_TO_PROCESS]
BOOL bDone_VisualDamageForTargetVehicle
BOOL bDone_HospitalJumpPaperTrailFX
BOOL bHadToFadeOutToLoadMissionAssets
BOOL bHasPTFX_ForHospitalDebrisBeenRequested
BOOL bIsImportantDialoguePlaying
BOOL bIsInterruptDialoguePlaying
BOOL bIsHospitalDebrisEffectActive
BOOL bStartedPlayBack_ScriptedVehicleAtStartLocation
BOOL bStatTracker_HasPlayerKeptCloseToDiNapoli
BOOL bStatTracker_IsNigelVehicleDamageStatActive
BOOL bStatTracker_HasPlayerInjuredPedWhilstInHospital
//BOOL bSetHospital_IPL_InteriorLoadedState
BOOL bHasSetExitStateFromOutroMocapForGameplayCam
BOOL bRequestedOutroMocap
BOOL bForceTargetblipFlashThisFrame
BOOL bSetNigelVehicleHDForMocapExit
BOOL bTriggerLandingFromJumpSFX
CAMERA_INDEX camHospitalJump
FAILED_REASON_ENUM eN2_MissionFailedReason
INT iHospitalSoundFX_Progress
INT iPreLoadRecordingsTimer
INT iRayfireProgress_HospitalDoors
INT iTimer_DialogueDelay
INT iTimer_TrevorGetBackInCarDialogue
INT iTimer_NigelVehicleJumpsTimer
INT iTriggerScriptedPedestriansTimer
INT iUpdateUberVehicleTweaksTimer
INT iMissionMusicEventTriggerCounter
INT iTimer_NigelVehicleStuckOnRoofFail
INT iSoundID_UberVehicleFakeRevs
INT iMainCarRecID
INT iScenarioBlockingAreaStage
INT iTimer_MissionPassedDelay	// added because Nigel has to use waypoint rec to drive off which needs a little time
INT iTimer_OutroMocapDelay
INTERIOR_INSTANCE_INDEX HospitalInteriorIndex
HOSPITAL_INTERIOR_STATE eHospitalInteriorState
FLOAT fCimematicCamOvverideZoneDepth
FLOAT fCurrentChaseDistance
FLOAT fCurrentPlaybackTime
FLOAT fDistPlayerToNigelVehicle
FLOAT fMainPlaybackSpeed
FLOAT fTimeScale_HospitalJump
MODEL_NAMES mnDefaultPedForVehicle
MODEL_NAMES mnDoctorMale
MODEL_NAMES mnScrubsFemale
MODEL_NAMES mnFemaleStartArea
STRUCT_MISSION_OBJECT sObjectHospitalDoors
PED_INDEX pedIndexDialogueInterrupter
PTFX_ID HospitalDebris_PTFX_ID
PTFX_ID HospitalJump_PTFX_ID
RAYFIRE_INDEX rfHospitalDoors
REL_GROUP_HASH relGroupEnemy 
REL_GROUP_HASH relGroupFriendly
SCENARIO_BLOCKING_INDEX scenarioBlockingIntroArea
SCENARIO_BLOCKING_INDEX scenarioBlockingOutroArea
SCENARIO_BLOCKING_INDEX scenarioBlockingChaseRoute[NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE]
STRING sNigel2_UberRecordingName = "Nigel2U"
STRING sNigel2_CarRecNigelOutro = "Nigel2Outro"
STRING sSceneHandle_MrsThornhill = "Mrs_Thornhill"
STRING sSceneHandle_Nigel = "Nigel"
STRING sSceneHandle_Trevor = "Trevor"
STRING sSceneHandle_AlDiNapoli = GET_AL_DI_NAPOLI_SCENE_HANDLE()
STRING sSceneHandle_NigelVehicle = "Showroom_Car"
STRING sSceneHandle_AlDiNapoliVehicle = "EXL_2_abandoned_car"
structPedsForConversation sDialogue
STRUCT_MISSION_PED sMrsThornhillPed
STRUCT_MISSION_PED sNigelPed
STRUCT_MISSION_PED sTargetPed
STRUCT_MISSION_VEHICLE sNigelVehicle
STRUCT_MISSION_VEHICLE sTargetVehicle
//STRUCT_SIMPLE_MISSION_PED sDogPedSimple
STRUCT_SIMPLE_MISSION_PED sDefaultPedForScriptedVehicle
STRUCT_SIMPLE_MISSION_PED sHospitalPedSimple[TOTAL_HOSPITAL_PEDS]					
STRUCT_SIMPLE_MISSION_PED sStartAreaPedSimple[TOTAL_START_AREA_PEDS]
STRUCT_SIMPLE_MISSION_VEHICLE sStartLocationVehicle[NIGEL2_TOTAL_START_AREA_SCRIPTED_VEHICLES]
STRUCT_SIMPLE_MISSION_VEHICLE sVehicleForPlayerOnMissionPassed
HOSPITAL_DOORS_OBJECT_STATE eHospitalDoorsObjectState
//TEXT_LABEL_23 tl23_CurrentConversation
TEXT_LABEL_23 tlOutroMocapName = "NMT_2_MCS_2"
VECTOR vCinematicCamOverrideZone01
VECTOR vCinematicCamOverrideZone02
VECTOR vPlayerPos
//VECTOR vCoords_IPL_PILLBOX_HILL_INTERIOR
//VECTOR vCoords_IPL_PILLBOX_HILL
VECTOR vJumpOutHospitalCamCoords
VECTOR vJumpOutHospitalCamLookAtOffset

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:DEBUG FUNCS / PROCS / WIDGETS
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD	
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintMissionInfoToTTY							= FALSE		// needs to be false for submit!
	BOOL bDebug_PrintMissionRubberbandInfoToTTY					= FALSE		// needs to be false for submit!
	BOOL bDebug_PrintHospitalStatTrackInfoToTTY					= FALSE		// needs to be false for submit!
	BOOL bOutputTargetPlaybackTime 								= FALSE
	BOOL bDebug_OpenNigelCarBoot 								= FALSE
	BOOL bDebug_CloseNigelCarBoot 								= FALSE
	BOOL bDebug_ResetForPlaceInBoot								= FALSE
	BOOL bDebug_UnFreezePedInBootPos 							= FALSE
	VECTOR vDebug_PositionPedInBoot
	VECTOR vDebug_PositionPedInBootOffset						= <<-0.2000, -1.8000, -1.0000>>
	VECTOR vDebug_RotationPedInBoot 							= <<15.0000, -85.0000, 0.0000>>
	BOOL bDebug_OutputPedInBootValues 							= FALSE
	BOOL bDebug_JustUpdatePedInBootPosAndRot 					= FALSE
	BOOL bDebug_ApplyGameplayCam_HeadingAndPitch 				= FALSE
	FLOAT fDebug_GameplayCam_Heading 							= 0.0
	FLOAT fDebug_GameplayCam_Pitch 								= 0.0
	BOOL bDebug_GetCurrentGameplayHeadingAndPitch 				= FALSE
	BOOL bDebug_DoneDeformCar 									= FALSE
	VECTOR vDebug_CarDamageCoords	 							= << 0.0, 0.0, 0.0 >>
	FLOAT fDebug_CarDamageAmount								= 1.0
	FLOAT fDebug_CarDeformationAmount 							= 1.0
	BOOL bDebug_PrintPlayerVehicleInfo 							= FALSE	
	INT iDebug_TrafficCarToTurnCollisionOffOn 					= 0
	INT iDebug_SetPieceCarToTurnCollisionOffOn 					= 0
	BOOL bDebug_SelectedTrafficCarHasCollision 					= TRUE
	BOOL bDebug_SelectedSetPieceCarHasCollision 				= TRUE	
	VECTOR vDebug_TempPlayerVehicleSetPosition					= << 0.0, 0.0, 0.0 >>
	BOOL bDebug_ApplyPositionValueToPlayerVehicle 				= FALSE
	FLOAT fDebug_TempPlayerVehicleQuatX
	FLOAT fDebug_TempPlayerVehicleQuatY 
	FLOAT fDebug_TempPlayerVehicleQuatZ
	FLOAT fDebug_TempPlayerVehicleQuatW
	BOOL bDebug_ApplyQuaternionValuesToPlayerVehicle 			= FALSE
	BOOL bDebug_SnapPlayerVehicleToGround 						= FALSE
	PTFX_ID DEBUG_HospitalDebris_PTFX_ID
	VEHICLE_INDEX vehDebug_Ghost
	//FLOAT fDebug_CatchUpMultiplier
	INT iDebug_AdditionalAIDriveTime							= 0
	BOOL bDebug_ToggleGhostCarVisible							= FALSE
	BOOL bDebug_StartHospitalDebris_PTFX_ID						= FALSE
	BOOL bDebug_OutputRayfireHospitalDoorsProgress				= FALSE
	BOOL bDebug_OutputRayfireHospitalDoorsCurrentState			= FALSE
	BOOL bDebug_DisplayStatTrackerInfo_PlayerCloseToTarget		= FALSE
	BOOL bDebug_OverrideStatTracker_PlayerCloseToTargetRange	= FALSE
	FLOAT fDebug_StatTracker_PlayerCloseToTargetRange			= 100
	BOOl bDebug_CheckPlayerInsideHospital						= FALSE
	INT iDebug_IPL_PillBox_State								= 0
	BOOL bDebug_Set_IPL_PillBox_State							= FALSE
	INT iDebug_ES_PillBox_State									= 0
	BOOL bDebug_Set_ES_PillBox_State							= FALSE
	VECTOR vDebug_HospitalJumpFXOffset							= << 0.0, -1.5, -0.4 >>
	VECTOR vDebug_HospitalJumpFXRotation						= << 0.0, 0.0, 0.0 >>
	FLOAT fDebug_HospitalJumpFXScale							= 0.3
	BOOL bDebug_StartHospitalJumpFX								= FALSE
	
	BOOL bDebug_SetupMission_DebugStage							= FALSE
	FLOAT fDebug_OverrideMinTimeScale							= 0.25
	BOOL bDebug_OverrideMinTimeScale							= FALSE
	FLOAT fDebug_TimeScaleStep_ModifierDecrease					= 0.9
	BOOL bDebug_OverrideTimeScaleStepModifier_Decrease			= FALSE
	FLOAT fDebug_TimeScaleStep_ModifierIncrease					= 2.0
	BOOL bDebug_OverrideTimeScaleStepModifier_Increase			= FALSE
	
	/// PURPOSE:
	/// My debug mission widget groups, which get created in RAG->SCRIPT
	PROC SETUP_MISSION_WIDGET()
		widgetGroup = START_WIDGET_GROUP("NIGEL 2 WIDGETS")
			START_WIDGET_GROUP("General")
				ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintMissionInfoToTTY)	
				ADD_WIDGET_BOOL("TTY Toggle - Print Rubberbanding Debug Info", bDebug_PrintMissionRubberbandInfoToTTY)			
				ADD_WIDGET_BOOL("TTY Toggle - Print hospital stat track Debug Info", bDebug_PrintHospitalStatTrackInfoToTTY)					
				ADD_WIDGET_BOOL("Debug Output Rayfire Hospital Doors Progress", bDebug_OutputRayfireHospitalDoorsProgress)
				ADD_WIDGET_BOOL("Debug Output Rayfire Hospital Doors Current state every frame", bDebug_OutputRayfireHospitalDoorsCurrentState)				
				ADD_WIDGET_BOOL("Apply Gameplay Cam Heading And Pitch", bDebug_ApplyGameplayCam_HeadingAndPitch)
				ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Heading -", fDebug_GameplayCam_Heading, -360.0, 360.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Pitch -", fDebug_GameplayCam_Pitch, -360.0, 360.0, 0.1)
				ADD_WIDGET_BOOL("Print Current Gameplay Cam Heading and Pitch", bDebug_GetCurrentGameplayHeadingAndPitch)
				ADD_WIDGET_BOOL("Test player inside hospital interior", bDebug_CheckPlayerInsideHospital)
				ADD_WIDGET_BOOL("Debug Display Stat Tracker Info - Player Close To Target Range", bDebug_DisplayStatTrackerInfo_PlayerCloseToTarget)
				ADD_WIDGET_BOOL("Debug Override Stat Tracker - Player Close To Target Range", bDebug_OverrideStatTracker_PlayerCloseToTargetRange)
				ADD_WIDGET_FLOAT_SLIDER("Debug Stat Tracker Player Close To Target Range = ", fDebug_StatTracker_PlayerCloseToTargetRange, 0.0, 300.0, 0.1)
				ADD_WIDGET_INT_SLIDER("Traffic Car To Turn Off Collision On ID - ", iDebug_TrafficCarToTurnCollisionOffOn, 0, 150, 1)
				ADD_WIDGET_BOOL("Toggle Selected Traffic Car's Collision - ", bDebug_SelectedTrafficCarHasCollision)
				ADD_WIDGET_INT_SLIDER("Set Piece Car To Turn Off Collision On ID - ", iDebug_SetPieceCarToTurnCollisionOffOn, 0, 150, 1)
				ADD_WIDGET_BOOL("Toggle Selected Set Piece Car's Collision - ", bDebug_SelectedSetPieceCarHasCollision)							
				ADD_WIDGET_BOOL("Start hospital debris PTFX", bDebug_StartHospitalDebris_PTFX_ID)				
							
				ADD_WIDGET_INT_SLIDER("iDebug_IPL_PillBox_State : ", iDebug_IPL_PillBox_State, 0, 150, 1)
				ADD_WIDGET_BOOL("bDebug_Set_IPL_PillBox_State", bDebug_Set_IPL_PillBox_State)	
				ADD_WIDGET_INT_SLIDER("iDebug_ES_PillBox_State : ", iDebug_ES_PillBox_State, 0, 150, 1)
				ADD_WIDGET_BOOL("bDebug_Set_ES_PillBox_State", bDebug_Set_ES_PillBox_State)		
				
				ADD_WIDGET_FLOAT_SLIDER("Force Cinematic Cam Zone Depth - ", fCimematicCamOvverideZoneDepth, -360.0, 360.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Force Cinematic Cam Zone Pos 1 - ", vCinematicCamOverrideZone01, -1000, 1000, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Force Cinematic Cam Zone Pos 2 - ", vCinematicCamOverrideZone02, -1000, 1000, 0.1)
				
				ADD_WIDGET_BOOL("bDebug_StartHospitalJumpFX", bDebug_StartHospitalJumpFX)	
				ADD_WIDGET_VECTOR_SLIDER("vDebug_HospitalJumpFXOffset : ", vDebug_HospitalJumpFXOffset, -1000, 1000, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vDebug_HospitalJumpFXRotation : ", vDebug_HospitalJumpFXRotation, -1000, 1000, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fDebug_HospitalJumpFXScale : ", fDebug_HospitalJumpFXScale, 0.0, 5.0, 0.001)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Hospital Window Jump")
			
				ADD_WIDGET_BOOL("Set Mission To Debug Window Jump Stage", bDebug_SetupMission_DebugStage)
				
				ADD_WIDGET_VECTOR_SLIDER("vJumpOutHospitalCamCoords : ", vJumpOutHospitalCamCoords, -1000, 1000, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vJumpOutHospitalCamLookAtOffset : ", vJumpOutHospitalCamLookAtOffset, -1000, 1000, 0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("Set value : Min Time Scale Override (1.0 is normal) : ", fDebug_OverrideMinTimeScale, 0.0, 1.0, 0.001)	
				ADD_WIDGET_BOOL("Toggle - use Min time scale override", bDebug_OverrideMinTimeScale)
				
				ADD_WIDGET_FLOAT_SLIDER("Set value : Time Scale Step Modifier (to min scale) : ", fDebug_TimeScaleStep_ModifierDecrease, 0.0, 5.0, 0.001)	
				ADD_WIDGET_BOOL("Toggle - use time scale modifier (to min scale)", bDebug_OverrideTimeScaleStepModifier_Decrease)
				
				ADD_WIDGET_FLOAT_SLIDER("Set value : Time Scale Step Modifier (back to normal scale) : ", fDebug_TimeScaleStep_ModifierIncrease, 0.0, 5.0, 0.001)	
				ADD_WIDGET_BOOL("Toggle - use time scale modifier (back to normal scale)", bDebug_OverrideTimeScaleStepModifier_Increase)					
			STOP_WIDGET_GROUP()
		/*	START_WIDGET_GROUP("Rerecord main car")
				ADD_WIDGET_BOOL("TTY Toggle - toggle ghost car visible", bDebug_ToggleGhostCarVisible)	
				ADD_WIDGET_INT_SLIDER("iDebug_AdditionalAIDriveTime : ", iDebug_AdditionalAIDriveTime, -5000, 5000, 1)
			STOP_WIDGET_GROUP()*/
			/*
			START_WIDGET_GROUP("Placement of Ped in Boot")
				ADD_WIDGET_BOOL("Open Nigel's car boot", bDebug_OpenNigelCarBoot)
				ADD_WIDGET_BOOL("Close Nigel's car boot", bDebug_CloseNigelCarBoot)
				ADD_WIDGET_BOOL("Reset for ped positioning in boot", bDebug_ResetForPlaceInBoot)
				ADD_WIDGET_BOOL("Unfreeze ped", bDebug_UnFreezePedInBootPos)
				ADD_WIDGET_BOOL("Just update pos and rot", bDebug_JustUpdatePedInBootPosAndRot)
				ADD_WIDGET_VECTOR_SLIDER("Position Offset from car of ped", vDebug_PositionPedInBootOffset, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Rotation of ped", vDebug_RotationPedInBoot, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_BOOL("print ped in boot info to Temp_debug.txt", bDebug_OutputPedInBootValues)
			STOP_WIDGET_GROUP()
			*/
			/*
			START_WIDGET_GROUP("Configure Damage applied to target vehicle in crash scene")
				ADD_WIDGET_BOOL("Apply deform the target car.", bDebug_DoneDeformCar)
				ADD_WIDGET_VECTOR_SLIDER("Car Damage Coords - ", vDebug_CarDamageCoords, -1000.0, 1000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Car Damage amount - ", fDebug_CarDamageAmount, -50.0, 10000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Car Deformation amount - ", fDebug_CarDeformationAmount, -50.0, 10000.0, 1.0)
			STOP_WIDGET_GROUP()
			*/			
			START_WIDGET_GROUP("Player vehicle positioning stuff")
				ADD_WIDGET_VECTOR_SLIDER("Set player vehicle position vector - ", vDebug_TempPlayerVehicleSetPosition, -10000, 10000, 0.0001)
				ADD_WIDGET_BOOL("Apply above position to player's vehicle", bDebug_ApplyPositionValueToPlayerVehicle)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion X value -", fDebug_TempPlayerVehicleQuatX, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion Y value -", fDebug_TempPlayerVehicleQuatY, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion Z value -", fDebug_TempPlayerVehicleQuatZ, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion W value -", fDebug_TempPlayerVehicleQuatW, -3.0, 3.0, 0.0001)
				ADD_WIDGET_BOOL("Apply above quaternion to player's vehicle", bDebug_ApplyQuaternionValuesToPlayerVehicle)
				ADD_WIDGET_BOOL("Snap the player's current vehicle to the ground", bDebug_SnapPlayerVehicleToGround)
				ADD_WIDGET_BOOL("Print player's current vehicle info, pos, quat and heading", bDebug_PrintPlayerVehicleInfo)
			STOP_WIDGET_GROUP()			
			SET_UBER_PARENT_WIDGET_GROUP(widgetGroup)			
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    updates my mission widgets, based off RAG input
	PROC MAINTAIN_MISSION_WIDGETS()
		FLOAT fDebug_PlaybackTime
		TEXT_LABEL_63 tlTemp		
		/*	// debug fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRESSED KEY 0") ENDIF #ENDIF
	    	IF IS_PED_UNINJURED(sTargetPed.ped)
				SET_ENTITY_HEALTH(sTargetPed.ped, 0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - KILLED sTargetPed PED") ENDIF #ENDIF
			ENDIF
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRESSED KEY 9") ENDIF #ENDIF
	    	IF IS_PED_UNINJURED(PLAYER_PED_ID())
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - KILLED Player PED") ENDIF #ENDIF
			ENDIF
	    ENDIF
		*/
		BUILDING_STATE_ENUM eTempBuildingState
		// toggle the build swap states
		IF bDebug_Set_IPL_PillBox_State
			eTempBuildingState = INT_TO_ENUM(BUILDING_STATE_ENUM, iDebug_IPL_PillBox_State)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, eTempBuildingState)
			bDebug_Set_IPL_PillBox_State = FALSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG WIDGET: bDebug_Set_IPL_PillBox_State set to : ", eTempBuildingState) ENDIF #ENDIF
		ENDIF
		IF bDebug_Set_ES_PillBox_State
			eTempBuildingState = INT_TO_ENUM(BUILDING_STATE_ENUM, iDebug_ES_PillBox_State)
			SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, eTempBuildingState)	// contains the rayfire for the front door
			bDebug_Set_ES_PillBox_State = FALSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG WIDGET: bDebug_Set_ES_PillBox_State set to : ", eTempBuildingState) ENDIF #ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
		AND NOT IS_ENTITY_DEAD(sNigelVehicle.vehicle)
			IF bDebug_StartHospitalJumpFX
				REQUEST_PTFX_ASSET()
				IF HAS_PTFX_ASSET_LOADED()			
					IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalJump_PTFX_ID)
						STOP_PARTICLE_FX_LOOPED(HospitalJump_PTFX_ID)
					ENDIF
					HospitalJump_PTFX_ID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_rcn2_debris_trail", sNigelVehicle.vehicle, vDebug_HospitalJumpFXOffset,
																	vDebug_HospitalJumpFXRotation, fDebug_HospitalJumpFXScale)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG : PTFX hospital jump started FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					bDebug_StartHospitalJumpFX = FALSE
				ENDIF
			ENDIF
			//DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sNigelVehicle.vehicle, vDebug_HospitalJumpFXOffset), 0.25, 0, 50, 0, 100)
		ENDIF
		
		IF bDebug_ToggleGhostCarVisible
			IF IS_VEHICLE_OK(vehDebug_Ghost)
				IF IS_ENTITY_VISIBLE(vehDebug_Ghost)
					SET_ENTITY_VISIBLE(vehDebug_Ghost, FALSE)
				ELSE
					SET_ENTITY_VISIBLE(vehDebug_Ghost, TRUE)
				ENDIF
				bDebug_ToggleGhostCarVisible = FALSE
			ENDIF
		ENDIF
		
		//SET_DEBUG_ACTIVE(TRUE)
		//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
		IF bDebug_OpenNigelCarBoot
			IF DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
			AND NOT IS_ENTITY_DEAD(sNigelVehicle.vehicle)
				//IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(sNigelVehicle.vehicle, SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_OPEN(sNigelVehicle.vehicle, SC_DOOR_BOOT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG WIDGET: nigel vehicle foot set open") ENDIF #ENDIF
				//ENDIF
				bDebug_OpenNigelCarBoot = FALSE
			ENDIF
		ENDIF		
		IF bDebug_CloseNigelCarBoot
			IF DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
			AND NOT IS_ENTITY_DEAD(sNigelVehicle.vehicle)
				//IF IS_VEHICLE_DOOR_FULLY_OPEN(sNigelVehicle.vehicle, SC_DOOR_BOOT)
					SET_VEHICLE_DOOR_SHUT(sNigelVehicle.vehicle, SC_DOOR_BOOT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG WIDGET: nigel vehicle door set closed") ENDIF #ENDIF
				//ENDIF
				bDebug_CloseNigelCarBoot = FALSE
			ENDIF
		ENDIF			
		IF bDebug_ResetForPlaceInBoot
			IF DOES_ENTITY_EXIST(sTargetPed.ped)
			AND NOT IS_PED_INJURED(sTargetPed.ped)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG WIDGET: about to detach player and celeb") ENDIF #ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(sTargetPed.ped)
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(sTargetPed.ped)
						DETACH_ENTITY(sTargetPed.ped)
					ENDIF
				ENDIF
				SET_PED_GRAVITY(sTargetPed.ped, FALSE)
				SET_ENTITY_COLLISION(sTargetPed.ped, FALSE)
				FREEZE_ENTITY_POSITION(sTargetPed.ped, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTargetPed.ped, TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(sTargetPed.ped)
				//TASK_PLAY_ANIM(sTargetPed.ped, "ped", "nm_foetal", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME) //AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE, 1.0)
				//vDebug_PositionPedInBoot = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sNigelVehicle.vehicle, vDebug_PositionPedInBootOffset)
				SET_ENTITY_COORDS(sTargetPed.ped, vDebug_PositionPedInBoot)
				SET_ENTITY_ROTATION(sTargetPed.ped, vDebug_RotationPedInBoot)
				//IF NOT IS_ENTITY_PLAYING_ANIM(sTargetPed.ped, "ped", "nm_foetal")
					//TASK_PLAY_ANIM_ADVANCED(sTargetPed.ped, "ped", "nm_foetal", vDebug_PositionPedInBoot, vDebug_RotationPedInBoot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE, 1.0)
				//ENDIF
				//FREEZE_ENTITY_POSITION(sTargetPed.ped, TRUE)
				bDebug_ResetForPlaceInBoot = FALSE
			ENDIF			
		ENDIF		
		IF bDebug_UnFreezePedInBootPos
			IF DOES_ENTITY_EXIST(sTargetPed.ped)
			AND NOT IS_PED_INJURED(sTargetPed.ped)
				FREEZE_ENTITY_POSITION(sTargetPed.ped, FALSE)
				//SET_PED_GRAVITY(sTargetPed.ped, TRUE)
				//SET_ENTITY_COLLISION(sTargetPed.ped, TRUE)
				bDebug_UnFreezePedInBootPos = FALSE
			ENDIF			
		ENDIF		
		IF bDebug_JustUpdatePedInBootPosAndRot
			SET_ENTITY_COORDS(sTargetPed.ped, vDebug_PositionPedInBoot)
			SET_ENTITY_ROTATION(sTargetPed.ped, vDebug_RotationPedInBoot)
			FREEZE_ENTITY_POSITION(sTargetPed.ped, TRUE)
			bDebug_JustUpdatePedInBootPosAndRot = FALSE
		ENDIF		
		IF bDebug_OutputPedInBootValues
				OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "vPositionOffsetFromCarOfPedForBoot "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_VECTOR_TO_DEBUG_FILE(vDebug_PositionPedInBootOffset)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "vRotationOfPedForBoot "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_VECTOR_TO_DEBUG_FILE(vDebug_RotationPedInBoot)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "BOOT PED INFO PRINTED") ENDIF #ENDIF
			bDebug_OutputPedInBootValues = FALSE
		ENDIF		
		IF bOutputTargetPlaybackTime
		AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
			IF NOT IS_ENTITY_DEAD(sTargetVehicle.vehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
					fDebug_PlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)
					OPEN_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "TARGET'S PLAYBACK TIME : "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_PlaybackTime)
						SAVE_NEWLINE_TO_DEBUG_FILE()
					CLOSE_DEBUG_FILE()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TARGET pLAYBACK INFO PRINTED") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF		
		IF bDebug_ApplyGameplayCam_HeadingAndPitch
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(fDebug_GameplayCam_Heading)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(fDebug_GameplayCam_Pitch)
			bDebug_ApplyGameplayCam_HeadingAndPitch = FALSE
		ENDIF
		IF bDebug_GetCurrentGameplayHeadingAndPitch
			FLOAT fTemp_Heading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
			FLOAT fTemp_Pitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
			OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "Current Gameplay Cam Relative Heading : "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_FLOAT_TO_DEBUG_FILE(fTemp_Heading)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "Current Gameplay Cam Relative Pitch : "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_FLOAT_TO_DEBUG_FILE(fTemp_Pitch)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()		
			bDebug_GetCurrentGameplayHeadingAndPitch = FALSE
		ENDIF		
		IF bDebug_DoneDeformCar
			IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
				//SET_ENTITY_HEALTH(sTargetVehicle.vehicle, 1000)				
				FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
				WAIT(0)
				IF NOT IS_ENTITY_DEAD(sTargetVehicle.vehicle)
					SET_VEHICLE_DAMAGE(sTargetVehicle.vehicle, vDebug_CarDamageCoords, fDebug_CarDamageAmount, fDebug_CarDeformationAmount, TRUE)
					INT iDebug_VehicleHealth = GET_ENTITY_HEALTH(sTargetVehicle.vehicle)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Vehicle damage applied, health now at - ", iDebug_VehicleHealth) ENDIF #ENDIF
					bDebug_DoneDeformCar = FALSE
				ENDIF
			ENDIF
		ENDIF			
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehDebug = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehDebug)
				IF bDebug_ApplyPositionValueToPlayerVehicle
					SET_ENTITY_COORDS_NO_OFFSET(vehDebug, vDebug_TempPlayerVehicleSetPosition)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - UPDATED PLAYER VEHICLE POS") ENDIF #ENDIF
					bDebug_ApplyPositionValueToPlayerVehicle = FALSE
				ENDIF				
				IF bDebug_ApplyQuaternionValuesToPlayerVehicle
					SET_ENTITY_QUATERNION(vehDebug, fDebug_TempPlayerVehicleQuatX, fDebug_TempPlayerVehicleQuatY, fDebug_TempPlayerVehicleQuatZ, fDebug_TempPlayerVehicleQuatW)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - UPDATED PLAYER VEHICLE QUATERNION") ENDIF #ENDIF
					bDebug_ApplyQuaternionValuesToPlayerVehicle = FALSE
				ENDIF				
				IF bDebug_SnapPlayerVehicleToGround
					SET_VEHICLE_ON_GROUND_PROPERLY(vehDebug)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - PLAYER VEHICLE SET ON GROUND PROPERLY") ENDIF #ENDIF
					bDebug_SnapPlayerVehicleToGround = FALSE
				ENDIF			
				IF bDebug_PrintPlayerVehicleInfo
					OPEN_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Position = "
						VECTOR vDebug_VehiclePos = GET_ENTITY_COORDS(vehDebug)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_VECTOR_TO_DEBUG_FILE(vDebug_VehiclePos)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						FLOAT fDebug_VehicleQuaternion_X, fDebug_VehicleQuaternion_Y, fDebug_VehicleQuaternion_Z, fDebug_VehicleQuaternion_W
						GET_ENTITY_QUATERNION(vehDebug, fDebug_VehicleQuaternion_X, fDebug_VehicleQuaternion_Y, fDebug_VehicleQuaternion_Z, fDebug_VehicleQuaternion_W)						
						tlTemp = "Vehicle Quaternion X = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_X)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion Y = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_Y)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion Z = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_Z)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion W = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_W)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						tlTemp = "Vehicle Heading = "
						FLOAT fDebug_VehicleHeading = GET_ENTITY_HEADING(vehDebug)
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleHeading)
						SAVE_NEWLINE_TO_DEBUG_FILE()						
					CLOSE_DEBUG_FILE()					
					bDebug_PrintPlayerVehicleInfo = FALSE
				ENDIF
			ENDIF			
		ENDIF		
		IF DOES_ENTITY_EXIST(TrafficCarID[iDebug_TrafficCarToTurnCollisionOffOn])
		AND NOT IS_ENTITY_DEAD(TrafficCarID[iDebug_TrafficCarToTurnCollisionOffOn])
			IF bDebug_SelectedTrafficCarHasCollision
				SET_ENTITY_COLLISION(TrafficCarID[iDebug_TrafficCarToTurnCollisionOffOn], TRUE)
			ELSE
				SET_ENTITY_COLLISION(TrafficCarID[iDebug_TrafficCarToTurnCollisionOffOn], FALSE)
			ENDIF
		ENDIF		
		IF DOES_ENTITY_EXIST(SetPieceCarID[iDebug_SetPieceCarToTurnCollisionOffOn])
		AND NOT IS_ENTITY_DEAD(SetPieceCarID[iDebug_SetPieceCarToTurnCollisionOffOn])
			IF bDebug_SelectedSetPieceCarHasCollision
				SET_ENTITY_COLLISION(SetPieceCarID[iDebug_SetPieceCarToTurnCollisionOffOn], TRUE)
			ELSE
				SET_ENTITY_COLLISION(SetPieceCarID[iDebug_SetPieceCarToTurnCollisionOffOn], FALSE)
			ENDIF
		ENDIF		
		IF bDebug_StartHospitalDebris_PTFX_ID
			IF HAS_PTFX_ASSET_LOADED()
				IF DOES_PARTICLE_FX_LOOPED_EXIST(DEBUG_HospitalDebris_PTFX_ID)
					STOP_PARTICLE_FX_LOOPED(DEBUG_HospitalDebris_PTFX_ID)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REMOVED EXISTING PARTICLE FX") ENDIF #ENDIF
				ENDIF
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(DEBUG_HospitalDebris_PTFX_ID)
					DEBUG_HospitalDebris_PTFX_ID = START_PARTICLE_FX_LOOPED_AT_COORD("scr_rcn2_ceiling_debris", << 325.0, -589.0, 45.0 >>, << 0.0, 0.0, 0.0 >>)  
					//  scr_rcn2_ceiling_debris
					//  scr_trev2_heli_wreck	ripped from another mission to check my script setup is ok.
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PTFX EFFECT HOSPITAL DEBRIS STARTED") ENDIF #ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PTFX ASSET NOT LOADED !") ENDIF #ENDIF
			ENDIF
			bDebug_StartHospitalDebris_PTFX_ID = FALSE
		ENDIF		
	ENDPROC
	
	/// PURPOSE:
	/// 	removes my debug mission widget group
	PROC CLEANUP_MISSION_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	ENDPROC
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:FUNCTIONS AND PROCEEDURES
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    contains all the data needed for the uber recording
PROC LOAD_UBER_DATA()

// ****  UBER RECORDED TRAFFIC  **** 

TrafficCarPos[0] = <<-1189.9189, -864.3419, 13.5062>>
TrafficCarQuatX[0] = 0.0110
TrafficCarQuatY[0] = -0.0057
TrafficCarQuatZ[0] = -0.4728
TrafficCarQuatW[0] = 0.8811
TrafficCarRecording[0] = 3
TrafficCarStartime[0] = 12794.0000
TrafficCarModel[0] = f620

TrafficCarPos[1] = <<-1168.0251, -846.5394, 13.8245>>
TrafficCarQuatX[1] = 0.0067
TrafficCarQuatY[1] = 0.0025
TrafficCarQuatZ[1] = 0.3507
TrafficCarQuatW[1] = 0.9364
TrafficCarRecording[1] = 4
TrafficCarStartime[1] = 12926.0000
TrafficCarModel[1] = washington

TrafficCarPos[2] = <<-1052.3594, -730.7704, 18.7290>>
TrafficCarQuatX[2] = -0.0125
TrafficCarQuatY[2] = -0.0121
TrafficCarQuatZ[2] = 0.9079
TrafficCarQuatW[2] = 0.4187
TrafficCarRecording[2] = 14
TrafficCarStartime[2] = 19594.9609
TrafficCarModel[2] = serrano

TrafficCarPos[3] = <<-1050.4532, -743.3076, 18.7718>>
TrafficCarQuatX[3] = -0.0019
TrafficCarQuatY[3] = -0.0002
TrafficCarQuatZ[3] = -0.3894
TrafficCarQuatW[3] = 0.9211
TrafficCarRecording[3] = 6
TrafficCarStartime[3] = 20400.0000
TrafficCarModel[3] = washington

//car drives up second hill outside lane (causing issue currently as doesn't always playout)
TrafficCarPos[4] = <<-1032.5537, -734.0855, 19.1223>>
TrafficCarQuatX[4] = 0.0233
TrafficCarQuatY[4] = -0.0077
TrafficCarQuatZ[4] = -0.4212
TrafficCarQuatW[4] = 0.9066
TrafficCarRecording[4] = 16
TrafficCarStartime[4] = 19960.8496 // 20460.8496 made it kick off a little ealier to hopefully get out way!
TrafficCarModel[4] = serrano

TrafficCarPos[5] = <<-865.4848, -623.9280, 28.3187>>
TrafficCarQuatX[5] = 0.0038
TrafficCarQuatY[5] = -0.0257
TrafficCarQuatZ[5] = 0.9893
TrafficCarQuatW[5] = -0.1433
TrafficCarRecording[5] = 7
TrafficCarStartime[5] = 28417.0000
TrafficCarModel[5] = washington

TrafficCarPos[6] = <<-869.6063, -612.9931, 28.5539>>
TrafficCarQuatX[6] = 0.0050
TrafficCarQuatY[6] = -0.0119
TrafficCarQuatZ[6] = 0.9777
TrafficCarQuatW[6] = -0.2094
TrafficCarRecording[6] = 8
TrafficCarStartime[6] = 28417.0000
TrafficCarModel[6] = serrano

TrafficCarPos[7] = <<-853.7338, -652.7806, 27.3348>>
TrafficCarQuatX[7] = -0.0022
TrafficCarQuatY[7] = -0.0021
TrafficCarQuatZ[7] = 0.7085
TrafficCarQuatW[7] = 0.7057
TrafficCarRecording[7] = 9
TrafficCarStartime[7] = 28549.0000
TrafficCarModel[7] = serrano

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
TrafficCarPos[8] = <<-797.3943, -667.4530, 28.1434>>
TrafficCarQuatX[8] = 0.0434
TrafficCarQuatY[8] = 0.0031
TrafficCarQuatZ[8] = -0.7039
TrafficCarQuatW[8] = 0.7090
TrafficCarRecording[8] = 10
TrafficCarStartime[8] = 30654.0000
TrafficCarModel[8] = feltzer2

TrafficCarPos[9] = <<-728.5153, -661.8074, 29.8396>>
TrafficCarQuatX[9] = -0.0017
TrafficCarQuatY[9] = 0.0032
TrafficCarQuatZ[9] = -0.7028
TrafficCarQuatW[9] = 0.7114
TrafficCarRecording[9] = 11
TrafficCarStartime[9] = 33027.0000
TrafficCarModel[9] = RapidGT

TrafficCarPos[10] = <<-595.1748, -661.9557, 31.8497>>
TrafficCarQuatX[10] = 0.0098
TrafficCarQuatY[10] = -0.0088
TrafficCarQuatZ[10] = -0.7017
TrafficCarQuatW[10] = 0.7123
TrafficCarRecording[10] = 15
TrafficCarStartime[10] = 48186.0000
TrafficCarModel[10] = sultan

TrafficCarPos[11] = <<-477.1852, -644.7173, 32.0538>>
TrafficCarQuatX[11] = -0.0087
TrafficCarQuatY[11] = 0.0318
TrafficCarQuatZ[11] = 0.0461
TrafficCarQuatW[11] = 0.9984
TrafficCarRecording[11] = 17
TrafficCarStartime[11] = 55765.8516
TrafficCarModel[11] = serrano

TrafficCarPos[12] = <<-489.0458, -666.3751, 32.3303>>
TrafficCarQuatX[12] = 0.0113
TrafficCarQuatY[12] = 0.0176
TrafficCarQuatZ[12] = -0.7063
TrafficCarQuatW[12] = 0.7076
TrafficCarRecording[12] = 23
TrafficCarStartime[12] = 58613.0000
TrafficCarModel[12] = asterope

// heading towards the player, bunny hops over seats
TrafficCarPos[13] = <<-487.5655, -744.9453, 32.6015>>
TrafficCarQuatX[13] = -0.0937
TrafficCarQuatY[13] = 0.0450
TrafficCarQuatZ[13] = 0.8793
TrafficCarQuatW[13] = 0.4648
TrafficCarRecording[13] = 19
TrafficCarStartime[13] = 60459.3203	// 59006.3203
TrafficCarModel[13] = BMX

TrafficCarPos[14] = <<-478.1905, -835.1024, 29.9746>>
TrafficCarQuatX[14] = -0.0021
TrafficCarQuatY[14] = -0.0021
TrafficCarQuatZ[14] = 0.7008
TrafficCarQuatW[14] = 0.7134
TrafficCarRecording[14] = 18
TrafficCarStartime[14] = 64923.8516
TrafficCarModel[14] = serrano

TrafficCarPos[15] = <<-430.6053, -816.6718, 36.8249>>
TrafficCarQuatX[15] = -0.0001
TrafficCarQuatY[15] = 0.0048
TrafficCarQuatZ[15] = 0.9993
TrafficCarQuatW[15] = 0.0373
TrafficCarRecording[15] = 28
TrafficCarStartime[15] = 68551.0000
TrafficCarModel[15] = asterope

TrafficCarPos[16] = <<-427.4086, -762.0120, 36.6573>>
TrafficCarQuatX[16] = 0.0001
TrafficCarQuatY[16] = 0.0049
TrafficCarQuatZ[16] = 1.0000
TrafficCarQuatW[16] = 0.0047
TrafficCarRecording[16] = 30
TrafficCarStartime[16] = 70333.0000
TrafficCarModel[16] = sultan

//target cuts across in front of this vehicle on carpark exit
TrafficCarPos[17] = <<-476.9606, -664.9507, 31.9901>>
TrafficCarQuatX[17] = -0.0030
TrafficCarQuatY[17] = -0.0083
TrafficCarQuatZ[17] = 0.7264
TrafficCarQuatW[17] = -0.6872
TrafficCarRecording[17] = 70
TrafficCarStartime[17] = 77000.0000
TrafficCarModel[17] = sultan

//on carpark exit goes across front of target
TrafficCarPos[18] = <<-436.3420, -650.5261, 30.4239>>
TrafficCarQuatX[18] = 0.0056
TrafficCarQuatY[18] = 0.0224
TrafficCarQuatZ[18] = 0.7071
TrafficCarQuatW[18] = 0.7068
TrafficCarRecording[18] = 13
TrafficCarStartime[18] = 77590.0000
TrafficCarModel[18] = sultan

//third vehicle to go across exit from carpark
TrafficCarPos[19] = <<-420.9600, -656.3897, 30.5754>>
TrafficCarQuatX[19] = 0.0112
TrafficCarQuatY[19] = 0.0112
TrafficCarQuatZ[19] = 0.7070
TrafficCarQuatW[19] = 0.7070
TrafficCarRecording[19] = 12
TrafficCarStartime[19] = 78000.0000
TrafficCarModel[19] = Packer

TrafficCarPos[20] = <<-455.2408, -535.8500, 24.7102>>
TrafficCarQuatX[20] = -0.0022
TrafficCarQuatY[20] = 0.0022
TrafficCarQuatZ[20] = -0.7070
TrafficCarQuatW[20] = 0.7072
TrafficCarRecording[20] = 31
TrafficCarStartime[20] = 80849.0000
TrafficCarModel[20] = sultan

TrafficCarPos[21] = <<-421.8391, -535.8503, 24.7099>>
TrafficCarQuatX[21] = -0.0024
TrafficCarQuatY[21] = 0.0024
TrafficCarQuatZ[21] = -0.7071
TrafficCarQuatW[21] = 0.7071
TrafficCarRecording[21] = 32
TrafficCarStartime[21] = 80849.0000
TrafficCarModel[21] = sultan

TrafficCarPos[22] = <<-323.2696, -519.5041, 24.7690>>
TrafficCarQuatX[22] = -0.0086
TrafficCarQuatY[22] = -0.0166
TrafficCarQuatZ[22] = -0.7001
TrafficCarQuatW[22] = 0.7138
TrafficCarRecording[22] = 34
TrafficCarStartime[22] = 85280.0000
TrafficCarModel[22] = asterope

TrafficCarPos[23] = <<-384.7831, -496.5516, 24.8060>>
TrafficCarQuatX[23] = -0.0022
TrafficCarQuatY[23] = -0.0022
TrafficCarQuatZ[23] = 0.7072
TrafficCarQuatW[23] = 0.7071
TrafficCarRecording[23] = 33
TrafficCarStartime[23] = 85722.0000
TrafficCarModel[23] = landstalker

TrafficCarPos[24] = <<-299.8336, -519.6835, 24.8040>>
TrafficCarQuatX[24] = -0.0071
TrafficCarQuatY[24] = -0.0166
TrafficCarQuatZ[24] = -0.7047
TrafficCarQuatW[24] = 0.7093
TrafficCarRecording[24] = 35
TrafficCarStartime[24] = 86195.0000
TrafficCarModel[24] = asterope

// highway vehicle
TrafficCarPos[25] = <<-296.6222, -496.0120, 24.8684>>
TrafficCarQuatX[25] = -0.0026
TrafficCarQuatY[25] = -0.0022
TrafficCarQuatZ[25] = 0.7084
TrafficCarQuatW[25] = 0.7058
TrafficCarRecording[25] = 21
TrafficCarStartime[25] = 87170.8516
TrafficCarModel[25] = landstalker

TrafficCarPos[26] = <<-242.5362, -502.0159, 25.6451>>
TrafficCarQuatX[26] = -0.0096
TrafficCarQuatY[26] = -0.0094
TrafficCarQuatZ[26] = 0.7194
TrafficCarQuatW[26] = 0.6945
TrafficCarRecording[26] = 36
TrafficCarStartime[26] = 88537.0000
TrafficCarModel[26] = landstalker

TrafficCarPos[27] = <<-234.2082, -530.1547, 25.9344>>
TrafficCarQuatX[27] = -0.0058
TrafficCarQuatY[27] = 0.0064
TrafficCarQuatZ[27] = 0.7091
TrafficCarQuatW[27] = -0.7050
TrafficCarRecording[27] = 39
TrafficCarStartime[27] = 90066.0000
TrafficCarModel[27] = RapidGT

TrafficCarPos[28] = <<-204.8521, -534.9627, 26.6094>>
TrafficCarQuatX[28] = 0.0281
TrafficCarQuatY[28] = 0.0002
TrafficCarQuatZ[28] = -0.6936
TrafficCarQuatW[28] = 0.7198
TrafficCarRecording[28] = 37
TrafficCarStartime[28] = 91140.0000
TrafficCarModel[28] = asterope

TrafficCarPos[29] = <<-186.2448, -518.6406, 27.3221>>
TrafficCarQuatX[29] = -0.0061
TrafficCarQuatY[29] = -0.0197
TrafficCarQuatZ[29] = -0.7053
TrafficCarQuatW[29] = 0.7086
TrafficCarRecording[29] = 41
TrafficCarStartime[29] = 91890.0000
TrafficCarModel[29] = fq2

TrafficCarPos[30] = <<-181.6259, -523.8001, 27.2923>>
TrafficCarQuatX[30] = 0.0116
TrafficCarQuatY[30] = -0.0124
TrafficCarQuatZ[30] = -0.7029
TrafficCarQuatW[30] = 0.7111
TrafficCarRecording[30] = 38
TrafficCarStartime[30] = 92230.0000
TrafficCarModel[30] = sultan

// highway car
TrafficCarPos[31] = <<-152.8287, -489.4706, 28.3269>>
TrafficCarQuatX[31] = -0.0213
TrafficCarQuatY[31] = 0.0018
TrafficCarQuatZ[31] = 0.7254
TrafficCarQuatW[31] = 0.6880
TrafficCarRecording[31] = 24
TrafficCarStartime[31] = 92554.8516
TrafficCarModel[31] = landstalker

TrafficCarPos[32] = <<-172.1277, -500.8638, 27.8326>>
TrafficCarQuatX[32] = -0.0252
TrafficCarQuatY[32] = -0.0296
TrafficCarQuatZ[32] = 0.7083
TrafficCarQuatW[32] = 0.7048
TrafficCarRecording[32] = 40
TrafficCarStartime[32] = 94528.0000
TrafficCarModel[32] = fq2

//highway vehicle
TrafficCarPos[33] = <<-114.8660, -500.2346, 29.8111>>
TrafficCarQuatX[33] = -0.0104
TrafficCarQuatY[33] = -0.0098
TrafficCarQuatZ[33] = 0.7021
TrafficCarQuatW[33] = 0.7119
TrafficCarRecording[33] = 78
TrafficCarStartime[33] = 95865.4375
TrafficCarModel[33] = asterope

TrafficCarPos[34] = <<-129.1858, -505.6940, 29.2420>>
TrafficCarQuatX[34] = -0.0150
TrafficCarQuatY[34] = -0.0163
TrafficCarQuatZ[34] = 0.7139
TrafficCarQuatW[34] = 0.6999
TrafficCarRecording[34] = 42
TrafficCarStartime[34] = 95886.0000
TrafficCarModel[34] = asterope

//highway
TrafficCarPos[35] = <<-55.6079, -499.5939, 31.8493>>
TrafficCarQuatX[35] = -0.0116
TrafficCarQuatY[35] = -0.0112
TrafficCarQuatZ[35] = 0.7134
TrafficCarQuatW[35] = 0.7006
TrafficCarRecording[35] = 25
TrafficCarStartime[35] = 96787.8516
TrafficCarModel[35] = landstalker

TrafficCarPos[36] = <<-87.1120, -504.9880, 30.9077>>
TrafficCarQuatX[36] = -0.0159
TrafficCarQuatY[36] = -0.0149
TrafficCarQuatZ[36] = 0.7118
TrafficCarQuatW[36] = 0.7020
TrafficCarRecording[36] = 43
TrafficCarStartime[36] = 97140.0000
TrafficCarModel[36] = fq2

TrafficCarPos[37] = <<26.5103, -499.3918, 34.2088>>
TrafficCarQuatX[37] = -0.0005
TrafficCarQuatY[37] = -0.0002
TrafficCarQuatZ[37] = 0.7186
TrafficCarQuatW[37] = 0.6954
TrafficCarRecording[37] = 79
TrafficCarStartime[37] = 100001.3984
TrafficCarModel[37] = Packer

/* drives up slip road ahead of target.
commented out because he now slams the brakes on in front of the player
TrafficCarPos[38] = <<39.6029, -533.6916, 33.5574>>
TrafficCarQuatX[38] = -0.0111
TrafficCarQuatY[38] = 0.0144
TrafficCarQuatZ[38] = 0.7909
TrafficCarQuatW[38] = -0.6116
TrafficCarRecording[38] = 45
TrafficCarStartime[38] = 101958.0000
TrafficCarModel[38] = bati
*/

//more than likely needs a re-record as comet model has been removed from game!
TrafficCarPos[39] = <<70.5888, -526.8503, 33.7031>>
TrafficCarQuatX[39] = -0.0004
TrafficCarQuatY[39] = -0.0004
TrafficCarQuatZ[39] = -0.7039
TrafficCarQuatW[39] = 0.7103
TrafficCarRecording[39] = 46
TrafficCarStartime[39] = 102948.0000
TrafficCarModel[39] = ninef2

TrafficCarPos[40] = <<99.8609, -521.5526, 33.5624>>
TrafficCarQuatX[40] = 0.0145
TrafficCarQuatY[40] = -0.0145
TrafficCarQuatZ[40] = 0.7070
TrafficCarQuatW[40] = -0.7069
TrafficCarRecording[40] = 47
TrafficCarStartime[40] = 104004.0000
TrafficCarModel[40] = landstalker

TrafficCarPos[41] = <<196.1974, -526.9512, 33.5498>>
TrafficCarQuatX[41] = 0.0178
TrafficCarQuatY[41] = -0.0178
TrafficCarQuatZ[41] = 0.7069
TrafficCarQuatW[41] = -0.7068
TrafficCarRecording[41] = 48
TrafficCarStartime[41] = 107502.0000
TrafficCarModel[41] = landstalker

TrafficCarPos[42] = <<230.2825, -516.1511, 33.5591>>
TrafficCarQuatX[42] = -0.0023
TrafficCarQuatY[42] = 0.0023
TrafficCarQuatZ[42] = -0.7071
TrafficCarQuatW[42] = 0.7071
TrafficCarRecording[42] = 49
TrafficCarStartime[42] = 108624.0000
TrafficCarModel[42] = landstalker

/*// car on slip road next to hospital  FLOATS!!!
TrafficCarPos[43] = <<304.4125, -550.3855, 43.4028>>
TrafficCarQuatX[43] = -0.0250
TrafficCarQuatY[43] = -0.0155
TrafficCarQuatZ[43] = 0.7199
TrafficCarQuatW[43] = -0.6934
TrafficCarRecording[43] = 26
TrafficCarStartime[43] = 110670.8516
TrafficCarModel[43] = asterope*/

TrafficCarPos[44] = <<301.1906, -521.5500, 33.4621>>
TrafficCarQuatX[44] = 0.0001
TrafficCarQuatY[44] = -0.0001
TrafficCarQuatZ[44] = 0.7071
TrafficCarQuatW[44] = -0.7071
TrafficCarRecording[44] = 50
TrafficCarStartime[44] = 111198.0000
TrafficCarModel[44] = sultan

// ****  UBER RECORDED PARKED CARS  **** 

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[0] = <<-1200.0914, -849.0596, 13.5431>>
ParkedCarQuatX[0] = -0.0171
ParkedCarQuatY[0] = -0.0063
ParkedCarQuatZ[0] = 0.8800
ParkedCarQuatW[0] = 0.4747
ParkedCarModel[0] = feltzer2

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[1] = <<-1058.3451, -772.2757, 18.8924>>
ParkedCarQuatX[1] = -0.0028
ParkedCarQuatY[1] = 0.0149
ParkedCarQuatZ[1] = 0.3553
ParkedCarQuatW[1] = 0.9346
ParkedCarModel[1] = feltzer2

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[2] = <<-1070.6639, -739.6081, 18.8482>>
ParkedCarQuatX[2] = -0.0111
ParkedCarQuatY[2] = 0.0192
ParkedCarQuatZ[2] = 0.9254
ParkedCarQuatW[2] = 0.3784
ParkedCarModel[2] = feltzer2

ParkedCarPos[3] = <<-1121.0957, -880.1656, 7.6752>>
ParkedCarQuatX[3] = 0.0021
ParkedCarQuatY[3] = -0.0011
ParkedCarQuatZ[3] = -0.4944
ParkedCarQuatW[3] = 0.8692
ParkedCarModel[3] = serrano

ParkedCarPos[4] = <<-1115.6182, -807.3134, 16.6315>>
ParkedCarQuatX[4] = -0.0414
ParkedCarQuatY[4] = -0.0257
ParkedCarQuatZ[4] = 0.8471
ParkedCarQuatW[4] = -0.5292
ParkedCarModel[4] = serrano

ParkedCarPos[5] = <<-1112.4027, -774.3158, 18.3801>>
ParkedCarQuatX[5] = -0.0097
ParkedCarQuatY[5] = -0.0319
ParkedCarQuatZ[5] = 0.5058
ParkedCarQuatW[5] = 0.8620
ParkedCarModel[5] = serrano

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[6] = <<-1130.5292, -792.3233, 16.5972>>
ParkedCarQuatX[6] = -0.0307
ParkedCarQuatY[6] = -0.0293
ParkedCarQuatZ[6] = 0.9133
ParkedCarQuatW[6] = 0.4051
ParkedCarModel[6] = feltzer2

ParkedCarPos[7] = <<-1045.3898, -778.9995, 18.3863>>
ParkedCarQuatX[7] = 0.0118
ParkedCarQuatY[7] = 0.0370
ParkedCarQuatZ[7] = 0.5106
ParkedCarQuatW[7] = 0.8590
ParkedCarModel[7] = serrano

ParkedCarPos[8] = <<-1034.8263, -739.7039, 18.8810>>
ParkedCarQuatX[8] = 0.0195
ParkedCarQuatY[8] = 0.0190
ParkedCarQuatZ[8] = -0.4353
ParkedCarQuatW[8] = 0.8999
ParkedCarModel[8] = radi

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[9] = <<-1033.9292, -712.6562, 19.4857>>
ParkedCarQuatX[9] = -0.0193
ParkedCarQuatY[9] = -0.0194
ParkedCarQuatZ[9] = 0.9056
ParkedCarQuatW[9] = 0.4231
ParkedCarModel[9] = serrano

ParkedCarPos[10] = <<-1005.5381, -715.5715, 20.3848>>
ParkedCarQuatX[10] = -0.0440
ParkedCarQuatY[10] = -0.1329
ParkedCarQuatZ[10] = -0.3895
ParkedCarQuatW[10] = 0.9103
ParkedCarModel[10] = bati

//!!! Pos and quaternion will need update as vehicle model had to change, cogsport removed from the game !!!
ParkedCarPos[11] = <<-980.4462, -670.3936, 22.8034>>
ParkedCarQuatX[11] = -0.0432
ParkedCarQuatY[11] = -0.0121
ParkedCarQuatZ[11] = 0.8701
ParkedCarQuatW[11] = 0.4908
ParkedCarModel[11] = washington

ParkedCarPos[12] = <<-969.1606, -664.5714, 23.8870>>
ParkedCarQuatX[12] = -0.0460
ParkedCarQuatY[12] = -0.0213
ParkedCarQuatZ[12] = 0.8534
ParkedCarQuatW[12] = 0.5187
ParkedCarModel[12] = RapidGT

ParkedCarPos[13] = <<-928.2644, -652.7425, 26.6901>>
ParkedCarQuatX[13] = -0.0303
ParkedCarQuatY[13] = -0.0054
ParkedCarQuatZ[13] = 0.7573
ParkedCarQuatW[13] = 0.6524
ParkedCarModel[13] = RapidGT

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
ParkedCarPos[14] = <<-916.6693, -650.6878, 27.0863>>
ParkedCarQuatX[14] = -0.0240
ParkedCarQuatY[14] = 0.0101
ParkedCarQuatZ[14] = 0.7742
ParkedCarQuatW[14] = 0.6324
ParkedCarModel[14] = feltzer2

ParkedCarPos[15] = <<-790.8272, -667.5106, 28.4613>>
ParkedCarQuatX[15] = -0.0369
ParkedCarQuatY[15] = -0.0050
ParkedCarQuatZ[15] = 0.7209
ParkedCarQuatW[15] = -0.6921
ParkedCarModel[15] = RapidGT

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
ParkedCarPos[16] = <<-790.0645, -647.0408, 28.4882>>
ParkedCarQuatX[16] = -0.0370
ParkedCarQuatY[16] = 0.0003
ParkedCarQuatZ[16] = 0.7201
ParkedCarQuatW[16] = 0.6929
ParkedCarModel[16] = feltzer2

ParkedCarPos[17] = <<-716.8614, -667.7233, 29.6724>>
ParkedCarQuatX[17] = -0.0267
ParkedCarQuatY[17] = -0.0106
ParkedCarQuatZ[17] = 0.7068
ParkedCarQuatW[17] = -0.7068
ParkedCarModel[17] = sultan

ParkedCarPos[18] = <<-653.5672, -605.7774, 32.8442>>
ParkedCarQuatX[18] = 0.0008
ParkedCarQuatY[18] = -0.0298
ParkedCarQuatZ[18] = 0.9991
ParkedCarQuatW[18] = 0.0310
ParkedCarModel[18] = radi

ParkedCarPos[19] = <<-598.4409, -647.7642, 31.2207>>
ParkedCarQuatX[19] = 0.0659
ParkedCarQuatY[19] = -0.1257
ParkedCarQuatZ[19] = 0.6417
ParkedCarQuatW[19] = 0.7537
ParkedCarModel[19] = bati

//inside carpark
ParkedCarPos[20] = <<-457.3376, -774.9608, 29.9679>>
ParkedCarQuatX[20] = -0.0002
ParkedCarQuatY[20] = 0.0000
ParkedCarQuatZ[20] = 0.7033
ParkedCarQuatW[20] = 0.7109
ParkedCarModel[20] = sultan

//car park exit from tunnel
ParkedCarPos[21] = <<-506.0643, -614.7634, 29.6699>>
ParkedCarQuatX[21] = -0.0131
ParkedCarQuatY[21] = -0.1302
ParkedCarQuatZ[21] = -0.0218
ParkedCarQuatW[21] = 0.9912
ParkedCarModel[21] = bati

ParkedCarPos[22] = <<-487.6026, -614.8040, 30.5800>>
ParkedCarQuatX[22] = 0.0002
ParkedCarQuatY[22] = 0.0000
ParkedCarQuatZ[22] = 0.9999
ParkedCarQuatW[22] = 0.0166
ParkedCarModel[22] = sultan

//parked at junction before turn for tunnel
ParkedCarPos[23] = <<-856.7647, -679.0630, 27.2491>>
ParkedCarQuatX[23] = -0.0080
ParkedCarQuatY[23] = 0.0277
ParkedCarQuatZ[23] = 0.0148
ParkedCarQuatW[23] = 0.9995
ParkedCarModel[23] = sultan

//on road approach to tunnel
ParkedCarPos[24] = <<-750.8827, -607.8928, 29.5466>>
ParkedCarQuatX[24] = 0.0609
ParkedCarQuatY[24] = 0.0021
ParkedCarQuatZ[24] = 0.9978
ParkedCarQuatW[24] = -0.0261
ParkedCarModel[24] = sultan

//inside car park
ParkedCarPos[25] = <<-477.1844, -757.5135, 30.1527>>
ParkedCarQuatX[25] = -0.0022
ParkedCarQuatY[25] = -0.0021
ParkedCarQuatZ[25] = 0.7233
ParkedCarQuatW[25] = 0.6905
ParkedCarModel[25] = RapidGT

//parked next to tunnel exit area
ParkedCarPos[26] = <<-471.0100, -624.5500, 30.5800>>
ParkedCarQuatX[26] = 0.0001
ParkedCarQuatY[26] = -0.0002
ParkedCarQuatZ[26] = -0.0144
ParkedCarQuatW[26] = 0.9999
ParkedCarModel[26] = sultan

//underground tunnel carpark
ParkedCarPos[27] = <<-679.3469, -593.3812, 24.7059>>
ParkedCarQuatX[27] = -0.0011
ParkedCarQuatY[27] = 0.0004
ParkedCarQuatZ[27] = 0.6772
ParkedCarQuatW[27] = 0.7358
ParkedCarModel[27] = sultan

//end area
ParkedCarPos[28] = <<433.6576, -608.7065, 27.7732>>
ParkedCarQuatX[28] = -0.0003
ParkedCarQuatY[28] = 0.0005
ParkedCarQuatZ[28] = 0.7774
ParkedCarQuatW[28] = -0.6290
ParkedCarModel[28] = sultan

//end area
ParkedCarPos[29] = <<416.3534, -638.7431, 27.8704>>
ParkedCarQuatX[29] = 0.0891
ParkedCarQuatY[29] = -0.0957
ParkedCarQuatZ[29] = 0.7111
ParkedCarQuatW[29] = 0.6908
ParkedCarModel[29] = bati

//end area
ParkedCarPos[30] = <<433.9501, -603.2841, 27.8743>>
ParkedCarQuatX[30] = 0.0794
ParkedCarQuatY[30] = -0.1039
ParkedCarQuatZ[30] = 0.6505
ParkedCarQuatW[30] = 0.7482
ParkedCarModel[30] = bati

//Ambulance outside hospital entrance
ParkedCarPos[31] = <<291.0103, -589.8865, 42.9593>>
ParkedCarQuatX[31] = 0.0053
ParkedCarQuatY[31] = -0.0026
ParkedCarQuatZ[31] = 0.9489
ParkedCarQuatW[31] = 0.3156
ParkedCarModel[31] = AMBULANCE

// ****  UBER RECORDED SET PIECE CARS  **** 

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now    
//may need to remain here as could drop out as traffic vehicle
SetPieceCarPos[0] = <<-1104.2826, -775.1389, 18.8468>>
SetPieceCarQuatX[0] = -0.0068
SetPieceCarQuatY[0] = -0.0121
SetPieceCarQuatZ[0] = 0.9186
SetPieceCarQuatW[0] = 0.3949
SetPieceCarRecording[0] = 52
SetPieceCarStartime[0] = 16000.0000
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = feltzer2

//set here in hope it starts playing
SetPieceCarPos[1] = <<-1129.4855, -805.7412, 15.9592>>
SetPieceCarQuatX[1] = 0.0303
SetPieceCarQuatY[1] = -0.0208
SetPieceCarQuatZ[1] = -0.3298
SetPieceCarQuatW[1] = 0.9433
SetPieceCarRecording[1] = 5
SetPieceCarStartime[1] = 16488.0000
SetPieceCarRecordingSpeed[1] = 1.0000
SetPieceCarModel[1] = washington

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
//incidental vehicle can be converted back to traffic eventually
SetPieceCarPos[2] = <<-998.2445, -692.8199, 21.5328>>
SetPieceCarQuatX[2] = -0.0136
SetPieceCarQuatY[2] = -0.0158
SetPieceCarQuatZ[2] = 0.8929
SetPieceCarQuatW[2] = 0.4498
SetPieceCarRecording[2] = 53
SetPieceCarStartime[2] = 21412.0000
SetPieceCarRecordingSpeed[2] = 1.0000
SetPieceCarModel[2] = feltzer2

//!!! Pos and quaternion will need update as vehicle model had to change, FELTZER2010 removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
//vehicle swerves in front of target as he cuts across the wrong side of the road, in the lead up to the tunnel
SetPieceCarPos[3] = <<-883.8546, -655.3506, 27.5493>>
SetPieceCarQuatX[3] = 0.0032
SetPieceCarQuatY[3] = -0.0018
SetPieceCarQuatZ[3] = 0.7120
SetPieceCarQuatW[3] = 0.7021
SetPieceCarRecording[3] = 54
SetPieceCarStartime[3] = 25000.0000
SetPieceCarRecordingSpeed[3] = 1.0000
SetPieceCarModel[3] = feltzer2

//incidental vehicle can be made traffic eventually
SetPieceCarPos[4] = <<-850.7409, -661.5627, 27.3668>>
SetPieceCarQuatX[4] = -0.0015
SetPieceCarQuatY[4] = 0.0017
SetPieceCarQuatZ[4] = -0.6912
SetPieceCarQuatW[4] = 0.7227
SetPieceCarRecording[4] = 55
SetPieceCarStartime[4] = 28500.0000
SetPieceCarRecordingSpeed[4] = 1.0000
SetPieceCarModel[4] = serrano

//vehicle swerves in front of target as he turns left to head for the tunnel
SetPieceCarPos[5] = <<-721.2928, -652.8879, 29.8160>>
SetPieceCarQuatX[5] = -0.0024
SetPieceCarQuatY[5] = -0.0102
SetPieceCarQuatZ[5] = 0.6908
SetPieceCarQuatW[5] = 0.7230
SetPieceCarRecording[5] = 56
SetPieceCarStartime[5] = 32500.0000
SetPieceCarRecordingSpeed[5] = 1.0000
SetPieceCarModel[5] = serrano

// can be converted back to traffic
SetPieceCarPos[6] = <<-724.7061, -648.6400, 29.7668>>
SetPieceCarQuatX[6] = -0.0212
SetPieceCarQuatY[6] = 0.0050
SetPieceCarQuatZ[6] = 0.7061
SetPieceCarQuatW[6] = 0.7078
SetPieceCarRecording[6] = 57
SetPieceCarStartime[6] = 32750.0000
SetPieceCarRecordingSpeed[6] = 1.0000
SetPieceCarModel[6] = washington

//vehicle driving along tunnel same direction as target
SetPieceCarPos[7] = <<-688.4173, -579.6989, 24.8499>>
SetPieceCarQuatX[7] = 0.0026
SetPieceCarQuatY[7] = -0.0029
SetPieceCarQuatZ[7] = 0.7397
SetPieceCarQuatW[7] = -0.6730
SetPieceCarRecording[7] = 58
SetPieceCarStartime[7] = 40273.0000
SetPieceCarRecordingSpeed[7] = 1.0000
SetPieceCarModel[7] = washington

//vehicle down tunnel coming towards target
SetPieceCarPos[8] = <<-563.1786, -579.4122, 25.3124>>
SetPieceCarQuatX[8] = 0.0020
SetPieceCarQuatY[8] = 0.0021
SetPieceCarQuatZ[8] = 0.7032
SetPieceCarQuatW[8] = 0.7110
SetPieceCarRecording[8] = 59
SetPieceCarStartime[8] = 43500.0000
SetPieceCarRecordingSpeed[8] = 1.0000
SetPieceCarModel[8] = Trash

//bmx rider who will eventually be performing bunny hops / tricks
SetPieceCarPos[9] = <<-556.1506, -631.2017, 30.5620>>
SetPieceCarQuatX[9] = -0.0352
SetPieceCarQuatY[9] = 0.0378
SetPieceCarQuatZ[9] = -0.6805
SetPieceCarQuatW[9] = 0.7309
SetPieceCarRecording[9] = 60
SetPieceCarStartime[9] = 51889.0000
SetPieceCarRecordingSpeed[9] = 1.0000
SetPieceCarModel[9] = BMX

//vehicle screeches to a halt as target cuts across road on tunnel exit
SetPieceCarPos[10] = <<-445.0543, -655.1606, 31.3466>>
SetPieceCarQuatX[10] = 0.0076
SetPieceCarQuatY[10] = 0.0093
SetPieceCarQuatZ[10] = 0.7136
SetPieceCarQuatW[10] = 0.7005
SetPieceCarRecording[10] = 61
SetPieceCarStartime[10] = 56772.0000
SetPieceCarRecordingSpeed[10] = 1.0000
SetPieceCarModel[10] = sultan

//second vehicle to swerve where target cuts across road from tunnel
SetPieceCarPos[11] = <<-519.4122, -666.3391, 32.7365>>
SetPieceCarQuatX[11] = 0.0105
SetPieceCarQuatY[11] = 0.0179
SetPieceCarQuatZ[11] = -0.6817
SetPieceCarQuatW[11] = 0.7313
SetPieceCarRecording[11] = 62
SetPieceCarStartime[11] = 57772.0000
SetPieceCarRecordingSpeed[11] = 1.0000
SetPieceCarModel[11] = washington

//bmx kid jump down stairs
SetPieceCarPos[12] = <<-491.8959, -716.5375, 32.5585>>
SetPieceCarQuatX[12] = 0.1433
SetPieceCarQuatY[12] = 0.0787
SetPieceCarQuatZ[12] = 0.9609
SetPieceCarQuatW[12] = -0.2234
SetPieceCarRecording[12] = 63
SetPieceCarStartime[12] = 60800.00	// 60084.00
SetPieceCarRecordingSpeed[12] = 1.2000
SetPieceCarModel[12] = BMX

SetPieceCarPos[13] = <<-492.3147, -827.8944, 29.9952>>
SetPieceCarQuatX[13] = -0.0050
SetPieceCarQuatY[13] = 0.0046
SetPieceCarQuatZ[13] = 0.0169
SetPieceCarQuatW[13] = 0.9998
SetPieceCarRecording[13] = 64
SetPieceCarStartime[13] = 64030.0000
SetPieceCarRecordingSpeed[13] = 1.0000
SetPieceCarModel[13] = washington

//to be converted to traffic car
SetPieceCarPos[14] = <<-518.8372, -756.7956, 31.5421>>
SetPieceCarQuatX[14] = -0.0147
SetPieceCarQuatY[14] = -0.0264
SetPieceCarQuatZ[14] = 0.9686
SetPieceCarQuatW[14] = -0.2467
SetPieceCarRecording[14] = 65
SetPieceCarStartime[14] = 64230.0000
SetPieceCarRecordingSpeed[14] = 1.0000
SetPieceCarModel[14] = washington

//vehicle coming out carpark, blocking other entrance
SetPieceCarPos[15] = <<-477.6166, -806.7344, 30.0523>>
SetPieceCarQuatX[15] = 0.0000
SetPieceCarQuatY[15] = 0.0025
SetPieceCarQuatZ[15] = -0.7010
SetPieceCarQuatW[15] = 0.7132
SetPieceCarRecording[15] = 66
SetPieceCarStartime[15] = 66084.0000
SetPieceCarRecordingSpeed[15] = 1.0000
SetPieceCarModel[15] = washington

//truck skiding as target cuts back into carpark
SetPieceCarPos[16] = <<-417.4774, -836.1498, 31.4420>>
SetPieceCarQuatX[16] = -0.0077
SetPieceCarQuatY[16] = -0.0047
SetPieceCarQuatZ[16] = 0.7025
SetPieceCarQuatW[16] = 0.7117
SetPieceCarRecording[16] = 67
SetPieceCarStartime[16] = 65384.0000
SetPieceCarRecordingSpeed[16] = 1.0000
SetPieceCarModel[16] = Benson

//second vehicle to skid at carpark entrance
SetPieceCarPos[17] = <<-413.1984, -831.7321, 30.8216>>
SetPieceCarQuatX[17] = -0.0237
SetPieceCarQuatY[17] = 0.0144
SetPieceCarQuatZ[17] = 0.7088
SetPieceCarQuatW[17] = 0.7049
SetPieceCarRecording[17] = 68
SetPieceCarStartime[17] = 67078.0000
SetPieceCarRecordingSpeed[17] = 1.0000
SetPieceCarModel[17] = sultan

//follows bus towards carpark turn
SetPieceCarPos[18] = <<-528.5989, -840.7145, 29.4130>>
SetPieceCarQuatX[18] = -0.0237
SetPieceCarQuatY[18] = 0.0236
SetPieceCarQuatZ[18] = 0.7098
SetPieceCarQuatW[18] = -0.7036
SetPieceCarRecording[18] = 69
SetPieceCarStartime[18] = 64606.3203
SetPieceCarRecordingSpeed[18] = 1.0000
SetPieceCarModel[18] = sultan

//vehicle in the carpark with lights on
SetPieceCarPos[19] = <<-446.0574, -767.6522, 29.9676>>
SetPieceCarQuatX[19] = -0.0002
SetPieceCarQuatY[19] = 0.0001
SetPieceCarQuatZ[19] = 0.7110
SetPieceCarQuatW[19] = 0.7032
SetPieceCarRecording[19] = 51
SetPieceCarStartime[19] = 70071.8516
SetPieceCarRecordingSpeed[19] = 1.0000
SetPieceCarModel[19] = sultan

//first car to react on the highway as target gets on
SetPieceCarPos[20] = <<-488.5203, -523.8471, 24.8363>>
SetPieceCarQuatX[20] = -0.0010
SetPieceCarQuatY[20] = 0.0015
SetPieceCarQuatZ[20] = 0.7208
SetPieceCarQuatW[20] = -0.6931
SetPieceCarRecording[20] = 71
SetPieceCarStartime[20] = 81355.8516
SetPieceCarRecordingSpeed[20] = 1.0000
SetPieceCarModel[20] = landstalker

//landstalker on highway driving normally towards target (whilst on opposite side)
SetPieceCarPos[21] = <<-305.7855, -508.7270, 24.7583>>
SetPieceCarQuatX[21] = 0.0168
SetPieceCarQuatY[21] = -0.0137
SetPieceCarQuatZ[21] = 0.7019
SetPieceCarQuatW[21] = 0.7119
SetPieceCarRecording[21] = 72
SetPieceCarStartime[21] = 86290.8516
SetPieceCarRecordingSpeed[21] = 1.0000
SetPieceCarModel[21] = landstalker

//Ambulance coming out hospital
SetPieceCarPos[22] = <<294.6554, -576.5358, 42.9547>>
SetPieceCarQuatX[22] = 0.0001
SetPieceCarQuatY[22] = 0.0078
SetPieceCarQuatZ[22] = 0.2262
SetPieceCarQuatW[22] = 0.9740
SetPieceCarRecording[22] = 73
SetPieceCarStartime[22] = 109000.0	//576.0000
SetPieceCarRecordingSpeed[22] = 1.0000
SetPieceCarModel[22] = AMBULANCE

//truck target cuts in front of on highway
SetPieceCarPos[23] = <<-393.8603, -503.4236, 25.4432>>
SetPieceCarQuatX[23] = 0.0016
SetPieceCarQuatY[23] = 0.0018
SetPieceCarQuatZ[23] = 0.7075
SetPieceCarQuatW[23] = 0.7067
SetPieceCarRecording[23] = 74
SetPieceCarStartime[23] = 82431.8516
SetPieceCarRecordingSpeed[23] = 1.0000
SetPieceCarModel[23] = Packer

//bus guides player to the left just before full turn back into car park
SetPieceCarPos[24] = <<-527.6368, -846.5891, 29.9361>>
SetPieceCarQuatX[24] = 0.0422
SetPieceCarQuatY[24] = -0.0031
SetPieceCarQuatZ[24] = -0.7009
SetPieceCarQuatW[24] = 0.7120
SetPieceCarRecording[24] = 75
SetPieceCarStartime[24] = 62500.0	//63000.0000
SetPieceCarRecordingSpeed[24] = 1.0000
SetPieceCarModel[24] = bus

//car target swerves around back off when exiting highway onto slip road.
SetPieceCarPos[25] = <<3.4457, -533.8352, 33.2443>>
SetPieceCarQuatX[25] = 0.0232
SetPieceCarQuatY[25] = 0.0045
SetPieceCarQuatZ[25] = -0.6929
SetPieceCarQuatW[25] = 0.7206
SetPieceCarRecording[25] = 80
SetPieceCarStartime[25] = 100001.3984
SetPieceCarRecordingSpeed[25] = 1.0000
SetPieceCarModel[25] = asterope

//moved here in hope it appears from traffic section
SetPieceCarPos[26] = <<-483.5551, -531.6314, 24.8571>>
SetPieceCarQuatX[26] = 0.0034
SetPieceCarQuatY[26] = -0.0031
SetPieceCarQuatZ[26] = -0.6972
SetPieceCarQuatW[26] = 0.7168
SetPieceCarRecording[26] = 76
SetPieceCarStartime[26] = 81217.4375
SetPieceCarRecordingSpeed[26] = 1.0000
SetPieceCarModel[26] = asterope

//moved here in hope it appears from traffic section. Highway
SetPieceCarPos[27] = <<-277.6243, -508.3559, 24.9767>>
SetPieceCarQuatX[27] = 0.0108
SetPieceCarQuatY[27] = -0.0139
SetPieceCarQuatZ[27] = 0.7117
SetPieceCarQuatW[27] = 0.7023
SetPieceCarRecording[27] = 77
SetPieceCarStartime[27] = 87256.4375
SetPieceCarRecordingSpeed[27] = 1.0000
SetPieceCarModel[27] = asterope

//first truck on highway where target reaches wrong side of the road
SetPieceCarPos[28] = <<-201.7354, -496.1847, 27.3531>>
SetPieceCarQuatX[28] = -0.0125
SetPieceCarQuatY[28] = -0.0117
SetPieceCarQuatZ[28] = 0.7296
SetPieceCarQuatW[28] = 0.6837
SetPieceCarRecording[28] = 82
SetPieceCarStartime[28] = 90798.3203
SetPieceCarRecordingSpeed[28] = 1.0000
SetPieceCarModel[28] = Packer

//as target goes off the highway, swerves around him
SetPieceCarPos[29] = <<-118.4835, -529.6654, 29.6739>>
SetPieceCarQuatX[29] = 0.0116
SetPieceCarQuatY[29] = -0.0127
SetPieceCarQuatZ[29] = -0.6860
SetPieceCarQuatW[29] = 0.7274
SetPieceCarRecording[29] = 44
SetPieceCarStartime[29] = 94954.8594
SetPieceCarRecordingSpeed[29] = 1.0000
SetPieceCarModel[29] = landstalker

	// add extra time to recording start times for main car re record
	INT i = 0
	INT iNumSetPieceVehicles = COUNT_OF(SetPieceCarID)
	INT iNumTrafficVehicles = COUNT_OF(TrafficCarID)
	REPEAT iNumSetPieceVehicles i
		IF SetPieceCarStartime[i] >= NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_01
			SetPieceCarStartime[i] -= NIGEL2_UBER_TIME_STEP_01
			//CPRINTLN(DEBUG_MISSION, "LOAD_UBER_DATA : updated for NIGEL2_UBER_TIME_STEP_01 - - SetPieceCarStartime[", i, "] = ", SetPieceCarStartime[i])			
		ENDIF
		IF SetPieceCarStartime[i] >= NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_02
			SetPieceCarStartime[i] -= NIGEL2_UBER_TIME_STEP_02
			//CPRINTLN(DEBUG_MISSION, "LOAD_UBER_DATA : updated for NIGEL2_UBER_TIME_STEP_02 -  - SetPieceCarStartime[", i, "] = ", SetPieceCarStartime[i])			
		ENDIF		
	ENDREPEAT
	i = 0
	REPEAT iNumTrafficVehicles i 
		IF TrafficCarStartime[i] >= NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_01
			TrafficCarStartime[i] -= NIGEL2_UBER_TIME_STEP_01
			//CPRINTLN(DEBUG_MISSION, "LOAD_UBER_DATA : updated for NIGEL2_UBER_TIME_STEP_01 - TrafficCarStartime[", i, "] = ", TrafficCarStartime[i])			
		ENDIF
		IF TrafficCarStartime[i] >= NIGEL2_UBER_PLAYBACK_TIME_STEP_POINT_02
			TrafficCarStartime[i] -= NIGEL2_UBER_TIME_STEP_02
			//CPRINTLN(DEBUG_MISSION, "LOAD_UBER_DATA : updated for NIGEL2_UBER_TIME_STEP_02 -  - TrafficCarStartime[", i, "] = ", TrafficCarStartime[i])			
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    remove object from the game
/// PARAMS:
///    objectIndex - object to remove
///    bDelete - if true delete it, else set as no longer needed
PROC SAFE_REMOVE_OBJECT(OBJECT_INDEX &objectIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_OBJECT(objectIndex)
	ELSE
		SAFE_RELEASE_OBJECT(objectIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes vehicle from the world
/// PARAMS:
///    vehIndex - vehicle to be removed
///    bDelete - if true AND player isn't inside it delete it, else set as no longer needed
PROC SAFE_REMOVE_VEHICLE(VEHICLE_INDEX &vehIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_VEHICLE(vehIndex)
	ELSE
		SAFE_RELEASE_VEHICLE(vehIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes ped from the game
/// PARAMS:
///    ped - ped to be removed
///    bDelete - if true delete it, else set as no longer needed
///    bDetach - if true ped will be detached if attached, and no in a vehicle or getting into a vehicle
PROC SAFE_REMOVE_PED(PED_INDEX &ped, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_PED(ped)
	ELSE
		SAFE_RELEASE_PED(ped)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a ped to a new location
/// PARAMS:
///    pedIndex - ped to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the ped at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
///    bForcePedAiAndAnimUpdate - if true FORCE_PED_AI_AND_ANIMATION_UPDATE is applied on the ped
PROC SAFE_TELEPORT_PED(PED_INDEX pedIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bForcePedAiAndAnimUpdate = FALSE)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF bSnapToGround
			vPosition.Z = INVALID_WORLD_Z
		ENDIF
		SET_ENTITY_COORDS(pedIndex, vPosition)
		SET_ENTITY_HEADING(pedIndex, fHeading)
		IF bForcePedAiAndAnimUpdate
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a vehicle to a new location
/// PARAMS:
///    vehIndex - vehicle to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the vehicle at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
PROC SAFE_TELEPORT_VEHICLE(VEHICLE_INDEX &vehIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE)
	IF IS_ENTITY_ALIVE(vehIndex)
		IF bSnapToGround
			vPosition.Z = INVALID_WORLD_Z
		ENDIF
		SET_ENTITY_COORDS(vehIndex, vPosition)
		SET_ENTITY_HEADING(vehIndex, fHeading)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleports a ped out of their vehicle.
///    NOTE: proc waits until GET_SAFE_COORD_FOR_PED returns true
/// PARAMS:
///    pedIndex - ped to Teleport
PROC SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PED_INDEX pedIndex)
	VECTOR vRespotCoords = << 0.0, 0.0, 0.0 >>
	FLOAT fRespotHeading = 0.0	
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(pedIndex)		
			vRespotCoords = GET_ENTITY_COORDS(pedIndex)			
			WHILE NOT GET_SAFE_COORD_FOR_PED(vRespotCoords, FALSE, vRespotCoords)
				WAIT(0)
				vRespotCoords.x += 2.0
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE - looking for safe coords") ENDIF #ENDIF
			ENDWHILE			
			fRespotHeading = GET_ENTITY_HEADING(pedIndex)			
			SAFE_TELEPORT_PED(pedIndex, vRespotCoords, fRespotHeading, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks ped and vehicle are ok before putting the ped in the vehicle
///    If the seat is already occupied by a ped, it gets deleted.
/// PARAMS:
///    pedIndex - ped to put in the vehicle
///    vehIndex - vehicle ped is going into
///    seat - seat ped is to sit in
PROC SAFE_PUT_PED_INTO_VEHICLE(PED_INDEX pedIndex, VEHICLE_INDEX &vehIndex, VEHICLE_SEAT seat = VS_DRIVER)
	IF IS_ENTITY_ALIVE(pedIndex)
	AND IS_VEHICLE_OK(vehIndex)
		IF IS_VEHICLE_SEAT_FREE(vehIndex, seat)
			SAFE_SET_PED_INTO_VEHICLE(pedIndex, vehIndex, seat)
		ELSE			
			PED_INDEX pedTemp_AlreadyInSeat = GET_PED_IN_VEHICLE_SEAT(vehIndex, seat)	//check the ped we want to put in the vehicle isn't already in the vehicle				
			IF pedTemp_AlreadyInSeat != pedIndex
				IF IS_ENTITY_ALIVE(pedTemp_AlreadyInSeat)
					SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(pedTemp_AlreadyInSeat)
					SAFE_PUT_PED_INTO_VEHICLE(pedIndex, vehIndex, seat)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    check if iTimeAmount has passed since iTimer was set
/// PARAMS:
///    iTimer - the timer
///    iTimeAmount - the amount of time to check has passed
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimer, INT iTimeAmount)
	RETURN (GET_GAME_TIMER() - iTimer) > iTimeAmount
ENDFUNC

/// PURPOSE:
///    Test for if two coords are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN VDIST2(v1, v2) <= (fRange*fRange)
ENDFUNC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
/// PARAMS:
///    sConversationRoot - the conversation root to test
///    bReturnTrueIfStringIsNull - checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT can return "NULL" the first few frames after CREATE_CONVERSATION has returned true
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a root matching the passed in string or "NULL" if bReturnTrueIfNULL
FUNC BOOL IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(STRING sConversationRoot, BOOL bReturnTrueIfStringIsNull = TRUE)
	TEXT_LABEL_23 tlTempRoot		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
		//if current root matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempRoot, sConversationRoot)
			RETURN TRUE
		ENDIF		
		IF bReturnTrueIfStringIsNull
			//if current root matches null, return true
			//because if the correct root isn't returned the first frame or so after CREATE_CONVERSATION has return true.
			IF ARE_STRINGS_EQUAL(tlTempRoot, "NULL")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
///    cloneConversation - ?
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - Which line of dialogue to start from
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG2_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using PLAY_SINGLE_LINE_FROM_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - the specific line we want to play
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen 
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG2_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    safely check if a ped is playing ambient speech
/// PARAMS:
///    pedIndex - to check
/// RETURNS:
///    TRUE if uninjured and playing ambient speech
FUNC BOOL SAFE_IS_PED_PLAYING_AMBIENT_SPEECH(PED_INDEX &pedIndex)
	
	IF IS_PED_UNINJURED(pedIndex)
		IF IS_AMBIENT_SPEECH_PLAYING(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    trigger a music event, whilst first checking it has been prepared (if required)
/// PARAMS:
///    sMusicEvent - the music event name
///    bRequiresPrepare - if TRUE PREPARE_MUSIC_EVENT is setup before the event can be trigger.  Check the doc to see if an event needs preloading
/// RETURNS:
///    TRUE if the event is trigger successfully
FUNC BOOL SAFE_TRIGGER_MISSION_MUSIC_EVENT(STRING sMusicEvent, BOOL bRequiresPrepare = FALSE)
	IF bRequiresPrepare
		IF NOT PREPARE_MUSIC_EVENT(sMusicEvent)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning FALSE, not yet prepared for = ", sMusicEvent) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	IF TRIGGER_MUSIC_EVENT(sMusicEvent)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning TRUE for = ", sMusicEvent) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if Trevor is driving on the wrong side of the road
///    NOTE: used to trigger dialogue
/// RETURNS:
///    TRUE if he is
FUNC BOOL IS_TREVOR_DRIVING_ON_ROAD_WRONG_WAY()

	IF GET_IS_PLAYER_DRIVING_WRECKLESS(PLAYER_ID(), WT_DROVE_AGAINST_TRAFFIC)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    checks to see if vehIndex is in a settled upright position ready to stop for a cutscene
///     NOTE: doesn't safe check vehIndex
/// PARAMS:
///    vehIndex - the vehicle to check
/// RETURNS:
///    TRUE if ready to rock
FUNC BOOL IS_VEHICLE_SETTLED_FOR_CUTSCENE(VEHICLE_INDEX &vehIndex)

	IF IS_ENTITY_IN_AIR(vehIndex)	// B*1370588 - abrupt stop - wait until landed now
		CPRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : return FALSE : IS_ENTITY_IN_AIR check FC = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF

	IF NOT IS_ENTITY_UPRIGHT(vehIndex, 5.0)
		CPRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : return FALSE : IS_ENTITY_UPRIGHT check FC = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// if the vehicle is on all wheels are this point accept as ok
	IF IS_VEHICLE_ON_ALL_WHEELS(vehIndex)
		CPRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : return TRUE : IS_VEHICLE_ON_ALL_WHEELS check FC = ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	VECTOR vRotVelocity
	vRotVelocity = GET_ENTITY_ROTATION_VELOCITY(vehIndex)
	//CPRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : rvRotVelocity.X = ", vRotVelocity.X, " vRotVelocity.Y = ", vRotVelocity.Y, " FC = ", GET_FRAME_COUNT())

	IF vRotVelocity.X > 0.2
	OR vRotVelocity.X < -0.2
		PRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : return FALSE : vRotVelocity.X = ", vRotVelocity.X, " check FC = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	IF vRotVelocity.Y > 0.2
	OR vRotVelocity.Y < -0.2
		PRINTLN(DEBUG_MISSION, " IS_VEHICLE_SETTLED_FOR_CUTSCENE : return FALSE : vRotVelocity.Y = ", vRotVelocity.Y, " check FC = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
							
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    used to apply specific changes to certain vehicles in the uber recording
PROC UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING()
	IF GET_GAME_TIMER() - iUpdateUberVehicleTweaksTimer > 50 	
		//car target swerves around on way up hill
		IF IS_VEHICLE_OK(SetPieceCarID[3])
			IF fCurrentPlaybackTime > 28750.0
			AND fCurrentPlaybackTime < 29750.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[3], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[3], 2.0)
				IF NOT bDone_UberVehicleHorn[0]
					START_VEHICLE_HORN(SetPieceCarID[3], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[0] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[3], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[3], 1.0)
			ENDIF			
		ENDIF		
		//car target turns in front off when heading to tunnel
		IF IS_VEHICLE_OK(SetPieceCarID[5])
			IF fCurrentPlaybackTime > 34500.0
			AND fCurrentPlaybackTime < 35500.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[5], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[5], 2.0)
				IF NOT bDone_UberVehicleHorn[1]
					START_VEHICLE_HORN(SetPieceCarID[5], 3000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[1] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[5], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[5], 1.0)
			ENDIF			
		ENDIF		
		//truck in the tunnel
		IF IS_VEHICLE_OK(SetPieceCarID[8])
			//ensure garbage truck has acceptable ped model as driver
			IF NOT bDone_UpdateTrashTruckPedModel
				bDone_UpdateTrashTruckPedModel = OVERRIDE_VEHICLE_DRIVER_MODEL(SetPieceCarID[8], S_M_Y_GARBAGE)
			ENDIF			
			IF fCurrentPlaybackTime > 46000.0
			AND fCurrentPlaybackTime < 47000.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[8], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[8], 2.0)	
				IF NOT bDone_UberVehicleHorn[2]
					START_VEHICLE_HORN(SetPieceCarID[8], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[2] = TRUE
				ENDIF
			ELIF fCurrentPlaybackTime > 47500.0
			AND fCurrentPlaybackTime < 48500.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[8], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[8], 2.0)				
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[8], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[8], 1.0)
			ENDIF			
		ENDIF	
		//bmx kids
		IF IS_VEHICLE_OK(SetPieceCarID[9])
			//ensure bmx rider has acceptable ped model as driver
			IF NOT bDone_UpdateBMXPedModel
				bDone_UpdateBMXPedModel = OVERRIDE_VEHICLE_DRIVER_MODEL(SetPieceCarID[9], A_M_Y_Cyclist_01)
			ENDIF					
		ENDIF	
		//truck when doing u turn into carpark
		IF IS_VEHICLE_OK(SetPieceCarID[16])
			IF fCurrentPlaybackTime > 69000.0
			AND fCurrentPlaybackTime < 71000.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[16], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[16], 2.0)
				IF NOT bDone_UberVehicleHorn[3]
					START_VEHICLE_HORN(SetPieceCarID[16], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[3] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[16], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[16], 1.0)
			ENDIF			
		ENDIF		
		//car handbrake turns on exit from carpark
		IF IS_VEHICLE_OK(TrafficCarID[17])
			IF NOT bDone_UberVehicleHorn[4]
				IF fCurrentPlaybackTime > 78700.0
				AND fCurrentPlaybackTime < 79300.0					
					START_VEHICLE_HORN(TrafficCarID[17], 3000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[4] = TRUE
				ENDIF
			ENDIF
		ENDIF	
		//Truck on exit from carpark
		IF IS_VEHICLE_OK(TrafficCarID[19])
			IF NOT bDone_UberVehicleHorn[4]
				IF fCurrentPlaybackTime > 79950.0
				AND fCurrentPlaybackTime < 82300.0					
					START_VEHICLE_HORN(TrafficCarID[19], 3000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[4] = TRUE
				ENDIF
			ENDIF
		ENDIF
		//first car to react on the highway as target gets on
		IF IS_VEHICLE_OK(SetPieceCarID[20])
			IF NOT bDone_UberVehicleHorn[5]
				IF fCurrentPlaybackTime > 84000.0
				AND fCurrentPlaybackTime < 84750.0					
					START_VEHICLE_HORN(SetPieceCarID[20], 3000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[5] = TRUE
				ENDIF
			ENDIF
		ENDIF		
		//landstalker on highway driving normally towards target (whilst on opposite side)
		IF IS_VEHICLE_OK(SetPieceCarID[21])
			IF fCurrentPlaybackTime > 88000.0
			AND fCurrentPlaybackTime < 90502.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[21], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[21], 2.0)
				IF NOT bDone_UberVehicleHorn[6]
					START_VEHICLE_HORN(SetPieceCarID[21], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[6] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[21], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[21], 1.0)
			ENDIF			
		ENDIF		
		//truck target cuts in front of on highway entrance
		IF IS_VEHICLE_OK(SetPieceCarID[23])
			IF fCurrentPlaybackTime > 84800.0
			AND fCurrentPlaybackTime < 87000.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[23], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[23], 2.0)
				IF NOT bDone_UberVehicleHorn[7]
					START_VEHICLE_HORN(SetPieceCarID[23], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[7] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[23], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[23], 1.0)
			ENDIF			
		ENDIF		
		//vehicle where target reverts to correct side of highway
		IF IS_VEHICLE_OK(TrafficCarID[21])
			IF NOT bDone_UberVehicleHorn[8]
				IF fCurrentPlaybackTime > 98800.0
				AND fCurrentPlaybackTime < 99400.0				
					START_VEHICLE_HORN(TrafficCarID[21], 2000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[8] = TRUE
				ENDIF
			ENDIF
		ENDIF		
		//range rover on highway target swerves in front of
		IF IS_VEHICLE_OK(TrafficCarID[23])
			IF fCurrentPlaybackTime > 91200.0
			AND fCurrentPlaybackTime < 92200.0
				SET_VEHICLE_LIGHTS(TrafficCarID[23], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(TrafficCarID[23], 2.0)
				IF NOT bDone_UberVehicleHorn[9]
					START_VEHICLE_HORN(TrafficCarID[23], 2000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[9] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(TrafficCarID[23], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(TrafficCarID[23], 1.0)
			ENDIF			
		ENDIF		
		//first truck on highway where target reaches wrong side of the road
		IF IS_VEHICLE_OK(SetPieceCarID[28])
			IF fCurrentPlaybackTime > 93500.0
			AND fCurrentPlaybackTime < 95000.0
				SET_VEHICLE_LIGHTS(SetPieceCarID[28], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[28], 2.0)
				IF NOT bDone_UberVehicleHorn[10]
					START_VEHICLE_HORN(SetPieceCarID[28], 3500, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[10] = TRUE
				ENDIF
			ELSE
				SET_VEHICLE_LIGHTS(SetPieceCarID[28], FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[28], 1.0)
			ENDIF			
		ENDIF		
		//range rover on highway target goes around when heading for slip road
		IF IS_VEHICLE_OK(SetPieceCarID[29])
			IF NOT bDone_UberVehicleHorn[11]
				IF fCurrentPlaybackTime > 104500.0
				AND fCurrentPlaybackTime < 105000.0				
					START_VEHICLE_HORN(SetPieceCarID[29], 2000, GET_HASH_KEY("HELDDOWN"))
					bDone_UberVehicleHorn[11] = TRUE
				ENDIF
			ENDIF		
		ENDIF		
		//open doors on ambulance and turn sirens on
		IF IS_VEHICLE_OK(ParkedCarID[31])
			IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(ParkedCarID[31], SC_DOOR_REAR_LEFT)
				SET_VEHICLE_DOOR_OPEN(ParkedCarID[31], SC_DOOR_REAR_LEFT)
			ENDIF
			IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(ParkedCarID[31], SC_DOOR_REAR_RIGHT)
				SET_VEHICLE_DOOR_OPEN(ParkedCarID[31], SC_DOOR_REAR_RIGHT)
			ENDIF
			IF NOT IS_VEHICLE_SIREN_ON(ParkedCarID[31])
				SET_VEHICLE_SIREN(ParkedCarID[31], TRUE)
			ENDIF
		ENDIF		
		//ambulance sirens on coming out the hospital
		IF IS_VEHICLE_OK(SetPieceCarID[22])
			IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[22])
				SET_VEHICLE_SIREN(SetPieceCarID[22], TRUE)
			ENDIF
		ENDIF
		//car inside the carpark, parked up set Lights on and fake engine revs
		IF IS_VEHICLE_OK(SetPieceCarID[19])
			INT iVehicleLightState = -1
			INT iVehicleBeamState = -1
			GET_VEHICLE_LIGHTS_STATE(SetPieceCarID[19], iVehicleLightState, iVehicleBeamState)
			IF iVehicleLightState != 1
				SET_VEHICLE_LIGHTS(SetPieceCarID[19], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHT_MULTIPLIER(SetPieceCarID[19], 2.0)
			ENDIF
			IF iSoundID_UberVehicleFakeRevs = -1	// not yet been assigned a sound_id
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[19])
					 iSoundID_UberVehicleFakeRevs = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iSoundID_UberVehicleFakeRevs, "FAKE_REVS_VEHICLE_02", SetPieceCarID[19], "NIGEL_02_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING - set fake rev for set piece car 19, FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ELSE
				IF NOT HAS_SOUND_FINISHED(iSoundID_UberVehicleFakeRevs)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[19])
						STOP_SOUND(iSoundID_UberVehicleFakeRevs)
						RELEASE_SOUND_ID(iSoundID_UberVehicleFakeRevs)
						iSoundID_UberVehicleFakeRevs = -1 	// reset
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING - cleaned up fake rev for set piece car 19, FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		iUpdateUberVehicleTweaksTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    deals with requesting and removing the vehicle recordings
///    used by ther set piece and traffic vehicles in the uber recording
PROC PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
	IF GET_GAME_TIMER() - iPreLoadRecordingsTimer > 3
		INT i = 0
		INT iNumSetPieceVehicles = COUNT_OF(SetPieceCarID)
		INT iNumTrafficVehicles = COUNT_OF(TrafficCarID)
		REPEAT iNumSetPieceVehicles i
			IF SetPieceCarRecording[i] > 0
				IF (fCurrentPlaybackTime > (SetPieceCarStartime[i] - 5000.0))
					REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[i], sNigel2_UberRecordingName)
				ELIF (fCurrentPlaybackTime > (SetPieceCarStartime[i] + 3000.0))
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[i])
						REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], sNigel2_UberRecordingName)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
		REPEAT iNumTrafficVehicles i 
			IF TrafficCarRecording[i] > 0
				IF (fCurrentPlaybackTime > (TrafficCarStartime[i] - 5000.0))
					REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], sNigel2_UberRecordingName)
				ELIF (fCurrentPlaybackTime > (TrafficCarStartime[i] + 3000.0))
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrafficCarID[i])
						REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], sNigel2_UberRecordingName)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
		iPreLoadRecordingsTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    removes all of the set piece and traiffic car recordingd which may be loaded
PROC REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
	INT iNumTrafficCars = COUNT_OF(TrafficCarID)
	INT iNumSetPieceCars = COUNT_OF(SetPieceCarID)
	INT i = 0	
	REPEAT iNumTrafficCars i
		IF TrafficCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], sNigel2_UberRecordingName)
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT iNumSetPieceCars i
		IF SetPieceCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], sNigel2_UberRecordingName)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    all the common setup needed to initialise the uber playback
/// PARAMS:
///    bAllowUberInitToClearVehicles - if TRUE call to INITIALISE_UBER_PLAYBACK() will clear all ambient vehicles
PROC SETUP_UBER_PLAYBACK(BOOL bAllowUberInitToClearVehicles = TRUE)

	//for uber playback
	INITIALISE_UBER_PLAYBACK(sNigel2_UberRecordingName, iMainCarRecID, bAllowUberInitToClearVehicles)
	LOAD_UBER_DATA()
	SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
	SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
	CREATE_ALL_WAITING_UBER_CARS()
	fUberPlaybackDensitySwitchOffRange = 250.0	// fail dist is 200 so this should be plenty
	switch_SetPieceCar_to_ai_on_collision = true
	SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE) // B* 1490105 - stop peds driving the uber cars interfering with uber chase because of reaction to player shooting
	allow_veh_to_stop_on_any_veh_impact = TRUE			
	//bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
	//fPlaybackCarStreamingDistance = 160.0	// 120.0 // is default
	traffic_block_vehicle_colour(true, traffic_black)			
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
	CPRINTLN(DEBUG_MISSION, " SETUP_UBER_PLAYBACK - done : bAllowUberInitToClearVehicles = ", bAllowUberInitToClearVehicles, " : frame count :", GET_FRAME_COUNT())
ENDPROC

/// PURPOSE:
///    check if the specified position is inside the hospital using 
///    IS_COLLISION_MARKED_OUTSIDE check
/// PARAMS:
///    vPos - the position to test
/// RETURNS:
///    TRUE if the position is inside the music club interior
FUNC BOOL IS_POSITION_INSIDE_HOSPITAL_INTERIOR(VECTOR vPos)

	// make sure the handle to the hospital is valid first
	IF NOT IS_VALID_INTERIOR(HospitalInteriorIndex)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_POSITION_INSIDE_HOSPITAL_INTERIOR - return FALSE unable to test  HospitalInteriorIndex ins't valid interior : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	IF NOT IS_COLLISION_MARKED_OUTSIDE(vPos)
		INTERIOR_INSTANCE_INDEX interiorInstanceIndex = GET_INTERIOR_FROM_COLLISION(vPos)
		IF interiorInstanceIndex = HospitalInteriorIndex
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_POSITION_INSIDE_HOSPITAL_INTERIOR - return TRUE - for pos : ", vPos) ENDIF #ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_POSITION_INSIDE_HOSPITAL_INTERIOR - not hospital interior index! return FALSE - for pos : ", vPos) ENDIF #ENDIF
		ENDIF
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_POSITION_INSIDE_HOSPITAL_INTERIOR - collision marked outside at coords! return FALSE - for pos : ", vPos) ENDIF #ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Cycles through hospital room hash keys to see if parameter matches
/// RETURNS:
///    TRUE if specified iHashKeyToTest matches a hospital room's hash key
FUNC BOOL IS_HASH_KEY_A_HOSPITAL_ROOM(INT iHashKeyToTest)
	IF (iHashKeyToTest = GET_HASH_KEY("v_40_Room1"))	// entrance
	OR (iHashKeyToTest = GET_HASH_KEY("v_40_Room2"))	// corridor between two broken walls
	OR (iHashKeyToTest = GET_HASH_KEY("v_40_Room3"))	// ward (near broken wall)
	OR (iHashKeyToTest = GET_HASH_KEY("v_40_Room4"))	// ward area (past final broken wall)
	OR (iHashKeyToTest = GET_HASH_KEY("V_40_Room005"))	// entrance from shop to first broken wall
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if a ped is inside the hospital interior
///    NOTE: if ped isn't alive results aren't completely accurate see bug 1008508
/// PARAMS:
///    pedIndex - ped to test
/// RETURNS:
///    true if the ped is inside the hosptial interior and isn't injured
///    RETURNS false if bDoDeathCheck was set TRUE and the ped was dead
FUNC BOOL IS_PED_INSIDE_HOSPITAL_INTERIOR(PED_INDEX pedIndex)

	// make sure the handle to the hospital is valid first
	IF NOT IS_VALID_INTERIOR(HospitalInteriorIndex)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_PED_INSIDE_HOSPITAL_INTERIOR - return FALSE unable to test  HospitalInteriorIndex ins't valid interior : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_ALIVE(pedIndex)
		INT iHaskKey_PedCurrentRoom		
		iHaskKey_PedCurrentRoom = GET_KEY_FOR_ENTITY_IN_ROOM(pedIndex)
		IF IS_HASH_KEY_A_HOSPITAL_ROOM(iHaskKey_PedCurrentRoom)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_PED_INSIDE_HOSPITAL_INTERIOR - return TRUE with room check : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF	
	ELSE
		// check is returning inaccurate results, see bug 1008508
		VECTOR vPos = GET_ENTITY_COORDS(pedIndex, FALSE)
		IF (GET_INTERIOR_AT_COORDS(vPos) = HospitalInteriorIndex)	//unfortunately this check on its own can return true if stood outside interior closeby			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_PED_INSIDE_HOSPITAL_INTERIOR - return TRUE basic check : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_PED_INSIDE_HOSPITAL_INTERIOR - return FALSE:", GET_FRAME_COUNT()) ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if a vehicle is inside the hospital interior
/// PARAMS:
///    vehicleIndex - vehicle to test
/// RETURNS:
///    true if the vehicle is inside the hospital interior
FUNC BOOL IS_VEHICLE_INSIDE_HOSPITAL_INTERIOR(VEHICLE_INDEX vehicleIndex)
	
	// make sure the handle to the hospital is valid first
	IF NOT IS_VALID_INTERIOR(HospitalInteriorIndex)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " IS_VEHICLE_INSIDE_HOSPITAL_INTERIOR - return FALSE unable to test  HospitalInteriorIndex ins't valid interior : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehicleIndex)	
		VECTOR vPos = GET_ENTITY_COORDS(vehicleIndex)
		IF (GET_INTERIOR_AT_COORDS(vPos) = HospitalInteriorIndex)	//unfortunately this check on its own can return true if stood outside interior closeby
			INT iHaskKey_VehicleCurrentRoom		
			iHaskKey_VehicleCurrentRoom = GET_KEY_FOR_ENTITY_IN_ROOM(vehicleIndex)
			IF IS_HASH_KEY_A_HOSPITAL_ROOM(iHaskKey_VehicleCurrentRoom)
				RETURN TRUE
			ENDIF			
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check the musician has seen the player attack a specific ped
/// RETURNS:
///    true if player has injured a ped inside the hospital
FUNC BOOL HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL()
	//IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	//	#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED - true : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
		INT i = 0
		PED_INDEX mPed
		IF Has_Ped_Been_Killed()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - Has_Ped_Been_Killed - true : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF		
			INT iNumPedKilledEvents = Get_Number_Of_Ped_Killed_Events()
			IF iNumPedKilledEvents <> 0
				//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - iNumPedKilledEvents =, ", iNumPedKilledEvents, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
				BOOL bWasPedKilledInsideHopsital = FALSE
				i = 0
				REPEAT iNumPedKilledEvents i
					mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Killed_Ped(i))
					IF DOES_ENTITY_EXIST(mPed)
						#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 0 :, Killed ped found Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
						IF IS_POSITION_INSIDE_HOSPITAL_INTERIOR(GET_ENTITY_COORDS(mPed, FALSE))
						//OR IS_PED_INSIDE_HOSPITAL_INTERIOR(mPed)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 1 : Killed ped is INSIDE the hospital interior Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
								//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 2 : Killed ped has been DAMAGED by player Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
								bWasPedKilledInsideHopsital = TRUE
							ENDIF
						ELSE
							//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 1 : killed ped position is NOT inside the hospital interior Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				IF bWasPedKilledInsideHopsital
					#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 3 : killed ", " returning TRUE : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
					//CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF Has_Ped_Been_Injured()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - Has_Ped_Been_Injured - true : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF		
			INT iNumPedInjuredEvents = Get_Number_Of_Ped_Injured_Events()
			IF iNumPedInjuredEvents <> 0
				//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - iNumPedInjuredEvents =, ", iNumPedInjuredEvents, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
				BOOL bWasPedInjuredInsideHopsital = FALSE
				i = 0
				REPEAT iNumPedInjuredEvents i
					mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Injured_Ped(i))
					IF DOES_ENTITY_EXIST(mPed)		            	
						#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 0 :, Injuredped found Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
						IF IS_POSITION_INSIDE_HOSPITAL_INTERIOR(GET_ENTITY_COORDS(mPed, FALSE))
						//OR IS_PED_INSIDE_HOSPITAL_INTERIOR(mPed)	
							//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 1 : Injuredped is INSIDE the hospital interior Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
								//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 2 :, Injuredped has been DAMAGED by player Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
								bWasPedInjuredInsideHopsital = TRUE
							ENDIF
						ELSE
							//#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 1 : Injured ped position is NOT inside the hospital interior Count = ", i, " : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				IF bWasPedInjuredInsideHopsital
					#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - step 3 : injured ", " returning TRUE : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
					//CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	//IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	//	#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, " HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL - HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED - true : frame count :", GET_FRAME_COUNT()) ENDIF #ENDIF
	//	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
	//ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    this proc tracks the mission stat -
///    INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH)
PROC NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL()
	
	IF NOT bStatTracker_HasPlayerInjuredPedWhilstInHospital
		// only test when the player gets close to the hosiptal
		IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 307.3065, -589.9595, 42.3020 >>, 200.0)
			IF HAS_PLAYER_INJURED_A_PED_INSIDE_THE_HOSPITAL()
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH)
				bStatTracker_HasPlayerInjuredPedWhilstInHospital = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintHospitalStatTrackInfoToTTY CPRINTLN(DEBUG_MISSION, "NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_HARM_IN_HOSPITAL_DRIVE_THROUGH) ") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    this proc tracks the mission stat -
///    INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI) 
PROC NIG2_TRACK_STAT_PLAYER_KEEPS_CLOSE_TO_TARGET()
	FLOAT fInRangeDist = 100.0
	#IF IS_DEBUG_BUILD
		IF bDebug_OverrideStatTracker_PlayerCloseToTargetRange
			fInRangeDist = fDebug_StatTracker_PlayerCloseToTargetRange
		ENDIF	
		IF bDebug_DisplayStatTrackerInfo_PlayerCloseToTarget
			IF IS_ENTITY_ALIVE(sTargetPed.ped)
				//SET_DEBUG_ACTIVE(TRUE)
				//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				VECTOR vDebugPos = GET_ENTITY_COORDS(sTargetPed.ped)
				DRAW_DEBUG_SPHERE(vDebugPos, fInRangeDist, 200, 100, 0, 150)
			ENDIF
		ENDIF	
	#ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIG2_TRACK_STAT_PLAYER_KEEPS_CLOSE_TO_TARGET - current dist  = ", fCurrentChaseDistance, " : point in uber = ", fCurrentPlaybackTime) ENDIF #ENDIF
	IF bStatTracker_HasPlayerKeptCloseToDiNapoli
		IF fCurrentChaseDistance > fInRangeDist
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI) 
			bStatTracker_HasPlayerKeptCloseToDiNapoli = FALSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIG2_TRACK_STAT_PLAYER_KEEPS_CLOSE_TO_TARGET - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI2_GOT_TOO_FAR_AWAY_FROM_NAPOLI) range was = ", fCurrentChaseDistance) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Rubberbands the chase speed of Trevor to Target's vehicle
///    uses UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING and CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR
///    NOTE: having to override the standard uber recording behaviour a bit as the player needs to be able to get along side Miranda comfortably
/// PARAMS:
///    fCurrentChaseSpeed - the main chase speed which is to be updated
///    fPlaybackTime - the main car's current playback time
PROC UPDATE_UBER_CHASE_SPEED(FLOAT &fCurrentChaseSpeed, FLOAT fPlaybackTime)

	BOOL bUseNewRubberbandingSystem = TRUE	
	IF bUseNewRubberbandingSystem
		
		IF IS_VEHICLE_OK(sNigelVehicle.vehicle)	// new commands only take vehicles in parameter instead player ped, so need ot check Nigel's car for use
			
			// variables used by CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR	
			FLOAT fDesiredPlaybackSpeed = fCurrentChaseSpeed	// Stores the calculated speed.
						
			FLOAT fClosestDist = 9.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
			FLOAT fIdealDist = 20.0								// fIdealDist - The ideal distance you want the player to be behind the trigger.
			FLOAT fMaxDist = 36.0								// fMaxDist - The max distance the player should be behind the trigger.
			
			// point where the flashing blip kicks in for escaping
			FLOAT fLosingTriggerDist = (NIGEL2_TARGET_ESCAPED_DISTANCE / 100) * (NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE * 100)	 // At this distance or greater extra slowdown will be applied if safe to do so.
			
			FLOAT fMaxDistInFront = 30.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
			FLOAT fMinSpeedIfBehind = 0.7						// The minimum playback speed allowed if behind, values lower than 0.7 may be too noticeable.
			FLOAT fMinSpeedIfLosingTrigger = 0.45				// The minimum playback speed allowed if the player is really far behind (this value is only used if the trigger is not visible).
			FLOAT fMaxSpeedIfTooClose = 1.6						// The max speed allowed if the player gets too close.

			// variables used by UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING
			FLOAT fAccel = 0.04									// fAccel - D = 0.01 -  Modify this to increase/decrease the rate that the current speed changes to achieve the desired speed. Changing this can help if you see the trigger car noticeably speed off during a chase.


			//***	Rubberbanding tweaks during the chase based of the main car's playback progress - 
			
			// start, heading past Trevor into the first turning (keep this slow to give the player plenty of time to get in the car.
			IF (fPlaybackTime > 0.0 AND  fPlaybackTime < 6000.0)
			
				fClosestDist = 1.0
				fIdealDist = 10.0
				fMaxDist = 22.0	
				fMaxDistInFront = 45.0
				fMaxSpeedIfTooClose = 1.0
				fAccel = 0.02
				
			// turn out of pedestrian area onto the main road again
			ELIF (fPlaybackTime > 12000.0 AND  fPlaybackTime < 20000.0)
			
				fClosestDist = 10.5							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 22.0								// fIdealDist - The ideal distance you want the player to be behind the trigger.
				
				fMaxDistInFront = 15.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
				
				fAccel = 0.05								// fAccel - D = 0.01 -  Modify this to increase/decrease the rate that the current speed changes to achieve the desired speed. Changing this can help if you see the trigger car noticeably speed off during a chase.
				
			// left turn of main road heading towards tunnel entrance turning
			ELIF (fPlaybackTime > 31000.0 AND  fPlaybackTime < 35500.0)
				
				fClosestDist = 9.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 20.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				
				fMaxDistInFront = 20.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
				fMaxSpeedIfTooClose = 1.8					// The max speed allowed if the player gets too close.
			
			//turn into underground tunnel
			ELIF (fPlaybackTime > 37000.0 AND  fPlaybackTime < 42500.0)
				
				fClosestDist = 8.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 21.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				
			//turn out of the undeground section
			ELIF (fPlaybackTime > 54500.0 AND  fPlaybackTime < 58000.0)
				
				fClosestDist = 8.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 22.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				
			// crossing road down pedestrian section.
			ELIF (fPlaybackTime > 58000.0 AND  fPlaybackTime < 63500.0)   
			
				fClosestDist = 15.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 22.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				
				fAccel = 0.35								// Modify this to increase/decrease the rate that the current speed changes to achieve the desired speed. Changing this can help if you see the trigger car noticeably speed off during a chase.
				
			// jump down the stairs to the turn into carpark
			ELIF (fPlaybackTime > 63500.0 AND  fPlaybackTime < 72500.0)

				fClosestDist = 10.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 20.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 30.0								// fMaxDist - The max distance the player should be behind the trigger.
			
			// through the car park onto the highway
			ELIF (fPlaybackTime > 72500.0 AND  fPlaybackTime < 81500.0)

				fMaxDist = 30.0								// fMaxDist - The max distance the player should be behind the trigger.
				
			// jump leading on to highway
			ELIF (fPlaybackTime > 81500.0 AND  fPlaybackTime < 84000.0)	

				fMaxDist = 25.0								// fMaxDist - The max distance the player should be behind the trigger.
			
			// highway - speed up if player is driving on wrong side of the highway.	
			ELIF (fPlaybackTime > 88000.0 AND  fPlaybackTime < 96500.0)	

				fMaxDist = 30.0								// fMaxDist - The max distance the player should be behind the trigger.
					
				fMaxDistInFront = 20.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
				
				//override settings if player is traveling up opposite side of highway to napoli
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-423.235077,-528.185303,22.334507>>, <<-234.671829,-527.774048,30.579157>>, 27.000000)

					fClosestDist = 20.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
					fIdealDist = 35.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
					fMaxDist = 60.0								// fMaxDist - The max distance the player should be behind the trigger.
					
					fMaxDistInFront = 15.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "rubberbanding >>>> player on wrong side of freeway") ENDIF #ENDIF
				ENDIF			
			
			// highway
			ELIF (fPlaybackTime > 96500.0 AND  fPlaybackTime < 102500.0)
			
				fClosestDist = 15.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 20.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 25.0								// fMaxDist - The max distance the player should be behind the trigger.
				
				fMaxDistInFront = 20.0						// If the player is ahead the speed is increased, the max speed is achieved at this distance.
				
				fAccel = 0.35							// fAccel - D = 0.01 -  Modify this to increase/decrease the rate that the current speed changes to achieve the desired speed. Changing this can help if you see the trigger car noticeably speed off during a chase.
				
			// skid leading off highway up sliproad
			ELIF (fPlaybackTime > 102500.0 AND  fPlaybackTime < 107500.0)

				fClosestDist = 15.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 25.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 40.0								// fMaxDist - The max distance the player should be behind the trigger.
				
			// up the slip road
			ELIF (fPlaybackTime > 107500.0 AND  fPlaybackTime < 110500.0)

				fClosestDist = 17.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 30.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 50.0								// fMaxDist - The max distance the player should be behind the trigger.
				
				fMaxSpeedIfTooClose = 1.9						// The max speed allowed if the player gets too close.
				
			// run up to the hospital
			ELIF (fPlaybackTime > 110500.0 AND  fPlaybackTime < 114000.0)	

				fClosestDist = 20.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 35.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 55.0								// fMaxDist - The max distance the player should be behind the trigger.

				fMaxSpeedIfTooClose = 1.9						// The max speed allowed if the player gets too close.
				
			// inside hospital up till wall smashes
			ELIF (fPlaybackTime > 114500.0 AND fPlaybackTime < 118000.0) 
				
				fClosestDist = 22.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 37.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 60.0								// fMaxDist - The max distance the player should be behind the trigger.

				fMaxSpeedIfTooClose = 2.0					// The max speed allowed if the player gets too close.
				
			// inside hospital after wall smashes
			ELIF (fPlaybackTime > 118000.0) 
				
				fClosestDist = 27.0							// fClosestDist - The minimum distance the player should be behind the trigger. 
				fIdealDist = 45.0							// fIdealDist - The ideal distance you want the player to be behind the trigger.
				fMaxDist = 70.0								// fMaxDist - The max distance the player should be behind the trigger.

				fMaxSpeedIfTooClose = 2.0					// The max speed allowed if the player gets too close.*/
				
			ENDIF
			
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sNigelVehicle.vehicle, sTargetVehicle.vehicle, 
					fClosestDist, 						// fClosestDist - The minimum distance the player should be behind the trigger. 
					fIdealDist, 						// fIdealDist - The ideal distance you want the player to be behind the trigger.
					fMaxDist,							// fMaxDist - The max distance the player should be behind the trigger.
					fLosingTriggerDist,					// fLosingTriggerDist - At this distance or greater extra slowdown will be applied if safe to do so.
					fMaxDistInFront,					// fMaxDistInFront D = 30.0 - If the player is ahead the speed is increased, the max speed is achieved at this distance.
					NIGEL2_DEFAULT_UBER_PLAYBACK_SPEED,	// fDefaultPlaybackSpeed D = 1.0 - The default playback speed: when at the ideal distance the chase will run around this speed.
					fMinSpeedIfBehind,					// fMinSpeedIfBehind D = 0.7 - The minimum playback speed allowed if behind, values lower than 0.7 may be too noticeable.
					fMinSpeedIfLosingTrigger, 			// fMinSpeedIfLosingTrigger D = 0.5 - The minimum playback speed allowed if the player is really far behind (this value is only used if the trigger is not visible).
					fMaxSpeedIfTooClose,				// fMaxSpeedIfTooClose D = 2.0 - The max speed allowed if the player gets too close.
					TRUE)								// bApplySlowdownToPlayer - If TRUE slowdown will be applied to the player based on playback speed (using MODIFY_VEHICLE_TOP_SPEED).
					//0.0,								// fMaxPlayerSlowdownPercent D = 0.0 - The maximum percentage of top speed that can be removed from the player.
					//15.0,								// fMaxPlayerSlowdownPercent D = 15.0 - The maximum percentage of top speed that can be removed from the player.
					//TRUE)								// bAccountForSpecialAbility - If TRUE then extra adjustments are made when Franklin's special ability is active.


			//override playback speed for section driving through the hospital
			IF (fPlaybackTime > 114500.0)
			
				//override if player goes around hospital on highway sliproad
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<290.124298,-545.218384,38.397015>>, <<448.905365,-554.200134,48.652863>>, 19.250000)
					
					fDesiredPlaybackSpeed = 1.2
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "rubberbanding >>>> override player watching crash from highway") ENDIF #ENDIF
					
				ELSE
				
					//only use fast speed whilst hidden in the interior, return to a normal speed for jump
					IF (fPlaybackTime < 120000.0)	// 118000.0) 	
						
						fDesiredPlaybackSpeed = 2.2
						
						fAccel = 1.0
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "rubberbanding >>>> override target trigving through the interor, fCurrentChaseSpeed = ", fCurrentChaseSpeed) ENDIF #ENDIF
						
					ENDIF
				ENDIF	
			ENDIF

			UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentChaseSpeed, fDesiredPlaybackSpeed, fAccel)
			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_UBER_CHASE_SPEED - New System - fPlaybackTime = ", fPlaybackTime, " fAccel = ", fAccel, " fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed, " returning playback speed is = ", fCurrentChaseSpeed, " frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ELSE
		// original rubberbanding setup - to be removed once the new system has been bedded in
		FLOAT fDesiredPlaybackSpeed = fCurrentChaseSpeed	// Stores the calculated speed.
		FLOAT fMinDist_Local = 6.0
		FLOAT fIdealDist_Local = 18.0
		FLOAT fSlowDownDist_Local = 35.0
		FLOAT fMaxPlaybackSpeedMultiplier_Local = 1.5	
		//tweak values depending where the target is in the chase
		IF (fPlaybackTime > 37000.0 AND  fPlaybackTime < 42500.0)//turn into underground tunnel
			fMinDist_Local = 8.0
			fIdealDist_Local = 22.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 54500.0 AND  fPlaybackTime < 58000.0)//turn out of the undeground section
			fMinDist_Local = 8.0
			fIdealDist_Local = 20.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 58000.0 AND  fPlaybackTime < 63500.0)   // crossing road down pedestrian section.
			fIdealDist_Local = 15.0
			fSlowDownDist_Local = 22.0		
		ELIF (fPlaybackTime > 63500.0 AND  fPlaybackTime < 72500.0)//jump down the stairs to the turn into carpark
			fMinDist_Local = 10.0
			fIdealDist_Local = 20.0
			fSlowDownDist_Local = 30.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 72500.0 AND  fPlaybackTime < 81500.0)//through the car park onto the highway
			fSlowDownDist_Local = 30.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 81500.0 AND  fPlaybackTime < 84000.0)	//jump leading on to highway
			fSlowDownDist_Local = 25	//20
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 88000.0 AND  fPlaybackTime < 96500.0)// highway - speed up if player is driving on wrong side of the highway.		
			fSlowDownDist_Local = 20.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6		
			//override settings if player is traveling up opposite side of highway to napoli
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-423.235077,-528.185303,22.334507>>, <<-234.671829,-527.774048,30.579157>>, 27.000000)
				fMinDist_Local = 12.0
				fIdealDist_Local = 30.0
				fSlowDownDist_Local = 45.0
				fMaxPlaybackSpeedMultiplier_Local = 1.6
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "rubberbanding >>>> player on wrong side of freeway") ENDIF #ENDIF
			ENDIF		
		ELIF (fPlaybackTime > 96500.0 AND  fPlaybackTime < 102500.0)// highway
			fSlowDownDist_Local = 20
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 102500.0 AND  fPlaybackTime < 107500.0)//skid leading off highway up sliproad
			fMinDist_Local = 12.0
			fIdealDist_Local = 25.0
			fSlowDownDist_Local = 40.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 107500.0 AND  fPlaybackTime < 110500.0)//up the slip road
			fMinDist_Local = 14.0
			fIdealDist_Local = 30.0
			fSlowDownDist_Local = 50.0
			fMaxPlaybackSpeedMultiplier_Local = 1.6
		ELIF (fPlaybackTime > 110500.0 AND  fPlaybackTime < 114000.0)	//run up to the hospital
			fMinDist_Local = 20.0
			fIdealDist_Local = 35.0
			fSlowDownDist_Local = 55.0
			fMaxPlaybackSpeedMultiplier_Local = 1.9
		ELIF (fPlaybackTime > 114500.0 AND fPlaybackTime < 118000.0) // inside hospital up till wall smashes
			fMinDist_Local = 40.0
			fIdealDist_Local = 50.0
			fSlowDownDist_Local	= 70.0
			fMaxPlaybackSpeedMultiplier_Local = 3.0
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	RUBBBBBBER BANNNNNDDDDING IN HOSPITAL 		xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx") ENDIF #ENDIF
		ELIF (fPlaybackTime > 118000.0) // inside hospital after wall smashes
			fMinDist_Local = 20.0
			fIdealDist_Local = 35.0
			fSlowDownDist_Local	= 65.0
			fMaxPlaybackSpeedMultiplier_Local = 2.0
		ENDIF	
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sTargetVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, 1, 
												fMinDist_Local, fIdealDist_Local, fSlowDownDist_Local, fMaxPlaybackSpeedMultiplier_Local)	
		IF (fPlaybackTime > 114500.0 AND fPlaybackTime < 118000.0) 	//override playback speed for section driving through the hospital
			fDesiredPlaybackSpeed = 2.2
		ENDIF
		IF (fPlaybackTime > 114500.0)	//override if player goes around hospital on highway sliproad
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<290.124298,-545.218384,38.397015>>, <<448.905365,-554.200134,48.652863>>, 19.250000)
				fDesiredPlaybackSpeed = 1.2
			ENDIF	
		ENDIF
		
		fCurrentChaseSpeed = fDesiredPlaybackSpeed
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_UBER_CHASE_SPEED - OLD SYSTEM - returning playback speed is - ", fCurrentChaseSpeed) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles creation, updating and removal of scripted peds that feature on the chase route
/// PARAMS:
///    vPlayerCurrentPos - player's current position to perform checks with 
PROC MANAGE_PEDESTRIANS_ON_CHASE_ROUTE(VECTOR vPlayerCurrentPos)

	BOOL bCreatedPedThisFrame = FALSE
	MODEL_NAMES mnTempPedModel
	VECTOR vTempSpawnPos
	FLOAT fTempSpawnHeading
	INT iAIUpdateDelay = 250
	INT i
	SEQUENCE_INDEX sSeqIndex
	#IF IS_DEBUG_BUILD	TEXT_LABEL tDebugName #ENDIF
	
	IF IS_ENTITY_ALIVE(sTargetPed.ped)		
	
		//peds around the start of the car chase, busy shopping streets
		FOR i = 0 TO (TOTAL_START_AREA_PEDS - 1)			
			// create the peds (limit to one ped create per frame to help framerate
			IF sStartAreaPedSimple[i].eState = SPS_INIT
				IF NOT bCreatedPedThisFrame
					IF (fCurrentPlaybackTime  >= NIGEL2_START_AREA_PEDS_TRIGGER_TIME)
						SWITCH i
							CASE 0
								mnTempPedModel = mnDefaultPedForVehicle
								vTempSpawnPos = << -1262.53, -731.25, 21.88 >>
								fTempSpawnHeading = 47.38
							BREAK
							CASE 1
								mnTempPedModel = mnFemaleStartArea
								vTempSpawnPos = << -1300.17, -704.10, 24.61 >>
								fTempSpawnHeading = -87.53									
							BREAK
							CASE 2
								mnTempPedModel = mnFemaleStartArea
								vTempSpawnPos = << -1298.33, -704.16, 24.53 >>
								fTempSpawnHeading = 83.96
							BREAK
							CASE 3
								mnTempPedModel = mnFemaleStartArea
								vTempSpawnPos = << -1209.92, -817.51, 15.62 >>
								fTempSpawnHeading = 15.06
							BREAK								
						ENDSWITCH						
						REQUEST_MODEL(mnTempPedModel)					
						IF HAS_MODEL_LOADED(mnTempPedModel)
							sStartAreaPedSimple[i].ped = CREATE_PED(PEDTYPE_MISSION, mnTempPedModel, vTempSpawnPos, fTempSpawnHeading)	
							IF IS_PED_UNINJURED(sStartAreaPedSimple[i].ped)
								sStartAreaPedSimple[i].eState = SPS_BEGIN_ROUTINE
								SET_PED_RANDOM_COMPONENT_VARIATION(sStartAreaPedSimple[i].ped)
								SET_PED_KEEP_TASK(sStartAreaPedSimple[i].ped, TRUE)
								#IF IS_DEBUG_BUILD
									tDebugName = "StartAreaPed: "
									tDebugName += i
									SET_PED_NAME_DEBUG(sStartAreaPedSimple[i].ped, tDebugName)
								#ENDIF
							ENDIF
							IF i != 0 // 0 don't bin off the standard ped model used in the uber chase
								SET_MODEL_AS_NO_LONGER_NEEDED(mnTempPedModel)
							ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "StartArea PEDS CREATED index = ", i, " fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							bCreatedPedThisFrame = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// don't update their AI every frame
				IF GET_GAME_TIMER() - iTriggerScriptedPedestriansTimer > iAIUpdateDelay
					IF IS_PED_UNINJURED(sStartAreaPedSimple[i].ped)
						SWITCH sStartAreaPedSimple[i].eState
							CASE SPS_BEGIN_ROUTINE
								IF IS_ENTITY_IN_RANGE_ENTITY(sTargetPed.ped, sStartAreaPedSimple[i].ped, 150.0)
									IF i = 0
									OR i = 3
										TASK_USE_MOBILE_PHONE(sStartAreaPedSimple[i].ped, TRUE)
										SET_PED_KEEP_TASK(sStartAreaPedSimple[i].ped, FALSE)
										sStartAreaPedSimple[i].eState = SPS_REACT_TO_TARGET
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Start Area Peds Begin Routine - ", i) ENDIF #ENDIF
									ELSE
										// these guys have to wait for each other to spawn
										IF i = 1
											IF IS_PED_UNINJURED(sStartAreaPedSimple[i+1].ped)
												TASK_CHAT_TO_PED(sStartAreaPedSimple[i].ped, sStartAreaPedSimple[i+1].ped, CF_IS_INITIATOR | CF_AUTO_CHAT, <<0.0, 0.0, 0.0>>, 0.0, 0.0)
												SET_PED_KEEP_TASK(sStartAreaPedSimple[i].ped, FALSE)
												sStartAreaPedSimple[i].eState = SPS_REACT_TO_TARGET
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Start Area Peds Begin Routine - ", i) ENDIF #ENDIF
											ENDIF
										ELIF i = 2
											IF IS_PED_UNINJURED(sStartAreaPedSimple[i-1].ped)
												TASK_CHAT_TO_PED(sStartAreaPedSimple[i].ped, sStartAreaPedSimple[i-1].ped, CF_AUTO_CHAT, <<0.0, 0.0, 0.0>>, 0.0, 0.0)
												SET_PED_KEEP_TASK(sStartAreaPedSimple[i].ped, FALSE)
												sStartAreaPedSimple[i].eState = SPS_REACT_TO_TARGET
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Start Area Peds Begin Routine - ", i) ENDIF #ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE SPS_REACT_TO_TARGET
								IF IS_ENTITY_IN_RANGE_ENTITY(sTargetPed.ped, sStartAreaPedSimple[i].ped, 3.0)
									CLEAR_PED_TASKS(sStartAreaPedSimple[i].ped)
									OPEN_SEQUENCE_TASK(sSeqIndex)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, sTargetPed.ped, -1)
										//TASK_LOOK_AT_ENTITY(NULL, sTargetPed.ped, -1)
									CLOSE_SEQUENCE_TASK(sSeqIndex)
									TASK_PERFORM_SEQUENCE(sStartAreaPedSimple[i].ped, sSeqIndex)
									CLEAR_SEQUENCE_TASK(sSeqIndex)	
									sStartAreaPedSimple[i].eState = SPS_READY_FOR_CLEANUP
								ENDIF
								IF NOT IS_ENTITY_IN_RANGE_COORDS(sStartAreaPedSimple[i].ped, vPlayerCurrentPos, 300.0)  //cleanup the peds player far away
									SAFE_REMOVE_PED(sStartAreaPedSimple[i].ped)
									sStartAreaPedSimple[i].eState = SPS_CLEANED_UP
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "StartArea PED REMOVED - ", i) ENDIF #ENDIF
								ENDIF
							BREAK
							CASE SPS_READY_FOR_CLEANUP
								IF NOT IS_ENTITY_IN_RANGE_COORDS(sStartAreaPedSimple[i].ped, vPlayerCurrentPos, 150.0)  //cleanup the peds player far away
									SAFE_REMOVE_PED(sStartAreaPedSimple[i].ped)
									sStartAreaPedSimple[i].eState = SPS_CLEANED_UP
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "StartArea PED REMOVED - ", i) ENDIF #ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDFOR

		//Hospital peds	
		FOR i = 0 TO (TOTAL_HOSPITAL_PEDS - 1)
			IF sHospitalPedSimple[i].eState = SPS_INIT
				IF NOT bCreatedPedThisFrame
					IF (fCurrentPlaybackTime  >= NIGEL2_HOSPITAL_PEDS_TRIGGER_TIME)	
					AND eHospitalInteriorState > HIS_PIN_INTERIOR_IN_MEMORY	// ensure the hospital has been requested beofre checking it's ready
						IF IS_VALID_INTERIOR(HospitalInteriorIndex)
						AND IS_INTERIOR_READY(HospitalInteriorIndex)
							SWITCH i
								// peds near entrance
								CASE 0
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 309.34, -596.87, 43.31 >>
									fTempSpawnHeading = 14.8
								BREAK
								CASE 1
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 309.52, -594.85, 43.31 >>
									fTempSpawnHeading = 153.4102
								BREAK
								CASE 2
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 316.00, -588.77, 42.30 >>
									fTempSpawnHeading = 207.5560
								BREAK
								CASE 3
									mnTempPedModel = mnDoctorMale
									vTempSpawnPos = << 317.1715, -590.0840, 42.3020 >>
									fTempSpawnHeading = 60.6
								BREAK	
								//ped in corridor
								CASE 4
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 332.54, -588.50, 43.30 >>
									fTempSpawnHeading = 70.64
								BREAK
								CASE 5
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 331.68, -578.60, 43.32 >>
									fTempSpawnHeading = 234.5983 
								BREAK
								CASE 6
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 362.56, -589.32, 43.33 >>
									fTempSpawnHeading = -21.77
								BREAK
								CASE 7
									mnTempPedModel = mnScrubsFemale
									vTempSpawnPos = << 363.3849, -587.8508, 42.3275 >>
									fTempSpawnHeading =  161.2689
								BREAK
								CASE 8
									mnTempPedModel = mnDoctorMale
									vTempSpawnPos = << 358.5161, -586.0200, 42.3275 >>
									fTempSpawnHeading = 67.7037
								BREAK
							ENDSWITCH	
							
							REQUEST_MODEL(mnTempPedModel)					
							IF HAS_MODEL_LOADED(mnTempPedModel)
								sHospitalPedSimple[i].ped = CREATE_PED(PEDTYPE_MISSION, mnTempPedModel, vTempSpawnPos, fTempSpawnHeading)	
								IF IS_PED_UNINJURED(sHospitalPedSimple[i].ped)
									sHospitalPedSimple[i].eState = SPS_REACT_TO_TARGET
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sHospitalPedSimple[i].ped, TRUE)
									SET_PED_FLEE_ATTRIBUTES(sHospitalPedSimple[i].ped, FA_CAN_SCREAM, TRUE)
									SET_PED_KEEP_TASK(sHospitalPedSimple[i].ped, TRUE)
									//RETAIN_ENTITY_IN_INTERIOR(sHospitalPedSimple[i].ped, HospitalInteriorIndex)	// don't do this for now as it's making the peds invisible inside the interior!!!
									#IF IS_DEBUG_BUILD
										tDebugName = "HospitalP: "
										tDebugName += i
										SET_PED_NAME_DEBUG(sHospitalPedSimple[i].ped, tDebugName)
									#ENDIF
									SWITCH i
										CASE 0
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										BREAK
										CASE 1
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[i].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										BREAK
										CASE 2
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 1, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[2].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
										BREAK
										CASE 3
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[3].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[3].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[3].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[3].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[3].ped, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)		
										BREAK	
										CASE 4
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,5), 1, 1, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[4].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)	
										BREAK
										CASE 5
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 2, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[5].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)	
										BREAK
										CASE 6
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[6].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs) 
										BREAK
										CASE 7
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[7].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)	
										BREAK
										CASE 8
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[8].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[8].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[8].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[8].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_COMPONENT_VARIATION(sHospitalPedSimple[8].ped, INT_TO_ENUM(PED_COMPONENT,11), 1, 2, 0) //(jbib)	
										BREAK
									ENDSWITCH
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "hospital PEDS CREATED") ENDIF #ENDIF						
									//Setup talkers
									//TASK_CHAT_TO_PED(sHospitalPedSimple[0].ped, sHospitalPedSimple[1].ped, CF_AUTO_CHAT, <<0.0, 0.0, 0.0>>, 0.0, 0.0)
									//TASK_CHAT_TO_PED(sHospitalPedSimple[2].ped, sHospitalPedSimple[3].ped, CF_IS_INITIATOR | CF_AUTO_CHAT, <<0.0, 0.0, 0.0>>, 0.0, 0.0)
									//TASK_CHAT_TO_PED(sHospitalPedSimple[6].ped, sHospitalPedSimple[7].ped, CF_AUTO_CHAT, <<0.0, 0.0, 0.0>>, 0.0, 0.0)						
									//setup guys walk
									//TASK_FOLLOW_NAV_MESH_TO_COORD(sHospitalPedSimple[4].ped, << 338.3217, -592.5885, 42.2918 >>, PEDMOVEBLENDRATIO_WALK)
									//TASK_FOLLOW_NAV_MESH_TO_COORD(sHospitalPedSimple[5].ped, << 347.9706, -588.3258, 42.3275 >>, PEDMOVEBLENDRATIO_WALK)								
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(mnTempPedModel)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " Hospital PEDS CREATED index = ", i, " fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
								bCreatedPedThisFrame = TRUE							
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// don't update their AI every frame
				IF GET_GAME_TIMER() - iTriggerScriptedPedestriansTimer > iAIUpdateDelay
					IF IS_PED_UNINJURED(sHospitalPedSimple[i].ped)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "hospital ped - ", i) ENDIF #ENDIF
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "the square of distance between the two points is ", fDistBetweenPlayerAndHospitalPed) ENDIF #ENDIF
						SWITCH sHospitalPedSimple[i].eState
							CASE SPS_REACT_TO_TARGET
								IF IS_ENTITY_IN_RANGE_COORDS(sHospitalPedSimple[i].ped, vPlayerCurrentPos, 5.0)
								OR IS_ENTITY_IN_RANGE_ENTITY(sTargetPed.ped, sHospitalPedSimple[i].ped, 8.0)
									//TASK_SMART_FLEE_PED(sHospitalPedSimple[i].ped, PLAYER_PED_ID(), 100.0, -1)
									TASK_COWER(sHospitalPedSimple[i].ped, -1)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sHospitalPedSimple[i].ped, TRUE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HOSPITAL PED COWER- ", i) ENDIF #ENDIF
									sHospitalPedSimple[i].eState = SPS_READY_FOR_CLEANUP
								ENDIF
							BREAK
							CASE SPS_REACT_TO_PLAYER	
								/*IF IS_ENTITY_IN_RANGE_COORDS(sHospitalPedSimple[i].ped, vPlayerCurrentPos, 2.5) 
									SET_ENTITY_HEALTH(sHospitalPedSimple[i].ped, 0)
									CLEAR_PED_TASKS(sHospitalPedSimple[i].ped)
									sHospitalPedSimple[i].eState = SPS_READY_FOR_CLEANUP
								ENDIF*/
							BREAK
							CASE SPS_READY_FOR_CLEANUP
								IF IS_PED_SHOOTING(PLAYER_PED_ID())
								OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sHospitalPedSimple[i].ped)	
								OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sHospitalPedSimple[i].ped)
									IF GET_SCRIPT_TASK_STATUS(sHospitalPedSimple[i].ped, SCRIPT_TASK_COWER) != PERFORMING_TASK
		            				AND GET_SCRIPT_TASK_STATUS(sHospitalPedSimple[i].ped, SCRIPT_TASK_COWER) != WAITING_TO_START_TASK
										TASK_COWER(sHospitalPedSimple[i].ped, -1)
									ENDIF
								ELIF NOT IS_ENTITY_IN_RANGE_COORDS(sHospitalPedSimple[i].ped, vPlayerCurrentPos, 250.0) 	//cleanup the peds player far away
									SAFE_REMOVE_PED(sHospitalPedSimple[i].ped)
									sHospitalPedSimple[i].eState = SPS_CLEANED_UP
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HOSPITAL PED REMOVED - ", i) ENDIF #ENDIF
								ENDIF								
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
		
	//don't do this every frame
	IF GET_GAME_TIMER() - iTriggerScriptedPedestriansTimer > iAIUpdateDelay
		iTriggerScriptedPedestriansTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    triggers dialogue during the chase
///    uses mission global var - fCurrentPlaybackTime 
/// PARAMS:
///    bReturnOverrideTargetBlipFlashThisFrame - dialogue dictates if the blip flash needs to be overriden each frame for special case
PROC MANAGE_DIALOGUE_DURING_CHASE(BOOL &bReturnOverrideTargetBlipFlashThisFrame)
	INT iTempRandomLine
	TEXT_LABEL_31 tl31TempDialogueRoot
	enumSubtitlesState eDisplaySubtitles
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		iTimer_DialogueDelay = GET_GAME_TIMER()
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - IS_ANY_CONVERSATION_ONGOING_OR_QUEUED timer set fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELSE
		bIsImportantDialoguePlaying = FALSE
	ENDIF
	// detect when a interrupt line has finished so we can free up for another interrupt
	IF bIsInterruptDialoguePlaying
		IF IS_PED_UNINJURED(pedIndexDialogueInterrupter)
			IF NOT IS_AMBIENT_SPEECH_PLAYING(pedIndexDialogueInterrupter)
				pedIndexDialogueInterrupter = NULL
				bIsInterruptDialoguePlaying = FALSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - ped finished interrupt -  reset bIsInterruptDialoguePlaying reset  fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - IS_AMBIENT_SPEECH_PLAYING delay  bIsInterruptDialoguePlaying  fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ELSE
			pedIndexDialogueInterrupter = NULL
			bIsInterruptDialoguePlaying = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_IN_AIR(sNigelVehicle.vehicle)	// IF IS_VEHICLE_ON_ALL_WHEELS(sNigelVehicle.vehicle)
		iTimer_NigelVehicleJumpsTimer = GET_GAME_TIMER()
	ENDIF
	//GET_CURRENT_UNRESOLVED_SCRIPTED_CONVERSATION_LINE
	
	// only play the chase dialogue during that stage
	IF eMissionStage = MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
		
		IF fCurrentChaseDistance < ((NIGEL2_TARGET_ESCAPED_DISTANCE / 100) * (NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE * 100))	// same time the chase blip will kick in
		
			// Important dialogue which will trigger when conditions are met
			
			// first turn into the highstreet
			IF (fCurrentPlaybackTime > 3000.0 AND fCurrentPlaybackTime < 7500.0)
				IF NOT bDoneDialogue_InitialInstruction
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						// Don't display subtitles if objective is on screen						
						eDisplaySubtitles = DISPLAY_SUBTITLES
						IF IS_MESSAGE_BEING_DISPLAYED()
							eDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES
						ENDIF
						IF NIG2_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_INSTR", "NIG2_INSTR_13", CONV_PRIORITY_HIGH, eDisplaySubtitles)
							// He's taking a left turn down the high street, Jock.
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_InitialInstruction = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_INSTR_13 fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
			
			//turn when leaving highstreet
			IF (fCurrentPlaybackTime > 15500.0 AND fCurrentPlaybackTime < 16000.0)
				IF NOT bDoneDialogue_Chase[0]			
					//check player vehicle is an area where where it would see di napoli turn
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<-1237.778442,-763.467896,26.422665>>, <<-1186.702759,-829.477234,10.580473>>, 31.000000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		//ONLY DO THIS ONE IF THE FIRST CONVO HAS FINISHED
							IF NIG2_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_INSTR", "NIG2_INSTR_1", CONV_PRIORITY_HIGH)
								// He's turning left!
								bIsImportantDialoguePlaying = TRUE
								bDoneDialogue_Chase[0] = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_INSTR_1 fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
			//Tunnel section
			IF (fCurrentPlaybackTime > 45500.0 AND fCurrentPlaybackTime < 47500.0)	// B*1545310 - reduced from 46500 to make it trigger sooner
				IF NOT bDoneDialogue_Chase[1]				
					//check player vehicle is in area where he'd see the truck
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<-664.53711, -580.79431, 29.308285>>, <<-590.720276,-581.193237,23.308285>>, 13.500000) // B*1545310 - prev <<-661.360962,-581.020081,29.308285>>
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG_SP1", CONV_PRIORITY_HIGH)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
							
							// Oh my word! Jock! Do you see that lorry? 
							// What the fuck's a lorry?
							// Have a little faith. Anyone'd think this was my first kidnapping.  
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_Chase[1] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG_SP1 fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
			//entrance to carpark
			IF (fCurrentPlaybackTime > 69000.0 AND fCurrentPlaybackTime < 71000.0) //68500.0 AND fCurrentPlaybackTime < 70000.0)
				IF NOT bDoneDialogue_Chase[2]			
					//check player vehicle is in area where he'd him head into the carpark
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<-440.785736,-828.323730,35.735603>>, <<-511.811157,-826.655945,27.668936>>, 60.000000)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF					
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG_SP2", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
							// Mind out!
							// He's going into the car park!
							// The car park. No worries, mate.
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_Chase[2] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG_SP2 fc = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			//entrance to highway
			IF (fCurrentPlaybackTime > 85000.0 AND fCurrentPlaybackTime < 89000.0)
				IF NOT bDoneDialogue_Chase[3]				
					//check player vehicle has entered the highway
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<-432.327759,-540.988037,44.071632>>, <<-433.248322,-485.564941,22.470394>>, 60.000000)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF					
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG_SP3", CONV_PRIORITY_HIGH)
							// Oh my giddy aunt!  
							// Everything okay there, Earl Crazycakes?  
							// My prostate has taken quite a beating.  
							// Do you have to lower the tone?
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_Chase[3] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG_SP3 FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF					
			//slip road highway exit
			IF (fCurrentPlaybackTime > 104000.0 AND fCurrentPlaybackTime < 110000.0)
				IF NOT bDoneDialogue_Chase[4]
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		// B*1392677 - moaning about dialogue getting cut off
						//check player vehicle is on highway following napoli
						IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<81.946068,-518.602417,38.074284>>, <<-107.368584,-519.744385,27.815357>>, 60.000000)					
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								KILL_ANY_CONVERSATION()	// B*1392677 - moaning about dialogue getting cut off
							ENDIF					
							IF NIG2_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_INSTR", "NIG2_INSTR_12", CONV_PRIORITY_HIGH)
								// Take the slip road Jock.
								bIsImportantDialoguePlaying = TRUE
								bDoneDialogue_Chase[4] = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_INSTR_12 FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			//entrance to hospital
			IF (fCurrentPlaybackTime > 113000.0 AND fCurrentPlaybackTime < 116000.0)
				IF NOT bDoneDialogue_Chase[5]
					//check player vehicle could see hospital
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<254.999435,-594.382629,49.602520>>, <<270.464294,-527.226013,39.682480>>, 70.000000)
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF					
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_HOSP01", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
							// Is he going through the hospital?  
							// Oh my lord!  
							// Hold on to your corsets.  
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_Chase[5] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_HOSP01 FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			IF fCurrentPlaybackTime > 118000.0	//only do these checks towards end of the chase
				//go out the window comment
				IF NOT bDoneDialogue_Chase[6]
					//lead up to window
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<338.011139,-581.905457,41.577534>>, <<350.986938,-586.752930,46.077515>>, 6.750000)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "inside do hospital window dialogue") ENDIF #ENDIF
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF		
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_HWH", CONV_PRIORITY_HIGH)
							// Uh, seems rude not to follow him through the window.  
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_Chase[6] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_HWH FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF			
				//go out the window comment
				IF NOT bDoneDialogue_Chase[7]
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<368.571442,-594.946655,45.542202>>, <<389.501495,-614.127930,27.117891>>, 20.750000)
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_HWH")	// // Uh, seems rude not to follow him through the window.  
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_HWS", CONV_PRIORITY_HIGH)
								// Bloody hell!  
								bIsImportantDialoguePlaying = TRUE
								bDoneDialogue_Chase[7] = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - important dialogue - NIG2_HWS FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bIsInterruptDialoguePlaying

		// Nigel's vehicle jumps and/or flips over
		IF NOT bDoneDialogue_UpsideDownOrAirbourne
		
			IF IS_VEHICLE_STUCK_ON_ROOF(sNigelVehicle.vehicle)
			OR IS_VEHICLE_STUCK_TIMER_UP(sNigelVehicle.vehicle, VEH_STUCK_ON_ROOF, 1000)	// IS_VEHICLE_STUCK_ON_ROOF didn't seem to work for this
			OR IS_VEHICLE_STUCK_TIMER_UP(sNigelVehicle.vehicle, VEH_STUCK_ON_SIDE, 1000)
			OR HAS_TIME_PASSED(iTimer_NigelVehicleStuckOnRoofFail, 500)
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//ENDIF
				// random dialogue, so only one line will be selected
				tl31TempDialogueRoot = "NIG2_FLIP_"
				iTempRandomLine = GET_RANDOM_INT_IN_RANGE(1, 3)	// selection 1-2
				tl31TempDialogueRoot += iTempRandomLine
				
				// B*1366130 - use INTERRUPT_CONVERSATION instead since dialogue doesn't play otherwise
				
				IF iTempRandomLine = 1
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_FLIP_1A:NIG2AUD] NIG2_CCAA
					INTERRUPT_CONVERSATION(sNigelPed.ped, "NIG2_CCAA", "NIGEL")
					// A little rusty on the old stunts, Jock.
					bDoneDialogue_UpsideDownOrAirbourne = TRUE
					pedIndexDialogueInterrupter = sNigelPed.ped
					bIsInterruptDialoguePlaying = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - nigel's vehicle flipped dialogue : NIG2_CCAA FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					EXIT
				ELSE
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_FLIP_2A:NIG2AUD] NIG2_CCAB
					INTERRUPT_CONVERSATION(sMrsThornhillPed.ped, "NIG2_CCAB", "MRSTHORNHILL")
					// It's all gone topsy-turvy upside down cake.  
					bDoneDialogue_UpsideDownOrAirbourne = TRUE
					pedIndexDialogueInterrupter = sMrsThornhillPed.ped
					bIsInterruptDialoguePlaying = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - Mrs Thornhill's vehicle flipped dialogue : NIG2_CCAB FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					EXIT		
				ENDIF

			// airborne - only do if we aren't currently doing the upside down dialogue
			ELIF IS_ENTITY_IN_AIR(sNigelVehicle.vehicle)	// IF NOT IS_VEHICLE_ON_ALL_WHEELS(sNigelVehicle.vehicle)
				
				IF HAS_TIME_PASSED(iTimer_NigelVehicleJumpsTimer, 500) //make sure Nigel's car has been in the air for a decent chunk of time, iTimer_NigelVehicleJumpsTimer is reset at top of proc
					
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_HWH")	// bDoneDialogue_Chase[6] // prevent this playing if inital jump out window dialogue is waiting to trigger
					AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_HWS")	// bDoneDialogue_Chase[7] // prevent this playing if inital jump out window dialogue is waiting to trigger
						
						// allows current conversations to be interrupted with the specific comments
			
						// random select between Nigel and Mrs Thornhill comments
						IF (GET_RANDOM_INT_IN_RANGE(0, 11) > 5)
							// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_JUMP_1A:NIG2AUD] NIG2_CDAA
							tl31TempDialogueRoot = "NIG2_CDAA"					
							// Just like in the films! 
							INTERRUPT_CONVERSATION(sNigelPed.ped, tl31TempDialogueRoot, "NIGEL")
							pedIndexDialogueInterrupter = sNigelPed.ped
						ELSE
							// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_JUMP_2A:NIG2AUD] NIG2_CDAB
							tl31TempDialogueRoot = "NIG2_CDAB"					
							// Mercy me, we're airborne!   
							INTERRUPT_CONVERSATION(sMrsThornhillPed.ped, tl31TempDialogueRoot, "MRSTHORNHILL")
							pedIndexDialogueInterrupter = sMrsThornhillPed.ped
						ENDIF
						iTimer_NigelVehicleJumpsTimer = GET_GAME_TIMER()
						bIsInterruptDialoguePlaying = TRUE
						bDoneDialogue_UpsideDownOrAirbourne = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - nigel's vehicle jump dialogue, context = ", tl31TempDialogueRoot, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// reset dialogue trigger when vehicle is back on the ground
			IF NOT IS_ENTITY_IN_AIR(sNigelVehicle.vehicle)
				IF IS_VEHICLE_ON_ALL_WHEELS(sNigelVehicle.vehicle)
				OR (NOT IS_VEHICLE_STUCK_ON_ROOF(sNigelVehicle.vehicle)
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(sNigelVehicle.vehicle, VEH_STUCK_ON_ROOF, 1000)	// IS_VEHICLE_STUCK_ON_ROOF didn't seem to work for this
				AND NOT IS_VEHICLE_STUCK_TIMER_UP(sNigelVehicle.vehicle, VEH_STUCK_ON_SIDE, 1000))
					bDoneDialogue_UpsideDownOrAirbourne = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - reset bDoneDialogue_UpsideDownOrAirbourne") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		// temp wrap to until code fix for bug 1443371 & 1443404 
		IF NOT bIsImportantDialoguePlaying

			// Trevor drives into a ped
			IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
			AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
				
				// allows current conversations to be interrupted with the specific comments
				
				// random select between Nigel and Mrs Thornhill comments
				IF (GET_RANDOM_INT_IN_RANGE(0, 11) > 5)
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_PEDA:NIG2AUD] NIG2_CQAA
					tl31TempDialogueRoot = "NIG2_CQAA"					
					// It's okay, they all have private healthcare.
					// Jock, I think you just hit someone!
					// Mrs. Thornhill was throwing herself under cars earlier.
					INTERRUPT_CONVERSATION(sNigelPed.ped, tl31TempDialogueRoot, "NIGEL")
					pedIndexDialogueInterrupter = sNigelPed.ped
				ELSE
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_PED2A:NIG2AUD] NIG2_CPAA
					tl31TempDialogueRoot = "NIG2_CPAA"					
					// That's right, Jock, no prisoners!
					// Just a scratch!  
					INTERRUPT_CONVERSATION(sMrsThornhillPed.ped, tl31TempDialogueRoot, "MRSTHORNHILL")
					pedIndexDialogueInterrupter = sMrsThornhillPed.ped
				ENDIF
				bIsInterruptDialoguePlaying = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - player hit ped dialogue, context = ", tl31TempDialogueRoot, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				EXIT
			ENDIF
			
			// collide dialogue
			IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sNigelVehicle.vehicle)
				
				// allows current conversations to be interrupted with the specific comments
				
				// random select between Nigel and Mrs Thornhill comments
				IF (GET_RANDOM_INT_IN_RANGE(0, 11) > 5)
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_CRASHA:NIG2AUD] NIG2_COAA
					tl31TempDialogueRoot = "NIG2_COAA"					
					// Careful!
					// Oh, the hernia!
					// Maybe I should have driven.
					INTERRUPT_CONVERSATION(sNigelPed.ped, tl31TempDialogueRoot, "NIGEL")
					pedIndexDialogueInterrupter = sNigelPed.ped
				ELSE
					// context uses to key geenrated in AmericanDialogueFiles e.g. - [NIG2_CRASH2A:NIG2AUD] NIG2_CNAA
					tl31TempDialogueRoot = "NIG2_CNAA"					
					// Mind out!
					// The hire car!
					// We'll never get that deposit back! 
					INTERRUPT_CONVERSATION(sMrsThornhillPed.ped, tl31TempDialogueRoot, "MRSTHORNHILL")
					pedIndexDialogueInterrupter = sMrsThornhillPed.ped
				ENDIF			
				bIsInterruptDialoguePlaying = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - vehicle collides dialogue, context = ", tl31TempDialogueRoot, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// Medium important dialogue - will only trigger if no other dialogue of same importance of higher is playing.			
	IF NOT bIsImportantDialoguePlaying
	AND NOT bIsInterruptDialoguePlaying
	
		//warn player about shooting at target
		IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sTargetVehicle.vehicle, PLAYER_PED_ID())
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_SHOOTS")
				AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_SHOOTSN")			
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					// random pick between nigel or mrs t line
					IF GET_RANDOM_INT_IN_RANGE(0, 11) > 5
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_SHOOTS", CONV_PRIORITY_HIGH)
							// We didn't say kill the poor man!  
							// Stop shooting! I can't hear myself think!
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetVehicle.vehicle)
							bIsInterruptDialoguePlaying = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - warn shooting dialogue mrs t - NIG2_SHOOTS") ENDIF #ENDIF
							EXIT
						ENDIF
					ELSE
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_SHOOTSN", CONV_PRIORITY_HIGH)
							// Why do you Americans have to shoot at everything?   
							// He's not much use to us in a body bag, Jock!  
							// He thinks he's on one of those drive-bys, Mrs Thornhill!
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetVehicle.vehicle)
							bIsInterruptDialoguePlaying = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - warn shooting dialogue nigel - NIG2_SHOOTSN") ENDIF #ENDIF
							EXIT
						ENDIF
					ENDIF
				ENDIF
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetVehicle.vehicle)
			ENDIF
			
			//warn player about ramming target
			IF NOT bDoneDialogue_WarnRamming				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sTargetVehicle.vehicle, PLAYER_PED_ID())
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE01", CONV_PRIORITY_HIGH)
						// Don't go crazy, we'd prefer him in one piece!
						// Oh yeah, nobody here go crazy. Heaven forbid.  
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sTargetVehicle.vehicle)
						bIsImportantDialoguePlaying = TRUE
						bDoneDialogue_WarnRamming = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - rammin dialogue - NIG2_CHASE01") ENDIF #ENDIF
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		// trevor gets a wanted level
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF NOT bDoneDialogue_TrevorGetsWantedLevel
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
				IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_WANTED", CONV_PRIORITY_HIGH)
					// Is that the old bill?
					// The police, what terrible luck.   
					bDoneDialogue_TrevorGetsWantedLevel = TRUE
					bIsImportantDialoguePlaying = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - wanted dialogue - NIG2_WANTED") ENDIF #ENDIF
				ENDIF
			ELIF NOT bDoneDialogue_CopsSeenChasing
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG2_WANTED")
					//IF IS_COP_VEHICLE_IN_AREA_3D(vTemp1, vTemp2)	// not found a cheap way to detect cop car on screen yet
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_POLICE", CONV_PRIORITY_HIGH)
						// Do you think we'll be on one of those cop chase programmes?  
						// The LSPD, how glamorous!  
						bDoneDialogue_CopsSeenChasing = TRUE
						bIsImportantDialoguePlaying = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - cops seen dialogue - NIG2_POLICE") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		// Low priority dialogue - only plays if no other dialogue is playing
		// only play the chase dialogue during that stage
		IF eMissionStage = MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
		
			IF fCurrentChaseDistance < ((NIGEL2_TARGET_ESCAPED_DISTANCE / 100) * (NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE * 100))	// same time the chase blip will kick in
			
				IF NOT bIsImportantDialoguePlaying
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					IF HAS_TIME_PASSED(iTimer_DialogueDelay, 500)
						
						// general banter dialogue
						IF (fCurrentPlaybackTime < 116000.0)	// DONT ALLOW BANTER FROM HOSPTIAL SECTION ONWARDS
						
							// driving wrong side of the road
							IF NOT bDoneDialogue_DrivingOnWrongSideRoad								
								IF fCurrentPlaybackTime > 20000.0	// got out of pedestrian area first
									IF IS_TREVOR_DRIVING_ON_ROAD_WRONG_WAY()
										IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_LEFT", CONV_PRIORITY_HIGH)
											// Glad to see you on the proper side of the road, dear boy.
											bDoneDialogue_DrivingOnWrongSideRoad = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - driving wrong side of the road dialogue - NIG2_LEFT") ENDIF #ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ENDIF 
						
							//BANTER
							IF NOT bDoneDialogue_BanterChase[0]
								IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE02", CONV_PRIORITY_HIGH)
									// Did you ever imagine we might get a private audience with Al Di Napoli?
									// Only in my wildest dreams.
									// All those nights spent roleplaying in mother's utility room, 
									// if only we'd known!  
									bDoneDialogue_BanterChase[0] = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE02") ENDIF #ENDIF
								ENDIF
							ENDIF
							IF NOT bDoneDialogue_BanterChase[1]
								IF bDoneDialogue_Chase[0] //after first left dialogue
								OR (fCurrentPlaybackTime > 16500.0)	// ahalf  second after its bDoneDialogue_Chase[0] trigger time
									IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE05", CONV_PRIORITY_HIGH)
										// I loved him in The Redeemer. Oh, Nigel, do your Al Di Napoli line!
										// "This is turban warfare, motherfucker!"   
										// It's uncanny!  
										// Oh dear, poor Jock must think we're batty!  
										// Oh, don't be silly.
										bDoneDialogue_BanterChase[1] = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE05") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELIF NOT bDoneDialogue_BanterChase[2]
								IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE08", CONV_PRIORITY_HIGH)								
										// I hear that behind his charitable public face, he's remarkably right-wing. 
										// One can but hope.  
										// Liberal in the bedroom, conservative at the border, Mrs Thornhill!
										// I love this country!
										bDoneDialogue_BanterChase[2] = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE08") ENDIF #ENDIF
									ENDIF																
							ELIF NOT bDoneDialogue_BanterChase[3]
								IF bDoneDialogue_Chase[1]	//only trigger after tunnel
								OR (fCurrentPlaybackTime > 52500.0)	// half a second after its bDoneDialogue_Chase[1] trigger time
									IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE06", CONV_PRIORITY_HIGH)
										// What on earth will we feed the man?  
										// I hadn't even thought of that. He probably eats sushi or quinoa. Is that even how you even say it?
										// What if he wants meth?
										// Don't get flustered again, Nigel.  
										bDoneDialogue_BanterChase[3] = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE06") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELIF NOT bDoneDialogue_BanterChase[4]
								IF bDoneDialogue_Chase[2]		// trigger after going in car park
								OR (fCurrentPlaybackTime > 71500.0)	// half a second after its bDoneDialogue_Chase[2] trigger time
									IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE03", CONV_PRIORITY_HIGH)
										// Oh, I hope he likes us!  
										// How could he not? This lock-up you've prepared sounds charming.  
										// Just be yourself, Mrs Thornhill.  
										bDoneDialogue_BanterChase[4] = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE03") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELIF NOT bDoneDialogue_BanterChase[5]
								IF bDoneDialogue_Chase[3]	//after highway dialogue
								OR (fCurrentPlaybackTime > 89500.0)	// half a second after its bDoneDialogue_Chase[3] trigger time
									IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE04", CONV_PRIORITY_HIGH)
										// This must be second nature to you, Jock. All those stunts you used to do.
										// And we saw you're up for governor of San Andreas!  
										// That's right. In between openly attacking celebrities all over town, I'm running for public office.
										// Such simplistic and uninhibited people. Marvelous.  
										bDoneDialogue_BanterChase[5] = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE04") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELIF NOT bDoneDialogue_BanterChase[6]
								IF NOT bDoneDialogue_Chase[5]	// don't trigger this after the hospital entrance comment
									IF bDoneDialogue_Chase[4]	//after slip road instruction
									OR (fCurrentPlaybackTime > 106500.0)	// half a second after its bDoneDialogue_Chase[4] trigger time
										//check player vehicle isn't still on highway waiting for sliproad comment
										IF NOT IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<81.946068,-518.602417,38.074284>>, <<-107.368584,-519.744385,27.815357>>, 60.000000)	
											IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CHASE07", CONV_PRIORITY_HIGH)								
												// This level of derangement, I'm impressed.  
												// Little by little I'm tip-toeing out of the comfort zone. 
												// Before I met Mrs Thornhill I was so vanilla.  
												// I had a very confined upbringing.
												// Well let me assure you there's no comfort in this zone.  
												bDoneDialogue_BanterChase[6] = TRUE
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - banter dialogue - NIG2_CHASE07") ENDIF #ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// dialogue for target escaping
	IF fCurrentChaseDistance > ((NIGEL2_TARGET_ESCAPED_DISTANCE / 100) * (NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE * 100))

		// catch up dialogue - Al Di Napoli escaping
		IF NOT bIsImportantDialoguePlaying
		
			IF NOT bDoneDialogue_CatchUp[2]
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					IF NOT bDoneDialogue_CatchUp[0]
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CU01", CONV_PRIORITY_HIGH)
							// Can't you go any faster?
							// Blame Nigel for renting this shitheap of a car.
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_CatchUp[0] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - catch up dialogue - NIG2_CU03") ENDIF #ENDIF
						ENDIF
					ELIF NOT bDoneDialogue_CatchUp[1]
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CU02", CONV_PRIORITY_HIGH)
							// We can't lose him, Jock!  
							// How will we all become the best of friends? 
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_CatchUp[1] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - catch up dialogue - NIG2_CU03") ENDIF #ENDIF
						ENDIF
					ELIF NOT bDoneDialogue_CatchUp[2]
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CU03", CONV_PRIORITY_HIGH)
							// Hurry up! We can't let him get away!  
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_CatchUp[2] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - catch up dialogue - NIG2_CU03") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	// B*1513201 - when target goes down the tunnel and player doesn't follow
	// needs to match up with UPDATE_FAIL_REASON
	ELIF fCurrentPlaybackTime > 40000.0	// enter tunnel
		IF fCurrentPlaybackTime < 58000.0	// exit tunnel
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-731.689087,-657.824158,26.269114>>, <<-448.393585,-657.125000,40.717342>>, 40.000000)	// the road area			
				IF NOT bDoneDialogue_WrongWaySpecialFail
					IF NOT bDoneDialogue_CatchUp[1]
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CU02", CONV_PRIORITY_HIGH)
							// We can't lose him, Jock!  
							// How will we all become the best of friends? 
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_CatchUp[1] = TRUE
							bDoneDialogue_WrongWaySpecialFail = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - special case Wrong way dialogue - NIG2_CU03") ENDIF #ENDIF
						ENDIF
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						// make sure this back is used
						IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_CU03", CONV_PRIORITY_HIGH)
							// Hurry up! We can't let him get away!  
							bIsImportantDialoguePlaying = TRUE
							bDoneDialogue_CatchUp[2] = TRUE
							bDoneDialogue_WrongWaySpecialFail = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - special case Wrong way dialogue - NIG2_CU03") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				bReturnOverrideTargetBlipFlashThisFrame = TRUE	// set to ensure blip flash is forced on for this situation
			ENDIF
		ENDIF
	ENDIf
ENDPROC

/// PURPOSE:
///    deals with pinning the hospital interior to memory, setting the interior active and setting the IPL groups
///    B*1504088 - need to request the ipl changes really early otherwise rayfire doesn't kick in quick enough
PROC MANAGE_HOSPITAL_STATE_DURING_CHASE()
	
	// make sure the IPL swaps are done first of all
	IF eHospitalInteriorState = HIS_SET_INTERIOR_IPLS_FOR_CHASE
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED) 			// sets the window to destroyed so player/target can drive through it
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)	// sets the interior to stream
		HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
		eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
		CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_SET_INTERIOR_IPLS_FOR_CHASE : FC = ", GET_FRAME_COUNT())
		
	// interior needs to be pinned into memory really early
	ELIF eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF IS_VALID_INTERIOR(HospitalInteriorIndex)
				IF NOT IS_INTERIOR_READY(HospitalInteriorIndex)	
					PIN_INTERIOR_IN_MEMORY(HospitalInteriorIndex)
				ELSE
					SET_INTERIOR_ACTIVE(HospitalInteriorIndex, TRUE)
					eHospitalInteriorState = HIS_SET_ENTITY_SET_FOR_CHASE
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_PIN_INTERIOR_IN_MEMORY : FC = ", GET_FRAME_COUNT())
				ENDIF
			ELSE
				HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE getting handle to valid interior : FC = ", GET_FRAME_COUNT())
			ENDIF	
		ENDIF
		
	// set the entity set once we know the interior is pinned in memory
	ELIF eHospitalInteriorState = HIS_SET_ENTITY_SET_FOR_CHASE
		IF IS_VALID_INTERIOR(HospitalInteriorIndex)
			IF IS_INTERIOR_READY(HospitalInteriorIndex)	
				SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_NORMAL)				// sets entity set for the rayfire doors
				eHospitalInteriorState = HIS_REFRESH_INTERIOR
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - SKIPPED done HIS_SET_ENTITY_SET_FOR_CHASE : FC = ", GET_FRAME_COUNT())
			ELSE
				eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_SET_ENTITY_SET_FOR_CHASE - error interior no longer ready! : FC = ", GET_FRAME_COUNT())
			ENDIF
		ELSE
			eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
			CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_SET_ENTITY_SET_FOR_CHASE - error interior no longer valid! : FC = ", GET_FRAME_COUNT())
		ENDIF
	
	// refresh the interior once everything should be setup correctly
	ELIF eHospitalInteriorState = HIS_REFRESH_INTERIOR
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF IS_VALID_INTERIOR(HospitalInteriorIndex)
				IF IS_INTERIOR_READY(HospitalInteriorIndex)	
					REFRESH_INTERIOR(HospitalInteriorIndex)
					eHospitalInteriorState = HIS_READY	// HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR	//
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - SKIPPED ^ done HIS_REFRESH_INTERIOR : FC = ", GET_FRAME_COUNT())
				ELSE
					PIN_INTERIOR_IN_MEMORY(HospitalInteriorIndex)
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - re pinnning in memory : FC = ", GET_FRAME_COUNT())
					//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
					//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - error interior no longer ready! : FC = ", GET_FRAME_COUNT())
				ENDIF
			ELSE
				HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - getting handle to valid interior : FC = ", GET_FRAME_COUNT())
				//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
				//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - error interior no longer valid! : FC = ", GET_FRAME_COUNT())
			ENDIF
		ENDIF
		
	/* removed for now as Ian thinks it shouldn't be needed anymore because the interior is pinned so early
	// load the scene at the interior (5m inside the interior apparenlt not too expensive and should do the job)
	ELIF eHospitalInteriorState = HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR		
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 308.96793, -591.07501, 43.29187 >>, 350.0) // 550.0) //350.0)
				NEW_LOAD_SCENE_STOP()
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START_SPHERE(<< 308.96793, -591.07501, 43.29187 >>, 5.0, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)	// pos more in the middle of the interior
					eHospitalInteriorState = HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE NEW_LOAD_SCENE_START_SPHERE(<< 326.36642, -590.09155, 42.29188 >>, 5.0 : FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		ENDIF
		
	// check for the new load scene completing
	ELIF eHospitalInteriorState = HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				IF IS_NEW_LOAD_SCENE_LOADED()
					eHospitalInteriorState = HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR : FC = ", GET_FRAME_COUNT())
				// if player moves away before load is completed bin it off
				ELIF NOT IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 308.96793, -591.07501, 43.29187 >>, 650.0) // 550.0) //350.0)
					NEW_LOAD_SCENE_STOP()
					eHospitalInteriorState = HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - binned off HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR for range check : FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		ENDIF
		
	// bin off the new load scene once chase has reached the area
	ELIF eHospitalInteriorState = HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR
		IF NOT IS_REPLAY_BEING_SET_UP()
			//IF IS_NEW_LOAD_SCENE_ACTIVE()
			//	NEW_LOAD_SCENE_STOP()
			//	eHospitalInteriorState = HIS_READY
			//	CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR : FC = ", GET_FRAME_COUNT())
			//ENDIF
			IF NOT IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 308.96793, -591.07501, 43.29187 >>, 650.0) // 550.0) //350.0)
				NEW_LOAD_SCENE_STOP()
				eHospitalInteriorState = HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - binned off HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR for range check : FC = ", GET_FRAME_COUNT())
			ENDIF
		ENDIF
	*/
	
	// everything should be setup by this point, just monitor things going wrong
	ELIF eHospitalInteriorState = HIS_READY
		IF IS_VALID_INTERIOR(HospitalInteriorIndex)
			IF NOT IS_INTERIOR_READY(HospitalInteriorIndex)	
				PIN_INTERIOR_IN_MEMORY(HospitalInteriorIndex)
				CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_READY - re pinnning in memory : FC = ", GET_FRAME_COUNT())
				//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
				//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_READY - error interior no longer ready! : FC = ", GET_FRAME_COUNT())
			ENDIF
		ELSE
			HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
			CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_READY - getting handle to valid interior : FC = ", GET_FRAME_COUNT())
			//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
			//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_READY - error interior no longer valid! : FC = ", GET_FRAME_COUNT())
		ENDIF	
		
	//ELIF eHospitalInteriorState = HIS_ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles rayfire doors triggering
///    uses mission global var - fCurrentPlaybackTime
PROC MANAGE_RAYFIRE_HOSPITAL_DOORS()	
	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow during replay setup / stage skips
	AND bFinishedStageSkipping
	
		// B*1572752 - now creating doors object in script to display until rayfire anim kicks in - rayfire object always struggled to stream prior to start state
		IF eHospitalDoorsObjectState = HDO_CREATE
			IF (fCurrentPlaybackTime >= 99000.0)	// point shortly after object normal exists
				IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 308.96793, -591.07501, 43.29187 >>, 500.0)	// dist player from rayfire object
					IF NOT DOES_ENTITY_EXIST(sObjectHospitalDoors.objectIndex)
						REQUEST_MODEL(sObjectHospitalDoors.modelName)
						IF HAS_MODEL_LOADED(sObjectHospitalDoors.modelName)
							sObjectHospitalDoors.objectIndex = CREATE_OBJECT(sObjectHospitalDoors.modelName, sObjectHospitalDoors.vPos)
							IF IS_ENTITY_ALIVE(sObjectHospitalDoors.objectindex)
								SET_ENTITY_ROTATION(sObjectHospitalDoors.objectindex, sObjectHospitalDoors.vRot)
								SET_ENTITY_COORDS_NO_OFFSET(sObjectHospitalDoors.objectindex, sObjectHospitalDoors.vPos)
								FREEZE_ENTITY_POSITION(sObjectHospitalDoors.objectindex, TRUE)
								SET_MODEL_AS_NO_LONGER_NEEDED(sObjectHospitalDoors.modelName)
								eHospitalDoorsObjectState = HDO_WAIT_FOR_DELETE
								CPRINTLN(DEBUG_MISSION, "OBJECT Hospital Doors Progress, created doors : eHospitalDoorsObjectState > HDO_WAIT_FOR_DELETE")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		// same frame the rayfire object begins to playback from this object
		ELIF eHospitalDoorsObjectState = HDO_WAIT_FOR_DELETE
			IF (fCurrentPlaybackTime >= NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT)	
				IF DOES_ENTITY_EXIST(sObjectHospitalDoors.objectIndex)
					SAFE_DELETE_OBJECT(sObjectHospitalDoors.objectIndex)
					eHospitalDoorsObjectState = HDO_END
					CPRINTLN(DEBUG_MISSION, "OBJECT Hospital Doors Progress, doors deleted for rayfire : eHospitalDoorsObjectState > HDO_END")
				ENDIF
			ENDIF
		ELIF eHospitalDoorsObjectState = HDO_END
		
		ENDIF
		
		// grab the rayfire_index
		IF iRayfireProgress_HospitalDoors = 0
			IF (fCurrentPlaybackTime >= 99000.0)	// point shortly after object normal exists
				IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, << 308.96793, -591.07501, 43.29187 >>, 500.0)	// dist player from rayfire object
					IF eHospitalInteriorState > HIS_REFRESH_INTERIOR	// point before dist checks kick in
						rfHospitalDoors = GET_RAYFIRE_MAP_OBJECT(<< 299.4302, -584.8925, 42.2629 >>, 100, "DES_hospitaldoors")
						
						IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfHospitalDoors)
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors, RFMO_STATE_RESET)
							iRayfireProgress_HospitalDoors++
							CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, DOES_RAYFIRE_MAP_OBJECT_EXIST and RFMO_STATE_RESET at playback time = ", fCurrentPlaybackTime)			
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_OutputRayfireHospitalDoorsProgress CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, in priming stage - Rayfire Object doesn't exist") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
		ELIF iRayfireProgress_HospitalDoors = 1
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfHospitalDoors)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors) = RFMO_STATE_PRIMED
					IF (fCurrentPlaybackTime >= NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT)					
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors, RFMO_STATE_START_ANIM)
						CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, in start anim stage - Rayfire Object Anim Started", fCurrentPlaybackTime)
						iRayfireProgress_HospitalDoors++
					ENDIF
				ELSE
					IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors) != RFMO_STATE_PRIMING
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors, RFMO_STATE_PRIMING)
						#IF IS_DEBUG_BUILD IF bDebug_OutputRayfireHospitalDoorsProgress CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, priming") ENDIF #ENDIF				
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_OutputRayfireHospitalDoorsProgress CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, in start anim stage - Rayfire object no longer exists so going back to priming stage") ENDIF #ENDIF
				iRayfireProgress_HospitalDoors--
			ENDIF
				
		/*ELIF iRayfireProgress_HospitalDoors = 2
		
			// New building swap setup - commented out whilst window isn't working...also calling this here causes the interior to reset, so it's a visible stream issue
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfHospitalDoors)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors) = RFMO_STATE_END
					// need to set the es building state to destroyed when after the rayfire has finished
					SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)	// contains the rayfire for the front door
					iRayfireProgress_HospitalDoors++
					#IF IS_DEBUG_BUILD
						IF bDebug_OutputRayfireHospitalDoorsProgress
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, rayfire ended - set BUILDINGNAME_ES_PILLBOX_HILL to BUILDINGSTATE_DESTROYED : pbtime = ", fCurrentPlaybackTime) ENDIF #ENDIF
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				// need to set the es building state to destroyed when after the rayfire has finished
				SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)	// contains the rayfire for the front door
				iRayfireProgress_HospitalDoors++
				#IF IS_DEBUG_BUILD
					IF bDebug_OutputRayfireHospitalDoorsProgress
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RAYFIRE Hospital Doors Progress, object doesn't exist - set BUILDINGNAME_ES_PILLBOX_HILL to BUILDINGSTATE_DESTROYED : pbtime = ", fCurrentPlaybackTime) ENDIF #ENDIF
					ENDIF
				#ENDIF				
			ENDIF*/
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bDebug_OutputRayfireHospitalDoorsCurrentState	
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfHospitalDoors)
					RAY_FIRE_MAP_OBJECT_STATE rfsTemp
					rfsTemp = GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors)
					SWITCH rfsTemp
						CASE RFMO_STATE_ANIMATING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_ANIMATING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_ANIMATING ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_END
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_END = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_END ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_ENDING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_ENDING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_ENDING ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_INIT
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_INIT = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_INIT ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_INVALID
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_INVALID = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_INVALID ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_PAUSE
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PAUSE = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PAUSE ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_PRIMED
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PRIMED = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PRIMED ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_PRIMING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PRIMING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_PRIMING ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_RESET
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_RESET = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_RESET ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_RESUME
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_RESUME = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_RESUME ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_START
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_START = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_START ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_START_ANIM
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_START_ANIM = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_START_ANIM ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_STARTING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_STARTING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_STARTING ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_SYNC_ENDING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_SYNC_ENDING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_SYNC_ENDING ") ENDIF #ENDIF
						BREAK
						CASE RFMO_STATE_SYNC_STARTING
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_SYNC_STARTING = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - RFMO_STATE_SYNC_STARTING ") ENDIF #ENDIF
						BREAK
						DEFAULT
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - UNKNOWN = ", fCurrentPlaybackTime) ENDIF #ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "-------	RAYFIRE HOSPITAL DOORS STATE - UNKNOWN ") ENDIF #ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles requesting and triggering of audio in the hospital
///    uses mission global var - fCurrentPlaybackTime
PROC MANAGE_HOSPITAL_SOUND_FX()
	IF NOT bDone_RequestHospitalAudio
		IF (fCurrentPlaybackTime > (NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT - 4000.0))
			REQUEST_SCRIPT_AUDIO_BANK("NIGEL_02_CRASH_A")
			REQUEST_SCRIPT_AUDIO_BANK("NIGEL_02_CRASH_B")
			REQUEST_SCRIPT_AUDIO_BANK("PANIC_WALLA_INTERIOR")			
			IF REQUEST_SCRIPT_AUDIO_BANK("NIGEL_02_CRASH_A")
			AND REQUEST_SCRIPT_AUDIO_BANK("NIGEL_02_CRASH_B")
			AND REQUEST_SCRIPT_AUDIO_BANK("PANIC_WALLA_INTERIOR")
				bDone_RequestHospitalAudio = TRUE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "AUDIO BANKS LOADED FOR HOSPITAL AT ", fCurrentPlaybackTime) ENDIF #ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "WAITING FOR AUDIO BANKS TO LOAD FOR HOSPITAL") ENDIF #ENDIF
			ENDIF
		ENDIF
	ELSE
		//sounds triggered by the vehicle driving through
		SWITCH iHospitalSoundFX_Progress
			CASE 0
				IF (fCurrentPlaybackTime >= NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1,"DOOR_CRASH" , << 299.5664, -584.6757, 42.3020 >>, "NIGEL_02_SOUNDSET")						
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	DOOR_CRASH TRIGGERED and walla") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			CASE 1
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_CRASH_SCREAMS_01_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1, "SCREAMS", << 310.5149, -595.4158, 42.3020 >>, "NIGEL_02_SOUNDSET")		
					FORCE_PED_PANIC_WALLA()	// added in attempt to make the peds scream
					PLAY_SOUND_FROM_COORD(-1, "PANIC_WALLA", << 316.4345, -589.3902, 42.2919 >>, "NIGEL_02_SOUNDSET")	// tuck shop area
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	SCREAMS 01 TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			CASE 2
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_WALL_SMASH_01_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1,"WALL_CRASH" , << 325.66, -589.01, 42.30 >>, "NIGEL_02_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	WALL_CRASH 1 TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			CASE 3
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_WALL_SMASH_02_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1,"WALL_CRASH" , << 330.13, -585.72, 42.42 >>, "NIGEL_02_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	WALL_CRASH 2 TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			CASE 4
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_CRASH_SCREAMS_02_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1, "SCREAMS", << 331.7845, -578.5184, 42.3167 >>, "NIGEL_02_SOUNDSET")
					PLAY_SOUND_FROM_COORD(-1, "PANIC_WALLA", <<354.6585, -584.2177, 42.3150>>, "NIGEL_02_SOUNDSET")	// near window
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	SCREAMS 02 TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK		
			CASE 5
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_WALL_SMASH_03_TRIGGER_POINT)
					PLAY_SOUND_FROM_COORD(-1,"WALL_CRASH" , << 337.89, -582.38, 42.33 >>, "NIGEL_02_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	WALL_CRASH 3 TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			CASE 6
				IF (fCurrentPlaybackTime >= NIGEL2_HOSPITAL_WINDOW_SMASH_TRIGGER_POINT)	//NOTE: recording finishes before window so this value isn't exact.
					PLAY_SOUND_FROM_COORD(-1,"WINDOW_CRASH" , << 364.64, -595.43, 42.54 >>, "NIGEL_02_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	SFX	-	WINDOW_CRASH TRIGGERED") ENDIF #ENDIF
					iHospitalSoundFX_Progress++
				ENDIF
			BREAK
			DEFAULT
			BREAK		
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles particle fx inside the hospital
///    uses mission global var - fCurrentPlaybackTime
PROC MANAGE_HOSPITAL_DEBRIS_EFFECT()
	FLOAT fTriggerTime = 115225.0
	IF NOT bIsHospitalDebrisEffectActive	
		IF NOT bHasPTFX_ForHospitalDebrisBeenRequested
			IF fCurrentPlaybackTime > (fTriggerTime - 5000.0)
				REQUEST_PTFX_ASSET()
				bHasPTFX_ForHospitalDebrisBeenRequested = TRUE
			ENDIF
		ELSE
			IF fCurrentPlaybackTime > fTriggerTime
				IF HAS_PTFX_ASSET_LOADED()
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(HospitalDebris_PTFX_ID)
						HospitalDebris_PTFX_ID = START_PARTICLE_FX_LOOPED_AT_COORD("scr_rcn2_ceiling_debris", << 325.0, -589.0, 45.0 >>, << 0.0, 0.0, 0.0 >>)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PTFX EFFECT HOSPITAL DEBRIS STARTED") ENDIF #ENDIF
						bIsHospitalDebrisEffectActive = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT HAS_PTFX_ASSET_LOADED()
				REQUEST_PTFX_ASSET()
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUESTING SCRIPT PTFX") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle triggering the FX paper trail on Nigel's car when it jumps out the hospital window
PROC MANAGE_HOSPITAL_JUMP_FX()
	IF NOT bDone_HospitalJumpPaperTrailFX
		FLOAT fTriggerTime = 115225.0	// wait until chase has got close to the hospital (using same value as debris FX)
		IF fCurrentPlaybackTime > (fTriggerTime)
			REQUEST_PTFX_ASSET()
			IF HAS_PTFX_ASSET_LOADED()
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(HospitalJump_PTFX_ID)
					IF GET_ENTITY_SPEED(sNigelVehicle.vehicle) > 8.0
						IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<368.910980,-599.172546,41.238842>>, <<366.015289,-596.099976,44.985764>>, 6.500000) //<<368.14462, -600.03833,40.5>>, <<365.451416,-595.751282,45.0>>, 6.500000, TRUE)
							HospitalJump_PTFX_ID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_rcn2_debris_trail", sNigelVehicle.vehicle, << 0.0, -1.5, -0.4 >>, << 0.0, 0.0, 0.0 >>, 0.3)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_HOSPITAL_JUMP_FX : PTFX hospital jump started FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							bDone_HospitalJumpPaperTrailFX = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalJump_PTFX_ID)
			IF GET_ENTITY_SPEED(sNigelVehicle.vehicle) < 4.0
			OR NOT IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<368.910980,-599.172546,41.238842>>, <<366.015289,-596.099976,44.985764>>, 6.500000)
				STOP_PARTICLE_FX_LOOPED(HospitalJump_PTFX_ID)
				bDone_HospitalJumpPaperTrailFX = FALSE	// TEMP!
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_HOSPITAL_JUMP_FX : PTFX hospital jump stopped FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    makes target vehicle use horn or show brake lights
///    Helps to warn player of up coming hazards.  Needs to be called every frame
///    uses mission global var - fCurrentPlaybackTime
PROC MANAGE_TARGET_BRAKING_AND_USING_HORN()	
	//Brake lights
	IF (fCurrentPlaybackTime > 3000.0 AND fCurrentPlaybackTime < 3800.0)	//first turn at start location down the highstreet	
	OR (fCurrentPlaybackTime > 14750.0 AND fCurrentPlaybackTime < 15250.0)	//left turn out of the highstreet	
	OR (fCurrentPlaybackTime > 28500.0 AND fCurrentPlaybackTime < 29400.0)	//swerve around on coming vehicle up the first hill section	
	OR (fCurrentPlaybackTime > 34000.0 AND fCurrentPlaybackTime < 35250.0)	//left turn toward tunnel entrance	
	OR (fCurrentPlaybackTime > 38800.0 AND fCurrentPlaybackTime < 41000.0)	//turn into tunnel	
	OR (fCurrentPlaybackTime > 47000.0 AND fCurrentPlaybackTime < 48300.0)	//right turn in the tunnel	
	OR (fCurrentPlaybackTime > 56000.0 AND fCurrentPlaybackTime < 57200.0)	//right turn out of tunnel section	
	OR (fCurrentPlaybackTime > 66750.0 AND fCurrentPlaybackTime < 67250.0)	//slight brake when taking the left turn just before the U turn	
	OR (fCurrentPlaybackTime > 68500.0 AND fCurrentPlaybackTime < 70500.0)	//going around truck on highway entrance.	
	OR (fCurrentPlaybackTime > 101500.0 AND fCurrentPlaybackTime < 102500.0)	//swerving around vehicle at slip road exit	
	OR (fCurrentPlaybackTime > 114500.0 AND fCurrentPlaybackTime < 116500.0)	//from hospital doors to first right turn.		
		SET_VEHICLE_BRAKE_LIGHTS(sTargetVehicle.vehicle, TRUE)
	ENDIF	
	//Horn
	IF (fCurrentPlaybackTime > 6500.0 AND fCurrentPlaybackTime < 7000.0)	//shopping area, first stretch
	OR (fCurrentPlaybackTime > 7500.0 AND fCurrentPlaybackTime < 8500.0)	//shopping area, first stretch	
	OR (fCurrentPlaybackTime > 10500.0 AND fCurrentPlaybackTime < 11250.0)	//shopping area, second stretch
	OR (fCurrentPlaybackTime > 11750.0 AND fCurrentPlaybackTime < 12250.0)	//shopping area, first stretch	
	OR (fCurrentPlaybackTime > 13250.0 AND fCurrentPlaybackTime < 13500.0)	//shopping area, third stretch
	OR (fCurrentPlaybackTime > 14000.0 AND fCurrentPlaybackTime < 14500.0)	//shopping area, first stretch	
		SET_HORN_PERMANENTLY_ON(sTargetVehicle.vehicle)
	ENDIF
ENDPROC

/// PURPOSE:
///    damage the target's vehicle when he crashes
///    uses mission global var - fCurrentPlaybackTime
PROC APPLY_VISUAL_DAMAGE_TO_TARGET_VEHICLE()
	IF NOT bDone_VisualDamageForTargetVehicle
		IF (fCurrentPlaybackTime > NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT)
			//B*1476195 - set Al Di Napoli health higher for this damage so it doesn't kill him.
			IF IS_PED_UNINJURED(sTargetPed.ped)
				IF (GET_ENTITY_HEALTH(sTargetPed.ped) < 200)
					SET_ENTITY_HEALTH(sTargetPed.ped, 200)
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
				SET_ENTITY_HEALTH(sTargetVehicle.vehicle, 1000)
				IF NOT IS_VEHICLE_DOOR_DAMAGED(sTargetVehicle.vehicle, SC_DOOR_BONNET)
					SET_VEHICLE_DOOR_BROKEN(sTargetVehicle.vehicle, SC_DOOR_BONNET, TRUE)
				ENDIF
				SET_VEHICLE_TYRES_CAN_BURST(sTargetVehicle.vehicle, TRUE)
				//SET_VEHICLE_TYRE_BURST(sTargetVehicle.vehicle, SC_WHEEL_CAR_FRONT_LEFT)	causes wheel to clip through the floor
				SMASH_VEHICLE_WINDOW(sTargetVehicle.vehicle, SC_WINDOW_FRONT_LEFT)
				SMASH_VEHICLE_WINDOW(sTargetVehicle.vehicle, SC_WINDOW_FRONT_RIGHT)
				//SET_VEHICLE_DIRT_LEVEL(sTargetVehicle.vehicle, 10.0)	need proper colours setting up first.
				POP_OUT_VEHICLE_WINDSCREEN(sTargetVehicle.vehicle)
				SET_VEHICLE_DAMAGE(sTargetVehicle.vehicle, << 0.0, 1.0, 0.1 >>, 800.0, 1850.0, TRUE)
				SET_VEHICLE_DAMAGE(sTargetVehicle.vehicle, << -0.2, 1.0, 0.5 >>, 50.0, 650.0, TRUE)
				SET_VEHICLE_DAMAGE(sTargetVehicle.vehicle, << -0.7, -0.2, 0.3 >>, 50.0, 500.0, TRUE)
				CPRINTLN(DEBUG_MISSION, "VISUAL DAMAGE APPLIED TO TARGET VEHICLE FC = ", GET_FRAME_COUNT())
				bDone_VisualDamageForTargetVehicle = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles triggering the requested mission music events
///    Since these cues need to be sequental, request event will only trigger if the previous event has triggered
/// PARAMS:
///    iMusicEvent - the event we want to trigger
PROC MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE(INT iMusicEvent)

	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
	
		IF iMusicEvent = iMissionMusicEventTriggerCounter
			SWITCH iMissionMusicEventTriggerCounter
				CASE NIGEL2_MUSIC_EVENT_START
					IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_START")
						iMissionMusicEventTriggerCounter++
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE : ", "NIGEL2_START") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE NIGEL2_MUSIC_EVENT_CAR
					IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_CAR")
						PREPARE_MUSIC_EVENT("NIGEL2_JUMP")
						iMissionMusicEventTriggerCounter++
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE : ", "NIGEL2_CAR") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE NIGEL2_MUSIC_EVENT_JUMP
					IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_JUMP", TRUE)
						iMissionMusicEventTriggerCounter++
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE : ", "NIGEL2_JUMP") ENDIF #ENDIF
					ENDIF
				BREAK
				CASE NIGEL2_MUSIC_EVENT_STOP
					IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_STOP")
						iMissionMusicEventTriggerCounter++
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE : ", "NIGEL2_STOP") ENDIF #ENDIF
					ENDIF
				BREAK
				DEFAULT
				BREAK
			ENDSWITCH
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE : ERROR - iMusicEvent = ", iMusicEvent, " but iMissionMusicEventTriggerCounter = ", iMissionMusicEventTriggerCounter) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles trigger correct music events after a skip has occured.  Replay system sets it's own music scene which 
///    we can't interupr
/// PARAMS:
///    eCurrentStage - which mission stage we are returning to
PROC SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS(MISSION_STAGE eCurrentStage)
	SWITCH eCurrentStage
		CASE MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE
			IF iMissionMusicEventTriggerCounter = NIGEL2_MUSIC_EVENT_START
				WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_START")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE ", "triggering NIGEL2_START", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
				iMissionMusicEventTriggerCounter++
			ENDIF
			IF iMissionMusicEventTriggerCounter = NIGEL2_MUSIC_EVENT_CAR
				WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_CAR")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE ", "triggering NIGEL2_MUSIC_EVENT_CAR", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
				iMissionMusicEventTriggerCounter++
			ENDIF
			IF iMissionMusicEventTriggerCounter = NIGEL2_MUSIC_EVENT_JUMP
				WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_JUMP", TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE ", "triggering NIGEL2_MUSIC_EVENT_JUMP", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
				iMissionMusicEventTriggerCounter++
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    set target's anim when recording ends and he has crashed
PROC SET_TARGET_PLAY_HEAD_ON_WHEEL_ANIM(FLOAT fBlendInTime = NORMAL_BLEND_IN)

	IF IS_PED_UNINJURED(sTargetPed.ped)
		IF NOT IS_ENTITY_PLAYING_ANIM(sTargetPed.ped, "rcmnigel2", "die_horn")
			REQUEST_ANIM_DICT("rcmnigel2")
			IF HAS_ANIM_DICT_LOADED("rcmnigel2")
				TASK_PLAY_ANIM(sTargetPed.ped, "rcmnigel2", "die_horn", fBlendInTime, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME, 0.9)
				CPRINTLN(DEBUG_MISSION, "SET_TARGET_PLAY_HEAD_ON_WHEEL_ANIM - set this frame : ", GET_FRAME_COUNT())
			ENDIF
		ENDIF
	ENDIF
ENDPROC	
			
/// PURPOSE:
///    gets the target and his vehicle into position for the cutscene
PROC SETUP_TARGET_FOR_CRASH()
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "in SETUP_TARGET_FOR_CRASH *****") ENDIF #ENDIF
	REQUEST_ANIM_DICT("rcmnigel2")	//to play head on horn anim on target in truck	
	IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
		SET_VEHICLE_ON_GROUND_PROPERLY(sTargetVehicle.vehicle)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sTargetVehicle.vehicle)
		FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sTargetVehicle.vehicle, FALSE)
		SET_DISABLE_VEHICLE_ENGINE_FIRES(sTargetVehicle.vehicle, TRUE)		
		SET_VEHICLE_ENGINE_ON(sTargetVehicle.vehicle, TRUE, TRUE)			// B*1557148 - engine needs to be on for damage sfx
		SET_VEHICLE_AUDIO_BODY_DAMAGE_FACTOR(sTargetVehicle.vehicle, 1.0)	// B*1557148 - make damage engine sound louder
		SET_VEHICLE_ENGINE_HEALTH(sTargetVehicle.vehicle, 0.0)				// B*1069508 - Smoking engine is not visible until after the end cutscene
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TARGET_FOR_CRASH - set target vehicle to 0.0 health *****") ENDIF #ENDIF
		
		// no longer need to set position as we'll use the last pos of the vehicle recording
		// NOTE: has the new position and quaternion values
		//FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
		//SET_ENTITY_COORDS(sTargetVehicle.vehicle, <<393.4298, -621.3337, 28.4891>>)
		//SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0081, -0.0061, 0.7994, -0.6007)		
		//FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)		
	ENDIF	
	IF IS_PED_UNINJURED(sTargetPed.ped)
		SET_TARGET_PLAY_HEAD_ON_WHEEL_ANIM()
		//set Al Di Napoli health to low so he will die if shot.
		IF (GET_ENTITY_HEALTH(sTargetPed.ped) > 110)
			SET_ENTITY_HEALTH(sTargetPed.ped, 110)
			SET_PED_SUFFERS_CRITICAL_HITS(sTargetPed.ped, TRUE)
		ENDIF
	ENDIF	
	//stop peds appearing at the crash site
	IF NOT IS_SPHERE_VISIBLE(<< 393.38, -617.53, 27.91 >>, 30.0)
		CLEAR_AREA_OF_PEDS(<< 393.38, -617.53, 27.91 >>, 35.0)
		CLEAR_AREA_OF_OBJECTS(<< 393.38, -617.53, 27.91 >>, 8.0)
	ENDIF					
ENDPROC

/// PURPOSE:
///    Changes the time scale and follow vehicle cam view mode, to force a cinematic camera shot
///    for the jump out the hospital window
/// PARAMS:
///    bApplyChanges - if true time scale is slowed and cinematic cam applied
///    if false time scale reset to 1.0 and third person cam applied
PROC SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(BOOL bApplyChanges)
	IF bApplyChanges	
		IF NOT DOES_CAM_EXIST(camHospitalJump)
			camHospitalJump = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		ENDIF
		IF DOES_CAM_EXIST(camHospitalJump)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_CAM_COORD(camHospitalJump, vJumpOutHospitalCamCoords)
			//SET_CAM_DOF_STRENGTH(camHospitalJump, 0.2)
			SET_CAM_FOV(camHospitalJump, 14.0)
			POINT_CAM_AT_ENTITY(camHospitalJump, sNigelVehicle.vehicle, vJumpOutHospitalCamLookAtOffset) //, << 0.0,2.0,0.0 >>)
			ACTIVATE_AUDIO_SLOWMO_MODE("NIGEL_02_SLOWMO_SETTING")
			//SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", 0.3)
		ENDIF
		//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_CINEMATIC)
		/*SET_TIME_SCALE(0.5)
		IF IS_ENTITY_ALIVE(sNigelVehicle.vehicle)
			IF NOT IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_HELI_CHASE)
				CREATE_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE, 30000, NULL, sNigelVehicle.vehicle)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW : CREATE_CINEMATIC_SHOT, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF*/
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW - TRUE") ENDIF #ENDIF
	ELSE
		STOP_SCRIPT_GLOBAL_SHAKING()	
		IF DOES_CAM_EXIST(camHospitalJump)
			IF IS_CAM_ACTIVE(camHospitalJump)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_CAM_ACTIVE(camHospitalJump, FALSE)
			ENDIF			
			DESTROY_CAM	(camHospitalJump)
		ENDIF		
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		
		//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
		/*IF IS_CINEMATIC_SHOT_ACTIVE(SHOTTYPE_HELI_CHASE)
			STOP_CINEMATIC_SHOT(SHOTTYPE_HELI_CHASE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW : STOP_CINEMATIC_SHOT, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF*/
		SET_TIME_SCALE(1.0)
		DEACTIVATE_AUDIO_SLOWMO_MODE("NIGEL_02_SLOWMO_SETTING")
		bDoneOneTime_JumpOutHospitalWindow = TRUE
		
		#IF IS_DEBUG_BUILD
			// override for debug testing
			IF eMissionStage = MISSION_STAGE_DEBUG_HOPSITAL_DOORS
				bDoneOneTime_JumpOutHospitalWindow = FALSE
			ENDIF
		#ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW - FALSE") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    trigger scripted sfx for nigel vehicle landing
PROC TRIGGER_LANDING_JUMP_SFX()
	IF bTriggerLandingFromJumpSFX
		IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
			VECTOR vTemp = GET_ENTITY_COORDS(sNigelVehicle.vehicle, FALSE)
			IF vTemp.Z < 35.0
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sNigelVehicle.vehicle)
				OR IS_VEHICLE_ON_ALL_WHEELS(sNigelVehicle.vehicle)
					PLAY_SOUND_FROM_ENTITY(-1, "CAR_DROP_WRAP", sNigelVehicle.vehicle)
					bTriggerLandingFromJumpSFX = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRIGGER_LANDING_JUMP_SFX - sfx triggered") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF			
	ENDIF
ENDPROC

/// PURPOSE:
///    handles the player jumping out of the hospital window in a vehicle
///    sets scripted camera and time scale override
///    NOTE: doesn't check Nigel's vehicle is OK
PROC MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
	
	IF NOT bDoneOneTime_JumpOutHospitalWindow	// only do this first time player jumps out the window - gets set FALSE in SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW when set FALSE
	
		IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
			
				//managed forced cinematic cam and sloowed time during hospital jump	
				IF NOT IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<373.288605,-580.871033,37.239483>>, <<359.660248,-605.220703,29.138260>>, 20.000000)	// B*1538706 - exclusion area under window on platform
				AND (IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, vCinematicCamOverrideZone01, vCinematicCamOverrideZone02, fCimematicCamOvverideZoneDepth)	
				OR IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<370.705353,-596.143677,45.439484>>, <<361.383423,-600.700012,33.239487>>, 6.000000))	// B*1533197
					MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE(NIGEL2_MUSIC_EVENT_JUMP)
					
					IF bDoneCleanup_JumpOutHospitalWindow	
						SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(TRUE)
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)	// B*1557320 - don't allow hint cam during this shot (renabled by main loop checking bDoneCleanup_JumpOutHospitalWindow = true
						bDoneCleanup_JumpOutHospitalWindow = FALSE
						bTriggerLandingFromJumpSFX	= TRUE // allow custom sfx to trigger for vehicle landing on the floor
						fTimeScale_HospitalJump = 1.0	// set initial value
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOWEST)	// B*1841676 - record the jump out of the hospital window
					ENDIF	
					
					IF bStatTracker_IsNigelVehicleDamageStatActive	// stop tracking when you jump out the window
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL,NI2_VEHICLE_DAMAGE)	// end tracking nigel's vehicle damage for stats
						bStatTracker_IsNigelVehicleDamageStatActive = FALSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)") ENDIF #ENDIF
					ENDIF
				
					// jump area - bottom of window Z height 42.4, road below Z height 27.9 / dist = 14.5  halfway 35.15		
					VECTOR vVehicleCoords = GET_ENTITY_COORDS(sNigelVehicle.vehicle)
					
					FLOAT fDesiredMinTimeScale = 0.2	// 0.5
					#IF IS_DEBUG_BUILD IF bDebug_OverrideMinTimeScale fDesiredMinTimeScale = fDebug_OverrideMinTimeScale ENDIF #ENDIF					
					FLOAT fTimeScaleStepDecrease = 0.9
					#IF IS_DEBUG_BUILD IF bDebug_OverrideTimeScaleStepModifier_Decrease fTimeScaleStepDecrease = fDebug_TimeScaleStep_ModifierDecrease ENDIF #ENDIF
					FLOAT fTimeScaleStepIncrease = 2.0
					#IF IS_DEBUG_BUILD IF bDebug_OverrideTimeScaleStepModifier_Increase fTimeScaleStepIncrease = fDebug_TimeScaleStep_ModifierIncrease ENDIF #ENDIF
					
					// ramp the time scale down to 0.5 as we fly out the window
					IF vVehicleCoords.z > 35.25
						IF fTimeScale_HospitalJump > fDesiredMinTimeScale			
							fTimeScale_HospitalJump = fTimeScale_HospitalJump -@ fTimeScaleStepDecrease
							
							// cap to 0.5
							IF fTimeScale_HospitalJump < fDesiredMinTimeScale
								fTimeScale_HospitalJump = fDesiredMinTimeScale
							ENDIF
							SET_TIME_SCALE(fTimeScale_HospitalJump)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - ramping time scale down = ", fTimeScale_HospitalJump) ENDIF #ENDIF
						ENDIF
					// when we approach the ground ramp the time scale back up to 1.0
					ELIF vVehicleCoords.z < 35.0
						IF fTimeScale_HospitalJump < 1.0				
							fTimeScale_HospitalJump = fTimeScale_HospitalJump +@ fTimeScaleStepIncrease
							
							// cap to 1.0
							IF fTimeScale_HospitalJump > 1.0
								fTimeScale_HospitalJump = 1.0
							ENDIF
							SET_TIME_SCALE(fTimeScale_HospitalJump)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - ramping time scale up = ", fTimeScale_HospitalJump) ENDIF #ENDIF
						ENDIF
					ENDIF
					
					// fallen on roof of hospital entrance
					IF vVehicleCoords.z < 40.0
						IF IS_VEHICLE_ON_ALL_WHEELS(sNigelVehicle.vehicle)
							IF NOT bDoneCleanup_JumpOutHospitalWindow	
								SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
								REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
								bDoneCleanup_JumpOutHospitalWindow = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - bDoneCleanup_JumpOutHospitalWindow veh landed on all wheels whilst still in the activation area") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bDoneCleanup_JumpOutHospitalWindow	
						SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
						REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
						bDoneCleanup_JumpOutHospitalWindow = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - bDoneCleanup_JumpOutHospitalWindow done") ENDIF #ENDIF
					ENDIF
				ENDIF				
			ELSE
				IF NOT bDoneCleanup_JumpOutHospitalWindow	
					SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
					REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
					bTriggerLandingFromJumpSFX = FALSE	// skip in this instance
					bDoneCleanup_JumpOutHospitalWindow = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - bDoneCleanup_JumpOutHospitalWindow early for player not sat in nigel's vehicle being wrecked") ENDIF #ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bDoneCleanup_JumpOutHospitalWindow	
				SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
				REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
				bTriggerLandingFromJumpSFX = FALSE	// skip in this instance
				bDoneCleanup_JumpOutHospitalWindow = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - bDoneCleanup_JumpOutHospitalWindow early for nigel's vehicle being wrecked") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	TRIGGER_LANDING_JUMP_SFX()
ENDPROC

/// PURPOSE:
///    Sets up the vehicle parked nearby to the outro for the player to use on mission passed
PROC CREATE_VEHICLE_FOR_PLAYER_TO_USE_ON_MISSION_PASSED(BOOL bWaitForModelToLoad = FALSE, BOOL UseUberRecordingToDetermineSpawnTime = TRUE)
	IF NOT DOES_ENTITY_EXIST(sVehicleForPlayerOnMissionPassed.vehicle)	
		FLOAT fSpawnTimeInUberRecording_Min = 115000.0 // which is just after NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT
		FLOAT fSpawnTimeInUberRecording_Max = 116821
		//if we are using the uber recording to decide the spawn time, exit if we are too early to start requesting the model
		IF UseUberRecordingToDetermineSpawnTime
			IF fCurrentPlaybackTime < (fSpawnTimeInUberRecording_Min - 3000.0)
				EXIT
			ENDIF
		ENDIF		
		REQUEST_MODEL(sVehicleForPlayerOnMissionPassed.model)		
		IF bWaitForModelToLoad
			WHILE NOT HAS_MODEL_LOADED(sVehicleForPlayerOnMissionPassed.model)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_VEHICLE_FOR_PLAYER_TO_USE_ON_MISSION_PASSED - LOADING VEHICLE ASSET") ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
		ENDIF		
		//if we are using the uber recording to decide the spawn time, exit if we are too early or late to create the vehicle
		IF UseUberRecordingToDetermineSpawnTime
			IF fCurrentPlaybackTime < fSpawnTimeInUberRecording_Min // which is just after NIGEL2_RAYFIRE_HOSPITAL_DOORS_TRIGGER_POINT
			OR fCurrentPlaybackTime > fSpawnTimeInUberRecording_Max
				EXIT
			ENDIF
		ENDIF		
		IF HAS_MODEL_LOADED(sVehicleForPlayerOnMissionPassed.model)
			sVehicleForPlayerOnMissionPassed.vehicle = CREATE_VEHICLE(sVehicleForPlayerOnMissionPassed.model, <<419.3891, -604.8867, 27.7732>>, 0)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL tDebugName = "PlayerVMP"
				SET_VEHICLE_NAME_DEBUG(sVehicleForPlayerOnMissionPassed.vehicle, tDebugName)
			#ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_VEHICLE_FOR_PLAYER_TO_USE_ON_MISSION_PASSED - VEHICLE CREATED") ENDIF #ENDIF
		ENDIF
		IF IS_VEHICLE_OK(sVehicleForPlayerOnMissionPassed.vehicle)
			SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleForPlayerOnMissionPassed.model)
			SET_ENTITY_QUATERNION(sVehicleForPlayerOnMissionPassed.vehicle, 0.0000, 0.0006, 0.9996, -0.0275)
			SET_VEHICLE_DOORS_LOCKED(sVehicleForPlayerOnMissionPassed.vehicle, VEHICLELOCK_UNLOCKED)
		ENDIF		
	ENDIF	
ENDPROC

/// PURPOSE:
///    The cars around the start of the uber chase, have to be scripted because the don't start in time
/// PARAMS:
///    fLocalMainPlaybackSpeed - basically takes the value of fCurrentPlaybackTime
///    vPlayerCurrentPos - used for cleanup
PROC UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(FLOAT fLocalMainPlaybackSpeed, VECTOR vPlayerCurrentPos)
	FLOAT fDistFromScriptedVehicle	
	//truck
	IF IS_VEHICLE_OK(sStartLocationVehicle[0].vehicle)
		//Set hazard lights on
		SET_VEHICLE_INDICATOR_LIGHTS(sStartLocationVehicle[0].vehicle, TRUE, TRUE)
		SET_VEHICLE_INDICATOR_LIGHTS(sStartLocationVehicle[0].vehicle, FALSE, TRUE)		
		//cleanup
		IF fCurrentPlaybackTime > 10000.0	//wait until car chase has gone past the start area
			fDistFromScriptedVehicle = VDIST2(GET_ENTITY_COORDS(sStartLocationVehicle[0].vehicle), vPlayerCurrentPos)
			IF fDistFromScriptedVehicle > 14400.0
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANED UP SCRIPTED VEHICLE [0]") ENDIF #ENDIF
				SAFE_REMOVE_VEHICLE(sStartLocationVehicle[0].vehicle)
			ENDIF
		ENDIF
	ENDIF	
	//parked car
	IF IS_VEHICLE_OK(sStartLocationVehicle[1].vehicle)
		//cleanup
		IF fCurrentPlaybackTime > 10000.0	//wait until car chase has gone past the start area
			fDistFromScriptedVehicle = VDIST2(GET_ENTITY_COORDS(sStartLocationVehicle[1].vehicle), vPlayerCurrentPos)
			IF fDistFromScriptedVehicle > 14400.0
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANED UP SCRIPTED VEHICLE [1]") ENDIF #ENDIF
				SAFE_REMOVE_VEHICLE(sStartLocationVehicle[1].vehicle)
			ENDIF
		ENDIF
	ENDIF	
	//car which pulls out on target to force him down highstreet
	IF IS_VEHICLE_OK(sStartLocationVehicle[2].vehicle)
		IF sDefaultPedForScriptedVehicle.eState = SPS_INIT
			REQUEST_MODEL(mnDefaultPedForVehicle)			
			IF HAS_MODEL_LOADED(mnDefaultPedForVehicle)
				IF NOT DOES_ENTITY_EXIST(sDefaultPedForScriptedVehicle.ped)
					sDefaultPedForScriptedVehicle.ped = CREATE_PED_INSIDE_VEHICLE(sStartLocationVehicle[2].vehicle, PEDTYPE_MISSION, mnDefaultPedForVehicle)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnDefaultPedForVehicle)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sDefaultPedForScriptedVehicle.ped, TRUE)
					sDefaultPedForScriptedVehicle.eState = SPS_BEGIN_ROUTINE
				ENDIF
			ENDIF
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sStartLocationVehicle[2].vehicle)
				//have to redo this as CREATE_PED_INSIDE_VEHICLE interfers with vehicle quaternion
				SET_ENTITY_QUATERNION(sStartLocationVehicle[2].vehicle, 0.0269, 0.0079, -0.4791, 0.8773)
			ENDIF
		ENDIF		
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sStartLocationVehicle[2].vehicle)
			IF NOT bStartedPlayBack_ScriptedVehicleAtStartLocation
				IF fCurrentPlaybackTime >= 200.0
					START_PLAYBACK_RECORDED_VEHICLE(sStartLocationVehicle[2].vehicle, 2, sNigel2_UberRecordingName)
					SET_PLAYBACK_SPEED(sStartLocationVehicle[2].vehicle, fLocalMainPlaybackSpeed)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "~~~~~~~~~~	STARTED VEHICLE PLAYBACK AT TIME = ", fCurrentPlaybackTime) ENDIF #ENDIF
					bStartedPlayBack_ScriptedVehicleAtStartLocation = TRUE
				ENDIF
			ENDIF
		ELSE
			SET_PLAYBACK_SPEED(sStartLocationVehicle[2].vehicle, fLocalMainPlaybackSpeed)
			IF NOT bDone_StartLocationVehicleHorn
				IF fCurrentPlaybackTime > 3500.0
				AND fCurrentPlaybackTime < 4500.0
					START_VEHICLE_HORN(sStartLocationVehicle[2].vehicle, 2000, GET_HASH_KEY("HELDDOWN"))
					bDone_StartLocationVehicleHorn = TRUE
				ENDIF
			ENDIF		
		ENDIF		
		//cleanup
		IF fCurrentPlaybackTime > 10000.0	//wait until car chase has gone past the start area
			fDistFromScriptedVehicle = VDIST2(GET_ENTITY_COORDS(sStartLocationVehicle[2].vehicle), vPlayerCurrentPos)
			IF fDistFromScriptedVehicle > 14400.0
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sStartLocationVehicle[2].vehicle)
					STOP_PLAYBACK_RECORDED_VEHICLE(sStartLocationVehicle[2].vehicle)
				ENDIF
				REMOVE_VEHICLE_RECORDING(2, sNigel2_UberRecordingName)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANED UP SCRIPTED VEHICLE [2]") ENDIF #ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(mnDefaultPedForVehicle)
				SAFE_REMOVE_PED(sDefaultPedForScriptedVehicle.ped)
				SAFE_REMOVE_VEHICLE(sStartLocationVehicle[2].vehicle)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    requests the assets for the outro mocap cutscene entities so SET_CUTSCENE_PED_COMPONENT_VARIATION_... can be called
PROC SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
				
	IF IS_ENTITY_ALIVE(sNigelPed.ped)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_Nigel, sNigelPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - set Nigel - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(sMrsThornhillPed.ped)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_MrsThornhill, sMrsThornhillPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - set Mrs Thornhill - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(sTargetPed.ped)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_AlDiNapoli, sTargetPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - set Al Di Napoli - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles requesting mocap on player approach and releasing outro mocap if player moves away
PROC MANAGE_OUTRO_MOCAP_LOADING()

	VECTOR vMocapPosition = <<392.9445, -619.5032, 27.9764>>
	
	IF IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMocapPosition, DEFAULT_CUTSCENE_LOAD_DIST)
		IF NOT IS_PLAYER_CHANGING_CLOTHES()
			REQUEST_CUTSCENE(tlOutroMocapName)
			SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			bRequestedOutroMocap = TRUE
		ENDIF
	ELSE	
		IF bRequestedOutroMocap
			IF NOT IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMocapPosition, DEFAULT_CUTSCENE_UNLOAD_DIST)
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
					bRequestedOutroMocap = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ELSE
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			ENDIF
		ENDIF
	ENDIF
	// B*1521921 - have to bin off the mocap if requested to start over
	IF bRequestedOutroMocap
		IF IS_PLAYER_CHANGING_CLOTHES()
			REMOVE_CUTSCENE()
			bRequestedOutroMocap = FALSE
			CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " player changing clothes! Frame Count : ", GET_FRAME_COUNT())		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - the mission stage 
PROC SET_STAGE(MISSION_STAGE msStage)
	emissionStage = msStage
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///    sets the initial values for all the variables used on the mission
///    also sets up relationship groups
PROC SET_MISSION_VARIABLES()
	INT i		
	FOR i = 0 TO (NIGEL2_TOTAL_IMPORTANT_DIALOGUE_DURING_CHASE - 1)
		bDoneDialogue_Chase[i] = FALSE
	ENDFOR
	FOR i = 0 TO (NIGEL2_TOTAL_BANTER_DIALOGUE_DURING_CHASE - 1)
		bDoneDialogue_BanterChase[i] = FALSE
	ENDFOR
	FOR i = 0 TO (NIGEL2_TOTAL_CATCH_UP_DIALOGUE_DURING_CHASE - 1)
		bDoneDialogue_CatchUp[i] = FALSE
	ENDFOR
	bDoneDialogue_InitialInstruction				= FALSE
	bDoneDialogue_WarnRamming						= FALSE
	bDoneObjective_WarnLeavingNigelAndMrsT			= FALSE
	bDoneDialogue_UpsideDownOrAirbourne 			= FALSE
	bDoneObjective_GetBackInVehicle					= FALSE
	bDoneDialogue_TrevorGetsWantedLevel				= FALSE
	bDoneDialogue_CopsSeenChasing					= FALSE
	bDoneDialogue_DrivingOnWrongSideRoad			= FALSE
	bDone_UberRecordingCleanupForChaseEnd			= FALSE
	bDone_TargetStartChaseRoute						= FALSE
	bDone_PlayerInNigelVehicle_FirstTime			= FALSE
	bDoneObjective_Initial_GetInNigelsVehicle		= FALSE
	FOR i = 0 TO (NIGEL2_TOTAL_DIALOGUE_LINES_GET_IN_NIGEL_CAR - 1)
		bDoneDialogue_GetInNigelsVehicle[i] = FALSE
	ENDFOR
	bIsHospitalDebrisEffectActive					= FALSE
	bHasPTFX_ForHospitalDebrisBeenRequested			= FALSE
	//bDoneObjective_WaitForNigelAndMrsT				= FALSE	
	bIsImportantDialoguePlaying						= FALSE
	bIsInterruptDialoguePlaying						= FALSE
	bStartedPlayBack_ScriptedVehicleAtStartLocation = FALSE
	//bTracker_SeemlessCameraTransition				= FALSE
	bStatTracker_HasPlayerKeptCloseToDiNapoli		= TRUE
	bStatTracker_IsNigelVehicleDamageStatActive		= FALSE
	bStatTracker_HasPlayerInjuredPedWhilstInHospital = FALSE

	//bSetHospital_IPL_InteriorLoadedState			= FALSE
	FOR i = 0 TO (NIGEL2_TOTAL_UBER_VEHICLE_HORNS_TO_PROCESS - 1)
		bDone_UberVehicleHorn[i] = FALSE
	ENDFOR
	bDone_StartLocationVehicleHorn					= FALSE
	bDone_TurnEngineOnForPlayer						= FALSE
	bDoneCleanup_JumpOutHospitalWindow				= TRUE
	bDoneOneTime_JumpOutHospitalWindow				= FALSE
	bDone_RequestHospitalAudio						= FALSE
	bDone_KillConvoForOutTheVehicleDialogue			= FALSE
	bHadToFadeOutToLoadMissionAssets				= FALSE
	bDone_VisualDamageForTargetVehicle				= FALSE
	bDone_HospitalJumpPaperTrailFX					= FALSE
	bDone_UpdateTrashTruckPedModel					= FALSE	
	bDone_UpdateBMXPedModel							= FALSE
	bHasSetExitStateFromOutroMocapForGameplayCam	= FALSE
	bRequestedOutroMocap							= FALSE
	bForceTargetblipFlashThisFrame					= FALSE
	bSetNigelVehicleHDForMocapExit					= FALSE
	bTriggerLandingFromJumpSFX						= FALSE
	eN2_MissionFailedReason							= FAILED_DEFAULT		//used in Script_Failed() to see if a fail reason needs displaying
	iTriggerScriptedPedestriansTimer				= 0
	iPreLoadRecordingsTimer							= 0
	iUpdateUberVehicleTweaksTimer					= 0
	iMissionMusicEventTriggerCounter				= 0
	iTimer_NigelVehicleStuckOnRoofFail				= 0
	iRayfireProgress_HospitalDoors					= 0
	iTimer_DialogueDelay							= 0
	iHospitalSoundFX_Progress						= 0
	iTimer_TrevorGetBackInCarDialogue				= 0
	iTimer_NigelVehicleJumpsTimer					= 0
	iSoundID_UberVehicleFakeRevs = -1	// initialise to -1 so I can test when a sound_id has been assigned
	iMainCarRecID									= 1	// 1
	iScenarioBlockingAreaStage						= 0
	fCurrentChaseDistance							= 0.0
	fCurrentPlaybackTime							= 0.0
	//fMainPlaybackSpeed 								= 1.0 // moved - initial set when uber recording beings
	fCimematicCamOvverideZoneDepth 					= 20.750000
	fDistPlayerToNigelVehicle						= 0.0	
	vCinematicCamOverrideZone01 = <<368.571442,-594.946655,45.542202>>
	vCinematicCamOverrideZone02 = <<389.501495,-614.127930,29.618>>
	// get the coords being used for the building swaps
	//BUILDING_DATA_STRUCT sTempBuildingData
	//GET_BUILDING_DATA(sTempBuildingData, BUILDINGNAME_IPL_PILLBOX_HILL)
	//vCoords_IPL_PILLBOX_HILL = sTempBuildingData.coords
	//GET_BUILDING_DATA(sTempBuildingData, BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR)
	//vCoords_IPL_PILLBOX_HILL_INTERIOR = sTempBuildingData.coords
	eHospitalInteriorState 		= HIS_SET_INTERIOR_IPLS_FOR_CHASE
	//tl23_CurrentConversation					= "NULL"
	//two uber models
//	sStartLocationVehicle[0].model				= BOXVILLE3  //Compilation fix. BOXVILLE3 removed from project. Ben R
	sStartLocationVehicle[0].model				= BOXVILLE2
	sStartLocationVehicle[1].model				= WASHINGTON
	sStartLocationVehicle[2].model				= WASHINGTON	
	sVehicleForPlayerOnMissionPassed.model		= SULTAN	
	mnDefaultPedForVehicle 						= A_M_Y_BUSINESS_01	//same as one of the start location peds to cut memory	also used by ped on foot near start area
	mnDoctorMale 								= S_M_M_Doctor_01
	mnScrubsFemale 								= S_F_Y_Scrubs_01
	mnFemaleStartArea 							= A_F_Y_BevHills_03
	sTargetVehicle.model 			= DUBSTA 	//	DUBSTA doesn't fit through hospital window currently
	sTargetVehicle.vSpawn 			= << -1290.73, -634.603, 26.1004 >>	// from uber recording
	sTargetVehicle.fSpawnHeading 	= 126.961		// QUATERNION -0.0248, 0.0004, 0.8949, 0.4456 for uber chase (don't reset this now as you see a movement)	rotation << -0.453279, 2.26519, 126.961 >>		
	sNigelVehicle.model 			= GET_NIGEL_VEHICLE_MODEL()
	sNigelVehicle.vSpawn 			= << -1304.01, -644.58, 25.91 >>	// << -1303.42, -644.04, 25.89  >>	//had to tweak position because of crate causing pad rumble
	sNigelVehicle.fSpawnHeading 	= 127.67	// 128.45	
	sTargetPed.model 				= GET_AL_DI_NAPOLI_PED_MODEL()  
	sTargetPed.vSpawn 				= << -1290.0477, -631.4590, 25.6863 >>
	sTargetPed.fSpawnHeading 		= 121.5060	
	sMrsThornhillPed.model 			= GET_NPC_PED_MODEL(CHAR_MRS_THORNHILL)
	sMrsThornhillPed.vSpawn 			= << -1309.61, -640.33, 26.53 >>
	sMrsThornhillPed.fSpawnHeading 	= -124.00	
	sNigelPed.model 				= GET_NPC_PED_MODEL(CHAR_NIGEL)
	sNigelPed.vSpawn				= << -1310.24, -640.19, 26.53 >>
	sNigelPed.fSpawnHeading 		= -73.78
	sObjectHospitalDoors.vPos		= << 299.48, -584.84, 41.445 >>
	sObjectHospitalDoors.vRot		= << 0.00, 0.00, -20.00 >>
	sObjectHospitalDoors.modelName	= PROP_HOSPITALDOORS_START
	eHospitalDoorsObjectState 		= HDO_CREATE
	vJumpOutHospitalCamCoords		= << 394.5, -619.1, 28.2 >>
	vJumpOutHospitalCamLookAtOffset = << 0.0, 0.0, 0.0 >>
	ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
	relGroupFriendly = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) //ADD_RELATIONSHIP_GROUP("FRIENDLIES", relGroupFriendly)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, relGroupFriendly)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupFriendly, relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendly, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupFriendly)
ENDPROC

/// PURPOSE:
///    Initialization of the mission
PROC INIT_MISSION()
	SET_MISSION_VARIABLES()
	#IF IS_DEBUG_BUILD		
		SETUP_MISSION_WIDGET()		//this needs setting up first to ensure debug print can be displayed when loading assets	
		// stage skipping
	    mSkipMenu[Z_SKIP_INTRO_MOCAP].sTxtLabel = "Mocap: NMT_2_RCM"    
	    mSkipMenu[Z_SKIP_START_CHASE].sTxtLabel = "Chase after the Al Di Napoli"     
	    mSkipMenu[Z_SKIP_PARK_CAR_NEXT_TO_TARGET].sTxtLabel = "Park alongside Al Di Napoli" 
	    mSkipMenu[Z_SKIP_OUTRO_CUTSCENE].sTxtLabel = "Mocap: NMT_2_MCS_2" 
		mSkipMenu[Z_SKIP_MISSION_PASSED].sTxtLabel = "Mission Passed"
	#ENDIF	
	//INFORM_MISSION_STATS_OF_MISSION_START_NIGEL_2()
	ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK, FALSE)	
	SET_WANTED_LEVEL_MULTIPLIER(0.2)	
	//ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)	
	SET_AGGRESSIVE_HORNS(TRUE)
	DISABLE_TAXI_HAILING(TRUE)		// don't allow taxis on mission
	REGISTER_SCRIPT_WITH_AUDIO()
	SET_START_CHECKPOINT_AS_FINAL() // tells replay controller to display "skip mission" for shitskips
ENDPROC

//-----------------------------------------------------------------------------------------------------		--------------------------------------------
//		Script Terminated Functions
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Deletes all of the blips used in the mission, checking if they exist.
PROC DELETE_ALL_MISSION_BLIPS()
	SAFE_REMOVE_BLIP(sTargetPed.blip)
	SAFE_REMOVE_BLIP(sNigelVehicle.blip)
	SAFE_REMOVE_BLIP(sNigelPed.blip)
	SAFE_REMOVE_BLIP(sMrsThornhillPed.blip)
ENDPROC

/// PURPOSE:
///    Removes all the assets loaded by the mission, used in mission cleanup and reset mission functions.
/// PARAMS:
///    bClearTextSlots - if true CLEAR_ADDITIONAL_TEXT will be called for MISSION_TEXT_SLOT
PROC UNLOAD_ALL_MISSION_ASSETS(BOOL bClearTextSlots = TRUE)
	INT i = 0	
	//hospital interior
	IF IS_VALID_INTERIOR(HospitalInteriorIndex)
		IF IS_INTERIOR_READY(HospitalInteriorIndex)
			SET_INTERIOR_ACTIVE(HospitalInteriorIndex, FALSE)
			UNPIN_INTERIOR(HospitalInteriorIndex)
		ENDIF	
	ENDIF
	//rayfire hospital doors
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfHospitalDoors)
		IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors) != RFMO_STATE_END
			SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfHospitalDoors, RFMO_STATE_ENDING)
		ENDIF
	ENDIF	
	//uber recording stuff
	REMOVE_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)	//main car
	REMOVE_VEHICLE_RECORDING(2, sNigel2_UberRecordingName)	//used by sStartLocationVehicle[2].vehicle
	REMOVE_VEHICLE_RECORDING(1, sNigel2_CarRecNigelOutro)		// used to make Nigel blend out the outro mocap as he drives away
	REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
	CLEANUP_UBER_PLAYBACK()	
	//hospital debris cleanup
	IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalDebris_PTFX_ID)
		STOP_PARTICLE_FX_LOOPED(HospitalDebris_PTFX_ID)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalJump_PTFX_ID)
		STOP_PARTICLE_FX_LOOPED(HospitalJump_PTFX_ID)
	ENDIF
	REMOVE_PTFX_ASSET()		
	IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sTargetVehicle.vehicle)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_02_CHASE")		
		STOP_AUDIO_SCENE("NIGEL_02_CHASE")	
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UNLOAD_ALL_MISSION_ASSETS : STOP_AUDIO_SCENE : NIGEL_02_CHASE ", "***") ENDIF #ENDIF
	ENDIF							
	RELEASE_SOUND_ID(iSoundID_UberVehicleFakeRevs)
	RELEASE_SCRIPT_AUDIO_BANK()	
	UNREGISTER_SCRIPT_WITH_AUDIO()	
	REMOVE_ANIM_DICT("rcmnigel2")	
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjectHospitalDoors.modelName)
	SET_MODEL_AS_NO_LONGER_NEEDED(sTargetVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sNigelVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sTargetPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sMrsThornhillPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sNigelPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDefaultPedForVehicle)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnDoctorMale)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnScrubsFemale)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnFemaleStartArea)
	REPEAT NIGEL2_TOTAL_START_AREA_SCRIPTED_VEHICLES i 
		SET_MODEL_AS_NO_LONGER_NEEDED(sStartLocationVehicle[i].model)
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleForPlayerOnMissionPassed.model)	
	IF bClearTextSlots
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all of the mission entities checking if they exist
///    Used in Mission Failed, when faded out.
/// PARAMS:
///    bDelete - if true all entities will be deleted, else released
PROC CLEANUP_ALL_MISSION_ENTITIES(BOOL bDelete = FALSE)
	INT i = 0	
	REPEAT TOTAL_HOSPITAL_PEDS i
		SAFE_REMOVE_PED(sHospitalPedSimple[i].ped, bDelete)
	ENDREPEAT
	REPEAT TOTAL_START_AREA_PEDS i
		SAFE_REMOVE_PED(sStartAreaPedSimple[i].ped, bDelete)
	ENDREPEAT	
	//SAFE_REMOVE_PED(sDogPedSimple.ped)	
	IF IS_ENTITY_ALIVE(sTargetPed.ped)
		SET_ENTITY_LOAD_COLLISION_FLAG(sTargetPed.ped, FALSE)	//unsure if this is needed, might be costly?
		IF NOT IS_PED_INJURED(sTargetPed.ped)
			SET_PED_KEEP_TASK(sTargetPed.ped, TRUE)
		ENDIF
	ENDIF
	SET_PED_MODEL_IS_SUPPRESSED(sTargetPed.model, FALSE)	
	SAFE_REMOVE_PED(sTargetPed.ped, bDelete)
	
	SET_PED_MODEL_IS_SUPPRESSED(sMrsThornhillPed.model, FALSE)	
	IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
		SET_PED_KEEP_TASK(sMrsThornhillPed.ped, TRUE)
	ENDIF	
	SAFE_REMOVE_PED(sMrsThornhillPed.ped, bDelete)
	
	SET_PED_MODEL_IS_SUPPRESSED(sNigelPed.model, FALSE)
	IF IS_PED_UNINJURED(sNigelPed.ped)
		SET_PED_KEEP_TASK(sNigelPed.ped, TRUE)
	ENDIF	
	SAFE_REMOVE_PED(sNigelPed.ped, bDelete)
	SAFE_DELETE_OBJECT(sObjectHospitalDoors.objectIndex)
	IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
		SET_VEHICLE_CAN_LEAK_OIL(sNigelVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(sNigelVehicle.vehicle, TRUE)
		SET_DISABLE_PRETEND_OCCUPANTS(sNigelVehicle.vehicle, TRUE)
		// reset flag once player is in the vehice
		IF bSetNigelVehicleHDForMocapExit
			SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, FALSE)	// B*1546501 - stops vehicle pop after mocap
			bSetNigelVehicleHDForMocapExit = FALSE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
		REMOVE_VEHICLE_UPSIDEDOWN_CHECK(sNigelVehicle.vehicle)
	ENDIF
	SAFE_REMOVE_VEHICLE(sNigelVehicle.vehicle, bDelete)		
	IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
		SET_ENTITY_PROOFS(sTargetVehicle.vehicle, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)	// B*1954082 - stop window smash during transition to cutscene RESETTING
		SET_DISABLE_VEHICLE_ENGINE_FIRES(sTargetVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(sTargetVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(sTargetVehicle.vehicle, TRUE)
		SET_DISABLE_PRETEND_OCCUPANTS(sTargetVehicle.vehicle, TRUE)
		IF bDone_UberRecordingCleanupForChaseEnd
			SET_VEHICLE_ENGINE_HEALTH(sTargetVehicle.vehicle, 0.0)
		ENDIF
	ENDIF
	SAFE_REMOVE_VEHICLE(sTargetVehicle.vehicle, bDelete)	
	REPEAT NIGEL2_TOTAL_START_AREA_SCRIPTED_VEHICLES i
		SAFE_REMOVE_VEHICLE(sStartLocationVehicle[i].vehicle, bDelete)
	ENDREPEAT
	SAFE_REMOVE_VEHICLE(sVehicleForPlayerOnMissionPassed.vehicle, bDelete)
	SAFE_REMOVE_PED(sDefaultPedForScriptedVehicle.ped, bDelete)	
ENDPROC

/// PURPOSE:
///    Mission cleanup
/// PARAMS:
///    bDeleteAll - if TRUE all entities are deleted, else released
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC MISSION_CLEANUP(BOOL bDeleteAll = FALSE, BOOL bClearTextSlots = TRUE)
	INT i
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(TRUE)
	ENDIF
	KILL_ANY_CONVERSATION()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	DISABLE_CELLPHONE(FALSE)
	//RENDER_SCRIPT_CAMS(FALSE, FALSE)	//this brakes my interpolate camera when mission ends normally.
	RC_END_CUTSCENE_MODE()
	STOP_SCRIPT_GLOBAL_SHAKING()
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)	
	IF NOT bDoneCleanup_JumpOutHospitalWindow	
		REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
	ENDIF
	//reset roads and peds	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-1415.903931,-732.548889,17.545492>>, <<-1236.628174,-601.026489,31.153446>>, 23.000000)	// match with values when turned on - intro	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<418.283752,-571.662903,24.697943>>, <<362.025787,-663.139771,38.339985>>, 28.000000)	//match with values when turned on - outro	
	// turn back on road from Road leading to tunnel exit
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-668.363464,-658.380066,27.433815>>, <<-440.021973,-657.423767,40.251266>>, 40.000000)	// cover exit from tunnel
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1409.34351, -756.38098, 17.59896 >>, << -1239.14758, -587.12195, 31.25266 >>, TRUE)	// match with values when turned on - intro	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 341.2, -645.83, 25.0 >>, << 429.1, -572.89, 35.0 >>, TRUE)	// match with values when turned on - outro
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingIntroArea)
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingOutroArea)
	FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
	ENDFOR
	SET_PED_PATHS_BACK_TO_ORIGINAL(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 28.0 >>)	// intro
	SET_PED_PATHS_BACK_TO_ORIGINAL(<< -496.28848, -681.55994, 25.0 >>, << -481.59882, -668.74115, 38.0 >>)	// chase - second pedestrian area
	SET_PED_PATHS_BACK_TO_ORIGINAL(<< -463.21530, -829.04749, 22.48120 >>, << -445.46454, -813.10107, 36.56588 >>)	// chase - entrance to car park
	SET_PED_PATHS_BACK_TO_ORIGINAL(<< 341.2, -645.83, 25.0 >>, << 429.1, -572.89, 35.0 >>)	// outro
	CLEAR_PED_NON_CREATION_AREA()
	SET_AGGRESSIVE_HORNS(FALSE)	
	DISABLE_TAXI_HAILING(FALSE)
	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
		SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_MISSION_FAIL")
	ENDIF
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)	// reset flag for ensure player uses direct door access as per bug 1044490
	ENDIF	
	IF DOES_CAM_EXIST(camHospitalJump)
		SET_CAM_ACTIVE(camHospitalJump, FALSE)
		DESTROY_CAM(camHospitalJump)
	ENDIF
	WAIT_FOR_CUTSCENE_TO_STOP()
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 2)		// "TREVOR"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)		// "DINAPOLI"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "NIGEL"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 5)		// "MRSTHORNHILL"	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
	SET_TIME_SCALE(1.0)	
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	//REMOVE_RELATIONSHIP_GROUP(relGroupFriendly) no longer doing this to resolve B*512565	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sNigelVehicle.model, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sTargetVehicle.model, FALSE)	
	DELETE_ALL_MISSION_BLIPS()	
	CLEANUP_ALL_MISSION_ENTITIES(bDeleteAll)	
	UNLOAD_ALL_MISSION_ASSETS(bClearTextSlots)
		
	IF eMissionStage = MISSION_STAGE_MISSION_PASSED
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_CLEANUP, TRUE) // set the building swaps to the intial state
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_IPL_PILLBOX_HILL -> BUILDINGSTATE_CLEANUP with delay for leave area") ENDIF #ENDIF
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_DESTROYED, TRUE)	// unloaded version of the interior
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR -> BUILDINGSTATE_DESTROYED with delay for leave area") ENDIF #ENDIF
		SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_CLEANUP, TRUE)	// boarded up doors state
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_ES_PILLBOX_HILL -> BUILDINGSTATE_CLEANUP ith delay for leave area") ENDIF #ENDIF
	ELSE
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_NORMAL) // set the building swaps to the intial state
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_IPL_PILLBOX_HILL -> BUILDINGSTATE_NORMAL") ENDIF #ENDIF	
		SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_DESTROYED)	// unloaded version of the interior
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR -> BUILDINGSTATE_DESTROYED") ENDIF #ENDIF
		SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)	// contains the rayfire for the front door
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - set building swaps BUILDINGNAME_ES_PILLBOX_HILL -> BUILDINGSTATE_DESTROYED (default) since mission was failed") ENDIF #ENDIF
	ENDIF
	/*// release new load scene
	IF eHospitalInteriorState = HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR
	OR eHospitalInteriorState = HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
			CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP : NEW_LOAD_SCENE_STOP() : FC = ", GET_FRAME_COUNT())
		ENDIF
	ENDIF*/
	
	#IF IS_DEBUG_BUILD
		CLEANUP_MISSION_WIDGETS()
		STOP_RECORDING_ALL_VEHICLES()
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF	
	CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - done")
ENDPROC

/// PURPOSE:
///    Handles call to MISSION_CLEANUP and terminates the thread
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MISSION_CLEANUP()
	ENDIF	

	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)	
	TERMINATE_THIS_THREAD()	
ENDPROC

/// PURPOSE:
///    Mission Passed
PROC Script_Passed()

	IF HAS_TIME_PASSED(iTimer_MissionPassedDelay, 1000)	// shorten once this veh rec is working!
		CPRINTLN(DEBUG_MISSION, "Script_Passed()")
		IF IS_PED_UNINJURED(sNigelPed.ped)
		AND IS_VEHICLE_OK(sNigelVehicle.vehicle)
			IF IS_PED_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sNigelVehicle.vehicle)
					STOP_PLAYBACK_RECORDED_VEHICLE(sNigelVehicle.vehicle)
					REMOVE_VEHICLE_RECORDING(1, sNigel2_CarRecNigelOutro)
				ENDIF
				IF NOT IsPedPerformingTask(sNigelPed.ped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
					//SET_VEHICLE_ENGINE_ON(sNigelVehicle.vehicle, TRUE, TRUE)
					//SET_VEHICLE_FORWARD_SPEED(sNigelVehicle.vehicle, 30)
					TASK_VEHICLE_DRIVE_WANDER(sNigelPed.ped, sNigelVehicle.vehicle, 60.0, DRIVINGMODE_AVOIDCARS)		
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sNigelPed.ped, TRUE)
					SET_PED_KEEP_TASK(sNigelPed.ped, TRUE)
					CPRINTLN(DEBUG_MISSION, "Script_Passed - set Nigel with wander task - Frame Count : ", GET_FRAME_COUNT())
				ENDIF
			ENDIF			
		ENDIF		
		SAFE_FADE_SCREEN_IN_FROM_BLACK()
		Random_Character_Passed(CP_RAND_C_NIG2)
		Script_Cleanup()	
	ELSE
		CPRINTLN(DEBUG_MISSION, "Script_Passed() delay for Nigel driving")
	ENDIF
ENDPROC

/// PURPOSE:
///    updates the reason for mission failed
///    in order of most important fail reason so if multiple fails conditions have been set, we use the most important
PROC UPDATE_FAIL_REASON()
	IF DOES_ENTITY_EXIST(sNigelPed.ped)
	AND DOES_ENTITY_EXIST(sMrsThornhillPed.ped)
		IF IS_ENTITY_DEAD(sNigelPed.ped)
		OR IS_PED_INJURED(sNigelPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_NIGEL_DIED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_NIGEL_DIED
			EXIT
		ENDIF
		IF IS_ENTITY_DEAD(sMrsThornhillPed.ped)
		OR IS_PED_INJURED(sMrsThornhillPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_MRS_THORNHILL_DIED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_MRS_THORNHILL_DIED
			EXIT
		ENDIF		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sNigelPed.ped, PLAYER_PED_ID())
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_NIGEL_ATTACKED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_NIGEL_ATTACKED
			EXIT
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sMrsThornhillPed.ped, PLAYER_PED_ID())
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_MRS_THORNHILL_ATTACKED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_MRS_THORNHILL_ATTACKED
			EXIT
		ENDIF
	ENDIF	
	IF DOES_ENTITY_EXIST(sTargetPed.ped)
		IF IS_ENTITY_DEAD(sTargetPed.ped)
		OR IS_PED_INJURED(sTargetPed.ped)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_TARGET_DIED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_TARGET_DIED
			EXIT
		ELSE
			//Kill off Di Napoli, if his vehicle is set on fire
			IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
				IF NOT IS_ENTITY_DEAD(sTargetVehicle.vehicle)
				AND IS_ENTITY_ON_FIRE(sTargetVehicle.vehicle)
					EXPLODE_VEHICLE(sTargetVehicle.vehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIGEL VEHICLE IS_VEHICLE_STUCK_ON_ROOF CHECKED	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^") ENDIF #ENDIF
		IF IS_ENTITY_DEAD(sNigelVehicle.vehicle)
		OR IS_ENTITY_IN_WATER(sNigelVehicle.vehicle)
		OR NOT IS_VEHICLE_DRIVEABLE(sNigelVehicle.vehicle)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_NIGEL_VEHICLE_WRECKED") ENDIF #ENDIF
			eN2_MissionFailedReason = FAILED_NIGEL_VEHICLE_WRECKED
			EXIT
		ELSE
			IF eMissionStage != MISSION_STAGE_INTRO_MOCAP_SCENE		// don't worry during the mocaps
				// setup a grace period for this to fix instant fail, B*1080979
				IF IS_VEHICLE_STUCK_ON_ROOF(sNigelVehicle.vehicle)
					IF HAS_TIME_PASSED(iTimer_NigelVehicleStuckOnRoofFail, 3000)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - stuck on the roof FAILED_NIGEL_VEHICLE_WRECKED") ENDIF #ENDIF
						eN2_MissionFailedReason = FAILED_NIGEL_VEHICLE_WRECKED
						EXIT
					ENDIF
				ELSE
					iTimer_NigelVehicleStuckOnRoofFail = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	//if one of these fails has already been set, don't worry about retesting it
	IF eN2_MissionFailedReason = FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND
	OR eN2_MissionFailedReason = FAILED_LEFT_NIGEL_BEHIND
	OR eN2_MissionFailedReason = FAILED_LEFT_MRS_T_BEHIND
	OR eN2_MissionFailedReason = FAILED_TARGET_ESCAPED
		EXIT
	ENDIF	
	IF eMissionStage = MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
	OR eMissionStage = MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE		
		//check for target escaping / left behind
		IF IS_PED_UNINJURED(sTargetPed.ped)
			IF fCurrentChaseDistance > NIGEL2_TARGET_ESCAPED_DISTANCE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_TARGET_ESCAPED") ENDIF #ENDIF
				eN2_MissionFailedReason = FAILED_TARGET_ESCAPED
				EXIT
			ENDIF
			// B*1513201 - when target goes down the tunnel and player doesn't follow
			// needs to match up with MANAGE_DIALOGUE_DURING_CHASE
			IF fCurrentPlaybackTime > 50000.0	// 40000.0	// enter tunnel
			AND fCurrentPlaybackTime < 58000.0	// exit tunnel
				IF bDoneDialogue_WrongWaySpecialFail
				OR NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-668.363464,-658.380066,27.433815>>, <<-440.021973,-657.423767,40.251266>>, 40.000000)	// smaller area covering exit
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_TARGET_ESCAPED") ENDIF #ENDIF
						eN2_MissionFailedReason = FAILED_TARGET_ESCAPED
						EXIT
					ENDIF
				ENDIF
			ENDIF					
		ENDIF	
		//check if the player leaves Nigel and Mrs Thornhill behind
		IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
		AND IS_ENTITY_ALIVE(sNigelPed.ped)
		AND IS_ENTITY_ALIVE(sMrsThornhillPed.ped)
			VECTOR vTempPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			// check for player leaving Nigel or Mrs Thornhill behind on foot			
			IF NOT IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
				IF NOT IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)		
					// nigel and mrs T out of the vehicle
					IF NOT bDoneObjective_WarnLeavingNigelAndMrsT
						IF NOT IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
						OR NOT IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
							KILL_ANY_CONVERSATION()
							PRINT_NOW("NIGEL2_08", DEFAULT_GOD_TEXT_TIME, 1)		// Return to ~b~Nigel~s~ and ~b~Mrs Thornhill.~s~
							bDoneObjective_WarnLeavingNigelAndMrsT = TRUE
						ENDIF
					ELSE
						IF IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
						AND IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_08")
								CLEAR_THIS_PRINT("NIGEL2_08")	// Return to ~b~Nigel~s~ and ~b~Mrs Thornhill.~s~
							ENDIF
							bDoneObjective_WarnLeavingNigelAndMrsT = FALSE
						ELIF NOT IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE)
						AND NOT IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND on foot") ENDIF #ENDIF
							eN2_MissionFailedReason = FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND
							EXIT
						ENDIF
					ENDIF
				ELSE
					// just Nigel out of the vehicle
					IF NOT bDoneObjective_WarnLeavingNigelAndMrsT
						IF NOT IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
							KILL_ANY_CONVERSATION()
							PRINT_NOW("NIGEL2_05", DEFAULT_GOD_TEXT_TIME, 1)		// Return to ~b~Nigel.~s~
							bDoneObjective_WarnLeavingNigelAndMrsT = TRUE
						ENDIF
					ELSE
						IF IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_05")
								CLEAR_THIS_PRINT("NIGEL2_05")	// Return to ~b~Nigel.~s~
							ENDIF
							bDoneObjective_WarnLeavingNigelAndMrsT = FALSE
						ELIF NOT IS_ENTITY_IN_RANGE_COORDS(sNigelPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_NIGEL_BEHIND on foot") ENDIF #ENDIF
							eN2_MissionFailedReason = FAILED_LEFT_NIGEL_BEHIND
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ELIF NOT IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)		
				// just mrs T out of the vehicle
				IF NOT bDoneObjective_WarnLeavingNigelAndMrsT
					IF NOT IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
						KILL_ANY_CONVERSATION()
						PRINT_NOW("NIGEL2_09", DEFAULT_GOD_TEXT_TIME, 1)		// Return to ~b~Mrs. Thornhill.~s~
						bDoneObjective_WarnLeavingNigelAndMrsT = TRUE
					ENDIF
				ELSE
					IF IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE)
						IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_09")
							CLEAR_THIS_PRINT("NIGEL2_09")	// Return to ~b~Mrs. Thornhill.~s~
						ENDIF
						bDoneObjective_WarnLeavingNigelAndMrsT = FALSE
					ELIF NOT IS_ENTITY_IN_RANGE_COORDS(sMrsThornhillPed.ped, vTempPlayerPos, PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_MRS_T_BEHIND on foot") ENDIF #ENDIF
						eN2_MissionFailedReason = FAILED_LEFT_MRS_T_BEHIND
						EXIT
					ENDIF
				ENDIF
			ELSE
				// if player leaves Nigel and Mrs Thornhill in the car
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					VECTOR vTempGetNigelVehiclePos = GET_ENTITY_COORDS(sNigelVehicle.vehicle)
					fDistPlayerToNigelVehicle = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTempGetNigelVehiclePos)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist between player and nigel Vehicle = ", fDistPlayerToNigelVehicle) ENDIF #ENDIF				
					IF NOT bDoneObjective_WarnLeavingNigelAndMrsT
						IF fDistPlayerToNigelVehicle > PLAYER_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE
							KILL_ANY_CONVERSATION()
							PRINT_NOW("NIGEL2_08", DEFAULT_GOD_TEXT_TIME, 1)		// Return to ~b~Nigel~s~ and ~b~Mrs Thornhill.~s~
							bDoneObjective_WarnLeavingNigelAndMrsT = TRUE
						ENDIF
					ELSE
						IF fDistPlayerToNigelVehicle > PLAYER_LEFT_NIGEL_AND_MRS_T_BEHIND_DISTANCE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND in the car") ENDIF #ENDIF
							eN2_MissionFailedReason = FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND
							EXIT
						ELIF fDistPlayerToNigelVehicle < RESET_LEFT_NIGEL_AND_MRS_T_WARN_DISTANCE
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_08")
								CLEAR_THIS_PRINT("NIGEL2_08")	// Return to ~b~Nigel~s~ and ~b~Mrs Thornhill.~s~
							ENDIF
							bDoneObjective_WarnLeavingNigelAndMrsT = FALSE
						ENDIF
					ENDIF				
					//seperate check if Al Di Napoli is in crashed state and the player ditches nigel's vehicle in the hospital to approach him on foot.
					IF bDone_UberRecordingCleanupForChaseEnd
						IF (fCurrentChaseDistance < 35.0)	//distance from crashed Al di Napoli to just outside hospital window
							IF IS_VEHICLE_INSIDE_HOSPITAL_INTERIOR(sNigelVehicle.vehicle)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND") ENDIF #ENDIF
								eN2_MissionFailedReason = FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND
								EXIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has failed
PROC MISSION_FAILED_CHECKS()
	
	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_INTRO_MOCAP_SCENE
		AND eMissionStage != MISSION_STAGE_END_CUTSCENE_MOCAP
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED
			UPDATE_FAIL_REASON()
			IF eN2_MissionFailedReason <> FAILED_DEFAULT
				SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		Mission Setup
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    handles all the mission asset requests
/// PARAMS:
///    bWaitForAssetsToLoad - if true game waits for all requests to be loaded
PROC LOAD_MISSION_ASSETS(BOOL bWaitForAssetsToLoad = FALSE)
	REQUEST_ADDITIONAL_TEXT("NIGEL2", MISSION_TEXT_SLOT)	
	REQUEST_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)					//main uber recording
	REQUEST_MODEL(sTargetVehicle.model)
	REQUEST_MODEL(sNigelVehicle.model)
	REQUEST_MODEL(sTargetPed.model)
	REQUEST_MODEL(sMrsThornhillPed.model)
	REQUEST_MODEL(sNigelPed.model)	
	REQUEST_MODEL(sStartLocationVehicle[0].model)
	REQUEST_MODEL(sStartLocationVehicle[1].model)
	REQUEST_MODEL(sStartLocationVehicle[2].model)		
	REQUEST_VEHICLE_RECORDING(2, sNigel2_UberRecordingName)			//vehicle at start which pulls out on target vehicle	
	//wait until loaded
	IF bWaitForAssetsToLoad	
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)	
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iMainCarRecID, sNigel2_UberRecordingName)		//MAIN CAR FOR UBER RECORDING	
		OR NOT HAS_MODEL_LOADED(sTargetVehicle.model)
		OR NOT HAS_MODEL_LOADED(sNigelVehicle.model)
		OR NOT HAS_MODEL_LOADED(sTargetPed.model)
		OR NOT HAS_MODEL_LOADED(sMrsThornhillPed.model)
		OR NOT HAS_MODEL_LOADED(sNigelPed.model)		
		OR NOT HAS_MODEL_LOADED(sStartLocationVehicle[0].model)
		OR NOT HAS_MODEL_LOADED(sStartLocationVehicle[1].model)
		OR NOT HAS_MODEL_LOADED(sStartLocationVehicle[2].model)		
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, sNigel2_UberRecordingName)		//vehicle at start which pulls out on target vehicle
			IF bFinishedStageSkipping	//don't fade if we are stage skipping
				IF NOT IS_SCREEN_FADED_OUT()
					SAFE_FADE_SCREEN_OUT_TO_BLACK(0, TRUE)		// backup fade in, mainly for repeat play purposes.
					bHadToFadeOutToLoadMissionAssets = TRUE	//stored so I only fade back up if, I faded down
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SCREEN FADED BECAUSE ASSETS WEREN'T LOADED IN TIME") ENDIF #ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "waiting for asset to load   ") ENDIF #ENDIF
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION ASSETS LOADED - GO GO GO   ") ENDIF #ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    sets up the Nigel's vehicle for the start of in game play
PROC SETUP_NIGEL_VEHICLE_FOR_IN_GAME_START()
	IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
		SET_VEHICLE_FIXED(sNigelVehicle.vehicle)
		SET_VEHICLE_DOORS_LOCKED(sNigelVehicle.vehicle, VEHICLELOCK_UNLOCKED)
		//SET_VEHICLE_ON_GROUND_PROPERLY(sNigelVehicle.vehicle) stops a visible snap
		SET_VEHICLE_COLOURS(sNigelVehicle.vehicle, 65, 0) //bluey black, ripped from Tom Nigel 3.  //88, 88) //YELLOW
		//SET_VEHICLE_TYRES_CAN_BURST(sNigelVehicle.vehicle, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(sNigelVehicle.vehicle, TRUE)
		SET_VEHICLE_STRONG(sNigelVehicle.vehicle, TRUE)
		SET_ENTITY_HEALTH(sNigelVehicle.vehicle, NIGEL2_MISSION_VEHICLES_HEALTH)
		SET_VEHICLE_CAN_LEAK_OIL(sNigelVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sNigelVehicle.vehicle, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sNigelVehicle.vehicle, SC_DOOR_BOOT, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(sNigelVehicle.model, TRUE)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(sNigelVehicle.vehicle)
		SET_VEHICLE_AS_RESTRICTED(sNigelVehicle.vehicle, 0)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_NIGEL_VEHICLE_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up the target's vehicle for the start of in game play
PROC SETUP_TARGET_VEHICLE_FOR_IN_GAME_START()
	IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
		SET_VEHICLE_FIXED(sTargetVehicle.vehicle)
		SET_VEHICLE_DOORS_LOCKED(sTargetVehicle.vehicle, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_COLOURS(sTargetVehicle.vehicle, 0, 0) //BLACK
		SET_VEHICLE_ENGINE_ON(sTargetVehicle.vehicle, TRUE, TRUE)
		SET_VEHICLE_TYRES_CAN_BURST(sTargetVehicle.vehicle, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(sTargetVehicle.vehicle, TRUE)
		SET_VEHICLE_STRONG(sTargetVehicle.vehicle, TRUE)
		SET_ENTITY_HEALTH(sTargetVehicle.vehicle, NIGEL2_MISSION_VEHICLES_HEALTH)
		SET_DISABLE_VEHICLE_ENGINE_FIRES(sTargetVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_OIL(sTargetVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sTargetVehicle.vehicle, FALSE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sTargetVehicle.vehicle, FALSE)
		SET_VEHICLE_DOOR_SHUT(sTargetVehicle.vehicle, SC_DOOR_FRONT_LEFT, TRUE)
		SET_VEHICLE_DOORS_LOCKED(sTargetVehicle.vehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sTargetVehicle.vehicle, SC_DOOR_FRONT_LEFT, FALSE)	//added this so player rips him out in the cutscene
		SET_VEHICLE_MODEL_IS_SUPPRESSED(sTargetVehicle.model, TRUE)	
		SET_VEHICLE_AS_RESTRICTED(sTargetVehicle.vehicle, 1)
		SET_ENTITY_COORDS(sTargetVehicle.vehicle, sTargetVehicle.vSpawn)	// from uber recording
		SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0248, 0.0004, 0.8949, 0.4456)	// from uber recording	// heading 126.961 // rotation << -0.453279, 2.26519, 126.961 >>		
		FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TARGET_VEHICLE_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up Mrs Thornhill for the start of in game play
PROC SETUP_MRS_THORNHILL_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
		SET_PED_DEFAULT_COMPONENT_VARIATION(sMrsThornhillPed.ped)
		SET_PED_MODEL_IS_SUPPRESSED(sMrsThornhillPed.model, TRUE)
		SET_PED_CAN_BE_TARGETTED(sMrsThornhillPed.ped, FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sMrsThornhillPed.ped, FALSE)
		SET_PED_CONFIG_FLAG(sMrsThornhillPed.ped, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(sMrsThornhillPed.ped, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(sMrsThornhillPed.ped, PCF_GetOutUndriveableVehicle, FALSE)		// //stop ped leaving undriveable vehicle
		SET_PED_CONFIG_FLAG(sMrsThornhillPed.ped, PCF_GetOutBurningVehicle, FALSE)		// //stop ped leaving undriveable vehicle		
		IF IS_PED_IN_GROUP(sMrsThornhillPed.ped)
			REMOVE_PED_FROM_GROUP(sMrsThornhillPed.ped)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sMrsThornhillPed.ped, relGroupFriendly)
		ADD_PED_FOR_DIALOGUE(sDialogue, 5, sMrsThornhillPed.ped, "MRSTHORNHILL")	
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sMrsThornhillPed.ped, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMrsThornhillPed.ped, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_MRS_THORNHILL_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up Nigel for the start of in game play
PROC SETUP_NIGEL_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sNigelPed.ped)
		SET_PED_DEFAULT_COMPONENT_VARIATION(sNigelPed.ped)
		SET_PED_MODEL_IS_SUPPRESSED(sNigelPed.model, TRUE)		
		SET_PED_CAN_BE_TARGETTED(sNigelPed.ped, FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sNigelPed.ped, FALSE)
		SET_PED_CONFIG_FLAG(sNigelPed.ped, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(sNigelPed.ped, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)		
		SET_PED_CONFIG_FLAG(sNigelPed.ped, PCF_GetOutUndriveableVehicle, FALSE)		// //stop ped leaving undriveable vehicle
		SET_PED_CONFIG_FLAG(sNigelPed.ped, PCF_GetOutBurningVehicle, FALSE)		// //stop ped leaving undriveable vehicle		
		IF IS_PED_IN_GROUP(sNigelPed.ped)
			REMOVE_PED_FROM_GROUP(sNigelPed.ped)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sNigelPed.ped, relGroupFriendly)
		ADD_PED_FOR_DIALOGUE(sDialogue, 4, sNigelPed.ped, "NIGEL")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sNigelPed.ped, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sNigelPed.ped, TRUE)		
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_NIGEL_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up Target for the start of in game play
PROC SETUP_TARGET_PED_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sTargetPed.ped)
		SET_PED_COMPONENT_VARIATION(sTargetPed.ped, PED_COMP_LEG, 0, 0)	// default now has pants on show!
		SET_PED_MODEL_IS_SUPPRESSED(sTargetPed.model, TRUE)		
		SET_PED_CAN_BE_TARGETTED(sTargetPed.ped, FALSE)
		SET_PED_CAN_BE_DRAGGED_OUT(sTargetPed.ped, FALSE)
		SET_PED_CONFIG_FLAG(sTargetPed.ped, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(sTargetPed.ped, PCF_GetOutUndriveableVehicle, FALSE)		// //stop ped leaving undriveable vehicle
		SET_PED_CONFIG_FLAG(sTargetPed.ped, PCF_GetOutBurningVehicle, FALSE)		// //stop ped leaving undriveable vehicle		
		SET_PED_DIES_WHEN_INJURED(sTargetPed.ped, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(sTargetPed.ped, TRUE)	//unsure if this is needed, might be costly?
		IF IS_PED_IN_GROUP(sTargetPed.ped)
			REMOVE_PED_FROM_GROUP(sTargetPed.ped)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sTargetPed.ped, relGroupEnemy)
		ADD_PED_FOR_DIALOGUE(sDialogue, 3, sTargetPed.ped, "DINAPOLI")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sTargetPed.ped, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTargetPed.ped, TRUE)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TARGET_PED_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets everything up for the start of the mission.
///    Called at the end of the intro mocap
PROC SETUP_FOR_START_OF_MISSION()

	// Setup Nigel's Vehicle	
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])	// from initial setup
	IF NOT DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
		CLEAR_AREA(sNigelVehicle.vSpawn, 2.0, TRUE)
		CREATE_NIGEL_VEHICLE(sNigelVehicle.vehicle, sNigelVehicle.vSpawn, sNigelVehicle.fSpawnHeading, FALSE, TRUE)
		CPRINTLN(DEBUG_MISSION, "created Nigel's vehicle in script, not mocap	FC = ", GET_FRAME_COUNT())
		SETUP_NIGEL_VEHICLE_FOR_IN_GAME_START()
	ENDIF	
	SET_MODEL_AS_NO_LONGER_NEEDED(sNigelVehicle.model)
	
	// Setup Target's Vehicle
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])	// from initial setup
	IF NOT DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
		CLEAR_AREA(sTargetVehicle.vSpawn, 2.0, TRUE)
		sTargetVehicle.vehicle = CREATE_VEHICLE(sTargetVehicle.model, sTargetVehicle.vSpawn, sTargetVehicle.fSpawnHeading)
		IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
			SET_ENTITY_COORDS(sTargetVehicle.vehicle, sTargetVehicle.vSpawn)	// from uber recording
			SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0248, 0.0004, 0.8949, 0.4456)	// from uber recording	// heading 126.961 // rotation << -0.453279, 2.26519, 126.961 >>	
		ENDIF
		CPRINTLN(DEBUG_MISSION, "created Target's vehicle in script, not mocap	FC = ", GET_FRAME_COUNT())
		SETUP_TARGET_VEHICLE_FOR_IN_GAME_START()
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sTargetVehicle.model)
	
	// Setup Nigel and Mrs Thornhill (start in Nigel's vehicle)
	IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
		// Mrs T
		IF NOT DOES_ENTITY_EXIST(sMrsThornhillPed.ped)
			sMrsThornhillPed.ped = CREATE_PED_INSIDE_VEHICLE(sNigelVehicle.vehicle, PEDTYPE_MISSION, sMrsThornhillPed.model, VS_BACK_RIGHT)
			CPRINTLN(DEBUG_MISSION, "created Mrs T in script, not mocap	FC = ", GET_FRAME_COUNT())
			SETUP_MRS_THORNHILL_FOR_IN_GAME_START()
		ENDIF
		// Nigel
		SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])	// from initial setup
		IF NOT DOES_ENTITY_EXIST(sNigelPed.ped)
			sNigelPed.ped = CREATE_PED_INSIDE_VEHICLE(sNigelVehicle.vehicle, PEDTYPE_MISSION, sNigelPed.model, VS_FRONT_RIGHT)
			CPRINTLN(DEBUG_MISSION, "created Nigel in script, not mocap	FC = ", GET_FRAME_COUNT())
			SETUP_NIGEL_FOR_IN_GAME_START()
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sMrsThornhillPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sNigelPed.model)
	
	// Setup Target (start in Target's vehicle)
	IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
		IF NOT DOES_ENTITY_EXIST(sTargetPed.ped)
			sTargetPed.ped = CREATE_PED_INSIDE_VEHICLE(sTargetVehicle.vehicle, PEDTYPE_MISSION, sTargetPed.model, VS_DRIVER)
			CPRINTLN(DEBUG_MISSION, "created Target ped in script, not mocap	FC = ", GET_FRAME_COUNT())
			SETUP_TARGET_PED_FOR_IN_GAME_START()
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sTargetPed.model)	
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
	ENDIF	
	
	//truck unloading at start location	
	IF NOT DOES_ENTITY_EXIST(sStartLocationVehicle[0].vehicle)
		CLEAR_AREA(<< -1329.7417, -665.3537, 26.3413 >>, 2.0, TRUE)
		sStartLocationVehicle[0].vehicle = CREATE_VEHICLE(sStartLocationVehicle[0].model, << -1329.7417, -665.3537, 26.3413 >>, 126.9722)
	ENDIF
	IF IS_VEHICLE_OK(sStartLocationVehicle[0].vehicle)
		SET_ENTITY_QUATERNION(sStartLocationVehicle[0].vehicle, -0.0318, 0.0067, 0.8936, 0.4476)
		IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(sStartLocationVehicle[0].vehicle, SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(sStartLocationVehicle[0].vehicle, SC_DOOR_REAR_LEFT)
		ENDIF
		IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(sStartLocationVehicle[0].vehicle, SC_DOOR_REAR_RIGHT)
			SET_VEHICLE_DOOR_OPEN(sStartLocationVehicle[0].vehicle, SC_DOOR_REAR_RIGHT)
		ENDIF
		SET_VEHICLE_INDICATOR_LIGHTS(sStartLocationVehicle[0].vehicle, TRUE, TRUE)
		SET_VEHICLE_INDICATOR_LIGHTS(sStartLocationVehicle[0].vehicle, FALSE, TRUE)
	ENDIF	
	
	//car parked at start location	
	IF NOT DOES_ENTITY_EXIST(sStartLocationVehicle[1].vehicle)
		CLEAR_AREA(<< -1329.7509, -676.4864, 25.8557 >>, 2.0, TRUE)
		sStartLocationVehicle[1].vehicle = CREATE_VEHICLE(sStartLocationVehicle[1].model, << -1329.7509, -676.4864, 25.8557 >>, 307.7227)
	ENDIF
	IF IS_VEHICLE_OK(sStartLocationVehicle[1].vehicle)
		SET_ENTITY_QUATERNION(sStartLocationVehicle[1].vehicle, 0.0248, 0.0142, -0.4405, 0.8973)	
	ENDIF	
	
	//car pulls out at start location	
	IF NOT DOES_ENTITY_EXIST(sStartLocationVehicle[2].vehicle)
		CLEAR_AREA(<<-1336.3171, -680.9752, 25.5144>>, 2.0, TRUE)
		sStartLocationVehicle[2].vehicle = CREATE_VEHICLE(sStartLocationVehicle[2].model, <<-1336.3171, -680.9752, 25.5144>>, 307.7227)
	ENDIF
	IF IS_VEHICLE_OK(sStartLocationVehicle[2].vehicle)
		SET_ENTITY_QUATERNION(sStartLocationVehicle[2].vehicle, 0.0269, 0.0079, -0.4791, 0.8773)	
	ENDIF	
	
	INT i = 0
	REPEAT NIGEL2_TOTAL_START_AREA_SCRIPTED_VEHICLES i
		IF IS_VEHICLE_OK(sStartLocationVehicle[i].vehicle)
			SET_MODEL_AS_NO_LONGER_NEEDED(sStartLocationVehicle[i].model)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL tDebugName = "sStartLV"
				tDebugName += i
				SET_VEHICLE_NAME_DEBUG(sStartLocationVehicle[i].vehicle, tDebugName)
			#ENDIF
		ENDIF
	ENDREPEAT
	UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)	
	
	//back up fade in, if we had to fade out during loading of assets
	IF bFinishedStageSkipping	//however, don't fade if we are stage skipping
		IF bHadToFadeOutToLoadMissionAssets
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, mainly for repeat play purposes.
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_FOR_START_OF_MISSION - SAFE_FADE_SCREEN_IN_FROM_BLACK called as bHadToFadeOutToLoadMissionAssets and finished stage skipping", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    setup the state at the crash site, turning off peds vehicles in the area
/// PARAMS:
///    bCallClearArea - if TRUE clear areas will be called
PROC SETUP_CRASH_SITE_FOR_OUTRO_MOCAP(BOOL bCallClearArea = FALSE)
	
	INT i
	VECTOR vAreaMin = << 341.2, -645.83, 25.0 >>
	VECTOR vAreaMax = << 429.1, -572.89, 35.0 >>
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vAreaMin, vAreaMax, FALSE)	// match the cleanup call coords
	SET_ROADS_IN_ANGLED_AREA(<<418.283752,-571.662903,24.697943>>, <<362.025787,-663.139771,38.339985>>, 28.000000, FALSE, FALSE)	// match the cleanup call coords - outro	
	IF bCallClearArea
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vAreaMin, vAreaMax)
		CLEAR_ANGLED_AREA_OF_VEHICLES(<<418.283752,-571.662903,24.697943>>, <<362.025787,-663.139771,38.339985>>, 28.000000)
	ENDIF
	
	SET_PED_PATHS_IN_AREA(vAreaMin, vAreaMax, FALSE)	
	SET_PED_NON_CREATION_AREA(vAreaMin, vAreaMax)
	
	// clean up the blocking area from earlier in the chase (stops global limit being pushed up)
	FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
		IF i != 1 	// don't remove the one at front of the hospital for now
			REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
		ENDIF
	ENDFOR
	scenarioBlockingOutroArea = ADD_SCENARIO_BLOCKING_AREA(vAreaMin, vAreaMax)
	
	IF bCallClearArea
		CLEAR_AREA_OF_OBJECTS(<< 395.24756, -615.01337, 27.87107 >>, 20.0)	
		CLEAR_AREA(<< 395.24756, -615.01337, 27.87107 >>, 20.0, TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_CRASH_SITE_FOR_OUTRO_MOCAP - done with bCallClearArea = ", bCallClearArea, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    handles setting up the scenario blocking area during the chase
///    prevent target driving through peds without collision
PROC UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE(FLOAT fLocal_CurrentPlaybackTime)
	
	INT i
	SWITCH iScenarioBlockingAreaStage
	
	//SET_PED_PATHS_IN_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>, FALSE)
	//SET_PED_NON_CREATION_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>)
	//scenarioBlockingIntroArea = ADD_SCENARIO_BLOCKING_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>)
		CASE 0
			// target just entering the pedestrian road
			IF fLocal_CurrentPlaybackTime > 5000.0
				
				SET_PED_NON_CREATION_AREA(<< -1332.35083, -763.45764, 10.0 >>, << -1229.14221, -654.35461, 42.0 >>)
				
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK	
		CASE 1
			// target just entering second half of the pedestrian road
			IF fLocal_CurrentPlaybackTime > 10000.0
				
				SET_PED_NON_CREATION_AREA(<< -1274.14758, -842.63574, 10.0 >>, << -1139.83557, -703.74365, 42.0 >>)
				
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE 2
			// stop uber cars continuing up the road and interfering with chase again on tunnel exit
			IF fLocal_CurrentPlaybackTime > 40000.0
				// cleanup the blocking areas for the pedestrian area at the start of the chase - setup in SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()
				FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
					REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
				ENDFOR
				
				SET_ROADS_IN_ANGLED_AREA(<<-668.363464,-658.380066,27.433815>>, <<-440.021973,-657.423767,40.251266>>, 40.000000, FALSE, FALSE)	// cover exit from tunnel
				
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE 3
			// sets up the entrance to the second pedestrian area where Di Napoli cuts over the road from the tunnel
			IF fLocal_CurrentPlaybackTime > 54000.0
				// cleanup the blocking areas for the pedestrian area at the start of the chase - setup in SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()
				FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
					REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
				ENDFOR
				
				// make sure ambient cars on this stretch get deleted if player in the underground to ensure veh don't smash into chase
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-513.487732,-598.495544,30.898241>>, <<-695.000977,-593.838440,19.307816>>, 60.000000)	// covers majority of underground section
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-518.404114,-622.829346,32.301056>>, <<-608.405396,-622.358826,26.787561>>, 11.000000) 
					CLEAR_ANGLED_AREA_OF_VEHICLES(<<-668.363464,-658.380066,27.433815>>, <<-440.021973,-657.423767,40.251266>>, 40.000000, FALSE)	// cover exit from tunnel
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE : cleared exit from tunnel area of vehicles") ENDIF #ENDIF
					// B*1789860 - clear peds in pedestrian area who keep spawning - can' set non creation area earler since it's used in diff pos
					CLEAR_AREA_OF_PEDS(<< -497.22577, -710.61609, 32.22013 >>, 5.85)	// North on the path - side of underground
					CLEAR_AREA_OF_PEDS(<< -498.18228, -719.31335, 32.21197 >>, 2.15)	// near scenario bin
					CLEAR_AREA_OF_PEDS(<< -497.90323, -724.97406, 32.21197 >>, 3.8)		// south of above spot
					CLEAR_AREA_OF_PEDS(<< -499.56677, -731.96655, 32.21197 >>, 7.3)		// further south again	
					CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE : cleared peds at pedestrian area near underground!")
				ENDIF
				
				SET_PED_PATHS_IN_AREA(<< -496.28848, -681.55994, 25.0 >>, << -481.59882, -668.74115, 38.0 >>, FALSE)	
				SET_PED_NON_CREATION_AREA(<< -496.28848, -681.55994, 25.0 >>, << -481.59882, -668.74115, 38.0 >>)
				
				// pedestrian area where player cuts across over the road from the tunnel
				scenarioBlockingChaseRoute[0] = ADD_SCENARIO_BLOCKING_AREA(<< -497.21988, -679.97705, 22.01260 >>, << -486.81241, -672.01343, 41.80794 >>)
				// continuing in that pedestrian along the chase route
				scenarioBlockingChaseRoute[1] = ADD_SCENARIO_BLOCKING_AREA(<< -489.72855, -680.74554, 22.01260 >>, << -497.19778, -690.00702, 41.80794 >>)
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE 4
			// sets up the exit from second pedestrian area where Di Napoli cuts over the road from the tunnel
			IF fLocal_CurrentPlaybackTime > 61000.0
				// cleanup previous blocking areas
				FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
					REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
				ENDFOR
				
				// turn back on road from Road leading to tunnel exit
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-668.363464,-658.380066,27.433815>>, <<-440.021973,-657.423767,40.251266>>, 40.000000)	// cover exit from tunnel
				
				// B* 1481336 - area where the ramp is leaving the pedestrian area
				
				//SET_PED_PATHS_IN_AREA(<< -508.79080, -764.63538, 28.0 >>, << -488.04373, -711.80060, 38.0 >>, FALSE)
				SET_PED_NON_CREATION_AREA(<< -508.79080, -764.63538, 28.0 >>, << -488.04373, -702.80060, 38.0 >>)
				
				// slimer section covering the ramp
				scenarioBlockingChaseRoute[0] = ADD_SCENARIO_BLOCKING_AREA(<< -503.09494, -754.96472, 29.0 >>, << -494.79437, -730.12964, 37.0 >>)
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK		
		CASE 5
			// sets up area where Di Napoli goes into the car park
			IF fLocal_CurrentPlaybackTime > 65000.0
				// cleanup previous blocking areas
				FOR i = 0 TO (NIGEL2_TOTAL_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
					REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
				ENDFOR
				SET_PED_PATHS_BACK_TO_ORIGINAL(<< -496.28848, -681.55994, 25.0 >>, << -481.59882, -668.74115, 38.0 >>)
				
				SET_PED_PATHS_IN_AREA(<< -463.21530, -829.04749, 22.48120 >>, << -445.46454, -813.10107, 36.56588 >>, FALSE)	
				SET_PED_NON_CREATION_AREA(<< -463.21530, -829.04749, 22.48120 >>, << -445.46454, -813.10107, 36.56588 >>)
	
				scenarioBlockingChaseRoute[0] = ADD_SCENARIO_BLOCKING_AREA(<< -497.21988, -679.97705, 22.01260 >>, << -486.81241, -672.01343, 41.80794 >>)
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE 6
			// B*1533193 - block guards at hospital entrance
			IF fLocal_CurrentPlaybackTime > 75000.0
				// cleanup previous stage
				SET_PED_PATHS_BACK_TO_ORIGINAL(<< -463.21530, -829.04749, 22.48120 >>, << -445.46454, -813.10107, 36.56588 >>)
				
				// covering area in front of hospital doors [0] already used above, update SETUP_CRASH_SITE_FOR_OUTRO_MOCAP if index changes
				scenarioBlockingChaseRoute[1] = ADD_SCENARIO_BLOCKING_AREA(<< 291.58481, -588.88062, 37.0 >>, << 303.76028, -579.58820, 48.0 >>)				
				
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE 7
			// heading through the hospital - setup crash site
			IF fLocal_CurrentPlaybackTime > 114500.0
				// cleanup previous stage
				SETUP_CRASH_SITE_FOR_OUTRO_MOCAP(TRUE)
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    handles everything which needs to happen after END_REPLAY_SETUP() has been called
/// PARAMS:
///    eCurrentStage - which mission stage we are returning to
PROC REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(MISSION_STAGE eReturnToStage)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH eReturnToStage
			CASE MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(22.0)	
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)	
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : ", "MISSION_STAGE_CHASE_TARGET_IN_VEHICLE set gameplay cam heading 15.0 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			BREAK
			CASE MISSION_STAGE_END_CUTSCENE_MOCAP
				// preload the mocap do it kicks in straight away
				IF eMissionSkipTargetStage = MISSION_STAGE_END_CUTSCENE_MOCAP
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, << 391.08, -615.87, 28.33 >>, 333.66)	// pos in outro mocap)
						
					RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : ", "RC_REQUEST_MID_MISSION_CUTSCENE : NMT_2_MCS_2 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
					
					WHILE NOT RC_IS_CUTSCENE_OK_TO_START(TRUE)
						RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : ", "waiting on HAS_CUTSCENE_LOADED : NMT_2_MCS_2 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
						WAIT(0)
					ENDWHILE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC
	  
/// PURPOSE:
///    Handles setup game to fulfil mission stage criteria so it can advance to the next stage
PROC SKIP_STAGE()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		//implement switch statement based of eMissionStage
		//which takes care of cleanup and setup for next stage.
		VECTOR vTempPos
		KILL_ANY_CONVERSATION()
		CLEAR_PRINTS()	
		SWITCH eMissionStage		
			CASE MISSION_STAGE_INTRO_MOCAP_SCENE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_INTRO_MOCAP_SCENE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF			
			BREAK		
			CASE MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					bDone_TargetStartChaseRoute = TRUE //stops playback getting started				
					
					SAFE_PUT_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
					SAFE_PUT_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_BACK_RIGHT)
					
					SAFE_PUT_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
					IF IS_VEHICLE_OK(sTargetVehicle.vehicle)		
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						ENDIF
						SET_ENTITY_COORDS_NO_OFFSET(sTargetVehicle.vehicle, <<393.4298, -621.3337, 28.4891>>)
						SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0081, -0.0061, 0.7994, -0.6007)
						SET_VEHICLE_ON_GROUND_PROPERLY(sTargetVehicle.vehicle)
					ENDIF
					REQUEST_ANIM_DICT("rcmnigel2")	//to play head on horn anim on target in truck
					WHILE NOT HAS_ANIM_DICT_LOADED("rcmnigel2")
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_TARGET_IN_VEHICLE : ", "loading anim dict - rcmnigel2 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						WAIT(0)
					ENDWHILE
					CREATE_VEHICLE_FOR_PLAYER_TO_USE_ON_MISSION_PASSED(TRUE, FALSE)
					CLEAR_AREA_OF_VEHICLES(<< 393.38, -617.53, 27.91 >>, 50.0)	//stops cars appearing at outro location when warping there			
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
						SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, <<364.7079, -608.3558, 27.7364>>, 246.4916) // << 384.26, -609.65, 28.51 >>, 227.32)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 25.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)		
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_TARGET_IN_VEHICLE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					// Added because info from Matt, uber recording isn't updated each frame in the skip because of waits so ambient traffic could appear.
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)					
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 150, FALSE)
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_TARGET_IN_VEHICLE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			CASE MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.					
					SAFE_PUT_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
					SAFE_PUT_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_BACK_RIGHT)	
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
						vTempPos = << 391.08, -615.87, 28.33 >> // GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sTargetVehicle.vehicle, << 0.0, -5.0, 0.0 >>)
						SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, vTempPos, 333.66)	// pos in outro mocap)
						
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_END_CUTSCENE_MOCAP	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 25.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK	
			CASE MISSION_STAGE_END_CUTSCENE_MOCAP
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
					ENDIF	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_END_CUTSCENE_MOCAP : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK	
			DEFAULT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
/// PARAMS:
///    eNewStage - stage to jump to
PROC JUMP_TO_STAGE(MISSION_STAGE eNewStage)
    IF eMissionStage = eNewStage	// end skip stage
		IF IS_REPLAY_BEING_SET_UP()
			VEHICLE_INDEX vehReplayPlayer = NULL
			IF eMissionStage = MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE
			OR eMissionStage = MISSION_STAGE_END_CUTSCENE_MOCAP
				vehReplayPlayer = sNigelVehicle.vehicle
			ENDIF
			END_REPLAY_SETUP(vehReplayPlayer, VS_DRIVER, FALSE)
		ENDIF
		REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(eMissionStage)
		SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS(eMissionStage)
		//B* 1468238 - don't fade back in here when skipping to mocap (let the mocap stage handle it)
		IF eMissionStage = MISSION_STAGE_END_CUTSCENE_MOCAP
			RC_END_Z_SKIP(DEFAULT, FALSE)
			// don't reset the gameplay cams if we are coming out of the mocap
		ELIF eMissionStage = MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
			RC_END_Z_SKIP(FALSE)
		ELSE
			RC_END_Z_SKIP()
		ENDIF
        bFinishedStageSkipping = TRUE
		bLoadedWorldForStageSkipping = FALSE
		// ensure we are fully faded in if we have skipped to the mission passed stage, since the mission passed GUI doesn't display if not (seems to need a frame wait too)
		IF eMissionStage = MISSION_STAGE_MISSION_PASSED
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
    ELSE
    	SKIP_STAGE() 
    ENDIF	
ENDPROC

/// PURPOSE: 
///     Reset the mission, cleanups the current state and set's the mission up again
///     USED by the mission replay checkpoint setup and Debug skips
PROC RESET_MISSION()
	MISSION_CLEANUP(TRUE, FALSE)
	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
		WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_MISSION_FAIL")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - killing music cues") ENDIF #ENDIF
			WAIT(0)
		ENDWHILE
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE, FALSE)	//not done in normal cleanup as this brakes my interpolate camera when mission ends.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_REPLAY_BEING_SET_UP()
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF	
	
	#IF IS_DEBUG_BUILD // should only get called in debug
		//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_2(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " RESET_MISSION - waiting on SetupScene_NIGEL_2")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_NIGEL_2, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
	#ENDIF
	//re do mission initialization
	INIT_MISSION()
	SET_STAGE(MISSION_STAGE_INTRO_MOCAP_SCENE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Perform a Z skip.  Used by the mission checkpoints and the debug Z skip function
/// PARAMS:
///    iNewStage - Mission stage we want to skip to
///    bResetMission - used when we go backwards in mission flow.  If false we also don't stop the active cutscene in RC_START_Z_SKIP, instead handled in SKIP_STAGE to fix bug 1006740
PROC DO_Z_SKIP(INT iNewStage, BOOL bResetMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP with parameters  - iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	RC_START_Z_SKIP(bResetMission)
	// knock out any active music event
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_MISSION_FAIL")
	ENDIF
	IF bResetMission
		RESET_MISSION()
	ENDIF
	eMissionSkipTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
	bFinishedStageSkipping = FALSE
	IF IS_REPLAY_BEING_SET_UP()
		bLoadedWorldForStageSkipping = TRUE
	ELSE
		bLoadedWorldForStageSkipping = FALSE
	ENDIF
	// load world for the mission start area if we are resetting the mission to the intro mocap.  Moved here from script skip stage to fix bug 1006740 - mocap exit states not getting set as game is waiting on world to load before getting to check				
	// basically if you press CROSS to confirm which stage in the z menu, it skipped the mocap but couldn't sent exit states as it was waiting for world to load first.					
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF eMissionSkipTargetStage = MISSION_STAGE_INTRO_MOCAP_SCENE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-1309.0155, -641.8671, 25.5017>>, 242.3463)	//setup player in spot which matches mocap end
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				bLoadedWorldForStageSkipping = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP -  LOADED WORLD ready at mission start area framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP") ENDIF #ENDIF
	JUMP_TO_STAGE(eMissionSkipTargetStage)
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG - J,P and Z skip stuff
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for S, F, J and P skips
	PROC DEBUG_Check_Debug_Keys()
		INT iNewStage
	    // Check for Pass
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()	
			WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_MISSION_FAIL")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - killing music cues") ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			SET_STAGE(MISSION_STAGE_MISSION_PASSED)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "S SKIP") ENDIF #ENDIF
	        Script_Passed()
	    ENDIF
	    // Check for Fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()					
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "F SKIP") ENDIF #ENDIF
		   	SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
	    ENDIF	    
	    // Check for Skip forward
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
	    	iNewStage = ENUM_TO_INT(eMissionStage) + 1
			DO_Z_SKIP(iNewStage, FALSE)	//perform a Z skip to the next stage, without the mission reset
	    	//SKIP_STAGE()	
	    ENDIF	    
	    // Check for Skip backwards
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
	    	iNewStage = ENUM_TO_INT(eMissionStage)-1
		    IF iNewStage > - 1
		    	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "new stage = ", iNewStage) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
		    ENDIF
	    ENDIF	    
	    // Z skip menu
	    IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage)
			// if we are skipping forward in the mission stages, just J skip rather than a full mission reset
			// make sure additional stages are handled here!  These sit at the end of the enum so need to be dealt with differently.
			IF (iNewStage <= ENUM_TO_INT(eMissionStage))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, FALSE)
			ENDIF
	    ENDIF 
	ENDPROC
#ENDIF

/// PURPOSE:
///    setups up the scenario blocking area, and clear areas needed when the mocap starts
///    Also performs the player vehicle resolves for the intro
PROC SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()

	CPRINTLN(DEBUG_MISSION, "DOES SNAPSHOT VEHICLE EXIST = ", DOES_ENTITY_EXIST(g_startSnapshot.mVehicleIndex))
	
	VECTOR vPlayerLastVehicleRespotPosition = << -1290.00, -646.10, 26.05 >>
	Float fPlayerLastVehicleRespotHeading = 307.66
	//GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSE(GET_ENTITY_COORDS(PLAYER_PED_ID()), CHAR_TREVOR, vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading)	// back at safehouse setup
	
	// main road mission triggers on including Nigel's position
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1311.409424,-648.413330,24.925262>>, <<-1289.027588,-632.042969,29.552197>>, 17.5,
														vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE, FALSE)
	// pedestrian road at the start of the chase
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1319.689819,-655.777344,29.458387>>, <<-1184.461304,-834.820068,12.343782>>, 30.0,
														vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE, FALSE)									
	CPRINTLN(DEBUG_MISSION, "SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - finished vehicle resolves - DOES SNAPSHOT VEHICLE EXIST = ", DOES_ENTITY_EXIST(g_startSnapshot.mVehicleIndex), " framecount : ", GET_FRAME_COUNT())
	
	// set the vehicle the player triggered the mission in to regenerate
	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<0,0,0>>, TRUE)
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, FALSE)
		//SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, TRUE, CHAR_TREVOR)	// back at safehouse setup
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "* SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - vehicle gen setup framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "* SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - vehicle to big to setup vehicle gen! framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ENDIF

	//Intro Cutscene area
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1409.34351, -756.38098, 17.59896 >>, << -1239.14758, -587.12195, 31.25266 >>, FALSE)	// match the cleanup call coords
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -1409.34351, -756.38098, 17.59896 >>, << -1239.14758, -587.12195, 31.25266 >>)
	SET_ROADS_IN_ANGLED_AREA(<<-1415.903931,-732.548889,17.545492>>, <<-1236.628174,-601.026489,31.153446>>, 23.000000, FALSE, FALSE)	// match the cleanup call coords
	CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1415.903931,-732.548889,17.545492>>, <<-1236.628174,-601.026489,31.153446>>, 23.000000, FALSE, FALSE, FALSE, TRUE, TRUE)	// near side of the main road
	
	SET_PED_PATHS_IN_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>, FALSE)
	SET_PED_NON_CREATION_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>)
	scenarioBlockingIntroArea = ADD_SCENARIO_BLOCKING_AREA(<< -1318.50049, -678.88257, 23.0 >>, << -1296.62781, -631.54492, 29.0 >>)
	CLEAR_AREA_OF_OBJECTS(<< -1297.80, -637.42, 25.55 >>, 16.0)		
	// pedestrian area at start of the chase - first section (middle road when you turn onto shopping street
	scenarioBlockingChaseRoute[0] = ADD_SCENARIO_BLOCKING_AREA(<< -1315.67981, -682.76141, 23.24123 >>, << -1295.26648, -670.37604, 27.97680 >>)
	CLEAR_AREA(<< -1305.33203, -676.37390, 25.79325 >>, 10.0, TRUE)
	// pedestrian area at start of the chase - part way down first section on east side road
	scenarioBlockingChaseRoute[1] = ADD_SCENARIO_BLOCKING_AREA(<< -1286.90613, -697.55927, 21.73920 >>, << -1279.69836, -691.58759, 26.33636 >>)
	CLEAR_AREA(<< -1287.70203, -698.19574, 27.50166 >>, 10.0, TRUE)
	// pedestrian area at start of the chase - first open section
	scenarioBlockingChaseRoute[2] = ADD_SCENARIO_BLOCKING_AREA(<< -1270.72009, -723.89026, 19.46898 >>, << -1262.89795, -718.27856, 24.43682 >>)
	CLEAR_AREA(<< -1269.86023, -722.46637, 21.54627 >>, 8.0, TRUE)
	// pedestrian area at start of the chase - infront of second section
	scenarioBlockingChaseRoute[3] = ADD_SCENARIO_BLOCKING_AREA(<< -1268.05750, -731.51825, 18.92047 >>, << -1263.57446, -727.55023, 24.05783 >>)
	CLEAR_AREA(<< -1266.43066, -728.92267, 21.08542 >>, 2.5, TRUE)
	// pedestrian area at start of the chase - second open section (not including bottom clusters)
	scenarioBlockingChaseRoute[3] = ADD_SCENARIO_BLOCKING_AREA(<< -1237.02380, -777.42224, 15.58289 >>, << -1221.88904, -764.59692, 20.58871 >>)
	CLEAR_AREA(<< -1231.44055, -770.94794, 17.70891 >>, 8.5, TRUE)
	// pedestrian area at start of the chase - bottom of second open section
	scenarioBlockingChaseRoute[4] = ADD_SCENARIO_BLOCKING_AREA(<< -1222.44775, -786.73486, 13.60073 >>, << -1215.77295, -780.20215, 20.43715 >>)
	CLEAR_AREA(<< -1222.00769, -783.68341, 16.73119 >>, 7.5, TRUE)
	
	// B*1763717 - clear area at junction of vehicles sine these turn into the road where nodes are blocked
	CLEAR_AREA_OF_VEHICLES(<< -1459.2, -736.9, 23.6 >>, 50.0, FALSE, FALSE, FALSE, TRUE)	// Junction turning left onto scene road from North West
	CLEAR_AREA_OF_VEHICLES(<< -1332.2, -537.6, 31.5 >>, 30.0, FALSE, FALSE, FALSE, TRUE)	// road behind where Nigel is (North)
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		Mission Stages			
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Trevors spots Nigel on the street, Mrs Thornhill has seen Al Di Napoli.
///    Nigel asks Trevor to help them get Al Di Napoli "somewhere prviate so they can talk"
PROC INTRO_MOCAP_SCENE()

	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			// don't request the mocap if we are skipping past this stage
			IF NOT bFinishedStageSkipping
				IF NOT IS_REPLAY_BEING_SET_UP()
					SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-1309.0155, -641.8671, 25.5017>>, 242.3463)	//setup player in spot which matches mocap end
					// load world if target stage is the next stage				
					IF bLoadedWorldForStageSkipping = FALSE
						IF eMissionSkipTargetStage = MISSION_STAGE_CHASE_TARGET_IN_VEHICLE	
							IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
								WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)		
								bLoadedWorldForStageSkipping = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE : SS_SETUP - bFinishedStageSkipping - LOADED WORLD framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-1309.0155, -641.8671, 25.5017>>, 242.3463, TRUE, TRUE)	//second attempt put the player on the floor B* 1076463
				ENDIF
				// remove the temporary RC entities which were in the initial scene
				SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
				SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
				SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
				
				SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()	
				LOAD_MISSION_ASSETS(TRUE)
				SETUP_FOR_START_OF_MISSION()
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)				
					SET_VEHICLE_ON_GROUND_PROPERLY(sNigelVehicle.vehicle)	// ensure it's positioned correctly to match mocap cut
				ENDIF
				UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)	
				SETUP_UBER_PLAYBACK()
				CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP done for bFinishedStageSkipping FC = ", GET_FRAME_COUNT()) 
				eSubStage = SS_CLEANUP
			ELSE
				RC_REQUEST_CUTSCENE("NMT_2_RCM")
				
				// B*1347748 - set Di Napoli to trouser's variation
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle_AlDiNapoli, PED_COMP_LEG, 0, 0, sTargetPed.model)	// default now has pants on show! so set trousers version for mocap
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SET_CUTSCENE_PED_COMPONENT_VARIATION Di Napoli trousers FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
					
				IF RC_IS_CUTSCENE_OK_TO_START(TRUE)
				
					// reset the vehicles positions from the initial scene incase the player had tampered with them B*1079638
					// nigel's car
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
						ASSIGN_VEHICLE_INDEX(sNigelVehicle.vehicle, sRCLauncherDataLocal.vehID[0])
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP assigned sRCLauncherData.vehID[0] as Nigel vehicle") ENDIF #ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(sNigelVehicle.vehicle)
						SET_VEHICLE_FIXED(sNigelVehicle.vehicle)
						SET_ENTITY_PROOFS(sNigelVehicle.vehicle, FALSE, FALSE, FALSE, FALSE, FALSE)	// B*1281090 - initial scene set on, but turned back off when getting handle
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP reset Nigel vehicle") ENDIF #ENDIF
					ENDIF
					//Target vehicle
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[1])
						ASSIGN_VEHICLE_INDEX(sTargetVehicle.vehicle, sRCLauncherDataLocal.vehID[1])
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP assigned sRCLauncherData.vehID[1] as Target vehicle") ENDIF #ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
						SET_VEHICLE_FIXED(sTargetVehicle.vehicle)		// potentially need to remove this and just have the mocap create me some new vehicles!
						SET_ENTITY_PROOFS(sTargetVehicle.vehicle, FALSE, FALSE, FALSE, FALSE, FALSE)	// B*1281090 - initial scene set on, but turned back off when getting handle
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP reset Target's vehicle") ENDIF #ENDIF
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Trevor, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(sNigelPed.ped, sSceneHandle_Nigel, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sNigelPed.model)
					REGISTER_ENTITY_FOR_CUTSCENE(sMrsThornhillPed.ped, sSceneHandle_MrsThornhill, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sMrsThornhillPed.model)
					REGISTER_ENTITY_FOR_CUTSCENE(sTargetPed.ped, sSceneHandle_AlDiNapoli, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sTargetPed.model)
					IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(sNigelVehicle.vehicle, sSceneHandle_NigelVehicle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - registering Nigel's vehicle existing entity to be animated by mocap")
					ELSE
						SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
						SAFE_DELETE_VEHICLE(sNigelVehicle.vehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(sNigelVehicle.vehicle, sSceneHandle_NigelVehicle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sNigelVehicle.model)
						CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - registering Nigel's vehicle new entity to be created by mocap")
					ENDIF
					IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(sTargetVehicle.vehicle, sSceneHandle_AlDiNapoliVehicle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - registering Target's vehicle existing entity to be animated by mocap")
					ELSE
						SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
						SAFE_DELETE_VEHICLE(sTargetVehicle.vehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(sTargetVehicle.vehicle, sSceneHandle_AlDiNapoliVehicle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sTargetVehicle.model)
						CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - registering Target's vehicle new entity to be created by mocap")
					ENDIF
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, mainly for repeat play purposes.
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
					
					START_CUTSCENE()
					WAIT(0)	//added because the cutscene doesn't start straight away.  This ensures peds / cars aren't visibly cleared.
					
					RC_CLEANUP_LAUNCHER()	// ensure called same frame as mission blocking area are setup
					
					RC_START_CUTSCENE_MODE(<< -1310.70, -640.22, 26.54 >>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE) //has to be after start_cutscene and wait(0)
					SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()
					
					// remove the temporary RC entities which were in the initial scene
					SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
					//SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
					//SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
					
					// B*1247905 - limit density for framerate rest of the scene
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
					
					LOAD_MISSION_ASSETS()				
					UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)					
									
					CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_SETUP done")
					eSubStage = SS_UPDATE
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "WAITING FOR INTRO MOCAP TO LOAD - NMT_2_RCM") ENDIF #ENDIF
				ENDIF	
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE		
			UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)				
			
			// Get handle to Nigel ped in the mocap
			IF NOT DOES_ENTITY_EXIST(sNigelPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Nigel))
					sNigelPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Nigel))
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sNigelPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
			// Get handle to Nigel ped in the mocap
			IF NOT DOES_ENTITY_EXIST(sMrsThornhillPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MrsThornhill))
					sMrsThornhillPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MrsThornhill))
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sMrsThornhillPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
			// Get handle to target ped in the mocap
			IF NOT DOES_ENTITY_EXIST(sTargetPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_AlDiNapoli))
					sTargetPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_AlDiNapoli))
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sTargetPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
			// Get handle to Nigel's vehicle in the mocap
			IF NOT DOES_ENTITY_EXIST(sNigelVehicle.vehicle)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_NigelVehicle))
					sNigelVehicle.vehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_NigelVehicle))
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sNigelVehicle.vehicle - got handle to mocap registered entity FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
			// Get handle to target's vehicle in the mocap
			IF NOT DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_AlDiNapoliVehicle))
					sTargetVehicle.vehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_AlDiNapoliVehicle))
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sTargetVehicle.vehicle - got handle to mocap registered entity FC = ", GET_FRAME_COUNT())
				ENDIF
			ENDIF

			// Set exit state for Nigel, Mrs T and Nigel's vehicle
			IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
			AND IS_PED_UNINJURED(sNigelPed.ped)
			AND IS_PED_UNINJURED(sMrsThornhillPed.ped)				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Nigel)
					REPLAY_STOP_EVENT()
					IF NOT IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
						SET_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(sNigelPed.ped)
						//FORCE_PED_MOTION_STATE(sNigelPed.ped, MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Nigel put in his car in ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Nigel in ss_update Frame Count : ", GET_FRAME_COUNT())
					SETUP_NIGEL_FOR_IN_GAME_START()		
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_MrsThornhill)
					IF NOT IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)
						SET_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_BACK_RIGHT)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(sMrsThornhillPed.ped)
						//FORCE_PED_MOTION_STATE(sMrsThornhillPed.ped, MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Mrs T put in his car in ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF						
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Mrs T in ss_update Frame Count : ", GET_FRAME_COUNT())
					SETUP_MRS_THORNHILL_FOR_IN_GAME_START()
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_NigelVehicle)
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Nigel vehicle wcTF in ss_update Frame Count : ", GET_FRAME_COUNT())
					SETUP_NIGEL_VEHICLE_FOR_IN_GAME_START()					
					SET_VEHICLE_DOORS_SHUT(sNigelVehicle.vehicle, TRUE)					
					SET_VEHICLE_ON_GROUND_PROPERLY(sNigelVehicle.vehicle)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sNigelVehicle.vehicle)	// B*1404500 - stops doors staying open					
				ENDIF
				// B*1546501 - stops vehicle pop after mocap - set this everyframe as soon as possible before exit state / end of mocap until a little while after
				SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, TRUE)
				bSetNigelVehicleHDForMocapExit = TRUE
			ENDIF
			
			// Set exit state for Al Di Napoli
			IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
			AND IS_PED_UNINJURED(sTargetPed.ped)				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_AlDiNapoli)
					IF NOT IS_PED_SITTING_IN_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
						SET_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle, VS_DRIVER)
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Al Di Napoli put in his car in ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					SETUP_TARGET_PED_FOR_IN_GAME_START()						
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Al Di Napoli in ss_update Frame Count : ", GET_FRAME_COUNT())
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_AlDiNapoliVehicle)
					CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for Al Di Napoli vehicle in ss_update Frame Count : ", GET_FRAME_COUNT())				
					SETUP_TARGET_VEHICLE_FOR_IN_GAME_START()					
					SET_VEHICLE_DOORS_SHUT(sTargetVehicle.vehicle, TRUE)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sTargetVehicle.vehicle)	// B*1404500 - stops doors staying open					
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(22.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ", " set exit state for camera (H = 15.0 | P = 0.0) in ss_update Frame Count : ", GET_FRAME_COUNT())
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
			AND IS_SCREEN_FADED_OUT()
				// NOTE: if this gets removed consider moving SETUP_UBER_PLAYBACK() call or at least making sure vehicle are cleared for a skipped cutscene
				CLEAR_AREA_OF_VEHICLES(<<-1312.5560, -664.3428, 25.5716>>, 150)	//larger area for skipping since we are faded down anyway
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Clear cars  - skipped over the mocap FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF			
				
			IF NOT IS_CUTSCENE_PLAYING()
				CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_UPDATE - mocap cutscene finished FC = ", GET_FRAME_COUNT())			
				LOAD_MISSION_ASSETS(TRUE)				
				SETUP_FOR_START_OF_MISSION()
				SETUP_UBER_PLAYBACK(FALSE)
				eSubStage = SS_CLEANUP
			ELSE
				// B*1366548 - prevent cutscene skip during blend out
				IF GET_CUTSCENE_TIME() >= 104000
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
				// B*1359637 - prevent ambient cars spawning in distance during some shots
				// B*1506934 - prevent ambient peds spawning in distance during same shots
				IF GET_CUTSCENE_TIME() > 900
				AND GET_CUTSCENE_TIME() < 1480
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				ELSE	// B*1247905 - limit density for framerate rest of the scene
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
				ENDIF
				LOAD_MISSION_ASSETS()
			ENDIF				
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP		
			IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
				// B*1546501 - stops vehicle pop after mocap - set this everyframe as soon as possible before exit state / end of mocap until a little while after
				SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, TRUE)
				bSetNigelVehicleHDForMocapExit = TRUE
			ENDIF
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ELSE
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ENDIF
				IF bFinishedStageSkipping	//only do this if we aren't stage skipping
					RC_END_CUTSCENE_MODE()
					SET_MULTIHEAD_SAFE(FALSE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RC Nigel 2 : ", "INTRO_MOCAP_SCENE - SS_CLEANUP - RC_END_CUTSCENE_MODE() called as not skipping stage") ENDIF #ENDIF	
				ENDIF
				bForceTargetblipFlashThisFrame = FALSE	// initial set to false
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)				
				SET_STAGE(MISSION_STAGE_CHASE_TARGET_IN_VEHICLE)   //   MISSION_STAGE_CHASE_TARGET_IN_VEHICLE  //MISSION_STAGE_DEBUG
				CPRINTLN(DEBUG_MISSION, "INTRO_MOCAP_SCENE - SS_CLEANUP done")
			ENDIF			
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    car chases where Trevor, Nigel and Mrs Thornhill drives after Al Di Napoli
PROC CHASE_TARGET_IN_VEHICLE()	

	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)	// lower chance of peds interfering in route
	
 	SWITCH eSubStage	
		//	------------------------------------------
	 	CASE SS_SETUP			
			IF IS_PED_UNINJURED(sTargetPed.ped)
			AND IS_VEHICLE_OK(sTargetVehicle.vehicle)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTargetPed.ped, TRUE)		//BUT HE STILL GETS OUT OF THE VEHICLE IF ITS NOT DRIVEABLE!				
				SET_PED_RESET_FLAG(sTargetPed.ped, PRF_UseFastEnterExitVehicleRates, TRUE)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(sTargetVehicle.vehicle, "NIGEL_02_CHASE_CAR_MG")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " NIGEL 02 -	AUDIO_SCENE target vehicle added : NIGEL_02_CHASE ", "***") ENDIF #ENDIF				
			ENDIF			
			IF IS_PED_UNINJURED(sMrsThornhillPed.ped)		
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMrsThornhillPed.ped, TRUE)
			ENDIF
			IF IS_PED_UNINJURED(sNigelPed.ped)	
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sNigelPed.ped, TRUE)
			ENDIF
			IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
				IF NOT bStatTracker_IsNigelVehicleDamageStatActive
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(sNigelVehicle.vehicle,NI2_VEHICLE_DAMAGE)	// begin tracking nigel's vehicle damage for stats
					bStatTracker_IsNigelVehicleDamageStatActive = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(sNigelVehicle.vehicle)") ENDIF #ENDIF
				ENDIF				
				// B*1546501 - stops vehicle pop after mocap - set this everyframe as soon as possible before exit state / end of mocap until a little while after
				SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, TRUE)
				bSetNigelVehicleHDForMocapExit = TRUE
			ENDIF	
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 15.0, REPLAY_IMPORTANCE_LOW)
			
			UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, TRUE)	// ensure player uses direct door access as per bug 1044490
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())	// clear this ready to be used by player injuring / killing someone inside the hospital checks
			CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE - SS_SETUP done")
			eSubStage = SS_UPDATE			
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE	
			MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE(NIGEL2_MUSIC_EVENT_START)
			MANAGE_HOSPITAL_STATE_DURING_CHASE()
			bForceTargetblipFlashThisFrame = FALSE	// initial set to false
			
			// used by mission fail checks, dialogue triggers, uber recording rubberbanding
			IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
				fCurrentChaseDistance = VDIST(vPlayerPos, GET_ENTITY_COORDS(sTargetVehicle.vehicle, FALSE))	
			ENDIF
			
			IF IS_PED_UNINJURED(sTargetPed.ped)
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)					
					/*
					IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
						IF (GET_VEHICLE_ENGINE_HEALTH(sNigelVehicle.vehicle) < 800.0)
							SET_VEHICLE_ENGINE_HEALTH(sNigelVehicle.vehicle, 800.0)	//ensure the nigrl vehicle doesn't get wrecked.
						ENDIF
					ENDIF
					*/				
					IF NOT bDone_TargetStartChaseRoute
						SET_PED_RESET_FLAG(sTargetPed.ped, PRF_UseFastEnterExitVehicleRates, TRUE)
						IF IS_PED_SITTING_IN_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
						AND (GET_PED_IN_VEHICLE_SEAT(sTargetVehicle.vehicle, VS_DRIVER) = sTargetPed.ped)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
								FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
								START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sTargetVehicle.vehicle)
								fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)
								//fMainPlaybackSpeed 	= 0.85	// 1.0 reduce speed to begin with to allow player chance to get in the car
								fMainPlaybackSpeed 	= 0.85	// 1.0	// REMOVE BEFORE SUBMIT
								SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)							
								PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()			
								UPDATE_UBER_PLAYBACK(sTargetVehicle.vehicle, fMainPlaybackSpeed)
								UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING()
								IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_02_CHASE")
									IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
									AND bFinishedStageSkipping
										START_AUDIO_SCENE("NIGEL_02_CHASE")	
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " NIGEL 02 -	START_AUDIO_SCENE : NIGEL_02_CHASE ", "***") ENDIF #ENDIF
									ELSE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " NIGEL 02 -	START_AUDIO_SCENE : skipped for replay being active NIGEL_02_CHASE ", "***") ENDIF #ENDIF
									ENDIF
								ENDIF
								/*// B* 1490105 - currently only reliable way to stop peds drivign the uber cars interfering with uber chase because of reaction to player shooting
								looks pretty gash since no one reacts...would of been better if uber veh don't react until released and when released have the can't use veh flee flag set so they just dive out the car
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS_FOR_AMBIENT_PEDS_THIS_FRAME(TRUE)
								ENDIF*/
								bDone_TargetStartChaseRoute = TRUE
								CPRINTLN(DEBUG_MISSION, "bDone_TargetStartChaseRoute - UBER CHASE started FC : ", GET_FRAME_COUNT())
							ENDIF
						ENDIF	
					ELSE				
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)					
							UPDATE_UBER_CHASE_SPEED(fMainPlaybackSpeed, fCurrentPlaybackTime)
							SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)						
							PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()										
							UPDATE_UBER_PLAYBACK(sTargetVehicle.vehicle, fMainPlaybackSpeed)
							UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING()														
							MANAGE_TARGET_BRAKING_AND_USING_HORN()
							APPLY_VISUAL_DAMAGE_TO_TARGET_VEHICLE()
							CREATE_VEHICLE_FOR_PLAYER_TO_USE_ON_MISSION_PASSED()							
							IF fCurrentPlaybackTime > 82000.0	// attempt to get cars on the highway to reliably spawn in
							AND fCurrentPlaybackTime < 105000.0
								//fUberPlaybackMinCreationDistance = 80.0 //The minimum distance a car has to be from the player to be allowed to create.  FROM traffic.sch
								CREATE_ALL_WAITING_UBER_CARS()
							ENDIF
							/*// B* 1490105 - currently only reliable way to stop peds drivign the uber cars interfering with uber chase because of reaction to player shooting
							looks pretty gash since no one reacts...would of been better if uber veh don't react until released and when released have the can't use veh flee flag set so they just dive out the car
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS_FOR_AMBIENT_PEDS_THIS_FRAME(TRUE)
							ENDIF*/
							//NOTE: temp as cars are taking far more damage in latest build
							IF (GET_VEHICLE_ENGINE_HEALTH(sTargetVehicle.vehicle) < 800.0)
								SET_VEHICLE_ENGINE_HEALTH(sTargetVehicle.vehicle, 800.0)	//ensure the target vehicle doesn't get wrecked.
							ENDIF
						ELSE
							IF NOT bDone_UberRecordingCleanupForChaseEnd			
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "END PLAYBACK TIME IS - ", fCurrentPlaybackTime) ENDIF #ENDIF
								CLEANUP_UBER_PLAYBACK()
								REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
								//main car recording
								REMOVE_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)
								REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sTargetVehicle.vehicle)
								IF IS_AUDIO_SCENE_ACTIVE("NIGEL_02_CHASE")								
									STOP_AUDIO_SCENE("NIGEL_02_CHASE")	
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " NIGEL 02 -	STOP_AUDIO_SCENE : NIGEL_02_CHASE ", "***") ENDIF #ENDIF
								ENDIF
								SETUP_CRASH_SITE_FOR_OUTRO_MOCAP()
								SETUP_TARGET_FOR_CRASH()							
								IF fCurrentPlaybackTime < NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT	//mainly for debug skip purpose, it sets time pass point the damage can be applied
									fCurrentPlaybackTime = (NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT + 1000.0)
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INCREASED PLAYBACK TIME MANUALLY FOR VISUAL DAMAGE IN SS_CLEANUP") ENDIF #ENDIF
								ENDIF
								APPLY_VISUAL_DAMAGE_TO_TARGET_VEHICLE()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UBER RECORDING ENDED, set up for crash scene") ENDIF #ENDIF
								bDone_UberRecordingCleanupForChaseEnd = TRUE
							ELSE
								SET_TARGET_PLAY_HEAD_ON_WHEEL_ANIM()
							ENDIF					
						ENDIF
					ENDIF					
				ENDIF
				IF bDoneCleanup_JumpOutHospitalWindow	// B*1557320 - don't allow during scripted camera jump out the window
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetPed.ped)
				ENDIF
				MANAGE_RAYFIRE_HOSPITAL_DOORS()
				MANAGE_HOSPITAL_SOUND_FX()
				UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE(fCurrentPlaybackTime)
				MANAGE_PEDESTRIANS_ON_CHASE_ROUTE(vPlayerPos)
				MANAGE_HOSPITAL_DEBRIS_EFFECT()
				NIG2_TRACK_STAT_PLAYER_KEEPS_CLOSE_TO_TARGET()
				UPDATE_SCRIPTED_VEHICLES_AROUND_START_LOCATION(fMainPlaybackSpeed, vPlayerPos)	//this needs to be below uber stuff as it uses fPlaybackTime				
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
				AND IS_PED_UNINJURED(sNigelPed.ped)
				AND IS_PED_UNINJURED(sMrsThornhillPed.ped)
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
						IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_03")
							CLEAR_THIS_PRINT("NIGEL2_03")
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_04")
							CLEAR_THIS_PRINT("NIGEL2_04")		// Get back in ~b~Nigel's car.~s~
						ENDIF
						SAFE_REMOVE_BLIP(sNigelVehicle.blip)
						// reset flag once player is in the vehice
						IF bSetNigelVehicleHDForMocapExit
							SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, FALSE)	// B*1546501 - stops vehicle pop after mocap
							bSetNigelVehicleHDForMocapExit = FALSE
						ENDIF
						
						// check if nigel and mrs T are in the car
						IF NOT IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
						OR NOT IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)
							SAFE_REMOVE_BLIP(sTargetPed.blip)
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_04")
								CLEAR_THIS_PRINT("NIGEL2_04")		//Chase after ~r~Al Di Napoli.~w~
							ENDIF													
							//IF NOT bDoneObjective_WaitForNigelAndMrsT							
							//	PRINT_NOW("NIGEL2_05", DEFAULT_GOD_TEXT_TIME, 1)	//Wait for ~b~Nigel and Mrs Thornhill.~w~
							//	bDoneObjective_WaitForNigelAndMrsT = TRUE
							//ENDIF	
						ELSE	
							//get the engine on quicker for the player at the start of the chase
							IF NOT bDone_TurnEngineOnForPlayer
								IF (GET_PED_IN_VEHICLE_SEAT(sNigelVehicle.vehicle, VS_DRIVER) = PLAYER_PED_ID())
									SET_VEHICLE_ENGINE_ON(sNigelVehicle.vehicle, TRUE, FALSE)
									//!!! comment out for now as something changed in v189.1 making it really quick, too fast for the target
									bDone_TurnEngineOnForPlayer = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TURNED ENGINE ON FOR THE PLAYER") ENDIF #ENDIF
								ENDIF
							ENDIF
							MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE(NIGEL2_MUSIC_EVENT_CAR)
							IF NOT bDone_PlayerInNigelVehicle_FirstTime
								PRINT_NOW("NIGEL2_01", DEFAULT_GOD_TEXT_TIME, 1)	//Chase after ~r~Al Di Napoli.~w~
								bDone_PlayerInNigelVehicle_FirstTime = TRUE
							ENDIF
							bDone_KillConvoForOutTheVehicleDialogue = FALSE	//reset bool used to stop dialogue when player gets out the car
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_05")
								CLEAR_THIS_PRINT("NIGEL2_05")	//return to ~b~Nigel~w~
							ENDIF		
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_08")
								CLEAR_THIS_PRINT("NIGEL2_08")	//return to ~b~Nigel~s~ and ~b~Mrs. Thornhill.~s~
							ENDIF	
							IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_09")
								CLEAR_THIS_PRINT("NIGEL2_09")	//Return to ~b~Mrs. Thornhill.~s~
							ENDIF	
							MANAGE_DIALOGUE_DURING_CHASE(bForceTargetblipFlashThisFrame)	// needs to come before UPDATE_CHASE_BLIP since it sets bForceTargetblipFlashThisFrame
							IF NOT DOES_BLIP_EXIST(sTargetPed.blip)
								sTargetPed.blip = CREATE_PED_BLIP(sTargetPed.ped, TRUE, FALSE)
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(sTargetPed.ped)
									SET_BLIP_SCALE(sTargetPed.blip, 1.0)	//TEMP: added as per bug 241312
								ENDIF
								// must come after MANAGE_DIALOGUE_DURING_CHASE since this controls bForceTargetblipFlashThisFrame override
								UPDATE_CHASE_BLIP(sTargetPed.blip, sTargetPed.ped, NIGEL2_TARGET_ESCAPED_DISTANCE, NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE, bForceTargetblipFlashThisFrame)
							ENDIF														
							//Check for player close to target at end of the chase.
							IF bDone_TargetStartChaseRoute
								IF bDone_UberRecordingCleanupForChaseEnd
									//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sTargetPed.ped), NIGEL2_PLAYER_CLOSE_TO_TARGET_CRASHED, 0, 0, 255, 120)
									IF(fCurrentChaseDistance < NIGEL2_PLAYER_CLOSE_TO_TARGET_CRASHED)
										CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE - SS_UPDATE - PLAYER CLOSE TO CRASHED AL DI NAPOLI - GO TO PUT IN BOOT SECTION")
										eSubStage = SS_CLEANUP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						MANAGE_HOSPITAL_JUMP_FX()
					ELSE
						// B*1546501 - stops vehicle pop after mocap - keep applying everyframe whilst it has initially been applied
						IF bSetNigelVehicleHDForMocapExit
							SET_FORCE_HD_VEHICLE(sNigelVehicle.vehicle, TRUE)
							bSetNigelVehicleHDForMocapExit = TRUE
						ENDIF
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)	// reset flag for ensure player uses direct door access as per bug 1044490
						SAFE_REMOVE_BLIP(sTargetPed.blip)
						IF NOT DOES_BLIP_EXIST(sNigelVehicle.blip)
							sNigelVehicle.blip = CREATE_VEHICLE_BLIP(sNigelVehicle.vehicle, TRUE, BLIPPRIORITY_HIGH)
						ENDIF
						IF NOT bDone_PlayerInNigelVehicle_FirstTime	
							IF NOT bDoneDialogue_GetInNigelsVehicle[0]
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_DAWDM", CONV_PRIORITY_MEDIUM)
									// Come on Jock, get in!
									bDoneDialogue_GetInNigelsVehicle[0] = TRUE
								ENDIF
							ELIF NOT bDoneObjective_Initial_GetInNigelsVehicle
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									PRINT_NOW("NIGEL2_03", DEFAULT_GOD_TEXT_TIME, 1)	//Get into ~b~Nigel's car.~w~ 
									bDoneObjective_Initial_GetInNigelsVehicle = TRUE
								ENDIF
							ELIF NOT bDoneDialogue_GetInNigelsVehicle[1]
								//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_03")		//*commented out for new display message check with subtitle profile settings
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
								AND IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)
									IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_SPOTSP", CONV_PRIORITY_HIGH)
										// Hurry, we're going to lose him!  
										bDoneDialogue_GetInNigelsVehicle[1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE	
							//kill off any dialogue which is based on the player being in the car
							IF NOT bDone_KillConvoForOutTheVehicleDialogue
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								bDone_KillConvoForOutTheVehicleDialogue = TRUE
							ENDIF							
							IF NOT bDoneObjective_GetBackInVehicle					
								PRINT_NOW("NIGEL2_04", DEFAULT_GOD_TEXT_TIME, 1)	//Get back into ~b~Nigel's car.~w~
								iTimer_TrevorGetBackInCarDialogue = GET_GAME_TIMER()
								bDoneObjective_GetBackInVehicle = TRUE
							ELSE							
								//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_04")	//Get back into ~b~Nigel's car.~w~		//*commented out for new display message check with subtitle profile settings
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()									
										IF (fDistPlayerToNigelVehicle < 20.0)					//fDistPlayerToNigelVehicle is calculated in MISSION_FAILED_CHECKS()
											IF (GET_GAME_TIMER() - iTimer_TrevorGetBackInCarDialogue) > GET_RANDOM_INT_IN_RANGE(4500, 6500)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)	//needed to test him in getting in car state
													//Do additional dialogue telling player to get back in the car
													// random select between Nigel and Mrs Thornhill comments
													IF (GET_RANDOM_INT_IN_RANGE(0, 11) > 5)
														// Nigel lines
														IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_GIC", CONV_PRIORITY_HIGH)
															// Jock, what are you doing?
															// Where are you going?
															// We've come this far, don't desert us now!   
															#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - trevor leaving N & MRS T behind  - NIG2_GIC") ENDIF #ENDIF
														ENDIF
													ELSE
														// Mrs Thornhill lines
														IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_GIC2", CONV_PRIORITY_HIGH)
															// Jock! Please! We need you!  
															// We'll never catch him on foot, not with Nigel's hip!  
															#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE -trevor leaving N & MRS T behind - NIG2_GIC2") ENDIF #ENDIF
														ENDIF
													ENDIF								
												ENDIF
											ENDIF
										ENDIF
									ELSE
										iTimer_TrevorGetBackInCarDialogue = GET_GAME_TIMER()
									ENDIF
								//ENDIF
							ENDIF
						ENDIF	
						HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()	// make buddy's look at the player as he enters the vehicle.
					ENDIF
				ENDIF
			ENDIF
			MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
			NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL()
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP		
			IF NOT bDone_UberRecordingCleanupForChaseEnd
				CLEANUP_UBER_PLAYBACK()
				REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
				IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						//check if its at the end time wise, if so freeze its pos?
						STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						// SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF
					FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)
				ENDIF				
				REMOVE_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)	
				SETUP_CRASH_SITE_FOR_OUTRO_MOCAP()
				IF IS_AUDIO_SCENE_ACTIVE("NIGEL_02_CHASE")					
					STOP_AUDIO_SCENE("NIGEL_02_CHASE")	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE : SS_CLEANUP : STOP_AUDIO_SCENE : NIGEL_02_CHASE ", "***") ENDIF #ENDIF
				ENDIF							
				IF fCurrentPlaybackTime < NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT	//mainly for debug skip purpose, it sets time pass point the damage can be applied
					fCurrentPlaybackTime = (NIGEL2_TARGET_VEHICLE_VISUAL_DAMAGE_POINT + 1000.0)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INCREASED PLAYBACK TIME MANUALLY FOR VISUAL DAMAGE IN SS_CLEANUP") ENDIF #ENDIF
				ENDIF
				APPLY_VISUAL_DAMAGE_TO_TARGET_VEHICLE()
				SETUP_TARGET_FOR_CRASH()
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UBER RECORDING ENDED IN SS_CLEANUP") ENDIF #ENDIF
				bDone_UberRecordingCleanupForChaseEnd = TRUE
			ENDIF
			IF IS_ENTITY_ALIVE(sTargetPed.ped)
				IF bDoneCleanup_JumpOutHospitalWindow	// B*1557320 - don't allow during scripted camera jump out the window
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetPed.ped)
				ENDIF
			ENDIF
			MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
			IF bStatTracker_IsNigelVehicleDamageStatActive
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL,NI2_VEHICLE_DAMAGE)	// end tracking nigel's vehicle damage for stats
				bStatTracker_IsNigelVehicleDamageStatActive = FALSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE - SS_CLEANUP - INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)") ENDIF #ENDIF
			ENDIF
			/*IF NOT IS_REPLAY_BEING_SET_UP()
				IF eHospitalInteriorState = HIS_ACTIVE_NEW_LOAD_SCENE_AT_INTERIOR
				OR eHospitalInteriorState = HIS_CLEANUP_NEW_LOAD_SCENE_AT_INTERIOR
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
						eHospitalInteriorState = HIS_END
						CPRINTLN(DEBUG_MISSION, " CHASE_TARGET_IN_VEHICLE : IS_NEW_LOAD_SCENE_ACTIVE - NEW_LOAD_SCENE_STOP() : FC = ", GET_FRAME_COUNT())
					ENDIF
				ENDIF	
			ENDIF*/
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			//KILL_ANY_CONVERSATION()			
			IF bDone_HospitalJumpPaperTrailFX
				IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalJump_PTFX_ID)
					STOP_PARTICLE_FX_LOOPED(HospitalJump_PTFX_ID)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PTFX hospital jump stopped FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			//hospital sfx cleanup
			RELEASE_SCRIPT_AUDIO_BANK()			
			SET_STAGE(MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE)
			CPRINTLN(DEBUG_MISSION, "CHASE_TARGET_IN_VEHICLE - SS_CLEANUP done")
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Al Di Napoli has crashed and the player has got close to him.
///    Player instructed to park alongside An Di Napoli
PROC GET_CLOSE_TO_TARGET_FOR_CUTSCENE()	

	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)	// lower chance of peds interfering in route
	REQUEST_VEHICLE_RECORDING(1, sNigel2_CarRecNigelOutro)		// make sure the recording for Nigel driving off in the outro mocap is loaded ready
	MANAGE_OUTRO_MOCAP_LOADING()
	SET_TARGET_PLAY_HEAD_ON_WHEEL_ANIM()
	
 	SWITCH eSubStage	
		//	------------------------------------------
	 	CASE SS_SETUP
			MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
			iTimer_OutroMocapDelay = GET_GAME_TIMER()
			IF IS_PED_UNINJURED(sTargetPed.ped)		
				IF bDoneCleanup_JumpOutHospitalWindow	// B*1557320 - don't allow during scripted camera jump out the window
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetPed.ped)
				ENDIF
			ENDIF
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "GET_CLOSE_TO_TARGET_FOR_CUTSCENE - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE		
			IF IS_PED_UNINJURED(sTargetPed.ped)		
				IF bDoneCleanup_JumpOutHospitalWindow	// B*1557320 - don't allow during scripted camera jump out the window
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetPed.ped)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
					fCurrentChaseDistance = VDIST(vPlayerPos, GET_ENTITY_COORDS(sTargetVehicle.vehicle, FALSE))
				ENDIF
				
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
				AND IS_PED_UNINJURED(sNigelPed.ped)
				AND IS_PED_UNINJURED(sMrsThornhillPed.ped)
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
						IF IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_04")
							CLEAR_THIS_PRINT("NIGEL2_04")		// Get back in ~b~Nigel's car.~s~
						ENDIF	
						bDone_KillConvoForOutTheVehicleDialogue = FALSE	//reset bool used to stop dialogue when player gets out the car
						SAFE_REMOVE_BLIP(sNigelVehicle.blip)
						MANAGE_DIALOGUE_DURING_CHASE(bForceTargetblipFlashThisFrame)	// needs to come before UPDATE_CHASE_BLIP since it sets bForceTargetblipFlashThisFrame
						IF NOT DOES_BLIP_EXIST(sTargetPed.blip)
							sTargetPed.blip = CREATE_PED_BLIP(sTargetPed.ped, TRUE, FALSE)
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(sTargetPed.ped)
								SET_BLIP_SCALE(sTargetPed.blip, 1.0)	//TEMP: added as per bug 241312
							ENDIF
							// must come after MANAGE_DIALOGUE_DURING_CHASE since this controls bForceTargetblipFlashThisFrame override
							UPDATE_CHASE_BLIP(sTargetPed.blip, sTargetPed.ped, NIGEL2_TARGET_ESCAPED_DISTANCE, NIGEL2_FLASH_CHASE_BLIP_PERCENTAGE, bForceTargetblipFlashThisFrame)
						ENDIF						
						IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<381.915619,-628.101440,22.932293>>, <<394.477234,-606.126953,35.774826>>, 18.500000)
							IF IS_VEHICLE_SETTLED_FOR_CUTSCENE(sNigelVehicle.vehicle)
								// must be settle up min 500 ms
								IF HAS_TIME_PASSED(iTimer_OutroMocapDelay, 500)
									eSubStage = SS_CLEANUP
									CPRINTLN(DEBUG_MISSION, "GET_CLOSE_TO_TARGET_FOR_CUTSCENE - SS_UPDATE - in range and not in air to trigger cutscene")
								ENDIF
							ELSE
								iTimer_OutroMocapDelay = GET_GAME_TIMER()
							ENDIF
						ELSE
							iTimer_OutroMocapDelay = GET_GAME_TIMER()
						ENDIF
					ELSE
						SAFE_REMOVE_BLIP(sTargetPed.blip)
						IF NOT DOES_BLIP_EXIST(sNigelVehicle.blip)
							sNigelVehicle.blip = CREATE_VEHICLE_BLIP(sNigelVehicle.vehicle, TRUE, BLIPPRIORITY_HIGH)
						ENDIF						
						//kill off any dialogue which is based on the player being in the car
						IF NOT bDone_KillConvoForOutTheVehicleDialogue
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							bDone_KillConvoForOutTheVehicleDialogue = TRUE
						ENDIF						
						IF NOT bDoneObjective_GetBackInVehicle					
							PRINT_NOW("NIGEL2_04", DEFAULT_GOD_TEXT_TIME, 1)	//Get back into ~b~Nigel's car.~w~
							iTimer_TrevorGetBackInCarDialogue = GET_GAME_TIMER()
							bDoneObjective_GetBackInVehicle = TRUE
						ELSE							
							//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIGEL2_04")	//Get back into ~b~Nigel's car.~w~		//*commented out for new display message check with subtitle profile settings
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()									
									IF (fDistPlayerToNigelVehicle < 20.0)					//fDistPlayerToNigelVehicle is calculated in MISSION_FAILED_CHECKS()
										IF (GET_GAME_TIMER() - iTimer_TrevorGetBackInCarDialogue) > GET_RANDOM_INT_IN_RANGE(4500, 6500)
											IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)	//needed to test him in getting in car state
												//Do additional dialogue telling player to get back in the car
												// random select between Nigel and Mrs Thornhill comments
												IF (GET_RANDOM_INT_IN_RANGE(0, 11) > 5)
													// Nigel lines
													IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_GIC", CONV_PRIORITY_HIGH)
														// Jock, what are you doing?
														// Where are you going?
														// We've come this far, don't desert us now!   
														#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE - trevor leaving N & MRS T behind  - NIG2_GIC") ENDIF #ENDIF
													ENDIF
												ELSE
													// Mrs Thornhill lines
													IF NIG2_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG2AUD", "NIG2_GIC2", CONV_PRIORITY_HIGH)
														// Jock! Please! We need you!  
														// We'll never catch him on foot, not with Nigel's hip!  
														#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE -trevor leaving N & MRS T behind - NIG2_GIC2") ENDIF #ENDIF
													ENDIF
												ENDIF								
											ENDIF
										ENDIF
									ENDIF
								ELSE
									iTimer_TrevorGetBackInCarDialogue = GET_GAME_TIMER()
								ENDIF
							//ENDIF						
						ENDIF	
						HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()	// make buddy's look at the player as he enters the vehicle.
						iTimer_OutroMocapDelay = GET_GAME_TIMER()
					ENDIF
				ENDIF	
				NIG2_TRACK_STAT_PLAYER_KEEPS_CLOSE_TO_TARGET()
			ENDIF
			MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
			NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL()
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP		
			SAFE_REMOVE_BLIP(sNigelVehicle.blip)
			SAFE_REMOVE_BLIP(sTargetPed.blip)
			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			IF NOT bDoneCleanup_JumpOutHospitalWindow	
				SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
				REPLAY_STOP_EVENT()	// B*1841676 - cleanup the record jump out of the hospital window
				bDoneCleanup_JumpOutHospitalWindow = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_JUMP_OUT_HOSPITAL_WINDOW - bDoneCleanup_JumpOutHospitalWindow early for nigel's vehicle being wrecked") ENDIF #ENDIF
			ENDIF
			//hospital debris cleanup
			IF DOES_PARTICLE_FX_LOOPED_EXIST(HospitalDebris_PTFX_ID)
				STOP_PARTICLE_FX_LOOPED(HospitalDebris_PTFX_ID)
			ENDIF
			eHospitalDoorsObjectState = HDO_END
			SAFE_DELETE_OBJECT(sObjectHospitalDoors.objectIndex)
			REMOVE_PTFX_ASSET()
			IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
			AND bFinishedStageSkipping
				SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_STOP")
			ENDIF
			MANAGE_TRIGGER_MUSIC_EVENTS_DURING_CHASE(NIGEL2_MUSIC_EVENT_STOP)
			SET_STAGE(MISSION_STAGE_END_CUTSCENE_MOCAP)
			CPRINTLN(DEBUG_MISSION, "GET_CLOSE_TO_TARGET_FOR_CUTSCENE - SS_CLEANUP done")
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Trevor throws Al Di Napoli in the boot of Nigel's car
PROC STAGE_END_CUTSCENE_MOCAP()
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)	// lower chance of peds interfering in route
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			// don't request the mocap if we are skipping past this stage
			IF NOT bFinishedStageSkipping
				IF NOT IS_REPLAY_BEING_SET_UP()
					//SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PLAYER_PED_ID())			
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)			
				ENDIF
				CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP setup done for skip, heading to SS_CLEANUP = Frame Count :", GET_FRAME_COUNT())
				eSubStage = SS_CLEANUP
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)	// B*1521637 - dive off car for cutscene
				RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
				REQUEST_VEHICLE_RECORDING(1, sNigel2_CarRecNigelOutro)		// make sure the recording for Nigel driving off in the outro mocap is loaded ready
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP ; RC_REQUEST_MID_MISSION_CUTSCENE : NMT_2_MCS_2 - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
				
				IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
					SET_ENTITY_PROOFS(sTargetVehicle.vehicle, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)	// B*1954082 - stop window smash during transition to cutscene
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP ; setting target veh collision proof this frame : ", GET_FRAME_COUNT())
				ENDIF
						
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, sNigel2_CarRecNigelOutro)
				AND NOT SAFE_IS_PED_PLAYING_AMBIENT_SPEECH(sMrsThornhillPed.ped)	// don't cut of Nig or Mrs T ambient comments
				AND NOT SAFE_IS_PED_PLAYING_AMBIENT_SPEECH(sNigelPed.ped)		
					IF RC_IS_CUTSCENE_OK_TO_START(TRUE)				
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Trevor, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						IF IS_ENTITY_ALIVE(sNigelPed.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(sNigelPed.ped, sSceneHandle_Nigel, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF IS_ENTITY_ALIVE(sMrsThornhillPed.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(sMrsThornhillPed.ped, sSceneHandle_MrsThornhill, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF IS_ENTITY_ALIVE(sTargetPed.ped)
							REGISTER_ENTITY_FOR_CUTSCENE(sTargetPed.ped, sSceneHandle_AlDiNapoli, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF IS_ENTITY_ALIVE(sNigelVehicle.vehicle)
							REGISTER_ENTITY_FOR_CUTSCENE(sNigelVehicle.vehicle, sSceneHandle_NigelVehicle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
							REGISTER_ENTITY_FOR_CUTSCENE(sTargetVehicle.vehicle, sSceneHandle_AlDiNapoliVehicle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							// B*1954082 - stop sparks from vehicle on transition to cutscene
							REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(sTargetVehicle.vehicle, FALSE), 4.0)
						ENDIF
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "REGISTER_ENTITY_FOR_CUTSCENE done Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF

						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW) 

						START_CUTSCENE()				
						WAIT(0)	//needed because cutscene doesn't start straight away, causing player to see vehicles getting removed.
						
						SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// //B*1481353 - when skipping to mocap (let the mocap stage handle fade in) tried just before START_CUTSCENE but issue still occured
						
						CLEAR_ANGLED_AREA_OF_VEHICLES(<<418.283752,-571.662903,24.697943>>, <<362.025787,-663.139771,38.339985>>, 28.000000, FALSE, FALSE, FALSE, TRUE, TRUE)	// B*1564102 make sure no vehicles are in the area where the nodes are turned off
						RC_START_CUTSCENE_MODE(<< 392.86, -621.04, 27.97 >>, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, FALSE)
						SETUP_CRASH_SITE_FOR_OUTRO_MOCAP(TRUE)
						
						// B*1322009 - limit density for framerate in the scene
						SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	// prevent ambient cars interfering with the scene
						SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
					
						eSubStage = SS_UPDATE
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SS_SETUP	 - Frame Count : ", GET_FRAME_COUNT())
					ENDIF
				ELSE
					IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(sNigelVehicle.vehicle, DEFAULT_VEH_STOPPING_DISTANCE, 1, 0.0)
					ENDIF
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SS_SETUP waiting on veh rec load and ambient speech	 - Frame Count : ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
		
			// setup Trevor's exit state
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Trevor)
				REPLAY_STOP_EVENT()
				IF WAS_CUTSCENE_SKIPPED()
				AND IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "SS_UPDATE - Camera - set skipped mocap Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				
				bHasSetExitStateFromOutroMocapForGameplayCam = TRUE	// stop setting the gameplay cam now.
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Trevor - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			
			// handle exit state for Nigel, Mrs Thornhill and Nigel's vehicle
			IF IS_PED_UNINJURED(sNigelPed.ped)
			AND IS_PED_UNINJURED(sMrsThornhillPed.ped)
			AND IS_VEHICLE_OK(sNigelVehicle.vehicle)	
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Nigel)
					IF NOT IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
						SET_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_DRIVER)							
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " set exit state for Nigel put in his vehicle ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					//TASK_VEHICLE_DRIVE_WANDER(sNigelPed.ped, sNigelVehicle.vehicle, 60.0, DRIVINGMODE_AVOIDCARS)					
					//SET_DRIVE_TASK_CRUISE_SPEED(sNigelPed.ped, 60.0)		
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sNigelPed.ped, TRUE)
					//SET_PED_KEEP_TASK(sNigelPed.ped, TRUE)
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Nigel - Frame Count : ", GET_FRAME_COUNT())
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_MrsThornhill)
					IF NOT IS_PED_SITTING_IN_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle)
						SET_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " set exit state for Mrs T put in his vehicle ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMrsThornhillPed.ped, TRUE)
					SET_PED_KEEP_TASK(sMrsThornhillPed.ped, TRUE)
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Mrs T - Frame Count : ", GET_FRAME_COUNT())
				ENDIF		
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_NigelVehicle)	
					SET_VEHICLE_ENGINE_ON(sNigelVehicle.vehicle, TRUE, TRUE)
					SET_VEHICLE_FORWARD_SPEED(sNigelVehicle.vehicle, 17)	
					SET_VEHICLE_DOORS_SHUT(sNigelVehicle.vehicle, TRUE)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sNigelVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(sNigelVehicle.vehicle, 1, sNigel2_CarRecNigelOutro, 35, DRIVINGMODE_AVOIDCARS_RECKLESS)
						//SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Nigel - set veh rec going for outro blend - Frame Count : ", GET_FRAME_COUNT())
					ENDIF					
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Nigel's vehicle - Frame Count : ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
			
			// handle Al Di Napoli's exit state
			IF IS_PED_UNINJURED(sTargetPed.ped)
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_AlDiNapoli)
					SET_ENTITY_LOAD_COLLISION_FLAG(sTargetPed.ped, FALSE)	//unsure if this is needed, might be costly?
					SAFE_REMOVE_PED(sTargetPed.ped, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Al Di Napoli - deleted - Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			
			// unfortunately the only way to ensure the mocap camera blends out to gameplay cam at a sensible position is to spam this until the player's character exit state has fired
			IF NOT bHasSetExitStateFromOutroMocapForGameplayCam
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()	
				eSubStage = SS_CLEANUP
				CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " SS_UPDATE done mocap ended Frame Count : ", GET_FRAME_COUNT())
			ELSE
				// B*1322009 - limit density for framerate in the scene
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	// prevent ambient cars interfering with the scene
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ELSE
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ENDIF				
				IF IS_ENTITY_ALIVE(sTargetPed.ped)
					SET_ENTITY_LOAD_COLLISION_FLAG(sTargetPed.ped, FALSE)	//unsure if this is needed, might be costly?
				ENDIF
				SAFE_REMOVE_PED(sTargetPed.ped, TRUE)
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)					
					IF IS_PED_UNINJURED(sNigelPed.ped)
						IF NOT IsPedPerformingTask(sNigelPed.ped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
							IF IS_PED_SITTING_IN_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sNigelPed.ped, TRUE)
								//SET_PED_KEEP_TASK(sNigelPed.ped, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "SS_CLEANUP set Nigel's drive task Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sMrsThornhillPed.ped, TRUE)
						SET_PED_KEEP_TASK(sMrsThornhillPed.ped, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", "SS_CLEANUP set Mrs T task Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_ALIVE(sTargetVehicle.vehicle)
					SET_ENTITY_PROOFS(sTargetVehicle.vehicle, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)	// B*1954082 - stop window smash during transition to cutscene
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP ; turning off target veh collision proof this frame : ", GET_FRAME_COUNT())
				ENDIF
			
				/*// setup emergency services to appear at the crash scene after the mission ends
				IF DOES_ENTITY_EXIST(sTargetVehicle.vehicle)
					CREATE_INCIDENT_WITH_ENTITY(DT_AMBULANCE_DEPARTMENT, sTargetVehicle.vehicle, 2.0, 3.0, tempIncidentIndex)
				ENDIF*/
				
				IF bFinishedStageSkipping	//only do this if we aren't stage skipping
					RC_END_CUTSCENE_MODE()					
				ENDIF
				iTimer_MissionPassedDelay = GET_GAME_TIMER()		// delay for Nigel switching to wander task on mission passed 
	
				SET_STAGE(MISSION_STAGE_MISSION_PASSED)
				CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SS_CLEANUP done Frame Count : ", GET_FRAME_COUNT())
			ENDIF
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Mission has already failed at this point, but this handles
///    having to update the fail reason if the player does something more
///    serious during the fade.
PROC MISSION_FAILED_WAIT_FOR_FADE()	

	STRING sFailReason = NULL	
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL2_MISSION_FAIL")
				CLEAR_PRINTS()
				CLEAR_HELP()			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF			
				DELETE_ALL_MISSION_BLIPS()			
				SEQUENCE_INDEX seqIndex		
				//implemented to ensure Nigel and Mrs Thornhill get out the car on mission failed,	
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())					
						BRING_VEHICLE_TO_HALT(sNigelVehicle.vehicle, 6.0, 2)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "player vehicle halted IN FAILED SETUP	$$$") ENDIF #ENDIF
					ENDIF
				ENDIF				
				IF IS_PED_UNINJURED(sNigelPed.ped)
					IF IS_PED_IN_GROUP(sNigelPed.ped)
						REMOVE_PED_FROM_GROUP(sNigelPed.ped)
					ENDIF
					CLEAR_PED_TASKS(sNigelPed.ped)		//cant do CLEAR_PED_TASKS_IMMEDIATELY because it teleports them out the car				
					IF eN2_MissionFailedReason = FAILED_MRS_THORNHILL_DIED
					OR eN2_MissionFailedReason = FAILED_MRS_THORNHILL_ATTACKED
					OR eN2_MissionFailedReason = FAILED_NIGEL_ATTACKED
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET NIGEL UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
						SET_PED_FLEE_ATTRIBUTES(sNigelPed.ped, FA_USE_VEHICLE, FALSE)					
						OPEN_SEQUENCE_TASK(seqIndex)
							IF IS_PED_IN_ANY_VEHICLE(sNigelPed.ped)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)								
							ENDIF
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300.0, -1)
						CLOSE_SEQUENCE_TASK(seqIndex)
						
						TASK_PERFORM_SEQUENCE(sNigelPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
						SET_PED_KEEP_TASK(sNigelPed.ped, TRUE)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
					IF IS_PED_IN_GROUP(sMrsThornhillPed.ped)
						REMOVE_PED_FROM_GROUP(sMrsThornhillPed.ped)
					ENDIF
					CLEAR_PED_TASKS(sMrsThornhillPed.ped)		//cant do CLEAR_PED_TASKS_IMMEDIATELY because it teleports them out the car
					IF eN2_MissionFailedReason = FAILED_NIGEL_DIED
					OR eN2_MissionFailedReason = FAILED_NIGEL_ATTACKED
					OR eN2_MissionFailedReason = FAILED_MRS_THORNHILL_ATTACKED
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET MRS THORNHILL UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
						SET_PED_FLEE_ATTRIBUTES(sMrsThornhillPed.ped, FA_USE_VEHICLE, FALSE)					
						IF IS_PED_IN_ANY_VEHICLE(sMrsThornhillPed.ped)
							OPEN_SEQUENCE_TASK(seqIndex)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)								
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300.0, -1)
							CLOSE_SEQUENCE_TASK(seqIndex)
							TASK_PERFORM_SEQUENCE(sMrsThornhillPed.ped, seqIndex)
							CLEAR_SEQUENCE_TASK(seqIndex)
							SET_PED_KEEP_TASK(sMrsThornhillPed.ped, TRUE)
						ELSE
							TASK_SMART_FLEE_PED(sMrsThornhillPed.ped, PLAYER_PED_ID(), 300.0, -1)
							SET_PED_KEEP_TASK(sMrsThornhillPed.ped, TRUE)
						ENDIF
					ENDIF
				ENDIF			
				//Stop Di Napoli's car recording AND uber recording if not already done so in script
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIGEL 2 : ", "target vehicle recording stopped in failed mission stage") ENDIF #ENDIF
					ENDIF
					IF NOT bDone_UberRecordingCleanupForChaseEnd
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "NIGEL 2 : ", "uber recording cleaned up in failed mission stage") ENDIF #ENDIF
						CLEANUP_UBER_PLAYBACK()
					ENDIF
				ENDIF						
				//setup Al Di Napoli behaviour on mission failed
				IF IS_PED_UNINJURED(sTargetPed.ped)
					IF bDone_UberRecordingCleanupForChaseEnd
						SET_PED_KEEP_TASK(sTargetPed.ped, TRUE)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET TARGET UP TO KEEP TASKS AND HE IS CRASHED IN FAILED SETUP	$$$") ENDIF #ENDIF
					ELSE					
						CLEAR_PED_TASKS(sTargetPed.ped)		//cant do CLEAR_PED_TASKS_IMMEDIATELY because it teleports them out the car
						SET_PED_FLEE_ATTRIBUTES(sTargetPed.ped, FA_USE_VEHICLE, TRUE)
						SET_PED_FLEE_ATTRIBUTES(sTargetPed.ped, FA_PREFER_PAVEMENTS, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sTargetPed.ped, FA_USE_COVER, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sTargetPed.ped, FA_LOOK_FOR_CROWDS, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sTargetPed.ped, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
						
						IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
						AND IS_PED_IN_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
						AND IS_VEHICLE_OK(sNigelVehicle.vehicle)
							OPEN_SEQUENCE_TASK(seqIndex)								
								TASK_VEHICLE_MISSION_PED_TARGET(NULL, sTargetVehicle.vehicle, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 500.0, -1.0)
							CLOSE_SEQUENCE_TASK(seqIndex)
							TASK_PERFORM_SEQUENCE(sTargetPed.ped, seqIndex)
							CLEAR_SEQUENCE_TASK(seqIndex)
						ELSE
							OPEN_SEQUENCE_TASK(seqIndex)								
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300.0, -1)
							CLOSE_SEQUENCE_TASK(seqIndex)
							TASK_PERFORM_SEQUENCE(sTargetPed.ped, seqIndex)
							CLEAR_SEQUENCE_TASK(seqIndex)
						ENDIF
						SET_PED_KEEP_TASK(sTargetPed.ped, TRUE)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET TARGET UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
					ENDIF
				ENDIF			
				// Set the fail reason
				SWITCH eN2_MissionFailedReason
					CASE FAILED_TARGET_DIED		
						sFailReason = "NIGEL2_F1" 	//~r~Al Di Napoli died.~w~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: AL DI NAPOLI DIED") ENDIF #ENDIF
					BREAK
					CASE FAILED_TARGET_ESCAPED	
						sFailReason = "NIGEL2_F2"	//~r~Al Di Napoli escaped.~w~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: AL DI NAPOLI ESCAPED") ENDIF #ENDIF
					BREAK
					CASE FAILED_NIGEL_VEHICLE_WRECKED		
						sFailReason = "NIGEL2_F3"	//~r~Nigel's car was wrecked.~w~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: NIGEL'S CAR WRECKED") ENDIF #ENDIF
					BREAK
					CASE FAILED_NIGEL_ATTACKED		
						sFailReason = "NIGEL2_F7"	//~w~Nigel was injured.~s~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: NIGEL ATTACKED") ENDIF #ENDIF
					BREAK
					CASE FAILED_NIGEL_DIED		
						sFailReason = "NIGEL2_F4"	//~r~Nigel died.~w~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: NIGEL DIED") ENDIF #ENDIF
					BREAK	
					CASE FAILED_MRS_THORNHILL_ATTACKED		
						sFailReason = "NIGEL2_F8"	//~w~Mrs. Thornhill was injured.~s~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: MRS THORNHILL ATTACKED") ENDIF #ENDIF
					BREAK
					CASE FAILED_MRS_THORNHILL_DIED		
						sFailReason = "NIGEL2_F5"	//~r~Mrs. Thornhill died.~w~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: MRS THORNHILL DIED") ENDIF #ENDIF
					BREAK		
					CASE FAILED_LEFT_NIGEL_AND_MRS_T_BEHIND
						sFailReason = "NIGEL2_F6"	//~w~Nigel and Mrs. Thornhill were abandoned.~s~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Nigel and Mrs Thorton were abandoned.") ENDIF #ENDIF
					BREAK
					CASE FAILED_LEFT_NIGEL_BEHIND
						sFailReason = "NIGEL2_F10"	//~w~Nigel was abandoned.~s~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Nigel was left abandoned.") ENDIF #ENDIF
					BREAK
					CASE FAILED_LEFT_MRS_T_BEHIND
						sFailReason = "NIGEL2_F9"	//~w~Mrs. Thornhill was abandoned.~s~
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Mrs Thorton was abandoned.") ENDIF #ENDIF
					BREAK
					DEFAULT
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: generic") ENDIF #ENDIF
					BREAK
				ENDSWITCH	
				//Check if fail reason needs to be displayed
				IF eN2_MissionFailedReason = FAILED_DEFAULT 		
					Random_Character_Failed()
				ELSE
					Random_Character_Failed_With_Reason(sFailReason)
				ENDIF
				eSubStage = SS_UPDATE
				CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP done")
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				//---- Do any specific cleanup here----			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)			
				// get the player out of the hospital
				IF IS_PED_INSIDE_HOSPITAL_INTERIOR(PLAYER_PED_ID())	// required true check to use GET_KEY_FOR_ENTITY_IN_ROOM check to work, and need ot use this to prevent inaccurate results
					CPRINTLN(DEBUG_MISSION, "Player in hosp")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - player in hospital interior, warp setup") ENDIF #ENDIF
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<279.4137, -585.8815, 42.3102>>, 48.8028)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<276.27, -584.00, 42.73>>, 337.40)
				ENDIF			
				CLEANUP_ALL_MISSION_ENTITIES(TRUE)
				Script_Cleanup()
				CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - SS_CLEANUP done")		
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK		

	ENDSWITCH
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	FUNC BOOL IS_CAR_PAUSED_AT_START_OF_RECORDING(VEHICLE_INDEX inCar, INT iRec)

		FLOAT fTemp

		IF IS_VEHicle_DRIVEABLE(inCar)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_vehicle(inCar)
				START_PLAYBACK_RECORDED_vehicle(inCar, iRec, sNigel2_UberRecordingName)	
			ELSE
				fTemp = get_TIME_POSITION_IN_RECORDING(inCar)
				SKIP_TIME_IN_PLAYBACK_RECORDED_vehicle(inCar, (0.0 - fTemp))
				PAUSE_PLAYBACK_RECORDED_vehicle(inCar)
				RETURN(TRUE)
			ENDIF
		ENDIF

		RETURN(FALSE)

	ENDFUNC

	PROC RECORD_CHASE_ROUTE()	
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		INT iRecNumTarget, iRecNumGhost
		iRecNumTarget = 1	//999
		iRecNumGhost = 888
		VECTOR vRecordingPosition
		VECTOR vRecordingStartPos
		FLOAT fRecordedTime
		FLOAT fGhostCarPlaybackTime
		FLOAT fTimeDiff
		
	 	SWITCH eSubStage			
			//	------------------------------------------
	 		CASE SS_SETUP
				SET_CLOCK_TIME(10, 0, 0)			
				LOAD_MISSION_ASSETS(TRUE)
				SETUP_FOR_START_OF_MISSION()
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)					
					 //for uber recording
					SET_VEHICLE_DOORS_LOCKED(sTargetVehicle.vehicle, VEHICLELOCK_UNLOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sTargetVehicle.vehicle, TRUE)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sTargetVehicle.vehicle)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_VEHICLE_ENGINE_ON(sTargetVehicle.vehicle, TRUE, TRUE)
					//SET_ENTITY_COORDS(sTargetVehicle.vehicle, << -1290.7233, -634.5989, 26.1005 >>)
					//SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0248, 0.0004, 0.8949, 0.4456)					

					REQUEST_VEHICLE_RECORDING(iRecNumGhost, sNigel2_UberRecordingName)
					
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNumGhost, sNigel2_UberRecordingName)
						CPRINTLN(DEBUG_MISSION, "LOADING THE VEHICLE RECORDING 1 Nigel2U")
						WAIT(0)
					ENDWHILE
					
					REQUEST_VEHICLE_RECORDING(iRecNumTarget, sNigel2_UberRecordingName)
					
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNumTarget, sNigel2_UberRecordingName)
						CPRINTLN(DEBUG_MISSION, "LOADING THE VEHICLE RECORDING 1 Nigel2U")
						WAIT(0)
					ENDWHILE
					
					IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					AND IS_VEHICLE_OK(vehDebug_Ghost)
						SET_ENTITY_COLLISION(vehDebug_Ghost, FALSE)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
						FREEZE_ENTITY_POSITION(vehDebug_Ghost, FALSE)
						vRecordingStartPos = GET_POSITION_OF_vehicle_RECORDING_AT_TIME(iRecNumTarget, 0.0, sNigel2_UberRecordingName)
						SET_ENTITY_COORDS(sTargetVehicle.vehicle, vRecordingStartPos)
						vRecordingStartPos = GET_POSITION_OF_vehicle_RECORDING_AT_TIME(iRecNumGhost, 0.0, sNigel2_UberRecordingName)
						SET_ENTITY_COORDS(vehDebug_Ghost, vRecordingStartPos)
						
						//FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)
						//FREEZE_ENTITY_POSITION(vehDebug_Ghost, TRUE)
					ENDIF
					
					
					WHILE NOT IS_car_PAUSED_AT_START_OF_RECORDING(sTargetVehicle.vehicle, iRecNumTarget)
					OR NOT IS_car_PAUSED_AT_START_OF_RECORDING(vehDebug_Ghost, iRecNumGhost)
						CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_SETUP pausing cars at start of recording...")
						WAIT(0)
					ENDWHILE
									
					//INIT_UBER_RECORDING(sNigel2_UberRecordingName)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					eSubStage = SS_UPDATE
					CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_SETUP done")
				ENDIF				
			BREAK			
			//	------------------------------------------
			CASE SS_UPDATE			
				// for uber recording
				//UPDATE_UBER_RECORDING()					
				
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
				AND IS_VEHICLE_OK(vehDebug_Ghost)				
					
					IF IS_RECORDING_GOING_ON_FOR_vehicle(sTargetVehicle.vehicle)
						fRecordedTime = GET_TIME_POSITION_IN_RECORDED_RECORDING(sTargetVehicle.vehicle)
					ENDIF
					
					fMainPlaybackSpeed = 1.0
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebug_Ghost)
						fGhostCarPlaybackTime = GET_TIME_POSITION_IN_RECORDING(vehDebug_Ghost)
						DISPLAY_PLAYBACK_RECORDED_vehicle(vehDebug_Ghost, RDM_WHOLELINE)
						SET_PLAYBACK_SPEED(vehDebug_Ghost, fMainPlaybackSpeed)
					endif
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)
						SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)
					endif
					
					vRecordingPosition = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecNumTarget, fCurrentPlaybackTime, sNigel2_UberRecordingName)
		
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						/*
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebug_Ghost)
						
							FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
							FREEZE_ENTITY_POSITION(vehDebug_Ghost, FALSE)
							START_PLAYBACK_RECORDED_vehicle(vehDebug_Ghost, iRecNumGhost, sNigel2_UberRecordingName)	
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehDebug_Ghost)
							SET_PLAYBACK_SPEED(vehDebug_Ghost, fMainPlaybackSpeed)
							START_PLAYBACK_RECORDED_vehicle(sTargetVehicle.vehicle, iRecNumTarget, sNigel2_UberRecordingName)	
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sTargetVehicle.vehicle)
							SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)
							CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : PLAYBACK OF RECORDING STARTED FC = ", GET_FRAME_COUNT())		
						ENDIF*/
						//START_RECORDING_VEHICLE(sTargetVehicle.vehicle, 999, sNigel2_UberRecordingName, TRUE)
						//CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : PLAYBACK OF RECORDING STARTED FC = ", GET_FRAME_COUNT())	
						
						//WAIT(0)
						
						IF IS_PLAYBACK_GOING_ON_FOR_vehicle(vehDebug_Ghost)							
							UNPAUSE_PLAYBACK_RECORDED_vehicle(vehDebug_Ghost)	
							CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : unpaused ghost car fGhostCarPlaybackTime = ", fGhostCarPlaybackTime, "FC = ", GET_FRAME_COUNT(), " game timer = ", GET_GAME_TIMER())	
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_vehicle(sTargetVehicle.vehicle)
							UNPAUSE_PLAYBACK_RECORDED_vehicle(sTargetVehicle.vehicle)	
							CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : unpaused target car fCurrentPlaybackTime = ", fCurrentPlaybackTime, " FC = ", GET_FRAME_COUNT())
						ENDIF
															
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						
						// RECORD SECTIONS
						
						/*IF fCurrentPlaybackTime > 10000.160
						AND fCurrentPlaybackTime < 11000.490
							IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
								SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(sTargetVehicle.vehicle, (iDebug_AdditionalAIDriveTime + ROUND(11000.490 - fCurrentPlaybackTime)), DRIVINGMODE_PLOUGHTHROUGH)
								CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER this fCurrentPlaybackTime = ", fCurrentPlaybackTime)
							ENDIF
						ENDIF*/						
						IF fCurrentPlaybackTime > 41000.0 // 41410.0
						AND fCurrentPlaybackTime < 43500.00	// 43250.960
							IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
								//IF NOT IS_RECORDING_GOING_ON_FOR_vehicle(sTargetVehicle.vehicle)	
								//	START_RECORDING_VEHICLE_TRANSITION_FROM_PLAYBACK(sTargetVehicle.vehicle, 985, sNigel2_UberRecordingName, TRUE)
								//	CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : START_RECORDING_VEHICLE_TRANSITION_FROM_PLAYBACK STARTED FC = ", GET_FRAME_COUNT(), " game timer = ", GET_GAME_TIMER())	
									
								//	WAIT(0)
								//	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)	
								//		START_PLAYBACK_RECORDED_vehicle(sTargetVehicle.vehicle, iRecNumTarget, sNigel2_UberRecordingName)
								//		CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : START_PLAYBACK_RECORDED_vehicle STARTED FC = ", GET_FRAME_COUNT(), " game timer = ", GET_GAME_TIMER())	
										SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(sTargetVehicle.vehicle, (iDebug_AdditionalAIDriveTime + 175 +(ROUND(43500.00 - fCurrentPlaybackTime))), DRIVINGMODE_PLOUGHTHROUGH)
										CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER this fCurrentPlaybackTime = ", fCurrentPlaybackTime, " game timer = ", GET_GAME_TIMER())
								//	ENDIF									
								//ENDIF								
							ENDIF
						ENDIF
						IF fCurrentPlaybackTime > 77000.0
						AND fCurrentPlaybackTime < 81263.80
							IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
								SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(sTargetVehicle.vehicle, (iDebug_AdditionalAIDriveTime +(ROUND(81263.80 - fCurrentPlaybackTime))), DRIVINGMODE_PLOUGHTHROUGH)
								CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER this fCurrentPlaybackTime = ", fCurrentPlaybackTime)
							ENDIF
						ENDIF
						
						fTimeDiff = fCurrentPlaybackTime
						fTimeDiff -= fGhostCarPlaybackTime
						IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE this fCurrentPlaybackTime = ", fCurrentPlaybackTime,
																" fGhostCarPlaybackTime = ", fGhostCarPlaybackTime, " fTimeDiff = ", fTimeDiff, " fRecordedTime = ", fRecordedTime)
						ENDIF
					ENDIF
					fTimeDiff = fCurrentPlaybackTime
					fTimeDiff -= fGhostCarPlaybackTime
					CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : STANDARD CHECKS : fCurrentPlaybackTime = ", fCurrentPlaybackTime,
								" fGhostCarPlaybackTime = ", fGhostCarPlaybackTime, " fTimeDiff = ", fTimeDiff, " fRecordedTime = ", fRecordedTime, " vRecordingPosition = ", vRecordingPosition)
				ENDIF
				// drop out
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
					eSubStage = SS_CLEANUP
				ENDIF
			BREAK			
			//	------------------------------------------
			CASE SS_CLEANUP
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_RECORDING_GOING_ON_FOR_vehicle(sTargetVehicle.vehicle)						
						STOP_RECORDING_VEHICLE(sTargetVehicle.vehicle)
						CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_UPDATE : STOP_RECORDING_VEHICLE")
					ENDIF
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF
				ENDIF
				IF IS_VEHICLE_OK(vehDebug_Ghost)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDebug_Ghost)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehDebug_Ghost)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_MISSION, "DEBUG : RECORD_CHASE_ROUTE : SS_CLEANUP")
				eSubStage = SS_SETUP
			BREAK
		ENDSWITCH	
	ENDPROC
	
	PROC PLAYBACK_CHASE_ROUTE()	
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		eSubStage = SS_CLEANUP
		SWITCH eSubStage			
			//	------------------------------------------
	 		CASE SS_SETUP
				LOAD_MISSION_ASSETS(TRUE)				
				SETUP_FOR_START_OF_MISSION()				
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)	
					IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()					
					IF IS_PED_UNINJURED(sTargetPed.ped)
					AND IS_VEHICLE_OK(sTargetVehicle.vehicle)	
						IF NOT IS_PED_IN_ANY_VEHICLE(sTargetPed.ped)
							CLEAR_PED_TASKS(sTargetPed.ped)
							SAFE_PUT_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TARGET WARPED INTO VEHICLE USING SAFETY NET") ENDIF #ENDIF
						ENDIF
						IF NOT DOES_BLIP_EXIST(sTargetPed.blip)
							sTargetPed.blip = CREATE_PED_BLIP(sTargetPed.ped, TRUE, FALSE)
							SET_BLIP_SCALE(sTargetPed.blip, 1.0)	//TEMP: added as per bug 241312
						ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTargetPed.ped, TRUE)	
					ENDIF					
					//for uber playback
					INITIALISE_UBER_PLAYBACK(sNigel2_UberRecordingName, iMainCarRecID)
					LOAD_UBER_DATA()
					CREATE_ALL_WAITING_UBER_CARS()
					switch_SetPieceCar_to_ai_on_collision = true
					allow_veh_to_stop_on_any_veh_impact = TRUE
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
					//fPlaybackCarStreamingDistance = 160.0	// 120.0 // is default
					traffic_block_vehicle_colour(true, traffic_black)	
					//deactivate_SetPieceCars_up_to_time(77000)
					IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
							//DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE)
							//PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE SETUP DONE") ENDIF #ENDIF
					/*
					//for normal play
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sTargetVehicle.vehicle, VS_FRONT_RIGHT)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_VEHICLE_ENGINE_ON(sTargetVehicle.vehicle, TRUE, TRUE)
					SET_ENTITY_COORDS(sTargetVehicle.vehicle, << -1363.1553, -596.5627, 28.6889 >>)  //601 = << -1355.8376, -590.6566, 28.6889 >>
					SET_ENTITY_QUATERNION(sTargetVehicle.vehicle, -0.0197, -0.0202, 0.9972, -0.0693)	//601 = 0.0160, 0.0349, 0.4617, 0.8862	
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF
					*/					
					/*
					//debug crash setup
					IF IS_PED_UNINJURED(sTargetPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
						SET_ENTITY_COORDS(sTargetVehicle.vehicle, << 394.8243, -621.0986, 27.9387 >>)
						SET_ENTITY_HEADING(sTargetVehicle.vehicle, 235.1330)
					ENDIF
					IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
						SET_ENTITY_COORDS(sNigelVehicle.vehicle, << 333.4894, -580.4035, 42.3070 >>)  
						SET_ENTITY_HEADING(sNigelVehicle.vehicle, 251.8706)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_VEHICLE_ENGINE_ON(sTargetVehicle.vehicle, TRUE, TRUE)
					ENDIF
					*/
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					eSubStage = SS_UPDATE
				ENDIF
			BREAK			
			//	------------------------------------------
			CASE SS_UPDATE				
				//for uber playback
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						fCurrentChaseDistance = VDIST(vPlayerPos, GET_ENTITY_COORDS(sTargetVehicle.vehicle, FALSE))
						fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)						
						//CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sTargetVehicle.vehicle, PLAYER_PED_ID(), fMainPlaybackSpeed, 1, 5, 20, 100, 2, 1)
						//UPDATE_UBER_CHASE_SPEED(fMainPlaybackSpeed, fCurrentPlaybackTime)
						fMainPlaybackSpeed = 1.0
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?						
						SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)						
						PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()	
						UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING()
						CREATE_ALL_WAITING_UBER_CARS()  //might help with some traffic failing to spawn?						
						UPDATE_UBER_PLAYBACK(sTargetVehicle.vehicle, fMainPlaybackSpeed)								
						CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetVehicle.vehicle)
						/*
						IF fCurrentPlaybackTime >= 113836.0	//target just about to enter hospital
							eSubStage = SS_CLEANUP
						ENDIF
						*/					
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CURRENT PLAYBACK TIME = ", fCurrentPlaybackTime) ENDIF #ENDIF						
						//UPDATE_UBER_CHASE_SPEED(fMainPlaybackSpeed, fCurrentPlaybackTime)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "fMainPlaybackSpeed = ", fMainPlaybackSpeed) ENDIF #ENDIF
					ENDIF
				ENDIF				
				/*	//for normal playback
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					//for normal play
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK OF RECORDING UNPAUSED") ENDIF #ENDIF
						ENDIF
					ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK OF RECORDING PAUSED") ENDIF #ENDIF
						ENDIF
					ENDIF
					MANAGE_PEDESTRIANS_ON_CHASE_ROUTE()
				ENDIF
				*/				
			BREAK		
			//	------------------------------------------
			CASE SS_CLEANUP
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE CLEANUP") ENDIF #ENDIF
				//for uber playback
				//CLEANUP_UBER_PLAYBACK()
			BREAK			
		ENDSWITCH	
	ENDPROC
	
	PROC DEBUG_STATE()	
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SWITCH eSubStage		
			//	------------------------------------------
	 		CASE SS_SETUP
				LOAD_MISSION_ASSETS(TRUE)				
				SETUP_FOR_START_OF_MISSION()				
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					IF IS_PED_UNINJURED(sNigelPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
					ENDIF
					IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_BACK_RIGHT)
					ENDIF
				ENDIF				
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_PED_UNINJURED(sTargetPed.ped)					
						//SAFE_PUT_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
					ENDIF					
					//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sTargetVehicle.vehicle)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)	// sets the window to destroyed so player/target can drive through it
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)
				//SAFE_TELEPORT_VEHICLE(sTargetVehicle.vehicle, << 285.5170, -571.1989, 42.1589 >>, 221.0)
				/*
				//hospital debris trials
				REQUEST_PTFX_ASSET()
				
				WHILE NOT HAS_PTFX_ASSET_LOADED()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUESTING SCRIPT PTFX") ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
				*/
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 285.5170, -571.1989, 42.1589 >>)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), 221.0036)
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
				//outside hospital
				SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, << 337.2605, -581.4758, 42.3258 >>, 254.3901)				
				SET_DEBUG_ACTIVE(TRUE)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)			
				/*
				REQUEST_VEHICLE_RECORDING(9, "Nigel2R")
				WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(9, "Nigel2R")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOADING THE NEW CAR REC") ENDIF #ENDIF
					WAIT(0)
				ENDWHILE				
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, 9, "Nigel2R")
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE)
						//PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF
				ENDIF
				*/				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)					
				eSubStage = SS_UPDATE
			BREAK			
			//	------------------------------------------
			CASE SS_UPDATE	
				MANAGE_HOSPITAL_STATE_DURING_CHASE()
			/*
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
					SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_CINEMATIC)
				ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
					SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_FIRST_PERSON)
				ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
					SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
				ENDIF
			*/		
			/*
				//for playback
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sTargetVehicle.vehicle, PLAYER_PED_ID(), fMainPlaybackSpeed, 1, 8, 25, 80, 1.4, 1)
						SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)
					ENDIF
				ENDIF
				*/
			BREAK	
			//	------------------------------------------
			CASE SS_CLEANUP
			BREAK
		ENDSWITCH
	ENDPROC
	
	//for testing hospital section!
	PROC DEBUG_HOSPITAL_STATE()
		SWITCH eSubStage		
			//	------------------------------------------
	 		CASE SS_SETUP
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bosh") ENDIF #ENDIF
				LOAD_MISSION_ASSETS(TRUE)				
				SETUP_FOR_START_OF_MISSION()				
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					IF IS_PED_UNINJURED(sNigelPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sNigelPed.ped, sNigelVehicle.vehicle, VS_FRONT_RIGHT)
					ENDIF
					IF IS_PED_UNINJURED(sMrsThornhillPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sMrsThornhillPed.ped, sNigelVehicle.vehicle, VS_BACK_RIGHT)
					ENDIF
					SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, << -413.1205, -503.5363, 24.3236 >>, 258.4841) // << -469.2073, -836.5956, 29.5250 >>, 315.1508)
					SET_VEHICLE_ENGINE_ON(sNigelVehicle.vehicle, TRUE, TRUE)
				ENDIF
				IF IS_PED_UNINJURED(sTargetPed.ped)
				AND IS_VEHICLE_OK(sTargetVehicle.vehicle)	
					IF NOT IS_PED_SITTING_IN_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)	
						CLEAR_PED_TASKS(sTargetPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sTargetPed.ped, sTargetVehicle.vehicle)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TARGET WARPED INTO VEHICLE USING SAFETY NET") ENDIF #ENDIF
					ENDIF
					IF NOT DOES_BLIP_EXIST(sTargetPed.blip)
						sTargetPed.blip = CREATE_PED_BLIP(sTargetPed.ped, TRUE, FALSE)
						SET_BLIP_SCALE(sTargetPed.blip, 1.0)	//TEMP: added as per bug 241312
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sTargetPed.ped, TRUE)	
				ENDIF
				//for uber playback
				INITIALISE_UBER_PLAYBACK(sNigel2_UberRecordingName, iMainCarRecID)
				LOAD_UBER_DATA()
				CREATE_ALL_WAITING_UBER_CARS()
				switch_SetPieceCar_to_ai_on_collision = true
				allow_veh_to_stop_on_any_veh_impact = TRUE
				//bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
				//fPlaybackCarStreamingDistance = 160.0	// 120.0 // is default
				traffic_block_vehicle_colour(true, traffic_black)	
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
				AND IS_PED_UNINJURED(sTargetPed.ped)		
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
						#IF IS_DEBUG_BUILD DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE) #ENDIF					
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle,	88006.860) // 70000.0)	// 
						SET_UBER_PLAYBACK_TO_TIME_NOW(sTargetVehicle.vehicle, 88006.860)	// 70000.0)	// 
						//PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF				
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)
				SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG_HOSPITAL_STATE - SS_SETUP - set BUILDINGNAME_IPL_PILLBOX_HILL swaps to BUILDINGSTATE_DESTROYED") ENDIF #ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "		TESTER IT BUILT		$$$$$$$$$$$$$$") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			BREAK			
			//	------------------------------------------
			CASE SS_UPDATE
				NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL()
				IF IS_PED_UNINJURED(sTargetPed.ped)
				AND IS_VEHICLE_OK(sTargetVehicle.vehicle)	
					fCurrentChaseDistance = VDIST(vPlayerPos, GET_ENTITY_COORDS(sTargetVehicle.vehicle, FALSE))
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)						
						fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sTargetVehicle.vehicle)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CURRENT PLAYBACK TIME = ", fCurrentPlaybackTime) ENDIF #ENDIF						
						UPDATE_UBER_CHASE_SPEED(fMainPlaybackSpeed, fCurrentPlaybackTime)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "fMainPlaybackSpeed = ", fMainPlaybackSpeed) ENDIF #ENDIF
						SET_PLAYBACK_SPEED(sTargetVehicle.vehicle, fMainPlaybackSpeed)
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
						PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()	
						CREATE_ALL_WAITING_UBER_CARS()  //might help with some traffic failing to spawn?					
						UPDATE_CHASE_VEHICLES_FROM_UBER_RECORDING()
						UPDATE_UBER_PLAYBACK(sTargetVehicle.vehicle, fMainPlaybackSpeed)	
						MANAGE_RAYFIRE_HOSPITAL_DOORS()
						MANAGE_HOSPITAL_STATE_DURING_CHASE()
						CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sTargetVehicle.vehicle)
						MANAGE_TARGET_BRAKING_AND_USING_HORN()
						APPLY_VISUAL_DAMAGE_TO_TARGET_VEHICLE()
					ELSE
						IF NOT bDone_UberRecordingCleanupForChaseEnd
							CLEANUP_UBER_PLAYBACK()
							REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
							//main car recording
							REMOVE_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)		
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sTargetVehicle.vehicle)
							IF IS_AUDIO_SCENE_ACTIVE("NIGEL_02_CHASE")								
								STOP_AUDIO_SCENE("NIGEL_02_CHASE")	
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " NIGEL 02 -	STOP_AUDIO_SCENE : NIGEL_02_CHASE ", "***") ENDIF #ENDIF
							ENDIF
							SETUP_TARGET_FOR_CRASH()
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UBER RECORDING ENDED, set up for crash scene") ENDIF #ENDIF
							bDone_UberRecordingCleanupForChaseEnd = TRUE	
						ENDIF
					ENDIF
					MANAGE_HOSPITAL_SOUND_FX()
					MANAGE_PEDESTRIANS_ON_CHASE_ROUTE(vPlayerPos)
					MANAGE_HOSPITAL_DEBRIS_EFFECT()
					MANAGE_DIALOGUE_DURING_CHASE(bForceTargetblipFlashThisFrame)	// needs to come before UPDATE_CHASE_BLIP since it sets bForceTargetblipFlashThisFrame
					MANAGE_JUMP_OUT_HOSPITAL_WINDOW()
					//Checl for player close to target at end of the chase.
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sTargetPed.ped), NIGEL2_PLAYER_CLOSE_TO_TARGET_CRASHED, 0, 0, 255, 120)
						IF(GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, GET_ENTITY_COORDS(sTargetPed.ped)) < NIGEL2_PLAYER_CLOSE_TO_TARGET_CRASHED)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER CLOSE TO CRASHED AL DI NAPOLI - GO TO PUT IN BOOT SECTION") ENDIF #ENDIF
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			BREAK			
			//	------------------------------------------
			CASE SS_CLEANUP
				IF bDoneCleanup_JumpOutHospitalWindow
					SET_CHANGES_FOR_FORCED_CINEMATIC_JUMP_OUT_HOSPITAL_WINDOW(FALSE)
					bDoneCleanup_JumpOutHospitalWindow = FALSE
				ENDIF				
				IF NOT bDone_UberRecordingCleanupForChaseEnd
					CLEANUP_UBER_PLAYBACK()
					REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
					IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							//check if its at the end time wise, if so freeze its pos?
							STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						ENDIF
						//SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, TRUE)
					ENDIF
					REMOVE_VEHICLE_RECORDING(iMainCarRecID, sNigel2_UberRecordingName)						
					SETUP_TARGET_FOR_CRASH()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UBER RECORDING ENDED IN SS_CLEANUP") ENDIF #ENDIF
					bDone_UberRecordingCleanupForChaseEnd = TRUE
				ENDIF
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				//KILL_ANY_CONVERSATION()
				SET_STAGE(MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE)
			BREAK			
		ENDSWITCH
	ENDPROC
	
	PROC DEBUG_HOSPITAL_DOORS_STATE()	
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SWITCH eSubStage
		//	------------------------------------------
	 		CASE SS_SETUP
				bDebug_PrintMissionInfoToTTY = TRUE
				LOAD_MISSION_ASSETS(TRUE)				
				SETUP_FOR_START_OF_MISSION()				
				/*
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)	
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sTargetVehicle.vehicle)
				ENDIF			
				
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
						FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
						#IF IS_DEBUG_BUILD
							DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE)
						#ENDIF					
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, 109072.500)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
					ENDIF				
				ENDIF
				*/
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sNigelVehicle.vehicle)
					SAFE_TELEPORT_VEHICLE(sNigelVehicle.vehicle, <<337.6252, -582.2834, 42.3174>>, 252.3868) // for window
						// << 293.3672, -579.1942, 42.1950 >>, 223.0003)	// for hospital doors
					SET_VEHICLE_ENGINE_ON(sNigelVehicle.vehicle, TRUE, TRUE)
				ENDIF
				//for ray fire test only <
				//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 293.3672, -579.1942, 42.1950 >>)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), 223.0003)				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				//SET_BUILDING_STATE(BUILDINGNAME_ES_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)
				//SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED)
				//SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)
				SAFE_FADE_SCREEN_IN_FROM_BLACK(0, FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				
				//temp
				IF eHospitalInteriorState = HIS_SET_INTERIOR_IPLS_FOR_CHASE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL, BUILDINGSTATE_DESTROYED) 			// sets the window to destroyed so player/target can drive through it
					SET_BUILDING_STATE(BUILDINGNAME_IPL_PILLBOX_HILL_INTERIOR, BUILDINGSTATE_NORMAL)	// sets the interior to stream
					HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
					eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
					CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_SET_INTERIOR_IPLS_FOR_CHASE : FC = ", GET_FRAME_COUNT())
					
				// interior needs to be pinned into memory really early
				ELIF eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VALID_INTERIOR(HospitalInteriorIndex)
							IF NOT IS_INTERIOR_READY(HospitalInteriorIndex)	
								PIN_INTERIOR_IN_MEMORY(HospitalInteriorIndex)
							ELSE
								SET_INTERIOR_ACTIVE(HospitalInteriorIndex, TRUE)
								eHospitalInteriorState = HIS_REFRESH_INTERIOR
								CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - done HIS_PIN_INTERIOR_IN_MEMORY : FC = ", GET_FRAME_COUNT())
							ENDIF
						ELSE
							HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
							CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE getting handle to valid interior : FC = ", GET_FRAME_COUNT())
						ENDIF	
					ENDIF
	
				// refresh the interior once everything should be setup correctly
				ELIF eHospitalInteriorState = HIS_REFRESH_INTERIOR
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VALID_INTERIOR(HospitalInteriorIndex)
							IF IS_INTERIOR_READY(HospitalInteriorIndex)	
								REFRESH_INTERIOR(HospitalInteriorIndex)
								eHospitalInteriorState = HIS_READY	// HIS_REQUEST_NEW_LOAD_SCENE_AT_INTERIOR	//
								CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE - SKIPPED ^ done HIS_REFRESH_INTERIOR : FC = ", GET_FRAME_COUNT())
							ELSE
								PIN_INTERIOR_IN_MEMORY(HospitalInteriorIndex)
								CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - re pinnning in memory : FC = ", GET_FRAME_COUNT())
								//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
								//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - error interior no longer ready! : FC = ", GET_FRAME_COUNT())
							ENDIF
						ELSE
							HospitalInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 307.3065, -589.9595, 43.3020 >>, "v_hospital")	//get handle to hospital interior
							CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - getting handle to valid interior : FC = ", GET_FRAME_COUNT())
							//eHospitalInteriorState = HIS_PIN_INTERIOR_IN_MEMORY
							//CPRINTLN(DEBUG_MISSION, " ***** MANAGE_HOSPITAL_STATE_DURING_CHASE : HIS_REFRESH_INTERIOR - error interior no longer valid! : FC = ", GET_FRAME_COUNT())
						ENDIF
					ENDIF
				ENDIF
				
				
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG_HOSPITAL_DOORS_STATE - SS_SETUP - set building swaps to BUILDINGSTATE_DESTROYED") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			BREAK			
			//	------------------------------------------
			CASE SS_UPDATE
				//MANAGE_HOSPITAL_STATE_DURING_CHASE()
				MANAGE_JUMP_OUT_HOSPITAL_WINDOW()	
				NIG2_TRACK_STAT_HAS_PLAYER_INJURED_PED_WHILST_IN_HOSPITAL()
				
				IF IS_VEHICLE_OK(sNigelVehicle.vehicle)
					IF IS_ENTITY_IN_ANGLED_AREA(sNigelVehicle.vehicle, <<381.915619,-628.101440,22.932293>>, <<394.477234,-606.126953,35.774826>>, 18.500000)
			
						//IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS()
						//	CPRINTLN(DEBUG_MISSION, "+++++++++ ok for stop cutscene FC = ", GET_FRAME_COUNT())
						//ENDIF
						
						IF IS_VEHICLE_SETTLED_FOR_CUTSCENE(sNigelVehicle.vehicle)
							IF HAS_TIME_PASSED(iTimer_OutroMocapDelay, 500)
								DRAW_RECT(0.2, 0.5, 0.2, 0.4, 76, 255, 0, 100)		// green
							ELSE
								DRAW_RECT(0.2, 0.5, 0.2, 0.4, 170, 0, 255, 100)		// purple
							ENDIF
						ELSE
							iTimer_OutroMocapDelay = GET_GAME_TIMER()
							DRAW_RECT(0.2, 0.5, 0.2, 0.4, 255, 0, 0, 100)			// red
						ENDIF
					ELSE
						iTimer_OutroMocapDelay = GET_GAME_TIMER()
					ENDIF
				ENDIF
				
				// B*1572752 - now creating doors object in script to display until rayfire anim kicks in - rayfire object always struggled to stream prior to start state
				IF eHospitalDoorsObjectState = HDO_CREATE
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
						IF NOT DOES_ENTITY_EXIST(sObjectHospitalDoors.objectIndex)
							REQUEST_MODEL(sObjectHospitalDoors.modelName)
							IF HAS_MODEL_LOADED(sObjectHospitalDoors.modelName)
								sObjectHospitalDoors.objectIndex = CREATE_OBJECT(sObjectHospitalDoors.modelName, sObjectHospitalDoors.vPos)
								IF IS_ENTITY_ALIVE(sObjectHospitalDoors.objectindex)
									SET_ENTITY_ROTATION(sObjectHospitalDoors.objectindex, sObjectHospitalDoors.vRot)
									FREEZE_ENTITY_POSITION(sObjectHospitalDoors.objectindex, TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(sObjectHospitalDoors.modelName)
									eHospitalDoorsObjectState = HDO_WAIT_FOR_DELETE
									CPRINTLN(DEBUG_MISSION, "OBJECT Hospital Doors Progress, created doors : eHospitalDoorsObjectState > HDO_WAIT_FOR_DELETE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF

				// same frame the rayfire object begins to playback from this object
				ELIF eHospitalDoorsObjectState = HDO_WAIT_FOR_DELETE
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
						IF DOES_ENTITY_EXIST(sObjectHospitalDoors.objectIndex)
							SAFE_DELETE_OBJECT(sObjectHospitalDoors.objectIndex)
							eHospitalDoorsObjectState = HDO_CREATE
							CPRINTLN(DEBUG_MISSION, "OBJECT Hospital Doors Progress, doors deleted for rayfire : eHospitalDoorsObjectState > HDO_CREATE")
						ENDIF
					ENDIF
				ELIF eHospitalDoorsObjectState = HDO_END
				
				ENDIF
				
				//IF GET_ENTITY_UPRIGHT_VALUE(sNigelVehicle.vehicle) < 0.1
				//	APPLY_FORCE_TO_ENTITY(sNigelVehicle.vehicle, APPLY_TYPE_ANGULAR_IMPULSE, -GET_ENTITY_ROTATION_VELOCITY(vehCar), << 0.0, 0.0, 0.0 >>, 0, TRUE, FALSE, FALSE)
				//ENDIF	
				
				/*
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "trigger screams sfx") ENDIF #ENDIF
					PLAY_SOUND_FROM_COORD(-1, "SCREAMS", << 310.5149, -595.4158, 42.3020 >>, "NIGEL_02_SOUNDSET")
				ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "trigger flatline sfx") ENDIF #ENDIF
					PLAY_SOUND_FROM_COORD(-1, "ECG", << 357.6582, -585.5173, 42.3275 >>, "NIGEL_02_SOUNDSET")
				ENDIF
				*/				
				/*
				IF IS_VEHICLE_OK(sTargetVehicle.vehicle)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK OF RECORDING UNPAUSED") ENDIF #ENDIF
						ENDIF
					ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)	
							FREEZE_ENTITY_POSITION(sTargetVehicle.vehicle, FALSE)
							START_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, iMainCarRecID, sNigel2_UberRecordingName)
							#IF IS_DEBUG_BUILD
								DISPLAY_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, RDM_WHOLELINE)
							#ENDIF	
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sTargetVehicle.vehicle)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle, 109072.500)
							PAUSE_PLAYBACK_RECORDED_VEHICLE(sTargetVehicle.vehicle)
						ENDIF
					ENDIF
				ENDIF
				*/				
			BREAK				
			//	------------------------------------------
			CASE SS_CLEANUP
			BREAK			
		ENDSWITCH	
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
		
	/*IF IS_REPLAY_IN_PROGRESS() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_2(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " IS_REPLAY_IN_PROGRESS - waiting on SetupScene_NIGEL_2")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_NIGEL_2, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
		g_bSceneAutoTrigger = FALSE
	ENDIF*/
	
	INIT_MISSION()	
	
	// handle replay checkpoints
	IF IS_REPLAY_IN_PROGRESS()	
	
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF		
		SWITCH iReplayStage		
			CASE CP_NIGEL2_START_CHASE
				START_REPLAY_SETUP(<<-1309.0155, -641.8671, 25.5017>>, 242.3463, FALSE)	// match mocap finished position and heading
				bDoneDialogue_GetInNigelsVehicle[0] = TRUE	//set this dialogue to have already played, because they want the skip to be at the objective instead
				DO_Z_SKIP(Z_SKIP_START_CHASE)	 // skip the mocap intro
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "start chase in progress")
			BREAK	
			CASE CP_NIGEL2_MISSION_PASSED
				START_REPLAY_SETUP(<< 391.08, -615.87, 28.33 >>, 333.66, FALSE)	// match position for kicking off outro mocap
				DO_Z_SKIP(Z_SKIP_OUTRO_CUTSCENE)	 
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "mission passed in progress (play outro mocap first")
			BREAK
			DEFAULT
				 SCRIPT_ASSERT("RC - NIGEL 2 :  Replay checkpoint * - starting mission from invalid checkpoint")
			BREAK
		ENDSWITCH
	ENDIF	
	
	WHILE(TRUE)			
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_VSADN")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())		
		
			MISSION_FAILED_CHECKS()
			
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())		
			
			// Fix B*1121745 - swap over to call everyframe commands
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())

			SWITCH eMissionStage			
				CASE MISSION_STAGE_INTRO_MOCAP_SCENE					
					INTRO_MOCAP_SCENE()
				BREAK				
				CASE MISSION_STAGE_CHASE_TARGET_IN_VEHICLE
					CHASE_TARGET_IN_VEHICLE()
				BREAK					
				CASE MISSION_STAGE_GET_CLOSE_TO_TARGET_FOR_CUTSCENE
					GET_CLOSE_TO_TARGET_FOR_CUTSCENE()
				BREAK				
				CASE MISSION_STAGE_END_CUTSCENE_MOCAP
					STAGE_END_CUTSCENE_MOCAP()
				BREAK
				CASE MISSION_STAGE_MISSION_PASSED
					Script_Passed()
				BREAK
				CASE MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
					MISSION_FAILED_WAIT_FOR_FADE()
				BREAK					
					CASE MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE	// debug stage
						#IF IS_DEBUG_BUILD
							RECORD_CHASE_ROUTE()
						#ENDIF
					BREAK					
					CASE MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE	// debug stage
						#IF IS_DEBUG_BUILD
							PLAYBACK_CHASE_ROUTE()
						#ENDIF
					BREAK					
					CASE MISSION_STAGE_DEBUG						// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_STATE()
						#ENDIF
					BREAK						
					CASE MISSION_STAGE_DEBUG_HOPSITAL				// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_HOSPITAL_STATE()
						#ENDIF
					BREAK					
					CASE MISSION_STAGE_DEBUG_HOPSITAL_DOORS		// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_HOSPITAL_DOORS_STATE()
						#ENDIF
					BREAK					
			ENDSWITCH			
		ENDIF
		
		// if we are skipping through the mission stages, for a checkpoint / debug skip		
		IF bFinishedStageSkipping = FALSE		
			JUMP_TO_STAGE(eMissionSkipTargetStage)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_MISSION_WIDGETS()
			//This is here so it can use the debug skipping functions
			IF bDebug_SetupMission_DebugStage
				RC_START_Z_SKIP(TRUE, FALSE)
				RESET_MISSION()
				SET_STAGE(MISSION_STAGE_DEBUG_HOPSITAL_DOORS)
				RC_END_Z_SKIP()
				IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - mission setup in debug stage.") ENDIF
				bDebug_SetupMission_DebugStage = FALSE
			ENDIF
			
			// temp placement
			IF bDebug_CheckPlayerInsideHospital
				IS_PED_INSIDE_HOSPITAL_INTERIOR(PLAYER_PED_ID())
			ENDIF
		  	IF bFinishedStageSkipping = TRUE		// not skipping stages, check for debug keys as long as we aren't in fail state				
				IF eMissionStage <> MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
		        	DEBUG_Check_Debug_Keys()
				ENDIF
			ENDIF
        #ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
