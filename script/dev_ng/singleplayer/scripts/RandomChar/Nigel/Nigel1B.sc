// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Nigel1B.sc
//		AUTHOR			:	Ahron Mason
//		DESCRIPTION		:	Trevor steals clothes from celebrity for Nigel and Mrs Thornhill.
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "initial_scenes_Nigel.sch"
USING "RC_Area_public.sch"
USING "commands_event.sch"
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:ENUMS
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Mission stages
ENUM MISSION_STAGE
	//Main Mission Stages
	MISSION_STAGE_STEAL_CELEB_POSSESSIONS,
	MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA,
	MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA,
	MISSION_STAGE_OUTRO_PHONECALL,
	MISSION_STAGE_MISSION_PASSED,
	//Additional stages
	MISSION_STAGE_LOSE_THE_COPS,
	MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE,
	//Debug Stages
	MISSION_STAGE_DEBUG_POOL_SETUP
ENDENUM

/// PURPOSE: each mission stage has uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

/// PURPOSE: fail conditions
ENUM FAILED_REASON_ENUM
	FAILED_DEFAULT
ENDENUM

/// PURPOSE: used to update mission peds behaviour
ENUM N1B_PED_AI
	N1B_PED_AI_STATE_RELAXED,
	N1B_PED_AI_SETUP_ROUTINE,
	N1B_PED_AI_STATE_ROUTINE,
	N1B_PED_AI_SETUP_ROUTINE_SECONDARY,
	N1B_PED_AI_STATE_ROUTINE_SECONDARY,
	N1B_PED_AI_STATE_ROUTINE_COMPLETED,
	N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH,
	N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH,
	N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL,
	N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL,
	N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED,
	N1B_PED_AI_CELEB_ONLY_SETUP_POSE,
	N1B_PED_AI_CELEB_ONLY_STATE_POSE,
	N1B_PED_AI_CELEB_ONLY_STATE_POSE_COMPLETED,
	N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB,
	N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB,
	N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM,
	N1B_PED_AI_CELEB_ONLY_STATE_ENTER_HOT_TUB_ANIM,
	N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB,
	N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB,
	N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB,
	N1B_PED_AI_FEMALE_ONLY_STATE_WATCH_DANCE_IN_HOT_TUB,
	N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES,	//	left in for now may want to replace final length
	N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES,
	N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES,
	N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES,
	N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS,
	N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS,
	N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS,
	N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS,
	N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL,
	N1B_PED_AI_CELEB_ONLY_STATE_END_PHONECALL,
	N1B_PED_AI_SETUP_AWARE_OF_PLAYER,
	N1B_PED_AI_STATE_AWARE_OF_PLAYER,
	N1B_PED_AI_SETUP_JUST_FACE_PLAYER,
	N1B_PED_AI_STATE_JUST_FACE_PLAYER,
	N1B_PED_AI_SETUP_AWARE_OF_GARDENER,
	N1B_PED_AI_STATE_AWARE_OF_GARDENER,
	N1B_PED_AI_SETUP_WARN_OTHER_PEDS,
	N1B_PED_AI_STATE_WARN_OTHER_PEDS,
	N1B_PED_AI_SETUP_WAIT_TO_ATTACK,
	N1B_PED_AI_STATE_WAIT_TO_ATTACK,
	N1B_PED_AI_SETUP_BEATEN_UP,
	N1B_PED_AI_STATE_BEATEN_UP,
	N1B_PED_AI_SETUP_SURRENDERED,
	N1B_PED_AI_STATE_SURRENDERED,
	N1B_PED_AI_SETUP_FRIGHTENED,
	N1B_PED_AI_STATE_FRIGHTENED,
	N1B_PED_AI_SETUP_FLEE_WITH_REACT,
	N1B_PED_AI_SETUP_FLEE,
	N1B_PED_AI_STATE_FLEE,
	N1B_PED_AI_STATE_DEAD
ENDENUM

/// PURPOSE: used to keep track of the player's position
ENUM N1B_PLAYER_LOCATION
	N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA,
	N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA,
	N1B_PLAYER_LOCATION_POOL_AREA,
	N1B_PLAYER_LOCATION_GARDEN_AREA
ENDENUM

/// PURPOSE: used to maintain the pool area's state
ENUM N1B_POOL_AREA_STATE
	N1B_POOL_AREA_STATE_UNDETECTED,
	N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES,
	N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED,
	N1B_POOL_AREA_STATE_SPOTTED,
	N1B_POOL_AREA_STATE_CALL_COPS,
	N1B_POOL_AREA_STATE_SURRENDERED,
	N1B_POOL_AREA_STATE_FLEE
ENDENUM

/// PURPOSE: used to maintain the garden area's state
ENUM N1B_GARDEN_AREA_STATE
	N1B_GARDEN_AREA_STATE_UNDETECTED,
	N1B_GARDEN_AREA_STATE_SPOTTED,
	N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA,
	N1B_GARDEN_AREA_STATE_SURRENDERED,
	N1B_GARDEN_AREA_STATE_FLEE,
	N1B_GARDEN_AREA_STATE_GARDENER_DEAD
ENDENUM

/// PURPOSE: used to update a ped's movement state - for use with detection system
ENUM N1B_PED_MOVEMENT_STATE
	N1B_PEDMOVEMENTSTATE_COVER,
	N1B_PEDMOVEMENTSTATE_STEALTH,
	N1B_PEDMOVEMENTSTATE_RUNNING,
	N1B_PEDMOVEMENTSTATE_SPRINTING,
	N1B_PEDMOVEMENTSTATE_WALKING,
	N1B_PEDMOVEMENTSTATE_IN_VEHICLE,
	N1B_PEDMOVEMENTSTATE_DEFAULT
ENDENUM

/// PURPOSE: contains all of the door's which I need to control in script
ENUM N1B_MISSION_CONTROLLED_DOORS_ENUM
	N1B_MCD_NORTH_GATE_01 = 0,
	N1B_MCD_NORTH_GATE_02,
	N1B_MCD_SOUTH_GATE_01,
	N1B_MCD_SOUTH_GATE_02,
	N1B_MCD_EAST_GATE_01,
	N1B_MCD_WEST_GATE_01,
	N1B_MAX_MISSION_CONTROLLED_DOORS
ENDENUM

// ENUM containing states for handling stopping & restarting conversation to avoid subtitles overwriting god text
// differs from RC version as this prevents god text getting override if already displaying when a conversation starts
ENUM N1B_CONV_RESTORE_STATE
	N1B_CONV_IDLE,
	N1B_CONV_STOP_CURRENT_CONV,
	N1B_CONV_RESTART_WITHOUT_SUBS,
	N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT,
	N1B_CONV_RESTART_WITH_SUBS
ENDENUM

/// PURPOSE: list of animations the club peds have
ENUM N1B_SYNCED_SCENE_ANIM_TYPE
	N1B_SSAT_BASE_FEMALE_ONLY,
	N1B_SSAT_ENTER_CELEB_ONLY,
	N1B_SSAT_DANCE_INTRO,
	N1B_SSAT_DANCE_LOOP,
	N1B_SSAT_DANCE_OUTRO_CELEB_ONLY,
	N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY,
	N1B_SSAT_STAND_UP_FEMALE_ONLY,
	N1B_MAX_SYNCED_SCENE_ANIM_TYPE
ENDENUM

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:STRUCTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: used by all scripted peds in the mission
/// AI uses N1B_PED_AI to set behaviours
STRUCT MISSION_PED
	AI_BLIP_STRUCT blipAIStruct
	PED_INDEX pedIndex
	N1B_PED_AI AI
	MODEL_NAMES model
	INT iTimer
	INT iAiDelayTimer
	INT	iFrameCountLastSeenPlayer
ENDSTRUCT

/// PURPOSE: used by all the basic objects/props in the mission
STRUCT MISSION_OBJECT
	OBJECT_INDEX	object
	MODEL_NAMES		model
	VECTOR 			vPosition
	VECTOR 			vRotation
	FLOAT 			fHeading
ENDSTRUCT

/// PURPOSE: used by the pickups in the mission
///    currently using object whilst pickup type won't work see B*1516887
STRUCT STRUCT_MISSION_PICKUP	
	PICKUP_INDEX	index
	MODEL_NAMES		mnModel
	BLIP_INDEX		blipIndex
	VECTOR 			vPosition
	PICKUP_TYPE 	type = PICKUP_CUSTOM_SCRIPT
	INT 			iPlacementFlags	
	VECTOR 			vOrientation
ENDSTRUCT

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:CONSTANTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

CONST_FLOAT NIGEL1B_DIST_PLAYER_LEAVES_THE_HOUSE_AREA 			70.0		// used to detect when the player has completed the objective to leave the area
CONST_FLOAT NIGEL1B_PLAYER_DIST_FROM_HOUSE_FOR_DIALOGUE			35.0		// dist player must be within for dialogue at house to play
CONST_FLOAT NIGEL1B_PED_SEEING_RANGE_STANDARD					18.0		// defined range for ped's seeing range - ped to see standard
CONST_FLOAT NIGEL1B_PED_SEEING_RANGE_PLAYER_STEALTH_MODE		15.0		// defined range for ped's seeing range - ped to see in stealth
CONST_FLOAT NIGEL1B_PED_SEEING_RANGE_PLAYER_RUNNING				22.0		// defined range for ped's seeing range - ped to see running
CONST_FLOAT NIGEL1B_PED_HEARING_RANGE_STANDARD					5.0			// defined range for ped's hearing range - ped to see running
CONST_FLOAT NIGEL1B_PED_HEARING_RANGE_PLAYER_STEALTH_MODE		0.0			// defined range for ped's hearing range - ped to see in stealth
CONST_FLOAT NIGEL1B_PED_HEARING_RANGE_PLAYER_RUNNING			10.0		// defined range for ped's hearing range - ped to see running
CONST_FLOAT NIGEL1B_RANGE_CELEB_PED_CLOSE_TO_CLOTHES_PICKUP		1.25		// dist from clothes pickup location celeb will try to get to
CONST_FLOAT NIGEL1B_POOL_AREA_NORTH_CIRCLE_RADIUS				5.93		// used in definition of pool area
CONST_FLOAT NIGEL1B_POOL_AREA_SOUTH_WALL_POS_AB_WIDTH			2.80		// used in definition of pool area
CONST_FLOAT NIGEL1B_POOL_AREA_SOUTH_WALL_POS_CD_WIDTH			12.50		// used in definition of pool area
CONST_FLOAT NIGEL1B_POOL_AREA_SOUTH_WALL_POS_EF_WIDTH			8.50		// used in definition of pool area
CONST_FLOAT NIGEL1B_POOL_AREA_SOUTH_WALL_POS_GH_WIDTH			4.0			// used in definition of pool area
CONST_FLOAT NIGEL1B_PED_DETECTED_CLOSEBY_WALKING_RANGE			3.5			// defined range for ped detection when ped is walking nearby
CONST_FLOAT NIGEL1B_PED_DETECTED_CLOSEBY_RUNNING_RANGE			7.0			// defined range for ped detection when ped is running nearby
CONST_FLOAT NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE			66.21		// used for the Z checks when updating the player's location
CONST_FLOAT NIGEL1B_GROUND_Z_TOLERANCE_AT_HOUSE					11.0		// used for the Z checks when updating the player's location
CONST_FLOAT NIGEL1B_DIST_LIMIT_PLAYER_TO_PED_OK_FOR_DIALOGUE	25.0
CONST_INT POOL_PED_CELEB										0    		// celeb who owns the clothes
CONST_INT POOL_PED_FEMALE										1			// celeb's bird in the HOT_TUB
CONST_INT TOTAL_POOL_PEDS										2    		// total number of peds in the pool area
CONST_INT TOTAL_POOL_AREA_PROPS 								2			// number of scripted objects placed as dressing
CONST_INT NIGEL1B_BEATEN_UP_HEALTH_THRESHOLD					180			// health threshold for setting a ped as beaten up
CONST_INT NIGEL1B_TIMER_TILL_CELEB_LEAVES_POOL					150000000	 	// don't do this for now	//10000
CONST_INT NIGEL1B_TIME_DELAY_FOR_POOL_PEDS_TO_CALL_COPS			10000		// timer till pool peds call the cops if the player isn't present to see it
CONST_INT NIGEL1B_TIME_DELAY_GARDENER_START_ROUTINE				12000		// gardener starts in relaxed ai state with head in back of truck, to allow player chance to sneak up on him
CONST_INT TIME_DELAY_FOR_OUTRO_PHONECALL						2500		// don't have the player call Nigel directly after losing cops
CONST_INT CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD				10			// grace period in frame count for can ped see ped tests
CONST_INT CP_NIGEL1B_STEAL_ITEMS								0			// Initial mission replay checkpoint Note: this doesn't get set by the mission, but is used to skip over the phonecall
CONST_INT CP_NIGEL1B_ITEMS_STOLEN_LEAVE_THE_AREA				1			// 1st mission replay checkpoint : set by this script
CONST_INT CP_NIGEL1B_MISSION_PASSED								2			// 2nd mission replay checkpoint - used by shitskip
CONST_INT Z_SKIP_STEAL_CELEB_CLOTHES							0			// z skip stage
CONST_INT Z_SKIP_ITEMS_STOLEN_LEAVE_THE_AREA					1			// z skip stage
CONST_INT Z_SKIP_PLAYER_LEFT_THE_AREA							2			// z skip stage
CONST_INT Z_SKIP_OUTRO_PHONECALL								3			// z skip stage
CONST_INT Z_SKIP_MISSION_PASSED									4			// z skip stage
CONST_INT NIGEL1B_PLAYER_ON_GARDENERS_RIGHT_SIDE				1			// define to show side returned from GET_SIDE_POS_IS_TO_PED
CONST_INT NIGEL1B_REPLAY_POOL_PED_STATE							0			// index in the replay array 
CONST_INT NIGEL1B_REPLAY_GARDENER_STATE							1			// index in the replay array
CONST_INT NIGEL1B_POOL_NORTH_SIDE								0			// index used in various vector arrays
CONST_INT NIGEL1B_POOL_SOUTH_SIDE								1			// index used in various vector arrays
CONST_INT NIGEL1B_TOTAL_POOL_SIDES								2			// max counter used in various vector arrays
CONST_INT NIGEL1B_MUSIC_EVENT_START								0
CONST_INT NIGEL1B_MUSIC_EVENT_WANTED							1
CONST_INT NIGEL1B_MUSIC_EVENT_COPS_LOST							2
CONST_INT NIGEL1B_MUSIC_EVENT_RESET_FLAG_ALLOW_SCORE_AND_RADIO	3
CONST_INT NIGEL1B_MUSIC_EVENT_MAX								4
CONST_INT NIGEL1B_TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE		3000
CONST_INT NUM_TREVOR_BARGE_GATE_COMMENTS						2
CONST_INT NIGEL1B_PROP_GARDENER_LEAF_BLOWER						0
CONST_INT NIGEL1B_PROP_GARDENER_TOOL_BOX						1
CONST_INT NIGEL1B_PROP_GARDENER_TOOL_RAKE						2
CONST_INT NIGEL1B_TOTAL_GARDENER_VAN_PROPS						3
//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:VARIABLES
//-------------------------------------------------------------------------------------------------------------------------------------------------

MISSION_STAGE eMissionStage	= MISSION_STAGE_STEAL_CELEB_POSSESSIONS
MISSION_STAGE eMissionSkipTargetStage					//used in mission checkpoint setup and debug stage skipping
SUB_STAGE eSubStage 		= SS_SETUP
g_structRCScriptArgs sRCLauncherDataLocal
structPedsForConversation sDialogue
#IF IS_DEBUG_BUILD			// Mission skipping
	CONST_INT MAX_SKIP_MENU_LENGTH 5
    MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF			
BOOL bApplyWantedLevelInScript 
BOOL bCelebOkToStartPhoneConvo
BOOL bDetachedPropsInGardenVehicle
BOOL bDisplayedWantedRatingObjective	
BOOL bDoneDialogueGardener_Flee
BOOL bDoneDialogueGardener_InformsPoolPeds
BOOL bDoneDialogueGardener_PlayerSaysGetLost
BOOL bDoneDialogueGardener_Spotted
BOOL bDoneDialogueGardener_Surrendered
BOOL bDoneDialogueGarden_TrevorKillsGardener
BOOL bDoneDialoguePool_CallCops
BOOL bDoneDialoguePool_CelebGetsOutPool
BOOL bDoneDialoguePool_CelebInTheHotTub
BOOL bDoneDialoguePool_CelebInTheHotTubRunOutOfLines
BOOL bDoneDialoguePool_CelebPoses
BOOL bDoneDialoguePool_CelebSeesClothesAreMissing
BOOL bDoneDialoguePool_InitialExchange
BOOL bDoneDialoguePool_InitiallySpotted
BOOL bDoneDialoguePool_Spotted 	
BOOL bDoneDialoguePool_Surrendered 							
BOOL bDoneDialogue_ItemsStolenPlayerLeftTheArea
BOOL bDoneDialogue_PlayerAcknowledgesCops
BOOL bDoneDialogue_PlayerGrabsClothes 
BOOL bDoneDialogue_ShoutCallCopsWhenPlayerLeaves
BOOL bDoneDialogue_TrevorBargesLockedGate[NUM_TREVOR_BARGE_GATE_COMMENTS]
BOOL bDoneDialogue_PlayerMissedCelebCallingCops
BOOL bTrevorBargedWestGate
BOOL bTrevorBargedEastGate
BOOL bDoSwitch_PoolStateTo_CelebLookingForClothes
BOOL bFinishedStageSkipping						= TRUE			//used to determine if we are mission replay checkpoint skipping or debug skipping
BOOL bLoadedWorldForStageSkipping = FALSE	// flag to say if we haveloaded the world around the player when stage skipping
BOOL bForceHouseGatesOpenDuringGameplay
BOOL bHasCelebReachedClothesLocation
BOOL bHasCopBeenCreatedForScriptedWantedLevel
BOOL bHasOutroPhonecallSucceeded
BOOL bHaveNorthGatesBeenOpenedByPlayer
BOOL bHavePoolPedsCallTheCops
BOOL bHaveSouthGatesBeenOpenedByPlayer
BOOL bPauseDialoguePool_CelebInTheHotTub
BOOL bPlayerStoppedCelebCallingCops			
BOOL bPlayerWasSpottedByGardener
BOOL bDoneDanceOutroSceneForPoolPeds[TOTAL_POOL_PEDS] 
BOOL bDoneStandUpSceneForFemalePoolPed
BOOL bStoppedGardenerVanAnim
FAILED_REASON_ENUM eN1B_MissionFailedReason
FLOAT fCelebPoolSwimArrivalHeading[NIGEL1B_TOTAL_POOL_SIDES]
FLOAT fCelebPoolSwimInitialHeading[NIGEL1B_TOTAL_POOL_SIDES]
FLOAT fStepOutOfPoolHeading[NIGEL1B_TOTAL_POOL_SIDES]
//FLOAT fPedLeftPoolHeading[NIGEL1B_TOTAL_POOL_SIDES]
FLOAT fDistToPoolEdge
FLOAT fHotTubDimensionsWidth
FLOAT fPoolDimensionsWidth
INT iAdditionalSurrenderedDialoguePool_Counter
INT iCelebInPoolInitialPositionID
INT iMissionControlledDoors[N1B_MAX_MISSION_CONTROLLED_DOORS]
INT iPedID_PoolPedWhoDetectedPlayer
//INT iPoolPedId_ToUpdateInManager							//ommented out for now as this is pretty slow at updating
INT iSoundID_WhistlingGardener
INT iTimer_DelayAdditionalSurrenderedDialogue
INT iTimer_DelayForPhoneTask
INT iTimer_DelayTrevorCopsDialogue
INT iTimer_PoolPedsTo_CallCops
INT iTimer_PoolStateTo_CelebLookingForClothes
INT iMissionMusicEventTriggerCounter
INT iTimer_DelayForOutroPhonecall
INT iTimer_DelayResetAudioFlag_AllowScoreAndRadio
INT iTimer_DelayGardenerSearchingVanDialogue
INT iDialogueTimer_PoolPedFlee
INT iDialogueTimer_PoolPedWaitingForCops
INT iTimer_DelayPoolPedsFleeWhenSurrendered
INT iTimer_TimeWindowForSpottedStealingClothesDialogue
INT iTimer_GardenAreaState
INT iSyncScenesID_PoolPeds[N1B_MAX_SYNCED_SCENE_ANIM_TYPE]
//INT iSyncSceneID_GardenerIdle
INT iPoolPedID_PlayerAttackedToFlee
FLOAT fHeading_PlayerMissionStart
MISSION_PED sGardenerPed
MISSION_PED sPoolPed[TOTAL_POOL_PEDS]
STRUCT_MISSION_PICKUP mpClothesPickup
MISSION_OBJECT moCelebPhone
MISSION_OBJECT moGardnerVanProps[NIGEL1B_TOTAL_GARDENER_VAN_PROPS]
MISSION_OBJECT moPoolAreaProps[TOTAL_POOL_AREA_PROPS]
MODEL_NAMES mnCelebVehicle
MODEL_NAMES mnCopCar
MODEL_NAMES mnCopPed
MODEL_NAMES mnGardenerVehicle
N1B_GARDEN_AREA_STATE eGardenAreaState
N1B_PED_MOVEMENT_STATE ePlayerMovementState
N1B_PLAYER_LOCATION	ePlayerLocation
N1B_POOL_AREA_STATE ePoolAreaState
REL_GROUP_HASH relGroupPlayerEnemy
N1B_CONV_RESTORE_STATE eRestoreConversationState
SCENARIO_BLOCKING_INDEX scenarioBlockingHouse
SEQUENCE_INDEX sequenceAI
STRING sRoot_PlayerLeavesAreaDialogue						//Alternative dialogue for player leaving area, based on if he was detected.
TEST_POLY tpCelebHouseArea
TEST_POLY tpGardenArea
TEXT_LABEL_23 tlPausedDialogue_CelebInTheHotTub = ""
TEXT_LABEL_63 tlAnimDict_PedAwareIdles = "rcmnigel1b@reactions"
TEXT_LABEL_63 tlAnimDict_MissionMocappedAnims = "rcmnigel1bnmt_1b"
TEXT_LABEL_23 tlDialogueRoot_PoolPedsSurrender = ""
TEXT_LABEL_23 tlDialogueRoot_PoolPedsCurrentReactionDialogue = ""
TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel
VECTOR vCelebGoToClothesPos
VECTOR vCelebPoolSwimGotos[NIGEL1B_TOTAL_POOL_SIDES]
VECTOR vCelebritiesHouse_CentralPoint
VECTOR vClearPedZone_HouseArea_Max
VECTOR vClearPedZone_HouseArea_Min
VECTOR vPlayerPos
VECTOR vStepOutOfPoolPos[NIGEL1B_TOTAL_POOL_SIDES]
VECTOR vPedLeftPoolPos[NIGEL1B_TOTAL_POOL_SIDES]
VECTOR vGotowardsHotTubInitialPos[NIGEL1B_TOTAL_POOL_SIDES]
VECTOR vHotTubDimensionsCentrePos
VECTOR vPoolDimensionsMinPos
VECTOR vPoolDimensionsMaxPos
//pool areas
VECTOR vPoolArea_NorthCircle_Pos
VECTOR vPoolArea_MajorityPosA
VECTOR vPoolArea_MajorityPosB
VECTOR vPoolArea_SouthWallPosA
VECTOR vPoolArea_SouthWallPosB
VECTOR vPoolArea_SouthWallPosC
VECTOR vPoolArea_SouthWallPosD
VECTOR vPoolArea_SouthWallPosE
VECTOR vPoolArea_SouthWallPosF
VECTOR vPoolArea_SouthWallPosG
VECTOR vPoolArea_SouthWallPosH
VECTOR vSyncScene_HotTub_Position
VECTOR vSyncScene_HotTub_Rotation
VECTOR vPos_PlayerMissionStart
VEHICLE_INDEX vehCelebsVehicle
VEHICLE_INDEX vehGardenerVehicle
VEHICLE_INDEX vehIndex_MissionReplayRestore
WEAPON_TYPE weaponTypeStoredCurrentEquipped = WEAPONTYPE_UNARMED

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:DEBUG FUNCS / PROCS / WIDGETS
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintMissionInfoToTTY										= FALSE		// needs to be false before submit!
	BOOL bDebug_Display_PP_AI												= FALSE		// needs to be false before submit!
	BOOL bDebug_DisplayPlayerLocation 										= FALSE
	//BOOL bDebug_ActivateDebugLines 										= FALSE
	BOOL bDebug_DoIsPlayerAtPoolChecks										= FALSE
	BOOL bDebug_DoIsPlayerUsingStealth 										= FALSE
	BOOL bDebug_PrintSpottedBoolInfo 										= FALSE	
	BOOL bDebug_DoPlayerStealthModeCheck 									= FALSE
	INT iDebug_PoolPedID_StealthCheck 										= 0
	BOOL bDebug_DoIndividualStealthCheck 									= FALSE	
	FLOAT fDebug_DistCheckForStealth_PoolPedID[TOTAL_POOL_PEDS]
	BOOL bDebug_DoIndividualDistanceCheck									= FALSE
	BOOL bDebug_DoGardenerStealthCheck 										= FALSE
	FLOAT fDebug_DistCheckGardenerSpottedRange
	BOOL bDebug_DoGardenerSeeHatedPedsCheck 								= FALSE
	FLOAT fDebug_GardenerSeeingRange 										= 15.0
	FLOAT fDebug_GardenerHearingRange 										= 0.0
	BOOL bDebug_DoStealthCheckForAllPoolPeds 								= FALSE
	BOOL bDebug_DoDistanceChecksForAllPoolPeds								= FALSE
	BOOL bDebug_PrintDistChecksForAllPoolPeds 								= FALSE
	BOOL bDebug_UnPauseGame 												= FALSE
	BOOL bDebug_ToggleDoSpottedPlayerChecksPause 							= FALSE
	BOOL bDebug_TogglePRF_DisableSeeThroughChecksWhenTargeting_Gardener 	= FALSE
	BOOL bDebug_SetupMissionToTest_Can_Ped_See_Hated_Ped 					= FALSE
	BOOL bDebug_ToggleDoIsPlayerInWaterCheck 								= FALSE
	BOOL bDebug_PrintCurrentPoolAreaState 									= FALSE
	BOOL bDebug_PrintCurrentGardenAreaState 								= FALSE
	BOOL bDebug_UpdateGardenerPropAttachmentPosAndRot							= FALSE
	BOOL bDebug_SetGardenerUsingPlantScenario								= FALSE
	BOOL bDebug_SetGardenerUsingLeafBlowerScenario							= FALSE	
	BOOL bDebug_ClearGardenersTasks											= FALSE	
	BOOL bDebug_AttachLeafBlowerToGardener									= FALSE
	BOOL bDebug_DrawSphereAroundLeafBlower									= FALSE
	BOOL bDebug_SetGardenerInIdleAnimAtCar									= FALSE
	BOOL bDebug_PrintGardenerAIState										= FALSE
	BOOL bDebug_SphereAroundGardener										= FALSE
	FLOAT fDebug_SphereAroundGardenerRadius									= 1.0
	BOOL bDebug_GivePlayerSilencedWeapon									= FALSE
	INT iDebug_GardenerVanPropIndex											= 2	
	BOOL bDebug_SetGardenerSyncScenePosRot 									= FALSE
	VECTOR vDebug_GardenerSyncScenePos										= << -1010.89, 360.057, 70.6622 >> //= GET_ENTITY_COORDS(vehGardenerVehicle) + << 0.0, 0.0, 0.02 >>
	VECTOR vDebug_GardenerSyncSceneRot								 		= << 7.36256, -0.0311022, -28.5377 >> //= GET_ENTITY_ROTATION(vehGardenerVehicle))
	
	/// PURPOSE:
	/// My debug mission widget groups, which get created in RAG->SCRIPT
	PROC SETUP_MISSION_WIDGET()
		widgetGroup = START_WIDGET_GROUP("NIGEL 1B WIDGETS")		
			START_WIDGET_GROUP("General")			
				ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintMissionInfoToTTY)					
				ADD_WIDGET_BOOL("Toggle - Print Pool Peds scripted AI TTY info into Script Mission windowg", bDebug_Display_PP_AI)			
				ADD_WIDGET_BOOL("bDebug_DisplayPlayerLocation", bDebug_DisplayPlayerLocation)				
				ADD_WIDGET_BOOL("Do is player at pool area check", bDebug_DoIsPlayerAtPoolChecks)
				ADD_WIDGET_BOOL("Do is player using stealth checks", bDebug_DoIsPlayerUsingStealth)				
				ADD_WIDGET_BOOL("bDebug_PrintCurrentPoolAreaState", bDebug_PrintCurrentPoolAreaState)
				ADD_WIDGET_BOOL("bDebug_PrintCurrentGardenAreaState", bDebug_PrintCurrentGardenAreaState)
				//	ADD_WIDGET_BOOL("Activate Debug Lines And Spheres", bDebug_ActivateDebugLines)					
				ADD_WIDGET_BOOL("Toggle printing to tty if player is classed as being in water or not", bDebug_ToggleDoIsPlayerInWaterCheck)
				ADD_WIDGET_BOOL("Print Spotted Bool Info", bDebug_PrintSpottedBoolInfo)		
				ADD_WIDGET_BOOL("Print Gardener AI State", bDebug_PrintGardenerAIState)		
				ADD_WIDGET_BOOL("Give player a silenced weapon", bDebug_GivePlayerSilencedWeapon)		
			STOP_WIDGET_GROUP()	
			START_WIDGET_GROUP("Gardener setup")
				ADD_WIDGET_VECTOR_SLIDER("Gardener van prop attach pos", moGardnerVanProps[iDebug_GardenerVanPropIndex].vPosition, -1000, 1000, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Gardener van prop attach rot", moGardnerVanProps[iDebug_GardenerVanPropIndex].vRotation, -1000, 1000, 0.1)
				ADD_WIDGET_INT_SLIDER("Set gardener van prop to update attachment", iDebug_GardenerVanPropIndex, 0, (NIGEL1B_TOTAL_GARDENER_VAN_PROPS - 1), 1)
				ADD_WIDGET_BOOL("Update gardener van prop pos and rot ATTACHMENT TO van", bDebug_UpdateGardenerPropAttachmentPosAndRot)
				ADD_WIDGET_BOOL("Update gardener sync scene position and rot ", bDebug_SetGardenerSyncScenePosRot) 
				ADD_WIDGET_VECTOR_SLIDER("Gardener sync scene pos ", vDebug_GardenerSyncScenePos, -1500, 1500, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Gardener sync scene rot ", vDebug_GardenerSyncSceneRot, -1500, 1500, 0.1)
				
				ADD_WIDGET_BOOL("Set Gardener Using Plant Scenario", bDebug_SetGardenerUsingPlantScenario)
				ADD_WIDGET_BOOL("Set Gardener Using Leaf Blower Scenario", bDebug_SetGardenerUsingLeafBlowerScenario)
				ADD_WIDGET_BOOL("Clear Gardeners Tasks", bDebug_ClearGardenersTasks)				
				ADD_WIDGET_BOOL("Attach gardener prop to van", bDebug_AttachLeafBlowerToGardener)
				ADD_WIDGET_BOOL("Debug Draw Sphere Around Leaf Blower", bDebug_DrawSphereAroundLeafBlower)	
				ADD_WIDGET_BOOL("Set Gardener In Idle Anim At Car", bDebug_SetGardenerInIdleAnimAtCar)		
				ADD_WIDGET_BOOL("debug draw sphere around gardener", bDebug_SphereAroundGardener)
				ADD_WIDGET_FLOAT_SLIDER("sphere around gardener radius", fDebug_SphereAroundGardenerRadius, 0, 50.0, 0.1)
			STOP_WIDGET_GROUP()		
			START_WIDGET_GROUP("STEALTH CHECKS")
				ADD_WIDGET_BOOL("bDebug_DoPlayerStealthModeCheck", bDebug_DoPlayerStealthModeCheck)				
				ADD_WIDGET_INT_SLIDER("POOL PED TO CHECK AGAINST - ", iDebug_PoolPedID_StealthCheck, 0, (TOTAL_POOL_PEDS - 1), 1)
				ADD_WIDGET_BOOL("DO INDIVIDUAL STEALTH CHECK - ", bDebug_DoIndividualStealthCheck)
				ADD_WIDGET_FLOAT_SLIDER("DISTANCE CHECK - ", fDebug_DistCheckForStealth_PoolPedID[iDebug_PoolPedID_StealthCheck], 0.0, 200.0, 0.01)
				ADD_WIDGET_BOOL("DO INDIVIDUAL DISTANCE CHECK - ", bDebug_DoIndividualDistanceCheck)				
				ADD_WIDGET_BOOL("DO STEALTH CHECK FOR ALL - ", bDebug_DoStealthCheckForAllPoolPeds)
				ADD_WIDGET_BOOL("DO DISTANCE CHECKS FOR ALL - ", bDebug_DoDistanceChecksForAllPoolPeds)
				ADD_WIDGET_BOOL("PRINT DISTANCE CHECKS FOR ALL - ", bDebug_PrintDistChecksForAllPoolPeds)				
				ADD_WIDGET_BOOL("Do gardener stealth check", bDebug_DoGardenerStealthCheck)
				ADD_WIDGET_FLOAT_SLIDER("Distance check for gardener range", fDebug_DistCheckGardenerSpottedRange, 0.0, 200.0, 0.01)				
				ADD_WIDGET_BOOL("Do can gardener see hated ped check", bDebug_DoGardenerSeeHatedPedsCheck)
				ADD_WIDGET_FLOAT_SLIDER("Gardener seeing distance ", fDebug_GardenerSeeingRange, 0.0, 200.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Gardener hearing distance", fDebug_GardenerHearingRange, 0.0, 200.0, 0.01)									
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("B*433149 - CAN_PED_SEE_HATED_PED  issue")
				ADD_WIDGET_BOOL("Setup Mission State to test this bug", bDebug_SetupMissionToTest_Can_Ped_See_Hated_Ped)
				//Basically i use these in conjunction with the stage MISSION_STAGE_DEBUG_POOL_SETUP to test out stealth
				//these appear in rag and work as followed
				//toggle one of the spotted checks on
				//once spotted the game will pause so you can fly the camera to that peds position to check the player was visible
				//then press the uppause game tick box to get the game running again
				//you'll have to set one of the these toggles back on in the widget to test again.
				ADD_WIDGET_BOOL("Toggle gardener spotted player checks (will pause gameplay when CAN_PED_SEE_HATED_PED returns TRUE)", bDebug_ToggleDoSpottedPlayerChecksPause)				
				ADD_WIDGET_BOOL("Unpause game (use after game is paused by the gardener spotted player checks)", bDebug_UnPauseGame)
				ADD_WIDGET_BOOL("Toggle PRF_DisableSeeThroughChecksWhenTargeting on the gardener", bDebug_TogglePRF_DisableSeeThroughChecksWhenTargeting_Gardener)	
			STOP_WIDGET_GROUP()			
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    updates my mission widgets, based off RAG input
	PROC MAINTAIN_MISSION_WIDGETS()
		INT i
		/*IF bDebug_ActivateDebugLines
			SET_DEBUG_ACTIVE(TRUE)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		ELSE
			SET_DEBUG_ACTIVE(FALSE)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ENDIF*/
		/*//debug teleport to pool area entrance
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0))
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -1038.3511, 347.9641, 69.8164 >>, 91.8594, TRUE)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " NIGEL 1B : ", "DEBUG TELEPORTED PLAYER") ENDIF #ENDIF
		ENDIF*/
		/*// debug fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRESSED KEY 0") ENDIF #ENDIF
	    	IF IS_PED_UNINJURED(sPoolPed[0].pedIndex)
				EXPLODE_PED_HEAD(sPoolPed[0].pedIndex)
				SET_ENTITY_HEALTH(sPoolPed[0].pedIndex, 0)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - KILLED pool ped") ENDIF #ENDIF
			ENDIF
	    ENDIF*/		
		IF bDebug_GivePlayerSilencedWeapon
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
				bDebug_GivePlayerSilencedWeapon = FALSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - given player a silenced weapon") ENDIF #ENDIF
			ENDIF
		ENDIF
		IF bDebug_PrintGardenerAIState
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - Gardener AI State = ", sGardenerPed.AI) ENDIF #ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
			IF bDebug_SetGardenerSyncScenePosRot
				/*IF IS_VEHICLE_OK(vehGardenerVehicle)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID_GardenerIdle)
						SET_SYNCHRONIZED_SCENE_ORIGIN(iSyncSceneID_GardenerIdle, vDebug_GardenerSyncScenePos, vDebug_GardenerSyncSceneRot)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerSyncScenePosRot updated Pos = ", vDebug_GardenerSyncScenePos, " Rot = ", vDebug_GardenerSyncSceneRot) ENDIF #ENDIF
					ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iNigel1B_Gardener_SyncSceneIndex)
						SET_SYNCHRONIZED_SCENE_ORIGIN(iNigel1B_Gardener_SyncSceneIndex, vDebug_GardenerSyncScenePos, vDebug_GardenerSyncSceneRot)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerSyncScenePosRot (iNigel1B_Gardener_SyncSceneIndex) updated Pos = ", vDebug_GardenerSyncScenePos, " Rot = ", vDebug_GardenerSyncSceneRot) ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerSyncScenePosRot fail no sync scene active") ENDIF #ENDIF
					ENDIF
				ENDIF*/
				bDebug_SetGardenerSyncScenePosRot = FALSE
			ENDIF						
			IF bDebug_SetGardenerInIdleAnimAtCar
				IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener")
					TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS) // B*1544302 - AF_USE_KINEMATIC_PHYSICS)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_SetGardenerInIdleAnimAtCar anim applied") ENDIF #ENDIF
				ELSE
					CLEAR_PED_TASKS(sGardenerPed.pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_SetGardenerInIdleAnimAtCar already playing anim so cleared task instead this frame") ENDIF #ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_SetGardenerInIdleAnimAtCar done") ENDIF #ENDIF
				bDebug_SetGardenerInIdleAnimAtCar = FALSE
			ENDIF		
			IF bDebug_SetGardenerUsingPlantScenario
				CLEAR_PED_TASKS(sGardenerPed.pedIndex)
				IF NOT IS_PED_USING_SCENARIO(sGardenerPed.pedIndex, "WORLD_HUMAN_GARDENER_PLANT")
					TASK_START_SCENARIO_IN_PLACE(sGardenerPed.pedIndex, "WORLD_HUMAN_GARDENER_PLANT", -1, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerUsingPlantScenario - WORLD_HUMAN_GARDENER_PLANT - set ") ENDIF #ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerUsingPlantScenario - done") ENDIF #ENDIF
				bDebug_SetGardenerUsingPlantScenario = FALSE
			ENDIF
			IF bDebug_SetGardenerUsingLeafBlowerScenario
				CLEAR_PED_TASKS(sGardenerPed.pedIndex)
				IF NOT IS_PED_USING_SCENARIO(sGardenerPed.pedIndex, "WORLD_HUMAN_GARDENER_LEAF_BLOWER")
					TASK_START_SCENARIO_IN_PLACE(sGardenerPed.pedIndex, "WORLD_HUMAN_GARDENER_LEAF_BLOWER", -1, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerUsingLeafBlowerScenario - WORLD_HUMAN_GARDENER_LEAF_BLOWER - set ") ENDIF #ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_SetGardenerUsingLeafBlowerScenario - done") ENDIF #ENDIF
				bDebug_SetGardenerUsingLeafBlowerScenario = FALSE
			ENDIF
			IF bDebug_ClearGardenersTasks
				CLEAR_PED_TASKS(sGardenerPed.pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_ClearGardenersTasks - done") ENDIF #ENDIF
				bDebug_ClearGardenersTasks = FALSE
			ENDIF
			IF bDebug_SphereAroundGardener
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sGardenerPed.pedIndex), fDebug_SphereAroundGardenerRadius, 100, 100, 0, 100)
			ENDIF
			IF bDebug_AttachLeafBlowerToGardener
				IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					FREEZE_ENTITY_POSITION(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, FALSE)
					IF IS_ENTITY_ATTACHED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
						DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					ENDIF
					ATTACH_ENTITY_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex, GET_PED_BONE_INDEX(sGardenerPed.pedIndex, BONETAG_PH_R_HAND),
											moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].vPosition, moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].vRotation)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_AttachLeafBlowerToGardener - done") ENDIF #ENDIF
					bDebug_AttachLeafBlowerToGardener = FALSE
				ENDIF
			ENDIF	
			IF bDebug_DrawSphereAroundLeafBlower
				IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object), 2.0, 10, 255, 0, 150)
				ENDIF
			ENDIF
		ENDIF
		IF bDebug_UpdateGardenerPropAttachmentPosAndRot
			IF IS_ENTITY_ALIVE(moGardnerVanProps[iDebug_GardenerVanPropIndex].object)
			AND IS_ENTITY_ALIVE(vehGardenerVehicle)
				FREEZE_ENTITY_POSITION(moGardnerVanProps[iDebug_GardenerVanPropIndex].object, FALSE)
				IF IS_ENTITY_ATTACHED(moGardnerVanProps[iDebug_GardenerVanPropIndex].object)
					DETACH_ENTITY(moGardnerVanProps[iDebug_GardenerVanPropIndex].object)
				ENDIF
				INT iVehicleBone = GET_ENTITY_BONE_INDEX_BY_NAME(vehGardenerVehicle, "chassis")
				ATTACH_ENTITY_TO_ENTITY(moGardnerVanProps[iDebug_GardenerVanPropIndex].object, vehGardenerVehicle, iVehicleBone, moGardnerVanProps[iDebug_GardenerVanPropIndex].vPosition, moGardnerVanProps[iDebug_GardenerVanPropIndex].vRotation)
				//SET_ENTITY_COORDS_NO_OFFSET(moGardnerVanProps[iDebug_GardenerVanPropIndex].vPosition)
				//SET_ENTITY_ROTATION(moGardnerVanProps[iDebug_GardenerVanPropIndex].vRotation)
				//FREEZE_ENTITY_POSITION(moGardnerVanProps[iDebug_GardenerVanPropIndex].object, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - bDebug_UpdateGardenerPropAttachmentPosAndRot - done for iDebug_GardenerVanPropIndex =", iDebug_GardenerVanPropIndex) ENDIF #ENDIF
			ENDIF
			bDebug_UpdateGardenerPropAttachmentPosAndRot = FALSE
		ENDIF
		IF bDebug_DisplayPlayerLocation
			SWITCH ePlayerLocation
				CASE N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA") ENDIF #ENDIF
				BREAK
				CASE N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA") ENDIF #ENDIF
				BREAK
				CASE N1B_PLAYER_LOCATION_POOL_AREA
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PLAYER_LOCATION_POOL_AREA") ENDIF #ENDIF
				BREAK
				CASE N1B_PLAYER_LOCATION_GARDEN_AREA
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PLAYER_LOCATION_GARDEN_AREA") ENDIF #ENDIF
				BREAK
			ENDSWITCH
		ENDIF		
		IF bDebug_PrintCurrentPoolAreaState
			SWITCH ePoolAreaState
				CASE N1B_POOL_AREA_STATE_UNDETECTED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_UNDETECTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_SPOTTED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_CALL_COPS
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_CALL_COPS") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_SURRENDERED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_SURRENDERED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_FLEE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentPoolAreaState - N1B_POOL_AREA_STATE_FLEE") ENDIF #ENDIF
				BREAK
			ENDSWITCH
		ENDIF		
		IF bDebug_PrintCurrentGardenAreaState
			SWITCH eGardenAreaState
				CASE N1B_GARDEN_AREA_STATE_UNDETECTED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentGardenAreaState - N1B_GARDEN_AREA_STATE_UNDETECTED") ENDIF #ENDIF
				BREAK
				CASE N1B_GARDEN_AREA_STATE_SPOTTED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentGardenAreaState - N1B_GARDEN_AREA_STATE_SPOTTED") ENDIF #ENDIF
				BREAK
				CASE N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentGardenAreaState - N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA") ENDIF #ENDIF
				BREAK
				CASE N1B_GARDEN_AREA_STATE_SURRENDERED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentGardenAreaState - N1B_GARDEN_AREA_STATE_SURRENDERED") ENDIF #ENDIF
				BREAK
				CASE N1B_GARDEN_AREA_STATE_GARDENER_DEAD
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_PrintCurrentGardenAreaState - N1B_GARDEN_AREA_STATE_GARDENER_DEAD") ENDIF #ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		IF bDebug_PrintSpottedBoolInfo
			IF ePoolAreaState != N1B_POOL_AREA_STATE_UNDETECTED
			AND ePoolAreaState != N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	TRUE 	- player has been spotted") ENDIF #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "	FALSE 	- player has not been spotted") ENDIF #ENDIF
			ENDIF
			bDebug_PrintSpottedBoolInfo = FALSE
		ENDIF		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF bDebug_DoStealthCheckForAllPoolPeds
				FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
					IF IS_ENTITY_ALIVE(sPoolPed[i].pedIndex)
						IF CAN_PED_SEE_HATED_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID())							
							IF bDebug_DoDistanceChecksForAllPoolPeds
								FLOAT fTempStealthDist
								fTempStealthDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sPoolPed[i].pedIndex))
								IF (fTempStealthDist < fDebug_DistCheckForStealth_PoolPedID[i])
									SWITCH i
										CASE 0
											DRAW_RECT(0.15, 0.1, 0.1, 0.1, 0, 0, 100, 100)	//BLUE SWIM MALE
										BREAK
										CASE 1
											DRAW_RECT(0.25, 0.1, 0.1, 0.1, 100, 0, 0, 100)	//RED HOT_TUB FEMALE	right as looking at them
										BREAK
										CASE 2
											DRAW_RECT(0.35, 0.1, 0.1, 0.1, 255, 216, 0, 100)	//YELLOW HOT_TUB FEMALE		left as looking at them
										BREAK
										CASE 3
											DRAW_RECT(0.45, 0.1, 0.1, 0.1, 0, 100, 0, 100)	//GREEN	HOT_TUB MALE
										BREAK
										DEFAULT
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG STEALTH : INVALID PED ID") ENDIF #ENDIF
										BREAK
									ENDSWITCH
								ENDIF
								IF bDebug_PrintDistChecksForAllPoolPeds
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STEALTH CHECKER - PED ID HAS CLEAR LOS TO PLAYER - ", i) ENDIF #ENDIF
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STEALTH CHECKER - AT DISTANCE OF - ", fTempStealthDist) ENDIF #ENDIF
									bDebug_PrintDistChecksForAllPoolPeds = FALSE
								ENDIF
							ELSE
								SWITCH i
									CASE 0
										DRAW_RECT(0.15, 0.1, 0.1, 0.1, 0, 0, 100, 100)	//BLUE SWIM MALE
									BREAK
									CASE 1
										DRAW_RECT(0.25, 0.1, 0.1, 0.1, 100, 0, 0, 100)	//RED HOT_TUB FEMALE
									BREAK
									CASE 2
										DRAW_RECT(0.35, 0.1, 0.1, 0.1, 255, 216, 0, 100)	//YELLOW HOT_TUB FEMALE
									BREAK
									CASE 3
										DRAW_RECT(0.45, 0.1, 0.1, 0.1, 0, 100, 0, 100)	//GREEN	HOT_TUB MALE
									BREAK
									DEFAULT
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG STEALTH : INVALID PED ID") ENDIF #ENDIF
									BREAK
								ENDSWITCH
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STEALTH CHECKER - PED ID HAS CLEAR LOS TO PLAYER no dist check  - ", i) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF bDebug_DoIndividualStealthCheck
					IF IS_ENTITY_ALIVE(sPoolPed[iDebug_PoolPedID_StealthCheck].pedIndex)
						IF CAN_PED_SEE_HATED_PED(sPoolPed[iDebug_PoolPedID_StealthCheck].pedIndex, PLAYER_PED_ID())					
							IF bDebug_DoIndividualDistanceCheck
								FLOAT fTempStealthDist
								fTempStealthDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sPoolPed[iDebug_PoolPedID_StealthCheck].pedIndex))
								IF (fTempStealthDist < fDebug_DistCheckForStealth_PoolPedID[iDebug_PoolPedID_StealthCheck])
									SWITCH iDebug_PoolPedID_StealthCheck
										CASE 0
											DRAW_RECT(0.15, 0.1, 0.1, 0.1, 0, 0, 100, 100)	//BLUE SWIM MALE
										BREAK
										CASE 1
											DRAW_RECT(0.25, 0.1, 0.1, 0.1, 100, 0, 0, 100)	//RED HOT_TUB FEMALE
										BREAK
										CASE 2
											DRAW_RECT(0.35, 0.1, 0.1, 0.1, 255, 216, 0, 100)	//YELLOW HOT_TUB FEMALE
										BREAK
										CASE 3
											DRAW_RECT(0.45, 0.1, 0.1, 0.1, 0, 100, 0, 100)	//GREEN	HOT_TUB MALE
										BREAK
										DEFAULT
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG STEALTH : INVALID PED ID") ENDIF #ENDIF
										BREAK
									ENDSWITCH
								ENDIF	
							ELSE
								SWITCH iDebug_PoolPedID_StealthCheck
									CASE 0
										DRAW_RECT(0.15, 0.1, 0.1, 0.1, 0, 0, 100, 100)	//BLUE SWIM MALE
									BREAK
									CASE 1
										DRAW_RECT(0.25, 0.1, 0.1, 0.1, 100, 0, 0, 100)	//RED HOT_TUB FEMALE
									BREAK
									CASE 2
										DRAW_RECT(0.35, 0.1, 0.1, 0.1, 255, 216, 0, 100)	//YELLOW HOT_TUB FEMALE
									BREAK
									CASE 3
										DRAW_RECT(0.45, 0.1, 0.1, 0.1, 0, 100, 0, 100)	//GREEN	HOT_TUB MALE
									BREAK
									DEFAULT
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG STEALTH : INVALID PED ID") ENDIF #ENDIF
									BREAK
								ENDSWITCH
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STEALTH CHECKER - PED ID HAS CLEAR LOS TO PLAYER no dist check - ", iDebug_PoolPedID_StealthCheck) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			IF bDebug_DoGardenerStealthCheck
				IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					IF CAN_PED_SEE_HATED_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
						FLOAT fTempStealthDist
						fTempStealthDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sGardenerPed.pedIndex))
						IF (fTempStealthDist < fDebug_DistCheckGardenerSpottedRange)
							DRAW_RECT(0.55, 0.1, 0.1, 0.1, 178, 0, 255, 50)	//PURPLE GARDENER SPOTTED
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bDebug_DoGardenerSeeHatedPedsCheck
				IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					SET_PED_SEEING_RANGE(sGardenerPed.pedIndex, fDebug_GardenerSeeingRange)
					SET_PED_HEARING_RANGE(sGardenerPed.pedIndex, fDebug_GardenerHearingRange)
										
					IF CAN_PED_SEE_HATED_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
						DRAW_RECT(0.65, 0.1, 0.1, 0.1, 178, 20, 200, 60)
					ENDIF
				ENDIF
			ENDIF				
			
			IF bDebug_ToggleDoIsPlayerInWaterCheck
				IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - player is in water		++++++++++++++") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - player is NOT!! in water	---------------") ENDIF #ENDIF
				ENDIF
			ENDIF			
			IF bDebug_UnPauseGame
				SET_GAME_PAUSED(FALSE)
				bDebug_UnPauseGame = FALSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GAME UNPAUSED") ENDIF #ENDIF
			ENDIF			
			IF bDebug_DoPlayerStealthModeCheck
				IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "^^^ player in stealth mode  ^^^^^") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "^^^ player NOT in stealth mode  ^^^^^") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

	/// PURPOSE:
	/// removes my debug mission widget group
	PROC CLEANUP_MISSION_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:FUNCTIONS AND PROCEEDURES
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    remove object from the game
/// PARAMS:
///    objectIndex - object to remove
///    bDelete - if true delete it, else set as no longer needed
PROC SAFE_REMOVE_OBJECT(OBJECT_INDEX &objectIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_OBJECT(objectIndex)
	ELSE
		SAFE_RELEASE_OBJECT(objectIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes vehicle from the world
/// PARAMS:
///    vehIndex - vehicle to be removed
///    bDelete - if true AND player isn't inside it delete it, else set as no longer needed
PROC SAFE_REMOVE_VEHICLE(VEHICLE_INDEX &vehIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_VEHICLE(vehIndex)
	ELSE
		SAFE_RELEASE_VEHICLE(vehIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes ped from the game
/// PARAMS:
///    ped - ped to be removed
///    bDelete - if true delete it, else set as no longer needed
///    bDetach - if true ped will be detached if attached, and no in a vehicle or getting into a vehicle
PROC SAFE_REMOVE_PED(PED_INDEX &ped, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_PED(ped)
	ELSE
		SAFE_RELEASE_PED(ped)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a ped to a new location
/// PARAMS:
///    pedIndex - ped to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the ped at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
///    bForcePedAiAndAnimUpdate - if true FORCE_PED_AI_AND_ANIMATION_UPDATE is applied on the ped
PROC SAFE_TELEPORT_PED(PED_INDEX pedIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bForcePedAiAndAnimUpdate = FALSE)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF bSnapToGround
			vPosition.Z = INVALID_WORLD_Z
		ENDIF
		SET_ENTITY_COORDS(pedIndex, vPosition)
		SET_ENTITY_HEADING(pedIndex, fHeading)
		IF bForcePedAiAndAnimUpdate
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a vehicle to a new location
/// PARAMS:
///    vehIndex - vehicle to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the vehicle at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
PROC SAFE_TELEPORT_VEHICLE(VEHICLE_INDEX &vehIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE)
	IF IS_ENTITY_ALIVE(vehIndex)
		IF bSnapToGround
			vPosition.Z = INVALID_WORLD_Z
		ENDIF
		SET_ENTITY_COORDS(vehIndex, vPosition)
		SET_ENTITY_HEADING(vehIndex, fHeading)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a blip of the correct size and colour for an object being used as a pickup
/// PARAMS:
///    vPos - where to create the blip
///    bBlipPriority - priority of the blip
///    bDisplayRoute - do we want to add a route for this blip?
/// RETURNS:
///   the blip index of the new blip (null if blip was not created
FUNC BLIP_INDEX CREATE_OBJECT_PICKUP_BLIP(ENTITY_INDEX EntityIndex, BLIP_PRIORITY bBlipPriority=BLIPPRIORITY_MED)
	BLIP_INDEX bRetBlip = NULL
	
	IF IS_ENTITY_ALIVE(EntityIndex)
		bRetBlip = ADD_BLIP_FOR_ENTITY(EntityIndex)
		IF DOES_BLIP_EXIST(bRetBlip)
			SET_BLIP_PRIORITY(bRetBlip,bBlipPriority)
			SET_BLIP_SCALE(bRetBlip, BLIP_SIZE_PICKUP)
		ENDIF
	ENDIF
	RETURN bRetBlip
ENDFUNC

/// PURPOSE:
///    check if iTimeAmount has passed since iTimer was set
/// PARAMS:
///    iTimer - the timer
///    iTimeAmount - the amount of time to check has passed
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimer, INT iTimeAmount)
	RETURN (GET_GAME_TIMER() - iTimer) > iTimeAmount
ENDFUNC

///    Check is the specified frame count has passed using specified timer.
/// PARAMS:
///    iFrameCounterTimer - Frame Count to check.
///    iFrameAmount - Frame Count to check against.
/// RETURNS:
///    True is the specified amount of iFrameAmount has passed for the specified iFrameCounterTimer.
FUNC BOOL HAS_FRAME_COUNTER_PASSED(INT iFrameCounterTimer, INT iFrameAmount)
	RETURN (GET_FRAME_COUNT() - iFrameCounterTimer) > iFrameAmount
ENDFUNC

/// PURPOSE:
///    simple Check to see if entity is on screen and not occluded
/// PARAMS:
///    entityIndex - entity to test
///    bCheckIsAlive - if TRUE func will test entity exists and is alive before proceeding
/// RETURNS:
///    True if entity is visible on screen
FUNC BOOL IS_ENTITY_VISIBLE_ON_SCREEN(ENTITY_INDEX entityIndex, BOOL bCheckIsAlive = TRUE)
	IF bCheckIsAlive
		IF NOT IS_ENTITY_ALIVE(entityIndex)
			RETURN FALSE
		ENDIF	
	ENDIF
	IF IS_ENTITY_ON_SCREEN(entityIndex)
		IF NOT IS_ENTITY_OCCLUDED(entityIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Test for if two coords are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN VDIST2(v1, v2) <= (fRange*fRange)
ENDFUNC

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    Performs the CAN_PED_SEE_PED check and sets the specified FrameCounter if it returned TRUE
///    counter is used as a grace period for checking the ped has seen the player, as CAN_PED_SEE_PED takes several frames to return
/// PARAMS:
///    pedIndex
PROC UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(PED_INDEX pedIndex, INT &iFrameCountLastSeenPlayer)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF CAN_PED_SEE_PED(pedIndex, PLAYER_PED_ID())
			iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER - CAN_PED_SEE_PED returned TRUE so set frame count to : ", iFrameCountLastSeenPlayer) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    return a IndexID for a pool ped who is ok to speak
///    checks ped being alive and within range
/// PARAMS:
///    FLOAT fMaxDistForConvo - dist between player and ped for ped to be returned suitable, if -1.0 check is ignored
///    VECTOR vPlayerCoords - player's current position, ignored if fMaxDistForConvo test if not to be performed 
/// RETURNS:
///    INT - indexID for the poolped array
FUNC INT GET_RANDOM_OK_POOL_PED_TO_SPEAK(VECTOR vPlayerCoords, FLOAT fMaxDistForConvo = -1.0)
	INT i
	// random pick a ped
	i = GET_RANDOM_INT_IN_RANGE(0, TOTAL_POOL_PEDS)
	IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
		IF fMaxDistForConvo = -1.0
			RETURN i
		ELSE
			IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vPlayerCoords, fMaxDistForConvo)
				RETURN i
			ENDIF
		ENDIF
	ENDIF
	
	// if failed find the first avaliable ped which suits
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
			IF fMaxDistForConvo = -1.0
				RETURN i
			ELSE
				IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vPlayerCoords, fMaxDistForConvo)
					RETURN i
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

/// PURPOSE:
///    trigger a music event, whilst first checking it has been prepared (if required)
/// PARAMS:
///    sMusicEvent - the music event name
///    bRequiresPrepare - if TRUE PREPARE_MUSIC_EVENT is setup before the event can be trigger.  Check the doc to see if an event needs preloading
/// RETURNS:
///    TRUE if the event is trigger successfully
FUNC BOOL SAFE_TRIGGER_MISSION_MUSIC_EVENT(STRING sMusicEvent, BOOL bRequiresPrepare = FALSE)
	IF bRequiresPrepare
		IF NOT PREPARE_MUSIC_EVENT(sMusicEvent)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning FALSE, not yet prepared for = ", sMusicEvent) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	IF TRIGGER_MUSIC_EVENT(sMusicEvent)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning TRUE for = ", sMusicEvent) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Register a new door with the door system
///    NOTE: code taken from Lester1A.sc
/// PARAMS:
///    eDoor - The specific door to register taken from N1B_MISSION_CONTROLLED_DOORS_ENUM
///    mnModel - the door's model name 
///    vPos - the door's position
PROC REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor, MODEL_NAMES mnModel, VECTOR vPos)
	TEXT_LABEL_23	str_hash = "N1B_"
	str_hash += ENUM_TO_INT(eDoor)
	iMissionControlledDoors[eDoor] = GET_HASH_KEY(str_hash)
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		ADD_DOOR_TO_SYSTEM(iMissionControlledDoors[eDoor], mnModel, vPos, FALSE, TRUE)	// note: useOldOverrides parameter must be set to FALSE! Default is TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REGISTER_NEW_MISSION_CONTROLLED_DOOR - door added to the system : ", str_hash, " hash key : ", iMissionControlledDoors[eDoor], " at pos : ", vPos) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the state of a mission controlled door which has been registered with the door system
/// PARAMS:
///    eDoor - specific door to set state taken from N1B_MISSION_CONTROLLED_DOORS_ENUM
///    eDoorState - the new door state
///    bFlushState - this should only be set true for the last command called on a door in the single frame, BUT also this should be the last call to update a door so set to TRUE by default
/// RETURNS:
///    True if door state was successfully set
FUNC BOOL SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor, DOOR_STATE_ENUM eDoorState, BOOL bFlushState = TRUE)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		DOOR_SYSTEM_SET_DOOR_STATE(iMissionControlledDoors[eDoor], eDoorState, FALSE, bFlushState)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_CONTROLLED_DOOR_STATE - RETURN true for door :", eDoor, " set state : ", eDoorState) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    get the door's state
/// RETURNS:
///    the doors state - DOOR_STATE_ENUM
FUNC DOOR_STATE_ENUM GET_DOOR_STATE(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor)
	DOOR_STATE_ENUM eCurrentState
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		eCurrentState = DOOR_SYSTEM_GET_DOOR_STATE(iMissionControlledDoors[eDoor])
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_DOOR_STATE - for door :", eDoor, " state : ", eCurrentState) ENDIF #ENDIF
	ENDIF
	RETURN eCurrentState
ENDFUNC

/// PURPOSE:
///    Set the open ratio and hold open flag on a mission controlled door which has been registered with the door system
/// PARAMS:
///    eDoor - specific door to set state taken from N1B_MISSION_CONTROLLED_DOORS_ENUM
///    fOpenRatio - 0.0 = shut, 1.0 = fully open
///    bSetHoldOpen - if the door is to hold it's open state
///    bFlushState - this should only be set true for the last command called on a door in the single frame.
/// RETURNS:
///    True if door open state was successfully set
FUNC BOOL SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor, FLOAT fOpenRatio = 0.0, BOOL bSetHoldOpen = FALSE, BOOL bFlushState = FALSE)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])		
		DOOR_SYSTEM_SET_HOLD_OPEN(iMissionControlledDoors[eDoor], bSetHoldOpen)
		DOOR_SYSTEM_SET_OPEN_RATIO(iMissionControlledDoors[eDoor], fOpenRatio, FALSE, bFlushState)		
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO - RETURN true for door :", eDoor, " ratio : ", fOpenRatio, " bSetHoldOpen :", bSetHoldOpen) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the specific mission controlled door which has been registered with the door system is open
/// PARAMS:
///    eDoor - which specific door to test
/// RETURNS:
///    TRUE if the door's open
FUNC BOOL IS_DOOR_OPEN(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor) //, FLOAT fClosedThreshold = 0.1)	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		/*// this doesn't currently work! DOOR_SYSTEM_GET_OPEN_RATIO will not update from the player pushing open the door.  Added bug to get this sorted - B*
		FLOAT fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[eDoor])
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DOOR_OPEN - for door :", eDoor, " ratio : ", fOpenRatio) ENDIF #ENDIF
		IF fOpenRatio > fClosedThreshold
		OR fOpenRatio < -fClosedThreshold
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DOOR_OPEN - RETURN true for door :", eDoor, " ratio : ", fOpenRatio) ENDIF #ENDIF
			RETURN TRUE
		ENDIF */
		IF NOT IS_DOOR_CLOSED(iMissionControlledDoors[eDoor])
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DOOR_OPEN - RETURN true for door :", eDoor) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
/*
/// PURPOSE:
///    Check if the specific mission controlled door which has been registered with the door system is not streamed in, but it's state is pending
/// PARAMS:
///    eDoor - which specific door to test
/// RETURNS:
///    TRUE if the door's pending state is DOORSTATE_INVALID then it's not streamed in but a request to update its state when it does stream in has gone through
///    NOTE: initialised doors are set to DOORSTATE_INVALID so best to call set state first then check with this
FUNC BOOL IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[eDoor])
		IF DOOR_SYSTEM_GET_DOOR_PENDING_STATE(iMissionControlledDoors[eDoor]) = DOORSTATE_INVALID	// this means my call to set the state on an unstreamed door has gone through apparently.
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DOOR_IN_LOCKED_STATE - RETURN true for door :", eDoor, " door not streamed in yet but pending state has been set") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
*/
/// PURPOSE:
///    Check if the specific mission controlled door which has been registered with the door system is in locked state 
///    only returns correctly if the door is streamed in. 
/// PARAMS:
///    eDoor - which specific door to test
/// RETURNS:
///    TRUE if the door's state is DOORSTATE_LOCKED or DOORSTATE_FORCE_LOCKED_THIS_FRAME
FUNC BOOL IS_DOOR_IN_LOCKED_STATE(N1B_MISSION_CONTROLLED_DOORS_ENUM eDoor) 	
	DOOR_STATE_ENUM eTempState
	eTempState = GET_DOOR_STATE(eDoor)
	IF eTempState = DOORSTATE_LOCKED
	OR eTempState = DOORSTATE_FORCE_LOCKED_THIS_FRAME	// this is used to snap the door to the locked state
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_DOOR_IN_LOCKED_STATE - RETURN true for door :", eDoor, " streamed in and locked") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    set the locked state of the double gates, if they haven't already been opened by the player once
/// PARAMS:
///    bSetLocked - if TRUE attempt to lock gates, else attempt to unlock gates
PROC SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(BOOL bSetLocked = TRUE)
	DOOR_STATE_ENUM eNewLockedState = DOORSTATE_UNLOCKED
	IF bSetLocked
		eNewLockedState = DOORSTATE_LOCKED
	ENDIF	
	//unlock the gates up if they haven't been opened by the player
	IF NOT bHaveNorthGatesBeenOpenedByPlayer
		SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_01, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_NORTH_GATE_01]), FALSE, FALSE)
		SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_01, eNewLockedState)
		SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_02, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_NORTH_GATE_02]), FALSE, FALSE)
		SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_02, eNewLockedState)		
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_LOCKED_STATE_ON_THE_HOUSE_DOUBLE_GATES north gates set : ", eNewLockedState) ENDIF #ENDIF
	ENDIF
	IF NOT bHaveSouthGatesBeenOpenedByPlayer		
		SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_01, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_SOUTH_GATE_01]), FALSE, FALSE)
		SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_01, eNewLockedState)		
		SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_02, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_SOUTH_GATE_02]), FALSE, FALSE)
		SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_02, eNewLockedState)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_LOCKED_STATE_ON_THE_HOUSE_DOUBLE_GATES south gates set : ", eNewLockedState) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    monitors the north and south double gates open ratio, if one becomes open set the ajacent one to tbe open
PROC CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED()
	//lock the gates up if they haven't been opened when the player leaves the garden
	IF NOT bHaveNorthGatesBeenOpenedByPlayer				
		IF IS_DOOR_OPEN(N1B_MCD_NORTH_GATE_01)
		OR IS_DOOR_OPEN(N1B_MCD_NORTH_GATE_02)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED north gates opened ") ENDIF #ENDIF
			bHaveNorthGatesBeenOpenedByPlayer = TRUE
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED north gates not opened ") ENDIF #ENDIF
		ENDIF
	ENDIF
	IF NOT bHaveSouthGatesBeenOpenedByPlayer
		IF IS_DOOR_OPEN(N1B_MCD_SOUTH_GATE_01)
		OR IS_DOOR_OPEN(N1B_MCD_SOUTH_GATE_02)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED south gates opened ") ENDIF #ENDIF
			bHaveSouthGatesBeenOpenedByPlayer = TRUE
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED south gates not opened ") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if all the house peds are dead or injured
/// RETURNS:
///    true if pool peds and gardener are
FUNC BOOL ARE_ALL_POOL_PEDS_AND_GARDENER_DEAD_OR_INJURED()
	INT i	
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ARE_ALL_POOL_PEDS_AND_GARDENER_DEAD_OR_INJURED - returned FALSE pool ped uninjured TD : ", i) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDFOR
	IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ARE_ALL_POOL_PEDS_AND_GARDENER_DEAD_OR_INJURED - returned FALSE gardener ped uninjured TD") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Check if any of the pool peds have been killed or are classed as injured
/// RETURNS:
/// true id a pool ped has been killed / injured   
FUNC BOOL HAVE_ANY_POOL_PEDS_DIED_OR_INJURED_STATE()
	INT i	
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		IF DOES_ENTITY_EXIST(sPoolPed[i].pedIndex)
			IF IS_ENTITY_DEAD(sPoolPed[i].pedIndex)
			OR IS_PED_INJURED(sPoolPed[i].pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAVE_ANY_POOL_PEDS_DIED_OR_INJURED_STATE - returned TRUE") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    reset's the STRUCT_MISSION_PED's iTimer and iFrameCountLastSeenPlayer
PROC RESET_MISSION_PED_TIMERS()
	INT i 
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		sPoolPed[i].iTimer = 0
		sPoolPed[i].iAiDelayTimer = 0
		sPoolPed[i].iFrameCountLastSeenPlayer = 0		
	ENDFOR
	sGardenerPed.iTimer = 0
	sGardenerPed.iAiDelayTimer = 0
	sGardenerPed.iFrameCountLastSeenPlayer = 0		
ENDPROC

/// PURPOSE:
///    check the state of the restore conversation manager
/// PARAMS:
///    tl23SpecificRootLabel - specific root to check, pass "NULL" or "" if it doesn't matter which root
/// RETURNS:
///    TRUE if convo is waiting to restore
FUNC BOOL IS_CONVERSATION_TRYING_TO_BE_RESTORED(STRING sSpecificRootLabel)
	IF eRestoreConversationState = N1B_CONV_RESTART_WITHOUT_SUBS
	OR eRestoreConversationState =  N1B_CONV_RESTART_WITH_SUBS
		IF ARE_STRINGS_EQUAL(sSpecificRootLabel, "NULL")
		OR ARE_STRINGS_EQUAL(sSpecificRootLabel, "")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_CONVERSATION_TRYING_TO_BE_RESTORED return TRUE standard check : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ELSE
			IF ARE_STRINGS_EQUAL(sSpecificRootLabel, tSavedConversationRoot)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_CONVERSATION_TRYING_TO_BE_RESTORED return TRUE specific root check = ", sSpecificRootLabel, " : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_CONVERSATION_TRYING_TO_BE_RESTORED  return FALSE : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check the state of the restore conversation manager for a specific label
/// PARAMS:
///    tl23SpecificLabel - specific label to check
/// RETURNS:
///    TRUE if convo is waiting to restore
FUNC BOOL IS_CONVERSATION_LABEL_TRYING_TO_BE_RESTORED(STRING sSpecificLabel)
	IF eRestoreConversationState = N1B_CONV_RESTART_WITHOUT_SUBS
	OR eRestoreConversationState =  N1B_CONV_RESTART_WITH_SUBS
		IF ARE_STRINGS_EQUAL(sSpecificLabel, tSavedConversationRoot)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY  CPRINTLN(DEBUG_MISSION, "IS_CONVERSATION_LABEL_TRYING_TO_BE_RESTORED return TRUE specific label check = ", sSpecificLabel, " : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_CONVERSATION_LABEL_TRYING_TO_BE_RESTORED  return FALSE : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
///    also checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT
///    can return "NULL" the first few frames after NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK has returned true
/// PARAMS:
///    tl23ConversationRoot - the conversation root to test
///    bTestRestoreConversations - if TRUE test to see if sConversationRoot is waiting to be restored, if so return TRUE
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a root matching the passed in string or "NULL"
FUNC BOOL IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(STRING sConversationRoot, BOOL bTestRestoreConversations = TRUE)
	TEXT_LABEL_23 tlTempRoot	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
		//if current root matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempRoot, sConversationRoot)
			RETURN TRUE
		ENDIF		
		//if current root matches null, return true
		//because if the correct root isn't returned the first frame or so after NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK has return true.
		IF ARE_STRINGS_EQUAL(tlTempRoot, "NULL")
			RETURN TRUE
		ENDIF
	ENDIF
	IF bTestRestoreConversations
		IF IS_CONVERSATION_TRYING_TO_BE_RESTORED(sConversationRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
///    also checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL
///    can return "NULL" the first few frames after NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK has returned true
/// PARAMS:
///    sConversationLabel - the conversation label to test
///    bTestRestoreConversations - if TRUE test to see if sConversationLabel is waiting to be restored, if so return TRUE
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a label matching the passed in string or "NULL"
FUNC BOOL IS_SPECIFIC_CONVERSATION_LABEL_CURRENTLY_PLAYING(STRING sConversationLabel, BOOL bTestRestoreConversations = TRUE)
	TEXT_LABEL_23 tlTempLabel	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()		
		//if current label matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempLabel, sConversationLabel)
			RETURN TRUE
		ENDIF		
		//if current label matches null, return true
		//because if the correct root isn't returned the first frame or so after NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK has return true.
		IF ARE_STRINGS_EQUAL(tlTempLabel, "NULL")
			RETURN TRUE
		ENDIF
	ENDIF
	IF bTestRestoreConversations
		IF IS_CONVERSATION_LABEL_TRYING_TO_BE_RESTORED(sConversationLabel)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If a scripted conversation is in progress and god text is printed, this will stop the check to avoid the dialogue subtitles overwriting the god text
PROC NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(TEXT_LABEL_23&tConversationRoot, TEXT_LABEL_23 &tConversationLabel)

	IF eRestoreConversationState != N1B_CONV_IDLE
		CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: Stopping check for conversation and objective text conflicts : FC = ", GET_FRAME_COUNT())
		tConversationRoot = "NULL"
		tConversationLabel = "NULL"
		eRestoreConversationState = N1B_CONV_IDLE
	ENDIF
ENDPROC

/// PURPOSE:
///    If a scripted conversation is in progress and god text is printed, this will begin the check to avoid the dialogue subtitles overwriting the god text
PROC NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW()

	IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0 // Don't do any checking if the user isn't displaying conversation subtitles anyway
	AND eRestoreConversationState != N1B_CONV_STOP_CURRENT_CONV
	AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() // IS_SCRIPTED_CONVERSATION_ONGOING()
		CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW - Starting check for conversation and objective text conflicts : FC = ", GET_FRAME_COUNT())
		eRestoreConversationState = N1B_CONV_STOP_CURRENT_CONV
	ELSE
		CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW - no conflicts found : FC = ", GET_FRAME_COUNT())
	ENDIF
ENDPROC

/// PURPOSE:
///    Attempts to start dialogue using NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK, checks that the subtitles are off in the profile settings and starts the convo without subtitles if objective text is currently being displayed
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
///    cloneConversation - ?
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	
	BOOL bSwitchedSubtitlesOffForObjective = FALSE
	
	// if we are supposed to display subtitles, check for objective text currently being displayed, if so start the convo without subtitles
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0	// Don't check if the user isn't displaying conversation subtitles anyway
			IF IS_MESSAGE_BEING_DISPLAYED()
				bSwitchedSubtitlesOffForObjective = TRUE
				ShouldDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES
			ENDIF
		ENDIF
	ENDIF
	
	IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
		// set the convo and objective text conflict manager's state
		IF bSwitchedSubtitlesOffForObjective
			eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT
			CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT : FC = ", GET_FRAME_COUNT())
		ELSE
			NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
			CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = ", tSavedConversationRoot, " tSavedConversationLabel = ", tSavedConversationLabel, " : FC = ", GET_FRAME_COUNT())
		ENDIF
		CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: Return TRUE : rootLabel = ", WhichRootLabel, " : FC = ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK, checks that the subtitles are off in the profile settings and starts the convo without subtitles if objective text is currently being displayed
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - Which line of dialogue to start from
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	
	BOOL bSwitchedSubtitlesOffForObjective = FALSE
	
	// if we are supposed to display subtitles, check for objective text currently being displayed, if so start the convo without subtitles
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0	// Don't check if the user isn't displaying conversation subtitles anyway
			IF IS_MESSAGE_BEING_DISPLAYED()
				bSwitchedSubtitlesOffForObjective = TRUE
				ShouldDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES
			ENDIF
		ENDIF
	ENDIF

	IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		// set the convo and objective text conflict manager's state
		IF bSwitchedSubtitlesOffForObjective
			eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT
			CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT : FC = ", GET_FRAME_COUNT())
		ELSE
			NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
			CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = N1B_CONV_IDLE : FC = ", GET_FRAME_COUNT())
		ENDIF
		CPRINTLN(DEBUG_MISSION, "NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK: Return TRUE : rootLabel = ", WhichRootLabel,
																			" : WhichSpecificLabel = ", WhichSpecificLabel, " : FC = ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK, checks that the subtitles are off in the profile settings and starts the convo without subtitles if objective text is currently being displayed
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - the specific line we want to play
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen 
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	
	BOOL bSwitchedSubtitlesOffForObjective = FALSE
	
	// if we are supposed to display subtitles, check for objective text currently being displayed, if so start the convo without subtitles
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0	// Don't check if the user isn't displaying conversation subtitles anyway
			IF IS_MESSAGE_BEING_DISPLAYED()
				bSwitchedSubtitlesOffForObjective = TRUE
				ShouldDisplaySubtitles = DO_NOT_DISPLAY_SUBTITLES
			ENDIF
		ENDIF
	ENDIF

	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		// set the convo and objective text conflict manager's state
		IF bSwitchedSubtitlesOffForObjective
			eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT
			CPRINTLN(DEBUG_MISSION, "NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT : FC = ", GET_FRAME_COUNT())
		ELSE
			NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
			CPRINTLN(DEBUG_MISSION, "NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: convo created : eRestoreConversationState = N1B_CONV_IDLE : FC = ", GET_FRAME_COUNT())
		ENDIF
		CPRINTLN(DEBUG_MISSION, "NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK: Return TRUE : rootLabel = ", WhichRootLabel,
																			" : WhichSpecificLabel = ", WhichSpecificLabel, " : FC = ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If a scripted conversation is in progress and god text is printed, this will handle stopping & restarting the conversation without subtitles to prevent overwriting the god text, then stopping & restarting with subtitles after the text has cleared.
PROC NIG1B_HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(structPedsForConversation &sConversationToUse, STRING sTextBlockToUse, TEXT_LABEL_23&tConversationRoot, TEXT_LABEL_23 &tConversationLabel)

	SWITCH eRestoreConversationState
		CASE N1B_CONV_IDLE
		BREAK
		CASE N1B_CONV_STOP_CURRENT_CONV
			IF IS_SCRIPTED_CONVERSATION_ONGOING()
				tConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				tConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
				IF ARE_STRINGS_EQUAL(tConversationLabel, "")
				OR ARE_STRINGS_EQUAL(tConversationLabel, "NULL")
					//#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_STOP_CURRENT_CONV - Already last line of the conversation so not stopping it : FC = ", GET_FRAME_COUNT()) #ENDIF
					//eRestoreConversationState = N1B_CONV_IDLE
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_STOP_CURRENT_CONV - Stopping and storing conversation ", tConversationRoot, " : FC = ", GET_FRAME_COUNT()) #ENDIF
					KILL_ANY_CONVERSATION()
					eRestoreConversationState = N1B_CONV_RESTART_WITHOUT_SUBS
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_STOP_CURRENT_CONV - no scripted convo ongoing : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE N1B_CONV_RESTART_WITHOUT_SUBS
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF IS_MESSAGE_BEING_DISPLAYED() // Re-check to see if the god text has since gone offscreen
					IF NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sConversationToUse, sTextBlockToUse, tConversationRoot, tConversationLabel, CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - Restoring conversation ", tConversationRoot, " from label ", tConversationLabel, " without subtitles : FC = ", GET_FRAME_COUNT()) #ENDIF
						eRestoreConversationState = N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - God text removed before last line of dialogue was finished, so just restart with subtitles : FC = ", GET_FRAME_COUNT()) #ENDIF
					eRestoreConversationState = N1B_CONV_RESTART_WITH_SUBS
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - no convo ongoing or queued : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
			AND IS_SCRIPTED_CONVERSATION_ONGOING()
				tConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				tConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
				IF ARE_STRINGS_EQUAL(tConversationLabel, "")
				OR ARE_STRINGS_EQUAL(tConversationLabel, "NULL")
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - Already last line of the conversation so not stopping it") #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - Stopping and storing conversation ", tConversationRoot) #ENDIF
					KILL_ANY_CONVERSATION()
					eRestoreConversationState = N1B_CONV_RESTART_WITH_SUBS
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTORE_WAIT_FOR_NO_TEXT - mesaage being displayed or no convo ongoing this frame FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
		CASE N1B_CONV_RESTART_WITH_SUBS
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sConversationToUse, sTextBlockToUse, tConversationRoot, tConversationLabel, CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITHOUT_SUBS - Restoring conversation ", tConversationRoot, " from label ", tConversationLabel, " with subtitles : FC = ", GET_FRAME_COUNT()) #ENDIF
				eRestoreConversationState = N1B_CONV_IDLE
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B CONV CONFLICTS: N1B_CONV_RESTART_WITH_SUBS - crete convo failed this frame or convo still ongoing or queued : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    makes call to kill the current conversation and reset the convo/objective text conflict state
/// PARAMS:
///    bFinishCurrentLine - if true current dialogue line will be allowed to play out
///    bKillPhoneConversation - if true KILL_PHONE_CONVERSATION() is called
PROC N1B_KILL_CURRENT_CONVERSATION(BOOL bFinishCurrentLine = TRUE, BOOL bKillPhoneConversation = FALSE)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF bFinishCurrentLine
			KILL_ANY_CONVERSATION()
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
	IF bKillPhoneConversation
		KILL_PHONE_CONVERSATION()
	ENDIF
	NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
ENDPROC

/// PURPOSE:
///    Test the float values are similar with in the specified threshold
/// PARAMS:
///    v1 - 
///    v2 - 
///    fTolerance - 
/// RETURNS:
///    TRUE if the difference in the float values is less than or equal to fTolerance
FUNC BOOL ARE_FLOAT_VALUES_SIMILAR(FLOAT f1, FLOAT f2, FLOAT fTolerance)
	FLOAT fDiff = ABSF(f1 - f2)
	IF fDiff <= fTolerance
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
/// takes care of detaching the props I spawn in the back of the gardener's van
///    for example if the player starts driving the vehicle away
PROC DETACH_PROPS_INSIDE_GARDENER_VEHICLE()
	IF NOT bDetachedPropsInGardenVehicle
		IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
			IF IS_ENTITY_ATTACHED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
				IF NOT IS_ENTITY_ALIVE(vehGardenerVehicle)
					DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					SET_ENTITY_DYNAMIC(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
					//APPLY_FORCE_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, APPLY_TYPE_EXTERNAL_IMPULSE, << 0.0, 0.0, -1.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DETACH_PROPS_INSIDE_GARDENER_VEHICLE - bDetachedPropsInGardenVehicle true - vehicles is no good") ENDIF #ENDIF
				ELSE
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_VELOCITY(vehGardenerVehicle), << 0.0, 0.0, 0.0 >>, 5.0)
						DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
						SET_ENTITY_DYNAMIC(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
						SET_ENTITY_CAN_BE_DAMAGED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
						APPLY_FORCE_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, APPLY_TYPE_EXTERNAL_IMPULSE, << 0.0, 1.5, 0.2 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DETACH_PROPS_INSIDE_GARDENER_VEHICLE - bDetachedPropsInGardenVehicle true - vehicle moved") ENDIF #ENDIF
					ENDIF
				ENDIF
			ELSE
				//SET_ENTITY_CAN_BE_DAMAGED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
				//SET_ENTITY_DYNAMIC(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, TRUE)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DETACH_PROPS_INSIDE_GARDENER_VEHICLE - bDetachedPropsInGardenVehicle true - entity already detached") ENDIF #ENDIF
				//bDetachedPropsInGardenVehicle = TRUE
			ENDIF
		ENDIF			
	ENDIF
ENDPROC

/// PURPOSE:
///    used to check which side of a position is a ped
///    NOTE: doesn't check ped is alive / valid
/// PARAMS:
///    pedIndex - ped to test
///    vPos - position to test 
/// RETURNS:
///    Int depicting the side of the ped the pos is.  -1 = Left side, 1 = Right side, 0 = directly in line
FUNC INT GET_SIDE_POS_IS_TO_PED(PED_INDEX pedIndex, VECTOR vPos)
	VECTOR vRightOffset
	vRightOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedIndex, << 3.0, 0.0, 0.0 >>)
	VECTOR vPedCoords = GET_ENTITY_COORDS(pedIndex)
	vRightOffset -= vPedCoords
	NORMALISE_VECTOR(vRightOffset)
	FLOAT fSide = DOT_PRODUCT(vPos - vPedCoords, vRightOffset)
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, DEBUG_MISSION, "GET_SIDE_POS_IS_TO_PED returning - ", fSide) ENDIF #ENDIF
	IF (fSide < 0)
    	RETURN 0	//left side
	ELIF (fSide > 0)
		RETURN 1	//right side
	ENDIF
	RETURN -1	// directly in line
ENDFUNC

/// PURPOSE:
///    convert a TEXT_LABEL into type STRING
/// PARAMS:
///    tlTextLabel - the text label to convert
/// RETURNS:
///    STRING
FUNC STRING N1B_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(STRING tlTextLabel)
	RETURN(tlTextLabel)
ENDFUNC

/// PURPOSE:
///    get the anim string for the specified ped's anim type
/// PARAMS:
///    iPoolPedIndex - the specific ped
///    eWhichAnim - the anim type
/// RETURNS:
///    TEXT_LABEL_23 name of the anim
FUNC TEXT_LABEL_23 GET_POOL_SYNC_ANIM_NAME(INT iPoolPedIndex, N1B_SYNCED_SCENE_ANIM_TYPE eWhichAnim)

	TEXT_LABEL_23 tlAnimName = ""
	
	SWITCH eWhichAnim	
		CASE N1B_SSAT_BASE_FEMALE_ONLY
			tlAnimName = "BASE_"
		BREAK
		CASE N1B_SSAT_ENTER_CELEB_ONLY
			tlAnimName = "ENTER"
		BREAK
		CASE N1B_SSAT_DANCE_INTRO
			tlAnimName = "DANCE_INTRO_"
		BREAK
		CASE N1B_SSAT_DANCE_LOOP
			tlAnimName = "DANCE_LOOP_"
		BREAK
		CASE N1B_SSAT_DANCE_OUTRO_CELEB_ONLY
			FALLTHRU
		CASE N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY
			tlAnimName = "REACT_"
		BREAK
		CASE N1B_SSAT_STAND_UP_FEMALE_ONLY
			tlAnimName = "OUTRO_"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1B.sc : GET_POOL_SYNC_ANIM_NAME invalid parameter eWhichAnim")
		BREAK
	ENDSWITCH
	
	TEXT_LABEL_23 tlPedExtention = ""
	
	SWITCH iPoolPedIndex
		CASE POOL_PED_CELEB
			tlPedExtention = "TYLER"
		BREAK
		CASE POOL_PED_FEMALE
			tlPedExtention = "GIRL"
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1B.sc : GET_POOL_SYNC_ANIM_NAME invalid parameter iPoolPedIndex")
		BREAK
	ENDSWITCH
	
	tlAnimName += tlPedExtention
	
	// celeb enter anim is the only one to not following the naming convention so override
	IF eWhichAnim = N1B_SSAT_ENTER_CELEB_ONLY
		tlAnimName = "ENTER"
	ENDIF
	
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_POOL_SYNC_ANIM_NAME - RETURN TEXT_LABEL_23 ", tlAnimName, " for ped ID : ", iPoolPedIndex) ENDIF #ENDIF
	
	RETURN tlAnimName
ENDFUNC

/// PURPOSE:
///    task the pool peds to use the specific anim
/// PARAMS:
///    iPoolPedIndex - the specific ped
///    eWhichAnim - which animation they should be using
PROC SET_SPECIFIC_POOL_PED_SYNCED_ANIM(INT iPoolPedIndex, INT iSyncedSceneID, N1B_SYNCED_SCENE_ANIM_TYPE eWhichAnim, FLOAT fBlendInDelta = NORMAL_BLEND_IN)
	
	IF IS_PED_UNINJURED(sPoolPed[iPoolPedIndex].pedIndex)
		TEXT_LABEL_23 tlSyncedAnimName = GET_POOL_SYNC_ANIM_NAME(iPoolPedIndex, eWhichAnim)
		STRING sSyncedAnimName = N1B_CONVERT_TEXT_LABEL_TO_STRING_FORMAT(tlSyncedAnimName)

		IF IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, sSyncedAnimName)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_SPECIFIC_PED_SYNCED_ANIM - already playing anim : ", sSyncedAnimName, " for ped ID :", iPoolPedIndex) ENDIF #ENDIF
		ELSE
			IF IsPedPerformingTask(sPoolPed[iPoolPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)
			ENDIF
			TASK_SYNCHRONIZED_SCENE(sPoolPed[iPoolPedIndex].pedIndex, iSyncedSceneID, tlAnimDict_MissionMocappedAnims, sSyncedAnimName, fBlendInDelta, NORMAL_BLEND_OUT,
					SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_USE_PHYSICS)	// SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION
			// B*1380462 only allow force ped update if we aren't stage skipping as it can get called on subsequent frames which fires assert
			IF bFinishedStageSkipping
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[iPoolPedIndex].pedIndex)
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_SPECIFIC_PED_SYNCED_ANIM - set anim : ", sSyncedAnimName, " for ped ID :", iPoolPedIndex) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    task the pool peds to use the specific anim
/// PARAMS:
///    eWhichAnim - which animation they should be using
PROC SET_POOL_PEDS_SYNCED_ANIM(N1B_SYNCED_SCENE_ANIM_TYPE eWhichAnim)	

	// don't allow the synched scene to go through if its already running
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[eWhichAnim])
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_PEDS_SYNCED_ANIM - requested sync scene already running : ", eWhichAnim) ENDIF #ENDIF
		EXIT
	ENDIF
	
	INT i
	BOOL bLoopedScene = FALSE
	BOOL bHoldLastFrameScene = FALSE
	FLOAT fBlendInDelta = NORMAL_BLEND_IN
	
	SWITCH eWhichAnim
		CASE N1B_SSAT_BASE_FEMALE_ONLY
			bLoopedScene = TRUE
			bHoldLastFrameScene = FALSE
		BREAK
		CASE N1B_SSAT_ENTER_CELEB_ONLY
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		CASE N1B_SSAT_DANCE_INTRO
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		CASE N1B_SSAT_DANCE_LOOP
			bLoopedScene = TRUE
			bHoldLastFrameScene = FALSE
		BREAK
		CASE N1B_SSAT_DANCE_OUTRO_CELEB_ONLY
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		CASE N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY		
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		CASE N1B_SSAT_STAND_UP_FEMALE_ONLY		
			bLoopedScene = FALSE
			bHoldLastFrameScene = FALSE
			fBlendInDelta = SLOW_BLEND_IN
		BREAK
		DEFAULT
			SCRIPT_ASSERT("Nigel1B.sc : SET_POOL_PEDS_SYNCED_ANIM invalid parameter eWhichAnim")
		BREAK
	ENDSWITCH
	
	iSyncScenesID_PoolPeds[eWhichAnim] = CREATE_SYNCHRONIZED_SCENE(vSyncScene_HotTub_Position, vSyncScene_HotTub_Rotation)
	SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScenesID_PoolPeds[eWhichAnim], bLoopedScene)
	SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncScenesID_PoolPeds[eWhichAnim], bHoldLastFrameScene)
	
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		// only female these scenes
		IF eWhichAnim = N1B_SSAT_BASE_FEMALE_ONLY
		OR eWhichAnim = N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY
		OR eWhichAnim = N1B_SSAT_STAND_UP_FEMALE_ONLY
			IF i = POOL_PED_FEMALE
				SET_SPECIFIC_POOL_PED_SYNCED_ANIM(i, iSyncScenesID_PoolPeds[eWhichAnim], eWhichAnim, fBlendInDelta)
			ENDIF
		ELIF eWhichAnim = N1B_SSAT_ENTER_CELEB_ONLY
		OR eWhichAnim = N1B_SSAT_DANCE_OUTRO_CELEB_ONLY
			IF i = POOL_PED_CELEB
				SET_SPECIFIC_POOL_PED_SYNCED_ANIM(i, iSyncScenesID_PoolPeds[eWhichAnim], eWhichAnim, fBlendInDelta)
			ENDIF
		ELSE
			SET_SPECIFIC_POOL_PED_SYNCED_ANIM(i, iSyncScenesID_PoolPeds[eWhichAnim], eWhichAnim, fBlendInDelta)
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE msStage)
	emissionStage = msStage
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///    Ensures all the gates at the house are shut and locked for the start of the mission
/// RETURNS:
///    TRUE when all doors are in closed state
FUNC BOOL ARE_ALL_GATES_SHUT_FOR_START_OF_THE_MISSION()
	BOOL bAllGatesLocked = TRUE
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_NORTH_GATE_01)	
		IF IS_DOOR_OPEN(N1B_MCD_NORTH_GATE_01)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_NORTH_GATE_01)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_01, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_NORTH_GATE_02)	
		IF IS_DOOR_OPEN(N1B_MCD_NORTH_GATE_02)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_NORTH_GATE_02)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_02, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_02, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_SOUTH_GATE_01)	
		IF IS_DOOR_OPEN(N1B_MCD_SOUTH_GATE_01)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_SOUTH_GATE_01)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_01, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_SOUTH_GATE_02)	
		IF IS_DOOR_OPEN(N1B_MCD_SOUTH_GATE_02)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_SOUTH_GATE_02)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_02, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_02, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_EAST_GATE_01)	
		IF IS_DOOR_OPEN(N1B_MCD_EAST_GATE_01)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_EAST_GATE_01)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_EAST_GATE_01, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_EAST_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	//IF NOT IS_UNSTREAMED_DOOR_STATE_PENDING(N1B_MCD_WEST_GATE_01)	
		IF IS_DOOR_OPEN(N1B_MCD_WEST_GATE_01)
		OR NOT IS_DOOR_IN_LOCKED_STATE(N1B_MCD_WEST_GATE_01)
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_WEST_GATE_01, 0.0, FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_WEST_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
			bAllGatesLocked = FALSE
		ENDIF
	//ENDIF
	IF bAllGatesLocked
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "ARE_ALL_GATES_SHUT_FOR_START_OF_THE_MISSION() return TRUE") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    general mission setup which affects the world, such as clear areas
PROC SETUP_WORLD_STATE_FOR_MISSION()

	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)	
	SET_CREATE_RANDOM_COPS(FALSE)
	
	//remove stuff we don't want around the pool - vehicles, peds, objects, blood decals
	SET_PED_NON_CREATION_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max)	// make sure any value changes are reflected in the initial scene setup!
	scenarioBlockingHouse = ADD_SCENARIO_BLOCKING_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max)	// make sure any value changes are reflected in the initial scene setup!
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max, FALSE)
	
	// only do it here for replays / z skips
	// clear areas which would normally get missed since they occur in the launcher's SETUP_AREA_FOR_MISSION	
	IF IS_REPLAY_IN_PROGRESS()
	OR IS_REPEAT_PLAY_ACTIVE()
	OR IS_REPLAY_BEING_SET_UP()
	OR NOT bFinishedStageSkipping
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max)
		CLEAR_AREA(vCelebritiesHouse_CentralPoint, 35.0, TRUE)	// extended to fix bug 1042629 - Blood stain persisted after retry
		CLEAR_AREA_OF_VEHICLES(vCelebritiesHouse_CentralPoint, 27.0, FALSE)
		CLEAR_AREA_OF_PEDS(vCelebritiesHouse_CentralPoint, 27.0)	
		CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - simulated SETUP_AREA_FOR_MISSION for replay / skip")
	ENDIF

	// for bug 922367 having to call set state on these doors first then check their state and reapply if not set up right
	// apparently when a door is initialised its pending state is set to invalid so thats the issue here
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_NORTH_GATE_02, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_NORTH_GATE_02, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_SOUTH_GATE_02, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_SOUTH_GATE_02, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_EAST_GATE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_EAST_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_WEST_GATE_01, 0.0, FALSE, FALSE)
	SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_WEST_GATE_01, DOORSTATE_FORCE_LOCKED_THIS_FRAME)	
	/*
	WHILE NOT ARE_ALL_GATES_SHUT_FOR_START_OF_THE_MISSION()
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - ARE_ALL_GATES_SHUT_FOR_START_OF_THE_MISSION() returning FALSE") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	*/
ENDPROC

/// PURPOSE:
///    requests and checks the pool area props' models have loaded
/// RETURNS:
///    true if all prop models have loaded
FUNC BOOL HAVE_POOL_AREA_PROP_MODELS_LOADED()
	INT i
	FOR i = 0 TO (TOTAL_POOL_AREA_PROPS - 1)
		REQUEST_MODEL(moPoolAreaProps[i].model)
	ENDFOR
	FOR i = 0 TO (TOTAL_POOL_AREA_PROPS - 1)
		IF NOT HAS_MODEL_LOADED(moPoolAreaProps[i].model)
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    requests and checks the gardener's van props' models have loaded
/// RETURNS:
///    true if all prop models have loaded
FUNC BOOL HAVE_GARDENER_VAN_PROP_MODELS_LOADED()
	INT i
	FOR i = 0 TO (NIGEL1B_TOTAL_GARDENER_VAN_PROPS - 1)
		REQUEST_MODEL(moGardnerVanProps[i].model)
	ENDFOR
	FOR i = 0 TO (NIGEL1B_TOTAL_GARDENER_VAN_PROPS - 1)
		IF NOT HAS_MODEL_LOADED(moGardnerVanProps[i].model)
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Requests and waits for the mission assets to load
PROC LOAD_MISSION_ASSETS()
	REQUEST_MODEL(mpClothesPickup.mnModel)
	REQUEST_MODEL(sPoolPed[POOL_PED_CELEB].model)
	REQUEST_MODEL(sPoolPed[POOL_PED_FEMALE].model)	
	REQUEST_MODEL(sGardenerPed.model)	
  	REQUEST_MODEL(mnCelebVehicle)	
	REQUEST_MODEL(mnGardenerVehicle)	
	REQUEST_ANIM_DICT("rcmnigel1b")	
	REQUEST_ANIM_DICT(tlAnimDict_MissionMocappedAnims)
	REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1B_WHISTLES")
	REQUEST_ADDITIONAL_TEXT("NIGEL1B", MISSION_TEXT_SLOT)
	WHILE NOT HAVE_POOL_AREA_PROP_MODELS_LOADED()
	OR NOT HAVE_GARDENER_VAN_PROP_MODELS_LOADED()
	OR NOT HAS_MODEL_LOADED(mpClothesPickup.mnModel)
	OR NOT HAS_MODEL_LOADED(sPoolPed[POOL_PED_CELEB].model)
	OR NOT HAS_MODEL_LOADED(sPoolPed[POOL_PED_FEMALE].model)
	OR NOT HAS_MODEL_LOADED(sGardenerPed.model)	
	OR NOT HAS_MODEL_LOADED(mnCelebVehicle)
	OR NOT HAS_MODEL_LOADED(mnGardenerVehicle)	
	OR NOT HAS_ANIM_DICT_LOADED("rcmnigel1b")	
	OR NOT HAS_ANIM_DICT_LOADED(tlAnimDict_MissionMocappedAnims)	
	OR NOT REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1B_WHISTLES")
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "loading mission assets") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    initial values for all the mission variables
PROC SET_MISSION_VARIABLES()
	INT i = 0
	bApplyWantedLevelInScript 							= FALSE
	bDisplayedWantedRatingObjective 					= FALSE
	bDoneDialogue_ItemsStolenPlayerLeftTheArea 			= FALSE
	bDoneDialogue_PlayerGrabsClothes 					= FALSE
	bDoneDialoguePool_Spotted 							= FALSE
	bDoneDialoguePool_InitiallySpotted					= FALSE
	bDoneDialoguePool_CallCops							= FALSE
	bDoneDialoguePool_Surrendered 						= FALSE
	bDoneDialoguePool_InitialExchange					= FALSE
	bDoneDialogue_PlayerAcknowledgesCops				= FALSE
	bHasCopBeenCreatedForScriptedWantedLevel			= FALSE
	bHasOutroPhonecallSucceeded							= FALSE
	bHaveNorthGatesBeenOpenedByPlayer					= FALSE
	bHaveSouthGatesBeenOpenedByPlayer					= FALSE
	bDetachedPropsInGardenVehicle						= FALSE
	bDoneDialogueGardener_Spotted						= FALSE
	bDoneDialogueGardener_PlayerSaysGetLost				= FALSE
	bDoneDialogueGardener_Surrendered					= FALSE
	bDoneDialogueGardener_InformsPoolPeds				= FALSE
	bDoneDialogueGarden_TrevorKillsGardener				= FALSE
	bDoneDialoguePool_CelebGetsOutPool					= FALSE
	bDoneDialoguePool_CelebSeesClothesAreMissing		= FALSE
	bDoneDialoguePool_CelebInTheHotTub					= FALSE
	bDoneDialoguePool_CelebInTheHotTubRunOutOfLines		= FALSE
	bDoneDialoguePool_CelebPoses						= FALSE
	bDoneDialogueGardener_Flee							= FALSE
	bDoneDialogue_ShoutCallCopsWhenPlayerLeaves			= FALSE
	FOR i = 0 TO (NUM_TREVOR_BARGE_GATE_COMMENTS - 1)
		bDoneDialogue_TrevorBargesLockedGate[i] 		= FALSE
	ENDFOR
	bDoneDialogue_PlayerMissedCelebCallingCops			= TRUE	// only swapped to false if it's needed
	bTrevorBargedWestGate								= FALSE
	bTrevorBargedEastGate								= FALSE
	bDoSwitch_PoolStateTo_CelebLookingForClothes		= FALSE
	bForceHouseGatesOpenDuringGameplay					= FALSE
	bHasCelebReachedClothesLocation						= FALSE
	bHavePoolPedsCallTheCops 							= FALSE
	bCelebOkToStartPhoneConvo							= FALSE
	bPlayerStoppedCelebCallingCops						= FALSE
	bPlayerWasSpottedByGardener							= FALSE
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		bDoneDanceOutroSceneForPoolPeds[i] 				= FALSE
	ENDFOR
	bDoneStandUpSceneForFemalePoolPed					= FALSE
	bStoppedGardenerVanAnim								= FALSE
	bPauseDialoguePool_CelebInTheHotTub					= FALSE
	eN1B_MissionFailedReason							= FAILED_DEFAULT
	ePlayerLocation										= N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
	eGardenAreaState									= N1B_GARDEN_AREA_STATE_UNDETECTED
	ePlayerMovementState								= N1B_PEDMOVEMENTSTATE_DEFAULT
	ePoolAreaState										= N1B_POOL_AREA_STATE_UNDETECTED
	fCelebPoolSwimInitialHeading[NIGEL1B_POOL_NORTH_SIDE] = 148.84
	fCelebPoolSwimInitialHeading[NIGEL1B_POOL_SOUTH_SIDE] = 23.67
	fCelebPoolSwimArrivalHeading[NIGEL1B_POOL_NORTH_SIDE] = 6.24
	fCelebPoolSwimArrivalHeading[NIGEL1B_POOL_SOUTH_SIDE] = 165.10	
	fStepOutOfPoolHeading[NIGEL1B_POOL_NORTH_SIDE]		= -4.80
	fStepOutOfPoolHeading[NIGEL1B_POOL_SOUTH_SIDE]		= -170.96
	//fPedLeftPoolHeading[NIGEL1B_POOL_NORTH_SIDE]		= 
	//fPedLeftPoolHeading[NIGEL1B_POOL_SOUTH_SIDE]		= 
	fHotTubDimensionsWidth								= 2.0
	fPoolDimensionsWidth								= 5.5
	fDistToPoolEdge										= 2.4	// 2.3
	iAdditionalSurrenderedDialoguePool_Counter			= 0
	iCelebInPoolInitialPositionID						= NIGEL1B_POOL_SOUTH_SIDE
	//iPoolPedId_ToUpdateInManager						= 0		//ommented out for now as this is pretty slow at updating
	iTimer_PoolStateTo_CelebLookingForClothes 			= 0
	iTimer_PoolPedsTo_CallCops							= -1
	iMissionMusicEventTriggerCounter					= 0
	iTimer_DelayResetAudioFlag_AllowScoreAndRadio		= 0
	iTimer_DelayForOutroPhonecall						= 0
	iTimer_DelayGardenerSearchingVanDialogue			= 0
	iDialogueTimer_PoolPedFlee							= 0
	iDialogueTimer_PoolPedWaitingForCops				= 0
	iTimer_DelayPoolPedsFleeWhenSurrendered				= 0
	iTimer_TimeWindowForSpottedStealingClothesDialogue	= 0
	iTimer_GardenAreaState								= 0
	FOR i = 0 TO (ENUM_TO_INT(N1B_MAX_SYNCED_SCENE_ANIM_TYPE) - 1)
		iSyncScenesID_PoolPeds[i] = -1
	ENDFOR
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherDataLocal.iSyncSceneIndex)
		iSyncScenesID_PoolPeds[N1B_SSAT_BASE_FEMALE_ONLY] = sRCLauncherDataLocal.iSyncSceneIndex
		CPRINTLN(DEBUG_MISSION, " Update sync scene handle from TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE : sRCLauncherDataLocal.iSyncSceneIndex = ", sRCLauncherDataLocal.iSyncSceneIndex,
								" iSyncScenesID_PoolPeds[N1B_SSAT_BASE_FEMALE_ONLY] = ", iSyncScenesID_PoolPeds[N1B_SSAT_BASE_FEMALE_ONLY])
	ENDIF
	//iSyncSceneID_GardenerIdle							= -1
	iPoolPedID_PlayerAttackedToFlee							= -1
	iPedID_PoolPedWhoDetectedPlayer						= POOL_PED_CELEB 	//default to the main ped at the pool
	iSoundID_WhistlingGardener							= GET_SOUND_ID()
	iTimer_DelayForPhoneTask							= 0
	iTimer_DelayTrevorCopsDialogue						= 0
	iTimer_DelayAdditionalSurrenderedDialogue			= 0
	fHeading_PlayerMissionStart							= 40.9431
	sRoot_PlayerLeavesAreaDialogue						= "NULL"
	vCelebritiesHouse_CentralPoint = << -1035.73, 365.44, 68.91 >>	// << -1041.35, 362.76, 68.91 >>
	vCelebGoToClothesPos = <<-1051.5638, 369.6159, 68.9137>>
	vClearPedZone_HouseArea_Min = << -1073.19, 342.05, 63.328316 >>
	vClearPedZone_HouseArea_Max = << -966.24, 411.05, 84.820435 >>
	vPoolArea_NorthCircle_Pos = << -1058.74, 378.36, 68.97 >>
	vPoolArea_MajorityPosA = << -1064.68, 378.42, 67.0 >>
	vPoolArea_MajorityPosB = << -1039.15, 351.76, 75.0 >>
	vPoolArea_SouthWallPosA = <<-1041.712402,348.062195,66.916428>>
	vPoolArea_SouthWallPosB = <<-1036.664429,347.773102,75.363640>>  
	vPoolArea_SouthWallPosC = <<-1047.452026,347.567963,66.914040>>
	vPoolArea_SouthWallPosD = <<-1046.709595,353.177917,73.917915>> 
	vPoolArea_SouthWallPosE = <<-1057.541626,348.667389,66.914093>>
	vPoolArea_SouthWallPosF = <<-1057.282471,352.616150,73.913872>> 
	vPoolArea_SouthWallPosG = <<-1063.374146,350.395935,66.913910>>
	vPoolArea_SouthWallPosH = <<-1061.651123,352.834503,73.815926>> 
	vCelebPoolSwimGotos[NIGEL1B_POOL_NORTH_SIDE] = << -1054.42, 368.18, 68.35 >>
	vCelebPoolSwimGotos[NIGEL1B_POOL_SOUTH_SIDE] = << -1054.70, 355.94, 68.35 >>	
	vStepOutOfPoolPos[NIGEL1B_POOL_NORTH_SIDE]				= << -1053.9471, 370.4447, 68.6948 >>
	vStepOutOfPoolPos[NIGEL1B_POOL_SOUTH_SIDE]				= << -1054.2603, 353.4767, 68.6948 >>
	vPedLeftPoolPos[NIGEL1B_POOL_NORTH_SIDE]				= << -1054.0918, 373.4720, 68.9138 >>
	vPedLeftPoolPos[NIGEL1B_POOL_SOUTH_SIDE]				= << -1054.130, 350.572, 68.913 >>
	vHotTubDimensionsCentrePos								= <<-1059.450684,361.982147,69.786629>>
	vPoolDimensionsMinPos									= <<-1054.633423,353.726318,67.444794>>
	vPoolDimensionsMaxPos									= <<-1054.807007,370.001160,69.429314>>
	vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE] 	= << -1059.84460, 366.60718, 69.75161 >>
	vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE] 	= << -1059.64185, 357.37802, 69.75161 >>
	vSyncScene_HotTub_Position								= << -1059.000, 360.976, 69.0 >>
	vSyncScene_HotTub_Rotation								= << -0.000, 0.000, -1.080 >>
	vPos_PlayerMissionStart									= <<-1011.8854, 346.9502, 69.2904>>

	// ensure any values changes are updated in the mission script to keep in sync
	sPoolPed[POOL_PED_CELEB].model 				= IG_TYLERDIX
	sPoolPed[POOL_PED_FEMALE].model				= A_F_Y_BEACH_01
	sGardenerPed.model 							= S_M_M_GARDENER_01	
	mpClothesPickup.vPosition					= << -1050.03, 368.95, 69.29 >>	// original  << -1045.2437, 367.4617, 68.9128 >>
	mpClothesPickup.vOrientation				= << 0.0, 0.0, -11.27 >>
	mpClothesPickup.mnModel 					= Prop_nigel_bag_pickup	//PROP_BEACH_BAG_01B
	// only needed if we switch over to pickup_index, currently object_index
	mpClothesPickup.type						= PICKUP_CUSTOM_SCRIPT
	mpClothesPickup.iPlacementFlags 			= 0
	SET_BIT(mpClothesPickup.iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(mpClothesPickup.iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	SET_BIT(mpClothesPickup.iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	SET_BIT(mpClothesPickup.iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	
	mnCelebVehicle 								= COMET2
	mnGardenerVehicle 							= SPEEDO
	moPoolAreaProps[0].model					= PROP_BEACH_RING_01
	moPoolAreaProps[0].vPosition				= << -1047.42, 370.22, 69.09 >>
	moPoolAreaProps[0].fHeading 				= 38.2398
	moPoolAreaProps[0].vRotation 				= << -0.57, -0.15, 92.03 >>
	moPoolAreaProps[1].model					= PROP_BEACHBALL_01
	moPoolAreaProps[1].vPosition				= << -1051.1575, 368.6751, 68.9139 >>
	moPoolAreaProps[1].vRotation				= << 0.0, 0.0, 90.0 >>
	moPoolAreaProps[1].fHeading 				= 53.2741
	//moPoolAreaProps[2].model					= PROP_BBQ_1
	//moPoolAreaProps[2].vPosition				= <<-1046.099976,360.130005,68.910004>>	
	//moPoolAreaProps[2].vRotation				= <<0.000000,0.000000,-135.000000>>
	//moPoolAreaProps[2].fHeading 				= -135.000000
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].model 			= PROP_LEAF_BLOWER_01
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].vPosition 		= << -0.3, -2.4, 0.16 >>
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].vRotation 		= << 0.0, 0.0, 75.0 >>
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_BOX].model 			= PROP_TOOL_BOX_01
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_BOX].vPosition 		= << 0.3, -0.8, -0.15 >>
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_BOX].vRotation 		= << 0.0, 0.0, 325.0 >>
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_RAKE].model 			= PROP_TOOL_RAKE
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_RAKE].vPosition 		= << -0.2, -0.9, -0.1 >>
	moGardnerVanProps[NIGEL1B_PROP_GARDENER_TOOL_RAKE].vRotation 		= << 270.0, 180.0, 20.0 >>		
	moCelebPhone.model							= P_AMB_PHONE_01
	moCelebPhone.vPosition						= << 0.0, 0.0, 0.0  >>
	moCelebPhone.vRotation						= << 0.0, 0.0, 0.0 >>	
	mnCopCar 									= POLICE
	mnCopPed 									= S_M_Y_COP_01
	tlPausedDialogue_CelebInTheHotTub 			= ""
	tlAnimDict_PedAwareIdles 					= "rcmnigel1b@reactions"
	tlAnimDict_MissionMocappedAnims				= "rcmnigel1bnmt_1b"
	weaponTypeStoredCurrentEquipped 			= WEAPONTYPE_UNARMED
	// Setup mission controlled doors
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_NORTH_GATE_01, PROP_LRGGATE_01_L, << -1011.63, 367.21, 72.24 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_NORTH_GATE_02, PROP_LRGGATE_01_R, << -1006.89, 364.48, 72.24 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_SOUTH_GATE_01, PROP_LRGGATE_01_L, << -1017.81, 351.32, 70.66 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_SOUTH_GATE_02, PROP_LRGGATE_01_R, << -1012.49, 352.58, 70.66 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_EAST_GATE_01, PROP_LRGGATE_03B_LD, << -1040.49, 384.38, 69.92 >>)
	REGISTER_NEW_MISSION_CONTROLLED_DOOR(N1B_MCD_WEST_GATE_01, PROP_LRGGATE_03B_LD, << -1056.19, 382.26, 69.72 >>)
	//HouseArea Poly Check - used by IS_PED_IN_HOUSE_AREA()
	OPEN_TEST_POLY(tpCelebHouseArea)	//from most north east point, running clockwise
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -987.81, 363.85, 71.32 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -988.15, 352.77, 71.03 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1030.05, 342.06, 67.88 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1071.70, 346.33, 66.21 >>)	// this is the lower z position for the garden area, if changed update NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1070.97, 379.39, 67.96 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1067.30, 387.20, 67.82 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1059.75, 390.57, 67.96 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1030.35, 392.35, 69.34 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1016.20, 378.02, 70.67 >>)
		ADD_TEST_POLY_VERT(tpCelebHouseArea, << -1006.00, 370.87, 71.04 >>)
	CLOSE_TEST_POLY(tpCelebHouseArea)	//must be called once verts have been added.
	//Garden Area Poly Check - used by IS_PED_IN_GARDEN_AREA()
	OPEN_TEST_POLY(tpGardenArea)		//from most north point, running clockwise
		ADD_TEST_POLY_VERT(tpGardenArea, << -1025.32, 380.54, 70.79 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1020.40, 375.87, 70.38 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1012.08, 369.80, 71.10 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1011.78, 367.31, 71.12 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1006.72, 364.41, 71.28 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1004.07, 365.51, 71.78 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -993.65, 360.78, 71.51 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -993.68, 355.40, 71.02 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1008.25, 350.70, 70.00 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1010.90, 352.95, 69.54 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1018.94, 351.08, 69.90 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1020.24, 349.86, 69.67 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1020.85, 347.65, 68.93 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1030.98, 346.06, 70.4 >>)
		ADD_TEST_POLY_VERT(tpGardenArea, << -1036.05, 346.14, 70.45 >>)	
	CLOSE_TEST_POLY(tpGardenArea)	//must be called once verts have been added.
	RESET_THREAT_SHAPETESTS()
ENDPROC

/// PURPOSE:
///    Initialise the peds and the clothes object
PROC SPAWN_PEDS_AND_PICKUP_OBJ()
	INT i	
	
	// Gardner's vehicle	
	// Assign Gardner's vehicle over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
		vehGardenerVehicle = sRCLauncherDataLocal.vehID[0]
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener's vehicle passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(vehGardenerVehicle)
			vehGardenerVehicle = CREATE_VEHICLE(mnGardenerVehicle, << -1010.97, 359.91, 70.65 >>, 331.46)
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener's vehicle created in mission script")
		ENDIF
		IF IS_ENTITY_ALIVE(vehGardenerVehicle)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehGardenerVehicle)
			SET_VEHICLE_COLOURS(vehGardenerVehicle, 97, 97)
			SET_VEHICLE_DIRT_LEVEL(vehGardenerVehicle, 13.5)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehGardenerVehicle, SC_DOOR_REAR_LEFT, FALSE)
			SET_VEHICLE_DOOR_OPEN(vehGardenerVehicle, SC_DOOR_REAR_LEFT, FALSE, TRUE)				// Ste says door needs to be open before setting ratio
			SET_VEHICLE_DOOR_CONTROL(vehGardenerVehicle, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)	// can take these out with anim setup otherwise door has no collision			
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehGardenerVehicle)
			IF NOT IS_ENTITY_PLAYING_ANIM(vehGardenerVehicle, "rcmnigel1b", "idle_speedo")
				REQUEST_ANIM_DICT("rcmnigel1b")
				IF HAS_ANIM_DICT_LOADED("rcmnigel1b")
					TASK_VEHICLE_PLAY_ANIM(vehGardenerVehicle, "rcmnigel1b", "idle_speedo")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener's vehicle anim applied idle_speedo") ENDIF #ENDIF
				ENDIF
			ENDIF
			/*// have to wait on the door opening as it pushed the gardener away from his position otherwise
			BOOL bIsDoorOpen = FALSE
			WHILE NOT bIsDoorOpen
				IF IS_ENTITY_ALIVE(vehGardenerVehicle)
					FLOAT fOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(vehGardenerVehicle, SC_DOOR_REAR_LEFT)
					IF NOT IS_VEHICLE_DOOR_FULLY_OPEN(vehGardenerVehicle, SC_DOOR_REAR_LEFT)
					AND (fOpenRatio < 0.8)	//secondary check to see if door is open
						//SET_VEHICLE_DOOR_OPEN(vehGardenerVehicle, SC_DOOR_REAR_LEFT)	// swapped for below command due to bug*896419 - Mission doesn't progress to initial phone call
						SET_VEHICLE_DOOR_CONTROL(vehGardenerVehicle, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 1.0)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ waiting for gardener's van door to set open, current ratio is : ", fOpenRatio) ENDIF #ENDIF
					ELSE
						bIsDoorOpen = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ gardener's van door is open") ENDIF #ENDIF
					ENDIF
				ENDIF
				WAIT(0)
			ENDWHILE*/
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(mnGardenerVehicle)
	
	// Pickup_Index Setup
	SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[0])		
	IF NOT DOES_PICKUP_EXIST(mpClothesPickup.index)
		mpClothesPickup.index =  CREATE_PICKUP_ROTATE(mpClothesPickup.type, mpClothesPickup.vPosition, mpClothesPickup.vOrientation, mpClothesPickup.iPlacementFlags, -1, EULER_XYZ, TRUE, mpClothesPickup.mnModel)
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - pickup created in mission script")
	ENDIF
	IF DOES_PICKUP_EXIST(mpClothesPickup.index)
		IF DOES_PICKUP_OBJECT_EXIST(mpClothesPickup.index)
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ : DOES_PICKUP_OBJECT_EXIST return TRUE : after creating clothes pickup")
		ELSE
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ : DOES_PICKUP_OBJECT_EXIST return FALSE! : after creating clothes pickup")
		ENDIF
	ELSE	
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ : DOES_PICKUP_EXIST return FALSE! : after creating clothes pickup")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(mpClothesPickup.mnModel)
	
	// Create Tyler Dixon
	// Assign celeb ped over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		sPoolPed[POOL_PED_CELEB].pedIndex = sRCLauncherDataLocal.pedID[0]
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - celeb ped passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(sPoolPed[POOL_PED_CELEB].pedIndex)
			iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
			sPoolPed[POOL_PED_CELEB].pedIndex = CREATE_PED(PEDTYPE_MISSION, sPoolPed[POOL_PED_CELEB].model, vCelebPoolSwimGotos[iCelebInPoolInitialPositionID], fCelebPoolSwimInitialHeading[iCelebInPoolInitialPositionID])
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - celeb ped created in mission script")
		ENDIF
		// only do this created in this script (since initial scene ped slready exists player could be interacting
		IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
			SET_ENTITY_COORDS_NO_OFFSET(sPoolPed[POOL_PED_CELEB].pedIndex, vCelebPoolSwimGotos[iCelebInPoolInitialPositionID]) 
			SET_ENTITY_HEADING(sPoolPed[POOL_PED_CELEB].pedIndex, fCelebPoolSwimInitialHeading[iCelebInPoolInitialPositionID]) 
			TASK_PLAY_ANIM(sPoolPed[POOL_PED_CELEB].pedIndex, "rcmnigel1b", "Swimming_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
			#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ -> set Tyler alt walk anims") ENDIF #ENDIF
		ENDIF
	ENDIF
	// want this to be applied if created by initial scene or misison script - 
	IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
		SET_PED_DIES_IN_WATER(sPoolPed[POOL_PED_CELEB].pedIndex, FALSE)
		SET_PED_LEG_IK_MODE(sPoolPed[POOL_PED_CELEB].pedIndex, LEG_IK_FULL)
		SET_PED_ALTERNATE_WALK_ANIM(sPoolPed[POOL_PED_CELEB].pedIndex, tlAnimDict_MissionMocappedAnims, "walk_tyler")
		TASK_PLAY_ANIM(sPoolPed[POOL_PED_CELEB].pedIndex, "rcmnigel1b", "Swimming_Idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
		#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ -> set Tyler alt walk anims") ENDIF #ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sPoolPed[POOL_PED_CELEB].model)
	
	//female in HOT_TUB
	// Assign female ped over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[1])
		sPoolPed[POOL_PED_FEMALE].pedIndex = sRCLauncherDataLocal.pedID[1]
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - female ped passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(sPoolPed[POOL_PED_FEMALE].pedIndex)
			sPoolPed[POOL_PED_FEMALE].pedIndex = CREATE_PED(PEDTYPE_MISSION, sPoolPed[POOL_PED_FEMALE].model, << -1058.43, 362.78, 69.86 >>, 130.45)
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - female ped created in mission script")
		ENDIF
		IF IS_PED_UNINJURED(sPoolPed[POOL_PED_FEMALE].pedIndex)
			SET_PED_COMPONENT_VARIATION(sPoolPed[POOL_PED_FEMALE].pedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sPoolPed[POOL_PED_FEMALE].pedIndex, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(sPoolPed[POOL_PED_FEMALE].pedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sPoolPed[POOL_PED_FEMALE].pedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sPoolPed[POOL_PED_FEMALE].pedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)		
			
			SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_BASE_FEMALE_ONLY)
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sPoolPed[POOL_PED_FEMALE].model)
	
	// common setup needs on the pool peds
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sPoolPed[i].pedIndex, TRUE)	//using this causes bug 453584, waiting to find out if it's intended behaviour
			SET_PED_CAN_BE_TARGETTED(sPoolPed[i].pedIndex, TRUE)
			SET_PED_DIES_WHEN_INJURED(sPoolPed[i].pedIndex, TRUE)
			SET_PED_DIES_IN_WATER(sPoolPed[i].pedIndex, FALSE)
			SET_PED_MAX_TIME_UNDERWATER(sPoolPed[i].pedIndex, 60.0)
			SET_PED_PATH_PREFER_TO_AVOID_WATER(sPoolPed[i].pedIndex, FALSE)	//Possibly switch this once they notice the player
			SET_ENTITY_LOAD_COLLISION_FLAG(sPoolPed[i].pedIndex, TRUE)
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sPoolPed[i].pedIndex, TRUE)	
			STOP_PED_SPEAKING(sPoolPed[i].pedIndex, TRUE)	
			IF IS_PED_IN_GROUP(sPoolPed[i].pedIndex)
				REMOVE_PED_FROM_GROUP(sPoolPed[i].pedIndex)
			ENDIF
			SET_PED_RELATIONSHIP_GROUP_HASH(sPoolPed[i].pedIndex, relGroupPlayerEnemy)			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_STATE_RELAXED initially set for id = ", i) ENDIF #ENDIF
			sPoolPed[i].AI = N1B_PED_AI_STATE_RELAXED
			sPoolPed[i].iFrameCountLastSeenPlayer = 0
			sPoolPed[i].iAiDelayTimer = 0
			sPoolPed[i].iTimer = 0
			#IF IS_DEBUG_BUILD
				TEXT_LABEL tDebugName = "PoolPedID: "
				tDebugName += i
				SET_PED_NAME_DEBUG(sPoolPed[i].pedIndex, tDebugName)
			#ENDIF
		ENDIF
	ENDFOR
	
	// props - current not created in the initial scene
	FOR i = 0 TO (TOTAL_POOL_AREA_PROPS - 1)
		IF NOT DOES_ENTITY_EXIST(moPoolAreaProps[i].object)
			moPoolAreaProps[i].object = CREATE_OBJECT(moPoolAreaProps[i].model, moPoolAreaProps[i].vPosition)
		ENDIF
		IF IS_ENTITY_ALIVE(moPoolAreaProps[i].object)			
			IF i = 0
			OR i = 1
				SET_ENTITY_ROTATION(moPoolAreaProps[i].object, moPoolAreaProps[i].vRotation)
				SET_ENTITY_COORDS_NO_OFFSET(moPoolAreaProps[i].object, moPoolAreaProps[i].vPosition)
			//ELSE
			//	SET_ENTITY_HEADING(moPoolAreaProps[i].object, moPoolAreaProps[i].fHeading)
			ENDIF
			ACTIVATE_PHYSICS(moPoolAreaProps[i].object)
		ENDIF
	ENDFOR
	//release all prop models after created, as some share same MODEL_NAMES
	FOR i = 0 TO (TOTAL_POOL_AREA_PROPS - 1)
		SET_MODEL_AS_NO_LONGER_NEEDED(moPoolAreaProps[i].model)
	ENDFOR
	
	// props - inside gardener's van - having to attach as vehicle interiors have no collision! not ideal
	IF IS_ENTITY_ALIVE(vehGardenerVehicle)
		INT iVehicleBone = GET_ENTITY_BONE_INDEX_BY_NAME(vehGardenerVehicle, "chassis")
		FOR i = 0 TO (NIGEL1B_TOTAL_GARDENER_VAN_PROPS - 1)
			IF NOT DOES_ENTITY_EXIST(moGardnerVanProps[i].object)
				moGardnerVanProps[i].object = CREATE_OBJECT(moGardnerVanProps[i].model, moGardnerVanProps[i].vPosition)
			ENDIF
			IF IS_ENTITY_ALIVE(moGardnerVanProps[i].object)
				ATTACH_ENTITY_TO_ENTITY(moGardnerVanProps[i].object, vehGardenerVehicle, iVehicleBone, moGardnerVanProps[i].vPosition, moGardnerVanProps[i].vRotation, TRUE, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener prop attached to van Id =", i) ENDIF #ENDIF
				//SET_ENTITY_COLLISION(moGardnerVanProps[i].object, FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(moGardnerVanProps[i].object, FALSE)
				SET_ENTITY_DYNAMIC(moGardnerVanProps[i].object, FALSE)
				//FREEZE_ENTITY_POSITION(moGardnerVanProps[i].object, TRUE)
				SET_CAN_AUTO_VAULT_ON_ENTITY(moGardnerVanProps[i].object, FALSE)
				SET_CAN_CLIMB_ON_ENTITY(moGardnerVanProps[i].object, FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(moGardnerVanProps[i].model)
			ENDIF
		ENDFOR
	ENDIF
	
	//Celeb's super car
	// Assign celeb's vehicle over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[1])
		vehCelebsVehicle = sRCLauncherDataLocal.vehID[1]
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - celeb's vehicle passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(vehCelebsVehicle)
			vehCelebsVehicle = CREATE_VEHICLE(mnCelebVehicle, << -1018.26, 357.76, 70.20 >>, 339.52)
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - celeb's vehicle created in mission script")
		ENDIF
		IF IS_ENTITY_ALIVE(vehCelebsVehicle)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehCelebsVehicle)	
			SET_VEHICLE_COLOURS(vehCelebsVehicle, 27, 27)	//red			
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCelebVehicle)
	
	//Gardener
	// Assign gardener ped over from initial scene
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[2])
		sGardenerPed.pedIndex = sRCLauncherDataLocal.pedID[2]
		CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener ped passed over from launcher")
	ELSE
		IF NOT DOES_ENTITY_EXIST(sGardenerPed.pedIndex)
			sGardenerPed.pedIndex = CREATE_PED(PEDTYPE_MISSION, sGardenerPed.model, << -1012.57, 357.48, 70.62 >>, -28.55)
			CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener ped created in mission script")
		ENDIF	
		IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
			SET_PED_DEFAULT_COMPONENT_VARIATION(sGardenerPed.pedIndex)
			SET_ENTITY_HEALTH(sGardenerPed.pedIndex, 160)	
			SET_ENTITY_COORDS_NO_OFFSET(sGardenerPed.pedIndex, << -1012.57, 357.48, 70.62 >>)
			SET_PED_DIES_WHEN_INJURED(sGardenerPed.pedIndex, TRUE)
			
			REQUEST_ANIM_DICT("rcmnigel1b")
			IF HAS_ANIM_DICT_LOADED("rcmnigel1b")
				TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS) // B*1544302 - AF_USE_KINEMATIC_PHYSICS)				
			ENDIF
			/*IF IS_VEHICLE_OK(vehGardenerVehicle)
				//iSyncSceneID_GardenerIdle = CREATE_SYNCHRONIZED_SCENE(<<-1010.970 , 359.960, 70.665 >>, << 6.500, 0.000, -28.000 >>)
				//iSyncSceneID_GardenerIdle = CREATE_SYNCHRONIZED_SCENE((GET_ENTITY_COORDS(vehGardenerVehicle) + << 0.0, 0.0, 0.05 >>), GET_ENTITY_ROTATION(vehGardenerVehicle))
				iSyncSceneID_GardenerIdle = CREATE_SYNCHRONIZED_SCENE(<< 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncSceneID_GardenerIdle, vehGardenerVehicle, 0)
			
				SET_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneID_GardenerIdle, TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncSceneID_GardenerIdle, FALSE)
				
				INT iEntitySyncedSceneFlags = 0
				iEntitySyncedSceneFlags = ENUM_TO_INT(SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
				iEntitySyncedSceneFlags = ENUM_TO_INT(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(vehGardenerVehicle, iSyncSceneID_GardenerIdle, "idle_speedo", "rcmnigel1b", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iEntitySyncedSceneFlags)
				
				// this makes door have no collision and van doesn't come out of the anim ever
				//PLAY_ENTITY_ANIM(vehGardenerVehicle, "idle_speedo", "rcmnigel1b", INSTANT_BLEND_IN, TRUE, FALSE, FALSE, 0, ENUM_TO_INT(AF_ABORT_ON_WEAPON_DAMAGE))

				TASK_SYNCHRONIZED_SCENE(sGardenerPed.pedIndex, iSyncSceneID_GardenerIdle, "rcmnigel1b", "idle_gardener", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,
														SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)	
				CPRINTLN(DEBUG_MISSION, "SPAWN_PEDS_AND_PICKUP_OBJ - gardener and van sync scene applied iSyncSceneID_GardenerIdle = ", iSyncSceneID_GardenerIdle)
			ENDIF*/
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sGardenerPed.model)
	
	// common gardener setup
	IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sGardenerPed.pedIndex, TRUE)
		SET_PED_CAN_BE_TARGETTED(sGardenerPed.pedIndex, TRUE)
		SET_PED_DIES_IN_WATER(sGardenerPed.pedIndex, FALSE)
		SET_ENTITY_LOAD_COLLISION_FLAG(sGardenerPed.pedIndex, TRUE)
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sGardenerPed.pedIndex, TRUE)			
		STOP_PED_SPEAKING(sGardenerPed.pedIndex, TRUE)	
		SET_PED_SUFFERS_CRITICAL_HITS(sGardenerPed.pedIndex, TRUE)	
		BLOCK_PED_FROM_GENERATING_DEAD_BODY_EVENTS_WHEN_DEAD(sGardenerPed.pedIndex, TRUE)		// B*1537381 - peds outside house area going nuts
		//GIVE_PED_HELMET(sGardenerPed.pedIndex, TRUE, PV_FLAG_JOB)
		//SET_PED_PATH_CAN_USE_CLIMBOVERS(sGardenerPed.pedIndex, TRUE)
		IF IS_PED_IN_GROUP(sGardenerPed.pedIndex)
			REMOVE_PED_FROM_GROUP(sGardenerPed.pedIndex)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sGardenerPed.pedIndex, relGroupPlayerEnemy)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)	
		#IF IS_DEBUG_BUILD
			TEXT_LABEL tDebugNameGardener = "GardenerN1B"
			SET_PED_NAME_DEBUG(sGardenerPed.pedIndex, tDebugNameGardener)
		#ENDIF		
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_STATE_RELAXED initially set for sGardenerPed") ENDIF #ENDIF
		sGardenerPed.AI = N1B_PED_AI_STATE_RELAXED
		sGardenerPed.iFrameCountLastSeenPlayer = 0
		sGardenerPed.iAiDelayTimer = 0
		sGardenerPed.iTimer = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialisation of the mission
PROC INIT_MISSION()

	SET_MISSION_VARIABLES()
	
	#IF IS_DEBUG_BUILD			
		SETUP_MISSION_WIDGET()		//this needs setting up first to ensure debug print can be displayed when loading assets	
		// stage skipping            
	    mSkipMenu[Z_SKIP_STEAL_CELEB_CLOTHES].sTxtLabel = "STEAL CELEB'S CLOTHES" 
	    mSkipMenu[Z_SKIP_ITEMS_STOLEN_LEAVE_THE_AREA].sTxtLabel = "LEAVE THE AREA"
		mSkipMenu[Z_SKIP_PLAYER_LEFT_THE_AREA].sTxtLabel = "PLAYER LEFT THE AREA"
		mSkipMenu[Z_SKIP_OUTRO_PHONECALL].sTxtLabel = "OUTRO_PHONECALL"
		mSkipMenu[Z_SKIP_MISSION_PASSED].sTxtLabel = "Mission Passed"
	#ENDIF
	
	RC_CLEANUP_LAUNCHER()
	SETUP_WORLD_STATE_FOR_MISSION()	// needs to be no wait between mission trigger and resetup of scenario blocking area and ped non creation zone
	
	//INFORM_MISSION_STATS_OF_MISSION_START_NIGEL_1_B()
	
	LOAD_MISSION_ASSETS()
	
	ADD_RELATIONSHIP_GROUP("N1B_PLAYER_HATE_GROUP", relGroupPlayerEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupPlayerEnemy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupPlayerEnemy)	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupPlayerEnemy, RELGROUPHASH_COP)		// make the cops work with the celebs instead of fight them
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relGroupPlayerEnemy)
	REGISTER_SCRIPT_WITH_AUDIO()	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK, FALSE)
	
	CLEAR_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLB_KILLED_TYLER))	// ensure this bit is initially clear when starting the mission
	
	SPAWN_PEDS_AND_PICKUP_OBJ()
ENDPROC

/// PURPOSE:
///    Removes all the assets loaded by the mission, used in mission cleanup and reset mission functions.
/// PARAMS:
///    bClearTextSlots - if TRUE we clear the additional text slots - MISSION_TEXT_SLOT
PROC UNLOAD_ALL_MISSION_ASSETS(BOOL bClearTextSlots = TRUE)
	INT i
	REMOVE_ANIM_DICT("rcmnigel1b")
	REMOVE_ANIM_DICT(tlAnimDict_PedAwareIdles)
	REMOVE_ANIM_DICT(tlAnimDict_MissionMocappedAnims)	
	IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
		STOP_SOUND(iSoundID_WhistlingGardener)
	ENDIF
	RELEASE_SOUND_ID(iSoundID_WhistlingGardener)	
	RELEASE_SCRIPT_AUDIO_BANK()
	UNREGISTER_SCRIPT_WITH_AUDIO()	
	i = 0
	REPEAT TOTAL_POOL_AREA_PROPS i
		SET_MODEL_AS_NO_LONGER_NEEDED(moPoolAreaProps[i].model)
	ENDREPEAT
	i = 0
	REPEAT NIGEL1B_TOTAL_GARDENER_VAN_PROPS i
		SET_MODEL_AS_NO_LONGER_NEEDED(moGardnerVanProps[i].model)
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(mpClothesPickup.mnModel)
	i = 0
	REPEAT TOTAL_POOL_PEDS i
		SET_MODEL_AS_NO_LONGER_NEEDED(sPoolPed[i].model)
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCelebVehicle)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnGardenerVehicle)	
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCar)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPed)
	SET_MODEL_AS_NO_LONGER_NEEDED(sGardenerPed.model)	
	IF bClearTextSlots
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all of the blips used in the mission, checking if they exist.
PROC DELETE_ALL_MISSION_BLIPS()
	INT i
	SAFE_REMOVE_BLIP(mpClothesPickup.blipIndex)
	FOR i = 0 TO (TOTAL_POOL_PEDS-1)
		CLEANUP_AI_PED_BLIP(sPoolPed[i].blipAIStruct)
	ENDFOR
	CLEANUP_AI_PED_BLIP(sGardenerPed.blipAIStruct)
ENDPROC

/// PURPOSE:
///    Deletes all of the mission entities checking if they exist
///    Used in Mission Failed, when faded out.
/// PARAMS:
///    bDelete - if true all entities will be deleted, else released
PROC CLEANUP_ALL_MISSION_ENTITIES(BOOL bDelete = FALSE)
	INT i
	FOR i = 0 TO (TOTAL_POOL_PEDS-1)
		IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
			SET_PED_KEEP_TASK(sPoolPed[i].pedIndex, TRUE)
			SET_ENTITY_LOAD_COLLISION_FLAG(sPoolPed[i].pedIndex, FALSE)
			IF i = POOL_PED_CELEB
				SET_PED_LEG_IK_MODE(sPoolPed[i].pedIndex, LEG_IK_PARTIAL)
			ENDIF
		ENDIF
		SAFE_REMOVE_PED(sPoolPed[i].pedIndex, bDelete)
	ENDFOR	
	IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
		SET_PED_KEEP_TASK(sGardenerPed.pedIndex, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(sGardenerPed.pedIndex, FALSE)
	ENDIF
	SAFE_REMOVE_PED(sGardenerPed.pedIndex, bDelete)	
	SAFE_REMOVE_PICKUP(mpClothesPickup.index)
	i = 0
	REPEAT TOTAL_POOL_AREA_PROPS i
		SAFE_REMOVE_OBJECT(moPoolAreaProps[i].object, bDelete)
	ENDREPEAT
	i = 0
	REPEAT NIGEL1B_TOTAL_GARDENER_VAN_PROPS i
		SAFE_REMOVE_OBJECT(moGardnerVanProps[i].object, bDelete)
	ENDREPEAT
	SAFE_REMOVE_VEHICLE(vehCelebsVehicle, bDelete)
	IF IS_VEHICLE_OK(vehGardenerVehicle)
		IF IS_ENTITY_PLAYING_ANIM(vehGardenerVehicle, "rcmnigel1b", "idle_speedo")
			STOP_ENTITY_ANIM(vehGardenerVehicle, "idle_speedo", "rcmnigel1b", SLOW_BLEND_OUT)
			CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : CLEANUP_ALL_MISSION_ENTITIES() : stopped van sync anim")
		ENDIF
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehGardenerVehicle) 
	ENDIF
	SAFE_REMOVE_VEHICLE(vehGardenerVehicle, bDelete)
	SAFE_REMOVE_VEHICLE(vehIndex_MissionReplayRestore, bDelete)	
ENDPROC

/// PURPOSE:
///    Mission cleanup
/// PARAMS:
///    bDeleteAll - if TRUE all entities are deleted, else released
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC MISSION_CLEANUP(BOOL bDeleteAll = FALSE, BOOL bClearTextSlots = TRUE)
	INT i
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(TRUE)
	ENDIF
	N1B_KILL_CURRENT_CONVERSATION(TRUE, TRUE)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	DISABLE_CELLPHONE(FALSE)
	HIDE_ACTIVE_PHONE(FALSE)
	ALLOW_DIALOGUE_IN_WATER(FALSE)
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	RC_END_CUTSCENE_MODE()
	SET_TIME_SCALE(1)	
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)	
	SET_CREATE_RANDOM_COPS(TRUE)
	CLEAR_PED_NON_CREATION_AREA()	
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingHouse)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max, TRUE)
	
	// only do it here for replays / z skips
	// clear areas which would normally get missed since they occur in the launcher's SETUP_AREA_FOR_MISSION	
	IF IS_REPLAY_IN_PROGRESS()
	OR NOT bFinishedStageSkipping
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearPedZone_HouseArea_Min, vClearPedZone_HouseArea_Max)
		CLEAR_AREA(vCelebritiesHouse_CentralPoint, 35.0, TRUE)	// extended to fix bug 1042629 - Blood stain persisted after retry
		CLEAR_AREA_OF_VEHICLES(vCelebritiesHouse_CentralPoint, 27.0, FALSE)
		CLEAR_AREA_OF_PEDS(vCelebritiesHouse_CentralPoint, 27.0)	
		CPRINTLN(DEBUG_MISSION, "SETUP_WORLD_STATE_FOR_MISSION - simulated SETUP_AREA_FOR_MISSION for replay / skip")
	ENDIF
	
	FOR i = 0 TO (ENUM_TO_INT(N1B_MAX_MISSION_CONTROLLED_DOORS) - 1)
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iMissionControlledDoors[i])
			REMOVE_DOOR_FROM_SYSTEM(iMissionControlledDoors[i])
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " MISSION_CLEANUP - ", " REMOVE_DOOR_FROM_SYSTEM : ", iMissionControlledDoors[i]) ENDIF #ENDIF
		ENDIF
	ENDFOR	
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 2)		// "TREVOR"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)		// "NIGEL"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "MRSTHORNHILL"	/	 "GARDENER"	
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 5)		// "CELEBRITYMALE"		sPoolPed[POOL_PED_CELEB].pedIndex
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 6)		// "CELEBRITYFEMALE"	sPoolPed[POOL_PED_FEMALE].pedIndex
	IF IS_PLAYER_PLAYING(PLAYER_ID())		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE()
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1B_MAIN_MIX")
		STOP_AUDIO_SCENE("NIGEL_1B_MAIN_MIX")
	ENDIF
	SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
	SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)	// reset to default value
	WAIT_FOR_CUTSCENE_TO_STOP()
	REMOVE_RELATIONSHIP_GROUP(relGroupPlayerEnemy)
	RESET_THREAT_SHAPETESTS()
	DELETE_ALL_MISSION_BLIPS()	
	CLEANUP_ALL_MISSION_ENTITIES(bDeleteAll)	
	UNLOAD_ALL_MISSION_ASSETS(bClearTextSlots)	
	#IF IS_DEBUG_BUILD
		CLEANUP_MISSION_WIDGETS()
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - done")
ENDPROC

/// PURPOSE:
///    checks if the player's current weapon is lethal
/// PARAMS:
///    bIncludeProjectilesCheck - if true, checks for grenades,stickybombs etc
///    bIncludeLeathalMeleeWeapons - if true check includes knife, hammer, crowbar etc
///    bIncludeStunGunAsLethal - added parameter as we now want pool peds to surrender to stun gun attack
/// RETURNS:
///    true - if lethal
FUNC BOOL IS_PLAYER_CURRENT_WEAPON_LETHAL(BOOL bIncludeProjectilesCheck = TRUE, BOOL bIncludeLeathalMeleeWeapons = FALSE, BOOL bIncludeStunGunAsLethal = FALSE)					
	//different dialogue based on player weapon
	WEAPON_TYPE currentPlayerWeapon
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
	
	SWITCH currentPlayerWeapon
		CASE WEAPONTYPE_PISTOL
			FALLTHRU
		CASE WEAPONTYPE_COMBATPISTOL
			FALLTHRU
		CASE WEAPONTYPE_APPISTOL
			FALLTHRU
		CASE WEAPONTYPE_MICROSMG
			FALLTHRU
		CASE WEAPONTYPE_SMG
			FALLTHRU
		CASE WEAPONTYPE_DLC_SNSPISTOL
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTRIFLE
			FALLTHRU
		CASE WEAPONTYPE_CARBINERIFLE
			FALLTHRU
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			FALLTHRU
		CASE WEAPONTYPE_ADVANCEDRIFLE
			FALLTHRU
		CASE WEAPONTYPE_MG
			FALLTHRU
		CASE WEAPONTYPE_COMBATMG
			FALLTHRU
		CASE WEAPONTYPE_PUMPSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SNIPERRIFLE
			FALLTHRU
		CASE WEAPONTYPE_HEAVYSNIPER
			FALLTHRU
		CASE WEAPONTYPE_REMOTESNIPER
			FALLTHRU
		CASE WEAPONTYPE_GRENADELAUNCHER
			FALLTHRU
		CASE WEAPONTYPE_RPG
			FALLTHRU
		CASE WEAPONTYPE_MINIGUN
			RETURN TRUE
	ENDSWITCH	
	IF bIncludeProjectilesCheck
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_GRENADE
				FALLTHRU
			CASE WEAPONTYPE_STICKYBOMB
				FALLTHRU
			CASE WEAPONTYPE_MOLOTOV
				RETURN TRUE
		ENDSWITCH
	ENDIF
	IF bIncludeLeathalMeleeWeapons
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_KNIFE
				FALLTHRU
			CASE WEAPONTYPE_HAMMER
				FALLTHRU
			CASE WEAPONTYPE_CROWBAR
				FALLTHRU
			CASE WEAPONTYPE_DLC_BOTTLE
				FALLTHRU
		ENDSWITCH
	ENDIF
	IF bIncludeStunGunAsLethal
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check to see if the player is making the ped frightened.
/// PARAMS:
///    pedIndex - ped to test
/// RETURNS:
///    true if the player is shooting and either bullets are in the area around the ped, player is free aiming at or targetting the ped
FUNC BOOL IS_PLAYER_MAKING_PED_FRIGHTENED(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF	
	IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
	OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
		FLOAT fTargettingDist
		// player is targetting ped, set targetting dist based on whether he is using melee or ranged attack
		//IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
		IF IS_PLAYER_CURRENT_WEAPON_LETHAL(TRUE, FALSE, TRUE)
			fTargettingDist = 40.0 // ranged distance
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fTargettingDist)
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED return true Player in range aim and shooting at ped with weapon : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED gun / projectile dist set : ", fTargettingDist, " : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ELSE
			// commented out as this goes through too easy...ideally i need is_ped_shooting equivalent for melee
			/*fTargettingDist = 3.0 // melee distance
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, fTargettingDist)	// melee distance
				IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED return true Player in range aim and melee at ped : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED melee dist set : ", fTargettingDist, " : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF*/			
		ENDIF
	ENDIF
	IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedIndex), 4.0, TRUE)				
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED return true is Bullet in area : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED return true - Combat and damaged : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	FLOAT fProjectileRadiusCheck = 15.0
	VECTOR vMin, vMax // is one of the player's projectiles nearby?
	vMin = GET_ENTITY_COORDS(pedIndex)
	vMax = vMin
	vMin.x= vMin.x - fProjectileRadiusCheck
	vMin.y = vMin.y -fProjectileRadiusCheck
	vMin.z = vMin.z - fProjectileRadiusCheck
	vMax.x = vMax.x + fProjectileRadiusCheck
	vMax.y = vMax.y + fProjectileRadiusCheck
	vMax.z = vMax.z + fProjectileRadiusCheck
	IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_MAKING_PED_FRIGHTENED return true - projectile in area around ped : FRAME_COUNT = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    switch the celeb's initial position based of the entrance to the pool the player uses, this is to stop him spotting the player
///    straight away.  He will always be facing away from him.
PROC UPDATE_CELEB_IN_POOL_INITIAL_POSITION_BASED_OF_PLAYERS_APPROACH()
	IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_STATE_RELAXED
		IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
			IF IS_ENTITY_VISIBLE_ON_SCREEN(sPoolPed[POOL_PED_CELEB].pedIndex, FALSE)	// as soon as the player spots the celeb in the pool, start him doing his swimming routine
			AND ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA //had to add this test as IS_ENTITY_VISIBLE_ON_SCREEN was returning true when it shouldn't do!
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CELEB_IN_POOL_INITIAL_POSITION_BASED_OF_PLAYERS_APPROACH - set celeb in pool to N1B_PED_AI_SETUP_ROUTINE") ENDIF #ENDIF
				sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_SETUP_ROUTINE
			ELSE
				VECTOR vPoolAreaEntranceCentrePoint
				FLOAT fCheckDist
				IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
					// if the celeb is at the south side, detect player entering from the north
					vPoolAreaEntranceCentrePoint = << -1056.29443, 383.01282, 68.68782 >>
					fCheckDist = 3.0
					IF IS_COORD_IN_RANGE_OF_COORD_2D(vPlayerPos, vPoolAreaEntranceCentrePoint, fCheckDist)		
						iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						SET_ENTITY_COORDS_NO_OFFSET(sPoolPed[POOL_PED_CELEB].pedIndex, vCelebPoolSwimGotos[iCelebInPoolInitialPositionID]) 
						SET_ENTITY_HEADING(sPoolPed[POOL_PED_CELEB].pedIndex, fCelebPoolSwimInitialHeading[iCelebInPoolInitialPositionID]) 
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CELEB_IN_POOL_INITIAL_POSITION_BASED_OF_PLAYERS_APPROACH - set celeb to NIGEL1B_POOL_NORTH_SIDE") ENDIF #ENDIF
					ENDIF
				ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
					// if the celeb is at the north side, detect player entering from the south
					vPoolAreaEntranceCentrePoint = << -1035.07849, 347.51779, 70.36205 >>
					fCheckDist = 4.0
					IF IS_COORD_IN_RANGE_OF_COORD_2D(vPlayerPos, vPoolAreaEntranceCentrePoint, fCheckDist)		
						iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						SET_ENTITY_COORDS_NO_OFFSET(sPoolPed[POOL_PED_CELEB].pedIndex, vCelebPoolSwimGotos[iCelebInPoolInitialPositionID]) 
						SET_ENTITY_HEADING(sPoolPed[POOL_PED_CELEB].pedIndex, fCelebPoolSwimInitialHeading[iCelebInPoolInitialPositionID]) 
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CELEB_IN_POOL_INITIAL_POSITION_BASED_OF_PLAYERS_APPROACH - set celeb to NIGEL1B_POOL_SOUTH_SIDE") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check the specific ped is playing on of the aware of player anims
/// PARAMS:
///    pedIndex - specific ped to check
/// RETURNS:
///    TRUE if ped is playing one of the anims
FUNC BOOL IS_PED_PLAYING_AWARE_OF_PLAYER_ANIM(PED_INDEX &pedIndex)
	IF IS_PED_UNINJURED(pedIndex)
		IF IS_ENTITY_PLAYING_ANIM(pedIndex, tlAnimDict_PedAwareIdles, "react_small_variations_k")
		OR IS_ENTITY_PLAYING_ANIM(pedIndex, tlAnimDict_PedAwareIdles, "react_small_variations_m")
		OR IS_ENTITY_PLAYING_ANIM(pedIndex, tlAnimDict_PedAwareIdles, "react_small_variations_n")
		OR IS_ENTITY_PLAYING_ANIM(pedIndex, tlAnimDict_PedAwareIdles, "react_small_variations_o")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles ped's playing anims when they are stood around looking at the player
/// PARAMS:
///    pedIndex - the specific ped to apply the anim to
///    sSpecificAnim - specify an anim to play, otherwise pass NULL or empty for random selected
///    fBlendInDelta - speed to blend the anim in
///    fBlendOutDelta - speed to blend the anim out
///    nTimeToPlay - the time the animation has to play
///    AnimFlags - 
///    startPhase - which phase in the anim to start from
///    phaseControlled - 
///    ikFlags - 
/// RETURNS:
///    TRUE if the anim was tasked successfully
FUNC BOOL SET_PED_PLAY_AWARE_OF_PLAYER_ANIM(PED_INDEX &pedIndex, STRING sSpecificAnim = NULL, FLOAT fBlendInDelta = NORMAL_BLEND_IN, FLOAT fBlendOutDelta = NORMAL_BLEND_OUT, INT nTimeToPlay =-1, ANIMATION_FLAGS AnimFlags = AF_DEFAULT, FLOAT startPhase = 0.0, BOOL phaseControlled = FALSE, IK_CONTROL_FLAGS ikFlags = AIK_NONE)
	IF IS_PED_UNINJURED(pedIndex)
		REQUEST_ANIM_DICT(tlAnimDict_PedAwareIdles)	
		IF HAS_ANIM_DICT_LOADED(tlAnimDict_PedAwareIdles)	
			IF IS_STRING_NULL_OR_EMPTY(sSpecificAnim)
				// pick a random anim to play
				INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				SWITCH iRand
					CASE 0
						sSpecificAnim = "react_small_variations_k"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - set random anim : ", sSpecificAnim, " iRand : ", iRand) ENDIF #ENDIF
					BREAK
					CASE 1
						sSpecificAnim = "react_small_variations_m"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - set random anim : ", sSpecificAnim, " iRand : ", iRand) ENDIF #ENDIF
					BREAK
					CASE 2
						sSpecificAnim = "react_small_variations_n"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - set random anim : ", sSpecificAnim, " iRand : ", iRand) ENDIF #ENDIF
					BREAK
					CASE 3
						sSpecificAnim = "react_small_variations_o"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - set random anim : ", sSpecificAnim, " iRand : ", iRand) ENDIF #ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			//CLEAR_PED_TASKS(pedIndex)
			TASK_PLAY_ANIM(pedIndex, tlAnimDict_PedAwareIdles, sSpecificAnim, fBlendInDelta, fBlendOutDelta, nTimeToPlay, AnimFlags, startPhase, phaseControlled, ikFlags)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - anim applied : ", sSpecificAnim, " : RETURN TRUE") ENDIF #ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - waiting on anim request to load : ", tlAnimDict_PedAwareIdles) ENDIF #ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_PLAY_AWARE_OF_PLAYER_ANIM - RETURN FALSE") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    gets the celeb to blend out of the dancy scene to head over the clothes
///    and sets the women back into her idle anim
///    they'll only be using the dance sync scene at this point
/// RETURNS:
///    TRUE if sync was ended successfully
FUNC BOOL SET_CELEB_TO_STOP_DANCING_FOR_CLOTHES_SEARCH()
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP])
		IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP]) > 0.2554
		AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP]) < 0.295
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP], FALSE)
			//SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP], 1.0)	// have to do this to end it I believe
			DETACH_SYNCHRONIZED_SCENE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP])
			
			SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_BASE_FEMALE_ONLY)
			sPoolPed[POOL_PED_FEMALE].AI = N1B_PED_AI_STATE_RELAXED
			
			IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
				IF IsPedPerformingTask(sPoolPed[POOL_PED_CELEB].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[POOL_PED_CELEB].pedIndex, REALLY_SLOW_BLEND_OUT, TRUE)		
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CELEB_TO_STOP_DANCING_FOR_CLOTHES_SEARCH - * set blend out task for CELEB  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES
			ENDIF
	
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CELEB_TO_STOP_DANCING_FOR_CLOTHES_SEARCH - * set loop false and scene to phase 1.0 to stop  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check the female has finished her stand up sync anim.  Handles triggering and checking it's finished
/// RETURNS:
///    TRUE if the female ped has finished her stand up anim
FUNC BOOL HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM()
	
	IF IS_PED_UNINJURED(sPoolPed[POOL_PED_FEMALE].pedIndex)
		IF NOT bDoneStandUpSceneForFemalePoolPed
			IF IsPedPerformingTask(sPoolPed[POOL_PED_FEMALE].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				// if this dance outro is running wait for it to end other wise apply straight away
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY])
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY]) > 0.98
						SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_STAND_UP_FEMALE_ONLY)
						bDoneStandUpSceneForFemalePoolPed = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM - set N1B_SSAT_STAND_UP_FEMALE_ONLY for task ss and active dance outro scene exit scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ELSE
					SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_STAND_UP_FEMALE_ONLY)
					bDoneStandUpSceneForFemalePoolPed = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM - set N1B_SSAT_STAND_UP_FEMALE_ONLY for task ss but not active dance outro scene exit scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF			
			ELSE
				bDoneStandUpSceneForFemalePoolPed = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM - set N1B_SSAT_STAND_UP_FEMALE_ONLY scene skipped for ped not having task ss scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF			
		ELSE
			// if scenes aren't running check ped has finished sync scene task
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_STAND_UP_FEMALE_ONLY])
				IF NOT IsPedPerformingTask(sPoolPed[POOL_PED_FEMALE].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM - return TRUE FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    if the specific ped has finished their surrender reaction animation.  Handles triggering and checking it's finished
/// PARAMS:
///    iPoolPedIndex - specific ped to check
/// RETURNS:
///    TRUE if the specfic ped has finished their surrender reaction anim
FUNC BOOL HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM(INT iPoolPedIndex)
	
	// decide which exit scene to play - if celeb is in synced scene both will need exit
	IF iPoolPedIndex = POOL_PED_CELEB
		IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
			IF NOT bDoneDanceOutroSceneForPoolPeds[POOL_PED_CELEB]
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_ENTER_CELEB_ONLY])
					// only use his dance outro anim if he's in the tub
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_ENTER_CELEB_ONLY]) > 0.55
						SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_OUTRO_CELEB_ONLY)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set N1B_SSAT_DANCE_OUTRO_CELEB_ONLY exit scene from N1B_SSAT_ENTER_CELEB_ONLY  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ELSE
						STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[POOL_PED_CELEB].pedIndex, SLOW_BLEND_OUT, TRUE)
						SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_ENTER_CELEB_ONLY], 1.0)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set blend out of N1B_SSAT_ENTER_CELEB_ONLY scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP])
				OR IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_INTRO])
					SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_OUTRO_CELEB_ONLY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set N1B_SSAT_DANCE_OUTRO_CELEB_ONLY exit scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				bDoneDanceOutroSceneForPoolPeds[POOL_PED_CELEB] = TRUE
			ELSE
				// if scene isn't running check ped has finished sync scene task
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_OUTRO_CELEB_ONLY])
					IF NOT IsPedPerformingTask(sPoolPed[iPoolPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - return TRUE for iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF iPoolPedIndex = POOL_PED_FEMALE
		IF IS_PED_UNINJURED(sPoolPed[POOL_PED_FEMALE].pedIndex)
			IF NOT bDoneDanceOutroSceneForPoolPeds[POOL_PED_FEMALE]						
				IF IsPedPerformingTask(sPoolPed[POOL_PED_FEMALE].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
					SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set N1B_SSAT_DANCE_OUTRO_FEMALE_ONLY exit scene  FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				bDoneDanceOutroSceneForPoolPeds[POOL_PED_FEMALE] = TRUE
			ELIF HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM()
				// if scenes aren't running check ped has finished sync scene task
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_OUTRO_CELEB_ONLY])
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_STAND_UP_FEMALE_ONLY])
					IF NOT IsPedPerformingTask(sPoolPed[iPoolPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - return TRUE for iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	/*IF IS_PED_UNINJURED(sPoolPed[iPoolPedIndex].pedIndex)	
		IF iPoolPedIndex = POOL_PED_CELEB		
			// apply the reaction animation
			//IF IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_intro_tyler")
			//OR IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_loop_tyler")
			IF IsPedPerformingTask(sPoolPed[iPoolPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
			
				VECTOR vPos, vRot
				//vPos = GET_ANIM_INITIAL_OFFSET_POSITION(tlAnimDict_MissionMocappedAnims, "react_tyler", vSyncScene_HotTub_Rotation, vSyncScene_HotTub_Rotation)
				//vRot = GET_ANIM_INITIAL_OFFSET_ROTATION(tlAnimDict_MissionMocappedAnims, "react_tyler", vSyncScene_HotTub_Rotation, vSyncScene_HotTub_Rotation)				
				STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)				
				TASK_PLAY_ANIM_ADVANCED(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_tyler", vPos, vRot, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET)				
				//TASK_PLAY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_tyler", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[iPoolPedIndex].pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set exit anim from anim task iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
				
			// test for the celeb finishing her reaction animation
			ELIF NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_tyler")			
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - return TRUE for iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
			
		ELIF iPoolPedIndex = POOL_PED_FEMALE
		
			// apply the reaction animation
			IF IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_intro_girl")
			OR IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_loop_girl")
			
				TASK_PLAY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_girl", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[iPoolPedIndex].pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set exit anim from anim task iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
			
			// apply the reaction animation
			ELIF IsPedPerformingTask(sPoolPed[iPoolPedIndex].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
			
				STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, NORMAL_BLEND_OUT, FALSE)
				TASK_PLAY_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_girl", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[iPoolPedIndex].pedIndex)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - set exit anim from sync scene task iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
				
			// test for the female finishing her reaction animation
			ELIF NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_girl")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM - return TRUE for iPoolPedIndex :", iPoolPedIndex) ENDIF #ENDIF
				RETURN TRUE
			ELSE
				SET_ENTITY_ANIM_SPEED(sPoolPed[iPoolPedIndex].pedIndex, tlAnimDict_MissionMocappedAnims, "react_girl", 1.2)
			ENDIF
		ENDIF	
	ENDIF*/
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Controls pool peds behaviour.
/// PARAMS:
///    i - array index of the pool ped we are going to update
PROC CONTROL_POOL_PEDS_AI(INT i)
	IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
		SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_PreventAllMeleeTakedowns, TRUE)	// must be called everyframe to prevent takedown and takedown failed
		IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	//having to test this because changes to tasks breaks the execution/takedown moves
			SWITCH (sPoolPed[i].AI)			
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_RELAXED
					//currently an idle state
					IF i = POOL_PED_CELEB
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_STAND_STILL)
							TASK_STAND_STILL(sPoolPed[i].pedIndex, -1)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_RELAXED set for celeb ID = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_ROUTINE				//only used by celeb in pool for now - swim the first length
					IF i = POOL_PED_CELEB
						// swim over to opposite side of the pool
						INT iGotoPos
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iGotoPos = NIGEL1B_POOL_NORTH_SIDE
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iGotoPos = NIGEL1B_POOL_SOUTH_SIDE
						ENDIF										
						OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_GO_STRAIGHT_TO_COORD(NULL, vCelebPoolSwimGotos[iGotoPos], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP, fCelebPoolSwimArrivalHeading[iGotoPos])
							//TASK_ACHIEVE_HEADING(NULL, fCelebPoolSwimInitialHeading[iGotoPos], 500)	
						CLOSE_SEQUENCE_TASK(sequenceAI)		
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
						sPoolPed[i].AI = N1B_PED_AI_STATE_ROUTINE
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_SETUP_ROUTINE -> N1B_PED_AI_STATE_ROUTINE", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_ROUTINE				//only used by celeb in pool for now - swim the first length
					IF i = POOL_PED_CELEB	
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							//IF IS_COORD_IN_RANGE_OF_COORD_2D(GET_ENTITY_COORDS(sPoolPed[i].pedIndex), vCelebPoolSwimGotos[iGotoPos], 5.0)
							//TASK_STAND_STILL(sPoolPed[i].pedIndex, 500)
							sPoolPed[i].AI = N1B_PED_AI_SETUP_ROUTINE_SECONDARY
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_STATE_ROUTINE -> N1B_PED_AI_SETUP_ROUTINE_SECONDARY", " id = ", i) ENDIF #ENDIF
						ELSE
							//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
	              		ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_ROUTINE_SECONDARY				//only used by celeb in pool for now - swim the second length - done like this to fix bug 1023500
					IF i = POOL_PED_CELEB
						// swim back to side of the pool he started at
						INT iGotoPos
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iGotoPos = NIGEL1B_POOL_SOUTH_SIDE
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iGotoPos = NIGEL1B_POOL_NORTH_SIDE
						ENDIF									
						OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_GO_STRAIGHT_TO_COORD(NULL, vCelebPoolSwimGotos[iGotoPos], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP, fCelebPoolSwimArrivalHeading[iGotoPos])
							//TASK_ACHIEVE_HEADING(NULL, fCelebPoolSwimInitialHeading[iGotoPos], 500)	
						CLOSE_SEQUENCE_TASK(sequenceAI)		
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
						sPoolPed[i].AI = N1B_PED_AI_STATE_ROUTINE_SECONDARY
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_SETUP_ROUTINE_SECONDARY -> N1B_PED_AI_STATE_ROUTINE_SECONDARY", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_ROUTINE_SECONDARY				//only used by celeb in pool for now - swim the second length - done like this to fix bug 1023500
					IF i = POOL_PED_CELEB	
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							//IF IS_COORD_IN_RANGE_OF_COORD_2D(GET_ENTITY_COORDS(sPoolPed[i].pedIndex), vCelebPoolSwimGotos[iGotoPos], 5.0)
							//TASK_STAND_STILL(sPoolPed[i].pedIndex, 500)
							sPoolPed[i].AI = N1B_PED_AI_STATE_ROUTINE_COMPLETED
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_STATE_ROUTINE_SECONDARY -> N1B_PED_AI_STATE_ROUTINE_COMPLETED", " id = ", i) ENDIF #ENDIF
	              		ELSE
							//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_ROUTINE_COMPLETED				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
					ENDIF
				BREAK
				//	------------------------------------------				
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_FEMALE].pedIndex)	// also requires female ped to be alive for tasks
							INT iFinalSwimLengthPos
							IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
								iFinalSwimLengthPos = NIGEL1B_POOL_NORTH_SIDE
							ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
								iFinalSwimLengthPos = NIGEL1B_POOL_SOUTH_SIDE
							ENDIF
							OPEN_SEQUENCE_TASK(sequenceAI)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vCelebPoolSwimGotos[iFinalSwimLengthPos], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP) //, fCelebPoolSwimArrivalHeading[iFinalSwimLengthPos])							
								//TASK_TURN_PED_TO_FACE_ENTITY(NULL, sPoolPed[POOL_PED_FEMALE].pedIndex), -1)
							CLOSE_SEQUENCE_TASK(sequenceAI)		
							CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
							TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
							CLEAR_SEQUENCE_TASK(sequenceAI)
							//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH -> N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH", " id = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH 			//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						INT iFinalSwimLengthPos
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iFinalSwimLengthPos = NIGEL1B_POOL_NORTH_SIDE
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iFinalSwimLengthPos = NIGEL1B_POOL_SOUTH_SIDE
						ENDIF
						IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_FEMALE].pedIndex)	// also requires female ped to be alive for tasks
							IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)	
								IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vCelebPoolSwimGotos[iFinalSwimLengthPos], 2.5)
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH -> N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL done task", " id = ", i) ENDIF #ENDIF
									sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH -> N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH reapply", " id = ", i) ENDIF #ENDIF
									sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH								
								ENDIF
							ELSE
								//SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_AllowTasksIncompatibleWithMotion, TRUE)	// added to make the ped swim again...seen from bug 1019576
							ENDIF
						ENDIF
					ENDIF
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						INT iLeavePoolSide
						// get out the opposite side to where he started, as this will be where the player is
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_NORTH_SIDE
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_SOUTH_SIDE
						ENDIF
						OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_GO_STRAIGHT_TO_COORD(NULL, vStepOutOfPoolPos[iLeavePoolSide], PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, fStepOutOfPoolHeading[iLeavePoolSide])	//TASK_GO_STRAIGHT_TO_COORD gets stuck on lip
							TASK_CLIMB(NULL, FALSE)
						CLOSE_SEQUENCE_TASK(sequenceAI)		
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)		
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL -> N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						INT iLeavePoolSide
						// get out the opposite side to where he started, as this will be where the player is
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_NORTH_SIDE
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_SOUTH_SIDE
						ENDIF
						IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vPedLeftPoolPos[iLeavePoolSide], fDistToPoolEdge)
							IF IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)	
								CLEAR_PED_TASKS_IMMEDIATELY(sPoolPed[i].pedIndex)	//having to do this as TASK_CLIMB makes him walk for too long after the climb
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL -> N1B_PED_AI_CELEB_ONLY_SETUP_POSE done, task cleared as out water", " id = ", i) ENDIF #ENDIF
								sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED
							ENDIF
						ELSE	
							IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)	
								IF NOT IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
								AND NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
								AND NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
								AND NOT IS_PED_CLIMBING(sPoolPed[i].pedIndex)	// added for bug 970159
									CLEAR_PED_TASKS_IMMEDIATELY(sPoolPed[i].pedIndex)	//having to do this as TASK_CLIMB makes him walk for too long after the climb
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL -> N1B_PED_AI_CELEB_ONLY_SETUP_POSE done, task ended unexpected but already out of water", " id = ", i) ENDIF #ENDIF
									sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_LEAVE_POOL -> N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL reapply task", " id = ", i) ENDIF #ENDIF
									sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_LEAVE_POOL
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED				//only used by celeb in pool for now
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_POSE				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						OPEN_SEQUENCE_TASK(sequenceAI)	
							TASK_PLAY_ANIM(NULL, tlAnimDict_MissionMocappedAnims, "that_had_to_be_tyler", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
						CLOSE_SEQUENCE_TASK(sequenceAI)		
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)		
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
						SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_UseHeadOrientationForPerception, TRUE)	// B*1537407 - makes sure CAN_PED_SEE_HATED_PED check accoun the ped's head rotation
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_POSE
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_POSE -> N1B_PED_AI_CELEB_ONLY_STATE_POSE", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_POSE				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)									
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_POSE -> N1B_PED_AI_CELEB_ONLY_STATE_POSE_COMPLETED done task ", " id = ", i) ENDIF #ENDIF
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_POSE_COMPLETED
						ELSE
							SET_PED_RESET_FLAG(sPoolPed[i].pedIndex, PRF_UseHeadOrientationForPerception, TRUE)	// B*1537407 - makes sure CAN_PED_SEE_HATED_PED check accoun the ped's head rotation
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_POSE_COMPLETED				//only used by celeb in pool for now
				BREAK
				
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						INT iLeavePoolSide
						IF iCelebInPoolInitialPositionID = NIGEL1B_POOL_SOUTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_NORTH_SIDE	// , 158.0680 this is North pos, as we end up this side
						ELIF iCelebInPoolInitialPositionID = NIGEL1B_POOL_NORTH_SIDE
							iLeavePoolSide = NIGEL1B_POOL_SOUTH_SIDE		// , 16.5588 this is South pos, as we end up this side
						ENDIF				
																			
						VECTOR vEnterTubPosition, vEnterTubOrientation
						//vEnterTubPosition =  << -1061.36, 363.19, 70.84 >>	hard coded
						//vEnterTubOrientation = << 0.0, 0.0, -119.30 >>		hard coded						
						vEnterTubPosition = GET_ANIM_INITIAL_OFFSET_POSITION(tlAnimDict_MissionMocappedAnims, "ENTER", vSyncScene_HotTub_Position, vSyncScene_HotTub_Rotation)
						vEnterTubOrientation = GET_ANIM_INITIAL_OFFSET_ROTATION(tlAnimDict_MissionMocappedAnims, "ENTER", vSyncScene_HotTub_Position, vSyncScene_HotTub_Rotation)
						CPRINTLN(DEBUG_MISSION, "GENERATED pos and rot for ENTER anim - pos = ", vEnterTubPosition, " rot = ", vEnterTubOrientation)		
						
						SET_PED_PATH_PREFER_TO_AVOID_WATER(sPoolPed[i].pedIndex, TRUE)
						SET_PED_PATH_MAY_ENTER_WATER(sPoolPed[i].pedIndex, FALSE)
						SET_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "walk_tyler")
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB -> set Tyler alt walk anims", " id = ", i) ENDIF #ENDIF
						
						OPEN_SEQUENCE_TASK(sequenceAI)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGotowardsHotTubInitialPos[iLeavePoolSide], PEDMOVEBLENDRATIO_WALK, 
															DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)	// ENAV_NO_STOPPING added to make not between gotos																
							//TASK_GO_STRAIGHT_TO_COORD(NULL, vEnterTubPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, vEnterTubOrientation.z)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vEnterTubPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP,
																DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, vEnterTubOrientation.z)
						CLOSE_SEQUENCE_TASK(sequenceAI)					
						//CLEAR_PED_TASKS(sPoolPed[i].pedIndex)				
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)					
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							VECTOR vEnterTubPosition
							//vEnterTubPosition =  << -1061.36, 363.19, 70.84 >>	hard coded
							vEnterTubPosition = GET_ANIM_INITIAL_OFFSET_POSITION(tlAnimDict_MissionMocappedAnims, "ENTER", vSyncScene_HotTub_Position, vSyncScene_HotTub_Rotation)
							
							IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vEnterTubPosition, 1.0)
								CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
								sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM", " id = ", i) ENDIF #ENDIF
		              		ELSE
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_GOTO_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB", " id = ", i) ENDIF #ENDIF
								sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB
							ENDIF
						ENDIF
					ENDIF
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						/*REQUEST_ANIM_DICT(tlAnimDict_MissionMocappedAnims)
						IF HAS_ANIM_DICT_LOADED(tlAnimDict_MissionMocappedAnims)	
							VECTOR vEnterTubPosition, vEnterTubOrientation
							vEnterTubPosition = << -1061.36, 363.19, 70.84 >> // << -1061.618, 362.785, 70.72 >>	// 70.911 >>
							vEnterTubOrientation = << 0.0, 0.0, -119.30 >>	// << 0.000, 0.000, -107.0 >>	// 105.120 >>
							TASK_PLAY_ANIM_ADVANCED(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "ENTER", vEnterTubPosition, vEnterTubOrientation,
														SLOW_BLEND_IN, SLOW_BLEND_OUT, -1) // AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY | AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[i].pedIndex)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM -> N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
						ENDIF*/
						SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_ENTER_CELEB_ONLY)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM -> N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
					ENDIF
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_ENTER_HOT_TUB_ANIM				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB					
						IF IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PLAY_ANIM)
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_ENTER_HOT_TUB_ANIM -> N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
						ELSE
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_ENTER_HOT_TUB_ANIM -> N1B_PED_AI_CELEB_ONLY_SETUP_ENTER_HOT_TUB_ANIM", " id = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						/*IF NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "ENTER")
							SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_INTRO)
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
							sPoolPed[POOL_PED_FEMALE].AI = N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "Female AI Updated to Match dance anim started -> N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB") ENDIF #ENDIF
						ENDIF*/
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_ENTER_CELEB_ONLY])
							IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_ENTER_CELEB_ONLY]) > 0.999
								SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_INTRO)
								sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
								sPoolPed[POOL_PED_FEMALE].AI = N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "Female AI Updated to Match dance anim started -> N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB") ENDIF #ENDIF
							ENDIF
						ELSE
							SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_INTRO)
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
							sPoolPed[POOL_PED_FEMALE].AI = N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "Female AI Updated to Match dance anim started -> N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB") ENDIF #ENDIF
						ENDIF	
					ENDIF					
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB				//only used by celeb in pool for now
					IF i = POOL_PED_CELEB
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP])
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_INTRO])
								IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_INTRO]) > 0.999
									SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_LOOP)
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB -> N1B_SSAT_DANCE_LOOP set interupted end of N1B_SSAT_DANCE_INTRO", " id = ", i) ENDIF #ENDIF
								ENDIF
							ELSE
								SET_POOL_PEDS_SYNCED_ANIM(N1B_SSAT_DANCE_LOOP)
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB -> N1B_SSAT_DANCE_LOOP set N1B_SSAT_DANCE_INTRO had finished", " id = ", i) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					/*IF i = POOL_PED_CELEB
						IF NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_intro_tyler")
						AND NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_loop_tyler")
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB -> N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF*/
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB				//only used by female in hot tub
					/*IF i = POOL_PED_FEMALE
						REQUEST_ANIM_DICT(tlAnimDict_MissionMocappedAnims)
						IF HAS_ANIM_DICT_LOADED(tlAnimDict_MissionMocappedAnims)
							IF IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sPoolPed[i].pedIndex, SLOW_BLEND_OUT, FALSE)
							ENDIF
							OPEN_SEQUENCE_TASK(sequenceAI)
								TASK_PLAY_ANIM(NULL, tlAnimDict_MissionMocappedAnims, "dance_intro_girl", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION)
								TASK_PLAY_ANIM(NULL, tlAnimDict_MissionMocappedAnims, "dance_loop_girl", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_TURN_OFF_COLLISION)
							CLOSE_SEQUENCE_TASK(sequenceAI)					
							CLEAR_PED_TASKS(sPoolPed[i].pedIndex)					
							TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
							CLEAR_SEQUENCE_TASK(sequenceAI)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sPoolPed[i].pedIndex)	// fixes pop issue when ped changes to new anims
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
							sPoolPed[i].AI = N1B_PED_AI_FEMALE_ONLY_STATE_WATCH_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB -> N1B_PED_AI_FEMALE_ONLY_STATE_WATCH_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF*/
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_FEMALE_ONLY_STATE_WATCH_DANCE_IN_HOT_TUB				//only used by female in hot tub
					/*IF i = POOL_PED_FEMALE
						IF NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_intro_girl")
						AND NOT IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, tlAnimDict_MissionMocappedAnims, "dance_loop_girl")
							sPoolPed[i].AI = N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "N1B_PED_AI_FEMALE_ONLY_STATE_WATCH_DANCE_IN_HOT_TUB -> N1B_PED_AI_FEMALE_ONLY_SETUP_WATCH_DANCE_IN_HOT_TUB", " id = ", i) ENDIF #ENDIF
						ENDIF
					ENDIF*/
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenesID_PoolPeds[N1B_SSAT_DANCE_LOOP])
					AND NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
						OPEN_SEQUENCE_TASK(sequenceAI)											
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE], PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vCelebGoToClothesPos, PEDMOVEBLENDRATIO_WALK, 
															DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 223.4962)
							TASK_PLAY_ANIM(NULL, "rcmnigel1b", "wipehead", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
							TASK_STAND_STILL(NULL, -1)
						CLOSE_SEQUENCE_TASK(sequenceAI)					
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)					
						TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)					
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES
					IF IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vCelebGoToClothesPos, NIGEL1B_RANGE_CELEB_PED_CLOSE_TO_CLOTHES_PICKUP)
						bHasCelebReachedClothesLocation = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES done, set bHasCelebReachedClothesLocation TRUE ", " id = ", i) ENDIF #ENDIF
					ELSE
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES -> N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES no task", " id = ", i) ENDIF #ENDIF
							sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES
		              	ENDIF
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES
					OPEN_SEQUENCE_TASK(sequenceAI)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1049.9147, 357.2381, 68.9132 >>, PEDMOVEBLENDRATIO_WALK, 
														DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 71.1227)
						TASK_STAND_STILL(NULL, 1000)	//look for a scratch head anim instead
						//play a searching anim here						
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1048.4167, 364.7032, 68.9129 >>, PEDMOVEBLENDRATIO_WALK, 
														DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 6.9753)
						TASK_STAND_STILL(NULL, 2000)	//look for a scratch head anim instead
						//play a searching anim here	
						SET_SEQUENCE_TO_REPEAT(sequenceAI, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(sequenceAI)					
					CLEAR_PED_TASKS(sPoolPed[i].pedIndex)				
					TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
					CLEAR_SEQUENCE_TASK(sequenceAI)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)					
					#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES -> N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES", " id = ", i) ENDIF #ENDIF
					sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
	                	// PED HAS LOST HIS TASK, RESET IT
	                	sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES - PED ID = ", i) ENDIF #ENDIF
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_SEARCH_FOR_CLOTHES -> N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES no task", " id = ", i) ENDIF #ENDIF
	              	ENDIF
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS
					//check if we need to get the celeb into a suitable pos for the phonecall
					IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
					OR IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
					OR IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
						VECTOR vGotoPos
						FLOAT fGoToHeading
						vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE]	// north side of the HOT_TUB
						fGoToHeading = 201.7237						
						//IF VDIST2(vPlayerPos, vGotoPos) > 
						IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vGotoPos, 2.5)	// if player is at that spot go for the opposite sid
							vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE]	// south side of the HOT_TUB
							fGoToHeading = 325.0476
						ENDIF
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(sPoolPed[i].pedIndex, vGotoPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fGoToHeading)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS task finished", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS
					ELSE
						IF NOT IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							IF NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
								IF NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
									IF NOT IS_PED_CLIMBING(sPoolPed[i].pedIndex)	// added for bug 970159
										IF NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.13635, 369.17224, 69.18319 >>, 1.25)	// B*1218631 - block stopping on North corner of pool where feet clip through the wall
											IF NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.10815, 354.76880, 69.19383 >>, 1.25)	// B*1218631 - block stopping on South corner of pool where feet clip through the wall
												IF NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, <<-1051.339722,358.404541,66.914551>>, <<-1051.361816,365.510620,71.914162>>, 2.250000)	// B*1327618 - block stopping in shrubs next to pool
												//AND NOT GET_IS_TASK_ACTIVE(sPoolPed[i].pedIndex, CODE_TASK_CLIMB_LADDER)
													CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
													#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_GET_TO_POS_FOR_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS", " id = ", i) ENDIF #ENDIF
													sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF							
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS
					IF NOT IS_ENTITY_ALIVE(moCelebPhone.object)
						IF DOES_ENTITY_EXIST(moCelebPhone.object)	// if the phone already exists but is dead, delete it and create a new one
							DELETE_OBJECT(moCelebPhone.object)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS -> moCelebPhone existed but dead so deleted") ENDIF #ENDIF
						ENDIF
						REQUEST_MODEL(moCelebPhone.model)
						IF HAS_MODEL_LOADED(moCelebPhone.model)
							moCelebPhone.object = CREATE_OBJECT(moCelebPhone.model, (GET_ENTITY_COORDS(sPoolPed[i].pedIndex) + << 0.0, 0.0, -10.0 >>))	//spawn under ground
							SET_MODEL_AS_NO_LONGER_NEEDED(moCelebPhone.model)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS -> moCelebPhone.object created!") ENDIF #ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(moCelebPhone.object, sPoolPed[i].pedIndex)
							ATTACH_ENTITY_TO_ENTITY(moCelebPhone.object, sPoolPed[i].pedIndex, GET_PED_BONE_INDEX(sPoolPed[i].pedIndex, BONETAG_PH_R_HAND),
													moCelebPhone.vPosition, moCelebPhone.vRotation, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS -> moCelebPhone.object attached to ped", " id = ", i) ENDIF #ENDIF
						ENDIF
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1, SLF_NARROWEST_PITCH_LIMIT | SLF_NARROWEST_YAW_LIMIT)
						TASK_USE_MOBILE_PHONE(sPoolPed[i].pedIndex, TRUE)
						/*
						OPEN_SEQUENCE_TASK(sequenceAI)
							//IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							VECTOR vTempGotoNorthSide
							VECTOR vTempGotoSouthSide
							VECTOR vCurrentCoords
							vCurrentCoords = GET_ENTITY_COORDS(sPoolPed[i].pedIndex)
							IF IS_COORD_IN_RANGE_OF_COORD(vCurrentCoords, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
								vTempGotoNorthSide = << -1059.4672, 364.6955, 69.7516 >>
								vTempGotoSouthSide = << -1059.9067, 359.1591, 69.7516 >>
								IF (VDIST2(vTempGotoNorthSide, vCurrentCoords)
								< VDIST2(vTempGotoSouthSide, vCurrentCoords))	// if player is closer to north side point, go there
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTempGotoNorthSide, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS - GET OUT OF HOT_TUB COORD NORTH SIDE = ", vTempGotoNorthSide, " GIVEN PED ID = ", i) ENDIF #ENDIF
								ELSE											// else go to the south point
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTempGotoSouthSide, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS - GET OUT OF HOT_TUB COORD NORTH SIDE = ", vTempGotoSouthSide, " GIVEN PED ID = ", i) ENDIF #ENDIF
								ENDIF							
							ELIF IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool							
								vTempGotoNorthSide = << -1054.5756, 371.9297, 68.9139 >>
								vTempGotoSouthSide = << -1054.2416, 352.0960, 68.9139 >>
								IF (VDIST2(vTempGotoNorthSide, vCurrentCoords)
								< VDIST2(vTempGotoSouthSide, vCurrentCoords))	// if player is closer to north side point, go there
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTempGotoNorthSide, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS - GET OUT OF POOL COORD NORTH SIDE = ", vTempGotoNorthSide, " GIVEN PED ID = ", i) ENDIF #ENDIF
								ELSE											// else go to the south point
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTempGotoSouthSide, PEDMOVEBLENDRATIO_WALK)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS - GET OUT OF POOL COORD NORTH SIDE = ", vTempGotoSouthSide, " GIVEN PED ID = ", i) ENDIF #ENDIF
								ENDIF
							ENDIF
							TASK_USE_MOBILE_PHONE(NULL, TRUE)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						CLOSE_SEQUENCE_TASK(sequenceAI)	
						*/					
						//TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
						//CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
						iTimer_DelayForPhoneTask = GET_GAME_TIMER()
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS
					ENDIF					
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(sPoolPed[i].pedIndex)
						IF NOT IS_PED_HEADTRACKING_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1, SLF_NARROWEST_PITCH_LIMIT | SLF_NARROWEST_YAW_LIMIT)
						ENDIF
						IF (GET_GAME_TIMER() - iTimer_DelayForPhoneTask > 1500)	//delay for phone actually making it up to the ear
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS detected ped using mobile phone task", " id = ", i) ENDIF #ENDIF
							bCelebOkToStartPhoneConvo = TRUE
						ENDIF
					ELSE
						bCelebOkToStartPhoneConvo = FALSE
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS -> N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS reapply task", " id = ", i) ENDIF #ENDIF
					ENDIF					
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(sPoolPed[i].pedIndex)
						TASK_USE_MOBILE_PHONE(sPoolPed[i].pedIndex, FALSE)
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
						//CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
						//CLEAR_PED_SECONDARY_TASK(sPoolPed[i].pedIndex)	// mobile phone task is now classed as secondary but this won't show him put the phone back down
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL -> IS_PED_RUNNING_MOBILE_PHONE_TASK - TASK_USE_MOBILE_PHONE(sPoolPed[i].pedIndex, FALSE) ", " id = ", i) ENDIF #ENDIF
					ELSE
						SAFE_REMOVE_OBJECT(moCelebPhone.object, TRUE)
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL -> N1B_PED_AI_CELEB_ONLY_STATE_END_PHONECALL", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_END_PHONECALL		
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_CELEB_ONLY_STATE_END_PHONECALL
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_AWARE_OF_PLAYER			
					IF IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, "rcmnigel1b", "wipehead")
						STOP_ANIM_TASK(sPoolPed[i].pedIndex, "rcmnigel1b", "wipehead", SLOW_BLEND_OUT)
					ENDIF
					IF i = POOL_PED_CELEB
						CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
						//#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_PLAYER - alt walk anim cleared", " id = ", i) ENDIF #ENDIF
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	//B*1051019 - Tyler didn't turn to face ped
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
						TASK_TURN_PED_TO_FACE_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
						sPoolPed[i].iTimer = GET_GAME_TIMER()
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_PLAYER", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_STATE_AWARE_OF_PLAYER
					ELIF i = POOL_PED_FEMALE
						IF HAS_FEMALE_POOL_PED_FINISHED_STAND_UP_ANIM()
							TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
							TASK_TURN_PED_TO_FACE_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
							sPoolPed[i].iTimer = GET_GAME_TIMER()
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_PLAYER", " id = ", i) ENDIF #ENDIF
							sPoolPed[i].AI = N1B_PED_AI_STATE_AWARE_OF_PLAYER
						ENDIF
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_AWARE_OF_PLAYER
					// allow idle anims
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PLAY_ANIM)
						IF NOT IS_PED_FACING_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID(), 45.0)
							IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_PLAYER -> N1B_PED_AI_SETUP_AWARE_OF_PLAYER ID = ", i) ENDIF #ENDIF
								sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
							ENDIF
						ELSE								
							IF HAS_TIME_PASSED(sPoolPed[i].iTimer, GET_RANDOM_INT_IN_RANGE(750, 3000))
							AND NOT IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
								STRING sTempAnimName
								IF i = POOL_PED_FEMALE
									sTempAnimName = "react_small_variations_k"
								ELSE
									IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
										sTempAnimName = "react_small_variations_m"
									ELSE
										sTempAnimName = "react_small_variations_n"
									ENDIF
								ENDIF
								IF SET_PED_PLAY_AWARE_OF_PLAYER_ANIM(sPoolPed[i].pedIndex, sTempAnimName, SLOW_BLEND_IN, SLOW_BLEND_OUT)
									sPoolPed[i].iTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_FACING_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID(), 170.0)	// stop the anim early if the player goes behind them
							CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_PLAYER stopped idle anim early for player behind ped ID = ", i) ENDIF #ENDIF
						ENDIF						
						sPoolPed[i].iTimer = GET_GAME_TIMER()
					ENDIF
					IF i = POOL_PED_FEMALE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_PLAYER -> N1B_PED_AI_SETUP_BEATEN_UP female ped attacked by the player ID = ", i) ENDIF #ENDIF
							sPoolPed[i].AI = N1B_PED_AI_SETUP_BEATEN_UP
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_AWARE_OF_GARDENER
					IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					AND ePoolAreaState = N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
						IF IS_ENTITY_PLAYING_ANIM(sPoolPed[i].pedIndex, "rcmnigel1b", "wipehead")
							STOP_ANIM_TASK(sPoolPed[i].pedIndex, "rcmnigel1b", "wipehead", SLOW_BLEND_OUT)
						ENDIF
						IF i = POOL_PED_CELEB
							CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
							//#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_GARDENER - alt walk anim cleared", " id = ", i) ENDIF #ENDIF
						ENDIF
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	//B*1051019 - Tyler didn't turn to face ped
						TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, sGardenerPed.pedIndex, -1)
						TASK_TURN_PED_TO_FACE_ENTITY(sPoolPed[i].pedIndex, sGardenerPed.pedIndex, -1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_GARDENER", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_STATE_AWARE_OF_GARDENER
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_AWARE_OF_GARDENER -> N1B_PED_AI_SETUP_AWARE_OF_PLAYER", " id = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_AWARE_OF_GARDENER
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_GARDENER -> N1B_PED_AI_SETUP_AWARE_OF_GARDENER ID = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_GARDENER
					ENDIF
					// switch to tracking player if gardener is dead / finish intrupt state
					IF NOT IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					OR ePoolAreaState != N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_GARDENER -> N1B_PED_AI_SETUP_AWARE_OF_PLAYER ID = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					ENDIF
					IF i = POOL_PED_FEMALE
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_AWARE_OF_GARDENER -> N1B_PED_AI_SETUP_BEATEN_UP female ped attacked by the player ID = ", i) ENDIF #ENDIF
							sPoolPed[i].AI = N1B_PED_AI_SETUP_BEATEN_UP
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_WAIT_TO_ATTACK						
					OPEN_SEQUENCE_TASK(sequenceAI)
						IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							//TASK_GET OUT POOL
							VECTOR vTempGotoPos
							vTempGotoPos = GET_ENTITY_COORDS(sPoolPed[i].pedIndex)
							IF GET_SAFE_COORD_FOR_PED(vTempGotoPos, TRUE, vTempGotoPos)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTempGotoPos, PEDMOVEBLENDRATIO_RUN)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_WAIT_TO_ATTACK - GET OUT OF POOL COORD GIVEN PED ID = ", i) ENDIF #ENDIF
							ENDIF
						ENDIF
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					CLOSE_SEQUENCE_TASK(sequenceAI)					
					CLEAR_PED_TASKS_IMMEDIATELY(sPoolPed[i].pedIndex)
					TASK_PERFORM_SEQUENCE(sPoolPed[i].pedIndex, sequenceAI)
					CLEAR_SEQUENCE_TASK(sequenceAI)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)					
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_WAIT_TO_ATTACK - PED ID = ", i) ENDIF #ENDIF
					sPoolPed[i].AI = N1B_PED_AI_STATE_WAIT_TO_ATTACK
				BREAK					
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_WAIT_TO_ATTACK
					//Check for getting beaten up
					IF GET_ENTITY_HEALTH(sPoolPed[i].pedIndex) < NIGEL1B_BEATEN_UP_HEALTH_THRESHOLD
						sPoolPed[i].AI = N1B_PED_AI_SETUP_BEATEN_UP
					ELIF NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_WAIT_TO_ATTACK -> N1B_PED_AI_SETUP_ATTACK - PED ID = player out water ", i) ENDIF #ENDIF
						//sPoolPed[i].AI = N1B_PED_AI_SETUP_ATTACK
					ELSE
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
							// PED HAS LOST HIS TASK, RESET IT
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_WAIT_TO_ATTACK -> N1B_PED_AI_SETUP_WAIT_TO_ATTACK task no running - PED ID = ", i) ENDIF #ENDIF
	                        sPoolPed[i].AI = N1B_PED_AI_SETUP_WAIT_TO_ATTACK
						ENDIF
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_BEATEN_UP
					IF i = POOL_PED_CELEB	
						IF IS_ENTITY_ALIVE(moCelebPhone.object)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(moCelebPhone.object, sPoolPed[i].pedIndex)
								DETACH_ENTITY(moCelebPhone.object)
							ENDIF
							SAFE_REMOVE_OBJECT(moCelebPhone.object, FALSE)
						ENDIF
					ENDIF
					//SET_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex, "move_injured_generic", "walk")									
					SET_PED_CAN_RAGDOLL(sPoolPed[i].pedIndex, TRUE)						
					IF NOT IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
						SET_PED_TO_RAGDOLL(sPoolPed[i].pedIndex, 1000, 1000, TASK_RELAX)
					ENDIF					
					//doesn't work how i wanted, because the ped health drains to zero and he dies failing the mission.
					/*
					IF (GET_ENTITY_HEALTH(sPoolPed[i].pedIndex) > 100)
						SET_ENTITY_HEALTH(sPoolPed[i].pedIndex, 100)	
					ENDIF				
					SET_PED_INJURED_ON_GROUND_BEHAVIOUR(sPoolPed[i].pedIndex, -1.0)
					*/					
					sPoolPed[i].AI = N1B_PED_AI_STATE_BEATEN_UP
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_BEATEN_UP
					IF NOT IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_COWER)
						AND NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_HANDS_UP)						
							//Additional checks to stop peds cowering under water
							IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							OR IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
							OR IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
								IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
									CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
									VECTOR vGotoPos
									FLOAT fGoToHeading
									vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE]	// north side of the HOT_TUB
									fGoToHeading = 201.7237
									IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vGotoPos, 1.5)	// if player is at that spot go for the opposite side
										vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE]	// south side of the HOT_TUB
										fGoToHeading = 325.0476
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(sPoolPed[i].pedIndex, vGotoPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fGoToHeading)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_SURRENDERED -> get out water first", " id = ", i) ENDIF #ENDIF
								ENDIF
								//TASK_HANDS_UP(sPoolPed[i].pedIndex, -1, PLAYER_PED_ID(), -1)
							ELSE
								SET_PED_COWER_HASH(sPoolPed[i].pedIndex, "CODE_HUMAN_STAND_COWER")
								TASK_COWER(sPoolPed[i].pedIndex, -1)								
							ENDIF
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_BEATEN_UP - PED ID = ", i) ENDIF #ENDIF
	                  	ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_SURRENDERED	
					STOP_PED_SPEAKING(sPoolPed[i].pedIndex, FALSE)	//added to make them scream // hopefully?
					
					IF i = POOL_PED_CELEB	
						IF IS_ENTITY_ALIVE(moCelebPhone.object)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(moCelebPhone.object, sPoolPed[i].pedIndex)
								DETACH_ENTITY(moCelebPhone.object)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_SURRENDERED - detached phone : PED ID = ", i) ENDIF #ENDIF
							ENDIF
							SAFE_REMOVE_OBJECT(moCelebPhone.object, FALSE)
						ENDIF
						CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
						//#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_SURRENDERED - alt walk anim cleared", " id = ", i) ENDIF #ENDIF
					ENDIF
					
					IF HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM(i)
						IF NOT IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						AND NOT IS_PED_GETTING_UP(sPoolPed[i].pedIndex)
						AND NOT IS_PED_CLIMBING(sPoolPed[i].pedIndex)	// added for bug 970159
							IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							AND IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
							OR IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.13635, 369.17224, 69.18319 >>, 1.25)	// B*1218631 - block stopping on North corner of pool where feet clip through the wall
							OR IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.10815, 354.76880, 69.19383 >>, 1.25)	// B*1218631 - block stopping on South corner of pool where feet clip through the wall
							OR IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, <<-1051.339722,358.404541,66.914551>>, <<-1051.361816,365.510620,71.914162>>, 2.250000)	// B*1327618 - block stopping in shrubs next to pool
								IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
									CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
									VECTOR vGotoPos
									FLOAT fGoToHeading
									vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE]	// north side of the HOT_TUB
									fGoToHeading = 201.7237
									IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vGotoPos, 1.5)	// if player is at that spot go for the opposite side
										vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE]	// south side of the HOT_TUB
										fGoToHeading = 325.0476
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(sPoolPed[i].pedIndex, vGotoPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fGoToHeading)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)	
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_SURRENDERED -> get out water first", " id = ", i) ENDIF #ENDIF
								ENDIF
							ELSE
								CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
								TASK_LOOK_AT_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID(), -1)		
								TASK_HANDS_UP(sPoolPed[i].pedIndex, -1, PLAYER_PED_ID(), -1)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_SURRENDERED - PED ID = ", i) ENDIF #ENDIF
								sPoolPed[i].AI = N1B_PED_AI_STATE_SURRENDERED
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - waiting for reaction anim - N1B_PED_AI_SETUP_SURRENDERED - PED ID = ", i) ENDIF #ENDIF						
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_SURRENDERED
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_HANDS_UP)
		                	// PED HAS LOST HIS TASK, RESET IT
		                	sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_SURRENDERED - lost task - PED ID = ", i) ENDIF #ENDIF
		              	ELSE
							// reapply get out water if fell back in
							IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
							AND IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool)
								sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_SURRENDERED - back in water - PED ID = ", i) ENDIF #ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
							IF NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.13635, 369.17224, 69.18319 >>, 1.25)	// B*1218631 - block stopping on North corner of pool where feet clip through the wall
								IF NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, << -1057.10815, 354.76880, 69.19383 >>, 1.25)	// B*1218631 - block stopping on South corner of pool where feet clip through the wall
									IF NOT IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, <<-1051.339722,358.404541,66.914551>>, <<-1051.361816,365.510620,71.914162>>, 2.250000)	// B*1327618 - block stopping in shrubs next to pool
										sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI -> N1B_PED_AI_STATE_SURRENDERED - navigated to safe spot for surrender behaviour - PED ID = ", i) ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_FRIGHTENED
					//Additional checks to stop peds cowering under water
					IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)
					OR IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
					OR IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
					OR IS_PED_CLIMBING(sPoolPed[i].pedIndex)	// added for bug 970159
					OR IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_FRIGHTENED going to N1B_PED_AI_SETUP_SURRENDERED - PED ID = ", i) ENDIF #ENDIF
					ELSE
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
						SET_PED_COWER_HASH(sPoolPed[i].pedIndex, "CODE_HUMAN_STAND_COWER")
						TASK_COWER(sPoolPed[i].pedIndex, -1)	
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPoolPed[i].pedIndex, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_SETUP_FRIGHTENED - PED ID = ", i) ENDIF #ENDIF
						sPoolPed[i].AI = N1B_PED_AI_STATE_FRIGHTENED
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_FRIGHTENED	
					IF NOT IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_COWER)
	                    	// PED HAS LOST HIS TASK, RESET IT
	                    	sPoolPed[i].AI = N1B_PED_AI_SETUP_FRIGHTENED
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_FRIGHTENED - PED ID = ", i) ENDIF #ENDIF
						ELSE
							IF IS_ENTITY_IN_WATER(sPoolPed[i].pedIndex)								
								sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - N1B_PED_AI_STATE_FRIGHTENED going to N1B_PED_AI_SETUP_SURRENDERED - PED ID = ", i) ENDIF #ENDIF
							ENDIF
	                  	ENDIF
					ENDIF
				BREAK	
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_FLEE_WITH_REACT						
					// if ped is playing exit sync scene anim then just switch to normal flee
					IF HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM(i)
						IF NOT IS_PED_RAGDOLL(sPoolPed[i].pedIndex)
						AND NOT IS_PED_SWIMMING(sPoolPed[i].pedIndex)
						AND NOT IS_PED_CLIMBING(sPoolPed[i].pedIndex)	
							STOP_PED_SPEAKING(sPoolPed[i].pedIndex, FALSE)	//added to make them scream // hopefully?
							IF i = POOL_PED_CELEB	
								CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
								//#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_FLEE_WITH_REACT - alt walk anim cleared", " id = ", i) ENDIF #ENDIF
							ENDIF
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_DISABLE_HANDS_UP, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_USE_VEHICLE, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_PREFER_PAVEMENTS, TRUE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_USE_COVER, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_CAN_SCREAM, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sPoolPed[i].pedIndex, CA_ALWAYS_FIGHT, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sPoolPed[i].pedIndex, CA_ALWAYS_FLEE, TRUE)
						
							//CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
							TASK_REACT_AND_FLEE_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID())
							sPoolPed[i].AI = N1B_PED_AI_STATE_FLEE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - set - N1B_PED_AI_SETUP_FLEE_WITH_REACT - PED ID = ", i) ENDIF #ENDIF
						ELSE
							sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - was in ragdoll so switch to normal flee N1B_PED_AI_SETUP_FLEE  - PED ID = ", i) ENDIF #ENDIF						
						ENDIF					
					ELSE
						sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - waiting for reaction anim - N1B_PED_AI_SETUP_FLEE_WITH_REACT switch to N1B_PED_AI_SETUP_FLEE  - PED ID = ", i) ENDIF #ENDIF						
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_FLEE	
					STOP_PED_SPEAKING(sPoolPed[i].pedIndex, FALSE)	//added to make them scream // hopefully?
					IF i = POOL_PED_CELEB	
						CLEAR_PED_ALTERNATE_WALK_ANIM(sPoolPed[i].pedIndex)
						//#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "N1B_PED_AI_SETUP_FLEE - alt walk anim cleared", " id = ", i) ENDIF #ENDIF
					ENDIF
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_DISABLE_HANDS_UP, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_PREFER_PAVEMENTS, TRUE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sPoolPed[i].pedIndex, FA_CAN_SCREAM, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(sPoolPed[i].pedIndex, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sPoolPed[i].pedIndex, CA_ALWAYS_FLEE, TRUE)
					
					IF HAS_POOL_PED_FINISHED_SURRENDER_REACTION_ANIM(i)					
						//CLEAR_PED_TASKS(sPoolPed[i].pedIndex)	
						TASK_SMART_FLEE_PED(sPoolPed[i].pedIndex, PLAYER_PED_ID(), 150.0, -1, TRUE)
						sPoolPed[i].AI = N1B_PED_AI_STATE_FLEE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - set - N1B_PED_AI_SETUP_FLEE - PED ID = ", i) ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - waiting for reaction anim - N1B_PED_AI_SETUP_FLEE - PED ID = ", i) ENDIF #ENDIF						
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_FLEE
					IF NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_SMART_FLEE_PED)
					AND NOT IsPedPerformingTask(sPoolPed[i].pedIndex, SCRIPT_TASK_REACT_AND_FLEE_PED)
                    	sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CONTROL_POOL_PEDS_AI - reapply - N1B_PED_AI_STATE_FLEE - PED ID = ", i) ENDIF #ENDIF
					ENDIF
				BREAK	
				//	------------------------------------------
				DEFAULT
					SCRIPT_ASSERT("NIGEL1B - invalid N1B_PED_AI in CONTROL_POOL_PEDS_AI")
				BREAK				
			ENDSWITCH
		ENDIF
		
		// Handle ped's blips
		IF sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE
		OR sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE_WITH_REACT
		OR sPoolPed[i].AI = N1B_PED_AI_STATE_FLEE
			CLEANUP_AI_PED_BLIP(sPoolPed[i].blipAIStruct)
		ELSE
			// once clothes have been stolen, unblip the peds when the player moves away if he was undetected
			IF eMissionStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS
				UPDATE_AI_PED_BLIP(sPoolPed[i].pedIndex, sPoolPed[i].blipAIStruct)
			ELSE
				IF ePoolAreaState = N1B_POOL_AREA_STATE_UNDETECTED
				OR ePoolAreaState = N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
					IF ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
					AND NOT IS_ENTITY_IN_RANGE_COORDS(sPoolPed[i].pedIndex, vPlayerPos, 35.0)
						CLEANUP_AI_PED_BLIP(sPoolPed[i].blipAIStruct)
					ELSE
						UPDATE_AI_PED_BLIP(sPoolPed[i].pedIndex, sPoolPed[i].blipAIStruct)
					ENDIF					
				ELSE
					UPDATE_AI_PED_BLIP(sPoolPed[i].pedIndex, sPoolPed[i].blipAIStruct)
				ENDIF
			ENDIF
		ENDIF
	ELSE		
		IF sPoolPed[i].AI != N1B_PED_AI_STATE_DEAD
			IF i = POOL_PED_CELEB
				IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLB_KILLED_TYLER))
					SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLB_KILLED_TYLER))
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_NGLB_KILLED_TYLER) set")
				ENDIF
				REPLAY_RECORD_BACK_FOR_TIME(2.5, 2.0, REPLAY_IMPORTANCE_LOWEST)	// B*1880376 - record killing Tyler Dixon
			ENDIF
			sPoolPed[i].AI = N1B_PED_AI_STATE_DEAD
		ENDIF
		CLEANUP_AI_PED_BLIP(sPoolPed[i].blipAIStruct)
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls Gardeners AI.
PROC CONTROL_GARDENERS_AI()
	IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
		IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	//having to test this because changes to tasks breaks the execution/takedown moves
			SWITCH (sGardenerPed.AI)			
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_RELAXED
					IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener")
						TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_USE_KINEMATIC_PHYSICS) // B*1544302 - AF_USE_KINEMATIC_PHYSICS)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_STATE_RELAXED reapplied anim ") ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					AND IS_ENTITY_ATTACHED_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex)
						DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					ENDIF
					CLEAR_PED_SECONDARY_TASK(sGardenerPed.pedIndex)
					IF NOT IS_PED_HEADTRACKING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), -1)
					ENDIF
					// play his exit anim					
					IF IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener")
						IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
						AND NOT IS_PED_GETTING_UP(sGardenerPed.pedIndex)
							TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_gardener", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT | AF_EXIT_AFTER_INTERRUPTED)
						ELSE
							IF IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sGardenerPed.pedIndex, FAST_BLEND_OUT, TRUE)
							ENDIF
						ENDIF
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_AWARE_OF_PLAYER going to N1B_PED_AI_STATE_AWARE_OF_PLAYER") ENDIF #ENDIF
					sGardenerPed.AI = N1B_PED_AI_STATE_AWARE_OF_PLAYER
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_AWARE_OF_PLAYER
					IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_gardener")
						IF NOT IS_PED_FACING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID(), 45)
							IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								TASK_TURN_PED_TO_FACE_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), 0)	
							ENDIF
						ENDIF
					ELSE
						SET_ENTITY_ANIM_SPEED(sGardenerPed.pedIndex, "rcmnigel1b", "exit_gardener", 1.2)
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_JUST_FACE_PLAYER
					CLEAR_PED_TASKS(sGardenerPed.pedIndex)
					IF NOT IS_PED_HEADTRACKING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), -1)
					ENDIF								
					IF NOT IS_PED_FACING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID(), 45)
						TASK_TURN_PED_TO_FACE_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), 0)	
					ENDIF	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
					sGardenerPed.iTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_JUST_FACE_PLAYER going to N1B_PED_AI_STATE_JUST_FACE_PLAYER") ENDIF #ENDIF
					sGardenerPed.AI = N1B_PED_AI_STATE_JUST_FACE_PLAYER
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_JUST_FACE_PLAYER
					// allow idle anims
					IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_PLAY_ANIM)
						IF NOT IS_PED_FACING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID(), 45.0)
							IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "GARDENER AI - N1B_PED_AI_STATE_JUST_FACE_PLAYER -> N1B_PED_AI_SETUP_JUST_FACE_PLAYER") ENDIF #ENDIF
								sGardenerPed.AI = N1B_PED_AI_SETUP_JUST_FACE_PLAYER
							ENDIF
						ELSE								
							IF HAS_TIME_PASSED(sGardenerPed.iTimer, GET_RANDOM_INT_IN_RANGE(2000, 4000))								
								IF SET_PED_PLAY_AWARE_OF_PLAYER_ANIM(sGardenerPed.pedIndex, "react_small_variations_o", SLOW_BLEND_IN, SLOW_BLEND_OUT)
									sGardenerPed.iTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_FACING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID(), 170.0)	// stop the anim early if the player goes behind them
							CLEAR_PED_TASKS(sGardenerPed.pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "GARDENER AI - N1B_PED_AI_STATE_JUST_FACE_PLAYER stopped idle anim early for player behind ped") ENDIF #ENDIF
						ENDIF						
						sGardenerPed.iTimer = GET_GAME_TIMER()
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_WARN_OTHER_PEDS
					// wait for his exit anim first
					IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_gardener")
						OPEN_SEQUENCE_TASK(sequenceAI)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1032.2950, 347.6416, 70.3615 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)	//south side of house
							IF NOT IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, << -1050.3589, 354.0594, 68.9132 >>, 3.0)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1050.3589, 354.0594, 68.9132 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 38.0338)	//pool area
							ENDIF
							IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_CELEB].pedIndex)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, sPoolPed[POOL_PED_CELEB].pedIndex, -1)
							ENDIF
						CLOSE_SEQUENCE_TASK(sequenceAI)					
						TASK_PERFORM_SEQUENCE(sGardenerPed.pedIndex, sequenceAI)
						CLEAR_SEQUENCE_TASK(sequenceAI)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)					
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_WARN_OTHER_PEDS going to N1B_PED_AI_STATE_WARN_OTHER_PEDS") ENDIF #ENDIF
						sGardenerPed.AI = N1B_PED_AI_STATE_WARN_OTHER_PEDS
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - waiting on exit anim to finish - N1B_PED_AI_SETUP_WARN_OTHER_PEDS ") ENDIF #ENDIF
					ENDIF
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_WARN_OTHER_PEDS
					IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_PERFORM_SEQUENCE)
	                	// PED HAS LOST HIS TASK, RESET IT
	                	sGardenerPed.AI = N1B_PED_AI_SETUP_WARN_OTHER_PEDS
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_STATE_WARN_OTHER_PEDS going BACK to N1B_PED_AI_SETUP_WARN_OTHER_PEDS") ENDIF #ENDIF
	              	ELSE
						IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_CELEB].pedIndex)
							IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_LOOK_AT_ENTITY)
								TASK_LOOK_AT_ENTITY(sGardenerPed.pedIndex, sPoolPed[POOL_PED_CELEB].pedIndex, -1)
							ENDIF
						ENDIF
					ENDIF
				BREAK								
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_BEATEN_UP
					IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					AND IS_ENTITY_ATTACHED_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex)
						DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
					ENDIF
					CLEAR_PED_SECONDARY_TASK(sGardenerPed.pedIndex)
					CLEAR_PED_TASKS(sGardenerPed.pedIndex)					
					//SET_PED_ALTERNATE_WALK_ANIM(sGardenerPed.pedIndex, "move_injured_generic", "walk")									
					SET_PED_CAN_RAGDOLL(sGardenerPed.pedIndex, TRUE)					
					IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
						SET_PED_TO_RAGDOLL(sGardenerPed.pedIndex, 1000, 1000, TASK_RELAX)
					ENDIF		
					//does work how i wanted, because the ped health drains to zero and he dies failing the mission.
					/*
					IF (GET_ENTITY_HEALTH(sGardenerPed.pedIndex) > 100)
						SET_ENTITY_HEALTH(sGardenerPed.pedIndex, 100)	
					ENDIF						
					SET_PED_INJURED_ON_GROUND_BEHAVIOUR(sGardenerPed.pedIndex, -1.0)
					*/					
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_BEATEN_UP going to N1B_PED_AI_STATE_BEATEN_UP") ENDIF #ENDIF
					sGardenerPed.AI = N1B_PED_AI_STATE_BEATEN_UP
				BREAK					
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_BEATEN_UP
					IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
						IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_COWER)
						AND NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_HANDS_UP)						
							//Additional checks to stop peds cowering under water
							IF NOT IS_ENTITY_IN_WATER(sGardenerPed.pedIndex)
							AND NOT IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
							AND NOT IS_ENTITY_IN_ANGLED_AREA(sGardenerPed.pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
							AND NOT IS_PED_CLIMBING(sGardenerPed.pedIndex)	// added for bug 970159
								SET_PED_COWER_HASH(sGardenerPed.pedIndex, "CODE_HUMAN_STAND_COWER")
								TASK_COWER(sGardenerPed.pedIndex, -1)
							ELSE
								IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
									CLEAR_PED_TASKS(sGardenerPed.pedIndex)
									VECTOR vGotoPos
									FLOAT fGoToHeading
									vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE]	// north side of the HOT_TUB
									fGoToHeading = 201.7237
									IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vGotoPos, 1.5)	// if player is at that spot go for the opposite side
										vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE]	// south side of the HOT_TUB
										fGoToHeading = 325.0476
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(sGardenerPed.pedIndex, vGotoPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fGoToHeading)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)	
									#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "NIGEL 1B: ", "GARDENER AI -> get out water first", " id = ") ENDIF #ENDIF
								ENDIF
								//TASK_HANDS_UP(sGardenerPed.pedIndex, -1, PLAYER_PED_ID(), -1)
							ENDIF
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - N1B_PED_AI_STATE_BEATEN_UP - PED ID = ") ENDIF #ENDIF
	                  	ENDIF
					ENDIF
				BREAK		
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_SURRENDERED
					IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex)
							DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
						ENDIF
						SAFE_REMOVE_OBJECT(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, FALSE)
					ENDIF					
					IF IS_PED_USING_ANY_SCENARIO(sGardenerPed.pedIndex)
						//CLEAR_PED_TASKS(sGardenerPed.pedIndex)
						SET_PED_PANIC_EXIT_SCENARIO(sGardenerPed.pedIndex, vPlayerPos)
					ENDIF
					IF IS_ENTITY_IN_WATER(sGardenerPed.pedIndex)						
					AND IS_ENTITY_IN_ANGLED_AREA(sGardenerPed.pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
						IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
							AND NOT IS_PED_GETTING_UP(sGardenerPed.pedIndex)
							AND NOT IS_PED_CLIMBING(sGardenerPed.pedIndex)	// added for bug 970159
								VECTOR vGotoPos
								FLOAT fGoToHeading
								vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_NORTH_SIDE]	// north side of the HOT_TUB
								fGoToHeading = 201.7237
								IF IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vGotoPos, 1.5)	// if player is at that spot go for the opposite side
									vGotoPos = vGotowardsHotTubInitialPos[NIGEL1B_POOL_SOUTH_SIDE]	// south side of the HOT_TUB
									fGoToHeading = 325.0476
								ENDIF
								TASK_FOLLOW_NAV_MESH_TO_COORD(sGardenerPed.pedIndex, vGotoPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fGoToHeading)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)	
								#IF IS_DEBUG_BUILD IF bDebug_Display_PP_AI CPRINTLN(DEBUG_MISSION, "GARDENER AI	- N1B_PED_AI_SETUP_SURRENDERED -> get out water first", " id = ") ENDIF #ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_HEADTRACKING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), -1)
						ENDIF
						// play his exit anim				
						IF IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener")
							IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
							AND NOT IS_PED_GETTING_UP(sGardenerPed.pedIndex)
							AND NOT IS_PED_CLIMBING(sGardenerPed.pedIndex)	// added for bug 970159
							AND NOT IS_PED_BEING_STUNNED(sGardenerPed.pedIndex)
								TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_panic_gardener", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT | AF_EXIT_AFTER_INTERRUPTED)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_SURRENDERED set exit anim going to N1B_PED_AI_STATE_SURRENDERED") ENDIF #ENDIF
							ELSE
								IF IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE)
									STOP_SYNCHRONIZED_ENTITY_ANIM(sGardenerPed.pedIndex, FAST_BLEND_OUT, TRUE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_SURRENDERED STOP_SYNCHRONIZED_ENTITY_ANIM(sGardenerPed.pedIndex, FAST_BLEND_OUT, TRUE)") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE												
							TASK_HANDS_UP(sGardenerPed.pedIndex, -1, PLAYER_PED_ID(), -1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
						ENDIF
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_SURRENDERED set exit anim going to N1B_PED_AI_STATE_SURRENDERED") ENDIF #ENDIF
						sGardenerPed.AI = N1B_PED_AI_STATE_SURRENDERED
					ENDIF
				BREAK			
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_SURRENDERED							
					IF IS_ENTITY_IN_WATER(sGardenerPed.pedIndex)
					AND IS_ENTITY_IN_ANGLED_AREA(sGardenerPed.pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
						sGardenerPed.AI = N1B_PED_AI_SETUP_SURRENDERED
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	- N1B_PED_AI_STATE_SURRENDERED - fell back in water - PED ID = ") ENDIF #ENDIF
					ELSE
						IF NOT IS_PED_HEADTRACKING_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), -1)
						ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_panic_gardener")
							IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_HANDS_UP)
			                	TASK_HANDS_UP(sGardenerPed.pedIndex, -1, PLAYER_PED_ID(), -1)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_STATE_SURRENDERED set hands up task") ENDIF #ENDIF
			              	ENDIF
							IF IS_PLAYER_MAKING_PED_FRIGHTENED(sGardenerPed.pedIndex, FALSE)
								sGardenerPed.AI = N1B_PED_AI_SETUP_FRIGHTENED
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_STATE_SURRENDERED going to N1B_PED_AI_SETUP_FRIGHTENED") ENDIF #ENDIF
							ENDIF
						ENDIF
					endif
				BREAK				
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_FRIGHTENED	
					//Additional checks to stop peds cowering under water
					IF IS_ENTITY_IN_WATER(sGardenerPed.pedIndex)
					OR IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vHotTubDimensionsCentrePos, fHotTubDimensionsWidth)	//inside the HOT_TUB
					OR IS_ENTITY_IN_ANGLED_AREA(sGardenerPed.pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
					OR IS_PED_CLIMBING(sGardenerPed.pedIndex)	// added for bug 970159
					OR IS_PED_RAGDOLL(sGardenerPed.pedIndex)
						sGardenerPed.AI = N1B_PED_AI_SETUP_SURRENDERED
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER - N1B_PED_AI_SETUP_FRIGHTENED going to N1B_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
					ELSE
						IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex)
								DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
							ENDIF	
							SAFE_REMOVE_OBJECT(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, FALSE)
						ENDIF
						IF IS_PED_USING_ANY_SCENARIO(sGardenerPed.pedIndex)
							//CLEAR_PED_TASKS(sGardenerPed.pedIndex)
							SET_PED_PANIC_EXIT_SCENARIO(sGardenerPed.pedIndex, vPlayerPos)
						ENDIF
						CLEAR_PED_SECONDARY_TASK(sGardenerPed.pedIndex)
						SET_PED_COWER_HASH(sGardenerPed.pedIndex, "CODE_HUMAN_STAND_COWER")
						TASK_COWER(sGardenerPed.pedIndex, -1)	
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sGardenerPed.pedIndex, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_SETUP_FRIGHTENED going to N1B_PED_AI_STATE_FRIGHTENED") ENDIF #ENDIF
						sGardenerPed.AI = N1B_PED_AI_STATE_FRIGHTENED
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_FRIGHTENED	
					IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
						IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_COWER)
	                    	sGardenerPed.AI = N1B_PED_AI_SETUP_FRIGHTENED
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI	-	N1B_PED_AI_STATE_FRIGHTENED going to N1B_PED_AI_SETUP_FRIGHTENED") ENDIF #ENDIF
	                  	ELSE
							IF IS_ENTITY_IN_WATER(sGardenerPed.pedIndex)								
								sGardenerPed.AI = N1B_PED_AI_SETUP_SURRENDERED
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER - N1B_PED_AI_STATE_FRIGHTENED going to N1B_PED_AI_SETUP_SURRENDERED") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_SETUP_FLEE
					IF NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_panic_gardener")
					AND NOT IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_gardener")
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_DISABLE_HANDS_UP, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_USE_VEHICLE, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_PREFER_PAVEMENTS, TRUE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_USE_COVER, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_LOOK_FOR_CROWDS, FALSE)
						SET_PED_FLEE_ATTRIBUTES(sGardenerPed.pedIndex, FA_CAN_SCREAM, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(sGardenerPed.pedIndex, CA_ALWAYS_FIGHT, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sGardenerPed.pedIndex, CA_ALWAYS_FLEE, TRUE)			
						IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, sGardenerPed.pedIndex)
								DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
							ENDIF	
							SAFE_REMOVE_OBJECT(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object, FALSE)
						ENDIF
						CLEAR_PED_SECONDARY_TASK(sGardenerPed.pedIndex)
						IF IS_PED_USING_ANY_SCENARIO(sGardenerPed.pedIndex)
							//CLEAR_PED_TASKS(sGardenerPed.pedIndex)
							SET_PED_PANIC_EXIT_SCENARIO(sGardenerPed.pedIndex, vPlayerPos)
						ENDIF
						// play his exit anim
						IF IS_ENTITY_PLAYING_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "idle_gardener")
							TASK_PLAY_ANIM(sGardenerPed.pedIndex, "rcmnigel1b", "exit_panic_gardener", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT | AF_EXIT_AFTER_INTERRUPTED)
						ELSE
							IF NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
							AND NOT IS_PED_SWIMMING(sGardenerPed.pedIndex)
							AND NOT IS_PED_CLIMBING(sGardenerPed.pedIndex)	
								TASK_REACT_AND_FLEE_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - TASK_REACT_AND_FLEE_PED applied ") ENDIF #ENDIF								
							ELSE
								TASK_SMART_FLEE_PED(sGardenerPed.pedIndex, PLAYER_PED_ID(), 150.0, -1, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - TASK_SMART_FLEE_PED applied ") ENDIF #ENDIF
							ENDIF
							sGardenerPed.AI = N1B_PED_AI_STATE_FLEE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - set - N1B_PED_AI_SETUP_FLEE ") ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - waiting on exit anim to finish - N1B_PED_AI_SETUP_FLEE ") ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				CASE N1B_PED_AI_STATE_FLEE
					IF NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_SMART_FLEE_PED)
					AND NOT IsPedPerformingTask(sGardenerPed.pedIndex, SCRIPT_TASK_REACT_AND_FLEE_PED)
                    	sGardenerPed.AI = N1B_PED_AI_SETUP_FLEE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDENER AI - reapply - N1B_PED_AI_STATE_FLEE ") ENDIF #ENDIF
					ENDIF
				BREAK
				//	------------------------------------------
				DEFAULT
					SCRIPT_ASSERT("NIGEL1B - invalid N1B_PED_AI in Gardener AI")
				BREAK				
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/*
/// PURPOSE:
///    Handles the updating of the pools peds and gardener's ai, only updates one ped a frame
///    //ommented out for now as this is pretty slow at updating
PROC MANAGE_UPDATING_POOL_PEDS_AND_GARDENER_AI()
	//IF (GET_GAME_TIMER() - iUpdatePoolPedAI_ThisFrameCounter > 5)
		//reached end so undate gardener instead this frame
		IF (iPoolPedId_ToUpdateInManager >= TOTAL_POOL_PEDS)
			CONTROL_GARDENERS_AI()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_UPDATING_POOL_PEDS_AND_GARDENER_AI	- 	GARDENER UPDATED THIS FRAME") ENDIF #ENDIF
			iPoolPedId_ToUpdateInManager = 0
		ELSE
			CONTROL_POOL_PEDS_AI(iPoolPedId_ToUpdateInManager)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_UPDATING_POOL_PEDS_AND_GARDENER_AI	- 	POOL PED UPDATED THIS FRAME	ID = ", iPoolPedId_ToUpdateInManager) ENDIF #ENDIF
			iPoolPedId_ToUpdateInManager++
		ENDIF
	//	iUpdatePoolPedAI_ThisFrameCounter = GET_GAME_TIMER()
	//ENDIF	
ENDPROC
*/

/// PURPOSE:
///    Check to see if the ped is in the house area
/// PARAMS:
///    pedIndex - ped to test
///    bDoDeadCheck - do IS_ENTITY_ALIVE check first
/// RETURNS:
///    FALSE if the player is dead, or outside the area TRUE, if the ped is inside the area.
FUNC BOOL IS_PED_IN_HOUSE_AREA(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	VECTOR vPos = GET_ENTITY_COORDS(pedIndex)
	IF IS_POINT_IN_POLY_2D(tpCelebHouseArea, vPos)
		IF ARE_FLOAT_VALUES_SIMILAR(vPos.Z, NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE, NIGEL1B_GROUND_Z_TOLERANCE_AT_HOUSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check to see if the ped is in the pool area
/// PARAMS:
///    pedIndex - Ped to test
///    bDoDeadCheck - do IS_ENTITY_ALIVE check first
/// RETURNS:
///    FALSE if the player is dead, or outside the area TRUE, if the ped is inside the area.
FUNC BOOL IS_PED_IN_POOL_AREA(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	VECTOR vPos = GET_ENTITY_COORDS(pedIndex)
	IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPoolArea_NorthCircle_Pos, NIGEL1B_POOL_AREA_NORTH_CIRCLE_RADIUS)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_POOL_AREA - inside north radius") ENDIF #ENDIF
		// exclusion area to fix bug 980603
		IF NOT IS_ENTITY_AT_COORD(PedIndex, <<-1055.184448,382.732361,70.688316>>,<<1.000000,2.000000,2.750000>>)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_POOL_AREA - also not in the exlusion area so return true!") ENDIF #ENDIF
			IF ARE_FLOAT_VALUES_SIMILAR(vPos.Z, NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE, NIGEL1B_GROUND_Z_TOLERANCE_AT_HOUSE)
				RETURN TRUE
			ENDIF
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_POOL_AREA - but inside exclusion area###") ENDIF #ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_IN_AREA(pedIndex, vPoolArea_MajorityPosA, vPoolArea_MajorityPosB)
	OR IS_ENTITY_IN_ANGLED_AREA(pedIndex, vPoolArea_SouthWallPosA, vPoolArea_SouthWallPosB, NIGEL1B_POOL_AREA_SOUTH_WALL_POS_AB_WIDTH)
	OR IS_ENTITY_IN_ANGLED_AREA(pedIndex, vPoolArea_SouthWallPosC, vPoolArea_SouthWallPosD, NIGEL1B_POOL_AREA_SOUTH_WALL_POS_CD_WIDTH)
	OR IS_ENTITY_IN_ANGLED_AREA(pedIndex, vPoolArea_SouthWallPosE, vPoolArea_SouthWallPosF, NIGEL1B_POOL_AREA_SOUTH_WALL_POS_EF_WIDTH)
	OR IS_ENTITY_IN_ANGLED_AREA(pedIndex, vPoolArea_SouthWallPosG, vPoolArea_SouthWallPosH, NIGEL1B_POOL_AREA_SOUTH_WALL_POS_GH_WIDTH)		
		IF ARE_FLOAT_VALUES_SIMILAR(vPos.Z, NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE, NIGEL1B_GROUND_Z_TOLERANCE_AT_HOUSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check to see if the ped is in the garden area
/// PARAMS:
///    pedIndex - Ped to test
///    bDoDeadCheck - do IS_ENTITY_ALIVE check first
/// RETURNS:
///    FALSE if the player is dead, or outside the area TRUE, if the ped is inside the area.
FUNC BOOL IS_PED_IN_GARDEN_AREA(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	VECTOR vPos = GET_ENTITY_COORDS(pedIndex)
	IF IS_POINT_IN_POLY_2D(tpGardenArea, vPos)
		IF ARE_FLOAT_VALUES_SIMILAR(vPos.Z, NIGEL1B_LOWEST_GROUND_Z_POSITION_AT_HOUSE, NIGEL1B_GROUND_Z_TOLERANCE_AT_HOUSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Checks the player's location and updates N1B_PLAYER_LOCATION
///    also modifies the wanted level multiplier
///    and controls the gates' state switching
PROC CHECK_AND_UPDATE_PLAYER_LOCATION()

	IF ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
		IF IS_PED_IN_HOUSE_AREA(PLAYER_PED_ID(), FALSE)
			SET_WANTED_LEVEL_MULTIPLIER(0.2)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION - SET_WANTED_LEVEL_MULTIPLIER(0.2)") ENDIF #ENDIF
			ePlayerLocation = N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
			EXIT
		ENDIF
		
	ELIF ePlayerLocation = N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
		IF IS_PED_IN_POOL_AREA(PLAYER_PED_ID(), FALSE)
			SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(FALSE)
			ALLOW_DIALOGUE_IN_WATER(TRUE)
			IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_1B_MAIN_MIX")
				START_AUDIO_SCENE("NIGEL_1B_MAIN_MIX")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION - set audio scene active - ", "NIGEL_1B_MAIN_MIX") ENDIF #ENDIF
			ENDIF
			ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
			EXIT
		ELIF IS_PED_IN_GARDEN_AREA(PLAYER_PED_ID(), FALSE)	
			SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(FALSE)
			IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_1B_MAIN_MIX")
				START_AUDIO_SCENE("NIGEL_1B_MAIN_MIX")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION - set audio scene active - ", "NIGEL_1B_MAIN_MIX") ENDIF #ENDIF
			ENDIF
			ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
			EXIT
		ELIF NOT IS_PED_IN_HOUSE_AREA(PLAYER_PED_ID(), FALSE)
			SET_WANTED_LEVEL_MULTIPLIER(0.8)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION - SET_WANTED_LEVEL_MULTIPLIER(0.8)") ENDIF #ENDIF
			ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
			EXIT
		ENDIF
		
	ELIF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA			
		IF IS_PED_IN_POOL_AREA(PLAYER_PED_ID(), FALSE)
			// B*1537381 - Random peds screaming in the background when performing a stealth takedown on the gardener.
			IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION : N1B_PLAYER_LOCATION_GARDEN_AREA", " SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME() for player stealth kill") ENDIF #ENDIF
			ENDIF
			CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED()
		ELSE
			SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(TRUE)
			ALLOW_DIALOGUE_IN_WATER(FALSE)
			ePlayerLocation = N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
			EXIT
		ENDIF
		
	ELIF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA			
		IF IS_PED_IN_GARDEN_AREA(PLAYER_PED_ID(), FALSE)
			// B*1537381 - Random peds screaming in the background when performing a stealth takedown on the gardener.
			IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_LOCATION : N1B_PLAYER_LOCATION_GARDEN_AREA", " SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME() for player stealth kill") ENDIF #ENDIF
			ENDIF
			CHECK_FOR_HOUSE_DOUBLE_GATES_BEING_OPENED()
		ELSE
			SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(TRUE)
			ePlayerLocation = N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
			EXIT
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles triggering the requested mission music events
///    Since these cues need to be sequental, request event will only trigger if the previous event has triggered
/// PARAMS:
///    iMusicEvent - the event we want to trigger
PROC REQUEST_TRIGGER_MISSION_MUSIC_EVENT(INT iMusicEvent)
	IF iMusicEvent = iMissionMusicEventTriggerCounter
		SWITCH iMissionMusicEventTriggerCounter
			CASE NIGEL1B_MUSIC_EVENT_START				
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_START")
					iMissionMusicEventTriggerCounter++
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUEST_TRIGGER_MISSION_MUSIC_EVENT : ", "NIGEL1B_MUSIC_EVENT_START") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE NIGEL1B_MUSIC_EVENT_WANTED
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_WANTED")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
					iMissionMusicEventTriggerCounter++
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUEST_TRIGGER_MISSION_MUSIC_EVENT : ", "NIGEL1B_MUSIC_EVENT_WANTED") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE NIGEL1B_MUSIC_EVENT_COPS_LOST
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					 IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_COPS_LOST")
						SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
						iTimer_DelayResetAudioFlag_AllowScoreAndRadio = GET_GAME_TIMER()
						iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_RESET_FLAG_ALLOW_SCORE_AND_RADIO
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUEST_TRIGGER_MISSION_MUSIC_EVENT : ", "NIGEL1B_MUSIC_EVENT_COPS_LOST") ENDIF #ENDIF
					ENDIF
				ELSE
					IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
						iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_MAX
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUEST_TRIGGER_MISSION_MUSIC_EVENT : ", "NIGEL1B_MISSION_FAIL") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			DEFAULT
			BREAK
		ENDSWITCH
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REQUEST_TRIGGER_MISSION_MUSIC_EVENT : ERROR - iMusicEvent = ", iMusicEvent, " but iMissionMusicEventTriggerCounter = ", iMissionMusicEventTriggerCounter) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles requesting to trigger music events 2 and 3
PROC MONITOR_TRIGGER_MISSION_MUSIC_EVENTS()
	///    This should only get called after the clothes have been stolen
	IF eMissionStage >= MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA
		// we'll only do the rest if the initial music event has been triggered, since they run in sequences
		IF iMissionMusicEventTriggerCounter > NIGEL1B_MUSIC_EVENT_START
		AND iMissionMusicEventTriggerCounter < NIGEL1B_MUSIC_EVENT_MAX						
			IF iMissionMusicEventTriggerCounter <> NIGEL1B_MUSIC_EVENT_RESET_FLAG_ALLOW_SCORE_AND_RADIO
				IF iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_WANTED
					IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
						REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_WANTED)
					ENDIF
				ENDIF
				// if player manages to lose the cops early need to clear the music
				IF eMissionStage = MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA					
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)				
						IF bHavePoolPedsCallTheCops
							iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_COPS_LOST
							REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_COPS_LOST)
						ENDIF
					ENDIF
				// if player has reached the outro phonecall music needs to cleared since the wanted level will have been cleared
				ELIF eMissionStage = MISSION_STAGE_OUTRO_PHONECALL
					iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_COPS_LOST
					REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_COPS_LOST)
				ENDIF
			ELSE
				// reset the audio flag after the delay
				IF HAS_TIME_PASSED(iTimer_DelayResetAudioFlag_AllowScoreAndRadio, 5000)	// time delay suggested by Rebecca J				
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
					iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_TRIGGER_MISSION_MUSIC_EVENTS : ", "reset audio flag AllowScoreAndRadio to FALSE") ENDIF #ENDIF					
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles the player using try to barge door animations
///    when approaching the gates at the side of the house
///    Setup taken from Armenian 3 - Alan's
PROC MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE()

	IF NOT IS_PHONE_ONSCREEN()
	AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	// skip playing anims and taking away player control if he's wanted
		VECTOR vRotation
		//West gate
		IF NOT bTrevorBargedWestGate
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1056.142578,382.923309,67.745857>>, <<-1054.965088,382.899017,70.988388>>, 1.200000)
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF IS_DOOR_IN_LOCKED_STATE(N1B_MCD_WEST_GATE_01)	// only play the anim if the door is locked	
						IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
						AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
						AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
							// make sure player is facing towards the door
							IF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 87.9952, 90.0)
								IF NOT IS_AREA_OCCUPIED(<< -1056.17236, 382.19086, 68.08848 >>, << -1054.68164, 383.83936, 70.96900 >>, FALSE, TRUE, FALSE, FALSE, FALSE)	// B*1165823
									REQUEST_ANIM_DICT("rcmnigel1b")
									vRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("rcmnigel1b", "lockeddoor_tryopen", << -1056.171, 383.577, 68.688 >>, << -0.000, 0.000, 97.793 >>)
									IF HAS_ANIM_DICT_LOADED("rcmnigel1b")
										IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
											OPEN_SEQUENCE_TASK(sequenceAI)
												TASK_GO_STRAIGHT_TO_COORD(NULL, GET_ANIM_INITIAL_OFFSET_POSITION("rcmnigel1b", "lockeddoor_tryopen", << -1056.171, 383.577, 68.688 >>,
																			<< -0.000, 0.000, 97.793 >>),PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, vRotation.z)
												TASK_PLAY_ANIM_ADVANCED(NULL, "rcmnigel1b", "lockeddoor_tryopen", << -1056.171, 383.577, 68.688 >>, << -0.000, 0.000, 97.793 >>,
																		SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY | AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION)
											CLOSE_SEQUENCE_TASK(sequenceAI)
											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), sequenceAI)
											CLEAR_SEQUENCE_TASK(sequenceAI)
											//B*1988897 - force player weapon away for anim
											GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponTypeStoredCurrentEquipped)
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
											
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
											
											bTrevorBargedWestGate = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered west gate anim, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered west gate anim, failed area occupied, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered west gate anim, failed for player heading out of range, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered west gate anim, failed for player climbing / jumping, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered west gate anim, failed for door being open, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		// East gate
		IF NOT bTrevorBargedEastGate
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1040.536987,383.536926,67.963135>>, <<-1041.791748,383.561371,70.993462>>, 1.200000)
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF IS_DOOR_IN_LOCKED_STATE(N1B_MCD_EAST_GATE_01)	// only play the anim if the door is locked
						IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
						AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
						AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
							// make sure player is facing towards the door
							IF IS_PED_CLOSE_TO_IDEAL_HEADING(PLAYER_PED_ID(), 277.6446, 90.0)
								IF NOT IS_AREA_OCCUPIED(<< -1042.09949, 382.67090, 68.07068 >>, << -1040.59473, 384.26083, 71.07093 >>, FALSE, TRUE, FALSE, FALSE, FALSE)	// B*1165823
									REQUEST_ANIM_DICT("rcmnigel1b")
									vRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("rcmnigel1b", "lockeddoor_tryopen", << -1040.550, 382.850, 68.850 >>, << 0.000, 0.000, -90.000 >>)
									IF HAS_ANIM_DICT_LOADED("rcmnigel1b")
										IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
											OPEN_SEQUENCE_TASK(sequenceAI)
												TASK_GO_STRAIGHT_TO_COORD(NULL, GET_ANIM_INITIAL_OFFSET_POSITION("rcmnigel1b", "lockeddoor_tryopen", << -1040.550, 382.850, 68.850 >>,
																			<< 0.000, 0.000, -90.000 >>),PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, vRotation.z)
												TASK_PLAY_ANIM_ADVANCED(NULL, "rcmnigel1b", "lockeddoor_tryopen", << -1040.550, 382.850, 68.850 >>, << 0.000, 0.000, -90.000 >>,
																		SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY | AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION)
											CLOSE_SEQUENCE_TASK(sequenceAI)
											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), sequenceAI)
											CLEAR_SEQUENCE_TASK(sequenceAI)
											//B*1988897 - force player weapon away for anim
											GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponTypeStoredCurrentEquipped)
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
											REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
											bTrevorBargedEastGate = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered east gate anim, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
										ENDIF
									ENDIF
								ELSE									
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered east gate anim, failed area occupied, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered east gate anim, failed for player heading out of range, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered east gate anim, failed for player climbing / jumping, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - triggered east gate anim, failed for door being open, frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// comments from Trevor when barging the gates
	IF bDoneDialogue_TrevorBargesLockedGate[0]
	AND bDoneDialogue_TrevorBargesLockedGate[1]
		//both comments done so don't recheck
	ELSE
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "rcmnigel1b", "lockeddoor_tryopen")
			IF (GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "rcmnigel1b", "lockeddoor_tryopen") > 0.568)
				IF NOT bDoneDialogue_TrevorBargesLockedGate[0]
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PTLG1", CONV_PRIORITY_MEDIUM)
							// Nope, locked.  
							bDoneDialogue_TrevorBargesLockedGate[0] = TRUE
						ENDIF
					ENDIF
				ELIF NOT bDoneDialogue_TrevorBargesLockedGate[1]
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PTLG2", CONV_PRIORITY_MEDIUM)
							// I could just climb over.  
							bDoneDialogue_TrevorBargesLockedGate[1] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Interrupt"))
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		//B*1988897 - re-equip player weapon
		IF weaponTypeStoredCurrentEquipped != WEAPONTYPE_UNARMED
		AND weaponTypeStoredCurrentEquipped != WEAPONTYPE_INVALID
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), weaponTypeStoredCurrentEquipped)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weaponTypeStoredCurrentEquipped, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE - * triggered anim interrupt event so reset player current weapon , frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			weaponTypeStoredCurrentEquipped = WEAPONTYPE_UNARMED	// stop this getting re-equiped again
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles the player not losing his wanted level inside the house area
///    also triggers the music event for the player getting a wanted level early on
PROC CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		//don't let the wanted level clear if the player is inside the house area
		IF ePlayerLocation <> N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA			
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			// If player is in the house with wanted level, force the big gates open, bug fix B*1050905 - cops don't seem able to enter house unless a gate is open
			// also done if player has made the pool peds flee, so they can use alternative flee routes bug fix B*1129401
			IF NOT bForceHouseGatesOpenDuringGameplay
				SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(FALSE)
				// open the side gate too
				SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_WEST_GATE_01, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_WEST_GATE_01]), FALSE, FALSE)
				SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_WEST_GATE_01, DOORSTATE_UNLOCKED)		
				bHaveNorthGatesBeenOpenedByPlayer = TRUE
				bHaveSouthGatesBeenOpenedByPlayer = TRUE
				bForceHouseGatesOpenDuringGameplay = TRUE
				CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL - bForceHouseGatesOpenDuringGameplay set TRUE")
			ENDIF
		ENDIF
		REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_START)
	ENDIF		
ENDPROC

/// PURPOSE:
///    get the given ped's current movement state
///    NOTE: for use with the ped detection system
/// PARAMS:
///    pedIndex - ped to get update for 
///    bDoDeadCheck - check ped is alive first
/// RETURNS:
///    N1B_PED_MOVEMENT_STATE for the ped
FUNC N1B_PED_MOVEMENT_STATE GET_UPDATED_PED_MOVEMENT_STATE(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	N1B_PED_MOVEMENT_STATE eCurrent_PedMovementState
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_DEFAULT
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() return N1B_PEDMOVEMENTSTATE_DEFAULT ped not alive") ENDIF #ENDIF
			RETURN eCurrent_PedMovementState
		ENDIF
	ENDIF	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_IN_VEHICLE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_IN_VEHICLE") ENDIF #ENDIF
	ELIF GET_PED_STEALTH_MOVEMENT(pedIndex)
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_STEALTH
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_STEALTH") ENDIF #ENDIF
	ELIF IS_PED_IN_COVER(pedIndex)
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_COVER
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_COVER") ENDIF #ENDIF
	ELIF IS_PED_WALKING(pedIndex)
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_WALKING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_WALKING") ENDIF #ENDIF
	ELIF IS_PED_RUNNING(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_RUNNING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_RUNNING") ENDIF #ENDIF
	ELIF IS_PED_SPRINTING(PLAYER_PED_ID())
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_SPRINTING
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_SPRINTING") ENDIF #ENDIF
	ELSE
		eCurrent_PedMovementState = N1B_PEDMOVEMENTSTATE_DEFAULT
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_PED_MOVEMENT_STATE() -> N1B_PEDMOVEMENTSTATE_DEFAULT") ENDIF #ENDIF		
	ENDIF
	RETURN eCurrent_PedMovementState
ENDFUNC

/// PURPOSE:
///    update the player's movement state, for use with ped detection
/// PARAMS:
///    bDoDeadCheck - check player is alive first
PROC UPDATE_PLAYER_MOVEMENT_STATE(BOOL bDoDeadCheck = TRUE)
	ePlayerMovementState = GET_UPDATED_PED_MOVEMENT_STATE(PLAYER_PED_ID(), bDoDeadCheck)
ENDPROC

/// PURPOSE:
///    get the player's updated movement state
/// RETURNS:
///    ePlayerMovementState
FUNC N1B_PED_MOVEMENT_STATE GET_UPDATED_PLAYER_MOVEMENT_STATE()
	RETURN ePlayerMovementState
ENDFUNC

/// PURPOSE:
///    Set the specified ped's seeing and hearing range based on the player's movements.  Small and standar range can be overriden
/// PARAMS:
///    pedIndex - ped to set seeing and hearing range for
///    bDoDeadCheck - check ped is alive first
/// RETURNS:
///    TRUE if the ranges where set successfully
FUNC BOOl SET_PED_SEEING_AND_HEARING_RANGE_BASED_ON_PLAYER_MOVEMENT(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	// ePlayerMovementState is set each frame in the main mission stage loop, cuts out calling the functions multiple times a frame
	IF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_COVER
	OR ePlayerMovementState = N1B_PEDMOVEMENTSTATE_STEALTH
		SET_PED_SEEING_RANGE(pedIndex, NIGEL1B_PED_SEEING_RANGE_PLAYER_STEALTH_MODE)
		SET_PED_HEARING_RANGE(pedIndex, NIGEL1B_PED_HEARING_RANGE_PLAYER_STEALTH_MODE)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_SEEING_AND_HEARING_RANGE - Stealth / Cover settings") ENDIF #ENDIF
	ELIF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_RUNNING
	OR ePlayerMovementState = N1B_PEDMOVEMENTSTATE_SPRINTING
		SET_PED_SEEING_RANGE(pedIndex, NIGEL1B_PED_SEEING_RANGE_PLAYER_RUNNING)
		SET_PED_HEARING_RANGE(pedIndex, NIGEL1B_PED_HEARING_RANGE_PLAYER_RUNNING)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_SEEING_AND_HEARING_RANGE  - RUNNING / SPRINTING SETTINGS") ENDIF #ENDIF	
	ELSE
		//default seeing and hearing range for player walking around
		SET_PED_SEEING_RANGE(pedIndex, NIGEL1B_PED_SEEING_RANGE_STANDARD)
		SET_PED_HEARING_RANGE(pedIndex, NIGEL1B_PED_HEARING_RANGE_STANDARD)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_PED_SEEING_AND_HEARING_RANGE  - NIGEL1B_PED_HEARING_RANGE_STANDARD SETTINGS") ENDIF #ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player has beaten up one of the pool peds
/// RETURNS:
///    	TRUE if a the MISSION_PED's AI is N1B_PED_AI_SETUP_BEATEN_UP or N1B_PED_AI_STATE_BEATEN_UP
FUNC BOOL HAS_PLAYER_BEATEN_UP_A_POOL_PED(INT &iReturnPedIndex)
	INT i = 0
	REPEAT TOTAL_POOL_PEDS i
		IF IS_ENTITY_ALIVE(sPoolPed[i].pedIndex)					
			IF sPoolPed[i].AI = N1B_PED_AI_SETUP_BEATEN_UP
			OR sPoolPed[i].AI = N1B_PED_AI_STATE_BEATEN_UP
				iReturnPedIndex = i
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player has a weapon and is intimidating a ped enough that they should react
/// PARAMS:
///    pedIndex - Ped who the player is intimidating
///    fPlayerDetectionDist - Player is automatically classed as intimidating when shooting or targetting within this range
///    fBulletRadiusCheck - Used for IS_BULLET_IN_AREA check around pedIndex
///    fProjectileRadiusCheck - Used for IS_PROJECTILE_IN_AREA check around pedIndex
/// RETURNS:
///    True if bullet is in area around ped, if player is shooting, if player is targetting or free aiming at ped in view or if damaged.
FUNC BOOL IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(PED_INDEX pedIndex, FLOAT fPlayerDetectionDist = 20.0, FLOAT fBulletRadiusCheck = 5.0, FLOAT fProjectileRadiusCheck = 15.0)
	IF IS_PED_UNINJURED(pedIndex)	
		IF IS_PLAYER_CURRENT_WEAPON_LETHAL(TRUE, TRUE, TRUE)
			VECTOR vTemp_PedPos = GET_ENTITY_COORDS(pedIndex)		
			IF IS_BULLET_IN_AREA(vTemp_PedPos, fBulletRadiusCheck, TRUE)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - bullet in area around ped") ENDIF #ENDIF
				RETURN TRUE
			ENDIF		
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedIndex, fPlayerDetectionDist)	//and is with in the range
				IF SET_PED_SEEING_AND_HEARING_RANGE_BASED_ON_PLAYER_MOVEMENT(pedIndex, FALSE)
					//IF CAN_PED_SEE_PLAYER(pedIndex)						
					IF CAN_PED_SEE_HATED_PED(pedIndex, PLAYER_PED_ID())		
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - playing free aiming or targeting ped") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
						IF IS_PED_SHOOTING(PLAYER_PED_ID())	
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - player shooting with weapon") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
					ELSE
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - ped can't see player") ENDIF #ENDIF
					ENDIF
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - player not in range of ped") ENDIF #ENDIF
			ENDIF		
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - ped damaged by player with weapon") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF			
			VECTOR vMin, vMax // is one of the player's projectiles nearby?
			vMin = vTemp_PedPos
			vMax = vMin
			vMin.x= vMin.x - fProjectileRadiusCheck
			vMin.y = vMin.y -fProjectileRadiusCheck
			vMin.z = vMin.z - fProjectileRadiusCheck
			vMax.x = vMax.x + fProjectileRadiusCheck
			vMax.y = vMax.y + fProjectileRadiusCheck
			vMax.z = vMax.z + fProjectileRadiusCheck
			IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "intimidating checks - projectile in area around ped") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Checks if player has intimidated a pool ped, uses IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED()
/// RETURNS:
///    true if player has intimidated a pool ped
FUNC BOOL IS_PLAYER_INTIMIDATING_POOL_PEDS()
	INT i
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		IF IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sPoolPed[i].pedIndex)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check to see if the player has started fighting the ped
/// PARAMS:
///    pedIndex - Ped who player is starting fight with
/// RETURNS:
///    True if the player is targetting the ped, or has damaged the ped
FUNC BOOL HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(PED_INDEX pedIndex, FLOAT fPlayerDetectionDist = 4.0)
	IF IS_PED_UNINJURED(pedIndex)			
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER DAMAGED PED") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedIndex, fPlayerDetectionDist)	//and is with in the range
			IF CAN_PED_SEE_PLAYER(pedIndex) 			// player can be seen by other ped
				IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
				OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED() check - PLAYER TARGETTED PED") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if player has started fight with pool peds, uses HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED()
/// RETURNS:
///    true if player has started fight
FUNC BOOL HAS_PLAYER_STARTED_FIGHT_WITH_POOL_PEDS()
	INT i = 0
	REPEAT TOTAL_POOL_PEDS i
		IF HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(sPoolPed[i].pedIndex)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player is firing a lethal weapon on the property
/// RETURNS:
///    TRUE if the player is with in the grounds and firing
FUNC BOOL IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
	IF IS_PED_SHOOTING(PLAYER_PED_ID())	
		IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
		OR ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
		OR ePlayerLocation = N1B_PLAYER_LOCATION_INSIDE_HOUSE_AREA
			IF NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())	// added as per B*1079200 firing silence weapon alerts everyone
				WEAPON_TYPE currentPlayerWeapon
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
				IF currentPlayerWeapon != WEAPONTYPE_STICKYBOMB	// B*1890698 - ignore sticky bombs here, covered by intimidate checks
					IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY - return TRUE") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY - return FALSE : failed sticky bomb check")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player is firing a lethal weapon on the property
/// RETURNS:
///    TRUE if the player is with in the grounds and firing
FUNC BOOL IS_PLAYER_DRIVING_VEHICLE_IN_AREA_OF_PROPERTY(N1B_PLAYER_LOCATION eLocationToTest)
	IF ePlayerLocation = eLocationToTest
		IF (GET_UPDATED_PLAYER_MOVEMENT_STATE() = N1B_PEDMOVEMENTSTATE_IN_VEHICLE)
			VEHICLE_INDEX vehicleTempPlayer
			vehicleTempPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), FALSE)
			IF IS_VEHICLE_OK(vehicleTempPlayer)
				IF GET_IS_VEHICLE_ENGINE_RUNNING(vehicleTempPlayer)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_DRIVING_VEHICLE_IN_AREA_OF_PROPERTY - RETURN TRUE - for area - ", eLocationToTest) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check for B*1544327 & B*1544302 - pushing door pushes gardener who doesn't react
/// RETURNS:
///    TRUE if player is probably pushing the van door into the gardener
FUNC BOOL HAS_PLAYER_POTENTIALLY_KNOCKED_VAN_DOOR_INTO_GARDENER()

	IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
		IF IS_VEHICLE_OK(vehGardenerVehicle)
			FLOAT fGardenerVanDoorRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(vehGardenerVehicle, SC_DOOR_REAR_LEFT)
			IF fGardenerVanDoorRatio < 0.49
				CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_POTENTIALLY_KNOCKED_VAN_DOOR_INTO_GARDENER return TRUE ratio : ", fGardenerVanDoorRatio)
				RETURN TRUE
			ENDIF
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_PLAYER_POTENTIALLY_KNOCKED_VAN_DOOR_INTO_GARDENER ratio : ", fGardenerVanDoorRatio) ENDIF #ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a ped should detect the player - using IS_ENTITY_TOUCHING_ENTITY, CAN_PED_SEE_HATED_PED and distance checks between the peds
/// PARAMS:
///    pedIndex - the ped to test 
///    bDoDeadCheck - do dead check on ped first
///    bDoCanPedSeePedCheck - if false CAN_PED_SEE_HATED_PED isn't done.
///    bDoDistanceDetectionTestPlayerWalk - do ped distance check for player walking closeby
///    bDoDistanceDetectionTestPlayerRun - do ped distance check for player walking closeby
///    fSeeingRangeSmall - ped's small seeing range (for when player is in cover / using stealth)
///    fSeeingRangeStandard - ped's standard seeing range
///    bExitEarlyIfPlayerIsPerformingStealthKillOnPed - if TRUE, when the player is performing a stealth kill on pedIndex, the function will return FALSE regardless
/// RETURNS:
///    True if pedIndex can see the player
FUNC BOOL HAS_SPECIFIC_PED_DETECTED_THE_PLAYER(PED_INDEX pedIndex, BOOL bDoDeadCheck = TRUE, BOOL bDoCanPedSeePedCheck = TRUE, BOOL bDoDistanceDetectionTestPlayerWalk = TRUE, BOOL bDoDistanceDetectionTestPlayerRun = TRUE, BOOL bExitEarlyIfPlayerIsPerformingStealthKillOnPed = TRUE)
	IF bDoDeadCheck
		IF NOT IS_ENTITY_ALIVE(pedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	IF bExitEarlyIfPlayerIsPerformingStealthKillOnPed
		IF IS_PED_BEING_STEALTH_KILLED(pedIndex)
			IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN FALSE - exit early as player is performing stealth kill") ENDIF #ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF	
	//Check to see if the ped can see the player based off the set seeing and hearing ranges
	IF bDoCanPedSeePedCheck
		IF SET_PED_SEEING_AND_HEARING_RANGE_BASED_ON_PLAYER_MOVEMENT(pedIndex, FALSE)	// update the ped's seeing and hearing range based off the players movement state
			IF CAN_PED_SEE_HATED_PED(pedIndex, PLAYER_PED_ID())		
				//#IF IS_DEBUG_BUILD DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID()), 255, 0, 0)	#ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN TRUE - PED SEES HATED PLAYER ") ENDIF #ENDIF	
				#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF bDoDistanceDetectionTestPlayerRun
		IF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_RUNNING
		OR ePlayerMovementState = N1B_PEDMOVEMENTSTATE_SPRINTING
		//detect the player regardless of seeing range if he's running close to the ped					
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, NIGEL1B_PED_DETECTED_CLOSEBY_RUNNING_RANGE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN TRUE - PLAYER CLOSE TO PED running / sprinting") ENDIF #ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF				
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	IF bDoDistanceDetectionTestPlayerWalk	
		IF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_WALKING					
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerPos, NIGEL1B_PED_DETECTED_CLOSEBY_WALKING_RANGE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN TRUE - PLAYER CLOSE TO PED walking") ENDIF #ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	IF IS_PED_BEING_STUNNED(pedIndex)
		WEAPON_TYPE currentPlayerWeapon
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN TRUE - ped getting stunned by player") ENDIF #ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	//automatically detect the player if he's touching the ped
	IF IS_ENTITY_TOUCHING_ENTITY(pedIndex, PLAYER_PED_ID())	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_SPECIFIC_PED_DETECTED_THE_PLAYER - RETURN TRUE - Player touching PED") ENDIF #ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF		
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the gardener ped should detect the player - using IS_ENTITY_TOUCHING_ENTITY, CAN_PED_SEE_HATED_PED (with extra test to fix B*415016) and distance checks between the peds
/// PARAMS:
///    bDoCanPedSeePedCheck - if false CAN_PED_SEE_HATED_PED isn't done.
///    iFrameCounter_PedLastSeePlayer - the frame CAN_PED_SEE_PED last returned true for the pedIndex
///    bDoDistanceDetectionTestPlayerWalk - do ped distance check for player walking closeby
///    bDoDistanceDetectionTestPlayerRun - do ped distance check for player walking closeby
///    fSeeingRangeSmall - ped's small seeing range (for when player is in cover / using stealth)
///    fSeeingRangeStandard - ped's standard seeing range
///    bExitEarlyIfPlayerIsPerformingStealthKillOnPed - if TRUE, when the player is performing a stealth kill on sGardenerPed.pedIndex, the function will return FALSE regardless
/// RETURNS:
///    True if sGardenerPed.pedIndex can see the player
FUNC BOOL HAS_GARDENER_PED_DETECTED_THE_PLAYER(BOOL bDoCanPedSeePedCheck = TRUE, INT iFrameCounter_PedLastSeePlayer = 0, BOOL bDoDistanceDetectionTestPlayerWalk = TRUE, BOOL bDoDistanceDetectionTestPlayerRun = TRUE, BOOL bExitEarlyIfPlayerIsPerformingStealthKillOnPed = TRUE)
	IF NOT IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
		RETURN FALSE
	ENDIF
	IF bExitEarlyIfPlayerIsPerformingStealthKillOnPed
		IF IS_PED_BEING_STEALTH_KILLED(sGardenerPed.pedIndex)
			IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())			
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN FALSE - exit early as player is performing stealth kill") ENDIF #ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF	
	//Check to see if the ped can see the player based off the set seeing and hearing ranges
	IF bDoCanPedSeePedCheck
		IF SET_PED_SEEING_AND_HEARING_RANGE_BASED_ON_PLAYER_MOVEMENT(sGardenerPed.pedIndex, FALSE)	// update the ped's seeing and hearing range based off the players movement state
			IF CAN_PED_SEE_HATED_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())				
				#IF IS_DEBUG_BUILD	DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sGardenerPed.pedIndex), GET_ENTITY_COORDS(PLAYER_PED_ID()), 255, 0, 0) #ENDIF	
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - PED SEES HATED PLAYER = TRUE at GET_GAME_TIMER() ", GET_GAME_TIMER()) ENDIF #ENDIF
				// if the ped has seen the player with the last x amount of frames
				IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN TRUE - PED SEES HATED PLAYER with iFrameCounter_PedLastSeePlayer at GET_FRAME_COUNT() ", GET_FRAME_COUNT()) ENDIF #ENDIF
					//#IF IS_DEBUG_BUILD DRAW_RECT(0.3, 0.5, 0.2, 0.4, 0, 255, 0, 150) #ENDIF	
					#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF					
					RETURN TRUE
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN FALSE - extra player clear sight check is FALSE : GAME TIME = ", GET_GAME_TIMER()) ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bDoDistanceDetectionTestPlayerRun
		IF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_RUNNING
		OR ePlayerMovementState = N1B_PEDMOVEMENTSTATE_SPRINTING
		//detect the player regardless of seeing range if he's running close to the ped					
			IF IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, NIGEL1B_PED_DETECTED_CLOSEBY_RUNNING_RANGE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN TRUE - PLAYER CLOSE TO PED running / sprinting") ENDIF #ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF				
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	IF bDoDistanceDetectionTestPlayerWalk	
		IF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_WALKING					
			IF IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, NIGEL1B_PED_DETECTED_CLOSEBY_WALKING_RANGE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN TRUE - PLAYER CLOSE TO PED walking") ENDIF #ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_BEING_STUNNED(sGardenerPed.pedIndex)
		WEAPON_TYPE currentPlayerWeapon
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN TRUE - ped getting stunned by player") ENDIF #ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	//automatically detect the player if he's touching the ped
	IF IS_ENTITY_TOUCHING_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID())	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HAS_GARDENER_PED_DETECTED_THE_PLAYER - RETURN TRUE - Player touching PED") ENDIF #ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_ToggleDoSpottedPlayerChecksPause SET_GAME_PAUSED(TRUE) ENDIF #ENDIF
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the player is using stealth in the pool area, area checks
/// RETURNS:
///    True if the player isn't deemed as in stealth mode
FUNC BOOL IS_PLAYER_USING_STEALTH_IN_POOL_AREA()
	IF IS_PLAYER_DRIVING_VEHICLE_IN_AREA_OF_PROPERTY(N1B_PLAYER_LOCATION_POOL_AREA)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_USING_STEALTH_IN_POOL_AREA - Player in a vehicle, in the pool area") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA	// ideally this will be outside of the player location check, but left here for now as you can be detected through the house bug				
		//Can set this up so it only checks one ped per frame
		INT i
		FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
			IF i = POOL_PED_FEMALE
				//change the seeing distance for the girls in the HOT_TUB so the player can sneak around the back still
				IF HAS_SPECIFIC_PED_DETECTED_THE_PLAYER(sPoolPed[POOL_PED_FEMALE].pedIndex, TRUE, TRUE, TRUE, TRUE)	// 9.0, 10.0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, USING HAS_SPECIFIC_PED_DETECTED_THE_PLAYER id = ", i) ENDIF #ENDIF
					iPedID_PoolPedWhoDetectedPlayer = POOL_PED_FEMALE	//store ped ID who spotted the player, to play alternative dialogue.
					RETURN FALSE
				ENDIF
			ELSE
				IF HAS_SPECIFIC_PED_DETECTED_THE_PLAYER(sPoolPed[i].pedIndex, FALSE)
					//ignore the detecting if the celeb is swimming with his head underwater
					IF sPoolPed[i].AI = N1B_PED_AI_STATE_ROUTINE								//only used by celeb in pool for now
					OR sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH				//only used by celeb in pool for now
					OR sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_STATE_DO_FINAL_LENGTH				//only used by celeb in pool for now		
						// if the player has jumped in the pool, we don't want to override the ped not spotting him
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, player is in the pool id = ", i) ENDIF #ENDIF
							iPedID_PoolPedWhoDetectedPlayer = POOL_PED_CELEB	//store ped ID who spotted the player, to play alternative dialogue.
							RETURN FALSE
						ENDIF
						FLOAT fPedMovementRatio
						fPedMovementRatio = GET_PED_DESIRED_MOVE_BLEND_RATIO(sPoolPed[i].pedIndex)
						/*IF IS_PED_SWIMMING(sPoolPed[i].pedIndex)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, ped swimming # 1 ") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, <<-1054.633423,353.726318,67.444794>>, <<-1054.807007,370.001160,70.5>>, 5.500000)	//inside the pool
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, ped in pool # 2 ") ENDIF #ENDIF
						ENDIF
						IF IS_MOVE_BLEND_RATIO_SPRINTING(fPedMovementRatio)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, ped is sprinting # 3 ") ENDIF #ENDIF
						ENDIF
						IF IS_MOVE_BLEND_RATIO_RUNNING(fPedMovementRatio)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, ped is running # 3 ") ENDIF #ENDIF
						ENDIF*/
						IF IS_MOVE_BLEND_RATIO_SPRINTING(fPedMovementRatio)
						OR IS_MOVE_BLEND_RATIO_RUNNING(fPedMovementRatio)
						AND IS_PED_SWIMMING(sPoolPed[i].pedIndex)
						AND IS_ENTITY_IN_ANGLED_AREA(sPoolPed[i].pedIndex, vPoolDimensionsMinPos, vPoolDimensionsMaxPos, fPoolDimensionsWidth)	//inside the pool						
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, USING HAS_SPECIFIC_PED_DETECTED_THE_PLAYER id = ", i, "but ped was swimming head down") ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, USING HAS_SPECIFIC_PED_DETECTED_THE_PLAYER id = ", i) ENDIF #ENDIF
							iPedID_PoolPedWhoDetectedPlayer = POOL_PED_CELEB	//store ped ID who spotted the player, to play alternative dialogue.
							RETURN FALSE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER DETECTED, USING HAS_SPECIFIC_PED_DETECTED_THE_PLAYER id = ", i) ENDIF #ENDIF
						iPedID_PoolPedWhoDetectedPlayer = POOL_PED_CELEB	//store ped ID who spotted the player, to play alternative dialogue.
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///     Requests the standard cop ped model and cop car model
PROC REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
	REQUEST_MODEL(mnCopCar)
	REQUEST_MODEL(mnCopPed)
ENDPROC

/// PURPOSE:
///    checks if the standard cop ped model and cop car model have loaded
/// RETURNS:
///    TRUE if they have loaded
FUNC BOOL HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
	REQUEST_MODEL(mnCopCar)
	REQUEST_MODEL(mnCopPed)	
	IF HAS_MODEL_LOADED(mnCopCar)
	AND HAS_MODEL_LOADED(mnCopPed)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "cop models - LOADED  ") ENDIF #ENDIF
		RETURN TRUE
	ENDIF	
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "waiting for asset to load - cop models   ") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    creates a cop and cop car close to the player, First attempt spawn pos from behind, fall back attempt is spawn point in front
/// PARAMS:
///    vTempPlayerPos - the player's current position
///    fTempRangeBehindPlayer - spawn cops from behind this distance away
///    fTempRangeInFrontPlayer - spawn cops in front this distance away
/// RETURNS:
///    TRUE if cop was successful created in range of the player
FUNC BOOL CREATE_COP_IN_RANGE_OF_PLAYER(VECTOR vTempPlayerPos, FLOAT fTempRangeBehindPlayer = -40.00, FLOAT fTempRangeInFrontPlayer = 40.0)
	BOOL bOkToSpawn	
	VECTOR vTempPlayerOffset
	VECTOR vTempSpawnPoint
	FLOAT fTempSpawnHeading
	//attempt to spawn the cop car BEHIND the player
	vTempPlayerOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, fTempRangeBehindPlayer, 0.0>>)	
	IF GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(vTempPlayerOffset, vTempPlayerPos, 0, vTempSpawnPoint, fTempSpawnHeading)	
		IF NOT IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vTempSpawnPoint, <<7.0, 7.0, 7.0>>)		
			IF WOULD_ENTITY_BE_OCCLUDED(mnCopCar, vTempSpawnPoint)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: SUCCESSFUL SPAWN NODE FOUND BEHIND") ENDIF #ENDIF
				bOkToSpawn = TRUE
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED ENTITY WOULD BE OCCLUDED USING THE CURRENT NODE BEHIND") ENDIF #ENDIF
				bOkToSpawn = FALSE
			ENDIF
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED DUE TO NODE POS OBSCURED BY MISSION ENTITY BEHIND" )#ENDIF
			bOkToSpawn = FALSE
		ENDIF
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED TO FIND SUITABLE NODE BEHIND") ENDIF #ENDIF
		bOkToSpawn = FALSE
	ENDIF	
	IF NOT bOkToSpawn
		//second attempt to spawn the cop car, this time IN FRONT of the player
		vTempPlayerOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0, fTempRangeInFrontPlayer, 0.0>>)		
		IF GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(vTempPlayerOffset, vTempPlayerPos, 0, vTempSpawnPoint, fTempSpawnHeading)
			IF NOT IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vTempSpawnPoint, <<7.0, 7.0, 7.0>>)		
				IF WOULD_ENTITY_BE_OCCLUDED(mnCopCar, vTempSpawnPoint)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: SUCCESSFUL SPAWN NODE FOUND IN FRONT") ENDIF #ENDIF
					bOkToSpawn = TRUE
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED ENTITY WOULD BE OCCLUDED USING THE CURRENT NODE IN FRONT") ENDIF #ENDIF
					bOkToSpawn = FALSE
				ENDIF
			ELSE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED DUE TO NODE POS OBSCURED BY MISSION ENTITY IN FRONT") ENDIF #ENDIF
				bOkToSpawn = FALSE
			ENDIF
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: FAILED TO FIND SUITABLE NODE IN FRONT") ENDIF #ENDIF
			bOkToSpawn = FALSE
		ENDIF
	ENDIF	
	IF bOkToSpawn
		VEHICLE_INDEX vehIndex
		IF NOT DOES_ENTITY_EXIST(vehIndex)
			vehIndex = CREATE_VEHICLE(mnCopCar, vTempSpawnPoint, fTempSpawnHeading)
		ENDIF		
		IF IS_ENTITY_ALIVE(vehIndex)		
			PED_INDEX pedIndex1
			IF NOT DOES_ENTITY_EXIST(pedIndex1)
				pedIndex1 = CREATE_PED_INSIDE_VEHICLE(vehIndex, PEDTYPE_COP, mnCopPed, VS_DRIVER)
			ENDIF			
			SET_SIREN_WITH_NO_DRIVER(vehIndex, TRUE)
			SET_VEHICLE_SIREN(vehIndex, TRUE)			
			//match player's speed
			FLOAT fTempSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
			IF fTempSpeed > 40.0
				fTempSpeed = 40.0
			ENDIF
			SET_VEHICLE_FORWARD_SPEED(vehIndex, fTempSpeed)			
			IF IS_ENTITY_ALIVE(pedIndex1)
				SET_PED_RANDOM_COMPONENT_VARIATION(pedIndex1)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex1, FALSE)
				GIVE_WEAPON_TO_PED(pedIndex1, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE) 
				SET_CURRENT_PED_WEAPON(pedIndex1, WEAPONTYPE_PISTOL, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(pedIndex1)
			ENDIF			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehIndex)		
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE COP: COPS CREATED ********	_______	------	******") ENDIF #ENDIF
			RETURN TRUE
		ENDIF	
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates celeb possession blips, sets blip colours and set's blip names for Map pause menu
PROC CREATE_CELEB_POSSESSION_BLIPS()
	//Pickup_index setup
	IF NOT DOES_BLIP_EXIST(mpClothesPickup.blipIndex)
		IF DOES_PICKUP_EXIST(mpClothesPickup.index)
			mpClothesPickup.blipIndex = CREATE_BLIP_FOR_PICKUP(mpClothesPickup.index)
		ENDIF
	ENDIF
	IF DOES_BLIP_EXIST(mpClothesPickup.blipIndex)
		//SET_BLIP_COLOUR(mpClothesPickup.blipIndex, BLIP_COLOUR_GREEN)
		SET_BLIP_NAME_FROM_TEXT_FILE(mpClothesPickup.blipIndex, "NIG1B_CP_BLIP")	// Clothes
	ENDIF
ENDPROC

/// PURPOSE:
///    trigger additional dialogue from the celeb and the female pool ped when player is hanging around
///    and has been noticed
/// RETURNS:
///    TRUE if dialogue was triggered
FUNC BOOL TRIGGER_ADDITIONAL_AMBIENT_LINES_FOR_TREVOR_HANGING_AROUND()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
		IF iRand = POOL_PED_CELEB
			IF bDoneDialogue_PlayerGrabsClothes	// different dialogue depending if player has been seen nicking the clothes or not
				IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TAMB", CONV_PRIORITY_MEDIUM)
					// Random selected lines - 
					// You're like the creepiest stalker ever.
					// This is so unpleasant, man. Just give me my stuff back.
					// Are you going to sell my underwear to a Japanese businessman?  
					// I hope you've got a really good lawyer.  
					// Two words for you - restraining order.  
					RETURN TRUE
				ENDIF
			ELSE
				IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TAMB2", CONV_PRIORITY_MEDIUM)
					// Random selected lines - 
					// What are you doing in here, man?  
					// You're being a major buzzkill.
					// Are you one of those dudes with a kid's brain?    
					// Who gave you this address?  
					// I've had obsessed fans before, but you're the weirdest.  
					// Did Cloe put you up to this?
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // POOL_PED_FEMALE 
			IF bDoneDialogue_PlayerGrabsClothes	// different dialogue depending if player has been seen nicking the clothes or not
				IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GAMB", CONV_PRIORITY_MEDIUM)
					// Random selected lines - 
					// Just take the clothes and go, please.  
					// What else do you want from us?
					// I think I saw him sniff your boxers. 
					RETURN TRUE
				ENDIF
			ELSE
				IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GAMB2", CONV_PRIORITY_MEDIUM)
					// Random selected lines - 
					// He looks like a crackhead.  
					// Is this a reality TV stunt? Am I on camera here?  
					// Tyler deserves privacy, just like everyone else!?
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    pool peds dialogue only
PROC MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE()

	IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)
	AND IS_PED_UNINJURED(sPoolPed[POOL_PED_FEMALE].pedIndex) 
		IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(tlDialogueRoot_PoolPedsCurrentReactionDialogue)
			// player firing weapon dialogue
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sPoolPed[POOL_PED_CELEB].pedIndex), 3.0)
					IF IS_PLAYER_CURRENT_WEAPON_LETHAL(FALSE, FALSE, TRUE)
					// B*1511108 - try to ensure weapon is visible
					AND (IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
					OR IS_PED_RELOADING(PLAYER_PED_ID()))
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TREACT", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Okay, okay, put the gun down.  
							// Okay, I get the message. You're a fucking lunatic.  
							// What are you even shooting at, you maniac?  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TREACT"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_TREACT") ENDIF #ENDIF
							EXIT
						ENDIF
					ELSE
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TSHOCK", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Fuck me!
							// This is insane!   
							// Oh, shit!  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TSHOCK"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_TSHOCK") ENDIF #ENDIF
							EXIT
						ENDIF
					ENDIF
				ELIF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sPoolPed[POOL_PED_FEMALE].pedIndex), 3.0)
					IF IS_PLAYER_CURRENT_WEAPON_LETHAL(FALSE, FALSE, TRUE)
					// B*1511108 - try to ensure weapon is visible
					OR (IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
					AND IS_PED_RELOADING(PLAYER_PED_ID()))
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GREACT", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Oh my God, he's going to kill us!  
							// He's shooting! Why aren't you protecting me?  
							// Oh shit, see now we've pissed him off!
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GREACT"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_GREACT") ENDIF #ENDIF
							EXIT
						ENDIF
					ELSE
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GSHOCK", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Jesus Christ!
							// No way! 
							// I can't handle this!  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GSHOCK"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_GSHOCK") ENDIF #ENDIF
							EXIT
						ENDIF
					ENDIF
				ELSE
					// shooting whilst on in the pool area but not close to one of the pool peds
					IF CAN_PED_SEE_HATED_PED(sPoolPed[POOL_PED_CELEB].pedIndex, PLAYER_PED_ID())	
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TSHOCK", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Fuck me!
							// This is insane!   
							// Oh, shit!  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TSHOCK"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_TSHOCK for seeing player in pool area") ENDIF #ENDIF
							EXIT
						ENDIF
					ELIF CAN_PED_SEE_HATED_PED(sPoolPed[POOL_PED_FEMALE].pedIndex, PLAYER_PED_ID())	
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GSHOCK", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Jesus Christ!
							// No way! 
							// I can't handle this!  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GSHOCK"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_GSHOCK for seeing player in pool area") ENDIF #ENDIF
							EXIT
						ENDIF					
					ENDIF		
				ENDIF
			// player targetting peds dialogue
			ELIF IS_PLAYER_FREE_AIMING(PLAYER_ID())
			OR IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())				
			
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sPoolPed[POOL_PED_CELEB].pedIndex)	
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sPoolPed[POOL_PED_CELEB].pedIndex)
					IF CAN_PED_SEE_HATED_PED(sPoolPed[POOL_PED_CELEB].pedIndex, PLAYER_PED_ID())	
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TTARG", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Take what you want, man, it's not even my house.  
							// If you're going to violate me, just make it quick.
							// Is this chick your daughter or something? 
							// I'm a good guy, don't believe all that stuff they write about me.  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TTARG"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_TTARG") ENDIF #ENDIF
							EXIT
						ENDIF
					ENDIF
				ELIF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sPoolPed[POOL_PED_FEMALE].pedIndex)	
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sPoolPed[POOL_PED_FEMALE].pedIndex)
					IF CAN_PED_SEE_HATED_PED(sPoolPed[POOL_PED_FEMALE].pedIndex, PLAYER_PED_ID())	
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GTARG", CONV_PRIORITY_MEDIUM)
							// Random selected lines - 
							// Oh my God, please, I'm not even famous.  
							// He picked me up in a bar, I swear I don't even know him.
							// No, what are you going to do to us?  
							tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GTARG"
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_GTARG") ENDIF #ENDIF
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_TOUCHING_ENTITY(sPoolPed[POOL_PED_CELEB].pedIndex, PLAYER_PED_ID())
				AND IS_PED_RAGDOLL(sPoolPed[POOL_PED_CELEB].pedIndex)
					IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TBUMP", CONV_PRIORITY_MEDIUM)
						// Random selected lines - 
						// Don't be a dick. 
						// This isn't funny.
						// Don't touch me, man.  
						tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TBUMP"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_TBUMP") ENDIF #ENDIF
						EXIT
					ENDIF
				ELIF IS_ENTITY_TOUCHING_ENTITY(sPoolPed[POOL_PED_FEMALE].pedIndex, PLAYER_PED_ID())
				AND IS_PED_RAGDOLL(sPoolPed[POOL_PED_FEMALE].pedIndex)
					IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GBUMP", CONV_PRIORITY_MEDIUM)
						// Random selected lines - 
						// Ow, that hurt.  
						// Ty-Di, the hobo pushed me!
						// He tried to feel my tits!  
						tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GBUMP"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE - triggered NIG1B_GBUMP") ENDIF #ENDIF
						EXIT
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
				iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER()
			ELSE
				IF HAS_TIME_PASSED(iTimer_DelayAdditionalSurrenderedDialogue, GET_RANDOM_INT_IN_RANGE(2000, 4000))				
					//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_03")	// Leave the area.~s~		//*commented out for new display message check with subtitle profile settings
					//AND NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_04")	// Lose the cops.			//*commented out for new display message check with subtitle profile settings
						IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
							SWITCH iAdditionalSurrenderedDialoguePool_Counter 
								CASE 0
									IF IS_PLAYER_CURRENT_WEAPON_LETHAL(FALSE, FALSE, TRUE)
									AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SU1", "NIG1B_SU1_1", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)	
											//Come on man, just put the gun down.
											iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER()
											iAdditionalSurrenderedDialoguePool_Counter++	
											//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INSIDE ADDITIONAL SURRENDER DIALOGUE- ", iAdditionalBrawlingDialoguePool_Counter) ENDIF #ENDIF
										ENDIF
									ELSE
										iAdditionalSurrenderedDialoguePool_Counter++									
									ENDIF						
								BREAK
								CASE 1
									IF NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SU1", "NIG1B_SU1_2", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)	
										// Am I getting punked here? Don't screw with me. 
										iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER()
										iAdditionalSurrenderedDialoguePool_Counter++	
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INSIDE ADDITIONAL SURRENDER DIALOGUE- ", iAdditionalBrawlingDialoguePool_Counter) ENDIF #ENDIF
									ENDIF					
								BREAK
								CASE 2
									IF NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SU1", "NIG1B_SU1_3", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)	
										// Tyler, he's scaring me!
										iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER()
										iAdditionalSurrenderedDialoguePool_Counter++	
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INSIDE ADDITIONAL SURRENDER DIALOGUE- ", iAdditionalBrawlingDialoguePool_Counter) ENDIF #ENDIF
									ENDIF					
								BREAK
								DEFAULT
									IF TRIGGER_ADDITIONAL_AMBIENT_LINES_FOR_TREVOR_HANGING_AROUND()
										iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER()
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "INSIDE ADDITIONAL SURRENDER DIALOGUE- TRIGGER_ADDITIONAL_AMBIENT_LINES_FOR_TREVOR_HANGING_AROUND done : timer = ", iTimer_DelayAdditionalSurrenderedDialogue) ENDIF #ENDIF
									ENDIF
								BREAK
							ENDSWITCH						
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the current pool area's state, along with the pool ped's relevant AI state.
/// PARAMS:
///    eNewState - The new state
///    bSetRelevantAIState_ForPoolPeds - If TRUE the ped's AI will be updated to the relevant state
PROC SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE eNewState, BOOL bSetRelevantAIState_ForPoolPeds = TRUE)
	ePoolAreaState = eNewState
	IF bSetRelevantAIState_ForPoolPeds		
		INT i 
		FOR i = 0 TO (TOTAL_POOL_PEDS - 1)						
			SWITCH ePoolAreaState	
				CASE N1B_POOL_AREA_STATE_UNDETECTED	
					sPoolPed[i].AI = N1B_PED_AI_STATE_RELAXED
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_UNDETECTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
					//IF i = POOL_PED_CELEB
					//	sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES
					//ELSE
					//	sPoolPed[i].AI = N1B_PED_AI_STATE_RELAXED
					//ENDIF
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
					sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_GARDENER
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_SPOTTED
					sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_CALL_COPS
					IF i = POOL_PED_CELEB
						sPoolPed[i].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_TO_POS_FOR_CALL_THE_COPS
					ELSE
						sPoolPed[i].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_CALL_COPS") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_SURRENDERED
					IF sPoolPed[i].AI <> N1B_PED_AI_SETUP_BEATEN_UP
					OR sPoolPed[i].AI <> N1B_PED_AI_STATE_BEATEN_UP
						sPoolPed[i].AI = N1B_PED_AI_SETUP_SURRENDERED
						// reset the last damaged entity so we can test for the player hurting them whilst they are surrendered.
						IF IS_ENTITY_ALIVE(sPoolPed[i].pedIndex)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sPoolPed[i].pedIndex)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_SURRENDERED") ENDIF #ENDIF
				BREAK
				CASE N1B_POOL_AREA_STATE_FLEE
					sPoolPed[i].AI = N1B_PED_AI_SETUP_FLEE_WITH_REACT
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_POOL_AREA_STATE - set ped AI (ID = ", i, ") for N1B_POOL_AREA_STATE_FLEE") ENDIF #ENDIF
				BREAK
			ENDSWITCH
		ENDFOR
	ENDIF
	IF ePoolAreaState = N1B_POOL_AREA_STATE_SURRENDERED		
		iTimer_DelayPoolPedsFleeWhenSurrendered = GET_GAME_TIMER()	// Set timer ready for peds to flee when the player leaves the area
	ELIF ePoolAreaState = N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
	OR ePoolAreaState = N1B_POOL_AREA_STATE_CALL_COPS
		iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()	//used in these state to countdown till cops should be called.
	ELIF ePoolAreaState = N1B_POOL_AREA_STATE_FLEE
		// If player has made the pool peds flee, force the big gates open and the side gate, so they can use alternative flee routes bug fix B*1129401, also done if player is wanted on property
		IF NOT bForceHouseGatesOpenDuringGameplay
			SET_HOUSE_DOUBLE_GATES_LOCKED_STATE(FALSE)
			// open the side gate too
			SET_MISSION_CONTROLLED_DOOR_OPEN_RATIO(N1B_MCD_WEST_GATE_01, DOOR_SYSTEM_GET_OPEN_RATIO(iMissionControlledDoors[N1B_MCD_WEST_GATE_01]), FALSE, FALSE)
			SET_MISSION_CONTROLLED_DOOR_STATE(N1B_MCD_WEST_GATE_01, DOORSTATE_UNLOCKED)			
			bHaveNorthGatesBeenOpenedByPlayer = TRUE
			bHaveSouthGatesBeenOpenedByPlayer = TRUE
			bForceHouseGatesOpenDuringGameplay = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL - bForceHouseGatesOpenDuringGameplay set TRUE") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages the players actions around the pool area
PROC MANAGE_POOL_AREA_STATE()
	INT i
	TEXT_LABEL_23 tlTempRoot
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sPoolPed[i].pedIndex, sPoolPed[i].iFrameCountLastSeenPlayer)
	ENDFOR
	IF ePoolAreaState <> N1B_POOL_AREA_STATE_FLEE
		IF HAVE_ANY_POOL_PEDS_DIED_OR_INJURED_STATE()
			SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_FLEE, TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - going to N1B_POOL_AREA_STATE_FLEE") ENDIF #ENDIF
		ENDIF
	ENDIF
	SWITCH ePoolAreaState	
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_UNDETECTED		
			//jump to gardener interrupted if he's made it into the pool area alive
			IF eGardenAreaState = N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA
			AND IS_PED_UNINJURED(sGardenerPed.pedIndex)
			AND IS_PED_IN_POOL_AREA(sGardenerPed.pedIndex, TRUE)					
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES from N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED") ENDIF #ENDIF
				iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()	//used in next state to countdown till cops should be called.
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED)
			ELSE
				UPDATE_CELEB_IN_POOL_INITIAL_POSITION_BASED_OF_PLAYERS_APPROACH()	
				IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
					IF NOT bDoneDialoguePool_InitialExchange
						IF sPoolPed[POOL_PED_CELEB].AI <> N1B_PED_AI_STATE_RELAXED	// wait untill the celeb in the pool has started swimming lengths
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_P0", CONV_PRIORITY_MEDIUM)
								// Come on, get in the hot tub with me! 
								// I'm bored, I think that pill you gave me is wearing off.  
								// I've been watching you do laps for like two hours now.  
								// Okay, baby, just one more. 
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
								bDoneDialoguePool_InitialExchange = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - bDoneDialoguePool_InitialExchange done") ENDIF #ENDIF
							ENDIF
						ENDIF
					ELIF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_STATE_ROUTINE_COMPLETED
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_P0")	//initial dialogue
							sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - set sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DO_FINAL_LENGTH") ENDIF #ENDIF
						ENDIF
					ELIF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_P1", CONV_PRIORITY_MEDIUM)
							// That had to be a four-digit calorie burn. 
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - set sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_POSE") ENDIF #ENDIF
							sPoolPed[POOL_PED_CELEB].AI =  N1B_PED_AI_CELEB_ONLY_SETUP_POSE
						ENDIF
					ELIF NOT bDoneDialoguePool_CelebPoses
						IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_POSE
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_P1")	//go out pool dialogue
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_P2", CONV_PRIORITY_MEDIUM)
									// Really even definition.
									// It's like I'm airbrushed but I'm not.
									// How do you like me now, Cloe?  
									// I'm not kidding, this might be the best I've ever looked.
									// Hurry up, Ty-Di, I'm lonely in here.
									// Okay, I'm coming. Poppa needs some sugar after all that cardio.  
									bDoneDialoguePool_CelebPoses = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELIF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_POSE_COMPLETED
					OR sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_POSE
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_P2")	//pose dialogue
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - set sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB") ENDIF #ENDIF
							sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GOTO_HOT_TUB
						ENDIF
					ELIF NOT bDoneDialoguePool_CelebInTheHotTub
						IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_ENTER_HOT_TUB_ANIM
						OR sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_DANCE_IN_HOT_TUB
						OR sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								IF bPauseDialoguePool_CelebInTheHotTub	// restart convo from last line 
									IF NIG1B_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_P3", tlPausedDialogue_CelebInTheHotTub, CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
										bPauseDialoguePool_CelebInTheHotTub = FALSE
										bDoneDialoguePool_CelebInTheHotTub = TRUE
									ENDIF
								ELSE
									IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_P3", CONV_PRIORITY_MEDIUM)
										// Do you want me to dance for you, baby?  
										// Ooh yeah, show me some of those backing dancer moves.  
										// I can back it up all day.  
										// My glutes feel like granite.  
										// I can't believe how fat I got for a while.  
										// Check it out! I call this one the bikini dropper.
										// You likey, girl whose name I don't remember?
										// You're terrible! It's Kathi.
										// With an I or Y?
										// An I.
										// Trashy, I love it
										// This house is amazing.
										// Divorce, best thing that ever happened to me.
										// The rhythm, baby, you gotta tame it.
										// Are you still dancing professionally?  
										// I'm focusing more on my reality work at the moment.  
										// Ooh, there it is, spank it, bank it.  
										// Woo, yeah, it's getting hot in here!
										// You want me to tell you a secret, baby?
										// That last jet... wasn't from the hot tub.
										// Ew, that's gross!
										// You can't stop the funk. For real.    
										bDoneDialoguePool_CelebInTheHotTub = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT bDoneDialoguePool_CelebInTheHotTubRunOutOfLines
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_P3")	// N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB dialogue
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_IDLE", CONV_PRIORITY_MEDIUM)
								// I can't believe I'm in a hot tub with Cloe Parker's ex!
								// Ssh. Just enjoy me. Feel the chemistry. Save the talk for the cocaine later.  
								iTimer_PoolStateTo_CelebLookingForClothes = GET_GAME_TIMER()
								bDoneDialoguePool_CelebInTheHotTubRunOutOfLines = TRUE
							ENDIF							
						ENDIF						
					ELSE
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_IDLE")
							//switch pool state to N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES after period of time, if the player is hanging around after getting the clothes
							//bDoSwitch_PoolStateTo_CelebLookingForClothes and iTimer_PoolStateTo_CelebLookingForClothes are set in STAGE_STEAL_CELEB_POSSESSIONS
							//when the player gets the clothes
							IF bDoSwitch_PoolStateTo_CelebLookingForClothes
								IF HAS_TIME_PASSED(iTimer_PoolStateTo_CelebLookingForClothes, NIGEL1B_TIMER_TILL_CELEB_LEAVES_POOL)
									//IF SET_CELEB_TO_STOP_DANCING_FOR_CLOTHES_SEARCH()
									//	SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES, FALSE)
									//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - going to N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES") ENDIF #ENDIF
									//	EXIT
									//ENDIF
								ENDIF
							ENDIF
						ELSE
							iTimer_PoolStateTo_CelebLookingForClothes = GET_GAME_TIMER()
						ENDIF
					ENDIF			
				ELSE
					// stop the pool area dialogue if the player leaves
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
						tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						IF ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_P0")	// bDoneDialoguePool_InitialExchange
						OR ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_P1")	// N1B_PED_AI_CELEB_ONLY_STATE_LEFT_POOL_COMPLETED
						OR ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_P2")	// N1B_PED_AI_CELEB_ONLY_STATE_POSE
						OR ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_P3")	// N1B_PED_AI_CELEB_ONLY_STATE_DANCE_IN_HOT_TUB
						OR ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_IDLE")	// They run out of lines for the dancing idle
							N1B_KILL_CURRENT_CONVERSATION(TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - killed pool convo as player left area") ENDIF #ENDIF
						ELIF ARE_STRINGS_EQUAL(tlTempRoot, "NIG1B_P3")	// bDoneDialoguePool_CelebInTheHotTub
							// Bug fix - could return dialogue as playing in first frame even though the line is invalid, so wait for line to be valid
							TEXT_LABEL_23 tl23CurrentDialogueLine
							tl23CurrentDialogueLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							IF NOT IS_STRING_NULL_OR_EMPTY(tl23CurrentDialogueLine)
								bPauseDialoguePool_CelebInTheHotTub = TRUE
								bDoneDialoguePool_CelebInTheHotTub = FALSE
								tlPausedDialogue_CelebInTheHotTub = tl23CurrentDialogueLine
								N1B_KILL_CURRENT_CONVERSATION(TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - paused hot tub convo as player left area") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
				IF NOT IS_PLAYER_USING_STEALTH_IN_POOL_AREA()
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS DETECTED THE PLAYER") ENDIF #ENDIF
					SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SPOTTED)
				ENDIF
			ENDIF			
			//Go straight to surrendered state if the player has intimidated a pool ped
			IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
			OR IS_PLAYER_INTIMIDATING_POOL_PEDS()
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO SURRENDERED FROM N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SURRENDERED)
			ENDIF			
		BREAK		
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES		
			//jump to gardener interrupted if he's made it into the pool area alive
			IF eGardenAreaState = N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA
			AND IS_PED_UNINJURED(sGardenerPed.pedIndex)
			AND IS_PED_IN_POOL_AREA(sGardenerPed.pedIndex, TRUE)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES from N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED") ENDIF #ENDIF
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED)
			ELSE			
				IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
					//setup celeb leave pool dialogue to trigger
					IF NOT bDoneDialoguePool_CelebGetsOutPool
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Not done get out pool dialogue yet") ENDIF #ENDIF
						N1B_KILL_CURRENT_CONVERSATION(TRUE)
						NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_CPT", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
							// Hey, where are you going?
							// I need a cigarette.  				
							bDoneDialoguePool_CelebGetsOutPool = TRUE
						ENDIF
					ELSE
						//bHasCelebReachedClothesLocation gets set in CONTROL_POOL_PEDS_AI() for the celeb, when he's reached the clothes
						IF bHasCelebReachedClothesLocation
							IF NOT bDoneDialoguePool_CelebSeesClothesAreMissing
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Not done clothes are missing dialogue yet") ENDIF #ENDIF
								N1B_KILL_CURRENT_CONVERSATION(FALSE)
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_CSC", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// Hey, where the fuck did my clothes go?
									// Weird, they must be around here somewhere.
									bDoneDialoguePool_CelebSeesClothesAreMissing = TRUE
								ENDIF
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
									IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_GET_OUT_POOL_FOR_CLOTHES
									OR sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_GET_OUT_POOL_FOR_CLOTHES
										sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE		-	Finished WHERE are clothes dialogue going to N1B_PED_AI_CELEB_ONLY_SETUP_SEARCH_FOR_CLOTHES ID =", i) ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF					
					ENDIF									
					IF NOT IS_PLAYER_USING_STEALTH_IN_POOL_AREA()			
						SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SPOTTED)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS DETECTED THE PLAYER") ENDIF #ENDIF
					ENDIF
				ENDIF	
				//Go straight to surrendered state if the player has intimidated a pool ped
				IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
				OR IS_PLAYER_INTIMIDATING_POOL_PEDS()					
					SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SURRENDERED)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO SURRENDERED FROM N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF	
				ENDIF
			ENDIF	
		BREAK
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
			//to get here eGardenAreaState = N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA and
			//the gardener had to be in the pool area.		
			IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA				
				IF NOT bDoneDialogue_PlayerGrabsClothes	//don't do any of this dialogue if the player has just been seen stealing the clothes					
					//Play the player seen stealing the clothes dialogue (if seen doing so at this point)
					//this can override the gardener informing the pool peds dialogue						
					IF NOT HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 2000)		//this gets set when the clothes pickup has been collected
						IF HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 500)		//half second delay so it's not instant also doesn't conflict with restore convo 
							N1B_KILL_CURRENT_CONVERSATION(FALSE)
							//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_03")	// Leave the area.~s~		//*commented out for new display message check with subtitle profile settings
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PUP", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// Hey, he just took my clothes!
								bDoneDialogue_PlayerGrabsClothes = TRUE
								iTimer_TimeWindowForSpottedStealingClothesDialogue = 0
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED - player seen stealing clothes dialogue") ENDIF #ENDIF
							ENDIF
						ENDIF
					ELSE						
						//Dialogue exchange where gardener informs pool peds that player is on property
						IF eGardenAreaState = N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA
						AND IS_PED_UNINJURED(sGardenerPed.pedIndex)
						AND IS_PED_IN_POOL_AREA(sGardenerPed.pedIndex, TRUE)
							IF NOT bDoneDialogueGardener_InformsPoolPeds
								N1B_KILL_CURRENT_CONVERSATION(FALSE)
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GIC", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// Senor Dixon! Senor Dixon!  
									// We have intruder!  
									iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()	//Reset this time used in next state to countdown till cops should be called.
									bDoneDialogueGardener_InformsPoolPeds = TRUE
								ENDIF
							ELIF NOT bDoneDialoguePool_Spotted
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
									bDoneDialoguePool_Spotted = NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SPO", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									//What the fuck are you doing here?
									bDoneDialoguePool_Spotted = TRUE
								ENDIF
							ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CALL_COPS)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO CALL COPS, from N1B_PLAYER_LOCATION_POOL_AREA finished gardener telling them") ENDIF #ENDIF
								EXIT
							ENDIF
						ELSE
							//Something else went wrong..gardener is no longer in the pool area?
							SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SPOTTED, FALSE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED going to N1B_POOL_AREA_STATE_SPOTTED gardener no longer in the pool area") ENDIF #ENDIF
							EXIT
						ENDIF	
					ENDIF
				ELSE
					//don't attack until the spotted stealing clothes dialgoue has ended
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
					AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
						SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CALL_COPS)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO CALL COPS, from N1B_PLAYER_LOCATION_POOL_AREA finished spotted stealing clothes whilst gardener was deliverying his lines to them") ENDIF #ENDIF
						EXIT
					ENDIF
				ENDIF					
			ELSE	//if player isn't in the pool area						
				IF (GET_GAME_TIMER() - iTimer_PoolPedsTo_CallCops > NIGEL1B_TIME_DELAY_FOR_POOL_PEDS_TO_CALL_COPS)	//Count doen until call the cops
					SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CALL_COPS)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO CALL COPS, from NOT N1B_PLAYER_LOCATION_POOL_AREA faked gardener telling them") ENDIF #ENDIF
					bDoneDialogueGardener_InformsPoolPeds = TRUE	//stop this dialogue from triggering
					EXIT
				ENDIF					
			ENDIF		
			//Go straight to surrendered state if the player has intimidated a pool ped
			IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
			OR IS_PLAYER_INTIMIDATING_POOL_PEDS()
			OR HAS_PLAYER_STARTED_FIGHT_WITH_POOL_PEDS()
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SURRENDERED)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO SURRENDERED FROM N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF	
				EXIT
			ENDIF						
		BREAK		
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_SPOTTED		
			//Play the initially spotted the player dialogue, changes depending who spotted the player			
			IF NOT bDoneDialoguePool_InitiallySpotted
				IF bDoneDialoguePool_Spotted	// possible for gardener being in pool area already triggering this
					bDoneDialoguePool_InitiallySpotted = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SPOTTED skipped already done bDoneDialoguePool_Spotted dialogue from gardener") ENDIF #ENDIF
				ELSE
					IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							N1B_KILL_CURRENT_CONVERSATION(FALSE)
						ELSE				
							STRING sIndividualDialogueLineToPlay						
							IF iPedID_PoolPedWhoDetectedPlayer = POOL_PED_FEMALE
								sIndividualDialogueLineToPlay = "NIG1B_SPI_2"
								// Do you know that guy?
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SPOTTED, POOL_PED_FEMALE to say initial dialogue line ") ENDIF #ENDIF
							ELSE
								sIndividualDialogueLineToPlay = "NIG1B_SPI_1"
								// Hey, who the fuck are you?
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SPOTTED, POOL_PED_CELEB to say initial dialogue line ") ENDIF #ENDIF
							ENDIF						
							IF NIG1B_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SPI", sIndividualDialogueLineToPlay, CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								bDoneDialoguePool_InitiallySpotted = TRUE
							ENDIF						
						ENDIF
					ENDIF		
				ENDIF
			ELSE
				REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_START)
				IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA						
					//Play the player seen stealing the clothes dialogue (if seen doing so)
					IF NOT HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 2000)
						IF NOT bDoneDialogue_PlayerGrabsClothes
							IF HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 500)		//half second delay so it's not instant also doesn't conflict with restore convo 
								N1B_KILL_CURRENT_CONVERSATION(FALSE)
								//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_03")	// Leave the area.~s~		//*commented out for new display message check with subtitle profile settings
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PUP", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// Hey, he just took my clothes!
									bDoneDialogue_PlayerGrabsClothes = TRUE
									iTimer_TimeWindowForSpottedStealingClothesDialogue = 0
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD OF DONE THE SPOTTED STEALING CLOTHES DIALOGUE") ENDIF #ENDIF																	
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bDoneDialoguePool_Spotted
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								bDoneDialoguePool_Spotted = NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_SPO", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// What do you think you're doing? This is private property.  
								// Just pretend I'm not here, kids. 
								// He's crazy. I'm calling the cops.
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SPOTTED, done spotted dialogue") ENDIF #ENDIF
							ENDIF
						ELSE
							//start call cops, warning dialogue / seen stealing clothes dialogue has finished
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
								SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CALL_COPS)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO N1B_POOL_AREA_STATE_CALL_COPS, FINISHED SPOTTED DIALOGUE") ENDIF #ENDIF						
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
					SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_CALL_COPS)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO N1B_POOL_AREA_STATE_CALL_COPS, player left pool area so going straight to call cops") ENDIF #ENDIF		
				ENDIF	
			ENDIF
			//Go straight to surrendered state if the player has intimidated a pool ped
			IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
			OR IS_PLAYER_INTIMIDATING_POOL_PEDS()
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SURRENDERED)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - POOL PEDS TO SURRENDERED FROM N1B_POOL_AREA_STATE_SPOTTED") ENDIF #ENDIF	
				EXIT
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_CALL_COPS
			IF NOT bDoneDialoguePool_CallCops
				IF bCelebOkToStartPhoneConvo
					IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
						N1B_KILL_CURRENT_CONVERSATION(FALSE)
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_CC", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
							// Hello, police?
							// There's a vagrant on my property who won't leave.  
							// That's it, Rockford Hills. Please hurry, I think he's mentally defective.  
							iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
							bDoneDialoguePool_CallCops = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS , bDoneDialoguePool_CallCops done") ENDIF #ENDIF		
						ENDIF						
					ELSE
						IF (GET_GAME_TIMER() - iTimer_PoolPedsTo_CallCops > 4000)	//Count doen until call the cops
							iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
							bDoneDialoguePool_CallCops = TRUE	//add timer here instead
							bDoneDialogue_PlayerMissedCelebCallingCops = FALSE	// player will need another chance to hear that cops have been called
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS , bDoneDialoguePool_CallCops done Player not in area") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT bHavePoolPedsCallTheCops
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_CC")	//call cops convo
						IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_CALL_THE_COPS
						OR sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_CALL_THE_COPS
							sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS set sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_SETUP_END_PHONECALL") ENDIF #ENDIF
						ENDIF
						IF eMissionStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS		// To fix B*922388 if the player has reached the stage in the mission where he's left the area don't apply the wanted level in this way
						OR eMissionStage = MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA						
							IF (GET_GAME_TIMER() - iTimer_PoolPedsTo_CallCops > 500)	//slight delay after phone call
								IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
									bHavePoolPedsCallTheCops = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS player already had a wanted rating so not reapplied") ENDIF #ENDIF
								ELSE
									IF NOT HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
										REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
									ENDIF
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									iDialogueTimer_PoolPedWaitingForCops = GET_GAME_TIMER()
									bHavePoolPedsCallTheCops = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS player given a wanted rating") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
					ENDIF
				ELSE
					// initial comment towards the player if he had missed celeb calling the cops
					IF NOT bDoneDialogue_PlayerMissedCelebCallingCops
						IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_CC3", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// I've called the cops, they're on their way!
								iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
								bDoneDialogue_PlayerMissedCelebCallingCops = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS , bDoneDialogue_PlayerMissedCelebCallingCops done") ENDIF #ENDIF		
							ENDIF
						ENDIF
					ELSE
						// player knocking into the peds
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(tlDialogueRoot_PoolPedsCurrentReactionDialogue)
							IF IS_ENTITY_TOUCHING_ENTITY(sPoolPed[POOL_PED_CELEB].pedIndex, PLAYER_PED_ID())
							AND IS_PED_RAGDOLL(sPoolPed[POOL_PED_CELEB].pedIndex)
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_TBUMP", CONV_PRIORITY_MEDIUM)
									// Random selected lines - 
									// Don't be a dick. 
									// This isn't funny.
									// Don't touch me, man.  
									tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_TBUMP"
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS - triggered NIG1B_TBUMP") ENDIF #ENDIF
								ENDIF
							ELIF IS_ENTITY_TOUCHING_ENTITY(sPoolPed[POOL_PED_FEMALE].pedIndex, PLAYER_PED_ID())
							AND IS_PED_RAGDOLL(sPoolPed[POOL_PED_FEMALE].pedIndex)
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GBUMP", CONV_PRIORITY_MEDIUM)
									// Random selected lines - 
									// Ow, that hurt.  
									// Ty-Di, the hobo pushed me!
									// He tried to feel my tits!  
									tlDialogueRoot_PoolPedsCurrentReactionDialogue = "NIG1B_GBUMP"
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS - triggered NIG1B_GBUMP") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
						// dialogue - additional comments from pool ped if cops are on route
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iDialogueTimer_PoolPedWaitingForCops = GET_GAME_TIMER()
						ELSE
							IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
								IF HAS_TIME_PASSED(iDialogueTimer_PoolPedWaitingForCops, NIGEL1B_TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
									IF GET_RANDOM_INT_IN_RANGE(0, 10)	< 5
										IF IS_PED_UNINJURED(sPoolPed[POOL_PED_CELEB].pedIndex)											
											IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_CC2", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
												// Cops'll be here any minute, dude.  
												// You stay cool, maybe I won't press charges.  
												// Is that sirens I hear? Yep, definitely police sirens. Lots of them.
												// It's not too late to run, man. Let's just end this shit.  
												// The cops will take you back to the asylum, man, is that what you want?  
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS - random waiting for cops dialogue trigger for ID :", POOL_PED_CELEB) ENDIF #ENDIF
												iDialogueTimer_PoolPedWaitingForCops = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ELSE
										IF TRIGGER_ADDITIONAL_AMBIENT_LINES_FOR_TREVOR_HANGING_AROUND()
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS -TRIGGER_ADDITIONAL_AMBIENT_LINES_FOR_TREVOR_HANGING_AROUND") ENDIF #ENDIF
											iDialogueTimer_PoolPedWaitingForCops = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_CELEB_ONLY_STATE_END_PHONECALL	
					sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS set sPoolPed[POOL_PED_CELEB].AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER") ENDIF #ENDIF
				ENDIF
			ENDIF
			//Go straight to surrendered state if the player has intimidated a pool ped
			IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
			OR IS_PLAYER_INTIMIDATING_POOL_PEDS()
			OR HAS_PLAYER_STARTED_FIGHT_WITH_POOL_PEDS()
				IF bDoneDialoguePool_CallCops
				AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_CC")	//call cops convo
					bPlayerStoppedCelebCallingCops = FALSE
				ELSE
					bPlayerStoppedCelebCallingCops = TRUE	// setup alternative dialogue if player interrupts celeb calling the cops
				ENDIF
				SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_SURRENDERED)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_CALL_COPS -POOL PEDS TO SURRENDERED") ENDIF #ENDIF	
				EXIT
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_SURRENDERED
			REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_START)
			IF ePlayerLocation = N1B_PLAYER_LOCATION_POOL_AREA
				IF NOT bDoneDialoguePool_Surrendered
					N1B_KILL_CURRENT_CONVERSATION(FALSE)
					IF bPlayerStoppedCelebCallingCops	// different dialogue if player has interrupted	celeb calling the cops	
						tlDialogueRoot_PoolPedsSurrender = "NIG1B_SCC"
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", tlDialogueRoot_PoolPedsSurrender, CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
							// I wouldn't do that if I were you.
							// Fuck, what is it you want? Just leave us alone, man.
							bDoneDialoguePool_Surrendered = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED - done dialogue - interupted call to cops") ENDIF #ENDIF	
						ENDIF
					ELSE
						//different dialogue based on player weapon					
						IF IS_PLAYER_CURRENT_WEAPON_LETHAL()	//player has lethal weapon which isn't a melee
							tlDialogueRoot_PoolPedsSurrender = "NIG1B_SUR"
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", tlDialogueRoot_PoolPedsSurrender, CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// Don't get any more stupid, or I will blow your botoxed brains out.  
								// Shit, man, just take whatever you want.
								bDoneDialoguePool_Surrendered = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED - done dialogue - lethal weapon") ENDIF #ENDIF	
							ENDIF
						ELSE	//player has non lethal weapon
							tlDialogueRoot_PoolPedsSurrender = "NIG1B_SNL"
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", tlDialogueRoot_PoolPedsSurrender, CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// Any dumb moves, lovebirds, you'll need a lot more cosmetic surgery.  
								// Shit, man, just take whatever you want.
								bDoneDialoguePool_Surrendered = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED - done dialogue - none leathal weapon") ENDIF #ENDIF	
							ENDIF
						ENDIF
					ENDIF				
					iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER() //used for additional surrender dialogue timer
				ELSE
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(tlDialogueRoot_PoolPedsSurrender)
						// another opportunity for clothes stolen dialogue
						IF NOT bDoneDialogue_PlayerGrabsClothes					
							//Play the player seen stealing the clothes dialogue (if seen doing so at this point)
							//this can override the gardener informing the pool peds dialogue		
							IF NOT HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 2000)
								IF HAS_TIME_PASSED(iTimer_TimeWindowForSpottedStealingClothesDialogue, 500)		//half second delay so it's not instant also doesn't conflict with restore convo 
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
										//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_03")	// Leave the area.~s~		//*commented out for new display message check with subtitle profile settings
										IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PUP", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
											// Hey, he just took my clothes!
											iTimer_TimeWindowForSpottedStealingClothesDialogue = 0
											bDoneDialogue_PlayerGrabsClothes = TRUE
											//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED - player seen stealing clothes dialogue") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						MANAGE_ADDITIONAL_SURRENDERED_DIALOGUE()
					ENDIF
				ENDIF				
			ELSE
				// only do this if the pool peds haven't already called the cops
				IF NOT bHavePoolPedsCallTheCops
					//IF ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
						// pool peds to shout "Call the police." if player leaves area when they are surrendered
						IF NOT bDoneDialogue_ShoutCallCopsWhenPlayerLeaves
							IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
								bHavePoolPedsCallTheCops = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED already had a wanted rating so not reapplied") ENDIF #ENDIF
							ELSE
								IF IS_PED_UNINJURED(sPoolPed[POOL_PED_FEMALE].pedIndex)
								AND IS_ENTITY_IN_RANGE_COORDS(sPoolPed[POOL_PED_FEMALE].pedIndex, vPlayerPos, 50) // only do the dialogue if player is in ear shot
									IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_C1", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
										// Now call the police!
										iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
										bDoneDialogue_ShoutCallCopsWhenPlayerLeaves = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED , bDoneDialogue_ShoutCallCopsWhenPlayerLeaves done") ENDIF #ENDIF		
									ENDIF
								ELSE
									iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
									bDoneDialogue_ShoutCallCopsWhenPlayerLeaves = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED , bDoneDialogue_ShoutCallCopsWhenPlayerLeaves skipped as player out of range or ped dead") ENDIF #ENDIF		
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_C1")	//Call the police
								IF eMissionStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS		// To fix B*922388 if the player has reached the stage in the mission where he's left the area don't apply the wanted level in this way
								OR eMissionStage = MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA						
									IF (GET_GAME_TIMER() - iTimer_PoolPedsTo_CallCops > 2000)	//slight delay after phone call
										IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
											bHavePoolPedsCallTheCops = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED player already had a wanted rating so not reapplied") ENDIF #ENDIF
										ELSE
											IF NOT HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
												REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
											ENDIF
											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
											SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())														
											bHavePoolPedsCallTheCops = TRUE
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED player given a wanted rating") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
				// setup the peds to flee if the player leaves them surrendered
				IF ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
					IF HAS_TIME_PASSED(iTimer_DelayPoolPedsFleeWhenSurrendered, 5000)
						SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_FLEE, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED going to N1B_POOL_AREA_STATE_FLEE player left area so flee pedID : ", i) ENDIF #ENDIF
					ENDIF
				ELSE
					iTimer_DelayPoolPedsFleeWhenSurrendered = GET_GAME_TIMER()
				ENDIF
			ENDIF
			// make the peds flee if the player has attacked them whilst they are surrendered
			FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
				//IF IS_PLAYER_MAKING_PED_FRIGHTENED(sPoolPed[i].pedIndex)
				IF IS_ENTITY_ALIVE(sPoolPed[i].pedIndex)
					//IF sPoolPed[i].AI = N1B_PED_AI_STATE_SURRENDERED // potentially needed incase this triggers before they can get up to surrender whilst in N1B_PED_AI_SETUP_SURRENDERED
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sPoolPed[i].pedIndex, PLAYER_PED_ID())
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sPoolPed[i].pedIndex)
							
							N1B_KILL_CURRENT_CONVERSATION(FALSE)
							IF i = POOL_PED_CELEB
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_THIT", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// You crazy bastard!
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - done dialogue NIG1B_THIT") ENDIF #ENDIF		
								ENDIF
								iPoolPedID_PlayerAttackedToFlee = POOL_PED_CELEB
							ELIF i = POOL_PED_FEMALE
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GHIT", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// Aagh, Tyler! Do something!  
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - done dialogue NIG1B_GHIT") ENDIF #ENDIF		
								ENDIF
								iPoolPedID_PlayerAttackedToFlee = POOL_PED_FEMALE
							ENDIF
							SET_POOL_AREA_STATE(N1B_POOL_AREA_STATE_FLEE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_SURRENDERED going to N1B_POOL_AREA_STATE_FLEE player damaged pedID : ", i) ENDIF #ENDIF
							EXIT
						ENDIF
					//ENDIF
				ENDIF
			ENDFOR
		BREAK
		//	------------------------------------------
		CASE N1B_POOL_AREA_STATE_FLEE
			// dialogue - peds are fleeing as a pool ped was killed
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDialogueTimer_PoolPedFlee = GET_GAME_TIMER()
			ELSE
				// say response from Trevor for hitting ped when they were surrendered
				IF iPoolPedID_PlayerAttackedToFlee != -1
					IF iPoolPedID_PlayerAttackedToFlee = POOL_PED_CELEB
						IF VDIST2(GET_ENTITY_COORDS(sPoolPed[POOL_PED_CELEB].pedIndex, FALSE), vPlayerPos) < (20.0 * 20.0)
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_PBU", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// Come on, we're both white trash!  
								// Don't pretend you don't need the publicity!  
								iPoolPedID_PlayerAttackedToFlee = -1
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE - done dialogue - NIG1B_PBU") ENDIF #ENDIF	
							ENDIF
						ELSE
							iPoolPedID_PlayerAttackedToFlee = -1
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE - skipped dialogue out of range - NIG1B_PBU") ENDIF #ENDIF	
						ENDIF
					ELIF iPoolPedID_PlayerAttackedToFlee = POOL_PED_FEMALE
						IF VDIST2(GET_ENTITY_COORDS(sPoolPed[POOL_PED_FEMALE].pedIndex, FALSE), vPlayerPos) < (20.0 * 20.0)
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_BEAT", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// Sorry, blame it on the mommy issues.  
								iPoolPedID_PlayerAttackedToFlee = -1
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE - done dialogue - NIG1B_BEAT") ENDIF #ENDIF	
							ENDIF
						ELSE
							iPoolPedID_PlayerAttackedToFlee = -1
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE - skipped dialogue out of range - NIG1B_PBU") ENDIF #ENDIF	
						ENDIF
					ELSE
						iPoolPedID_PlayerAttackedToFlee = -1
					ENDIF						
				ELIF HAS_TIME_PASSED(iDialogueTimer_PoolPedFlee, NIGEL1B_TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
					INT iSpeakerID
					iSpeakerID = GET_RANDOM_OK_POOL_PED_TO_SPEAK(vPlayerPos, 20.0)
					IF iSpeakerID != -1	// if able to find a suitable speaker (-1 if none was found
					
						TEXT_LABEL_23 tl23Root
						IF iSpeakerID = POOL_PED_CELEB
							tl23Root = "NIG1B_CF1"
							// Fuck this, I'm out of here!
							// Not cool, dude!  
							// What's your problem, man?  
							// Why do I always get the psychos?   
						ELIF iSpeakerID = POOL_PED_FEMALE				
							tl23Root = "NIG1B_CF2"
							// He's a lunatic!  
							// Stay away from me!
							// I just want to go home!  
							// Leave me alone, you freak!
						ENDIF
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", tl23Root, CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE - random flee dialogue trigger for ID :", iSpeakerID) ENDIF #ENDIF
							iDialogueTimer_PoolPedFlee = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			// only do this if the pool peds haven't already called the cops
			IF NOT bHavePoolPedsCallTheCops
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					bHavePoolPedsCallTheCops = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE already had a wanted rating so not reapplied") ENDIF #ENDIF
				ELSE
					// skip the dialogue to call the cops, just apply with a bigger wait time
					IF NOT bDoneDialogue_ShoutCallCopsWhenPlayerLeaves
						iTimer_PoolPedsTo_CallCops = GET_GAME_TIMER()
						bDoneDialogue_ShoutCallCopsWhenPlayerLeaves = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE , bDoneDialogue_ShoutCallCopsWhenPlayerLeaves done") ENDIF #ENDIF		
					ELSE
						IF eMissionStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS		// To fix B*922388 if the player has reached the stage in the mission where he's left the area don't apply the wanted level in this way
						OR eMissionStage = MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA	
							IF HAS_TIME_PASSED(iTimer_PoolPedsTo_CallCops, 8000)
								IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
									bHavePoolPedsCallTheCops = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE player already had a wanted rating so not reapplied") ENDIF #ENDIF
								ELSE
									IF NOT HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
										REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
									ENDIF
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())														
									bHavePoolPedsCallTheCops = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_POOL_AREA_STATE - N1B_POOL_AREA_STATE_FLEE player given a wanted rating") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
		CONTROL_POOL_PEDS_AI(i)
	ENDFOR
ENDPROC

/// PURPOSE:
///    Set the current garden area's state, along with the garden ped's relevant AI state.
/// PARAMS:
///    eNewState - The new state
///    bSetRelevantAIState_ForGardenPed - If TRUE the ped's AI will be updated to the relevant state
PROC SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE eNewState, BOOL bSetRelevantAIState_ForGardenPed = TRUE)
	eGardenAreaState = eNewState
	IF bSetRelevantAIState_ForGardenPed
		SWITCH eGardenAreaState		
			CASE N1B_GARDEN_AREA_STATE_UNDETECTED
				sGardenerPed.AI = N1B_PED_AI_STATE_RELAXED
			BREAK
			CASE N1B_GARDEN_AREA_STATE_SPOTTED	
				sGardenerPed.AI = N1B_PED_AI_SETUP_AWARE_OF_PLAYER
			BREAK
			CASE N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA		
				sGardenerPed.AI = N1B_PED_AI_SETUP_WARN_OTHER_PEDS
			BREAK		
			CASE N1B_GARDEN_AREA_STATE_SURRENDERED
				sGardenerPed.AI = N1B_PED_AI_SETUP_SURRENDERED
				// reset the last damaged entity so we can test for the player hurting him whilst they are surrendered.
				IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sGardenerPed.pedIndex)
				ENDIF
			BREAK
			CASE N1B_GARDEN_AREA_STATE_FLEE
				sGardenerPed.AI = N1B_PED_AI_SETUP_FLEE
			BREAK
			CASE N1B_GARDEN_AREA_STATE_GARDENER_DEAD
				sGardenerPed.AI = N1B_PED_AI_STATE_DEAD	
			BREAK
		ENDSWITCH
	ENDIF	
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_GARDEN_AREA_STATE - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Manages the players actions around the garden area
PROC MANAGE_GARDEN_AREA_STATE()
	IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
		UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sGardenerPed.pedIndex, sGardenerPed.iFrameCountLastSeenPlayer)
		SWITCH eGardenAreaState	
			//	------------------------------------------
			CASE N1B_GARDEN_AREA_STATE_UNDETECTED
				//play the gardener's whistling audio
				IF HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
					IF NOT IS_PED_BEING_STEALTH_KILLED(sGardenerPed.pedIndex)
					AND NOT IS_PED_RAGDOLL(sGardenerPed.pedIndex)
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sGardenerPed.pedIndex)	// stop him whilsting whilst he talks
						PLAY_SOUND_FROM_ENTITY(iSoundID_WhistlingGardener, "WHISTLING", sGardenerPed.pedIndex, "NIGEL_1B_SOUNDSET")
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDEN AREA STATE - set whistling gardener audio active	^^^^^^") ENDIF #ENDIF
					ENDIF
				ELSE
					IF IS_PED_BEING_STEALTH_KILLED(sGardenerPed.pedIndex)
					OR IS_PED_RAGDOLL(sGardenerPed.pedIndex)
					OR IS_PED_IN_CURRENT_CONVERSATION(sGardenerPed.pedIndex)	// stop him whilsting whilst he talks
						IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
							STOP_SOUND(iSoundID_WhistlingGardener)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDEN AREA STATE - set whistling gardener audio STOPPED ped being stealth killed	^^^^^^") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				//check for player intimidating first, as detected would go here eventually anyway
				IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
				OR IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sGardenerPed.pedIndex)
					IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
						STOP_SOUND(iSoundID_WhistlingGardener)
					ENDIF
					// skipped over surrender dialogue if the player has already damaged the gardener to fix B*1094304
					IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID())
							bDoneDialogueGardener_Surrendered = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - player damaged gardener so skipping bDoneDialogueGardener_Surrendered") ENDIF #ENDIF
						ENDIF
					ENDIF
					SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SURRENDERED)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER SURRENDERED") ENDIF #ENDIF
				ELSE
					//don't do the ped seeing checks if he's at the start where he's looking in the back of the truck, also override range for on foot approach
					IF sGardenerPed.AI = N1B_PED_AI_STATE_RELAXED
						IF IS_PLAYER_DRIVING_VEHICLE_IN_AREA_OF_PROPERTY(N1B_PLAYER_LOCATION_GARDEN_AREA)
						OR HAS_GARDENER_PED_DETECTED_THE_PLAYER(FALSE, sGardenerPed.iFrameCountLastSeenPlayer, FALSE, FALSE) //stop gardener detecting player running close / walking whilst he has his head in the van.
						AND ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
						OR HAS_PLAYER_POTENTIALLY_KNOCKED_VAN_DOOR_INTO_GARDENER()
							IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
								STOP_SOUND(iSoundID_WhistlingGardener)
							ENDIF	
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SPOTTED)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER DETECTED THE PLAYER whilst N1B_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						// these checks are normall done as part of HAS_GARDENER_PED_DETECTED_THE_PLAYER, however they need to be custom for this intial setup to allow the player to get into the property
						ELIF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_RUNNING OR ePlayerMovementState = N1B_PEDMOVEMENTSTATE_SPRINTING
							IF IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, 4.0)
								IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
									STOP_SOUND(iSoundID_WhistlingGardener)
								ENDIF	
								SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SPOTTED)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER DETECTED THE PLAYER whilst N1B_PED_AI_STATE_RELAXED ran or sprint closeby") ENDIF #ENDIF
							ENDIF
						ELIF ePlayerMovementState = N1B_PEDMOVEMENTSTATE_WALKING
							IF IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, 2.5)
								IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
									STOP_SOUND(iSoundID_WhistlingGardener)
								ENDIF	
								SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SPOTTED)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER DETECTED THE PLAYER whilst N1B_PED_AI_STATE_RELAXED walked closeby") ENDIF #ENDIF
							ENDIF
						// B*1489377
						ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID())
							IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
								STOP_SOUND(iSoundID_WhistlingGardener)
							ENDIF	
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SPOTTED)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - player damaged gardener whilst N1B_PED_AI_STATE_RELAXED") ENDIF #ENDIF
						ELSE
							//gardener in back of the van dialogue
							IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
									IF HAS_TIME_PASSED(iTimer_DelayGardenerSearchingVanDialogue, GET_RANDOM_INT_IN_RANGE(5000, 12000))
										IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GBV", CONV_PRIORITY_MEDIUM)										
											// Donde esta?  
											// Se que esta por aqui.  
											// No puede ser.  
											// El jefe me va a matar
											IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
												STOP_SOUND(iSoundID_WhistlingGardener)
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GARDEN AREA STATE - stopped whistling sound for dialogue") ENDIF #ENDIF
											ENDIF
											iTimer_DelayGardenerSearchingVanDialogue = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELSE
									iTimer_DelayGardenerSearchingVanDialogue = GET_GAME_TIMER()
								ENDIF
							ELSE
								iTimer_DelayGardenerSearchingVanDialogue = GET_GAME_TIMER()
							ENDIF							
						ENDIF
					ELSE
						// to be removed since, gardener is now to stay still at the van
						IF HAS_GARDENER_PED_DETECTED_THE_PLAYER(TRUE, sGardenerPed.iFrameCountLastSeenPlayer, TRUE, TRUE)
						OR IS_PLAYER_DRIVING_VEHICLE_IN_AREA_OF_PROPERTY(N1B_PLAYER_LOCATION_GARDEN_AREA)
							IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
								STOP_SOUND(iSoundID_WhistlingGardener)
							ENDIF	
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SPOTTED)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER DETECTED THE PLAYER") ENDIF #ENDIF
						ENDIF
					ENDIF		
				ENDIF
			BREAK			
			//	------------------------------------------
			CASE N1B_GARDEN_AREA_STATE_SPOTTED			
				IF NOT bDoneDialogueGardener_Spotted
					N1B_KILL_CURRENT_CONVERSATION(FALSE)
					IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
						IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GSP", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
							// Hey, you cannot be here!  
							// I go tell the boss, he won't be happy!
							iTimer_GardenAreaState = GET_GAME_TIMER()
							bDoneDialogueGardener_Spotted = TRUE
						ENDIF
					ELSE
						bDoneDialogueGardener_Spotted = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - bDoneDialogueGardener_Spotted - skipped as player was out of range") ENDIF #ENDIF
					ENDIF
				ELSE
					REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_START)
					IF ePoolAreaState = N1B_POOL_AREA_STATE_UNDETECTED	// let gardener start going around to tell the celeb, if we are NOT plannning on using the alternative dialogue
					OR ePoolAreaState = N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_GSP")
						OR HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(sGardenerPed.pedIndex)
						OR HAS_TIME_PASSED(iTimer_GardenAreaState, 1250)
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDEN STATE SWITCHED TO N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA") ENDIF #ENDIF
						ENDIF
					ELSE
						IF NOT bDoneDialogueGardener_PlayerSaysGetLost
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_GSP")
								IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
									IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GSP2", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
										// I've already introduced myself.
										// He's over by the pool, shitting himself.
										// So I guess you could either go join him or fuck off.
										bDoneDialogueGardener_PlayerSaysGetLost = TRUE
									ENDIF
								ELSE
									bDoneDialogueGardener_PlayerSaysGetLost = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - bDoneDialogueGardener_PlayerSaysGetLost - skipped as player was out of range") ENDIF #ENDIF
								ENDIF
							ELSE
								IF ePlayerLocation != N1B_PLAYER_LOCATION_GARDEN_AREA
									N1B_KILL_CURRENT_CONVERSATION(TRUE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - bDoneDialogueGardener_Spotted - dialogue killed early for player leaving area") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("NIG1B_GSP2")	// bDoneDialogueGardener_PlayerSaysGetLost
								SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_FLEE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDEN STATE SWITCHED TO N1B_GARDEN_AREA_STATE_FLEE") ENDIF #ENDIF
							ELSE
								IF ePlayerLocation != N1B_PLAYER_LOCATION_GARDEN_AREA
									N1B_KILL_CURRENT_CONVERSATION(TRUE)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - bDoneDialogueGardener_PlayerSaysGetLost - dialogue killed early for player leaving area") ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF						
				ENDIF
				IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
				OR IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sGardenerPed.pedIndex)
					SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SURRENDERED)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER SURRENDERED") ENDIF #ENDIF
				ENDIF				
			BREAK			
			//	------------------------------------------
			CASE N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA	
				// switch to watching the player when celeb goes to ring the cops
				IF sGardenerPed.AI = N1B_PED_AI_SETUP_WARN_OTHER_PEDS
				OR sGardenerPed.AI = N1B_PED_AI_STATE_WARN_OTHER_PEDS
					IF bHavePoolPedsCallTheCops
					OR ePoolAreaState = N1B_POOL_AREA_STATE_CALL_COPS
						sGardenerPed.AI = N1B_PED_AI_SETUP_JUST_FACE_PLAYER
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER switched to N1B_PED_AI_SETUP_JUST_FACE_PLAYER state after pool peds called the cops") ENDIF #ENDIF
					ENDIF
				ENDIF
				IF ePoolAreaState = N1B_POOL_AREA_STATE_SURRENDERED
				OR ePoolAreaState = N1B_POOL_AREA_STATE_FLEE
					IF IS_PED_IN_POOL_AREA(sGardenerPed.pedIndex, FALSE)
						SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SURRENDERED)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER SURRENDERED") ENDIF #ENDIF
					ENDIF					
				ENDIF
				IF HAS_PLAYER_STARTED_FIGHT_WITH_SPECIFIC_PED(sGardenerPed.pedIndex)
					IF bDoneDialogueGardener_InformsPoolPeds	// only make him flee once he's informed the pool peds
						IF NOT IS_SPECIFIC_CONVERSATION_LABEL_CURRENTLY_PLAYING("NIG1B_GIC")	// bDoneDialogueGardener_InformsPoolPeds
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_FLEE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA -> N1B_GARDEN_AREA_STATE_FLEE") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PLAYER_FIRING_LETHAL_WEAPON_ON_PROPERTY()
				OR IS_PLAYER_USING_WEAPON_TO_INTIMIDATE_SPECIFIC_PED(sGardenerPed.pedIndex)
					SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_SURRENDERED)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - GARDENER SURRENDERED") ENDIF #ENDIF
				ENDIF				
			BREAK					
			//	------------------------------------------
			CASE N1B_GARDEN_AREA_STATE_SURRENDERED		
				REQUEST_TRIGGER_MISSION_MUSIC_EVENT(NIGEL1B_MUSIC_EVENT_START)
				IF NOT bDoneDialogueGardener_Surrendered
					//don't play his surrender dialogue if, he's in the pool area and the pool peds are surrendered because there dialogue will play instead
					IF IS_PED_IN_POOL_AREA(sGardenerPed.pedIndex, FALSE)
					AND ePoolAreaState = N1B_POOL_AREA_STATE_SURRENDERED
					OR ePoolAreaState = N1B_POOL_AREA_STATE_FLEE
					// also skipped over if the player has already damaged the gardener to fix B*1094304
						bDoneDialogueGardener_Surrendered = TRUE
					ELSE
						IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA	
						OR IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, 5.0)
							N1B_KILL_CURRENT_CONVERSATION(FALSE)
							IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GSU", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
								// I hope you're not going to ruin this surprise visit.  
								// No dispare! Por favor! No dispare!  
								iTimer_DelayAdditionalSurrenderedDialogue = GET_GAME_TIMER() //used for additional surrender dialogue timer												
								bDoneDialogueGardener_Surrendered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					
				ENDIF
				// make the ped flee if the player has attacked them whilst they are surrendered
				//IF IS_PLAYER_MAKING_PED_FRIGHTENED(sGardenerPed.pedIndex)
				IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)
					//IF sGardenerPed.AI = N1B_PED_AI_STATE_SURRENDERED // potentially needed incase this triggers before they can get up to surrender whilst in N1B_PED_AI_SETUP_SURRENDERED
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID())
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sGardenerPed.pedIndex)
							SET_GARDEN_AREA_STATE(N1B_GARDEN_AREA_STATE_FLEE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - N1B_GARDEN_AREA_STATE_SURRENDERED going to N1B_GARDEN_AREA_STATE_FLEE player damaged gardener ") ENDIF #ENDIF
						ENDIF
					//ENDIF
				ENDIF
			BREAK
			//	------------------------------------------
			CASE N1B_GARDEN_AREA_STATE_FLEE
				IF NOT bDoneDialogueGardener_Flee
					IF IS_PED_IN_HOUSE_AREA(sGardenerPed.pedIndex, FALSE)
						IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA	
						OR CAN_PED_SEE_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GF", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
									// Este gringo esta loco!  
									bDoneDialogueGardener_Flee = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		CONTROL_GARDENERS_AI()	
		IF sGardenerPed.AI = N1B_PED_AI_SETUP_FLEE
		OR sGardenerPed.AI = N1B_PED_AI_STATE_FLEE
			CLEANUP_AI_PED_BLIP(sGardenerPed.blipAIStruct)
		ELSE
			// once clothes have been stolen, unblip the peds when the player moves away if he was undetected
			IF eMissionStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS
				UPDATE_AI_PED_BLIP(sGardenerPed.pedIndex, sGardenerPed.blipAIStruct)
			ELSE
				IF NOT bPlayerWasSpottedByGardener
					IF ePlayerLocation = N1B_PLAYER_LOCATION_OUTSIDE_HOUSE_AREA
					AND NOT IS_ENTITY_IN_RANGE_COORDS(sGardenerPed.pedIndex, vPlayerPos, 35.0)
						// cleanup the blips if the player is a distance away AND wasn't detected by the gardener
						CLEANUP_AI_PED_BLIP(sGardenerPed.blipAIStruct)
					ELSE
						UPDATE_AI_PED_BLIP(sGardenerPed.pedIndex, sGardenerPed.blipAIStruct)
					ENDIF				
				ELSE
					UPDATE_AI_PED_BLIP(sGardenerPed.pedIndex, sGardenerPed.blipAIStruct)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
		AND IS_ENTITY_ATTACHED_TO_ANY_PED(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
			DETACH_ENTITY(moGardnerVanProps[NIGEL1B_PROP_GARDENER_LEAF_BLOWER].object)
		ENDIF
		IF NOT HAS_SOUND_FINISHED(iSoundID_WhistlingGardener)
			STOP_SOUND(iSoundID_WhistlingGardener)
		ENDIF	
		IF NOT bDoneDialogueGarden_TrevorKillsGardener			
			IF ePlayerLocation = N1B_PLAYER_LOCATION_GARDEN_AREA
			AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())	// fix bug 1050318 - player shouldn't talk when in stealth
				IF eGardenAreaState = N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA	//Only play this dialogue if the gardener had spotted the player dialogue has played and he's off to tell the pool peds
				OR bDoneDialogueGardener_Spotted	//or gardener had said he would tell the celeb
					IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_GK", CONV_PRIORITY_MEDIUM) //, DO_NOT_DISPLAY_SUBTITLES)
						// Nobody likes a tell-tale.   
						bDoneDialogueGarden_TrevorKillsGardener = TRUE
					ENDIF
				ELSE
					bDoneDialogueGarden_TrevorKillsGardener = TRUE
				ENDIF
			ELSE
				bDoneDialogueGarden_TrevorKillsGardener = TRUE
			ENDIF
		ENDIF
		IF eGardenAreaState != N1B_GARDEN_AREA_STATE_GARDENER_DEAD
			IF DOES_ENTITY_EXIST(sGardenerPed.pedIndex)
				IF WAS_PED_KILLED_BY_STEALTH(sGardenerPed.pedIndex)	// some reason stealth kills fail the following two checks
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)
					REPLAY_RECORD_BACK_FOR_TIME(2.5, 2.0, REPLAY_IMPORTANCE_LOWEST)	// B*1880376 - record gardener knockout
					CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - set stealth kill check - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)")
				ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sGardenerPed.pedIndex, PLAYER_PED_ID(), TRUE)
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)
					REPLAY_RECORD_BACK_FOR_TIME(2.5, 2.0, REPLAY_IMPORTANCE_LOWEST)	// B*1880376 - record gardener knockout
					CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - set entity damaged by entity - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)")
				ELSE
					// second chance test source of death (this doesn't work if you cause van to explode which causes ped to die
					ENTITY_INDEX entityDeathSource = GET_PED_SOURCE_OF_DEATH(sGardenerPed.pedIndex)
					IF DOES_ENTITY_EXIST(entityDeathSource)
						IF IS_ENTITY_A_PED(entityDeathSource)
							IF GET_PED_INDEX_FROM_ENTITY_INDEX(entityDeathSource) = PLAYER_PED_ID()
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)
								REPLAY_RECORD_BACK_FOR_TIME(2.5, 2.0, REPLAY_IMPORTANCE_LOWEST)	// B*1880376 - record gardener knockout
								CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - set source of death - INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_GARDENER_TAKEN_OUT)")
							ELSE
								CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - dead! source of death a ped but not player")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - dead! source of death not a ped")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - dead! source of death doesn't exist")
					ENDIF
				ENDIF
			ENDIF
			IF eGardenAreaState != N1B_GARDEN_AREA_STATE_UNDETECTED	// set flag for player having been spotted
				bPlayerWasSpottedByGardener = TRUE
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - bPlayerWasSpottedByGardener = TRUE") ENDIF #ENDIF
			ENDIF
			sGardenerPed.AI = N1B_PED_AI_STATE_DEAD
			CLEANUP_AI_PED_BLIP(sGardenerPed.blipAIStruct)
			eGardenAreaState = N1B_GARDEN_AREA_STATE_GARDENER_DEAD	
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_GARDEN_AREA_STATE - eGardenAreaState set N1B_GARDEN_AREA_STATE_GARDENER_DEAD") ENDIF #ENDIF
		ENDIF
		//remove gardener from dialogue if he's dead / injured
		//REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	// "GARDENER"
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles call to MISSION_CLEANUP and terminates the thread
PROC Script_Cleanup()
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MISSION_CLEANUP()	
	ENDIF
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)	
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Mission Passed
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Script_Passed")
	SAFE_FADE_SCREEN_IN_FROM_BLACK()
	Random_Character_Passed(CP_RAND_C_NIG1B)
	Script_Cleanup()		
ENDPROC

/// PURPOSE:
///    updates the reason for mission failed
///    in order of most important fail reason so if multiple fails conditions have been set, we use the most important
PROC UPDATE_FAIL_REASON()
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has failed
PROC MISSION_FAILED_CHECKS()
	
	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED
			UPDATE_FAIL_REASON()
			IF eN1B_MissionFailedReason <> FAILED_DEFAULT
				SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
 
 /// PURPOSE:
 ///    Jumps the mission to the next stage in the script
PROC SKIP_STAGE()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		//implement switch statement based of eMissionStage
		//which takes care of cleanup and setup for next stage.
		N1B_KILL_CURRENT_CONVERSATION(TRUE)
		CLEAR_PRINTS()	
		SWITCH eMissionStage		
			//	------------------------------------------
			CASE MISSION_STAGE_STEAL_CELEB_POSSESSIONS
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe guard that the SS_SETUP has already had chance to run.		
					SAFE_REMOVE_BLIP(mpClothesPickup.blipIndex)
					SAFE_REMOVE_PICKUP(mpClothesPickup.index)		
					IF Is_Replay_In_Progress()		// setup if it's a replay with next stage beign the checkpoint
					AND GET_REPLAY_MID_MISSION_STAGE() = CP_NIGEL1B_ITEMS_STOLEN_LEAVE_THE_AREA
						SET_GARDEN_AREA_STATE(INT_TO_ENUM(N1B_GARDEN_AREA_STATE, g_replay.iReplayInt[NIGEL1B_REPLAY_GARDENER_STATE]))
						SET_POOL_AREA_STATE(INT_TO_ENUM(N1B_POOL_AREA_STATE, g_replay.iReplayInt[NIGEL1B_REPLAY_POOL_PED_STATE]))
						IF eGardenAreaState	= N1B_GARDEN_AREA_STATE_INFORM_POOL_AREA
						AND ePoolAreaState = N1B_POOL_AREA_STATE_GARDENER_INTERRUPTED
							//teleport gardener to the pool area as he must of reached it in the previous playthrough
							SAFE_TELEPORT_PED(sGardenerPed.pedIndex, << -1050.3589, 354.0594, 68.9132 >>, 38.0338)
							sGardenerPed.AI = N1B_PED_AI_SETUP_WARN_OTHER_PEDS
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STEAL_CELEB_POSSESSIONS : ", "* Replay checkpoint * - ",
																									"set gardener in pool area informing peds", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELIF eGardenAreaState = N1B_GARDEN_AREA_STATE_GARDENER_DEAD
							sGardenerPed.AI = N1B_PED_AI_STATE_DEAD
							SAFE_REMOVE_PED(sGardenerPed.pedIndex, TRUE)	// don't respawn dead gardener, just delete gardener if he was killed on previous playthrough
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STEAL_CELEB_POSSESSIONS : ", "* Replay checkpoint * - ",
																										"gardener set dead", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -1045.0170, 367.3012, 68.9128 >>, 103.7010)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STEAL_CELEB_POSSESSIONS : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF ePoolAreaState = N1B_POOL_AREA_STATE_UNDETECTED		// setup the timer till we switch pool states to N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
						bDoSwitch_PoolStateTo_CelebLookingForClothes = TRUE
						iTimer_PoolStateTo_CelebLookingForClothes = GET_GAME_TIMER()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STEAL_CELEB_POSSESSIONS : ", "setup timer till celeb looks for clothes", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					RESET_MISSION_PED_TIMERS()
					eSubStage = SS_CLEANUP					
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STEAL_CELEB_POSSESSIONS : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			//	------------------------------------------
			CASE MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					RESET_MISSION_PED_TIMERS()
					IF NOT IS_REPLAY_BEING_SET_UP()
						SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -462.2575, -156.7884, 37.0458 >>, 112.5754, FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						WAIT(0)
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			//	------------------------------------------
			CASE MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					RESET_MISSION_PED_TIMERS()
					N1B_KILL_CURRENT_CONVERSATION(FALSE)
					bDoneDialogue_ItemsStolenPlayerLeftTheArea = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			//	------------------------------------------
			CASE MISSION_STAGE_OUTRO_PHONECALL
				IF IS_PHONE_ONSCREEN(FALSE)
					HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
				ENDIF
				IF NOT bHasOutroPhonecallSucceeded
					eSubStage = SS_CLEANUP 						
				ELSE
					//needed to stop the outgoing phonecalls!!!
					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						N1B_KILL_CURRENT_CONVERSATION(FALSE, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_OUTRO_PHONECALL : ", " killing ongoing convo", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						WAIT(0)
					ENDWHILE					
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_OUTRO_PHONECALL : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			BREAK		
			//	------------------------------------------
			CASE MISSION_STAGE_LOSE_THE_COPS
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_LOSE_THE_COPS : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK			
			DEFAULT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
/// PARAMS:
///    eNewStage - stage to skip to
PROC JUMP_TO_STAGE(MISSION_STAGE eNewStage)
	// skip current stage
    IF eMissionStage = eNewStage 
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP(NULL, DEFAULT, FALSE)
		ENDIF
		RC_END_Z_SKIP()
    	bFinishedStageSkipping = TRUE
		bLoadedWorldForStageSkipping = FALSE
		// ensure we are fully faded in if we have skipped to the mission passed stage, since the mission passed GUI doesn't display if not (seems to need a frame wait too)
		IF eMissionStage = MISSION_STAGE_MISSION_PASSED
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
    ELSE
        SKIP_STAGE() 
    ENDIF
ENDPROC

/// PURPOSE: 
///     Reset the mission, cleanups the current state and set's the mission up again
///     USED by the mission replay checkpoint setup and Debug skips
PROC RESET_MISSION()
	MISSION_CLEANUP(TRUE, FALSE)
	WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - killing music cues") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		IF NOT IS_REPLAY_BEING_SET_UP()
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart, TRUE, TRUE)	//setup player position for restart mission
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
	ENDIF
	SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
	SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		N1B_KILL_CURRENT_CONVERSATION(FALSE, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - ATTEMPTTING TO KILL OUTRO PHONECALL FOR SKIP") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	NIG1B_STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(tSavedConversationRoot, tSavedConversationLabel)
	WAIT(0)	// added one frame wait so doors being registered in INIT_MISSION() aren't the same frame as MISSION_CLEANUP()
	
	/*//set the initial scene back up
	eInitialSceneStage = IS_REQUEST_SCENE
	WHILE NOT SetupScene_NIGEL_1B(sRCLauncherDataLocal)	
		CPRINTLN(DEBUG_MISSION, " RESET_MISSION - waiting on SetupScene_NIGEL_1B")
		WAIT(0)
	ENDWHILE
	RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	SETUP_AREA_FOR_MISSION(RC_NIGEL_1B, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
	*/
	//re do mission initialization	
	INIT_MISSION()
	SET_STAGE(MISSION_STAGE_STEAL_CELEB_POSSESSIONS)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Perform a Z skip.  Used by the mission checkpoints and the debug Z skip function
/// PARAMS:
///    iNewStage - Mission stage we want to skip to
///    bResetMission - used when we go backwards in mission flow.  If false we also don't stop the active cutscene in RC_START_Z_SKIP, instead handled in SKIP_STAGE to fix bug 1006740
PROC DO_Z_SKIP(INT iNewStage, BOOL bResetMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP with parameters  - iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	RC_START_Z_SKIP(bResetMission)
	IF bResetMission
		RESET_MISSION()
	ENDIF
	eMissionSkipTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
	bFinishedStageSkipping = FALSE
	IF IS_REPLAY_BEING_SET_UP()
		bLoadedWorldForStageSkipping = TRUE
	ELSE
		bLoadedWorldForStageSkipping = FALSE
	ENDIF
	// load world for the mission start area if we are resetting the mission to the intro mocap.  Moved here from script skip stage to fix bug 1006740 - mocap exit states not getting set as game is waiting on world to load before getting to check				
	// basically if you press CROSS to confirm which stage in the z menu, it skipped the mocap but couldn't sent exit states as it was waiting for world to load first.					
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF eMissionSkipTargetStage = MISSION_STAGE_STEAL_CELEB_POSSESSIONS
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart, TRUE, TRUE)	//setup player position for restart mission
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				bLoadedWorldForStageSkipping = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP -  LOADED WORLD ready at mission start area framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	JUMP_TO_STAGE(eMissionSkipTargetStage)
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	DEBUG - J,P and Z skip stuff
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
	
	    INT iNewStage
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))	// Check for Pass
			WAIT_FOR_CUTSCENE_TO_STOP()	
			IF IS_PED_UNINJURED(sGardenerPed.pedIndex)
				IF IS_PED_IN_MELEE_COMBAT(sGardenerPed.pedIndex)	// clear ped tasks doesn't work ons a ped in combat :(
					CLEAR_PED_TASKS_IMMEDIATELY(sGardenerPed.pedIndex)
				ELSE
					CLEAR_PED_TASKS(sGardenerPed.pedIndex)
				ENDIF
				SET_PED_KEEP_TASK(sGardenerPed.pedIndex, FALSE)
				sGardenerPed.AI = N1B_PED_AI_STATE_RELAXED
			ENDIF
			INT i
			FOR i = 0 TO (TOTAL_POOL_PEDS - 1)
				IF IS_PED_UNINJURED(sPoolPed[i].pedIndex)
					IF IS_PED_IN_MELEE_COMBAT(sPoolPed[i].pedIndex)	// clear ped tasks doesn't work on a ped in combat :(
						CLEAR_PED_TASKS_IMMEDIATELY(sPoolPed[i].pedIndex)
					ELSE
						CLEAR_PED_TASKS(sPoolPed[i].pedIndex)
					ENDIF
					SET_PED_KEEP_TASK(sPoolPed[i].pedIndex, FALSE)
					sPoolPed[i].AI = N1B_PED_AI_STATE_RELAXED
				ENDIF
			ENDFOR
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_COPS_LOST
			SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
			SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "S SKIP") ENDIF #ENDIF
	        Script_Passed()
	    ENDIF	    
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))	// Check for Fail
	    	WAIT_FOR_CUTSCENE_TO_STOP()			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "F SKIP") ENDIF #ENDIF
		    SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
	    ENDIF	     
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))	// Check for Skip forward		
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_OUTRO_PHONECALL)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage) + 1
				BREAK
			ENDSWITCH
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "J SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
			DO_Z_SKIP(iNewStage, FALSE)	//perform a Z skip to the next stage, without the mission reset
	    ENDIF    
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))	// Check for Skip backwards
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage)-1
				BREAK
			ENDSWITCH	    
		    IF iNewStage > -1
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "P SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
		    ENDIF
	    ENDIF	
		
	    // Z skip menu
	    IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage)
			// if we are skipping forward in the mission stages, just J skip rather than a full mission reset
			IF eMissionStage = MISSION_STAGE_LOSE_THE_COPS	// additional stages dealt seperately since they sit at the end of the MISSION_STAGE enum
				IF (iNewStage = ENUM_TO_INT(MISSION_STAGE_OUTRO_PHONECALL))	// trying to z skip past lose cops so don't reset mission
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, FALSE)
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, TRUE)
				ENDIF
			ELIF (iNewStage <= ENUM_TO_INT(eMissionStage))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, FALSE)
			ENDIF
	    ENDIF 
	ENDPROC
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:MISSION STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------

/*PURPOSE: When Player has celeb's clothes, go onto next stage.
If Player gets in a vehicle again, they can't get the diary, tell them to get out*/
PROC STAGE_STEAL_CELEB_POSSESSIONS()	

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CHECK_AND_UPDATE_PLAYER_LOCATION()	
	CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	UPDATE_PLAYER_MOVEMENT_STATE(FALSE) // used in the ped detection system.  updated each frame
	MANAGE_POOL_AREA_STATE()
	MANAGE_GARDEN_AREA_STATE()
	DETACH_PROPS_INSIDE_GARDENER_VEHICLE()
	MONITOR_PLAYER_BARGE_DOOR_AT_HOUSE()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP	
			CREATE_CELEB_POSSESSION_BLIPS()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
				//SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressLethalMeleeActions)
			ENDIF
			IF IS_ENTITY_ALIVE(sGardenerPed.pedIndex)		
				ADD_PED_FOR_DIALOGUE(sDialogue, 4, sGardenerPed.pedIndex, "Nigel1BGardener01")
			ENDIF
			IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_CELEB].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, 5, sPoolPed[POOL_PED_CELEB].pedIndex, "Nigel1BCelebMale01")
			ENDIF				
			IF IS_ENTITY_ALIVE(sPoolPed[POOL_PED_FEMALE].pedIndex)
				ADD_PED_FOR_DIALOGUE(sDialogue, 6, sPoolPed[POOL_PED_FEMALE].pedIndex, "Nigel1BCelebFemale01")
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOW)
			
			PRINT_NOW("NIG1B_OBJ_01", DEFAULT_GOD_TEXT_TIME, 0)	// Steal Tyler Dixon's ~g~clothes.~s~	
			NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW()
			//clothes sound sfx PUT A REQUEST TO LOAD BANK
			REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1B_CLOTHES")			
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET STEALTH MOVEMENTS ON PLAYER")	//DONT SEEM TO DO ANYTHING? #ENDIF
			//SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), TRUE)	
			IF bFinishedStageSkipping
				SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, NEEDED for repeat play purposes.
			ENDIF
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_STEAL_CELEB_POSSESSIONS - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			// stop the anim task on the van (gets applied in the initial scene / init mission
			IF NOT bStoppedGardenerVanAnim
				IF IS_ENTITY_ALIVE(vehGardenerVehicle)
					IF IS_ENTITY_PLAYING_ANIM(vehGardenerVehicle, "rcmnigel1b", "idle_speedo")
						STOP_ENTITY_ANIM(vehGardenerVehicle, "idle_speedo", "rcmnigel1b", REALLY_SLOW_BLEND_OUT)
						bStoppedGardenerVanAnim = TRUE
						CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : STAGE_STEAL_CELEB_POSSESSIONS() : stopped van sync anim")						
					ENDIF
				ENDIF
			ENDIF
			//Check for player collecting celeb possession		
			IF HAS_PICKUP_BEEN_COLLECTED(mpClothesPickup.index)	// pickup_Index setup - don't check pickup exists though
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_LOW)
				
				IF ePoolAreaState <> N1B_POOL_AREA_STATE_UNDETECTED
				AND ePoolAreaState <> N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
					iTimer_TimeWindowForSpottedStealingClothesDialogue = GET_GAME_TIMER()	// limited window to say the spotted stealing clothes dialogue
				ENDIF											
				// setup the timer till we switch pool states to N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
				IF ePoolAreaState = N1B_POOL_AREA_STATE_UNDETECTED
					bDoSwitch_PoolStateTo_CelebLookingForClothes = TRUE
					iTimer_PoolStateTo_CelebLookingForClothes = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "setup timer till celeb looks for clothes	") ENDIF #ENDIF
				ENDIF					
				SAFE_REMOVE_BLIP(mpClothesPickup.blipIndex)
				SAFE_REMOVE_PICKUP(mpClothesPickup.index)
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 2.0, REPLAY_IMPORTANCE_LOWEST)	// B*1880376 - record collecting the clothes
				CPRINTLN(DEBUG_MISSION, " : STAGE_STEAL_CELEB_POSSESSIONS : clothes pickup Collected , heading to SS_CLEANUP ")
				eSubStage = SS_CLEANUP						
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP			
			SAFE_REMOVE_BLIP(mpClothesPickup.blipIndex)
			SAFE_REMOVE_PICKUP(mpClothesPickup.index)
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_01")	
				CLEAR_THIS_PRINT("NIG1B_OBJ_01")	// Steal Tyler Dixon's ~g~clothes.~s~	
			ENDIF
			
			// stop the anim task on the van (gets applied in the initial scene / init mission
			IF NOT bStoppedGardenerVanAnim
				IF IS_ENTITY_ALIVE(vehGardenerVehicle)
					IF IS_ENTITY_PLAYING_ANIM(vehGardenerVehicle, "rcmnigel1b", "idle_speedo")
						STOP_ENTITY_ANIM(vehGardenerVehicle, "idle_speedo", "rcmnigel1b", REALLY_SLOW_BLEND_OUT)
						bStoppedGardenerVanAnim = TRUE
						CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : STAGE_STEAL_CELEB_POSSESSIONS() : stopped van sync anim")						
					ENDIF
				ENDIF
			ENDIF
			
			// moved here to ensure they still get set if we are heading to lose cops state instead
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_NIGEL1B_ITEMS_STOLEN_LEAVE_THE_AREA, "Leave the area", TRUE) // 1st Mission replay checkpoint
			g_replay.iReplayInt[NIGEL1B_REPLAY_GARDENER_STATE] = ENUM_TO_INT(eGardenAreaState)
			g_replay.iReplayInt[NIGEL1B_REPLAY_POOL_PED_STATE] = ENUM_TO_INT(ePoolAreaState)
			
			// Bug fix 1050386 - if player already has wanted level just say lose cops instead of leave area, since wanted level won't drop in the area
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
				bDoneDialogue_PlayerAcknowledgesCops = TRUE		//don't make the cop comment if player already has the wanted level				
				//Player already wanted so go to lose the cops stage
				CPRINTLN(DEBUG_MISSION, "STAGE_STEAL_CELEB_POSSESSIONS - SS_CLEANUP player already has a wanted level")
				SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
			ELSE
				SET_STAGE(MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA)
				CPRINTLN(DEBUG_MISSION, "STAGE_STEAL_CELEB_POSSESSIONS - SS_CLEANUP done")
			ENDIF
		BREAK
	ENDSWITCH
	NIG1B_HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(sDialogue, "NIG1BAU", tSavedConversationRoot, tSavedConversationLabel)
ENDPROC

/// PURPOSE:
///    Player has to leave the pool area before the mission can be passed
PROC STAGE_ITEMS_STOLEN_LEAVE_THE_AREA()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CHECK_AND_UPDATE_PLAYER_LOCATION()
	CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	MONITOR_TRIGGER_MISSION_MUSIC_EVENTS()
	UPDATE_PLAYER_MOVEMENT_STATE(FALSE) // used in the ped detection system.  updated each frame
	MANAGE_POOL_AREA_STATE()
	MANAGE_GARDEN_AREA_STATE()	
	DETACH_PROPS_INSIDE_GARDENER_VEHICLE()
	
	SWITCH eSubStage
		//	------------------------------------------
		CASE SS_SETUP	
			// Bug fix 1050386 - if player already has wanted level just say lose cops instead of leave area
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				eSubStage = SS_CLEANUP
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_LEAVE_THE_AREA - SS_SETUP player already has a wanted level > SS_CLEANUP")
			ELSE
				PRINT_NOW("NIG1B_OBJ_03", 10000, 0)	// Leave the area.~s~	B*1583984 increase display time from DEFAULT_GOD_TEXT_TIME (7500)
				NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW()
				eSubStage = SS_UPDATE
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_LEAVE_THE_AREA - SS_SETUP done")
			ENDIF			
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE			
			//Check for player leaving area
			IF NOT IS_COORD_IN_RANGE_OF_COORD(vPlayerPos, vCelebritiesHouse_CentralPoint, NIGEL1B_DIST_PLAYER_LEAVES_THE_HOUSE_AREA)
			OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	// Bug fix 1050386 - if player already has wanted level just say lose cops instead of leave area
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_03")	
				CLEAR_THIS_PRINT("NIG1B_OBJ_03")	//leave the area
			ENDIF
			// Bug fix 1050386 - if player already has wanted level just say lose cops instead of leave area, since wanted level won't drop in the area
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				//Player already wanted so go to lose the cops stage
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_LEAVE_THE_AREA - SS_CLEANUP player already has a wanted level")
				SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
			ELSE
				SET_STAGE(MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA)
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_LEAVE_THE_AREA - SS_CLEANUP done")
			ENDIF
		BREAK
	ENDSWITCH
	
	NIG1B_HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(sDialogue, "NIG1BAU", tSavedConversationRoot, tSavedConversationLabel)
ENDPROC

/// PURPOSE:
///    Player has left the pool area, triggers dialogue for Trevor
///    and detects if he needs to lose a wanted level bef
///    ore Mission Passed
PROC STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CHECK_AND_UPDATE_PLAYER_LOCATION()	
	CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	MONITOR_TRIGGER_MISSION_MUSIC_EVENTS()
	UPDATE_PLAYER_MOVEMENT_STATE(FALSE) // used in the ped detection system.  updated each frame
	MANAGE_POOL_AREA_STATE()
	MANAGE_GARDEN_AREA_STATE()	
	DETACH_PROPS_INSIDE_GARDENER_VEHICLE()
	// clean up the audio scene when the player leaves the area after collecting the pickup
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1B_MAIN_MIX")
		IF ePlayerLocation != N1B_PLAYER_LOCATION_POOL_AREA
		AND ePlayerLocation != N1B_PLAYER_LOCATION_GARDEN_AREA
			STOP_AUDIO_SCENE("NIGEL_1B_MAIN_MIX")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - stop active audio scene - ", "NIGEL_1B_MAIN_MIX") ENDIF #ENDIF
		ENDIF
	ENDIF
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			//Setup which dialogue we want to play for the player leaving the area
			IF ePoolAreaState <> N1B_POOL_AREA_STATE_UNDETECTED
			AND ePoolAreaState <> N1B_POOL_AREA_STATE_CELEB_LOOKING_FOR_CLOTHES
				sRoot_PlayerLeavesAreaDialogue = "NIG1B_DD"
				// Why do these celebrities make such a drama about everything?    
			ELIF bPlayerWasSpottedByGardener
				sRoot_PlayerLeavesAreaDialogue = "NIG1B_DDG"
				// Almost stalking perfection. That gardener had to go and ruin it.  
			ELSE
				sRoot_PlayerLeavesAreaDialogue = "NIG1B_DS"
				// Who'd have thought it? Turns out I am a weapons-grade celebrity stalker.  
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1B_CLOTHES_TAKEN_NO_ALERTS) 
			ENDIF
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE		
			//load assets used to create a cop car when applying the wanted level
			IF NOT ARE_STRINGS_EQUAL(sRoot_PlayerLeavesAreaDialogue, "NIG1B_DS")	//check we will be spawning cops
				IF NOT (ARE_STRINGS_EQUAL(sRoot_PlayerLeavesAreaDialogue, "NIG1B_DDG") AND NOT IS_PED_UNINJURED(sGardenerPed.pedIndex))	//don't spawn cops if only gardener was alerted but killed
					IF NOT HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
						REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
					ENDIF
				ENDIF
			ENDIF			
			IF bDoneDialogue_ItemsStolenPlayerLeftTheArea
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(sRoot_PlayerLeavesAreaDialogue)
					eSubStage = SS_CLEANUP
				ENDIF
			ELSE
				N1B_KILL_CURRENT_CONVERSATION(FALSE)
				IF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", sRoot_PlayerLeavesAreaDialogue, CONV_PRIORITY_MEDIUM)
					// sRoot_PlayerLeavesAreaDialogue is set in SS_SETUP
					bDoneDialogue_ItemsStolenPlayerLeftTheArea = TRUE
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	//dplayer already has wanted level			
				bDoneDialogue_PlayerAcknowledgesCops = TRUE		//don't make the cop comment if player already has the wanted level				
				//Player already wanted so go to lose the cops stage
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_CLEANUP player already has a wanted level")
				SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
			ELIF bHavePoolPedsCallTheCops	//if pool peds already called the cops and player lost them earlier, don't reapply the cops
				REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	//	Gardener
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_CLEANUP pool peds had already called cops and player lsot cops")
				SET_STAGE(MISSION_STAGE_OUTRO_PHONECALL)
			ELSE
				//if player wasn't detected, and he doesn't currently have a wanted level.  Go straight to mission passed
				IF ARE_STRINGS_EQUAL(sRoot_PlayerLeavesAreaDialogue, "NIG1B_DS")
				OR (ARE_STRINGS_EQUAL(sRoot_PlayerLeavesAreaDialogue, "NIG1B_DDG") AND NOT IS_PED_UNINJURED(sGardenerPed.pedIndex))	//don't do cops if only gardener was alerted but killed
					REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	//	Gardener
					CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_CLEANUP player wasn't detected so going to outro phonecall")
					SET_STAGE(MISSION_STAGE_OUTRO_PHONECALL)
				ELSE		// need to give player a wanted level in script
					IF NOT HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
						REQUEST_COP_ASSETS_FOR_APPLYING_WANTED_LEVEL()
					ENDIF
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					bApplyWantedLevelInScript = TRUE			
					//Given player wanted level in script so, go to lose the cops stage
				CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_CLEANUP player wanted level set in script")
					SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - SS_CLEANUP done")
		BREAK
	ENDSWITCH
	
	NIG1B_HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(sDialogue, "NIG1BAU", tSavedConversationRoot, tSavedConversationLabel)
ENDPROC

/// PURPOSE:
///    Player has to lose the cops before the mission can pass
PROC STAGE_LOSE_THE_COPS()

	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CHECK_AND_UPDATE_PLAYER_LOCATION()
	CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	MONITOR_TRIGGER_MISSION_MUSIC_EVENTS()
	UPDATE_PLAYER_MOVEMENT_STATE(FALSE) // used in the ped detection system.  updated each frame
	MANAGE_POOL_AREA_STATE()
	MANAGE_GARDEN_AREA_STATE()	
	DETACH_PROPS_INSIDE_GARDENER_VEHICLE()
	// clean up the audio scene when the player leaves the area after collecting the pickup
	IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1B_MAIN_MIX")
		IF ePlayerLocation != N1B_PLAYER_LOCATION_POOL_AREA
		AND ePlayerLocation != N1B_PLAYER_LOCATION_GARDEN_AREA
			STOP_AUDIO_SCENE("NIGEL_1B_MAIN_MIX")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA - stop active audio scene - ", "NIGEL_1B_MAIN_MIX") ENDIF #ENDIF
		ENDIF
	ENDIF
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			eSubStage = SS_UPDATE
			bDisplayedWantedRatingObjective = FALSE	// lose cops objective should display everytime
			iTimer_DelayTrevorCopsDialogue = GET_GAME_TIMER()
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE				
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER LOST HIS WANTED LEVEL") ENDIF #ENDIF
				bDoneDialogue_PlayerAcknowledgesCops = TRUE	//don't make the cop comment if player has lost the wanted level already
				eSubStage = SS_CLEANUP
			ELSE
				IF bApplyWantedLevelInScript
					IF NOT bDoneDialogue_PlayerAcknowledgesCops
						IF HAS_TIME_PASSED(iTimer_DelayTrevorCopsDialogue, 1000)						
							VECTOR vTemp1, vTemp2//, vTemp3, vTemp4
							vTemp1 = vPlayerPos + << -60.0, -60.0, -60.0 >>
							vTemp2 = vPlayerPos + << 60.0, 60.0, 60.0 >>								
							IF IS_COP_VEHICLE_IN_AREA_3D(vTemp1, vTemp2)
								IF ARE_ALL_POOL_PEDS_AND_GARDENER_DEAD_OR_INJURED()
									bDoneDialogue_PlayerAcknowledgesCops = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Trevor spots cops dialogue akipped since everyone is dead /injured") ENDIF #ENDIF
								ELIF NIG1B_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "NIG1BAU", "NIG1B_DC", CONV_PRIORITY_MEDIUM)
									// The cops. Really? Now that's not nice.  
									bDoneDialogue_PlayerAcknowledgesCops = TRUE
								ENDIF							
							ELSE
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TRYING TO TRIGGER TREV COP DIALOGUE BUT NO COP NEARBY - SO TRYING TO CREATE ONE") ENDIF #ENDIF
								IF NOT bHasCopBeenCreatedForScriptedWantedLevel
									IF HAS_COP_ASSETS_LOADED_FOR_APPLYING_WANTED_LEVEL()
										bHasCopBeenCreatedForScriptedWantedLevel = CREATE_COP_IN_RANGE_OF_PLAYER(vPlayerPos)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bDisplayedWantedRatingObjective
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_04")
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "lose cops objective displayed ^^^^^^ +++++ ^^^^^") ENDIF #ENDIF
									PRINT_NOW("NIG1B_OBJ_04", DEFAULT_GOD_TEXT_TIME, 0)	// Lose the cops.
									NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW()
									bDisplayedWantedRatingObjective = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT bDisplayedWantedRatingObjective
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_04")
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "lose cops objective displayed ^^^^^^ +++++ ^^^^^") ENDIF #ENDIF
							PRINT_NOW("NIG1B_OBJ_04", DEFAULT_GOD_TEXT_TIME, 0)	// Lose the cops.
							NIG1B_CHECK_OBJECTIVE_TEXT_OVERWRITE_ONGOING_CONVERSATION_CONFLICT_NOW()
							bDisplayedWantedRatingObjective = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF				
		BREAK
		//	------------------------------------------
		CASE SS_CLEANUP
			IF IS_THIS_PRINT_BEING_DISPLAYED("NIG1B_OBJ_04")
				CLEAR_THIS_PRINT("NIG1B_OBJ_04")	//Lose the cops.
			ENDIF
			REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	//	Gardener
			SET_STAGE(MISSION_STAGE_OUTRO_PHONECALL)
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_CLEANUP done")
		BREAK
	ENDSWITCH
	
	NIG1B_HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(sDialogue, "NIG1BAU", tSavedConversationRoot, tSavedConversationLabel)
ENDPROC

/// PURPOSE:
///    Trveor calls Nigel, to inform him of the successful mission
PROC STAGE_OUTRO_PHONECALL()

	MONITOR_TRIGGER_MISSION_MUSIC_EVENTS()
	MANAGE_POOL_AREA_STATE()
	MANAGE_GARDEN_AREA_STATE()	
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			N1B_KILL_CURRENT_CONVERSATION(FALSE, TRUE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			ADD_PED_FOR_DIALOGUE(sDialogue, 3, NULL, "NIGEL")
			ADD_PED_FOR_DIALOGUE(sDialogue, 4, NULL, "MRSTHORNHILL")
			iTimer_DelayForOutroPhonecall = GET_GAME_TIMER()
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_SETUP done")
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE			
			IF NOT bHasOutroPhonecallSucceeded
				IF HAS_TIME_PASSED(iTimer_DelayForOutroPhonecall, TIME_DELAY_FOR_OUTRO_PHONECALL)
					IF PLAYER_CALL_CHAR_CELLPHONE(sDialogue, CHAR_NIGEL, "NIG1BAU", "NIG1B_OP", CONV_PRIORITY_CELLPHONE)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_LOW)
						// Hello, this is Nigel. Please be quick, I am roaming in the United States of America.  
						// It's me, you nutcase. I picked up some clothes from your friend Tyler Dixon. There's even a skid mark. 
						// What fantastic news! Wait till Mrs. Thornhill hears about this, oh she will just be pleased as punch!
						bHasOutroPhonecallSucceeded = TRUE
					ENDIF
				ELSE
					// player gets wanted before phonecall triggers
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)	// player gets wanted rating		
						//Player already wanted so go to lose the cops stage
						CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_UPDATE player has a wanted level revert back to MISSION_STAGE_LOSE_THE_COPS")
						SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_CONVERSATION_TRYING_TO_BE_RESTORED("NULL")
					IF HAS_CELLPHONE_CALL_FINISHED()
						IF HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()         // If the cellphone was in use and the player receives an injury or starts ragdolling then,
							bHasOutroPhonecallSucceeded = FALSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - cellphone convo was interrupted so bHasOutroPhonecallSucceeded * set FALSE") ENDIF #ENDIF
						ELSE
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_CLEANUP
			N1B_KILL_CURRENT_CONVERSATION(FALSE, TRUE)
			SET_STAGE(MISSION_STAGE_MISSION_PASSED)
			CPRINTLN(DEBUG_MISSION, "STAGE_OUTRO_PHONECALL - SS_CLEANUP done")
		BREAK
	ENDSWITCH					
ENDPROC

/// PURPOSE:
///    Mission has already failed at this point, but this handles
///    having to update the fail reason if the player does something more
///    serious during the fade.
PROC MISSION_FAILED_WAIT_FOR_FADE()

	STRING sFailReason = NULL
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	CHECK_AND_UPDATE_PLAYER_LOCATION()	
	CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
	
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
				CLEAR_PRINTS()
				CLEAR_HELP()			
				N1B_KILL_CURRENT_CONVERSATION(TRUE, TRUE)
				DELETE_ALL_MISSION_BLIPS()			
				//Check if fail reason needs to be displayed
				IF eN1B_MissionFailedReason = FAILED_DEFAULT
					RANDOM_CHARACTER_FAILED()
				ELSE
					RANDOM_CHARACTER_FAILED_WITH_REASON(sFailReason)
				ENDIF
				iMissionMusicEventTriggerCounter = NIGEL1B_MUSIC_EVENT_COPS_LOST
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				SAFE_TRIGGER_MISSION_MUSIC_EVENT("NIGEL1B_MISSION_FAIL")
				eSubStage = SS_UPDATE
				CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP done")
			ENDIF
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//---- Do any specific cleanup here----
				CLEANUP_ALL_MISSION_ENTITIES(TRUE)				
				CLEAR_AREA(vCelebritiesHouse_CentralPoint, 35.0, TRUE)
				CLEAR_AREA_OF_PEDS(vCelebritiesHouse_CentralPoint, 35.0)
				CLEAR_AREA_OF_VEHICLES(vCelebritiesHouse_CentralPoint, 35.0)
				CLEAR_AREA_OF_OBJECTS(vCelebritiesHouse_CentralPoint, 35.0)			
				CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE - cleanup done")	
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	PROC DEBUG_POOL_SETUP()
	
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		CHECK_AND_UPDATE_PLAYER_LOCATION()
		CHECK_AND_UPDATE_PLAYER_WANTED_LEVEL()
		CONTROL_GARDENERS_AI()	
		SWITCH eSubStage
			CASE SS_SETUP
				#IF IS_DEBUG_BUILD 
					SET_DEBUG_ACTIVE(TRUE)
				    SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				#ENDIF
				sGardenerPed.AI = N1B_PED_AI_SETUP_ROUTINE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG_POOL_SETUP  - GARDENER AI SET TO N1B_PED_AI_SETUP_ROUTINE") ENDIF #ENDIF				
				eSubStage = SS_UPDATE
			BREAK			
			CASE SS_UPDATE				
				/*
				IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "NIGEL 1B :", "PLAYER STEALTH ++++++++++++") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "NIGEL 1B :", "PLAYER NOT IN STEALTH --------") ENDIF #ENDIF
				ENDIF 
				*/
				IF bDebug_ToggleDoSpottedPlayerChecksPause
					//Basically i use these in conjunction with some widget i have setup in SCRIPT -> AZ WIDGETS -> STEALTH CHECKS
					//toggle one of the spotted checks on
					//once spotted the game will pause so you can fly the camera to that peds position to check the player was visible
					//then press the uppause game tick box to get the game running again
					//you'll have to set one of the these toggles back on in the widget to test again.	
					IF IS_PED_UNINJURED(sGardenerPed.pedIndex)						
						IF bDebug_TogglePRF_DisableSeeThroughChecksWhenTargeting_Gardener
							SET_PED_RESET_FLAG(sGardenerPed.pedIndex, PRF_DisableSeeThroughChecksWhenTargeting, TRUE)
						ENDIF					
						/*
						IF HAS_SPECIFIC_PED_DETECTED_THE_PLAYER(sGardenerPed.pedIndex)
							bDebug_ToggleDoSpottedPlayerChecksPause = FALSE						
						ENDIF
						*/					
						//Basically same as what above but stripped down version									
						//alternative to using HAS_SPECIFIC_PED_DETECTED_THE_PLAYER as this is stripped right back to basics
						SET_PED_SEEING_RANGE(sGardenerPed.pedIndex, fDebug_GardenerSeeingRange)
						SET_PED_HEARING_RANGE(sGardenerPed.pedIndex, fDebug_GardenerHearingRange)
						
						//IF CAN_PED_SEE_HATED_PED(sGardenerPed.pedIndex, PLAYER_PED_ID())						
						//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG B*433149 - CAN_PED_SEE_HATED_PED - RETURN TRUE - GARDENER SEES PLAYER") ENDIF #ENDIF
						//	SET_GAME_PAUSED(TRUE)					
						//	bDebug_ToggleDoSpottedPlayerChecksPause = FALSE						
						//ENDIF
					ENDIF
					IF NOT IS_PLAYER_USING_STEALTH_IN_POOL_AREA()
						bDebug_ToggleDoSpottedPlayerChecksPause = FALSE
					ENDIF					
				ENDIF				
			BREAK
			CASE SS_CLEANUP
			BREAK
		ENDSWITCH
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)	
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	// take ownership from the initial scene (needs to happen before launcher terminates) must check is running, could get here from replay and not be setup
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherDataLocal.iSyncSceneIndex)
		TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.iSyncSceneIndex)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF	
	
	/*IF IS_REPLAY_IN_PROGRESS() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_1B(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " IS_REPLAY_IN_PROGRESS - waiting on SetupScene_NIGEL_1B")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_NIGEL_1B, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
		g_bSceneAutoTrigger = FALSE
	ENDIF*/

	INIT_MISSION()
	
	// handle replay checkpoints
	IF IS_REPLAY_IN_PROGRESS()	
	
		CREATE_VEHICLE_FOR_REPLAY(vehIndex_MissionReplayRestore, << -990.5321, 342.9136, 70.4615 >>, 111.0291, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF		
		
		SWITCH iReplayStage	
			CASE CP_NIGEL1B_STEAL_ITEMS
				START_REPLAY_SETUP(vPos_PlayerMissionStart, fHeading_PlayerMissionStart)
				DO_Z_SKIP(Z_SKIP_STEAL_CELEB_CLOTHES)
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "steal items in progress")
			BREAK
			CASE CP_NIGEL1B_ITEMS_STOLEN_LEAVE_THE_AREA
				START_REPLAY_SETUP(<< -1045.0170, 367.3012, 68.9128 >>, 103.7010)
				DO_Z_SKIP(Z_SKIP_ITEMS_STOLEN_LEAVE_THE_AREA)
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "leave the area in progress")
			BREAK
			CASE CP_NIGEL1B_MISSION_PASSED
				START_REPLAY_SETUP(<< -462.2575, -156.7884, 37.0458 >>, 112.5754)
				DO_Z_SKIP(Z_SKIP_MISSION_PASSED)	 
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "mission passed in progress")
			BREAK
			DEFAULT
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "starting mission from invalid checkpoint, so possibly very start of the mission")
			BREAK
		ENDSWITCH
	ELIF IS_REPEAT_PLAY_ACTIVE()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SAFE_TELEPORT_PED(PLAYER_PED_ID(), vPos_PlayerMissionStart, fHeading_PlayerMissionStart)	//setup player in spot which matches start of the mission
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			CPRINTLN(DEBUG_MISSION, "IS_REPEAT_PLAY_ACTIVE -  set player coords for repeat play ", vPos_PlayerMissionStart, " framecount : ", GET_FRAME_COUNT())
		ENDIF
	ENDIF	

	WHILE(TRUE)	
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_VST")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		
			MISSION_FAILED_CHECKS()	
			
			// Fix B*1121745 - swap over to call everyframe commands
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			SWITCH eMissionStage						
				CASE MISSION_STAGE_STEAL_CELEB_POSSESSIONS
					STAGE_STEAL_CELEB_POSSESSIONS()
				BREAK				
				CASE MISSION_STAGE_ITEMS_STOLEN_LEAVE_THE_AREA
					STAGE_ITEMS_STOLEN_LEAVE_THE_AREA()
				BREAK				
				CASE MISSION_STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA
					STAGE_ITEMS_STOLEN_PLAYER_LEFT_THE_AREA()
				BREAK				
				CASE MISSION_STAGE_LOSE_THE_COPS
					STAGE_LOSE_THE_COPS()
				BREAK
				CASE MISSION_STAGE_OUTRO_PHONECALL
					STAGE_OUTRO_PHONECALL()
				BREAK	
				CASE MISSION_STAGE_MISSION_PASSED
					Script_Passed()
				BREAK
				CASE MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
					MISSION_FAILED_WAIT_FOR_FADE()
				BREAK				
					CASE MISSION_STAGE_DEBUG_POOL_SETUP		// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_POOL_SETUP()
						#ENDIF
					BREAK
			ENDSWITCH			
		ENDIF
		
		// if we are skipping through the mission stages, for a checkpoint / debug skip		
		IF bFinishedStageSkipping = FALSE		
			JUMP_TO_STAGE(eMissionSkipTargetStage)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_MISSION_WIDGETS()	
			//This is here so it can use the debug skipping functions
			IF bDebug_SetupMissionToTest_Can_Ped_See_Hated_Ped
				RC_START_Z_SKIP()
				RESET_MISSION()
				SET_STAGE(MISSION_STAGE_DEBUG_POOL_SETUP)
				RC_END_Z_SKIP()
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -1009.2120, 355.0336, 69.9676 >>, 27.9385)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission setup to test B*433149 - CAN_PED_SEE_HATED_PED returning true when ped is out of sight.") ENDIF #ENDIF
				bDebug_SetupMissionToTest_Can_Ped_See_Hated_Ped = FALSE
			ENDIF	
		  	IF bFinishedStageSkipping = TRUE		// not skipping stages, check for debug keys as long as we aren't in fail state				
				IF eMissionStage <> MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
		        	DEBUG_Check_Debug_Keys()
				ENDIF
			ENDIF
        #ENDIF	
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
