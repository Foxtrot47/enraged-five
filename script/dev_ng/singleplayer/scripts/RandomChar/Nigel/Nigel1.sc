
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "CompletionPercentage_public.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "cutscene_public.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "initial_scenes_Nigel.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Nigel1.sc
//		AUTHOR			:	Joanna Wright
//		DESCRIPTION		:	Random character mission, watch cutscene
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs 	  sRCLauncherDataLocal
structPedsForConversation sDialogue

ENUM eRC_MainState
	RC_MEET_NIGEL = 0,
	RC_LEADOUT
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Mission state
eRC_MainState m_eState    = RC_MEET_NIGEL
eRC_SubState  m_eSubState = SS_SETUP
BOOL bEndConv 			  = FALSE
SCENARIO_BLOCKING_INDEX mScenarioBlocker

CONST_INT NIGEL			0
CONST_INT MRS_THORNHILL	1

INT iCutsceneStage

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF
	
	ENABLE_VEHICLES_NEAR_NIGEL1(TRUE)
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 2)
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	CLEAR_PED_NON_CREATION_AREA()
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK)
	Random_Character_Passed(CP_RAND_C_NIG1)
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	
	/// PURPOSE:
	///    Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		// Check for Pass / j skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			WAIT_FOR_CUTSCENE_TO_STOP()
			// Reset camera behind player
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF	
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================
/// PURPOSE:
///    Sets the new mission state and initialises the substate.
PROC SetState(eRC_MainState in)
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

/// PURPOSE:
///    Triggers the mocap cutscene and completes mission once finished
PROC STATE_MeetNigel()

	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	
	SWITCH m_eSubState
		
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Init: RC_MEET_NIGEL") 
			RC_REQUEST_CUTSCENE("NMT_1_RCM", TRUE)
			iCutsceneStage = 0
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE	
			SWITCH iCutsceneStage
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NIGEL])
							REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[NIGEL], "Nigel", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MRS_THORNHILL])
							REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[MRS_THORNHILL], "MRS_Thornhill", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						RC_CLEANUP_LAUNCHER()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						START_CUTSCENE()
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1103.326904,786.843994,157.713257>>, <<-1090.286499,788.601440,168.522827>>, 10.0, <<-1107.0227, 788.9111, 163.0734>>, 187.0703, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1107.0227, 788.9111, 163.0734>>, 187.0703)
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE, TRUE) // B*1569275 Ensure area is cleared
						ENDIF
						RC_START_CUTSCENE_MODE(<< -1098.9072, 787.6641, 163.0641 >>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_CUTSCENE_PLAYING()
					    RC_END_CUTSCENE_MODE()
						RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
						IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
							CREATE_NIGEL_VEHICLE(sRCLauncherDataLocal.vehID[0], <<-1084.4558, 787.2556, 163.9860>>, 100.8542, FALSE, TRUE)
						ENDIF
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
							SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.vehID[0], <<-1094.4845, 785.5936, 163.2927>>, 102.2748)
							SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[0])
							SET_DISABLE_PRETEND_OCCUPANTS(sRCLauncherDataLocal.vehID[0], TRUE)
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NIGEL])
								SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[NIGEL], RELGROUPHASH_PLAYER)
								CLEAR_PED_TASKS_IMMEDIATELY(sRCLauncherDataLocal.pedID[NIGEL])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[NIGEL], TRUE)
								SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[NIGEL], sRCLauncherDataLocal.vehID[0], VS_DRIVER)
								SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[NIGEL], TRUE)
							ENDIF
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MRS_THORNHILL])
								SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[MRS_THORNHILL], RELGROUPHASH_PLAYER)
								CLEAR_PED_TASKS_IMMEDIATELY(sRCLauncherDataLocal.pedID[MRS_THORNHILL])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[MRS_THORNHILL], TRUE)
								SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[MRS_THORNHILL], sRCLauncherDataLocal.vehID[0], VS_FRONT_RIGHT)
								SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[MRS_THORNHILL], TRUE)
							ENDIF
							SET_VEHICLE_ENGINE_ON(sRCLauncherDataLocal.vehID[0], TRUE, TRUE)
							TASK_VEHICLE_DRIVE_WANDER(sRCLauncherDataLocal.pedID[NIGEL], sRCLauncherDataLocal.vehID[0], 20, DRIVINGMODE_STOPFORCARS)
						ENDIF
						REPLAY_STOP_EVENT()
						SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[NIGEL])
						SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[MRS_THORNHILL])
						SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[0])
						CPRINTLN(DEBUG_MISSION, "Cleaning up RC_MEET_NIGEL") 			
						m_eState 	= RC_LEADOUT
						m_eSubState = SS_SETUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Trevor comments on Nigel etc
PROC STATE_Leadout()

	SWITCH m_eSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Init: RC_LEADOUT")
			bEndConv = FALSE
			ADD_PED_FOR_DIALOGUE(sDialogue, 2, PLAYER_PED_ID(), "TREVOR")
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE	
			IF NOT bEndConv
				bEndConv = CREATE_CONVERSATION(sDialogue, "NIG1AAU", "NIG1A_COMM", CONV_PRIORITY_MEDIUM)	
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CPRINTLN(DEBUG_MISSION, "Conv finished")
					m_eSubState = SS_CLEANUP // Complete mission
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleaning up RC_LEADOUT") 
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_1(sRCLauncherDataLocal)
	  		WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	mScenarioBlocker = Nigel1_Scenario_Blocker()
	SET_NIGEL1_PED_NON_CREATION_AREA()

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_NAMT")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			CASE RC_MEET_NIGEL
				STATE_MeetNigel()
			BREAK
			
			CASE RC_LEADOUT
				STATE_Leadout()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
