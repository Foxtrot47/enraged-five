
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "commands_entity.sch"
USING "commands_event.sch"
USING "commands_script.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch"
USING "CompletionPercentage_public.sch"
USING "script_ped.sch"
USING "script_player.sch"


USING "randomChar_public.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_public.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF
USING "commands_recording.sch"

// CONSTANTS FOR UBER PLAYBACK
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      5
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       5
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    5

// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           3
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         3         
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            3

USING "initial_scenes_Nigel.sch"
USING "traffic.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Nigel1C.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Random character mission, Catch and return a celebrity dog
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

g_structRCScriptArgs sRCLauncherDataLocal


//*************************************************************************************************************************************************
//													:ENUMS:
//*************************************************************************************************************************************************
// Mission stages
ENUM MISSION_STAGE
	MS_INIT,
	MS_GO_TO_SHOPS,
	MS_STAGE_APPROACH_CELEB,
	MS_STAGE_CHAT_UP_CELEB,
	MS_DOG_RUNS_AWAY,
	MS_DOG_RUNS_ON_ROAD,
	MS_DOG_WAITING_AT_FIRESTATION,
	MS_GRAB_DOG_CUTSCENE,
	MS_STAGE_PLAY_OUTRO,
	MS_WAIT_FOR_CALL_TO_END,
	MS_FAIL_WAIT_FOR_FADE
ENDENUM

ENUM STAGE_STAGE
	SS_SETUP,
	SS_STAGE,
	SS_STAGE_2
ENDENUM

ENUM CRASH_STAGE
	CS_WAITING,
	CS_SET_TO_DRIVE,
	CS_DRIVING,
	CS_SET_TO_CRASH,
	CS_CRASHING,
	CS_TASK_THEM_TO_EXIT,
	CS_FINISHED
ENDENUM
CRASH_STAGE crashStage = CS_WAITING

ENUM JEEP_STAGE
	JS_WAITING,
	JS_SET_TO_DRIVE,
	JS_DRIVING,
	JS_SET_TO_SKID,
	JS_EXITING_CAR,
	JS_LOOKS_AT_CRASH,
	JS_PLAYER_ENTERS_CAR,
	JS_JEEP_FLEES
ENDENUM
JEEP_STAGE jeepStage = JS_WAITING

ENUM FAIL_REASON_ENUM
	FAIL_CELEB_INJURED,
	FAIL_CELEB_KILLED,
	FAIL_CELEB_SCARED,
	FAIL_KILLED_DOG,
	FAIL_INJURED_DOG,
	FAIL_LOST_DOG,
	FAIL_DEFAULT
ENDENUM
FAIL_REASON_ENUM failReason

ENUM CELEBRITY_CHAT
	CC_PUT_GUN_AWAY,
	CC_SET_WHISTLE,
	CC_WHISTLE,
	CC_SET_TALKING,
	CC_PLAYER_AND_CELEBRITY_TALKING,
	CC_DOG_STARTS_WALKING,
	CC_PLAYER_TOO_FAR,
	CC_RETURN_TO_CONVO_LINE
ENDENUM
CELEBRITY_CHAT celebDialogueState = CC_PLAYER_TOO_FAR

ENUM debugSkipToMissionStage
	DEBUG_TO_START,
	DEBUG_TO_CHECKPOINT,
	DEBUG_TO_STORES,
	DEBUG_TO_ROAD_CHASE,
	DEBUG_TO_END_CUTSCENE
ENDENUM

ENUM DOG_RUN_SPEEDS
	dogRunsFastest,
	dogRunsNormal,
	dogRunsSlow,
	dogRunsVerySlow
ENDENUM
DOG_RUN_SPEEDS runSpeed = dogRunsFastest

ENUM TAXI_DRIVER_STATES
	taxiDriver_lookingAtCarCrash,
	taxiDriver_attackingPlayer,
	taxiDriver_watchingPlayer,
	taxiDriver_returnToCrash,
	taxiDriver_fleeing
ENDENUM
TAXI_DRIVER_STATES taxiDriverState = taxiDriver_lookingAtCarCrash

ENUM FEMALE_DRIVER_STATES
	femaleDriver_lookingAtCarCrash,
	femaleDriver_cower,
	femaleDriver_flee
ENDENUM
FEMALE_DRIVER_STATES femaleDriverState = femaleDriver_lookingAtCarCrash

//*************************************************************************************************************************************************
//													:STRUCTS:
//*************************************************************************************************************************************************
STRUCT MISSION_VEHICLE
	VEHICLE_INDEX index
	PED_INDEX driver
	MODEL_NAMES carModel
	MODEL_NAMES driverModel
	VECTOR startPosition
	FLOAT startHeading
ENDSTRUCT

STRUCT MISSION_PED
	PED_INDEX index
	MODEL_NAMES model
	VECTOR vPosStart
	FLOAT fHeading
	VECTOR vPosMid
	VECTOR vPosEnd
	INT startWalkingWaypoint
	BOOL startedWalking
	BOOL beenShocked
	STRING anim
ENDSTRUCT

//*************************************************************************************************************************************************
//													:VARIABLES:
//*************************************************************************************************************************************************
MISSION_STAGE missionStage  = MS_INIT //track what mission stage we are at
STAGE_STAGE stageStage = SS_SETUP

MISSION_PED shockedPed[5]

PED_INDEX CELEBRITY_PED
PED_INDEX CELEBRITY_DOG
VECTOR vPos_Celeb = <<-626.87, -268.85, 38>>
VECTOR vModelDest = <<-642.32, -238.21, 36.86>>
/*VECTOR vPos_Dog = <<-627.40, -267.12, 38.23>>
FLOAT fDir_Celeb = 121.00
FLOAT fDir_Dog = -59.95*/
STRING dogRoute = "NIG1C"
BOOL bDoStopRunningConv = TRUE
CONST_FLOAT fChaseFailDistance 100.0
CONST_FLOAT fBlipFlashDistance 0.7

CONST_INT WAIT_LINE_TIME 5000
INT iWaitLineTimer = 0

PED_INDEX TAXI_PASSENGER_PED

INT iSyncedSceneID

MODEL_NAMES femaleHipsterModel = A_F_Y_Hipster_04 
MODEL_NAMES businessManModel = A_M_Y_Business_01
//MODEL_NAMES fireManModel = S_M_Y_FIREMAN_01
MODEL_NAMES dogModel = A_C_ROTTWEILER
MODEL_NAMES taxiDriverModel = S_M_M_TRUCKER_01
MODEL_NAMES taxiModel = TAXI
MODEL_NAMES sentinelCarModel = SENTINEL
MODEL_NAMES familyCarModel = HABANERO
MODEL_NAMES jeepCarModel = LANDSTALKER
MODEL_NAMES poshCarModel = BANSHEE
MODEL_NAMES escortCarModel = BJXL

MISSION_VEHICLE CRASH_CAR_POSH
MISSION_VEHICLE CRASH_CAR_FAMILY
MISSION_VEHICLE SKID_CAR_TAXI
MISSION_VEHICLE SKID_CAR_JEEP
// This will probably go back in so just commenting it out. B* 1107526
//MISSION_VEHICLE FIRE_TRUCK
MISSION_VEHICLE SKID_CAR_SENTINEL
//MISSION_VEHICLE ESCORT_CAR_FAMILY
MISSION_VEHICLE TAXI_CAR
MISSION_VEHICLE FIRST_CAR
MISSION_VEHICLE SECOND_CAR
MISSION_VEHICLE FINAL_CAR

VEHICLE_INDEX STATIONARY_FIRE_TRUCK

BLIP_INDEX GOTO_BLIP

structPedsForConversation sSpeech
SEQUENCE_INDEX seq
//REL_GROUP_HASH relGroupFriendly
REL_GROUP_HASH relGroupPushed

REL_GROUP_HASH relGroupDrivers

//VECTOR vPos_GoToShops = <<-623.880859,-266.201294,37.765167>>//<<-617.765381,-275.147888,37.857578>>//<<-610.360840,-283.530853,37.859695>>//<<-625.902832,-267.894592,37.808289>>//<<-626.87, -268.85, 38>>
VECTOR vPOS_FINAL_WAYPOINT
INT iNUM_WAYPOINTS

CAMERA_INDEX camCutsceneStart

BOOL bBEEN_WARNED_ABOUT_DISTANCE = FALSE

BOOL bCAR_HONKING = FALSE
BOOL bSPORTS_HONKING = FALSE
BOOL bSENTINEL_HONKING = FALSE
BOOL bRELEASE_SPORTS_CAR = FALSE
BOOL bRELEASE_SENTINEL_CAR = FALSE
BOOL bOUTRO_CALL_GOING_ON = FALSE
//BOOL bPLAYER_RETURNED_TO_CELEB = FALSE

BOOL bSHOP_PED_SHOUTED = FALSE
BOOL bTREVOR_RESPONDED = FALSE
BOOL bCRASH_SHOUTED = FALSE

BOOL bPLAYED_COLLAR_SOUND = FALSE
//BOOL bRESUMED_COLLAR_CONVERSATION = FALSE

BOOL bFinal_CHASE_LINES_READY = FALSE
BOOL bGotchaLineReady = FALSE
BOOL bWHAT_YOU_LOOKIN_AT_LINES_READY = FALSE
BOOL bCelebPleaded = FALSE
BOOL bPlayerThreatenedModel = FALSE

BOOL bFirstCarsWaiting = TRUE
BOOL bSeconCarRepositioned = FALSE

BOOL bSecondPartReady = TRUE

STRING recBrakeCar = "NIG1CBrake"
INT iNumRecBrakeCar = 102

STRING recCrashCars = "NIG1CCrash"
INT iNumRecStraightCrashCar = 101
INT iNumRecSwerveCrashCar = 103

INT iKerryShoppingTimer
VECTOR vKerrySpeaksPos1 = <<-603.926575,-293.947876,34.779064>>
VECTOR vKerrySpeaksPos2 = <<-636.499878,-242.544357,39.160896>> 
FLOAT fKerrySpeaksWidth = 20.0
INT iCelebScaredTimer

STRING NIGEL1C_DICT = "rcmnigel1c"
//STRING TAXI_HAIL_DICT = "taxi_hail"
/*STRING KNEEL_ENTER_DICT = "amb@medic@standing@kneel@enter"
STRING KNEEL_DICT = "amb@medic@standing@kneel@base"
STRING KNEEL_EXIT_DICT = "amb@medic@standing@kneel@exit"*/
STRING DOG_DICT = "misschop_vehicle@back_of_van"
STRING MILAN_DICT = "gestures@f@standing@casual"
STRING OUTRO_DICT = "random@nigel@1c"

VECTOR vFirestationClearLocation = <<-643.8831, -108.9039, 36.9360>>
VECTOR vFirestationClearAreaMin = <<-662.3463, -126.8440, 36.8854>>
VECTOR vFirestationClearAreaMax = <<-627.8564, -92.9756, 38.0087>>

VECTOR vShoppingAreaA = <<-606.683472,-293.856354,34.779278>>
VECTOR vShoppingAreaB = <<-642.655945,-236.122238,38.889034>>
FLOAT fShoppingArea = 20.0

VECTOR vChatUpAreaPos1 = <<-608.483154,-286.606964,39.750637>>
VECTOR vChatUpAreaPos2 = <<-634.568115,-246.608185,36.346996>> 
FLOAT fChatUpAreaWidth = 20.0

// Cutscene details for the player stopping the dog in the alley
VECTOR vAlleyCutscenePos1 = <<-620.598328,-263.043945,38.763931>>
VECTOR vAlleyCutscenePos2 = <<-665.576782,-220.708847,35.374737>> 
FLOAT fAlleyCutsceneWidth = 30.0
VECTOR vEmergencyOutro = <<-641.819824,-235.572113,37.881439>>
FLOAT fEmergencyOutro = 65.317154
VECTOR vEmergencyCarRespot = <<-665.364014,-232.790390,36.649349>>
FLOAT fEmergencyCarRespot = 81.837189

// timers
INT iChatUpTimer = 0
//INT iGrowlTimer = 0
INT iOutroTimer = 0
//INT iCallRestartDelay = 0
CONST_INT CALL_RESTART_DELAY 2000

// variables to check for mission passed stats
CONST_FLOAT WARN_DIST 50.0

ENUM SCARE_REASON
	SCARED_DOG,
	SCARED_MODEL,
	MODEL_SCARED_DOG,
	PUSHED_MODEL
ENDENUM
SCARE_REASON reasonDogRan

//TEXT_LABEL_23 ConvResumeLabel = ""

FLOAT fCAR_REC_SPEED = 1.0

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_START 0
//CONST_INT CP_CELEB 1
CONST_INT CP_MISSION_PASSED 1//2

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 			5
	INT i_debug_jump_stage					= 0
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH] 
	
	WIDGET_GROUP_ID widgetGroup
	PROC  SETUP_WIDGETS()
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup = START_WIDGET_GROUP("NIGEL1C WIDGETS")
			SET_UBER_PARENT_WIDGET_GROUP(widgetGroup) 
			STOP_WIDGET_GROUP()
		ENDIF
	ENDPROC
#ENDIF
// ===========================================================================================================
//		Termination
// ===========================================================================================================


// -----------------------------------------------------------------------------------------------------------
//		Script cleanup helpers
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Remove one of the shocked peds
/// PARAMS:
///    i - the index of the shocked ped to remove
PROC CLEANUP_SHOCKED_PEDS(INT i)
	IF DOES_ENTITY_EXIST(shockedPed[i].index)
		SAFE_RELEASE_PED(shockedPed[i].index, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(shockedPed[i].model)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Removes a script vehicle and its associated driver
/// PARAMS:
///    vehicle - the MISSION_VEHICLE struct to be removed
PROC CLEANUP_CAR_AND_DRIVER(MISSION_VEHICLE vehicle)
	CPRINTLN(DEBUG_MISSION, "Driver ")
	SAFE_RELEASE_PED(vehicle.driver)
	
	CPRINTLN(DEBUG_MISSION, "and vehicle is gone")
	SAFE_RELEASE_VEHICLE(vehicle.index)
ENDPROC

/// PURPOSE:
///    Removes all assets used by the mission
PROC ASSETS_CLEANUP()	
	
	REMOVE_RELATIONSHIP_GROUP(relGroupPushed)
	REMOVE_RELATIONSHIP_GROUP(relGroupDrivers)

	IF DOES_BLIP_EXIST(GOTO_BLIP)
		REMOVE_BLIP(GOTO_BLIP)
	ENDIF
		
	SAFE_RELEASE_PED(CELEBRITY_PED)
	SAFE_RELEASE_PED(CELEBRITY_DOG)
	SAFE_RELEASE_PED(TAXI_PASSENGER_PED, FALSE)
	SAFE_RELEASE_VEHICLE(STATIONARY_FIRE_TRUCK)
	
	// This will probably go back in so just commenting it out. B* 1107526
	//CLEANUP_CAR_AND_DRIVER(FIRE_TRUCK)
	CLEANUP_CAR_AND_DRIVER(CRASH_CAR_FAMILY)
	CLEANUP_CAR_AND_DRIVER(SKID_CAR_JEEP)
	CLEANUP_CAR_AND_DRIVER(CRASH_CAR_POSH)	
	CLEANUP_CAR_AND_DRIVER(SKID_CAR_TAXI)
	CLEANUP_CAR_AND_DRIVER(SKID_CAR_SENTINEL)
	//CLEANUP_CAR_AND_DRIVER(ESCORT_CAR_FAMILY)
	CLEANUP_CAR_AND_DRIVER(TAXI_CAR)
	CLEANUP_CAR_AND_DRIVER(FIRST_CAR)
	CLEANUP_CAR_AND_DRIVER(SECOND_CAR)
	CLEANUP_CAR_AND_DRIVER(FINAL_CAR)
	
	CLEANUP_SHOCKED_PEDS(0)
	CLEANUP_SHOCKED_PEDS(1)
	CLEANUP_SHOCKED_PEDS(2)
	CLEANUP_SHOCKED_PEDS(4)				
	
	SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)	
	
	REMOVE_ANIM_DICT(NIGEL1C_DICT)
	/*REMOVE_ANIM_DICT(KNEEL_ENTER_DICT)
	REMOVE_ANIM_DICT(KNEEL_DICT)
	REMOVE_ANIM_DICT(KNEEL_EXIT_DICT)*/
	REMOVE_ANIM_DICT(OUTRO_DICT)
	REMOVE_ANIM_DICT(DOG_DICT)
	REMOVE_ANIM_DICT(MILAN_DICT)
	//REMOVE_ANIM_DICT(TAXI_HAIL_DICT)
	
	REMOVE_VEHICLE_RECORDING(iNumRecStraightCrashCar, recCrashCars)		
	REMOVE_VEHICLE_RECORDING(iNumRecSwerveCrashCar, recCrashCars)	
	REMOVE_VEHICLE_RECORDING(iNumRecBrakeCar, recBrakeCar)		
	REMOVE_VEHICLE_RECORDING(001, "NIG1C_COMET")	
	REMOVE_VEHICLE_RECORDING(002, "NIG1C_COMET2")	
	REMOVE_VEHICLE_RECORDING(001, "NIG1CEscort")	
	REMOVE_VEHICLE_RECORDING(001, "NIG1C_FIRE")		
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	SET_PED_MODEL_IS_SUPPRESSED(femaleHipsterModel, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(businessManModel, FALSE)
	//SET_PED_MODEL_IS_SUPPRESSED(fireManModel, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(taxiDriverModel, FALSE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(taxiModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(sentinelCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(familyCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(escortCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(poshCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(jeepCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(femaleHipsterModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(dogModel)	
	SET_MODEL_AS_NO_LONGER_NEEDED(businessManModel)	
	//SET_MODEL_AS_NO_LONGER_NEEDED(fireManModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(taxiDriverModel)
	
ENDPROC

/// PURPOSE:
///    Turns roads on or off
/// PARAMS:
///    roadsOn - TRUE if turning roads on, FALSE if turning them off
PROC TURN_ROADS_ON(BOOL roadsOn)
		
	/*SET_ROADS_IN_ANGLED_AREA(<<-690.617798,-232.600967,35.813908>>, <<-621.191406,-190.706741,37.760944>>, 49.750000, TRUE, roadsOn)
	SET_ROADS_IN_ANGLED_AREA(<<-695.448059,-235.328827,38.787209>>, <<-657.659302,-299.484222,33.460602>>, 25.500000, TRUE, roadsOn)
	SET_ROADS_IN_ANGLED_AREA(<<-627.666870,-179.075363,35.767746>>, <<-662.012634,-119.546036,38.777054>>, 10.250000, TRUE, roadsOn)
	SET_ROADS_IN_ANGLED_AREA(<<-598.983337,-240.166519,34.820423>>, <<-497.172363,-193.373306,37.849590>>, 133.000000, TRUE, roadsOn)
	
	SET_ROADS_IN_ANGLED_AREA(<<-689.055115,-217.763550,35.166630>>, <<-596.409790,-170.035034,38.057011>>, 20.0, TRUE, roadsOn)
	SET_ROADS_IN_ANGLED_AREA(<<-685.225769,-76.094269,35.667927>>, <<-618.345642,-194.644562,38.753483>>, 20.0, TRUE, roadsOn)*/
	
	// instead of disabling individual roads, try one huge area
	SET_ROADS_IN_ANGLED_AREA(<<-533.335815,-316.634003,30.996708>>, <<-696.271423,-30.989979,45.693817>>, 250.0, TRUE, roadsOn)
	
	if roadsOn
		SET_ALL_VEHICLE_GENERATORS_ACTIVE()
			
		CLEAR_PED_NON_CREATION_AREA()
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_PED_PATHS_BACK_TO_ORIGINAL(<<-654.77, -100.04, 44.87>>, <<-623.99, -165.20, 36.78>>)
	ELSE
		CLEAR_AREA_OF_VEHICLES(vFirestationClearLocation, 50)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-698.44, -333.93, 33>>, <<-532.67, -44.35, 40>>, FALSE)	
			
		SET_PED_NON_CREATION_AREA(<<-654.77, -100.04, 44.87>>, <<-623.99, -165.20, 36.78>>)
		ADD_SCENARIO_BLOCKING_AREA(<<-654.77, -100.04, 44.87>>, <<-623.99, -165.20, 36.78>>)
		ADD_SCENARIO_BLOCKING_AREA(vFirestationClearAreaMin, vFirestationClearAreaMax)
		ADD_SCENARIO_BLOCKING_AREA(<<-714.4662, -233.7738, 33.0351>>, <<-611.9510, -89.9566, 57.4080>>)
		SET_PED_PATHS_IN_AREA(<<-654.77, -100.04, 44.87>>, <<-623.99, -165.20, 36.78>>, FALSE)
		CLEAR_AREA_OF_PEDS(<<-640.004761,-140.212540,36.378510>>, 24.25)
		CLEAR_AREA_OF_PEDS(vFirestationClearLocation, 50)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Cleans up the mission
PROC Script_Cleanup()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
							
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1C_SCENE")
			STOP_AUDIO_SCENE("NIGEL_1C_SCENE")
		ENDIF
		
		TURN_ROADS_ON(TRUE)
		
		STOP_GAMEPLAY_HINT()
		
		CLEAR_PRINTS()
	
		CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT, TRUE)
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
		
	ASSETS_CLEANUP()
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FIRETRUK, FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)

	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	TERMINATE_THIS_THREAD()

ENDPROC

/// PURPOSE:
///    Cleans up the scripted cutscene at the end of the mission
/// PARAMS:
///    bInterp - TRUE if the cutscene camera should interpolate to the game camera, FALSE otherwise
///    bResetGamePlayCamera - TRUE if the camera should be placed behind the player, FALSE otherwise
PROC FINISH_CUTSCENE(BOOL bInterp = TRUE, BOOL bResetGamePlayCamera = TRUE)
	//bInterp = bInterp
	RENDER_SCRIPT_CAMS(FALSE, bInterp, DEFAULT_INTERP_TO_FROM_GAME)
	
	IF DOES_CAM_EXIST(camCutsceneStart)
		SET_CAM_ACTIVE(camCutsceneStart, FALSE)
		DESTROY_CAM	(camCutsceneStart)
	ENDIF
	
	IF bResetGamePlayCamera
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5)
	ENDIF

	TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	RC_END_CUTSCENE_MODE()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Passes the mission
PROC Script_Passed()
		
	#IF IS_DEBUG_BUILD
		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			CPRINTLN(DEBUG_MISSION, "WAITING FOR THE CONVO TO END BEFORE CLEANING UP")
			KILL_ANY_CONVERSATION()
			WAIT(0)
		ENDWHILE
	#ENDIF

	Random_Character_Passed(CP_RAND_C_NIG1C)
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Cleans up the mission on failure
PROC Script_Failed()
	
	Script_Cleanup()

ENDPROC

// ===========================================================================================================
//		PASS_FAIL_CLEANUP
// ===========================================================================================================
/// PURPOSE:
///    Has the player left the area the shops are in?
/// RETURNS:
///    TRUE if the player has left the area, FALSE otherwise
FUNC BOOL PLAYER_LEFT_SHOPS_AREA()
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-650.622925,-358.134674,33.782822>>, <<-548.965820,-302.563751,41>>, 83.250000)
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-606.433105,-289.939331,35.779270>>, <<-629.503845,-249.769501,41>>, 19.750000)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Has the player entered the building site used in one of the heist missions?
/// RETURNS:
///    TRUE if the player has entered the building site, FALSE otherwise
FUNC BOOL PLAYER_ENTERED_HEIST_BUILDING_SITE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-594.618530,-288.646393,34.454720>>, <<-582.652222,-281.528717,40.954720>>, 12.750000)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Checks if the player has run over Dexie
/// RETURNS:
///    TRUE if the player is in a vehicle that is touching Dexie and the vehicle is moving
FUNC BOOL PLAYER_RAN_OVER_DOG()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_TOUCHING_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID())
	AND NOT IS_PED_STOPPED(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks all the reasons the player might have failed the mission for
PROC CHECK_FAIL_POSSIBILITIES()

	IF DOES_ENTITY_EXIST(CELEBRITY_DOG)
		IF IS_PED_INJURED(CELEBRITY_DOG)
			failReason = FAIL_KILLED_DOG
			stageStage = SS_SETUP
			missionStage = MS_FAIL_WAIT_FOR_FADE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID())
			//OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_TOUCHING_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID()))
			OR PLAYER_RAN_OVER_DOG()
           		SET_PED_TO_RAGDOLL(CELEBRITY_DOG, 500, 1000, TASK_RELAX)
				failReason = FAIL_INJURED_DOG
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			ELIF GET_NUMBER_OF_FIRES_IN_RANGE(GET_ENTITY_COORDS(CELEBRITY_DOG), 2.0) > 0
           		SET_PED_TO_RAGDOLL(CELEBRITY_DOG, 500, 1000, TASK_RELAX)
				failReason = FAIL_KILLED_DOG
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(CELEBRITY_PED)
		IF IS_ENTITY_DEAD(CELEBRITY_PED)
			failReason = FAIL_CELEB_KILLED
			stageStage = SS_SETUP
			missionStage = MS_FAIL_WAIT_FOR_FADE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CELEBRITY_PED, PLAYER_PED_ID())
           		failReason = FAIL_CELEB_INJURED		
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			//ELIF IS_PLAYER_SHOOTING_NEAR_PED(CELEBRITY_PED)
			ELIF CHECK_BULLET_SHOOTING_IN_AREA(CELEBRITY_PED, TRUE, 8.0, 15.0)
			OR CHECK_PROJECTILE_IN_RANGE(CELEBRITY_PED, 5.0)
				IF IS_PED_UNINJURED(CELEBRITY_PED) AND NOT IS_PED_FLEEING(CELEBRITY_PED)
					IF IS_PED_USING_ANY_SCENARIO(CELEBRITY_PED)
	    				SET_PED_PANIC_EXIT_SCENARIO(CELEBRITY_PED, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF
					TASK_SMART_FLEE_PED(CELEBRITY_PED, PLAYER_PED_ID(), 500, -1)
				ENDIF
				IF IS_PED_UNINJURED(CELEBRITY_DOG) AND NOT IS_PED_FLEEING(CELEBRITY_DOG)
					TASK_SMART_FLEE_PED(CELEBRITY_DOG, PLAYER_PED_ID(), 500, -1)
				ENDIF
				failReason = FAIL_CELEB_SCARED
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			ENDIF
		ENDIF
	ENDIF	
	
	IF missionStage = MS_DOG_RUNS_AWAY OR missionStage = MS_DOG_RUNS_ON_ROAD OR missionStage = MS_DOG_WAITING_AT_FIRESTATION
		IF IS_PED_UNINJURED(CELEBRITY_DOG)
		and IS_PED_UNINJURED(PLAYER_PED_ID())
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, fChaseFailDistance)
				failReason = FAIL_LOST_DOG
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			ELSE
				IF bBEEN_WARNED_ABOUT_DISTANCE = FALSE
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, WARN_DIST)
						/*CLEAR_PRINTS()
						PRINT_NOW("N1C_WRN_DOG", DEFAULT_GOD_TEXT_TIME, 1)	// Catch up with ~b~Mr. Muffy Cakes.*/
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY)
						bBEEN_WARNED_ABOUT_DISTANCE = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ===========================================================================================================
//		HELPER FUNCTIONS
// ===========================================================================================================

/// PURPOSE:
///    Checks if all the vehicle recording cars and their drivers are ok
/// RETURNS:
///    TRUE if they all exist and are OK, FALSE otherwise
FUNC BOOL ALL_CARS_ARE_WORKING()
	IF IS_VEHICLE_OK(CRASH_CAR_POSH.index) AND IS_PED_UNINJURED(CRASH_CAR_POSH.driver) AND IS_VEHICLE_OK(CRASH_CAR_FAMILY.index) AND IS_PED_UNINJURED(CRASH_CAR_FAMILY.driver)
	AND IS_VEHICLE_OK(SKID_CAR_TAXI.index) AND IS_PED_UNINJURED(SKID_CAR_TAXI.driver)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds the closest waypoint recording waypoint to the dog when on the road
/// RETURNS:
///    The closest waypoint recording waypoint to the dog when on the road
FUNC INT GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE()
	INT wayP
	WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(dogRoute, GET_ENTITY_COORDS(CELEBRITY_DOG), wayP)
	RETURN wayP
ENDFUNC

/// PURPOSE:
///    Finds the closest waypoint recording waypoint to the player
/// RETURNS:
///    The closest waypoint recording waypoint to the player
FUNC INT GET_THE_CLOSEST_WAYPOINT_TO_PLAYER()
	INT wayP
	WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(dogRoute, GET_ENTITY_COORDS(PLAYER_PED_ID()), wayP)
	RETURN wayP
ENDFUNC

/// PURPOSE:
///    Works out the position for the camera when the player tries to pick up the dog
/// RETURNS:
///    The position for the camera when the player tries to pick up the dog
FUNC VECTOR get_cam_pos()	
	VECTOR pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-0.8, -1.2, 1.5>>)
	RETURN pos
ENDFUNC

/// PURPOSE:
///    Checks if it's ok to display subtitles for a conversation
/// RETURNS:
///    The appropriate subtitles state, based on whether or not an objective is being displayed
FUNC enumSubtitlesState CheckDisplaySubtitles()

	IF IS_MESSAGE_BEING_DISPLAYED()
		RETURN DO_NOT_DISPLAY_SUBTITLES
	ELSE
		RETURN DISPLAY_SUBTITLES
	ENDIF
ENDFUNC

/// PURPOSE:
///    Loads the vehicle recordings, models and anim dictionaries used by the mission
PROC LOAD_OTHER_ASSETS()
		
	REQUEST_VEHICLE_RECORDING(iNumRecStraightCrashCar, recCrashCars)
	REQUEST_VEHICLE_RECORDING(iNumRecSwerveCrashCar, recCrashCars)
	REQUEST_VEHICLE_RECORDING(001, "NIG1C_COMET")
	REQUEST_VEHICLE_RECORDING(002, "NIG1C_COMET2")
	REQUEST_VEHICLE_RECORDING(001, "NIG1CEscort")
	REQUEST_VEHICLE_RECORDING(iNumRecBrakeCar, recBrakeCar)
	REQUEST_VEHICLE_RECORDING(001, "NIG1C_FIRE")
	REQUEST_WAYPOINT_RECORDING(dogRoute)
	REQUEST_MODEL(taxiModel)
	REQUEST_MODEL(sentinelCarModel)
	REQUEST_MODEL(familyCarModel)
	REQUEST_MODEL(escortCarModel)
	REQUEST_MODEL(poshCarModel)
	REQUEST_MODEL(jeepCarModel)
	REQUEST_MODEL(femaleHipsterModel)
	REQUEST_MODEL(businessManModel)
	REQUEST_MODEL(FIRETRUK)
	//REQUEST_MODEL(fireManModel)
	REQUEST_MODEL(taxiDriverModel)
	REQUEST_ANIM_DICT(NIGEL1C_DICT)
	/*REQUEST_ANIM_DICT(KNEEL_ENTER_DICT)
	REQUEST_ANIM_DICT(KNEEL_DICT)
	REQUEST_ANIM_DICT(KNEEL_EXIT_DICT)*/
	REQUEST_ANIM_DICT(OUTRO_DICT)
	REQUEST_ANIM_DICT(MILAN_DICT)
	//REQUEST_ANIM_DICT(TAXI_HAIL_DICT)
	REQUEST_ANIM_DICT(DOG_DICT)
	
	REQUEST_SCRIPT_AUDIO_BANK("NIGEL_1C_COLLAR")
	
ENDPROC

/// PURPOSE:
///    Checks if all the assets are loaded
/// RETURNS:
///    TRUE if all assets are loaded, FALSE otherwise
FUNC BOOL ARE_ASSETS_LOADED()
	IF NOT HAS_MODEL_LOADED(taxiModel)
		CPRINTLN(DEBUG_MISSION, "Taxi not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(sentinelCarModel)
		CPRINTLN(DEBUG_MISSION, "Sentinel not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(familyCarModel)
		CPRINTLN(DEBUG_MISSION, "Family car not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(escortCarModel)
		CPRINTLN(DEBUG_MISSION, "escort car not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(poshCarModel)
		CPRINTLN(DEBUG_MISSION, "posh car not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(jeepCarModel)
		CPRINTLN(DEBUG_MISSION, "jeep not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(femaleHipsterModel)
		CPRINTLN(DEBUG_MISSION, "female hipster not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(businessManModel)
		CPRINTLN(DEBUG_MISSION, "business man not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(FIRETRUK)
		CPRINTLN(DEBUG_MISSION, "firetruck not loaded")
		RETURN FALSE
	ENDIF
	/*IF NOT HAS_MODEL_LOADED(fireManModel)
		CPRINTLN(DEBUG_MISSION, "fireman not loaded")
		RETURN FALSE
	ENDIF*/
	IF NOT HAS_MODEL_LOADED(taxiDriverModel)
		CPRINTLN(DEBUG_MISSION, "taxi driver not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED(NIGEL1C_DICT)
		CPRINTLN(DEBUG_MISSION, "Main anim dict not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED(MILAN_DICT)
		CPRINTLN(DEBUG_MISSION, "Milan anim dict not loaded")
		RETURN FALSE
	ENDIF
	IF NOT HAS_ANIM_DICT_LOADED(DOG_DICT)
		CPRINTLN(DEBUG_MISSION, "Dog anim dict not loaded")
		RETURN FALSE
	ENDIF
	
	SET_PED_MODEL_IS_SUPPRESSED(femaleHipsterModel, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(businessManModel, TRUE)
	//SET_PED_MODEL_IS_SUPPRESSED(fireManModel, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(taxiDriverModel, TRUE)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Waits until all assets are loaded
PROC WAIT_TILL_ASSETS_ARE_LOADED()
	
	WHILE NOT ARE_ASSETS_LOADED()
		CPRINTLN(DEBUG_MISSION, "Loading other assets")
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Defines the base variables for the shockable peds
PROC SETUP_SHOCKABLE_PEDS()

	shockedPed[0].model = femaleHipsterModel
	shockedPed[0].fHeading = 65.71//-170.53
	shockedPed[0].vPosStart = <<-645.97, -177.03, 37>>
	shockedPed[0].vPosMid = <<-643.84, -181.83, 36.69>>
	shockedPed[0].vPosEnd = <<-691.20, -207.27, 36.25>>
	shockedPed[0].startWalkingWaypoint = 21
	shockedPed[0].startedWalking = FALSE
	shockedPed[0].beenShocked = FALSE
	shockedPed[0].anim = "avoid_rf"	
	
	shockedPed[1].model = femaleHipsterModel
	shockedPed[1].fHeading = -50.00//-151.41
	shockedPed[1].vPosStart = <<-643.92, -135.89, 37.84>>//<<-641.40, -134.22, 37>>
	shockedPed[1].vPosMid = <<-634.06, -147.80, 36.87>>
	shockedPed[1].vPosEnd = <<-623.98, -162.83, 36.85>>
	shockedPed[1].startWalkingWaypoint = 28
	shockedPed[1].startedWalking = FALSE
	shockedPed[1].beenShocked = FALSE		
	shockedPed[1].anim = "avoid_fr"	
	
	shockedPed[2].model = businessManModel
	shockedPed[2].fHeading = 5.73
	shockedPed[2].vPosStart = <<-671.58, -195.03, 37.39>>
	shockedPed[2].beenShocked = FALSE	
	
	shockedPed[4].model = businessManModel
	shockedPed[4].fHeading = -108.48
	shockedPed[4].vPosStart = <<-632.18, -154.89, 37.83>>
	shockedPed[4].startedWalking = FALSE
ENDPROC

/// PURPOSE:
///    Checks if the assets needed at the start of the mission are loaded
/// RETURNS:
///    TRUE if the assets are loaded, FALSE otherwise
FUNC BOOL ARE_INITIAL_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(femaleHipsterModel)
	AND HAS_MODEL_LOADED(dogModel)
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)	// B*2109500 - script needs to wait for mission text slot to load
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loads the assets needed at the start of the mission
/// PARAMS:
///    bWaitForLoading - TRUE if the procedure should wait until the assets are loaded, FALSE otherwise
PROC LOAD_INITIAL_ASSETS(BOOL bWaitForLoading = FALSE)

	REQUEST_ADDITIONAL_TEXT("NIG1CAU", MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("NIGEL1C", MISSION_TEXT_SLOT)
	
	REQUEST_MODEL(femaleHipsterModel)
	REQUEST_MODEL(dogModel)
	CPRINTLN(DEBUG_MISSION, "Requested model and dog and text")
	IF bWaitForLoading
		WHILE NOT ARE_INITIAL_ASSETS_LOADED()
			CPRINTLN(DEBUG_MISSION, "LOADING CELEB MODELS")
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the model and dog for the player to meet
PROC CREATE_MODEL_AND_DOG_AT_SHOPS()
		
	IF DOES_ENTITY_EXIST(CELEBRITY_DOG)
		DELETE_PED(CELEBRITY_DOG)
	ENDIF
	IF DOES_ENTITY_EXIST(CELEBRITY_PED)
		DELETE_PED(CELEBRITY_PED)
	ENDIF
	
	eInitialSceneStage = IS_REQUEST_SCENE
	WHILE NOT SetupScene_NIGEL_1C(sRCLauncherDataLocal)
		WAIT(0)
	ENDWHILE
	
	CELEBRITY_PED = sRCLauncherDataLocal.pedID[0]
	CELEBRITY_DOG = sRCLauncherDataLocal.pedID[1]
	
	SET_ENTITY_COORDS_GROUNDED(CELEBRITY_PED, vPos_Celeb)
	//SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_DOG, relGroupFriendly)	
ENDPROC

/// PURPOSE:
///    Creates one of the shockable peds
/// PARAMS:
///    i - the index of the ped to create
PROC CREATE_SHOCKABLE_PED(int i)
	
	IF DOES_ENTITY_EXIST(shockedPed[i].index)
		DELETE_PED(shockedPed[i].index)
	ENDIF
	
	IF HAS_MODEL_LOADED(shockedPed[i].model)
		shockedPed[i].index = CREATE_PED(PEDTYPE_CIVFEMALE, shockedPed[i].model, shockedPed[i].vPosStart, shockedPed[i].fHeading)	
		
		VECTOR pos = shockedPed[i].vPosStart
		FLOAT zCoord
		GET_GROUND_Z_FOR_3D_COORD(pos, zCoord)
		SET_ENTITY_COORDS(shockedPed[i].index, <<pos.x, pos.y, zCoord>>)
		SET_PED_RELATIONSHIP_GROUP_HASH(shockedPed[i].index, relGroupDrivers)
		SET_PED_COMBAT_ATTRIBUTES(shockedPed[i].index, CA_ALWAYS_FLEE, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the model shout at the player when she stops running
PROC MODEL_SPEAKS_WHEN_STOPPING()
	IF IS_PED_UNINJURED(CELEBRITY_PED) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(CELEBRITY_PED, vModelDest) < 5.0 AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vModelDest) < 25.0
		IF bDoStopRunningConv
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_2",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
				bDoStopRunningConv = FALSE
				iWaitLineTimer = GET_GAME_TIMER() + WAIT_LINE_TIME
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, -1)
			ENDIF
		ELSE
			IF GET_GAME_TIMER() > iWaitLineTimer AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_WAIT",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
					iWaitLineTimer = GET_GAME_TIMER() + WAIT_LINE_TIME
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the shockable peds
/// PARAMS:
///    i - the index of the shockable ped to be controlled
PROC UPDATE_THE_SHOCKABLE_PED(INT i)
	IF NOT shockedPed[i].beenShocked
		IF IS_PED_UNINJURED(shockedPed[i].index)
		AND IS_PED_UNINJURED(CELEBRITY_DOG)
			IF NOT shockedPed[i].startedWalking
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
					IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > shockedPed[i].startWalkingWaypoint
						open_sequence_task(seq)
							IF runSpeed = dogRunsVerySlow
								TASK_PAUSE(NULL, 1000)
							ENDIF
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, shockedPed[i].vPosMid, 1)
						close_sequence_task(seq)
						task_perform_sequence(shockedPed[i].index, seq)
						clear_sequence_task(seq)
						shockedPed[i].startedWalking = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_IN_RANGE_ENTITY(shockedPed[i].index, CELEBRITY_DOG, 3)
					VECTOR vec
					vec = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(shockedPed[i].index, GET_ENTITY_COORDS(CELEBRITY_DOG))
					CLEAR_PED_TASKS(shockedPed[i].index)
					open_sequence_task(seq)
						TASK_LOOK_AT_ENTITY(null, CELEBRITY_DOG, -1)
						if vec.y > 0.7
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, shockedPed[i].anim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)	
						ELSE
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "avoid_rb", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
						ENDIF
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, CELEBRITY_DOG)
						TASK_PAUSE(NULL, 1000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, shockedPed[i].vPosEnd, 1)
					close_sequence_task(seq)
					task_perform_sequence(shockedPed[i].index, seq)
					clear_sequence_task(seq)
					shockedPed[i].beenShocked = TRUE
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the ped who falls over
PROC UPDATE_THE_FALL_PED()
		IF IS_PED_UNINJURED(shockedPed[4].index)	
		AND IS_PED_UNINJURED(CELEBRITY_DOG)
			IF shockedPed[4].startedWalking = FALSE
				IF IS_ENTITY_AT_ENTITY(shockedPed[4].index, CELEBRITY_DOG, <<2.5,2.5,2>>)
					CLEAR_PED_TASKS(shockedPed[4].index)
					SET_PED_TO_RAGDOLL_WITH_FALL(shockedPed[4].index, 1500, 2000, TYPE_OVER_WALL, -GET_ENTITY_FORWARD_VECTOR(shockedPed[4].index), 1, <<0,0,0>>, <<0,0,0>>)
					open_sequence_task(seq)
						TASK_LOOK_AT_ENTITY(null, CELEBRITY_DOG, -1)		
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-631.84, -153.48, 36.83>>, 1, DEFAULT_TIME_BEFORE_WARP)						
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, CELEBRITY_DOG)	
						TASK_PAUSE(NULL, 1500)
						TASK_WANDER_STANDARD(NULL)
					close_sequence_task(seq)
					task_perform_sequence(shockedPed[4].index, seq)
					clear_sequence_task(seq)
					shockedPed[4].startedWalking = TRUE
				ENDIF	
			ENDIF
		ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the vehicle and ped held in a MISSION_VEHICLE struct are alive
/// PARAMS:
///    vehicle - the MISSION_VEHICLE we're checking
/// RETURNS:
///    TRUE if both vehicle and ped index are OK
FUNC BOOL CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(MISSION_VEHICLE vehicle)
	IF IS_PED_UNINJURED(vehicle.driver) AND IS_VEHICLE_OK(vehicle.index)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initialises the values for the mission vehicles
PROC SETUP_MISSION_VEHICLES()

	CRASH_CAR_POSH.carModel = poshCarModel
	CRASH_CAR_POSH.startPosition = <<-655.81, -278.73, 35.21>>
	CRASH_CAR_POSH.startHeading = 35
	CRASH_CAR_POSH.driverModel = femaleHipsterModel
	
	CRASH_CAR_FAMILY.carModel = familyCarModel
	CRASH_CAR_FAMILY.startPosition = <<-591.7191, -166.3491, 37.8459>>
	CRASH_CAR_FAMILY.startHeading = 35
	CRASH_CAR_FAMILY.driverModel = businessManModel
		
	SKID_CAR_TAXI.carModel = taxiModel
	SKID_CAR_TAXI.startPosition = <<-651.40, -153.68, 36.95>>
	SKID_CAR_TAXI.startHeading = 217.44
	SKID_CAR_TAXI.driverModel = taxiDriverModel
	
	// This will probably go back in so just commenting it out. B* 1107526
	/*FIRE_TRUCK.carModel = FIRETRUK
	FIRE_TRUCK.startPosition = <<-635.68, -105.39, 38.14>>
	FIRE_TRUCK.startHeading = 87.14
	FIRE_TRUCK.driverModel = fireManModel*/
	
	SKID_CAR_SENTINEL.carModel = sentinelCarModel
	SKID_CAR_SENTINEL.startPosition = <<-525.33, -137.35, 38.04>>
	SKID_CAR_SENTINEL.startHeading = 111.20
	SKID_CAR_SENTINEL.driverModel = businessManModel
	
	/*ESCORT_CAR_FAMILY.carModel = escortCarModel
	ESCORT_CAR_FAMILY.startPosition = <<-519.58, -134.52, 38.65>>
	ESCORT_CAR_FAMILY.startHeading = 111.20
	ESCORT_CAR_FAMILY.driverModel = businessManModel*/
	
	FIRST_CAR.carModel = familyCarModel
	FIRST_CAR.startPosition = <<-683.30, -238.03, 36.63>> 
	FIRST_CAR.startHeading = 29.79
	FIRST_CAR.driverModel = taxiDriverModel
	
	SECOND_CAR.carModel = taxiModel
	SECOND_CAR.startPosition = <<-674.44, -262.95, 35.97>> 
	SECOND_CAR.startHeading = 29.80
	SECOND_CAR.driverModel = taxiDriverModel
	
	TAXI_CAR.carModel = taxiModel
	TAXI_CAR.startPosition = <<-644.56, -155.68, 37.32>> 
	TAXI_CAR.startHeading = 208.31
	TAXI_CAR.driverModel = taxiDriverModel
	
	FINAL_CAR.carModel = familyCarModel
	FINAL_CAR.startPosition = <<-641.91, -158.94, 36.70>>
	FINAL_CAR.startHeading = 208.7
	FINAL_CAR.driverModel = taxiDriverModel
ENDPROC

/// PURPOSE:
///    Creates the scripted traffic and the drivers for the vehicles
FUNC BOOL CREATE_TRAFFIC_VEHICLES()
	
	//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) //OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vShoppingAreaA, vShoppingAreaB, fShoppingArea)
	IF IS_PED_ON_FOOT(PLAYER_PED_ID())
		IF NOT DOES_ENTITY_EXIST(CRASH_CAR_POSH.index)
			IF HAS_MODEL_LOADED(CRASH_CAR_POSH.carModel) AND HAS_MODEL_LOADED(CRASH_CAR_POSH.driverModel)
				CRASH_CAR_POSH.index = CREATE_VEHICLE(CRASH_CAR_POSH.carModel, CRASH_CAR_POSH.startPosition, CRASH_CAR_POSH.startHeading)
				CRASH_CAR_POSH.driver = CREATE_PED_INSIDE_VEHICLE(CRASH_CAR_POSH.index, PEDTYPE_CIVMALE, CRASH_CAR_POSH.driverModel)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(CRASH_CAR_POSH.index, TRUE)
				SET_VEHICLE_DOORS_LOCKED(CRASH_CAR_POSH.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SET_PED_MONEY(CRASH_CAR_POSH.driver, 0)
				SET_PED_COMBAT_ATTRIBUTES(CRASH_CAR_POSH.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(CRASH_CAR_POSH.driver, relGroupDrivers)
				SET_PED_CONFIG_FLAG(CRASH_CAR_POSH.driver, PCF_FallsOutOfVehicleWhenKilled, TRUE)
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(CRASH_CAR_FAMILY.index)
			IF HAS_MODEL_LOADED(familyCarModel) AND HAS_MODEL_LOADED(businessManModel)
				CRASH_CAR_FAMILY.index = CREATE_VEHICLE(familyCarModel, <<-591.7191, -166.3491, 37.8459>>, 35)
				CRASH_CAR_FAMILY.driver = CREATE_PED_INSIDE_VEHICLE(CRASH_CAR_FAMILY.index, PEDTYPE_CIVFEMALE, businessManModel)
				SET_PED_CONFIG_FLAG(CRASH_CAR_FAMILY.driver, PCF_WillFlyThroughWindscreen, TRUE)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(CRASH_CAR_FAMILY.index, TRUE)
				SET_VEHICLE_DOORS_LOCKED(CRASH_CAR_FAMILY.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SET_PED_COMBAT_ATTRIBUTES(CRASH_CAR_FAMILY.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(CRASH_CAR_FAMILY.driver, relGroupDrivers)
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(SKID_CAR_TAXI.index)
			IF HAS_MODEL_LOADED(taxiModel) AND HAS_MODEL_LOADED(taxiDriverModel)
				SKID_CAR_TAXI.index = CREATE_VEHICLE(taxiModel, <<-651.40, -153.68, 36.95>>, 217.44)
				SKID_CAR_TAXI.driver = CREATE_PED_INSIDE_VEHICLE(SKID_CAR_TAXI.index, PEDTYPE_CIVMALE, taxiDriverModel)
				TAXI_PASSENGER_PED = CREATE_PED_INSIDE_VEHICLE(SKID_CAR_TAXI.index, PEDTYPE_CIVMALE, businessManModel, VS_BACK_RIGHT)
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(SKID_CAR_TAXI.index, TRUE)
				SET_VEHICLE_DOORS_LOCKED(SKID_CAR_TAXI.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)	
				SET_PED_COMBAT_ATTRIBUTES(SKID_CAR_TAXI.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(TAXI_PASSENGER_PED, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(SKID_CAR_TAXI.driver, relGroupDrivers)
				SET_PED_RELATIONSHIP_GROUP_HASH(TAXI_PASSENGER_PED, relGroupDrivers)
			ENDIF
			RETURN FALSE
		ENDIF
		IF NOT DOES_ENTITY_EXIST(SKID_CAR_SENTINEL.index)
			IF HAS_MODEL_LOADED(sentinelCarModel) AND HAS_MODEL_LOADED(businessManModel)
				SKID_CAR_SENTINEL.index = CREATE_VEHICLE(sentinelCarModel, <<-525.33, -137.35, 38.04>>, 111.20)
				SET_VEHICLE_ON_GROUND_PROPERLY(SKID_CAR_SENTINEL.index)
				SET_VEHICLE_DOORS_LOCKED(SKID_CAR_SENTINEL.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				IF NOT IS_PED_UNINJURED(SKID_CAR_SENTINEL.driver)
					SKID_CAR_SENTINEL.driver = CREATE_PED_INSIDE_VEHICLE(SKID_CAR_SENTINEL.index, PEDTYPE_CIVMALE, businessManModel)
					SET_PED_COMBAT_ATTRIBUTES(SKID_CAR_SENTINEL.driver, CA_ALWAYS_FLEE, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(SKID_CAR_SENTINEL.driver, relGroupDrivers)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		/*IF NOT DOES_ENTITY_EXIST(ESCORT_CAR_FAMILY.index)
			IF HAS_MODEL_LOADED(escortCarModel) AND HAS_MODEL_LOADED(businessManModel)
				ESCORT_CAR_FAMILY.index = CREATE_VEHICLE(escortCarModel, <<-519.58, -134.52, 38.65>>, 111.20)
				SET_VEHICLE_ON_GROUND_PROPERLY(ESCORT_CAR_FAMILY.index)
				SET_VEHICLE_DOORS_LOCKED(ESCORT_CAR_FAMILY.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				IF NOT IS_PED_UNINJURED(ESCORT_CAR_FAMILY.driver)
					ESCORT_CAR_FAMILY.driver = CREATE_PED_INSIDE_VEHICLE(ESCORT_CAR_FAMILY.index, PEDTYPE_CIVMALE, businessManModel)
					SET_PED_COMBAT_ATTRIBUTES(ESCORT_CAR_FAMILY.driver, CA_ALWAYS_FLEE, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(ESCORT_CAR_FAMILY.driver, relGroupDrivers)
				ENDIF
			ENDIF
		ENDIF*/

		IF NOT DOES_ENTITY_EXIST(TAXI_CAR.index)
			IF HAS_MODEL_LOADED(taxiModel) AND HAS_MODEL_LOADED(taxiDriverModel)
				TAXI_CAR.index = CREATE_VEHICLE(taxiModel, TAXI_CAR.startPosition, TAXI_CAR.startHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(TAXI_CAR.index)
				SET_VEHICLE_DOORS_LOCKED(TAXI_CAR.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				IF NOT IS_PED_UNINJURED(TAXI_CAR.driver)
					TAXI_CAR.driver = CREATE_PED_INSIDE_VEHICLE(TAXI_CAR.index, PEDTYPE_CIVMALE, taxiDriverModel)
					SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FLEE, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(TAXI_CAR.driver, relGroupDrivers)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(SKID_CAR_JEEP.index)
			IF HAS_MODEL_LOADED(jeepCarModel) AND HAS_MODEL_LOADED(businessManModel)
				SKID_CAR_JEEP.index = CREATE_VEHICLE(jeepCarModel, <<-584.32, -156.66, 36.95>>, 117.29)
				SET_VEHICLE_ON_GROUND_PROPERLY(SKID_CAR_JEEP.index)
				SET_VEHICLE_DOORS_LOCKED(SKID_CAR_JEEP.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SKID_CAR_JEEP.driver = CREATE_PED_INSIDE_VEHICLE(SKID_CAR_JEEP.index, PEDTYPE_CIVMALE, businessManModel)
				SET_PED_COMBAT_ATTRIBUTES(SKID_CAR_JEEP.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(SKID_CAR_JEEP.driver, relGroupDrivers)
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Always create these vehicles
	IF NOT DOES_ENTITY_EXIST(FIRST_CAR.index)
		IF HAS_MODEL_LOADED(FIRST_CAR.carModel) AND HAS_MODEL_LOADED(taxiDriverModel)
			FIRST_CAR.index = CREATE_VEHICLE(escortCarModel, <<-683.30, -238.03, 36.63>>, 29.79)
			SET_VEHICLE_ON_GROUND_PROPERLY(FIRST_CAR.index)
			SET_VEHICLE_DOORS_LOCKED(FIRST_CAR.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			IF NOT IS_PED_UNINJURED(FIRST_CAR.driver)
				FIRST_CAR.driver = CREATE_PED_INSIDE_VEHICLE(FIRST_CAR.index, PEDTYPE_CIVMALE, taxiDriverModel)
				SET_PED_COMBAT_ATTRIBUTES(FIRST_CAR.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(FIRST_CAR.driver, relGroupDrivers)
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(SECOND_CAR.index)
		IF HAS_MODEL_LOADED(SECOND_CAR.carModel) AND HAS_MODEL_LOADED(SECOND_CAR.driverModel)
			SECOND_CAR.index = CREATE_VEHICLE(SECOND_CAR.carModel, SECOND_CAR.startPosition, SECOND_CAR.startHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(SECOND_CAR.index)
			SET_VEHICLE_DOORS_LOCKED(SECOND_CAR.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			IF NOT IS_PED_UNINJURED(SECOND_CAR.driver)
				SECOND_CAR.driver = CREATE_PED_INSIDE_VEHICLE(SECOND_CAR.index, PEDTYPE_CIVMALE, SECOND_CAR.driverModel)
				SET_PED_COMBAT_ATTRIBUTES(SECOND_CAR.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(SECOND_CAR.driver, relGroupDrivers)
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(FINAL_CAR.index)
		IF HAS_MODEL_LOADED(familyCarModel) AND HAS_MODEL_LOADED(taxiDriverModel)
			FINAL_CAR.index = CREATE_VEHICLE(familyCarModel, FINAL_CAR.startPosition, FINAL_CAR.startHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(FINAL_CAR.index)
			SET_VEHICLE_DOORS_LOCKED(FINAL_CAR.index, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			IF NOT IS_PED_UNINJURED(FINAL_CAR.driver)
				FINAL_CAR.driver = CREATE_PED_INSIDE_VEHICLE(FINAL_CAR.index, PEDTYPE_CIVMALE, taxiDriverModel)
				SET_PED_COMBAT_ATTRIBUTES(FINAL_CAR.driver, CA_ALWAYS_FLEE, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(FINAL_CAR.driver, relGroupDrivers)
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	// This will probably go back in so just commenting it out. B* 1107526
	/*IF NOT DOES_ENTITY_EXIST(FIRE_TRUCK.index)
		FIRE_TRUCK.index = CREATE_VEHICLE(FIRETRUK, <<-635.68, -105.39, 38.14>>, 87.14)
		SET_VEHICLE_ON_GROUND_PROPERLY(FIRE_TRUCK.index)	
		IF NOT IS_PED_UNINJURED(FIRE_TRUCK.driver)
			FIRE_TRUCK.driver = CREATE_PED_INSIDE_VEHICLE(FIRE_TRUCK.index, PEDTYPE_FIRE, fireManModel)
			SET_PED_COMBAT_ATTRIBUTES(FIRE_TRUCK.driver, CA_ALWAYS_FLEE, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(FIRE_TRUCK.driver, relGroupDrivers)
		ENDIF
	ENDIF*/
	IF NOT DOES_ENTITY_EXIST(STATIONARY_FIRE_TRUCK)
		IF HAS_MODEL_LOADED(FIRETRUK)
			STATIONARY_FIRE_TRUCK = CREATE_VEHICLE(FIRETRUK, <<-637.88, -112.82, 38.10>>, 83.08)
			SET_VEHICLE_ON_GROUND_PROPERLY(STATIONARY_FIRE_TRUCK)	
		ENDIF
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the player's current weapon
/// RETURNS:
///    The player's current weapon
FUNC WEAPON_TYPE CURRENT_PLAYER_WEAPON()
	WEAPON_TYPE wep
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wep)
	RETURN wep
ENDFUNC

/// PURPOSE:
///    Controls the drivers of the cars involved in the crash
PROC UPDATE_THE_PEDS_AFTER_CRASH()

	IF IS_PED_UNINJURED(TAXI_CAR.driver)
	AND IS_ENTITY_ALIVE(TAXI_CAR.index)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
		SWITCH taxiDriverState	
			CASE taxiDriver_lookingAtCarCrash
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CRASH_CAR_FAMILY.index)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CRASH_CAR_POSH.index)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TAXI_CAR.index)
					CLEAR_PED_TASKS(TAXI_CAR.driver)
					IF CURRENT_PLAYER_WEAPON() = WEAPONTYPE_UNARMED
						SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FIGHT, TRUE)
					ENDIF
					TASK_COMBAT_PED(TAXI_CAR.driver, PLAYER_PED_ID())
					taxiDriverState = taxiDriver_attackingPlayer
				ENDIF
				
				IF IS_PED_FLEEING(TAXI_CAR.driver)
					taxiDriverState = taxiDriver_fleeing
				ENDIF
			BREAK
			CASE taxiDriver_attackingPlayer
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TAXI_CAR.index)
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(TAXI_CAR.driver, TAXI_CAR.index, 20)
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), TAXI_CAR.index, 20)
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), TAXI_CAR.driver, 10)
					AND NOT IS_PED_FLEEING(TAXI_CAR.driver)
						CLEAR_PED_TASKS(TAXI_CAR.driver)					
						TASK_TURN_PED_TO_FACE_ENTITY(TAXI_CAR.driver, PLAYER_PED_ID(), -1)						
						SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FLEE, TRUE)
						taxiDriverState = taxiDriver_watchingPlayer
					ENDIF
				ENDIF
				
				IF IS_PED_FLEEING(TAXI_CAR.driver)
					taxiDriverState = taxiDriver_fleeing
				ENDIF
				
				if HAS_PLAYER_THREATENED_PED(TAXI_CAR.driver, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FIGHT, FALSE)					
					SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FLEE, TRUE)
					CLEAR_PED_TASKS(TAXI_CAR.driver)	
					TASK_SMART_FLEE_PED(TAXI_CAR.driver, PLAYER_PED_ID(), 60, -1)
					taxiDriverState = taxiDriver_fleeing
				ENDIF
			BREAK
			CASE taxiDriver_watchingPlayer
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TAXI_CAR.driver) < 8
					SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FIGHT, TRUE)
					TASK_COMBAT_PED(TAXI_CAR.driver, PLAYER_PED_ID())
					taxiDriverState = taxiDriver_attackingPlayer
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TAXI_CAR.driver) > 20
					open_sequence_task(seq)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1000)
						TASK_ENTER_VEHICLE(NULL, TAXI_CAR.index, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, 1.0)
					close_sequence_task(seq)
					task_perform_sequence(TAXI_CAR.driver, seq)
					clear_sequence_task(seq)
					taxiDriverState = taxiDriver_returnToCrash
				ENDIF
				
				IF IS_PED_FLEEING(TAXI_CAR.driver)
					taxiDriverState = taxiDriver_fleeing
				ENDIF
			BREAK
			CASE taxiDriver_returnToCrash
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), TAXI_CAR.driver, 8)
					SET_PED_COMBAT_ATTRIBUTES(TAXI_CAR.driver, CA_ALWAYS_FIGHT, TRUE)
					TASK_COMBAT_PED(TAXI_CAR.driver, PLAYER_PED_ID())
					taxiDriverState = taxiDriver_attackingPlayer
				ENDIF	
				
				IF IS_PED_FLEEING(TAXI_CAR.driver)
					taxiDriverState = taxiDriver_fleeing
				ENDIF		
			BREAK	
		ENDSWITCH
	ENDIF
	
	IF IS_PED_UNINJURED(CRASH_CAR_POSH.driver)
		SWITCH femaleDriverState	
			CASE femaleDriver_lookingAtCarCrash
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CRASH_CAR_FAMILY.index)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CRASH_CAR_POSH.index)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), TAXI_CAR.index)
					CLEAR_PED_TASKS(CRASH_CAR_POSH.driver)	
					TASK_TURN_PED_TO_FACE_ENTITY(CRASH_CAR_POSH.driver, PLAYER_PED_ID(), 0)
					femaleDriverState = femaleDriver_cower
				ENDIF
			BREAK
			CASE femaleDriver_cower
			
			BREAK
			CASE femaleDriver_flee
				
			BREAK	
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Causes the crash vehicles to crash and makes their drivers react appropriately
PROC UPDATE_THE_CRASH()
	
	IF crashStage <> CS_FINISHED
		IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(CRASH_CAR_FAMILY)	
		AND CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(CRASH_CAR_POSH)	
		AND CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(TAXI_CAR)
			IF bCAR_HONKING = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(CELEBRITY_DOG, <<-662.768799,-208.317108,35.493214>>, <<-674.628906,-214.651871,38.805908>>, 6.500000)											
					START_VEHICLE_HORN(CRASH_CAR_POSH.index, 1500)
					TASK_LOOK_AT_ENTITY(CRASH_CAR_POSH.driver, CELEBRITY_DOG, 5000)
					IF IS_PED_UNINJURED(shockedPed[2].index)	
						IF IS_PED_USING_ANY_SCENARIO(shockedPed[2].index)
							SET_PED_PANIC_EXIT_SCENARIO(shockedPed[2].index, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						ENDIF
						open_sequence_task(seq)
							TASK_PAUSE(NULL, 500)
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "reaction_backward_big_intro_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "react_big_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)	
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "exit_to_generic", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
						close_sequence_task(seq)
						task_perform_sequence(shockedPed[2].index, seq)
						clear_sequence_task(seq)
					ENDIF
							
					bCAR_HONKING = TRUE
				ENDIF
			ENDIF
		
			
			SWITCH crashStage
				CASE CS_WAITING
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
						IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 8
							CPRINTLN(DEBUG_MISSION, "CAR_REC_SHOULD BE KICKING OFF")
							crashStage = CS_SET_TO_DRIVE
						ENDIF
					ENDIF
				BREAK
				CASE CS_SET_TO_DRIVE
					START_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_POSH.index, iNumRecBrakeCar, recBrakeCar)							
					START_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_FAMILY.index, iNumRecStraightCrashCar, recCrashCars)	
					START_PLAYBACK_RECORDED_VEHICLE(TAXI_CAR.index, iNumRecSwerveCrashCar, recCrashCars)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_POSH.index, 2000)//3000)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_FAMILY.index, 2650)//3650)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(TAXI_CAR.index, 6000)//7000)
					
					crashStage = CS_DRIVING
				BREAK
				CASE CS_DRIVING
					
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(CRASH_CAR_FAMILY.index)		
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TAXI_CAR.index)
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(CRASH_CAR_POSH.index)
						
							SET_PLAYBACK_SPEED(CRASH_CAR_POSH.index, 0.7)	
							SET_PLAYBACK_SPEED(CRASH_CAR_FAMILY.index, 0.7)
							SET_PLAYBACK_SPEED(TAXI_CAR.index, 0.85)
							
							IF IS_ENTITY_IN_RANGE_ENTITY(CRASH_CAR_FAMILY.index, TAXI_CAR.index, 7)
								crashStage = CS_SET_TO_CRASH
							ENDIF
							
						ENDIF
				BREAK
				CASE CS_SET_TO_CRASH
							
					STOP_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_FAMILY.index)	
					REMOVE_VEHICLE_RECORDING(iNumRecStraightCrashCar, recCrashCars)		
					STOP_PLAYBACK_RECORDED_VEHICLE(TAXI_CAR.index)	
					REMOVE_VEHICLE_RECORDING(iNumRecSwerveCrashCar, recCrashCars)	
					STOP_PLAYBACK_RECORDED_VEHICLE(CRASH_CAR_POSH.index)	
					REMOVE_VEHICLE_RECORDING(iNumRecBrakeCar, recBrakeCar)
					TASK_VEHICLE_MISSION(TAXI_CAR.driver, TAXI_CAR.index, CRASH_CAR_FAMILY.index, MISSION_RAM, 20, DRIVINGMODE_PLOUGHTHROUGH, 0.5, 2)
					TASK_VEHICLE_MISSION(CRASH_CAR_FAMILY.driver, CRASH_CAR_FAMILY.index, TAXI_CAR.index, MISSION_RAM, 20, DRIVINGMODE_PLOUGHTHROUGH, 0.5, 2)
					
					START_VEHICLE_HORN(CRASH_CAR_FAMILY.index, 800)
					crashStage = CS_CRASHING
				BREAK
				CASE CS_CRASHING
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(CRASH_CAR_FAMILY.index)	
						open_sequence_task(seq)						
							TASK_VEHICLE_TEMP_ACTION(NULL, TAXI_CAR.index, TEMPACT_HANDBRAKETURNRIGHT, 1000)
						close_sequence_task(seq)
						task_perform_sequence(TAXI_CAR.driver, seq)
						clear_sequence_task(seq)
						
						TASK_VEHICLE_TEMP_ACTION(CRASH_CAR_FAMILY.driver, CRASH_CAR_FAMILY.index, TEMPACT_HANDBRAKETURNLEFT, 1000)
						SMASH_VEHICLE_WINDOW(CRASH_CAR_FAMILY.index, SC_WINDOW_REAR_LEFT)
						SMASH_VEHICLE_WINDOW(CRASH_CAR_FAMILY.index, SC_WINDOW_fRONT_LEFT)
						
						bCRASH_SHOUTED = TRUE
						
						ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_CAR_PILE_UP, TAXI_CAR.index, -1)
										
						IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FINAL_CAR)
							TASK_VEHICLE_DRIVE_TO_COORD(FINAL_CAR.driver, FINAL_CAR.index, <<-567.85, -288.80, 34.08>>, 12, DRIVINGSTYLE_NORMAL, familyCarModel, DF_SteerAroundPeds | DF_SteerAroundStationaryCars, 5,5)
						ENDIF
						
						crashStage = CS_TASK_THEM_TO_EXIT
					ENDIF
				BREAK
				CASE CS_TASK_THEM_TO_EXIT
					IF GET_ENTITY_SPEED(CRASH_CAR_FAMILY.index) < 0.5
					AND GET_ENTITY_SPEED(TAXI_CAR.index) < 0.5
						open_sequence_task(seq)						
							TASK_PAUSE(NULL, 1000)
							TASK_LEAVE_VEHICLE(NULL, TAXI_CAR.index)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CRASH_CAR_FAMILY.driver, <<-1, 1, 0>>), 1.3, DEFAULT_TIME_BEFORE_WARP)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, CRASH_CAR_FAMILY.driver, 0)	
							//TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_SHOCKING_BIG_REACTION", 0, TRUE)
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "react_big_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)	
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "exit_to_generic", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
						close_sequence_task(seq)
						task_perform_sequence(TAXI_CAR.driver, seq)
						clear_sequence_task(seq)
						
						open_sequence_task(seq)		
							TASK_LEAVE_VEHICLE(NULL, CRASH_CAR_POSH.index)	
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, TAXI_CAR.index, 2500)		
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CRASH_CAR_FAMILY.driver, <<-1, 1, 0>>), 1.2, DEFAULT_TIME_BEFORE_WARP, 1)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, CRASH_CAR_FAMILY.driver, 0)	
							//TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_SHOCKING_BIG_REACTION", 0, TRUE)
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "react_big_variations_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)	
							TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "exit_to_generic", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
						close_sequence_task(seq)
						task_perform_sequence(CRASH_CAR_POSH.driver, seq)
						clear_sequence_task(seq)
						
						SET_ENTITY_HEALTH(CRASH_CAR_FAMILY.driver, 0)
						
						taxiDriverState	= taxiDriver_lookingAtCarCrash
						femaleDriverState = femaleDriver_lookingAtCarCrash
						
						crashStage = CS_FINISHED
					ENDIF
				BREAK
				CASE CS_FINISHED
				BREAK
			ENDSWITCH		
		ELIF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FINAL_CAR)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
				IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 19
					TASK_VEHICLE_DRIVE_TO_COORD(FINAL_CAR.driver, FINAL_CAR.index, <<-567.85, -288.80, 34.08>>, 12, DRIVINGSTYLE_NORMAL, familyCarModel, DF_SteerAroundPeds | DF_SteerAroundStationaryCars, 5,5)
					crashStage = CS_FINISHED
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
				
		IF IS_VEHICLE_OK(CRASH_CAR_FAMILY.index)
		AND IS_VEHICLE_OK(CRASH_CAR_POSH.index)
		AND IS_VEHICLE_OK(TAXI_CAR.index)
					
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(CRASH_CAR_FAMILY.index, TRUE)					
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(CRASH_CAR_POSH.index, TRUE)					
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(TAXI_CAR.index, TRUE)						
					
			UPDATE_THE_PEDS_AFTER_CRASH()
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the taxi skid to avoid the dog
PROC UPDATE_THE_SKIDDING_TAXI()
	IF bRELEASE_SPORTS_CAR = FALSE
		IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SKID_CAR_TAXI)	
		AND IS_PED_UNINJURED(CELEBRITY_DOG)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SKID_CAR_TAXI.index)
				SET_PLAYBACK_SPEED(SKID_CAR_TAXI.index, fCAR_REC_SPEED)	
				IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 32
				
					STOP_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_TAXI.index)
					REMOVE_VEHICLE_RECORDING(001, "NIG1C_COMET")						
					CLEAR_PED_TASKS(SKID_CAR_TAXI.driver)
					TASK_VEHICLE_DRIVE_WANDER(SKID_CAR_TAXI.driver, SKID_CAR_TAXI.index, 20, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
										
					bRELEASE_SPORTS_CAR = TRUE
				ENDIF
				IF bSPORTS_HONKING = FALSE
					IF IS_ENTITY_IN_RANGE_ENTITY(SKID_CAR_TAXI.index, CELEBRITY_DOG, 6)
						START_VEHICLE_HORN(SKID_CAR_TAXI.index, 3000)
						bSPORTS_HONKING = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
					IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 25			
						START_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_TAXI.index, 001, "NIG1C_COMET")		
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_TAXI.index, 2500)
						TASK_LOOK_AT_ENTITY(SKID_CAR_TAXI.driver, CELEBRITY_DOG, -1)
						IF IS_PED_UNINJURED(TAXI_PASSENGER_PED)
							TASK_LOOK_AT_ENTITY(TAXI_PASSENGER_PED, CELEBRITY_DOG, -1)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(SKID_CAR_TAXI.index, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the other cars react appropriately to the dog
PROC UPDATE_THE_OTHER_CAR()
	IF bRELEASE_SENTINEL_CAR = FALSE
		IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SKID_CAR_SENTINEL)
		AND IS_PED_UNINJURED(CELEBRITY_DOG)
		//AND CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(ESCORT_CAR_FAMILY)	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SKID_CAR_SENTINEL.index)
				SET_PLAYBACK_SPEED(SKID_CAR_SENTINEL.index, fCAR_REC_SPEED)	
				//SET_PLAYBACK_SPEED(ESCORT_CAR_FAMILY.index, fCAR_REC_SPEED)	
				IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 35
				
					STOP_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_SENTINEL.index)
					REMOVE_VEHICLE_RECORDING(002, "NIG1C_COMET2")		
					CLEAR_PED_TASKS(SKID_CAR_SENTINEL.driver)
					TASK_VEHICLE_DRIVE_WANDER(SKID_CAR_SENTINEL.driver, SKID_CAR_SENTINEL.index, 20, DF_SteerAroundStationaryCars | DF_StopForCars | DF_StopForPeds | DF_StopAtLights)
					
					/*STOP_PLAYBACK_RECORDED_VEHICLE(ESCORT_CAR_FAMILY.index)
					REMOVE_VEHICLE_RECORDING(001, "NIG1CEscort")		
					CLEAR_PED_TASKS(ESCORT_CAR_FAMILY.driver)
					open_sequence_task(seq)
						TASK_PAUSE(NULL, 2000)
						TASK_VEHICLE_DRIVE_WANDER(NULL, ESCORT_CAR_FAMILY.index, 20, DF_SteerAroundStationaryCars | DF_StopForCars | DF_StopForPeds | DF_StopAtLights)
					close_sequence_task(seq)
					task_perform_sequence(ESCORT_CAR_FAMILY.driver, seq)
					clear_sequence_task(seq)*/
					
					if CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FIRST_CAR)
						TASK_VEHICLE_DRIVE_WANDER(FIRST_CAR.driver, FIRST_CAR.index, 15, DF_SteerAroundStationaryCars | DF_StopForCars | DF_StopForPeds | DF_StopAtLights)
					ENDIF
								
					bRELEASE_SENTINEL_CAR = TRUE
				ENDIF
				IF bSENTINEL_HONKING = FALSE
					IF IS_ENTITY_IN_RANGE_ENTITY(SKID_CAR_SENTINEL.index, CELEBRITY_DOG, 6)
						START_VEHICLE_HORN(SKID_CAR_SENTINEL.index, 4000)
						bSENTINEL_HONKING = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
					IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() = 19			
						START_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_SENTINEL.index, 002, "NIG1C_COMET2")		
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(SKID_CAR_SENTINEL.index, 4400)
						TASK_LOOK_AT_ENTITY(SKID_CAR_SENTINEL.driver, CELEBRITY_DOG, -1)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(SKID_CAR_SENTINEL.index, TRUE)
						
						/*START_PLAYBACK_RECORDED_VEHICLE(ESCORT_CAR_FAMILY.index, 001, "NIG1CEscort")		
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(ESCORT_CAR_FAMILY.index, 4400)
						TASK_LOOK_AT_ENTITY(ESCORT_CAR_FAMILY.driver, CELEBRITY_DOG, -1)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(ESCORT_CAR_FAMILY.index, TRUE)*/
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the fire truck drive out of the fire station, blocking the dog's route
// This will probably go back in so just commenting it out. B* 1107526
/*PROC UPDATE_THE_FIRE_TRUCK()	
	IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)
		IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FIRE_TRUCK)	
			IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() < 41	
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(FIRE_TRUCK.index)
					SET_PLAYBACK_SPEED(FIRE_TRUCK.index, fCAR_REC_SPEED)	
				ELSE
					IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() = 39			
						START_PLAYBACK_RECORDED_VEHICLE(FIRE_TRUCK.index, 001, "NIG1C_FIRE")		
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(FIRE_TRUCK.index, 1000)
						SET_VEHICLE_SIREN(FIRE_TRUCK.index, TRUE)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(FIRE_TRUCK.index, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(FIRE_TRUCK.index)
					open_sequence_task(seq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRE_TRUCK.index, <<-654.22, -117.82, 36.72>>, 3, DRIVINGSTYLE_NORMAL, FIRETRUK, DF_SwerveAroundAllCars | DF_SteerAroundPeds, 3, 5)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRE_TRUCK.index, <<-639.11, -166.04, 36.67>>, 20, DRIVINGSTYLE_NORMAL, FIRETRUK, DF_SwerveAroundAllCars | DF_SteerAroundPeds, 5, 3)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRE_TRUCK.index, <<-551.31, -313.15, 34.11>>, 20, DRIVINGSTYLE_NORMAL, FIRETRUK, DF_SwerveAroundAllCars | DF_SteerAroundPeds, 5, 3)
					close_sequence_task(seq)
					task_perform_sequence(FIRE_TRUCK.driver, seq)
					clear_sequence_task(seq)
				ENDIF
			ENDIF		
		ENDIF
	
	ENDIF
		
ENDPROC*/

/// PURPOSE:
///    Makes the jeep and its driver react appropriately to the dog
PROC UPDATE_THE_JEEP()
	IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SKID_CAR_JEEP)
	AND IS_PED_UNINJURED(CELEBRITY_DOG)
		
		SWITCH jeepStage 
			CASE JS_WAITING
			BREAK
			CASE JS_SET_TO_DRIVE
				SET_ENTITY_COORDS(SKID_CAR_JEEP.index, <<-616.56, -176.60, 37.26>>)
				SET_ENTITY_HEADING(SKID_CAR_JEEP.index, 117.29)
				SET_VEHICLE_ON_GROUND_PROPERLY(SKID_CAR_JEEP.index)
				IF IS_VEHICLE_OK(CRASH_CAR_FAMILY.index)
					TASK_VEHICLE_MISSION(SKID_CAR_JEEP.driver, SKID_CAR_JEEP.index, CRASH_CAR_FAMILY.index, MISSION_ESCORT_REAR, 15, DRIVINGMODE_PLOUGHTHROUGH, 20, 5)
				ENDIF
				SET_VEHICLE_FORWARD_SPEED(SKID_CAR_JEEP.index, 10)
				jeepStage = JS_DRIVING
			BREAK
			CASE JS_DRIVING
				SET_DRIVE_TASK_CRUISE_SPEED(SKID_CAR_JEEP.driver, 15)
				IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, SKID_CAR_JEEP.driver, 18)
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 2.0, REPLAY_IMPORTANCE_LOWEST)
					jeepStage = JS_SET_TO_SKID
				ENDIF
			BREAK
			CASE JS_SET_TO_SKID
				open_sequence_task(seq)
					TASK_VEHICLE_TEMP_ACTION(NULL, SKID_CAR_JEEP.index, TEMPACT_SWERVELEFT, 500)
					TASK_VEHICLE_TEMP_ACTION(NULL, SKID_CAR_JEEP.index, TEMPACT_HANDBRAKETURNLEFT, 1000)
					TASK_PAUSE(NULL, 1000)
					TASK_LEAVE_VEHICLE(NULL, SKID_CAR_JEEP.index)			
				close_sequence_task(seq)
				task_perform_sequence(SKID_CAR_JEEP.driver, seq)
				clear_sequence_task(seq)
				START_VEHICLE_HORN(SKID_CAR_JEEP.index, 1000)
				
				jeepStage = JS_EXITING_CAR
			BREAK
			CASE JS_EXITING_CAR
				IF GET_SCRIPT_TASK_STATUS(SKID_CAR_JEEP.driver, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				AND IS_VEHICLE_SEAT_FREE(SKID_CAR_JEEP.index, VS_DRIVER)
				AND NOT IS_PED_FLEEING(SKID_CAR_JEEP.driver)
					open_sequence_task(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SKID_CAR_JEEP.index, <<-2, 2, 0>>), 2)
						IF IS_PED_UNINJURED(CRASH_CAR_POSH.driver)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, CRASH_CAR_POSH.driver, 5000)
						ELSE
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 5000)						
						ENDIF	
						TASK_ENTER_VEHICLE(NULL, SKID_CAR_JEEP.index, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, 1.8, ECF_RESUME_IF_INTERRUPTED | ECF_JACK_ANYONE)
						TASK_VEHICLE_DRIVE_WANDER(NULL, SKID_CAR_JEEP.index, 10, DF_StopAtLights | DF_StopForPeds | DF_StopForCars)				
					close_sequence_task(seq)
					task_perform_sequence(SKID_CAR_JEEP.driver, seq)
					clear_sequence_task(seq)
					jeepStage = JS_LOOKS_AT_CRASH
				ENDIF	
				if IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SKID_CAR_JEEP.index)
				AND NOT IS_PED_FLEEING(SKID_CAR_JEEP.driver)
					CLEAR_PED_TASKS(SKID_CAR_JEEP.driver)
					jeepStage = JS_PLAYER_ENTERS_CAR
				ENDIF
				SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(SKID_CAR_JEEP.index, TRUE)
			BREAK
			CASE JS_LOOKS_AT_CRASH
				if IS_PED_IN_VEHICLE(PLAYER_PED_ID(), SKID_CAR_JEEP.index)
				AND NOT IS_PED_FLEEING(SKID_CAR_JEEP.driver)
					CLEAR_PED_TASKS(SKID_CAR_JEEP.driver)
					jeepStage = JS_PLAYER_ENTERS_CAR
				ENDIF				
			BREAK
			CASE JS_PLAYER_ENTERS_CAR
				open_sequence_task(seq)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)					
					TASK_ENTER_VEHICLE(null, SKID_CAR_JEEP.index, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, 2.5, ECF_RESUME_IF_INTERRUPTED | ECF_JACK_ANYONE)
					TASK_VEHICLE_MISSION_PED_TARGET(NULL, SKID_CAR_JEEP.index, PLAYER_PED_ID(), MISSION_FLEE, 20,DF_SteerAroundPeds | DF_SwerveAroundAllCars, 100, 5)					
				close_sequence_task(seq)
				task_perform_sequence(SKID_CAR_JEEP.driver, seq)
				clear_sequence_task(seq)
				jeepStage = JS_JEEP_FLEES
			BREAK
			CASE JS_JEEP_FLEES
			BREAK
		ENDSWITCH
		
	ENDIF		
ENDPROC

/// PURPOSE:
///    Positions the dog when the player catches it
PROC SET_DOG_POSITION_WHEN_CAUGHT()
	
	FLOAT zPos
	VECTOR vPos_Final
	SET_ENTITY_COORDS(CELEBRITY_DOG, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.3,0.9,0>>))
	SET_ENTITY_HEADING(CELEBRITY_DOG, GET_ENTITY_HEADING(PLAYER_PED_ID()) + 90)
	GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(CELEBRITY_DOG), zPos)				
	vPos_Final = GET_ENTITY_COORDS(CELEBRITY_DOG)	
	SET_ENTITY_COORDS(CELEBRITY_DOG, <<vPos_Final.x, vPos_Final.y, zPos>>)
ENDPROC

/// PURPOSE: 
///    Checks if the player is close enough and running fast enough to scare Muffy Cakes into running
/// RETURNS:
///    TRUE if the player is close enough and running fast enough to scare Muffy Cakes into running, FALSE otherwise
FUNC BOOL PLAYER_SCARED_DOG()
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(CELEBRITY_DOG)
		IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 6.5)
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-630.475281,-263.955231,37.405323>>, <<-621.448730,-258.858795,39.220131>>, 4.000000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is close enough to the dog to have caught it
/// PARAMS:
///    fAdditionalDistance - an additional value to add on to the standard checking distance
/// RETURNS:
///    TRUE if the player is close enough to have caught the dog, false otherwise
FUNC BOOL PLAYER_CAUGHT_DOG(FLOAT fAdditionalDistance = 0.0)	
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(CELEBRITY_DOG)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR IS_PED_RAGDOLL(PLAYER_PED_ID()) OR IS_PED_GETTING_UP(PLAYER_PED_ID()) OR IS_PED_PRONE(PLAYER_PED_ID())
		OR IS_PED_FALLING(PLAYER_PED_ID()) OR IS_PED_JUMPING(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(CELEBRITY_DOG, vChatUpAreaPos1, vChatUpAreaPos2, fChatUpAreaWidth) //missionStage <> MS_GRAB_DOG_CUTSCENE AND missionStage <> MS_DOG_WAITING_AT_FIRESTATION
		//IF missionStage = MS_DOG_RUNS_AWAY AND stageStage = SS_STAGE
		//AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, 2.0)
		AND IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CELEBRITY_DOG, <<0,1.0,0>>), 1.5)
		AND NOT HAS_ENTITY_COLLIDED_WITH_ANYTHING(CELEBRITY_DOG)
			SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 500, 1000, TASK_RELAX, FALSE, FALSE)
			SET_PED_RAGDOLL_FORCE_FALL(PLAYER_PED_ID())
			/*IF HAS_ANIM_DICT_LOADED(NIGEL1C_DICT)
				TASK_PLAY_ANIM(PLAYER_PED_ID(), NIGEL1C_DICT, "avoid_rf")
			ENDIF*/
			CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_CLOSE", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
			RETURN FALSE
		ENDIF
		
		IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), (0.8 + fAdditionalDistance))
			/*IF missionStage <> MS_GRAB_DOG_CUTSCENE AND missionStage <> MS_DOG_WAITING_AT_FIRESTATION
			//IF missionStage = MS_DOG_RUNS_AWAY AND stageStage = SS_STAGE
			AND IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), GET_ENTITY_COORDS(CELEBRITY_DOG), 2.0)
				SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 500, 1000, TASK_RELAX, FALSE, FALSE)
				CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_CLOSE", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
			ELSE*/
				VECTOR offset
				offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(CELEBRITY_DOG))
				if offset.y > 0 AND offset.z < 0.1
					RETURN TRUE
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
		
ENDFUNC

PROC FINISHED_CHASE_EARLY()
	IF IS_PED_UNINJURED(CELEBRITY_PED)
		TASK_TURN_PED_TO_FACE_ENTITY(CELEBRITY_PED, PLAYER_PED_ID(), -1)
	ENDIF
	stageStage = SS_SETUP
	missionStage = MS_GRAB_DOG_CUTSCENE
ENDPROC

/// PURPOSE:
///    Speeds up the dog if the player is too close, slows it down if the player is too far away
PROC UPDATE_DOG_RUN_SPEED()
	
	IF IS_PED_UNINJURED(CELEBRITY_DOG)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)	
		
		IF runSpeed = dogRunsSlow
		or runSpeed = dogRunsVerySlow
			if GET_THE_CLOSEST_WAYPOINT_TO_PLAYER() > GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() - 1
				runSpeed = dogRunsFastest
			ENDIF
		ENDIF
		
		IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() < 18
		AND GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 6
			runSpeed = dogRunsFastest
		ENDIF
		
		SWITCH runSpeed	
			CASE dogRunsFastest
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(CELEBRITY_DOG, 2.9)
				fCAR_REC_SPEED = 1
				
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 9)
					runSpeed = dogRunsNormal
				ENDIF
			BREAK		
			CASE dogRunsNormal
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(CELEBRITY_DOG, 2.65)
				fCAR_REC_SPEED = 0.77
				
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 12)
					runSpeed = dogRunsSlow
				ELSE 
					IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 8)			
						runSpeed = dogRunsFastest	
					ENDIF
				ENDIF
			BREAK		
			CASE dogRunsSlow
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(CELEBRITY_DOG, 2.5)
				fCAR_REC_SPEED = 0.7
				
				IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 10)	
					runSpeed = dogRunsNormal
				ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 15)	
					IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 8
						runSpeed = dogRunsVerySlow
					ENDIF
				ENDIF
			BREAK		
			CASE dogRunsVerySlow
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(CELEBRITY_DOG, 2.3)
				fCAR_REC_SPEED = 0.5
				
				IF IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_DOG, PLAYER_PED_ID(), 12)	
					runSpeed = dogRunsSlow
				ENDIF
			BREAK			
		ENDSWITCH
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks if it's ok to play dialogue duing the chase
/// RETURNS:
///    TRUE if it's OK, FALSE otherwise
FUNC BOOL IF_CAN_PLAY_CHASE_DIALOGUE()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("N1C_WRN_DOG")	// Catch up with ~b~Mr. Muffy Cakes.
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether or not the first shocked ped is playing the avoid animation
/// RETURNS:
///    TRUE if they're not playing the animation, FALSE otherwise
FUNC BOOL NOT_PLAYING_AVOID_ANIM()
	IF NOT IS_ENTITY_PLAYING_ANIM(shockedPed[0].index, NIGEL1C_DICT, "avoid_rf")
	AND NOT IS_ENTITY_PLAYING_ANIM(shockedPed[0].index, NIGEL1C_DICT, "avoid_rb")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Plays appropriate dialogue from the peds along the chase route
PROC DEAL_WITH_RANDOM_DIALOGUE()

	IF shockedPed[0].beenShocked
		IF bSHOP_PED_SHOUTED = FALSE
			IF IS_PED_UNINJURED(shockedPed[0].index)
			AND IS_PED_UNINJURED(PLAYER_PED_ID())
			AND NOT IS_PED_FLEEING(shockedPed[0].index)
				IF NOT_PLAYING_AVOID_ANIM()
				OR IS_ENTITY_IN_RANGE_ENTITY(shockedPed[0].index, PLAYER_PED_ID(), 5)	
					IF IS_ENTITY_IN_RANGE_ENTITY(shockedPed[0].index, PLAYER_PED_ID(), 12)
						ADD_PED_FOR_DIALOGUE(sSpeech, 5, shockedPed[0].index, "ShockedHipsterFemale")
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_1",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
							CLEAR_PED_TASKS(shockedPed[0].index)
							open_sequence_task(seq)
								TASK_LOOK_AT_ENTITY(null, PLAYER_PED_ID(), -1)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, shockedPed[0].vPosEnd, 1)
							close_sequence_task(seq)
							task_perform_sequence(shockedPed[0].index, seq)
							clear_sequence_task(seq)
							bSHOP_PED_SHOUTED = TRUE
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), shockedPed[0].index, 5000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF bTREVOR_RESPONDED = FALSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_2",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
					bTREVOR_RESPONDED = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bCRASH_SHOUTED = TRUE
		IF IS_PED_UNINJURED(shockedPed[2].index)
		AND IS_PED_UNINJURED(PLAYER_PED_ID())
		AND NOT IS_PED_FLEEING(shockedPed[2].index)
			ADD_PED_FOR_DIALOGUE(sSpeech, 4, shockedPed[2].index, "ShockedBusinessMan")
			IF IS_ENTITY_IN_RANGE_ENTITY(shockedPed[2].index, PLAYER_PED_ID(), 50)
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_3",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
					bCRASH_SHOUTED = FALSE
				ENDIF
			ELSE
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_3",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					bCRASH_SHOUTED = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF shockedPed[1].beenShocked = TRUE
		IF bWHAT_YOU_LOOKIN_AT_LINES_READY = TRUE
			IF IS_PED_UNINJURED(shockedPed[1].index)
			AND IS_PED_UNINJURED(PLAYER_PED_ID())
			AND NOT IS_PED_FLEEING(shockedPed[1].index)
				IF IS_ENTITY_IN_RANGE_ENTITY(shockedPed[1].index, PLAYER_PED_ID(), 8)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_4", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
						CLEAR_PED_TASKS(shockedPed[1].index)
						open_sequence_task(seq)
							TASK_LOOK_AT_ENTITY(null, PLAYER_PED_ID(), -1)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, shockedPed[1].vPosEnd, 1)
						close_sequence_task(seq)
						task_perform_sequence(shockedPed[1].index, seq)
						clear_sequence_task(seq)					
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), shockedPed[1].index, 3500)						
						bWHAT_YOU_LOOKIN_AT_LINES_READY = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bFinal_CHASE_LINES_READY = TRUE
		IF GET_THE_CLOSEST_WAYPOINT_TO_PLAYER() > 40
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_5", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
			//IF CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_5", "NIG1C_COLLAR", "NIG1C_COLLAR_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, -1)
				bFinal_CHASE_LINES_READY = FALSE
				bGotchaLineReady = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bGotchaLineReady
		IF IS_PED_UNINJURED(CELEBRITY_DOG)
		AND IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, 10.0)
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_COLLAR", "NIG1C_COLLAR_1", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
					bGotchaLineReady = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes the vehicle and ped indexes held by a MISSION_VEHICLE struct
/// PARAMS:
///    vehicle - the MISSION_VEHICLE whose vehicle and ped indexes we're deleting
PROC DELETE_MISSION_VEHICLE_AND_DRIVER(MISSION_VEHICLE vehicle)	
	SAFE_DELETE_PED(vehicle.driver)	
	SAFE_DELETE_VEHICLE(vehicle.index)	
ENDPROC

/// PURPOSE:
///    
PROC TriggerSecondPartofCelebConv()
	IF bSecondPartReady
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_MODEL", "NIG1C_MODEL_2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				bSecondPartReady = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    State machine to control the player attempting to chat up the model
PROC UPDATE_CHAT_UP_SEQUENCE()
			
	SWITCH celebDialogueState
		CASE CC_PUT_GUN_AWAY			
			if CURRENT_PLAYER_WEAPON() = WEAPONTYPE_UNARMED
				celebDialogueState = CC_SET_WHISTLE
			ENDIF
		BREAK
		CASE CC_SET_WHISTLE
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				ANIM_DATA none
				ANIM_DATA animDataBlend
				animDataBlend.type = APT_SINGLE_ANIM
				animDataBlend.anim0 = "hailing_whistle_waive_a"
				animDataBlend.dictionary0 = NIGEL1C_DICT
				animDataBlend.phase0 = 0.0
				animDataBlend.rate0 = 1.0
				animDataBlend.weight0 = 0.5
				animDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_L_ARM")
				animDataBlend.flags = AF_UPPERBODY | AF_SECONDARY
				animDataBlend.ikFlags = AIK_USE_FP_ARM_LEFT
				TASK_SCRIPTED_ANIMATION(PLAYER_PED_ID(), animDataBlend, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)
			ENDIF
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 2.0, REPLAY_IMPORTANCE_LOWEST)
			IF IS_ENTITY_ALIVE(CELEBRITY_DOG)
				SET_GAMEPLAY_ENTITY_HINT(CELEBRITY_DOG, <<0,0,0>>)
			ENDIF
			iChatUpTimer = GET_GAME_TIMER() + 750
			celebDialogueState = CC_WHISTLE
		BREAK
		CASE CC_WHISTLE
			IF GET_GAME_TIMER() > iChatUpTimer
			AND IS_PED_UNINJURED(CELEBRITY_PED)
				IF IS_PED_USING_ANY_SCENARIO(CELEBRITY_PED)
					SET_PED_PANIC_EXIT_SCENARIO(CELEBRITY_PED, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				celebDialogueState = CC_SET_TALKING
			ENDIF
		BREAK
		case CC_SET_TALKING
			//IF CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_MODEL", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())	
			IF IS_PED_UNINJURED(CELEBRITY_PED)
			AND PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_MODEL", "NIG1C_MODEL_1", CONV_PRIORITY_MEDIUM)//, CheckDisplaySubtitles())
				//TASK_TURN_PED_TO_FACE_ENTITY(CELEBRITY_PED, PLAYER_PED_ID(), -1)
				TASK_LOOK_AT_ENTITY(CELEBRITY_PED, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				open_sequence_task(seq)	
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, MILAN_DICT, "gesture_easy_now", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)//|AF_UPPERBODY|AF_SECONDARY)
					/*TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, MILAN_DICT, "gesture_bye_hard", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, MILAN_DICT, "gesture_damn", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, MILAN_DICT, "gesture_no_way", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)*/
					//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_PED, seq)
				clear_sequence_task(seq)
				//TASK_PLAY_ANIM(CELEBRITY_PED, MILAN_DICT, "gesture_easy_now", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
				/*open_sequence_task(seq)	
					TASK_LOOK_AT_ENTITY(null, PLAYER_PED_ID(), -1)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_PED, seq)
				clear_sequence_task(seq)*/
				bSecondPartReady = TRUE
				iChatUpTimer = GET_GAME_TIMER() + 1000
				celebDialogueState = CC_DOG_STARTS_WALKING
			ENDIF
		BREAK
		CASE CC_DOG_STARTS_WALKING
			TriggerSecondPartofCelebConv()
			IF GET_GAME_TIMER() > iChatUpTimer
			AND IS_PED_UNINJURED(CELEBRITY_DOG)
				TASK_FOLLOW_NAV_MESH_TO_COORD(CELEBRITY_DOG, <<-628.1631, -261.5749, 37.6045>>, 1.5, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, -15.13)
				celebDialogueState = CC_PLAYER_AND_CELEBRITY_TALKING
			ENDIF
		BREAK
		CASE CC_PLAYER_AND_CELEBRITY_TALKING
			TriggerSecondPartofCelebConv()
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
			AND IS_ENTITY_IN_RANGE_COORDS(CELEBRITY_DOG, <<-628.1631, -261.5749, 37.6045>>, 2.0)
				CLEAR_PRINTS()						
				reasonDogRan = SCARED_DOG
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY
			ENDIF
			/*IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				CLEAR_PRINTS()						
				reasonDogRan = MODEL_SCARED_DOG
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY
			ENDIF
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), CELEBRITY_PED) > 23	
				ConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
				KILL_FACE_TO_FACE_CONVERSATION()
				celebDialogueState = CC_PLAYER_TOO_FAR
			ENDIF*/
		BREAK
		/*CASE CC_PLAYER_TOO_FAR
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), CELEBRITY_PED) < 15
				IF bPLAYER_RETURNED_TO_CELEB = FALSE
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_1", CONV_PRIORITY_VERY_HIGH)	
						celebDialogueState = CC_RETURN_TO_CONVO_LINE
						bPLAYER_RETURNED_TO_CELEB = TRUE
					ENDIF
				ELSE
					celebDialogueState = CC_RETURN_TO_CONVO_LINE
				ENDIF
			ENDIF
		BREAK
		CASE CC_RETURN_TO_CONVO_LINE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, "NIG1CAU", "NIG1C_MODEL", ConvResumeLabel, CONV_PRIORITY_MEDIUM)
					celebDialogueState = CC_PLAYER_AND_CELEBRITY_TALKING
				ENDIF
			ENDIF
		BREAK*/
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    If necessary, stops car recording playback for a vehicle
/// PARAMS:
///    vehicle - The vehicle we want to stop the car recording for
PROC CHECK_AND_STOP_THIS_CAR_RECORDING(MISSION_VEHICLE vehicle)
	if CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(vehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicle.index)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehicle.index)
			TASK_VEHICLE_DRIVE_WANDER(vehicle.driver, vehicle.index, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes a vehicle drive away using AI
/// PARAMS:
///    vehicle - The vehicle we want to drive off
PROC CLEANUP_SCRIPTED_VEHICLE_TASK(MISSION_VEHICLE vehicle)
	
	IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(vehicle)
		if IS_PED_IN_VEHICLE(vehicle.driver, vehicle.index)
			CLEAR_PED_TASKS(vehicle.driver)
			TASK_VEHICLE_DRIVE_WANDER(vehicle.driver, vehicle.index, 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops all car recordings and makes the cars drive away under AI control
PROC CHECK_EACH_VEHICLE_FOR_CAR_RECORDING()
	CHECK_AND_STOP_THIS_CAR_RECORDING(CRASH_CAR_POSH)
	CHECK_AND_STOP_THIS_CAR_RECORDING(CRASH_CAR_FAMILY)
	CHECK_AND_STOP_THIS_CAR_RECORDING(SKID_CAR_TAXI)
	CHECK_AND_STOP_THIS_CAR_RECORDING(SKID_CAR_SENTINEL)
	//CHECK_AND_STOP_THIS_CAR_RECORDING(ESCORT_CAR_FAMILY)
	// This will probably go back in so just commenting it out. B* 1107526
	//CHECK_AND_STOP_THIS_CAR_RECORDING(FIRE_TRUCK)	
	CLEANUP_SCRIPTED_VEHICLE_TASK(SKID_CAR_JEEP)
	CLEANUP_SCRIPTED_VEHICLE_TASK(FIRST_CAR)
	CLEANUP_SCRIPTED_VEHICLE_TASK(SECOND_CAR)
	CLEANUP_SCRIPTED_VEHICLE_TASK(FINAL_CAR)
	CLEANUP_SCRIPTED_VEHICLE_TASK(TAXI_CAR)
ENDPROC

/// PURPOSE:
///    Deletes everything created by the mission
PROC DELETE_EVERYTHING()
	TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	
	SAFE_DELETE_PED(CELEBRITY_PED)
	SAFE_DELETE_PED(CELEBRITY_DOG)
	SAFE_DELETE_PED(shockedPed[0].index)	
	SAFE_DELETE_PED(shockedPed[1].index)
	SAFE_DELETE_PED(shockedPed[2].index)	
	SAFE_DELETE_PED(shockedPed[3].index)	
	SAFE_DELETE_PED(shockedPed[4].index)
	SAFE_DELETE_PED(TAXI_PASSENGER_PED)	
	
	DELETE_MISSION_VEHICLE_AND_DRIVER(CRASH_CAR_FAMILY)
	DELETE_MISSION_VEHICLE_AND_DRIVER(CRASH_CAR_POSH)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_TAXI)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_SENTINEL)
	DELETE_MISSION_VEHICLE_AND_DRIVER(TAXI_CAR)
	//DELETE_MISSION_VEHICLE_AND_DRIVER(ESCORT_CAR_FAMILY)
	DELETE_MISSION_VEHICLE_AND_DRIVER(FIRST_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SECOND_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(FINAL_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_JEEP)
	// This will probably go back in so just commenting it out. B* 1107526
	//DELETE_MISSION_VEHICLE_AND_DRIVER(FIRE_TRUCK)

	SAFE_DELETE_VEHICLE(STATIONARY_FIRE_TRUCK)
ENDPROC

/// PURPOSE:
///    Checks for the player threatening the model
PROC CHECK_IF_PLAYER_THREATENED_MILAN()
	IF IS_PED_UNINJURED(CELEBRITY_PED)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())

		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), CELEBRITY_PED)
			//SET_PED_CAN_BE_TARGETTED(CELEBRITY_PED, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED, relGroupPushed)
			bPlayerThreatenedModel = TRUE
		ELSE
			//IF GET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED) <> relGroupFriendly
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CELEBRITY_PED, PLAYER_PED_ID())
					IF IS_PED_USING_ANY_SCENARIO(CELEBRITY_PED)
	    				SET_PED_PANIC_EXIT_SCENARIO(CELEBRITY_PED, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF
					CLEAR_PED_TASKS(CELEBRITY_PED)
					TASK_SMART_FLEE_PED(CELEBRITY_PED, PLAYER_PED_ID(), 80, -1)
				ENDIF
				//SET_PED_CAN_BE_TARGETTED(CELEBRITY_PED, TRUE)
				//SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED, relGroupFriendly)
			//ENDIF			
		ENDIF
		IF NOT IS_PED_FLEEING(CELEBRITY_PED)
			IF IS_PLAYER_VISIBLY_TARGETTING_PED(CELEBRITY_PED) AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CELEBRITY_PED, PLAYER_PED_ID()) AND NOT IS_PLAYER_SHOOTING_NEAR_PED(CELEBRITY_PED)
				IF IS_PED_USING_ANY_SCENARIO(CELEBRITY_PED)
    				SET_PED_PANIC_EXIT_SCENARIO(CELEBRITY_PED, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				CLEAR_PED_TASKS(CELEBRITY_PED)
				TASK_LOOK_AT_ENTITY(CELEBRITY_PED, PLAYER_PED_ID(), -1)
				TASK_SMART_FLEE_PED(CELEBRITY_PED, PLAYER_PED_ID(), 80, -1)
				bPlayerThreatenedModel = TRUE
			ENDIF
		ENDIF
		IF bPlayerThreatenedModel = TRUE AND GET_GAME_TIMER() > iCelebScaredTimer AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), CELEBRITY_PED, 10)
			//IF bCelebPleaded = FALSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					STOP_SCRIPTED_CONVERSATION(FALSE)
				ENDIF
				STRING sConv
				INT iConvID = GET_RANDOM_INT_IN_RANGE(0, 5)
				SWITCH iConvID
					CASE 0 sConv = "NIG1C_GUN" BREAK
					CASE 1 sConv = "NIG1C_BUMP" BREAK
					CASE 2 sConv = "NIG1C_HIT" BREAK
					CASE 3 sConv = "NIG1C_SHOCK" BREAK
					DEFAULT sConv = "NIG1C_RUN" BREAK
				ENDSWITCH
				IF CREATE_CONVERSATION(sSpeech, "NIG1CAU", sConv,CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
					iCelebScaredTimer = GET_GAME_TIMER() + 5000
					bCelebPleaded = TRUE
				ENDIF
				/*IF NOT IS_THIS_PRINT_BEING_DISPLAYED("N1C_WRN_DOG")	// Catch up with ~b~Mr. Muffy Cakes.
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("N1C_CATCH")	// Catch ~b~Mr. Muffy Cakes.
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_GUN", "NIG1C_GUN_1",CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
						bCelebPleaded = TRUE
					ENDIF
				ELSE
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_GUN", "NIG1C_GUN_1",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						bCelebPleaded = TRUE
					ENDIF
				ENDIF*/
			//ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CONTROL_KERRY_SHOPPING()
	IF IS_PED_UNINJURED(CELEBRITY_PED)
		IF GET_GAME_TIMER() > iKerryShoppingTimer AND IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vKerrySpeaksPos1, vKerrySpeaksPos2, fKerrySpeaksWidth)
			/*IF CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_SHOP", CONV_PRIORITY_HIGH, CheckDisplaySubtitles())
				iKerryShoppingTimer = GET_GAME_TIMER() + 3000
			ENDIF*/
			BOOL bConvDone = FALSE
			IF IS_MESSAGE_BEING_DISPLAYED()
				bConvDone = CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_SHOP", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			ELSE
				bConvDone = CREATE_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_SHOP", CONV_PRIORITY_HIGH)
			ENDIF
			IF bConvDone
				iKerryShoppingTimer = GET_GAME_TIMER() + 7000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		DEBUG
// ===========================================================================================================

/// PURPOSE:
///    Jumps to a particular stage in the mission
/// PARAMS:
///    nextState - The stage to jump to
PROC JUMP_TO_STAGE(debugSkipToMissionStage nextState)

	RC_START_Z_SKIP()
	
	CHECK_FAIL_POSSIBILITIES()	
	CLEAR_PRINTS()
	
	if IS_PED_UNINJURED(CELEBRITY_DOG)
		CLEAR_PED_TASKS_IMMEDIATELY(CELEBRITY_DOG)
	ENDIF
	if IS_PED_UNINJURED(CELEBRITY_PED)
		CLEAR_PED_TASKS(CELEBRITY_PED)
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	
	bCelebPleaded = FALSE
	bPlayerThreatenedModel = FALSE
	
	DISABLE_CELLPHONE(FALSE)
	FINISH_CUTSCENE(FALSE, TRUE)	

	IF DOES_BLIP_EXIST(GOTO_BLIP)
		REMOVE_BLIP(GOTO_BLIP)
	ENDIF
	
	SAFE_DELETE_PED(shockedPed[0].index)
	SAFE_DELETE_PED(shockedPed[1].index)	
	SAFE_DELETE_PED(shockedPed[2].index)	
	SAFE_DELETE_PED(shockedPed[3].index)	
	SAFE_DELETE_PED(shockedPed[4].index)
	SAFE_DELETE_PED(TAXI_PASSENGER_PED)
	SAFE_DELETE_PED(CELEBRITY_PED)	
	SAFE_DELETE_PED(CELEBRITY_DOG)	
	
	DELETE_MISSION_VEHICLE_AND_DRIVER(CRASH_CAR_FAMILY)
	DELETE_MISSION_VEHICLE_AND_DRIVER(CRASH_CAR_POSH)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_TAXI)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_SENTINEL)
	DELETE_MISSION_VEHICLE_AND_DRIVER(TAXI_CAR)
	//DELETE_MISSION_VEHICLE_AND_DRIVER(ESCORT_CAR_FAMILY)
	DELETE_MISSION_VEHICLE_AND_DRIVER(FIRST_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SECOND_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(FINAL_CAR)
	DELETE_MISSION_VEHICLE_AND_DRIVER(SKID_CAR_JEEP)
	// This will probably go back in so just commenting it out. B* 1107526
	//DELETE_MISSION_VEHICLE_AND_DRIVER(FIRE_TRUCK)

	SAFE_DELETE_VEHICLE(STATIONARY_FIRE_TRUCK)
	
	CLEAR_AREA_OF_VEHICLES(<<-669.084290,-209.142578,36.272766>>, 25.25)
	CLEAR_AREA_OF_VEHICLES(vFirestationClearLocation, 50)
	CLEAR_AREA_OF_PEDS(<<-670.171875,-208.086105,36.258213>>, 7.250000)
	stageStage = SS_SETUP
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			
	SWITCH nextState
		CASE DEBUG_TO_START	
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-604.82, -350.10, 34.01>>)	
			SET_ENTITY_HEADING(PLAYER_PED_ID(), -7.52)
			
			LOAD_INITIAL_ASSETS(TRUE)
			CREATE_MODEL_AND_DOG_AT_SHOPS()
			
			missionStage = MS_GO_TO_SHOPS
		BREAK
		CASE DEBUG_TO_CHECKPOINT
		
			LOAD_OTHER_ASSETS()
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-604.82, -350.10, 34.01>>)	
			SET_ENTITY_HEADING(PLAYER_PED_ID(), -7.52)		
			
			LOAD_INITIAL_ASSETS(TRUE)
			CREATE_MODEL_AND_DOG_AT_SHOPS()

			ADD_PED_FOR_DIALOGUE(sSpeech, 3, CELEBRITY_PED, "KERRY")	
			missionStage = MS_GO_TO_SHOPS
		BREAK
		CASE DEBUG_TO_STORES
			
			LOAD_OTHER_ASSETS()
			WAIT_TILL_ASSETS_ARE_LOADED()
			
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -605.2227, -291.6463, 35.7791 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 38.21)
			ENDIF
			
			LOAD_INITIAL_ASSETS(TRUE)
			CREATE_MODEL_AND_DOG_AT_SHOPS()

			ADD_PED_FOR_DIALOGUE(sSpeech, 3, CELEBRITY_PED, "KERRY")
			missionStage = MS_STAGE_APPROACH_CELEB
		BREAK
		CASE DEBUG_TO_ROAD_CHASE	
			
			LOAD_OTHER_ASSETS()
			WAIT_TILL_ASSETS_ARE_LOADED()
			
			GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-630.43, -255.38, 37.5>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 27.58)
			
			CELEBRITY_PED = CREATE_PED(PEDTYPE_CIVFEMALE, femaleHipsterModel, vPos_Celeb, 27.58)
			SET_ENTITY_COORDS_GROUNDED(CELEBRITY_PED, vPos_Celeb)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_PED, PED_COMP_HEAD, 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_PED, PED_COMP_HAIR, 1, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_PED, PED_COMP_TORSO, 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_PED, PED_COMP_LEG, 0, 0, 0) //(lowr)				
			
			CELEBRITY_DOG = CREATE_PED(PEDTYPE_MISSION, dogModel, <<-633.89, -250.10, 37.38>>, 50) 
			SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, PED_COMP_TORSO, 0, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, PED_COMP_LEG, 0, 1, 0) //(lowr)
					
			/*IF IS_PED_UNINJURED(CELEBRITY_PED)
				SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED, relGroupFriendly)
			ENDIF
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_DOG, relGroupFriendly)
			ENDIF*/
			
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, CELEBRITY_PED, "KERRY")			
			
			TURN_ROADS_ON(FALSE)
			
			missionStage = MS_DOG_RUNS_AWAY
		BREAK
		CASE DEBUG_TO_END_CUTSCENE		
		
			REQUEST_WAYPOINT_RECORDING(dogRoute)	
		
			REQUEST_MODEL(dogModel)
			
			WHILE NOT HAS_MODEL_LOADED(dogModel)
				WAIT(0)
			ENDWHILE
		
			CELEBRITY_DOG = CREATE_PED(PEDTYPE_MISSION, dogModel,  <<-636.42, -95.84, 37.48>>, -95.46) 
			SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, PED_COMP_TORSO, 0, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, PED_COMP_LEG, 0, 1, 0) //(lowr)	
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-636.21, -96.51, 37.2>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), -5.46)
			STOP_SCRIPTED_CONVERSATION(FALSE)
						
			/*IF IS_PED_UNINJURED(CELEBRITY_PED)
				SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED, relGroupFriendly)
			ENDIF
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_DOG, relGroupFriendly)
			ENDIF*/
			
			missionStage = MS_GRAB_DOG_CUTSCENE
		BREAK
	ENDSWITCH
	
	SET_MODEL_AS_NO_LONGER_NEEDED(dogModel)
	
	IF IS_REPLAY_BEING_SET_UP()
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, << -597.52, -307.37, 34.50 >>, 120.04, FALSE, FALSE, FALSE, TRUE, TRUE)
		INSTANTLY_FILL_PED_POPULATION()
		END_REPLAY_SETUP()
	ELSE
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		INSTANTLY_FILL_PED_POPULATION()
	ENDIF
	RC_END_Z_SKIP()
ENDPROC


// ===========================================================================================================
//		STAGE INIT AND CLEAN UP FUNCTIONS
// ===========================================================================================================

/// PURPOSE:
///    Initialises and loads everything needed at the start of the mission
PROC MISSION_INIT()
	
	LOAD_INITIAL_ASSETS()
	IF ARE_INITIAL_ASSETS_LOADED()
		#IF IS_DEBUG_BUILD	
			s_skip_menu[0].sTxtLabel = "INTRO PHONE CALL"  		
			s_skip_menu[1].sTxtLabel = "REPLAY CHECKPOINT (AFTER PHONECALL)"     	
			s_skip_menu[2].sTxtLabel = "APPROACH KERRY"    
			s_skip_menu[3].sTxtLabel = "CHASE DEXIE ON ROAD"       	
			s_skip_menu[4].sTxtLabel = "OUTRO CUTSCENE" 			
		#ENDIF
		
		SETUP_SHOCKABLE_PEDS()
		SETUP_MISSION_VEHICLES()
		
		IF IS_PHONE_ONSCREEN()
			HANG_UP_AND_PUT_AWAY_PHONE()
		ENDIF
		
		ADD_CONTACT_TO_PHONEBOOK(CHAR_NIGEL, TREVOR_BOOK, FALSE)
		
		/*IF IS_PED_UNINJURED(PLAYER_PED_ID())
			relGroupFriendly = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		ENDIF*/

		ADD_RELATIONSHIP_GROUP("PUSHED", relGroupPushed)
		ADD_RELATIONSHIP_GROUP("DRIVERS", relGroupDrivers)	
		
		CLEAR_AREA_OF_VEHICLES(<<-633.449280,-248.269058,35.874512>> , 50, FALSE)
		CLEAR_AREA_OF_VEHICLES(vFirestationClearLocation, 50)
		
		SET_START_CHECKPOINT_AS_FINAL()
		
		LOAD_OTHER_ASSETS()	
		//CREATE_MODEL_AND_DOG_AT_SHOPS()
		CELEBRITY_PED = sRCLauncherDataLocal.pedID[0]
		IF IS_PED_UNINJURED(CELEBRITY_PED)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CELEBRITY_PED, TRUE)
		ENDIF
		CELEBRITY_DOG = sRCLauncherDataLocal.pedID[1]
		IF IS_PED_UNINJURED(CELEBRITY_DOG)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(CELEBRITY_DOG, TRUE)
		ENDIF
		//SET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_DOG, relGroupFriendly)
		ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
		ADD_PED_FOR_DIALOGUE(sSpeech, 3, CELEBRITY_PED, "KERRY")
		
		iKerryShoppingTimer = GET_GAME_TIMER() + 3000
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(FIRETRUK, TRUE)
		stageStage = SS_SETUP
		missionStage = MS_GO_TO_SHOPS
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up everything needed to start the scripted cutscene
PROC GO_TO_CUTSCENE()
	SET_WIDESCREEN_BORDERS(TRUE, 0)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
ENDPROC

// ===========================================================================================================
//		MISSION STAGES
// ===========================================================================================================

/// PURPOSE:
///    The player goes to the shops to find the model and her dog
PROC STAGE_GO_TO_SHOPS()
	CONTROL_KERRY_SHOPPING()
	SWITCH stageStage
		CASE SS_SETUP
		
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_GO_TO_SHOPS")
			//GOTO_BLIP = ADD_BLIP_FOR_COORD(vPos_GoToShops)
			GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)
			PRINT_NOW("N1C_INIT", DEFAULT_GOD_TEXT_TIME, 1)
			TRIGGER_MUSIC_EVENT("NIGEL1C_FORA")
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vShoppingAreaA, vShoppingAreaB, fShoppingArea)
				stageStage = SS_SETUP
				missionStage = MS_STAGE_APPROACH_CELEB
			ELSE
				//PRINT("N1C_INIT", DEFAULT_GOD_TEXT_TIME, 1)	// Go to the ~y~stores.~s~
				stageStage = SS_STAGE
			ENDIF
		BREAK
		CASE SS_STAGE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vShoppingAreaA, vShoppingAreaB, fShoppingArea)
			//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_GoToShops, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_GO_TO_SHOPS")
				stageStage = SS_SETUP
				missionStage = MS_STAGE_APPROACH_CELEB
			ENDIF			
			
			IF IS_PED_FLEEING(CELEBRITY_PED)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					STOP_SCRIPTED_CONVERSATION(FALSE)
				ENDIF			
				CLEAR_PRINTS()	
				/*REMOVE_BLIP(GOTO_BLIP)
				GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)*/
				PRINT_NOW("N1C_CATCH", DEFAULT_GOD_TEXT_TIME, 1)	// Catch ~b~Mr. Muffy Cakes.
				TRIGGER_MUSIC_EVENT("NIGEL1C_START")
				reasonDogRan = SCARED_MODEL
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_GO_TO_SHOPS")
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY	
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The player approaches the model
PROC STAGE_APPROACH_CELEBRITY()
	CONTROL_KERRY_SHOPPING()
	SWITCH stageStage	
		CASE SS_SETUP
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), CELEBRITY_PED, -1)
			
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_APPROACH_CELEBRITY")
			
			/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_GoToShops, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
				// need this here to avoid the locate chevron flashing off for a frame
			ENDIF*/
			
			IF NOT DOES_BLIP_EXIST(GOTO_BLIP)
				/*PRINT("N1C_INIT", DEFAULT_GOD_TEXT_TIME, 1)	// Go to the ~y~stores.~s~
				GOTO_BLIP = ADD_BLIP_FOR_COORD(vPos_GoToShops)*/
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_LOW)
				GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)
				PRINT_NOW("N1C_INIT", DEFAULT_GOD_TEXT_TIME, 1)
				TRIGGER_MUSIC_EVENT("NIGEL1C_FORA")
			ENDIF
				
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vChatUpAreaPos1, vChatUpAreaPos2, fChatUpAreaWidth)
			//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_GoToShops, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_APPROACH_CELEBRITY")
				stageStage = SS_SETUP
				missionStage = MS_STAGE_CHAT_UP_CELEB 
			ENDIF
				
			IF IS_PED_FLEEING(CELEBRITY_PED)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					STOP_SCRIPTED_CONVERSATION(FALSE)
				ENDIF				
				reasonDogRan = SCARED_MODEL
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_APPROACH_CELEBRITY")
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY	
			ENDIF		
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The player attempts to chat up the model
PROC STAGE_CHAT_UP_CELEB()	
	/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_GoToShops, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
		// need this here to avoid the locate chevron flashing off for a frame
	ENDIF*/
	
	SWITCH stageStage
		CASE SS_SETUP
			IF CURRENT_PLAYER_WEAPON() <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				celebDialogueState = CC_PUT_GUN_AWAY
			ELSE
				celebDialogueState = CC_SET_WHISTLE
			ENDIF
			
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_CHAT_UP_CELEB")
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE	
			
			UPDATE_CHAT_UP_SEQUENCE()			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		
			IF bCelebPleaded = TRUE
				reasonDogRan = SCARED_MODEL
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY	
			ENDIF
			
			IF PLAYER_SCARED_DOG()	
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
					STOP_SCRIPTED_CONVERSATION(TRUE)
				ENDIF
				reasonDogRan = SCARED_DOG
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_CHAT_UP_CELEB")
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY	
			ENDIF
			
			IF GET_PED_RELATIONSHIP_GROUP_HASH(CELEBRITY_PED) = relGroupPushed
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
					STOP_SCRIPTED_CONVERSATION(FALSE)
				ENDIF
				reasonDogRan = PUSHED_MODEL
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_CHAT_UP_CELEB")
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_AWAY	
			ENDIF		
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The dog runs away and the player starts chasing it
PROC STAGE_DOG_RUNS_AWAY()
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	LOAD_OTHER_ASSETS()
	SWITCH stageStage	
		CASE SS_SETUP
			/*IF DOES_BLIP_EXIST(GOTO_BLIP)
				REMOVE_BLIP(GOTO_BLIP)
			ENDIF
			GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)	
			CLEAR_PRINTS()			
			PRINT("N1C_CATCH", DEFAULT_GOD_TEXT_TIME, 1)	// Catch ~b~Mr. Muffy Cakes.
			*/
			IF NOT DOES_BLIP_EXIST(GOTO_BLIP)
				GOTO_BLIP = CREATE_PED_BLIP(CELEBRITY_DOG, TRUE, TRUE)
			ENDIF
			PRINT_NOW("N1C_CATCH", DEFAULT_GOD_TEXT_TIME, 1)
			TRIGGER_MUSIC_EVENT("NIGEL1C_START")
			STOP_GAMEPLAY_HINT()
			
			TURN_ROADS_ON(FALSE)
			
			WAYPOINT_RECORDING_GET_NUM_POINTS(dogRoute, iNUM_WAYPOINTS)
			WAYPOINT_RECORDING_GET_COORD(dogRoute, iNUM_WAYPOINTS - 1, vPOS_FINAL_WAYPOINT)
		
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				CLEAR_PED_TASKS(CELEBRITY_DOG)
				open_sequence_task(seq)	
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null, TRUE)	
					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, dogRoute, 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_SLOW_FOR_CORNERS | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_DOG, seq)
				clear_sequence_task(seq)
			ENDIF
		
			if reasonDogRan <> SCARED_MODEL
			AND IS_PED_UNINJURED(CELEBRITY_PED)
			AND IS_PED_UNINJURED(CELEBRITY_DOG)
				CLEAR_PED_TASKS(CELEBRITY_PED)
				open_sequence_task(seq)
					IF reasonDogRan = MODEL_SCARED_DOG
						// waiting on an animation for this ped, the gestures dictionary was removed so currently she doesn't have an anim here
						//TASK_PLAY_ANIM(NULL, "gestures@female", "get_lost", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 2700, AF_EXIT_AFTER_INTERRUPTED)
						TASK_LOOK_AT_ENTITY(null, CELEBRITY_DOG, -1)
						TASK_PAUSE(NULL, 500)
					ELSE
						TASK_LOOK_AT_ENTITY(null, CELEBRITY_DOG, -1)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, CELEBRITY_DOG, 1500)
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, vModelDest, 2.2)
					TASK_CLEAR_LOOK_AT(NULL)
					TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "IDLE_D", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_PED, seq)
				clear_sequence_task(seq)	
			ENDIF
			bDoStopRunningConv = TRUE
			iWaitLineTimer = 0
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOW)
			
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_DOG_RUNS_AWAY")
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR bCelebPleaded = TRUE
				BOOL bConvDone
				bConvDone = FALSE
				IF IS_PED_UNINJURED(CELEBRITY_PED)
				AND IS_PED_UNINJURED(PLAYER_PED_ID())
				AND IS_ENTITY_IN_RANGE_ENTITY(CELEBRITY_PED, PLAYER_PED_ID(), 35.0)
					/*SWITCH reasonDogRan
						CASE SCARED_DOG
							bConvDone = CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_4", "NIG1C_CHASE", "NIG1C_CHASE_6", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						BREAK
						CASE SCARED_MODEL
							bCelebPleaded = TRUE
						BREAK
						CASE MODEL_SCARED_DOG
							bConvDone = CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_2", "NIG1C_CHASE", "NIG1C_CHASE_3", "NIG1C_CHASE", "NIG1C_CHASE_6", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						BREAK
						CASE PUSHED_MODEL
							bConvDone = CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_7", "NIG1C_CHASE", "NIG1C_CHASE_3", "NIG1C_CHASE", "NIG1C_CHASE_6", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						BREAK
					ENDSWITCH*/
					bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_CHASE", "NIG1C_CHASE_1", CONV_PRIORITY_VERY_HIGH, CheckDisplaySubtitles())
				ELSE
					// player is too far away to hear Kerry so don't bother with the conversation
					bConvDone = TRUE
				ENDIF
				IF bConvDone
					stageStage = SS_STAGE_2
				ENDIF
			ENDIF
			
			IF PLAYER_CAUGHT_DOG()
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_DOG_RUNS_AWAY")
				FINISHED_CHASE_EARLY()
			ENDIF
			
			UPDATE_CHASE_BLIP(GOTO_BLIP, CELEBRITY_DOG, fChaseFailDistance, fBlipFlashDistance)
		
			SET_PED_MIN_MOVE_BLEND_RATIO(CELEBRITY_DOG, 2.0)//1.5)
			//SET_PED_MOVE_RATE_OVERRIDE(CELEBRITY_DOG, 1.15)
		BREAK
		CASE SS_STAGE_2
			
			UPDATE_CHASE_BLIP(GOTO_BLIP, CELEBRITY_DOG, fChaseFailDistance, fBlipFlashDistance)
		
			SET_PED_MIN_MOVE_BLEND_RATIO(CELEBRITY_DOG, 3.0)//2.5)
			//SET_PED_MOVE_RATE_OVERRIDE(CELEBRITY_DOG, 1.15)
			
			MODEL_SPEAKS_WHEN_STOPPING()
			
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(CELEBRITY_DOG)		
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_DOG_RUNS_AWAY")
				stageStage = SS_SETUP
				missionStage = MS_DOG_RUNS_ON_ROAD
			ENDIF
			
			IF PLAYER_CAUGHT_DOG()
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_DOG_RUNS_AWAY")
				FINISHED_CHASE_EARLY()
			ENDIF	
			
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The dog runs onto the road, causing several car crashes
PROC DOG_RUNS_ON_ROAD()
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	//SET_PED_MOVE_RATE_OVERRIDE(CELEBRITY_DOG, 1.15)
	SWITCH stageStage	
		CASE SS_SETUP			
		
			LOAD_OTHER_ASSETS()
			//IF ARE_ASSETS_LOADED()
				
				bRELEASE_SENTINEL_CAR = FALSE
				bRELEASE_SPORTS_CAR = FALSE
				bCAR_HONKING = FALSE
				
				bSHOP_PED_SHOUTED = FALSE
				bTREVOR_RESPONDED = FALSE
				bCRASH_SHOUTED = FALSE
				
				bFinal_CHASE_LINES_READY = FALSE
				bWHAT_YOU_LOOKIN_AT_LINES_READY = FALSE
				
				CLEAR_AREA_OF_VEHICLES(<<-628.896301,-183.174133,36.762650>>, 50, TRUE)	
				CLEAR_AREA_OF_VEHICLES(<<-679.264893,-250.060471,35.605679>>, 21.75, TRUE)	
				CLEAR_AREA_OF_VEHICLES(<<-655.583008,-298.519806,34.453159>>, 39.75, TRUE)		
				CLEAR_AREA_OF_VEHICLES(<<-670.700378,-99.161484,36.779835>>, 60, TRUE)
				CLEAR_AREA_OF_VEHICLES(<<-561.318176,-153.089767,36.634796>>, 50, TRUE)		
				CLEAR_AREA_OF_VEHICLES(<<-590.626282,-247.092285,34.694180>>, 35, TRUE)
				CLEAR_AREA_OF_VEHICLES(vFirestationClearLocation, 50)
				
				IF NOT CREATE_TRAFFIC_VEHICLES()
					EXIT
				ENDIF
				
				crashStage = CS_WAITING
				jeepStage = JS_WAITING
				
				CREATE_SHOCKABLE_PED(0)
				CREATE_SHOCKABLE_PED(1)
				CREATE_SHOCKABLE_PED(2)
				CREATE_SHOCKABLE_PED(4)
				
				IF IS_PED_UNINJURED(shockedPed[0].index)
					TASK_START_SCENARIO_IN_PLACE(shockedPed[0].index, "WORLD_HUMAN_WINDOW_SHOP_BROWSE", 0, TRUE)
				ENDIF
				IF IS_PED_UNINJURED(shockedPed[2].index)
					TASK_START_SCENARIO_IN_PLACE(shockedPed[2].index, "WORLD_HUMAN_WINDOW_SHOP_BROWSE", 0, TRUE)
				ENDIF
				IF IS_PED_UNINJURED(shockedPed[4].index)
					TASK_START_SCENARIO_IN_PLACE(shockedPed[4].index, "WORLD_HUMAN_STAND_MOBILE", 0)
				ENDIF
				
				IF IS_PED_UNINJURED(shockedPed[1].index)							
					TASK_START_SCENARIO_IN_PLACE(shockedPed[1].index, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
					SET_PED_COMPONENT_VARIATION(shockedPed[1].index, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(shockedPed[1].index, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(shockedPed[1].index, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(shockedPed[1].index, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(taxiModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(sentinelCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(familyCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(escortCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(poshCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(businessManModel)
				//SET_MODEL_AS_NO_LONGER_NEEDED(fireManModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(taxiDriverModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(FIRETRUK)
				SET_MODEL_AS_NO_LONGER_NEEDED(jeepCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(femaleHipsterModel)				
				
				IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SECOND_CAR)				
					open_sequence_task(seq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, SECOND_CAR.index, <<-758.84, -115.64, 36.89>>, 12, DRIVINGSTYLE_NORMAL, SECOND_CAR.carModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
					close_sequence_task(seq)
					task_perform_sequence(SECOND_CAR.driver, seq)
					clear_sequence_task(seq)				
				ENDIF
								
				IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FIRST_CAR)				
					open_sequence_task(seq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-655.18, -202.16, 36.43>>, 18, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-643.56, -196.16, 36.58>>, 10, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-633.40, -191.10, 36.67>>, 5, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
					close_sequence_task(seq)
					task_perform_sequence(FIRST_CAR.driver, seq)
					clear_sequence_task(seq)				
				ENDIF
				
				/*TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG, -1)*/
				
				shockedPed[0].startedWalking = FALSE
				shockedPed[0].beenShocked = FALSE
				shockedPed[1].startedWalking = FALSE
				shockedPed[1].beenShocked = FALSE
				shockedPed[4].startedWalking = FALSE
				shockedPed[4].beenShocked = FALSE
						
				bWHAT_YOU_LOOKIN_AT_LINES_READY = TRUE
				bFinal_CHASE_LINES_READY = TRUE		
				bFirstCarsWaiting = TRUE
				bSeconCarRepositioned = FALSE
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_LOW)
				
				CPRINTLN(DEBUG_MISSION, "Setup DOG_RUNS_ON_ROAD")
				stageStage = SS_STAGE
			//ENDIF
		BREAK
		CASE SS_STAGE
			
			UPDATE_CHASE_BLIP(GOTO_BLIP, CELEBRITY_DOG, fChaseFailDistance, fBlipFlashDistance)
			
			IF IF_CAN_PLAY_CHASE_DIALOGUE()
				DEAL_WITH_RANDOM_DIALOGUE()
			ENDIF
			
			IF bFirstCarsWaiting = TRUE
				IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 7
					IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SECOND_CAR)				
						open_sequence_task(seq)		
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, SECOND_CAR.index, <<-758.84, -115.64, 36.89>>, 12, DRIVINGSTYLE_NORMAL, SECOND_CAR.carModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
						close_sequence_task(seq)
						task_perform_sequence(SECOND_CAR.driver, seq)
						clear_sequence_task(seq)				
					ENDIF
									
					IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(FIRST_CAR)				
						open_sequence_task(seq)		
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-655.18, -202.16, 36.43>>, 18, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-643.56, -196.16, 36.58>>, 10, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, FIRST_CAR.index, <<-633.40, -191.10, 36.67>>, 5, DRIVINGSTYLE_NORMAL, escortCarModel, DF_SteerAroundPeds | DF_StopForCars, 1,1)
						close_sequence_task(seq)
						task_perform_sequence(FIRST_CAR.driver, seq)
						clear_sequence_task(seq)				
					ENDIF
					bFirstCarsWaiting = FALSE
				ENDIF
			ELSE
				IF bSeconCarRepositioned = FALSE
					IF CHECK_IF_VEHICLE_AND_DRIVER_ARE_ALIVE(SECOND_CAR)	
						IF GET_THE_CLOSEST_WAYPOINT_TO_PLAYER() > 25
							CLEAR_PED_TASKS(SECOND_CAR.driver)
							SET_ENTITY_COORDS(SECOND_CAR.index, <<-678.85, -95.08, 36.86>>)
							SET_ENTITY_HEADING(SECOND_CAR.index, 208.7)
							TASK_VEHICLE_DRIVE_TO_COORD(SECOND_CAR.driver, SECOND_CAR.index, <<-567.85, -288.80, 34.08>>, 12, DRIVINGSTYLE_NORMAL, taxiModel, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 5,5)
							bSeconCarRepositioned = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF jeepStage = JS_WAITING
				IF GET_THE_CLOSEST_WAYPOINT_ON_DOG_ROAD_ROUTE() > 12
					jeepStage = JS_SET_TO_DRIVE
				ENDIF
			ENDIF
			
			MODEL_SPEAKS_WHEN_STOPPING()
			
			UPDATE_THE_SHOCKABLE_PED(0)		
			UPDATE_THE_SHOCKABLE_PED(1)		
			UPDATE_THE_FALL_PED()
			
			UPDATE_THE_JEEP()
			UPDATE_THE_CRASH()
			UPDATE_THE_SKIDDING_TAXI()	
			// This will probably go back in so just commenting it out. B* 1107526
			//UPDATE_THE_FIRE_TRUCK()
			UPDATE_THE_OTHER_CAR()			
			
			//UPDATE_DOG_RUN_SPEED()	
	
			IF IS_ENTITY_AT_COORD(CELEBRITY_DOG, vPOS_FINAL_WAYPOINT, <<3,3,2>>)
				CPRINTLN(DEBUG_MISSION, "Finish DOG_RUNS_ON_ROAD")
				stageStage = SS_SETUP
				missionStage = MS_DOG_WAITING_AT_FIRESTATION			
			ENDIF
			
			IF PLAYER_CAUGHT_DOG()
				CPRINTLN(DEBUG_MISSION, "Finish DOG_RUNS_ON_ROAD")
				FINISHED_CHASE_EARLY()
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    The dog waits for the player in the firestation
PROC STAGE_DOG_WAITING_AT_FIRESTATION()
	SWITCH stageStage	
		CASE SS_SETUP
			
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				CLEAR_PED_TASKS(CELEBRITY_DOG)
				open_sequence_task(seq)
					//TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
					/*TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PAUSE(NULL, 2000)
					TASK_WANDER_IN_AREA(NULL, <<-636.45,-98.21,37.04>>, 3.75)*/
					TASK_TURN_PED_TO_FACE_COORD(NULL, <<-641.7725, -104.2859, 37.0147>>)
					TASK_PLAY_ANIM(NULL, DOG_DICT, "chop_growl", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_DOG, seq)
				clear_sequence_task(seq)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(CELEBRITY_DOG, TRUE)
			ENDIF
			
			TURN_ROADS_ON(TRUE)
			
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_DOG_WAITING_AT_FIRESTATION")
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_LOW)
			
			// want the dog to growl immediately the first time
			//iGrowlTimer = GET_GAME_TIMER()
			ADD_PED_FOR_DIALOGUE(sSpeech, 6, CELEBRITY_DOG, "MrMuffyCakes")
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE
			
			UPDATE_CHASE_BLIP(GOTO_BLIP, CELEBRITY_DOG, fChaseFailDistance, fBlipFlashDistance)
		
			UPDATE_THE_JEEP()
			UPDATE_THE_CRASH()
			UPDATE_THE_SKIDDING_TAXI()	
			// This will probably go back in so just commenting it out. B* 1107526
			//UPDATE_THE_FIRE_TRUCK()
			UPDATE_THE_OTHER_CAR()	
			
			/*IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), CELEBRITY_DOG) < 12
				IF GET_GAME_TIMER() > iGrowlTimer
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_DOG", "NIG1C_DOG_6",CONV_PRIORITY_MEDIUM)
							iGrowlTimer = GET_GAME_TIMER() + 8000
						ENDIF
					ENDIF
				ENDIF
			ENDIF*/
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), CELEBRITY_DOG)
					SET_PED_TO_RAGDOLL(CELEBRITY_DOG, 500, 1000, TASK_RELAX, FALSE, FALSE)
					SET_ENTITY_HEALTH(CELEBRITY_DOG, 0)
				ENDIF
			ENDIF
			
			IF IF_CAN_PLAY_CHASE_DIALOGUE()
				DEAL_WITH_RANDOM_DIALOGUE()
			ENDIF
			
			FLOAT fDistMod
			IF GET_PED_DESIRED_MOVE_BLEND_RATIO(PLAYER_PED_ID()) = PEDMOVE_WALK
				fDistMod = 1.0
			ELSE
				fDistMod = 2.0
			ENDIF
			IF PLAYER_CAUGHT_DOG(fDistMod)
			AND NOT IS_PED_FALLING(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "Finish STAGE_DOG_WAITING_AT_FIRESTATION")
				stageStage = SS_SETUP
				missionStage = MS_GRAB_DOG_CUTSCENE
			ENDIF
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Plays the scripted cutscene in which the player takes the dog's collar
PROC STAGE_GRAB_DOG_CUTSCENE()
	SWITCH stageStage	
		CASE SS_SETUP
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_LOW)
			
			CHECK_EACH_VEHICLE_FOR_CAR_RECORDING()

			ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), vAlleyCutscenePos1, vAlleyCutscenePos2, fAlleyCutsceneWidth)
				SAFE_FADE_SCREEN_OUT_TO_BLACK()
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vAlleyCutscenePos1, vAlleyCutscenePos2, fAlleyCutsceneWidth, vEmergencyCarRespot, fEmergencyCarRespot, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RC_START_CUTSCENE_MODE(vEmergencyOutro)
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vEmergencyOutro)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fEmergencyOutro)
			ELSE
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-636.693176,-96.585403,36.547657>>, <<-628.838684,-97.430687,38.069145>>, 7.0, <<-641.35, -99.50, 37.62>>, 135.52, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RC_START_CUTSCENE_MODE(<<-636.7341, -99.1310, 37.0476>>)
			ENDIF
			
			REQUEST_ANIM_DICT(OUTRO_DICT)
			while not HAS_ANIM_DICT_LOADED(OUTRO_DICT)
				WAIT(0)
			endwhile
			
			/*camCutsceneStart = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			SET_CAM_PARAMS(camCutsceneStart, get_cam_pos(), <<-0.6717, -0.0000, -175.6980>>, 50)
			POINT_CAM_AT_ENTITY(camCutsceneStart, PLAYER_PED_ID(), <<0,0,-0.1>>)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_CAM_ACTIVE(camCutsceneStart, TRUE)*/
			
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				CLEAR_PED_TASKS_IMMEDIATELY(CELEBRITY_DOG)
				//SET_DOG_POSITION_WHEN_CAUGHT()
			ENDIF
			
			STOP_SCRIPTED_CONVERSATION(TRUE)
			
			IF DOES_BLIP_EXIST(GOTO_BLIP)
				REMOVE_BLIP(GOTO_BLIP)
			ENDIF
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			IF IS_PED_UNINJURED(CELEBRITY_DOG)					
				/*open_sequence_task(seq)		
					//TASK_LOOK_AT_ENTITY(null, CELEBRITY_DOG, -1)
					//TASK_PLAY_ANIM(NULL, KNEEL_ENTER_DICT, "enter", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_DEFAULT, 0.3)
					//TASK_PLAY_ANIM(NULL, KNEEL_DICT, "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
					//TASK_PLAY_ANIM(NULL, NIGEL1C_DICT, "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
					TASK_PAUSE(NULL, iPauseTime)
					TASK_PLAY_ANIM(NULL, OUTRO_DICT, "take_collar_trevor")//, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1
					TASK_PAUSE(NULL, -1)
				close_sequence_task(seq)
				task_perform_sequence(PLAYER_PED_ID(), seq)
				clear_sequence_task(seq)				
				
				open_sequence_task(seq)		
					TASK_PAUSE(NULL, iPauseTime)
					TASK_PLAY_ANIM(NULL, OUTRO_DICT, "take_collar_dog")
					TASK_PAUSE(NULL, -1)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_DOG, seq)
				clear_sequence_task(seq)*/
				
				VECTOR vStartPos
				vStartPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vStartPos)
				/*FLOAT fStartZ
				IF REPEAT_GET_GROUND_Z(vStartPos, fStartZ, 2)
					vStartPos.z = fStartZ
				ENDIF*/
				iSyncedSceneID = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()))
				SET_SYNCHRONIZED_SCENE_LOOPED(iSyncedSceneID,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncedSceneID,FALSE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSyncedSceneID, OUTRO_DICT, "take_collar_trevor", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(CELEBRITY_DOG, iSyncedSceneID, OUTRO_DICT, "take_collar_dog", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				camCutsceneStart = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(camCutsceneStart, iSyncedSceneID, "take_collar_cam", OUTRO_DICT)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			
			//while not CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(sSpeech, "NIG1CAU", "NIG1C_COLLAR", "NIG1C_COLLAR_1", "NIG1C_COLLAR", "NIG1C_COLLAR_2", CONV_PRIORITY_MEDIUM)
			WHILE NOT PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "NIG1CAU", "NIG1C_COLLAR", "NIG1C_COLLAR_2", CONV_PRIORITY_MEDIUM)
				STOP_SCRIPTED_CONVERSATION(TRUE)
				KILL_FACE_TO_FACE_CONVERSATION()
				WAIT(0)
			ENDWHILE
			
			SAFE_DELETE_PED(CELEBRITY_PED)
			
			//iOutroTimer = GET_GAME_TIMER() + 2000//(iPauseTime+1000)
			
			bPLAYED_COLLAR_SOUND = FALSE
			//bRESUMED_COLLAR_CONVERSATION = FALSE
		
			TURN_ROADS_ON(TRUE)
			
			TRIGGER_MUSIC_EVENT("NIGEL1C_END")
			
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_GRAB_DOG_CUTSCENE")
		
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE
			
			//if IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), NIGEL1C_DICT, "idle_b")
			//if IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), KNEEL_DICT, "base")
				IF bPLAYED_COLLAR_SOUND = FALSE AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncedSceneID) >= 0.75 //GET_GAME_TIMER() > iOutroTimer
				AND IS_PED_UNINJURED(CELEBRITY_DOG)
					//if NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//CLEAR_PRINTS()
						PLAY_SOUND_FROM_ENTITY(-1, "COLLAR", CELEBRITY_DOG, "NIGEL_1C_SOUNDSET")
						SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, PED_COMP_TORSO, 1, 0, 0) //(uppr)
						//iOutroTimer = GET_GAME_TIMER() + 1200
						bPLAYED_COLLAR_SOUND = TRUE
					//ENDIF
				ELSE
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, "NIG1CAU", "NIG1C_COLLAR", "NIG1C_COLLAR_3", CONV_PRIORITY_MEDIUM)
							//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							/*RENDER_SCRIPT_CAMS(FALSE, TRUE)
							IF DOES_CAM_EXIST(camCutsceneStart)
								SET_CAM_ACTIVE(camCutsceneStart, FALSE)
								DESTROY_CAM	(camCutsceneStart)
							ENDIF*/
							//FINISH_CUTSCENE()
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							IF DOES_CAM_EXIST(camCutsceneStart)
								SET_CAM_ACTIVE(camCutsceneStart, FALSE)
								DESTROY_CAM	(camCutsceneStart)
							ENDIF
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							RC_END_CUTSCENE_MODE()
							CPRINTLN(DEBUG_MISSION, "Finish STAGE_GRAB_DOG_CUTSCENE")
							stageStage = SS_SETUP
							missionstage = MS_STAGE_PLAY_OUTRO
						ENDIF
					ENDIF
					/*IF bRESUMED_COLLAR_CONVERSATION = FALSE
						IF GET_GAME_TIMER() > iOutroTimer
							IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, "NIG1CAU", "NIG1C_COLLAR", "NIG1C_COLLAR_3", CONV_PRIORITY_MEDIUM)
								//iOutroTimer = GET_GAME_TIMER() + 1000
								bRESUMED_COLLAR_CONVERSATION = TRUE
							ENDIF
						ENDIF
					ELSE
						//IF GET_GAME_TIMER() > iOutroTimer
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//TASK_PLAY_ANIM(PLAYER_PED_ID(), KNEEL_EXIT_DICT, "exit", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1)				
							//FINISH_CUTSCENE(FALSE, FALSE)
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
							IF DOES_CAM_EXIST(camCutsceneStart)
								SET_CAM_ACTIVE(camCutsceneStart, FALSE)
								DESTROY_CAM	(camCutsceneStart)
							ENDIF
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							RC_END_CUTSCENE_MODE()
							CPRINTLN(DEBUG_MISSION, "Finish STAGE_GRAB_DOG_CUTSCENE")
							stageStage = SS_SETUP
							missionstage = MS_STAGE_PLAY_OUTRO
						ENDIF
					ENDIF*/
				ENDIF
			//ENDIF			
			
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				stageStage = SS_STAGE_2
			ENDIF		

		BREAK	
		CASE SS_STAGE_2
			SAFE_FADE_SCREEN_OUT_TO_BLACK()
			RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, FALSE, FALSE, FALSE)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncedSceneID)
				SET_SYNCHRONIZED_SCENE_PHASE(iSyncedSceneID, 1.0)
			ENDIF
			WAIT(FLOW_FAIL_FADE_TIME+1000)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				STOP_SCRIPTED_CONVERSATION(FALSE)
			ENDIF
			/*IF IS_PED_UNINJURED(PLAYER_PED_ID())
				TASK_PLAY_ANIM(PLAYER_PED_ID(), KNEEL_EXIT_DICT, "exit", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1)
			ENDIF*/
			IF IS_PED_UNINJURED(CELEBRITY_DOG)
				SET_PED_COMPONENT_VARIATION(CELEBRITY_DOG, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			ENDIF
			FINISH_CUTSCENE(FALSE, TRUE)
			CPRINTLN(DEBUG_MISSION, "Finish STAGE_GRAB_DOG_CUTSCENE")
			stageStage = SS_SETUP
			missionstage = MS_STAGE_PLAY_OUTRO
			RC_END_CUTSCENE_MODE()
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		BREAK
	ENDSWITCH
	
ENDPROC

//----------------------------
//OUTRO
//----------------------------

/// PURPOSE:
///    Makes the dog wakl away and plays the end of mission phone call with Nigel and Mrs Thornhill
PROC STAGE_PLAY_OUTRO()
	SWITCH stageStage	
		CASE SS_SETUP		
			//TASK_SMART_FLEE_PED(CELEBRITY_DOG, PLAYER_PED_ID(), 100, -1, TRUE)			
			
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
			ENDIF
			
			IF IS_PED_UNINJURED(CELEBRITY_DOG)	
				CLEAR_PED_TASKS(CELEBRITY_DOG)
				open_sequence_task(seq)
					//TASK_PAUSE(NULL, 1000)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CELEBRITY_DOG, <<0,2,0>>), 2.5)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(CELEBRITY_DOG, <<0.5,1,0>>), 2.5)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -650.2209, -120.7035, 36.8320 >>, 2.5, DEFAULT_TIME_BEFORE_WARP, 1)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 50, -1, TRUE)
				close_sequence_task(seq)
				task_perform_sequence(CELEBRITY_DOG, seq)
				clear_sequence_task(seq)
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("NIGEL_1C_SCENE")
				STOP_AUDIO_SCENE("NIGEL_1C_SCENE")
			ENDIF
			
			IF IS_PED_UNINJURED(CELEBRITY_PED)
				IF GET_DISTANCE_BETWEEN_PEDS(CELEBRITY_PED, PLAYER_PED_ID()) < 30
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(CELEBRITY_PED, CELEBRITY_DOG, <<0,0,0>>, 2, -1, 3)
				ENDIF
			ENDIF
			iOutroTimer = -1 // GET_GAME_TIMER() + 3750
			CPRINTLN(DEBUG_MISSION, "Setup STAGE_PLAY_OUTRO")
			stageStage = SS_STAGE
		BREAK
		CASE SS_STAGE	
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF iOutroTimer < 0
					iOutroTimer = GET_GAME_TIMER() + 3000
				ELIF GET_GAME_TIMER() > iOutroTimer
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())		
			
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
			
					ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "NIGEL")
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, NULL, "MRSTHORNHILL")
					IF PLAYER_CALL_CHAR_CELLPHONE(sSpeech, CHAR_NIGEL, "NIG1CAU", "NIG1C_OUT", CONV_PRIORITY_VERY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_LOW)
						CPRINTLN(DEBUG_MISSION, "Finish STAGE_PLAY_OUTRO")
						missionStage = MS_WAIT_FOR_CALL_TO_END
					ENDIF
				ENDIF
			/*ELSE
				iOutroTimer = GET_GAME_TIMER() + 750*/
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Waits for the phone call to end, passes the mission when it has
PROC STAGE_WAIT_FOR_CALL_TO_END()
	
	IF bOUTRO_CALL_GOING_ON = FALSE
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			bOUTRO_CALL_GOING_ON = TRUE
		ENDIF
	ENDIF
	
	IF bOUTRO_CALL_GOING_ON = TRUE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CLEAR_PRINTS()
			Script_Passed()	
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades out when fading the mission
PROC FAIL_WAIT_FOR_FADE()
	SWITCH stageStage
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "*******************    MISSION FAILED    *********************")
			KILL_FACE_TO_FACE_CONVERSATION()
				
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF			
			CLEAR_PRINTS()
			CLEAR_HELP()
			// remove blips
			IF DOES_BLIP_EXIST(GOTO_BLIP)
				REMOVE_BLIP(GOTO_BLIP)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "CASE SS_SETUP")
			
			TRIGGER_MUSIC_EVENT("NIGEL1C_FAIL")
			
			IF NOT IS_PED_UNINJURED(CELEBRITY_DOG)
				failReason = FAIL_KILLED_DOG
			ENDIF
			
			STRING sFailReason
			
			SWITCH failReason
				CASE FAIL_CELEB_KILLED
					sFailReason = "N1C_F_EXMOD"		// ~r~Kerry died.~s~
				BREAK		
				CASE FAIL_KILLED_DOG
					sFailReason = "N1C_F_EXDOG"		// ~r~Dexie died.~s~
				BREAK
				CASE FAIL_CELEB_INJURED
					sFailReason = "N1C_F_MOD"		// ~r~You injured Kerry.~s~
				BREAK	
				CASE FAIL_CELEB_SCARED
					sFailReason = "N1_C_F_THREAT"	// ~r~You scared Kerry.~s~
				BREAK
				CASE FAIL_INJURED_DOG
					sFailReason = "N1C_F_DOG"		// ~r~You injured Dexie.~s~
				BREAK	
				CASE FAIL_LOST_DOG	
					sFailReason = 	"N1C_F_LOST"	// ~r~You lost Dexie.~s~
				BREAK	
				DEFAULT
					// no fail text
				BREAK
			ENDSWITCH
			
			IF failReason = FAIL_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)//, TRUE, bDelayFade)
			ENDIF

			CPRINTLN(DEBUG_MISSION, "Setup FAIL_WAIT_FOR_FADE")
			stageStage = SS_STAGE
		BREAK
		
		CASE SS_STAGE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()

				CPRINTLN(DEBUG_MISSION, "Finish FAIL_WAIT_FOR_FADE")
				DELETE_EVERYTHING()
				
				IF IS_PHONE_ONSCREEN()
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF
				#IF IS_DEBUG_BUILD
					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						CPRINTLN(DEBUG_MISSION, "WAITING FOR THE CONVO TO END BEFORE CLEANING UP")
						KILL_ANY_CONVERSATION()
						WAIT(0)
					ENDWHILE
				#ENDIF
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		DEBUG
// ===========================================================================================================

/// PURPOSE:
///    Checks for debug pass or fail and skips stages if necessary
#IF IS_DEBUG_BUILD
	PROC DEBUG_Check_Debug_Keys()

		IF missionStage <> MS_FAIL_WAIT_FOR_FADE
			// Check for Pass
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				Script_Passed()
			ENDIF
			// Check for Fail
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))			
				failReason = FAIL_DEFAULT
				stageStage = SS_SETUP
				missionStage = MS_FAIL_WAIT_FOR_FADE
			ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) //skip current stage
				SWITCH missionStage	
					CASE MS_GO_TO_SHOPS
						JUMP_TO_STAGE(DEBUG_TO_STORES)
					BREAK
					CASE MS_STAGE_APPROACH_CELEB
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK
					CASE MS_STAGE_CHAT_UP_CELEB
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK
					CASE MS_DOG_RUNS_AWAY
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK
					CASE MS_DOG_RUNS_ON_ROAD
						JUMP_TO_STAGE(DEBUG_TO_END_CUTSCENE)				
					BREAK	
					CASE MS_DOG_WAITING_AT_FIRESTATION
						JUMP_TO_STAGE(DEBUG_TO_END_CUTSCENE)
					BREAK
					CASE MS_GRAB_DOG_CUTSCENE
						CLEAR_PRINTS()
						Script_Passed()
					BREAK		
					CASE MS_STAGE_PLAY_OUTRO
						CLEAR_PRINTS()
						Script_Passed()
					BREAK		
					CASE MS_WAIT_FOR_CALL_TO_END
						CLEAR_PRINTS()
						Script_Passed()
					BREAK
				ENDSWITCH
			ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) //skip back
				SWITCH missionStage	
					CASE MS_GO_TO_SHOPS
						JUMP_TO_STAGE(DEBUG_TO_START)
					BREAK
					CASE MS_STAGE_APPROACH_CELEB
						JUMP_TO_STAGE(DEBUG_TO_START)
					BREAK
					CASE MS_STAGE_CHAT_UP_CELEB
						JUMP_TO_STAGE(DEBUG_TO_START)
					BREAK
					CASE MS_DOG_RUNS_AWAY
						JUMP_TO_STAGE(DEBUG_TO_STORES)
					BREAK
					CASE MS_DOG_RUNS_ON_ROAD
						JUMP_TO_STAGE(DEBUG_TO_STORES)
					BREAK	
					CASE MS_DOG_WAITING_AT_FIRESTATION
						JUMP_TO_STAGE(DEBUG_TO_STORES)
					BREAK	
					CASE MS_GRAB_DOG_CUTSCENE
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK			
					CASE MS_STAGE_PLAY_OUTRO
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK		
					CASE MS_WAIT_FOR_CALL_TO_END
						JUMP_TO_STAGE(DEBUG_TO_ROAD_CHASE)
					BREAK
				ENDSWITCH
			ENDIF

			IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
		        INT e_stage = i_debug_jump_stage
		        JUMP_TO_STAGE(INT_TO_ENUM(debugSkipToMissionStage, e_stage))
		    ENDIF
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()

	SET_MISSION_FLAG(TRUE)
	
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		TRIGGER_MUSIC_EVENT("NIGEL1C_FAIL")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF	
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_NIGEL_1C(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("NIGEL_1C_SCENE")
		START_AUDIO_SCENE("NIGEL_1C_SCENE")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SETUP_WIDGETS()
	#ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
				
	IF IS_REPLAY_IN_PROGRESS()
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF
		
		SWITCH iReplayStage
			CASE CP_START
				CPRINTLN(DEBUG_MISSION, "Replay at Start")
				/*LOAD_INITIAL_ASSETS(TRUE)
				MISSION_INIT()
				JUMP_TO_STAGE(DEBUG_TO_CHECKPOINT)
			BREAK
			
			CASE CP_CELEB
				CPRINTLN(DEBUG_MISSION, "Replay at celeb")*/
				START_REPLAY_SETUP(<< -605.2227, -291.6463, 35.7791 >>, 38.21)
				RC_START_Z_SKIP()
				LOAD_INITIAL_ASSETS(TRUE)
				MISSION_INIT()
				JUMP_TO_STAGE(DEBUG_TO_STORES)
			BREAK
			
			CASE CP_MISSION_PASSED
				CPRINTLN(DEBUG_MISSION, "Replay mission passed")
				/*SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-636.6826, -97.2394, 37.1276>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 152.2261)
				WAIT(0)
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)*/
				START_REPLAY_SETUP(<<-636.6826, -97.2394, 37.1276>>, 152.2261)
				VEHICLE_INDEX viTemp
				CREATE_VEHICLE_FOR_REPLAY(viTemp, << -649.19, -105.83, 37.63 >>, 125.45, FALSE, FALSE, FALSE, TRUE, TRUE)
				END_REPLAY_SETUP()
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
				RC_END_Z_SKIP()
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(NI1C_PLAYER_GOT_TOO_FAR_FROM_MUFFY)
				bBEEN_WARNED_ABOUT_DISTANCE = TRUE
				Script_Passed()
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Invalid replay checkpoint")
			BREAK
		ENDSWITCH
	ELSE
		IF IS_REPEAT_PLAY_ACTIVE()
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-604.1522, -292.7876, 35.7793>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 30.3570)
				WAIT(0)
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			ENDIF
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
	ENDIF
		
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_VSTLA")
	
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
			
		SWITCH missionStage
			CASE MS_INIT
				MISSION_INIT()
			BREAK
			CASE MS_GO_TO_SHOPS
				STAGE_GO_TO_SHOPS()
			BREAK			
			CASE MS_STAGE_APPROACH_CELEB
				STAGE_APPROACH_CELEBRITY()
			BREAK			
			CASE MS_STAGE_CHAT_UP_CELEB
				STAGE_CHAT_UP_CELEB()
			BREAK			
			CASE MS_DOG_RUNS_AWAY
				STAGE_DOG_RUNS_AWAY()
			BREAK				
			CASE MS_DOG_RUNS_ON_ROAD	
				DOG_RUNS_ON_ROAD()
			BREAK				
			CASE MS_DOG_WAITING_AT_FIRESTATION
				STAGE_DOG_WAITING_AT_FIRESTATION()				
			BREAK		
			CASE MS_GRAB_DOG_CUTSCENE
				STAGE_GRAB_DOG_CUTSCENE()				
			BREAK
			CASE MS_STAGE_PLAY_OUTRO
				STAGE_PLAY_OUTRO()
			BREAK
			CASE MS_WAIT_FOR_CALL_TO_END
				STAGE_WAIT_FOR_CALL_TO_END()
			BREAK		
			CASE MS_FAIL_WAIT_FOR_FADE
				FAIL_WAIT_FOR_FADE()
			BREAK
			
		ENDSWITCH
	
		if missionStage <> MS_FAIL_WAIT_FOR_FADE
			CHECK_FAIL_POSSIBILITIES()
			CHECK_IF_PLAYER_THREATENED_MILAN()
		ENDIF
		
		WAIT(0)
		
		// Loop within here until the mission passes or fails
		
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

