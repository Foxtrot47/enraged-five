
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "RC_Helper_Functions.sch"
USING "CompletionPercentage_public.sch"
USING "clearMissionArea.sch"
USING "initial_scenes_barry.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Barry4.sc
//		AUTHOR			:	Tom Waters
//		DESCRIPTION		:	Barry thread ending cutscene
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Mission state enum
ENUM eRC_MainState
	RC_INTRO
ENDENUM

ENUM eRC_SubState
	SS_ASSET_REQUEST,
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

eRC_MainState		m_state 	   = RC_INTRO		  //Main states of the mission
eRC_SubState		m_subState     = SS_ASSET_REQUEST //Mini state machine for each state the mission goes into
g_structRCScriptArgs sRCLauncherDataLocal
BOOL bHasInitialPhonecallSucceeded = FALSE
structPedsForConversation sDialogue

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely cleans up the script
/// PARAMS:
///    None.
/// RETURNS:
///    N/A
PROC Script_Cleanup()
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)
	TERMINATE_THIS_THREAD()
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
/// PARAMS:
///    None.
/// RETURNS:
///    N/A
PROC Script_Passed()
	Random_Character_Passed(CP_RAND_C_BAR4, FALSE)
	Script_Cleanup()
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

// ===========================================================================================================
//		DEBUG
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for Forced Pass or Fail
	/// PARAMS:
	///    None.
	/// RETURNS:
	///    N/A
	PROC DEBUG_Check_Debug_Keys()
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    Play intro cutscene state
PROC STATE_IntroCutscene()
	
	SWITCH m_subState
	
		CASE SS_ASSET_REQUEST
			#IF IS_DEBUG_BUILD
				ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, FRANKLIN_BOOK, FALSE)
			#ENDIF
			IF IS_REPLAY_IN_PROGRESS()	
				START_REPLAY_SETUP(<<243.3670, -387.3153, 44.4046>>, 175.2)
				END_REPLAY_SETUP()
			ENDIF
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			m_subState = SS_Setup
		BREAK
		
		CASE SS_Setup
			CPRINTLN(DEBUG_MISSION, "Init RC_INTRO") 
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_PHONE_CONVERSATION()
			ENDIF			
			ADD_PED_FOR_DIALOGUE(sDialogue, 1, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(sDialogue, 3, NULL, "BARRY")
			bHasInitialPhonecallSucceeded = FALSE
			m_subState 					  = SS_Update
		BREAK
		
		CASE SS_Update
			IF NOT bHasInitialPhonecallSucceeded
				IF PLAYER_CALL_CHAR_CELLPHONE(sDialogue, CHAR_BARRY, "BARR4AU", "BAR4_RCM_LI", CONV_PRIORITY_CELLPHONE)
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 20.0, REPLAY_IMPORTANCE_LOW)
					bHasInitialPhonecallSucceeded = TRUE
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_CELLPHONE_CALL_FINISHED()
						m_subState = SS_Cleanup
					ENDIF
				ELSE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_Cleanup
			CPRINTLN(DEBUG_MISSION, "Cleaning up RC_INTRO") 
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	// Set up the initial scene for replays
	IF Is_Replay_In_Progress()
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_BARRY_4(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		VEHICLE_INDEX vTmp
		CREATE_VEHICLE_FOR_REPLAY(vTmp, <<236.1378, -369.3902, 43.2497>>, 251.2, FALSE, FALSE, FALSE, TRUE, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF

	WHILE(TRUE)
		
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_GRTSI")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Loop within here until the mission passes or fails
		SWITCH(m_state)
			CASE RC_INTRO
				STATE_IntroCutscene()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
