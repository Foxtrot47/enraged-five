// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Barry1.sc
//		AUTHOR			:	Aaron Gandaa / Stefan Arnold
//		DESCRIPTION		:	First Barry Mission
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "commands_shapetest.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "rgeneral_include.sch"
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Area_public.sch"
USING "CompletionPercentage_public.sch"
USING "replay_public.sch"
USING "shop_public.sch"
USING "initial_scenes_barry.sch"
USING "RC_Asset_public.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
USING "taxi_functions.sch"

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
#ENDIF

//----------------------
//	ENUMS
//----------------------
ENUM ALIEN_STATE
	AS_NULL,			
	AS_DELAY,			// this is the wait between us creating the alien and him starting teleport
	AS_TELEPORT_FXSTART,
	AS_TELEPORT,
	AS_TELEPORT_OUT_START,
	AS_TELEPORT_OUT,
	AS_RELOCATE,
	AS_DYING,
	AS_DEAD,
	AS_ACTIVE,			// all battle states need to come after here or IS_ALIEN_IN_BATTLE_STATE() will break
	AS_SUMMON_RUSH,		// run towards the player
	AS_SUMMON_START,	
	AS_SUMMON_FAIL,
	AS_WATCH_SUMMON,
	AS_WATCH_SUMMON_END,
	AS_CHARGE_RUSH,
	AS_CHARGE_START,
	AS_CHARGE,
	AS_BEAM_START,
	AS_BEAM,
	AS_BEAM_KNOCKDOWN
ENDENUM

ENUM STAGE_ENUM
	SE_LEAD_IN = -1,
	SE_INTRO = 0,
	SE_FIRSTCONTACT,
	SE_WAVE1,
	SE_WAVE2,
	SE_PASSOUT,
	SE_OUTRO,
	SE_FAILED,
	SE_ABDUCT
ENDENUM

ENUM PASS_OUT_STAGE
	POS_SETUP,
	POS_WAIT_FOR_TIRED,
	POS_START_DIALOG,
	POS_WAIT_DIALOGUE,
	POS_TRANSITION_FX,
	POS_WAIT_PASS_OUT
ENDENUM

//----------------------
//	STRUCTURES
//----------------------
STRUCT ALIEN_PED
	BOOL bIsActive = FALSE
	ALIEN_STATE aState
	
	PED_INDEX pedID
	BLIP_INDEX blipID
	OBJECT_INDEX projectorID
	OBJECT_INDEX impactID
	OBJECT_INDEX screenImpactID
	
	INT iTimer
	INT iNextDamageTime
	INT iLastImpactFrame
	FLOAT fDistToPlayer
	FLOAT fRunRatio
	FLOAT fBeamLength
	FLOAT fBeamEvoValue
	FLOAT fBeamTriggerRange		// fAlienBeamStartRange + (rand (-fAlienBeamTriggerVariance, fAlienBeamTriggerVariance) * fAlienBeamStartRange)
	FLOAT fPedAlpha = 0.0
	
	INT iClickSoundTimer
	INT mindCtrlSoundID = -1
	INT iButtonMashCount = 0
	INT iNextSummonEffectTime 
	INT fOldMashSign = 0
	
	PTFX_ID fxSummonID
	PTFX_ID fxChargeID
	PTFX_ID fxBeamID
	PTFX_ID fxImpactID
	PTFX_ID fxScreenImpactID
	
	SHAPETEST_INDEX sBeamLOS
	VECTOR vLastHitPoint
	INT iAlienIndex
	BOOL bHasDamaged = FALSE
	BOOL bWallSpawn = FALSE
ENDSTRUCT

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MISSION_VERSION					015		// this so i can be sure that my changes have been accepted
CONST_INT MAX_ALIEN_PEDS					8 		// reducing from 10 to reduce difficulty

CONST_INT GENDELAY_TIME						500
CONST_INT TELEPORT_TIME						450
CONST_INT ALIEN_FADE_TIME					1000

CONST_INT CHAIN_KILL_AMOUNT					5		// number of aliens to kill to do the chain
CONST_INT CHAIN_KILL_TIME					10000	// number of ms to complete the chain
CONST_INT MUSICEVENT_RC18A_START_DELAY  	58500	// 33500 	// 2m 07s in the FOV changes in cutscene
CONST_INT MUSICEVENT_RC18A_INCREASE_DELAY  	70500	//	45500	// 12 seconds after  MUSICEVENT_RC18A_START_DELAY 

CONST_INT FAIL_WARNING_TIME					10000

// @SBA - Max aliens after initial rush
CONST_INT kiMaxStartingAliens				3

// @SBA - number of aliens considered already dead if you start in wave2 (this is currently for debug z-skip)
CONST_INT kiDeadAliensStartingWave2			29

// match up with object array in g_structRCScriptArgs (see initial_scenes_barry.sch)
CONST_INT OBJ_TABLE 	0
CONST_INT OBJ_CHAIR 	1
CONST_INT OBJ_PAPERS 	2
CONST_INT OBJ_LIGHTER 	3
CONST_INT OBJ_JOINT 	4
CONST_INT OBJ_SIGN		5

//----------------------
//	CHECKPOINTS
//----------------------
// @SBA - currently not using this, and it wasn't fully implemented yet
//CONST_INT CP_MISSION_START				0
//CONST_INT CP_MID_STAGE					1
//CONST_INT CP_OUTRO						2

CONST_INT Z_SKIP_INTRO					0
CONST_INT Z_SKIP_BATTLE					1
CONST_INT Z_SKIP_BATTLE_2				2	// @SBA - inserting this
CONST_INT Z_SKIP_PASSOUT				3
CONST_INT Z_SKIP_OUTRO					4
CONST_INT MAX_SKIP_MENU_LENGTH			5

//----------------------
//	RC VARIABLES
//----------------------
g_structRCScriptArgs sRCLauncherDataLocal

//----------------------
//	VARIABLES
//----------------------
INT iStateTimeOut
INT iFailTimer
STRING sFailString = ""
STAGE_ENUM stageEnum = SE_LEAD_IN
PASS_OUT_STAGE ePassOutStage
STAGE_PROGRESS stageProgress
OBJECT_INDEX gunObject

INT iDbgStageJump // now also used in checkpoint / replay stuff
SCENARIO_BLOCKING_INDEX scenarioBlock
VECTOR vPlayerCarRespot = <<174.6435, -1012.3608, 28.3152>>
FLOAT fPlayerCarRespot = 30.2623
VECTOR vBattleCenter = <<180.5319, -970.6324, 29.0923>>
FLOAT fBattleRadius = 100.0
VECTOR vMissionStart = << 188.86, -955.83, 30.09 >>
FLOAT fMissionStart = 62.08

FLOAT fPlayerWeaponDamageModifier = 1.0
FLOAT fRegenMultiplier = 1.0
FLOAT fTimeScale = 0.8

BOOL bIsPlayerInBattleArea = TRUE
INT iMissionAbandonedTimer = 0
BLIP_INDEX locationBlip
BOOL bDebugTTYSpew = FALSE
BOOL bUseIsAreaOccupied = FALSE
INT iAlphaCounter = 0
INT iSummonEscapeCount = 0

// @SBA - Weapons
WEAPON_TYPE			wtSniperRifle			= WEAPONTYPE_SNIPERRIFLE
WEAPON_TYPE			wtPistol				= WEAPONTYPE_COMBATPISTOL
WEAPON_TYPE			wtGrenLauncher			= WEAPONTYPE_GRENADELAUNCHER
//WEAPON_TYPE		wtStunGun				= WEAPONTYPE_STUNGUN
WEAPON_TYPE			wtMiniGun				= WEAPONTYPE_MINIGUN

ANGLED_AREA respotVehicleArea
ANGLED_AREA missionBoundary
FLOAT fPlayerAlpha = 255.0
WEAPON_TYPE playerWeapon = WEAPONTYPE_MINIGUN//wtSniperRifle
ASSET_REQUESTER assetRequester

//----------------------
//	ANIM / CS VARIABLES
//----------------------
STRING sSceneHandleBarry = "Barry"
STRING sSceneHandleChair = "Barrys_chair"
STRING sSceneHandleTable = "Barrys_protest_table"
STRING sSceneHandleWeapon = "Michaels_Weapon"
STRING sSceneHandlePapers = "Barry_Pamphlet_1"
STRING sSceneHandleLighter = "Lighter_Michael"
STRING sSceneHandleJoint = "Cig_Michael"

STRING FX_DrugsIn = "DrugsMichaelAliensFightIn" 
STRING FX_DrugsSkip = "DrugsMichaelAliensFight" 
STRING FX_DrugsOut = "DrugsMichaelAliensFightOut" 
STRING sMissionAnimDictionary = "rcmbarry"
STRING sLeadInAnimDictionary = "rcmbarryleadinout"
STRING sndAlienAmbientScene = "BARRY_01_DRUGS"

// mini game anims
STRING sAlienGrabIntroAnim = "bar_1_attack_intro_aln"
STRING sAlienGrabbedIntroAnim = "bar_1_attack_intro_mic"
STRING sAlienGrabAnim = "bar_1_attack_idle_aln"
STRING sAlienGrabbedAnim = "bar_1_attack_idle_mic"
STRING sAlienGrabFailAnim = "bar_1_attack_michael_wins_aln"
STRING sAlienGrabbedFailAnim = "bar_1_attack_michael_wins_mic"
STRING sAlienGrabWinAnim = "bar_1_attack_alien_wins_aln"
STRING sAlienGrabbedWinAnim = "bar_1_attack_alien_wins_mic"

STRING sAlienWatchAnim = "m_cower_01"
STRING sAlienTeleportAnim = "bar_1_teleport_aln"
STRING sMichaelTeleportAnim = "bar_1_teleport_mic"

STRING sExhaustedAnimEnter = "enter"
STRING sExhaustedAnim = "idle_d"

BOOL bHasMusicStarted = FALSE
BOOL bHasMusicIncreased = FALSE

BOOL bHasPlayerCameraExitState = FALSE

//----------------------
//	MODEL VARIABLES
//----------------------
MODEL_NAMES alienModel = S_M_M_MOVALIEN_01
MODEL_NAMES beamDummyModel = PROP_LD_TEST_01
MODEL_NAMES mnProtestSign = Prop_Protest_Sign_01

//----------------------
//	DIALOG VARIABLES
//----------------------
BOOL bHasInitialOutburst
INT iFirstDialogTime
INT iNextFightDialogTime
INT iNextFreakDialogTime
INT iNextDamageDialogTime
LOCATES_HEADER_DATA sLocateData
structPedsForConversation convoMichael
STRING sAmbientZone = "AZ_BARRY_01_AMBIENCE"

//----------------------
//	ALIEN VARIABLES
//----------------------
BOOL bAutoCreateAliens = FALSE
BOOL bKillAliensOnTeleportOut = TRUE
BOOL bSummonRushFlagActive = FALSE
BOOL bSummonFlagActive = FALSE
BOOL bAllowSummoning = FALSE

INT iAlienSummonerIndex = -1
INT iAlienWatchSummonTimeOut = 3500
INT iAlienNextSummonTime
INT iAlienNextSummonTimeOut = 15000
INT iAlienButtonMashLimit = 1
INT iAlienSummonTimeLength = 6000
INT iAlienMaxHP = 500

INT iAlienMinClickTime = 1000
INT iAlienMaxClickTime = 2000

FLOAT fAlienSummonRange = 5.0

FLOAT fAlienImpactEffectSize = 0.375
FLOAT fAlienChargeEffectSize = 0.8
FLOAT fAlienTeleportEffectSize = 2.0
FLOAT fAlienDeathEffectSize = 1.0

BOOL bUseAlienSpawnPoint = TRUE
VECTOR vAlienSpawnPoint = <<183.58, -967.10, 29.09>>
FLOAT fAlienSpawnPointRadius = 38.250

FLOAT fAlienMinSpawnRadius = 30.0
FLOAT fAlienMaxSpawnRadius = 60.0

// auto alien gen
CONST_INT kiAlienAutoGenDeltaTime		200
CONST_INT kiAlienMinAutoGenBaseTime		600
CONST_INT kiAlienMaxAutoGenBaseTime		1000

INT iAlienAutoGenTime
INT iAlienMinAutoGenTime = kiAlienMinAutoGenBaseTime
INT iAlienMaxAutoGenTime = kiAlienMaxAutoGenBaseTime
INT iAlienAutoGenDeltaTime = kiAlienAutoGenDeltaTime

ALIEN_PED alienPed[MAX_ALIEN_PEDS]

//----------------------
//	ALIEN BEAM VARIABLES
//----------------------
BOOL bAlienBeamKnockDown = FALSE
BOOL bBeamArmorHelpDisplayed = FALSE

INT iAlienBeamDamage = 1
INT iAlienBeamDamageDelay = 100
INT iAlienBeamChargeTime = 2000
INT iAlienBeamShootTime = 6000

FLOAT fAlienBeamStartRange = 15.0
FLOAT fAlienBeamEffectSize = 0.75
FLOAT fAlienBeamMaximumLength = 100.0
FLOAT fAlienBeamLengthInc = 0.125 // 2.5

FLOAT fAlienMinBeamTriggerVariance = -0.1
FLOAT fAlienMaxBeamTriggerVariance = 0.3
FLOAT fAlienBeamKnockDownForce = 1.75

VECTOR vAlienBeamEmitterOffset = <<0.3, 0.3, 0>>
VECTOR vAlienBeamRotationVector = <<270.0, 0.0, 0>>

FLOAT fAlienScreenImpactShift = 0.10
//FLOAT fAlienBeamEvoIncrease = 0.0125
//VECTOR vCameraForward
//VECTOR vCameraCoords
//FLOAT fCameraNearClip

//----------------------
//	STATS VARIABLES
//----------------------
INT iMaxActiveAliens = kiMaxStartingAliens
INT iMaxKillChain = 0
INT iKillChain = 0
INT iKillChainEndTime = 0

INT iTotalAlienSummonCount = 0
INT iTotalAlienSpawnCount = 0
INT iTotalAlienDeadCount = 0
INT iTotalAlienDeadRequired = 31
INT iAlienAliveCount = 0 

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID m_WidgetGroup
	BOOL bUpdateWidgets = TRUE
	BOOL bShowAlienPos = FALSE
#ENDIF

//----------------------
//	@SBA - ADDITIONAL CONSTANTS/VARIABLES
//----------------------

CONST_INT			kiSniperAmmo			10
CONST_INT			kiPistolAmmo			30
CONST_INT			kiGrenLaunchAmmo		10
CONST_INT			kiMinigunAmmo			INFINITE_AMMO

CONST_FLOAT			kfMinClickSoundDist		20.0

// Min gap between fight/freak lines
CONST_INT		kiDialogTimeCheck			2500

// @SBA - new pos to point at the advancing aliens
VECTOR				vPlayerStartPos			= << 188.86, -955.83, 29.09 >>
CONST_FLOAT			kfPlayerStartHead		327.2303

BOOL				bSkipAllowed = FALSE
OBJECT_INDEX		oiMichaelsWeapon

// Aliens left to kill to end 1st wave
CONST_INT			kiAliensLeftToKill		5
// aliens to add when player gerts minigun
INT					iAliensToAdd			= kiAliensLeftToKill


// Position and radius for first contact random starts
VECTOR				v1stContactStartCtr 	= <<216.765884,-908.962097,29.692392>> 
FLOAT				f1stContactStartRadius	= 7.00

structTIMER 		tmr1stContactSpawn
FLOAT				f1stContactSpawnTime

// To put some time between the end of one stage and the start of the next
structTIMER 		tmrStageTimer

CONST_INT			kiAlienWallSpawnArraySize	9

STRUCT ALIEN_WALL_SPAWN_DATA
	VECTOR	vAlienWallSpawnLoc
ENDSTRUCT
ALIEN_WALL_SPAWN_DATA AlienWallSpawnData[kiAlienWallSpawnArraySize]

// Top of structure for possible abduction use later
//VECTOR				vTopStructure			= <<214.2620, -921.4756, 59.6946>>
//VECTOR				vTopStructureCamPos		= <<214.4669, -921.1708, 64.5663>>
//VECTOR				vTopStructureCamRot		= <<-89.4999, 0.0000, 54.8117>>

// car stuff
VECTOR				vPlayerVehicleLoc
FLOAT				fPlayerVehicleHead
BOOL				bPlayerVehicleMoved


//----------------------
//	MISC FUNCTIONS
//----------------------

// Populate a single indes in the array
PROC POPULATE_ALIEN_WALL_SPAWN_DATUM(ALIEN_WALL_SPAWN_DATA &DataStruct[], INT iArrayIndex, VECTOR vSpawnLoc)
	DataStruct[iArrayIndex].vAlienWallSpawnLoc = vSpawnLoc
	//CPRINTLN(DEBUG_MISSION, "POPULATE_ALIEN_WALL_SPAWN_DATUM: Populating specific array datum.  Spawn location = ", DataStruct[iArrayIndex].vAlienWallSpawnLoc)
ENDPROC

// populate the wall spawn data array
PROC POPULATE_ALIEN_WALL_SPAWN_DATA()
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 0, (<<175.5842, -964.2895, 35.0957>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 1, (<<183.1626, -970.6071, 35.0957>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 2, (<<179.9724, -943.0597, 34.0921>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 3, (<<185.2821, -935.3659, 34.0922>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 4, (<<199.9392, -951.7982, 35.0511>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 5, (<<190.3101, -976.7255, 35.0916>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 6, (<<205.6451, -974.7205, 35.0476>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 7, (<<212.8866, -936.2545, 33.7047>>))
	POPULATE_ALIEN_WALL_SPAWN_DATUM(AlienWallSpawnData, 8, (<<194.1555, -957.8777, 33.3463>>))  // cube
ENDPROC

/// PURPOSE:
///    @SBA - Resets the array to default values
PROC RESET_LOCAL_ALIEN_WALL_SPAWN_DATA(ALIEN_WALL_SPAWN_DATA &DataStruct[])
	INT idx
	REPEAT COUNT_OF(DataStruct) idx
		POPULATE_ALIEN_WALL_SPAWN_DATUM(DataStruct, idx, (<<0,0,0>>))
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Get a vector from the data for a wall spawn
/// PARAMS:
///    vOut - returns the vector
/// RETURNS:
///    Returns TRUE if there's a valid vector to use
FUNC BOOL GET_SPECIFIC_WALL_SPAWN_DATA(VECTOR &vOut)

	INT idx
	INT iIndex = 0 
	
	ALIEN_WALL_SPAWN_DATA localAlienWallSpawnData[kiAlienWallSpawnArraySize]
	
	RESET_LOCAL_ALIEN_WALL_SPAWN_DATA(localAlienWallSpawnData)

	// comb the permanent data 
	REPEAT kiAlienWallSpawnArraySize idx
		// Make sure there's room in the array
		IF iIndex < COUNT_OF(localAlienWallSpawnData)
			// Make sure peds aren't too close to the prospective spawn location
			// Any more condtions?  Do we want a max distance too?
			IF NOT IS_ANY_PED_NEAR_POINT(AlienWallSpawnData[idx].vAlienWallSpawnLoc, 1.5)
				POPULATE_ALIEN_WALL_SPAWN_DATUM(localAlienWallSpawnData, iIndex, AlienWallSpawnData[idx].vAlienWallSpawnLoc)
				iIndex ++
			ELSE
				//CPRINTLN(DEBUG_MISSION, "GET_SPECIFIC_WALL_SPAWN_DATA: *** Spawn point ", AlienWallSpawnData[idx].vAlienWallSpawnLoc, " is too close to an existing ped ***")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "GET_SPECIFIC_WALL_SPAWN_DATA: *** Wha' hoppend?  Array Is Full! ***")
			idx = kiAlienWallSpawnArraySize
		ENDIF
	ENDREPEAT
	
	IF iIndex = 0
		CPRINTLN(DEBUG_MISSION, "GET_SPECIFIC_WALL_SPAWN_DATA: *** No good spawn locations for wall spawn! ***")	
	ELSE	
		// Pick a random index for the specific data array
		INT iReturnSpecificIndex = GET_RANDOM_INT_IN_RANGE(0, iIndex)
		vOut = localAlienWallSpawnData[iReturnSpecificIndex].vAlienWallSpawnLoc
		IF ARE_VECTORS_EQUAL(localAlienWallSpawnData[iReturnSpecificIndex].vAlienWallSpawnLoc, (<<0,0,0>>))
			CPRINTLN(DEBUG_MISSION, "GET_SPECIFIC_WALL_SPAWN_DATA: WHOOPS! Random index = ", iReturnSpecificIndex,".  Spawn location = ", vOut, ".")
		ELSE
			//CPRINTLN(DEBUG_MISSION, "GET_SPECIFIC_WALL_SPAWN_DATA: Returning TRUE. Random index = ", iReturnSpecificIndex,".  Spawn location = ", vOut, ".")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Obtain a valid wall spawn location and heading, if we want one
/// PARAMS:
///    vOut - returns a wall spawn location
///    fSpawnChance - chance of getting a wall spawn (or try to at least)
/// RETURNS:
///    TRUE if we've chosen a valid location to spawn; FALSE otherwise
FUNC BOOL GENERATE_ALIEN_WALL_SPAWN(VECTOR &vOut, FLOAT fSpawnChance = 0.10)

	IF NOT bIsPlayerInBattleArea
		RETURN FALSE
	ENDIF
	
	// Do we want a wall spawn this time?
	FLOAT rnd = GET_RANDOM_FLOAT_IN_RANGE() 
	IF rnd > fSpawnChance
		RETURN FALSE
	ENDIF
	
	VECTOR vWallLoc
	
	IF GET_SPECIFIC_WALL_SPAWN_DATA(vWallLoc)
		vOut = vWallLoc
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///  	Resolve cars for mission
PROC RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()				
	CPRINTLN(DEBUG_MISSION, "Vehicles Resolved For Intro")

	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
		CPRINTLN(DEBUG_MISSION, "Setting post-mission respawn location to where vehicle was")
		IF IS_ENTITY_ALIVE(GET_MISSION_START_VEHICLE_INDEX())
			vPlayerVehicleLoc = GET_ENTITY_COORDS(GET_MISSION_START_VEHICLE_INDEX())
			fPlayerVehicleHead = GET_ENTITY_HEADING(GET_MISSION_START_VEHICLE_INDEX())
		ENDIF
	ENDIF
	
	IF NOT IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
	OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_MISSION_START_VEHICLE_INDEX(), vMissionStart) < 10.0
		vPlayerVehicleLoc = vPlayerCarRespot
		fPlayerVehicleHead = fPlayerCarRespot
	ENDIF
	CPRINTLN(DEBUG_MISSION, "Vehicles Loc = ", vPlayerVehicleLoc)
	CPRINTLN(DEBUG_MISSION, "Vehicles Head = ", fPlayerVehicleHead)

	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(respotVehicleArea.vPosition[0], respotVehicleArea.vPosition[1], respotVehicleArea.fWidth,
		vPlayerCarRespot, fPlayerCarRespot, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
	
	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
		SET_MISSION_VEHICLE_GEN_VEHICLE(GET_MISSION_START_VEHICLE_INDEX(), vPlayerVehicleLoc, fPlayerVehicleHead)
	ENDIF
		
	CPRINTLN(DEBUG_MISSION, "Vehicles Loc (after gen) = ", vPlayerVehicleLoc)
	CPRINTLN(DEBUG_MISSION, "Vehicles Head (after gen) = ", fPlayerVehicleHead)
ENDPROC	

PROC MOVE_PLAYERS_VEHICLE()
	CPRINTLN(DEBUG_MISSION, "MOVE_PLAYERS_VEHICLE")
	
	IF bPlayerVehicleMoved
		CPRINTLN(DEBUG_MISSION, "Vehicle already moved")
		EXIT
	ENDIF

	IF DOES_ENTITY_EXIST(GET_MISSION_START_VEHICLE_INDEX())
	AND NOT ARE_VECTORS_EQUAL(vPlayerVehicleLoc, vPlayerCarRespot)
		CLEAR_AREA(vPlayerVehicleLoc, 3.0, TRUE)
		
		IF IS_ENTITY_ALIVE(GET_MISSION_START_VEHICLE_INDEX())
			SET_ENTITY_COORDS(GET_MISSION_START_VEHICLE_INDEX(), vPlayerVehicleLoc)
			SET_ENTITY_HEADING(GET_MISSION_START_VEHICLE_INDEX(), fPlayerVehicleHead)
			SET_VEHICLE_ON_GROUND_PROPERLY(GET_MISSION_START_VEHICLE_INDEX())
		ENDIF

		IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
			SET_MISSION_VEHICLE_GEN_VEHICLE(GET_MISSION_START_VEHICLE_INDEX(), vPlayerVehicleLoc, fPlayerVehicleHead)
		ENDIF
		
		CPRINTLN(DEBUG_MISSION, "Vehicles Loc = ", vPlayerVehicleLoc)
		CPRINTLN(DEBUG_MISSION, "Vehicles Head = ", fPlayerVehicleHead)
	ENDIF
	
	bPlayerVehicleMoved = TRUE
ENDPROC

/// PURPOSE:
///    Tells us if the mission is in combat state
FUNC BOOL IS_MISSION_IN_COMBAT_STAGE()
	INT i1 = ENUM_TO_INT(SE_INTRO)
	INT i2 = ENUM_TO_INT(SE_PASSOUT)
	INT i = ENUM_TO_INT(stageEnum)
	
	RETURN (i > i1) AND (i < i2)
ENDFUNC

/// PURPOSE:
///  	Resets all the values that we set up - useful for z menu
PROC RESET_MISSION_COUNTERS()
	iAlienNextSummonTime = GET_GAME_TIMER()
	bSummonRushFlagActive = FALSE
	bSummonFlagActive = FALSE
	bAllowSummoning = FALSE
	iAlienSummonerIndex = -1
	
	iTotalAlienSummonCount = 0
	iTotalAlienSpawnCount = 0
	iTotalAlienDeadCount = 0

	iKillChain = 0
	iKillChainEndTime = 0
	
	iMaxActiveAliens = kiMaxStartingAliens	
ENDPROC

/// PURPOSE:
///  	Resets all the values that we set up - useful for z menu
PROC RESET_MISSION_PARAMETERS()

	// @SBA - ADDED BY REQUEST (just minigun)
	playerWeapon = WEAPONTYPE_MINIGUN

	fAlienSummonRange = 7.5
	iAlienButtonMashLimit = 10
	iAlienSummonTimeLength = 6000

	fAlienBeamStartRange = 15.0
	iAlienBeamChargeTime = 1500
	iAlienBeamShootTime = 4500

	fAlienImpactEffectSize = 0.375
	fAlienChargeEffectSize = 0.8
	fAlienTeleportEffectSize = 2.0
	fAlienBeamEffectSize = 0.75
	fAlienBeamMaximumLength = 100.0
	fAlienBeamLengthInc = 2.0
	fAlienMinBeamTriggerVariance = -0.1
	fAlienMaxBeamTriggerVariance = 0.3
	fAlienMinSpawnRadius = 30.0
	fAlienMaxSpawnRadius = 40.0

	fAlienBeamKnockDownForce = 1.75
	bAlienBeamKnockDown = FALSE
	iAlienBeamDamage = 1
	iAlienBeamDamageDelay = 75

	iAlienWatchSummonTimeOut = 3500

	iAlienMinAutoGenTime = kiAlienMinAutoGenBaseTime
	iAlienMaxAutoGenTime = kiAlienMaxAutoGenBaseTime
	iAlienAutoGenDeltaTime = kiAlienAutoGenDeltaTime

	fTimeScale = 0.8
	
	bPlayerVehicleMoved = FALSE
ENDPROC

PROC CLEANUP_SCREEN_FX()
	IF ANIMPOSTFX_IS_RUNNING(FX_DrugsIn)
		ANIMPOSTFX_STOP(FX_DrugsIn)
	ENDIF
	IF ANIMPOSTFX_IS_RUNNING(FX_DrugsSkip)
		ANIMPOSTFX_STOP(FX_DrugsSkip)
	ENDIF
	IF ANIMPOSTFX_IS_RUNNING(FX_DrugsOut)
		ANIMPOSTFX_STOP(FX_DrugsOut)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the mission fail reason and goes into the fail state
PROC SET_MISSION_FAILED_ABDUCTED()
	IF (stageEnum <> SE_ABDUCT) AND (stageEnum <> SE_FAILED)
		stageProgress = SP_LOADING
		stageEnum = SE_ABDUCT
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the mission fail reason and goes into the fail state
/// PARAMS:
///    sFailReason - text label of fail reason
PROC SET_MISSION_FAILED(STRING sFailReason)
	IF stageEnum <> SE_FAILED
		CPRINTLN(DEBUG_MISSION, "Setting mission as failed. ", sFailReason)
		sFailString = sFailReason
		stageProgress = SP_LOADING
		stageEnum = SE_FAILED
	ENDIF
ENDPROC

/// PURPOSE: 
///  	Handles the death sequence
/// PARAMS:
///    	beamDirection - direction of beam that kills player
PROC ABDUCT_SEQUENCE_END()
	
	IF GET_GAME_TIMER() > iFailTimer
	AND iFailTimer <> -1
		IF IS_ENTITY_OK(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		ENDIF
		RC_END_CUTSCENE_MODE(TRUE, FALSE)
		iFailTimer = -1
	ENDIF
ENDPROC

/// PURPOSE: 
///  	Handles the death sequence
/// PARAMS:
///    	beamDirection - direction of beam that kills player
PROC DEATH_SEQUENCE(VECTOR beamOrigin, VECTOR beamImpact, BOOL bKillPlayer = TRUE)
	//VECTOR v
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Player got lasered to death")
		ENDIF
	#ENDIF
	
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 0)
	IF NOT IS_VECTOR_ZERO(beamOrigin) AND NOT IS_VECTOR_ZERO(beamImpact) 
		// SHOOT_SINGLE_BULLET_BETWEEN_COORDS(beamOrigin, beamImpact, 100000, TRUE, WEAPONTYPE_PISTOL, NULL, FALSE) - IS THIS CRASHING IT?
	ENDIF
	
	gunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(playerWeapon)
	SAFE_RELEASE_OBJECT(gunObject)
	
	IF (bKillPlayer = TRUE)
		APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), 100000, TRUE)
		EXPLODE_PED_HEAD(PLAYER_PED_ID())
	ENDIF
ENDPROC

PROC HANDLE_LOCK_PLAYER_IN_FRONT_OF_TABLE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), (<<189.13988, -955.80487, 29.09192>>), (<<187.52740, -955.01056,31.091919>>), 2.5000000)
				CPRINTLN(DEBUG_MISSION, "HANDLE_LOCK_PLAYER_IN_FRONT_OF_TABLE: Locking player")
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Given a point does a ground z check, checks it not in a no spawn zone and there are no peds too close
/// PARAMS:
///    pos - position to check
///    bUseSmallerPlayerRange - decrease player range so aliens can spawn closer
/// RETURNS:
///    True / False
FUNC BOOL CHECK_SPAWN_POINT_IS_OK(VECTOR &pos, BOOL bUseSmallerPlayerRange = FALSE)
	VECTOR spawnPos
	FLOAT fPlayerRange = 15.0
	
	// do a safety check here
	IF NOT GET_SAFE_COORD_FOR_PED(pos, FALSE, spawnPos, GSC_FLAG_NOT_ISOLATED | GSC_FLAG_NOT_INTERIOR | GSC_FLAG_NOT_WATER)
		CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - CAN'T GET A SAFE COORD FOR POS:[", pos.x, ",", pos.y, ",", pos.z, "]")
		RETURN FALSE
	ENDIF
	
	IF bUseSmallerPlayerRange
		fPlayerRange = 2.5
	ELIF iTotalAlienDeadCount > 27
		fPlayerRange = 10.0
	ENDIF
	
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), spawnPos, fPlayerRange)
		CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - PLAYER WITHIN RANGE OF SPAWN POS:[", spawnPos.x, ",", spawnPos.y, ",", spawnPos.z, "] - RANGE:", fPlayerRange)
		RETURN FALSE
	ENDIF
	
	// check there isn't a ped in the same area
	//IF IS_ANY_PED_IN_SPHERE(spawnPos, 2.0, PLAYER_PED_ID())  /// don't need to exclude player as the check above will do that
	
	IF (GET_DISTANCE_BETWEEN_COORDS(spawnPos, vBattleCenter) > fBattleRadius)
		CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - SPAWN POS:[", spawnPos.x, ",", spawnPos.y, ",", spawnPos.z, "] IS TOO FAR AWAY FROM BATTLE CENTER")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_POINT_IN_ANGLED_AREA(spawnPos, missionBoundary.vPosition[0], missionBoundary.vPosition[1], missionBoundary.fWidth)
		CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - SPAWN POS:[", spawnPos.x, ",", spawnPos.y, ",", spawnPos.z, "] IS NOT WITHIN MISSION BOUNDARY")
		RETURN FALSE
	ENDIF
	
	IF (bUseIsAreaOccupied)
		IF IS_ANY_PED_IN_SPHERE(spawnPos, 2.0)
			CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - IS_ANY_PED_IN_SPHERE - SPAWN POS:[", spawnPos.x, ",", spawnPos.y, ",", spawnPos.z, "] THERE IS ANOTHER ALIEN WITHIN 2 METERS")
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_ANY_PED_NEAR_POINT(spawnPos, 2.0)
			CPRINTLN(DEBUG_MISSION, "CHECK SPAWN POINT IS OK - FAILED - IS_ANY_PED_NEAR_POINT - SPAWN POS:[", spawnPos.x, ",", spawnPos.y, ",", spawnPos.z, "] THERE IS ANOTHER ALIEN WITHIN 2 METERS")
			RETURN FALSE
		ENDIF
	ENDIF
	
	pos = spawnPos
	RETURN TRUE
ENDFUNC

//----------------------
//	@SBA - MISC FUNCTIONS
//----------------------

/// PURPOSE:
///    Generates a ped spawn point in range of player - also uses CHECK_SPAWN_POINT_IS_OK()
/// PARAMS:
///    out - new position
///    minrange - desired minimum range
///    maxrange - desired maximum range
/// RETURNS:
///    True on success
FUNC BOOL GENERATE_ALIEN_SPAWN_POINT(VECTOR &out)
	IF (bIsPlayerInBattleArea = FALSE)
		RETURN FALSE
	ENDIF
	
	IF (bUseAlienSpawnPoint)
		out = GET_RANDOM_POINT_IN_DISC(vAlienSpawnPoint, fAlienSpawnPointRadius, 6.0)
	ELSE 
		out = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), 
			GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0), <<0, GET_RANDOM_FLOAT_IN_RANGE(fAlienMinSpawnRadius, fAlienMaxSpawnRadius), 6.0>>)
	ENDIF	
	RETURN CHECK_SPAWN_POINT_IS_OK(out)
ENDFUNC

/// PURPOSE:
///    @SBA - a verion of GENERATE_ALIEN_SPAWN_POINT that takes a vector and radius
///    (Added this to keep the functionality of the two distinct)
/// PARAMS:
///    vIn - Centerpoint
///    fRadius - radius of disc
///    out - new position
/// RETURNS:
///    TRUE if a useable spawn vector is found
FUNC BOOL GENERATE_ALIEN_SPAWN_POINT_NEAR_LOCATION(VECTOR vIn, FLOAT fRadius, VECTOR &out)
	IF (bIsPlayerInBattleArea = FALSE)
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "GENERATE_ALIEN_SPAWN_POINT_NEAR_LOCATION - TRYING GENERATE POINT WITHIN ", fRadius, " METERS OF [", vIn.x, ",", vIn.y, "," ,vIn.z, "]")
	out = GET_RANDOM_POINT_IN_DISC(vIn, fRadius, 6.0)
	RETURN CHECK_SPAWN_POINT_IS_OK(out, TRUE)
ENDFUNC


/// PURPOSE:
///    Gets which weapon the player ought to have next
/// PARAMS:
///    wtPlayerWeapon - the player's current weapon
/// RETURNS:
///    The next weapon in the list
FUNC WEAPON_TYPE GET_PLAYERS_NEXT_WEAPON(WEAPON_TYPE wtPlayerWeapon)
	WEAPON_TYPE wtNextWeapon
	IF wtPlayerWeapon = wtSniperRifle
		wtNextWeapon = wtPistol
	ELIF wtPlayerWeapon = wtPistol
		wtNextWeapon = wtGrenLauncher
	ELIF wtPlayerWeapon = wtGrenLauncher
		wtNextWeapon = wtMiniGun  //@TODO - change this to wtStunGun when I get it working
//	ELIF wtPlayerWeapon = wtStunGun
//		wtNextWeapon = wtMiniGun
	ELSE
		CPRINTLN(DEBUG_MISSION, "*** GET_PLAYERS_NEXT_WEAPON: How'd we get to DEFAULT case?! ***")
		wtNextWeapon = wtMiniGun
	ENDIF
	CPRINTLN(DEBUG_MISSION, "GET_PLAYERS_NEXT_WEAPON: Next weapon = ", GET_WEAPON_NAME(wtNextWeapon))
	RETURN wtNextWeapon
ENDFUNC

/// PURPOSE:
///    Gets the appropriate ammount of ammo depending on the weapon type
/// PARAMS:
///    wtPlayerWeapon - The weapon type to check
/// RETURNS:
///    The ammount of ammo that weapon should have
FUNC INT GET_ALLOWED_AMMO_FOR_WEAPON(WEAPON_TYPE wtPlayerWeapon)
	INT iAmmoAllowed
	IF wtPlayerWeapon = wtSniperRifle
		iAmmoAllowed = kiSniperAmmo
	ELIF wtPlayerWeapon = wtPistol
		iAmmoAllowed = kiPistolAmmo
	ELIF wtPlayerWeapon = wtGrenLauncher
		iAmmoAllowed = kiGrenLaunchAmmo
//	ELIF wtPlayerWeapon = wtStunGun
//		iAmmoAllowed = INFINITE_AMMO
	ELIF wtPlayerWeapon = wtMiniGun
		iAmmoAllowed = kiMinigunAmmo
	ELSE
		CPRINTLN(DEBUG_MISSION, "*** GET_ALLOWED_AMMO_FOR_WEAPON: How'd we get to DEFAULT case?! ***")
		iAmmoAllowed = INFINITE_AMMO
	ENDIF
	CPRINTLN(DEBUG_MISSION, "GET_ALLOWED_AMMO_FOR_WEAPON: Returning ammo ammount = ", iAmmoAllowed)
	RETURN iAmmoAllowed
ENDFUNC

/// PURPOSE:
///    Checks if the player has the wepaon passed in and if it has no ammo in it
/// PARAMS:
///    wtPlayerWeapon - the weapon to check
/// RETURNS:
///    TRUE if out of ammo for this weapon; FALSE otherwise
FUNC BOOL IS_PLAYER_OUT_OF_AMMO(WEAPON_TYPE wtPlayerWeapon)

	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon)
		CPRINTLN(DEBUG_MISSION, "IS_PLAYER_OUT_OF_AMMO: *** Player does not have weapon type: ", GET_WEAPON_NAME(wtPlayerWeapon))
		RETURN FALSE
	ENDIF
	
	IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon) = 0
		CPRINTLN(DEBUG_MISSION, "IS_PLAYER_OUT_OF_AMMO: Yes! Empty!")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Give the player the next weapon he should have
/// PARAMS:
///    wtPlayerWeapon - Pass by ref the player's weapon type
PROC SWAP_THE_PLAYERS_WEAPON(WEAPON_TYPE &wtPlayerWeapon)
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	// Remove existing weapon
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), wtPlayerWeapon)
		CPRINTLN(DEBUG_MISSION, "SWAP_THE_PLAYERS_WEAPON: Removing player's weapon: ", GET_WEAPON_NAME(wtPlayerWeapon))	
	ENDIF
	
	// init the next weapon and get its ammo
	wtPlayerWeapon = GET_PLAYERS_NEXT_WEAPON(wtPlayerWeapon)
	INT iAmmo = GET_ALLOWED_AMMO_FOR_WEAPON(wtPlayerWeapon)
	
	// give it to the player
	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtPlayerWeapon, iAmmo, TRUE)
	IF wtPlayerWeapon = wtMiniGun
		SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), TRUE, wtPlayerWeapon)
	ELSE
		// @SBA - make sure he has the right ammount of ammo (this seems really convoluted)
		SET_AMMO_IN_CLIP(PLAYER_PED_ID(), wtPlayerWeapon, 0)
		SET_PED_AMMO(PLAYER_PED_ID(), wtPlayerWeapon, 0)
		ADD_AMMO_TO_PED(PLAYER_PED_ID(), wtPlayerWeapon, iAmmo)
	ENDIF
	CPRINTLN(DEBUG_MISSION, "SWAP_THE_PLAYERS_WEAPON: Player given new weapon: ", GET_WEAPON_NAME(wtPlayerWeapon))	
	CPRINTLN(DEBUG_MISSION, "SWAP_THE_PLAYERS_WEAPON: Ammo in new weapon = ", GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon))	
ENDPROC


/// PURPOSE: 
///  	Forces the players weapon
PROC FORCE_PLAYER_WEAPON(WEAPON_TYPE wtWeaponToGive)
	WEAPON_TYPE wpn
	
	//playerWeapon = wtWeaponToGive
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtWeaponToGive)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, 10, TRUE, TRUE)
			CPRINTLN(DEBUG_MISSION, "Player doesn't have a minigun lets give him one.")
		ELSE
			IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wpn) OR (wpn <> wtWeaponToGive)
				CPRINTLN(DEBUG_MISSION, "Player has a minigun but doesn't want to equip it for some reason.")
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToGive, TRUE)
			ELSE
				CPRINTLN(DEBUG_MISSION, "Player - Should have the minigun and it should be in his hand")
			ENDIF
		ENDIF
		
	
		// @SBA - we don't always want the weapon to have inifinite ammo now
		// @SBA - COMMENTING OUT BY REQUEST
//		SET_AMMO_IN_CLIP(PLAYER_PED_ID(), wtWeaponToGive, 0)
//		SET_PED_AMMO(PLAYER_PED_ID(), wtWeaponToGive, 0)

		ADD_AMMO_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, 10000) // OLD = 10000  // 10
		SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), TRUE, wtWeaponToGive)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE) 	
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), FALSE)
		
		REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1)
		//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ACTIONMODE_IDLE, TRUE)
	ENDIF
ENDPROC

//----------------------
//	DIALOG FUNCTIONS
//----------------------

/// PURPOSE: 
///    Returns True if God Text is on
FUNC BOOL ARE_OBJECTIVES_BEING_DISPLAYED()
	IF IS_THIS_PRINT_BEING_DISPLAYED("PRIME2") // Kill the ~r~clowns.~s~
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PRINT_BEING_DISPLAYED("B1_WARN") // Return to the ~r~battle.~s~
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Returns True if God Text is on
FUNC BOOL ARE_OBJECTIVES_OR_TEXT_BEING_DISPLAYED()
	IF IS_THIS_PRINT_BEING_DISPLAYED("PRIME2") // Kill the ~r~clowns.~s~
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PRINT_BEING_DISPLAYED("B1_WARN") // Return to the ~r~battle.~s~
		RETURN TRUE
	ENDIF
	
	IF IS_ANY_TEXT_BEING_DISPLAYED(sLocateData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Initial sets up Michaels Ranting during the battle
PROC SETUP_MICHAEL_DIALOG()
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	
	iFirstDialogTime = GET_GAME_TIMER() + 1500
	iNextFreakDialogTime = iFirstDialogTime + GET_RANDOM_INT_IN_RANGE(20000, 40000)
	iNextFightDialogTime = iFirstDialogTime + GET_RANDOM_INT_IN_RANGE(12000, 18000)
	iNextDamageDialogTime = GET_GAME_TIMER()
	bHasInitialOutburst = FALSE
ENDPROC

/// PURPOSE: 
///    Runs the fight rants Michael can shout out during the battle
PROC PLAY_MICHAEL_FREAK_DIALOG_LINES()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < iNextFreakDialogTime)
		EXIT
	ENDIF
	
	IF (NOT bHasInitialOutburst)
		EXIT 
	ENDIF
	
	IF ARE_OBJECTIVES_OR_TEXT_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Trying to play Michael Freak Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
		ENDIF
	#ENDIF
	
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	IF CREATE_CONVERSATION(convoMichael, "BARY1AU", "BARY1_FREAK", CONV_PRIORITY_LOW)
		#IF IS_DEBUG_BUILD
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Michael Freak Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
			ENDIF
		#ENDIF
		iNextFreakDialogTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(20000, 40000)
		// don't let fight line play too quickly after
		IF iNextFightDialogTime - GET_GAME_TIMER() < kiDialogTimeCheck
			CPRINTLN(DEBUG_MISSION, "Fight Rant Time too soon: resetting.")
			iNextFightDialogTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Runs the fight rants Michael can shout out during the battle
PROC PLAY_MICHAEL_FIGHT_DIALOG_LINES()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() < iNextFightDialogTime)
		EXIT
	ENDIF

	IF (NOT bHasInitialOutburst)
		EXIT 
	ENDIF
	
	IF ARE_OBJECTIVES_OR_TEXT_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Trying to play Michael Fight Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
		ENDIF
	#ENDIF
	
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	IF CREATE_CONVERSATION(convoMichael, "BARY1AU", "BARY1_FIGHT", CONV_PRIORITY_LOW)
		#IF IS_DEBUG_BUILD
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Michael Fight Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
			ENDIF
		#ENDIF
		iNextFightDialogTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(10000, 20000)
		// don't let freak line play too quickly after
		IF iNextFreakDialogTime - GET_GAME_TIMER() < kiDialogTimeCheck
			CPRINTLN(DEBUG_MISSION, "Freak Rant Time too soon: resetting.")
			iNextFreakDialogTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, 5000)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Runs the one shot rants Michael can shout at mission start
PROC PLAY_MICHAEL_DIALOG_LINES()
	IF (GET_GAME_TIMER() < iNextDamageDialogTime)
		EXIT
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Trying to play Michael Damage Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
		ENDIF
	#ENDIF
	
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	IF CREATE_CONVERSATION(convoMichael, "BARY1AU", "BARY1_DAMAGE", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
		#IF IS_DEBUG_BUILD
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Michael Damage Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
			ENDIF
		#ENDIF
		iNextDamageDialogTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 7000)
	ENDIF
ENDPROC

/// PURPOSE: 
///    Runs the one shot rants Michael can shout at mission start
PROC PLAY_MICHAEL_INITIAL_LINE()
	IF (GET_GAME_TIMER() < iFirstDialogTime)
		EXIT
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	IF (bHasInitialOutburst)
		EXIT 
	ENDIF
	
	TEXT_LABEL linename = "BARY1_1SHOT_"
	linename += (GET_RANDOM_INT_IN_RANGE(0, 3) + 1)
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Michael First Line Rant:", linename)
		ENDIF
	#ENDIF
	
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(convoMichael, "BARY1AU", "BARY1_1SHOT", linename, CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
		CPRINTLN(DEBUG_MISSION, "Initial Rant Played Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
		bHasInitialOutburst = TRUE 
	ENDIF
ENDPROC

/// PURPOSE: 
///    Runs the fight rants Michael can say during abduction
PROC PLAY_MICHAEL_ABDUCT_DIALOG_LINE()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		EXIT
	ENDIF
	
	IF (NOT bHasInitialOutburst)
		EXIT 
	ENDIF
	
	IF ARE_OBJECTIVES_OR_TEXT_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Trying to play Michael Freak Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
		ENDIF
	#ENDIF
	
	ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
	IF CREATE_CONVERSATION(convoMichael, "BARY1AU", "BARY1_ABDUCT", CONV_PRIORITY_LOW)
		#IF IS_DEBUG_BUILD
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Michael Abduct Rant - Time:", (GET_GAME_TIMER() - iMissionDebugTime) / 1000)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

//----------------------
//	ALIEN FUNCTIONS
//----------------------

/// PURPOSE: 
///    Fills the index - this wouldn't be necessary if i could store references to objects...
PROC POPULATE_ALIEN_INDEX()
	INT i = 0
	REPEAT COUNT_OF(alienPed) i
		alienPed[i].iAlienIndex = i
	ENDREPEAT
ENDPROC

/// PURPOSE: 
///    Returns True if this alien can do summon
/// PARAMS:
/// 	rp - reference to alien ped
FUNC BOOL CAN_ALIEN_SUMMON(ALIEN_PED &rp)
	IF rp.bWallSpawn
		RETURN FALSE
	ENDIF

	IF (iAlienSummonerIndex = -1) 
		RETURN FALSE
	ELSE
		RETURN (bAllowSummoning = TRUE) AND (GET_GAME_TIMER() > iAlienNextSummonTime) AND (rp.iAlienIndex = iAlienSummonerIndex)
	ENDIF
ENDFUNC

/// PURPOSE: 
///    Returns True if alien ped is living
/// PARAMS:
/// 	rp - reference to alien ped
FUNC BOOL IS_ALIEN_PED_ALIVE(ALIEN_PED &rp)
	IF NOT rp.bIsActive
		RETURN FALSE
	ENDIF
	
	RETURN (rp.aState <> AS_DYING) AND (rp.aState <> AS_DEAD) AND IS_ENTITY_ALIVE(rp.pedID)
ENDFUNC

/// PURPOSE: 
///    Returns True if alien ped is in dying state (or dead)
/// PARAMS:
/// 	rp - reference to alien ped
FUNC BOOL IS_ALIEN_DYING(ALIEN_PED &rp)
	RETURN (rp.aState = AS_DYING) OR (rp.aState = AS_DEAD)
ENDFUNC

/// PURPOSE: 
///    Returns True if alien ped is in dying state (or dead)
/// PARAMS:
/// 	rp - reference to alien ped
FUNC BOOL HAS_ALIEN_FINSHED_DYING(ALIEN_PED &rp)
	IF (rp.aState <> AS_DYING)
		RETURN FALSE 
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(rp.pedID)
		RETURN FALSE 
	ENDIF
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_VELOCITY(rp.pedID), <<0, 0, 0>>, 0.125)
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Alien ", rp.iAlienIndex, " is dying but still moving")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PED_RAGDOLL(rp.pedID) OR IS_PED_RUNNING_RAGDOLL_TASK(rp.pedID)		
		//CPRINTLN(DEBUG_MISSION, "Alien ", rp.iAlienIndex, " is dying but still doing ragdoll stuff")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    	Tells us if this is in summon state
/// PARAMS:
///    alien - alien reference
/// RETURNS:
///    True or False
FUNC BOOL IS_ALIEN_IN_BEAM_STATE(ALIEN_PED &alien)
	RETURN (alien.aState = AS_BEAM_KNOCKDOWN) OR (alien.aState = AS_BEAM) 
		OR (alien.aState = AS_BEAM_START) OR (alien.aState = AS_CHARGE) OR (alien.aState = AS_CHARGE_RUSH)
ENDFUNC

/// PURPOSE:
///    	Tells us if this is in summon state
/// PARAMS:
///    alien - alien reference
/// RETURNS:
///    True or False
FUNC BOOL IS_ALIEN_IN_SUMMON_STATE(ALIEN_PED &alien)
	RETURN (alien.aState = AS_SUMMON_RUSH) OR (alien.aState = AS_SUMMON_START)
ENDFUNC

/// PURPOSE:
///    	Tells us if this is in battle state
/// PARAMS:
///    alien - alien reference
/// RETURNS:
///    True or False
FUNC BOOL IS_ALIEN_IN_BATTLE_STATE(ALIEN_PED &alien)
	INT asnum = ENUM_TO_INT(alien.aState)
	RETURN (asnum >= ENUM_TO_INT(AS_ACTIVE))
ENDFUNC

/// PURPOSE: 
///    Returns True if alien ped is ready to be re-used
/// PARAMS:
/// 	rp - reference to alien ped
FUNC BOOL IS_ALIEN_PED_FREE(ALIEN_PED &rp)
	RETURN NOT rp.bIsActive
ENDFUNC

/// PURPOSE: 
///   Returns active number of alien peds
FUNC INT GET_ALIVE_ALIEN_PED_COUNT()
	INT i = 0
	INT cnt = 0
	
	REPEAT (COUNT_OF(alienPed)) i 
		IF IS_ALIEN_PED_ALIVE(alienPed[i])
			cnt ++
		ENDIF
	ENDREPEAT
	
	RETURN cnt
ENDFUNC

/// PURPOSE: 
///    Grabs an index to the first alive alien
/// PARAMS:
/// 	out - index in array to next free alien (this gets overwritten)
FUNC BOOL GET_FIRST_ALIVE_ALIEN_PED_INDEX(INT &out)
	INT i = 0
	
	out = -1
	REPEAT (COUNT_OF(alienPed)) i 
		IF IS_ALIEN_PED_ALIVE(alienPed[i])
			out = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Returns True if a alien ped is ready to be re-used
/// PARAMS:
/// 	out - index in array to next free alien (this gets overwritten)
FUNC BOOL GET_FREE_ALIEN_PED_INDEX(INT &out)
	INT i = 0
	
	out = -1
	REPEAT (COUNT_OF(alienPed)) i 
		IF (alienPed[i].bIsActive = FALSE)
			out = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Kills all alien peds
PROC KILL_ALL_ALIEN_PEDS()
	INT i = 0
	
	REPEAT (COUNT_OF(alienPed)) i 
		IF IS_ALIEN_PED_ALIVE(alienPed[i]) AND NOT IS_ENTITY_DEAD(alienPed[i].pedID)
			EXPLODE_PED_HEAD(alienPed[i].pedID)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Common things which are called in both delete and cleanup
/// PARAMS:
///    ent - 
PROC REMOVE_ALIEN_PARTICLE_FX(ALIEN_PED &ent)
	SAFE_REMOVE_PARTICLE_FX(ent.fxSummonID)
	SAFE_REMOVE_PARTICLE_FX(ent.fxChargeID)
	SAFE_REMOVE_PARTICLE_FX(ent.fxBeamID)
	SAFE_REMOVE_PARTICLE_FX(ent.fxImpactID)
	SAFE_REMOVE_PARTICLE_FX(ent.fxScreenImpactID)
ENDPROC

/// PURPOSE: 
///    Tells all alien peds to flee
PROC FLEE_ALL_ALIEN_PEDS()
	INT i = 0
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	ENDIF
	
	REPEAT COUNT_OF(alienPed) i
		IF IS_ALIEN_PED_ALIVE(alienPed[i])
			REMOVE_ALIEN_PARTICLE_FX(alienPed[i])
			alienPed[i].aState = AS_NULL
			TASK_SMART_FLEE_COORD(alienPed[i].pedID, GET_ENTITY_COORDS(PLAYER_PED_ID()), 400.0, -1)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Common things which are called in both delete and cleanup
/// PARAMS:
///    ent - 
PROC SHARED_DELETE_CLEANUP(ALIEN_PED &ent)
	IF IS_ALIEN_IN_SUMMON_STATE(ent)
		bSummonRushFlagActive = FALSE
		bSummonFlagActive = FALSE
	ENDIF
	
	SAFE_REMOVE_BLIP(ent.blipID)
	SAFE_DELETE_OBJECT(ent.screenImpactID)
	SAFE_DELETE_OBJECT(ent.projectorID)
	SAFE_DELETE_OBJECT(ent.impactID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(ent.mindCtrlSoundID)
	REMOVE_ALIEN_PARTICLE_FX(ent)
	
	ent.iButtonMashCount = 0
	ent.sBeamLOS = NULL
ENDPROC

/// PURPOSE:
///    Force everything off for hard reset
PROC STOP_ALL_ALIEN_EVERYTHING()
	INT i = 0
	REPEAT COUNT_OF(alienPed) i
		SHARED_DELETE_CLEANUP(alienPed[i])
	ENDREPEAT
ENDPROC

/// PURPOSE: 
///    Cleans up a single alien ped
/// PARAMS:
/// 	ent - reference to alien ped
///    	keepTask - set this to keep tasks after cleanup
PROC CLEANUP_ALIEN_PED(ALIEN_PED &ent, BOOL keepTask = TRUE)
	IF (ent.bIsActive = FALSE)
		EXIT
	ENDIF
	
	SHARED_DELETE_CLEANUP(ent)
	SAFE_RELEASE_PED(ent.pedID, keepTask, FALSE)
	ent.bIsActive = FALSE
ENDPROC

/// PURPOSE: 
///    Cleans up a single alien ped
/// PARAMS:
/// 	ent - reference to alien ped
///    	keepTask - set this to keep tasks after cleanup
PROC DELETE_ALIEN_PED(ALIEN_PED &ent)
	IF (ent.bIsActive = FALSE)
		EXIT
	ENDIF

	SHARED_DELETE_CLEANUP(ent)
	SAFE_DELETE_PED(ent.pedID)
	ent.bIsActive = FALSE
ENDPROC

/// PURPOSE: 
///    Cleans all alien peds
/// PARAMS:
///    	keepTask - set this to keep tasks after cleanup
PROC CLEANUP_ALL_ALIEN_PEDS(BOOL keepTask = TRUE)
	INT i = 0
	REPEAT COUNT_OF(alienPed) i
		CLEANUP_ALIEN_PED(alienPed[i], keepTask)
	ENDREPEAT
ENDPROC

/// PURPOSE: 
///    Deletes all alien peds
PROC DELETE_ALL_ALIEN_PEDS()
	INT i = 0
	REPEAT COUNT_OF(alienPed) i
		DELETE_ALIEN_PED(alienPed[i])
	ENDREPEAT
ENDPROC

PROC HANDLE_PLAYING_ALIEN_CLICKING_SOUND(ALIEN_PED &alien)

	IF alien.aState != AS_ACTIVE
	OR alien.bWallSpawn  // so as not to interfere with attack sound
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() > alien.iClickSoundTimer)
	
		IF alien.fDistToPlayer < kfMinClickSoundDist
			alien.iClickSoundTimer = GET_GAME_TIMER() +(GET_RANDOM_INT_IN_RANGE(iAlienMinClickTime, iAlienMaxClickTime))
			EXIT
		ENDIF
	
		//CPRINTLN(DEBUG_MISSION, "Playing click speech for alien ", alien.iAlienIndex)
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(alien.pedID, "ALIEN_CLICKS", "ALIENS", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
		alien.iClickSoundTimer = GET_GAME_TIMER() +(GET_RANDOM_INT_IN_RANGE(iAlienMinClickTime, iAlienMaxClickTime))
	ENDIF
ENDPROC

/// PURPOSE: 
///  	Creates one alien ped
/// PARAMS
///    	alien - alien ped reference
///    	pos - alien spawn point
///    	rank - this dicates the order people spawn in 
FUNC BOOL CREATE_ALIEN_PED(ALIEN_PED &alien, VECTOR pos, INT rank = 0, BOOL checkSpt = TRUE)

	// do a safety check here
	IF (checkSpt)
		IF NOT CHECK_SPAWN_POINT_IS_OK(pos)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// create the alien
	alien.pedID = CREATE_PED(PEDTYPE_MISSION, alienModel, pos)
	IF NOT DOES_ENTITY_EXIST(alien.pedID)
		RETURN FALSE
	ENDIF
	
	CLEAR_PED_TASKS_IMMEDIATELY(alien.pedID)
	SET_ENTITY_COORDS_GROUNDED(alien.pedID, pos)
	SET_ENTITY_HEADING_FACE_PLAYER(alien.pedID)

	SET_PED_RELATIONSHIP_GROUP_HASH(alien.pedID, RELGROUPHASH_HATES_PLAYER)
	SET_PED_CONFIG_FLAG(alien.pedID, PCF_DisableMelee, TRUE)
	STOP_PED_SPEAKING(alien.pedID, TRUE)
	DISABLE_PED_PAIN_AUDIO(alien.pedID, TRUE)
	SET_AMBIENT_VOICE_NAME(alien.pedID, "ALIENS")

	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(alien.pedID, TRUE)
	SET_PED_DEFAULT_COMPONENT_VARIATION(alien.pedID)
	SET_PED_AS_ENEMY(alien.pedID, TRUE)	
	SET_PED_MONEY(alien.pedID, 0)
	
	FREEZE_ENTITY_POSITION(alien.pedID, TRUE)
	SET_ENTITY_INVINCIBLE(alien.pedID, TRUE)
	SET_ENTITY_COLLISION(alien.pedID, FALSE)
	SET_ENTITY_VISIBLE(alien.pedID, FALSE)

	SET_ENTITY_CAN_BE_TARGETED_WITHOUT_LOS(alien.pedID, FALSE)
	SET_PED_CAN_BE_TARGETTED(alien.pedID, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(alien.pedID, TRUE)
	SET_ENTITY_MAX_HEALTH(alien.pedID, iAlienMaxHP)
	SET_ENTITY_HEALTH(alien.pedID, iAlienMaxHP)
	
	// create beam projector
	alien.projectorID = CREATE_OBJECT_NO_OFFSET(beamDummyModel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(alien.pedID, <<0, -5, 0>>))

	IF IS_ENTITY_OK(alien.projectorID)
		SET_ENTITY_COLLISION(alien.projectorID, FALSE)
		SET_ENTITY_VISIBLE(alien.projectorID, FALSE)
		FREEZE_ENTITY_POSITION(alien.projectorID, TRUE)
	ENDIF
	
	// create impact object
	alien.impactID = CREATE_OBJECT_NO_OFFSET(beamDummyModel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(alien.pedID, <<0, -5, 0>>))

	IF IS_ENTITY_OK(alien.impactID)
		SET_ENTITY_COLLISION(alien.impactID, FALSE)
		SET_ENTITY_VISIBLE(alien.impactID, FALSE)
		FREEZE_ENTITY_POSITION(alien.impactID, TRUE)
	ENDIF
	
	alien.screenImpactID = CREATE_OBJECT_NO_OFFSET(beamDummyModel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(alien.pedID, <<0, 5, 0>>))

	IF IS_ENTITY_OK(alien.screenImpactID)
		SET_ENTITY_COLLISION(alien.screenImpactID, FALSE)
		SET_ENTITY_VISIBLE(alien.screenImpactID, FALSE)
		FREEZE_ENTITY_POSITION(alien.screenImpactID, TRUE)
	ENDIF

	// turn him on
	alien.aState = AS_DELAY
	alien.bIsActive = TRUE
	alien.bWallSpawn = FALSE
	alien.iTimer = GET_GAME_TIMER() + (rank * GENDELAY_TIME)
	alien.iClickSoundTimer = GET_GAME_TIMER() + (GET_RANDOM_INT_IN_RANGE(iAlienMinClickTime, iAlienMaxClickTime))
	alien.fDistToPlayer = GET_DISTANCE_BETWEEN_ENTITIES(alien.pedID, PLAYER_PED_ID())	
	alien.fRunRatio = GET_RANDOM_FLOAT_IN_RANGE(PEDMOVE_RUN, PEDMOVE_SPRINT)
	alien.fBeamTriggerRange = fAlienBeamStartRange + (GET_RANDOM_FLOAT_IN_RANGE(fAlienMinBeamTriggerVariance, fAlienMaxBeamTriggerVariance) * fAlienBeamStartRange)
	alien.fPedAlpha = 0
	
	IF bDebugTTYSpew = TRUE
		CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " Beam Trigger Range = ", alien.fBeamTriggerRange )
	ENDIF
	
	TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienTeleportAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	iTotalAlienSpawnCount ++
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Set the alien to be more easily killed 
/// PARAMS:
///    alien - alien struct
PROC SET_ALIEN_WEAK(ALIEN_PED &alien)
	IF NOT IS_PED_INJURED(alien.pedID)
		CPRINTLN(DEBUG_MISSION, "SET_ALIEN_WEAK" )
		SET_ENTITY_HEALTH(alien.pedID, iAlienMaxHP/4)
		SET_PED_DIES_WHEN_INJURED(alien.pedID, TRUE)
		SET_PED_SUFFERS_CRITICAL_HITS(alien.pedID, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawn in a new alien under certain conditions
/// PARAMS:
///    iAlienCount - 
PROC SPAWN_NEXT_ALIEN_NEAR_LOCATION(VECTOR vCenterPt, FLOAT fRadius, INT iAlienCount = 5, BOOL bSetAsWeak = FALSE)
	INT i 
	VECTOR v

	// If the alien count drops low enough, spawn a new one if timer is up
	IF (iAlienAliveCount < iAlienCount)
		IF TIMER_DO_ONCE_WHEN_READY(tmr1stContactSpawn, f1stContactSpawnTime)
			IF GET_FREE_ALIEN_PED_INDEX(i)
				IF GENERATE_ALIEN_SPAWN_POINT_NEAR_LOCATION(vCenterPt, fRadius, v)
					IF CREATE_ALIEN_PED(alienPed[i], v, 0, FALSE)
						CPRINTLN(DEBUG_MISSION, "SPAWN_NEXT_ALIEN_NEAR_LOCATION: Alien ", alienPed[i].iAlienIndex, " Spawned!" )
						IF bSetAsWeak
							SET_ALIEN_WEAK(alienPed[i])
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "SPAWN_NEXT_ALIEN_NEAR_LOCATION: Could not spawn alien: ", alienPed[i].iAlienIndex)
					ENDIF
					START_TIMER_NOW(tmr1stContactSpawn)
					f1stContactSpawnTime = GET_RANDOM_FLOAT_IN_RANGE(1.500, 2.500)
				ELSE
					CPRINTLN(DEBUG_MISSION, "SPAWN_NEXT_ALIEN_NEAR_LOCATION: Could not generate a spawn point for alien: ", alienPed[i].iAlienIndex)
					START_TIMER_NOW(tmr1stContactSpawn)
					f1stContactSpawnTime = 1.000
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_AUTO_ALIEN_GENERATION()

	// increase the alien auto gen delay timer as the number of allowed aliens rises
	INT m = 0
	IF iMaxActiveAliens > kiMaxStartingAliens
		m = iMaxActiveAliens - kiMaxStartingAliens
		iAlienAutoGenDeltaTime = kiAlienAutoGenDeltaTime * m
		iAlienMinAutoGenTime = kiAlienMinAutoGenBaseTime + iAlienAutoGenDeltaTime
		iAlienMaxAutoGenTime = kiAlienMaxAutoGenBaseTime + iAlienAutoGenDeltaTime
	ENDIF
	
	// run the gauntlet of checks, reset timer
	// is auto create active
	IF NOT bAutoCreateAliens
		iAlienAutoGenTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienMinAutoGenTime, iAlienMaxAutoGenTime)
		EXIT
	ENDIF
	// are we doing an abduction attempt (if so, wait a little longer before spawning new aliens)
	IF bSummonFlagActive
		iAlienAutoGenTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienMinAutoGenTime*3, iAlienMaxAutoGenTime*2)
		EXIT
	ENDIF
	// do we have enough aliens already
	IF (iAlienAliveCount >= iMaxActiveAliens)
		iAlienAutoGenTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienMinAutoGenTime, iAlienMaxAutoGenTime)
		EXIT
	ENDIF
	
	INT i 
	VECTOR v	
	// timer so we don't spawn all possible aliens ones at once
	IF GET_GAME_TIMER() > iAlienAutoGenTime 
		IF GET_FREE_ALIEN_PED_INDEX(i)
			IF GENERATE_ALIEN_WALL_SPAWN(v, 0.4)
				CREATE_ALIEN_PED(alienPed[i], v, 0, FALSE)
				alienPed[i].bWallSpawn = TRUE
				alienPed[i].fBeamTriggerRange = 150
				iAlienAutoGenTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienMinAutoGenTime, iAlienMaxAutoGenTime)
				SET_ALIEN_WEAK(alienPed[i])
				//CPRINTLN(DEBUG_MISSION, "HANDLE_AUTO_ALIEN_GENERATION: (Wall spawn) Time until next alien spawn = ", iAlienAutoGenTime - GET_GAME_TIMER())
			ELIF GENERATE_ALIEN_SPAWN_POINT(v)
				CREATE_ALIEN_PED(alienPed[i], v, 0, FALSE)
				iAlienAutoGenTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienMinAutoGenTime, iAlienMaxAutoGenTime)
				//CPRINTLN(DEBUG_MISSION, "HANDLE_AUTO_ALIEN_GENERATION: Time until next alien spawn = ", iAlienAutoGenTime - GET_GAME_TIMER())
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//----------------------
//	ALIEN UPDATE FUNCTIONS
//----------------------

/// PURPOSE:
///    Dead Checking
/// PARAMS:
///    alien - alien reference
/// RETURNS:
///    Return true if the alien is dead
FUNC BOOL UPDATE_ALIEN_DEAD_CHECKING(ALIEN_PED &alien)
	IF NOT IS_ALIEN_DYING(alien)
		alien.fDistToPlayer = GET_DISTANCE_BETWEEN_ENTITIES(alien.pedID, PLAYER_PED_ID(), FALSE)
		IF IS_ENTITY_DEAD(alien.pedID)
		OR IS_PED_INJURED(alien.pedID)
		
			IF IS_ALIEN_IN_SUMMON_STATE(alien)
				bSummonRushFlagActive = FALSE
				bSummonFlagActive = FALSE
			ENDIF
			
			IF alien.aState = AS_SUMMON_START
				CPRINTLN(DEBUG_MISSION, "Alien Summon aborted because he died")
				IF(GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK)
					// Because he's perfoming a sequence, stopping the intro anim can put him into the looping anim.
					// Failsafe so player isn't stuck, plus he'll just straighten up rather than play his "win" anim.  
					CPRINTLN(DEBUG_MISSION, "Clearing player tasks, player should be free.")
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

			ENDIF
			
			PLAY_AMBIENT_SPEECH_FROM_POSITION("ALIEN_DEATH", "ALIENS", GET_ENTITY_COORDS(alien.pedID), SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
			
			REMOVE_ALIEN_PARTICLE_FX(alien)
			SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
			SAFE_DELETE_OBJECT(alien.projectorID)
			SAFE_DELETE_OBJECT(alien.impactID)
			SAFE_DELETE_OBJECT(alien.screenImpactID)
			
			PLAY_MICHAEL_FIGHT_DIALOG_LINES()
			SAFE_REMOVE_BLIP(alien.blipID)
			
			alien.iTimer = GET_GAME_TIMER() + 2500
			
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is dying")
			ENDIF
			
			iTotalAlienDeadCount ++	
			CPRINTLN(DEBUG_MISSION, "TOTAL DEAD ALIENS = ", iTotalAlienDeadCount)

			
			IF (iKillChain = 0)
				iKillChainEndTime = GET_GAME_TIMER() + CHAIN_KILL_TIME
			ENDIF	
				
			iKillChain ++
			IF (iKillChain > iMaxKillChain)
				iMaxKillChain = iKillChain 
			ENDIF
			
			IF (iKillChain >= CHAIN_KILL_AMOUNT)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(BA1_KILLCHAIN)
				CPRINTLN(DEBUG_MISSION, "INFORM STATS OF KILL CHAIN")
			ENDIF
			
			IF (iTotalAlienDeadCount % 5 = 0)
				
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "WAVE CLEAR")
				ENDIF	
				
				iMaxActiveAliens ++
				IF alien.aState != AS_SUMMON_START
					IF (bAllowSummoning = FALSE)
						CPRINTLN(DEBUG_MISSION, "UPDATE_ALIEN_DEAD_CHECKING: Reactivating Summon.")
						bAllowSummoning = TRUE
					ENDIF
				ELSE
					IF bDebugTTYSpew = TRUE
						CPRINTLN(DEBUG_MISSION, "UPDATE_ALIEN_DEAD_CHECKING: Alien was summoning, don't reactivate summon this time.")
					ENDIF
				ENDIF
				
				fAlienMinSpawnRadius -= 4.0
				IF (fAlienMinSpawnRadius < 10.0) 
					fAlienMinSpawnRadius = 10.0
				ENDIF
				
				IF (iMaxActiveAliens >= COUNT_OF(alienPed))
					iMaxActiveAliens = COUNT_OF(alienPed)

				ENDIF
			ENDIF
			
			alien.aState = AS_DYING
			
			RETURN TRUE
		ENDIF
	ELSE
		alien.fDistToPlayer = -1.0
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the next possible alien to be allowed to summon
PROC UPDATE_NEXT_ALIEN_SUMMONER_INDEX()
	INT i = 0
	FLOAT minDist = -1
	
	IF (bAllowSummoning = FALSE)
		iAlienSummonerIndex = -1
		EXIT 
	ENDIF	
	
	REPEAT (COUNT_OF(alienPed)) i
		IF IS_ALIEN_PED_ALIVE(alienPed[i]) AND (alienPed[i].aState = AS_ACTIVE)
			IF (minDist = -1) OR ((alienPed[i].fDistToPlayer < minDist) AND (alienPed[i].fDistToPlayer <> -1))
				iAlienSummonerIndex = i
				alienPed[i].fDistToPlayer = minDist
			ENDIF
		ENDIF
	ENDREPEAT	
ENDPROC


/// PURPOSE: 
///  	This handles the aliens day to day stuff 
/// PARAMS
///    	alien - alien ped reference
//PROC UPDATE_ALIEN_PED_BEAM_EVO(ALIEN_PED &alien)
//	VECTOR v
//	FLOAT sx, sy 
//	INT side1, side2
//
//	SET_ENTITY_COORDS_NO_OFFSET(alien.screenImpactID, GET_ENTITY_COORDS(alien.projectorID))
//	GET_LINE_PLANE_INTERSECT(v, GET_ENTITY_COORDS(alien.projectorID), GET_ENTITY_COORDS(alien.impactID), vCameraForward, vCameraCoords + (vCameraForward * fCameraNearClip))
//	
//	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(v, sx, sy)
//		SAFE_REMOVE_PARTICLE_FX(alien.fxScreenImpactID)
//		
//	 	alien.fBeamEvoValue -= fAlienBeamEvoIncrease
//		IF (alien.fBeamEvoValue < 0.0)
//			alien.fBeamEvoValue = 0.0
//		ENDIF
//	ELSE	
//		side1 = ENUM_TO_INT(GET_PLANE_SIDE(vCameraForward, vCameraCoords, GET_ENTITY_COORDS(alien.projectorID)))
//		side2 = ENUM_TO_INT(GET_PLANE_SIDE(vCameraForward, vCameraCoords, GET_ENTITY_COORDS(alien.impactID)))
//		
//		IF (side1 > 0) AND (side2 < 0)
//			SET_ENTITY_COORDS_NO_OFFSET(alien.screenImpactID, v - (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * fAlienScreenImpactShift))
//		ELIF (side1 < 0) AND (side2 > 0)
//			SET_ENTITY_COORDS_NO_OFFSET(alien.screenImpactID, v + (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * fAlienScreenImpactShift))
//		ENDIF
//		
//		alien.fBeamEvoValue = 1.0
//		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxScreenImpactID)
//			alien.fxScreenImpactID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_impact", alien.screenImpactID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, fAlienImpactEffectSize / 3.0)	
//		ENDIF
//		
//		//alien.fBeamLength = GET_DISTANCE_BETWEEN_ENTITIES(alien.screenImpactID, alien.projectorID)
//	ENDIF
//	
//	IF DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxBeamID)
//		SET_PARTICLE_FX_LOOPED_EVOLUTION(alien.fxBeamID, "angle", alien.fBeamEvoValue)
//	ENDIF
//ENDPROC

/// PURPOSE:
///    Converts a rotation vector into a normalised direction vector (basically the direction an entity would move if it moved forward at the current rotation).
///    For example: a rotation of <<0.0, 0.0, 45.0>> gives a direction vector of <<-0.707107, 0.707107, 0.0>>
/// PARAMS:
///    v_rot - The rotation vector
/// RETURNS:
///    The direction vector
FUNC VECTOR CONVERT_ROT_TO_DIR_VECTOR(VECTOR v_rot)
	RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

PROC UPDATE_BEAM_CAMERA_IMPACT(ALIEN_PED &alien)
	VECTOR camPos = GET_FINAL_RENDERED_CAM_COORD()
	VECTOR camDir = CONVERT_ROT_TO_DIR_VECTOR(GET_FINAL_RENDERED_CAM_ROT())
	VECTOR cameraPlanePos = camPos + (camDir * GET_FINAL_RENDERED_CAM_NEAR_CLIP())
	VECTOR impactPoint 
	FLOAT  fBeamToScreenLength
	FLOAT  fScreenImpactEffectSize = fAlienImpactEffectSize * 1.25
	INT side1, side2

	IF GET_LINE_PLANE_INTERSECT(impactPoint, GET_ENTITY_COORDS(alien.projectorID), GET_ENTITY_COORDS(alien.impactID), camDir, cameraPlanePos)  // 1st param is a reference
		//CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex,"'s beam is intersecting the camera plane")
		
		IF IS_SPHERE_VISIBLE(impactPoint, 0.5)
			//CPRINTLN(DEBUG_MISSION, "Impact sphere is visible for Alien ", alien.iAlienIndex)
		
			side1 = ENUM_TO_INT(GET_PLANE_SIDE(camDir, camPos, GET_ENTITY_COORDS(alien.projectorID)))
			side2 = ENUM_TO_INT(GET_PLANE_SIDE(camDir, camPos, GET_ENTITY_COORDS(alien.impactID)))
			
			// projector side
			IF (side1 > 0) AND (side2 < 0)
				impactPoint = impactPoint - (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * fAlienScreenImpactShift)
			// far side
			ELIF (side1 < 0) AND (side2 > 0)
				impactPoint = impactPoint + (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * (fAlienScreenImpactShift * 4.5))
			ENDIF
			
			// draw particles at impactpoint to hide hard edge.
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxScreenImpactID)
				alien.fxScreenImpactID = START_PARTICLE_FX_LOOPED_AT_COORD("scr_alien_impact", impactPoint, (<<0.0, 0.0, 0.0>>), fScreenImpactEffectSize) // / 3.0)	
			ENDIF
			
			//set beam length to be distance from beamStart to impactPoint
			fBeamToScreenLength = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(alien.projectorID, impactPoint)
			// If we've already impacted something, don't extend beam
			// But if we hit the camera first, let it through to hit the player
			IF alien.fBeamLength >= fBeamToScreenLength
			AND (side1 > 0) AND (side2 < 0)
				alien.fBeamLength = fBeamToScreenLength
			ENDIF
		ELSE
			SAFE_REMOVE_PARTICLE_FX(alien.fxScreenImpactID)
		ENDIF
	ELSE
		SAFE_REMOVE_PARTICLE_FX(alien.fxScreenImpactID)
	ENDIF
ENDPROC


/// PURPOSE: 
///  	This handles summon rush
/// PARAMS
///    	alien - alien ped reference
PROC UPDATE_ALIEN_SUMMON_RUSH(ALIEN_PED &alien)
	REMOVE_ALIEN_PARTICLE_FX(alien)
	SET_PED_RESET_FLAG(alien.pedID, PRF_InfiniteStamina, TRUE)
	SET_PED_NAME_DEBUG(alien.pedID, "S-RUSH")
	SET_PED_MOVE_RATE_OVERRIDE(alien.pedID, 1.15)
	SET_PED_DESIRED_MOVE_BLEND_RATIO(alien.pedID, PEDMOVE_SPRINT)
	
	IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
		TASK_GO_TO_ENTITY(alien.pedID, PLAYER_PED_ID())
	ENDIF

	IF (bSummonRushFlagActive)
	ENDIF
	
	IF (alien.fDistToPlayer > fAlienSummonRange) 
		EXIT
	ENDIF 
	
	IF NOT IS_MISSION_IN_COMBAT_STAGE()
		EXIT
	ENDIF
	
	IF IS_PED_FALLING(alien.pedID) OR IS_PED_GETTING_UP(alien.pedID)
	OR IS_PED_RAGDOLL(alien.pedID) OR IS_PED_RUNNING_RAGDOLL_TASK(alien.pedID)
		CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is down - cannot start summon")
		EXIT
	ENDIF

	
	IF NOT IS_PED_IN_PED_VIEW_CONE(alien.pedID, PLAYER_PED_ID()) 
		EXIT 
	ENDIF
	
	IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(alien.pedID, PLAYER_PED_ID()) 
		EXIT 
	ENDIF
	
	SEQUENCE_INDEX seq

	SET_ENTITY_HEADING_FACE_PLAYER(alien.pedID)
	OPEN_SEQUENCE_TASK(seq)
		//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())  // not quick enough
		TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sAlienGrabIntroAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
		TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sAlienGrabAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(alien.pedID, seq)
	CLEAR_SEQUENCE_TASK(seq)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(alien.pedID)
	
	SET_ENTITY_HEADING_FACE_ENTITY(PLAYER_PED_ID(), alien.pedID)
	OPEN_SEQUENCE_TASK(seq)
		TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sAlienGrabbedIntroAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
		TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sAlienGrabbedAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	CLOSE_SEQUENCE_TASK(seq)
	TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
	CLEAR_SEQUENCE_TASK(seq)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	
	IF bDebugTTYSpew = TRUE
		CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has started summon")
	ENDIF
	
	alien.aState = AS_SUMMON_START
	PLAY_MICHAEL_ABDUCT_DIALOG_LINE()
	
	// on the first time this happens give the player more time
	IF (iTotalAlienSummonCount = 0)
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			PRINT_HELP("BAR1_GRAB_HLP") // Michael is being abducted. Move ~INPUT_MOVE_LR~ rapidly to escape.
		ELSE
			PRINT_HELP("BAR1_GRAB_HPC") // Michael is being abducted. Rapidly press ~INPUT_MOVE_LR~ alternately to escape.
		ENDIF
		alien.iTimer = GET_GAME_TIMER() + iAlienSummonTimeLength + 2000
		SET_GAMEPLAY_PED_HINT(alien.pedID, <<0, 0, 0>>, TRUE, iAlienSummonTimeLength + 2000) 
	ELSE
		alien.iTimer = GET_GAME_TIMER() + iAlienSummonTimeLength 
		SET_GAMEPLAY_PED_HINT(alien.pedID, <<0, 0, 0>>, TRUE, iAlienSummonTimeLength) 				
	ENDIF
	
	iTotalAlienSummonCount ++
	alien.iButtonMashCount = 0

	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	fRegenMultiplier = 0.0
	
	SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
	alien.mindCtrlSoundID = GET_SOUND_ID()
	PLAY_SOUND_FROM_ENTITY(alien.mindCtrlSoundID, "PLAYER_BEAMED_UP", alien.pedID, "BARRY_01_SOUNDSET")
	
	// start fx
	START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_alien_teleport", PLAYER_PED_ID(), <<0, 0, 0>>, <<0, 0, 0>>, fAlienTeleportEffectSize)
	alien.iNextSummonEffectTime = GET_GAME_TIMER() + 2000
ENDPROC
			
/// PURPOSE: 
///  	This handles summon 
/// PARAMS
///    	alien - alien ped reference
PROC UPDATE_ALIEN_SUMMON(ALIEN_PED &alien)
	INT sgn 
	// check for button mash
	iAlienNextSummonTime = GET_GAME_TIMER() + iAlienNextSummonTimeOut
	bSummonFlagActive = TRUE 
	STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
	SET_PED_NAME_DEBUG(alien.pedID, "SUMMON")
	
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()

	// Steve R - Changed from script inputs to normal move inputs to fix PC mapping issues. Controls are disabled during this section so using disabled controls check.
	sgn = FSGN(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR))
	IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR) OR (sgn <> alien.fOldMashSign)
		alien.fOldMashSign = sgn
		alien.iButtonMashCount ++
	ENDIF

	// start fx
	IF (GET_GAME_TIMER() > alien.iNextSummonEffectTime)
		START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_alien_teleport", PLAYER_PED_ID(), <<0, 0, 0>>, <<0, 0, 0>>, fAlienTeleportEffectSize)
		alien.iNextSummonEffectTime = GET_GAME_TIMER() + 2000
	ENDIF
	
	// stop if we mash enough or if alien is interupted
	IF (alien.iButtonMashCount > iAlienButtonMashLimit)
	OR GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_PERFORM_SEQUENCE) > PERFORMING_TASK
		IF IS_ENTITY_PLAYING_ANIM(alien.pedID, sMissionAnimDictionary, sAlienGrabAnim)
		OR IS_ENTITY_PLAYING_ANIM(alien.pedID, sMissionAnimDictionary, sAlienGrabIntroAnim)
			TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienGrabFailAnim) 
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
			CPRINTLN(DEBUG_MISSION, "UPDATE_ALIEN_SUMMON: Player was playing ", sAlienGrabbedAnim)
			//STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			TASK_PLAY_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ELIF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
			CPRINTLN(DEBUG_MISSION, "UPDATE_ALIEN_SUMMON: Player was playing ", sAlienGrabbedIntroAnim)
			//STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			TASK_PLAY_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ELSE
			CPRINTLN(DEBUG_MISSION, "UPDATE_ALIEN_SUMMON: Failsafe: Player was playing non-standard anim.")
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			TASK_PLAY_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT()
		ENDIF
		//SET_PED_TO_RAGDOLL(alien.pedID, 6000, 6500, TASK_RELAX, FALSE, FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " summon has failed")
		ENDIF

		//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "CANT_LOSE_ENEMY_DRUNK", "MICHAEL_DRUNK", SPEECH_PARAMS_FORCE_NORMAL_CLEAR)
		
		SAFE_REMOVE_PARTICLE_FX(alien.fxSummonID)
		bSummonRushFlagActive = FALSE
		//bSummonFlagActive = FALSE
		alien.iButtonMashCount = 0
		alien.aState = AS_SUMMON_FAIL
		alien.iTimer = GET_GAME_TIMER() + 2000
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BAR1_GRAB_HLP")
			CLEAR_HELP()
		ENDIF

		// this records the player getting grabbed
		IF (iSummonEscapeCount = 0)
			FLOAT fRecordTime = (TO_FLOAT(iAlienSummonTimeLength + 2000) / 1000.0)
			CPRINTLN(DEBUG_MISSION, "Recording the Last ", fRecordTime ," seconds of game play as player broke out of abduction")
			REPLAY_RECORD_BACK_FOR_TIME(fRecordTime, 2.0, REPLAY_IMPORTANCE_LOW) 
		ENDIF
		
		iSummonEscapeCount ++
		EXIT
	ENDIF	
	
	// else fail the mission
	IF (GET_GAME_TIMER() > alien.iTimer)	
		TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienGrabWinAnim) 
		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		SET_MISSION_FAILED_ABDUCTED()
	ENDIF
ENDPROC

/// PURPOSE: 
///  	This handles start of charge 
/// PARAMS
///    	alien - alien ped reference
PROC UPDATE_ALIEN_CHARGE_START(ALIEN_PED &alien)
	INT hits
	SHAPETEST_STATUS st
	SEQUENCE_INDEX seqTask 
	VECTOR v1, pos, norm
	ENTITY_INDEX hitEntity
	
	v1 = GET_ENTITY_COORDS(alien.projectorID)
	IF NOT alien.bWallSpawn
		IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
			TASK_GO_TO_ENTITY(alien.pedID, PLAYER_PED_ID())
		ENDIF
	ELSE
		IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_STAND_STILL) <> PERFORMING_TASK
			TASK_STAND_STILL(alien.pedID, -1)
			alien.iTimer = GET_GAME_TIMER() + iAlienBeamChargeTime * 2
		ENDIF
	ENDIF
	
	IF NOT IS_MISSION_IN_COMBAT_STAGE()
		EXIT 
	ENDIF

	IF (alien.sBeamLOS = NULL) 
		pos = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0, 0, 0>>)
		alien.sBeamLOS = START_SHAPE_TEST_LOS_PROBE(v1, pos, SCRIPT_INCLUDE_ALL, PLAYER_PED_ID())
		IF GET_GAME_TIMER() > alien.iTimer
		AND alien.bWallSpawn
			SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
			alien.mindCtrlSoundID = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(alien.mindCtrlSoundID, "MIND_CONTROL", alien.pedID, "BARRY_01_SOUNDSET")
			alien.aState = AS_CHARGE
		ENDIF
	ELSE
		st = GET_SHAPE_TEST_RESULT(alien.sBeamLOS, hits, pos, norm, hitEntity)
		IF (st = SHAPETEST_STATUS_RESULTS_READY) 
			IF (hits = 0) AND IS_MISSION_IN_COMBAT_STAGE()
				OPEN_SEQUENCE_TASK(seqTask)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, "MIND_CONTROL_B_Enter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				CLOSE_SEQUENCE_TASK(seqTask)
				TASK_PERFORM_SEQUENCE(alien.pedID, seqTask)
				CLEAR_SEQUENCE_TASK(seqTask)	
				
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxChargeID)
					alien.fxChargeID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_charging", alien.projectorID, <<0, 0, 0>>, <<0, 0, 0>>, fAlienChargeEffectSize)
				ENDIF
				
				SET_ENTITY_COORDS(alien.impactID, v1)
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxImpactID)
					alien.fxImpactID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_impact", alien.impactID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, fAlienImpactEffectSize)
				ENDIF
				
				alien.aState = AS_CHARGE
				
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has started charging")
				ENDIF
				
				alien.iTimer = GET_GAME_TIMER() + iAlienBeamChargeTime
				
				SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
				alien.mindCtrlSoundID = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(alien.mindCtrlSoundID, "MIND_CONTROL", alien.pedID, "BARRY_01_SOUNDSET")
			ENDIF
			alien.sBeamLOS = NULL
		ELIF (st = SHAPETEST_STATUS_NONEXISTENT) 
			alien.sBeamLOS = NULL
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///  	This handles beam 
/// PARAMS
///    	alien - alien ped reference
PROC UPDATE_ALIEN_BEAM(ALIEN_PED &alien)
	INT hits
	SHAPETEST_STATUS st
	VECTOR plyrPos
	VECTOR v1, v2, pos, norm
	ENTITY_INDEX hitEntity
	
	SET_PED_NAME_DEBUG(alien.pedID, "BEAM")
	
	IF (GET_GAME_TIMER() >= alien.iTimer)
		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		REMOVE_ALIEN_PARTICLE_FX(alien)
		
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is teleporting out")
		ENDIF
		
		alien.aState = AS_TELEPORT_OUT_START
		EXIT
	ENDIF
	
	// abort entirely if summon is going on
	IF (bSummonFlagActive = TRUE)
		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		REMOVE_ALIEN_PARTICLE_FX(alien)
		alien.aState = AS_WATCH_SUMMON
		EXIT
	ENDIF
	
	// update beam movement
	IF (GET_FRAME_COUNT() >= alien.iLastImpactFrame + 8)
		alien.fBeamLength += fAlienBeamLengthInc
		IF (alien.fBeamLength > fAlienBeamMaximumLength)
			alien.fBeamLength = fAlienBeamMaximumLength
		ENDIF
	ENDIF
	
	// if the alien is falling over or getting up or if another alien is summoning - turn off beam 
	IF IS_ENTITY_ALIVE(alien.pedID)
		IF IS_PED_FALLING(alien.pedID) OR IS_PED_GETTING_UP(alien.pedID)
		OR IS_PED_RAGDOLL(alien.pedID) OR IS_PED_RUNNING_RAGDOLL_TASK(alien.pedID)
			//SAFE_REMOVE_PARTICLE_FX(alien.fxImpactID)
			REMOVE_ALIEN_PARTICLE_FX(alien)
			SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
			alien.fBeamLength = 0
			alien.aState = AS_BEAM_KNOCKDOWN
		ENDIF
	ENDIF
	
	// if the ped isn't playing anim - drop into beam knockdown state where we wait until he is playing the anim and re-orient the beam
	IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK

		alien.fBeamLength = 0
		
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has been knocked out of beam firing")
		ENDIF
	
		alien.sBeamLOS = NULL
		alien.aState = AS_BEAM_KNOCKDOWN
		alien.fBeamLength = 0
	ENDIF
	
	IF (alien.fBeamLength <= 0)
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxBeamID)
			SET_PARTICLE_FX_LOOPED_FAR_CLIP_DIST(alien.fxBeamID, alien.fBeamLength)
		ENDIF
	
		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		SAFE_REMOVE_PARTICLE_FX(alien.fxImpactID)
		alien.sBeamLOS = NULL
		EXIT
	ELSE
		//UPDATE_ALIEN_PED_BEAM_EVO(alien)
		UPDATE_BEAM_CAMERA_IMPACT(alien)
	ENDIF	
	
	// do collision
	//SET_ENTITY_RENDER_SCORCHED(PLAYER_PED_ID(), FALSE)
	v1 = GET_ENTITY_COORDS(alien.projectorID)
	v2 = v1 + (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * (alien.fBeamLength + 3))
	
	IF (alien.sBeamLOS = NULL)
		alien.sBeamLOS = START_SHAPE_TEST_LOS_PROBE(v1, v2, SCRIPT_INCLUDE_ALL, alien.pedID)
	ELSE
		st = GET_SHAPE_TEST_RESULT(alien.sBeamLOS, hits, pos, norm, hitEntity)
		
		IF (st = SHAPETEST_STATUS_NONEXISTENT)
			alien.sBeamLOS = NULL
		ELIF (st = SHAPETEST_STATUS_RESULTS_READY) 
			IF (hits > 0)
				alien.iLastImpactFrame = GET_FRAME_COUNT()	
				SET_ENTITY_RENDER_SCORCHED(PLAYER_PED_ID(), TRUE)
					
				alien.vLastHitPoint = GET_CLOSEST_POINT_ON_LINE(pos, v1, v2, FALSE)
				SET_ENTITY_COORDS(alien.impactID, alien.vLastHitPoint)
				alien.fBeamLength = GET_DISTANCE_BETWEEN_ENTITIES(alien.impactID, alien.projectorID)
				
				// check for hit damage

				plyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())				
				IF GET_DISTANCE_BETWEEN_COORDS(alien.vLastHitPoint, plyrPos) <= 0.70
					fRegenMultiplier = 0.0				
					IF ABSF(alien.vLastHitPoint.z - plyrPos.z) < 1.0
						
						// fake a reaction by the player character
						IF GET_FRAME_COUNT() % 20 = 0
							SHOOT_SINGLE_BULLET_BETWEEN_COORDS(v1, v2, 0, TRUE, WEAPONTYPE_PISTOL, NULL, FALSE)
						ENDIF
						
						IF (bAlienBeamKnockDown = TRUE)
							APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, fAlienBeamKnockDownForce * GET_ENTITY_FORWARD_VECTOR(alien.projectorID), <<0,0,0>>, 0, FALSE, TRUE, TRUE)
						ENDIF
						
						// B*942972
						IF (bBeamArmorHelpDisplayed = FALSE)
							IF (GET_PED_ARMOUR(PLAYER_PED_ID()) > 0)
								PRINT_HELP("BAR1_MIND_HLP")
								bBeamArmorHelpDisplayed = TRUE
							ENDIF
						ENDIF
					
						// damage outpuy
						
						IF (GET_GAME_TIMER() > alien.iNextDamageTime)
							PLAY_MICHAEL_DIALOG_LINES()
							
							hits = GET_RANDOM_INT_IN_RANGE(0, 6)
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								CASE 0
									APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),hits,0.657,0.566,126.360,0.07,1,0,"cs_trev1_blood")
								BREAK
								CASE 1
									APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),hits,0.582,0.383,111.360,0.05,3,0,"cs_trev1_blood")
								BREAK
								CASE 2
									APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),hits,0.709,0.396,126.360,0.00,3,0,"cs_trev1_blood")
								BREAK
								DEFAULT
									APPLY_PED_BLOOD_SPECIFIC(PLAYER_PED_ID(),hits,0.582,0.383,200.360,0.1,2,0,"cs_trev1_blood")
								BREAK
							ENDSWITCH
							
							IF ((GET_ENTITY_HEALTH(PLAYER_PED_ID()) - iAlienBeamDamage) <= 100)
								FLEE_ALL_ALIEN_PEDS()
								CLEANUP_ALL_ALIEN_PEDS(TRUE)
								DEATH_SEQUENCE(v1, v2)// GET_ENTITY_FORWARD_VECTOR(alien.projectorID))
								EXIT
							ELSE
								APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), iAlienBeamDamage, FALSE)
							ENDIF								
							alien.iNextDamageTime = GET_GAME_TIMER() + ROUND(iAlienBeamDamageDelay * 1.25)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iAlienBeamDamageDelay / 2, 255)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, iAlienBeamDamageDelay / 2, 255)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			alien.sBeamLOS = NULL
		ENDIF
	ENDIF			
	
	IF IS_ENTITY_ALIVE(alien.impactID)
		SET_ENTITY_COORDS(alien.impactID, GET_ENTITY_COORDS(alien.projectorID) + (GET_ENTITY_FORWARD_VECTOR(alien.projectorID) * alien.fBeamLength))
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxBeamID)
		SET_PARTICLE_FX_LOOPED_FAR_CLIP_DIST(alien.fxBeamID, alien.fBeamLength)
	ENDIF
	
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxImpactID)
		alien.fxImpactID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_impact", alien.impactID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, fAlienImpactEffectSize)
	ENDIF
ENDPROC

/// PURPOSE: 
///  	This handles the aliens day to day stuff 
/// PARAMS
///    	alien - alien ped reference
PROC UPDATE_ALIEN_PED(ALIEN_PED &alien)
	VECTOR pos  
	FLOAT z 
	
	// handle clean up
	IF NOT DOES_ENTITY_EXIST(alien.pedID)
		CLEANUP_ALIEN_PED(alien)
		EXIT
	ENDIF
	
	IF UPDATE_ALIEN_DEAD_CHECKING(alien) 
		IF (iAlienSummonerIndex = alien.iAlienIndex)
			IF bDebugTTYSpew = TRUE
				CPRINTLN(DEBUG_MISSION, "Alien Summoner ", alien.iAlienIndex, " has been vanquished")
			ENDIF
			iAlienSummonerIndex = -1
		ENDIF
	ENDIF
	
	HANDLE_PLAYING_ALIEN_CLICKING_SOUND(alien)
	
	// update the beam projector
	IF DOES_ENTITY_EXIST(alien.projectorID)
		pos = GET_PED_BONE_COORDS(alien.pedID, BONETAG_HEAD, vAlienBeamEmitterOffset)
		SET_ENTITY_COORDS_NO_OFFSET(alien.projectorID, pos)
		SET_ENTITY_COLLISION(alien.projectorID, FALSE)
	ENDIF
	
	// if the alien is in a beam state while summon is going on kick him out of it
	IF IS_ALIEN_IN_BEAM_STATE(alien) AND (bSummonFlagActive = TRUE)
		IF bDebugTTYSpew = TRUE
			CPRINTLN(DEBUG_MISSION, "Alien", alien.iAlienIndex, " is watching summon")
		ENDIF
		
		TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienWatchAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		REMOVE_ALIEN_PARTICLE_FX(alien)
		SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
		alien.aState = AS_WATCH_SUMMON
		alien.sBeamLOS = NULL
	ENDIF
	
	// shut down blip if player leaves area so we can put the direction blip back on
	IF NOT (bIsPlayerInBattleArea) 
		IF (alien.blipID <> NULL)
			SAFE_REMOVE_BLIP(alien.blipID)
		ENDIF
	ELIF IS_ALIEN_IN_BATTLE_STATE(alien) AND (alien.blipID = NULL)
		alien.blipID = CREATE_PED_BLIP(alien.pedID)
	ENDIF
	
	// update states
	SWITCH (alien.aState)
		CASE AS_DELAY	// delay before teleport in
			IF (GET_GAME_TIMER() > alien.iTimer)	
				alien.bHasDamaged = FALSE
				alien.sBeamLOS = NULL
				alien.aState = AS_TELEPORT_FXSTART
				alien.fPedAlpha = 0.0
				SET_ENTITY_ALPHA(alien.pedID, FLOOR(alien.fPedAlpha), FALSE)
			ENDIF
		BREAK
		CASE AS_TELEPORT_FXSTART // start teleport
			SET_ENTITY_COLLISION(alien.pedID, FALSE)
			FREEZE_ENTITY_POSITION(alien.pedID, TRUE)
			SET_PED_CAN_BE_TARGETTED(alien.pedID, FALSE)
			SET_ENTITY_HEADING_FACE_PLAYER(alien.pedID)
			//SET_PED_RELATIONSHIP_GROUP_HASH(alien.pedID, RELGROUPHASH_PLAYER)
			TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienTeleportAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_alien_teleport", alien.pedID, <<0, 0, 0>>, <<0, 0, 0>>, fAlienTeleportEffectSize)
			alien.iTimer = GET_GAME_TIMER() + TELEPORT_TIME
			alien.aState = AS_TELEPORT
			SET_PED_NAME_DEBUG(alien.pedID, "TELEPORT ST")
			PLAY_SOUND_FROM_ENTITY(-1, "SPAWN", alien.pedID, "BARRY_01_SOUNDSET")
			SAFE_REMOVE_BLIP(alien.blipID)
		BREAK
		CASE AS_TELEPORT
			alien.fPedAlpha += GET_FRAME_TIME() * 512.0
			IF (alien.fPedAlpha >= 255)
				alien.fPedAlpha = 255
			ENDIF
		
			IF (GET_GAME_TIMER() > alien.iTimer)
				FREEZE_ENTITY_POSITION(alien.pedID, FALSE)
				SET_ENTITY_INVINCIBLE(alien.pedID, FALSE)
				SET_PED_CAN_BE_TARGETTED(alien.pedID, TRUE)
				SET_ENTITY_COLLISION(alien.pedID, TRUE)
				SET_ENTITY_VISIBLE(alien.pedID, TRUE)
				alien.fPedAlpha = 255
				
				SET_PED_RELATIONSHIP_GROUP_HASH(alien.pedID, RELGROUPHASH_HATES_PLAYER)
				SET_PED_NAME_DEBUG(alien.pedID, "ACTIVE")
				alien.aState = AS_ACTIVE
			ENDIF
			
			IF (alien.fPedAlpha < 255)
				SET_PED_ALPHA(alien.pedID, FLOOR(alien.fPedAlpha), iAlphaCounter)	
			ELSE
				RESET_ENTITY_ALPHA(alien.pedID)
			ENDIF
		BREAK	
		CASE AS_TELEPORT_OUT_START
			FREEZE_ENTITY_POSITION(alien.pedID, TRUE)
			SET_ENTITY_INVINCIBLE(alien.pedID, TRUE)
			SET_ENTITY_COLLISION(alien.pedID, FALSE)
			SET_PED_CAN_BE_TARGETTED(alien.pedID, FALSE)
			TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienTeleportAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			//SET_PED_RELATIONSHIP_GROUP_HASH(alien.pedID, RELGROUPHASH_PLAYER)
			REMOVE_ALIEN_PARTICLE_FX(alien)
			SAFE_REMOVE_BLIP(alien.blipID)
			SAFE_STOP_AND_RELEASE_SOUND_ID(alien.mindCtrlSoundID)
			
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_alien_teleport", GET_ENTITY_COORDS(alien.pedID), GET_ENTITY_ROTATION(alien.pedID), fAlienTeleportEffectSize)
			PLAY_SOUND_FROM_ENTITY(-1, "SPAWN", alien.pedID, "BARRY_01_SOUNDSET")
			alien.iTimer = GET_GAME_TIMER() + TELEPORT_TIME
			alien.aState = AS_TELEPORT_OUT
			SET_PED_NAME_DEBUG(alien.pedID, "TPORT OUT")
		BREAK
		CASE AS_TELEPORT_OUT
			alien.fPedAlpha = ((alien.iTimer - GET_GAME_TIMER()) * 255) / TO_FLOAT(TELEPORT_TIME)
			//CPRINTLN(DEBUG_MISSION, "ALIEN ALPHA", alien.fPedAlpha )
			IF (alien.fPedAlpha < 0.0)
				alien.fPedAlpha = 0.0
			ENDIF
					
			SET_PED_ALPHA(alien.pedID, FLOOR(alien.fPedAlpha), iAlphaCounter)	
			IF (GET_GAME_TIMER() > alien.iTimer) OR (alien.fPedAlpha = 0.0)
				SET_PED_NAME_DEBUG(alien.pedID, "RELOCATE")
				alien.aState = AS_RELOCATE
			ENDIF
		BREAK	
		CASE AS_RELOCATE
			//SET_PED_RELATIONSHIP_GROUP_HASH(alien.pedID, RELGROUPHASH_PLAYER)
			SET_ENTITY_VISIBLE(alien.pedID, FALSE)
			SET_ENTITY_COLLISION(alien.pedID, FALSE)
			SET_PED_CAN_BE_TARGETTED(alien.pedID, FALSE)
			IF (bKillAliensOnTeleportOut = TRUE)
				DELETE_ALIEN_PED(alien)
				alien.bIsActive = FALSE
				EXIT
			ENDIF
			
			// teleport to a new location
			
			IF GENERATE_ALIEN_SPAWN_POINT(pos)
				SET_ENTITY_COORDS_GROUNDED(alien.pedID, pos)
				alien.iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1250, 2000)
				alien.fBeamTriggerRange = fAlienBeamStartRange + (GET_RANDOM_FLOAT_IN_RANGE(fAlienMinBeamTriggerVariance, fAlienMaxBeamTriggerVariance) * fAlienBeamStartRange)
				
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " Beam Trigger Range = ", alien.fBeamTriggerRange )
				ENDIF
				
				alien.aState = AS_DELAY
			ENDIF
		BREAK
		CASE AS_ACTIVE
			SET_PED_NAME_DEBUG(alien.pedID, "ACTIVE")
	
			IF NOT alien.bWallSpawn
				IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
					TASK_GO_TO_ENTITY(alien.pedID, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF NOT IS_MISSION_IN_COMBAT_STAGE()
			 	EXIT 
			ENDIF
			
			IF CAN_ALIEN_SUMMON(alien)
				SET_PED_DESIRED_MOVE_BLEND_RATIO(alien.pedID, PEDMOVE_SPRINT)
				IF (alien.fDistToPlayer < (fAlienSummonRange + 8.0)) 
					alien.aState = AS_SUMMON_RUSH
					
					IF bDebugTTYSpew = TRUE
						CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has started summon rush")
					ENDIF
					bSummonRushFlagActive = TRUE
				ENDIF
			ELIF (alien.fDistToPlayer < (alien.fBeamTriggerRange + 12.0)) 
			
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has started charge rush")
				ENDIF
				
				alien.aState = AS_CHARGE_RUSH
			ENDIF
		BREAK
		CASE AS_SUMMON_RUSH
			UPDATE_ALIEN_SUMMON_RUSH(alien)
		BREAK
		CASE AS_SUMMON_START
			UPDATE_ALIEN_SUMMON(alien)
		BREAK
		CASE AS_SUMMON_FAIL
			SET_PED_NAME_DEBUG(alien.pedID, "SUMMON FAIL")
			IF (iAlienSummonerIndex = alien.iAlienIndex)
				iAlienSummonerIndex = -1
			ENDIF
			
			// Speed up and shorten Michael's win anim to get back in the action faster
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim)
				//SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim, 1.5)
				//IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim) >= 0.735
				//	CPRINTLN(DEBUG_MISSION, "AS_SUMMON_FAIL: Stopping anim ", sAlienGrabbedFailAnim)
				//	STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim, SLOW_BLEND_OUT)
				//ENDIF
			ELIF (GET_GAME_TIMER() > alien.iTimer)
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "AS_SUMMON_FAIL: Alien ", alien.iAlienIndex, " is teleporting out")
				ENDIF
				
				IF NOT IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
					CPRINTLN(DEBUG_MISSION, "Putting Player back into action mode")
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				// @SBA - fix rare case in which player is unable to reload after an abduction attempt
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), PlayerWeapon)
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), PlayerWeapon) > 0
						INT iAmmoLeft
						//CPRINTLN(DEBUG_MISSION, "AS_SUMMON_FAIL: Player's weapon ", GET_WEAPON_NAME(PlayerWeapon), " has ammo.")
						IF GET_AMMO_IN_CLIP(PLAYER_PED_ID(), PlayerWeapon, iAmmoLeft)
							IF iAmmoLeft = 0
								CPRINTLN(DEBUG_MISSION, "AS_SUMMON_FAIL: Player's clip is empty for weapon ", GET_WEAPON_NAME(PlayerWeapon), ".  Refilling clip.")
								REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				bSummonFlagActive = FALSE
				alien.aState = AS_TELEPORT_OUT_START
				EXIT
			ENDIF
		BREAK
		CASE AS_WATCH_SUMMON
			SET_PED_NAME_DEBUG(alien.pedID, "WATCH")
			IF (bSummonFlagActive = TRUE)
				IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
					TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, sAlienWatchAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				EXIT 
			ENDIF
			
			alien.aState = AS_WATCH_SUMMON_END
			alien.iTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iAlienWatchSummonTimeOut - 1500, iAlienWatchSummonTimeOut - 500)
		BREAK
		CASE AS_WATCH_SUMMON_END
			SET_PED_NAME_DEBUG(alien.pedID, "WATCH END")
			IF (GET_GAME_TIMER() > alien.iTimer) AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim)
				alien.aState = AS_ACTIVE
			ENDIF
		BREAK
		CASE AS_CHARGE_RUSH
			REMOVE_ALIEN_PARTICLE_FX(alien)
			SET_PED_RESET_FLAG(alien.pedID, PRF_InfiniteStamina, TRUE)
			SET_PED_NAME_DEBUG(alien.pedID, "C-RUSH")
			
			IF NOT alien.bWallSpawn
				SET_PED_MOVE_RATE_OVERRIDE(alien.pedID, 1.15)
				SET_PED_DESIRED_MOVE_BLEND_RATIO(alien.pedID, PEDMOVE_SPRINT)
				
				IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
					TASK_GO_TO_ENTITY(alien.pedID, PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF (alien.fDistToPlayer < alien.fBeamTriggerRange) AND IS_MISSION_IN_COMBAT_STAGE()
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(alien.pedID, "ALIEN_HOSTILE", "ALIENS", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				alien.aState = AS_CHARGE_START
				alien.sBeamLOS = NULL
			ELIF bSummonFlagActive
				alien.aState = AS_WATCH_SUMMON
			ENDIF
		BREAK
		CASE AS_CHARGE
			SET_ENTITY_COORDS(alien.impactID, GET_ENTITY_COORDS(alien.projectorID))
			SET_PED_NAME_DEBUG(alien.pedID, "CHARGE")
			IF (GET_GAME_TIMER() > alien.iTimer) 
				IF IS_MISSION_IN_COMBAT_STAGE()
					SAFE_REMOVE_PARTICLE_FX(alien.fxSummonID)
					SAFE_REMOVE_PARTICLE_FX(alien.fxChargeID)
					SAFE_REMOVE_PARTICLE_FX(alien.fxBeamID)
					SAFE_REMOVE_PARTICLE_FX(alien.fxImpactID)
					
					IF bDebugTTYSpew = TRUE
						CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is firing")
					ENDIF
					
					alien.aState = AS_BEAM_START
				ENDIF
			ENDIF
		BREAK
		CASE AS_CHARGE_START
			UPDATE_ALIEN_CHARGE_START(alien)
		BREAK
		CASE AS_BEAM_START
			IF NOT IS_PED_RUNNING_RAGDOLL_TASK(alien.pedID)
				SET_PED_NAME_DEBUG(alien.pedID, "BEAM ST")
				SET_ENTITY_HEADING_FACE_PLAYER(alien.pedID)
				pos = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0, 0, 0>>)
				SET_ENTITY_ROTATION_FACE_COORDS(alien.projectorID, pos)
				
				alien.iTimer = GET_GAME_TIMER() + iAlienBeamShootTime
				
				SAFE_REMOVE_PARTICLE_FX(alien.fxSummonID)
				SAFE_REMOVE_PARTICLE_FX(alien.fxChargeID)
				SAFE_REMOVE_PARTICLE_FX(alien.fxBeamID)
				SAFE_REMOVE_PARTICLE_FX(alien.fxImpactID)
					
				alien.fBeamLength = 0.0
				alien.fxChargeID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_impact", alien.projectorID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, fAlienImpactEffectSize / 3.0)
				alien.fxBeamID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_beam", alien.projectorID, <<0.0, 0.0, 0.0>>, vAlienBeamRotationVector, fAlienBeamEffectSize) 
				SET_PARTICLE_FX_LOOPED_FAR_CLIP_DIST(alien.fxBeamID, alien.fBeamLength)
				
				TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, "MIND_CONTROL_B_Loop", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				alien.aState = AS_BEAM
			ENDIF
		BREAK
		CASE AS_BEAM
			UPDATE_ALIEN_BEAM(alien)
		BREAK
		CASE AS_BEAM_KNOCKDOWN
			alien.fBeamLength = 0
			IF IS_ENTITY_ALIVE(alien.pedID)
				IF IS_PED_FALLING(alien.pedID) OR IS_PED_GETTING_UP(alien.pedID) OR
				   IS_PED_RAGDOLL(alien.pedID) OR IS_PED_RUNNING_RAGDOLL_TASK(alien.pedID)
				   EXIT
				ENDIF
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
				TASK_PLAY_ANIM(alien.pedID, sMissionAnimDictionary, "MIND_CONTROL_B_LOOP", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			ENDIF
			
			IF GET_SCRIPT_TASK_STATUS(alien.pedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK 
				SET_ENTITY_HEADING(alien.projectorID, GET_ENTITY_HEADING(alien.pedID))
				
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(alien.fxChargeID)
					alien.fxChargeID = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_alien_charging", alien.projectorID, <<0, 0, 0>>, <<0, 0, 0>>, fAlienChargeEffectSize)
				ENDIF
				
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " has resumed beam firing")
				ENDIF
				
				alien.aState = AS_BEAM
			ENDIF
		BREAK
		CASE AS_DYING
			IF HAS_ALIEN_FINSHED_DYING(alien) OR (GET_GAME_TIMER() > alien.iTimer)	
				pos = GET_ENTITY_COORDS(alien.pedID)
				IF GET_GROUND_Z_FOR_3D_COORD(pos, z)
					pos.z = z 
				ENDIF
				
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_alien_disintegrate", alien.pedID, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, fAlienDeathEffectSize)
				PLAY_SOUND_FROM_COORD(-1, "DESPAWN", pos, "BARRY_01_SOUNDSET")
				
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is started dead")
				ENDIF
				
				alien.aState = AS_DEAD
				SET_PED_NAME_DEBUG(alien.pedID, "DEAD")
				alien.iTimer = GET_GAME_TIMER() + ALIEN_FADE_TIME
			ENDIF
		BREAK
		CASE AS_DEAD
			alien.fPedAlpha = ((alien.iTimer - GET_GAME_TIMER()) * 255) / TO_FLOAT(ALIEN_FADE_TIME)
			//CPRINTLN(DEBUG_MISSION, "ALIEN ALPHA", alien.fPedAlpha )
			IF (alien.fPedAlpha < 0.0)
				alien.fPedAlpha = 0.0
			ENDIF
			
			IF IS_ENTITY_ON_SCREEN(alien.pedID)
				SET_PED_ALPHA(alien.pedID, FLOOR(alien.fPedAlpha), iAlphaCounter)
			ELSE
				RESET_ENTITY_ALPHA(alien.pedID)
			ENDIF
			
			IF (GET_GAME_TIMER() > alien.iTimer) OR (alien.fPedAlpha <= 0.0)
			
				IF bDebugTTYSpew = TRUE
					CPRINTLN(DEBUG_MISSION, "Alien ", alien.iAlienIndex, " is dead")
				ENDIF
				
				RESET_ENTITY_ALPHA(alien.pedID)
				DELETE_ALIEN_PED(alien)
				alien.bIsActive = FALSE
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///  	This handles all aliens day to day stuff 
PROC UPDATE_ALIEN_PEDS()
	INT i = 0

	iAlienAliveCount = 0
	REPEAT (COUNT_OF(alienPed)) i
		IF (alienPed[i].bIsActive)
			UPDATE_ALIEN_PED(alienPed[i])
			iAlienAliveCount ++
		ENDIF
	ENDREPEAT	
	
	// clear the summoner index if time out is active
	IF (GET_GAME_TIMER() < iAlienNextSummonTime)
		iAlienSummonerIndex = -1
	ELIF (iAlienSummonerIndex = -1) 
		UPDATE_NEXT_ALIEN_SUMMONER_INDEX()
	ENDIF
ENDPROC

/// PURPOSE: 
///  	This teleports out all aliens
/// PARAMS:
///    	tWait - If this is set pause script for 2000ms while aliens teleport out
PROC TELEPORT_OUT_ALL_ALIEN_PEDS(BOOL tWait = FALSE)
	INT i = 0
	INT n = 0
	
	bKillAliensOnTeleportOut = TRUE
	REPEAT COUNT_OF(alienPed) i
		IF IS_ALIEN_PED_ALIVE(alienPed[i])
			alienPed[i].aState = AS_TELEPORT_OUT_START
			n ++
		ENDIF
	ENDREPEAT
	
	IF (tWait = FALSE) OR (n = 0)
		EXIT
	ENDIF
	
	i = GET_GAME_TIMER() + 2000
	WHILE (GET_GAME_TIMER() < i)
		UPDATE_ALIEN_PEDS()
		WAIT(0)
	ENDWHILE
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	
	m_WidgetGroup = START_WIDGET_GROUP("Barry 1 Config")
	
		START_WIDGET_GROUP("Test Options")
			ADD_WIDGET_BOOL("Show Debug Stuff", bDebugTTYSpew)
			ADD_WIDGET_BOOL("Show Mission Timer", bShowDebugMissionTime)
			ADD_WIDGET_BOOL("Update Widgets", bUpdateWidgets)
			ADD_WIDGET_BOOL("Show Alien Pos", bShowAlienPos)
			ADD_WIDGET_BOOL("bUseIsAreaOccupied", bUseIsAreaOccupied)
			ADD_WIDGET_FLOAT_READ_ONLY("Player Regen Muliplier", fRegenMultiplier)
			ADD_WIDGET_FLOAT_SLIDER("Player Damage Muliplier", fPlayerWeaponDamageModifier, 0.0, 5.0, 0.05)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Spawn Options")
			ADD_WIDGET_INT_SLIDER("Max Active Aliens", iMaxActiveAliens, 0, COUNT_OF(alienPed) - 1, 1)
			ADD_WIDGET_FLOAT_SLIDER("Minimum Spawn Range", fAlienMinSpawnRadius, 5.0, 100.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Maximum Spawn Range", fAlienMaxSpawnRadius, 10.0, 150.0, 0.5)
			ADD_WIDGET_INT_SLIDER("Aliens Kill Limit", iTotalAlienDeadRequired, 1, 100, 1)
			ADD_WIDGET_INT_SLIDER("Alien Max HP", iAlienMaxHP, 200, 20000, 125)
			
			ADD_WIDGET_BOOL("Use Custom Spawn Point", bUseAlienSpawnPoint)
			ADD_WIDGET_VECTOR_SLIDER("Spawn Point Position", vAlienSpawnPoint, -6000.0, 6000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Spawn Point Radius", fAlienSpawnPointRadius, 5.0, 100.0, 0.5)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Area Options")
			ADD_WIDGET_BOOL("Player In Battle Area", bIsPlayerInBattleArea)
			ADD_WIDGET_VECTOR_SLIDER("Battle Center", vBattleCenter, -6000.0, 6000.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Battle Range", fBattleRadius, 5.0, 1000.0, 0.5)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Beam Special Attack")
			START_WIDGET_GROUP("Parameters")
				ADD_WIDGET_FLOAT_SLIDER("Attack Range", fAlienBeamStartRange, 1, 40.0, 0.5)
				ADD_WIDGET_INT_SLIDER("Charge Time", iAlienBeamChargeTime, 500, 20000, 500)
				ADD_WIDGET_INT_SLIDER("Duration", iAlienBeamShootTime, 3000, 60000, 500)
				ADD_WIDGET_FLOAT_SLIDER("Maximum Length", fAlienBeamMaximumLength, 0, 200.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("Length Increase", fAlienBeamLengthInc, 0.1, 20.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("Min Trigger Variance", fAlienMinBeamTriggerVariance, -1.0, 0.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("Max Trigger Variance", fAlienMaxBeamTriggerVariance, 0.0, 1.0, 0.05)
			STOP_WIDGET_GROUP()	
			
			START_WIDGET_GROUP("Damage Options")
				ADD_WIDGET_BOOL("Apply Pushback Force", bAlienBeamKnockDown)
				ADD_WIDGET_FLOAT_SLIDER("Force Scalar", fAlienBeamKnockDownForce, 0, 20.0, 0.25)
				ADD_WIDGET_INT_SLIDER("Damage", iAlienBeamDamage, 0, 100, 1)
				ADD_WIDGET_INT_SLIDER("Damage Rate", iAlienBeamDamageDelay, 1, 2000, 25)
			STOP_WIDGET_GROUP()	
			
			START_WIDGET_GROUP("Emitter Options")
				ADD_WIDGET_VECTOR_SLIDER("Beam Rotation", vAlienBeamRotationVector, -360.0, 360.0, 0.5)
				ADD_WIDGET_VECTOR_SLIDER("Beam Offset", vAlienBeamEmitterOffset, -10.0, 10.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("Radius", fAlienBeamEffectSize, 0.001, 10.0, 0.025)
			STOP_WIDGET_GROUP()	
			
			START_WIDGET_GROUP("FX Options")
				ADD_WIDGET_FLOAT_SLIDER("Charge Effect Scale", fAlienChargeEffectSize, 0, 5.0, 0.025)
				ADD_WIDGET_FLOAT_SLIDER("Death Effect Scale", fAlienDeathEffectSize, 0, 5.0, 0.025)
				ADD_WIDGET_FLOAT_SLIDER("Impact Effect Scale", fAlienImpactEffectSize, 0, 5.0, 0.025)
				ADD_WIDGET_FLOAT_SLIDER("Teleport Effect Scale", fAlienTeleportEffectSize, 0, 5.0, 0.025)
			STOP_WIDGET_GROUP()		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Summon Special Attack")
			ADD_WIDGET_BOOL("Allow Summoning", bAllowSummoning)
			
			START_WIDGET_GROUP("Parameters")
				ADD_WIDGET_FLOAT_SLIDER("Attack Range", fAlienSummonRange, 1, 20.0, 0.5)
				ADD_WIDGET_INT_SLIDER("Mash Limit", iAlienButtonMashLimit, 1, 20, 1)
				ADD_WIDGET_INT_SLIDER("Duration", iAlienSummonTimeLength, 3000, 20000, 500)
				ADD_WIDGET_INT_SLIDER("Time Out", iAlienNextSummonTimeOut, 3000, 60000, 500)
				ADD_WIDGET_INT_SLIDER("Watch Time Out", iAlienWatchSummonTimeOut, 1000, 8000, 500)
			STOP_WIDGET_GROUP()	
			
			START_WIDGET_GROUP("Read Only Values")
				ADD_WIDGET_INT_READ_ONLY("Current Game Time", iCurrentGameTime)
				ADD_WIDGET_INT_READ_ONLY("Next Summon Time", iAlienNextSummonTime)
				ADD_WIDGET_INT_READ_ONLY("Summoner Index", iAlienSummonerIndex)
				ADD_WIDGET_BOOL("Is Summon Rush Active", bSummonRushFlagActive)
				ADD_WIDGET_BOOL("Is Summon Active", bSummonFlagActive)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Read Only Values")
			ADD_WIDGET_INT_READ_ONLY("Current Game Time", iCurrentGameTime)

			START_WIDGET_GROUP("Dialog Stats")
				ADD_WIDGET_BOOL("Has First Outburst", bHasInitialOutburst)
				ADD_WIDGET_INT_READ_ONLY("Initial Speech Time", iFirstDialogTime)
				ADD_WIDGET_INT_READ_ONLY("Next Fight Dialog Time", iNextFightDialogTime)
				ADD_WIDGET_INT_READ_ONLY("Next Freak Dialog Time", iNextFreakDialogTime)
				ADD_WIDGET_INT_READ_ONLY("Next Damage Dialog Time", iNextDamageDialogTime)
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Mission Stats")
				ADD_WIDGET_INT_READ_ONLY("Total Aliens Spawned", iTotalAlienSpawnCount)
				ADD_WIDGET_INT_READ_ONLY("Total Aliens Killed", iTotalAlienDeadCount)
				ADD_WIDGET_INT_READ_ONLY("Total Summon Attempts", iTotalAlienSummonCount)
				ADD_WIDGET_INT_READ_ONLY("Current Kill Chain", iKillChain)
				ADD_WIDGET_INT_READ_ONLY("Maximum Kill Chain", iMaxKillChain)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///    		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC 

#ENDIF

//----------------------
//	SCRIPT FLOW
//----------------------

/// PURPOSE: 
///  	Handles the Pass Out Sequence, it's animations and dialog
/// PARAMS:
///    	pWait - Do we wait 2.5 seconds
FUNC BOOL PASS_OUT_SEQUENCE(BOOL bPass = FALSE)	
	VECTOR v
	BOOL bOK = FALSE
	SEQUENCE_INDEX seqTask
	CAMERA_INDEX cCamera
	
	// do global stuff here
	UPDATE_ALIEN_PEDS()
		
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()))
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		ENDIF
		SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandleBarry, ANCHOR_EYES, 0)
		CPRINTLN(DEBUG_MISSION, "PED CUTSCENE VARIATIONS SET")
	ENDIF
	
	// flow of the pass out sequence
	SWITCH ePassOutStage
		
		CASE POS_SETUP
			// -----setup--------------------------------
			
			IF (bPass = TRUE)
				RC_REQUEST_CUTSCENE("bar_2_rcm")
				REQUEST_SCRIPT("postRC_Barry1and2")
			ENDIF
			// make player tired [B*540333] 
			//FLEE_ALL_ALIEN_PEDS()
			PREPARE_MUSIC_EVENT("RC18A_END_OS")
			CPRINTLN(DEBUG_MISSION, "Interactive Music Event: RC18A_COLLAPSE Prepared")
			CPRINTLN(DEBUG_MISSION, "Pass Out - 2.5 Second Warm Up")
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

			ePassOutStage = POS_WAIT_FOR_TIRED
			STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 2.0)
		BREAK
		CASE POS_WAIT_FOR_TIRED
			// -----wait for tired--------------------------------
			IF (bPass = FALSE)
				IF IS_ENTITY_OK(gunObject)
					DETACH_ENTITY(gunObject, DEFAULT, FALSE)
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "Pass Out - Should be playing the tired anim here")
				OPEN_SEQUENCE_TASK(seqTask)
					TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sExhaustedAnimEnter, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
					TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sExhaustedAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(seqTask)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqTask)
				CLEAR_SEQUENCE_TASK(seqTask)
				
			ENDIF			
			
			cCamera = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV(), TRUE)
			v = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_GAMEPLAY_CAM_COORD()
			ATTACH_CAM_TO_ENTITY(cCamera, PLAYER_PED_ID(), -v)

			IF DOES_CAM_EXIST(cCamera)
				SET_CAM_ACTIVE(cCamera, TRUE)
			ENDIF
			
			// fail safe
			iFailTimer = GET_GAME_TIMER() + 6000
				
			ADD_PED_FOR_DIALOGUE(convoMichael, ENUM_TO_INT(CHAR_MICHAEL), PLAYER_PED_ID(), "MICHAEL")
			
			CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Going to Start Dialog State")
			ePassOutStage = POS_START_DIALOG
		BREAK
		CASE POS_START_DIALOG
			
			// -----start dialog--------------------------------
			bOK = FALSE
			
			IF (bPass = TRUE)
				IF CREATE_CONVERSATION(convoMichael, "BARY1AU", "BARY1_PREOUT", CONV_PRIORITY_HIGH)
				OR (GET_GAME_TIMER() > iFailTimer)
					IF (GET_GAME_TIMER() < iFailTimer)
						CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Created")
					ELSE
						CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Not Created - Emergency Abort")
					ENDIF
					
					iFailTimer = GET_GAME_TIMER() + 4500
					bOK = TRUE
				ENDIF
			ELSE
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(convoMichael, "BARY1AU", "BARY1_PREOUT", "BARY1_PREOUT_2", CONV_PRIORITY_HIGH)
				OR (GET_GAME_TIMER() > iFailTimer)
					iFailTimer = GET_GAME_TIMER() + 2000
					
					IF (GET_GAME_TIMER() < iFailTimer)
						CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Created")
					ELSE
						CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Not Created - Emergency Abort")
					ENDIF
					
					bOK = TRUE
				ENDIF
			ENDIF
			
			IF (bOK = TRUE)
				CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - We should be ready to play line now")
				ePassOutStage = POS_TRANSITION_FX
			ENDIF
		BREAK
		
		CASE POS_TRANSITION_FX
			bOK = FALSE
			IF (bPass = TRUE)
				IF (GET_GAME_TIMER() > iFailTimer)
					// @SBA - filter for passing out
					CLEANUP_SCREEN_FX()
					ANIMPOSTFX_PLAY(FX_DrugsOut, 0, FALSE)
					// wait for FX white out
					iFailTimer = GET_GAME_TIMER() + 3500
					bOK = TRUE
				ENDIF
			ELSE
				bOK = TRUE
			ENDIF
			
			IF (bOK = TRUE)
				CPRINTLN(DEBUG_MISSION, "Pass Out - FX should be transitioning")
				ePassOutStage = POS_WAIT_DIALOGUE
			ENDIF
		BREAK
		
		CASE POS_WAIT_DIALOGUE
		
			IF (bPass = TRUE)
				bOK = (GET_GAME_TIMER() > iFailTimer)
			ELSE			
				IF (GET_GAME_TIMER() < iFailTimer)
					CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Finished")
				ELSE
					CPRINTLN(DEBUG_MISSION, "Pass Out - Start Dialog - Conversation Not Finished - Emergency Abort")
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "Pass Out - Setting Player To Rag Doll")
				CPRINTLN(DEBUG_MISSION, "Interactive Music Event: RC18A_COLLAPSE")
				
				// To slow down his ragdoll fall
				SET_TIME_SCALE(0.6)
				
				// make the player pass out
				gunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(playerWeapon)
				
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 7000, 7500, TASK_RELAX, FALSE, FALSE)
					
					// if for any reason the gun hasn't detached...
					IF IS_ENTITY_OK(gunObject) AND IS_ENTITY_ATTACHED(gunObject)
						DETACH_ENTITY(gunObject, DEFAULT, FALSE)
					ENDIF
				ENDIF
				bOK = TRUE
			ENDIF
				
			IF (bOK = TRUE)
				CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_END_OS")
				TRIGGER_MUSIC_EVENT("RC18A_END_OS") 
				
				iFailTimer = GET_GAME_TIMER() + 1000 //3500
				ePassOutStage = POS_WAIT_PASS_OUT
			ENDIF
		BREAK
		CASE POS_WAIT_PASS_OUT
			// ----wait for player to pass out--------------------------------
			IF GET_GAME_TIMER() > iFailTimer
				IF (bPass = TRUE)
					
					// put this in a proc and call in fail too???
					//CLEANUP_ALL_ALIEN_PEDS(FALSE)
					SAFE_DELETE_OBJECT(gunObject)
					
					IF DOES_CAM_EXIST(cCamera)
						SET_CAM_ACTIVE(cCamera, FALSE)
						DESTROY_CAM(cCamera)
					ENDIF
					
					SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", 0)
					SET_TIME_SCALE(1.0)
					DEACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")
					SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE)
					SET_AUDIO_FLAG("AllowAmbientSpeechInSlowMo", FALSE)
					
					CLEAR_AREA_OF_VEHICLES(vMissionStart, 200.0)
					REMOVE_DECALS_IN_RANGE(vMissionStart, 500.0)
					
					CLEAR_AMBIENT_ZONE_STATE(sAmbientZone, TRUE)
					
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
						CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
					ENDIF	
				ENDIF
				
				
				CPRINTLN(DEBUG_MISSION, "Pass Out - Complete")
				RETURN TRUE // completed the pass out sequence
			ENDIF
		BREAK
	ENDSWITCH		
	
	RETURN FALSE // not completed the pass out sequence
ENDFUNC

/// PURPOSE: 
///  	Loads Mission Assets
/// PARAMS:
///    	loadWait - if this is true pause script until all assets are loaded
PROC REQUEST_MISSION_ASSETS()
	ADD_ASSET_REQUEST_FOR_WEAPON(assetRequester, 0, wtMiniGun)
	ADD_ASSET_REQUEST_FROM_STRING(assetRequester, 1, ASSET_AUDIOBANK, "SCRIPT\\BARRY_01_ALIEN_A")
	ADD_ASSET_REQUEST_FROM_STRING(assetRequester, 2, ASSET_AUDIOBANK, "SCRIPT\\BARRY_01_ALIEN_B")
	ADD_ASSET_REQUEST_FROM_STRING(assetRequester, 3, ASSET_AUDIOBANK, "SCRIPT\\BARRY_01_ALIEN_C")
	
	ADD_ASSET_REQUEST_FOR_MODEL(assetRequester, 4, alienModel)
	ADD_ASSET_REQUEST_FOR_MODEL(assetRequester, 5, beamDummyModel)
	
	ADD_ASSET_REQUEST_FOR_TEXT(assetRequester, 6, "BARY1", MISSION_TEXT_SLOT)

	ADD_ASSET_REQUEST_FOR_PTFX(assetRequester, 7)
	ADD_ASSET_REQUEST_FROM_STRING(assetRequester, 8, ASSET_ANIMDICT, sMissionAnimDictionary)
	// @SBA - adding new assests below here
	ADD_ASSET_REQUEST_FOR_MODEL(assetRequester, 9, mnProtestSign)
	//ADD_ASSET_REQUEST_FOR_WEAPON(assetRequester, 9, wtSniperRifle)
	//ADD_ASSET_REQUEST_FOR_WEAPON(assetRequester, 10, wtPistol)
	//ADD_ASSET_REQUEST_FOR_WEAPON(assetRequester, 11, wtGrenLauncher)
ENDPROC

/// PURPOSE: 
///  	Unloads all current mission assets - marks models as not needed etc
PROC UNLOAD_MISSION_ASSETS()
	UNLOAD_REQUESTED_ASSETS(assetRequester)
	//CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT, FALSE)
ENDPROC

/// PURPOSE: 
///  	Cleans mission
PROC MISSION_SETUP()
	// my mission setup - these things are only done once
	CPRINTLN(DEBUG_MISSION, " ") 
	CPRINTLN(DEBUG_MISSION, "BARRY 1 - VERSION:", MISSION_VERSION)

	// setup z skip menu
	#IF IS_DEBUG_BUILD
		sSkipMenu[Z_SKIP_INTRO].sTxtLabel = "INTRO"    
		sSkipMenu[Z_SKIP_BATTLE].sTxtLabel = "BATTLE"     
		sSkipMenu[Z_SKIP_BATTLE_2].sTxtLabel = "BATTLE P.2"     
		sSkipMenu[Z_SKIP_PASSOUT].sTxtLabel = "COLLAPSE"     
		sSkipMenu[Z_SKIP_OUTRO].sTxtLabel = "OUTRO"     
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	// turn off debug lines just in case i forget and shops
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)

	
	// stop police bothering me
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
	ENDIF
	
	ENABLE_ALL_DISPATCH_SERVICES(FALSE)
	DISABLE_CELLPHONE(TRUE)
	
	// set weather transition
	SET_WEATHER_TYPE_OVERTIME_PERSIST("extrasunny", 10.000)
	
	SET_ANGLED_AREA(missionBoundary, <<126.585876,-903.245178,87.535141>>, <<266.423584,-956.580627,27.452606>>, 230.000000)
	// @SBA - set a lager area to cover most of the battle zone
	//SET_ANGLED_AREA(respotVehicleArea, <<176.729996,-969.593384,37.092262>>, <<195.129456,-941.357605,27.191963>>, 21.500000)
	SET_ANGLED_AREA(respotVehicleArea, <<168.850021,-1001.023804,23.091291>>, <<225.904175,-862.596924,34.092327>>, 55.000000)

	// if you don't call this line summmoning will fail
	POPULATE_ALIEN_INDEX()
	RESET_MISSION_PARAMETERS()
	POPULATE_ALIEN_WALL_SPAWN_DATA()
	
	SPECIAL_ABILITY_LOCK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	SPECIAL_ABILITY_RESET(PLAYER_ID())
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, TRUE)
	DISABLE_TAXI_HAILING(TRUE)
	 
	IF IS_REPLAY_IN_PROGRESS()= TRUE // Set up the initial scene for replays
		CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS", MISSION_VERSION)
		g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_BARRY_1(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
		ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, TRUE)
		BLOCK_SCENARIOS_AND_AMBIENT(scenarioBlock)

		SET_AMBIENT_ZONE_STATE(sAmbientZone, TRUE)
		
		REQUEST_MISSION_ASSETS()
		WHILE NOT HAVE_ASSET_REQUESTS_LOADED(assetRequester)
			WAIT(0)
		ENDWHILE
		
		START_REPLAY_SETUP(vPlayerStartPos, kfPlayerStartHead, FALSE)
		
		CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_RESTART")
		TRIGGER_MUSIC_EVENT("RC18A_RESTART")
	
		stageEnum = SE_FIRSTCONTACT
		stageProgress = SP_LOADING
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			CPRINTLN(DEBUG_MISSION, "Please Stop T-Posing...")
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
				STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
			ENDIF
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
				STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
			ENDIF
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			// store weapons on replay
			CPRINTLN(DEBUG_MISSION, "REPLAY: STORING PLAYER'S WEAPONS")
			STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			CPRINTLN(DEBUG_MISSION, "Wait for it...")
		ENDIF

		// @SBA - Adding in replay stuff (put back in if we want it)
//		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
//
//		IF g_bShitskipAccepted = TRUE
//			iReplayStage++ // player is skipping this stage
//		ENDIF
//
//		SWITCH iReplayStage 
//			CASE CP_MISSION_START	
//				Do_Z_Skip(Z_SKIP_BATTLE)   						 // skip to the inital wave
//			BREAK
//			CASE CP_MID_STAGE
//				SETUP_MICHAEL_DIALOG()
//				Do_Z_Skip(Z_SKIP_BATTLE_2)    					// skip to part 2 of the battle
//				PRINT_NOW("PRIME2", DEFAULT_GOD_TEXT_TIME, 1) 	// Kill the aliens
//			BREAK
//			CASE CP_OUTRO
//				SAFE_FADE_SCREEN_OUT_TO_BLACK()
//				Do_Z_Skip(Z_SKIP_OUTRO)    	 					// skip to the end
//			BREAK
//			
//			DEFAULT
//				SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
//			BREAK
//		ENDSWITCH

		END_REPLAY_SETUP()
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()

		IF NOT ANIMPOSTFX_IS_RUNNING(FX_DrugsSkip)
			ANIMPOSTFX_PLAY(FX_DrugsSkip, 0, TRUE)
		ENDIF
		
		SET_TIME_SCALE(fTimeScale)
		ACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")
		SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", TRUE)
		SET_AUDIO_FLAG("AllowAmbientSpeechInSlowMo", TRUE)

	ENDIF
	
	ENABLE_ALIEN_BLOOD_VFX(TRUE)
	
	CPRINTLN(DEBUG_MISSION, "SETUP DONE")
ENDPROC


// PURPOSE: 
///  	Cleans mission
PROC MISSION_CLEANUP()
	IF IS_AUDIO_SCENE_ACTIVE(sndAlienAmbientScene)
         STOP_AUDIO_SCENE(sndAlienAmbientScene)
    ENDIF
	
	CLEAR_AMBIENT_ZONE_STATE(sAmbientZone, TRUE)
	
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		DELETE_PED(sRCLauncherDataLocal.pedID[0])
	ENDIF
	
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.0)
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(GET_PLAYER_INDEX(), 1.0)
	
	SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
	SPECIAL_ABILITY_UNLOCK(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
	DELETE_ALL_ALIEN_PEDS()
	
	ENABLE_ALIEN_BLOOD_VFX(FALSE)
	
	//SAFE_REMOVE_BLIP(locationBlip)
ENDPROC

/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC SCRIPT_CLEANUP()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Random Character Script was triggered so additional cleanup required")
		#ENDIF
	ENDIF

	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(GET_PLAYER_INDEX(), 1.0)
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(GET_PLAYER_INDEX(), 1.0)
		
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
			STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedAnim)
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
			STOP_ANIM_TASK(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedIntroAnim)
		ENDIF
	ENDIF	
	
	// @SBA - reset for minigun
	IF playerWeapon = wtMiniGun
	AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), playerWeapon)
		SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), FALSE, playerWeapon)
	ENDIF
	
	g_RestoreStoredWeaponsOnDeath = TRUE
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, FALSE)
	
	CLEANUP_SCREEN_FX()
	STOP_ALL_ALIEN_EVERYTHING()
	MISSION_CLEANUP()
	UNLOAD_REQUESTED_ASSETS(assetRequester)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0) 
 	DISABLE_TAXI_HAILING(FALSE)
	
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	// restore world based things
	SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", 0)
	
	SAFE_REMOVE_BLIP(locationBlip)
	
	CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
	//CLEAR_AREA_OF_VEHICLES(vMainLocation, 30.0)
	REMOVE_DECALS_IN_RANGE(vMissionStart, 500.0)
	SET_TIME_SCALE(1.0)
	DEACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO") 
	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE)
	SET_AUDIO_FLAG("AllowAmbientSpeechInSlowMo", FALSE)
	
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	CLEAR_ALL_PICKUP_REWARD_TYPE_SUPPRESSION()
	DISABLE_CELLPHONE(FALSE)
	
	ENABLE_AMBIENT_PEDS_AND_VEHICLES(TRUE, TRUE)
	UNBLOCK_SCENARIOS_AND_AMBIENT(scenarioBlock)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	MOVE_PLAYERS_VEHICLE()
	
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	CLEAR_AMBIENT_ZONE_STATE(sAmbientZone, TRUE)
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC
  

/// PURPOSE: 
///  	Handles Mission Passed
PROC SCRIPT_PASSED()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "Mission Passed")
	#ENDIF
	
	//FLEE_ALL_ALIEN_PEDS()
	// @SBA - reset for minigun
	IF playerWeapon = wtMiniGun
	AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), playerWeapon)
		SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), FALSE, playerWeapon)
	ENDIF
	
//	ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, MICHAEL_BOOK)
	
	// This needs to be done before random character passed as the replay values are reset
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_MISSION, "Barry1 passed: weapons restored")
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID(), TRUE)
		STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "Starting postRC_Barry1and2")
	START_NEW_SCRIPT("postRC_Barry1and2", DEFAULT_STACK_SIZE)
	SET_SCRIPT_AS_NO_LONGER_NEEDED("postRC_Barry1and2")

	Random_Character_Passed(CP_RAND_C_BAR1)
	SCRIPT_CLEANUP()
ENDPROC

//----------------------
//	STATE FUNCTIONS
//----------------------

FUNC BOOL HAS_LEADIN_ANIM_FINISHED(PED_INDEX ped, STRING str)
	IF NOT IS_ENTITY_PLAYING_ANIM(ped, sLeadInAnimDictionary, str)
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTITY_ANIM_FINISHED(ped, sLeadInAnimDictionary, str) 
		RETURN TRUE
	ENDIF	
	
	IF (GET_ENTITY_ANIM_CURRENT_TIME(ped, sLeadInAnimDictionary, str) >= 1.0)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///  	Updates Leading Sequence
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_LEADIN(STAGE_PROGRESS &prog)
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	HANDLE_LOCK_PLAYER_IN_FRONT_OF_TABLE()
	
	SWITCH (prog)
		CASE SP_LOADING
			REQUEST_MISSION_ASSETS()
			SECURE_REQUEST_AND_LOAD_ANIM_DICT(sLeadInAnimDictionary, FALSE)
			prog = SP_SETUP
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0.0, 0, 0.8>>), TRUE, -1, 3000)		
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.0)
				ELSE
					SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0.5, 0, 0.8>>), TRUE, -1, 3000)	
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.04)
				ENDIF
				
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.43)
				
				//SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(1.0)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.02)
                SET_GAMEPLAY_HINT_FOV(30.00)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
			ENDIF
			
//			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), (<<187.2756, -954.7941, 29.0919>>), PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 242.2959)
//				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.ped[0], 4000)
//			ENDIF
			
			/*
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				IF GET_PLANE_SIDE(GET_ENTITY_FORWARD_VECTOR(sRCLauncherDataLocal.pedID[0]), GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) = BEHIND_PLANE
					stageEnum = SE_INTRO
					prog = SP_LOADING
				ELSE
					SECURE_REQUEST_AND_LOAD_ANIM_DICT(sLeadInAnimDictionary, FALSE)
					CPRINTLN(DEBUG_MISSION, "LEAD IN - STARTED - PLAYER IS ON WRONG SIDE")
					prog = SP_SETUP
				ENDIF
			ENDIF
			*/
		BREAK
		CASE SP_SETUP	
			IF HAS_ANIM_DICT_LOADED(sLeadInAnimDictionary)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CPRINTLN(DEBUG_MISSION, "LEAD-IN ANIMS LOADED")
				structPedsForConversation sConversation  
				ADD_PED_FOR_DIALOGUE(sConversation, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(sConversation, 5, sRCLauncherDataLocal.pedID[0], "BARRY")
				
				IF CREATE_CONVERSATION(sConversation ,"BARY1AU", "BARY1_LEADIN", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
					TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[0], sLeadInAnimDictionary, "leadin", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME) 
					TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[0], PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					CPRINTLN(DEBUG_MISSION, "STARTED PLAYING LEAD-IN CONVERSATION")
					prog = SP_RUNNING
					iStateTimeOut = GET_GAME_TIMER() + 30000
				ENDIF
			ENDIF
		BREAK
		CASE SP_RUNNING		
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR (GET_GAME_TIMER() > iStateTimeOut )
					prog = SP_CLEANUP
				ENDIF
			ENDIF
		BREAK
		CASE SP_CSKIPPED
		BREAK
		CASE SP_CLEANUP	
		
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
				
			CPRINTLN(DEBUG_MISSION, "LEAD IN FINISHED")
			stageEnum = SE_INTRO
			prog = SP_LOADING
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///  	Updates Intro Sequence
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_INTRO(STAGE_PROGRESS &prog)

	SWITCH (prog)
		CASE SP_LOADING
			//DELETE_ALL_ALIEN_PEDS()
			CPRINTLN(DEBUG_MISSION, "REQUESTED CUTSCENE")
			RC_REQUEST_CUTSCENE("bar_1_rcm_p2")		
			stageProgress = SP_SETUP
			START_MISSION_DEBUG_TIMER()
			bHasPlayerCameraExitState = FALSE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				// store weapons
				CPRINTLN(DEBUG_MISSION, "STORING PLAYER'S WEAPONS")
				STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			ENDIF
		BREAK
		CASE SP_SETUP
			
			CPRINTLN(DEBUG_MISSION, "CHECKING TO SEE IF CUTSCENE IS OK TO START")
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()))
				ENDIF
				SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandleBarry, ANCHOR_EYES, 0)
				CPRINTLN(DEBUG_MISSION, "PED CUTSCENE VARIATIONS SET")
			ENDIF
	
			IF RC_IS_CUTSCENE_OK_TO_START()
				CPRINTLN(DEBUG_MISSION, "CUTSCENE IS OK TO START")
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[0], sSceneHandleBarry, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED BARRY")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[0], sSceneHandleBarry, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	GET_NPC_PED_MODEL(CHAR_BARRY))
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW BARRY")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_TABLE])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_TABLE], sSceneHandleTable, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED TABLE")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_TABLE], sSceneHandleTable, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_PROTEST_TABLE_01)
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW TABLE")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_CHAIR])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_CHAIR], sSceneHandleChair, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED CHAIR")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_CHAIR], sSceneHandleChair, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CHAIR_08)
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW CHAIR")
				ENDIF				
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_PAPERS])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_PAPERS], sSceneHandlePapers, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED PAPERS")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_PAPERS], sSceneHandlePapers, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_A4_Sheets_S)
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW PAPERS")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_LIGHTER])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_LIGHTER], sSceneHandleLighter, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED LIGHTER")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_LIGHTER], sSceneHandleLighter, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_CS_Lighter_01)
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW LIGHTER")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_JOINT])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_JOINT], sSceneHandleJoint, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED JOINT")
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[OBJ_JOINT], sSceneHandleJoint, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_CS_JOINT_01)
					CPRINTLN(DEBUG_MISSION, "REGISTERED NEW JOINT")
				ENDIF
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "REGISTERED MICHAEL")
				ENDIF
				
				// mini-gun
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), playerWeapon, INFINITE_AMMO, FALSE, FALSE)
				oiMichaelsWeapon = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), playerWeapon)
				REGISTER_ENTITY_FOR_CUTSCENE(oiMichaelsWeapon, sSceneHandleWeapon, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				// Cleanup launcher which will remove mission blip
				RC_CLEANUP_LAUNCHER()

				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				bHasPlayerCameraExitState = FALSE
				WAIT(0)
				
				// @SBA - assert fix
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_JOINT])
					IF IS_ENTITY_ATTACHED(sRCLauncherDataLocal.objID[OBJ_JOINT])
						DETACH_ENTITY(sRCLauncherDataLocal.objID[OBJ_JOINT])
					ENDIF
				ENDIF

				ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, FALSE)
				RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()	
				RC_START_CUTSCENE_MODE(vMissionStart, TRUE, FALSE, TRUE, TRUE, FALSE, TRUE, TRUE) 	
				BLOCK_SCENARIOS_AND_AMBIENT(scenarioBlock)
				
				CLEANUP_SCREEN_FX()
				
				bSkipAllowed = FALSE

				CREATE_STANDING_SIGN(sRCLauncherDataLocal.objID[OBJ_SIGN], mnProtestSign)
				
				SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
				REQUEST_MISSION_ASSETS()
				
				stageProgress = SP_RUNNING
			ENDIF				
		BREAK
		CASE SP_RUNNING	
			INT iCutTime 
			
			IF NOT IS_CUTSCENE_PLAYING()
				REPLAY_STOP_EVENT()
				RC_END_CUTSCENE_MODE(TRUE, FALSE)
				FORCE_PLAYER_WEAPON(playerWeapon)
				stageProgress = SP_CLEANUP
			ELSE

				iCutTime = GET_CUTSCENE_TIME()
				CPRINTLN(DEBUG_MISSION, "Cutscene Time = ", iCutTime)
				
				IF (bShowDebugMissionTime)
					SET_TEXT_SCALE(0.5, 0.5)
					DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBER", GET_CUTSCENE_TIME())
				ENDIF
				
				IF HAVE_ASSET_REQUESTS_LOADED(assetRequester)
					IF NOT bSkipAllowed
						SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
						bSkipAllowed = TRUE
					ENDIF
					// not sure this is really used - skipping seems built in
					IF (iCutTime < 68000)
						IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
							stageProgress = SP_CSKIPPED
							CPRINTLN(DEBUG_MISSION, "Cutscene Skipped - Assets Loaded")
							EXIT
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bHasPlayerCameraExitState
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					CPRINTLN(DEBUG_MISSION, "EXIT STATE FOR PLAYER SET")
					bHasPlayerCameraExitState = TRUE
				ENDIF
				
				IF IS_ENTITY_ALIVE(oiMichaelsWeapon)
					REQUEST_WEAPON_HIGH_DETAIL_MODEL(oiMichaelsWeapon)
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandleWeapon)
						GIVE_WEAPON_OBJECT_TO_PED(oiMichaelsWeapon, PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandlePapers)
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_PAPERS])
						CPRINTLN(DEBUG_MISSION, "EXIT STATE FOR PAPERS - DELETING")
						DELETE_OBJECT(sRCLauncherDataLocal.objID[OBJ_PAPERS])
					ENDIF
				ENDIF
				
				// B*750704 - Interactive Music Event 
				IF (bHasMusicStarted = FALSE)
					IF (iCutTime >= MUSICEVENT_RC18A_START_DELAY)					
						bHasMusicStarted = TRUE
						STRING FXname
						FXname = FX_DrugsIn
						IF WAS_CUTSCENE_SKIPPED()
							CPRINTLN(DEBUG_MISSION, "Starting screen effect (cs skipped)")
							FXname = FX_DrugsSkip
						ENDIF
						ANIMPOSTFX_PLAY(FXname, 0, TRUE)
						CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_START")
						TRIGGER_MUSIC_EVENT("RC18A_START")
					ENDIF
				ENDIF

				IF (bHasMusicIncreased = FALSE)
					IF (iCutTime >= MUSICEVENT_RC18A_INCREASE_DELAY)
						
						bHasMusicIncreased = TRUE
						CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_INCREASE")
						TRIGGER_MUSIC_EVENT("RC18A_INCREASE")
					ENDIF
				ENDIF
			
			ENDIF
		BREAK
		CASE SP_CSKIPPED
			
			SET_CUTSCENE_FADE_VALUES(false, false, FALSE, false)
			SAFE_FADE_SCREEN_OUT_TO_BLACK(250, FALSE)
			IF IS_CUTSCENE_PLAYING()
				WAIT_FOR_CUTSCENE_TO_STOP()
			ENDIF
			RC_END_CUTSCENE_MODE(TRUE, FALSE)
			
			// just in case the player skips the cutscene we still need to play the event
			IF (bHasMusicStarted = FALSE)
				bHasMusicStarted = TRUE
				CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_CS_SKIP_BEFORE - Cutscene Skip")
				TRIGGER_MUSIC_EVENT("RC18A_CS_SKIP_BEFORE")
				IF NOT ANIMPOSTFX_IS_RUNNING(FX_DrugsSkip)
					ANIMPOSTFX_PLAY(FX_DrugsSkip, 0, TRUE)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_CS_SKIP_AFTER - Cutscene Skip")
				TRIGGER_MUSIC_EVENT("RC18A_CS_SKIP_AFTER")	
				bHasMusicIncreased = TRUE
			ENDIF
			IF IS_ENTITY_ALIVE(oiMichaelsWeapon)
				SAFE_DELETE_OBJECT(oiMichaelsWeapon)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "Cutscene skipped - player's heading: ", kfPlayerStartHead)
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vMissionStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), kfPlayerStartHead)// @SBA OLD = fMissionStart
			FORCE_PLAYER_WEAPON(playerWeapon)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			WAIT(500)		
			
			stageProgress = SP_CLEANUP
		BREAK
		CASE SP_CLEANUP	
			stageProgress = SP_LOADING
			stageEnum = SE_FIRSTCONTACT

			IF WAS_CUTSCENE_SKIPPED() 		
				IF (bHasMusicStarted = FALSE)
					bHasMusicStarted = TRUE
					CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_CS_SKIP_BEFORE - Cutscene Skip")
					TRIGGER_MUSIC_EVENT("RC18A_CS_SKIP_BEFORE")
				ELIF (bHasMusicIncreased = FALSE)
					CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_CS_SKIP_AFTER - Cutscene Skip")
					TRIGGER_MUSIC_EVENT("RC18A_CS_SKIP_AFTER")	
					bHasMusicIncreased = TRUE
				ENDIF
			ENDIF

		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///  	Updates First Contact (Initial wave of 3 aliens)
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_FIRSTCONTACT(STAGE_PROGRESS &prog)
	INT ind 
		
	SWITCH (prog)
		CASE SP_LOADING	

			SET_TIME_SCALE(fTimeScale)
			DEACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")  // assert fix
			ACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")
			SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", TRUE)
			SET_AUDIO_FLAG("AllowAmbientSpeechInSlowMo", TRUE)
			
			IF NOT HAVE_ASSET_REQUESTS_LOADED(assetRequester)
				REQUEST_MISSION_ASSETS()
			ENDIF
			prog = SP_SETUP
			SETUP_MICHAEL_DIALOG()
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)
			RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE, FALSE)
			START_MISSION_DEBUG_TIMER()
			FORCE_PLAYER_WEAPON(playerWeapon)
			SET_AMBIENT_ZONE_STATE(sAmbientZone, TRUE)
			
		BREAK
		CASE SP_SETUP
			CLEAR_PRINTS()
			START_AUDIO_SCENE(sndAlienAmbientScene)
			CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
			
			// reset summon flags and counters
			iTotalAlienSpawnCount = 0
			iTotalAlienDeadCount = 0
			iAlienNextSummonTime = 0
			bSummonRushFlagActive = FALSE
			bSummonFlagActive = FALSE
			bAllowSummoning = FALSE
			iAlienSummonerIndex = -1
			iMaxActiveAliens = kiMaxStartingAliens
			fAlienMinSpawnRadius = 40.0
			fAlienMaxSpawnRadius = 60.0		
			
			// @SBA - updating these first aliens
			IF GET_FREE_ALIEN_PED_INDEX(ind)
				CREATE_ALIEN_PED(alienPed[ind], <<212.9178, -920.5014, 30.6922>>, 0, FALSE) //<<181.1, -951.9, 30.1>>
				alienPed[ind].fBeamTriggerRange = 15
				SET_ALIEN_WEAK(alienPed[ind])
			ENDIF
			
			IF GET_FREE_ALIEN_PED_INDEX(ind)
				CREATE_ALIEN_PED(alienPed[ind], <<213.2008, -917.9012, 30.6923>>, 2, FALSE) // <<173.0, -956.9, 30.1>>
				alienPed[ind].fBeamTriggerRange = 15
				SET_ALIEN_WEAK(alienPed[ind])
			ENDIF			
				
			IF GET_FREE_ALIEN_PED_INDEX(ind)
				CREATE_ALIEN_PED(alienPed[ind], <<217.8108, -916.8170, 30.6923>>, 4, FALSE) // <<174.7, -948.1, 30.1>>
				alienPed[ind].fBeamTriggerRange = 15
				SET_ALIEN_WEAK(alienPed[ind])
			ENDIF	
			// @SBA - adding some more to this wave
			IF GET_FREE_ALIEN_PED_INDEX(ind)
				CREATE_ALIEN_PED(alienPed[ind], <<211.0789, -912.2070, 30.6923>>, 8, FALSE) 
				alienPed[ind].fBeamTriggerRange = 15
				SET_ALIEN_WEAK(alienPed[ind])
			ENDIF

			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
			PRINT_NOW("PRIME2", DEFAULT_GOD_TEXT_TIME, 1) // Kill the Aliens
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID())
			
			// @SBA - spawn timer
			START_TIMER_NOW(tmr1stContactSpawn)
			f1stContactSpawnTime = 5.000
			prog = SP_RUNNING
		BREAK
		CASE SP_RUNNING
			
			PLAY_MICHAEL_INITIAL_LINE()
			
			SPAWN_NEXT_ALIEN_NEAR_LOCATION(v1stContactStartCtr, f1stContactStartRadius, 5, TRUE)
			
			// update kill chain
			IF (iKillChainEndTime <> 0)
			AND (GET_GAME_TIMER() > iKillChainEndTime)
				CPRINTLN(DEBUG_MISSION, "KILL CHAIN FAIL!!")
				iKillChainEndTime = 0
				iKillChain = 0
			ENDIF
			
			// @SBA - CHANGING BY REQUEST
//			IF (iAlienAliveCount = 0)
//			OR IS_PLAYER_OUT_OF_AMMO(PlayerWeapon)
			IF (iTotalAlienDeadCount >= 10)

				CPRINTLN(DEBUG_MISSION, "GOING TO FIRST CONTACT CLEANUP")
				// @SBA - CHANGING BY REQUEST
				//SWAP_THE_PLAYERS_WEAPON(PlayerWeapon)
				prog = SP_CLEANUP
			ENDIF
		BREAK
		CASE SP_CLEANUP
			prog = SP_LOADING
			stageEnum = SE_WAVE1
			
			//prog = SP_LOADING
			//stageEnum = SE_PASSOUT
		BREAK
	ENDSWITCH
ENDPROC
 
/// PURPOSE: 
///  	Updates first wave
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_WAVE_1(STAGE_PROGRESS &prog)

	SWITCH (prog)
		CASE SP_LOADING	
			CPRINTLN(DEBUG_MISSION, "LOADING FIRST WAVE")
			START_TIMER_NOW(tmrStageTimer)
			prog = SP_SETUP
		BREAK
		CASE SP_SETUP
			// @SBA - give a little space before new aliens start spawning in
			IF TIMER_DO_ONCE_WHEN_READY(tmrStageTimer, 5.000)
				CPRINTLN(DEBUG_MISSION, "FIRST WAVE TIMER IS UP")
				bAutoCreateAliens = TRUE
				bAllowSummoning = TRUE
				iMaxActiveAliens = kiMaxStartingAliens
				iAlienAutoGenTime = 0
				prog = SP_RUNNING
			ENDIF
		BREAK
		CASE SP_RUNNING
		
			// update kill chain
			IF (iKillChainEndTime <> 0)
			AND (GET_GAME_TIMER() > iKillChainEndTime)
				CPRINTLN(DEBUG_MISSION, "KILL CHAIN FAIL!!")
				iKillChainEndTime = 0
				iKillChain = 0
			ENDIF
			
			// drugs effect and other thing
			IF IS_AUDIO_SCENE_ACTIVE(sndAlienAmbientScene)
				SET_AUDIO_SCENE_VARIABLE(sndAlienAmbientScene, "DrugsEffect", (SIN(((TO_FLOAT(GET_GAME_TIMER())/1000)*45)%360))*0.25+0.25)
			ENDIF
			
			// @SBA - advance through the weapons we want the player to use
			IF IS_PLAYER_OUT_OF_AMMO(PlayerWeapon)
				SWAP_THE_PLAYERS_WEAPON(PlayerWeapon)
			ENDIF
			
			// auto create aliens as supplies run low
			HANDLE_AUTO_ALIEN_GENERATION()
			
			PLAY_MICHAEL_FREAK_DIALOG_LINES()
			//SET_TIME_SCALE(fTimeScale)
			
			// @SBA - conditions to end wave 1
			IF (iTotalAlienDeadCount >= iTotalAlienDeadRequired - kiAliensLeftToKill)
				bAutoCreateAliens = FALSE
				CPRINTLN(DEBUG_MISSION, "GOING TO FIRST WAVE CLEANUP")
				prog = SP_CLEANUP
			ENDIF
		BREAK
		CASE SP_CLEANUP
			prog = SP_LOADING
			stageEnum = SE_WAVE2
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///  	Updates first wave
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_WAVE_2(STAGE_PROGRESS &prog)

	SWITCH (prog)
		CASE SP_LOADING	
		CASE SP_SETUP
			CPRINTLN(DEBUG_MISSION, "LOADING SECOND WAVE")
			bAutoCreateAliens = TRUE
			prog = SP_RUNNING
		BREAK
		CASE SP_RUNNING
		
			// update kill chain
			IF (iKillChainEndTime <> 0)
			AND (GET_GAME_TIMER() > iKillChainEndTime)
				CPRINTLN(DEBUG_MISSION, "KILL CHAIN FAIL!!")
				iKillChainEndTime = 0
				iKillChain = 0
			ENDIF
			
			// drugs effect and other thing
			IF IS_AUDIO_SCENE_ACTIVE(sndAlienAmbientScene)
				SET_AUDIO_SCENE_VARIABLE(sndAlienAmbientScene, "DrugsEffect", (SIN(((TO_FLOAT(GET_GAME_TIMER())/1000)*45)%360))*0.25+0.25)
			ENDIF
			
			// @SBA - advance through the weapons we want the player to use
			// Note Mini-gun (last weapon) gets infinite ammo
			IF IS_PLAYER_OUT_OF_AMMO(PlayerWeapon)
				SWAP_THE_PLAYERS_WEAPON(PlayerWeapon)
				IF PlayerWeapon = wtMiniGun
				AND iTotalAlienDeadCount >= iTotalAlienDeadRequired
					CPRINTLN(DEBUG_MISSION, "UPDATE_STAGE_WAVE_2: Player just got minigun, adding ",iAliensToAdd, " more aliens to the required kill total." )
					iTotalAlienDeadRequired += iAliensToAdd
				ENDIF
			ENDIF
			
			// auto create aliens as supplies run low
			HANDLE_AUTO_ALIEN_GENERATION()
			
			PLAY_MICHAEL_FREAK_DIALOG_LINES()
			//SET_TIME_SCALE(fTimeScale)
			
			// @SBA - make sure the player has gotten the mini-gun first
			IF PlayerWeapon = wtMiniGun
				// then check dead count
				IF (iTotalAlienDeadCount > iTotalAlienDeadRequired)
					bAutoCreateAliens = FALSE
					IF (iAlienAliveCount = 0) 
						CPRINTLN(DEBUG_MISSION, "GOING TO FIRST WAVE CLEANUP")
						prog = SP_CLEANUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SP_CLEANUP
			prog = SP_LOADING
			stageEnum = SE_PASSOUT
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: 
///  	Updates The Pass Out Sequence
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_PASSOUT(STAGE_PROGRESS &prog)
	SWITCH (prog)
		CASE SP_LOADING
		CASE SP_SETUP
			//TELEPORT_OUT_ALL_ALIEN_PEDS()
			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
			CPRINTLN(DEBUG_MISSION, "ENTERING UPDATE STAGE PASSOUT")
			CLEAR_PRINTS()
			CLEAR_HELP()
			prog = SP_RUNNING
		BREAK
		CASE SP_RUNNING
			IF PASS_OUT_SEQUENCE(TRUE)
				prog = SP_CLEANUP		
			ENDIF
		BREAK
		CASE SP_CLEANUP	
			prog = SP_LOADING
			stageEnum = SE_OUTRO
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///  	Updates Outro
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_OUTRO(STAGE_PROGRESS &prog)
		
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()))
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		ENDIF
		SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandleBarry, ANCHOR_EYES, 0)
		CPRINTLN(DEBUG_MISSION, "PED CUTSCENE VARIATIONS SET")
	ENDIF
	
	SWITCH (prog)
		CASE SP_LOADING
			CPRINTLN(DEBUG_MISSION, "REQUESTING CUTSCENE - bar_2_rcm")	
			RC_REQUEST_CUTSCENE("bar_2_rcm")
			WAIT_FOR_WORLD_TO_LOAD(vMissionStart)
			IF NOT HAS_SCRIPT_LOADED("postRC_Barry1and2")
				REQUEST_SCRIPT("postRC_Barry1and2")
			ENDIF
			prog = SP_SETUP
		BREAK
		CASE SP_SETUP
			
			IF RC_IS_CUTSCENE_OK_TO_START()
			AND HAS_SCRIPT_LOADED("postRC_Barry1and2")
				bHasPlayerCameraExitState = FALSE
				
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				ENDIF
				
				MISSION_CLEANUP()
				
				CLEAR_AREA_OF_VEHICLES(vMissionStart, 250.0)
				CLEAR_AREA_OF_PEDS(vMissionStart, 50.0)
				CLEAR_AREA_OF_OBJECTS(vMissionStart, 30.0)
				REMOVE_DECALS_IN_RANGE(vMissionStart, 500.0)
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				MOVE_PLAYERS_VEHICLE()

				REGISTER_ENTITY_FOR_CUTSCENE(NULL, sSceneHandleBarry, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_BARRY))
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, sSceneHandleTable, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_PROTEST_TABLE_01)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, sSceneHandleChair, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CHAIR_08)
	
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
					CPRINTLN(DEBUG_MISSION, "REGISTERED MICHAEL")
				ENDIF
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				START_CUTSCENE()
				WAIT(0)
				
				SAFE_FADE_SCREEN_IN_FROM_BLACK(1000, FALSE)
//				RESOLVE_MISSION_VEHICLES_FOR_CUTSCENE()	
				RC_START_CUTSCENE_MODE(<<0, 0, 0>>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE) 
				
				CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
				TRIGGER_MUSIC_EVENT("RC18A_STOP")
				
				//SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				prog = SP_RUNNING
			ENDIF				
		BREAK
		CASE SP_RUNNING
		
			// Don't look back
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				CPRINTLN(DEBUG_MISSION, "PRE RC_END_CUTSCENE_MODE")
				RC_END_CUTSCENE_MODE()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				REPLAY_STOP_EVENT()
				CPRINTLN(DEBUG_MISSION, "POST RC_END_CUTSCENE_MODE")
				prog = SP_CLEANUP
			ELSE
				IF NOT bHasPlayerCameraExitState
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				
				IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_SIGN])
					CREATE_STANDING_SIGN(sRCLauncherDataLocal.objID[OBJ_SIGN], mnProtestSign)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					CPRINTLN(DEBUG_MISSION, "SET EXIT STATE FOR MICHAEL")
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(player_id(), pedmove_walk, 2000)
					bHasPlayerCameraExitState = TRUE
				ENDIF
	
				// register things as we exit
				IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleBarry))
						sRCLauncherDataLocal.pedID[0] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleBarry))
						SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[0], ANCHOR_EYES, 0) //Glasses prop.
					ENDIF
				ENDIF
			
				IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_TABLE])
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleTable))
						sRCLauncherDataLocal.objID[OBJ_TABLE] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleTable))
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_CHAIR])
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleChair))
						sRCLauncherDataLocal.objID[OBJ_CHAIR] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandleChair))
					ENDIF
				ENDIF
				
				IF CAN_SET_ENTER_STATE_FOR_REGISTERED_ENTITY(sSceneHandleTable)
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_TABLE]) AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[OBJ_PAPERS])
						ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.objID[OBJ_PAPERS], sRCLauncherDataLocal.objID[OBJ_TABLE], 0, <<0, -0.25, 0>>, <<0, 0, 0>>)
						CPRINTLN(DEBUG_MISSION, "PAPERS REATTACHED TO TABLE")
					ENDIF
				ENDIF
				
				/*
				SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					stageProgress = SP_CSKIPPED
				ENDIF
				*/
			ENDIF
		BREAK
		CASE SP_CSKIPPED
			SET_CUTSCENE_FADE_VALUES(false, false, FALSE, false)
			SAFE_FADE_SCREEN_OUT_TO_BLACK(250, FALSE)
			IF IS_CUTSCENE_PLAYING()
				WAIT_FOR_CUTSCENE_TO_STOP()		
			ENDIF
			RC_END_CUTSCENE_MODE()
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vMissionStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fMissionStart)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			WAIT(250)	
			SAFE_FADE_SCREEN_IN_FROM_BLACK(250, FALSE)
			SCRIPT_PASSED()
		BREAK
		CASE SP_CLEANUP
			CPRINTLN(DEBUG_MISSION, "SP_CLEANUP")
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			SCRIPT_PASSED()
		BREAK
	ENDSWITCH
ENDPROC

// @SBA - Quick and dirty prototype of pulling camera into the sky and dropping Michael from there
CAMERA_INDEX ciDropCam
SELECTOR_CAM_STRUCT scsCamDetails
INT iTest = 0
INT iTimeFadeOut
VECTOR vSkyCam = <<184.8, -961.5, 200.0>>
VECTOR vSkyCamRot = <<-90, 0.0, -109.6>>

FUNC BOOL OPERATION_DROP_MICHAEL()
	
	SWITCH iTest
		// set up
		CASE 0
			ciDropCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vSkyCam, vSkyCamRot, 50.0)
			scsCamDetails.camSky = ciDropCam
			iTimeFadeOut = GET_GAME_TIMER() + 400
			iTest ++
		BREAK
		// sky cam and fade
		CASE 1
			IF GET_GAME_TIMER() > iTimeFadeOut
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			IF NOT RUN_CAM_SPLINE_FROM_PLAYER_TO_SKY_CAM(scsCamDetails, 0.8)
				iTest ++
			ENDIF
		BREAK
		// start black screen timer
		CASE 2
			IF IS_SCREEN_FADED_OUT()
				// reusing timer
				iTimeFadeOut = GET_GAME_TIMER() + 500
				iTest ++
			ENDIF
		BREAK
		// Drop player
		CASE 3
			IF GET_GAME_TIMER() > iTimeFadeOut
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vSkyCam)
				//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
					IF DOES_CAM_EXIST(ciDropCam)
						SET_CAM_ROT(ciDropCam, vSkyCamRot)
						SET_CAM_ACTIVE(ciDropCam, TRUE)
						ATTACH_CAM_TO_ENTITY(ciDropCam, PLAYER_PED_ID(), <<0,0,4>>, FALSE)
					ENDIF
				ELSE
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
				
				DELETE_ALL_ALIEN_PEDS()
				iTest ++
			ENDIF
		BREAK
		// fade in
		CASE 4
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			sFailString = "B1_FAIL3"
			iTest ++
		BREAK
		// return camera near impact
		CASE 5
			VECTOR vPlayerCoord
			vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF vPlayerCoord.z < 32.0  // be sure to alter this according to ground height
				RENDER_SCRIPT_CAMS(FALSE, TRUE)
				iTest ++
			ENDIF
		BREAK
		// clean up
		CASE 6
			IF IS_GAMEPLAY_CAM_RENDERING()
				IF DOES_CAM_EXIST(ciDropCam)
					SET_CAM_ACTIVE(ciDropCam, FALSE)
					DESTROY_CAM(ciDropCam)
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///  	Updates failed stage
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_ABDUCT(STAGE_PROGRESS &prog)
	SEQUENCE_INDEX seqTask 
	
	SWITCH (prog)
		CASE SP_LOADING
		CASE SP_SETUP
			CPRINTLN(DEBUG_MISSION, "Player Abducted")
			iTest = 0
	
			//RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, 0.0, FALSE, FALSE)
			gunObject = CREATE_FAKE_WEAPON_FOR_PLAYER(playerWeapon)
			IF IS_ENTITY_OK(gunObject) AND IS_ENTITY_ATTACHED(gunObject)
				DETACH_ENTITY(gunObject)
			ENDIF
			
			OPEN_SEQUENCE_TASK(seqTask)
				TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sAlienGrabbedWinAnim)
				TASK_PLAY_ANIM(NULL, sMissionAnimDictionary, sMichaelTeleportAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			CLOSE_SEQUENCE_TASK(seqTask)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqTask)
			CLEAR_SEQUENCE_TASK(seqTask)		
			
			prog = SP_RUNNING
		BREAK
		CASE SP_RUNNING 
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_alien_teleport", GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_ROTATION(PLAYER_PED_ID()), fAlienTeleportEffectSize)
				PLAY_SOUND_FROM_ENTITY(-1, "SPAWN", PLAYER_PED_ID(), "BARRY_01_SOUNDSET")
				fPlayerAlpha = 255.0
				prog = SP_CLEANUP
			ENDIF
		BREAK
		CASE SP_CLEANUP
			fPlayerAlpha -= (GET_FRAME_TIME() * 512.0)
			CPRINTLN(DEBUG_MISSION, "PLAYER GETTING TELEPORTED OUT - ALPHA:", fPlayerAlpha)
			IF (fPlayerAlpha <= 0.0)
				fPlayerAlpha = 0.0
				RESET_ENTITY_ALPHA(PLAYER_PED_ID())
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				prog = SP_CSKIPPED
				EXIT
			ENDIF
			
			SET_PED_ALPHA(PLAYER_PED_ID(), FLOOR(fPlayerAlpha), iAlphaCounter)
		BREAK
		// @SBA - prototyping case
		CASE SP_CSKIPPED
			IF OPERATION_DROP_MICHAEL()
				SET_MISSION_FAILED("B1_FAIL3")
				iFailTimer = GET_GAME_TIMER() + 50
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
			
/// PURPOSE: 
///  	Updates failed stage
/// PARAMS:
///    	prog - stage progress enum, tells us what state we are in
PROC UPDATE_STAGE_FAILED(STAGE_PROGRESS &prog)
	SWITCH (prog)
		CASE SP_LOADING
		CASE SP_SETUP
			CPRINTLN(DEBUG_MISSION, "Mission Failed")
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			// Remove all blips here
			
			// Make all aliens flee
			STOP_ALL_ALIEN_EVERYTHING()
			FLEE_ALL_ALIEN_PEDS()
			CLEANUP_ALL_ALIEN_PEDS(TRUE)
			
			// stop mission music
			CPRINTLN(DEBUG_MISSION, "FAIL STRING = ", sFailString)
			CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
			TRIGGER_MUSIC_EVENT("RC18A_STOP")
			
			// B*938802 - No more delay fades
			IF IS_STRING_NULL_OR_EMPTY(sFailString)
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailString, TRUE) ///, bDelayFade)
			ENDIF
			prog = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Set warp locations
				MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<165.4821, -990.5140, 29.0923>>, 166.7554)
  				SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(vPlayerVehicleLoc, fPlayerVehicleHead)
			
				// delete everything here!
				//DeleteEverything()?
				//CLEANUP_ALL_ALIEN_PEDS(TRUE)?
				
				// script_cleanup should terminate the thread
				RC_END_CUTSCENE_MODE(TRUE, FALSE)
				
				CPRINTLN(DEBUG_MISSION, "Barry1 fail stage: Restore weapons on fail")
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID(), TRUE)
					STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
				ENDIF
				Script_Cleanup()
				
			ELSE

				// not finished fading out
				// you may want to handle dialogue etc here.
				IF ARE_STRINGS_EQUAL(sFailString, "B1_FAIL") // Fled the battle
					IF PASS_OUT_SEQUENCE() AND IS_REPEAT_PLAY_ACTIVE()
						CPRINTLN(DEBUG_MISSION, "Repeat Play - Emergency Exit")
						Script_Cleanup()
					ENDIF
				ELIF ARE_STRINGS_EQUAL(sFailString, "B1_FAIL2") // Abducted
					ABDUCT_SEQUENCE_END()
				ENDIF
			ENDIF			
		BREAK
	ENDSWITCH
ENDPROC

//----------------------
//	CHECKPNT FUNCTIONS
//----------------------

/// PURPOSE: 
///  	Performs Z Skip
/// PARAMS:
///    	iTargetStage - which stage to jump to
PROC DO_Z_SKIP(INT iTargetStage)
	DO_SCREEN_FADE_OUT(0)
	RC_START_Z_SKIP()
	SWITCH iTargetStage
		CASE Z_SKIP_INTRO
			CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
			TRIGGER_MUSIC_EVENT("RC18A_STOP")
			CPRINTLN(DEBUG_MISSION, "Skip to intro - player heading set to: ", kfPlayerStartHead)
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vMissionStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), kfPlayerStartHead) // @SBA OLD = fMissionStart
			
			iDbgStageJump = 0
		BREAK
		CASE Z_SKIP_BATTLE
			RESET_MISSION_COUNTERS()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "Z-Skip or Replay: Start Music")
			#ENDIF

			CPRINTLN(DEBUG_MISSION, "Skip to battle - player heading set to: ", kfPlayerStartHead)
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vMissionStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), kfPlayerStartHead) // @SBA OLD = fMissionStart
			iDbgStageJump = ENUM_TO_INT(SE_FIRSTCONTACT)
		BREAK
		// @SBA - adding case (will need to update weapon, dead count, etc. if ever used for a checkpoint!)
		CASE Z_SKIP_BATTLE_2
			RESET_MISSION_COUNTERS()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "Z-Skip or Replay: Battle2")
			#ENDIF
			// Set number of dead aliens
			iTotalAlienDeadCount = kiDeadAliensStartingWave2
			PlayerWeapon = wtGrenLauncher//wtPistol // @TODO - should be wtGrenLauncher, to swap to mini gaun
			SWAP_THE_PLAYERS_WEAPON(PlayerWeapon)
			SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vMissionStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fMissionStart)
			iDbgStageJump = ENUM_TO_INT(SE_WAVE2)
		BREAK		
		CASE Z_SKIP_PASSOUT
			iDbgStageJump = ENUM_TO_INT(SE_PASSOUT)
		BREAK	
		CASE Z_SKIP_OUTRO
			iDbgStageJump = ENUM_TO_INT(SE_OUTRO)
		BREAK	
	ENDSWITCH
	
	// delete and unload everything
	stageEnum = INT_TO_ENUM(STAGE_ENUM, iDbgStageJump) 
	stageProgress = SP_LOADING
	
	DELETE_ALL_ALIEN_PEDS()
	ENABLE_AMBIENT_PEDS_AND_VEHICLES(FALSE, FALSE, TRUE, TRUE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
	SET_TIME_SCALE(fTimeScale)
	DEACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")
	ACTIVATE_AUDIO_SLOWMO_MODE("BARRY_01_SLOWMO")
	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", TRUE)
	SET_AUDIO_FLAG("AllowAmbientSpeechInSlowMo", TRUE)
	RC_END_Z_SKIP()
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

/// PURPOSE: 
///  	Operates S, F, J and Z keys
PROC UPDATE_DEBUG_KEYS()
	UPDATE_MISSION_DEBUG_TIMER()
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		IF IS_CUTSCENE_PLAYING()
			WAIT_FOR_CUTSCENE_TO_STOP(TRUE, FALSE)
		ENDIF
		WHILE NOT HAS_SCRIPT_LOADED("postRC_Barry1and2")
			REQUEST_SCRIPT("postRC_Barry1and2")
			WAIT(0)
		ENDWHILE
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT()
		ENDIF
		CLEANUP_SCREEN_FX()
		CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
		TRIGGER_MUSIC_EVENT("RC18A_STOP")
		TELEPORT_OUT_ALL_ALIEN_PEDS(TRUE)
		SCRIPT_PASSED()
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		IF IS_CUTSCENE_PLAYING()
			WAIT_FOR_CUTSCENE_TO_STOP(TRUE, FALSE)
		ENDIF
		SET_MISSION_FAILED("")
	ENDIF
	
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		IF IS_CUTSCENE_PLAYING()
			WAIT_FOR_CUTSCENE_TO_STOP(TRUE, FALSE)
		ENDIF
		
		IF (stageEnum = SE_INTRO) OR (stageEnum = SE_FIRSTCONTACT)
			DO_Z_SKIP(Z_SKIP_INTRO)
		ELSE
			DO_Z_SKIP(Z_SKIP_BATTLE)
		ENDIF
	ENDIF

	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		IF IS_CUTSCENE_PLAYING()
			WAIT_FOR_CUTSCENE_TO_STOP(TRUE, FALSE)
		ENDIF
		
		IF (stageEnum = SE_INTRO)
			DO_Z_SKIP(Z_SKIP_BATTLE)
		ELIF (stageEnum = SE_OUTRO)
			CLEANUP_SCREEN_FX()
			CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
			TRIGGER_MUSIC_EVENT("RC18A_STOP")
			SCRIPT_PASSED()
		ELSE
			DO_Z_SKIP(Z_SKIP_OUTRO)
		ENDIF
	ENDIF
	
	IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iDbgStageJump)
		DO_Z_SKIP(iDbgStageJump)
	ENDIF
ENDPROC

/// PURPOSE:
///    		Updates Debug Widgets and draw debug stuff
PROC UPDATE_DEBUG_WIDGETS()
	INT i 
	VECTOR v = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	DEBUG_DRAW_ANGLED_AREA_EX(respotVehicleArea, 255, 128, 0)
	
	IF (bUseAlienSpawnPoint)
		DRAW_DEBUG_CIRCLE(vAlienSpawnPoint, fAlienSpawnPointRadius, 255, 0, 0, 255, 32)
	ELSE
		DRAW_DEBUG_CIRCLE(v, fAlienMinSpawnRadius, 255, 0, 0, 255, 32)
		DRAW_DEBUG_CIRCLE(v, fAlienMaxSpawnRadius, 0, 255, 0, 255, 32)
	ENDIF
			
	IF (bShowAlienPos)
		REPEAT COUNT_OF(alienPed) i 
			IF IS_ALIEN_PED_ALIVE(alienPed[i])
				DRAW_DEBUG_LINE(v, GET_ENTITY_COORDS(alienPed[i].pedID))
			ENDIF
		ENDREPEAT
	ENDIF
	
	DRAW_DEBUG_CIRCLE(vBattleCenter, fBattleRadius)
ENDPROC

#ENDIF	

/// PURPOSE: 
///  	Check to see if player has left battle area if so handle failure
PROC UPDATE_PLAYER_AREA_CHECK()
	BOOL chk = (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vBattleCenter) < (fBattleRadius + 10.0))
	
	IF (bIsPlayerInBattleArea) 
		IF (chk = FALSE) AND (bSummonFlagActive = FALSE)
			PRINT_NOW("B1_WARN", DEFAULT_GOD_TEXT_TIME, 1)	// Return to the ~y~battleground.~s~
			SAFE_REMOVE_BLIP(locationBlip)
			locationBlip = CREATE_COORD_BLIP(vBattleCenter)
			iMissionAbandonedTimer = GET_GAME_TIMER() + FAIL_WARNING_TIME
			TELEPORT_OUT_ALL_ALIEN_PEDS()
			bIsPlayerInBattleArea = FALSE
		ENDIF
	ELSE
		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vBattleCenter) > fBattleRadius)
			IF (GET_GAME_TIMER() > iMissionAbandonedTimer) AND (bSummonFlagActive = FALSE)
				SET_MISSION_FAILED("B1_FAIL") // ~r~You fled the battle. The aliens took over.~s~
			ENDIF
		ELSE
			PRINT_NOW("PRIME2", DEFAULT_GOD_TEXT_TIME, 1)	// Kill the ~r~aliens.~s~
			SAFE_REMOVE_BLIP(locationBlip)
			bIsPlayerInBattleArea = TRUE
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	VECTOR amin, amax
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	sFailString = ""
	
	// clean up any live projectiles near Barry
	CLEAR_AREA_OF_PROJECTILES((<<190.2424, -956.3790, 28.63>>), 6.0)
	
	//BREAK_ON_NATIVE_COMMAND("CLEAR_AREA_OF_OBJECTS", FALSE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_MISSION, "IM Trigger - RC18A_STOP")
		TRIGGER_MUSIC_EVENT("RC18A_STOP")
		CPRINTLN(DEBUG_MISSION, "Barry1 Force Cleanup: Restore weapons on fail")
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PLAYER_PED_ID())
			STORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
		ELSE
			RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)
		ENDIF
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		// abduction
		IF ARE_STRINGS_EQUAL(sFailString, "B1_FAIL3")
			Random_Character_Failed_With_Reason("B1_FAIL3")
		// death (custom text)
		ELIF IS_PLAYER_DEAD(PLAYER_ID())
			Random_Character_Failed_With_Reason("B1_FAIL2")
		ELSE
			Random_Character_Failed()
		ENDIF
		SCRIPT_CLEANUP()
	ENDIF

	// setup areas
	MISSION_SETUP()
	
	// @SBA
	SECURE_REQUEST_AND_LOAD_WEAPON_ASSET(wtMiniGun, TRUE)
	//SECURE_REQUEST_AND_LOAD_WEAPON_ASSET(wtSniperRifle, TRUE)
	CALCULATE_MIN_MAX_FROM_POSITION_AND_RADIUS(<<181.3, -969.1, 30.3>>, 200.0, amin, amax)
	DUMMY_REFERENCE_INT(iTotalAlienSpawnCount)
	
	IF IS_REPEAT_PLAY_ACTIVE()
		if not is_screen_faded_in()
			if not is_screen_fading_in()
				do_screen_fade_in(default_fade_time)
			endif 
		endif
	ENDIF
		
	// main loop
	WHILE (TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_GRM")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
	
		// globals
		fRegenMultiplier = 1.0
		iAlphaCounter = 0
		NO_AMBIENT_MALARKY_THIS_FRAME()
		
		// for beam FX evo
//		vCameraForward = GET_FINAL_RENDERED_CAM_ROT()
//		vCameraCoords = GET_FINAL_RENDERED_CAM_COORD()
//		fCameraNearClip = GET_FINAL_RENDERED_CAM_NEAR_CLIP()
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		SET_PED_NON_CREATION_AREA(amin, amax)
		SET_PLAYER_MAY_NOT_ENTER_ANY_VEHICLE(GET_PLAYER_INDEX())

		// update aliens
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		UPDATE_ALIEN_PEDS()
			
		/* B*1182105
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sMissionAnimDictionary, sAlienGrabbedFailAnim)
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
		ENDIF
		*/
		
		// main switch
		SWITCH (stageEnum)
			CASE SE_LEAD_IN
				UPDATE_STAGE_LEADIN(stageProgress)
			BREAK
			CASE SE_INTRO
				UPDATE_STAGE_INTRO(stageProgress)
			BREAK
			CASE SE_FIRSTCONTACT
				UPDATE_STAGE_FIRSTCONTACT(stageProgress)
			BREAK
			CASE SE_WAVE1
				UPDATE_STAGE_WAVE_1(stageProgress)
			BREAK
			CASE SE_WAVE2
				UPDATE_STAGE_WAVE_2(stageProgress)
			BREAK
			CASE SE_PASSOUT
				UPDATE_STAGE_PASSOUT(stageProgress)
			BREAK
			CASE SE_OUTRO
				UPDATE_STAGE_OUTRO(stageProgress)
			BREAK
			CASE SE_ABDUCT
				UPDATE_STAGE_ABDUCT(stageProgress)
			BREAK
			CASE SE_FAILED
				UPDATE_STAGE_FAILED(stageProgress)
			BREAK
		ENDSWITCH

		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(GET_PLAYER_INDEX(), fRegenMultiplier)
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(GET_PLAYER_INDEX(), fPlayerWeaponDamageModifier)
		
		IF IS_MISSION_IN_COMBAT_STAGE()
			UPDATE_PLAYER_AREA_CHECK()
		ENDIF
		
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
			IF (stageEnum <> SE_FAILED)
				UPDATE_DEBUG_KEYS()
			ENDIF
			
			IF (bUpdateWidgets)
				UPDATE_DEBUG_WIDGETS()
			ENDIF
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
