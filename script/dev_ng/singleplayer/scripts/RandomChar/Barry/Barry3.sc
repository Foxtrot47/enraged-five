
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "replay_public.sch"
USING "RC_Threat_public.sch"
USING "initial_scenes_barry.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Barry3.sc
//		AUTHOR			:	Joanna Wright
//		DESCRIPTION		:	Franklin meets Barry in the street
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM MISSION_STAGE
	MS_INIT 				= 0,
	MS_LEADIN 				= -1,
	MS_LEADOUT 				= -2,
	MS_MEET_BARRY 			= 1,
	MS_LEAVE_AREA 			= 2,
	MS_LOSE_WANTED_LEVEL 	= 3,
	MS_MISSION_PASSED 		= 4,
	MS_FAIL 				= 5
ENDENUM

ENUM STAGE_PROGRESS
	SP_SETUP,
	SP_RUNNING,
	SP_CLEANUP
ENDENUM

STRING sFailReason = "DEFAULT"

CONST_INT BARRY				0
CONST_INT COPS_DELAY		40000
CONST_INT BARRY_ZONE		100
CONST_INT BARRY_TALK_ZONE	15
CONST_INT NOTICE_PLAYER_ZONE 40 //Barry will notice the Player get in a cop car within this distance
//checkpoints
CONST_INT CP_AFTER_MOCAP 	0	

// Mission state
MISSION_STAGE mStage = MS_INIT
STAGE_PROGRESS sProgress = SP_SETUP

STRING animDicBarry = "rcm_barry3"
SEQUENCE_INDEX barryRunSeq

// Ped index for local Barry
PED_INDEX piBarry
VEHICLE_INDEX copVehicle
PED_INDEX copPed[2]
OBJECT_INDEX objJoint
MODEL_NAMES modelCopVeh = POLICE
MODEL_NAMES modelCop    = S_F_Y_COP_01
MODEL_NAMES modelJoint  = PROP_SH_JOINT_01

INT iCopsArriveTimer 		 	  = 0
BOOL bCopsArrived 			 	  = FALSE
BOOL bBarrySeePlayerInCopCar 	  = FALSE
BOOL bBarryScaredByPlayerInCopCar = FALSE
BOOL bCopCarCheck 				  = TRUE //whether script has to check if Player in cop car or not

BOOL bStartedPush = FALSE

CONST_INT NUM_CONVS  4
CONST_INT CONV_WARN1 0
CONST_INT CONV_WARN2 1
CONST_INT CONV_WARN3 2
//any other warns go in here
CONST_INT CONV_COPS  3
INT iConvNum = 0 //what conversation Barry needs to say next
BOOL bConvArray[NUM_CONVS]
BOOL bScaredConv  = FALSE
BOOL bTrickedConv = FALSE
BOOL bLeadInConv  = FALSE
BOOL bBlockDoingPush = FALSE
BOOL bLeadOutConv = FALSE
STRING bConvString[NUM_CONVS]
INT iConvTimer //timer for conversations before cop conversation
structPedsForConversation mConversationStruct
INT iConvDelay[CONV_COPS]

VECTOR playerLoc		= << 415.6973, -767.4496, 28.3675 >>
VECTOR BarryLoc 		= << 417.77, -765.71, 29.42 >>
//choice of cop spawning points, one one less likely to be seen is used
VECTOR copSpawnLocA  = <<442.4225, -674.2079, 27.6901>>
VECTOR copSpawnLocB  = <<446.5630, -821.9631, 27.1615>> 
//where the cop drives to
VECTOR copStopCoords = <<408.4366, -763.0913, 28.2794>>

FLOAT fPlayerHeading	= 311.6763
FLOAT fBarryHeading  	= 87.30
FLOAT fCopSpawnHeadingA = 92.7329
FLOAT fCopSpawnHeadingB = 99.7090
INT iSyncScene
INT iSyncScene2

STRING sSceneHandle_Franklin = "FRANKLIN"
STRING sSceneHandle_Barry = "BARRY"

REL_GROUP_HASH relGroupPlayer

VECTOR syncScenePos 	= <<417.533, -765.230, 28.410>>
VECTOR syncSceneHeading = <<0.0, 0.0, 12.960>>
			
#IF IS_DEBUG_BUILD
	BOOL bDebug_PrintToTTY = TRUE
	CONST_INT MAX_SKIP_MENU_LENGTH 3
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID widgetGroup
#ENDIF

/// PURPOSE:
///    disables or enables navmesh around the Barry mission to prevent amient peds wndering through
/// PARAMS:
///    if you want to disable the navmesh (TRUE) or enable (FALSE)
PROC SORT_BARRY_NAVMESH(BOOL bDisable)
	DISABLE_NAVMESH_IN_AREA(<<437.6351, -747.0594, 28.4126>>, <<397.6351, -787.0594, 28.4126>>, bDisable)
ENDPROC
	
/// PURPOSE:
///    creates a joint object
PROC CREATE_JOINT()
	IF NOT DOES_ENTITY_EXIST(objJoint)
		objJoint = CREATE_OBJECT(modelJoint, <<416.3885, -764.8415, 28.3848>>)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objJoint)
		SET_ENTITY_ROTATION(objJoint, <<0.0, 0.0, 20.0>>)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes or releases the cop and cop car
/// PARAMS:
///   bRelease - whether to release (TRUE) or delete (FALSE) 
PROC REMOVE_COP(BOOL bRelease = TRUE)
	IF bRelease
		SAFE_RELEASE_PED(copPed[0])
		SAFE_RELEASE_PED(copPed[1])
		SAFE_RELEASE_VEHICLE(copVehicle)
	ELSE
		SAFE_DELETE_PED(copPed[0])
		SAFE_DELETE_PED(copPed[1])
		SAFE_DELETE_VEHICLE(copVehicle)
	ENDIF
ENDPROC

/// PURPOSE:
///    Chooses the least likely location to be seen from a selection of locations and spawns a cop car there
FUNC BOOL CREATE_COP()
	
	IF NOT DOES_ENTITY_EXIST(copVehicle)
	AND HAS_MODEL_LOADED(modelCopVeh)
	AND HAS_MODEL_LOADED(modelCop)

		VECTOR spawnLoc 	= copSpawnLocA
		FLOAT spawnHeading  = fCopSpawnHeadingA

		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), copSpawnLocA)) < (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), copSpawnLocB))
			spawnLoc 	 = copSpawnLocB
			spawnHeading = fCopSpawnHeadingB
		ENDIF
	
		copVehicle = CREATE_VEHICLE(modelCopVeh, spawnLoc, spawnHeading)
		copPed[0] = CREATE_PED_INSIDE_VEHICLE(copVehicle, PEDTYPE_COP, modelCop, VS_DRIVER)
		GIVE_DELAYED_WEAPON_TO_PED(copPed[0], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
		SET_PED_INFINITE_AMMO(copPed[0], TRUE, WEAPONTYPE_PISTOL)
		copPed[1] = CREATE_PED_INSIDE_VEHICLE(copVehicle, PEDTYPE_COP, modelCop, VS_FRONT_RIGHT)
		GIVE_DELAYED_WEAPON_TO_PED(copPed[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
		SET_PED_INFINITE_AMMO(copPed[1], TRUE, WEAPONTYPE_PISTOL)
		SET_VEHICLE_SIREN(copVehicle, TRUE)
		SET_VEHICLE_FORWARD_SPEED(copVehicle, 20.0)
		TASK_VEHICLE_DRIVE_TO_COORD(copPed[0], copVehicle, copStopCoords, 20.0, DRIVINGSTYLE_RACING, modelCopVeh, DRIVINGMODE_STOPFORCARS, 5.0, 5.0)
		
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Handles head tracking between Franklin and Barry
PROC UPDATE_HEAD_TRACKING()
	IF IS_PLAYER_PLAYING(PLAYER_ID()) 
	AND IS_ENTITY_ALIVE(piBarry)
		SET_IK_TARGET(piBarry, IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
		SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, piBarry, ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	CPRINTLN(DEBUG_MISSION, "Barry3 script cleanup") 
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(mConversationStruct, 1)		// "FRANKLIN"
	REMOVE_PED_FOR_DIALOGUE(mConversationStruct, 3)		// "BARRY"
	CLEAR_PRINTS()
	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT, TRUE)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelCopVeh)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelCop)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelJoint)
	SAFE_RELEASE_PED(piBarry)
	SAFE_RELEASE_OBJECT(objJoint)
	REMOVE_COP()
	
	SORT_BARRY_NAVMESH(FALSE)

	// Return ambient cop creation to normal
	SET_CREATE_RANDOM_COPS(TRUE)

	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	#IF IS_DEBUG_BUILD 
		IF bDebug_PrintToTTY 
			CPRINTLN(DEBUG_MISSION, "MS_MISSION_PASSED") 
		ENDIF 
	#ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BARRY, FRANKLIN_BOOK)
	Random_Character_Passed()
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		Utility procs
// ===========================================================================================================

/// PURPOSE:
///    Detach the clipboard
/// PARAMS:
///    bAttach - TRUE = attach clipboard, FALSE = detach
PROC SORT_CLIPBOARD(BOOL bAttach)
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
		IF bAttach
			IF NOT IS_ENTITY_ATTACHED(sRCLauncherDataLocal.objID[0])
				ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.objID[0], piBarry, GET_PED_BONE_INDEX(piBarry, BONETAG_PH_L_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "clipboard attached") ENDIF #ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_ATTACHED(sRCLauncherDataLocal.objID[0])
				DETACH_ENTITY(sRCLauncherDataLocal.objID[0])
				SET_ENTITY_HAS_GRAVITY(sRCLauncherDataLocal.objID[0], TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "clipboard detached") ENDIF #ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "clipboard does not exist") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the Player gets close to Barry, headtrack, else don't (unless Barry is conversing)
PROC SORT_HEAD_TRACKING_WHEN_CLOSE()
	IF IS_ENTITY_IN_RANGE_ENTITY(piBarry, PLAYER_PED_ID(), 7.0)
		IF NOT IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
			TASK_LOOK_AT_ENTITY(piBarry, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
		ENDIF
	ELSE
		IF IS_PED_IN_CURRENT_CONVERSATION(piBarry)
			IF NOT IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(piBarry, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
			ENDIF
		ELSE
			IF IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
				TASK_CLEAR_LOOK_AT(piBarry)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_RANGE_ENTITY(piBarry, PLAYER_PED_ID(), 6.0)
		IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), piBarry)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), piBarry, -1, SLF_DEFAULT)
		ENDIF
	ELSE
		IF IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), piBarry)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the stage in the parameter, and its progress to setup
/// PARAMS:
///    MISSION_STAGE eMissionStageToSkipTo - the stage to skip to
PROC setStage(MISSION_STAGE eMissionStageToSkipTo)
	mStage = eMissionStageToSkipTo
	sProgress = SP_SETUP
ENDPROC

/// PURPOSE:
///    Sets the fail reason, and goes to the fail stage
/// PARAMS:
///    sFail - the fail reason
PROC SET_MISSION_FAILED(STRING sFail)
	STOP_SCRIPTED_CONVERSATION(FALSE)
	CLEAR_PRINTS()
	
	IF mStage <> MS_FAIL
		sFailReason =  sFail
		setStage(MS_FAIL)
	ENDIF
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Resets Barry3 variables and wanted level etc, ready for debug skipping
	PROC RESET_BARRY3()
		bScaredConv					 = FALSE
		bCopsArrived 				 = FALSE
		bBarrySeePlayerInCopCar 	 = FALSE
		bBarryScaredByPlayerInCopCar = FALSE
		bCopCarCheck 				 = TRUE
		bTrickedConv 				 = FALSE
		iConvNum 		 = 0
		iCopsArriveTimer = 0
		iConvTimer		 = 0
		INT i 			 = 0
		
		WAIT_FOR_CUTSCENE_TO_STOP()
		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
		ENDIF
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), playerLoc, fPlayerHeading)
		SAFE_TELEPORT_ENTITY(piBarry, BarryLoc, fBarryHeading, TRUE)
		IF IS_ENTITY_ALIVE(piBarry)
			SET_PED_MAX_HEALTH(piBarry, GET_PED_MAX_HEALTH(piBarry))
			CLEAR_PED_TASKS_IMMEDIATELY(piBarry)
			RESET_PED_MOVEMENT_CLIPSET(piBarry)
			
			IF IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
            	 TASK_CLEAR_LOOK_AT(piBarry)
            ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
			IF NOT IS_ENTITY_VISIBLE(sRCLauncherDataLocal.objID[0])
				SET_ENTITY_VISIBLE(sRCLauncherDataLocal.objID[0], TRUE)
			ENDIF
		ENDIF
		REMOVE_COP(FALSE)
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[0])
			ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.objID[0], piBarry, GET_PED_BONE_INDEX(piBarry, BONETAG_PH_L_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
		ENDIF
		
		REPEAT NUM_CONVS i
			bConvArray[i] = FALSE
		ENDREPEAT
	ENDPROC
	
	/// PURPOSE:
	///    Skips to the correct stage
	/// PARAMS:
	///    MISSION_STAGE eMissionStageToSkipTo: The stage you want to enter
	PROC BARRY3_DEBUG_SKIP_STATE(MISSION_STAGE eMissionStageToSkipTo)
		RC_START_Z_SKIP()
		
		SWITCH eMissionStageToSkipTo
			CASE MS_MEET_BARRY
				RESET_BARRY3()
				RC_REQUEST_CUTSCENE("BAR_5_RCM_p2")
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID())
							CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE on player in debug skip to MS_MEET_BARRY")
						ENDIF
						SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle_Barry, ANCHOR_EYES, 0)
						CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_PROP_VARIATION on Barry in debug skip to MS_MEET_BARRY")
					ENDIF
				ENDIF
				setStage(eMissionStageToSkipTo)
			BREAK
			
			CASE MS_LEAVE_AREA
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				RESET_BARRY3()
				IF IS_ENTITY_ALIVE(piBarry)
					iSyncScene2 = CREATE_SYNCHRONIZED_SCENE(syncScenePos, syncSceneHeading)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScene2, TRUE)
					TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene2, animDicBarry, "barry_3_sit_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				CREATE_JOINT()
				setStage(eMissionStageToSkipTo)
			BREAK

			CASE MS_LOSE_WANTED_LEVEL
				RESET_BARRY3()
				IF IS_ENTITY_ALIVE(piBarry)
					iSyncScene2 = CREATE_SYNCHRONIZED_SCENE(syncScenePos, syncSceneHeading)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScene2, TRUE)
					TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene2, animDicBarry, "barry_3_sit_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				CREATE_JOINT()
				setStage(eMissionStageToSkipTo)
			BREAK
			
			CASE MS_MISSION_PASSED
				RESET_BARRY3()
				setStage(MS_MISSION_PASSED)
			BREAK
			
			DEFAULT
				CERRORLN(DEBUG_MISSION, " BARRY3_DEBUG_SKIP_STATE tried to skip to ", eMissionStageToSkipTo)
				SCRIPT_ASSERT("Script cannot handle the given debug skip-to state")
			BREAK
		ENDSWITCH
		
		RC_END_Z_SKIP()
	ENDPROC
	
	/// PURPOSE:
	///		Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
			ENDIF
			Script_Passed()
		// Check for Fail
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
			ENDIF
			SET_MISSION_FAILED("DEFAULT")
		// Forward skip
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			SWITCH (mStage)
				CASE MS_INIT
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted j skip - you cannot skip initialisation") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_LEADIN
				FALLTHRU
				CASE MS_MEET_BARRY
				FALLTHRU
				CASE MS_LEADOUT
					BARRY3_DEBUG_SKIP_STATE(MS_LEAVE_AREA)
				BREAK
				
				CASE MS_LEAVE_AREA
					BARRY3_DEBUG_SKIP_STATE(MS_LOSE_WANTED_LEVEL)
				BREAK
				
				CASE MS_LOSE_WANTED_LEVEL
					BARRY3_DEBUG_SKIP_STATE(MS_MISSION_PASSED)
				BREAK
				
				CASE MS_MISSION_PASSED
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted j skip - you cannot skip mission pass") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_FAIL
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted j skip - you cannot skip during fail") 
						ENDIF 
					#ENDIF
				BREAK
				
				DEFAULT
					CERRORLN(DEBUG_MISSION, " DEBUG_Check_Debug_Keys tried to j skip to ", mStage)
					SCRIPT_ASSERT("Unhandled j skip")
				BREAK
			ENDSWITCH
			
		//back skip
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			SWITCH (mStage)
				CASE MS_INIT
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted p skip - there are no skippable stages before initialisation") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_LEADIN
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted p skip - there are no skippable stages before lead in") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_MEET_BARRY
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted p skip - there are no skippable stages before meeting barry cutscene") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_LEADOUT
				FALLTHRU
				CASE MS_LEAVE_AREA
					BARRY3_DEBUG_SKIP_STATE(MS_MEET_BARRY)
				BREAK
				
				CASE MS_LOSE_WANTED_LEVEL
					BARRY3_DEBUG_SKIP_STATE(MS_LEAVE_AREA)
				BREAK
				
				CASE MS_MISSION_PASSED
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted p skip - you cannot skip mission pass") 
						ENDIF 
					#ENDIF
				BREAK
				
				CASE MS_FAIL
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Attempted p skip - you cannot skip during fail") 
						ENDIF 
					#ENDIF
				BREAK
				
				DEFAULT
					CERRORLN(DEBUG_MISSION, " DEBUG_Check_Debug_Keys tried to p skip to ", mStage)
					SCRIPT_ASSERT("Unhandled p skip")
				BREAK
			ENDSWITCH
		ENDIF	
		
		INT i_new_state
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "Z skip menu used so doing z skip") ENDIF#ENDIF
			i_new_state++  //+1 because you can't skip to init which is 0 and you can't skip to mission passed this way
			BARRY3_DEBUG_SKIP_STATE(INT_TO_ENUM(MISSION_STAGE, i_new_state))
		ENDIF
	ENDPROC
#ENDIF	

/// PURPOSE:
///    Attempts to play Barry's end line when he's been spooked/ hurt
/// RETURNS:
///    TRUE if he's started saying it, else return FALSE
FUNC BOOL PLAY_END_CONV()
	INT rand = 0
	IF NOT bScaredConv
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_ANY_CONVERSATION()
			//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(mConversationStruct, "BARR3AU", "BARRY3_EEP", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
		ELSE
			rand = GET_RANDOM_INT_IN_RANGE(0, 3)
			IF (rand = 0)
				bScaredConv = PLAY_SINGLE_LINE_FROM_CONVERSATION(mConversationStruct, "BARR3AU", "BARRY3_EEP", "BARRY3_EEP_1", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			ELIF (rand = 1)
				bScaredConv = PLAY_SINGLE_LINE_FROM_CONVERSATION(mConversationStruct, "BARR3AU", "BARRY3_EEP", "BARRY3_EEP_2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			ELSE
				bScaredConv = PLAY_SINGLE_LINE_FROM_CONVERSATION(mConversationStruct, "BARR3AU", "BARRY3_EEP", "BARRY3_EEP_3", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD 
			IF bDebug_PrintToTTY 
				CPRINTLN(DEBUG_MISSION, "Attempting end conversation") 
			ENDIF 
		#ENDIF
	ENDIF
	
	IF bScaredConv
		#IF IS_DEBUG_BUILD 
			IF bDebug_PrintToTTY 
				CPRINTLN(DEBUG_MISSION, "Successful end conversation") 
			ENDIF 
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    bFromCops - whether the task should initially flee in a specific direction to avoid cops
PROC CREATE_BARRY_SEQUENCE_TASK(BOOL bFromCops = FALSE)

	IF bFromCops
		IF DOES_ENTITY_EXIST(copVehicle)
			OPEN_SEQUENCE_TASK(barryRunSeq)
				TASK_PLAY_ANIM(NULL, animDicBarry, "barry_get_up")
				TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(copVehicle, FALSE), 200, -1)
				TASK_SMART_FLEE_PED(NULL,PLAYER_PED_ID(), 500, -1)
			CLOSE_SEQUENCE_TASK(barryRunSeq)
		ELSE
			CPRINTLN(DEBUG_MISSION, "Couldn't get cop car to flee from, falling back to getting ")
			OPEN_SEQUENCE_TASK(barryRunSeq)
				TASK_PLAY_ANIM(NULL, animDicBarry, "barry_get_up")
				TASK_SMART_FLEE_PED(NULL,PLAYER_PED_ID(), 500, -1)
			CLOSE_SEQUENCE_TASK(barryRunSeq)
		ENDIF
	ELSE
		OPEN_SEQUENCE_TASK(barryRunSeq)
			TASK_PLAY_ANIM(NULL, animDicBarry, "barry_get_up")
			TASK_SMART_FLEE_PED(NULL,PLAYER_PED_ID(), 500, -1)
		CLOSE_SEQUENCE_TASK(barryRunSeq)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if conditions are met that will fail the mission, for example if Barry is scared away
///    Plays a fail conversation if need be 
/// RETURNS:
///    TRUE if mission has failed, else return FALSE
FUNC BOOL FAIL_CHECKS()	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN TRUE
	ELIF DOES_ENTITY_EXIST(piBarry)
		IF IS_ENTITY_DEAD(piBarry)
			SORT_CLIPBOARD(FALSE)
			SET_MISSION_FAILED("B3BARRYDEAD") // Barry died.
			RETURN TRUE
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piBarry, PLAYER_PED_ID())
		OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), piBarry)
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "FAIL HURT BARRY") 
				ENDIF 
			#ENDIF
			TASK_SMART_FLEE_PED(piBarry,PLAYER_PED_ID(), 500, -1)
			SORT_CLIPBOARD(FALSE)
			SET_MISSION_FAILED("B3BARRYHURT") // Barry was injured
			RETURN TRUE
		ELIF HAS_PLAYER_THREATENED_PED(piBarry)
		OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), piBarry, TRUE)
			IF NOT bCopsArrived
				#IF IS_DEBUG_BUILD 
					IF bDebug_PrintToTTY 
						CPRINTLN(DEBUG_MISSION, "FAIL SCARED BARRY") 
					ENDIF 
				#ENDIF
				// Make Barry flee - but can only use sequence if he is already sitting
				IF mStage = MS_LEADOUT
				AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
				AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) < 0.82
					// Don't interrupt the task sequence if it's going to look stupid
				ELSE
					CREATE_BARRY_SEQUENCE_TASK()
					TASK_PERFORM_SEQUENCE(piBarry, barryRunSeq)
				ENDIF
				SORT_CLIPBOARD(FALSE)
				SET_MISSION_FAILED("B3BARRYSCARE")
				RETURN TRUE	
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		MISSION STAGE PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Requests cutscene required by mission depending on if it's a replay
///    Sets variables for mission
PROC STATE_INIT()

	// Not using switch - need to save a few frames

	//
	// SETUP
	//
	IF sProgress = SP_SETUP
		INT i
		#IF IS_DEBUG_BUILD 
			IF bDebug_PrintToTTY 
				CPRINTLN(DEBUG_MISSION, "MS_INIT") 
			ENDIF 
		#ENDIF

		REPEAT NUM_CONVS i
			bConvArray[i] = FALSE
		ENDREPEAT
		
		bConvString[CONV_WARN1] = "BARRY3_C1"
		bConvString[CONV_WARN2] = "BARRY3_C2"
		bConvString[CONV_WARN3] = "BARRY3_C3"
		bConvString[CONV_COPS] = "BARRY3_COPS"
		
		iConvDelay[CONV_WARN1] = 0
		iConvDelay[CONV_WARN2] = 20000
		iConvDelay[CONV_WARN3] = 7000
		
		// Get the PLAYER relationship group
		relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())

		SET_PED_RELATIONSHIP_GROUP_HASH(piBarry, relGroupPlayer)
		SET_PED_CONFIG_FLAG(piBarry, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)		
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piBarry, TRUE)
		SET_PED_CONFIG_FLAG(piBarry, PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(piBarry, FALSE)
		
		ADD_PED_FOR_DIALOGUE(mConversationStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		ADD_PED_FOR_DIALOGUE(mConversationStruct, 3, piBarry, "BARRY")
		
		#IF IS_DEBUG_BUILD
			mSkipMenu[0].sTxtLabel = "Intro"
			mSkipMenu[1].sTxtLabel = "Leave area" 
			mSkipMenu[2].sTxtLabel = "Lose wanted level" 
			IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
				widgetGroup = START_WIDGET_GROUP("Barry 3 widgets")
				ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)	
				STOP_WIDGET_GROUP()
			ENDIF
		#ENDIF
		
		REQUEST_ADDITIONAL_TEXT("BARY3", MISSION_TEXT_SLOT)
		REQUEST_MODEL(modelJoint)
		REQUEST_ANIM_DICT(animDicBarry)
		
		sProgress = SP_RUNNING
	ENDIF
	
	//
	// RUNNING
	//
	IF sProgress = SP_RUNNING
		IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		AND HAS_MODEL_LOADED(modelJoint)
		AND HAS_ANIM_DICT_LOADED(animDicBarry)
		
			IF IS_REPLAY_IN_PROGRESS()	
				SWITCH GET_REPLAY_MID_MISSION_STAGE()
					CASE CP_AFTER_MOCAP
						REQUEST_MODEL(modelCopVeh)
						REQUEST_MODEL(modelCop)
						SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
						SAFE_TELEPORT_ENTITY(piBarry, BarryLoc, fBarryHeading, TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						IF IS_ENTITY_ALIVE(piBarry)
							iSyncScene2 = CREATE_SYNCHRONIZED_SCENE(syncScenePos, syncSceneHeading)
							SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScene2, TRUE)
							TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene2, animDicBarry, "barry_3_sit_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						ENDIF
			
						CREATE_JOINT()
						START_REPLAY_SETUP(playerLoc, fPlayerHeading)
						END_REPLAY_SETUP()
						setStage(MS_LEAVE_AREA)
					BREAK

					DEFAULT
						CERRORLN(DEBUG_MISSION, " STATE_INIT tried to replay ", GET_REPLAY_MID_MISSION_STAGE())
						SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
					BREAK
				ENDSWITCH
			ELSE
				// Request scene where Franklin meets Barry
				RC_REQUEST_CUTSCENE("BAR_5_RCM_p2", FALSE)
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())	// added by Ahron for release build fix
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID())						
						CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE player")
					ENDIF
					SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle_Barry, ANCHOR_EYES, 0)
					CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_PROP_VARIATION on Barry in STATE_INIT")
				ENDIF
				// Turn off random cops for now - B*1217186
				SET_CREATE_RANDOM_COPS(FALSE)
				// Handle screen fadeout from replay
				SAFE_FADE_SCREEN_IN_FROM_BLACK() 
				setStage(MS_LEADIN)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Plays lead in animation
PROC STATE_LEADIN()

	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	UPDATE_HEAD_TRACKING()
	
	// Replaced switch with IFs - shave off a few frames
	IF sProgress = SP_SETUP
		CPRINTLN(DEBUG_MISSION, "Init LEADIN") 
		bLeadInConv = FALSE
		bBlockDoingPush = FALSE
		SORT_CLIPBOARD(TRUE)
		RC_END_CUTSCENE_MODE() //return script systems to normal. //TEST
		
		IF NOT Is_Replay_In_Progress()
			IF IS_ENTITY_ALIVE(piBarry)
				TASK_PLAY_ANIM(piBarry, animDicBarry, "lead_in", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
				CPRINTLN(DEBUG_MISSION, "Done barry leadin anim") 
				sProgress = SP_RUNNING
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "Replay in progress or in wrong area - skipping to intro") 
			setStage(MS_MEET_BARRY)
		ENDIF
	
	// Need to wait a frame so leadin anim can start
	ELIF sProgress = SP_RUNNING
		IF IS_PED_UNINJURED(piBarry)
			IF NOT bStartedPush
			AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<415.478058,-771.704102,27.855623>>, <<415.560608,-759.937195,31.361616>>, 7.25) OR IS_PLAYER_IN_FIRST_PERSON_CAMERA())
				bStartedPush = TRUE
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, 5.0)
					CPRINTLN(DEBUG_MISSION, "STATE_LEADIN found player <5m from Barry on start push, blocked the push")
					bBlockDoingPush = TRUE
				ENDIF
				CPRINTLN(DEBUG_MISSION, "STATE_LEADIN starting focus push now")
			ENDIF
			IF bStartedPush
				IF NOT bBlockDoingPush
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						SET_GAMEPLAY_ENTITY_HINT(piBarry, (<<0.0, 0, 0.5>>), TRUE, 5000, DEFAULT_INTERP_IN_TIME, DEFAULT_INTERP_OUT_TIME)
					ELSE
						SET_GAMEPLAY_ENTITY_HINT(piBarry, (<<0.5, 0, 0.5>>), TRUE, 5000, DEFAULT_INTERP_IN_TIME, DEFAULT_INTERP_OUT_TIME)
						SET_GAMEPLAY_HINT_FOV(35.0)
						SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.35)
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.2)
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.01)
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, 2.5)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piBarry)
				ENDIF
			ENDIF
			IF IS_ENTITY_PLAYING_ANIM(piBarry, animDicBarry, "lead_in")
				IF bLeadInConv = FALSE
					IF CREATE_CONVERSATION(mConversationStruct, "BARR3AU", "BAR_3_INT_LI", CONV_PRIORITY_HIGH)
						CPRINTLN(DEBUG_MISSION, "Done leadin conv") 
						bLeadInConv = TRUE
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CPRINTLN(DEBUG_MISSION, "leadin conv over") 
						sProgress = SP_CLEANUP
					ENDIF
				ENDIF
				IF GET_ENTITY_ANIM_CURRENT_TIME(piBarry, animDicBarry, "lead_in") > 0.90
					CPRINTLN(DEBUG_MISSION, "leadin anim over") 
					sProgress = SP_CLEANUP
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "not playing leadin anim anymore - skip to cutscene for safety") 
				sProgress = SP_CLEANUP
			ENDIF
		ENDIF
	ENDIF
	
	IF sProgress = SP_CLEANUP
		CPRINTLN(DEBUG_MISSION, "Cleaned up LEADIN") 
		REQUEST_MODEL(modelCopVeh)
		REQUEST_MODEL(modelCop)
		setStage(MS_MEET_BARRY)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Triggers the mocap cutscene and skips to next mission state once finished
PROC STATE_MEET_BARRY()
	
	SWITCH sProgress
		
		CASE SP_SETUP
			
			CPRINTLN(DEBUG_MISSION, "Init MS_MEET_BARRY") 
			
			CLEAR_AREA_OF_COPS(<< 417.78, -765.71, 29.39>>, 40)
			SET_CREATE_RANDOM_COPS(FALSE)
			
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID())
					CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE on player in STATE_MEET_BARRY")
				ENDIF
				SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle_Barry, ANCHOR_EYES, 0)
				CPRINTLN(DEBUG_MISSION, "SET_CUTSCENE_PED_PROP_VARIATION on Barry in STATE_MEET_BARRY")
			ENDIF
			
			IF RC_IS_CUTSCENE_OK_TO_START()
				REGISTER_ENTITY_FOR_CUTSCENE(piBarry, sSceneHandle_Barry, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Franklin, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
					SORT_CLIPBOARD(FALSE)
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[0], "Barrys_Clipboard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					CPRINTLN(DEBUG_MISSION, "Register clipboard for cutscene")
				ENDIF
				
				// Cleanup launcher to remove lead-in blip
				RC_CLEANUP_LAUNCHER()
				
				// Start mocap scene
				START_CUTSCENE()
				WAIT(0)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<416.8295, -762.7533, 27.3960>>, <<416.6834, -767.3608, 34.4159>>, 6.0, 
													<< 411.6, -773.5004, 27.9818 >>, 4.0678)
				RC_START_CUTSCENE_MODE(<< 417.78, -765.71, 29.39>>)
	
				// Monitor cutscene
				sProgress = SP_RUNNING 
			ENDIF
		BREAK
		
		CASE SP_RUNNING
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT(TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH()						
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Franklin)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSCeneHandle_Barry)
				IF IS_ENTITY_ALIVE(piBarry)
					iSyncScene = CREATE_SYNCHRONIZED_SCENE(<<417.533, -765.230, 28.410>>, <<0.0, 0.0, 12.960>>)
					TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene, animDicBarry, "lead_out", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Barrys_Clipboard")
					SORT_CLIPBOARD(TRUE)
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
				CREATE_JOINT()
				RC_END_CUTSCENE_MODE()
				sProgress = SP_CLEANUP
			ENDIF
		BREAK
		
		CASE SP_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleaned up MS_MEET_BARRY")
			setStage(MS_LEADOUT)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    If Barry doesn't notice the Player get in a cop car and they drive up to him, he'll freak out and run away
///    RETURNS:
///    TRUE if Barry has been tricked, else FALSE
FUNC BOOL SORT_IF_BARRY_TRICKED_BY_PLAYER_IN_COP_CAR()
	IF IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
		//sort if Barry has seen Player enter cop car
		IF bCopCarCheck
		AND NOT bBarryScaredByPlayerInCopCar
			IF IS_ENTITY_IN_RANGE_ENTITY(piBarry, PLAYER_PED_ID(), NOTICE_PLAYER_ZONE)
				bBarrySeePlayerInCopCar = TRUE
				#IF IS_DEBUG_BUILD 
					IF bDebug_PrintToTTY 
						CPRINTLN(DEBUG_MISSION, "Barry has seen you enter the cop car") 
					ENDIF 
				#ENDIF
			ELSE
				bBarrySeePlayerInCopCar = FALSE
				#IF IS_DEBUG_BUILD 
					IF bDebug_PrintToTTY 
						CPRINTLN(DEBUG_MISSION, "Barry's not seen you enter the cop car") 
					ENDIF 
				#ENDIF
			ENDIF
			
			bCopCarCheck = FALSE
		ENDIF
		
		//run away if tricked by Player
		IF NOT bBarryScaredByPlayerInCopCar
		AND NOT bBarrySeePlayerInCopCar
			IF IS_ENTITY_IN_RANGE_ENTITY(piBarry, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), BARRY_TALK_ZONE)
				#IF IS_DEBUG_BUILD 
					IF bDebug_PrintToTTY 
						CPRINTLN(DEBUG_MISSION, "Barry tricked by Player in cop car") 
					ENDIF 
				#ENDIF
				bBarryScaredByPlayerInCopCar = TRUE
				STOP_SCRIPTED_CONVERSATION(FALSE)
				CREATE_BARRY_SEQUENCE_TASK()
				TASK_PERFORM_SEQUENCE(piBarry, barryRunSeq)
				IF IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
			       TASK_CLEAR_LOOK_AT(piBarry)
			    ENDIF
			ENDIF
		ENDIF
	ELSE
		bCopCarCheck = TRUE
	ENDIF
	
	RETURN bBarryScaredByPlayerInCopCar
ENDFUNC

/// PURPOSE:
///    check if ped is performing a certain task or not
/// PARAMS:
///    pedIndex - the ped index you're looking for
///    theScrTaskName - the SCRIPT_TASK_NAME you are looking for
/// RETURNS:
///    TRUE if performing or waiting for the task, else FALSE
FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX pedIndex, SCRIPT_TASK_NAME theScrTaskName)
	IF (GET_SCRIPT_TASK_STATUS(pedIndex, theScrTaskName) = PERFORMING_TASK) 
	OR (GET_SCRIPT_TASK_STATUS(pedIndex, theScrTaskName) = WAITING_TO_START_TASK)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays Barry's lead out animation
PROC STATE_LEADOUT()
	SWITCH sProgress
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Init MS_LEADOUT") 
				ENDIF 
			#ENDIF
			//double check that sync scene is running
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
				IF IS_ENTITY_ALIVE(piBarry)
					iSyncScene = CREATE_SYNCHRONIZED_SCENE(<<417.533, -765.230, 28.410>>, <<0.0, 0.0, 12.960>>)
					TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene, animDicBarry, "lead_out", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ENDIF
			ENDIF
			SORT_CLIPBOARD(TRUE)
			bLeadOutConv = FALSE
			sProgress 	 = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
				IF bLeadOutConv = FALSE
					IF CREATE_CONVERSATION(mConversationStruct, "BARR3AU", "BAR_3_INT_LO", CONV_PRIORITY_HIGH)
						#IF IS_DEBUG_BUILD 
							IF bDebug_PrintToTTY 
								CPRINTLN(DEBUG_MISSION, "Done leadin conv") 
							ENDIF 
						#ENDIF
						bLeadOutConv = TRUE
					ENDIF
				ENDIF
			
				IF (GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) > 0.99)
					#IF IS_DEBUG_BUILD 
						IF bDebug_PrintToTTY 
							CPRINTLN(DEBUG_MISSION, "Finished lead out") 
						ENDIF 
					#ENDIF
					sProgress = SP_CLEANUP
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD 
					IF bDebug_PrintToTTY 
						CPRINTLN(DEBUG_MISSION, "Barry not performing task, skipping lead in") 
					ENDIF 
				#ENDIF
				sProgress = SP_CLEANUP
			ENDIF
		BREAK
		
		CASE SP_CLEANUP
			SORT_CLIPBOARD(FALSE)
			setStage(MS_LEAVE_AREA)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Barry keeps telling the player to get out of the area
///    Completes mission if player does so, else calls cops if player hangs around for too long
PROC STATE_LEAVE_AREA()
	SWITCH sProgress	
		
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Init MS_LEAVE_AREA") 
				ENDIF 
			#ENDIF
			
			SORT_BARRY_NAVMESH(FALSE)
		//	SORT_CLIPBOARD(TRUE)
			
			IF DOES_ENTITY_EXIST(piBarry)
				CLEAR_AREA_OF_COPS(GET_ENTITY_COORDS(piBarry), 1000)
				iSyncScene2 = CREATE_SYNCHRONIZED_SCENE(syncScenePos, syncSceneHeading)
				SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScene2, TRUE)
				TASK_SYNCHRONIZED_SCENE(piBarry, iSyncScene2, animDicBarry, "barry_3_sit_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
			ENDIF
			
			SET_CREATE_RANDOM_COPS(FALSE)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			
			if not IS_PLAYER_USING_A_TAXI()
				SAFE_FADE_SCREEN_IN_FROM_BLACK() // Handle screen fadeout from replay
			endif
			
			iCopsArriveTimer = GET_GAME_TIMER()
			iConvTimer		 = GET_GAME_TIMER()
			sProgress 		 = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			IF NOT SORT_IF_BARRY_TRICKED_BY_PLAYER_IN_COP_CAR()
				SORT_HEAD_TRACKING_WHEN_CLOSE()
				
				IF iConvNum < CONV_COPS //so don't go over any arrays
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, BARRY_TALK_ZONE)
						IF NOT bConvArray[iConvNum]
							IF (GET_GAME_TIMER() - iConvTimer) > iConvDelay[iConvNum]
								bConvArray[iConvNum] = CREATE_CONVERSATION(mConversationStruct, "BARR3AU", bConvString[iConvNum], CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
							ENDIF
						ELSE
							iConvNum++ 
							iConvTimer = GET_GAME_TIMER() // Initialise new timer
						ENDIF
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							#IF IS_DEBUG_BUILD 
								IF bDebug_PrintToTTY 
									CPRINTLN(DEBUG_MISSION, "NOT IN RANGE, stopping conversation") 
								ENDIF 
							#ENDIF
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, BARRY_TALK_ZONE)
					IF NOT bTrickedConv
						bTrickedConv = CREATE_CONVERSATION(mConversationStruct, "BARR3AU", "BARRY3_TRICK", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT bCopsArrived
				IF (GET_GAME_TIMER() - iCopsArriveTimer) > COPS_DELAY
				OR GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) > 0
					bCopsArrived = TRUE
					sProgress = SP_CLEANUP
				ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, BARRY_ZONE)
					sProgress = SP_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SP_CLEANUP
			IF NOT bCopsArrived
				mStage = MS_MISSION_PASSED
			ELSE
				mStage = MS_LOSE_WANTED_LEVEL
			ENDIF
			
			sProgress = SP_SETUP
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Cleaned up MS_LEAVE_AREA") 
				ENDIF 
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Cops are after the player. Player has to lose his wanted level.
///    Completes mission if player does so
PROC STATE_LOSE_WANTED_LEVEL()
	SWITCH sProgress	
		CASE SP_SETUP

			IF CREATE_COP()

				CPRINTLN(DEBUG_MISSION, "Init MS_LOSE_WANTED_LEVEL") 
				
				bCopsArrived = TRUE
				
				SORT_CLIPBOARD(FALSE)
				STOP_SCRIPTED_CONVERSATION(FALSE)
				SET_CREATE_RANDOM_COPS(TRUE)
				SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
				
				PRINT_NOW("LOSE_WANTED_LVL", DEFAULT_GOD_TEXT_TIME, 1)
				IF NOT bConvArray[CONV_COPS]
				AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, BARRY_TALK_ZONE)
					bConvArray[CONV_COPS] = CREATE_CONVERSATION(mConversationStruct, "BARR3AU", bConvString[CONV_COPS], CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				ENDIF
				
				IF NOT bBarryScaredByPlayerInCopCar
					CREATE_BARRY_SEQUENCE_TASK(TRUE)
					TASK_PERFORM_SEQUENCE(piBarry, barryRunSeq)
					IF IS_PED_HEADTRACKING_PED(piBarry, PLAYER_PED_ID())
				       TASK_CLEAR_LOOK_AT(piBarry)
				    ENDIF
				ENDIF
				
				IF IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), piBarry)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
				
				sProgress = SP_RUNNING
			ENDIF
			
		BREAK
		
		CASE SP_RUNNING
			
			IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
				sProgress = SP_CLEANUP
			ENDIF
			
			IF IS_ENTITY_ALIVE(copPed[0])
			AND IS_ENTITY_ALIVE(copPed[1])
				IF IS_ENTITY_IN_RANGE_COORDS(copPed[0], copStopCoords, 30.0)
				OR IS_PED_IN_PED_VIEW_CONE(copPed[0], PLAYER_PED_ID())
				OR IS_PED_IN_PED_VIEW_CONE(copPed[1], PLAYER_PED_ID())
					SAFE_RELEASE_PED(copPed[0])
					SAFE_RELEASE_PED(copPed[1])
					SAFE_RELEASE_VEHICLE(copVehicle)
				ENDIF
			ENDIF
		BREAK
		
		CASE SP_CLEANUP
			mStage 		= MS_MISSION_PASSED
			sProgress   = SP_SETUP
			#IF IS_DEBUG_BUILD
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Cleaned up MS_LOSE_WANTED_LEVEL") 
				ENDIF 
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Fails and cleans up the mission correctly
PROC STAGE_FAIL()
	SWITCH sProgress	
		CASE SP_SETUP
			IF ARE_STRINGS_EQUAL(sFailReason,"DEFAULT")
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason, TRUE)
			ENDIF
			
			sProgress = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING	
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()			
				Script_Cleanup() // script_cleanup should terminate the thread
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				//make Barry say "What the hell?" if he's there
					IF IS_PED_UNINJURED(piBarry)
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
						AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piBarry, BARRY_TALK_ZONE)
							PLAY_END_CONV()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	CPRINTLN(DEBUG_MISSION, "BARRY3::: Script starting... ") 

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()= TRUE // Set up the initial scene for replays
		g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_BARRY_3(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		VEHICLE_INDEX vTmp
		CREATE_VEHICLE_FOR_REPLAY(vTmp, << 410.74, -778.37, 28.87 >>, 359.2, FALSE, FALSE, FALSE, FALSE, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF

	// Grab Barry ped so we can handle him more easily
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[BARRY])
		ASSIGN_PED_INDEX(piBarry, sRCLauncherDataLocal.pedID[BARRY])
	ENDIF
	
	SORT_BARRY_NAVMESH(TRUE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_GRF")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF mStage = MS_FAIL
			STAGE_FAIL()
		ELSE
			IF NOT FAIL_CHECKS() //if not failed mission
				REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(392.7562, -842.7891, 511.9787,-669.2035)
				SWITCH(mStage)
					CASE MS_INIT
						STATE_INIT()
					BREAK
					
					CASE MS_LEADIN
						STATE_LEADIN()
					BREAK
				
					CASE MS_MEET_BARRY
						STATE_MEET_BARRY()
					BREAK
					
					CASE MS_LEADOUT
						STATE_LEADOUT()
					BREAK
					
					CASE MS_LEAVE_AREA
						STATE_LEAVE_AREA()
					BREAK
					
					CASE MS_LOSE_WANTED_LEVEL
						STATE_LOSE_WANTED_LEVEL()
					BREAK
					
					CASE MS_MISSION_PASSED
						Script_Passed()
					BREAK
				ENDSWITCH
				
				// Check debug completion/failure
				#IF IS_DEBUG_BUILD
					DEBUG_Check_Debug_Keys()
				#ENDIF
			ENDIF
		ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

