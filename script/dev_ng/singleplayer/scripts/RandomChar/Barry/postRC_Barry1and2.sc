// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	postRC_Barry1and2.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Clean up For Barry 1 and 2
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"

//----------------------
//	VARIABLES
//----------------------
OBJECT_INDEX chairObject
OBJECT_INDEX deskObject
OBJECT_INDEX signObject
PED_INDEX barryPed

STRING		sBarryAnimDict = "RCMBarryLeadInOut"
STRING		sBarryAnimLoop = "idle"

INT			iBarryDialogueTimer = 0
INT			iNumber = 0
INT 		iDisableControlTimer

VECTOR		vPos = <<189.5964, -956.0344, 29.5771>>
VECTOR		vBarryPos = <<190.2424, -956.3790, 28.63>>

#IF IS_DEBUG_BUILD	
WIDGET_GROUP_ID mDebugWidget
BOOL bMissionFlagTest = FALSE
#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD	
PROC CREATE_DEBUG_WIDGETS()
	mDebugWidget = START_WIDGET_GROUP("PostRC_Barry1And2")
		ADD_WIDGET_BOOL("Mission Flag Test", bMissionFlagTest)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	IF (bMissionFlagTest)
		SET_MISSION_FLAG(TRUE)
		bMissionFlagTest = FALSE
	ENDIF
ENDPROC

PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(mDebugWidget)
		DELETE_WIDGET_GROUP(mDebugWidget)
	ENDIF
ENDPROC	
#ENDIF

PROC SCRIPT_CLEANUP(BOOL bDelete=FALSE)
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - SCRIPT_CLEANUP")
	
	#IF IS_DEBUG_BUILD	
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF

/*
	IF IS_ENTITY_ALIVE(barryPed)
		STOP_PED_SPEAKING(barryPed, TRUE)
	ENDIF
*/

	IF bDelete
		SAFE_DELETE_PED(barryPed)
	ELSE
		SAFE_RELEASE_PED(barryPed, TRUE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(deskObject)
		FREEZE_ENTITY_POSITION(deskObject, FALSE)
	ENDIF
	IF IS_ENTITY_ALIVE(chairObject)
		FREEZE_ENTITY_POSITION(chairObject, FALSE)
	ENDIF

	IF bDelete
		SAFE_DELETE_OBJECT(chairObject)
		SAFE_DELETE_OBJECT(deskObject)
		SAFE_DELETE_OBJECT(signObject)
	ELSE
		SAFE_RELEASE_OBJECT(chairObject)
		SAFE_RELEASE_OBJECT(deskObject)
		SAFE_RELEASE_OBJECT(signObject)
	ENDIF
	
	REMOVE_ANIM_DICT(sBarryAnimDict)
	CLEAR_ADDITIONAL_TEXT(AMBIENT_DIALOGUE_TEXT_SLOT, FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SETUP_BARRY()
	IF IS_ENTITY_ALIVE(barryPed)
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - SETUP BARRY!")
		IF NOT IS_ENTITY_A_MISSION_ENTITY(barryPed)
			SET_ENTITY_AS_MISSION_ENTITY(barryPed)
		ENDIF
		
		SET_PED_PROP_INDEX(barryPed, ANCHOR_EYES, 0) //Glasses prop.
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(barryPed, TRUE)
		SET_PED_MONEY(barryPed, 0)
		SET_PED_CAN_BE_TARGETTED(barryPed, FALSE)
		SET_PED_NAME_DEBUG(barryPed, "POSTBARRY")
		SET_PED_RELATIONSHIP_GROUP_HASH(barryPed, RELGROUPHASH_PLAYER)
		
		SET_ENTITY_COLLISION(barryPed, FALSE)
		FREEZE_ENTITY_POSITION(barryPed, TRUE)
		SET_ENTITY_COORDS(barryPed, vBarryPos, TRUE)
		SET_ENTITY_HEADING(barryPed, RAD_TO_DEG(1.12))
		FREEZE_ENTITY_POSITION(barryPed, TRUE)
		
		TASK_PLAY_ANIM(barryPed, sBarryAnimDict, sBarryAnimLoop, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
		TASK_LOOK_AT_ENTITY(barryPed, PLAYER_PED_ID(), -1)
	ENDIF
ENDPROC

// mothballing - we'll just create him
//FUNC BOOL GET_BARRY_PED()
//	INT cnt 
//	INT i 
//	PED_INDEX tmpArray[32]
//	
//	cnt = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), tmpArray)
//	
//	REPEAT cnt i
//		IF IS_ENTITY_ALIVE(tmpArray[i])
//			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - PED FOUND!")
//			IF (GET_ENTITY_MODEL(tmpArray[i]) = GET_NPC_PED_MODEL(CHAR_BARRY))
//				
//				CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - BARRY GET!")
//				barryPed = tmpArray[i]
//
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	RETURN FALSE
//ENDFUNC

PROC SETUP_PROPS()
	// get desk (should be left over from outro cutscene)
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<190.56, -956.40, 28.63>>, 20.0, PROP_PROTEST_TABLE_01)
		deskObject = GET_CLOSEST_OBJECT_OF_TYPE(<<190.56, -956.40, 28.63>>, 20.0, PROP_PROTEST_TABLE_01, FALSE)
		IF IS_ENTITY_OK(deskObject)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(deskObject)
				SET_ENTITY_AS_MISSION_ENTITY(deskObject)
				CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - DESK GET!")
			ENDIF
			vPos = <<189.5964, -956.0344, 29.540>>
			SET_ENTITY_COORDS(deskObject, vPos)
			SET_ENTITY_HEADING(deskObject, RAD_TO_DEG(-2.01))
			FREEZE_ENTITY_POSITION(deskObject, TRUE)
		ENDIF
		
	ENDIF
	// get chair (should be left over from outro cutscene)
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<190.56, -956.40, 28.63>>, 20.0, PROP_CHAIR_08)
		chairObject = GET_CLOSEST_OBJECT_OF_TYPE(<<190.56, -956.40, 28.63>>, 20.0, PROP_CHAIR_08, FALSE)
		IF IS_ENTITY_OK(chairObject)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(chairObject)
				SET_ENTITY_AS_MISSION_ENTITY(chairObject)
				CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - CHAIR GET!")
			ENDIF
			vPos = <<190.2574, -956.3513, 29.621>>
			SET_ENTITY_COORDS(chairObject, vPos)
			SET_ENTITY_HEADING(chairObject, RAD_TO_DEG(-1.68))
			FREEZE_ENTITY_POSITION(chairObject, TRUE)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - CREATING CHAIR!")
		vPos = <<190.2574, -956.3513, 29.621>>
		CREATE_SCENE_PROP(chairObject, PROP_CHAIR_08, vPos, RAD_TO_DEG(-1.68))
		SET_ENTITY_COORDS(chairObject, vPos)
		SET_ENTITY_HEADING(chairObject, RAD_TO_DEG(-1.68))
		FREEZE_ENTITY_POSITION(chairObject, TRUE)
		
	ENDIF
	// get standing sign 
	vPos = <<192.4462, -953.5946, 29.0919>>
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vPos, 25.0, Prop_Protest_Sign_01)
		signObject = GET_CLOSEST_OBJECT_OF_TYPE(vPos, 25.0, Prop_Protest_Sign_01, FALSE)
		IF IS_ENTITY_OK(signObject)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(signObject)
				SET_ENTITY_AS_MISSION_ENTITY(signObject)
				CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - SIGN GET!")
			ENDIF
			// Correct height if moving existing object
			vPos.z = 29.603
			SET_ENTITY_COORDS(signObject, vPos)
			SET_ENTITY_HEADING(signObject, 23.45)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - CREATING SIGN!")
		CREATE_SCENE_PROP(signObject, Prop_Protest_Sign_01, vPos, 23.45)
		SET_ENTITY_COORDS(signObject, vPos)
		SET_ENTITY_HEADING(signObject, 23.45)
	ENDIF
ENDPROC

FUNC STRING GET_NEXT_BARRY_DIALOGUE()
	STRING DialogueName
	
	// Get dialogue label
	SWITCH iNumber
		CASE 0
			DialogueName = "BARY1_RCMLI1"
		BREAK
		CASE 1
			DialogueName = "BARY1_RCMLI2"
		BREAK
		CASE 2
			DialogueName = "BARY1_RCMLI3"
		BREAK
		CASE 3
			DialogueName = "BARY1_RCMLI4"
		BREAK
		CASE 4
			DialogueName = "BARY1_RCMLI5"
		BREAK
	ENDSWITCH
	
	// increase count for next time
	iNumber ++
	IF iNumber > 4
		iNumber = 0
	ENDIF
	
	RETURN DialogueName
ENDFUNC

/// PURPOSE: Play ambient dialogue from the RC ped every so often
FUNC BOOL PLAY_POST_BARRY_AMBIENT_DIALOGUE()

	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(barryPed)
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND (GET_GAME_TIMER() > iBarryDialogueTimer)
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), barryPed) < 20.0
	AND NOT GET_MISSION_FLAG()
		structPedsForConversation s_conversation  
		ADD_PED_FOR_DIALOGUE(s_conversation, 0, barryPed, "BARRY")
		ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation, "BARY1AU", GET_NEXT_BARRY_DIALOGUE(), CONV_PRIORITY_MEDIUM)
		iBarryDialogueTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(12000, 18000)
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// let barry and props get exploded
PROC HANDLE_EXPLOSIONS()

	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vBarryPos, 5.0)
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - Explosion detected!")
		IF IS_ENTITY_ALIVE(barryPed)
			FREEZE_ENTITY_POSITION(barryPed, FALSE)
			APPLY_DAMAGE_TO_PED(barryPed, 1000, TRUE)
		ENDIF
		IF IS_ENTITY_ALIVE(deskObject)
			FREEZE_ENTITY_POSITION(deskObject, FALSE)
		ENDIF
		IF IS_ENTITY_ALIVE(chairObject)
			FREEZE_ENTITY_POSITION(chairObject, FALSE)
		ENDIF
	ENDIF
ENDPROC

SCRIPT
	
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - INIT AMBIENT SCRIPT")
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - Resetting wanted level to zero")
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		ENDIF
	ENDIF
	
	// Default callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		SCRIPT_CLEANUP()
	ENDIF
	
	REQUEST_ANIM_DICT(sBarryAnimDict)
	REQUEST_ADDITIONAL_TEXT("BARY1", AMBIENT_DIALOGUE_TEXT_SLOT)
	
	WHILE NOT HAS_ANIM_DICT_LOADED(sBarryAnimDict)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(AMBIENT_DIALOGUE_TEXT_SLOT)
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - Waiting on loads...")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - disable look behind, post load")
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)

	IS_ENTITY_OK(PLAYER_PED_ID())
	
	// allow time for mission passed graphic
	iBarryDialogueTimer = GET_GAME_TIMER() + 13000
	// start on a random line
	iNumber = GET_RANDOM_INT_IN_RANGE(0,5)
	
	SETUP_PROPS()

	#IF IS_DEBUG_BUILD	
		CREATE_DEBUG_WIDGETS()
	#ENDIF
	
	WHILE NOT CREATE_NPC_PED_ON_FOOT(barryPed, CHAR_BARRY, vBarryPos, RAD_TO_DEG(1.12), FALSE)
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - CREATING BARRY!")
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - disable look behind, post barry creation")
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	SETUP_BARRY()
	iDisableControlTimer = GET_GAME_TIMER() + 1000
	
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - disable look behind, post barry setup")
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	
	// Main loop
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID())
		IF GET_GAME_TIMER() < iDisableControlTimer
			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - disable look behind, waiting for timer...")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENDIF
		
		HANDLE_EXPLOSIONS()
		
		IF NOT IS_ENTITY_OK(barryPed)
			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - BARRY IS PROBABLY DEAD - CLEANUP")
			SCRIPT_CLEANUP()
		ENDIF
		
		IF PLAY_POST_BARRY_AMBIENT_DIALOGUE()
			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2: Shhh... Barry is saying something important...")
		ENDIF
		
		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vBarryPos) > 70.0)
			CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY1AND2 - BARRY IS FAR ENOUGH - QUIT SCRIPT")
			SAFE_RELEASE_OBJECT(chairObject)
			SAFE_RELEASE_OBJECT(deskObject)
			SAFE_RELEASE_OBJECT(signObject)
			
			SCRIPT_CLEANUP()
		ENDIF
		
	#IF IS_DEBUG_BUILD	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_H)
			SET_MISSION_FLAG(TRUE)
		ENDIF
		
		UPDATE_DEBUG_WIDGETS()
	#ENDIF			
			
		WAIT(0)
	ENDWHILE
	
ENDSCRIPT




