
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "randomChar_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	postRC_Barry4.sc											//
//		AUTHOR			:																//
//		DESCRIPTION		:	Handles Franklin's gestures and conversation after 			//
//                          completing Barry 4 RCM.										//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

ENUM MISSION_STAGE
	MS_INIT,
	MS_RUNNING
ENDENUM

MISSION_STAGE mStage 	= MS_INIT
structPedsForConversation convStruct
INT iConvTimer

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Terminates ambient script after performing cleanup functions
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY4.SC - SCRIPT CLEANUP") 
	REMOVE_PED_FOR_DIALOGUE(convStruct, 1)		// "FRANKLIN"
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY4.SC - TERMINATING AMBIENT SCRIPT") 
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Initialises variables etc
PROC INIT()
	iConvTimer = GET_GAME_TIMER()
	ADD_PED_FOR_DIALOGUE(convStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
	mStage = MS_RUNNING
ENDPROC

/// PURPOSE:
///    Plays Franklin's annoyed lines randomly
PROC PLAY_ANNOYED_LINES()
	IF (GET_GAME_TIMER() - iConvTimer) > 5000 // 5.0 sec delay between each 'call'
		IF CREATE_CONVERSATION(convStruct, "BARR4AU", "BARR4_AMB", CONV_PRIORITY_HIGH)
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT 
	
	CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY4.SC - INIT AMBIENT SCRIPT")
	
	// Default callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_AMBIENT, "POSTRC_BARRY4.SC - DEFAULT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE (TRUE)
		
		WAIT(0)
		
		// Terminate if we are not Franklin
		IF SHOULD_POST_RC_SCRIPT_TERMINATE(CHAR_FRANKLIN)
			SCRIPT_CLEANUP()
		ELSE
			SWITCH mStage
				CASE MS_INIT
					INIT()
				BREAK
				
				CASE MS_RUNNING
					PLAY_ANNOYED_LINES()
				BREAK
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT
