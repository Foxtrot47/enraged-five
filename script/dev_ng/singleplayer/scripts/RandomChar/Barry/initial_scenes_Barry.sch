USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE
MODEL_NAMES 	    mContactModel      = GET_NPC_PED_MODEL(CHAR_BARRY)

/// PURPOSE:
///    Create Barry's weed sign
PROC CREATE_STANDING_SIGN(OBJECT_INDEX &oiSign, MODEL_NAMES mnSign)
	VECTOR vPos = <<192.4462, -953.5946, 29.0919>>
	FLOAT fHead = 23.45
	CREATE_SCENE_PROP(oiSign, mnSign, vPos, fHead)
ENDPROC

/// PURPOSE:
///    Blocks the worker and security scenarios at Barry3C's start
FUNC SCENARIO_BLOCKING_INDEX Barry3C_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< -564.0, -1758.1, 16.5 >>, << -419.31, -1649.0, 23.0 >>)
ENDFUNC

/// PURPOSE: Creates initial scene for BARRY 1.
/// NOTE:    Barry is hanging out at Mirror Park
FUNC BOOL SetupScene_BARRY_1(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_BARRY 	0
	CONST_INT MODEL_DESK   	1
	CONST_INT MODEL_CHAIR 	2
	CONST_INT MODEL_STUFF  	3
	CONST_INT MODEL_LIGHTER	4
	CONST_INT MODEL_JOINT 	5
	CONST_INT MODEL_SIGN 	6
	
	// Variables
	MODEL_NAMES mModel[7]
//	PED_BONETAG attachBone    = BONETAG_R_HAND
	VECTOR 		vBarryPos     = <<190.2424, -956.3790, 28.63>>
	FLOAT  		fBarryHeading = RAD_TO_DEG(1.12)
	INT 	    iCount
	BOOL        bCreatedScene
	
//	VECTOR 		v
//	VECTOR 		jointRotOffset = <<-80, 0.0, -20.0>>
//	VECTOR 		jointPosOffset = <<0.105, 0.0, -0.01>>


	// Assign model names
	mModel[MODEL_BARRY]   = mContactModel
	mModel[MODEL_DESK]    = PROP_PROTEST_TABLE_01
	mModel[MODEL_CHAIR]   = PROP_CHAIR_08
	mModel[MODEL_STUFF]   = P_A4_Sheets_S
	mModel[MODEL_LIGHTER] = P_CS_Lighter_01
	mModel[MODEL_JOINT]   = P_CS_JOINT_01
	mModel[MODEL_SIGN]    = Prop_Protest_Sign_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS 
			sRCLauncherData.sIntroCutscene = "BAR_1_RCM_P2"
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.triggerLocate[0] = <<190.031509,-956.335510,29.091919>>
			sRCLauncherData.triggerLocate[1] = <<178.045792,-950.659424,31.093576>>
			sRCLauncherData.triggerWidth = 23.0

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "RCMBarryLeadInOut", "idle", "fidget")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK

		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Barry
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BARRY, vBarryPos, fBarryHeading, "RC LAUNCHER: BARRY 1")
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_EYES, 0) // Glasses prop.
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create desk
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
				
				// Clear area for table
				CLEAR_AREA_OF_OBJECTS(<<189.5964, -956.0344, 29.09179>>, 5.0)
				CREATE_SCENE_PROP(sRCLauncherData.objID[0], mModel[MODEL_DESK], <<189.5964, -956.0344, 29.09179>>, RAD_TO_DEG(-2.01))
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[0]) 
					FREEZE_ENTITY_POSITION(sRCLauncherData.objID[0], TRUE)
				
					// Create papers on table
					CREATE_SCENE_PROP(sRCLauncherData.objID[2], mModel[MODEL_STUFF], <<189.79014, -956.16747, 29.8229>>, RAD_TO_DEG(-2.01))
					IF IS_ENTITY_ALIVE(sRCLauncherData.objID[2]) 
						FREEZE_ENTITY_POSITION(sRCLauncherData.objID[2], TRUE)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Create chair
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[1])
				CREATE_SCENE_PROP(sRCLauncherData.objID[1], mModel[MODEL_CHAIR], <<190.2574, -956.3513, 29.09179>>, RAD_TO_DEG(-1.68))
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[1]) 
					FREEZE_ENTITY_POSITION(sRCLauncherData.objID[1], TRUE)
					SET_ENTITY_COLLISION(sRCLauncherData.objID[1], FALSE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
	
		CASE IS_COMPLETE_SCENE
			
			// Setup Barry
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[0], FALSE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], RELGROUPHASH_PLAYER)
				SET_ENTITY_COORDS(sRCLauncherData.pedID[0], vBarryPos, TRUE)
				SET_ENTITY_HEADING(sRCLauncherData.pedID[0], fBarryHeading)
				FREEZE_ENTITY_POSITION(sRCLauncherData.pedID[0], TRUE)
				
				// Play anim
				TASK_PLAY_ANIM(sRCLauncherData.pedID[0], sRCLauncherData.sAnims.sDictionary, sRCLauncherData.sAnims.sIdleAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE) // AF_TAG_SYNC_OUT | AF_TURN_OFF_COLLISION
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1)
			ENDIF
			
			// Setup desk
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[0]) 
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[0], true)
			ENDIF
			
			// Setup chair
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[1]) 
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[1], true)
				SET_ENTITY_COLLISION(sRCLauncherData.objID[1], TRUE)
			ENDIF
	
			// Create joint on table
			CREATE_SCENE_PROP(sRCLauncherData.objID[4], mModel[MODEL_JOINT], <<189.895, -955.945, 29.818>>, 0) 
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[4]) 
				SET_ENTITY_ROTATION(sRCLauncherData.objID[4], (<<-90.0, 0.0, -30.8>>))
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[4], true)
			ENDIF
	
			// @SBA - adding lighter
			CREATE_SCENE_PROP(sRCLauncherData.objID[3], mModel[MODEL_LIGHTER], <<189.783, -956.015, 29.79>>, 0) 
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[3]) 
				SET_ENTITY_ROTATION(sRCLauncherData.objID[3], (<<-90.0, 0.0, 75.0>>))
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[3], true)
			ENDIF
			
			// Create sign
			CREATE_STANDING_SIGN(sRCLauncherData.objID[5], mModel[MODEL_SIGN])
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: Creates initial scene for BARRY 2.
/// NOTE:    Barry is still hanging out at Mirror Park
FUNC BOOL SetupScene_BARRY_2(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_BARRY  0
	CONST_INT MODEL_DESK   1
	CONST_INT MODEL_CHAIR  2
	CONST_INT MODEL_STUFF  3
	CONST_INT MODEL_SIGN   4
	
	// Variables
	MODEL_NAMES mModel[5]
	VECTOR      vBarryPos     = <<190.2424, -956.4790, 29.08>> 
	FLOAT       fBarryHeading = 69.171  // try another 5 - 10 degrees counter clockwise
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_BARRY] = mContactModel
	mModel[MODEL_DESK]  = PROP_PROTEST_TABLE_01
	mModel[MODEL_CHAIR] = PROP_CHAIR_08
	mModel[MODEL_STUFF] = P_A4_Sheets_S
	mModel[MODEL_SIGN]  = Prop_Protest_Sign_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.sIntroCutscene = "BAR_3_RCM"
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.triggerLocate[0] = <<190.031509,-956.335510,29.091919>>
			sRCLauncherData.triggerLocate[1] = <<178.045792,-950.659424,31.093576>>
			sRCLauncherData.triggerWidth = 23.0

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_barry2", "lead_in_loop", "lead_in_loop")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Barry
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BARRY, vBarryPos, fBarryHeading, "RC LAUNCHER: BARRY 2")
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_EYES, 0) // Glasses prop.
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF

			// Create desk
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
			
				// Clear area for table
				CLEAR_AREA_OF_OBJECTS(<< 189.5964, -956.0344, 29.09179 >>, 2.0)
				CREATE_SCENE_PROP(sRCLauncherData.objID[0], mModel[MODEL_DESK], << 189.5964, -956.0344, 29.09179 >>, RAD_TO_DEG(-2.01))
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[0]) 
					FREEZE_ENTITY_POSITION(sRCLauncherData.objID[0], TRUE)
					
					// Create papers on table
					CREATE_SCENE_PROP(sRCLauncherData.objID[2], mModel[MODEL_STUFF], << 189.79014, -956.16747, 29.8229 >>, RAD_TO_DEG(-2.01))
					IF IS_ENTITY_ALIVE(sRCLauncherData.objID[2]) 
						FREEZE_ENTITY_POSITION(sRCLauncherData.objID[2], TRUE)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF

			// Create chair
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[1])
				CREATE_SCENE_PROP(sRCLauncherData.objID[1], mModel[MODEL_CHAIR], <<190.417, -956.511, 29.09179>>, -99.157)
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[1]) 
					FREEZE_ENTITY_POSITION(sRCLauncherData.objID[1], TRUE)
					SET_ENTITY_COLLISION(sRCLauncherData.objID[1], FALSE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF

			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK

		CASE IS_COMPLETE_SCENE
			
			// Setup Barry
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[0], FALSE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], RELGROUPHASH_PLAYER)
				SET_ENTITY_COORDS(sRCLauncherData.pedID[0], vBarryPos, TRUE)
				SET_ENTITY_HEADING(sRCLauncherData.pedID[0], fBarryHeading)
				FREEZE_ENTITY_POSITION(sRCLauncherData.pedID[0], TRUE)
				
				// Play anim
				TASK_PLAY_ANIM(sRCLauncherData.pedID[0], sRCLauncherData.sAnims.sDictionary, sRCLauncherData.sAnims.sIdleAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1)
			ENDIF
			
			// Setup desk
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[0]) 
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[0], true)
			ENDIF
	
			// Setup chair
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[1]) 
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[1], true)
				SET_ENTITY_COLLISION(sRCLauncherData.objID[1], TRUE)
			ENDIF
			
			// Create sign
			CREATE_STANDING_SIGN(sRCLauncherData.objID[3], mModel[MODEL_SIGN])
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for BARRY 3.
FUNC BOOL SetupScene_BARRY_3(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_BARRY  	  0
	CONST_INT MODEL_CLIPBOARD 1
	CONST_INT OBJ_CLIPBOARD   0
	
	// Variables
	MODEL_NAMES mModel[2]
	VECTOR 	    vBarryPos     = << 417.78, -765.71, 29.39 >>
	FLOAT  	    fBarryHeading = 87.3
	INT 	    iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_BARRY]     = mContactModel
	mModel[MODEL_CLIPBOARD] = P_CS_CLIPBOARD
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType 			= RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.triggerLocate[0]		= <<415.458679,-773.361511,27.854902>>
			sRCLauncherData.triggerLocate[1]		= <<415.520538,-758.571533,31.344553>>
			sRCLauncherData.triggerWidth			= 14.50
			sRCLauncherData.sIntroCutscene 			= "BAR_5_RCM_P2"
			sRCLauncherData.bPedsCritical 			= TRUE
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_barry3", "bar_3_rcm_barry_standing_on_street_base", "bar_3_rcm_barry_standing_on_street_fidget")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK		
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Barry
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BARRY, vBarryPos, fBarryHeading, "RC LAUNCHER: BARRY 3")
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_EYES, 0) // Glasses prop.
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Attach clipboard
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[OBJ_CLIPBOARD])
				CREATE_SCENE_PROP(sRCLauncherData.objID[OBJ_CLIPBOARD], mModel[MODEL_CLIPBOARD], (vBarryPos+ <<5.0, 0.0, 0.0>>), -115.18)
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJ_CLIPBOARD]) AND IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[OBJ_CLIPBOARD], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_PH_L_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC		

/// PURPOSE: 
///    Creates initial scene for BARRY 3A.
FUNC BOOL SetupScene_BARRY_3A(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_TRUCK  0
	CONST_INT MODEL_STASH  1
	CONST_INT MODEL_UCCAR  2
	CONST_INT MODEL_UCCOP  3
	
	// Variables
	MODEL_NAMES mModel[4]
	VECTOR 	    vCarPos   	 = << 1199.80, -1259.22, 34.23 >>
	VECTOR	    vStashOffset = << 0.0, -1.00, 0.64 >>
	VECTOR	    vCopPos		 = << 1195.30, -1312.74, 34.75 >>
	FLOAT  	    fCarHeading  = 174.7
	FLOAT  	    fCopHeading  = 282.4
	INT 	    iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_TRUCK] = DLOADER
	mModel[MODEL_STASH] = Prop_Weed_Tub_01b
	mModel[MODEL_UCCAR] = POLICE4
	mModel[MODEL_UCCOP] = A_M_Y_BUSINESS_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<1198.366577,-1326.094971,33.968296>>
			sRCLauncherData.triggerLocate[1] = <<1202.236816,-1235.373535,43.976414>>
			sRCLauncherData.triggerWidth = 31.00
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.bVehsCritical = TRUE

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create stash car
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_TRUCK], vCarPos, fCarHeading)
			IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
	
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
				SET_VEHICLE_ALARM(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_TRUCK], TRUE)
				SET_VEHICLE_NAME_DEBUG(sRCLauncherData.vehID[0], "WEED STASH VEHICLE")
				SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_EXTRA(sRCLauncherData.vehID[0], 1, TRUE)
				SET_VEHICLE_EXTRA(sRCLauncherData.vehID[0], 2, TRUE)
				SET_VEHICLE_EXTRA(sRCLauncherData.vehID[0], 3, TRUE)
				
			ELSE
				bCreatedScene = FALSE
			ENDIF
			
			// Attach stash to vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
				CREATE_SCENE_PROP(srcLauncherData.objID[0], mModel[MODEL_STASH], vCarPos+vStashOffset, 0)
				IF DOES_ENTITY_EXIST(srcLauncherData.objID[0]) AND DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
					ATTACH_ENTITY_TO_ENTITY(srcLauncherData.objID[0], sRCLauncherData.vehID[0], 0, vStashOffset, << 0,0,0 >>, FALSE, FALSE, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create cop car and cops
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_UCCAR], vCopPos, fCopHeading)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				CREATE_SCENE_PED(sRCLauncherData.pedID[0], mModel[MODEL_UCCOP], vCopPos, fCopHeading, PEDTYPE_COP)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_UCCOP], vCopPos, fCopHeading, PEDTYPE_COP)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
			
			// Setup cops into cop car
			IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[1])
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					SET_PED_AS_COP(sRCLauncherData.pedID[0])
					SET_PED_INTO_VEHICLE(sRCLauncherData.pedID[0], sRCLauncherData.vehID[1], VS_DRIVER)
					GIVE_DELAYED_WEAPON_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_PISTOL, 100, TRUE)
				ENDIF
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
					SET_PED_AS_COP(sRCLauncherData.pedID[1])
					SET_PED_INTO_VEHICLE(sRCLauncherData.pedID[1], sRCLauncherData.vehID[1], VS_FRONT_RIGHT)
					GIVE_DELAYED_WEAPON_TO_PED(sRCLauncherData.pedID[1], WEAPONTYPE_PISTOL, 100, TRUE)
				ENDIF
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for BARRY 3C.
FUNC BOOL SetupScene_BARRY_3C(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CAR      0
	CONST_INT MODEL_STASH    1
	CONST_INT MODEL_TOWTRUCK 2
	
	// Variables
	MODEL_NAMES mModel[3]
	VECTOR 		vCarPos       = << -468.90, -1713.06, 18.21 >>
	VECTOR 		vTruckPos 	  = << -426.56, -1717.89, 18.20 >>
	VECTOR 		vStashOffset  = << -0.10, -0.85, 0.23 >>
	VECTOR 		vStashRotate  = << 8.0, 0.5, 0.0 >>
	FLOAT 		fCarHeading   = -76.20
	FLOAT 		fTruckHeading = 43.5
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CAR]      = EMPEROR2
	mModel[MODEL_STASH]    = Prop_Weed_Tub_01
	mModel[MODEL_TOWTRUCK] = TOWTRUCK
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-480.992920,-1712.649536,17.452402>>
			sRCLauncherData.triggerLocate[1] = <<-443.612946,-1705.886353,27.622137>>
			sRCLauncherData.triggerWidth = 31.00
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.bVehsCritical = TRUE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], vCarPos, fCarHeading)
				IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
					SET_VEHICLE_ALARM(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_CAR], TRUE)
					SET_VEHICLE_NAME_DEBUG(sRCLauncherData.vehID[0], "WEED STASH VEHICLE")
					SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherData.vehID[0], FALSE)
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
					SET_VEHICLE_UNDRIVEABLE(sRCLauncherData.vehID[0], TRUE)

				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Attach stash to vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
				CREATE_SCENE_PROP(srcLauncherData.objID[0], mModel[MODEL_STASH], vCarPos+vStashOffset, 0)
				IF DOES_ENTITY_EXIST(srcLauncherData.objID[0]) AND DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
					ATTACH_ENTITY_TO_ENTITY(srcLauncherData.objID[0], sRCLauncherData.vehID[0], 0, vStashOffset, vStashRotate)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create tow truck
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_TOWTRUCK], vTruckPos, fTruckHeading)
				SET_VEHICLE_COLOURS(sRCLauncherData.vehID[1], 39, 43)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC


/// PURPOSE: 
///    Creates initial scene for BARRY 4.
FUNC BOOL SetupScene_BARRY_4(g_structRCScriptArgs& sRCLauncherData)
	
	// Setup specific launcher data
	sRCLauncherData.triggerType = RC_TRIG_LOCATE_POINT
	sRCLauncherData.triggerLocate[0]  = <<237.65, -385.41, 44.40>>
	sRCLauncherData.bAllowVehicleActivation = TRUE
	
	// Scene is good to go!
	RETURN TRUE
ENDFUNC
