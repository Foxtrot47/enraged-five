
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "chase_hint_cam.sch"
USING "RC_Threat_Public.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Epsilon.sch"
USING "achievement_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Epsilon8.sc
//		AUTHOR			:	Tom Kingsley
//		DESCRIPTION		:	Michael has to drive a car full of money for the Epsilonists, 
//							he can choose to drive it to their helicopter or steal it.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// RC scene information
g_structRCScriptArgs sRCLauncherDataLocal

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FLOAT fEscapeDistance = 400.0          			// How far away you have to get to evade security cars
FLOAT fOnFootEscapeDistance = 150.0				// How far away you have to get to evade security (on foot)
FLOAT fHeliEscapeDistance = 450.0          		// How far away you have to get to evade Helicopter	
FLOAT fSmallBlipScale = BLIP_SIZE_PED
FLOAT fLargeBlipScale = BLIP_SIZE_VEHICLE
FLOAT fObjectBlipScale = BLIP_SIZE_PICKUP
INT iNumberOfMoneyBags = 4      				// Number of money bags
INT iCashInEachBag = 525000    					// Amount of $ in each bag
FLOAT fAreaWidth = 34.750000                    // Default width of area at helipad
FLOAT fHeliSpeed = 50                           // Default heli speed (it won't actually go this fast but this float affects how fast it loses speed if all other guys evaded)
INT iAccuracy = 25                      		// Accuracy of guys before modifiers  //35
INT iHeliSniperAccModifier = 10                	// Less accuracy for Todd
INT iShootRate = 25								// Shoot rate of guys before modifiers  //16
INT iHeliSniperSRModifier = 75					// Extra shoot rate for Todd
VECTOR vParkPos = <<-363.3094, -82.3607, 44.6577>>    //Park position at the helicopter
INT iCol1 = 157 //67 //140
//INT iCol2 = 140  //127  //140

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

VECTOR vPosEpsilonCar[7]
VECTOR vPosEpsilonHiSec[7]
VECTOR vPosChopper
VECTOR vPlayer
VECTOR vPosPlayerCar
VECTOR vCashCarParkedPos
VECTOR vCashCar
VECTOR vHeliTarget
VECTOR vCompoundBack
VECTOR vCompoundFront
VECTOR vSafeCoord																			

FLOAT fHeadingEpsilonCar[7]
FLOAT fHeadingEpsilonHiSec[7]
FLOAT fDistBetweenPlayerAndDriver
FLOAT fHeadingChopper
FLOAT fHeadingPlayerCar
FLOAT fTopSpeed
FLOAT fPlayerCarPetrolTankHP
FLOAT fPlayerCarEngineHP
/*
FLOAT fY
FLOAT fX
FLOAT fZ2
*/
//FLOAT fHeadingHeliTarget

INT iTotalCash
INT i = 0
INT iEpSecBlipped = 0
INT iEpSecEvaded = 0
//INT iRandomInt
INT iWantedLevel
INT iZ
INT iConvoCounter
INT iHeliNoLosConvoCounter
INT iTimerHeliDeadUpdate
INT iTimerHeliDead
INT iPassedDelay
INT iCounterPassedDelay
INT iCounterPassedDelayUpdate
INT iTimerHeliLostSightOfPlayer
INT iTimerHeliLostSightOfPlayerUpdate
INT iHeliGivenUp
INT iPlayerCarHP
INT iSeqHeli
INT iTimerHeliSeq
INT iTimerHeliSeqUpdate
INT iPlayerDamagingEscort
//INT i2
INT iTimerThreatenPlayerInCar
INT iSeqThreatenPlayerInCar
INT iTimerPlayerCarDestroyed
INT iNodeNumber = 1
//INT iLoadChopperSeqSwitch
INT iConvoChaseRandom
INT iConvoChaseCar
INT iTimerPlayerLeftCar
INT iPrintPlayerLeftCar
INT iHeliSpotPlayerConvo
INT iHeliCamSwitch
INT iPrintToddDied
INT iTimerToddDied
INT iEpsilonistBeckonConvo
INT iTimerPrayConvo
INT iTimerPlayerStoleMoney
INT iTimerMissionInit
INT iTimerSecurityGettingOnChopper
INT iTimerPlayerParkedAtHeli
INT iTimerDetectDamage
INT iEpsilonistsKilled
INT iTimerDelayAggression
INT iSeqDelayAggression
INT iSeqDelayAggressionParkedUp
INT iVehThief = -1
INT iHeliTask = 0
INT iCounterHeliCamLostPlayer
INT iCam = 0
INT iTriggerSecondMusicCue = 0
INT iBagsPutInHeli
//INT iBagsTakenFromCar
INT iTimerHurryUp
INT iCounterHurryUp = 0
INT iTimerIntroCams			
INT	iIntroCamSeq
INT issLoadHeli
INT iTimerOverrideStartWalk
//INT iTimerWaypointRec
//INT iCounterHeliCantSeePlayerChase = 0

BOOL bPlayerStoleMoney
BOOL bPlayerGaveMoney
BOOL bCutsceneChopperTaskGiven
BOOL bLoseCopsObjGiven
BOOL bHelicopterHostile
BOOL bDriveByTaskGiven[7]
BOOL bGuard1SaidKifflom
BOOL bGuard2SaidKifflom
BOOL bGuard3SaidKifflom
BOOL bMissionPassed
BOOL bPlayerAtHelipad
BOOL bMessagePrintedLeavingCash
//BOOL bHeliLoadSwitch[7]
BOOL bBootOpen
BOOL bGuardGettingOnHeli
BOOL bGetInCarObjGiven
BOOL bChopperBeingJacked
BOOL bPlayerWentToTractor
BOOL bEscapeMessagePrinted
BOOL bNoCashLeftInCar
BOOL bPlayerCarDestroyed
BOOL bEpsilonistAtTractor
BOOL bRoadSwitchOn
BOOL bEpsilonistPlayingAnims
BOOL bEpsDriverForcedToLeaveVeh[7]
BOOL bPrintHurryUp
BOOL bPrintRamWarning1
BOOL bPrintRamWarning2
BOOL bPrintFrontWarning
BOOL bPrintEscapeWarning
BOOL bPrintMikeOnFoot
BOOL bPrintMikeInCover
BOOL bPrintToddShotMike
BOOL bDebugSkipping
BOOL bFollowEpsSecObj
BOOL bPlayerSkippedToEnd
BOOL bHeliCantSeePlayer
BOOL bPrintHeliCamHelp
BOOL bMikeLookAtEpsilonist
BOOL bStartRearConvoyCar
BOOL bOnlyHeliLeft
BOOL bPrintEvadeHeli
BOOL bPrayConvo
BOOL bOnlyHeliLeftConvo
BOOL bDoHeliCam
BOOL bPrintFollowEpsilonist
BOOL bDebugPassOrFail
BOOL bDoneCrisAngryPhoneCall
BOOL bHeliCollOff
BOOL bOverrideHeliColl
BOOL bSecurityStealingTailgater
BOOL bMikePickedUpABag = FALSE	
BOOL bHeliGivenUp = FALSE
BOOL bPrintExitCar = FALSE
BOOL bCSExitMike = FALSE
BOOL bCSExitCris = FALSE
BOOL bCSExitCam = FALSE
BOOL bCSExitCar = FALSE
BOOL bCarStopped = FALSE
BOOL bOverrideStartWalk = FALSE	
BOOL bTriggerMusic = FALSE	
BOOL bSwitchWaypointFlags = FALSE
//BOOL bHeliCantSeePlayerChase = FALSE

STRING sFailReason

// Mission stages
ENUM MISSION_STAGE
	MS_SETUP,
	MS_MOCAP,
	MS_INIT,
	MS_DRIVING_TO_CHOPPER,
	MS_PARKING_AT_CHOPPER,
	MS_PARKED_AT_CHOPPER,
	MS_EXITED_CAR,
	MS_WALKING_TO_REWARD,
	MS_TAKEN_MONEY,
	MS_LOADED_MONEY_IN_CHOPPER,
	MS_MISSION_FAILING
ENDENUM

// Fail stages
ENUM FAIL_STATE
	FS_SETUP,
	FS_UPDATE,
	FS_CLEANUP
ENDENUM	

// Cutscene stages
ENUM CUTSCENE_STAGE
	eCutInit,
	eCutUpdate,
	eCutCleanup
ENDENUM	

ENUM LOAD_HELI_STAGE
	LOAD_HELI_INIT,
	WALK_TO_CAR,
	TAKE_BAG,
	WALK_TO_HELI,
	DROP_OFF_BAG,
	GET_IN_HELI
ENDENUM

LOAD_HELI_STAGE eLOAD_HELI = LOAD_HELI_INIT

/*
ENUM iSub
	iSubTrue,
	iSubFalse
ENDENUM	
*/
/*
ENUM enumSubtitlesState

    DISPLAY_SUBTITLES,
    DO_NOT_DISPLAY_SUBTITLES

ENDENUM
*/

REL_GROUP_HASH relEpsilon
REL_GROUP_HASH relEpsilonCiv
structPedsForConversation s_conversation_peds
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
CUTSCENE_STAGE eCutsceneState = eCutInit
MISSION_STAGE missionStage = MS_SETUP
FAIL_STATE eFAIL_STATE

PED_INDEX pedEpsilonHiSecurity[7]
PED_INDEX pedPilot
PED_INDEX pedChopperShotgun
PED_INDEX pedEpsilonHiSecDriver[7]
PED_INDEX pedEpsilonHiSecShotgun[7]
PED_INDEX pedEpsilonist

OBJECT_INDEX objCashBag[10]

CAMERA_INDEX camHeli
CAMERA_INDEX camHeli2
CAMERA_INDEX camHeliLostPlayer[2]

CAMERA_INDEX camIntro1
CAMERA_INDEX camIntro2

VEHICLE_INDEX vehEpsilonChopper
VEHICLE_INDEX vehEpsilonCar[7]
VEHICLE_INDEX vehExtraCar[7]
VEHICLE_INDEX vehPlayerCar
VEHICLE_INDEX vehTractor

VEHICLE_INDEX vehTrafficRoute1[7]
PED_INDEX pedTrafficRoute1[7]
VECTOR vTrafficRoute1[7]
FLOAT fHeadingTrafficRoute1[7]

VEHICLE_INDEX vehTrafficRoute2[7]
PED_INDEX pedTrafficRoute2[7]
VECTOR vTrafficRoute2[7]
FLOAT fHeadingTrafficRoute2[7]

VEHICLE_INDEX vehTrafficRoute3[7]
PED_INDEX pedTrafficRoute3[7]
VECTOR vTrafficRoute3[7]
FLOAT fHeadingTrafficRoute3[7]

VEHICLE_INDEX vehTrafficRoute4[7]
PED_INDEX pedTrafficRoute4[7]
VECTOR vTrafficRoute4[7]
FLOAT fHeadingTrafficRoute4[7]

VEHICLE_INDEX vehTrafficRoute5[7]
PED_INDEX pedTrafficRoute5[7]
VECTOR vTrafficRoute5[7]
FLOAT fHeadingTrafficRoute5[7]

VEHICLE_INDEX vehTrafficRoute6[7]
PED_INDEX pedTrafficRoute6[7]
VECTOR vTrafficRoute6[7]
FLOAT fHeadingTrafficRoute6[7]

INT iTraffic

BLIP_INDEX blipEpsilonHiSecDriver[7]
BLIP_INDEX blipEpsilonHiSecShotgun[7]
BLIP_INDEX blipEpsilonHiSecurity[7]
BLIP_INDEX blipChopper
BLIP_INDEX blipRandomChar
BLIP_INDEX blipPilot
BLIP_INDEX blipPlayerCar
BLIP_INDEX blipEscort
BLIP_INDEX blipEpsOnFootEscort
BLIP_INDEX blipLastCashBag
//BLIP_INDEX blipCashBag

SEQUENCE_INDEX seqExitLeanStartDriving
SEQUENCE_INDEX seqGoToPlayerFacePlayer
//SEQUENCE_INDEX seqStopCarAndExit
//SEQUENCE_INDEX seqLoadChopper1
//SEQUENCE_INDEX seqLoadChopper2
SEQUENCE_INDEX seqPrayToTractor

SCENARIO_BLOCKING_INDEX EpsBuildingBlock
SCENARIO_BLOCKING_INDEX EpsBuildingBlock2
SCENARIO_BLOCKING_INDEX CockatoosCarBlocker

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 3
	INT iReturnStage                                       
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]     
#ENDIF

/// PURPOSE: Gets a heading between 2 vectors
FUNC FLOAT GET_HEADING_FROM_COORDS(vector oldCoords,vector newCoords, bool invert=true)
	float heading
	float dX = newCoords.x - oldCoords.x
	float dY = newCoords.y - oldCoords.y
	if dY != 0
		heading = ATAN2(dX,dY)
	ELSE
		if dX < 0
			heading = -90
		ELSE
			heading = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	IF invert = TRUE 	
		heading *= -1.0
		//below not necessary but helps for debugging
		IF heading < 0
			heading += 360.0
		ENDIF
	ENDIF
	
	RETURN heading
ENDFUNC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE: //Remove all blips
PROC RemoveBlips()

	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[0])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[1])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[2])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[3])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[4]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[5])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[6])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[0]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[1])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[2])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[3])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[4])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[5])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[6]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[0])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[1])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[2]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[3])
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[4]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[5]) 
	SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[6])
	SAFE_REMOVE_BLIP(blipChopper)
	SAFE_REMOVE_BLIP(blipRandomChar) 
	SAFE_REMOVE_BLIP(blipPilot) 
	SAFE_REMOVE_BLIP(blipPlayerCar)
	SAFE_REMOVE_BLIP(blipEscort)
	SAFE_REMOVE_BLIP(blipEpsOnFootEscort)
	SAFE_REMOVE_BLIP(blipLastCashBag)

ENDPROC

PROC START_AUDIO_SCENE_COMPLIANCE()  

	IF NOT IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_COMPLIANCE")	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[0])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[0], "EPSILONISM_08_COMPLIANCE_CAR_LEADER")
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonChopper)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonChopper, "EPSILONISM_08_COMPLIANCE_HELI")
		ENDIF	
		START_AUDIO_SCENE("EPSILONISM_08_COMPLIANCE")
	ENDIF
	
ENDPROC

PROC CLEANUP_AUDIO_SCENE_COMPLIANCE() 
	
	IF IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_COMPLIANCE")
		/*
		IF IS_ENTITY_ALIVE(vehEpsilonCar[0])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[0])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehPlayerCar)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayerCar)
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonChopper)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonChopper)
		ENDIF
		*/
		STOP_AUDIO_SCENE("EPSILONISM_08_COMPLIANCE")
	ENDIF

ENDPROC

PROC START_AUDIO_SCENE_NON_COMPLIANCE()  

	IF NOT IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_COMPLIANCE")
		IF NOT IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_NON_COMPLIANCE")	
			IF IS_ENTITY_ALIVE(vehEpsilonCar[0])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[0], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[1])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[1], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[2])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[2], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[3])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[3], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[4])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[4], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[5])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[5], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonCar[6])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[6], "EPSILONISM_08_NON_COMPLIANCE_ENEMY_CAR")
			ENDIF
			IF IS_ENTITY_ALIVE(vehEpsilonChopper)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonChopper, "EPSILONISM_08_NON_COMPLIANCE_HELI")
			ENDIF	
			START_AUDIO_SCENE("EPSILONISM_08_NON_COMPLIANCE")
		ENDIF
	ELSE
		STOP_AUDIO_SCENE("EPSILONISM_08_COMPLIANCE")
	ENDIF

ENDPROC

PROC CLEANUP_AUDIO_SCENE_NON_COMPLIANCE() 

	IF IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_NON_COMPLIANCE")
		/*
		IF IS_ENTITY_ALIVE(vehEpsilonCar[0])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[0])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[1])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[1])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[2])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[2])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[3])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[3])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[4])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[4])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[5])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[5])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonCar[6])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[6])
		ENDIF	
		IF IS_ENTITY_ALIVE(vehPlayerCar)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehPlayerCar)
		ENDIF	
		IF IS_ENTITY_ALIVE(vehEpsilonChopper)
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonChopper)
		ENDIF	
		*/
		STOP_AUDIO_SCENE("EPSILONISM_08_NON_COMPLIANCE")
	ENDIF

ENDPROC

PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	
	// Cleanup scene entities created by the RC launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	CLEANUP_AUDIO_SCENE_COMPLIANCE() 
	CLEANUP_AUDIO_SCENE_NON_COMPLIANCE()  
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)					
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(issLoadHeli)
		STOP_SYNCHRONIZED_ENTITY_ANIM(vehPlayerCar,NORMAL_BLEND_OUT,TRUE)
	ENDIF
	
	IF (DOES_ENTITY_EXIST(pedPilot))
		IF bPlayerGaveMoney = TRUE
			IF NOT IS_ENTITY_DEAD(pedPilot)
				SET_PED_KEEP_TASK(pedPilot,TRUE)
			ENDIF
		ENDIF
		SAFE_RELEASE_PED(pedPilot)
	ENDIF
	
	IF IS_ENTITY_ALIVE(vehPlayerCar)
		IF bBootOpen = TRUE
			SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
			SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
			bBootOpen = FALSE
		ENDIF
	ENDIF

	SAFE_RELEASE_PED(pedChopperShotgun)
	IF IS_ENTITY_ALIVE(vehEpsilonChopper)
		SET_VEHICLE_DOORS_LOCKED(vehEpsilonChopper,VEHICLELOCK_UNLOCKED)
	ENDIF
	SAFE_RELEASE_VEHICLE(vehEpsilonChopper)
	
	IF DOES_CAM_EXIST(camHeli)
		
		SET_CAM_ACTIVE(camHeli,FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camHeli)
		
		IF DOES_CAM_EXIST(camHeli2)	
			
			SET_CAM_ACTIVE(camHeli2,FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camHeli2)
	
		ENDIF
		
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
	
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
	
	ENDIF
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	i = 0
	
	IF IS_VEHICLE_OK(vehEpsilonCar[0])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[0])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[0], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[1])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[1])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[1], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[2])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[2])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[2], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[3])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[3])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[3], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[4])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[4])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[4], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[5])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[5])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[5], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[6])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[6])
		SET_DISABLE_PRETEND_OCCUPANTS(vehEpsilonCar[6], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehExtraCar[0])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehExtraCar[0])
		SET_DISABLE_PRETEND_OCCUPANTS(vehExtraCar[0], TRUE)
	ENDIF
	IF IS_VEHICLE_OK(vehExtraCar[1])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehExtraCar[1])
		SET_DISABLE_PRETEND_OCCUPANTS(vehExtraCar[1], TRUE)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_CHOPPER")			
		STOP_AUDIO_SCENE("EPSILONISM_08_CHOPPER")		
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(EpsBuildingBlock)
	REMOVE_SCENARIO_BLOCKING_AREA(EpsBuildingBlock2)
	REMOVE_SCENARIO_BLOCKING_AREA(CockatoosCarBlocker)
		
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_HIGHSEC_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL)
	SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
	SET_MODEL_AS_NO_LONGER_NEEDED(LANDSTALKER)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_EPSILON_01)
		
	REMOVE_ANIM_DICT("rcmepsilonism8")

	SET_VEHICLE_MODEL_IS_SUPPRESSED(LANDSTALKER,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL2,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,FALSE)
	
	SET_PED_MODEL_IS_SUPPRESSED(A_F_Y_EPSILON_01,FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_Y_EPSILON_01,FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_Y_EPSILON_02,FALSE)
	
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER,FALSE)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER,FALSE)
	
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_AMBULANCE_DEPARTMENT,FALSE)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_FIRE_DEPARTMENT,FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1)
	
	IF bMissionPassed = TRUE	
		DISABLE_CELLPHONE(FALSE)
		
		IF bPlayerStoleMoney = TRUE
			KILL_ANY_CONVERSATION()                                          //Cris rings Michael (pissed off)
			IF bPlayerSkippedToEnd = FALSE
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				IF bDoneCrisAngryPhonecall = FALSE	
					REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 4)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, NULL, "CRIS")
					REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EPSILON8_STOLE,CT_END_OF_MISSION,BIT_MICHAEL,CHAR_CRIS,4,CC_END_OF_MISSION_QUEUE_TIME,CC_END_OF_MISSION_QUEUE_TIME)
					bDoneCrisAngryPhonecall = TRUE
				ENDIF
			ENDIF
			IF bNoCashLeftInCar = FALSE	
				IF IS_VEHICLE_OK(vehPlayerCar)
					IF NOT IS_ENTITY_DEAD(objCashBag[0])                     //Working out how much cash to give player
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[0],vehPlayerCar)
							iTotalCash = iTotalCash + iCashInEachBag
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[1])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[1],vehPlayerCar)
							iTotalCash = iTotalCash + iCashInEachBag
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[2])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[2],vehPlayerCar)
							iTotalCash = iTotalCash + iCashInEachBag
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[3])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[3],vehPlayerCar)
							iTotalCash = iTotalCash + iCashInEachBag
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//iTotalCash = iTotalCash + iCashInEachBag
			ENDIF
			IF iTotalCash <> 0                                              //If cash is not 0 pay it into Michael's bank
				CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, iTotalCash)
			ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "STAT **** JUDAS - PLAYER STOLE MONEY AND ESCAPED ****")
			#ENDIF
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(EP8_MONEY_STOLEN) 
		ELSE
			IF bPlayerWentToTractor = TRUE                                 //Player went to tractor
				/*
				IF IS_PED_UNINJURED(pedEpsilonist)
					FORCE_PED_MOTION_STATE(pedEpsilonist,MS_ON_FOOT_RUN,FALSE,FAUS_DEFAULT)
					SET_PED_KEEP_TASK(pedEpsilonist,TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonist,<<-355.7390, -181.6352, 36.7355>>,PEDMOVEBLENDRATIO_RUN,-1,3.0,ENAV_DEFAULT)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonist,FALSE)
					SET_PED_AS_NO_LONGER_NEEDED(pedEpsilonist)
				ENDIF
				*/
				IF bBootOpen = TRUE
					SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
					SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
					bBootOpen = FALSE
				ENDIF
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "STAT **** PRAISE KIFFLOM - PLAYER ACCEPTED REWARD ****")
				#ENDIF
			ELSE                                                           //Player stood around watching the guy load the chopper
				IF bPlayerSkippedToEnd = FALSE	
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					/*
					REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 4)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, NULL, "CRIS")
					//CHAR_CALL_PLAYER_CELLPHONE(s_conversation_peds,CHAR_CRIS,"EPS8AU", "EPS8_CF3",CONV_PRIORITY_VERY_HIGH)
					REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EPSILON8_DONE,CT_END_OF_MISSION,BIT_MICHAEL,CHAR_CRIS,4,CC_END_OF_MISSION_QUEUE_TIME,CC_END_OF_MISSION_QUEUE_TIME)
					*/
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_STATIC_EMITTER_ENABLED("SE_LOS_SANTOS_EPSILONISM_BUILDING_01",TRUE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

PROC Script_Passed()
	
	IF bPlayerStoleMoney = TRUE	
		IF iEpsilonistsKilled = iEpSecBlipped
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "STAT **** CULT INTERVENTION - ALL EPSILONISTS KILLED ****")
			#ENDIF
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(EP8_SECURITY_WIPED_OUT) 
		ENDIF	
		TRIGGER_MUSIC_EVENT("EPS8_PASS")
      	g_savedGlobals.sRandomChars.g_bStoleEpsilonCash = TRUE
	ELSE
		TRIGGER_MUSIC_EVENT("EPS8_PASS")
		g_savedGlobals.sRandomChars.g_bStoleEpsilonCash = FALSE
	ENDIF
		
	//Set epsilon step stat
	INT iCurrent
	STAT_GET_INT(NUM_EPSILON_STEP,iCurrent)
	IF iCurrent < 20
		STAT_SET_INT(NUM_EPSILON_STEP,20)
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH20),20)
		CPRINTLN(debug_dan,"Epsilon progress:",20)
	ENDIF
	
	// [AG]: Achievement Unlocked
	AWARD_ACHIEVEMENT(ACH20) // Kifflom!

	Random_Character_Passed(CP_RAND_C_EPS8)
	bMissionPassed = TRUE
	Script_Cleanup()

ENDPROC

/// PURPOSE: //Load models and text
PROC LoadStuff()    
	
	REQUEST_VEHICLE_RECORDING(222,"Ep8Heli01")
	REQUEST_WAYPOINT_RECORDING("Eps8EPED")   
	REQUEST_WAYPOINT_RECORDING("Eps8LS01")   
	REQUEST_ANIM_DICT("rcmepsilonism8")
	REQUEST_MODEL(S_M_M_HIGHSEC_01)
	REQUEST_MODEL(A_M_Y_EPSILON_01)
	REQUEST_MODEL(SENTINEL)
	REQUEST_MODEL(MAVERICK)
	REQUEST_MODEL(LANDSTALKER)
	REQUEST_MODEL(TAILGATER)
	REQUEST_MODEL(P_LD_HEIST_BAG_S_1)
	
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC01")   
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC02")   
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC03")   
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC04")  
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC05")   
	REQUEST_WAYPOINT_RECORDING("Eps8TRAFFIC06")   

	REQUEST_MODEL(TAXI)
	//REQUEST_MODEL(PATRIOT)
	REQUEST_MODEL(SADLER)
	REQUEST_MODEL(HABANERO)
	REQUEST_MODEL(SCHAFTER2)
	REQUEST_MODEL(RUMPO)
	
	REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")	

	REQUEST_ADDITIONAL_TEXT("EPS8",MISSION_TEXT_SLOT)
	//REQUEST_ADDITIONAL_TEXT("EPS8AU",MISSION_DIALOGUE_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)     	 //wait for stuff we need right at the start
	//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
	OR NOT HAS_MODEL_LOADED(LANDSTALKER)
	OR NOT HAS_MODEL_LOADED(TAILGATER)
	OR NOT HAS_MODEL_LOADED(S_M_M_HIGHSEC_01)
		WAIT(0)
	ENDWHILE
	
	vCompoundBack = << -281.6395, -102.0944, 45.8373 >>  //back alley
	vCompoundFront = << -409.0169, -58.1734, 43.6077 >>
	
	vPosPlayerCar = <<-697.321533,39.040600,42.867363>>
	fHeadingPlayerCar = -66.988541
	
	vPosChopper = << -357.1071, -91.7128, 45.0982 >>
	fHeadingChopper = 8.7486

	vPosEpsilonCar[0] = << -686.5046, 43.9098, 42.2067 >>   //used
	vPosEpsilonCar[1] = << 188.3458, 169.2785, 104.3145 >>   
	vPosEpsilonCar[2] = << -410.1774, -68.8325, 42.4324 >>   //used
	vPosEpsilonCar[3] = << -656.5880, 56.5390, 42.7190 >>   //used //<< 271.0465, 144.4226, 103.3244 >> //<< 271.7032, 143.8821, 103.2951 >>
	vPosEpsilonCar[4] = << 299.5176, -11.6420, 76.1623 >>
	vPosEpsilonCar[5] = << 175.4031, 93.0495, 87.8098 >>    
	vPosEpsilonCar[6] = << 257.4567, 161.3084, 103.6166 >>  
	fHeadingEpsilonCar[0] = 296.8100  //used
	fHeadingEpsilonCar[1] = 162.1519
	fHeadingEpsilonCar[2] = 317.5537  //used
	fHeadingEpsilonCar[3] = 176.38631  //used          
	fHeadingEpsilonCar[4] = 157.9
	fHeadingEpsilonCar[5] = 340.5091 
	fHeadingEpsilonCar[6] = 249.2435

	vPosEpsilonHiSec[0] = <<-726.455505,33.335682,43.226952>>   //used keep     //Guy at west entrance of epsilon mansion  //<< -727.0070, 32.2846, 41.9410 >>
	vPosEpsilonHiSec[1] = << 222.1460, 206.6046, 104.4984 >>
	vPosEpsilonHiSec[2] = <<-666.551208,49.197594,42.071537>>   //used  keep    //Guy at east entrance of epsilon mansion  //<< -663.8454, 41.9318, 39.3568 >>
	vPosEpsilonHiSec[3] = << -379.9735, -73.8836, 44.6215 >>    //used
	vPosEpsilonHiSec[4] = << -378.7636, -86.7811, 44.6579 >>    //used
	vPosEpsilonHiSec[5] = << -374.7542, -81.9368, 44.6577 >>    //used
	vPosEpsilonHiSec[6] = << 248.7298, 109.6775, 101.4054 >>
	fHeadingEpsilonHiSec[0] = -147.809906     //used             //Guy at west entrance of epsilon mansion  //201.3041
	fHeadingEpsilonHiSec[1] = 129.6183
	fHeadingEpsilonHiSec[2] = -171.680267   //used              //Guy at east entrance of epsilon mansion  //262.2314
	fHeadingEpsilonHiSec[3] = 340.8513     //used
	fHeadingEpsilonHiSec[4] = 256.0816     //used
	fHeadingEpsilonHiSec[5] = 251.5665      //used
	fHeadingEpsilonHiSec[6] = 23.1111
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(LANDSTALKER,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SENTINEL2,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,TRUE)
	
	SET_PED_MODEL_IS_SUPPRESSED(A_F_Y_EPSILON_01,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_Y_EPSILON_01,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_Y_EPSILON_02,TRUE)

	SET_WANTED_LEVEL_MULTIPLIER(0.5)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-375.76, -101.27, 42.79>>, <<-344.30, -100.84, 48.77>>, FALSE)
	
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_CONTACT_TO_PHONEBOOK(CHAR_CRIS,MICHAEL_BOOK,FALSE)
	
ENDPROC

/// PURPOSE: //After Cutscene, Wait for the rest of the stuff to load (it probably has)
PROC WaitForLoad()         
	
	WHILE NOT HAS_MODEL_LOADED(S_M_M_HIGHSEC_01) 
	OR NOT HAS_MODEL_LOADED(A_M_Y_EPSILON_01)
	OR NOT HAS_MODEL_LOADED(SENTINEL)
	OR NOT HAS_MODEL_LOADED(MAVERICK)
	OR NOT HAS_MODEL_LOADED(P_LD_HEIST_BAG_S_1)
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(222,"Ep8Heli01")
	OR NOT HAS_ANIM_DICT_LOADED("rcmepsilonism8")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Eps8EPED")	
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Eps8LS01")
	OR NOT HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
		WAIT(0)
	ENDWHILE

ENDPROC

/// PURPOSE: //Spawn all the epsilon vehicles and peds, give them weapons etc 
PROC Init()  
	
	//Init helicopter and occupants
	vehEpsilonChopper = CREATE_VEHICLE(MAVERICK,vPosChopper, fHeadingChopper)
	//SET_VEHICLE_COLOURS(vehEpsilonChopper,23,23) 
	//SET_VEHICLE_EXTRA_COLOURS(vehEpsilonChopper,111,111)
	SET_VEHICLE_COLOURS(vehEpsilonChopper,iCol1,iCol1)
	SET_VEHICLE_EXTRA_COLOURS(vehEpsilonChopper,0,0)
	//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehEpsilonChopper,204, 255, 255 )
	SET_MODEL_AS_NO_LONGER_NEEDED(MAVERICK)
	pedPilot = CREATE_PED_INSIDE_VEHICLE(vehEpsilonChopper,PEDTYPE_MISSION,S_M_M_HIGHSEC_01)
	SET_PED_ACCURACY(pedPilot,iAccuracy)
	SET_PED_SHOOT_RATE(pedPilot,iShootRate)
	//SET_PED_COMP_ITEM_CURRENT_SP(pedPilot, COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	GIVE_WEAPON_TO_PED(pedPilot,WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedPilot,relEpsilon)
	SET_VEHICLE_DOORS_LOCKED(vehEpsilonChopper,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	SET_ENTITY_IS_TARGET_PRIORITY(pedPilot,TRUE)
	SET_PED_COMPONENT_VARIATION(pedPilot, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedPilot, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedPilot, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedPilot, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
	SET_PED_COMPONENT_VARIATION(pedPilot, INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)
	pedEpsilonist = CREATE_PED_INSIDE_VEHICLE(vehEpsilonChopper,PEDTYPE_MISSION,A_M_Y_EPSILON_01,VS_BACK_LEFT)
	SET_ENTITY_LOD_DIST(pedEpsilonist,100)
	SET_PED_ACCURACY(pedEpsilonist,iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonist,iShootRate)
	//SET_PED_COMP_ITEM_CURRENT_SP(pedEpsilonist, COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	/*
	SET_PED_COMPONENT_VARIATION(pedEpsilonist,PED_COMP_HEAD,1,1,0)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist,PED_COMP_HAIR,1,1,0)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist,PED_COMP_TORSO,1,0,0)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist,PED_COMP_LEG,0,1,0)
	*/
	SET_PED_COMPONENT_VARIATION(pedEpsilonist, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist, INT_TO_ENUM(PED_COMPONENT,1), 1, 0, 0) //(berd)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonist, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonist,relEpsilon)
	GIVE_WEAPON_TO_PED(pedEpsilonist,WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonist,WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
	SET_PED_KEEP_TASK(pedEpsilonist,TRUE)
	TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
	SET_PED_CAN_HEAD_IK(pedEpsilonist,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_AGGRESSIVE,TRUE)
	SET_PED_CAN_BE_TARGETTED(pedEpsilonist,FALSE)
	pedChopperShotgun = CREATE_PED_INSIDE_VEHICLE(vehEpsilonChopper,PEDTYPE_MISSION,S_M_M_HIGHSEC_01,VS_BACK_RIGHT)
	SET_PED_ACCURACY(pedChopperShotgun,iAccuracy - iHeliSniperAccModifier)
	SET_PED_SHOOT_RATE(pedChopperShotgun,iShootRate + iHeliSniperSRModifier)
	SET_PED_CONFIG_FLAG(pedChopperShotgun,PCF_DontBehaveLikeLaw,TRUE)
	/*
	GIVE_WEAPON_TO_PED(pedChopperShotgun,WEAPONTYPE_COMBATMG,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
	SET_CURRENT_PED_WEAPON(pedChopperShotgun,WEAPONTYPE_COMBATMG,TRUE)
	*/
	
	GIVE_WEAPON_TO_PED(pedChopperShotgun,WEAPONTYPE_ADVANCEDRIFLE,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_ADVANCEDRIFLE,WEAPONCOMPONENT_AT_AR_FLSH)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_ADVANCEDRIFLE,WEAPONCOMPONENT_AT_SCOPE_SMALL)
	SET_CURRENT_PED_WEAPON(pedChopperShotgun,WEAPONTYPE_ADVANCEDRIFLE,TRUE)
	
	/*
	GIVE_WEAPON_TO_PED(pedChopperShotgun,WEAPONTYPE_CARBINERIFLE,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_CARBINERIFLE,WEAPONCOMPONENT_AT_RAILCOVER_01)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_CARBINERIFLE,WEAPONCOMPONENT_AT_AR_FLSH)
	GIVE_WEAPON_COMPONENT_TO_PED(pedChopperShotgun,WEAPONTYPE_CARBINERIFLE,WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
	SET_CURRENT_PED_WEAPON(pedChopperShotgun,WEAPONTYPE_CARBINERIFLE,TRUE)
	*/
	SET_PED_KEEP_TASK(pedChopperShotgun,TRUE)
	SET_PED_COMBAT_RANGE(pedChopperShotgun,CR_FAR)
	SET_PED_HEARING_RANGE(pedChopperShotgun,400.0)
	SET_PED_SEEING_RANGE(pedChopperShotgun,400.0)
	SET_PED_ID_RANGE(pedChopperShotgun,400.0)
	SET_PED_FIRING_PATTERN(pedChopperShotgun,FIRING_PATTERN_FULL_AUTO) 
	SET_PED_COMBAT_ATTRIBUTES(pedChopperShotgun,CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedChopperShotgun,CA_LEAVE_VEHICLES,FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedChopperShotgun,relEpsilon)
	TASK_LOOK_AT_ENTITY(pedChopperShotgun,PLAYER_PED_ID(),-1)
	SET_ENTITY_IS_TARGET_PRIORITY(pedChopperShotgun,TRUE)
	pedEpsilonHiSecurity[5] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonChopper,PEDTYPE_MISSION,S_M_M_HIGHSEC_01,VS_FRONT_RIGHT)
	SET_PED_ACCURACY(pedEpsilonHiSecurity[5],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecurity[5],iShootRate)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecurity[5],WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecurity[5],WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[5],CA_BLIND_FIRE_IN_COVER,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecurity[5],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecurity[5],TRUE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[5],PCF_DontBehaveLikeLaw,TRUE)

	//Init first security car and occupants
	vehEpsilonCar[2] = CREATE_VEHICLE(SENTINEL,vPosEpsilonCar[2], fHeadingEpsilonCar[2])
	SET_VEHICLE_ON_GROUND_PROPERLY(vehEpsilonCar[2])
	//SET_VEHICLE_COLOURS(vehEpsilonCar[2],0,0)
	//SET_VEHICLE_EXTRA_COLOURS(vehEpsilonCar[2],0,0)
	SET_VEHICLE_COLOURS(vehEpsilonCar[2],iCol1,iCol1)
	SET_VEHICLE_EXTRA_COLOURS(vehEpsilonCar[2],0,0)
	//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehEpsilonCar[2],204, 255, 255 )
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehEpsilonCar[2],TRUE)
	SET_VEHICLE_MOD_KIT(vehEpsilonCar[2],0)
	SET_VEHICLE_WINDOW_TINT(vehEpsilonCar[2],2)
	SET_VEHICLE_MOD(vehEpsilonCar[2],MOD_SUSPENSION,1)
	SET_VEHICLE_MOD(vehEpsilonCar[2],MOD_ENGINE,1)
	SET_VEHICLE_MOD(vehEpsilonCar[2],MOD_BRAKES,0)
	TOGGLE_VEHICLE_MOD(vehEpsilonCar[2],MOD_TOGGLE_TURBO,TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehEpsilonCar[2], 0) 
	pedEpsilonHiSecDriver[2] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[2],PEDTYPE_MISSION,S_M_M_HIGHSEC_01)
	SET_DRIVER_ABILITY(pedEpsilonHiSecDriver[2],1)
	SET_DRIVER_RACING_MODIFIER(pedEpsilonHiSecDriver[2],1)
	SET_PED_ACCURACY(pedEpsilonHiSecDriver[2],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecDriver[2],iShootRate)
	SET_ENTITY_LOAD_COLLISION_FLAG(pedEpsilonHiSecDriver[2],TRUE)
	SET_PED_KEEP_TASK(pedEpsilonHiSecDriver[2],TRUE)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecDriver[2],WEAPONTYPE_PISTOL,-1,FALSE,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[2],CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[2],CA_BLIND_FIRE_IN_COVER,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecDriver[2],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecDriver[2],TRUE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecDriver[2],PCF_DontBehaveLikeLaw,TRUE)
	pedEpsilonHiSecShotgun[2] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[2],PEDTYPE_MISSION,S_M_M_HIGHSEC_01,VS_FRONT_RIGHT)
	SET_PED_ACCURACY(pedEpsilonHiSecShotgun[2],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecShotgun[2],iShootRate)
	SET_PED_KEEP_TASK(pedEpsilonHiSecShotgun[2],TRUE)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecShotgun[2],WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)	
	GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecShotgun[2],WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[2],CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[2],CA_AGGRESSIVE,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecShotgun[2],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecShotgun[2],TRUE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecShotgun[2],PCF_DontBehaveLikeLaw,TRUE)

	//Init second security car and occupants
	vehEpsilonCar[3] = CREATE_VEHICLE(SENTINEL,vPosEpsilonCar[3], fHeadingEpsilonCar[3])
	SET_VEHICLE_ON_GROUND_PROPERLY(vehEpsilonCar[3])
	//SET_VEHICLE_COLOURS(vehEpsilonCar[3],0,0)
	//SET_VEHICLE_EXTRA_COLOURS(vehEpsilonCar[3],0,0)
	SET_VEHICLE_COLOURS(vehEpsilonCar[3],iCol1,iCol1)
	SET_VEHICLE_EXTRA_COLOURS(vehEpsilonCar[3],0,0)
	//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehEpsilonCar[3],204, 255, 255 )
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehEpsilonCar[3],TRUE)
	SET_VEHICLE_MOD_KIT(vehEpsilonCar[3],0)
	SET_VEHICLE_WINDOW_TINT(vehEpsilonCar[3],2)
	SET_VEHICLE_MOD(vehEpsilonCar[3],MOD_SUSPENSION,1)
	SET_VEHICLE_MOD(vehEpsilonCar[3],MOD_ENGINE,1)
	SET_VEHICLE_MOD(vehEpsilonCar[3],MOD_BRAKES,0)
	TOGGLE_VEHICLE_MOD(vehEpsilonCar[3],MOD_TOGGLE_TURBO,TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehEpsilonCar[3], 0) 
	pedEpsilonHiSecDriver[3] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[3],PEDTYPE_MISSION,S_M_M_HIGHSEC_01)
	SET_DRIVER_ABILITY(pedEpsilonHiSecDriver[3],1)
	SET_DRIVER_RACING_MODIFIER(pedEpsilonHiSecDriver[3],1)
	SET_PED_ACCURACY(pedEpsilonHiSecDriver[3],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecDriver[3],iShootRate)
	SET_ENTITY_LOAD_COLLISION_FLAG(pedEpsilonHiSecDriver[3],TRUE)
	SET_PED_KEEP_TASK(pedEpsilonHiSecDriver[3],TRUE)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecDriver[3],WEAPONTYPE_PISTOL,-1,FALSE,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[3],CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[3],CA_BLIND_FIRE_IN_COVER,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecDriver[3],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecDriver[3],TRUE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecDriver[3],PCF_DontBehaveLikeLaw,TRUE)
	pedEpsilonHiSecShotgun[3] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[3],PEDTYPE_MISSION,S_M_M_HIGHSEC_01,VS_FRONT_RIGHT)
	SET_PED_ACCURACY(pedEpsilonHiSecShotgun[3],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecShotgun[3],iShootRate)
	SET_PED_KEEP_TASK(pedEpsilonHiSecShotgun[3],TRUE)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecShotgun[3],WEAPONTYPE_PISTOL,-1,FALSE,FALSE)	
	GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecShotgun[3],WEAPONTYPE_PISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[3],CA_DO_DRIVEBYS,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[3],CA_AGGRESSIVE,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecShotgun[3],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecShotgun[3],TRUE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecShotgun[3],PCF_DontBehaveLikeLaw,TRUE)
	
	//Init on-foot security guys
	IF IS_PED_UNINJURED(pedEpsilonHiSecurity[0])	
		SET_PED_ACCURACY(pedEpsilonHiSecurity[0],iAccuracy)
		SET_PED_SHOOT_RATE(pedEpsilonHiSecurity[0],iShootRate)
		GIVE_WEAPON_TO_PED(pedEpsilonHiSecurity[0],WEAPONTYPE_SMG,-1,FALSE,FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[0],CA_BLIND_FIRE_IN_COVER,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[0],CA_USE_VEHICLE,FALSE)
		TASK_LOOK_AT_ENTITY(pedEpsilonHiSecurity[0],PLAYER_PED_ID(),-1)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecurity[0],relEpsilon)
		SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecurity[0],TRUE)
		SET_PED_CAN_EVASIVE_DIVE(pedEpsilonHiSecurity[0],FALSE)
		SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[0],PCF_DontBehaveLikeLaw,TRUE)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[0], INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)
		//TASK_START_SCENARIO_IN_PLACE(pedEpsilonHiSecurity[0],"WORLD_HUMAN_GUARD_STAND")
	ENDIF
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecurity[2])	
		SET_PED_ACCURACY(pedEpsilonHiSecurity[2],iAccuracy)
		SET_PED_SHOOT_RATE(pedEpsilonHiSecurity[2],iShootRate)
		GIVE_WEAPON_TO_PED(pedEpsilonHiSecurity[2],WEAPONTYPE_SMG,-1,FALSE,FALSE)
		GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecurity[2],WEAPONTYPE_SMG,WEAPONCOMPONENT_AT_AR_FLSH)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[2],CA_BLIND_FIRE_IN_COVER,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[2],CA_USE_VEHICLE,FALSE)
		TASK_LOOK_AT_ENTITY(pedEpsilonHiSecurity[2],PLAYER_PED_ID(),-1)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecurity[2],relEpsilon)
		SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecurity[2],TRUE)
		SET_PED_CAN_EVASIVE_DIVE(pedEpsilonHiSecurity[2],FALSE)
		SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[2],PCF_DontBehaveLikeLaw,TRUE)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[2], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[2], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
		//TASK_START_SCENARIO_IN_PLACE(pedEpsilonHiSecurity[2],"WORLD_HUMAN_GUARD_STAND")
	ENDIF
	
	pedEpsilonHiSecurity[3] = CREATE_PED(PEDTYPE_MISSION,S_M_M_HIGHSEC_01,vPosEpsilonHiSec[3],fHeadingEpsilonHiSec[3])
	SET_PED_ACCURACY(pedEpsilonHiSecurity[3],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecurity[3],iShootRate)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecurity[3],WEAPONTYPE_SMG,-1,FALSE,FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[3],CA_BLIND_FIRE_IN_COVER,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[3],CA_USE_VEHICLE,FALSE)
	TASK_LOOK_AT_ENTITY(pedEpsilonHiSecurity[3],PLAYER_PED_ID(),-1)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecurity[3],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecurity[3],TRUE)
	//TASK_START_SCENARIO_IN_PLACE(pedEpsilonHiSecurity[3],"WORLD_HUMAN_GUARD_STAND")
	TASK_PLAY_ANIM(pedEpsilonHiSecurity[3],"rcmepsilonism8","security_idle",4,-4,-1,AF_LOOPING)
	SET_PED_CAN_EVASIVE_DIVE(pedEpsilonHiSecurity[3],FALSE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[3],PCF_DontBehaveLikeLaw,TRUE)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[3], INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[3], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[3], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[3], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)

	pedEpsilonHiSecurity[4] = CREATE_PED(PEDTYPE_MISSION,S_M_M_HIGHSEC_01,vPosEpsilonHiSec[4],fHeadingEpsilonHiSec[4])
	SET_PED_ACCURACY(pedEpsilonHiSecurity[4],iAccuracy)
	SET_PED_SHOOT_RATE(pedEpsilonHiSecurity[4],iShootRate)
	GIVE_WEAPON_TO_PED(pedEpsilonHiSecurity[4],WEAPONTYPE_SMG,-1,FALSE,FALSE)
	GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecurity[4],WEAPONTYPE_SMG,WEAPONCOMPONENT_AT_AR_FLSH)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[4],CA_AGGRESSIVE,TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[4],CA_USE_VEHICLE,FALSE)
	TASK_LOOK_AT_ENTITY(pedEpsilonHiSecurity[4],PLAYER_PED_ID(),-1)
	SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecurity[4],relEpsilon)
	SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecurity[4],TRUE)
	//TASK_START_SCENARIO_IN_PLACE(pedEpsilonHiSecurity[4],"WORLD_HUMAN_GUARD_STAND")
	TASK_PLAY_ANIM(pedEpsilonHiSecurity[4],"rcmepsilonism8","security_idle",4,-4,-1,AF_LOOPING)
	SET_PED_CAN_EVASIVE_DIVE(pedEpsilonHiSecurity[4],FALSE)
	SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[4],PCF_DontBehaveLikeLaw,TRUE)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[4], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[4], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[4], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[4], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
	SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecurity[4], INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)

	
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
		SET_DRIVER_ABILITY(pedEpsilonHiSecDriver[0],1)
		SET_DRIVER_RACING_MODIFIER(pedEpsilonHiSecDriver[0],1)
		SET_PED_ACCURACY(pedEpsilonHiSecDriver[0],iAccuracy)
		SET_PED_SHOOT_RATE(pedEpsilonHiSecDriver[0],iShootRate)
		GIVE_WEAPON_TO_PED(pedEpsilonHiSecDriver[0],WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[0],CA_BLIND_FIRE_IN_COVER,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[0],CA_DO_DRIVEBYS,TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecDriver[0],relEpsilon)
		SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecDriver[0],TRUE)
		SET_PED_CONFIG_FLAG(pedEpsilonHiSecDriver[0],PCF_DontBehaveLikeLaw,TRUE)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecDriver[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecDriver[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecDriver[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecDriver[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecDriver[0], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
	ENDIF
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecShotgun[0])
		SET_PED_ACCURACY(pedEpsilonHiSecShotgun[0],iAccuracy)
		SET_PED_SHOOT_RATE(pedEpsilonHiSecShotgun[0],iShootRate)
		GIVE_WEAPON_TO_PED(pedEpsilonHiSecShotgun[0],WEAPONTYPE_MICROSMG,-1,FALSE,FALSE)
		GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecShotgun[0],WEAPONTYPE_MICROSMG,WEAPONCOMPONENT_AT_PI_FLSH)
		GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonHiSecShotgun[0],WEAPONTYPE_MICROSMG,WEAPONCOMPONENT_AT_SCOPE_MACRO)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[0],CA_DO_DRIVEBYS,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[0],CA_AGGRESSIVE,TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedEpsilonHiSecShotgun[0],relEpsilon)
		SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonHiSecShotgun[0],TRUE)
		SET_PED_CONFIG_FLAG(pedEpsilonHiSecShotgun[0],PCF_DontBehaveLikeLaw,TRUE)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecShotgun[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecShotgun[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecShotgun[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecShotgun[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(pedEpsilonHiSecShotgun[0], INT_TO_ENUM(PED_COMPONENT,11), 0, 1, 0) //(jbib)
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 8, pedEpsilonist, "EPSGUARD")
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedPilot, "EPSPILOT")
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedEpsilonHiSecurity[0], "EPSGUARD2")
	
	//ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedEpsilonHiSecurity[5], "EPSGUARD")
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, pedEpsilonHiSecurity[3], "EPSGUARD7")  //EPSGUARD4
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, pedEpsilonHiSecurity[4], "EPSGUARD8")  //EPSGUARD3
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_FRANKLIN,MICHAEL_BOOK,FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_MARNIE, MICHAEL_BOOK, FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY_BOSTON, MICHAEL_BOOK, FALSE)
	
	//Give Michael a headset
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	
	//Block police helicopters or it can get confusing
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER,TRUE)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER,TRUE)
	
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_AMBULANCE_DEPARTMENT,TRUE)
	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_FIRE_DEPARTMENT,TRUE)
	
	iTimerMissionInit = GET_GAME_TIMER()

ENDPROC

PROC LockDoors()
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-697.94, 48.35, 44.30>>,15.0,prop_epsilon_door_r)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_epsilon_door_r,<<-697.94, 48.35, 44.30>>,TRUE,0)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-700.17, 47.31, 44.30>>,15.0,prop_epsilon_door_l)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_epsilon_door_l,<<-700.17, 47.31, 44.30>>,TRUE,0)                             
	ENDIF

ENDPROC

PROC DoTraffic()

	CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()),200,TRUE,TRUE)

	IF HAS_MODEL_LOADED(TAXI)
	//AND HAS_MODEL_LOADED(PATRIOT)
	AND HAS_MODEL_LOADED(SADLER)
	AND HAS_MODEL_LOADED(HABANERO)
	AND HAS_MODEL_LOADED(SCHAFTER2)
	AND HAS_MODEL_LOADED(RUMPO)

		vTrafficRoute1[0] = << -131.8380, -84.8160, 54.8058 >>
		vTrafficRoute1[1] = << -131.8380, -84.8160, 54.8058 >>
		vTrafficRoute1[2] = << -202.8393, -60.2254, 49.8713 >>
		vTrafficRoute1[3] = << -283.2474, -32.2227, 48.2857 >>
		vTrafficRoute1[4] = << -372.0164, -1.9755, 45.9995 >>
		vTrafficRoute1[5] = << -463.3591, 7.4364, 44.6764 >>
		vTrafficRoute1[6] = << -536.8303, 12.2933, 43.2611 >>

		fHeadingTrafficRoute1[0] = 71.7829
		fHeadingTrafficRoute1[1] = 71.7829
		fHeadingTrafficRoute1[2] = 70.6852
		fHeadingTrafficRoute1[3] = 70.5582
		fHeadingTrafficRoute1[4] = 75.2358
		fHeadingTrafficRoute1[5] = 88.0273
		fHeadingTrafficRoute1[6] = 86.8248
		
		vTrafficRoute2[0] = << -312.1015, 130.6894, 66.5865 >>
		vTrafficRoute2[1] = << -345.6338, 129.7367, 65.7121 >>
		vTrafficRoute2[2] = << -393.3472, 75.7086, 58.9759 >>
		vTrafficRoute2[3] = << -394.0365, 33.2626, 47.2826 >>
		vTrafficRoute2[4] = << -432.5279, 11.5918, 45.1093 >>
		vTrafficRoute2[5] = << -473.8784, 13.9020, 44.3884 >>
		vTrafficRoute2[6] = << -515.9524, 16.8126, 43.5537 >>

		fHeadingTrafficRoute2[0] = 91.2112
		fHeadingTrafficRoute2[1] = 92.7461
		fHeadingTrafficRoute2[2] = 179.5078
		fHeadingTrafficRoute2[3] = 174.2351
		fHeadingTrafficRoute2[4] = 87.2938
		fHeadingTrafficRoute2[5] = 85.9476
		fHeadingTrafficRoute2[6] = 86.5195
		
		vTrafficRoute3[0] = << -821.8678, -15.2100, 38.7738 >>
		vTrafficRoute3[1] = << -762.9101, -61.3690, 36.8265 >>
		vTrafficRoute3[2] = << -683.5615, -22.2863, 37.2221 >>
		vTrafficRoute3[3] = << -598.2457, -13.4337, 42.8497 >>
		vTrafficRoute3[4] = << -566.7648, -39.6658, 41.7440 >>
		vTrafficRoute3[5] = << -547.0430, -87.3705, 39.7725 >>
		vTrafficRoute3[6] = << -542.4178, -145.0670, 37.4423 >>

		fHeadingTrafficRoute3[0] = 213.1008
		fHeadingTrafficRoute3[1] = 292.6512
		fHeadingTrafficRoute3[2] = 280.8633
		fHeadingTrafficRoute3[3] = 270.1932
		fHeadingTrafficRoute3[4] = 162.4637
		fHeadingTrafficRoute3[5] = 220.7259
		fHeadingTrafficRoute3[6] = 109.6765
		
		vTrafficRoute4[0] = << -538.8824, 2.5863, 43.2495 >>
		vTrafficRoute4[1] = << -510.9343, 0.7568, 43.7336 >>
		vTrafficRoute4[2] = << -468.7612, -1.3575, 44.5868 >>
		vTrafficRoute4[3] = << -419.5594, -4.5629, 45.5167 >>
		vTrafficRoute4[4] = << -373.0446, -11.2775, 46.0324 >>
		vTrafficRoute4[5] = << -278.3581, -44.5050, 48.5490 >>
		vTrafficRoute4[6] = << -211.5438, -68.0673, 49.5385 >>

		fHeadingTrafficRoute4[0] = 264.3932
		fHeadingTrafficRoute4[1] = 265.6151
		fHeadingTrafficRoute4[2] = 265.8879 
		fHeadingTrafficRoute4[3] = 267.9286
		fHeadingTrafficRoute4[4] = 251.6172 
		fHeadingTrafficRoute4[5] = 254.6602
		fHeadingTrafficRoute4[6] = 252.3606
		
		vTrafficRoute5[0] = << -669.2421, -7.4354, 37.8635 >>
		vTrafficRoute5[1] = << -700.1176, -17.5754, 36.9578 >>
		vTrafficRoute5[2] = << -727.3986, -31.1413, 36.8838 >>
		vTrafficRoute5[3] = << -775.4231, -56.9022, 36.8581 >>
		vTrafficRoute5[4] = << -813.0173, -76.7235, 36.8717 >>
		vTrafficRoute5[5] = << -857.0458, -99.6788, 36.9241 >>
		vTrafficRoute5[6] = << -911.4196, -127.5572, 36.8240 >>

		fHeadingTrafficRoute5[0] = 101.9021
		fHeadingTrafficRoute5[1] = 116.4117
		fHeadingTrafficRoute5[2] = 116.5154
		fHeadingTrafficRoute5[3] = 117.9260 
		fHeadingTrafficRoute5[4] = 117.6888 
		fHeadingTrafficRoute5[5] = 117.7467
		fHeadingTrafficRoute5[6] = 115.4069
		
		vTrafficRoute6[0] = << -808.2740, -46.2696, 36.8528 >>
		vTrafficRoute6[1] = << -823.1899, -74.6149, 36.7824 >>
		vTrafficRoute6[2] = << -862.0953, -95.4273, 36.8721 >>
		vTrafficRoute6[3] = << -913.0912, -122.3970, 36.7913 >>
		vTrafficRoute6[4] = << -959.4056, -145.7552, 36.7732 >>
		vTrafficRoute6[5] = << -1020.0234, -163.9246, 36.7896 >>
		vTrafficRoute6[6] = << -1071.1110, -164.6870, 36.7161 >>

		fHeadingTrafficRoute6[0] = 206.0301
		fHeadingTrafficRoute6[1] = 119.7110 
		fHeadingTrafficRoute6[2] = 117.2803
		fHeadingTrafficRoute6[3] = 117.1178
		fHeadingTrafficRoute6[4] = 115.7739
		fHeadingTrafficRoute6[5] = 118.1293 
		fHeadingTrafficRoute6[6] = 66.6215
		
		MODEL_NAMES modVeh
		
		INT iRandom

		FLOAT fZ

		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC01")
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute1[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute1[iTraffic],50)	
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute1[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)	
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute1[iTraffic],8)	
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = SADLER
						ELIF iRandom = 2
							modVeh = SCHAFTER2
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 1")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute1[iTraffic],fZ)
							fZ = vTrafficRoute1[iTraffic].z
						ENDIF
						IF iTraffic > 4	//test
						OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vParkPos,150)	
							vehTrafficRoute1[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute1[iTraffic].x,vTrafficRoute1[iTraffic].y,fZ>>, fHeadingTrafficRoute1[iTraffic])
							pedTrafficRoute1[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute1[iTraffic])
							SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute1[iTraffic])
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute1[iTraffic],vehTrafficRoute1[iTraffic],"Eps8TRAFFIC01",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute1[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute1[iTraffic])	
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute1[iTraffic]) > 70
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute1[iTraffic],70)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute1[iTraffic])
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 1")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute1[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute1[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute1[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 1")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute1[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute1[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC02")
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute2[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute2[iTraffic],50)		
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute2[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)		
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute2[iTraffic],8)	
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = TAXI
						ELIF iRandom = 2
							modVeh = SADLER
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 2")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute2[iTraffic],fZ)
							fZ = vTrafficRoute2[iTraffic].z
						ENDIF
						vehTrafficRoute2[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute2[iTraffic].x,vTrafficRoute2[iTraffic].y,fZ>>, fHeadingTrafficRoute2[iTraffic])
						pedTrafficRoute2[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute2[iTraffic])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute2[iTraffic])
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute2[iTraffic],vehTrafficRoute2[iTraffic],"Eps8TRAFFIC02",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute2[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute2[iTraffic])	
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute2[iTraffic]) > 70
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute2[iTraffic],70)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute2[iTraffic])
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 2")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute2[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute2[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute2[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 2")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute2[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute2[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC03")	
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute3[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute3[iTraffic],50)	
				AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute3[iTraffic],220)		
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute3[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)		
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute3[iTraffic],8)		
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = SCHAFTER2
						ELIF iRandom = 2
							modVeh = RUMPO
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 3")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute3[iTraffic],fZ)
							fZ = vTrafficRoute3[iTraffic].z
						ENDIF
						vehTrafficRoute3[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute3[iTraffic].x,vTrafficRoute3[iTraffic].y,fZ>>, fHeadingTrafficRoute3[iTraffic])
						pedTrafficRoute3[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute3[iTraffic])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute3[iTraffic])
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute3[iTraffic],vehTrafficRoute3[iTraffic],"Eps8TRAFFIC03",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute3[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute3[iTraffic])	
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute3[iTraffic]) > 70
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute3[iTraffic],70)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute3[iTraffic])
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 3")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute3[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute3[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute3[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 3")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute3[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute3[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC04")	
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute4[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute4[iTraffic],50)		
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute4[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)		
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute4[iTraffic],8)	
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = SADLER
						ELIF iRandom = 2
							modVeh = SADLER
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 4")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute4[iTraffic],fZ)
							fZ = vTrafficRoute4[iTraffic].z
						ENDIF
						vehTrafficRoute4[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute4[iTraffic].x,vTrafficRoute4[iTraffic].y,fZ>>, fHeadingTrafficRoute4[iTraffic])
						pedTrafficRoute4[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute4[iTraffic])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute4[iTraffic])
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute4[iTraffic],vehTrafficRoute4[iTraffic],"Eps8TRAFFIC04",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute4[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute4[iTraffic])	
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute4[iTraffic]) > 70
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute4[iTraffic],70)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute4[iTraffic])
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 4")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute4[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute4[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute4[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 4")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute4[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute4[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC05")	
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute5[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute5[iTraffic],50)	
				AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute5[iTraffic],170)		
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute5[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)		
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute5[iTraffic],8)	
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = SCHAFTER2
						ELIF iRandom = 2
							modVeh = SADLER
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 5")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute5[iTraffic],fZ)
							fZ = vTrafficRoute5[iTraffic].z
						ENDIF
						vehTrafficRoute5[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute5[iTraffic].x,vTrafficRoute5[iTraffic].y,fZ>>, fHeadingTrafficRoute5[iTraffic])
						pedTrafficRoute5[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute5[iTraffic])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute5[iTraffic])
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute5[iTraffic],vehTrafficRoute5[iTraffic],"Eps8TRAFFIC05",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute5[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute5[iTraffic])	
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute5[iTraffic]) > 40
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute5[iTraffic],60)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute5[iTraffic])
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 5")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute5[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute5[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute5[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 5")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute5[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute5[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC06")	
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute6[iTraffic])	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute6[iTraffic],50)	
				AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute6[iTraffic],170)		
				AND NOT IS_POSITION_OCCUPIED(vTrafficRoute6[iTraffic],15,FALSE,TRUE,FALSE,FALSE,FALSE)		
					IF NOT IS_SPHERE_VISIBLE(vTrafficRoute6[iTraffic],8)	
						iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
						IF iRandom = 0
							modVeh = TAXI
						ELIF iRandom = 1
							modVeh = SADLER
						ELIF iRandom = 2
							modVeh = SADLER
						ELIF iRandom = 3
							modVeh = HABANERO
						ELIF iRandom = 4
							modVeh = SCHAFTER2
						ELSE
							modVeh = RUMPO
						ENDIF
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 6")
						#ENDIF
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vTrafficRoute6[iTraffic],fZ)
							fZ = vTrafficRoute6[iTraffic].z
						ENDIF
						vehTrafficRoute6[iTraffic] = CREATE_VEHICLE(modVeh,<<vTrafficRoute6[iTraffic].x,vTrafficRoute6[iTraffic].y,fZ>>, fHeadingTrafficRoute6[iTraffic])
						pedTrafficRoute6[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute6[iTraffic])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute6[iTraffic])
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute6[iTraffic],vehTrafficRoute6[iTraffic],"Eps8TRAFFIC06",DRIVINGMODE_STOPFORCARS_STRICT,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute6[iTraffic])	
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute6[iTraffic])
						IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute6[iTraffic]) > 40
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute6[iTraffic],60)	
								IF IS_ENTITY_OCCLUDED(vehTrafficRoute6[iTraffic])	
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 6")
									#ENDIF
									SAFE_DELETE_PED(pedTrafficRoute6[iTraffic])
									SAFE_DELETE_VEHICLE(vehTrafficRoute6[iTraffic])
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_OCCLUDED(vehTrafficRoute6[iTraffic])
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 6")
							#ENDIF
							SAFE_DELETE_PED(pedTrafficRoute6[iTraffic])
							SAFE_DELETE_VEHICLE(vehTrafficRoute6[iTraffic])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		++iTraffic
		IF iTraffic > 6
			iTraffic = 0
		ENDIF
	
	ENDIF
	
ENDPROC

PROC CleanupTraffic()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(TAXI)
	//SET_MODEL_AS_NO_LONGER_NEEDED(PATRIOT)
	SET_MODEL_AS_NO_LONGER_NEEDED(SADLER)
	SET_MODEL_AS_NO_LONGER_NEEDED(HABANERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(SCHAFTER2)
	SET_MODEL_AS_NO_LONGER_NEEDED(RUMPO)
	SAFE_RELEASE_PED(pedTrafficRoute1[0])
	SAFE_RELEASE_PED(pedTrafficRoute1[1])
	SAFE_RELEASE_PED(pedTrafficRoute1[2])
	SAFE_RELEASE_PED(pedTrafficRoute1[3])
	SAFE_RELEASE_PED(pedTrafficRoute1[4])
	SAFE_RELEASE_PED(pedTrafficRoute1[5])
	SAFE_RELEASE_PED(pedTrafficRoute1[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[6])
	SAFE_RELEASE_PED(pedTrafficRoute2[0])
	SAFE_RELEASE_PED(pedTrafficRoute2[1])
	SAFE_RELEASE_PED(pedTrafficRoute2[2])
	SAFE_RELEASE_PED(pedTrafficRoute2[3])
	SAFE_RELEASE_PED(pedTrafficRoute2[4])
	SAFE_RELEASE_PED(pedTrafficRoute2[5])
	SAFE_RELEASE_PED(pedTrafficRoute2[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[6])
	SAFE_RELEASE_PED(pedTrafficRoute3[0])
	SAFE_RELEASE_PED(pedTrafficRoute3[1])
	SAFE_RELEASE_PED(pedTrafficRoute3[2])
	SAFE_RELEASE_PED(pedTrafficRoute3[3])
	SAFE_RELEASE_PED(pedTrafficRoute3[4])
	SAFE_RELEASE_PED(pedTrafficRoute3[5])
	SAFE_RELEASE_PED(pedTrafficRoute3[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute3[6])
	SAFE_RELEASE_PED(pedTrafficRoute4[0])
	SAFE_RELEASE_PED(pedTrafficRoute4[1])
	SAFE_RELEASE_PED(pedTrafficRoute4[2])
	SAFE_RELEASE_PED(pedTrafficRoute4[3])
	SAFE_RELEASE_PED(pedTrafficRoute4[4])
	SAFE_RELEASE_PED(pedTrafficRoute4[5])
	SAFE_RELEASE_PED(pedTrafficRoute4[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute4[6])
	SAFE_RELEASE_PED(pedTrafficRoute5[0])
	SAFE_RELEASE_PED(pedTrafficRoute5[1])
	SAFE_RELEASE_PED(pedTrafficRoute5[2])
	SAFE_RELEASE_PED(pedTrafficRoute5[3])
	SAFE_RELEASE_PED(pedTrafficRoute5[4])
	SAFE_RELEASE_PED(pedTrafficRoute5[5])
	SAFE_RELEASE_PED(pedTrafficRoute5[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute5[6])
	SAFE_RELEASE_PED(pedTrafficRoute6[0])
	SAFE_RELEASE_PED(pedTrafficRoute6[1])
	SAFE_RELEASE_PED(pedTrafficRoute6[2])
	SAFE_RELEASE_PED(pedTrafficRoute6[3])
	SAFE_RELEASE_PED(pedTrafficRoute6[4])
	SAFE_RELEASE_PED(pedTrafficRoute6[5])
	SAFE_RELEASE_PED(pedTrafficRoute6[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute6[6])
	
ENDPROC


/// PURPOSE: //Heli flies off
PROC PlayerGaveMoneyHeliFlyAway()   

	SEQUENCE_INDEX seqHeliFlyAway

	SAFE_REMOVE_BLIP(blipEscort)
	
	IF IS_PED_UNINJURED(pedEpsilonist)
		CLEAR_PED_TASKS_IMMEDIATELY(pedEpsilonist)
	ENDIF
	
	IF IS_VEHICLE_OK(vehEpsilonChopper)
		IF IS_PED_UNINJURED(pedPilot)	
			IF bCutsceneChopperTaskGiven = FALSE
				SET_VEHICLE_ENGINE_ON(vehEpsilonChopper,TRUE,TRUE)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
				ENDIF
				iZ = ROUND(vPlayer.z + 10.0)
				OPEN_SEQUENCE_TASK(seqHeliFlyAway)	
					TASK_HELI_MISSION(NULL,vehEpsilonChopper,NULL,NULL,<< -582.5234, -363.9155, 498.4256 >>,MISSION_GOTO,200.0,2.0,130.0071,100,30)
					TASK_HELI_MISSION(NULL,vehEpsilonChopper,NULL,NULL,<< -1352.7047, -2861.1943, 498.4256 >>,MISSION_GOTO,200.0,2.0,183.7912,100,30)
				CLOSE_SEQUENCE_TASK(seqHeliFlyAway)
				TASK_PERFORM_SEQUENCE(pedPilot,seqHeliFlyAway)
				CLEAR_SEQUENCE_TASK(seqHeliFlyAway)
				SET_PED_KEEP_TASK(pedPilot,TRUE)
				bCutsceneChopperTaskGiven = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC DO_LOAD_HELI()

	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SET_PED_INCREASED_AVOIDANCE_RADIUS(PLAYER_PED_ID())
	ENDIF
	
	//VECTOR vRot
	
	SEQUENCE_INDEX seqGuard
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])
	AND IS_ENTITY_ALIVE(vehPlayerCar)
	AND IS_ENTITY_ALIVE(vehEpsilonChopper)

		SWITCH eLOAD_HELI
		
			CASE LOAD_HELI_INIT
				
				OPEN_SEQUENCE_TASK(seqGuard)
					TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-357.75, -87.47, 45.49>>,PEDMOVEBLENDRATIO_WALK,-1,0.25,ENAV_ACCURATE_WALKRUN_START,70.00)
					//IF IS_ENTITY_ALIVE(vehPlayerCar)
					//	TASK_GO_TO_ENTITY(NULL,vehPlayerCar,-1,8,PEDMOVEBLENDRATIO_WALK,3)
					//ENDIF
					IF IS_ENTITY_ALIVE(vehPlayerCar)	
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-3.2,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.4,ENAV_ACCURATE_WALKRUN_START,140.8925)
					ENDIF
					TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_GUARD_STAND",0,TRUE)
				CLOSE_SEQUENCE_TASK(seqGuard)
				TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqGuard)
				CLEAR_SEQUENCE_TASK(seqGuard)
				
				eLOAD_HELI = GET_IN_HELI
				//eLOAD_HELI = WALK_TO_CAR  //UNCOMMENT FOR LOADING HELI SYNCED SCENE

			BREAK
			
			CASE WALK_TO_CAR
				/*
				IF IS_ENTITY_IN_RANGE_COORDS(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-3.2,0.0>>),1)	
				AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK			
					IF iBagsTakenFromCar = 0	
						TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_TAG_SYNC_OUT | AF_TAG_SYNC_CONTINUOUS | AF_TAG_SYNC_IN)
					ELSE	
						TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_TAG_SYNC_OUT | AF_TAG_SYNC_CONTINUOUS | AF_TAG_SYNC_IN)
					ENDIF
					eLOAD_HELI = TAKE_BAG
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK	
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-3.2,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.4,ENAV_DEFAULT,GET_ENTITY_HEADING(vehPlayerCar))  //<<0.0,-2.6,0.0>>
					ENDIF
				ENDIF	
				*/
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-371.399628,-88.471542,44.658291>>, <<-378.745514,-85.821953,48.210369>>, 5.750000)
				AND IS_SPHERE_VISIBLE(<<-378.46, -88.44, 45.66>>,2)	
				AND NOT IS_ENTITY_ON_SCREEN(vehPlayerCar)	
				AND IS_PED_FACING_PED(PLAYER_PED_ID(),pedEpsilonist,120)	
					issLoadHeli = CREATE_SYNCHRONIZED_SCENE(<<-356.91, -89.03, 45.875>>,<<0.0, 0.0, -120>>) //336.09   //z 45.75
					//vRot = GET_ENTITY_ROTATION(vehEpsilonChopper)
					//issLoadHeli = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehEpsilonChopper),<<vRot.x,vRot.y,vRot.z - 120>>)
					TASK_SYNCHRONIZED_SCENE(pedEpsilonHiSecurity[5], issLoadHeli, "rcmepsilonism8", "grab_all_4_bags_carrier", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(vehPlayerCar, issLoadHeli, "grab_all_4_bags_car","rcmepsilonism8",  INSTANT_BLEND_IN, -1.5)
					
					//SET_SYNCHRONIZED_SCENE_RATE(issLoadHeli,0)
					eLOAD_HELI = TAKE_BAG
					
					DETACH_ENTITY(objCashBag[0])
					DETACH_ENTITY(objCashBag[1])
					DETACH_ENTITY(objCashBag[2])
					DETACH_ENTITY(objCashBag[3])
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCashBag[0],issLoadHeli,"base_bag_1","rcmepsilonism8",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCashBag[1],issLoadHeli,"base_bag_2","rcmepsilonism8",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCashBag[2],issLoadHeli,"base_bag_3","rcmepsilonism8",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objCashBag[3],issLoadHeli,"base_bag_4","rcmepsilonism8",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
					
					SET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli,0.098)
					
					/*
					SET_ENTITY_COORDS(objCashBag[0],<<-364.871, -84.4189, 45.3012>>)
					SET_ENTITY_ROTATION(objCashBag[0],<<-14.2576, 65.8288, 162.947>>)
					FREEZE_ENTITY_POSITION(objCashBag[0],TRUE)
					SET_ENTITY_COORDS(objCashBag[1],<<-364.895, -84.1603, 45.3983>>)
					SET_ENTITY_ROTATION(objCashBag[1],<<-22.5393, 67.4194, 171.118>>)
					FREEZE_ENTITY_POSITION(objCashBag[1],TRUE)
					SET_ENTITY_COORDS(objCashBag[2],<<-364.748, -83.8787, 45.4493 >>)
					SET_ENTITY_ROTATION(objCashBag[2],<<-41.7808, 62.5704, -173.08>>)
					FREEZE_ENTITY_POSITION(objCashBag[2],TRUE)
					SET_ENTITY_COORDS(objCashBag[3],<<-364.508, -83.818, 45.2875>>)
					SET_ENTITY_ROTATION(objCashBag[3],<<-9.77984, 56.2112, 153.408>>)
					FREEZE_ENTITY_POSITION(objCashBag[3],TRUE)
					*/
					
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[0])),GET_ENTITY_ROTATION(objCashBag[0]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[1])),GET_ENTITY_ROTATION(objCashBag[1]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[2])),GET_ENTITY_ROTATION(objCashBag[2]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[3],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[3])),GET_ENTITY_ROTATION(objCashBag[3]))
					
					//TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-3.2,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.4,ENAV_DEFAULT,GET_ENTITY_HEADING(vehPlayerCar))    //<<0.0,-2.6,0.0>>
					
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,-1,<<0,0,0>>,GET_ENTITY_ROTATION(objCashBag[0]),FALSE,FALSE,FALSE,FALSE,EULER_YXZ,FALSE)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,-1,<<0,0,0>>,GET_ENTITY_ROTATION(objCashBag[0]),FALSE,FALSE,FALSE,FALSE,EULER_YXZ,FALSE)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,-1,<<0,0,0>>,GET_ENTITY_ROTATION(objCashBag[0]),FALSE,FALSE,FALSE,FALSE,EULER_YXZ,FALSE)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[3],vehPlayerCar,-1,<<0,0,0>>,GET_ENTITY_ROTATION(objCashBag[0]),FALSE,FALSE,FALSE,FALSE,EULER_YXZ,FALSE)
				
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[0])),GET_ENTITY_ROTATION(objCashBag[0]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[1])),GET_ENTITY_ROTATION(objCashBag[1]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[2])),GET_ENTITY_ROTATION(objCashBag[2]))
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[3],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[3])),GET_ENTITY_ROTATION(objCashBag[3]))
					
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[0])),<<0,0,0>>)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[1])),<<0,0,0>>)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[2])),<<0,0,0>>)
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[3],vehPlayerCar,-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlayerCar,GET_ENTITY_COORDS(objCashBag[3])),<<0,0,0>>)
				
				ENDIF
				
			BREAK
			
			CASE TAKE_BAG
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(issLoadHeli)
					
					//0.126 pickup bag 1
					//0.237 drop off bag 1
					//0.347 pickup bag 2
					//0.455 drop off bag 2
					//0.563 pickup bag 3
					//0.678 drop off bag 3
					//0.786 pick up bag 4
					//0.901 drop off bag 4
				
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.126
					AND GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) < 0.237
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5])
							ATTACH_ENTITY_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<< 0.25, 0.075, -0.3 >>,<< 50, 50, -135 >>)  //<<0.1,-0.35,-0.35>>
							FREEZE_ENTITY_POSITION(objCashBag[0],FALSE)
							PRINTVECTOR(GET_ENTITY_COORDS(objCashBag[0]))
							PRINTVECTOR(GET_ENTITY_ROTATION(objCashBag[0]))
							PRINTNL()
						ELSE
							/*	
							SET_SYNCHRONIZED_SCENE_RATE(issLoadHeli,0.025)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LT) 
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) 
									fy = fy + 0.025	
								ENDIF
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) 
									fy = fy - 0.025		
								ENDIF
								DETACH_ENTITY(objCashBag[0])
								ATTACH_ENTITY_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<fy,fx,fZ2>>,<< 50, 50, -135 >>)
								PRINTVECTOR(<<fy,fx,fZ2>>)
								PRINTNL()
							ENDIF
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT) 
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) 
									fx = fx + 0.025		
								ENDIF
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) 
									fx = fx - 0.025		
								ENDIF
								DETACH_ENTITY(objCashBag[0])
								ATTACH_ENTITY_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<fy,fx,fZ2>>,<< 50, 50, -135 >>)
								PRINTVECTOR(<<fy,fx,fZ2>>)
								PRINTNL()
							ENDIF
							IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LT) 	
							AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RT)  	
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) 
									fz2 = fz2 + 0.025		
								ENDIF
								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) 
									fz2 = fz2 - 0.025	
								ENDIF
								DETACH_ENTITY(objCashBag[0])
								ATTACH_ENTITY_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<fy,fx,fZ2>>,<< 50, 50, -135 >>)
								PRINTVECTOR(<<fy,fx,fZ2>>)
								PRINTNL()
							ENDIF
							*/
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(objCashBag[0])		
							IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.237
								DETACH_ENTITY(objCashBag[0])
								DELETE_OBJECT(objCashBag[0])
							ENDIF
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.347
					AND GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) < 0.455
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[1],pedEpsilonHiSecurity[5])
							ATTACH_ENTITY_TO_ENTITY(objCashBag[1],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<< 0.25, 0.075, -0.3 >>,<< 50, 50, -135 >>)
							FREEZE_ENTITY_POSITION(objCashBag[1],FALSE)
							PRINTVECTOR(GET_ENTITY_COORDS(objCashBag[1]))
							PRINTVECTOR(GET_ENTITY_ROTATION(objCashBag[1]))
							PRINTNL()
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(objCashBag[1])		
							IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.455
								DETACH_ENTITY(objCashBag[1])
								DELETE_OBJECT(objCashBag[1])
							ENDIF
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.563
					AND GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) < 0.678
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[2],pedEpsilonHiSecurity[5])
							ATTACH_ENTITY_TO_ENTITY(objCashBag[2],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<< 0.25, 0.075, -0.3 >>,<< 50, 50, -135 >>)
							FREEZE_ENTITY_POSITION(objCashBag[2],FALSE)
							PRINTVECTOR(GET_ENTITY_COORDS(objCashBag[2]))
							PRINTVECTOR(GET_ENTITY_ROTATION(objCashBag[2]))
							PRINTNL()
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(objCashBag[2])	
							IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.678
								DETACH_ENTITY(objCashBag[2])
								DELETE_OBJECT(objCashBag[2])
							ENDIF
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.789
					AND GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) < 0.901
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[3],pedEpsilonHiSecurity[5])
							ATTACH_ENTITY_TO_ENTITY(objCashBag[3],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<< 0.25, 0.075, -0.3 >>,<< 50, 50, -135 >>)
							FREEZE_ENTITY_POSITION(objCashBag[3],FALSE)
							bNoCashLeftInCar = TRUE
							PRINTVECTOR(GET_ENTITY_COORDS(objCashBag[3]))
							PRINTVECTOR(GET_ENTITY_ROTATION(objCashBag[3]))
							PRINTNL()
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(objCashBag[3])	
							IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.901
								//DETACH_ENTITY(objCashBag[3])
								//DELETE_OBJECT(objCashBag[3])
							ENDIF
						ENDIF
					ENDIF
					/*
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.126
					AND GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) < 0.237
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5])
							ATTACH_ENTITY_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>)
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.237
							DETACH_ENTITY(objCashBag[0])
						ENDIF
					ENDIF
					*/
					IF GET_SYNCHRONIZED_SCENE_PHASE(issLoadHeli) >= 0.903
						IF DOES_ENTITY_EXIST(objCashBag[3])	
							DETACH_ENTITY(objCashBag[3])
							DELETE_OBJECT(objCashBag[3])
						ENDIF
						CLEAR_PED_TASKS(pedEpsilonHiSecurity[5])
						SET_PED_KEEP_TASK(pedEpsilonHiSecurity[5],TRUE)
						TASK_ENTER_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
						iTimerSecurityGettingOnChopper = GET_GAME_TIMER()
						bGuardGettingOnHeli = TRUE
						eLOAD_HELI = GET_IN_HELI
					ENDIF
				ENDIF
				
			
			/*
				//IF iBagsTakenFromCar = 0
				IF IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right") 	
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right") > 0.24	
					IF bBootOpen = FALSE
						SET_VEHICLE_DOOR_OPEN(vehPlayerCar,SC_DOOR_BOOT)
						SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)	
						bBootOpen = TRUE
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[iBagsTakenFromCar],pedEpsilonHiSecurity[5])	
					DETACH_ENTITY(objCashBag[iBagsTakenFromCar])	
					//ATTACH_ENTITY_TO_ENTITY(objCashBag[iBagsTakenFromCar],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<0.1,-0.35,-0.35>>,<<0.0,0.0,0.0>>)
					ATTACH_ENTITY_TO_ENTITY(objCashBag[iBagsTakenFromCar],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<0.0,0.0,0.0>>,<<0.0,0.0,0.0>>)
					//SET_ENTITY_DYNAMIC(objCashBag[iBagsTakenFromCar], TRUE)
					//SET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5],"move_ped_wpn_jerrycan_generic")
					
					SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK,"rcmepsilonism8","bag_handler_walk",INSTANT_BLEND_IN)
					SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE,"rcmepsilonism8","bag_handler_idle",INSTANT_BLEND_IN)
					
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right")	
				AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_left")
				AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_close_trunk_walk_left")
				AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_close_trunk_walk_right")
				//IF GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_right") > 0.7
				//OR GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_grab_walk_left") > 0.7	
				//OR GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_close_trunk_walk_left") > 0.7
				//OR GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_close_trunk_walk_right") > 0.7
					++iBagsTakenFromCar
					IF iBagsTakenFromCar = iNumberOfMoneyBags
						bNoCashLeftInCar = TRUE
						IF bBootOpen = TRUE
							SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
							SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
							bBootOpen = FALSE
						ENDIF
					ENDIF
					TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_walk",INSTANT_BLEND_IN,INSTANT_BLEND_OUT, -1, AF_UPPERBODY | AF_LOOPING | AF_SECONDARY | AF_TAG_SYNC_CONTINUOUS)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<-1.5,0,0>>),PEDMOVEBLENDRATIO_WALK,-1,0.4,ENAV_DEFAULT,GET_ENTITY_HEADING(vehEpsilonChopper) - 90)
					eLOAD_HELI = WALK_TO_HELI
				ENDIF
			*/
			BREAK
			
			CASE WALK_TO_HELI
			
				IF IS_ENTITY_IN_RANGE_COORDS(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<-1.5,0,0>>),0.5)					
				AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
					STOP_ANIM_TASK(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_walk")
					TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_load_walk_away_left",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_TAG_SYNC_OUT | AF_TAG_SYNC_CONTINUOUS | AF_TAG_SYNC_IN | AF_USE_MOVER_EXTRACTION | AF_REORIENT_WHEN_FINISHED | AF_REPOSITION_WHEN_FINISHED)
					eLOAD_HELI = DROP_OFF_BAG
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK	
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<-1.5,0,0>>),PEDMOVEBLENDRATIO_WALK,-1,0.4,ENAV_DEFAULT,GET_ENTITY_HEADING(vehEpsilonChopper) - 90)
					ENDIF
				ENDIF
			
			BREAK

			CASE DROP_OFF_BAG
			
				IF IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_load_walk_away_left")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_load_walk_away_left") > 0.5
					DETACH_ENTITY(objCashBag[iBagsPutInHeli])
				ENDIF
					
				IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_load_walk_away_left")
				OR GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonHiSecurity[5],"rcmepsilonism8","bag_handler_load_walk_away_left") > 0.8
					//DETACH_ENTITY(objCashBag[iBagsPutInHeli])
					SAFE_DELETE_OBJECT(objCashBag[iBagsPutInHeli])
					//RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5])
					CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK,INSTANT_BLEND_OUT)
					CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE,INSTANT_BLEND_OUT)
					++iBagsPutInHeli
					IF iBagsPutInHeli = iNumberOfMoneyBags
						CLEAR_PED_TASKS(pedEpsilonHiSecurity[5])
						SET_PED_KEEP_TASK(pedEpsilonHiSecurity[5],TRUE)
						TASK_ENTER_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
						iTimerSecurityGettingOnChopper = GET_GAME_TIMER()
						bGuardGettingOnHeli = TRUE
						eLOAD_HELI = GET_IN_HELI
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonHiSecurity[5],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-3.2,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3,ENAV_DEFAULT,GET_ENTITY_HEADING(vehPlayerCar))
						eLOAD_HELI = WALK_TO_CAR
					ENDIF
				ENDIF
			
			BREAK
			
			CASE GET_IN_HELI
				
				
			BREAK

		ENDSWITCH
		
	ENDIF	
/*
		OPEN_SEQUENCE_TASK(seqLoadChopper1)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -354.5543, -85.2916, 44.6475 >>,PEDMOVEBLENDRATIO_WALK,-1,1.0,ENAV_NO_STOPPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
					CLOSE_SEQUENCE_TASK(seqLoadChopper1)
					OPEN_SEQUENCE_TASK(seqLoadChopper2)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehPlayerCar,1200)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -358.1744, -88.4951, 44.5940 >>,PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,0.3)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<-1.5,0,0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehEpsilonChopper,1200)
					SET_SEQUENCE_TO_REPEAT(seqLoadChopper2,REPEAT_FOREVER)	
					CLOSE_SEQUENCE_TASK(seqLoadChopper2)
					OPEN_SEQUENCE_TASK(seqGoToPlayerFacePlayer)	
						TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1,6.1,PEDMOVEBLENDRATIO_WALK)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
					CLOSE_SEQUENCE_TASK(seqGoToPlayerFacePlayer)
					TASK_PERFORM_SEQUENCE(pedEpsilonist,seqGoToPlayerFacePlayer)
					CLEAR_SEQUENCE_TASK(seqGoToPlayerFacePlayer)

		IF iLoadChopperSeqSwitch = 2	
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])		
			IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				IF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 1
					IF bNoCashLeftInCar = FALSE
						IF bBootOpen = FALSE
							SET_VEHICLE_DOOR_OPEN(vehPlayerCar,SC_DOOR_BOOT)
							SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)	
							bBootOpen = TRUE
						ENDIF
					ENDIF
				ELIF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 2	
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[i2],pedEpsilonHiSecurity[5])	
						DETACH_ENTITY(objCashBag[i2])
						ATTACH_ENTITY_TO_ENTITY(objCashBag[i2],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<0.1,-0.35,-0.35>>,<<0.0,0.0,0.0>>)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5],"move_ped_wpn_jerrycan_generic")
					ENDIF
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK,"move_ped_wpn_jerrycan_generic","walk")
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE,"move_ped_wpn_jerrycan_generic","idle")
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_RUN,"move_ped_wpn_jerrycan_generic","run")
					bHeliLoadSwitch[i2] = FALSE
					IF i2 = (iNumberOfMoneyBags - 1) 
						bNoCashLeftInCar = TRUE
						IF bBootOpen = TRUE
							SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
							SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
							bBootOpen = FALSE
						ENDIF
					ENDIF
				ELIF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 3
					IF bHeliLoadSwitch[i2] = FALSE	
						IF i2 <> (iNumberOfMoneyBags - 1) 
							SAFE_DELETE_OBJECT(objCashBag[i2])
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5])
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK)
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE)
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_RUN)
						ENDIF
						++i2
						bHeliLoadSwitch[i2] = TRUE
						IF i2 > (iNumberOfMoneyBags - 1) 
							IF bGuardGettingOnHeli = FALSE
								CLEAR_PED_TASKS(pedEpsilonHiSecurity[5])
								CLEAR_SEQUENCE_TASK(seqLoadChopper1)
								CLEAR_SEQUENCE_TASK(seqLoadChopper2)
								SET_PED_KEEP_TASK(pedEpsilonHiSecurity[5],TRUE)
								TASK_ENTER_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
								iTimerSecurityGettingOnChopper = GET_GAME_TIMER()
								bGuardGettingOnHeli = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	*/	
		
ENDPROC

/// PURPOSE: //Respawn/Init Michael's car and attach cash bags
PROC InitPlayerCar()

	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		vehPlayerCar = CREATE_VEHICLE(TAILGATER,vPosPlayerCar,fHeadingPlayerCar)
		SET_VEHICLE_AS_RESTRICTED(vehPlayerCar,0)
		//SET_VEHICLE_COLOUR_COMBINATION(vehPlayerCar,5)
		SET_VEHICLE_COLOURS(vehPlayerCar,iCol1,iCol1)
		SET_VEHICLE_EXTRA_COLOURS(vehPlayerCar,0,0)
		//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehPlayerCar,204, 255, 255 )
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehPlayerCar, 0) 
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehPlayerCar,"K1FFL0M")
	ENDIF
	
	IF IS_VEHICLE_OK(vehPlayerCar)
		
		iPlayerCarHP = GET_ENTITY_HEALTH(vehPlayerCar)
		fPlayerCarPetrolTankHP = GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayerCar)
		fPlayerCarEngineHP = GET_VEHICLE_ENGINE_HEALTH(vehPlayerCar)
		SET_ENTITY_HEALTH(vehPlayerCar,(iPlayerCarHP + (iPlayerCarHP/3)))
		SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayerCar,(fPlayerCarPetrolTankHP + (fPlayerCarPetrolTankHP/3)))
		SET_VEHICLE_ENGINE_HEALTH(vehPlayerCar,(fPlayerCarEngineHP + (fPlayerCarEngineHP/3)))
		SET_VEHICLE_CAN_LEAK_OIL(vehPlayerCar, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(vehPlayerCar, FALSE)
		SET_VEHICLE_TYRES_CAN_BURST(vehPlayerCar, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehPlayerCar, TRUE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar,SC_DOOR_BOOT,FALSE)
		ADD_VEHICLE_STUCK_CHECK_WITH_WARP(vehPlayerCar,0.2,1000,FALSE,FALSE,FALSE,-1)
		
		objCashBag[0] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,0.0,10.0>>))
		objCashBag[1] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,0.0,11.0>>))
		objCashBag[2] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,0.0,12.0>>))
		objCashBag[3] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,0.0,13.0>>))

		//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,0,<<-0.1,-2.0,0.16>>,<<40.0,22.0,0.0>>)   //old prop P_LD_HEIST_BAG_S
		//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,0,<<0.3,-2.0,0.05>>,<<140.0,18.0,50.0>>)
		//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,0,<<-0.34,-2.1,0.05>>,<<105.0,1.0,10.0>>)

		//ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,0,<<-0.06,-1.93,-0.1>>,<<0,0,91>>)  
		//ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,0,<<0.3,-1.92,-0.1>>,<<0,0,93>>)
		//ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,0,<<0.66,-1.94,-0.1>>,<<0,0,89>>)
		
		ATTACH_ENTITY_TO_ENTITY(objCashBag[0],vehPlayerCar,0,<<-0.06,-1.93,-0.1>>,<<0,0,91>>)  
		ATTACH_ENTITY_TO_ENTITY(objCashBag[1],vehPlayerCar,0,<<0.2,-1.92,-0.1>>,<<0,0,93>>)
		ATTACH_ENTITY_TO_ENTITY(objCashBag[2],vehPlayerCar,0,<<0.4,-1.94,-0.1>>,<<0,0,89>>)
		ATTACH_ENTITY_TO_ENTITY(objCashBag[3],vehPlayerCar,0,<<0.66,-1.95,-0.1>>,<<0,0,89>>)

		SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
			ENDIF
		ENDIF
		
	ENDIF
	
	CLEAR_AREA_OF_VEHICLES(<< -567.6498, -2.9035, 43.3971 >>,400,TRUE)
	
	SET_PED_NON_CREATION_AREA(<< -711.6323, 42.0868, 14.5077 >>,<< -663.5175, 118.0403, 86.0362 >>)

ENDPROC

/// PURPOSE: //Start convoy
PROC Init2()    
	
	CLEAR_PRINTS()

	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_HIGHSEC_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(SENTINEL)
	SET_MODEL_AS_NO_LONGER_NEEDED(LANDSTALKER)

	blipEscort = CREATE_VEHICLE_BLIP(vehEpsilonCar[0],TRUE)
	
	PRINT_NOW("EPS8_05",DEFAULT_GOD_TEXT_TIME,0)    // Follow ~b~Epsilon's security~s~ to the helicopter.
	bFollowEpsSecObj = TRUE
	
	iTimerMissionInit = GET_GAME_TIMER()
	
	IF IS_VEHICLE_OK(vehEpsilonCar[0])
		IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])	
			OPEN_SEQUENCE_TASK(seqExitLeanStartDriving)
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,vehEpsilonCar[0],"Eps8LS01",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,-1)
			CLOSE_SEQUENCE_TASK(seqExitLeanStartDriving)
			CLEAR_PED_TASKS(pedEpsilonHiSecDriver[0])
			TASK_PERFORM_SEQUENCE(pedEpsilonHiSecDriver[0],seqExitLeanStartDriving)
			CLEAR_SEQUENCE_TASK(seqExitLeanStartDriving)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_OK(vehEpsilonChopper)
		START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
		SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
	ENDIF

ENDPROC

/// PURPOSE: //Respot launcher vehicles
PROC RespotLauncherVehicles()        

	VECTOR vPosPlayerCarCurrent
	VECTOR vPosLandstalkerCurrent

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[1])
		vehPlayerCar = sRCLauncherDataLocal.vehID[1]
		sRCLauncherDataLocal.vehID[1] = NULL
		SET_VEHICLE_AS_RESTRICTED(vehPlayerCar,0)
		vPosPlayerCarCurrent = GET_ENTITY_COORDS(vehPlayerCar)
		IF vPosPlayerCarCurrent.x < vPosPlayerCar.x - 0.01
		OR vPosPlayerCarCurrent.x > vPosPlayerCar.x + 0.01
		OR vPosPlayerCarCurrent.y < vPosPlayerCar.y - 0.01
		OR vPosPlayerCarCurrent.y > vPosPlayerCar.y + 0.01	
			SAFE_TELEPORT_ENTITY(vehPlayerCar,vPosPlayerCar, fHeadingPlayerCar)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
		ENDIF
	ELSE
		vehPlayerCar = CREATE_VEHICLE(TAILGATER,vPosPlayerCar, fHeadingPlayerCar)
		SET_VEHICLE_AS_RESTRICTED(vehPlayerCar,0)
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehPlayerCar, 0) 
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehPlayerCar,"K1FFL0M")
		//SET_VEHICLE_COLOUR_COMBINATION(vehPlayerCar,5)
		SET_VEHICLE_COLOURS(vehPlayerCar,iCol1,iCol1)
		SET_VEHICLE_EXTRA_COLOURS(vehPlayerCar,0,0)
		//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehPlayerCar,204, 255, 255 )
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
	ENDIF
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
		vehEpsilonCar[0] = sRCLauncherDataLocal.vehID[0]
		sRCLauncherDataLocal.vehID[0] = NULL
		SET_VEHICLE_DOORS_LOCKED(vehEpsilonCar[0],VEHICLELOCK_UNLOCKED)
		vPosLandstalkerCurrent = GET_ENTITY_COORDS(vehPlayerCar)
		IF vPosLandstalkerCurrent.x < vPosEpsilonCar[0].x - 0.01
		OR vPosLandstalkerCurrent.x > vPosEpsilonCar[0].x + 0.01
		OR vPosLandstalkerCurrent.y < vPosEpsilonCar[0].y - 0.01
		OR vPosLandstalkerCurrent.y > vPosEpsilonCar[0].y + 0.01	
			SAFE_TELEPORT_ENTITY(vehEpsilonCar[0],vPosEpsilonCar[0], fHeadingEpsilonCar[0])
			SET_VEHICLE_ON_GROUND_PROPERLY(vehEpsilonCar[0])
		ENDIF
	ELSE
		vehEpsilonCar[0] = CREATE_VEHICLE(LANDSTALKER,vPosEpsilonCar[0], fHeadingEpsilonCar[0])
		SET_VEHICLE_COLOURS(vehEpsilonCar[0],iCol1,iCol1)
		SET_VEHICLE_EXTRA_COLOURS(vehEpsilonCar[0],0,0)
		//SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehEpsilonCar[0],204, 255, 255 )
		SET_VEHICLE_ON_GROUND_PROPERLY(vehEpsilonCar[0])
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehEpsilonCar[0], 0) 
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedEpsilonHiSecDriver[0])
		pedEpsilonHiSecDriver[0] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[0],PEDTYPE_MISSION,S_M_M_HIGHSEC_01)
	ENDIF
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])	
		SET_PED_COMP_ITEM_CURRENT_SP(pedEpsilonHiSecDriver[0], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedEpsilonHiSecDriver[0], "EPSGUARD2")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedEpsilonHiSecShotgun[0])
		pedEpsilonHiSecShotgun[0] = CREATE_PED_INSIDE_VEHICLE(vehEpsilonCar[0],PEDTYPE_MISSION,S_M_M_HIGHSEC_01,VS_FRONT_RIGHT)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedEpsilonHiSecurity[0])	
		pedEpsilonHiSecurity[0] = CREATE_PED(PEDTYPE_MISSION,S_M_M_HIGHSEC_01,vPosEpsilonHiSec[0],fHeadingEpsilonHiSec[0])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedEpsilonHiSecurity[2])	
		pedEpsilonHiSecurity[2] = CREATE_PED(PEDTYPE_MISSION,S_M_M_HIGHSEC_01,vPosEpsilonHiSec[2],fHeadingEpsilonHiSec[2])
	ENDIF

ENDPROC

/// PURPOSE: Added for B*1776144 - Can't call TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING every frame anymore.
PROC SET_LEAD_CAR_SPEED()

	IF fTopSpeed = -1
		VEHICLE_WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(vehEpsilonCar[0])
	ELSE
		IF GET_ENTITY_SPEED(vehEpsilonCar[0]) > fTopSpeed
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(vehEpsilonCar[0],fTopSpeed)
		ELSE
			VEHICLE_WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(vehEpsilonCar[0])
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: //Rubberbanding for the convoy
PROC RubberBand()        
	
	BOOL bLeadCarDecel = FALSE
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -358.0764, -88.2705, 44.5901 >>) > GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedEpsilonHiSecDriver[0]),<< -358.0764, -88.2705, 44.5901 >>)
		fTopSpeed = (40.0 - (GET_DISTANCE_BETWEEN_ENTITIES(pedEpsilonHiSecDriver[0],vehPlayerCar)))   //60.0 -
		IF fTopSpeed > 0.0	
			fTopSpeed = (fTopSpeed / 2.5)
		ELSE
			fTopSpeed = 0.000001
		ENDIF
		IF fTopSpeed < 4.0
			fTopSpeed = 0.000001
		ENDIF
		IF fTopSpeed > 8.0
			fTopSpeed = -1
		ENDIF
		IF fTopSpeed < 3.0
			bLeadCarDecel = TRUE
		ENDIF
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
			IF GET_TIME_POSITION_IN_RECORDING(vehEpsilonChopper) < 47043.750000
				SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
			ENDIF
		ENDIF
	ELSE		
		fTopSpeed = -1
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)       //Heli speeds up if player goes in front of lead car
			IF GET_TIME_POSITION_IN_RECORDING(vehEpsilonChopper) < 47043.750000	
				SET_PLAYBACK_SPEED(vehEpsilonChopper,1.5)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
		IF IS_VEHICLE_OK(vehEpsilonCar[0])		
			IF IS_PED_SITTING_IN_VEHICLE(pedEpsilonHiSecDriver[0],vehEpsilonCar[0])
				IF fTopSpeed <> -1	
					IF bLeadCarDecel
						SET_VEHICLE_BRAKE_LIGHTS(vehEpsilonCar[0],TRUE)
					ELSE
						SET_VEHICLE_BRAKE_LIGHTS(vehEpsilonCar[0],FALSE)
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					IF missionStage = MS_DRIVING_TO_CHOPPER
					OR missionStage = MS_PARKING_AT_CHOPPER
					OR missionStage = MS_PARKED_AT_CHOPPER	
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0])
							VEHICLE_WAYPOINT_PLAYBACK_PAUSE(vehEpsilonCar[0])
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonHiSecDriver[0], <<-345.643707,-85.666794,53.658215>>, <<-378.453491,-73.822990,43.158215>>, 18.250000)	
							IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0]) OR NOT bSwitchWaypointFlags
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[0],vehEpsilonCar[0],"Eps8LS01",DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
								bSwitchWaypointFlags = TRUE
							ELSE
								SET_LEAD_CAR_SPEED()
							ENDIF
						ELSE
							IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0])
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[0],vehEpsilonCar[0],"Eps8LS01",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
							ELSE
								SET_LEAD_CAR_SPEED()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonHiSecDriver[0], <<-345.643707,-85.666794,53.658215>>, <<-378.453491,-73.822990,43.158215>>, 18.250000)	
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0]) OR NOT bSwitchWaypointFlags
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[0],vehEpsilonCar[0],"Eps8LS01",DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
							bSwitchWaypointFlags = TRUE
						ELSE
							SET_LEAD_CAR_SPEED()
						ENDIF
					ELSE
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0])
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[0],vehEpsilonCar[0],"Eps8LS01",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
						ELSE
							SET_LEAD_CAR_SPEED()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	////Lead car Indicators////
	SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],FALSE,FALSE)
	SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],TRUE,FALSE)
	
	//first right turn
	IF IS_ENTITY_IN_ANGLED_AREA( pedEpsilonHiSecDriver[0], <<-673.624207,46.507320,45.420940>>, <<-657.845581,45.623535,35.899090>>, 7.000000)
		SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],FALSE,TRUE)
	ENDIF
	//second left turn
	IF IS_ENTITY_IN_ANGLED_AREA( pedEpsilonHiSecDriver[0], <<-659.369751,21.066603,45.169373>>, <<-641.968750,-12.162420,35.589779>>, 14.000000)
		SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],TRUE,TRUE)
	ENDIF
	//third right
	IF IS_ENTITY_IN_ANGLED_AREA( pedEpsilonHiSecDriver[0], <<-429.660980,-7.262719,52.317867>>, <<-394.008270,-12.029229,41.923225>>, 14.000000)
		SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],FALSE,TRUE)
	ENDIF
	//forth left
	IF IS_ENTITY_IN_ANGLED_AREA( pedEpsilonHiSecDriver[0], <<-405.986420,-54.693531,51.030403>>, <<-397.146149,-36.447598,41.663853>>, 14.000000)
		SET_VEHICLE_INDICATOR_LIGHTS(vehEpsilonCar[0],TRUE,TRUE)
	ENDIF
	
ENDPROC

/// PURPOSE: Cinematic camera for the helicopter
PROC HeliCinematicCam()    
	
	VECTOR vHeliLostPlayerOffset
	/*
	IF iEpSecBlipped <> iEpSecEvaded + 1	
		IF bHeliCantSeePlayer = FALSE
			IF CAN_HELI_PILOT_SEE_PLAYER(pedPilot)
				iCounterHeliCantSeePlayerChase = 0
				bHeliCantSeePlayerChase = FALSE
			ELSE
				++iCounterHeliCantSeePlayerChase
				IF iCounterHeliCantSeePlayerChase > 100
					bHeliCantSeePlayerChase = TRUE	 
					iCounterHeliCantSeePlayerChase = 101
				ENDIF
			ENDIF
		ELSE
			iCounterHeliCantSeePlayerChase = 0
			bHeliCantSeePlayerChase = FALSE
		ENDIF
	ELSE
		iCounterHeliCantSeePlayerChase = 0
		bHeliCantSeePlayerChase = FALSE
	ENDIF
	*/
	IF IS_PED_UNINJURED(pedPilot)
	AND IS_VEHICLE_OK(vehEpsilonChopper)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND bHelicopterHostile = TRUE
	AND NOT IS_PHONE_ONSCREEN_AND_RINGING()
	AND bHeliGivenUp = FALSE
			
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehEpsilonChopper,300.0)	
			
			IF bPrintHeliCamHelp = FALSE
				PRINT_HELP("EPS8_22")           //Hold ~INPUT_VEH_CIN_CAM~ to see the view from the helicopter.
				bPrintHeliCamHelp = TRUE
			ENDIF
			
		ENDIF
		
		IF bPrintHeliCamHelp = TRUE
			
			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				IF bDoHeliCam = FALSE	
					bDoHeliCam = TRUE
				ELSE
					bDoHeliCam = FALSE	
				ENDIF
			ENDIF
		
		ENDIF
		
	ELSE
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		bDoHeliCam = FALSE
	ENDIF
	
	IF bDoHeliCam = TRUE	
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("EPS8_22")
			CLEAR_HELP(TRUE)
		ENDIF
		
		DRAW_SPRITE("helicopterhud", "hud_corner", 0.46, 0.457,0.023,0.023, 0, 255,255,255, 150)
		DRAW_SPRITE("helicopterhud", "hud_corner", 0.54, 0.457,0.023,0.023,90, 255,255,255, 150)
		DRAW_SPRITE("helicopterhud", "hud_corner", 0.46, 0.548,0.023,0.023,270, 255,255,255, 150)
		DRAW_SPRITE("helicopterhud", "hud_corner", 0.54, 0.548,0.023,0.023,180, 255,255,255, 150)
		
		IF iHeliCamSwitch = 0	
			
			IF NOT DOES_CAM_EXIST(camHeli)
			AND NOT DOES_CAM_EXIST(camHeli2)	
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				camHeli = CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA",TRUE)
				camHeli2 = CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA",TRUE)
				
				//SET_CAM_INHERIT_ROLL_VEHICLE(camHeli,vehEpsilonChopper)
				//SET_CAM_INHERIT_ROLL_VEHICLE(camHeli2,vehEpsilonChopper)

				ATTACH_CAM_TO_ENTITY(camHeli,vehEpsilonChopper,<<0.001,3.001,-2.202>>,TRUE)
				ATTACH_CAM_TO_ENTITY(camHeli2,vehEpsilonChopper,<<0.001,3.001,-2.202>>,TRUE)
				
				POINT_CAM_AT_ENTITY(camHeli,PLAYER_PED_ID(),<<0,0,0>>)
				POINT_CAM_AT_ENTITY(camHeli2,PLAYER_PED_ID(),<<0,0,0>>)
				
				SET_CAM_FOV(camHeli,45.0)
				SET_CAM_FOV(camHeli2,28.0)

				SET_CAM_ACTIVE_WITH_INTERP(camHeli2,camHeli,7000)

				RENDER_SCRIPT_CAMS(TRUE, FALSE)	
				
				SHAKE_CAM(camHeli,"HAND_SHAKE",2.0)
				SHAKE_CAM(camHeli2,"HAND_SHAKE",2.0)
			
			ENDIF

			iHeliCamSwitch = 1	
			
		ELSE
			IF DOES_CAM_EXIST(camHeli)
			AND DOES_CAM_EXIST(camHeli2)

				IF bHeliCantSeePlayer = FALSE	
				//AND bHeliCantSeePlayerChase = FALSE	 
					POINT_CAM_AT_ENTITY(camHeli,PLAYER_PED_ID(),<<0,0,0>>)
					POINT_CAM_AT_ENTITY(camHeli2,PLAYER_PED_ID(),<<0,0,0>>)
					IF iCounterHeliCamLostPlayer <> 0
						IF NOT IS_CAM_INTERPOLATING(GET_RENDERING_CAM())
							SET_CAM_ACTIVE_WITH_INTERP(camHeli2,GET_RENDERING_CAM(),2000)
							iCounterHeliCamLostPlayer = 0
						ENDIF
					ENDIF
				ELSE
					
					++iCounterHeliCamLostPlayer
					IF iCounterHeliCamLostPlayer > 80 + GET_RANDOM_INT_IN_RANGE(0,80)
						
						IF NOT DOES_CAM_EXIST(camHeliLostPlayer[0])
							camHeliLostPlayer[0] = CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA",TRUE)
							//SET_CAM_INHERIT_ROLL_VEHICLE(camHeliLostPlayer[0],vehEpsilonChopper)
						ENDIF
						
						IF NOT DOES_CAM_EXIST(camHeliLostPlayer[1])
							camHeliLostPlayer[1] = CREATE_CAM("DEFAULT_SCRIPTED_FLY_CAMERA",TRUE)
							//SET_CAM_INHERIT_ROLL_VEHICLE(camHeliLostPlayer[1],vehEpsilonChopper)
						ENDIF
						
						ATTACH_CAM_TO_ENTITY(camHeliLostPlayer[iCam],vehEpsilonChopper,<<0.001,3.001,-2.202>>,TRUE)
						SET_CAM_FOV(camHeliLostPlayer[iCam],28.0)
						SHAKE_CAM(camHeliLostPlayer[iCam],"HAND_SHAKE",2.0)

						vHeliLostPlayerOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<GET_RANDOM_FLOAT_IN_RANGE(-50,50),GET_RANDOM_FLOAT_IN_RANGE(-50,50),0>>)
	
						POINT_CAM_AT_COORD(camHeliLostPlayer[iCam],vHeliLostPlayerOffset)
	
						SET_CAM_ACTIVE_WITH_INTERP(camHeliLostPlayer[iCam],GET_RENDERING_CAM(),GET_RANDOM_INT_IN_RANGE(1500,3000))
						
						IF iCam = 0
							iCam = 1
						ELSE
							iCam = 0
						ENDIF
						
						iCounterHeliCamLostPlayer = 0
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ELSE
		
		IF iHeliCamSwitch = 1
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF DOES_CAM_EXIST(camHeli)
				
				SET_CAM_ACTIVE(camHeli,FALSE)
				DESTROY_CAM(camHeli)
			
			ENDIF	
			
			IF DOES_CAM_EXIST(camHeli2)
				
				SET_CAM_ACTIVE(camHeli2,FALSE)
				DESTROY_CAM(camHeli2)
			
			ENDIF
			
			IF DOES_CAM_EXIST(camHeliLostPlayer[0])
				
				SET_CAM_ACTIVE(camHeliLostPlayer[0],FALSE)
				DESTROY_CAM(camHeliLostPlayer[0])
				
			ENDIF
			
			IF DOES_CAM_EXIST(camHeliLostPlayer[1])
				
				SET_CAM_ACTIVE(camHeliLostPlayer[1],FALSE)
				DESTROY_CAM(camHeliLostPlayer[1])
				
			ENDIF
			
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
			
			iHeliCamSwitch = 0
		
		ENDIF
	
	ENDIF	
	
ENDPROC

/// PURPOSE: //Stop Mike looking at the Epsilonist if he's not in FOV
PROC HandleMikeLooking()
	
	IF bMikeLookAtEpsilonist = TRUE
		IF IS_PED_UNINJURED(pedEpsilonist)	
			IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(),pedEpsilonist,90)
			OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedEpsilonist,50.0)
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: //Manage friendly blips and objectives
PROC blipsPlayerCar()           
	
	IF bNoCashLeftInCar = FALSE	
		IF bChopperBeingJacked = FALSE
			IF missionStage = MS_TAKEN_MONEY
				SAFE_REMOVE_BLIP(blipChopper)
			ENDIF 
			IF NOT IS_ENTITY_DEAD(vehPlayerCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					IF NOT DOES_BLIP_EXIST(blipPlayerCar)
						blipPlayerCar = CREATE_VEHICLE_BLIP(vehPlayerCar,TRUE)
						IF missionStage = MS_DRIVING_TO_CHOPPER
							IF DOES_BLIP_EXIST(blipEscort)
								SAFE_REMOVE_BLIP(blipEscort)
								IF bGetInCarObjGiven = FALSE
									PRINT_NOW("EPS8_06",DEFAULT_GOD_TEXT_TIME,0)    // Get back in the ~b~car.
									bGetInCarObjGiven = TRUE
								ENDIF
							ENDIF
						ELIF missionStage = MS_PARKING_AT_CHOPPER
							IF DOES_BLIP_EXIST(blipChopper)
								SAFE_REMOVE_BLIP(blipChopper)
								IF bGetInCarObjGiven = FALSE	
									PRINT_NOW("EPS8_06",DEFAULT_GOD_TEXT_TIME,0)    // Get back in the ~b~car.
									bGetInCarObjGiven = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 17.0
						IF bBootOpen = TRUE
							SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
							bBootOpen = FALSE
						ENDIF
					ENDIF
					SAFE_REMOVE_BLIP(blipPlayerCar)
					IF missionStage = MS_DRIVING_TO_CHOPPER
						IF NOT DOES_BLIP_EXIST(blipEscort)
							blipEscort = CREATE_VEHICLE_BLIP(vehEpsilonCar[0],TRUE)
							IF bFollowEpsSecObj = FALSE
								PRINT_NOW("EPS8_05",DEFAULT_GOD_TEXT_TIME,0)    // Follow ~b~Epsilon's security~s~ to the helicopter.
								bFollowEpsSecObj = TRUE
							ENDIF
						ENDIF
					ELIF missionStage = MS_PARKING_AT_CHOPPER
						IF NOT DOES_BLIP_EXIST(blipChopper)
							blipChopper = ADD_BLIP_FOR_COORD(vParkPos)
							//blipChopper = CREATE_VEHICLE_BLIP(vehEpsilonChopper,TRUE)
							//PRINT_NOW("EPS8_07",DEFAULT_GOD_TEXT_TIME,0)    // Park near the ~b~helicopter.
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF missionStage = MS_TAKEN_MONEY	
				SAFE_REMOVE_BLIP(blipPlayerCar)
			ENDIF  
			IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonChopper)
					IF NOT DOES_BLIP_EXIST(blipChopper)
						blipChopper = ADD_BLIP_FOR_COORD(vParkPos)
						//blipChopper = CREATE_VEHICLE_BLIP(vehEpsilonChopper,TRUE)
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipChopper)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF missionStage = MS_TAKEN_MONEY	
			SAFE_REMOVE_BLIP(blipPlayerCar)
		ENDIF
		IF missionStage = MS_TAKEN_MONEY
			SAFE_REMOVE_BLIP(blipChopper)
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE: //Manage conversations during convoy
PROC ConvoConvoy()        

	IF IS_VEHICLE_OK(vehPlayerCar)
		IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])	
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)		
				IF GET_GAME_TIMER() > iTimerMissionInit + 8000	   
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedEpsilonHiSecDriver[0],40.0)	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
							IF GET_ENTITY_SPEED(vehPlayerCar) > 2.0	
								IF bPrintEscapeWarning = FALSE	                        //Get back here now!               
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		                 
										//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_20", CONV_PRIORITY_LOW)
										IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS4", CONV_PRIORITY_LOW)	
											bPrintEscapeWarning = TRUE
											IF iConvoCounter < 2
												iConvoCounter = 2
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF iCounterHurryUp = 10	
						IF GET_GAME_TIMER() > iTimerHurryUp + 1000	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								sFailReason = "EPS8_23"            			// ~r~You failed to deliver the cash in time.
								missionStage = MS_MISSION_FAILING
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
								EXIT
							ENDIF
						ENDIF
					ENDIF
					IF fTopSpeed < 0.5      //You can't work a stick shift or something?
					AND fTopSpeed <> -1
						IF GET_ENTITY_SPEED(vehPlayerCar) < 2.0
							IF bPrintHurryUp = FALSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS5", "EPS8_LS5_1", CONV_PRIORITY_LOW)
										iTimerHurryUp = GET_GAME_TIMER()
										bPrintHurryUp = TRUE
										IF iConvoCounter < 2
											iConvoCounter = 2
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF iConvoCounter >= 7	
									IF GET_GAME_TIMER() > iTimerHurryUp + 9000
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
											IF iCounterHurryUp < 5
												CREATE_CONVERSATION(s_conversation_peds,"EPS8AU","EPS8_T2",CONV_PRIORITY_MEDIUM)        //Come on! We're on a tight schedule here!
												++iCounterHurryUp
												iTimerHurryUp = GET_GAME_TIMER()
											ELSE
												IF iCounterHurryUp <> 10
													IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_6", CONV_PRIORITY_MEDIUM)   //Fuck this, I'm getting someone else.
														iCounterHurryUp = 10
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF iPlayerDamagingEscort = 1                            //What the fuck? Do you know how to drive?
						IF bPrintRamWarning1 = FALSE	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS3", "EPS8_LS3_1", CONV_PRIORITY_LOW)
									bPrintRamWarning1 = TRUE	
									IF iConvoCounter < 2
										iConvoCounter = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF iPlayerDamagingEscort = 2                            //Do that again I dare you.
						IF bPrintRamWarning2 = FALSE	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS3", "EPS8_LS3_2", CONV_PRIORITY_LOW)
									bPrintRamWarning2 = TRUE	
									IF iConvoCounter < 2
										iConvoCounter = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF           
				ENDIF	
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -358.0764, -88.2705, 44.5901 >>) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedEpsilonHiSecDriver[0]),<< -358.0764, -88.2705, 44.5901 >>)	
					IF GET_GAME_TIMER() > iTimerMissionInit + 8000	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
							IF bPrintFrontWarning = FALSE	                      //What do you think you're doing? Get back behind me.
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
									//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_10", CONV_PRIORITY_LOW)
									IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS2", CONV_PRIORITY_LOW)		
										bPrintFrontWarning = TRUE
										IF iConvoCounter < 2
											iConvoCounter = 2
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF iConvoCounter = 0                   //You got a headset Zondar? Or whatever your name is...
						IF GET_GAME_TIMER() > iTimerMissionInit + 7000	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_1", CONV_PRIORITY_VERY_LOW)
									iConvoCounter = 1
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 1                //Yep... You the guy in front?
						IF GET_GAME_TIMER() > iTimerMissionInit + 9000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_2", CONV_PRIORITY_VERY_LOW)
									iConvoCounter = 2	
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 2				 // Affirmative. Hey Bill, ETA?
						IF GET_GAME_TIMER() > iTimerMissionInit + 12000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_3", CONV_PRIORITY_VERY_LOW)
									iConvoCounter = 3
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 3               //Nearly there man.
						IF GET_GAME_TIMER() > iTimerMissionInit + 13000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_4", CONV_PRIORITY_VERY_LOW)
									iConvoCounter = 4
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 4             // Oh yeah, I see you.
						IF GET_GAME_TIMER() > iTimerMissionInit + 14000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF IS_ENTITY_ALIVE(vehEpsilonChopper)	
								AND	IS_ENTITY_ALIVE(vehEpsilonCar[0])	
									//IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecDriver[0],vehEpsilonChopper,340.0)
									//IF CAN_PED_SEE_PED(pedEpsilonHiSecDriver[0],vehEpsilonChopper)	
									//IF GET_VEHICLE_WAYPOINT_PROGRESS(vehEpsilonCar[0]) > 15//19
									//AND NOT IS_ENTITY_OCCLUDED(vehEpsilonChopper)	
									IF NOT IS_ENTITY_IN_RANGE_COORDS(vehEpsilonCar[0],<<-696.0367, 40.7954, 42.2066>>,80)	
									AND NOT IS_ENTITY_IN_RANGE_COORDS(vehEpsilonChopper,<<-355.8568, -89.7074, 44.6341>>,50)		
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_5", CONV_PRIORITY_VERY_LOW)
											iConvoCounter = 5
										ENDIF
									ELSE
										iConvoCounter = 5
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 5             //How much money is in here anyway?
						IF GET_GAME_TIMER() > iTimerMissionInit + 15000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_6", CONV_PRIORITY_VERY_LOW)
									iConvoCounter = 6	
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoCounter = 6             //Don't even think about it, you can't get away from us.
						IF GET_GAME_TIMER() > iTimerMissionInit + 16000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
								//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_7", CONV_PRIORITY_VERY_LOW)
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, "EPS8AU", "EPS8_LS", "EPS8_LS_7", CONV_PRIORITY_MEDIUM)
									iConvoCounter = 7
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iPrintPlayerLeftCar = 0
					iTimerPlayerLeftCar = GET_GAME_TIMER()
					iPrintPlayerLeftCar = 1
				ELIF iPrintPlayerLeftCar = 1
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 7000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //Get back in the car, we've got a job to do.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_1", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iTimerHurryUp = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 2
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 2
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 8000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //Come on for fucks sake.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_2", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iTimerHurryUp = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 3
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 3
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 8000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //Get back in the car!
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_3", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iTimerHurryUp = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 4
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 4
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 7000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //Is my mic broken or something?
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_4", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iTimerHurryUp = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 5
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 5
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 1000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //I can hear you just fine...
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_5", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iTimerHurryUp = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 6
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 6
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 10000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		  //Fuck this, I'm getting someone else.
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_T1", "EPS8_T1_6", CONV_PRIORITY_VERY_LOW)
								iTimerPlayerLeftCar = GET_GAME_TIMER()
								iPrintPlayerLeftCar = 7
								IF iConvoCounter < 2
									iConvoCounter = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPrintPlayerLeftCar = 7
					IF GET_GAME_TIMER() > iTimerPlayerLeftCar + 1000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							sFailReason = "EPS8_23"            			// ~r~You failed to deliver the cash in time.
							missionStage = MS_MISSION_FAILING
							iPrintPlayerLeftCar = 8
							IF iConvoCounter < 2
								iConvoCounter = 2
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC

/// PURPOSE: //Manage conversations during chase
PROC ConvoChase()     

	INT iSubs

	IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_12")		
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_25")
		iSubs = ENUM_TO_INT(DISPLAY_SUBTITLES)
	ELSE
		iSubs = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
	ENDIF
	
	IF IS_PED_UNINJURED(pedPilot)	
		//IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
			IF iConvoChaseCar = 0	
				IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 1800
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Fuck Fuck Fuck Fuck. Get on him Bill.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_1", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseCar = 1
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvoChaseCar = 1
				IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 3000
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Can you see him?
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_2", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseCar = 2
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvoChaseCar = 2
				IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 5000	
					IF bHeliCantSeePlayer = FALSE	
						IF CAN_HELI_PILOT_SEE_PLAYER(pedPilot)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //It's ok I've got eyes on.
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_1", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
									iConvoChaseCar = 3
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvoChaseCar = 3
				IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 10000
					IF bHeliCantSeePlayer = FALSE
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
							IF GET_ENTITY_SPEED(vehPlayerCar) > 17.0	
								IF CAN_HELI_PILOT_SEE_PLAYER(pedPilot)		
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										fHeadingPlayerCar = GET_ENTITY_HEADING(vehPlayerCar)
										IF fHeadingPlayerCar >= 315.0
										OR fHeadingPlayerCar < 45.0     //He's heading north.												
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_2", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
												iConvoChaseCar = 4
											ENDIF
										ENDIF
										IF fHeadingPlayerCar >= 45.0
										AND fHeadingPlayerCar < 135.0   //He's heading east.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_3", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
												iConvoChaseCar = 4
											ENDIF
										ENDIF
										IF fHeadingPlayerCar >= 135.0
										AND fHeadingPlayerCar < 225.0   //He's heading south.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_4", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
												iConvoChaseCar = 4
											ENDIF
										ENDIF
										IF fHeadingPlayerCar >= 225.0
										AND fHeadingPlayerCar < 315.0    //He's heading west.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_5", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
												iConvoChaseCar = 4
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ELSE
			//IF iConvoChaseCar = 0
			//	iConvoChaseCar = 1
			//ENDIF
			IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 8000
				IF bPrintMikeOnFoot = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()                    //He's on foot.
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_20", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
						bPrintMikeOnFoot = TRUE
					ENDIF
				ENDIF
				IF IS_PED_IN_COVER(PLAYER_PED_ID())
					IF bPrintMikeInCover = FALSE		
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehEpsilonChopper,200.0)	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()                    //He's in cover, flush him out.
								IF bHeliCantSeePlayer = FALSE
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_6", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
										bPrintMikeInCover = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(pedChopperShotgun)	
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(PLAYER_PED_ID(),pedChopperShotgun)
						IF bPrintToddShotMike = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()                    //I think Todd winged him.	
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_7", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
									bPrintToddShotMike = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF iPrintToddDied = 0
						iTimerToddDied = GET_GAME_TIMER()
						iPrintToddDied = 1
					ELIF iPrintToddDied = 1
						IF GET_GAME_TIMER() > iTimerToddDied + 2400
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()                    //Shit, Todd is down... He was getting married next week.
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_8", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
									iPrintToddDied = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ENDIF
	ENDIF
	
	IF iEpSecBlipped > iEpSecEvaded + 2
		IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 8000
			IF GET_RANDOM_INT_IN_RANGE(0,100) = 25
				IF iConvoChaseRandom = 0	
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Somebody put him down.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH3", "EPS8_CH3_1", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseRandom = 1
						ENDIF
					ENDIF
				ELIF iConvoChaseRandom = 1
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Fucking kill him.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH3", "EPS8_CH3_2", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseRandom = 2
						ENDIF
					ENDIF
				ELIF iConvoChaseRandom = 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Shit, my contact lens came out.
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_5", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
						iConvoChaseRandom = 3
					ENDIF
				ELIF iConvoChaseRandom = 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //We don't get paid enough for this shit.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH3", "EPS8_CH3_3", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseRandom = 4
						ENDIF
					ENDIF
				ELIF iConvoChaseRandom = 4
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Anyone got spare ammo?.
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_8", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
						iConvoChaseRandom = 5
					ENDIF
				ELIF iConvoChaseRandom = 5
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //This is crazy.
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_9", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
						iConvoChaseRandom = 6
					ENDIF
				ELIF iConvoChaseRandom = 6
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Come on, stop him. It's just one guy.
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH", "EPS8_CH_10", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
						iConvoChaseRandom = 7
					ENDIF
				ELIF iConvoChaseRandom = 7
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	    //Fucking randoms getting in the way.
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH3", "EPS8_CH3_4", CONV_PRIORITY_LOW,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iConvoChaseRandom = 8
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: // Working out if the player took the money
PROC Update()      

	/*
	IF NOT IS_PED_UNINJURED(pedPilot)
		IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)
			FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
		ENDIF
	ENDIF
	*/
	IF IS_VEHICLE_OK(vehPlayerCar)	
		SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehPlayerCar,TRUE)	
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[0])	
		IF bRoadSwitchOn = FALSE
			IF GET_GAME_TIMER() > iTimerMissionInit + 5000                          //Switch traffic back on if lead car gets out or timer is more than X ms
			OR NOT IS_ENTITY_IN_ANGLED_AREA(vehEpsilonCar[0], <<-664.436279,53.475925,31.860622>>, <<-727.772949,24.454491,46.731674>>, 22.500000)	
				bRoadSwitchOn = TRUE
			ENDIF
		ENDIF
		SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehEpsilonCar[0],TRUE)
		IF missionStage = MS_DRIVING_TO_CHOPPER
		OR missionStage = MS_PARKING_AT_CHOPPER	
			IF GET_GAME_TIMER() > iTimerDetectDamage + 4000 	
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehEpsilonCar[0],PLAYER_PED_ID())
					IF iPlayerDamagingEscort < 2	
						++iPlayerDamagingEscort
						iTimerDetectDamage = GET_GAME_TIMER()
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehEpsilonCar[0])
					ELSE
						//TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						//IF NOT ARE_STRINGS_EQUAL(root, "PEPS8_LS")
						IF NOT ARE_STRINGS_EQUAL(label, "EPS8_LS_13")
							++iPlayerDamagingEscort
							iTimerDetectDamage = GET_GAME_TIMER()
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehEpsilonCar[0])
						ELSE
							iPlayerDamagingEscort = 2
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehEpsilonCar[0])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iPlayerDamagingEscort > 2           //If player damages lead car X+1 times -> everyone kick off 
				//TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				//IF NOT ARE_STRINGS_EQUAL(root, "PEPS8_LS")
				IF NOT ARE_STRINGS_EQUAL(label, "EPS8_LS_13")
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "**** Player damaged front escort car 3 times ****")
					#ENDIF
					bPlayerStoleMoney = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[2])
		SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehEpsilonCar[2],TRUE)	
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[3])	
		SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehEpsilonCar[3],TRUE)
	ENDIF
	
	//If player goes X from lead car -> everyone kick off 
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])	
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(vehPlayerCar,pedEpsilonHiSecDriver[0],50.0)	
			IF bPlayerAtHelipad = FALSE                             
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player driving away ****")
				#ENDIF
				bPlayerStoleMoney = TRUE              
			ENDIF
		ENDIF
	ENDIF
	
	//If player dropping sticky bombs near helicopter -> everyone kick off 
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-366.354156,-107.600349,31.931767>>, <<-355.058075,-74.912842,55.840660>>, 85.250000)			
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			IF GET_SELECTED_PED_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_STICKYBOMB	
			OR GET_SELECTED_PED_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player dropped sticky bombs ****")
				#ENDIF
				bPlayerStoleMoney = TRUE          
			ENDIF
		ENDIF
	ENDIF

	//Working out if the player went to the tractor
	IF bPlayerAtHelipad = FALSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-366.354156,-107.600349,31.931767>>, <<-355.058075,-74.912842,55.840660>>, 85.250000)		
			bPlayerAtHelipad = TRUE
		ENDIF
	ELSE
		IF missionStage = MS_WALKING_TO_REWARD                        
			IF IS_VEHICLE_OK(vehTractor)
			//AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-380.104370,-98.856026,37.515076>>, <<-370.216644,-104.364128,39.490211>>, 21.000000)
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-383.155762,-102.744186,37.430794>>, <<-371.118317,-109.461861,40.429165>>, 13.000000)
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				IF IS_PED_UNINJURED(pedEpsilonist)
					IF bEpsilonistAtTractor = TRUE
						IF bEpsilonistPlayingAnims = TRUE	
							//IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","c1_pose")
							IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
							AND bPrayConvo = TRUE	
							AND GET_GAME_TIMER() > iTimerPrayConvo + 3200
							AND NOT IS_PED_RAGDOLL(pedEpsilonist)		
							AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedEpsilonist,PLAYER_PED_ID())	
								IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_base")	
								OR IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_idle_a")	
									//TASK_PLAY_ANIM(pedEpsilonist,"rcmepsilonism8","c1_pose",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)	
									//STOP_ANIM_TASK(pedEpsilonist,"rcmepsilonism8","c1_pose",-2)
									TASK_PLAY_ANIM(pedEpsilonist,"rcmepsilonism8","worship_exit",2,-2,-1,AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_TAG_SYNC_OUT)
									bPlayerGaveMoney = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehTractor) < GET_DISTANCE_BETWEEN_ENTITIES(pedEpsilonist,vehTractor)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehTractor,TRUE)
							IF iSeqDelayAggression = 0	
								bPlayerGaveMoney = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				bPlayerWentToTractor = TRUE
			ENDIF
			IF iSeqDelayAggression = 0	
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-430.659424,-63.760262,99.820374>>, <<-257.635529,-120.517120,37.302601>>, 170.500000)
					bPlayerWentToTractor = FALSE
					bPlayerGaveMoney = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Player went to the heli compound then drove off -> everyone kick off 
	IF bPlayerAtHelipad = TRUE              
		IF iSeqDelayAggression = 0	
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-366.354156,-107.600349,31.931767>>, <<-355.058075,-74.912842,55.840660>>, 85.250000)	
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
					IF bPlayerWentToTractor = FALSE		
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "**** Player drove away after reaching helicopter ****")
						#ENDIF
						iTimerDelayAggression = GET_GAME_TIMER()
						iSeqDelayAggression = 1    
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iSeqDelayAggression = 1
			IF GET_GAME_TIMER()	> iTimerDelayAggression + 1500
				bPlayerStoleMoney = TRUE
			ENDIF
		ENDIF
	ENDIF

	//Heli made it to the compound, freeze it until loaded up
	//IF missionStage < MS_EXITED_CAR 	
		IF IS_VEHICLE_OK(vehEpsilonChopper)                          
			IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])
				IF IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonHiSecurity[5],TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[5],CA_LEAVE_VEHICLES,FALSE)
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonHiSecurity[5],FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[5],CA_LEAVE_VEHICLES,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[5],CA_USE_VEHICLE,FALSE)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(pedPilot)
				IF IS_PED_IN_VEHICLE(pedPilot,vehEpsilonChopper)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedPilot,CA_LEAVE_VEHICLES,FALSE)
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPilot,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedPilot,CA_LEAVE_VEHICLES,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedPilot,CA_USE_VEHICLE,FALSE)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(pedEpsilonist)
				IF IS_PED_IN_VEHICLE(pedEpsilonist,vehEpsilonChopper)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonist,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_LEAVE_VEHICLES,FALSE)
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonist,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_LEAVE_VEHICLES,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_USE_VEHICLE,FALSE)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(pedChopperShotgun)
				IF IS_PED_IN_VEHICLE(pedChopperShotgun,vehEpsilonChopper)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedChopperShotgun,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedChopperShotgun,CA_LEAVE_VEHICLES,FALSE)
				ELSE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedChopperShotgun,FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedChopperShotgun,CA_LEAVE_VEHICLES,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedChopperShotgun,CA_USE_VEHICLE,FALSE)
				ENDIF
			ENDIF
			//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
				IF GET_TIME_POSITION_IN_RECORDING(vehEpsilonChopper) >= 47043.750000
					SET_PLAYBACK_SPEED(vehEpsilonChopper,0)
				ENDIF
				//PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehEpsilonChopper))
				//PRINTNL()
				/*
				IF i2 < (iNumberOfMoneyBags - 1)
					FREEZE_ENTITY_POSITION(vehEpsilonChopper,TRUE)
					IF iConvoCounter < 5
						iConvoCounter = 5
					ENDIF
				ENDIF
				*/
			ENDIF
		ENDIF
	//ENDIF

	//Rear convoy car, follow player then stop outside heli compound
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[3])	         
		IF IS_VEHICLE_OK(vehEpsilonCar[3])			
			IF IS_VEHICLE_OK(vehPlayerCar)		
				IF IS_ENTITY_AT_COORD(pedEpsilonHiSecDriver[3],<< -403.4930, -39.7956, 45.1741 >>,<<8.0,8.0,8.0>>)
					bStartRearConvoyCar = FALSE
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[3])	
						VEHICLE_WAYPOINT_PLAYBACK_PAUSE(vehEpsilonCar[3])
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[3],SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK	
						TASK_VEHICLE_TEMP_ACTION(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],TEMPACT_BRAKE,-1)
					ENDIF
				ELSE
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -658.9627, 46.1624, 40.1914 >>,<<4.0,4.0,5.0>>)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
							IF bStartRearConvoyCar = FALSE	
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],"Eps8LS01",DRIVINGMODE_AVOIDCARS_RECKLESS,18,EWAYPOINT_DEFAULT,-1,40.0)
								bStartRearConvoyCar = TRUE	
							ENDIF
						ENDIF
					ENDIF
					IF bStartRearConvoyCar = TRUE
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
							IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonHiSecDriver[3], <<-431.309967,-13.288644,44.087696>>, <<-391.900452,-16.811705,49.912033>>, 33.750000)	
								IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[3])
									TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],"Eps8LS01",DRIVINGMODE_AVOIDCARS,18,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,40.0)	
								ENDIF
							ELSE	
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedEpsilonHiSecDriver[3],30.0)		
									IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[3])	
										TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],"Eps8LS01",DRIVINGMODE_AVOIDCARS_RECKLESS,18,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,40.0)	
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[3],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
										//TASK_VEHICLE_ESCORT(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],vehPlayerCar,VEHICLE_ESCORT_REAR,40.0,DRIVINGMODE_AVOIDCARS_RECKLESS,9)
										TASK_VEHICLE_FOLLOW(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],vehPlayerCar,40.0,DRIVINGMODE_PLOUGHTHROUGH,10)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[3],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								//TASK_VEHICLE_ESCORT(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],vehPlayerCar,VEHICLE_ESCORT_REAR,40.0,DRIVINGMODE_AVOIDCARS_RECKLESS,9)
								TASK_VEHICLE_FOLLOW(pedEpsilonHiSecDriver[3],vehEpsilonCar[3],vehPlayerCar,40.0,DRIVINGMODE_PLOUGHTHROUGH,10)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//If any drivers are in combat with the player -> everyone kick off
	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[i])	
		IF IS_PED_IN_COMBAT(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player in combat with a driver ****")
			#ENDIF
			bPlayerStoleMoney = TRUE                         
		ENDIF
	ENDIF
	
	//If any passengers are in combat with the player -> everyone kick off
	IF IS_PED_UNINJURED(pedEpsilonHiSecShotgun[i])		
		IF IS_PED_IN_COMBAT(pedEpsilonHiSecShotgun[i],PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player in combat with a passenger ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF

	//If any on foot security are in combat with the player or hurt by the player -> everyone kick off else say "kifflom"
	IF IS_PED_UNINJURED(pedEpsilonHiSecurity[i])	
		IF IS_PED_IN_COMBAT(pedEpsilonHiSecurity[i],PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player in combat with an on-foot security guy ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ELSE
			IF missionStage = MS_WALKING_TO_REWARD
				IF IS_PED_RAGDOLL(pedEpsilonHiSecurity[i])
					IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonHiSecurity[i], <<-381.895935,-98.083679,37.746490>>, <<-378.512177,-88.643440,46.518566>>, 2.400000)
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "**** Player pushed security guy down the stairs ****")
						#ENDIF
						bPlayerStoleMoney = TRUE
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
					//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_15")	
					//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")	
					//AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
						//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF i = 5
								IF bGuard1SaidKifflom = FALSE        //Kifflom.       //(guy loading chopper)
									IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[5],PLAYER_PED_ID(),4.5)
										//IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_G1",CONV_PRIORITY_LOW)	
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedEpsilonHiSecurity[5],"EPS8_ANAA","EPSGUARD2","SPEECH_PARAMS_FORCE")
											bGuard1SaidKifflom = TRUE
										//ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF i = 3
								IF bGuard2SaidKifflom = FALSE       //Kifflom, Zondar.     //(guy in tunnel)
									IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[3],PLAYER_PED_ID(),5.0)	
										//IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_G2",CONV_PRIORITY_LOW)		
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedEpsilonHiSecurity[3],"EPS8_AOAA","EPSGUARD7","SPEECH_PARAMS_FORCE")  //EPSGUARD4
											TASK_PLAY_ANIM(pedEpsilonHiSecurity[3],"rcmepsilonism8","security_greet",2,-2,-1,AF_SECONDARY,0.2,TRUE)
											bGuard2SaidKifflom = TRUE
										//ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF i = 4
								IF bGuard3SaidKifflom = FALSE       //Kifflom.     //(guy at top of steps)
									IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[4],PLAYER_PED_ID(),5.0)	
										//IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_G3",CONV_PRIORITY_LOW)		
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedEpsilonHiSecurity[4],"EPS8_APAA","EPSGUARD8","SPEECH_PARAMS_FORCE")  //EPSGUARD3
											TASK_PLAY_ANIM(pedEpsilonHiSecurity[4],"rcmepsilonism8","security_greet",2,-2,-1,AF_SECONDARY,0.2,TRUE)
											bGuard3SaidKifflom = TRUE
										//ENDIF
									ENDIF
								ENDIF
							ENDIF
						//ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	//If is player aiming at/fighting heli pilot -> everyone kicks off
	IF IS_PED_UNINJURED(pedPilot)	
		IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),pedPilot)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),pedPilot)
			IF IS_PED_FACING_PED(pedPilot,PLAYER_PED_ID(),90)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player threatening pilot ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
		ENDIF
		IF IS_PED_IN_COMBAT(pedPilot,PLAYER_PED_ID())			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player threatening pilot ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF
	
	//If heli sniper is in combat with the player -> everyone kick off
	IF IS_PED_UNINJURED(pedChopperShotgun)
		IF IS_PED_IN_COMBAT(pedChopperShotgun,PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player in combat with heli sniper ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF
	
	//If epsilonist is in combat with the player -> everyone kick off + blip on foot guys and give them combat tasks (needs to react faster than other situations)
	IF IS_PED_UNINJURED(pedEpsilonist)	
		IF IS_PED_IN_COMBAT(pedEpsilonist,PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player in combat with epsilonist ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedEpsilonist,PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player damaged epsilonist ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
		IF IS_PED_RAGDOLL(pedEpsilonist)
			IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonist, <<-381.895935,-98.083679,37.746490>>, <<-378.512177,-88.643440,46.518566>>, 2.400000)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player pushed Epsilonist down the stairs ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[0])
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[0],PLAYER_PED_ID(),100.0)
				blipEpsilonHiSecurity[0] = CREATE_PED_BLIP(pedEpsilonHiSecurity[0],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecurity[0],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecurity[0],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[2])
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[2],PLAYER_PED_ID(),100.0)	
				blipEpsilonHiSecurity[2] = CREATE_PED_BLIP(pedEpsilonHiSecurity[2],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecurity[2],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecurity[2],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[3])
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[3],PLAYER_PED_ID(),100.0)	
				blipEpsilonHiSecurity[3] = CREATE_PED_BLIP(pedEpsilonHiSecurity[3],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecurity[3],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecurity[3],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[4])
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[4],PLAYER_PED_ID(),100.0)	
				blipEpsilonHiSecurity[4] = CREATE_PED_BLIP(pedEpsilonHiSecurity[4],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecurity[4],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecurity[4],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[5],PLAYER_PED_ID(),100.0)	
				blipEpsilonHiSecurity[5] = CREATE_PED_BLIP(pedEpsilonHiSecurity[5],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecurity[5],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecurity[5],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "**** Player killed epsilonist ****")
		#ENDIF
		bPlayerStoleMoney = TRUE
	ENDIF
	
	//If anyone dies -> everyone kick off
	IF IS_ENTITY_DEAD(pedEpsilonHiSecurity[0])
	OR IS_ENTITY_DEAD(pedEpsilonHiSecurity[2])
	OR IS_ENTITY_DEAD(pedEpsilonHiSecurity[3])
	OR IS_ENTITY_DEAD(pedEpsilonHiSecurity[4])
	OR IS_ENTITY_DEAD(pedEpsilonHiSecurity[5])
	OR IS_ENTITY_DEAD(pedPilot)
	OR IS_ENTITY_DEAD(pedChopperShotgun)
	OR IS_PED_BEING_JACKED(pedPilot)
	OR IS_PED_BEING_JACKED(pedChopperShotgun)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "**** Player killed someone or tried to jack heli ****")
		#ENDIF
		bPlayerStoleMoney = TRUE
	ENDIF
	
	//If player jacks any epsilon cars -> everyone kicks off
	IF DOES_ENTITY_EXIST(vehEpsilonCar[i])
		IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])
			IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(vehEpsilonCar[i]), WEAPONTYPE_GRENADE, 4, TRUE)
			OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(vehEpsilonCar[i]), WEAPONTYPE_MOLOTOV, 4, TRUE)
			OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(vehEpsilonCar[i]), WEAPONTYPE_SMOKEGRENADE, 4, TRUE)
			OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(vehEpsilonCar[i]), WEAPONTYPE_STICKYBOMB, 4, TRUE)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Projectile belonging to player near an Epsilon vehicle ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonCar[i])
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player jacked a security car ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
		ELSE
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF
	
	//If player jacks any epsilon cars -> everyone kicks off
	IF DOES_ENTITY_EXIST(pedEpsilonHiSecDriver[i])
		IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecDriver[i])
			IF IS_PED_BEING_JACKED(pedEpsilonHiSecDriver[i])
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player jacked a security car ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
		ELSE
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF
	
	//If player jacks any epsilon cars -> everyone kicks off
	IF DOES_ENTITY_EXIST(pedEpsilonHiSecShotgun[i])
		IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecShotgun[i])
			IF IS_PED_BEING_JACKED(pedEpsilonHiSecShotgun[i])
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player jacked a security car ****")
				#ENDIF
				bPlayerStoleMoney = TRUE
			ENDIF
		ELSE
			bPlayerStoleMoney = TRUE
		ENDIF
	ENDIF

	////If player jacks/hurts helicopter -> everyone kicks off
	IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)
		IF IS_ENTITY_IN_RANGE_ENTITY(vehEpsilonChopper,PLAYER_PED_ID(),3)
		OR IS_ENTITY_IN_RANGE_ENTITY(vehEpsilonChopper,vehPlayerCar,3)
		OR GET_DISTANCE_BETWEEN_ENTITIES(vehEpsilonChopper,GET_PLAYERS_LAST_VEHICLE()) <= 3
			ACTIVATE_PHYSICS(vehEpsilonChopper)
		ENDIF
		IF IS_CHAR_USING_VEHICLE(PLAYER_PED_ID(),vehEpsilonChopper)
			//bChopperBeingJacked = TRUE     //Comment in to enable stealing chopper
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player jacking heli ****")
			#ENDIF
			bPlayerStoleMoney = TRUE
		ENDIF
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonChopper)
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehEpsilonChopper,PLAYER_PED_ID())
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehEpsilonChopper,vehPlayerCar)
		//OR IS_ENTITY_TOUCHING_ENTITY(vehEpsilonChopper,PLAYER_PED_ID())
		OR IS_ENTITY_TOUCHING_ENTITY(vehEpsilonChopper,vehPlayerCar)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Player damaged heli ****")
			#ENDIF
			//FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
			bPlayerStoleMoney = TRUE
		ENDIF
		//IF iSeqDelayAggression <> 0
			//IF IS_ENTITY_ALIVE(vehPlayerCar)
				/*
				IF IS_ENTITY_IN_RANGE_ENTITY(vehPlayerCar,vehEpsilonChopper,4.6)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "**** Player car too close to heli ****")
					#ENDIF
					FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
					bPlayerStoleMoney = TRUE
				ENDIF
				*/
			//ENDIF
		//ENDIF
	ENDIF
	/*
	//Random anims on the security guards
	iRandomInt = GET_RANDOM_INT_IN_RANGE(0,40)               
	IF bPlayerStoleMoney = FALSE	
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[i])	
			IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[i],"rcmepsilonism8","think")
			AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[i],"rcmepsilonism8","think_b")
			AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[i],"rcmepsilonism8","security_greet")
				IF i = 0
				OR i = 2
				OR i = 3
				OR i = 4
					IF iRandomInt = 1	
						TASK_PLAY_ANIM(pedEpsilonHiSecurity[i],"rcmepsilonism8","think",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
					ENDIF
					IF iRandomInt = 2	
						TASK_PLAY_ANIM(pedEpsilonHiSecurity[i],"rcmepsilonism8","think_b",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	*/
	++i
	
	IF i = 7
		i = 0
	ENDIF
	
ENDPROC

/// PURPOSE: //Ai for epsilonist at the helicopter
PROC EpsilonistEscortGuyAi()     
	
	IF bNoCashLeftInCar = FALSE	
		IF iSeqDelayAggressionParkedUp = 0	
			vCashCar = GET_ENTITY_COORDS(vehPlayerCar)     //If player's car moves a bit after being parked -> everyone kicks off 
			IF vCashCar.x < vCashCarParkedPos.x - 0.3
			OR vCashCar.x > vCashCarParkedPos.x + 0.3
			OR vCashCar.y < vCashCarParkedPos.y - 0.3
			OR vCashCar.y > vCashCarParkedPos.y + 0.3	
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Player moved car after parking it ****")
				#ENDIF
				iSeqDelayAggressionParkedUp = 1
				iTimerDelayAggression = GET_GAME_TIMER()
			ENDIF
		ENDIF
		IF iSeqDelayAggressionParkedUp = 1
			IF GET_GAME_TIMER() > iTimerDelayAggression + 3500
			AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)			
				bPlayerStoleMoney = TRUE
			ENDIF	
		ENDIF
	ENDIF

	//Epsilonist escorts player to the tractor, plays anims once he gets there, threatens him if he gets back in the car 	 
	IF IS_PED_UNINJURED(pedEpsilonist)	
	AND IS_VEHICLE_OK(vehTractor)	
		IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,vehTractor,15.0)		
			IF NOT IS_PED_HEADTRACKING_ENTITY(pedEpsilonist,vehTractor)	
				TASK_LOOK_AT_ENTITY(pedEpsilonist,vehTractor,-1)
			ENDIF
		ENDIF
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)		
		//AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")	
			//IF IS_VEHICLE_OK(vehTractor)			
				IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,vehTractor,5.0)		
					IF bEpsilonistPlayingAnims = FALSE					
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedEpsilonist,FALSE)
							OPEN_SEQUENCE_TASK(seqPrayToTractor)
								//TASK_CLEAR_LOOK_AT(NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehTractor)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
								//TASK_SET_DECISION_MAKER(NULL,DECISION_MAKER_EMPTY)
								//TASK_PLAY_ANIM(NULL,"rcmepsilonism8","c1_pose",2,-2,-1)
								TASK_PLAY_ANIM(NULL,"rcmepsilonism8","worship_enter",2,-2,-1,AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL,"rcmepsilonism8","worship_base",2,-2,-1,AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL,"rcmepsilonism8","worship_idle_a",2,-2,-1,AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
								TASK_PLAY_ANIM(NULL,"rcmepsilonism8","worship_base",2,-2,-1,AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seqPrayToTractor)
							TASK_PERFORM_SEQUENCE(pedEpsilonist,seqPrayToTractor)
							CLEAR_SEQUENCE_TASK(seqPrayToTractor)
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_LOW)
							
							bEpsilonistPlayingAnims = TRUE
						ENDIF
					ENDIF

					bEpsilonistAtTractor = TRUE
					
					IF bPlayerWentToTractor = TRUE
						IF bPrayConvo = FALSE	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF CREATE_CONVERSATION(s_conversation_peds,"EPS8AU","EPS8_PRAY",CONV_PRIORITY_MEDIUM)
									bMikeLookAtEpsilonist = FALSE
									bPrayConvo = TRUE	
									iTimerPrayConvo = GET_GAME_TIMER()
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),vehTractor,10000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			//ENDIF
			IF bEpsilonistAtTractor = FALSE
				IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),9.0)		
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")		
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,vehEpsilonChopper,8)		
					//AND GET_GAME_TIMER() > 	iTimerWaypointRec + 2000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF iEpsilonistBeckonConvo = 0                //Be a thesis. Cris loves you, brother-brother. 
								IF CREATE_CONVERSATION(s_conversation_peds,"EPS8AU","EPS8_E1",CONV_PRIORITY_MEDIUM)
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_LOW)
									//TASK_PLAY_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_UPPERBODY | AF_SECONDARY | AF_TAG_SYNC_CONTINUOUS)
									iEpsilonistBeckonConvo = 3  
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bOverrideStartWalk = FALSE	
					FLOAT fStartTagOutEp1
					FLOAT fEndTagOutEp1 
					IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","jump_off_heli")
					AND FIND_ANIM_EVENT_PHASE("rcmepsilonism8","jump_off_heli","WalkInterruptible",fStartTagOutEp1,fEndTagOutEp1)
					AND GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonist,"rcmepsilonism8","jump_off_heli") >= fStartTagOutEp1
					//IF GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonist,"rcmepsilonism8","jump_off_heli") >= 0.9022
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedEpsilonist)	
							FORCE_PED_MOTION_STATE(pedEpsilonist,MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT)
							SET_PED_MIN_MOVE_BLEND_RATIO(pedEpsilonist,PEDMOVE_WALK)
							//TASK_FOLLOW_WAYPOINT_RECORDING(pedEpsilonist,"Eps8EPED",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							TASK_GO_STRAIGHT_TO_COORD(pedEpsilonist,<<-377.2280, -87.0252, 44.6583>>,1)
							bOverrideStartWalk = TRUE
							iTimerOverrideStartWalk = GET_GAME_TIMER()
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "FOUND ANIM TAG")
							#ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","jump_off_heli")
							IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedEpsilonist)	
								FORCE_PED_MOTION_STATE(pedEpsilonist,MS_ON_FOOT_WALK,FALSE,FAUS_DEFAULT)
								SET_PED_MIN_MOVE_BLEND_RATIO(pedEpsilonist,PEDMOVE_WALK)
								//TASK_FOLLOW_WAYPOINT_RECORDING(pedEpsilonist,"Eps8EPED",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
								TASK_GO_STRAIGHT_TO_COORD(pedEpsilonist,<<-377.2280, -87.0252, 44.6583>>,1)
								bOverrideStartWalk = TRUE	
								iTimerOverrideStartWalk = GET_GAME_TIMER()
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MISSION, "DIDNT FIND ANIM TAG")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bOverrideStartWalk
				AND GET_GAME_TIMER() > iTimerOverrideStartWalk + 2000	
					IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),6.0)			
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedEpsilonist)
							TASK_FOLLOW_WAYPOINT_RECORDING(pedEpsilonist,"Eps8EPED",0,EWAYPOINT_START_FROM_CLOSEST_POINT)	
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),9.0)	
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),6.0)	
						IF NOT IS_PED_HEADTRACKING_PED(pedEpsilonist,PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
						ENDIF
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
						//AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")		
							IF NOT IS_PED_FACING_PED(pedEpsilonist,PLAYER_PED_ID(),20)		
								TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								IF NOT ARE_STRINGS_EQUAL(root,"EPS8_E1")		  	
									//IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")
									//	STOP_ANIM_TASK(pedEpsilonist,"rcmepsilonism8","epsilonist_talks",REALLY_SLOW_BLEND_OUT)
									//ENDIF
									TASK_TURN_PED_TO_FACE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),0)
								ENDIF
								//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_15")		
								/*
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")		
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
										IF iEpsilonistBeckonConvo = 0                //The fourth paradigm awaits.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_E1", "EPS8_E1_1", CONV_PRIORITY_VERY_LOW)
												iEpsilonistBeckonConvo = 1     
											ENDIF
										ELIF iEpsilonistBeckonConvo = 1              //This way Zondar.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_E1", "EPS8_E1_2", CONV_PRIORITY_VERY_LOW)
												iEpsilonistBeckonConvo = 2
											ENDIF
										ELIF iEpsilonistBeckonConvo = 2             //Follow me.
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_E1", "EPS8_E1_3", CONV_PRIORITY_VERY_LOW)
												iEpsilonistBeckonConvo = 3
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								*/
							ELSE
								IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_ANY) = PERFORMING_TASK	
									CLEAR_PED_TASKS(pedEpsilonist)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),9.0)	
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
						//AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")		
							TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
							TASK_GO_TO_ENTITY(pedEpsilonist,PLAYER_PED_ID(),DEFAULT_TIME_BEFORE_WARP,6.1,PEDMOVEBLENDRATIO_WALK)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE	
			IF bNoCashLeftInCar = FALSE
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
			//AND NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")		
				TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				//IF NOT ARE_STRINGS_EQUAL(root,"EPS8_E1")		  	
					//IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")
					//	STOP_ANIM_TASK(pedEpsilonist,"rcmepsilonism8","epsilonist_talks",REALLY_SLOW_BLEND_OUT)
					//ENDIF
				//ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(root)
					IF ARE_STRINGS_EQUAL(root,"EPS8_E1")				
						KILL_ANY_CONVERSATION()
					ENDIF
				ENDIF	
				IF iSeqThreatenPlayerInCar = 0
					TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
					IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,vehPlayerCar,7.0)	
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
						ENDIF
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_15")	
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")		
						AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_SEC", "EPS8_SEC_1", CONV_PRIORITY_MEDIUM)  //Ok... Get out of the car please.	
									iSeqThreatenPlayerInCar = 1
									iTimerThreatenPlayerInCar = GET_GAME_TIMER()	
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonist,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,2.5,0.0>>),PEDMOVEBLENDRATIO_RUN,DEFAULT_TIME_BEFORE_WARP,4.5)
						ENDIF
					ENDIF
				ELIF iSeqThreatenPlayerInCar = 1
					IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
						TASK_TURN_PED_TO_FACE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
					ENDIF
					IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])	
						IF GET_GAME_TIMER() > iTimerThreatenPlayerInCar + 300	
							IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
								TASK_TURN_PED_TO_FACE_ENTITY(pedEpsilonHiSecurity[5],PLAYER_PED_ID(),-1)
							ENDIF
						ENDIF	
					ENDIF
					IF GET_GAME_TIMER() > iTimerThreatenPlayerInCar + 6000 					
						//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_SEC", "EPS8_SEC_2", CONV_PRIORITY_MEDIUM)  //Out of the car or I blow your fucking head off!
								iSeqThreatenPlayerInCar = 2
								iTimerThreatenPlayerInCar = GET_GAME_TIMER()
							ENDIF
						//ENDIF
					ENDIF
				ELIF iSeqThreatenPlayerInCar = 2
					IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
						GIVE_WEAPON_TO_PED(pedEpsilonist,WEAPONTYPE_COMBATPISTOL,-1,TRUE,TRUE)
						GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonist,WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
						TASK_AIM_GUN_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
					ENDIF
					IF GET_GAME_TIMER() > iTimerThreatenPlayerInCar + 10000 
						iSeqThreatenPlayerInCar = 3
						iTimerThreatenPlayerInCar = GET_GAME_TIMER()
					ENDIF
				ELIF iSeqThreatenPlayerInCar = 3
					IF NOT IS_PED_IN_COMBAT(pedEpsilonist,PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "**** Player wouldn't leave car so epsilonist kicked off ****")
						#ENDIF
						IF GET_SCRIPT_TASK_STATUS(pedEpsilonist,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
							TASK_COMBAT_PED(pedEpsilonist,PLAYER_PED_ID())
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: //Player stole money so make epsilon guys hostile and blip them
PROC SetupEpsilonHostile() 

	SAFE_REMOVE_BLIP(blipEscort)
	SAFE_REMOVE_BLIP(blipEpsOnFootEscort)

	IF IS_PED_UNINJURED(pedEpsilonist)	
		IF NOT DOES_BLIP_EXIST(blipRandomChar)	
			IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),fOnFootEscapeDistance)	
				IF NOT IS_PED_IN_ANY_VEHICLE(pedEpsilonist)
					SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_USE_VEHICLE,FALSE)
					blipRandomChar = CREATE_PED_BLIP(pedEpsilonist,TRUE,FALSE,BLIPPRIORITY_LOW)
					SET_BLIP_SCALE(blipRandomChar,fSmallBlipScale)
					TASK_COMBAT_PED(pedEpsilonist,PLAYER_PED_ID())
					++iEpSecBlipped
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[i])		
		IF NOT DOES_BLIP_EXIST(blipEpsilonHiSecDriver[i])		
			IF IS_PED_IN_COMBAT(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())
			OR GET_GAME_TIMER() > iTimerPlayerStoleMoney + 20000	
			OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-335.858063,-97.340446,62.208683>>, <<-403.093201,-76.600517,25.804146>>, 34.000000)
				blipEpsilonHiSecDriver[i] = CREATE_PED_BLIP(pedEpsilonHiSecDriver[i],TRUE,FALSE,BLIPPRIORITY_MED)
				SET_BLIP_SCALE(blipEpsilonHiSecDriver[i],fLargeBlipScale)
				IF i = 0
					IF NOT IS_ENTITY_DEAD(vehEpsilonCar[0])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[0])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonCar[0])
						ENDIF
					ENDIF   
				ENDIF               //Init Driving AI here
				IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])
					IF bPlayerAtHelipad = TRUE
						IF i = 0
							IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonCar[i])     //telling first landstalker guys to get out if they're in the compound
								SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[i],CA_USE_VEHICLE,FALSE)
								IF DOES_ENTITY_EXIST(pedEpsilonHiSecShotgun[i])		
									IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecShotgun[i])
										IF NOT IS_PED_INJURED(pedEpsilonHiSecShotgun[i])	
											SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[i],CA_USE_VEHICLE,FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
						
						//DRIVING AI SWITCH
					TASK_VEHICLE_ESCORT(pedEpsilonHiSecDriver[i],vehEpsilonCar[i],PLAYER_PED_ID(),VEHICLE_ESCORT_FRONT,200.0,DRIVINGMODE_AVOIDCARS_RECKLESS)	
					//TASK_COMBAT_PED(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())

				ELSE
					TASK_COMBAT_PED(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())
				ENDIF
				++iEpSecBlipped
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecShotgun[i])					
		IF NOT DOES_BLIP_EXIST(blipEpsilonHiSecShotgun[i])		
			IF IS_PED_IN_COMBAT(pedEpsilonHiSecShotgun[i],PLAYER_PED_ID())		
			OR GET_GAME_TIMER() > iTimerPlayerStoleMoney + 20100	
			OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-335.858063,-97.340446,62.208683>>, <<-403.093201,-76.600517,25.804146>>, 34.000000)		
				blipEpsilonHiSecShotgun[i] = CREATE_PED_BLIP(pedEpsilonHiSecShotgun[i],TRUE,FALSE,BLIPPRIORITY_LOW)
				SET_BLIP_SCALE(blipEpsilonHiSecShotgun[i],fSmallBlipScale)
				TASK_COMBAT_PED(pedEpsilonHiSecShotgun[i],PLAYER_PED_ID())
				++iEpSecBlipped
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(pedEpsilonHiSecurity[i])			
		IF NOT DOES_BLIP_EXIST(blipEpsilonHiSecurity[i])		
			IF IS_PED_IN_COMBAT(pedEpsilonHiSecurity[i],PLAYER_PED_ID())	
			OR GET_GAME_TIMER() > iTimerPlayerStoleMoney + 50		
				IF NOT IS_PED_IN_ANY_VEHICLE(pedEpsilonHiSecurity[i])		
					IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[i],PLAYER_PED_ID(),fOnFootEscapeDistance)		
						IF i = 5
							SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[i],CA_USE_VEHICLE,FALSE)
						ENDIF
						blipEpsilonHiSecurity[i] = CREATE_PED_BLIP(pedEpsilonHiSecurity[i],TRUE,FALSE,BLIPPRIORITY_LOW)
						SET_BLIP_SCALE(blipEpsilonHiSecurity[i],fSmallBlipScale)
						TASK_COMBAT_PED(pedEpsilonHiSecurity[i],PLAYER_PED_ID())
						++iEpSecBlipped
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF bHelicopterHostile = FALSE
		IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 300
			IF IS_VEHICLE_OK(vehEpsilonChopper)		       
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
				ENDIF
				//FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
				SET_VEHICLE_ENGINE_ON(vehEpsilonChopper,TRUE,TRUE)
				IF NOT IS_ENTITY_DEAD(pedPilot)	
					IF NOT DOES_BLIP_EXIST(blipPilot)		
						blipPilot = ADD_BLIP_FOR_ENTITY(pedPilot)
						IF IS_PED_IN_VEHICLE(pedPilot,vehEpsilonChopper)
						AND bChopperBeingJacked = FALSE	                   //If player jacks heli -> everyone get out of heli
							SET_BLIP_SCALE(blipPilot,fLargeBlipScale)
							SET_BLIP_PRIORITY(blipPilot,BLIPPRIORITY_HIGH)
							SET_BLIP_SPRITE(blipPilot,RADAR_TRACE_ENEMY_HELI_SPIN)
							SET_BLIP_COLOUR(blipPilot,BLIP_COLOUR_RED)
						ELSE
							SET_BLIP_SCALE(blipPilot,fSmallBlipScale)
							SET_BLIP_PRIORITY(blipPilot,BLIPPRIORITY_LOW)
							IF IS_PED_UNINJURED(pedPilot)
								IF IS_PED_IN_VEHICLE(pedPilot,vehEpsilonChopper)
									TASK_LEAVE_VEHICLE(pedPilot,vehEpsilonChopper)
								ENDIF
							ENDIF
							IF IS_PED_UNINJURED(pedEpsilonist)
								IF IS_PED_IN_VEHICLE(pedEpsilonist,vehEpsilonChopper)
									TASK_LEAVE_VEHICLE(pedEpsilonist,vehEpsilonChopper)
								ENDIF
							ENDIF
							IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])
								IF IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper)
									TASK_LEAVE_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper)
								ENDIF
							ENDIF
							IF IS_PED_UNINJURED(pedChopperShotgun)
								IF IS_PED_IN_VEHICLE(pedChopperShotgun,vehEpsilonChopper)
									TASK_LEAVE_VEHICLE(pedChopperShotgun,vehEpsilonChopper)
								ENDIF
							ENDIF
						ENDIF
						++iEpSecBlipped
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(pedChopperShotgun)		
					TASK_COMBAT_PED(pedChopperShotgun,PLAYER_PED_ID())
				ENDIF
			ENDIF
			bHelicopterHostile = TRUE
		ENDIF
	ENDIF
	
	++i
	
	IF i = 7
		i = 0
	ENDIF

ENDPROC

/// PURPOSE:  // Manage blips and stuff for hostile epsilon guys
PROC UpdateEpsilonHostile()   
	
	IF IS_ENTITY_ALIVE(vehEpsilonChopper)
		IF GET_HELI_MAIN_ROTOR_HEALTH(vehEpsilonChopper) < 200
		OR GET_HELI_TAIL_BOOM_HEALTH(vehEpsilonChopper)	< 200
		OR GET_HELI_TAIL_ROTOR_HEALTH(vehEpsilonChopper) < 200	
		OR GET_ENTITY_HEALTH(vehEpsilonChopper)	< 200
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehEpsilonChopper) < 200	
			SET_VEHICLE_OUT_OF_CONTROL(vehEpsilonChopper,FALSE)
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])                    // stop epsilon guys from warping back into their car if player is in it
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonCar[i])
			IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecDriver[i])
				SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[i],CA_USE_VEHICLE,FALSE)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecShotgun[i])
				SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecShotgun[i],CA_USE_VEHICLE,FALSE)
			ENDIF
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehEpsilonCar[i],FALSE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEpsilonCar[i])
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(pedEpsilonHiSecDriver[i])		
		IF NOT IS_PED_UNINJURED(pedEpsilonHiSecDriver[i])
			IF DOES_BLIP_EXIST(blipEpsilonHiSecDriver[i])
				SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[i])
				++iEpSecEvaded
				++iEpsilonistsKilled
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecDriver[i],PLAYER_PED_ID(),fEscapeDistance)	
				IF DOES_BLIP_EXIST(blipEpsilonHiSecDriver[i])
					IF NOT IS_ENTITY_ON_SCREEN(pedEpsilonHiSecDriver[i])	
						SAFE_REMOVE_BLIP(blipEpsilonHiSecDriver[i])
						SAFE_DELETE_PED(pedEpsilonHiSecDriver[i])
						IF DOES_ENTITY_EXIST(vehEpsilonCar[i])
							IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])
								IF NOT IS_ENTITY_ON_SCREEN(vehEpsilonCar[i])
									SAFE_DELETE_VEHICLE(vehEpsilonCar[i])
								ENDIF
							ENDIF
						ENDIF
						++iEpSecEvaded
					ENDIF
				ENDIF
			ELSE
				//IF IS_PED_DOING_DRIVEBY(pedEpsilonHiSecDriver[i])
				//	SET_PED_FIRING_PATTERN(pedEpsilonHiSecDriver[i],FIRING_PATTERN_BURST_FIRE_DRIVEBY)
				//ENDIF
				IF DOES_BLIP_EXIST(blipEpsilonHiSecDriver[i])  //if the player just stays at compound shooting, make the guys drive to the closest entrance, then attack on foot
					IF i <> 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-367.561707,-107.370163,32.445591>>, <<-356.058563,-74.557419,50.422489>>, 82.250000)
							IF IS_ENTITY_IN_ANGLED_AREA(pedEpsilonHiSecDriver[i], <<-361.797729,-108.584549,67.124771>>, <<-347.599182,-65.633202,36.464310>>, 174.750000)
								SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[i],CA_USE_VEHICLE,FALSE)
								IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_COMBAT) <> PERFORMING_TASK	
									TASK_COMBAT_PED(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())
								ENDIF
								bEpsDriverForcedToLeaveVeh[i] = TRUE
							ELSE
								SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[i],CA_USE_VEHICLE,TRUE)
								IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])	
									IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedEpsilonHiSecDriver[i]),vCompoundBack) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedEpsilonHiSecDriver[i]),vCompoundFront)
											TASK_VEHICLE_DRIVE_TO_COORD(pedEpsilonHiSecDriver[i],vehEpsilonCar[i],vCompoundBack,50.0,DRIVINGSTYLE_NORMAL,GET_ENTITY_MODEL(vehEpsilonCar[i]),DRIVINGMODE_AVOIDCARS_RECKLESS,7.0,1.0)
										ELSE
											TASK_VEHICLE_DRIVE_TO_COORD(pedEpsilonHiSecDriver[i],vehEpsilonCar[i],vCompoundFront,50.0,DRIVINGSTYLE_NORMAL,GET_ENTITY_MODEL(vehEpsilonCar[i]),DRIVINGMODE_AVOIDCARS_RECKLESS,7.0,1.0)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecDriver[i],CA_USE_VEHICLE,TRUE)
							IF bEpsDriverForcedToLeaveVeh[i] = TRUE	
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])		
										IF IS_PED_IN_VEHICLE(pedEpsilonHiSecDriver[i],vehEpsilonCar[i])
											bEpsDriverForcedToLeaveVeh[i] = FALSE
										ELSE
											IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[i])		
												IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
													TASK_ENTER_VEHICLE(pedEpsilonHiSecDriver[i],vehEpsilonCar[i])
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
							
									//DRIVING AI SWITCH	
								IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_COMBAT) <> PERFORMING_TASK	
									TASK_COMBAT_PED(pedEpsilonHiSecDriver[i],PLAYER_PED_ID())
								ENDIF
								//IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK        //comment back in for proper driving ai
								//	TASK_VEHICLE_ESCORT(pedEpsilonHiSecDriver[i],vehEpsilonCar[i],PLAYER_PED_ID(),VEHICLE_ESCORT_FRONT,200.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								//ENDIF
							
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(vehEpsilonCar[i])		
							IF IS_PED_IN_VEHICLE(pedEpsilonHiSecDriver[i],vehEpsilonCar[i])	
								//IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecDriver[i],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK        //comment back in for proper driving ai
								//	TASK_VEHICLE_ESCORT(pedEpsilonHiSecDriver[i],vehEpsilonCar[i],PLAYER_PED_ID(),VEHICLE_ESCORT_FRONT,200.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								//ENDIF
							ENDIF
						ENDIF
					ENDIF	
					IF IS_PED_SITTING_IN_ANY_VEHICLE(pedEpsilonHiSecDriver[i])
						SET_BLIP_SCALE(blipEpsilonHiSecDriver[i],fLargeBlipScale)
						//vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
						//vEpsilonHiSecDriver[i] = GET_ENTITY_COORDS(pedEpsilonHiSecDriver[i])
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF bDriveByTaskGiven[i] = FALSE
								IF fDistBetweenPlayerAndDriver <= 50.0	
									//TASK_DRIVE_BY(pedEpsilonHiSecDriver[i],PLAYER_PED_ID(),NULL,<<0.0,0.0,0.0>>,600.0,70,TRUE)
									bDriveByTaskGiven[i] = TRUE
								ENDIF
							ELSE	
								IF fDistBetweenPlayerAndDriver > 50.0		
									bDriveByTaskGiven[i] = FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_BLIP_SCALE(blipEpsilonHiSecDriver[i],fSmallBlipScale)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
		
	IF DOES_ENTITY_EXIST(pedEpsilonHiSecShotgun[i])		
		IF NOT IS_PED_UNINJURED(pedEpsilonHiSecShotgun[i])
			IF DOES_BLIP_EXIST(blipEpsilonHiSecShotgun[i])
				SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[i])
				++iEpSecEvaded
				++iEpsilonistsKilled
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecShotgun[i],PLAYER_PED_ID(),fEscapeDistance)	
				IF DOES_BLIP_EXIST(blipEpsilonHiSecShotgun[i])
					IF NOT IS_ENTITY_ON_SCREEN(pedEpsilonHiSecShotgun[i])	
						SAFE_REMOVE_BLIP(blipEpsilonHiSecShotgun[i])
						SAFE_DELETE_PED(pedEpsilonHiSecShotgun[i])
						++iEpSecEvaded
					ENDIF
				ENDIF
			ELSE
				SET_PED_FIRING_PATTERN(pedEpsilonHiSecShotgun[i],FIRING_PATTERN_BURST_FIRE_DRIVEBY)
				//IF IS_PED_DOING_DRIVEBY(pedEpsilonHiSecShotgun[i])
					//SET_PED_FIRING_PATTERN(pedEpsilonHiSecShotgun[i],FIRING_PATTERN_BURST_FIRE_DRIVEBY)
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iVehThief <> -1
		IF NOT IS_PED_UNINJURED(pedEpsilonHiSecurity[iVehThief])
			iVehThief = -1
			bSecurityStealingTailgater = FALSE	
		ELSE
			IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[iVehThief],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[iVehThief],SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
				TASK_VEHICLE_MISSION_PED_TARGET(pedEpsilonHiSecurity[iVehThief],vehPlayerCar,PLAYER_PED_ID(),MISSION_FLEE,40.0,DRIVINGMODE_AVOIDCARS_RECKLESS,100.0,30.0)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Someone told to steal the car ****")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedEpsilonHiSecurity[i])		
		IF NOT IS_PED_UNINJURED(pedEpsilonHiSecurity[i])
			IF DOES_BLIP_EXIST(blipEpsilonHiSecurity[i])
				SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[i])
				++iEpSecEvaded
				++iEpsilonistsKilled
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[i],PLAYER_PED_ID(),fOnFootEscapeDistance)		
				IF NOT IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[i],vehPlayerCar)	
					IF DOES_BLIP_EXIST(blipEpsilonHiSecurity[i])
						SAFE_REMOVE_BLIP(blipEpsilonHiSecurity[i])
						IF iVehThief = i
							iVehThief = -1
							bSecurityStealingTailgater = FALSE	
						ENDIF
						++iEpSecEvaded
					ENDIF
				ENDIF
			ELSE
				IF i <> 5 //Don't make the guy in the heli try to steal the car	
					IF bSecurityStealingTailgater = FALSE	
						IF bNoCashLeftInCar = FALSE	
							IF IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonHiSecurity[i],vehPlayerCar,60)	
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehPlayerCar,30)		
									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
										IF IS_VEHICLE_SEAT_FREE(vehPlayerCar)	
											iVehThief = i
											IF bPlayerAtHelipad = TRUE	
												SET_ROADS_IN_ANGLED_AREA(<<-397.618073,-63.278549,42.658855>>, <<-345.021851,-93.334587,49.657681>>, 24.750000,FALSE,TRUE)
											ENDIF
											CLEAR_PED_TASKS(pedEpsilonHiSecurity[iVehThief])
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonHiSecurity[iVehThief],TRUE)
											SET_PED_COMBAT_ATTRIBUTES(pedEpsilonHiSecurity[iVehThief],CA_USE_VEHICLE,TRUE)
											bSecurityStealingTailgater = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF i = 1
	OR i = 4
		IF NOT IS_PED_UNINJURED(pedEpsilonist)
			IF DOES_BLIP_EXIST(blipRandomChar)
				SAFE_REMOVE_BLIP(blipRandomChar)
				REMOVE_PED_FOR_DIALOGUE(s_conversation_peds,2)
				++iEpSecEvaded
				++iEpsilonistsKilled
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedEpsilonist,PLAYER_PED_ID(),fOnFootEscapeDistance)			
				IF DOES_BLIP_EXIST(blipRandomChar)
					SAFE_REMOVE_BLIP(blipRandomChar)
					++iEpSecEvaded
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF i = 2
	OR i = 6      
		IF NOT IS_PED_UNINJURED(pedPilot)
			IF DOES_BLIP_EXIST(blipPilot)
				IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)
					//SET_ENTITY_COLLISION(vehEpsilonChopper,TRUE)
					SET_VEHICLE_OUT_OF_CONTROL(vehEpsilonChopper,FALSE)
					bOverrideHeliColl = TRUE
				ENDIF
				iTimerHeliDead = GET_GAME_TIMER()
				REMOVE_PED_FOR_DIALOGUE(s_conversation_peds,3)
				SAFE_REMOVE_BLIP(blipPilot)
				++iEpSecEvaded
				++iEpsilonistsKilled
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedPilot,PLAYER_PED_ID(),fHeliEscapeDistance)		
			AND GET_GAME_TIMER() > iTimerPlayerStoleMoney + 15000	
				IF DOES_BLIP_EXIST(blipPilot)
					IF NOT IS_ENTITY_ON_SCREEN(pedPilot)
						iTimerHeliDead = GET_GAME_TIMER()
						REMOVE_PED_FOR_DIALOGUE(s_conversation_peds,3)
						SAFE_REMOVE_BLIP(blipPilot)
						SAFE_DELETE_PED(pedPilot)
						IF DOES_ENTITY_EXIST(vehEpsilonChopper)
							IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)	
								IF NOT IS_ENTITY_DEAD(pedChopperShotgun)		
									IF IS_PED_IN_VEHICLE(pedChopperShotgun,vehEpsilonChopper)
										SAFE_DELETE_PED(pedChopperShotgun)
									ENDIF
								ENDIF
								IF bChopperBeingJacked = FALSE
									IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
										IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonChopper)	
											IF NOT IS_ENTITY_ON_SCREEN(vehEpsilonChopper)
												SAFE_DELETE_VEHICLE(vehEpsilonChopper)
											ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						++iEpSecEvaded
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	/*
	IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)             // Detach sniper if player steals heli, kill shooter and pilot if heli crashes
		IF NOT IS_VEHICLE_DRIVEABLE(vehEpsilonChopper)
			IF NOT IS_ENTITY_DEAD(pedChopperShotgun)	
				SET_ENTITY_HEALTH(pedChopperShotgun,0)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedPilot)	
				SET_ENTITY_HEALTH(pedPilot,0)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_DEAD(pedChopperShotgun)
			SET_ENTITY_HEALTH(pedChopperShotgun,0)
		ENDIF
	ENDIF		
	*/
				
	++i
	
	IF i = 7
		i = 0
	ENDIF

ENDPROC

/// PURPOSE: // Handle Ai/dialogue for helicopter
PROC HelicopterHostile()  

	IF IS_PED_UNINJURED(pedPilot)	
		IF IS_VEHICLE_OK(vehEpsilonChopper)		
			IF IS_PED_IN_VEHICLE(pedPilot,vehEpsilonChopper)		
				IF IS_ENTITY_ALIVE(vehPlayerCar)
					IF CONTROL_MOUNTED_WEAPON(pedPilot)
						SET_MOUNTED_WEAPON_TARGET(pedPilot,NULL,vehPlayerCar,<<0,0,0>>) 
					ENDIF
				ENDIF
				IF bChopperBeingJacked = FALSE	
					vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
					iZ = ROUND(vPlayer.z + 20.0)
					IF iZ = 0
					ENDIF
					IF iHeliGivenUp < 2
						IF iSeqHeli = 0
							IF bHeliCantSeePlayer = FALSE	
								IF bOnlyHeliLeft = FALSE
									
									//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,PLAYER_PED_ID(),vPlayer,MISSION_POLICE_BEHAVIOUR,fHeliSpeed,2.0,-1,(iZ + 10),35) //target reach 2.0
									//TASK_HELI_CHASE(pedPilot,PLAYER_PED_ID(),<<0,-20,0>>)
									IF IS_ENTITY_ALIVE(vehPlayerCar)
										TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,vehPlayerCar,NULL,<<0,0,0>>,MISSION_GOTO,fHeliSpeed,50,-1,-1,40)  //MISSION_GOTO
									ENDIF
									//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,PLAYER_PED_ID(),vPlayer,MISSION_GOTO,fHeliSpeed,-1,-1,(iZ + 10),35)
								
									//IF NOT IS_ENTITY_DEAD(vehPlayerCar)
									//	TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,vehPlayerCar,NULL,GET_ENTITY_COORDS(vehPlayerCar),MISSION_GOTO,fHeliSpeed,-1,-1,(iZ + 10),35)
									//ENDIF
								
								ENDIF
							ELSE
								TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,iZ + 100,100)
							ENDIF
							iTimerHeliSeq = GET_GAME_TIMER()
							iSeqHeli = 1
						ELIF iSeqHeli = 1
							IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 10000
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehEpsilonChopper,FALSE) < 85.0	
									IF iNodeNumber < 25	
										GET_NTH_CLOSEST_VEHICLE_NODE(vPlayer,iNodeNumber,vHeliTarget)
										IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vHeliTarget,65.0)
										AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vHeliTarget,40.0)
											//fHeadingHeliTarget = GET_HEADING_FROM_COORDS(vHeliTarget,vPlayer)
											IF NOT IS_ENTITY_DEAD(pedChopperShotgun)
												IF bHeliCantSeePlayer = FALSE	
													//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,vHeliTarget,MISSION_GOTO,5.1,5.0,fHeadingHeliTarget + 90.0,(iZ),24)
												ELSE
													//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,iZ + 100,100)
												ENDIF
											ELSE
												IF bHeliCantSeePlayer = FALSE
													//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,vHeliTarget,MISSION_GOTO,5.1,5.0,fHeadingHeliTarget,(iZ),24)
												ELSE
													//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,iZ + 100,100)
												ENDIF
											ENDIF
											iTimerHeliSeq = GET_GAME_TIMER()
											iSeqHeli = 2
											iNodeNumber = 1
										ELSE
											++iNodeNumber
										ENDIF	
									ELSE		
										IF NOT IS_ENTITY_DEAD(pedChopperShotgun)
											IF bHeliCantSeePlayer = FALSE		
												//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,40.0,0.0>>),MISSION_GOTO,10.0,2.0,(GET_ENTITY_HEADING(PLAYER_PED_ID()) - 90.0),(iZ + 10),35)
											ELSE
												//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,iZ + 100,100)
											ENDIF
										ELSE
											IF bHeliCantSeePlayer = FALSE			
												//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,40.0,0.0>>),MISSION_GOTO,10.0,2.0,(GET_ENTITY_HEADING(PLAYER_PED_ID()) + 180.0),(iZ + 10),35)
											ELSE
												//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,iZ + 100,100)
											ENDIF
										ENDIF										
										iTimerHeliSeq = GET_GAME_TIMER()
										iSeqHeli = 2	
										iNodeNumber = 1
									ENDIF
								ENDIF
							ENDIF
						ELIF iSeqHeli = 2
							iTimerHeliSeqUpdate = GET_GAME_TIMER()
							IF iTimerHeliSeqUpdate > iTimerHeliSeq + 20000		
							OR NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehEpsilonChopper,85.0)
							OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								iSeqHeli = 0
							ENDIF
						ENDIF	
						IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 8000
							IF iEpSecBlipped = iEpSecEvaded + 1	        //only the helicopter is left
							OR bOnlyHeliLeft = TRUE
								
								bOnlyHeliLeft = TRUE
								
								IF NOT IS_MOBILE_PHONE_CALL_ONGOING()	
									
									IF bPrintEvadeHeli = FALSE
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
											//PRINT_NOW("EPS8_24",DEFAULT_GOD_TEXT_TIME,0)
											bPrintEvadeHeli = TRUE
										ENDIF
									ENDIF
									
									IF bOnlyHeliLeftConvo = FALSE
										//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_24")
											IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_HEL3", CONV_PRIORITY_MEDIUM)   //Any guys on the ground still with us?
												bOnlyHeliLeftConvo = TRUE
											ENDIF
										//ENDIF
									ENDIF
									
									IF fHeliEscapeDistance > 300.0							
										fHeliEscapeDistance = fHeliEscapeDistance - 0.1  //0.2  //reducing heli escape distance
									ENDIF
									
									IF fHeliSpeed > 28.0
										fHeliSpeed = fHeliSpeed - 0.02  //0.04             //reducing heli speed
									ENDIF
								
								ENDIF

								IF iHeliTask = 0	
									//IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
										IF bHeliCantSeePlayer = FALSE	
											
											//IF NOT IS_ENTITY_DEAD(vehPlayerCar)
											//	TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,vehPlayerCar,NULL,GET_ENTITY_COORDS(vehPlayerCar),MISSION_GOTO,fHeliSpeed,2.0,-1,(iZ + 10),35)
											//ENDIF
											IF IS_ENTITY_ALIVE(vehPlayerCar)
												//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,vehPlayerCar,NULL,<<0,0,0>>,MISSION_POLICE_BEHAVIOUR,fHeliSpeed,2.0,-1,-1,50)  //target reach 2.0
												TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,vehPlayerCar,NULL,<<0,0,0>>,MISSION_GOTO,fHeliSpeed,50,-1,-1,40)
											ENDIF
											//TASK_HELI_CHASE(pedPilot,PLAYER_PED_ID(),<<0,-40,40>>)
											
											//TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,PLAYER_PED_ID(),vPlayer,MISSION_POLICE_BEHAVIOUR,fHeliSpeed,2.0,-1,(iZ + 10),35)
											
											SET_DRIVE_TASK_CRUISE_SPEED(pedPilot,fHeliSpeed)
											++iHeliTask
										ENDIF
									//ENDIF
								ELSE
									++iHeliTask
									IF iHeliTask > 30
										iHeliTask = 0
									ENDIF
								ENDIF
								
								IF iConvoChaseCar > 3	
									IF CAN_HELI_PILOT_SEE_PLAYER(pedPilot)	   //Heli gives up if it cant see player for a while
										iHeliGivenUp = 0
										IF bHeliCantSeePlayer = TRUE
											TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,PLAYER_PED_ID(),vPlayer,MISSION_CIRCLE,200.0,2.0,-1,(iZ + 10),50)
											IF iHeliNoLosConvoCounter > 0
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
													IF iHeliSpotPlayerConvo = 0                        //Ah! There he is!
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_HEL2", "EPS8_HEL2_1", CONV_PRIORITY_VERY_LOW)
															iHeliSpotPlayerConvo = 1
														ENDIF
													ELIF iHeliSpotPlayerConvo = 1                      //Eyes on the fucker!
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_HEL2", "EPS8_HEL2_2", CONV_PRIORITY_VERY_LOW)
															iHeliSpotPlayerConvo = 2
														ENDIF
													ENDIF
												ENDIF
											ENDIF	
										ENDIF	
										bHeliCantSeePlayer = FALSE
										IF IS_PED_UNINJURED(pedChopperShotgun)
											IF GET_SCRIPT_TASK_STATUS(pedChopperShotgun,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
												IF GET_SCRIPT_TASK_STATUS(pedChopperShotgun,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
													TASK_COMBAT_PED(pedChopperShotgun,PLAYER_PED_ID())	
												ENDIF
											ENDIF
										ENDIF	
									ELSE
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
											IF iHeliGivenUp = 0
												iTimerHeliLostSightOfPlayer = GET_GAME_TIMER()            
												iHeliGivenUp = 1
											ELIF iHeliGivenUp = 1
												iTimerHeliLostSightOfPlayerUpdate = GET_GAME_TIMER()
												IF iTimerHeliLostSightOfPlayerUpdate > iTimerHeliLostSightOfPlayer + 4000
													IF IS_PED_UNINJURED(pedChopperShotgun)
														CLEAR_PED_TASKS(pedChopperShotgun)
													ENDIF
													IF bHeliCantSeePlayer = FALSE	
														TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,GET_ENTITY_COORDS(vehEpsilonChopper),MISSION_GOTO,0.1,-1,-1,-1,100)
													ENDIF
													bHeliCantSeePlayer = TRUE
												ENDIF
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
													IF iTimerHeliLostSightOfPlayerUpdate > iTimerHeliLostSightOfPlayer + 10000
														IF iHeliNoLosConvoCounter = 0		//I can't see him... 
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_9", CONV_PRIORITY_VERY_LOW)
																iHeliNoLosConvoCounter = 1
															ENDIF
														ENDIF
													ENDIF
													IF iTimerHeliLostSightOfPlayerUpdate > iTimerHeliLostSightOfPlayer + 25000
														IF iHeliNoLosConvoCounter = 1		//Anyone see where he went?
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_10", CONV_PRIORITY_VERY_LOW)
																iHeliNoLosConvoCounter = 2
															ENDIF
														ENDIF
													ENDIF
													IF iTimerHeliLostSightOfPlayerUpdate > iTimerHeliLostSightOfPlayer + 40000          
														IF iHeliNoLosConvoCounter = 2		//I gotta head back, nearly out of fuel... Fuck.
															IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_CH2", "EPS8_CH2_11", CONV_PRIORITY_VERY_LOW)
																
																REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_LOW ) 
																
																iHeliNoLosConvoCounter = 3
																iHeliGivenUp = 2
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF                    
					ELSE                  //heli gives up and flies away
						TASK_HELI_MISSION(pedPilot,vehEpsilonChopper,NULL,NULL,<<vPlayer.x + 600.0,vPlayer.y + 600.0, vPlayer.z + 100.0>>,MISSION_GOTO,200.0,-1,-1,-1,100)
						SET_ENTITY_COLLISION(vehEpsilonChopper,FALSE)
						bOverrideHeliColl = TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:  //Checks all the fail conditions
PROC CheckForFail()    

	IF bNoCashLeftInCar = FALSE
		IF bChopperBeingJacked = FALSE	
			IF DOES_ENTITY_EXIST(vehPlayerCar)
				IF IS_ENTITY_DEAD(vehPlayerCar)
					IF bPlayerCarDestroyed = FALSE
						iTimerPlayerCarDestroyed = GET_GAME_TIMER()
						bPlayerCarDestroyed = TRUE
					ELSE
						IF GET_GAME_TIMER() > iTimerPlayerCarDestroyed + 100         //delay to fix assert      
							IF IS_ENTITY_IN_WATER(vehPlayerCar)
								sFailReason = "EPS8_19"            			// ~r~The car was wrecked.
							ELSE	
								sFailReason = "EPS8_18" 				// ~r~The cash was destroyed.
							ENDIF
							missionStage = MS_MISSION_FAILING
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar,VEH_STUCK_ON_ROOF,5000)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar,VEH_STUCK_ON_SIDE,20000)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar,VEH_STUCK_HUNG_UP,30000)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar,VEH_STUCK_JAMMED,60000)
						sFailReason = "EPS8_19"            			// ~r~The car was wrecked.
						missionStage = MS_MISSION_FAILING
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						IF bPlayerCarDestroyed = FALSE
							iTimerPlayerCarDestroyed = GET_GAME_TIMER()
							bPlayerCarDestroyed = TRUE
						ELSE
							IF GET_GAME_TIMER() > iTimerPlayerCarDestroyed + 100      //delay to fix assert      
								sFailReason = "EPS8_19" 			// ~r~The car was wrecked.
								missionStage = MS_MISSION_FAILING
							ENDIF
						ENDIF
					ENDIF
					IF missionStage <> MS_WALKING_TO_REWARD		
						IF bMessagePrintedLeavingCash = FALSE
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(vehPlayerCar,PLAYER_PED_ID(),150.0)
								IF iVehThief <> -1
								AND IS_PED_UNINJURED(pedEpsilonHiSecurity[iVehThief])
								AND IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[iVehThief],vehPlayerCar)
									PRINT_NOW("EPS8_25",DEFAULT_GOD_TEXT_TIME,0)       // Don't let the ~b~car~s~ get away.
								ELSE
									PRINT_NOW("EPS8_16",DEFAULT_GOD_TEXT_TIME,0)       // Don't leave the ~b~cash~s~ behind.
								ENDIF
								bMessagePrintedLeavingCash = TRUE
							ENDIF
						ELSE
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_25")	
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")	
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(vehPlayerCar,PLAYER_PED_ID(),230.0)      
									IF iVehThief <> -1
									AND IS_PED_UNINJURED(pedEpsilonHiSecurity[iVehThief])
									AND IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[iVehThief],vehPlayerCar)
										sFailReason = "EPS8_26"             // You lost the car.
									ELSE
										sFailReason = "EPS8_17" 			// ~r~You left the cash behind.
									ENDIF
									missionStage = MS_MISSION_FAILING
								ELIF IS_ENTITY_IN_RANGE_ENTITY(vehPlayerCar,PLAYER_PED_ID(),100.0)
									//bMessagePrintedLeavingCash = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehEpsilonChopper)
				IF IS_ENTITY_DEAD(vehEpsilonChopper)   
					sFailReason = "EPS8_18" 				// ~r~The cash was destroyed.
					missionStage = MS_MISSION_FAILING
				ELSE
					IF NOT IS_VEHICLE_DRIVEABLE(vehEpsilonChopper)
						sFailReason = "EPS8_18" 			// ~r~The cash was destroyed.
						missionStage = MS_MISSION_FAILING
					ENDIF
					IF missionStage <> MS_WALKING_TO_REWARD	
						IF bMessagePrintedLeavingCash = FALSE
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(vehEpsilonChopper,PLAYER_PED_ID(),150.0)
								PRINT_NOW("EPS8_16",DEFAULT_GOD_TEXT_TIME,0)       // Don't leave the ~b~cash~s~ behind.
								bMessagePrintedLeavingCash = TRUE
							ENDIF
						ELSE
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(vehEpsilonChopper,PLAYER_PED_ID(),230.0)     
								sFailReason = "EPS8_17" 				// ~r~You left the cash behind.
								missionStage = MS_MISSION_FAILING
							ELIF IS_ENTITY_IN_RANGE_ENTITY(vehEpsilonChopper,PLAYER_PED_ID(),100.0)
								bMessagePrintedLeavingCash = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE: //Adds all the cars to the mix group for dynamic mixing
PROC AddCarsToMix()  
	
	IF IS_VEHICLE_OK(vehEpsilonCar[0])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[0], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[1])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[1], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[2])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[2], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[3])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[3], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[4])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[4], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[5])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[5], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF
	IF IS_VEHICLE_OK(vehEpsilonCar[6])
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehEpsilonCar[6], "EPSILONISM_08_CLOSE_CAR_MG")
	ENDIF

ENDPROC

/// PURPOSE: //Manage transition to stealing/not stealing money
PROC ManageTransitions()

	IF missionStage = MS_DRIVING_TO_CHOPPER	
	OR missionStage = MS_PARKING_AT_CHOPPER	
	OR missionStage = MS_PARKED_AT_CHOPPER	
	OR missionStage = MS_EXITED_CAR	
	OR missionStage = MS_WALKING_TO_REWARD
		IF bPlayerGaveMoney = TRUE    
			IF IS_PED_UNINJURED(pedEpsilonist)
				//IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","c1_pose")	
				IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_enter")
				AND NOT	IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_base")
				AND NOT	IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_idle_a")
					IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","worship_exit")	
						i = 0
						PlayerGaveMoneyHeliFlyAway()
						Script_Passed()
					ELSE	
						FLOAT fStartTagOut
						FLOAT fEndTagOut 
						IF FIND_ANIM_EVENT_PHASE("rcmepsilonism8","worship_exit","WalkInterruptible",fStartTagOut,fEndTagOut)
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonist,"rcmepsilonism8","worship_exit") >= fStartTagOut
							FORCE_PED_MOTION_STATE(pedEpsilonist,MS_ON_FOOT_RUN,FALSE,FAUS_DEFAULT)
							SET_PED_KEEP_TASK(pedEpsilonist,TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonist,<<-355.7390, -181.6352, 36.7355>>,PEDMOVEBLENDRATIO_RUN,-1,3.0,ENAV_DEFAULT)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonist,FALSE)
							SET_PED_AS_NO_LONGER_NEEDED(pedEpsilonist)
							i = 0
							PlayerGaveMoneyHeliFlyAway()
							Script_Passed()
						ELSE
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedEpsilonist,"rcmepsilonism8","worship_exit") >= 0.880//0.890
								FORCE_PED_MOTION_STATE(pedEpsilonist,MS_ON_FOOT_RUN,FALSE,FAUS_DEFAULT)
								SET_PED_KEEP_TASK(pedEpsilonist,TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedEpsilonist,<<-355.7390, -181.6352, 36.7355>>,PEDMOVEBLENDRATIO_RUN,-1,3.0,ENAV_DEFAULT)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEpsilonist,FALSE)
								SET_PED_AS_NO_LONGER_NEEDED(pedEpsilonist)
								i = 0
								PlayerGaveMoneyHeliFlyAway()
								Script_Passed()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bPlayerStoleMoney = TRUE	
			TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			IF NOT IS_STRING_NULL_OR_EMPTY(root)
				IF ARE_STRINGS_EQUAL(root,"EPS8_ESC")								
				OR ARE_STRINGS_EQUAL(root,"EPS8_E1")		
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(issLoadHeli)
				STOP_SYNCHRONIZED_ENTITY_ANIM(vehPlayerCar,NORMAL_BLEND_OUT,TRUE)
			ENDIF
		ENDIF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		OR NOT bPrintEscapeWarning = TRUE	
			IF bPlayerStoleMoney = TRUE	
				//TRIGGER_MUSIC_EVENT("EPS8_EVADE_FORA") 	
				//IF IS_PED_UNINJURED(pedEpsilonist)	
				//	IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")
				//		STOP_ANIM_TASK(pedEpsilonist,"rcmepsilonism8","epsilonist_talks",SLOW_BLEND_OUT)
				//	ENDIF
				//ENDIF
				IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])	
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5])
				ENDIF
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				//TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				//IF ARE_STRINGS_EQUAL(root, "EPS8_ESC") 
				//OR ARE_STRINGS_EQUAL(root, "EPS8_E1") 
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//ENDIF
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				//SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				//TRIGGER_MUSIC_EVENT("EPS8_EVADE_FORA")
				SAFE_REMOVE_BLIP(blipEpsOnFootEscort)
				REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 6)
				REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 7)
				REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 4)
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, NULL, "EPSGUARD4")
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, NULL, "EPSGUARD8")  //EPSGUARD3
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, NULL, "CRIS")
				AddCarsToMix()
				CleanupTraffic()
				IF IS_PED_UNINJURED(pedEpsilonist)	
					SET_PED_CAN_BE_TARGETTED(pedEpsilonist,TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(pedEpsilonist,TRUE)
					IF IS_ENTITY_ALIVE(vehEpsilonChopper)
					AND IS_PED_IN_VEHICLE(pedEpsilonist,vehEpsilonChopper)
						SET_PED_ACCURACY(pedEpsilonist,iAccuracy - iHeliSniperAccModifier)
						SET_PED_SHOOT_RATE(pedEpsilonist,iShootRate + iHeliSniperSRModifier)
						SET_PED_CONFIG_FLAG(pedEpsilonist,PCF_DontBehaveLikeLaw,TRUE)
						GIVE_WEAPON_TO_PED(pedEpsilonist,WEAPONTYPE_ADVANCEDRIFLE,-1,FALSE,FALSE)
						GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonist,WEAPONTYPE_ADVANCEDRIFLE,WEAPONCOMPONENT_AT_AR_FLSH)
						GIVE_WEAPON_COMPONENT_TO_PED(pedEpsilonist,WEAPONTYPE_ADVANCEDRIFLE,WEAPONCOMPONENT_AT_SCOPE_SMALL)
						SET_CURRENT_PED_WEAPON(pedEpsilonist,WEAPONTYPE_ADVANCEDRIFLE,TRUE)
						SET_PED_KEEP_TASK(pedEpsilonist,TRUE)
						SET_PED_COMBAT_RANGE(pedEpsilonist,CR_FAR)
						SET_PED_HEARING_RANGE(pedEpsilonist,400.0)
						SET_PED_SEEING_RANGE(pedEpsilonist,400.0)
						SET_PED_ID_RANGE(pedEpsilonist,400.0)
						SET_PED_FIRING_PATTERN(pedEpsilonist,FIRING_PATTERN_FULL_AUTO) 
						SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_DO_DRIVEBYS,TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedEpsilonist,CA_LEAVE_VEHICLES,FALSE)
						TASK_COMBAT_PED(pedEpsilonist,PLAYER_PED_ID())
					ENDIF
				ENDIF
				SET_ROADS_IN_ANGLED_AREA(<<-651.314819,121.844025,63.453121>>, <<-650.061829,0.954960,35.754333>>, 41.750000,FALSE,TRUE)
				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_AMBULANCE_DEPARTMENT,FALSE)
				BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_FIRE_DEPARTMENT,FALSE)
				i = 0
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecurity[5])
					IF NOT IS_ENTITY_DEAD(objCashBag[0])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[0],pedEpsilonHiSecurity[5])
							DETACH_ENTITY(objCashBag[0])
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[1])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[1],pedEpsilonHiSecurity[5])
							DETACH_ENTITY(objCashBag[1])
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[2])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[2],pedEpsilonHiSecurity[5])
							DETACH_ENTITY(objCashBag[2])
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(objCashBag[3])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[3],pedEpsilonHiSecurity[5])
							DETACH_ENTITY(objCashBag[3])
						ENDIF
					ENDIF
				ENDIF
				iTimerPlayerStoleMoney = GET_GAME_TIMER()
				CLEAR_PRINTS()
				IF bNoCashLeftInCar = FALSE		
					IF bChopperBeingJacked = FALSE		
						IF NOT IS_ENTITY_DEAD(vehPlayerCar)		
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
								IF iEpSecEvaded <> iEpSecBlipped	
									IF bLoseCopsObjGiven = FALSE
										PRINT_NOW("EPS8_12",DEFAULT_GOD_TEXT_TIME,0)       // Escape from ~r~Epsilon's security.
									ENDIF
									bEscapeMessagePrinted = TRUE
								ENDIF
							ELSE
								IF missionStage = MS_PARKED_AT_CHOPPER	
								OR missionStage = MS_EXITED_CAR	
								OR missionStage = MS_WALKING_TO_REWARD
									PRINT_NOW("EPS8_11",DEFAULT_GOD_TEXT_TIME,0)       // Steal the ~b~car.
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF iEpSecEvaded <> iEpSecBlipped		
							IF bLoseCopsObjGiven = FALSE	
								PRINT_NOW("EPS8_12",DEFAULT_GOD_TEXT_TIME,0)       // Escape from ~r~Epsilon's security.
							ENDIF
							bEscapeMessagePrinted = TRUE
						ENDIF
					ENDIF
					/*
					IF NOT DOES_BLIP_EXIST(blipCashBag)	
						IF DOES_ENTITY_EXIST(objCashBag[i2])
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(objCashBag[i2])	
								blipCashBag = ADD_BLIP_FOR_ENTITY(objCashBag[i2])
								SET_BLIP_COLOUR(blipCashBag,BLIP_COLOUR_GREEN)
								SET_BLIP_SCALE(blipCashBag,fObjectBlipScale)
							ENDIF
						ENDIF
					ENDIF
					*/
				ELSE
					IF NOT DOES_BLIP_EXIST(blipLastCashBag)	
						blipLastCashBag = ADD_BLIP_FOR_ENTITY(objCashBag[3])
						SET_BLIP_COLOUR(blipLastCashBag,BLIP_COLOUR_GREEN)
						SET_BLIP_SCALE(blipLastCashBag,fObjectBlipScale)
						PRINT_NOW("EPS8_21",DEFAULT_GOD_TEXT_TIME,0)       // Steal the ~g~cash bag.
					ENDIF
				ENDIF
				//Turn roads back on
				/*
				SET_ROADS_IN_ANGLED_AREA(<<-517.559509,-12.416608,47.145653>>, <<-549.604431,-26.475727,38.927837>>, 12.250000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-471.516296,-110.937531,36.399551>>, <<-393.784912,-16.179173,52.934433>>, 53.000000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-649.620605,87.362564,56.020050>>, <<-650.510132,2.981458,29.153057>>, 28.000000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-557.134094,-22.472237,24.845257>>, <<-553.822021,24.367741,52.193020>>, 4.500000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-703.001831,-24.123243,45.880470>>, <<-608.923767,-9.902658,29.563908>>, 12.500000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-564.054626,1.096008,52.370747>>, <<-402.393585,-8.127587,33.435059>>, 9.000000,FALSE,TRUE)
				SET_ROADS_IN_ANGLED_AREA(<<-443.215759,0.934761,54.023079>>, <<-405.435303,-1.109592,33.373653>>, 1.750000,FALSE,TRUE)
				*/
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "**** Init player stole the money ****")
				#ENDIF
				IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_EPS_STOLEN_CASH))
					SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_EPS_STOLEN_CASH))
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_EPS_STOLEN_CASH) set")		
				ENDIF
				missionStage = MS_TAKEN_MONEY     
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: //Skip past the intro mocap, used for mission replay
PROC SkipPastCS()
	
	START_REPLAY_SETUP(<<-702.9905, 37.0211, 42.2136>>, 292.7606)
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	LoadStuff()
	
	RespotLauncherVehicles()
	
	InitPlayerCar()
	
	WaitForLoad()
	
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()),100.0,TRUE)
	CLEAR_AREA(<< -661.5685, 45.5499, 40.0669 >>,20.0,TRUE)    //Eps building gates
	
	END_REPLAY_SETUP(vehPlayerCar)
	
	Init()
	
	Init2()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	//LOAD_SCENE(<<-693.5082, 41.4818, 42.2245>>)
	
	PRINT_NOW("EPS8_05",DEFAULT_GOD_TEXT_TIME,0)    // Follow ~b~Epsilon's security~s~ to the helicopter.
	bFollowEpsSecObj = TRUE
	
	SET_STATIC_EMITTER_ENABLED("SE_LOS_SANTOS_EPSILONISM_BUILDING_01",FALSE)
	TRIGGER_MUSIC_EVENT("EPS8_START")
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	RC_END_Z_SKIP(TRUE) 
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "**** Replay skipped cutscene ****")
	#ENDIF
	
	missionStage = MS_DRIVING_TO_CHOPPER

ENDPROC

/// PURPOSE: //Skip to the helicopter, used for mission replay (This checkpoint was removed)
/*
PROC SkipToHeli()       

	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

	LoadStuff()
				
	RespotLauncherVehicles()
	
	InitPlayerCar()
	
	WaitForLoad()
	
	Init()
	
	Init2()
	
	IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
			START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
			SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper,47000)
			SET_HELI_BLADES_FULL_SPEED(vehEpsilonChopper)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(vehPlayerCar)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
			ENDIF
			SET_ENTITY_COORDS(vehPlayerCar,<< -362.7763, -82.0394, 44.6577 >>)
			SET_ENTITY_HEADING(vehPlayerCar,203.5849)
			vCashCarParkedPos = << -362.7763, -82.0394, 44.6577 >>
		ENDIF
	ENDIF
	
	SET_ENTITY_COORDS (PLAYER_PED_ID(), << -359.8641, -83.9259, 44.6226 >>)
	SET_ENTITY_HEADING (PLAYER_PED_ID(), 157.4846) 

	SET_ENTITY_COORDS (pedEpsilonist, <<-359.002106,-87.734573,44.5686>>)   //epsilonist
	SET_ENTITY_HEADING (pedEpsilonist, 4.876407) 

	IF NOT IS_ENTITY_DEAD(pedEpsilonHiSecDriver[0])
		IF NOT IS_ENTITY_DEAD(vehEpsilonCar[0])
			SET_ENTITY_COORDS(vehEpsilonCar[0],<< -374.3037, -72.2820, 44.6577 >>)
			SET_ENTITY_HEADING(vehEpsilonCar[0],249.5594)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(vehEpsilonCar[3])
		SET_ENTITY_COORDS(vehEpsilonCar[3],<< -400.3082, -26.8387, 45.7889 >>)
		SET_ENTITY_HEADING(vehEpsilonCar[3],173.6462)
	ENDIF
	
	REQUEST_MODEL(TRACTOR)
	
	WHILE NOT HAS_MODEL_LOADED(TRACTOR)
		WAIT(0)
	ENDWHILE
	
	vehTractor = CREATE_VEHICLE(TRACTOR,<< -375.6338, -105.9456, 37.6790 >>, 119.7025)
	SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR)

	OPEN_SEQUENCE_TASK(seqLoadChopper1)
		TASK_LEAVE_ANY_VEHICLE(NULL)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -354.5543, -85.2916, 44.6475 >>,PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,1.0,ENAV_NO_STOPPING)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,0.25)
	CLOSE_SEQUENCE_TASK(seqLoadChopper1)
	OPEN_SEQUENCE_TASK(seqLoadChopper2)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,0.25)
		TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehPlayerCar,1200)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -358.1744, -88.4951, 44.5940 >>,PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,0.25)
		TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehEpsilonChopper,1200)
	SET_SEQUENCE_TO_REPEAT(seqLoadChopper2,REPEAT_FOREVER)	
	CLOSE_SEQUENCE_TASK(seqLoadChopper2)
	
	iTimerPlayerParkedAtHeli = GET_GAME_TIMER() + 2500
	
	DISABLE_CELLPHONE(FALSE)
	
	IF DOES_BLIP_EXIST(blipEscort)
		REMOVE_BLIP(blipEscort)
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	LOAD_SCENE(<< -359.8641, -83.9259, 44.6226 >>)
	
	WAIT(1000)
	
	SET_ENTITY_COORDS (PLAYER_PED_ID(), << -359.8641, -83.9259, 44.6226 >>)
	SET_ENTITY_HEADING (PLAYER_PED_ID(), 157.4846) 
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	RC_END_Z_SKIP(TRUE) 
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "**** Replay skipped to helicopter ****")
	#ENDIF
	
	missionStage = MS_EXITED_CAR

ENDPROC
*/

/// PURPOSE: //Reset everything
PROC ResetStuff()   

	FORCE_CHASE_HINT_CAM(localChaseHintCamStruct, FALSE)

	eCutsceneState = eCutInit
	
	fHeliSpeed = 100
	
	CLEAR_PRINTS()
	
	IF DOES_ENTITY_EXIST(vehEpsilonCar[0])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[1])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[1])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[2])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[2])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[3])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[3])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[4])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[4])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[5])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[5])
	ENDIF
	IF DOES_ENTITY_EXIST(vehEpsilonCar[6])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehEpsilonCar[6])
	ENDIF
	IF DOES_ENTITY_EXIST(vehExtraCar[0])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehExtraCar[0])
	ENDIF
	IF DOES_ENTITY_EXIST(vehExtraCar[1])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehExtraCar[1])
	ENDIF
	
	REMOVE_RELATIONSHIP_GROUP(relEpsilon)
	REMOVE_RELATIONSHIP_GROUP(relEpsilonCiv)	
		
	IF bDebugPassOrFail = FALSE	
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				GET_SAFE_COORD_FOR_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE,vSafeCoord)
				SET_ENTITY_COORDS(PLAYER_PED_ID(),vSafeCoord)
			ELSE
				IF DOES_ENTITY_EXIST(vehPlayerCar)		
					IF NOT IS_ENTITY_DEAD(vehPlayerCar)	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
							GET_SAFE_COORD_FOR_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE,vSafeCoord)
							SET_ENTITY_COORDS(PLAYER_PED_ID(),vSafeCoord)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	
	SAFE_DELETE_PED(pedEpsilonist)
	SAFE_DELETE_PED(pedPilot)
	SAFE_DELETE_PED(pedChopperShotgun)
	SAFE_DELETE_VEHICLE(vehEpsilonChopper)
	SAFE_DELETE_VEHICLE(vehTractor)
	SAFE_DELETE_OBJECT(objCashBag[0])
	SAFE_DELETE_OBJECT(objCashBag[1])
	SAFE_DELETE_OBJECT(objCashBag[2])
	SAFE_DELETE_OBJECT(objCashBag[3])
	
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		REMOVE_VEHICLE_STUCK_CHECK(vehPlayerCar)
		SAFE_DELETE_VEHICLE(vehPlayerCar)
	ENDIF
	
	i = 0
	
	WHILE i < 7
	
		SAFE_DELETE_PED(pedEpsilonHiSecDriver[i])
		SAFE_DELETE_PED(pedEpsilonHiSecShotgun[i])
		SAFE_DELETE_PED(pedEpsilonHiSecurity[i])	
		SAFE_DELETE_VEHICLE(vehEpsilonCar[i])
			
		WAIT(0)
		
		++ i
		
	ENDWHILE	
	
	CleanupTraffic()
		
	DISABLE_CELLPHONE(FALSE)
	
	i = 0
	
	bPlayerStoleMoney = FALSE
	bPlayerGaveMoney = FALSE
	bLoseCopsObjGiven = FALSE
	bCutsceneChopperTaskGiven = FALSE
	bHelicopterHostile = FALSE
	iConvoCounter = 0
	iEpSecBlipped = 0
	iEpSecEvaded = 0
	iHeliGivenUp = 0
	iConvoCounter = 0
	iHeliNoLosConvoCounter = 0
	bDriveByTaskGiven[0] = FALSE
	bDriveByTaskGiven[1] = FALSE
	bDriveByTaskGiven[2] = FALSE
	bDriveByTaskGiven[3] = FALSE
	bDriveByTaskGiven[4] = FALSE
	bDriveByTaskGiven[5] = FALSE
	bDriveByTaskGiven[6] = FALSE
	bEpsDriverForcedToLeaveVeh[0] = FALSE
	bEpsDriverForcedToLeaveVeh[1] = FALSE
	bEpsDriverForcedToLeaveVeh[2] = FALSE
	bEpsDriverForcedToLeaveVeh[3] = FALSE
	bEpsDriverForcedToLeaveVeh[4] = FALSE
	bEpsDriverForcedToLeaveVeh[5] = FALSE
	bEpsDriverForcedToLeaveVeh[6] = FALSE
	iPassedDelay = 0
	bGuard1SaidKifflom = FALSE
	bGuard2SaidKifflom = FALSE
	bGuard3SaidKifflom = FALSE
	bPlayerAtHelipad = FALSE
	iSeqHeli = 0
	iPlayerDamagingEscort = 0
	//i2 = 0
	bBootOpen = FALSE
	bGuardGettingOnHeli = FALSE
	bGetInCarObjGiven = FALSE
	bChopperBeingJacked = FALSE
	bPlayerWentToTractor = FALSE
	bEscapeMessagePrinted = FALSE
	bNoCashLeftInCar = FALSE	
	iSeqThreatenPlayerInCar = 0
	bPlayerCarDestroyed = FALSE
	bEpsilonistAtTractor = FALSE
	bRoadSwitchOn = FALSE
	iNodeNumber = 1
	//iLoadChopperSeqSwitch = 0
	bEpsilonistPlayingAnims = FALSE
	bPrintHurryUp = FALSE
	bPrintRamWarning1 = FALSE
	bPrintRamWarning2 = FALSE
	bPrintFrontWarning = FALSE
	bPrintEscapeWarning = FALSE	
	iConvoChaseRandom = 0
	iConvoChaseCar = 0	
	bPrintMikeOnFoot = FALSE
	bPrintMikeInCover = FALSE
	bPrintToddShotMike = FALSE
	iPrintToddDied = 0
	iPrintPlayerLeftCar = 0
	bFollowEpsSecObj = FALSE
	fHeliEscapeDistance = 450.0 
	bHeliCantSeePlayer = FALSE
	iHeliSpotPlayerConvo = 0
	bPrintHeliCamHelp = FALSE
	bMikeLookAtEpsilonist = FALSE
	bStartRearConvoyCar = FALSE
	bOnlyHeliLeft = FALSE
	iEpsilonistBeckonConvo = 0
	bPrintEvadeHeli = FALSE
	bPrayConvo = FALSE	
	bOnlyHeliLeftConvo = FALSE	
	bDoHeliCam = FALSE	
	bPrintFollowEpsilonist = FALSE	
	iHeliCamSwitch = 0
	iEpsilonistsKilled = 0
	iSeqDelayAggression = 0
	iSeqDelayAggressionParkedUp = 0
	bDoneCrisAngryPhoneCall = FALSE
	bOverrideHeliColl = FALSE	
	bHeliCollOff = FALSE
	bSecurityStealingTailgater = FALSE
	iVehThief = -1
	bMikePickedUpABag = FALSE	
	iHeliTask = 0
	bHeliGivenUp = FALSE
	iTriggerSecondMusicCue = 0
	iBagsPutInHeli = 0
	//iBagsTakenFromCar = 0
	bPrintExitCar = FALSE
	iCounterHurryUp = 0
	bOverrideStartWalk = FALSE
	iIntroCamSeq = 0
	bTriggerMusic = FALSE
	bSwitchWaypointFlags = FALSE
	eLOAD_HELI = LOAD_HELI_INIT
	
	//SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)

	IF DOES_CAM_EXIST(camHeli)
		
		SET_CAM_ACTIVE(camHeli,FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camHeli)
		
		IF DOES_CAM_EXIST(camHeli2)	
			
			SET_CAM_ACTIVE(camHeli2,FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camHeli2)
	
		ENDIF
		
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
	
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
	
	ENDIF
			
	IF IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_CHOPPER")			
		STOP_AUDIO_SCENE("EPSILONISM_08_CHOPPER")		
	ENDIF
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "**** Reset mission ****")
	#ENDIF

ENDPROC

/// PURPOSE: //Waits for the screen to fade out, then updates the fail reason for the mission
PROC FailWait()       

	SWITCH eFAIL_STATE
	
		CASE FS_SETUP
			TRIGGER_MUSIC_EVENT("EPS8_FAIL")
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Triggered music event EPS8_FAIL ****")
			#ENDIF
			IF bMikePickedUpABag = TRUE	
				CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, - iCashInEachBag)
			ENDIF	
			RemoveBlips()
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				Random_Character_Failed_With_Reason(sFailReason)
			ELSE
				Random_Character_Failed()
			ENDIF

			eFAIL_STATE = FS_UPDATE
		BREAK
		
		CASE FS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< -714.3730, 33.2691, 42.0057 >>, 104.3473)
 				//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< -719.6254, 33.7486, 41.9291 >>, 111.1122)
				
				ResetStuff()

				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

/// PURPOSE: Check for Forced Pass or Fail
PROC DEBUG_Check_Debug_Keys()

#IF IS_DEBUG_BUILD
	// Check for Pass
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		WAIT_FOR_CUTSCENE_TO_STOP()
		CLEAR_PRINTS()
		bDebugPassOrFail = TRUE
		ResetStuff()
		Script_Passed()
	ENDIF

	// Check for Fail
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		WAIT_FOR_CUTSCENE_TO_STOP()
		missionStage = MS_MISSION_FAILING
	ENDIF
	
#ENDIF
	
ENDPROC		
		
PROC DEBUG_Check_Skips()		
		
	#IF IS_DEBUG_BUILD	
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			
			WAIT_FOR_CUTSCENE_TO_STOP()
			
			bDebugSkipping = TRUE
			
			IF missionStage = MS_DRIVING_TO_CHOPPER	
			OR missionStage = MS_SETUP
				
				CLEAR_PRINTS()
				
				IF IS_VEHICLE_OK(vehEpsilonChopper)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
						START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
						SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper,39000)
					ENDIF
				ENDIF

				IF NOT IS_VEHICLE_OK(vehPlayerCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					ENDIF
					SET_ENTITY_COORDS(vehPlayerCar,<< -382.7294, -68.8150, 44.4474 >>)
					SET_ENTITY_HEADING(vehPlayerCar,248.1496)
				ENDIF

				IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
					IF IS_VEHICLE_OK(vehEpsilonCar[0])
						SET_ENTITY_COORDS(vehEpsilonCar[0],<< -374.3037, -72.2820, 44.6577 >>)
						SET_ENTITY_HEADING(vehEpsilonCar[0],249.5594)
					ENDIF
				ENDIF
			
			ENDIF
			
			IF missionStage = MS_TAKEN_MONEY	
			
				IF IS_VEHICLE_OK(vehPlayerCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					ENDIF
				ENDIF

				CLEAR_PRINTS()
				
				bPlayerSkippedToEnd = TRUE
				
				Script_Passed()
			
			ENDIF
			
			IF missionStage = MS_PARKING_AT_CHOPPER
			OR missionStage = MS_PARKED_AT_CHOPPER
			OR missionStage = MS_EXITED_CAR
			OR missionStage = MS_WALKING_TO_REWARD
				
				CLEAR_PRINTS()
				
				REQUEST_MODEL(TRACTOR)
				
				WHILE NOT HAS_MODEL_LOADED(TRACTOR)
					WAIT(0)
				ENDWHILE
				
				IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
					IF IS_VEHICLE_OK(vehEpsilonCar[0])
						SET_ENTITY_COORDS(vehEpsilonCar[0],<< -374.3037, -72.2820, 44.6577 >>)
						SET_ENTITY_HEADING(vehEpsilonCar[0],249.5594)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_OK(vehEpsilonChopper)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
						START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
						SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
						SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_OK(vehPlayerCar)
					SET_ENTITY_COORDS(vehPlayerCar,<< -364.1155, -85.4846, 44.6577 >>)
					SET_ENTITY_HEADING(vehPlayerCar,221.5985)
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -377.8041, -102.7889, 37.6790 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),215.0944)
				ENDIF
				
				IF IS_PED_UNINJURED(pedEpsilonist)	
					SET_ENTITY_COORDS(pedEpsilonist,<< -382.0936, -98.4388, 37.6792 >>)
					SET_ENTITY_HEADING(pedEpsilonist,184.1947)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedEpsilonist,"Eps8EPED",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
				ENDIF
				
				vehTractor = CREATE_VEHICLE(TRACTOR,<< -375.6338, -105.9456, 37.6790 >>, 119.7025)
				//IF DOES_EXTRA_EXIST(vehTractor,8)
					SET_VEHICLE_EXTRA(vehTractor,8,FALSE)
				//ENDIF
				SET_VEHICLE_EXTRA(vehTractor,1,TRUE)
				SET_VEHICLE_NUMBER_PLATE_TEXT(vehTractor,"K1FFL0M1")
				SET_VEHICLE_AS_RESTRICTED(vehTractor,1)
				SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR)
				
				bPlayerGaveMoney = TRUE
				
				bPlayerStoleMoney = FALSE
				
				bPlayerWentToTractor = TRUE
				
				bPlayerSkippedToEnd = TRUE
				
				missionStage = MS_WALKING_TO_REWARD
					
			ENDIF
		
		ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
				bDebugSkipping = TRUE
				
				WAIT_FOR_CUTSCENE_TO_STOP()
		
				ResetStuff()
				
				ADD_RELATIONSHIP_GROUP("Epsilon",relEpsilon)
				ADD_RELATIONSHIP_GROUP("EpsilonCivilian",relEpsilonCiv)
				
				missionStage = MS_SETUP
		
			ENDIF
	
	#IF IS_DEBUG_BUILD
		
		SkipMenuStruct[0].sTxtLabel = "Mission start"  
		SkipMenuStruct[1].sTxtLabel = "Arriving at helicopter"  
		SkipMenuStruct[2].sTxtLabel = "Tractor"  
	
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage)
		
			IF iReturnStage = 0
				
				bDebugSkipping = TRUE
				
				ResetStuff()
			
				ADD_RELATIONSHIP_GROUP("Epsilon",relEpsilon)
				ADD_RELATIONSHIP_GROUP("EpsilonCivilian",relEpsilonCiv)
			
				missionStage = MS_SETUP
				
			ELIF iReturnStage = 1
				
				bDebugSkipping = TRUE
				
				WAIT_FOR_CUTSCENE_TO_STOP()
				
				ResetStuff()
				
				ADD_RELATIONSHIP_GROUP("Epsilon",relEpsilon)
				ADD_RELATIONSHIP_GROUP("EpsilonCivilian",relEpsilonCiv)
				
				LoadStuff()
				
				RespotLauncherVehicles()
				
				InitPlayerCar()
				
				WaitForLoad()
				
				Init()
				
				Init2()
				
				IF IS_VEHICLE_OK(vehEpsilonChopper)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
						START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
						SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper,39000)
					ELSE
						START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
						SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper,39000)
					ENDIF
				ENDIF
				

				IF IS_VEHICLE_OK(vehPlayerCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					ENDIF
					SET_ENTITY_COORDS(vehPlayerCar,<< -382.7294, -68.8150, 44.4474 >>)
					SET_ENTITY_HEADING(vehPlayerCar,248.1496)
				ENDIF

				
				IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
					IF NOT IS_ENTITY_DEAD(vehEpsilonCar[0])
						SET_ENTITY_COORDS(vehEpsilonCar[0],<< -374.3037, -72.2820, 44.6577 >>)
						SET_ENTITY_HEADING(vehEpsilonCar[0],249.5594)
					ENDIF
				ENDIF
				
				missionStage = MS_DRIVING_TO_CHOPPER
				
			ELIF iReturnStage = 2
				
				bDebugSkipping = TRUE
				
				WAIT_FOR_CUTSCENE_TO_STOP()
				
				CLEAR_PRINTS()
				
				IF IS_PED_UNINJURED(pedEpsilonHiSecDriver[0])
					IF IS_VEHICLE_OK(vehEpsilonCar[0])
						SET_ENTITY_COORDS(vehEpsilonCar[0],<< -374.3037, -72.2820, 44.6577 >>)
						SET_ENTITY_HEADING(vehEpsilonCar[0],249.5594)
					ENDIF
				ENDIF
				
				REQUEST_MODEL(TRACTOR)
				
				WHILE NOT HAS_MODEL_LOADED(TRACTOR)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_OK(vehEpsilonChopper)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEpsilonChopper)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
						START_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper, 222, "Ep8Heli01")
						SET_PLAYBACK_SPEED(vehEpsilonChopper,1.25)
						SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehEpsilonChopper)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_OK(vehPlayerCar)
					SET_ENTITY_COORDS(vehPlayerCar,<< -364.1155, -85.4846, 44.6577 >>)
					SET_ENTITY_HEADING(vehPlayerCar,221.5985)
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -377.8041, -102.7889, 37.6790 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),215.0944)
				ENDIF
				
				IF IS_PED_UNINJURED(pedEpsilonist)
					SET_ENTITY_COORDS(pedEpsilonist,<< -382.0936, -98.4388, 37.6792 >>)
					SET_ENTITY_HEADING(pedEpsilonist,184.1947)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedEpsilonist,"Eps8EPED",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
				ENDIF
				
				vehTractor = CREATE_VEHICLE(TRACTOR,<< -375.6338, -105.9456, 37.6790 >>, 119.7025)
				//IF DOES_EXTRA_EXIST(vehTractor,8)
					SET_VEHICLE_EXTRA(vehTractor,8,FALSE)
				//ENDIF
				SET_VEHICLE_EXTRA(vehTractor,1,TRUE)
				SET_VEHICLE_NUMBER_PLATE_TEXT(vehTractor,"K1FFL0M1")
				SET_VEHICLE_AS_RESTRICTED(vehTractor,1)
				SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR)
				
				bPlayerGaveMoney = TRUE
				
				bPlayerStoleMoney = FALSE
				
				bPlayerWentToTractor = TRUE
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				Script_Passed()

			ENDIF
		ENDIF
	#ENDIF	
#ENDIF
		
ENDPROC

/// PURPOSE: //Intro mocap
PROC IntroCutscene()    

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()

	SEQUENCE_INDEX seqParkUp

	SWITCH eCutsceneState
		
		CASE eCutInit
			
			IF iIntroCamSeq = 0
				RC_REQUEST_CUTSCENE("ep_8_rcm") //B*1730878 - Reqesting cutscene at the start of the leadin
				IF NOT DOES_CAM_EXIST(camIntro1)
					camIntro1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
				ENDIF
				IF NOT DOES_CAM_EXIST(camIntro2)
					camIntro2 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
				ENDIF
				
				SET_CAM_PARAMS(camIntro1,<< -692.1, 18.8, 40.3 >>, << 0.7, 0.0, 24.5 >>,50.0)
	
				SET_CAM_PARAMS(camIntro2,<< -691.8, 18.9, 42.9 >>, << 22.2, 0.0, 24.8 >>,50.0)
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH)
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-677.929871,31.374311,37.971050>>, <<-677.726257,51.893734,48.207581>>, 25.000000)
						SAFE_TELEPORT_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),<<-676.7216, 46.6862, 41.7595>>, 91.6871)
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-682.9754, 48.2520, 42.1423>>, 107.7723)
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-696.1459, 41.7659, 42.2180>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_DEFAULT,25.2690)
					ELSE
						SAFE_TELEPORT_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),<<-719.0406, 32.5248, 41.7419>>, 287.2928)
						SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),5)
						OPEN_SEQUENCE_TASK(seqParkUp)	
							TASK_VEHICLE_DRIVE_TO_COORD(NULL,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),<<-703.6560, 37.1430, 42.2135>>,3,DRIVINGSTYLE_NORMAL,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_PLOUGHTHROUGH,6.0,100)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-696.1459, 41.7659, 42.2180>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_DEFAULT,25.2690)
						CLOSE_SEQUENCE_TASK(seqParkUp)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(),seqParkUp)
						CLEAR_SEQUENCE_TASK(seqParkUp)
					ENDIF
				ELSE
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-696.1459, 41.7659, 42.2180>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_DEFAULT,25.2690)
				ENDIF
				
				SET_CAM_ACTIVE_WITH_INTERP(camIntro2,camIntro1,6200)
				
				iTimerIntroCams = GET_GAME_TIMER()
				
				iIntroCamSeq = 1
				
			ELIF iIntroCamSeq = 1
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF NOT IS_ENTITY_AT_COORD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),<<-703.6560, 37.1430, 42.2135>>,<<9,9,9>>)
							SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),5)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_GAME_TIMER() > iTimerIntroCams + 6000
				OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					
					iIntroCamSeq = 2
				
				ENDIF
				
			ELIF iIntroCamSeq = 2
			
				//RC_REQUEST_CUTSCENE("ep_8_rcm") //B*1730878 - Reqesting cutscene earlier now
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()		
					IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON)
						SET_CUTSCENE_PED_OUTFIT("Michael",PLAYER_ZERO,OUTFIT_P0_EPSILON)
					ENDIF
					IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL)
						SET_CUTSCENE_PED_OUTFIT("Michael",PLAYER_ZERO,OUTFIT_P0_EPSILON_WITH_MEDAL)
					ENDIF
				ENDIF

				IF RC_IS_CUTSCENE_OK_TO_START()	
					
					bCSExitMike = FALSE
					bCSExitCris = FALSE
					bCSExitCar = FALSE
					bCSExitCam = FALSE
					
					IF NOT IS_ENTITY_DEAD(vehPlayerCar)
						REGISTER_ENTITY_FOR_CUTSCENE(vehPlayerCar,"epsilon_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,TAILGATER,CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "**** Registered tailgater for cutscene ****")
						#ENDIF
					ENDIF
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
					
					RC_CLEANUP_LAUNCHER()
					START_CUTSCENE()
					SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(TAILGATER)
					WAIT(0)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					DESTROY_CAM(camIntro1)
					DESTROY_CAM(camIntro2)
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-702.028198,38.177532,46.456772>>, <<-659.647095,47.085461,38.170300>>, 16.000000,<< -717.3519, 34.7669, 42.0465 >>, 289.3352)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0,TRUE,CHAR_MICHAEL)
					RC_START_CUTSCENE_MODE(<< -697.75, 45.38, 43.03 >>,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					
					CLEAR_AREA_OF_VEHICLES(<<-651.9490, 62.9160, 44.7353>>,150)
					SET_ROADS_IN_ANGLED_AREA(<<-651.314819,121.844025,63.453121>>, <<-650.061829,0.954960,35.754333>>, 41.750000,FALSE,FALSE)
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "**** Cutscene started ****")
					#ENDIF
					eCutsceneState = eCutUpdate
				
				ENDIF
			
			ENDIF
			
		BREAK	
	
		CASE eCutUpdate
			
			IF IS_CUTSCENE_PLAYING()

				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
						STOP_CUTSCENE()
					ENDIF
					DEBUG_Check_Debug_Keys()
				#ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					REPLAY_STOP_EVENT()
					IF IS_VEHICLE_OK(vehPlayerCar)	
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
							bCSExitMike = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("epsilon_car")
					IF NOT IS_ENTITY_DEAD(vehPlayerCar)	
						//SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPlayerCar,FALSE)
					ENDIF
					bCSExitCar = TRUE
				ENDIF
				
				IF bCSExitCar = FALSE
					IF NOT IS_ENTITY_DEAD(vehPlayerCar)	
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPlayerCar)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Cris")
					bCSExitCris = TRUE
				ENDIF
				
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				
			ELSE
				eCutsceneState = eCutCleanup
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				bCSExitCam = TRUE
			ENDIF
			
			IF bCSExitMike = FALSE
			OR WAS_CUTSCENE_SKIPPED()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			IF bCSExitMike = TRUE
			AND bCSExitCris = TRUE
			AND bCSExitCar = TRUE
			AND bCSExitCam = TRUE
				eCutsceneState = eCutCleanup
			ENDIF
			
		BREAK
		
		CASE eCutCleanup
			
			IF NOT bTriggerMusic
				SET_STATIC_EMITTER_ENABLED("SE_LOS_SANTOS_EPSILONISM_BUILDING_01",FALSE)
				TRIGGER_MUSIC_EVENT("EPS8_START")
				bTriggerMusic = TRUE
			ENDIF
			
			//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

			// Deduct money from player's account
			IF (g_savedGlobals.sRandomChars.g_bFinalEpsilonPayment = FALSE)
				// May need to check whether we require a new BAAC type
				DEBIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_EPSILON_SITE_DONATION, EPSILON_8_CASH_AMOUNT)
				g_savedGlobals.sRandomChars.g_bFinalEpsilonPayment = TRUE
			ENDIF
			
			//CLEAR_AREA(<< -661.5685, 45.5499, 40.0669 >>,20.0,TRUE)    //Eps building gates
			
			RC_END_CUTSCENE_MODE()

			IF IS_VEHICLE_OK(vehPlayerCar)
				//SET_VEHICLE_FIXED(vehPlayerCar)
				SET_VEHICLE_DOORS_SHUT(vehPlayerCar)
				SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Cutscene finished ****")
			#ENDIF
			missionStage = MS_INIT
			
		BREAK
	
	ENDSWITCH
	
ENDPROC

/// PURPOSE:  //Dynamic mixing for the helicopter - increasing rolloff when the heli flies over the convoy and for the chase section
PROC HeliDynaMix()

	FLOAT fHeliMix
	FLOAT fHeliMixRange
		                            									 
	IF missionStage <> MS_TAKEN_MONEY                                 //Stop audio scene when heli gets near the compound 
	AND IS_ENTITY_IN_ANGLED_AREA(vehEpsilonChopper, <<-353.922821,-75.671692,99.266102>>, <<-364.354065,-105.649544,33.290710>>, 34.000000)	
		IF IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_CHOPPER")		
			STOP_AUDIO_SCENE("EPSILONISM_08_CHOPPER")		
		ENDIF
	ELSE
		IF NOT IS_AUDIO_SCENE_ACTIVE("EPSILONISM_08_CHOPPER")
			START_AUDIO_SCENE("EPSILONISM_08_CHOPPER")
		ELSE                                                           
			IF NOT IS_ENTITY_DEAD(vehEpsilonChopper)						 
				IF missionStage = MS_DRIVING_TO_CHOPPER
				OR missionStage = MS_PARKING_AT_CHOPPER
					fHeliMixRange = 600
				ELSE
					fHeliMixRange = 200
				ENDIF
				fHeliMix = fHeliMixRange - GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehEpsilonChopper)
				fHeliMix = fHeliMix / fHeliMixRange
				IF fHeliMix > 0.62
					fHeliMix = 0.62
				ENDIF
				IF fHeliMix < 0.0
					fHeliMix = 0.0
				ENDIF
			ELSE
				fHeliMix = 0.0
			ENDIF
			SET_AUDIO_SCENE_VARIABLE("EPSILONISM_08_CHOPPER","fHeliMix",fHeliMix)
			SET_AUDIO_SCENE_VARIABLE("EPSILONISM_08_CHOPPER","fCloseCarMix",0.6)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: //Load and initialise everything
PROC Setup()   
	
	IF IS_REPLAY_IN_PROGRESS() = TRUE
	AND bDebugSkipping = FALSE	
		SkipPastCS()
	ELSE
		LoadStuff()
		RespotLauncherVehicles()
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "**** Init mocap cutscene ****")
		#ENDIF
		missionStage = MS_MOCAP
	ENDIF

ENDPROC

PROC InitMission()

	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)

	InitPlayerCar()
	WaitForLoad()
	Init()
	Init2()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "**** Init driving to chopper ****")
	#ENDIF
	
	IF NOT bTriggerMusic
		SET_STATIC_EMITTER_ENABLED("SE_LOS_SANTOS_EPSILONISM_BUILDING_01",FALSE)
		TRIGGER_MUSIC_EVENT("EPS8_START")
		bTriggerMusic = TRUE
	ENDIF
	
	//SET_VEHICLE_DOOR_OPEN(vehPlayerCar,SC_DOOR_BOOT)
	missionStage = MS_DRIVING_TO_CHOPPER

ENDPROC

/// PURPOSE: //Player is driving to the helicopter, waiting for him to get there
PROC DrivingToChopper()   
	
	IF NOT bTriggerMusic
		SET_STATIC_EMITTER_ENABLED("SE_LOS_SANTOS_EPSILONISM_BUILDING_01",FALSE)
		TRIGGER_MUSIC_EVENT("EPS8_START")
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOW)
		bTriggerMusic = TRUE
	ENDIF
	
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)

	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-744.6715, -175.2881,-205.6593, 118.4107)

	/*
	WHILE(TRUE)	                     //Comment in for traffic debugging
		DoTraffic()
		DEBUG_Check_Debug_Keys()
		DEBUG_Check_Skips()
		WAIT(0)
	ENDWHILE
	*/
	
	START_AUDIO_SCENE_COMPLIANCE()  
	
	IF i = 1
	OR i = 4
		DoTraffic()
	ENDIF
	IF bPlayerStoleMoney = FALSE
		ConvoConvoy()
	ENDIF
	Update()
	RubberBand()
	blipsPlayerCar()
	//HeliDynaMix()
	
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)  
		IF IS_VEHICLE_OK(vehEpsilonCar[0])                           //Hint cam on lead convoy car                  
			CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehEpsilonCar[0])
		ENDIF
		IF IS_PED_IN_VEHICLE(pedEpsilonHiSecDriver[0],vehEpsilonCar[0])    //Switch blip to helicopter when lead car or player is nearly there	
			IF IS_ENTITY_IN_ANGLED_AREA(vehEpsilonCar[0], <<-384.129181,-72.768463,41.735500>>, <<-381.626129,-65.756081,52.896889>>, 8.500000)	
				IF DOES_BLIP_EXIST(blipEscort)
					REMOVE_BLIP(blipEscort)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipChopper)
					blipChopper = ADD_BLIP_FOR_COORD(vParkPos)
					//SET_BLIP_AS_FRIENDLY(blipChopper,TRUE)
				ENDIF
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				PRINT_NOW("EPS8_28",DEFAULT_GOD_TEXT_TIME,0)        //Park the car at the ~y~drop off point.
				missionStage = MS_PARKING_AT_CHOPPER
			ENDIF
		ENDIF																
		IF IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-384.129181,-72.768463,41.735500>>, <<-381.626129,-65.756081,52.896889>>, 8.500000)	
			SAFE_REMOVE_BLIP(blipEscort)
			IF NOT DOES_BLIP_EXIST(blipChopper)
				blipChopper = ADD_BLIP_FOR_COORD(vParkPos)
				//blipChopper = CREATE_VEHICLE_BLIP(vehEpsilonChopper,TRUE)
			ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Init parking at chopper ****")
			#ENDIF
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			PRINT_NOW("EPS8_28",DEFAULT_GOD_TEXT_TIME,0)           //Park the car at the ~y~drop off point.
			missionStage = MS_PARKING_AT_CHOPPER
		ENDIF
	ELSE
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF
	
	IF IS_ENTITY_AT_COORD(vehPlayerCar,vParkPos,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE)
	ENDIF

ENDPROC

/// PURPOSE: //Player reached heli compound, waiting for him to park near the heli 
PROC ParkingAtChopper()   

	/*
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_SPRINT)
			++iCol1
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_JUMP)
			--iCol1
		ENDIF
		IF iCol1 < 0
			iCol1 = 0
		ENDIF
		IF iCol1 > 155
			iCol1 = 155
		ENDIF
		PRINTINT(iCol1)
		PRINTNL()
		SET_VEHICLE_COLOURS(vehPlayerCar,iCol1,iCol1)
		SET_VEHICLE_EXTRA_COLOURS(vehPlayerCar,0,0)
		SET_VEHICLE_COLOURS(vehEpsilonChopper,iCol1,iCol1)
		SET_VEHICLE_EXTRA_COLOURS(vehEpsilonChopper,0,0)
	*/

	CLEANUP_AUDIO_SCENE_COMPLIANCE()  

	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)

	Update()
	RubberBand()
	IF NOT IS_ENTITY_AT_COORD(vehPlayerCar,vParkPos,<<2,2,LOCATE_SIZE_HEIGHT>>,FALSE)
		blipsPlayerCar()
	ENDIF
	//HeliDynaMix()
	IF NOT IS_PED_HEADTRACKING_PED(pedEpsilonist,PLAYER_PED_ID())
		TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
	ENDIF
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)  	
		IF IS_VEHICLE_OK(vehEpsilonChopper)                        		  //Hint cam on helicopter      
			CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehEpsilonChopper)
		ENDIF
	ELSE
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF
	
	//IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)   	  //Waiting for player to park near the helicopter
	//IF GET_ENTITY_SPEED(vehPlayerCar) >= 0.1	
	//	bCarStopped = FALSE
	//ENDIF
	IF IS_ENTITY_AT_COORD(vehPlayerCar,vParkPos,<<2,2,LOCATE_SIZE_HEIGHT>>,FALSE)  //<<1.95,1.95,LOCATE_SIZE_HEIGHT>>
	//AND GET_ENTITY_SPEED(vehPlayerCar) < 0.1		
	AND IS_VEHICLE_STOPPED(vehPlayerCar)	
		SAFE_REMOVE_BLIP(blipChopper) 
		IF GET_ENTITY_SPEED(vehPlayerCar) < 12.8	     
			fAreaWidth = 30.750000//23.750000                //Area width depends on player speed to stop the player crashing into the heli                                      
		ENDIF
		IF GET_ENTITY_SPEED(vehPlayerCar) >= 12.8	 
		AND GET_ENTITY_SPEED(vehPlayerCar) < 18.0	
			fAreaWidth = 32.750000 //27.750000
		ENDIF
		IF GET_ENTITY_SPEED(vehPlayerCar) >= 18.0
			fAreaWidth = 34.750000 //29.750000
		ENDIF
		//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-351.627869,-76.402191,41.777630>>, <<-361.892456,-104.871811,48.977909>>, fAreaWidth)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
			//SAFE_REMOVE_BLIP(blipChopper)                              //Getting the player out of the car
			SAFE_REMOVE_BLIP(blipPlayerCar)
			/*
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH)
			DISABLE_CELLPHONE(TRUE)
			OPEN_SEQUENCE_TASK(seqStopCarAndExit)
				TASK_VEHICLE_TEMP_ACTION(NULL,vehPlayerCar,TEMPACT_BRAKE,800)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				IF IS_PED_UNINJURED(pedEpsilonist)	
					TASK_LOOK_AT_ENTITY(NULL,pedEpsilonist,-1)
				ENDIF
			CLOSE_SEQUENCE_TASK(seqStopCarAndExit)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(),seqStopCarAndExit)
			CLEAR_SEQUENCE_TASK(seqStopCarAndExit)
			*/
			bMikeLookAtEpsilonist = TRUE
			REQUEST_MODEL(TRACTOR)
			REQUEST_CLIP_SET("move_ped_wpn_jerrycan_generic")
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Init parked at chopper ****")
			#ENDIF
			missionStage = MS_PARKED_AT_CHOPPER
		ELSE
			IF bPrintExitCar = FALSE
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_LOW)
				PRINT_NOW("EPS8_27",DEFAULT_GOD_TEXT_TIME,0)     //Exit the Car.
				bPrintExitCar = TRUE
				//vParkPos = GET_ENTITY_COORDS(vehPlayerCar)
			ENDIF
			bCarStopped = TRUE	
		ENDIF		
	ELSE
		IF IS_ENTITY_AT_COORD(vehPlayerCar,vParkPos,<<2,2,LOCATE_SIZE_HEIGHT>>,FALSE)	
			SAFE_REMOVE_BLIP(blipChopper)
			IF NOT IS_VEHICLE_STOPPED(vehPlayerCar)		
				IF bCarStopped = FALSE	
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar,4)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF DOES_BLIP_EXIST(blipChopper)	
		bCarStopped = FALSE	
		IF IS_ENTITY_AT_COORD(vehPlayerCar,vParkPos,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:  //Setting up sequences for the security guys and the Epsilonist, spawning the tractor
PROC ParkedAtChopper()  

	CleanupTraffic()
	Update()
	RubberBand()
	//HeliDynaMix()
	IF NOT IS_PED_HEADTRACKING_PED(pedEpsilonist,PLAYER_PED_ID())
		TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
	ENDIF
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar) 			
		IF HAS_MODEL_LOADED(TRACTOR)	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF CREATE_CONVERSATION(s_conversation_peds, "EPS8AU", "EPS8_ESC", CONV_PRIORITY_MEDIUM)   //Kifflom Zondar, these men will load up the helicopter.	
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					vehTractor = CREATE_VEHICLE(TRACTOR,<< -375.6338, -105.9456, 37.6790 >>, 119.7025)
					//IF DOES_EXTRA_EXIST(vehTractor,8)
						SET_VEHICLE_EXTRA(vehTractor,8,FALSE)
					//ENDIF
					SET_VEHICLE_EXTRA(vehTractor,1,TRUE)
					SET_VEHICLE_NUMBER_PLATE_TEXT(vehTractor,"K1FFL0M1")
					SET_VEHICLE_AS_RESTRICTED(vehTractor,1)
					SET_MODEL_AS_NO_LONGER_NEEDED(TRACTOR)
					/*
					OPEN_SEQUENCE_TASK(seqLoadChopper1)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -354.5543, -85.2916, 44.6475 >>,PEDMOVEBLENDRATIO_WALK,-1,1.0,ENAV_NO_STOPPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
					CLOSE_SEQUENCE_TASK(seqLoadChopper1)
					OPEN_SEQUENCE_TASK(seqLoadChopper2)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar,<<0.0,-2.15,0.0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehPlayerCar,1200)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -358.1744, -88.4951, 44.5940 >>,PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_BEFORE_WARP,0.3)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<-1.5,0,0>>),PEDMOVEBLENDRATIO_WALK,-1,0.3)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,vehEpsilonChopper,1200)
					SET_SEQUENCE_TO_REPEAT(seqLoadChopper2,REPEAT_FOREVER)	
					CLOSE_SEQUENCE_TASK(seqLoadChopper2)
					*/
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedEpsilonist,FALSE)
					OPEN_SEQUENCE_TASK(seqGoToPlayerFacePlayer)	
						TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						//TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1,6.1,PEDMOVEBLENDRATIO_WALK)
						//TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID(),-1)
						TASK_PLAY_ANIM(NULL,"rcmepsilonism8","jump_off_heli",REALLY_SLOW_BLEND_IN,-1,-1,AF_TAG_SYNC_OUT,0.084)  //0.074
						//TASK_PLAY_ANIM(NULL,"rcmepsilonism8","epsilonist_talks",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT)
					CLOSE_SEQUENCE_TASK(seqGoToPlayerFacePlayer)
					TASK_PERFORM_SEQUENCE(pedEpsilonist,seqGoToPlayerFacePlayer)
					CLEAR_SEQUENCE_TASK(seqGoToPlayerFacePlayer)
					iTimerPlayerParkedAtHeli = GET_GAME_TIMER()
					DISABLE_CELLPHONE(FALSE)
					vCashCarParkedPos = GET_ENTITY_COORDS(vehPlayerCar)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "**** Init exited car ****")
					#ENDIF
					blipEpsOnFootEscort = CREATE_PED_BLIP(pedEpsilonist,TRUE,TRUE)
					bPrintFollowEpsilonist = TRUE
					SET_BLIP_SCALE(blipEpsOnFootEscort,fSmallBlipScale)
					//FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
					//SET_VEHICLE_ENGINE_ON(vehEpsilonChopper,FALSE,FALSE)
					REQUEST_CLIP_SET("move_ped_wpn_jerrycan_generic")
					iBagsPutInHeli = 0
					//iBagsTakenFromCar = 0
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					eLOAD_HELI = LOAD_HELI_INIT
					missionStage = MS_EXITED_CAR
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//Player got back in the car and drove off -> make guys hostile
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-351.627869,-76.402191,41.777630>>, <<-361.892456,-104.871811,48.977909>>, fAreaWidth)
			bPlayerStoleMoney = TRUE	
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:  Start security guy loading money into the helicopter, Start Epsilonist walking to the tractor
PROC PlayerExitedCar()   

	Update()
	RubberBand()
	//HeliDynaMix()
	IF NOT IS_PED_HEADTRACKING_PED(pedEpsilonist,PLAYER_PED_ID())
		TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
	ENDIF
	//FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
	//SET_VEHICLE_ENGINE_ON(vehEpsilonChopper,FALSE,FALSE)
	//SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[5],PCF_DisablePedAvoidance,TRUE)
	//SET_PED_CONFIG_FLAG(pedEpsilonist,PCF_DisablePedAvoidance,TRUE)

	IF GET_GAME_TIMER() > iTimerPlayerParkedAtHeli + 6500
		DO_LOAD_HELI()
		/*
		IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			IF iLoadChopperSeqSwitch = 0
				TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqLoadChopper1)
				//IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1")
				//	TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				//ENDIF
				iLoadChopperSeqSwitch = 1
			ELSE
				//TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqLoadChopper2)
				IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1")
					TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				ENDIF
				iLoadChopperSeqSwitch = 2
			ENDIF
		ENDIF
		*/
	ENDIF
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF NOT DOES_BLIP_EXIST(blipEpsOnFootEscort)	
			IF bPrintFollowEpsilonist = FALSE		
				blipEpsOnFootEscort = CREATE_PED_BLIP(pedEpsilonist,TRUE,TRUE)
				SET_BLIP_SCALE(blipEpsOnFootEscort,fSmallBlipScale)
				bPrintFollowEpsilonist = TRUE
			ENDIF
		ENDIF
		/*
		IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqLoadChopper1)
			//IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1")
			//	TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
			//ENDIF
			iLoadChopperSeqSwitch = 1
		ENDIF
		*/
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "**** Init walking to reward ****")
		#ENDIF
		missionStage = MS_WALKING_TO_REWARD
	ENDIF
	
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
		TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		//IF NOT ARE_STRINGS_EQUAL(root,"EPS8_E1")		  	
		//	IF IS_ENTITY_PLAYING_ANIM(pedEpsilonist,"rcmepsilonism8","epsilonist_talks")
		//		STOP_ANIM_TASK(pedEpsilonist,"rcmepsilonism8","epsilonist_talks",REALLY_SLOW_BLEND_OUT)
		//	ENDIF
		//ENDIF
		IF NOT IS_STRING_NULL_OR_EMPTY(root)
			IF ARE_STRINGS_EQUAL(root,"EPS8_E1")				
				KILL_ANY_CONVERSATION()
			ENDIF
		ENDIF	
	ENDIF

ENDPROC

/// PURPOSE: // Security guys load up the helicopter, Epsilonist escorts Michael to the tractor
PROC WalkingToReward()
	
	Update()
	RubberBand()
	EpsilonistEscortGuyAi()
	//HeliDynaMix()
	HandleMikeLooking()
	//IF NOT IS_PED_HEADTRACKING_PED(pedEpsilonist,PLAYER_PED_ID())
	//	TASK_LOOK_AT_ENTITY(pedEpsilonist,PLAYER_PED_ID(),-1)
	//ENDIF
	//FREEZE_ENTITY_POSITION(vehEpsilonChopper,FALSE)
	//SET_VEHICLE_ENGINE_ON(vehEpsilonChopper,FALSE,FALSE)
	//SET_PED_CONFIG_FLAG(pedEpsilonHiSecurity[5],PCF_DisablePedAvoidance,TRUE)
	//SET_PED_CONFIG_FLAG(pedEpsilonist,PCF_DisablePedAvoidance,TRUE)
	/*
	IF bGuardGettingOnHeli
	AND eLOAD_HELI = GET_IN_HELI
		IF GET_GAME_TIMER() > iTimerSecurityGettingOnChopper + 1500
			PlayerGaveMoneyHeliFlyAway()
		ENDIF
	ENDIF
	*/
	IF bPrintFollowEpsilonist = FALSE	
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
			blipEpsOnFootEscort = CREATE_PED_BLIP(pedEpsilonist,TRUE,TRUE)
			SET_BLIP_SCALE(blipEpsOnFootEscort,fSmallBlipScale)
			bPrintFollowEpsilonist = TRUE
		ENDIF
	ENDIF
	
	IF bGuardGettingOnHeli = FALSE	
		IF GET_GAME_TIMER() > iTimerPlayerParkedAtHeli + 6500
			IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])	
				DO_LOAD_HELI()
				/*
				IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					IF iLoadChopperSeqSwitch = 0
						TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqLoadChopper1)
						//IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1")
						//	TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						//ENDIF
						iLoadChopperSeqSwitch = 1
					ELSE
						TASK_PERFORM_SEQUENCE(pedEpsilonHiSecurity[5],seqLoadChopper2)
						//IF NOT IS_ENTITY_PLAYING_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1")
						//	TASK_PLAY_ANIM(pedEpsilonHiSecurity[5],"rcmepsilonism8","grab_bag_1",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						//ENDIF
						iLoadChopperSeqSwitch = 2
					ENDIF
				ENDIF
				*/
			ENDIF
		ENDIF
	ENDIF
	/*
	IF iLoadChopperSeqSwitch = 2	
		IF IS_PED_UNINJURED(pedEpsilonHiSecurity[5])		
			IF GET_SCRIPT_TASK_STATUS(pedEpsilonHiSecurity[5],SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				IF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 1
					IF bNoCashLeftInCar = FALSE
						IF bBootOpen = FALSE
							SET_VEHICLE_DOOR_OPEN(vehPlayerCar,SC_DOOR_BOOT)
							SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
							bBootOpen = TRUE
						ENDIF
					ENDIF
				ELIF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 2	
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCashBag[i2],pedEpsilonHiSecurity[5])	
						DETACH_ENTITY(objCashBag[i2])
						ATTACH_ENTITY_TO_ENTITY(objCashBag[i2],pedEpsilonHiSecurity[5],GET_PED_BONE_INDEX(pedEpsilonHiSecurity[5],BONETAG_PH_R_HAND),<<0.1,-0.35,-0.35>>,<<0.0,0.0,0.0>>)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5],"move_ped_wpn_jerrycan_generic")
					ENDIF
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK,"move_ped_wpn_jerrycan_generic","walk")
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE,"move_ped_wpn_jerrycan_generic","idle")
					//SET_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_RUN,"move_ped_wpn_jerrycan_generic","run")
					bHeliLoadSwitch[i2] = FALSE
					IF i2 = (iNumberOfMoneyBags - 1) 
						bNoCashLeftInCar = TRUE
						IF bBootOpen = TRUE
							SET_VEHICLE_DOOR_SHUT(vehPlayerCar,SC_DOOR_BOOT,FALSE)
							SET_VEHICLE_DOORS_LOCKED(vehPlayerCar,VEHICLELOCK_UNLOCKED)
							bBootOpen = FALSE
						ENDIF
					ENDIF
				ELIF GET_SEQUENCE_PROGRESS(pedEpsilonHiSecurity[5]) = 3
					IF bHeliLoadSwitch[i2] = FALSE	
						IF i2 <> (iNumberOfMoneyBags - 1) 
							SAFE_DELETE_OBJECT(objCashBag[i2])
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(pedEpsilonHiSecurity[5])
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_WALK)
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_IDLE)
							//CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedEpsilonHiSecurity[5],AAT_RUN)
						ENDIF
						++i2
						bHeliLoadSwitch[i2] = TRUE
						IF i2 > (iNumberOfMoneyBags - 1) 
							IF bGuardGettingOnHeli = FALSE
								CLEAR_PED_TASKS(pedEpsilonHiSecurity[5])
								CLEAR_SEQUENCE_TASK(seqLoadChopper1)
								CLEAR_SEQUENCE_TASK(seqLoadChopper2)
								SET_PED_KEEP_TASK(pedEpsilonHiSecurity[5],TRUE)
								TASK_ENTER_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
								iTimerSecurityGettingOnChopper = GET_GAME_TIMER()
								bGuardGettingOnHeli = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	*/
	IF bGuardGettingOnHeli = TRUE          //Guard has finished loading the money
		IF GET_GAME_TIMER() > iTimerSecurityGettingOnChopper + 10000	
		OR IS_PED_IN_VEHICLE(pedEpsilonHiSecurity[5],vehEpsilonChopper)
			bPlayerGaveMoney = TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_DEAD(pedEpsilonist)
		SAFE_REMOVE_BLIP(blipEpsOnFootEscort)
	ENDIF
	/*
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)	
		TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(root,"EPS8_ESC")								
		OR ARE_STRINGS_EQUAL(root,"EPS8_E1")		
			//KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
	*/

ENDPROC

/// PURPOSE: //Player has stolen the money
PROC PlayerTakenMoney()   
/*
	// Mark money as stolen           //Moved to Script_Passed()
	IF NOT Is_Replay_In_Progress() 
      	IF (g_savedGlobals.sRandomChars.g_bStoleEpsilonCash = FALSE)
			g_savedGlobals.sRandomChars.g_bStoleEpsilonCash = TRUE
		ENDIF
	ENDIF
*/
	

	CLEANUP_AUDIO_SCENE_COMPLIANCE()  
	START_AUDIO_SCENE_NON_COMPLIANCE()  
/*
	IF bDoneCrisAngryPhoneCall = FALSE	
		IF iEpSecEvaded <> iEpSecBlipped
			IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 30000
				IF IS_VEHICLE_OK(vehPlayerCar)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
				AND bDoHeliCam = FALSE		
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_12")		
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_16")
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("EPS8_11")	
						IF CHAR_CALL_PLAYER_CELLPHONE(s_conversation_peds,CHAR_CRIS,"EPS8AU", "EPS8_CF1",CONV_PRIORITY_VERY_HIGH)
							bDoneCrisAngryPhoneCall = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
*/	
	IF iTriggerSecondMusicCue = 0
		IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 5000
			//TRIGGER_MUSIC_EVENT("EPS8_EVADE_STA")
			iTriggerSecondMusicCue = 1
		ENDIF
	ELIF iTriggerSecondMusicCue = 1
		IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 12000
			//SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			iTriggerSecondMusicCue = 2
		ENDIF
	ENDIF
			
	IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 9700
		IF iConvoChaseCar > 2
		OR bNoCashLeftInCar = TRUE
			//HeliCinematicCam()
		ENDIF
	ENDIF
	
	IF iHeliNoLosConvoCounter = 3
	AND iHeliGivenUp = 2
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		bHeliGivenUp = TRUE
	ENDIF
	
	//HeliDynaMix()
	
	IF iEpSecEvaded > 10000       //Failsafe to stop these ints getting too huge, this will never actually happen
	AND iEpSecBlipped > 10000
		SCRIPT_ASSERT("iEpSecEvaded and iEpSecBlipped > 10000. Please tell Tom Kingsley")
		iEpSecEvaded = iEpSecEvaded - 9000
		iEpSecBlipped = iEpSecBlipped - 9000
	ENDIF
	
	IF bNoCashLeftInCar = FALSE
		IF bEscapeMessagePrinted = FALSE
			IF IS_VEHICLE_OK(vehPlayerCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
					IF iEpSecEvaded <> iEpSecBlipped		
						IF bLoseCopsObjGiven = FALSE
						
							REPLAY_RECORD_BACK_FOR_TIME(15.0, 4.0, REPLAY_IMPORTANCE_LOW)
						
							PRINT_NOW("EPS8_12",DEFAULT_GOD_TEXT_TIME,0)       //Escape from ~r~Epsilon's security.
						ENDIF
						bEscapeMessagePrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		/*
		IF DOES_BLIP_EXIST(blipCashBag)	
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objCashBag[iBagsTakenFromCar],FALSE) < 1.0
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
					REMOVE_BLIP(blipCashBag)
					DELETE_OBJECT(objCashBag[iBagsTakenFromCar])
					CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, iCashInEachBag)
					bMikePickedUpABag = TRUE
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 0)    // Give Michael a bag
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),objCashBag[iBagsTakenFromCar],150)
					REMOVE_BLIP(blipCashBag)
					DELETE_OBJECT(objCashBag[iBagsTakenFromCar])
				ENDIF
			ENDIF
		ENDIF
		*/
	ELSE
		IF DOES_BLIP_EXIST(blipLastCashBag)	
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objCashBag[3],FALSE) < 1.0
					REMOVE_BLIP(blipLastCashBag)
					DELETE_OBJECT(objCashBag[3])
					CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, iCashInEachBag)
					bMikePickedUpABag = TRUE
					//SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 0)    // Give Michael a bag
					IF iEpSecEvaded <> iEpSecBlipped		
						IF bEscapeMessagePrinted = FALSE
							IF bLoseCopsObjGiven = FALSE
								REPLAY_RECORD_BACK_FOR_TIME(15.0, 4.0, REPLAY_IMPORTANCE_LOW)
								PRINT_NOW("EPS8_12",DEFAULT_GOD_TEXT_TIME,0)       //Escape from ~r~Epsilon's security.
							ENDIF
							bEscapeMessagePrinted = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SEQUENCE_INDEX seqShuffle
	IF IS_ENTITY_ALIVE(vehEpsilonChopper)		
	AND IS_ENTITY_ALIVE(pedChopperShotgun)	
		VECTOR vHeli = GET_ENTITY_COORDS(vehEpsilonChopper)
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<0,0,vHeli.z - 200>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehEpsilonChopper,<<500,0,vHeli.z + 200>>),1000)
			IF IS_PED_SITTING_IN_VEHICLE_SEAT(pedChopperShotgun,vehEpsilonChopper,VS_BACK_RIGHT)
			AND IS_VEHICLE_SEAT_FREE(vehEpsilonChopper,VS_BACK_LEFT)
				IF GET_SCRIPT_TASK_STATUS(pedChopperShotgun,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				OR GET_SEQUENCE_PROGRESS(pedChopperShotgun) > 0
					OPEN_SEQUENCE_TASK(seqShuffle)
						TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL,vehEpsilonChopper)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seqShuffle)
					TASK_PERFORM_SEQUENCE(pedChopperShotgun,seqShuffle)
					CLEAR_SEQUENCE_TASK(seqShuffle)
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_SITTING_IN_VEHICLE_SEAT(pedChopperShotgun,vehEpsilonChopper,VS_BACK_LEFT)
			AND IS_VEHICLE_SEAT_FREE(vehEpsilonChopper,VS_BACK_RIGHT)
				IF GET_SCRIPT_TASK_STATUS(pedChopperShotgun,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				OR GET_SEQUENCE_PROGRESS(pedChopperShotgun) > 0
					OPEN_SEQUENCE_TASK(seqShuffle)
						TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL,vehEpsilonChopper)
						TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seqShuffle)
					TASK_PERFORM_SEQUENCE(pedChopperShotgun,seqShuffle)
					CLEAR_SEQUENCE_TASK(seqShuffle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SetupEpsilonHostile()
	UpdateEpsilonHostile()
	blipsPlayerCar()
	
	IF bHelicopterHostile = TRUE
		HelicopterHostile()
	ENDIF
			
	IF bOverrideHeliColl = FALSE	
		IF IS_VEHICLE_OK(vehEpsilonChopper)	
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
				IF GET_SELECTED_PED_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_STICKYBOMB	
					IF GET_GAME_TIMER() > iTimerPlayerStoleMoney + 5000	
						IF bHeliCollOff = FALSE
							//SET_ENTITY_COLLISION(vehEpsilonChopper,FALSE)
							bHeliCollOff = TRUE
						ENDIF
					ENDIF
				ELSE
					IF bHeliCollOff = TRUE
						//SET_ENTITY_COLLISION(vehEpsilonChopper,TRUE)
						bHeliCollOff = FALSE
					ENDIF
				ENDIF
			ELSE
				IF bHeliCollOff = TRUE
					//SET_ENTITY_COLLISION(vehEpsilonChopper,TRUE)
					bHeliCollOff = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iEpSecEvaded = iEpSecBlipped   // If these ints are equal, no blips remain so all security has been evaded or killed
	AND GET_GAME_TIMER() > iTimerPlayerStoleMoney + 8000		              // Keep this a bit higher than the attack timer or mission can pass straight away
		iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
		IF iWantedLevel = 0	
			iTimerHeliDeadUpdate = GET_GAME_TIMER()
			IF iTimerHeliDeadUpdate > iTimerHeliDead + 1200	    //delay after heli dies
				IF iPassedDelay = 0
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					bGetInCarObjGiven = FALSE
					iCounterPassedDelay = GET_GAME_TIMER()
					iPassedDelay = 1
				ELIF iPassedDelay = 1
					iCounterPassedDelayUpdate = GET_GAME_TIMER()
					IF iCounterPassedDelayUpdate > iCounterPassedDelay + 2000     //delay before mission finishes
						IF NOT IS_MOBILE_PHONE_CALL_ONGOING()	
							IF bNoCashLeftInCar = FALSE	
								IF bChopperBeingJacked = FALSE
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
										TRIGGER_MUSIC_EVENT("EPS8_ESCAPE")
										Script_Passed()
									ELSE
										IF bGetInCarObjGiven = FALSE
											PRINT_NOW("EPS8_06",DEFAULT_GOD_TEXT_TIME,0)    //Get back in the ~b~car.
											bGetInCarObjGiven = TRUE
										ENDIF
									ENDIF
								ELSE
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehEpsilonChopper)
										TRIGGER_MUSIC_EVENT("EPS8_ESCAPE")
										Script_Passed()
									ELSE
										IF bGetInCarObjGiven = FALSE
											PRINT_NOW("EPS8_20",DEFAULT_GOD_TEXT_TIME,0)    //Get back in the ~b~helicopter.
											bGetInCarObjGiven = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								REPLAY_RECORD_BACK_FOR_TIME(15.0, 4.0, REPLAY_IMPORTANCE_LOW)
								Script_Passed()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bLoseCopsObjGiven = FALSE
				PRINT_NOW("EPS8_03",DEFAULT_GOD_TEXT_TIME,0)      //Lose the cops
				bLoseCopsObjGiven = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	ConvoChase()

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)	

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		IF bPlayerStoleMoney = TRUE
			TRIGGER_MUSIC_EVENT("EPS8_FAIL")
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Triggered music event EPS8_FAIL ****")
			#ENDIF
			PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
			Random_Character_Failed()
			Script_Cleanup()
		ELSE
			PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDIF	
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_EPSILON_8(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(Eps8_Scenario_Blocker())
	
	CLEAR_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_EPS_STOLEN_CASH))	// ensure this bit is initially clear when starting the mission
	
	ADD_RELATIONSHIP_GROUP("Epsilon",relEpsilon)
	ADD_RELATIONSHIP_GROUP("EpsilonCivilian",relEpsilonCiv)
	
	EpsBuildingBlock  = ADD_SCENARIO_BLOCKING_AREA(<< -711.6323, 42.0868, 14.5077 >>,<< -663.5175, 118.0403, 86.0362 >>)
	EpsBuildingBlock2 = ADD_SCENARIO_BLOCKING_AREA(<< -657.5416, 38.5153, 38.7892 >>,<< -665.8512, 52.9232, 42.3853 >>)
	CockatoosCarBlocker = ADD_SCENARIO_BLOCKING_AREA(<<-413.4749, -36.1436, 45.2982>>-<<7,7,5>>,<<-413.4749, -36.1436, 45.2982>>+<<7,7,5>>)
	
	// Clearing entity proofs here as they will be transferred to local peds - will need to update and ensure
	// following peds are made invulnerable for the cutscene - and reset upon scene completion
	RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
	
	pedEpsilonHiSecurity[0] = sRCLauncherDataLocal.pedID[0]
	pedEpsilonHiSecurity[2] = sRCLauncherDataLocal.pedID[1]
	pedEpsilonHiSecDriver[0] = sRCLauncherDataLocal.pedID[2]
	pedEpsilonHiSecShotgun[0] = sRCLauncherDataLocal.pedID[3]
	
	sRCLauncherDataLocal.pedID[0] = NULL
	sRCLauncherDataLocal.pedID[1] = NULL
	sRCLauncherDataLocal.pedID[2] = NULL
	sRCLauncherDataLocal.pedID[3] = NULL
	
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)					
						
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		STOP_ALL_GARAGE_ACTIVITY()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Epsilon8")
		
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF missionStage <> MS_SETUP
		AND missionStage <> MS_MOCAP
		AND missionStage <> MS_INIT
			//LockDoors()
			IF missionStage <> MS_MISSION_FAILING
				CheckForFail()
			ENDIF
			ManageTransitions()
		ENDIF						
								
		SWITCH missionStage
			
			CASE MS_SETUP
				Setup()
			BREAK
			
			CASE MS_MOCAP
				IntroCutscene()
			BREAK
			
			CASE MS_INIT
				InitMission()
			BREAK
		
			CASE MS_DRIVING_TO_CHOPPER
				DrivingToChopper()
			BREAK

			CASE MS_PARKING_AT_CHOPPER
				ParkingAtChopper()
			BREAK

			CASE MS_PARKED_AT_CHOPPER
				ParkedAtChopper()
			BREAK

			CASE MS_EXITED_CAR
				PlayerExitedCar()
			BREAK
			
			CASE MS_WALKING_TO_REWARD
				WalkingToReward()
			BREAK
			
			CASE MS_TAKEN_MONEY
				PlayerTakenMoney()
			BREAK
		
			CASE MS_MISSION_FAILING
				FailWait()
			BREAK

		ENDSWITCH	
		
		// Check debug completion/failure
		IF missionStage <> MS_MISSION_FAILING
			DEBUG_Check_Debug_Keys()
			DEBUG_Check_Skips()
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

