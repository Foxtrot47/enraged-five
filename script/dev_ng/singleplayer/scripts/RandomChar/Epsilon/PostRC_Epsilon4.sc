
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "RC_helper_functions.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	postRC_Epsilon4.sc											//
//		AUTHOR			:																//
//		DESCRIPTION		:	Handles Marnie and Jimmy's conversations as they			//
//                          walk to their car after completing Eps 4					//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

ENUM MISSION_STAGE
	MS_INIT,
	MS_RUNNING,
	MS_CLEANUP
ENDENUM

MISSION_STAGE mStage 	= MS_INIT
structPedsForConversation convStruct
INT iConvTimer

PED_INDEX pedMarnie
PED_INDEX pedJimmy

/// PURPOSE:
///    Initialises variables etc
PROC INIT()
	
	CPRINTLN(DEBUG_MISSION, "Post Eps 4: Trying to grab peds now...")
	
	INT cnt 
	INT i 
	PED_INDEX tmpArray[32]
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		cnt = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), tmpArray)
	ENDIF
	
	REPEAT cnt i
		IF IS_ENTITY_ALIVE(tmpArray[i])
			IF (GET_ENTITY_MODEL(tmpArray[i]) = GET_NPC_PED_MODEL(CHAR_JIMMY_BOSTON))
				
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MISSION, "Post Eps 4: - Jimmy get!")
				#ENDIF
				pedJimmy = tmpArray[i]
				IF NOT IS_ENTITY_A_MISSION_ENTITY(pedJimmy)
					SET_ENTITY_AS_MISSION_ENTITY(pedJimmy)
				ENDIF
				
				SET_PED_MONEY(pedJimmy, 0)
				SET_PED_CAN_BE_TARGETTED(pedJimmy, FALSE)
				SET_PED_NAME_DEBUG(pedJimmy, "POSTJIMMY")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, RELGROUPHASH_PLAYER)
				ADD_PED_FOR_DIALOGUE(ConvStruct, 5, pedJimmy, "JIMMYBOSTON", TRUE)
				
			ENDIF
			IF (GET_ENTITY_MODEL(tmpArray[i]) = GET_NPC_PED_MODEL(CHAR_MARNIE))
				
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MISSION, "Post Eps 4: - Marnie get!")
				#ENDIF
				pedMarnie = tmpArray[i]
				IF NOT IS_ENTITY_A_MISSION_ENTITY(pedMarnie)
					SET_ENTITY_AS_MISSION_ENTITY(pedMarnie)
				ENDIF
				
				SET_PED_MONEY(pedMarnie, 0)
				SET_PED_CAN_BE_TARGETTED(pedMarnie, FALSE)
				SET_PED_NAME_DEBUG(pedMarnie, "POSTMARNIE")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedMarnie, RELGROUPHASH_PLAYER)
				ADD_PED_FOR_DIALOGUE(ConvStruct, 4, pedMarnie, "MARNIE", TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	mStage = MS_RUNNING
ENDPROC

PROC CHECK_PEDS_FOR_CONVERSATION()

	IF IS_ENTITY_ALIVE(pedJimmy)
		// If the player is <=3m from Jimmy, play one of his ambient lines, with an 8sec delay between them
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_PED_FLEEING(pedJimmy)
		AND NOT IS_PED_RAGDOLL(pedJimmy)
		AND NOT IS_PED_PRONE(pedJimmy)
		AND NOT IS_PED_INJURED(pedJimmy)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedJimmy)
			IF GET_DISTANCE_BETWEEN_ENTITIES(pedJimmy, PLAYER_PED_ID()) <= 2.3
				IF (GET_GAME_TIMER() - iConvTimer) > 8000
					IF CREATE_CONVERSATION(ConvStruct, "EPS4AUD", "EPS4_FOLLJ", CONV_PRIORITY_MEDIUM)
						iConvTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(pedJimmy, PLAYER_PED_ID()) > 150.0
			SAFE_DELETE_PED(pedJimmy) // Jimmy won't be alive after this, it'll set the bool to false the frame after
		ENDIF
	ELSE
		SAFE_RELEASE_PED(pedJimmy) // In case he still exists but is dead - no point holding onto the ped index anymore
	ENDIF
	
	IF IS_ENTITY_ALIVE(pedMarnie)
		// If the player is <=3m from Marnie, play one of his ambient lines, with an 8sec delay between them
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_PED_FLEEING(pedMarnie)
		AND NOT IS_PED_RAGDOLL(pedMarnie)
		AND NOT IS_PED_PRONE(pedMarnie)
		AND NOT IS_PED_INJURED(pedMarnie)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedMarnie)
			IF GET_DISTANCE_BETWEEN_ENTITIES(pedMarnie, PLAYER_PED_ID()) <= 2.7
				IF (GET_GAME_TIMER() - iConvTimer) > 8000
					IF CREATE_CONVERSATION(ConvStruct, "EPS4AUD", "EPS4_FOLLM", CONV_PRIORITY_MEDIUM)
						iConvTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(pedMarnie, PLAYER_PED_ID()) > 150.0
			SAFE_DELETE_PED(pedMarnie) // Marnie won't be alive after this, it'll set the bool to false the frame after
		ENDIF
	ELSE
		SAFE_RELEASE_PED(pedMarnie) // In case he still exists but is dead - no point holding onto the ped index anymore
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(pedMarnie)
	AND NOT IS_ENTITY_ALIVE(pedJimmy)
		mStage = MS_CLEANUP
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Terminates ambient script after performing cleanup functions
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_MISSION, "POSTRC_EPS4.SC - SCRIPT CLEANUP") 
	REMOVE_PED_FOR_DIALOGUE(convStruct, 4)		// "MARNIE"
	REMOVE_PED_FOR_DIALOGUE(convStruct, 5)		// "JIMMY"
	CPRINTLN(DEBUG_MISSION, "POSTRC_EPS4.SC - TERMINATING AMBIENT SCRIPT") 
	TERMINATE_THIS_THREAD()
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT 
	
	CPRINTLN(DEBUG_MISSION, "POSTRC_EPS4.SC - INIT AMBIENT SCRIPT")
	
	// Default callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_MISSION, "POSTRC_EPS4.SC - DEFAULT CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE (TRUE)
		
		WAIT(0)
		
		// Terminate if we are not Michael
		IF SHOULD_POST_RC_SCRIPT_TERMINATE(CHAR_MICHAEL)
			SCRIPT_CLEANUP()
		ELSE
			SWITCH mStage
				CASE MS_INIT
					INIT()
				BREAK
				
				CASE MS_RUNNING
					CHECK_PEDS_FOR_CONVERSATION()
				BREAK
				
				CASE MS_CLEANUP
					SCRIPT_CLEANUP()
				BREAK
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT
