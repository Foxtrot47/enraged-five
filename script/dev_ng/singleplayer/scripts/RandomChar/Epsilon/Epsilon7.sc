
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "initial_scenes_Epsilon.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Epsilon7.sc
//		AUTHOR			:	David Roberts
//		DESCRIPTION		:	MICHAEL has found a hideaway in the desert. 
//                          MARNIE is waiting outside.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

ENUM eRC_MainState
	RC_INTRO,
	RC_WAITTOPASS,
	RC_FAILED
ENDENUM

ENUM eRC_SubState
	SS_ASSET_REQUEST,
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC = 0,
	FAILED_SPOOKED,
	FAILED_HURT
ENDENUM

// Mission state
eRC_MainState	m_eState   = RC_INTRO
eRC_SubState	m_subState = SS_ASSET_REQUEST

FAILED_REASON	failReason = FAILED_GENERIC

BOOL 			bHasChanged

REL_GROUP_HASH 	relGroupPlayer

PED_INDEX		pedMarnie
PED_INDEX		pedJimmy
PED_INDEX		pedTom
VEHICLE_INDEX	eps_vehicle

SEQUENCE_INDEX	seq

INT 			iCutsceneStage = 0
INT 			iFailStage = 0

g_structRCScriptArgs sRCLauncherDataLocal

#IF IS_DEBUG_BUILD
	BOOL bDebug_PrintToTTY = TRUE
#ENDIF

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		#IF IS_DEBUG_BUILD 
			IF bDebug_PrintToTTY 
				CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
			ENDIF 
		#ENDIF
	ENDIF

	SAFE_RELEASE_VEHICLE(eps_vehicle)
	SAFE_RELEASE_PED(pedMarnie, TRUE, TRUE)
	SAFE_RELEASE_PED(pedTom, TRUE, TRUE)
	SAFE_RELEASE_PED(pedJimmy, TRUE, TRUE)
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
/// PARAMS:
///    None.
/// RETURNS:
///    N/A
PROC Script_Passed()
	STOP_SCRIPTED_CONVERSATION(FALSE)
	CLEAR_PRINTS()
	ADD_HELP_TO_FLOW_QUEUE("EPSDES_HE", FHP_MEDIUM, 4000, FLOW_HELP_NEVER_EXPIRES, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL) 
	//Set epsilon step stat
		INT iCurrent
		STAT_GET_INT(NUM_EPSILON_STEP,iCurrent)
		IF iCurrent < 18
			STAT_SET_INT(NUM_EPSILON_STEP,18)
			SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH20),18)
			CPRINTLN(debug_dan,"Epsilon progress:",18)
		ENDIF
	
	Random_Character_Passed()
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for Forced Pass or Fail
	/// PARAMS:
	///    None.
	/// RETURNS:
	///    N/A
	PROC DEBUG_Check_Debug_Keys()
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			STOP_SCRIPTED_CONVERSATION(FALSE)
			CLEAR_PRINTS()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Sets the medal version as unavailable and not acquired. And the normal version as available and acquired.
PROC Reset_Outfit_Availability()
	SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, FALSE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, FALSE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, TRUE)
	SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, TRUE)
ENDPROC

PROC GRAB_MARNIE()
	IF NOT DOES_ENTITY_EXIST(pedMarnie)
		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Marnie", IG_MARNIE))
			pedMarnie = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Marnie", IG_MARNIE))
			CPRINTLN(DEBUG_MISSION, "Got Marnie") 
		ENDIF
	ENDIF
ENDPROC

PROC GRAB_JIMMY()
	IF NOT DOES_ENTITY_EXIST(pedJimmy)
		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_Boston", IG_JIMMYBOSTON))
			pedJimmy = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Jimmy_Boston", IG_JIMMYBOSTON))
			CPRINTLN(DEBUG_MISSION, "Got Jimmy") 
		ENDIF
	ENDIF
ENDPROC

PROC GRAB_TOM()
	IF NOT DOES_ENTITY_EXIST(pedTom)
		IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tom_Epsilon", IG_TOMEPSILON))
			pedTom = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Tom_Epsilon", IG_TOMEPSILON))
			CPRINTLN(DEBUG_MISSION, "Got Tom") 
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_MICHAEL_EXIT()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			CPRINTLN(DEBUG_MISSION, "Mike exit state") 
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC DO_MARNIE_EXIT()
	IF IS_ENTITY_ALIVE(pedMarnie)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Marnie")
			CPRINTLN(DEBUG_MISSION, "Marnie exit state") 
			IF IS_VEHICLE_OK(eps_vehicle)
				TASK_WARP_PED_INTO_VEHICLE(pedMarnie, Eps_Vehicle, VS_FRONT_RIGHT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_JIMMY_EXIT()
	IF IS_ENTITY_ALIVE(pedJimmy)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jimmy_Boston")
			CPRINTLN(DEBUG_MISSION, "Jimmy exit state") 
			IF IS_VEHICLE_OK(eps_vehicle)
				TASK_WARP_PED_INTO_VEHICLE(pedJimmy, Eps_Vehicle, VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_TOM_EXIT()
	IF IS_ENTITY_ALIVE(pedTom)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Tom_Epsilon")
			CPRINTLN(DEBUG_MISSION, "Tom exit state") 
			IF IS_VEHICLE_OK(eps_vehicle)
				TASK_WARP_PED_INTO_VEHICLE(pedTom, Eps_Vehicle, VS_BACK_RIGHT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs intro cutscene state
/// PARAMS:
///    None.
/// RETURNS:
///    N/A
PROC STATE_Intro()
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	SWITCH m_subState
		CASE SS_ASSET_REQUEST
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(DEFAULT, DEFAULT, FALSE)
			ENDIF
			RC_REQUEST_CUTSCENE("ep_7_rcm", TRUE)
			REQUEST_MODEL(BISON)
			REQUEST_ADDITIONAL_TEXT("EPS7", MISSION_TEXT_SLOT)
			REQUEST_WAYPOINT_RECORDING("eps7_driveaway")
			bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)  
			// preload the outfit with medal
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_EPSILON_WITH_MEDAL)
			ENDIF
			m_subState = SS_Setup
		BREAK

		CASE SS_Setup
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Init RC_INTRO") 
				ENDIF 
			#ENDIF
			IF RC_IS_CUTSCENE_OK_TO_START()
			AND HAS_MODEL_LOADED(BISON)
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("eps7_driveaway")
				RC_CleanupSceneEntities(sRCLauncherDataLocal)
				REGISTER_ENTITY_FOR_CUTSCENE(pedMarnie, "Marnie", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_MARNIE)
				REGISTER_ENTITY_FOR_CUTSCENE(pedJimmy, "Jimmy_Boston", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_JIMMYBOSTON)
				REGISTER_ENTITY_FOR_CUTSCENE(pedTom, "Tom_Epsilon", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_TOMEPSILON)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				RC_CLEANUP_LAUNCHER()
				TRIGGER_MUSIC_EVENT("EPS7_START")
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				START_CUTSCENE()
				WAIT(0)
				IF IS_REPLAY_IN_PROGRESS()
					RC_END_Z_SKIP()
				ENDIF
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<516.167358,3080.050293,33.841984>>, <<543.897888,3066.680908,44.412357>>, 18.75,
					<< 515.90, 3073.92, 39.72 >>, 9.72, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RC_START_CUTSCENE_MODE(<< 524.1728, 3079.5901, 39.5043 >>, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
				CLEAR_ANGLED_AREA_OF_VEHICLES(<<519.447510,3075.087646,38.668587>>, <<546.645569,3068.314697,43.136059>>, 17.25)
				m_subState = SS_Update
			ENDIF
		BREAK

		CASE SS_Update
			
			IF iCutsceneStage = 0
				// Create the Epsilonist vehicle
				IF NOT IS_VEHICLE_OK(eps_vehicle)
					eps_vehicle = CREATE_VEHICLE(BISON, <<537.28, 3081.30, 39.91>>, 179.03)
					SET_VEHICLE_COLOURS(eps_vehicle, 157, 157)
					SET_VEHICLE_EXTRA_COLOURS(eps_vehicle, 5, 157)
					SET_VEHICLE_ON_GROUND_PROPERLY(eps_vehicle)
				ENDIF
				
				// Loop to grab entities from cutscene
				GRAB_MARNIE()
				GRAB_JIMMY()
				GRAB_TOM()
				
				// Reset every frame for blend
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				IF WAS_CUTSCENE_SKIPPED()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					iCutsceneStage = 1
				ENDIF
				
				IF DO_MICHAEL_EXIT()
					iCutsceneStage = 1
				ENDIF
				
				DO_MARNIE_EXIT()
				DO_JIMMY_EXIT()
				DO_TOM_EXIT()
			
			ELIF iCutsceneStage = 1
			
				// Loop to grab entities from cutscene
				GRAB_MARNIE()
				GRAB_JIMMY()
				GRAB_TOM()
				
				DO_MARNIE_EXIT()
				DO_JIMMY_EXIT()
				DO_TOM_EXIT()
				
				DO_MICHAEL_EXIT()
				
				IF NOT IS_CUTSCENE_PLAYING()
					// update the player's outfit to be the epsilon medal outfit
					SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, TRUE)
					SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL, TRUE)
			
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL)
						SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
						RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
					ENDIF
					
//					WHILE GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT) != OUTFIT_P0_EPSILON_WITH_MEDAL
//						WAIT(0)
//						#IF IS_DEBUG_BUILD 
//							IF bDebug_PrintToTTY 
//								CPRINTLN(DEBUG_MISSION, "Waiting for outfit to be correct...") 
//							ENDIF 
//						#ENDIF
//					ENDWHILE
					
					SET_PED_COMP_ITEM_AVAILABLE_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE)
					SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON, FALSE)
			
					RC_END_CUTSCENE_MODE()
					m_subState = SS_Cleanup
				ENDIF
			ENDIF
		BREAK

		CASE SS_Cleanup
			REPLAY_STOP_EVENT()
			#IF IS_DEBUG_BUILD 
				IF bDebug_PrintToTTY 
					CPRINTLN(DEBUG_MISSION, "Cleaning up RC_INTRO") 
				ENDIF 
			#ENDIF
			IF IS_PED_UNINJURED(pedTom)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedTom, relGroupPlayer)
				SET_PED_CONFIG_FLAG(pedTom, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			ENDIF
			IF IS_PED_UNINJURED(pedMarnie)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedMarnie, relGroupPlayer)
				SET_PED_CONFIG_FLAG(pedMarnie, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			ENDIF
			IF IS_PED_UNINJURED(pedJimmy)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedJimmy, relGroupPlayer)
				SET_PED_CONFIG_FLAG(pedJimmy, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			ENDIF
			TRIGGER_MUSIC_EVENT("EPS7_STOP")
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			m_eState = RC_WAITTOPASS
			m_subState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC

PROC STATE_WaitToPass()
	SWITCH m_subState
		CASE SS_SETUP
			IF IS_ENTITY_ALIVE(pedJimmy)
			AND IS_VEHICLE_OK(Eps_Vehicle)
				OPEN_SEQUENCE_TASK(seq)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, Eps_Vehicle, "eps7_driveaway", DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, 11)
					TASK_VEHICLE_DRIVE_WANDER(NULL, Eps_Vehicle, 40, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedJimmy, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_ENTITY_LOAD_COLLISION_FLAG(Eps_Vehicle, TRUE)
				m_subState = SS_UPDATE
			ENDIF
		BREAK
		CASE SS_UPDATE
			IF IS_ENTITY_ALIVE(pedJimmy)
				IF HAS_PLAYER_THREATENED_PED(pedJimmy, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
				OR IS_PED_BEING_JACKED(pedJimmy)
					failReason = FAILED_SPOOKED
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					m_eState = RC_FAILED
				ENDIF
			ELSE
				failReason = FAILED_HURT
				TRIGGER_MUSIC_EVENT("EPS_FAIL")
				m_eState = RC_FAILED
			ENDIF
			IF IS_ENTITY_ALIVE(pedMarnie)
				IF HAS_PLAYER_THREATENED_PED(pedMarnie, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
				OR IS_PED_BEING_JACKED(pedMarnie)
					failReason = FAILED_SPOOKED
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					m_eState = RC_FAILED
				ENDIF
			ELSE
				failReason = FAILED_HURT
				TRIGGER_MUSIC_EVENT("EPS_FAIL")
				m_eState = RC_FAILED
			ENDIF
			IF IS_ENTITY_ALIVE(pedTom)
				IF HAS_PLAYER_THREATENED_PED(pedTom, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
				OR IS_PED_BEING_JACKED(pedTom)
					failReason = FAILED_SPOOKED
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					m_eState = RC_FAILED
				ENDIF
			ELSE
				failReason = FAILED_HURT
				TRIGGER_MUSIC_EVENT("EPS_FAIL")
				m_eState = RC_FAILED
			ENDIF
			IF IS_VEHICLE_OK(eps_vehicle)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(eps_vehicle, PLAYER_PED_ID())
					failReason = FAILED_SPOOKED
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					m_eState = RC_FAILED
				ENDIF
			ELSE
				failReason = FAILED_SPOOKED
				TRIGGER_MUSIC_EVENT("EPS_FAIL")
				m_eState = RC_FAILED
			ENDIF
			if GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), eps_vehicle) >= 75
				m_subState = SS_CLEANUP
			ENDIF
		BREAK
		CASE SS_CLEANUP
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

PROC STATE_Failed()

	SWITCH iFailStage
		CASE 0
			
			CLEAR_PRINTS()
			
			STRING sFailReason
			
			IF NOT IS_ENTITY_ALIVE(pedMarnie)
			OR NOT IS_ENTITY_ALIVE(pedJimmy)
			OR NOT IS_ENTITY_ALIVE(pedTom)
				CPRINTLN(DEBUG_MISSION,"Changing fail reason to HURT because 1 or more peds isn't alive")
				failReason = FAILED_HURT
			ENDIF
	
			SWITCH failReason
				CASE FAILED_GENERIC
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_GENERIC")
				BREAK
				CASE FAILED_SPOOKED
					sFailReason = "EPS7_FAIL" // ~r~The Epsilonists were spooked.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_SPOOKED")
				BREAK
				CASE FAILED_HURT
					sFailReason = "EPS7_HURT" // ~r~The Epsilonists were spooked.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_SPOOKED")
				BREAK
			ENDSWITCH
	
			IF failReason = FAILED_GENERIC
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			iFailStage = 1
		BREAK
		
		CASE 1
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 822.7919, 1278.0978, 359.4304 >>, 105.0195)
				//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<823.432, 1280.526, 359.923>>, -90.133)
				CPRINTLN(DEBUG_MISSION,"Restoring mission start outfit.")
				Reset_Outfit_Availability()
				RESTORE_MISSION_START_OUTFIT()
				RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)

				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Reset_Outfit_Availability()
		Script_Cleanup()
		TRIGGER_MUSIC_EVENT("EPS_FAIL")
	ENDIF
	
	IF NOT Is_Replay_In_Progress() // Set up the initial scene for replays
		IF NOT IS_CUTSCENE_ACTIVE()
			RC_REQUEST_CUTSCENE("ep_7_rcm", TRUE)
		ENDIF
	ELSE
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_EPSILON_7(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
		START_REPLAY_SETUP(<<523.37, 3077.92, 39.35>>, 329.9107)
	ENDIF
	
	// Get the PLAYER relationship group
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_MARNIE, MICHAEL_BOOK, FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JIMMY_BOSTON, MICHAEL_BOOK, FALSE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Esplison7")
		
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			CASE RC_INTRO
				STATE_Intro()
			BREAK
			CASE RC_WAITTOPASS
				STATE_WaitToPass()
			BREAK
			CASE RC_FAILED
				STATE_Failed()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
