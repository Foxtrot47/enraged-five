
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "CompletionPercentage_public.sch"
USING "initial_scenes_Epsilon.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Epsilon3.sc
//		AUTHOR			:	David Roberts
//		DESCRIPTION		:	MARNIE appears in the woods.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal
STRING leadInAnimDict = "rcmepsilonism3"

ENUM eRC_MainState
	RC_LEAD_IN = -1,
	RC_MEET_MARNIE = 0,
	RC_LEAVE_AREA = 1,
	RC_FAILED = 2
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_SKIPPED,
	SS_CLEANUP
ENDENUM

ENUM FAILED_REASON
	FAILED_GENERIC,
	FAILED_WOMAN_DIED,
	FAILED_WOMAN_HURT,
	FAILED_WOMAN_SCARED
ENDENUM

CONST_INT MARNIE	0

// Mission state
eRC_MainState 			  m_eState = RC_LEAD_IN
eRC_SubState  			  m_eSubState = SS_SETUP

// Player location 
STRING 					  sSceneHandleMarnie = "Marnie"
structPedsForConversation convStruct
FAILED_REASON 			failedReason = FAILED_GENERIC
BOOL					bConv 		= FALSE
BOOL					bExitState = FALSE
BOOL					bLeadInCloseStop = FALSE
SEQUENCE_INDEX 			seqWalkAway
SEQUENCE_INDEX			siEps3Flee
SCENARIO_BLOCKING_INDEX mScenarioBlocker
REL_GROUP_HASH 			relGroupPlayer
BOOL 					bTriggeredMusic = FALSE
INT 					iConvTimer
INT						iConvCloseTimer // Special timer for when you're particularly close to Marnie
INT 					iConvDelay // We change the delay on her conversation after she says her "go away" dialogue
INT 					iConvLinesSaid = 0

FLOAT fHintFov = 40.0
FLOAT fHintFollow = 0.25
FLOAT fHintPitchOrbit = 0.80
FLOAT fHintSide = -0.80
FLOAT fHintVert = 0.04

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
		IF IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID())
			TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[MARNIE])
		ENDIF
	ENDIF
	
	// Remove scenario blocks
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF
	
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[MARNIE])
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	
	IF PREPARE_MUSIC_EVENT("EPS3_STOP")
		IF TRIGGER_MUSIC_EVENT("EPS3_STOP")
			CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_STOP")
		ENDIF
	ENDIF
	
	// This is for the reminder e-mails for epsilon
	IF (g_savedGlobals.sAmbient.iEpsilonLastEmailSentDay = 0)
		g_savedGlobals.sAmbient.iEpsilonLastEmailSentDay = GET_CLOCK_DAY_OF_MONTH()
	ENDIF
	
	//Set epsilon step stat
	INT iCurrent
	STAT_GET_INT(NUM_EPSILON_STEP,iCurrent)
	IF iCurrent < 6
		STAT_SET_INT(NUM_EPSILON_STEP,6)
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH20),6)
		CPRINTLN(debug_dan,"Epsilon progress:",6)
	ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(convStruct, 3)
	Random_Character_Passed(CP_RAND_C_EPS3)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Sets the new mission state and initialises the substate.
PROC SetState(eRC_MainState in)
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		
		// Check for Pass
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			WAIT_FOR_CUTSCENE_TO_STOP()
			IF PREPARE_MUSIC_EVENT("EPS3_STOP")
				IF TRIGGER_MUSIC_EVENT("EPS3_STOP")
					CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_STOP")
				ENDIF
			ENDIF
			Script_Passed()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_CUTSCENE_ACTIVE()
				WAIT_FOR_CUTSCENE_TO_STOP()
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
					TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[MARNIE], leadInAnimDict, "base_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					CPRINTLN(DEBUG_MISSION, "Animate Marnie - base loop") 
				ENDIF
				SetState(RC_LEAVE_AREA)
			ELSE
				IF PREPARE_MUSIC_EVENT("EPS3_STOP")
					IF TRIGGER_MUSIC_EVENT("EPS3_STOP")
						CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_STOP")
					ENDIF
				ENDIF
				Script_Passed()
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF NOT IS_CUTSCENE_ACTIVE()
				bExitState = FALSE
				RC_REQUEST_CUTSCENE("EP_3_RCM_ALT1")
				REQUEST_ADDITIONAL_TEXT("EPS3", MISSION_TEXT_SLOT)
				SetState(RC_MEET_MARNIE)
			ENDIF
		ENDIF

		// Check for Fail
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			WAIT_FOR_CUTSCENE_TO_STOP()
			IF PREPARE_MUSIC_EVENT("EPS_FAIL")
				IF TRIGGER_MUSIC_EVENT("EPS_FAIL")
					CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_FAIL")
				ENDIF
			ENDIF
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    performs the lead in if need be, and goes onto next stage
PROC STATE_LeadIn()
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) <= 9.0
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDIF
	
	SWITCH m_eSubState
		
		CASE SS_SETUP
			
			REQUEST_ANIM_DICT(leadInAnimDict)
			REQUEST_ADDITIONAL_TEXT("EPS3", MISSION_TEXT_SLOT)
			
			WHILE NOT HAS_ANIM_DICT_LOADED(leadInAnimDict)
			AND NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				WAIT(0)
			ENDWHILE
			
			// Get the PLAYER relationship group
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
			ENDIF
			
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MARNIE])
				//SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[MARNIE], FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[MARNIE], relGroupPlayer)
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[MARNIE], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			ENDIF
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[MARNIE], TRUE)
				SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[MARNIE], TRUE)
			ENDIF
			
			bConv = FALSE
			ADD_PED_FOR_DIALOGUE(convStruct, 3, sRCLauncherDataLocal.pedID[MARNIE], "MARNIE")
			
			OPEN_SEQUENCE_TASK(seqWalkAway)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<1817.3235, 4708.1060, 38.1031>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
				TASK_WANDER_STANDARD(NULL)
			CLOSE_SEQUENCE_TASK(seqWalkAway)
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) >= 4.0
				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[MARNIE], <<0,0,-0.5>>, TRUE, 30000)
						ELSE
							SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[MARNIE], <<-0.75,0,-0.5>>, TRUE, 30000)
						ENDIF
						SET_GAMEPLAY_HINT_FOV(fHintFov)
						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
						SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
						
						//B* 2264625: Activate multihead blinders early
						SET_MULTIHEAD_SAFE(TRUE)
					ENDIF
				ELSE
					STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
				ENDIF
			ENDIF
		
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
				IF bLeadInCloseStop = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) <= 3.0
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1847.705200,4703.013672,36.584274>>, <<1844.083740,4709.531738,39.737553>>, 5.000000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1842.976929,4705.945801,36.780891>>, <<1842.160522,4707.851563,40.105244>>, 2.500000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1843.526733,4704.969238,36.837273>>, <<1844.177734,4703.326172,40.253311>>, 2.500000)
						// Stop the player
						IF IS_GAMEPLAY_HINT_ACTIVE()
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE])
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE], 7000)
						bLeadInCloseStop = TRUE
						CPRINTLN(DEBUG_MISSION, "Player too close to Mary Ann, making him stop") 
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) >= 4.0
					IF NOT IS_GAMEPLAY_HINT_ACTIVE()
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
							SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[MARNIE], <<-0.75,0,-0.5>>, TRUE, 30000)
							SET_GAMEPLAY_HINT_FOV(fHintFov)
							SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
							SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
						ENDIF
					ELSE
						STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT bConv
				bConv = CREATE_CONVERSATION(convStruct, "EPS3AUD", "EP3_RCM_L1", CONV_PRIORITY_HIGH)
				IF bConv
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
					TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[MARNIE], leadInAnimDict, "leadin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION| AF_HOLD_LAST_FRAME)
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					WAIT(500)
					m_eSubState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP

			CPRINTLN(DEBUG_MISSION, "Cleaning up RC_lead_in") 
	
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
				IF IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID())
				    TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[MARNIE])
				ENDIF
			ENDIF
			
			SetState(RC_MEET_MARNIE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Triggers the mocap cutscene and completes mission once finished
PROC STATE_MeetMarnie()
	SWITCH m_eSubState
	
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Init RC_MEET_MARNIE") 
			bTriggeredMusic = FALSE
			
			IF IS_REPLAY_IN_PROGRESS()
			
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
					CPRINTLN(DEBUG_MISSION, "Ending replay setup") 
				ENDIF
				
				RC_REQUEST_CUTSCENE("EP_3_RCM_ALT1")
				REQUEST_ADDITIONAL_TEXT("EPS3", MISSION_TEXT_SLOT)
				WHILE NOT HAS_CUTSCENE_LOADED()
				AND NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
					WAIT(0)
					CPRINTLN(DEBUG_MISSION, "Waiting for cutscene to load for replay") 
				ENDWHILE
				
				RC_END_Z_SKIP(FALSE, FALSE, FALSE)
				CPRINTLN(DEBUG_MISSION, "End Z skip") 
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RC_PLAYER_TRIGGER_SCENE_LOCK_IN()

			ELIF RC_IS_CUTSCENE_OK_TO_START()
				CPRINTLN(DEBUG_MISSION, "Start cutscene") 
				
				// Start mocap scene
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[MARNIE], sSceneHandleMarnie, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				// Cleanup launcher which will remove lead-in blip
				RC_CLEANUP_LAUNCHER()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				START_CUTSCENE()
				WAIT(0)
				STOP_GAMEPLAY_HINT()
				IF Is_Replay_In_Progress()
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT, FALSE)
				ENDIF
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1842.190186,4707.640137,36.875866>>, <<1845.486084,4701.726074,40.280556>>, 9.0, 
													<<1819.4010, 4707.2886, 38.1416>>, 274.5804)
				RC_START_CUTSCENE_MODE(<< 1835.53, 4705.86, 38.1 >>)
				// Monitor cutscene
				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			
			IF WAS_CUTSCENE_SKIPPED()
				CPRINTLN(DEBUG_MISSION, "Cutscene skipped at ", GET_CUTSCENE_TIME())
				m_eSubState = SS_SKIPPED
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
				REPLAY_STOP_EVENT()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,3,0>>), PEDMOVEBLENDRATIO_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					bExitState = TRUE
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandleMarnie)
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
					TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[MARNIE], leadInAnimDict, "base_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS, 0.01)
					SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.pedID[MARNIE], <<1843.82, 4705.74, 40.80>>, 143.14, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[MARNIE], TRUE)
					CPRINTLN(DEBUG_MISSION, "Animate Marnie - base loop")
				ENDIF
			ENDIF
			
			IF NOT bExitState
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			
			IF HAS_CUTSCENE_FINISHED()
			    RC_END_CUTSCENE_MODE()
				
				m_eSubState = SS_CLEANUP // Complete mission
			ELSE
				IF NOT bTriggeredMusic
					IF PREPARE_MUSIC_EVENT("EPS3_START")
						IF TRIGGER_MUSIC_EVENT("EPS3_START")
							CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_START")
							bTriggeredMusic = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			
			SET_CUTSCENE_FADE_VALUES(false, false, FALSE, false)
			SAFE_FADE_SCREEN_OUT_TO_BLACK(250, FALSE)
			
			IF IS_CUTSCENE_PLAYING()
				WAIT_FOR_CUTSCENE_TO_STOP()
			ENDIF
			RC_END_CUTSCENE_MODE()
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
				TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[MARNIE], leadInAnimDict, "base_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
				SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.pedID[MARNIE], <<1843.82, 4705.74, 40.80>>, 143.14, TRUE)
				CPRINTLN(DEBUG_MISSION, "Animate Marnie - base loop (skip)")
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,3,0>>), PEDMOVEBLENDRATIO_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK)
					bExitState = TRUE
				ENDIF
			ENDIF
			
			WAIT(250)
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK(250, FALSE)
			m_eSubState = SS_CLEANUP
		BREAK
		
		CASE SS_CLEANUP
			//B* 2264625: Deactivate multihead blinders
			SET_MULTIHEAD_SAFE(FALSE)
			CPRINTLN(DEBUG_MISSION, "Cleanup RC_MEET_MARNIE") 
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			SetState(RC_LEAVE_AREA)
		BREAK
	ENDSWITCH
ENDPROC

PROC STATE_LeaveArea()
	SWITCH m_eSubState
	
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Init RC_LEAVE_AREA") 

			PRINT_NOW("EPS3_LEAVE", DEFAULT_GOD_TEXT_TIME, 1)
			iConvTimer = GET_GAME_TIMER() + 7500
			iConvCloseTimer = GET_GAME_TIMER()
			iConvDelay = 10000
			
			m_eSubState = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "Now running RC_LEAVE_AREA") 
		BREAK
		
		CASE SS_UPDATE
		
			IF bExitState = TRUE
//				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
//				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
//				OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
//				OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
//				OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) != 0
//				OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) != 0
//					IF IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
//						CLEAR_PED_TASKS(PLAYER_PED_ID())
//						bExitState = FALSE
//						CPRINTLN(DEBUG_MISSION, "Cancelled exit movement") 
//					ELSE
//						bExitState = FALSE
//						CPRINTLN(DEBUG_MISSION, "Not performing task")
//					ENDIF
//				ELSE
//					CPRINTLN(DEBUG_MISSION, "Control not pressed") 
//				ENDIF
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) <= 1.4
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
				AND (GET_GAME_TIMER() - iConvCloseTimer) > 5000
					IF IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(convStruct, "EPS3AUD", "EP3_FOLLOW", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							iConvTimer = GET_GAME_TIMER()
							iConvLinesSaid++
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(convStruct, "EPS3AUD", "EP3_FOLLOW", CONV_PRIORITY_HIGH)
							iConvTimer = GET_GAME_TIMER()
							iConvLinesSaid++
						ENDIF
					ENDIF
				ENDIF
			ELIF NOT IS_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND (GET_GAME_TIMER() - iConvTimer) > iConvDelay
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) <= 10
						IF iConvLinesSaid < 5
							IF CREATE_CONVERSATION(convStruct, "EPS3AUD", "EP3_FOLLOW", CONV_PRIORITY_HIGH)
								iConvTimer = GET_GAME_TIMER()
								iConvLinesSaid++
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(convStruct, "EPS3AUD", "EP3_AMB", CONV_PRIORITY_HIGH)
								iConvTimer = GET_GAME_TIMER()
								iConvDelay = 1000
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[MARNIE])
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF iConvLinesSaid < 5
						IF NOT IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID(), -1)
						ENDIF
					ENDIF
				ELSE
					IF IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[MARNIE], PLAYER_PED_ID())
						TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[MARNIE])
					ENDIF
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MARNIE]) >= 100
				AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE]), 3)
					m_eSubState = SS_CLEANUP
				ENDIF
				
				IF GET_IS_PETROL_DECAL_IN_RANGE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), 5.0)
				OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), 5.0, FALSE)
				OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), WEAPONTYPE_GRENADELAUNCHER, 5.0, FALSE)
				OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), WEAPONTYPE_RPG, 5.0, FALSE)
				OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), WEAPONTYPE_GRENADE, 5.0, FALSE)
				OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), WEAPONTYPE_MOLOTOV, 5.0, FALSE)
				OR IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[MARNIE], FALSE), WEAPONTYPE_FLARE, 5.0, FALSE)
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					OPEN_SEQUENCE_TASK(siEps3Flee)
						TASK_PLAY_ANIM(NULL, "rcmepsilonism3", "outro")
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
					CLOSE_SEQUENCE_TASK(siEps3Flee)
					TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MARNIE], siEps3Flee)
					failedReason = FAILED_WOMAN_SCARED
					SetState(RC_FAILED)
				ENDIF

				IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[MARNIE])
				OR IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MARNIE])
					TRIGGER_MUSIC_EVENT("EPS_FAIL")
					TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MARNIE], siEps3Flee)
					failedReason = FAILED_WOMAN_HURT
					SetState(RC_FAILED)
				ENDIF
			ELSE
				TRIGGER_MUSIC_EVENT("EPS_FAIL")
				failedReason = FAILED_WOMAN_DIED
				SetState(RC_FAILED)
			ENDIF
		
		BREAK
		
		CASE SS_CLEANUP
		
			IF PREPARE_MUSIC_EVENT("EPS3_STOP")
				IF TRIGGER_MUSIC_EVENT("EPS3_STOP")
					CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_STOP")
					ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD(DYNAMIC_THREAD_EPSILON3)

					CPRINTLN(DEBUG_MISSION, "EPSCARS.SC - WAITING TO SEND")
					
					FIRE_EMAIL_INTO_DYNAMIC_THREAD(DYNAMIC_THREAD_EPSILON3, CULTINITIAL)
					OVERRIDE_CONTENT_FOR_DYNAMIC_THREAD(DYNAMIC_THREAD_EPSILON3, "EPS_1STEMAIL")
					Script_Passed()
				ENDIF
			ENDIF
		
		BREAK		
		
	ENDSWITCH
ENDPROC

INT iFailStage = 0
BOOL bDoneFailDialogue

/// PURPOSE:
///    Main mission failed fade state
PROC STATE_Failed()

	SWITCH iFailStage
		CASE 0
			CLEAR_PRINTS()
			
			// set if there is any fail dialogue to play
			bDoneFailDialogue = TRUE
//			IF failedReason = FAILED_WOMAN_SCARED
//				bDoneFailDialogue = FALSE
//			ENDIF
			STRING sFailReason
	
			SWITCH failedReason
				CASE FAILED_GENERIC
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_GENERIC")
				BREAK
				CASE FAILED_WOMAN_DIED
					sFailReason = "EPS3_FAILKILL"  // ~r~Marnie died.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_WOMAN_DIED")
				BREAK
				CASE FAILED_WOMAN_HURT
					sFailReason = "EPS3_FAILHURT" // ~r~Marnie was hurt.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_WOMAN_HURT")
				BREAK
				CASE FAILED_WOMAN_SCARED
					sFailReason = "EPS3_FAILSCARE"  // ~r~Marnie was scared off.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED reason=FAILED_WOMAN_SCARED")
				BREAK
			ENDSWITCH
	
			IF failedReason = FAILED_GENERIC
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			iFailStage = 1
		BREAK
		
		CASE 1
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 822.7919, 1278.0978, 359.4304 >>, 105.0195)
				//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<823.432, 1280.526, 359.923>>, -90.133)
				
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
				IF bDoneFailDialogue = FALSE
					IF failedReason = FAILED_WOMAN_SCARED
						bDoneFailDialogue = CREATE_CONVERSATION(convStruct, "FAN2AU", "FAN2_SCARED", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	mScenarioBlocker = Eps4_Scenario_Blocker() //also used in this mission
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		IF PREPARE_MUSIC_EVENT("EPS_FAIL")
			IF TRIGGER_MUSIC_EVENT("EPS_FAIL")
				CPRINTLN(DEBUG_MISSION, "Playing audio - EPS3_FAIL")
			ENDIF
		ENDIF
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_EPSILON_3(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
		START_REPLAY_SETUP(<<1843.1725, 4701.2490, 37.5487>>, 320.2838)
		SetState(RC_MEET_MARNIE)
	ENDIF
	
	IF NOT Is_Replay_In_Progress()
		IF NOT IS_CUTSCENE_ACTIVE()
			// Request mocap cutcene
			RC_REQUEST_CUTSCENE("EP_3_RCM_ALT1", TRUE)
		ENDIF
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_MARNIE, MICHAEL_BOOK, FALSE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_AsTT")
		
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			CASE RC_LEAD_IN
				STATE_LeadIn()
			BREAK
			
			CASE RC_MEET_MARNIE
				STATE_MeetMarnie()
			BREAK
			
			CASE RC_LEAVE_AREA
				STATE_LeaveArea()
			BREAK
			
			CASE RC_FAILED
				STATE_Failed()
			BREAK

		ENDSWITCH

		// Check debug completion/failure
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
