// Includes
USING "RC_Helper_Functions.sch"
USING "flow_public_core.sch"
USING "rc_launcher_public.sch"

// Enums
ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

// Constants
CONST_INT EPSILON_2_CASH_AMOUNT      500
CONST_INT EPSILON_3_CASH_AMOUNT		 5000
CONST_INT EPSILON_8_CASH_AMOUNT		 50000

/// PURPOSE: contains all of the door's which I need to control in script
HASH_ENUM MISSION_CONTROLLED_DOORS_ENUM // THIS MUST BE A HASH_ENUM - 
	EPS2_FRONT_DOOR
ENDENUM

// Variables
MODEL_NAMES    mMarnieModel = GET_NPC_PED_MODEL(CHAR_MARNIE)
MODEL_NAMES    mJimmyModel = GET_NPC_PED_MODEL(CHAR_JIMMY)
REL_GROUP_HASH relgroupEpsilon8

/// PURPOSE: Tries to block any farming scenarios around the Epsilon 4 launch area
FUNC SCENARIO_BLOCKING_INDEX Eps4_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< 1789.20, 4658.00, 30.73 >>, << 1871.87, 4745.51, 40.17 >>)
ENDFUNC

/// PURPOSE: Tries to block any scenarios around the Epsilon 7 launch area
FUNC SCENARIO_BLOCKING_INDEX Eps7_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<529.7899, 3067.5781, 37.8313>>, <<545.8693, 3098.9985, 43.0209>>)
ENDFUNC

/// PURPOSE: Tries to block any scenarios in front of the Epsilon mansion for Epsilon 8
FUNC SCENARIO_BLOCKING_INDEX Eps8_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<-722.7994, 14.8417, 37.4501>>, <<-661.7510, 56.4227, 42.8989>>)
ENDFUNC

/// PURPOSE: 
///    Setup scene for Epsilon 1
FUNC BOOL SetupScene_EPSILON_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_TRUCK 0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT 	    iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_TRUCK] = BISON
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType    = RC_TRIG_LOCATE_POINT
			sRCLauncherData.triggerLocate[0] = << -1626.97, 4205.64, 83.01 >>
			sRCLauncherData.activationRange = 17.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical  = TRUE
			sRCLauncherData.sIntroCutscene = "EP_1_RCM_Concat"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Truck
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_TRUCK], <<-1619.53, 4204.10, 83.30>>, 77.92)
				IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKED)
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 27, 27)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_TRUCK], TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[0])
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			SET_AMBIENT_ZONE_STATE("AZ_EPSILONISM_01_HILLS", TRUE)
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR	
	
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates scene for Epsilon 2
FUNC BOOL SetupScene_EPSILON_2(g_structRCScriptArgs& sRCLauncherData)
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<246.81818, 374.06854, 103.12333>>
			sRCLauncherData.triggerLocate[1] = <<241.98058, 360.87244, 108.86990>>
			sRCLauncherData.triggerWidth = 9.0
			sRCLauncherData.activationRange = 2.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene = "EP_2_RCM"

			IF g_bSceneAutoTrigger
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE, TRUE)
			ENDIF
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK	
	
		CASE IS_WAIT_FOR_SCENE	
			
			IF NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<241.3607, 361.0345, 105.8884>>, 4.0, V_ILev_EpsStoreDoor)
				RETURN FALSE
			ENDIF
			
			// Door exits
			eInitialSceneStage = IS_COMPLETE_SCENE
		BREAK
		
		CASE IS_COMPLETE_SCENE	
		
			SET_DOOR_STATE(DOORNAME_EPSILON2_STORAGE_ROOM, DOORSTATE_UNLOCKED)
			SET_INTERIOR_CAPPED( INTERIOR_V_EPSILONISM, FALSE)
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for Epsilon 3
FUNC BOOL SetupScene_EPSILON_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_MARNIE 0
	
	// Variables
	MODEL_NAMES    mModel[1]
	INT 	       iCount
	VECTOR 		   vMarniePos = <<1843.82, 4705.74, 38.80>>
	FLOAT 		   fMarnieHeading = RAD_TO_DEG(1.99)
	BOOL           bCreatedScene
	REL_GROUP_HASH relGroupPlayer
	
	// Assign model names
	mModel[MODEL_MARNIE] = mMarnieModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_POINT
			sRCLauncherData.triggerLocate[0] = << 1842.66, 4704.65, 37.81 >>
			sRCLauncherData.activationRange = 9.0
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene = "EP_3_RCM_ALT1"
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmepsilonism3", "base_loop")
		
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE

			// Has scene been created?
			bCreatedScene = TRUE

			// Clear area around Marnie
			CLEAR_AREA_OF_PEDS(vMarniePos, 35)

			// Create Marnie
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARNIE, vMarniePos, fMarnieHeading, "EPSILON LAUNCHER RC - MARNIE")
					IF DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
					AND IS_PED_UNINJURED(PLAYER_PED_ID())
						relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
						
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherData.pedID[0], FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], relGroupPlayer)
						SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
					
						// Play anim
						TASK_PLAY_ANIM(sRCLauncherData.pedID[0], "rcmepsilonism3", "ep_3_rcm_marnie_meditating", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					ELSE
						bCreatedScene = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for Epsilon 4
FUNC BOOL SetupScene_EPSILON_4(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_MARNIE  0
	CONST_INT MODEL_JIMMY  	1
	
	// Variables
	MODEL_NAMES    mModel[2]
	INT 	       iCount
	BOOL           bCreatedScene
	REL_GROUP_HASH relGroupPlayer
	
	// Assign model names
	mModel[MODEL_MARNIE] = mMarnieModel
	mModel[MODEL_JIMMY]  = mJimmyModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.ActivationRange = 11.5
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "EP_4_RCM_CONCAT"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_epsilonism4", "ep_4_rcm_marnie_base_marnie", "ep_4_rcm_marnie_lookaround_marnie")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
			ENDIF
			
			// Create Marnie
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARNIE, <<1826.13, 4698.88, 38.92>>, 21.63, "EPSILON LAUNCHER RC - MARNIE")
					SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], relGroupPlayer)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Jimmy
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[1], CHAR_JIMMY_BOSTON, <<1827.24, 4699.76, 39.09>>, 57.09, "EPSILON LAUNCHER RC - JIMMY")
					SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[1], relGroupPlayer)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[1], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[1], ANCHOR_EYES, 0, 0)
					
					CLEAR_PED_TASKS(sRCLauncherData.pedID[1])
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[1], WEAPONTYPE_DIGISCANNER, INFINITE_AMMO, TRUE)
					TASK_PLAY_ANIM(sRCLauncherData.pedID[1], "rcm_epsilonism4", "ep_4_rcm_jimmyboston_base_jb", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					SAFE_TELEPORT_ENTITY(sRCLauncherData.pedID[1], <<1827.24, 4699.76, 39.09>>, 57.09, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF

			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: Creates initial scene for Epsilon 5.
/// NOTE:    Marnie in a weird part of Vinewood
FUNC BOOL SetupScene_EPSILON_5(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_MARNIE 0
	
	// Variables
	MODEL_NAMES    mModel[1]
	INT 	       iCount
	BOOL		   bCreatedScene
	REL_GROUP_HASH relGroupPlayer
	
	// Assign model names
	mModel[MODEL_MARNIE] = mMarnieModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<638.147827,106.128418,87.363007>>
			sRCLauncherData.triggerLocate[1] = <<646.644897,129.226624,93.149765>>
			sRCLauncherData.triggerWidth = 12.5
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "EP_5_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_epsilonism5", "ep_5_rcm_marnie_strokes_wall")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE

			// Create Marnie
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARNIE, << 636.58, 119.68, 90.56 >>, 80.62, "EPSILON LAUNCHER RC")	
				AND IS_PED_UNINJURED(PLAYER_PED_ID())
					relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
					SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], relGroupPlayer)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
	
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for Epsilon 6.
FUNC BOOL SetupScene_EPSILON_6(g_structRCScriptArgs& sRCLauncherData)
		
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_PLANE  	0
	CONST_INT MODEL_CULTIST 1
	
	// Variables
	MODEL_NAMES     mModel[2]
	INT 	        iCount
	VEHICLE_INDEX 	viPlane
	STRING 			sCultistAnimDict = "rcmepsilonism6"
	BOOL            bCreatedScene
	REL_GROUP_HASH  relGroupPlayer
	
	// Assign model names
	mModel[MODEL_PLANE]   = VELUM
	mModel[MODEL_CULTIST] = IG_TOMEPSILON
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-2869.424, 3170.774, 1.954>>
			sRCLauncherData.triggerLocate[1] = <<-2918.701, 3217.056, 32.294>> 
			sRCLauncherData.triggerWidth = 94.500000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			REQUEST_ANIM_DICT(sCultistAnimDict)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sCultistAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	
			
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Check for vehicle gen version of Epsilon plane
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				viPlane = GET_RANDOM_VEHICLE_IN_SPHERE(<<-2892.93,3192.37,11.66>>, 11.0, mModel[MODEL_PLANE], VEHICLE_SEARCH_FLAG_RETURN_PLANES_ONLY | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
				IF IS_ENTITY_ALIVE(viPlane)
					sRCLauncherData.vehID[0] = viPlane
					CPRINTLN(DEBUG_RANDOM_CHAR, "There's a plane here lets use that")
					SET_ENTITY_AS_MISSION_ENTITY(viPlane, TRUE, TRUE)
				ELSE
					CLEAR_AREA_OF_VEHICLES(<<-2892.93,3192.37,11.66>>, 11.0)
					CPRINTLN(DEBUG_RANDOM_CHAR, "There is no a plane here, create one")
					CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_PLANE],<<-2892.93,3192.37,11.66>>, -132.35)
				ENDIF
			ENDIF
	
			// Disable gen
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_EPSILON6_PLANE, FALSE)	

			// Setup plane
			IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_PLANE], TRUE)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(sRCLauncherData.vehID[0])
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 0)
				SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 157, 157)
				SET_VEHICLE_EXTRA_COLOURS(sRCLauncherData.vehID[0], 157, 5)
				SET_VEHICLE_LIVERY(sRCLauncherData.vehID[0], 3)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(sRCLauncherData.vehID[0], TRUE)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				
				IF g_bSceneAutoTrigger
					//Move player to just inside the trigger zone
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-2907.29, 3215.91, 9.86>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 215.2149)
					ENDIF
				ENDIF
			ELSE
				bCreatedScene = FALSE
			ENDIF
	
			// Create Tom
			CREATE_SCENE_PED(sRCLauncherData.pedID[0], mModel[MODEL_CULTIST], << -2881.7554, 3188.1252, 10.1136 >>, 254.1723)
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
			AND IS_PED_UNINJURED(PLAYER_PED_ID())
				relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())

				GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_DIGISCANNER, INFINITE_AMMO, TRUE)
				SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[0])
				SET_PED_DIES_WHEN_INJURED(sRCLauncherData.pedID[0], TRUE)
				SET_PED_CAN_BE_TARGETTED(sRCLauncherData.pedID[0], FALSE)
			
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableHurt, TRUE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
				STOP_PED_SPEAKING(sRCLauncherData.pedID[0], TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0], relGroupPlayer)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], FALSE)
			ELSE
				bCreatedScene = FALSE
			ENDIF

			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates initial scene for Epsilon 7
FUNC BOOL SetupScene_EPSILON_7(g_structRCScriptArgs& sRCLauncherData)

	sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
	sRCLauncherData.triggerLocate[0] = <<513.48, 3081.22, 38.84>>
	sRCLauncherData.triggerLocate[1] = <<530.53, 3073.00, 44.13>>
	sRCLauncherData.triggerWidth = 9.25
	sRCLauncherData.bAllowVehicleActivation = FALSE
	sRCLauncherData.sIntroCutscene = "EP_7_RCM"
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: 
///    Creates scene for Epsilon 8
FUNC BOOL SetupScene_EPSILON_8(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CAR1             0
	CONST_INT MODEL_CAR2             1
	CONST_INT MODEL_SECURITY_GUARDS  2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT 	    iCount
	
	// Assign model names
	mModel[MODEL_CAR1] = LANDSTALKER
	mModel[MODEL_CAR2] = TAILGATER
	mModel[MODEL_SECURITY_GUARDS] = S_M_M_HIGHSEC_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-670.968079,53.530624,40.043388>>//<<-668.629456,49.649693,39.508224>>
			sRCLauncherData.triggerLocate[1] = <<-723.763184,34.054497,46.970173>>//<<-721.730652,32.638142,46.981270>>
			sRCLauncherData.triggerWidth = 19.750000//16.750000
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "EP_8_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE

			// Create Epsilon cars
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR1], << -686.5046, 43.9098, 42.2067 >>, 296.8100)
			SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0],157,157)
			SET_VEHICLE_EXTRA_COLOURS(sRCLauncherData.vehID[0],0,0)
			SET_DISABLE_PRETEND_OCCUPANTS(sRCLauncherData.vehID[0],TRUE)
			SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[0], 0) 
	
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_CAR2], <<-697.321533,39.040600,42.867363>>, -66.988541)
			SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[1], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[1],"K1FFL0M")
			SET_VEHICLE_COLOURS(sRCLauncherData.vehID[1],157,157)
			SET_VEHICLE_EXTRA_COLOURS(sRCLauncherData.vehID[1],0,0)
			SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[1], 0) 

			// Create Epsilon security
			// Guy at west entrance of epsilon mansion
			CREATE_SCENE_PED(sRCLauncherData.pedID[0],mModel[MODEL_SECURITY_GUARDS],<<-726.455505,33.335682,43.226952>>,-147.809906)
			GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0],WEAPONTYPE_SMG,-1,FALSE,FALSE)
			TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0],PLAYER_PED_ID(),-1)
			SET_PED_LEG_IK_MODE(sRCLauncherData.pedID[0], LEG_IK_FULL)
			TASK_START_SCENARIO_IN_PLACE(sRCLauncherData.pedID[0],"WORLD_HUMAN_GUARD_STAND")
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)

			// Guy at east entrance of epsilon mansion
			CREATE_SCENE_PED(sRCLauncherData.pedID[1],mModel[MODEL_SECURITY_GUARDS],<<-666.551208,49.197594,42.071537>>,-171.680267)
			GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[1],WEAPONTYPE_SMG,-1,FALSE,FALSE)
			TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[1],PLAYER_PED_ID(),-1)
			SET_PED_LEG_IK_MODE(sRCLauncherData.pedID[1], LEG_IK_FULL)
			TASK_START_SCENARIO_IN_PLACE(sRCLauncherData.pedID[1],"WORLD_HUMAN_GUARD_STAND")
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)

			// Create security inside the car
			CREATE_SCENE_PED(sRCLauncherData.pedID[2],mModel[MODEL_SECURITY_GUARDS],<< -690.0, 42.0, 42.0 >>,0.0)
			CREATE_SCENE_PED(sRCLauncherData.pedID[3],mModel[MODEL_SECURITY_GUARDS],<< -690.0, 42.0, 43.0 >>,0.0)
			SET_PED_INTO_VEHICLE(sRCLauncherData.pedID[2],sRCLauncherData.vehID[0],VS_DRIVER)
			SET_PED_INTO_VEHICLE(sRCLauncherData.pedID[3],sRCLauncherData.vehID[0],VS_FRONT_RIGHT)
			GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[2],WEAPONTYPE_COMBATPISTOL,-1,FALSE,FALSE)
			GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[3],WEAPONTYPE_MICROSMG,-1,FALSE,FALSE)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[3], INT_TO_ENUM(PED_COMPONENT,11), 0, 1, 0) //(jbib)

			// Set relationship groups
			ADD_RELATIONSHIP_GROUP("gang",relgroupEpsilon8)
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[0],relgroupEpsilon8)
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[1],relgroupEpsilon8)
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[2],relgroupEpsilon8)
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherData.pedID[3],relgroupEpsilon8)
			SET_PED_CAN_EVASIVE_DIVE(sRCLauncherData.pedID[0],FALSE)
			SET_PED_CAN_EVASIVE_DIVE(sRCLauncherData.pedID[1],FALSE)
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
