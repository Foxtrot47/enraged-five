USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

MODEL_NAMES mContactModel = IG_HUNTER
 
/// PURPOSE: 
///    Creates the scene for TheLastOne.
FUNC BOOL SetupScene_TheLastOne(g_structRCScriptArgs& sRCLauncherData)
 
 	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  			0
	CONST_INT MODEL_CONTACT_VEHICLE  	1
	CONST_INT MODEL_CRATE				2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = DUNE
	mModel[MODEL_CRATE]           = PROP_CRATE_07A
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
 	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "sas_1_rcm_concat"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			REQUEST_ANIM_DICT("rcmlastone1")
			
			// Request weapons
			REQUEST_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_SNIPERRIFLE)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmlastone1")
			OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PUMPSHOTGUN)
			OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_SNIPERRIFLE)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create the hunter
			sRCLauncherData.pedID[0] = CREATE_PED(PEDTYPE_SPECIAL, mContactModel,<< -1299.02, 4639.86, 106.66 >> , 345.7769)
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
				GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_SNIPERRIFLE, 100, TRUE)
				TASK_PLAY_ANIM(sRCLauncherData.pedID[0], "rcmlastone1", "idle_action_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
			ELSE
				bCreatedScene = FALSE
			ENDIF

			// Setup buggy
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CONTACT_VEHICLE], << -1303.1628, 4648.0850, 104.9664 >>, 293.1504)
				IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0],0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH

	// Scene not ready
	RETURN FALSE
ENDFUNC
