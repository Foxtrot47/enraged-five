
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "dialogue_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "locates_public.sch"
USING "CompletionPercentage_public.sch"
USING "initial_scenes_TheLastOne.sch"
USING "rgeneral_include.sch"
USING "RC_Area_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_public.sch"
USING "RC_Pedsight_public.sch"
USING "script_blips.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	TheLastOne.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Chase the last one through the woods
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//*******************************************************************************************************************
//	DATATYPES
//*******************************************************************************************************************

ENUM STATE_MODE
	SETUP_STATE,
	UPDATE_STATE,
	DONE_STATE
ENDENUM

//-- Trail

CONST_INT TRAIL_SIZE_MAX 25

ENUM TRAIL_NODE_TYPE
	NODETYPE_HOWL,
	NODETYPE_SIGHTING
ENDENUM

STRUCT TRAIL_NODE_DATA
	TRAIL_NODE_TYPE		type
	VECTOR				vPedSpawn
	FLOAT				fPedSpawnRot
	VECTOR				vPedDest
	FLOAT				fTriggerDist
	FLOAT				fTriggerDistCar
	INT					iLeadInTime
	INT					iExclusionAngle

	OBJECT_INDEX		hBushA
	OBJECT_INDEX		hBushB
ENDSTRUCT

STRUCT TRAIL_DATA
	STATE_MODE			mode

	TRAIL_NODE_DATA		nodes[TRAIL_SIZE_MAX]
	INT					curNode
	INT					size
ENDSTRUCT


//-- The Last One

ENUM THELASTONE_STATE
	BF_NULL = 0,

	BF_LEADIN,
	BF_HOWL,
	BF_ESCAPE,
	BF_LEADOUT,
	BF_BRAWL,
	BF_BLOCKED_FIGHT
ENDENUM

STRUCT THELASTONE_DATA
	THELASTONE_STATE	state
	STATE_MODE			mode

	PED_INDEX			hPed
	BLIP_INDEX			hBlip
	INT					timer
	
	INT					hHowlSnd
	INT					hGrumbleSnd
	INT					hCoughSnd
	INT					iNode
	
	INT					iBrawlingState
	BOOL				bIsLastOneShotAt
	
	BOOL				bHowlTimeoutStatus
	INT					howlTimeout

ENDSTRUCT


//-- Start-scene

STRUCT START_SCENE
	PED_INDEX			hHunter
	OBJECT_INDEX		hCrate
	PICKUP_INDEX		hGun
	PICKUP_INDEX		hShotGun
	
	BOOL				bHasBeenCreated
ENDSTRUCT


//-- Dressing

STRUCT DRESSING
	OBJECT_INDEX		hScat
	BOOL				bHasBeenCreated
ENDSTRUCT


//-- Lion

ENUM LION_STATE
	LION_STATE_NULL = 0,
	LION_STATE_READY,
	LION_STATE_POUNCE,
	LION_STATE_FLEE
ENDENUM

STRUCT LION_DATA
	PED_INDEX			hPed
	LION_STATE			state
	STATE_MODE			mode
	AI_BLIP_STRUCT		hBlip

	VECTOR				vSpawnPos
	FLOAT				fSpawnRot
	FLOAT				fTriggerRadius
ENDSTRUCT


//*******************************************************************************************************************
//	STAGE DATATYPES
//*******************************************************************************************************************

ENUM MISSION_STAGE
	MS_NULL = -1,
	MS_INIT,
	MS_INTRO,
	MS_GOTOVALLEY,
	MS_CHASE,
	MS_OUTRO,
	
	MS_PASSED,
	MS_FAILED
ENDENUM


CONST_INT	CP_INTRO		0
CONST_INT	CP_GOTOVALLEY	1
CONST_INT	CP_CHASE		2
CONST_INT	CP_END_OF_CHASE	3
CONST_INT	CP_OUTRO		4
CONST_INT	CP_GOTOVALLEY2	5
CONST_INT	MAX_CP			6


ENUM STAGE_SECTION
	SECTION_SETUP = 0,
	SECTION_RUNNING,
	SECTION_FADEOUT,
	SECTION_CLEANUP,
	SECTION_SKIP
ENDENUM

ENUM FAIL_REASON
	FAIL_REASON_DEFAULT,
	FAIL_REASON_PLAYER_DEAD,
	FAIL_REASON_HUNTER_DEAD,
	FAIL_REASON_HUNTER_THREATENED,
	FAIL_REASON_LOST_THELASTONE
ENDENUM

STRUCT MISSION_DATA
	MISSION_STAGE		stage
	STAGE_SECTION		section
	INT					iEvent

	// A stage can store what stage should be next, then run through it's exit section. At the end of the exit section it will switch to this stage.
	BOOL				bSyncForSkip
	BOOL				bRequestJSkip
	BOOL				bRequestPSkip
	BOOL				bRequestCpSkip
	INT					queueCpSkip
	
	MISSION_STAGE		queueNextStage
	
	// When the mission goes to fail-state, it should set this so the right message can be displayed
	FAIL_REASON			failReason

ENDSTRUCT


MISSION_DATA			g_mission

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct zSkipMenu[MAX_CP]
#ENDIF

g_structRCScriptArgs sRCLauncherDataLocal


//*******************************************************************************************************************
//	DATA
//*******************************************************************************************************************

START_SCENE		gStartScene
DRESSING		gDressing
TRAIL_DATA		gTrail
THELASTONE_DATA	gTheLastOne
VECTOR			vDeadInRoadPos = << -1568.8677, 4542.4253, 16.1893 >>

VECTOR vIntroAreaPos1 = <<-1295.969116,4641.688965,104.378647>>
VECTOR vIntroAreaPos2 = <<-1303.088623,4639.469727,107.984077>> 
FLOAT  fIntroAreaWidth = 6.0
VECTOR vRoadAreaPos1 = <<-1568.374268,4546.200195,15.217278>>
VECTOR vRoadAreaPos2 = <<-1568.955566,4539.636719,18.195089>> 
FLOAT  fRoadAreaWidth = 7.0
VECTOR vCliffAreaPos1 = <<-1537.227783,4535.780273,45.627308>>
VECTOR vCliffAreaPos2 = <<-1538.339355,4540.548828,48.822357>> 
FLOAT  fCliffAreaWidth = 8.0

CONST_INT		LION_MAX 4//8
LION_DATA		gLionArray[LION_MAX]
INT				iLionCount

BLIP_INDEX		g_hBlip
#IF IS_DEBUG_BUILD
	BOOL			g_bDebugWaypoints
#ENDIF

CONST_INT		HOWL_TIMEOUT				60000
CONST_INT		PLAYER_LEAVING_RESET_TIME	30000
CONST_INT		HELP_TEXT_DELAY				1000

CONST_INT		HUNTER_ID	0

OBJECT_INDEX oiRifle

/*CONST_INT NUM_VEHICLE_CHASE_LINES 3
STRING sVehicleChaseLines[NUM_VEHICLE_CHASE_LINES]
INT iCurrentVehicleChaseLines = 0
CONST_INT VEHICLE_CHASE_LINE_TIME 20000
INT iVehicleChaseLineTimer = 0*/

TEST_POLY		g_polyPlayArea
TEST_POLY		g_polyCliffArea


BOOL	g_bObjDoneGotoValley	= FALSE
BOOL	g_bObjDoneLeavingValley	= FALSE
//BOOL	g_bHintGiven			= FALSE
BOOL	g_bObjDoneInvestigate	= FALSE
//BOOL	g_bPrintCarObj			= TRUE
BOOL	g_bObjDoneChase			= FALSE

BOOL bBlockedByCar = FALSE
INT		g_iPlayerLeavingTimer	= 0

BOOL	g_bDestroyCarConvReady	= TRUE

BOOL	g_bAudDoneStartGoValley	= FALSE
BOOL	g_bAudDoneStartGetGun	= FALSE
BOOL	g_bAudDoneHowl			= FALSE
BOOL	g_bAudDoneHeardHowl		= FALSE
BOOL	g_bAudDoneChase			= FALSE
//BOOL	g_bAudDoneWounded		= FALSE
BOOL	g_bAudDoneCornered		= FALSE

BOOL	g_bCutsceneInRoad		= FALSE

BOOL	g_bStartAnimSequence	= TRUE

BOOL	bLoadChaseModels		= TRUE

BOOL	g_bHelpDoneListen		= FALSE
INT		g_iHelpDelayListen		= 0

INT iExplosionCounter = 0
INT iExplosionTimer = 0

INT iBlockLookBehind = -1

// Used for the sight test on the last one
INT iSightTestID = -1

INT iPassDelay
CONST_INT PASS_DELAY_TIME 5000

INT iHowlTimer = -1
CONST_INT HOWL_TIME 2000

INT iRunTimer = -1
INT iHideTimer = -1
CONST_INT CHASE_CONV_TIME 7000

INT iBumpTimer = 0
CONST_INT BUMP_TIME 10000
INT iTargetTimer = 0
CONST_INT TARGET_TIME 7000
INT iShootTimer = 0
CONST_INT SHOOT_TIME 7000
INT iReturnTimer = 0
CONST_INT RETURN_TIME 40000

INT iTriggerPostFxTime
BOOL bTriggerPostFx

structPedsForConversation pedsForConversation

INT iOutroStartTimer
//INT iConvFailedTimer = -1

CONST_INT NUM_BARKS 4
STRING sBarks[NUM_BARKS]
INT iCurrentBark = 0
INT iBarkTimer = 0
CONST_INT TIME_BETWEEN_BARKS 5000

//*******************************************************************************************************************
//	MODELS + POSITIONS
//*******************************************************************************************************************

WEAPON_TYPE	weapon_HunterGun		= WEAPONTYPE_SNIPERRIFLE
WEAPON_TYPE	weapon_HunterShotGun	= WEAPONTYPE_PUMPSHOTGUN

MODEL_NAMES	model_TheLastOne		= IG_ORLEANS
MODEL_NAMES model_MainHunter		= IG_HUNTER
MODEL_NAMES	model_HunterCar			= DUNE
MODEL_NAMES	model_HunterGun			= GET_WEAPONTYPE_MODEL(weapon_HunterGun)
MODEL_NAMES	model_HunterShotGun		= GET_WEAPONTYPE_MODEL(weapon_HunterShotGun)
MODEL_NAMES	model_HunterCrate		= PROP_CRATE_07A

MODEL_NAMES model_DressingScat		= PROP_BIG_SHIT_02
MODEL_NAMES	model_DressingTruck		= REBEL

MODEL_NAMES	model_Lion				= A_C_MTLION

STRING	animdict_Plead				= "rcmlastone1"//"rcmsasquatch1"
STRING	animdict_Leadout			= "rcmlastone2leadinout"//"rcmsasquatch2leadinoutsas_2_rcm"
STRING	EMPTY_STRING				= "EMPTY"
ANIMATION_FLAGS eHunterAnimFlags = AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION

/*
VECTOR	vPos_OutroRespot			= << -1537.1522, 4545.0649, 45.8478 >>
FLOAT	fRot_OutroRespot			= 39.3353
*/

VECTOR	vPos_RespotLastCar			= << -1582.8912, 4758.9502, 49.7778 >>
FLOAT	fRot_RespotLastCar			= 357.0128

VEHICLE_INDEX viMissionFinishedVehicle
VECTOR	vPos_DressingTruck			= << -1557.5588, 4592.0908, 18.7528 >>
FLOAT	fRot_DressingTruck			= 352.9490

VECTOR	vPos_ScatSite
VECTOR	vRot_ScatSite

VECTOR vPos_Valley = <<-1581.155518,4698.455566,46.330231>>

VECTOR	vPos_BrawlOffArena
VECTOR	vPos_BrawlOnArena
//FLOAT	fRad_BrawlOn

VECTOR	vPos_StartPlayerSpawn
FLOAT	fRot_StartPlayerSpawn

VECTOR	vPos_StartHunterSpawn
FLOAT	fRot_StartHunterSpawn
VECTOR	vPos_CheckHunterPushed = <<-1297.85, 4639.92, 106.64>>

//VECTOR	vPos_StartCrateSpawn = <<-1296.59, 4639.90, 105.57>>
//VECTOR	vRot_StartCrateSpawn = <<-3.00, -7.00, -125.34>>

VECTOR	vPos_StartGunSpawn = <<-1297.21, 4637.65, 105.66>>
VECTOR	vRot_StartGunSpawn = <<-100.08, -59.60, -103.04>>

VECTOR	vPos_StartShotGunSpawn = <<-1296.76, 4637.52, 105.67>>
VECTOR	vRot_StartShotGunSpawn = <<-90.00, -55.19, -141.85>>

VECTOR	vPos_ChasePlayerStart
FLOAT	fRot_ChasePlayerStart

VECTOR	vClearZone0 = << -1651.2321, 4672.7104, 30.9844 >> // radius 50
VECTOR	vClearZone1 = << -1625.1282, 4586.0063, 42.1184 >> // radius 50
VECTOR	vClearZone2 = << -1486.2358, 4551.2949, 42.7094 >> // radius 50
VECTOR	vClearZone3 = << -1565.4634, 4601.7319, 19.5286 >> // radius 20

VECTOR vIntroAreaMin = <<-1315.815430,4627.916504,94.387100>>
VECTOR vIntroAreaMax = <<-1273.391968,4659.724121,113.797508>>

// variables to check for mission passed stats
BOOL bToScatOnFoot = TRUE

INT iChangeFaceTimer = -1
BOOL bChangeFace = TRUE

//*******************************************************************************************************************
//	SETUP
//*******************************************************************************************************************

/// PURPOSE:
///    Adds a node for Sasquatch to use
/// PARAMS:
///    type - The type of node being added
///    vPedSpawn - The position Sasquatch will be spawned at
///    fPedSpawnRot - The rotation Sasquatch will be spawned at
///    vPedDest - Sasquatch's destination to run to
///    fTriggerDist - The distance from this node the player will trigger it on foot
///    fTriggerDistCar - The distance from this node the player will trigger it in a vehicle
///    iLeadInTime - 
///    iExclusionAngle - If the player is in this angle along Sasquatch's route, it will skip this node
PROC ADD_NODE(TRAIL_NODE_TYPE type, VECTOR vPedSpawn, FLOAT fPedSpawnRot, VECTOR vPedDest, FLOAT fTriggerDist, FLOAT fTriggerDistCar, INT iLeadInTime, INT iExclusionAngle=60)
	IF gTrail.size >= TRAIL_SIZE_MAX
		SCRIPT_ASSERT("Trying to add too many trail nodes")
	ENDIF
	
	INT idx = gTrail.size
	gTrail.nodes[idx].type				= type
	gTrail.nodes[idx].hBushA			= NULL
	gTrail.nodes[idx].hBushB			= NULL
	gTrail.nodes[idx].vPedSpawn			= vPedSpawn
	gTrail.nodes[idx].fPedSpawnRot		= fPedSpawnRot
	gTrail.nodes[idx].vPedDest			= vPedDest
	gTrail.nodes[idx].fTriggerDist		= fTriggerDist
	gTrail.nodes[idx].fTriggerDistCar	= fTriggerDistCar
	gTrail.nodes[idx].iLeadInTime		= iLeadInTime
	gTrail.nodes[idx].iExclusionAngle	= iExclusionAngle
	gTrail.size++
ENDPROC

/// PURPOSE:
///    Adds a node where Sasquatch runs from one point to another
/// PARAMS:
///    vPedSpawn - The position Sasquatch will be spawned at
///    vPedDest - The position Sasquatch will run to
///    fTriggerDist - The distance from this node the player will trigger it on foot
///    fTriggerDistCar - The distance from this node the player will trigger it in a vehicle
///    iLeadInTime - 
///    iExclusionAngle - If the player is in this angle along Sasquatch's route, it will skip this node
PROC ADD_SIGHTING(VECTOR vPedSpawn, VECTOR vPedDest, FLOAT fTriggerDist = 30.0, FLOAT fTriggerDistCar = 70.0, INT iLeadInTime = 4000, INT iExclusionAngle = 60)
	ADD_NODE(NODETYPE_SIGHTING, vPedSpawn, 0, vPedDest, fTriggerDist, fTriggerDistCar, iLeadInTime, iExclusionAngle)
ENDPROC

/// PURPOSE:
///    Sets the location the player must reach for the chase to start
/// PARAMS:
///    vPos - The position the player must reach for the chase to start
///    fRot - The heading to set the player to when skipping to the chase section
PROC SET_CHASE_START(VECTOR vPos, FLOAT fRot)
	vPos_ChasePlayerStart = vPos
	fRot_ChasePlayerStart = fRot
ENDPROC

/// PURPOSE:
///    Sets the position the player is set to at the start of the mission
/// PARAMS:
///    vPos - The position the player will be set to after the intro
///    fRot - The heading the player will be set to after the intro
PROC SET_START_POS_PLAYER(VECTOR vPos, FLOAT fRot)
	vPos_StartPlayerSpawn	= vPos
	fRot_StartPlayerSpawn	= fRot
ENDPROC

/// PURPOSE:
///    Sets the position the hunter is set to at the start of the mission
/// PARAMS:
///    vPos - The position the hunter will be set to after the intro
///    fRot - The heading the hunter will be set to after the intro
PROC SET_START_POS_HUNTER(VECTOR vPos, FLOAT fRot)
	vPos_StartHunterSpawn = vPos
	fRot_StartHunterSpawn = fRot
ENDPROC

/// PURPOSE:
///    Sets the position of the scat site
/// PARAMS:
///    vPos - The position the scat will be spawned at
///    vRot - The rotation the scat will be spawned at
PROC SET_SCATSITE_POS(VECTOR vPos, VECTOR vRot)
	vPos_ScatSite = vPos
	vRot_ScatSite = vRot
ENDPROC

/// PURPOSE:
///    Sets the position for the area where the last one will attack
/// PARAMS:
///    vOff - The centre of the area where the last one will stop attacking
///    vOn - The centre of the area where the last one will start attacking
///    fRadOn - The radius of the area where the last one will start attacking
PROC SET_BRAWLING_ARENA(VECTOR vOff, VECTOR vOn, FLOAT fRadOn)
	vPos_BrawlOffArena = vOff
	vPos_BrawlOnArena = vOn
	
	//fRad_BrawlOn = fRadOn
	fRadOn = fRadOn
ENDPROC

/// PURPOSE:
///    Initialises the values for adding a lion
/// PARAMS:
///    vPos - The position the lion will be created at
///    fRot - The heading the lion will be created at
///    fTriggerRadius - When the player is within this radius, the lion will be activated
PROC ADD_LION(VECTOR vPos, FLOAT fRot, FLOAT fTriggerRadius)
	IF iLionCount >= LION_MAX
		SCRIPT_ASSERT("Trying to add too many lions")
	ENDIF
	
	INT idx = iLionCount
	gLionArray[idx].state = LION_STATE_NULL
	gLionArray[idx].vSpawnPos		= vPos
	gLionArray[idx].fSpawnRot		= fRot
	gLionArray[idx].fTriggerRadius	= fTriggerRadius
	iLionCount++
ENDPROC

/// PURPOSE:
///    Initialises all the values used in the mission
PROC INIT_DATA()

	REGISTER_SCRIPT_WITH_AUDIO()
	SET_WANTED_LEVEL_MULTIPLIER(0.1)

	gTrail.size = 0
	vPos_ChasePlayerStart.z 	= 0
	vPos_StartPlayerSpawn.z		= 0
	vPos_StartHunterSpawn.z 	= 0
	
	// Setup route
	// -----------
		//SET_START_POS_PLAYER(<< -1302.2573, 4640.7910, 105.6339 >>, 304.7601)
	//SET_START_POS_PLAYER(<<-1306.4396, 4642.0522, 106.0141>>, 249.1855)
	SET_START_POS_PLAYER(<<-1301.9270, 4640.9043, 105.6265>>, 105.2908)
	SET_START_POS_HUNTER(<< -1298.7554, 4639.8711, 105.6797 >>, 31.4224)
	
	// Sneak over hill version
	SET_SCATSITE_POS(<< -1562.72, 4699.07, 49.81 >>, <<9.00, 3.00, -0.00>>)
	
	SET_CHASE_START(<< -1562.8904, 4699.2236, 49.9836 >>, 81.8633)

	ADD_SIGHTING(<< -1608.5690, 4704.6196, 40.9750 >>, <<-1638.412109,4669.232910,33.017025>>, 46.5, -1, 0)		// First glimpse, and up hill
	//ADD_SIGHTING(<<-1641.1367, 4665.7402, 31.6091>>, <<-1638.610840,4651.239258,31.377729>>, 70, 80, 2000)
	ADD_SIGHTING(<<-1641.1367, 4665.7402, 31.6091>>, <<-1636.581177,4651.864258,32.587139>>, 70, 80, 2000)
	ADD_LION(<< -1628.3339, 4624.6309, 40.8662 >>, 19.5915, 30.0)
	
	//ADD_SIGHTING(<< -1640.1631, 4616.7466, 42.6314 >>, << -1630.2805, 4597.0176, 40.9653 >>, 30, -1, 2000)		// Over top of hill and on plateau
	ADD_SIGHTING(<< -1640.1631, 4616.7466, 42.6314 >>, <<-1629.573486,4595.201172,40.989510>>, 30, -1, 2000)		// Over top of hill and on plateau
	ADD_SIGHTING(<< -1617.9073, 4577.0332, 42.3259 >>, << -1594.5806, 4587.9233, 35.4044 >>, 40, 50, 6000)

	ADD_SIGHTING(<< -1579.0415, 4612.7061, 29.0916 >>, << -1547.6313, 4600.6743, 21.0284 >>, 30, -1)			// Across road
	//ADD_LION(<< -1534.6357, 4604.0591, 23.9694 >>, 91.2343, 45.0)
	
	ADD_SIGHTING(<< -1530.2128, 4619.2285, 29.8095 >>, << -1508.7700, 4618.6279, 38.1616 >>, 25.0, 45)			// Up into woods
	//ADD_SIGHTING(<< -1498.6869, 4620.0610, 42.6271 >>, << -1491.3115, 4608.3159, 43.9719 >>, 25.0, 45, 2500)
	ADD_SIGHTING(<<-1499.076416,4618.754395,42.879841>>, << -1491.3115, 4608.3159, 43.9719 >>, 25.0, 45, 2500)
	//ADD_SIGHTING(<< -1476.7128, 4613.2949, 47.3980 >>, << -1471.7212, 4598.9722, 46.1177 >>, 22.5, -1, 2500)
	ADD_SIGHTING(<< -1476.7128, 4613.2949, 47.3980 >>, <<-1469.926636,4596.369141,45.796448>>, 22.5, -1, 2500)
	
	ADD_LION(<< -1458.9276, 4590.8887, 46.9975 >>, 25.5399, 25.0)
	//ADD_LION(<<-1644.410645,4605.859375,44.830025>>, 15.5399, 25.0)
	ADD_LION(<<-1495.754272,4569.355469,37.398315>>, -70.47, 25.0)
	//ADD_LION(<< -1490.7159, 4598.1172, 41.5723 >>, 309.13, 25.0)
	ADD_LION(<< -1487.8613, 4550.3735, 42.6880 >>, 149.2, 25.0)
	//ADD_LION(<< -1638.7268, 4634.1973, 36.8269 >>, 141.94, 25.0)

	//ADD_SIGHTING(<< -1453.7600, 4579.5317, 44.3204 >>, << -1461.2373, 4561.9448, 41.7624 >>, 27.5, 80, 3000)	// Woods to cornered
	ADD_SIGHTING(<< -1453.7600, 4579.5317, 44.3204 >>, <<-1461.224609,4559.684082,43.082272>>, 27.5, 80, 3000)	// Woods to cornered
	//ADD_SIGHTING(<< -1467.3835, 4553.3125, 45.3815 >>, << -1489.1077, 4545.5947, 44.2197 >>, 27.5, 2250)
	//ADD_SIGHTING(<<-1469.6111, 4552.7456, 45.5610>>, << -1489.1077, 4545.5947, 44.2197 >>, 27.5, 2250)
	ADD_SIGHTING(<<-1469.6111, 4552.7456, 45.5610>>, <<-1490.853638,4546.869629,43.323307>>, 27.5, 2250)
	ADD_SIGHTING(<< -1505.9806, 4534.8262, 44.4468 >>, << -1539.3424, 4539.5303, 46.8468 >>, 45, 80, 0)

	SET_BRAWLING_ARENA(<<-1537.53,4539.51,47.87>>, <<-1537.53,4539.51,47.87>>, 10.0)

	// Setup play area polys
	OPEN_TEST_POLY(g_polyPlayArea)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1241.7709, 4394.9653, 3.8679 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1240.5320, 4611.6948, 130.3852 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1221.6125, 4702.2856, 125.4371 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1461.1195, 4795.8184, 84.8864 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1545.0295, 4769.5073, 61.4836 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1649.4033, 4828.1045, 59.7308 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1828.7227, 4677.1211, 56.0479 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1721.5918, 4503.2275, 0.0447 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1640.8074, 4488.2358, 0.3443 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1561.4725, 4377.0366, 2.2095 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1359.1692, 4339.5313, 3.8651 >>)
	CLOSE_TEST_POLY(g_polyPlayArea)
	
	// setup the area for the cliff Sasquatch ends on
	OPEN_TEST_POLY(g_polyCliffArea)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1541.932373,4538.026367,46.205505>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1530.172729,4515.199707,41.436943>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1507.496704,4498.375977,45.285702>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1489.460327,4511.402344,47.723251>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1507.926880,4587.871582,34.033413>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1535.494141,4571.508300,38.508503>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1543.280640,4554.625488,35.515556>>)
	ADD_TEST_POLY_VERT(g_polyCliffArea, <<-1539.656738,4545.944824,45.684471>>)
	CLOSE_TEST_POLY(g_polyCliffArea)
	
	sBarks[0] = "SAS1_LEADOUT_3"
	sBarks[1] = "SAS1_LEADOUT_4"
	sBarks[2] = "SAS1_LEADOUT_5"
	sBarks[3] = "SAS1_LEADOUT_6"
	
ENDPROC

//*******************************************************************************************************************
//	UTILS
//*******************************************************************************************************************
/// PURPOSE:
///    Loads mission text
/// RETURNS:
///    TRUE if the mission text has loaded, FALSE otherwise
FUNC BOOL LOAD_TEXT()
	// Request
	REQUEST_ADDITIONAL_TEXT("SAS1", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("SAS1AUD", MISSION_DIALOGUE_TEXT_SLOT)

	// Check
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Enables or disables a ped
/// PARAMS:
///    hPed - The ped to be enabled/disabled
///    bVisible - TRUE to enable the ped, FALSE to disable
PROC SET_PED_ENABLED(PED_INDEX hPed, BOOL bVisible)
	FREEZE_ENTITY_POSITION(hPed, NOT bVisible)
	SET_ENTITY_COLLISION(hPed, bVisible)
	SET_ENTITY_VISIBLE(hPed, bVisible)
ENDPROC

/// PURPOSE:
///    Checks if a ped has been enabled or not
/// PARAMS:
///    hPed - The ped to check
/// RETURNS:
///    TRUE if the ped is enabled, FALSE otherwise
FUNC BOOL IS_PED_ENABLED(PED_INDEX hPed)
	RETURN IS_ENTITY_VISIBLE(hPed)
ENDFUNC

/// PURPOSE:
///    Checks if a ped has been harmed
/// PARAMS:
///    hPed - The ped to check
/// RETURNS:
///    TRUE if the ped is injured, doesn't exist or has been harmed, FALSE otherwise
FUNC BOOL IS_PED_HARMED(PED_INDEX hPed)
	IF DOES_ENTITY_EXIST(hPed)
		IF /*(HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(hPed, PLAYER_PED_ID(), TRUE) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(hPed, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
		OR*/ IS_PED_INJURED(hPed)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player destroys the hunter's car and plays a conversation if they have
PROC CheckHunterCar()
	IF g_bDestroyCarConvReady
		IF NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HUNTER_ID]) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), gStartScene.hHunter) < 25.0
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_BUGGY", CONV_PRIORITY_HIGH)
				g_bDestroyCarConvReady = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//*******************************************************************************************************************
//	MISSION STAGE FUNCTIONS
//*******************************************************************************************************************

/// PURPOSE:
///    Loads the last one model and anim dictionary
PROC LOAD_THELASTONE_MODELS()
	REQUEST_MODEL(model_TheLastOne)
	REQUEST_ANIM_DICT(animdict_Plead)
	REQUEST_ANIM_DICT(animdict_Leadout)
ENDPROC

/// PURPOSE:
///    Checks if the last one model and anim dictionary have loaded
/// RETURNS:
///    TRUE if the last one model and anim dictionary have loaded, FALSE otherwise
FUNC BOOL HAVE_THELASTONE_MODELS_LOADED()
	IF HAS_MODEL_LOADED(model_TheLastOne)
	AND HAS_ANIM_DICT_LOADED(animdict_Plead)
	AND HAS_ANIM_DICT_LOADED(animdict_Leadout)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Releases The Last One model
PROC RELEASE_THELASTONE_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(model_TheLastOne)
ENDPROC

/// PURPOSE:
///    Creates The Last One
/// RETURNS:
///    TRUE if The Last One has been created, FALSE otherwise
FUNC BOOL CREATE_THELASTONE()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_TheLastOne, gTrail.nodes[1].vPedSpawn)
	ELSE
		gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_TheLastOne, gTrail.nodes[0].vPedSpawn)
	ENDIF
	
	IF IS_PED_UNINJURED(gTheLastOne.hPed)
	
		// Setup permanent attributes
		//REQUEST_PED_VISIBILITY_TRACKING(gTheLastOne.hPed)
		iSightTestID = REGISTER_PEDSIGHT_TEST(PLAYER_PED_ID(), gTheLastOne.hPed, 0)
		SET_ENTITY_LOAD_COLLISION_FLAG(gTheLastOne.hPed, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(gTheLastOne.hPed, FALSE)
		SET_PED_CAN_BE_TARGETTED(gTheLastOne.hPed, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gTheLastOne.hPed, TRUE)
		GIVE_WEAPON_TO_PED(gTheLastOne.hPed, WEAPONTYPE_UNARMED, -1, TRUE)
		SET_PED_MONEY(gTheLastOne.hPed, 0)
		
		// Make sure he has the appropriate parts here
		SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_HEAD, 1, 0, 0)
		SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_BERD, 0, 0, 0)
		SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_SPECIAL, 0, 0, 0)
		
		/*SET_PED_ALTERNATE_MOVEMENT_ANIM(gTheLastOne.hPed, AAT_IDLE, animdict_Plead, "tlo_idle_a")
		SET_PED_ALTERNATE_MOVEMENT_ANIM(gTheLastOne.hPed, AAT_WALK, animdict_Plead, "tlo_walk_normal")
		SET_PED_ALTERNATE_MOVEMENT_ANIM(gTheLastOne.hPed, AAT_RUN, animdict_Plead, "tlo_run_fast")
		SET_PED_ALTERNATE_MOVEMENT_ANIM(gTheLastOne.hPed, AAT_SPRINT, animdict_Plead, "tlo_run_fast")*/
		
		SET_PED_CONFIG_FLAG(gTheLastOne.hPed, PCF_UseKinematicModeWhenStationary, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(gTheLastOne.hPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
		
		// Stop the ped using human pain noises
		STOP_PED_SPEAKING(gTheLastOne.hPed, TRUE)
		DISABLE_PED_PAIN_AUDIO(gTheLastOne.hPed, TRUE)
		
		SET_PED_TARGET_LOSS_RESPONSE(gTheLastOne.hPed, TLR_NEVER_LOSE_TARGET)
		
		ADD_PED_FOR_DIALOGUE(pedsForConversation, 3, gTheLastOne.hPed, "ORLEANS")

		// Setup temporary attributes
		SET_PED_ENABLED(gTheLastOne.hPed, FALSE)

		// Setup data
		gTheLastOne.state		= BF_NULL
		gTheLastOne.mode		= DONE_STATE
		gTheLastOne.timer		= 0
		
		gTheLastOne.hHowlSnd = GET_SOUND_ID()
		gTheLastOne.hGrumbleSnd = GET_SOUND_ID()
		gTheLastOne.hCoughSnd = GET_SOUND_ID()
		
		gTheLastOne.bHowlTimeoutStatus = FALSE
		
		RETURN TRUE
	ENDIF
	
	SCRIPT_ASSERT("Failed to create Bigfoot")
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Releases or deletes The Last One
/// PARAMS:
///    bDelete - TRUE if The Last One should be deleted, FALSE to release him
PROC RELEASE_THELASTONE(BOOL bDelete)
	
	SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
	IF IS_PED_UNINJURED(gTheLastOne.hPed)
		SET_ENTITY_LOAD_COLLISION_FLAG(gTheLastOne.hPed, FALSE)
	ENDIF
	
	IF bDelete
		SAFE_DELETE_PED(gTheLastOne.hPed)
	ELSE
		SAFE_RELEASE_PED(gTheLastOne.hPed)
	ENDIF

	STOP_SOUND(gTheLastOne.hGrumbleSnd)
	STOP_SOUND(gTheLastOne.hCoughSnd)
	gTheLastOne.state = BF_NULL
ENDPROC

/// PURPOSE:
///    Releases or deletes the lions
/// PARAMS:
///    bDelete - TRUE if the lions should be deleted, FALSE to release them
PROC RELEASE_LIONS(BOOL bDelete)
	INT i
	REPEAT iLionCount i
		IF gLionArray[i].state <> LION_STATE_NULL
			CLEANUP_AI_PED_BLIP(gLionArray[i].hBlip)
			IF bDelete
				SAFE_DELETE_PED(gLionArray[i].hPed)
			ELSE
				SAFE_RELEASE_PED(gLionArray[i].hPed)
			ENDIF
			gLionArray[i].state = LION_STATE_NULL
			gLionArray[i].mode = SETUP_STATE
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Releases or deletes the hunter, the pickups and the props from the start of the mission
/// PARAMS:
///    bDelete - TRUE if they should be deleted, FALSE if they should be released
PROC RELEASE_START_SCENE(BOOL bDelete)
	IF gStartScene.bHasBeenCreated = TRUE
		IF bDelete
			SAFE_DELETE_PED(gStartScene.hHunter)
			SAFE_REMOVE_PICKUP(gStartScene.hGun)
			SAFE_REMOVE_PICKUP(gStartScene.hShotGun)
			SAFE_DELETE_OBJECT(gStartScene.hCrate)
		ELSE
			IF DOES_ENTITY_EXIST(gStartScene.hHunter)
				IF IS_PED_UNINJURED(gStartScene.hHunter)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gStartScene.hHunter, FALSE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(gStartScene.hHunter, FALSE)
					//FREEZE_ENTITY_POSITION(gStartScene.hHunter, FALSE)
				ENDIF
				SAFE_RELEASE_PED(gStartScene.hHunter)
			ENDIF
			SAFE_REMOVE_PICKUP(gStartScene.hGun)
			SAFE_REMOVE_PICKUP(gStartScene.hShotGun)
			SAFE_RELEASE_OBJECT(gStartScene.hCrate)
		ENDIF
		
		gStartScene.bHasBeenCreated = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialises the variables used in mission stage selection
PROC INIT_MISSION_DATA()
	g_mission.bRequestJSkip		= FALSE
	g_mission.bRequestPSkip		= FALSE
	g_mission.bRequestCpSkip	= FALSE
	g_mission.bSyncForSkip		= FALSE
	g_mission.queueNextStage	= MS_NULL

	#IF IS_DEBUG_BUILD
		zSkipMenu[0].sTxtLabel		= "Intro"
		zSkipMenu[1].sTxtLabel	= "Go to valley"
		zSkipMenu[2].sTxtLabel		= "Chase"
		zSkipMenu[3].sTxtLabel = "Kill Sasquatch"
		zSkipMenu[4].sTxtLabel		= "Outro"
		zSkipMenu[5].sTxtLabel	= "Go to valley (and create waypoints)"
		
		INT i
		REPEAT MAX_CP i
			zSkipMenu[i].bSelectable = TRUE
		ENDREPEAT
	#ENDIF
ENDPROC

/// PURPOSE:
///    Sets the next stage to move to
/// PARAMS:
///    newStage - The next stage to move to
PROC QUEUE_MISSION_STAGE(MISSION_STAGE newStage)
	g_mission.queueNextStage = newStage
ENDPROC

/// PURPOSE:
///    Sets the next stage to skip to for a checkpoint
/// PARAMS:
///    iCheckpoint - The stage to skip to
PROC QUEUE_CP_SKIP(INT iCheckpoint)
	g_mission.queueCpSkip = iCheckpoint
	g_mission.bRequestCpSkip = TRUE
ENDPROC

/// PURPOSE:
///    Skips to the end of the chase where the player has Sasquatch cornered. Used by checkpoints and debugging
PROC SKIP_TO_CHASE_END()
	RC_START_Z_SKIP()
	
	RELEASE_THELASTONE(TRUE)
	RELEASE_LIONS(TRUE)
	RELEASE_START_SCENE(TRUE)
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[HUNTER_ID])
	LOAD_THELASTONE_MODELS()
	
	WHILE NOT HAVE_THELASTONE_MODELS_LOADED()
		WAIT(0)
	ENDWHILE
	
	CREATE_THELASTONE()
	SAFE_TELEPORT_ENTITY(gTheLastOne.hPed, << -1539.3424, 4539.5303, 46.8468 >>)
	SET_PED_ENABLED(gTheLastOne.hPed, TRUE)
	gTheLastOne.state = BF_BRAWL
	gTheLastOne.mode = SETUP_STATE
	
	SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
	gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
	SET_BLIP_SCALE(gTheLastOne.hBlip, BLIP_SIZE_VEHICLE)
	
	gTrail.curNode = gTrail.size-1
	gTrail.mode = UPDATE_STATE
	g_bAudDoneCornered = TRUE
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1518.7616, 4540.3525, 44.5008>>, 95.2894)
	ENDIF
	
	//RELEASE_THELASTONE_MODELS()
	REQUEST_CUTSCENE("sas_2_rcm_t7")	// Pre-request outro so it's ready at the end

	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_END_OF_CHASE, "Kill Sasquatch", TRUE)
	
	g_mission.stage = MS_CHASE
	g_mission.section = SECTION_RUNNING
	g_mission.queueNextStage = MS_NULL
	g_mission.bSyncForSkip = FALSE
	g_bHelpDoneListen = TRUE
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	ELSE
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Change to the queued mission stage
PROC CHANGE_MISSION_STAGE()
	
	// Handle skips
	IF g_mission.bRequestJSkip OR g_mission.bRequestPSkip OR g_mission.bRequestCpSkip
		g_mission.bSyncForSkip = TRUE
	ELSE
		g_mission.bSyncForSkip = FALSE
	ENDIF

	IF g_mission.bRequestCpSkip
		SWITCH (g_mission.queueCpSkip)
			CASE CP_INTRO
				g_mission.queueNextStage = MS_INTRO
			BREAK
			
			CASE CP_GOTOVALLEY
				g_mission.queueNextStage = MS_GOTOVALLEY
				#IF IS_DEBUG_BUILD
					g_bDebugWaypoints = FALSE
				#ENDIF
			BREAK

			CASE CP_CHASE
				g_mission.queueNextStage = MS_CHASE
			BREAK

			CASE CP_OUTRO
				g_mission.queueNextStage = MS_OUTRO
			BREAK

			CASE CP_GOTOVALLEY2
				g_mission.queueNextStage = MS_GOTOVALLEY
				#IF IS_DEBUG_BUILD
					g_bDebugWaypoints = TRUE
				#ENDIF
			BREAK
			
			CASE CP_END_OF_CHASE
				g_mission.queueNextStage = MS_CHASE
			BREAK
		ENDSWITCH
	ENDIF
	
	// Change stage
	
	g_mission.stage				= g_mission.queueNextStage
	g_mission.section			= SECTION_SETUP
	g_mission.iEvent			= 0
	
	IF g_mission.bRequestCpSkip AND g_mission.queueCpSkip = CP_END_OF_CHASE
		SKIP_TO_CHASE_END()
	ENDIF

	g_mission.bRequestJSkip		= FALSE
	g_mission.bRequestPSkip		= FALSE
	g_mission.bRequestCpSkip	= FALSE

ENDPROC

//*******************************************************************************************************************
//	START
//*******************************************************************************************************************

/// PURPOSE:
///    Loads the models and anim dictionaries needed at the start of the mission
PROC LOAD_START_MODELS()
	REQUEST_MODEL(model_MainHunter)
	REQUEST_MODEL(model_HunterCar)
	REQUEST_MODEL(model_HunterGun)
	REQUEST_MODEL(model_HunterShotGun)
	REQUEST_MODEL(model_HunterCrate)
	
	REQUEST_VEHICLE_ASSET(model_HunterCar)
	
	REQUEST_ANIM_DICT(animdict_Plead)
	REQUEST_ANIM_DICT(animdict_Leadout)

ENDPROC

/// PURPOSE:
///    Checks if the models needed at the start of the mission have loaded
/// RETURNS:
///    TRUE if they have, FALSE if they haven't
FUNC BOOL HAVE_START_MODELS_LOADED()
	IF HAS_MODEL_LOADED(model_MainHunter)
	AND HAS_MODEL_LOADED(model_HunterCar)
	AND HAS_MODEL_LOADED(model_HunterGun)
	AND HAS_MODEL_LOADED(model_HunterShotGun)
	AND HAS_MODEL_LOADED(model_HunterCrate)
	AND HAS_VEHICLE_ASSET_LOADED(model_HunterCar)
	AND HAS_ANIM_DICT_LOADED(animdict_Plead)
	AND HAS_ANIM_DICT_LOADED(animdict_Leadout)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Releases the models needed at the start of the mission
PROC RELEASE_START_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(model_MainHunter)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_HunterCar)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_HunterGun)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_HunterShotGun)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_HunterCrate)
ENDPROC

/// PURPOSE:
///    Initialises the start scene ped values
/// PARAMS:
///    hPed - The ped whose values we want to initialise
///    hRelGroup - The relationship group to add this ped to
///    weapon - The weapon to give this ped
/// RETURNS:
///    TRUE if the ped is uninjured and can be initialise, FALSE if they're injured or don't exist
FUNC BOOL SETUP_START_SCENE_PED(PED_INDEX hPed, REL_GROUP_HASH hRelGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED)
	IF IS_PED_UNINJURED(hPed)
		SET_PED_RELATIONSHIP_GROUP_HASH(hPed, hRelGroup)
		SET_PED_CONFIG_FLAG(hPed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)

		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hPed, TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(hPed, TRUE)
		SET_PED_CAN_BE_TARGETTED(hPed, FALSE)
		
		IF weapon <> WEAPONTYPE_UNARMED
			GIVE_WEAPON_TO_PED(hPed, weapon, 100, TRUE)
			SET_PED_ACCURACY(hPed, 10)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates the scat model used as dressing in the intro
/// RETURNS:
///    TRUE if the set dressing was created, FALSE otherwise
FUNC BOOL ATTEMPT_CREATE_DRESSING()

	IF gDressing.bHasBeenCreated
		RETURN TRUE
	ELSE
		REQUEST_MODEL(model_DressingScat)
		
		IF HAS_MODEL_LOADED(model_DressingScat)
			gDressing.hScat = CREATE_OBJECT_NO_OFFSET(model_DressingScat, vPos_ScatSite)
			IF DOES_ENTITY_EXIST(gDressing.hScat)
				SET_ENTITY_ROTATION(gDressing.hScat, vRot_ScatSite)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_DressingScat)
				FREEZE_ENTITY_POSITION(gDressing.hScat, TRUE)
			ELSE
				RETURN FALSE
			ENDIF
			
			gDressing.bHasBeenCreated = TRUE
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CREATE_PICKUPS()
	PRINTSTRING("Paul create the pickups.") PRINTNL()
	
	INT iPlacementFlags = 0
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	IF NOT DOES_PICKUP_EXIST(gStartScene.hGun)
		//gStartScene.hGun = CREATE_PICKUP(PICKUP_WEAPON_SNIPERRIFLE, vPos_StartGunSpawn, iPlacementFlags, 100)
		gStartScene.hGun = CREATE_PICKUP_ROTATE(PICKUP_WEAPON_SNIPERRIFLE, vPos_StartGunSpawn, vRot_StartGunSpawn, iPlacementFlags, 100, EULER_XYZ)
	ENDIF
	
	IF NOT DOES_PICKUP_EXIST(gStartScene.hShotGun)
		IF vPos_StartShotGunSpawn.z <> 0.0
			//gStartScene.hShotGun = CREATE_PICKUP(PICKUP_WEAPON_PUMPSHOTGUN, vPos_StartGunSpawn, iPlacementFlags, 48)
			gStartScene.hShotGun =CREATE_PICKUP_ROTATE(PICKUP_WEAPON_PUMPSHOTGUN, vPos_StartShotGunSpawn, vRot_StartShotGunSpawn, iPlacementFlags, 48, EULER_XYZ)
		ENDIF
	ENDIF
	//<<-1297.21, 4637.65, 105.66>>, <<-100.08, -59.60, -103.04>>
	//vPos_StartShotGunSpawn = vPos_StartShotGunSpawn
	//vRot_StartShotGunSpawn = vRot_StartShotGunSpawn
	//<<-1296.76, 4637.52, 105.67>>, <<-90.00, -55.19, -141.85>>
	//vRot_StartGunSpawn = vRot_StartGunSpawn
ENDPROC

/// PURPOSE:
///    Sets up the start scene for the mission
PROC CREATE_START_SCENE(BOOL bPlacePlayer = TRUE)
	IF gStartScene.bHasBeenCreated = FALSE
		// Set player pos
		IF bPlacePlayer
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND NOT IS_REPLAY_BEING_SET_UP()
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos_StartPlayerSpawn)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fRot_StartPlayerSpawn)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
		//ENDIF
		
			// Create hunter
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HUNTER_ID])
				gStartScene.hHunter = sRCLauncherDataLocal.pedID[HUNTER_ID]
				SET_ENTITY_COORDS(gStartScene.hHunter, vPos_StartHunterSpawn)
				SET_ENTITY_HEADING(gStartScene.hHunter, fRot_StartHunterSpawn)
			ELSE
				gStartScene.hHunter = CREATE_PED(PEDTYPE_MISSION, model_MainHunter, vPos_StartHunterSpawn, fRot_StartHunterSpawn)
				SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[HUNTER_ID])
			ENDIF
			
			IF SETUP_START_SCENE_PED(gStartScene.hHunter, RELGROUPHASH_PLAYER, weapon_HunterGun)
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(gStartScene.hHunter, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
				ENDIF
			ENDIF
		ENDIF
		
		// Create pickups
		CREATE_PICKUPS()
		
		gStartScene.bHasBeenCreated = TRUE
	ENDIF
ENDPROC

//*******************************************************************************************************************
//	THELASTONE
//*******************************************************************************************************************

/// PURPOSE:
///    Starts The Last One's actions from a set node
/// PARAMS:
///    iNode - The node to start The Last One from
/// RETURNS:
///    TRUE if The Last One could be started, FALSE otherwise
FUNC BOOL START_THELASTONE_CYCLE(INT iNode)
	
	IF gTheLastOne.state = BF_NULL
		IF iNode <= 1 AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			gTheLastOne.state = BF_ESCAPE
		ELSE
			gTheLastOne.state = BF_LEADIN
		ENDIF
		gTheLastOne.mode = SETUP_STATE
		gTheLastOne.iNode = iNode
		RETURN TRUE
	ENDIF
	
	SCRIPT_ASSERT("Trying to start TheLastOne cycle, but state <> NULL. Not an issue if debug skipping, only a problem in regular game flow")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if The Last One's howl has timed out
/// RETURNS:
///    TRUE if it's timed out, FALSE otherwise
FUNC BOOL HAS_THELASTONE_HOWL_TIMED_OUT()
	RETURN gTheLastOne.bHowlTimeoutStatus
ENDFUNC

/// PURPOSE:
///    Makes Sasquatch react to being shot at
PROC SET_THELASTONE_SHOT_AT()

	IF gTheLastOne.state = BF_ESCAPE
	AND gTheLastOne.mode = UPDATE_STATE AND IS_PED_UNINJURED(gTheLastOne.hPed)
	
		STOP_SOUND(gTheLastOne.hGrumbleSnd)
		PLAY_SOUND_FROM_ENTITY(gTheLastOne.hGrumbleSnd, "WOUNDED", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
		//TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
		gTheLastOne.bIsLastOneShotAt = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if it's ok to start Sasquatch running from his node
/// PARAMS:
///    curNode - The node we're checking
/// RETURNS:
///    TRUE if it's OK to start him running, FALSE if the player  has done something to make this node unsuitable
FUNC BOOL CAN_DO_THELASTONE_SIGHTING(TRAIL_NODE_DATA& curNode)
	
	// If this is the final node, going to have to do the run whatever
	IF (gTrail.curNode = gTrail.size-1)
		RETURN TRUE
	ENDIF
	
	// Otherwise...
	
	// Player must be apporaching from behind the trigger point (otherwise he could head the last one off)
	VECTOR vStartToPlayer
	VECTOR vStartToEnd
	vStartToPlayer	= NORMALISE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()) - curNode.vPedSpawn)
	vStartToEnd		= NORMALISE_VECTOR(curNode.vPedDest - curNode.vPedSpawn)
	
	IF vStartToPlayer.x*vStartToEnd.x + vStartToPlayer.y*vStartToEnd.y < COS( TO_FLOAT(curNode.iExclusionAngle) )
		
		// No cars can be blocking the way
		VECTOR vMin
		VECTOR vMax
		
		IF curNode.vPedSpawn.x < curNode.vPedDest.x
			vMin.x = curNode.vPedSpawn.x - 4.0
			vMax.x = curNode.vPedDest.x + 4.0
		ELSE
			vMin.x = curNode.vPedDest.x - 4.0
			vMax.x = curNode.vPedSpawn.x + 4.0
		ENDIF

		IF curNode.vPedSpawn.y < curNode.vPedDest.y
			vMin.y = curNode.vPedSpawn.y - 4.0
			vMax.y = curNode.vPedDest.y + 4.0
		ELSE
			vMin.y = curNode.vPedDest.y - 4.0
			vMax.y = curNode.vPedSpawn.y + 4.0
		ENDIF

		IF curNode.vPedSpawn.z < curNode.vPedDest.z
			vMin.z = curNode.vPedSpawn.z - 4.0
			vMax.z = curNode.vPedDest.z + 4.0
		ELSE
			vMin.z = curNode.vPedDest.z - 4.0
			vMax.z = curNode.vPedSpawn.z + 4.0
		ENDIF

		RETURN NOT IS_AREA_OCCUPIED(vMin, vMax, FALSE, TRUE, FALSE, FALSE, FALSE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If player shoots the last one with any proper gun, make it a one shot kill
PROC ONE_SHOT_KILL()
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(gTheLastOne.hPed, PLAYER_PED_ID())
		/*IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(gTheLastOne.hPed, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYMELEE)
		AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(gTheLastOne.hPed, WEAPONTYPE_STUNGUN)*/
		/*IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(gTheLastOne.hPed, WEAPONTYPE_UNARMED)
		// Rubber gun is being moved to DLC, commenting out for now but will probably need to put this back in
		//AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(gTheLastOne.hPed, WEAPONTYPE_DLC_RUBBERGUN)
			SET_ENTITY_HEALTH(gTheLastOne.hPed, 99)
		ENDIF*/
		SET_ENTITY_HEALTH(gTheLastOne.hPed, 99)
		//EXPLODE_PED_HEAD(gTheLastOne.hPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the mountain lions stop attacking the player and run away
PROC MAKE_LIONS_FLEE()
	INT i
	REPEAT iLionCount i
		CPRINTLN(DEBUG_MISSION,"Checking lion")
		IF IS_PED_UNINJURED(gLionArray[i].hPed)
			CPRINTLN(DEBUG_MISSION,"Making lion flee")
			TASK_SMART_FLEE_PED(gLionArray[i].hPed, PLAYER_PED_ID(), 500, -1)
			SAFE_RELEASE_PED(gLionArray[i].hPed)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Plays dialogue for when the Last One is running
PROC PLAY_RUN_LINE()
	
	IF iRunTimer < 0
		iRunTimer = GET_GAME_TIMER() + CHASE_CONV_TIME
		iHideTimer = -1
	ELIF GET_GAME_TIMER() > iRunTimer
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_PED_STOPPED(PLAYER_PED_ID())
			IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_RUNS", CONV_PRIORITY_MEDIUM)
				iRunTimer = GET_GAME_TIMER() + CHASE_CONV_TIME
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue for when the Last One is hiding
PROC PLAY_HIDE_LINE()
	
	IF iHideTimer < 0
		iHideTimer = GET_GAME_TIMER() + CHASE_CONV_TIME
		iRunTimer = -1
	ELIF GET_GAME_TIMER() > iHideTimer
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED() AND NOT IS_PED_STOPPED(PLAYER_PED_ID())
			IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_HIDE", CONV_PRIORITY_MEDIUM)
				iHideTimer = GET_GAME_TIMER() + CHASE_CONV_TIME
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the Last One behaviour
/// PARAMS:
///    curNode - The node he's currently using
PROC UPDATE_THELASTONE_CYCLE(TRAIL_NODE_DATA& curNode)
	
	FLOAT fTriggerDist = curNode.fTriggerDist
	INT iLeadInTime = curNode.iLeadInTime
	
	// If ped is in vehicle, override trigger radius / lead in time
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		fTriggerDist = curNode.fTriggerDistCar
		
		/*IF gTrail.curNode = 1
			iLeadInTime = 0	// This is just a hack so that if the first one gets skipped cause player is in a car, then there's no delay before trail starts
		ELSE*/
			iLeadInTime = 2000
		//ENDIF
	ENDIF

	SWITCH (gTheLastOne.state)
		//------------------------------ LEADIN
		CASE BF_LEADIN

			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF gTheLastOne.mode = SETUP_STATE
					SET_PED_ENABLED(gTheLastOne.hPed, FALSE)
					IF gTheLastOne.iNode > 0
						SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
					ENDIF

					SET_ENTITY_COORDS(gTheLastOne.hPed, curNode.vPedSpawn)
					IF curNode.type = NODETYPE_SIGHTING
						SET_ENTITY_FACING(gTheLastOne.hPed, curNode.vPedDest)
					ENDIF

					gTheLastOne.timer = GET_GAME_TIMER() + iLeadInTime
					gTheLastOne.mode = UPDATE_STATE
					
				ELIF gTheLastOne.mode = UPDATE_STATE
					IF fTriggerDist = -1.0
						gTheLastOne.state = BF_NULL
						gTheLastOne.mode = SETUP_STATE
						
					ELIF GET_GAME_TIMER() > gTheLastOne.timer
						gTheLastOne.state = BF_HOWL
						gTheLastOne.mode = SETUP_STATE
	
					ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), curNode.vPedSpawn, fTriggerDist)
						gTheLastOne.state = BF_HOWL
						gTheLastOne.mode = SETUP_STATE
						
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		//------------------------------ HOWL
		CASE BF_HOWL
			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF gTheLastOne.mode = SETUP_STATE
					gTheLastOne.howlTimeout = GET_GAME_TIMER() + HOWL_TIMEOUT
					gTheLastOne.timer = 0
					gTheLastOne.mode = UPDATE_STATE

				ELIF fTriggerDist = -1.0 OR IS_ENTITY_IN_RANGE_ENTITY(gTheLastOne.hPed, PLAYER_PED_ID(), fTriggerDist)
					
					IF curNode.type = NODETYPE_SIGHTING AND fTriggerDist <> -1.0 AND CAN_DO_THELASTONE_SIGHTING(curNode)
						// Sighting is safe -> run to destination
						gTheLastOne.state = BF_ESCAPE
						gTheLastOne.mode = SETUP_STATE
					ELSE
						// Disallowed angle -> skip to next node
						gTheLastOne.state = BF_LEADOUT
						gTheLastOne.mode = SETUP_STATE
					ENDIF

				ELIF DOES_BLIP_EXIST(gTheLastOne.hBlip)	// Decay blip
					INT iAlpha
					iAlpha = GET_BLIP_ALPHA(gTheLastOne.hBlip) - 5
					IF iAlpha > 0
						SET_BLIP_ALPHA(gTheLastOne.hBlip, iAlpha)
					ELSE
						SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
					ENDIF
				
				ELIF GET_GAME_TIMER() > gTheLastOne.howlTimeout
					gTheLastOne.bHowlTimeoutStatus = TRUE

				ELIF GET_GAME_TIMER() > gTheLastOne.timer
					IF HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hCoughSnd)
						PLAY_SOUND_FROM_ENTITY(gTheLastOne.hHowlSnd, "ALERT", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")
						CPRINTLN(DEBUG_MISSION,"Triggering sonar blip at node ", gTrail.curNode)
						TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
					ENDIF

					/*gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
					IF DOES_BLIP_EXIST(gTheLastOne.hBlip)
						SET_BLIP_ALPHA(gTheLastOne.hBlip, 200)
					ENDIF*/
					gTheLastOne.timer = GET_GAME_TIMER() + 7500
				ENDIF
				
				FORCE_SONAR_BLIPS_THIS_FRAME()
				PLAY_HIDE_LINE()
			ENDIF
		BREAK
		
		//------------------------------ ESCAPE
		CASE BF_ESCAPE
			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF gTheLastOne.mode = SETUP_STATE
					SET_PED_ENABLED(gTheLastOne.hPed, TRUE)
					TASK_GO_STRAIGHT_TO_COORD(gTheLastOne.hPed, curNode.vPedDest, PEDMOVE_SPRINT, -1)//20000)
					IF HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hCoughSnd)
						PLAY_SOUND_FROM_ENTITY(gTheLastOne.hGrumbleSnd, "RUNNING", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
						//TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
					ENDIF
					gTheLastOne.bIsLastOneShotAt = FALSE
					
					IF NOT DOES_BLIP_EXIST(gTheLastOne.hBlip)
						gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
						SET_BLIP_SCALE(gTheLastOne.hBlip, BLIP_SIZE_VEHICLE)
					ELSE
						SET_BLIP_ALPHA(gTheLastOne.hBlip, 255)
					ENDIF
					
					// IF this is the last run, make the last one vulnerable
					IF gTrail.curNode = gTrail.size-1
						SET_ENTITY_CAN_BE_DAMAGED(gTheLastOne.hPed, TRUE)
						SET_PED_CAN_BE_TARGETTED(gTheLastOne.hPed, TRUE)
						SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
					ENDIF

					//gTheLastOne.timer = GET_GAME_TIMER() + 15000
					gTheLastOne.mode = UPDATE_STATE
					
				ELIF gTheLastOne.mode = UPDATE_STATE
				
					//ELIF NOT IsPedPerformingTask(gTheLastOne.hPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) AND NOT IsPedPerformingTask(gTheLastOne.hPed, SCRIPT_TASK_PLAY_ANIM)
					//	gTheLastOne.mode = DONE_STATE
						
					/*ELIF IS_ENTITY_IN_RANGE_COORDS(gTheLastOne.hPed, curNode.vPedDest, 1.5)
						gTheLastOne.mode = DONE_STATE
						
					ELIF gTrail.curNode < (gTrail.size-1) AND IS_ENTITY_IN_RANGE_COORDS(gTheLastOne.hPed, curNode.vPedDest, 3.5) AND NOT IsPedPerformingTask(gTheLastOne.hPed, SCRIPT_TASK_PLAY_ANIM)
						gTheLastOne.mode = DONE_STATE
					*/	
					IF gTrail.curNode < (gTrail.size-1) AND IS_ENTITY_IN_RANGE_COORDS(gTheLastOne.hPed, curNode.vPedDest, 4.0) AND NOT IsPedPerformingTask(gTheLastOne.hPed, SCRIPT_TASK_PLAY_ANIM)
						TASK_PLAY_ANIM(gTheLastOne.hPed, animdict_Plead, "tlo_leap_out", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_TURN_OFF_COLLISION)
						gTheLastOne.mode = DONE_STATE
						
					ELIF gTrail.curNode >= (gTrail.size-1) AND  IS_ENTITY_IN_RANGE_COORDS(gTheLastOne.hPed, curNode.vPedDest, 1.5)
						gTheLastOne.mode = DONE_STATE

					/*ELIF GET_GAME_TIMER() > gTheLastOne.timer
						gTheLastOne.mode = DONE_STATE*/
					
					ELIF HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hCoughSnd)
						IF gTheLastOne.bIsLastOneShotAt = FALSE
							PLAY_SOUND_FROM_ENTITY(gTheLastOne.hGrumbleSnd, "RUNNING", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
							//TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
						ELSE
							PLAY_SOUND_FROM_ENTITY(gTheLastOne.hGrumbleSnd, "WOUNDED", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
							//TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
						ENDIF
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viPlayer
						viPlayer = GET_PLAYERS_LAST_VEHICLE()
						IF IS_VEHICLE_OK(viPlayer) AND IS_ENTITY_TOUCHING_ENTITY(viPlayer, gTheLastOne.hPed)
							gTheLastOne.state = BF_BLOCKED_FIGHT
							gTheLastOne.mode = SETUP_STATE
						ENDIF
					ENDIF
				ENDIF
				
				PLAY_RUN_LINE()
				
				IF gTheLastOne.mode = DONE_STATE AND IS_PED_UNINJURED(gTheLastOne.hPed) AND NOT IsPedPerformingTask(gTheLastOne.hPed, SCRIPT_TASK_PLAY_ANIM)
					CLEAR_PED_TASKS(gTheLastOne.hPed)
					
					IF gTrail.curNode < gTrail.size-1
						IF HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hCoughSnd)
							PLAY_SOUND_FROM_ENTITY(gTheLastOne.hCoughSnd, "COUGH", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
							//TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
						ENDIF
						gTheLastOne.state = BF_LEADOUT
						gTheLastOne.mode = SETUP_STATE
					ELSE
						gTheLastOne.state = BF_BRAWL
						gTheLastOne.mode = SETUP_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//------------------------------ LEADOUT
		CASE BF_LEADOUT

			IF gTheLastOne.mode = SETUP_STATE
				SET_PED_ENABLED(gTheLastOne.hPed, FALSE)
				gTheLastOne.mode = UPDATE_STATE
			ENDIF
			
			PLAY_HIDE_LINE()

			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF DOES_BLIP_EXIST(gTheLastOne.hBlip)	// Decay blip
					INT iAlpha
					INT iSub
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						iSub = 16
					ELSE
						iSub = 8
					ENDIF
					iAlpha = GET_BLIP_ALPHA(gTheLastOne.hBlip) - iSub
					IF iAlpha > 0
						SET_BLIP_ALPHA(gTheLastOne.hBlip, iAlpha)
					ELSE
						SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
					ENDIF
				ENDIF
			ENDIF
				
			IF NOT DOES_BLIP_EXIST(gTheLastOne.hBlip)
				gTheLastOne.state = BF_NULL
				gTheLastOne.mode = SETUP_STATE
			ENDIF
		BREAK
				
		//------------------------------ BRAWL
		CASE BF_BRAWL
			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF gTheLastOne.mode = SETUP_STATE
					ADD_PED_FOR_DIALOGUE(pedsForConversation, 3, gTheLastOne.hPed, "ORLEANS")
					
					SET_ENTITY_CAN_BE_DAMAGED(gTheLastOne.hPed, TRUE)
					SET_PED_CAN_BE_TARGETTED(gTheLastOne.hPed, TRUE)
					SET_PED_ENABLED(gTheLastOne.hPed, TRUE)
					
					IF NOT DOES_BLIP_EXIST(gTheLastOne.hBlip)
						gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
						SET_BLIP_SCALE(gTheLastOne.hBlip, BLIP_SIZE_VEHICLE)
					ELSE
						SET_BLIP_ALPHA(gTheLastOne.hBlip, 255)
					ENDIF
					
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_END_OF_CHASE, "Kill Sasquatch", TRUE)

					//CLEAR_PED_TASKS_IMMEDIATELY(gTheLastOne.hPed)

					SEQUENCE_INDEX seq
					OPEN_SEQUENCE_TASK(seq)
						/*TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_01")
						TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_action_01")
						TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_02")
						TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_action_02")
						TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_03")
						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)*/
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)
						//TASK_PLAY_ANIM(NULL, animdict_Plead, "convict_idleshort", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						TASK_COWER(NULL)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(gTheLastOne.hPed, seq)
					
					// Stop the mountain lions attacking
					MAKE_LIONS_FLEE()
					
					//iConvFailedTimer = -1
						
					gTheLastOne.mode = UPDATE_STATE
					gTheLastOne.iBrawlingState = 0
					
//					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					
				ELIF gTheLastOne.mode = UPDATE_STATE
//					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 12, 0, 0, 255, 60)
					
					/*IF HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hGrumbleSnd) AND HAS_SOUND_FINISHED(gTheLastOne.hCoughSnd)
						PLAY_SOUND_FROM_ENTITY(gTheLastOne.hGrumbleSnd, "CORNERED", gTheLastOne.hPed, "SASQUATCH_01_SOUNDSET")	// This should be played from the last one, not from the player
						TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
					ENDIF*/

					// STATE: Play anim
					IF gTheLastOne.iBrawlingState = 0
						/*IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_LEADOUT", CONV_PRIORITY_HIGH)
						OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							gTheLastOne.iBrawlingState = 1
						ENDIF*/
						/*IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(gTheLastOne.hPed), <<fRad_BrawlOn*2,fRad_BrawlOn*2,fRad_BrawlOn*2>>))
						OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(gTheLastOne.hPed), <<fRad_BrawlOn,fRad_BrawlOn,fRad_BrawlOn>>))*/
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), gTheLastOne.hPed, 20.0)
							SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
							SET_PED_RELATIONSHIP_GROUP_HASH(gTheLastOne.hPed, RELGROUPHASH_HATES_PLAYER)
							SET_PED_CONFIG_FLAG(gTheLastOne.hPed, PCF_UseKinematicModeWhenStationary, TRUE)
							// Stop the mountain lions attacking
							MAKE_LIONS_FLEE()
							iCurrentBark = 0
							iBarkTimer = GET_GAME_TIMER()
							gTheLastOne.iBrawlingState = 1
						ENDIF
					ELIF gTheLastOne.iBrawlingState = 1
						// If Player is in arena -> flee over the edge of the cliff
						/*IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(gTheLastOne.hPed), <<fRad_BrawlOn*2,fRad_BrawlOn*2,fRad_BrawlOn*2>>))
						OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(gTheLastOne.hPed), <<fRad_BrawlOn,fRad_BrawlOn,fRad_BrawlOn>>))
							SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
							SET_PED_RELATIONSHIP_GROUP_HASH(gTheLastOne.hPed, RELGROUPHASH_HATES_PLAYER)
							SET_PED_CONFIG_FLAG(gTheLastOne.hPed, PCF_UseKinematicModeWhenStationary, TRUE)
							// Stop the mountain lions attacking
							MAKE_LIONS_FLEE()
							gTheLastOne.iBrawlingState = 2
						ENDIF*/
						IF GET_GAME_TIMER() > iBarkTimer
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_LEADOUT", sBarks[iCurrentBark], CONV_PRIORITY_MEDIUM)
								iBarkTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(3000, TIME_BETWEEN_BARKS)
								iCurrentBark = GET_RANDOM_INT_IN_RANGE(0, NUM_BARKS)
								/*iCurrentBark++
								IF iCurrentBark >= NUM_BARKS
									iCurrentBark = 0
								ENDIF*/
							ENDIF
						ENDIF
					/*ELIF gTheLastOne.iBrawlingState = 2
						// if the player gets close enough to start brawling, The Last One kicks the shit out of them
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), gTheLastOne.hPed, 3.5) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
							TASK_COMBAT_PED(gTheLastOne.hPed, PLAYER_PED_ID())
							//SET_AI_MELEE_WEAPON_DAMAGE_MODIFIER(500.0)
							//SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), 0.11)
							//SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 0.11)
							gTheLastOne.iBrawlingState = 3
						ENDIF*/
					ENDIF
					
					ONE_SHOT_KILL()
				ENDIF
						
			ENDIF
		BREAK
		
		CASE BF_BLOCKED_FIGHT
			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				IF gTheLastOne.mode = SETUP_STATE
					
					SET_ENTITY_CAN_BE_DAMAGED(gTheLastOne.hPed, TRUE)
					SET_PED_CAN_BE_TARGETTED(gTheLastOne.hPed, TRUE)
					SET_ENTITY_HEALTH(gTheLastOne.hPed, 120)
					SET_PED_ENABLED(gTheLastOne.hPed, TRUE)
					
					IF NOT DOES_BLIP_EXIST(gTheLastOne.hBlip)
						gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
						SET_BLIP_SCALE(gTheLastOne.hBlip, BLIP_SIZE_VEHICLE)
					ELSE
						SET_BLIP_ALPHA(gTheLastOne.hBlip, 255)
					ENDIF
					
					bBlockedByCar = TRUE
					
					TASK_COMBAT_PED(gTheLastOne.hPed, PLAYER_PED_ID())
					
					// Stop the mountain lions attacking
					MAKE_LIONS_FLEE()
						
					gTheLastOne.mode = UPDATE_STATE
				ELIF gTheLastOne.mode = UPDATE_STATE
					// in combat
				ENDIF
			ENDIF
		BREAK
		
		//------------------------------ NULL
		CASE BF_NULL
			IF gTheLastOne.mode = SETUP_STATE
				SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
				gTheLastOne.mode = DONE_STATE
			ENDIF
		BREAK
				
	ENDSWITCH
	
ENDPROC

//*******************************************************************************************************************
//	LION
//*******************************************************************************************************************

/// PURPOSE:
///    Loads the mountain lion model
PROC LOAD_LION_MODELS()
	REQUEST_MODEL(model_Lion)
ENDPROC

/// PURPOSE:
///    Checks if the mountain lion model has loaded
/// RETURNS:
///    TRUE if it's loaded, FALSE otherwise
FUNC BOOL HAVE_LION_MODELS_LOADED()
	RETURN HAS_MODEL_LOADED(model_Lion)
ENDFUNC

/// PURPOSE:
///    Releases the mountain lion model
PROC RELEASE_LION_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(model_Lion)
ENDPROC

/// PURPOSE:
///    Creates the mountain lions
PROC CREATE_LIONS()

	INT i
	REPEAT iLionCount i
		gLionArray[i].hPed = CREATE_PED(PEDTYPE_SPECIAL, model_Lion, gLionArray[i].vSpawnPos, gLionArray[i].fSpawnRot)
	
		IF IS_PED_UNINJURED(gLionArray[i].hPed)

			// Setup permenant attributes
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(gLionArray[i].hPed, TRUE)

			SET_PED_TARGET_LOSS_RESPONSE(gLionArray[i].hPed, TLR_NEVER_LOSE_TARGET)
	
			TASK_WANDER_IN_AREA(gLionArray[i].hPed, gLionArray[i].vSpawnPos, 15.0)
			
			SET_PED_COMBAT_ATTRIBUTES(gLionArray[i].hPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
			
			GIVE_DELAYED_WEAPON_TO_PED(gLionArray[i].hPed, WEAPONTYPE_COUGAR, -1, TRUE)

			// Setup data
			gLionArray[i].state		= LION_STATE_READY
			gLionArray[i].mode		= SETUP_STATE
		
		ELSE
			SCRIPT_ASSERT("Failed to create lion")
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Updates the mountain lion behaviour
/// PARAMS:
///    lion - The lion whose behaviour we're updating
PROC UPDATE_LION(LION_DATA& lion)
	UPDATE_AI_PED_BLIP(lion.hPed, lion.hBlip)
	SWITCH (lion.state)
		
		CASE LION_STATE_READY
			IF NOT IS_PED_UNINJURED(lion.hPed)
				lion.state = LION_STATE_NULL
				lion.mode = SETUP_STATE
			ELSE
				IF IS_ENTITY_IN_RANGE_ENTITY(lion.hPed, PLAYER_PED_ID(), lion.fTriggerRadius)
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(lion.hPed, PLAYER_PED_ID())
						lion.state = LION_STATE_POUNCE
						lion.mode = SETUP_STATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE LION_STATE_POUNCE
			IF NOT IS_PED_UNINJURED(lion.hPed)
				lion.state = LION_STATE_NULL
				lion.mode = SETUP_STATE
			
			ELIF lion.mode = SETUP_STATE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_SMART_FLEE_PED(lion.hPed, PLAYER_PED_ID(), 500, -1)
				ELSE
					TASK_COMBAT_PED(lion.hPed, PLAYER_PED_ID())
				ENDIF
				lion.mode = UPDATE_STATE
			ENDIF
		BREAK
		
		CASE LION_STATE_NULL
			IF lion.mode = SETUP_STATE
				SAFE_RELEASE_PED(lion.hPed)
				lion.mode = UPDATE_STATE
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

FUNC BOOL IS_PED_ANIMAL(PED_INDEX ped)
	MODEL_NAMES mnPedModel = GET_ENTITY_MODEL(ped)
	IF mnPedModel = A_C_BOAR
	OR mnPedModel = A_C_CHICKENHAWK
	OR mnPedModel = A_C_CHIMP
	OR mnPedModel = A_C_CHOP
	OR mnPedModel = A_C_CORMORANT
	OR mnPedModel = A_C_COW
	OR mnPedModel = A_C_COYOTE
	OR mnPedModel = A_C_CROW
	OR mnPedModel = A_C_DEER
	OR mnPedModel = A_C_HEN
	//OR mnPedModel = A_C_HORSE
	OR mnPedModel = A_C_MTLION
	OR mnPedModel = A_C_PIG
	OR mnPedModel = A_C_PIGEON
	OR mnPedModel = A_C_RAT
	OR mnPedModel = A_C_RETRIEVER
	OR mnPedModel = A_C_RHESUS
	OR mnPedModel = A_C_ROTTWEILER
	OR mnPedModel = A_C_SEAGULL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for the player killing any wild animals
PROC CHECK_FOR_DEAD_ANIMALS()

    EVENT_NAMES eEventType
	STRUCT_ENTITY_ID sEntityID
	INT iCount = 0
	PED_INDEX ped
	
	// to stop things crying about unreference variable
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
		eEventType = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
        SWITCH (eEventType)
            CASE EVENT_ENTITY_DAMAGED
			
				// Grab the event data and process also kill the ped on injury
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sEntityID, SIZE_OF(STRUCT_ENTITY_ID))
				
				IF DOES_ENTITY_EXIST(sEntityID.EntityId)
				
					IF IS_ENTITY_A_PED(sEntityID.EntityId)
						ped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.EntityId)
						IF IS_PED_ANIMAL(ped)
							IF IS_ENTITY_DEAD(ped)
								INFORM_MISSION_STATS_OF_INCREMENT(TLO_AMBIENT_ANIMAL_KILLS)
							ELIF IS_PED_INJURED(ped) 
								SET_ENTITY_HEALTH(ped, 0) // insta kill the ped
								INFORM_MISSION_STATS_OF_INCREMENT(TLO_AMBIENT_ANIMAL_KILLS)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks for the player shooting the last one
PROC CHECK_FOR_ORLEANS_INJURY()
			
	IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(gTheLastOne.hPed), 1.5)
		IF gTrail.curNode < gTrail.size-1
			SET_THELASTONE_SHOT_AT()
			
			// increase the shot at count for the mission stats
			INFORM_MISSION_STATS_OF_INCREMENT(TLO_WOUNDS)
			
			//IF g_bAudDoneWounded = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
					/*IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_CHASE2", CONV_PRIORITY_MEDIUM)	// Conversation: "Wounded him, that'll slow him down"
						g_bAudDoneWounded = TRUE
					ENDIF*/
					CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_CHASE2", CONV_PRIORITY_MEDIUM)	// Conversation: "Wounded him, that'll slow him down"
				ENDIF
			//ENDIF
		ENDIF
	
	ELIF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(gTheLastOne.hPed), 5.0)
		IF gTrail.curNode < gTrail.size-1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
				CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_MISS", CONV_PRIORITY_MEDIUM)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//*******************************************************************************************************************
//	TRAIL
//*******************************************************************************************************************

/// PURPOSE:
///    Sets the Last One chase to its beginning
PROC START_TRAIL()
	gTrail.mode = SETUP_STATE
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		gTrail.curNode = 1
	ELSE
		gTrail.curNode = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the Last One chase
PROC UPDATE_TRAIL()
	
	// Display current node
//	DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBR", gTrail.curNode)

	// Process nodes (the last one)
	SWITCH (gTrail.nodes[gTrail.curNode].type)

		CASE NODETYPE_HOWL
			FALLTHRU
		CASE NODETYPE_SIGHTING
			IF gTrail.mode = SETUP_STATE
				START_THELASTONE_CYCLE(gTrail.curNode)
				gTrail.mode = UPDATE_STATE
			
			ELIF gTrail.mode = UPDATE_STATE
				UPDATE_THELASTONE_CYCLE(gTrail.nodes[gTheLastOne.iNode])

				IF gTheLastOne.state = BF_NULL AND gTheLastOne.mode = DONE_STATE
					gTrail.mode = DONE_STATE
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH
	
	// Process lions
	INT i
	REPEAT iLionCount i
		UPDATE_LION(gLionArray[i])
	ENDREPEAT

ENDPROC

/*PROC PLAY_IN_VEHICLE_LINE()
	IF iCurrentVehicleChaseLines < NUM_VEHICLE_CHASE_LINES AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_GAME_TIMER() > iVehicleChaseLineTimer
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_VEHICLE", sVehicleChaseLines[iCurrentVehicleChaseLines], CONV_PRIORITY_MEDIUM)
					iCurrentVehicleChaseLines++
					iVehicleChaseLineTimer = GET_GAME_TIMER() + VEHICLE_CHASE_LINE_TIME
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC*/

/// PURPOSE:
///    Clears vehicles and peds from the chase area
PROC CLEAR_ROUTE_AREAS()
	CLEAR_AREA(vClearZone0, 50, TRUE)
	CLEAR_AREA(vClearZone1, 50, TRUE)
	CLEAR_AREA(vClearZone2, 50, TRUE)
	CLEAR_AREA(vClearZone3, 20, TRUE)
						
	VEHICLE_INDEX hPlayerCar = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(hPlayerCar) AND NOT IS_ENTITY_DEAD(hPlayerCar)
		IF IS_ENTITY_IN_RANGE_COORDS(hPlayerCar, vClearZone0, 50)
		OR IS_ENTITY_IN_RANGE_COORDS(hPlayerCar, vClearZone1, 50)
		OR IS_ENTITY_IN_RANGE_COORDS(hPlayerCar, vClearZone2, 50)
		OR IS_ENTITY_IN_RANGE_COORDS(hPlayerCar, vClearZone3, 20)
			SET_ENTITY_COORDS(hPlayerCar, vPos_RespotLastCar)
			SET_ENTITY_HEADING(hPlayerCar, fRot_RespotLastCar)
			SET_VEHICLE_ON_GROUND_PROPERLY(hPlayerCar)
		ENDIF
	ENDIF

ENDPROC

//*******************************************************************************************************************
//	MAIN BODY STAGES
//*******************************************************************************************************************

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Stops dialogue and clears objectives
PROC Script_TextCleanup()
	STOP_SCRIPTED_CONVERSATION(FALSE)
	CLEAR_PRINTS()
ENDPROC

/// PURPOSE:
///    Cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION,"...Random Character Script was triggered so additional cleanup required")
		RELEASE_THELASTONE_MODELS()
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)

	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF
	
	RESET_AI_MELEE_WEAPON_DAMAGE_MODIFIER()
	RESET_AI_WEAPON_DAMAGE_MODIFIER()
	SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), 1.0)
	SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), 1.0)
	
	IF IS_AUDIO_SCENE_ACTIVE("SASQUATCH_MIX")
		STOP_AUDIO_SCENE("SASQUATCH_MIX")
	ENDIF
		
	SET_PED_MODEL_IS_SUPPRESSED(model_Lion, FALSE)

	RELEASE_THELASTONE(FALSE)
	RELEASE_LIONS(FALSE)
	RELEASE_START_SCENE(FALSE)
	SAFE_RELEASE_VEHICLE(viMissionFinishedVehicle)
	
	//DELETE_WAYPOINTS()
	
	SET_PED_PATHS_BACK_TO_ORIGINAL(vIntroAreaMin, vIntroAreaMax)
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER",TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",TRUE)

	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()

ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Passes the mission
PROC Script_Passed()
	IF bToScatOnFoot
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(TLO_TO_SITE_ON_FOOT)
	ENDIF

	Script_TextCleanup()
	Random_Character_Passed(CP_RAND_C_SAS1)
	Script_Cleanup()
	
ENDPROC

// ===========================================================================================================
//		STAGES
// ===========================================================================================================
/// PURPOSE:
///    Loads the audio and text used by the mission, creates the set dressing for the intro. Skips to the appropriate stage on a replay
PROC STAGE_Init()
	IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SAS_BANK_01") AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\SAS_BANK_02") AND REQUEST_SCRIPT_AUDIO_BANK("SAS_BANK_03") AND LOAD_TEXT() AND ATTEMPT_CREATE_DRESSING()
		CPRINTLN(DEBUG_MISSION,"STAGE_Init")
		
		// Disable the ambient audio zone
		IF IS_AMBIENT_ZONE_ENABLED("AZ_DISTANT_SASQUATCH")
			SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_DISTANT_SASQUATCH", FALSE, TRUE)
		ENDIF
		
		SET_PED_MODEL_IS_SUPPRESSED(model_Lion, TRUE)
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		
		IF Is_Replay_In_Progress()
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				IF iReplayStage = CP_INTRO
					iReplayStage = CP_CHASE
				ELSE
					iReplayStage++
				ENDIF
				bToScatOnFoot = FALSE
			ENDIF
			
			AMMO_TYPE eAmmoType
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
				eAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN)
				IF GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType) < 8
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 8, TRUE)
				ENDIF
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 8, TRUE)
			ENDIF
			
			SWITCH iReplayStage
				CASE CP_INTRO
					START_REPLAY_SETUP(vPos_StartPlayerSpawn, fRot_StartPlayerSpawn)
					QUEUE_CP_SKIP(CP_GOTOVALLEY)
					CHANGE_MISSION_STAGE()
				BREAK
				
				CASE CP_CHASE
					START_REPLAY_SETUP(vPos_ChasePlayerStart, fRot_ChasePlayerStart)
					QUEUE_CP_SKIP(CP_CHASE)
					CHANGE_MISSION_STAGE()
				BREAK
				
				CASE CP_END_OF_CHASE
					START_REPLAY_SETUP(<<-1518.7616, 4540.3525, 44.5008>>, 95.2894)
					QUEUE_CP_SKIP(CP_END_OF_CHASE)
					CHANGE_MISSION_STAGE()
				BREAK
				
				CASE CP_OUTRO
					//START_REPLAY_SETUP(<<-1518.7616, 4540.3525, 44.5008>>, 95.2894)
					QUEUE_CP_SKIP(CP_OUTRO)
					CHANGE_MISSION_STAGE()
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
				BREAK
			ENDSWITCH
		ELSE
			IF IS_REPEAT_PLAY_ACTIVE()
				//REQUEST_CUTSCENE("sas_1_rcm")
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("sas_1_rcm_concat", CS_SECTION_1 | CS_SECTION_3)
				WHILE NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					WAIT(0)
				ENDWHILE
			ENDIF
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			REQUEST_ANIM_DICT(animdict_Plead)
			REQUEST_ANIM_DICT(animdict_Leadout)
			QUEUE_MISSION_STAGE(MS_INTRO)
			CHANGE_MISSION_STAGE()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the intro
PROC STAGE_Intro()

//	Mocap scene
// -------------
	IF g_mission.bSyncForSkip = TRUE
		CPRINTLN(DEBUG_MISSION,"Skipping to STAGE_Intro")
		RC_START_Z_SKIP()

		RELEASE_THELASTONE(TRUE)
		RELEASE_LIONS(TRUE)
		RELEASE_START_SCENE(TRUE)
		RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE, TRUE)

		// Re-create the initial scene
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_TheLastOne(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE

		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			REMOVE_CUTSCENE()
		ENDIF
		
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RC_END_Z_SKIP()
		g_mission.bSyncForSkip = FALSE
	ENDIF

	IF g_mission.section = SECTION_SETUP
		//RC_REQUEST_CUTSCENE("sas_1_rcm")
		RC_PRE_REQUEST_CUTSCENE(FALSE)
		FLOAT fDistPlayerSectionOne
		fDistPlayerSectionOne = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1295.843628,4641.988281,105.332458>>)
		FLOAT fDistPlayerSectionTwo
		fDistPlayerSectionTwo = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1301.435303,4641.213867,105.624947>>)
		FLOAT fDifference
		fDifference = fDistPlayerSectionOne - fDistPlayerSectionTwo
		IF fDifference < 0
			fDifference *= -1
		ENDIF
		// Don't load new cutscenes if the player is in the middle of the two locates
		//IF fDifference > 0.5//1
			IF fDistPlayerSectionOne < fDistPlayerSectionTwo
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("sas_1_rcm_concat", CS_SECTION_1 | CS_SECTION_3)
			ELSE
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("sas_1_rcm_concat", CS_SECTION_2 | CS_SECTION_3)
			ENDIF
		//ENDIF
		IF RC_IS_CUTSCENE_OK_TO_START()
			CREATE_PICKUPS()
			/*IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HUNTER_ID])
				SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[HUNTER_ID], << -1303.1628, 4648.0850, 104.9664 >>)
				SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[HUNTER_ID])
				ACTIVATE_PHYSICS(sRCLauncherDataLocal.vehID[HUNTER_ID])
				SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[HUNTER_ID], 293.1504)
			ENDIF*/
			
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			// Insert hunter ped into cutscene
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HUNTER_ID])
                REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[HUNTER_ID], "Hunter", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				oiRifle = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(sRCLauncherDataLocal.pedID[HUNTER_ID])
				IF DOES_ENTITY_EXIST(oiRifle)
					REGISTER_ENTITY_FOR_CUTSCENE(oiRifle, "SAS_SniperRifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
			ENDIF
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
			
			// stop peds walking through the intro
			SET_PED_PATHS_IN_AREA(vIntroAreaMin, vIntroAreaMax, FALSE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER",FALSE)
			SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER",FALSE)
			
			RC_CLEANUP_LAUNCHER()
			START_CUTSCENE()
			WAIT(0)
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TheLastOne")
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaPos1, vIntroAreaPos2, fIntroAreaWidth, <<-1310.38, 4642.96, 107.35>>, 313.34, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
			RC_START_CUTSCENE_MODE(<< -1298.98, 4640.16, 105.67>>)
			CLEAR_AREA(<< -1561.793213, 4668.805664, 48.043373 >>, 218.907410, TRUE)

			LOAD_START_MODELS()
			CPRINTLN(DEBUG_MISSION,"Setup STAGE_Intro")
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			g_mission.section = SECTION_RUNNING
		ENDIF
	ENDIF

	IF g_mission.section = SECTION_RUNNING
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				/*SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos_StartPlayerSpawn)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fRot_StartPlayerSpawn)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-11.3366)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(6.5818)*/
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HUNTER_ID])
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Hunter")
				gStartScene.hHunter = sRCLauncherDataLocal.pedID[HUNTER_ID]
				/*SET_ENTITY_COORDS(gStartScene.hHunter, vPos_StartHunterSpawn)
				SET_ENTITY_HEADING(gStartScene.hHunter, fRot_StartHunterSpawn)*/
				
				IF SETUP_START_SCENE_PED(gStartScene.hHunter, RELGROUPHASH_PLAYER, weapon_HunterGun)
					//TASK_LOOK_AT_ENTITY(gStartScene.hHunter, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
					IF HAS_ANIM_DICT_LOADED(animdict_Plead)
						/*SEQUENCE_INDEX siHunter
						OPEN_SEQUENCE_TASK(siHunter)
							TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_leadout", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_action", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
							TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_look", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
							//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						CLOSE_SEQUENCE_TASK(siHunter)
						TASK_PERFORM_SEQUENCE(gStartScene.hHunter, siHunter)
						CLEAR_SEQUENCE_TASK(siHunter)*/
						TASK_PLAY_ANIM(gStartScene.hHunter, animdict_Plead, "hunter_leadout", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, eHunterAnimFlags)
						//FREEZE_ENTITY_POSITION(gStartScene.hHunter, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
		ENDIF
		
		IF g_mission.bRequestJSkip
			STOP_CUTSCENE()
			QUEUE_MISSION_STAGE(MS_GOTOVALLEY)
			g_mission.section = SECTION_CLEANUP

		ELIF g_mission.bRequestCpSkip
			STOP_CUTSCENE()
			g_mission.section = SECTION_CLEANUP

		ELIF HAS_CUTSCENE_FINISHED()
			LOAD_START_MODELS()
			IF HAVE_START_MODELS_LOADED()
				g_mission.section = SECTION_CLEANUP
			ENDIF
		ENDIF
	ENDIF
	
	IF g_mission.section = SECTION_CLEANUP
		IF HAS_CUTSCENE_FINISHED()
			REPLAY_STOP_EVENT()
			RC_END_CUTSCENE_MODE(true, TRUE, TRUE, FALSE)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			SAFE_DELETE_OBJECT(oiRifle)
			/*IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HUNTER_ID])
				gStartScene.hHunter = sRCLauncherDataLocal.pedID[HUNTER_ID]
				SET_ENTITY_COORDS(gStartScene.hHunter, vPos_StartHunterSpawn)
				//SET_ENTITY_HEADING(gStartScene.hHunter, fRot_StartHunterSpawn)
				
				IF SETUP_START_SCENE_PED(gStartScene.hHunter, RELGROUPHASH_PLAYER, weapon_HunterGun)
					TASK_LOOK_AT_ENTITY(gStartScene.hHunter, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
				ENDIF
			ENDIF*/
			//SET_PED_PATHS_BACK_TO_ORIGINAL(vIntroAreaMin, vIntroAreaMax)
			iBlockLookBehind = GET_GAME_TIMER() + 500
			CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_Intro")
			QUEUE_MISSION_STAGE(MS_GOTOVALLEY)
			CHANGE_MISSION_STAGE()
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_HUNTER_ANIMS()
	IF g_bStartAnimSequence AND IS_PED_UNINJURED(gStartScene.hHunter)
		IF IS_ENTITY_PLAYING_ANIM(gStartScene.hHunter, animdict_Plead, "hunter_leadout")
			IF GET_ENTITY_ANIM_CURRENT_TIME(gStartScene.hHunter,animdict_Plead,"hunter_leadout") > 0.9
				IF HAS_ANIM_DICT_LOADED(animdict_Plead)
					SEQUENCE_INDEX siHunter
					OPEN_SEQUENCE_TASK(siHunter)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_action", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_look", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_action", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						TASK_PLAY_ANIM(NULL, animdict_Plead, "hunter_idle_look", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eHunterAnimFlags)
						SET_SEQUENCE_TO_REPEAT(siHunter, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(siHunter)
					TASK_PERFORM_SEQUENCE(gStartScene.hHunter, siHunter)
					CLEAR_SEQUENCE_TASK(siHunter)
					g_bStartAnimSequence = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays a comversation if the appropriate timer has expired
/// PARAMS:
///    iConvTimer - The timer to check
///    iTimeIncrease - The amount of time to increase the timer by if the conversation plays
///    sDialogue - The conversation to play
PROC PlayHunterDialogue(INT & iConvTimer, INT iTimeIncrease, STRING sDialogue)
	IF GET_GAME_TIMER() > iConvTimer
		ADD_PED_FOR_DIALOGUE(pedsForConversation, 4, gStartScene.hHunter, "HUNTER")
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", sDialogue, CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				iConvTimer = GET_GAME_TIMER() + iTimeIncrease
			ENDIF
		ELSE
			IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", sDialogue, CONV_PRIORITY_MEDIUM)
				iConvTimer = GET_GAME_TIMER() + iTimeIncrease
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls dialogue from the hunter if the player aims at him, bumps into him, shoots nearby or hangs around
///    Only play lines if the player is within 50m of the hunter
PROC CONTROL_HUNTER_CONVS()
	IF IS_PED_UNINJURED(gStartScene.hHunter) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), gStartScene.hHunter, 50)
			// Dialogue for the player aiming at the hunter
			IF IS_PLAYER_VISIBLY_TARGETTING_PED(gStartScene.hHunter)
				PlayHunterDialogue(iTargetTimer, TARGET_TIME, "SAS1_TARGET")
			ENDIF
			
			// Dialogue for the player shooting in the same area as the hunter
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				PlayHunterDialogue(iShootTimer, SHOOT_TIME, "SAS1_GUN")
			ENDIF
			
			// Dialogue for the player bumping into the hunter
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), gStartScene.hHunter)
				PlayHunterDialogue(iBumpTimer, BUMP_TIME, "SAS1_BUMP")
			ENDIF
			
			// Dialogue for the player hanging around
			PlayHunterDialogue(iReturnTimer, RETURN_TIME, "SAS1_RETURN")
		ELSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player drives to the scat site and hears the Last One
PROC STAGE_GoToValley()
	
	IF GET_GAME_TIMER() > iBlockLookBehind AND iBlockLookBehind > 0
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			iBlockLookBehind = -1
		ENDIF
	ENDIF
	
	IF g_mission.bSyncForSkip = TRUE
		RC_START_Z_SKIP()
		CPRINTLN(DEBUG_MISSION,"Skipping to STAGE_GoToValley")
		//-- Sync for skip
		RELEASE_THELASTONE(TRUE)
		RELEASE_LIONS(TRUE)
		RELEASE_START_SCENE(TRUE)
		RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE, TRUE)
		
		// Re-create the initial scene
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_TheLastOne(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		
		LOAD_START_MODELS()
		WHILE NOT HAVE_START_MODELS_LOADED()
			WAIT(0)
		ENDWHILE
		CREATE_START_SCENE()
		
		WHILE NOT LOAD_TEXT()
			WAIT(0)
		ENDWHILE

		WHILE NOT ATTEMPT_CREATE_DRESSING()
			WAIT(0)
		ENDWHILE
		
		g_bStartAnimSequence = TRUE
		
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
		ELSE
			WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		RC_END_Z_SKIP()
		g_mission.bSyncForSkip = FALSE
	ENDIF

	IF g_mission.section = SECTION_SETUP
		// Create start scene
		LOAD_START_MODELS()
		IF HAVE_START_MODELS_LOADED()
			//CREATE_START_SCENE(FALSE)
			RELEASE_START_MODELS()
			
			IF IS_PED_UNINJURED(gStartScene.hHunter)
				SET_PED_CONFIG_FLAG(gStartScene.hHunter, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(gStartScene.hHunter, FALSE)
			ENDIF
			
			g_hBlip = ADD_BLIP_FOR_RADIUS(vPos_Valley, 75.0)
			SET_BLIP_COLOUR(g_hBlip, BLIP_COLOUR_YELLOW)
			SET_BLIP_ALPHA(g_hBlip , 128)
			SHOW_HEIGHT_ON_BLIP(g_hBlip, FALSE)
				
			/*LOAD_THELASTONE_MODELS()
			LOAD_LION_MODELS()*/
			
			CLEAR_AREA(vClearZone0, 100, TRUE)
			CLEAR_AREA(vClearZone1, 100, TRUE)
			CLEAR_AREA(vClearZone2, 100, TRUE)
			CLEAR_AREA(vClearZone3, 100, TRUE)
		
			g_bObjDoneGotoValley	= FALSE
			g_bObjDoneLeavingValley	= FALSE
			//g_bHintGiven			= FALSE
			g_bObjDoneInvestigate	= FALSE
			
			g_bAudDoneStartGoValley	= FALSE
			g_bAudDoneStartGetGun	= FALSE
			g_bAudDoneHowl			= FALSE
			g_bAudDoneHeardHowl		= FALSE
			
			//g_bPrintCarObj			= TRUE

			g_bDestroyCarConvReady	= TRUE
			
			g_bStartAnimSequence = TRUE
			
			bLoadChaseModels = TRUE
			
			iReturnTimer = GET_GAME_TIMER() + RETURN_TIME
			
			START_AUDIO_SCENE("SASQUATCH_MIX")
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
			CPRINTLN(DEBUG_MISSION,"Setup STAGE_GoToValley")
			g_mission.section = SECTION_RUNNING
		ENDIF
	ENDIF
	
	IF g_mission.section = SECTION_RUNNING
		
		CHECK_FOR_DEAD_ANIMALS()
		UPDATE_PEDSIGHT_TESTS()
		TRIGGER_HUNTER_ANIMS()
	
		//--- Check state fail conditions + stage skips
		IF NOT IS_PED_UNINJURED(PLAYER_PED_ID())
			QUEUE_MISSION_STAGE(MS_FAILED)
			g_mission.failReason = FAIL_REASON_PLAYER_DEAD
			g_mission.section = SECTION_CLEANUP
		
		ELIF IS_PED_HARMED(gStartScene.hHunter)
			QUEUE_MISSION_STAGE(MS_FAILED)
			PRINT_NOW("Hunnter dead", 7000, 1)
			g_mission.failReason = FAIL_REASON_HUNTER_DEAD
			g_mission.section = SECTION_CLEANUP
		
		// Only fail if the player is actually attacking the hunter, have dialogue for aiming and shooting in the general area
		ELIF HAS_PLAYER_THREATENED_PED(gStartScene.hHunter, FALSE, 70, 170, TRUE)
		OR NOT IS_ENTITY_IN_RANGE_COORDS(gStartScene.hHunter, vPos_CheckHunterPushed, 1.5)
		OR IS_PED_RAGDOLL(gStartScene.hHunter)
			SET_PED_RELATIONSHIP_GROUP_HASH(gStartScene.hHunter, RELGROUPHASH_HATES_PLAYER)
			TASK_COMBAT_PED(gStartScene.hHunter, PLAYER_PED_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(pedsForConversation, "SAS1AUD", "SAS1_START3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)

			QUEUE_MISSION_STAGE(MS_FAILED)
			g_mission.failReason = FAIL_REASON_HUNTER_THREATENED
			g_mission.section = SECTION_CLEANUP
		
		/*ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ChasePlayerStart, <<6.0, 6.0, 6.0>>)
			QUEUE_MISSION_STAGE(MS_CHASE)
			g_mission.section = SECTION_CLEANUP*/
		
		ELIF g_mission.bRequestCpSkip
			g_mission.section = SECTION_CLEANUP
			
		ELIF g_mission.bRequestJSkip
			QUEUE_MISSION_STAGE(MS_CHASE)
			g_mission.section = SECTION_CLEANUP
			
		ELIF g_mission.bRequestPSkip
			QUEUE_MISSION_STAGE(MS_INTRO)
			g_mission.section = SECTION_CLEANUP
			
		ELSE
			// Update hunter to face player
			/*VECTOR vPedToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(gStartScene.hHunter)
			VECTOR vPedForward = GET_ENTITY_FORWARD_VECTOR(gStartScene.hHunter)
			IF GET_SCRIPT_TASK_STATUS(gStartScene.hHunter, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
			AND ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vPedToPlayer.x, vPedToPlayer.y, vPedForward.x, vPedForward.y)) > 65
				TASK_TURN_PED_TO_FACE_ENTITY(gStartScene.hHunter, PLAYER_PED_ID(), 1500)
			ENDIF*/

			// Check if player is in valley
			BOOL bIsPlayerInValley = IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
			IF bLoadChaseModels AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPos_ScatSite, 250)
				LOAD_THELASTONE_MODELS()
				LOAD_LION_MODELS()
				bLoadChaseModels = FALSE
			ENDIF
			
			// Check if the player is in a vehicle for the mission stat tracking
			IF bToScatOnFoot
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					bToScatOnFoot = FALSE
				ENDIF
			ENDIF
			
			CONTROL_HUNTER_CONVS()
			
			/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ScatSite, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
				// need this here to avoid the locate chevron flashing off for a frame
			ENDIF*/
			
			IF g_bAudDoneStartGoValley = FALSE
				/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ScatSite, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
					// need this here to avoid the locate chevron flashing off for a frame
				ENDIF*/
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(pedsForConversation, 4, gStartScene.hHunter, "HUNTER")
					//IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_START1", CONV_PRIORITY_MEDIUM)	// Conversation: "Scat sites down in the valley"
					IF CREATE_MULTIPART_CONVERSATION_WITH_3_LINES(pedsForConversation, "SAS1AUD", "SAS1_LO", "SAS1_LO_1", "SAS1_START1", "SAS1_START1_1", "SAS1_START1", "SAS1_START1_2", CONV_PRIORITY_MEDIUM)
						g_bAudDoneStartGoValley = TRUE
					ENDIF
				ENDIF

			ELIF g_bAudDoneStartGetGun = FALSE
				/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ScatSite, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
					// need this here to avoid the locate chevron flashing off for a frame
				ENDIF*/
				CheckHunterCar()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					ADD_PED_FOR_DIALOGUE(pedsForConversation, 4, gStartScene.hHunter, "HUNTER")
					
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), gStartScene.hHunter, <<25,25,25>>)
						IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE)
						OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE)
						OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE)
						OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SPECIALCARBINE)
						OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
							IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_START2", CONV_PRIORITY_MEDIUM)	// Conversation: "If I hear them terrible howls I know you found him..."
								g_bAudDoneStartGetGun = TRUE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_START1B", CONV_PRIORITY_MEDIUM)	// Conversation: "Better take a rifle, city boy"
								g_bAudDoneStartGetGun = TRUE
							ENDIF
						ENDIF
					ELSE
						g_bAudDoneStartGetGun = TRUE
					ENDIF
				ENDIF

			ELIF g_bObjDoneGotoValley = FALSE
				/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ScatSite, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
					// need this here to avoid the locate chevron flashing off for a frame
				ENDIF*/
				CheckHunterCar()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), gStartScene.hHunter) > 25.0
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ELSE
					PRINT_NOW("SAS1_B1", DEFAULT_GOD_TEXT_TIME, 0)	// Objective: Investigate the scat site
					g_bObjDoneGotoValley = TRUE
				ENDIF
			
			ELSE
				
				/*IF g_bHintGiven = FALSE
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPos_ScatSite, 25.0)
						SET_GAMEPLAY_COORD_HINT(vPos_ScatSite, -1)
						g_bHintGiven = TRUE
					ENDIF
				EL*/IF g_bObjDoneInvestigate = FALSE
					/*IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_ScatSite, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
						// need this here to avoid the locate chevron flashing off for a frame
					ENDIF*/
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vPos_ScatSite, 5)
						/*IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF g_bPrintCarObj
								PRINT_NOW("SAS1_CAR", DEFAULT_GOD_TEXT_TIME, 0) // Leave your vehicle.
								g_bPrintCarObj = FALSE
							ENDIF
						ELSE
							IF IS_THIS_PRINT_BEING_DISPLAYED("SAS1_CAR")
								CLEAR_THIS_PRINT("SAS1_CAR")
							ENDIF*/
							ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")	// Dialogue: "Damn that's nasty."
							IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_SCAT", CONV_PRIORITY_MEDIUM)
								REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0)
								//SAFE_REMOVE_BLIP(g_hBlip)
								g_bObjDoneInvestigate = TRUE
								//iHowlTimer = -1
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
					
				IF g_bAudDoneHowl = FALSE
					/*IF NOT IS_PED_STOPPED(PLAYER_PED_ID())
						STOP_GAMEPLAY_HINT()
					ENDIF*/
					
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), gTrail.nodes[0].vPedSpawn, 40)
					//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						/*IF iHowlTimer < 0
							iHowlTimer = GET_GAME_TIMER() + HOWL_TIME
						ELIF GET_GAME_TIMER() > iHowlTimer AND HAVE_THELASTONE_MODELS_LOADED()*/
							IF CREATE_THELASTONE()
								SAFE_REMOVE_BLIP(g_hBlip)
								gTheLastOne.hBlip = CREATE_PED_BLIP(gTheLastOne.hPed)
								SET_BLIP_SCALE(gTheLastOne.hBlip, BLIP_SIZE_VEHICLE)
								PLAY_SOUND_FROM_COORD(gTheLastOne.hHowlSnd, "ALERT", <<-1541.9651, 4692.7246, 44.0651>>, "SASQUATCH_01_SOUNDSET")	// Audio: Howl
								TRIGGER_SONAR_BLIP(GET_ENTITY_COORDS(gTheLastOne.hPed), 30, HUD_COLOUR_RED)
								TASK_LOOK_AT_COORD(PLAYER_PED_ID(), gTrail.nodes[0].vPedSpawn, 6000, SLF_USE_TORSO)
								//STOP_GAMEPLAY_HINT()
								g_bAudDoneHowl = TRUE
								iHowlTimer = -1

								// Clear areas over hill
								CLEAR_ROUTE_AREAS()
							ENDIF
						//ENDIF
					ENDIF

				ELIF g_bAudDoneHeardHowl = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF iHowlTimer < 0
							iHowlTimer = GET_GAME_TIMER() + HOWL_TIME
						ELIF GET_GAME_TIMER() > iHowlTimer
							ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
							IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_CHASE1a", CONV_PRIORITY_MEDIUM)	// Dialogue: "What in the hell was that"
								g_bAudDoneHeardHowl = TRUE
								QUEUE_MISSION_STAGE(MS_CHASE)
								g_mission.section = SECTION_CLEANUP
							ENDIF
						ENDIF
					ENDIF
				/*ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					QUEUE_MISSION_STAGE(MS_CHASE)
					g_mission.section = SECTION_CLEANUP*/
				ENDIF
				
				IF g_bObjDoneLeavingValley = FALSE
					IF bIsPlayerInValley = FALSE
						PRINT_NOW("SAS1_A0", DEFAULT_GOD_TEXT_TIME, 0)	// Objective: Don't leave the valley
						g_bObjDoneLeavingValley = TRUE
						g_iPlayerLeavingTimer = GET_GAME_TIMER() + PLAYER_LEAVING_RESET_TIME
					ENDIF

				ELIF GET_GAME_TIMER() > g_iPlayerLeavingTimer
					IF bIsPlayerInValley
						g_bObjDoneLeavingValley = FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF

	ENDIF
	
	IF g_mission.section = SECTION_CLEANUP
		SAFE_REMOVE_BLIP(g_hBlip)
		CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_GoToValley")
		CHANGE_MISSION_STAGE()
	ENDIF

ENDPROC

PROC SwitchToOutro()
	CLEAR_PRINTS()
	SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
	IF IS_POINT_IN_POLY_2D(g_polyCliffArea, GET_ENTITY_COORDS(gTheLastOne.hPed, FALSE))
		g_bCutsceneInRoad = FALSE
	ELSE
		g_bCutsceneInRoad = TRUE
	ENDIF
	CHANGE_MISSION_STAGE()
ENDPROC

/// PURPOSE:
///    The player chases the Last One
PROC STAGE_Chase()
	
	IF g_mission.bSyncForSkip = TRUE
		//-- Sync for skip
		RC_START_Z_SKIP()
		CPRINTLN(DEBUG_MISSION,"Skipping to STAGE_Chase")
		RELEASE_THELASTONE(TRUE)
		RELEASE_LIONS(TRUE)
		RELEASE_START_SCENE(TRUE)
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos_ChasePlayerStart)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fRot_ChasePlayerStart)
		ENDIF
		
		WHILE NOT LOAD_TEXT()
			WAIT(0)
		ENDWHILE

		WHILE NOT ATTEMPT_CREATE_DRESSING()
			WAIT(0)
		ENDWHILE
		
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
		ELSE
			WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		RC_END_Z_SKIP()
		g_mission.bSyncForSkip = FALSE
	ENDIF

	IF g_mission.section = SECTION_SETUP
		LOAD_THELASTONE_MODELS()
		LOAD_LION_MODELS()
		IF HAVE_THELASTONE_MODELS_LOADED()
		AND HAVE_LION_MODELS_LOADED()
			RELEASE_START_SCENE(FALSE)
			
			IF NOT DOES_ENTITY_EXIST(gTheLastOne.hPed)
				CREATE_THELASTONE()
			ENDIF
			CREATE_LIONS()
			START_TRAIL()
			
			//RELEASE_THELASTONE_MODELS()
			RELEASE_LION_MODELS()
			REQUEST_CUTSCENE("sas_2_rcm_t7")	// Pre-request outro so it's ready at the end

			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_CHASE, "Chase")

			g_bAudDoneChase = FALSE
			//g_bAudDoneWounded = FALSE
			g_bAudDoneCornered = FALSE
			
			/*iCurrentVehicleChaseLines = 0
			sVehicleChaseLines[0] = "SAS1_VEHICLE_1"
			sVehicleChaseLines[1] = "SAS1_VEHICLE_2"
			sVehicleChaseLines[2] = "SAS1_VEHICLE_3"
			iVehicleChaseLineTimer = 0*/
			
			// Tone down the amount of damage the lions do
			SET_AI_MELEE_WEAPON_DAMAGE_MODIFIER(0.2)
			SET_AI_WEAPON_DAMAGE_MODIFIER(0.2)
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
			iExplosionCounter = 0
			iExplosionTimer = 0
			bBlockedByCar = FALSE
			
			g_bObjDoneChase = FALSE
			g_bObjDoneLeavingValley = FALSE
			CPRINTLN(DEBUG_MISSION,"Setup STAGE_Chase")
			g_bHelpDoneListen = FALSE
			g_mission.section = SECTION_RUNNING
		ENDIF
	ENDIF
	
	IF g_mission.section = SECTION_RUNNING
	
		//--- Check state fail conditions + stage skips
		IF NOT IS_PED_UNINJURED(PLAYER_PED_ID())
			QUEUE_MISSION_STAGE(MS_FAILED)
			g_mission.failReason = FAIL_REASON_PLAYER_DEAD
			g_mission.section = SECTION_CLEANUP
		
		ELIF HAS_THELASTONE_HOWL_TIMED_OUT()
			QUEUE_MISSION_STAGE(MS_FAILED)
			g_mission.failReason = FAIL_REASON_LOST_THELASTONE
			g_mission.section = SECTION_CLEANUP
		
		ELIF NOT IS_PED_UNINJURED(gTheLastOne.hPed)
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(gTheLastOne.hPed), 4.0)
			OR bBlockedByCar
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
				Script_Passed()
			ELSE
				QUEUE_MISSION_STAGE(MS_OUTRO)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iOutroStartTimer = GET_GAME_TIMER() + 3000
				g_mission.section = SECTION_CLEANUP
				SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
			ENDIF
		
		ELIF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(gTheLastOne.hPed), 4.0)
			IF GET_GAME_TIMER() > iExplosionTimer
				iExplosionCounter++
				iExplosionTimer = GET_GAME_TIMER() + 1000
				INFORM_MISSION_STATS_OF_INCREMENT(TLO_WOUNDS)
			ENDIF
			
			IF (g_bAudDoneCornered AND iExplosionCounter > 0) OR (NOT g_bAudDoneCornered AND iExplosionCounter > 1)
				IF IS_PED_UNINJURED(gTheLastOne.hPed)
					//EXPLODE_PED_HEAD(gTheLastOne.hPed)
					SET_ENTITY_HEALTH(gTheLastOne.hPed, 99)
				ENDIF
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 2, REPLAY_IMPORTANCE_HIGH)	// Record the player exploding the sasquatch
				Script_Passed()
			ENDIF

		ELIF g_mission.bRequestCpSkip
			g_mission.section = SECTION_CLEANUP
			
		ELIF g_mission.bRequestJSkip
			IF g_bAudDoneCornered
				QUEUE_MISSION_STAGE(MS_OUTRO)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iOutroStartTimer = GET_GAME_TIMER() + 3000
				g_mission.section = SECTION_CLEANUP
			ELSE
				SKIP_TO_CHASE_END()
				g_mission.bRequestJSkip		= FALSE
				g_mission.bRequestPSkip		= FALSE
				g_mission.bRequestCpSkip	= FALSE
			ENDIF
			
		ELIF g_mission.bRequestPSkip
			QUEUE_MISSION_STAGE(MS_GOTOVALLEY)
			g_mission.section = SECTION_CLEANUP
			
		//--- Update state
		ELSE
			UPDATE_TRAIL()
			
			//PLAY_IN_VEHICLE_LINE()
			
			CHECK_FOR_DEAD_ANIMALS()
			CHECK_FOR_ORLEANS_INJURY()
			
			IF gTrail.mode = DONE_STATE
				gTrail.mode = SETUP_STATE
				IF gTrail.curNode = 0 AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					gTrail.curNode = 2
				ELSE
					gTrail.curNode++
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), gTrail.nodes[10].vPedDest, DEFAULT_CUTSCENE_LOAD_DIST)
				REQUEST_CUTSCENE("sas_2_rcm_t7")	// Pre-request outro so it's ready at the end
			ENDIF

			IF g_bObjDoneChase = FALSE
				PRINT_NOW("SAS1_C1", DEFAULT_GOD_TEXT_TIME, 0)	// Objective: "Kill the Last One"
				g_bObjDoneChase = TRUE
				g_iHelpDelayListen = GET_GAME_TIMER() + HELP_TEXT_DELAY
			
			ELIF g_bHelpDoneListen = FALSE
				IF g_iHelpDelayListen < GET_GAME_TIMER()
					PRINT_HELP("SAS1_H0")	// Listen for the Last One's howl to find his location.
					g_bHelpDoneListen = TRUE
				ENDIF
			
			ELIF g_bAudDoneChase = FALSE
				IF gTrail.curNode < 5
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//AND IS_TRACKED_PED_VISIBLE(gTheLastOne.hPed)
					AND DID_SIGHTTEST_PASS(iSightTestID)
						ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(pedsForConversation, "SAS1AUD", "SAS1_CHASE1b", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						g_bAudDoneChase = TRUE
					ENDIF
				ENDIF

			ELIF g_bAudDoneCornered = FALSE
				IF gTrail.curNode >= gTrail.size-1
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPos_BrawlOnArena, <<35,35,35>>)
						// Stop the mountain lions attacking
						MAKE_LIONS_FLEE()
						ADD_PED_FOR_DIALOGUE(pedsForConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
						IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_CHASE3", CONV_PRIORITY_MEDIUM)	// Conversation: "You cornered now"
							SET_PED_ANGLED_DEFENSIVE_AREA(gTheLastOne.hPed, <<-1536.6, 4543.7, 45.5>>, <<-1538.2,4536.2,48.6>>, 5.0)
							g_bAudDoneCornered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF g_bObjDoneLeavingValley = FALSE
				IF NOT IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					PRINT_NOW("SAS1_C2", DEFAULT_GOD_TEXT_TIME, 0)	// Don't leave the Last One
					g_bObjDoneLeavingValley = TRUE
					g_iPlayerLeavingTimer = GET_GAME_TIMER() + PLAYER_LEAVING_RESET_TIME
				ENDIF

			ELIF GET_GAME_TIMER() > g_iPlayerLeavingTimer
				IF IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					g_bObjDoneLeavingValley = FALSE
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
	IF g_mission.section = SECTION_CLEANUP
		IF g_mission.queueNextStage = MS_OUTRO
			IF GET_GAME_TIMER() > iOutroStartTimer
				CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_Chase")
				SwitchToOutro()
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_Chase")
			SAFE_REMOVE_BLIP(gTheLastOne.hBlip)
			CLEAR_PRINTS()
			CHANGE_MISSION_STAGE()
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Plays the outro cutscene after the player shoots the Last One
PROC STAGE_Outro()	

//	Mocap scene
// -------------

	IF g_mission.bSyncForSkip = TRUE
		RC_START_Z_SKIP()
		CPRINTLN(DEBUG_MISSION,"Skipping to STAGE_Outro")
		RELEASE_THELASTONE(TRUE)
		RELEASE_LIONS(TRUE)
		RELEASE_START_SCENE(TRUE)
		LOAD_THELASTONE_MODELS()
		WHILE NOT HAVE_THELASTONE_MODELS_LOADED()
			WAIT(0)
		ENDWHILE
			
		WHILE NOT DOES_ENTITY_EXIST(gTheLastOne.hPed)
			CREATE_THELASTONE()
			WAIT(0)
		ENDWHILE
		SET_PED_ENABLED(gTheLastOne.hPed, TRUE)

		WHILE NOT LOAD_TEXT()
			WAIT(0)
		ENDWHILE

		WHILE NOT ATTEMPT_CREATE_DRESSING()
			WAIT(0)
		ENDWHILE
		
		REQUEST_MODEL(model_DressingTruck)
		REQUEST_MODEL(model_TheLastOne)
		//REQUEST_ANIM_DICT("Dead")

		RC_REQUEST_CUTSCENE("sas_2_rcm_t7")
		WHILE NOT RC_IS_CUTSCENE_OK_TO_START()
			WAIT(0)
		ENDWHILE
		
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			RC_END_Z_SKIP()
		ELSE
			WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		//RC_END_Z_SKIP()
		g_mission.bSyncForSkip = FALSE
	ENDIF

	IF g_mission.section = SECTION_SETUP
		
		REQUEST_MODEL(model_DressingTruck)
		REQUEST_MODEL(model_TheLastOne)
		//REQUEST_ANIM_DICT("Dead")

		RC_REQUEST_CUTSCENE("sas_2_rcm_t7")
		
		IF RC_IS_CUTSCENE_OK_TO_START()
			
			//RELEASE_THELASTONE(TRUE)
			IF g_bCutsceneInRoad
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(FLOW_FAIL_FADE_TIME)
				ENDIF
				RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, FALSE, FALSE, FALSE)
				WAIT(FLOW_FAIL_FADE_TIME+1000)
				/*IF NOT (GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED) AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					oiRifle = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(oiRifle)
						REGISTER_ENTITY_FOR_CUTSCENE(oiRifle, "Franklins_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				ENDIF*/
				/*gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_Bigfoot, <<-1566.59, 4542.98, 17.14>>, -90.15)
				SET_ENTITY_COORDS_NO_OFFSET(gTheLastOne.hPed, <<-1566.59, 4542.98, 17.14>>)*/
				IF DOES_ENTITY_EXIST(gTheLastOne.hPed) AND IS_PED_INJURED(gTheLastOne.hPed)
					// Set health to only just alive so that he dies with a single hit
					RESURRECT_PED(gTheLastOne.hPed)
				ENDIF
				IF IS_PED_UNINJURED(gTheLastOne.hPed)
					SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
					REGISTER_ENTITY_FOR_CUTSCENE(gTheLastOne.hPed, "Orleans", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				START_CUTSCENE_AT_COORDS(vDeadInRoadPos)
				WAIT(0)
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TheLastOne")
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vRoadAreaPos1, vRoadAreaPos2, fRoadAreaWidth, << -1519.9150, 4524.7002, 44.4031 >>, 116.3951, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RC_START_CUTSCENE_MODE(vDeadInRoadPos)
			ELSE
				/*IF NOT (GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED) AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					oiRifle = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(oiRifle)
						REGISTER_ENTITY_FOR_CUTSCENE(oiRifle, "Franklins_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				ENDIF*/
				/*gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_Bigfoot, vPos_BrawlOffArena)
				SET_ENTITY_COORDS_NO_OFFSET(gTheLastOne.hPed, vPos_BrawlOffArena)*/
				IF DOES_ENTITY_EXIST(gTheLastOne.hPed) AND IS_PED_INJURED(gTheLastOne.hPed)
					// Set health to only just alive so that he dies with a single hit
					RESURRECT_PED(gTheLastOne.hPed)
				ENDIF
				IF IS_PED_UNINJURED(gTheLastOne.hPed)
					SET_ENTITY_HEALTH(gTheLastOne.hPed, 110)
					REGISTER_ENTITY_FOR_CUTSCENE(gTheLastOne.hPed, "Orleans", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					SET_PED_PRELOAD_VARIATION_DATA(gTheLastOne.hPed, PED_COMP_HEAD, 1, 1)
					SET_PED_PRELOAD_VARIATION_DATA(gTheLastOne.hPed, PED_COMP_BERD, 1, 0)
					SET_PED_PRELOAD_VARIATION_DATA(gTheLastOne.hPed, PED_COMP_SPECIAL, 1, 0)
					SET_PED_PRELOAD_VARIATION_DATA(gTheLastOne.hPed, PED_COMP_TEETH, 1, 0)
				ENDIF
				START_CUTSCENE()
				WAIT(0)
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TheLastOne")
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vCliffAreaPos1, vCliffAreaPos2, fCliffAreaWidth, <<-1530.6138, 4538.0234, 46.8171>>, 318.992, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RC_START_CUTSCENE_MODE(vPos_BrawlOffArena)
			ENDIF
			
			RELEASE_LIONS(FALSE)
			RELEASE_START_SCENE(TRUE)
			
			// set timer to change the face
			iChangeFaceTimer = GET_GAME_TIMER() + 500
			bChangeFace = TRUE
			
			// Create truck (or use players last car)
			VEHICLE_INDEX hVehicle = GET_PLAYERS_LAST_VEHICLE()
			CLEAR_AREA(vPos_DressingTruck, 200.0, TRUE)
			
			iTriggerPostFxTime = GET_CUTSCENE_TOTAL_DURATION() - 300
			bTriggerPostFx = TRUE
			
			IF IS_VEHICLE_OK(hVehicle)
				SET_ENTITY_COORDS(hVehicle, vPos_DressingTruck)
				SET_ENTITY_HEADING(hVehicle, fRot_DressingTruck)
				SET_VEHICLE_ON_GROUND_PROPERLY(hVehicle)
			ENDIF
			CPRINTLN(DEBUG_MISSION,"Setup STAGE_Outro")
			g_mission.section = SECTION_RUNNING
		ENDIF
	ENDIF
	
	IF g_mission.section = SECTION_RUNNING
		IF NOT IS_VEHICLE_OK(viMissionFinishedVehicle)
			viMissionFinishedVehicle = GET_PLAYERS_LAST_VEHICLE()
			IF NOT IS_VEHICLE_OK(viMissionFinishedVehicle) AND HAS_MODEL_LOADED(model_DressingTruck)
				viMissionFinishedVehicle = CREATE_VEHICLE(model_DressingTruck, vPos_DressingTruck, fRot_DressingTruck)
				IF IS_VEHICLE_OK(viMissionFinishedVehicle)
					SET_VEHICLE_COLOUR_COMBINATION(viMissionFinishedVehicle, 1)
				ENDIF
			ENDIF
		ENDIF
		/*IF DOES_ENTITY_EXIST(gTheLastOne.hPed)
			IF IS_ENTITY_ALIVE(gTheLastOne.hPed)
				SET_ENTITY_HEALTH(gTheLastOne.hPed, 0)
			ENDIF
		ELSE*/
		/*IF NOT DOES_ENTITY_EXIST(gTheLastOne.hPed)
			IF HAS_MODEL_LOADED(model_Bigfoot) //AND HAS_ANIM_DICT_LOADED("Dead")
				IF g_bCutsceneInRoad
					gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_Bigfoot, <<-1566.59, 4542.98, 17.14>>, -90.15)
					SET_ENTITY_COORDS_NO_OFFSET(gTheLastOne.hPed, <<-1566.59, 4542.98, 17.14>>)
				ELSE
					gTheLastOne.hPed = CREATE_PED(PEDTYPE_SPECIAL, model_Bigfoot, vPos_BrawlOffArena)
					SET_ENTITY_COORDS_NO_OFFSET(gTheLastOne.hPed, vPos_BrawlOffArena)
				ENDIF
				//TASK_PLAY_ANIM(gTheLastOne.hPed, "Dead", "dead_a", INSTANT_BLEND_IN)
				//TASK_PLAY_ANIM(gTheLastOne.hPed, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
				SET_ENTITY_VISIBLE(gTheLastOne.hPed, FALSE)
				SET_PED_MONEY(gTheLastOne.hPed, 0)
			ENDIF
		ENDIF*/
		
		// Place player
		/*IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF g_bCutsceneInRoad
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), << -1568.5000, 4531.7500, 16.6564 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 343.2536)
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos_OutroRespot)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fRot_OutroRespot)
				ENDIF
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-22)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
		ENDIF*/
		
		IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
			IF bTriggerPostFx
				IF GET_CUTSCENE_TIME() > iTriggerPostFxTime
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					bTriggerPostFx = FALSE
				ENDIF
			ENDIF
		ENDIF
		//IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)= CAM_VIEW_MODE_FIRST_PERSON 
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-36.1791)
				//CPRINTLN(DEBUG_MISSION, "Setting camera exit pitch")
			ENDIF
			//CPRINTLN(DEBUG_MISSION, "setting camera exit state")
		//ENDIF
			
		IF IS_PED_UNINJURED(gTheLastOne.hPed)
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Orleans")
				/*SEQUENCE_INDEX siSasquatch
				OPEN_SEQUENCE_TASK(siSasquatch)
					TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 5000, AF_LOOPING | AF_NOT_INTERRUPTABLE)
					TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_ENDS_IN_DEAD_POSE)
				CLOSE_SEQUENCE_TASK(siSasquatch)
				TASK_PERFORM_SEQUENCE(gTheLastOne.hPed, siSasquatch)
				CLEAR_SEQUENCE_TASK(siSasquatch)*/
				/*IF g_bCutsceneInRoad
					SET_ENTITY_COORDS_GROUNDED(gTheLastOne.hPed, <<-1569.26, 4542.49, 17.25>>)
					// PRINT_NOW("Setting Sasquatch pos", DEFAULT_HELP_TEXT_TIME, 1)		- Don't check in with this!
				ELSE
					SET_ENTITY_COORDS_GROUNDED(gTheLastOne.hPed, <<-1539.40, 4538.07, 49.38>>)
					// PRINT_NOW("Setting Sasquatch pos", DEFAULT_HELP_TEXT_TIME, 1)		- Don't check in with this!
				ENDIF*/
				//TASK_PLAY_ANIM(gTheLastOne.hPed, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
				
				IF g_bCutsceneInRoad
					SET_ENTITY_COORDS_GROUNDED(gTheLastOne.hPed, <<-1569.30, 4542.46, 17.21>>)
				ELSE
					SET_ENTITY_COORDS_GROUNDED(gTheLastOne.hPed, <<-1538.96, 4538.03, 47.83>>)
				ENDIF
				SET_PED_CONFIG_FLAG(gTheLastOne.hPed, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(gTheLastOne.hPed, FALSE)
				SEQUENCE_INDEX siSasquatch
				OPEN_SEQUENCE_TASK(siSasquatch)
					//TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 5000, AF_LOOPING | AF_NOT_INTERRUPTABLE)
					/*TASK_PLAY_ANIM(NULL, animdict_Leadout, "leadout_sas_3_rcm_sas", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
					IF NOT g_bCutsceneInRoad
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1479.375000,4511.744141,46.871365>>, PEDMOVE_SPRINT)
					ENDIF
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)*/ 
					TASK_PLAY_ANIM(NULL, animdict_Leadout, "leadout_sas_3_rcm_sas", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE, default, default, AIK_DISABLE_LEG_IK)
					TASK_PLAY_ANIM(NULL, animdict_Leadout, "sas_idle_sit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_ABORT_ON_WEAPON_DAMAGE, default, default, AIK_DISABLE_LEG_IK)
				CLOSE_SEQUENCE_TASK(siSasquatch)
				TASK_PERFORM_SEQUENCE(gTheLastOne.hPed, siSasquatch)
				CLEAR_SEQUENCE_TASK(siSasquatch)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(gTheLastOne.hPed, TRUE)
				//SET_SUPPRESS_LOW_RAGDOLL_LOD_SWITCH_UPON_DEATH(gTheLastOne.hPed)
				//SET_ENTITY_VISIBLE(gTheLastOne.hPed, TRUE)
				SET_PED_MONEY(gTheLastOne.hPed, 0)
				
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(gTheLastOne.hPed, PDZ_HEAD,0.810, 0.733, BDT_BULLET_LARGE)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(gTheLastOne.hPed, PDZ_TORSO,0.940, 0.590, BDT_BULLET_LARGE)
				
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FRANKLIN")
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), gTheLastOne.hPed, -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(gTheLastOne.hPed, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				ENDIF
			ENDIF
			
			IF bChangeFace
				IF GET_GAME_TIMER() > iChangeFaceTimer
					SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_HEAD, 1, 1, 0)
					SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_BERD, 1, 0, 0)
					SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_SPECIAL, 1, 0, 0)
					SET_PED_COMPONENT_VARIATION(gTheLastOne.hPed, PED_COMP_TEETH, 1, 0, 0)
					bChangeFace = false
				ENDIF
			ENDIF
		ENDIF
		
		IF g_mission.bRequestPSkip
			//QUEUE_MISSION_STAGE(MS_CHASE)
			STOP_CUTSCENE()
			//g_mission.section = SECTION_CLEANUP
			SKIP_TO_CHASE_END()
			g_mission.bRequestJSkip		= FALSE
			g_mission.bRequestPSkip		= FALSE
			g_mission.bRequestCpSkip	= FALSE
			
		ELIF g_mission.bRequestCpSkip
			STOP_CUTSCENE()
			QUEUE_MISSION_STAGE(MS_PASSED)
			g_mission.section = SECTION_CLEANUP
			
		ELIF NOT IS_CUTSCENE_PLAYING()
			QUEUE_MISSION_STAGE(MS_PASSED)
			g_mission.section = SECTION_CLEANUP
		ENDIF
	ENDIF
	
	IF g_mission.section = SECTION_CLEANUP
		IF NOT IS_CUTSCENE_PLAYING()
			REPLAY_STOP_EVENT()
			/*IF IS_PED_UNINJURED(gTheLastOne.hPed)
				SEQUENCE_INDEX siSasquatch
				OPEN_SEQUENCE_TASK(siSasquatch)
					TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 5000, AF_LOOPING | AF_NOT_INTERRUPTABLE)
					TASK_PLAY_ANIM(NULL, animdict_Plead, "sas_idle_sit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_ENDS_IN_DEAD_POSE)
				CLOSE_SEQUENCE_TASK(siSasquatch)
				TASK_PERFORM_SEQUENCE(gTheLastOne.hPed, siSasquatch)
				CLEAR_SEQUENCE_TASK(siSasquatch)
				SET_ENTITY_VISIBLE(gTheLastOne.hPed, TRUE)
			ENDIF*/
			
			RC_END_CUTSCENE_MODE()
			
			IF WAS_CUTSCENE_SKIPPED()
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)= CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-36.1791)
					CPRINTLN(DEBUG_MISSION, "Setting camera exit pitch, skipped cutscene")
				ENDIF
				CPRINTLN(DEBUG_MISSION, "Skipped cutscene")
			ENDIF
			
			CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_Outro")
			CHANGE_MISSION_STAGE()
			SAFE_DELETE_OBJECT(oiRifle)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades the screen out and fails the mission
PROC STAGE_FailFade()	

	SWITCH g_mission.section
	
		CASE SECTION_SETUP
		
			CLEAR_PRINTS()
			
			IF IS_PED_HARMED(gStartScene.hHunter)
				g_mission.failReason = FAIL_REASON_HUNTER_DEAD
			ENDIF
			
			// Enable the ambient audio zone again
			IF NOT IS_AMBIENT_ZONE_ENABLED("AZ_DISTANT_SASQUATCH")
				SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_DISTANT_SASQUATCH", TRUE, TRUE)
			ENDIF
			
			CPRINTLN(DEBUG_MISSION,"Setup STAGE_FailFade")
			
			STRING sFailType 
			sFailType = EMPTY_STRING
			
			// Convert fail reason to string
			SWITCH(g_mission.failReason)
				CASE FAIL_REASON_HUNTER_DEAD
					sFailType = "SAS1_X0"	// ~r~You killed the hunter.
				BREAK

				CASE FAIL_REASON_HUNTER_THREATENED
					sFailType = "SAS1_X1"	// ~r~You threatened the hunter.
				BREAK

				CASE FAIL_REASON_LOST_THELASTONE
					sFailType = "SAS1_X6"	// ~r~The Last One escaped.
				BREAK
			ENDSWITCH
			
			IF ARE_STRINGS_EQUAL(sFailType, EMPTY_STRING)
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailType)//, TRUE, bDelayFade)
			ENDIF
			
			g_mission.section = SECTION_RUNNING
		BREAK
		
		CASE SECTION_RUNNING
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()	
				CPRINTLN(DEBUG_MISSION,"Cleanup STAGE_FailFade")

				Script_TextCleanup()
				
				// Immediately destroy all entities
				RELEASE_THELASTONE(TRUE)
				RELEASE_LIONS(TRUE)
				RELEASE_START_SCENE(TRUE)
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC STAGE_PassDelay()

	SWITCH g_mission.section
	
		CASE SECTION_SETUP
			
			IF IS_PED_UNINJURED(gTheLastOne.hPed)
				ADD_PED_FOR_DIALOGUE(pedsForConversation, 3, gTheLastOne.hPed, "ORLEANS")
				IF CREATE_CONVERSATION(pedsForConversation, "SAS1AUD", "SAS1_LEADOUT", CONV_PRIORITY_HIGH)
					CLEAR_PRINTS()
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0)
					iPassDelay = GET_GAME_TIMER() + PASS_DELAY_TIME
					
					g_mission.section = SECTION_RUNNING
				ENDIF
			ELSE
				Script_Passed()
			ENDIF
		BREAK
		
		CASE SECTION_RUNNING
			IF (GET_GAME_TIMER() > iPassDelay AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			OR NOT IS_PED_UNINJURED(gTheLastOne.hPed)
				Script_Passed()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Check for Forced Pass or Fail and handle skipping stages
	PROC DEBUG_Check_Debug_Keys()
		IF g_mission.stage <> MS_FAILED
			IF g_mission.section = SECTION_RUNNING

				// Mission is running, so reset sync-for-skip flag
				g_mission.bSyncForSkip = FALSE

				// Check for Pass
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()

				// Check for Fail
				ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					WAIT_FOR_CUTSCENE_TO_STOP()
					QUEUE_MISSION_STAGE(MS_FAILED)
					g_mission.failReason = FAIL_REASON_DEFAULT
					g_mission.section = SECTION_CLEANUP
				
				// Check for J skip
				ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
					g_mission.bRequestJSkip = TRUE
			
				// Check for P skip
				ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
					g_mission.bRequestPSkip = TRUE

				// Check for Z skip
				ELIF LAUNCH_MISSION_STAGE_MENU(zSkipMenu, g_mission.queueCpSkip, g_mission.queueCpSkip)
					g_mission.bRequestCpSkip = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

#ENDIF

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	//-- Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		IF Get_Replay_Mid_Mission_Stage() = 0
	      	g_bSceneAutoTrigger = TRUE
			// Re-create the initial scene
			eInitialSceneStage = IS_REQUEST_SCENE
			WHILE NOT SetupScene_TheLastOne(sRCLauncherDataLocal)
				WAIT(0)
			ENDWHILE
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			g_bSceneAutoTrigger = FALSE
		ENDIF
	ENDIF
										
	//-- Mission initialisation ***************
	INIT_DATA()
	INIT_MISSION_DATA()

	//-- Mission loop *************************

	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TheLastOne")
	
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
	
// 		Debug drawing functions, uncomment to see them

//		IF IS_PED_UNINJURED(g_TheLastOne.hPed)
//			IF IS_TRACKED_PED_VISIBLE(g_TheLastOne.hPed)
//				DISPLAY_TEXT(0.1,0.2,"DEST_DK")
//			ENDIF
//		ENDIF

//		DISPLAY_POLY(g_polyPlayArea)					// play area = red
//		DISPLAY_POLY(g_polyFailArea, 100, 100, 255)		// fail area = blue
		//DISPLAY_POLY(g_polyCliffArea, 0, 255, 0)		// cliff area = green
		//DISPLAY_POLY2(g_polyCliffArea)
		
//		DISPLAY_TEXT_WITH_FLOAT(0.5, 0.5, "NUMBR", GET_ENTITY_SPEED(PLAYER_PED_ID()), 1)
//		DISPLAY_TEXT_WITH_FLOAT(0.5, 0.8, "NUMBR", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPos_ScatSite), 2)

		#IF IS_DEBUG_BUILD
			IF g_bDebugWaypoints
				DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBR", gTrail.curNode)
				
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[0].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[0].vPedDest, 2, 255,0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[1].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[1].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[2].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[2].vPedDest, 2, 255, 0,0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[3].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[3].vPedDest, 2, 255, 0 ,0)

				DRAW_DEBUG_SPHERE(gTrail.nodes[4].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[4].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[5].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[5].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[6].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[6].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[7].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[7].vPedDest, 2, 255, 0, 0)

				DRAW_DEBUG_SPHERE(gTrail.nodes[8].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[8].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[9].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[9].vPedDest, 2, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(gTrail.nodes[10].vPedSpawn, 2)
				DRAW_DEBUG_SPHERE(gTrail.nodes[10].vPedDest, 2, 255, 0, 0)
			ENDIF
		#ENDIF
		
		// Remove all scenario peds during the mission
		//SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(1.0, 0.0)
		
		// Don't want the player to be able to take photos of the last guy B* 1392716
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		
		//Fix for bug 2174315
		IF DOES_ENTITY_EXIST(gTheLastOne.hPed)
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		ENDIF
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SWITCH (g_mission.stage)
				CASE MS_INIT
					STAGE_Init()
				BREAK

				CASE MS_INTRO
					STAGE_Intro()
				BREAK

				CASE MS_GOTOVALLEY
					STAGE_GoToValley()
				BREAK

				CASE MS_CHASE
					STAGE_Chase()
				BREAK

				CASE MS_OUTRO
					STAGE_Outro()
				BREAK

				CASE MS_PASSED
					STAGE_PassDelay()
				BREAK

				CASE MS_FAILED
					STAGE_FailFade()
				BREAK

			ENDSWITCH
		ENDIF

		WAIT(0)
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD 
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

	SCRIPT_ASSERT("Script should never reach here. Always terminate with cleanup function.")
ENDSCRIPT
