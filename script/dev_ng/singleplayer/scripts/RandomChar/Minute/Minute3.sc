
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "script_blips.sch"
using "net_include.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch"
USING "commands_hud.sch"
USING "rgeneral_include.sch"
USING "CompletionPercentage_public.sch"
USING "initial_scenes_Minute.sch"
USING "taxi_functions.sch"
USING "RC_Threat_public.sch"
USING "RC_Area_public.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

USING "initial_scenes_Minute.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Minute3.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Launcher vars
g_structRCScriptArgs sRCLauncherDataLocal
SCENARIO_BLOCKING_INDEX mScenarioBlocker

// ===========================================================================================================
//		Enums, structs and consts
// ===========================================================================================================
ENUM MISSION_STATUS
	MS_INIT,
	MS_PRE_INTRO,
	MS_INTRO_PLAYING,
	MS_DRIVE_TO_FARM,
	MS_JOSEF_ATTACKS,
	MS_SEARCH_FARM,
	MS_CAR_CHASE,
	MS_FLEE_ON_FOOT,
	MS_PASS_DELAY,
	MS_FAIL_DELAY
ENDENUM

STRUCT MISSION_DUDE
	PED_INDEX piDude
	BLIP_INDEX biBlip
ENDSTRUCT

ENUM MISSION_FAIL_REASON
	MFR_NONE,
	MFR_KILLED_MANUEL,
	MFR_KILLED_A_MEXICAN,
	MFR_INJURED_MANUEL,
	MFR_INJURED_A_MEXICAN,
	MFR_JOE_ESCAPED,
	MFR_EXECUTION
ENDENUM

ENUM SUB_STATE
	SS_SETUP,
	SS_UPDATE,
	SS_UPDATE_TWO,
	SS_UPDATE_THREE,
	SS_CLEANUP
ENDENUM

ENUM NEAREST_ENEMY
	NE_NEITHER,
	NE_JOE,
	NE_JOSEF
ENDENUM

// ped conversation IDs
CONST_INT TREVOR_ID 2
CONST_INT MANUEL_ID 3
CONST_INT JOE_ID 4
CONST_INT JOSEF_ID 5
CONST_INT MEX_ID 6

CONST_INT MANUEL 0
CONST_INT MANUEL_CAR 1
CONST_INT PLAYER_CAR 0

CONST_INT CP_AFTER_MOCAP 0
CONST_INT CP_KILL_JOSEF 1
CONST_INT CP_KILL_JOE 2
CONST_INT CP_MISSION_PASSED 3

// ===========================================================================================================
//		Mission variables
// ===========================================================================================================
MISSION_STATUS missionStage = MS_INIT
MISSION_FAIL_REASON MissionFailReason = MFR_NONE
SUB_STATE MissionSubState = SS_SETUP

STRING ANIM_DICT_GLANCES = "rcmminute3glances"
STRING ANIM_DICT_PANIC = "rcmminute3panic"

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[3]
#ENDIF

VECTOR vFarmPosition = <<423.575714,6478.868652,32.815613>>

VECTOR vSpawnPedsSize = <<250.0, 250.0, 250.0>>
FLOAT fJoeDrives = 35.0
FLOAT fJoeDrivesLarge = 45.0
CONST_INT JOE_FLEES_TIME 30000

OBJECT_INDEX oiBarnDoorLeft
OBJECT_INDEX oiBarnDoorRight
MODEL_NAMES mnBarnDoorLeft = PROP_BARN_DOOR_L
MODEL_NAMES mnBarnDoorRight = PROP_BARN_DOOR_R
CONST_FLOAT DOOR_LEFT_HEADING_END 66.46
CONST_FLOAT DOOR_LEFT_HEADING_ALT -131.39
CONST_FLOAT DOOR_RIGHT_HEADING_END 360.0
CONST_FLOAT DOOR_RIGHT_HEADING_ALT -128.34
BOOL bBarnDoorLeftSetup = FALSE
BOOL bBarnDoorRightSetup = FALSE

OBJECT_INDEX oiPistol	// Trevor's pistol, used in the intro

BOOL bNotAimedAtManuel = TRUE
INT iManuelStoppedTimer = -1
CONST_INT MAX_MANUEL_STOPPED_TIME 10000
INT iManuelFollowTimer = -1
INT iManuelBumpTimer = -1
CONST_INT MANUEL_CONV_TIME 1000

MISSION_DUDE mdJosef
VECTOR vAltJosefPos = <<426.96, 6513.80, 27.72>>
FLOAT fAltJosefHeading = -108.29
MISSION_DUDE mdJoe
VECTOR vAltJoePos = <<429.98, 6470.58, 28.78>>
FLOAT fAltJoeHeading = -126.05
REL_GROUP_HASH enemyGroup

CONST_INT NUM_MEXICANS 5
PED_INDEX piMexican[NUM_MEXICANS]
VECTOR vMexicanPos[NUM_MEXICANS]
FLOAT fMexicanHeading[NUM_MEXICANS]
VECTOR vLeaveDest[NUM_MEXICANS]
/*STRING sMexicanLine[NUM_MEXICANS]
STRING sMexicanName[NUM_MEXICANS]*/
BOOL bFleeTalkReady[NUM_MEXICANS]

CONST_INT NUM_MEXICANS_2 3
PED_INDEX piMexican2[NUM_MEXICANS_2]
VECTOR vMexicanPos2[NUM_MEXICANS_2]
FLOAT fMexicanHeading2[NUM_MEXICANS_2]
//VECTOR vLeaveDest2[NUM_MEXICANS_2]

MODEL_NAMES mnMexicanMale = S_M_M_Migrant_01
MODEL_NAMES mnMexicanFemale = S_F_Y_Migrant_01
enumCharacterList eclJoe = CHAR_JOE
enumCharacterList eclJosef = CHAR_JOSEF

// farm vehicle variables
VEHICLE_INDEX viTractor
VECTOR vTractorPos = <<416.4,6499.5,28.0>>
FLOAT fTractorHeading = 170.4
MODEL_NAMES mnTractor = TRACTOR2
BOOL bTractorSetup = FALSE

VEHICLE_INDEX viFarmCar
MODEL_NAMES mnFarmCar = BLAZER
VECTOR vAltFarmCarPos = << 422.3570, 6486.7886, 27.7595 >>
FLOAT fAltFarmCarHeading = 83.28
BOOL bFarmCarSetup = FALSE

VECTOR vManuelAfterCutscene = <<-300.7390, 6212.6108, 30.3998>>//<<-301.64, 6212.02, 31.41>>
FLOAT fManuelAfterCutscne = 249.7937//-50.57

VEHICLE_INDEX viVan
//MODEL_NAMES mnVan = tractor2
VECTOR vAltVanPos = <<430.01, 6467.49, 28.41>>
FLOAT fAltVanHeading = 50.92
VECTOR vRecordingStartAlt = << 427.6566, 6469.3843, 28.4275 >>
FLOAT fRecQuaternionXAlt = 0.0014
FLOAT fRecQuaternionYAlt = 0.0015
FLOAT fRecQuaternionZAlt = 0.3467
FLOAT fRecQuaternionWAlt = 0.9380
BOOL bVanSetup = FALSE

STRING sWaypointRecMain = "min3_JandJ01"
STRING sWaypointRecAlt = "min3_JandJ02"
STRING sJoesWaypointRec
INT iNumPointsInJoesRec = 0
CONST_FLOAT iEscapeDist 250.0
CONST_FLOAT iWarnDist 125.0
VECTOR vCheckForPlayersCar1 = <<409.957886,6488.018066,25.749815>>
VECTOR vCheckForPlayersCar2 = <<151.116852,6485.835449,31.627085>>
FLOAT fCheckForPlayersCar = 50.0

// variables for dealing with the player bursting the car tyres
BOOL bTyresBurst = FALSE
INT iTyresBurstTimer = 0
CONST_INT iTyresTime 1000

// clearing the farm area variables
VECTOR vFarmMin = <<144.369324,6368.498047,25.529602>>
VECTOR vFarmMax = <<489.021454,6573.714844,35.059156>>

// the area inside the barn
VECTOR vBarnAreaPos1 = <<443.043427,6456.974121,26.783266>>
VECTOR vBarnAreaPos2 = <<422.019592,6474.671875,30.813354>> 
FLOAT fBarnAreaWidth = 17.0

// The area to boost Joe's casule size so he doesn't clip through the doors
VECTOR vBarnForJoePos1 = <<444.225250,6456.484863,26.768524>>
VECTOR vBarnForJoePos2 = <<420.526215,6475.383789,29.812666>> 
FLOAT fBarnForJoeWidth = 10.0

// The area outside the barn doors
/*VECTOR vBarnBlockingPos1 = <<423.676910,6481.187988,26.809072>>
VECTOR vBarnBlockingPos2 = <<415.893799,6471.259277,29.810034>> 
FLOAT fBarnBlockingWidth = 8.0*/

structPedsForConversation pedConvStruct

BOOL bStartAiDriving = TRUE
BOOL bAllowOneDeadMessage = TRUE
BOOL bJackedJoeAndJosef = FALSE
CONST_INT ONE_GUY_DEAD_TIMEOUT 100
CONST_INT J_AND_J_PAUSE 1500
CONST_INT JOSEF_PAUSES 1500//750

CONST_INT DRIVE_LINE_TIME 20000
CONST_INT NUM_DRIVE_LINES 4
INT iCurrentDriveLine = 0
STRING sDriveLines[NUM_DRIVE_LINES]

CONST_INT JOE_BARN_LINE_TIME 10000
CONST_INT NUM_JOE_BARN_LINES 6
INT iCurrentJoeBarnLine = 0
STRING sJoeBarnLines[NUM_JOE_BARN_LINES]

CONST_INT CHASE_LINE_TIME 20000
CONST_FLOAT CHASE_LINE_DIST 75.0

CONST_INT NUM_CHASE_LINES 5
INT iCurrentChaseLine = 0
STRING sChaseLines[NUM_CHASE_LINES]

CONST_INT NUM_JOE_CONVS 3
INT iCurrentJoeConv = 0
STRING sJoeConvs[NUM_JOE_CONVS]

CONST_INT NUM_JOSEF_CONVS 3
INT iCurrentJosefConv = 0
//STRING sJosefConvs[NUM_JOSEF_CONVS]

INT iMexicansFleeTimer = 0

BOOL bSetJoesCheckpoint = TRUE
INT iJoesCheckpointTimer = 0
CONST_INT JOES_CHECKPOINT_TIME 5000

// broken down car scene variables
VEHICLE_INDEX viBrokenCar
MODEL_NAMES mnBrokenCar = TORNADO3
VECTOR vBrokenCar = << 57.00, 6463.62, 31.11 >>
FLOAT fBrokenCar = 134.03
PED_INDEX piBrokenCarDriver
MODEL_NAMES mnBrokenCarDriver = A_M_O_SALTON_01
VECTOR vBrokenCarDriver = << 55.269218,6461.805176,31.394144 >>
FLOAT fBrokenCarDriver = 310.2723
STRING sBreakdownAnim = "BREAKDOWN_IDLE_B"

VEHICLE_INDEX viTowTruck
PED_INDEX piTowTruckDriver
MODEL_NAMES mnTowTruck = TOWTRUCK
VECTOR vTowTruck = << 52.61, 6458.64, 31.37 >>
FLOAT fTowTruck = 135.10
MODEL_NAMES mnTowTruckDriver = S_M_M_AUTOSHOP_02
BOOL bTowTruckSceneSetup = FALSE

// timers
INT iJoeFleesTimer
INT iJosefAttacksTimer
INT iConversationTimer
INT iJoeBarnLineTimer = 0

CONST_INT MEX_DIALOGUE_TIME  7500
INT iMexDialogueTimer = -1
BOOL bMexDialogueTriggered = FALSE

// variables to check for mission passed stats
BOOL bJosefStunned = FALSE
BOOL bJoeStunned = FALSE
BOOL bKilledOnFarm = FALSE
TEST_POLY tpFarmArea

// variables used in triggering the Arrive At Farm audio events
BOOL bAudioReady = TRUE
INT iAudioEventTimer = 0
CONST_INT AUDIO_EVENT_WAIT_TIME 5000

// variables for dealing with the number of times the player has rammed the tractor
INT iJoeRammedCount = 0
BOOL bJoeRammedLastFrame = FALSE
FLOAT fPlayerVsJoeClosingSpeedLastFrame = 0
INT iRamTimer = 0

INT iConvFailedTimer = -1

VECTOR vCarRespotOne = <<-297.7666, 6214.2837, 30.3288>>
FLOAT fCarRespotOne = 237.7338
/*VECTOR vCarRespotTwo = <<-309.4576, 6212.4595, 30.3097>>
FLOAT fCarRespotTwo = 313.923*/
VECTOR vIntroAreaOne = <<-299.502167,6211.961914,29.450109>>
VECTOR vIntroAreaTwo =  <<-305.560822,6211.881348,32.478580>>
FLOAT fIntroAreaWidth = 4.0

BOOL bKnickedManuelsCar = FALSE

SCENARIO_BLOCKING_INDEX sbiFarmArea

//------------------------------------------------------------------------------------------------------------------------------------------------------------------
// see if two floats are within a certain range of each other
FUNC BOOL AreFloatsClose(FLOAT firstNumber, FLOAT secondNumber, FLOAT range = 0.5)
	FLOAT difference = firstNumber - secondNumber
	IF difference < 0.0
		difference = difference * -1
	ENDIF
	IF (difference < range)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Kills scripted conversations if the player enters a shop
PROC KillConversationsInShops()
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> NULL
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely removes all blips created during the mission
PROC CLEAR_ALL_BLIPS()
	SAFE_REMOVE_BLIP(mdJosef.biBlip)
	
	SAFE_REMOVE_BLIP(mdJoe.biBlip)
	
	//SAFE_REMOVE_BLIP(biFarm)
ENDPROC

/// PURPOSE:
///    Checks that the player is being given the correct reason for failing
PROC CheckFailReason()
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[MANUEL])
		IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[MANUEL])
			MissionFailReason = MFR_KILLED_MANUEL
			EXIT
		ENDIF
	ENDIF
	
	INT i = 0
	REPEAT NUM_MEXICANS i
		IF DOES_ENTITY_EXIST(piMexican[i])
			IF IS_PED_INJURED(piMexican[i])
				IF bMexDialogueTriggered
					MissionFailReason = MFR_EXECUTION
				ELSE
					MissionFailReason = MFR_KILLED_A_MEXICAN
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Safely deletes all entities created by the mission
PROC DeleteEverything()
	INT i
	
	REMOVE_RELATIONSHIP_GROUP(enemyGroup)
	
	SAFE_DELETE_PED(mdJosef.piDude)
	SAFE_DELETE_PED(mdJoe.piDude)
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[MANUEL])
	
	i = 0
	REPEAT NUM_MEXICANS i
		SAFE_DELETE_PED(piMexican[i])
	ENDREPEAT
	
	SAFE_DELETE_VEHICLE(viTractor)
	SAFE_DELETE_VEHICLE(viFarmCar)
	SAFE_DELETE_VEHICLE(viVan)
	
	SAFE_DELETE_OBJECT(oiBarnDoorLeft)
	SAFE_DELETE_OBJECT(oiBarnDoorRight)
ENDPROC

/// PURPOSE:
///    Removes all blips and releases all entities used by the mission
PROC MissionCleanup()
	INT i
	
	CLEAR_ALL_BLIPS()
	
	//SET_PED_WALLA_DENSITY(0.5, 0.0)
	
	REMOVE_RELATIONSHIP_GROUP(enemyGroup)
	
	IF IS_PED_UNINJURED(mdJosef.piDude)
		SET_ENTITY_LOAD_COLLISION_FLAG(mdJosef.piDude, FALSE)
	ENDIF
	IF IS_PED_UNINJURED(mdJoe.piDude)
		SET_ENTITY_LOAD_COLLISION_FLAG(mdJoe.piDude, FALSE)
	ENDIF
	SAFE_RELEASE_PED(mdJosef.piDude)
	SAFE_RELEASE_PED(mdJoe.piDude)
	i = 0
	REPEAT NUM_MEXICANS i
		IF DOES_ENTITY_EXIST(piMexican[i])
			IF NOT IS_PED_INJURED(piMexican[i])
				IF NOT (IsPedPerformingTask(piMexican[i], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) OR IS_PED_FLEEING(piMexican[i]))
					TASK_FOLLOW_NAV_MESH_TO_COORD(piMexican[i], vLeaveDest[i], PEDMOVEBLENDRATIO_RUN)
				ENDIF
				SET_PED_KEEP_TASK(piMexican[i], TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(piMexican[i])
			ENDIF
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT NUM_MEXICANS_2 i
		IF DOES_ENTITY_EXIST(piMexican2[i])
			IF NOT IS_PED_INJURED(piMexican2[i])
				SET_PED_KEEP_TASK(piMexican2[i], TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(piMexican2[i])
			ENDIF
		ENDIF
	ENDREPEAT
	SAFE_RELEASE_PED(piBrokenCarDriver)
	IF IS_PED_UNINJURED(piTowTruckDriver)
		SET_ENTITY_LOAD_COLLISION_FLAG(piTowTruckDriver, FALSE)
	ENDIF
	SAFE_RELEASE_PED(piTowTruckDriver)
	
	SAFE_RELEASE_VEHICLE(viTractor)
	SAFE_RELEASE_VEHICLE(viFarmCar)
	SAFE_RELEASE_VEHICLE(viVan)
	SAFE_RELEASE_VEHICLE(viBrokenCar)
	SAFE_RELEASE_VEHICLE(viTowTruck)
	
	IF DOES_ENTITY_EXIST(oiBarnDoorLeft)
		FREEZE_ENTITY_POSITION(oiBarnDoorLeft, FALSE)
	ENDIF
	IF DOES_ENTITY_EXIST(oiBarnDoorRight)
		FREEZE_ENTITY_POSITION(oiBarnDoorRight, FALSE)
	ENDIF
	SAFE_RELEASE_OBJECT(oiBarnDoorLeft)
	SAFE_RELEASE_OBJECT(oiBarnDoorRight)
	
	CLEAR_PED_NON_CREATION_AREA()
    REMOVE_SCENARIO_BLOCKING_AREA(sbiFarmArea)
    SET_PED_PATHS_BACK_TO_ORIGINAL(vFarmMin, vFarmMax)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	
	IF IS_AUDIO_SCENE_ACTIVE("MINUTE_03_SCENE")
		STOP_AUDIO_SCENE("MINUTE_03_SCENE")
	ENDIF
	
	SET_CREATE_RANDOM_COPS(TRUE)
	
	SET_GPS_DISABLED_ZONE(<<0,0,0>>, <<0,0,0>>)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	KILL_ANY_CONVERSATION()
ENDPROC

/// PURPOSE:
///    Cleans up the mission
PROC Script_Cleanup()
	
	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
		MissionCleanup()
	ENDIF
	
	CLEAR_PRINTS()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	// Delete the Character if it has been created
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	// Remove scenario blocking area
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Passes the mission and cleans everything up
PROC Script_Passed()

	IF bJosefStunned AND bJoeStunned
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIN3_STUNNED_AND_KILLED)
	ENDIF
	IF bKilledOnFarm
		CPRINTLN(DEBUG_MISSION, "Setting no migration stat now")
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIN3_KILL_BEFORE_ESCAPE)
	ENDIF
	Random_Character_Passed(CP_RAND_C_MIN3)
	Script_Cleanup()
	
ENDPROC

// ===========================================================================================================
//		DEBUG AND CREATION FUNCTIONS
// ===========================================================================================================
/// PURPOSE:
///    Removes all peds for debug skipping
PROC RemovePeds()
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[MANUEL])
		REMOVE_PED_FOR_DIALOGUE(pedConvStruct, MANUEL_ID)
		SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[MANUEL])
	ENDIF
	
	CLEAR_ALL_BLIPS()
	
	REMOVE_RELATIONSHIP_GROUP(enemyGroup)
	
	IF DOES_ENTITY_EXIST(mdJosef.piDude)
		IF NOT IS_PED_INJURED(mdJosef.piDude)
			SET_ENTITY_LOAD_COLLISION_FLAG(mdJosef.piDude, FALSE)
		ENDIF
		REMOVE_PED_FOR_DIALOGUE(pedConvStruct, JOSEF_ID)
		SAFE_DELETE_PED(mdJosef.piDude)
	ENDIF
	IF DOES_ENTITY_EXIST(mdJoe.piDude)
		IF NOT IS_PED_INJURED(mdJoe.piDude)
			SET_ENTITY_LOAD_COLLISION_FLAG(mdJoe.piDude, FALSE)
		ENDIF
		REMOVE_PED_FOR_DIALOGUE(pedConvStruct, JOE_ID)
		SAFE_DELETE_PED(mdJoe.piDude)
	ENDIF
	
	INT i = 0
	REPEAT NUM_MEXICANS i
		SAFE_DELETE_PED(piMexican[i])
	ENDREPEAT
	
	SAFE_DELETE_PED(piBrokenCarDriver)
	IF IS_PED_UNINJURED(piTowTruckDriver)
		SET_ENTITY_LOAD_COLLISION_FLAG(piTowTruckDriver, FALSE)
	ENDIF
	SAFE_DELETE_PED(piTowTruckDriver)
	
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[PLAYER_CAR])
	SAFE_DELETE_VEHICLE(viTractor)
	SAFE_DELETE_VEHICLE(viFarmCar)
	SAFE_DELETE_VEHICLE(viVan)
	SAFE_DELETE_VEHICLE(viBrokenCar)
	SAFE_DELETE_VEHICLE(viTowTruck)
	
	SAFE_DELETE_OBJECT(oiBarnDoorLeft)
	SAFE_DELETE_OBJECT(oiBarnDoorRight)
ENDPROC

/// PURPOSE:
///    Resets the values of variables to what they should be at the mission start
PROC ResetVariables()
	iConversationTimer = GET_GAME_TIMER() + DRIVE_LINE_TIME
	iCurrentDriveLine = 0
	iCurrentJoeBarnLine = 0
	iCurrentChaseLine = 0
	iCurrentJoeConv = 0
	iCurrentJosefConv = 0
	bAllowOneDeadMessage = TRUE
	bTyresBurst = FALSE
	bJackedJoeAndJosef = FALSE
	bStartAiDriving = TRUE
	iTyresBurstTimer = 0
	bFleeTalkReady[0] = TRUE
	bFleeTalkReady[1] = TRUE//FALSE
	bFleeTalkReady[2] = TRUE
	bFleeTalkReady[3] = TRUE//FALSE
	bFleeTalkReady[4] = TRUE
	bJosefStunned = FALSE
	bJoeStunned = FALSE
	bKilledOnFarm = FALSE
	bVanSetup = FALSE
	bFarmCarSetup = FALSE
	bTractorSetup = FALSE
	bBarnDoorLeftSetup = FALSE
	bBarnDoorRightSetup = FALSE
	bTowTruckSceneSetup = FALSE
	iMexDialogueTimer = -1
	bMexDialogueTriggered = FALSE
	bNotAimedAtManuel = TRUE
	bAudioReady = TRUE
	iManuelStoppedTimer = -1
	iManuelFollowTimer = -1
	iManuelBumpTimer = -1
ENDPROC

/// PURPOSE:
///    Fills the arrays containing the Mexicans' start points, headings and destinations
PROC PopulateMexicanPositionArray()
	vMexicanPos[0] = <<433.290741,6508.463867,28.314806>>
	fMexicanHeading[0] = 81.59
	vLeaveDest[0] = << 444.1198, 6489.0840, 28.5351 >>
	vMexicanPos[1] = <<434.631592,6511.008300,28.394669>>
	fMexicanHeading[1] = 67.03
	vLeaveDest[1] = << 445.7498, 6483.2534, 28.6280 >>
	vMexicanPos[2] = <<435.393097,6507.186035,28.420036>>
	fMexicanHeading[2] = 50.1
	vLeaveDest[2] = << 445.4445, 6521.4688, 27.8593 >>
	vMexicanPos[3] = <<436.226715,6509.413086,28.451788>>
	fMexicanHeading[3] = 55.00
	vLeaveDest[3] = << 442.8672, 6527.3452, 27.4831 >>
	vMexicanPos[4] = <<436.508789,6511.723145,28.522154>>
	fMexicanHeading[4] = 48.2
	vLeaveDest[4] = << 440.3789, 6533.7314, 27.1066 >>
	
	vMexicanPos2[0] = <<435.5727, 6465.7324, 27.7508>>
	fMexicanHeading2[0] = 47.74
	//vLeaveDest2[0] = << 441.0882, 6479.7251, 27.9188 >>
	vMexicanPos2[1] = <<436.2014, 6464.4033, 27.7476>>
	fMexicanHeading2[1] = 50.1
	//vLeaveDest2[1] = << 423.7309, 6512.6343, 26.6510 >>
	vMexicanPos2[2] = <<436.5488, 6461.7852, 27.7461>>
	fMexicanHeading2[2] = 48.2
	//vLeaveDest2[2] = << 400.0897, 6495.6104, 26.9440 >>
ENDPROC

PROC LoadBrokenCarModels()
	REQUEST_LOAD_MODEL(mnBrokenCar)
	REQUEST_LOAD_MODEL(mnBrokenCarDriver)
ENDPROC

/// PURPOSE:
///    Loads all ped models for the game
PROC LoadModels()
	REQUEST_LOAD_MODEL(mnMexicanFemale)
	REQUEST_LOAD_MODEL(mnMexicanMale)
	REQUEST_LOAD_MODEL(GET_NPC_PED_MODEL(eclJosef))
	REQUEST_LOAD_MODEL(GET_NPC_PED_MODEL(eclJoe))
	REQUEST_LOAD_MODEL(mnTractor)
	REQUEST_LOAD_MODEL(mnFarmCar)
	//REQUEST_LOAD_MODEL(mnVan)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(mnTractor, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(mnFarmCar, TRUE)
	//SET_VEHICLE_MODEL_IS_SUPPRESSED(mnVan, TRUE)
	REQUEST_LOAD_MODEL(mnBarnDoorLeft)
	REQUEST_LOAD_MODEL(mnBarnDoorRight)
	ADD_RELATIONSHIP_GROUP("ENEMIES", enemyGroup)	// add this here so it's only created once
ENDPROC

/// PURPOSE:
///    Loads and plays the intro
PROC PlayIntro()
	RC_REQUEST_CUTSCENE("mmb_3_rcm")
			
	IF RC_IS_CUTSCENE_OK_TO_START()
		
		PopulateMexicanPositionArray()
		
		//LoadModels()
		
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
			REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[MANUEL], "Manuel", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ADD_PED_FOR_DIALOGUE(pedConvStruct, MANUEL_ID, sRCLauncherDataLocal.pedID[MANUEL], "MANUEL", TRUE)
			TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[MANUEL])
		ENDIF
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ADD_PED_FOR_DIALOGUE(pedConvStruct, TREVOR_ID, PLAYER_PED_ID(), "TREVOR", TRUE)
			
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
				oiPistol = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
				oiPistol = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
				oiPistol = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, FALSE)
				oiPistol = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
			ENDIF
			
			IF DOES_ENTITY_EXIST(oiPistol)
				REGISTER_ENTITY_FOR_CUTSCENE(oiPistol, "Trevors_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[MANUEL])
			REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.ObjID[MANUEL], "MMB_Chair", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		ENDIF
		
		SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)

		// Cleanup launcher which will remove lead-in blip
		RC_CLEANUP_LAUNCHER()

		REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)

		// Start mocap scene
		START_CUTSCENE()
		WAIT(0)
		/*IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[MANUEL])
			SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[MANUEL], FALSE)
		ENDIF*/
		SAFE_DELETE_OBJECT(sRCLauncherDataLocal.ObjID[1])
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaOne, vIntroAreaTwo, fIntroAreaWidth, vCarRespotOne, fCarRespotOne, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
		RC_START_CUTSCENE_MODE(<<-303.82,6211.29,31.05>>)
		/*VEHICLE_INDEX viPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF IS_VEHICLE_OK(viPlayer) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
			IF IS_ENTITY_IN_RANGE_ENTITY(viPlayer, sRCLauncherDataLocal.pedID[MANUEL], 2)//5)
				FLOAT fDistOne = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCarRespotOne)
				FLOAT fDistTwo = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCarRespotTwo)
				IF fDistOne < fDistTwo
					SET_ENTITY_COORDS_GROUNDED(viPlayer, vCarRespotOne)
					SET_ENTITY_HEADING(viPlayer, fCarRespotOne)
				ELSE
					SET_ENTITY_COORDS_GROUNDED(viPlayer, vCarRespotTwo)
					SET_ENTITY_HEADING(viPlayer, fCarRespotTwo)
				ENDIF
			ENDIF
		ENDIF*/
		
		CPRINTLN(DEBUG_MISSION, "Start INTRO_PLAYING")
		missionStage = MS_INTRO_PLAYING
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears everything up and restarts the mission
PROC RESTART_MISSION()
	RC_START_Z_SKIP()
	SET_ENTITY_COORDS(PLAYER_PED_ID(), << -286.8000, 6198.2002, 30.4470 >>)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 39.4)
	
	RemovePeds()
	WHILE NOT CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[MANUEL], CHAR_MANUEL, <<-293.0, 6207.0, 31.0>> , 216.0)
		WAIT(0)
	ENDWHILE
	SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[0], ANCHOR_HEAD, 0)
	
	MODEL_NAMES carModel = SABREGT
	REQUEST_MODEL(carModel)
	WHILE NOT HAS_MODEL_LOADED(carModel)
		WAIT(0)
	ENDWHILE
	// Release/delete vehicle if it already exists...
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[PLAYER_CAR])
	// Create vehicle
	sRCLauncherDataLocal.vehID[PLAYER_CAR] = CREATE_VEHICLE(carModel, <<-281.93,6198.32,30.77>>, -135.32)
	SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[PLAYER_CAR], 1)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(carModel, TRUE)
	
	MODEL_NAMES truckModel = BISON
	REQUEST_MODEL(truckModel)
	WHILE NOT HAS_MODEL_LOADED(truckModel)
		WAIT(0)
	ENDWHILE
	// Release/delete vehicle if it already exists...
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[MANUEL_CAR])
	// Create vehicle
	sRCLauncherDataLocal.vehID[MANUEL_CAR] = CREATE_VEHICLE(truckModel, <<-291.72, 6208.33, 30.87>>, -134.501633)
	SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[MANUEL_CAR], 0)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(truckModel, TRUE)
	
	WAIT(500)
	
	ResetVariables()
	
	REQUEST_CUTSCENE("mmb_3_rcm")
	WHILE NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
		WAIT(0)
	ENDWHILE
	PlayIntro()
ENDPROC

/// PURPOSE:
///    Loads the tow truck and driver models, these aren't needed until the car chase starts. Deletes the existing broken car and driver so they can be recreated
PROC LoadTowTruckModels()
	REQUEST_LOAD_MODEL(mnTowTruck)
	REQUEST_LOAD_MODEL(mnTowTruckDriver)
	REQUEST_LOAD_MODEL(mnBrokenCar)
	REQUEST_LOAD_MODEL(mnBrokenCarDriver)
	SAFE_DELETE_PED(piBrokenCarDriver)
	SAFE_DELETE_VEHICLE(viBrokenCar)
ENDPROC

/// PURPOSE:
///    creates the broken down car
PROC SpawnBrokenCar()
	IF DOES_ENTITY_EXIST(viBrokenCar)
		IF IS_VEHICLE_OK(viBrokenCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBrokenCar, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCar)
			SET_VEHICLE_DOOR_OPEN(viBrokenCar, SC_DOOR_BONNET)
			SET_VEHICLE_COLOUR_COMBINATION(viBrokenCar, 1)
			SET_VEHICLE_ENGINE_HEALTH(viBrokenCar, -1.0)
			SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viBrokenCar, TRUE)
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBrokenCar)
			viBrokenCar = CREATE_VEHICLE(mnBrokenCar, vBrokenCar, fBrokenCar)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(piBrokenCarDriver)
		IF IS_PED_UNINJURED(piBrokenCarDriver) AND NOT IsPedPerformingTask(piBrokenCarDriver, SCRIPT_TASK_PLAY_ANIM)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCarDriver)
			SET_PED_CAN_BE_TARGETTED(piBrokenCarDriver, FALSE)
			TASK_PLAY_ANIM(piBrokenCarDriver, ANIM_DICT_GLANCES, sBreakdownAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBrokenCarDriver) AND HAS_ANIM_DICT_LOADED(ANIM_DICT_GLANCES)
			piBrokenCarDriver = CREATE_PED(PEDTYPE_CIVMALE, mnBrokenCarDriver, vBrokenCarDriver, fBrokenCarDriver)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns the tow truck and attaches it to the broken down car, makes them drive off when the player is close enough
PROC SpawnTowTruck()
	IF IS_VEHICLE_OK(viTowTruck)
		IF NOT DOES_ENTITY_EXIST(piBrokenCarDriver)
			IF HAS_MODEL_LOADED(mnBrokenCarDriver)
				piBrokenCarDriver = CREATE_PED_INSIDE_VEHICLE(viTowTruck, PEDTYPE_CIVMALE, mnBrokenCarDriver, VS_FRONT_RIGHT)
				SET_PED_CAN_BE_TARGETTED(piBrokenCarDriver, FALSE)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(piTowTruckDriver)
			IF IS_VEHICLE_OK(viBrokenCar)
				IF bTowTruckSceneSetup
					// if player is close to the truck, start driving
					IF (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), viTowTruck) < 100.0)
						IF IS_PED_UNINJURED(piTowTruckDriver) AND NOT IsPedPerformingTask(piTowTruckDriver, SCRIPT_TASK_PERFORM_SEQUENCE)
							FREEZE_ENTITY_POSITION(viTowTruck, FALSE)
							FREEZE_ENTITY_POSITION(viBrokenCar, FALSE)
							SET_PED_CAN_BE_TARGETTED(piTowTruckDriver, FALSE)
							ATTACH_VEHICLE_TO_TOW_TRUCK(viTowTruck, viBrokenCar, -1, <<0,1.5,0>>)
							SET_VEHICLE_TOW_TRUCK_ARM_POSITION(viTowTruck, 0.5)
							SEQUENCE_INDEX siDrive
							OPEN_SEQUENCE_TASK(siDrive)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, viTowTruck, << 41.5323, 6429.8125, 30.3523 >>, 10, DRIVINGSTYLE_NORMAL, mnTowTruck, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS | DF_ForceStraightLine, 4.0, 10.0)
								TASK_VEHICLE_DRIVE_WANDER(null, viTowTruck, 10.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
							CLOSE_SEQUENCE_TASK(siDrive)
							TASK_PERFORM_SEQUENCE(piTowTruckDriver, siDrive)
							CLEAR_SEQUENCE_TASK(siDrive)
						ENDIF
					ENDIF
				ELSE
					// Everything is created, finish the scene
					// Set the tow truck's values
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mnTowTruck, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(viTowTruck, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnTowTruck)
					SET_VEHICLE_TOW_TRUCK_ARM_POSITION(viTowTruck, 0.0)
					FREEZE_ENTITY_POSITION(viTowTruck, TRUE)
					
					// Set the broken car's values
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBrokenCar, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCar)
					SET_VEHICLE_COLOUR_COMBINATION(viBrokenCar, 1)
					SET_VEHICLE_DOORS_LOCKED(viBrokenCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(viBrokenCar, TRUE)
					FREEZE_ENTITY_POSITION(viBrokenCar, TRUE)
					
					// remove the ped models
					SET_MODEL_AS_NO_LONGER_NEEDED(mnTowTruckDriver)
					SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCarDriver)
					
					bTowTruckSceneSetup = TRUE
				ENDIF
			ELSE
				IF HAS_MODEL_LOADED(mnBrokenCar)
					viBrokenCar = CREATE_VEHICLE(mnBrokenCar, vBrokenCar, fBrokenCar)
				ENDIF
			ENDIF
		ELSE
			IF HAS_MODEL_LOADED(mnTowTruckDriver)
				piTowTruckDriver = CREATE_PED_INSIDE_VEHICLE(viTowTruck, PEDTYPE_CIVMALE, mnTowTruckDriver)
			ENDIF
		ENDIF
	ELSE
		LoadTowTruckModels()
		IF HAS_MODEL_LOADED(mnTowTruck)
			viTowTruck = CREATE_VEHICLE(mnTowTruck, vTowTruck, fTowTruck)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates all the peds, vehicles and objects for the farm scene and gives them tasks
PROC Setup_Farm()
	// create the mexicans
	INT i = 0
	REPEAT NUM_MEXICANS i
		IF IS_PED_UNINJURED(piMexican[i])
			IF NOT IsPedPerformingTask(piMexican[i], SCRIPT_TASK_HANDS_UP) AND NOT IsPedPerformingTask(piMexican[i], SCRIPT_TASK_COWER)
				SET_ENTITY_COORDS_NO_OFFSET(piMexican[i], vMexicanPos[i])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piMexican[i], TRUE)
				SET_PED_PATH_CAN_USE_LADDERS(piMexican[i], FALSE)
				// prevent the player auto aiming at the mexicans
				SET_PED_CAN_BE_TARGETTED(piMexican[i], FALSE)
				SET_PED_FLEE_ATTRIBUTES(piMexican[i], FA_CAN_SCREAM, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piMexican[i], TRUE)
				// make some Mexicans cower, some have their hands up
				IF i % 2 = 0
					TASK_HANDS_UP(piMexican[i], -1)
				ELSE
					TASK_COWER(piMexican[i])
				ENDIF
			ENDIF
		ELSE
			IF HAS_MODEL_LOADED(mnMexicanFemale) AND HAS_MODEL_LOADED(mnMexicanMale)
				IF i < 2
					// Mexicans 0 and 1 are female
					piMexican[i] = CREATE_PED(PEDTYPE_CIVFEMALE, mnMexicanFemale, vMexicanPos[i], fMexicanHeading[i])
				ELSE
					// Mexicans 2, 3 and 4 are male
					piMexican[i] = CREATE_PED(PEDTYPE_CIVMALE, mnMexicanMale, vMexicanPos[i], fMexicanHeading[i])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// create Josef, give him a shotgun and make him aim at the Mexicans
	IF HAS_ANIM_DICT_LOADED(ANIM_DICT_GLANCES) AND HAS_ANIM_DICT_LOADED(ANIM_DICT_PANIC)
		SEQUENCE_INDEX siGunAnims
		IF DOES_ENTITY_EXIST(mdJosef.piDude)
			IF NOT IS_PED_INJURED(mdJosef.piDude)
				IF NOT IsPedPerformingTask(mdJosef.piDude, SCRIPT_TASK_PERFORM_SEQUENCE)
					SET_ENTITY_LOAD_COLLISION_FLAG(mdJosef.piDude, TRUE)
					GIVE_WEAPON_TO_PED(mdJosef.piDude, WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
					SET_PED_ACCURACY(mdJosef.piDude, 25)
					OPEN_SEQUENCE_TASK(siGunAnims)
						TASK_PLAY_ANIM(NULL, ANIM_DICT_GLANCES, "0")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_GLANCES, "180")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_PANIC, "-90")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_GLANCES, "90")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_PANIC, "90")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_GLANCES, "-90")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_PANIC, "180")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_GLANCES, "-180")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_PANIC, "0")
						TASK_PLAY_ANIM(NULL, ANIM_DICT_PANIC, "-180")
						SET_SEQUENCE_TO_REPEAT(siGunAnims, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(siGunAnims)
					TASK_PERFORM_SEQUENCE(mdJosef.piDude, siGunAnims)
					CLEAR_SEQUENCE_TASK(siGunAnims)
					ADD_PED_FOR_DIALOGUE(pedConvStruct, JOSEF_ID, mdJosef.piDude, "JOSEF", TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(mdJosef.piDude, vAltJosefPos)
					SET_PED_SPHERE_DEFENSIVE_AREA(mdJosef.piDude, vAltJosefPos, 25.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mdJosef.piDude, TRUE)
					SET_PED_CONFIG_FLAG(mdJosef.piDude, PCF_GetOutBurningVehicle, FALSE)		//stop ped auto leaving undriveable vehicle
					SET_PED_CONFIG_FLAG(mdJosef.piDude, PCF_GetOutUndriveableVehicle, FALSE)		//stop ped auto leaving undriveable vehicle
					SET_PED_CONFIG_FLAG(mdJosef.piDude, PCF_PedsJackingMeDontGetIn, TRUE)	// player won't get in the van if they jack Josef
					
					SET_PED_PROP_INDEX(mdJosef.piDude, ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(mdJosef.piDude, ANCHOR_EYES, 0)
					
					REQUEST_WAYPOINT_RECORDING(sWaypointRecMain)
					REQUEST_WAYPOINT_RECORDING(sWaypointRecAlt)
				ENDIF
			ENDIF
		ELSE
			IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(eclJosef))
				mdJosef.piDude = CREATE_PED(PEDTYPE_MISSION, GET_NPC_PED_MODEL(eclJosef), vAltJosefPos, fAltJosefHeading)
			ENDIF
		ENDIF
	ENDIF
	
	// create vehicles
	/*IF IS_VEHICLE_OK(viTractor)
		IF NOT bTractorSetup
			SET_VEHICLE_ON_GROUND_PROPERLY(viTractor)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnTractor, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnTractor)
			bTractorSetup = TRUE
		ENDIF
	ELSE*/
	IF NOT IS_VEHICLE_OK(viTractor)
		IF HAS_MODEL_LOADED(mnTractor)
			viTractor = CREATE_VEHICLE(mnTractor, vTractorPos, fTractorHeading)
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(viFarmCar)
		IF NOT bFarmCarSetup
			SET_VEHICLE_ON_GROUND_PROPERLY(viFarmCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnFarmCar, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnFarmCar)
			bFarmCarSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnFarmCar)
			viFarmCar = CREATE_VEHICLE(mnFarmCar, vAltFarmCarPos, fAltFarmCarHeading)
		ENDIF
	ENDIF
	
	// create the barn doors
	IF DOES_ENTITY_EXIST(oiBarnDoorLeft)
		IF NOT bBarnDoorRightSetup
			SET_ENTITY_HEADING(oiBarnDoorLeft, DOOR_LEFT_HEADING_ALT)
			FREEZE_ENTITY_POSITION(oiBarnDoorLeft, TRUE)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorLeft, <<424.32, 6477.74, 30.79>>, TRUE, 0.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBarnDoorLeft)
			bBarnDoorRightSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBarnDoorLeft)
			oiBarnDoorLeft = CREATE_OBJECT_NO_OFFSET(mnBarnDoorLeft, <<424.32, 6477.74, 30.79>>)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(oiBarnDoorRight)
		IF NOT bBarnDoorLeftSetup
			SET_ENTITY_HEADING(oiBarnDoorRight, DOOR_RIGHT_HEADING_ALT)
			FREEZE_ENTITY_POSITION(oiBarnDoorRight, TRUE)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorRight, << 419.30, 6471.71, 30.74>>, TRUE, 0.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBarnDoorRight)
			bBarnDoorLeftSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBarnDoorRight)
			oiBarnDoorRight = CREATE_OBJECT_NO_OFFSET(mnBarnDoorRight, << 419.30, 6471.71, 30.74>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up Joe fleeing from the barn in the van
PROC Setup_Farm_Two()
	IF IS_VEHICLE_OK(viVan) AND IS_VEHICLE_OK(viTractor)
		IF NOT bVanSetup AND NOT bTractorSetup
			/*SET_VEHICLE_MODEL_IS_SUPPRESSED(mnVan, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnVan)*/
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnTractor, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnTractor)
			
			SET_VEHICLE_ON_GROUND_PROPERLY(viTractor)
			
			SET_ENTITY_COORDS(viVan, vRecordingStartAlt)
			SET_ENTITY_QUATERNION(viVan, fRecQuaternionXAlt, fRecQuaternionYAlt, fRecQuaternionZAlt, fRecQuaternionWAlt)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(viVan, TRUE)
			SET_VEHICLE_ENGINE_ON(viVan, TRUE, TRUE)
			SET_VEHICLE_DOOR_OPEN(viVan, SC_DOOR_FRONT_RIGHT)
			bTractorSetup = TRUE
			bVanSetup = TRUE
		ENDIF
	ENDIF
	IF NOT IS_VEHICLE_OK(viVan)
		/*IF HAS_MODEL_LOADED(mnVan)
			viVan = CREATE_VEHICLE(mnVan, vAltVanPos, fAltVanHeading)
		ENDIF*/
		IF HAS_MODEL_LOADED(mnTractor)
			viVan = CREATE_VEHICLE(mnTractor, vAltVanPos, fAltVanHeading)
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mdJoe.piDude)
		IF NOT IsPedPerformingTask(mdJoe.piDude, SCRIPT_TASK_AIM_GUN_AT_COORD)
			GIVE_WEAPON_TO_PED(mdJoe.piDude, WEAPONTYPE_SAWNOFFSHOTGUN, -1, TRUE)
			TASK_AIM_GUN_AT_COORD(mdJoe.piDude, vMexicanPos2[0], -1)
			SET_ENTITY_LOAD_COLLISION_FLAG(mdJoe.piDude, TRUE)
			//ADD_PED_FOR_DIALOGUE(pedConvStruct, JOE_ID, mdJoe.piDude, "JOE", TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mdJoe.piDude, TRUE)
			SET_PED_CONFIG_FLAG(mdJoe.piDude, PCF_GetOutBurningVehicle, FALSE)		//stop ped auto leaving undriveable vehicle
			SET_PED_CONFIG_FLAG(mdJoe.piDude, PCF_GetOutUndriveableVehicle, FALSE)		//stop ped auto leaving undriveable vehicle
			SET_PED_CONFIG_FLAG(mdJoe.piDude, PCF_PedsJackingMeDontGetIn, TRUE)	// player won't get in the van if they jack Joe
			SET_PED_SUFFERS_CRITICAL_HITS(mdJoe.piDude, FALSE)
			
			SET_PED_PROP_INDEX(mdJoe.piDude, ANCHOR_EYES, 0)
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(eclJoe))
			mdJoe.piDude = CREATE_PED(PEDTYPE_MISSION, GET_NPC_PED_MODEL(eclJoe), vAltJoePos, fAltJoeHeading)
		ENDIF
	ENDIF
	
	INT i = 0
	INT numReady = 0
	REPEAT NUM_MEXICANS_2 i
		IF IS_PED_UNINJURED(piMexican2[i])
			numReady++
			IF NOT IsPedPerformingTask(piMexican2[i], SCRIPT_TASK_HANDS_UP) AND NOT IsPedPerformingTask(piMexican2[i], SCRIPT_TASK_COWER)
				SET_ENTITY_COORDS_NO_OFFSET(piMexican2[i], vMexicanPos2[i])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piMexican2[i], TRUE)
				SET_PED_PATH_CAN_USE_LADDERS(piMexican2[i], FALSE)
				// prevent the player auto aiming at the mexicans
				SET_PED_CAN_BE_TARGETTED(piMexican2[i], FALSE)
				SET_PED_FLEE_ATTRIBUTES(piMexican2[i], FA_CAN_SCREAM, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piMexican2[i], TRUE)
				// make some Mexicans cower, some have their hands up
				IF i % 2 = 0
					TASK_HANDS_UP(piMexican2[i], -1)
				ELSE
					TASK_COWER(piMexican2[i])
				ENDIF
			ENDIF
		ELSE
			IF HAS_MODEL_LOADED(mnMexicanFemale) AND HAS_MODEL_LOADED(mnMexicanMale)
				IF i < 2
					// Mexicans 0 and 1 are female
					piMexican2[i] = CREATE_PED(PEDTYPE_CIVFEMALE, mnMexicanFemale, vMexicanPos2[i], fMexicanHeading2[i])
				ELSE
					// Mexicans 2, 3 and 4 are male
					piMexican2[i] = CREATE_PED(PEDTYPE_CIVMALE, mnMexicanMale, vMexicanPos2[i], fMexicanHeading2[i])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	IF numReady = NUM_MEXICANS_2
		SET_MODEL_AS_NO_LONGER_NEEDED(mnMexicanFemale)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnMexicanMale)
	ENDIF
	
	IF IS_VEHICLE_OK(viFarmCar)
		IF NOT bFarmCarSetup
			SET_VEHICLE_ON_GROUND_PROPERLY(viFarmCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnFarmCar, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnFarmCar)
			REQUEST_WAYPOINT_RECORDING(sWaypointRecMain)
			REQUEST_WAYPOINT_RECORDING(sWaypointRecAlt)
			bFarmCarSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnFarmCar)
			viFarmCar = CREATE_VEHICLE(mnFarmCar, vAltFarmCarPos, fAltFarmCarHeading)
		ENDIF
	ENDIF
	
	// create the barn doors, just in case they hadn't spawned in during the first setup function
	IF DOES_ENTITY_EXIST(oiBarnDoorLeft)
		IF NOT bBarnDoorLeftSetup
			SET_ENTITY_HEADING(oiBarnDoorLeft, DOOR_LEFT_HEADING_ALT)
			FREEZE_ENTITY_POSITION(oiBarnDoorLeft, TRUE)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorLeft, <<424.32, 6477.74, 30.79>>, TRUE, 0.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBarnDoorLeft)
			bBarnDoorLeftSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBarnDoorLeft)
			oiBarnDoorLeft = CREATE_OBJECT_NO_OFFSET(mnBarnDoorLeft, <<424.32, 6477.74, 30.79>>)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(oiBarnDoorRight)
		IF NOT bBarnDoorRightSetup
			SET_ENTITY_HEADING(oiBarnDoorRight, DOOR_RIGHT_HEADING_ALT)
			FREEZE_ENTITY_POSITION(oiBarnDoorRight, TRUE)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorRight, << 419.30, 6471.71, 30.74>>, TRUE, 0.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnBarnDoorRight)
			bBarnDoorRightSetup = TRUE
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnBarnDoorRight)
			oiBarnDoorRight = CREATE_OBJECT_NO_OFFSET(mnBarnDoorRight, << 419.30, 6471.71, 30.74>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the player more ammo when they start from a checkpoint
PROC GivePlayerAmmoForCheckpoint()
	AMMO_TYPE eAmmoType
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
		eAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
		IF GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType) < 12
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)
		ENDIF
	ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
		eAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
		IF GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType) < 12
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)
		ENDIF
	ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
		eAmmoType = GET_PED_AMMO_TYPE_FROM_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
		IF GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), eAmmoType) < 12
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)
		ENDIF
	ELSE
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    restarts the mission after the Intro
PROC AfterMocapCheckpoint()
	RC_START_Z_SKIP()
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	PopulateMexicanPositionArray()
	//LoadModels()
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
		ADD_PED_FOR_DIALOGUE(pedConvStruct, MANUEL_ID, sRCLauncherDataLocal.pedID[MANUEL], "MANUEL", TRUE)
		SET_ENTITY_COORDS_GROUNDED(sRCLauncherDataLocal.pedID[MANUEL], vManuelAfterCutscene)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[MANUEL], fManuelAfterCutscne)
	ENDIF
	
	GivePlayerAmmoForCheckpoint()
	
	ADD_PED_FOR_DIALOGUE(pedConvStruct, TREVOR_ID, PLAYER_PED_ID(), "TREVOR", TRUE)
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), << -303.3698, 6212.3550, 30.4696 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 229.2303)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-297.987488,6193.311523,28.874229>>, <<-274.159363,6209.037598,32.357018>>, FALSE)
	DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
	CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
	
	CPRINTLN(DEBUG_MISSION, "Start INTRO_PLAYING")
	missionStage = MS_INTRO_PLAYING
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
		/*SET_ENTITY_COLLISION(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
		FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.pedID[MANUEL], FALSE)*/
		SEQUENCE_INDEX siManuel
		OPEN_SEQUENCE_TASK(siManuel)
			//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[MANUEL_CAR]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[MANUEL_CAR])
				TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK)
				TASK_VEHICLE_DRIVE_WANDER(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], 20, DRIVINGMODE_AVOIDCARS)
			ELSE
				TASK_WANDER_STANDARD(NULL)
			ENDIF
		CLOSE_SEQUENCE_TASK(siManuel)
		TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MANUEL], siManuel)
		CLEAR_SEQUENCE_TASK(siManuel)
	ENDIF
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[MANUEL])
		SET_ENTITY_COLLISION(sRCLauncherDataLocal.objID[MANUEL], TRUE)
		FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.objID[MANUEL], FALSE)
	ENDIF
	
	IF IS_REPLAY_BEING_SET_UP()
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, << -283.27, 6216.75, 31.84 >>, 43.56, FALSE, FALSE, TRUE, TRUE, TRUE, SABREGT)
		END_REPLAY_SETUP()
	ENDIF
	
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Teleports the player to a position and gives them a vehicle if they don't have one
/// PARAMS:
///    vNewPos - The position to teleport the player to
///    fNewHeading - The heading to give the player
PROC SetPlayerPosWithVehicle(VECTOR vNewPos, FLOAT fNewHeading)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vNewPos)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fNewHeading)
	ELSE
		VEHICLE_INDEX viPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF IS_VEHICLE_OK(viPlayer)
			SET_ENTITY_AS_MISSION_ENTITY(viPlayer, TRUE, TRUE)
		ENDIF
		SAFE_DELETE_VEHICLE(viPlayer)
		
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, vNewPos, fNewHeading, TRUE, FALSE, TRUE, TRUE, TRUE, SABREGT)
		SAFE_RELEASE_VEHICLE(viTemp)
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
ENDPROC

/// PURPOSE:
///    Restarts the mission
PROC DebugSkipRestart()
	RESTART_MISSION()
	RC_END_Z_SKIP(TRUE)
ENDPROC

/// PURPOSE:
///    Checks if the models have loaded
/// RETURNS:
///    TRUE if the models have loaded
FUNC BOOL HaveModelsLoaded()
	IF HAS_MODEL_LOADED(mnMexicanFemale)
	AND HAS_MODEL_LOADED(mnMexicanMale)
	AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(eclJosef))
	AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(eclJoe))
	AND HAS_MODEL_LOADED(mnTractor)
	AND HAS_MODEL_LOADED(mnFarmCar)
	//AND HAS_MODEL_LOADED(mnVan)
	AND HAS_MODEL_LOADED(mnBarnDoorLeft)
	AND HAS_MODEL_LOADED(mnBarnDoorRight)
	AND HAS_MODEL_LOADED(mnBrokenCar)
	AND HAS_MODEL_LOADED(mnBrokenCarDriver)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the first half of the farm scene has been set up
/// RETURNS:
///    TRUE if the first half of the farm scene has been set up
FUNC BOOL IsFarmSetup()
	IF IS_PED_UNINJURED(mdJosef.piDude) AND IS_VEHICLE_OK(viTractor) AND IS_VEHICLE_OK(viFarmCar) AND IS_PED_UNINJURED(piMexican[0]) AND IS_PED_UNINJURED(piMexican[1])
	AND IS_PED_UNINJURED(piMexican[2]) AND IS_PED_UNINJURED(piMexican[3])AND IS_PED_UNINJURED(piMexican[4]) AND DOES_ENTITY_EXIST(oiBarnDoorLeft) AND DOES_ENTITY_EXIST(oiBarnDoorRight)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the second half of the farm scene has been set up
/// RETURNS:
///    TRUE if the second half of the farm scene has been set up
FUNC BOOL IsFarmTwoSetup()
	IF IS_PED_UNINJURED(mdJoe.piDude) AND IS_VEHICLE_OK(viVan) AND IS_PED_UNINJURED(piMexican2[0]) AND IS_PED_UNINJURED(piMexican2[1])AND IS_PED_UNINJURED(piMexican2[2])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates the initial blips for Joe and Josef (uses coords because the peds don't exist yet)
PROC CreateInitialBlips()
	SAFE_REMOVE_BLIP(mdJoe.biBlip)
	mdJoe.biBlip = CREATE_COORD_BLIP(vAltJoePos, BLIPPRIORITY_MED, FALSE)
	SET_BLIP_COLOUR(mdJoe.biBlip, BLIP_COLOUR_RED)
	SET_BLIP_SCALE(mdJoe.biBlip, BLIP_SIZE_PED)
	SET_BLIP_NAME_FROM_TEXT_FILE(mdJoe.biBlip, "BLIP_ENEMY")
	
	SAFE_REMOVE_BLIP(mdJosef.biBlip)
	mdJosef.biBlip = CREATE_COORD_BLIP(vAltJosefPos, BLIPPRIORITY_MED, FALSE)
	SET_BLIP_COLOUR(mdJosef.biBlip, BLIP_COLOUR_RED)
	SET_BLIP_SCALE(mdJosef.biBlip, BLIP_SIZE_PED)
	SET_BLIP_NAME_FROM_TEXT_FILE(mdJosef.biBlip, "BLIP_ENEMY")
	SET_BLIP_ROUTE(mdJosef.biBlip, TRUE)
	
	SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(mdJoe.biBlip, <<405.69, 6557.56, 27.04>>, 266.03)
	SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(mdJosef.biBlip, <<405.69, 6557.56, 27.04>>, 266.03)
ENDPROC

/// PURPOSE:
///    Creates the actual ped blips for Joe and Josef
PROC CreateProperBlips()
	IF IS_PED_UNINJURED(mdJoe.piDude)
		SAFE_REMOVE_BLIP(mdJoe.biBlip)
		mdJoe.biBlip = CREATE_PED_BLIP(mdJoe.piDude)
		SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(mdJoe.biBlip, <<405.69, 6557.56, 27.04>>, 266.03)
	ENDIF
	
	IF IS_PED_UNINJURED(mdJosef.piDude)
		SAFE_REMOVE_BLIP(mdJosef.biBlip)
		mdJosef.biBlip = CREATE_PED_BLIP(mdJosef.piDude)
		SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(mdJosef.biBlip, <<405.69, 6557.56, 27.04>>, 266.03)
	ENDIF
ENDPROC

/// PURPOSE:
///    stops non-mission things being spawned at the farm
PROC ClearTheFarm()
	SET_PED_NON_CREATION_AREA(vFarmMin, vFarmMax)
	sbiFarmArea = ADD_SCENARIO_BLOCKING_AREA(vFarmMin, vFarmMax)
	SET_PED_PATHS_IN_AREA(vFarmMin, vFarmMax, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vFarmMin, vFarmMax, FALSE)
	CLEAR_AREA_OF_PEDS(vFarmPosition, 500)
	CLEAR_AREA_OF_VEHICLES(vFarmPosition, 500, FALSE, FALSE, TRUE, TRUE)
ENDPROC

/// PURPOSE:
///    Restarts the mission just before the Kill Josef stage
PROC DebugSkipKillJosef()
	RC_START_Z_SKIP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	RemovePeds()
	
	//VEHICLE_INDEX viTemp
	// teleport player to near the farm and get them a vehicle
	IF NOT IS_REPLAY_BEING_SET_UP()
		SetPlayerPosWithVehicle(<< 389.4927, 6554.7339, 26.6191 >>, 270.5771)
	//ELSE
	//	CREATE_VEHICLE_FOR_REPLAY(viTemp, << 389.4927, 6554.7339, 26.6191 >>, 270.5771, FALSE, FALSE, FALSE, TRUE, TRUE, SABREGT)
	ENDIF
	
	PopulateMexicanPositionArray()
	ResetVariables()
	LoadModels()
	LoadBrokenCarModels()
	WHILE NOT HaveModelsLoaded()
		WAIT(0)
	ENDWHILE
	
	Setup_Farm()
	WHILE NOT IsFarmSetup()
		Setup_Farm()
		WAIT(0)
	ENDWHILE
	Setup_Farm()
	
	Setup_Farm_Two()
	WHILE NOT IsFarmTwoSetup()
		Setup_Farm_Two()
		WAIT(0)
	ENDWHILE
	Setup_Farm_Two()
	
	GivePlayerAmmoForCheckpoint()
	
	MissionSubState = SS_UPDATE_THREE
	missionStage = MS_DRIVE_TO_FARM
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOSEF, "Kill Josef")
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	PRINT_NOW("MIN3_06", DEFAULT_GOD_TEXT_TIME, 1)	// Kill ~r~Joe ~s~and ~r~Josef.
	CreateInitialBlips()
	ClearTheFarm()
	
	IF IS_REPLAY_BEING_SET_UP()
		//VEHICLE_INDEX viTemp = GET_PLAYERS_LAST_VEHICLE()
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, << 389.4927, 6554.7339, 26.6191 >>, 270.5771, FALSE, FALSE, TRUE, TRUE, TRUE, SABREGT)
		END_REPLAY_SETUP(viTemp)
	ENDIF
	
	RC_END_Z_SKIP(TRUE)
ENDPROC

/// PURPOSE:
///    Restarts the mission just before the car chase stage
PROC DebugSkipChaseJoe()
	RC_START_Z_SKIP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	RemovePeds()
	
	//VEHICLE_INDEX viTemp
	// teleport player to near the barn
	IF NOT IS_REPLAY_BEING_SET_UP()
		SetPlayerPosWithVehicle(<< 423.6803, 6519.7100, 26.6747 >>, 170.9605)
	//ELSE
	//	CREATE_VEHICLE_FOR_REPLAY(viTemp, << 423.6803, 6519.7100, 26.6747 >>, 170.9605, FALSE, FALSE, FALSE, TRUE, TRUE, SABREGT)
	ENDIF
	
	PopulateMexicanPositionArray()
	ResetVariables()
	LoadModels()
	LoadBrokenCarModels()
	WHILE NOT HaveModelsLoaded()
		WAIT(0)
	ENDWHILE
	
	Setup_Farm_Two()
	WHILE NOT IsFarmTwoSetup()
		Setup_Farm_Two()
		WAIT(0)
	ENDWHILE
	Setup_Farm_Two()
	
	CreateProperBlips()
	
	GivePlayerAmmoForCheckpoint()
	
	MissionSubState = SS_SETUP
	missionStage = MS_SEARCH_FARM
	ClearTheFarm()
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOE, "Kill Joe", TRUE)
	
	IF IS_REPLAY_BEING_SET_UP()
		//VEHICLE_INDEX viTemp = GET_PLAYERS_LAST_VEHICLE()
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, << 423.6803, 6519.7100, 26.6747 >>, 170.9605, FALSE, FALSE, TRUE, TRUE, TRUE, SABREGT)
		END_REPLAY_SETUP(viTemp)
	ELSE
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	TRIGGER_MUSIC_EVENT("MM3_RESTART1")
	
	RC_END_Z_SKIP(TRUE)
ENDPROC

/// PURPOSE:
///    Checks for debug pass or fail and for the Z skip menu
#IF IS_DEBUG_BUILD
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
		IF missionStage <> MS_FAIL_DELAY
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				WAIT_FOR_CUTSCENE_TO_STOP()
				TRIGGER_MUSIC_EVENT("MM3_STOP")
				Script_Passed()
			ENDIF

			// Check for Fail
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				WAIT_FOR_CUTSCENE_TO_STOP()
				MissionFailReason = MFR_NONE
				MissionSubState = SS_SETUP
				missionStage = MS_FAIL_DELAY
			ENDIF
			
			INT debugJumpStage = -1
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, debugJumpStage)
				IF debugJumpStage = 0
					// restart mission
					DebugSkipRestart()
				ELIF debugJumpStage = 1
					// Skip to the Kill Josef stage
					DebugSkipKillJosef()
				ELIF debugJumpStage = 2
					// Skip to the car chase stage
					DebugSkipChaseJoe()
				ENDIF
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

// ===========================================================================================================
//		HELPER FUNCTIONS
// ===========================================================================================================

/// PURPOSE:
///    Fails the mission with an appropriate reason if a ped has been damaged
/// PARAMS:
///    testPed - The ped we want to test for failing the mission
PROC PedDamageFailControl(PED_INDEX testPed)
	IF DOES_ENTITY_EXIST(testPed)
		IF testPed = sRCLauncherDataLocal.pedID[MANUEL]
			IF IS_PED_INJURED(testPed)
				MissionFailReason = MFR_KILLED_MANUEL
			ELSE
				IF (IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID()) AND IS_PED_BEING_STEALTH_KILLED(testPed)) OR WAS_PED_KILLED_BY_TAKEDOWN(testPed)
					MissionFailReason = MFR_KILLED_MANUEL
				ELSE
					MissionFailReason = MFR_INJURED_MANUEL
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_INJURED(testPed)
				IF bMexDialogueTriggered
					MissionFailReason = MFR_EXECUTION
				ELSE
					MissionFailReason = MFR_KILLED_A_MEXICAN
				ENDIF
			ELSE
				IF (IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID()) AND IS_PED_BEING_STEALTH_KILLED(testPed)) OR WAS_PED_KILLED_BY_TAKEDOWN(testPed)
					MissionFailReason = MFR_KILLED_A_MEXICAN
				ELSE
					MissionFailReason = MFR_INJURED_A_MEXICAN
				ENDIF
			ENDIF
		ENDIF
		
		INT i = 0
		REPEAT NUM_MEXICANS i
			IF IS_PED_UNINJURED(piMexican[i])
				IF NOT IS_PED_FLEEING(piMexican[i])
					TASK_SMART_FLEE_PED(piMexican[i], PLAYER_PED_ID(), 500, -1)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF IS_PED_UNINJURED(mdJosef.piDude) AND IS_PED_UNINJURED(PLAYER_PED_ID())
			TASK_COMBAT_PED(mdJosef.piDude, PLAYER_PED_ID())
		ENDIF
		CLEAR_ALL_BLIPS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		MissionSubState = SS_SETUP
		missionStage = MS_FAIL_DELAY
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a ped has been damaged or killed, calls the fail message function if so
/// PARAMS:
///    testPed - The ped we want to test
PROC Check_Essential_Ped(PED_INDEX testPed)
	IF NOT (missionStage = MS_FAIL_DELAY)
		IF DOES_ENTITY_EXIST(testPed)
			IF IS_PED_INJURED(testPed)
				PedDamageFailControl(testPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player has attacked a mission critical ped, calls the fail message function if so
/// PARAMS:
///    testPed - The ped we want to test
PROC CheckPlayerHurtsPed(PED_INDEX testPed)
	IF NOT (missionStage = MS_FAIL_DELAY)
		IF IS_PED_UNINJURED(testPed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(testPed, PLAYER_PED_ID())
				TASK_SMART_FLEE_PED(testPed, PLAYER_PED_ID(), 500, -1)
				PedDamageFailControl(testPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is messing with Manuel's car, fails the mission if so
///    Also makes Manuel switch to wandering on foot if appropriate
PROC CheckManuelsCar()
	IF NOT (missionStage = MS_FAIL_DELAY)
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
			IF NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[MANUEL_CAR])
			OR IS_PLAYER_SHOOTING_NEAR_PED(sRCLauncherDataLocal.pedID[MANUEL], FALSE)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRCLauncherDataLocal.vehID[MANUEL_CAR], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
			OR (IS_PED_BEING_JACKED(sRCLauncherDataLocal.pedID[MANUEL]) AND IS_PED_JACKING(PLAYER_PED_ID()))
				TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MANUEL], PLAYER_PED_ID(), 500, -1)
				PedDamageFailControl(sRCLauncherDataLocal.pedID[MANUEL])
			ELSE
				IF bKnickedManuelsCar = FALSE
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[MANUEL_CAR], TRUE)
						BOOL bConversation = FALSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							bConversation = TRUE	// If he's already talking, don't bother with his car steal line
						ELIF IS_THIS_PRINT_BEING_DISPLAYED("MIN3_06")
							bConversation = CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_STEAL", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ELSE
							bConversation = CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_STEAL", CONV_PRIORITY_HIGH)
						ENDIF
						IF bConversation
							SEQUENCE_INDEX siWatchPlayer
							OPEN_SEQUENCE_TASK(siWatchPlayer)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
								TASK_WANDER_STANDARD(NULL)
							CLOSE_SEQUENCE_TASK(siWatchPlayer)
							TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MANUEL], siWatchPlayer)
							CLEAR_SEQUENCE_TASK(siWatchPlayer)
							bKnickedManuelsCar = TRUE
						ENDIF
					ELIF /*(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[MANUEL_CAR]) < 2 AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MANUEL]))
					OR*/ IS_THERE_A_CAR_BLOCKING_THIS_CAR(sRCLauncherDataLocal.vehID[MANUEL_CAR])
						TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[MANUEL])
						bKnickedManuelsCar = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers the driving to farm dialogue
PROC TriggerDriveDialogue()
	IF iCurrentDriveLine < NUM_DRIVE_LINES AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = NULL
		IF GET_GAME_TIMER() > iConversationTimer
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_DRIVE", sDriveLines[iCurrentDriveLine], CONV_PRIORITY_HIGH)
					iCurrentDriveLine++
					iConversationTimer = GET_GAME_TIMER() + DRIVE_LINE_TIME
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers lines of Joe shouting at the Mexicans in the barn when the player gets close
PROC TriggerJoeInBarnDialogue()
	IF iCurrentJoeBarnLine < NUM_JOE_BARN_LINES
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<408.370331,6485.696777,25.787706>>, <<452.994080,6449.248047,33.708237>>, 50.000000)
			IF IS_PED_UNINJURED(mdJoe.piDude) AND GET_GAME_TIMER() > iJoeBarnLineTimer
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(mdJoe.piDude, sJoeBarnLines[iCurrentJoeBarnLine], "JOE")
				iCurrentJoeBarnLine++
				iJoeBarnLineTimer = GET_GAME_TIMER() + JOE_BARN_LINE_TIME
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is close to either Joe or Josef
/// RETURNS:
///    TRUE if the player is close to one of them, FALSE otherwise
FUNC BOOL CloseToBadGuy()
	FLOAT tempDist = 0.0
	FLOAT dist = 9999.9
	
	IF IS_PED_UNINJURED(mdJoe.piDude)
		dist = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), mdJoe.piDude)
	ENDIF
	IF IS_PED_UNINJURED(mdJosef.piDude)
		tempDist = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), mdJosef.piDude)
	ENDIF
	IF tempDist < dist
		dist = tempDist
	ENDIF
	
	IF dist < CHASE_LINE_DIST
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Works out who is closest and has available conversations, Joe, Josef or neither
/// RETURNS:
///    An enum value referring to Joe, Josef or neither
FUNC NEAREST_ENEMY NearestBadGuy()
	FLOAT tempDist = 0.0
	FLOAT dist = 10.0
	NEAREST_ENEMY tempGuy = NE_NEITHER
	
	IF IS_PED_UNINJURED(mdJoe.piDude) AND iCurrentJoeConv < NUM_JOE_CONVS
		tempDist = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), mdJoe.piDude)
		IF tempDist < dist
			dist = tempDist
			tempGuy = NE_JOE
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(mdJosef.piDude) AND iCurrentJosefConv < NUM_JOSEF_CONVS
		tempDist = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), mdJosef.piDude)
		IF tempDist < dist
			tempGuy = NE_JOSEF
		ENDIF
	ENDIF
	
	RETURN tempGuy
ENDFUNC

/// PURPOSE:
///    Triggers the chase dialogue
PROC TriggerChaseDialogue()
	IF GET_GAME_TIMER() > iConversationTimer AND CloseToBadGuy() AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = NULL
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND (NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			NEAREST_ENEMY badGuy = NearestBadGuy()
			SWITCH badGuy
				CASE NE_JOE
					IF iCurrentJoeConv < NUM_JOE_CONVS
						IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", sJoeConvs[iCurrentJoeConv], CONV_PRIORITY_HIGH)
							iCurrentJoeConv++
							iConversationTimer = GET_GAME_TIMER() + CHASE_LINE_TIME
						ENDIF
					ENDIF
				BREAK
				
				/*CASE NE_JOSEF
					IF iCurrentJosefConv < NUM_JOSEF_CONVS
						IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", sJosefConvs[iCurrentJosefConv], CONV_PRIORITY_HIGH)
							iCurrentJosefConv++
							iConversationTimer = GET_GAME_TIMER() + CHASE_LINE_TIME
						ENDIF
					ENDIF
				BREAK*/
				
				CASE NE_NEITHER
					IF iCurrentChaseLine < NUM_CHASE_LINES
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_CHASE", sChaseLines[iCurrentChaseLine], CONV_PRIORITY_HIGH)
							iCurrentChaseLine++
							iConversationTimer = GET_GAME_TIMER() + CHASE_LINE_TIME
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Passes the mission if Joe has been killed
/// RETURNS:
///    TRUE if Joe is dead
FUNC BOOL IsJoeDead()
	// if Joe is dead, yay, we pass
	IF DOES_ENTITY_EXIST(mdJoe.piDude)
		IF IS_PED_INJURED(mdJoe.piDude)
			MissionSubState = SS_SETUP
			missionStage = MS_PASS_DELAY
			IF IS_POINT_IN_POLY_2D(tpFarmArea, GET_ENTITY_COORDS(mdJoe.piDude, FALSE))
				CPRINTLN(DEBUG_MISSION, "Should be setting no migration stat")
				bKilledOnFarm = TRUE
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Removes Joe's blip if he's killed before the car chase
PROC ClearJoesBlip()
	IF DOES_ENTITY_EXIST(mdJoe.piDude)
		IF IS_PED_INJURED(mdJoe.piDude)
			SAFE_REMOVE_BLIP(mdJoe.biBlip)
			IF IS_POINT_IN_POLY_2D(tpFarmArea, GET_ENTITY_COORDS(mdJoe.piDude, FALSE))
				CPRINTLN(DEBUG_MISSION, "Should be setting no migration stat")
				bKilledOnFarm = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player has parked a vehicle outside the barn doors
/// RETURNS:
///    TRUE if the player is blocking the barn doors
FUNC BOOL PlayerBlockingBarn()
	VEHICLE_INDEX viPlayer = GET_PLAYERS_LAST_VEHICLE()
	
	IF IS_VEHICLE_OK(viPlayer)
		//IF IS_ENTITY_IN_ANGLED_AREA( viPlayer, vBarnBlockingPos1, vBarnBlockingPos2, fBarnBlockingWidth)
		IF IS_ENTITY_IN_RANGE_COORDS_2D(viPlayer, <<422.568817,6474.530762,27.811201>>, 7.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks for the player using a stun gun on Joe and Josef. Used for mission stat tracking
PROC CheckForStunGunUse()
	IF NOT bJosefStunned
		IF IS_PED_UNINJURED(mdJosef.piDude)
			IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(mdJosef.piDude, WEAPONTYPE_STUNGUN)
				bJosefStunned = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mdJoe.piDude)
		IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(mdJoe.piDude, WEAPONTYPE_STUNGUN)
			IF NOT bJoeStunned
				bJoeStunned = TRUE
			ENDIF
			CLEAR_ENTITY_LAST_WEAPON_DAMAGE(mdJoe.piDude)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the Mexicans flee at slightly different times
PROC MexicansFlee()
	INT i = 0
	INT timeDifference = GET_GAME_TIMER() - iMexicansFleeTimer
	INT timeToGo = GET_RANDOM_INT_IN_RANGE(100, 300)
	SEQUENCE_INDEX siFlee
	REPEAT NUM_MEXICANS i
		IF IS_PED_UNINJURED(piMexican[i])
			IF NOT IS_PED_FLEEING(piMexican[i]) AND NOT IsPedPerformingTask(piMexican[i], SCRIPT_TASK_PERFORM_SEQUENCE) AND timeDifference > (i * timeToGo)
			//IF bFleeTalkReady[i] AND timeDifference > (i * timeToGo)
				OPEN_SEQUENCE_TASK(siFlee)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLeaveDest[i], PEDMOVEBLENDRATIO_SPRINT)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
				CLOSE_SEQUENCE_TASK(siFlee)
				IF IS_PED_USING_ANY_SCENARIO(piMexican[i])
    				SET_PED_PANIC_EXIT_SCENARIO(piMexican[i], GET_ENTITY_COORDS(piMexican[i]))
				ENDIF
				TASK_PERFORM_SEQUENCE(piMexican[i], siFlee)
				CLEAR_SEQUENCE_TASK(siFlee)
			ENDIF
			IF bFleeTalkReady[i] AND timeDifference > (i * 1500)
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piMexican[i], sMexicanLine[i], sMexicanName[i])
				MAKE_PED_SCREAM(piMexican[i])
				/*IF i = 0
					SET_PED_WALLA_DENSITY(1.0, 1.0)
					FORCE_PED_PANIC_WALLA()
				ENDIF*/
				bFleeTalkReady[i] = FALSE
			ENDIF
			// TEMP - turn off ambient dialogue if the player is a fair distance from the Mexicans, should be dealt with properly eventually
			/*IF IS_AMBIENT_SPEECH_PLAYING(piMexican[i]) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piMexican[i]) > 50.0
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(piMexican[i])
			ENDIF*/
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Makes the second group of Mexicans flee at slightly different times
PROC MexicansFleeTwo()
	INT i = 0
	INT timeDifference = GET_GAME_TIMER() - iMexicansFleeTimer
	//SEQUENCE_INDEX siFlee
	REPEAT NUM_MEXICANS_2 i
		IF NOT PlayerBlockingBarn()
			IF IS_PED_UNINJURED(piMexican2[i])
				IF NOT IS_PED_FLEEING(piMexican2[i]) /*AND NOT IsPedPerformingTask(piMexican2[i], SCRIPT_TASK_PERFORM_SEQUENCE)*/ AND timeDifference > ((i*1000)+5000)
					/*OPEN_SEQUENCE_TASK(siFlee)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLeaveDest2[i], PEDMOVEBLENDRATIO_SPRINT)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
					CLOSE_SEQUENCE_TASK(siFlee)*/
					IF IS_PED_USING_ANY_SCENARIO(piMexican2[i])
	    				SET_PED_PANIC_EXIT_SCENARIO(piMexican2[i], GET_ENTITY_COORDS(piMexican2[i]))
					ENDIF
					/*TASK_PERFORM_SEQUENCE(piMexican2[i], siFlee)
					CLEAR_SEQUENCE_TASK(siFlee)*/
					IF NOT PlayerBlockingBarn()
						TASK_SMART_FLEE_PED(piMexican2[i], PLAYER_PED_ID(), 500, -1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Check to see if Joe has escaped
/// RETURNS:
///    TRUE if Joe has escaped
FUNC BOOL BadGuysEscaped()
	FLOAT chaseDist = 0.0
	
	// First, checks for if Joe's alive
	IF IS_PED_UNINJURED(mdJoe.piDude)
		// Get the distance between Joe and the player if he's not visible
		IF NOT IS_ENTITY_ON_SCREEN(mdJoe.piDude) OR NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), mdJoe.piDude)
			chaseDist = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), mdJoe.piDude)
			MissionFailReason = MFR_JOE_ESCAPED
		ENDIF
	ENDIF
	
	// if Joe is too far away and not visisble, fail the mission
	IF (chaseDist > iEscapeDist)
		CLEAR_ALL_BLIPS()
		KILL_ANY_CONVERSATION()
		MissionSubState = SS_SETUP
		missionStage = MS_FAIL_DELAY
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Counts how many tyres on the enemy vehicle have been burst
/// RETURNS:
///    Returns TRUE if three or more tyres have been burst and enough time for Joe and Josef to notice has passed
FUNC BOOL EnemyTyresBurst()
	IF bTyresBurst
		IF GET_GAME_TIMER() > iTyresBurstTimer
			RETURN TRUE
		ENDIF
	ELIF IS_VEHICLE_OK(viVan) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viVan)
		INT numTyres = 0
		IF IS_VEHICLE_TYRE_BURST(viVan, SC_WHEEL_CAR_FRONT_LEFT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viVan, SC_WHEEL_CAR_FRONT_RIGHT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viVan, SC_WHEEL_CAR_REAR_LEFT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viVan, SC_WHEEL_CAR_REAR_RIGHT)
			numTyres++
		ENDIF
		
		IF numTyres > 0
			bTyresBurst = TRUE
			iTyresBurstTimer = GET_GAME_TIMER() + iTyresTime
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Joe's tractor has been blocked by the player's car while he's in the barn
/// RETURNS:
///    TRUE if he's blocked in the barn, FALSE otherwise
FUNC BOOL JoeBlockedByPlayer()
	IF IS_VEHICLE_OK(viVan)
		VECTOR vAheadOfTractor = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viVan, <<0,6,0>>)//<<0, 3, 0>>)
		IF IS_ANY_VEHICLE_NEAR_POINT(vAheadOfTractor, 5.0/*2.9*/) AND IS_ENTITY_IN_ANGLED_AREA(viVan, vBarnAreaPos1, vBarnAreaPos2, fBarnAreaWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the tractor is stuck
/// RETURNS:
///    TRUE if the tractor is stuck, FALSE otherwise
FUNC BOOL TractorIsStuck()
	IF IS_VEHICLE_OK(viTractor)
		IF IS_VEHICLE_STUCK_TIMER_UP(viTractor, VEH_STUCK_HUNG_UP, 1000)
		OR IS_VEHICLE_STUCK_TIMER_UP(viTractor, VEH_STUCK_JAMMED, 1000)
		OR IS_VEHICLE_STUCK_TIMER_UP(viTractor, VEH_STUCK_ON_ROOF, 1000)
		OR IS_VEHICLE_STUCK_TIMER_UP(viTractor, VEH_STUCK_ON_SIDE, 1000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks the enemy car and makes Joe bail out and flee if it's too damaged.
PROC IsCarOk()
	IF DOES_ENTITY_EXIST(viVan)
		IF NOT IS_VEHICLE_OK(viVan) OR IS_ENTITY_ON_FIRE(viVan) OR (GET_ENTITY_HEALTH(viVan) < 700) OR (GET_VEHICLE_ENGINE_HEALTH(viVan) < 50) OR (GET_VEHICLE_PETROL_TANK_HEALTH(viVan) < 50)
		OR EnemyTyresBurst() OR TractorIsStuck() OR HAS_PLAYER_RAMMED_ENEMY_ENOUGH(viVan, bJoeRammedLastFrame, iRamTimer, iJoeRammedCount, fPlayerVsJoeClosingSpeedLastFrame, 11, 5.0) //OR JoeBlockedByPlayer()
		OR IS_ENTITY_IN_WATER(viVan)
			// Bail out if the car is too damaged
			IF IS_PED_UNINJURED(mdJoe.piDude)
				IF DOES_BLIP_EXIST(mdJoe.biBlip)
					SET_BLIP_SCALE(mdJoe.biBlip, BLIP_SIZE_PED)
				ELSE
					mdJoe.biBlip = CREATE_PED_BLIP(mdJoe.piDude)
				ENDIF
				/*SEQUENCE_INDEX siFlee
				OPEN_SEQUENCE_TASK(siFlee)
					TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT| ECF_DONT_CLOSE_DOOR)
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
				CLOSE_SEQUENCE_TASK(siFlee)
				TASK_PERFORM_SEQUENCE(mdJoe.piDude, siFlee)
				CLEAR_SEQUENCE_TASK(siFlee)
				REMOVE_ALL_PED_WEAPONS(mdJoe.piDude)*/
				SET_PED_FLEE_ATTRIBUTES(mdJoe.piDude, FA_FORCE_EXIT_VEHICLE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(mdJoe.piDude, FA_USE_VEHICLE, FALSE)
				TASK_SMART_FLEE_PED(mdJoe.piDude, PLAYER_PED_ID(), 500, -1)
			ENDIF
			iMexicansFleeTimer = GET_GAME_TIMER()
			CPRINTLN(DEBUG_MISSION, "Start FLEE_ON_FOOT")
			missionStage = MS_FLEE_ON_FOOT
			EXIT
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mdJoe.piDude)
		IF IS_PED_BEING_JACKED(mdJoe.piDude)
		OR NOT IS_PED_IN_ANY_VEHICLE(mdJoe.piDude)
			IF DOES_BLIP_EXIST(mdJoe.biBlip)
				SET_BLIP_SCALE(mdJoe.biBlip, BLIP_SIZE_PED)
			ELSE
				mdJoe.biBlip = CREATE_PED_BLIP(mdJoe.piDude)
			ENDIF
			TASK_SMART_FLEE_PED(mdJoe.piDude, PLAYER_PED_ID(), 500, -1)
			
			IF IS_VEHICLE_OK(viVan) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viVan)
				STOP_PLAYBACK_RECORDED_VEHICLE(viVan)
			ENDIF
			bJackedJoeAndJosef = TRUE
			CPRINTLN(DEBUG_MISSION, "Start FLEE_ON_FOOT")
			missionStage = MS_FLEE_ON_FOOT
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Make the van drive with AI after the vehicle recording has finished. Stop the vehicle recording if Joe has bailed out or is dead
PROC VanController()
	IF IS_VEHICLE_OK(viVan) AND IS_PED_UNINJURED(mdJoe.piDude)
		IF IsPedPerformingTask(mdJoe.piDude, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) AND GET_IS_WAYPOINT_RECORDING_LOADED(sJoesWaypointRec)
			INT iCurrentWaypoint
			IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sJoesWaypointRec, GET_ENTITY_COORDS(viVan), iCurrentWaypoint)
				IF iCurrentWaypoint > iNumPointsInJoesRec AND bStartAiDriving AND NOT bJackedJoeAndJosef
					// Joe is reaching the end of his waypoint recording, switch him to flee behaviour
					IF NOT (GET_SCRIPT_TASK_STATUS(mdJoe.piDude, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK)
					AND NOT (GET_SCRIPT_TASK_STATUS(mdJoe.piDude, SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK)
					AND NOT IS_PED_FLEEING(mdJoe.piDude) AND NOT (GET_SCRIPT_TASK_STATUS(mdJoe.piDude, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK)
						SET_DRIVE_TASK_MAX_CRUISE_SPEED(mdJoe.piDude, 15)
						TASK_VEHICLE_MISSION_PED_TARGET(mdJoe.piDude, viVan, PLAYER_PED_ID(), MISSION_FLEE, 15.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
						bStartAiDriving = FALSE
					ENDIF
				ELSE
					// Joe is still on the waypoint recording so control his speed if necessary
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if it's appropriate to activate Joe
/// PARAMS:
///    fAreaWidth - the width of the area outside the barn to check
///    bUseTimer - should we activate Joe if 30 seconds have passed?
/// RETURNS:
///    True if the player is in front of the barn or if 30 seconds have passed (if bUseTimer is true)
FUNC BOOL CheckActivateJoe(FLOAT fAreaWidth, BOOL bUseTimer)
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAltJoePos, fAreaWidth) OR (bUseTimer AND GET_GAME_TIMER() > iJoeFleesTimer)
		MissionSubState = SS_SETUP
		missionStage = MS_CAR_CHASE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if it's appropriate to activate Joe using different values if the player is in a vehicle or not
/// PARAMS:
///    bUseTimer - should we activate Joe if 30 seconds have passed?
/// RETURNS:
///    True if Joe should be activated
FUNC BOOL ActivateJoe(BOOL bUseTimer = TRUE)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN CheckActivateJoe(fJoeDrivesLarge, bUseTimer)
	ELSE
		RETURN CheckActivateJoe(fJoeDrives, bUseTimer)
	ENDIF
ENDFUNC

/// PURPOSE:
///    Blips Josef and sets the mission to the Josef Attacks stage
/// PARAMS:
///    iDelayTimerValue - How long Josef should pause for before he attacks the player
PROC BlipJosef(INT iDelayTimerValue = 0)
	iJosefAttacksTimer = GET_GAME_TIMER() + iDelayTimerValue
	TRIGGER_MUSIC_EVENT("MM3_START_FORA")
	bAudioReady = TRUE
	iAudioEventTimer = GET_GAME_TIMER() + AUDIO_EVENT_WAIT_TIME
	MissionSubState = SS_SETUP
	missionStage = MS_JOSEF_ATTACKS
ENDPROC

// ===========================================================================================================
//		MISSION STAGES
// ===========================================================================================================
/// PURPOSE:
///    If this is a replay, skip to the appropriate stage, otherwise play the intro
PROC INIT()
	sDriveLines[0] = "MIN3_DRIVE_1"
	sDriveLines[1] = "MIN3_DRIVE_2"
	sDriveLines[2] = "MIN3_DRIVE_3"
	sDriveLines[3] = "MIN3_DRIVE_4"
	
	sJoeBarnLines[0] = "MIN3_ASAA"
	sJoeBarnLines[1] = "MIN3_ASAB"
	sJoeBarnLines[2] = "MIN3_ASAC"
	sJoeBarnLines[3] = "MIN3_ASAD"
	sJoeBarnLines[4] = "MIN3_ASAE"
	sJoeBarnLines[5] = "MIN3_ASAF"
	
	sChaseLines[0] = "MIN3_CHASE_1"
	sChaseLines[1] = "MIN3_CHASE_2"
	sChaseLines[2] = "MIN3_CHASE_3"
	sChaseLines[3] = "MIN3_CHASE_4"
	sChaseLines[4] = "MIN3_CHASE_5"
	
	sJoeConvs[0] = "MIN3_JOE1"
	sJoeConvs[1] = "MIN3_JOE2"
	sJoeConvs[2] = "MIN3_JOE3"
	
	/*sJosefConvs[0] = "MIN3_JOSEF1"
	sJosefConvs[1] = "MIN3_JOSEF2"
	sJosefConvs[2] = "MIN3_JOSEF3"*/
	
	/*sMexicanLine[0] = "MIN3_AOAA"
	sMexicanLine[1] = "MIN3_AOAB"
	sMexicanLine[2] = "MIN3_AOAC"
	sMexicanLine[3] = "MIN3_AOAD"
	sMexicanLine[4] = "MIN3_AOAE"
	sMexicanName[0] = "Min3Mexican0"
	sMexicanName[1] = "Min3Mexican1"
	sMexicanName[2] = "Min3Mexican2"
	sMexicanName[3] = "Min3Mexican3"
	sMexicanName[4] = "Min3Mexican4"*/
	bFleeTalkReady[0] = TRUE
	bFleeTalkReady[1] = TRUE//FALSE
	bFleeTalkReady[2] = TRUE
	bFleeTalkReady[3] = TRUE//FALSE
	bFleeTalkReady[4] = TRUE
	
	REQUEST_ANIM_DICT(ANIM_DICT_GLANCES)
	REQUEST_ANIM_DICT(ANIM_DICT_PANIC)
	REQUEST_ADDITIONAL_TEXT("MIN3", MISSION_TEXT_SLOT)
	
	SET_CREATE_RANDOM_COPS(FALSE)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OPEN_TEST_POLY(tpFarmArea)
			ADD_TEST_POLY_VERT(tpFarmArea, <<468.640381,6547.755859,26.092230>>)//<<444.766785,6551.482910,26.293985>>)
			ADD_TEST_POLY_VERT(tpFarmArea, <<477.909210,6447.278320,29.429457>>)//<<445.532562,6446.873535,28.948292>>)
			ADD_TEST_POLY_VERT(tpFarmArea, <<240.051117,6414.416016,30.429955>>)
			ADD_TEST_POLY_VERT(tpFarmArea, <<148.916870,6500.048828,30.583202>>)
			ADD_TEST_POLY_VERT(tpFarmArea, <<229.888519,6546.030273,30.679270>>)
		CLOSE_TEST_POLY(tpFarmArea)
		IF Is_Replay_In_Progress()
			IF NOT IS_AUDIO_SCENE_ACTIVE("MINUTE_03_SCENE")
				START_AUDIO_SCENE("MINUTE_03_SCENE")
			ENDIF
			
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
				TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[MANUEL])
			ENDIF
			
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				iReplayStage++ // player is skipping this stage
			ENDIF
			
			SAFE_DELETE_OBJECT(sRCLauncherDataLocal.ObjID[1])
			
			SWITCH iReplayStage
				CASE CP_AFTER_MOCAP
					START_REPLAY_SETUP(<< -303.3698, 6212.3550, 30.4696 >>, 229.2303)
					AfterMocapCheckpoint()
				BREAK
				
				CASE CP_KILL_JOSEF
					ADD_PED_FOR_DIALOGUE(pedConvStruct, TREVOR_ID, PLAYER_PED_ID(), "TREVOR", TRUE)
					START_REPLAY_SETUP(<< 389.4927, 6554.7339, 26.6191 >>, 270.5771)
					DebugSkipKillJosef()
				BREAK
				
				CASE CP_KILL_JOE
					ADD_PED_FOR_DIALOGUE(pedConvStruct, TREVOR_ID, PLAYER_PED_ID(), "TREVOR", TRUE)
					START_REPLAY_SETUP(<< 423.6803, 6519.7100, 26.6747 >>, 170.9605)
					DebugSkipChaseJoe()
				BREAK
				
				CASE CP_MISSION_PASSED
					/*WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)*/
					START_REPLAY_SETUP(<<428.21, 6509.16, 27.92>>, 161.97)
					VEHICLE_INDEX viTemp
					CREATE_VEHICLE_FOR_REPLAY(viTemp, << 424.07, 6509.42, 27.33 >>, 181.39, FALSE, FALSE, TRUE, TRUE, TRUE, SABREGT)
					END_REPLAY_SETUP()
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					RC_END_Z_SKIP()
					Script_Passed()
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
				BREAK
			ENDSWITCH
		ELSE
			MissionSubState = SS_SETUP
			missionStage = MS_PRE_INTRO
		ENDIF
	ENDIF
ENDPROC

PROC StopPlayerWhenTooClose()
	/*IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) < 3
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
	ENDIF*/
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<-303.675201,6212.526855,30.455961>>, 2.0)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_STILL)
		IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
			TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0], -1)
		ENDIF
	ENDIF
ENDPROC

PROC PRE_INTRO()
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	//SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	SWITCH MissionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Start PRE_INTRO")
			ADD_PED_FOR_DIALOGUE(pedConvStruct, TREVOR_ID, PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(pedConvStruct, MANUEL_ID, sRCLauncherDataLocal.pedID[0], "MANUEL")
			
			StopPlayerWhenTooClose()
			
			iConvFailedTimer = -1
			
			// Setup pushin camera
			IF IS_SCREEN_FADED_IN() // Don't do gameplay hint if skipping to intro
			AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			//AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) > 6
				SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0, 0, 0>>), TRUE, -1, 3000)															
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.45)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.01)
                SET_GAMEPLAY_HINT_FOV(25.00)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				
				//MissionSubState = SS_UPDATE
			ELSE
				//MissionSubState = SS_CLEANUP
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MIN3: Not doing leadin focus camera") #ENDIF
			ENDIF
			
			// Disable the nearby tattoo shop so the tutorial doesn't interfere
			SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(TATTOO_PARLOUR_03_PB, TRUE)
			
			MissionSubState = SS_UPDATE
			
			/*IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) < 1.5
				MissionSubState = SS_CLEANUP
			ELSE
				MissionSubState = SS_UPDATE
			ENDIF*/
		BREAK
		
		CASE SS_UPDATE
			IF CREATE_CONVERSATION(pedConvStruct ,"MIN3AUD", "MIN3_INT_LI", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
				MissionSubState = SS_UPDATE_TWO
			ENDIF
			StopPlayerWhenTooClose()
		BREAK
		
		CASE SS_UPDATE_TWO
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				MissionSubState = SS_CLEANUP
			ENDIF
			StopPlayerWhenTooClose()
		BREAK
		
		CASE SS_CLEANUP
			IF IS_REPEAT_PLAY_ACTIVE()
				REQUEST_CUTSCENE("mmb_3_rcm")
				WHILE NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					WAIT(0)
				ENDWHILE
			ENDIF
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			// Enable the tattoo shop again
			SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(TATTOO_PARLOUR_03_PB, FALSE)
			
			PlayIntro()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Wait for the intro to finish then set up the post-intro stuff
PROC INTRO_PLAYING()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
			ENDIF
		ENDIF
	#ENDIF
	
	// Debug prints to check the states of Trevor and his pistol
	/*IF DOES_ENTITY_EXIST(oiPistol)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
			CPRINTLN(DEBUG_MISSION, "Pistol exists and is ready to leave the cutscene")
		ELSE
			CPRINTLN(DEBUG_MISSION, "Pistol exists and is not ready to leave the cutscene")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "Pistol does not exist")
	ENDIF
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			CPRINTLN(DEBUG_MISSION, "Player exists and is ready to leave the cutscene")
		ELSE
			CPRINTLN(DEBUG_MISSION, "Player exists and is not ready to leave the cutscene")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "Player does not exist")
	ENDIF*/
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(oiPistol)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor") AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, TRUE)
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, TRUE)
			/*ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)*/
			ENDIF
			DELETE_OBJECT(oiPistol)
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Manuel")
			/*SET_ENTITY_COLLISION(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
			FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.pedID[MANUEL], FALSE)*/
			
			//IF WAS_CUTSCENE_SKIPPED()
				SET_ENTITY_COORDS_GROUNDED(sRCLauncherDataLocal.pedID[MANUEL], vManuelAfterCutscene)
				SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[MANUEL], fManuelAfterCutscne)
			//ENDIF
			SEQUENCE_INDEX siManuel
			OPEN_SEQUENCE_TASK(siManuel)
				//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[MANUEL_CAR]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[MANUEL_CAR])
				AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.vehID[MANUEL_CAR], (sRCLauncherDataLocal.pedID[MANUEL]), 25.0)
					TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK)
					TASK_VEHICLE_DRIVE_WANDER(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], 20, DRIVINGMODE_AVOIDCARS)
				ELSE
					TASK_WANDER_STANDARD(NULL)
				ENDIF
			CLOSE_SEQUENCE_TASK(siManuel)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MANUEL], siManuel)
			CLEAR_SEQUENCE_TASK(siManuel)
		ENDIF
	ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_CAMERA()
		REPLAY_STOP_EVENT()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	
	/*IF DOES_ENTITY_EXIST(oiPistol)
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_weapon")
			DELETE_OBJECT(oiPistol)
		ENDIF
	ENDIF*/
	
	IF IS_CUTSCENE_PLAYING()
		IF IS_GAMEPLAY_HINT_ACTIVE()
			STOP_GAMEPLAY_HINT()
		ENDIF
	ELSE
		// Make player face Manuel
		//SET_ENTITY_HEADING(PLAYER_PED_ID(), -94.08)

		// Reset camera behind player
		//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		
		/*IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[MANUEL])
			SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[MANUEL], TRUE)
		ENDIF*/
		
		// Position Manuel and make him tell the player where to go
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
			//IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_MANUEL", CONV_PRIORITY_MEDIUM)
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_MANUEL", "MIN3_MANUEL_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				
				RC_END_CUTSCENE_MODE(TRUE, FALSE, TRUE)
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[MANUEL], FALSE)
				SET_PED_CAN_BE_TARGETTED(sRCLauncherDataLocal.pedID[MANUEL], FALSE)
				//SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[MANUEL], <<-299.33, 6212.24, 31.45>>)
				//SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[MANUEL], 95.07)
				/*SET_ENTITY_COLLISION(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
				FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.pedID[MANUEL], FALSE)*/
				/*SEQUENCE_INDEX siManuel
				OPEN_SEQUENCE_TASK(siManuel)
					//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[MANUEL_CAR]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[MANUEL_CAR])
						TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK)
						TASK_VEHICLE_DRIVE_WANDER(NULL, sRCLauncherDataLocal.vehID[MANUEL_CAR], 20, DRIVINGMODE_AVOIDCARS)
					ELSE
						TASK_WANDER_STANDARD(NULL)
					ENDIF
				CLOSE_SEQUENCE_TASK(siManuel)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MANUEL], siManuel)
				CLEAR_SEQUENCE_TASK(siManuel)*/
				
				ClearTheFarm()
				
				/*IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
				ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, TRUE)
				ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, TRUE)
				ELSE
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 12, TRUE)
				ENDIF*/
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.objID[MANUEL])
					SET_ENTITY_COLLISION(sRCLauncherDataLocal.objID[MANUEL], TRUE)
					FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.objID[MANUEL], FALSE)
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("MINUTE_03_SCENE")
					START_AUDIO_SCENE("MINUTE_03_SCENE")
				ENDIF
				
				PRINT_NOW("MIN3_06", DEFAULT_GOD_TEXT_TIME, 1)	// Kill ~r~Joe ~s~and ~r~Josef.
				CreateInitialBlips()
				
				//WAIT(500)	// hopefully temporary. For some reason I can't work out, Manuel's dialogue won't play without this here
				MissionSubState = SS_SETUP
				missionStage = MS_DRIVE_TO_FARM
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    The player drives to the farm
PROC DRIVE_TO_FARM()
	ClearJoesBlip()
	SWITCH MissionSubState
		CASE SS_SETUP
			SET_GPS_DISABLED_ZONE(<<168.894516,6408.562988,27.136101>>, <<401.467010,6519.562500,29.836889>>)
			iConversationTimer = GET_GAME_TIMER() + DRIVE_LINE_TIME
			iManuelFollowTimer = GET_GAME_TIMER() + (MANUEL_CONV_TIME*2)
			iManuelBumpTimer = 0
			iConvFailedTimer = -1
			bKnickedManuelsCar = FALSE
			CPRINTLN(DEBUG_MISSION, "Start DRIVE_TO_FARM")
			LoadBrokenCarModels()			
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			MissionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			//SpawnBrokenCar()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR CONVERSATION_TIMED_OUT(iConvFailedTimer, 15000)
				/*PRINT_NOW("MIN3_06", DEFAULT_GOD_TEXT_TIME, 1)	// Kill ~r~Joe ~s~and ~r~Josef.
				CreateInitialBlips()*/
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_MANUEL", "MIN3_MANUEL_2", CONV_PRIORITY_MEDIUM)
					MissionSubState = SS_UPDATE_TWO
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_UPDATE_TWO
			SpawnBrokenCar()
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAltJosefPos) > 100.0)
				TriggerDriveDialogue()
			ENDIF
			// load the farm peds when the player gets close
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAltJosefPos, vSpawnPedsSize)
				LoadModels()
				ClearTheFarm()
				MissionSubState = SS_UPDATE_THREE
			ENDIF
		BREAK
		
		CASE SS_UPDATE_THREE
			Setup_Farm()
			Setup_Farm_Two()
			TriggerJoeInBarnDialogue()
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAltJosefPos) > 150.0)
				TriggerDriveDialogue()
			ELSE
				SET_WANTED_LEVEL_MULTIPLIER(0.1)
			ENDIF
			IF IS_PED_UNINJURED(mdJosef.piDude) AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAltJosefPos) < 45.0//60.0
				IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_J_FARM", CONV_PRIORITY_MEDIUM)
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
					MissionSubState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			TriggerJoeInBarnDialogue()
			IF IS_PED_UNINJURED(mdJosef.piDude)
				SET_PED_RESET_FLAG(mdJosef.piDude, PRF_InstantBlendToAim, TRUE)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAltJosefPos) < 25.0
					IF CAN_PED_SEE_PLAYER(mdJosef.piDude, 90)
					OR (CAN_PED_SEE_PLAYER(mdJosef.piDude, 0.0) AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()))
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_T_FARM", CONV_PRIORITY_MEDIUM)
							BlipJosef(JOSEF_PAUSES)
						ENDIF
					ENDIF
				ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vAltJosefPos) > 100.0
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ELIF IS_PED_UNINJURED(piMexican[0])
					IF iMexDialogueTimer > 0
						IF GET_GAME_TIMER() > iMexDialogueTimer
							IF bMexDialogueTriggered
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_PED_IN_COMBAT(mdJosef.piDude)
									SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piMexican[0], FALSE)
									SET_ENTITY_HEALTH(piMexican[0], 110)
									SET_PED_ACCURACY(mdJosef.piDude, 100)
									TASK_COMBAT_PED(mdJosef.piDude, piMexican[0])
								ENDIF
							ELSE
								bMexDialogueTriggered = CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_REPLY", CONV_PRIORITY_MEDIUM)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iMexDialogueTimer = GET_GAME_TIMER() + MEX_DIALOGUE_TIME
							ADD_PED_FOR_DIALOGUE(pedConvStruct, MEX_ID, piMexican[0], "Min3Mexican0", TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) <> NULL
					MissionFailReason = MFR_EXECUTION
					MissionSubState = SS_SETUP
					missionStage = MS_FAIL_DELAY
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF (DOES_ENTITY_EXIST(viTractor) AND (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viTractor, PLAYER_PED_ID()) OR NOT IS_ENTITY_ALIVE(viTractor)))
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vAltJosefPos, 50)
		BlipJosef()
	ENDIF
	IF DOES_ENTITY_EXIST(mdJosef.piDude)
		IF IS_PED_INJURED(mdJosef.piDude)
			IF NOT ActivateJoe(FALSE)
				IF bAllowOneDeadMessage
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_KILL", "MIN3_KILL_1", CONV_PRIORITY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
						bAllowOneDeadMessage = FALSE
						MissionSubState = SS_SETUP
						missionStage = MS_SEARCH_FARM
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_PLAYER_SHOOTING_NEAR_PED(mdJosef.piDude) OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mdJosef.piDude, PLAYER_PED_ID())
			BlipJosef()
		ENDIF
	ENDIF
	
	// remove the farmer if the player is far enough away
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MANUEL])
		// if the farmer exists and the player fires a gun, make him flee
		IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MANUEL]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MANUEL], TRUE)
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_THREAT", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MANUEL], PLAYER_PED_ID(), 500, -1)
				ENDIF
			ELIF bNotAimedAtManuel AND IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[MANUEL])
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_AIM", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MANUEL], SCRIPT_TASK_WANDER_STANDARD)
						TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[MANUEL])
					ENDIF
					bNotAimedAtManuel = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MANUEL])
			IF IS_PED_STOPPED(sRCLauncherDataLocal.pedID[MANUEL])
				IF iManuelStoppedTimer = -1
					iManuelStoppedTimer = GET_GAME_TIMER() + MAX_MANUEL_STOPPED_TIME
				ELSE
					IF GET_GAME_TIMER() > iManuelStoppedTimer
						IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MANUEL], SCRIPT_TASK_WANDER_STANDARD)
							TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[MANUEL])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iManuelStoppedTimer > -1
					iManuelStoppedTimer = -1
				ENDIF
			ENDIF
		ENDIF
		
		// Play dialogue if the player bumps or follows Manuel
		IF GET_GAME_TIMER() > iManuelBumpTimer AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MANUEL])
			enumSubtitlesState eSubtitles = DISPLAY_SUBTITLES
			IF IS_MESSAGE_BEING_DISPLAYED()
				eSubtitles = DO_NOT_DISPLAY_SUBTITLES
			ENDIF
			IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_BUMP", CONV_PRIORITY_MEDIUM, eSubtitles)
				iManuelBumpTimer = GET_GAME_TIMER() + MANUEL_CONV_TIME
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() > iManuelFollowTimer AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MANUEL], 15.0)
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MANUEL]) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				enumSubtitlesState eSubtitles = DISPLAY_SUBTITLES
				IF IS_MESSAGE_BEING_DISPLAYED()
					eSubtitles = DO_NOT_DISPLAY_SUBTITLES
				ENDIF
				IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_FOLLOW", CONV_PRIORITY_MEDIUM, eSubtitles)
					iManuelFollowTimer = GET_GAME_TIMER() + MANUEL_CONV_TIME
				ENDIF
			ENDIF
		ENDIF
		
		// keep this at the end, releases Manuel
		IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[MANUEL], <<100,100,2.5>>)
			REMOVE_PED_FOR_DIALOGUE(pedConvStruct, MANUEL_ID)
			SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[MANUEL])
		ENDIF
	ENDIF
	
	// debug skip to next stage
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF MissionSubState = SS_UPDATE_THREE OR MissionSubState = SS_CLEANUP
				RC_START_Z_SKIP()
				SetPlayerPosWithVehicle(<< 424.9119, 6536.3882, 26.6722 >>, 175.3812)//, FALSE)
				RC_END_Z_SKIP(TRUE)
			ELSE
				DebugSkipKillJosef()
			ENDIF
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			DebugSkipRestart()
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Josef attacks the player
PROC JOSEF_ATTACKS()
	ClearJoesBlip()
	TriggerJoeInBarnDialogue()
	SWITCH MissionSubState
		CASE SS_SETUP
			IF IS_PED_UNINJURED(mdJosef.piDude)
				SET_PED_RESET_FLAG(mdJosef.piDude, PRF_InstantBlendToAim, TRUE)
			ENDIF
			IF GET_GAME_TIMER() > iJosefAttacksTimer
				IF IS_PED_UNINJURED(mdJosef.piDude)
					TASK_COMBAT_PED(mdJosef.piDude, PLAYER_PED_ID())
				ENDIF
				CreateProperBlips()
				iMexicansFleeTimer = GET_GAME_TIMER()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOSEF, "Kill Josef")
				SET_WANTED_LEVEL_MULTIPLIER(0.1)
				CPRINTLN(DEBUG_MISSION, "Start JOSEF_ATTACKS")
				MissionSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			MexicansFlee()
			IF NOT IS_PED_UNINJURED(mdJosef.piDude)
				IF IS_PED_UNINJURED(mdJoe.piDude)
					IF NOT ActivateJoe(FALSE)
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						IF bAllowOneDeadMessage
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_KILL", "MIN3_KILL_1", CONV_PRIORITY_HIGH)
								
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
								
								bAllowOneDeadMessage = FALSE
								MissionSubState = SS_SETUP
								missionStage = MS_SEARCH_FARM
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MissionSubState = SS_SETUP
					missionStage = MS_PASS_DELAY
				ENDIF
			ENDIF
			
			IF bAudioReady
				IF GET_GAME_TIMER() > iAudioEventTimer
					TRIGGER_MUSIC_EVENT("MM3_START_STA")
					bAudioReady = FALSE
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(mdJosef.piDude)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJosef.piDude)
			ENDIF
		BREAK
	ENDSWITCH
	
	// debug skip to next stage
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			DebugSkipChaseJoe()
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			DebugSkipKillJosef()
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Increases the size of Joe's collision capsule while he's in the barn to stop his gun clipping through the doors
PROC BOOST_JOES_CAPSULE_SIZE()
	IF IS_PED_UNINJURED(mdJoe.piDude)
		IF IS_ENTITY_IN_ANGLED_AREA( mdJoe.piDude, vBarnForJoePos1, vBarnForJoePos2, fBarnForJoeWidth)
		AND NOT IS_PED_IN_ANY_VEHICLE(mdJoe.piDude)
			SET_PED_CAPSULE(mdJoe.piDude, 0.35)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player searches the farm for Joe
PROC SEARCH_FARM()
	IF IsJoeDead()
		EXIT
	ENDIF
	TriggerJoeInBarnDialogue()
	BOOST_JOES_CAPSULE_SIZE()
	
	SWITCH MissionSubState
		CASE SS_SETUP
			SAFE_REMOVE_BLIP(mdJosef.biBlip)
			iMexicansFleeTimer = GET_GAME_TIMER()
			MexicansFlee()
			iJoeFleesTimer = GET_GAME_TIMER() + JOE_FLEES_TIME
			bAllowOneDeadMessage = TRUE
			CPRINTLN(DEBUG_MISSION, "Start SEARCH_FARM")
			IF bAudioReady
				TRIGGER_MUSIC_EVENT("MM3_START_FORA")
				iAudioEventTimer = GET_GAME_TIMER() + AUDIO_EVENT_WAIT_TIME
			ENDIF
			MissionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			MexicansFlee()
			ActivateJoe()
			
			IF bAudioReady
				IF GET_GAME_TIMER() > iAudioEventTimer
					TRIGGER_MUSIC_EVENT("MM3_START_STA")
					bAudioReady = FALSE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	// debug skip to next stage
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			RC_START_Z_SKIP()
			SetPlayerPosWithVehicle(<< 422.0315, 6493.4644, 27.2282 >>, 131.0355)
			RC_END_Z_SKIP(TRUE)
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			DebugSkipKillJosef()
		ENDIF
	#ENDIF
ENDPROC

PROC UnlockBarnDoors()
	IF DOES_ENTITY_EXIST(oiBarnDoorLeft)
		FREEZE_ENTITY_POSITION(oiBarnDoorLeft, FALSE)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorLeft, <<424.32, 6477.74, 30.79>>, FALSE, 0.0)
		SAFE_RELEASE_OBJECT(oiBarnDoorLeft)
	ENDIF
	IF DOES_ENTITY_EXIST(oiBarnDoorRight)
		FREEZE_ENTITY_POSITION(oiBarnDoorRight, FALSE)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(mnBarnDoorRight, << 419.30, 6471.71, 30.74>>, FALSE, 0.0)
		SAFE_RELEASE_OBJECT(oiBarnDoorRight)
	ENDIF
ENDPROC

/// PURPOSE:
///    The player chases Joe in a car
PROC CAR_CHASE()
	IF IsJoeDead() OR BadGuysEscaped()
		EXIT
	ENDIF
	
	/*IF IS_VEHICLE_OK(viVan)
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, viVan)
	EL*/IF IS_PED_UNINJURED(mdJoe.piDude)
		CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJoe.piDude)
	ENDIF
	
	MexicansFlee()
	BOOST_JOES_CAPSULE_SIZE()
	
	SWITCH MissionSubState
		CASE SS_SETUP
			SAFE_REMOVE_BLIP(mdJosef.biBlip)
			IF IS_PED_UNINJURED(mdJoe.piDude)
				IF IS_VEHICLE_OK(viVan) AND IS_VEHICLE_SEAT_FREE(viVan) AND NOT JoeBlockedByPlayer()
				AND NOT PlayerBlockingBarn()
					ADD_PED_FOR_DIALOGUE(pedConvStruct, JOE_ID, mdJoe.piDude, "JOE", TRUE)
					IF IS_ENTITY_ON_SCREEN(mdJoe.piDude) AND
					(IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<420.732849,6463.860352,27.903580>>, 5.0)
					OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<434.056580,6476.199707,27.833858>>, 10.0))
						TASK_ENTER_VEHICLE(mdJoe.piDude, viVan)
					ELSE
						TASK_WARP_PED_INTO_VEHICLE(mdJoe.piDude, viVan)
					ENDIF
					IF bAudioReady
						TRIGGER_MUSIC_EVENT("MM3_START_FORA")
						iAudioEventTimer = GET_GAME_TIMER() + AUDIO_EVENT_WAIT_TIME
					ENDIF
					SET_WANTED_LEVEL_MULTIPLIER(0.1)
					PREPARE_MUSIC_EVENT("MM3_TRACTOR")
					INIT_HAS_PLAYER_RAMMED_ENEMY_ENOUGH(bJoeRammedLastFrame, iRamTimer, iJoeRammedCount, fPlayerVsJoeClosingSpeedLastFrame)
					MissionSubState = SS_UPDATE
				ELSE
					/*SEQUENCE_INDEX siFlee
					OPEN_SEQUENCE_TASK(siFlee)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLeaveDest2[0], PEDMOVEBLENDRATIO_SPRINT)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
					CLOSE_SEQUENCE_TASK(siFlee)
					TASK_PERFORM_SEQUENCE(mdJoe.piDude, siFlee)
					CLEAR_SEQUENCE_TASK(siFlee)
					SET_PED_DROPS_WEAPON(mdJoe.piDude)*/
					TASK_COMBAT_PED(mdJoe.piDude, PLAYER_PED_ID())
					IF bAudioReady
						TRIGGER_MUSIC_EVENT("MM3_START_FORA")
						iAudioEventTimer = GET_GAME_TIMER() + AUDIO_EVENT_WAIT_TIME
					ENDIF
					UnlockBarnDoors()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOE, "Kill Joe", TRUE)
					missionStage = MS_FLEE_ON_FOOT
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF IS_PED_UNINJURED(mdJoe.piDude) AND IS_VEHICLE_OK(viVan) AND IS_PED_IN_VEHICLE(mdJoe.piDude, viVan)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecMain) AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecAlt)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_T_BARN", CONV_PRIORITY_MEDIUM)
					UnlockBarnDoors()
					// start car recording here
					VEHICLE_INDEX viPlayer
					viPlayer = GET_PLAYERS_LAST_VEHICLE()
					IF IS_VEHICLE_OK(viPlayer) AND IS_ENTITY_IN_ANGLED_AREA(viPlayer, vCheckForPlayersCar1, vCheckForPlayersCar2, fCheckForPlayersCar)
						sJoesWaypointRec = sWaypointRecAlt
						iNumPointsInJoesRec = 19
					ELSE
						sJoesWaypointRec = sWaypointRecMain
						iNumPointsInJoesRec = 37
					ENDIF
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST)	// Record Joe driving out of the barn
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mdJoe.piDude, viVan, sJoesWaypointRec, DRIVINGMODE_PLOUGHTHROUGH, 0, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
					SET_VEHICLE_FORWARD_SPEED(viVan, 15.0)
					LoadTowTruckModels()
					iConversationTimer = GET_GAME_TIMER() + CHASE_LINE_TIME
					iMexicansFleeTimer = GET_GAME_TIMER()
					//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOE, "Kill Joe", TRUE)
					bSetJoesCheckpoint = TRUE
					iJoesCheckpointTimer = GET_GAME_TIMER() + JOES_CHECKPOINT_TIME
					CPRINTLN(DEBUG_MISSION, "Start CAR_CHASE")
					/*IF DOES_BLIP_EXIST(mdJoe.biBlip)
						SET_BLIP_SCALE(mdJoe.biBlip, BLIP_SIZE_VEHICLE)
					ELSE*/
					SAFE_REMOVE_BLIP(mdJoe.biBlip)
						mdJoe.biBlip = CREATE_PED_BLIP(mdJoe.piDude)
						SET_BLIP_SCALE(mdJoe.biBlip, BLIP_SIZE_VEHICLE)
					//ENDIF
					TRIGGER_MUSIC_EVENT("MM3_TRACTOR")
					MissionSubState = SS_UPDATE_TWO
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_UPDATE_TWO
			VanController()
			SpawnTowTruck()
			MexicansFleeTwo()
			TriggerChaseDialogue()
			IsCarOk()
			IF bAudioReady
				IF GET_GAME_TIMER() > iAudioEventTimer
					TRIGGER_MUSIC_EVENT("MM3_START_STA")
					bAudioReady = FALSE
				ENDIF
			ENDIF
			IF bSetJoesCheckpoint AND GET_GAME_TIMER() > iJoesCheckpointTimer
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_KILL_JOE, "Kill Joe", TRUE)
				bSetJoesCheckpoint = FALSE
			ENDIF
			UPDATE_CHASE_BLIP(mdJoe.biBlip, mdJoe.piDude, iEscapeDist)
		BREAK
	ENDSWITCH
	
	// debug skip to next stage
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF IS_PED_UNINJURED(mdJoe.piDude)
				EXPLODE_PED_HEAD(mdJoe.piDude)
			ENDIF
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			DebugSkipKillJosef()
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Joe and Josef flee on foot
PROC FLEE_ON_FOOT()
	IF IsJoeDead()
		EXIT
	ENDIF
	TriggerChaseDialogue()
	VanController()
	MexicansFlee()
	MexicansFleeTwo()
	IF NOT DOES_ENTITY_EXIST(viTowTruck) AND NOT DOES_ENTITY_EXIST(piTowTruckDriver)
		LoadTowTruckModels()
	ENDIF
	SpawnTowTruck()
	UPDATE_CHASE_BLIP(mdJoe.biBlip, mdJoe.piDude, iEscapeDist)
	BOOST_JOES_CAPSULE_SIZE()
	
	BOOL joeOk = FALSE
	IF IS_PED_UNINJURED(mdJoe.piDude)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(mdJoe.piDude, PEDMOVE_RUN)
        SET_PED_MAX_MOVE_BLEND_RATIO(mdJoe.piDude, PEDMOVE_RUN)
		joeOk = TRUE
	ENDIF
	BOOL josefOk = FALSE
	IF IS_PED_UNINJURED(mdJosef.piDude)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(mdJosef.piDude, PEDMOVE_RUN)
        SET_PED_MAX_MOVE_BLEND_RATIO(mdJosef.piDude, PEDMOVE_RUN)
		josefOk = TRUE
	ENDIF
	IF joeOk AND josefOk
		// both are alive so focus on the nearest
		FLOAT joeDist = GET_DISTANCE_BETWEEN_PEDS(mdJoe.piDude, PLAYER_PED_ID())
		FLOAT josefDist = GET_DISTANCE_BETWEEN_PEDS(mdJosef.piDude, PLAYER_PED_ID())
		IF joeDist < josefDist
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJoe.piDude)
		ELSE
			CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJosef.piDude)
		ENDIF
	ELIF joeOk
		// Joe is alive, Josef is dead so focus on Joe
		CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJoe.piDude)
	ELIF josefOk
		// Josef is alive, Joe is dead so focus on Josef
		CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mdJosef.piDude)
	ENDIF
	
	IF bAudioReady
		IF GET_GAME_TIMER() > iAudioEventTimer
			TRIGGER_MUSIC_EVENT("MM3_START_STA")
			bAudioReady = FALSE
		ENDIF
	ENDIF
	
	IF BadGuysEscaped()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF IS_PED_UNINJURED(mdJosef.piDude)
				EXPLODE_PED_HEAD(mdJosef.piDude)
			ENDIF
			IF IS_PED_UNINJURED(mdJoe.piDude)
				EXPLODE_PED_HEAD(mdJoe.piDude)
			ENDIF
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			DebugSkipChaseJoe()
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Plays the mission passed dialogue
PROC PASS_DELAY()
	SWITCH MissionSubState
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_ALL_BLIPS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			REMOVE_PED_FOR_DIALOGUE(pedConvStruct, JOSEF_ID)
			REMOVE_PED_FOR_DIALOGUE(pedConvStruct, JOE_ID)
			CPRINTLN(DEBUG_MISSION, "Start PASS_DELAY")
			TRIGGER_MUSIC_EVENT("MM3_STOP")
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_LOW)
			
			MissionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MIN3AUD", "MIN3_KILL", "MIN3_KILL_2", CONV_PRIORITY_HIGH)
					MissionSubState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				Script_Passed()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Fade out for the fail screen
PROC FAIL_DELAY()
	SWITCH MissionSubState
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_ALL_BLIPS()

			STRING sFailReason
			
			IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[MANUEL])
				IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[MANUEL])
					MissionFailReason = MFR_KILLED_MANUEL
				ENDIF
			ENDIF

			// set the fail reason
			SWITCH MissionFailReason
				CASE MFR_NONE
					// no fail reaon
				BREAK
				
				CASE MFR_KILLED_MANUEL
					sFailReason = "MIN3_FAIL1"
				BREAK
				
				CASE MFR_KILLED_A_MEXICAN
					sFailReason = "MIN3_FAIL2"
				BREAK
				
				CASE MFR_INJURED_MANUEL
					sFailReason = "MIN3_FAIL3"
				BREAK
				
				CASE MFR_INJURED_A_MEXICAN
					sFailReason = "MIN3_FAIL4"
				BREAK
				
				CASE MFR_JOE_ESCAPED
					sFailReason = "MIN3_FAIL5"
				BREAK
				
				CASE MFR_EXECUTION
					sFailReason = "MIN3_FAIL8"
				BREAK
			ENDSWITCH
			
			TRIGGER_MUSIC_EVENT("MM3_FAIL")
			
			IF MissionFailReason = MFR_NONE
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			CPRINTLN(DEBUG_MISSION, "Start FAIL_DELAY")
			MissionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		
				DeleteEverything()
				
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK

	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	mScenarioBlocker = Minute3_Scenario_Blocker()

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		TRIGGER_MUSIC_EVENT("MM3_FAIL")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF

	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		IF GET_REPLAY_MID_MISSION_STAGE() = CP_AFTER_MOCAP
	      	g_bSceneAutoTrigger = TRUE
			eInitialSceneStage = IS_REQUEST_SCENE
			WHILE NOT SetupScene_MINUTE_3(sRCLauncherDataLocal)
		  		WAIT(0)
			ENDWHILE
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			g_bSceneAutoTrigger = FALSE
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "Restart"
		sSkipMenu[1].sTxtLabel = "Kill Josef"
		sSkipMenu[2].sTxtLabel = "Car Chase"
	#ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOE, TREVOR_BOOK, FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSEF, TREVOR_BOOK, FALSE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_MMB")
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		//DISPLAY_POLY2(tpFarmArea)
		
		CheckForStunGunUse()
		KillConversationsInShops()
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SWITCH missionStage
				CASE MS_INIT
					INIT()
				BREAK
				
				CASE MS_PRE_INTRO
					PRE_INTRO()
				BREAK
				
				CASE MS_INTRO_PLAYING
					INTRO_PLAYING()
				BREAK
				
				CASE MS_DRIVE_TO_FARM
					DRIVE_TO_FARM()
				BREAK
				
				CASE MS_JOSEF_ATTACKS
					JOSEF_ATTACKS()
				BREAK
				
				CASE MS_SEARCH_FARM
					SEARCH_FARM()
				BREAK
				
				CASE MS_CAR_CHASE
					CAR_CHASE()
				BREAK
				
				CASE MS_FLEE_ON_FOOT
					FLEE_ON_FOOT()
				BREAK
				
				CASE MS_PASS_DELAY
					PASS_DELAY()
				BREAK
				
				CASE MS_FAIL_DELAY
					FAIL_DELAY()
				BREAK
			ENDSWITCH
		ENDIF
		
		// check if any mission essential peds have been killed or hurt by the player
		IF missionStage <> MS_FAIL_DELAY AND missionStage <> MS_INIT AND missionStage <> MS_PRE_INTRO AND missionStage <> MS_INTRO_PLAYING
			Check_Essential_Ped(sRCLauncherDataLocal.pedID[MANUEL])
			CheckPlayerHurtsPed(sRCLauncherDataLocal.pedID[MANUEL])
			CheckManuelsCar()
			INT i = 0
			REPEAT NUM_MEXICANS i
				Check_Essential_Ped(piMexican[i])
				CheckPlayerHurtsPed(piMexican[i])
			ENDREPEAT
			i = 0
			REPEAT NUM_MEXICANS_2 i
				Check_Essential_Ped(piMexican2[i])
				CheckPlayerHurtsPed(piMexican2[i])
			ENDREPEAT
			
			// Check debug completion/failure
			#IF IS_DEBUG_BUILD
				DEBUG_Check_Debug_Keys()
			#ENDIF
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

