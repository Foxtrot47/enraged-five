

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Minute.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Minute.sc
//
//		AUTHOR		:	David Roberts / Andrew Minghella
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
//
// 						If multiple missions share same coord or are within close proximity,
//						we should just add one world point, and adjust the tolerance float.
// *****************************************************************************************

// ------------Variables----------------------
CONST_FLOAT 			WORLD_POINT_COORD_TOLERANCE 1.0
INT 					iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST // ID to register off-mission cutscene load request with cutscene_controller.
SCENARIO_BLOCKING_INDEX mScenarioBlocker
BLIP_INDEX  			biLeadIn

ENUM convPauseState
	CONVPS_READY,
	CONVPS_PAUSED,
	CONVPS_PLAYING,
	CONVPS_COMPLETE
ENDENUM
convPauseState convState = CONVPS_READY

// ----------Functions -------------------------
/// PURPOSE: 
///    Play a conversation in full from two RC peds
FUNC BOOL PLAY_LAUNCHER_FULL_CONVERSATION_ONCE(g_structRCScriptArgs& sData, VECTOR v_ped_pos, STRING s_dialogue_slot, STRING s_dialogue_to_play, INT ped0_dialogue_number, STRING ped0_dialogue_name, INT ped1_dialogue_number, STRING ped1_dialogue_name, convPauseState& convPS)
	
	IF NOT (convPS = CONVPS_COMPLETE)
		IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_CHARACTER)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_ENTITY_ALIVE(sData.pedID[0])
				AND IS_ENTITY_ALIVE(sData.pedID[1])
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ped_pos) < 20.0
					PRINT_LAUNCHER_DEBUG("start conversation")
					structPedsForConversation s_conversation  
					IF (convPS = CONVPS_READY)
						ADD_PED_FOR_DIALOGUE(s_conversation, ped0_dialogue_number, sData.pedID[0], ped0_dialogue_name)
						ADD_PED_FOR_DIALOGUE(s_conversation, ped1_dialogue_number, sData.pedID[1], ped1_dialogue_name)
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation, s_dialogue_slot, s_dialogue_to_play, CONV_PRIORITY_MEDIUM)
						convPS = CONVPS_PLAYING
					ELIF (convPS = CONVPS_PLAYING)
						PRINT_LAUNCHER_DEBUG("conversation ended")
						convPS = CONVPS_COMPLETE
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF RC_IS_THIS_CONVERSATION_ROOT_PLAYING(s_dialogue_to_play)
				AND NOT (convPS = CONVPS_PAUSED)
				AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ped_pos) >= 20.0
					PRINT_LAUNCHER_DEBUG("pause conversation")
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					convPS = CONVPS_PAUSED
				ENDIF
			ENDIF
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_ped_pos) < 20.0)
			AND (convPS = CONVPS_PAUSED)
				PRINT_LAUNCHER_DEBUG("unpause conversation")
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				convPS = CONVPS_PLAYING
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_JOE_JOSEF_CUTSCENE_PROPS()
	
	// B*1420964 - Ensure Joe and Josef spawn their props
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		CPRINTLN(DEBUG_MISSION, "CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY is TRUE")
		//SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_HEAD, 0, 0) // Setting a hat on Joe causes B*1509088
		SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_EYES, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_HEAD, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_EYES, 0, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
/// PARAMS:
///    sData - launcher data struct
///    bCleanupEntities - do we want to cleanup the entities in the launcher struct
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)
	
	IF bCleanupEntities
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Cleaning up entities in Launcher")
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	// Remove lead-in blip
	IF DOES_BLIP_EXIST(biLeadIn)
		REMOVE_BLIP(biLeadIn)
	ENDIF
	
	IF sData.eMissionID = RC_MINUTE_1
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<322,3409,35>>-<<20,20,20>>, <<322,3409,35>>+<<20,20,20>>, TRUE)
	ELIF sData.eMissionID = RC_MINUTE_3
		REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker) // Remove scenario blocking areas
	ENDIF
	
	// Unload launcher animation dictionary
	REMOVE_LAUNCHER_ANIM_DICT(sData.sAnims)
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF
	
	// Stop launcher conversation
	STRING sConversationRoot 
	SWITCH sData.eMissionID
		CASE RC_MINUTE_1
			sConversationRoot = "MIN1_AMB"
		BREAK
		CASE RC_MINUTE_3
			sConversationRoot = "MIN3_AMB"
		BREAK
	ENDSWITCH
	RC_STOP_LAUNCHER_DIALOGUE(sConversationRoot)
	
	//B*1574385 - Force update to blip in RandChar Controller if mission wasn't launched
	IF bCleanupEntities
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Creates the initial scene
/// PARAMS:
///    sData - launcher data struct
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)
	
	//Setup the correct initial scene
	SWITCH sData.eMissionID
		
		CASE RC_MINUTE_1
			IF NOT SetupScene_MINUTE_1(sData)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE RC_MINUTE_2
			IF NOT SetupScene_MINUTE_2(sData)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE RC_MINUTE_3
			IF NOT SetupScene_MINUTE_3(sData)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Custom version of function that ignores height    
FUNC BOOL ARE_VECTORS_ALMOST_EQUAL_2D(VECTOR v1, VECTOR v2, FLOAT fTolerance = 0.5)
	
	IF fTolerance < 0
		fTolerance = 0
	ENDIF
	
	IF ABSF(v1.x - v2.x) <= fTolerance
		IF ABSF(v1.y - v2.y) <= fTolerance
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)

	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs 	sRCLauncherData			// Scene information to pass to mission script
	VECTOR				 	vInCoords = <<0,0,0>>	// Stores world point location
	
	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update world point
	vInCoords = in_coords.vec_coord[0]

	// Determine which RC mission we are attempting to launch (currently done on a launcher by launcher basis)
	g_eRC_MissionIDs eRCMissions[3]
	eRCMissions[0] = RC_MINUTE_1
	eRCMissions[1] = RC_MINUTE_2
	eRCMissions[2] = RC_MINUTE_3

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF

	//Pick which mission activated us
	IF NOT DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vInCoords, WORLD_POINT_COORD_TOLERANCE)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - Don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - Don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Shutdown launcher when we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - Don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE
	
	// Setup for specific RCM's
	IF sRCLauncherData.eMissionID = RC_MINUTE_1
		
		// Remove vehicle gens
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<322,3409,35>>-<<20,20,20>>, <<322,3409,35>>+<<20,20,20>>, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<322,3409,35>>-<<20,20,20>>, <<322,3409,35>>+<<20,20,20>>)
		CLEAR_AREA(vInCoords, 40, TRUE)

	ELIF sRCLauncherData.eMissionID = RC_MINUTE_3
		
		// Add scenario blocking area
		mScenarioBlocker = Minute3_Scenario_Blocker()
	ENDIF
	
	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vInCoords, sRCLauncherData.activationRange, TRUE)
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		
		WAIT(0)
		
		//Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
			Script_Cleanup(sRCLauncherData)
		ENDIF
		
		// Minute 1 specific checks
		IF sRCLauncherData.eMissionID = RC_MINUTE_1
			
			// Setup prop variations
			SETUP_JOE_JOSEF_CUTSCENE_PROPS()
			
			// Check for player moving the SURFER2 vehicle
			IF IS_VEHICLE_OK(sRCLauncherData.vehID[1])	
				IF NOT ARE_VECTORS_ALMOST_EQUAL_2D(<<329.513,3402.691,36.258>>, GET_ENTITY_COORDS(sRCLauncherData.vehID[1]))
					PRINT_LAUNCHER_DEBUG("Player has moved SURFER2 vehicle [TERMINATING]")
					MAKE_ALL_INITIAL_SCENE_PEDS_FLEE(sRCLauncherData)
					Script_Cleanup(sRCLauncherData)
				ENDIF
			ENDIF
		ENDIF
		
		//Update launcher blip + preload intro
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vInCoords)
		
		IF sRCLauncherData.eMissionID = RC_MINUTE_1
			PLAY_LAUNCHER_FULL_CONVERSATION_ONCE(sRCLauncherData, vInCoords, "MIN1AUD", "MIN1_AMB", 3, "JOE", 4, "JOSEF", convState)
		ELIF sRCLauncherData.eMissionID = RC_MINUTE_3
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "MIN3AUD", "MIN3_AMB", 3, "MANUEL", 5000)
		ENDIF
		
		//Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)
			
			// Create mission blip that persists for lead-in scene
			// Original will be removed when RC controller shuts down
			IF DOES_RC_MISSION_HAVE_LEAD_IN(sRCLauncherData.eMissionID)
				CREATE_BLIP_FOR_LEAD_IN(sRCLauncherData.eMissionID, biLeadIn)
			ENDIF
			
			//Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			//Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				Script_Cleanup(sRCLauncherData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
			ENDIF	
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
