
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "CompletionPercentage_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "commands_physics.sch"
USING "script_ped.sch"
USING "locates_public.sch"
USING "dialogue_public.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Minute.sch"
USING "buddy_head_track_public.sch"
USING "commands_event.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

USING "RC_Helper_Functions.sch"
USING "RC_Threat_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Minute2.sc
//		AUTHOR			:	Tom Kingsley
//		DESCRIPTION		:	Catch "illegal" immigrants with Josef and Joe
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
CONST_INT ic_JOE 0
CONST_INT ic_JOSEF 1
// The Random Character - sRCLauncherDataLocal.pedID[ic_JOSEF]
g_structRCScriptArgs sRCLauncherDataLocal
//---------------------------------------------------------------

VEHICLE_INDEX vehChaseCar[4]
VEHICLE_INDEX vehChaseCarChase2
VEHICLE_INDEX vehSpeedo
VEHICLE_INDEX vehDuster
VEHICLE_INDEX vehRandom

PED_INDEX pedImmigrant[4]
PED_INDEX pedImmigrantChase2
PED_INDEX pedWorker[10]
PED_INDEX pedDusterPilot

BLIP_INDEX blipImmigrant[4]
BLIP_INDEX blipLocate[4]
BLIP_INDEX blipPlayerCar
BLIP_INDEX blipJosef
BLIP_INDEX blipJoe
BLIP_INDEX blipImmigrantChase2

SEQUENCE_INDEX seqChase1
SEQUENCE_INDEX seqChase2Route
SEQUENCE_INDEX seqChase2RouteB
SEQUENCE_INDEX seqChase2RouteC
SEQUENCE_INDEX seqChase2RouteD
SEQUENCE_INDEX seqChase2RouteSECONDPED
SEQUENCE_INDEX seqChase2RouteBSECONDPED
SEQUENCE_INDEX seqChase2RouteCSECONDPED
SEQUENCE_INDEX seqChase2RouteDSECONDPED
SEQUENCE_INDEX seqPointAtImmigrants
SEQUENCE_INDEX seqJosefThrowPapers

VEHICLE_INDEX vehTrafficRoute1[15]
PED_INDEX pedTrafficRoute1[15]
VECTOR vTrafficRoute1[15]
FLOAT fHeadingTrafficRoute1[15]

VEHICLE_INDEX vehTrafficRoute2[15]
PED_INDEX pedTrafficRoute2[15]
VECTOR vTrafficRoute2[15]
FLOAT fHeadingTrafficRoute2[15]

INT iTraffic

INT ssClimbToBack_1
INT ssClimbToBack_1B
INT ssClimbToBack_2

INT ssPassport

INT ssManuelSitBack
INT ssManuelSitBack2
INT ssPed2SitBack
INT iPed1getIn

//OBJECT_INDEX ObjPhone
OBJECT_INDEX ObjPapers

//SCENARIO_BLOCKING_INDEX blockScenarios

SCENARIO_BLOCKING_INDEX sbiCement

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 4
	INT iReturnStage                                       
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]     
#ENDIF

structPedsForConversation s_conversation_peds

//---------------------------------------------------------------	

CONST_INT DEFAULT_TIME_BEFORE_WARP_MINUTE_2 60000

FLOAT fEscortDistance = 12	//10

INT i = 0
//INT iGameTimer
//INT iGameTimerUpdate
INT iMultiHeadTimer
INT iChaseStartedTimer
INT iChaseStartedTimerUpdate
INT iConvoTimer
INT iConvoTimerUpdate
INT iConvo
INT iJosefCutSeq
INT iRangerHP
INT iGameTimerPed1KnockedOffUpdate
INT iGameTimerPed1KnockedOff
INT iGameTimerPed2KnockedOffUpdate
INT iGameTimerPed2KnockedOff
//INT iGameTimerSecondPedGettingAway
//INT iGameTimerSecondPedGettingAwayUpdate
INT iTimesTazed
INT iTimesTazedSECONDPED
//INT iNextCoordPed1
INT iArrivedAtCementFactory
INT iSeqCementFactory
INT iTimerJoeLeftCar
INT iTimerDontLetTheOtherEscape
INT iConvoGetBackInCar
INT iTimerPlayerNotInCar
INT iConvoTrevorDamagingCar
INT iRollingStartTimer
INT iTimerCarHorn
INT iSeqJosefThrowPapers	
INT iTimerPassDriveOff
INT iGetOutOfCarLines
INT iSeqHintCam = 0
INT iTimerHintCamSwitch
INT iPedsStunnedOffBike = 0
INT iTimerChase1Started
INT iTimerGetOutOfCarLines
INT iTimerCatchUpLines
INT iCatchUpLines = 0
INT iPed2Kneeling
INT iPedKneeling
INT iPedInWater
INT iPed2InWater
INT iCrueltyLines
INT iTimerCrueltyLines
INT iSyncPed = 0
INT iTimerJackTrev
INT iGoBackLines
INT iTimerGoBackLines
INT iSeqChase2Dialogue	
INT iDialogueStopping
INT iTimerDialogueStopping
INT iDialogueCrashed
INT iTimerDialogueCrashed
INT iShoutLines
INT iTimerShoutLines
INT iTimerPed1Ragdoll
INT iTimerPed2Ragdoll

INT iShoutSurr1
INT iShoutSurr3
INT iShoutSurr5
							
VECTOR vpedImmigrant[3]
VECTOR vSafeCoord
VECTOR vPlayer
VECTOR vChase[55]

//VECTOR vNodeSwitch1
//VECTOR vNodeSwitch2

FLOAT fDistBetweenJosefAndImmigrant
FLOAT fDistBetweenJoeAndImmigrant
FLOAT fDistBetweenCarAndMigrant	
FLOAT fDistBetweenPlayerAndImmigrant
//FLOAT fDistBetweenBikers
//FLOAT fDistBiker1ToNextCoord
//FLOAT fDistBiker2ToNextCoord
FLOAT fRangerPetrolTankHP
FLOAT fRangerEngineHP
FLOAT fPlaybackSpeed
FLOAT fTrafficStopped

BOOL bOkForJosefToLeaveCar
BOOL bOkForJoeToLeaveCar
BOOL bBloke1AttachedToCar
BOOL bBloke2AttachedToCar
BOOL bOkToStartChase
BOOL bBlokestunned
BOOL bImmigrantHandsUp
BOOL bJosefCombatTaskGiven
BOOL bJoeCombatTaskGiven
BOOL bGettingAwayMessagePrinted
BOOL bGetCloseMessagePrinted
BOOL bImmigrantGettingIntoCar
BOOL bDrivingToMotel
BOOL bEveryoneInCar
BOOL bSecondGettingAwayMessagePrinted
BOOL bpedImmigrantChase2Attached
BOOL bChaseStarted
BOOL bSECONDBlokestunned
BOOL bImmigrantChase2HandsUp
BOOL bImmigrantChase2GettingIntoCar
BOOL bSeqSetup
BOOL bFirstImmigrantOnBike
BOOL bImmigrantChase2OnBike
BOOL bDebugSkippedToCementFactory
BOOL bPlayerAtChaseOnFootMessagePrinted
BOOL bImmigrantChase2Fleeing
BOOL bFirstImmigrantFleeing
BOOL bMissionFailed
BOOL bDebugSkipping
BOOL bDialogueDontletTheOtherEscape
BOOL bConvoSpottedImmigrant1	
BOOL bCarHorn
BOOL bConvoImmigrantManuelGiveUp
BOOL bConvoImmigrant1GiveUp
BOOL bConvoImmigrant2GiveUp
BOOL bCementFactoryCheckpoint
BOOL bPlayerNotInCarBetweenCapturingImmigrants = FALSE
BOOL bPrint31 = FALSE
BOOL bBikeEscorting = FALSE
BOOL bBikeLeftFactory = FALSE
BOOL bWander = FALSE	
BOOL bCloseFinalTimeWindowForStats = FALSE	
BOOL bReplayCementFactory = FALSE
BOOL bSecondMusicCue = FALSE
BOOL bCreateNmMessage = FALSE
BOOL bRagdollTaskGiven = FALSE
BOOL bCreateNmMessagePed2 = FALSE
BOOL bRagdollTaskGivenPed2 = FALSE
BOOL bLosingCops = FALSE
BOOL bPrintLosingCops = FALSE
BOOL bPassport = FALSE
BOOL bManuelAttached = FALSE
BOOL bPassportSound = FALSE	
BOOL bButterFingers = FALSE
BOOL bWeaponSwap = FALSE
BOOL bWarpJosef = FALSE
BOOL bThanks = FALSE
BOOL CCSExitTrev
BOOL CCSExitJosef
BOOL CCSExitJoe
BOOL CCsExitTruck
BOOL CCsExitCam
BOOL bPatrolVehDialogue
BOOL bDialogueGoBack
BOOL bDialoguePickUpLastGuy
BOOL bDialogueOkLetsGo 
BOOL bDialogueDriveOff
BOOL bDoDelayedClimbInBack
BOOL bDoneDelayedClimbInBack
BOOL bDisableAbortConvo = FALSE
BOOL bDisableAbortConvoPed2 = FALSE
BOOL bSwitchWaypointFlags = FALSE
BOOL bMultiheadTriggered = FALSE

BOOL bDialogueLeftJorJ
INT iDialogueGoBackJorJ
INT iTimerGoBackJorJ

VECTOR vStorePlayerPosForWrongWayDialogue
INT iDialogueWrongWay
								
BOOL bBlipJoe
BOOL bBlipJosef
BOOL bBlipCar

BOOL bPrint18            //bools for text
BOOL bPrint03
BOOL bPrint06
BOOL bPrint08
BOOL bPrint33
BOOL bPrint29
BOOL bPrint32
BOOL bPrint19
BOOL bPrint42
BOOL bPrint01
BOOL bPrint30
BOOL bPrint39
BOOL bPrint37
BOOL bPrint47
BOOL bPrint49

BOOL bPrint48GoBack = FALSE

BOOL bPed1GettingUp = FALSE
INT iTimerPed1GettingUp
BOOL bPed2GettingUp = FALSE
INT iTimerPed2GettingUp

STRING sFailReason

BOOL bJosefGetIn

//----------------------------------------------------

ENUM JOSEFSTATE
	JS_AIM,
	JS_AIM_WALK,
	JS_RUN
ENDENUM

ENUM FAIL_STATE
	FS_SETUP,
	FS_UPDATE,
	FS_CLEANUP
ENDENUM	

ENUM CUTSCENE_STAGE
	eCutSpawnJJ,
	eCutInit,
	eCutUpdate,
	eCutCleanup
ENDENUM	

FAIL_STATE eFAIL_STATE

CUTSCENE_STAGE eCutsceneState = eCutSpawnJJ

//---------------------------------------------------------

// Mission stages
ENUM MISSION_STAGE
	MS_INTRO_MOCAP,
	MS_SETUP_MISSION,
	MS_SETUP_CHASE1,
	MS_UPDATE_CHASE1,
	MS_JOSEF_TAZERED_IMMIGRANT1,
	MS_IMMIGRANT1_APPREHENDED,
	MS_SETUP_CHASE2,
	MS_CEMENT_FACTORY,
	MS_UPDATE_CHASE2,
	MS_JOSEF_TAZERED_IMMIGRANT2,
	MS_JOSEF_PICKED_UP_IMMIGRANT2,
	MS_SETUP_CHASE3,
	MS_UPDATE_CHASE3,
	MS_JOSEF_TAZERED_IMMIGRANT3,
	MS_JOSEF_PICKED_UP_IMMIGRANT3,
	MS_DRIVE_TO_END,
	MS_MISSION_FAILING,
	MS_MISSION_PASSING
ENDENUM

MISSION_STAGE missionStage = MS_INTRO_MOCAP

/// PURPOSE: Remove all blips
PROC RemoveBlips()

	SAFE_REMOVE_BLIP(blipImmigrant[0])
	SAFE_REMOVE_BLIP(blipImmigrant[1])
	SAFE_REMOVE_BLIP(blipImmigrant[2])
	SAFE_REMOVE_BLIP(blipImmigrant[3])
	SAFE_REMOVE_BLIP(blipLocate[0])
	SAFE_REMOVE_BLIP(blipLocate[1])
	SAFE_REMOVE_BLIP(blipLocate[2])
	SAFE_REMOVE_BLIP(blipLocate[3])
	SAFE_REMOVE_BLIP(blipPlayerCar)
	SAFE_REMOVE_BLIP(blipJosef)
	SAFE_REMOVE_BLIP(blipJoe)
	SAFE_REMOVE_BLIP(blipImmigrantChase2)

ENDPROC

PROC START_AUDIO_SCENE_CHASE()  

	IF NOT IS_AUDIO_SCENE_ACTIVE("MINUTE_02_SCENE")	
		IF IS_ENTITY_ALIVE(vehChaseCar[0])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehChaseCar[0], "MINUTE_02_SCENE_QUAD_BIKE")
		ENDIF	
		START_AUDIO_SCENE("MINUTE_02_SCENE")
	ENDIF
	
ENDPROC

PROC CLEANUP_AUDIO_SCENE_CHASE() 
	
	IF IS_AUDIO_SCENE_ACTIVE("MINUTE_02_SCENE")
		STOP_AUDIO_SCENE("MINUTE_02_SCENE")
	ENDIF

ENDPROC

FUNC BOOL IS_IT_OK_FOR_JOSEF_TO_GET_IN()
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbToBack_1)
	AND GET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1) > 0.35
		bJosefGetIn = TRUE
	ENDIF
	IF missionStage >= MS_CEMENT_FACTORY
	OR IS_SYNCHRONIZED_SCENE_RUNNING(ssManuelSitBack)
	OR IS_SYNCHRONIZED_SCENE_RUNNING(ssManuelSitBack2)
		bJosefGetIn = TRUE
	ENDIF

	RETURN bJosefGetIn
ENDFUNC

/// PURPOSE: Request assets and initialise
PROC Initialise() 
	
	SET_ROADS_IN_AREA(<<-199.2860, 4214.0547, 43.7470>> - <<10,10,10>>,<<-199.2860, 4214.0547, 43.7470>> + <<10,10,10>>,TRUE)  //For B*1435595
	
	//INFORM_MISSION_STATS_OF_MISSION_START_MINUTE_2()

	SET_VEHICLE_MODEL_IS_SUPPRESSED(SANCHEZ,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PRANGER,TRUE)
	
	REQUEST_MODEL(PRANGER) 
	
	REQUEST_MODEL(Prop_Passport_01)
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		iRangerHP = GET_ENTITY_HEALTH(sRCLauncherDataLocal.vehID[0])
		fRangerPetrolTankHP = GET_VEHICLE_PETROL_TANK_HEALTH(sRCLauncherDataLocal.vehID[0])
		fRangerEngineHP = GET_VEHICLE_ENGINE_HEALTH(sRCLauncherDataLocal.vehID[0])
		SET_ENTITY_HEALTH(sRCLauncherDataLocal.vehID[0],(iRangerHP * 3))
		SET_VEHICLE_PETROL_TANK_HEALTH(sRCLauncherDataLocal.vehID[0],(fRangerPetrolTankHP * 3))
		SET_VEHICLE_ENGINE_HEALTH(sRCLauncherDataLocal.vehID[0],(fRangerEngineHP * 3))
		SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherDataLocal.vehID[0], FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherDataLocal.vehID[0], FALSE)
		//SET_VEHICLE_MOD_KIT(sRCLauncherDataLocal.vehID[0],0)
		//SET_VEHICLE_WINDOW_TINT(sRCLauncherDataLocal.vehID[0],1)
	ENDIF
	
	REQUEST_ANIM_DICT("rcmminute2")	
	REQUEST_ANIM_DICT("rcmminute2lean")	
	
	REQUEST_ANIM_DICT("missminuteman_2ig_1")	
	REQUEST_ANIM_DICT("missminuteman_2ig_3")	

	REQUEST_ADDITIONAL_TEXT("MIN2",MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("MIN2AU",MISSION_DIALOGUE_TEXT_SLOT)
	
	REQUEST_NPC_PED_MODEL(CHAR_MANUEL)
	REQUEST_NPC_PED_MODEL(CHAR_JOSEF)
	REQUEST_NPC_PED_MODEL(CHAR_JOE)

	REQUEST_MODEL(BLAZER) 
	REQUEST_VEHICLE_ASSET(PRANGER)
	REQUEST_CLIP_SET("MOVE_M@BAIL_BOND_NOT_TAZERED")
	REQUEST_CLIP_SET("MOVE_M@BAIL_BOND_TAZERED")

	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	
	//blockScenarios = ADD_SCENARIO_BLOCKING_AREA(<< -280.1734, 4187.3604,203.9223 >>,<< -172.1141, 4235.1616, 70.4678 >>)
	
	sbiCement = ADD_SCENARIO_BLOCKING_AREA(<<341.1107, 2846.7971, 40.6462>>,<<350.9610, 2871.4719, 43.8518>>)

	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO",FALSE)		
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_CONSTRUCTION_SOLO", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_CONSTRUCTION_PASSENGERS", FALSE)
	
	vChase[0] = << 327.3949, 2850.5437, 42.4360 >>
	vChase[1] = << 327.0572, 2841.2034, 42.7507 >>
	vChase[2] = << 337.6567, 2821.3130, 41.1778 >>
	vChase[3] = << 350.4040, 2797.2534, 47.3014 >>
	vChase[4] = << 373.3039, 2757.9229, 42.0141 >>
	vChase[5] = << 393.3462, 2725.5056, 41.9263 >>
	//vChase[6] = << 413.7730, 2704.2275, 42.2798 >>
	vChase[6] = << 416.6357, 2697.7898, 42.4937 >>
	vChase[7] = << 444.9309, 2689.1550, 42.5934 >>
	vChase[8] = << 474.5866, 2679.4988, 42.2943 >>
	vChase[9] = << 636.1514, 2696.9749, 39.9808 >>
	vChase[10] = << 831.2220, 2695.5740, 39.5328 >>
	vChase[11] = << 880.9315, 2708.0527, 39.8050 >>
	vChase[12] = << 915.5502, 2715.8682, 39.6578 >>
	vChase[13] = << 949.9031, 2732.2583, 39.3729 >>
	vChase[14] = << 979.2463, 2748.7639, 37.0951 >>
	vChase[15] = << 1007.8654, 2761.7441, 35.0162 >>
	vChase[16] = << 1039.2654, 2779.2336, 33.8708 >>
	vChase[17] = << 1070.0198, 2810.3762, 35.6906 >>
	vChase[18] = << 1097.6158, 2838.8896, 37.2296 >>
	vChase[19] = << 1121.2598, 2844.5132, 37.2992 >>
	vChase[20] = << 1149.2218, 2836.6943, 37.1993 >>
	vChase[21] = << 1203.2163, 2801.9336, 36.8600 >>
	vChase[22] = << 1253.4020, 2767.1658, 37.4919 >>
	vChase[23] = << 1283.3680, 2741.5134, 36.6510 >>
	vChase[24] = << 1297.2052, 2715.3333, 36.7748 >>
	vChase[25] = << 1303.8243, 2693.4988, 36.6285 >>
	vChase[26] = << 1312.9229, 2659.9019, 36.6804 >>
	vChase[27] = << 1323.7251, 2622.2717, 36.6871 >>
	vChase[28] = << 1337.2678, 2595.0356, 36.6180 >>
	vChase[29] = << 1359.4213, 2571.5676, 36.9024 >>
	vChase[30] = << 1478.0682, 2269.0791, 71.6347 >>
	vChase[31] = << 1506.9707, 1874.8892, 106.8773 >>
	vChase[32] = << 1501.8845, 1618.5989, 111.4967 >>
	vChase[33] = << 1474.6619, 1583.4229, 109.2177 >>
	vChase[34] = << 1447.2242, 1574.7394, 107.6650 >>
	vChase[35] = << 1421.3652, 1570.7217, 107.0452 >>
	vChase[36] = << 1393.5858, 1566.8616, 106.1980 >>
	vChase[37] = << 1363.6519, 1552.6820, 104.8892 >>
	vChase[38] = << 1344.7573, 1535.9220, 102.4010 >>
	vChase[39] = << 1323.5651, 1507.2051, 98.0900 >>
	vChase[40] = << 1305.1832, 1461.4269, 98.0123 >>
	vChase[41] = << 1294.0465, 1395.4415, 100.7730 >>
	vChase[42] = << 1287.9890, 1316.5039, 105.9520 >>
	vChase[43] = << 1238.2271, 705.9078, 100.6087 >>
	vChase[44] = << 1652.5648, 3487.9536, 35.5959 >>
	vChase[45] = << 2518.1792, 4145.9326, 37.7126 >>
	vChase[46] = << 2731.3145, 4390.3496, 47.4449 >>
	/*	
	REQUEST_MODEL(EMPEROR2)
	REQUEST_MODEL(SURFER2)
	REQUEST_MODEL(BISON)
	REQUEST_MODEL(TORNADO3)
	REQUEST_MODEL(STANIER)
	REQUEST_MODEL(MESA)
	*/
	REQUEST_WAYPOINT_RECORDING("Min2_Traffic01")
	REQUEST_WAYPOINT_RECORDING("Min2_Traffic02")
		
ENDPROC

/// PURPOSE: Wait for assets
PROC WaitForAssets()

	WHILE NOT HAS_MODEL_LOADED(PRANGER) 
	OR NOT HAS_MODEL_LOADED(BLAZER) 
	OR NOT HAS_MODEL_LOADED(Prop_Passport_01) 
	OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_MANUEL)
	OR NOT HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_NOT_TAZERED")	
	OR NOT HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_TAZERED")	
	OR NOT HAS_ANIM_DICT_LOADED("rcmminute2")
	OR NOT HAS_ANIM_DICT_LOADED("rcmminute2lean")
	OR NOT HAS_ANIM_DICT_LOADED("missminuteman_2ig_1")
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)		
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)	
		WAIT(0)
	ENDWHILE
	
ENDPROC

/// PURPOSE: Request assets for second chase
PROC RequestModelsSecondChase()

	REQUEST_WAYPOINT_RECORDING("Min2_Bike02")
	//REQUEST_VEHICLE_RECORDING(343,"Min2_Dust")
	REQUEST_VEHICLE_RECORDING(269,"Min2Rolling")
	REQUEST_VEHICLE_RECORDING(101,"Min2DB1")
	REQUEST_VEHICLE_RECORDING(102,"Min2DB2")
	REQUEST_MODEL(SPEEDO)
	REQUEST_MODEL(S_M_M_GAFFER_01)
	//REQUEST_MODEL(P_AMB_PHONE_01)
	//REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(SANCHEZ) 
	REQUEST_MODEL(S_M_M_Migrant_01)
	//REQUEST_MODEL(DUSTER)

ENDPROC

/// PURPOSE: Wait for assets to load
PROC LoadModelsSecondChase()

	WHILE NOT HAS_MODEL_LOADED(SPEEDO)
	OR NOT HAS_MODEL_LOADED(S_M_M_GAFFER_01)
	//OR NOT HAS_MODEL_LOADED(P_AMB_PHONE_01)
	//OR NOT HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	OR NOT HAS_MODEL_LOADED(SANCHEZ)
	OR NOT HAS_MODEL_LOADED(S_M_M_Migrant_01) 
	//OR NOT HAS_MODEL_LOADED(DUSTER) 
	//OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(343,"Min2_Dust")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(269,"Min2Rolling")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(101,"Min2DB1")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(102,"Min2DB2")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Min2_Bike02")
		WAIT(0)
	ENDWHILE
	
ENDPROC

/// PURPOSE: Wait for assets to load
FUNC BOOL HaveDoneLoadModelsSecondChase()

	IF NOT HAS_MODEL_LOADED(SPEEDO)
	OR NOT HAS_MODEL_LOADED(S_M_M_GAFFER_01)
	//OR NOT HAS_MODEL_LOADED(P_AMB_PHONE_01)
	//OR NOT HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	OR NOT HAS_MODEL_LOADED(SANCHEZ)
	OR NOT HAS_MODEL_LOADED(S_M_M_Migrant_01) 
	//OR NOT HAS_MODEL_LOADED(DUSTER) 
	//OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(343,"Min2_Dust")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(269,"Min2Rolling")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(101,"Min2DB1")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(102,"Min2DB2")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Min2_Bike02")
		RETURN FALSE
	ENDIF
	RETURN TRUE

ENDFUNC

PROC DO_VEHICLE_WEAPON_SWAP()

	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT bWeaponSwap	
				WEAPON_TYPE wep
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wep)
				IF wep <> WEAPONTYPE_STUNGUN
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,TRUE)
				ENDIF
				bWeaponSwap = TRUE
			ENDIF
		ELSE
			bWeaponSwap = FALSE
		ENDIF
	ENDIF
					
ENDPROC

/// PURPOSE: Transition to idles after sync scenes
PROC RESOLVE_IDLES()

	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	IF ARE_STRINGS_EQUAL(label,"MIN2_IMM1_3")				
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])			
	
		ROLL_DOWN_WINDOWS(sRCLauncherDataLocal.vehID[0])
	
		IF NOT DOES_ENTITY_EXIST(ObjPapers)	
			ObjPapers = CREATE_OBJECT_NO_OFFSET(Prop_Passport_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<0,0,20>>))
		ENDIF
		
		bManuelAttached = bManuelAttached
		
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
			ssPassport = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
			//ssPassport = CREATE_SYNCHRONIZED_SCENE(<<0,-0.05,-0.05>>,<<0,0,0>>)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssPassport,TRUE)
			PROCESS_ENTITY_ATTACHMENTS(ObjPapers)
			PROCESS_ENTITY_ATTACHMENTS(sRCLauncherDataLocal.pedID[ic_JOSEF])
			PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[0])
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssPassport,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_l"))
			PLAY_SYNCHRONIZED_ENTITY_ANIM(ObjPapers,ssPassport,"passport_prop","missminuteman_2ig_3",INSTANT_BLEND_IN,SLOW_BLEND_OUT)
			TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[ic_JOSEF],ssPassport,"missminuteman_2ig_3","passport_josef",2,-2,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
			TASK_SYNCHRONIZED_SCENE(pedImmigrant[0],ssPassport,"missminuteman_2ig_3","passport_manuel",1,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[ic_JOSEF])
			//IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[0],"missminuteman_2ig_3","passport_manuel")
			//	TASK_PLAY_ANIM(pedImmigrant[0],"missminuteman_2ig_3","passport_manuel",2,NORMAL_BLEND_OUT,-1,AF_NOT_INTERRUPTABLE)
			//ENDIF
		ENDIF
		
		bPassport = TRUE
	
	ENDIF

	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbToBack_1B)
	AND GET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1B) = 1
		PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[0])
		/*
		ATTACH_ENTITY_TO_ENTITY(pedImmigrant[0],sRCLauncherDataLocal.vehID[0],-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[0])),<<0,0,270>>)
		IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[0],"missminuteman_2ig_1","trunk_manuel")
			TASK_PLAY_ANIM(pedImmigrant[0],"missminuteman_2ig_1","trunk_manuel",2,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_NOT_INTERRUPTABLE)
			SET_RAGDOLL_BLOCKING_FLAGS(pedImmigrant[0],RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
		ENDIF
		*/
		ssManuelSitBack2 = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[0])),<<0,0,270>>)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssManuelSitBack2,FALSE)
		SET_SYNCHRONIZED_SCENE_LOOPED(ssManuelSitBack2,TRUE)
		PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[0])
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssManuelSitBack2,sRCLauncherDataLocal.vehID[0],-1)
		SET_PED_KEEP_TASK(pedImmigrant[0],TRUE)
		TASK_SYNCHRONIZED_SCENE(pedImmigrant[0],ssManuelSitBack2,"missminuteman_2ig_1","trunk_manuel",SLOW_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrant[0])
	ENDIF

	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbToBack_1)
		IF GET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1) = 1 //> 0.99
			PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[0])
			/*
			ATTACH_ENTITY_TO_ENTITY(pedImmigrant[0],sRCLauncherDataLocal.vehID[0],-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[0])),<<0,0,270>>)
			IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[0],"missminuteman_2ig_1","trunk_manuel")
				TASK_PLAY_ANIM(pedImmigrant[0],"missminuteman_2ig_1","trunk_manuel",2,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_NOT_INTERRUPTABLE)
				SET_RAGDOLL_BLOCKING_FLAGS(pedImmigrant[0],RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
			ENDIF
			//GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r")))
			*/
			/*
			VECTOR vManuelBackseat = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[0]))
			PRINTVECTOR(vManuelBackseat)
			PRINTNL()
			*/
			ssManuelSitBack = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[0])),<<0,0,270>>)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssManuelSitBack,FALSE)
			SET_SYNCHRONIZED_SCENE_LOOPED(ssManuelSitBack,TRUE)
			SET_SYNCHRONIZED_SCENE_RATE(ssManuelSitBack,1.6)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssManuelSitBack,sRCLauncherDataLocal.vehID[0],-1)
			TASK_SYNCHRONIZED_SCENE(pedImmigrant[0],ssManuelSitBack,"missminuteman_2ig_1","trunk_manuel",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)	
		ENDIF
	ELSE
		IF bPassport = TRUE	
			IF IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
				IF DOES_ENTITY_EXIST(ObjPapers)	
					IF bPassportSound = FALSE	
						IF GET_SYNCHRONIZED_SCENE_PHASE(ssPassport) >= 0.544	
							PLAY_SOUND_FROM_ENTITY(-1,"PASSPORT",ObjPapers,"MINUTE_02_SOUNDSET")
							bPassportSound = TRUE	
						ENDIF
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(ssPassport) >= 0.562	
					IF DOES_ENTITY_EXIST(ObjPapers)		
						STOP_SYNCHRONIZED_ENTITY_ANIM(ObjPapers,NORMAL_BLEND_DURATION,TRUE)
						ACTIVATE_PHYSICS(ObjPapers)
						//SET_OBJECT_AS_NO_LONGER_NEEDED(ObjPapers)
					ENDIF
				ENDIF
			ENDIF
			IF NOT bWarpJosef
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
				OR GET_SYNCHRONIZED_SCENE_PHASE(ssPassport) = 1
					PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[0])
					ssClimbToBack_1B = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssClimbToBack_1B,TRUE)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_1B,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
					TASK_SYNCHRONIZED_SCENE(pedImmigrant[0], ssClimbToBack_1B, "missminuteman_2ig_1", "entertrunk_manuel", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
					SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1B,1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrant[0],TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[ic_JOSEF])
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
					TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[ic_JOSEF],"missminuteman_2ig_3","passport_josef",SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT,-1,AF_DEFAULT,0.99)
					bWarpJosef = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDoDelayedClimbInBack
	AND NOT bDoneDelayedClimbInBack
		IF iSyncPed = 1	
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])	
				IF NOT IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[i])
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrant[i])
					//RESET_PED_MOVEMENT_CLIPSET(pedImmigrant[i])
					//ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0.39,-0.8,0.171>>,<<0,0,0>>)
					//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],-1)
					ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssClimbToBack_2,TRUE)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
					TASK_SYNCHRONIZED_SCENE(pedImmigrant[i], ssClimbToBack_2, "missminuteman_2ig_1", "entertrunk_josef", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
					//SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2,0.06)
					SET_SYNCHRONIZED_SCENE_RATE(ssClimbToBack_2,1.4)
					bDoneDelayedClimbInBack = TRUE
				ENDIF
			ENDIF
		ENDIF	
		IF iSyncPed = 2
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])		
				IF NOT IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					PROCESS_ENTITY_ATTACHMENTS(pedImmigrantChase2)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrantChase2)
					//RESET_PED_MOVEMENT_CLIPSET(pedImmigrantChase2)
					//ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0.39,-0.8,0.171>>,<<0,0,0>>)
					//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],-1)
					ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
					TASK_SYNCHRONIZED_SCENE(pedImmigrantChase2, ssClimbToBack_2, "missminuteman_2ig_1", "entertrunk_josef", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
					//SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2,0.06)
					SET_SYNCHRONIZED_SCENE_RATE(ssClimbToBack_2,1.4)
					bDoneDelayedClimbInBack = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbToBack_2)
		IF GET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2) = 1 //> 0.99	
			//ssPed2SitBack = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
			//SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssPed2SitBack,FALSE)
			//SET_SYNCHRONIZED_SCENE_LOOPED(ssPed2SitBack,TRUE)
			IF iSyncPed = 1
				/*
				IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[1],"missminuteman_2ig_1","trunk_josef")
					PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[1])
					ATTACH_ENTITY_TO_ENTITY(pedImmigrant[1],sRCLauncherDataLocal.vehID[0],-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[1])),<<0,0,90>>)
					TASK_PLAY_ANIM(pedImmigrant[1],"missminuteman_2ig_1","trunk_josef",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_NOT_INTERRUPTABLE)
					SET_RAGDOLL_BLOCKING_FLAGS(pedImmigrant[1],RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
				ENDIF
				*/
				PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[1])
				ssPed2SitBack = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrant[1])),<<0,0,90>>)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssPed2SitBack,FALSE)
				SET_SYNCHRONIZED_SCENE_LOOPED(ssPed2SitBack,TRUE)
				SET_SYNCHRONIZED_SCENE_RATE(ssPed2SitBack,1.6)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssPed2SitBack,sRCLauncherDataLocal.vehID[0],-1)
				SET_PED_KEEP_TASK(pedImmigrant[1],TRUE)
				TASK_SYNCHRONIZED_SCENE(pedImmigrant[1],ssPed2SitBack,"missminuteman_2ig_1","trunk_josef",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)

			ELIF iSyncPed = 2
				/*
				IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"missminuteman_2ig_1","trunk_josef")
					PROCESS_ENTITY_ATTACHMENTS(pedImmigrantChase2)
					ATTACH_ENTITY_TO_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],-1,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrantChase2)),<<0,0,90>>)
					TASK_PLAY_ANIM(pedImmigrantChase2,"missminuteman_2ig_1","trunk_josef",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_NOT_INTERRUPTABLE)
					SET_RAGDOLL_BLOCKING_FLAGS(pedImmigrantChase2,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
				ENDIF
				*/
				PROCESS_ENTITY_ATTACHMENTS(pedImmigrantChase2)
				ssPed2SitBack = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_ENTITY_COORDS(pedImmigrantChase2)),<<0,0,90>>)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssPed2SitBack,FALSE)
				SET_SYNCHRONIZED_SCENE_LOOPED(ssPed2SitBack,TRUE)
				SET_SYNCHRONIZED_SCENE_RATE(ssPed2SitBack,1.6)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssPed2SitBack,sRCLauncherDataLocal.vehID[0],-1)
				SET_PED_KEEP_TASK(pedImmigrantChase2,TRUE)
				TASK_SYNCHRONIZED_SCENE(pedImmigrantChase2,ssPed2SitBack,"missminuteman_2ig_1","trunk_josef",INSTANT_BLEND_IN,SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC DoTraffic()

	IF HAS_MODEL_LOADED(EMPEROR2)
	AND HAS_MODEL_LOADED(SURFER2)
	AND HAS_MODEL_LOADED(BISON)
	AND HAS_MODEL_LOADED(TORNADO3)
	AND HAS_MODEL_LOADED(STANIER)
	AND HAS_MODEL_LOADED(MESA)
	/*
	AND GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC01")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC02")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC03")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("Eps8TRAFFIC04")
	*/	
		vTrafficRoute1[0] = <<301.6302, 3426.7095, 35.9073>>
		vTrafficRoute1[1] = <<75.7878, 3591.8013, 38.7696>>
		vTrafficRoute1[2] = <<-96.4917, 3616.2417, 43.8209>>
		vTrafficRoute1[3] = <<-193.8901, 3739.2117, 42.0091>>
		vTrafficRoute1[4] = <<-223.8502, 3936.6489, 36.4372>>
		vTrafficRoute1[5] = <<-231.1717, 4119.1846, 38.6720>>
		vTrafficRoute1[6] = <<-74.4490, 4334.0449, 49.3153>>
		//vTrafficRoute1[7] = << -202.3507, 4205.0635, 43.5704 >>
		//vTrafficRoute1[8] = << -79.9476, 4317.0864, 47.5128 >>
		//vTrafficRoute1[9] = << -49.5753, 4405.9521, 55.8139 >>
		//vTrafficRoute1[10] = << 57.3223, 4449.8618, 62.7177 >>

		fHeadingTrafficRoute1[0] = 131.8059
		fHeadingTrafficRoute1[1] = 80.1408
		fHeadingTrafficRoute1[2] = 59.8754
		fHeadingTrafficRoute1[3] = 14.4899
		fHeadingTrafficRoute1[4] = 351.0852
		fHeadingTrafficRoute1[5] = 343.5272
		fHeadingTrafficRoute1[6] = 343.5132
		//fHeadingTrafficRoute1[7] = 330.3210
		//fHeadingTrafficRoute1[8] = 339.2812
		//fHeadingTrafficRoute1[9] = 320.5906
		//fHeadingTrafficRoute1[10] = 260.8352
		
		vTrafficRoute2[0] = <<110.6866, 4435.8774, 69.0993>>
		vTrafficRoute2[1] = <<-68.2519, 4363.0513, 52.4024>>
		vTrafficRoute2[2] = <<-208.7096, 4199.1304, 43.2855>>
		vTrafficRoute2[3] = <<-224.2061, 3956.5005, 36.5195>>
		vTrafficRoute2[4] = <<-196.2560, 3732.5298, 42.2655>>
		vTrafficRoute2[5] = <<30.9941, 3594.5623, 38.7656>>
		vTrafficRoute2[6] = <<181.0861, 3394.6218, 37.0298>>
		//vTrafficRoute2[7] = << -213.4657, 3811.5183, 37.8680 >>
		//vTrafficRoute2[8] = << -191.0385, 3700.5374, 42.9150 >>
		//vTrafficRoute2[9] = << 67.4248, 3585.1685, 38.7595 >>
		//vTrafficRoute2[10] = << 99.6722, 3432.2373, 38.5112 >>

		fHeadingTrafficRoute2[0] = 67.0420
		fHeadingTrafficRoute2[1] = 163.8586
		fHeadingTrafficRoute2[2] = 159.4639 
		fHeadingTrafficRoute2[3] = 172.2390
		fHeadingTrafficRoute2[4] = 189.3230
		fHeadingTrafficRoute2[5] = 260.3510
		fHeadingTrafficRoute2[6] = 250.8270
		//fHeadingTrafficRoute2[7] = 195.5279
		//fHeadingTrafficRoute2[8] = 194.5786
		//fHeadingTrafficRoute2[9] = 251.4245
		//fHeadingTrafficRoute2[10] = 234.4719
		
		MODEL_NAMES modVeh
		
		INT iRandom

		INT iClosestWaypointToPlayer
		INT iClosestWaypointToSpawnPoint

		INT iWaypointProgress

		IF GET_IS_WAYPOINT_RECORDING_LOADED("Min2_Traffic01")
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute1[iTraffic])	
				IF bBlokestunned = FALSE	
				AND missionStage = MS_UPDATE_CHASE1
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic01",GET_ENTITY_COORDS(PLAYER_PED_ID()),iClosestWaypointToPlayer)
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic01",vTrafficRoute1[iTraffic],iClosestWaypointToSpawnPoint)
					IF iClosestWaypointToSpawnPoint < iClosestWaypointToPlayer
					AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute1[iTraffic],80)	
					//AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute1[iTraffic],220)	
					AND NOT IS_POSITION_OCCUPIED(vTrafficRoute1[iTraffic],200,FALSE,TRUE,FALSE,FALSE,FALSE)	
						IF NOT IS_SPHERE_VISIBLE(vTrafficRoute1[iTraffic],8)	
							iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
							IF iRandom = 0
								modVeh = EMPEROR2
							ELIF iRandom = 1
								modVeh = SURFER2
							ELIF iRandom = 2
								modVeh = BISON
							ELIF iRandom = 3
								modVeh = TORNADO3
							ELIF iRandom = 4
								modVeh = STANIER
							ELSE
								modVeh = MESA
							ENDIF
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 1")
							#ENDIF
							vehTrafficRoute1[iTraffic] = CREATE_VEHICLE(modVeh,vTrafficRoute1[iTraffic], fHeadingTrafficRoute1[iTraffic])
							pedTrafficRoute1[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute1[iTraffic])
							SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute1[iTraffic])
							SET_ENTITY_LOAD_COLLISION_FLAG(vehTrafficRoute1[iTraffic],TRUE)
							//TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute1[iTraffic],vehTrafficRoute1[iTraffic],"Min2_Traffic01",DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							TASK_VEHICLE_DRIVE_TO_COORD(pedTrafficRoute1[iTraffic],vehTrafficRoute1[iTraffic],<<254.0842, 4495.0879, 65.5812>>,10,DRIVINGSTYLE_NORMAL, modVeh, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,20,20)
							SET_PED_COMBAT_ATTRIBUTES(pedTrafficRoute1[iTraffic],CA_ALWAYS_FLEE,TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute1[iTraffic])	
					IF IS_PED_UNINJURED(pedTrafficRoute1[iTraffic])	
						//IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute1[iTraffic])	
						IF GET_SCRIPT_TASK_STATUS(pedTrafficRoute1[iTraffic],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK	
							IF IS_PED_UNINJURED(pedImmigrant[i])
								//IF IS_PED_UNINJURED(pedTrafficRoute1[iTraffic])
									IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],pedTrafficRoute1[iTraffic],20.0 + (fTrafficStopped * 10.0))
										IF IS_PED_FACING_PED(pedTrafficRoute1[iTraffic],pedImmigrant[i],120)	
											IF bBlokestunned = TRUE
												TASK_VEHICLE_TEMP_ACTION(pedTrafficRoute1[iTraffic],vehTrafficRoute1[iTraffic],TEMPACT_BRAKE,-1)
												fTrafficStopped = fTrafficStopped + 1.0
											ENDIF										
											//IF IS_PED_BEING_STUNNED(pedImmigrant[i])
											//	TASK_SMART_FLEE_PED(pedTrafficRoute1[iTraffic],PLAYER_PED_ID(),200,-1)
											//ENDIF
										ENDIF
									ENDIF
								//ENDIF
							ENDIF
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic01",GET_ENTITY_COORDS(PLAYER_PED_ID()),iClosestWaypointToPlayer)
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic01",GET_ENTITY_COORDS(vehTrafficRoute1[iTraffic]),iWaypointProgress)
							IF iWaypointProgress > iClosestWaypointToPlayer
							//IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute1[iTraffic]) > iClosestWaypointToPlayer
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute1[iTraffic],80)	
								//OR NOT IS_ENTITY_ON_SCREEN(vehTrafficRoute1[iTraffic])	
									//IF NOT IS_SPHERE_VISIBLE(vTrafficRoute1[iTraffic],9)		
									IF IS_ENTITY_OCCLUDED(vehTrafficRoute1[iTraffic])
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 1")
										#ENDIF
										SAFE_RELEASE_PED(pedTrafficRoute1[iTraffic])
										SAFE_RELEASE_VEHICLE(vehTrafficRoute1[iTraffic])
									ENDIF
								ENDIF
							ENDIF
						ELSE	
							//IF NOT IS_SPHERE_VISIBLE(vTrafficRoute1[iTraffic],9)		
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute1[iTraffic],80)	
							AND IS_ENTITY_OCCLUDED(vehTrafficRoute1[iTraffic])
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 1")
								#ENDIF
								SAFE_RELEASE_PED(pedTrafficRoute1[iTraffic])
								SAFE_RELEASE_VEHICLE(vehTrafficRoute1[iTraffic])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Min2_Traffic02")
			IF NOT DOES_ENTITY_EXIST(vehTrafficRoute2[iTraffic])	
				IF bBlokestunned = FALSE		
				AND missionStage = MS_UPDATE_CHASE1	
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic02",GET_ENTITY_COORDS(PLAYER_PED_ID()),iClosestWaypointToPlayer)
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic02",vTrafficRoute1[iTraffic],iClosestWaypointToSpawnPoint)
					IF iClosestWaypointToSpawnPoint > iClosestWaypointToPlayer
					AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute2[iTraffic],80)	
					//AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vTrafficRoute2[iTraffic],220)		
					AND NOT IS_POSITION_OCCUPIED(vTrafficRoute2[iTraffic],200,FALSE,TRUE,FALSE,FALSE,FALSE)		
						IF NOT IS_SPHERE_VISIBLE(vTrafficRoute2[iTraffic],8)	
							iRandom = GET_RANDOM_INT_IN_RANGE(0,6)
							IF iRandom = 0
								modVeh = EMPEROR2
							ELIF iRandom = 1
								modVeh = SURFER2
							ELIF iRandom = 2
								modVeh = BISON
							ELIF iRandom = 3
								modVeh = TORNADO3
							ELIF iRandom = 4
								modVeh = STANIER
							ELSE
								modVeh = MESA
							ENDIF
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "SPAWNING TRAFFIC ON 2")
							#ENDIF
							vehTrafficRoute2[iTraffic] = CREATE_VEHICLE(modVeh,vTrafficRoute2[iTraffic], fHeadingTrafficRoute2[iTraffic])
							pedTrafficRoute2[iTraffic] = CREATE_RANDOM_PED_AS_DRIVER(vehTrafficRoute2[iTraffic])
							SET_VEHICLE_ON_GROUND_PROPERLY(vehTrafficRoute2[iTraffic])
							SET_ENTITY_LOAD_COLLISION_FLAG(vehTrafficRoute2[iTraffic],TRUE)
							//TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedTrafficRoute2[iTraffic],vehTrafficRoute2[iTraffic],"Min2_Traffic02",DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							TASK_VEHICLE_DRIVE_TO_COORD(pedTrafficRoute2[iTraffic],vehTrafficRoute2[iTraffic],<<213.2652, 3214.4578, 41.7024>>,10,DRIVINGSTYLE_NORMAL, modVeh, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,20,20)
							SET_PED_COMBAT_ATTRIBUTES(pedTrafficRoute2[iTraffic],CA_ALWAYS_FLEE,TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(vehTrafficRoute2[iTraffic])	
					IF IS_PED_UNINJURED(pedTrafficRoute2[iTraffic])	
						//IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrafficRoute2[iTraffic])	
						IF GET_SCRIPT_TASK_STATUS(pedTrafficRoute2[iTraffic],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK		
							IF IS_PED_UNINJURED(pedImmigrant[i])
								//IF IS_PED_UNINJURED(pedTrafficRoute2[iTraffic])
									IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],pedTrafficRoute2[iTraffic],20.0 + (fTrafficStopped * 10.0))
										IF IS_PED_FACING_PED(pedTrafficRoute2[iTraffic],pedImmigrant[i],120)		
											IF bBlokestunned = TRUE
												TASK_VEHICLE_TEMP_ACTION(pedTrafficRoute2[iTraffic],vehTrafficRoute2[iTraffic],TEMPACT_BRAKE,-1)
												fTrafficStopped = fTrafficStopped + 1.0
											ENDIF
											//IF IS_PED_BEING_STUNNED(pedImmigrant[i])
											//	TASK_SMART_FLEE_PED(pedTrafficRoute2[iTraffic],PLAYER_PED_ID(),200,-1)
											//ENDIF
										ENDIF
									ENDIF
								//ENDIF
							ENDIF
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic02",GET_ENTITY_COORDS(PLAYER_PED_ID()),iClosestWaypointToPlayer)
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("Min2_Traffic02",GET_ENTITY_COORDS(vehTrafficRoute2[iTraffic]),iWaypointProgress)
							IF iWaypointProgress < iClosestWaypointToPlayer
							//IF GET_VEHICLE_WAYPOINT_PROGRESS(vehTrafficRoute2[iTraffic]) < iClosestWaypointToPlayer
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute2[iTraffic],80)	
								//OR NOT IS_ENTITY_ON_SCREEN(vehTrafficRoute2[iTraffic])	
									//IF NOT IS_SPHERE_VISIBLE(vTrafficRoute2[iTraffic],8)		
									IF IS_ENTITY_OCCLUDED(vehTrafficRoute2[iTraffic])
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 2")
										#ENDIF
										SAFE_RELEASE_PED(pedTrafficRoute2[iTraffic])
										SAFE_RELEASE_VEHICLE(vehTrafficRoute2[iTraffic])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//IF NOT IS_SPHERE_VISIBLE(vTrafficRoute2[iTraffic],9)		
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),vehTrafficRoute2[iTraffic],80)	
							AND IS_ENTITY_OCCLUDED(vehTrafficRoute2[iTraffic])
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MISSION, "CLEANING UP TRAFFIC ON 2")
								#ENDIF
								SAFE_RELEASE_PED(pedTrafficRoute2[iTraffic])
								SAFE_RELEASE_VEHICLE(vehTrafficRoute2[iTraffic])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		++iTraffic
		IF iTraffic > 6
			iTraffic = 0
		ENDIF
	
	ENDIF
	
ENDPROC

PROC CleanupTraffic()

	SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR2)
	SET_MODEL_AS_NO_LONGER_NEEDED(SURFER2)
	SET_MODEL_AS_NO_LONGER_NEEDED(BISON)
	SET_MODEL_AS_NO_LONGER_NEEDED(TORNADO3)
	SET_MODEL_AS_NO_LONGER_NEEDED(STANIER)
	SET_MODEL_AS_NO_LONGER_NEEDED(MESA)

	SAFE_RELEASE_PED(pedTrafficRoute1[0])
	SAFE_RELEASE_PED(pedTrafficRoute1[1])
	SAFE_RELEASE_PED(pedTrafficRoute1[2])
	SAFE_RELEASE_PED(pedTrafficRoute1[3])
	SAFE_RELEASE_PED(pedTrafficRoute1[4])
	SAFE_RELEASE_PED(pedTrafficRoute1[5])
	SAFE_RELEASE_PED(pedTrafficRoute1[6])
	SAFE_RELEASE_PED(pedTrafficRoute1[7])
	SAFE_RELEASE_PED(pedTrafficRoute1[8])
	SAFE_RELEASE_PED(pedTrafficRoute1[9])
	SAFE_RELEASE_PED(pedTrafficRoute1[10])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[7])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[8])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[9])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute1[10])
	SAFE_RELEASE_PED(pedTrafficRoute2[0])
	SAFE_RELEASE_PED(pedTrafficRoute2[1])
	SAFE_RELEASE_PED(pedTrafficRoute2[2])
	SAFE_RELEASE_PED(pedTrafficRoute2[3])
	SAFE_RELEASE_PED(pedTrafficRoute2[4])
	SAFE_RELEASE_PED(pedTrafficRoute2[5])
	SAFE_RELEASE_PED(pedTrafficRoute2[6])
	SAFE_RELEASE_PED(pedTrafficRoute2[7])
	SAFE_RELEASE_PED(pedTrafficRoute2[8])
	SAFE_RELEASE_PED(pedTrafficRoute2[9])
	SAFE_RELEASE_PED(pedTrafficRoute2[10])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[0])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[1])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[2])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[3])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[4])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[5])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[6])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[7])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[8])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[9])
	SAFE_RELEASE_VEHICLE(vehTrafficRoute2[10])
	
ENDPROC

FUNC BOOL JOSEF_AND_JOE_LEFT_BEHIND()

	IF missionStage = MS_UPDATE_CHASE1
	OR missionStage = MS_IMMIGRANT1_APPREHENDED
		//IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE],150)
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOSEF],150)
			IF bPrint49 = FALSE
				PRINT_NOW("MIN2_49",DEFAULT_GOD_TEXT_TIME,0)    //return to
				bPrint49 = TRUE
			ENDIF
			/*
			IF NOT DOES_BLIP_EXIST(blipJoe)
				blipJoe = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE,TRUE)
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipJosef)
				blipJosef = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOE],TRUE,TRUE)
			ENDIF
			*/
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Grab random cars and toot their horns
PROC RandomCarHorns()

	IF bCarHorn = FALSE	
		vehRandom = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()),40.0,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		IF IS_VEHICLE_OK(vehRandom)
			START_VEHICLE_HORN(vehRandom,GET_RANDOM_INT_IN_RANGE(2000,6000),GET_RANDOM_INT_IN_RANGE(0,2))
			iTimerCarHorn = GET_GAME_TIMER()	
			bCarHorn = TRUE
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > iTimerCarHorn + 15000
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRandom)
			bCarHorn = FALSE	
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Init the park ranger vehicle
PROC SpawnPlayerCar()
	
	IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
		sRCLauncherDataLocal.vehID[0] = CREATE_VEHICLE(PRANGER,<<20.80, 4532.65, 104.66>>, 284.70, TRUE, TRUE)
	ENDIF
		
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(PLAYER_PED_ID())		
			SET_VEHICLE_AS_RESTRICTED(sRCLauncherDataLocal.vehID[0],1)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(PRANGER,TRUE)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherDataLocal.vehID[0], SC_DOOR_BOOT, FALSE)
			ROLL_DOWN_WINDOWS(sRCLauncherDataLocal.vehID[0])
			//REMOVE_VEHICLE_WINDOW(sRCLauncherDataLocal.vehID[0],SC_WINDOW_MIDDLE_LEFT)
			//REMOVE_VEHICLE_WINDOW(sRCLauncherDataLocal.vehID[0],SC_WINDOW_REAR_LEFT)
			SET_VEHICLE_DIRT_LEVEL(sRCLauncherDataLocal.vehID[0],14.0)
			SET_VEHICLE_HAS_STRONG_AXLES(sRCLauncherDataLocal.vehID[0],TRUE)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
			ENDIF
			SET_VEHICLE_DEFORMATION_FIXED(sRCLauncherDataLocal.vehID[0])
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(PRANGER)

ENDPROC	

/// PURPOSE: Spawn the crop duster and pilot
PROC SpawnDuster()

	IF NOT DOES_ENTITY_EXIST(vehDuster)
		vehDuster = CREATE_VEHICLE(DUSTER,<< -433.5560, 2609.4536, 77.2352 >>, 22.3231)
		IF NOT DOES_ENTITY_EXIST(pedDusterPilot)
			pedDusterPilot = CREATE_PED_INSIDE_VEHICLE(vehDuster,PEDTYPE_MISSION,S_M_M_Migrant_01)
			START_PLAYBACK_RECORDED_VEHICLE(vehDuster, 343, "Min2_Dust")
			SET_PLAYBACK_SPEED(vehDuster,1.1)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Spawn/setup Josef and Joe
PROC SpawnJosefAndJoe()
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
		
		ADD_VEHICLE_STUCK_CHECK_WITH_WARP(sRCLauncherDataLocal.vehID[0],0.5,1000,FALSE,FALSE,FALSE,-1)
		
		IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[ic_JOE])
			CREATE_NPC_PED_INSIDE_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],CHAR_JOE,sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
		ELSE
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[ic_JOE],GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_DO_DRIVEBYS,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_AGGRESSIVE,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_DISABLE_SECONDARY_TARGET,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_USE_COVER,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_REQUIRES_LOS_TO_SHOOT,TRUE)
			SET_PED_COMBAT_RANGE(sRCLauncherDataLocal.pedID[ic_JOE],CR_NEAR)
			SET_PED_COMBAT_MOVEMENT(sRCLauncherDataLocal.pedID[ic_JOE],CM_STATIONARY)
			GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[ic_JOE],WEAPONTYPE_STUNGUN,-1,TRUE)
			SET_PED_ACCURACY(sRCLauncherDataLocal.pedID[ic_JOE],100)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOE], PCF_WillFlyThroughWindscreen, FALSE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOE], PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOE], PCF_GetOutBurningVehicle, FALSE)
			//SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOE], PCF_WaitForDirectEntryPointToBeFreeWhenExiting, TRUE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOE], PCF_ForceDirectEntry, TRUE)
			TASK_SET_DECISION_MAKER(sRCLauncherDataLocal.pedID[ic_JOE],DECISION_MAKER_EMPTY)
			SET_PED_CAN_BE_TARGETTED(sRCLauncherDataLocal.pedID[ic_JOE],FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[ic_JOE],TRUE)
			SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(sRCLauncherDataLocal.pedID[ic_JOE],TRUE)
			SET_PED_LEG_IK_MODE(sRCLauncherDataLocal.pedID[ic_JOE], LEG_IK_FULL)
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[ic_JOSEF])
			CREATE_NPC_PED_INSIDE_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],CHAR_JOSEF,sRCLauncherDataLocal.vehID[0],VS_BACK_RIGHT)
		ELSE
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])		
			SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
			GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],WEAPONTYPE_STUNGUN,-1,TRUE)
			SET_PED_ACCURACY(sRCLauncherDataLocal.pedID[ic_JOSEF],100)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_DO_DRIVEBYS,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_AGGRESSIVE,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_DISABLE_SECONDARY_TARGET,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_USE_COVER,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_REQUIRES_LOS_TO_SHOOT,TRUE)
			SET_PED_COMBAT_RANGE(sRCLauncherDataLocal.pedID[ic_JOSEF],CR_NEAR)
			SET_PED_COMBAT_MOVEMENT(sRCLauncherDataLocal.pedID[ic_JOSEF],CM_STATIONARY)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOSEF], PCF_WillFlyThroughWindscreen, FALSE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOSEF], PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOSEF], PCF_GetOutBurningVehicle, FALSE)
			//SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOSEF], PCF_WaitForDirectEntryPointToBeFreeWhenExiting, TRUE)
			SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[ic_JOSEF], PCF_ForceDirectEntry, TRUE)
			TASK_SET_DECISION_MAKER(sRCLauncherDataLocal.pedID[ic_JOSEF],DECISION_MAKER_EMPTY)
			SET_PED_CAN_BE_TARGETTED(sRCLauncherDataLocal.pedID[ic_JOSEF],FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE)
			SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE)
			TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			SET_PED_LEG_IK_MODE(sRCLauncherDataLocal.pedID[ic_JOSEF], LEG_IK_FULL)
		ENDIF
	
	ENDIF	
		
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 2, PLAYER_PED_ID(), "TREVOR")
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, sRCLauncherDataLocal.pedID[ic_JOSEF], "JOSEF")
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, sRCLauncherDataLocal.pedID[ic_JOE], "JOE")
		
ENDPROC	

/// PURPOSE: Spawn chase vehicles
PROC SpawnChaseCar()

	IF i = 0
	
		IF NOT DOES_ENTITY_EXIST(vehChaseCar[i])	
			//vehChaseCar[i] = CREATE_VEHICLE(BLAZER,<< 191.5226, 4433.6460, 72.2440 >>, 132.2115, TRUE, TRUE)
			vehChaseCar[i] = CREATE_VEHICLE(BLAZER,<<246.4836, 4493.2305, 66.0521>>, 90.3329, TRUE, TRUE)
			//vehChaseCar[i] = CREATE_VEHICLE(BLAZER,<<162.2808, 4411.3193, 74.7961>>, 68.1696 , TRUE, TRUE)    //On the bank at the bottom (start off vehicle)
			SET_VEHICLE_COLOUR_COMBINATION(vehChaseCar[i],2)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehChaseCar[i],TRUE)
			SET_VEHICLE_DIRT_LEVEL(vehChaseCar[i],14.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER)
		ENDIF
		
	ENDIF
	
	IF i = 1
	
		IF IS_VEHICLE_OK(vehChaseCar[0])
			SAFE_RELEASE_VEHICLE(vehChaseCar[0])	
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehChaseCar[i])
			IF IS_REPLAY_IN_PROGRESS()
			AND GET_REPLAY_MID_MISSION_STAGE() = 2
				vehChaseCar[i] = CREATE_VEHICLE(SANCHEZ, <<303.0419, 2892.9541, 42.5927>>, 209.59, TRUE, TRUE) //<< 307.6654, 2884.1946, 42.4575 >>, 214.6454
			ELSE	
				vehChaseCar[i] = CREATE_VEHICLE(SANCHEZ, <<303.0419, 2892.9541, 42.5927>>, 209.59, TRUE, TRUE)  //<< 301.5931, 2896.0457, 42.6070 >>, 196.3851
			ENDIF
			SET_VEHICLE_COLOUR_COMBINATION(vehChaseCar[i],2)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehChaseCar[i],TRUE)
			SET_VEHICLE_ENGINE_ON(vehChaseCar[i],TRUE,TRUE)
			SET_VEHICLE_DIRT_LEVEL(vehChaseCar[i],14.0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehChaseCarChase2)
			IF IS_REPLAY_IN_PROGRESS()
			AND GET_REPLAY_MID_MISSION_STAGE() = 2
				vehChaseCarChase2 = CREATE_VEHICLE(SANCHEZ,<<297.1000, 2895.4980, 42.5893>>, 214.25, TRUE, TRUE) //<< 300.2892, 2885.5376, 42.5449 >>, 220.7304
			ELSE		
				vehChaseCarChase2 = CREATE_VEHICLE(SANCHEZ,<<297.1000, 2895.4980, 42.5893>>, 214.25, TRUE, TRUE)  //<< 297.5408, 2888.8457, 42.6080 >>, 217.2791
			ENDIF
			SET_VEHICLE_COLOUR_COMBINATION(vehChaseCar[i],1)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehChaseCarChase2,TRUE)
			SET_VEHICLE_ENGINE_ON(vehChaseCarChase2,TRUE,TRUE)
			SET_VEHICLE_DIRT_LEVEL(vehChaseCarChase2,14.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehSpeedo)
			vehSpeedo = CREATE_VEHICLE(SPEEDO,<< 290.8079, 2859.2888, 42.6421 >>, 313.9074)
			SET_VEHICLE_DOOR_OPEN(vehSpeedo,SC_DOOR_REAR_LEFT)
			SET_VEHICLE_DOOR_OPEN(vehSpeedo,SC_DOOR_REAR_RIGHT)
			SET_VEHICLE_COLOUR_COMBINATION(vehSpeedo,1)
			SET_VEHICLE_DIRT_LEVEL(vehSpeedo,15.0)
			SET_MODEL_AS_NO_LONGER_NEEDED(SPEEDO)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedWorker[0])
			pedWorker[0] = CREATE_PED(PEDTYPE_MISSION,S_M_M_GAFFER_01,<< 289.6810, 2860.0039, 42.6421 >>, 66.4056)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_HEAD,0,1)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_HAIR,0,0)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_SPECIAL,0,0)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_HEAD,0,1)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_TORSO,0,1)
			SET_PED_COMPONENT_VARIATION(pedWorker[0],PED_COMP_LEG,1,0)
			TASK_PLAY_ANIM(pedWorker[0],"rcmminute2lean","idle_c",NORMAL_BLEND_IN,-2,-1,AF_LOOPING)
			SET_PED_COMBAT_ATTRIBUTES(pedWorker[0],CA_ALWAYS_FLEE,TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GAFFER_01)
		ENDIF
		/*
		IF NOT DOES_ENTITY_EXIST(pedWorker[1])
			pedWorker[1] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_CONSTRUCT_01,<<287.395721,2837.913574,43.573788>>, 124.411591)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_HEAD,2,2)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_HAIR,0,1)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_SPECIAL,0,0)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_TORSO,2,2)
			SET_PED_COMPONENT_VARIATION(pedWorker[1],PED_COMP_LEG,2,2)
			TASK_PLAY_ANIM(pedWorker[1],"rcmminute2","idle_e",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
			ObjPhone = CREATE_OBJECT_NO_OFFSET(P_AMB_PHONE_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedWorker[1],<<0.0,0.0,10.0>>))
			ATTACH_ENTITY_TO_ENTITY(ObjPhone, pedWorker[1], GET_PED_BONE_INDEX(pedWorker[1], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedWorker[1],CA_ALWAYS_FLEE,TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_AMB_PHONE_01)
		ENDIF
		*/
	ENDIF
			
ENDPROC

/// PURPOSE: Is the player abandoning the mission by driving off
PROC IsPlayerAbandoning() 

	IF i = 0	
		IF IS_PED_UNINJURED(pedImmigrant[i])
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedImmigrant[i]) > 240.0
				IF bPrint18 = FALSE
					IF GET_GAME_TIMER() > iTimerChase1Started + 12000		
						//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
						//iGameTimer = GET_GAME_TIMER()
						bPrint18 = TRUE
					ENDIF
				ELSE
					//iGameTimerUpdate = GET_GAME_TIMER()
					//IF iGameTimerUpdate > (iGameTimer + 10000)
						IF fDistBetweenJosefAndImmigrant > 300.0//260.0
							sFailReason = "MIN2_43"
							missionStage = MS_MISSION_FAILING
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
 
ENDPROC

/// PURPOSE: Manage the entities at the cement factory
PROC UpdateAmbientCementFactory()

	IF IS_PED_UNINJURED(pedWorker[0])
		IF GET_DISTANCE_BETWEEN_PEDS(pedWorker[0],PLAYER_PED_ID()) < 15.0
			TASK_LOOK_AT_ENTITY(pedWorker[0],PLAYER_PED_ID(),-1)
		ENDIF
	ENDIF
	
	IF iSeqCementFactory = 0
		IF IS_PED_UNINJURED(pedWorker[0])
			OPEN_SEQUENCE_TASK(seqPointAtImmigrants)
				TASK_PLAY_ANIM(NULL,"rcmminute2lean","exit",1,NORMAL_BLEND_OUT,-1)
				TASK_PLAY_ANIM(NULL,"rcmminute2","intro_90_r",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
				TASK_PLAY_ANIM(NULL,"rcmminute2","loop_90_r",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
				TASK_PLAY_ANIM(NULL,"rcmminute2","outro_90_r",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
			CLOSE_SEQUENCE_TASK(seqPointAtImmigrants)
			CLEAR_PED_TASKS(pedWorker[0])
			TASK_PERFORM_SEQUENCE(pedWorker[0],seqPointAtImmigrants)
		ENDIF
		IF IS_SCREEN_FADED_IN()			
			IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CEM",CONV_PRIORITY_MEDIUM)    //Border Patrol! Come out to play!
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
				iSeqCementFactory = 1
			ENDIF
		ELSE
			iSeqCementFactory = 1
		ENDIF
	ENDIF
		
	IF GET_GAME_TIMER() > iArrivedAtCementFactory + 100	
		IF iSeqCementFactory = 1	
			IF IS_PED_UNINJURED(pedImmigrant[i])
				TASK_LOOK_AT_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),-1)
			ENDIF
			iSeqCementFactory = 2
		ENDIF	
	ENDIF
	
	IF GET_GAME_TIMER() > iArrivedAtCementFactory + 200	
		IF iSeqCementFactory = 2	
			IF IS_PED_UNINJURED(pedImmigrant[i])
				IF IS_VEHICLE_OK(vehChaseCar[i])	
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])
						START_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i],101,"Min2DB1")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i],1000.0)
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 6.0
							fPlaybackSpeed = 0.82
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 6.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 8.0
							fPlaybackSpeed = 0.91
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 8.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 10.0
							fPlaybackSpeed = 1
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 10.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 12.0
							fPlaybackSpeed = 1.2
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 12.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 14.0
							fPlaybackSpeed = 1.3
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 14.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 16.0
							fPlaybackSpeed = 1.35
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 16.0
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 18.0
							fPlaybackSpeed = 1.4
						ENDIF
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) >= 18.0
							fPlaybackSpeed = 1.5
						ENDIF
						SET_PLAYBACK_SPEED(vehChaseCar[i],fPlaybackSpeed)
					ENDIF
				ENDIF
				IF IS_VEHICLE_OK(vehChaseCarChase2)	
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)	
						START_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2,102,"Min2DB2")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2,1100.0)  //1400
						SET_PLAYBACK_SPEED(vehChaseCarChase2,fPlaybackSpeed)
					ENDIF
				ENDIF
			ENDIF
			iSeqCementFactory = 5
		ENDIF	
	ENDIF
	
	//IF IS_VEHICLE_OK(vehChaseCarChase2)	
	//AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)
	//AND GET_TIME_POSITION_IN_RECORDING(vehChaseCarChase2) > 800
	IF GET_GAME_TIMER() > iArrivedAtCementFactory + 3400	
	
		IF iSeqCementFactory = 5	
			IF IS_PED_UNINJURED(pedImmigrantChase2)
				IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_PERFORM_SEQUENCE)	<> PERFORMING_TASK
					TASK_PERFORM_SEQUENCE(pedImmigrantChase2,seqChase2RouteSECONDPED)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(pedImmigrant[i])
				IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE)	<> PERFORMING_TASK
					TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqChase2Route)
				ENDIF
			ENDIF
			iSeqCementFactory = 6
		ENDIF	
	ENDIF
	
	IF IS_PED_UNINJURED(pedImmigrant[i])				
		IF IS_PED_UNINJURED(pedImmigrantChase2)		
			IF iSeqCementFactory = 10
				iArrivedAtCementFactory = iArrivedAtCementFactory - 10000
				iSeqCementFactory = 11
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Spawn the immigrants
PROC SpawnImmigrant() 

	IF IS_VEHICLE_OK(vehChaseCar[i])
		IF NOT DOES_ENTITY_EXIST(pedImmigrant[i])
			IF i = 0
				CREATE_NPC_PED_INSIDE_VEHICLE(pedImmigrant[i],CHAR_MANUEL,vehChaseCar[i])
				//CREATE_NPC_PED_ON_FOOT(pedImmigrant[i],CHAR_MANUEL,<<162.2808, 4411.3193, 74.7961>>, 68.1696)  //old version where he out of the car at the bank at the bottom of the road
				SET_DRIVER_ABILITY(pedImmigrant[i],1)
				SET_DRIVER_RACING_MODIFIER(pedImmigrant[i],1)
				TASK_LOOK_AT_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),-1)
				//SET_PED_DIES_WHEN_INJURED(pedImmigrant[i],FALSE)
			ENDIF
			IF i = 1
				pedImmigrant[i] = CREATE_PED_INSIDE_VEHICLE(vehChaseCar[i],PEDTYPE_MISSION,S_M_M_Migrant_01)
				SET_PED_COMPONENT_VARIATION(pedImmigrant[i],PED_COMP_HEAD,0,1)
				SET_PED_COMPONENT_VARIATION(pedImmigrant[i],PED_COMP_TORSO,1,0)
				SET_PED_COMPONENT_VARIATION(pedImmigrant[i],PED_COMP_LEG,0,0)
				SET_PED_COMPONENT_VARIATION(pedImmigrant[i],PED_COMP_FEET,1,0)
				SET_PED_COMPONENT_VARIATION(pedImmigrant[i],PED_COMP_SPECIAL,1,0)
				//SET_PED_DIES_WHEN_INJURED(pedImmigrant[i],FALSE)
				IF IS_VEHICLE_OK(vehChaseCarChase2)
					pedImmigrantChase2 = CREATE_PED_INSIDE_VEHICLE(vehChaseCarChase2,PEDTYPE_MISSION,S_M_M_Migrant_01)
					SET_PED_COMPONENT_VARIATION(pedImmigrantChase2,PED_COMP_HEAD,1,0)
					SET_PED_COMPONENT_VARIATION(pedImmigrantChase2,PED_COMP_TORSO,1,1)
					SET_PED_COMPONENT_VARIATION(pedImmigrantChase2,PED_COMP_LEG,0,0)
					SET_PED_COMPONENT_VARIATION(pedImmigrantChase2,PED_COMP_FEET,1,0)
					SET_PED_COMPONENT_VARIATION(pedImmigrantChase2,PED_COMP_SPECIAL,1,0)
					SET_ENTITY_IS_TARGET_PRIORITY(pedImmigrantChase2,TRUE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_DisableGoToWritheWhenInjured, TRUE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_WillFlyThroughWindscreen, FALSE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_GetOutUndriveableVehicle, FALSE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_GetOutBurningVehicle, FALSE)
					//SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_WaitForDirectEntryPointToBeFreeWhenExiting, TRUE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_ForceDirectEntry, TRUE)
					//SET_PED_LEG_IK_MODE(pedImmigrantChase2, LEG_IK_FULL)
					SET_PED_MAX_TIME_IN_WATER(pedImmigrantChase2,2)
					IF IS_PED_ON_ANY_BIKE(pedImmigrantChase2)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrantChase2,KNOCKOFFVEHICLE_DEFAULT)
					ENDIF
					SET_PED_DIES_WHEN_INJURED(pedImmigrantChase2,FALSE)
					TASK_SET_DECISION_MAKER(pedImmigrantChase2,DECISION_MAKER_EMPTY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrantChase2,TRUE)
					SET_ENTITY_HEALTH(pedImmigrantChase2,300)
					SET_PED_KEEP_TASK(pedImmigrantChase2,TRUE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_RunFromFiresAndExplosions, FALSE)
					SET_PED_CONFIG_FLAG(pedImmigrantChase2, PCF_DisableExplosionReactions, TRUE)
					IF IS_PED_ON_ANY_BIKE(pedImmigrant[i])
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrant[i],KNOCKOFFVEHICLE_DEFAULT)
					ENDIF
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Migrant_01)
			ENDIF
			SET_ENTITY_IS_TARGET_PRIORITY(pedImmigrant[i],TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedImmigrant[i],CA_CAN_TAUNT_IN_VEHICLE,TRUE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_DisableGoToWritheWhenInjured, TRUE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_WillFlyThroughWindscreen, FALSE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_GetOutBurningVehicle, FALSE)
			//SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_WaitForDirectEntryPointToBeFreeWhenExiting, TRUE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_ForceDirectEntry, TRUE)
			//SET_PED_LEG_IK_MODE(pedImmigrant[i], LEG_IK_FULL)
			SET_PED_MAX_TIME_IN_WATER(pedImmigrant[i],2)
			TASK_SET_DECISION_MAKER(pedImmigrant[i],DECISION_MAKER_EMPTY)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrant[i],TRUE)
			SET_ENTITY_HEALTH(pedImmigrant[i],300)
			SET_PED_KEEP_TASK(pedImmigrant[i],TRUE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(pedImmigrant[i], PCF_DisableExplosionReactions, TRUE)
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(pedImmigrant[i])
		IF i = 0
			IF GET_DISTANCE_BETWEEN_PEDS(pedImmigrant[i],PLAYER_PED_ID()) < 500 //125.0
				bOkToStartChase = TRUE
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<313.491394,2974.186523,17.270653>>, <<354.219116,2772.717285,84.748848>>, 122.250000)   //Cheat	
			OR IS_PLAYER_SHOOTING_NEAR_PED(pedImmigrant[i])	
			OR IS_PLAYER_SHOOTING_NEAR_PED(pedImmigrantChase2)	
				bOkToStartChase = TRUE
				iSeqCementFactory = 10
			ENDIF
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<268.072601,2861.259766,37.143517>>, <<274.734955,2847.247070,66.008347>>, 15.750000)   //Entrance
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<281.031952,2884.983643,37.307434>>, <<304.812927,2837.810059,73.936058>>, 57.500000)   //Whole place
				bOkToStartChase = TRUE
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE: Setup the chase route sequences for the immigrants
PROC SetupSequences()

	IF bSeqSetup = FALSE
		OPEN_SEQUENCE_TASK(seqChase2Route)
			IF IS_VEHICLE_OK(vehChaseCar[i])
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,vehChaseCar[i],"Min2_Bike02",DRIVINGMODE_AVOIDCARS_RECKLESS,28)	//10
				TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],vChase[44],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],vChase[45],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],vChase[46],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
				TASK_VEHICLE_DRIVE_WANDER(NULL,vehChaseCar[i],24.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
			ENDIF
		CLOSE_SEQUENCE_TASK(seqChase2Route)
		OPEN_SEQUENCE_TASK(seqChase2RouteSECONDPED)
			TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
			IF IS_VEHICLE_OK(vehChaseCarChase2)	
				IF IS_VEHICLE_OK(vehChaseCar[i])	
					TASK_VEHICLE_ESCORT(NULL,vehChaseCarChase2,vehChaseCar[i],VEHICLE_ESCORT_REAR,40.0,DRIVINGMODE_PLOUGHTHROUGH,fEscortDistance)
					/*
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,vehChaseCarChase2,"Min2_Bike02",DRIVINGMODE_AVOIDCARS_RECKLESS,24)	//12
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCarChase2,vChase[44],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCarChase2,vChase[45],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCarChase2,vChase[46],24.0,DRIVINGSTYLE_NORMAL,SANCHEZ,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,5.0)
					TASK_VEHICLE_DRIVE_WANDER(NULL,vehChaseCarChase2,24.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
					*/
				ENDIF
			ENDIF
		CLOSE_SEQUENCE_TASK(seqChase2RouteSECONDPED)
		bSeqSetup = TRUE
	ENDIF

ENDPROC

/// PURPOSE: Start chases
PROC StartVehiclePlayback()
	
	IF i = 0
		
		IF IS_VEHICLE_OK(vehChaseCar[i])
			IF IS_PED_UNINJURED(pedImmigrant[i])	
				OPEN_SEQUENCE_TASK(seqChase1)
					TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<161.1383, 4417.4663, 74.7775>>,10.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,30.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<17.4915, 4453.2012, 58.9788>>,20.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<-91.1074, 4301.0918, 45.4100>>,25.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<-223.0806, 4012.2888, 36.0401>>,25.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<-194.4012, 3733.1128, 42.2381>>,25.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<<89.5701, 3582.7971, 38.7910>>,25.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,2.0)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehChaseCar[i],<< 326.0252, 3438.8760, 35.3116 >>,25.0,DRIVINGSTYLE_NORMAL,BLAZER,DRIVINGMODE_AVOIDCARS_RECKLESS,25.0,2.0)
					TASK_VEHICLE_DRIVE_WANDER(NULL,vehChaseCar[i],22.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
				CLOSE_SEQUENCE_TASK(seqChase1)
				TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqChase1)
			ENDIF
		ENDIF
	
	ENDIF
	
	IF i = 1
			
		IF IS_PED_UNINJURED(pedImmigrant[i])
			IF bFirstImmigrantFleeing = FALSE		
				IF IS_VEHICLE_OK(vehChaseCar[i])	
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])
						START_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i],101,"Min2DB1")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehChaseCar[i],"MINUTE_02_SCENE_SCRAMBLER_BIKE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(pedImmigrantChase2)
			IF bImmigrantChase2Fleeing = FALSE		
				IF IS_VEHICLE_OK(vehChaseCarChase2)	
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)	
						START_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2,102,"Min2DB2")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehChaseCarChase2,"MINUTE_02_SCENE_SCRAMBLER_BIKE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	iChaseStartedTimer = GET_GAME_TIMER()
	
	bChaseStarted = TRUE

ENDPROC

/// PURPOSE: To fix B*1778492 - clamps the top speed of the lead bike in chase 2. Can no longer call TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING every frame. 
PROC SET_LEAD_BIKE_TOP_SPEED(FLOAT fTopSpeed)

	IF fTopSpeed = -1
		VEHICLE_WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(vehChaseCar[i])
	ELSE
		IF GET_ENTITY_SPEED(vehChaseCar[i]) > fTopSpeed
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(vehChaseCar[i],fTopSpeed)
		ELSE
			VEHICLE_WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(vehChaseCar[i])
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Manage rubberbanding etc for chases
PROC UpdateVehiclePlayback() 

	FLOAT fTopSpeed

	IF i = 0
		IF bBlokestunned = FALSE
		//IF bImmigrantHandsUp = FALSE	
			//IF IS_VEHICLE_OK(vehChaseCar[i])
				IF IS_PED_UNINJURED(pedImmigrant[i])
					//IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
						//CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehChaseCar[i])
						CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedImmigrant[i])
					//ELSE
						//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					//ENDIF
				ENDIF
			//ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ELIF i = 1

		IF iSeqHintCam = 0
			IF bSecondBlokestunned = FALSE	
				IF IS_PED_UNINJURED(pedImmigrantChase2)
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedImmigrantChase2)
				ENDIF
			ELSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				iTimerHintCamSwitch = GET_GAME_TIMER()
				iSeqHintCam = 1
			ENDIF
		ELIF iSeqHintCam = 1
			IF GET_GAME_TIMER() > iTimerHintCamSwitch + 5	
				IF bBlokestunned = FALSE		
					IF IS_PED_UNINJURED(pedImmigrant[i])
						CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedImmigrant[i])
					ENDIF
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					iSeqHintCam = 2
				ENDIF
			ENDIF
		ENDIF
			/*
			IF bBlokestunned = TRUE
			AND bSecondBlokestunned = TRUE	
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			ENDIF
			*/
	ELSE
		IF iSeqHintCam < 3
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			iSeqHintCam = 3
		ENDIF
	ENDIF
			
	/*	
		IF bBlokestunned = FALSE
			IF IS_VEHICLE_OK(vehChaseCar[i])
				IF IS_PED_UNINJURED(pedImmigrant[i])
					IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
						//CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehChaseCar[i])
						CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedImmigrant[i])
					ELSE
						//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bSECONDBlokestunned = FALSE
				IF IS_VEHICLE_OK(vehChaseCarChase2)
					IF IS_PED_UNINJURED(pedImmigrantChase2)
						IF IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)
							//CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehChaseCarChase2)
							CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedImmigrantChase2)
						ELSE
							//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			ENDIF
		ENDIF
	ENDIF
	*/
	
	IF i = 0
		IF bChaseStarted = TRUE                      //Chase 1
			IF IS_PED_UNINJURED(pedImmigrant[i])	
				IF IS_VEHICLE_OK(vehChaseCar[i])
					IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
						IF DOES_BLIP_EXIST(blipImmigrant[i])
							SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_VEHICLE)
						ENDIF
						/*
						IF IS_PED_FACING_PED(pedImmigrant[i],PLAYER_PED_ID(),90)
						//AND IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),50)	
							IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								TASK_VEHICLE_MISSION_PED_TARGET(pedImmigrant[i],vehChaseCar[i],PLAYER_PED_ID(),MISSION_FLEE,35.0,DRIVINGMODE_AVOIDCARS,100.0,30.0) //100.0,30.0
							ENDIF
						ENDIF
						*/
						IF GET_GAME_TIMER() > iTimerChase1Started + 15000	
							IF GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrant[i],PLAYER_PED_ID()) > 100
								SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],20)
							ELSE
								SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],25)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF i = 1
		IF bChaseStarted = TRUE
			
			IF IS_PED_UNINJURED(pedImmigrant[i])
				IF IS_VEHICLE_OK(vehChaseCar[i])
					IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
						//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])	
							//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<1183.165283,3536.686035,44.112286>>, <<1149.735107,3536.621582,28.927197>>, 25.250000)
							//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<1927.303345,3249.611084,36.220833>>, <<1893.591064,3237.221436,50.346451>>, 18.500000)	
							//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<1300.488525,1099.818481,101.656265>>, <<1307.387573,1002.332336,113.962875>>, 21.250000)
							IF bWander = TRUE	
								IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
									TASK_VEHICLE_DRIVE_WANDER(pedImmigrant[i],vehChaseCar[i],24.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								ENDIF
							ELSE
								//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<394.674927,2869.969971,31.988247>>, <<375.853394,2863.012939,47.178463>>, 18.500000)
								//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<394.727386,2879.424072,36.769821>>, <<407.209137,2861.601318,46.693233>>, 24.750000)
								//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<350.947235,2857.252686,39.281879>>, <<379.071808,2857.875977,50.558121>>, 21.250000)
								IF bBikeLeftFactory = TRUE
									
									IF IS_VEHICLE_OK(vehChaseCarChase2)
									AND IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,pedImmigrant[i],20)	
										fTopSpeed = -1
									ELSE
										fTopSpeed = 22
									ENDIF
									
									IF NOT bSwitchWaypointFlags
										IF GET_VEHICLE_WAYPOINT_PROGRESS(vehChaseCar[i]) > 30
											TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedImmigrant[i],vehChaseCar[i],"Min2_Bike02",DRIVINGMODE_AVOIDCARS_RECKLESS,32,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
											bSwitchWaypointFlags = TRUE
										ENDIF
									ELSE
										IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])	
											TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedImmigrant[i],vehChaseCar[i],"Min2_Bike02",DRIVINGMODE_AVOIDCARS_RECKLESS,32,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,fTopSpeed)
										ELSE
											SET_LEAD_BIKE_TOP_SPEED(fTopSpeed)
										ENDIF
									ENDIF
									
									/*
									IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE)	<> PERFORMING_TASK
									AND	GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
										TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqChase2Route)
									ENDIF
									*/
								ENDIF
							ENDIF
						//ENDIF
						IF bFirstImmigrantOnBike = FALSE
							SET_PED_CAN_RAGDOLL(pedImmigrant[i],TRUE)
							IF DOES_BLIP_EXIST(blipImmigrant[i])
								SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_VEHICLE)
							ENDIF
							bFirstImmigrantOnBike = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_UNINJURED(pedImmigrantChase2)
				IF IS_VEHICLE_OK(vehChaseCarChase2)
					IF IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)
						//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)	
							//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrantChase2, <<1183.165283,3536.686035,44.112286>>, <<1149.735107,3536.621582,28.927197>>, 25.250000)
							//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrantChase2, <<1927.303345,3249.611084,36.220833>>, <<1893.591064,3237.221436,50.346451>>, 18.500000)	
							
								
							IF bWander = TRUE	
								IF NOT IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
								OR NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,pedImmigrant[i],29)	
									IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
										TASK_VEHICLE_DRIVE_WANDER(pedImmigrantChase2,vehChaseCarChase2,24.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
										bBikeEscorting = FALSE
									ENDIF
								ENDIF
							ELSE
								//IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrantChase2, <<394.674927,2869.969971,31.988247>>, <<375.853394,2863.012939,47.178463>>, 18.500000)	
								
								IF bBikeLeftFactory = FALSE 	
									IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrantChase2, <<350.947235,2857.252686,39.281879>>, <<379.071808,2857.875977,50.558121>>, 21.250000)	
										IF IS_PED_UNINJURED(pedImmigrant[i])
											IF IS_VEHICLE_OK(vehChaseCar[i])	
												TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedImmigrant[i],vehChaseCar[i],"Min2_Bike02",DRIVINGMODE_PLOUGHTHROUGH,28,EWAYPOINT_DEFAULT,-1,-1)	//10
												//TASK_VEHICLE_ESCORT(pedImmigrantChase2,vehChaseCarChase2,vehChaseCar[i],VEHICLE_ESCORT_REAR,40.0,DRIVINGMODE_PLOUGHTHROUGH,8)
												TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedImmigrantChase2,vehChaseCarChase2,"Min2_Bike02",DRIVINGMODE_PLOUGHTHROUGH,27,EWAYPOINT_DEFAULT,-1,-1)	//10
											ENDIF
										ENDIF
										bBikeLeftFactory = TRUE
									ENDIF
								ENDIF
								
								//IF bBikeLeftFactory = TRUE	
									IF IS_VEHICLE_OK(vehChaseCar[i])
									AND IS_PED_UNINJURED(pedImmigrant[i])
									AND IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
									AND IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,pedImmigrant[i],22)  //20	
									AND GET_VEHICLE_WAYPOINT_PROGRESS(vehChaseCar[i]) > 29	
										IF bBikeEscorting = FALSE
										//AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK	
											TASK_VEHICLE_ESCORT(pedImmigrantChase2,vehChaseCarChase2,vehChaseCar[i],VEHICLE_ESCORT_REAR,40.0,DRIVINGMODE_PLOUGHTHROUGH,fEscortDistance) //10
											bBikeEscorting = TRUE
										ENDIF
									ELSE
										IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)	
										//IF GET_VEHICLE_WAYPOINT_PROGRESS(vehChaseCar[i]) > 23		
											TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedImmigrantChase2,vehChaseCarChase2,"Min2_Bike02",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,-1)
											bBikeEscorting = FALSE
										ENDIF
									ENDIF		
									/*	
									IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK	
										TASK_PERFORM_SEQUENCE(pedImmigrantChase2,seqChase2RouteSECONDPED)
									ENDIF
									*/
								//ENDIF
							
							ENDIF
						//ENDIF
						IF bImmigrantChase2OnBike = FALSE
							SET_PED_CAN_RAGDOLL(pedImmigrantChase2,TRUE)
							IF DOES_BLIP_EXIST(blipImmigrantChase2)
								SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_VEHICLE)
							ENDIF
							bImmigrantChase2OnBike = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(pedImmigrantChase2, <<1300.488525,1099.818481,101.656265>>, <<1307.387573,1002.332336,113.962875>>, 21.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(pedImmigrant[i], <<1300.488525,1099.818481,101.656265>>, <<1307.387573,1002.332336,113.962875>>, 21.250000)
				bWander = TRUE
			ENDIF
			
			//IF IS_VEHICLE_OK(vehChaseCarChase2)
				//IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)
					//IF IS_VEHICLE_OK(vehChaseCar[i])

			/*
			IF IS_PED_UNINJURED(pedImmigrantChase2)	                   //Chase 2
				IF IS_PED_UNINJURED(pedImmigrant[i])	
					IF IS_VEHICLE_OK(vehChaseCar[i])	
						IF IS_VEHICLE_OK(vehChaseCarChase2)	
							IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
							AND IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)
								fDistBetweenBikers = GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrant[i],pedImmigrantChase2)
								IF fDistBetweenBikers > 10.0
									iNextCoordPed1 = 0
									fDistBiker1ToNextCoord = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedImmigrant[i]),vChase[iNextCoordPed1])
									fDistBiker2ToNextCoord = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedImmigrantChase2),vChase[iNextCoordPed1])
									IF fDistBiker1ToNextCoord < fDistBiker2ToNextCoord
										IF GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrant[i],PLAYER_PED_ID()) < 90
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],29)
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,24)
										ELSE
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],19)
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,14)
										ENDIF
									ELIF fDistBiker1ToNextCoord >= fDistBiker2ToNextCoord
										IF GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrantChase2,PLAYER_PED_ID()) < 90
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,29)
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],24)
										ELSE
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,19)
											SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],14)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])
									IF GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrant[i],PLAYER_PED_ID()) > 90
										SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],17)
									ELSE
										SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrant[i],24)
									ENDIF
								ENDIF
								IF IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)
									IF GET_DISTANCE_BETWEEN_ENTITIES(pedImmigrantChase2,PLAYER_PED_ID()) > 100
										SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,17)
									ELSE
										SET_DRIVE_TASK_CRUISE_SPEED(pedImmigrantChase2,24)
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
		ENDIF
	ENDIF
	
	IF i = 0
		IF bBlokestunned = FALSE
			IF IS_PED_UNINJURED(pedImmigrant[i])	
				SET_ENTITY_LOAD_COLLISION_FLAG(pedImmigrant[i],TRUE)
			ENDIF
		ENDIF
	ELIF i = 1
		IF bBlokestunned = FALSE
			IF IS_PED_UNINJURED(pedImmigrant[i])	
				SET_ENTITY_LOAD_COLLISION_FLAG(pedImmigrant[i],TRUE)
			ENDIF
		ENDIF
		IF bSECONDBlokestunned = FALSE
			IF IS_PED_UNINJURED(pedImmigrantChase2)		
				SET_ENTITY_LOAD_COLLISION_FLAG(pedImmigrantChase2,TRUE)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

/// PURPOSE: Knocks ped off their bike and makes them stagger off
PROC KnockPedOffBike()
	
	REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST) //Taking out the alleged illegals (each instance).
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[i],WEAPONTYPE_STUNGUN)	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "+ 1 Ped got stunned off his bike")
		#ENDIF		
		++iPedsStunnedOffBike
		IF iPedsStunnedOffBike > 2
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "STATS >>>> Shock and Awe <<<< ")
			#ENDIF		
			//IF NOT IS_REPLAY_IN_PROGRESS()	
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIN2_STUNNED_ALL) 
			//ENDIF
		ENDIF             
	ENDIF					
	
	CLEAR_PED_TASKS(pedImmigrant[i])					
						
	vpedImmigrant[i] = GET_ENTITY_COORDS(pedImmigrant[i])
	
	IF IS_PED_IN_ANY_VEHICLE(pedImmigrant[i])	
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrant[i],KNOCKOFFVEHICLE_EASY)
		KNOCK_PED_OFF_VEHICLE(pedImmigrant[i])
	ENDIF
	
	IF DOES_BLIP_EXIST(blipImmigrant[i])
		SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_PED)
	ENDIF
	
	SET_ENTITY_PROOFS(pedImmigrant[i],FALSE,FALSE,FALSE,TRUE,FALSE)

	SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE],CA_USE_COVER,FALSE)
	
	IF i = 0
		SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_USE_COVER,FALSE)
	ENDIF
	
	SET_PED_MOVEMENT_CLIPSET(pedImmigrant[i],"MOVE_M@BAIL_BOND_NOT_TAZERED")

	GET_NTH_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOE],<<-20.0,0.0,0.0>>),129,vSafeCoord)
	
	iGameTimerPed1KnockedOff = GET_GAME_TIMER()
	
	IF IS_ENTITY_ALIVE(vehChaseCar[i])
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])
			STOP_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i])
		ENDIF
	ENDIF
	
	IF i = 0
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")    //Bullseye! etc
		IF iPedsStunnedOffBike <> 0
			CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_7", CONV_PRIORITY_LOW)
		ELSE
			CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_RAM", CONV_PRIORITY_LOW)
		ENDIF
		//IF NOT IS_REPLAY_IN_PROGRESS()	
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE,MIN2_FIRST_CAPTURE) 
		//ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "STATS >>>> TIME WINDOW CLOSED (First chase) <<<< ")
		#ENDIF		
	ENDIF
	IF i = 1
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE")
		//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_9", CONV_PRIORITY_LOW)
		CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_9", CONV_PRIORITY_LOW)
		IF bSECONDBlokestunned = FALSE
			iTimerDontLetTheOtherEscape = GET_GAME_TIMER()
			bDialogueDontletTheOtherEscape = TRUE
		ENDIF
	ENDIF
	
	bBlokestunned = TRUE

ENDPROC

/// PURPOSE: Knocks ped off their bike and makes them stagger off
PROC KnockSECONDPedOffBike()
	
	REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST)		//Taking out the alleged illegals (each instance).
			
	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrantChase2,WEAPONTYPE_STUNGUN)	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "+ 1 Ped got stunned off his bike")
		#ENDIF		
		++iPedsStunnedOffBike
		IF iPedsStunnedOffBike > 2
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "STATS >>>> Shock and Awe <<<< ")
			#ENDIF		
			//IF NOT IS_REPLAY_IN_PROGRESS()		
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIN2_STUNNED_ALL) 
			//ENDIF
		ENDIF             
	ENDIF									
	
	CLEAR_PED_TASKS(pedImmigrantChase2)						
					
	IF IS_PED_ON_ANY_BIKE(pedImmigrantChase2)
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrantChase2,KNOCKOFFVEHICLE_EASY)
		KNOCK_PED_OFF_VEHICLE(pedImmigrantChase2)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipImmigrantChase2)
		SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_PED)
	ENDIF
	
	SET_ENTITY_PROOFS(pedImmigrantChase2,FALSE,FALSE,FALSE,TRUE,FALSE)

	SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF],CA_USE_COVER,FALSE)
	
	SET_PED_MOVEMENT_CLIPSET(pedImmigrantChase2,"MOVE_M@BAIL_BOND_NOT_TAZERED")

	GET_NTH_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOSEF],<<-20.0,0.0,0.0>>),97,vSafeCoord)
	
	iGameTimerPed2KnockedOff = GET_GAME_TIMER()
	
	IF IS_ENTITY_ALIVE(vehChaseCarChase2)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2)
		ENDIF
	ENDIF
	
	CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_8", CONV_PRIORITY_LOW)
	
	IF bBlokestunned = FALSE
		iTimerDontLetTheOtherEscape = GET_GAME_TIMER()
		bDialogueDontletTheOtherEscape = TRUE
	ENDIF
	
	bSECONDBlokestunned = TRUE

ENDPROC

/// PURPOSE: Makes immigrant put his hands up and get in the vehicle
PROC SetImmigrantChase2HandsUp()
				
	CLEAR_PED_TASKS(pedImmigrantChase2)
	//RESET_PED_MOVEMENT_CLIPSET(pedImmigrantChase2)
	/*
	OPEN_SEQUENCE_TASK(seqImmigrantBeCarried)
		TASK_SET_DECISION_MAKER(NULL,DECISION_MAKER_EMPTY)
		TASK_ENTER_VEHICLE(NULL,sRCLauncherDataLocal.vehID[0],-1,VS_BACK_RIGHT,PEDMOVEBLENDRATIO_WALK)
	CLOSE_SEQUENCE_TASK(seqImmigrantBeCarried)
	TASK_PERFORM_SEQUENCE(pedImmigrantChase2,seqImmigrantBeCarried)
	*/
	IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
		IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
			TASK_ENTER_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
		ENDIF
	ENDIF
	
	bImmigrantChase2HandsUp = TRUE
	
ENDPROC

/// PURPOSE: Makes immigrant put his hands up and get in the vehicle
PROC SetImmigrantHandsUp()
				
	CLEAR_PED_TASKS(pedImmigrant[i])
	//RESET_PED_MOVEMENT_CLIPSET(pedImmigrant[i])
	/*
	OPEN_SEQUENCE_TASK(seqImmigrantBeCarried)
		TASK_SET_DECISION_MAKER(NULL,DECISION_MAKER_EMPTY)
		TASK_ENTER_VEHICLE(NULL,sRCLauncherDataLocal.vehID[0],-1,VS_BACK_RIGHT,PEDMOVEBLENDRATIO_WALK)
	CLOSE_SEQUENCE_TASK(seqImmigrantBeCarried)
	TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqImmigrantBeCarried)
	*/
	IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
		IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
			TASK_ENTER_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
		ENDIF
	ENDIF
	
	bImmigrantHandsUp = TRUE
	
ENDPROC

/// PURPOSE: Immigrant got in car so attach in the back
PROC AttachPedToCar()
						
	IF i < 2	
		IF IS_PED_UNINJURED(pedImmigrant[i])	
			IF bpedImmigrantChase2Attached = FALSE
				//CLEAR_PED_TASKS_IMMEDIATELY(pedImmigrant[i])
				vpedImmigrant[i] = GET_ENTITY_COORDS(pedImmigrant[i])
			ELSE
				CLEAR_PED_TASKS(pedImmigrant[i])
				CLEAR_SEQUENCE_TASK(seqChase2Route)
				CLEAR_SEQUENCE_TASK(seqChase2RouteB)
				CLEAR_SEQUENCE_TASK(seqChase2RouteC)
				CLEAR_SEQUENCE_TASK(seqChase2RouteD)
			ENDIF
			//SET_ENTITY_COLLISION(pedImmigrant[i],FALSE)
			SET_PED_CAN_BE_TARGETTED(pedImmigrant[i],FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedImmigrant[i],GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
		ENDIF
	ENDIF	

	SAFE_REMOVE_BLIP(blipJosef)		

	IF i = 0
		
		PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[i])
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrant[i])
		//RESET_PED_MOVEMENT_CLIPSET(pedImmigrant[i])
		//ssClimbToBack_1 = CREATE_SYNCHRONIZED_SCENE(<<0.555,-0.79,0.171>>,<<0,0,0>>)
		ssClimbToBack_1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssClimbToBack_1,TRUE)
		ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_1,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
		
		TASK_SYNCHRONIZED_SCENE(pedImmigrant[i], ssClimbToBack_1, "missminuteman_2ig_1", "entertrunk_manuel", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
		//SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1,0.1)
		SET_SYNCHRONIZED_SCENE_RATE(ssClimbToBack_1,2.0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrant[i],TRUE)
	ENDIF
	
	IF i = 1
		IF bpedImmigrantChase2Attached = FALSE
			
			IF NOT IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				PROCESS_ENTITY_ATTACHMENTS(pedImmigrant[i])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrant[i])
				//RESET_PED_MOVEMENT_CLIPSET(pedImmigrant[i])
				//ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0.39,-0.8,0.171>>,<<0,0,0>>)
				//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],-1)
				ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssClimbToBack_2,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
				
				TASK_SYNCHRONIZED_SCENE(pedImmigrant[i], ssClimbToBack_2, "missminuteman_2ig_1", "entertrunk_josef", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
				//SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2,0.06)
				SET_SYNCHRONIZED_SCENE_RATE(ssClimbToBack_2,2.0)
			ELSE
				CLEAR_PED_TASKS(pedImmigrant[i])
				CLEAR_SEQUENCE_TASK(seqChase2Route)
				CLEAR_SEQUENCE_TASK(seqChase2RouteB)
				CLEAR_SEQUENCE_TASK(seqChase2RouteC)
				CLEAR_SEQUENCE_TASK(seqChase2RouteD)
				bDoDelayedClimbInBack = TRUE
			ENDIF
			iSyncPed = 1
		
		ENDIF
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrant[i],TRUE)
	ENDIF
	
	IF bDebugSkippedToCementFactory = FALSE	
		IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
			IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
			AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
				IF i = 0
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_RUN)
				ELSE	
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF missionStage = MS_UPDATE_CHASE1
	OR missionStage = MS_IMMIGRANT1_APPREHENDED	
		IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			IF IS_IT_OK_FOR_JOSEF_TO_GET_IN()	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
					IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],10.0)	
						TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1)
					ELSE
						TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
					ENDIF
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
					TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.2,0>>),1,DEFAULT_TIME_BEFORE_WARP_MINUTE_2,0.5,ENAV_DEFAULT,GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])-90.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF i = 0
		bJosefCombatTaskGiven = FALSE
		bJoeCombatTaskGiven = FALSE
		bBlokestunned = FALSE
		bImmigrantHandsUp = FALSE
		bOkForJosefToLeaveCar = FALSE
		bOkForJoeToLeaveCar = FALSE
		bOkToStartChase = FALSE
		bGetCloseMessagePrinted = FALSE
		bImmigrantGettingIntoCar = FALSE
		bEveryoneInCar = FALSE
		iTimesTazed = 0
		bChaseStarted = FALSE
	ENDIF
	
	IF i = 0
		TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[ic_JOSEF])
		TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[ic_JOE])
		bBloke1AttachedToCar = TRUE
	ENDIF
	
	IF i = 1
		TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[ic_JOE])
		bBloke2AttachedToCar = TRUE
		IF bSECONDBlokestunned = TRUE
			IF bpedImmigrantChase2Attached = FALSE
				IF IS_PED_UNINJURED(pedImmigrantChase2)
					IF GET_DISTANCE_BETWEEN_PEDS(pedImmigrantChase2,PLAYER_PED_ID()) > 70.0	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0],TRUE)	
							PRINT_NOW("MIN2_31",DEFAULT_GOD_TEXT_TIME,0)
						ELSE
							IF bPrint19 = FALSE	
								PRINT_NOW("MIN2_19",DEFAULT_GOD_TEXT_TIME,0)
								bPrint19 = TRUE
							ENDIF
							IF DOES_BLIP_EXIST(blipImmigrant[i])
								SAFE_REMOVE_BLIP(blipImmigrant[i])
							ENDIF
							IF DOES_BLIP_EXIST(blipImmigrantChase2)
								SAFE_REMOVE_BLIP(blipImmigrantChase2)
							ENDIF
							IF NOT DOES_BLIP_EXIST(blipPlayerCar)
								blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
							ENDIF
							bPlayerNotInCarBetweenCapturingImmigrants = TRUE
						ENDIF
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_12", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
						CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_12", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF i = 2
		REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 5)
	ENDIF
	
	IF i = 0
		++i
	ENDIF
	
	iTimerPlayerNotInCar = GET_GAME_TIMER()
							
ENDPROC

/// PURPOSE: Immigrant got in car so attach in the back
PROC AttachSECONDPedToCar()
								
	IF IS_PED_UNINJURED(pedImmigrantChase2)
		IF bBloke2AttachedToCar = FALSE
			
			IF NOT IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				PROCESS_ENTITY_ATTACHMENTS(pedImmigrantChase2)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedImmigrantChase2)
				//RESET_PED_MOVEMENT_CLIPSET(pedImmigrantChase2)
				//ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0.39,-0.8,0.171>>,<<0,0,0>>)
				//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],-1)
				ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
				
				TASK_SYNCHRONIZED_SCENE(pedImmigrantChase2, ssClimbToBack_2, "missminuteman_2ig_1", "entertrunk_josef", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
				//SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2,0.06)
				SET_SYNCHRONIZED_SCENE_RATE(ssClimbToBack_2,2.0)
			ELSE
				CLEAR_PED_TASKS(pedImmigrantChase2)
				CLEAR_SEQUENCE_TASK(seqChase2RouteSECONDPED)
				CLEAR_SEQUENCE_TASK(seqChase2RouteBSECONDPED)
				CLEAR_SEQUENCE_TASK(seqChase2RouteCSECONDPED)
				CLEAR_SEQUENCE_TASK(seqChase2RouteDSECONDPED)
				bDoDelayedClimbInBack = TRUE
			ENDIF
			iSyncPed = 2
		
		ELSE
			CLEAR_PED_TASKS(pedImmigrantChase2)
			CLEAR_SEQUENCE_TASK(seqChase2RouteSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteBSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteCSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteDSECONDPED)
		ENDIF
		SET_PED_CAN_BE_TARGETTED(pedImmigrantChase2,FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedImmigrantChase2,GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedImmigrantChase2,TRUE)
	ENDIF	
							
	IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
	AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
		TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT)
	ENDIF
	
	IF i = 1
		IF bBlokestunned = TRUE
			IF bBloke2AttachedToCar = FALSE
				IF IS_PED_UNINJURED(pedImmigrant[i])
					IF GET_DISTANCE_BETWEEN_PEDS(pedImmigrant[i],PLAYER_PED_ID()) > 70.0	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0],TRUE)	
							PRINT_NOW("MIN2_31",DEFAULT_GOD_TEXT_TIME,0)
						ELSE
							IF bPrint19 = FALSE	
								PRINT_NOW("MIN2_19",DEFAULT_GOD_TEXT_TIME,0)
								bPrint19 = TRUE
							ENDIF
							IF DOES_BLIP_EXIST(blipImmigrant[i])
								SAFE_REMOVE_BLIP(blipImmigrant[i])
							ENDIF
							IF DOES_BLIP_EXIST(blipImmigrantChase2)
								SAFE_REMOVE_BLIP(blipImmigrantChase2)
							ENDIF
							IF NOT DOES_BLIP_EXIST(blipPlayerCar)
								blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
							ENDIF
							bPlayerNotInCarBetweenCapturingImmigrants = TRUE
						ENDIF
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_13", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
						//CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_13", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	bpedImmigrantChase2Attached = TRUE
	
	TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[ic_JOSEF])
	
	iTimerPlayerNotInCar = GET_GAME_TIMER()
							
ENDPROC

FUNC BOOL ARE_GUYS_IN_CAR_OR_DOING_SCENE()
	
	IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0]) 
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
		RETURN TRUE			
	ENDIF
	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)
		RETURN TRUE	
	ENDIF
	RETURN FALSE

ENDFUNC

/// PURPOSE: Manage all the blips and objectives
PROC ManageBlipsAndObjectives()
	 
	IF missionStage <> MS_DRIVE_TO_END
	AND missionStage <> MS_MISSION_PASSING
	AND missionStage <> MS_MISSION_FAILING
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
			SAFE_REMOVE_BLIP(blipPlayerCar)
		ELSE
			IF NOT DOES_BLIP_EXIST(blipPlayerCar)
				blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
			ENDIF
		ENDIF
		IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
			SAFE_REMOVE_BLIP(blipJosef)
		ELSE	
			IF NOT DOES_BLIP_EXIST(blipJosef)	
				blipJosef = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOE],TRUE,TRUE)
				SET_BLIP_SCALE(blipJosef,BLIP_SIZE_PED)
				SET_BLIP_PRIORITY(blipJosef,BLIPPRIORITY_LOW)
			ENDIF
		ENDIF	
		IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
			SAFE_REMOVE_BLIP(blipJoe)
		ELSE	
			IF NOT DOES_BLIP_EXIST(blipJoe)	
				blipJoe = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE,TRUE)
				SET_BLIP_SCALE(blipJoe,BLIP_SIZE_PED)
				SET_BLIP_PRIORITY(blipJoe,BLIPPRIORITY_LOW)
			ENDIF
		ENDIF
	ENDIF	
		
	IF missionStage = MS_SETUP_MISSION	
	OR missionStage = MS_SETUP_CHASE1
	OR missionStage = MS_SETUP_CHASE2
	OR missionStage = MS_SETUP_CHASE3
	
		bPlayerAtChaseOnFootMessagePrinted = FALSE
		
		SAFE_REMOVE_BLIP(blipImmigrant[0])
		SAFE_REMOVE_BLIP(blipImmigrant[1])
		SAFE_REMOVE_BLIP(blipImmigrant[2])

		IF DOES_ENTITY_EXIST(pedImmigrant[0])
			//fDistBetweenJosefAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				//SAFE_REMOVE_BLIP(blipPlayerCar)
				//IF bBlipCar
					bBlipCar = FALSE
				//ENDIF		
				IF ARE_GUYS_IN_CAR_OR_DOING_SCENE()
					//IF bBlipJoe
						bBlipJoe = FALSE
					//ENDIF		
					//IF bBlipJosef
						bBlipJosef = FALSE
					//ENDIF		
					//SAFE_REMOVE_BLIP(blipJosef)
					//SAFE_REMOVE_BLIP(blipJoe)
					IF bEveryoneInCar = FALSE
						bEveryoneInCar = TRUE
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipLocate[i])	
						//CLEAR_PRINTS()
						IF i = 0                            //Head towards the trailer park.
							IF bPrint03 = FALSE
								//PRINT_NOW("MIN2_03",7000,0)           //Go to locate
								bPrint03 = TRUE
							ENDIF
						ENDIF
						IF i = 1                         //The next target works at the cement factory.
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
								IF bPrint06 = FALSE	
									IF bReplayCementFactory = FALSE
										PRINT_NOW("MIN2_06",7000,0)
										bPrint06 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF i = 2                         //Head towards the gas station.	
							IF bPrint08 = FALSE		
								PRINT_NOW("MIN2_08",7000,0)
								bPrint08 = TRUE
							ENDIF
						ENDIF
						IF i = 0
							//blipLocate[i] = CREATE_COORD_BLIP(<< 166.3456, 4418.0342, 74.5901 >>)
						ELSE	
							IF i = 1
								blipLocate[i] = CREATE_COORD_BLIP(<< 267.2497, 2852.1667, 42.6129 >>)
							ELSE
								vpedImmigrant[i] = GET_ENTITY_COORDS(pedImmigrant[i])
								blipLocate[i] = CREATE_COORD_BLIP(vpedImmigrant[i])
							ENDIF
						ENDIF
						iConvoTimer = GET_GAME_TIMER()
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipLocate[i])
					IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						//IF bBlipJosef
							bBlipJosef = FALSE
						//ENDIF		
						//SAFE_REMOVE_BLIP(blipJosef)
					ELSE	
						//IF NOT DOES_BLIP_EXIST(blipJosef)
						IF NOT bBlipJosef
							IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],TRUE)		
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
									TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT)
								ENDIF
							ENDIF
							//blipJosef = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOE],TRUE,TRUE)
							//SET_BLIP_SCALE(blipJosef,BLIP_SIZE_PED)
							bBlipJosef = TRUE
							IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
								IF bPrint33 = FALSE
									PRINT_NOW("MIN2_33",7000,0)           //Wait for ~b~Joe~s~ to get in the vehicle.
									bPrint33 = TRUE
								ENDIF
							ELSE	
								IF bPrint29 = FALSE
									PRINT_NOW("MIN2_29",7000,0)           //Wait for ~b~Josef~s~ and ~b~Joe~s~ to get in the vehicle.
									bPrint29 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					//OR IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
						//IF DOES_BLIP_EXIST(blipJoe)
						//	SAFE_REMOVE_BLIP(blipJoe)
						//ENDIF	
						//IF bBlipJoe
							bBlipJoe = FALSE
						//ENDIF	
					ELSE
						//IF NOT DOES_BLIP_EXIST(blipJoe)
						IF NOT bBlipJoe	
							IF IS_IT_OK_FOR_JOSEF_TO_GET_IN()	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
								//AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)		
									IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],10.0)	
										TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1)
									ELSE
										TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
									TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.2,0>>),1,DEFAULT_TIME_BEFORE_WARP_MINUTE_2,0.5,ENAV_DEFAULT,GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])-90.0)
								ENDIF
							ENDIF
							//blipJoe = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE,TRUE)
							//SET_BLIP_SCALE(blipJoe,BLIP_SIZE_PED)
							bBlipJoe = TRUE
							IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
								IF bPrint32 = FALSE	
									PRINT_NOW("MIN2_32",7000,0)           //Wait for ~b~Josef~s~ to get in the vehicle.
									bPrint32 = TRUE
								ENDIF
							ELSE	
								IF bPrint29 = FALSE		
									PRINT_NOW("MIN2_29",7000,0)           //Wait for ~b~Josef~s~ and ~b~Joe~s~ to get in the vehicle.
									bPrint29 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//SAFE_REMOVE_BLIP(blipJosef)
				//SAFE_REMOVE_BLIP(blipJoe)
				//IF bBlipJoe
					bBlipJoe = FALSE
				//ENDIF		
				//IF bBlipJosef
					bBlipJosef = FALSE
				//ENDIF		
				SAFE_REMOVE_BLIP(blipLocate[i])
				//IF NOT DOES_BLIP_EXIST(blipPlayerCar)	
				IF NOT bBlipCar	
					//CLEAR_PRINTS()
					IF bPrint19 = FALSE	
						//IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						//AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
							//KILL_ANY_CONVERSATION()                       //Where are you going?
							//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR", "MIN2_CAR_3", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
						//ENDIF
						PRINT_NOW("MIN2_19",7000,0)                //Get back in Josef's Truck
						bPrint19 = TRUE
					ENDIF
					//blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
					bBlipCar = TRUE
					iTimerPlayerNotInCar = GET_GAME_TIMER()
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

	IF missionStage = MS_DRIVE_TO_END
		
		bPlayerAtChaseOnFootMessagePrinted = FALSE
		
		SAFE_REMOVE_BLIP(blipImmigrant[2])
		SAFE_REMOVE_BLIP(blipImmigrant[1])
		SAFE_REMOVE_BLIP(blipImmigrantChase2)
		
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
			SAFE_REMOVE_BLIP(blipPlayerCar)
			//IF bBlipCar
				bBlipCar = FALSE
			//ENDIF
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
				SAFE_REMOVE_BLIP(blipJosef)
				SAFE_REMOVE_BLIP(blipJoe)
				//IF bBlipJoe
					bBlipJoe = FALSE
				//ENDIF		
				//IF bBlipJosef
					bBlipJosef = FALSE
				//ENDIF		
				IF NOT DOES_BLIP_EXIST(blipLocate[i])	
					bDrivingToMotel = TRUE
					iConvoTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				SAFE_REMOVE_BLIP(blipLocate[i])
				IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					SAFE_REMOVE_BLIP(blipJosef)
					//IF bBlipJosef
						bBlipJosef = FALSE
					//ENDIF	
				ELSE	
					IF NOT DOES_BLIP_EXIST(blipJosef)
					AND NOT bBlipJosef
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
							TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT)
						ENDIF
						blipJosef = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOE],TRUE,TRUE)
						SET_BLIP_SCALE(blipJosef,BLIP_SIZE_PED)
						bBlipJosef = TRUE
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
							IF bPrint33 = FALSE	
								PRINT_NOW("MIN2_33",7000,0)           //Wait for ~b~Joe~s~ to get in the vehicle.
								bPrint33 = TRUE
							ENDIF
						ELSE	
							IF bPrint29 = FALSE		
								PRINT_NOW("MIN2_29",7000,0)           //Wait for ~b~Josef~s~ and ~b~Joe~s~ to get in the vehicle.
								bPrint29 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					SAFE_REMOVE_BLIP(blipJoe)			
					//IF bBlipJoe
						bBlipJoe = FALSE
					//ENDIF	
				ELSE
					IF NOT DOES_BLIP_EXIST(blipJoe)
					AND NOT bBlipJoe	
						IF IS_IT_OK_FOR_JOSEF_TO_GET_IN()	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
								IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],10.0)	
									TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1)
								ELSE
									TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
								TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.2,0>>),1,DEFAULT_TIME_BEFORE_WARP_MINUTE_2,0.5,ENAV_DEFAULT,GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])-90.0)
							ENDIF
						ENDIF
						blipJoe = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE,TRUE)
						SET_BLIP_SCALE(blipJoe,BLIP_SIZE_PED)
						bBlipJoe = TRUE
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
							IF bPrint32 = FALSE
								PRINT_NOW("MIN2_32",7000,0)           //Wait for ~b~Josef~s~ to get in the vehicle.
								bPrint32 = TRUE
							ENDIF
						ELSE	
							IF bPrint29 = FALSE	
								PRINT_NOW("MIN2_29",7000,0)           //Wait for ~b~Josef~s~ and ~b~Joe~s~ to get in the vehicle.
								bPrint29 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//IF bBlipJoe
				bBlipJoe = FALSE
			//ENDIF		
			//IF bBlipJosef
				bBlipJosef = FALSE
			//ENDIF		
			SAFE_REMOVE_BLIP(blipJosef)
			SAFE_REMOVE_BLIP(blipJoe)
			SAFE_REMOVE_BLIP(blipLocate[i])
		ENDIF	
	ENDIF
	
	IF missionStage = MS_UPDATE_CHASE1
	OR missionStage = MS_UPDATE_CHASE2	
	OR missionStage = MS_UPDATE_CHASE3	
		
		//SAFE_REMOVE_BLIP(blipJosef)
		//SAFE_REMOVE_BLIP(blipJoe)
		
		//IF bBlipJoe
			bBlipJoe = FALSE
		//ENDIF		
		//IF bBlipJosef
			bBlipJosef = FALSE
		//ENDIF		
		
		IF i = 0
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
				//SAFE_REMOVE_BLIP(blipPlayerCar)
				//IF bBlipCar
					bBlipCar = FALSE
				//ENDIF		
			ELSE
				IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
				AND bBlokestunned = FALSE
					SAFE_REMOVE_BLIP(blipImmigrant[i])
					SAFE_REMOVE_BLIP(blipImmigrantChase2)
					SAFE_REMOVE_BLIP(blipLocate[i])
					//IF NOT DOES_BLIP_EXIST(blipPlayerCar)
					IF NOT bBlipCar	
						IF bPrint19 = FALSE	
							PRINT_NOW("MIN2_19",7000,0)                //Get back in Josef's Truck
							bPrint19 = TRUE
						ENDIF
						//blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
						bBlipCar = TRUE
						iTimerPlayerNotInCar = GET_GAME_TIMER()
						bPlayerAtChaseOnFootMessagePrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			iChaseStartedTimerUpdate = GET_GAME_TIMER()
			IF iChaseStartedTimerUpdate > iChaseStartedTimer + 0 //4200		
				SAFE_REMOVE_BLIP(blipLocate[i])
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
						IF NOT DOES_BLIP_EXIST(blipImmigrant[i])
							blipImmigrant[i] = CREATE_PED_BLIP(pedImmigrant[i])
							//CLEAR_PRINTS()
							IF bPrint01	= FALSE
								PRINT_NOW("MIN2_01",7000,0)                 //chase the immigrant
								bPrint01 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
				//SAFE_REMOVE_BLIP(blipPlayerCar)
				//IF bBlipCar
					bBlipCar = FALSE
				//ENDIF	
			ELSE
				IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
				AND bBlokestunned = FALSE
				AND bSecondBlokestunned = FALSE
					SAFE_REMOVE_BLIP(blipImmigrant[i])
					SAFE_REMOVE_BLIP(blipImmigrantChase2)
					SAFE_REMOVE_BLIP(blipLocate[i])
					//IF NOT DOES_BLIP_EXIST(blipPlayerCar)
					IF NOT bBlipCar	
						IF bPrint19 = FALSE	
							PRINT_NOW("MIN2_19",7000,0)                //Get back in Josef's Truck
							bPrint19 = TRUE
						ENDIF
						//blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
						bBlipCar = TRUE
						iTimerPlayerNotInCar = GET_GAME_TIMER()
						bPlayerAtChaseOnFootMessagePrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
				//SAFE_REMOVE_BLIP(blipPlayerCar)
				//IF bBlipCar
					bBlipCar = FALSE
				//ENDIF	
			ELSE
				IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
				AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOSEF]) > 90.0
					SAFE_REMOVE_BLIP(blipImmigrant[i])
					SAFE_REMOVE_BLIP(blipImmigrantChase2)
					//IF NOT DOES_BLIP_EXIST(blipPlayerCar)
					IF NOT bBlipCar	
						IF bPrint42 = FALSE
							//PRINT_NOW("MIN2_42",7000,0)                           //Get back in Josef's Truck
							bPrint42 = TRUE
						ENDIF
						//blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
						bBlipCar = TRUE
						iTimerPlayerNotInCar = GET_GAME_TIMER()
						SET_BLIP_AS_FRIENDLY(blipPlayercar,TRUE)
						bPlayerAtChaseOnFootMessagePrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			iChaseStartedTimerUpdate = GET_GAME_TIMER()
			IF iChaseStartedTimerUpdate > iChaseStartedTimer + 0
				SAFE_REMOVE_BLIP(blipLocate[i])
				IF bBloke2AttachedToCar = FALSE
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
							IF NOT DOES_BLIP_EXIST(blipImmigrant[i])
								blipImmigrant[i] = CREATE_PED_BLIP(pedImmigrant[i])
								IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[i])	
									SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_PED)
								ELSE
									SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_VEHICLE)
								ENDIF
								//CLEAR_PRINTS()
								IF bPrint30 = FALSE
									//PRINT_NOW("MIN2_30",7000,0)       //chase the immigrants
									bPrint30 = TRUE
									IF i = 0                                        //There he is! Get after him! etc             
										//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF", "MIN2_JOSEF_4", CONV_PRIORITY_VERY_LOW)
										CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF_4", CONV_PRIORITY_LOW)
									ELIF i = 1
										ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE")
										//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_15", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
										CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_15", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
									ELIF i = 2
										//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF", "MIN2_JOSEF_6", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
									ENDIF
								ENDIF
								IF bPlayerNotInCarBetweenCapturingImmigrants = TRUE
									IF bPrint31 = FALSE
										PRINT_NOW("MIN2_31",7000,0)       //Go back and pick up the ~r~questionable.
										bPrint31 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT DOES_BLIP_EXIST(blipImmigrant[i])
								IF bPlayerNotInCarBetweenCapturingImmigrants = FALSE	
									IF bPlayerAtChaseOnFootMessagePrinted = FALSE
										IF bPrint39 = FALSE
											PRINT_NOW("MIN2_39",7000,0)                                  //The Immigrants are escaping! Get in the ~b~vehicle.
											bPrint39 = TRUE
										ENDIF
										bPlayerAtChaseOnFootMessagePrinted = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF

				ELSE	
					SAFE_REMOVE_BLIP(blipImmigrant[i])
				ENDIF
				
				IF i = 1
					IF bpedImmigrantChase2Attached = FALSE	
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
								IF NOT DOES_BLIP_EXIST(blipImmigrantChase2)
									blipImmigrantChase2 = CREATE_PED_BLIP(pedImmigrantChase2)
									IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)	
										SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_PED)
									ELSE
										SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_VEHICLE)
									ENDIF
									IF bPlayerNotInCarBetweenCapturingImmigrants = TRUE
										IF bPrint31 = FALSE
											PRINT_NOW("MIN2_31",7000,0)       //Go back and pick up the ~r~questionable.
											bPrint31 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SAFE_REMOVE_BLIP(blipImmigrantChase2)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: AI for Joe and Josef during chase 1
PROC Chase1JJAI() 
	
		IF IS_PED_UNINJURED(pedImmigrant[i])
	
			fDistBetweenJosefAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
			IF missionStage = MS_UPDATE_CHASE2
			OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2
				fDistBetweenJoeAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
			ELSE
				fDistBetweenJoeAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])
			ENDIF
			IF bGetCloseMessagePrinted = FALSE
				IF bBlokestunned = FALSE
					IF GET_GAME_TIMER() > iChaseStartedTimer + 20000	
						IF fDistBetweenJosefAndImmigrant < 50.0
							IF i = 0		 							       //get me close so i can taze the bastard
								TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								IF ARE_STRINGS_EQUAL(root,"MIN2_JJ1")				
								OR ARE_STRINGS_EQUAL(root,"MIN2_JJ4")				
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF", "MIN2_JOSEF_2", CONV_PRIORITY_VERY_LOW)
									IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF_2", CONV_PRIORITY_LOW)	
										bGetCloseMessagePrinted = TRUE
									ENDIF
								ENDIF
							ENDIF
							IF i = 1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									bGetCloseMessagePrinted = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF bBlokestunned = FALSE
				IF fDistBetweenJosefAndImmigrant > 200.0
					IF bGettingAwayMessagePrinted = FALSE
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
								IF i = 0
									IF GET_GAME_TIMER() > iTimerChase1Started + 12000		
										IF bPrint18 = FALSE
											//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
											bPrint18 = TRUE
										ENDIF
									ENDIF
								ELSE
									IF bBlokestunned = FALSE
									AND bSECONDBlokestunned = FALSE
										IF bPrint37 = FALSE	
											//PRINT_NOW("MIN2_37",7000,0)                          //The immigrants are getting away
											bPrint37 = TRUE
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > iTimerChase1Started + 12000	
											IF bPrint18 = FALSE
												//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
												bPrint18 = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//iGameTimer = GET_GAME_TIMER()
						bGettingAwayMessagePrinted = TRUE
					ELSE
						//iGameTimerUpdate = GET_GAME_TIMER()
						//IF iGameTimerUpdate > (iGameTimer + 20000)
							IF fDistBetweenJosefAndImmigrant > 300.0//260.0
								IF i = 0
									sFailReason = "MIN2_43"
								ELSE	
									IF bBlokestunned = FALSE
									AND bSECONDBlokestunned = FALSE	
										sFailReason = "MIN2_35"    
									ELSE	
										sFailReason = "MIN2_23"
									ENDIF
								ENDIF
								missionStage = MS_MISSION_FAILING
							ENDIF
						//ENDIF
					ENDIF
				ELSE
					bGettingAwayMessagePrinted = FALSE
				ENDIF
			ENDIF
		
			IF bBlokestunned = FALSE
				IF fDistBetweenJosefAndImmigrant < 9.0                 //josef taze ped if within 10 meters otherwise clear tasks
				AND IS_PED_IN_ANY_VEHICLE(pedImmigrant[i])	
					IF bJosefCombatTaskGiven = FALSE
						//TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
						TASK_DRIVE_BY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],NULL,<<0,0,0>>,10,100)
						IF IS_PED_ON_ANY_BIKE(pedImmigrant[i])
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrant[i],KNOCKOFFVEHICLE_EASY)
						ENDIF
						bJosefCombatTaskGiven = TRUE
					ENDIF
				ELSE
					IF bJosefCombatTaskGiven = TRUE
					OR GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_COMBAT) = PERFORMING_TASK	
					OR IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])	
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOE])
						bJosefCombatTaskGiven = FALSE
					ENDIF
				ENDIF
			ELSE	
				IF fDistBetweenJosefAndImmigrant >= 40.0	
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOE])
				ENDIF
			ENDIF				
			IF missionStage = MS_UPDATE_CHASE2
			OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2
				/*
				IF IS_PED_UNINJURED(pedImmigrantChase2)	
					IF bSECONDBlokestunned = FALSE
						IF fDistBetweenJoeAndImmigrant < 9.0                
							IF bJoeCombatTaskGiven = FALSE
								//TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
								TASK_DRIVE_BY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,NULL,<<0,0,0>>,10,100)
								IF IS_PED_ON_ANY_BIKE(pedImmigrantChase2)
									SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrantChase2,KNOCKOFFVEHICLE_EASY)
								ENDIF
								bJoeCombatTaskGiven = TRUE
							ENDIF
						ELSE
							IF bJoeCombatTaskGiven = TRUE
								CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
								bJoeCombatTaskGiven = FALSE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bImmigrantDied = TRUE
				ENDIF
				*/
			ELSE
				IF bBlokestunned = FALSE
					IF fDistBetweenJoeAndImmigrant < 9.0                
					AND IS_PED_IN_ANY_VEHICLE(pedImmigrant[i])	
						IF bJoeCombatTaskGiven = FALSE
							//TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])
							TASK_DRIVE_BY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],NULL,<<0,0,0>>,10,100)
							bJoeCombatTaskGiven = TRUE
						ENDIF
					ELSE
						IF bJoeCombatTaskGiven = TRUE
						OR GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) = PERFORMING_TASK	
						OR IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])		
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
							bJoeCombatTaskGiven = FALSE
						ENDIF
					ENDIF
				ELSE	
					IF fDistBetweenJoeAndImmigrant >= 40.0		
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
					ENDIF
				ENDIF		
			ENDIF
			IF bBlokestunned = FALSE
				//IF CAN_PED_RAGDOLL(pedImmigrant[i])	
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[i],WEAPONTYPE_STUNGUN)	
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehChaseCar[i],sRCLauncherDataLocal.vehID[0])	
						//vNodeSwitch1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,-70,0>>)
						//vNodeSwitch2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,70,0>>)
						//SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,140,FALSE,FALSE)
						KnockPedOffBike()                                                     //Knock ped off bike if he gets tazed                     
					ENDIF
				//ENDIF
			ENDIF
			IF 	missionStage = MS_UPDATE_CHASE2
			OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2
				IF bSECONDBlokestunned = FALSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrantChase2,WEAPONTYPE_STUNGUN)	
						KnockSECONDPedOffBike()                                                     //Knock ped off bike if he gets tazed             
					ENDIF
				ENDIF
			ENDIF	
			IF bBlokestunned = FALSE
				IF IS_VEHICLE_OK(vehChaseCar[i])	
					IF GET_GAME_TIMER() > iTimerChase1Started + 15000		
						IF NOT IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])		
							//IF CAN_PED_RAGDOLL(pedImmigrant[i])		
								IF bFirstImmigrantFleeing = FALSE
									//vNodeSwitch2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,-70,0>>)
									//vNodeSwitch2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,70,0>>)
									//SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,140,FALSE,FALSE)
									KnockPedOffBike()                                                     //Knock ped off bike if he gets rammed by player's car        
								ENDIF
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bBlokestunned = TRUE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[i],WEAPONTYPE_STUNGUN)
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY STUNGUN")
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOSEF],FALSE)	
						CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOSEF")
						IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
							++iTimesTazed
						ENDIF
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOE],FALSE)	
						CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOE")
						IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
							++iTimesTazed
						ENDIF
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),FALSE)	
						CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY PLAYER")
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							++iTimesTazed
						ENDIF
					ENDIF
					CLEAR_PED_LAST_WEAPON_DAMAGE(pedImmigrant[i])
				ENDIF
				IF bImmigrantHandsUp = FALSE	
					iGameTimerPed1KnockedOffUpdate = GET_GAME_TIMER()
					IF iGameTimerPed1KnockedOffUpdate > iGameTimerPed1KnockedOff + 4000
						SET_ENTITY_PROOFS(pedImmigrant[i],FALSE,FALSE,FALSE,FALSE,FALSE)
					ENDIF
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF HAS_PLAYER_THREATENED_PED(pedImmigrant[i])
							IF iTimesTazed = 0	
								++iTimesTazed
							ENDIF
							SetImmigrantHandsUp() 
						ENDIF
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),FALSE)	
							fDistBetweenPlayerAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedImmigrant[i])
							IF 	fDistBetweenPlayerAndImmigrant < 9.0
								IF iTimesTazed > 0	
									SetImmigrantHandsUp() 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
						IF fDistBetweenJosefAndImmigrant < 7.0
							IF iTimesTazed > 0	
								SetImmigrantHandsUp()                            //Hands up
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
						IF fDistBetweenJoeAndImmigrant < 7.0
							IF iTimesTazed > 0	
								SetImmigrantHandsUp()                            //Hands up
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF 	missionStage = MS_UPDATE_CHASE2
			OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2
				IF bSECONDBlokestunned = TRUE
					IF bImmigrantChase2HandsUp = FALSE	
						IF GET_GAME_TIMER() > iChaseStartedTimer + 4000
							SET_ENTITY_PROOFS(pedImmigrantChase2,FALSE,FALSE,FALSE,FALSE,FALSE)
						ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),FALSE)		
								fDistBetweenPlayerAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedImmigrantChase2)
								IF 	fDistBetweenPlayerAndImmigrant < 9.0		
									SetImmigrantChase2HandsUp() 
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
							IF fDistBetweenJoeAndImmigrant < 7.0
								SetImmigrantChase2HandsUp()                            //Hands up	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

ENDPROC

/// PURPOSE: AI for Josef when he's on foot
PROC JosefOnFoot()
	
	FLOAT fAimGoTo
	FLOAT fAimRange
	
	IF 	missionStage = MS_UPDATE_CHASE2
	OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2		
		fAimGoTo = 2.2
		fAimRange = 3.5
	ELSE
		fAimGoTo = 4.5
		fAimRange = 6.0
	ENDIF
	
	IF bBlokestunned = TRUE
		IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
			IF GET_ENTITY_SPEED(sRCLauncherDataLocal.vehID[0]) < 0.1
				IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i]) < 40.0
					iTimerJoeLeftCar = GET_GAME_TIMER()
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK		
						SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE], CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE], CA_DO_DRIVEBYS, FALSE)
						TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],-1)
						TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
						//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF i <> 0	
								//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_5", CONV_PRIORITY_MEDIUM)   //Don't make me use the stun gun again!
								//CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_5", CONV_PRIORITY_VERY_HIGH)
								IF iShoutSurr5 <> 1	
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
								iShoutSurr5 = 1
							ENDIF
						//ENDIF
						bOkForJosefToLeaveCar = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bOkForJosefToLeaveCar = TRUE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
				IF iShoutSurr5 = 1 	
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_5", CONV_PRIORITY_VERY_HIGH)
						iShoutSurr5 = 2 
					ENDIF
				ENDIF
			ENDIF
			IF bImmigrantHandsUp = FALSE
				IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])  < 6.0
				AND CAN_PED_SEE_PED(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])	
					IF NOT IS_PED_BEING_STUNNED(pedImmigrant[i])	
					OR IS_PED_GETTING_UP(pedImmigrant[i])		
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_COMBAT) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK		
							iTimerJoeLeftCar = GET_GAME_TIMER()
							TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],COMBAT_PED_PREVENT_CHANGING_TARGET)
							//TASK_SHOOT_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],-1,FIRING_TYPE_DEFAULT)
						ENDIF
					ELSE
						IF IS_PED_SITTING_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK
								TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
							ENDIF
						ELSE		
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK	
								iTimerJoeLeftCar = GET_GAME_TIMER()
								TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],-1)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_COMBAT) = PERFORMING_TASK
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])  > 9.0
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK
								TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
							TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
						ENDIF
					ENDIF
				ENDIF	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ANY) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ANY) <> WAITING_TO_START_TASK	
					TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_COMBAT) = PERFORMING_TASK	
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOE])
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])  < fAimRange
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i])  < 1.6
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
							//IF NOT IS_PED_FACING_PED(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOE],90)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOE],<<0.0,-4.5,0.0>>),pedImmigrant[i],2.0,FALSE,2.5)
							//ELSE
								//IF NOT IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrant[i],<<4.5,0.0,0.0>>),3,FALSE,TRUE,TRUE,FALSE,FALSE)
								//	TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrant[i],<<4.5,0.0,0.0>>),pedImmigrant[i],1.0,FALSE,2.5)
								//ELSE
								//	TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrant[i],<<-4.5,0.0,0.0>>),pedImmigrant[i],1.0,FALSE,2.5)
								//ENDIF
							//ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK		
							TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],-1)
						ENDIF
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],pedImmigrant[i],PEDMOVEBLENDRATIO_WALK,FALSE,fAimGoTo)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_BLOCKING_DOOR()
	IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
	AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],14),<<1.5,1.5,1.5>>)
		RETURN TRUE
	ENDIF
RETURN FALSE
ENDFUNC

/// PURPOSE: AI for Joe when he's on foot
PROC JoeOnFoot()
			
	FLOAT fJosefMBR				
					
	IF 	missionStage = MS_UPDATE_CHASE2
	OR missionStage = MS_JOSEF_PICKED_UP_IMMIGRANT2		
		
		IF bSECONDBlokestunned = TRUE
			IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				IF bOkForJoeToLeaveCar = FALSE		
					IF GET_ENTITY_SPEED(sRCLauncherDataLocal.vehID[0]) < 0.1
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2) < 40.0
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
							//AND NOT IS_POSITION_OCCUPIED(GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],14),2.5,FALSE,FALSE,TRUE,FALSE,FALSE,pedImmigrantChase2)	
							AND NOT IS_PLAYER_BLOCKING_DOOR()	
								SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_USE_VEHICLE, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_DO_DRIVEBYS, FALSE)
								TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,-1)
								TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
								//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
									//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_3", CONV_PRIORITY_MEDIUM)   //Nenutte me pouzit paralyzer znovu!
									//CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_3", CONV_PRIORITY_VERY_HIGH)
								//ENDIF
								IF iShoutSurr3 <> 1	
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
								iShoutSurr3 = 1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bOkForJoeToLeaveCar = TRUE
			ENDIF
			
			IF bOkForJoeToLeaveCar = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
					IF iShoutSurr3 = 1 	
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_3", CONV_PRIORITY_VERY_HIGH)
							iShoutSurr3 = 2 
						ENDIF
					ENDIF
				ENDIF
				IF bImmigrantChase2HandsUp = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 6.0
					AND CAN_PED_SEE_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)		
						IF NOT IS_PED_BEING_STUNNED(pedImmigrantChase2)	
						OR IS_PED_GETTING_UP(pedImmigrantChase2)	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK		
								TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,COMBAT_PED_PREVENT_CHANGING_TARGET) 
								//TASK_SHOOT_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,-1,FIRING_TYPE_DEFAULT)
							ENDIF
						ELSE
							IF IS_PED_SITTING_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK	
								//AND NOT IS_POSITION_OCCUPIED(GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],14),2.5,FALSE,FALSE,TRUE,FALSE,FALSE,pedImmigrantChase2)	
								AND NOT IS_PLAYER_BLOCKING_DOOR()		
									TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
								ENDIF
							ELSE	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK		
									TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,-1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) = PERFORMING_TASK
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  > 9.0
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
									TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK		
								TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
							ENDIF
						ENDIF
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ANY) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ANY) <> WAITING_TO_START_TASK	
						TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) = PERFORMING_TASK	
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
					ENDIF
					/*
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 6.0
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 1.6	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK		
								//IF NOT IS_PED_FACING_PED(pedImmigrantChase2,sRCLauncherDataLocal.pedID[ic_JOSEF],90)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOSEF],<<0.0,-4.5,0.0>>),pedImmigrantChase2,2.0,FALSE,2.5)
								//ELSE
									//IF NOT IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrantChase2,<<4.5,0.0,0.0>>),3,FALSE,TRUE,TRUE,FALSE,FALSE)
									//	TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrantChase2,<<4.5,0.0,0.0>>),pedImmigrant[i],1.0,FALSE,2.5)
									//ELSE
									//	TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedImmigrantChase2,<<-4.5,0.0,0.0>>),pedImmigrant[i],1.0,FALSE,2.5)
									//ENDIF
								//ENDIF
							ENDIF
						ELSE	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK		
								TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,-1)
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,pedImmigrantChase2,PEDMOVEBLENDRATIO_WALK,FALSE,4.5)
						ENDIF
					ENDIF
					*/
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 3.0//9.0
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 1.4	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOSEF],<<0.0,-4.5,0.0>>),pedImmigrantChase2,2.0,FALSE,2.5)
							ENDIF
						ELSE	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK		
								TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,-1)
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
						AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK		
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)  < 9.0
								fJosefMBR = 1.0
							ELSE
								fJosefMBR = 2.0
							ENDIF
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,pedImmigrantChase2,fJosefMBR,FALSE,2.0) //4.5
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF			
	ELSE	
		IF bBlokestunned = TRUE
			IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])		
				IF GET_ENTITY_SPEED(sRCLauncherDataLocal.vehID[0]) < 0.1
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i]) < 40.0
						IF bOkForJosefToLeaveCar = TRUE
							IF GET_GAME_TIMER() > iTimerJoeLeftCar + 400 //200	
								TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],-1)
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
								//AND NOT IS_POSITION_OCCUPIED(GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],14),2.5,FALSE,FALSE,TRUE,FALSE,FALSE,pedImmigrant[i])	
								AND NOT IS_PLAYER_BLOCKING_DOOR()		
									SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_USE_VEHICLE, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_DO_DRIVEBYS, FALSE)
									TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],-1)
									//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
										//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_1", CONV_PRIORITY_MEDIUM)   //Ziskat do auta fuckermother!
										//CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_1", CONV_PRIORITY_VERY_HIGH)
									//ENDIF	
									IF iShoutSurr1 <> 1	
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
									iShoutSurr1 = 1
									bOkForJoeToLeaveCar = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF bOkForJoeToLeaveCar = TRUE
				IF bImmigrantHandsUp = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
						IF iShoutSurr1 = 1 	
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_1", CONV_PRIORITY_VERY_HIGH)
								iShoutSurr1 = 2 
							ENDIF
						ENDIF
					ENDIF
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  < 9//7.0
					AND CAN_PED_SEE_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])		
						IF NOT IS_PED_BEING_STUNNED(pedImmigrant[i])		
						OR IS_PED_GETTING_UP(pedImmigrant[i])	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) <> WAITING_TO_START_TASK	
								TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],COMBAT_PED_PREVENT_CHANGING_TARGET)
								//TASK_SHOOT_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],-1,FIRING_TYPE_DEFAULT)
							ENDIF
						ELSE
							IF IS_PED_SITTING_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK		
								AND GET_ENTITY_SPEED(sRCLauncherDataLocal.vehID[0])	< 0.1
								//AND NOT IS_POSITION_OCCUPIED(GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],14),2.5,FALSE,FALSE,TRUE,FALSE,FALSE,pedImmigrant[i])		
								AND NOT IS_PLAYER_BLOCKING_DOOR()		
									TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],1200)
								ENDIF
							ELSE	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK	
									TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],-1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) = PERFORMING_TASK
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  > 11//9.0
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
									TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],DEFAULT_TIME_BEFORE_WARP_MINUTE_2)
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK	
								TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],DEFAULT_TIME_BEFORE_WARP_MINUTE_2)
							ENDIF
						ENDIF
					ENDIF	
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ANY) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ANY) <> WAITING_TO_START_TASK	
						TASK_GO_TO_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])
					ENDIF
				ELSE
					
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
						IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_COMBAT) = PERFORMING_TASK	
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
						ENDIF
						/*
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.pedID[ic_JOE])  < 9
						AND GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i]) < 9
						AND NOT IS_PED_BEING_STUNNED(pedImmigrant[i])	
						AND NOT IS_PED_RAGDOLL(pedImmigrant[i])
						AND NOT IS_PED_GETTING_UP(pedImmigrant[i])	
						AND GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])  > 5.0			
						AND	GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])  < 15.0	
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  < 1.6	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOSEF],<<0.0,-4.5,0.0>>),pedImmigrant[i],2.0,FALSE,2.5)
								ENDIF
							ELSE		
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],pedImmigrant[i],1,FALSE,4)
								ENDIF
							ENDIF
						ELSE		
						*/	
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  < 3.5//9.0
							OR IS_PED_IN_VEHICLE(pedImmigrant[0],sRCLauncherDataLocal.vehID[0],TRUE)			
								IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  < 1.4//1.6	
									IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK	
										TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[ic_JOSEF],<<0.0,-4.5,0.0>>),pedImmigrant[i],2.0,FALSE,2.5)
									ENDIF
								ELSE	
									IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
									AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK		
										TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],-1)
									ENDIF
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY) <> WAITING_TO_START_TASK		
									IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i])  < 9.0
										fJosefMBR = 1.0
									ELSE
										fJosefMBR = 2.0
									ENDIF
									TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrant[i],pedImmigrant[i],fJosefMBR,FALSE,2.7) //4.5
								ENDIF
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF			
	ENDIF
	
ENDPROC

/// PURPOSE: AI for Josef during chase 2	
PROC Chase2JosefAi()

	IF IS_PED_UNINJURED(pedImmigrant[1])

		fDistBetweenJosefAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[1])
		
		IF bBlokestunned = FALSE
			IF fDistBetweenJosefAndImmigrant < 9.0                 //josef taze ped if within 10 meters otherwise clear tasks
				IF bJosefCombatTaskGiven = FALSE
					//TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[1])
					TASK_DRIVE_BY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[1],NULL,<<0,0,0>>,9,100)
					IF IS_PED_ON_ANY_BIKE(pedImmigrant[1])
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrant[1],KNOCKOFFVEHICLE_EASY)
					ENDIF
					bJosefCombatTaskGiven = TRUE
				ENDIF
			ELSE
				IF bJosefCombatTaskGiven = TRUE
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOE])
					bJosefCombatTaskGiven = FALSE
				ENDIF
			ENDIF
		ELSE
			IF fDistBetweenJosefAndImmigrant >= 40.0
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOE])
			ENDIF
		ENDIF
		
		IF bBlokestunned = FALSE
			IF fDistBetweenJosefAndImmigrant > 220.0
				IF bGettingAwayMessagePrinted = FALSE	
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
							IF i = 0
								IF GET_GAME_TIMER() > iTimerChase1Started + 12000		
									IF bPrint18 = FALSE	
										//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
										bPrint18 = TRUE	
									ENDIF
								ENDIF
							ELSE
								IF bBlokestunned = FALSE
								AND bSECONDBlokestunned = FALSE
									IF bPrint37 = FALSE		
										//PRINT_NOW("MIN2_37",7000,0)                          //The immigrants are getting away
										bPrint37 = TRUE
									ENDIF
								ELSE
									IF GET_GAME_TIMER() > iTimerChase1Started + 12000		
										IF bPrint18 = FALSE		
											//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
											bPrint18 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//iGameTimer = GET_GAME_TIMER()
					bGettingAwayMessagePrinted = TRUE
				ELSE
					//iGameTimerUpdate = GET_GAME_TIMER()
					//IF iGameTimerUpdate > (iGameTimer + 20000)
						IF fDistBetweenJosefAndImmigrant > 300.0
							IF i = 0
								sFailReason = "MIN2_43"
							ELSE	
								IF bBlokestunned = FALSE
								AND bSECONDBlokestunned = FALSE	
									sFailReason = "MIN2_35"    
								ELSE	
									sFailReason = "MIN2_23"
								ENDIF
							ENDIF
							missionStage = MS_MISSION_FAILING
						ENDIF
					//ENDIF
				ENDIF
			ELSE
				bGettingAwayMessagePrinted = FALSE
			ENDIF
		ENDIF
		
		IF bBlokestunned = FALSE
			//IF CAN_PED_RAGDOLL(pedImmigrant[i])		
				IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[1],WEAPONTYPE_STUNGUN)	
					KnockPedOffBike()                                                     //Knock ped off bike if he gets tazed                  
				ENDIF
			//ENDIF
		ENDIF
		
		IF bBlokestunned = FALSE
			//IF CAN_PED_RAGDOLL(pedImmigrant[i])	
				IF IS_VEHICLE_OK(vehChaseCar[1])	
					IF NOT IS_PED_IN_VEHICLE(pedImmigrant[1],vehChaseCar[1])	
						IF bFirstImmigrantFleeing = FALSE	
							KnockPedOffBike()                                                     //Knock ped off bike if he gets rammed by player's car
	                    ENDIF               
					ENDIF
				ENDIF
			//ENDIF
		ENDIF

		IF bBlokestunned = TRUE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[i],WEAPONTYPE_STUNGUN)
				CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY STUNGUN")
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOSEF],FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOSEF")
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
						++iTimesTazed
					ENDIF
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOE],FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOE")
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
						++iTimesTazed
					ENDIF
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY PLAYER")
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						++iTimesTazed
					ENDIF
				ENDIF
				CLEAR_PED_LAST_WEAPON_DAMAGE(pedImmigrant[i])
			ENDIF
			IF bImmigrantHandsUp = FALSE	
				iGameTimerPed1KnockedOffUpdate = GET_GAME_TIMER()
				IF iGameTimerPed1KnockedOffUpdate > iGameTimerPed1KnockedOff + 4000
					SET_ENTITY_PROOFS(pedImmigrant[1],FALSE,FALSE,FALSE,FALSE,FALSE)
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF HAS_PLAYER_THREATENED_PED(pedImmigrant[i])
						IF iTimesTazed = 0	
							++iTimesTazed
						ENDIF
						SetImmigrantHandsUp() 
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),FALSE)		
						fDistBetweenPlayerAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedImmigrant[1])
						IF 	fDistBetweenPlayerAndImmigrant < 9.0
							IF iTimesTazed > 0
								SetImmigrantHandsUp() 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
					IF fDistBetweenJosefAndImmigrant < 7.0
						IF iTimesTazed > 0
							SetImmigrantHandsUp()                            //Hands up
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: AI for Joe during chase 2
PROC Chase2JoeAi()

	IF IS_PED_UNINJURED(pedImmigrantChase2)

		fDistBetweenJoeAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
		
		IF bSECONDBlokestunned = FALSE
			IF fDistBetweenJoeAndImmigrant < 9.0                 //joe taze ped if within 10 meters otherwise clear tasks
				IF bJoeCombatTaskGiven = FALSE	
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "~~~~~~~~~JOSEF SHOULD DO DRIVEBY~~~~~~~~~")
					#ENDIF
					
					//TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2)
					TASK_DRIVE_BY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,NULL,<<0,0,0>>,10,100)
					IF IS_PED_ON_ANY_BIKE(pedImmigrantChase2)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedImmigrantChase2,KNOCKOFFVEHICLE_EASY)
					ENDIF
					bJoeCombatTaskGiven = TRUE
				ENDIF
			ELSE
				IF bJoeCombatTaskGiven = TRUE
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
					bJoeCombatTaskGiven = FALSE
				ENDIF
			ENDIF
		ELSE
			IF fDistBetweenJoeAndImmigrant >= 40.0
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[ic_JOSEF])
			ENDIF
		ENDIF
		
		IF bSECONDBlokestunned = FALSE
			IF fDistBetweenJoeAndImmigrant > 220.0
				IF bSecondGettingAwayMessagePrinted = FALSE
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
							IF i = 0
								IF GET_GAME_TIMER() > iTimerChase1Started + 12000	
									IF bPrint18 = FALSE	
										//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
										bPrint18 = TRUE
									ENDIF
								ENDIF
							ELSE
								IF bBlokestunned = FALSE
								AND bSECONDBlokestunned = FALSE
									IF bPrint37 = FALSE		
										//PRINT_NOW("MIN2_37",7000,0)                          //The immigrants are getting away
										bPrint37 = TRUE
									ENDIF
								ELSE
									IF bPrint18 = FALSE		
										//PRINT_NOW("MIN2_18",7000,0)                          //The immigrant is getting away
										bPrint18 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//iGameTimerSecondPedGettingAway = GET_GAME_TIMER()
					bSecondGettingAwayMessagePrinted = TRUE
				ELSE
					//iGameTimerSecondPedGettingAwayUpdate = GET_GAME_TIMER()
					//IF iGameTimerSecondPedGettingAwayUpdate > (iGameTimerSecondPedGettingAway + 20000)
						IF fDistBetweenJoeAndImmigrant > 300.0
							IF i = 0
								sFailReason = "MIN2_43"
							ELSE	
								IF bBlokestunned = FALSE
								AND bSECONDBlokestunned = FALSE	
									sFailReason = "MIN2_35"    
								ELSE	
									sFailReason = "MIN2_23"
								ENDIF
							ENDIF
							missionStage = MS_MISSION_FAILING
						ENDIF
					//ENDIF
				ENDIF
			ELSE
				bSecondGettingAwayMessagePrinted = FALSE
			ENDIF
		ENDIF

		IF bSECONDBlokestunned = FALSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrantChase2,WEAPONTYPE_STUNGUN)	
				KnockSECONDPedOffBike()                                                     //Knock ped off bike if he gets tazed                  
			ENDIF
		ENDIF
		
		IF bSECONDBlokestunned = FALSE
			IF IS_PED_UNINJURED(pedImmigrantChase2)	
				IF NOT IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)	
					IF bImmigrantChase2Fleeing = FALSE	
						KnockSECONDPedOffBike()                                                     //Knock ped off bike if he gets rammed by player's car
                    ENDIF               
				ENDIF
			ENDIF
		ENDIF

		IF bSECONDBlokestunned = TRUE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrantChase2,WEAPONTYPE_STUNGUN)
				CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY STUNGUN")
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.pedID[ic_JOSEF],FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOSEF")
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
						++iTimesTazedSECONDPED
					ENDIF
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.pedID[ic_JOE],FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY JOE")
					IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])
						++iTimesTazedSECONDPED
					ENDIF
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),FALSE)	
					CPRINTLN(DEBUG_MISSION, "IMMIGRANT DAMAGED BY PLAYER")
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						++iTimesTazedSECONDPED
					ENDIF
				ENDIF
				CLEAR_PED_LAST_WEAPON_DAMAGE(pedImmigrantChase2)
			ENDIF
			IF bImmigrantChase2HandsUp = FALSE	
				iGameTimerPed2KnockedOffUpdate = GET_GAME_TIMER()
				IF iGameTimerPed2KnockedOffUpdate > iGameTimerPed2KnockedOff + 4000
					SET_ENTITY_PROOFS(pedImmigrantChase2,FALSE,FALSE,FALSE,FALSE,FALSE)
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF HAS_PLAYER_THREATENED_PED(pedImmigrantChase2)
						IF iTimesTazedSECONDPED = 0	
							++iTimesTazedSECONDPED
						ENDIF
						SetImmigrantChase2HandsUp() 
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),FALSE)		
						fDistBetweenPlayerAndImmigrant = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedImmigrantChase2)
						IF 	fDistBetweenPlayerAndImmigrant < 9.0
							IF iTimesTazedSECONDPED > 0
								SetImmigrantChase2HandsUp() 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])
					IF fDistBetweenJoeAndImmigrant < 7.0
						IF iTimesTazedSECONDPED > 0
							SetImmigrantChase2HandsUp()                            //Hands up
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Manage Immigrant while he gets in the vehicle
PROC ImmigrantGetInTruck()

	IF IS_PED_UNINJURED(pedImmigrant[i])
		//SET_PED_MOVE_RATE_OVERRIDE(pedImmigrant[i],1.15)
		IF IS_PED_SITTING_IN_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0])
			AttachPedToCar()
			EXIT
		ENDIF
		IF bBloke2AttachedToCar = FALSE
			fDistBetweenCarAndMigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.vehID[0],pedImmigrant[i])
			IF fDistBetweenCarAndMigrant < 40	
			OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],40)	
				IF iPedKneeling = 0	
					//IF fDistBetweenCarAndMigrant < 2.5
					//IF IS_ENTITY_IN_RANGE_COORDS(pedImmigrant[i],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<1.5,0,0>>),3)	
					IF IS_PED_RAGDOLL(pedImmigrant[i])
					OR IS_PED_GETTING_UP(pedImmigrant[i])
					OR IS_PED_BEING_STUNNED(pedImmigrant[i])	
						iTimerPed1Ragdoll = GET_GAME_TIMER()
					ENDIF
					IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],4.5)
					OR GET_GAME_TIMER() > iTimerPed1Ragdoll + 2500
						SET_PED_MOVEMENT_CLIPSET(pedImmigrant[i],"MOVE_M@BAIL_BOND_TAZERED")
						IF IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","arrest_walk")
							STOP_ANIM_TASK(pedImmigrant[i],"rcmminute2","arrest_walk",-2)
							TASK_PLAY_ANIM(pedImmigrant[i],"rcmminute2","handsup_exit",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY,0.3)
							//KILL_ANY_CONVERSATION()
						ENDIF
						
						IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],3)
						AND IS_PED_IN_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],TRUE)		
							KILL_ANY_CONVERSATION()
						ENDIF
						
						IF bImmigrantGettingIntoCar = FALSE
							/*
							CLEAR_PED_TASKS(pedImmigrant[i])
							OPEN_SEQUENCE_TASK(seqImmigrantBeCarried)
								TASK_SET_DECISION_MAKER(NULL,DECISION_MAKER_EMPTY)
								TASK_ENTER_VEHICLE(NULL,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,PEDMOVEBLENDRATIO_WALK)
							CLOSE_SEQUENCE_TASK(seqImmigrantBeCarried)
							TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqImmigrantBeCarried)
							*/
							IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
								IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
									TASK_ENTER_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
								ENDIF
							ENDIF
							bImmigrantGettingIntoCar = TRUE
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
								IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
									TASK_ENTER_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SET_PED_MOVEMENT_CLIPSET(pedImmigrant[i],"MOVE_M@BAIL_BOND_NOT_TAZERED")
						IF IS_PED_GETTING_UP(pedImmigrant[i])	
							IF bPed1GettingUp = FALSE
								IF IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","arrest_walk")
									STOP_ANIM_TASK(pedImmigrant[i],"rcmminute2","arrest_walk",NORMAL_BLEND_OUT)
								ENDIF
								iTimerPed1GettingUp = GET_GAME_TIMER()
								bPed1GettingUp = TRUE
							ELSE
								IF GET_GAME_TIMER() > iTimerPed1GettingUp + 3100//1400
									IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
									//AND NOT IS_PED_BEING_STUNNED(pedImmigrant[i],WEAPONTYPE_STUNGUN)
										IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","arrest_walk")	
											IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())	
												TASK_PLAY_ANIM(pedImmigrant[i], "rcmminute2","arrest_walk", 1.4, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
							AND NOT IS_PED_BEING_STUNNED(pedImmigrant[i],WEAPONTYPE_STUNGUN)	
								IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","arrest_walk")	
									IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
										TASK_PLAY_ANIM(pedImmigrant[i], "rcmminute2","arrest_walk", 1.4, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
									ENDIF
								ENDIF
							ENDIF
							bPed1GettingUp = FALSE
						ENDIF
					ENDIF
					IF IS_PED_SITTING_IN_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0])
						iPed1getIn = 1000
					ENDIF
					IF IS_PED_IN_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],TRUE)	
						++iPed1getIn
						IF i <> 0
						OR iPed1getIn > 80
							IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],TRUE)	
								IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
									IF i = 0
										IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],15.0)	
											TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK)
										ELSE
											TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_RUN)
										ENDIF
									ELSE	
										TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF iPed1getIn > 20
						AND	i = 0
						AND NOT IS_PED_INJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
							IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],TRUE)	
								IF IS_IT_OK_FOR_JOSEF_TO_GET_IN()		
									IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
									AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK			
										IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],10.0)	
											TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1)
										ELSE
											TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
										ENDIF
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
									AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
										TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.2,0>>),1,DEFAULT_TIME_BEFORE_WARP_MINUTE_2,0.5,ENAV_DEFAULT,GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])-90.0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF iPedKneeling = 1
					IF fDistBetweenCarAndMigrant < 20
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],20)	
						IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","kneeling_arrest_get_up")
							TASK_PLAY_ANIM(pedImmigrant[i], "rcmminute2","kneeling_arrest_get_up", FAST_BLEND_IN, -2, -1)
							iPedKneeling = 2
						ENDIF
					ENDIF
				ELIF iPedKneeling = 2
					IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","kneeling_arrest_get_up")
					//OR GET_ENTITY_ANIM_CURRENT_TIME(pedImmigrant[i],"rcmminute2","kneeling_arrest_get_up") > 0.9	
						TASK_PLAY_ANIM(pedImmigrant[i], "rcmminute2","arrest_walk", 2, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
						TASK_ENTER_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1.2)
						iPedKneeling = 0
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
				AND NOT IS_PED_BEING_STUNNED(pedImmigrant[i],WEAPONTYPE_STUNGUN)		
					IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrant[i],"rcmminute2","kneeling_arrest_idle")
						CLEAR_PED_TASKS(pedImmigrant[i])
						TASK_PLAY_ANIM(pedImmigrant[i], "rcmminute2","kneeling_arrest_idle", 2, FAST_BLEND_OUT, -1, AF_LOOPING)
						IF iPedKneeling <> 1
							KILL_ANY_CONVERSATION()
						ENDIF
						iPedKneeling = 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
ENDPROC

/// PURPOSE: Manage Immigrant while he gets in the vehicle
PROC ImmigrantChase2GetInTruck()

	IF IS_PED_UNINJURED(pedImmigrantChase2)
		//SET_PED_MOVE_RATE_OVERRIDE(pedImmigrantChase2,1.15)
		IF IS_PED_SITTING_IN_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0])
			AttachSECONDPedToCar()
			EXIT
		ENDIF
		IF bpedImmigrantChase2Attached = FALSE	
			fDistBetweenCarAndMigrant = GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.vehID[0],pedImmigrantChase2)
			IF fDistBetweenCarAndMigrant < 40	
			OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,40)	
				IF iPed2Kneeling = 0		
					//IF fDistBetweenCarAndMigrant < 2.5
					//IF IS_ENTITY_IN_RANGE_COORDS(pedImmigrantChase2,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<1.5,0,0>>),3)	
					IF IS_PED_RAGDOLL(pedImmigrantChase2)
					OR IS_PED_GETTING_UP(pedImmigrantChase2)
					OR IS_PED_BEING_STUNNED(pedImmigrantChase2)	
						iTimerPed2Ragdoll = GET_GAME_TIMER()
					ENDIF
					IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],4.5)	
					OR GET_GAME_TIMER() > iTimerPed2Ragdoll + 2500	
						SET_PED_MOVEMENT_CLIPSET(pedImmigrantChase2,"MOVE_M@BAIL_BOND_TAZERED")
						IF IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","arrest_walk")
							STOP_ANIM_TASK(pedImmigrantChase2,"rcmminute2","arrest_walk",-2)
							TASK_PLAY_ANIM(pedImmigrantChase2,"rcmminute2","handsup_exit",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY,0.3)
							//KILL_ANY_CONVERSATION()
						ENDIF
						
						IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],3)
						AND IS_PED_IN_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],TRUE)	
							KILL_ANY_CONVERSATION()
						ENDIF
						
						IF bImmigrantChase2GettingIntoCar = FALSE
							/*
							CLEAR_PED_TASKS(pedImmigrantChase2)
							OPEN_SEQUENCE_TASK(seqImmigrantBeCarried)
								TASK_SET_DECISION_MAKER(NULL,DECISION_MAKER_EMPTY)
								TASK_ENTER_VEHICLE(NULL,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,PEDMOVEBLENDRATIO_WALK)
							CLOSE_SEQUENCE_TASK(seqImmigrantBeCarried)
							TASK_PERFORM_SEQUENCE(pedImmigrantChase2,seqImmigrantBeCarried)
							*/
							IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
								IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
									TASK_ENTER_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
								ENDIF
							ENDIF
							bImmigrantChase2GettingIntoCar = TRUE
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
								IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
									TASK_ENTER_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_GETTING_UP(pedImmigrantChase2)	
							IF bPed2GettingUp = FALSE
								IF IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","arrest_walk")
									STOP_ANIM_TASK(pedImmigrantChase2,"rcmminute2","arrest_walk",NORMAL_BLEND_OUT)
								ENDIF
								iTimerPed2GettingUp = GET_GAME_TIMER()
								bPed2GettingUp = TRUE
							ELSE
								IF GET_GAME_TIMER() > iTimerPed2GettingUp + 3100//1400
									IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
									//AND NOT IS_PED_BEING_STUNNED(pedImmigrantChase2,WEAPONTYPE_STUNGUN)	
										IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","arrest_walk")	
											IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
												TASK_PLAY_ANIM(pedImmigrantChase2, "rcmminute2","arrest_walk", 1.4, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
							AND NOT IS_PED_BEING_STUNNED(pedImmigrantChase2,WEAPONTYPE_STUNGUN)		
								IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","arrest_walk")	
									IF NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
										TASK_PLAY_ANIM(pedImmigrantChase2, "rcmminute2","arrest_walk", 1.4, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
									ENDIF
								ENDIF
							ENDIF
							bPed2GettingUp = FALSE
						ENDIF
					ENDIF
					IF IS_PED_IN_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0])	
						IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
							IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
							AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
								TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1.2)
							ENDIF
						ENDIF
					ENDIF	
				ELIF iPed2Kneeling = 1
					IF fDistBetweenCarAndMigrant < 20
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,20)		
						IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","kneeling_arrest_get_up")
							TASK_PLAY_ANIM(pedImmigrantChase2, "rcmminute2","kneeling_arrest_get_up", FAST_BLEND_IN, -2, -1)
							iPed2Kneeling = 2
						ENDIF
					ENDIF
				ELIF iPed2Kneeling = 2
					IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","kneeling_arrest_get_up")
					//OR GET_ENTITY_ANIM_CURRENT_TIME(pedImmigrantChase2,"rcmminute2","kneeling_arrest_get_up") > 0.9
						TASK_PLAY_ANIM(pedImmigrantChase2, "rcmminute2","arrest_walk", 2, NORMAL_BLEND_OUT, -1, AF_SECONDARY| AF_UPPERBODY | AF_LOOPING | AF_TAG_SYNC_CONTINUOUS)
						TASK_ENTER_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_RIGHT,1.4)
						iPed2Kneeling = 0
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
				AND NOT IS_PED_BEING_STUNNED(pedImmigrantChase2,WEAPONTYPE_STUNGUN)		
					IF NOT IS_ENTITY_PLAYING_ANIM(pedImmigrantChase2,"rcmminute2","kneeling_arrest_idle")
						CLEAR_PED_TASKS(pedImmigrantChase2)
						TASK_PLAY_ANIM(pedImmigrantChase2, "rcmminute2","kneeling_arrest_idle", 2, FAST_BLEND_OUT, -1, AF_LOOPING)
						IF iPed2Kneeling <> 1
							KILL_ANY_CONVERSATION()
						ENDIF
						iPed2Kneeling = 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Josef Throws Manuel's papers out the window
PROC JosefThrowPapers()
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
		IF iSeqJosefThrowPapers = 0	
			IF NOT DOES_ENTITY_EXIST(ObjPapers)
				ObjPapers = CREATE_OBJECT_NO_OFFSET(Prop_Passport_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.3,0.7>>))
				ATTACH_ENTITY_TO_ENTITY(ObjPapers, sRCLauncherDataLocal.pedID[ic_JOSEF], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[ic_JOSEF], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				OPEN_SEQUENCE_TASK(seqJosefThrowPapers)
					TASK_PLAY_ANIM(NULL,"rcmminute2","intro_90_l",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_UPPERBODY)
					TASK_PLAY_ANIM(NULL,"rcmminute2","outro_90_l",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_UPPERBODY)
				CLOSE_SEQUENCE_TASK(seqJosefThrowPapers)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[ic_JOSEF],seqJosefThrowPapers)
				iSeqJosefThrowPapers = 1
			ENDIF
		ELIF iSeqJosefThrowPapers = 1
			IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
				IF GET_SEQUENCE_PROGRESS(sRCLauncherDataLocal.pedID[ic_JOSEF]) = 1
					DETACH_ENTITY(ObjPapers)
					iSeqJosefThrowPapers = 2
				ENDIF
			ENDIF
		ENDIF			
	ENDIF

ENDPROC

FUNC BOOL ARE_ANY_IN_CAR_PRINTS_BEING_DISPLAYED()

	IF IS_THIS_PRINT_BEING_DISPLAYED("MIN2_COPS")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_47")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_48")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_31")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_01")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_49")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_06")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_08")
	OR IS_THIS_PRINT_BEING_DISPLAYED("MIN2_33")
		RETURN TRUE
	ENDIF
	
RETURN FALSE
ENDFUNC

/// PURPOSE: Manage conversations
PROC Convo()
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
	AND DOES_BLIP_EXIST(blipPlayerCar)
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
		IF NOT IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[ic_JOE],PLAYER_PED_ID())
			TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],PLAYER_PED_ID(),-1)
		ENDIF
		IF NOT IS_PED_HEADTRACKING_PED(sRCLauncherDataLocal.pedID[ic_JOSEF],PLAYER_PED_ID())
			TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],PLAYER_PED_ID(),-1)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipLocate[i])	
		IF NOT bDialogueOkLetsGo
			IF iConvoGetBackInCar <> 0      	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_06")
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR3", CONV_PRIORITY_MEDIUM)    //So unprofessional!
							bDialogueOkLetsGo = TRUE
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR3", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							bDialogueOkLetsGo = TRUE
						ENDIF
					ENDIF
				ELSE
					bDialogueOkLetsGo = TRUE
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
	AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		AND DOES_BLIP_EXIST(blipLocate[i])		
			IF iConvo >= 4	
				IF iDialogueWrongWay = 0
					vStorePlayerPosForWrongWayDialogue = GET_ENTITY_COORDS(PLAYER_PED_ID())
					iDialogueWrongWay = 1
				ELSE
					IF iDialogueWrongWay < 4 	   //I said the cement factory, where are you going?
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< 267.2497, 2852.1667, 42.6129 >>) > 
							GET_DISTANCE_BETWEEN_COORDS(vStorePlayerPosForWrongWayDialogue,<< 267.2497, 2852.1667, 42.6129 >>) + 200.0
							IF NOT ARE_ANY_IN_CAR_PRINTS_BEING_DISPLAYED()	
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_WRONG", CONV_PRIORITY_MEDIUM)
									++iDialogueWrongWay
									vStorePlayerPosForWrongWayDialogue = GET_ENTITY_COORDS(PLAYER_PED_ID())
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_WRONG", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
									++iDialogueWrongWay
									vStorePlayerPosForWrongWayDialogue = GET_ENTITY_COORDS(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_GAME_TIMER()	> iTimerDialogueStopping + 9000
					IF iDialogueStopping < 3	
						IF GET_ENTITY_SPEED(sRCLauncherDataLocal.vehID[0]) < 0.1
							IF NOT ARE_ANY_IN_CAR_PRINTS_BEING_DISPLAYED()	
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_STOP", CONV_PRIORITY_MEDIUM)
									++iDialogueStopping
									iTimerDialogueStopping = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_STOP", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
									++iDialogueStopping
									iTimerDialogueStopping = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF GET_GAME_TIMER()	> iTimerDialogueCrashed + 15000
				IF iDialogueCrashed < 6	
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sRCLauncherDataLocal.vehID[0])
						/*
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CRASH", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							++iDialogueCrashed
							iTimerDialogueCrashed = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
						ENDIF
						*/
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
			iTimerDialogueStopping = GET_GAME_TIMER() + 5000
		ENDIF
	ENDIF
	
	IF missionStage = MS_IMMIGRANT1_APPREHENDED
		IF bImmigrantHandsUp = TRUE
			IF bConvoImmigrantManuelGiveUp = FALSE                    //I'm fucking legal man!
				IF IS_PED_UNINJURED(pedImmigrant[0])	
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[0],50)
					AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[0],20)	
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CC1", CONV_PRIORITY_MEDIUM)
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
							bDisableAbortConvo = TRUE
							bConvoImmigrantManuelGiveUp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[0],PLAYER_PED_ID())		
				AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[0],WEAPONTYPE_STUNGUN)		
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE],30)	
						IF iCrueltyLines < 5
							IF GET_GAME_TIMER()	> iTimerCrueltyLines + 7000
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_STUN",CONV_PRIORITY_MEDIUM)  //Yeah! Take that bitch!
										++iCrueltyLines
										iTimerCrueltyLines = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedImmigrant[0])
					CLEAR_ENTITY_LAST_WEAPON_DAMAGE(pedImmigrant[0])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID()) < 30.0
			IF DOES_BLIP_EXIST(blipPlayerCar)
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_19")	
				HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
				IF GET_GAME_TIMER() > iTimerPlayerNotInCar + 7000						
					IF iConvoGetBackInCar = 0                                 //Hey, you getting back in the car or what?
						IF iConvo >= 4		
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF iConvoTrevorDamagingCar = 0	
									IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
									AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
									AND missionStage < MS_UPDATE_CHASE2	
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR","MIN2_CAR_2", CONV_PRIORITY_MEDIUM)
											iConvoGetBackInCar = 1
											iTimerPlayerNotInCar = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF iConvoGetBackInCar = 1                                //Come on man! Uncle Sam needs you!	
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	     
							IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
							AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])		
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR","MIN2_CAR_3", CONV_PRIORITY_MEDIUM)
									iConvoGetBackInCar = 2
									iTimerPlayerNotInCar = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF iConvoGetBackInCar < 5
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	     
								IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
								AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])		
									IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR2", CONV_PRIORITY_MEDIUM)
										bDialogueOkLetsGo = FALSE
										++iConvoGetBackInCar
										iTimerPlayerNotInCar = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF missionStage = MS_UPDATE_CHASE2
			IF bImmigrantChase2HandsUp = TRUE
				IF bConvoImmigrant2GiveUp = FALSE                    //What? 
					IF IS_PED_UNINJURED(pedImmigrantChase2)	
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,50)	
						AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],pedImmigrantChase2,20)		
							ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE")
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CC2", CONV_PRIORITY_MEDIUM)
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
								bDisableAbortConvoPed2 = TRUE
								bConvoImmigrant2GiveUp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrantChase2,PLAYER_PED_ID())		
					AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrantChase2,WEAPONTYPE_STUNGUN)		
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE],30)	
							IF iCrueltyLines < 5
								IF GET_GAME_TIMER()	> iTimerCrueltyLines + 7000
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_STUN",CONV_PRIORITY_MEDIUM)  //Yeah! Take that bitch!
											++iCrueltyLines
											iTimerCrueltyLines = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedImmigrantChase2)
						CLEAR_ENTITY_LAST_WEAPON_DAMAGE(pedImmigrantChase2)
					ENDIF
				ENDIF
			ENDIF
			IF bImmigrantHandsUp = TRUE
				IF bConvoImmigrant1GiveUp = FALSE                    //I'm fucking legal man!
					IF IS_PED_UNINJURED(pedImmigrant[i])	
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],50)	
						AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],pedImmigrant[i],20)			
							ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedImmigrant[i], "IMMIGRANTMALE2")
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CC3", CONV_PRIORITY_MEDIUM)
								SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
								bDisableAbortConvo = TRUE
								bConvoImmigrant1GiveUp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedImmigrant[i],PLAYER_PED_ID())		
					AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedImmigrant[i],WEAPONTYPE_STUNGUN)		
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE],30)	
							IF iCrueltyLines < 5
								IF GET_GAME_TIMER()	> iTimerCrueltyLines + 7000
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_STUN",CONV_PRIORITY_MEDIUM)  //Yeah! Take that bitch!
											++iCrueltyLines
											iTimerCrueltyLines = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedImmigrant[i])
						CLEAR_ENTITY_LAST_WEAPON_DAMAGE(pedImmigrant[i])
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(pedImmigrantChase2)
				IF DOES_ENTITY_EXIST(pedImmigrant[1])
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(pedImmigrantChase2)
					OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(pedImmigrant[1])
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID()) < 30.0
								IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
								AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
									IF GET_GAME_TIMER() > iTimerPlayerNotInCar + 9000						
										IF iConvoGetBackInCar = 0                                 //Hey, you getting back in the car or what?
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR","MIN2_CAR_1", CONV_PRIORITY_MEDIUM)
													iConvoGetBackInCar = 1
													iTimerPlayerNotInCar = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ELIF iConvoGetBackInCar = 1                                //Come on man! Uncle Sam needs you!		
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	     
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_CAR","MIN2_CAR_2", CONV_PRIORITY_MEDIUM)
													iConvoGetBackInCar = 2
													iTimerPlayerNotInCar = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
						
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_01")	
			IF missionStage = MS_SETUP_CHASE1	
			OR missionStage = MS_UPDATE_CHASE1		
				iConvoTimerUpdate = GET_GAME_TIMER()
				IF iConvoTimerUpdate > (iConvoTimer + 7000)
					IF iConvo < 1
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN)
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ5b", CONV_PRIORITY_MEDIUM)
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,TRUE)
									iConvo = 1
								ENDIF
							ELSE       //Here, use this stun gun.
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ5", CONV_PRIORITY_MEDIUM)
									GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,1,TRUE)
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,TRUE)
									iConvo = 1
								ENDIF
							ENDIF
						ENDIF                                                                    
					ENDIF
				ENDIF
			ENDIF
			IF missionStage = MS_UPDATE_CHASE1	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					IF bConvoSpottedImmigrant1 = FALSE
						bConvoSpottedImmigrant1 = TRUE
					ENDIF
					IF iConvo < 2
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	   //These illegal aliens stealing the jobs of hard working Americans like Josef here...
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ1", CONV_PRIORITY_VERY_LOW)
									iConvo = 2
								ENDIF
							ENDIF
						ENDIF	
					ELSE
						IF NOT bPatrolVehDialogue	
						//AND NOT bGetCloseMessagePrinted		
							IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
							AND IS_PED_UNINJURED(pedImmigrant[0])	
							AND NOT IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.vehID[0],pedImmigrant[0],50)	
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	   //How did you get your hands on this patrol car?
							AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	   
							AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	  
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_48")	
								IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ4", CONV_PRIORITY_VERY_LOW)
									bPatrolVehDialogue = TRUE
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF missionStage = MS_SETUP_CHASE2	
			IF DOES_BLIP_EXIST(blipLocate[i])
				iConvoTimerUpdate = GET_GAME_TIMER()
				IF iConvoTimerUpdate > (iConvoTimer + 100)
					IF iConvo < 3
						IF bDebugSkippedToCementFactory = FALSE                                     //The next targets work at the cement factory.
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ6", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
								iConvo = 3
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF missionStage = MS_SETUP_CHASE2	
			IF iConvo = 3
				IF bEveryoneInCar = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
								IF bDebugSkippedToCementFactory = FALSE                         //Who the fuck are you guys?
									IF IS_PED_UNINJURED(pedImmigrant[0])
										IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
											TASK_LOOK_AT_ENTITY(pedImmigrant[0],sRCLauncherDataLocal.pedID[ic_JOE],-1)
										ENDIF
									ENDIF
									IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_IMM1", CONV_PRIORITY_VERY_LOW)
										iConvo = 4
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvo = 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
					TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					IF ARE_STRINGS_EQUAL(label,"MIN2_IMM1_7")				
					AND GET_SYNCHRONIZED_SCENE_PHASE(ssPassport) < 0.54
						KILL_FACE_TO_FACE_CONVERSATION()
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(ssPassport) >= 0.54
							IF NOT bButterFingers	
								CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, "MIN2AU", "MIN2_IMM1","MIN2_IMM1_9", CONV_PRIORITY_MEDIUM)    //Oops! Butterfingers!
								bButterFingers = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>) < 70
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ELIF iConvo = 5
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>) < 70
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF	

		IF missionStage = MS_SETUP_CHASE3	
			IF DOES_BLIP_EXIST(blipLocate[i])
				iConvoTimerUpdate = GET_GAME_TIMER()
				IF iConvoTimerUpdate > (iConvoTimer + 7000)
					IF iConvo <= 5
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ7", CONV_PRIORITY_VERY_LOW)
							iConvo = 5
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF missionStage = MS_UPDATE_CHASE3	
			IF GET_GAME_TIMER() > iChaseStartedTimer + 7000
				IF iConvo < 6
					IF bEveryoneInCar = TRUE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ2", CONV_PRIORITY_VERY_LOW)
								iConvo = 6
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF missionStage = MS_DRIVE_TO_END	
			IF bDrivingToMotel = TRUE
				iConvoTimerUpdate = GET_GAME_TIMER()
				IF iConvoTimerUpdate > (iConvoTimer + 7000)
					IF iConvo < 7
						iConvo = 7
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF missionStage = MS_UPDATE_CHASE1
		IF IS_PED_IN_VEHICLE(pedImmigrant[0],vehChaseCar[0])
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedImmigrant[0]) < 30
				IF iShoutLines = 0	
					IF NOT IS_ANY_SPEECH_PLAYING(pedImmigrant[0])
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedImmigrant[0], "MIN2_ACAA_01", "MANUEL", SPEECH_PARAMS_STANDARD)
					ELSE
						iShoutLines = 1
						iTimerShoutLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
					ENDIF
				ELIF iShoutLines = 1	
					IF GET_GAME_TIMER()	> iTimerShoutLines + 4000
						IF NOT IS_ANY_SPEECH_PLAYING(pedImmigrant[0])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedImmigrant[0], "MIN2_ACAA_02", "MANUEL", SPEECH_PARAMS_STANDARD)
						ELSE
							iShoutLines = 2
							iTimerShoutLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000,4000)
						ENDIF
					ENDIF
				ELIF iShoutLines = 2	
					IF GET_GAME_TIMER()	> iTimerShoutLines + 4000	
						IF NOT IS_ANY_SPEECH_PLAYING(pedImmigrant[0])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedImmigrant[0], "MIN2_ACAA_03", "MANUEL", SPEECH_PARAMS_STANDARD)
						ELSE
							iShoutLines = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF

ENDPROC

/// PURPOSE: Opens the gates at the cement factory
PROC OpenGates()

	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<263.669403, 2854.145752, 43.939796>>,15.0,prop_facgate_03_l)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_facgate_03_l,<<263.669403, 2854.145752, 43.939796>>,TRUE,0.85)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<268.126953, 2845.054443, 44.089664 >>,15.0,prop_facgate_03_ld_l)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_facgate_03_ld_l,<<268.126953, 2845.054443, 44.089664 >>,TRUE,-0.85)                             
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<346.19, 2859.69, 43.63>>,15.0,prop_fnclink_06gate3)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_fnclink_06gate3,<<346.19, 2859.69, 43.63>>,TRUE,1.2)                             
	ENDIF
	
ENDPROC

/// PURPOSE: Player shot the car up so Joe drives off - mission fail
PROC FailDriveOff()

	SEQUENCE_INDEX seqMoveOverDriveOff
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[0],VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
				OPEN_SEQUENCE_TASK(seqMoveOverDriveOff)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL,sRCLauncherDataLocal.vehID[0])
					TASK_VEHICLE_DRIVE_WANDER(NULL,sRCLauncherDataLocal.vehID[0],100.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
				CLOSE_SEQUENCE_TASK(seqMoveOverDriveOff)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[ic_JOE],seqMoveOverDriveOff)
			ELSE
				OPEN_SEQUENCE_TASK(seqMoveOverDriveOff)
					TASK_VEHICLE_DRIVE_WANDER(NULL,sRCLauncherDataLocal.vehID[0],100.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
				CLOSE_SEQUENCE_TASK(seqMoveOverDriveOff)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[ic_JOE],seqMoveOverDriveOff)
			ENDIF
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Check all the fail conditions
PROC CheckForFail()

	IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
	AND NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
		sFailReason = "MIN2_45" // Josef and Joe died
		missionStage = MS_MISSION_FAILING
		EXIT
	ENDIF

	IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
		sFailReason = "MIN2_16" // Josef died
		missionStage = MS_MISSION_FAILING
		EXIT
	ELSE
		SET_PED_INCREASED_AVOIDANCE_RADIUS(sRCLauncherDataLocal.pedID[ic_JOSEF])
	ENDIF
	
	IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
		sFailReason = "MIN2_25" // Joe died
		missionStage = MS_MISSION_FAILING
		EXIT
	ELSE
		SET_PED_INCREASED_AVOIDANCE_RADIUS(sRCLauncherDataLocal.pedID[ic_JOE])
	ENDIF

	IF DOES_ENTITY_EXIST(pedImmigrant[0])
		IF NOT IS_PED_UNINJURED(pedImmigrant[0])	
			sFailReason = "MIN2_44" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ELSE
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedImmigrant[0])
			IF NOT IS_ENTITY_ATTACHED(pedImmigrant[0])
			AND NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[0])
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			ENDIF
		ENDIF
	ENDIF
			
	IF DOES_ENTITY_EXIST(pedImmigrant[1])
		IF NOT IS_PED_UNINJURED(pedImmigrant[1])	
			sFailReason = "MIN2_15" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ELSE
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedImmigrant[1])  
			IF NOT IS_ENTITY_ATTACHED(pedImmigrant[1])
			AND NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[1])
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			ENDIF
		ENDIF
	ENDIF	
	
	IF DOES_ENTITY_EXIST(pedImmigrantChase2)
		IF NOT IS_PED_UNINJURED(pedImmigrantChase2)
			sFailReason = "MIN2_15" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ELSE
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedImmigrantChase2)
			IF NOT IS_ENTITY_ATTACHED(pedImmigrantChase2)
			AND NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			ENDIF
		ENDIF
	ENDIF
	/*	
	IF bBloke1AttachedToCar = TRUE
		IF NOT IS_PED_UNINJURED(pedImmigrant[0])
			sFailReason = "MIN2_15" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF
	
	IF bBloke2AttachedToCar = TRUE
		IF NOT IS_PED_UNINJURED(pedImmigrant[1])
			sFailReason = "MIN2_15" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF
	
	IF bpedImmigrantChase2Attached = TRUE
		IF NOT IS_PED_UNINJURED(pedImmigrantChase2)
			sFailReason = "MIN2_15" // Immigrant died
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF
	*/
	IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE]) > 200.0
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOSEF]) > 200.0
			sFailReason = "MIN2_36" // Left Joe / Josef behind
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF
	
	IF missionStage <>	MS_UPDATE_CHASE2
	AND missionStage <>	MS_DRIVE_TO_END		
	AND missionStage <>	MS_MISSION_PASSING		
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOSEF]) > 210.0
			sFailReason = "MIN2_40" // Left Joe / Josef behind
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE]) > 210.0
			sFailReason = "MIN2_41" // Left Joe / Josef behind
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF
	
	IF missionStage = MS_UPDATE_CHASE2
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOSEF]) > 250.0
		AND iPed2Kneeling = 0	
			sFailReason = "MIN2_40" // Left Joe / Josef behind
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[ic_JOE]) > 250.0
		AND iPedKneeling = 0	
			sFailReason = "MIN2_41" // Left Joe / Josef behind
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
	ENDIF

	IF NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		sFailReason = "MIN2_17" // Car wrecked
		missionStage = MS_MISSION_FAILING
		EXIT
	ELSE
		IF IS_VEHICLE_STUCK_TIMER_UP(sRCLauncherDataLocal.vehID[0],VEH_STUCK_ON_ROOF,12000)
			CPRINTLN(DEBUG_MISSION, "STUCK")
			sFailReason = "MIN2_17" // Car wrecked
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF	
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])			
			IF IS_VEHICLE_STUCK_TIMER_UP(sRCLauncherDataLocal.vehID[0],VEH_STUCK_ON_SIDE,20000)
			OR IS_VEHICLE_STUCK_TIMER_UP(sRCLauncherDataLocal.vehID[0],VEH_STUCK_HUNG_UP,30000)
			OR IS_VEHICLE_STUCK_TIMER_UP(sRCLauncherDataLocal.vehID[0],VEH_STUCK_JAMMED,30000)
				CPRINTLN(DEBUG_MISSION, "STUCK")
				sFailReason = "MIN2_17" // Car wrecked
				missionStage = MS_MISSION_FAILING
				EXIT
			ENDIF
		ENDIF	
		IF NOT IS_VEHICLE_DRIVEABLE(sRCLauncherDataLocal.vehID[0])
			CPRINTLN(DEBUG_MISSION, "NOT DRIVEABLE")
			sFailReason = "MIN2_17" // Car wrecked
			missionStage = MS_MISSION_FAILING
			EXIT
		ENDIF
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
			IF iConvoTrevorDamagingCar = 0
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID())
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()                    //Hey! I just got that serviced!
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SHCAR","MIN2_SHCAR_1", CONV_PRIORITY_VERY_HIGH)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
							iConvoTrevorDamagingCar = 1
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvoTrevorDamagingCar = 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
					iConvoTrevorDamagingCar = 2
				ENDIF
			ELIF iConvoTrevorDamagingCar = 2
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID())	  //Leave my car alone you fucking psycho tweaker!
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SHCAR","MIN2_SHCAR_2", CONV_PRIORITY_VERY_HIGH)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
							iConvoTrevorDamagingCar = 3
						ENDIF
					ENDIF
				ENDIF
			ELIF iConvoTrevorDamagingCar = 3
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
					iConvoTrevorDamagingCar = 4
				ENDIF
			ELIF iConvoTrevorDamagingCar = 4
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID())
						//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SHCAR","MIN2_SHCAR_3", CONV_PRIORITY_VERY_HIGH)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
							iConvoTrevorDamagingCar = 5
						//ENDIF
					ENDIF
				ENDIF
			ELIF iConvoTrevorDamagingCar = 5
				/*
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
					KILL_ANY_CONVERSATION()
					iConvoTrevorDamagingCar = 6
					sFailReason = "MIN2_46" // Cops Alerted
					FailDriveOff()
					missionStage = MS_MISSION_FAILING
					EXIT
				ENDIF
				*/
			ENDIF
		ELSE
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				OR NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
				OR NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(sRCLauncherDataLocal.vehID[0],TRUE)	
					IF IS_VEHICLE_OK(vehChaseCar[0])
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehChaseCar[0],TRUE)	
					ENDIF
					IF IS_VEHICLE_OK(vehChaseCar[1])
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehChaseCar[1],TRUE)	
					ENDIF
					IF IS_VEHICLE_OK(vehChaseCarChase2)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehChaseCarChase2,TRUE)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: All immigrants captured, Joe drives off - mission passed
PROC PassDriveOff()

	SAFE_REMOVE_BLIP(blipLocate[i])
	SAFE_REMOVE_BLIP(blipPlayerCar)
	
	INT iBrakeTime
	
	SEQUENCE_INDEX seqMoveOverDriveOff

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, sRCLauncherDataLocal.pedID[ic_JOSEF], "JOSEF")
			IF bThanks = FALSE	
				CREATE_CONVERSATION(s_conversation_peds,"MIN2AU","MIN2_JJ8",CONV_PRIORITY_HIGH)   //Thanks for the help man, you're a true patriot.
			ENDIF
			CREDIT_BANK_ACCOUNT(CHAR_TREVOR, BAAC_UNLOGGED_SMALL_ACTION, 500)
			REMOVE_ALL_PED_WEAPONS(sRCLauncherDataLocal.pedID[ic_JOE])
			REMOVE_ALL_PED_WEAPONS(sRCLauncherDataLocal.pedID[ic_JOSEF])
		ENDIF
	ENDIF
	
	iTimerPassDriveOff = GET_GAME_TIMER()

	WHILE iJosefCutSeq < 2
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_AAW")
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
				IF iJosefCutSeq = 0
					IF GET_GAME_TIMER() > iTimerPassDriveOff + iBrakeTime
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_LOWEST)
						iJosefCutSeq = 1	
					ENDIF
				ENDIF
				IF iJosefCutSeq = 1
					IF GET_GAME_TIMER() > iTimerPassDriveOff + (iBrakeTime + 500)
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])		
								//PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_CURSE_HIGH")
								CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_PUSH",CONV_PRIORITY_MEDIUM)  //You're impeding national security, boy!
							ELSE
								PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ8", "MIN2_JJ8_2", CONV_PRIORITY_MEDIUM)  //So I just get left here?
							ENDIF
							SET_DISABLE_PRETEND_OCCUPANTS(sRCLauncherDataLocal.vehID[0], TRUE)
							OPEN_SEQUENCE_TASK(seqMoveOverDriveOff)
								TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL,sRCLauncherDataLocal.vehID[0])
								IF IS_ENTITY_IN_ANGLED_AREA(sRCLauncherDataLocal.vehID[0], <<599.210693,2814.523682,37.923046>>, <<592.278625,2918.858154,67.900497>>, 110.000000)
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,sRCLauncherDataLocal.vehID[0],<<657.2675, 2820.9895, 40.3190>>,16,DRIVINGSTYLE_NORMAL,PRANGER,DRIVINGMODE_AVOIDCARS_RECKLESS,10,200)
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,sRCLauncherDataLocal.vehID[0],<<662.5688, 2702.0947, 39.7719>>,20,DRIVINGSTYLE_NORMAL,PRANGER,DRIVINGMODE_AVOIDCARS_RECKLESS,10,200)
									TASK_VEHICLE_DRIVE_WANDER(NULL,sRCLauncherDataLocal.vehID[0],100.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								ELIF IS_ENTITY_IN_ANGLED_AREA(sRCLauncherDataLocal.vehID[0], <<535.598022,2706.873047,40.613750>>, <<529.495911,2810.597412,50.720314>>, 36.000000)
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,sRCLauncherDataLocal.vehID[0],<<538.2090, 2696.4927, 41.2063>>,16,DRIVINGSTYLE_NORMAL,PRANGER,DRIVINGMODE_AVOIDCARS_RECKLESS,10,200)
									TASK_VEHICLE_DRIVE_WANDER(NULL,sRCLauncherDataLocal.vehID[0],100.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								ELSE
									TASK_VEHICLE_DRIVE_WANDER(NULL,sRCLauncherDataLocal.vehID[0],100.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
								ENDIF
							CLOSE_SEQUENCE_TASK(seqMoveOverDriveOff)
							TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[ic_JOE],seqMoveOverDriveOff)
							iJosefCutSeq = 2
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		WAIT(0)
	ENDWHILE
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[0],VEHICLELOCK_LOCKED)
	ENDIF
		
ENDPROC

PROC EndSetup()
	
	IF bMissionFailed = TRUE	
	ENDIF
		
	vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
	GET_SAFE_COORD_FOR_PED(vPlayer,FALSE,vSafeCoord)
	
ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()

	/*
	IF IS_PED_UNINJURED(pedImmigrant[0])
		IF IS_ENTITY_ATTACHED(pedImmigrant[0])
			IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[0])		
				SAFE_DELETE_PED(pedImmigrant[0])
			ENDIF
		ENDIF
	ENDIF	
	IF IS_PED_UNINJURED(pedImmigrant[1])
		IF IS_ENTITY_ATTACHED(pedImmigrant[1])
			IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[1])		
				SAFE_DELETE_PED(pedImmigrant[1])
			ENDIF
		ENDIF
	ENDIF	
	IF IS_PED_UNINJURED(pedImmigrantChase2)
		IF IS_ENTITY_ATTACHED(pedImmigrantChase2)
			IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)
				SAFE_DELETE_PED(pedImmigrantChase2)
			ENDIF
		ENDIF
	ENDIF
	*/

	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MANUEL)
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	TRIGGER_MUSIC_EVENT("MM2_FAIL")	
	
	RESET_SCENARIO_TYPES_ENABLED()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(SANCHEZ)
	SET_MODEL_AS_NO_LONGER_NEEDED(PRANGER) 
	SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER) 
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Migrant_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(SPEEDO)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_GAFFER_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_AMB_PHONE_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PRANGER,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SANCHEZ,FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BLAZER,FALSE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
	
	//REMOVE_SCENARIO_BLOCKING_AREA(blockScenarios)	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiCement)	
	/*
	IF NOT IS_REPLAY_IN_PROGRESS()		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE) 
	ENDIF
	*/
	CLEANUP_AUDIO_SCENE_CHASE()
	
	//SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,140,FALSE,TRUE)
	
	KILL_ANY_CONVERSATION()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	
	// Set the Character as no longer needed if it has been created
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[ic_JOSEF])
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[ic_JOE])
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO",TRUE)		
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_CONSTRUCTION_SOLO", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_CONSTRUCTION_PASSENGERS", TRUE)
	
	SET_ROADS_BACK_TO_ORIGINAL(<<-199.2860, 4214.0547, 43.7470>> - <<10,10,10>>,<<-199.2860, 4214.0547, 43.7470>> + <<10,10,10>>)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

PROC Script_Passed()
	//CREDIT_BANK_ACCOUNT(CHAR_TREVOR, BAAC_UNLOGGED_SMALL_ACTION, 200) // See B*1207293
	Random_Character_Passed(CP_RAND_C_MIN2)
	Script_Cleanup()
	
ENDPROC


// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

	PROC ResetStuff()
			
			CleanupTraffic()
			
			i = 0
	
			CLEAR_PRINTS()
			
			eCutsceneState = eCutSpawnJJ
			
			SAFE_REMOVE_BLIP(blipImmigrant[0])
			SAFE_REMOVE_BLIP(blipImmigrant[1])
			SAFE_REMOVE_BLIP(blipImmigrant[2])
			SAFE_REMOVE_BLIP(blipLocate[0])		
			SAFE_REMOVE_BLIP(blipLocate[1])
			SAFE_REMOVE_BLIP(blipLocate[2])		
			SAFE_REMOVE_BLIP(blipPlayerCar)		
			SAFE_REMOVE_BLIP(blipJosef)		
			SAFE_REMOVE_BLIP(blipJoe)		
			SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[ic_JOE])		
			SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[ic_JOSEF])		
			SAFE_DELETE_PED(pedImmigrant[0])		
			SAFE_DELETE_PED(pedImmigrant[1])		
			SAFE_DELETE_PED(pedImmigrant[2])		
			SAFE_DELETE_PED(pedWorker[0])	
			SAFE_DELETE_PED(pedWorker[1])		
			SAFE_DELETE_PED(pedImmigrantChase2)		
			SAFE_DELETE_PED(pedDusterPilot)		
			SAFE_DELETE_VEHICLE(vehDuster)
			SAFE_DELETE_VEHICLE(vehChaseCar[0])
			SAFE_DELETE_VEHICLE(vehChaseCar[1])
			SAFE_DELETE_VEHICLE(vehChaseCar[2])
			SAFE_DELETE_VEHICLE(vehChaseCarChase2)
			SAFE_DELETE_VEHICLE(vehSpeedo)
			
			SAFE_DELETE_OBJECT(ObjPapers)
			
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
			ENDIF
			/*
			IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 94.6126, 4506.0459, 87.7443 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),198.9812)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
			ENDIF
			*/
			REMOVE_VEHICLE_STUCK_CHECK(sRCLauncherDataLocal.vehID[0])
			
			SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
			
			bBloke1AttachedToCar = FALSE
			bBloke2AttachedToCar = FALSE
			bpedImmigrantChase2Attached = FALSE
			bChaseStarted = FALSE
			iTimesTazed = 0
			iTimesTazedSECONDPED = 0
			//iNextCoordPed1 = 0
			bSECONDBlokestunned = FALSE
			bImmigrantChase2HandsUp = FALSE
			bImmigrantChase2GettingIntoCar = FALSE
			bBlokestunned = FALSE
			bImmigrantHandsUp = FALSE
			bImmigrantGettingIntoCar = FALSE
			bSeqSetup = FALSE
			bFirstImmigrantOnBike = FALSE
			bImmigrantChase2OnBike = FALSE
			bOkToStartChase = FALSE
			iSeqCementFactory = 0
			bImmigrantChase2Fleeing = FALSE
			bFirstImmigrantFleeing = FALSE
			iJosefCutSeq = 0
			bDialogueDontletTheOtherEscape = FALSE
			iConvoGetBackInCar = 0
			iConvoTrevorDamagingCar = 0
			bConvoSpottedImmigrant1 = FALSE
			bCarHorn = FALSE
			iSeqJosefThrowPapers = 0	
			bCementFactoryCheckpoint = FALSE
			iGetOutOfCarLines = 0
			iSeqHintCam = 0
			bPlayerNotInCarBetweenCapturingImmigrants = FALSE
			bBikeEscorting = FALSE
			bBikeLeftFactory = FALSE
			bWander = FALSE
			fTrafficStopped = 0.0
			iPedsStunnedOffBike = 0
			bCloseFinalTimeWindowForStats = FALSE	
			bReplayCementFactory = FALSE
			iPedKneeling = 0
			iPed2Kneeling = 0
			iCrueltyLines = 0
			bSecondMusicCue = FALSE
			bCreateNmMessage = FALSE
			bRagdollTaskGiven = FALSE
			bCreateNmMessagePed2 = FALSE
			bRagdollTaskGivenPed2 = FALSE
			bLosingCops = FALSE
			bPrintLosingCops = FALSE
			iSyncPed = 0
			bPassport = FALSE
			bManuelAttached = FALSE
			iConvo = 0
			bPassportSound = FALSE	
			bButterFingers = FALSE
			bWeaponSwap = FALSE
			bWarpJosef = FALSE
			bThanks = FALSE
			bPatrolVehDialogue = FALSE
			bDialogueGoBack = FALSE
			iGoBackLines = 0
			iSeqChase2Dialogue = 0
			bDialoguePickUpLastGuy = FALSE
			bPatrolVehDialogue = FALSE
			bDialogueGoBack = FALSE
			bDialoguePickUpLastGuy = FALSE
			bDialogueOkLetsGo = FALSE
			bDialogueDriveOff = FALSE
			bDoDelayedClimbInBack = FALSE
			bDoneDelayedClimbInBack = FALSE
			bSwitchWaypointFlags = FALSE
	
			bPed1GettingUp = FALSE
			bPed2GettingUp = FALSE
			
			bPrint18 = FALSE
			bPrint03 = FALSE
			bPrint06 = FALSE
			bPrint08 = FALSE
			bPrint33 = FALSE
			bPrint29 = FALSE
			bPrint32 = FALSE
			bPrint19 = FALSE
			bPrint42 = FALSE
			bPrint01 = FALSE
			bPrint30 = FALSE
			bPrint31 = FALSE
			bPrint39 = FALSE
			bPrint37 = FALSE
			bPrint48GoBack = FALSE
			
			bConvoImmigrantManuelGiveUp = FALSE
			bConvoImmigrant1GiveUp = FALSE
			bConvoImmigrant2GiveUp = FALSE
			
			bJosefGetIn = FALSE
			
			CLEAR_SEQUENCE_TASK(seqChase2Route)
			CLEAR_SEQUENCE_TASK(seqChase2RouteB)
			CLEAR_SEQUENCE_TASK(seqChase2RouteC)
			CLEAR_SEQUENCE_TASK(seqChase2RouteD)
			CLEAR_SEQUENCE_TASK(seqChase2RouteSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteBSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteCSECONDPED)
			CLEAR_SEQUENCE_TASK(seqChase2RouteDSECONDPED)
	
ENDPROC

/// PURPOSE: Waits for the screen to fade out, then updates the fail reason for the mission
PROC FailWait()

	SWITCH eFAIL_STATE
	
		CASE FS_SETUP
			TRIGGER_MUSIC_EVENT("MM2_FAIL")		
			RemoveBlips()
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				Random_Character_Failed_With_Reason(sFailReason)
			ELSE
				Random_Character_Failed()
			ENDIF
			
			eFAIL_STATE =FS_UPDATE
		BREAK
		
		CASE FS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
							
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				// MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 101.6329, 4519.8418, 89.4828 >>, 175.0489)
  				// SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 99.4852, 4507.5508, 88.1143 >>, 179.7428)
				
				ResetStuff()
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
	
/// PURPOSE: skip from MS_SETUP_CHASE1
PROC SkipStage1()

	SAFE_REMOVE_BLIP(blipLocate[i])
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])		
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<< 132.1798, 4453.1421, 79.8101 >>)
	SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],227.5366 )
		
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

ENDPROC

/// PURPOSE: skip from MS_UPDATE_CHASE1
PROC SkipStage2()

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	AttachPedToCar()
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbToBack_1)
		SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_1,0.9)
	ENDIF
	
	i = 1								
											
	SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<< 227.1500, 2950.6118, 41.8969 >>)
	SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],186.9743 )
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							
	missionStage = MS_SETUP_CHASE2
							
	RequestModelsSecondChase()
	LoadModelsSecondChase()
				
	bDebugSkippedToCementFactory = TRUE

ENDPROC

/// PURPOSE: skip from MS_SETUP_CHASE2 or MS_CEMENT_FACTORY or MS_UPDATE_CHASE2
PROC SkipStage3()

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SAFE_REMOVE_BLIP(blipLocate[0])
	SAFE_REMOVE_BLIP(blipLocate[1])
	
	IF IS_PED_UNINJURED(pedImmigrant[i])	
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
			IF NOT IS_PED_IN_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0])
				TASK_WARP_PED_INTO_VEHICLE(pedImmigrant[i],sRCLauncherDataLocal.vehID[0],VS_BACK_RIGHT)
			ENDIF
		ENDIF
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	missionStage = MS_DRIVE_TO_END
	
	bDebugSkippedToCementFactory = TRUE

ENDPROC

/// PURPOSE: skip from MS_DRIVE_TO_END
PROC SkipStage4()

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					TASK_WARP_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SAFE_REMOVE_BLIP(blipLocate[0])
	SAFE_REMOVE_BLIP(blipLocate[1])
	
	SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<< 325.4911, 2643.6294, 43.5842 >>)
	SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],96.8564)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	PassDriveOff()
							
	//EndSetup()
	missionStage = MS_MISSION_PASSING						
	//Script_Passed()

ENDPROC

/// PURPOSE: Check for Forced Pass or Fail
PROC DEBUG_Check_Debug_Keys()
// Check for Pass
#IF IS_DEBUG_BUILD
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		//EndSetup()
		WAIT_FOR_CUTSCENE_TO_STOP()
		TRIGGER_MUSIC_EVENT("MM2_STOP")		
		CLEAR_PRINTS()
		Script_Passed()
	ENDIF

	// Check for Fail
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		//EndSetup()
		WAIT_FOR_CUTSCENE_TO_STOP()
		TRIGGER_MUSIC_EVENT("MM2_STOP")		
		CLEAR_PRINTS()
		bMissionFailed = TRUE	
		missionStage = MS_MISSION_FAILING		
	ENDIF
#ENDIF

ENDPROC

/// PURPOSE: Play intro mocap
PROC IntroCutscene()
	
	MODEL_NAMES mnVehPlayer
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		//SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_HEAD, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_EYES, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_HEAD, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_EYES, 0, 0)
	ENDIF
	
	SWITCH eCutsceneState
		
		CASE eCutSpawnJJ
		
			//Added time to allow multihead blinders to turn on
			IF IS_PC_VERSION() AND NOT bMultiheadTriggered
				IF SET_MULTIHEAD_SAFE(TRUE)
					iMultiHeadTimer = GET_GAME_TIMER() + 1000
					bMultiheadTriggered = TRUE
				ENDIF
			ENDIF
						
			IF iMultiHeadTimer < GET_GAME_TIMER()
				IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
					sRCLauncherDataLocal.vehID[0] = CREATE_VEHICLE(PRANGER,<<20.80, 4532.65, 104.66>>, 284.70, TRUE, TRUE)
				ENDIF
				
				IF CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[ic_JOE],CHAR_JOE,<< 38.2818, 4537.5703, 95.4783 >>, 252.9034)
				AND CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[ic_JOSEF],CHAR_JOSEF,<< 33.8430, 4537.3950, 95.9413 >>, 309.8384)
					//SET_PED_PRELOAD_PROP_DATA(sRCLauncherDataLocal.pedID[ic_JOE],ANCHOR_HEAD,0)
					//SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOE], ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOE], ANCHOR_EYES, 0)
					SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOSEF], ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOSEF], ANCHOR_EYES, 0)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "Min2****Init cutscene****")
					#ENDIF	
					
					eCutsceneState = eCutInit
				ENDIF
			ENDIF
				
		BREAK
		
		CASE eCutInit
			
			RC_REQUEST_CUTSCENE("mmb_2_rcm")
			
			IF RC_IS_CUTSCENE_OK_TO_START()	
				
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.vehID[0],"MMB_Parked_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,PRANGER)		
				ENDIF
			
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[ic_JOSEF],"Josef",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_JOSEF)	
				ENDIF
			
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[ic_JOE],"Joe",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,IG_JOEMINUTEMAN)	
				ENDIF
				
				CCSExitTrev = FALSE
				CCSExitJosef = FALSE
				CCSExitJoe = FALSE
				CCsExitTruck = FALSE
				CCsExitCam = FALSE
						
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE()
				SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(PRANGER)
				WAIT(0)
		
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<14.701344,4530.018066,102.562462>>, <<27.267115,4533.092285,107.864037>>, 11.250000,<<-0.3602, 4523.2480, 107.0955>>, 256.1712)
				IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					mnVehPlayer = GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())
				ENDIF
				IF NOT IS_THIS_MODEL_A_HELI(mnVehPlayer)
				AND NOT IS_THIS_MODEL_A_PLANE(mnVehPlayer)	
				AND NOT IS_THIS_MODEL_A_BOAT(mnVehPlayer)
				AND NOT IS_THIS_MODEL_A_TRAIN(mnVehPlayer)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0,TRUE,CHAR_TREVOR)
				ENDIF
				CLEAR_AREA(<< 18.00, 4527.00, 105.00 >>,150,TRUE)
				RC_START_CUTSCENE_MODE(<< 18.00, 4527.00, 105.00 >>,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
				eCutsceneState = eCutUpdate
			ENDIF
				
		BREAK
		
		CASE eCutUpdate
	
			//IF IS_CUTSCENE_PLAYING()
				
				IF CCSExitTrev
				AND CCSExitJosef
				AND CCSExitJoe
				//AND CCsExitTruck
				AND CCsExitCam
					eCutsceneState = eCutCleanup
				ENDIF
			
	    		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					CCsExitCam = TRUE
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					REPLAY_STOP_EVENT()
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							CCSExitTrev = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Josef",IG_JOSEF)
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])		
						IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
							SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_RIGHT)
			   				FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[ic_JOSEF])
							CCSExitJosef = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Joe",IG_JOEMINUTEMAN)
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])				
						IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
							SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
			   				FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[ic_JOE])
							CCSExitJoe = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MMB_Parked_car",PRANGER)
					CCsExitTruck = TRUE
				ENDIF
				CCsExitTruck = CCsExitTruck
				IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Parked_car"))
						sRCLauncherDataLocal.vehID[0] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Parked_car"))	
						CPRINTLN(DEBUG_MISSION, "Min2**** GRABBED CAR FROM CS ****")
					ENDIF
				ENDIF	
				
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
						STOP_CUTSCENE()
					ENDIF
					DEBUG_Check_Debug_Keys()
				#ENDIF
			//ELSE
				
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				
				//eCutsceneState = eCutCleanup
			//ENDIF
	
		BREAK
		
		CASE eCutCleanup
			
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
				SET_VEHICLE_DOOR_SHUT(sRCLauncherDataLocal.vehID[0],SC_DOOR_FRONT_LEFT)
				SET_VEHICLE_DOOR_SHUT(sRCLauncherDataLocal.vehID[0],SC_DOOR_REAR_RIGHT)
				SET_VEHICLE_DOOR_SHUT(sRCLauncherDataLocal.vehID[0],SC_DOOR_FRONT_RIGHT)
			ENDIF
			

			IF bPrint01	= FALSE
				PRINT_NOW("MIN2_01",7000,0)                 //chase the immigrant
				bPrint01 = TRUE
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	
			RC_END_CUTSCENE_MODE()
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)

			//Cleanup multihead blinders
			IF IS_PC_VERSION()
				SET_MULTIHEAD_SAFE(FALSE)
			ENDIF

			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "Min2****Cleanup cutscene****")
			#ENDIF	
			
			missionStage = MS_SETUP_MISSION
			
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE: Skip past intro mocap
PROC SkipPastCS()
	
	START_REPLAY_SETUP(<<20.80, 4532.65, 104.66>>, 284.70)
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	Initialise()
	WaitForAssets()
	SpawnPlayerCar()
	SpawnJosefAndJoe()
	missionStage = MS_SETUP_CHASE1

	END_REPLAY_SETUP(sRCLauncherDataLocal.vehID[0])

	//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	//WAIT(1000)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_IN()
	      	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
		ENDIF
	ENDIF
	
	IF bPrint01	= FALSE
		PRINT_NOW("MIN2_01",7000,0)                 //chase the immigrant
		bPrint01 = TRUE
	ENDIF
	
	iTimerChase1Started = GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "Min2****Replay skipped past cutscene****")
	#ENDIF	
	
	RC_END_Z_SKIP(TRUE) 

ENDPROC

/// PURPOSE: Skip the first chase				
PROC SkipFirstChase()

	START_REPLAY_SETUP(<< 13.7719, 3617.9197, 39.2074 >>, 242.9842)

	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	Initialise()
	WaitForAssets()
	SpawnPlayerCar()
	SpawnJosefAndJoe()
	SkipStage1()
	SpawnChaseCar()
	SpawnImmigrant()
	SkipStage2()
	RequestModelsSecondChase()
	LoadModelsSecondChase()
	
	END_REPLAY_SETUP(sRCLauncherDataLocal.vehID[0])
	
	bDebugSkippedToCementFactory = FALSE
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<< 13.7719, 3617.9197, 39.2074 >>)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],242.9842)
	ENDIF
	
	missionStage = MS_SETUP_CHASE2
	
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
	iConvo = 2
	
	//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN)						
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,1,TRUE)							
	ENDIF
	
	WAIT(500)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_IN()
	      	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "Min2****Replay skipped first chase****")
	#ENDIF	
	
	RC_END_Z_SKIP(TRUE) 

ENDPROC

/// PURPOSE: Skip to chase 2					
PROC SkipToCementFact()
			
	START_REPLAY_SETUP(<< 277.4130, 2856.9407, 42.6421 >>, 300.9114)		
			
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	Initialise()
	WaitForAssets()
	SpawnPlayerCar()
	SpawnJosefAndJoe()
	SkipStage1()
	SpawnChaseCar()
	SpawnImmigrant()
	SkipStage2()
	RequestModelsSecondChase()
	LoadModelsSecondChase()
	SpawnChaseCar()
	SpawnImmigrant()
	
	bDebugSkippedToCementFactory = FALSE
	
	END_REPLAY_SETUP(sRCLauncherDataLocal.vehID[0])
	
	IF IS_VEHICLE_OK(vehChaseCar[i])	
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCar[i])
			START_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i],101,"Min2DB1")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehChaseCar[i],400)
			SET_PLAYBACK_SPEED(vehChaseCar[i],1.1)
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(vehChaseCarChase2)	
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehChaseCarChase2)	
			START_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2,102,"Min2DB2")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehChaseCarChase2,500)
			SET_PLAYBACK_SPEED(vehChaseCarChase2,1.1)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<< 277.4130, 2856.9407, 42.6421 >>)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],300.9114)
		START_PLAYBACK_RECORDED_VEHICLE(sRCLauncherDataLocal.vehID[0],269,"Min2Rolling")
		SET_PLAYBACK_SPEED(sRCLauncherDataLocal.vehID[0],1.0)
		iRollingStartTimer = GET_GAME_TIMER()
	ENDIF
	missionStage = MS_SETUP_CHASE2
	
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
	
	//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN)						
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,1,TRUE)							
	ENDIF
	
	WAIT(800)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_IN()
	      	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
		ENDIF
	ENDIF		
	
	TRIGGER_MUSIC_EVENT("MM2_RESTART1")		
			
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "Min2****Replay skipped to cement factory****")
	#ENDIF			
	
	bReplayCementFactory = TRUE		
			
	RC_END_Z_SKIP(TRUE) 		
			
ENDPROC

PROC SkipToEnd()

	START_REPLAY_SETUP(<<349.8519, 2662.7688, 43.6822>>, 137.3727)		

	WAIT_FOR_CUTSCENE_TO_STOP()
	TRIGGER_MUSIC_EVENT("MM2_STOP")
	bDebugSkipping = TRUE
	ResetStuff()
	Initialise()
	WaitForAssets()
	SpawnPlayerCar()
	SpawnJosefAndJoe()
	SkipStage1()
	SpawnChaseCar()
	SpawnImmigrant()
	SkipStage2()
	RequestModelsSecondChase()
	LoadModelsSecondChase()
	SpawnChaseCar()
	SpawnImmigrant()
	
	END_REPLAY_SETUP()
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])
				//IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				//	SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				//ENDIF
				IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],VS_FRONT_RIGHT)
				ENDIF
				//IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],VS_BACK_LEFT)
				//ENDIF
			ENDIF
		ENDIF
		IF NOT DOES_ENTITY_EXIST(pedImmigrant[1])
			pedImmigrant[1] = CREATE_PED(PEDTYPE_MISSION,S_M_M_Migrant_01,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<0,0,10>>),0)
			SET_PED_COMPONENT_VARIATION(pedImmigrant[1],PED_COMP_HEAD,0,1)
			SET_PED_COMPONENT_VARIATION(pedImmigrant[1],PED_COMP_TORSO,1,0)
			SET_PED_COMPONENT_VARIATION(pedImmigrant[1],PED_COMP_LEG,0,0)
			SET_PED_COMPONENT_VARIATION(pedImmigrant[1],PED_COMP_FEET,1,0)
			SET_PED_COMPONENT_VARIATION(pedImmigrant[1],PED_COMP_SPECIAL,1,0)
			CPRINTLN(DEBUG_MISSION, "**** pedImmigrant[1] didn't exist - create ****")
		ENDIF
		IF IS_PED_UNINJURED(pedImmigrant[1])	
			//RESET_PED_MOVEMENT_CLIPSET(pedImmigrant[1])
			ssClimbToBack_2 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssClimbToBack_2,TRUE)
			ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssClimbToBack_2,sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))
			TASK_SYNCHRONIZED_SCENE(pedImmigrant[1], ssClimbToBack_2, "missminuteman_2ig_1", "entertrunk_josef", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT | RBF_VEHICLE_IMPACT)
			SET_SYNCHRONIZED_SCENE_PHASE(ssClimbToBack_2,0.99)
			SET_PED_CAN_BE_TARGETTED(pedImmigrant[1],FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedImmigrant[1],GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
			iSyncPed = 1
			CPRINTLN(DEBUG_MISSION, "**** pedImmigrant[1] kick off SS ****")
		ENDIF	
		IF IS_PED_UNINJURED(pedImmigrantChase2)	
			IF NOT IS_PED_IN_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0])
				SET_PED_INTO_VEHICLE(pedImmigrantChase2,sRCLauncherDataLocal.vehID[0],VS_BACK_RIGHT)
				SET_PED_CAN_BE_TARGETTED(pedImmigrantChase2,FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedImmigrantChase2,GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
			ENDIF
		ENDIF
	ENDIF
	
	//RESOLVE_IDLES()
			
	SAFE_REMOVE_BLIP(blipLocate[0])
	SAFE_REMOVE_BLIP(blipLocate[1])
	
	SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<<696.1606, 2704.9006, 39.4572>>)
	SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],92.8104)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()					
	
	SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<698.7277, 2708.0000, 39.3397>>, 91.2899)
	
	Passdriveoff()
	
	iTimerPassDriveOff = GET_GAME_TIMER() - 17000
	
	missionStage = MS_MISSION_PASSING
	
	//LOAD_SCENE(<<349.8519, 2662.7688, 43.6822>>)

	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN)						
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_STUNGUN,1,TRUE)							
	ENDIF

	//WAIT(500)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)

	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)

ENDPROC

PROC DEBUG_Check_Skips()

#IF IS_DEBUG_BUILD
	
	SkipMenuStruct[0].sTxtLabel = "Intro Cutscene"  
	SkipMenuStruct[1].sTxtLabel = "Quad Chase"  
	SkipMenuStruct[2].sTxtLabel = "Scrambler Chase"  
	SkipMenuStruct[3].sTxtLabel = "End"  
	
	IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage)
		
		IF iReturnStage = 0
			WAIT_FOR_CUTSCENE_TO_STOP()
			//bDebugSkipping = TRUE	           
		   	ResetStuff()
			Initialise()
			missionStage = MS_INTRO_MOCAP
			
		ELIF iReturnStage = 1	
			WAIT_FOR_CUTSCENE_TO_STOP()
			bDebugSkipping = TRUE
			ResetStuff()
			Initialise()
			WaitForAssets()
			SpawnPlayerCar()
			SpawnJosefAndJoe()
			SkipStage1()
			missionStage = MS_SETUP_CHASE1
			
		ELIF iReturnStage = 2
			WAIT_FOR_CUTSCENE_TO_STOP()
			bDebugSkipping = TRUE
			ResetStuff()
			Initialise()
			WaitForAssets()
			SpawnPlayerCar()
			SpawnJosefAndJoe()
			SkipStage1()
			SpawnChaseCar()
			SpawnImmigrant()
			SkipStage2()
			RequestModelsSecondChase()
			LoadModelsSecondChase()
			missionStage = MS_SETUP_CHASE2
			
		ELIF iReturnStage = 3
			/*
			WAIT_FOR_CUTSCENE_TO_STOP()
			bDebugSkipping = TRUE
			ResetStuff()
			Initialise()
			WaitForAssets()
			SpawnPlayerCar()
			SpawnJosefAndJoe()
			SkipStage1()
			SpawnChaseCar()
			SpawnImmigrant()
			SkipStage2()
			RequestModelsSecondChase()
			LoadModelsSecondChase()
			SpawnChaseCar()
			SpawnImmigrant()
			SkipStage4()
			*/
			SkipToEnd()
		ENDIF
			
	ENDIF

	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	
		bDebugSkipping = TRUE
		
		TRIGGER_MUSIC_EVENT("MM2_STOP")
		
		SWITCH missionStage
		
			CASE MS_SETUP_MISSION
			
				WAIT_FOR_CUTSCENE_TO_STOP()
			
			BREAK
			
			CASE MS_SETUP_CHASE1
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage1()
			BREAK
			
			CASE MS_UPDATE_CHASE1
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage2()
			BREAK
			
			CASE MS_IMMIGRANT1_APPREHENDED
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage2()
			BREAK
			
			CASE MS_SETUP_CHASE2
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage3()
			BREAK
			
			CASE MS_CEMENT_FACTORY
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage3()
			BREAK
			
			CASE MS_UPDATE_CHASE2
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage3()
			BREAK
			
			CASE MS_JOSEF_PICKED_UP_IMMIGRANT2
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage3()
			BREAK
			
			CASE MS_SETUP_CHASE3
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage3()
			BREAK
				
			CASE MS_DRIVE_TO_END
				WAIT_FOR_CUTSCENE_TO_STOP()
				SkipStage4()
			BREAK	
			
			CASE MS_MISSION_PASSING
				WAIT_FOR_CUTSCENE_TO_STOP()
				CLEAR_PRINTS()
				Script_Passed()
			BREAK	
			
		ENDSWITCH
	ENDIF
		
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		WAIT_FOR_CUTSCENE_TO_STOP()
		bDebugSkipping = TRUE
		ResetStuff()
		Initialise()						
		IF missionStage = MS_INTRO_MOCAP
			missionStage = MS_INTRO_MOCAP
		ELIF missionStage = MS_SETUP_MISSION
			missionStage = MS_INTRO_MOCAP
		ELIF missionStage = MS_SETUP_CHASE1
			missionStage = MS_INTRO_MOCAP
		ELIF missionStage = MS_UPDATE_CHASE1
			missionStage = MS_INTRO_MOCAP
		ELIF missionStage = MS_IMMIGRANT1_APPREHENDED
			SkipPastCS()
		ELIF missionStage = MS_SETUP_CHASE2
			SkipPastCS()
		ELIF missionStage = MS_CEMENT_FACTORY
			SkipFirstChase()
		ELIF missionStage = MS_UPDATE_CHASE2
			IF GET_GAME_TIMER() > iArrivedAtCementFactory + 15000
				SkipToCementFact()
			ELSE
				SkipFirstChase()
			ENDIF
		ELIF missionStage = MS_DRIVE_TO_END
			SkipToCementFact()
		ELIF missionStage = MS_MISSION_PASSING
			SkipToCementFact()	
		ENDIF		
	ENDIF
		
#ENDIF	

ENDPROC

/// PURPOSE: Manage the rolling start at chase 2
PROC ManageRollingStart()
	
	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[0])
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
		OR GET_GAME_TIMER() > iRollingStartTimer + 3500
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[0])
				STOP_PLAYBACK_RECORDED_VEHICLE(sRCLauncherDataLocal.vehID[0])
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Sets up the mission, spawn/init everything
PROC MS_IntroMocap()
	
	IF IS_REPLAY_IN_PROGRESS() = TRUE
	AND bDebugSkipping = FALSE	
		IF CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[ic_JOE],CHAR_JOE,<< 38.2818, 4537.5703, 95.4783 >>, 252.9034)
		AND CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[ic_JOSEF],CHAR_JOSEF,<< 33.8430, 4537.3950, 95.9413 >>, 309.8384)
			//SET_PED_PRELOAD_PROP_DATA(sRCLauncherDataLocal.pedID[ic_JOE],ANCHOR_HEAD,0)
			//SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOE], ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOE], ANCHOR_EYES, 0)
			SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOSEF], ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(sRCLauncherDataLocal.pedID[ic_JOSEF], ANCHOR_EYES, 0)
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		
			IF g_bShitskipAccepted = TRUE
				iReplayStage++ // player is skipping this stage
			ENDIF	
			
			IF iReplayStage = 0
				SkipPastCS()
			ELIF iReplayStage = 1
				SkipFirstChase()
			ELIF iReplayStage = 2
				SkipToCementFact()
			ELIF iReplayStage > 2
				SkipToEnd()
			ENDIF
		
		ENDIF
	ELSE
		IntroCutscene()
	ENDIF

ENDPROC

/// PURPOSE: Sets up the mission, spawn/init everything
PROC MS_SetupMission()
	
	WaitForAssets()
	SpawnPlayerCar()
	SpawnJosefAndJoe()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "Min2****Init setup chase 1****")
	#ENDIF
	IF bPrint01	= FALSE
		PRINT_NOW("MIN2_01",7000,0)                 //chase the immigrant
		bPrint01 = TRUE
	ENDIF
	iTimerChase1Started = GET_GAME_TIMER()
	missionStage = MS_SETUP_CHASE1
	
ENDPROC

/// PURPOSE: Setup the first chase
PROC MS_SetupChase1()
	
	SpawnChaseCar()
	SpawnImmigrant()
	Convo()
	IF bOkToStartChase = TRUE
		StartVehiclePlayback()
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Min2****Init update chase 1****")
		#ENDIF		
		//IF NOT IS_REPLAY_IN_PROGRESS()	
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIN2_FIRST_CAPTURE)
		//ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "STATS >>>> TIME WINDOW OPENED (First chase) <<<< ")
		#ENDIF		
		
		REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
		
		missionStage = MS_UPDATE_CHASE1
	ENDIF

ENDPROC

/// PURPOSE: Manage the first chase
PROC MS_UpdateChase1()

	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)

	IF IS_PED_UNINJURED(pedImmigrant[0])	
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[0])
		AND NOT IS_PED_RAGDOLL(pedImmigrant[0])	
		AND GET_ENTITY_HEALTH(pedImmigrant[0]) > 150
			SET_ENTITY_HEALTH(pedImmigrant[0],150)
		ENDIF
		IF bDisableAbortConvo
		AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_PED_RAGDOLL(pedImmigrant[i])	
			SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
			bDisableAbortConvo = FALSE
		ENDIF
		START_AUDIO_SCENE_CHASE()  
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])		
				IF bBlokestunned = TRUE
					IF bImmigrantHandsUp = FALSE
						IF IS_ENTITY_ALIVE(vehChaseCar[0])
							IF GET_IS_VEHICLE_ENGINE_RUNNING(vehChaseCar[0])	
								SET_VEHICLE_ENGINE_ON(vehChaseCar[0],FALSE,TRUE)
							ENDIF
						ENDIF
						//IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
							IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
							OR GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
							OR GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK
								CLEAR_PED_TASKS(pedImmigrant[i])
							ENDIF
						//ENDIF
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOE],10)
							AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOSEF],10)	
							AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),10)	
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()			
									IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),100)		
										IF bPrint48GoBack = FALSE
											PRINT_NOW("MIN2_48",DEFAULT_GOD_TEXT_TIME,0)    //Go back and pick up the ~r~questionable.
											bPrint48GoBack = TRUE
										ENDIF
									ENDIF
								ENDIF
								IF NOT IS_ENTITY_IN_WATER(pedImmigrant[i])
									iPedInWater = 0
									/*
									IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
									AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
										TASK_WRITHE(pedImmigrant[i],PLAYER_PED_ID(),-1,SHM_ONGROUND)
									ENDIF
									*/
									IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
										bCreateNmMessage = FALSE
										IF CAN_PED_RAGDOLL(pedImmigrant[i])	
											bRagdollTaskGiven = TRUE
											SET_PED_TO_RAGDOLL(pedImmigrant[i],1000,2000,TASK_NM_SCRIPT)
										ENDIF
									ELSE
										IF bRagdollTaskGiven = TRUE	
											IF bCreateNmMessage = FALSE	
											AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrant[i])	
												CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
												GIVE_PED_NM_MESSAGE(pedImmigrant[i])
												bRagdollTaskGiven = FALSE
												bCreateNmMessage = TRUE
												IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),40)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
														CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ1",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 1
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									++iPedInWater
									IF iPedInWater > 20
										SET_ENTITY_HEALTH(pedImmigrant[i],0)
									ENDIF
								ENDIF
							ENDIF
							IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
							AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
								IF NOT IS_ENTITY_IN_WATER(pedImmigrant[i])	
									iPedInWater = 0
									/*
									IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
									AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
										TASK_WRITHE(pedImmigrant[i],PLAYER_PED_ID(),-1,SHM_ONGROUND)
									ENDIF
									*/
									IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
										bCreateNmMessage = FALSE
										IF CAN_PED_RAGDOLL(pedImmigrant[i])	
											bRagdollTaskGiven = TRUE
											SET_PED_TO_RAGDOLL(pedImmigrant[i],1000,2000,TASK_NM_SCRIPT)
										ENDIF
									ELSE
										IF bRagdollTaskGiven = TRUE	
											IF bCreateNmMessage = FALSE	
											AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrant[i])	
												CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
												GIVE_PED_NM_MESSAGE(pedImmigrant[i])
												bRagdollTaskGiven = FALSE
												bCreateNmMessage = TRUE
												IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),40)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
														CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ1",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 1
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									++iPedInWater
									IF iPedInWater > 20
										SET_ENTITY_HEALTH(pedImmigrant[i],0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_OK(vehChaseCar[i])	
			IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])	
				IF DOES_BLIP_EXIST(blipImmigrant[i])	
					UPDATE_CHASE_BLIP(blipImmigrant[i],pedImmigrant[i],300,0.8)
				ENDIF
			ENDIF
			IF bBlokestunned = FALSE	
				IF GET_GAME_TIMER() > iTimerChase1Started + 9000	
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
								IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])			
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
										IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],180)	
											IF iCatchUpLines < 3
												IF GET_GAME_TIMER()	> iTimerCatchUpLines + 6000
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CATCH1",CONV_PRIORITY_MEDIUM)  //Come on! Step on it!
															++iCatchUpLines
															iTimerCatchUpLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						TASK_PERFORM_SEQUENCE(pedImmigrant[i],seqChase1)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_48")	
					IF NOT bDialogueGoBack
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
							IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])			
							AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],20)
									IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_GOBACK",CONV_PRIORITY_MEDIUM)  //Right, let's go get him in custody.
										REPLAY_RECORD_BACK_FOR_TIME(3.0, 7.0, REPLAY_IMPORTANCE_LOW)
										bDialogueGoBack = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
							IF iGoBackLines < 4	
								IF GET_GAME_TIMER()	> iTimerGoBackLines + 6000	
									IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])			
									AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
									AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
										IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],40)
											IF iGoBackLines < 4		
												IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_REMIND",CONV_PRIORITY_MEDIUM)  //Are you just going to leave an enemy of this country on the side of the road?
													++iGoBackLines
													iTimerGoBackLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
												ENDIF
											/*
											ELSE
												IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_ABAND",CONV_PRIORITY_MEDIUM)  //Trevor, you forgot the illegal!
													++iGoBackLines
													iTimerGoBackLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
												ENDIF
												*/
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	//DoTraffic()
	JosefOnFoot()
	JoeOnFoot()
	UpdateVehiclePlayback()
	Chase1JJAI()
	Convo()
	//RandomCarHorns()
	IsPlayerAbandoning() 

	IF GET_GAME_TIMER() > iTimerChase1Started + 20000
		IF IS_PED_UNINJURED(pedImmigrant[0])
			IF IS_ENTITY_ATTACHED(pedImmigrant[0])
			OR IS_PED_IN_ANY_VEHICLE(pedImmigrant[0])
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
			ENDIF
		ENDIF
	ELSE
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	ENDIF
	
	IF bImmigrantHandsUp = TRUE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Min2****Init immigrant 1 apprehended****")
		#ENDIF		
		SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
		bDisableAbortConvo = FALSE
		iCatchUpLines = 0
		missionStage = MS_IMMIGRANT1_APPREHENDED
	ENDIF
	
ENDPROC

/// PURPOSE: First immigrant captured, waiting for them to get in the vehicle
PROC MS_Immigrant1Apprehended()
		
	JoeOnFoot()
	ImmigrantGetInTruck()
	IsPlayerAbandoning() 
	Convo()
	IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
	AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
		JosefOnFoot()
		/*
		IF IS_PED_IN_VEHICLE(pedImmigrant[0],sRCLauncherDataLocal.vehID[0],TRUE)	
			IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],15.0)	
				TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
			ELSE
				TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
			ENDIF
		ENDIF
		*/
	ENDIF
	
	IF bBloke1AttachedToCar = TRUE
		
		//RequestModelsSecondChase()
		//LoadModelsSecondChase()
		bLosingCops = TRUE
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Min2****Init setup chase 2****")
		#ENDIF	
		REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_LOW)
		missionStage = MS_SETUP_CHASE2
	ENDIF
	
ENDPROC

/// PURPOSE: Setup the second chase, waiting for the player to get to the cement factory
PROC MS_SetupChase2()
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>) < 200
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>,250)	
			IF bLosingCops = TRUE
				RequestModelsSecondChase()
				LoadModelsSecondChase()
			ENDIF
			bLosingCops = FALSE
			bPrintLosingCops = FALSE
			SpawnChaseCar()
			SpawnImmigrant()
			OpenGates()
		ENDIF
		IF DOES_BLIP_EXIST(blipLocate[i])
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE)
			ENDIF
			IF bCementFactoryCheckpoint = FALSE
				CleanupTraffic()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Driving to the cement factory")
				bCementFactoryCheckpoint = TRUE
			ENDIF
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>,240)
				SetupSequences()
			ENDIF
		ELSE
			IF bCementFactoryCheckpoint = FALSE	
				//DoTraffic()
			ENDIF
		ENDIF
		Convo()
		IF bOkToStartChase = TRUE
			SetupSequences()
			iArrivedAtCementFactory = GET_GAME_TIMER()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "Min2****Init cement factory****")
			#ENDIF
			TRIGGER_MUSIC_EVENT("MM2_START_FORA")
			missionStage = MS_CEMENT_FACTORY	
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOSEF], CA_DO_DRIVEBYS, TRUE)
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
				IF IS_IT_OK_FOR_JOSEF_TO_GET_IN()		
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
						IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],10.0)	
							TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,1)
						ELSE
							TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT,PEDMOVEBLENDRATIO_RUN)
						ENDIF
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK		
						TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[ic_JOSEF],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-1.8,-0.2,0>>),1,DEFAULT_TIME_BEFORE_WARP_MINUTE_2,0.5,ENAV_DEFAULT,GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])-90.0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE], CA_USE_VEHICLE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[ic_JOE], CA_DO_DRIVEBYS, TRUE)
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],TRUE)	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bPatrolVehDialogue	
			AND bButterFingers	
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<< 267.2497, 2852.1667, 42.6129 >>,200)	
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	   //How did you get your hands on this patrol car?
					AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	   
					AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	  
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_48")	
						IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ4", CONV_PRIORITY_VERY_LOW)
							bPatrolVehDialogue = TRUE
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssPassport)	
				IF NOT bDialogueLeftJorJ
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	 
						IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 10.0
							IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
							AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],40)	
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_29")	
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_32")	
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_33")	
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_49")
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_06")
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_NOJOE", CONV_PRIORITY_VERY_LOW)      //Hey, you forgot me, you dolt!
								ELSE
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_NOJOE", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
								ENDIF
								bDialogueLeftJorJ = TRUE
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
							AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],40)	
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_29")	
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_32")	
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_33")
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_49")
								AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_06")
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_NOJOSEF", CONV_PRIORITY_VERY_LOW)     //Hey! I hate this new guy!
								ELSE
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_NOJOSEF", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
								ENDIF
								bDialogueLeftJorJ = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	 
						IF iDialogueGoBackJorJ < 3
						AND GET_GAME_TIMER() > iTimerGoBackJorJ + 6000
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_29")	
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_32")	
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_33")
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_49")	
							AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_06")
								IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
								AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
								AND NOT IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],50)	
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_GOBACK2", CONV_PRIORITY_VERY_LOW)           //No! No! Need Joe!
									++iDialogueGoBackJorJ
									iTimerGoBackJorJ = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
								ENDIF
								IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])	
								AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
								AND NOT IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],50)	
									CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_GOBACK1", CONV_PRIORITY_VERY_LOW)            //We're not leaving without Josef.
									++iDialogueGoBackJorJ
									iTimerGoBackJorJ = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		bLosingCops = TRUE
		SAFE_DELETE_PED(pedImmigrant[1])
		SAFE_DELETE_PED(pedImmigrantChase2)
		SAFE_DELETE_PED(pedWorker[0])
		SAFE_DELETE_VEHICLE(vehChaseCar[1])
		SAFE_DELETE_VEHICLE(vehChaseCarChase2)
		SAFE_DELETE_VEHICLE(vehSpeedo)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
			IF NOT DOES_BLIP_EXIST(blipPlayerCar)
				blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
			ENDIF
		ELSE
			IF bPrintLosingCops = FALSE
				PRINT_NOW("MIN2_COPS",DEFAULT_GOD_TEXT_TIME,0)
				bPrintLosingCops = TRUE
			ENDIF
			SAFE_REMOVE_BLIP(blipPlayerCar)
		ENDIF
		//<< 267.2497, 2852.1667, 42.6129 >>  //cement factory
	ENDIF	

ENDPROC

/// PURPOSE: Player arrived at the cement factory, start second chase
PROC MS_CementFactory()

	IF bSecondMusicCue = FALSE
		IF GET_GAME_TIMER() > iArrivedAtCementFactory + 5000
			TRIGGER_MUSIC_EVENT("MM2_START_STA")
			bSecondMusicCue = TRUE
		ENDIF
	ENDIF
/*
	WHILE(TRUE)	
		OpenGates()	
		DEBUG_Check_Debug_Keys()
		DEBUG_Check_Skips()
		WAIT(0)
	ENDWHILE
*/
	SAFE_REMOVE_BLIP(blipLocate[i])
	OpenGates()
	Convo()
	UpdateAmbientCementFactory()
	
	IF GET_GAME_TIMER() > iArrivedAtCementFactory + 2000
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
			IF NOT DOES_BLIP_EXIST(blipImmigrant[i])
				blipImmigrant[i] = CREATE_PED_BLIP(pedImmigrant[i])
				IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[i])	
					SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_PED)
				ELSE
					SET_BLIP_SCALE(blipImmigrant[i],BLIP_SIZE_VEHICLE)
				ENDIF
				//CLEAR_PRINTS()
				IF bPrint30 = FALSE
					//PRINT_NOW("",7000,0)       //chase the immigrants
					bPrint30 = TRUE
					IF i = 0                                        //There he is! Get after him! etc             
						PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF", "MIN2_JOSEF_4", CONV_PRIORITY_VERY_LOW)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_LOW)
					ELIF i = 1
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE")
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_15", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
						CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_15", CONV_PRIORITY_LOW,DO_NOT_DISPLAY_SUBTITLES)
					ELIF i = 2
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JOSEF", "MIN2_JOSEF_6", CONV_PRIORITY_VERY_LOW,DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
				IF bPlayerNotInCarBetweenCapturingImmigrants = TRUE
					IF bPrint31 = FALSE
						PRINT_NOW("MIN2_31",7000,0)       //Go back and pick up the ~r~questionable.
						bPrint31 = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipImmigrantChase2)
				blipImmigrantChase2 = CREATE_PED_BLIP(pedImmigrantChase2)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)	
					SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_PED)
				ELSE
					SET_BLIP_SCALE(blipImmigrantChase2,BLIP_SIZE_VEHICLE)
				ENDIF
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipImmigrant[i])
				IF bPlayerNotInCarBetweenCapturingImmigrants = FALSE	
					IF bPlayerAtChaseOnFootMessagePrinted = FALSE
						IF bPrint39 = FALSE
							PRINT_NOW("MIN2_39",7000,0)                                  //The Immigrants are escaping! Get in the ~b~vehicle.
							bPrint39 = TRUE
						ENDIF
						bPlayerAtChaseOnFootMessagePrinted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_GAME_TIMER() > iArrivedAtCementFactory + 5000	
		SetupSequences()
		StartVehiclePlayback()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Dirt bike chase",TRUE)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Min2****Init update chase 2****")
		#ENDIF
		//IF NOT IS_REPLAY_IN_PROGRESS()	
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIN2_SECOND_CAPTURE)
		//ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "STATS >>>> TIME WINDOW OPENED (Second chase) <<<< ")
		#ENDIF		
		missionStage = MS_UPDATE_CHASE2
	ENDIF

ENDPROC

/// PURPOSE: Manage second chase
PROC MS_UpdateChase2()

	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)

	IF bSecondMusicCue = FALSE
		IF GET_GAME_TIMER() > iArrivedAtCementFactory + 5000
			TRIGGER_MUSIC_EVENT("MM2_START_STA")
			bSecondMusicCue = TRUE
		ENDIF
	ENDIF
	//IF GET_GAME_TIMER() > iChaseStartedTimer + 32500
	//	SpawnDuster()
	//ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])		
			IF bSECONDBlokestunned = TRUE
				IF bImmigrantChase2HandsUp = FALSE
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	 	
						IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.pedID[ic_JOE],10)	
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,sRCLauncherDataLocal.pedID[ic_JOSEF],10)	
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),10)	
							IF NOT IS_ENTITY_IN_WATER(pedImmigrantChase2)	
								iPed2InWater = 0
								/*
								IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
									TASK_WRITHE(pedImmigrantChase2,PLAYER_PED_ID(),-1,SHM_ONGROUND)
								ENDIF
								*/
								IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
									bCreateNmMessagePed2 = FALSE
									IF CAN_PED_RAGDOLL(pedImmigrantChase2)		
										bRagdollTaskGivenPed2 = TRUE
										SET_PED_TO_RAGDOLL(pedImmigrantChase2,1000,2000,TASK_NM_SCRIPT)
									ENDIF
								ELSE
									IF bRagdollTaskGivenPed2 = TRUE	
										IF bCreateNmMessagePed2 = FALSE	
										AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrantChase2)	
											CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
											GIVE_PED_NM_MESSAGE(pedImmigrantChase2)
											bRagdollTaskGivenPed2 = FALSE
											bCreateNmMessagePed2 = TRUE
											IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),40)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE")
													CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ2",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 4
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								++iPed2InWater
								IF iPed2InWater > 60
									SET_ENTITY_HEALTH(pedImmigrantChase2,0)
								ENDIF
							ENDIF
						ENDIF
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
							IF NOT IS_ENTITY_IN_WATER(pedImmigrantChase2)		
								iPed2InWater = 0
								/*
								IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
									TASK_WRITHE(pedImmigrantChase2,PLAYER_PED_ID(),-1,SHM_ONGROUND)
								ENDIF
								*/
								IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
									bCreateNmMessagePed2 = FALSE
									IF CAN_PED_RAGDOLL(pedImmigrantChase2)			
										bRagdollTaskGivenPed2 = TRUE
										SET_PED_TO_RAGDOLL(pedImmigrantChase2,1000,2000,TASK_NM_SCRIPT)
									ENDIF
								ELSE
									IF bRagdollTaskGivenPed2 = TRUE	
										IF bCreateNmMessagePed2 = FALSE	
										AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrantChase2)	
											CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
											GIVE_PED_NM_MESSAGE(pedImmigrantChase2)
											bRagdollTaskGivenPed2 = FALSE
											bCreateNmMessagePed2 = TRUE
											IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrantChase2,PLAYER_PED_ID(),40)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE2")
													CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ2",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 4
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								++iPed2InWater
								IF iPed2InWater > 60	
									SET_ENTITY_HEALTH(pedImmigrantChase2,0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bBlokestunned = TRUE
				IF bImmigrantHandsUp = FALSE
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	 			
						IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOE],10)	//9.2
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],sRCLauncherDataLocal.pedID[ic_JOSEF],10)	
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),10)	
							IF NOT IS_ENTITY_IN_WATER(pedImmigrant[i])		
								iPedInWater = 0
								/*
								IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
									TASK_WRITHE(pedImmigrant[i],PLAYER_PED_ID(),-1,SHM_ONGROUND)
								ENDIF
								*/
								IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
									bCreateNmMessage = FALSE
									IF CAN_PED_RAGDOLL(pedImmigrant[i])			
										bRagdollTaskGiven = TRUE
										SET_PED_TO_RAGDOLL(pedImmigrant[i],1000,2000,TASK_NM_SCRIPT)
									ENDIF
								ELSE
									IF bRagdollTaskGiven = TRUE	
										IF bCreateNmMessage = FALSE	
										AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrant[i])		
											CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
											GIVE_PED_NM_MESSAGE(pedImmigrant[i])
											bRagdollTaskGiven = FALSE
											bCreateNmMessage = TRUE
											IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),40)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedImmigrant[i], "IMMIGRANTMALE2")
													CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ3",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 7
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								++iPedInWater
								IF iPedInWater > 60
									SET_ENTITY_HEALTH(pedImmigrant[i],0)
								ENDIF
							ENDIF
						ENDIF
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
							IF NOT IS_ENTITY_IN_WATER(pedImmigrant[i])		
								iPedInWater = 0
								/*
								IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> PERFORMING_TASK	
								AND GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_WRITHE) <> WAITING_TO_START_TASK	
									TASK_WRITHE(pedImmigrant[i],PLAYER_PED_ID(),-1,SHM_ONGROUND)
								ENDIF
								*/
								IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
									bCreateNmMessage = FALSE
									IF CAN_PED_RAGDOLL(pedImmigrant[i])	
										bRagdollTaskGiven = TRUE
										SET_PED_TO_RAGDOLL(pedImmigrant[i],1000,2000,TASK_NM_SCRIPT)
									ENDIF
								ELSE
									IF bRagdollTaskGiven = TRUE	
										IF bCreateNmMessage = FALSE	
										AND IS_PED_RUNNING_RAGDOLL_TASK(pedImmigrant[i])
											CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
											GIVE_PED_NM_MESSAGE(pedImmigrant[i])
											bRagdollTaskGiven = FALSE
											bCreateNmMessage = TRUE
											IF IS_ENTITY_IN_RANGE_ENTITY(pedImmigrant[i],PLAYER_PED_ID(),40)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedImmigrant[i], "IMMIGRANTMALE2")
													CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_INJ3",CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)  //Immigrant groaning in pain 7
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								++iPedInWater
								IF iPedInWater > 60
									SET_ENTITY_HEALTH(pedImmigrant[i],0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlokestunned = TRUE
	AND bSECONDBlokestunned = TRUE
		IF bCloseFinalTimeWindowForStats = FALSE	
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "STATS >>>> TIME WINDOW CLOSED (Second chase) <<<< ")
			#ENDIF		
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 8.0, REPLAY_IMPORTANCE_LOWEST)	
			TRIGGER_MUSIC_EVENT("MM2_STOP")		
			//IF NOT IS_REPLAY_IN_PROGRESS()	
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE,MIN2_SECOND_CAPTURE) 
			//ENDIF
			bCloseFinalTimeWindowForStats = TRUE
		ENDIF
	ENDIF
	
	IF IS_REPLAY_IN_PROGRESS()
		ManageRollingStart()
	ENDIF
	
	IF bDialogueDontletTheOtherEscape = TRUE
		IF bBlokeStunned = FALSE
		OR bSECONDBlokeStunned = FALSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF GET_GAME_TIMER() > iTimerDontLetTheOtherEscape + 1500       //We can come back for him... Don't let the other one get away!
					//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR", "MIN2_SURR_10", CONV_PRIORITY_LOW)
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_SURR_10", CONV_PRIORITY_LOW)	
						bDialogueDontletTheOtherEscape = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBloke2AttachedToCar = FALSE
		IF bBlokestunned = TRUE
		AND bSECONDBlokestunned = TRUE	
		AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
		AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
			JosefOnFoot()
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],TRUE)	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bpedImmigrantChase2Attached = FALSE
		IF bBlokestunned = TRUE
		AND bSECONDBlokestunned = TRUE	
			IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
			AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
				JoeOnFoot()
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK	
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlokestunned = FALSE
	OR bSECONDBlokestunned = FALSE
		UpdateVehiclePlayback()
	ELSE
		IF iSeqHintCam < 3
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			iSeqHintCam = 3
		ENDIF
	ENDIF
	
	OpenGates()
	Convo()
	UpdateAmbientCementFactory()
	
	IF bImmigrantHandsUp = TRUE
		IF bBloke2AttachedToCar = FALSE	
			ImmigrantGetInTruck()
		ELSE
			IF NOT bDialoguePickUpLastGuy
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_31")	
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,100)		
							IF IS_ENTITY_ALIVE(pedImmigrant[0])	
								ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
							ENDIF
							IF IS_ENTITY_ALIVE(pedImmigrant[1])	
								ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, pedImmigrant[1], "IMMIGRANTMALE2")
							ENDIF
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHATB2",CONV_PRIORITY_MEDIUM)  //Please, I have a family.
								bDialoguePickUpLastGuy = TRUE
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		Chase2JosefAi()
	ENDIF	
	
	IF bImmigrantChase2HandsUp = TRUE
		IF bpedImmigrantChase2Attached = FALSE
			ImmigrantChase2GetInTruck()
		ELSE
			IF NOT bDialoguePickUpLastGuy
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_31")	
						IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],100)		
							IF IS_ENTITY_ALIVE(pedImmigrantChase2)		
								ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, pedImmigrantChase2, "IMMIGRANTMALE2")
							ENDIF
							IF IS_ENTITY_ALIVE(pedImmigrant[0])	
								ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
							ENDIF
							IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHATB1",CONV_PRIORITY_MEDIUM)  //Whoa, boys. What happened to the land of the free, here?
								bDialoguePickUpLastGuy = TRUE
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		Chase2JoeAi()
	ENDIF
	
	IF IS_PED_UNINJURED(pedImmigrant[1])	
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[1])
		AND NOT IS_PED_RAGDOLL(pedImmigrant[1])	
		AND GET_ENTITY_HEALTH(pedImmigrant[1]) > 150
			SET_ENTITY_HEALTH(pedImmigrant[1],150)
		ENDIF
		IF bBlokestunned = TRUE
			IF bImmigrantHandsUp = FALSE
				//IF NOT IS_PED_RAGDOLL(pedImmigrant[i])
					IF GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(pedImmigrant[i],SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK
						CLEAR_PED_TASKS(pedImmigrant[i])
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_OK(vehChaseCar[i])	
			IF IS_PED_IN_VEHICLE(pedImmigrant[i],vehChaseCar[i])	
				IF DOES_BLIP_EXIST(blipImmigrant[i])	
					UPDATE_CHASE_BLIP(blipImmigrant[i],pedImmigrant[i],300,0.8)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
								IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])		
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
										IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],180)	
											IF iCatchUpLines < 3
												IF GET_GAME_TIMER()	> iTimerCatchUpLines + 7000
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CATCH2",CONV_PRIORITY_MEDIUM)  //Come on! Step on it!
															++iCatchUpLines
															iTimerCatchUpLines = GET_GAME_TIMER()
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(pedImmigrantChase2)	
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)
		AND NOT IS_PED_RAGDOLL(pedImmigrantChase2)	
		AND GET_ENTITY_HEALTH(pedImmigrantChase2) > 150
			SET_ENTITY_HEALTH(pedImmigrantChase2,150)
		ENDIF
		IF bSECONDBlokestunned = TRUE
			IF bImmigrantChase2HandsUp = FALSE
				//IF NOT IS_PED_RAGDOLL(pedImmigrantChase2)
					IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK
						CLEAR_PED_TASKS(pedImmigrantChase2)
					ENDIF
				//ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_OK(vehChaseCarChase2)	
			IF IS_PED_IN_VEHICLE(pedImmigrantChase2,vehChaseCarChase2)	
				IF DOES_BLIP_EXIST(blipImmigrantChase2)	
					UPDATE_CHASE_BLIP(blipImmigrantChase2,pedImmigrantChase2,300,0.8)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])		
								IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])				
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
										IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,180)	
											IF iCatchUpLines < 3
												IF GET_GAME_TIMER()	> iTimerCatchUpLines + 7000
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CATCH2",CONV_PRIORITY_MEDIUM)  //Come on! Step on it!
															++iCatchUpLines
															iTimerCatchUpLines = GET_GAME_TIMER()
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iSeqChase2Dialogue <> 5
	AND GET_GAME_TIMER() > iArrivedAtCementFactory + 20000	
		iSeqChase2Dialogue = 5
	ENDIF
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		IF iSeqChase2Dialogue = 0
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHAT1",CONV_PRIORITY_MEDIUM)  //That's an admission of guilt if ever I saw one!
					iSeqChase2Dialogue = GET_RANDOM_INT_IN_RANGE(1,5)
					//++iSeqChase2Dialogue
				ENDIF
			ENDIF
		ELIF iSeqChase2Dialogue = 1
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,50)
				AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],50)
					IF IS_ENTITY_ALIVE(pedImmigrant[0])	
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
					ENDIF
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHAT2",CONV_PRIORITY_MEDIUM)  //Who the hell are you guys?
						iSeqChase2Dialogue = 5
						//++iSeqChase2Dialogue
					ENDIF
				ENDIF
			ENDIF	
		ELIF iSeqChase2Dialogue = 2
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,50)
				AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],50)
					IF IS_ENTITY_ALIVE(pedImmigrant[0])	
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
					ENDIF
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHAT5",CONV_PRIORITY_MEDIUM)  //Look at them. Where's the moral code, huh?  
						iSeqChase2Dialogue = 5
						//++iSeqChase2Dialogue
					ENDIF
				ENDIF
			ENDIF	
		ELIF iSeqChase2Dialogue = 3
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,50)
				AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],50)
					IF IS_ENTITY_ALIVE(pedImmigrant[0])	
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
					ENDIF
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHAT4",CONV_PRIORITY_MEDIUM)  //All they want to do is work menial jobs for abysmal pay with no benefits.  
						iSeqChase2Dialogue = 5
						//++iSeqChase2Dialogue
					ENDIF
				ENDIF
			ENDIF	
		ELIF iSeqChase2Dialogue = 4
			IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrantChase2,50)
				AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedImmigrant[i],50)
					IF IS_ENTITY_ALIVE(pedImmigrant[0])	
						ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedImmigrant[0], "MANUEL")
					ENDIF
					IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_CHAT3",CONV_PRIORITY_MEDIUM)  //Haven't you got anything better to do?  
						iSeqChase2Dialogue = 5
						//++iSeqChase2Dialogue
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF			
	
	IF IS_PED_UNINJURED(pedImmigrant[1])
		IF bDisableAbortConvo
		AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_PED_RAGDOLL(pedImmigrant[1])	
			SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
			bDisableAbortConvo = FALSE
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(pedImmigrantChase2)
		IF bDisableAbortConvoPed2
		AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_PED_RAGDOLL(pedImmigrantChase2)	
			SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
			bDisableAbortConvoPed2 = FALSE
		ENDIF
	ENDIF
	
	IF bBloke2AttachedToCar = TRUE
	AND bpedImmigrantChase2Attached = TRUE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "Min2****Init exit vehicle****")
		#ENDIF
		SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE)
		bDisableAbortConvo = FALSE
		bDisableAbortConvoPed2 = FALSE
		iTimerJackTrev = GET_GAME_TIMER()
		iTimerJackTrev = iTimerJackTrev
		missionStage = MS_DRIVE_TO_END
	ENDIF

ENDPROC

/// PURPOSE: All immigrants captured, waiting for Joe and Josef to get back in the car
PROC MS_DriveToEnd()

	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		bLosingCops = FALSE
		bPrintLosingCops = FALSE
		Convo()
		IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0])	
		AND IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0])		
			SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[0],VEHICLELOCK_CANNOT_ENTER)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
			OR GET_GAME_TIMER()	> iTimerJackTrev + 30000//20000
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "Min2****Init pass drive off****")
					#ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 12.0, REPLAY_IMPORTANCE_LOWEST)
					
					SET_ROADS_IN_ANGLED_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<200,0,50>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],<<-200,0,-50>>),400,FALSE,TRUE)
					PassDriveOff()
					missionStage = MS_MISSION_PASSING
				ENDIF
			ELSE
				IF bThanks = FALSE	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "MIN2AU", "MIN2_JJ8", "MIN2_JJ8_1", CONV_PRIORITY_HIGH)   //Thanks for the help man, you're a true patriot.
						bThanks = TRUE
					ENDIF	
				ELSE	
					IF bPrint47 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							PRINT_NOW("MIN2_47",DEFAULT_GOD_TEXT_TIME,0)   //Exit the vehicle
							iTimerGetOutOfCarLines = GET_GAME_TIMER()
							bPrint47 = TRUE
						ENDIF
					ELSE
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("MIN2_47")	
							IF iGetOutOfCarLines < 6
								IF GET_GAME_TIMER()	> iTimerGetOutOfCarLines + 9000
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_BYE",CONV_PRIORITY_MEDIUM)    //Get out of the car, we'll take it from here.
											++iGetOutOfCarLines
											iTimerGetOutOfCarLines = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOSEF])	
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF])	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOSEF],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOSEF],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[ic_JOE])	
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE])	
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[ic_JOE],SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK		
					TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[ic_JOE],sRCLauncherDataLocal.vehID[0],DEFAULT_TIME_BEFORE_WARP_MINUTE_2,VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		bLosingCops = TRUE
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
			IF NOT DOES_BLIP_EXIST(blipPlayerCar)
				blipPlayerCar = CREATE_VEHICLE_BLIP(sRCLauncherDataLocal.vehID[0])
			ENDIF
		ELSE
			IF bPrintLosingCops = FALSE
				PRINT_NOW("MIN2_COPS",DEFAULT_GOD_TEXT_TIME,0)
				bPrintLosingCops = TRUE
			ENDIF
			SAFE_REMOVE_BLIP(blipPlayerCar)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Mission is passing, wait for the car to get away or time out
PROC MS_MissionPassing()
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		IF NOT bDialogueDriveOff
		AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0]) < 35.0		
			IF GET_GAME_TIMER() > iTimerPassDriveOff + 3000
				IF CREATE_CONVERSATION(s_conversation_peds, "MIN2AU","MIN2_DRIVOFF",CONV_PRIORITY_MEDIUM)    //We made Uncle Sam proud today!
					bDialogueDriveOff = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])	
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0]) > 150.0 //100.0					
		OR GET_GAME_TIMER() > iTimerPassDriveOff + 20000 //15000
			Script_Passed()
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Epand capsules on the immigrants while they get in the vehicle (to stop them clipping)
PROC EXPAND_PED_CAPSULES_WHILE_ENTERING_VEHICLES()
		
	IF IS_PED_UNINJURED(pedImmigrant[0])
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[0])
			IF GET_SCRIPT_TASK_STATUS(pedImmigrant[0],SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK	
				SET_PED_RESET_FLAG(pedImmigrant[0],PRF_ExpandPedCapsuleFromSkeleton,TRUE)
				CPRINTLN(DEBUG_MISSION, "MIN2 - Expand capsule - Manuel")
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(pedImmigrant[1])
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrant[1])
			IF GET_SCRIPT_TASK_STATUS(pedImmigrant[1],SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK	
				SET_PED_RESET_FLAG(pedImmigrant[1],PRF_ExpandPedCapsuleFromSkeleton,TRUE)
				CPRINTLN(DEBUG_MISSION, "MIN2 - Expand capsule - Immigrant 2")
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(pedImmigrantChase2)
		IF NOT IS_PED_IN_ANY_VEHICLE(pedImmigrantChase2)
			IF GET_SCRIPT_TASK_STATUS(pedImmigrantChase2,SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK	
				SET_PED_RESET_FLAG(pedImmigrantChase2,PRF_ExpandPedCapsuleFromSkeleton,TRUE)
				CPRINTLN(DEBUG_MISSION, "MIN2 - Expand capsule - Immigrant 3")
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF	
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
	  	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_MINUTE_2(sRCLauncherDataLocal)
	  		WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOE, TREVOR_BOOK, FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSEF, TREVOR_BOOK, FALSE)
	
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[0], VEHICLELOCK_UNLOCKED)
	ENDIF

	Initialise()

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_AAW")
					
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF
			
		IF missionStage <> MS_SETUP_MISSION
		AND missionStage <> MS_INTRO_MOCAP
			IF missionStage <> MS_MISSION_FAILING	
				CheckForFail()
			ENDIF
			IF NOT JOSEF_AND_JOE_LEFT_BEHIND()
				ManageBlipsAndObjectives()
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				SAFE_REMOVE_BLIP(blipLocate[i])
			ENDIF
		ENDIF

		RESOLVE_IDLES()
		
		DO_VEHICLE_WEAPON_SWAP()
		/*
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[ic_JOSEF])
		AND	IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRCLauncherDataLocal.vehID[0],GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0],GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "seat_pside_r"))))
			PRINTNL()
			//<< 0.447884, -0.807867, 0.211781 >> //?
		ENDIF
		*/
		
		EXPAND_PED_CAPSULES_WHILE_ENTERING_VEHICLES()
		
		SWITCH missionStage
			
			CASE MS_INTRO_MOCAP
				MS_IntroMocap()
			BREAK
			
			CASE MS_SETUP_MISSION
				MS_SetupMission()
			BREAK
			
			CASE MS_SETUP_CHASE1
				MS_SetupChase1()
			BREAK
			
			CASE MS_UPDATE_CHASE1
				MS_UpdateChase1()
			BREAK
			
			CASE MS_IMMIGRANT1_APPREHENDED
				MS_Immigrant1Apprehended()
			BREAK

			CASE MS_SETUP_CHASE2
				MS_SetupChase2()
			BREAK
			
			CASE MS_CEMENT_FACTORY
				MS_CementFactory()
			BREAK
			
			CASE MS_UPDATE_CHASE2
				MS_UpdateChase2()
			BREAK
			
			CASE MS_DRIVE_TO_END
				MS_DriveToEnd()
			BREAK	
			
			CASE MS_MISSION_PASSING
				MS_MissionPassing()
			BREAK
			
			CASE MS_MISSION_FAILING
				FailWait()
			BREAK
			
		ENDSWITCH
		
		// Check debug completion/failure
		IF missionStage <> MS_MISSION_FAILING	
			DEBUG_Check_Debug_Keys()
			DEBUG_Check_Skips()
		ENDIF
	
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

