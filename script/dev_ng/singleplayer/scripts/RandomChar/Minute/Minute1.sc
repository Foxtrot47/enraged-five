// MISSION NAME : Minute1.sc
// AUTHOR : Kev Edwards
// DESCRIPTION : Help minutemen round up some mariachi band members

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "dialogue_public.sch"
USING "script_ped.sch"
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch" 
USING "initial_scenes_Minute.sch"
USING "drunk_public.sch"
USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF
USING "commands_event.sch" // Required to use REMOVE_ALL_SHOCKING_EVENTS
USING "emergency_call.sch" // Required to use SUPPRESS_EMERGENCY_CALLS
USING "commands_recording.sch"

g_structRCScriptArgs sRCLauncherDataLocal

CONST_INT CP_GO_TO_BAR 0 // Checkpoints
CONST_INT CP_GO_IN_BAR 1
CONST_INT CP_CAR_CHASE 2
CONST_INT CP_MISSION_PASSED 3 // Only used for shitskips

CONST_INT Z_SKIP_INTRO 0 // Z Skips
CONST_INT Z_SKIP_GO_TO_BAR 1
CONST_INT Z_SKIP_GO_INSIDE_BAR 2
CONST_INT Z_SKIP_CHASE_CAR 3
CONST_INT Z_SKIP_STUN_MARIACHI 4
CONST_INT Z_SKIP_MISSION_PASSED 5

ENUM MIN1_MISSION_STATE
	stateNULL,
	stateIntro,
	stateGoToBar,
	stateGoInsideBar,
	stateGoOutsideBar,
	stateChaseMariachiCar,
	stateStunMariachiPeds,
	stateLeaveArea,
	stateMissionPassed,
	stateFailed,
	NUM_MISSION_STATES
ENDENUM
MIN1_MISSION_STATE missionState = stateIntro // Stores the current MIN1_MISSION_STATE
MIN1_MISSION_STATE missionStateSkip = stateNULL // Used if needing to change to a MIN1_MISSION_STATE out of sequence

ENUM MIN1_STATE_MACHINE
	MIN1_STATE_MACHINE_SETUP,
	MIN1_STATE_MACHINE_LOOP,
	MIN1_STATE_MACHINE_CLEANUP
ENDENUM
MIN1_STATE_MACHINE missionStateMachine = MIN1_STATE_MACHINE_SETUP

ENUM FAILED_REASON
	FAILED_DEFAULT = 0,
	FAILED_JOE_DIED,
	FAILED_JOSEF_DIED,
	FAILED_PATROL_VEHICLE_WRECKED,
	FAILED_MARIACHI_DIED,
	FAILED_MARIACHI_INJURED,
	FAILED_ABANDONED_BUDDIES,
	FAILED_MARIACHI_ESCAPED,
	FAILED_JOE_INJURED,
	FAILED_JOSEF_INJURED,
	FAILED_ALERTED_COPS,
	FAILED_THREW_EXPLOSIVES,
	FAILED_PATROL_VEHICLE_ATTACKED,
	FAILED_ABANDONED_JOE
ENDENUM
FAILED_REASON failReason
STRING failString

ENUM PATROL_VEHICLE_STATUS
	PV_STATUS_PLAYER_NOT_INSIDE,
	PV_STATUS_LOSE_WANTED_LEVEL,
	PV_STATUS_GO_TO_DESTINATION
ENDENUM
PATROL_VEHICLE_STATUS pvStatus

ENUM MARIACHI_STATUS
	MARIACHI_EXITING_HIS_VEHICLE,
	MARIACHI_WAITING_TO_BE_STUNNED,
	MARIACHI_HAS_BEEN_STUNNED,
	MARIACHI_AFTER_STUNNED,
	MARIACHI_GOING_TO_PATROL_VEHICLE,
	MARIACHI_ENTERING_PATROL_VEHICLE,
	MARIACHI_SAT_IN_PATROL_VEHICLE
ENDENUM

ENUM BUDDY_STATUS
	BUDDY_WAITING_TO_GET_OUT,
	BUDDY_GOING_TO_MARIACHI_PED,
	BUDDY_AIMING_AT_MARIACHI_PED,
	BUDDY_TRYING_TO_ENTER_PATROL_VEHICLE,
	BUDDY_ENTERING_PATROL_VEHICLE,
	BUDDY_SAT_IN_PATROL_VEHICLE
ENDENUM

BOOL bZSkipping = FALSE // Changes to TRUE if z skipping to prevent subsequent mission state changes etc

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 5
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintToTTY = TRUE
	INT iDebugSkipTime // Prevents skipping instantaneously
#ENDIF

VECTOR vPlayerCarRespot = <<323.6060, 3415.3496, 35.6612>>
CONST_FLOAT fPlayerCarRespot 254.7861

CONST_INT NUM_PED_MODELS 2
MODEL_NAMES modelPed[NUM_PED_MODELS]
CONST_INT MIN1_MODEL_MARIACHI_PED 0
CONST_INT MIN1_MODEL_DRUNK_PED_1 1

INT iCutsceneStage
INT iLeadInTimer
SEQUENCE_INDEX seqTrevorLeadIn

VEHICLE_INDEX vehCamperVan

structPedsForConversation sConversation
BOOL bStartedArrivedAtBarConversation
BOOL bStartedInBarConversation
BOOL bStartedDrunkPedConversation
BOOL bStartedBumpDrunkConversation
BOOL bStartedGetOutHereConversation
BOOL bStartedOutsideBarConversation
BOOL bStartedChaseVanConversation
BOOL bStartedOffRoadConversation
BOOL bStartedCrashedConversation
BOOL bStartedStunThemConversation
BOOL bStartedLeaveAreaConversation
BOOL bStartedCyaConversation
BOOL bStartedDriveToBarConversation
BOOL bFinishedDriveToBarConversation
BOOL bStartedStopDriveConversation
BOOL bStartedWrongWayConversation
BOOL bStartedAlmostThereConversation
BOOL bStartedBringVehicleConversation
BOOL bStartedNoJoeConversation
BOOL bStartedGoBackForJoeConversation
BOOL bStartedFleeConversation
INT iWaitConversationsPlayed
INT iGetInConversationsPlayed
INT iStoppedConversationsPlayed
BOOL bPatrolVehicleStoppedDuringChase
INT iFarBehindConversationsPlayed
BOOL bPlayedFarBehindConversation
INT iExitedVehicleConversationsPlayed
INT iGetBackInVehicleConversationsPlayed
INT iGotBackInVehicleConversationsPlayed
BOOL bAllowPlayExitedVehicleConversation
BOOL bAllowPlayGetBackInVehicleConversation
BOOL bAllowPlayGotBackInVehicleConversation
BOOL bStartedSingingStream

CONST_INT NUM_CRASH_CONVERSATIONS 4
VECTOR vCrashConversationPos[NUM_CRASH_CONVERSATIONS]
BOOL bStartedCrashConversation[NUM_CRASH_CONVERSATIONS]
INT iDoneCrashConversations

TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel

MODEL_NAMES modelPatrolVehicle
VEHICLE_INDEX vehPatrolVehicle
BLIP_INDEX blipPatrolVehicle
INT iNumToldToGetInPatrolVehicle
INT iNumToldToGoToDestination
BOOL bDisplayObjective
BOOL bPatrolVehicleDrivingAway
BOOL bDoneRollingStart
BOOL bPlayerIsCurrentlyInPatrolVehicle
FLOAT fWrongWayDistance

MODEL_NAMES modelMariachiCar
VEHICLE_INDEX vehMariachiCar
VECTOR vMariachiCar = << 1996.5011, 3063.1003, 46.7716 >>
FLOAT fMariachiCar = 56.146961
BOOL bUsePlaybackAIForMariachiCar
INT iRequiredHealth
FLOAT fCurrentPositionInRecording

BLIP_INDEX blipDestination
VECTOR vBarInside = << 1989.8622, 3047.6040, 46.21511 >>
VECTOR vBarOutside = << 1990.9784, 3054.2000, 46.2149 >>
INTERIOR_INSTANCE_INDEX interiorBar
BOOL bIsInteriorPinned

STRUCT COMPANION
	PED_INDEX ped
	VEHICLE_SEAT seatStart
	VEHICLE_SEAT seatEnd
	BUDDY_STATUS status
	INT iInsultTimer
	INT iNumInsultsGiven
	INT iDelayBeforeNextInsult
ENDSTRUCT
CONST_INT NUM_BUDDIES 2
COMPANION pedBuddy[NUM_BUDDIES]
CONST_INT JOE 0
CONST_INT JOSEF 1
BLIP_INDEX blipJoe

CONST_INT NUM_MARIACHI_PEDS 2
PED_INDEX pedMariachi[NUM_MARIACHI_PEDS]
BLIP_INDEX blipMariachi[NUM_MARIACHI_PEDS]
VEHICLE_SEAT seatMariachi[NUM_MARIACHI_PEDS]
MARIACHI_STATUS statusMariachi[NUM_MARIACHI_PEDS]
BOOL bPlayedStunnedConversation[NUM_MARIACHI_PEDS]
BOOL bPlayedSoberConversation[NUM_MARIACHI_PEDS]
BOOL bCheckForTazeredAgain[NUM_MARIACHI_PEDS]
BOOL bHasBeenTazeredAgain[NUM_MARIACHI_PEDS]
FLOAT fSeekOffsetFromPatrolVehicle[NUM_MARIACHI_PEDS]
CONST_INT NUM_MARIACHI_DRUNK_CONVERSATIONS 4
CONST_INT NUM_MARIACHI_BUMP_CONVERSATIONS 3
CONST_INT NUM_MARIACHI_AIM_CONVERSATIONS 2
CONST_INT NUM_MARIACHI_SHOCK_CONVERSATIONS 1// There are 3 conversations for each mariachi but we only want to play 1 of each
CONST_INT NUM_MARIACHI_WALK_CONVERSATIONS 2
INT iMariachiConversationTimer[NUM_MARIACHI_PEDS]
INT iMariachiConversationDelay[NUM_MARIACHI_PEDS]
INT iNumPlayedDrunkConversations[NUM_MARIACHI_PEDS]
INT iNumPlayedBumpConversations[NUM_MARIACHI_PEDS]
INT iNumPlayedAimConversations[NUM_MARIACHI_PEDS]
INT iNumPlayedShockConversations[NUM_MARIACHI_PEDS]
INT iNumPlayedWalkConversations[NUM_MARIACHI_PEDS]
INT iMariachiExitVehicleTimer[NUM_MARIACHI_PEDS]
INT iPlayedStunnedConversation
CONST_INT NUM_HURRY_STUN_CONVERSATIONS 5
TEXT_LABEL_15 sHurryStunConversation[NUM_HURRY_STUN_CONVERSATIONS]
INT iHurryStunConversationsPlayed
INT iTimesStunnedMoreThanNecessary
INT iCurrentTimesStunnedDialogue
CONST_INT NUM_TIMES_STUNNED_DIALOGUE 3
STRING sTimesStunnedDialogueString[NUM_TIMES_STUNNED_DIALOGUE]
CONST_INT NUM_SHOT_VEHICLE_DIALOGUE 2
INT iCurrentShotVehicleDialogue
STRING sShotVehicleDialogueString[NUM_SHOT_VEHICLE_DIALOGUE]
VECTOR vOpenGroundWanderArea = <<2698.87769, 4110.84863, 42.11631>>

PED_INDEX pedWalkOutBar
VECTOR vWalkOutBarPed = << 1989.4276, 3051.9133, 46.1256 >>
CONST_FLOAT fWalkOutBarPed 332.6105
BOOL bPedGoneOutDoor

PED_INDEX pedJanet
BOOL bDoneJanetTalk

FLOAT fFailDist

INT iSingingDialogueTimer
INT iChaseDialogueTimer
INT iStunGunDialogueTimer
INT iNotStunnedDialogueTimer
INT iRollingStartTimer
INT iLookAroundTimer
INT iWaitConversationTimer
INT iMariachiCarTyresBurstTimer
INT iPatrolVehicleTyresBurstTimer

CONST_INT NUM_CHASE_CONVERSATIONS 7
BOOL bDoneChaseConversation[NUM_CHASE_CONVERSATIONS]
TEXT_LABEL_15 sChaseConversation[NUM_CHASE_CONVERSATIONS]
INT iDoneChaseConversations

REL_GROUP_HASH relGroupFriendly

SEQUENCE_INDEX fleeSequence
SEQUENCE_INDEX drunkSequence

VEHICLE_INDEX vehForReplay

INT iWantedFailedTimer

INT iDisableReplayCameraTimer	//Fix for bug 2258455

/// PURPOSE:
///    Used in every MIN1_STATE_MACHINE_SETUP. Advances state machine to MIN1_STATE_MACHINE_LOOP.
PROC MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Initialising ", s_text) ENDIF #ENDIF
	bZSkipping = FALSE
	#IF IS_DEBUG_BUILD iDebugSkipTime = GET_GAME_TIMER() #ENDIF
	missionStateMachine = MIN1_STATE_MACHINE_LOOP
ENDPROC

/// PURPOSE:
///    Used in every MIN1_STATE_MACHINE_CLEANUP. Advances state machine to MIN1_STATE_MACHINE_SETUP.
PROC MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Cleaning up ", s_text) ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	IF missionStateSkip = stateNULL
		missionState = INT_TO_ENUM(MIN1_MISSION_STATE, ENUM_TO_INT(missionState) + 1)
	ELSE
		missionState = missionStateSkip
	ENDIF
	missionStateSkip = stateNULL
	missionStateMachine = MIN1_STATE_MACHINE_SETUP
ENDPROC

/// PURPOSE:
///    Models required for a mission state are all loaded when the mission state inits, but this is a safety check to ensure a model is loaded before subsequently creating an entity using it.
PROC MIN1_ENSURE_MODEL_IS_LOADED(MODEL_NAMES e_model)
	REQUEST_MODEL(e_model)
	IF NOT HAS_MODEL_LOADED(e_model)
		WHILE NOT HAS_MODEL_LOADED(e_model)
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely creates a vehicle.
PROC MIN1_CREATE_VEHICLE(VEHICLE_INDEX &veh_to_create, MODEL_NAMES e_model, VECTOR v_pos, FLOAT f_heading = 0.0, FLOAT fDirt = 0.0)
	IF NOT DOES_ENTITY_EXIST(veh_to_create)
		MIN1_ENSURE_MODEL_IS_LOADED(e_model)
		veh_to_create = CREATE_VEHICLE(e_model, v_pos, f_heading)
		SET_VEHICLE_DIRT_LEVEL(veh_to_create, fDirt)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely spawn a ped.
PROC MIN1_CREATE_PED(PED_INDEX &ped_to_create, MODEL_NAMES e_model, VECTOR v_pos, FLOAT f_heading = 0.0)
	IF NOT DOES_ENTITY_EXIST(ped_to_create)
		MIN1_ENSURE_MODEL_IS_LOADED(e_model)
		ped_to_create = CREATE_PED(PEDTYPE_MISSION, e_model, v_pos, f_heading)
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped_to_create)
		REMOVE_ALL_PED_WEAPONS(ped_to_create)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely spawn a ped inside a vehicle.
PROC MIN1_CREATE_PED_IN_VEHICLE(VEHICLE_INDEX veh, PED_INDEX &ped_to_create, MODEL_NAMES e_model, VEHICLE_SEAT seat)
	IF NOT DOES_ENTITY_EXIST(ped_to_create)
	AND DOES_ENTITY_EXIST(veh)
		MIN1_ENSURE_MODEL_IS_LOADED(e_model)
		ped_to_create = CREATE_PED_INSIDE_VEHICLE(veh, PEDTYPE_MISSION, e_model, seat)
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped_to_create)
		REMOVE_ALL_PED_WEAPONS(ped_to_create)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all blips in this mission.
PROC MIN1_REMOVE_ALL_BLIPS()	
	SAFE_REMOVE_BLIP(blipPatrolVehicle)
	SAFE_REMOVE_BLIP(blipDestination)
	SAFE_REMOVE_BLIP(blipMariachi[0])
	SAFE_REMOVE_BLIP(blipMariachi[1])
	SAFE_REMOVE_BLIP(blipJoe)
ENDPROC

/// PURPOSE:
///    Safely remove all vehicles in this mission.
PROC MIN1_REMOVE_ALL_VEHICLES(BOOL b_delete = FALSE)
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		SET_ENTITY_LOAD_COLLISION_FLAG(vehMariachiCar, FALSE)
	ENDIF
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehPatrolVehicle)
		REMOVE_VEHICLE_STUCK_CHECK(vehPatrolVehicle)
	ENDIF
	IF b_delete = TRUE
		SAFE_DELETE_VEHICLE(vehMariachiCar)
		SAFE_DELETE_VEHICLE(vehCamperVan)
		SAFE_RELEASE_VEHICLE(vehPatrolVehicle) // Prevents the patrol vehicle being deleted at the end of the mission
		SAFE_DELETE_VEHICLE(vehForReplay)
	ELSE
		SAFE_RELEASE_VEHICLE(vehMariachiCar)
		SAFE_RELEASE_VEHICLE(vehCamperVan)
		SAFE_RELEASE_VEHICLE(vehPatrolVehicle)
		SAFE_RELEASE_VEHICLE(vehForReplay)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all peds in this mission.
PROC MIN1_REMOVE_ALL_PEDS(BOOL b_delete = FALSE, BOOL b_terminate_mission = TRUE)
	IF b_delete = TRUE
		IF b_terminate_mission = TRUE
			SAFE_DELETE_PED(pedBuddy[JOE].ped)
			SAFE_DELETE_PED(pedBuddy[JOSEF].ped)
		ENDIF
		SAFE_DELETE_PED(pedWalkOutBar)
		SAFE_DELETE_PED(pedMariachi[0])
		SAFE_DELETE_PED(pedMariachi[1])
		SAFE_DELETE_PED(pedJanet)
	ELSE
		IF b_terminate_mission = TRUE
			SAFE_RELEASE_PED(pedBuddy[JOE].ped)
			SAFE_RELEASE_PED(pedBuddy[JOSEF].ped)
		ENDIF
		SAFE_RELEASE_PED(pedWalkOutBar)
		SAFE_RELEASE_PED(pedMariachi[0])
		SAFE_RELEASE_PED(pedMariachi[1])
		SAFE_RELEASE_PED(pedJanet)
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures props that the Mariachi car smashes through have their full physics loaded. Fixes B*578109.
PROC MIN1_SET_PROPS_TO_USE_FULL_PHYSICS(BOOL b_enable)
	IF b_enable = TRUE
		CREATE_FORCED_OBJECT(<<2142.74, 3355.45, 44.44>>,5,prop_fnc_farm_01e,TRUE)
		CREATE_FORCED_OBJECT(<<2146.07, 3351.57, 44.50>>,5,prop_fnclink_01d,TRUE)
		CREATE_FORCED_OBJECT(<<2067.85, 3266.79, 44.45>>,5,prop_sign_road_03a,TRUE) // B*1098655
	ELSE
		REMOVE_FORCED_OBJECT(<<2142.74, 3355.45, 44.44>>,5,prop_fnc_farm_01e)
		REMOVE_FORCED_OBJECT(<<2146.07, 3351.57, 44.50>>,5,prop_fnclink_01d)
		REMOVE_FORCED_OBJECT(<<2067.85, 3266.79, 44.45>>,5,prop_sign_road_03a)
	ENDIF
ENDPROC

/// PURPOSE:
///    Switches off the road nodes along the road at the start of the car chase.
PROC MIN1_CHANGE_ROAD_NODES_FOR_CAR_CHASE(BOOL b_set_on)
	IF b_set_on = TRUE
		SET_ROADS_BACK_TO_ORIGINAL(<<1906,3000,0>>, <<2150,3500,100>>)
	ELSE
		SET_ROADS_IN_AREA(<<1906,3000,0>>, <<2150,3500,100>>, FALSE)
		CLEAR_AREA_OF_VEHICLES(<<1995.33972, 3061.05371, 46.04894>>, 10, FALSE, FALSE, TRUE, TRUE)
		CLEAR_AREA_OF_VEHICLES(<<1958.77063, 3090.40576, 45.94596>>, 40, FALSE, FALSE, TRUE, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Set road nodes off at start of car chase") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up everything conceivably setup in this mission.
/// PARAMS:
///    b_delete_entities - TRUE if should delete entites, FALSE if should remove entities for elegant cleanup.
///    b_terminate_mission - TRUE if cleaning up before terminating, FALSE if cleaning up before a debug skip.
PROC MIN1_MISSION_CLEANUP(BOOL b_delete_entities = FALSE, BOOL b_terminate_mission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Cleaning up mission...") ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	REMOVE_PED_FOR_DIALOGUE(sConversation, 2) // Trevor
	REMOVE_PED_FOR_DIALOGUE(sConversation, 3) // Joe
	REMOVE_PED_FOR_DIALOGUE(sConversation, 4) // Josef
	REMOVE_PED_FOR_DIALOGUE(sConversation, 5) // Drunken hillbilly
	REMOVE_PED_FOR_DIALOGUE(sConversation, 6) // Mariachi van ped
	REMOVE_PED_FOR_DIALOGUE(sConversation, 7) // Mariachi van ped
	REMOVE_PED_FOR_DIALOGUE(sConversation, 8) // Bar woman
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		SET_LAST_DRIVEN_VEHICLE(vehMariachiCar) // B*1423496 Make sure the mariachi car doesn't get removed
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehMariachiCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar)
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(vehPatrolVehicle) // Re-enable damage when we're going to clean up the mission
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehPatrolVehicle, FALSE)
		SET_ENTITY_PROOFS(vehPatrolVehicle, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	MIN1_REMOVE_ALL_BLIPS()
	MIN1_REMOVE_ALL_PEDS(b_delete_entities)
	MIN1_REMOVE_ALL_VEHICLES(b_delete_entities)
	REMOVE_VEHICLE_RECORDING(500, "Min1_Van")
	INT i = 0
	REPEAT NUM_PED_MODELS i
		SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[i])
	ENDREPEAT
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelMariachiCar, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelPatrolVehicle, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelMariachiCar)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPatrolVehicle)
	UNPIN_INTERIOR(interiorBar)
	MIN1_SET_PROPS_TO_USE_FULL_PHYSICS(FALSE)
	MIN1_CHANGE_ROAD_NODES_FOR_CAR_CHASE(TRUE)
	REMOVE_ANIM_DICT("missminuteman_1ig_1")
	TRIGGER_MUSIC_EVENT("MM1_STOP")
	IF IS_AUDIO_SCENE_ACTIVE("MINUTE_01_SCENE")
		STOP_AUDIO_SCENE("MINUTE_01_SCENE")
	ENDIF
	REMOVE_VEHICLE_ASSET(modelPatrolVehicle)
	REMOVE_ANIM_DICT("move_m@drunk@verydrunk")
	REMOVE_ANIM_SET("MOVE_M@BAIL_BOND_NOT_TAZERED")
	REMOVE_ANIM_SET("MOVE_M@BAIL_BOND_TAZERED")
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	STOP_STREAM() // Mariachi singing
	IF b_terminate_mission = TRUE
		SET_CREATE_RANDOM_COPS(TRUE)
		SET_CREATE_RANDOM_COPS_ON_SCENARIOS(TRUE)
		RELEASE_SUPPRESSED_EMERGENCY_CALLS() // B*1472113 - Prevent player calling the cops
		SET_RANDOM_TRAINS(TRUE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<322,3409,35>>-<<40,40,40>>, <<322,3409,35>>+<<40,40,40>>, TRUE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 2710.43, 4141.74, 42.92 >> - <<100.0, 100.0, 100.0>>, << 2710.43, 4141.74, 42.92 >> + <<100.0, 100.0, 100.0>>, TRUE) // B*2082981
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
				DELETE_WIDGET_GROUP(widgetGroup)
			ENDIF
		#ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Cleaned up mission") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Standard RC mission cleanup function.
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	IF (Random_Character_Cleanup_If_Triggered())
		MIN1_MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Standard RC mission passed function.
PROC Script_Passed()
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_MICHAEL, WEAPONTYPE_STUNGUN, TRUE, TRUE) // See B*1138740
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_FRANKLIN, WEAPONTYPE_STUNGUN, TRUE, TRUE)
	SET_PLAYER_PED_WEAPON_UNLOCKED(CHAR_TREVOR, WEAPONTYPE_STUNGUN, TRUE, TRUE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOE, TREVOR_BOOK)
	WAIT(0)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSEF, TREVOR_BOOK)
	CREDIT_BANK_ACCOUNT(CHAR_TREVOR, BAAC_UNLOGGED_SMALL_ACTION, 500) // See B*1207293 // See B*1341071
	Random_Character_Passed(CP_RAND_C_MIN1)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Mission failed function.
PROC MIN1_MISSION_FAILED(FAILED_REASON reason = FAILED_DEFAULT)
	IF bZSkipping = FALSE
		failReason = reason
		missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
		missionStateSkip = stateFailed
	ENDIF
ENDPROC

/// PURPOSE:
///    Puts the desired ped into a group friendly to the player.
PROC MIN1_SET_PED_AS_PLAYER_FRIEND(PED_INDEX ped_to_set_as_friend)
	IF IS_ENTITY_ALIVE(ped_to_set_as_friend)
		relGroupFriendly = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(ped_to_set_as_friend, relGroupFriendly)
		SET_PED_CONFIG_FLAG(ped_to_set_as_friend, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set up mission prior to going into any mission states.
PROC MIN1_MISSION_SETUP()
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Starting mission setup...") ENDIF #ENDIF
	failReason = FAILED_DEFAULT
	REQUEST_ADDITIONAL_TEXT("MIN1", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Intro - mmb_1_rcm"
		mSkipMenu[Z_SKIP_GO_TO_BAR].sTxtLabel = "Go to bar"
		mSkipMenu[Z_SKIP_GO_INSIDE_BAR].sTxtLabel = "Enter bar"
		mSkipMenu[Z_SKIP_CHASE_CAR].sTxtLabel = "Car chase"
		mSkipMenu[Z_SKIP_STUN_MARIACHI].sTxtLabel = "Stun Mariachi"
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup = START_WIDGET_GROUP("Minute 1  widgets")
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)	
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	modelMariachiCar = TORNADO4
	modelPatrolVehicle = PRANGER
	modelPed[MIN1_MODEL_MARIACHI_PED] = S_M_M_MARIACHI_01
	modelPed[MIN1_MODEL_DRUNK_PED_1] = A_M_M_HILLBILLY_01
	pedBuddy[JOE].seatStart = VS_FRONT_RIGHT
	pedBuddy[JOSEF].seatStart = VS_BACK_RIGHT
	pedBuddy[JOE].seatEnd = VS_DRIVER
	pedBuddy[JOSEF].seatEnd = VS_FRONT_RIGHT
	interiorBar = GET_INTERIOR_AT_COORDS(vBarInside)
	iCurrentShotVehicleDialogue = 0
	sShotVehicleDialogueString[0] = "MIN1_SHOOT1"
	sShotVehicleDialogueString[1] = "MIN1_SHOOT2"
	vCrashConversationPos[0] = <<2212.9785, 3478.3857, 47.9132>>
	vCrashConversationPos[1] = <<2268.7229, 3601.1167, 46.2687>>
	vCrashConversationPos[2] = <<2392.1316, 3710.2808, 48.0988>>
	vCrashConversationPos[3] = <<2632.2820, 3854.4854, 64.1631>>
	tSavedConversationRoot = ""
	iPatrolVehicleTyresBurstTimer = GET_GAME_TIMER()
	REQUEST_VEHICLE_ASSET(modelPatrolVehicle) // B*1037225 - Prestream vehicle anims in case player exits car after the intro mocap
	SUPPRESS_EMERGENCY_CALLS() // B*1472113 - Prevent player calling the cops
	SET_CREATE_RANDOM_COPS(FALSE)
	SET_CREATE_RANDOM_COPS_ON_SCENARIOS(FALSE)
	//IF IS_REPEAT_PLAY_ACTIVE() // Remove the stun gun regardless
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN) // B*1351410
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Player already had a stun gun so removed it") ENDIF #ENDIF
	ENDIF
	iWantedFailedTimer = -1
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 2710.43, 4141.74, 42.92 >> - <<100.0, 100.0, 100.0>>, << 2710.43, 4141.74, 42.92 >> + <<100.0, 100.0, 100.0>>, FALSE) // B*2082981
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished mission setup") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Loads the assets for the required mission stage.
PROC MIN1_LOAD_ASSETS_FOR_MISSION_STATE(MIN1_MISSION_STATE current_state)
SWITCH current_state
		CASE stateIntro
			REQUEST_MODEL(modelPatrolVehicle)
		BREAK
		CASE stateGoInsideBar
			REQUEST_MODEL(modelPed[MIN1_MODEL_DRUNK_PED_1])
		BREAK
		CASE stateGoOutsideBar
			REQUEST_MODEL(modelMariachiCar)
			REQUEST_MODEL(modelPed[MIN1_MODEL_MARIACHI_PED])
			REQUEST_VEHICLE_RECORDING(500, "Min1_Van")
			REQUEST_VEHICLE_RECORDING(500, "Min1_PV")
			REQUEST_ANIM_DICT("missminuteman_1ig_1")
		BREAK
		CASE stateStunMariachiPeds
			REQUEST_ANIM_DICT("missminuteman_1ig_2")
			REQUEST_ANIM_SET("MOVE_M@BAIL_BOND_NOT_TAZERED")
			REQUEST_ANIM_SET("MOVE_M@BAIL_BOND_TAZERED")			
		BREAK
		CASE stateMissionPassed
			REQUEST_MODEL(modelMariachiCar)
		BREAK
	ENDSWITCH
	BOOL b_assets_loaded = FALSE
	WHILE b_assets_loaded = FALSE
		SWITCH current_state
			CASE stateIntro
				IF HAS_MODEL_LOADED(modelPatrolVehicle)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(modelPatrolVehicle, TRUE)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateGoInsideBar
				IF HAS_MODEL_LOADED(modelPed[MIN1_MODEL_DRUNK_PED_1])
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateGoOutsideBar
				IF HAS_MODEL_LOADED(modelMariachiCar)
				AND HAS_MODEL_LOADED(modelPed[MIN1_MODEL_MARIACHI_PED])
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "Min1_Van")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "Min1_PV")
				AND HAS_ANIM_DICT_LOADED("missminuteman_1ig_1")
					SET_VEHICLE_MODEL_IS_SUPPRESSED(modelMariachiCar, TRUE)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateStunMariachiPeds
				IF HAS_ANIM_DICT_LOADED("missminuteman_1ig_2")
				AND HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_NOT_TAZERED")
				AND HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_TAZERED")
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateMissionPassed
				IF HAS_MODEL_LOADED(modelMariachiCar)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Loading assets...") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Assets loaded") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Teleports the patrol vehicle for a debug skip, and ensures Trevor, Joe and Josef are sat inside it.
/// PARAMS:
///    b_recreate - TRUE if we need to delete and recreate the patrol vehicle.
///    b_set_rotation - TRUE if starting at the car chase so the car's rotation needs setting for the rolling start car recording, FALSE if simply create the patrol vehicle at the pos and heading.
PROC MIN1_SETUP_PATROL_VEHICLE(VECTOR v_spawn_pos, FLOAT f_spawn_pos, BOOL b_set_for_rolling_start = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Starting setting up patrol vehicle...") ENDIF #ENDIF
	IF NOT IS_ENTITY_ALIVE(vehPatrolVehicle)
		MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateIntro)
		MIN1_CREATE_VEHICLE(vehPatrolVehicle, modelPatrolVehicle, v_spawn_pos, f_spawn_pos, 10)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelPatrolVehicle)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Patrol vehicle didn't exist so created it") ENDIF #ENDIF
	ENDIF
	IF NOT IS_ENTITY_ALIVE(pedBuddy[JOE].ped)
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
		WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedBuddy[JOE].ped, CHAR_JOE, vehPatrolVehicle, pedBuddy[JOE].seatStart)
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Joe didn't exist so created him") ENDIF #ENDIF
	ENDIF
	IF NOT IS_ENTITY_ALIVE(pedBuddy[JOSEF].ped)
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
		WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedBuddy[JOSEF].ped, CHAR_JOSEF, vehPatrolVehicle, pedBuddy[JOSEF].seatStart)
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Josef didn't exist so created him") ENDIF #ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(vehPatrolVehicle, TRUE, TRUE)
		IF b_set_for_rolling_start = TRUE
			START_PLAYBACK_RECORDED_VEHICLE(vehPatrolVehicle, 500, "Min1_PV")
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPatrolVehicle)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPatrolVehicle, 1500)
		ELSE
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPatrolVehicle)
		ENDIF
		SET_VEHICLE_AS_RESTRICTED(vehPatrolVehicle, 0)
		SET_VEHICLE_ALARM(vehPatrolVehicle, FALSE) 
		SET_VEHICLE_DOORS_LOCKED(vehPatrolVehicle, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_ALARM(vehPatrolVehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(vehPatrolVehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(vehPatrolVehicle, FALSE)
		SET_DISABLE_PRETEND_OCCUPANTS(vehPatrolVehicle, TRUE)
		SET_ENTITY_LOD_DIST(vehPatrolVehicle, 200)
		SET_VEHICLE_DIRT_LEVEL(vehPatrolVehicle, 10)
		SET_VEHICLE_STRONG(vehPatrolVehicle, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehPatrolVehicle, TRUE)
		INT i = 0
		REPEAT NUM_BUDDIES i
			IF IS_ENTITY_ALIVE(pedBuddy[i].ped)
				IF i = 1 // Setting a hat on Joe causes B*1509088
					SET_PED_PROP_INDEX(pedBuddy[i].ped, ANCHOR_HEAD, 0)
				ENDIF
				SET_PED_PROP_INDEX(pedBuddy[i].ped, ANCHOR_EYES, 0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBuddy[i].ped, TRUE)
				IF NOT IS_PED_IN_VEHICLE(pedBuddy[i].ped, vehPatrolVehicle)
					FREEZE_ENTITY_POSITION(pedBuddy[i].ped, FALSE) // B*886344
					SET_PED_INTO_VEHICLE(pedBuddy[i].ped, vehPatrolVehicle, pedBuddy[i].seatStart)
				ENDIF
				MIN1_SET_PED_AS_PLAYER_FRIEND(pedBuddy[i].ped)
				SET_PED_CAN_BE_TARGETTED(pedBuddy[i].ped, FALSE)
				SET_PED_CAN_BE_DRAGGED_OUT(pedBuddy[i].ped, FALSE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_WillFlyThroughWindscreen, FALSE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_GetOutUndriveableVehicle, FALSE) // See B*922227
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_GetOutBurningVehicle, TRUE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_RunFromFiresAndExplosions, TRUE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_DontInfluenceWantedLevel, TRUE)
				SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_ForceDirectEntry, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedBuddy[i].ped, FALSE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedBuddy[i].ped, TRUE)
				IF i = JOE
					ADD_PED_FOR_DIALOGUE(sConversation, 3, pedBuddy[JOE].ped, "JOE")
				ELSE
					ADD_PED_FOR_DIALOGUE(sConversation, 4, pedBuddy[JOSEF].ped, "JOSEF")
				ENDIF
			ENDIF
		ENDREPEAT
		IF NOT IS_REPLAY_BEING_SET_UP()
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle, VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF
	ADD_PED_FOR_DIALOGUE(sConversation, 2, PLAYER_PED_ID(), "TREVOR")
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished setting up patrol vehicle") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Create the car and Mariachi occupants.
PROC MIN1_SPAWN_MARIACHI_CAR(VECTOR v_spawn_pos, FLOAT f_spawn_pos, FLOAT f_skip_to_carrec_time, BOOL b_set_rotation = TRUE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started creating Mariachi car and occupants...") ENDIF #ENDIF
	CLEAR_AREA_OF_PROJECTILES(v_spawn_pos, 10)
	CLEAR_AREA_OF_PROJECTILES(<<2032.00610, 3308.28491, 45.05850>>, 250) // For B*1084769
	STOP_FIRE_IN_RANGE(v_spawn_pos, 10) // For B*1119875
	MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateGoOutsideBar)
	MIN1_CREATE_VEHICLE(vehMariachiCar, modelMariachiCar, v_spawn_pos, f_spawn_pos, 15)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelMariachiCar)
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		IF b_set_rotation = TRUE
			SET_ENTITY_ROTATION(vehMariachiCar, GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(500, "Min1_Van"), 0))
		ENDIF
		SET_ENTITY_LOAD_COLLISION_FLAG(vehMariachiCar, TRUE)
		SET_ENTITY_LOD_DIST(vehMariachiCar, 200)
		SET_DISABLE_PRETEND_OCCUPANTS(vehMariachiCar, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehMariachiCar, TRUE)
		MIN1_CREATE_PED_IN_VEHICLE(vehMariachiCar, pedMariachi[0], modelPed[MIN1_MODEL_MARIACHI_PED], VS_DRIVER)
		MIN1_CREATE_PED_IN_VEHICLE(vehMariachiCar, pedMariachi[1], modelPed[MIN1_MODEL_MARIACHI_PED], VS_FRONT_RIGHT)
		IF f_skip_to_carrec_time > -1
			START_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar, 500, "Min1_Van")
			PAUSE_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar, f_skip_to_carrec_time)
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[MIN1_MODEL_MARIACHI_PED])
	INT i = 0
	REPEAT NUM_MARIACHI_PEDS i
		IF IS_ENTITY_ALIVE(pedMariachi[i])
			SET_ENTITY_HEALTH(pedMariachi[i], 400)
			SET_PED_MAX_HEALTH(pedMariachi[i], 400)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_WillFlyThroughWindscreen, FALSE)
			//SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_GetOutUndriveableVehicle, TRUE) // See B*905867
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_GetOutBurningVehicle, TRUE)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_RunFromFiresAndExplosions, TRUE)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_PedsJackingMeDontGetIn, TRUE)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_DontInfluenceWantedLevel, TRUE)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_ForceDirectEntry, TRUE)
			SET_PED_CONFIG_FLAG(pedMariachi[i], PCF_DisableGoToWritheWhenInjured, TRUE) // Hopefully fixes B*1407704
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedMariachi[i], TRUE)
			SET_PED_SUFFERS_CRITICAL_HITS(pedMariachi[i], FALSE)
			SET_PED_CAN_BE_TARGETTED(pedMariachi[i], FALSE)
			SET_ENTITY_LOD_DIST(pedMariachi[i], 200)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMariachi[i], TRUE)
			IF i = 0
				ADD_PED_FOR_DIALOGUE(sConversation, 6, pedMariachi[i], "MARIACHI1")
				SET_PED_COMPONENT_VARIATION(pedMariachi[i], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
			ELSE
				ADD_PED_FOR_DIALOGUE(sConversation, 7, pedMariachi[i], "MARIACHI2")
				SET_PED_COMPONENT_VARIATION(pedMariachi[i], INT_TO_ENUM(PED_COMPONENT,0), 2, 1, 0) //(head)
			ENDIF
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished creating Mariachi car") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Handles debug skipping.
///  PARAMS:
///     i_new_state - The state to debug skip to.
PROC MIN1_DEBUG_SKIP_STATE(INT i_new_state)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Starting debug skip to ", i_new_state, "...") ENDIF #ENDIF
	RC_START_Z_SKIP()
	bZSkipping = TRUE
	tSavedConversationRoot = ""
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID())) // If doing a z skip ensure he's warped out of the patrol vehicle cos we need to delete it
	ENDIF
	MIN1_MISSION_CLEANUP(TRUE)
	REMOVE_ALL_SHOCKING_EVENTS(FALSE) // B*1469724 - Try to let scenario peds respawn immediately
	REMOVE_SHOCKING_EVENT_SPAWN_BLOCKING_AREAS() // B*1407724 - And another try
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE)
	iCurrentShotVehicleDialogue = 0
	SWITCH i_new_state
		CASE Z_SKIP_INTRO
			missionStateSkip = stateIntro
		BREAK
		CASE Z_SKIP_GO_TO_BAR
			MIN1_SETUP_PATROL_VEHICLE(<<321.687347,3408.640625,35.343765>>, -105.797066)
			CREATE_VEHICLE_FOR_REPLAY(vehForReplay, vPlayerCarRespot, fPlayerCarRespot, FALSE, FALSE, FALSE, FALSE, FALSE)
			missionStateSkip = stateGoToBar
		BREAK
		CASE Z_SKIP_GO_INSIDE_BAR
			MIN1_SETUP_PATROL_VEHICLE(<< 1981.662720,3076.814209,46.637543 >>, -140.250092)
			bIsInteriorPinned = FALSE
			missionStateSkip = stateGoInsideBar
		BREAK
		CASE Z_SKIP_CHASE_CAR
			MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateGoOutsideBar)
			MIN1_SETUP_PATROL_VEHICLE(<< 1995.8782, 3058.1973, 46.6988 >>, 45.833717, TRUE)
			MIN1_SPAWN_MARIACHI_CAR(vMariachiCar, fMariachiCar, 4000)
			MIN1_CHANGE_ROAD_NODES_FOR_CAR_CHASE(FALSE)
			missionStateSkip = stateChaseMariachiCar
		BREAK
		CASE Z_SKIP_STUN_MARIACHI
			MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateGoOutsideBar)
			MIN1_SETUP_PATROL_VEHICLE(<< 2673.9829, 4277.2314, 43.6559 >>, 25.4114)
			MIN1_SPAWN_MARIACHI_CAR(<< 2668.0261, 4288.9590, 43.7695 >>, 28.0026, -1, FALSE)
			missionStateSkip = stateStunMariachiPeds
		BREAK
		CASE Z_SKIP_MISSION_PASSED
			MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateMissionPassed)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<2698.2053, 4135.9048, 42.8234>>, 323.4031)
			ENDIF
			MIN1_CREATE_VEHICLE(vehMariachiCar, modelMariachiCar, <<2699.6541, 4141.0044, 42.6652>>, 312.6240, 15)
			SET_MODEL_AS_NO_LONGER_NEEDED(modelMariachiCar)
			missionStateSkip = stateMissionPassed
		BREAK
	ENDSWITCH
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	IF i_new_state = Z_SKIP_CHASE_CAR
		bStartedOutsideBarConversation = TRUE // If retrying the player is already in the patrol vehicle so don't play this
	ENDIF
	IF i_new_state = Z_SKIP_GO_TO_BAR
	OR i_new_state = Z_SKIP_GO_INSIDE_BAR
	OR i_new_state = Z_SKIP_CHASE_CAR
	OR i_new_state = Z_SKIP_STUN_MARIACHI
		END_REPLAY_SETUP(vehPatrolVehicle)
	ELSE
		END_REPLAY_SETUP()
	ENDIF
	IF i_new_state = Z_SKIP_MISSION_PASSED
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SAFE_FADE_SCREEN_IN_FROM_BLACK() // Screen needs to be fully faded back in before the mission complete GUI stuff will display
	ENDIF
	IF i_new_state = Z_SKIP_INTRO
		RC_END_Z_SKIP(TRUE, FALSE)
	ELSE
		RC_END_Z_SKIP()
	ENDIF
	missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished debug skip to ", i_new_state) ENDIF #ENDIF
	
ENDPROC

/// PURPOSE:
///    Setup Joe/Josef for cutscene.
PROC MIN1_SETUP_JOE_JOSEF_CUTSCENE_PROPS()
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY() // B*1420964
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MINUTE1: CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY is TRUE") #ENDIF
		//SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_HEAD, 0, 0) // Setting a hat on Joe causes B*1509088
		SET_CUTSCENE_PED_PROP_VARIATION("Joe", ANCHOR_EYES, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_HEAD, 0, 0)
		SET_CUTSCENE_PED_PROP_VARIATION("Josef", ANCHOR_EYES, 0, 0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Make/stop Joe and Josef look at the player.
PROC MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(BOOL b_look_at_player)
	INT i = 0
	REPEAT NUM_BUDDIES i
		IF IS_ENTITY_ALIVE(pedBuddy[i].ped)
			IF b_look_at_player
				TASK_LOOK_AT_ENTITY(pedBuddy[i].ped, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
			ELSE
				TASK_CLEAR_LOOK_AT(pedBuddy[i].ped)
			ENDIF
		ENDIF
	ENDREPEAT
	IF b_look_at_player
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Made buddies look at player") ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopped buddies looking at player") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if the player is in the patrol vehicle.
FUNC BOOL MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if a conversation can be played.
FUNC BOOL MIN1_ALLOW_CONVERSATION_TO_PLAY(BOOL b_check_for_interrupted_conversations = TRUE, BOOL b_check_for_god_text = TRUE)
	IF bZSkipping = TRUE
		RETURN FALSE
	ENDIF
	IF b_check_for_god_text = TRUE // If FALSE the conversation is set to not display subtitles ever, so play conversation regardless of whether there is god text onscreen or not
	AND IS_MESSAGE_BEING_DISPLAYED()
	AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) > 0 // B*665755 Support playing conversations over god text when subtitles are turned off
		RETURN FALSE
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	IF b_check_for_interrupted_conversations = TRUE
	AND NOT ARE_STRINGS_EQUAL(tSavedConversationRoot, "") // A conversation has been interrupted and still needs to be restored
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles playing the conversation if the player exits the player vehicle.
PROC MIN1_MANAGE_EXIT_VEHICLE_CONVERSATION()
	IF bStartedStopDriveConversation = FALSE
	AND missionState = stateGoToBar
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY(FALSE)
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_GETSOUT", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_GETSOUT conversation") ENDIF #ENDIF
		bStartedStopDriveConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    State machine for objectives whenever the player is expected to drive in the patrol vehicle.
PROC MIN1_MANAGE_PATROL_VEHICLE_OBJECTIVES()
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
		SWITCH pvStatus
			CASE PV_STATUS_PLAYER_NOT_INSIDE
				IF missionState = stateGoToBar
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					SAFE_REMOVE_BLIP(blipPatrolVehicle)
					bDisplayObjective = TRUE
					pvStatus = PV_STATUS_LOSE_WANTED_LEVEL
				ELSE
					HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
					IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
						CLEAR_PRINTS()
						SAFE_REMOVE_BLIP(blipPatrolVehicle)
						bDisplayObjective = TRUE
						IF missionState = stateGoToBar
							pvStatus = PV_STATUS_LOSE_WANTED_LEVEL
						ELSE
							pvStatus = PV_STATUS_GO_TO_DESTINATION
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT DOES_BLIP_EXIST(blipPatrolVehicle)
							blipPatrolVehicle = CREATE_VEHICLE_BLIP(vehPatrolVehicle)
							IF iNumToldToGetInPatrolVehicle = 0
								PRINT_NOW("MIN1_01", 5000, 1) // Get in the ~b~patrol vehicle.
								iNumToldToGetInPatrolVehicle++
							ELIF iNumToldToGetInPatrolVehicle = 1
								PRINT_NOW("MIN1_08", 5000, 1) // Get back in the ~b~patrol vehicle.
								iNumToldToGetInPatrolVehicle++
							ELSE
								CLEAR_PRINTS()
							ENDIF
						ENDIF
						MIN1_MANAGE_EXIT_VEHICLE_CONVERSATION()
					ENDIF
				ENDIF
			BREAK
			CASE PV_STATUS_LOSE_WANTED_LEVEL
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					CLEAR_PRINTS()
					IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
						bDisplayObjective = TRUE
						pvStatus = PV_STATUS_GO_TO_DESTINATION
					ELSE
						pvStatus = PV_STATUS_PLAYER_NOT_INSIDE
					ENDIF
				ELIF bDisplayObjective = TRUE
					bDisplayObjective = FALSE
					PRINT_NOW("MIN1_02", 5000, 1) // Lose the cops.
				ENDIF
			BREAK
			CASE PV_STATUS_GO_TO_DESTINATION
				IF missionState = stateGoToBar
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					SAFE_REMOVE_BLIP(blipDestination)
					bDisplayObjective = TRUE
					pvStatus = PV_STATUS_LOSE_WANTED_LEVEL
				ELSE
					IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
						IF NOT DOES_BLIP_EXIST(blipDestination)
							SWITCH missionState
								CASE stateGoToBar
									blipDestination = CREATE_COORD_BLIP(vBarInside)
								BREAK
								CASE stateChaseMariachiCar
									IF IS_ENTITY_ALIVE(vehMariachiCar)
										blipDestination = CREATE_BLIP_FOR_VEHICLE(vehMariachiCar, TRUE)
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF
						IF bDisplayObjective = TRUE
							bDisplayObjective = FALSE
							IF iNumToldToGoToDestination < 1
								SWITCH missionState
									CASE stateGoToBar
										PRINT_NOW("MIN1_03", 5000, 1) // Go to the ~y~illegals.
									BREAK
									CASE stateChaseMariachiCar
										PRINT_NOW("MIN1_06", 5000, 1) // Stop the ~r~mariachi band's car.
									BREAK
								ENDSWITCH
								iNumToldToGoToDestination++
							ELSE
								CLEAR_PRINTS()
							ENDIF
							MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(FALSE)
						ENDIF
						IF GET_VEHICLE_TYRES_CAN_BURST(vehPatrolVehicle)
							SET_VEHICLE_TYRES_CAN_BURST(vehPatrolVehicle, FALSE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Set to can't burst patrol vehicle tyres") ENDIF #ENDIF
						ENDIF
					ELIF GET_ENTITY_SPEED(vehPatrolVehicle) < 1
						SAFE_REMOVE_BLIP(blipDestination)
						MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(TRUE)
						IF NOT GET_VEHICLE_TYRES_CAN_BURST(vehPatrolVehicle)
							SET_VEHICLE_TYRES_CAN_BURST(vehPatrolVehicle, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Set to can burst patrol vehicle tyres") ENDIF #ENDIF
						ENDIF
						pvStatus = PV_STATUS_PLAYER_NOT_INSIDE
					ELSE
						BRING_VEHICLE_TO_HALT(vehPatrolVehicle, 10.0, 1)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the patrol vehicle variables.
PROC MIN1_RESET_PATROL_VEHICLE_VARS()
	iNumToldToGetInPatrolVehicle = 1 // B*1299260
	iNumToldToGoToDestination = 0
	bDisplayObjective = TRUE
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Patrol vehicle vars reset") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Resets the chase conversation variables.
PROC MIN1_RESET_CHASE_CONVERSATION_VARS()
	INT i = 0
	REPEAT NUM_CHASE_CONVERSATIONS i
		bDoneChaseConversation[i] = FALSE
		sChaseConversation[i] = "MIN1_CHASE"
		sChaseConversation[i] += (i+1)
	ENDREPEAT
	iDoneChaseConversations = 0
	i = 0
	REPEAT NUM_CRASH_CONVERSATIONS i
		bStartedCrashConversation[i] = FALSE
	ENDREPEAT
	iDoneCrashConversations = 0
ENDPROC

/// PURPOSE:
///    Resets the hurry stun conversation variables.
PROC MIN1_RESET_HURRY_STUN_CONVERSATION_VARS()
	INT i = 0
	REPEAT NUM_HURRY_STUN_CONVERSATIONS i
		sHurryStunConversation[i] = "MIN1_HURRY"
		sHurryStunConversation[i] += (i+1)
	ENDREPEAT
	iHurryStunConversationsPlayed = 0
ENDPROC

/// PURPOSE:
///    Handles playing the conversation when they've almost reached the bar.
PROC MIN1_MANAGE_ALMOST_THERE_CONVERSATION()
	IF bStartedAlmostThereConversation = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND IS_ENTITY_IN_RANGE_COORDS(vehPatrolVehicle, vBarOutside, 60) // Don't play until near to the bar
	AND NOT IS_ENTITY_IN_RANGE_COORDS(vehPatrolVehicle, vBarOutside, 40) // ...but not too near
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_ALMOST", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_ALMOST conversation") ENDIF #ENDIF
		bStartedAlmostThereConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation if the player drives away from the bar.
PROC MIN1_MANAGE_WRONG_WAY_CONVERSATION()
	IF bStartedWrongWayConversation = FALSE
	AND bFinishedDriveToBarConversation = TRUE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehPatrolVehicle, vBarOutside) > fWrongWayDistance+50
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_WRONG", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_WRONG conversation") ENDIF #ENDIF
		bStartedWrongWayConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Play the next drive to bar conversation in the sequence.
PROC MIN1_MANAGE_DRIVE_TO_BAR_CONVERSATION()
	IF bFinishedDriveToBarConversation = FALSE
		IF bStartedDriveToBarConversation = FALSE
			IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
			AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_DRIVEA", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_DRIVEA conversation") ENDIF #ENDIF
				bStartedDriveToBarConversation = TRUE
			ENDIF
		ELSE
			IF ARE_STRINGS_EQUAL(tSavedConversationRoot, "")
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					tSavedConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
					IF NOT MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
					AND NOT IS_ENTITY_IN_RANGE_COORDS(vehPatrolVehicle, vBarOutside, 20) // B*1378563 Don't pause conversation as player has arrived at the bar
						tSavedConversationRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping and storing conversation ", tSavedConversationRoot) ENDIF #ENDIF
						KILL_ANY_CONVERSATION()
					ENDIF
				ELIF ARE_STRINGS_EQUAL(tSavedConversationLabel, "NULL") // Wait until MIN1_DRIVEA conversation has finished
					bFinishedDriveToBarConversation = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished MIN1_DRIVEA conversation") ENDIF #ENDIF
					IF IS_ENTITY_ALIVE(vehPatrolVehicle)
						fWrongWayDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehPatrolVehicle, vBarOutside)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: fWrongWayDistance = ", fWrongWayDistance) ENDIF #ENDIF
					ENDIF
				ENDIF
			ELSE // A conversation has been stored so we need to check for conditions to restore it
				IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
				AND MIN1_ALLOW_CONVERSATION_TO_PLAY(FALSE)
				AND CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConversation, "MIN1AUD", tSavedConversationRoot, tSavedConversationLabel, CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Restoring conversation ", tSavedConversationRoot) ENDIF #ENDIF
					tSavedConversationRoot = ""
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the player doesn't abandon the patrol vehicle, and fails the mission if necessary.
PROC MIN1_MANAGE_ABANDONING_VEHICLE()
	IF bZSkipping = FALSE
		IF pvStatus = PV_STATUS_PLAYER_NOT_INSIDE
		OR pvStatus = PV_STATUS_LOSE_WANTED_LEVEL
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
			AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehPatrolVehicle) > 100
				MIN1_MISSION_FAILED(FAILED_ABANDONED_BUDDIES)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pin/unpin interior depending on distance. See B*1080382.
PROC MIN1_MANAGE_INTERIOR_PINNING()
	IF IS_ENTITY_IN_RANGE_COORDS(vehPatrolVehicle, vBarOutside, 100)
		IF bIsInteriorPinned = FALSE
			PIN_INTERIOR_IN_MEMORY(interiorBar)
			bIsInteriorPinned = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Pinned bar interior into memory") ENDIF #ENDIF
		ENDIF
	ELSE
		IF bIsInteriorPinned = TRUE
			UNPIN_INTERIOR(interiorBar)
			bIsInteriorPinned = FALSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Unpinned bar interior from memory") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    For B*1084002 - Prevent scenario peds in bar becoming agitated
PROC MIN1_DISABLE_TREVOR_AGITATION_TRIGGERS()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitationTriggers, TRUE)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitation, TRUE) 
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTalk, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the arrived at bar conversation.
PROC MIN1_MANAGE_ARRIVED_AT_BAR_CONVERSATION()
	IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
		IF bStartedArrivedAtBarConversation = FALSE
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_ATBAR", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_ATBAR conversation") ENDIF #ENDIF
				bStartedArrivedAtBarConversation = TRUE
			ENDIF
		ELIF NOT DOES_BLIP_EXIST(blipDestination)
			blipDestination = CREATE_COORD_BLIP(vBarInside, BLIPPRIORITY_HIGH, FALSE)
			PRINT_NOW("MIN1_03", 5000, 1) // Go to the ~y~illegals.
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversations is the player remains in the car outside the bar.
PROC MIN1_MANAGE_WAIT_CONVERSATIONS()
	IF iWaitConversationsPlayed < 5
	AND (GET_GAME_TIMER() - iWaitConversationTimer) > 10000
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_WAIT", CONV_PRIORITY_HIGH)
		iWaitConversationsPlayed++
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_WAIT conversation, iWaitConversationsPlayed is now ", iWaitConversationsPlayed) ENDIF #ENDIF
		iWaitConversationTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    See B*947514. Handles Trevor looking for the mariachi inside the bar.
PROC MIN1_MANAGE_TREVOR_LOOKING_FOR_MARIACHI()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND (GET_GAME_TIMER() - iLookAroundTimer) > 1500
	AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorBar
		IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(-10,-1),5,GET_RANDOM_FLOAT_IN_RANGE(-1,1.5)>>), 2000)
		ELSE
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(1,10),5,GET_RANDOM_FLOAT_IN_RANGE(-1,1.5)>>), 2000)
		ENDIF
		iLookAroundTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing Janet's dialogue when Trevor walks into the bar.
PROC MIN1_MANAGE_JANET_CONVERSATION()
	IF bDoneJanetTalk = FALSE
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1987.802856,3055.128418,45.215298>>, <<1986.586548,3053.183838,48.215187>>, 1.500000)
		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		IF GET_CLOSEST_PED(<<1983.41, 3054.88, 47.01>>, 3, TRUE, TRUE, pedJanet)
		AND IS_ENTITY_ALIVE(pedJanet)
			IF GET_ENTITY_MODEL(pedJanet) = IG_JANET
				KILL_ANY_CONVERSATION() // Stop the drunk ped conversation if ongoing
				ADD_PED_FOR_DIALOGUE(sConversation, 8, pedJanet, "JANET")
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "MIN1AUD", "MIN1_BANNED", CONV_PRIORITY_AMBIENT_MEDIUM)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_BANNED conversation") ENDIF #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Found a ped in the bar but it wasn't Janet, so no doing MIN1_BANNED conversation") ENDIF #ENDIF
			ENDIF
			bDoneJanetTalk = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the drunken hillbilly dialogue.
PROC MIN1_MANAGE_DRUNK_PED_CONVERSATION()
	IF bStartedDrunkPedConversation = FALSE
		IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "MIN1AUD", "MIN1_DRUNK", CONV_PRIORITY_AMBIENT_MEDIUM)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_DRUNK conversation") ENDIF #ENDIF
			bStartedDrunkPedConversation = TRUE
		ENDIF
	ELIF bStartedBumpDrunkConversation = FALSE
		IF IS_PED_FLEEING(pedWalkOutBar)
			CPRINTLN(DEBUG_MISSION, "pedWalkOutBar is now fleeing")
			IF RC_IS_THIS_CONVERSATION_ROOT_PLAYING("MIN1_DRUNK")
				CPRINTLN(DEBUG_MISSION, "MIN1_DRUNK was playing so killing the conversation")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "MIN1AUD", "MIN1_BUMP", CONV_PRIORITY_AMBIENT_MEDIUM)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_BUMP conversation") ENDIF #ENDIF
			bStartedBumpDrunkConversation = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles hurling the drunk out of the bar door when the player approaches.
PROC MIN1_MANAGE_DRUNK_PED_OUT_DOOR()
	IF bZSkipping = FALSE // Stops ped being spawned if skipping to chase car section
		IF bPedGoneOutDoor = FALSE
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vWalkOutBarPed, 20)
			AND NOT DOES_ENTITY_EXIST(pedWalkOutBar)
				MIN1_CREATE_PED(pedWalkOutBar, modelPed[MIN1_MODEL_DRUNK_PED_1], vWalkOutBarPed, fWalkOutBarPed)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[MIN1_MODEL_DRUNK_PED_1])
				SET_PED_CONFIG_FLAG(pedWalkOutBar, PCF_OpenDoorArmIK, TRUE) // B*1299271 Make the ped search for the door
				SET_PED_COMBAT_ATTRIBUTES(pedWalkOutBar, CA_AGGRESSIVE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(pedWalkOutBar, CA_ALWAYS_FLEE, TRUE)
				SET_PED_FLEE_ATTRIBUTES(pedWalkOutBar, FA_USE_VEHICLE, FALSE)
				ADD_PED_FOR_DIALOGUE(sConversation, 5, pedWalkOutBar, "MIN1DRUNKMALE")
				OPEN_SEQUENCE_TASK(drunkSequence)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<1992.0579, 3056.1792, 46.0630>>, PEDMOVE_WALK)
					TASK_WANDER_IN_AREA(NULL, <<1994,3059,47>>, 5)
				CLOSE_SEQUENCE_TASK(drunkSequence)
				TASK_PERFORM_SEQUENCE(pedWalkOutBar, drunkSequence)
				CLEAR_SEQUENCE_TASK(drunkSequence)
				Make_Ped_Drunk_Constant(pedWalkOutBar)
				bPedGoneOutDoor = TRUE
			ENDIF
		ELIF IS_ENTITY_ALIVE(pedWalkOutBar)
			MIN1_MANAGE_DRUNK_PED_CONVERSATION()
			SET_PED_RESET_FLAG(pedWalkOutBar, PRF_SearchForClosestDoor, TRUE) // B*1299271 Make the ped search for the door
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the conversation inside the bar.
PROC MIN1_MANAGE_IN_BAR_CONVERSATION()
	IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
		IF bStartedInBarConversation = FALSE
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_INBAR", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_INBAR conversation") ENDIF #ENDIF
				bStartedInBarConversation = TRUE
			ENDIF
		ELIF NOT DOES_BLIP_EXIST(blipPatrolVehicle)
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
				blipPatrolVehicle = CREATE_VEHICLE_BLIP(vehPatrolVehicle)
				PRINT_NOW("MIN1_01", DEFAULT_GOD_TEXT_TIME, 1) // Get in the ~b~patrol vehicle.
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the conversation in which Joe tells Trevor to get outside.
PROC MIN1_MANAGE_GET_OUT_HERE_CONVERSATION()
	IF bStartedGetOutHereConversation = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY(TRUE, FALSE)
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<1987.8, 3053.5, 46.1>>, 1.5)
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_GETOUT", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_GETOUT conversation") ENDIF #ENDIF
		bStartedGetOutHereConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Joe telling the player to get into the patrol vehicle.
PROC MIN1_MANAGE_OUTSIDE_BAR_CONVERSATIONS()
	IF bStartedOutsideBarConversation = FALSE
		IF MIN1_ALLOW_CONVERSATION_TO_PLAY(TRUE, FALSE)
		AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_OUTBAR", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_OUTBAR conversation") ENDIF #ENDIF
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
			bStartedOutsideBarConversation = TRUE
		ENDIF
	ELIF bStartedChaseVanConversation = FALSE // Don't play these remainders if the player has got into the patrol vehicle
		IF iGetInConversationsPlayed < 3
		AND NOT MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
		AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
		AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_WAIT2", CONV_PRIORITY_HIGH)
			iGetInConversationsPlayed++
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_WAIT2 conversation, iGetInConversationsPlayed is now ", iGetInConversationsPlayed) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation upon spotting the Mariachi van.
PROC MIN1_MANAGE_START_CHASE_CONVERSATION()
	IF bStartedOutsideBarConversation = TRUE
		IF bStartedChaseVanConversation = FALSE
			IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
			AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
			AND (GET_GAME_TIMER() - iStunGunDialogueTimer) > 5000
			AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_DRIVEB", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_DRIVEB conversation") ENDIF #ENDIF
				bStartedChaseVanConversation = TRUE
				bPlayerIsCurrentlyInPatrolVehicle = TRUE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_CAR_CHASE, "Car chase", TRUE) // B*853089 Don't set the checkpoint until the player has got into the patrol vehicle outside the bar
			ENDIF
		ELIF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN) // B*573295
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				TEXT_LABEL_23 t_conversation_label = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
				IF ARE_STRINGS_EQUAL(t_conversation_label, "MIN1_DRIVEB_4")
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN, 200, TRUE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN, TRUE) // B*1353528
					PLAY_SOUND_FRONTEND(-1, "STUN_COLLECT", "MINUTE_MAN_01_SOUNDSET")
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave stun gun to player") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the anims on the mariachi peds while in their car.
PROC MIN1_MANAGE_MARIACHI_ANIMS()
	IF bUsePlaybackAIForMariachiCar = FALSE // B*1256009 Prevent anim tasks stopping the ai playback task
		INT i_random
		INT i = 0
		REPEAT NUM_MARIACHI_PEDS i
			IF IS_ENTITY_ALIVE(pedMariachi[i])
				IF NOT IsPedPerformingTask(pedMariachi[i], SCRIPT_TASK_PLAY_ANIM)
					IF fCurrentPositionInRecording > 1100
						i_random = 0 // Just use the base anims when nearing driving under AI playback
					ELSE
						i_random = GET_RANDOM_INT_IN_RANGE(0, 5)
					ENDIF
					IF i = 0
						SWITCH i_random
							CASE 0
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "base_drunk_mariachi_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 1
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_sing_1_mariachi_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 2
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_sing_2_mariachi_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 3
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_tequila_mariachi_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 4
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_waive_mariachi_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
						ENDSWITCH
					ELSE
						SWITCH i_random
							CASE 0
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "base_drunk_mariachi_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 1
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_sing_1_mariachi_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 2
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_sing_2_mariachi_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 3
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_tequila_mariachi_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
							CASE 4
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_1", "idle_waive_mariachi_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if we should play some drunken dialogue.
FUNC BOOL MIN1_TRY_TO_PLAY_SINGING_DIALOGUE()
	IF fCurrentPositionInRecording > 1400
		RETURN FALSE
	ENDIF
	IF bStartedChaseVanConversation = FALSE
		RETURN FALSE
	ENDIF
	IF (GET_GAME_TIMER() - iSingingDialogueTimer) < 10000
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_ALIVE(vehMariachiCar)
	AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehMariachiCar, 50)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles playing the drunken Mariachi dialogue.
PROC MIN1_MANAGE_SINGING_DIALOGUE()
	IF MIN1_TRY_TO_PLAY_SINGING_DIALOGUE()
		iSingingDialogueTimer = GET_GAME_TIMER()
		IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "MIN1AUD", "MIN1_SING1", CONV_PRIORITY_AMBIENT_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_SING1 dialogue") ENDIF #ENDIF
		ELSE
			ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "MIN1AUD", "MIN1_SING2", CONV_PRIORITY_AMBIENT_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_SING2 dialogue") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the van chase dialogue from Joe and Josef.
PROC MIN1_MANAGE_CHASE_CONVERSATIONS()
	IF iDoneChaseConversations < NUM_CHASE_CONVERSATIONS
	//AND fCurrentPositionInRecording < 1200 // B*1413441 Allow these conversations to play earlier
	AND fCurrentPositionInRecording < 1250 // B*1563150 Don't allow these conversation to play near the end of the chase
	AND bStartedChaseVanConversation = TRUE
	//AND bStartedOffRoadConversation = TRUE // B*1413441 Allow these conversations to play earlier
	AND (GET_GAME_TIMER() - iChaseDialogueTimer) > 12000
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND IS_ENTITY_ALIVE(vehMariachiCar)
	AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehMariachiCar, 50)
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
		INT i = GET_RANDOM_INT_IN_RANGE(0, NUM_CHASE_CONVERSATIONS)
		IF bDoneChaseConversation[i] = FALSE
		AND CREATE_CONVERSATION(sConversation, "MIN1AUD", sChaseConversation[i], CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started ", sChaseConversation[i], " dialogue") ENDIF #ENDIF
			bDoneChaseConversation[i] = TRUE
			iChaseDialogueTimer = GET_GAME_TIMER()
			iDoneChaseConversations++
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iDoneChaseConversations = ", iDoneChaseConversations) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing conversations when the player lands after big jumps.
PROC MIN1_MANAGE_CRASH_CONVERSATIONS()
	IF iDoneCrashConversations < NUM_CRASH_CONVERSATIONS
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
		INT i = 0
		REPEAT NUM_CRASH_CONVERSATIONS i
			IF bStartedCrashConversation[i] = FALSE
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vCrashConversationPos[i]) < 10
			AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_CRASH2", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_CRASH2 dialogue") ENDIF #ENDIF
				bStartedCrashConversation[i] = TRUE
				iDoneCrashConversations++
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iDoneCrashConversations = ", iDoneCrashConversations) ENDIF #ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles displaying the hint camera and text for the mariachi car.
PROC MIN1_MANAGE_MARIACHI_CAR_FOCUS_CAMERA()
	IF bStartedChaseVanConversation = TRUE
	AND IS_ENTITY_ALIVE(vehMariachiCar)
		CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehMariachiCar, "MIN1_05")
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if either Mariachi leaves the van (probably getting dragged out by the player).
FUNC BOOL MIN1_HAS_EITHER_MARIACHI_LEFT_VAN()
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		IF IS_ENTITY_ALIVE(pedMariachi[0])
		AND NOT IS_PED_IN_VEHICLE(pedMariachi[0], vehMariachiCar)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: pedMariachi[0] left vehicle") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(pedMariachi[1])
		AND NOT IS_PED_IN_VEHICLE(pedMariachi[1], vehMariachiCar)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: pedMariachi[1] left vehicle") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if either Mariachi is low on health (probably due to being shot by the player).
FUNC BOOL MIN1_IF_EITHER_MARIACHI_ALMOST_DEAD()
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		IF IS_ENTITY_ALIVE(pedMariachi[0])
		AND GET_ENTITY_HEALTH(pedMariachi[0]) < 200
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: pedMariachi[0] health low") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(pedMariachi[1])
		AND GET_ENTITY_HEALTH(pedMariachi[1]) < 200
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: pedMariachi[1] health low") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks player distance to the Mariachi peds, and fails mission if the distance gets too far.
PROC MIN1_MANAGE_ABANDONING_MARIACHI()
	IF bZSkipping = FALSE
		IF IS_ENTITY_ALIVE(vehMariachiCar)
			IF DOES_BLIP_EXIST(blipDestination)
			AND pvStatus = PV_STATUS_GO_TO_DESTINATION
				UPDATE_CHASE_BLIP(blipDestination, vehMariachiCar, fFailDist, 0.8) // B*904083
			ENDIF
		ENDIF
		INT i = 0
		REPEAT NUM_MARIACHI_PEDS i
			IF IS_ENTITY_ALIVE(pedMariachi[i])
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedMariachi[i]) > fFailDist
				IF missionState = stateStunMariachiPeds
				AND IS_ENTITY_ALIVE(pedBuddy[JOE].ped)
				AND IS_ENTITY_ALIVE(pedBuddy[JOSEF].ped)
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedBuddy[JOE].ped) > (fFailDist/2) // B*1435725 Only fail for abandoning Joe/Josef if they're not in the patrol vehicle and are not close, else use FAILED_MARIACHI_ESCAPED
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedBuddy[JOSEF].ped) > (fFailDist/2)
					MIN1_MISSION_FAILED(FAILED_ABANDONED_BUDDIES)
				ELSE
					MIN1_MISSION_FAILED(FAILED_MARIACHI_ESCAPED)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages loading and playing the mariachi singing stream.
PROC MIN1_MANAGE_PLAYING_SINGING_STREAM()
	IF bStartedSingingStream = FALSE
	AND LOAD_STREAM("MARIACHI", "MINUTE_MAN_01_SOUNDSET")
	AND IS_ENTITY_ALIVE(vehMariachiCar)
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
		PLAY_STREAM_FROM_VEHICLE(vehMariachiCar) // Mariachi singing
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
		bStartedSingingStream = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started singing stream") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Joe/Josef comment on the Mariachi driving offroad.
PROC MIN1_MANAGE_OFFROAD_CONVERSATION()
	IF bStartedOffRoadConversation = FALSE
	AND fCurrentPositionInRecording > 450
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(vehMariachiCar)
	AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
	AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehMariachiCar, 75)
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_OFFROAD", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_OFFROAD conversation") ENDIF #ENDIF
		bStartedOffRoadConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing a conversation if the player stops his car during the chase.
PROC MIN1_MANAGE_STOPPED_CONVERSATIONS()
	IF iStoppedConversationsPlayed < 3
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
		IF bPatrolVehicleStoppedDuringChase = FALSE // Patrol vehicle is moving
			IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
			AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
			AND GET_ENTITY_SPEED(vehPatrolVehicle) < 1
			AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_STOP", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_STOP conversation") ENDIF #ENDIF
				iStoppedConversationsPlayed++
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iStoppedConversationsPlayed = ", iStoppedConversationsPlayed) ENDIF #ENDIF
				bPatrolVehicleStoppedDuringChase = TRUE
			ENDIF
		ELSE // Patrol vehicle is stationary
			IF GET_ENTITY_SPEED(vehPatrolVehicle) > 2
				bPatrolVehicleStoppedDuringChase = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation if the player lags behind in the chase.
PROC MIN1_MANAGE_FAR_BEHIND_CONVERSATIONS()
	IF iFarBehindConversationsPlayed < 3
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND IS_ENTITY_ALIVE(vehMariachiCar)
		IF bPlayedFarBehindConversation = FALSE
			IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
			AND MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
			AND GET_DISTANCE_BETWEEN_ENTITIES(vehPatrolVehicle, vehMariachiCar) > 100
			AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_BEHIND", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_BEHIND conversation") ENDIF #ENDIF
				iFarBehindConversationsPlayed++
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iFarBehindConversationsPlayed = ", iFarBehindConversationsPlayed) ENDIF #ENDIF
				bPlayedFarBehindConversation = TRUE
			ENDIF
		ELIF GET_DISTANCE_BETWEEN_ENTITIES(vehPatrolVehicle, vehMariachiCar) < 75
			bPlayedFarBehindConversation = FALSE // Reset flag so we can check again for the player lagging far behind and play another random conversation
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation if the player leaves the patrol vehicle during the chase.
PROC MIN1_MANAGE_LEAVE_VEHICLE_CONVERSATIONS()
	IF bStartedChaseVanConversation = TRUE // Only start checking this after the player has got back in to start the start
	AND fCurrentPositionInRecording < 1465 // Chase has almost ended so don't play any of these conversations
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
		IF bPlayerIsCurrentlyInPatrolVehicle = TRUE
			IF NOT MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Player exited patrol vehicle") ENDIF #ENDIF
				bPlayerIsCurrentlyInPatrolVehicle = FALSE
				bAllowPlayExitedVehicleConversation = TRUE
				bAllowPlayGetBackInVehicleConversation = TRUE
			ELIF MIN1_ALLOW_CONVERSATION_TO_PLAY()
				IF iGotBackInVehicleConversationsPlayed < 3
				AND bAllowPlayGotBackInVehicleConversation = TRUE
				AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_GOGO", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_GOGO conversation") ENDIF #ENDIF
					iGotBackInVehicleConversationsPlayed++
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iGotBackInVehicleConversationsPlayed = ", iGotBackInVehicleConversationsPlayed) ENDIF #ENDIF
					bAllowPlayGotBackInVehicleConversation = FALSE
				ENDIF
			ENDIF
		ELSE
			IF MIN1_IS_PLAYER_IN_PATROL_VEHICLE()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Player got in patrol vehicle") ENDIF #ENDIF
				bPlayerIsCurrentlyInPatrolVehicle = TRUE
				bAllowPlayGotBackInVehicleConversation = TRUE
			ELIF MIN1_ALLOW_CONVERSATION_TO_PLAY()
				IF iExitedVehicleConversationsPlayed < 3
				AND bAllowPlayExitedVehicleConversation = TRUE
				AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_GETIN", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_GETIN conversation") ENDIF #ENDIF
					iExitedVehicleConversationsPlayed++
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iExitedVehicleConversationsPlayed = ", iExitedVehicleConversationsPlayed) ENDIF #ENDIF
					bAllowPlayExitedVehicleConversation = FALSE
				ENDIF
				IF iGetBackInVehicleConversationsPlayed < 3
				AND bAllowPlayGetBackInVehicleConversation = TRUE
				AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_BACKIN", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_BACKIN conversation") ENDIF #ENDIF
					iGetBackInVehicleConversationsPlayed++
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iGetBackInVehicleConversationsPlayed = ", iGetBackInVehicleConversationsPlayed) ENDIF #ENDIF
					bAllowPlayGetBackInVehicleConversation = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensure the long chase conversation get stopped when the chase ends.
PROC MIN1_STOP_ANY_CHASE_CONVERSATIONS()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_23 t_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(t_root, sChaseConversation[0])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[1])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[2])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[3])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[4])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[5])
		OR ARE_STRINGS_EQUAL(t_root, sChaseConversation[6])
			KILL_FACE_TO_FACE_CONVERSATION()
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Car chase finished so stopped a long chase conversation") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if the player is closer to the end of the chase route than the mariachi car.
FUNC BOOL MIN1_IS_PLAYER_NEARER_TO_END_OF_ROUTE()
	IF IS_ENTITY_ALIVE(vehMariachiCar)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		FLOAT f_dist_mariachi = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehMariachiCar, vOpenGroundWanderArea)
		FLOAT f_dist_player = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vOpenGroundWanderArea)
		IF f_dist_player < f_dist_mariachi
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the player is closer to the end of the chase route than the mariachi car.
FUNC BOOL MIN1_IS_MARIACHI_CAR_DOING_JUMP()
	IF IS_ENTITY_ALIVE(vehMariachiCar)
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehMariachiCar, <<2262, 3590, 58>>) < 15
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehMariachiCar, <<2387, 3705, 53>>) < 15
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehMariachiCar, <<2620, 3853, 72>>) < 15
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handle starting the vehicle recording when there is a driver, and subsequently stopping the vehicle recording if the driver leaves.
PROC MIN1_MANAGE_MARIACHI_CAR_RECORDING()
	IF IS_ENTITY_ALIVE(vehMariachiCar)
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehMariachiCar)
		fCurrentPositionInRecording = GET_POSITION_IN_RECORDING(vehMariachiCar)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Recording playback pos = ", fCurrentPositionInRecording) ENDIF #ENDIF
		IF IS_VEHICLE_SEAT_FREE(vehMariachiCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar)
		ELSE
			FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehMariachiCar)
			IF bUsePlaybackAIForMariachiCar = FALSE
				IF fCurrentPositionInRecording > 1250 // See B*640795 - Go into AI mode when the mariachi car is passed all the hills
					bUsePlaybackAIForMariachiCar = TRUE
					SET_PLAYBACK_TO_USE_AI(vehMariachiCar, DRIVINGMODE_PLOUGHTHROUGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Setting Mariachi car to use AI") ENDIF #ENDIF
				ELSE
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
						SET_PLAYBACK_SPEED(vehMariachiCar, 0.7)
					ELIF MIN1_IS_PLAYER_NEARER_TO_END_OF_ROUTE() // B*1294781 Set to max speed if the player is closer to the end of the route
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.7)
					ELIF MIN1_IS_MARIACHI_CAR_DOING_JUMP() // B*1294781 Set to normal speed if doing a jump
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.0)
					ELIF fDist > 75
						SET_PLAYBACK_SPEED(vehMariachiCar, 0.5)
					ELIF fDist > 30
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.0)
					ELIF fDist > 20
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.3)
					ELIF fDist > 10
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.5)
					ELSE
						SET_PLAYBACK_SPEED(vehMariachiCar, 1.7)
					ENDIF
				ENDIF
			ELSE
				IF fDist > 75
					SET_PLAYBACK_SPEED(vehMariachiCar, 0.5)
				ELIF fDist > 30
					SET_PLAYBACK_SPEED(vehMariachiCar, 1.0)
				ELSE
					SET_PLAYBACK_SPEED(vehMariachiCar, 1.3)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if the Mariachi vehicle has been damaged enough to stop or for some other reason.
FUNC BOOL MIN1_SHOULD_MARIACHI_VEHICLE_STOP()
	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehMariachiCar)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - playback not playing on car") ENDIF #ENDIF
		SET_VEHICLE_ENGINE_HEALTH(vehMariachiCar, 200)
		RETURN TRUE
	ELIF bStartedCrashedConversation = FALSE
		IF fCurrentPositionInRecording > 1465
		AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
		AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehMariachiCar, 75)
		AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_CRASH", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_CRASH conversation") ENDIF #ENDIF
			bStartedCrashedConversation = TRUE
		ENDIF
	ENDIF
	IF MIN1_HAS_EITHER_MARIACHI_LEFT_VAN()
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - at least one of the Mariachi has left the vehicle") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	IF MIN1_IF_EITHER_MARIACHI_ALMOST_DEAD()
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - at least one of the Mariachi has low health") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	IF GET_ENTITY_HEALTH(vehMariachiCar) < iRequiredHealth
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - entity health lower than threshold") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	IF bUsePlaybackAIForMariachiCar = TRUE
		IF IS_VEHICLE_STOPPED(vehMariachiCar)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - IS_VEHICLE_STOPPED returned true") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF GET_ENTITY_SPEED(vehMariachiCar) < 0.5
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - GET_ENTITY_SPEED less than 0.5") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		IF bUsePlaybackAIForMariachiCar = TRUE // Don't check while on car recording only while under AI playback
		AND IS_ENTITY_UPSIDEDOWN(vehMariachiCar) // B*1377842 Get the mariachi out as soon as their car is upsidedown
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - upside down") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	IF (GET_GAME_TIMER() - iMariachiCarTyresBurstTimer) > 1000 // Check for burst tyres once a second
		INT iTyresBurst = 0
		IF IS_VEHICLE_TYRE_BURST(vehMariachiCar, SC_WHEEL_CAR_REAR_LEFT)
			iTyresBurst++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehMariachiCar, SC_WHEEL_CAR_REAR_RIGHT)
			iTyresBurst++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehMariachiCar, SC_WHEEL_CAR_FRONT_LEFT)
			iTyresBurst++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehMariachiCar, SC_WHEEL_CAR_FRONT_RIGHT)
			iTyresBurst++
		ENDIF
		IF iTyresBurst > 1 // B*1001500 - Stop car if any 2 tyres are burst
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping Mariachi vehicle - 2 tyres are burst") ENDIF #ENDIF
			RETURN TRUE
		ELSE
			iMariachiCarTyresBurstTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles performing a rolling start on the player's vehicle.
PROC MIN1_MANAGE_ROLLING_START()
	IF bDoneRollingStart = FALSE
		IF IS_VEHICLE_OK(vehPatrolVehicle)
			IF iRollingStartTimer = -1
				iRollingStartTimer = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Rolling start underway") ENDIF #ENDIF
			ELSE
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPatrolVehicle)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
				OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
				OR IS_VEHICLE_SEAT_FREE(vehPatrolVehicle)
				OR (GET_GAME_TIMER() - iRollingStartTimer > 3000) 
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPatrolVehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehPatrolVehicle)
					ENDIF
					REMOVE_VEHICLE_RECORDING(500, "Min1_PV")
					bDoneRollingStart = TRUE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Rolling start finished") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Joe tells Trevor to stun the Mariachi.
PROC MIN1_MANAGE_STUN_THEM_DIALOGUE()
	IF bStartedStunThemConversation = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_STUN", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_STUN conversation") ENDIF #ENDIF
		bStartedStunThemConversation = TRUE
		PRINT_NOW("MIN1_07", DEFAULT_GOD_TEXT_TIME, 1) // Use the stun-gun on the ~r~Mariachi.
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversations when Trevor stuns the Mariachi.
PROC MIN1_MANAGE_STUNNED_MARIACHI_DIALOGUE(INT i)
	IF bPlayedStunnedConversation[i] = FALSE
		IF MIN1_ALLOW_CONVERSATION_TO_PLAY()
			IF iPlayedStunnedConversation = 0
				IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_CAR1", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_CAR1 conversation") ENDIF #ENDIF
					iPlayedStunnedConversation = 1
					bPlayedStunnedConversation[i] = TRUE
				ENDIF
			ELIF iPlayedStunnedConversation = 1
				IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_CAR2", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_CAR2 conversation") ENDIF #ENDIF
					iPlayedStunnedConversation = 2
					bPlayedStunnedConversation[i] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversations when the Mariachi sober up.
PROC MIN1_MANAGE_SOBER_MARIACHI_DIALOGUE(INT i)
	IF bPlayedSoberConversation[i] = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
		IF i = 0
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_SOBER1", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_SOBER1 conversation") ENDIF #ENDIF
				bPlayedSoberConversation[i] = TRUE
			ENDIF
		ELSE
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_SOBER2", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_SOBER2 conversation") ENDIF #ENDIF
				bPlayedSoberConversation[i] = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Joe telling the player to hurry up and stun the Mariachi.
PROC MIN1_MANAGE_HURRY_STUN_DIALOGUE()
	IF iPlayedStunnedConversation < 2 // Don't play any of these conversations if player has stunned both Mariachi
	AND iHurryStunConversationsPlayed < NUM_HURRY_STUN_CONVERSATIONS
	AND bStartedStunThemConversation = TRUE // Wait until first told to stun them
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(pedBuddy[JOE].ped)
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedBuddy[JOE].ped) < 50
	AND (GET_GAME_TIMER() - iNotStunnedDialogueTimer) > 20000*(iHurryStunConversationsPlayed+1)
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", sHurryStunConversation[iHurryStunConversationsPlayed], CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started ", sHurryStunConversation[iHurryStunConversationsPlayed], " conversation") ENDIF #ENDIF
		iHurryStunConversationsPlayed++
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing Joe and Josef's insults when shepherding a Mariachi into the patrol vehicle.
PROC MIN1_MANAGE_JOE_JOSEF_INSULTS(INT i_buddy)
	IF pedBuddy[i_buddy].iNumInsultsGiven < 3 // Don't repeat any insults
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND (GET_GAME_TIMER() - pedBuddy[i_buddy].iInsultTimer) > pedBuddy[i_buddy].iDelayBeforeNextInsult
	AND NOT IS_ENTITY_IN_RANGE_ENTITY(vehPatrolVehicle, pedMariachi[i_buddy], 4) // Don't play if Mariachi is near to the patrol vehicle ie about to get in
	AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedMariachi[i_buddy], 50) // Don't play if Mariachi is far away from player
		IF i_buddy = JOE
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_INSULT1", CONV_PRIORITY_MEDIUM)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_INSULT1 conversation for Joe") ENDIF #ENDIF
				pedBuddy[i_buddy].iNumInsultsGiven++
				pedBuddy[i_buddy].iInsultTimer = GET_GAME_TIMER()
				pedBuddy[i_buddy].iDelayBeforeNextInsult = GET_RANDOM_INT_IN_RANGE(8000, 10000)
			ENDIF
		ELSE
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_INSULT2", CONV_PRIORITY_MEDIUM)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_INSULT2 conversation for Josef") ENDIF #ENDIF
				pedBuddy[i_buddy].iNumInsultsGiven++
				pedBuddy[i_buddy].iInsultTimer = GET_GAME_TIMER()
				pedBuddy[i_buddy].iDelayBeforeNextInsult = GET_RANDOM_INT_IN_RANGE(8000, 10000)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Try to play one of the mariachi conversation when the objective is to stun them.
PROC MIN1_PLAY_MARIACHI_CONVERSATION(INT i_ped, STRING string_to_play, INT &i_counter, INT i_max)
	IF i_counter < i_max
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(pedMariachi[i_ped])
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedMariachi[i_ped]) < 50
		TEXT_LABEL_15 text_to_play = string_to_play
		IF i_ped = 0
			text_to_play += 1
		ELSE
			text_to_play += 2
		ENDIF
		IF CREATE_CONVERSATION(sConversation, "MIN1AUD", text_to_play, CONV_PRIORITY_HIGH)
			i_counter++
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Played mariachi conversation ", text_to_play, " i_counter = ", i_counter, " i_max = ", i_max) ENDIF #ENDIF
			iMariachiConversationTimer[i_ped] = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if the mariachi has been damaged by an acceptable weapon.
FUNC BOOL MIN1_HAS_MARIACHI_BEEN_DAMAGED_BY_ALLOWED_WEAPON(INT i)
	IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedMariachi[i], WEAPONTYPE_STUNGUN, GENERALWEAPON_TYPE_INVALID)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedMariachi[i], WEAPONTYPE_UNARMED, GENERALWEAPON_TYPE_INVALID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Play all the dialogue while the mariachi are being marched to the patrol vehicle.
PROC MIN1_MANAGE_GOING_TO_PATROL_VEHICLE_CONVERSATIONS(INT i)
	MIN1_MANAGE_SOBER_MARIACHI_DIALOGUE(i)
	IF (GET_GAME_TIMER() - iMariachiConversationTimer[i]) > iMariachiConversationDelay[i]
		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedMariachi[i])
			MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_BUMP", iNumPlayedBumpConversations[i], NUM_MARIACHI_BUMP_CONVERSATIONS)
		ELSE
			MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_WALK", iNumPlayedWalkConversations[i], NUM_MARIACHI_WALK_CONVERSATIONS)
		ENDIF
	ENDIF
	MIN1_MANAGE_JOE_JOSEF_INSULTS(i)
ENDPROC

/// PURPOSE:
///    Handles the drunken Mariachi getting into the patrol vehicle after being stunned.
PROC MIN1_MANAGE_MARIACHI_GETTING_INTO_PATROL_VEHICLE()
	INT i = 0
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
		REPEAT NUM_MARIACHI_PEDS i
			IF IS_ENTITY_ALIVE(pedMariachi[i])
				SWITCH statusMariachi[i]
					CASE MARIACHI_EXITING_HIS_VEHICLE
						IF NOT IS_PED_IN_ANY_VEHICLE(pedMariachi[i])
							SET_PED_CAN_BE_TARGETTED(pedMariachi[i], TRUE)
							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedMariachi[i], vOpenGroundWanderArea) < 25
								TASK_WANDER_IN_AREA(pedMariachi[i], vOpenGroundWanderArea, 10, 0, 0) // See B*1167158 - If mariachi car reaches the end of its route set their wander area to a specific patch of open ground
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " is in range of open ground wander area") ENDIF #ENDIF
							ELSE
								TASK_WANDER_IN_AREA(pedMariachi[i], GET_ENTITY_COORDS(pedMariachi[i]), 10, 0, 0)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " is not in range of open ground wander area") ENDIF #ENDIF
							ENDIF
							statusMariachi[i] = MARIACHI_WAITING_TO_BE_STUNNED
						ELIF iMariachiExitVehicleTimer[i] > -1
							IF (GET_GAME_TIMER() - iMariachiExitVehicleTimer[i]) > 3000 // B*1481321 - If failsafe timer has elapsed just warp them out somewhere if the door is blocked
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " told to warp out if door is blocked") ENDIF #ENDIF
								TASK_LEAVE_ANY_VEHICLE(pedMariachi[i], 0, ECF_DONT_CLOSE_DOOR)
								iMariachiExitVehicleTimer[i] = -1
							ENDIF
						ENDIF
					BREAK
					CASE MARIACHI_WAITING_TO_BE_STUNNED
						IF (GET_GAME_TIMER() - iMariachiConversationTimer[i]) > iMariachiConversationDelay[i]
							IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedMariachi[i])
							OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedMariachi[i])
								MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_AIM", iNumPlayedAimConversations[i], NUM_MARIACHI_AIM_CONVERSATIONS)
							ELIF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedMariachi[i])
								MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_BUMP", iNumPlayedBumpConversations[i], NUM_MARIACHI_BUMP_CONVERSATIONS)
							ELSE
								MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_DRNK", iNumPlayedDrunkConversations[i], NUM_MARIACHI_DRUNK_CONVERSATIONS)
							ENDIF
						ENDIF
						SET_PED_RESET_FLAG(pedMariachi[i], PRF_PreventAllMeleeTakedowns, TRUE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMariachi[i], PLAYER_PED_ID())
							IF MIN1_HAS_MARIACHI_BEEN_DAMAGED_BY_ALLOWED_WEAPON(i)
								CLEAR_PED_LAST_WEAPON_DAMAGE(pedMariachi[i])
								SAFE_REMOVE_BLIP(blipMariachi[i])
								SET_PED_CAN_BE_TARGETTED(pedMariachi[i], FALSE)
								SET_PED_CAN_BE_DRAGGED_OUT(pedMariachi[i], FALSE)
								SET_PED_MOVEMENT_CLIPSET(pedMariachi[i], "MOVE_M@BAIL_BOND_NOT_TAZERED")
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " now using MOVE_M@BAIL_BOND_NOT_TAZERED clipset") ENDIF #ENDIF
								statusMariachi[i] = MARIACHI_HAS_BEEN_STUNNED
							ENDIF
						ENDIF
					BREAK
					CASE MARIACHI_HAS_BEEN_STUNNED
						MIN1_PLAY_MARIACHI_CONVERSATION(i, "MIN1_SHOCK", iNumPlayedShockConversations[i], NUM_MARIACHI_SHOCK_CONVERSATIONS)
						MIN1_MANAGE_STUNNED_MARIACHI_DIALOGUE(i)
						IF NOT IS_PED_BEING_STUNNED(pedMariachi[i])
						AND NOT IS_PED_GETTING_UP(pedMariachi[i])
							TASK_CLEAR_LOOK_AT(pedMariachi[i])
							Make_Ped_Sober(pedMariachi[i])
							CLEAR_PED_TASKS(pedMariachi[i])
							SET_PED_CAN_BE_DRAGGED_OUT(pedMariachi[i], FALSE)
							IF i = 0
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_2", "tasered_1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TAG_SYNC_OUT)
							ELSE
								TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_2", "tasered_2", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TAG_SYNC_OUT)
							ENDIF
							statusMariachi[i] = MARIACHI_AFTER_STUNNED
						ENDIF
					BREAK
					CASE MARIACHI_AFTER_STUNNED
						MIN1_MANAGE_SOBER_MARIACHI_DIALOGUE(i)
						IF NOT IsPedPerformingTask(pedMariachi[i], SCRIPT_TASK_PLAY_ANIM)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Finished tazering anim on mariachi ", i) ENDIF #ENDIF
							pedBuddy[i].iNumInsultsGiven = 0
							pedBuddy[i].iInsultTimer = GET_GAME_TIMER() // Start the timer for Joe/Josef's insults while shepherding the Mariachi into the patrol vehicle
							pedBuddy[i].iDelayBeforeNextInsult = 5000
							statusMariachi[i] = MARIACHI_GOING_TO_PATROL_VEHICLE
						ENDIF
					BREAK
					CASE MARIACHI_GOING_TO_PATROL_VEHICLE
						IF bHasBeenTazeredAgain[i] = FALSE
						AND bCheckForTazeredAgain[i] = TRUE
						AND IS_PED_BEING_STUNNED(pedMariachi[i])
							bHasBeenTazeredAgain[i] = TRUE
							SET_PED_MOVEMENT_CLIPSET(pedMariachi[i], "MOVE_M@BAIL_BOND_TAZERED")
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " now using MOVE_M@BAIL_BOND_TAZERED clipset") ENDIF #ENDIF
						ENDIF
						IF NOT IS_PED_BEING_STUNNED(pedMariachi[i])
						AND NOT IS_PED_GETTING_UP(pedMariachi[i])
							bCheckForTazeredAgain[i] = TRUE // Don't check for tazered again until they've got up for the first time
							MIN1_MANAGE_GOING_TO_PATROL_VEHICLE_CONVERSATIONS(i)
							IF GET_DISTANCE_BETWEEN_ENTITIES(pedMariachi[i], vehPatrolVehicle) > 5
								IF NOT IS_ENTITY_PLAYING_ANIM(pedMariachi[i], "missminuteman_1ig_2", "handsup_base")
									TASK_PLAY_ANIM(pedMariachi[i], "missminuteman_1ig_2", "handsup_base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_UPPERBODY|AF_LOOPING|AF_TAG_SYNC_CONTINUOUS)
									#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " playing hands up anim") ENDIF #ENDIF
								ENDIF
								IF NOT IsPedPerformingTask(pedMariachi[i], SCRIPT_TASK_GOTO_ENTITY_OFFSET)
									IF GET_DISTANCE_BETWEEN_ENTITIES(pedMariachi[i], vehPatrolVehicle) < 20
										TASK_GOTO_ENTITY_OFFSET_XY(pedMariachi[i], vehPatrolVehicle, 20000, DEFAULT_SEEK_RADIUS, fSeekOffsetFromPatrolVehicle[i], 0.0, PEDMOVEBLENDRATIO_WALK)
										#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave mariachi ", i," a task to go to the patrol vehicle, warp time is 20s") ENDIF #ENDIF
									ELIF GET_DISTANCE_BETWEEN_ENTITIES(pedMariachi[i], vehPatrolVehicle) < 40 // B*1470057 - Longer warp time if further away
										TASK_GOTO_ENTITY_OFFSET_XY(pedMariachi[i], vehPatrolVehicle, 40000, DEFAULT_SEEK_RADIUS, fSeekOffsetFromPatrolVehicle[i], 0.0, PEDMOVEBLENDRATIO_WALK)
										#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave mariachi ", i," a task to go to the patrol vehicle, warp time is 40s") ENDIF #ENDIF
									ELSE
										TASK_GOTO_ENTITY_OFFSET_XY(pedMariachi[i], vehPatrolVehicle, 60000, DEFAULT_SEEK_RADIUS, fSeekOffsetFromPatrolVehicle[i], 0.0, PEDMOVEBLENDRATIO_WALK)
										#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave mariachi ", i," a task to go to the patrol vehicle, warp time is 60s") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELSE
								statusMariachi[i] = MARIACHI_ENTERING_PATROL_VEHICLE
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Mariachi ", i, " close to patrol vehicle so progressing to MARIACHI_ENTERING_PATROL_VEHICLE") ENDIF #ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE MARIACHI_ENTERING_PATROL_VEHICLE
						IF IS_PED_SITTING_IN_VEHICLE(pedMariachi[i], vehPatrolVehicle) // Changed to sitting in vehicle for B*899751
							SET_PED_KEEP_TASK(pedMariachi[i], TRUE)
							statusMariachi[i] = MARIACHI_SAT_IN_PATROL_VEHICLE
						ELIF IS_PED_BEING_STUNNED(pedMariachi[i])
							statusMariachi[i] = MARIACHI_GOING_TO_PATROL_VEHICLE
						ELSE
							MIN1_MANAGE_GOING_TO_PATROL_VEHICLE_CONVERSATIONS(i)
							IF IS_ENTITY_PLAYING_ANIM(pedMariachi[i], "missminuteman_1ig_2", "handsup_base")
							AND GET_DISTANCE_BETWEEN_ENTITIES(pedMariachi[i], vehPatrolVehicle) < 3
								STOP_ANIM_TASK(pedMariachi[i], "missminuteman_1ig_2", "handsup_base", REALLY_SLOW_BLEND_OUT)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping mariachi ", i, " hands up anim") ENDIF #ENDIF
							ENDIF
							IF NOT IsPedPerformingTask(pedMariachi[i], SCRIPT_TASK_ENTER_VEHICLE)
								TASK_ENTER_VEHICLE(pedMariachi[i], vehPatrolVehicle, 20000, seatMariachi[i], PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE|ECF_WARP_IF_DOOR_IS_BLOCKED)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave mariachi ", i," a task to enter the patrol vehicle, warp time is 20s") ENDIF #ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE MARIACHI_SAT_IN_PATROL_VEHICLE
						IF IS_ENTITY_PLAYING_ANIM(pedMariachi[i], "missminuteman_1ig_2", "handsup_base")
							STOP_ANIM_TASK(pedMariachi[i], "missminuteman_1ig_2", "handsup_base", INSTANT_BLEND_OUT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Stopping mariachi ", i, " hands up anim") ENDIF #ENDIF
						ENDIF
						IF NOT IS_PED_SITTING_IN_VEHICLE(pedMariachi[i], vehPatrolVehicle) // B*1235814 - Make mariachi get back in if player stuns him while in the vehicle
							statusMariachi[i] = MARIACHI_GOING_TO_PATROL_VEHICLE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if both Mariachi are stunned by stun-guns.
FUNC BOOL MIN1_ARE_BOTH_MARIACHI_STUNNED()
	IF statusMariachi[0] > MARIACHI_HAS_BEEN_STUNNED
	AND statusMariachi[1] > MARIACHI_HAS_BEEN_STUNNED
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE if Joe/Josef should leave the patrol vehicle.
FUNC BOOL MIN1_SHOULD_BUDDY_GET_OUT()
	IF MIN1_ARE_BOTH_MARIACHI_STUNNED()
		RETURN TRUE
	ENDIF
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(vehMariachiCar)
		AND GET_ENTITY_SPEED(vehPatrolVehicle) < 1
		AND GET_DISTANCE_BETWEEN_ENTITIES(vehPatrolVehicle, vehMariachiCar) < 30
			RETURN TRUE // Vehicle is almost stationary and within 30 metres
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles Joe/Josef aiming at the drunken Mariachi getting into the patrol vehicle.
PROC MIN1_MANAGE_BUDDIES_GUARDING_MARIACHI()
	INT i = 0
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
		REPEAT NUM_BUDDIES i
			IF IS_ENTITY_ALIVE(pedBuddy[i].ped)
			AND IS_ENTITY_ALIVE(pedMariachi[i])
				SWITCH pedBuddy[i].status
				
					CASE BUDDY_WAITING_TO_GET_OUT
						IF MIN1_SHOULD_BUDDY_GET_OUT()
							GIVE_WEAPON_TO_PED(pedBuddy[i].ped, WEAPONTYPE_STUNGUN, 200, TRUE)
							BRING_VEHICLE_TO_HALT(vehPatrolVehicle, 10.0, 2) // B*1397040 Prevent driving away with Josef still in the patrol vehicle
							TASK_LEAVE_ANY_VEHICLE(pedBuddy[i].ped, i*1500 + 250, ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
							pedBuddy[i].status = BUDDY_GOING_TO_MARIACHI_PED
						ENDIF
					BREAK
					
					CASE BUDDY_GOING_TO_MARIACHI_PED
						IF NOT IS_PED_IN_VEHICLE(pedBuddy[i].ped, vehPatrolVehicle)
							IF IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, pedMariachi[i], 5)
								SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_UseKinematicModeWhenStationary, TRUE) // See B*771704 - Only set this when not in a car
								pedBuddy[i].status = BUDDY_AIMING_AT_MARIACHI_PED
							ELIF NOT IsPedPerformingTask(pedBuddy[i].ped, SCRIPT_TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY)
								SET_PED_CONFIG_FLAG(pedBuddy[i].ped, PCF_UseKinematicModeWhenStationary, TRUE) // See B*771704 - Only set this when not in a car
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedBuddy[i].ped, pedMariachi[i], pedMariachi[i], PEDMOVEBLENDRATIO_RUN, FALSE)
							ENDIF
						ENDIF
					BREAK
					
					CASE BUDDY_AIMING_AT_MARIACHI_PED
						IF statusMariachi[i] = MARIACHI_SAT_IN_PATROL_VEHICLE
							pedBuddy[i].status = BUDDY_TRYING_TO_ENTER_PATROL_VEHICLE
						ELSE
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, pedMariachi[i], 6)
								pedBuddy[i].status = BUDDY_GOING_TO_MARIACHI_PED
							ELIF NOT IsPedPerformingTask(pedBuddy[i].ped, SCRIPT_TASK_AIM_GUN_AT_ENTITY)
								TASK_AIM_GUN_AT_ENTITY(pedBuddy[i].ped, pedMariachi[i], -1)
							ENDIF
						ENDIF
					BREAK
					
					CASE BUDDY_TRYING_TO_ENTER_PATROL_VEHICLE
						IF IS_VEHICLE_SEAT_FREE(vehPatrolVehicle, pedBuddy[i].seatEnd)
							IF i = JOE
								SAFE_REMOVE_BLIP(blipJoe)
								IF IS_THIS_PRINT_BEING_DISPLAYED("MIN1_10") // Park near ~b~Joe ~s~and exit the vehicle.
									CLEAR_PRINTS()
								ENDIF
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPatrolVehicle, FALSE) // When Joe is told to enter vehicle, prevent player screwing around by getting back in
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle) // Josef's seat is free but the player is in the patrol vehicle driver's seat
								AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
								AND IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 50)
								AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_NOJOSEF", CONV_PRIORITY_HIGH)
									#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_NOJOSEF conversation") ENDIF #ENDIF
								ENDIF
							ENDIF
							pedBuddy[i].status = BUDDY_ENTERING_PATROL_VEHICLE
						ELSE
							IF i = JOE // Joe needs to play dialogue and display an objective, Josef doesn't
								IF NOT DOES_BLIP_EXIST(blipJoe)
									blipJoe = CREATE_PED_BLIP(pedBuddy[i].ped, TRUE, TRUE)
									PRINT_NOW("MIN1_10", DEFAULT_GOD_TEXT_TIME, 1) // Park near ~b~Joe ~s~and exit the vehicle.
								ENDIF
								IF bStartedBringVehicleConversation = FALSE
									IF MIN1_ALLOW_CONVERSATION_TO_PLAY(TRUE, FALSE)
									AND IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 50)
									AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_BRING", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES) // No subs cos will be displaying at same time as MIN1_10 god text
										#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_BRING conversation") ENDIF #ENDIF
										bStartedBringVehicleConversation = TRUE
									ENDIF
								ELSE
									IF bStartedNoJoeConversation = FALSE
									AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
									AND IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 50)
									AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_NOJO", CONV_PRIORITY_HIGH)
										#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_NOJO conversation") ENDIF #ENDIF
										bStartedNoJoeConversation = TRUE
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 100)
								MIN1_MISSION_FAILED(FAILED_ABANDONED_JOE)
							ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 10)
								IF NOT IsPedPerformingTask(pedBuddy[i].ped, SCRIPT_TASK_GO_TO_ENTITY)
									TASK_GO_TO_ENTITY(pedBuddy[i].ped, vehPatrolVehicle, 30000, 5.0, PEDMOVEBLENDRATIO_RUN)
								ENDIF
							ELIF NOT IS_PED_FACING_PED(pedBuddy[i].ped, PLAYER_PED_ID(), 30)
								TASK_TURN_PED_TO_FACE_ENTITY(pedBuddy[i].ped, PLAYER_PED_ID())
							ELIF NOT IsPedPerformingTask(pedBuddy[i].ped, SCRIPT_TASK_STAND_STILL)
								TASK_STAND_STILL(pedBuddy[i].ped, -1)
							ENDIF
						ENDIF
					BREAK
					
					CASE BUDDY_ENTERING_PATROL_VEHICLE
						IF IS_PED_IN_VEHICLE(pedBuddy[i].ped, vehPatrolVehicle)
							SET_PED_KEEP_TASK(pedBuddy[i].ped, TRUE)
							pedBuddy[i].status = BUDDY_SAT_IN_PATROL_VEHICLE
						ELIF NOT IsPedPerformingTask(pedBuddy[i].ped, SCRIPT_TASK_ENTER_VEHICLE) // See B*929660 - Deal with the TASK_ENTER_VEHICLE stopping for whatever reason
							TASK_ENTER_VEHICLE(pedBuddy[i].ped, vehPatrolVehicle, 30000, pedBuddy[i].seatEnd, PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE|ECF_WARP_IF_DOOR_IS_BLOCKED)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave buddy ", i," a task to enter the patrol vehicle") ENDIF #ENDIF
						ENDIF
					BREAK
					
					CASE BUDDY_SAT_IN_PATROL_VEHICLE
						IF bStartedGoBackForJoeConversation = FALSE
						AND i = JOSEF
						AND statusMariachi[0] = MARIACHI_SAT_IN_PATROL_VEHICLE
						AND statusMariachi[1] = MARIACHI_SAT_IN_PATROL_VEHICLE
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle) // Player is in the patrol vehicle with Josef and the two mariachis so Josef needs to say something
						AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
						AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_GOBACK2", CONV_PRIORITY_HIGH)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_GOBACK2 conversation") ENDIF #ENDIF
							bStartedGoBackForJoeConversation = TRUE
						ENDIF
					BREAK
					
				ENDSWITCH
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the next line of dialogue to warn the player about excessively stunning the Mariachi.
PROC MIN1_PLAY_NEXT_TIMES_STUNNED_DIALOGUE()
	IF iCurrentTimesStunnedDialogue < NUM_TIMES_STUNNED_DIALOGUE
	AND (statusMariachi[0] != MARIACHI_SAT_IN_PATROL_VEHICLE OR statusMariachi[1] != MARIACHI_SAT_IN_PATROL_VEHICLE) // B*1515716 - If queued up don't allow this to play if they've both subsequently got in the vehicle
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", sTimesStunnedDialogueString[iCurrentTimesStunnedDialogue], CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_STOP conversation ", iCurrentTimesStunnedDialogue) ENDIF #ENDIF
		iCurrentTimesStunnedDialogue++
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing dialogue if the player keeps stunning the Mariachi peds.
PROC MIN1_MANAGE_EXCESSIVE_STUN_DIALOGUE()
	INT i = 0
	REPEAT NUM_MARIACHI_PEDS i
		IF statusMariachi[i] > MARIACHI_WAITING_TO_BE_STUNNED
		AND IS_ENTITY_ALIVE(pedMariachi[i])
		AND IS_PED_SHOOTING(PLAYER_PED_ID())
		AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedMariachi[i], WEAPONTYPE_STUNGUN)
			CLEAR_PED_LAST_WEAPON_DAMAGE(pedMariachi[i])
			iTimesStunnedMoreThanNecessary++
			IF iTimesStunnedMoreThanNecessary >= 8
				SET_ENTITY_HEALTH(pedMariachi[i], 0)
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: iTimesStunnedMoreThanNecessary = ", iTimesStunnedMoreThanNecessary) ENDIF #ENDIF
		ENDIF
	ENDREPEAT
	IF iTimesStunnedMoreThanNecessary < 8
	AND iTimesStunnedMoreThanNecessary > (iCurrentTimesStunnedDialogue*2)
		MIN1_PLAY_NEXT_TIMES_STUNNED_DIALOGUE()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Joe tells Trevor his task is done.
PROC MIN1_MANAGE_LEAVE_AREA_DIALOGUE()
	IF bStartedLeaveAreaConversation = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_OUTRO", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_OUTRO conversation") ENDIF #ENDIF
		bStartedLeaveAreaConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Joe/Josef say cya to Trevor.
PROC MIN1_MANAGE_CYA_DIALOGUE()
	IF bStartedCyaConversation = FALSE
	AND MIN1_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_CYA", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_CYA conversation") ENDIF #ENDIF
		REPLAY_RECORD_BACK_FOR_TIME(2.0, 3.5, REPLAY_IMPORTANCE_LOWEST)
		bStartedCyaConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Fails the mission if the player stuns anyone while the patrol vehicle is driving away. See B*1235814.
PROC MIN1_HANDLE_PLAYER_STUNNING_PEDS_WHEN_DRIVING_AWAY()
	INT i = 0
	REPEAT NUM_BUDDIES i
		IF IS_ENTITY_ALIVE(pedBuddy[i].ped)
		AND IS_PED_BEING_STUNNED(pedBuddy[i].ped)
		AND GET_ENTITY_HEALTH(pedBuddy[i].ped) > 0
			SET_ENTITY_HEALTH(pedBuddy[i].ped, 0)
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT NUM_MARIACHI_PEDS i
		IF IS_ENTITY_ALIVE(pedMariachi[i])
		AND IS_PED_BEING_STUNNED(pedMariachi[i])
		AND GET_ENTITY_HEALTH(pedMariachi[i]) > 0
			SET_ENTITY_HEALTH(pedMariachi[i], 0)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Handles the patrol vehicle driving off when Joe, Josef and both Mariachi are inside it.
PROC MIN1_MANAGE_BUDDIES_DRIVING_AWAY()
	IF pedBuddy[0].status = BUDDY_SAT_IN_PATROL_VEHICLE
	AND pedBuddy[1].status = BUDDY_SAT_IN_PATROL_VEHICLE
	AND statusMariachi[0] = MARIACHI_SAT_IN_PATROL_VEHICLE
	AND statusMariachi[1] = MARIACHI_SAT_IN_PATROL_VEHICLE
	AND IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND IS_ENTITY_ALIVE(pedBuddy[0].ped)
		SET_VEHICLE_SIREN(vehPatrolVehicle, FALSE)
		TASK_VEHICLE_DRIVE_WANDER(pedBuddy[0].ped, vehPatrolVehicle, 10, DRIVINGMODE_STOPFORCARS) // B*870427
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehPatrolVehicle, TRUE) // B*538720 Attempt 1
		SET_ENTITY_PROOFS(vehPatrolVehicle, FALSE, FALSE, FALSE, TRUE, FALSE) // B*538720 Attempt 2
		//ADD_VEHICLE_STUCK_CHECK_WITH_WARP(vehPatrolVehicle, 10, 5000, TRUE, TRUE, TRUE, 10) // Increased from 1000ms to 5000ms to fix B*1294783 // Commented out, causes B*1532500
		IF GET_VEHICLE_ENGINE_HEALTH(vehPatrolVehicle) < 500
			SET_VEHICLE_ENGINE_HEALTH(vehPatrolVehicle, 500)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle engine health set to 500") ENDIF #ENDIF
		ENDIF
		IF GET_ENTITY_HEALTH(vehPatrolVehicle) < 500
			SET_ENTITY_HEALTH(vehPatrolVehicle, 500)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle health set to 500") ENDIF #ENDIF
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_LEFT)
			SET_VEHICLE_TYRE_FIXED(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_LEFT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle tyre SC_WHEEL_CAR_FRONT_LEFT fixed") ENDIF #ENDIF
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
		SET_VEHICLE_TYRE_FIXED(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle tyre SC_WHEEL_CAR_FRONT_RIGHT fixed") ENDIF #ENDIF
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_REAR_LEFT)
		SET_VEHICLE_TYRE_FIXED(vehPatrolVehicle, SC_WHEEL_CAR_REAR_LEFT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle tyre SC_WHEEL_CAR_REAR_LEFT fixed") ENDIF #ENDIF
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_REAR_RIGHT)
		SET_VEHICLE_TYRE_FIXED(vehPatrolVehicle, SC_WHEEL_CAR_REAR_RIGHT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle tyre SC_WHEEL_CAR_REAR_RIGHT fixed") ENDIF #ENDIF
		ENDIF
		bPatrolVehicleDrivingAway = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if the player is more than 50 metres away from Joe, Josef, the patrol vehicle and both Mariachi peds.
FUNC BOOL MIN1_IS_PLAYER_FAR_AWAY_FROM_ENTITIES()
	INT i
	IF IS_ENTITY_ALIVE(vehPatrolVehicle)
	AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), vehPatrolVehicle, 50)
		RETURN FALSE
	ENDIF
	i = 0
	REPEAT NUM_BUDDIES i
		IF IS_ENTITY_ALIVE(pedBuddy[i].ped)
		AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedBuddy[i].ped, 50)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT NUM_MARIACHI_PEDS i
		IF IS_ENTITY_ALIVE(pedMariachi[i])
		AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedMariachi[i], 50)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles playing the dialogue if the player fails the mission causing Joe/Josef to flee.
PROC MIN1_MANAGE_JOE_JOSEF_FLEE_DIALOGUE()
	IF bStartedFleeConversation = FALSE
	AND failReason != FAILED_MARIACHI_ESCAPED // B*1083469 - Don't make Joe/Josef leave the car if the mariachi escape
		IF IS_ENTITY_ALIVE(pedBuddy[JOE].ped)
			ADD_PED_FOR_DIALOGUE(sConversation, 3, pedBuddy[JOE].ped, "JOE")
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_FLEE1", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_FLEE1 conversation") ENDIF #ENDIF
				bStartedFleeConversation = TRUE
			ENDIF
		ELIF IS_ENTITY_ALIVE(pedBuddy[JOSEF].ped)
			ADD_PED_FOR_DIALOGUE(sConversation, 4, pedBuddy[JOSEF].ped, "JOSEF")
			IF CREATE_CONVERSATION(sConversation, "MIN1AUD", "MIN1_FLEE2", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_FLEE2 conversation") ENDIF #ENDIF
				bStartedFleeConversation = TRUE
			ENDIF
		ELSE
			bStartedFleeConversation = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mission is failing so set alive peds to flee from the player.
PROC MIN1_SET_PED_TO_FLEE(PED_INDEX ped_to_flee)
	IF IS_ENTITY_ALIVE(ped_to_flee)
		IF IS_PED_IN_ANY_VEHICLE(ped_to_flee)
			VEHICLE_INDEX veh_ped_is_in = GET_VEHICLE_PED_IS_IN(ped_to_flee)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND veh_ped_is_in = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped is in the player's vehicle, so exit and flee") ENDIF #ENDIF
				OPEN_SEQUENCE_TASK(fleeSequence)
					TASK_LEAVE_ANY_VEHICLE(NULL, GET_RANDOM_INT_IN_RANGE(0, 250))
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 100, -1)
				CLOSE_SEQUENCE_TASK(fleeSequence)
				TASK_PERFORM_SEQUENCE(ped_to_flee, fleeSequence)
				CLEAR_SEQUENCE_TASK(fleeSequence)
			ELSE
				SET_PED_COMBAT_ATTRIBUTES(ped_to_flee, CA_LEAVE_VEHICLES, FALSE)
				SET_PED_FLEE_ATTRIBUTES(ped_to_flee, FA_USE_VEHICLE, TRUE)
				IF GET_PED_IN_VEHICLE_SEAT(veh_ped_is_in, VS_DRIVER) = ped_to_flee
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_ped_is_in)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped is the driver of the vehicle, but carrec playback is ongoing so leave it running") ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped is the driver of the vehicle, so set to drive wander") ENDIF #ENDIF
						TASK_VEHICLE_DRIVE_WANDER(ped_to_flee, veh_ped_is_in, 30, DRIVINGMODE_AVOIDCARS_RECKLESS)
					ENDIF
				ELSE
					IF GET_PED_IN_VEHICLE_SEAT(veh_ped_is_in, VS_FRONT_RIGHT) = ped_to_flee
					AND IS_VEHICLE_SEAT_FREE(veh_ped_is_in, VS_DRIVER)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped is in the front right and the driver seat is free, so shuffle across then drive wander") ENDIF #ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_ped_is_in)
							STOP_PLAYBACK_RECORDED_VEHICLE(veh_ped_is_in)
						ENDIF
						OPEN_SEQUENCE_TASK(fleeSequence)
							TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(NULL, veh_ped_is_in)
							TASK_VEHICLE_DRIVE_WANDER(NULL, veh_ped_is_in, 30, DRIVINGMODE_AVOIDCARS_RECKLESS)
						CLOSE_SEQUENCE_TASK(fleeSequence)
						TASK_PERFORM_SEQUENCE(ped_to_flee, fleeSequence)
						CLEAR_SEQUENCE_TASK(fleeSequence)
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped isn't in the player vehicle, but is sat in a rear seat or the driver seat is occupied by a non-player ped, so just flee") ENDIF #ENDIF
						TASK_SMART_FLEE_PED(ped_to_flee, PLAYER_PED_ID(), 100, -1)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: A ped isn't in any vehicle, so just flee") ENDIF #ENDIF
			TASK_SMART_FLEE_PED(ped_to_flee, PLAYER_PED_ID(), 100, -1)
		ENDIF
		SET_PED_KEEP_TASK(ped_to_flee, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the next line of dialogue to warn the player about shooting the patrol vehicle.
PROC MIN1_PLAY_NEXT_SHOT_VEHICLE_DIALOGUE()
	IF MIN1_ALLOW_CONVERSATION_TO_PLAY(FALSE)
	AND CREATE_CONVERSATION(sConversation, "MIN1AUD", sShotVehicleDialogueString[iCurrentShotVehicleDialogue], CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Started MIN1_SHOOT conversation ", iCurrentShotVehicleDialogue) ENDIF #ENDIF
		iCurrentShotVehicleDialogue++
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes both buddies and both Mariachi flee the player.
PROC MIN1_MAKE_BUDDIES_AND_MARIACHI_FLEE()
	IF failReason != FAILED_MARIACHI_ESCAPED // B*1083469 - Don't make Joe/Josef leave the car if the mariachi escape
		MIN1_SET_PED_TO_FLEE(pedBuddy[JOE].ped)
		MIN1_SET_PED_TO_FLEE(pedBuddy[JOSEF].ped)
	ENDIF
	MIN1_SET_PED_TO_FLEE(pedMariachi[0])
	MIN1_SET_PED_TO_FLEE(pedMariachi[1])
ENDPROC

/// PURPOSE:
///    Stops playing ambient anims on the mission peds.
PROC MIN1_STOP_AMBIENT_ANIMS_ON_PED(PED_INDEX ped)
	SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped, FALSE)
	SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(ped, FALSE)
	SET_PED_CAN_PLAY_GESTURE_ANIMS(ped, FALSE)
ENDPROC

/// PURPOSE:
///    Death checks for any mission critical entities.
PROC MIN1_DEATH_CHECKS()
	IF ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
		IF ENUM_TO_INT(missionState) > ENUM_TO_INT(stateGoToBar)
		AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF iWantedFailedTimer = -1
				iWantedFailedTimer = GET_GAME_TIMER()
			ELIF (GET_GAME_TIMER() - iWantedFailedTimer) > 2000
				MIN1_MISSION_FAILED(FAILED_ALERTED_COPS)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedBuddy[JOE].ped)
			IF IS_ENTITY_DEAD(pedBuddy[JOE].ped)
				MIN1_MISSION_FAILED(FAILED_JOE_DIED)
			ELSE
				MIN1_STOP_AMBIENT_ANIMS_ON_PED(pedBuddy[JOE].ped)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBuddy[JOE].ped, PLAYER_PED_ID())
					MIN1_MISSION_FAILED(FAILED_JOE_INJURED)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedBuddy[JOSEF].ped)
			IF IS_ENTITY_DEAD(pedBuddy[JOSEF].ped)
				MIN1_MISSION_FAILED(FAILED_JOSEF_DIED)
			ELSE
				MIN1_STOP_AMBIENT_ANIMS_ON_PED(pedBuddy[JOSEF].ped)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBuddy[JOSEF].ped, PLAYER_PED_ID())
					MIN1_MISSION_FAILED(FAILED_JOSEF_INJURED)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehPatrolVehicle)
			IF IS_ENTITY_DEAD(vehPatrolVehicle)
				MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_WRECKED)
			ELSE
				IF HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), vehPatrolVehicle, WEAPONTYPE_STICKYBOMB)
					MIN1_MISSION_FAILED(FAILED_THREW_EXPLOSIVES)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPatrolVehicle)
					IF ENUM_TO_INT(missionState) < ENUM_TO_INT(stateLeaveArea) // B*538720 Attempt 3
						IF IS_VEHICLE_STUCK_TIMER_UP(vehPatrolVehicle, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehPatrolVehicle, VEH_STUCK_JAMMED, JAMMED_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehPatrolVehicle, VEH_STUCK_ON_ROOF, ROOF_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehPatrolVehicle, VEH_STUCK_ON_SIDE, ROOF_TIME) // Using ROOF_TIME instead of SIDE_TIME to fix B*934538
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle is stuck") ENDIF #ENDIF
							MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_WRECKED)
						ENDIF
					ENDIF
					IF ENUM_TO_INT(missionState) < ENUM_TO_INT(stateStunMariachiPeds) // Fail mission if player shoots at patrol vehicle when not inside it, but only prior to stunning objective
					AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
					AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehPatrolVehicle, PLAYER_PED_ID())
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehPatrolVehicle, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehPatrolVehicle, WEAPONTYPE_UNARMED, GENERALWEAPON_TYPE_INVALID) // B*1433041 Don't check for damage when unarmed
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle was damaged by WEAPONTYPE_UNARMED so not incrementing iCurrentShotVehicleDialogue") ENDIF #ENDIF
							ELIF IS_ENTITY_ON_FIRE(vehPatrolVehicle) // B*1457651 B*1457673
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle was damaged by fire so just fail mission") ENDIF #ENDIF
								MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_ATTACKED)
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: vehPatrolVehicle was damaged by GENERALWEAPON_TYPE_ANYWEAPON. iCurrentShotVehicleDialogue = ",  iCurrentShotVehicleDialogue) ENDIF #ENDIF
								IF iCurrentShotVehicleDialogue = 0
								OR iCurrentShotVehicleDialogue = 1
									MIN1_PLAY_NEXT_SHOT_VEHICLE_DIALOGUE()
								ELIF MIN1_ALLOW_CONVERSATION_TO_PLAY(FALSE)
									MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_ATTACKED)
								ENDIF
							ENDIF
							CLEAR_ENTITY_LAST_WEAPON_DAMAGE(vehPatrolVehicle)
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehPatrolVehicle)
					ENDIF
					IF (GET_GAME_TIMER() - iPatrolVehicleTyresBurstTimer) > 1000 // Check for burst tyres once a second
						iPatrolVehicleTyresBurstTimer = GET_GAME_TIMER()
						IF IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_REAR_LEFT)
						AND IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_REAR_RIGHT)
						AND IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_LEFT)
						AND IS_VEHICLE_TYRE_BURST(vehPatrolVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: All vehPatrolVehicle tyres burst") ENDIF #ENDIF
							MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_ATTACKED)
						ENDIF
					ENDIF
				ELSE
					MIN1_MISSION_FAILED(FAILED_PATROL_VEHICLE_WRECKED)
				ENDIF
			ENDIF
		ENDIF
		INT i = 0
		REPEAT NUM_MARIACHI_PEDS i
			IF DOES_ENTITY_EXIST(pedMariachi[i])
				IF IS_ENTITY_DEAD(pedMariachi[i])
					MIN1_MISSION_FAILED(FAILED_MARIACHI_DIED)
				ELSE
					MIN1_STOP_AMBIENT_ANIMS_ON_PED(pedMariachi[i])
					IF NOT IS_PED_IN_ANY_VEHICLE(pedMariachi[i]) // Don't check for injuries when the player is chasing their car so he's allowed to pepper their car with gunfire
					AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMariachi[i], PLAYER_PED_ID())
					AND NOT MIN1_HAS_MARIACHI_BEEN_DAMAGED_BY_ALLOWED_WEAPON(i)
						MIN1_MISSION_FAILED(FAILED_MARIACHI_INJURED)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Main state for handling the intro cutscene.
PROC MS_INTRO()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_REQUEST_CUTSCENE("mmb_1_rcm", TRUE)
			MIN1_SETUP_JOE_JOSEF_CUTSCENE_PROPS()
			iCutsceneStage = 0
			IF IS_SCREEN_FADED_IN() // Don't do gameplay hint if skipping to intro
			AND NOT IS_REPLAY_IN_PROGRESS() // Or if replaying mission
			AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) > 7.0
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<329.287262,3403.302246,34.502598>>, <<336.421509,3385.255615,39.405560>>, 20.000000)
				SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0, 0, 0>>), TRUE, -1, 3000)															
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.45)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.01)
                SET_GAMEPLAY_HINT_FOV(25.00)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				OPEN_SEQUENCE_TASK(seqTrevorLeadIn)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(NULL)
					ENDIF
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(NULL, sRCLauncherDataLocal.pedID[0], <<0, -3, 0>>, PEDMOVEBLENDRATIO_WALK)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[0])
				CLOSE_SEQUENCE_TASK(seqTrevorLeadIn)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqTrevorLeadIn)
				CLEAR_SEQUENCE_TASK(seqTrevorLeadIn)
				iLeadInTimer = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Doing leadin focus camera") ENDIF #ENDIF
			ELSE
				iLeadInTimer = -1
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Not doing leadin focus camera") ENDIF #ENDIF
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			SWITCH iCutsceneStage
				CASE 0
					MIN1_SETUP_JOE_JOSEF_CUTSCENE_PROPS()
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
					ENDIF
					IF iLeadInTimer = -1
					OR (GET_GAME_TIMER() - iLeadInTimer) > 3000
						IF RC_IS_CUTSCENE_OK_TO_START()
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOE])
								pedBuddy[JOE].ped = sRCLauncherDataLocal.pedID[JOE]
								SET_ENTITY_AS_MISSION_ENTITY(pedBuddy[JOE].ped, TRUE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(pedBuddy[JOE].ped, "Joe", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOE))
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Joe") ENDIF #ENDIF
							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(pedBuddy[JOE].ped, "Joe", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOE))
							ENDIF
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSEF])
								pedBuddy[JOSEF].ped = sRCLauncherDataLocal.pedID[JOSEF]
								SET_ENTITY_AS_MISSION_ENTITY(pedBuddy[JOSEF].ped, TRUE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(pedBuddy[JOSEF].ped, "Josef", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOSEF))
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Josef") ENDIF #ENDIF
							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(pedBuddy[JOSEF].ped, "Josef", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOSEF))
							ENDIF
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
								vehPatrolVehicle = sRCLauncherDataLocal.vehID[0]
								SET_ENTITY_AS_MISSION_ENTITY(vehPatrolVehicle, TRUE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(vehPatrolVehicle, "MMB_Patrol_Vehicle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PRANGER)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: CU_ANIMATE_EXISTING_SCRIPT_ENTITY MMB_Patrol_Vehicle") ENDIF #ENDIF
							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(vehPatrolVehicle, "MMB_Patrol_Vehicle", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PRANGER)
							ENDIF
							IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[1])
								vehCamperVan = sRCLauncherDataLocal.vehID[1]
								SET_ENTITY_AS_MISSION_ENTITY(vehCamperVan, TRUE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(vehCamperVan, "MMB_Camper_Van", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, SURFER2)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: CU_ANIMATE_EXISTING_SCRIPT_ENTITY MMB_Camper_Van") ENDIF #ENDIF
							ELSE
								REGISTER_ENTITY_FOR_CUTSCENE(vehCamperVan, "MMB_Camper_Van", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SURFER2)
							ENDIF
							RC_CLEANUP_LAUNCHER()
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
							
							START_CUTSCENE(CUTSCENE_PROCGRASS_FORCE_HD) // For B*1268419
							IF IS_ENTITY_ALIVE(vehPatrolVehicle)
								SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(GET_ENTITY_MODEL(vehPatrolVehicle))
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: set_vehicle_model_player_will_exit_scene") ENDIF #ENDIF
							ENDIF
							iCutsceneStage++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						IF IS_GAMEPLAY_HINT_ACTIVE()
							STOP_GAMEPLAY_HINT()
						ENDIF
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<331.741241,3402.092529,34.439514>>, <<318.007355,3410.159668,38.728088>>, 5.000000, vPlayerCarRespot, fPlayerCarRespot, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespot, fPlayerCarRespot)
						RC_START_CUTSCENE_MODE(<<327.85, 3405.70, 35.73>>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF NOT DOES_ENTITY_EXIST(pedBuddy[JOE].ped)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Joe"))
							pedBuddy[JOE].ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Joe"))
							SET_ENTITY_AS_MISSION_ENTITY(pedBuddy[JOE].ped, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Grabbed pedBuddy[JOE].ped from Joe") ENDIF #ENDIF
						ENDIF		
						IF NOT DOES_ENTITY_EXIST(pedBuddy[JOSEF].ped)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Josef"))
							pedBuddy[JOSEF].ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Josef"))
							SET_ENTITY_AS_MISSION_ENTITY(pedBuddy[JOSEF].ped, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Grabbed pedBuddy[JOSEF].ped from Josef") ENDIF #ENDIF
						ENDIF		
						IF NOT DOES_ENTITY_EXIST(vehPatrolVehicle)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Patrol_Vehicle"))
							vehPatrolVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Patrol_Vehicle"))
							SET_ENTITY_AS_MISSION_ENTITY(vehPatrolVehicle, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Grabbed vehPatrolVehicle from MMB_Patrol_Vehicle") ENDIF #ENDIF
						ENDIF		
						IF NOT DOES_ENTITY_EXIST(vehCamperVan)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Camper_Van"))
							vehCamperVan = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MMB_Camper_Van"))
							SET_ENTITY_AS_MISSION_ENTITY(vehCamperVan, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Grabbed vehCamperVan from MMB_Camper_Van") ENDIF #ENDIF
						ENDIF	
					ELSE
						SET_VEHICLE_DOORS_SHUT(vehPatrolVehicle, TRUE) // See B*1340763 - Ensure the doors are closed whether skipping or not
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) // See B*1015583 - Should be a cut rather than a blend
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						MIN1_SETUP_PATROL_VEHICLE(<<321.687347,3408.640625,35.343765>>, -105.797066)
						RC_END_CUTSCENE_MODE()
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: IS_CUTSCENE_PLAYING returned false") ENDIF #ENDIF
						missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going to the bar.
PROC MS_GO_TO_BAR()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_TO_BAR" #ENDIF)
			SAFE_RELEASE_VEHICLE(vehCamperVan)
			SET_WANTED_LEVEL_MULTIPLIER(0.5)
			MIN1_RESET_PATROL_VEHICLE_VARS()
			pvStatus = PV_STATUS_GO_TO_DESTINATION
			bStartedDriveToBarConversation = FALSE
			bFinishedDriveToBarConversation = FALSE
			bStartedStopDriveConversation = FALSE
			bStartedWrongWayConversation = FALSE
			bStartedAlmostThereConversation = FALSE
			bIsInteriorPinned = FALSE
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<322,3409,35>>-<<40,40,40>>, <<322,3409,35>>+<<40,40,40>>, FALSE) // See B*1217223 - Stop trailers spawning
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
				IF IS_ENTITY_IN_RANGE_COORDS(vehPatrolVehicle, vBarOutside, 20)
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					KILL_ANY_CONVERSATION()
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
						BRING_VEHICLE_TO_HALT(vehPatrolVehicle, 10.0, 1)
					ENDIF
					missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
				ELSE
					MIN1_MANAGE_PATROL_VEHICLE_OBJECTIVES()
					MIN1_MANAGE_DRIVE_TO_BAR_CONVERSATION()
					MIN1_MANAGE_ALMOST_THERE_CONVERSATION()
					MIN1_MANAGE_WRONG_WAY_CONVERSATION()
					MIN1_MANAGE_ABANDONING_VEHICLE()
					MIN1_MANAGE_INTERIOR_PINNING()
				ENDIF
				IF IS_INTERIOR_READY(interiorBar)
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorBar
					MIN1_MISSION_FAILED(FAILED_ABANDONED_BUDDIES)
				ENDIF
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_TO_BAR" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going inside the bar.
PROC MS_GO_INSIDE_BAR()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_INSIDE_BAR" #ENDIF)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_GO_IN_BAR, "Enter bar")
			MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateGoInsideBar)
			MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(TRUE)
			// See B*1793310 - bringing across the below fix from the ng branch as requested by Fredrik at SD
			SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(<<1985.25757, 3048.66968, 46.21502>>, 7.0, 20) // B*1766485 - Ensure bar gets populated // B*1779653 Ensure peds don't visibly spawn in front of bar
			bStartedArrivedAtBarConversation = FALSE
			bStartedDrunkPedConversation = FALSE
			bStartedBumpDrunkConversation = FALSE
			iWaitConversationsPlayed = 0
			bPedGoneOutDoor = FALSE
			bDoneJanetTalk = FALSE
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			iLookAroundTimer = GET_GAME_TIMER()
			iWaitConversationTimer = GET_GAME_TIMER()
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			MIN1_DISABLE_TREVOR_AGITATION_TRIGGERS()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1991.563110,3049.608887,45.125618>>, <<1988.916260,3045.263184,48.125492>>, 5.000000)
				missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
			ELSE
				MIN1_MANAGE_ARRIVED_AT_BAR_CONVERSATION()
				MIN1_MANAGE_WAIT_CONVERSATIONS()
				MIN1_MANAGE_DRUNK_PED_OUT_DOOR()
				MIN1_MANAGE_JANET_CONVERSATION()
				MIN1_MANAGE_TREVOR_LOOKING_FOR_MARIACHI()
				MIN1_MANAGE_INTERIOR_PINNING()
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_INSIDE_BAR" #ENDIF)
			SAFE_REMOVE_BLIP(blipPatrolVehicle)
			SAFE_REMOVE_BLIP(blipDestination)
			SAFE_RELEASE_PED(pedWalkOutBar)
			SAFE_RELEASE_PED(pedJanet)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling returning back outside the bar.
PROC MS_GO_OUTSIDE_BAR()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_OUTSIDE_BAR" #ENDIF)
			KILL_ANY_CONVERSATION() // Stop the drunk ped conversation if ongoing
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
				SAFE_TELEPORT_ENTITY(vehPatrolVehicle, <<1995.878784,3058.194824,46.692181>>, 45.833717)
				SET_VEHICLE_FORWARD_SPEED(vehPatrolVehicle, 0.1)
				SET_ENTITY_HEALTH(vehPatrolVehicle, 1000)
				SET_VEHICLE_DOOR_OPEN(vehPatrolVehicle, SC_DOOR_FRONT_LEFT)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			
			MIN1_SPAWN_MARIACHI_CAR(vMariachiCar, fMariachiCar, 500)
			MIN1_CHANGE_ROAD_NODES_FOR_CAR_CHASE(FALSE)
			MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(TRUE)
			bStartedInBarConversation = FALSE
			bStartedGetOutHereConversation = FALSE
			bStartedOutsideBarConversation = FALSE
			
			iDisableReplayCameraTimer = GET_GAME_TIMER() + 1000	//Fix for bug 2223403
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			MIN1_DISABLE_TREVOR_AGITATION_TRIGGERS()
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1990.334961,3053.546143,45.112350>>, <<1989.141846,3051.732666,48.112213>>, 3.000000)
				missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
			ELSE
				MIN1_MANAGE_IN_BAR_CONVERSATION()
				MIN1_MANAGE_GET_OUT_HERE_CONVERSATION()
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_OUTSIDE_BAR" #ENDIF)
			UNPIN_INTERIOR(interiorBar)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling chasing the Mariachi van.
PROC MS_CHASE_MARIACHI_CAR()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_CHASE_MARIACHI_CAR" #ENDIF)
			MIN1_RESET_PATROL_VEHICLE_VARS()
			MIN1_RESET_CHASE_CONVERSATION_VARS()
			pvStatus = PV_STATUS_PLAYER_NOT_INSIDE
			iRequiredHealth = 500
			fFailDist = 200
			bStartedChaseVanConversation = FALSE
			bStartedSingingStream = FALSE
			IF IS_ENTITY_ALIVE(vehMariachiCar)
				IF NOT IS_AUDIO_SCENE_ACTIVE("MINUTE_01_SCENE")
					START_AUDIO_SCENE("MINUTE_01_SCENE")
				ENDIF
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehMariachiCar,"MINUTE_01_SCENE_MARIACHI_VEHICLE")
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehMariachiCar)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vehMariachiCar, TRUE) // For B*1098655
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar)
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPatrolVehicle)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Replaying from checkpoint so do a rolling start") ENDIF #ENDIF
				bDoneRollingStart = FALSE
				iRollingStartTimer = -1
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehPatrolVehicle)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Not replaying from checkpoint so don't do a rolling start") ENDIF #ENDIF
				bDoneRollingStart = TRUE
			ENDIF
			bUsePlaybackAIForMariachiCar = FALSE
			bStartedOffRoadConversation = FALSE
			bStartedCrashedConversation = FALSE
			iGetInConversationsPlayed = 0
			iStoppedConversationsPlayed = 0
			bPatrolVehicleStoppedDuringChase = FALSE
			iFarBehindConversationsPlayed = 0
			iExitedVehicleConversationsPlayed = 0
			iGetBackInVehicleConversationsPlayed = 0
			iGotBackInVehicleConversationsPlayed = 0
			bAllowPlayExitedVehicleConversation = FALSE
			bAllowPlayGetBackInVehicleConversation = FALSE
			bAllowPlayGotBackInVehicleConversation = FALSE
			bPlayedFarBehindConversation = FALSE
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			WAIT(0)
			MIN1_SET_PROPS_TO_USE_FULL_PHYSICS(TRUE)
			iSingingDialogueTimer = GET_GAME_TIMER()
			iChaseDialogueTimer = GET_GAME_TIMER()
			iMariachiCarTyresBurstTimer = GET_GAME_TIMER()
			iStunGunDialogueTimer = GET_GAME_TIMER()
			SET_RANDOM_TRAINS(FALSE)
			DELETE_ALL_TRAINS()
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(MIN1_FAST_STOP)
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
			IF IS_ENTITY_ALIVE(vehMariachiCar)
				MIN1_MANAGE_MARIACHI_CAR_RECORDING()
				MIN1_MANAGE_ABANDONING_VEHICLE()
				MIN1_MANAGE_ABANDONING_MARIACHI()
				MIN1_MANAGE_PLAYING_SINGING_STREAM()
				IF MIN1_SHOULD_MARIACHI_VEHICLE_STOP()
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0, REPLAY_IMPORTANCE_LOWEST)
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehMariachiCar)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehMariachiCar)
					ENDIF
					BRING_VEHICLE_TO_HALT(vehMariachiCar, 10.0, 1)
					REMOVE_VEHICLE_RECORDING(500, "Min1_Van")
					MIN1_STOP_ANY_CHASE_CONVERSATIONS()
					missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
				ELSE
					MIN1_MANAGE_PATROL_VEHICLE_OBJECTIVES()
					MIN1_MANAGE_OUTSIDE_BAR_CONVERSATIONS()
					MIN1_MANAGE_START_CHASE_CONVERSATION()
					MIN1_MANAGE_SINGING_DIALOGUE()
					MIN1_MANAGE_OFFROAD_CONVERSATION()
					MIN1_MANAGE_STOPPED_CONVERSATIONS()
					MIN1_MANAGE_FAR_BEHIND_CONVERSATIONS()
					MIN1_MANAGE_LEAVE_VEHICLE_CONVERSATIONS()
					MIN1_MANAGE_CHASE_CONVERSATIONS()
					MIN1_MANAGE_CRASH_CONVERSATIONS()
					MIN1_MANAGE_MARIACHI_ANIMS()
					MIN1_MANAGE_MARIACHI_CAR_FOCUS_CAMERA()
					MIN1_MANAGE_ROLLING_START()
				ENDIF
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_CHASE_MARIACHI_CAR" #ENDIF)
			SAFE_REMOVE_BLIP(blipDestination)
			SAFE_REMOVE_BLIP(blipPatrolVehicle)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(FALSE)
			MIN1_SET_PROPS_TO_USE_FULL_PHYSICS(FALSE)
			TRIGGER_MUSIC_EVENT("MM1_STOP")
			STOP_STREAM() // Mariachi singing
			REMOVE_ANIM_DICT("missminuteman_1ig_1")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling stunning the Mariachi peds.
PROC MS_STUN_MARIACHI_PEDS()
	INT i
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_STUN_MARIACHI_PEDS" #ENDIF)
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN, 200, TRUE)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN, TRUE)
				PLAY_SOUND_FRONTEND(-1, "STUN_COLLECT", "MINUTE_MAN_01_SOUNDSET")
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Gave stun gun to player") ENDIF #ENDIF
			ENDIF
			i = 0
			REPEAT NUM_MARIACHI_PEDS i
				IF IS_ENTITY_ALIVE(pedMariachi[i])
					CLEAR_ENTITY_LAST_WEAPON_DAMAGE(pedMariachi[i]) // Clear this otherwise it'll fail as soon as they leave their car if the player has shot one of them during the chase
					statusMariachi[i] = MARIACHI_EXITING_HIS_VEHICLE
					bPlayedStunnedConversation[i] = FALSE
					bPlayedSoberConversation[i] = FALSE
					bCheckForTazeredAgain[i] = FALSE
					bHasBeenTazeredAgain[i] = FALSE
					iMariachiConversationTimer[i] = GET_GAME_TIMER()
					iNumPlayedDrunkConversations[i] = 0
					iNumPlayedBumpConversations[i] = 0
					iNumPlayedAimConversations[i] = 0
					iNumPlayedShockConversations[i] = 0
					iNumPlayedWalkConversations[i] = 0
					Make_Ped_Drunk_Constant(pedMariachi[i])
					SET_PED_SUFFERS_CRITICAL_HITS(pedMariachi[i], TRUE) // B*1357897 Allow critical hits now he's out of the car
					TASK_LOOK_AT_ENTITY(pedMariachi[i], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					IF IS_PED_IN_ANY_VEHICLE(pedMariachi[i])
						TASK_LEAVE_ANY_VEHICLE(pedMariachi[i], i*1500, ECF_DONT_CLOSE_DOOR|ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED) // Time delay so they don't get out in unison // B*1481321 Don't warp if door is blocked
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipMariachi[i])
						blipMariachi[i] = CREATE_BLIP_FOR_PED(pedMariachi[i], TRUE)
					ENDIF
					iMariachiExitVehicleTimer[i] = GET_GAME_TIMER()
				ENDIF
			ENDREPEAT
			iMariachiConversationDelay[0] = 8000
			iMariachiConversationDelay[1] = 10000
			MIN1_RESET_HURRY_STUN_CONVERSATION_VARS()
			pedBuddy[0].status = BUDDY_WAITING_TO_GET_OUT
			pedBuddy[1].status = BUDDY_WAITING_TO_GET_OUT
			fFailDist = 100
			bStartedBringVehicleConversation = FALSE
			bStartedNoJoeConversation = FALSE
			bStartedGoBackForJoeConversation = FALSE
			bStartedStunThemConversation = FALSE
			iPlayedStunnedConversation = 0
			iTimesStunnedMoreThanNecessary = 0
			iCurrentTimesStunnedDialogue = 0
			sTimesStunnedDialogueString[0] = "MIN1_STOP1"
			sTimesStunnedDialogueString[1] = "MIN1_STOP2"
			sTimesStunnedDialogueString[2] = "MIN1_STOP3"
			seatMariachi[0] = VS_BACK_LEFT
			seatMariachi[1] = VS_BACK_RIGHT
			fSeekOffsetFromPatrolVehicle[0] = -3.0
			fSeekOffsetFromPatrolVehicle[1] = 3.0
			iNotStunnedDialogueTimer = GET_GAME_TIMER()
			iSingingDialogueTimer = GET_GAME_TIMER()
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			REQUEST_VEHICLE_ASSET(modelPatrolVehicle)
			REQUEST_ANIM_DICT("move_m@drunk@verydrunk") // B*1312957 - Preload drunk walk anims
			MIN1_LOAD_ASSETS_FOR_MISSION_STATE(stateStunMariachiPeds)
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_LOWEST)
			
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0) // B*476749 - Limit the likelihood that ambient cars will drive through the area when everyone is out of their vehicles
			IF MIN1_ARE_BOTH_MARIACHI_STUNNED()			
				missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
			ELSE
				MIN1_MANAGE_ABANDONING_MARIACHI()
				MIN1_MANAGE_MARIACHI_GETTING_INTO_PATROL_VEHICLE()
				MIN1_MANAGE_BUDDIES_GUARDING_MARIACHI()
				MIN1_MANAGE_STUN_THEM_DIALOGUE()
				MIN1_MANAGE_HURRY_STUN_DIALOGUE()
				MIN1_MANAGE_EXCESSIVE_STUN_DIALOGUE()
				MIN1_MANAGE_SINGING_DIALOGUE()
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_LOWEST)
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_STUN_MARIACHI_PEDS" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the the Mariachi driving away at the end of the mission.
INT iTimeOutDriveAway = -1
PROC MS_LEAVE_AREA()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_LEAVE_AREA" #ENDIF)
			MIN1_MAKE_BUDDIES_LOOK_AT_PLAYER(TRUE)
			bStartedLeaveAreaConversation = FALSE
			bStartedCyaConversation = FALSE
			bPatrolVehicleDrivingAway = FALSE
			
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP

		
			IF MIN1_IS_PLAYER_FAR_AWAY_FROM_ENTITIES()
			OR (iTimeOutDriveAway >= 0 AND GET_GAME_TIMER() >= iTimeOutDriveAway)
				PRINTSTRING("timeout = ") PRINTINT(GET_GAME_TIMER() - iTimeOutDriveAway) PRINTNL()
			
				IF IS_ENTITY_ALIVE(vehMariachiCar)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehMariachiCar)
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(MIN1_VEHICLE_TAKEN_AFTER_STUNS)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Achieved stat: MIN1_VEHICLE_TAKEN_AFTER_STUNS") ENDIF #ENDIF
				ENDIF
				missionStateMachine = MIN1_STATE_MACHINE_CLEANUP
			ELIF bPatrolVehicleDrivingAway = FALSE
				MIN1_MANAGE_ABANDONING_MARIACHI()
				MIN1_MANAGE_LEAVE_AREA_DIALOGUE()
				MIN1_MANAGE_MARIACHI_GETTING_INTO_PATROL_VEHICLE()
				MIN1_MANAGE_BUDDIES_GUARDING_MARIACHI()
				MIN1_MANAGE_BUDDIES_DRIVING_AWAY()
				MIN1_MANAGE_EXCESSIVE_STUN_DIALOGUE()
			ELSE
				IF iTimeOutDriveAway < 0
					iTimeOutDriveAway = GET_GAME_TIMER() + 30000
				ENDIF			
			
				MIN1_MANAGE_CYA_DIALOGUE()
				MIN1_HANDLE_PLAYER_STUNNING_PEDS_WHEN_DRIVING_AWAY()
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_CLEANUP
			MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_LEAVE_AREA" #ENDIF)
			SAFE_REMOVE_BLIP(blipJoe)
		BREAK
	ENDSWITCH
ENDPROC
	
/// PURPOSE:
///    Starts the mission failed sequence.
PROC MS_FAILED()
	SWITCH missionStateMachine
		CASE MIN1_STATE_MACHINE_SETUP
			MIN1_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
			CLEAR_PRINTS()
			CLEAR_HELP()
			MIN1_REMOVE_ALL_BLIPS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			TRIGGER_MUSIC_EVENT("MM1_FAIL")
			bStartedFleeConversation = FALSE
			MIN1_MAKE_BUDDIES_AND_MARIACHI_FLEE()
			IF IS_ENTITY_ALIVE(vehPatrolVehicle)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPatrolVehicle)
				BRING_VEHICLE_TO_HALT(vehPatrolVehicle, 10, 1)
			ENDIF
			SWITCH failReason
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_DEFAULT") ENDIF #ENDIF
				BREAK
				CASE FAILED_JOE_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_JOE_DIED") ENDIF #ENDIF
					failString = "MIN1_F1" // ~s~Joe died.
				BREAK
				CASE FAILED_JOSEF_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_JOSEF_DIED") ENDIF #ENDIF
					failString = "MIN1_F2" // ~s~Josef died.
				BREAK
				CASE FAILED_THREW_EXPLOSIVES
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_THREW_EXPLOSIVES") ENDIF #ENDIF
					failString = "MIN1_F3" // ~s~Trevor threw explosives at the patrol vehicle.
				BREAK
				CASE FAILED_PATROL_VEHICLE_WRECKED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_PATROL_VEHICLE_WRECKED") ENDIF #ENDIF
					failString = "MIN1_F4" // ~s~The patrol vehicle was destroyed.
				BREAK
				CASE FAILED_MARIACHI_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_MARIACHI_DIED") ENDIF #ENDIF
					failString = "MIN1_F5" // ~s~A mariachi died.
				BREAK
				CASE FAILED_MARIACHI_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_MARIACHI_INJURED") ENDIF #ENDIF
					failString = "MIN1_F6" // ~s~A mariachi was injured.
				BREAK
				CASE FAILED_ABANDONED_BUDDIES
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_ABANDONED_BUDDIES") ENDIF #ENDIF
					failString = "MIN1_F7" // ~s~Trevor abandoned Joe and Josef.
				BREAK
				CASE FAILED_MARIACHI_ESCAPED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_MARIACHI_ESCAPED") ENDIF #ENDIF
					failString = "MIN1_F8" // ~s~The mariachi escaped.
				BREAK
				CASE FAILED_JOE_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_JOE_INJURED") ENDIF #ENDIF
					failString = "MIN1_F9" // ~s~Joe was injured.
				BREAK
				CASE FAILED_JOSEF_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_JOSEF_INJURED") ENDIF #ENDIF
					failString = "MIN1_F10" // ~s~Josef was injured.
				BREAK
				CASE FAILED_ALERTED_COPS
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_ALERTED_COPS") ENDIF #ENDIF
					failString = "MIN1_F12" // ~s~Trevor alerted the cops.
				BREAK
				CASE FAILED_PATROL_VEHICLE_ATTACKED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_PATROL_VEHICLE_ATTACKED") ENDIF #ENDIF
					failString = "MIN1_F11" // ~s~The patrol vehicle was damaged.
				BREAK
				CASE FAILED_ABANDONED_JOE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: MISSION_FAILED reason=FAILED_ABANDONED_JOE") ENDIF #ENDIF
					failString = "MIN1_F15" // ~s~Trevor abandoned Joe.
				BREAK
			ENDSWITCH
			IF failReason = FAILED_DEFAULT
				Random_Character_Failed() // no fail reason
			ELSE
				Random_Character_Failed_With_Reason(failString)
			ENDIF
		BREAK
		CASE MIN1_STATE_MACHINE_LOOP
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				MIN1_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
				MIN1_REMOVE_ALL_PEDS(TRUE, TRUE) 
				SAFE_DELETE_VEHICLE(vehMariachiCar)
				SAFE_DELETE_VEHICLE(vehCamperVan)
				SAFE_DELETE_VEHICLE(vehPatrolVehicle)
				Script_Cleanup()
			ELSE				
				// not finished fading out: handle dialogue etc here
				MIN1_MANAGE_JOE_JOSEF_FLEE_DIALOGUE()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
	
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF bZSkipping = FALSE
		AND (GET_GAME_TIMER() - iDebugSkipTime) > 1000
		AND ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: S key pressed so passing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				MIN1_MAKE_BUDDIES_AND_MARIACHI_FLEE()
				Script_Passed()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: F key pressed so failing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				MIN1_MAKE_BUDDIES_AND_MARIACHI_FLEE()
				MIN1_MISSION_FAILED(FAILED_DEFAULT)
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: J key pressed so doing z skip forwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateIntro
						IF IS_CUTSCENE_ACTIVE()
						     STOP_CUTSCENE()
						ENDIF
					BREAK
					CASE stateGoToBar
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_GO_INSIDE_BAR)
					BREAK
					CASE stateGoInsideBar
					CASE stateGoOutsideBar
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_CHASE_CAR)
					BREAK
					CASE stateChaseMariachiCar
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_STUN_MARIACHI)
					BREAK
					CASE stateStunMariachiPeds
					CASE stateLeaveArea
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
					BREAK
				ENDSWITCH
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: P key pressed so doing z skip backwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateGoToBar
					CASE stateGoInsideBar
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_INTRO)
					BREAK
					CASE stateGoOutsideBar
					CASE stateChaseMariachiCar
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_GO_INSIDE_BAR)
					BREAK
					CASE stateStunMariachiPeds
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_CHASE_CAR)
					BREAK
					CASE stateLeaveArea
						MIN1_DEBUG_SKIP_STATE(Z_SKIP_STUN_MARIACHI)
					BREAK
				ENDSWITCH
			ENDIF
			INT i_new_state
			IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "MINUTE1: Z skip menu used so doing z skip") ENDIF #ENDIF
				MIN1_DEBUG_SKIP_STATE(i_new_state)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	MIN1_MISSION_SETUP()
	
	IF IS_REPLAY_IN_PROGRESS()
		RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE)
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_GO_TO_BAR
				START_REPLAY_SETUP(<<321.9098, 3410.6528, 35.7119>>, 243.0269)
				MIN1_DEBUG_SKIP_STATE(Z_SKIP_GO_TO_BAR)
			BREAK
			CASE CP_GO_IN_BAR
				START_REPLAY_SETUP(<<1982.9250, 3078.1924, 45.9798>>, 216.8597)
				MIN1_DEBUG_SKIP_STATE(Z_SKIP_GO_INSIDE_BAR)
			BREAK
			CASE CP_CAR_CHASE
				START_REPLAY_SETUP(<<1993.5508, 3058.1018, 46.0548>>, 40.7551)
				MIN1_DEBUG_SKIP_STATE(Z_SKIP_CHASE_CAR)
			BREAK
			CASE CP_MISSION_PASSED
				START_REPLAY_SETUP(<<2698.2053, 4135.9048, 42.8234>>, 323.4031)
				MIN1_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
			BREAK
		ENDSWITCH
	ENDIF

	WHILE(TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TCBP")
		IF iDisableReplayCameraTimer > GET_GAME_TIMER()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2258455
		ENDIF
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		MIN1_DEATH_CHECKS()
		SWITCH missionState
			CASE stateIntro
				MS_INTRO()
			BREAK
			CASE stateGoToBar
				MS_GO_TO_BAR()
			BREAK
			CASE stateGoInsideBar
				MS_GO_INSIDE_BAR()
			BREAK
			CASE stateGoOutsideBar
				MS_GO_OUTSIDE_BAR()
			BREAK
			CASE stateChaseMariachiCar
				MS_CHASE_MARIACHI_CAR()
			BREAK
			CASE stateStunMariachiPeds
				MS_STUN_MARIACHI_PEDS()
			BREAK
			CASE stateLeaveArea
				MS_LEAVE_AREA()
			BREAK
			CASE stateMissionPassed
				Script_Passed()
			BREAK
			CASE stateFailed
				MS_FAILED()
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
 ENDSCRIPT
