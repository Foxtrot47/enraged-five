USING "RC_Helper_Functions.sch"
USING "RC_Launcher_Public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

// Character enums
MODEL_NAMES mJoeModel      = GET_NPC_PED_MODEL(CHAR_JOE)
MODEL_NAMES mJosefModel    = GET_NPC_PED_MODEL(CHAR_JOSEF)
MODEL_NAMES mManuelModel   = GET_NPC_PED_MODEL(CHAR_MANUEL)
MODEL_NAMES mPatrolVehicle = PRANGER

/// PURPOSE:
///    Does common setup for vehicles uses in Minute 1
/// PARAMS:
///    mVehicle - vehicle index of vehicle we are setting up
PROC SETUP_MINUTE_1_VEHICLE(VEHICLE_INDEX mVehicle, MODEL_NAMES mModel)
	IF DOES_ENTITY_EXIST(mVehicle)
		SET_VEHICLE_ALARM(mVehicle, FALSE) 
		SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_LOCKED)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel, TRUE)
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(mVehicle, FALSE)
		SET_VEHICLE_DISABLE_TOWING(mVehicle, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Blocks the guys outside the vets at Minute3's start
FUNC SCENARIO_BLOCKING_INDEX Minute3_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<< -309.10, 6205.4, 30.00 >>, << -279.33, 6217.98, 40.00 >>)
ENDFUNC

/// PURPOSE: 
///    Setup scene for Minute 1
/// NOTES:    
///    Joe and Josef waiting near their vehicle.
FUNC BOOL SetupScene_MINUTE_1(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_PATROL_TRUCK 0
	CONST_INT MODEL_VAN			 1	
	CONST_INT MODEL_JOE  		 2
	CONST_INT MODEL_JOSEF  		 3
	
	// Variables
	MODEL_NAMES mModel[4]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_PATROL_TRUCK] = mPatrolVehicle
	mModel[MODEL_VAN] = SURFER2
	mModel[MODEL_JOE] = mJoeModel
	mModel[MODEL_JOSEF] = mJosefModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 15.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "MMB_1_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT("rcmminute1")
			
			// B*1485303 - Ensure trailer is cleared prior to the asset loading requests
			CLEAR_AREA(<< 321.69, 3408.64, 35.34 >>, 10.0, TRUE)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
			
			// B*1472689 - Streaming helper
			CLEAR_AREA(<< 321.69, 3408.64, 35.34 >>, 10.0, TRUE)
			
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmminute1")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Joe
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_JOE, <<329.3, 3404.2, 35.8>> , -174.9, "RCM MINUTE - JOE")
					//SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_HEAD, 0) // Setting a hat on Joe causes B*1509088
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_EYES, 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Josef
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[1], CHAR_JOSEF, <<328.1, 3403.8, 35.8>> , -149.6, "RCM MINUTE - JOSEF")
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[1], ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[1], ANCHOR_EYES, 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create patrol vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_PATROL_TRUCK], << 321.69, 3408.64, 35.34 >>,  -105.80)
				SETUP_MINUTE_1_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_PATROL_TRUCK])
			ENDIF
			
			// Create surfer van
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_VAN], << 329.49, 3402.71, 36.29 >>, 284.36)
				SETUP_MINUTE_1_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_VAN])
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sRCLauncherData.vehID[1], FALSE)
			ENDIF
			
			// Ready to apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK

		CASE IS_COMPLETE_SCENE
		
			// Animation shouldn't have unloaded
			IF HAS_ANIM_DICT_LOADED("rcmminute1")
			
				// Peds should be alive
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherData.pedID[0], "rcmminute1", "base_joe")
						CLEAR_PED_TASKS(sRCLauncherData.pedID[0])
						TASK_PLAY_ANIM(sRCLauncherData.pedID[0], "rcmminute1", "base_joe", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_UseKinematicModeWhenStationary, TRUE)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
					CLEAR_PED_TASKS(sRCLauncherData.pedID[1])
					TASK_PLAY_ANIM(sRCLauncherData.pedID[1], "rcmminute1", "base_josef", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[1], PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
		
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR

			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Minute 2
/// NOTE:    
///    Patrol vehicle on the middle of the road
FUNC BOOL SetupScene_MINUTE_2(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_PATROL_TRUCK 0
	
	//--------------------- Models --------------------------------------------------------
	// Variables
	MODEL_NAMES mModel[1]
	INT         iCount
	
	// Assign model names
	mModel[MODEL_PATROL_TRUCK] = mPatrolVehicle
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_NEAR_VEHICLE
			sRCLauncherData.activationRange = 7.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene = "MMB_2_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
		
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
			RETURN FALSE
		BREAK
		
		
		CASE IS_WAIT_FOR_SCENE
			
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
	
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
			RETURN FALSE
		BREAK
		
		CASE IS_CREATE_SCENE
	
			// Create patrol vehicle
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_PATROL_TRUCK], <<20.80, 4532.65, 104.66>>, 284.70)
			IF DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKED)
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sRCLauncherData.vehID[0], SC_DOOR_BOOT, FALSE)
				SET_VEHICLE_DIRT_LEVEL(sRCLauncherData.vehID[0],14.0)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_PATROL_TRUCK],TRUE)
			ENDIF
		
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
	
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: Setup scene for Minute 3
/// NOTE:    Manuel near his vehicle
FUNC BOOL SetupScene_MINUTE_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CAR 	0
	CONST_INT MODEL_TRUCK 	1
	CONST_INT MODEL_MANUEL 	2
	CONST_INT MODEL_CHAIR	3
	CONST_INT MODEL_PAPER	4
	
	// Variables
	MODEL_NAMES mModel[5]
	INT         iCount
	BOOL        bCreatedScene
	
	// Assign model names
	mModel[MODEL_CAR]    = SABREGT
	mModel[MODEL_TRUCK]  = BISON
	mModel[MODEL_MANUEL] = mManuelModel
	mModel[MODEL_CHAIR]  = PROP_TABLE_03_CHR
	mModel[MODEL_PAPER]  = PROP_CS_ROLLED_PAPER
	
	// Handle loading assets
	SWITCH eInitialSceneStage

		CASE IS_REQUEST_SCENE
	
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_AXIS//RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene = "MMB_3_RCM"
			sRCLauncherData.bPedsCritical = TRUE
			//sRCLauncherData.activationRange = 12.0
			sRCLauncherData.triggerLocate[0] = <<-303.511963,6211.809570,30.487585>>
			sRCLauncherData.triggerLocate[1] = <<20.000000,20.000000,3.000000>>
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmminute3base", "base", "beckon")
			sRCLauncherData.sAnims.vAnimPos = <<-303.73, 6211.56, 31.49>>
			sRCLauncherData.sAnims.vAnimRot = <<0,0,-2.80>>
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
			RETURN FALSE
		BREAK
		
		
		CASE IS_WAIT_FOR_SCENE
		
			//--------------------- Load everything -------------------------------------
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
			RETURN FALSE
		BREAK
		
		CASE IS_CREATE_SCENE
			
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create and freeze Manuel's chair
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[0])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[0], mModel[MODEL_CHAIR], <<-303.74, 6211.56, 30.49>>, -176.77)
				SET_ENTITY_COLLISION(sRCLauncherData.ObjID[0], FALSE)
				FREEZE_ENTITY_POSITION(sRCLauncherData.ObjID[0], TRUE)
				
				// Early out to wait a frame before creating Manuel to avoid floating issues
				RETURN FALSE
			ENDIF

			// Create Manuel
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MANUEL, sRCLauncherData.sAnims.vAnimPos , -2.80, "RCM MINUTE - MANUEL")
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[0], ANCHOR_HEAD, 0)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherData.pedID[0], FALSE)
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
					
			// Create scene vehicles
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], <<-281.93,6198.32,30.77>>, -135.32)
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_CAR], TRUE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_TRUCK], <<-291.72, 6208.33, 30.87>>, -134.501633 )
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[1], 0)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_TRUCK], TRUE)
			ENDIF
			
			// Attach newspaper to Manuel's hand
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[1])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[1], mModel[MODEL_PAPER], <<-303.74, 6211.56, 30.49>>, -176.77)
				ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.ObjID[1], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_L_HAND), <<0.12, 0.0, 0.03>>, <<90.0, 0.0, 90.0>>)
			ENDIF
			
			// Ready to apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK

		CASE IS_COMPLETE_SCENE
			
			// Release all assets no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
