USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

VECTOR vTriggerBoxStart = <<-1593.812988,5221.235352,1.944820>>
VECTOR vTriggerBoxEnd   = <<-1603.986084,5243.623535,4.940323>>
CONST_FLOAT fTriggerBoxWidth 8.0

MODEL_NAMES mContactModel = GET_NPC_PED_MODEL(CHAR_ABIGAIL)
STRING      abigailAnimDict = "rcmabigail"

ENUM ABIGAIL_CHATTER_PROGRESS
	ACP_INIT,
	ACP_WAITING,			//waiting a bit before playing a line
	ACP_WAITING_NEAR,		//waiting a bit before playing a line. Player is close to Abigail
	ACP_TURNING_TO_TALK,	//for if Player was further away, Abigail has to play the turning animation before talking, else it looks strange
	ACP_START_TALK_NOW,
	ACP_PLAYING				//playing conversation
ENDENUM

// Data struct needed for Abigail pre-engage chatter
STRUCT ABIGAIL_CHAT_DATA
	ABIGAIL_CHATTER_PROGRESS  chatStage
	structPedsForConversation convStruct
	INT                       iAbigailChatTimer
ENDSTRUCT

PROC ABIGAIL_PRE_ENGAGE_CHATTER(ABIGAIL_CHAT_DATA& chatData, PED_INDEX& pedAbigail, STRING string_to_play)
	
	IF IS_ENTITY_ALIVE(pedAbigail)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())

		SWITCH chatData.chatStage
		
			CASE ACP_INIT
				chatData.iAbigailChatTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 2000)
				ADD_PED_FOR_DIALOGUE(chatData.convStruct, 3, pedAbigail, "ABIGAIL")
				TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "base", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				chatData.chatStage = ACP_WAITING
			BREAK
			
			CASE ACP_WAITING
				IF GET_GAME_TIMER() > chatData.iAbigailChatTimer
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerBoxStart, vTriggerBoxEnd, fTriggerBoxWidth)
						TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "intro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						chatData.chatStage = ACP_TURNING_TO_TALK
					ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAbigail) < 15 // Player approaching from the right
						chatData.chatStage = ACP_START_TALK_NOW
					ENDIF
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(pedAbigail, abigailAnimDict, "outro")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedAbigail, abigailAnimDict, "outro") > 0.99
					TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
			BREAK

			CASE ACP_TURNING_TO_TALK
				IF IS_ENTITY_PLAYING_ANIM(pedAbigail, abigailAnimDict, "intro")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedAbigail, abigailAnimDict, "intro") > 0.99
					chatData.chatStage = ACP_START_TALK_NOW
				ENDIF
			BREAK
			
			CASE ACP_START_TALK_NOW
				IF CREATE_CONVERSATION(chatData.convStruct, "SONARAU", string_to_play, CONV_PRIORITY_AMBIENT_HIGH)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerBoxStart, vTriggerBoxEnd, fTriggerBoxWidth)
						TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "loop_talk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
					chatData.chatStage = ACP_PLAYING
				ENDIF
			BREAK
			
			CASE ACP_PLAYING
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_ENTITY_PLAYING_ANIM(pedAbigail, abigailAnimDict, "loop_talk")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedAbigail, abigailAnimDict, "loop_talk") > 0.99
							chatData.iAbigailChatTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 10000) 
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerBoxStart, vTriggerBoxEnd, fTriggerBoxWidth)
								TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
								chatData.chatStage = ACP_WAITING
							ELSE
								TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "loop_look", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								chatData.chatStage = ACP_WAITING_NEAR
							ENDIF
						ENDIF
					ELSE
						chatData.iAbigailChatTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(7000, 10000) 
						chatData.chatStage = ACP_WAITING
					ENDIF
				ENDIF
			BREAK
			
			CASE ACP_WAITING_NEAR
				IF IS_ENTITY_PLAYING_ANIM(pedAbigail, abigailAnimDict, "loop_look")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedAbigail, abigailAnimDict, "loop_look") > 0.99
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vTriggerBoxStart, vTriggerBoxEnd, fTriggerBoxWidth)
						IF GET_GAME_TIMER() > chatData.iAbigailChatTimer
							chatData.chatStage = ACP_START_TALK_NOW
						ENDIF
					ELSE
						TASK_PLAY_ANIM(pedAbigail, abigailAnimDict, "outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
						chatData.chatStage = ACP_WAITING
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE: 
///    Creates initial scene for Abigail 1.
FUNC BOOL SetupScene_ABIGAIL_1(g_structRCScriptArgs& sRCLauncherData)
	
	// Variables
	MODEL_NAMES mModel = mContactModel
	BOOL        bCreatedScene = FALSE
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup specific launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-1598.569580,5231.604980,1.969029>>
			sRCLauncherData.triggerLocate[1] = <<-1610.413208,5257.687012,5.965584>>
			sRCLauncherData.triggerWidth = 8.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical 			= TRUE
			sRCLauncherData.sIntroCutscene 			= "ABIGAIL_MCS_1_CONCAT"

			// Request models and anims
			REQUEST_MODEL(mModel)
			REQUEST_ANIM_DICT(abigailAnimDict)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAS_MODEL_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(abigailAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE	
	
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Abigail
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_ABIGAIL, << -1607.918, 5243.162, 3.045 >> , 125.5, "RC ABIGAIL 1")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF	
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release model
			SET_MODEL_AS_NO_LONGER_NEEDED(mModel)
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE	
ENDFUNC

/// PURPOSE: Creates initial scene for Abigail 2.
FUNC BOOL SetupScene_ABIGAIL_2(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_ABIGAIL  	0
	CONST_INT MODEL_CAR  		1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_ABIGAIL] = mContactModel
	mModel[MODEL_CAR] 	  = ASEA
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-1588.663574,5210.440430,2.488135>>
			sRCLauncherData.triggerLocate[1] = <<-1596.975220,5228.683594,5.966843>>
			sRCLauncherData.triggerWidth = 8.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical 			= TRUE
			sRCLauncherData.sIntroCutscene 			= "ABIGAIL_MCS_2"
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			REQUEST_ANIM_DICT(abigailAnimDict)
			
			// B*1462283 - Don't use the turning anims, just the base anim
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, abigailAnimDict, "base") 
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(abigailAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Abigail
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_ABIGAIL, << -1596.090, 5218.102, 3.045 >> , 125.5, "RC ABIGAIL 2")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Create car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], <<-1584.0127, 5158.5386, 18.6057>>, 210.5309)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0],VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
				SET_VEHICLE_DISABLE_TOWING(sRCLauncherData.vehID[0], TRUE)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
