//		MISSION NAME	:	Abigail1.sc
//		AUTHOR			:	
//		DESCRIPTION		:	Michael meets Abigail which unlocks diving for submarine scraps

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "replay_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_Public.sch"
USING "initial_scenes_Abigail.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct 
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

CONST_INT CP_CONFRONT_ABIGAIL 0

g_structRCScriptArgs 	sRCLauncherDataLocal
MODEL_NAMES modelAbigailCar = ASEA
VEHICLE_INDEX vehAbigail
VECTOR vAbigailVehCoords = <<-1573.7334, 5164.5078, 18.5576>>
FLOAT fAbigailVehHeading = 175.9451

ENUM MISSION_STAGE_TRACK
	MST_LEADIN,
	MST_CUTSCENE,
	MST_CONFRONT_ABIGAIL,
	MST_LOSE_WANTED_LEVEL,
	MST_ABIGAIL_FLEE,
	MST_FAILED
ENDENUM

ENUM STAGE_PROGRESS
	SP_SETUP,
	SP_RUNNING,
	SP_CLEANUP
ENDENUM

CONST_INT Z_SKIP_INTRO 0
CONST_INT Z_SKIP_ABIGAIL_ON_JETTY 1
CONST_INT Z_SKIP_ABIGAIL_IN_CAR 2
CONST_INT Z_SKIP_ABIGAIL_AT_AIRPORT 3

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 4
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

CONST_FLOAT fAbigailEscapeDist 250.0
CONST_INT ABIGAIL 0
CONST_INT MAX_FOLLOW_CONV_LINES 7
CONST_INT MAX_CHASE_CONV_LINES 15
CONST_INT FOLLOW_CONV_TIME 10000
CONST_INT CHASE_CONV_TIME 10000
CONST_FLOAT ABIGAIL_TALK_ZONE 20.0

MISSION_STAGE_TRACK msTrack = MST_LEADIN
STAGE_PROGRESS sProgress	= SP_SETUP

PED_INDEX pedAbigail
BLIP_INDEX blipAbigail
INT iControlTimer
INT iTextTimer
structPedsForConversation sDialogue
BOOL bThreatenedConv
BOOL bInCarConv
BOOL bArriveConv
BOOL bDisplayedLoseCopsObjective
INT iFollowConvTimer
INT iChaseConvTimer
SEQUENCE_INDEX seqEscapePlanA //get into vehicle, drive away
						
INT iFollowLinesSaid
INT iChaseLinesSaid
VECTOR vDriveToPoint = <<-1014.0543, -2474.0132, 19.1091>>

BOOL bCutsceneSkipped
INT iCutsceneStage

INT iAbigailHelpTextTimer

SCENARIO_BLOCKING_INDEX blockScenarios

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup(BOOL b_terminate_thread)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Script Cleanup") #ENDIF
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelAbigailCar, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelAbigailCar)
	SAFE_REMOVE_BLIP(blipAbigail)
	SAFE_RELEASE_PED(pedAbigail)
	IF IS_ENTITY_ALIVE(vehAbigail)
		SET_VEHICLE_DOORS_LOCKED(vehAbigail,VEHICLELOCK_UNLOCKED) // B*1541812 Allow player into Abigail's car if he's threatened her
	ENDIF
	SAFE_RELEASE_VEHICLE(vehAbigail)
	RC_CLEANUP_LAUNCHER()
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	REMOVE_SCENARIO_BLOCKING_AREA(blockScenarios)
	IF b_terminate_thread = TRUE
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Abigail 2: Passed")
	Random_Character_Passed()
	Script_Cleanup(TRUE)
ENDPROC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE_TRACK msStage)
	msTrack = msStage
	sProgress = SP_SETUP
ENDPROC

/// PURPOSE:
///    advance stage if jacked or car ruined
FUNC BOOL SHOULD_ABIGAIL_FLEE_PLAYER()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: SHOULD_ABIGAIL_FLEE_PLAYER - Player has a wanted level") #ENDIF
		RETURN TRUE
	ENDIF
	IF IS_ENTITY_ALIVE(vehAbigail)
		IF HAS_PLAYER_THREATENED_PED(pedAbigail)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: SHOULD_ABIGAIL_FLEE_PLAYER - HAS_PLAYER_THREATENED_PED returned true") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_PED_BEING_JACKED(pedAbigail)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: SHOULD_ABIGAIL_FLEE_PLAYER - pedAbigail is being jacked out") #ENDIF
			RETURN TRUE
		ENDIF
		IF IS_ENTITY_ALIVE(vehAbigail)
			IF IS_PED_IN_VEHICLE(pedAbigail, vehAbigail)
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehAbigail, PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: SHOULD_ABIGAIL_FLEE_PLAYER - player damaged vehAbigail when pedAbigail was inside it") #ENDIF
				RETURN TRUE
			ENDIF
			IF IS_VEHICLE_TYRE_BURST(vehAbigail, SC_WHEEL_CAR_FRONT_LEFT)
			OR IS_VEHICLE_TYRE_BURST(vehAbigail, SC_WHEEL_CAR_FRONT_RIGHT)
			OR IS_VEHICLE_TYRE_BURST(vehAbigail, SC_WHEEL_CAR_REAR_LEFT)
			OR IS_VEHICLE_TYRE_BURST(vehAbigail, SC_WHEEL_CAR_REAR_RIGHT)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: SHOULD_ABIGAIL_FLEE_PLAYER - a tyre on vehAbigail is burst") #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles displaying the hint camera and text for Abigail.
PROC MANAGE_ABIGAIL_HINT_CAMERA()
	IF IS_ENTITY_ALIVE(pedAbigail)
		IF (GET_GAME_TIMER() - iAbigailHelpTextTimer) > 1000
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT) // Prevent swinging punches when using hint camera
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedAbigail, "ABGAIL2_HINT") // Press ~INPUT_VEH_CIN_CAM~ to toggle focus on Abigail. // B*1508037 - No hint cam when on foot
		ENDIF
	ELSE
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF
ENDPROC

/// PURPOSE:
///    Wait for any leadin to finish here
PROC STAGE_LEADIN()
	REQUEST_ADDITIONAL_TEXT("ABGAIL2", MISSION_TEXT_SLOT)
	REQUEST_MODEL(modelAbigailCar)
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() // Wait for any pre-engage comment to finish
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	AND HAS_MODEL_LOADED(modelAbigailCar)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Pre-mission conversation finished and assets loaded") #ENDIF
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelAbigailCar, TRUE)
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			vehAbigail = sRCLauncherDataLocal.vehID[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehAbigail, TRUE, TRUE)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Grabbed vehAbigail from sRCLauncherDataLocal.vehID[0]") #ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(modelAbigailCar)
		ELSE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Couldn't vehAbigail from sRCLauncherDataLocal.vehID[0]") #ENDIF
		ENDIF
		msTrack = MST_CUTSCENE
	ELSE
		RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the scripted cutscene
PROC STAGE_CUTSCENE()
	SWITCH sProgress
	
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Starting state MST_CUTSCENE") #ENDIF
			RC_REQUEST_CUTSCENE("ABIGAIL_MCS_2")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			bCutsceneSkipped = FALSE
			iCutsceneStage = 0
			sProgress = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING	
			SWITCH iCutsceneStage
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						IF IS_ENTITY_ALIVE(pedAbigail)
							REGISTER_ENTITY_FOR_CUTSCENE(pedAbigail, "ABIGAIL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						RC_CLEANUP_LAUNCHER()
						START_CUTSCENE()
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-950.64136, -1507.60071, 4.17325>>, <<-953.25787, -1500.43884, 4.17084>>, 8.0, <<-970.3301, -1526.6260, 4.0877>>, 306.1)
						RC_START_CUTSCENE_MODE(<<-1592.84, 5214.04, 3.01>>)
						CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0) // B*1436470
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						REMOVE_ANIM_DICT("rcmabigail") // Pre-cutscene anims no longer required
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("ABIGAIL", GET_NPC_PED_MODEL(CHAR_ABIGAIL))
						AND IS_ENTITY_ALIVE(pedAbigail)
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedAbigail, <<-1579.27, 5194.96, 2.99>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Received Abigail exit state so set her to walk away") #ENDIF
						ENDIF
						IF WAS_CUTSCENE_SKIPPED()
							bCutsceneSkipped = TRUE
						ENDIF
					ELSE
						REPLAY_STOP_EVENT()
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Mocap cutscene finished") #ENDIF
						IF bCutsceneSkipped = TRUE
							IF IS_ENTITY_ALIVE(pedAbigail)
								CLEAR_PED_TASKS_IMMEDIATELY(pedAbigail)
								SAFE_TELEPORT_ENTITY(pedAbigail, <<-1587.11, 5204.18, 3.02>>, 210.9196)
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedAbigail, <<-1579.27, 5194.96, 2.99>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAbigail)
							ENDIF
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							WAIT(500)
						ENDIF
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
						RC_END_CUTSCENE_MODE()
						RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
						CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, 10) // B*1483000
						iAbigailHelpTextTimer = GET_GAME_TIMER()
						SET_STAGE(MST_CONFRONT_ABIGAIL)
					ENDIF
				BREAK		
			ENDSWITCH
		BREAK
		
	ENDSWITCH
ENDPROC

PROC SETUP_ABIGAIL_VEHICLE()
	IF NOT IS_ENTITY_ALIVE(vehAbigail)
		vehAbigail = CREATE_VEHICLE(modelAbigailCar, vAbigailVehCoords, fAbigailVehHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(modelAbigailCar)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Created new vehAbigail as it didn't exist") #ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(vehAbigail) // Check again as we might have grabbed it from sRCLauncherDataLocal.vehID[0]
		SET_VEHICLE_DOORS_LOCKED(vehAbigail,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		SET_VEHICLE_COLOUR_COMBINATION(vehAbigail, 1)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelAbigailCar, TRUE)
		SET_VEHICLE_DISABLE_TOWING(vehAbigail, TRUE)
	ENDIF
ENDPROC

PROC SETUP_ABIGAIL_PED()
	IF IS_ENTITY_ALIVE(pedAbigail)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAbigail, TRUE)
		ADD_PED_FOR_DIALOGUE(sDialogue, 3, pedAbigail, "ABIGAIL")
		SET_PED_FLEE_ATTRIBUTES(pedAbigail, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedAbigail, FA_CAN_SCREAM, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedAbigail, FA_DISABLE_COWER, TRUE)
		SET_PED_CONFIG_FLAG(pedAbigail, PCF_DisableGoToWritheWhenInjured, TRUE)
		SET_PED_CONFIG_FLAG(pedAbigail, PCF_GetOutUndriveableVehicle, TRUE)
		SET_PED_CONFIG_FLAG(pedAbigail, PCF_GetOutBurningVehicle, TRUE)
		SET_PED_CONFIG_FLAG(pedAbigail, PCF_RunFromFiresAndExplosions, TRUE)
		SET_PED_CAN_BE_TARGETTED(pedAbigail, TRUE) // B*1508059 - Ensure Abigail can be locked on
		SET_PED_PATH_CAN_DROP_FROM_HEIGHT(pedAbigail, FALSE) // See B*1789556 - Prevent Abigail climbing over stair railings
		SET_PED_PATH_CAN_USE_CLIMBOVERS(pedAbigail, FALSE) // See B*1789556 - Prevent Abigail climbing over stair railings
	ENDIF
ENDPROC

PROC ABI2_RESET_VARS()
	iControlTimer = 0
	iTextTimer = 0
	bThreatenedConv = FALSE
	bInCarConv = FALSE
	bArriveConv = FALSE
	bDisplayedLoseCopsObjective = FALSE
	iFollowConvTimer = 0
	iChaseConvTimer = 0
	iFollowLinesSaid = 0
	iChaseLinesSaid = 0
ENDPROC

PROC STAGE_CONFRONT_ABIGAIL()
	SWITCH sProgress
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Starting state MST_CONFRONT_ABIGAIL") #ENDIF
			blockScenarios = ADD_SCENARIO_BLOCKING_AREA(<<-1578.62, 5194.44, 2.99>>-<<10, 10, 10>>, <<-1578.62, 5194.44, 2.99>>+<<10, 10, 10>>) // B*1483144 - Stop the scenario peds at the bottom of the stair spawning
			SETUP_ABIGAIL_VEHICLE()
			IF IS_ENTITY_ALIVE(pedAbigail)
				SETUP_ABIGAIL_PED()
				IF IS_VEHICLE_OK(vehAbigail)
					IF NOT DOES_BLIP_EXIST(blipAbigail)
						blipAbigail = CREATE_PED_BLIP(pedAbigail)
					ENDIF
					INIT_FLASH_BLIP_AND_TEXT(blipAbigail, "", "", iControlTimer, iTextTimer, TRUE, FALSE)
					OPEN_SEQUENCE_TASK(seqEscapePlanA)
						IF NOT IS_PED_IN_VEHICLE(pedAbigail, vehAbigail) // In case we're z skipping
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1579.27, 5194.96, 2.99>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1567.85, 5178.12, 14.78>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1571.85, 5181.21, 16.88>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1579.01, 5174.08, 18.57>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
							TASK_ENTER_VEHICLE(NULL, vehAbigail, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK) 
						ENDIF
						TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(NULL, vehAbigail, vDriveToPoint, 20.0, DRIVINGMODE_AVOIDCARS, 10.0)
						TASK_VEHICLE_PARK(NULL, vehAbigail, vDriveToPoint, 49.5670, PARK_TYPE_PULL_OVER)
						TASK_LEAVE_VEHICLE(NULL, vehAbigail, ECF_DONT_CLOSE_DOOR)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
					CLOSE_SEQUENCE_TASK(seqEscapePlanA)
					TASK_PERFORM_SEQUENCE(pedAbigail, seqEscapePlanA)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Tasking pedAbigail to drive to airport") #ENDIF
				ELSE
					TASK_WANDER_STANDARD(pedAbigail)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Tasking pedAbigail to wander") #ENDIF
				ENDIF
			ENDIF
			iFollowConvTimer = GET_GAME_TIMER()
			sProgress = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			MANAGE_ABIGAIL_HINT_CAMERA()
			IF NOT IS_ENTITY_ALIVE(pedAbigail)
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_CONFRONT_ABIGAIL - pedAbigail isn't alive") #ENDIF
					sProgress = SP_CLEANUP
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_CONFRONT_ABIGAIL - pedAbigail isn't alive but player has a wanted level") #ENDIF
					SET_STAGE(MST_LOSE_WANTED_LEVEL)
				ENDIF
			// Abigail is alive so ok to do all of the following...
			ELIF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedAbigail) > fAbigailEscapeDist
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_CONFRONT_ABIGAIL - pedAbigail more than fAbigailEscapeDist away from player") #ENDIF
					sProgress = SP_CLEANUP
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_CONFRONT_ABIGAIL - pedAbigail more than fAbigailEscapeDist away from player but player has a wanted level") #ENDIF
					SET_STAGE(MST_LOSE_WANTED_LEVEL)
				ENDIF
			ELIF SHOULD_ABIGAIL_FLEE_PLAYER()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SET_STAGE(MST_ABIGAIL_FLEE)
			ELIF IS_ENTITY_AT_COORD(pedAbigail, vDriveToPoint, <<5.0, 5.0, 5.0>>)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: pedAbigail arrived at vEndPoint") #ENDIF
				SET_STAGE(MST_LOSE_WANTED_LEVEL)
			ELSE
				FLASH_BLIP_AND_TEXT(blipAbigail, "", "", iControlTimer, iTextTimer, FALSE)
				IF bInCarConv = FALSE
					IF IS_ENTITY_ALIVE(vehAbigail)
					AND IS_PED_IN_VEHICLE(pedAbigail, vehAbigail, TRUE)
					AND IS_ENTITY_IN_RANGE_ENTITY(pedAbigail, PLAYER_PED_ID(), ABIGAIL_TALK_ZONE)
					AND CREATE_CONVERSATION(sDialogue, "SONARAU", "SONAR_INCAR", CONV_PRIORITY_MEDIUM)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Started SONAR_INCAR conversation") #ENDIF
						bInCarConv = TRUE
					ENDIF
					IF iFollowLinesSaid < MAX_FOLLOW_CONV_LINES
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (GET_GAME_TIMER() - iFollowConvTimer) > FOLLOW_CONV_TIME
					AND IS_ENTITY_IN_RANGE_ENTITY(pedAbigail, PLAYER_PED_ID(), ABIGAIL_TALK_ZONE)
					AND NOT IS_PED_RAGDOLL(pedAbigail)
					AND (CREATE_CONVERSATION(sDialogue, "SONARAU", "SONAR_FOLLOW", CONV_PRIORITY_MEDIUM))
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Started SONAR_FOLLOW conversation") #ENDIF
						iFollowConvTimer = GET_GAME_TIMER()
						iFollowLinesSaid++
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: iFollowLinesSaid = ", iFollowLinesSaid) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SP_CLEANUP			
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_ABIGAIL_BLIP_TO_RED()
	IF NOT DOES_BLIP_EXIST(blipAbigail)
	AND IS_ENTITY_ALIVE(pedAbigail)
		blipAbigail = CREATE_PED_BLIP(pedAbigail)
	ENDIF
	IF DOES_BLIP_EXIST(blipAbigail)
	AND GET_BLIP_COLOUR(blipAbigail) != BLIP_COLOUR_RED
		SET_BLIP_AS_FRIENDLY(blipAbigail, FALSE)
		SET_BLIP_COLOUR(blipAbigail, BLIP_COLOUR_RED)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Set blipAbigail to red") #ENDIF
	ENDIF		
ENDPROC

PROC STAGE_LOSE_WANTED_LEVEL()
	SWITCH sProgress
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Starting state MST_LOSE_WANTED_LEVEL") #ENDIF
			sProgress = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			MANAGE_ABIGAIL_HINT_CAMERA()
			IF NOT IS_ENTITY_ALIVE(pedAbigail)
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
				bArriveConv = TRUE
				SAFE_REMOVE_BLIP(blipAbigail)
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_LOSE_WANTED_LEVEL - pedAbigail isn't alive and player's wanted level is 0") #ENDIF
					sProgress = SP_CLEANUP
				ENDIF
			ELSE
				IF bArriveConv = FALSE
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_ENTITY_IN_RANGE_ENTITY(pedAbigail, PLAYER_PED_ID(), ABIGAIL_TALK_ZONE)
				AND CREATE_CONVERSATION(sDialogue, "SONARAU", "SONAR_ARRIVE", CONV_PRIORITY_MEDIUM)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Started SONAR_ARRIVE conversation") #ENDIF
					bArriveConv = TRUE
					SET_ABIGAIL_BLIP_TO_RED()
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedAbigail) > fAbigailEscapeDist
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_LOSE_WANTED_LEVEL - pedAbigail more than fAbigailEscapeDist away from player") #ENDIF
					sProgress = SP_CLEANUP
				ENDIF
			ENDIF
			IF bDisplayedLoseCopsObjective = FALSE
			AND bArriveConv = TRUE
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_ENTITY_ALIVE(pedAbigail) // Don't increase wanted level if player has already got a wanted level before killing Abigail
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(), 3)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
				PRINT_NOW("ABGAIL2_WANTED", DEFAULT_GOD_TEXT_TIME, 1) // "Lose the cops."
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Displayed lose cops objective") #ENDIF
				bDisplayedLoseCopsObjective = TRUE
			ENDIF
		BREAK
		
		CASE SP_CLEANUP			
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Abigail runs away on foot
PROC STAGE_ABIGAIL_FLEE()
	SWITCH sProgress
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Starting state MST_ABIGAIL_FLEE") #ENDIF
			SET_ABIGAIL_BLIP_TO_RED()
			IF IS_ENTITY_ALIVE(vehAbigail)
				SET_VEHICLE_DOORS_LOCKED(vehAbigail,VEHICLELOCK_UNLOCKED) // B*1541812 Allow player into Abigail's car if he's threatened her
			ENDIF
			sProgress = SP_RUNNING
		BREAK
		
		CASE SP_RUNNING
			MANAGE_ABIGAIL_HINT_CAMERA()
			IF NOT IS_ENTITY_ALIVE(pedAbigail)
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_ABIGAIL_FLEE - pedAbigail isn't alive") #ENDIF
					sProgress = SP_CLEANUP
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_ABIGAIL_FLEE - pedAbigail isn't alive but player has a wanted level") #ENDIF
					SET_STAGE(MST_LOSE_WANTED_LEVEL)
				ENDIF
			ELIF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedAbigail) > fAbigailEscapeDist
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) = 0
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_ABIGAIL_FLEE - pedAbigail more than fAbigailEscapeDist away from player") #ENDIF
					sProgress = SP_CLEANUP
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: STAGE_ABIGAIL_FLEE - pedAbigail more than fAbigailEscapeDist away from player but player has a wanted level") #ENDIF
					SET_STAGE(MST_LOSE_WANTED_LEVEL)
				ENDIF
			ELSE
				IF NOT IsPedPerformingTask(pedAbigail, SCRIPT_TASK_SMART_FLEE_PED)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Tasking Abigail to flee the player") #ENDIF
					TASK_SMART_FLEE_PED(pedAbigail, PLAYER_PED_ID(), 250, -1)
				ENDIF
				IF bThreatenedConv = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND IS_ENTITY_IN_RANGE_ENTITY(pedAbigail, PLAYER_PED_ID(), ABIGAIL_TALK_ZONE)
					AND NOT IS_PED_RAGDOLL(pedAbigail)
					AND CREATE_CONVERSATION(sDialogue, "SONARAU", "SONAR_SHOOTS", CONV_PRIORITY_MEDIUM)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Started SONAR_SHOOTS conversation") #ENDIF
						bThreatenedConv = TRUE
					ENDIF
				ELSE
					IF iChaseLinesSaid < MAX_CHASE_CONV_LINES
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND (GET_GAME_TIMER() - iChaseConvTimer) > CHASE_CONV_TIME
					AND IS_ENTITY_IN_RANGE_ENTITY(pedAbigail, PLAYER_PED_ID(), ABIGAIL_TALK_ZONE)
					AND NOT IS_PED_RAGDOLL(pedAbigail)
					AND CREATE_CONVERSATION(sDialogue, "SONARAU", "SONAR_CHASE", CONV_PRIORITY_MEDIUM)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Started SONAR_CHASE conversation") #ENDIF
						iChaseConvTimer = GET_GAME_TIMER()
						iChaseLinesSaid++
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: iChaseLinesSaid = ", iChaseLinesSaid) #ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SP_CLEANUP			
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Fail the mission
PROC STAGE_FAILED()
	SWITCH sProgress
		CASE SP_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Starting state MST_FAILED") #ENDIF
			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			SAFE_REMOVE_BLIP(blipAbigail)
			Random_Character_Failed()
			sProgress = SP_RUNNING
		BREAK
		CASE SP_RUNNING
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: GET_MISSION_FLOW_SAFE_TO_CLEANUP returned true") #ENDIF
				SAFE_DELETE_PED(pedAbigail)
				SAFE_DELETE_VEHICLE(vehAbigail)
				Script_Cleanup(TRUE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Ensures the player, Abigail and her car are in the correct places for the z skip.
PROC ABI2_SETUP_FOR_Z_SKIP(VECTOR v_player_ped, FLOAT f_player_ped, VECTOR v_player_car, FLOAT f_player_car, VECTOR v_abigail_ped, FLOAT f_abigail_ped, VECTOR v_abigail_car, FLOAT f_abigail_car)
	IF IS_VECTOR_ZERO(v_player_ped)
		VEHICLE_INDEX veh_player
		WHILE NOT CREATE_PLAYER_VEHICLE(veh_player, CHAR_MICHAEL, v_player_car, f_player_car)
			WAIT(0)
		ENDWHILE
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_player)
		SAFE_RELEASE_VEHICLE(veh_player)
	ELIF NOT IS_REPLAY_IN_PROGRESS()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), v_player_ped, f_player_ped)
	ENDIF
	REQUEST_MODEL(modelAbigailCar)
	WHILE NOT HAS_MODEL_LOADED(modelAbigailCar)
		WAIT(0)
	ENDWHILE
	WHILE NOT RC_CREATE_NPC_PED(pedAbigail, CHAR_ABIGAIL, v_abigail_ped, f_abigail_ped, "RC ABIGAIL 2")
		WAIT(0)
	ENDWHILE
	vehAbigail = CREATE_VEHICLE(modelAbigailCar, v_abigail_car, f_abigail_car)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelAbigailCar, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelAbigailCar)
	SETUP_ABIGAIL_VEHICLE()
	IF IS_VECTOR_ZERO(v_abigail_ped)
		SET_PED_INTO_VEHICLE(pedAbigail, vehAbigail)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles debug skipping.
///  PARAMS:
///     i_new_state - The state to debug skip to.
PROC ABI2_DEBUG_SKIP_STATE(INT i_new_state)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Starting debug skip to ", i_new_state, "...") #ENDIF
	WAIT_FOR_CUTSCENE_TO_STOP(FALSE, FALSE, FALSE)
	RC_START_Z_SKIP()
	SAFE_DELETE_PED(pedAbigail)
	SAFE_DELETE_VEHICLE(vehAbigail)
	Script_Cleanup(FALSE)
	ABI2_RESET_VARS()
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE)
	SWITCH i_new_state
		CASE Z_SKIP_INTRO
			ABI2_SETUP_FOR_Z_SKIP(<<-1590.9642, 5211.7168, 3.0098>>, 37.4745, <<0,0,0>>, 0, <<-1596.090, 5218.102, 3.045>>, 125.5, <<-1584.0127, 5158.5386, 18.6057>>, 210.5309)
			SET_STAGE(MST_CUTSCENE)
		BREAK
		CASE Z_SKIP_ABIGAIL_ON_JETTY
			ABI2_SETUP_FOR_Z_SKIP(<<-1595.2010, 5217.4946, 3.0009>>, 208.2675, <<-1580.2358, 5169.5854, 18.5846>>, 189.4926, <<-1587.11, 5204.18, 3.02>>, -143.87, vAbigailVehCoords, fAbigailVehHeading)
			SET_STAGE(MST_CONFRONT_ABIGAIL)
		BREAK
		CASE Z_SKIP_ABIGAIL_IN_CAR
			ABI2_SETUP_FOR_Z_SKIP(<<0,0,0>>, 0, <<-1580.2358, 5169.5854, 18.5846>>, 189.4926, <<0,0,0>>, 0, vAbigailVehCoords, fAbigailVehHeading)
			SET_STAGE(MST_CONFRONT_ABIGAIL)
		BREAK
		CASE Z_SKIP_ABIGAIL_AT_AIRPORT
			ABI2_SETUP_FOR_Z_SKIP(<<0,0,0>>, 0, <<-973.0429, -2405.9248, 19.1698>>, 151.3282, <<0,0,0>>, 0, <<-982.9420, -2422.9114, 19.1683>>, 148.0451)
			bInCarConv = TRUE
			iFollowLinesSaid = MAX_FOLLOW_CONV_LINES
			SET_STAGE(MST_CONFRONT_ABIGAIL)
		BREAK
	ENDSWITCH
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	END_REPLAY_SETUP()
	RC_END_Z_SKIP()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Finished debug skip to ", i_new_state) #ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC DEBUG_Check_Debug_Keys()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "S key pressed so passing mission") #ENDIF
			Script_Passed()
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "F key pressed so failing mission") #ENDIF
			SET_STAGE(MST_FAILED)
		ENDIF
		INT i_new_state
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Z skip menu used so doing z skip") #ENDIF
			ABI2_DEBUG_SKIP_STATE(i_new_state)
		ENDIF
	ENDPROC
#ENDIF	

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Intro - abigail_mcs_2"    
		mSkipMenu[Z_SKIP_ABIGAIL_ON_JETTY].sTxtLabel = "Abigail on jetty"  
		mSkipMenu[Z_SKIP_ABIGAIL_IN_CAR].sTxtLabel = "Abigail in car"  
		mSkipMenu[Z_SKIP_ABIGAIL_AT_AIRPORT].sTxtLabel = "Abigail at airport"   
	#ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
		pedAbigail = sRCLauncherDataLocal.pedID[0]
		SET_ENTITY_AS_MISSION_ENTITY(pedAbigail, TRUE, TRUE)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "ABIGAIL 2: Grabbed pedAbigail from sRCLauncherDataLocal.pedID[0]") #ENDIF
	ENDIF
	
	ABI2_RESET_VARS()
	
	IF IS_REPLAY_IN_PROGRESS()
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_CONFRONT_ABIGAIL
				START_REPLAY_SETUP(<<-1595.2010, 5217.4946, 3.0009>>, 208.2675)
				ABI2_DEBUG_SKIP_STATE(Z_SKIP_ABIGAIL_ON_JETTY)
			BREAK
		ENDSWITCH
	ENDIF
	
	WHILE(TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_WLB")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SWITCH(msTrack)	
				CASE MST_LEADIN
					STAGE_LEADIN()
				BREAK
				CASE MST_CUTSCENE
					STAGE_CUTSCENE()
				BREAK
				CASE MST_CONFRONT_ABIGAIL
					STAGE_CONFRONT_ABIGAIL()
				BREAK
				CASE MST_LOSE_WANTED_LEVEL
					STAGE_LOSE_WANTED_LEVEL()
				BREAK
				CASE MST_ABIGAIL_FLEE
					STAGE_ABIGAIL_FLEE()
				BREAK
				CASE MST_FAILED
					STAGE_FAILED()
				BREAK
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT
