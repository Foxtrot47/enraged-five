
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "replay_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_Public.sch"
USING "initial_scenes_Abigail.sch"
USING "commands_recording.sch"

//		MISSION NAME	:	Abigail1.sc
//		AUTHOR			:	
//		DESCRIPTION		:	Michael meets Abigail which unlocks diving for submarine scraps

g_structRCScriptArgs sRCLauncherDataLocal
	
// Progression within a stage
ENUM STAGE_PROGRESS
	SP_SETUP,
	SP_RUNNING,
	SP_CLEANUP
ENDENUM

STRUCT CUT_CAMERA_DATA
	VECTOR position
	VECTOR angles
	FLOAT  fov
ENDSTRUCT

CONST_INT CAMERA_MICHAEL	0
CONST_INT CAMERA_ABIGAIL	1
CONST_INT MICHAEL_ID		0	// Conversation speaker ID
CONST_INT ABIGAIL_ID		3	// Conversation speaker ID

PED_INDEX pedAbigail

CAMERA_INDEX camCutscene

BOOL bSetupVehicleGen
INT iZodiacHelpTimer
INT iCutsceneStage = 0

BOOL bPlayerCloseToAbigail = FALSE

BOOL bDone1stPersonCameraFlash = FALSE

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Script Cleanup") #ENDIF
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Stopped NEW_LOAD_SCENE_START") #ENDIF
	ENDIF
	RC_CLEANUP_LAUNCHER()
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Abigail 1: Passed")
	ADD_CONTACT_TO_PHONEBOOK(CHAR_ABIGAIL, MICHAEL_BOOK)
	IF NOT bSetupVehicleGen // Ensure Zodiac is setup
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_PROPERTY_MARINA_ZODIAC, TRUE)
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_PROPERTY_MARINA_ZODIAC)
		bSetupVehicleGen = TRUE
	ENDIF
	Random_Character_Passed(CP_RAND_C_ABI1)
	Script_Cleanup()
ENDPROC

PROC PLAYER_LOCKED_INTO_CUTSCENES()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		IF bPlayerCloseToAbigail = FALSE // Conversation is playing and Dom is doing leadin anim so check for player proximity, see B*1284037
			IF IS_ENTITY_ALIVE(pedAbigail)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAbigail) < 4 // Player is very near Abigail
				OR (GET_ENTITY_SPEED(PLAYER_PED_ID()) > 2 AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAbigail) < 6) // Player is sprinting and near Abigail
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Player too close to Abigail so setting player control off") #ENDIF
					bPlayerCloseToAbigail = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the mocap cutscene
PROC STAGE_CUTSCENES()
	SWITCH iCutsceneStage
	
		CASE 0
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Starting state MST_CUTSCENES") #ENDIF
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			IF IS_ENTITY_ALIVE(pedAbigail)
				IF IS_ENTITY_PLAYING_ANIM(pedAbigail, "rcmabigail", "loop_talk")
				OR IS_ENTITY_PLAYING_ANIM(pedAbigail, "rcmabigail", "loop_look")
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Lead in is required") #ENDIF
					TASK_PLAY_ANIM(pedAbigail, "rcmabigail", "loop_2_cs", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT|AF_HOLD_LAST_FRAME)
					REMOVE_CUTSCENE()
					RC_PRE_REQUEST_CUTSCENE(TRUE)
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("ABIGAIL_MCS_1_CONCAT", CS_SECTION_2)
					iCutsceneStage = 1
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Lead in not required") #ENDIF
					RC_REQUEST_CUTSCENE("ABIGAIL_MCS_1_CONCAT", TRUE)
					iCutsceneStage = 2
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			PLAYER_LOCKED_INTO_CUTSCENES()
			IF IS_ENTITY_ALIVE(pedAbigail)
			AND IS_ENTITY_PLAYING_ANIM(pedAbigail, "rcmabigail", "loop_2_cs")
			AND GET_ENTITY_ANIM_CURRENT_TIME(pedAbigail, "rcmabigail", "loop_2_cs") > 0.99
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Lead in anim has finished") #ENDIF
				iCutsceneStage++
			ENDIF
		BREAK
		
		CASE 2
			PLAYER_LOCKED_INTO_CUTSCENES()
			IF RC_IS_CUTSCENE_OK_TO_START()
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Starting cutscene") #ENDIF
				IF IS_ENTITY_ALIVE(pedAbigail)
					REGISTER_ENTITY_FOR_CUTSCENE(pedAbigail, "ABIGAIL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				iCutsceneStage++
			ENDIF
		BREAK
		
		CASE 3
			IF IS_CUTSCENE_PLAYING()
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Cutscene is now playing") #ENDIF
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1608.165894,5246.844238,1.977510>>, <<-1604.420654,5238.246094,4.996789>>, 6.000000, <<-1596.4615, 5232.4551, 2.9794>>, 25.5125)
				SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1596.4615, 5232.4551, 2.9794>>, 25.5125)
				RC_START_CUTSCENE_MODE(<<-1604.668, 5239.100, 3.01>>)
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0) // B*1436470
				SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
				REMOVE_ANIM_DICT("rcmabigail") // Pre-cutscene anims no longer required
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_PROPERTY_MARINA_ZODIAC, TRUE) // Setup vehicle gen at the marina
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_PROPERTY_MARINA_ZODIAC)
				bSetupVehicleGen = TRUE	
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_START(<<-1606.249146,5273.964355,5.199186>>, <<-1.633685,0.000000,-175.274811>>, 300)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Started NEW_LOAD_SCENE_START") #ENDIF
				ENDIF
				iCutsceneStage++
			ENDIF
		BREAK
		
		CASE 4
			IF IS_CUTSCENE_PLAYING()
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Creating dinghy cam") #ENDIF
					camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					SET_CAM_PARAMS(camCutscene,<<-1606.249146,5273.964355,5.199186>>, <<-1.633685,0.000000,-175.274811>>, 30.166092, 0)
					SET_CAM_PARAMS(camCutscene,<<-1605.680664,5274.014160,3.870102>>, <<-2.089875,0.000000,-178.063965>>, 30.166092, 10000)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SHAKE_CAM(camCutscene, "HAND_SHAKE", 0.3)
				ENDIF
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Michael walking after cutscene") #ENDIF
					REPLAY_STOP_EVENT()
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1606.5344, 5253.9814, 2.9994>>, PEDMOVEBLENDRATIO_WALK)
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<-1602.62, 5260.37, 1.41>>, -1)
				ENDIF
				IF IS_ENTITY_ALIVE(pedAbigail)
				AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Abigail")
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Abigail walking after cutscene") #ENDIF
					CLEAR_PED_TASKS(pedAbigail)
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedAbigail, <<-1581.5586, 5198.0137, 3.0091>>, PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Mocap cutscene finished, starting scripted cutscene") #ENDIF
				iZodiacHelpTimer = GET_GAME_TIMER() + 10000
				iCutsceneStage++
			ENDIF
		BREAK
		
		CASE 5
			IF bDone1stPersonCameraFlash = FALSE
			AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			AND GET_GAME_TIMER() > (iZodiacHelpTimer - 300)
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bDone1stPersonCameraFlash = TRUE
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Doing 1st person camera flash") #ENDIF
			ENDIF
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()	
			OR GET_GAME_TIMER() > iZodiacHelpTimer
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Abigail 1: Scripted cutscene finished, passing mission") #ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DIVING_HELP")
					CLEAR_HELP(TRUE)
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_ALL_CAMS()
				RC_END_CUTSCENE_MODE()
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				SAFE_DELETE_PED(pedAbigail) // B*1433754 Delete Abigail after the cutscene
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<-1602.62, 5260.37, 1.41>>, 5000)
				ENDIF
				Script_Passed()
			ELIF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DIVING_HELP")
				PRINT_HELP_FOREVER("DIVING_HELP")
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD	
	PROC DEBUG_Check_Debug_Keys()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			Script_Passed()
		ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF	

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF

	ASSIGN_PED_INDEX(pedAbigail, sRCLauncherDataLocal.pedID[0])
		
	WHILE(TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_DAS")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
		STAGE_CUTSCENES()
	ENDWHILE

ENDSCRIPT
