USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE
MODEL_NAMES 	  	mDomModel = GET_NPC_PED_MODEL(CHAR_DOM)
MODEL_NAMES 	  	mDogModel = A_C_RETRIEVER

/// PURPOSE: Create dog entity for Extreme strand
PROC CREATE_DOG_CONTACT(PED_INDEX &iPed, VECTOR vCharPos, FLOAT fCharHeading)
	CREATE_SCENE_PED(iPed, mDogModel, vCharPos, fCharHeading)
    SET_PED_DEFAULT_COMPONENT_VARIATION(iPed)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(iPed, FALSE)
	SET_PED_NAME_DEBUG(iPed, "EXTREME LAUNCHER RC")
ENDPROC

/// PURPOSE: 
///    Creates the scene for Extreme 1.
FUNC BOOL SetupScene_EXTREME_1( g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_DOG  0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT 	    iCount

	// Assign model names
	mModel[MODEL_DOG] = mDogModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.sIntroCutscene = "ES_1_RCM_P1"
			sRCLauncherData.activationRange = 15.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_extreme1", "idle_a", "idle_c")
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
			
			// Create the dog
			CREATE_DOG_CONTACT(sRCLauncherData.pedID[0], <<-188.22, 1296.10, 302.86>>, 43.70)


			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Extreme 2.
FUNC BOOL SetupScene_EXTREME_2( g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT VEHICLE_QUAD_BIKE1	0
	CONST_INT VEHICLE_QUAD_BIKE2	1
	CONST_INT VEHICLE_TRUCK			2
	
	CONST_INT MODEL_QUAD_BIKE  		0
	CONST_INT MODEL_TRUCK  			1
	CONST_INT MODEL_DOM				2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT 	    iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_QUAD_BIKE] = BLAZER
	mModel[MODEL_TRUCK] = FLATBED
	mModel[MODEL_DOM] = mDomModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 8.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "es_2_rcm_concat"
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmextreme2", "sitting_idle", "loop_punching")
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Dom
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_DOM, <<-954.19, -2760.05, 14.64>>, -31.79, "EXTREME LAUNCHER RC")
					SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.pedID[0], <<-954.20, -2760.05, 14.04>>, TRUE)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_RunFromFiresAndExplosions, FALSE)
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE)
					FREEZE_ENTITY_POSITION(sRCLauncherData.pedID[0], TRUE)
					SET_ENTITY_COLLISION(sRCLauncherData.pedID[0], FALSE)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0)
					
					// Play anim
					TASK_PLAY_ANIM(sRCLauncherData.pedID[0], sRCLauncherData.sAnims.sDictionary, sRCLauncherData.sAnims.sIdleAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_TAG_SYNC_OUT | AF_TURN_OFF_COLLISION)
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF

			// Create Quad bikes
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE1], mModel[MODEL_QUAD_BIKE], <<-950.80, -2751.98, 13.21>>, 199.05)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE1], VEHICLELOCK_LOCKED)
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE1], VEHICLE_QUAD_BIKE1)
				SET_VEHICLE_DIRT_LEVEL(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE1], 0.0)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE2])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE2], mModel[MODEL_QUAD_BIKE], <<-950.192, -2755.96, 13.3639>>, -178.182)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE2], VEHICLELOCK_LOCKED)
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE2], VEHICLE_QUAD_BIKE2)
				SET_VEHICLE_DIRT_LEVEL(sRCLauncherData.vehID[VEHICLE_QUAD_BIKE2], 0.0)
			ENDIF
	
			// Create truck
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[VEHICLE_TRUCK])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[VEHICLE_TRUCK], mModel[MODEL_TRUCK],<<-957.23, -2764.89, 14.04>>, 147.61)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[VEHICLE_TRUCK],VEHICLELOCK_LOCKED)
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[VEHICLE_TRUCK], VEHICLE_TRUCK)
				SET_VEHICLE_DIRT_LEVEL(sRCLauncherData.vehID[VEHICLE_TRUCK], 0.0)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sRCLauncherData.vehID[VEHICLE_TRUCK], FALSE)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(sRCLauncherData.vehID[VEHICLE_TRUCK], FALSE)
			ENDIF

			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Extreme 3.
FUNC BOOL SetupScene_EXTREME_3( g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_DOM	0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT 	    iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_DOM] = mDomModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-63.191788,-808.916748,320.247742>>
			sRCLauncherData.triggerLocate[1] = <<-68.890800,-813.676086,324.290039>>
			sRCLauncherData.triggerWidth = 10.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "ES_3_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmextreme3", "idle_calm", "fidget_02")
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK	
	
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Dom
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_DOM, << -63.8, -809.5, 321.8 >>, -62.0, "EXTREME LAUNCHER RC")
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_RunFromFiresAndExplosions, FALSE) // B*852674
					SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE)  // B*852674
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0], GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC


/// PURPOSE: 
///    Creates the scene for Extreme 4.
FUNC BOOL SetupScene_EXTREME_4( g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_DOG 0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT 	    iCount

	// Assign model names
	mModel[MODEL_DOG] = mDogModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data	
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 12.0
			sRCLauncherData.sIntroCutscene = "ES_4_RCM_P1"
			sRCLauncherData.bAllowVehicleActivation = FALSE
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmextreme4", "idle_a", "idle_c")
	
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE
			
			CREATE_DOG_CONTACT(sRCLauncherData.pedID[0], <<1732.27, 96.36, 170.29>>, -98.56)//<<1731.41, 96.96, 170.39>>, -98.56)
			SET_PED_CAN_LEG_IK(sRCLauncherData.pedID[0],TRUE)
			SET_PED_LEG_IK_MODE(sRCLauncherData.pedID[0],LEG_IK_FULL)
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
