
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Extreme2.sc
//		AUTHOR			:	Kev Edwards
//		DESCRIPTION		:	Sneak onto cargo plane in airport and jump out on a jetski.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"
USING "initial_scenes_Extreme.sch"
USING "area_checks.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

g_structRCScriptArgs sRCLauncherDataLocal

CONST_INT CP_FOLLOW_DOM 0
CONST_INT CP_SKYDIVE 1
CONST_INT CP_LANDED 2
CONST_INT CP_MISSION_PASSED 3 // Only used for shitskips

CONST_INT Z_SKIP_INTRO 0
CONST_INT Z_SKIP_GO_IN_AIRPORT 1
CONST_INT Z_SKIP_DRIVE_OUT_PLANE 2
CONST_INT Z_SKIP_LANDED_AFTER_SKYDIVE 3
CONST_INT Z_SKIP_MISSION_PASSED 4

ENUM EXT2_MISSION_STATE
	stateNULL,
	stateIntro,
	stateGoIntoAirport,
	stateMidtroPlane,
	stateJumpOutPlane,
	stateSkydive,
	stateGoToDom,
	stateFinalConversation,
	stateMissionPassed,
	stateFailed,
	NUM_MISSION_STATES
ENDENUM

ENUM EXT2_STATE_MACHINE
	EXT2_STATE_MACHINE_SETUP,
	EXT2_STATE_MACHINE_LOOP,
	EXT2_STATE_MACHINE_CLEANUP
ENDENUM

ENUM FAILED_REASON
	FAILED_DEFAULT = 0,
	FAILED_DOM_DIED,
	FAILED_ATV_WRECKED,
	FAILED_ABANDONED_DOM,
	FAILED_DIDNT_FOLLOW_DOM,
	FAILED_DOM_INJURED,
	FAILED_DOM_ATV_DAMAGED,
	FAILED_GUARD_INJURED,
	FAILED_DIDNT_DRIVE_ATV,
	FAILED_DOM_SCARED,
	FAILED_OUTSIDE_ZONE,
	FAILED_FAILED_SKY_DIVE,
	FAILED_DIDNT_ATTEMPT_SKYDIVE
ENDENUM

ENUM DOM_STATUS
	DOM_IN_PLANE,
	DOM_DRIVING_OUT_PLANE,
	DOM_GETTING_OFF_ATV,
	DOM_SKYDIVING,
	DOM_LANDED
ENDENUM

ENUM STATUS_DOM_ATV
	ATV_INTO_AIRPORT,
	ATV_CHASE_PLANE,
	ATV_ATTACHED
ENDENUM

ENUM STATUS_SKYDIVE
	SKYDIVE_ON_ATV,
	SKYDIVE_PARACHUTING,
	SKYDIVE_LANDING,
	SKYDIVE_LANDED_OK,
	SKYDIVE_LANDED_FAR_AWAY
ENDENUM

EXT2_MISSION_STATE missionState = stateIntro // Stores the current EXT2_MISSION_STATE
EXT2_MISSION_STATE missionStateSkip = stateNULL // Used if needing to change to a EXT2_MISSION_STATE out of sequence
EXT2_STATE_MACHINE missionStateMachine = EXT2_STATE_MACHINE_SETUP
FAILED_REASON failReason
STRING failString
DOM_STATUS domStatus = DOM_IN_PLANE
STATUS_DOM_ATV statusDomATV
STATUS_SKYDIVE statusSkydive
BOOL bZSkipping = FALSE
INT iCutsceneWaitTime
BOOL bHasChanged // variable for storing the state of "player has changed outfit"

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 4
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintToTTY = TRUE
	INT iDebugSkipTime // Prevents skipping instantaneously
#ENDIF

VECTOR vPlayerCarRespot = <<-964.1799, -2751.0134, 12.7848>>
CONST_FLOAT fPlayerCarRespot 273.2345

VECTOR vPlayerOutro = << 139.8692, 3660.7466, 30.4814 >>
CONST_FLOAT fPlayerOutro 288.2311

SEQUENCE_INDEX seqFranklinLeadIn

CAMERA_INDEX camCutscene
INT iCutsceneStage = 0
BOOL bCutsceneSkipped
INT iLeadInTimer

PED_INDEX pedDom
SEQUENCE_INDEX seqDomAnim
BOOL bStartedDomTricks

CONST_INT NUM_VEHICLE_MODELS 5
MODEL_NAMES modelVehicle[NUM_VEHICLE_MODELS]
CONST_INT EXT2_MODEL_BLAZER 0
CONST_INT EXT2_MODEL_CARGOPLANE 1
CONST_INT EXT2_MODEL_JET 2
CONST_INT EXT2_MODEL_SEASHARK 3
CONST_INT EXT2_MODEL_FLATBED 4

CONST_INT NUM_ATVS 2
VEHICLE_INDEX vehATV[NUM_ATVS]
VECTOR vATVOnPlane[NUM_ATVS]
VECTOR vATV[NUM_ATVS]
FLOAT fATV[NUM_ATVS]
BOOL bCheckPlayerATV
CONST_INT VEHICLE_PLAYER 0
CONST_INT VEHICLE_DOM 1

VEHICLE_INDEX vehAirportPlane
INT iPlayerAttachedTimer
INT iPlayerCurseTimer

CONST_INT NUM_PED_MODELS 3
MODEL_NAMES modelPed[NUM_PED_MODELS]
CONST_INT EXT2_MODEL_SECURITY 0
CONST_INT EXT2_MODEL_DOM 1
CONST_INT EXT2_MODEL_PILOT 2

VECTOR vPlayerAfterIntro = <<-948.2926, -2755.0151, 12.8068>>
CONST_FLOAT fPlayerAfterIntro 331.4214

PED_INDEX pedAirportBooth
VECTOR vAirportBoothPed = << -966.79, -2799.24, 12.96 >>
CONST_FLOAT fAirportBoothPed 58.770409

PED_INDEX pedPlanePilot[2]

BLIP_INDEX blipDom
BLIP_INDEX blipPlayerATV

VEHICLE_INDEX vehPlaneChase
VEHICLE_INDEX vehPlaneSkydive
BLIP_INDEX blipPlane
CONST_FLOAT fPlaneEnd -73.338570
VECTOR vATVOffsetFromPlane
VECTOR vPlayerRampOffset
VECTOR vDomRampOffset
FLOAT fCurrentDomPlaybackPosition
FLOAT fCurrentPlanePlaybackPosition

INT iCheckEngineTimer
VECTOR vJetPlaneEngineRear[4]
VECTOR vJetPlaneEngineFront[4]
VECTOR vCargoPlaneEngineRear[4]
VECTOR vCargoPlaneEngineFront[4]

structPedsForConversation sConversation
BOOL bStartedFirstConversation
BOOL bStartedGuardConversation
BOOL bStartedLetsGoConversation
BOOL bStartedHurryUpConversation
BOOL bStartedDrivingOutConversation
BOOL bStartedThreatenDomConversation
BOOL bStartedDidntUseATVConversation
BOOL bStartedDriveInPlaneConversation1
BOOL bStartedDriveInPlaneConversation2
BOOL bStartedDriveInPlaneConversation3
BOOL bStartedPerformTricksConversation
BOOL bStoppedPerformTricksConversation
BOOL bRestartedPerformTricksConversation
BOOL bStartedJumpedOffATVConversation
BOOL bStoppedJumpedOffATVConversation
BOOL bStartedDeployedParachuteConversation
INT iDoneRemindConversation
INT iRemindConversationTimer

BOOL bPlayerIsOnATV = FALSE
VECTOR vLandingPos = << 145.57, 3661.28, 30.49 >>
BLIP_INDEX blipCheckpoint
VECTOR vCheckpoint = << 156.0000, 3665.3, 31.6 >>
VECTOR vMarkerRotation = << -90, 0, 0>>

BOOL bDisplayedSkydiveHelpText
BOOL bDisplayedJumpOffHelpText
BOOL bDisplayedGoToLandingZoneText
BOOL bDisplayedWaitDomLandText
INT iFailedLandingTimer

PTFX_ID ptfx_gen_sparks
PTFX_ID ptfx_moving_clouds
PTFX_ID ptfx_wind_and_smoke
FLOAT fCloudsCurrentAlpha
INT iSparksSound = GET_SOUND_ID()
MODEL_NAMES modelInvisibleProp
OBJECT_INDEX objectInvisibleProp

INT iATVFallingSound = GET_SOUND_ID()

BOOL bDisplayedGetBackOnATV
INT iTimesDisplayedRideATVObjective
BOOL bShutCargoPlaneDoors

VEHICLE_INDEX vehJetski
VECTOR vJetski = <<181.8,3676.0,35>>
CONST_FLOAT fJetski -71.712914
SEQUENCE_INDEX endSequence

REL_GROUP_HASH relGroupFriendly

VEHICLE_INDEX vehIntroTruck
VECTOR vIntroTruck = <<-957.23, -2764.89, 13.99>>
CONST_FLOAT fIntroTruck 147.61

INT iDomRideOutPlaneTimer
INT iSkydiveHelpTextTimer

VEHICLE_INDEX vehForReplay

BOOL bPlayerDeployedParachute

BOOL bResetStuntValues
FLOAT fPreviousPitch
FLOAT fCurrentPitch
FLOAT fTotalPitch

BOOL bStartedRunwayMusicEvent
BOOL bStartedEnterPlaneMusicEvent
BOOL bPreparedParachuteMusicEvent
BOOL bStartedParachuteMusicEvent

BOOL bPlayingPlaneStream
FLOAT fStreamInOut = 0.0

BOOL bReachedDom
BOOL bNearToDomStoppedConversation

RC_CONV_RESTORE_STATE stateRestoreConversation
TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel
TEXT_LABEL_23 tStoredTricksConversationLabel

STREAMVOL_ID streamVolumeExtreme2

INT iSynchedScene

OBJECT_INDEX lightRigObject

/// PURPOSE:
///    Used in every EXT2_STATE_MACHINE_SETUP. Advances state machine to EXT2_STATE_MACHINE_LOOP.
PROC EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Initialising ", s_text) ENDIF #ENDIF
	bZSkipping = FALSE
	#IF IS_DEBUG_BUILD iDebugSkipTime = GET_GAME_TIMER() #ENDIF
	missionStateMachine = EXT2_STATE_MACHINE_LOOP
ENDPROC

/// PURPOSE:
///    Used in every EXT2_STATE_MACHINE_CLEANUP. Advances state machine to EXT2_STATE_MACHINE_SETUP.
PROC EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Cleaning up ", s_text) ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	IF missionStateSkip = stateNULL
		missionState = INT_TO_ENUM(EXT2_MISSION_STATE, ENUM_TO_INT(missionState) + 1)
	ELSE
		missionState = missionStateSkip
	ENDIF
	missionStateSkip = stateNULL
	missionStateMachine = EXT2_STATE_MACHINE_SETUP
ENDPROC

/// PURPOSE:
///    Safely remove all blips in this mission.
PROC EXT2_REMOVE_ALL_BLIPS()	
	SAFE_REMOVE_BLIP(blipDom)
	SAFE_REMOVE_BLIP(blipPlane)
	SAFE_REMOVE_BLIP(blipPlayerATV)
	SAFE_REMOVE_BLIP(blipCheckpoint)
ENDPROC

/// PURPOSE:
///    Models required for a mission state are all loaded when the mission state inits, but this is a safety check to ensure a model is loaded before subsequently creating an entity using it.
PROC EXT2_ENSURE_MODEL_IS_LOADED(MODEL_NAMES e_model)
	REQUEST_MODEL(e_model)
	IF NOT HAS_MODEL_LOADED(e_model)
		WHILE NOT HAS_MODEL_LOADED(e_model)
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely creates a vehicle.
PROC EXT2_CREATE_VEHICLE(VEHICLE_INDEX &veh_to_create, MODEL_NAMES e_model, VECTOR v_pos, FLOAT f_heading = 0.0, INT i_colour = -1, FLOAT f_dirt = 0.0, INT i_use_specific_colours = -1)
	IF NOT DOES_ENTITY_EXIST(veh_to_create)
		IF HAS_MODEL_LOADED(e_model)
			EXT2_ENSURE_MODEL_IS_LOADED(e_model)
			veh_to_create = CREATE_VEHICLE(e_model, v_pos, f_heading)
			IF i_colour >= 0
				SET_VEHICLE_COLOURS(veh_to_create, i_colour, i_colour)
			ENDIF
			IF i_use_specific_colours > -1
				SET_VEHICLE_COLOUR_COMBINATION(veh_to_create, i_use_specific_colours)
			ENDIF
			SET_VEHICLE_DIRT_LEVEL(veh_to_create, f_dirt)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely creates a ped.
PROC EXT2_CREATE_PED(PED_INDEX &ped_to_create, MODEL_NAMES e_model, VECTOR v_pos, FLOAT f_heading = 0.0)
	IF NOT DOES_ENTITY_EXIST(ped_to_create)
		EXT2_ENSURE_MODEL_IS_LOADED(e_model)
		ped_to_create = CREATE_PED(PEDTYPE_MISSION, e_model, v_pos, f_heading)
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped_to_create)
		REMOVE_ALL_PED_WEAPONS(ped_to_create)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely creates a ped inside a vehicle.
PROC EXT2_CREATE_PED_IN_VEHICLE(VEHICLE_INDEX veh, PED_INDEX &ped_to_create, MODEL_NAMES e_model, VEHICLE_SEAT seat)
	IF NOT DOES_ENTITY_EXIST(ped_to_create)
	AND DOES_ENTITY_EXIST(veh)
		EXT2_ENSURE_MODEL_IS_LOADED(e_model)
		ped_to_create = CREATE_PED_INSIDE_VEHICLE(veh, PEDTYPE_MISSION, e_model, seat)
		SET_PED_DEFAULT_COMPONENT_VARIATION(ped_to_create)
		REMOVE_ALL_PED_WEAPONS(ped_to_create)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all vehicles in this mission.
PROC EXT2_REMOVE_ALL_VEHICLES(BOOL b_delete = FALSE)
	IF IS_ENTITY_ALIVE(vehPlaneChase)
		SET_ENTITY_LOAD_COLLISION_FLAG(vehPlaneChase, FALSE)
	ENDIF
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehATV[VEHICLE_DOM], FALSE)
	ENDIF
	IF b_delete = TRUE
		SAFE_DELETE_VEHICLE(vehATV[VEHICLE_PLAYER])
		SAFE_DELETE_VEHICLE(vehATV[VEHICLE_DOM])
		SAFE_DELETE_VEHICLE(vehAirportPlane)
		SAFE_DELETE_VEHICLE(vehJetski)
		SAFE_DELETE_VEHICLE(vehPlaneChase)
		SAFE_DELETE_VEHICLE(vehPlaneSkydive)
		SAFE_DELETE_VEHICLE(vehIntroTruck)
		SAFE_DELETE_VEHICLE(vehForReplay)
	ELSE
		SAFE_RELEASE_VEHICLE(vehATV[VEHICLE_PLAYER])
		SAFE_RELEASE_VEHICLE(vehATV[VEHICLE_DOM])
		SAFE_RELEASE_VEHICLE(vehAirportPlane)
		SAFE_RELEASE_VEHICLE(vehJetski)
		SAFE_RELEASE_VEHICLE(vehPlaneChase)
		SAFE_RELEASE_VEHICLE(vehPlaneSkydive)
		SAFE_RELEASE_VEHICLE(vehIntroTruck)
		SAFE_RELEASE_VEHICLE(vehForReplay)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets ped to wander after mission.
PROC EXT2_MAKE_PED_WANDER()
	IF IS_ENTITY_ALIVE(pedDom)
	AND NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_WANDER_STANDARD)
	AND NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_SMART_FLEE_PED)
	AND NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_PERFORM_SEQUENCE) // Don't make Dom wander at the end of the mission when performing the task sequence
	AND NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_SMART_FLEE_PED)
	AND NOT IS_PED_IN_ANY_VEHICLE(pedDom)
	AND GET_PED_PARACHUTE_STATE(pedDom) = PPS_INVALID
		SET_PED_KEEP_TASK(pedDom, TRUE)
		TASK_WANDER_STANDARD(pedDom)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set Dom to wander") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mission is failing so set alive peds to flee from the player.
PROC EXT2_SET_PED_TO_FLEE()
	IF IS_ENTITY_ALIVE(pedDom)
	AND NOT IS_PED_IN_ANY_VEHICLE(pedDom)
	AND GET_PED_PARACHUTE_STATE(pedDom) = PPS_INVALID
		CLEAR_PED_TASKS(pedDom)
		SET_PED_KEEP_TASK(pedDom, TRUE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			TASK_SMART_FLEE_PED(pedDom, PLAYER_PED_ID(), 100, -1)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set Dom to flee") ENDIF #ENDIF
		ELSE
			TASK_WANDER_STANDARD(pedDom)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set Dom to wander") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all peds in this mission.
PROC EXT2_REMOVE_ALL_PEDS(BOOL b_delete = FALSE)
	IF IS_ENTITY_ALIVE(pedDom)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedDom, FALSE)
	ENDIF
	IF b_delete = TRUE
		SAFE_DELETE_PED(pedAirportBooth)
		SAFE_DELETE_PED(pedDom)
		SAFE_DELETE_PED(pedPlanePilot[0])
		SAFE_DELETE_PED(pedPlanePilot[1])
	ELSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID()) // Dom should flee if Franklin has died, otherwise only be set to wander
			EXT2_MAKE_PED_WANDER()
		ELSE
			EXT2_SET_PED_TO_FLEE()
		ENDIF	
		SAFE_RELEASE_PED(pedAirportBooth)
		SAFE_RELEASE_PED(pedDom)
		SAFE_RELEASE_PED(pedPlanePilot[0])
		SAFE_RELEASE_PED(pedPlanePilot[1])
	ENDIF
ENDPROC

/// PURPOSE:
///    Stop carrec on vehicle if one is active.
PROC EXT2_STOP_CARREC_IF_PLAYING(VEHICLE_INDEX veh)
	IF IS_ENTITY_ALIVE(veh)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
		STOP_PLAYBACK_RECORDED_VEHICLE(veh)
	ENDIF
ENDPROC

/// PURPOSE:
///    Unload all models used in the mission.
PROC EXT2_RELEASE_ALL_MODELS()
	INT i = 0
	REPEAT NUM_VEHICLE_MODELS i
		SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[i])
		SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[i], FALSE)
	ENDREPEAT
	i = 0
	REPEAT NUM_PED_MODELS i
		SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[i])
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(modelInvisibleProp)
ENDPROC

/// PURPOSE:
///    Sets up Franklin in his skydiving outfit or back to his normal clothes.
PROC EXT2_SET_FRANKLIN_INTO_SKYDIVING_OUTFIT(BOOL b_set_into_outfit)
	IF b_set_into_outfit = TRUE
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID()) // B*1206376 - Store current clothes before setting into parachuting outfit
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P1_SKYDIVING)
		PRELOAD_OUTFIT(PLAYER_PED_ID(), SPECIAL_P1_PARACHUTE)
		WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID()) // Screen will always be faded down in this while loop
			WAIT(0)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Still preloading outfits") ENDIF #ENDIF
		ENDWHILE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SKYDIVING, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_PARACHUTE, FALSE)
		RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), 1)
		SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN) // Set that the player has changed clothes on mission
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set Franklin into his parachuting outfit") ENDIF #ENDIF
	ELSE
		REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		IF IS_SCREEN_FADED_OUT()
			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN, bHasChanged) // Reset that the player has changed outfit on mission to how it was at the start of the mission
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Restored Franklin's outfit") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Start/stop an audio scene.
PROC EXT2_START_AUDIO_SCENE(STRING s_scene_name, BOOL b_start)
	IF b_start = TRUE
		IF NOT IS_AUDIO_SCENE_ACTIVE(s_scene_name)
			START_AUDIO_SCENE(s_scene_name)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started audio scene ", s_scene_name) ENDIF #ENDIF
		ENDIF
	ELIF IS_AUDIO_SCENE_ACTIVE(s_scene_name)
		STOP_AUDIO_SCENE(s_scene_name)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Stopped audio scene ", s_scene_name) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Allows/prevents a ped raising the airport barrier when nearby. See B*1284403.
PROC EXT2_REGISTER_BARRIER_FOR_PED(PED_INDEX ped_to_raise_barrier, BOOL b_allow_raise_barrier)
	IF IS_ENTITY_ALIVE(ped_to_raise_barrier)
		IF b_allow_raise_barrier = TRUE
			IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, ped_to_raise_barrier)
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, ped_to_raise_barrier)
			ENDIF
		ELSE
			IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, ped_to_raise_barrier)
				UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_AIRPORT_BARRIER_IN, ped_to_raise_barrier)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Turns on/off the lighting in the plane. See B*1516771.
PROC SET_LIGHT_RIG_ON_CARGO_PLANE(BOOL bSet)
	IF bSet = TRUE
		IF IS_ENTITY_ALIVE(vehPlaneSkydive)
			DISABLE_VEHCILE_DYNAMIC_AMBIENT_SCALES(vehPlaneSkydive, 255, 255)
			IF NOT DOES_ENTITY_EXIST(lightRigObject)
				lightRigObject = CREATE_OBJECT(EXILE1_LIGHTRIG, GET_ENTITY_COORDS(vehPlaneSkydive))
				SET_ENTITY_ROTATION(lightRigObject, GET_ENTITY_ROTATION(vehPlaneSkydive))
				FREEZE_ENTITY_POSITION(lightRigObject, TRUE)
				SET_ENTITY_COLLISION(lightRigObject, FALSE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Created lighting rig") ENDIF #ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(vehPlaneSkydive)
			ENABLE_VEHICLE_DYNAMIC_AMBIENT_SCALES(vehPlaneSkydive)
		ENDIF
		SAFE_DELETE_OBJECT(lightRigObject)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Deleted lighting rig") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up everything conceivably setup in this mission.
/// PARAMS:
///    bTerminateMission - TRUE if cleaning up before terminating, FALSE if cleaning up before a debug skip.
PROC EXT2_MISSION_CLEANUP(BOOL b_delete_entities = FALSE, BOOL b_terminate_mission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Cleaning up mission...") ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	STOP_SOUND(iSparksSound)
	STOP_SOUND(iATVFallingSound)
	EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_TO_PLANE", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_FOCUS_ON_PLANE", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_UP_RAMP", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_PLANE_TAKEOFF_CUTSCENE", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_INSIDE_PLANE", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_SKYDIVE", FALSE)
	EXT2_START_AUDIO_SCENE("EXTREME_02_OPEN_PARACHUTE", FALSE)
	STOP_STREAM()
	RELEASE_SCRIPT_AUDIO_BANK()
	EXT2_STOP_CARREC_IF_PLAYING(vehPlaneChase)
	EXT2_STOP_CARREC_IF_PLAYING(vehPlaneSkydive)
	EXT2_STOP_CARREC_IF_PLAYING(vehAirportPlane)
	EXT2_STOP_CARREC_IF_PLAYING(vehATV[VEHICLE_DOM])
	REMOVE_VEHICLE_RECORDING(500, "Ext2_DomIntoAirport")
	REMOVE_VEHICLE_RECORDING(502, "Ext2_AirportVeh")
	REMOVE_VEHICLE_RECORDING(503, "Ext2_CargoTakeOff")
	REMOVE_VEHICLE_RECORDING(600, "Ext2_CargoFlight")
	REMOVE_ANIM_DICT("rcmextreme2atv")
	REMOVE_ANIM_DICT("rcmjosh1@impatient")
	EXT2_REGISTER_BARRIER_FOR_PED(PLAYER_PED_ID(), FALSE)
	EXT2_REGISTER_BARRIER_FOR_PED(pedDom, FALSE)
	SET_LIGHT_RIG_ON_CARGO_PLANE(FALSE)
	EXT2_REMOVE_ALL_BLIPS()
	EXT2_REMOVE_ALL_PEDS(b_delete_entities)
	EXT2_REMOVE_ALL_VEHICLES(b_delete_entities)
	EXT2_RELEASE_ALL_MODELS()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_moving_clouds)
		STOP_PARTICLE_FX_LOOPED(ptfx_moving_clouds)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_gen_sparks)
		STOP_PARTICLE_FX_LOOPED(ptfx_gen_sparks)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_wind_and_smoke)
		STOP_PARTICLE_FX_LOOPED(ptfx_wind_and_smoke)
	ENDIF
	REMOVE_PTFX_ASSET()
	SAFE_DELETE_OBJECT(objectInvisibleProp)
	REMOVE_VEHICLE_ASSET(modelVehicle[EXT2_MODEL_BLAZER])
	IF DOES_CAM_EXIST(camCutscene)
		DESTROY_CAM(camCutscene)
	ENDIF
	CLEAR_WEATHER_TYPE_PERSIST()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_NONE)
		REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
	ENDIF
	TRIGGER_MUSIC_EVENT("EXTREME2_STOP")
	IF STREAMVOL_IS_VALID(streamVolumeExtreme2)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Deleting streamVolumeExtreme2") ENDIF #ENDIF
		STREAMVOL_DELETE(streamVolumeExtreme2)
	ENDIF
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_AIRPORT_BARRIER_IN, FALSE)
	IF b_terminate_mission = TRUE
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
				DELETE_WIDGET_GROUP(widgetGroup)
			ENDIF
		#ENDIF
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
		ENDIF
		REMOVE_PED_FOR_DIALOGUE(sConversation, 1) // Franklin
		REMOVE_PED_FOR_DIALOGUE(sConversation, 3) // Dom
		IF DOES_SCENARIO_GROUP_EXIST("LSA_Planes") 
		AND NOT IS_SCENARIO_GROUP_ENABLED("LSA_Planes") 
			SET_SCENARIO_GROUP_ENABLED("LSA_Planes", TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Enabled scenario group LSA_Planes") ENDIF #ENDIF
		ENDIF
		RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE)
		SET_CREATE_RANDOM_COPS(TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Cleaned up mission") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Standard RC mission cleanup function.
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	IF (Random_Character_Cleanup_If_Triggered())
		EXT2_MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	// Disable sea race blip for Raton Canyon - B*1239034
	SET_BIT(g_savedGlobals.sSeaRaceData.iRaceLeaveArea, ENUM_TO_INT(SEA_RACE_CANYON))
	
	// Terminate script
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Standard RC mission passed function.
PROC Script_Passed()
	Random_Character_Passed(CP_RAND_C_EXT2)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Puts the desired ped into a group friendly to the player.
PROC EXT2_SET_PED_AS_PLAYER_FRIEND(PED_INDEX ped_to_set_as_friend)
	IF IS_ENTITY_ALIVE(ped_to_set_as_friend)
		relGroupFriendly = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(ped_to_set_as_friend, relGroupFriendly)
		SET_PED_CONFIG_FLAG(ped_to_set_as_friend, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if all the assets for the required mission stage have been loaded.
PROC EXT2_LOAD_ASSETS_FOR_MISSION_STATE(EXT2_MISSION_STATE current_state, BOOL b_wait_for_assets_to_load = TRUE)
	SWITCH current_state
		CASE stateIntro
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_BLAZER])
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_FLATBED])
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_CARGOPLANE])
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_JET])
			REQUEST_MODEL(modelPed[EXT2_MODEL_DOM])
			REQUEST_MODEL(modelPed[EXT2_MODEL_SECURITY])
			REQUEST_MODEL(modelPed[EXT2_MODEL_PILOT])
			REQUEST_MODEL(modelInvisibleProp)
			REQUEST_VEHICLE_RECORDING(500, "Ext2_DomIntoAirport")
			REQUEST_VEHICLE_RECORDING(502, "Ext2_AirportVeh")
			REQUEST_VEHICLE_RECORDING(503, "Ext2_CargoTakeOff")
			REQUEST_PTFX_ASSET()
		BREAK
		CASE stateJumpOutPlane
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_BLAZER])
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_CARGOPLANE])
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_SEASHARK])
			REQUEST_MODEL(modelPed[EXT2_MODEL_DOM])
			REQUEST_MODEL(modelPed[EXT2_MODEL_PILOT])
			REQUEST_VEHICLE_RECORDING(600, "Ext2_CargoFlight")
			REQUEST_PTFX_ASSET()
			REQUEST_ANIM_DICT("rcmextreme2atv")
			REQUEST_ANIM_DICT("rcmjosh1@impatient")
		BREAK
		CASE stateFinalConversation
			REQUEST_MODEL(modelVehicle[EXT2_MODEL_SEASHARK])
			REQUEST_ANIM_DICT("rcmextreme2")
		BREAK
	ENDSWITCH
	IF b_wait_for_assets_to_load = TRUE
		BOOL b_assets_loaded = FALSE
		WHILE b_assets_loaded = FALSE
			SWITCH current_state
				CASE stateIntro
					IF HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_BLAZER])
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_FLATBED])
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_CARGOPLANE])
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_JET])
					AND HAS_MODEL_LOADED(modelPed[EXT2_MODEL_DOM])
					AND HAS_MODEL_LOADED(modelPed[EXT2_MODEL_SECURITY])
					AND HAS_MODEL_LOADED(modelPed[EXT2_MODEL_PILOT])
					AND HAS_MODEL_LOADED(modelInvisibleProp)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "Ext2_DomIntoAirport")
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(502, "Ext2_AirportVeh")
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(503, "Ext2_CargoTakeOff")
					AND HAS_PTFX_ASSET_LOADED()
					AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\CARGO_PLANE_PH")
					AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\CARGO_PLANE_PH_B")
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_BLAZER], TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_CARGOPLANE], TRUE)
						b_assets_loaded = TRUE
					ENDIF
				BREAK
				CASE stateJumpOutPlane
					IF HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_BLAZER])
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_CARGOPLANE])
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_SEASHARK])
					AND HAS_MODEL_LOADED(modelPed[EXT2_MODEL_DOM])
					AND HAS_MODEL_LOADED(modelPed[EXT2_MODEL_PILOT])
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(600, "Ext2_CargoFlight")
					AND HAS_PTFX_ASSET_LOADED()
					AND HAS_ANIM_DICT_LOADED("rcmextreme2atv")
					AND HAS_ANIM_DICT_LOADED("rcmjosh1@impatient")
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_BLAZER], TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_CARGOPLANE], TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_SEASHARK], TRUE)
						b_assets_loaded = TRUE
					ENDIF
				BREAK
				CASE stateFinalConversation
					IF HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_SEASHARK])
					AND HAS_ANIM_DICT_LOADED("rcmextreme2")
						SET_VEHICLE_MODEL_IS_SUPPRESSED(modelVehicle[EXT2_MODEL_SEASHARK], TRUE)
						b_assets_loaded = TRUE
					ENDIF	
				BREAK
			ENDSWITCH
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Loading assets...") ENDIF #ENDIF
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Assets loaded") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mission failed function.
PROC EXT2_MISSION_FAILED(FAILED_REASON reason = FAILED_DEFAULT)
	IF bZSkipping = FALSE
		failReason = reason
		missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
		missionStateSkip = stateFailed
	ENDIF
ENDPROC

/// PURPOSE:
///    Death checks for any mission critical entities.
PROC EXT2_DEATH_CHECKS()
	IF ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
		IF DOES_ENTITY_EXIST(pedDom)
			IF IS_ENTITY_DEAD(pedDom)
				EXT2_MISSION_FAILED(FAILED_DOM_DIED)
			ELSE
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					EXT2_MISSION_FAILED(FAILED_DOM_SCARED)
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDom, PLAYER_PED_ID())
		            IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedDom, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
						EXT2_MISSION_FAILED(FAILED_DOM_INJURED)
		            ENDIF
				ELIF HAS_PLAYER_THREATENED_PED(pedDom)
					EXT2_MISSION_FAILED(FAILED_DOM_SCARED)
				ELIF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
					IF missionState = stateGoIntoAirport
					AND NOT IS_PED_IN_VEHICLE(pedDom, vehATV[VEHICLE_DOM])
						EXT2_MISSION_FAILED(FAILED_DOM_SCARED)
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehATV[VEHICLE_DOM], PLAYER_PED_ID())
					AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(vehATV[VEHICLE_DOM], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
						EXT2_MISSION_FAILED(FAILED_DOM_ATV_DAMAGED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedAirportBooth)
			IF IS_ENTITY_DEAD(pedAirportBooth)
				EXT2_MISSION_FAILED(FAILED_GUARD_INJURED)
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAirportBooth, PLAYER_PED_ID())
				AND HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedAirportBooth, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
					EXT2_MISSION_FAILED(FAILED_GUARD_INJURED)
				ENDIF
			ENDIF
		ENDIF
		IF bCheckPlayerATV = TRUE
		AND DOES_ENTITY_EXIST(vehATV[VEHICLE_PLAYER])
		AND IS_ENTITY_DEAD(vehATV[VEHICLE_PLAYER])
			EXT2_MISSION_FAILED(FAILED_ATV_WRECKED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set up mission prior to going into any mission states.
PROC EXT2_MISSION_SETUP()
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Starting mission setup...") ENDIF #ENDIF
	failReason = FAILED_DEFAULT
	REQUEST_ADDITIONAL_TEXT("EXT2", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	REGISTER_SCRIPT_WITH_AUDIO()
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Intro - es_2_rcm_concat"    
		mSkipMenu[Z_SKIP_GO_IN_AIRPORT].sTxtLabel = "Follow Dom"  
		mSkipMenu[Z_SKIP_DRIVE_OUT_PLANE].sTxtLabel = "ATV skydive"  
		mSkipMenu[Z_SKIP_LANDED_AFTER_SKYDIVE].sTxtLabel = "Landed on ground"   
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup = START_WIDGET_GROUP("Extreme 2 widgets")
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)	
			ADD_WIDGET_VECTOR_SLIDER("Marker Pos", vCheckpoint, -7000, 7000, 1)
			ADD_WIDGET_VECTOR_SLIDER("Marker Rot", vMarkerRotation, -360, 360, 1)
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	SET_CREATE_RANDOM_COPS(FALSE) // B*1401283
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
		REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
	ENDIF
	modelVehicle[EXT2_MODEL_BLAZER] = BLAZER
	modelVehicle[EXT2_MODEL_CARGOPLANE] = CARGOPLANE
	modelVehicle[EXT2_MODEL_JET] = JET
	modelVehicle[EXT2_MODEL_SEASHARK] = SEASHARK
	modelVehicle[EXT2_MODEL_FLATBED] = FLATBED
	modelPed[EXT2_MODEL_SECURITY] = S_M_M_SECURITY_01
	modelPed[EXT2_MODEL_DOM] = GET_NPC_PED_MODEL(CHAR_DOM)
	modelPed[EXT2_MODEL_PILOT] = S_M_M_PILOT_01
	modelInvisibleProp = Prop_LD_Test_01
	vATV[VEHICLE_PLAYER] = << -950.80, -2751.98, 13.21 >>
	fATV[VEHICLE_PLAYER] = 199.05
	vATV[VEHICLE_DOM] = << -950.192, -2755.96, 13.3639 >>
	fATV[VEHICLE_DOM] = -178.182
	vATVOnPlane[VEHICLE_PLAYER] = << 444.8519, 3921.2214, 2398.1 >>
	vATVOnPlane[VEHICLE_DOM] = << 443.3347, 3923.1145, 2398.1 >>
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
	IF DOES_SCENARIO_GROUP_EXIST("LSA_Planes") 
	AND IS_SCENARIO_GROUP_ENABLED("LSA_Planes") 
		SET_SCENARIO_GROUP_ENABLED("LSA_Planes", FALSE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Disabled scenario group LSA_Planes") ENDIF #ENDIF
	ENDIF
	bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)  //ensure bChange is set to the state of "player has changed outfit" at the start of the mission
	vJetPlaneEngineRear[0] = <<12.75,6,-7>>
	vJetPlaneEngineRear[1] = <<22.75,-4,-7>>
	vJetPlaneEngineRear[2] = <<-12.75,6,-7>>
	vJetPlaneEngineRear[3] = <<-22.75,-4,-7>>
	vJetPlaneEngineFront[0] = <<12.75,15,-7>>
	vJetPlaneEngineFront[1] = <<22.75,5,-7>>
	vJetPlaneEngineFront[2] = <<-12.75,15,-7>>
	vJetPlaneEngineFront[3] = <<-22.75,5,-7>>
	vCargoPlaneEngineRear[0] = <<15,-8,-4>>
	vCargoPlaneEngineRear[1] = <<29,-17,-4>>
	vCargoPlaneEngineRear[2] = <<-15,-8,-4>>
	vCargoPlaneEngineRear[3] = <<-29,-17,-4>>	
	vCargoPlaneEngineFront[0] = <<15,8,-4>>
	vCargoPlaneEngineFront[1] = <<29,-1,-4>>
	vCargoPlaneEngineFront[2] = <<-15,8,-4>>
	vCargoPlaneEngineFront[3] = <<-29,-1,-4>>
	REQUEST_VEHICLE_ASSET(modelVehicle[EXT2_MODEL_BLAZER])
	EXT2_REGISTER_BARRIER_FOR_PED(PLAYER_PED_ID(), TRUE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Finished mission setup") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Spawn Dom and setup his attributes.
PROC EXT2_SPAWN_DOM(BOOL b_spawn_dom_on_atv, BOOL b_give_parachute = FALSE)
	EXT2_ENSURE_MODEL_IS_LOADED(modelPed[EXT2_MODEL_DOM])
	IF IS_ENTITY_ALIVE(pedDom)
		IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
		AND NOT IS_PED_IN_VEHICLE(pedDom, vehATV[VEHICLE_DOM])
			SET_PED_INTO_VEHICLE(pedDom, vehATV[VEHICLE_DOM])
		ENDIF
	ELSE
		IF b_spawn_dom_on_atv = TRUE
			IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
				EXT2_CREATE_PED_IN_VEHICLE(vehATV[VEHICLE_DOM], pedDom, modelPed[EXT2_MODEL_DOM], VS_DRIVER)
			ENDIF
		ELSE
			EXT2_CREATE_PED(pedDom, modelPed[EXT2_MODEL_DOM], vLandingPos, fPlayerOutro-180)
		ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom didn't exist so created him") ENDIF #ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[EXT2_MODEL_DOM])
	EXT2_SET_PED_AS_PLAYER_FRIEND(pedDom)
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedDom, KNOCKOFFVEHICLE_NEVER)
	SET_ENTITY_LOD_DIST(pedDom, 500)
	ADD_PED_FOR_DIALOGUE(sConversation, 3, pedDom, "DOM")
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedDom, TRUE)
	SET_PED_CAN_BE_TARGETTED(pedDom, FALSE)
	SET_ENTITY_LOAD_COLLISION_FLAG(pedDom, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDom, TRUE)
	SET_PED_PROP_INDEX(pedDom, ANCHOR_EARS, 0, 0)
	SET_PED_HELMET(pedDom, FALSE)
	SET_PED_DIES_IN_WATER(pedDom, FALSE) // B*1421209
	SET_PED_PARACHUTE_TINT_INDEX(pedDom, 6) // Black
	IF b_give_parachute = TRUE
		SET_PED_COMPONENT_VARIATION(pedDom, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0)
		IF NOT HAS_PED_GOT_WEAPON(pedDom, GADGETTYPE_PARACHUTE)
			GIVE_WEAPON_TO_PED(pedDom, GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
		ENDIF
	ELSE
		SET_PED_COMPONENT_VARIATION(pedDom, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0)
	ENDIF
	IF b_spawn_dom_on_atv = TRUE
	AND IS_AUDIO_SCENE_ACTIVE("EXTREME_02_DRIVE_TO_PLANE")
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehATV[VEHICLE_DOM], "EXTREME_02_DOMS_ATV")
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Added Dom's ATV to audio mix group EXTREME_02_DRIVE_TO_PLANE") ENDIF #ENDIF
	ENDIF
	EXT2_REGISTER_BARRIER_FOR_PED(pedDom, TRUE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Setup Dom") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Create the jetskis by the shoreline.
PROC EXT2_SPAWN_JETSKIS()
	EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateFinalConversation)
	FLOAT z_water_height
	GET_WATER_HEIGHT(vJetski, z_water_height)
	EXT2_CREATE_VEHICLE(vehJetski, modelVehicle[EXT2_MODEL_SEASHARK], <<vJetski.x, vJetski.y, z_water_height>>, fJetski, -1, 0.0, 0)
	SET_VEHICLE_ENGINE_ON(vehJetski, FALSE, TRUE)
	SET_BOAT_ANCHOR(vehJetski, TRUE)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehJetski, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_SEASHARK])
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Created jetski") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Sets the standard params on the ATV.
PROC EXT2_SET_PARAMS_ON_ATV(INT i)
	IF IS_ENTITY_ALIVE(vehATV[i])
		SET_VEHICLE_CAN_LEAK_OIL(vehATV[i], FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(vehATV[i], FALSE)
		SET_VEHICLE_DOORS_LOCKED(vehATV[i], VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_HAS_STRONG_AXLES(vehATV[i], TRUE)
		IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehATV[i])
			SET_VEHICLE_ENGINE_ON(vehATV[i], TRUE, TRUE)
		ENDIF
		IF i = VEHICLE_DOM
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehATV[i], TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Create the cargo plane, ATVs, etc for the skydive.
PROC EXT2_CREATE_CARGO_PLANE_FOR_SKYDIVE()
	IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
	ENDIF
	EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateJumpOutPlane)
	EXT2_CREATE_VEHICLE(vehPlaneSkydive, modelVehicle[EXT2_MODEL_CARGOPLANE], GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(600, "Ext2_CargoFlight"), 0), fPlaneEnd)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_CARGOPLANE])
	EXT2_CREATE_PED_IN_VEHICLE(vehPlaneSkydive, pedPlanePilot[0], modelPed[EXT2_MODEL_PILOT], VS_DRIVER)
	IF IS_ENTITY_ALIVE(pedPlanePilot[0])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlanePilot[0], TRUE)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[EXT2_MODEL_PILOT])
	START_PLAYBACK_RECORDED_VEHICLE(vehPlaneSkydive, 600, "Ext2_CargoFlight")
	PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlaneSkydive)
	SET_VEHICLE_ENGINE_ON(vehPlaneSkydive, TRUE, TRUE)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlaneSkydive, FALSE)
	SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlaneSkydive, FALSE)
	SET_VEHICLE_CAN_BREAK(vehPlaneSkydive, FALSE)
	SET_ENTITY_PROOFS(vehPlaneSkydive, TRUE, TRUE, TRUE, TRUE, TRUE)
	SET_VEHICLE_DOOR_OPEN(vehPlaneSkydive, SC_DOOR_REAR_LEFT)
	SET_ENTITY_LOD_DIST(vehPlaneSkydive, 10000)
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vATVOnPlane[VEHICLE_PLAYER], 0) // In case the player was parachuting and he's z skipping back to retry the skydive
	ENDIF
	SAFE_DELETE_PED(pedDom)
	INT i = 0
	REPEAT NUM_ATVS i
		SAFE_DELETE_VEHICLE(vehATV[i])
		EXT2_CREATE_VEHICLE(vehATV[i], modelVehicle[EXT2_MODEL_BLAZER], vATVOnPlane[i], 91,-1,0.0,i)
		EXT2_SET_PARAMS_ON_ATV(i)
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_BLAZER])
	IF NOT IS_REPLAY_BEING_SET_UP()
	AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER], VS_DRIVER)
	ENDIF
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehATV[VEHICLE_DOM], FALSE)
	EXT2_SPAWN_DOM(TRUE, TRUE)
	EXT2_SET_FRANKLIN_INTO_SKYDIVING_OUTFIT(TRUE)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
	SET_LIGHT_RIG_ON_CARGO_PLANE(TRUE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Setup cargo plane for skydive") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Creates the flatbed truck if it's not already present.
PROC EXT2_CREATE_INTRO_TRUCK()
	IF NOT DOES_ENTITY_EXIST(vehIntroTruck)
		EXT2_CREATE_VEHICLE(vehIntroTruck, modelVehicle[EXT2_MODEL_FLATBED], vIntroTruck, fIntroTruck, 2, 0)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehIntroTruck)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_FLATBED])
	IF IS_ENTITY_ALIVE(vehIntroTruck)
		SET_VEHICLE_DOORS_LOCKED(vehIntroTruck, VEHICLELOCK_LOCKED)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIntroTruck, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the flatbed truck if it's not already present.
PROC EXT2_CREATE_ATV(INT i)
	IF NOT DOES_ENTITY_EXIST(vehATV[i])
		EXT2_CREATE_VEHICLE(vehATV[i], modelVehicle[EXT2_MODEL_BLAZER], vATV[i], fATV[i],-1,0.0,i)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: vehATV ", i, " didn't exist so created it") ENDIF #ENDIF
	ENDIF
	EXT2_SET_PARAMS_ON_ATV(i)
ENDPROC

/// PURPOSE:
///    Various setup just before the mocap finishes.
PROC EXT2_SETUP_BEFORE_MOCAP_FINISHES()
	EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateIntro)
	INT i = 0
	REPEAT NUM_ATVS i
		EXT2_CREATE_ATV(i)
	ENDREPEAT
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_BLAZER])
	IF NOT IS_REPLAY_BEING_SET_UP()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_SCREEN_FADED_OUT()
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerAfterIntro, fPlayerAfterIntro)
		ENDIF
		IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
		ENDIF
	ENDIF
	EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_TO_PLANE", TRUE)
	EXT2_SPAWN_DOM(TRUE, TRUE)
	ADD_PED_FOR_DIALOGUE(sConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
ENDPROC

/// PURPOSE:
///    Creates the cloud ptfx.
PROC EXT2_CREATE_CLOUD_PTFX()
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_moving_clouds)
		IF IS_ENTITY_ALIVE(vehPlaneSkydive)
			fCloudsCurrentAlpha = 0.4
			ptfx_moving_clouds = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_extrm2_moving_cloud", vehPlaneSkydive, <<0,-20,-13>>, <<0,0,0>>)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Created ptfx_moving_clouds") ENDIF #ENDIF
		ENDIF
	ELSE
		SET_PARTICLE_FX_LOOPED_ALPHA(ptfx_moving_clouds, fCloudsCurrentAlpha)
	ENDIF
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_wind_and_smoke)
		ptfx_wind_and_smoke = START_PARTICLE_FX_LOOPED_AT_COORD("scr_rcext2_cargo_smoke", <<417.0, 3920.0, 1449.0>>, <<0,0,0>>)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Created ptfx_wind_and_smoke") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles debug skipping.
///  PARAMS:
///     i_new_state - The state to debug skip to.
PROC EXT2_DEBUG_SKIP_STATE(INT i_new_state)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Starting debug skip to ", i_new_state, "...") ENDIF #ENDIF
	RC_START_Z_SKIP()
	iCutsceneStage = -1
	bZSkipping = TRUE
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	EXT2_SET_FRANKLIN_INTO_SKYDIVING_OUTFIT(FALSE)
	EXT2_MISSION_CLEANUP(TRUE)
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE)
	SWITCH i_new_state
		CASE Z_SKIP_INTRO
			CLEAR_AREA_OF_OBJECTS(vIntroTruck, 100) // B*1086415, 1513300 - Ensure barrier gets remade
			missionStateSkip = stateIntro
		BREAK
		CASE Z_SKIP_GO_IN_AIRPORT
			CLEAR_AREA_OF_OBJECTS(vIntroTruck, 100) // B*1086415, 1513300 - Ensure barrier gets remade
			EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateIntro)
			EXT2_SETUP_BEFORE_MOCAP_FINISHES()
			IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
				START_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 500, "Ext2_DomIntoAirport")
				PAUSE_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM])
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 1000)
			ENDIF
			CREATE_VEHICLE_FOR_REPLAY(vehForReplay, vPlayerCarRespot, fPlayerCarRespot, FALSE, FALSE, FALSE, FALSE, FALSE)
			EXT2_CREATE_INTRO_TRUCK()
			missionStateSkip = stateGoIntoAirport
		BREAK
		CASE Z_SKIP_DRIVE_OUT_PLANE
			EXT2_CREATE_CARGO_PLANE_FOR_SKYDIVE()
			EXT2_START_AUDIO_SCENE("EXTREME_02_INSIDE_PLANE", TRUE)
			missionStateSkip = stateJumpOutPlane
		BREAK
		CASE Z_SKIP_LANDED_AFTER_SKYDIVE
			EXT2_SPAWN_DOM(FALSE)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerOutro, fPlayerOutro)
			ENDIF
			EXT2_SET_FRANKLIN_INTO_SKYDIVING_OUTFIT(TRUE)
			EXT2_SPAWN_JETSKIS()
			missionStateSkip = stateFinalConversation
		BREAK
		CASE Z_SKIP_MISSION_PASSED
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerOutro, fPlayerOutro)
			ENDIF
			missionStateSkip = stateMissionPassed
		BREAK
	ENDSWITCH
	ADD_PED_FOR_DIALOGUE(sConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	IF i_new_state = Z_SKIP_GO_IN_AIRPORT
		TRIGGER_MUSIC_EVENT("EXTREME2_RESTART1") // Restart this music here, see B*1131790
	ELIF i_new_state = Z_SKIP_DRIVE_OUT_PLANE
		TRIGGER_MUSIC_EVENT("EXTREME2_RESTART2")
	ENDIF
	IF i_new_state = Z_SKIP_GO_IN_AIRPORT
	OR i_new_state = Z_SKIP_DRIVE_OUT_PLANE
		END_REPLAY_SETUP(vehATV[VEHICLE_PLAYER])
		WAIT(500) // Let the ATVs settle
	ELSE
		END_REPLAY_SETUP()
	ENDIF
	IF i_new_state = Z_SKIP_MISSION_PASSED
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SAFE_FADE_SCREEN_IN_FROM_BLACK() // Screen needs to be fully faded back in before the mission complete GUI stuff will display
	ENDIF
	IF i_new_state = Z_SKIP_INTRO
	OR i_new_state = Z_SKIP_LANDED_AFTER_SKYDIVE
		RC_END_Z_SKIP(TRUE, FALSE)
	ELIF i_new_state = Z_SKIP_DRIVE_OUT_PLANE
		RC_END_Z_SKIP(TRUE, TRUE, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_PARACHUTE, FALSE) // B*1742706 Ensure player has parachute
	ELSE
		RC_END_Z_SKIP()
	ENDIF
	missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Finished debug skip to ", i_new_state) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Handles the player getting off his ATV during any mission state prior to the midtro.
PROC EXT2_CHECK_PLAYER_STILL_ON_ATV()
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
		IF bPlayerIsOnATV = TRUE
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
				CLEAR_PRINTS()
				IF missionState = stateGoIntoAirport
					SAFE_REMOVE_BLIP(blipDom)
					SAFE_REMOVE_BLIP(blipPlane)
				ENDIF
				IF bDisplayedGetBackOnATV = FALSE
					PRINT_NOW("EXT2_09", DEFAULT_GOD_TEXT_TIME, 1) // Get back on your ~b~ATV.
					bDisplayedGetBackOnATV = TRUE
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipPlayerATV)
					blipPlayerATV = CREATE_VEHICLE_BLIP(vehATV[VEHICLE_PLAYER])
				ENDIF
				bPlayerIsOnATV = FALSE
			ENDIF
		ELSE
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
				CLEAR_PRINTS()
				SAFE_REMOVE_BLIP(blipPlayerATV)
				SWITCH missionState
					CASE stateGoIntoAirport
						IF IS_ENTITY_ALIVE(vehPlaneChase)
						AND NOT DOES_BLIP_EXIST(blipPlane)
							blipPlane = CREATE_VEHICLE_BLIP(vehPlaneChase)
						ENDIF
						IF iTimesDisplayedRideATVObjective < 2
							PRINT_NOW("EXT2_02", DEFAULT_GOD_TEXT_TIME, 1) // Follow Dom on to the ~b~cargo plane.
							iTimesDisplayedRideATVObjective++
						ENDIF
					BREAK
					CASE stateJumpOutPlane
						IF iTimesDisplayedRideATVObjective < 2
							PRINT_NOW("EXT2_08", DEFAULT_GOD_TEXT_TIME, 1) // Ride your ATV out of the plane.
							iTimesDisplayedRideATVObjective++
						ENDIF
					BREAK
				ENDSWITCH
				bPlayerIsOnATV = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the player doesn't abandon Dom, and fails the mission if necessary.
PROC EXT2_CHECK_PLAYER_ISNT_ABANDONING_DOM()
	IF bZSkipping = FALSE
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDom) > 500
		IF statusDomATV = ATV_CHASE_PLANE
		OR statusDomATV = ATV_ATTACHED
			EXT2_MISSION_FAILED(FAILED_DIDNT_DRIVE_ATV)
		ELSE
			EXT2_MISSION_FAILED(FAILED_DIDNT_FOLLOW_DOM)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles displaying the hint camera and text for Dom.
PROC EXT2_MANAGE_DISPLAY_DOM_FOCUS_CAMERA_HELP_TEXT()
	IF IS_ENTITY_ALIVE(pedDom)
		CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedDom)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles displaying the hint camera and text for the landing zone.
PROC EXT2_MANAGE_LANDING_ZONE_FOCUS_CAMERA()
	CONTROL_COORD_CHASE_HINT_CAM_ON_FOOT(localChaseHintCamStruct, vCheckpoint, "EXT2_15") // Press ~INPUT_VEH_CIN_CAM~ to toggle focus on the landing zone.
ENDPROC

/// PURPOSE:
///    Handles displaying the hint camera and text for the cargo plane.
PROC EXT2_MANAGE_PLANE_FOCUS_CAMERA()
	IF IS_ENTITY_ALIVE(vehPlaneChase)
	AND IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER]) // See B*904583 - only allow hint camera on plane if the player is on his ATV
			IF GET_DISTANCE_BETWEEN_ENTITIES(vehPlaneChase, vehATV[VEHICLE_PLAYER]) < 500
				CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehPlaneChase, "EXT2_12") // Press ~INPUT_VEH_CIN_CAM~ to toggle focus on the cargo plane.
				IF IS_GAMEPLAY_HINT_ACTIVE()
					EXT2_START_AUDIO_SCENE("EXTREME_02_FOCUS_ON_PLANE", TRUE)
				ELSE
					EXT2_START_AUDIO_SCENE("EXTREME_02_FOCUS_ON_PLANE", FALSE)
				ENDIF
			ENDIF
		ELIF IS_GAMEPLAY_HINT_ACTIVE()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if a conversation can be played.
FUNC BOOL EXT2_ALLOW_CONVERSATION_TO_PLAY(BOOL b_check_for_god_text = TRUE)
	IF bZSkipping = TRUE
		RETURN FALSE
	ENDIF
	IF b_check_for_god_text = TRUE // If FALSE the conversation is set to not display subtitles ever, so play conversation regardless of whether there is god text onscreen or not
	AND IS_MESSAGE_BEING_DISPLAYED()
	AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) > 0 // B*665755 Support playing conversations over god text when subtitles are turned off
		RETURN FALSE
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles the ambient airport plane moving around.
PROC EXT2_MANAGE_AMBIENT_AIRPORT_PLANE()
	IF IS_ENTITY_ALIVE(vehAirportPlane)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAirportPlane)
		SET_PLAYBACK_SPEED(vehAirportPlane, 0.32)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the airport planes.
PROC EXT2_SPAWN_AIRPORT_PLANES()
	VECTOR v_plane_rot = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(503, "Ext2_CargoTakeOff"), 0)
	EXT2_CREATE_VEHICLE(vehPlaneChase, modelVehicle[EXT2_MODEL_CARGOPLANE], GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(503, "Ext2_CargoTakeOff"), 0), v_plane_rot.z)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_CARGOPLANE])
	IF IS_ENTITY_ALIVE(vehPlaneChase)
		EXT2_CREATE_PED_IN_VEHICLE(vehPlaneChase, pedPlanePilot[0], modelPed[EXT2_MODEL_PILOT], VS_DRIVER)
		IF IS_ENTITY_ALIVE(pedPlanePilot[0])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlanePilot[0], TRUE)
		ENDIF
		SET_ENTITY_LOAD_COLLISION_FLAG(vehPlaneChase, TRUE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlaneChase, FALSE)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlaneChase, FALSE)
		SET_VEHICLE_CAN_BREAK(vehPlaneChase, FALSE)
		SET_ENTITY_PROOFS(vehPlaneChase, TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_VEHICLE_DOOR_OPEN(vehPlaneChase, SC_DOOR_REAR_LEFT)
		SET_ENTITY_LOD_DIST(vehPlaneChase, 10000)
		CONTROL_LANDING_GEAR(vehPlaneChase, LGC_DEPLOY_INSTANT)
		START_PLAYBACK_RECORDED_VEHICLE(vehPlaneChase, 503, "Ext2_CargoTakeOff")
		PAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlaneChase)
		SET_VEHICLE_ENGINE_ON(vehPlaneChase, TRUE, TRUE)
	ENDIF
	EXT2_CREATE_VEHICLE(vehAirportPlane, modelVehicle[EXT2_MODEL_JET], GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(502, "Ext2_AirportVeh"), 0), 161.569275, 0, 0.0, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelVehicle[EXT2_MODEL_JET])
	IF IS_ENTITY_ALIVE(vehAirportPlane)
		EXT2_CREATE_PED_IN_VEHICLE(vehAirportPlane, pedPlanePilot[1], modelPed[EXT2_MODEL_PILOT], VS_DRIVER)
		IF IS_ENTITY_ALIVE(pedPlanePilot[1])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlanePilot[1], TRUE)
		ENDIF
		SET_VEHICLE_LIVERY(vehAirportPlane, 0)
		SET_ENTITY_LOD_DIST(vehAirportPlane, 5000)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehAirportPlane, FALSE)
		START_PLAYBACK_RECORDED_VEHICLE(vehAirportPlane, 502, "Ext2_AirportVeh")
		SET_VEHICLE_ENGINE_ON(vehAirportPlane, TRUE, TRUE)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[EXT2_MODEL_PILOT])
ENDPROC

/// PURPOSE:
///    Returns TRUE if the player's ATV is within an acceptable area to attach to the plane.
FUNC BOOL EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(FLOAT f_x_to_check_positive, FLOAT f_x_to_check_negative, FLOAT f_y_to_check, FLOAT f_z_to_check)
	IF IS_ENTITY_ALIVE(vehPlaneChase)
		vPlayerRampOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlaneChase,<<0,-20,-3>>)
		IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
			vATVOffsetFromPlane = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneChase, GET_ENTITY_COORDS(vehATV[VEHICLE_PLAYER]))
			VECTOR vPlayerOffsetFromPlane = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneChase, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player pos within acceptable area to attach to plane") ENDIF #ENDIF
			//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: vATVOffsetFromPlane = ", vATVOffsetFromPlane) ENDIF #ENDIF
			IF vPlayerOffsetFromPlane.x < f_x_to_check_positive
			AND vPlayerOffsetFromPlane.x > f_x_to_check_negative
			AND vPlayerOffsetFromPlane.y > f_y_to_check
			AND vPlayerOffsetFromPlane.y < 25.0 // B*1546920 Check for player not being ahead of the plane
			AND vPlayerOffsetFromPlane.z > f_z_to_check
			AND vATVOffsetFromPlane.x < f_x_to_check_positive
			AND vATVOffsetFromPlane.x > f_x_to_check_negative
			AND vATVOffsetFromPlane.y > f_y_to_check
			AND vATVOffsetFromPlane.y < 25.0 // B*1546920 Check for player not being ahead of the plane
			AND vATVOffsetFromPlane.z > f_z_to_check
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles rubber banding for the cargo plane's speed based upon player distance from it.
PROC EXT2_MANAGE_CARGO_PLANE_SPEED()
	FLOAT fDist
	FLOAT fPlaybackModifier
	FLOAT fPlaybackSpeed
	IF bShutCargoPlaneDoors = TRUE
		fPlaybackSpeed = 0.65 // B*1146534 Ensure plane is going quickly when the cargo plane doors close with the player outside
	ELIF statusDomATV = ATV_ATTACHED // When Dom is attached to the plane, use rubber banding for the plane's playback speed depending on player distance
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerRampOffset)
			IF fDist > 80
				fDist = 80
			ENDIF
			fPlaybackModifier = (fDist / 100) / 2
			IF fDist < 40
				fPlaybackSpeed = 0.25 + fPlaybackModifier
			ELSE
				fPlaybackSpeed = 0.65 - fPlaybackModifier
			ENDIF
			//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: fDist = ", fDist, " fPlaybackModifier = ", fPlaybackModifier, " fPlaybackSpeed = ", fPlaybackSpeed) ENDIF #ENDIF
		ENDIF
	ELSE // Until Dom is attached to the plane, keep the plane's playback speed constant
		fPlaybackSpeed = 0.35
	ENDIF
	IF IS_ENTITY_ALIVE(vehPlaneChase)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
		SET_PLAYBACK_SPEED(vehPlaneChase, fPlaybackSpeed)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: GET_POSITION_IN_RECORDING(vehPlaneChase) = ", GET_POSITION_IN_RECORDING(vehPlaneChase)) ENDIF #ENDIF
		IF bShutCargoPlaneDoors = FALSE
		AND GET_POSITION_IN_RECORDING(vehPlaneChase) > 340
			IF EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(2, -2, -40, -7) // B*651102 If the player is on the ramp just allow it
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player is touching ramp so just progress") ENDIF #ENDIF
				missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Shutting cargo plane doors") ENDIF #ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_gen_sparks)
				STOP_PARTICLE_FX_LOOPED(ptfx_gen_sparks)
			ENDIF
			SET_VEHICLE_DOORS_SHUT(vehPlaneChase, FALSE)
			bShutCargoPlaneDoors = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns FALSE if we should allow god text or help text to display when following Dom or driving into the plane.
FUNC BOOL EXT2_ALLOW_TEXT_TO_DISPLAY()
	IF IS_ENTITY_ALIVE(vehPlaneChase)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
	AND GET_POSITION_IN_RECORDING(vehPlaneChase) > 325 // Don't display if plane is about to take out
		RETURN FALSE
	ENDIF
	IF IS_ENTITY_ALIVE(pedDom)
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDom) > 300 // Don't display if far away from Dom and mission is about to fail
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles attaching the player to the moving plane.
PROC EXT2_MANAGE_PLANE_TAKING_OFF()
	IF statusDomATV = ATV_ATTACHED
	AND IS_ENTITY_ALIVE(vehPlaneChase)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
		fCurrentPlanePlaybackPosition = GET_POSITION_IN_RECORDING(vehPlaneChase)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: fCurrentPlanePlaybackPosition = ", fCurrentPlanePlaybackPosition) ENDIF #ENDIF
		IF fCurrentPlanePlaybackPosition > 450
			CONTROL_LANDING_GEAR(vehPlaneChase, LGC_RETRACT)
			EXT2_MISSION_FAILED(FAILED_DIDNT_DRIVE_ATV)
		ELSE // Play some conversations from Dom to encourage the player to ride into the plane
			IF IS_ENTITY_ALIVE(pedDom)
			AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
			AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDom, 50)
				IF bStartedDriveInPlaneConversation1 = FALSE
				AND fCurrentPlanePlaybackPosition < 200 // Don't play this line if the plane is nearing the end of the runway
				AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_REMIND1", CONV_PRIORITY_MEDIUM)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_REMIND1 conversation") ENDIF #ENDIF
					bStartedDriveInPlaneConversation1 = TRUE
				ENDIF
				IF bStartedDriveInPlaneConversation2 = FALSE
				AND fCurrentPlanePlaybackPosition > 200 // Only play when plane is far down runway prior to taking off...
				AND fCurrentPlanePlaybackPosition < 325 // ...but not too far (B*650590)
				AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_REMIND2", CONV_PRIORITY_MEDIUM)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_REMIND2 conversation") ENDIF #ENDIF
					bStartedDriveInPlaneConversation2 = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the plane sfx.
PROC EXT2_MANAGE_PLANE_SFX(VEHICLE_INDEX veh_plane, FLOAT f_initial_value)
	IF IS_ENTITY_ALIVE(veh_plane)
		IF bPlayingPlaneStream = FALSE
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), veh_plane, 200)
			AND LOAD_STREAM("Plane_Interior", "EXTREME_02_SOUNDSET")
				PLAY_STREAM_FRONTEND()
				SET_VARIABLE_ON_STREAM("INOUT", f_initial_value)
				bPlayingPlaneStream = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Playing Plane_Interior audio stream now") ENDIF #ENDIF
			ENDIF
		ELSE
			VECTOR v_temp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh_plane, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			IF v_temp.x < 2.5
			AND v_temp.x > -2.5
			AND v_temp.z > -6
			AND v_temp.y < 30
			AND v_temp.y > -40
				// Player inside plane so need to work out how much
				FLOAT f_distance_along_plane = (v_temp.y - 30) / 70 // 30 is at ladder inside plane, -40 is at bottom of ramp, 70 is the difference
				fStreamInOut = ABSF(f_distance_along_plane)
			ELSE // Player fully outside plane
				fStreamInOut = 1.0
			ENDIF
			IF fStreamInOut < 0
				fStreamInOut = 0
			ELIF fStreamInOut > 1
				fStreamInOut = 1
			ENDIF
			//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: fStreamInOut = ", fStreamInOut) ENDIF #ENDIF
			SET_VARIABLE_ON_STREAM("INOUT", fStreamInOut)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the radar to the interior of the plane.
PROC EXT1_SET_RADAR_TO_PLANE_INTERIOR()
	IF NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_IN()
		IF IS_ENTITY_ALIVE(vehPlaneSkydive)
			VECTOR v_plane_pos = GET_ENTITY_COORDS(vehPlaneSkydive)
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeCargoPlaneClimb"), v_plane_pos.x, v_plane_pos.y, FLOOR(GET_ENTITY_HEADING(vehPlaneSkydive)), 1)
		ENDIF
		IF IS_HUD_HIDDEN()
			DISPLAY_HUD(TRUE)
		ENDIF
		IF IS_RADAR_HIDDEN()
			DISPLAY_RADAR(TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles attaching the player to the moving plane.
PROC EXT2_MANAGE_PLAYER_ATTACH_TO_PLANE()
	IF IS_ENTITY_ALIVE(vehPlaneChase)
		IF EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(2, -2, -24, -4)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player reached cargo plane interior ok") ENDIF #ENDIF
			REPLAY_RECORD_BACK_FOR_TIME(3.0)
			iPlayerAttachedTimer = GET_GAME_TIMER()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			KILL_ANY_CONVERSATION()
		ENDIF
		IF bStartedDriveInPlaneConversation3 = FALSE
		AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
		AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
		AND EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(1.5, -1.5, -50, -10)
		AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_BACK", CONV_PRIORITY_MEDIUM)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_BACK conversation") ENDIF #ENDIF
			bStartedDriveInPlaneConversation3 = TRUE
			EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_TO_PLANE", FALSE)
			EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_UP_RAMP", TRUE)
		ENDIF
		IF bStartedEnterPlaneMusicEvent = FALSE
		AND EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(2, -2, -30, -10)
			TRIGGER_MUSIC_EVENT("EXTREME2_ENTER")
			bStartedEnterPlaneMusicEvent = TRUE
		ENDIF
		IF (GET_GAME_TIMER() - iPlayerCurseTimer) > 50 // B*861994 Play a curse from Franklin if he drives off the side of the ramp
			iPlayerCurseTimer = GET_GAME_TIMER()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_SCRIPTED_SPEECH_PLAYING(PLAYER_PED_ID())
			AND NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
				IF EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(3.5, 1.6, -40, -5.75)
				OR EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(-1.6, -3.5, -40, -5.75)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player within swear area so playing a curse") ENDIF #ENDIF
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_CURSE_HIGH", SPEECH_PARAMS_INTERRUPT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Dom yells while driving out of the plane.
PROC EXT2_MANAGE_REMIND_DIALOGUE()
	IF iDoneRemindConversation < 3
	AND (GET_GAME_TIMER() - iRemindConversationTimer) > 9000
	AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_REMIND", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_REMIND conversation") ENDIF #ENDIF
		iDoneRemindConversation++
		iRemindConversationTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles attaching Dom to the moving plane.
PROC EXT2_MANAGE_DOM_ATTACH_TO_PLANE()
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
		SWITCH statusDomATV
			CASE ATV_INTO_AIRPORT
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehATV[VEHICLE_DOM])
					fCurrentDomPlaybackPosition = GET_POSITION_IN_RECORDING(vehATV[VEHICLE_DOM])
					SET_PLAYBACK_SPEED(vehATV[VEHICLE_DOM], 1.04)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: fCurrentDomPlaybackPosition = ", fCurrentDomPlaybackPosition) ENDIF #ENDIF
					IF bStartedFirstConversation = FALSE
						IF EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
						AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_START", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_START conversation") ENDIF #ENDIF
							bStartedFirstConversation = TRUE
						ENDIF
					ELIF bStartedGuardConversation = FALSE
						IF EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
						AND IS_ENTITY_ALIVE(pedDom)
						AND IS_ENTITY_ALIVE(pedAirportBooth)
						AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_GATE", CONV_PRIORITY_HIGH)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_GATE conversation") ENDIF #ENDIF
							bStartedGuardConversation = TRUE
							TASK_LOOK_AT_ENTITY(pedDom, pedAirportBooth, 2000)
							TASK_LOOK_AT_ENTITY(pedAirportBooth, pedDom, 4000)
						ENDIF
					ENDIF
					IF fCurrentDomPlaybackPosition > 265
						SAFE_RELEASE_PED(pedAirportBooth)
						FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_AIRPORT_BARRIER_IN, FALSE)
						IF IS_ENTITY_ALIVE(vehPlaneChase)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlaneChase)
							ptfx_gen_sparks = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_rcext2_ramp_scrape", vehPlaneChase, <<0,-40,-6.45>>, <<0,0,0>>)
							objectInvisibleProp = CREATE_OBJECT(modelInvisibleProp, GET_ENTITY_COORDS(vehPlaneChase))
							SET_ENTITY_COLLISION(objectInvisibleProp, FALSE)
							SET_ENTITY_VISIBLE(objectInvisibleProp, FALSE)
							ATTACH_ENTITY_TO_ENTITY(objectInvisibleProp, vehPlaneChase, 0, <<0,-40,-6.45>>, <<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(iSparksSound, "Scraping_Ramp", objectInvisibleProp, "EXTREME_02_SOUNDSET")
						ENDIF
						iTimesDisplayedRideATVObjective = 0
						statusDomATV = ATV_CHASE_PLANE
					ENDIF
				ENDIF
			BREAK
			CASE ATV_CHASE_PLANE
				EXT2_MANAGE_PLANE_FOCUS_CAMERA()
				EXT2_MANAGE_CARGO_PLANE_SPEED()
				EXT2_MANAGE_REMIND_DIALOGUE()
				IF IS_ENTITY_ALIVE(vehPlaneChase)
					vDomRampOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneChase, GET_ENTITY_COORDS(vehATV[VEHICLE_DOM]))
					IF vDomRampOffset.x < 3
					AND vDomRampOffset.x > -3
					AND vDomRampOffset.y > -8
					AND vDomRampOffset.z > -7
						SAFE_REMOVE_BLIP(blipDom)
						EXT2_STOP_CARREC_IF_PLAYING(vehATV[VEHICLE_DOM])
						VECTOR v_attach_offset
						v_attach_offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneChase, GET_ENTITY_COORDS(vehATV[VEHICLE_DOM]))
						ATTACH_ENTITY_TO_ENTITY(vehATV[VEHICLE_DOM], vehPlaneChase, 0, v_attach_offset, <<0,0,0>>, FALSE, FALSE, TRUE)
						statusDomATV = ATV_ATTACHED
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: statusDomATV = ATV_ATTACHED, v_attach_offset = ", v_attach_offset) ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ATV_ATTACHED
				EXT2_MANAGE_PLANE_FOCUS_CAMERA()
				EXT2_MANAGE_CARGO_PLANE_SPEED()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles starting the runway music event when the player reaches the runway.
PROC EXT2_MANAGE_RUNWAY_MUSIC_EVENT()
	IF bStartedRunwayMusicEvent = FALSE
	AND IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
	AND IS_ENTITY_IN_ANGLED_AREA(vehATV[VEHICLE_PLAYER], <<-1648.926270,-2752.311279,11.944445>>, <<-1327.844727,-2196.041016,17.944447>>, 50.000000)
		TRIGGER_MUSIC_EVENT("EXTREME2_RUNWAY")
		bStartedRunwayMusicEvent = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Player has driven near a jet engine so give him a physics blast.
PROC EXT2_CHECK_BLAST_PLAYER_WITH_JET_ENGINE(VECTOR v_engine_pos)
	iCheckEngineTimer = GET_GAME_TIMER()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), v_engine_pos, 4)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			KNOCK_PED_OFF_VEHICLE(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player blasted with a jet engine - knocked off ATV") ENDIF #ENDIF
		ELSE
			IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND NOT IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED_ID())
				VECTOR v_entity_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR v_dir = NORMALISE_VECTOR(v_engine_pos - v_entity_pos) // Pull towards engine
				v_dir *= 15.0
				SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 0, 1000, TASK_RELAX, FALSE)
				APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, v_dir, <<0,0,1>>, 0, FALSE, TRUE, TRUE) 
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player blasted with a jet engine - on foot") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check for the player driving near to an airplane engine.
PROC EXT2_CHECK_PLAYER_NEAR_ENGINES()
	IF (GET_GAME_TIMER() - iCheckEngineTimer) > 250
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Checking player proximity to jet engines ", GET_GAME_TIMER()) ENDIF #ENDIF
		INT i
		VECTOR v_engine_pos
		IF IS_ENTITY_ALIVE(vehAirportPlane)
			i = 0
			REPEAT 4 i
				v_engine_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehAirportPlane, vJetPlaneEngineRear[i])
				EXT2_CHECK_BLAST_PLAYER_WITH_JET_ENGINE(v_engine_pos)
			ENDREPEAT
			i = 0
			REPEAT 4 i
				v_engine_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehAirportPlane, vJetPlaneEngineFront[i])
				EXT2_CHECK_BLAST_PLAYER_WITH_JET_ENGINE(v_engine_pos)
			ENDREPEAT
		ENDIF
		IF IS_ENTITY_ALIVE(vehPlaneChase)
			i = 0
			REPEAT 4 i
				v_engine_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlaneChase, vCargoPlaneEngineRear[i])
				EXT2_CHECK_BLAST_PLAYER_WITH_JET_ENGINE(v_engine_pos)
			ENDREPEAT
			i = 0
			REPEAT 4 i
				v_engine_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlaneChase, vCargoPlaneEngineFront[i])
				EXT2_CHECK_BLAST_PLAYER_WITH_JET_ENGINE(v_engine_pos)
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the airport security guard at the barrier.
PROC EXT2_CREATE_AIRPORT_GUARD()
	EXT2_CREATE_PED(pedAirportBooth, modelPed[EXT2_MODEL_SECURITY], vAirportBoothPed, fAirportBoothPed)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPed[EXT2_MODEL_SECURITY])
	EXT2_SET_PED_AS_PLAYER_FRIEND(pedAirportBooth)
	SET_PED_MAX_HEALTH(pedAirportBooth, 1)
	SET_PED_CAN_BE_TARGETTED(pedAirportBooth, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAirportBooth, TRUE)
	TASK_STAND_STILL(pedAirportBooth, -1)
	SET_PED_KEEP_TASK(pedAirportBooth, TRUE)
	IF NOT HAS_PED_GOT_WEAPON(pedAirportBooth, WEAPONTYPE_SMG)
		GIVE_WEAPON_TO_PED(pedAirportBooth, WEAPONTYPE_SMG, 30, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Dom yells while driving out of the plane.
PROC EXT2_MANAGE_DRIVE_OUT_DIALOGUE()
	IF bStartedDrivingOutConversation = FALSE
	AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_DRIVE", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_DRIVE conversation") ENDIF #ENDIF
		bStartedDrivingOutConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Dom facing the player.
PROC EXT2_MANAGE_DOM_AT_LANDING_POS()
	IF IS_ENTITY_ALIVE(pedDom)
		IF IS_ENTITY_IN_RANGE_COORDS(pedDom, vLandingPos, 2)
			IF NOT IS_PED_FACING_PED(pedDom, PLAYER_PED_ID(), 20)
				IF NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
				AND NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_PERFORM_SEQUENCE)
					TASK_LOOK_AT_ENTITY(pedDom, PLAYER_PED_ID(), -1)
					TASK_TURN_PED_TO_FACE_ENTITY(pedDom, PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom told to face player") ENDIF #ENDIF
				ENDIF
			ELIF NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_PERFORM_SEQUENCE)
				OPEN_SEQUENCE_TASK(seqDomAnim)
					TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "enter")
					TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "idle_b")
					TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "exit")
				CLOSE_SEQUENCE_TASK(seqDomAnim)
				TASK_PERFORM_SEQUENCE(pedDom, seqDomAnim)
				CLEAR_SEQUENCE_TASK(seqDomAnim)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom performing idles at landing zone") ENDIF #ENDIF
			ENDIF
		ELSE
			IF NOT IsPedPerformingTask(pedDom, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedDom, vLandingPos, PEDMOVE_RUN)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom told to go to vLandingPos") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    B*1561570. Don't continue the EXT2_JUMPOFF conversation if Dom has landed.
PROC EXT2_CHECK_FOR_STOPPING_JUMPOFF_CONVERSATION()
	IF bStoppedJumpedOffATVConversation = FALSE
	AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_23 t_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(t_root, "EXT2_JUMPOFF")
			TEXT_LABEL_23 t_label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
			IF ARE_STRINGS_EQUAL(t_label, "EXT2_JUMPOFF_1")
			OR ARE_STRINGS_EQUAL(t_label, "EXT2_JUMPOFF_2")
			OR ARE_STRINGS_EQUAL(t_label, "EXT2_JUMPOFF_3")
				// Do nothing
			ELSE
				bStoppedJumpedOffATVConversation = TRUE
				KILL_FACE_TO_FACE_CONVERSATION()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Stopped EXT2_JUMPOFF because Dom is no longer on his ATV") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Dom skydiving out of the helicopter.
PROC EXT2_MANAGE_DOM_SKYDIVE()
	VECTOR v_offset
	VECTOR v_dom
	VECTOR v_plane
	INT i_dom_z
	INT i_plane_z
	IF IS_ENTITY_ALIVE(pedDom)
		IF IS_ENTITY_IN_WATER(pedDom) // Kill Dom to prevent mission not progressing if Dom happens to not deploy his parachute and land in the water - see B*450813
			SET_ENTITY_HEALTH(pedDom, 0)
		ENDIF
		SWITCH domStatus
			CASE DOM_IN_PLANE
				IF (GET_GAME_TIMER() - iDomRideOutPlaneTimer) > 240000 // See B*1087874 Mission fail for remaining in the plane is triggered from Dom landing on the ground
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
				OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PREPARE_MUSIC_EVENT("EXTREME2_JUMP")
					SET_ENTITY_PROOFS(pedDom, TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_PED_CAN_TORSO_VEHICLE_IK(pedDom, FALSE)
					IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
					AND IS_ENTITY_ALIVE(vehPlaneSkydive)
						v_offset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlaneSkydive,<<-1,-50,-4>>)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDom, TRUE)
						TASK_VEHICLE_DRIVE_TO_COORD(pedDom, vehATV[VEHICLE_DOM], v_offset, 60, DRIVINGSTYLE_STRAIGHTLINE, modelVehicle[EXT2_MODEL_BLAZER], DF_SwerveAroundAllCars, 3, 100)
						bStartedHurryUpConversation = TRUE
						domStatus = DOM_DRIVING_OUT_PLANE
					ENDIF
				ENDIF
			BREAK
			CASE DOM_DRIVING_OUT_PLANE
				IF IS_ENTITY_ALIVE(vehPlaneSkydive)
					v_dom = GET_ENTITY_COORDS(pedDom)
					v_plane = GET_ENTITY_COORDS(vehPlaneSkydive)
					i_dom_z = ROUND(v_dom.z)
					i_plane_z = ROUND(v_plane.z)
					IF i_dom_z < 250.0
						CLEAR_PED_TASKS_IMMEDIATELY(pedDom)
						TASK_LEAVE_ANY_VEHICLE(pedDom)
						domStatus = DOM_GETTING_OFF_ATV
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom getting off ATV") ENDIF #ENDIF
					ELIF i_dom_z < (i_plane_z-4) // B*764184
						IF bStartedDomTricks = FALSE
							bStartedDomTricks = TRUE
							IK_CONTROL_FLAGS eIKControlFlags
							eIKControlFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK | AIK_DISABLE_TORSO_REACT_IK | AIK_DISABLE_TORSO_VEHICLE_IK
							OPEN_SEQUENCE_TASK(seqDomAnim)
								TASK_PLAY_ANIM(NULL, "rcmextreme2atv", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, DEFAULT, DEFAULT, eIKControlFlags)
								TASK_PLAY_ANIM(NULL, "rcmextreme2atv", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, DEFAULT, DEFAULT, eIKControlFlags)
								TASK_PLAY_ANIM(NULL, "rcmextreme2atv", "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, DEFAULT, DEFAULT, eIKControlFlags)
								TASK_PLAY_ANIM(NULL, "rcmextreme2atv", "idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, DEFAULT, DEFAULT, eIKControlFlags)
								TASK_PLAY_ANIM(NULL, "rcmextreme2atv", "idle_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, DEFAULT, DEFAULT, eIKControlFlags)
								SET_SEQUENCE_TO_REPEAT(seqDomAnim, REPEAT_FOREVER)
							CLOSE_SEQUENCE_TASK(seqDomAnim)
							TASK_PERFORM_SEQUENCE(pedDom, seqDomAnim)
							CLEAR_SEQUENCE_TASK(seqDomAnim)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started Dom's tricks") ENDIF #ENDIF
						ENDIF
					ELSE
						EXT2_MANAGE_DRIVE_OUT_DIALOGUE()
					ENDIF
				ENDIF
			BREAK
			CASE DOM_GETTING_OFF_ATV
				IF NOT IS_PED_IN_ANY_VEHICLE(pedDom)
					TASK_PARACHUTE_TO_TARGET(pedDom, vLandingPos)
					domStatus = DOM_SKYDIVING
				ENDIF
			BREAK
			CASE DOM_SKYDIVING
				IF GET_PED_PARACHUTE_STATE(pedDom) = PPS_INVALID
					SET_PED_COMPONENT_VARIATION(pedDom, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom has landed") ENDIF #ENDIF
					IF missionState = stateJumpOutPlane
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Dom has landed but player is still in the plane, so fail or else all the dialogue will make no sense") ENDIF #ENDIF
						EXT2_MISSION_FAILED(FAILED_DIDNT_ATTEMPT_SKYDIVE)
					ENDIF
					bStoppedJumpedOffATVConversation = FALSE
					domStatus = DOM_LANDED
				ENDIF
			BREAK
			CASE DOM_LANDED
				EXT2_CHECK_FOR_STOPPING_JUMPOFF_CONVERSATION()
				EXT2_MANAGE_DOM_AT_LANDING_POS()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Dom tells the player to ride out of the plane.
PROC EXT2_MANAGE_LETS_GO_DIALOGUE()
	IF bStartedLetsGoConversation = FALSE
	AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
	AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_LETSGO", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_LETSGO conversation") ENDIF #ENDIF
		bStartedLetsGoConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Dom tells the player to hurry up and ride out of the plane.
PROC EXT2_MANAGE_HURRY_UP_DIALOGUE()
	IF bStartedHurryUpConversation = FALSE
	AND EXT2_ALLOW_CONVERSATION_TO_PLAY()
	AND (GET_GAME_TIMER() - iDomRideOutPlaneTimer) > 30000
	AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_COMEON", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_COMEON conversation") ENDIF #ENDIF
		bStartedHurryUpConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if player is on his ATV.
FUNC BOOL EXT2_IS_PLAYER_ON_ATV()
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Draws the checkpoint ring. Must be called every frame.
PROC EXT2_DRAW_TARGET_MARKER()
	DRAW_MARKER(MARKER_RING, vCheckpoint, <<0,0,0>>, vMarkerRotation, <<4, 4, 4>>, 255, 120, 0, 100, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vCheckpoint, <<0,0,0>>, vMarkerRotation, <<9, 9, 9>>, 255, 120, 0, 100, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vCheckpoint, <<0,0,0>>, vMarkerRotation, <<14, 14, 14>>, 255, 120, 0, 100, FALSE, FALSE)
ENDPROC

/// PURPOSE:
///    Handles playing the conversation in which Dom tells the player to perform tricks while skydiving on the ATV.
PROC EXT2_MANAGE_PERFORM_TRICKS_DIALOGUE()
	IF bStartedPerformTricksConversation = FALSE
		IF EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
		AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_TRICKS", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_TRICKS conversation") ENDIF #ENDIF
			bStartedPerformTricksConversation = TRUE
		ENDIF
	ELIF bStoppedPerformTricksConversation = FALSE
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			tStoredTricksConversationLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
			IF ARE_STRINGS_EQUAL(tStoredTricksConversationLabel, "EXT2_TRICKS_3")
				KILL_FACE_TO_FACE_CONVERSATION()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Stopped EXT2_TRICKS conversation") ENDIF #ENDIF
				bStoppedPerformTricksConversation = TRUE
			ENDIF
		ENDIF
	ELIF bRestartedPerformTricksConversation = FALSE
		IF EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
		AND CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConversation, "EXT2AU", "EXT2_TRICKS", tStoredTricksConversationLabel, CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Restarted EXT2_TRICKS conversation with subtitles") ENDIF #ENDIF
			bRestartedPerformTricksConversation = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation when the player has jumped off the ATV.
PROC EXT2_MANAGE_JUMPED_OFF_ATV_DIALOGUE()
	IF bStartedDidntUseATVConversation = FALSE
		IF EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
		AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_NOATV", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_NOATV conversation") ENDIF #ENDIF
			bStartedDidntUseATVConversation = TRUE
		ENDIF
	ELSE
		IF bStartedJumpedOffATVConversation = FALSE
		AND EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
		AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_JUMPOFF", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_JUMPOFF conversation") ENDIF #ENDIF
			bStartedJumpedOffATVConversation = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles playing the conversation when the player has deployed his parachute.
PROC EXT2_MANAGE_DEPLOYED_PARACHUTE_DIALOGUE()
	IF bStartedDeployedParachuteConversation = FALSE
	AND EXT2_ALLOW_CONVERSATION_TO_PLAY(FALSE)
	AND IS_ENTITY_ALIVE(pedDom)
		IF IS_PED_IN_ANY_VEHICLE(pedDom) // B*1119481 Vary conversation if Dom deploys before player or not (Dom will deploy immediately after getting off his ATV, and this check is simpler than faffing with parachute states)
			IF CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_DEPLOY", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_DEPLOY conversation") ENDIF #ENDIF
				bStartedDeployedParachuteConversation = TRUE
			ENDIF
		ELSE
			IF CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_DEPLOY2", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_DEPLOY2 conversation") ENDIF #ENDIF
				bStartedDeployedParachuteConversation = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL EXT2_IS_NOSE_GOING_UP()
	FLOAT fPitchDifference
	fPitchDifference = fCurrentPitch - fPreviousPitch
	IF fPitchDifference > 180
		fPitchDifference -= 360
	ENDIF
	IF fPitchDifference < -180
		fPitchDifference += 360
	ENDIF
	IF fPitchDifference > 0 // nose going up
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL EXT2_HAS_DONE_ROLL()
	fCurrentPitch = GET_ENTITY_PITCH(vehATV[VEHICLE_PLAYER])
	IF bResetStuntValues = TRUE
		fTotalPitch = 0
		fPreviousPitch = fCurrentPitch
		bResetStuntValues = FALSE
	ENDIF
	FLOAT fPitchDifferenceABSF = ABSF(ABSF(fCurrentPitch) - ABSF(fPreviousPitch))
	IF EXT2_IS_NOSE_GOING_UP()
		fTotalPitch += fPitchDifferenceABSF
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Nose going up fPitchDifferenceABSF = ", fPitchDifferenceABSF, " fTotalPitch = ", fTotalPitch) ENDIF #ENDIF
	ELSE
		fTotalPitch -= fPitchDifferenceABSF
		//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Nose going down fPitchDifferenceABSF = ", fPitchDifferenceABSF, " fTotalPitch = ", fTotalPitch) ENDIF #ENDIF
	ENDIF
	fPreviousPitch = fCurrentPitch
	IF fTotalPitch < -360
	OR fTotalPitch > 360
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Keeps track of the ATV roll done by the player.
PROC EXT2_MANAGE_ATV_ROLL_STATS()
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
	AND EXT2_HAS_DONE_ROLL()
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player did a forward or backward roll") ENDIF #ENDIF
		INFORM_MISSION_STATS_OF_INCREMENT(EXT2_NUMBER_OF_SPINS)
		bResetStuntValues = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Shows the checkpoint blip and objective, then draws the marker.
PROC EXT2_SHOW_CHECKPOINT_AND_OBJECTIVE()
	IF NOT DOES_BLIP_EXIST(blipCheckpoint)
		blipCheckpoint = CREATE_COORD_BLIP(vCheckpoint, BLIPPRIORITY_MED, FALSE) // B*764195 - Create checkpoint blip straight away
		PRINT_NOW("EXT2_10", DEFAULT_GOD_TEXT_TIME, 1) // Skydive to the ~y~landing zone.
		iSkydiveHelpTextTimer = GET_GAME_TIMER()
	ELSE
		EXT2_DRAW_TARGET_MARKER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the player skydiving out of the helicopter.
PROC EXT2_MANAGE_PLAYER_SKYDIVE()
	SWITCH statusSkydive
		CASE SKYDIVE_ON_ATV
			EXT2_SHOW_CHECKPOINT_AND_OBJECTIVE()
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CLEAR_HELP()
				KILL_FACE_TO_FACE_CONVERSATION() // Stop the EXT2_TRICKS conversation after the current line has finished
				STOP_SOUND(iATVFallingSound)
				statusSkydive = SKYDIVE_PARACHUTING
			ELSE
				EXT2_MANAGE_PERFORM_TRICKS_DIALOGUE()
				EXT2_MANAGE_ATV_ROLL_STATS()
				IF bDisplayedSkydiveHelpText = FALSE
					IF (GET_GAME_TIMER() - iSkydiveHelpTextTimer) > 1000
						PRINT_HELP("EXT2_05", 7500) // Perform tricks by using ~PAD_LSTICK_ALL~ to roll your ATV.
						bDisplayedSkydiveHelpText = TRUE
					ENDIF
				ELIF bDisplayedJumpOffHelpText = FALSE
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("EXT2_16") // Press ~INPUT_VEH_EXIT~ to jump off your ATV.
						bDisplayedJumpOffHelpText = TRUE
					ENDIF
				ENDIF
				IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
				AND IS_ENTITY_IN_WATER(vehATV[VEHICLE_PLAYER])
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_CLOSE_DOOR|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP) // See B*904660
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					CLEAR_HELP()
					statusSkydive = SKYDIVE_LANDED_OK
				ENDIF
			ENDIF
		BREAK
		CASE SKYDIVE_PARACHUTING
			IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				CLEAR_HELP()
				statusSkydive = SKYDIVE_LANDED_OK
			ELSE
				SWITCH GET_PED_PARACHUTE_STATE(PLAYER_PED_ID())
					CASE PPS_SKYDIVING
						IF bPreparedParachuteMusicEvent = FALSE
							PREPARE_MUSIC_EVENT("EXTREME2_PARA")
							bPreparedParachuteMusicEvent = TRUE
						ENDIF
						EXT2_SHOW_CHECKPOINT_AND_OBJECTIVE()
						EXT2_MANAGE_JUMPED_OFF_ATV_DIALOGUE()
						//IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							//PRINT_HELP_FOREVER("EXT2_06") // Press ~INPUT_SPRINT~ to deploy your parachute. // B*1514261 Apparently this isn't required anymore
						//ENDIF
					BREAK
					CASE PPS_DEPLOYING
						EXT2_DRAW_TARGET_MARKER()
						IF bPlayerDeployedParachute = FALSE
							EXT2_START_AUDIO_SCENE("EXTREME_02_SKYDIVE", FALSE)
							EXT2_START_AUDIO_SCENE("EXTREME_02_OPEN_PARACHUTE", TRUE)
							bPlayerDeployedParachute = TRUE
						ENDIF
					BREAK
					CASE PPS_PARACHUTING
						IF bStartedParachuteMusicEvent = FALSE
							TRIGGER_MUSIC_EVENT("EXTREME2_PARA")
							bStartedParachuteMusicEvent = TRUE
							KILL_FACE_TO_FACE_CONVERSATION() // Stop the EXT2_JUMPOFF conversation after the current line has finished
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
								CLEAR_PRINTS()
							ENDIF
						ENDIF
						EXT2_MANAGE_LANDING_ZONE_FOCUS_CAMERA()
						EXT2_DRAW_TARGET_MARKER()
						EXT2_MANAGE_DEPLOYED_PARACHUTE_DIALOGUE()
					BREAK
					CASE PPS_LANDING
					CASE PPS_INVALID
						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) > 50.0 // Player has manually removed his parachute more than x metres above the ground, so fails the skydive
							IF bPreparedParachuteMusicEvent = TRUE // When player jumps off ATV for a few frames he'll be in PPS_INVALID, so wait until he's been in PPS_SKYDIVING before allowing the mission to fail for this
							AND NOT GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID()) // B*1973306 Don't fail if the player has a reserve parachute
								EXT2_MISSION_FAILED(FAILED_FAILED_SKY_DIVE)
							ENDIF
						ELSE
							statusSkydive = SKYDIVE_LANDING
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE SKYDIVE_LANDING
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			TRIGGER_MUSIC_EVENT("EXTREME2_STOP")
			EXT2_START_AUDIO_SCENE("EXTREME_02_SKYDIVE", FALSE)
			EXT2_START_AUDIO_SCENE("EXTREME_02_OPEN_PARACHUTE", FALSE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player has landed") ENDIF #ENDIF
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCheckpoint) < 500.0
				statusSkydive = SKYDIVE_LANDED_OK
			ELSE
				SAFE_REMOVE_BLIP(blipDom)
				iFailedLandingTimer = GET_GAME_TIMER()
				statusSkydive = SKYDIVE_LANDED_FAR_AWAY
			ENDIF
		BREAK
		CASE SKYDIVE_LANDED_OK
			IF domStatus = DOM_LANDED
				missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				EXT2_MANAGE_DISPLAY_DOM_FOCUS_CAMERA_HELP_TEXT()
				IF bDisplayedGoToLandingZoneText = FALSE
					bDisplayedGoToLandingZoneText = TRUE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCheckpoint) > 15.0
						PRINT_NOW("EXT2_03", DEFAULT_GOD_TEXT_TIME, 1) // Go to the ~y~landing zone.
						CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
					ENDIF
				ELIF bDisplayedWaitDomLandText = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCheckpoint) < 15.0
						SAFE_REMOVE_BLIP(blipCheckpoint)
						PRINT_NOW("EXT2_07", DEFAULT_GOD_TEXT_TIME, 1) // Wait for ~b~Dom.
						CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
						bDisplayedWaitDomLandText = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE SKYDIVE_LANDED_FAR_AWAY
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles deleting the cloud ptfx.
PROC EXT2_MANAGE_DELETING_CLOUD_PTFX()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_moving_clouds)
	AND IS_ENTITY_ALIVE(vehPlaneSkydive)
		vATVOffsetFromPlane = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneSkydive, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		IF vATVOffsetFromPlane.z < -35
			IF fCloudsCurrentAlpha <= 0
				REMOVE_PARTICLE_FX(ptfx_moving_clouds)
				REMOVE_PARTICLE_FX(ptfx_wind_and_smoke)
				REMOVE_PTFX_ASSET()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Cloud ptfx removed") ENDIF #ENDIF
			ELSE
				fCloudsCurrentAlpha -= 0.01
				SET_PARTICLE_FX_LOOPED_ALPHA(ptfx_moving_clouds, fCloudsCurrentAlpha)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles getting Dom onto the jetski and driving off, then passing mission.
PROC EXT2_MANAGE_DOM_GETTING_ON_JETSKI(BOOL b_do_interp)
	IF b_do_interp = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player didn't skip cutscene - doing interp") ENDIF #ENDIF
		CAMERA_INDEX camCatchup = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		SET_CAM_PARAMS(camCatchup, GET_CAM_COORD(camCutscene) , GET_CAM_ROT(camCutscene), GET_CAM_FOV(camCutscene), 0)
		SET_CAM_PARAMS(camCatchup, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-0.25,-1.5,0.5>>), <<0,0,GET_ENTITY_HEADING(PLAYER_PED_ID())>>, GET_CAM_FOV(camCutscene), 2000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Player skipped cutscene - not doing interp") ENDIF #ENDIF
		DESTROY_ALL_CAMS()
		WAIT(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(10.0)
	ENDIF
	IF IS_ENTITY_ALIVE(pedDom)
		IF b_do_interp = TRUE
			CLEAR_PED_TASKS(pedDom)
		ELSE
			CLEAR_PED_TASKS_IMMEDIATELY(pedDom)
			SAFE_TELEPORT_ENTITY(pedDom, <<149.8338, 3664.6121, 30.5093>>, 282.0820)
		ENDIF
		IF IS_VEHICLE_OK(vehJetski)
		AND IS_ENTITY_IN_RANGE_COORDS(vehJetski, vJetski, 20)
		AND IS_VEHICLE_SEAT_FREE(vehJetski)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Sending Dom to get on jetski") ENDIF #ENDIF
			SET_DISABLE_PRETEND_OCCUPANTS(vehJetski, TRUE)
			SET_BOAT_ANCHOR(vehJetski, FALSE)
			OPEN_SEQUENCE_TASK(endSequence)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<171.4, 3662.0, 31.5>>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
				TASK_ENTER_VEHICLE(NULL, vehJetski, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
				TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehJetski, << 2368.1108, 4503.1675, 29.5964 >>, 30, DRIVINGSTYLE_NORMAL, modelVehicle[EXT2_MODEL_SEASHARK], DF_DontSteerAroundPlayerPed, 50, 100)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			CLOSE_SEQUENCE_TASK(endSequence)
			TASK_PERFORM_SEQUENCE(pedDom, endSequence)
			CLEAR_SEQUENCE_TASK(endSequence)
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Jetski not available so making Dom wander") ENDIF #ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDom, FALSE)
			TASK_WANDER_STANDARD(pedDom)
		ENDIF
		SET_PED_KEEP_TASK(pedDom, TRUE)
	ENDIF
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF b_do_interp = TRUE
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Wait for interp") ENDIF #ENDIF
			WAIT(2000) // Wait until camera has interpolated back to the player
		ELSE
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<145.6766, 3661.6016, 30.4832>>, 260.1765)
		ENDIF
	ENDIF
	IF DOES_CAM_EXIST(camCutscene)
		DESTROY_CAM(camCutscene)
	ENDIF
	IF b_do_interp = TRUE
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
	ELSE
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	RC_END_CUTSCENE_MODE()
ENDPROC

/// PURPOSE:
///    Handles playing the dialogue if the player threatens Dom and fails the mission.
PROC EXT2_MANAGE_THREATEN_DOM_DIALOGUE()
	IF failReason = FAILED_DOM_SCARED
	AND bStartedThreatenDomConversation = FALSE
	AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_THREAT", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_THREAT conversation") ENDIF #ENDIF
		bStartedThreatenDomConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures the HD model is used for the cargo plane and Dom's ATV.
PROC EXT2_USE_HD_VEHICLE_MODEL(VEHICLE_INDEX veh_index)
	IF IS_ENTITY_ALIVE(veh_index)
		REQUEST_VEHICLE_HIGH_DETAIL_MODEL(veh_index) // B*1460873 Ensure plane doesn't change to its low LOD model
		IF IS_VEHICLE_HIGH_DETAIL(veh_index)
			SET_FORCE_HD_VEHICLE(veh_index, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Setup Dom for cutscene.
PROC EXT2_SETUP_DOM_CUTSCENE_VARIATIONS()
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "EXTREME2: CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY is TRUE") #ENDIF
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Dom", sRCLauncherDataLocal.pedID[0])	
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set cutscene variations for sRCLauncherDataLocal.pedID[0] in cutscene") ENDIF #ENDIF
		ELIF IS_ENTITY_ALIVE(pedDom)
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Dom", pedDom)	
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Set cutscene variations for pedDom in cutscene") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main state for handling the intro cutscene.
PROC MS_INTRO()
	EXT2_SETUP_DOM_CUTSCENE_VARIATIONS()
	VECTOR v_vec, v_rot
	IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
		SET_FORCE_HD_VEHICLE(vehATV[VEHICLE_DOM], TRUE) // For B*1512897
	ENDIF
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_SCREEN_FADED_IN() // Don't do gameplay hint if skipping to intro
				AND NOT IS_REPLAY_IN_PROGRESS() // Or if replaying mission
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-954.535706,-2760.553223,11.916363>>, <<-947.684692,-2749.718994,15.631608>>, 26.000000) // See B*1543280
					REMOVE_CUTSCENE()
					RC_PRE_REQUEST_CUTSCENE(FALSE)
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("es_2_rcm_concat", CS_SECTION_2|CS_SECTION_3)
					iCutsceneStage = 0
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Loading partial es_2_rcm_concat cutscene") ENDIF #ENDIF
					SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0, 0, 0.5>>), TRUE, -1, 3000)															
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.45)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.9)
	                SET_GAMEPLAY_HINT_FOV(30.00)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
					OPEN_SEQUENCE_TASK(seqFranklinLeadIn)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							TASK_LEAVE_ANY_VEHICLE(NULL)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: task sequence for Franklin: leaving vehicle") ENDIF #ENDIF
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) > 6
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-953.09198, -2758.35083, 12.94>>, PEDMOVEBLENDRATIO_WALK) // Where Dom lands
							TASK_GO_TO_ENTITY(NULL, sRCLauncherDataLocal.pedID[0], DEFAULT_TIME_BEFORE_WARP, 6, PEDMOVEBLENDRATIO_WALK)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: task sequence for Franklin: walking to Dom") ENDIF #ENDIF
						ENDIF
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[0])
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: task sequence for Franklin: turning to Dom") ENDIF #ENDIF
					CLOSE_SEQUENCE_TASK(seqFranklinLeadIn)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqFranklinLeadIn)
					CLEAR_SEQUENCE_TASK(seqFranklinLeadIn)
					iLeadInTimer = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Doing leadin focus camera") ENDIF #ENDIF
				ELSE
					RC_REQUEST_CUTSCENE("es_2_rcm_concat")
					iCutsceneStage = 2
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Loading full es_2_rcm_concat cutscene") ENDIF #ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Not doing leadin focus camera") ENDIF #ENDIF
				ENDIF
			ENDIF
			// B*1958190 In case player bails out before triggering the RC
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[2])
				vehIntroTruck = sRCLauncherDataLocal.vehID[2]
				IF IS_ENTITY_ALIVE(vehIntroTruck)
					SET_ENTITY_AS_MISSION_ENTITY(vehIntroTruck, TRUE, TRUE)
					SAFE_TELEPORT_ENTITY(vehIntroTruck, vIntroTruck, fIntroTruck)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehIntroTruck)
					FREEZE_ENTITY_POSITION(vehIntroTruck, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Grabbed truck, teleported and frozen before intro") ENDIF #ENDIF
				ENDIF
			ENDIF
			bCutsceneSkipped = FALSE
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
		
			IF iCutsceneStage = 0
			OR iCutsceneStage = 1
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) < 5
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0])
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Franklin closer than 5m to Dom so halting him") ENDIF #ENDIF
				ENDIF
			ENDIF
		
			SWITCH iCutsceneStage
				CASE 0
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
						REQUEST_ANIM_DICT("rcmextreme2")
						IF HAS_ANIM_DICT_LOADED("rcmextreme2")
							ADD_PED_FOR_DIALOGUE(sConversation, 0, sRCLauncherDataLocal.pedID[0], "DOM")
							ADD_PED_FOR_DIALOGUE(sConversation, 1, PLAYER_PED_ID(), "FRANKLIN")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_INT_LI", CONV_PRIORITY_HIGH)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_INT_LI conversation") ENDIF #ENDIF
								FREEZE_ENTITY_POSITION(sRCLauncherDataLocal.pedID[0], FALSE)
								TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[0], "rcmextreme2", "leadin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_IGNORE_GRAVITY|AF_HOLD_LAST_FRAME)
								TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[0], PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT)
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0], -1)
								PLAY_FACIAL_ANIM(sRCLauncherDataLocal.pedID[0], "leadin_facial", "rcmextreme2") // B*1502033
								REQUEST_MODEL(modelVehicle[EXT2_MODEL_BLAZER])
								REQUEST_VEHICLE_RECORDING(500, "Ext2_DomIntoAirport")
								iCutsceneStage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF (GET_GAME_TIMER() - iLeadInTimer) > 3000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							REMOVE_PED_FOR_DIALOGUE(sConversation, 0)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Done leadin") ENDIF #ENDIF
							iCutsceneStage++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					REQUEST_MODEL(modelVehicle[EXT2_MODEL_BLAZER])
					REQUEST_VEHICLE_RECORDING(500, "Ext2_DomIntoAirport")
					IF RC_IS_CUTSCENE_OK_TO_START()
					AND HAS_MODEL_LOADED(modelVehicle[EXT2_MODEL_BLAZER])
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(500, "Ext2_DomIntoAirport")
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
							pedDom = sRCLauncherDataLocal.pedID[0]
							SET_ENTITY_AS_MISSION_ENTITY(pedDom, TRUE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(pedDom, "Dom", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modelPed[EXT2_MODEL_DOM])
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Dom") ENDIF #ENDIF
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(pedDom, "Dom", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modelPed[EXT2_MODEL_DOM])
						ENDIF
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
							vehATV[VEHICLE_PLAYER] = sRCLauncherDataLocal.vehID[0]
							SET_ENTITY_AS_MISSION_ENTITY(vehATV[VEHICLE_PLAYER], TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: SET_ENTITY_AS_MISSION_ENTITY vehATV[VEHICLE_PLAYER]") ENDIF #ENDIF
						ELSE
							EXT2_CREATE_ATV(VEHICLE_PLAYER)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: sRCLauncherDataLocal.vehID[0] didn't exist so created vehATV[VEHICLE_PLAYER]") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
							REGISTER_ENTITY_FOR_CUTSCENE(vehATV[VEHICLE_PLAYER], "Franklin_Quad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modelPed[EXT2_MODEL_BLAZER])
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Registered vehATV[VEHICLE_PLAYER]") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[1])
							vehATV[VEHICLE_DOM] = sRCLauncherDataLocal.vehID[1]
							SET_ENTITY_AS_MISSION_ENTITY(vehATV[VEHICLE_DOM], TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: SET_ENTITY_AS_MISSION_ENTITY vehATV[VEHICLE_DOM]") ENDIF #ENDIF
						ELSE
							EXT2_CREATE_ATV(VEHICLE_DOM)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: sRCLauncherDataLocal.vehID[1] didn't exist so created vehATV[VEHICLE_DOM]") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
							REGISTER_ENTITY_FOR_CUTSCENE(vehATV[VEHICLE_DOM], "DOM_Quad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modelPed[EXT2_MODEL_BLAZER])
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Registered vehATV[VEHICLE_DOM]") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(vehIntroTruck)
							REGISTER_ENTITY_FOR_CUTSCENE(vehIntroTruck, "DOM_Flatbed_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modelVehicle[EXT2_MODEL_FLATBED])
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: CU_ANIMATE_EXISTING_SCRIPT_ENTITY DOM_Flatbed_truck") ENDIF #ENDIF
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(vehIntroTruck, "DOM_Flatbed_truck", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modelVehicle[EXT2_MODEL_FLATBED])
						ENDIF
						RC_CLEANUP_LAUNCHER() // Cleanup launcher to remove lead-in blip
						START_CUTSCENE()
						IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
							SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(GET_ENTITY_MODEL(vehATV[VEHICLE_PLAYER])) // B*2027007
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: set_vehicle_model_player_will_exit_scene") ENDIF #ENDIF
						ENDIF
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 3
					IF IS_CUTSCENE_PLAYING()
						IF IS_GAMEPLAY_HINT_ACTIVE()
							STOP_GAMEPLAY_HINT()
						ENDIF
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-954.774231,-2761.566650,11.944938>>, <<-947.811523,-2750.971191,15.674092>>, 9.000000, vPlayerCarRespot, fPlayerCarRespot)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-945.467407,-2754.302246,11.870284>>, <<-989.596558,-2835.254883,15.964786>>, 7.000000, vPlayerCarRespot, fPlayerCarRespot)
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespot, fPlayerCarRespot)
						RC_START_CUTSCENE_MODE(<<-954.19, -2760.05, 14.64>>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						SAFE_TELEPORT_ENTITY(vehATV[VEHICLE_PLAYER], vATV[VEHICLE_PLAYER], fATV[VEHICLE_PLAYER])
						SAFE_TELEPORT_ENTITY(vehATV[VEHICLE_DOM], vATV[VEHICLE_DOM], fATV[VEHICLE_DOM])
						EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateIntro, FALSE) // Preload all these as we want to start immediately after the intro and not wait for the cargo plane etc to load
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 4
					IF IS_CUTSCENE_PLAYING()
						IF NOT DOES_ENTITY_EXIST(pedDom)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Dom"))
							pedDom = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Dom"))
							SET_ENTITY_AS_MISSION_ENTITY(pedDom, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Grabbed pedDom from Dom") ENDIF #ENDIF
						ENDIF		
						IF NOT DOES_ENTITY_EXIST(vehIntroTruck)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("DOM_Flatbed_truck"))
							vehIntroTruck = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("DOM_Flatbed_truck"))
							SET_ENTITY_AS_MISSION_ENTITY(vehIntroTruck, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Grabbed vehIntroTruck from DOM_Flatbed_truck") ENDIF #ENDIF
						ENDIF
						IF IS_ENTITY_ALIVE(pedDom)
						AND IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
						AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dom")
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: SET_FORCE_HD_VEHICLE on vehATV[VEHICLE_DOM] set to TRUE") ENDIF #ENDIF
							SET_PED_INTO_VEHICLE(pedDom, vehATV[VEHICLE_DOM])
							IF NOT WAS_CUTSCENE_SKIPPED() // Can't start a playable on a fixed vehicle so only start playable if not skipped
								START_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 500, "Ext2_DomIntoAirport")
								PAUSE_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM])
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 1000)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Teleported pedDom onto vehATV[VEHICLE_DOM] and started & paused playback") ENDIF #ENDIF
							ELSE
								v_vec = GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(500, "Ext2_DomIntoAirport"), 1000)
								v_rot = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(500, "Ext2_DomIntoAirport"), 1000)
								SAFE_TELEPORT_ENTITY(vehATV[VEHICLE_DOM], v_vec, v_rot.z)
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Teleported pedDom onto vehATV[VEHICLE_DOM] and moved it to playback position") ENDIF #ENDIF
								//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: v_vec = ", v_vec) ENDIF #ENDIF
								//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: v_rot = ", v_rot) ENDIF #ENDIF
							ENDIF
							SET_VEHICLE_ENGINE_ON(vehATV[VEHICLE_DOM], TRUE, TRUE)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehATV[VEHICLE_DOM])
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedDom)
						ENDIF
						IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
							IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								ENDIF
								#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Franklin given task to get onto vehATV[VEHICLE_PLAYER]") ENDIF #ENDIF
							ENDIF
							IF NOT CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin_Quad")
							AND IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
							AND IS_ENTITY_VISIBLE(vehATV[VEHICLE_PLAYER])
								SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehATV[VEHICLE_PLAYER], TRUE, FALSE)
								//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Doing SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION") ENDIF #ENDIF
							ENDIF
						ENDIF
						IF WAS_CUTSCENE_SKIPPED()
							bCutsceneSkipped = TRUE
						ENDIF
					ELSE
						IF bCutsceneSkipped = TRUE
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						ENDIF
						RC_END_CUTSCENE_MODE()
						IF IS_ENTITY_ALIVE(vehIntroTruck)
							FREEZE_ENTITY_POSITION(vehIntroTruck, FALSE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Unfroze truck after intro") ENDIF #ENDIF
						ENDIF
						EXT2_SETUP_BEFORE_MOCAP_FINISHES()
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: IS_CUTSCENE_PLAYING returned false") ENDIF #ENDIF
						missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0)
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going into the airport.
PROC MS_GO_INTO_AIRPORT()
	IF iPlayerAttachedTimer > -1
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_SCRIPT_RUP) // B*1308933 B*1483507 Prevent the player bailing after he's driven into the plane
	ENDIF
	EXT2_USE_HD_VEHICLE_MODEL(vehATV[VEHICLE_DOM]) // For B*1512897
	EXT2_USE_HD_VEHICLE_MODEL(vehPlaneChase)
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_INTO_AIRPORT" #ENDIF)
			IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehATV[VEHICLE_DOM], FALSE)
				SET_ENTITY_LOD_DIST(vehATV[VEHICLE_DOM], 500)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehATV[VEHICLE_DOM])
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM])
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Playback already started on vehATV[VEHICLE_DOM] so unpausing") ENDIF #ENDIF
				ELSE
					START_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 500, "Ext2_DomIntoAirport")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehATV[VEHICLE_DOM], 1000)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Starting playback on vehATV[VEHICLE_DOM]") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(pedDom)
				IF NOT DOES_BLIP_EXIST(blipDom)
					blipDom = CREATE_PED_BLIP(pedDom, TRUE, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blipDom)
					SET_BLIP_SCALE(blipDom, BLIP_SIZE_PED)
				ENDIF
			ENDIF
			EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateIntro)
			EXT2_CREATE_INTRO_TRUCK()
			EXT2_SPAWN_AIRPORT_PLANES()
			IF IS_ENTITY_ALIVE(vehPlaneChase)
			AND NOT DOES_BLIP_EXIST(blipPlane)
				blipPlane = CREATE_VEHICLE_BLIP(vehPlaneChase)
			ENDIF
			PRINT_NOW("EXT2_02", DEFAULT_GOD_TEXT_TIME, 1) // Follow Dom on to the ~b~cargo plane.
			EXT2_CREATE_AIRPORT_GUARD()
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
			bPlayerIsOnATV = TRUE
			bCheckPlayerATV = TRUE
			bDisplayedGetBackOnATV = FALSE
			iTimesDisplayedRideATVObjective = 1
			bShutCargoPlaneDoors = FALSE
			bStartedDriveInPlaneConversation1 = FALSE
			bStartedDriveInPlaneConversation2 = FALSE
			bStartedDriveInPlaneConversation3 = FALSE
			bStartedFirstConversation = FALSE
			bStartedGuardConversation = FALSE
			bStartedRunwayMusicEvent = FALSE
			bStartedEnterPlaneMusicEvent = FALSE
			bPlayingPlaneStream = FALSE
			iPlayerAttachedTimer = -1
			iDoneRemindConversation = 0
			iRemindConversationTimer = GET_GAME_TIMER()
			iPlayerCurseTimer = GET_GAME_TIMER()
			statusDomATV = ATV_INTO_AIRPORT
			iCheckEngineTimer = GET_GAME_TIMER()
			IF NOT Is_Replay_In_Progress()
				TRIGGER_MUSIC_EVENT("EXTREME2_START")
			ENDIF
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_AIRPORT_BARRIER_IN, TRUE)
			CLEAR_AREA_OF_VEHICLES(<<-1028,-2879,14>>, 50, FALSE) // B*1748672 - remove new cargens during mission
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			EXT2_MANAGE_PLANE_SFX(vehPlaneChase, 1.0)
			IF iPlayerAttachedTimer = -1
				EXT2_MANAGE_PLAYER_ATTACH_TO_PLANE()
				EXT2_MANAGE_PLANE_TAKING_OFF()
				EXT2_MANAGE_DOM_ATTACH_TO_PLANE()
				EXT2_MANAGE_RUNWAY_MUSIC_EVENT()
				EXT2_CHECK_PLAYER_STILL_ON_ATV()
				EXT2_CHECK_PLAYER_ISNT_ABANDONING_DOM()
				EXT2_CHECK_PLAYER_NEAR_ENGINES()
				EXT2_MANAGE_AMBIENT_AIRPORT_PLANE()
			ELSE
				IF ((GET_GAME_TIMER() - iPlayerAttachedTimer) > 1500 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
				OR EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(2, -2, -12, -4) // B*1146772 Don't let player smash into Dom
					missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
				ELSE
					EXT2_MANAGE_CARGO_PLANE_SPEED()
				ENDIF
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_INTO_AIRPORT" #ENDIF)
			SAFE_REMOVE_BLIP(blipDom)
			SAFE_REMOVE_BLIP(blipPlane)
			SAFE_RELEASE_VEHICLE(vehIntroTruck)
			SAFE_RELEASE_PED(pedAirportBooth)
			SAFE_RELEASE_PED(pedPlanePilot[1])
			SAFE_RELEASE_VEHICLE(vehAirportPlane)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_gen_sparks)
				STOP_PARTICLE_FX_LOOPED(ptfx_gen_sparks)
			ENDIF
			REMOVE_PTFX_ASSET()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the midtro cutscene.
PROC MS_MIDTRO_PLANE()
	EXT2_USE_HD_VEHICLE_MODEL(vehPlaneChase)
	EXT2_USE_HD_VEHICLE_MODEL(vehPlaneSkydive)
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_MIDTRO_PLANE" #ENDIF)
			KILL_FACE_TO_FACE_CONVERSATION() // B*1462010 Don't cut off the playing line
			EXT2_START_AUDIO_SCENE("EXTREME_02_DRIVE_UP_RAMP", FALSE)
			EXT2_START_AUDIO_SCENE("EXTREME_02_PLANE_TAKEOFF_CUTSCENE", TRUE)
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
			IF IS_ENTITY_ALIVE(vehPlaneChase)
				IF IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
					IF EXT2_IS_PLAYER_WITHIN_ACCEPTABLE_AREA_FROM_PLANE(2, -2, -24, -4)
						ATTACH_ENTITY_TO_ENTITY(vehATV[VEHICLE_PLAYER], vehPlaneChase, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneChase, GET_ENTITY_COORDS(vehATV[VEHICLE_PLAYER])), <<0,0,0>>, FALSE, FALSE, TRUE)
					ELSE
						ATTACH_ENTITY_TO_ENTITY(vehATV[VEHICLE_PLAYER], vehPlaneChase, 0, <<0,-24,-3>>, <<0,0,0>>)
					ENDIF
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
					ENDIF
				ENDIF
				SET_VEHICLE_DOORS_SHUT(vehPlaneChase, TRUE)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
					SKIP_TIME_IN_PLAYBACK_RECORDED_vehicle(vehPlaneChase, 12000 - GET_TIME_POSITION_IN_RECORDING(vehPlaneChase))
				ENDIF
				CONTROL_LANDING_GEAR(vehPlaneChase, LGC_RETRACT)
				WAIT(0)
			ENDIF
			camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			SET_CAM_PARAMS(camCutscene, << -1359.8, -2217.8, 14.2 >>, << 5.6, -0.0, 160.7 >>, 50.0000, 0)
			SET_CAM_PARAMS(camCutscene, << -1348.5, -2198.4, 14.2 >>, << 11.5, 0.0, 168.1 >>, 50.0000, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
			AND NOT IS_ENTITY_ATTACHED(vehATV[VEHICLE_DOM])
			AND IS_ENTITY_ALIVE(vehPlaneChase)
				ATTACH_ENTITY_TO_ENTITY(vehATV[VEHICLE_DOM], vehPlaneChase, 0, << 0.902488, -6, -3.55149 >>, <<0,0,0>>, FALSE, FALSE, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "Dom's ATV wasn't attached when the cutscene started so attached it inside vehPlaneChase") ENDIF #ENDIF
			ENDIF
			STOP_STREAM()
			EXT2_STOP_CARREC_IF_PLAYING(vehAirportPlane)
			EXT2_STOP_CARREC_IF_PLAYING(vehATV[VEHICLE_DOM])
			REMOVE_VEHICLE_RECORDING(500, "Ext2_DomIntoAirport")
			REMOVE_VEHICLE_RECORDING(502, "Ext2_AirportVeh")
			SAFE_DELETE_PED(pedPlanePilot[1])
			SAFE_DELETE_VEHICLE(vehAirportPlane)
			streamVolumeExtreme2 = STREAMVOL_CREATE_SPHERE(<< -1389.5, -2218.5, 33.7 >>, 400.0, FLAG_MAPDATA)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P1_SKYDIVING)
			PRELOAD_OUTFIT(PLAYER_PED_ID(), SPECIAL_P1_PARACHUTE)
			iCutsceneStage = 0
			iCutsceneWaitTime = GET_GAME_TIMER()
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			IF IS_ENTITY_ALIVE(vehPlaneChase)
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
				SET_PLAYBACK_SPEED(vehPlaneChase, 0.9)
			ENDIF
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				iCutsceneStage = 5
			ENDIF
			SWITCH iCutsceneStage
				CASE 0
					IF LOAD_STREAM("Plane_Flyby", "EXTREME_02_SOUNDSET")
						PLAY_STREAM_FRONTEND()
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF (GET_GAME_TIMER() - iCutsceneWaitTime) > 500
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF EXT2_ALLOW_CONVERSATION_TO_PLAY()
					AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_MIDTRO", CONV_PRIORITY_HIGH)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_MIDTRO conversation") ENDIF #ENDIF
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 3
					IF IS_ENTITY_ALIVE(vehPlaneChase)
					AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlaneChase)
					AND GET_TIME_POSITION_IN_RECORDING(vehPlaneChase) > 16000
						SET_CAM_PARAMS(camCutscene, << -1389.5, -2218.5, 33.8 >>, << 4.4, 0.0, -44.2 >>, 40.0000, 0)
						SET_CAM_PARAMS(camCutscene, << -1322.0, -2143.9, 69.7 >>, << 2.1, 0.0, -43.0 >>, 40.0000, 25000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 4
					IF EXT2_ALLOW_CONVERSATION_TO_PLAY()
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 5
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					EXT2_CREATE_CARGO_PLANE_FOR_SKYDIVE()
					RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)
					DESTROY_ALL_CAMS()
					STOP_SOUND(iSparksSound)
					EXT2_START_AUDIO_SCENE("EXTREME_02_PLANE_TAKEOFF_CUTSCENE", FALSE)
					SAFE_DELETE_OBJECT(objectInvisibleProp)
					EXT2_STOP_CARREC_IF_PLAYING(vehPlaneChase)
					REMOVE_VEHICLE_RECORDING(503, "Ext2_CargoTakeOff")
					SAFE_DELETE_PED(pedPlanePilot[0])
					SAFE_DELETE_VEHICLE(vehPlaneChase)
					IF STREAMVOL_IS_VALID(streamVolumeExtreme2)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Deleting streamVolumeExtreme2") ENDIF #ENDIF
						STREAMVOL_DELETE(streamVolumeExtreme2)
					ENDIF
					WAIT(1000)
					RC_END_CUTSCENE_MODE(TRUE, TRUE, FALSE)
					EXT2_START_AUDIO_SCENE("EXTREME_02_INSIDE_PLANE", TRUE)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
				BREAK
			ENDSWITCH
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_MIDTRO_PLANE" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling jumping out of the cargo plane.
PROC MS_JUMP_OUT_PLANE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE) // B*1276475 - Prevent player taking off backpack if getting off the ATV
	ENDIF
	IF IS_ENTITY_ALIVE(vehPlaneSkydive)
		EXT2_USE_HD_VEHICLE_MODEL(vehPlaneSkydive)
		SET_VEHICLE_INACTIVE_DURING_PLAYBACK(vehPlaneSkydive, TRUE)
	ENDIF
	EXT1_SET_RADAR_TO_PLANE_INTERIOR()
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_JUMP_OUT_PLANE" #ENDIF)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_SKYDIVE, "ATV skydive")
			IF IS_ENTITY_ALIVE(vehATV[VEHICLE_DOM])
			AND NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehATV[VEHICLE_DOM])
				SET_VEHICLE_ENGINE_ON(vehATV[VEHICLE_DOM], TRUE, TRUE)
			ENDIF
			iDomRideOutPlaneTimer = GET_GAME_TIMER()
			domStatus = DOM_IN_PLANE
			bPlayerIsOnATV = FALSE
			bCheckPlayerATV = TRUE
			bDisplayedGetBackOnATV = FALSE
			bStartedLetsGoConversation = FALSE
			bStartedHurryUpConversation = FALSE
			bStartedDrivingOutConversation = FALSE
			bPlayingPlaneStream = FALSE
			iTimesDisplayedRideATVObjective = 0
			SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_VEHICLE_IMPACT | RBF_IMPACT_OBJECT)
			IF Is_Replay_In_Progress()
				TRIGGER_MUSIC_EVENT("EXTREME2_RESTART2")
			ELSE
				TRIGGER_MUSIC_EVENT("EXTREME2_READY")
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			EXT2_CREATE_CLOUD_PTFX()
			EXT2_MANAGE_PLANE_SFX(vehPlaneSkydive, 0.0) // 0 is fully inside, 1 is outside
			EXT2_MANAGE_DOM_SKYDIVE()
			EXT2_MANAGE_LETS_GO_DIALOGUE()
			EXT2_MANAGE_HURRY_UP_DIALOGUE()
			EXT2_CHECK_PLAYER_STILL_ON_ATV()
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(vehPlaneSkydive)
				vATVOffsetFromPlane = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehPlaneSkydive, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				//#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: vATVOffsetFromPlane = ", vATVOffsetFromPlane) ENDIF #ENDIF
				IF vATVOffsetFromPlane.z < -6.5
				OR vATVOffsetFromPlane.y < -40
					REPLAY_RECORD_BACK_FOR_TIME(5.0)
					IF bPlayerIsOnATV = TRUE
						bStartedDidntUseATVConversation = TRUE // Set to true because we won't need to play it
						statusSkydive = SKYDIVE_ON_ATV
					ELSE
						bStartedDidntUseATVConversation = FALSE
						statusSkydive = SKYDIVE_PARACHUTING
					ENDIF
					IF IS_ENTITY_ALIVE(vehPlaneSkydive)
						SET_VEHICLE_INACTIVE_DURING_PLAYBACK(vehPlaneSkydive, FALSE)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehPlaneSkydive)
					ENDIF
					STOP_STREAM() // Cargo plane wind stream
					TRIGGER_MUSIC_EVENT("EXTREME2_JUMP")
					SET_LIGHT_RIG_ON_CARGO_PLANE(FALSE)
					SAFE_REMOVE_BLIP(blipPlayerATV)
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						SET_RAGDOLL_BLOCKING_FLAGS(PLAYER_PED_ID(), RBF_NONE)
					ENDIF
					missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
				ENDIF
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_LOW)
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_JUMP_OUT_PLANE" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling skydiving out of the cargo plane.
PROC MS_SKYDIVE()
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_SKYDIVE" #ENDIF)
			IF IS_ENTITY_ALIVE(pedDom)
			AND NOT DOES_BLIP_EXIST(blipDom)
				blipDom = CREATE_PED_BLIP(pedDom, TRUE, TRUE)
			ENDIF
			bCheckPlayerATV = FALSE
			bDisplayedSkydiveHelpText = FALSE
			bDisplayedJumpOffHelpText = FALSE
			bStartedPerformTricksConversation = FALSE
			bStoppedPerformTricksConversation = FALSE
			bRestartedPerformTricksConversation = FALSE
			bStartedJumpedOffATVConversation = FALSE
			bStartedDeployedParachuteConversation = FALSE
			bStartedDomTricks = FALSE
			bPlayerDeployedParachute = FALSE
			bResetStuntValues = TRUE
			bPreparedParachuteMusicEvent = FALSE
			bStartedParachuteMusicEvent = FALSE
			bDisplayedGoToLandingZoneText = FALSE
			bDisplayedWaitDomLandText = FALSE
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
			ENDIF
			EXT2_SPAWN_JETSKIS()
			EXT2_START_AUDIO_SCENE("EXTREME_02_INSIDE_PLANE", FALSE)
			EXT2_START_AUDIO_SCENE("EXTREME_02_SKYDIVE", TRUE)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(vehATV[VEHICLE_PLAYER])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehATV[VEHICLE_PLAYER])
				PLAY_SOUND_FROM_ENTITY(iATVFallingSound, "PLAYER_AT_SPEED_FREEFALL_MASTER", vehATV[VEHICLE_PLAYER])
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started PLAYER_AT_SPEED_FREEFALL_MASTER sfx") ENDIF #ENDIF
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			IF statusSkydive != SKYDIVE_LANDED_FAR_AWAY
				EXT2_MANAGE_DOM_SKYDIVE()
				EXT2_MANAGE_DELETING_CLOUD_PTFX()
				EXT2_MANAGE_PLAYER_SKYDIVE()
				HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, sConversation, "EXT2AU", tSavedConversationRoot, tSavedConversationLabel)
			ELIF (GET_GAME_TIMER() - iFailedLandingTimer) > 3000
				EXT2_MISSION_FAILED(FAILED_OUTSIDE_ZONE)
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(15.0, 0.0)
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_SKYDIVE" #ENDIF)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			IF bPlayerDeployedParachute = FALSE
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(EXT2_LEAP_FROM_ATV_TO_WATER)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Achieved stat: EX2_LEAP_FROM_ATV_TO_WATER") ENDIF #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going to meet Dom after the skydive.
PROC MS_GO_TO_DOM()
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_TO_DOM" #ENDIF)
			SAFE_REMOVE_BLIP(blipCheckpoint)
			IF IS_ENTITY_ALIVE(pedDom)
				SET_ENTITY_PROOFS(pedDom, FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedDom, FALSE)
				SET_PED_CONFIG_FLAG(pedDom, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDom, TRUE)
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDom, 15)
					IF NOT DOES_BLIP_EXIST(blipDom)
						blipDom = CREATE_PED_BLIP(pedDom, TRUE, TRUE)
					ENDIF
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vLandingPos, 10) // See B*1254800 - Don't give "get to Dom" objective if player is already at the landing spot.
						PRINT_NOW("EXT2_01", DEFAULT_GOD_TEXT_TIME, 1) // Wait for ~b~Dom.
					ELSE
						PRINT_NOW("EXT2_04", DEFAULT_GOD_TEXT_TIME, 1) // Get to ~b~Dom.
					ENDIF
					CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
				ENDIF
			ENDIF
			SAFE_DELETE_VEHICLE(vehPlaneSkydive)
			bReachedDom = FALSE
			bNearToDomStoppedConversation = FALSE
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				EXT2_MANAGE_DOM_AT_LANDING_POS()
				IF bReachedDom = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCheckpoint, FALSE) > 500.0
						EXT2_MISSION_FAILED(FAILED_ABANDONED_DOM)
					ELIF IS_ENTITY_ALIVE(pedDom)
						IF bNearToDomStoppedConversation = FALSE
						AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDom, 20) // B*1543309
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Near to Dom so stopping conversation in readiness for outro") ENDIF #ENDIF
							STOP_CHECKING_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation)
							KILL_FACE_TO_FACE_CONVERSATION() // Stop the EXT2_DEPLOY conversation after the current line has finished
							bNearToDomStoppedConversation = TRUE
						ENDIF
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDom, 10)
						AND IS_ENTITY_IN_RANGE_COORDS(pedDom, vLandingPos, 10) // B*1323668 Trigger if Dom is near enough
							bReachedDom = TRUE
						ELIF bNearToDomStoppedConversation = FALSE
							HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, sConversation, "EXT2AU", tSavedConversationRoot, tSavedConversationLabel)
						ENDIF
					ENDIF
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
				ENDIF
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_TO_DOM" #ENDIF)
			SAFE_REMOVE_BLIP(blipDom)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the final conversation between Franklin and Dom.
PROC MS_FINAL_CONVERSATION()
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_FINAL_CONVERSATION" #ENDIF)
			EXT2_LOAD_ASSETS_FOR_MISSION_STATE(stateFinalConversation)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(pedDom)
				iSynchedScene = CREATE_SYNCHRONIZED_SCENE(<<vPlayerOutro.x, vPlayerOutro.y+2.3, vPlayerOutro.z+1.05>>, <<0,0,110>>)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSynchedScene, "rcmextreme2", "banter_franklin", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_TAG_SYNC_OUT)
				TASK_SYNCHRONIZED_SCENE(pedDom, iSynchedScene, "rcmextreme2", "banter_dom", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_TAG_SYNC_OUT)
				camCutscene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				PLAY_SYNCHRONIZED_CAM_ANIM(camCutscene, iSynchedScene, "banter_cam", "rcmextreme2")
				RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CLEAR_AREA(vLandingPos, 10, TRUE) // B*1322579 - Clear immediate area of stuff
				REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_HEADSET) // Remove headset while Franklin is offscreen at start of outro rather than at end
			ENDIF
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
			iCutsceneStage = 0
			iCutsceneWaitTime = GET_GAME_TIMER()
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				SAFE_FADE_SCREEN_OUT_TO_BLACK()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				EXT2_MANAGE_DOM_GETTING_ON_JETSKI(FALSE)
				WAIT(250)
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
				missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
			ELSE
				SWITCH iCutsceneStage
					CASE 0
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
						IF EXT2_ALLOW_CONVERSATION_TO_PLAY()
						AND CREATE_CONVERSATION(sConversation, "EXT2AU", "EXT2_OUTRO", CONV_PRIORITY_HIGH)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Started EXT2_OUTRO conversation") ENDIF #ENDIF
							iCutsceneStage++
						ENDIF
					BREAK
					CASE 1
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSynchedScene)
						AND GET_SYNCHRONIZED_SCENE_PHASE(iSynchedScene) > 0.99
							EXT2_MANAGE_DOM_GETTING_ON_JETSKI(TRUE)
							missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_FINAL_CONVERSATION" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Starts the mission failed sequence.
PROC MS_FAILED()
	SWITCH missionStateMachine
		CASE EXT2_STATE_MACHINE_SETUP
			EXT2_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
			CLEAR_PRINTS()
			CLEAR_HELP()
			EXT2_REMOVE_ALL_BLIPS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			TRIGGER_MUSIC_EVENT("EXTREME2_FAIL")
			EXT2_SET_PED_TO_FLEE()
			bStartedThreatenDomConversation = FALSE
			SWITCH failReason
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DEFAULT") ENDIF #ENDIF
				BREAK
				CASE FAILED_DOM_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DOM_DIED") ENDIF #ENDIF
					failString = "EXT2_F1" // ~s~Dom died.
				BREAK
				CASE FAILED_ATV_WRECKED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_ATV_WRECKED") ENDIF #ENDIF
					failString = "EXT2_F3" // ~s~Franklin's ATV was destroyed.
				BREAK
				CASE FAILED_ABANDONED_DOM
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_ABANDONED_DOM") ENDIF #ENDIF
					failString = "EXT2_F2" // ~s~Franklin abandoned Dom.
				BREAK
				CASE FAILED_DIDNT_FOLLOW_DOM
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DIDNT_FOLLOW_DOM") ENDIF #ENDIF
					failString = "EXT2_F5" // ~s~Franklin didn't follow Dom.
				BREAK
				CASE FAILED_DOM_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DOM_INJURED") ENDIF #ENDIF
					failString = "EXT2_F7" // ~s~Dom was injured.
				BREAK
				CASE FAILED_DOM_ATV_DAMAGED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DOM_ATV_DAMAGED") ENDIF #ENDIF
					failString = "EXT2_F8" // ~s~Dom's ATV was damaged.
				BREAK
				CASE FAILED_GUARD_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_GUARD_INJURED") ENDIF #ENDIF
					failString = "EXT2_F9" // ~s~The security guard was injured.
				BREAK
				CASE FAILED_DIDNT_DRIVE_ATV
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DIDNT_DRIVE_ATV") ENDIF #ENDIF
					failString = "EXT2_F10" // ~s~Franklin didn't ride the ATV onto the plane.
				BREAK
				CASE FAILED_DOM_SCARED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DOM_SCARED") ENDIF #ENDIF
					failString = "EXT2_F11" // ~s~Dom was spooked.
				BREAK
				CASE FAILED_OUTSIDE_ZONE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_OUTSIDE_ZONE") ENDIF #ENDIF
					failString = "EXT2_F12" // ~s~Franklin landed too far outside the target zone.
				BREAK
				CASE FAILED_FAILED_SKY_DIVE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_FAILED_SKY_DIVE") ENDIF #ENDIF
					failString = "EXT2_F13" // ~s~Skydive failed.
				BREAK
				CASE FAILED_DIDNT_ATTEMPT_SKYDIVE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: MISSION_FAILED reason=FAILED_DIDNT_ATTEMPT_SKYDIVE") ENDIF #ENDIF
					failString = "EXT2_F4" // ~s~Franklin didn't attempt the skydive.
				BREAK
			ENDSWITCH
			IF failReason = FAILED_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(failString)
			ENDIF
		BREAK
		CASE EXT2_STATE_MACHINE_LOOP
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				EXT2_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
				EXT2_SET_FRANKLIN_INTO_SKYDIVING_OUTFIT(FALSE)
				IF IS_ENTITY_ALIVE(vehPlaneSkydive) // If this plane is alive the player will either be inside it or in the process of skydiving out
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1036.1206, -2731.8335, 12.7565>>, 332.5734)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-1023.8408, -2728.1885, 12.7005>>, 238.4360)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_gen_sparks)
					STOP_PARTICLE_FX_LOOPED(ptfx_gen_sparks)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_moving_clouds)
					STOP_PARTICLE_FX_LOOPED(ptfx_moving_clouds)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_wind_and_smoke)
					STOP_PARTICLE_FX_LOOPED(ptfx_wind_and_smoke)
				ENDIF
				STOP_SOUND(iSparksSound)
				EXT2_STOP_CARREC_IF_PLAYING(vehPlaneChase)
				EXT2_STOP_CARREC_IF_PLAYING(vehPlaneSkydive)
				EXT2_STOP_CARREC_IF_PLAYING(vehATV[VEHICLE_DOM])
				SAFE_DELETE_OBJECT(objectInvisibleProp)
				SAFE_DELETE_PED(pedDom)
				SAFE_DELETE_PED(pedAirportBooth)
				SAFE_DELETE_VEHICLE(vehATV[VEHICLE_PLAYER])
				SAFE_DELETE_VEHICLE(vehATV[VEHICLE_DOM])
				SAFE_DELETE_VEHICLE(vehPlaneChase)
				SAFE_DELETE_VEHICLE(vehPlaneSkydive)
				INFORM_MISSION_STATS_OF_INCREMENT(EXT2_NUMBER_OF_SPINS, 0, TRUE) // B*1550843 Reset to 0 when failing
				Script_Cleanup()
			ELSE
				EXT2_MANAGE_THREATEN_DOM_DIALOGUE()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF bZSkipping = FALSE
		AND (GET_GAME_TIMER() - iDebugSkipTime) > 1000
		AND ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: S key pressed so passing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				Script_Passed()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: F key pressed so failing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				EXT2_MISSION_FAILED()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: J key pressed so doing z skip forwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateIntro
						IF IS_CUTSCENE_ACTIVE()
						     STOP_CUTSCENE()
						ENDIF
					BREAK
					CASE stateGoIntoAirport
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_DRIVE_OUT_PLANE)
					BREAK
					CASE stateMidtroPlane
						missionStateMachine = EXT2_STATE_MACHINE_CLEANUP
					BREAK
					CASE stateJumpOutPlane
					CASE stateSkydive
					CASE stateGoToDom
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_LANDED_AFTER_SKYDIVE)
					BREAK
					CASE stateFinalConversation
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
					BREAK
				ENDSWITCH
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: P key pressed so doing z skip backwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateGoIntoAirport
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_INTRO)
					BREAK
					CASE stateJumpOutPlane
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_GO_IN_AIRPORT)
					BREAK
					CASE stateSkydive
					CASE stateGoToDom
					CASE stateFinalConversation
						EXT2_DEBUG_SKIP_STATE(Z_SKIP_DRIVE_OUT_PLANE)
					BREAK
				ENDSWITCH
			ENDIF
			INT i_new_state
			IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "EXTREME2: Z skip menu used so doing z skip") ENDIF #ENDIF
				EXT2_DEBUG_SKIP_STATE(i_new_state)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		IF IS_ENTITY_ALIVE(vehPlaneSkydive) // This is all for B*457088
			EXT2_STOP_CARREC_IF_PLAYING(vehPlaneSkydive)
			FREEZE_ENTITY_POSITION(vehPlaneSkydive, TRUE)
			SET_VEHICLE_STAYS_FROZEN_WHEN_CLEANED_UP(vehPlaneSkydive, TRUE)
		ENDIF
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	ADD_CONTACT_TO_PHONEBOOK(CHAR_DOM, FRANKLIN_BOOK, FALSE)
	
	EXT2_MISSION_SETUP()
	
	IF IS_REPLAY_IN_PROGRESS()
		RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE)
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_FOLLOW_DOM
				START_REPLAY_SETUP(vPlayerAfterIntro, fPlayerAfterIntro)
				EXT2_DEBUG_SKIP_STATE(Z_SKIP_GO_IN_AIRPORT)
			BREAK
			CASE CP_SKYDIVE
				START_REPLAY_SETUP(vATVOnPlane[VEHICLE_PLAYER], 0)
				EXT2_DEBUG_SKIP_STATE(Z_SKIP_DRIVE_OUT_PLANE)
			BREAK
			CASE CP_LANDED
				START_REPLAY_SETUP(vPlayerOutro, fPlayerOutro)
				EXT2_DEBUG_SKIP_STATE(Z_SKIP_LANDED_AFTER_SKYDIVE)
			BREAK
			CASE CP_MISSION_PASSED
				START_REPLAY_SETUP(vPlayerOutro, fPlayerOutro)
				EXT2_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
			BREAK
		ENDSWITCH
	ENDIF
	
	WHILE(TRUE)
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		EXT2_DEATH_CHECKS()
		SWITCH missionState
			CASE stateIntro
				MS_INTRO()
			BREAK
			CASE stateGoIntoAirport
				MS_GO_INTO_AIRPORT()
			BREAK
			CASE stateMidtroPlane
				MS_MIDTRO_PLANE()
			BREAK
			CASE stateJumpOutPlane
				MS_JUMP_OUT_PLANE()
			BREAK
			CASE stateSkydive
				MS_SKYDIVE()
			BREAK
			CASE stateGoToDom
				MS_GO_TO_DOM()
			BREAK
			CASE stateFinalConversation
				MS_FINAL_CONVERSATION()
			BREAK
			CASE stateMissionPassed
				Script_Passed()
			BREAK
			CASE stateFailed
				MS_FAILED()
			BREAK
		ENDSWITCH					
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Extreme2")
		
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
ENDSCRIPT
