
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Extreme.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Extreme4.sc
//		AUTHOR			:	Andy Minghella/Tom Kingsley
//		DESCRIPTION		:	Dom jumps off a dam
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// The Random Character - sRCLauncherDataLocal.pedID[0]
g_structRCScriptArgs sRCLauncherDataLocal
VECTOR m_vCharPos = << 1726.40, 83.0424, 169.67 >>
FLOAT m_fCharHeading = 328.7801

// Mission stages
ENUM MISSION_STAGE
	MS_INTRO,
	MS_MEET_DOM,
	MS_MIDTRO,
	MS_DOM_JUMPS,
	MS_MISSION_FAILING
ENDENUM

// Sub-stages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM FAIL_STATE
	FS_SETUP,
	FS_UPDATE,
	FS_CLEANUP
ENDENUM	

ENUM CUTSCENE_STAGE
	eCutInit,
	eCutUpdate,
	eCutCleanup
ENDENUM	

FAIL_STATE eFAIL_STATE

// MISSION PED
STRUCT MISSION_PED
	PED_INDEX mPed
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
	VECTOR vMidtroPos
	VECTOR vMidtroEndPos
	FLOAT fMidtroEndHeading
ENDSTRUCT

// ENUMS
MISSION_STAGE eMissionStage = MS_INTRO
SUB_STAGE eSubStage = SS_SETUP
CUTSCENE_STAGE eCutsceneState = eCutInit

// STAGE SKIPPING
#IF IS_DEBUG_BUILD
	BOOL bFinishedSkipping
	MISSION_STAGE eTargetStage
	CONST_INT MAX_SKIP_MENU_LENGTH 4
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

BOOL bDebugSkipping
BOOL bIntroCutDone
BOOL bWarnedAboutDistance
BOOL bKickedOffSS1

MISSION_PED mDom
CONST_INT DOM_IDLE_WAIT_TIME 8000
CONST_INT NUM_OF_DOM_IDLES 6
STRING sFailReason
INT iAttackConvo

BLIP_INDEX mMissionBlip

SEQUENCE_INDEX seqDomIdle

VEHICLE_INDEX vehPlayer
VEHICLE_INDEX vehDomsSanchez
VEHICLE_INDEX vehBmx

OBJECT_INDEX objDummyBox

OBJECT_INDEX objBox1
OBJECT_INDEX objBox2
OBJECT_INDEX objParachute
OBJECT_INDEX pickupParachute

PTFX_ID ptfxBleed

MODEL_NAMES modelVehPlayer

structPedsForConversation sSpeach  

BOOL bJumpCam

VECTOR vRepos
FLOAT fRepos

CAMERA_INDEX camDomJump
CAMERA_INDEX camDomJump2
CAMERA_INDEX camDomJump3
CAMERA_INDEX camDomJump4
CAMERA_INDEX camDomJump5
CAMERA_INDEX camDomJump6
CAMERA_INDEX camDomJump7
CAMERA_INDEX camDomJump8
CAMERA_INDEX camDomJump9
CAMERA_INDEX camDomJump10
CAMERA_INDEX camDomJump11

CAMERA_INDEX camDomJumpSS1
CAMERA_INDEX camDomJumpSS2
CAMERA_INDEX camDomJumpSS3

SCENARIO_BLOCKING_INDEX sbiDamBlock

//INT iWindSound = GET_SOUND_ID()
INT iDomPants = GET_SOUND_ID()

INT iDomIdleConvo
INT iTimerDomIdleConvo
INT iTimerDomJumped

INT issDomJump1
INT issDomJump2
INT issDomJump3
INT iTimerDomJumpCam 

BOOL bPlayerParaTask
BOOL bDomBreakBonesSound
BOOL bDomScream
BOOL bFrankComm
BOOL bSwitchSS
BOOL bSwitchSS2
BOOL bSkipCS
BOOL bDomBlood
//BOOL bInitSSAudio

BOOL bDoingFocusPush
INT iTimerFocusPush
//BOOL bFakeDomCreated

//VECTOR vDomRot
//VECTOR vDomPos
//FLOAT fDomHeading

#IF IS_DEBUG_BUILD
	INT iNewStage
#ENDIF

// ===========================================================================================================
//		Termination
// ===========================================================================================================

/// PURPOSE:
///    Unloads car recordings, animations, models. marks entities as no longer needed
PROC UnloadEverything()

	//Cleanup the scene created by the launcher (this will delete them)
	RC_CleanupSceneEntities(sRCLauncherDataLocal)
		
	//SAFE_RELEASE_PED(mDom.mPed)
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[0])

	//SET_MODEL_AS_NO_LONGER_NEEDED(mDom.mModel)
	REMOVE_ANIM_DICT("rcmextreme4")

ENDPROC

/// PURPOSE: Cleanup that needs to be done as soon as mission is passed or failed. Blip removal etc
PROC MissionCleanup(BOOL bClearText = TRUE)
	
	if bClearText =TRUE
		CLEAR_PRINTS()
	ENDIF
	
	SAFE_REMOVE_BLIP(mMissionBlip)
	
	SAFE_DELETE_OBJECT(objDummyBox)
		
	if IS_SCRIPTED_CONVERSATION_ONGOING() // stop any conversations
		STOP_SCRIPTED_CONVERSATION(FALSE)
	ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Script_Cleanup(BOOL bClearText = TRUE)
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MissionCleanup(bClearText)
		UnloadEverything()
	ENDIF
	REMOVE_SCENARIO_BLOCKING_AREA(sbiDamBlock)
	RELEASE_SCRIPT_AUDIO_BANK()
	SAFE_RELEASE_OBJECT(objBox1)
	SAFE_RELEASE_OBJECT(objBox2) 
	SAFE_RELEASE_OBJECT(objParachute) 
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN",TRUE)
	RC_CleanupSceneEntities(sRCLauncherDataLocal) //Cleanup the scene created by the launcher
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	
	IF bPlayerParaTask = TRUE	
		#IF IS_DEBUG_BUILD	
			CPRINTLN(DEBUG_MISSION, "STAT **** LEAP OF FAITH ****")
		#ENDIF
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(EXT4_FALL_SURVIVED) 
	ENDIF
	
	MissionCleanup()
	Random_Character_Passed(CP_RAND_C_EXT4)
	Script_Cleanup()

ENDPROC

PROC DO_FOCUS_PUSH()

	FLOAT fHintFov = 35.0
	FLOAT fHintFollow = 0.4//0.35
	FLOAT fHintPitchOrbit = 0.000
	FLOAT fHintSide = -0.85//-0.01
	FLOAT fHintVert = 0//0.05  //-0.050

	IF NOT IS_REPLAY_IN_PROGRESS()
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	
	AND IS_PED_UNINJURED(PLAYER_PED_ID())		
	AND bDoingFocusPush = TRUE	
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
			//IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],6)	
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],<<0,0,0>>,1,-1,0.5)
			//ELSE
			//	TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
			//ENDIF
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
			SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], <<-0.5,0,-0.2>>, TRUE, 30000)
			SET_GAMEPLAY_HINT_FOV(fHintFov)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
			SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
			iTimerFocusPush = GET_GAME_TIMER()
		ELSE
			/*
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],6)	
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) = PERFORMING_TASK
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			*/
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],4)	
				iTimerFocusPush -= 10000
			ENDIF
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
			STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC DO_FOCUS_PUSH_TWO()
/*
	FLOAT fHintFov = 25.0
	FLOAT fHintFollow = 0.4//0.35
	FLOAT fHintPitchOrbit = 0.000
	FLOAT fHintSide = -0.85//-0.01
	FLOAT fHintVert = 0//0.05  //-0.050
*/
	FLOAT fHintFov = 35.0
	FLOAT fHintFollow = 0.45//0.35
	FLOAT fHintPitchOrbit = 0.000
	FLOAT fHintSide = 0//0.07  //0.25 //-0.01
	FLOAT fHintVert = 0.07 //-0.050
	
	IF bDoingFocusPush	
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mDom.mPed,-1)
			/*
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,6)	
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(PLAYER_PED_ID(),mDom.mPed,<<0,-4,0>>,1,-1,0.5)
			ELSE
				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(),mDom.mPed,-1)
			ENDIF
			*/
			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<1658.95, -5.45, 173.78>>,1,-1,0.25,ENAV_DEFAULT,-161.08)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
			SET_GAMEPLAY_ENTITY_HINT(mDom.mPed, <<-0,0,0>>, TRUE, 30000)
			SET_GAMEPLAY_HINT_FOV(fHintFov)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
			SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
			iTimerFocusPush = GET_GAME_TIMER()
		ELSE
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
			STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC DoDomIdleConvo()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		IF iDomIdleConvo < 5
			IF GET_GAME_TIMER()	> iTimerDomIdleConvo + (8000 + GET_RANDOM_INT_IN_RANGE(0,2000))
				IF HAS_SOUND_FINISHED(iDomPants)	
					IF CREATE_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_DOM",CONV_PRIORITY_LOW)	
						iTimerDomIdleConvo = GET_GAME_TIMER()
						++iDomIdleConvo
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_A")
		AND REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_B")
		AND REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_C")
			IF HAS_SOUND_FINISHED(iDomPants)
				PLAY_SOUND_FROM_ENTITY(iDomPants,"DOM",mDom.mPed,"EXTREME_04_SOUNDSET")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Requests models and anims needed for Dom
/// PARAMS:
///    bWaitForLoad - if true, will wait in this proc until everything is loaded
PROC RequestDomAssets(BOOL bWaitForLoad = FALSE)

	REQUEST_MODEL(mDom.mModel)
	REQUEST_ANIM_DICT("rcmextreme4")
	REQUEST_ANIM_DICT("skydive@parachute@")
	REQUEST_MODEL(SANCHEZ)
	REQUEST_PTFX_ASSET()
	
	IF bWaitForLoad = TRUE
		WHILE NOT HAS_MODEL_LOADED(mDom.mModel)
			or NOT HAS_MODEL_LOADED(SANCHEZ)
			or NOT HAS_ANIM_DICT_LOADED("rcmextreme4")
			or NOT HAS_ANIM_DICT_LOADED("skydive@parachute@")
			or NOT HAS_PTFX_ASSET_LOADED()
			WAIT(0)
		ENDWHILE
	ENDIF

ENDPROC

/// PURPOSE: makes dom play idle anims
PROC HandleDomIdles()

	if IS_PED_UNINJURED(mDom.mPed)		
		IF GET_SCRIPT_TASK_STATUS(mDom.mPed,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			TASK_PERFORM_SEQUENCE(mDom.mPed,seqDomIdle)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Creates the RCM mission giver
FUNC BOOL RCM_CREATE_NPC_PED_ON_FOOT(PED_INDEX &ReturnPed, BOOL bCleanupModel = TRUE)

    MODEL_NAMES model = A_C_RETRIEVER
	REQUEST_MODEL(model)
    IF HAS_MODEL_LOADED(model)
        IF DOES_ENTITY_EXIST(ReturnPed)
            DELETE_PED(ReturnPed)
        ENDIF
        ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, m_vCharPos, m_fCharHeading, FALSE, FALSE)
        SET_PED_DEFAULT_COMPONENT_VARIATION(ReturnPed)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ReturnPed, FALSE)
        IF bCleanupModel
            SET_MODEL_AS_NO_LONGER_NEEDED(model)
        ENDIF
        RETURN TRUE
    ENDIF
    RETURN FALSE

ENDFUNC

/// PURPOSE: Sets the new mission stage and initialises the substate + cutscene stage.
PROC SetStage(MISSION_STAGE eNewStage)
	eMissionStage = eNewStage // Setup new mission state
	eSubStage = SS_SETUP
	eCutsceneState = eCutInit
ENDPROC

PROC KILL_SCRIPTED_CUT()

	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,SPC_REENABLE_CONTROL_ON_DEATH)
	SET_TIME_SCALE(1.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

ENDPROC

/// PURPOSE: initialises mission variables
PROC InitVariables()
	
	//INFORM_MISSION_STATS_OF_MISSION_START_EXTREME_4()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "EXT4**** Init variables ****")
	#ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_DOM, FRANKLIN_BOOK, FALSE)

	#IF IS_DEBUG_BUILD // stage skipping
		bFinishedSkipping = TRUE
		mSkipMenu[0].sTxtLabel = "INTRO"    
		mSkipMenu[1].sTxtLabel = "MEET DOM"                
		mSkipMenu[2].sTxtLabel = "MIDTRO"
		mSkipMenu[3].sTxtLabel = "DOM JUMPS" 
	#ENDIF
	
	bWarnedAboutDistance = FALSE
	
	//bFakeDomCreated = FALSE	

	mDom.vStartPos =  <<1658.42, -8.02, 174.78 - 1>>//<<1658.403809,-7.921877,174.558792>>
	mDom.fStartHeading = 116.87//116.945824
	mDom.mModel = GET_NPC_PED_MODEL(CHAR_DOM)
	mDom.vMidtroEndPos = <<1660.323853,-11.712502,173.963791>>  
	mDom.fMidtroEndHeading = 119.600716

	REQUEST_ADDITIONAL_TEXT("EXT4", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	REQUEST_MODEL(prop_box_guncase_03a)
	REQUEST_MODEL(prop_box_wood01a)
	REQUEST_MODEL(P_PARACHUTE_S)
	REQUEST_MODEL(BMX)
	WHILE NOT HAS_MODEL_LOADED(prop_box_guncase_03a)
	OR NOT HAS_MODEL_LOADED(prop_box_wood01a) 
	OR NOT HAS_MODEL_LOADED(P_PARACHUTE_S) 	
	OR NOT HAS_MODEL_LOADED(BMX) 
		WAIT(0)
	ENDWHILE
	
	IF NOT DOES_ENTITY_EXIST(objDummyBox)
		objDummyBox = CREATE_OBJECT_NO_OFFSET(prop_box_guncase_03a, <<1658.298950,-7.930665,173.71>>)  //173.925766
		SET_ENTITY_ROTATION (objDummyBox, <<90.000000,0,-62.999996>>)
		FREEZE_ENTITY_POSITION(objDummyBox,TRUE)
		SET_ENTITY_VISIBLE(objDummyBox,FALSE)
	ENDIF		
			
	RequestDomAssets(TRUE)
	IF NOT DOES_ENTITY_EXIST(mDom.mPed)
		RC_CREATE_NPC_PED(mDom.mPed, CHAR_DOM, mDom.vStartPos, mDom.fStartHeading, "Ext4-Dom") // create peds
		FREEZE_ENTITY_POSITION(mDom.mPed,TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mDom.mPed,TRUE)
		REMOVE_ALL_PED_WEAPONS(mDom.mPed)
		SET_PED_COMPONENT_VARIATION(mDom.mPed,PED_COMP_SPECIAL,2,0,0)
		DISABLE_PED_PAIN_AUDIO(mDom.mPed,TRUE)
		SET_PED_CONFIG_FLAG(mDom.mPed,PCF_RemoveDeadExtraFarAway,TRUE)
		SET_PED_CONFIG_FLAG(mDom.mPed,PCF_RunFromFiresAndExplosions,FALSE)    //To fix B*1958328
		SET_PED_CONFIG_FLAG(mDom.mPed,PCF_DisableExplosionReactions,TRUE)     //To fix B*1958328
		SET_PED_MONEY(mDom.mPed,5000)
	ENDIF
	
	if IS_ENTITY_ALIVE(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF

	bPlayerParaTask = FALSE
	bDomBreakBonesSound = FALSE
	bDomScream = FALSE
	bFrankComm = FALSE
	bSwitchSS = FALSE
	bSwitchSS2 = FALSE
	bSkipCS = FALSE
	bDoingFocusPush = FALSE
	bKickedOffSS1 = FALSE	
	
	SetStage(MS_INTRO)

ENDPROC

/// PURPOSE: Initialise everything
PROC Init()	
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION,"EXT4**** Init mission ****")
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),GADGETTYPE_PARACHUTE)
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),GADGETTYPE_PARACHUTE)
		ENDIF
		CPRINTLN(DEBUG_MISSION,"Debug build so removing parachute (so we can test the parachute pickup is working)")
	#ENDIF

	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
		SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.pedID[0],<<1737.4534, 101.7498, 170.0243>>, 288.5211)
	ELSE
		CREATE_DOG_CONTACT(sRCLauncherDataLocal.pedID[0], <<1737.4534, 101.7498, 170.0243>>, 288.5211)	
	ENDIF
	TASK_FOLLOW_NAV_MESH_TO_COORD(sRCLauncherDataLocal.pedID[0],<<1826.6621, 146.9161, 170.4472>>,1,-1)	
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN",FALSE)
	
	CLEAR_AREA_OF_VEHICLES(mDom.vStartPos, 20.0) // clear cars from the midtro area
		
	IF NOT DOES_ENTITY_EXIST(vehDomsSanchez)
		vehDomsSanchez = CREATE_VEHICLE(SANCHEZ,<< 1662.8799, -14.2550, 172.7742 >>, 205.3516)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehBmx)
		vehBmx = CREATE_VEHICLE(BMX,<<1633.7656, -23.6768, 127.0535>>, 55.7565)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objBox1)
		objBox1 = CREATE_OBJECT (prop_box_wood01a, <<1666.282349,-11.204082,172.777298>>)
		SET_ENTITY_ROTATION (objBox1, <<0.000637,-0.000136,10.660593>>)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objBox2)	
		objBox2 = CREATE_OBJECT (prop_box_wood01a, <<1667.076416,-12.077780,173.261093>>)
		SET_ENTITY_ROTATION (objBox2, <<90.039322,-0.257438,-13.861418>>)
	ENDIF
	
	//REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_A")
	//REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_B")
	//REQUEST_SCRIPT_AUDIO_BANK("EXTREME_04_DOM_C")
	
	//IF NOT DOES_PICKUP_EXIST(pickupParachute)
	//	pickupParachute = CREATE_AMBIENT_PICKUP(PICKUP_PARACHUTE,<<1666.29,-11.20,173.270350>>)
	//ENDIF
	//IF NOT DOES_ENTITY_EXIST(pickupParachute)
	//ENDIF
	/*
	IF NOT DOES_ENTITY_EXIST(objParachute)	
		objParachute = CREATE_OBJECT (P_Parachute_S, <<1666.29,-11.20,173.270350>>) //<<1665.941895,-11.189898,173.240350>> //173.640350
		SET_ENTITY_ROTATION (objParachute, <<-111.822540,-0.454914,-162.223083>>)
	ENDIF
	*/
	if IS_PED_UNINJURED(mDom.mPed)	
		IF GET_SCRIPT_TASK_STATUS(mDom.mPed,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK        //seq of idle anims
			OPEN_SEQUENCE_TASK(seqDomIdle)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "fidget_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "rcmextreme4", "fidget_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				SET_SEQUENCE_TO_REPEAT(seqDomIdle,REPEAT_FOREVER)	
			CLOSE_SEQUENCE_TASK(seqDomIdle)
			TASK_PERFORM_SEQUENCE(mDom.mPed,seqDomIdle)
		ENDIF
	ENDIF
	
	if IS_PED_UNINJURED(mDom.mPed)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mDom.mPed, FALSE)
		ADD_PED_FOR_DIALOGUE(sSpeach, 4, mDom.mPed, "DOM")
		SET_PED_RELATIONSHIP_GROUP_HASH(mDom.mPed,GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
	ENDIF
	
	//pickupParachute = CREATE_AMBIENT_PICKUP(PICKUP_PARACHUTE,<<1665.3146, -11.4569, 173.1747>>,6,-1,P_Parachute_S,TRUE) //172.7747
	//pickupParachute = CREATE_AMBIENT_PICKUP(PICKUP_PARACHUTE,<<1666.29,-11.20,173.250350>>,7,-1,P_Parachute_S,TRUE) //173.270350//172.7747

	IF NOT DOES_ENTITY_EXIST(pickupParachute)
		pickupParachute = CREATE_AMBIENT_PICKUP(PICKUP_PARACHUTE,<<1665.2640, -11.4427, 172.7747>>,7,-1,P_Parachute_S,TRUE) //173.270350//172.7747
	ENDIF
	
	SET_ENTITY_ROTATION (pickupParachute, <<-111.822540,-0.454914,-162.223083>>)
	PLAY_ENTITY_ANIM(pickupParachute,"Chute_Off_Bag","skydive@parachute@",4,FALSE,TRUE,FALSE,0.99)

	//ACTIVATE_PHYSICS(pickupParachute)
	/*	
		PLACEMENT_FLAG_MAP = 0,					
	PLACEMENT_FLAG_FIXED = 1,				
	PLACEMENT_FLAG_REGENERATES = 2,			
	PLACEMENT_FLAG_SNAP_TO_GROUND = 3,
	PLACEMENT_FLAG_ORIENT_TO_GROUND = 4,
	PLACEMENT_FLAG_LOCAL_ONLY = 5,
	PLACEMENT_FLAG_BLIPPED_SIMPLE = 6,
	PLACEMENT_FLAG_BLIPPED_COMPLEX = 7,
	PLACEMENT_FLAG_UPRIGHT = 8
	*/
	SetStage(MS_MEET_DOM)		
			
ENDPROC

/// PURPOSE: Check fail conditions
PROC CheckForFail()

	FLOAT fDistance	
		
	IF eMissionStage = MS_MEET_DOM
		IF NOT IS_PED_UNINJURED(mDom.mPed)
			sFailReason = "EXT4_F1"             //Dom died
			eMissionStage = MS_MISSION_FAILING
		ELSE	
			fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mDom.mPed))
			if bWarnedAboutDistance = FALSE
				if fDistance > 160.0  //180
					//PRINT_NOW("EXT4_02", DEFAULT_GOD_TEXT_TIME, 1) // warn player about leaving the area
					bWarnedAboutDistance = TRUE
				ENDIF
			ELSE
				if fDistance > 200.0 // player has been warned, check for fail
					sFailReason = "EXT4_F2"                                       //You left the area
					eMissionStage = MS_MISSION_FAILING
				ENDIF
			ENDIF
			if fDistance > 150.0  //180
				IF DOES_BLIP_EXIST(mMissionBlip)
					SET_BLIP_FLASHES(mMissionBlip,TRUE)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(mMissionBlip)
					SET_BLIP_FLASHES(mMissionBlip,FALSE)
				ENDIF
			ENDIF
		ENDIF
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<1660.332031,-20.456074,136.702896>>, <<1584.418945,-30.291168,120.603775>>, 32.000000)
			sFailReason = "EXT4_F2"                                       //You left the area
			eMissionStage = MS_MISSION_FAILING
		ENDIF
	ENDIF

ENDPROC

PROC FailWait()
	
	SWITCH eFAIL_STATE
	
		CASE FS_SETUP
			SAFE_REMOVE_BLIP(mMissionBlip)
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			IF IS_STRING_NULL_OR_EMPTY(sFailReason)
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
		
			eFAIL_STATE = FS_UPDATE	
		BREAK
		
		CASE FS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 1759.5635, 111.1878, 170.3075 >>, 281.1612)
			  	//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 1766.8259, 109.9883, 170.0763 >>, 291.1678)

				// delete everything
				MissionCleanup()

				Script_Cleanup(FALSE)
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE: Resets mission
	PROC ResetMission()
		
		DESTROY_CAM(camDomJumpSS1)
		DESTROY_CAM(camDomJumpSS2)
		DESTROY_CAM(camDomJumpSS3)
		
		//issDomJump1 = -1
		//issDomJump2 = -1
		//issDomJump3 = -1
		
		CLEAR_PRINTS()
		UnloadEverything()
		
		bIntroCutDone = FALSE
		bJumpCam = FALSE
		//bFakeDomCreated = FALSE	
		
		SAFE_DELETE_PED(mDom.mPed)

		SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
		
		SAFE_DELETE_OBJECT(objDummyBox)	
		SAFE_DELETE_OBJECT(objBox1)	
		SAFE_DELETE_OBJECT(objBox2)	
		SAFE_DELETE_OBJECT(objParachute)	
			
		WHILE NOT RCM_CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[0])
			WAIT(0)
		ENDWHILE
		
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << 1732.2219, 92.0087, 169.7867 >>, 138.1854, FALSE)
	
		InitVariables() // initialise everything
		SetStage(MS_INTRO)
	
	ENDPROC

	/// PURPOSE: Skips the current stage
	PROC SkipStage()

		SWITCH eMissionStage
		
			CASE MS_INTRO

			BREAK
			
			CASE MS_MEET_DOM
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << 1659.2189, -4.5402, 172.7742 >>, 182.5585, FALSE)
			BREAK
			
			CASE MS_MIDTRO

			BREAK
			
			CASE MS_DOM_JUMPS
				SAFE_TELEPORT_ENTITY(mDom.mPed, <<1658.713257,-8.521038,174.766663>>, 119.056664 , FALSE)
				SETTIMERA(40000)
			BREAK
		
		ENDSWITCH	
	
	ENDPROC

	/// PURPOSE: Jumps to the stage selected
	PROC JumpToStage(MISSION_STAGE eNewStage)
		
		if eMissionStage = eNewStage  // skip current stage
			bFinishedSkipping = TRUE
		ELSE
			SkipStage() 
		ENDIF
	
	ENDPROC

	/// PURPOSE: Goes back to the previous stage
	PROC PreviousStage()

		iNewStage = ENUM_TO_INT(eMissionStage)-1
		if iNewStage >-1  //we can skip to a previous stage
			eTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
			ResetMission()
			bFinishedSkipping = FALSE
			JumpToStage(eTargetStage)
		ENDIF
	
	ENDPROC
#ENDIF

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) // Check for Pass
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_SCRIPTED_CUT()
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)) // Check for Fail
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_SCRIPTED_CUT()
			CLEAR_PRINTS()
			eMissionStage = MS_MISSION_FAILING
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) // Check for Skip forward
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_SCRIPTED_CUT()
			SkipStage()
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) // Check for Skip backwards
			IF eMissionStage = MS_INTRO
				WAIT_FOR_CUTSCENE_TO_STOP()
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				init()
				RC_END_Z_SKIP()
				SetStage(MS_INTRO)
			ELIF eMissionStage = MS_MEET_DOM
				WAIT_FOR_CUTSCENE_TO_STOP()
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				init()
				RC_END_Z_SKIP()
				SetStage(MS_INTRO)
			ELIF eMissionStage = MS_MIDTRO
				WAIT_FOR_CUTSCENE_TO_STOP()	
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				Init()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << 1732.2219, 92.0087, 169.7867 >>, 138.1854, FALSE)
				RC_END_Z_SKIP()
				SetStage(MS_MEET_DOM)
			ELIF eMissionStage = MS_DOM_JUMPS
				WAIT_FOR_CUTSCENE_TO_STOP()
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				Init()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< 1659.2189, -4.5402, 172.7742 >>, 182.5585,FALSE)
				RC_END_Z_SKIP()
				SetStage(MS_MIDTRO)
			ENDIF
		ENDIF
		
		if LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage) // Check for jumping to stage
			bDebugSkipping = TRUE
			IF iNewStage = 0
				WAIT_FOR_CUTSCENE_TO_STOP()
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				init()
				RC_END_Z_SKIP()
				SetStage(MS_INTRO)
			ELIF iNewStage = 1	
				WAIT_FOR_CUTSCENE_TO_STOP()	
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				Init()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << 1732.2219, 92.0087, 169.7867 >>, 138.1854, FALSE)
				RC_END_Z_SKIP()
				SetStage(MS_MEET_DOM)
			ELIF iNewStage = 2
				WAIT_FOR_CUTSCENE_TO_STOP()
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				Init()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< 1659.2189, -4.5402, 172.7742 >>, 182.5585,FALSE)
				RC_END_Z_SKIP()
				SetStage(MS_MIDTRO)
			ELIF iNewStage = 3	
				WAIT_FOR_CUTSCENE_TO_STOP()	
				KILL_SCRIPTED_CUT()
				RC_START_Z_SKIP()
				ResetMission()
				InitVariables()
				Init()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< 1662.3895, -9.8186, 172.7746 >>, 124.6761,FALSE)
				RC_END_Z_SKIP()
				SetStage(MS_DOM_JUMPS)
			ENDIF
		
		ENDIF
	
	ENDPROC

#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

// --------------------------------------------------------------
//     STAGE FUNCTIONS
// --------------------------------------------------------------

/// PURPOSE: Handle intro mocap
PROC IntroMocap()
	
	SWITCH eCutsceneState
		
		CASE eCutInit
			
			DO_FOCUS_PUSH()
			
			bDebugSkipping = FALSE
			
			RC_REQUEST_CUTSCENE("es_4_rcm_p1")
			
			IF RC_IS_CUTSCENE_OK_TO_START()	

				IF NOT bDoingFocusPush
				OR GET_GAME_TIMER()	> iTimerFocusPush + 3500
					RC_CLEANUP_LAUNCHER()
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
					START_CUTSCENE()
					if IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])	
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[0],"Hudson",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,a_c_retriever)		
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "EXT4**** Registered dog for cutscene ****")
						#ENDIF
					ENDIF
					WAIT(0)
					STOP_GAMEPLAY_HINT()
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1729.921631,89.416199,174.012268>>, <<1739.745361,101.281578,167.828156>>, 7.000000,<< 1739.9564, 97.8651, 169.9797 >>, 140.8760)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1743.115479,105.832962,169.636169>>, <<1732.028809,93.771118,172.607452>>, 4.000000,<< 1739.9564, 97.8651, 169.9797 >>, 140.8760)
					RC_START_CUTSCENE_MODE(<< 1733.3149, 95.2246, 169.9192 >>,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,FALSE)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "EXT4**** Cutscene started ****")
					#ENDIF
					eCutsceneState = eCutUpdate
				ENDIF
			ENDIF
	
		BREAK	
	
		CASE eCutUpdate
			
			IF IS_CUTSCENE_PLAYING()
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
						STOP_CUTSCENE()
					ENDIF
					//DEBUG_Check_Debug_Keys()
				#ENDIF
			ELSE
				
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				
				eCutsceneState = eCutCleanup
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
				//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-63.1068, 294.9655, 105.0442>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				
				eCutsceneState = eCutCleanup
			ENDIF
			
		BREAK
		
		CASE eCutCleanup

			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
			//	SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
			//ENDIF
			
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << 1732.2219, 92.0087, 169.7867 >>, 138.1854, FALSE)
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
			
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
			TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<1723.4518, 80.7290, 169.6701>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7.5576)
			//SET_GAMEPLAY_CAM_RELATIVE_HEADING(-153.9845)
			/*
			if IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
			ENDIF
			*/
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** Cutscene finished ****")
			#ENDIF
			
			REPLAY_STOP_EVENT()
			
			bIntroCutDone = TRUE
			
			RC_END_CUTSCENE_MODE()
			
		BREAK
	
	ENDSWITCH
																												
ENDPROC	

FUNC BOOL IS_MIDTRO_OK_TO_START(BOOL check_for_cutscene_loaded = TRUE, FLOAT stopping_distance = DEFAULT_VEH_STOPPING_DISTANCE, BOOL b_player_exit_vehicle = FALSE)

	// See B*649704 - we need to do all this stuff rather than return false as soon as a condition returns false
	BOOL b_cutscene_ok_to_start = TRUE 

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) AND NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID()) AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			IF NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), stopping_distance, 1)
				b_cutscene_ok_to_start = FALSE
			ELSE // Vehicle has stopped
				IF b_player_exit_vehicle = TRUE
					b_cutscene_ok_to_start = FALSE
					IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				b_cutscene_ok_to_start = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Disable attack controls
	RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()

	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		//KILL_ANY_CONVERSATION()
		b_cutscene_ok_to_start = FALSE
	ENDIF

	IF b_cutscene_ok_to_start = FALSE	
		RETURN FALSE
	ENDIF
	
	// B*1521696 - swap over from HAS_CUTSCENE_LOADED
	// HAS_CUTSCENE_LOADED_WITH_FAILSAFE()- Ensure you are only calling this check when your script is ready to play the cutscene
	IF check_for_cutscene_loaded = TRUE
		IF NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Double check to make sure any conversations are killed when the function is returning TRUE
	//IF IS_SCRIPTED_CONVERSATION_ONGOING()
	//	STOP_SCRIPTED_CONVERSATION(FALSE)
	//ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Handle midtro mocap
PROC MidtroMocap()

	IF eSubStage < SS_CLEANUP
		PREPARE_MUSIC_EVENT("EXT4_JUMPED_OS")
	ENDIF

	SWITCH eSubStage
		CASE SS_SETUP

			bDebugSkipping = FALSE	
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1655.139282,5.342447,172.674774>>, <<1662.085327,-6.198325,175.024765>>, 8.250000)
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("es_4_rcm_p2_concat", CS_SECTION_2 | CS_SECTION_3)	
			ELSE
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("es_4_rcm_p2_concat", CS_SECTION_1 | CS_SECTION_3)	  //Player triggering cutscene a weird way
			ENDIF	
				
			STOP_SOUND(iDomPants)
			
			//KILL_ANY_CONVERSATION()	
				
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()		
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Dom",PED_COMP_SPECIAL,2,0)
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY = TRUE")
				#ENDIF
			ENDIF
				
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF IS_MIDTRO_OK_TO_START(TRUE,20,TRUE)	
					IF NOT bDoingFocusPush
					OR GET_GAME_TIMER()	> iTimerFocusPush + 3000	
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<1658.95, -5.45, 173.78>>,<<0.2,0.2,0.2>>)	
						//SET_CUTSCENE_PED_COMPONENT_VARIATION("Dom",PED_COMP_SPECIAL,2,0)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						if IS_PED_UNINJURED(mDom.mPed)	
							REGISTER_ENTITY_FOR_CUTSCENE(mDom.mPed,"Dom",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,CS_DOM)		
							//SET_CUTSCENE_PED_COMPONENT_VARIATION("Dom",PED_COMP_SPECIAL,2,0)
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "EXT4**** Dom registered for cutscene ****")
							#ENDIF
						ENDIF
						WAIT(0)
						bDoingFocusPush = FALSE
						STOP_GAMEPLAY_HINT()
						START_AUDIO_SCENE("EXTREME_04_CUSTOM_QUIET_SCENE")
						if IS_PED_UNINJURED(mDom.mPed)	
							FREEZE_ENTITY_POSITION(mDom.mPed,FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(pickupParachute) 
						AND NOT IS_ENTITY_DEAD(pickupParachute)
							SET_ENTITY_VISIBLE(pickupParachute,FALSE)
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
							vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							modelVehPlayer = GET_ENTITY_MODEL(vehPlayer)
							IF IS_THIS_MODEL_A_CAR(modelVehPlayer) = TRUE
							OR IS_THIS_MODEL_A_BIKE(modelVehPlayer) = TRUE
								vRepos = <<1649.7930, 11.7826, 172.7745>>
								fRepos = 220.9923
							ELSE
								vRepos = << 1662.0160, 53.0925, 171.2482 >>
								fRepos = 298.6364
							ENDIF
							//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1654.721069,4.166876,172.524506>>, <<1662.561646,-8.920052,178.274155>>, 6.250000,vRepos, fRepos)
						ELSE
							vRepos = <<1649.7930, 11.7826, 172.7745>>
							fRepos = 220.9923
							REPOSITION_PLAYERS_VEHICLE(vRepos, fRepos)	
						ENDIF
						
						RC_START_CUTSCENE_MODE(<< 1660.1857, -7.4920, 172.7740 >>,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,FALSE)
						//REPOSITION_PLAYERS_VEHICLE(vRepos, fRepos)	
						//CLEAR_AREA_OF_VEHICLES(<< 1660.1857, -7.4920, 172.7740 >>,200,TRUE)
						CLEAR_AREA_OF_PEDS(<< 1660.1857, -7.4920, 172.7740 >>,200)
						sbiDamBlock = ADD_SCENARIO_BLOCKING_AREA(<<1636.6907, -89.8127, 150.0>>,<<1706.1459, 58.7210, 190.0>>,FALSE,TRUE)
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "EXT4**** Cutscene started ****")
						#ENDIF
						
						eSubStage = SS_UPDATE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		CASE SS_UPDATE
			
			IF IS_CUTSCENE_PLAYING()
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
						STOP_CUTSCENE()
					ENDIF
					//DEBUG_Check_Debug_Keys()
				#ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()	
					IF NOT bKickedOffSS1		
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
							IF IS_PED_UNINJURED(mDom.mPed)			
								IF NOT DOES_CAM_EXIST(camDomJumpSS1)
									camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
								ENDIF
								SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								issDomJump1 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
								//camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
								TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump1,"rcmextreme4","Base_Jump_Spot",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump1,TRUE)
								SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS1,issDomJump1,"Base_Jump_Spot_CAM","rcmextreme4")
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
								bKickedOffSS1 = TRUE	 
							ENDIF
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
				ENDIF
				
				IF IS_PED_UNINJURED(mDom.mPed)		
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dom")
						/*
						IF NOT bKickedOffSS1	
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
								IF NOT DOES_CAM_EXIST(camDomJumpSS1)
									camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
								ENDIF
								SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								issDomJump1 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
								//camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
								TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump1,"rcmextreme4","Base_Jump_Spot",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump1,FALSE)
								SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
								PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS1,issDomJump1,"Base_Jump_Spot_CAM","rcmextreme4")
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
								bKickedOffSS1 = TRUE
							ENDIF
						ENDIF
						*/
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
				
				//SET_CAM_PARAMS(camDomJump,<<1652.527954,-5.777370,167.722397>>,<<51.350212,1.771070,-126.907448>>,26.295422)
				
				//SHAKE_CAM(camDomJump,"HAND_SHAKE",1)
				
			ELSE
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_STOP_EVENT()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** Cutscene finished ****")
			#ENDIF
			
			IF DOES_ENTITY_EXIST(pickupParachute) 
			AND NOT IS_ENTITY_DEAD(pickupParachute)
				SET_ENTITY_VISIBLE(pickupParachute,TRUE)
			ENDIF
			
			IF bDebugSkipping = FALSE
				/*
				IF NOT bKickedOffSS1
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
						issDomJump1 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
						camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
						TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump1,"rcmextreme4","Base_Jump_Spot",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump1,TRUE)
						SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS1,issDomJump1,"Base_Jump_Spot_CAM","rcmextreme4")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
					ENDIF
				ENDIF
				*/
				TRIGGER_MUSIC_EVENT("EXT4_JUMPED_OS")
				/*
				if IS_PED_UNINJURED(mDom.mPed)		
					IF NOT IS_ENTITY_PLAYING_ANIM(mDom.mPed,"rcmextreme4", "base_jump_spot")
						SET_ENTITY_COLLISION(mDom.mPed,TRUE)
						TASK_PLAY_ANIM(mDom.mPed, "rcmextreme4", "base_jump_spot",SLOW_BLEND_IN,-1,-1,AF_NOT_INTERRUPTABLE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
					ENDIF	
				ENDIF
				*/
				SetStage(MS_DOM_JUMPS)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE: Wait for the player to reach Dom's location
PROC MeetDom()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_LR)	
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UD)	
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ENTER)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_SPRINT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_JUMP)
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_LR))
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_UD))	
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UP_ONLY))	
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eSubStage
		CASE SS_SETUP
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** Init meet Dom ****")
			#ENDIF
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
				SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[0],CA_ALWAYS_FLEE,TRUE)
			ENDIF
			iDomIdleConvo = 0
			bDoingFocusPush = TRUE
			PRINT_NOW("EXT4_01", DEFAULT_GOD_TEXT_TIME, 1)
			mMissionBlip = CREATE_PED_BLIP(mDom.mPed,TRUE,TRUE)
			SETTIMERA(0)
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			
			//FREEZE_ENTITY_POSITION(mDom.mPed,TRUE)
			
			IF DOES_ENTITY_EXIST(objParachute)	
				IF NOT IS_ENTITY_DEAD(objParachute)
					//IF NOT DOES_ENTITY_HAVE_PHYSICS(objParachute)
					//	ACTIVATE_PHYSICS(objParachute)
					//ENDIF
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),objParachute,1.0)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1,FALSE,FALSE)
						DELETE_OBJECT(objParachute)
					ENDIF
				ENDIF
			ENDIF
			
			if IS_PED_UNINJURED(PLAYER_PED_ID()) // check for player meeting up with Dom
				if IS_PED_UNINJURED(mDom.mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,DEFAULT_CUTSCENE_LOAD_DIST)
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("es_4_rcm_p2_concat", CS_SECTION_2 | CS_SECTION_3)
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()		
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Dom",PED_COMP_SPECIAL,2,0)
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY = TRUE")
							#ENDIF
						ENDIF
					ENDIF
					IF IS_PED_RAGDOLL(mDom.mPed)
						SET_ENTITY_HEALTH(mDom.mPed,0)
					ENDIF
					//IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,70.0)	
					//	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN",FALSE)
					//ENDIF
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,32.0)	
						IF IS_PLAYER_SHOOTING_NEAR_PED(mDom.mPed)
							IF iAttackConvo = 0
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	  //What are you doing man?
									PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_ATTACK", "EXT4_ATTACK_1", CONV_PRIORITY_MEDIUM)
									iAttackConvo = 1
								ENDIF
							ELIF iAttackConvo = 1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	 //Seriously now, stop that please.
									PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_ATTACK", "EXT4_ATTACK_2", CONV_PRIORITY_MEDIUM)
									iAttackConvo = 2
								ENDIF
							ENDIF
						ELSE
							IF NOT bFrankComm	
								IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,30.0)
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_COMM", CONV_PRIORITY_MEDIUM)
										bFrankComm = TRUE
									ENDIF
								ENDIF
							ENDIF
							DoDomIdleConvo()
							IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(),mDom.mPed)
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mDom.mPed,-1) 
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1663.587280,-11.714151,172.674759>>, <<1658.386841,-1.735749,175.774765>>, 5.500000)	  //<<1657.426636,-2.182298,172.774017>>, <<1663.951416,-14.652416,175.774216>>, 4.200000
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
							eSubStage = SS_CLEANUP // player has met Dom (and is on foot)
						ELSE
							vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							modelVehPlayer = GET_ENTITY_MODEL(vehPlayer)
							IF IS_THIS_MODEL_A_CAR(modelVehPlayer) = TRUE
							OR IS_THIS_MODEL_A_BIKE(modelVehPlayer) = TRUE
								eSubStage = SS_CLEANUP // player has met Dom (and is in a car or bike)
							ENDIF
						ENDIF
					ELSE	
						HandleDomIdles() // handle dom's idle anims
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
			SAFE_REMOVE_BLIP(mMissionBlip)
			
			if IS_PED_UNINJURED(PLAYER_PED_ID()) // if player in car, stop him
				if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 5.0, 1)
					wait(600) //DEFAULT_CAR_STOPPING_TO_CUTSCENE
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			ENDIF

			if IS_SCRIPTED_CONVERSATION_ONGOING()
				STOP_SCRIPTED_CONVERSATION(FALSE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** Cleanup meet Dom ****")
			#ENDIF

			SetStage(MS_MIDTRO) 
		
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE: Scripted cameras for dom jumping off the dam
PROC DomJumpCam()

	INT iCamSwitch = 0
	FLOAT fTimeScale
	FLOAT fExpectedFrameSpeed
	FLOAT fTimeScalar
	
	fTimeScale = 1.0
	
	iTimerDomJumpCam = GET_GAME_TIMER()

	IF NOT DOES_CAM_EXIST(camDomJump)
		//camDomJump = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump2)
		//camDomJump2 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump3)
		//camDomJump3 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump4)
		//camDomJump4 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump5)
		//camDomJump5 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump6)
		//camDomJump6 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump7)
		//camDomJump7 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump8)
		//camDomJump8 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump9)
		//camDomJump9 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump10)
		//camDomJump10 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
	IF NOT DOES_CAM_EXIST(camDomJump11)
		//camDomJump11 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
	ENDIF
/*
	//first shot 			
	SET_CAM_PARAMS(camDomJump,<<1652.527954,-5.777370,167.722397>>,<<51.350212,1.771070,-126.907448>>,26.295422)
	
	//first shot (interp from camDomJump - lin)
	SET_CAM_PARAMS(camDomJump2,<<1652.527954,-5.777370,167.722397>>,<<51.882275,1.771069,-128.265274>>,26.295422)
	
	//second (cut)
	SET_CAM_PARAMS(camDomJump3,<<1657.819702,-8.218518,175.766830>>,<<-43.096615,1.730442,116.986420>>,60.016502)
	
	//Second (interp from camDomJump3 - lin)
	SET_CAM_PARAMS(camDomJump4,<<1657.002319,-8.632171,175.197128>>,<<-61.205029,1.730429,117.738411>>,60.016502)
	
	//third (Cut)
	SET_CAM_PARAMS(camDomJump5,<<1657.367676,-18.865385,168.643616>>,<<15.442405,1.690912,13.221531>>,19.589161)
	
	//Third (interp from camDomJump5)
	SET_CAM_PARAMS(camDomJump6,<<1656.873169,-17.804836,160.604385>>,<<-1.196615,1.690920,10.088464>>,19.589161)

	//cut on warp - dom falls at the camera
	SET_CAM_PARAMS(camDomJump7,<<1656.883301,-17.515141,134.562317>>,<<80.652748,1.690934,-30.455717>>,50.009060)

	//Interp to here from fall cam
	SET_CAM_PARAMS(camDomJump8,<<1656.883301,-17.515141,134.562317>>,<<76.504456,1.690932,-64.226204>>,50.009060)

	//splat shot 1 decal
	SET_CAM_PARAMS(camDomJump9,<<1655.864014,-16.665905,150.794205>>,<<-79.345596,-0.000001,145.071335>>,21.731354)

	//splat shot 2 5second decal
	SET_CAM_PARAMS(camDomJump10,<<1656.021484,-16.440193,152.257721>>,<<-79.345581,-0.000001,143.763870>>,21.731354)

	//catch up cam - over sholder
	SET_CAM_PARAMS(camDomJump11,<<1659.7664, -7.0467, 175.0521>>, <<-36.0925, 0.0000, 120.3561>>,50.000000)
*/

//	SHAKE_CAM(camDomJump,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump2,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump3,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump4,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump5,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump6,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump7,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump8,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump9,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump10,"HAND_SHAKE",0.5)
//	SHAKE_CAM(camDomJump11,"HAND_SHAKE",0.5)

	SET_TIME_SCALE(fTimeScale)
	//SET_CAM_ACTIVE(camDomJump,TRUE) //true on camera
	//RENDER_SCRIPT_CAMS(TRUE, FALSE)
	DISPLAY_HUD(FALSE)
	DISPLAY_RADAR(FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH)
	
	//PLAY_SOUND_FRONTEND(iWindSound,"WIND","EXTREME_04_SOUNDSET")
	
	STOP_AUDIO_SCENE("EXTREME_04_CUSTOM_QUIET_SCENE")
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "EXT4**** Starting scripted cutscene ****")
	#ENDIF
	
	bDomBreakBonesSound = FALSE
	bDomScream = FALSE
	bSwitchSS = FALSE
	bSwitchSS2 = FALSE
	bSkipCS = FALSE
	bDomScream = FALSE
	bDomBlood = FALSE
	//bInitSSAudio = FALSE
//	FLOAT fZheight
	ACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_EXTREME_04")
	REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOWEST) //Dom jumps off the dam
	/*
	IF NOT bKickedOffSS1
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
			issDomJump1 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
			camDomJumpSS1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
			TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump1,"rcmextreme4","Base_Jump_Spot",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump1,TRUE)
			SET_CAM_ACTIVE(camDomJumpSS1,TRUE)
			PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS1,issDomJump1,"Base_Jump_Spot_CAM","rcmextreme4")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
	ENDIF
	*/
	WHILE GET_GAME_TIMER() < iTimerDomJumpCam + 11000	
	AND NOT IS_SCREEN_FADED_OUT()	
		
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
		AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump2)
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(400)	
				bSkipCS = TRUE
			ENDIF
		ENDIF
		
		IF bSkipCS
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ENDIF

		WAIT(0)
		#IF IS_DEBUG_BUILD
			//CPRINTLN(DEBUG_MISSION, "EXT4**** shot time ****  = ", GET_GAME_TIMER() - iTimerDomJumpCam)
		#ENDIF
		
		IF NOT bSwitchSS	
			//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
			//OR GET_SYNCHRONIZED_SCENE_PHASE(issDomJump1) = 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
				IF GET_SYNCHRONIZED_SCENE_PHASE(issDomJump1) = 1	
					CPRINTLN(DEBUG_MISSION, "issDomJump1 PHASE 1")
					//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump2)	
					IF IS_PED_UNINJURED(mDom.mPed)		
						CPRINTLN(DEBUG_MISSION, "issDomJump2 NOT RUNNING")
						issDomJump2 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
						camDomJumpSS2 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
						TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump2,"rcmextreme4","Base_Jump_Spot_02",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)  //SYNCED_SCENE_USE_PHYSICS
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump2,FALSE)
						SET_CAM_ACTIVE(camDomJumpSS2,TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS2,issDomJump2,"Base_Jump_Spot_02_CAM","rcmextreme4")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
						DESTROY_CAM(camDomJumpSS1)
						
						bSwitchSS = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bSwitchSS2	
				//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump2)
				//OR GET_SYNCHRONIZED_SCENE_PHASE(issDomJump2) = 1
				//IF NOT bInitSSAudio	
					//INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("RCM_Extreme4_Fall_and_impact",mDom.mPed)
				//	bInitSSAudio = TRUE
				//ELSE	
				IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump2)
				AND GET_SYNCHRONIZED_SCENE_PHASE(issDomJump2) >= 0.2	
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)	
						PREPARE_SYNCHRONIZED_AUDIO_EVENT("RCM_Extreme4_Fall_and_impact",0)
					ENDIF
				ENDIF
				//ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump2)
				AND GET_SYNCHRONIZED_SCENE_PHASE(issDomJump2) = 1
					//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)	
					IF IS_PED_UNINJURED(mDom.mPed)			
						issDomJump3 = CREATE_SYNCHRONIZED_SCENE(<<1656.378, -12.057, 169.038>>,<< 0.000, 0.000, 115.850 >>)
						camDomJumpSS3 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA",TRUE)
						TASK_SYNCHRONIZED_SCENE(mDom.mPed,issDomJump3,"rcmextreme4","Fall_and_impact",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)  //SYNCED_SCENE_USE_PHYSICS
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(issDomJump3,TRUE)
						SET_CAM_ACTIVE(camDomJumpSS3,TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(camDomJumpSS3,issDomJump3,"Fall_and_impact_CAM","rcmextreme4")
						PLAY_SYNCHRONIZED_AUDIO_EVENT(issDomJump3)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
						DESTROY_CAM(camDomJumpSS2)
						bSwitchSS2 = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)
				AND GET_SYNCHRONIZED_SCENE_PHASE(issDomJump3) >= 0.323//0.578
				//IF IS_ENTITY_IN_ANGLED_AREA(mDom.mPed,<<1655.011841,-19.218821,132.747116>>, <<1655.736694,-22.015562,133.804886>>, 2.500000)
					IF NOT bDomBreakBonesSound		
						
						//<< 1655.4, -20.5861, 134.311 >> head hits water
						//<< 1655.27, -20.8134, 133.631 >> head end

						//START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_extrm4_water_splash", mDom.mPed,<<0, 0, 0>>, <<0, 0, 0>>, BONETAG_NECK,2)
						//VECTOR vDomNeck = GET_WORLD_POSITION_OF_ENTITY_BONE(mDom.mPed,GET_PED_BONE_INDEX(mDom.mPed, BONETAG_HEAD))
						//PRINTVECTOR(vDomNeck)
						//PRINTNL()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_extrm4_water_splash", << 1655.27, -20.8134, 133.841 >>, <<0,0,0>>, 1)
						SET_PARTICLE_FX_NON_LOOPED_ALPHA(1)
						//IF ptfxBleed = NULL
						//ENDIF
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//PLAY_SOUND_FROM_COORD(-1,"DOM_LANDS",<<1653.1371, -20.1164, 133.2946>>,"EXTREME_04_SOUNDSET")
						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "------- SPLAT! -------")
						#ENDIF
						
						//APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_HEAD,0.810, 0.733, BDT_STAB)
						//APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_TORSO,0.940, 0.590, BDT_SHOTGUN_SMALL)
						//APPLY_PED_BLOOD(mDom.mPed,0,<< 1655.27, -20.8134, 133.835 >>,"Scripted_Ped_Splash_Back")
						bDomBreakBonesSound = TRUE
					ELSE
						IF NOT bDomBlood
							IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)
							AND GET_SYNCHRONIZED_SCENE_PHASE(issDomJump3) >= 0.44	
								ptfxBleed = START_PARTICLE_FX_LOOPED_AT_COORD("scr_extrm4_water_blood", << 1655.27, -20.8134, 133.842 >>, <<0,0,0>>,1)
								SET_PARTICLE_FX_LOOPED_ALPHA(ptfxBleed,1)
								bDomBlood = TRUE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
			
		//IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump1)
		//AND GET_SYNCHRONIZED_SCENE_PHASE(issDomJump1) >= 0.2//0.584	
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 600	
		AND fTimeScale = 1	
			IF NOT bDomScream	
				//Aaaaaaaaaagggghhhh!
				//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_WIL", "EXT4_WIL_1", CONV_PRIORITY_MEDIUM)  
					bDomScream = TRUE
				//ENDIF
			ENDIF
		ENDIF	
			
		fExpectedFrameSpeed = 1.0 / 30.0	
		fTimeScalar = GET_FRAME_TIME() / (fExpectedFrameSpeed)	// Find the fraction of a frame updated this time
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 630 //650	
			fTimeScale = fTimeScale + (0.035*fTimeScalar)              //speeding up time
			IF fTimeScale > 1.0
				fTimeScale = 1.0
			ENDIF
			//IF fTimeScale >= 0.5	
			//IF iCamSwitch >= 3	
			SET_TIME_SCALE(fTimeScale)
		ELSE
			fTimeScale = fTimeScale - (0.07*fTimeScalar)     //slowing down time
			IF fTimeScale < 0.1   //0.05
				fTimeScale = 0.1  //0.05
			ENDIF
			SET_TIME_SCALE(fTimeScale)
		ENDIF
		
		IF iCamSwitch = 0 //start initial interp
			//RENDER_SCRIPT_CAMS(TRUE, FALSE)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** interp and activating Cam 2 ****")
			#ENDIF
			//SET_CAM_ACTIVE_WITH_INTERP(camDomJump2,camDomJump,7000,  GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			iCamSwitch = 1
		ENDIF
		
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 563
			IF iCamSwitch = 1 //cut
				SAFE_DELETE_OBJECT(objDummyBox)
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				#IF IS_DEBUG_BUILD
					//CPRINTLN(DEBUG_MISSION, "EXT4**** interp and activating Cam 4 ****")
				#ENDIF
				//SET_CAM_ACTIVE(camDomJump3,TRUE)
				//SET_CAM_ACTIVE_WITH_INTERP(camDomJump4,camDomJump3,8000,  GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				IF IS_PED_UNINJURED(mDom.mPed)
//					POINT_CAM_AT_ENTITY(camDomJump3, mDom.mPed, <<0,0,0>>)
//					POINT_CAM_AT_ENTITY(camDomJump4, mDom.mPed, <<0,0,0>>)
				ENDIF
				iCamSwitch = 2
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 871
			IF iCamSwitch = 2
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				//SET_CAM_ACTIVE(camDomJump5,TRUE)
				#IF IS_DEBUG_BUILD
					//CPRINTLN(DEBUG_MISSION, "EXT4**** interp and activating Cam 6 ****")
				#ENDIF
				//SET_CAM_ACTIVE_WITH_INTERP(camDomJump6,camDomJump5,1900,  GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				IF IS_PED_UNINJURED(mDom.mPed)
					//POINT_CAM_AT_ENTITY(camDomJump6, mDom.mPed, <<0,0,0>>)
					//POINT_CAM_AT_ENTITY(camDomJump5, mDom.mPed, <<0,0,0>>)
				ENDIF
				iCamSwitch = 3
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 2000
			IF iCamSwitch = 3
				IF IS_PED_UNINJURED(mDom.mPed)
					//SET_ENTITY_HEALTH(mDom.mPed, 101) // make sure he dies doing the jump
					//SET_ENTITY_COORDS(mDom.mPed,<< 1657.0461, -17.4571, 145.2477 >>,FALSE,TRUE,TRUE)  //z 150.2477

					//SET_PED_HEADING_AND_PITCH(mDom.mPed, mDom.fStartHeading, 145)

//					TASK_PLAY_ANIM(mDom.mPed, "rcmextreme4", "base_jump_spot",SLOW_BLEND_IN,-1,-1,AF_NOT_INTERRUPTABLE, 0.9)

//					GET_GROUND_Z_FOR_3D_COORD(<< 1657.0461, -17.4571, 145.2477 >>, fZheight)
//					SET_PED_TO_RAGDOLL_WITH_FALL(mDom.mPed, 8000, 20000, TYPE_DIE_FROM_HIGH, <<0,0,-1>>, fZheight, <<0,0,0>>, <<0,0,0>>) 
//					FORCE_PED_MOTION_STATE(mDom.mPed, MS_PARACHUTING)
					//SET_HIGH_FALL_TASK(mDom.mPed, 10000, 60000)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
				ENDIF
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				//SET_CAM_ACTIVE(camDomJump7,TRUE)
				#IF IS_DEBUG_BUILD
					//CPRINTLN(DEBUG_MISSION, "EXT4**** interp and activating Cam 7 ****")
				#ENDIF
				////SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1659.2251, -7.8510, 172.7747>>)
				//SET_ENTITY_HEADING(PLAYER_PED_ID(), 102.7977)
				//SET_CAM_ACTIVE_WITH_INTERP(camDomJump8,camDomJump7,2000,  GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				iCamSwitch = 4
			ENDIF
		ENDIF
		
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 3500
			IF iCamSwitch = 4
				//RENDER_SCRIPT_CAMS(TRUE, FALSE)
				//SET_CAM_ACTIVE(camDomJump9, TRUE)
				#IF IS_DEBUG_BUILD
					//CPRINTLN(DEBUG_MISSION, "EXT4**** interp and activating Cam 9 ****")
				#ENDIF
				//SET_CAM_ACTIVE_WITH_INTERP(camDomJump10,camDomJump9,5000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				iCamSwitch = 5
			ENDIF
		ENDIF
		/*
		IF iCamSwitch = 5
			IF NOT IS_ENTITY_DEAD(mDom.mPed)
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mDom.mPed)
					//SET_ENTITY_HEALTH(mDom.mPed, 0)
					//CPRINTLN(DEBUG_MISSION, "EXT4**** Setting Doms hp to 0 ****")
					APPLY_PED_BLOOD(mDom.mPed,0,GET_WORLD_POSITION_OF_ENTITY_BONE(mDom.mPed,GET_PED_BONE_INDEX(mDom.mPed, BONETAG_HEAD)),"Scripted_Ped_Splash_Back")
					iCamSwitch = 6
				ENDIF	
			ELSE
				APPLY_PED_BLOOD(mDom.mPed,0,GET_WORLD_POSITION_OF_ENTITY_BONE(mDom.mPed,GET_PED_BONE_INDEX(mDom.mPed, BONETAG_HEAD)),"Scripted_Ped_Splash_Back")
				iCamSwitch = 6
			ENDIF
		ENDIF
		*/
		IF GET_GAME_TIMER() > iTimerDomJumpCam + 10000
			IF iCamSwitch <> 7 
				DESTROY_CAM(camDomJumpSS3)
				camDomJump11 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
				SET_CAM_PARAMS(camDomJump11,<<1659.892700,-6.552459,174.870987>>,<<-29.259314,0.000001,122.450188>>,50.000000)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_CAM_ACTIVE(camDomJump11, TRUE)
				/*
				IF IS_PED_UNINJURED(mDom.mPed)	
					CLEAR_PED_TASKS(mDom.mPed)
				ENDIF
				IF IS_ENTITY_ALIVE(mDom.mPed)	
					CLEAR_PED_TASKS_IMMEDIATELY(mDom.mPed)
					SET_ENTITY_COORDS(mDom.mPed,<<1655.77, -20.04, 134.8>>) //134.74
					SET_ENTITY_HEALTH(mDom.mPed, 0)
				ENDIF
				*/
				iCamSwitch = 7
			ENDIF
		ENDIF
		/*
		IF NOT bDomBreakBonesSound		
			IF IS_ENTITY_DEAD(mDom.mPed)
			OR IS_PED_INJURED(mDom.mPed)
				PLAY_SOUND_FROM_COORD(-1,"DOM_LANDS",<<1653.1371, -20.1164, 133.2946>>,"EXTREME_04_SOUNDSET")
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_HEAD,0.810, 0.733, BDT_STAB)
				APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_TORSO,0.940, 0.590, BDT_SHOTGUN_SMALL)
				APPLY_PED_BLOOD(mDom.mPed,0,GET_WORLD_POSITION_OF_ENTITY_BONE(mDom.mPed,GET_PED_BONE_INDEX(mDom.mPed, BONETAG_HEAD)),"Scripted_Ped_Splash_Back")
				bDomBreakBonesSound = TRUE
			ENDIF
		ENDIF
		*/
		/*
		IF NOT IS_ENTITY_DEAD(mDom.mPed)
			vDomRot = GET_ENTITY_ROTATION(mDom.mPed)
			fDomHeading = GET_ENTITY_HEADING(mDom.mPed)
			vDomPos = GET_ENTITY_COORDS(mDom.mPed)
		ENDIF
		*/	
		#IF IS_DEBUG_BUILD
			IF eMissionStage <> MS_MISSION_FAILING	
				if bFinishedSkipping = TRUE
					DEBUG_Check_Debug_Keys() // Check for debug keys
				ELSE
					JumpToStage(eTargetStage) // Skipping stages
				ENDIF	
			ENDIF
		#ENDIF			
					
	ENDWHILE
	
	IF bSkipCS
		IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)
			STOP_SYNCHRONIZED_AUDIO_EVENT(issDomJump3)
		ENDIF
		CANCEL_MUSIC_EVENT("EXT4_JUMPED_OS")
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		camDomJump11 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
		SET_CAM_PARAMS(camDomJump11,<<1659.892700,-6.552459,174.870987>>,<<-29.259314,0.000001,122.450188>>,50.000000)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		SET_CAM_ACTIVE(camDomJump11, TRUE)
		IF NOT bDomBlood
			ptfxBleed = START_PARTICLE_FX_LOOPED_AT_COORD("scr_extrm4_water_blood", << 1655.27, -20.8134, 133.842 >>, <<0,0,0>>,1)
			SET_PARTICLE_FX_LOOPED_ALPHA(ptfxBleed,1)
			bDomBlood = TRUE
		ENDIF
		DO_SCREEN_FADE_IN(500)
		WAIT(100)
	ENDIF
		
	DEACTIVATE_AUDIO_SLOWMO_MODE("SLOWMO_EXTREME_04")
	
	//STOP_SOUND(iWindSound)
	
	SAFE_DELETE_OBJECT(objDummyBox)
	
	IF IS_PED_UNINJURED(mDom.mPed)
		IF iCamSwitch < 3
			SET_ENTITY_HEALTH(mDom.mPed, 101)
			VECTOR vDomSkipCoords = GET_ENTITY_COORDS(mDom.mPed)
			FLOAT fZ	
			IF vDomSkipCoords.z > 155
				fZ = 155
			ELSE
				fZ = vDomSkipCoords.z
			ENDIF
			SET_ENTITY_COORDS(mDom.mPed,<< 1657.0461, -17.4571, fZ >>)
		ENDIF
	ENDIF
	
	REPLAY_STOP_EVENT()
	RC_END_CUTSCENE_MODE()
//	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,SPC_REENABLE_CONTROL_ON_DEATH)
	SET_TIME_SCALE(1.0)
	IF IS_ENTITY_ALIVE(mDom.mPed)	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(issDomJump3)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4 SYNCH SCENE RUNNING (issDomJump3)")
			#ENDIF
			SET_SYNCHRONIZED_SCENE_PHASE(issDomJump3,1.0)
		ELSE
			CPRINTLN(DEBUG_MISSION, "EXT4 SYNCH SCENE NOT RUNNING (issDomJump3)")
		ENDIF
		CLEAR_PED_TASKS_IMMEDIATELY(mDom.mPed)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
		SET_ENTITY_COORDS(mDom.mPed,<<1655.77, -20.04, 135.8>>) //134.74
		SET_ENTITY_HEALTH(mDom.mPed, 0)
	ENDIF
	ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
	CREATE_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_DUMB", CONV_PRIORITY_MEDIUM)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "EXT4**** Finished scripted cutscene ****")
	#ENDIF
	
ENDPROC

/// PURPOSE: Dom jumps
PROC DomJumps()

	SWITCH eSubStage
		
		CASE SS_SETUP
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "EXT4**** Init Dom jumps ****")
			#ENDIF
			//if IS_PED_UNINJURED(mDom.mPed)			
				//IF NOT IS_ENTITY_PLAYING_ANIM(mDom.mPed,"rcmextreme4", "base_jump_spot")
					//SET_ENTITY_COLLISION(mDom.mPed,TRUE)
					//TASK_PLAY_ANIM(mDom.mPed, "rcmextreme4", "base_jump_spot",SLOW_BLEND_IN,-1,-1,AF_NOT_INTERRUPTABLE|AF_ACTIVATE_RAGDOLL_ON_COLLISION|AF_HOLD_LAST_FRAME)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(mDom.mPed)
				//ENDIF	
			//ENDIF
					   //Aaaaaaaaaagggghhhh!
			//PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_WIL", "EXT4_WIL_1", CONV_PRIORITY_MEDIUM)  
			bPlayerParaTask = FALSE	
			SETTIMERA(0)
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE

			IF DOES_ENTITY_EXIST(objParachute)	
				IF NOT IS_ENTITY_DEAD(objParachute)
					//IF NOT DOES_ENTITY_HAVE_PHYSICS(objParachute)
					//	ACTIVATE_PHYSICS(objParachute)
					//ENDIF
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),objParachute,1.0)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1,FALSE,FALSE)
						DELETE_OBJECT(objParachute)
					ENDIF
				ENDIF
			ENDIF
			/*
			IF bFakeDomCreated = FALSE	
				IF IS_ENTITY_DEAD(mDom.mPed)
					vDomRot = GET_ENTITY_ROTATION(mDom.mPed)
					fDomHeading = GET_ENTITY_HEADING(mDom.mPed)
					vDomPos = GET_ENTITY_COORDS(mDom.mPed,FALSE)
					IF NOT IS_SPHERE_VISIBLE(vDomPos,15.0)	
						DELETE_PED(mDom.mPed)
						RC_CREATE_NPC_PED(mDom.mPed, CHAR_DOM, <<vDomPos.x,vDomPos.y,vDomPos.z + 0.4>>, fDomHeading, "Ext4-Dom")
						SET_ENTITY_ROTATION(mDom.mPed,vDomRot)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mDom.mPed,TRUE)
						SET_PED_COMPONENT_VARIATION(mDom.mPed,PED_COMP_SPECIAL,2,0,0)
						DISABLE_PED_PAIN_AUDIO(mDom.mPed,TRUE)
						SET_PED_TO_RAGDOLL(mDom.mPed,1000,1000,TASK_RELAX,FALSE,FALSE)
						ACTIVATE_PHYSICS(mDom.mPed)
						SET_ENTITY_HEALTH(mDom.mPed, 0)
						bFakeDomCreated = TRUE
					ENDIF
				ELSE
					vDomRot = GET_ENTITY_ROTATION(mDom.mPed)
					fDomHeading = GET_ENTITY_HEADING(mDom.mPed)
					vDomPos = GET_ENTITY_COORDS(mDom.mPed)
				ENDIF
			ENDIF
			*/
			IF bJumpCam = FALSE
				DomJumpCam()
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
				iTimerDomJumped = GET_GAME_TIMER()
				if IS_ENTITY_ALIVE(mDom.mPed)
					//SET_ENTITY_COLLISION(mDom.mPed,TRUE)
				ENDIF
				bJumpCam = TRUE
			ELSE
				/*
				IF NOT bDomScream	
					//Aaaaaaaaaagggghhhh!
					PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_WIL", "EXT4_WIL_1", CONV_PRIORITY_MEDIUM)  
					bDomScream = TRUE
				ENDIF
				IF NOT bDomBreakBonesSound	
					IF IS_ENTITY_DEAD(mDom.mPed)
					OR IS_PED_INJURED(mDom.mPed)
						PLAY_SOUND_FROM_COORD(-1,"DOM_LANDS",<<1653.1371, -20.1164, 133.2946>>,"EXTREME_04_SOUNDSET")
						APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_HEAD,0.810, 0.733, BDT_STAB)
						APPLY_PED_BLOOD_DAMAGE_BY_ZONE(mDom.mPed, PDZ_TORSO,0.940, 0.590, BDT_SHOTGUN_SMALL)
						APPLY_PED_BLOOD(mDom.mPed,0,GET_WORLD_POSITION_OF_ENTITY_BONE(mDom.mPed,GET_PED_BONE_INDEX(mDom.mPed, BONETAG_HEAD)),"Scripted_Ped_Splash_Back")
						bDomBreakBonesSound = TRUE
					ENDIF
				ENDIF
				*/
			ENDIF
			if IS_PED_UNINJURED(PLAYER_PED_ID()) // wait for dom to jump
				IF bPlayerParaTask = FALSE	
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1624.015015,-5.451070,102.067657>>, <<1639.006592,-52.987503,181.335205>>, 58.750000)
					AND GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
						//IF IS_PED_FALLING(PLAYER_PED_ID())
							//TASK_PARACHUTE(PLAYER_PED_ID(),FALSE)
							//FORCE_PED_TO_OPEN_PARACHUTE(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION, "EXT4**** Franklin falling ****")
							#ENDIF
							bPlayerParaTask = TRUE
						//ENDIF
					ENDIF
				ENDIF
				if IS_ENTITY_ALIVE(mDom.mPed)
					if TIMERA() > 5000
						IF GET_GAME_TIMER() > iTimerDomJumped + 25000
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1665.728516,-26.278694,172.581512>>, <<1672.651855,-24.583941,177.254669>>, 4.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1650.518921,5.134407,172.674332>>, <<1656.570557,9.669823,176.774338>>, 15.250000)
							eSubStage = SS_CLEANUP // dom has jumped
						ENDIF
					ENDIF
				ELSE
					IF GET_GAME_TIMER() > iTimerDomJumped + 25000
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1665.728516,-26.278694,172.581512>>, <<1672.651855,-24.583941,177.254669>>, 4.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1650.518921,5.134407,172.674332>>, <<1656.570557,9.669823,176.774338>>, 15.250000)	
						eSubStage = SS_CLEANUP // dom died
					ENDIF
				ENDIF
			ENDIF
	
			//Disable timer on Dom initially falling
			IF GET_GAME_TIMER() < (iTimerDomJumped + 5000)
				IF IS_REPLAY_RECORDING()
					PRINTLN("[BEAG] DISABLING REPLAY CAMERA")
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP													//Evolution in action...	
			IF NOT IS_PED_FALLING(PLAYER_PED_ID())	
			AND NOT IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) < 2.0
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())	
				//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_WIL", "EXT4_WIL_2", CONV_PRIORITY_HIGH)  
				IF CREATE_CONVERSATION(sSpeach, "EXT4AUD", "EXT4_WIL2",CONV_PRIORITY_HIGH)
					Script_Passed()
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE: Mission setup
PROC Intro()

	IF IS_REPLAY_IN_PROGRESS() = FALSE
		IntroMocap()
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "EXT4**** Replay skip mocap cutscene ****")
		#ENDIF
		START_REPLAY_SETUP(<< 1732.1328, 91.9424, 169.7872 >>,128.6917)
		//SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 1732.1328, 91.9424, 169.7872 >>)
		//SET_ENTITY_HEADING(PLAYER_PED_ID(),128.6917)
		//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
		//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
		END_REPLAY_SETUP()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
		SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
		TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<1723.4518, 80.7290, 169.6701>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
		//WAIT(500)
		SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
		bIntroCutDone = TRUE
		RC_END_Z_SKIP(TRUE) 	
	ENDIF
	IF bIntroCutDone = TRUE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		Init()	
	ENDIF

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
	  	SetupScene_EXTREME_4(sRCLauncherDataLocal)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	/*
	IF NOT DOES_PICKUP_EXIST(pickupParachute)
		pickupParachute = CREATE_PICKUP_ROTATE(PICKUP_PARACHUTE,<<1666.29,-11.20,173.270350>>,<<-111.822540,-0.454914,-162.223083>>)
	ENDIF
	*/
	//IF NOT DOES_PICKUP_EXIST(pickupParachute)
	//	pickupParachute = CREATE_PICKUP_ROTATE(PICKUP_PARACHUTE,<<1665.3146, -11.4569, 172.7747>>,<<-111.822540,-0.454914,-162.223083>>)
	//ENDIF
	//pickupParachute = CREATE_AMBIENT_PICKUP(PICKUP_PARACHUTE,<<1666.29,-11.20,173.270350>>)
	IF DOES_ENTITY_EXIST(pickupParachute)
	ENDIF

	// INITIALISE EVERYTHING
	InitVariables()
	
	IF NOT IS_REPLAY_IN_PROGRESS()	
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	
		AND IS_PED_UNINJURED(PLAYER_PED_ID())	
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],4)	
		//AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 3.1	
			bDoingFocusPush = TRUE
		ENDIF
	ENDIF		
							
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_UR")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF eMissionStage <> MS_MISSION_FAILING	
		AND eMissionStage <> MS_INTRO		
			CheckForFail()
		ENDIF
		
		IF eMissionStage < MS_DOM_JUMPS	
		AND bDoingFocusPush		
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1668.882080,-19.652481,172.674759>>, <<1653.855347,8.008027,175.024780>>, 7.000000)
				IF IS_PED_UNINJURED(mDom.mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mDom.mPed,10)
						DO_FOCUS_PUSH_TWO()
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
		// MAIN LOOP
		SWITCH eMissionStage
			CASE MS_INTRO
				Intro()
			BREAK
			
			CASE MS_MEET_DOM
				MeetDom()
			BREAK
			
			CASE MS_MIDTRO
				MidtroMocap()
			BREAK
			
			CASE MS_DOM_JUMPS
				DomJumps()
			BREAK
			
			CASE MS_MISSION_FAILING
				FailWait()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			IF eMissionStage <> MS_MISSION_FAILING	
				if bFinishedSkipping = TRUE
					DEBUG_Check_Debug_Keys() // Check for debug keys
				ELSE
					JumpToStage(eTargetStage) // Skipping stages
				ENDIF	
			ENDIF
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

