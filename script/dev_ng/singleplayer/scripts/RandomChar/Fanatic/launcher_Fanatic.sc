

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Fanatic.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Fanatic.sc
//
//		AUTHOR		:	David Roberts / Andrew Minghella
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
//
// 						If multiple missions share same coord or are within close proximity,
//						we should just add one world point, and adjust the tolerance float.
// *****************************************************************************************
// Enums
ENUM FAN_CONV_STATE
	FAN_CONV_WAITING,
	FAN_CONV_DOLINE,
	FAN_CONV_SPEAKING
ENDENUM

// Constants
CONST_FLOAT WORLD_POINT_COORD_TOLERANCE 1.0

// ------------Variables----------------------
SCENARIO_BLOCKING_INDEX mScenarioBlocker
INT 					iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST // ID to register off-mission cutscene load request with cutscene_controller.
BOOL 					bVariationsSet = FALSE
BOOL 					bFan3ConvDone = FALSE
INT 					iFan3PushupTimer = -1
BLIP_INDEX  			biLeadIn

// ----------Functions -------------------------
/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
/// PARAMS:
///    sData - launcher data struct
///    bCleanupEntities - do we want to cleanup the entities in the launcher struct
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)
	
	IF bCleanupEntities
		PRINTSTRING("\nSCRIPT TERMINATING: Cleaning up entities in Launcher")PRINTNL()
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	// Remove lead-in blip
	IF DOES_BLIP_EXIST(biLeadIn)
		REMOVE_BLIP(biLeadIn)
	ENDIF
	
	// Unload launcher animation dictionary
	REMOVE_LAUNCHER_ANIM_DICT(sData.sAnims)
	
	// Remove scenario blocking areas
	IF sData.eMissionID = RC_FANATIC_2
		REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	ENDIF
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF
	
	// Stop launcher conversation
	STRING sConversationRoot 
	SWITCH sData.eMissionID
		CASE RC_FANATIC_1
			sConversationRoot = "FAN1_AMB"
		BREAK
		CASE RC_FANATIC_2
			sConversationRoot = "FAN2_AMB"
		BREAK
		CASE RC_FANATIC_3
			sConversationRoot = "FAN3_AMB"
		BREAK
	ENDSWITCH
	RC_STOP_LAUNCHER_DIALOGUE(sConversationRoot)
	
	//B*1574385 - Force update to blip in RandChar Controller if mission wasn't launched
	IF bCleanupEntities
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC DO_PUSHUP_BREATHS(g_structRCScriptArgs& sData)

	IF iFan3PushupTimer = -1
		iFan3PushupTimer = GET_GAME_TIMER() // Initialise timer
	ENDIF
	
	IF (GET_GAME_TIMER() - iFan3PushupTimer) >= 900
		IF IS_ENTITY_ALIVE(sData.pedID[0])
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(sData.pedID[0], "PUSHUP_BREATH", "MARYANN", SPEECH_PARAMS_FORCE_NORMAL)
				iFan3PushupTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates the initial scene
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)

	//Setup the correct initial scene
	SWITCH sData.eMissionID
		CASE RC_FANATIC_1
			IF NOT SetupScene_FANATIC_1(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_FANATIC_2
			IF NOT SetupScene_FANATIC_2(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_FANATIC_3
			IF NOT SetupScene_FANATIC_3(sData)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Specific Fanatic 2 setup
	IF sData.eMissionID = RC_FANATIC_2
		mScenarioBlocker = Fan2_Scenario_Blocker()
		CLEAR_AREA_OF_PEDS(<<816.30, 1275.61, 359.50>>, 75.0)
	ENDIF
	
	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)
	
	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs 	sRCLauncherData				// Scene information to pass to mission script
	VECTOR				 	vInCoords = <<0,0,0>>		// Stores world point location

	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update world point
	vInCoords = in_coords.vec_coord[0]

	// Determine which RC mission we are attempting to launch
	g_eRC_MissionIDs eRCMissions[3]
	eRCMissions[0] = RC_FANATIC_1
	eRCMissions[1] = RC_FANATIC_2
	eRCMissions[2] = RC_FANATIC_3
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF
	
	//Pick which mission activated us
	IF NOT DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vInCoords, WORLD_POINT_COORD_TOLERANCE)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF

	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF	

	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE
	
	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vInCoords, sRCLauncherData.activationRange, TRUE)
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger the mission?
	WHILE (TRUE)
		WAIT(0)
		
		//Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
			Script_Cleanup(sRCLauncherData)
		ENDIF	

		// Update launcher blip + preload intro
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vInCoords)
		
		IF bVariationsSet = FALSE
			IF sRCLauncherData.eMissionID = RC_FANATIC_2
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
						PRINT_LAUNCHER_DEBUG("Trying to set Mary Ann boyfriend component variation")
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("MaryAnnes_Friend", sRCLauncherData.pedID[1])
						SET_CUTSCENE_PED_PROP_VARIATION("MaryAnnes_Friend", ANCHOR_HEAD, 0)
						bVariationsSet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF sRCLauncherData.eMissionID = RC_FANATIC_1
			PLAY_LAUNCHER_FULL_CONVERSATION(sRCLauncherData, vInCoords, "FAN1AU", "FAN1_AMB", 3, "MARYANN", 1500)
		ELIF sRCLauncherData.eMissionID = RC_FANATIC_2
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "FAN2AU", "FAN2_AMB", 3, "MARYANN")
		ELIF sRCLauncherData.eMissionID = RC_FANATIC_3
			IF bFan3ConvDone = FALSE
				IF PLAY_LAUNCHER_FULL_CONVERSATION(sRCLauncherData, vInCoords, "FAN3AUD", "FAN3_AMB", 3, "MARYANN", 50)
					bFan3ConvDone = TRUE
				ENDIF
			ELSE
				DO_PUSHUP_BREATHS(sRCLauncherData)
			ENDIF
		ENDIF
		
		//Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)

			// Create mission blip that persists for lead-in scene
			// Original will be removed when RC controller shuts down
			IF DOES_RC_MISSION_HAVE_LEAD_IN(sRCLauncherData.eMissionID)
				CREATE_BLIP_FOR_LEAD_IN(sRCLauncherData.eMissionID, biLeadIn)
			ENDIF

			//Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			//Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				Script_Cleanup(sRCLauncherData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
			ENDIF	
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
