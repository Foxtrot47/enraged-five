USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE
MODEL_NAMES         mContactModel      = GET_NPC_PED_MODEL(CHAR_MARY_ANN)

/// PURPOSE:
///    Closes the gates at the top of the hill in Fanatic 2
/// RETURNS:
///    TRUE if both gates have been closed
FUNC BOOL CLOSE_GATES()
	
	INT iNumGatesOpen = 0
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<801.714844,1270.138306,359.285522>>, 6.0, prop_facgate_03_l)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_facgate_03_l, <<801.714844,1270.138306,359.285522>>, TRUE, 0.0)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Fanatic2: Gate 1 Closed")
	ENDIF

	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<802.919495, 1280.919678, 360.727234>>, 6.0, prop_facgate_03_r)	
   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_facgate_03_r, <<802.919495, 1280.919678, 360.727234>>, TRUE, 0.0)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Fanatic2:Gate 2 Closed")
	ENDIF
	
	IF iNumGatesOpen = 2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC SCENARIO_BLOCKING_INDEX Fan2_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<744.7144, 1254.7257, 357.0291>>, <<847.8754, 1342.2434, 370.0352>>)
ENDFUNC
 
/// PURPOSE: 
///    Creates the scene for Fanatic 1.
FUNC BOOL SetupScene_FANATIC_1(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT 0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 14.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.sIntroCutscene = "EF_1_RCM"	

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmfanatic1", "ef_1_rcm_mary_ann_streching_base")
			sRCLauncherData.sAnims.vAnimPos = <<-1878.223022,-440.518127,46.039829>>
			sRCLauncherData.sAnims.vAnimRot = <<0, 0, 159.97>>
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
	
		CASE IS_WAIT_FOR_SCENE
	
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE		
	
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Mary-Ann
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARY_ANN, <<-1878.27, -440.55, 46.23>> , 165.94, "FANATIC LAUNCHER RC")
					SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherData.pedID[0], TRUE)
					FREEZE_ENTITY_POSITION(sRCLauncherData.pedID[0], TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Fanatic 2.
FUNC BOOL SetupScene_FANATIC_2(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT NUM_OF_BIKES  2
	CONST_INT MODEL_CONTACT	0
	CONST_INT MODEL_CYCLIST 1
	CONST_INT MODEL_BIKE	2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_CYCLIST] = U_M_Y_CYCLIST_01
	mModel[MODEL_BIKE] = SCORCHER	
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 14.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "EF_2_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcm_fanatic2", "ef_2_rcm__maryann_biking_maryann")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Mary-Ann
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARY_ANN, <<809.66, 1279.76, 360.49>>, 122.53, "FANATIC LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create boyfriend
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_CYCLIST], <<808.43, 1279.16, 360.47>>, -79.11)
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_TORSO, 1, 2)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_LEG, 1, 0)
					SET_PED_PROP_INDEX(sRCLauncherData.pedID[1], ANCHOR_HEAD, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[1], TRUE)
					SET_PED_LOD_MULTIPLIER(sRCLauncherData.pedID[1], 3.5)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Bikes
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_BIKE], <<808.35, 1277.20, 360.15>>, 216.50)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1] , mModel[MODEL_BIKE], <<807.89, 1275.73, 360.18>>, 208.20)
			ENDIF

			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
	
		CASE IS_COMPLETE_SCENE
	
			// Colour setup	
			FOR iCount = 0 TO NUM_OF_BIKES -1
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[iCount])
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[iCount], iCount)
				ENDIF
			ENDFOR
			
			// Suppress ambient bikes
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_BIKE], TRUE)
	
			// Play animation for boyfriend
			IF HAS_ANIM_DICT_LOADED("rcm_fanatic2")
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
					TASK_PLAY_ANIM(sRCLauncherData.pedID[1], "rcm_fanatic2", "ef_2_rcm__maryann_biking_cyclist", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					PRINT_LAUNCHER_DEBUG("Playing Mary Ann's friend anim...")
				ENDIF
			ENDIF
	
			// Close hilltop gates
			CLOSE_GATES()
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Fanatic 3.
FUNC BOOL SetupScene_FANATIC_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT		0
	CONST_INT MODEL_TOWEL		1
	CONST_INT MODEL_DRINK		2
	CONST_INT MODEL_WEIGHT1		3
	CONST_INT MODEL_WEIGHT2		4
	
	// Variables
	MODEL_NAMES mModel[5]
	INT         iCount
	BOOL        bCreatedScene
	VECTOR 		vPos

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_TOWEL]   = prop_beach_towel_01
	mModel[MODEL_DRINK]   = prop_energy_drink
	mModel[MODEL_WEIGHT1] = prop_freeweight_01
	mModel[MODEL_WEIGHT2] = prop_freeweight_02
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 14.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.sIntroCutscene = "EF_3_RCM_CONCAT"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmfanatic3", "ef_3_rcm_loop_maryann")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
	
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create AMary-Ann
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MARY_ANN, << -915.600, 6139.192, 5.525 >> , 111.240, "FANATIC LAUNCHER RC")
					SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.pedID[0], << -915.600, 6139.192, 5.525 >>, TRUE)
					SET_ENTITY_ROTATION(sRCLauncherData.pedID[0], << 2.520, 0.000, 111.240 >>, EULER_XYZ, FALSE)
					//FREEZE_ENTITY_POSITION(sRCLauncherData.pedID[0], TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Rolled up towel
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
				vPos = <<-916.6389, 6137.0459, 4.7157>>
				sRCLauncherData.objID[0] = CREATE_OBJECT(mModel[MODEL_TOWEL], vPos)
				SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.objID[0], <<-916.6389, 6137.0459, 4.7157>>)
				SET_ENTITY_ROTATION(sRCLauncherData.objID[0], <<1.7000, 19.1067, 79.5150>>, EULER_XYZ)
				SET_ENTITY_QUATERNION(sRCLauncherData.objID[0], -0.0949, 0.1369, 0.6325, 0.7564)
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[0], TRUE)
			ENDIF
			
			// Energy drink
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[1])
				vPos = <<-917.70, 6138.89, 4.72>>
				GET_GROUND_Z_FOR_3D_COORD(vPos, vPos.z)
				sRCLauncherData.objID[1] = CREATE_OBJECT(mModel[MODEL_DRINK], vPos)
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[1], TRUE)
				SET_ENTITY_ROTATION(sRCLauncherData.objID[1], <<-6.625818,1.301590,-1.248891>>, EULER_XYZ, FALSE)
			ENDIF
			
			// Smaller weight
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[2])
				vPos = <<-915.6122, 6137.6621, 4.6129>>
				sRCLauncherData.objID[2] = CREATE_OBJECT(mModel[MODEL_WEIGHT1], vPos)
				SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.objID[2], <<-915.6122, 6137.6621, 4.6129>>)
				SET_ENTITY_ROTATION(sRCLauncherData.objID[2], <<-1.6843, 2.1363, -0.0913>>, EULER_XYZ)
				SET_ENTITY_QUATERNION(sRCLauncherData.objID[2], -0.0147, 0.0187, -0.0011, 0.9997)
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[2], TRUE)
			ENDIF
	
			// Slightly larger weight
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[3])
				vPos = <<-915.4199, 6137.8555, 4.6221>>
				sRCLauncherData.objID[3] = CREATE_OBJECT(mModel[MODEL_WEIGHT2], vPos)
				SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherData.objID[3], <<-915.4199, 6137.8555, 4.6221>>)
				SET_ENTITY_ROTATION(sRCLauncherData.objID[3], <<-5.7930, 1.4960, -36.3330>>)
				SET_ENTITY_QUATERNION(sRCLauncherData.objID[3], -0.0439, 0.0281, -0.3120, 0.9487)
				FREEZE_ENTITY_POSITION(sRCLauncherData.objID[3], TRUE)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
	
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
