
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "commands_vehicle.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "RC_helper_functions.sch"
USING "dialogue_public.sch"
USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"
USING "initial_scenes_josh.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Josh4.sc
//		AUTHOR			:	David Roberts/Ian Gander
//		DESCRIPTION		:	Josh tells the cops that Trevor burnt down his house in a 
//							cutscene. Trevor must escape from the cops and lose his
//							wanted level.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//-------------------------

STRING 			g_CutsceneName = "josh_4_int_concat"
CONST_INT		PLAYER_WANTED_LEVEL						2
CONST_INT		MAX_FLEE_DIRECTIONS						5
MODEL_NAMES 	cop_model = S_M_Y_Cop_01
MODEL_NAMES 	cop_car_model = POLICE3
VEHICLE_INDEX	viTemp		// Temp vehicle index used for player's vehicle during a replay

//Main states of the mission
ENUM RC_MainState
	RC_LEADIN,
	RC_INTRO,										//Intro cutscene
	RC_ESCAPE_COPS									//Escape the cops
ENDENUM

ENUM RC_AI_CopCarState
	RC_AIC_LOAD,
	RC_AIC_CREATE,
	RC_AIC_START_REC,
	RC_AIC_UPDATE_REC,
	RC_AIC_END_REC,
	RC_AIC_FINISHED
ENDENUM
//Mini state machine for each mission state
ENUM RC_SubState
	SS_Setup,										//Create everything and start things off (objectives etc)
	SS_Update,										//Every frame - main running of the state
	SS_Cleanup										//Called last time through the state - clean thing up, set new state etc
ENDENUM

ENUM COP_ENCOUNTER_STATE
	CE_WAIT,
	CE_AIM,
	CE_AIMWAIT,
	CE_ATTACK,
	CE_ARREST
ENDENUM

ENUM JOSH_STATE
	JOSH_WATCH,
	JOSH_FLEE,
	JOSH_LOST,
	JOSH_DEAD
ENDENUM

ENUM DIALOGUE_STATE
	CONV_PAUSE,
	CONV_START,
	CONV_PLAYING,
	CONV_STOP
ENDENUM

STRUCT RC_DrivingCopCar
	VECTOR vStartPos
	FLOAT Qx
	FLOAT Qy
	FLOAT Qz
	FLOAT Qw
	
	VEHICLE_INDEX iIndex
	PED_INDEX mCopsInCar
	
	INT iRecNum
	STRING sRecName
	
	RC_AI_CopCarState	mState
ENDSTRUCT

STRUCT RC_TriggerArea
	VECTOR vec1
	VECTOR vec2
	FLOAT width
ENDSTRUCT

STRUCT RC_CAR_QUATS
	FLOAT Qx
	FLOAT Qy
	FLOAT Qz
	FLOAT Qw
ENDSTRUCT

// Checkpoint
//CONST_INT	CP_START	1

RC_MainState	m_state = RC_LEADIN					//Main states of the mission
RC_SubState		m_subState = SS_Setup				//Mini state machine for each state the mission goes into
RC_DrivingCopCar	m_AI_FleeCar
COP_ENCOUNTER_STATE	m_Cop_State = CE_WAIT
JOSH_STATE JoshState = JOSH_WATCH

DIALOGUE_STATE	ConvState = CONV_START
BOOL	bDoCopConv = TRUE
INT		iDialogueTimer

VECTOR vPlayerPosAfterIntro	= <<-1102.9056, 284.7310, 63.0602>>		//Where the player is after the cutscene
FLOAT fPlayerHeadAfterIntro	= 9.4866													//Heading of the player after the cutscene

VECTOR vRC_runToPoint								//Where Josh runs to - back of the house

structPedsForConversation cConversation				//Conversatino of the cops shouting at the start.
structPedsForConversation jConversation				//Conversation between Josh and Trevor

g_structRCScriptArgs sRCLauncherDataLocal			//Local copy of the data created by the launcher

OBJECT_INDEX idleCopGun
OBJECT_INDEX notepadCopGun

SEQUENCE_INDEX seq
VEHICLE_INDEX 	viCopCar
VEHICLE_INDEX 	viJoshCar
MODEL_NAMES		JoshCarModel = FELON2

CONST_INT MAX_COP_DIALOG 3
CONST_INT MAX_JOSH_DIALOG 3
int iFlee
int iShootTimer
BOOL bDone10SecWarning = FALSE
BOOL bCopWait = FALSE
BOOL bCopMove = FALSE
BOOL bCopsDead[2]
BOOL bCopsShooting = FALSE

BOOL bLeadinPlayed = FALSE
BOOL bLeadinConvo = FALSE
BOOL bCutsceneSkipped = FALSE

// Music triggers
BOOL bDoneMusicCutscene = FALSE
BOOL bDoneMusicStart = FALSE
BOOL bDoneMusicVehicle = FALSE

bool bDoJoshComment = FALSE
BOOL bDoneJoshJackingConv = FALSE
int iCutsceneProgress = 0
int iRantProgress = 0
INT iJoshFleeConvoCount = 0

INT iCopReleaseTimer
INT iTrevRantTimer
INT iJoshFleeConvoTimer

BOOL 	bDonePoliceReport = FALSE

RC_TriggerArea FleeTrigger[MAX_FLEE_DIRECTIONS]
VECTOR vCopCarRushPos[MAX_FLEE_DIRECTIONS]
RC_CAR_QUATS qFleeQuats[MAX_FLEE_DIRECTIONS]

CONST_INT JOSH 0
CONST_INT SPEAKER_COP 1
CONST_INT NOTEPAD_COP 2
CONST_INT COP_CAR 0
CONST_INT JOSH_CAR 1
	
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 3
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	INT menu_option
	BOOL bDebug_PrintToTTY = TRUE
	WIDGET_GROUP_ID widgetGroup
#ENDIF

// ===========================================================================================================
//		Debug prints
// ===========================================================================================================

/// PURPOSE: Print a string to the console
PROC DEBUG_PRINTSTRING(STRING s)

	#IF IS_DEBUG_BUILD
		IF bDebug_PrintToTTY
			CPRINTLN(DEBUG_MISSION, s)
		ENDIF 
	#ENDIF
	
	// Stop release compile error
	s = s

ENDPROC

/// PURPOSE: Print a string with an int to the console
PROC DEBUG_PRINTSI(STRING s, INT i)

	#IF IS_DEBUG_BUILD
		IF bDebug_PrintToTTY
			CPRINTLN(DEBUG_MISSION, s, i)
		ENDIF 
	#ENDIF
	
	// Stop release compile error
	s = s
	i = i

ENDPROC

/// PURPOSE:
///    Handle starting the music at appropriate times during the mission
PROC HANDLE_MUSIC()

	IF m_state = RC_INTRO
		IF IS_CUTSCENE_PLAYING()
			IF GET_CUTSCENE_TIME() > 14000
			AND NOT bDoneMusicCutscene
				TRIGGER_MUSIC_EVENT("JOSH4_START")
				DEBUG_PRINTSTRING("Starting 'JOSH4_START'")
				bDoneMusicCutscene = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT bDoneMusicStart
			TRIGGER_MUSIC_EVENT("JOSH4_ACTION")
			DEBUG_PRINTSTRING("Starting 'JOSH4_ACTION'")
			bDoneMusicStart = TRUE
		ELSE
			IF NOT bDoneMusicVehicle
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TRIGGER_MUSIC_EVENT("JOSH4_VEHICLE")
						DEBUG_PRINTSTRING("Starting 'JOSH4_VEHICLE'")
						bDoneMusicVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE: Set up all the positions for the cops and flee car required in the mission
PROC SetupPositions()
	
	#IF IS_DEBUG_BUILD
		mSkipMenu[0].sTxtLabel = "Intro"
		mSkipMenu[1].sTxtLabel = "Escape cops"
		mSkipMenu[2].sTxtLabel = "Debug: Kill Josh so alternative Trevor rant occurs"
	#ENDIF
	
	//Players postion
	vPlayerPosAfterIntro = <<-1102.9056, 284.7310, 63.0602>>				//Using INVALID_WORLD_Z to ensure actor is on the ground
	fPlayerHeadAfterIntro = 9.4866
	
	//Where RCcharacter runs to
	vRC_runToPoint =  << -1140.98, 297.59, 65.89 >>
	
	//Everything else about this car will need to get filled out during the mission
	m_AI_FleeCar.sRecName = "Josh4CopRush"
	m_AI_FleeCar.mState = RC_AIC_LOAD
		
	FleeTrigger[0].vec1 = <<-1152.525513,278.231750,65.693336>>
	FleeTrigger[0].vec2 = <<-1136.276611,239.055130,69.440590>>
	FleeTrigger[0].width = 3.250000
	
	FleeTrigger[1].vec1 = <<-1151.918335,287.227570,66.040184>>
	FleeTrigger[1].vec2 = <<-1137.050903,290.545349,70.042900>> 
	FleeTrigger[1].width = 3.250000
	
	FleeTrigger[2].vec1 = <<-1093.003174,295.528900,63.115368>>
	FleeTrigger[2].vec2 = <<-1069.194458,293.528595,67.535675>> 
	FleeTrigger[2].width = 3.250000
	
	FleeTrigger[3].vec1 = <<-1050.775513,284.393494,63.156693>>
	FleeTrigger[3].vec2 = <<-1050.369385,244.094513,67.863937>> 
	FleeTrigger[3].width = 3.250000

	FleeTrigger[4].vec1 = <<-1065.411011,229.621628,61.886574>>
	FleeTrigger[4].vec2 = <<-1104.021484,232.132095,68.149750>> 
	FleeTrigger[4].width = 3.250000
	
	vCopCarRushPos[0] = << -1357.5698, 200.1279, 58.0750 >>
	qFleeQuats[0].Qx = 0.0072
	qFleeQuats[0].Qy = 0.0163
	qFleeQuats[0].Qz = -0.5901
	qFleeQuats[0].Qw = 0.8071
	
	vCopCarRushPos[1] = << -1219.0353, 401.5473, 74.7130 >>
	qFleeQuats[1].Qx = -0.0145
	qFleeQuats[1].Qy = 0.0329
	qFleeQuats[1].Qz = -0.6893
	qFleeQuats[1].Qw = 0.7235
	
	vCopCarRushPos[2] = << -1113.0596, 397.6948, 69.1133 >>
	qFleeQuats[2].Qx = -0.0155
	qFleeQuats[2].Qy = -0.0400
	qFleeQuats[2].Qz = 0.7661
	qFleeQuats[2].Qw = -0.6413
	
	vCopCarRushPos[3] = << -885.0710, 240.0283, 71.7539 >>
	qFleeQuats[3].Qx = -0.0576
	qFleeQuats[3].Qy =  0.0000
	qFleeQuats[3].Qz = 0.5936
	qFleeQuats[3].Qw = 0.8027
	
	vCopCarRushPos[4] = << -1020.4949, 121.0783, 53.0567 >>
	qFleeQuats[4].Qx = 0.0234
	qFleeQuats[4].Qy = 0.0126
	qFleeQuats[4].Qz = 0.2792
	qFleeQuats[4].Qw = 0.9599
	
	REQUEST_ADDITIONAL_TEXT("JOSH4", MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	IF IS_REPLAY_BEING_SET_UP()
		IF OPEN_FRONT_GATES(FALSE)
			DEBUG_PRINTSTRING("*** Opened Josh gate for replay")
		ENDIF
	ENDIF
	
	DEBUG_PRINTSTRING("*** Set up coords")
	
ENDPROC

/// PURPOSE: Register all mission entities for cutscene
PROC SetupStartOfIntro()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered Trevor")
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
		REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[JOSH], "Josh", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered Josh")
	ENDIF
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		idleCopGun = CREATE_WEAPON_OBJECT(WEAPONTYPE_PISTOL, INFINITE_AMMO, GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[SPEAKER_COP]), TRUE)
		REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[SPEAKER_COP], "Cop_standing_idle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered Cop standing")
	ENDIF
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		notepadCopGun = CREATE_WEAPON_OBJECT(WEAPONTYPE_PISTOL, INFINITE_AMMO, GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[NOTEPAD_COP]), TRUE)
		REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[NOTEPAD_COP], "Cop_with_notepad", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered Cop with notepad")
	ENDIF
	
	IF IS_ENTITY_ALIVE(idleCopGun)
		REGISTER_ENTITY_FOR_CUTSCENE(idleCopGun, "Security_guard_pistol", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered idle cop gun")
	ENDIF
	
	IF IS_ENTITY_ALIVE(notepadCopGun)
		REGISTER_ENTITY_FOR_CUTSCENE(notepadCopGun, "FBI_Agent_1_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		DEBUG_PRINTSTRING("*** Registered notepad cop gun")
	ENDIF
	
	bDoneMusicCutscene = FALSE
	bDoneMusicStart = FALSE
	bDoneMusicVehicle = FALSE
	
ENDPROC

/// PURPOSE: Continually try and set the exit state for the cop and Josh while the cutscene is running
PROC GrabCop()

	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Josh")
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
			DEBUG_PRINTSTRING("Playing Josh's leadout")
			OPEN_SEQUENCE_TASK (seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE)
				//TASK_PLAY_ANIM(NULL, "rcmjosh4", "josh_leadout", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
				TASK_PLAY_ANIM(NULL, "rcmjosh4", "josh_leadout_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[JOSH], seq)
			CLEAR_SEQUENCE_TASK(seq)
			FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[JOSH], MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
		ENDIF
	ENDIF

	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Security_guard_pistol")
		IF IS_ENTITY_ALIVE(idleCopGun)
		AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			GIVE_WEAPON_OBJECT_TO_PED(idleCopGun, sRCLauncherDataLocal.pedID[SPEAKER_COP])
			DEBUG_PRINTSTRING("*** Gave idle cop gun")
		ENDIF
	ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FBI_Agent_1_Gun")
		IF IS_ENTITY_ALIVE(notepadCopGun)
		AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			GIVE_WEAPON_OBJECT_TO_PED(notepadCopGun, sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			DEBUG_PRINTSTRING("*** Gave notepad cop gun")
		ENDIF
	ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Cop_standing_idle")
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[SPEAKER_COP], MS_AIMING, DEFAULT, FAUS_CUTSCENE_EXIT)
			OPEN_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE|SLF_USE_TORSO)
				//TASK_PLAY_ANIM(NULL, "rcmjosh4", "josh_leadout_cop2", FAST_BLEND_IN, SLOW_BLEND_OUT, DEFAULT, AF_USE_KINEMATIC_PHYSICS|AF_NOT_INTERRUPTABLE)
				TASK_AIM_GUN_AT_ENTITY(null, PLAYER_PED_ID(),-1, TRUE)
				TASK_PLAY_ANIM(NULL, "combat@gestures@pistol@glances", "90", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS|AF_NOT_INTERRUPTABLE|AF_SECONDARY)
//				TASK_STAND_STILL(NULL, 4000)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[SPEAKER_COP], seq)
			CLEAR_SEQUENCE_TASK(seq)
			DEBUG_PRINTSTRING("*** Idle cop anim")
		ENDIF
	ENDIF
	
	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Cop_with_notepad")
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[NOTEPAD_COP], MS_AIMING, DEFAULT, FAUS_CUTSCENE_EXIT)
			OPEN_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE|SLF_USE_TORSO)
				//TASK_PLAY_ANIM(NULL, "rcmjosh4", "josh_leadout_cop1", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-1103.72, 289.46, 63.23>>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 0.1,
					DEFAULT, DEFAULT, DEFAULT, TRUE)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-1106.08, 288.55, 63.31>>, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 0.1,
					DEFAULT, DEFAULT, DEFAULT, TRUE)
				TASK_AIM_GUN_AT_ENTITY(null,PLAYER_PED_ID(),-1, TRUE)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[NOTEPAD_COP], seq)
			CLEAR_SEQUENCE_TASK(seq)			
			DEBUG_PRINTSTRING("*** Notepad cop anim")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL DO_TREVOR_EXIT()

	IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

/// PURPOSE: Get all the actors ready to come back into gameplay, load animations and models, set initial tasks and teleport them
PROC SetupEndOfIntro(BOOL bSkipped = FALSE)

	REQUEST_ANIM_DICT("combat@gestures@pistol@halt")
	REQUEST_ANIM_DICT("combat@gestures@pistol@glances")
	REQUEST_MODEL(JoshCarModel)
	
	WHILE NOT HAS_ANIM_DICT_LOADED("combat@gestures@pistol@halt")
	OR NOT HAS_ANIM_DICT_LOADED("combat@gestures@pistol@glances")
	OR NOT HAS_MODEL_LOADED(JoshCarModel)
		wait(0)
	ENDWHILE
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1138.32, 299.69, 65.94>>, 5.0, PROP_LRGGATE_01c_L)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1138.32, 299.69, 65.94>>, FALSE, 0)
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1137.52, 297.02, 65.81>>, 5.0, PROP_LRGGATE_01c_R)
   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_R, <<-1137.053833, 295.585571, 67.180458>>, FALSE, 0)
	ENDIF
	
	// Move Josh
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
		IF bSkipped = TRUE
			SET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[JOSH],<<-1103.9620, 291.3401, 63.3670>>)
			SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[JOSH],193.1737)
			TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "josh_leadout_loop", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
		ENDIF
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(sRCLauncherDataLocal.pedID[JOSH], TRUE)
		SET_PED_CAN_BE_TARGETTED(sRCLauncherDataLocal.pedID[JOSH], TRUE)
	ENDIF
	
	//Get all cops into position
	//INT i
	IF bSkipped = TRUE
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[NOTEPAD_COP],WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE)
			SET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[NOTEPAD_COP], <<-1106.08, 288.55, 63.31>>)
			OPEN_SEQUENCE_TASK (seq)
				TASK_AIM_GUN_AT_ENTITY(null,PLAYER_PED_ID(),-1, TRUE)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[NOTEPAD_COP], seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[NOTEPAD_COP],TRUE)
		ENDIF
	ENDIF
	
	IF bSkipped = TRUE
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[SPEAKER_COP],WEAPONTYPE_PISTOL,INFINITE_AMMO,TRUE)
			SET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[SPEAKER_COP], <<-1102.41, 289.73, 63.18>>)
			OPEN_SEQUENCE_TASK (seq)
				TASK_AIM_GUN_AT_ENTITY(null,PLAYER_PED_ID(),-1, TRUE)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[SPEAKER_COP], seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[SPEAKER_COP],TRUE)
		ENDIF
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[SPEAKER_COP], CA_REQUIRES_LOS_TO_SHOOT, TRUE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[NOTEPAD_COP], CA_REQUIRES_LOS_TO_SHOOT, TRUE)
	ENDIF
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[JOSH_CAR])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[JOSH_CAR], VEHICLELOCK_UNLOCKED)
	ENDIF
	
	// Add Trevor to the conversation early to avoid nasty asserts
	ADD_PED_FOR_DIALOGUE(jConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
	
	bDone10SecWarning = FALSE
	bCopWait = FALSE
	bCopMove = FALSE
	
	bDoJoshComment = FALSE
	bCopsDead[0] = FALSE
	bCopsDead[1] = FALSE
	IF bCopsDead[0]
	ENDIF
	bCopsShooting = FALSE
	m_Cop_State = CE_WAIT
	JoshState = JOSH_WATCH
	
	DEBUG_PRINTSTRING("*** End of intro set up")
	
ENDPROC

/// PURPOSE: State machine for both cop's behaviour after the mocap
PROC Update_GateCops()
	
	INT i
	INT n
	
	SWITCH m_Cop_State
		CASE CE_WAIT
			IF NOT bCopWait
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
					bCopWait = TRUE
				ENDIF
			ENDIF
			IF NOT bCopMove
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])					
					bCopMove = TRUE
				ENDIF
			ENDIF
			
			IF (GET_GAME_TIMER() - iShootTimer) > 4000
				// Aim after 4 seconds
				DEBUG_PRINTSTRING("************************************** TIMEOUT - AIM next frame")
				m_Cop_State = CE_AIM
				iShootTimer = GET_GAME_TIMER() //Reset the timer to be used again
			ENDIF
			
			for i = 1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[i],PLAYER_PED_ID(),FALSE) > 10.0
						DEBUG_PRINTSTRING("************************************** TOO FAR - AIM next frame")
						m_Cop_State = CE_AIM
						iShootTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDFOR
			
			// [1] Aim
			// [2] Move over
			// [1] Play an ambient anim?
			// if timer > 4 secs, go to CE_AIM
			// if player > 7m away, go to CE_AIM
		BREAK
		
		CASE CE_AIM
			m_Cop_State = CE_AIMWAIT
			// Keep aiming at player
		BREAK
		
		CASE CE_AIMWAIT
			IF (GET_GAME_TIMER() - iShootTimer) > 20000
				// Attack after 15 seconds
				DEBUG_PRINTSTRING("************************************** TIMEOUT - ARREST next frame")
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[SPEAKER_COP])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[SPEAKER_COP],FALSE)
						TASK_ARREST_PED(sRCLauncherDataLocal.pedID[SPEAKER_COP],PLAYER_PED_ID())
					ENDIF
				m_Cop_State = CE_ARREST
				iShootTimer = GET_GAME_TIMER() //Reset the timer to be used again
			ENDIF
			
			for i = 1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[i],PLAYER_PED_ID(),FALSE) > 12.0
						DEBUG_PRINTSTRING("************************************** TOO FAR - ATTACK next frame")
						m_Cop_State = CE_ATTACK
						iShootTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE CE_ARREST
			for i = 1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[i],PLAYER_PED_ID(),FALSE) > 12.0
						DEBUG_PRINTSTRING("************************************** TOO FAR - ATTACK next frame")
						m_Cop_State = CE_ATTACK
						iShootTimer = GET_GAME_TIMER()
					ELSE
						IF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[i])
							DEBUG_PRINTSTRING("************************************** THREAT DURING ARREST - ATTACK next frame")
							m_Cop_State = CE_ATTACK
							iShootTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		BREAK
		CASE CE_ATTACK
			ConvState = CONV_STOP // Make sure the warning/taunting conversations don't play
			IF NOT bCopsShooting
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
				
				FOR i=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[i])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[i],FALSE)
						OPEN_SEQUENCE_TASK (seq)
							TASK_SHOOT_AT_ENTITY(null,PLAYER_PED_ID(),-1,FIRING_TYPE_RANDOM_BURSTS)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[i], seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDFOR
				
				ADD_PED_FOR_DIALOGUE(cConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(cConversation, 3, sRCLauncherDataLocal.pedID[SPEAKER_COP], "JOSHCOP")
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(cConversation, "JOSH4AU", "JOSH4COP2", CONV_PRIORITY_MEDIUM)
				ELSE
					CREATE_CONVERSATION(cConversation, "JOSH4AU", "JOSH4COP2", CONV_PRIORITY_MEDIUM)
				ENDIF
				
				DEBUG_PRINTSTRING("Doing cop fire warning")
				bCopsShooting = TRUE
				iCopReleaseTimer = GET_GAME_TIMER()
			ELSE
				FOR n=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[n])
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[n], PLAYER_PED_ID()) > 20
							SET_PED_ACCURACY(sRCLauncherDataLocal.pedID[n], 75)
						ELSE
							SET_PED_ACCURACY(sRCLauncherDataLocal.pedID[n], 1)
						ENDIF
					ENDIF
				ENDFOR
				
				IF (GET_GAME_TIMER() - iCopReleaseTimer) > 15000
					FOR n=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[n])
							IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[n], PLAYER_PED_ID()) >= 50
								SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[n], FALSE, TRUE)
							ENDIF
						ENDIF
					ENDFOR
				ELSE
					FOR n=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
						IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[n])
							IF n = 1
								bCopsDead[0] = TRUE
							ELIF n = 2
								bCopsDead[1] = TRUE
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			// Give attack order
			// Play dialogue
			// Release peds
			// Turn on dispatch
		BREAK
	ENDSWITCH

	IF m_Cop_State != CE_ATTACK // Don't want to keep checking these over and over when the player breaks one of these 'rules' and the cops are already attacking
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				DEBUG_PRINTSTRING("************************************** VEHICLE - ATTACK now")
				m_Cop_State = CE_ATTACK
			ENDIF
		ENDIF
		
		FOR i=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1 
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sRCLauncherDataLocal.pedID[i])
					WEAPON_TYPE tempWT = WEAPONTYPE_UNARMED
					IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), tempWT) //wType <> WEAPONTYPE_UNARMED
						DEBUG_PRINTSTRING("************************************** AIMED WEAPON - ATTACK now")
						m_Cop_State = CE_ATTACK
					ENDIF
				ENDIF
				IF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[i])
					DEBUG_PRINTSTRING("************************************** ATTACKED/THREATENED - ATTACK now")
					m_Cop_State = CE_ATTACK
				ENDIF
				IF m_Cop_State != CE_ARREST // Don't care about Trevor being too close if they're trying to arrest him
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[i],PLAYER_PED_ID(),FALSE) < 2.3
						DEBUG_PRINTSTRING("************************************** TOO CLOSE - ATTACK now")
						m_Cop_State = CE_ATTACK
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
				
ENDPROC

/// PURPOSE: State machine for the scripted cop car that spawns as you run away
PROC Update_AI_CopCars()
	
	SWITCH (m_AI_FleeCar.mState)
		CASE RC_AIC_LOAD
			for iFlee = 0 to (MAX_FLEE_DIRECTIONS-1)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), FleeTrigger[iFlee].vec1, FleeTrigger[iFlee].vec2, FleeTrigger[iFlee].width, FALSE, TRUE, TM_IN_VEHICLE)
					DEBUG_PRINTSI("Player detected inside flee direction trigger: ", iFlee)
					REQUEST_MODEL(cop_car_model)
					REQUEST_MODEL(cop_model)
					m_AI_FleeCar.vStartPos = vCopCarRushPos[iFlee]
					m_AI_FleeCar.Qx = qFleeQuats[iFlee].Qx
					m_AI_FleeCar.Qy = qFleeQuats[iFlee].Qy
					m_AI_FleeCar.Qz = qFleeQuats[iFlee].Qz
					m_AI_FleeCar.Qw = qFleeQuats[iFlee].Qw
					switch iFlee
						CASE 0	m_AI_FleeCar.iRecNum = 000 break
						CASE 1	m_AI_FleeCar.iRecNum = 001 break
						CASE 2	m_AI_FleeCar.iRecNum = 002 break
						CASE 3	m_AI_FleeCar.iRecNum = 003 break
						CASE 4	m_AI_FleeCar.iRecNum = 004 break
					ENDSWITCH
					DEBUG_PRINTSI("iFlee value is: ", iFlee)
					DEBUG_PRINTSTRING("Requesting flee recording: ") DEBUG_PRINTSI(m_AI_FleeCar.sRecName, m_AI_FleeCar.iRecNum)
					
					REQUEST_VEHICLE_RECORDING(m_AI_FleeCar.iRecNum,m_AI_FleeCar.sRecName)
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(m_AI_FleeCar.iRecNum,m_AI_FleeCar.sRecName)
					OR NOT HAS_MODEL_LOADED(cop_car_model)
					OR NOT HAS_MODEL_LOADED(cop_model)
						WAIT(0)
					ENDWHILE
					
					DEBUG_PRINTSTRING("Flee car has loaded!")
					m_AI_FleeCar.mState = RC_AIC_CREATE
				ENDIF
			ENDFOR
		BREAK
		CASE RC_AIC_CREATE
			IF NOT DOES_ENTITY_EXIST(m_AI_FleeCar.iIndex)
				m_AI_FleeCar.iIndex = CREATE_VEHICLE(cop_car_model,m_AI_FleeCar.vStartPos)
				SET_VEHICLE_ON_GROUND_PROPERLY(m_AI_FleeCar.iIndex)
				SET_ENTITY_QUATERNION(m_AI_FleeCar.iIndex,m_AI_FleeCar.Qx,m_AI_FleeCar.Qy,m_AI_FleeCar.Qz,m_AI_FleeCar.Qw)
				
				m_AI_FleeCar.mCopsInCar = CREATE_PED_INSIDE_VEHICLE(m_AI_FleeCar.iIndex,PEDTYPE_COP,cop_model,VS_DRIVER)
				GIVE_WEAPON_TO_PED(m_AI_FleeCar.mCopsInCar,WEAPONTYPE_PISTOL,INFINITE_AMMO,FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_AI_FleeCar.mCopsInCar,TRUE)
				CLEAR_PED_TASKS(m_AI_FleeCar.mCopsInCar)
				DEBUG_PRINTSTRING("Flee car created!")
				m_AI_FleeCar.mState = RC_AIC_START_REC
			ENDIF
		BREAK
		CASE RC_AIC_START_REC
			IF IS_VEHICLE_OK(m_AI_FleeCar.iIndex)
				IF IS_ENTITY_ALIVE(m_AI_FleeCar.mCopsInCar)
					IF IS_PED_IN_ANY_VEHICLE(m_AI_FleeCar.mCopsInCar)
						START_PLAYBACK_RECORDED_VEHICLE(m_AI_FleeCar.iIndex, m_AI_FleeCar.iRecNum, m_AI_FleeCar.sRecName)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(m_AI_FleeCar.iIndex)
						DEBUG_PRINTSI("*** m_AI_FleeCar.iRecNum: ", m_AI_FleeCar.iRecNum)
						switch m_AI_FleeCar.iRecNum
							CASE 0	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5)	break
							CASE 1	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1) 	break
							CASE 2	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.4) break
							CASE 3	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5) break
							CASE 4	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5) break
						ENDSWITCH
						DEBUG_PRINTSTRING("Flee car playing!")
						m_AI_FleeCar.mState = RC_AIC_UPDATE_REC
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE RC_AIC_UPDATE_REC
			IF IS_VEHICLE_OK(m_AI_FleeCar.iIndex)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(m_AI_FleeCar.iIndex)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(m_AI_FleeCar.iRecNum,m_AI_FleeCar.sRecName)
						REMOVE_VEHICLE_RECORDING(m_AI_FleeCar.iRecNum,m_AI_FleeCar.sRecName)
						DEBUG_PRINTSTRING("Flee car recording finished!")
						m_AI_FleeCar.mState = RC_AIC_END_REC
					ENDIF
				ELSE
					switch m_AI_FleeCar.iRecNum
						CASE 0	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5) break
						CASE 1	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1) break
						CASE 2	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.4) break
						CASE 3	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5) break
						CASE 4	SET_PLAYBACK_SPEED(m_AI_FleeCar.iIndex,1.5) break
					ENDSWITCH
				ENDIF
			ENDIF
		BREAK
		CASE RC_AIC_END_REC
			IF IS_VEHICLE_OK(m_AI_FleeCar.iIndex)
				IF IS_ENTITY_ALIVE(m_AI_FleeCar.mCopsInCar)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_AI_FleeCar.mCopsInCar,FALSE)
					SAFE_RELEASE_PED(m_AI_FleeCar.mCopsInCar)
				ENDIF
				m_AI_FleeCar.mState = RC_AIC_FINISHED
				SAFE_RELEASE_VEHICLE(m_AI_FleeCar.iIndex)
			ENDIF
			DEBUG_PRINTSTRING("Flee car & ped released!")
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE: Main process to handle Josh's behaviour during the mission
PROC Update_Josh()
		
	SWITCH JoshState
		CASE JOSH_WATCH			
			IF m_Cop_State = CE_ATTACK
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
					IF IS_VEHICLE_OK(viJoshCar)
						IF IS_POSITION_OCCUPIED(<<-1138.421265,298.030731,65.900322>>, 4.0, FALSE, TRUE, FALSE, FALSE, FALSE, viJoshCar)
							DEBUG_PRINTSTRING("Back gate blocked")
							vRC_runToPoint = <<-1130.35, 307.92, 65.19>>
							OPEN_SEQUENCE_TASK(seq)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vRC_runToPoint,PEDMOVE_RUN, DEFAULT, 1.0, ENAV_NO_STOPPING)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-1126.56, 319.85, 65.98>>,PEDMOVE_RUN, DEFAULT, 1.0, ENAV_NO_STOPPING)
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
							CLOSE_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[JOSH], TRUE)
							TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[JOSH], seq)
						ELSE
							DEBUG_PRINTSTRING("Back gate clear")
							OPEN_SEQUENCE_TASK(seq)
								//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vRC_runToPoint,PEDMOVE_RUN, DEFAULT, 1.0, ENAV_NO_STOPPING)
								IF IS_VEHICLE_OK(viJoshCar)
									//TASK_ENTER_VEHICLE(NULL, viJoshCar)
									TASK_VEHICLE_MISSION_PED_TARGET(NULL, viJoshCar, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, 500, 0.1, TRUE)
								ELSE
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[JOSH], TRUE)
							TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[JOSH], seq)
						ENDIF
					ELSE
						DEBUG_PRINTSTRING("Josh car not ok - do Josh flee anyway")
						OPEN_SEQUENCE_TASK(seq)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vRC_runToPoint,PEDMOVE_RUN, DEFAULT, 1.0, ENAV_NO_STOPPING)
							IF IS_VEHICLE_OK(viJoshCar)
								TASK_ENTER_VEHICLE(NULL, viJoshCar)
								TASK_VEHICLE_MISSION_PED_TARGET(NULL, viJoshCar, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, 500, 0.1, TRUE)
							ELSE
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
							ENDIF
						CLOSE_SEQUENCE_TASK(seq)
						SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[JOSH], TRUE)
						TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[JOSH], seq)
					ENDIF
					
					SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[JOSH], FA_USE_VEHICLE, TRUE)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				iJoshFleeConvoTimer = GET_GAME_TIMER()
				SET_PED_CAN_BE_DRAGGED_OUT(sRCLauncherDataLocal.pedID[JOSH], TRUE)
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[JOSH], PCF_PedsJackingMeDontGetIn, TRUE)
				JoshState = JOSH_FLEE
			ENDIF
		BREAK
		
		CASE JOSH_FLEE
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
				// If either of the cops die, make Josh comment about their deaths if the player's close enough
				IF NOT bDoJoshComment
					IF bCopsDead[0] OR bCopsDead[1]
						IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[JOSH],PLAYER_PED_ID(),FALSE) < 20.0
							ADD_PED_FOR_DIALOGUE(jConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
							ADD_PED_FOR_DIALOGUE(jConversation, 4, sRCLauncherDataLocal.pedID[JOSH], "JOSH")
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_ANY_CONVERSATION()
								ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(jConversation, "JOSH4AU", "JOSH4COP3", CONV_PRIORITY_VERY_HIGH)
							ELSE
								CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4COP3", CONV_PRIORITY_VERY_HIGH)
							ENDIF
							DEBUG_PRINTSTRING("Doing Josh comment")
						ELSE
							DEBUG_PRINTSTRING("Too far away to do Josh comment")
						ENDIF
						bDoJoshComment = TRUE
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_OK(viJoshCar)
					// If Josh's car's engine health gets too low, make him flee on foot
					IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[JOSH], viJoshCar, TRUE)
						IF GET_VEHICLE_ENGINE_HEALTH(viJoshCar) < 20
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
							TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[JOSH], 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP|ECF_DONT_CLOSE_DOOR|ECF_JUMP_OUT)
							TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID(), 510, -1)
							SAFE_RELEASE_VEHICLE(viJoshCar)
						ENDIF
					ENDIF
					
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viJoshCar, TRUE)
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
							TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID(), 510, -1)
							SAFE_RELEASE_VEHICLE(viJoshCar)
						ENDIF
					ENDIF
					
					// If Trevor jacks Josh, make him flee on foot
					IF IS_PED_BEING_JACKED(sRCLauncherDataLocal.pedID[JOSH])
						IF bDoneJoshJackingConv = FALSE
							IF CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4BEGS", CONV_PRIORITY_VERY_HIGH)
								bDoneJoshJackingConv = TRUE
							ENDIF
						ENDIF
						CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
						SAFE_RELEASE_VEHICLE(viJoshCar)
						TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID(), 510, -1)
					ENDIF
				ENDIF
				
				// If Josh gets 500m away from Trevor, consider him to have gotten away
				
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH]) > 350
					DEBUG_PRINTSTRING("Deleting Josh, setting as lost")
					SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[JOSH])
					SAFE_DELETE_VEHICLE(viJoshCar)
					JoshState = JOSH_LOST
				ENDIF
				
				// Do Josh's flee dialogue if the player is following them closely in a car
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
					IF iJoshFleeConvoCount < 3
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH]) < 10
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND (GET_GAME_TIMER() - iJoshFleeConvoTimer) > 8000
								IF CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4CHASE", CONV_PRIORITY_VERY_HIGH)
									iJoshFleeConvoCount++
									iJoshFleeConvoTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID())
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 2.0, REPLAY_IMPORTANCE_LOWEST) // Record 2 seconds before Josh died to try and capture the kill
					DEBUG_PRINTSTRING("Player killed Josh!")
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JO4_JOSH_KILLED_BEFORE_ESCAPE)
				ENDIF
				// Josh has died - likely at Trevor's hands
				DEBUG_PRINTSTRING("Josh - DEAD!")
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_ANY_CONVERSATION()
				ENDIF
				IF CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4KILL", CONV_PRIORITY_VERY_HIGH)
					JoshState = JOSH_DEAD
				ENDIF
			ENDIF
		BREAK
		
		CASE JOSH_LOST
			// Nothing to do, Josh got away
		BREAK
		
		CASE JOSH_DEAD
			// Nothing to do, Josh is dead
		BREAK
	ENDSWITCH
	
	
ENDPROC

PROC HANDLE_POLICE_REPORT()
	
	IF bDonePoliceReport = FALSE
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1116.62, 299.12, 64.96>>) > 50.0
			PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JOSH_4_01", 0.0)
			DEBUG_PRINTSTRING("*** Played special Josh 4 police report ***")
			bDonePoliceReport = TRUE
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Handle triggering Trevor's rant to himself as the player escapes
PROC Rant_During_Escape()

	IF iRantProgress = 0
		// Don't trigger Trevor's rant while he's still around the house
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1208.580688,309.849396,80.691895>>, <<-1041.644897,319.581085,60.402634>>, 178.500000)
			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION()
				wait(0)
			ENDWHILE
			ADD_PED_FOR_DIALOGUE(jConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(jConversation, 4, sRCLauncherDataLocal.pedID[JOSH], "JOSH")
			iTrevRantTimer = GET_GAME_TIMER()
			iRantProgress++
		ENDIF
	ELIF IRantProgress = 1
		IF JoshState <> JOSH_WATCH // Make sure Trevor doesn't say any of this if Josh is watching, just in case
			IF (GET_GAME_TIMER() - iTrevRantTimer) > 6000
				IF JoshState = JOSH_DEAD
					// Do the calm "why did I help him" comment if Josh is dead
					CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4CAR2", CONV_PRIORITY_VERY_HIGH)
				ELIF JoshState = JOSH_FLEE
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH]) < 75
							// If the player is <75m away from Josh (thus could potentially be chasing him), play the 'chase down' dialogue
							CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4CAR3", CONV_PRIORITY_VERY_HIGH)
						ELSE
							// Otherwise do the "You can't hide forever" line if Josh is still alive but Trevor may not know where he is
							CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4CAR1", CONV_PRIORITY_VERY_HIGH)
						ENDIF
					ENDIF
				ELIF JoshState = JOSH_LOST
					// Do the "You can't hide forever" line if Josh 'got away'
					CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4CAR1", CONV_PRIORITY_VERY_HIGH)
				ENDIF
				iRantProgress++
			ENDIF
		ENDIF
	ENDIF

ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE: release everything and terminate the script
PROC Script_Cleanup()

	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	CLEAR_PRINTS()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		DEBUG_PRINTSTRING("...Random Character Script was triggered so additional cleanup required")
	ENDIF
	
	INT i
	FOR i=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
			SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[i], TRUE, FALSE)
		ENDIF
	ENDFOR
	
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[JOSH])

	FOR i = 0 to COUNT_OF(sRCLauncherDataLocal.vehID)-1
		SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[i])
	ENDFOR
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_BACK_TO_ORIGINAL(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)
	
	//Release any requested models
	SET_MODEL_AS_NO_LONGER_NEEDED(cop_car_model)
	SET_MODEL_AS_NO_LONGER_NEEDED(cop_model)
					
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

PROC Script_Passed()

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TRIGGER_MUSIC_EVENT("JOSH4_COPS_LOST_RADIO")
			DEBUG_PRINTSTRING("Starting 'JOSH4_COPS_LOST_RADIO'")
		ELSE
			TRIGGER_MUSIC_EVENT("JOSH4_COPS_LOST")
			DEBUG_PRINTSTRING("Starting 'JOSH4_COPS_LOST'")
		ENDIF
	ELSE
		TRIGGER_MUSIC_EVENT("JOSH4_COPS_LOST")
		DEBUG_PRINTSTRING("Player is somehow not alive when passing the mission - starting 'JOSH4_COPS_LOST'")
	ENDIF

	Random_Character_Passed(CP_RAND_C_JOS4)
	Script_Cleanup()
	
ENDPROC

// ===========================================================================================================
//		DEBUG
// ===========================================================================================================
 /// PURPOSE: Reset the scene back to default so we can cleanly play the intro again
PROC Reset_Scene()

	bDone10SecWarning = FALSE
	bCopMove = FALSE
	m_Cop_State = CE_WAIT
	JoshState = JOSH_WATCH
	ConvState = CONV_START
	bDoCopConv = TRUE
	
	bDoJoshComment = FALSE
	bLeadInPlayed = FALSE
	bDonePoliceReport = FALSE
	
	iRantProgress = 0
	
	REQUEST_MODEL(cop_model)
	REQUEST_MODEL(cop_car_model)
	
	//-------------------------------------------------------------
	// Create random character
	//-------------------------------------------------------------	
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[JOSH])
	
	WHILE NOT RC_CREATE_NPC_PED(sRCLauncherDataLocal.pedID[JOSH], CHAR_JOSH, <<-1103.60, 290.81, 64.32>>, -99.24, "JOSH")
		WAIT(0)
	ENDWHILE
	
	WHILE NOT HAS_MODEL_LOADED(cop_model)
		WAIT(0)
	ENDWHILE
	
	// First cop
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[SPEAKER_COP])
	
	sRCLauncherDataLocal.pedID[SPEAKER_COP] = CREATE_PED(PEDTYPE_COP, cop_model, <<-1102.92, 290.12,/*64.25*/INVALID_WORLD_Z>>, 40.46, FALSE, FALSE)
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[SPEAKER_COP],true)
		GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[SPEAKER_COP], WEAPONTYPE_PISTOL, -1, FALSE)
	ENDIF
	
	//Second Cop
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[NOTEPAD_COP])

	sRCLauncherDataLocal.pedID[NOTEPAD_COP] = CREATE_PED(PEDTYPE_COP, cop_model, <<-1102.73, 290.91,/*64.35*/INVALID_WORLD_Z>>, 92.06, FALSE, FALSE)
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[NOTEPAD_COP], TRUE)
		GIVE_WEAPON_TO_PED(sRCLauncherDataLocal.pedID[NOTEPAD_COP], WEAPONTYPE_PISTOL, -1, FALSE)
	ENDIF
	
	// have the models loaded
	WHILE NOT HAS_MODEL_LOADED(cop_car_model)
		WAIT(0)
	ENDWHILE
	
	//Cop car outside gate
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[COP_CAR])
	
	sRCLauncherDataLocal.vehID[COP_CAR] = CREATE_VEHICLE(cop_car_model,<<-1107.50, 280.33, 64.01>>, 97.97)
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[COP_CAR])
		SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[COP_CAR])
	ENDIF

ENDPROC

 /// PURPOSE: Set the mission's new state
PROC SetState(RC_MainState in)
	m_state = in
	m_subState = SS_Setup
ENDPROC

 /// PURPOSE: Jump ahead straight to the Escape The Cops stage
PROC Jump_To_Escape()
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[SPEAKER_COP], <<-1102.48,290.20,64.22>>)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[SPEAKER_COP], 177.74)
	ELSE
		sRCLauncherDataLocal.pedID[SPEAKER_COP] = CREATE_PED(PEDTYPE_COP, cop_model, <<-1102.92, 290.12,/*64.25*/INVALID_WORLD_Z>>, 40.46, FALSE, FALSE)
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[NOTEPAD_COP], <<-1103.94,290.21,64.28>>)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[NOTEPAD_COP], -167.67)
	ELSE
		sRCLauncherDataLocal.pedID[NOTEPAD_COP] = CREATE_PED(PEDTYPE_COP, cop_model, <<-1102.73, 290.91,/*64.35*/INVALID_WORLD_Z>>, 92.06, FALSE, FALSE)
	ENDIF
	
	//Get player into position
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerPosAfterIntro, fPlayerHeadAfterIntro, TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	ENDIF
	
	SetupEndOfIntro(TRUE)
	
	REQUEST_MODEL(JoshCarModel)
	
	WHILE NOT HAS_MODEL_LOADED(JoshCarModel)
		WAIT(0)
	ENDWHILE
	
	SetState(RC_ESCAPE_COPS)
				
	// Return script systems to normal.
	IF NOT IS_REPLAY_BEING_SET_UP()
		RC_END_CUTSCENE_MODE()
	ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
	
	// Going to reset all of these here, to try and ensure that restarting after dying won't make the cops shoot you immediately
	bDone10SecWarning = FALSE
	bCopMove = FALSE
	m_Cop_State = CE_WAIT
	iJoshFleeConvoCount = 0
	
	IF Is_Replay_In_Progress()
		// Do the gate stuff again just in case the replay failed far enough away for these to have not worked
		IF NOT OPEN_FRONT_GATES(FALSE)
			CPRINTLN(DEBUG_MISSION, "Josh4: Front gates say they haven't opened...")
		ELSE
			CPRINTLN(DEBUG_MISSION, "Josh4: Front gates opened")
		ENDIF
		
		IF NOT CLOSE_REAR_GATES()
			CPRINTLN(DEBUG_MISSION, "Josh4: Rear gates say they haven't closed...")
		ELSE
			CPRINTLN(DEBUG_MISSION, "Josh4: Rear gates closed")
		ENDIF
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP()
		ENDIF
		RC_END_Z_SKIP()
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			ENDIF
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			ENDIF
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
			ENDIF
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			IF m_state = RC_INTRO
				Jump_To_Escape()
			ELSE
				Script_Passed()
			ENDIF
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			Reset_Scene()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			// We're going to run the intro cutscene again
			SetState(RC_INTRO)
		ENDIF
		
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, menu_option)
			IF menu_option = 2
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
					APPLY_DAMAGE_TO_PED(sRCLauncherDataLocal.pedID[JOSH], 1000, TRUE)
				ENDIF
				EXIT
			ENDIF
			RC_START_Z_SKIP()
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			IF menu_option = 0
				Reset_Scene()
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				// We're going to run the intro cutscene again
				SetState(RC_INTRO)
			ELIF menu_option = 1
				Jump_To_Escape()
			ENDIF
			RC_END_Z_SKIP()
		ENDIF
	ENDPROC
#ENDIF

PROC STATE_Leadin()

	// Disable controls and exit current vehicle
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	
	// If we re-requested the cutscene (we probably did), this will ensure the cop variations get set properly
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			DEBUG_PRINTSTRING("Trying to set Cop 1 component variation (leadin loop)")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_standing_idle", sRCLauncherDataLocal.pedID[SPEAKER_COP])
		ENDIF
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			DEBUG_PRINTSTRING("Trying to set Cop 2 component variation (leadin loop)")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_with_notepad", sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		ENDIF
	ENDIF
	
	SWITCH m_SubState
		CASE SS_Setup
			IF NOT IS_REPEAT_PLAY_ACTIVE()
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1107.352905,281.626038,61.464512>>, <<-1099.520508,282.507324,65.992226>>, 16.0)
				DEBUG_PRINTSTRING("Doing setup for leadin")
				ADD_PED_FOR_DIALOGUE(jConversation, 4, sRCLauncherDataLocal.pedID[JOSH], "JOSH")
				ADD_PED_FOR_DIALOGUE(jConversation, 2, PLAYER_PED_ID(), "TREVOR")
				REQUEST_ANIM_DICT("rcmjosh4")
				IF HAS_ANIM_DICT_LOADED("rcmjosh4")
					m_SubState = SS_Update
				ENDIF
			ELSE
				DEBUG_PRINTSTRING("Skipping setup for leadin (repeat play/wrong area)")
				SetState(RC_INTRO)
			ENDIF
		BREAK
		
		CASE SS_Update
			IF NOT IS_REPEAT_PLAY_ACTIVE() // Don't do the leadin if we're on a repeat-play, just go into the cutscene
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1107.352905,281.626038,61.464512>>, <<-1099.520508,282.507324,65.992226>>, 16.0)
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1105.570923,290.008026,62.871758>>, <<-1100.577393,290.429840,66.452118>>, 4.50)
						IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
								TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH])
							ENDIF
						ELSE
							RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
				IF NOT bLeadInPlayed
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
						IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4","beckon_a_josh") // Make sure Josh is playing the idle anim
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() // Allow the conversation line to end before playing the lead-in
							//IF GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "beckon_a_josh") <0.02 // Only start playing the leadin anims when Josh won't pop
								TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "josh_4_leadin_josh", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
								DEBUG_PRINTSTRING("Done Josh leadin anim")
								IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
									TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[NOTEPAD_COP], "rcmjosh4", "josh_4_leadin_cop_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									DEBUG_PRINTSTRING("Done Notepad cop leadin anim")
								ENDIF
								IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[SPEAKER_COP])
									TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[SPEAKER_COP], "rcmjosh4", "josh_4_leadin_cop_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									DEBUG_PRINTSTRING("Done Speaker cop leadin anim")
								ENDIF
								REMOVE_CUTSCENE()
								REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("JOSH_4_INT_CONCAT", CS_SECTION_2) // Request the concatinated version of the cutscene for the leadin instead
								bLeadInPlayed = TRUE
							//ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
					AND NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH])
						ENDIF
					ENDIF
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
						IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "josh_4_leadin_josh")
							// Comment back in when lead-in convo has been made
							IF GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "josh_4_leadin_josh") > 0.276
								IF bLeadinConvo = FALSE
									IF CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH_4_INTLI", CONV_PRIORITY_VERY_HIGH)
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 3, REPLAY_IMPORTANCE_LOW) 
										bLeadinConvo = TRUE
									ENDIF
								ENDIF
							ENDIF
							IF GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[JOSH], "rcmjosh4", "josh_4_leadin_josh") > 0.98
								SetState(RC_INTRO)
							ENDIF
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH]) <= 2.0
								// Kill the leadin convo early if it's started
								IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
								DEBUG_PRINTSTRING("Player too close to Josh, starting cutscene early") 
								SetState(RC_INTRO)
							ENDIF
						ELSE
							SetState(RC_INTRO)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SetState(RC_INTRO)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE: Main intro cutscene state
PROC STATE_IntroCutscene()

	HANDLE_MUSIC()
	
	// If we re-requested the cutscene (we probably did), this will ensure the cop variations get set properly
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
			DEBUG_PRINTSTRING("Trying to set Cop 1 component variation (intro loop)")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_standing_idle", sRCLauncherDataLocal.pedID[SPEAKER_COP])
		ENDIF
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
			DEBUG_PRINTSTRING("Trying to set Cop 2 component variation (intro loop)")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_with_notepad", sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		ENDIF
	ENDIF

	SWITCH m_subState
		CASE SS_Setup
			IF NOT ARE_STRINGS_EQUAL(g_CutsceneName , "")
				IF bLeadInPlayed = FALSE // If we haven't played the lead-in, just make sure the full cutscene is definitely loaded
					RC_REQUEST_CUTSCENE(g_CutsceneName)
				ENDIF
				iCutsceneProgress = 0
				m_subState = SS_Update
			ELSE
				SetupEndOfIntro()
				SetState(RC_ESCAPE_COPS)
			ENDIF
		BREAK
		CASE SS_Update
			SWITCH iCutsceneProgress
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						
						// Register cutscene entities
						SetupStartOfIntro()
						
						// Cleanup launcher which will remove lead-in blip
						RC_CLEANUP_LAUNCHER()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						// Showtime!
						START_CUTSCENE()
						WAIT(0)
						
						VEHICLE_INDEX veh
						veh = GET_PLAYERS_LAST_VEHICLE()
						
						VECTOR vMoveToLoc
						FLOAT fHead
						IF IS_VEHICLE_OK(veh)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh))
								vMoveToLoc = <<-1089.27, 283.76, 64.22>>
								fHead = 132.53
							ELIF IS_ENTITY_IN_ANGLED_AREA(veh, <<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.000000)
								vMoveToLoc = <<-1101.694336,280.501923,63.383522>>
								fHead = 85.45
							ELSE
								vMoveToLoc = << -1095.2307, 279.3389, 62.7919 >>
								fHead = 84.67
							ENDIF
						ELSE
							// Default coords/heading if the vehicle is fucked, just do whatever start cutscene mode does by default
							vMoveToLoc = << -1095.2307, 279.3389, 62.7919 >>
							fHead = 84.67
						ENDIF
						IF IS_VEHICLE_OK(veh)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(veh)) // Special case for helis (B*1341936)
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.0,
									vMoveToLoc, fHead, <<50, 50, 50>>)
							ELSE
								RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.0,
									vMoveToLoc, fHead, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
							ENDIF
						ELSE
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.0,
								vMoveToLoc, fHead, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
						ENDIF
						RC_START_CUTSCENE_MODE(<< -1104.93, 291.25, 64.30 >>,TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
						IF IS_VEHICLE_OK(viCopCar)
							SET_ENTITY_COORDS(viCopCar, <<-1107.50, 280.33, 64.01>>)
							SET_ENTITY_HEADING(viCopCar, 97.97)
							SET_VEHICLE_ON_GROUND_PROPERLY(viCopCar)
						ENDIF
						iCutsceneProgress++
					ENDIF
				BREAK
				
				CASE 1
					REQUEST_ANIM_DICT("rcmjosh4")
					REQUEST_ANIM_DICT("combat@gestures@pistol@halt")
					REQUEST_ANIM_DICT("combat@gestures@pistol@glances")
					REQUEST_MODEL(JoshCarModel)
					iCutsceneProgress++
				BREAK
				
				CASE 2
					IF HAS_ANIM_DICT_LOADED("rcmjosh4")
					AND HAS_ANIM_DICT_LOADED("combat@gestures@pistol@halt")
					AND HAS_ANIM_DICT_LOADED("combat@gestures@pistol@glances")
					AND HAS_MODEL_LOADED(JoshCarModel)
						iCutsceneProgress++
					ENDIF
				BREAK
				
				CASE 3
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					
					IF DO_TREVOR_EXIT()
						
						REPLAY_STOP_EVENT()
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_LOWEST)
						iCutsceneProgress = 4
					ENDIF
					
					GrabCop()
					
					IF WAS_CUTSCENE_SKIPPED()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						bCutsceneSkipped = TRUE
						iCutsceneProgress = 4
						IF NOT bDoneMusicCutscene
							TRIGGER_MUSIC_EVENT("JOSH4_START")
							DEBUG_PRINTSTRING("Starting 'JOSH4_START' via cutscene skip")
							bDoneMusicCutscene = TRUE
						ENDIF
					ENDIF
				BREAK
				
				CASE 4
					GrabCop()
					DO_TREVOR_EXIT()
					
					IF WAS_CUTSCENE_SKIPPED()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						bCutsceneSkipped = TRUE
						IF NOT bDoneMusicCutscene
							TRIGGER_MUSIC_EVENT("JOSH4_START")
							DEBUG_PRINTSTRING("Starting 'JOSH4_START' via cutscene skip")
							bDoneMusicCutscene = TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_CUTSCENE_PLAYING()
						SetupEndOfIntro(bCutsceneSkipped)
						
						// Return script systems to normal.
						RC_END_CUTSCENE_MODE()
						
						ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
						ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
						ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
						m_subState = SS_Cleanup
					ENDIF
				BREAK
				
				DEFAULT
					iCutsceneProgress = 0
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SS_Cleanup
			SetState(RC_ESCAPE_COPS)
		BREAK
	ENDSWITCH
ENDPROC

PROC Update_Conversations()

	IF NOT bDone10SecWarning
		ADD_PED_FOR_DIALOGUE(cConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
		ADD_PED_FOR_DIALOGUE(cConversation, 3, sRCLauncherDataLocal.pedID[SPEAKER_COP], "JOSHCOP")
		CREATE_CONVERSATION(cConversation, "JOSH4AU", "JOSH4GIVEUP", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
		bDone10SecWarning = TRUE // don't run this anymore
	ELSE
		SWITCH ConvState
			
			CASE CONV_PAUSE
				IF (GET_GAME_TIMER() - iDialogueTimer) > 1000
					ConvState = CONV_START
				ENDIF
			BREAK
			
			CASE CONV_START
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF bDoCopConv
						ADD_PED_FOR_DIALOGUE(cConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
						ADD_PED_FOR_DIALOGUE(cConversation, 3, sRCLauncherDataLocal.pedID[SPEAKER_COP], "JOSHCOP")
						IF CREATE_CONVERSATION(cConversation, "JOSH4AU", "JOSH4COP1", CONV_PRIORITY_HIGH)
							ConvState = CONV_PLAYING
							bDoCopConv = FALSE
						ENDIF
					ELSE
						ADD_PED_FOR_DIALOGUE(jConversation, ENUM_TO_INT(CHAR_TREVOR), PLAYER_PED_ID(), "TREVOR")
						ADD_PED_FOR_DIALOGUE(jConversation, 4, sRCLauncherDataLocal.pedID[JOSH], "JOSH")	
						IF CREATE_CONVERSATION(jConversation, "JOSH4AU", "JOSH4JOSH", CONV_PRIORITY_LOW)
							ConvState = CONV_PLAYING
							bDoCopConv = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE CONV_PLAYING
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iDialogueTimer = GET_GAME_TIMER()
					ConvState = CONV_PAUSE
				ENDIF
			BREAK

			CASE CONV_STOP
				// Do nothing, we want to stop the conversation
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE: Main process for the 'escape the cops' state (and more or less the only proper gameplay state for the mission)
PROC STATE_EscapeCops()

	HANDLE_MUSIC()

	SWITCH m_subState
		
		CASE SS_Setup
			
			// Ensure no entities are bulletproof...
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			IF IS_ENTITY_ALIVE(viCopCar) SET_ENTITY_PROOFS(viCopCar, FALSE, FALSE, FALSE, FALSE, FALSE) ENDIF
			IF IS_ENTITY_ALIVE(viJoshCar) SET_ENTITY_PROOFS(viJoshCar, FALSE, FALSE, FALSE, FALSE, FALSE) ENDIF

			PRINT_NOW("JOSH4_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)	//Using global text
			SET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX(),PLAYER_WANTED_LEVEL)
			SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
			SET_PED_NON_CREATION_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
			SET_PED_PATHS_IN_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>, FALSE)
			CLEAR_AREA_OF_PEDS(<<-1114.787842,319.340149,66.122597>>, 20.5)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
				TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID(), -1)
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			// Tell both cops to aim at the player
//			int i
//			for i=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
//				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
//					TASK_AIM_GUN_AT_ENTITY(sRCLauncherDataLocal.pedID[i], PLAYER_PED_ID(), -1, TRUE)
//				ENDIF
//			ENDFOR
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_START, "Escape cops")
			iShootTimer = GET_GAME_TIMER()
			m_subState = SS_Update
		BREAK 

		CASE SS_Update
			Update_Conversations()
			Update_GateCops()
			Update_AI_CopCars()
			Update_Josh()
			Rant_During_Escape()
			HANDLE_POLICE_REPORT()
			
			IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) <= 0
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() // Because we don't want Trevor's rant to himself to cut off if you lose your wanted level as it plays
				INT i
				for i=1 to COUNT_OF(sRCLauncherDataLocal.pedID)-1
					IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[i])
						SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[i], FALSE, TRUE)
					ENDIF
				ENDFOR
				IF IS_VEHICLE_OK(viCopCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar)
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JO4_ESCAPE_IN_PARKED_COP_CAR) 
					ENDIF
				ENDIF
				m_subState = SS_Cleanup
			ENDIF
		BREAK

		CASE SS_Cleanup
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)

	DEBUG_PRINTSTRING("*** Now in Josh 4 loop ***")
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		TRIGGER_MUSIC_EVENT("JOSH4_MISSION_FAIL")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		DEBUG_PRINTSTRING("*** Setting up initial scene for replay")
      	START_REPLAY_SETUP(vPlayerPosAfterIntro, fPlayerHeadAfterIntro)
		DEBUG_PRINTSTRING("*** Replay setup started")
		g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_JOSH_4(sRCLauncherDataLocal)
	  		WAIT(0)
		ENDWHILE
		DEBUG_PRINTSTRING("*** Josh 4 scene set up")
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[SPEAKER_COP])
		STOP_PED_SPEAKING(sRCLauncherDataLocal.pedID[SPEAKER_COP], TRUE)
	ENDIF
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[NOTEPAD_COP])
		STOP_PED_SPEAKING(sRCLauncherDataLocal.pedID[NOTEPAD_COP], TRUE)
	ENDIF
	viCopCar = sRCLauncherDataLocal.vehID[COP_CAR]
	viJoshCar = sRCLauncherDataLocal.vehID[JOSH_CAR]
	
	DEBUG_PRINTSTRING("*** About to set up positions...")
	
	SetupPositions()
	
	#IF IS_DEBUG_BUILD
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DEBUG_PRINTSTRING("*** Setting up Josh 4 RAG widgets")
			widgetGroup= START_WIDGET_GROUP("Josh 4 widgets")
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)        
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF

	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH, TREVOR_BOOK, FALSE)
	
	IF Is_Replay_In_Progress()
		DEBUG_PRINTSTRING("*** Creating replay vehicle and jumping to post-cutscene")
		CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-1100.04, 280.60, 63.53>>, 267.48, FALSE, FALSE, TRUE, TRUE, FALSE) 
		TRIGGER_MUSIC_EVENT("JOSH4_RESTART1")
		Jump_To_Escape()
	ENDIF

	WHILE(TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_BOC")
		
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Loop within here until the mission passes or fails
		SWITCH(m_state)
			CASE RC_LEADIN
				STATE_Leadin()
			BREAK
			CASE RC_INTRO
				STATE_IntroCutscene()
			BREAK
			CASE RC_ESCAPE_COPS
				STATE_EscapeCops()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

