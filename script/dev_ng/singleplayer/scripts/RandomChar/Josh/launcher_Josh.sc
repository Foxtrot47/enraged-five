

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Josh.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Josh.sc
//
//		AUTHOR		:	David Roberts / Andrew Minghella
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
//
// 						If multiple missions share same coord or are within close proximity,
//						we should just add one world point, and adjust the tolerance float.
// *****************************************************************************************
// Enums
ENUM JOSH_CONV_STATE
	JOSH_CONV_WAITING,
	JOSH_CONV_DOLINE,
	JOSH_CONV_SPEAKING
ENDENUM

// Constants
CONST_FLOAT WORLD_POINT_COORD_TOLERANCE 1.0

// Variables
INT 					  iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST // ID to register off-mission cutscene load request with cutscene_controller.
JOSH_CONV_STATE 		  JoshConv = JOSH_CONV_WAITING
structPedsForConversation ConvStruct

INT	iConvTimer
BOOL bJoshConvDone = FALSE
BLIP_INDEX biLeadIn

BOOL bVariationsSet = FALSE


// ----------Functions -------------------------
/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
/// PARAMS:
///    sData - launcher data struct
///    bCleanupEntities - do we want to cleanup the entities in the launcher struct
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)
	
	IF bCleanupEntities
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Cleaning up entities in Launcher")
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	// Remove lead-in blip
	IF DOES_BLIP_EXIST(biLeadIn)
		REMOVE_BLIP(biLeadIn)
	ENDIF

	// Unload launcher animation dictionary
	REMOVE_LAUNCHER_ANIM_DICT(sData.sAnims)
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF
	
	// Stop launcher conversation
	STRING sConversationRoot 
	SWITCH sData.eMissionID
		CASE RC_JOSH_4
			sConversationRoot = "JOSH4_AMB"
		BREAK
	ENDSWITCH
	RC_STOP_LAUNCHER_DIALOGUE(sConversationRoot)
	
	// Josh 4 uses CREATE_FORCED_OBJECT for the right gate - this must be cleaned up if the launcher terminates
	SWITCH sData.eMissionID
		CASE RC_JOSH_4
			REMOVE_FORCED_OBJECT(<<-1101.62, 290.36, 64.76>>, 5, prop_lrggate_01c_r)
		BREAK
	ENDSWITCH
	
	//B*1574385 - Force update to blip in RandChar Controller if mission wasn't launched
	IF bCleanupEntities
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Make player car stop and player get out if near to where Josh will walk using his synched scene anims - Same as Kev's function below for Josh 2
FUNC BOOL SPECIAL_JOSH2_LAUNCH(g_structRCScriptArgs& sData)
	
	IF sData.eMissionID = RC_JOSH_2
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<566.74719, -1771.87500, 28.35786>>) < 9
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PRINT_LAUNCHER_DEBUG("Special launching Josh 2")
				RETURN TRUE
			ELSE
				IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 4)
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Make player car stop and player get out if near to where Josh will walk using his synched scene anims - see B*1391178
FUNC BOOL SPECIAL_JOSH3_LAUNCH(g_structRCScriptArgs& sData)
	
	IF sData.eMissionID = RC_JOSH_3
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<566.74719, -1771.87500, 28.35786>>) < 6
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PRINT_LAUNCHER_DEBUG("Special launching Josh 3")
				RETURN TRUE
			ELSE
				IF NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				AND IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				AND BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 1)
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If the Josh 4 conversation finishes, we make the mission launch prematurely even if the player is in a car
/// RETURNS:
///    TRUE if we should special launch Josh
FUNC BOOL SPECIAL_JOSH4_LAUNCH(g_structRCScriptArgs& sData)
	
	IF sData.eMissionID = RC_JOSH_4
		IF IS_ENTITY_ALIVE(sData.pedID[0])
			IF bJoshConvDone
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sData.pedID[0]) <= 15
						PRINT_LAUNCHER_DEBUG("Special launching Josh 4 (not in vehicle)")
						RETURN TRUE // The conversation is done and the player is close enough for Josh to notice them
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sData.pedID[0]) <= 10
						PRINT_LAUNCHER_DEBUG("Special launching Josh 4 (in vehicle)")
						RETURN TRUE // The conversation is done and the player is close enough for Josh to notice them
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_JOSH4_AMB()
	SWITCH JoshConv
		CASE JOSH_CONV_WAITING
			IF (GET_GAME_TIMER() - iConvTimer) > 100
			AND bJoshConvDone = FALSE
				JoshConv = JOSH_CONV_DOLINE
			ENDIF
		BREAK
		CASE JOSH_CONV_DOLINE
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1103.44, 290.18, 63.28>>) <= 20.0
				IF CREATE_CONVERSATION(ConvStruct, "JOSH4AU", "JOSH4_AMB", CONV_PRIORITY_HIGH)
					JoshConv = JOSH_CONV_SPEAKING
				ENDIF
			ENDIF
		BREAK
		CASE JOSH_CONV_SPEAKING
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iConvTimer = GET_GAME_TIMER()
				JoshConv = JOSH_CONV_WAITING
				bJoshConvDone = TRUE
				PRINT_LAUNCHER_DEBUG("Josh 4 conversation finished and will not loop")
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1103.44, 290.18, 63.28>>) >= 25.0
				KILL_ANY_CONVERSATION()
				bJoshConvDone = TRUE
				JoshConv = JOSH_CONV_WAITING
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Creates the initial scene
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)

	//Setup the correct initial scene
	SWITCH sData.eMissionID
		CASE RC_JOSH_1
			IF NOT SetupScene_JOSH_1(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_JOSH_2
			IF NOT SetupScene_JOSH_2(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_JOSH_3
			IF NOT SetupScene_JOSH_3(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_JOSH_4
			IF NOT SetupScene_JOSH_4(sData)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)

	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs 	sRCLauncherData			// Scene information to pass to mission script
	VECTOR				 	vInCoords = <<0,0,0>>	// Stores world point location
	
	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update world point
	vInCoords = in_coords.vec_coord[0]

	// Determine which RC mission we are attempting to launch (currently done on a launcher by launcher basis)
	g_eRC_MissionIDs eRCMissions[4]
	eRCMissions[0] = RC_JOSH_1
	eRCMissions[1] = RC_JOSH_2
	eRCMissions[2] = RC_JOSH_3
	eRCMissions[3] = RC_JOSH_4
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF

	//Pick which mission activated us
	IF NOT DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vInCoords, WORLD_POINT_COORD_TOLERANCE)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF

	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF	
	
	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE

	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vInCoords, sRCLauncherData.activationRange, TRUE)
	
	IF sRCLauncherData.eMissionID = RC_JOSH_1
		ADD_PED_FOR_DIALOGUE(ConvStruct, 5, sRCLauncherData.pedID[0], "JOSH")
	ELIF sRCLauncherData.eMissionID = RC_JOSH_4
		ADD_PED_FOR_DIALOGUE(ConvStruct, 4, sRCLauncherData.pedID[0], "JOSH")
		ADD_PED_FOR_DIALOGUE(ConvStruct, 3, sRCLauncherData.pedID[1], "JOSHCOP")
		iConvTimer = GET_GAME_TIMER()
	ENDIF
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		WAIT(0)
		
		//Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
			IF sRCLauncherData.eMissionID = RC_JOSH_4 	// For B*968838 Cops attack player and Josh runs off
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1102.72412, 290.14630, 63.23002>>) < 200
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),1)
					MAKE_ALL_INITIAL_SCENE_PEDS_ATTACK_PLAYER(sRCLauncherData)
					PRINT_LAUNCHER_DEBUG("Making Josh cops attack")
				ENDIF
			ENDIF
			Script_Cleanup(sRCLauncherData)
		ENDIF
		
		//Update launcher blip + preload intro
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vInCoords)
		
		IF sRCLauncherData.eMissionID = RC_JOSH_1
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "JOSH1AU", "JOSH1_AMB1", 5, "JOSH", DEFAULT, 14)
		ELIF sRCLauncherData.eMissionID = RC_JOSH_2
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "JOSH2AU", "JOSH2_AMB", 5, "JOSH")
		ELIF sRCLauncherData.eMissionID = RC_JOSH_3
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "JOSH3AU", "JOSH3_AMB", 3, "JOSH")
		ELIF sRCLauncherData.eMissionID = RC_JOSH_4
			IF bVariationsSet = FALSE
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
						PRINT_LAUNCHER_DEBUG("Josh 4: Trying to set Cop 1 component variation")
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_standing_idle", sRCLauncherData.pedID[1])
					ENDIF
					IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[2])
						PRINT_LAUNCHER_DEBUG("Josh 4: Trying to set Cop 2 component variation")
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cop_with_notepad", sRCLauncherData.pedID[2])
					ENDIF
					bVariationsSet = TRUE
				ENDIF
			ENDIF
			DO_JOSH4_AMB()
		ENDIF
		
		//Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)
		OR SPECIAL_JOSH2_LAUNCH(sRCLauncherData)
		OR SPECIAL_JOSH3_LAUNCH(sRCLauncherData)
		OR SPECIAL_JOSH4_LAUNCH(sRCLauncherData)
			
			// Create mission blip that persists for lead-in scene
			// Original will be removed when RC controller shuts down
			IF DOES_RC_MISSION_HAVE_LEAD_IN(sRCLauncherData.eMissionID)
				CREATE_BLIP_FOR_LEAD_IN(sRCLauncherData.eMissionID, biLeadIn)
			ENDIF
			
			//Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			//Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				Script_Cleanup(sRCLauncherData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
			ENDIF	
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

