
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "initial_scenes_Josh.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Josh1.sc
//		AUTHOR			:	David Roberts/Ian Gander
//		DESCRIPTION		:	Trevor meets Josh outside his house
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM eRC_MainState
	RC_LEADIN = 0,
	RC_MEET_JOSH,
	RC_LEADOUT
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

CONST_INT		HELP_TIMER 8000
CONST_INT		JOSH 0

// Mission state
eRC_MainState             m_eState = RC_LEADIN
eRC_SubState              m_eSubState  = SS_SETUP

SEQUENCE_INDEX	seq
OBJECT_INDEX oLeftGate

//OBJECT_INDEX	oiPhone

INT iLeadInSynchSceneID
INT iLeadOutSynchSceneID


VECTOR vPos_SynchScene = <<-1106.999, 288.96, 63.40>>
VECTOR vHeading_SynchScene = <<0, 0, 12.060>>

BOOL bLeadInPlayed = FALSE
BOOL bLeadInConvo = FALSE

BOOL bSkipped = FALSE
INT iSkipTimer

BOOL bLeadOutPlayed = FALSE
BOOL bLeadOutConvo = FALSE
BOOL bLeadOutCancelled = FALSE

//FLOAT fHintFov = 25.0
//FLOAT fHintFollow = 0.35
//FLOAT fHintPitchOrbit = 0.000
//FLOAT fHintSide = -0.01
//FLOAT fHintVert = 0.050

structPedsForConversation pedsForConversation
REL_GROUP_HASH            RelGroupBuddy


// ===========================================================================================================
//		Debug prints
// ===========================================================================================================

/// PURPOSE: Print a string to the console
PROC DEBUG_PRINTSTRING(STRING s)

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, s)
	#ENDIF
	
	// Stop release compile error
	s = s

ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()
	
	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		DEBUG_PRINTSTRING("...Random Character Script was triggered so additional cleanup required")
	ENDIF

	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[JOSH], TRUE, TRUE)
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal,TRUE)
	
	// Reenable the paths and navmesh we disabled in the initial scene
	SET_PED_PATHS_IN_AREA(<<-1112.67, 287.38, 62.85>>, <<-1096.41, 297.50, 65.59>>, TRUE)
	DISABLE_NAVMESH_IN_AREA(<<-1112.67, 287.38, 62.85>>, <<-1096.41, 297.50, 65.59>>, FALSE)

//	NEW_LOAD_SCENE_STOP()
	DESTROY_ALL_CAMS()
	
	TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

PROC Script_Passed()

	//For Sale sign help now displayed by the RC controller to ensure poll the 
	//flow queue in a safe way. See Do_Josh1_For_Sale_Signs_Help() - BenR 

	// Add contact and register text message
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH, TREVOR_BOOK)
	WHILE NOT REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_JOSH_1, CT_END_OF_MISSION, BIT_TREVOR, CHAR_JOSH, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME, VID_RC_JOSH_HOUSE, DEFAULT, DEFAULT, COMM_BIT_TXTMSG_LOCKED|COMM_BIT_TXTMSG_NOT_CRITICAL)
		WAIT(0)
	ENDWHILE
	
	// Passed!
	Random_Character_Passed()
	Script_Cleanup()
	
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(FALSE, 0)
			ENDIF
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CLEAR_HELP()
			CLEAR_PRINTS()
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
			
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE: Sets the new mission state and initialises the substate.
PROC SetState(eRC_MainState in)
	
	// Setup new mission state
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

PROC STATE_LeadIn()

	// Disable controls and exit current vehicle
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		DEBUG_PRINTSTRING("Trying to set Josh component variation")
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Josh", sRCLauncherDataLocal.pedID[JOSH])
		ENDIF
	ENDIF

	SWITCH m_eSubState
		
		CASE SS_SETUP
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1115.070679,279.658112,62.421844>>, <<-1092.428467,284.139832,67.316521>>, 15.75)
				DEBUG_PRINTSTRING("Doing setup for leadin")
				ADD_PED_FOR_DIALOGUE(pedsForConversation, 2, PLAYER_PED_ID(), "Trevor")
				ADD_PED_FOR_DIALOGUE(pedsForConversation, 5, sRCLauncherDataLocal.pedID[JOSH], "Josh")
				
				REQUEST_ANIM_DICT("rcmjosh1leadinout")
				
				IF HAS_ANIM_DICT_LOADED("rcmjosh1leadinout") // Loops here until anims are loaded
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, FALSE, 0.0)
					ENDIF
					oLeftGate = GET_CLOSEST_OBJECT_OF_TYPE(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
					//RC_REQUEST_CUTSCENE("josh_1_int", TRUE)
					RC_PRE_REQUEST_CUTSCENE(TRUE)
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("JOSH_1_INT_CONCAT", CS_SECTION_2|CS_SECTION_3|CS_SECTION_4)
					m_eSubState = SS_UPDATE
				ENDIF
			ELSE
				DEBUG_PRINTSTRING("Skipping setup for leadin (wrong area)")
				REQUEST_ANIM_DICT("rcmjosh1leadinout")
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, FALSE, 0.0)
				ENDIF
				oLeftGate = GET_CLOSEST_OBJECT_OF_TYPE(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF NOT IS_REPEAT_PLAY_ACTIVE() // Don't do the leadin if we're on a repeat-play, just go into the cutscene
			
//				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
//				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
//					SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[JOSH], <<-1.0,0,0>>, FALSE, 30000)
//					SET_GAMEPLAY_HINT_FOV(fHintFov)
//					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
//					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
//					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
//					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
//					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
//					AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1104.6227, 289.0283, 63.2468>>, PEDMOVEBLENDRATIO_WALK)
//							TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[JOSH])
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//						CLEAR_SEQUENCE_TASK(seq)
//						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//					ENDIF
//				ELSE
//					STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
//				ENDIF
				IF NOT bLeadInPlayed
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1115.070679,279.658112,62.421844>>, <<-1092.428467,284.139832,67.316521>>, 15.75)
						IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
						AND DOES_ENTITY_EXIST(oLeftGate)
							iLeadInSynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPos_SynchScene, vHeading_SynchScene)
							TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[JOSH], iLeadInSynchSceneID, "rcmjosh1leadinout", "leadin_josh", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
								SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, iLeadInSynchSceneID, "leadin_gate", "rcmjosh1leadinout", NORMAL_BLEND_IN)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[JOSH], -1)
							TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID(), 4000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_PITCH_LIMIT|SLF_EXTEND_YAW_LIMIT)
							DEBUG_PRINTSTRING("Done Josh leadin anim")
							bLeadInPlayed = TRUE
						ENDIF
					ELSE
						IF NOT IS_REPEAT_PLAY_ACTIVE()
						AND NOT bLeadInPlayed
							DEBUG_PRINTSTRING("Cannot play lead-in, re-requesting cutscene with new sections")
							REMOVE_CUTSCENE()
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("JOSH_1_INT_CONCAT", CS_SECTION_1|CS_SECTION_3|CS_SECTION_4)
						ENDIF
						DEBUG_PRINTSTRING("Replay in progress, skipping leadin")
						m_eSubState = SS_CLEANUP
					ENDIF
				ELSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadInSynchSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadInSynchSceneID) > 0.15
							IF NOT bLeadInConvo
								IF CREATE_CONVERSATION(pedsForConversation, "JOSH1AU", "JOSH1_LEADIN", CONV_PRIORITY_HIGH)
									DEBUG_PRINTSTRING("Leadin convo done")
									bLeadInConvo = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadInSynchSceneID) > 0.9
							DEBUG_PRINTSTRING("Leadin anim done, launch cutscene")
							m_eSubState = SS_CLEANUP
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[JOSH]) <= 2.5
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
					ELSE
						DEBUG_PRINTSTRING("Leadin not playing, launch cutscene")
						m_eSubState = SS_CLEANUP
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_REPEAT_PLAY_ACTIVE()
				AND NOT bLeadInPlayed
					DEBUG_PRINTSTRING("Cannot play lead-in, re-requesting cutscene with new sections")
					REMOVE_CUTSCENE()
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("JOSH_1_INT_CONCAT", CS_SECTION_1|CS_SECTION_3|CS_SECTION_4)
				ENDIF
				DEBUG_PRINTSTRING("Replay in progress, skipping leadin")
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			m_eState = RC_MEET_JOSH
			m_eSubState = SS_SETUP
		BREAK
		
	ENDSWITCH

ENDPROC


/// PURPOSE: Triggers the mocap cutscene, then the scripted cutscene showing the For Sale sign example, and completes mission once finished
PROC STATE_MeetJosh()

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		DEBUG_PRINTSTRING("Trying to set Josh component variation")
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Josh", sRCLauncherDataLocal.pedID[JOSH])
		ENDIF
	ENDIF

	SWITCH m_eSubState
		
		CASE SS_SETUP

			IF RC_IS_CUTSCENE_OK_TO_START()
			
				REQUEST_ANIM_DICT("rcmjosh1@impatient")
				REQUEST_VEHICLE_RECORDING(500, "Josh1Driveaway")
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[JOSH], "Josh", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				SET_ROADS_IN_ANGLED_AREA(<<-1140.850098,263.471375,63.357758>>, <<-1205.440308,412.157379,78.258896>>, 43.750000, FALSE, FALSE)
				CLEAR_AREA_OF_VEHICLES(<<-1146.306030,295.983124,66.136116>>, 9.0)
				CLEAR_AREA_OF_VEHICLES(<<-1143.244385,280.276215,65.418922>>, 12.0)
				CLEAR_AREA_OF_VEHICLES(<<-1152.426025,313.615723,67.104294>> , 12.0)
				CLEAR_AREA_OF_VEHICLES(<<-1165.965576,332.678589,68.809998>> , 12.0)
				CLEAR_AREA_OF_VEHICLES(<<-1178.728760,346.457245,70.046745>>, 12.0)
				CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1140.850098,263.471375,63.357758>>, <<-1205.440308,412.157379,78.258896>>, 43.750000)
				
				VEHICLE_INDEX veh
				veh = GET_PLAYERS_LAST_VEHICLE()
				VECTOR vMoveToLoc
				FLOAT fHead
				IF IS_VEHICLE_OK(veh)
					IF IS_ENTITY_IN_ANGLED_AREA(veh, <<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.000000)
						vMoveToLoc = <<-1098.87, 280.88, 63.47>>
						fHead = 85.98
					ELSE
						vMoveToLoc = << -1095.2307, 279.3389, 62.7919 >>
						fHead = 84.67
					ENDIF
				ELSE
					// Default coords/heading if the vehicle is fucked, just do whatever start cutscene mode does by default
					vMoveToLoc = << -1095.2307, 279.3389, 62.7919 >>
					fHead = 84.67
				ENDIF
		
				// Cleanup launcher which will remove lead-in blip
				RC_CLEANUP_LAUNCHER()
				
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_LOWEST)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				// Start mocap scene
				START_CUTSCENE()
				WAIT(0)
				STOP_GAMEPLAY_HINT()
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
					DEBUG_PRINTSTRING("Locked left gate open?")
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, TRUE, 90)
				ENDIF

				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1115.984375,285.592438,61.942406>>, <<-1097.599243,285.134613,66.346695>>, 33.0,
							vMoveToLoc, fHead, <<3, 6, 3>>)
				RC_START_CUTSCENE_MODE(<< -1104.93, 291.25, 64.30 >>, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				
				// Monitor cutscene
				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_LOWEST)
				DEBUG_PRINTSTRING("Done Trevor exit")
				// Ideally I shouldn't need this - but the cutscene leaves Trevor inside the garden so I need to teleport him	
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Josh")
				DEBUG_PRINTSTRING("Exit state for Josh...")
				IF NOT bLeadOutPlayed
					DEBUG_PRINTSTRING("Played lead-out on Josh exit state")
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1104.65, 289.02, 63.25>>, 16.6717)
				
					// Reset camera behind player
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
					AND DOES_ENTITY_EXIST(oLeftGate)
						iLeadOutSynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPos_SynchScene, vHeading_SynchScene)
						TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[JOSH], iLeadOutSynchSceneID, "rcmjosh1leadinout", "leadout_josh", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
							SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_TORSO_REACT_IK)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, iLeadOutSynchSceneID, "leadout_gate", "rcmjosh1leadinout", NORMAL_BLEND_IN)
						DEBUG_PRINTSTRING("Done Josh leadout anim on Josh exit state")
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oLeftGate)
						FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[JOSH], MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[JOSH],)
						bLeadOutPlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
				DEBUG_PRINTSTRING("Exit state for camera...")
				IF NOT bLeadOutPlayed
					DEBUG_PRINTSTRING("Played lead-out on camera exit state")
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1104.65, 289.02, 63.25>>, 16.6717)
				
					// Reset camera behind player
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
					AND DOES_ENTITY_EXIST(oLeftGate)
						iLeadOutSynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPos_SynchScene, vHeading_SynchScene)
						TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[JOSH], iLeadOutSynchSceneID, "rcmjosh1leadinout", "leadout_josh", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
							SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_TORSO_REACT_IK)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, iLeadOutSynchSceneID, "leadout_gate", "rcmjosh1leadinout", NORMAL_BLEND_IN)
						DEBUG_PRINTSTRING("Done Josh leadout anim on cam exit state cutscene")
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oLeftGate)
						FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[JOSH], MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[JOSH],)
						bLeadOutPlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// it doesn't let you go the exit state frame if you skip, so do that stuff here...
			IF WAS_CUTSCENE_SKIPPED()
				bSkipped = TRUE
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
				iSkipTimer = GET_GAME_TIMER()
				//RC_START_Z_SKIP()
				
				//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1104.8643, 286.5974, 63.1910>>, 16.6717)
				
				// Reset camera behind player
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
					REMOVE_MODEL_HIDE(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, TRUE, 0)
				ENDIF
				
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1101.62, 290.36, 64.76>>, 5.0, PROP_LRGGATE_01c_R)
					REMOVE_MODEL_HIDE(<<-1101.62, 290.36, 64.76>>, 5.0, PROP_LRGGATE_01c_R)
			   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_R, <<-1101.62, 290.36, 64.76>>, TRUE, 0)
				ENDIF
				
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				DEBUG_PRINTSTRING("cutscene not active...")
				IF NOT bLeadOutPlayed
					DEBUG_PRINTSTRING("Played lead-out on not cutscene active")
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1104.65, 289.02, 63.25>>, 16.6717)
				
					// Reset camera behind player
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
					AND DOES_ENTITY_EXIST(oLeftGate)
						iLeadOutSynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPos_SynchScene, vHeading_SynchScene)
						TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[JOSH], iLeadOutSynchSceneID, "rcmjosh1leadinout", "leadout_josh", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
							SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_TORSO_REACT_IK)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, iLeadOutSynchSceneID, "leadout_gate", "rcmjosh1leadinout", NORMAL_BLEND_IN)
						DEBUG_PRINTSTRING("Done Josh leadout anim on non active cutscene")
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oLeftGate)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[JOSH])
						bLeadOutPlayed = TRUE
					ENDIF
				ENDIF
				m_eSubState = SS_CLEANUP
			ENDIF 
		BREAK
		
		CASE SS_CLEANUP
		
			RC_END_CUTSCENE_MODE()
		
			m_eState = RC_LEADOUT
			m_eSubState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC

PROC STATE_LeadOut()

	SWITCH m_eSubState
		CASE SS_SETUP
			
			DEBUG_PRINTSTRING("Doing setup for leadout")
			
			//ADD_PED_FOR_DIALOGUE(pedsForConversation, 2, PLAYER_PED_ID(), "Trevor")
			//ADD_PED_FOR_DIALOGUE(pedsForConversation, 3, sRCLauncherDataLocal.pedID[JOSH], "Josh")
			
			REQUEST_ANIM_DICT("rcmjosh1leadinout")
			REQUEST_MODEL(p_amb_phone_01)
				
			IF HAS_ANIM_DICT_LOADED("rcmjosh1leadinout") // Loops here until anims are loaded
			AND HAS_MODEL_LOADED(p_amb_phone_01)
				IF NOT DOES_ENTITY_EXIST(oLeftGate)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, FALSE, 0.0)
					ENDIF
					oLeftGate = GET_CLOSEST_OBJECT_OF_TYPE(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
				ENDIF
				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF bSkipped = TRUE
				// If we skipped the cutscene, wait half a second so we don't see Josh and the gate pop and then end the Z skip
				DEBUG_PRINTSTRING("Skip detected, waiting 500ms...")
				IF (GET_GAME_TIMER() - iSkipTimer) >=500
					RC_END_Z_SKIP()
					bSkipped = FALSE
				ENDIF
			ENDIF
			IF NOT bLeadOutPlayed
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[JOSH])
				AND DOES_ENTITY_EXIST(oLeftGate)
					
					// Play sync scene
					iLeadOutSynchSceneID = CREATE_SYNCHRONIZED_SCENE(vPos_SynchScene, vHeading_SynchScene)
					TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[JOSH], iLeadOutSynchSceneID, "rcmjosh1leadinout", "leadout_josh", NORMAL_BLEND_IN, SLOW_BLEND_OUT,
						SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ON_ABORT_STOP_SCENE|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_TAG_SYNC_OUT, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_TORSO_REACT_IK)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, iLeadOutSynchSceneID, "leadout_gate", "rcmjosh1leadinout", NORMAL_BLEND_IN)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oLeftGate)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[JOSH], TRUE)
					DEBUG_PRINTSTRING("Done Josh leadout anim")
					bLeadOutPlayed = TRUE
				ENDIF
			ELSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadOutSynchSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSynchSceneID) > 0.29
						IF NOT bLeadOutConvo
							ADD_PED_FOR_DIALOGUE(pedsForConversation, 2, PLAYER_PED_ID(), "Trevor")
							ADD_PED_FOR_DIALOGUE(pedsForConversation, 5, sRCLauncherDataLocal.pedID[JOSH], "Josh")
							IF CREATE_CONVERSATION(pedsForConversation, "JOSH1AU", "JOSH1_OUT", CONV_PRIORITY_HIGH)
								DEBUG_PRINTSTRING("Leadout convo done")
								bLeadOutConvo = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadOutSynchSceneID) > 0.98
						DEBUG_PRINTSTRING("Leadout done, end mission")
						m_eSubState = SS_CLEANUP
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1107.106812,290.427490,62.948700>>, <<-1102.037964,291.342072,66.278946>>, 2.0)
						DEBUG_PRINTSTRING("Too close, stopping scene")
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH]) // CLEAR_PED_TASKS is the better option here, I'm told
							//STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[JOSH], REALLY_SLOW_BLEND_OUT, TRUE)
							DEBUG_PRINTSTRING("Stopping Josh?")
						ENDIF
						IF DOES_ENTITY_EXIST(oLeftGate)
							STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, NORMAL_BLEND_OUT, TRUE)
						ENDIF
						bLeadOutCancelled = TRUE
					ENDIF
					IF GET_DISTANCE_BETWEEN_ENTITIES(sRCLauncherDataLocal.pedID[JOSH], PLAYER_PED_ID()) > 17.5
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
							STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[JOSH], NORMAL_BLEND_OUT, TRUE)
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
						ENDIF
						IF DOES_ENTITY_EXIST(oLeftGate)
							STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, NORMAL_BLEND_OUT, TRUE)
						ENDIF
						bLeadOutCancelled = TRUE
					ENDIF
					IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[JOSH])
					OR IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[JOSH])
					OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[JOSH]), 5.0)
						IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
							STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[JOSH], NORMAL_BLEND_OUT, TRUE)
							CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[JOSH])
						ENDIF
						IF DOES_ENTITY_EXIST(oLeftGate)
							STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftGate, NORMAL_BLEND_OUT, TRUE)
						ENDIF
						bLeadOutCancelled = TRUE
					ENDIF
				ELSE
					DEBUG_PRINTSTRING("Leadout ended early, end mission")
					m_eSubState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
				//oiPhone = CREATE_OBJECT(p_amb_phone_01, GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[JOSH]))
				//ATTACH_ENTITY_TO_ENTITY(oiPhone, sRCLauncherDataLocal.pedID[JOSH], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[JOSH], BONETAG_PH_R_HAND), <<0.06,0,-0.005>>, <<110,190,-40>>, TRUE)
				IF bLeadOutCancelled = TRUE
					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1115.44, 317.46, 65.98>>, PEDMOVEBLENDRATIO_WALK, 30000)
						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "enter")
						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "idle_b", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "exit")
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1117.77, 299.27, 65.00>>, PEDMOVEBLENDRATIO_WALK, 30000)
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "enter")
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "idle_b")
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "exit")
//						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq)
				ELSE
					OPEN_SEQUENCE_TASK(seq)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1106.86, 297.68, 64.23>>, PEDMOVEBLENDRATIO_WALK)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1105.03, 304.15, 65.18>>, PEDMOVEBLENDRATIO_WALK)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1103.71, 312.59, 65.98>>, PEDMOVEBLENDRATIO_WALK)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1105.14, 316.27, 65.98>>, PEDMOVEBLENDRATIO_WALK)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1115.44, 317.46, 65.98>>, PEDMOVEBLENDRATIO_WALK)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1115.44, 317.46, 65.98>>, PEDMOVEBLENDRATIO_WALK, 30000, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "enter")
						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "idle_b", DEFAULT, DEFAULT, DEFAULT, AF_LOOPING)
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "exit")
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1117.77, 299.27, 65.00>>, PEDMOVEBLENDRATIO_WALK, 30000)
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "enter")
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "idle_b")
//						TASK_PLAY_ANIM(NULL, "rcmjosh1@impatient", "exit")
//						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq)
				ENDIF
				
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[JOSH], seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[JOSH], FALSE)
				IF bLeadOutCancelled = FALSE
					FORCE_PED_MOTION_STATE(sRCLauncherDataLocal.pedID[JOSH], MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, TRUE)
				ENDIF
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[JOSH], TRUE)
			ENDIF
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, PROP_LRGGATE_01c_L)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1107.01, 289.38, 64.76>>, FALSE, 0)
			ENDIF
			IF bLeadOutCancelled = FALSE
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), 0.0, TRUE, TRUE)
				DEBUG_PRINTSTRING("Josh 4: PROP_LRGGATE_01c_L - Closed")
			ENDIF
			Script_Passed()
		BREAK
		
	ENDSWITCH

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_JOSH_1(sRCLauncherDataLocal)
	  		WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		RelGroupBuddy = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
	ENDIF
		
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[JOSH])
		SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[JOSH], RelGroupBuddy)
		SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[JOSH], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	ENDIF
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_EC")
		
		WAIT(0)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			CASE RC_LEADIN
				STATE_LeadIn()
			BREAK
			CASE RC_MEET_JOSH
				STATE_MeetJosh()
			BREAK
			CASE RC_LEADOUT
				STATE_LeadOut()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

