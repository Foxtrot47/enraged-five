USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

// See B*1112596
CONST_INT iJoshCarColourCombo 4 

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

MODEL_NAMES         mContactModel = GET_NPC_PED_MODEL(CHAR_JOSH)
MODEL_NAMES         mContactVehicleModel = FELON2
INT 			    iSynchScene

/// PURPOSE: Opens the gates that are at the front of Josh's house
/// RETURNS: TRUE if the gates exist and were opened, FALSE otherwise
/// NOTE: open_left - opens the left gate, default TRUE. open_right - opens the right gate, default TRUE.
FUNC BOOL OPEN_FRONT_GATES(BOOL open_left = TRUE, BOOL open_right = TRUE)
	
	INT iNumGatesOpen = 0
	
	IF open_left
		CREATE_FORCED_OBJECT(<<-1107.01, 289.38, 64.76>>, 5.0, prop_lrggate_01c_l, FALSE)		
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), 1.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//		DOOR_SYSTEM_SET_SPRING_REMOVED(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), TRUE, TRUE, true)
//		DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_F_L - Opened")
	ELSE // Ignoring the left gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF open_right
		CREATE_FORCED_OBJECT(<<-1101.62, 290.36, 64.76>>, 5.0, prop_lrggate_01c_r, FALSE)	
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R), -0.9, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//		DOOR_SYSTEM_SET_SPRING_REMOVED(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R), TRUE, TRUE, TRUE)
//		DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R), TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_F_R - Opened")
	ELSE // Ignoring the right gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF iNumGatesOpen >= 2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Opens the gates that are at the front of Josh's house
/// RETURNS: TRUE if the gates exist and were opened, FALSE otherwise
/// NOTE: open_left - opens the left gate, default TRUE. open_right - opens the right gate, default TRUE.
FUNC BOOL CLOSE_FRONT_GATES(BOOL close_left = TRUE, BOOL close_right = TRUE)
	
	INT iNumGatesOpen = 0
	
	IF close_left
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_F_L - Closed")
	ELSE // Ignoring the left gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF close_right
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_R), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_F_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_F_R - Closed")
	ELSE // Ignoring the right gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF iNumGatesOpen >= 2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Closes and locks the rear gates at Josh's house
/// PARAMS:
///    close_left - close and lock the left gate
///    close_right - close and lock the right gate
/// RETURNS:
///    True when the gates are locked, false otherwise
FUNC BOOL CLOSE_REAR_GATES(BOOL close_left = TRUE, BOOL close_right = TRUE)

	INT iNumGatesOpen = 0
	
	IF close_left
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_R_L), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_R_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_R_L - Closed")
	ELSE // Ignoring the left gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF close_right
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JOSH_GATE_R_R), 0.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JOSH_GATE_R_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
		iNumGatesOpen++
		CPRINTLN(DEBUG_RANDOM_CHAR, "Josh: DOORHASH_JOSH_GATE_R_R - Closed")
	ELSE // Ignoring the right gate, marking as 'done'
		iNumGatesOpen++
	ENDIF
	
	IF iNumGatesOpen >= 2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Create's Josh's vehicle at his appartment
/// PARAMS:
///    mVehicle - vehicle index to use
PROC CREATE_JOSH_CAR_AT_APT(VEHICLE_INDEX &mVehicle)
	CREATE_SCENE_VEHICLE(mVehicle, mContactVehicleModel, <<558.09, -1765.39, 28.86>>, 335.3252)
	SET_VEHICLE_COLOUR_COMBINATION(mVehicle, iJoshCarColourCombo)
	SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_LOCKED)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(mVehicle, FALSE)
	SET_VEHICLE_DISABLE_TOWING(mVehicle, TRUE)
ENDPROC

/// PURPOSE:
///    Creates Josh's car at the house in Josh 1 and 4 and set its colour combination.
/// PARAMS:
///    mVehicle - vehicle index to use
///    inside_driveway - Whether to spawn the car inside the house's driveway for Josh 1 (default) or outside the side gates for Josh 4.
PROC CREATE_JOSH_CAR_AT_HOUSE(VEHICLE_INDEX &mVehicle, BOOL inside_driveway = TRUE)

	VECTOR vJoshCarPos
	FLOAT fJoshCarHead
	
	if inside_driveway
		vJoshCarPos = <<-1129.18, 300.55, 65.92>>
		fJoshCarHead = 86.97
	ELSE
		vJoshCarPos = <<-1139.759, 291.483, 66.327>>
		fJoshCarHead = 6.077003
	ENDIF

	CREATE_SCENE_VEHICLE(mVehicle, mContactVehicleModel, vJoshCarPos, fJoshCarHead)
	SET_VEHICLE_COLOUR_COMBINATION(mVehicle, iJoshCarColourCombo)
	SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(mVehicle, FALSE)
	SET_VEHICLE_DISABLE_TOWING(mVehicle, TRUE)
ENDPROC

/// PURPOSE: 
///    Creates the scene for Josh 1.
FUNC BOOL SetupScene_JOSH_1(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  		0
	CONST_INT MODEL_CONTACT_VEHICLE 1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.activationRange = 9.0
			sRCLauncherData.sIntroCutscene = "JOSH_1_INT_CONCAT"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmjosh1", "idle" )
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE		
			
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF

			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Josh
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_JOSH, <<-1104.72, 291.02, 64.39>>, 138.35, "JOSH LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Josh's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_JOSH_CAR_AT_HOUSE(sRCLauncherData.vehID[0])
			ENDIF
			
			// Close gates at front of house
			IF NOT CLOSE_FRONT_GATES()
				bCreatedScene = FALSE
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Disable paths around here to stop peds walking into the house
			SET_PED_PATHS_IN_AREA(<<-1112.67, 287.38, 62.85>>, <<-1096.41, 297.50, 65.59>>, FALSE)

			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Josh 2.
FUNC BOOL SetupScene_JOSH_2(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  		0
	CONST_INT MODEL_CONTACT_VEHICLE 1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<574.052063,-1770.594360,27.669289>>
			sRCLauncherData.triggerLocate[1] = <<559.163147,-1768.663452,31.419291>>
			sRCLauncherData.triggerWidth = 17.25
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "josh_2_intp1_t4"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT("rcmjosh2")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmjosh2")	
				RETURN FALSE
			ENDIF

			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK

		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Josh
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_JOSH, <<565.390076,-1772.837769,29.800871>> , 62.5015565, "JOSH LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Create Josh's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_JOSH_CAR_AT_APT(sRCLauncherData.vehID[0])
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_UNLOCKED)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
	
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherData.pedID[0],TRUE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_RunFromFiresAndExplosions, FALSE) // B*577652
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE) 	// B*577652
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0],PLAYER_PED_ID(),-1)
				ENDIF

				iSynchScene = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
				SET_SYNCHRONIZED_SCENE_LOOPED(iSynchScene, TRUE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSynchScene, "rcmjosh2", "josh_wait_loop", 2, SLOW_BLEND_OUT)
			ENDIF
	
			// Unload anims
			REMOVE_ANIM_DICT("rcmjosh2")
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: Creates the scene for Josh 3.
/// NOTE:    Josh waiting at the motel.
FUNC BOOL SetupScene_JOSH_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  		0
	CONST_INT MODEL_CONTACT_VEHICLE 1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<567.514221,-1760.010986,27.169291>>
			sRCLauncherData.triggerLocate[1] = <<565.344543,-1775.931274,31.351496>>
			sRCLauncherData.triggerWidth = 7.00
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "josh_3_intp1"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT("rcmjosh3")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmjosh3")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Josh
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_JOSH, << 566.1639, -1773.8171, 29.0 >>, 14.4, "JOSH LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Josh's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_JOSH_CAR_AT_APT(sRCLauncherData.vehID[0])
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
	
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_RunFromFiresAndExplosions, FALSE) // B*577652
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_DisableExplosionReactions, TRUE) // B*577652
				
				sRCLauncherData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>, <<-0.00, 0.05, -110.70>>)
		        TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], sRCLauncherData.iSyncSceneIndex, "rcmjosh3", "sit_stairs_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS)
				SET_SYNCHRONIZED_SCENE_LOOPED(sRCLauncherData.iSyncSceneIndex, TRUE)
				CPRINTLN(DEBUG_RANDOM_CHAR, "Josh3: idle sync scene started")
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
	
/// PURPOSE: 
///    Creates the scene for Josh 4.
FUNC BOOL SetupScene_JOSH_4(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  			0
	CONST_INT MODEL_CONTACT_VEHICLE  	1
	CONST_INT MODEL_COP  				2
	CONST_INT MODEL_COP_CAR  			3
	
	// Variables
	MODEL_NAMES mModel[4]
	INT         iCount
	FLOAT 		newEngineHealth
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	mModel[MODEL_COP]             = S_M_Y_COP_01
	mModel[MODEL_COP_CAR]         = POLICE3
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 8.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "JOSH_4_INT_CONCAT"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmjosh4", "beckon_a_josh" )
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE	
			
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Open the front gates
			IF NOT OPEN_FRONT_GATES(FALSE)
				CPRINTLN(DEBUG_RANDOM_CHAR, "Josh4: Front gates say they haven't opened...")
			ELSE
				CPRINTLN(DEBUG_RANDOM_CHAR, "Josh4: Front gates opened")
			ENDIF
	
			// Close the rear gates
			IF NOT CLOSE_REAR_GATES()
				CPRINTLN(DEBUG_RANDOM_CHAR, "Josh4: Rear gates say they haven't closed...")
			ELSE
				CPRINTLN(DEBUG_RANDOM_CHAR, "Josh4: Rear gates closed")
			ENDIF

			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK

		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Josh
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_JOSH, <<-1103.60, 290.81, 64.31>>, -99.24, "JOSH LAUNCHER RC")
					SET_PED_COMBAT_ATTRIBUTES(sRCLauncherData.pedID[0],CA_ALWAYS_FLEE,TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// First cop
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_COP], <<-1102.92, 290.12,64.28>>, 40.46, PEDTYPE_COP)
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1])
					SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[1])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[1],true)
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[1], WEAPONTYPE_PISTOL, -1, FALSE)

					// Play anim for cop 1
					IF HAS_ANIM_DICT_LOADED("rcmjosh4")
						CLEAR_PED_TASKS(sRCLauncherData.pedID[1])
						TASK_PLAY_ANIM(sRCLauncherData.pedID[1], "rcmjosh4", "BECKON_A_COP_B", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Second cop
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[2])
				CREATE_SCENE_PED(sRCLauncherData.pedID[2], mModel[MODEL_COP], <<-1102.73, 290.91,64.28>>, 92.06, PEDTYPE_COP)
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[2])
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_HEAD, 1, 1)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_TORSO, 0, 1)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_DECL, 0, 1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[2], TRUE)
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[2], WEAPONTYPE_PISTOL, -1, FALSE)

					// Play anim for cop 2
					IF HAS_ANIM_DICT_LOADED("rcmjosh4")
						CLEAR_PED_TASKS(sRCLauncherData.pedID[2])
						TASK_PLAY_ANIM(sRCLauncherData.pedID[2], "rcmjosh4", "BECKON_A_COP_A", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Cop car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_COP_CAR], <<-1107.50, 280.33, 64.01>>, 97.97)
			ENDIF
			
			// Josh's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_JOSH_CAR_AT_HOUSE(sRCLauncherData.vehID[1], FALSE)
				IF IS_VEHICLE_OK(sRCLauncherData.vehID[1])
					// Decrease the petrol tank health
					SET_VEHICLE_PETROL_TANK_HEALTH(sRCLauncherData.vehID[1], 900)
					
					// Halve the engine health
					newEngineHealth = GET_VEHICLE_ENGINE_HEALTH(sRCLauncherData.vehID[1])
					newEngineHealth = newEngineHealth/2 
					SET_VEHICLE_ENGINE_HEALTH(sRCLauncherData.vehID[1], newEngineHealth)
					SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[1])
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Lock the rear gates
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1138.32, 299.69, 65.94>>, 5.0, PROP_LRGGATE_01c_L)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_L, <<-1138.32, 299.69, 65.94>>, TRUE, 0)
			ENDIF
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1137.52, 297.02, 65.81>>, 5.0, PROP_LRGGATE_01c_R)
		   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01c_R, <<-1137.053833, 295.585571, 67.180458>>, TRUE, 0)
			ENDIF
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
