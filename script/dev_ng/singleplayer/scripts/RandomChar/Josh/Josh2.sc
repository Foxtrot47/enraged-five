
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "taxi_functions.sch"
USING "RC_Threat_public.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Josh.sch"
USING "commands_event.sch"
USING "commands_recording.sch"

//USING "fake_cellphone_public.sch"

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Josh2.sc
//		AUTHOR			:	Tom Kingsley
//		DESCRIPTION		:	Trevor chases Avery - Ai chase.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

g_structRCScriptArgs sRCLauncherDataLocal			//Local copy of the data created by the launcher

structPedsForConversation s_conversation_peds
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

INT iRandomInt
INT iCometInitHp
INT iCometStuckCounter
INT iAveryTookMeleeHit
INT iPunchesAveryCanTake 	= 0  //Number of punches avery can take before being KO'd //2
INT iConvoCounter
INT iRoadSwitch
INT iTimerRoadsOff
INT iTimerDistanceWarning
INT iTimerMissionStarted
INT iTimerJoshSpeak
INT iJoshConvoCounter
INT iBikerConvoCounter
INT iGunThreaten
INT iTimerAveryGettingUp
INT iRollingStartTimer
INT iTimerPlayerHungUpPhone
INT iPleadConvo
INT iJoshScared
INT i
INT iTimerChaseStarted
INT iTimerAveryKO
INT iAverySpotted = 0
INT iTimerGunReact
INT iCounterReverseAverySeesPlayer = 0
INT Josh2Door[6]
INT iJoshMotelLines
INT iTimerJoshMotelLines
INT iTrevorChaseLines
INT iTimerTrevorChaseLines
INT iTimerAveryFleeOnFoot
INT iAveryOhFuckLines
INT iAveryFlee
INT iAveryGunThreat
INT iTimerDialogueDamageCar
INT iCounterDialogueDamageCar
						
FLOAT fCometEngineHp
FLOAT fCometPetrolHp
FLOAT fTopSpeed
FLOAT fHeadingComet
FLOAT fHeadingCometSpawn1
FLOAT fHeadingCometSpawn2

//VECTOR vAvery
VECTOR vCometPos
VECTOR vCometSpawn1
VECTOR vCometSpawn2
//VECTOR vSpawnTraffic1[6]
//FLOAT fHeadingTraffic1[6]
///VECTOR vDestTraffic1
VECTOR vNodeSwitch1
VECTOR vNodeSwitch2
//VECTOR vSpawnTraffic2[6]
//FLOAT fHeadingTraffic2[6]
//VECTOR vDestTraffic2
//VECTOR vSafeCoord

BOOL bWaypointPlaybackStopped
BOOL bCometReverse
BOOL bStartChase
BOOL bFindTheGreenCometMessagePrinted
BOOL bDistanceWarningPrinted
BOOL bPlayerRanAveryOver
BOOL bAveryFleeingInVehicle
BOOL bDebugSkipping
BOOL bDebugSkippingToStart
BOOL bPrintWrongColourComet
INT iBikerInCombat
BOOL bAveryGettingUp
BOOL bJoshCleanedUp
BOOL bRockfordInit
BOOL bJoshTasksCleared	
BOOL bRoadsOffAroundAvery
BOOL bPrintBeatAvery
BOOL bCometSmashed
BOOL bDoorFallenOff
BOOL bDebugSkippedToBeatAvery
BOOL bConvoFelon = FALSE
BOOL bHouseViewerYelling = FALSE	
BOOL bSwitchAnimFlags = FALSE
BOOL bOnFireHpSwitch = FALSE
BOOL bJoggerCreated = FALSE
BOOL bDogCreated = FALSE
BOOL bPrintGoToRockford = FALSE
BOOL bStreamVolCreated
BOOL bAveryFleeingOnFoot
BOOL bTrevThreat
BOOL bTrevThreatenedAvery
BOOL bDisabledTaxis = FALSE
BOOL bCSExitTrev = FALSE
BOOL bCSExitJosh = FALSE
BOOL bCSExitCam = FALSE
BOOL bDoCSLocate = FALSE
BOOL bLoveYa = FALSE
BOOL bDontKillAveryHelp = FALSE
BOOL bTurnedJosh
BOOL bCometGotStuck
INT iAveryKOLines

BOOL bJoggerLastLine
INT iTimerJoggerLastLine

INT Josh2_Leadin_1
INT Josh2_Leadin_1b
INT Josh2_Leadin_2
INT Josh2_Leadin_3
INT Josh2_Leadin_4

STRING sFailReason

// Mission stages
ENUM MISSION_STAGE
	MS_SETUP,
	MS_INTRO,
	MS_INIT,
	MS_GO_TO_ROCKFORD,
	MS_FIND_THE_GREEN_COMET,
	MS_CHASE_AVERY,
	MS_GO_TO_AVERY,
	MS_AVERY_KO,
	MS_DISTURB_AVERY,
	MS_FIGHT_AVERY,
	MS_OUTRO,
	MS_MISSION_FAILING
ENDENUM

ENUM FAIL_STATE
	FS_SETUP,
	FS_UPDATE,
	FS_CLEANUP
ENDENUM	

ENUM CUTSCENE_STAGE
	eCutInit,
	eCutLeadIn1,
	eCutLeadIn2,
	eCutLeadIn3,
	eCutLeadIn4,
	eCutLeadIn5,
	eCutUpdate,
	eCutLeadOut1,
	eCutLeadOut2,
	eCutCleanup
ENDENUM	

ENUM JOGGER_STAGE
	JOGGER_LOAD,
	JOGGER_SPAWN,
	JOGGER_JOG_DOWN_STREET,
	JOGGER_APPROACH_TREVOR,
	JOGGER_CHAT_TO_TREVOR,
	JOGGER_CONTINUE_JOGGING,
	JOGGER_FLEE,
	JOGGER_CLEANUP
ENDENUM

JOGGER_STAGE eJOGGER = JOGGER_LOAD

CUTSCENE_STAGE eCutsceneState = eCutInit

MISSION_STAGE eMissionStage = MS_SETUP

FAIL_STATE eFAIL_STATE

structTimelapse sTimelapse
TIMEOFDAY todReference
INT iSkipToHour
CONST_INT CONST_NUM_HOURS_SKIP_FOR_MOTEL_TIMELAPSE 2

VEHICLE_INDEX vehAveryComet
VEHICLE_INDEX vehBullet
VEHICLE_INDEX vehElegy
VEHICLE_INDEX vehGauntlet
VEHICLE_INDEX vehJoshFelon
//VEHICLE_INDEX vehTraffic1[6]
//VEHICLE_INDEX vehTraffic2[6]
VEHICLE_INDEX vehBati
VEHICLE_INDEX vehSuperd
VEHICLE_INDEX vehPlayer

PED_INDEX pedAvery
PED_INDEX pedViewerHusband
PED_INDEX pedBiker

BLIP_INDEX blipRockfordHills
BLIP_INDEX blipAvery
BLIP_INDEX blipCSDoor

SEQUENCE_INDEX seqComet2
SEQUENCE_INDEX seqAngryHusband
//SEQUENCE_INDEX seqAveryCower

VEHICLE_INDEX vehRandom[15]
PED_INDEX pedRandom[15]

PED_INDEX pedJogger
PED_INDEX pedDog

CAMERA_INDEX camMiniCut

SCENARIO_BLOCKING_INDEX sbiLenny

STREAMVOL_ID StreamVol
/*
SCALEFORM_INDEX	siJoshPhone

INT rt_ID
*/

//FAKE_CELLPHONE_DATA sJoshFakeCellphoneData

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 4
	INT iReturnStage                                       
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]     
#ENDIF

/// PURPOSE: Load the stuff we need right at the start.
PROC LoadStuff()
	
	//RELEASE_NAMED_RENDERTARGET("Prop_NPC_Phone")
	//RELEASE_NAMED_RENDERTARGET("CELLPHONE_CUTSCENE")
	
	//siJoshPhone = REQUEST_SCALEFORM_MOVIE("CELLPHONE_CUTSCENE")
	REQUEST_ANIM_DICT("rcmjosh")
	REQUEST_ANIM_DICT("rcmjosh2")
	REQUEST_ADDITIONAL_TEXT("JOSH2",MISSION_TEXT_SLOT)
	//REQUEST_ADDITIONAL_TEXT("JOSH2AU",MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_MODEL(FELON2)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
	OR NOT HAS_MODEL_LOADED(FELON2)
	OR NOT HAS_ANIM_DICT_LOADED("rcmjosh2")
	OR NOT HAS_ANIM_DICT_LOADED("rcmjosh")
	//OR NOT HAS_SCALEFORM_MOVIE_LOADED(siJoshPhone)
		CPRINTLN(DEBUG_MISSION, " LoadStuff : waiting ")
		WAIT(0)
	ENDWHILE
	/*
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("CELLPHONE_CUTSCENE")
		REGISTER_NAMED_RENDERTARGET("CELLPHONE_CUTSCENE")
	ENDIF
	*/

ENDPROC

/// PURPOSE: Request all assets for Rockford hills
PROC RequestRockfordAssets()

	REQUEST_MODEL(COMET2)
	REQUEST_MODEL(A_M_Y_BUSINESS_03)
	REQUEST_MODEL(BULLET)
	REQUEST_MODEL(SUPERD)
	REQUEST_MODEL(BATI)
	REQUEST_MODEL(A_M_M_BevHills_01)
	REQUEST_ANIM_DICT("rcmjosh2")
	REQUEST_ANIM_DICT("rcmjosh2ig_1")
	REQUEST_CLIP_SET("MOVE_M@BAIL_BOND_NOT_TAZERED")
	//REQUEST_VEHICLE_RECORDING(174,"Josh2Roll")	
	REQUEST_WAYPOINT_RECORDING("Josh2Roll")
	REQUEST_WAYPOINT_RECORDING("Josh2_Corner")
	REQUEST_VEHICLE_RECORDING(101,"Josh2_A1")	
	REQUEST_VEHICLE_RECORDING(102,"Josh2_A2")	
	//REQUEST_WAYPOINT_RECORDING("Josh2_Comet10")
	//REQUEST_WAYPOINT_RECORDING("Josh2_Comet11")

ENDPROC

/// PURPOSE: Wait for all assets for Rockford hills
PROC WaitForRockfordAssets()

	//WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("Josh2_Comet10")
	//OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Josh2_Comet11")
	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(101,"Josh2_A1")
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(102,"Josh2_A2")
	OR NOT HAS_MODEL_LOADED(COMET2)
	OR NOT HAS_MODEL_LOADED(A_M_Y_BUSINESS_03)
	OR NOT HAS_MODEL_LOADED(BULLET)
	OR NOT HAS_MODEL_LOADED(SUPERD)
	OR NOT HAS_MODEL_LOADED(BATI)
	OR NOT HAS_MODEL_LOADED(A_M_M_BevHills_01)
	OR NOT HAS_ANIM_DICT_LOADED("rcmjosh2")
	OR NOT HAS_ANIM_DICT_LOADED("rcmjosh2ig_1")
	OR NOT HAS_CLIP_SET_LOADED("MOVE_M@BAIL_BOND_NOT_TAZERED")
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Josh2Roll")
	//OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("Josh2_Corner")
	//OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(174,"Josh2Roll")
		WAIT(0)
	ENDWHILE

ENDPROC

PROC START_AUDIO_SCENE_CHASE()  

	IF NOT IS_AUDIO_SCENE_ACTIVE("JOSH_02_SCENE")	
		IF IS_ENTITY_ALIVE(vehAveryComet)
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehAveryComet, "JOSH_02_SCENE_LENNY_VEHICLE")
		ENDIF	
		START_AUDIO_SCENE("JOSH_02_SCENE")
	ENDIF
	
ENDPROC

PROC CLEANUP_AUDIO_SCENE_CHASE() 
	
	IF IS_AUDIO_SCENE_ACTIVE("JOSH_02_SCENE")
		STOP_AUDIO_SCENE("JOSH_02_SCENE")
	ENDIF

ENDPROC

PROC DO_JOSH_MOTEL_LINES()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<565.184204,-1777.227417,30.704159>>, <<549.950806,-1770.103027,35.947773>>, 7.000000)	
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])	
		AND	IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],30)	
			IF iJoshMotelLines < 5
				IF GET_GAME_TIMER()	> iTimerJoshMotelLines + (8000 + GET_RANDOM_INT_IN_RANGE(0,2000))	
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_MOTEL",CONV_PRIORITY_LOW)	
						iTimerJoshMotelLines = GET_GAME_TIMER()
						++iJoshMotelLines
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_TREVOR_CHASE_LINES()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
	AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedAvery,80)	
		IF iTrevorChaseLines < 7
			IF GET_GAME_TIMER()	> iTimerTrevorChaseLines + (20000 + GET_RANDOM_INT_IN_RANGE(0,10000))	
				IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CHASE",CONV_PRIORITY_LOW)	//It's wank
					iTimerTrevorChaseLines = GET_GAME_TIMER()
					++iTrevorChaseLines
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Grab cars around a ped and make them brake
PROC STOP_AMBIENT_CARS_AROUND_PED(PED_INDEX ped,FLOAT fRadius = 30.0,INT iBraketime = 60000)
	
	IF IS_PED_UNINJURED(ped)	
		IF GET_ENTITY_SPEED(ped) < 0.1	
			IF NOT DOES_ENTITY_EXIST(vehRandom[i])	
				vehRandom[i] = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(ped),fRadius,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
				IF IS_VEHICLE_OK(vehRandom[i])
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehRandom[i])
						pedRandom[i] = GET_PED_IN_VEHICLE_SEAT(vehRandom[i])
						IF IS_PED_UNINJURED(pedRandom[i])
							IF IS_PED_FACING_PED(pedRandom[i],ped,180.0)	
								IF GET_SCRIPT_TASK_STATUS(pedRandom[i],SCRIPT_TASK_VEHICLE_TEMP_ACTION) <> PERFORMING_TASK
									//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRandom[i],TRUE)
									TASK_VEHICLE_TEMP_ACTION(pedRandom[i],vehRandom[i],TEMPACT_BRAKE,iBraketime)
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, " TOLD A RANDOM VEHICLE TO BRAKE ")
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(pedRandom[i])
								SET_PED_AS_NO_LONGER_NEEDED(pedRandom[i])
							ENDIF
							IF DOES_ENTITY_EXIST(vehRandom[i])
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRandom[i])
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(pedRandom[i])
							SET_PED_AS_NO_LONGER_NEEDED(pedRandom[i])
						ENDIF
						IF DOES_ENTITY_EXIST(vehRandom[i])
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRandom[i])
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(pedRandom[i])
						SET_PED_AS_NO_LONGER_NEEDED(pedRandom[i])
					ENDIF
					IF DOES_ENTITY_EXIST(vehRandom[i])
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehRandom[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	++i	
	
	IF i > 14
		i = 0
	ENDIF
		
ENDPROC

PROC DO_JOGGER_PED()

	TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()

	SEQUENCE_INDEX seqJogger

	IF bJoggerCreated = TRUE
	AND bDogCreated = TRUE
		IF IS_PED_UNINJURED(pedJogger)
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedJogger)               
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-891.910583,17.157560,43.932739>>, <<-923.386963,42.692142,50.968578>>, 12.250000)	//Player drove past
				IF NOT IS_STRING_NULL_OR_EMPTY(root)
					IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG1")	
					OR ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")	
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedJogger,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedJogger,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
				AND NOT IS_ENTITY_IN_RANGE_COORDS(pedJogger,<<-824.8292, -24.5877, 38.1216>>,4)	
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedJogger,<<-824.8292, -24.5877, 38.1216>>,1.2,-1)
					eJOGGER = JOGGER_CONTINUE_JOGGING
				ENDIF
			ENDIF
			IF eJOGGER <> JOGGER_FLEE 	
				IF HAS_PLAYER_THREATENED_PED(pedJogger)
				OR HAS_PLAYER_THREATENED_PED(pedDog)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedJogger,PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDog,PLAYER_PED_ID())	
				OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedJogger),20,FALSE)	
					IF NOT IS_STRING_NULL_OR_EMPTY(root)
						IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG1")
						OR ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")		
							//IF NOT IS_STRING_NULL_OR_EMPTY(label)
							//	IF ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_1")			
							//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_3")	
							//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_4")	
							//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_5")			
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							//	ELSE
							//		KILL_FACE_TO_FACE_CONVERSATION()
							//	ENDIF
							//ENDIF
						ENDIF
					ENDIF
					STOP_PED_SPEAKING(pedJogger,FALSE)
					CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_ATTACK", CONV_PRIORITY_HIGH)
					IF IS_PED_UNINJURED(pedDog)
						TASK_SMART_FLEE_PED(pedDog,PLAYER_PED_ID(),200,-1)
						SET_PED_KEEP_TASK(pedDog,TRUE)
					ENDIF
					CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_IDLE)		
					CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_WALK)
					CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_RUN)	
					TASK_REACT_AND_FLEE_PED(pedJogger,PLAYER_PED_ID())
					SAFE_RELEASE_PED(pedJogger)
					SAFE_RELEASE_PED(pedDog)
					eJOGGER = JOGGER_FLEE
				ENDIF
			ENDIF
		ELSE	
			IF eJOGGER <> JOGGER_FLEE	
				IF IS_PED_UNINJURED(pedDog)
					TASK_SMART_FLEE_PED(pedDog,PLAYER_PED_ID(),200,-1)
					SET_PED_KEEP_TASK(pedDog,TRUE)
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(root)
					IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG1")
					OR ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")		
						//IF NOT IS_STRING_NULL_OR_EMPTY(label)
						//	IF ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_1")			
						//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_3")	
						//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_4")	
						//	OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_5")			
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//	ELSE
						//		KILL_FACE_TO_FACE_CONVERSATION()
						//	ENDIF
						//ENDIF
					ENDIF
				ENDIF
				STOP_PED_SPEAKING(pedJogger,FALSE)
				CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_ATTACK", CONV_PRIORITY_HIGH)
				SAFE_RELEASE_PED(pedJogger)
				SAFE_RELEASE_PED(pedDog)
				eJOGGER = JOGGER_FLEE
			ENDIF
		ENDIF
	ENDIF
	/*
	IF DOES_ENTITY_EXIST(pedJogger)
	AND DOES_ENTITY_EXIST(pedDog)	
		IF IS_ENTITY_DEAD(pedJogger)
		OR IS_PED_INJURED(pedJogger)
		OR IS_ENTITY_DEAD(pedDog)
		OR IS_PED_INJURED(pedDog)
			CLEAR_PED_TASKS(pedJogger)
			CLEAR_PED_TASKS(pedDog)
			SAFE_RELEASE_PED(pedJogger)
			SAFE_RELEASE_PED(pedDog)
			EXIT
		ELSE
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedJogger)
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedDog)
			IF HAS_PLAYER_THREATENED_PED(pedJogger)
			OR HAS_PLAYER_THREATENED_PED(pedDog,FALSE)	
				IF NOT IS_STRING_NULL_OR_EMPTY(root)
					IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")								
						IF NOT IS_STRING_NULL_OR_EMPTY(label)
							IF ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_1")			
							OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_3")	
							OR ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_5")			
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				CLEAR_PED_TASKS(pedJogger)
				SAFE_RELEASE_PED(pedJogger)
			ENDIF
		ENDIF
	ENDIF
	*/
	SWITCH eJOGGER
		
		CASE JOGGER_LOAD
			
			bJoggerLastLine = FALSE
			REQUEST_MODEL(A_F_Y_FITNESS_02)
			REQUEST_MODEL(A_C_RETRIEVER)
			REQUEST_ANIM_DICT("move_f@jogger")
			//REQUEST_CLIP_SET("AMB@WORLD_HUMAN_JOG_FEMALE@BASE")
			eJOGGER = JOGGER_SPAWN
		
		BREAK
		
		CASE JOGGER_SPAWN
		
			IF NOT DOES_ENTITY_EXIST(pedJogger)	
				IF HAS_MODEL_LOADED(A_F_Y_FITNESS_02)
				AND HAS_MODEL_LOADED(A_C_RETRIEVER)
				AND HAS_ANIM_DICT_LOADED("move_f@jogger")
				//AND HAS_CLIP_SET_LOADED("AMB@WORLD_HUMAN_JOG_FEMALE@BASE")
					pedJogger = CREATE_PED(PEDTYPE_MISSION,A_F_Y_FITNESS_02,<<-885.9105, 9.2858, 43.7928>>, 239.9410)
					pedDog = CREATE_PED(PEDTYPE_MISSION,A_C_RETRIEVER,<<-888.4332, 10.5150, 43.9829>>, 241.9898)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds,4,pedJogger,"JOSH2JOGGER")
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,2), 0, 2, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,8), 0, 2, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(pedJogger, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
					SET_PED_DEFAULT_COMPONENT_VARIATION(pedDog)
					//SET_PED_MOVEMENT_CLIPSET(pedJogger,"AMB@WORLD_HUMAN_JOG_FEMALE@BASE")
					SET_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_IDLE,"move_f@jogger","idle",2)		
					SET_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_WALK,"move_f@jogger","jogging",2)
					SET_PED_ALTERNATE_MOVEMENT_ANIM(pedJogger,AAT_RUN,"move_f@jogger","run",2)	
					TASK_LOOK_AT_ENTITY(pedDog,pedJogger,-1)
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedDog,pedJogger,<<0,-2,0>>,1.5,-1,0.5)
					STOP_PED_SPEAKING(pedJogger,TRUE)
					bJoggerCreated = TRUE
					bDogCreated = TRUE
				ENDIF
			ELSE
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),pedJogger,75)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(pedJogger,<<-863.1377, 0.1459, 42.3048>>,1.2,-1)
					OPEN_SEQUENCE_TASK(seqJogger)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-887.8427, 10.2152, 43.9376>>,1.2,-1,1,ENAV_NO_STOPPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-874.8856, 3.1640, 42.9592>>,1.2,-1,1,ENAV_NO_STOPPING)	
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-864.1951, -0.3664, 42.3140>>,1.2,-1,1,ENAV_NO_STOPPING)	
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-847.0853, -0.1079, 41.7293>>,1.2,-1,1,ENAV_NO_STOPPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-835.9113, -5.3215, 40.8279>>,1.2,-1,1,ENAV_NO_STOPPING)	
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-824.8292, -24.5877, 38.1216>>,1.2,-1,1,ENAV_NO_STOPPING)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(seqJogger)
					TASK_PERFORM_SEQUENCE(pedJogger,seqJogger)
					CLEAR_SEQUENCE_TASK(seqJogger)
					eJOGGER = JOGGER_JOG_DOWN_STREET
				ENDIF
			ENDIF
			
		BREAK
		
		CASE JOGGER_JOG_DOWN_STREET
			
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)	
			IF IS_PED_UNINJURED(pedJogger)
			AND IS_ENTITY_IN_RANGE_ENTITY(pedJogger,PLAYER_PED_ID(),15)
			AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 12	
				IF IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_07")	
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOG1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)  //Hey! Lady! Come here!	
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedJogger,-1)
						TASK_LOOK_AT_ENTITY(pedJogger,PLAYER_PED_ID(),-1)
						TASK_LOOK_AT_ENTITY(pedDog,PLAYER_PED_ID(),-1)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedJogger,PLAYER_PED_ID(),<<-2,2,0>>,1.2,-1,1.0)
						eJOGGER = JOGGER_APPROACH_TREVOR
					ENDIF
				ELSE	
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOG1", CONV_PRIORITY_MEDIUM)  //Hey! Lady! Come here!	
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedJogger,-1)
						TASK_LOOK_AT_ENTITY(pedJogger,PLAYER_PED_ID(),-1)
						TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedJogger,PLAYER_PED_ID(),<<-2,2,0>>,1.2,-1,1.0)
						eJOGGER = JOGGER_APPROACH_TREVOR
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE JOGGER_APPROACH_TREVOR
			
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)	
			IF IS_PED_UNINJURED(pedJogger)
			AND IS_ENTITY_IN_RANGE_ENTITY(pedJogger,PLAYER_PED_ID(),4.7)
				IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOG2", CONV_PRIORITY_MEDIUM)   //You seen a guy with a green car? A comet?
					IF GET_SCRIPT_TASK_STATUS(pedJogger,SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) = PERFORMING_TASK
						CLEAR_PED_TASKS(pedJogger)
					ENDIF
					eJOGGER = JOGGER_CHAT_TO_TREVOR
				ENDIF
			ENDIF
			
			STOP_AMBIENT_CARS_AROUND_PED(pedJogger,30,10000)
					
		BREAK
		
		CASE JOGGER_CHAT_TO_TREVOR
			
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)	
			IF IS_PED_UNINJURED(pedJogger)
				IF IS_PED_UNINJURED(pedDog)
					IF IS_ENTITY_IN_RANGE_ENTITY(pedJogger,pedDog,2.5)	
						//IF NOT IS_PED_FACING_PED(pedDog,PLAYER_PED_ID(),45)
						//	TASK_TURN_PED_TO_FACE_ENTITY(pedDog,PLAYER_PED_ID())
						//ELSE
							IF GET_SCRIPT_TASK_STATUS(pedDog,SCRIPT_TASK_START_SCENARIO_IN_PLACE) <> PERFORMING_TASK
								TASK_START_SCENARIO_IN_PLACE(pedDog,"WORLD_DOG_SITTING_RETRIEVER",0,TRUE)
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedJogger,PLAYER_PED_ID(),10)
					IF NOT IS_STRING_NULL_OR_EMPTY(root)
						IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")								
							KILL_ANY_CONVERSATION()
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_FACING_PED(pedJogger,PLAYER_PED_ID(),90)
						TASK_TURN_PED_TO_FACE_ENTITY(pedJogger,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF	
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedDog,pedJogger,<<0,-2,0>>,1.5,-1,0.5)
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedJogger,<<-824.8292, -24.5877, 38.1216>>,1.2,-1)
				eJOGGER = JOGGER_CONTINUE_JOGGING
			ELSE
				IF ARE_STRINGS_EQUAL(root,"JOSH2_JOG2")								
					IF ARE_STRINGS_EQUAL(label,"JOSH2_JOG2_11")	//7		
						IF bJoggerLastLine = FALSE
							iTimerJoggerLastLine = GET_GAME_TIMER()
							bJoggerLastLine = TRUE
						ELSE	
							IF GET_GAME_TIMER()	> iTimerJoggerLastLine + 1200
								TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedDog,pedJogger,<<0,-2,0>>,1.5,-1,0.5)
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedJogger,<<-824.8292, -24.5877, 38.1216>>,1.2,-1)
								eJOGGER = JOGGER_CONTINUE_JOGGING
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			STOP_AMBIENT_CARS_AROUND_PED(pedJogger,30,10000)
				
		BREAK
		
		CASE JOGGER_CONTINUE_JOGGING
		
			IF IS_PED_UNINJURED(pedJogger)
				IF IS_ENTITY_IN_RANGE_COORDS(pedJogger,<<-824.8292, -24.5877, 38.1216>>,15)
					bJoggerCreated = FALSE
					bDogCreated = FALSE
					TASK_WANDER_STANDARD(pedJogger)
					SET_PED_KEEP_TASK(pedJogger,TRUE)
					SAFE_RELEASE_PED(pedJogger)
					eJOGGER = JOGGER_CLEANUP
				ENDIF
			ENDIF
		
		BREAK
		
		CASE JOGGER_FLEE
		
		BREAK
		
		CASE JOGGER_CLEANUP
		
		BREAK
	
	ENDSWITCH
	
ENDPROC

/// PURPOSE: Initialise mission, spawn cars and peds
PROC Init()
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH, TREVOR_BOOK, FALSE)
	
	IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		WHILE NOT RC_CREATE_NPC_PED(sRCLauncherDataLocal.pedID[0],CHAR_JOSH, << 566.5068, -1772.1902, 28.3575 >>, 337.1148, "JOSH")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
		SET_PED_COMBAT_ATTRIBUTES(sRCLauncherDataLocal.pedID[0],CA_ALWAYS_FLEE,TRUE)
		
		//TASK_START_SCENARIO_AT_POSITION(sRCLauncherDataLocal.pedID[0],"WORLD_HUMAN_SEAT_STEPS",<<565.8320, -1772.9409, 28.8053>>, 335.4487)
		
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, sRCLauncherDataLocal.pedID[0], "JOSH")
	ENDIF
	
	RC_END_CUTSCENE_MODE()
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 2, PLAYER_PED_ID(), "TREVOR")
	
	iRandomInt = 2
	
	vCometSpawn1 = << -872.0014, -51.0350, 37.3370 >>
	vCometSpawn2 = << -924.3982, 8.4185, 46.7128 >>
	fHeadingCometSpawn1 = 270.2333
	fHeadingCometSpawn2 = 219.1117
	
	IF iRandomInt = 1
		vCometPos = vCometSpawn1
		fHeadingComet = fHeadingCometSpawn1
	ELIF iRandomInt = 2
		vCometPos = vCometSpawn2
		fHeadingComet = fHeadingCometSpawn2
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(blipRockfordHills)
		blipRockfordHills = CREATE_COORD_BLIP(<< -903.6451, 25.6888, 45.4577 >>)
		//SET_BLIP_ALPHA(blipRockfordHills,100)
		//SET_BLIP_SCALE(blipRockfordHills,5)
		SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(blipRockfordHills,<<-821.0944, -2.6563, 39.9561>>, 31.8236)
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(0.5)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(COMET2,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(A_M_Y_BUSINESS_03,TRUE)

	iTimerMissionStarted = GET_GAME_TIMER()

ENDPROC

/// PURPOSE: Spawn ambient cars and peds at rockford hills, start playing anims
PROC InitAmbientHouses()

	sbiLenny =  ADD_SCENARIO_BLOCKING_AREA(<<-928.948792,-9.733973,41.001431>>, <<-917.712463,26.534355,54.602386>>)
	SET_ROADS_IN_ANGLED_AREA(<<-887.370972,-101.877480,34.387718>>, <<-989.135986,60.986832,58.366745>>, 31.500000,FALSE,FALSE)
	SET_ROADS_IN_ANGLED_AREA(<<-928.948792,-9.733973,41.001431>>, <<-917.712463,26.534355,54.602386>>, 30,FALSE,FALSE)
	
	SET_ROADS_IN_ANGLED_AREA(<<-847.796692,-7.619044,37.653656>>, <<-961.217163,70.092979,56.287842>>, 28.750000,FALSE,FALSE)
	
	IF NOT DOES_ENTITY_EXIST(vehAveryComet)
		vehAveryComet = CREATE_VEHICLE(COMET2,vCometPos, fHeadingComet)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehAveryComet)
		
		SET_VEHICLE_COLOURS(vehAveryComet,139,139)
		SET_VEHICLE_EXTRA_COLOURS(vehAveryComet,90,90)
		
		iCometInitHp = GET_ENTITY_HEALTH(vehAveryComet)
		fCometEngineHp = GET_VEHICLE_PETROL_TANK_HEALTH(vehAveryComet)
		fCometPetrolHp = GET_VEHICLE_ENGINE_HEALTH(vehAveryComet)

		SET_VEHICLE_MOD_KIT(vehAveryComet,0)
		SET_VEHICLE_WINDOW_TINT(vehAveryComet,2)
		//SET_VEHICLE_MOD(vehAveryComet,MOD_SUSPENSION,2)
		//SET_VEHICLE_MOD(vehAveryComet,MOD_ENGINE,1)
		SET_VEHICLE_MOD(vehAveryComet,MOD_BRAKES,1)
		//SET_VEHICLE_MOD(vehAveryComet,MOD_HORN,0)
		ROLL_DOWN_WINDOWS(vehAveryComet)
		//TOGGLE_VEHICLE_MOD(vehAveryComet,MOD_TOGGLE_NITROUS,TRUE)
		//TOGGLE_VEHICLE_MOD(vehAveryComet,MOD_TOGGLE_TURBO,TRUE)
		//TOGGLE_VEHICLE_MOD(vehAveryComet,MOD_TOGGLE_SUBWOOFER,TRUE)
		TOGGLE_VEHICLE_MOD(vehAveryComet,MOD_TOGGLE_TYRE_SMOKE,TRUE)
		TOGGLE_VEHICLE_MOD(vehAveryComet,MOD_TOGGLE_XENON_LIGHTS,TRUE)
		//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehAveryComet,TRUE)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(pedAvery)
		
		pedAvery = CREATE_PED_INSIDE_VEHICLE(vehAveryComet,PEDTYPE_MISSION,A_M_Y_BUSINESS_03)  //Comment in for older version
		//pedAvery = CREATE_PED(PEDTYPE_MISSION,A_M_Y_BUSINESS_03,<<-923.43, 9.76, 47.71>>,255)
		
		STOP_PED_SPEAKING(pedAvery,TRUE)
		SET_PED_COMPONENT_VARIATION(pedAvery,PED_COMP_HEAD,0,2,0)
		SET_PED_COMPONENT_VARIATION(pedAvery,PED_COMP_TORSO,0,2,0)
		SET_PED_COMPONENT_VARIATION(pedAvery,PED_COMP_LEG,1,0,0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAvery,TRUE)
		SET_ENTITY_HEALTH(pedAvery,280)
		SET_ENTITY_IS_TARGET_PRIORITY(pedAvery,TRUE)
		//SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedAvery,TRUE)
		SET_PED_LEG_IK_MODE(pedAvery, LEG_IK_FULL)
		//SET_PED_COMBAT_ATTRIBUTES(pedAvery, CA_PLAY_REACTION_ANIMS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedAvery, CA_ALWAYS_FLEE, TRUE)
		//TASK_SET_DECISION_MAKER(pedAvery,DECISION_MAKER_DEFAULT)
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedAvery,TRUE)
		SET_PED_CONFIG_FLAG(pedAvery, PCF_DisableGoToWritheWhenInjured, TRUE)
		SET_PED_CONFIG_FLAG(pedAvery,PCF_DontInfluenceWantedLevel,TRUE)
		SET_PED_CONFIG_FLAG(pedAvery,PCF_PhoneDisableTextingAnimations,TRUE)
		SET_PED_CONFIG_FLAG(pedAvery,PCF_PhoneDisableTalkingAnimations,TRUE)
		//SET_PED_CONFIG_FLAG(pedAvery, PCF_ForceControlledKnockout, TRUE)
		SET_DRIVER_ABILITY(pedAvery,1)
		SET_DRIVER_RACING_MODIFIER(pedAvery,1)
		ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH,TREVOR_BOOK,FALSE)
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, pedAvery, "AVERY")
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, NULL, "JOSH")
		//TASK_START_SCENARIO_IN_PLACE(pedAvery,"WORLD_VEHICLE_BUSINESSMEN")
		//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedAvery)
	ENDIF
	
	//IF NOT DOES_ENTITY_EXIST(pedJogger)
		//pedAvery = CREATE_PED(PEDTYPE_MISSION,RUN,<<-923.43, 9.76, 47.71>>,255)
	
	IF NOT DOES_ENTITY_EXIST(vehBullet)
		vehBullet = CREATE_VEHICLE(BULLET,<< -887.6388, -16.3760, 42.0828 >>, 123.9787)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehBullet)
		SET_VEHICLE_COLOUR_COMBINATION(vehBullet,2)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehSuperd)
		vehSuperd = CREATE_VEHICLE(SUPERD,<< -921.9576, 19.3388, 46.7128 >>, 94.1395)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehSuperd)
		SET_VEHICLE_COLOUR_COMBINATION(vehSuperd,0)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(vehJoshFelon)
		IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
			vehJoshFelon = CREATE_VEHICLE(FELON2,<<558.093933,-1765.382568,28.862253>>, -24.676498)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehJoshFelon)
			SET_VEHICLE_COLOUR_COMBINATION(vehJoshFelon,2)
		ENDIF
	ENDIF

	IF NOT DOES_ENTITY_EXIST(vehGauntlet)
		vehGauntlet = CREATE_VEHICLE(COMET2,<< -877.5642, 43.1467, 47.7590 >>,81.1951)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehGauntlet)
		SET_VEHICLE_COLOUR_COMBINATION(vehGauntlet,3)  
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehElegy)
		IF iRandomInt = 1
			vehElegy = CREATE_VEHICLE(SUPERD,vCometSpawn2, fHeadingCometSpawn2)
		ELIF iRandomInt = 2
			vehElegy = CREATE_VEHICLE(SUPERD,vCometSpawn1, fHeadingCometSpawn1)
		ENDIF
		SET_VEHICLE_COLOUR_COMBINATION(vehElegy,2)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehElegy)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(pedViewerHusband)
		//pedViewerHusband = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_01,<<-925.603394,7.467144,47.749523>>, 150)  //  -54.312847 167.031372
		
		pedViewerHusband = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_01,<<-923.43, 9.76, 47.71>>, 150)           //Comment in for older version
		//pedViewerHusband = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_01,<<-922.920837,10.754505,47.713017>>, 150)  // <<-923.43, 9.76, 47.71>>// -54.312847 167.031372
		
		TASK_START_SCENARIO_IN_PLACE(pedViewerHusband,"WORLD_HUMAN_STAND_MOBILE")
		SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedViewerHusband)
		SET_PED_COMPONENT_VARIATION(pedViewerHusband,PED_COMP_HEAD,0,0,0)
		SET_PED_COMPONENT_VARIATION(pedViewerHusband,PED_COMP_TORSO,0,0,0)
		SET_PED_COMPONENT_VARIATION(pedViewerHusband,PED_COMP_LEG,0,0,0)
		SET_PED_COMPONENT_VARIATION(pedViewerHusband,PED_COMP_SPECIAL,0,0,0)
		SET_PED_COMPONENT_VARIATION(pedViewerHusband,PED_COMP_DECL,0,0,0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedViewerHusband,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedViewerHusband,CA_ALWAYS_FLEE,TRUE)
		TASK_LOOK_AT_ENTITY(pedViewerHusband,pedAvery, -1)
		TASK_LOOK_AT_ENTITY(pedAvery,pedViewerHusband, -1, SLF_WHILE_NOT_IN_FOV)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehBati)		
		vehBati = CREATE_VEHICLE(BATI,<< -888.3505, -8.9959, 42.3460 >>, 208.0802)
		SET_VEHICLE_COLOUR_COMBINATION(vehBati,1)  
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedBiker)	
		pedBiker = CREATE_PED(PEDTYPE_MISSION,A_M_M_BevHills_01,<<-890.075928,-9.350061,43.329903>>, -62.379219)
		SET_PED_COMPONENT_VARIATION(pedBiker,PED_COMP_HEAD,0,2,0)
		SET_PED_COMPONENT_VARIATION(pedBiker,PED_COMP_TORSO,1,1,0)
		SET_PED_COMPONENT_VARIATION(pedBiker,PED_COMP_LEG,0,1,0)
		SET_PED_COMPONENT_VARIATION(pedBiker,PED_COMP_SPECIAL,1,1,0)
		SET_PED_COMPONENT_VARIATION(pedBiker,PED_COMP_DECL,1,2,0)
		SET_PED_COMBAT_ATTRIBUTES(pedBiker,CA_ALWAYS_FLEE,TRUE)
		STOP_PED_SPEAKING(pedBiker,TRUE)
		ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, pedBiker, "Josh2PoshBiker")
	ENDIF

	IF IS_PED_UNINJURED(pedBiker)	
		TASK_START_SCENARIO_IN_PLACE(pedBiker,"WORLD_HUMAN_SMOKING")
	ENDIF
	
	bRockfordInit = TRUE

ENDPROC

/// PURPOSE: Handle ambient biker at rockford hills
PROC AmbientBiker()

	IF IS_PED_UNINJURED(pedBiker)
		IF NOT IS_PED_HEADTRACKING_PED(pedBiker,PLAYER_PED_ID())
			TASK_LOOK_AT_ENTITY(pedBiker,PLAYER_PED_ID(),-1)
		ENDIF
		IF iBikerInCombat <> 2
			IF iBikerInCombat <> 1
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBiker,PLAYER_PED_ID())
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedBiker)
					SET_PED_COMBAT_ATTRIBUTES(pedBiker,CA_ALWAYS_FLEE,TRUE)
					//CLEAR_PED_TASKS(pedBiker)
					//TASK_SMART_FLEE_PED(pedBiker,PLAYER_PED_ID(),300,-1)
					//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_5", CONV_PRIORITY_MEDIUM)   //Help! This tweaker is trying to rape me!
					CREATE_CONVERSATION(s_conversation_peds,"JOSH2AU", "JOSH2_ATTB", CONV_PRIORITY_MEDIUM)		
					TASK_REACT_AND_FLEE_PED(pedBiker,PLAYER_PED_ID())
					iBikerInCombat = 2
				ENDIF
			ENDIF
			IF HAS_PLAYER_THREATENED_PED(pedBiker,TRUE)	
				IF IS_PED_ARMED(PLAYER_PED_ID(),WF_INCLUDE_PROJECTILE | WF_INCLUDE_GUN)
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedBiker)
					SET_PED_COMBAT_ATTRIBUTES(pedBiker,CA_ALWAYS_FLEE,TRUE)
					//CLEAR_PED_TASKS(pedBiker)
					//TASK_SMART_FLEE_PED(pedBiker,PLAYER_PED_ID(),300,-1)
					//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_5", CONV_PRIORITY_MEDIUM)   //Help! This tweaker is trying to rape me!
					CREATE_CONVERSATION(s_conversation_peds,"JOSH2AU", "JOSH2_ATTB", CONV_PRIORITY_MEDIUM)	
					TASK_REACT_AND_FLEE_PED(pedBiker,PLAYER_PED_ID())
					iBikerInCombat = 2
				ENDIF
			ENDIF
		ENDIF
		IF iBikerInCombat = 0	
			/*
			IF IS_PED_RAGDOLL(pedBiker)
			OR HAS_PLAYER_THREATENED_PED(pedBiker,TRUE)
			//OR IS_PLAYER_SHOOTING_NEAR_PED(pedBiker)	
			//OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),pedBiker)
				IF GET_SCRIPT_TASK_STATUS(pedBiker,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK                                   
					IF GET_SCRIPT_TASK_STATUS(pedBiker,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
						TASK_SMART_FLEE_PED(pedBiker,PLAYER_PED_ID(),1000.0,-1)
					ENDIF
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_5", CONV_PRIORITY_MEDIUM)
						iBikerInCombat = TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			IF IS_VEHICLE_OK(vehBati)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehBati)
					//IF GET_SCRIPT_TASK_STATUS(pedBiker,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
						SET_PED_COMBAT_ATTRIBUTES(pedBiker,CA_ALWAYS_FLEE,FALSE)
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedBiker)
						TASK_COMBAT_PED(pedBiker,PLAYER_PED_ID())                                 //Hey! That's my bike!
						//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_3", CONV_PRIORITY_MEDIUM)
						IF CREATE_CONVERSATION(s_conversation_peds,"JOSH2AU", "JOSH2_STEALB", CONV_PRIORITY_MEDIUM)	
							iBikerInCombat = 1	
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(vehBullet)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehBullet)
					//IF GET_SCRIPT_TASK_STATUS(pedBiker,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
						SET_PED_COMBAT_ATTRIBUTES(pedBiker,CA_ALWAYS_FLEE,FALSE)
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedBiker)
						TASK_COMBAT_PED(pedBiker,PLAYER_PED_ID())                                 //Hey! That's my car!
						//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_4", CONV_PRIORITY_MEDIUM)
						IF CREATE_CONVERSATION(s_conversation_peds,"JOSH2AU", "JOSH2_STEALC", CONV_PRIORITY_MEDIUM)		
							iBikerInCombat = 1
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ELSE
			//IF GET_SCRIPT_TASK_STATUS(pedBiker,SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
			//	TASK_COMBAT_PED(pedBiker,PLAYER_PED_ID())      
			//ENDIF
			//IF iBikerInCombat = 1	
				
			//ENDIF
		ENDIF
	ENDIF
				
ENDPROC
/*
/// PURPOSE: Spawn random traffic
PROC RandomTraffic1()

	vSpawnTraffic1[0] = << -903.4059, 78.3809, 50.9436 >>
	vSpawnTraffic1[1] = << -762.4873, 98.5261, 54.3848 >>
	vSpawnTraffic1[2] = << -670.0525, 125.2276, 55.8838 >>

	fHeadingTraffic1[0] = 266.2728
	fHeadingTraffic1[1] = 270.6989
	fHeadingTraffic1[2] = 272.9075

	vDestTraffic1 = << -418.3191, 124.1427, 64.1284 >>

	i = 0
	
	WHILE i < 3
		vehTraffic1[i] = CREATE_VEHICLE(BULLET,vSpawnTraffic1[i],fHeadingTraffic1[i])
		pedRandom[i]Driver1[i] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic1[i])
		SET_PED_KEEP_TASK(pedRandom[i]Driver1[i],TRUE)
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandom[i]Driver1[i],vehTraffic1[i],vDestTraffic1,14.0,DRIVINGSTYLE_NORMAL,GET_ENTITY_MODEL(vehTraffic1[i]),DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,1.0,1.0)
		++i
	ENDWHILE
	
ENDPROC
*/
/*
/// PURPOSE: Spawn random traffic
PROC RandomTraffic2()

	vSpawnTraffic2[0] = << -699.6869, 119.8908, 55.3086 >>
	vSpawnTraffic2[1] = << -822.3788, 91.7024, 52.0150 >>
	vSpawnTraffic2[2] = << -913.1359, 85.5499, 50.8899 >>

	fHeadingTraffic2[0] = 118.2277
	fHeadingTraffic2[1] = 104.9941
	fHeadingTraffic2[2] = 76.4767

	vDestTraffic2 = << -983.3253, 81.6117, 50.8200 >>

	i = 0
	
	WHILE i < 3
		vehTraffic2[i] = CREATE_VEHICLE(BULLET,vSpawnTraffic2[i],fHeadingTraffic2[i])
		pedRandom[i]Driver2[i] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic2[i])
		SET_PED_KEEP_TASK(pedRandom[i]Driver2[i],TRUE)
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandom[i]Driver2[i],vehTraffic2[i],vDestTraffic2,14.0,DRIVINGSTYLE_NORMAL,GET_ENTITY_MODEL(vehTraffic2[i]),DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,1.0,1.0)
		++i
	ENDWHILE

ENDPROC
*/
/// PURPOSE: Handle conversations
PROC Convo()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
		IF eMissionStage = MS_GO_TO_ROCKFORD	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_01")	
					IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),30)
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
							IF IS_CHAR_USING_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])
								IF bConvoFelon = FALSE
									IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_FELON", CONV_PRIORITY_MEDIUM)  //Hey, that's my car!
										bConvoFelon = TRUE
									ENDIF
								ENDIF
							ENDIF
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[0],PLAYER_PED_ID())
							AND NOT IS_ANY_SPEECH_PLAYING(sRCLauncherDataLocal.pedID[0])
							AND iCounterDialogueDamageCar < 3	
							AND GET_GAME_TIMER() > iTimerDialogueDamageCar + 8000	
							AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),sRCLauncherDataLocal.vehID[0])	
								PLAY_PED_AMBIENT_SPEECH(sRCLauncherDataLocal.pedID[0], "WHATS_YOUR_PROBLEM",SPEECH_PARAMS_FORCE)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[0])
								bConvoFelon = TRUE
								iTimerDialogueDamageCar = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,4000)
								++iCounterDialogueDamageCar
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_GAME_TIMER() > iTimerMissionStarted + 15000
					IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0]) < 30.0
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF iJoshConvoCounter = 0	                    //So... You going to see Lenny or not?
									IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOSH",CONV_PRIORITY_LOW)	
										iJoshConvoCounter = 1
										iTimerJoshSpeak = GET_GAME_TIMER()
									ENDIF
								ELIF iJoshConvoCounter = 1                      //Are you ok Tony?
									IF GET_GAME_TIMER() > iTimerJoshSpeak + 10000
										IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOSH",CONV_PRIORITY_LOW)	
											iJoshConvoCounter = 2
										ENDIF
									ENDIF
								ELIF iJoshConvoCounter = 2                      //Damn, I think I got a splinter.
									IF GET_GAME_TIMER() > iTimerJoshSpeak + 19000	
										IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOSH",CONV_PRIORITY_LOW)	
											iJoshConvoCounter = 3
										ENDIF
									ENDIF
								ELIF iJoshConvoCounter = 3                      //Damn, I think I got a splinter.
									IF GET_GAME_TIMER() > iTimerJoshSpeak + 27000	
										IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOSH",CONV_PRIORITY_LOW)	
											iJoshConvoCounter = 4
										ENDIF
									ENDIF
								ELIF iJoshConvoCounter = 4                      //Damn, I think I got a splinter.
									IF GET_GAME_TIMER() > iTimerJoshSpeak + 35000	
										IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_JOSH",CONV_PRIORITY_LOW)	
											iJoshConvoCounter = 5
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-853.674500,-52.276905,31.257320>>, <<-918.126526,47.042919,121.331253>>, 75.000000)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-879.910889,56.844849,38.783661>>, <<-958.235535,59.315208,124.611870>>, 25.000000)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-859.330750,40.053410,29.865433>>, <<-937.509583,8.682496,123.805862>>, 68.250000)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-841.455811,-15.820283,54.401230>>, <<-859.777344,16.357359,39.161922>>, 24.500000)		
				IF iConvoCounter = 0
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,2200.0)
						IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_TREV2", CONV_PRIORITY_MEDIUM)  //Well, that was pleasant.
							iConvoCounter = 1
						ENDIF
					ENDIF
				ELIF iConvoCounter = 1 
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,1170.0)
							IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,400.0)	
								IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(s_conversation_peds,CHAR_JOSH,"JOSH2AU", "JOSH2_CALL",CONV_PRIORITY_VERY_HIGH)
									iConvoCounter = 2
								ENDIF
							ELSE
								iTimerPlayerHungUpPhone = GET_GAME_TIMER() - 8000
								iConvoCounter = 3
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF iConvoCounter = 2	
					IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
						iTimerPlayerHungUpPhone = GET_GAME_TIMER()
						iConvoCounter = 3
					ENDIF
				ENDIF
				IF iConvoCounter = 3	
					IF GET_GAME_TIMER() > iTimerPlayerHungUpPhone + 9000		
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_JOSH,"JOSH2_TXT",TXTMSG_UNLOCKED)	
							iConvoCounter = 4
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iConvoCounter <> 2
				IF bStartChase = TRUE
					iConvoCounter = 2
				ENDIF
			ENDIF
		ELIF eMissionStage = MS_FIND_THE_GREEN_COMET
			IF iConvoCounter <> 2
				IF bStartChase = TRUE
					iConvoCounter = 2
				ENDIF
			ENDIF
			IF bPrintWrongColourComet = FALSE
				IF IS_VEHICLE_OK(vehGauntlet)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehGauntlet) < 14.0        //Right make... Wrong color...
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_COMET", "JOSH2_COMET_1", CONV_PRIORITY_MEDIUM)
							bPrintWrongColourComet = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-893.643860,-0.506663,37.989750>>, <<-884.396484,-14.709142,47.418583>>, 9.500000)
				IF iBikerInCombat = 0
					IF iBikerConvoCounter = 0                                                     //You seen an old guy with a yellow comet?
						//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_1", CONV_PRIORITY_MEDIUM)
							iBikerConvoCounter = 1
						//ENDIF
					ELIF iBikerConvoCounter = 1                                                   //You mean Lenny? Don't think I've seen him today.
						//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_BIKE", "JOSH2_BIKE_2", CONV_PRIORITY_MEDIUM)
							iBikerConvoCounter = 2
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF eMissionStage = MS_CHASE_AVERY
			IF GET_GAME_TIMER() > iTimerChaseStarted + 7000	
				IF IS_PED_SITTING_IN_ANY_VEHICLE(pedAvery)	
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT bWaypointPlaybackStopped
					IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedAvery) < 16.0	
						IF iConvoCounter = 5
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CHASE2", "JOSH2_CHASE2_7", CONV_PRIORITY_MEDIUM)
								iConvoCounter = 6
							ENDIF
						ENDIF
						IF iConvoCounter = 4
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CHASE2", "JOSH2_CHASE2_5", CONV_PRIORITY_MEDIUM)
								iConvoCounter = 5
							ENDIF
						ENDIF
						IF iConvoCounter = 3
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CHASE2", "JOSH2_CHASE2_3", CONV_PRIORITY_MEDIUM)
								iConvoCounter = 4
							ENDIF
						ENDIF
						IF iConvoCounter <= 2
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CHASE2", "JOSH2_CHASE2_1", CONV_PRIORITY_MEDIUM)
								iConvoCounter = 3
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF eMissionStage = MS_AVERY_KO
			/*
			IF iConvoCounter <> 7
				IF GET_GAME_TIMER() > iTimerAveryKO + 1100	
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO", CONV_PRIORITY_VERY_HIGH)  //My beautiful face! Please don't hit me again. I'll do whatever you want.		
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						iConvoCounter = 7
					ENDIF
				ENDIF
			ENDIF
			*/
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Rubberbanding for Avery during chase
PROC Rubberband()

	fTopSpeed = (150.0 - (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehAveryComet))) 
	fTopSpeed = (fTopSpeed / 5)
	IF fTopSpeed < 25.0   //14
		fTopSpeed = 25.0
	ENDIF
	IF fTopSpeed > 40.0   //27
		fTopSpeed = 40.0
	ENDIF

ENDPROC

/// PURPOSE: Remove all blips
PROC RemoveBlips()
		
	SAFE_REMOVE_BLIP(blipRockfordHills) 
	SAFE_REMOVE_BLIP(blipAvery)
	SAFE_REMOVE_BLIP(blipCSDoor)

ENDPROC

PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()	
	ENDIF
	
	//RELEASE_FAKE_CELLPHONE_MOVIE(sJoshFakeCellphoneData)
	//RELEASE_NPC_PHONE_RENDERTARGET()
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	CLEANUP_AUDIO_SCENE_CHASE()  
	/*
	IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_NPC_Phone")
		RELEASE_NAMED_RENDERTARGET("Prop_NPC_Phone")
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("CELLPHONE_CUTSCENE")
		RELEASE_NAMED_RENDERTARGET("CELLPHONE_CUTSCENE")
	ENDIF
	*/
	STREAMVOL_DELETE(StreamVol)
	bStreamVolCreated = FALSE
	
	SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
	REMOVE_SCENARIO_BLOCKING_AREA(sbiLenny)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[0])
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[0])
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[1])	
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[1])
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[2])	
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[2])
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[3])	
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[3])
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[4])	
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[4])
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[5])	
		REMOVE_DOOR_FROM_SYSTEM(Josh2Door[5])
	ENDIF
	
	// B*2079566 - RESET disable taxis during the "go to door" state to fix PT issue
	IF bDisabledTaxis = TRUE
		DISABLE_TAXI_HAILING(FALSE)
		bDisabledTaxis = FALSE
		CPRINTLN(DEBUG_MISSION, " Script_Cleanup : bDisabledTaxis = FALSE")
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(COMET2)
	SET_MODEL_AS_NO_LONGER_NEEDED(GAUNTLET)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BUSINESS_03)
	SET_MODEL_AS_NO_LONGER_NEEDED(BULLET)
	SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)
	SET_MODEL_AS_NO_LONGER_NEEDED(FELON2)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(COMET2,FALSE)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_ROADS_IN_ANGLED_AREA(<<-887.370972,-101.877480,34.387718>>, <<-989.135986,60.986832,58.366745>>, 31.500000,FALSE,TRUE)
	SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,100,FALSE,TRUE)
	
	//SET_DISABLE_AMBIENT_MELEE_MOVE(PLAYER_ID(),FALSE)
	/*
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_a"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_b"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_rear"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_heavy"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_armed_takedown"), TRUE) 
	*/
	SAFE_RELEASE_VEHICLE(vehRandom[1])
	SAFE_RELEASE_VEHICLE(vehRandom[2])
	SAFE_RELEASE_VEHICLE(vehRandom[3])
	SAFE_RELEASE_VEHICLE(vehRandom[4])
	SAFE_RELEASE_VEHICLE(vehRandom[5])
	SAFE_RELEASE_VEHICLE(vehRandom[6])
	SAFE_RELEASE_VEHICLE(vehRandom[7])
	SAFE_RELEASE_VEHICLE(vehRandom[8])
	SAFE_RELEASE_VEHICLE(vehRandom[9])
	SAFE_RELEASE_VEHICLE(vehRandom[10])
	SAFE_RELEASE_VEHICLE(vehRandom[11])
	SAFE_RELEASE_VEHICLE(vehRandom[12])
	SAFE_RELEASE_VEHICLE(vehRandom[13])
	SAFE_RELEASE_VEHICLE(vehRandom[14])
	SAFE_RELEASE_PED(pedRandom[1])
	SAFE_RELEASE_PED(pedRandom[2])
	SAFE_RELEASE_PED(pedRandom[3])
	SAFE_RELEASE_PED(pedRandom[4])
	SAFE_RELEASE_PED(pedRandom[5])
	SAFE_RELEASE_PED(pedRandom[6])
	SAFE_RELEASE_PED(pedRandom[7])
	SAFE_RELEASE_PED(pedRandom[8])
	SAFE_RELEASE_PED(pedRandom[9])
	SAFE_RELEASE_PED(pedRandom[10])
	SAFE_RELEASE_PED(pedRandom[11])
	SAFE_RELEASE_PED(pedRandom[12])
	SAFE_RELEASE_PED(pedRandom[13])
	SAFE_RELEASE_PED(pedRandom[14])
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "JOSH2**** SCRIPT TERMINATING ****")
	#ENDIF
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal,FALSE,FALSE) //Cleanup the scene created by the launcher
	TERMINATE_THIS_THREAD()

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	
	IF IS_PED_UNINJURED(pedAvery)
		IF NOT bAveryFleeingOnFoot
			CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_END", CONV_PRIORITY_MEDIUM)	
		ENDIF
	ENDIF
	
	Random_Character_Passed(CP_RAND_C_JOS2,FALSE)
	Script_Cleanup()

ENDPROC

FUNC VECTOR GET_VECTOR_FROM_HEADING(FLOAT fHeading)
	RETURN <<SIN(fHeading), COS(fHeading), 0.0>>
ENDFUNC

/// PURPOSE: Start the chase
PROC InitChase()

	IF IS_PED_UNINJURED(pedAvery)
		IF IS_PED_UNINJURED(pedViewerHusband)
			OPEN_SEQUENCE_TASK(seqAngryHusband)
				//TASK_LOOK_AT_ENTITY(NULL,pedAvery,-1)
				/*
				IF bCometReverse = FALSE	
					TASK_ACHIEVE_HEADING(NULL,167.031372)
				ELSE
					TASK_ACHIEVE_HEADING(NULL,255.0)
				ENDIF
				*/
				IF bCometReverse = TRUE	
					TASK_ACHIEVE_HEADING(NULL,255.0)
				ELSE
					TASK_PLAY_ANIM(NULL,"rcmjosh2","house_viewer_yelling",REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)
				ENDIF
				
				//TASK_PLAY_ANIM(NULL,"rcmjosh2","house_viewer_yelling",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
				
				//IF bCometReverse = FALSE
				//	TASK_GO_STRAIGHT_TO_COORD(NULL,<< -925.47, 6.47, 46.64 >>,PEDMOVEBLENDRATIO_SPRINT,DEFAULT_TIME_NEVER_WARP) //184.6828
				//ENDIF
				
				
				/*
				IF bCometReverse = FALSE	
					//TASK_PLAY_ANIM_ADVANCED(NULL,"rcmjosh2","house_viewer_yelling",GET_ENTITY_COORDS(pedViewerHusband),GET_VECTOR_FROM_HEADING(167.031372 + 180))
					TASK_PLAY_ANIM_ADVANCED(NULL,"rcmjosh2","house_viewer_yelling",GET_ENTITY_COORDS(pedViewerHusband),<<0,0,150>>,REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET)
					//TASK_PLAY_ANIM(NULL,"rcmjosh2","house_viewer_yelling",REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)
				ELSE
					//TASK_PLAY_ANIM_ADVANCED(NULL,"rcmjosh2","house_viewer_yelling",GET_ENTITY_COORDS(pedViewerHusband),<<0,0,255>>,REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_EXTRACT_INITIAL_OFFSET)
					TASK_PLAY_ANIM(NULL,"rcmjosh2","house_viewer_yelling",REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)
				ENDIF
				*/
				/*
				IF bCometReverse = FALSE
					TASK_GO_STRAIGHT_TO_COORD(NULL,<< -924.4344, 6.2271, 46.6354 >>,PEDMOVEBLENDRATIO_RUN,DEFAULT_TIME_NEVER_WARP,167) //184.6828
				ELSE
					TASK_GO_STRAIGHT_TO_COORD(NULL,<<-923.1736, 8.2633, 46.7130>>,PEDMOVEBLENDRATIO_RUN,DEFAULT_TIME_NEVER_WARP,255)
				ENDIF
				*/
				/*
				IF bCometReverse = FALSE
					TASK_GO_STRAIGHT_TO_COORD(NULL,<<-925.65, 4.04, 46.22>>,PEDMOVEBLENDRATIO_RUN,DEFAULT_TIME_NEVER_WARP,150) //167 //184.6828
				ELSE
					TASK_GO_STRAIGHT_TO_COORD(NULL,<<-924.75, 7.64, 46.69>>,PEDMOVEBLENDRATIO_RUN,DEFAULT_TIME_NEVER_WARP,280) //255
				ENDIF
				*/
				//TASK_USE_MOBILE_PHONE(NULL,TRUE)
			CLOSE_SEQUENCE_TASK(seqAngryHusband)
		ENDIF
		IF IS_PED_UNINJURED(pedAvery)
		AND IS_VEHICLE_OK(vehAveryComet)
			SET_ENTITY_LOAD_COLLISION_FLAG(pedAvery,TRUE)
			IF NOT DOES_BLIP_EXIST(blipAvery)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedAvery) < 100.0	
					IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
						//IF IS_PED_UNINJURED(pedViewerHusband)
							//ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, pedViewerHusband, "Josh2Viewer")
							//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_VIEW", "JOSH2_VIEW_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES) //Hey! Where are you going?	
							//CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_VIEW",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
						//ENDIF
						TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedAvery,-1)
						SET_PED_SUFFERS_CRITICAL_HITS(pedAvery,FALSE)
						iTimerChaseStarted = GET_GAME_TIMER()
						SAFE_REMOVE_BLIP(blipRockfordHills)
						//PRINT_HELP("JOSH2_10")                              //The mission will fail if Avery dies.
						PRINT_NOW("JOSH2_04",DEFAULT_GOD_TEXT_TIME,0) 						//Ram ~r~Avery's car~s~ off the road.
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Chase Avery",TRUE)
						blipAvery = CREATE_PED_BLIP(pedAvery)
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JO2_STOPPED_IN_TIME)
						//IF IS_PED_IN_ANY_VEHICLE(pedAvery)
							SET_BLIP_SCALE(blipAvery,BLIP_SIZE_VEHICLE)
						//ELSE
						//	SET_BLIP_SCALE(blipAvery,BLIP_SIZE_PED)
						//ENDIF
						SET_WANTED_LEVEL_MULTIPLIER(0.2)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedAvery,"JOSH2_BCAA","AVERY","SPEECH_PARAMS_STANDARD")
					ENDIF
				ENDIF
			ENDIF   
			IF iRandomInt = 1	
				//IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK	
				//	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Comet10",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
				//ENDIF
			ELIF iRandomInt = 2
				IF bCometReverse = TRUE	
					//IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
					//	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Comet11",DRIVINGMODE_AVOIDCARS_RECKLESS,1,EWAYPOINT_DEFAULT,30,-1)
					//ENDIF
					IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
							START_PLAYBACK_RECORDED_VEHICLE(vehAveryComet, 102, "Josh2_A2")
						ENDIF
					ENDIF
				ELSE
					//IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)	
					//	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Comet10",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,-1)
					//ENDIF
					IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
							START_PLAYBACK_RECORDED_VEHICLE(vehAveryComet, 101, "Josh2_A1")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)
				//IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)	
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "JOSH2**** Init chase ****")
					#ENDIF
					eMissionStage = MS_CHASE_AVERY
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Open gates of houses at rockford hills
PROC OpenGates()	
	/*
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-824.44, -33.59, 38.71>>,15.0,prop_lrggate_01_r)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01_r,<<-824.44, -33.59, 38.71>>,TRUE,1.1)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-827.30, -29.11, 38.87>>,15.0,prop_lrggate_01_l)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01_l,<<-827.30, -29.11, 38.87>>,TRUE,-1.1)                             
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-924.975342,-9.033067,43.281506>>,10.0,prop_lrggate_03a)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-924.975342,-9.033067,43.281506>>,TRUE,1.0)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-905.696655,14.659701,45.675934>>,10.0,prop_lrggate_03a)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-905.696655,14.659701,45.675934>>,TRUE,1.0)                             
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-881.16, -12.75, 42.39>>,10.0,prop_lrggate_02_ld)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_02_ld,<<-881.16, -12.75, 42.39>>,TRUE,1.0)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-889.88, 6.78, 43.99>>,10.0,prop_lrggate_02_ld)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_02_ld,<<-889.88, 6.78, 43.99>>,TRUE,1.0)                             
	ENDIF
	*/

	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-824.44, -33.59, 38.71>>,15.0,prop_lrggate_01_r)   
		Josh2Door[0] = get_hash_key("Josh2_door_0") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[0])
			add_door_to_system(Josh2Door[0], prop_lrggate_01_r, <<-824.44, -33.59, 38.71>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[0], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[0], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01_r,<<-824.44, -33.59, 38.71>>,TRUE,1.1)          
	ENDIF

	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-827.30, -29.11, 38.87>>,15.0,prop_lrggate_01_l) 	
		Josh2Door[1] = get_hash_key("Josh2_door_1") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[1])
			add_door_to_system(Josh2Door[1], prop_lrggate_01_l, <<-827.30, -29.11, 38.87>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[1], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[1], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01_l,<<-827.30, -29.11, 38.87>>,TRUE,-1.1) 
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-924.975342,-9.033067,43.281506>>,10.0,prop_lrggate_03a)          
		Josh2Door[2] = get_hash_key("Josh2_door_2") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[2])	
			add_door_to_system(Josh2Door[2], prop_lrggate_03a, <<-924.975342,-9.033067,43.281506>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[2], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[2], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-924.975342,-9.033067,43.281506>>,TRUE,1.0)   
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-905.696655,14.659701,45.675934>>,10.0,prop_lrggate_03a)  //Avery Gate       
		Josh2Door[3] = get_hash_key("Josh2_door_3") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[3])	
			add_door_to_system(Josh2Door[3], prop_lrggate_03a, <<-905.696655,14.659701,45.675934>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[3], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[3], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-905.696655,14.659701,45.675934>>,TRUE,1.0)
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-881.16, -12.75, 42.39>>,10.0,prop_lrggate_02_ld)  	
		Josh2Door[4] = get_hash_key("Josh2_door_4") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[4])
			add_door_to_system(Josh2Door[4], prop_lrggate_02_ld, <<-881.16, -12.75, 42.39>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[4], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[4], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_02_ld,<<-881.16, -12.75, 42.39>>,TRUE,1.0)         
	ENDIF
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-889.88, 6.78, 43.99>>,10.0,prop_lrggate_02_ld)     
		Josh2Door[5] = get_hash_key("Josh2_door_5") 
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[5])	
			add_door_to_system(Josh2Door[5], prop_lrggate_02_ld, <<-889.88, 6.78, 43.99>>)
		ENDIF
		//DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[5], 1.0, false, false) 
		//DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[5], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_02_ld,<<-889.88, 6.78, 43.99>>,TRUE,1.0)    
	ENDIF
	
ENDPROC

/// PURPOSE: Check all the fail conditions
PROC CheckForFail()
	
	IF DOES_ENTITY_EXIST(pedAvery)
		IF IS_ENTITY_DEAD(pedAvery)     
			sFailReason = "JOSH2_F2"	 //~r~Avery died.		
			eMissionStage = MS_MISSION_FAILING
			EXIT
		ELSE
			IF IS_PED_INJURED(pedAvery)
				sFailReason = "JOSH2_F2"		 //~r~Avery died.	
				eMissionStage = MS_MISSION_FAILING
				EXIT
			ELSE
				IF eMissionStage = MS_CHASE_AVERY
				OR eMissionStage = MS_GO_TO_AVERY
				OR eMissionStage = MS_AVERY_KO
					IF DOES_BLIP_EXIST(blipAvery)
						IF NOT IS_PED_IN_ANY_VEHICLE(pedAvery)
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),280)
								sFailReason = "JOSH2_F1" //~r~Avery escaped.
								eMissionStage = MS_MISSION_FAILING
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF bStartChase = TRUE
					IF GET_GAME_TIMER() > iTimerChaseStarted + 5000	
						IF GET_GAME_TIMER() > iTimerDistanceWarning + 10000 	
							IF bDistanceWarningPrinted = TRUE	
								IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedAvery) > 280.0                                 
									sFailReason = "JOSH2_F1" //~r~Avery escaped.
									eMissionStage = MS_MISSION_FAILING
									EXIT
								ENDIF
							ENDIF
						ENDIF
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedAvery) > 200.0   
							IF bDistanceWarningPrinted = FALSE
								IF DOES_BLIP_EXIST(blipAvery)
									//PRINT_NOW("JOSH2_W1",DEFAULT_GOD_TEXT_TIME,0)   //Don't let ~r~Avery~s~ escape.
								ELSE
									//PRINT_NOW("JOSH2_09",DEFAULT_GOD_TEXT_TIME,0)   //Avery is escaping.
								ENDIF
								iTimerDistanceWarning = GET_GAME_TIMER()
								bDistanceWarningPrinted = TRUE
							ENDIF
						ELSE
							bDistanceWarningPrinted = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		IF IS_ENTITY_DEAD(sRCLauncherDataLocal.pedID[0])
		OR IS_PED_INJURED(sRCLauncherDataLocal.pedID[0])
			sFailReason = "JOSH2_14"             					//~r~Josh died.
			eMissionStage = MS_MISSION_FAILING
		ELSE	
			IF eMissionStage = MS_INTRO
				VECTOR vJosh = GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[0])
				IF IS_BULLET_IN_AREA(vJosh,20,FALSE)
				OR DOES_VEHICLE_OVERLAP_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(),<<566.619324,-1771.675415,28.357866>>, <<565.955750,-1773.148315,29.805521>>, 1.290000)
				//OR IS_PROJECTILE_IN_AREA(vJosh - <<5,5,5>>,vJosh + <<5,5,5>>,TRUE)	
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//TASK_REACT_AND_FLEE_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID())
					TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),100,-1)
					SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[0],TRUE)
					sFailReason = "JOSH2_11"             					//~r~You scared Josh.
					eMissionStage = MS_MISSION_FAILING
				ENDIF
				IF IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),30)
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					IF IS_COP_VEHICLE_IN_AREA_3D(GET_ENTITY_COORDS(PLAYER_PED_ID())-<<30.0, 30.0, 30.0>>, GET_ENTITY_COORDS(PLAYER_PED_ID())+<<30.0, 30.0, 30.0>>)
					OR IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(PLAYER_PED_ID())-<<30.0, 30.0, 30.0>>, GET_ENTITY_COORDS(PLAYER_PED_ID())+<<30.0, 30.0, 30.0>>)	
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),100,-1)
						SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[0],TRUE)
						sFailReason = "JOSH2_11"             					//~r~You scared Josh.
						eMissionStage = MS_MISSION_FAILING
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[0])	
				bJoshTasksCleared = TRUE
				sFailReason = "JOSH2_13"             					//~r~You injured Josh.
				eMissionStage = MS_MISSION_FAILING
			ENDIF
			IF bJoshTasksCleared = TRUE
				IF IS_PED_USING_ANY_SCENARIO(sRCLauncherDataLocal.pedID[0])
					SET_PED_PANIC_EXIT_SCENARIO(sRCLauncherDataLocal.pedID[0],GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
			ENDIF
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID())
				//TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_sitting_loop",1,-1)
				//SET_PED_TO_RAGDOLL(sRCLauncherDataLocal.pedID[0],1000,2000,TASK_RELAX)
				IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[0],SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
					IF IS_PED_USING_ANY_SCENARIO(sRCLauncherDataLocal.pedID[0])
						SET_PED_PANIC_EXIT_SCENARIO(sRCLauncherDataLocal.pedID[0],GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF
					SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[0],TRUE)
					//TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),300.0,-1)         
				ENDIF  
				sFailReason = "JOSH2_13"             					//~r~You injured Josh.
				eMissionStage = MS_MISSION_FAILING
			ELSE
				/*
				IF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[0])
					//TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_sitting_loop",1,-1)
					//SET_PED_TO_RAGDOLL(sRCLauncherDataLocal.pedID[0],1000,2000,TASK_RELAX)
					IF GET_SCRIPT_TASK_STATUS(sRCLauncherDataLocal.pedID[0],SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
						IF IS_PED_USING_ANY_SCENARIO(sRCLauncherDataLocal.pedID[0])
							SET_PED_PANIC_EXIT_SCENARIO(sRCLauncherDataLocal.pedID[0],GET_ENTITY_COORDS(PLAYER_PED_ID()))
						ENDIF
						SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[0],TRUE)
						TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),300.0,-1)         
					ENDIF  
					IF iJoshScared = 0
						KILL_ANY_CONVERSATION()
						iJoshScared = 1
					ENDIF
					sFailReason = "JOSH2_11"             					//~r~You scared Josh.
					eMissionStage = MS_MISSION_FAILING
				ENDIF
				*/
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: skip from MS_GO_TO_ROCKFORD
PROC SkipStage1()
	
	#IF IS_DEBUG_BUILD	
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		
			IF IS_VEHICLE_OK(vehPlayer)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer)
			ELSE
				CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< -851.0237, 4.1346, 41.9042 >>,86.4924,TRUE,TRUE,FALSE,TRUE,TRUE,FELON2,2)
			ENDIF
		ENDIF
	#ENDIF
	
	RequestRockfordAssets()
	WaitForRockfordAssets()
	InitAmbientHouses()
	
	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< -851.0237, 4.1346, 41.9042 >>) 
	SET_ENTITY_HEADING(PLAYER_PED_ID(),86.4924)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	eMissionStage = MS_GO_TO_ROCKFORD

ENDPROC

/// PURPOSE: skip from MS_FIND_THE_GREEN_COMET
PROC SkipStage2()
	
	IF NOT DOES_ENTITY_EXIST(vehPlayer)	
		CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< -904.9491, 17.5415, 45.5212 >>,109.7778,TRUE,TRUE,FALSE,TRUE,TRUE,FELON2,2)
	ENDIF
	
	SAFE_REMOVE_BLIP(blipRockfordHills)
	
	#IF IS_DEBUG_BUILD	
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		
			IF IS_VEHICLE_OK(vehPlayer)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer)
			ENDIF
		ENDIF
	#ENDIF
	
	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< -913.5941, 12.8344, 46.5934 >>) 
	SET_ENTITY_HEADING(PLAYER_PED_ID(),127.8599)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	eMissionStage = MS_FIND_THE_GREEN_COMET
	
ENDPROC

/// PURPOSE: skip from MS_CHASE_AVERY
PROC SkipStage3()
	
	IF IS_VEHICLE_OK(vehAveryComet)
		SAFE_TELEPORT_ENTITY(vehAveryComet,<< -1083.1884, 266.0258, 63.0025 >>, 111.2559)
		IF IS_PED_UNINJURED(pedAvery)
			IF IS_PED_SITTING_IN_VEHICLE(pedAvery,vehAveryComet)
				//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< -1081.5244, 254.3746, 62.9404 >>, 356.8038)
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< -1081.8066, 260.2781, 62.9998 >>, 7.9306)
				bDebugSkippedToBeatAvery = TRUE
				bCometSmashed = TRUE
				bWaypointPlaybackStopped = TRUE
				IF bDoorFallenOff = FALSE
					//SET_VEHICLE_DOOR_BROKEN(vehAveryComet,SC_DOOR_FRONT_LEFT,TRUE)
					bDoorFallenOff = TRUE
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				eMissionStage = MS_CHASE_AVERY
			ELSE
				Script_Passed()	
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: skip from MS_AVERY_KO
PROC SkipStage4()
	
	IF IS_PED_UNINJURED(pedAvery)	
		SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
		SET_PED_KEEP_TASK(pedAvery,TRUE)
		IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
			TASK_SMART_FLEE_PED(pedAvery,PLAYER_PED_ID(),1000.0,-1)
		ENDIF
		Script_Passed()	
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

ENDPROC

/// PURPOSE: Reset everything	
PROC ResetStuff()

	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	eCutsceneState = eCutInit

	iAveryTookMeleeHit = 0
	iConvoCounter = 0
	iRoadSwitch = 0
	iCometStuckCounter = 0
	iPleadConvo = 0
	bWaypointPlaybackStopped = FALSE
	bCometReverse = FALSE
	bStartChase = FALSE
	bFindTheGreenCometMessagePrinted = FALSE
	bDistanceWarningPrinted = FALSE
	bAveryFleeingInVehicle = FALSE
	iJoshConvoCounter = 0
	bPrintWrongColourComet = FALSE
	iBikerInCombat = 0	
	iAverySpotted = 0
	iBikerConvoCounter = 0
	iGunThreaten = 0
	iCounterReverseAverySeesPlayer = 0
	bAveryGettingUp = FALSE
	bJoshTasksCleared = FALSE	
	bRoadsOffAroundAvery = FALSE	
	bPrintBeatAvery = FALSE
	bCometSmashed = FALSE
	bJoshCleanedUp = FALSE
	bRockfordInit = FALSE
	bDoorFallenOff = FALSE
	bDebugSkippedToBeatAvery = FALSE	
	bConvoFelon = FALSE
	bHouseViewerYelling = FALSE	
	bSwitchAnimFlags = FALSE
	bOnFireHpSwitch = FALSE
	bJoggerCreated = FALSE
	bDogCreated = FALSE
	bLoveYa = FALSE
	bDontKillAveryHelp = FALSE
	bPrintGoToRockford = FALSE
	bStreamVolCreated = FALSE
	bCometGotStuck = FALSE
	iAveryOhFuckLines = 0
	bAveryFleeingOnFoot = FALSE
	iAveryFlee = 0
	iAveryGunThreat = 0
	bTrevThreat = FALSE
	bTrevThreatenedAvery = FALSE
	
	eJOGGER = JOGGER_LOAD
	
	SAFE_DELETE_VEHICLE(vehAveryComet)
	SAFE_DELETE_VEHICLE(vehBullet)
	SAFE_DELETE_VEHICLE(vehGauntlet)
	SAFE_DELETE_VEHICLE(vehElegy)
	SAFE_DELETE_VEHICLE(vehJoshFelon)
	SAFE_DELETE_VEHICLE(vehBati)
	SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[0])

	IF bDebugSkippingToStart = TRUE
		bDebugSkippingToStart = FALSE
	ENDIF
	
	SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,100,FALSE,TRUE)
	
	//SET_DISABLE_AMBIENT_MELEE_MOVE(PLAYER_ID(),FALSE)
	/*
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_a"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_b"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_rear"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_heavy"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_armed_takedown"), TRUE) 
	*/
	SAFE_DELETE_PED(pedAvery)
	SAFE_DELETE_PED(pedBiker)
	SAFE_DELETE_PED(pedJogger)
	SAFE_DELETE_PED(pedDog)
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
	SAFE_DELETE_PED(pedViewerHusband)

	SAFE_REMOVE_BLIP(blipRockfordHills)
	SAFE_REMOVE_BLIP(blipAvery)
	SAFE_REMOVE_BLIP(blipCSDoor)
	
	CLEAR_SEQUENCE_TASK(seqComet2)

ENDPROC

/// PURPOSE: Waits for the screen to fade out, then updates the fail reason for the mission	
PROC FailWait()

	SWITCH eFAIL_STATE
	
		CASE FS_SETUP
			CPRINTLN(DEBUG_MISSION, "JOSH2**** MISSION FAILED ****")
			RemoveBlips()
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			IF IS_STRING_NULL_OR_EMPTY(sFailReason)
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
	
			eFAIL_STATE = FS_UPDATE
		BREAK
		
		CASE FS_UPDATE
			IF IS_SCREEN_FADED_OUT()
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<568.756897,-1770.072144,27.893454>>, <<566.249146,-1768.923584,30.143578>>, 2.500000,<< 573.5463, -1759.4382, 28.1695 >>, 354.0835)
			ENDIF
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 598.2769, -1743.6196, 28.2526 >>, 71.1522)
  				//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 595.6382, -1739.4547, 28.1448 >>, 73.9324)
				
				//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<569.730225,-1774.378540,27.897423>>, <<560.649048,-1770.264526,31.606855>>, 14.750,<< 573.5463, -1759.4382, 28.1695 >>, 354.0835)
				
				ResetStuff()
				Script_Cleanup()
			ELSE
				IF iJoshScared = 1 	
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	    ////Oh shit! Please Tony, don't hurt me!
						PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_SCARE", "JOSH2_SCARE_1", CONV_PRIORITY_HIGH)
						iJoshScared = 2
					ENDIF
				ENDIF
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		DEBUG
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
	
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			CLEAR_PRINTS()
			eMissionStage = MS_MISSION_FAILING
		ENDIF
		
	ENDPROC
	
	PROC DEBUG_Check_Skips()
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			
			WAIT_FOR_CUTSCENE_TO_STOP()
			
			IF eMissionStage = MS_GO_TO_ROCKFORD	
				bDebugSkipping = TRUE
				bDebugSkippingToStart = TRUE	
				ResetStuff()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<567.5291, -1769.4231, 28.1428>>, 159.2917)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				//IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
					WHILE NOT CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[0], CHAR_JOSH, <<565.390076,-1772.837769,29.800871>> , 62.5015565)
						WAIT(0)
					ENDWHILE
				//ENDIF
				eMissionStage = MS_INTRO
			ENDIF
			
			IF eMissionStage = MS_FIND_THE_GREEN_COMET
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()						
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< 570.0823, -1763.5531, 28.1695 >>) 
				SET_ENTITY_HEADING(PLAYER_PED_ID(),347.4368)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				PRINT_NOW("JOSH2_01",DEFAULT_GOD_TEXT_TIME,0) 						//Go to ~y~Rockford Hills.
			ENDIF
				
			IF eMissionStage = MS_CHASE_AVERY
			AND bWaypointPlaybackStopped = FALSE		
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()						
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SkipStage1()
			ENDIF
				
			IF eMissionStage = MS_AVERY_KO
			OR bWaypointPlaybackStopped = TRUE	
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()							
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SkipStage1()
				SkipStage2()
			ENDIF
			
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			
			WAIT_FOR_CUTSCENE_TO_STOP()
			
			bDebugSkipping = TRUE
			
			IF eMissionStage = MS_GO_TO_ROCKFORD
				SkipStage1() 
			ENDIF
			
			IF eMissionStage = MS_FIND_THE_GREEN_COMET
				SkipStage2() 
			ENDIF
			
			IF eMissionStage = MS_CHASE_AVERY
				SkipStage3() 
			ENDIF
			
			IF eMissionStage = MS_AVERY_KO
				SkipStage4()
			ENDIF
				
		ENDIF
		
		SkipMenuStruct[0].sTxtLabel = "Mission start"  
		SkipMenuStruct[1].sTxtLabel = "Find the yellow comet"  
		SkipMenuStruct[2].sTxtLabel = "Chase Avery"  
		SkipMenuStruct[3].sTxtLabel = "Fight Avery"   

		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage)
		
			IF iReturnStage = 0
				bDebugSkipping = TRUE
				bDebugSkippingToStart = TRUE	
				ResetStuff()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<567.5291, -1769.4231, 28.1428>>, 159.2917)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				//IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
					WHILE NOT CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[0], CHAR_JOSH, <<565.390076,-1772.837769,29.800871>> , 62.5015565)
						WAIT(0)
					ENDWHILE
				//ENDIF
				eMissionStage = MS_INTRO
				
			ELIF iReturnStage = 1
				WAIT_FOR_CUTSCENE_TO_STOP()
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()							
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SkipStage1()
			
			ELIF iReturnStage = 2
				WAIT_FOR_CUTSCENE_TO_STOP()
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()						
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SkipStage1()
				SkipStage2()
				
			ELIF iReturnStage = 3
				WAIT_FOR_CUTSCENE_TO_STOP()
				bDebugSkipping = TRUE
				ResetStuff()
				LoadStuff()							
				Init()
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()
				SkipStage1()
				SkipStage2()
				SkipStage3()
				
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

FUNC BOOL IS_ANIM_PHASE_OK_FOR_LEADIN()

	//PRINTFLOAT(GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmpaparazzo_2", "pap_2_rcm_base"))
	//PRINTNL()

	IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		RETURN TRUE
	ELSE
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])		
			IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_wait_loop")
			OR GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_wait_loop") > 0.9
			OR GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_wait_loop") < 0.1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	//RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()		
	RETURN FALSE

ENDFUNC

/// PURPOSE: Play intro mocap cutscene
PROC IntroMocap()
	/*
	IF NOT IS_ANIM_PHASE_OK_FOR_LEADIN()
		EXIT
	ENDIF
	*/
	//INT iParam 
/*
	0 = generic menu screen
	1 = josh2 gallery
	2 = josh2 photo of Lenny Avery (currently temp image of white A_M_Y_business_02)
	3 = generic call screen
*/
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2199275
	
	IF eCutsceneState <	eCutLeadIn2
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<565.184204,-1777.227417,30.704159>>, <<549.950806,-1770.103027,35.947773>>, 7.000000)	//up the stairs
			IF NOT bStreamVolCreated
				StreamVol = STREAMVOL_CREATE_FRUSTUM(<<556.0839, -1724.9622, 33.0732>>, <<18.6933, 0.1321, -157.4946>>,200,FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
				bStreamVolCreated = TRUE
			ENDIF
		ELSE
			IF bStreamVolCreated
				STREAMVOL_DELETE(StreamVol)
				bStreamVolCreated = FALSE
			ENDIF
		ENDIF
	ENDIF
		
	SWITCH eCutsceneState
		
		CASE eCutInit
			
			bCSExitTrev = FALSE
			bCSExitJosh = FALSE
			bCSExitCam = FALSE
			bDoCSLocate = FALSE
			
			//RC_REQUEST_CUTSCENE("josh_2_intp1_t4")
			REQUEST_CUTSCENE("josh_2_intp1_t4")
			REQUEST_ANIM_DICT("rcmjosh")
			REQUEST_ANIM_DICT("rcmjosh2")
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
				IF HAS_ANIM_DICT_LOADED("rcmjosh2")	
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[0],FALSE)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, sRCLauncherDataLocal.pedID[0], "JOSH")
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 2, PLAYER_PED_ID(), "TREVOR")
					Josh2_Leadin_1 = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_INT_LI",CONV_PRIORITY_MEDIUM)	
						
						REPLAY_RECORD_BACK_FOR_TIME(1.0, 6.0)
						
						// B*2079566 - disable taxis during the "go to door" state to fix PT issue
						IF bDisabledTaxis = FALSE
							DISABLE_TAXI_HAILING(TRUE)
							bDisabledTaxis = TRUE
							CPRINTLN(DEBUG_MISSION, " IntroMocap : bDisabledTaxis = TRUE")
						ENDIF
						RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
						//BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[0],GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))
						IF IS_SCREEN_FADED_OUT()
							TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], Josh2_Leadin_1, "rcmjosh2", "josh_leadin", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<567.5291, -1769.4231, 28.1428>>, 159.2917)
						ELSE
							TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], Josh2_Leadin_1, "rcmjosh2", "josh_leadin", 1, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
						ENDIF	
						SET_MISSION_LAST_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0)
						eCutsceneState = eCutLeadIn1
						CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutInit ->  eCutLeadIn1")
					ENDIF
				ENDIF
			ENDIF
	
		BREAK	
	
		CASE eCutLeadIn1
			
			IF NOT bTurnedJosh
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_1)
					IF GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_1) >= 0.025	
						IF IS_SCREEN_FADED_OUT()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_1) >= 0.371
						bDoCSLocate = TRUE
						IF NOT DOES_BLIP_EXIST(blipCSDoor)
							blipCSDoor = CREATE_COORD_BLIP(<<550.20, -1772.53, 32.45>>)
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_1) < 0.371
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<565.184204,-1777.227417,30.704159>>, <<549.950806,-1770.103027,35.947773>>, 7.000000)	//up the stairs
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[0],TRUE)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
							TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID())
							bTurnedJosh = TRUE
							EXIT
						ENDIF
					ENDIF	
				ENDIF
				
				//UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_1b)	
				//IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0],"rcmjosh2", "josh_wait_loop")	
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_1)
					OR GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_1)	> 0.99
						//TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[0], "rcmjosh2", "josh_wait_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
						//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_1b)
							Josh2_Leadin_1b = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
							SET_SYNCHRONIZED_SCENE_LOOPED(Josh2_Leadin_1b,TRUE)
							TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], Josh2_Leadin_1b, "rcmjosh2", "josh_wait_loop_exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							//iTimerJoshMotelLines = GET_GAME_TIMER()
						//ENDIF
					ENDIF
				ELSE
					DO_JOSH_MOTEL_LINES()
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_1)
				AND GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_1) >= 0.371
					bDoCSLocate = TRUE
					IF NOT DOES_BLIP_EXIST(blipCSDoor)
						blipCSDoor = CREATE_COORD_BLIP(<<550.20, -1772.53, 32.45>>)
					ENDIF
				ENDIF
		
			ELSE
				
				IF IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
				ENDIF
				
				bDoCSLocate = TRUE
				IF NOT DOES_BLIP_EXIST(blipCSDoor)
					blipCSDoor = CREATE_COORD_BLIP(<<550.20, -1772.53, 32.45>>)
				ENDIF
				IF NOT IS_PED_FACING_PED(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID(),45)
					TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[0],PLAYER_PED_ID())
				ENDIF
				DO_JOSH_MOTEL_LINES()
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<550.20, -1772.53, 33.45>>,<<3,3,1>>,FALSE)
				IF HAS_ANIM_DICT_LOADED("rcmjosh")		
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					DISPLAY_HUD(FALSE)
					DISPLAY_RADAR(FALSE)
					bDoCSLocate = FALSE
					SAFE_REMOVE_BLIP(blipCSDoor)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[0],NORMAL_BLEND_DURATION,FALSE)
					Josh2_Leadin_2 = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
					camMiniCut = CREATE_CAMERA(CAMTYPE_ANIMATED)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), Josh2_Leadin_2, "rcmjosh", "trevor_enter_room_trevor", SLOW_BLEND_IN, SLOW_BLEND_OUT)
					SET_CAM_ACTIVE(camMiniCut,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					PLAY_SYNCHRONIZED_CAM_ANIM(camMiniCut,Josh2_Leadin_2,"trevor_enter_room_camera","rcmjosh")
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<549.33, -1773.09, 33.77>>,5,prop_motel_door_09,Josh2_Leadin_2,"trevor_enter_room_door","rcmjosh",NORMAL_BLEND_DURATION)
					RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, TRUE)
					DISABLE_CELLPHONE(TRUE)
					CLEAR_AREA_OF_PEDS(<<549.33, -1773.09, 33.77>>,50)
					eCutsceneState = eCutLeadIn2
					CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutLeadIn1 ->  eCutLeadIn2")
				ENDIF
			ENDIF
			
			IF bDoCSLocate = TRUE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<550.20, -1772.53, 32.45>>,<<4,4,LOCATE_SIZE_HEIGHT>>,TRUE)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eCutLeadIn2
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF NOT bStreamVolCreated
				StreamVol = STREAMVOL_CREATE_FRUSTUM(<<556.0839, -1724.9622, 33.0732>>, <<18.6933, 0.1321, -157.4946>>,150,FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)
				bStreamVolCreated = TRUE
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_2)
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
				IF GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_2) > 0.98
				OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() 		
					eCutsceneState = eCutLeadIn3
					CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutLeadIn2 ->  eCutLeadIn3")
				ENDIF
			ENDIF
		
		BREAK
	
		CASE eCutLeadIn3
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
				DESTROY_CAM(camMiniCut)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
				ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<556.0839, -1724.9622, 33.0732>>, <<18.6933, 0.1321, -157.4946>>, 7000)
				ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<556.0934, -1724.9857, 35.9731>>, <<12.1284, 0.1325, -157.7881>>, 7000)  //<<556.0882, -1724.9728, 34.3872>>, <<12.9316, 0.1321, -157.7965>>
				SET_CAM_FOV(sTimelapse.splineCamera, 37.1751)  //42.0503//27.1
				SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
				PLAY_SOUND_FRONTEND(-1, "TIME_LAPSE_MASTER")				
                SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE, FALSE)
				sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
				todReference = GET_CURRENT_TIMEOFDAY()
				ADD_TIME_TO_TIMEOFDAY(todReference, 0, 0, CONST_NUM_HOURS_SKIP_FOR_MOTEL_TIMELAPSE, 0)	// B*2173568 - ensure iSkipToHour is clamped to a valid hour
				iSkipToHour = GET_TIMEOFDAY_HOUR(todReference)
				CPRINTLN(DEBUG_MISSION, "JOSH2: GET_HOUR_TO_SKIP_TO_IN_MOTEL_TIMELAPSE return ", iSkipToHour)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				eCutsceneState = eCutLeadIn4
				CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutLeadIn3 ->  eCutLeadIn4")
			ENDIF
			
		BREAK
	
		CASE eCutLeadIn4
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_2)
				SET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_2,0.99)
			ENDIF

			IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iSkipToHour, 0, "EXTRASUNNY", "cirrocumulus", sTimelapse)
           	OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() 	
				RELEASE_AMBIENT_AUDIO_BANK()
				STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
				//SAFE_FADE_SCREEN_OUT_TO_BLACK(DEFAULT_FADE_TIME)
				eCutsceneState = eCutLeadIn5
				CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutLeadIn4 ->  eCutLeadIn5")
			ENDIF
			
		BREAK
	
		CASE eCutLeadIn5
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[0],"Josh",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				RC_CLEANUP_LAUNCHER()
				
				REPLAY_RECORD_BACK_FOR_TIME(15.0, 0.0, REPLAY_IMPORTANCE_LOWEST)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				START_CUTSCENE()
				WAIT(0)
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE) // this stomps on the timeOfDay if bResetTime parameter is TRUE
				//SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
				//SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE)
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<569.730225,-1774.378540,27.897423>>, <<560.649048,-1770.264526,31.606855>>, 14.750,<< 573.5463, -1759.4382, 28.1695 >>, 354.0835)
				DESTROY_CAM(camMiniCut)
				STREAMVOL_DELETE(StreamVol)
				bStreamVolCreated = FALSE
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				eCutsceneState = eCutUpdate
				CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutLeadIn5 ->  eCutUpdate CUTSCENE STARTED")
			ENDIF
			
		BREAK

		CASE eCutUpdate
			
			
			/*
			IF NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sJoshFakeCellphoneData)
				REQUEST_FAKE_CELLPHONE_MOVIE(sJoshFakeCellphoneData)
			ELSE
				IF GET_CUTSCENE_TIME() < 14428
					DRAW_FAKE_CELLPHONE_SCREEN(sJoshFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
				ELIF GET_CUTSCENE_TIME() > 15209
					DRAW_FAKE_CELLPHONE_SCREEN(sJoshFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_JOSH2_PHOTO)
				ELSE
					DRAW_FAKE_CELLPHONE_SCREEN(sJoshFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_JOSH2_GALLERY)
				ENDIF
			ENDIF
			*/
			
			
			/*
			IF IS_CUTSCENE_PLAYING()
				PRINTINT(GET_CUTSCENE_TIME())
				PRINTNL()
			ENDIF
			*/
			/*
			IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_NPC_Phone")
				rt_ID = GET_NAMED_RENDERTARGET_RENDER_ID("Prop_NPC_Phone")
			ENDIF
			
			IF GET_CUTSCENE_TIME() < 70960
				iParam = 0
			ELIF GET_CUTSCENE_TIME() > 71418
				iParam = 2
			ELSE
				iParam = 1
			ENDIF
			
			LINK_NAMED_RENDERTARGET(Prop_Phone_ING)
			
			BEGIN_SCALEFORM_MOVIE_METHOD(siJoshPhone,"DISPLAY_VIEW")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iParam)		
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_TEXT_RENDER_ID (rt_ID)
			DRAW_SCALEFORM_MOVIE(siJoshPhone, 0.100, 0.24000, 0.2100,0.51000, 100,100,100,255)
			*/
			IF DOES_CAM_EXIST(camMiniCut)
				DESTROY_CAM(camMiniCut)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
			ENDIF
			
			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Josh")	
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
					Josh2_Leadin_3 = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
					//IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_INT_LO",CONV_PRIORITY_MEDIUM)		
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[0])
						TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], Josh2_Leadin_3, "rcmjosh2", "josh_leadout", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						bCSExitJosh = TRUE
					//ENDIF
				ENDIF
			ENDIF
			/*
			IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_NPC_Phone")
				RELEASE_NAMED_RENDERTARGET("Prop_NPC_Phone")
			ENDIF
			IF IS_NAMED_RENDERTARGET_REGISTERED("CELLPHONE_CUTSCENE")
				RELEASE_NAMED_RENDERTARGET("CELLPHONE_CUTSCENE")
			ENDIF
			*/
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")	
				REPLAY_STOP_EVENT()
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
				//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,6,0>>),PEDMOVEBLENDRATIO_WALK,-1)
				//TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(PLAYER_PED_ID(),PLAYER_PED_ID(),<<0,5,0>>,PEDMOVEBLENDRATIO_WALK,-1)
				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,6,0>>),PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING)
				bCSExitTrev = TRUE
			ENDIF
			
			IF bCSExitTrev = FALSE
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			    SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			    SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				bCSExitCam = TRUE
			ENDIF
			
			IF bCSExitTrev = TRUE
			AND bCSExitJosh = TRUE
			AND bCSExitCam = TRUE
				eCutsceneState = eCutLeadOut1
			ENDIF
			
		BREAK
		
		CASE eCutLeadOut1
			
			//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				eCutsceneState = eCutCleanup
			//ENDIF
			
		BREAK
		
		CASE eCutCleanup
			
			//CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
			//RC_END_CUTSCENE_MODE()
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			//RELEASE_FAKE_CELLPHONE_MOVIE(sJoshFakeCellphoneData)
			//RELEASE_NPC_PHONE_RENDERTARGET()
			
			/*
			IF IS_NAMED_RENDERTARGET_REGISTERED("Prop_NPC_Phone")
				RELEASE_NAMED_RENDERTARGET("Prop_NPC_Phone")
			ENDIF
			IF IS_NAMED_RENDERTARGET_REGISTERED("CELLPHONE_CUTSCENE")
				RELEASE_NAMED_RENDERTARGET("CELLPHONE_CUTSCENE")
			ENDIF
			*/
			
			//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
			//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
			
			// B*2079566 - RESET disable taxis during the "go to door" state to fix PT issue
			IF bDisabledTaxis = TRUE
				DISABLE_TAXI_HAILING(FALSE)
				bDisabledTaxis = FALSE
				CPRINTLN(DEBUG_MISSION, " IntroMocap : eCutCleanup : bDisabledTaxis = FALSE")
			ENDIF
						
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[0],FALSE)
			DISABLE_CELLPHONE(FALSE)
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			SAFE_REMOVE_BLIP(blipCSDoor)
			eMissionStage = MS_INIT
			
		BREAK
	
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
			STOP_CUTSCENE()
			IF DOES_CAM_EXIST(camMiniCut)
				DESTROY_CAM(camMiniCut)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
			ENDIF
			eCutsceneState = eCutCleanup
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)		
			STOP_CUTSCENE()
			IF DOES_CAM_EXIST(camMiniCut)
				DESTROY_CAM(camMiniCut)
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
			ENDIF
			bDebugSkipping = TRUE
			bDebugSkippingToStart = TRUE	
			ResetStuff()
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<567.5291, -1769.4231, 28.1428>>, 159.2917)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			WHILE NOT CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[0], CHAR_JOSH, <<565.390076,-1772.837769,29.800871>> , 62.5015565)
				WAIT(0)
			ENDWHILE
			eMissionStage = MS_INTRO
		ENDIF
		DEBUG_Check_Debug_Keys()
	#ENDIF
	
ENDPROC

/// PURPOSE: Skip back to the start
PROC SkipToStart()
	
	STOP_CUTSCENE()
	IF DOES_CAM_EXIST(camMiniCut)
		DESTROY_CAM(camMiniCut)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
	ENDIF
	START_REPLAY_SETUP(<<567.5291, -1769.4231, 28.1428>>, 159.2917)
	
	LoadStuff()
	
	bDebugSkipping = TRUE
	bDebugSkippingToStart = TRUE	
	ResetStuff()
	//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<567.5291, -1769.4231, 28.1428>>, 159.2917)
	WHILE NOT CREATE_NPC_PED_ON_FOOT(sRCLauncherDataLocal.pedID[0], CHAR_JOSH, <<565.390076,-1772.837769,29.800871>> , 62.5015565)
		WAIT(0)
	ENDWHILE
	
	IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[0])
		sRCLauncherDataLocal.vehID[0] = CREATE_VEHICLE(FELON2,<<558.09, -1765.39, 28.86>>, 335.3252)
		SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[0],4)
	ELSE
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0],<<558.09, -1765.39, 28.86>>)
			SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0],335.3252)
		ENDIF
	ENDIF
	
	eMissionStage = MS_INTRO
	
	END_REPLAY_SETUP()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	RC_END_Z_SKIP(TRUE) 
	
ENDPROC

/// PURPOSE: Skip past intro cutscene, used for mission replay
PROC SkipPastCS()
	
	START_REPLAY_SETUP(<< 570.0771, -1763.5652, 28.1695 >>, 347.4368)
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	LoadStuff()
	Init()
	eMissionStage = MS_GO_TO_ROCKFORD

	//IF NOT DOES_ENTITY_EXIST(vehPlayer)
	//	CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 573.5463, -1759.4382, 28.1695 >>,354.0835,FALSE,FALSE,FALSE,TRUE,FALSE,FELON2,2)
	//ENDIF
	
	//SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 570.0771, -1763.5652, 28.1695 >>)
	//SET_ENTITY_HEADING(PLAYER_PED_ID(),347.4368)
	
	//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	END_REPLAY_SETUP()
	
	WAIT(100)
	
	PRINT_NOW("JOSH2_01",DEFAULT_GOD_TEXT_TIME,0) 						//Go to ~y~Rockford Hills.
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Go To Rockford")

	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	RC_END_Z_SKIP(TRUE)
ENDPROC

/// PURPOSE: Skip to the chase, used for mission replay
PROC SkipToChase()

	START_REPLAY_SETUP(<< -923.8829, 5.9621, 46.5911 >>, 0.0)

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> ENTERING REPLAY SKIP TO CHASE <><>")
	#ENDIF
/*
	IF STREAMVOL_IS_VALID(streamVolID)
		streamVolID = STREAMVOL_CREATE_SPHERE(<< -904.9491, 17.5415, 45.5212 >>, 30.0, FLAG_MAPDATA)        
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> CREATING STREAM VOL <><>")
	#ENDIF
*/
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

	ResetStuff()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED RESET STUFF <><>")
	#ENDIF
	LoadStuff()		
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED LOAD STUFF <><>")
	#ENDIF
	Init()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED INIT <><>")
	#ENDIF
	RequestRockfordAssets()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED REQ ROCKFORD ASSETS <><>")
	#ENDIF
	WaitForRockfordAssets()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED WAIT FOR ROCKFORD ASSETS <><>")
	#ENDIF
	InitAmbientHouses()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED INIT AMBIENT HOUSES <><>")
	#ENDIF
	SkipStage1()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED SKIP STAGE 1 <><>")
	#ENDIF
	
	SAFE_REMOVE_BLIP(blipRockfordHills)
	
	//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(),<< -913.5941, 12.8344, 46.5934 >>) 
	//SET_ENTITY_HEADING(PLAYER_PED_ID(),127.8599)

	eMissionStage = MS_FIND_THE_GREEN_COMET

	SET_ROADS_IN_ANGLED_AREA(<<-887.370972,-101.877480,34.387718>>, <<-989.135986,60.986832,58.366745>>, 31.500000,FALSE,FALSE)

	//SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -899.2744, 20.7509, 45.0002 >>)
	/*
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-924.975342,-9.033067,43.281506>>,10.0,prop_lrggate_03a)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-924.975342,-9.033067,43.281506>>,TRUE,1.0)                             
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-905.696655,14.659701,45.675934>>,10.0,prop_lrggate_03a)               
  		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-905.696655,14.659701,45.675934>>,TRUE,1.0)                             
	ENDIF
	*/
	//WAIT(500)
	/*
	IF STREAMVOL_IS_VALID(streamVolID)
		WHILE NOT STREAMVOL_HAS_LOADED(streamVolID)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "<><> WAITING FOR STREAMVOL <><>")
			#ENDIF
			WAIT(0)
		ENDWHILE
	ENDIF
	*/
	
	//STREAMVOL_DELETE(streamVolID)
	
	CLEAR_AREA(<< -923.8829, 5.9621, 46.5911 >>,50.0,TRUE)
	
	IF NOT DOES_ENTITY_EXIST(vehPlayer)
		CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<-897.5433, 20.4735, 44.9163>>,110.3058,TRUE,TRUE,FALSE,TRUE,TRUE,FELON2,2)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY STARTING LOAD SCENE <><>")
	#ENDIF
	//LOAD_SCENE(<< -899.2744, 20.7509, 45.0002 >>)
	//WAIT_FOR_WORLD_TO_LOAD(<< -899.2744, 20.7509, 45.0002 >>,50, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> REPLAY FINISHED LOAD SCENE <><>")
	#ENDIF
	
	IF IS_VEHICLE_OK(vehPlayer)
		SET_ENTITY_INVINCIBLE(vehPlayer,TRUE)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlayer,FALSE)
		SAFE_TELEPORT_ENTITY(vehPlayer,<<-903.2674, 18.7336, 45.2240>>, 113.1507)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
		iRollingStartTimer = GET_GAME_TIMER()
	ENDIF
	
	END_REPLAY_SETUP(vehPlayer)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)

	FLOAT fDoorOpenRatio
	BOOL bDoorLock
	
	WHILE fDoorOpenRatio < 1.0
	AND GET_GAME_TIMER() < iRollingStartTimer + 2000
		WAIT(0)
		GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_03a,<<-905.696655,14.659701,45.675934>>,bDoorLock,fDoorOpenRatio)
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(Josh2Door[3])	
			CPRINTLN(DEBUG_MISSION, "<><> GATE REGISTERED WITH SYSTEM <><>")
			DOOR_SYSTEM_SET_OPEN_RATIO(Josh2Door[3], 1.0, false, false) 
			DOOR_SYSTEM_SET_DOOR_STATE(Josh2Door[3], DOORSTATE_FORCE_OPEN_THIS_FRAME)
		ENDIF
		#IF IS_DEBUG_BUILD
			PRINTFLOAT(fDoorOpenRatio)
			PRINTNL()
		#ENDIF
		OpenGates()	
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		IF GET_GAME_TIMER() >= iRollingStartTimer + 2000 
			CPRINTLN(DEBUG_MISSION, "<><> REPLAY COULDN'T OPEN GATE - TIMING OUT <><>")
		ELSE
			CPRINTLN(DEBUG_MISSION, "<><> REPLAY OPENED GATE <><>")
		ENDIF
	#ENDIF 	
	
	IF IS_VEHICLE_OK(vehPlayer)
		SET_VEHICLE_FORWARD_SPEED(vehPlayer,2)
		TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(),vehPlayer,"Josh2Roll",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
		iRollingStartTimer = GET_GAME_TIMER()
	ENDIF
	
	WHILE GET_GAME_TIMER() < iRollingStartTimer + 1200
		WAIT(0)
		OpenGates()	
	ENDWHILE
	
	iRollingStartTimer = GET_GAME_TIMER()
	
	//WAIT(300)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	RC_END_Z_SKIP(TRUE) 

ENDPROC

PROC SkipToEnd()

	START_REPLAY_SETUP(<<-1066.0558, 387.0880, 67.8674>>, 284.5628)

	ResetStuff()
	LoadStuff()							
	Init()
	RequestRockfordAssets()
	WaitForRockfordAssets()
	InitAmbientHouses()
	SkipStage1()
	SkipStage2()
	

	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
	
	IF IS_VEHICLE_OK(vehAveryComet)
		SAFE_TELEPORT_ENTITY(vehAveryComet,<<-1059.8920, 387.9023, 67.9261>>, 307.9631)
		IF IS_PED_UNINJURED(pedAvery)
			
			//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-1066.0558, 387.0880, 67.8674>>, 284.5628,TRUE)
			//SAFE_TELEPORT_ENTITY(pedAvery,<<-1060.7996, 389.5708, 67.8701>>, 105.1316,TRUE)
			bDebugSkippedToBeatAvery = TRUE
			bCometSmashed = TRUE
			bWaypointPlaybackStopped = TRUE
			IF bDoorFallenOff = FALSE
				//SET_VEHICLE_DOOR_BROKEN(vehAveryComet,SC_DOOR_FRONT_LEFT,TRUE)
				bDoorFallenOff = TRUE
			ENDIF
			
		ENDIF
	ENDIF

	//SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1066.0558, 387.0880, 67.8674>>)
	//SET_ENTITY_HEADING(PLAYER_PED_ID(),284.5628)
	
	SET_ENTITY_COORDS(pedAvery,<<-1060.7996, 389.5708, 67.8701>>)
	SET_ENTITY_HEADING(pedAvery,105.1316)

	SET_VEHICLE_ON_GROUND_PROPERLY(vehAveryComet)

	SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
	SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
	
	SET_VEHICLE_ENGINE_HEALTH(vehAveryComet,101)  //299
	SET_VEHICLE_ENGINE_ON(vehAveryComet,FALSE,TRUE)
	
	//LOAD_SCENE(<<-1050.1335, 387.4164, 68.2507>>)
	
	END_REPLAY_SETUP()
	
	WAIT(1000)
	
	IF IS_PED_UNINJURED(pedAvery)	
		SET_PED_KEEP_TASK(pedAvery,TRUE)
		IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
			TASK_WANDER_STANDARD(pedAvery)
		ENDIF
	ENDIF	
		
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()

	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)	

	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	
	WHILE NOT IS_SCREEN_FADED_IN()
		WAIT(0)
	ENDWHILE		

	Script_Passed()	

ENDPROC

/// PURPOSE: Manage the rolling start at chase
PROC ManageRollingStart()
	
	IF IS_VEHICLE_OK(vehPlayer)	
		//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)		
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
			OR GET_GAME_TIMER() > iRollingStartTimer + 2500
				//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)		
					VEHICLE_WAYPOINT_PLAYBACK_PAUSE(vehPlayer)
				ENDIF	
					//STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
					SET_ENTITY_INVINCIBLE(vehPlayer,FALSE)
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehPlayer,TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				//ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Mission setup
PROC Setup()

	//INFORM_MISSION_STATS_OF_MISSION_START_JOSH_2()

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "<><> JOSH 2 STARTED <><>")
	#ENDIF

	IF IS_REPLAY_IN_PROGRESS() = TRUE
	AND bDebugSkipping = FALSE	
		
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF	
		
		IF iReplayStage = 0
			SkipToStart() //SkipPastCS()
		ELIF iReplayStage = 1
			SkipPastCS()
		ELIF iReplayStage = 2
			SkipToChase()
		ELIF iReplayStage = 3
			SkipToEnd()
		ELSE
			SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
		ENDIF
	
	ELSE
		LoadStuff()
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "JOSH2**** Init intro ****")
		#ENDIF
		eMissionStage = MS_INTRO
	ENDIF

ENDPROC

/// PURPOSE: Initialise mission
PROC InitMission()

	Init()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "JOSH2**** Init go to Rockford Hills ****")
	#ENDIF
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Go To Rockford")
	eMissionStage = MS_GO_TO_ROCKFORD

ENDPROC

/// PURPOSE: Waits for the player to reach rockford hills
PROC GoToRockford()
	
	IF NOT bPrintGoToRockford	
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
		PRINT_NOW("JOSH2_01",DEFAULT_GOD_TEXT_TIME,0) 	//Go to ~y~Rockford Hills.
		bPrintGoToRockford = TRUE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_LR)	
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UD)	
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ENTER)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_SPRINT)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_JUMP)
		OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_LR))
		OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_UD))	
		OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UP_ONLY))		
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_3)			
		IF GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_3)	> 0//0.15
			IF bLoveYa = FALSE
				IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_INT_LO",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)	//I love ya
					bLoveYa = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
		SET_PED_RESET_FLAG(sRCLauncherDataLocal.pedID[0],PRF_ExpandPedCapsuleFromSkeleton,TRUE)
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_4)	
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(Josh2_Leadin_3)
			OR GET_SYNCHRONIZED_SCENE_PHASE(Josh2_Leadin_3)	> 0.99
				Josh2_Leadin_4 = CREATE_SYNCHRONIZED_SCENE(<<563.76, -1773.86, 28.36>>,<<0.02, 0.02, -110.70>>)
				SET_SYNCHRONIZED_SCENE_LOOPED(Josh2_Leadin_4,TRUE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], Josh2_Leadin_4, "rcmjosh2", "josh_sitting_loop", SLOW_BLEND_IN, -1, SYNCED_SCENE_USE_PHYSICS)
			ENDIF
		ENDIF
	ENDIF
	
	IF bJoshCleanedUp = FALSE
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,2000.0)
			SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
			SET_MODEL_AS_NO_LONGER_NEEDED(ig_josh)
			REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 5)
			ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, NULL, "JOSH")
			bJoshCleanedUp = TRUE
		ENDIF
	ELSE
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,300.0) //1000.0	
			IF bRockfordInit = FALSE	
				RequestRockfordAssets()
				WaitForRockfordAssets()
				InitAmbientHouses()	
			ENDIF
			DO_JOGGER_PED()
		ENDIF
	ENDIF
	
	IF bStartChase = TRUE	
		InitChase()
	ENDIF
	IF bRockfordInit	// For B*2201679, make sure everything is spawned.
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-853.674500,-52.276905,31.257320>>, <<-918.126526,47.042919,121.331253>>, 75.000000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-879.910889,56.844849,38.783661>>, <<-958.235535,59.315208,124.611870>>, 25.000000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-859.330750,40.053410,29.865433>>, <<-937.509583,8.682496,123.805862>>, 68.250000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-841.455811,-15.820283,54.401230>>, <<-859.777344,16.357359,39.161922>>, 24.500000)
			SAFE_REMOVE_BLIP(blipRockfordHills)
			IF bFindTheGreenCometMessagePrinted = FALSE
				PRINT_NOW("JOSH2_07",DEFAULT_GOD_TEXT_TIME,0) 					//Find the green comet.
				bFindTheGreenCometMessagePrinted = TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "JOSH2**** Init find the green comet ****")
			#ENDIF
			eMissionStage = MS_FIND_THE_GREEN_COMET
		ENDIF
		
		//If the player drives round the other side - skip
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-928.048584,-38.298660,33.437916>>, <<-959.601013,5.279084,62.931038>>, 36.250000)
			bFindTheGreenCometMessagePrinted = TRUE
			bCometReverse = TRUE
			IF CAN_HELI_PILOT_SEE_PLAYER(pedAvery)
				++iCounterReverseAverySeesPlayer
				IF iCounterReverseAverySeesPlayer > 12
					bStartChase = TRUE	
					InitChase()
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	/*
	IF DOES_BLIP_EXIST(blipRockfordHills)	 //Displaying chevron
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -903.6451, 25.6888, 45.4577 >>,<<LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_ANY_MEANS,LOCATE_SIZE_HEIGHT>>,TRUE)
		ENDIF
	ENDIF
	*/			
ENDPROC			

/// PURPOSE: Waits for the player to spot the yellow comet
PROC FindTheGreenComet()

	DO_JOGGER_PED()
/*
	WHILE(TRUE)	
		OpenGates()	
		DEBUG_Check_Debug_Keys()
		DEBUG_Check_Skips()
		WAIT(0)
	ENDWHILE
*/
	IF NOT DOES_BLIP_EXIST(blipAvery)	
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-853.674500,-52.276905,31.257320>>, <<-918.126526,47.042919,121.331253>>, 75.000000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-879.910889,56.844849,38.783661>>, <<-958.235535,59.315208,124.611870>>, 25.000000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-859.330750,40.053410,29.865433>>, <<-937.509583,8.682496,123.805862>>, 68.250000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-841.455811,-15.820283,54.401230>>, <<-859.777344,16.357359,39.161922>>, 24.500000)
			IF NOT DOES_BLIP_EXIST(blipRockfordHills)
				blipRockfordHills = CREATE_COORD_BLIP(<< -903.6451, 25.6888, 45.4577 >>)
				//SET_BLIP_ALPHA(blipRockfordHills,100)
				//SET_BLIP_SCALE(blipRockfordHills,5)
				SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(blipRockfordHills,<<-821.0944, -2.6563, 39.9561>>, 31.8236)
			ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "JOSH2**** Init go to Rockford Hills ****")
			#ENDIF
			eMissionStage = MS_GO_TO_ROCKFORD
		ENDIF
	ENDIF

	IF IS_VEHICLE_OK(vehAveryComet)	

		//Road
		IF IS_PED_UNINJURED(pedViewerHusband)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-895.741699,19.698891,67.248306>>, <<-905.281555,26.627926,41.107346>>, 9.000000)
			AND IS_ENTITY_ON_SCREEN(vehAveryComet)
			AND NOT IS_ENTITY_OCCLUDED(vehAveryComet)
				IF GET_FOCUS_PED_ON_SCREEN(50,BONETAG_HEAD,0.8,0.8,0.5) = pedAvery
				OR GET_FOCUS_PED_ON_SCREEN(50,BONETAG_HEAD,0.8,0.8,0.5) = pedViewerHusband	
					IF NOT IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
						IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
							TASK_ENTER_VEHICLE(pedAvery,vehAveryComet)
						ENDIF
					ENDIF
					++iAverySpotted
					IF iAverySpotted > 30
					AND	IS_PED_IN_VEHICLE(pedAvery,vehAveryComet,TRUE)	
						bStartChase = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-950.742249,23.491081,33.747551>>, <<-925.265259,-17.061129,87.121948>>, 16.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-931.361755,-25.908237,34.825775>>, <<-946.292297,-5.137248,55.447624>>, 28.500000)		
				bCometReverse = TRUE
			ENDIF
			IF NOT IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
				IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
					TASK_ENTER_VEHICLE(pedAvery,vehAveryComet)
				ENDIF
			ENDIF
			IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet,TRUE)
				bStartChase = TRUE
			ENDIF
		ENDIF
		
		//Normal driveway
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-914.341797,14.098543,51.176998>>, <<-907.522766,23.196795,41.124313>>, 25.000000)
		
		//Reverse driveway
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-928.825195,-8.227508,56.120975>>, <<-922.306458,-3.359912,39.814888>>, 11.250000)
		
		//Reverse road
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-928.048584,-38.298660,33.437916>>, <<-959.601013,5.279084,62.931038>>, 36.250000)
		
		//Failsafe
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-912.877197,1.544632,67.438065>>, <<-927.428772,20.621159,42.385647>>, 20.500000)
		
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehAveryComet,PLAYER_PED_ID())	
		
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAvery,PLAYER_PED_ID())			

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-950.742249,23.491081,33.747551>>, <<-925.265259,-17.061129,87.121948>>, 16.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-931.361755,-25.908237,34.825775>>, <<-946.292297,-5.137248,55.447624>>, 28.500000)	
				bCometReverse = TRUE
				bStartChase = TRUE
			ELSE
				IF NOT IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
					IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
						TASK_ENTER_VEHICLE(pedAvery,vehAveryComet)
					ENDIF
				ENDIF
				IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet,TRUE)
					bStartChase = TRUE
				ENDIF
			ENDIF
		ENDIF	
		
		IF bStartChase = TRUE	
			InitChase()
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "JOSH2**** Init chase ****")
		#ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOW)
		
		eMissionStage = MS_CHASE_AVERY
	ENDIF

ENDPROC

FUNC BOOL HAVE_TWO_TYRES_POPPED()

	INT iTyresPopped = 0
	IF IS_ENTITY_ALIVE(vehAveryComet)
		IF IS_VEHICLE_TYRE_BURST(vehAveryComet,SC_WHEEL_CAR_FRONT_LEFT)	
			++iTyresPopped
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehAveryComet,SC_WHEEL_CAR_FRONT_RIGHT)	
			++iTyresPopped
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehAveryComet,SC_WHEEL_CAR_REAR_LEFT)	
			++iTyresPopped
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(vehAveryComet,SC_WHEEL_CAR_REAR_RIGHT)	
			++iTyresPopped
		ENDIF
	ENDIF
	IF iTyresPopped > 1
		RETURN TRUE
	ENDIF

RETURN FALSE
ENDFUNC

/// PURPOSE: Handle the chase
PROC ChaseAvery()

	START_AUDIO_SCENE_CHASE()  
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
	
	IF bWaypointPlaybackStopped = FALSE
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)	//0.6
	ENDIF

	IF bJoshCleanedUp = FALSE
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),vCometPos,2000.0)
			SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
			SET_MODEL_AS_NO_LONGER_NEEDED(ig_josh)
			bJoshCleanedUp = TRUE
		ENDIF
	ENDIF
	
	IF bRoadsOffAroundAvery = TRUE
		STOP_AMBIENT_CARS_AROUND_PED(pedAvery)
	ENDIF
	/*
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedAvery,FALSE)
	ENDIF
	*/

	IF DOES_ENTITY_EXIST(pedJogger)
		SAFE_RELEASE_PED(pedJogger)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_FITNESS_02)
	ENDIF
	IF DOES_ENTITY_EXIST(pedDog)	
		SAFE_RELEASE_PED(pedDog)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_C_RETRIEVER)
	ENDIF
	IF DOES_ENTITY_EXIST(pedBiker)	
		SAFE_RELEASE_PED(pedBiker)
		SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BevHills_01)
	ENDIF
	
	SAFE_RELEASE_VEHICLE(vehSuperd)
	SAFE_RELEASE_VEHICLE(vehElegy)
	SAFE_RELEASE_VEHICLE(vehGauntlet)
	/*
	IF STREAMVOL_IS_VALID(streamVolID)
		IF STREAMVOL_HAS_LOADED(streamVolID)
			STREAMVOL_DELETE(streamVolID)
		ENDIF
	ENDIF
	*/
	IF IS_REPLAY_IN_PROGRESS()
		ManageRollingStart()
	ENDIF
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(vehAveryComet)
			IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)
				CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vehAveryComet)
			ELSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ELSE
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	ENDIF
	
	IF NOT bDontKillAveryHelp	
		//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JOSH2_10")
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				//PRINT_HELP("JOSH2_10") //The mission will fail if Avery dies.
				bDontKillAveryHelp = TRUE
			ENDIF
		//ENDIF
	ENDIF
	
	IF IS_VEHICLE_OK(vehAveryComet)	
		IF NOT IS_PED_SITTING_IN_VEHICLE(pedAvery,vehAveryComet)
		OR IS_PED_BEING_JACKED(pedAvery)	
			bWaypointPlaybackStopped = TRUE
		ENDIF
	ENDIF

	IF IS_PED_UNINJURED(pedAvery)	
		IF bOnFireHpSwitch = FALSE
		AND IS_ENTITY_ON_FIRE(pedAvery)	 
			IF GET_ENTITY_HEALTH(pedAvery) > 160
				SET_ENTITY_HEALTH(pedAvery,160)
				bOnFireHpSwitch = TRUE
			ENDIF
		ENDIF
		IF NOT DOES_BLIP_EXIST(blipAvery)
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),pedAvery) < 100.0	
				IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)		
					//IF IS_PED_UNINJURED(pedViewerHusband)
						//ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, pedViewerHusband, "Josh2Viewer")
						//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_VIEW", "JOSH2_VIEW_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES) //Hey! Where are you going?	
						//CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_VIEW",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
					//ENDIF		
					TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedAvery,-1)
					SET_PED_SUFFERS_CRITICAL_HITS(pedAvery,FALSE)
					iTimerChaseStarted = GET_GAME_TIMER()
					SAFE_REMOVE_BLIP(blipRockfordHills)
					//PRINT_HELP("JOSH2_10")                              //The mission will fail if Avery dies.
					PRINT_NOW("JOSH2_04",DEFAULT_GOD_TEXT_TIME,0) 						//Ram ~r~Avery's car~s~ off the road.
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Chase Avery",TRUE)
					blipAvery = CREATE_PED_BLIP(pedAvery)
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JO2_STOPPED_IN_TIME)
					//IF IS_PED_IN_ANY_VEHICLE(pedAvery)
						SET_BLIP_SCALE(blipAvery,BLIP_SIZE_VEHICLE)
					//ELSE
					//	SET_BLIP_SCALE(blipAvery,BLIP_SIZE_PED)
					//ENDIF
					SET_WANTED_LEVEL_MULTIPLIER(0.2)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedAvery,"JOSH2_BCAA","AVERY","SPEECH_PARAMS_STANDARD")
				ENDIF
			ENDIF
		ELSE
			UPDATE_CHASE_BLIP(blipAvery,pedAvery,280,0.7)
		ENDIF
	ENDIF

	IF iRoadSwitch = 0			
		IF IS_PED_UNINJURED(pedViewerHusband)
			IF GET_GAME_TIMER() > iTimerChaseStarted + 300	 	
				IF bDebugSkippedToBeatAvery = FALSE
						
					TASK_PERFORM_SEQUENCE(pedViewerHusband,seqAngryHusband)
					/*
					IF bCometReverse = FALSE	
						TASK_ACHIEVE_HEADING(pedViewerHusband,167.031372)
					ELSE
						TASK_ACHIEVE_HEADING(pedViewerHusband,255.0)
					ENDIF
					*/
				ENDIF
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, pedViewerHusband, "Josh2Viewer")	
				iTimerRoadsOff = GET_GAME_TIMER()	
				iRoadSwitch = 1
			ENDIF
		ENDIF
	ELIF iRoadSwitch = 1
		IF IS_VEHICLE_OK(vehAveryComet)	
			//IF GET_VEHICLE_WAYPOINT_PROGRESS(vehAveryComet) > 94
			IF IS_PED_UNINJURED(pedViewerHusband)	
				IF IS_ENTITY_PLAYING_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling")	
					IF bHouseViewerYelling = FALSE	
						IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_VIEW",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							bHouseViewerYelling = TRUE
						ENDIF
					ENDIF
				ELSE
					IF bCometReverse
						IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(pedViewerHusband),255,40)	
							CLEAR_PED_TASKS(pedViewerHusband)
							TASK_PLAY_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling",REALLY_SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)
						ENDIF
					ENDIF
				ENDIF
				IF bSwitchAnimFlags	= FALSE
					/*
					FLOAT fPhase
					IF bCometReverse = FALSE	
						//IF GET_ENTITY_HEADING(pedViewerHusband) > 165
						IF GET_ENTITY_HEADING(pedViewerHusband) < 160
						//IF GET_GAME_TIMER() > iTimerChaseStarted + 2200	
							IF IS_ENTITY_PLAYING_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling")	
								fPhase = GET_ENTITY_ANIM_CURRENT_TIME(pedViewerHusband,"rcmjosh2","house_viewer_yelling")
								TASK_PLAY_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling")
								SET_ANIM_PHASE(pedViewerHusband,fPhase)
								bSwitchAnimFlags = TRUE
							ENDIF
						ENDIF
					ELSE
						IF GET_ENTITY_HEADING(pedViewerHusband) > 253
						//AND GET_ENTITY_HEADING(pedViewerHusband) < 257
						//IF GET_GAME_TIMER() > iTimerChaseStarted + 2200	
							IF IS_ENTITY_PLAYING_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling")	
								fPhase = GET_ENTITY_ANIM_CURRENT_TIME(pedViewerHusband,"rcmjosh2","house_viewer_yelling")
								TASK_PLAY_ANIM(pedViewerHusband,"rcmjosh2","house_viewer_yelling")
								SET_ANIM_PHASE(pedViewerHusband,fPhase)
								bSwitchAnimFlags = TRUE
							ENDIF
						ENDIF			
					ENDIF
					*/
				ENDIF
			ENDIF
			IF GET_GAME_TIMER() > iTimerChaseStarted + 8000
				SAFE_RELEASE_PED(pedViewerHusband)
			ENDIF
			IF GET_GAME_TIMER()	> iTimerRoadsOff + 10000
				iRoadSwitch = 2
			ENDIF
		ENDIF
	ENDIF
		
	IF DOES_BLIP_EXIST(blipAvery)	
		IF IS_PED_IN_ANY_VEHICLE(pedAvery)
			SET_BLIP_SCALE(blipAvery,BLIP_SIZE_VEHICLE)
		ELSE
			SET_BLIP_SCALE(blipAvery,BLIP_SIZE_PED)
		ENDIF
	ENDIF
	
	IF bAveryFleeingOnFoot
		IF GET_GAME_TIMER() > iTimerAveryFleeOnFoot + 5000
			SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
			SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
			SET_PED_KEEP_TASK(pedAvery, TRUE)
			Script_Passed()	
		ENDIF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()												
			IF iAveryOhFuckLines < 3
				IF iGunThreaten <> 5
					IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_SHGUN", CONV_PRIORITY_MEDIUM)
						++iAveryOhFuckLines
					ENDIF
				ELSE
					//IF GET_GAME_TIMER() > iTimerAveryFleeOnFoot + 1000
						IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_SHGUN", CONV_PRIORITY_MEDIUM)
							++iAveryOhFuckLines
						ENDIF
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	//IF IS_VEHICLE_OK(vehAveryComet)
		IF bDebugSkippedToBeatAvery = FALSE	
			IF bAveryFleeingInVehicle = FALSE		
				IF IS_PED_UNINJURED(pedAvery)	
					IF bCometReverse = TRUE
						IF GET_GAME_TIMER() > iTimerChaseStarted + 5000		
							bAveryFleeingInVehicle = TRUE
							SET_ROADS_IN_ANGLED_AREA(<<-887.370972,-101.877480,34.387718>>, <<-989.135986,60.986832,58.366745>>, 31.500000,FALSE,TRUE)
						ELSE
							//IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
							//	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Comet11",DRIVINGMODE_AVOIDCARS_RECKLESS,1,EWAYPOINT_DEFAULT,30,-1)
							//ENDIF
							IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)	
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0)
									START_PLAYBACK_RECORDED_VEHICLE(vehAveryComet, 102, "Josh2_A2")
								ELSE
									//ACTIVATE_PHYSICS(vehAveryComet)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_GAME_TIMER() > iTimerChaseStarted + 5000
							bAveryFleeingInVehicle = TRUE
							SET_ROADS_IN_ANGLED_AREA(<<-887.370972,-101.877480,34.387718>>, <<-989.135986,60.986832,58.366745>>, 31.500000,FALSE,TRUE)
						ELSE
							//IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)	
							//	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Comet10",DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,-1)
							//ENDIF
							IF IS_PED_IN_VEHICLE(pedAvery,vehAveryComet)		
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0)
									START_PLAYBACK_RECORDED_VEHICLE(vehAveryComet, 101, "Josh2_A1")
								ELSE	
									//ACTIVATE_PHYSICS(vehAveryComet)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF bAveryFleeingInVehicle = TRUE
				IF bWaypointPlaybackStopped = FALSE	
					IF IS_PED_UNINJURED(pedAvery)	
						IF GET_GAME_TIMER() > iTimerChaseStarted + 10000
							DO_TREVOR_CHASE_LINES()
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehAveryComet)
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(pedAvery, <<-1716.821777,493.295959,125.739830>>, <<-1866.034668,483.705200,139.247787>>, 18.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(pedAvery, <<-1842.730591,472.350891,108.040329>>, <<-1904.855591,451.842285,138.281235>>, 43.500000)	
							IF GET_IS_WAYPOINT_RECORDING_LOADED("Josh2_Corner")
								IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)	
									TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedAvery,vehAveryComet,"Josh2_Corner",DRIVINGMODE_AVOIDCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT,-1,-1)
								ENDIF
							ENDIF
						ELSE	
							IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								TASK_VEHICLE_MISSION_PED_TARGET(pedAvery,vehAveryComet,PLAYER_PED_ID(),MISSION_FLEE,34.0,DRIVINGMODE_AVOIDCARS,20.0,20.0) //100.0,30.0     //35.0
							ENDIF
						ENDIF
						//SET_DRIVE_TASK_CRUISE_SPEED(pedAvery,fTopSpeed)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehAveryComet)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehAveryComet)
				IF IS_VEHICLE_OK(vehAveryComet)
					SAFE_TELEPORT_ENTITY(vehAveryComet,<< -1083.1884, 266.0258, 63.0025 >>, 111.2559)
				ENDIF
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< -1081.5244, 254.3746, 62.9404 >>, 356.8038)
			ENDIF
		ENDIF
		/*
		IF bCometSmashed = TRUE	
		//	IF GET_ENTITY_SPEED(vehAveryComet) < 4	
				SET_VEHICLE_REDUCE_GRIP(vehAveryComet,FALSE)
		//	ENDIF
		ENDIF
		*/
		IF GET_ENTITY_SPEED(vehAveryComet) < 1.5
			++iCometStuckCounter
			IF iCometStuckCounter > 250
				bWaypointPlaybackStopped = TRUE
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				bCometSmashed = TRUE
				bDoorFallenOff = TRUE
				bCometGotStuck = TRUE
			ENDIF
		ELSE
			IF GET_ENTITY_SPEED(vehAveryComet) > 12.0
				iCometStuckCounter = 0
			ENDIF
		ENDIF
		//PRINTINT(iCometStuckCounter)
		//PRINTNL()
		IF GET_ENTITY_HEALTH(vehAveryComet) < iCometInitHp - 300//370  //How much damage avery's car can take before he stops
		OR GET_VEHICLE_ENGINE_HEALTH(vehAveryComet) < fCometEngineHp - 300//370.0
		OR GET_VEHICLE_PETROL_TANK_HEALTH(vehAveryComet) < fCometPetrolHp - 300//370.0
		OR NOT IS_VEHICLE_DRIVEABLE(vehAveryComet)
		OR IS_ENTITY_UPSIDEDOWN(vehAveryComet)
		OR HAVE_TWO_TYRES_POPPED()
			//SET_VEHICLE_REDUCE_GRIP(vehAveryComet,TRUE)
			bCometSmashed = TRUE
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			bWaypointPlaybackStopped = TRUE
			IF bDoorFallenOff = FALSE
				//SET_VEHICLE_DOOR_BROKEN(vehAveryComet,SC_DOOR_FRONT_LEFT,FALSE)
				bDoorFallenOff = TRUE
			ENDIF
		ENDIF
		
		IF bCometSmashed = TRUE
			IF NOT bCometGotStuck
				SET_VEHICLE_ENGINE_HEALTH(vehAveryComet,101)  //299
			ENDIF
			IF GET_IS_VEHICLE_ENGINE_RUNNING(vehAveryComet)	
				SET_VEHICLE_ENGINE_ON(vehAveryComet,FALSE,TRUE)
			ENDIF
		ENDIF
		
		IF bWaypointPlaybackStopped = TRUE	
			
			IF GET_ENTITY_SPEED(vehAveryComet) < 14.0
				IF bPrintBeatAvery = FALSE
					//PRINT_NOW("JOSH2_15",DEFAULT_GOD_TEXT_TIME,0) 	//Beat up ~r~Avery.
					IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),40)
						IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_CRASH",CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)    //Nooooooooooo!
							/*
							ACT_light_finish_move
							ACT_light_low_finish_move
							ACT_light_high_finish_move
							ACT_heavy_finish_move
							ACT_heavy_low_finish_move
							ACT_heavy_high_finish_move
							
							SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_SuppressLethalMeleeActions,TRUE)
							
							These from Action_Table.meta
							
							*/
							//SET_DISABLE_AMBIENT_MELEE_MOVE(PLAYER_ID(),TRUE)
							/*
							ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_a"), FALSE) 
							ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_b"), FALSE) 
							ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_rear"), FALSE) 
							ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_heavy"), FALSE) 
							ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_armed_takedown"), FALSE) 
							*/
							
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOW)
							
							PRINT_NOW("JOSH2_15",DEFAULT_GOD_TEXT_TIME,0) 
							bPrintBeatAvery = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_PED_INCREASED_AVOIDANCE_RADIUS(pedAvery)
			
			IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),50)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)		
			ENDIF	
					
			IF bRoadsOffAroundAvery = FALSE	
				vNodeSwitch1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehAveryComet,<<0,-70,0>>)
				vNodeSwitch2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehAveryComet,<<0,70,0>>)
				SET_ROADS_IN_ANGLED_AREA(vNodeSwitch1,vNodeSwitch2,100,FALSE,FALSE)
				bRoadsOffAroundAvery = TRUE
			ENDIF
			
			IF IS_PED_UNINJURED(pedAvery)		
				/*
				IF iPleadConvo > 0
					//IF IS_PED_RAGDOLL(pedAvery)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","please_no_im")	
						AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","you_cant")	
						AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","what_do_you")		
						AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","my_lawyer")
						AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","base")	
							TASK_TURN_PED_TO_FACE_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
						ENDIF
					//ENDIF
				ENDIF
				*/
				IF NOT IS_PED_IN_ANY_VEHICLE(pedAvery)
				
					TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					IF NOT IS_STRING_NULL_OR_EMPTY(root)
						IF ARE_STRINGS_EQUAL(root,"JOSH2_CRASH")								
							KILL_ANY_CONVERSATION()
						ENDIF
					ENDIF
					
					SET_PED_SUFFERS_CRITICAL_HITS(pedAvery,TRUE)
					SET_PED_RESET_FLAG(pedAvery,PRF_PreventAllMeleeTakedowns, TRUE)	// must be called everyframe to prevent takedown and takedown failed
					
					IF NOT IS_PED_HEADTRACKING_PED(pedAvery,PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					ENDIF
					
					IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_HANDS_UP) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK		
					AND NOT	bAveryFleeingOnFoot
						TASK_TURN_PED_TO_FACE_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
					ENDIF
					
					VECTOR vAvery = GET_ENTITY_COORDS(pedAvery)
					IF IS_BULLET_IN_AREA(vAvery,10,TRUE)
						IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK	
							TEXT_LABEL_23 labeThreat =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
							IF NOT IS_STRING_NULL_OR_EMPTY(labeThreat)
								IF NOT ARE_STRINGS_EQUAL(labeThreat,"JOSH2_OUTRO_5")							
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ELSE
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
							SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
							TASK_REACT_AND_FLEE_PED(pedAvery,PLAYER_PED_ID())
							iTimerAveryFleeOnFoot = GET_GAME_TIMER()
							bAveryFleeingOnFoot = TRUE
						ENDIF
					ENDIF	
					/*
					IF bPrintBeatAvery = FALSE
						//PRINT_NOW("JOSH2_15",DEFAULT_GOD_TEXT_TIME,0) 						//Beat up ~r~Avery.
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						bPrintBeatAvery = TRUE
					ENDIF
					*/
					/*
					//IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK	
						IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK	
							IF NOT IS_PED_FACING_PED(pedAvery,PLAYER_PED_ID(),45)	
								//SET_ENTITY_COLLISION(pedAvery,TRUE)
								TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
								IF iPleadConvo = 0
								AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2","hands_up_anxious_scientist") 	
								//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2","hands_up_shocked_scientist")		
									TASK_TURN_PED_TO_FACE_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
								ENDIF
							ENDIF
						ENDIF
						*/
						//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),8.0)
								IF NOT IS_PED_RAGDOLL(pedAvery)	
									IF NOT IS_PED_GETTING_UP(pedAvery)	
										IF IS_PED_FACING_PED(pedAvery,PLAYER_PED_ID(),45) 
											IF iPleadConvo = 0
												//IF NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","hands_up_anxious_scientist")
												//	TASK_PLAY_ANIM(pedAvery,"rcmjosh2ig_1","hands_up_anxious_scientist",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
												//ENDIF
												/*
												IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
												AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
													
													OPEN_SEQUENCE_TASK(seqAveryCower)
														TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","idle_intro",1,SLOW_BLEND_OUT,-1)
														TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_OUT,SLOW_BLEND_OUT,-1,AF_LOOPING)
													CLOSE_SEQUENCE_TASK(seqAveryCower)
													
													TASK_PERFORM_SEQUENCE(pedAvery,seqAveryCower)
													CLEAR_SEQUENCE_TASK(seqAveryCower)
												
												ENDIF
												*/
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
												//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","idle_intro")	
													//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_15")	   //No! Don't hurt me! Please...
														
														IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),8.0)	
														AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
															IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", CONV_PRIORITY_MEDIUM)
															//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_1", CONV_PRIORITY_MEDIUM)
																IF iAveryTookMeleeHit = 0

																	//TASK_PLAY_ANIM(pedAvery,"rcmjosh2","easy_now",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
																	//TASK_PLAY_ANIM(pedAvery,"rcmjosh2","hands_up_anxious_scientist",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
																	/*
																	OPEN_SEQUENCE_TASK(seqAveryCower)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","idle_intro",2,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","what_do_you",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","my_lawyer",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																		//TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","please_no_im",SLOW_BLEND_OUT,SLOW_BLEND_OUT,-1)
																		//TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_OUT,SLOW_BLEND_OUT,-1)
																		//TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","you_cant",SLOW_BLEND_OUT,SLOW_BLEND_OUT,-1)
																		TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
																	CLOSE_SEQUENCE_TASK(seqAveryCower)
																	
																	TASK_PERFORM_SEQUENCE(pedAvery,seqAveryCower)
																	CLEAR_SEQUENCE_TASK(seqAveryCower)
																	*/
																ENDIF
																iPleadConvo = 1
															ENDIF
														ENDIF
													
													/*
													ELSE                                               
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
															IF iAveryTookMeleeHit = 0	
																TASK_PLAY_ANIM(pedAvery,"rcmjosh2","easy_now",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
															ENDIF
															iPleadConvo = 1
														ENDIF
													ENDIF
													*/
												ENDIF
											ELIF iPleadConvo = 1
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
													//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_15")	      //I'll pay you whatever Josh is paying you!       
														//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_2", CONV_PRIORITY_MEDIUM)
															//IF iAveryTookMeleeHit = 0
																//TASK_PLAY_ANIM(pedAvery,"rcmjosh2","enough",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
															//ENDIF
															iPleadConvo = 2
														//ENDIF
													/*
													ELSE
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_2", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
															IF iAveryTookMeleeHit = 0	
																TASK_PLAY_ANIM(pedAvery,"rcmjosh2","enough",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
															ENDIF
															iPleadConvo = 2
														ENDIF
													ENDIF
													*/
												ENDIF
											ELIF iPleadConvo = 2
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
													//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_15")	         //Bouncie Bouncie?
														//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_3", CONV_PRIORITY_MEDIUM)
															iPleadConvo = 3
														//ENDIF
													/*
													ELSE
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_3", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
															iPleadConvo = 3
														ENDIF
													ENDIF
													*/
												ENDIF
											ELIF iPleadConvo = 3
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
													//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("JOSH2_15")	        //What?
														//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_4", CONV_PRIORITY_MEDIUM)
															//IF iAveryTookMeleeHit = 0
																//TASK_PLAY_ANIM(pedAvery,"rcmjosh2","what",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
															//ENDIF
															iPleadConvo = 4
														//ENDIF
													/*
													ELSE
														IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_THREAT", "JOSH2_THREAT_4", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
															IF iAveryTookMeleeHit = 0	
																TASK_PLAY_ANIM(pedAvery,"rcmjosh2","what",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
															ENDIF
															iPleadConvo = 4
														ENDIF
													ENDIF
													*/
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							//ENDIF
						//ENDIF
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),pedAvery)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),pedAvery) 
					OR bTrevThreatenedAvery		
						IF NOT IS_PED_RAGDOLL(pedAvery)
							IF GET_ENTITY_SPEED(pedAvery) < 0.1
								IF GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_PISTOL
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_MG
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_SMG
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_SNIPER
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_RIFLE
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_STUNGUN
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_SHOTGUN
								OR GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_HEAVY
								OR bTrevThreatenedAvery	
									//IF NOT IS_PED_RAGDOLL(pedAvery)	
										bTrevThreatenedAvery = TRUE 
										
										TEXT_LABEL_23 rootThreat =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()	
										IF NOT IS_STRING_NULL_OR_EMPTY(rootThreat)
											IF ARE_STRINGS_EQUAL(rootThreat,"JOSH2_THREAT")							
												KILL_ANY_CONVERSATION()
											ENDIF
										ENDIF
										
										IF NOT bTrevThreat
											IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO" ,"JOSH2_OUTRO_5", CONV_PRIORITY_MEDIUM)	
												iGunThreaten = 4
												bTrevThreat = TRUE
											ENDIF
										ENDIF
										++iAveryGunThreat
										iTimerGunReact = iTimerGunReact		
											
											//IF NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","please_no_im")	
											//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","you_cant")	
											//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","what_do_you")		
											//AND NOT IS_ENTITY_PLAYING_ANIM(pedAvery,"rcmjosh2ig_1","my_lawyer")		
												/*
												SEQUENCE_INDEX seqGunThreat
												OPEN_SEQUENCE_TASK(seqGunThreat)
													IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
														TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","please_no_im",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
													ELSE
														TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","you_cant",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
													ENDIF
													TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","base",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED | AF_ACTIVATE_RAGDOLL_ON_COLLISION)
													//TASK_PLAY_ANIM(NULL,"rcmjosh2ig_1","you_cant",SLOW_BLEND_OUT,SLOW_BLEND_OUT,-1)
													//TASK_PLAY_ANIM(NULL,"rcmjosh2","hands_up_shocked_scientist",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_EXIT_AFTER_INTERRUPTED)
													//TASK_PLAY_ANIM(NULL,"rcmjosh2","hands_up_anxious_scientist",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING | AF_EXIT_AFTER_INTERRUPTED)
												CLOSE_SEQUENCE_TASK(seqGunThreat)
												TASK_PERFORM_SEQUENCE(pedAvery,seqGunThreat)
												CLEAR_SEQUENCE_TASK(seqGunThreat)
												*/
												//iTimerGunReact = GET_GAME_TIMER()
											//ENDIF
											
										//ENDIF
										IF iGunThreaten = 0
											IF iPleadConvo <> 0	
												KILL_FACE_TO_FACE_CONVERSATION()
												iGunThreaten = 1
											ENDIF
										ELIF iGunThreaten = 1
											//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
												//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_TARG", "JOSH2_GUN_2", CONV_PRIORITY_MEDIUM)
												//IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_GUN", CONV_PRIORITY_MEDIUM)
												//	iGunThreaten = 3
												//ENDIF
											//ENDIF
											IF iAveryGunThreat >= 10
												IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_HANDS_UP) <> PERFORMING_TASK
													SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
													TASK_HANDS_UP(pedAvery,-1,PLAYER_PED_ID(),-1,HANDS_UP_NOTHING)
												ENDIF	
												iGunThreaten = 3
											ENDIF
										
										/*
										ELIF iGunThreaten = 2
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
												//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_TARG", "JOSH2_GUN_2", CONV_PRIORITY_MEDIUM)
												IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_GUN", CONV_PRIORITY_MEDIUM)
													iGunThreaten = 3
												ENDIF
											ENDIF
										*/
										ELIF iGunThreaten = 3
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
												//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_TARG", "JOSH2_GUN_2", CONV_PRIORITY_MEDIUM)
												//IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_GUN", CONV_PRIORITY_MEDIUM)
												IF NOT bTrevThreat
													IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO" ,"JOSH2_OUTRO_5", CONV_PRIORITY_MEDIUM)	
														iGunThreaten = 4
														bTrevThreat = TRUE
													ENDIF
												ENDIF
											ENDIF
										
										ELIF iGunThreaten = 4
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
												IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK	
													SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
													TASK_REACT_AND_FLEE_PED(pedAvery,PLAYER_PED_ID())
													iTimerAveryFleeOnFoot = GET_GAME_TIMER()
													bAveryFleeingOnFoot = TRUE
												ENDIF
												iGunThreaten = 5
											ENDIF
										ENDIF
										
									//ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),8.0)	
							SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
							SET_PED_COMBAT_ATTRIBUTES(pedAvery, CA_USE_VEHICLE, FALSE)
							CLEAR_PED_LAST_WEAPON_DAMAGE(pedAvery)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedAvery)
							IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								TASK_LEAVE_ANY_VEHICLE(pedAvery,0,ECF_DONT_CLOSE_DOOR)
							ENDIF
							TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()	
							IF NOT IS_STRING_NULL_OR_EMPTY(root)
							AND NOT	IS_STRING_NULL_OR_EMPTY(label)
								IF ARE_STRINGS_EQUAL(root,"JOSH2_CRASH")				
								AND	ARE_STRINGS_EQUAL(label,"JOSH2_CRASH_3")				
									KILL_ANY_CONVERSATION()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF bPrintBeatAvery = TRUE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
							SET_PED_COMBAT_ATTRIBUTES(pedAvery, CA_USE_VEHICLE, FALSE)
							CLEAR_PED_LAST_WEAPON_DAMAGE(pedAvery)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedAvery)
							IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								TASK_LEAVE_ANY_VEHICLE(pedAvery,0,ECF_DONT_CLOSE_DOOR)
							ENDIF
							TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()	
							IF NOT IS_STRING_NULL_OR_EMPTY(root)
							AND NOT	IS_STRING_NULL_OR_EMPTY(label)
								IF ARE_STRINGS_EQUAL(root,"JOSH2_CRASH")				
								AND	ARE_STRINGS_EQUAL(label,"JOSH2_CRASH_3")				
									KILL_ANY_CONVERSATION()
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
					bWaypointPlaybackStopped = TRUE
				ENDIF
			ENDIF
		ENDIF
	//ENDIF	

	IF NOT IS_PED_IN_ANY_VEHICLE(pedAvery)	
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedAvery) < 15.0
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAvery,PLAYER_PED_ID())	
				CPRINTLN(DEBUG_MISSION, "!!!!!!!!AVERY DAMAGED BY PLAYER!!!!!!!!")
				IF GET_WEAPONTYPE_GROUP(GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_MELEE
					++iAveryTookMeleeHit
					IF iAveryTookMeleeHit > iPunchesAveryCanTake
					AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedAvery,WEAPONTYPE_UNARMED)
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JO2_JOSH_MELEED) 
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MISSION, "STATS <><> PULVERISER <><> STATS")
						#ENDIF
					ENDIF
				ELSE
					iAveryTookMeleeHit = 5  //Player ran over or shot avery (or used stun gun or something) just make avery give up
				ENDIF
				CLEAR_PED_LAST_WEAPON_DAMAGE(pedAvery)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedAvery)
			ENDIF
		ENDIF
	ENDIF
	//IF NOT bAveryFleeingOnFoot
	//OR IS_PED_BEING_STUNNED(pedAvery)	
	IF IS_PED_FLEEING(pedAvery)	
		++iAveryFlee
	ENDIF
	IF iAveryFlee <= 4
		IF iAveryTookMeleeHit > iPunchesAveryCanTake
		AND NOT IS_PED_INJURED(pedAvery)
		AND NOT IS_ENTITY_ON_FIRE(pedAvery)	 	
			iTimerAveryKO = GET_GAME_TIMER()
			IF DOES_BLIP_EXIST(blipAvery)
				SET_BLIP_SCALE(blipAvery,0.7)
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				bPlayerRanAveryOver = TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "JOSH2**** Init go to Avery ****")
			#ENDIF
			TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()	
			IF NOT IS_STRING_NULL_OR_EMPTY(root)
			AND NOT	IS_STRING_NULL_OR_EMPTY(label)
				IF ARE_STRINGS_EQUAL(root,"JOSH2_THREAT")				
					IF ARE_STRINGS_EQUAL(label,"JOSH2_THREAT_3")				
						KILL_FACE_TO_FACE_CONVERSATION()
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
				IF ARE_STRINGS_EQUAL(root,"JOSH2_CRASH")				
					IF ARE_STRINGS_EQUAL(label,"JOSH2_CRASH_2")				
					OR ARE_STRINGS_EQUAL(label,"JOSH2_CRASH_4")				
						KILL_FACE_TO_FACE_CONVERSATION()
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
				IF ARE_STRINGS_EQUAL(root,"JOSH2_GUN")				
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ENDIF
			CLEAR_PED_TASKS(pedAvery)
			eMissionStage = MS_GO_TO_AVERY
		ENDIF
	ENDIF

	Rubberband()
							
ENDPROC

/// PURPOSE: Knocking Avery out
PROC GoToAvery()
	
	IF IS_PED_UNINJURED(pedAvery)			
		IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) = PERFORMING_TASK		
		OR GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) = WAITING_TO_START_TASK
			CLEAR_PED_TASKS(pedAvery)
		ENDIF
		STOP_AMBIENT_CARS_AROUND_PED(pedAvery)
		IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),50)
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)		
		ENDIF	
		SET_PED_RESET_FLAG(pedAvery,PRF_PreventAllMeleeTakedowns, TRUE)	// must be called everyframe to prevent takedown and takedown failed
		IF NOT IS_PED_RAGDOLL(pedAvery)
			/*
			SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
			vAvery = GET_ENTITY_COORDS(pedAvery)
			CLEAR_PED_TASKS_IMMEDIATELY(pedAvery)
			TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedAvery,-1)
			SET_PED_KEEP_TASK(pedAvery,TRUE)
			IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
				//GET_NTH_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedAvery,<<-20.0,0.0,0.0>>),129,vSafeCoord)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(pedAvery,vSafeCoord,PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_NEVER_WARP,2.0,ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
				TASK_WANDER_STANDARD(pedAvery)
			ENDIF
			SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
			*/
			IF bPlayerRanAveryOver = TRUE
			ENDIF
			//IF bPlayerRanAveryOver = TRUE
				//SET_PED_TO_RAGDOLL(pedAvery,2000,2000,TASK_RELAX)
			//ELSE
				//SET_PED_TO_RAGDOLL_WITH_FALL(pedAvery,2000,2000,TYPE_OVER_WALL,GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()),vAvery.z,<<0,0,0>>,<<0,0,0>>)	
				
				/*
				SET_PED_TO_RAGDOLL(pedAvery,1500,4000,TASK_NM_SCRIPT)
				CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
				GIVE_PED_NM_MESSAGE(pedAvery)
				*/
			
			//ENDIF
		ENDIF
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(),pedAvery) < 8.0
			iTimerAveryKO = GET_GAME_TIMER()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "JOSH2**** Init Avery KO ****")
			#ENDIF
			eMissionStage = MS_AVERY_KO
		ENDIF	
	ENDIF
	
ENDPROC

/// PURPOSE: Avery plays shellshock anim then gets up and staggers off
PROC AveryKo()
	
	//INT iShockAvery
	
	IF IS_PED_UNINJURED(pedAvery)
		VECTOR vAvery = GET_ENTITY_COORDS(pedAvery)
		IF IS_BULLET_IN_AREA(vAvery,10,TRUE)
			IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK	
				TEXT_LABEL_23 labeThreat =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				IF NOT IS_STRING_NULL_OR_EMPTY(labeThreat)
					IF NOT ARE_STRINGS_EQUAL(labeThreat,"JOSH2_OUTRO_1")
					AND NOT ARE_STRINGS_EQUAL(labeThreat,"JOSH2_OUTRO_5")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
				SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
				TASK_REACT_AND_FLEE_PED(pedAvery,PLAYER_PED_ID())
				iTimerAveryFleeOnFoot = GET_GAME_TIMER()
				bAveryFleeingOnFoot = TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF bAveryFleeingOnFoot
		IF GET_GAME_TIMER() > iTimerAveryFleeOnFoot + 5000
			SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
			SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
			SET_PED_KEEP_TASK(pedAvery, TRUE)
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 2.0, REPLAY_IMPORTANCE_LOWEST)
			Script_Passed()	
		ENDIF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()												
			IF iAveryOhFuckLines < 3
				IF CREATE_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_SHOCKH", CONV_PRIORITY_MEDIUM)
					++iAveryOhFuckLines
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > iTimerAveryKO + 2000	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				IF bTrevThreat
					/*
					IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SHOCKING_EVENT_HURRY_AWAY) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SHOCKING_EVENT_HURRY_AWAY) <> WAITING_TO_START_TASK		
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
						SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
						SET_PED_DESIRED_MOVE_BLEND_RATIO(pedAvery, PEDMOVEBLENDRATIO_WALK)				
						SET_PED_KEEP_TASK(pedAvery, TRUE)
						iShockAvery = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MUGGING,PLAYER_PED_ID())
						TASK_SHOCKING_EVENT_HURRY_AWAY(pedAvery,iShockAvery)
					ENDIF
					*/
					iAveryKOLines = 5
				ENDIF
				IF iAveryKOLines = 0	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO","JOSH2_OUTRO_1",CONV_PRIORITY_VERY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0)
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						++iAveryKOLines
					ENDIF
				ELIF iAveryKOLines = 1	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO","JOSH2_OUTRO_3",CONV_PRIORITY_VERY_HIGH)
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						++iAveryKOLines
					ENDIF
				ELIF iAveryKOLines = 2	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO","JOSH2_OUTRO_5",CONV_PRIORITY_VERY_HIGH)
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						++iAveryKOLines
					ENDIF
				ELIF iAveryKOLines = 3	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO","JOSH2_OUTRO_7",CONV_PRIORITY_VERY_HIGH)
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						++iAveryKOLines
					ENDIF
				ELIF iAveryKOLines = 4	
					/*
					IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SHOCKING_EVENT_HURRY_AWAY) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SHOCKING_EVENT_HURRY_AWAY) <> WAITING_TO_START_TASK			
						
						SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
						SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
						SET_PED_DESIRED_MOVE_BLEND_RATIO(pedAvery, PEDMOVEBLENDRATIO_WALK)				
						SET_PED_KEEP_TASK(pedAvery, TRUE)
						iShockAvery = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MUGGING,PLAYER_PED_ID())
						TASK_SHOCKING_EVENT_HURRY_AWAY(pedAvery,iShockAvery)	
					ENDIF
					*/
					//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, "JOSH2AU", "JOSH2_OUTRO","JOSH2_OUTRO_5",CONV_PRIORITY_VERY_HIGH)
						//SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						++iAveryKOLines
					//ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
		IF IS_PED_UNINJURED(pedAvery)		
			IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) = PERFORMING_TASK		
			OR GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_REACT_AND_FLEE_PED) = WAITING_TO_START_TASK
				CLEAR_PED_TASKS(pedAvery)
			ENDIF
			IF NOT IS_PED_RAGDOLL(pedAvery)	
				IF NOT IS_PED_HEADTRACKING_PED(pedAvery,PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK	
					IF NOT IS_PED_FACING_PED(pedAvery,PLAYER_PED_ID(),45)	
						TASK_TURN_PED_TO_FACE_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
					ENDIF
				ENDIF
			ENDIF
			STOP_AMBIENT_CARS_AROUND_PED(pedAvery)
			IF IS_ENTITY_IN_RANGE_ENTITY(pedAvery,PLAYER_PED_ID(),50)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)		
			ENDIF	
			SAFE_REMOVE_BLIP(blipAvery)
			SET_PED_RESET_FLAG(pedAvery,PRF_PreventAllMeleeTakedowns, TRUE)	// must be called everyframe to prevent takedown and takedown failed
			IF GET_GAME_TIMER() < iTimerAveryKO + 9000 		
			OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF GET_GAME_TIMER() < iTimerAveryKO + 9000 	
					IF GET_GAME_TIMER() < iTimerAveryKO + 4500	

						IF NOT IS_PED_RAGDOLL(pedAvery)
						AND NOT bTrevThreat
							
							REQUEST_RAGDOLL_BOUNDS_UPDATE(pedAvery)

							SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
							CLEAR_PED_TASKS_IMMEDIATELY(pedAvery)
							TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedAvery,-1)
							SET_PED_KEEP_TASK(pedAvery,TRUE)
							
							
							SET_PED_TO_RAGDOLL(pedAvery,2500,3000,TASK_NM_SCRIPT)
							//SET_PED_TO_RAGDOLL(pedAvery,58000,60000,TASK_RELAX)
							CLEAR_RAGDOLL_BLOCKING_FLAGS(pedAvery,RBF_NONE)
							
							CREATE_NM_MESSAGE(NM_START_START,NM_INJURED_ON_GROUND_MSG)
							GIVE_PED_NM_MESSAGE(pedAvery)
				
					
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedAvery)
							
							//SET_PED_INJURED_ON_GROUND_BEHAVIOUR(pedAvery,60000)
							
						ENDIF
						
					
					ELSE
						IF IS_PED_GETTING_UP(pedAvery)
							IF bAveryGettingUp = FALSE
								iTimerAveryGettingUp = GET_GAME_TIMER()
								bAveryGettingUp = TRUE
							ELSE	
								IF GET_GAME_TIMER() > iTimerAveryGettingUp + 2200
									
								ENDIF
							ENDIF
						ELSE
							IF bAveryGettingUp = TRUE	
								IF NOT IS_PED_RAGDOLL(pedAvery)
									IF NOT IS_PED_HEADTRACKING_PED(pedAvery,PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
									ENDIF
									IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK	
										IF NOT IS_PED_FACING_PED(pedAvery,PLAYER_PED_ID(),90)	
											TASK_TURN_PED_TO_FACE_ENTITY(pedAvery,PLAYER_PED_ID(),-1)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_PED_MOVEMENT_CLIPSET(pedAvery,"MOVE_M@BAIL_BOND_NOT_TAZERED")
				SET_PED_COMBAT_ATTRIBUTES(pedAvery,CA_ALWAYS_FLEE,TRUE)
				SET_PED_DESIRED_MOVE_BLEND_RATIO(pedAvery, PEDMOVEBLENDRATIO_WALK)				
				SET_PED_KEEP_TASK(pedAvery, TRUE)
				IF IS_ENTITY_ALIVE(vehAveryComet)	
					SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(vehAveryComet,FALSE)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedAvery,SCRIPT_TASK_SMART_FLEE_PED) <> WAITING_TO_START_TASK

					//IF NOT IS_PED_RAGDOLL(pedAvery)	
					//iShock = TASK_SHOCKING_EVENT_HURRY_AWAY()	
						//TASK_WANDER_SPECIFIC(pedAvery,"WANDER","CODE_HUMAN_WANDER_IDLES_MALE",GET_ENTITY_HEADING(PLAYER_PED_ID()))
					//ENDIF
					
					//iShockAvery = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MUGGING,PLAYER_PED_ID())
					
					//TASK_SHOCKING_EVENT_HURRY_AWAY(pedAvery,iShockAvery)	
					TASK_SMART_FLEE_PED(pedAvery,PLAYER_PED_ID(),100,-1)
				ELSE
					IF iAveryKOLines >= 5
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAvery,FALSE)
						Script_Passed()	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_JOSH_2(sRCLauncherDataLocal)
	  		WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF		
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSynchScene)	
		TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(iSynchScene)	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "JOSH2 TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(iSynchScene)")
		#ENDIF
	ENDIF
	
	WHILE(TRUE)
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_CTD")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		// Loop within here until the mission passes or fails
		
		IF eMissionStage <> MS_SETUP
		//AND eMissionStage <> MS_INTRO
		AND eMissionStage <> MS_INIT
			OpenGates()
			IF eMissionStage <> MS_MISSION_FAILING	
				CheckForFail()
			ENDIF
			AmbientBiker()
			Convo()
		ENDIF
		
		// MAIN LOOP
		SWITCH eMissionStage
			
			CASE MS_SETUP
				Setup()
			BREAK
			
			CASE MS_INTRO
				IntroMocap()
			BREAK
			
			CASE MS_INIT
				InitMission()
			BREAK
			
			CASE MS_GO_TO_ROCKFORD
				GoToRockford()
			BREAK
			
			CASE MS_FIND_THE_GREEN_COMET			
				FindTheGreenComet()
			BREAK
			
			CASE MS_CHASE_AVERY
				ChaseAvery()
			BREAK
			
			CASE MS_GO_TO_AVERY
				GoToAvery()
			BREAK
			
			CASE MS_AVERY_KO
				AveryKo()
			BREAK
			
			CASE MS_MISSION_FAILING
				FailWait()
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD // STAGE SKIPPING
			IF eMissionStage <> MS_MISSION_FAILING
				DEBUG_Check_Debug_Keys()
				DEBUG_Check_Skips()
			ENDIF
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

