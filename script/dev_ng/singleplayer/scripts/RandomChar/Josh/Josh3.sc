// MISSION NAME : Josh3.sc
// AUTHOR : Kev Edwards
// DESCRIPTION : Collect fuel can and burn down Josh's house

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "dialogue_public.sch"
USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"
USING "initial_scenes_Josh.sch"
USING "building_control_public.sch"
USING "script_ped.sch"
USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"
USING "RC_Area_public.sch"
USING "rgeneral_include.sch"
CONST_INT MAX_TRAIL_POINTS 48
USING "gas_trails.sch"
USING "commands_recording.sch"
USING "taxi_functions.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

g_structRCScriptArgs sRCLauncherDataLocal

CONST_INT CP_LEAD_IN 0 // Checkpoints
CONST_INT CP_GET_IN_CAR 1
CONST_INT CP_ARRIVE_AT_HOUSE 2
CONST_INT CP_MISSION_PASSED 3 // Only used for shitskips

CONST_INT Z_SKIP_LEAD_IN 0 // Z Skips
CONST_INT Z_SKIP_INTRO 1
CONST_INT Z_SKIP_GET_IN_CAR 2
CONST_INT Z_SKIP_GO_TO_BBQ 3
CONST_INT Z_SKIP_DROP_FUEL 4
CONST_INT Z_SKIP_LEAVE_AREA 5
CONST_INT Z_SKIP_MISSION_PASSED 6

ENUM JOSH3_MISSION_STATE
	stateNULL,
	stateLeadIn,
	stateWalkInMotelCutscene,
	stateIntro,
	stateGetInCar,
	stateGoToHouse,
	stateGoToBBQ,
	stateDropFuel,
	stateShootFuel,
	stateOutro,
	stateLeaveArea,
	stateLoseCops,
	statePhoneCall,
	stateMissionPassed,
	stateFailed,
	NUM_MISSION_STATES
ENDENUM
JOSH3_MISSION_STATE missionState = stateLeadIn // Stores the current JOSH3_MISSION_STATE
JOSH3_MISSION_STATE missionStateSkip = stateNULL // Used if needing to change to a JOSH3_MISSION_STATE out of sequence

ENUM JOSH3_STATE_MACHINE
	JOSH3_STATE_MACHINE_SETUP,
	JOSH3_STATE_MACHINE_LOOP,
	JOSH3_STATE_MACHINE_CLEANUP
ENDENUM
JOSH3_STATE_MACHINE missionStateMachine = JOSH3_STATE_MACHINE_SETUP

ENUM FAILED_REASON
	FAILED_DEFAULT = 0,
	FAILED_CAR_WRECKED,
	FAILED_USED_UP_FUEL,
	FAILED_DIDNT_COLLECT_FUEL,
	FAILED_USED_TOO_MUCH_GASOLINE,
	FAILED_SHOT_TRAIL_TOO_EARLY,
	FAILED_JOSH_DIED,
	FAILED_JOSH_INJURED,
	FAILED_JOSH_SCARED,
	FAILED_DIDNT_GO_TO_MOTEL_ROOM,
	FAILED_DIDNT_BURN_HOUSE
ENDENUM
FAILED_REASON failReason
STRING failString

ENUM STATUS_SHOOT_FUEL
	STATUS_SHOOT_FUEL_HAS_WEAPON,
	STATUS_SHOOT_FUEL_NO_WEAPON,
	STATUS_SHOOT_FUEL_FAR_AWAY
ENDENUM
STATUS_SHOOT_FUEL statusShootFuel

ENUM STATUS_MISSION_OBJECTIVE
	MISSION_OBJECTIVE_NORMAL, // Player is on his normal mission objective for that particular missionState
	MISSION_OBJECTIVE_LOSE_COPS // Player has alerted the cops and need to lose them before returning to the current missionState
ENDENUM

BOOL bZSkipping = FALSE // Changes to TRUE if z skipping to prevent subsequent mission state changes etc

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 6
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintToTTY = TRUE
	INT iDebugSkipTime // Prevents skipping instantaneously
#ENDIF

structTimelapse sTimelapse
TIMEOFDAY todReference
INT iSkipToHour
CONST_INT CONST_NUM_HOURS_SKIP_FOR_MOTEL_TIMELAPSE 2

VECTOR vPlayerCarRespotAtStart = <<572.4306, -1765.0028, 28.1693>>
CONST_FLOAT fPlayerCarRespotAtStart 354.2095
VECTOR vPlayerCarRespotAtHouse = <<-1116.9021, 281.7206, 64.2895>>
CONST_FLOAT fPlayerCarRespotAtHouse 101.2209
VEHICLE_INDEX vehForReplay

VECTOR vMotelRoom = <<550.3759, -1772.4634, 32.4478>>
VECTOR vInsideMainGate = << -1105.96240, 292.88455, 63.58510 >>
CONST_FLOAT fInsideMainGate 203.9533
VECTOR vHouse = << -1116, 309, 67 >>
VECTOR vBBQLocate = << -1116.11719, 318.26044, 65.97770 >>

VECTOR vPlayerLeadInCP = << 567.3073, -1762.9607, 28.1695 >>
CONST_FLOAT fPlayerLeadInCP 161.1730
VECTOR vPlayerGetInCarCP = << 565.4832, -1767.7043, 28.1470 >>
CONST_FLOAT fPlayerGetInCarCP 66.0379
VECTOR vPlayerGoToBBQCP = << -1102.4427, 284.4827, 63.0251 >>
CONST_FLOAT fPlayerGoToBBQCP 40.6354
VECTOR vPlayerMissionPassedCP = << -1069.3167, 236.0038, 62.3195 >>
CONST_FLOAT fPlayerMissionPassedCP 24.0453

OBJECT_INDEX oBBQ
MODEL_NAMES modelBBQ
VECTOR vBBQObject = << -1116.31, 317.28, 65.97 >>
CONST_FLOAT fBBQObject 170.40

BOOL bDisplayedShootFuelObjective
BOOL bDisplayedFuelHelpText
STATUS_MISSION_OBJECTIVE missionCurrentObjective
BOOL bToldToLoseWantedLevel
BOOL bGivenCarDistanceWarning
BOOL bGivenWastingFuelWarning
INT iPlayerFuelBeforeMission
INT iMaxJerryCanAmmo
BOOL bStartedPouringFuel
BOOL bDidntPourFuelInOneGo

BLIP_INDEX blipMotelRoom
BLIP_INDEX blipJoshCar
BLIP_INDEX blipGate
BLIP_INDEX blipBBQ
BLIP_INDEX blipHouse

VEHICLE_INDEX vehJoshCar
MODEL_NAMES modelJoshCar
VECTOR vJoshCar = << 558.09, -1765.39, 28.86 >>
CONST_FLOAT fJoshCar 335.3252
INT colourJoshCar = 4

structPedsForConversation sTrevorConversation 
BOOL bDoneLeadInConversation
BOOL bDoneCarefulWithCarConversation
BOOL bDoneJoshFuelConversation
BOOL bDoneJoshReminderConversation
BOOL bStoppedJoshReminderConversation
BOOL bDoneCollectedFuelConversation
BOOL bDoneTrevorCarConversation
BOOL bDoneJoshPhoneConversation
BOOL bDoneAfterPhoneConversation
BOOL bSentTextMessage
BOOL bDoneTrevorClimbWallConversation
BOOL bDoneTrevorOverWallConversation
BOOL bDoneTrevorCopsConversation
BOOL bDisplayedLeaveAreaObjective
BOOL bStartedMusic
BOOL bHasPlayerGotInAnyVehicle
INT iPourFuelConversationsDone
BOOL bDoneFinalPhoneCallConversation
INT iCounterDialogueDamageCar

INT iDoneLeadInConversationTimer
INT iEnterMotelRoomTimer
INT iJoshReminderTimer
INT iJoshPhoneTimer
INT iLeaveAreaTimer
INT iRestoreControlTimer
INT iDisplayFuelHelpTimer
INT iCameraChangeTimer
INT iEndCutsceneTimer
INT iPlayerHungUpPhoneTimer
INT iAfterPhoneTimer
INT iPourFuelConversationTimer
INT iDialogueDamageCarTimer

PTFX_ID fxBigFire
VECTOR vBigFire
VECTOR rBigFire
INT iBBQExplosionSound = GET_SOUND_ID()
INT iHouseFireSound = GET_SOUND_ID()
VECTOR vLightFromFire = <<-1107.8625, 296.1609, 65.3295>>
// These are each slightly further back <<-1105.8230, 292.4114, 63.5407>> <<-1105.8230, 292.4114, 63.5407>> <<-1105.8230, 292.4114, 63.5407>>

CONST_INT JOSH3_CAMERA_FRONT_PATHWAY 0
CONST_INT JOSH3_CAMERA_REAR_PATHWAY 1
CONST_INT JOSH3_CAMERA_EXPLODE_BBQ 2
CONST_INT NUM_OUTRO_CAMERAS 3
STRUCT JOSH3_OUTRO_CAMERA
	VECTOR vCamPosStart
	VECTOR vCamRotStart
	VECTOR vCamPosEnd
	VECTOR vCamRotEnd
	FLOAT fFOV
	BOOL bDisplayedCamera = FALSE
	VECTOR vNearestTrailPoint
	FLOAT fTriggerRange
	CAMERA_GRAPH_TYPE camGraphType
ENDSTRUCT
JOSH3_OUTRO_CAMERA cameraOutro[NUM_OUTRO_CAMERAS]
CAMERA_INDEX camCutscene
INT iCutsceneStage = 0
BOOL bCutsceneSkipped
BOOL bDoneCutsceneStartCommands
INT iLastCameraDisplayed

STRUCT JOSH3_EXPLOSION_LOCATION
	BOOL bDoneExplosion
	INT iExplosionTimer
ENDSTRUCT
JOSH3_EXPLOSION_LOCATION explosionOutro

TEST_POLY g_polyPlayArea

PED_INDEX pedJosh
REL_GROUP_HASH relGroupFriendly
INT iJoshSynchedScene
VECTOR vJoshSynchedScenePos = <<563.76, -1773.86, 28.36>>
VECTOR vJoshSynchedSceneRot = <<-0.00, 0.05, -110.70>>
INT iTrevorSynchedScene
CAMERA_INDEX cameraAnim

SCENARIO_BLOCKING_INDEX blockScenarios

STREAMVOL_ID streamVolumeJosh3

BOOL bStartedReplayEvent = FALSE

BOOL bDone1stPersonCameraFlash = FALSE

/// PURPOSE:
///    Used in every JOSH3_STATE_MACHINE_SETUP. Advances state machine to JOSH3_STATE_MACHINE_LOOP.
PROC JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Initialising ", s_text) ENDIF #ENDIF
	bZSkipping = FALSE
	bToldToLoseWantedLevel = FALSE
	missionCurrentObjective = MISSION_OBJECTIVE_NORMAL
	#IF IS_DEBUG_BUILD iDebugSkipTime = GET_GAME_TIMER() #ENDIF
	missionStateMachine = JOSH3_STATE_MACHINE_LOOP
ENDPROC

/// PURPOSE:
///    Used in every JOSH3_STATE_MACHINE_CLEANUP. Advances state machine to JOSH3_STATE_MACHINE_SETUP.
PROC JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD STRING s_text #ENDIF)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Cleaning up ", s_text) ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP(TRUE)
	IF missionStateSkip = stateNULL
		missionState = INT_TO_ENUM(JOSH3_MISSION_STATE, ENUM_TO_INT(missionState) + 1)
	ELSE
		missionState = missionStateSkip
	ENDIF
	missionStateSkip = stateNULL
	missionStateMachine = JOSH3_STATE_MACHINE_SETUP
ENDPROC

/// PURPOSE:
///    Safely remove all blips in this mission.
PROC JOSH3_REMOVE_ALL_BLIPS()	
	SAFE_REMOVE_BLIP(blipMotelRoom)
	SAFE_REMOVE_BLIP(blipJoshCar)
	SAFE_REMOVE_BLIP(blipGate)
	SAFE_REMOVE_BLIP(blipBBQ)
	SAFE_REMOVE_BLIP(blipHouse)
ENDPROC

/// PURPOSE:
///    Models required for a mission state are all loaded when the mission state inits, but this is a safety check to ensure a model is loaded before subsequently creating an entity using it.
PROC JOSH3_ENSURE_MODEL_IS_LOADED(MODEL_NAMES e_model)
	REQUEST_MODEL(e_model)
	IF NOT HAS_MODEL_LOADED(e_model)
		WHILE NOT HAS_MODEL_LOADED(e_model)
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely creates a vehicle.
PROC JOSH3_CREATE_VEHICLE(VEHICLE_INDEX &veh_to_create, MODEL_NAMES e_model, VECTOR v_pos, FLOAT f_heading = 0.0, INT i_colour = -1)
	IF NOT DOES_ENTITY_EXIST(veh_to_create)
		JOSH3_ENSURE_MODEL_IS_LOADED(e_model)
		veh_to_create = CREATE_VEHICLE(e_model, v_pos, f_heading)
		IF i_colour >= 0
			SET_VEHICLE_COLOURS(veh_to_create, i_colour, i_colour)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all vehicles in this mission.
PROC JOSH3_REMOVE_ALL_VEHICLES(BOOL b_delete = FALSE)
	IF b_delete = TRUE
		SAFE_DELETE_VEHICLE(vehJoshCar)
		SAFE_DELETE_VEHICLE(vehForReplay)
	ELSE
		SAFE_RELEASE_VEHICLE(vehJoshCar)
		SAFE_RELEASE_VEHICLE(vehForReplay)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets ped to wander after mission.
PROC JOSH3_MAKE_PED_WANDER()
	IF IS_ENTITY_ALIVE(pedJosh)
	AND NOT IsPedPerformingTask(pedJosh, SCRIPT_TASK_WANDER_STANDARD)
	AND NOT IsPedPerformingTask(pedJosh, SCRIPT_TASK_SMART_FLEE_PED)
		SET_PED_KEEP_TASK(pedJosh, TRUE)
		TASK_WANDER_STANDARD(pedJosh)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Set Josh to wander") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mission is failing so set alive peds to flee from the player.
PROC JOSH3_SET_PED_TO_FLEE()
	IF IS_ENTITY_ALIVE(pedJosh)
		CLEAR_PED_TASKS(pedJosh)
		SET_PED_KEEP_TASK(pedJosh, TRUE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			TASK_SMART_FLEE_PED(pedJosh, PLAYER_PED_ID(), 100, -1)
			MAKE_PED_SCREAM(pedJosh)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Set Josh to flee") ENDIF #ENDIF
		ELSE
			TASK_WANDER_STANDARD(pedJosh)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Set Josh to wander") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all peds in this mission.
PROC JOSH3_REMOVE_ALL_PEDS(BOOL b_delete = FALSE)
	IF b_delete = TRUE
		SAFE_DELETE_PED(pedJosh)
	ELSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID()) // Josh should flee if Franklin has died, otherwise only be set to wander
			JOSH3_MAKE_PED_WANDER()
		ELSE
			JOSH3_SET_PED_TO_FLEE()
		ENDIF	
		SAFE_RELEASE_PED(pedJosh)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely remove all script fire vfx.
PROC JOSH3_CLEANUP_BIG_FIRE()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(fxBigFire)
		STOP_PARTICLE_FX_LOOPED(fxBigFire)	
		fxBigFire = NULL
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up everything conceivably setup in this mission.
/// PARAMS:
///    bTerminateMission - TRUE if cleaning up before terminating, FALSE if cleaning up before a debug skip.
PROC JOSH3_MISSION_CLEANUP(BOOL b_delete_entities = FALSE, BOOL bTerminateMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Cleaning up mission...") ENDIF #ENDIF
	CLEAR_PRINTS()
	CLEAR_HELP()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	SAFE_RELEASE_OBJECT(oBBQ)
	JOSH3_REMOVE_ALL_BLIPS()
	JOSH3_REMOVE_ALL_PEDS(b_delete_entities)
	JOSH3_REMOVE_ALL_VEHICLES(b_delete_entities)
	JOSH3_CLEANUP_BIG_FIRE()
	SET_MODEL_AS_NO_LONGER_NEEDED(modelJoshCar)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelJoshCar, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBBQ)
	IF ASSISTED_MOVEMENT_IS_ROUTE_LOADED("Jo_3")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("Jo_3")
	ENDIF
	REMOVE_ANIM_DICT("rcmjosh3")
	REMOVE_ANIM_DICT("rcmjosh")
	REMOVE_ANIM_DICT("reaction@shellshock@unarmed")
	REMOVE_PTFX_ASSET()
	DESTROY_ALL_CAMS()
	RESET_GAS_TRAIL(FALSE)
	SET_WANTED_LEVEL_MULTIPLIER(1)
	SET_CREATE_RANDOM_COPS(TRUE)
	CLEAR_WEATHER_TYPE_PERSIST()
	STOP_SOUND(iBBQExplosionSound)
	STOP_SOUND(iHouseFireSound)
	STOP_STREAM()
	SET_AMBIENT_ZONE_LIST_STATE("AZL_JOSH_HOUSE_BURNING", FALSE, FALSE)
	RELEASE_SCRIPT_AUDIO_BANK()
	RELEASE_AMBIENT_AUDIO_BANK()
	TRIGGER_MUSIC_EVENT("JOSH3_MISSION_FAIL")
	RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
	IF STREAMVOL_IS_VALID(streamVolumeJosh3)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Deleting streamVolumeJosh3") ENDIF #ENDIF
		STREAMVOL_DELETE(streamVolumeJosh3)
	ENDIF
	DISABLE_TAXI_HAILING(FALSE) // B*2153231 Allow being able to hail a taxi
	IF bTerminateMission = TRUE
		REMOVE_PED_FOR_DIALOGUE(sTrevorConversation, 2)
		REMOVE_PED_FOR_DIALOGUE(sTrevorConversation, 3)
		CLEANUP_GAS_TRAILS()
		CLEAR_PED_NON_CREATION_AREA()
		REMOVE_SCENARIO_BLOCKING_AREA(blockScenarios)
		SET_PED_PATHS_BACK_TO_ORIGINAL(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>, TRUE)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
				DELETE_WIDGET_GROUP(widgetGroup)
			ENDIF
		#ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Cleaned up mission") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Standard RC mission cleanup function.
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()

	IF (Random_Character_Cleanup_If_Triggered())
		JOSH3_MISSION_CLEANUP(FALSE, TRUE)
	ENDIF
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Standard RC mission passed function.
PROC Script_Passed()
	Random_Character_Passed(CP_RAND_C_JOS3)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Mission failed function.
PROC JOSH3_MISSION_FAILED(FAILED_REASON reason = FAILED_DEFAULT)
	IF bZSkipping = FALSE
		failReason = reason
		missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
		missionStateSkip = stateFailed
	ENDIF
ENDPROC

/// PURPOSE:
///    Death checks for any mission critical entities.
PROC JOSH3_DEATH_CHECKS()
	IF ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
		IF DOES_ENTITY_EXIST(pedJosh)
			IF IS_ENTITY_DEAD(pedJosh)
				JOSH3_MISSION_FAILED(FAILED_JOSH_DIED)
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedJosh)
					JOSH3_MISSION_FAILED(FAILED_JOSH_INJURED)
				ENDIF
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedJosh, PLAYER_PED_ID())
					JOSH3_MISSION_FAILED(FAILED_JOSH_INJURED)
				ELIF HAS_PLAYER_THREATENED_PED(pedJosh)
					JOSH3_MISSION_FAILED(FAILED_JOSH_SCARED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set up mission prior to going into any mission states.
PROC JOSH3_MISSION_SETUP()
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Starting mission setup...") ENDIF #ENDIF
	failReason = FAILED_DEFAULT
	REQUEST_ADDITIONAL_TEXT("JOSH3", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_LEAD_IN].sTxtLabel = "Go to motel room"  
		mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Intro - josh_3_intp1"  
		mSkipMenu[Z_SKIP_GET_IN_CAR].sTxtLabel = "Get in car"  
		mSkipMenu[Z_SKIP_GO_TO_BBQ].sTxtLabel = "Arrived at house"   
		mSkipMenu[Z_SKIP_DROP_FUEL].sTxtLabel = "Pour fuel" 
		mSkipMenu[Z_SKIP_LEAVE_AREA].sTxtLabel = "Leave the area" 
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup = START_WIDGET_GROUP("Josh3  widgets")
			ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintToTTY)	
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	modelJoshCar = FELON2
	modelBBQ = prop_bbq_1
	vBigFire = << -1116.1232, 318.0682, 65.9778 >> // BBQ
	rBigFire = << 0,0,350 >>
	SET_PED_NON_CREATION_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
	blockScenarios = ADD_SCENARIO_BLOCKING_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
	SET_PED_PATHS_IN_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<< -1140.2233, 271.7450, 50 >>, << -1082.4034, 344.8568, 80 >>)
	CLEAR_AREA(vHouse, 100, TRUE, FALSE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL) // Ensure building state is undamaged. See B*401987.
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
	GET_MAX_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, iMaxJerryCanAmmo)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: iMaxJerryCanAmmo = ", iMaxJerryCanAmmo) ENDIF #ENDIF
	bHidden = FALSE // gas_trails.sch
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 30)
	SET_DOOR_STATE(DOORNAME_JOSH_HOTEL, DOORSTATE_LOCKED)
	ADD_PED_FOR_DIALOGUE(sTrevorConversation, 2, PLAYER_PED_ID(), "TREVOR")
	OPEN_TEST_POLY(g_polyPlayArea)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1094.6340, 292.4790, 63.0398 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1133.5137, 285.0669, 65.2301 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1139.6509, 304.8385, 66.4302 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1149.9860, 331.3831, 68.1542 >>)
	ADD_TEST_POLY_VERT(g_polyPlayArea, << -1092.2432, 334.3243, 65.9754 >>)
	CLOSE_TEST_POLY(g_polyPlayArea)
	iCounterDialogueDamageCar = 0
	iDialogueDamageCarTimer = GET_GAME_TIMER()
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Finished mission setup") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Loads the assets for the required mission stage.
PROC JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(JOSH3_MISSION_STATE current_state)
	SWITCH current_state
		CASE stateLeadIn
			REQUEST_ANIM_DICT("rcmjosh3")
			REQUEST_ANIM_DICT("rcmjosh")
		BREAK
		CASE stateGetInCar
			REQUEST_MODEL(modelJoshCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(modelJoshCar, TRUE)
		BREAK
		CASE stateGoToBBQ
			REQUEST_MODEL(modelBBQ)
		BREAK
		CASE stateDropFuel
			ASSISTED_MOVEMENT_REQUEST_ROUTE("Jo_3")
			REQUEST_PTFX_ASSET()
			REQUEST_MODEL(modelBBQ)
			REQUEST_ANIM_DICT("reaction@shellshock@unarmed")
		BREAK
	ENDSWITCH
	BOOL b_assets_loaded = FALSE
	WHILE b_assets_loaded = FALSE
		SWITCH current_state
			CASE stateLeadIn
				IF HAS_ANIM_DICT_LOADED("rcmjosh3")
				AND HAS_ANIM_DICT_LOADED("rcmjosh")
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateGetInCar
				IF HAS_MODEL_LOADED(modelJoshCar)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateGoToBBQ
				IF HAS_MODEL_LOADED(modelBBQ)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
			CASE stateDropFuel
				IF ASSISTED_MOVEMENT_IS_ROUTE_LOADED("Jo_3")
				AND HAS_PTFX_ASSET_LOADED()
				AND HAS_MODEL_LOADED(modelBBQ)
				AND HAS_ANIM_DICT_LOADED("reaction@shellshock@unarmed")
					ASSISTED_MOVEMENT_SET_ROUTE_PROPERTIES("Jo_3", EASSISTED_ROUTE_ACTIVE_WHEN_STRAFING)
					b_assets_loaded = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Loading assets...") ENDIF #ENDIF
		WAIT(0)
	ENDWHILE
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Assets loaded") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Give the fuel can weapon to the player.
PROC JOSH3_GIVE_FUEL_CAN_TO_PLAYER()
	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, iMaxJerryCanAmmo, FALSE, FALSE)
	//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN, TRUE) B*511832
ENDPROC

/// PURPOSE:
///    Ensures player has at least 30 pistol bullets.
PROC JOSH3_GIVE_PISTOL_TO_PLAYER()
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL) < 30
			SET_PED_AMMO(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 30)
		ENDIF
	ELSE
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 30)		
	ENDIF
ENDPROC

/// PURPOSE:
///    Locks both sets of gates into the grounds of Josh's house.
PROC JOSH3_LOCK_GATES(BOOL b_lock_gates = TRUE)
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1107.01, 289.38, 64.76>>, 5.0, prop_lrggate_01c_l)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01c_l, <<-1107.01, 289.38, 64.76>>, b_lock_gates, 0.0)
	ELSE
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_l, <<-1107.01, 289.38, 64.76>>, b_lock_gates, 0.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1101.62, 290.36, 64.76>>, 5.0, prop_lrggate_01c_r)	
   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01c_r, <<-1101.62, 290.36, 64.76>>, b_lock_gates, 0.0)
	ELSE
   	 	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_r, <<-1101.62, 290.36, 64.76>>, b_lock_gates, 0.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1137.05, 295.59, 67.18>>, 5.0, prop_lrggate_01c_r)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01c_r, <<-1137.05, 295.59, 67.18>>, b_lock_gates, 0.0)
	ELSE
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_r, <<-1137.05, 295.59, 67.18>>, b_lock_gates, 0.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1138.64, 300.82, 67.18>>, 5.0, prop_lrggate_01c_l)	
   	 	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_01c_l, <<-1138.64, 300.82, 67.18>>, b_lock_gates, 0.0)
	ELSE
   	 	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_lrggate_01c_l, <<-1138.64, 300.82, 67.18>>, b_lock_gates, 0.0)
	ENDIF
	IF b_lock_gates = TRUE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Locked gates") ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Unlocked gates") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a car parked outside Josh's mansion during replays only.
PROC JOSH3_CREATE_REPLAY_VEHICLE()
	IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			WAIT(0)
		ENDWHILE
		IF ENUM_TO_INT(missionStateSkip) < ENUM_TO_INT(stateGoToBBQ)
			vehForReplay = CREATE_REPLAY_CHECKPOINT_VEHICLE(vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
		ELSE
			vehForReplay = CREATE_REPLAY_CHECKPOINT_VEHICLE(vPlayerCarRespotAtHouse, fPlayerCarRespotAtHouse)
		ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Created replay vehicle") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleanup pre-mission Josh and spawn Josh for misson.
PROC JOSH3_SPAWN_JOSH_FOR_MISSION()
	JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateLeadIn)
	IF NOT IS_ENTITY_ALIVE(pedJosh)
		WHILE NOT RC_CREATE_NPC_PED(pedJosh,CHAR_JOSH, << 566.1639, -1773.8171, 29.8 >>, 14.4, "JOSH")
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Creating Josh...") ENDIF #ENDIF
			WAIT(0)
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Created Josh") ENDIF #ENDIF
		iJoshSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
		TASK_SYNCHRONIZED_SCENE(pedJosh, iJoshSynchedScene, "rcmjosh3", "sit_stairs_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
	ENDIF
	IF IS_ENTITY_ALIVE(pedJosh)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJosh, TRUE)
		relGroupFriendly = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(pedJosh, relGroupFriendly)
		SET_PED_CONFIG_FLAG(pedJosh, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CAN_BE_TARGETTED(pedJosh, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(pedJosh, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedJosh, FALSE) // B*1532473 Prevent interrupting Josh's anims if player runs into him
		TASK_LOOK_AT_ENTITY(pedJosh, PLAYER_PED_ID(), -1)
		ADD_PED_FOR_DIALOGUE(sTrevorConversation, 3, pedJosh, "JOSH")
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Set up Josh") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates Josh's car if it's not already created.
PROC JOSH3_CREATE_JOSHS_CAR_AT_MOTEL()
	IF NOT IS_ENTITY_ALIVE(vehJoshCar)
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			vehJoshCar = sRCLauncherDataLocal.vehID[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehJoshCar, TRUE, TRUE)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Josh's car already existed so grabbed it from RCLauncherDataLocal.vehID[0]") ENDIF #ENDIF
		ELSE
			JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateGetInCar)
			JOSH3_CREATE_VEHICLE(vehJoshCar, modelJoshCar, vJoshCar, fJoshCar, colourJoshCar)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Josh's car didn't already exist so created a new car") ENDIF #ENDIF
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelJoshCar)
	IF IS_ENTITY_ALIVE(vehJoshCar)
		SET_VEHICLE_COLOUR_COMBINATION(vehJoshCar, colourJoshCar)
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehJoshCar, FALSE)
		SET_VEHICLE_DISABLE_TOWING(vehJoshCar, TRUE)
		SET_VEHICLE_DOORS_LOCKED(vehJoshCar, VEHICLELOCK_LOCKED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the BBQ object model.
PROC JOSH3_CREATE_BBQ_OBJECT()
	CLEAR_AREA_OF_OBJECTS(vBBQObject, 2, CLEAROBJ_FLAG_FORCE)
	IF NOT DOES_ENTITY_EXIST(oBBQ)
		oBBQ = GET_CLOSEST_OBJECT_OF_TYPE(vBBQObject, 1.0, modelBBQ)
	ENDIF
	IF DOES_ENTITY_EXIST(oBBQ)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Grabbed BBQ object from world") #ENDIF
	ELSE
		oBBQ = CREATE_OBJECT(modelBBQ, vBBQObject)
		SET_ENTITY_HEADING(oBBQ, fBBQObject)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Couldn't grab BBQ object from world so created a new one") #ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBBQ)
ENDPROC

/// PURPOSE:
///    Checks for the player shooting the BBQ to make it explode.
PROC JOSH3_CHECK_BBQ_OBJECT_STILL_ALIVE()
	IF NOT IS_ENTITY_ALIVE(oBBQ)
	OR (IS_ENTITY_ALIVE(oBBQ) AND GET_ENTITY_HEALTH(oBBQ) <= 500)
		JOSH3_MISSION_FAILED(FAILED_DIDNT_BURN_HOUSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles debug skipping.
///  PARAMS:
///     i_new_state - The state to debug skip to.
PROC JOSH3_DEBUG_SKIP_STATE(INT i_new_state)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Starting debug skip to ", i_new_state, "...") ENDIF #ENDIF
	RC_START_Z_SKIP()
	bZSkipping = TRUE
	STOP_FIRE_IN_RANGE(vHouse, 100)
	JOSH3_MISSION_CLEANUP(TRUE)
	CLEAR_AREA(vHouse, 100, TRUE)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE)
	ENDIF
	SWITCH i_new_state
		CASE Z_SKIP_LEAD_IN
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerLeadInCP, fPlayerLeadInCP)
			ENDIF
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
			JOSH3_SPAWN_JOSH_FOR_MISSION()
			JOSH3_CREATE_JOSHS_CAR_AT_MOTEL()
			missionStateSkip = stateLeadIn
		BREAK
		CASE Z_SKIP_INTRO
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
			JOSH3_SPAWN_JOSH_FOR_MISSION()
			JOSH3_CREATE_JOSHS_CAR_AT_MOTEL()
			missionStateSkip = stateIntro
		BREAK
		CASE Z_SKIP_GET_IN_CAR
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF NOT IS_REPLAY_BEING_SET_UP()
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerGetInCarCP, fPlayerGetInCarCP)
				ENDIF
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 2500)
			ENDIF
			JOSH3_SPAWN_JOSH_FOR_MISSION()
			JOSH3_CREATE_JOSHS_CAR_AT_MOTEL()
			missionStateSkip = stateGetInCar
		BREAK
		CASE Z_SKIP_GO_TO_BBQ
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtHouse, fPlayerCarRespotAtHouse)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerGoToBBQCP, fPlayerGoToBBQCP)
			ENDIF
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
			JOSH3_LOCK_GATES()
			JOSH3_GIVE_FUEL_CAN_TO_PLAYER()
			JOSH3_GIVE_PISTOL_TO_PLAYER()
			JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateGoToBBQ)
			WAIT(1000)
			JOSH3_CREATE_BBQ_OBJECT()
			missionStateSkip = stateGoToBBQ
		BREAK
		CASE Z_SKIP_DROP_FUEL
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtHouse, fPlayerCarRespotAtHouse)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -1112.7906, 317.5085, 65.9778 >>, 86.0521)
			ENDIF
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
			JOSH3_LOCK_GATES()
			JOSH3_GIVE_FUEL_CAN_TO_PLAYER()
			JOSH3_GIVE_PISTOL_TO_PLAYER()
			JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateGoToBBQ)
			WAIT(1000)
			JOSH3_CREATE_BBQ_OBJECT()
			missionStateSkip = stateDropFuel
		BREAK
		CASE Z_SKIP_LEAVE_AREA
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtHouse, fPlayerCarRespotAtHouse)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vInsideMainGate, fInsideMainGate)
			ENDIF
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_DESTROYED)
			SET_AMBIENT_ZONE_LIST_STATE("AZL_JOSH_HOUSE_BURNING", TRUE, TRUE)
			JOSH3_LOCK_GATES(FALSE)
			REQUEST_PTFX_ASSET()
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				WAIT(0)
			ENDWHILE
			missionStateSkip = stateLeaveArea
		BREAK
		CASE Z_SKIP_MISSION_PASSED
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtHouse, fPlayerCarRespotAtHouse)
			IF NOT IS_REPLAY_BEING_SET_UP()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerMissionPassedCP, fPlayerMissionPassedCP)
			ENDIF
			missionStateSkip = stateMissionPassed
		BREAK
	ENDSWITCH
	JOSH3_CREATE_REPLAY_VEHICLE()
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	IF i_new_state = Z_SKIP_DROP_FUEL
		TRIGGER_MUSIC_EVENT("JOSH3_START")
	ELIF i_new_state = Z_SKIP_LEAVE_AREA
		TRIGGER_MUSIC_EVENT("JOSH3_RESTART1")
	ENDIF
	END_REPLAY_SETUP()
	IF i_new_state = Z_SKIP_MISSION_PASSED
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SAFE_FADE_SCREEN_IN_FROM_BLACK() // Screen needs to be fully faded back in before the mission complete GUI stuff will display
	ENDIF
	IF i_new_state = Z_SKIP_INTRO
		RC_END_Z_SKIP(TRUE, FALSE)
	ELSE
		RC_END_Z_SKIP()
	ENDIF
	missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Finished debug skip to ", i_new_state) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Updates the current objective for the mission state.
PROC JOSH3_CHANGE_CURRENT_MISSION_OBJECTIVE(STATUS_MISSION_OBJECTIVE new_objective)
	missionCurrentObjective = new_objective
	CLEAR_PRINTS()
	CLEAR_HELP()
	JOSH3_REMOVE_ALL_BLIPS()
	//INT i = 0
	IF missionState = stateDropFuel
	AND missionCurrentObjective = MISSION_OBJECTIVE_LOSE_COPS
		HIDE_GAS_TRAIL(TRUE)
	ENDIF
	SWITCH new_objective
		CASE MISSION_OBJECTIVE_NORMAL
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Changed missionCurrentObjective to MISSION_OBJECTIVE_NORMAL") ENDIF #ENDIF
			SWITCH missionState
				CASE stateGetInCar
					IF IS_ENTITY_ALIVE(vehJoshCar)
					AND NOT DOES_BLIP_EXIST(blipJoshCar)
						blipJoshCar = CREATE_VEHICLE_BLIP(vehJoshCar)
					ENDIF
				BREAK
				CASE stateGoToHouse
					IF NOT DOES_BLIP_EXIST(blipBBQ)
						blipBBQ = CREATE_COORD_BLIP(vBBQLocate)
					ENDIF
				BREAK
				CASE stateGoToBBQ
					IF NOT DOES_BLIP_EXIST(blipBBQ)
						blipBBQ = CREATE_COORD_BLIP(vBBQLocate)
					ENDIF
				BREAK
				CASE stateDropFuel
					HIDE_GAS_TRAIL(FALSE)
				BREAK
				CASE stateShootFuel
					bDisplayedShootFuelObjective = FALSE
				BREAK
				CASE stateLeaveArea
					PRINT_NOW("JOSH3_07", DEFAULT_GOD_TEXT_TIME, 1) // Leave the area.
				BREAK
			ENDSWITCH
		BREAK
		CASE MISSION_OBJECTIVE_LOSE_COPS
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Changed missionCurrentObjective to MISSION_OBJECTIVE_LOSE_COPS") ENDIF #ENDIF
			IF bToldToLoseWantedLevel = FALSE
				PRINT_NOW("JOSH3_11", DEFAULT_GOD_TEXT_TIME, 1) // Lose the cops.
				bToldToLoseWantedLevel = TRUE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles the player getting a wanted level at any point in the mission, or dropping the fuel can.
PROC JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
	SWITCH missionCurrentObjective
		CASE MISSION_OBJECTIVE_NORMAL
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				JOSH3_CHANGE_CURRENT_MISSION_OBJECTIVE(MISSION_OBJECTIVE_LOSE_COPS)
			ENDIF
		BREAK
		CASE MISSION_OBJECTIVE_LOSE_COPS
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				JOSH3_CHANGE_CURRENT_MISSION_OBJECTIVE(MISSION_OBJECTIVE_NORMAL)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Displays appropriate god text for getting in Josh's car, depending on whether the player has any fuel can ammo.
PROC JOSH3_DISPLAY_APPROPRIATE_GASOLINE_IN_CAR_TEXT()
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
		PRINT_NOW("JOSH3_13", DEFAULT_GOD_TEXT_TIME, 1) // Collect more gasoline in Josh's ~b~car.
	ELSE
		PRINT_NOW("JOSH3_01", DEFAULT_GOD_TEXT_TIME, 1) // Collect the jerry can in Josh's ~b~car.
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles checking the player abandoning Josh's car before having collected the fuel can weapon.
PROC JOSH3_CHECK_PLAYER_ISNT_ABANDONING_CAR()
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehJoshCar) > 100
		IF IS_SCREEN_FADED_IN()
			JOSH3_MISSION_FAILED(FAILED_DIDNT_COLLECT_FUEL)
		ENDIF
	ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehJoshCar) > 50
		IF bGivenCarDistanceWarning = FALSE
			JOSH3_DISPLAY_APPROPRIATE_GASOLINE_IN_CAR_TEXT()
			bGivenCarDistanceWarning = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages Josh's anim after the mocap has finished.
PROC JOSH3_MANAGE_JOSH_AFTER_MOCAP()
	IF IS_ENTITY_ALIVE(pedJosh)
	AND IS_SYNCHRONIZED_SCENE_RUNNING(iJoshSynchedScene)
	AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(iJoshSynchedScene)
	AND GET_SYNCHRONIZED_SCENE_PHASE(iJoshSynchedScene) > 0.99
		iJoshSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
		SET_SYNCHRONIZED_SCENE_LOOPED(iJoshSynchedScene, TRUE)
		TASK_SYNCHRONIZED_SCENE(pedJosh, iJoshSynchedScene, "rcmjosh3", "josh3_leadout_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
		TASK_LOOK_AT_ENTITY(pedJosh, PLAYER_PED_ID(), -1)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if a conversation can be played.
FUNC BOOL JOSH3_ALLOW_CONVERSATION_TO_PLAY(BOOL b_check_for_god_text = TRUE)
	IF bZSkipping = TRUE
		RETURN FALSE
	ENDIF
	IF b_check_for_god_text = TRUE // If FALSE the conversation is set to not display subtitles ever, so play conversation regardless of whether there is god text onscreen or not
	AND IS_MESSAGE_BEING_DISPLAYED()
	AND GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) > 0 // B*665755 Support playing conversations over god text when subtitles are turned off
		RETURN FALSE
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Plays the conversation for the lead into the mocap.
PROC JOSH3_MANAGE_LEAD_IN_CONVERSATION_AND_OBJECTIVE()
	IF bDoneLeadInConversation = FALSE
		IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
		AND IS_ENTITY_ALIVE(pedJosh)
		AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_INT_LI", CONV_PRIORITY_HIGH)
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_LOW)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_INT_LI conversation") ENDIF #ENDIF
			bDoneLeadInConversation = TRUE
		ENDIF
	ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vMotelRoom, g_vOnFootLocate, DOES_BLIP_EXIST(blipMotelRoom))
		KILL_ANY_CONVERSATION()
		missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
	ELIF iDoneLeadInConversationTimer > -1
		IF (GET_GAME_TIMER() - iDoneLeadInConversationTimer) > 2000 // Wait until the conversation has actually started
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PRINT_NOW("JOSH3_09", DEFAULT_GOD_TEXT_TIME, 1) // Go to the motel ~y~room.
			IF NOT DOES_BLIP_EXIST(blipMotelRoom)
				blipMotelRoom = CREATE_COORD_BLIP(vMotelRoom, BLIPPRIORITY_MED, FALSE)
			ENDIF
			iDoneLeadInConversationTimer = -1
			iEnterMotelRoomTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation if the player doesn't enter the motel room.
PROC JOSH3_MANAGE_MOTEL_ROOM_REMINDER_CONVERSATION()
	IF iEnterMotelRoomTimer > -1
	AND (GET_GAME_TIMER() - iEnterMotelRoomTimer) > 15000
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(pedJosh)
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedJosh) < 20.0
	AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_HANG", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_HANG conversation") ENDIF #ENDIF
		iEnterMotelRoomTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation after the intro has finished.
PROC JOSH3_MANAGE_JOSH_FUEL_CONVERSATION()
	IF bDoneJoshFuelConversation = FALSE
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY(FALSE)
	AND IS_ENTITY_ALIVE(pedJosh)
	AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_GAS", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_GAS conversation") ENDIF #ENDIF
		bDoneJoshFuelConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation when Trevor enters Josh's car.
PROC JOSH3_MANAGE_CAFEFUL_WITH_CAR_CONVERSATION()
	IF bDoneCarefulWithCarConversation = FALSE
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(vehJoshCar)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehJoshCar, TRUE)
	AND IS_ENTITY_ALIVE(pedJosh)
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedJosh) < 20
	AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_CAR", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_CAR conversation") ENDIF #ENDIF
		bDoneCarefulWithCarConversation = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue if Trevor damages Josh's car. See B*1413871.
PROC JOSH3_MANAGE_DAMAGED_JOSH_CAR_CONVERSATION()
	IF iCounterDialogueDamageCar < 3	
	AND (GET_GAME_TIMER() - iDialogueDamageCarTimer) > 5000
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(pedJosh)
	AND IS_ENTITY_ALIVE(vehJoshCar)
	AND NOT IS_ANY_SPEECH_PLAYING(pedJosh)
	AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehJoshCar)	
	AND GET_DISTANCE_BETWEEN_ENTITIES(pedJosh, vehJoshCar) < 40
	AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehJoshCar, PLAYER_PED_ID())
		PLAY_PED_AMBIENT_SPEECH(pedJosh, "WHATS_YOUR_PROBLEM", SPEECH_PARAMS_FORCE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started WHATS_YOUR_PROBLEM ambient speech") ENDIF #ENDIF
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehJoshCar)
		iDialogueDamageCarTimer = GET_GAME_TIMER()
		iCounterDialogueDamageCar++
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: iCounterDialogueDamageCar = ", iCounterDialogueDamageCar) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation if the player remains at the motel near Josh.
PROC JOSH3_MANAGE_JOSH_REMINDER_CONVERSATION()
	IF bDoneJoshReminderConversation = FALSE
		IF (GET_GAME_TIMER() - iJoshReminderTimer) > 25000
		AND JOSH3_ALLOW_CONVERSATION_TO_PLAY()
		AND IS_ENTITY_ALIVE(pedJosh)
		AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<566.53, -1770.40, 28.35>>, 10) // Fixes B*529059. This coord is a few metres in front of Josh.
		AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_REMIND", CONV_PRIORITY_HIGH)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_REMIND conversation") ENDIF #ENDIF
			bDoneJoshReminderConversation = TRUE
			bStoppedJoshReminderConversation = FALSE
		ENDIF
	ELIF bStoppedJoshReminderConversation = FALSE
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			TEXT_LABEL_23 t_conversation_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			IF ARE_STRINGS_EQUAL(t_conversation_root, "JOSH3_REMIND")
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(pedJosh)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedJosh) > 30.0
				KILL_FACE_TO_FACE_CONVERSATION()
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Stopping JOSH3_REMIND conversation because player moved away from Josh") ENDIF #ENDIF
				bStoppedJoshReminderConversation = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Play the collected fuel conversation.
PROC JOSH3_PLAY_COLLECTED_FUEL_CONVERSATION()
	IF bDoneCollectedFuelConversation = FALSE
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY(FALSE)
		IF iPlayerFuelBeforeMission = iMaxJerryCanAmmo
			IF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_FUEL3", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_FUEL3 conversation") ENDIF #ENDIF
				bDoneCollectedFuelConversation = TRUE
			ENDIF
		ELIF iPlayerFuelBeforeMission = 0
			IF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_FUEL", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_FUEL conversation") ENDIF #ENDIF
				bDoneCollectedFuelConversation = TRUE
			ENDIF
		ELIF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_FUEL2", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_FUEL2 conversation") ENDIF #ENDIF
			bDoneCollectedFuelConversation = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the fuel remaining in the player's fuel can weapon, and fails mission if necessary.
PROC JOSH3_MANAGE_REMAINING_FUEL()
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
		INT iFuelRemaining = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
		IF missionState = stateDropFuel
			IF iFuelRemaining = 0
				JOSH3_MISSION_FAILED(FAILED_USED_UP_FUEL)
			ENDIF
		ELIF iFuelRemaining < 3000
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: iFuelRemaining = ", iFuelRemaining) ENDIF #ENDIF
			JOSH3_MISSION_FAILED(FAILED_USED_TOO_MUCH_GASOLINE)
		ENDIF
	ELSE
		JOSH3_MISSION_FAILED(FAILED_USED_UP_FUEL)
	ENDIF
	IF bGivenWastingFuelWarning = FALSE
		IF missionState = stateGoToHouse
		OR missionState = stateGoToBBQ
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
			AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
				PRINT_HELP("JOSH3_08") // Do not waste gasoline. You will need it to burn down Josh's house.
				bGivenWastingFuelWarning = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation of Trevor talking to himself in the car.
PROC JOSH3_MANAGE_TREVOR_CAR_CONVERSATION()
	IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
		IF bDoneTrevorCarConversation = FALSE
			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMotelRoom, 100)
			AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_DRIVE", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_DRIVE conversation") ENDIF #ENDIF
				bDoneTrevorCarConversation = TRUE
				SAFE_RELEASE_PED(pedJosh)
				REMOVE_ANIM_DICT("rcmjosh3")
				REMOVE_ANIM_DICT("rcmjosh")
			ENDIF
		ELIF iJoshPhoneTimer = -1
			iJoshPhoneTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Josh calls Trevor on the phone, while the player is driving to Josh's mansion.
PROC JOSH3_MANAGE_JOSH_PHONE_CONVERSATION()
	IF bDoneJoshPhoneConversation = FALSE
		IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
		AND bDoneTrevorCarConversation = TRUE // Only play when Trevor has talked to himself about Josh
		AND iJoshPhoneTimer > -1 // Only play when JOSH3_DRIVE conversation has registered as finished
		AND (GET_GAME_TIMER() - iJoshPhoneTimer) > 15000
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sTrevorConversation, 3, NULL, "JOSH") // B*1373893 Add this again because pedJosh has been cleaned up by this point
			IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(sTrevorConversation, CHAR_JOSH, "JOSH3AU", "JOSH_PHONE1", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH_PHONE1 cellphone conversation") ENDIF #ENDIF
				bDoneJoshPhoneConversation = TRUE
				bSentTextMessage = FALSE
				iPlayerHungUpPhoneTimer = -1
				iAfterPhoneTimer = -1
			ENDIF
		ENDIF
	ELSE
		IF bDoneAfterPhoneConversation = FALSE
		AND JOSH3_ALLOW_CONVERSATION_TO_PLAY() // Wait until the JOSH_PHONE1 has finished
			IF iAfterPhoneTimer = -1
				iAfterPhoneTimer = GET_GAME_TIMER()
			ELIF (GET_GAME_TIMER() - iAfterPhoneTimer) > 2000
				IF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_COMM", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_COMM conversation") ENDIF #ENDIF
					bDoneAfterPhoneConversation = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF bSentTextMessage = FALSE
			IF iPlayerHungUpPhoneTimer = -1 
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player interrupted JOSH_PHONE1 cellphone conversation") ENDIF #ENDIF
					iPlayerHungUpPhoneTimer = GET_GAME_TIMER()
				ENDIF
			ELIF (GET_GAME_TIMER() - iPlayerHungUpPhoneTimer) > 5000
				IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_JOSH, "JOSH3_TXT", TXTMSG_UNLOCKED)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Sent JOSH3_TXT text message") ENDIF #ENDIF
					bSentTextMessage = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the conversation giving a hint to climb over a wall.
PROC JOSH3_MANAGE_TREVOR_WALL_CONVERSATION()
	IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 // B*1513592 Prevent dialogue if the player is wanted
		IF bDoneTrevorClimbWallConversation = FALSE
			IF IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				bDoneTrevorClimbWallConversation = TRUE // Don't play conversation JOSH3_WALL if already over the wall (likely restarted from checkpoint)
			ELIF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_WALL", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_WALL conversation") ENDIF #ENDIF
				bDoneTrevorClimbWallConversation = TRUE
			ENDIF
		ELIF bDoneTrevorOverWallConversation = FALSE
			IF bStartedMusic = TRUE
			AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_OVER", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_OVER conversation") ENDIF #ENDIF
				bDoneTrevorOverWallConversation = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles starting the music when the player is inside the boundaries of Josh's property.
PROC JOSH3_MANAGE_STARTING_MUSIC()
	IF bStartedMusic = FALSE
	AND IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		TRIGGER_MUSIC_EVENT("JOSH3_START")
		bStartedMusic = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles changing the music if the player enters a vehicle when fleeing the area.
PROC JOSH3_MANAGE_CHANGING_MUSIC_WHEN_IN_CAR()
	IF bHasPlayerGotInAnyVehicle = FALSE
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		TRIGGER_MUSIC_EVENT("JOSH3_COPS")
		bHasPlayerGotInAnyVehicle = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is pouring fuel inside the grounds of Josh's house.
FUNC BOOL JOSH3_IS_PLAYER_POURING_FUEL_IN_AREA()
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
	AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
	AND IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays the conversations when Trevor is pouring fuel.
PROC JOSH3_MANAGE_POURING_FUEL_CONVERSATIONS()
	IF iPourFuelConversationsDone < 3
	AND (GET_GAME_TIMER() - iPourFuelConversationTimer) > 10000
	AND JOSH3_ALLOW_CONVERSATION_TO_PLAY()
	AND JOSH3_IS_PLAYER_POURING_FUEL_IN_AREA()
	AND CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_POUR", CONV_PRIORITY_HIGH)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_POUR conversation ", iPourFuelConversationsDone) ENDIF #ENDIF
		iPourFuelConversationsDone++
		iPourFuelConversationTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles displaying the fuel can help text.
PROC JOSH3_MANAGE_DISPLAY_FUEL_HELP_TEXT()
	IF bDisplayedFuelHelpText = FALSE
		IF IS_PED_SHOOTING(PLAYER_PED_ID()) // B*1513587 Prevent this appearing if already pouring
		AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
			bDisplayedFuelHelpText = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player already pouring petrol so not displaying help text") ENDIF #ENDIF
		ELIF (GET_GAME_TIMER() - iDisplayFuelHelpTimer) > DEFAULT_GOD_TEXT_TIME
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player hasn't poured petrol yet so displaying help text") ENDIF #ENDIF
			PRINT_HELP_FOREVER("JOSH3_03") // Equip the Jerry Can and hold ~INPUT_ATTACK~ to pour gas.
			bDisplayedFuelHelpText = TRUE
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JOSH3_03")
		AND IS_PED_SHOOTING(PLAYER_PED_ID())
		AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles failing the mission if the player ignites the fuel trail before pouring out the entire expected length.
PROC JOSH3_MANAGE_PLAYER_SHOOTING_FUEL_TOO_EARLY()
	IF bFireBurning
	AND NOT IS_GAS_TRAIL_COMPLETE()
		JOSH3_MISSION_FAILED(FAILED_SHOT_TRAIL_TOO_EARLY)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles checking for the player pouring the fuel in one go to achieve the JO3_POUR_FUEL_IN_ONE_GO stat.
PROC JOSH3_MANAGE_POURING_FUEL_STAT()
	IF bStartedPouringFuel = FALSE
	AND IS_PED_SHOOTING(PLAYER_PED_ID())
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_PETROLCAN
		bStartedPouringFuel = TRUE
		TRIGGER_MUSIC_EVENT("JOSH3_PETROL")
	ENDIF
	IF bStartedPouringFuel = TRUE
	AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())
	AND NOT IS_GAS_TRAIL_COMPLETE()
		bDidntPourFuelInOneGo = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles switching between cameras during the outro.
PROC JOSH3_SHOW_CAMERA(INT i)
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Showing outro camera ", i) ENDIF #ENDIF
	IF NOT DOES_CAM_EXIST(camCutscene)
		camCutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	ENDIF
	IF i = JOSH3_CAMERA_EXPLODE_BBQ
	AND cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].bDisplayedCamera = FALSE
	AND cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].bDisplayedCamera = FALSE
		cameraOutro[i].vCamRotStart = << 1.0, 0.2, 150.0 >>
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Only showing camera JOSH3_CAMERA_EXPLODE_BBQ so add some interp") ENDIF #ENDIF
	ENDIF
	SET_CAM_PARAMS(camCutscene, cameraOutro[i].vCamPosStart, cameraOutro[i].vCamRotStart, cameraOutro[i].fFOV, 0, cameraOutro[i].camGraphType, cameraOutro[i].camGraphType)
	SET_CAM_PARAMS(camCutscene, cameraOutro[i].vCamPosEnd, cameraOutro[i].vCamRotEnd, cameraOutro[i].fFOV, 5000, cameraOutro[i].camGraphType, cameraOutro[i].camGraphType)
	cameraOutro[i].bDisplayedCamera = TRUE
	iLastCameraDisplayed = i
	IF bDoneCutsceneStartCommands = FALSE
		RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vInsideMainGate, fInsideMainGate) // If player is inside the grounds of Josh's house, reposition near gate to avoid showing the building swap
			ELSE
				// B*1741570 If player is outside the grounds of Josh's house, face away from house to avoid showing the building swap and explosion
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vPlayerCoords.x, vPlayerCoords.y, vHouse.x, vHouse.y)
				fAngle += 180.0
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: fAngle for Trevor's heading = ", fAngle) ENDIF #ENDIF
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fAngle)
			ENDIF
		ENDIF
		bDoneCutsceneStartCommands = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the specified outro explosion and camera shake.
/// PARAMS:
///    b_create_instantly_for_skipped_cutscene - if TRUE only create the explosion, if FALSE create the explosion and camera shake.
PROC JOSH3_DO_OUTRO_EXPLOSION(BOOL b_create_instantly_for_skipped_cutscene)
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(fxBigFire)
		fxBigFire = START_PARTICLE_FX_LOOPED_AT_COORD("scr_josh3_fires", vBigFire, rBigFire)
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_josh3_explosion", vBigFire, rBigFire)
		IF b_create_instantly_for_skipped_cutscene = FALSE // Don't create the explosion if the cutscene is being skipped, only create the fire
			IF DOES_CAM_EXIST(camCutscene)
				SHAKE_CAM(camCutscene, "SMALL_EXPLOSION_SHAKE", 0.25)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Shaking outro camera 2") ENDIF #ENDIF
			ENDIF
			PLAY_SOUND_FROM_COORD(iBBQExplosionSound, "BBQ_EXPLODE", vBigFire, "JOSH_03_SOUNDSET") // Only create the sfx if the explosion is seen during the outro, not if the cutscene has been skipped
			TRIGGER_MUSIC_EVENT("JOSH3_HOUSE_EXPLODE")
		ENDIF
	ENDIF
	explosionOutro.bDoneExplosion = TRUE
	#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Done outro explosion") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Returns TRUE if there is a fire within x distance of the explosion's nearest gas trail point.
FUNC BOOL JOSH3_IS_FIRE_NEAR_POS(VECTOR v_pos_to_check, FLOAT f_distance)
	VECTOR v_closest_fire_pos
	IF GET_CLOSEST_FIRE_POS(v_closest_fire_pos, v_pos_to_check)
	AND GET_DISTANCE_BETWEEN_COORDS(v_closest_fire_pos, v_pos_to_check) < f_distance
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles checking for ignited fuel being within the trigger bounds for creating outro explosions.
/// PARAMS:
///    b_create_instantly_for_skipped_cutscene - if TRUE only create the explosion, if FALSE create the explosion and camera shake.
PROC JOSH3_MANAGE_OUTRO_EXPLOSION(BOOL b_create_instantly_for_skipped_cutscene)
	IF explosionOutro.bDoneExplosion = FALSE
		IF b_create_instantly_for_skipped_cutscene = TRUE
			JOSH3_DO_OUTRO_EXPLOSION(b_create_instantly_for_skipped_cutscene)
		ELSE
			IF explosionOutro.iExplosionTimer = -1
				IF JOSH3_IS_FIRE_NEAR_POS(vBigFire, 1)
					explosionOutro.iExplosionTimer = GET_GAME_TIMER()
				ENDIF
			ELIF (GET_GAME_TIMER() - explosionOutro.iExplosionTimer) > 500 // B*437565 - short delay before the explosion occurs
				JOSH3_DO_OUTRO_EXPLOSION(b_create_instantly_for_skipped_cutscene)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player has any weapon that fires a projectile.
FUNC BOOL JOSH3_HAS_PLAYER_GOT_SUITABLE_WEAPON()
	WEAPON_TYPE weaponBest = GET_BEST_PED_WEAPON(PLAYER_PED_ID())
	WEAPON_GROUP weaponGroup = GET_WEAPONTYPE_GROUP(weaponBest)
	IF weaponGroup = WEAPONGROUP_PISTOL
	OR weaponGroup = WEAPONGROUP_SMG
	OR weaponGroup = WEAPONGROUP_RIFLE
	OR weaponGroup = WEAPONGROUP_MG
	OR weaponGroup = WEAPONGROUP_SHOTGUN
	OR weaponGroup = WEAPONGROUP_SNIPER
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), weaponBest) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles checking if the player has a projectile weapon or needs to go find one.
PROC JOSH3_MANAGE_PLAYER_WEAPON_CHECKING()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 // Losing the cops is more important
		SWITCH statusShootFuel
			CASE STATUS_SHOOT_FUEL_HAS_WEAPON
				IF bDisplayedShootFuelObjective = FALSE
					PRINT_NOW("JOSH3_05", DEFAULT_GOD_TEXT_TIME, 1) // Shoot the gasoline trail to ignite it.
					bDisplayedShootFuelObjective = TRUE
				ENDIF
				IF NOT JOSH3_HAS_PLAYER_GOT_SUITABLE_WEAPON()
					statusShootFuel = STATUS_SHOOT_FUEL_NO_WEAPON
					bDisplayedShootFuelObjective = FALSE
				ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vHouse, 40)
					statusShootFuel = STATUS_SHOOT_FUEL_FAR_AWAY
					bDisplayedShootFuelObjective = FALSE
				ENDIF
			BREAK
			CASE STATUS_SHOOT_FUEL_NO_WEAPON
				IF bDisplayedShootFuelObjective = FALSE
					PRINT_NOW("JOSH3_10", DEFAULT_GOD_TEXT_TIME, 1) // Find a gun so you can ignite the gasoline trail.
					bDisplayedShootFuelObjective = TRUE
				ENDIF
				IF JOSH3_HAS_PLAYER_GOT_SUITABLE_WEAPON()
					statusShootFuel = STATUS_SHOOT_FUEL_HAS_WEAPON
					bDisplayedShootFuelObjective = FALSE
				ENDIF
			BREAK
			CASE STATUS_SHOOT_FUEL_FAR_AWAY
				IF bDisplayedShootFuelObjective = FALSE
					PRINT_NOW("JOSH3_02", DEFAULT_GOD_TEXT_TIME, 1) // Go to ~y~Josh's house.
					IF NOT DOES_BLIP_EXIST(blipHouse)
						blipHouse = CREATE_COORD_BLIP(vHouse)
					ENDIF
					bDisplayedShootFuelObjective = TRUE
				ENDIF
				IF NOT JOSH3_HAS_PLAYER_GOT_SUITABLE_WEAPON()
					SAFE_REMOVE_BLIP(blipHouse)
					statusShootFuel = STATUS_SHOOT_FUEL_NO_WEAPON
					bDisplayedShootFuelObjective = FALSE
				ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vHouse, 40)
					SAFE_REMOVE_BLIP(blipHouse)
					statusShootFuel = STATUS_SHOOT_FUEL_HAS_WEAPON
					bDisplayedShootFuelObjective = FALSE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the closest point of the gas trails to the house car and BBQ, for checking when a fire is nearby.
PROC JOSH3_GET_NEAREST_TRAIL_POINT(INT i_camera)
	VECTOR v_camera_trigger_pos
	IF i_camera = JOSH3_CAMERA_FRONT_PATHWAY
		v_camera_trigger_pos = <<-1116.75818, 298.43961, 64.96789>>
		cameraOutro[i_camera].fTriggerRange = 8
	ELIF i_camera = JOSH3_CAMERA_REAR_PATHWAY
		v_camera_trigger_pos = <<-1128.22070, 317.22104, 65.17768>>
		cameraOutro[i_camera].fTriggerRange = 2
	ELSE // JOSH3_CAMERA_EXPLODE_BBQ
		v_camera_trigger_pos = vBigFire
		cameraOutro[i_camera].fTriggerRange = 4
	ENDIF
	FLOAT f_closest_distance = 9999
	FLOAT f_current_distance
	INT i_gas
	REPEAT COUNT_OF(gasTrail) i_gas
		f_current_distance = GET_DISTANCE_BETWEEN_COORDS(v_camera_trigger_pos, gasTrail[i_gas].coord)
		IF f_current_distance < f_closest_distance
			cameraOutro[i_camera].vNearestTrailPoint = gasTrail[i_gas].coord
			f_closest_distance = f_current_distance
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Plays the conversation about leaving the area.
PROC JOSH3_MANAGE_TREVOR_COPS_CONVERSATION()
	IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
		IF bDoneTrevorCopsConversation = FALSE
			IF CREATE_CONVERSATION(sTrevorConversation, "JOSH3AU", "JOSH3_FIRE", CONV_PRIORITY_HIGH)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_FIRE conversation") ENDIF #ENDIF
				bDoneTrevorCopsConversation = TRUE
			ENDIF
		ELIF bDisplayedLeaveAreaObjective = FALSE
			PRINT_NOW("JOSH3_07", DEFAULT_GOD_TEXT_TIME, 1) // Leave the area.
			bDisplayedLeaveAreaObjective = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles setting the player on fire if he gets near to one of the script fires.
PROC JOSH3_MANAGE_SETTING_PLAYER_ON_FIRE()
	IF NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBigFire, 2)
		#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player is close to a ptfx fire, so set him on fire") ENDIF #ENDIF
		START_ENTITY_FIRE(PLAYER_PED_ID())
	ENDIF
ENDPROC

/// PURPOSE:
///    Main state for handling Trevor going to the hotel room.
PROC MS_LEAD_IN
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_LEAD_IN" #ENDIF)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				pedJosh = sRCLauncherDataLocal.pedID[0]
				SET_ENTITY_AS_MISSION_ENTITY(pedJosh, TRUE, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Grabbed pedJosh from sRCLauncherDataLocal.pedID[0]") ENDIF #ENDIF
			ENDIF
			JOSH3_SPAWN_JOSH_FOR_MISSION()
			JOSH3_CREATE_JOSHS_CAR_AT_MOTEL()
			iJoshSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
			IF IS_ENTITY_ALIVE(pedJosh)
				TASK_SYNCHRONIZED_SCENE(pedJosh, iJoshSynchedScene, "rcmjosh3", "josh3_lead_in", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				TASK_LOOK_AT_ENTITY(pedJosh, PLAYER_PED_ID(), -1)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJosh)
			ENDIF
			IF IS_SCREEN_FADED_OUT()
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerLeadInCP, fPlayerLeadInCP)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // B*1437967 Ensure screen faded in when replaying mission from menu
			ENDIF
			REQUEST_CUTSCENE("josh_3_intp1")
			bDoneLeadInConversation = FALSE
			iDoneLeadInConversationTimer = GET_GAME_TIMER()
			iEnterMotelRoomTimer = -1
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMotelRoom) > 100
				IF IS_SCREEN_FADED_IN()
					JOSH3_MISSION_FAILED(FAILED_DIDNT_GO_TO_MOTEL_ROOM)
				ENDIF
			ELSE
				JOSH3_MANAGE_LEAD_IN_CONVERSATION_AND_OBJECTIVE()
				JOSH3_MANAGE_MOTEL_ROOM_REMINDER_CONVERSATION()
				JOSH3_MANAGE_DAMAGED_JOSH_CAR_CONVERSATION()
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJoshSynchedScene)
				AND NOT IS_SYNCHRONIZED_SCENE_LOOPED(iJoshSynchedScene)
				AND GET_SYNCHRONIZED_SCENE_PHASE(iJoshSynchedScene) > 0.99
				AND IS_ENTITY_ALIVE(pedJosh)
					iJoshSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
					SET_SYNCHRONIZED_SCENE_LOOPED(iJoshSynchedScene, TRUE)
					TASK_SYNCHRONIZED_SCENE(pedJosh, iJoshSynchedScene, "rcmjosh3", "josh3_curb_wait_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
					TASK_LOOK_AT_ENTITY(pedJosh, PLAYER_PED_ID(), -1)
				ENDIF
				IF NOT IS_ENTITY_ALIVE(vehJoshCar)
					JOSH3_MISSION_FAILED(FAILED_CAR_WRECKED)
				ENDIF
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_LEAD_IN" #ENDIF)
			SAFE_REMOVE_BLIP(blipMotelRoom)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the cutscene involving Trevor going to the hotel room.
PROC MS_WALK_IN_MOTEL_CUTSCENE()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_WALK_IN_MOTEL_CUTSCENE" #ENDIF)
			SET_DOOR_STATE(DOORNAME_JOSH_HOTEL, DOORSTATE_UNLOCKED)
			iTrevorSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
			cameraAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
			PLAY_SYNCHRONIZED_CAM_ANIM(cameraAnim, iTrevorSynchedScene, "TREVOR_ENTER_ROOM_camera", "rcmjosh")
			PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<549.26, -1773.11, 33.73>>, 10, prop_motel_door_09, iTrevorSynchedScene, "TREVOR_ENTER_ROOM_door", "rcmjosh", INSTANT_BLEND_IN)
			RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOWEST)
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iTrevorSynchedScene, "rcmjosh", "TREVOR_ENTER_ROOM_Trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			iCutsceneStage = 0
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			SWITCH iCutsceneStage
				CASE 0
					IF (IS_SYNCHRONIZED_SCENE_RUNNING(iTrevorSynchedScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iTrevorSynchedScene) > 0.99)
					OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
		            IF REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
						sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<578.9, -1761.4, 31.3>>, <<27.6, 2.5, 11.0>>, 5000)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<578.8, -1761.1, 31.4>>, <<27.6, 2.5, 11.0>>, 5000)
						SET_CAM_FOV(sTimelapse.splineCamera, 50)
						SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
						PLAY_SOUND_FRONTEND(-1, "TIME_LAPSE_MASTER")						
						SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE, FALSE)
						sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
						todReference = GET_CURRENT_TIMEOFDAY()
						ADD_TIME_TO_TIMEOFDAY(todReference, 0, 0, CONST_NUM_HOURS_SKIP_FOR_MOTEL_TIMELAPSE, 0)	// B*2173568 - ensure iSkipToHour is clamped to a valid hour
						iSkipToHour = GET_TIMEOFDAY_HOUR(todReference)
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: GET_HOUR_TO_SKIP_TO_IN_MOTEL_TIMELAPSE return ", iSkipToHour) ENDIF #ENDIF						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vMotelRoom) // B*1553642 - Prevent Trevor falling through the world
						iCutsceneStage++
		            ENDIF
				BREAK
				CASE 2
					IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iSkipToHour, 0, "EXTRASUNNY", "cirrocumulus", sTimelapse)
					OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
						IF IS_AUDIO_SCENE_ACTIVE("TOD_SHIFT_SCENE")
							STOP_AUDIO_SCENE("TOD_SHIFT_SCENE")
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Stopped audio scene TOD_SHIFT_SCENE") ENDIF #ENDIF
						ENDIF
						missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_WALK_IN_MOTEL_CUTSCENE" #ENDIF)
			RELEASE_AMBIENT_AUDIO_BANK()
			SET_DOOR_STATE(DOORNAME_JOSH_HOTEL, DOORSTATE_LOCKED)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the intro cutscene.
PROC MS_INTRO()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_REQUEST_CUTSCENE("josh_3_intp1")
			iCutsceneStage = 0
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			SWITCH iCutsceneStage
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						IF IS_ENTITY_ALIVE(pedJosh)
							REGISTER_ENTITY_FOR_CUTSCENE(pedJosh, "Josh", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOSH))
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: CU_ANIMATE_EXISTING_SCRIPT_ENTITY Josh") ENDIF #ENDIF
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(pedJosh, "Josh", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_JOSH))
						ENDIF
						RC_CLEANUP_LAUNCHER()
						START_CUTSCENE()
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						RENDER_SCRIPT_CAMS(FALSE, FALSE) // Required to stop the walk in door scripted cam
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<567.447693,-1761.026245,27.169291>>, <<565.640137,-1774.562744,31.354511>>, 5.000000, vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerCarRespotAtStart, fPlayerCarRespotAtStart)
						RC_START_CUTSCENE_MODE(<<565.39, -1772.88, 29.77>>)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF NOT DOES_ENTITY_EXIST(pedJosh)
						AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Josh"))
							pedJosh = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Josh"))
							SET_ENTITY_AS_MISSION_ENTITY(pedJosh, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Grabbed pedJosh from Josh") ENDIF #ENDIF
							JOSH3_SPAWN_JOSH_FOR_MISSION()
						ENDIF		
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor", GET_PLAYER_PED_MODEL(CHAR_TREVOR))
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerGetInCarCP, fPlayerGetInCarCP)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 2500)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						ENDIF
					ELSE
						RC_END_CUTSCENE_MODE()
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: IS_CUTSCENE_PLAYING returned false") ENDIF #ENDIF
						missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			REPLAY_STOP_EVENT()
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_INTRO" #ENDIF)
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the player getting into Josh's car.
PROC MS_GET_IN_CAR()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GET_IN_CAR" #ENDIF)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_GET_IN_CAR, "Get in car")
			iPlayerFuelBeforeMission = 0
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
				iPlayerFuelBeforeMission = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: iPlayerFuelBeforeMission = ", iPlayerFuelBeforeMission) ENDIF #ENDIF
			IF iPlayerFuelBeforeMission = iMaxJerryCanAmmo
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player already has maximum jerry can ammo") ENDIF #ENDIF
				bDoneJoshFuelConversation = TRUE
			ELSE
				JOSH3_DISPLAY_APPROPRIATE_GASOLINE_IN_CAR_TEXT()
				IF NOT DOES_BLIP_EXIST(blipJoshCar)
				AND IS_ENTITY_ALIVE(vehJoshCar)
					blipJoshCar = CREATE_VEHICLE_BLIP(vehJoshCar)
				ENDIF
				bDoneJoshFuelConversation = FALSE
			ENDIF
			IF IS_ENTITY_ALIVE(pedJosh)
				iJoshSynchedScene = CREATE_SYNCHRONIZED_SCENE(vJoshSynchedScenePos, vJoshSynchedSceneRot)
				TASK_SYNCHRONIZED_SCENE(pedJosh, iJoshSynchedScene, "rcmjosh3", "josh3_leadout_action", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE|SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
				TASK_LOOK_AT_ENTITY(pedJosh, PLAYER_PED_ID(), -1)
			ENDIF
			IF IS_ENTITY_ALIVE(vehJoshCar)
				SET_VEHICLE_DOORS_LOCKED(vehJoshCar, VEHICLELOCK_UNLOCKED)
			ENDIF
						
			bGivenCarDistanceWarning = FALSE
			bGivenWastingFuelWarning = FALSE
			bDoneJoshReminderConversation = FALSE
			bDoneCarefulWithCarConversation = FALSE
			iJoshReminderTimer = GET_GAME_TIMER()
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			JOSH3_MANAGE_JOSH_AFTER_MOCAP()
			JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
			IF IS_ENTITY_ALIVE(vehJoshCar)
				JOSH3_CHECK_PLAYER_ISNT_ABANDONING_CAR()
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					JOSH3_MANAGE_CAFEFUL_WITH_CAR_CONVERSATION()
					JOSH3_MANAGE_JOSH_FUEL_CONVERSATION()
					JOSH3_MANAGE_JOSH_REMINDER_CONVERSATION()
					JOSH3_MANAGE_DAMAGED_JOSH_CAR_CONVERSATION()
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehJoshCar)
						JOSH3_GIVE_PISTOL_TO_PLAYER()
						JOSH3_GIVE_FUEL_CAN_TO_PLAYER()
						missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
					ELIF iPlayerFuelBeforeMission = iMaxJerryCanAmmo // Player already has max ammo so continue
						JOSH3_GIVE_PISTOL_TO_PLAYER()
						missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
					ENDIF
				ENDIF
			ELSE
				JOSH3_MISSION_FAILED(FAILED_CAR_WRECKED)
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GET_IN_CAR" #ENDIF)
			SAFE_REMOVE_BLIP(blipJoshCar)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going to Josh's house.
PROC MS_GO_TO_HOUSE()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_TO_HOUSE" #ENDIF)
			PRINT_NOW("JOSH3_02", DEFAULT_GOD_TEXT_TIME, 1) // Go to ~y~Josh's house.
			IF NOT DOES_BLIP_EXIST(blipBBQ)
				blipBBQ = CREATE_COORD_BLIP(vBBQLocate)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
			
			bDoneCollectedFuelConversation = FALSE
			bDoneTrevorCarConversation = FALSE
			bDoneJoshPhoneConversation = FALSE
			bDoneAfterPhoneConversation = FALSE
			iJoshReminderTimer = GET_GAME_TIMER()
			iJoshPhoneTimer = -1
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			JOSH3_MANAGE_JOSH_AFTER_MOCAP()
			JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
			JOSH3_MANAGE_REMAINING_FUEL()
			IF missionCurrentObjective = MISSION_OBJECTIVE_NORMAL
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1120.502563,341.249268,62.281723>>, <<-1117.929688,267.019928,74.506630>>, 80.000000)
					missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
				ELSE
					JOSH3_MANAGE_CAFEFUL_WITH_CAR_CONVERSATION()
					JOSH3_PLAY_COLLECTED_FUEL_CONVERSATION()
					JOSH3_MANAGE_TREVOR_CAR_CONVERSATION()
					JOSH3_MANAGE_JOSH_REMINDER_CONVERSATION()
					JOSH3_MANAGE_JOSH_PHONE_CONVERSATION()
				ENDIF
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_TO_HOUSE" #ENDIF)
			REMOVE_ANIM_DICT("rcmjosh3")
			REMOVE_ANIM_DICT("rcmjosh")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling going inside the grounds of Josh's house to the BBQ.
PROC MS_GO_TO_BBQ()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_GO_TO_BBQ" #ENDIF)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_ARRIVE_AT_HOUSE, "Arrived at house", TRUE)
			JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateGoToBBQ)
			JOSH3_LOCK_GATES()
			JOSH3_CREATE_BBQ_OBJECT()
			IF NOT DOES_BLIP_EXIST(blipBBQ) // If playing from a replay we'll need to create the blip and display the god text again
				blipBBQ = CREATE_COORD_BLIP(vBBQLocate)
				PRINT_NOW("JOSH3_02", DEFAULT_GOD_TEXT_TIME, 1) // Go to ~y~Josh's house.
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 7.0, REPLAY_IMPORTANCE_LOWEST)
			
			bDoneTrevorClimbWallConversation = TRUE // B*1515411 Not required anymore apparently
			bDoneTrevorOverWallConversation = FALSE
			bStartedMusic = FALSE
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
			JOSH3_MANAGE_REMAINING_FUEL()
			JOSH3_MANAGE_STARTING_MUSIC()
			JOSH3_MANAGE_TREVOR_WALL_CONVERSATION()
			JOSH3_CHECK_BBQ_OBJECT_STILL_ALIVE()
			IF missionCurrentObjective = MISSION_OBJECTIVE_NORMAL
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBBQLocate, g_vOnFootLocate, TRUE)
				OR JOSH3_IS_PLAYER_POURING_FUEL_IN_AREA()
					missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
				ENDIF
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_GO_TO_BBQ" #ENDIF)
			SAFE_REMOVE_BLIP(blipBBQ)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling pouring the expected length of fuel trail.
PROC MS_DROP_FUEL()
	SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME() // B*1075616
	REQUEST_ADDITIONAL_COLLISION_AT_COORD(vHouse) // B*2153231 Ensure collision is loaded at the house if the player leaves the area so the petrol decals don't clean up
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_DROP_FUEL" #ENDIF)
			JOSH3_LOAD_ASSETS_FOR_MISSION_STATE(stateDropFuel)
			gasTrail[0].coord = <<-1116.0607, 318.2036, 65.9778>>
			gasTrail[1].coord = <<-1117.0483, 318.3977, 65.9778>>
			gasTrail[2].coord = <<-1118.0848, 318.5940, 65.9778>>
			gasTrail[3].coord = <<-1119.1498, 318.7873, 65.9778>>
			gasTrail[4].coord = <<-1120.1605, 318.8961, 65.9778>>
			gasTrail[5].coord = <<-1121.1591, 319.0799, 65.9778>>
			gasTrail[6].coord = <<-1122.1490, 319.2221, 65.9778>>
			gasTrail[7].coord = <<-1123.2157, 319.3653, 65.9778>>
			gasTrail[8].coord = <<-1124.2849, 319.5186, 65.9778>>
			gasTrail[9].coord = <<-1125.3802, 319.5970, 65.9778>>
			gasTrail[10].coord = <<-1126.3851, 319.1108, 65.9778>>
			gasTrail[11].coord = <<-1126.9430, 318.2621, 65.7698>>
			gasTrail[12].coord = <<-1127.5199, 317.5066, 65.1777>>
			gasTrail[13].coord = <<-1128.1208, 316.6581, 65.1777>>
			gasTrail[14].coord = <<-1129.0302, 316.0728, 65.1777>>
			gasTrail[15].coord = <<-1129.7354, 315.2601, 65.1777>>
			gasTrail[16].coord = <<-1129.8583, 314.1669, 65.1777>>
			gasTrail[17].coord = <<-1129.6305, 313.1406, 65.1777>>
			gasTrail[18].coord = <<-1129.7095, 312.1320, 65.1779>>
			gasTrail[19].coord = <<-1129.7778, 311.0894, 65.1779>>
			gasTrail[20].coord = <<-1129.7253, 310.0732, 65.1779>>
			gasTrail[21].coord = <<-1129.6354, 309.0656, 65.1779>>
			gasTrail[22].coord = <<-1129.4738, 308.0490, 65.1779>>
			gasTrail[23].coord = <<-1129.3799, 307.0058, 65.1779>>
			gasTrail[24].coord = <<-1129.3761, 305.9893, 65.1779>>
			gasTrail[25].coord = <<-1129.2896, 304.9550, 65.2038>>
			gasTrail[26].coord = <<-1129.1382, 303.9323, 65.2272>>
			gasTrail[27].coord = <<-1128.9680, 302.8818, 65.2272>>
			gasTrail[28].coord = <<-1128.6027, 301.8776, 65.2292>>
			gasTrail[29].coord = <<-1127.8608, 301.1598, 65.2275>>
			gasTrail[30].coord = <<-1127.0892, 300.4057, 65.2230>>
			gasTrail[31].coord = <<-1126.0264, 300.0129, 65.1661>>
			gasTrail[32].coord = <<-1124.9788, 299.7498, 65.1114>>
			gasTrail[33].coord = <<-1123.9432, 299.6741, 65.0805>>
			gasTrail[34].coord = <<-1122.9220, 299.5366, 65.0539>>
			gasTrail[35].coord = <<-1121.8743, 299.3183, 65.0430>>
			gasTrail[36].coord = <<-1120.8480, 299.0170, 65.0175>>
			gasTrail[37].coord = <<-1119.7845, 298.8040, 65.0047>>
			gasTrail[38].coord = <<-1118.7419, 298.5806, 65.0024>>
			gasTrail[39].coord = <<-1117.7535, 298.3252, 65.0007>>
			gasTrail[40].coord = <<-1116.7667, 298.1601, 64.9674>>
			gasTrail[41].coord = <<-1115.7808, 297.9280, 64.9265>>
			gasTrail[42].coord = <<-1114.7854, 297.6190, 64.8757>>
			gasTrail[43].coord = <<-1113.8207, 297.3486, 64.8166>>
			gasTrail[44].coord = <<-1112.8834, 296.9707, 64.7542>>
			gasTrail[45].coord = <<-1111.9723, 296.5717, 64.6463>>
			gasTrail[46].coord = <<-1111.0547, 296.1595, 64.5324>>
			gasTrail[47].coord = <<-1110.1647, 295.7042, 64.3585>>
			RESET_GAS_TRAIL()
						
			PRINT_NOW("JOSH3_04", DEFAULT_GOD_TEXT_TIME, 1) // Pour a ~y~gasoline trail.
			iDisplayFuelHelpTimer = GET_GAME_TIMER()
			iPourFuelConversationTimer = GET_GAME_TIMER()
			iPourFuelConversationsDone = 0
			bDisplayedFuelHelpText = FALSE
			bStartedPouringFuel = FALSE
			bDidntPourFuelInOneGo = FALSE
			SET_WANTED_LEVEL_MULTIPLIER(0)
			SET_CREATE_RANDOM_COPS(FALSE)
			DISABLE_TAXI_HAILING(TRUE) // B*2153231 Stop being able to hail a taxi at this point in the mission
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			SET_RADAR_AS_INTERIOR_THIS_FRAME()
			JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
			JOSH3_MANAGE_DISPLAY_FUEL_HELP_TEXT()
			JOSH3_MANAGE_REMAINING_FUEL()
			JOSH3_MANAGE_PLAYER_SHOOTING_FUEL_TOO_EARLY()
			JOSH3_MANAGE_POURING_FUEL_STAT()
			JOSH3_MANAGE_POURING_FUEL_CONVERSATIONS()
			JOSH3_CHECK_BBQ_OBJECT_STILL_ALIVE()
			CONTROL_GAS_TRAILS(#IF IS_DEBUG_BUILD widgetGroup #ENDIF)
			IF missionCurrentObjective = MISSION_OBJECTIVE_NORMAL
			AND IS_GAS_TRAIL_COMPLETE()
				IF bDidntPourFuelInOneGo = FALSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JO3_POUR_FUEL_IN_ONE_GO)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Achieved stat: JO3_POUR_FUEL_IN_ONE_GO") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Didn't achieve stat: JO3_POUR_FUEL_IN_ONE_GO") ENDIF #ENDIF
				ENDIF
				
				
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 2.0, REPLAY_IMPORTANCE_LOW)
				
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_DROP_FUEL" #ENDIF)
			IF ASSISTED_MOVEMENT_IS_ROUTE_LOADED("Jo_3")
				ASSISTED_MOVEMENT_REMOVE_ROUTE("Jo_3")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling igniting the fuel trail.
PROC MS_SHOOT_FUEL()
	SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
	REQUEST_ADDITIONAL_COLLISION_AT_COORD(vHouse) // B*2153231 Ensure collision is loaded at the house if the player leaves the area so the petrol decals don't clean up
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_SHOOT_FUEL" #ENDIF)
			IF JOSH3_HAS_PLAYER_GOT_SUITABLE_WEAPON()
				statusShootFuel = STATUS_SHOOT_FUEL_HAS_WEAPON
			ELSE
				statusShootFuel = STATUS_SHOOT_FUEL_NO_WEAPON
			ENDIF
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInsideMainGate, <<6,6,LOCATE_SIZE_HEIGHT>>, FALSE)
				bDisplayedShootFuelObjective = TRUE
				PRINT_NOW("JOSH3_06", DEFAULT_GOD_TEXT_TIME, 1) // Go to the ~y~main gate.
				IF NOT DOES_BLIP_EXIST(blipGate)
					blipGate = CREATE_COORD_BLIP(vInsideMainGate)
				ENDIF
			ELSE
				bDisplayedShootFuelObjective = FALSE
			ENDIF
			PREPARE_MUSIC_EVENT("JOSH3_HOUSE_EXPLODE")
			LOAD_STREAM("JOSH_03_HOUSE_EXPLOSION_MASTER") // Preload the stream
			streamVolumeJosh3 = STREAMVOL_CREATE_SPHERE(vInsideMainGate, 50.0, FLAG_MAPDATA) // B*1122092 Ensure all props are loaded in
			DISABLE_TAXI_HAILING(TRUE) // B*2153231 Stop being able to hail a taxi at this point in the mission
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			CONTROL_GAS_TRAILS(#IF IS_DEBUG_BUILD widgetGroup #ENDIF)
			IF bFireBurning
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				AND LOAD_STREAM("JOSH_03_HOUSE_EXPLOSION_MASTER")
					REPLAY_START_EVENT()
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: REPLAY_START_EVENT for outro") ENDIF #ENDIF
					bStartedReplayEvent = TRUE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CLEAR_PRINTS()
					CLEAR_HELP()
					HIDE_HELP_TEXT_THIS_FRAME() // See B*887268
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player shot fuel") ENDIF #ENDIF
					PLAY_STREAM_FRONTEND() // GET_GAS_TRAIL_IGNITE_POS()
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_LOWEST)
					
					missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipGate)
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInsideMainGate, <<6,6,LOCATE_SIZE_HEIGHT>>, TRUE)
					SAFE_REMOVE_BLIP(blipGate)
					bDisplayedShootFuelObjective = FALSE
				ENDIF
				JOSH3_MANAGE_CURRENT_MISSION_OBJECTIVE()
				JOSH3_MANAGE_PLAYER_WEAPON_CHECKING()
				JOSH3_CHECK_BBQ_OBJECT_STILL_ALIVE()
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_SHOOT_FUEL" #ENDIF)
			SAFE_REMOVE_BLIP(blipGate)
			SAFE_REMOVE_BLIP(blipHouse)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the outro cutscene.
PROC MS_OUTRO()
	SET_DISABLE_PETROL_DECALS_RECYCLING_THIS_FRAME()
	INT i
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_OUTRO" #ENDIF)
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].vCamPosStart = << -1113.1, 326.0, 67.1 >>
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].vCamRotStart = << 1.0, 0.2, 158.7 >>
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].vCamPosEnd = << -1113.1, 326.0, 67.1 >>
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].vCamRotEnd = << 1.0, 0.2, 158.7 >>
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].fFOV = 30.0
			cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].camGraphType = GRAPH_TYPE_LINEAR
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].vCamPosStart = << -1132.196777,298.688141,65.896484 >>
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].vCamRotStart = << 0.093347,0.300000,-71.295753 >>
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].vCamPosEnd = << -1132.196777,298.688141,65.896484 >>
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].vCamRotEnd = << 2.026602,0.300006,-21.158085 >>
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].fFOV = 30.0
			cameraOutro[JOSH3_CAMERA_FRONT_PATHWAY].camGraphType = GRAPH_TYPE_ACCEL
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].vCamPosStart = << -1126.456177,324.424194,69.346909 >>
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].vCamRotStart = << -25.112604,0.626431,177.440063 >>
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].vCamPosEnd = << -1126.456177,324.424194,69.346909 >>
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].vCamRotEnd = << -25.112606,0.626431,-161.700546 >>
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].fFOV = 30.0
			cameraOutro[JOSH3_CAMERA_REAR_PATHWAY].camGraphType = GRAPH_TYPE_LINEAR
			bDoneCutsceneStartCommands = FALSE
			i = 0
			REPEAT NUM_OUTRO_CAMERAS i
				cameraOutro[i].bDisplayedCamera = FALSE
				JOSH3_GET_NEAREST_TRAIL_POINT(i)
			ENDREPEAT
			explosionOutro.bDoneExplosion = FALSE
			explosionOutro.iExplosionTimer = -1
			bCutsceneSkipped = FALSE
			iLastCameraDisplayed = -1
			iEndCutsceneTimer = -1
			iCameraChangeTimer = GET_GAME_TIMER()
			bDone1stPersonCameraFlash = FALSE
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			HIDE_HELP_TEXT_THIS_FRAME() // See B*887268
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				bCutsceneSkipped = TRUE
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ENDIF
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_ENTITY_ON_FIRE(PLAYER_PED_ID()) // B*1085880 Check for the player being an idiot setting himself on fire
				AND bDoneCutsceneStartCommands = FALSE // B*2108148 Only do this if the cutscene hasn't yet started
					IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) > 0
						#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player set himself on fire, setting health to 0") ENDIF #ENDIF
						SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
					ENDIF
				ELSE
					i = 0
					REPEAT NUM_OUTRO_CAMERAS i
						IF cameraOutro[i].bDisplayedCamera = FALSE
						AND i > iLastCameraDisplayed // Don't go backwards along the fuel trail (towards the main gate)
						AND (GET_GAME_TIMER() - iCameraChangeTimer) > 500 // Initial delay so the first camera doesn't trigger the instant the trail ignites
						AND NOT IS_VECTOR_ZERO(cameraOutro[i].vNearestTrailPoint)
						AND JOSH3_IS_FIRE_NEAR_POS(cameraOutro[i].vNearestTrailPoint, cameraOutro[i].fTriggerRange)
							JOSH3_SHOW_CAMERA(i)
						ENDIF
					ENDREPEAT
					IF cameraOutro[JOSH3_CAMERA_EXPLODE_BBQ].bDisplayedCamera = TRUE
						IF iEndCutsceneTimer = -1
							iEndCutsceneTimer = GET_GAME_TIMER()
						ELIF (GET_GAME_TIMER() - iEndCutsceneTimer) > 1500
							missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
						ENDIF
						IF bDone1stPersonCameraFlash = FALSE
						AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
						AND (GET_GAME_TIMER() - iEndCutsceneTimer) > 1200
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bDone1stPersonCameraFlash = TRUE
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "JOSH3: Doing 1st person camera flash") #ENDIF
						ENDIF
					ENDIF
					JOSH3_MANAGE_OUTRO_EXPLOSION(FALSE)
				ENDIF
			ENDIF
			CONTROL_GAS_TRAILS(#IF IS_DEBUG_BUILD widgetGroup #ENDIF)
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_OUTRO" #ENDIF)
			IF bCutsceneSkipped = TRUE
				SAFE_FADE_SCREEN_OUT_TO_BLACK()
				// See B*1720154 - only stop the sounds if skipping, otherwise just let them play out to completion
				STOP_SOUND(iBBQExplosionSound)
				STOP_STREAM()
			ENDIF
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 3.0, REPLAY_IMPORTANCE_LOW)
			CLEANUP_GAS_TRAILS()
			JOSH3_CLEANUP_BIG_FIRE()
			STOP_FIRE_IN_RANGE(vHouse, 100)
			CLEAR_AREA_OF_PROJECTILES(vHouse, 100)
			REMOVE_DECALS_IN_RANGE(vHouse, 100) // B*1484401
			SET_AMBIENT_ZONE_LIST_STATE("AZL_JOSH_HOUSE_BURNING", TRUE, TRUE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE, 0)
			DESTROY_ALL_CAMS()
			JOSH3_MANAGE_OUTRO_EXPLOSION(TRUE)
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHouse)-180) // B*1243392 Rotate the camera so it's facing away from the house
				ELSE
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				ENDIF
			ENDIF
			PLAY_SOUND_FROM_COORD(iHouseFireSound, "HOUSE_FIRE", vHouse, "JOSH_03_SOUNDSET")
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_DESTROYED)
			TRIGGER_MUSIC_EVENT("JOSH3_HOUSE_EXPLODE")
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				TASK_PLAY_ANIM(PLAYER_PED_ID(), "reaction@shellshock@unarmed", "back_long", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_TAG_SYNC_OUT, 0.2) // B*1393823
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Done explosion reaction anim on player") ENDIF #ENDIF
			ENDIF
			REMOVE_ANIM_DICT("reaction@shellshock@unarmed")
			IF NOT IS_GAMEPLAY_CAM_SHAKING()
				SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.15)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Shaking gameplay camera after cutscene") ENDIF #ENDIF
			ENDIF
			WAIT(100)
			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
			RC_END_CUTSCENE_MODE(TRUE, FALSE, TRUE, FALSE)
        	ANIMPOSTFX_PLAY("ExplosionJosh3", 0, FALSE) // B*1525376
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_josh3_light_explosion", vLightFromFire, <<0,0,0>>) // B*1394068
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_josh3_exp_debris", vLightFromFire, <<0,0,0>>)
			IF STREAMVOL_IS_VALID(streamVolumeJosh3)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Deleting streamVolumeJosh3") ENDIF #ENDIF
				STREAMVOL_DELETE(streamVolumeJosh3)
			ENDIF
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND GET_ENTITY_HEALTH(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) <= 0
			AND IS_POINT_IN_POLY_2D(g_polyPlayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Player sat in a destroyed vehicle so killing him") ENDIF #ENDIF
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling leaving the vicinity of Josh's house before the cops arrive.
PROC MS_LEAVE_AREA()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_LEAVE_AREA" #ENDIF)
			bDoneTrevorCopsConversation = FALSE
			bDisplayedLeaveAreaObjective = FALSE
			bHasPlayerGotInAnyVehicle = FALSE
			JOSH3_LOCK_GATES(FALSE)
			iLeaveAreaTimer = GET_GAME_TIMER()
			iRestoreControlTimer = GET_GAME_TIMER()
			SET_WANTED_LEVEL_MULTIPLIER(1)
			SET_CREATE_RANDOM_COPS(TRUE)
			DISABLE_TAXI_HAILING(FALSE) // B*2153231 Allow being able to hail a taxi at this point in the mission
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			IF (GET_GAME_TIMER() - iRestoreControlTimer) > 3000 // B*1513603 Ensure player can't immediately swing camera around to see the building swap occur
			AND NOT IS_PLAYER_SCRIPT_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Restored player control") ENDIF #ENDIF
				IF bStartedReplayEvent = TRUE
					bStartedReplayEvent = FALSE
					REPLAY_STOP_EVENT()
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: REPLAY_STOP_EVENT for outro") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JOSH_3_01", 0.0) // B*1551716
				SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Didn't achieve stat: JO3_ESCAPE_WITHOUT_ALERTING_COPS") ENDIF #ENDIF
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vHouse, 100)
				SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Completing mission without increasing wanted level") ENDIF #ENDIF
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JO3_ESCAPE_WITHOUT_ALERTING_COPS)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Achieved stat: JO3_ESCAPE_WITHOUT_ALERTING_COPS") ENDIF #ENDIF
				missionStateSkip = statePhoneCall
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ELSE
				JOSH3_MANAGE_TREVOR_COPS_CONVERSATION()
				JOSH3_MANAGE_SETTING_PLAYER_ON_FIRE()
				JOSH3_MANAGE_CHANGING_MUSIC_WHEN_IN_CAR()
				IF iLeaveAreaTimer > -1
				AND (GET_GAME_TIMER() - iLeaveAreaTimer) > 20000
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Increasing wanted level") ENDIF #ENDIF
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					iLeaveAreaTimer = -1
				ENDIF
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_LEAVE_AREA" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling losing player's wanted level.
PROC MS_LOSE_COPS()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_LOSE_COPS" #ENDIF)
			PRINT_NOW("JOSH3_11", DEFAULT_GOD_TEXT_TIME, 1) // Lose your wanted level.
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			JOSH3_MANAGE_CHANGING_MUSIC_WHEN_IN_CAR()
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vHouse, 100)
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
				JOSH3_MANAGE_SETTING_PLAYER_ON_FIRE()
			ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TRIGGER_MUSIC_EVENT("JOSH3_COPS_LOST_RADIO")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				ELSE
					TRIGGER_MUSIC_EVENT("JOSH3_COPS_LOST")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_LOSE_COPS" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main state for handling the final phone call.
PROC MS_PHONE_CALL()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_PHONE_CALL" #ENDIF)
			SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL) // Player is 100m+ away from the house so we're ok to turn off the fires
			SET_AMBIENT_ZONE_LIST_STATE("AZL_JOSH_HOUSE_BURNING", FALSE, FALSE)
			ADD_PED_FOR_DIALOGUE(sTrevorConversation, 3, NULL, "JOSH") // B*1553753 Add this again because pedJosh has been cleaned up by this point
			bDoneFinalPhoneCallConversation = FALSE
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			IF bDoneFinalPhoneCallConversation = FALSE
				IF JOSH3_ALLOW_CONVERSATION_TO_PLAY()
				AND PLAYER_CALL_CHAR_CELLPHONE(sTrevorConversation, CHAR_JOSH, "JOSH3AU", "JOSH3_CALL", CONV_PRIORITY_HIGH)
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Started JOSH3_CALL cellphone conversation") ENDIF #ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_LOW)
					
					bDoneFinalPhoneCallConversation = TRUE
				ENDIF
			ELIF JOSH3_ALLOW_CONVERSATION_TO_PLAY() // Phone call has finished
				missionStateMachine = JOSH3_STATE_MACHINE_CLEANUP
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_CLEANUP
			JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_PHONE_CALL" #ENDIF)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Starts the mission failed sequence.
PROC MS_FAILED()
	SWITCH missionStateMachine
		CASE JOSH3_STATE_MACHINE_SETUP
			JOSH3_STATE_SETUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
			CLEAR_PRINTS()
			CLEAR_HELP()
			JOSH3_REMOVE_ALL_BLIPS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			TRIGGER_MUSIC_EVENT("JOSH3_MISSION_FAIL")
			JOSH3_SET_PED_TO_FLEE()		
			SWITCH failReason
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_DEFAULT") ENDIF #ENDIF
				BREAK
				CASE FAILED_CAR_WRECKED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_CAR_WRECKED") ENDIF #ENDIF
					failString = "JOSH3_F1" // ~s~Josh's car was wrecked.
				BREAK
				CASE FAILED_USED_UP_FUEL
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_USED_UP_FUEL") ENDIF #ENDIF
					failString = "JOSH3_F2" // ~s~You ran out of gasoline.
				BREAK
				CASE FAILED_DIDNT_COLLECT_FUEL
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_DIDNT_COLLECT_FUEL") ENDIF #ENDIF
					failString = "JOSH3_F3" // ~s~You didn't collect the gasoline.
				BREAK
				CASE FAILED_USED_TOO_MUCH_GASOLINE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_USED_TOO_MUCH_GASOLINE") ENDIF #ENDIF
					failString = "JOSH3_F5" // ~s~You don't have enough gasoline remaining.
				BREAK
				CASE FAILED_SHOT_TRAIL_TOO_EARLY
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_SHOT_TRAIL_TOO_EARLY") ENDIF #ENDIF
					failString = "JOSH3_F6" // ~s~You shot the gasoline trail before fully completing it.
				BREAK
				CASE FAILED_JOSH_DIED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_JOSH_DIED") ENDIF #ENDIF
					failString = "JOSH3_F7" // ~s~Josh died.
				BREAK
				CASE FAILED_JOSH_INJURED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_JOSH_INJURED") ENDIF #ENDIF
					failString = "JOSH3_F8" // ~s~Josh was injured.
				BREAK
				CASE FAILED_JOSH_SCARED
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_JOSH_SCARED") ENDIF #ENDIF
					failString = "JOSH3_F9" // ~s~Josh was scared away.
				BREAK
				CASE FAILED_DIDNT_GO_TO_MOTEL_ROOM
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_DIDNT_GO_TO_MOTEL_ROOM") ENDIF #ENDIF
					failString = "JOSH3_F4" // ~s~Trevor didn't go to the motel room.
				BREAK
				CASE FAILED_DIDNT_BURN_HOUSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: MISSION_FAILED reason=FAILED_DIDNT_BURN_HOUSE") ENDIF #ENDIF
					failString = "JOSH3_F10" // ~s~Trevor didn't burn down Josh's house.
				BREAK
			ENDSWITCH
			IF failReason = FAILED_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(failString)
			ENDIF
		BREAK
		CASE JOSH3_STATE_MACHINE_LOOP
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				JOSH3_STATE_CLEANUP(#IF IS_DEBUG_BUILD "MS_FAILED" #ENDIF)
				JOSH3_REMOVE_ALL_VEHICLES(TRUE)
				JOSH3_REMOVE_ALL_PEDS(TRUE)
				JOSH3_CLEANUP_BIG_FIRE()
				SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE, BUILDINGSTATE_NORMAL) // Ensure building state is undamaged	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_JOSHHOUSE_FIRE, BUILDINGSTATE_NORMAL)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Cleaned up MS_FAILED") ENDIF #ENDIF
				Script_Cleanup()
			ELSE
				// not finished fading out: handle dialogue etc here
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF bZSkipping = FALSE
		AND (GET_GAME_TIMER() - iDebugSkipTime) > 1000
		AND ENUM_TO_INT(missionState) < (ENUM_TO_INT(NUM_MISSION_STATES)-1) // Don't check during stateFailed
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: S key pressed so passing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				Script_Passed()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: F key pressed so failing mission") ENDIF #ENDIF
				IF missionState = stateIntro
					WAIT_FOR_CUTSCENE_TO_STOP()
				ENDIF
				JOSH3_MISSION_FAILED()
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: J key pressed so doing z skip forwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateLeadIn
					CASE stateWalkInMotelCutscene
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_INTRO)
					BREAK
					CASE stateIntro
						IF IS_CUTSCENE_ACTIVE()
						     STOP_CUTSCENE()
						ENDIF
					BREAK
					CASE stateGetInCar
					CASE stateGoToHouse
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_GO_TO_BBQ)
					BREAK
					CASE stateGoToBBQ
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_DROP_FUEL)
					BREAK
					CASE stateDropFuel
					CASE stateShootFuel
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_LEAVE_AREA)
					BREAK
					CASE stateLeaveArea
					CASE stateLoseCops
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
					BREAK
				ENDSWITCH
			ENDIF
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: P key pressed so doing z skip backwards") ENDIF #ENDIF
				SWITCH missionState
					CASE stateIntro
						WAIT_FOR_CUTSCENE_TO_STOP()
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_LEAD_IN)
					BREAK
					CASE stateGetInCar
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_LEAD_IN)
					BREAK
					CASE stateGoToHouse
					CASE stateGoToBBQ
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_GET_IN_CAR)
					BREAK
					CASE stateDropFuel
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_GO_TO_BBQ)
					BREAK
					CASE stateShootFuel
					CASE stateLeaveArea
					CASE stateLoseCops
						JOSH3_DEBUG_SKIP_STATE(Z_SKIP_DROP_FUEL)
					BREAK
				ENDSWITCH
			ENDIF
			INT i_new_state
			IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
				#IF IS_DEBUG_BUILD IF bDebug_PrintToTTY CPRINTLN(DEBUG_MISSION, "JOSH3: Z skip menu used so doing z skip") ENDIF #ENDIF
				JOSH3_DEBUG_SKIP_STATE(i_new_state)
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherDataLocal.iSyncSceneIndex)
		TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.iSyncSceneIndex) // B*1508526
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_JOSH, TREVOR_BOOK, FALSE)
	
	JOSH3_MISSION_SETUP()
	
	IF IS_REPLAY_IN_PROGRESS()
		RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE, TRUE)
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_LEAD_IN
				START_REPLAY_SETUP(vPlayerLeadInCP, fPlayerLeadInCP)
				JOSH3_DEBUG_SKIP_STATE(Z_SKIP_LEAD_IN)
			BREAK
			CASE CP_GET_IN_CAR
				START_REPLAY_SETUP(vPlayerGetInCarCP, fPlayerGetInCarCP)
				JOSH3_DEBUG_SKIP_STATE(Z_SKIP_GET_IN_CAR)
			BREAK
			CASE CP_ARRIVE_AT_HOUSE
				START_REPLAY_SETUP(vPlayerGoToBBQCP, fPlayerGoToBBQCP)
				JOSH3_DEBUG_SKIP_STATE(Z_SKIP_GO_TO_BBQ)
			BREAK
			CASE CP_MISSION_PASSED
				START_REPLAY_SETUP(vPlayerMissionPassedCP, fPlayerMissionPassedCP)
				JOSH3_DEBUG_SKIP_STATE(Z_SKIP_MISSION_PASSED)
			BREAK
		ENDSWITCH
	ENDIF
		
	WHILE(TRUE)
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_SE")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		JOSH3_DEATH_CHECKS()
		SWITCH missionState
			CASE stateLeadIn
				MS_LEAD_IN()
			BREAK
			CASE stateWalkInMotelCutscene
				MS_WALK_IN_MOTEL_CUTSCENE()
			BREAK
			CASE stateIntro
				MS_INTRO()
			BREAK
			CASE stateGetInCar
				MS_GET_IN_CAR()
			BREAK
			CASE stateGoToHouse
				MS_GO_TO_HOUSE()
			BREAK
			CASE stateGoToBBQ
				MS_GO_TO_BBQ()
			BREAK
			CASE stateDropFuel
				MS_DROP_FUEL()
			BREAK
			CASE stateShootFuel
				MS_SHOOT_FUEL()
			BREAK
			CASE stateOutro
				MS_OUTRO()
			BREAK
			CASE stateLeaveArea
				MS_LEAVE_AREA()
			BREAK
			CASE stateLoseCops
				MS_LOSE_COPS()
			BREAK
			CASE statePhoneCall
				MS_PHONE_CALL()
			BREAK
			CASE stateMissionPassed
				Script_Passed()
			BREAK
			CASE stateFailed
				MS_FAILED()
			BREAK
		ENDSWITCH
		#IF IS_DEBUG_BUILD		
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
ENDSCRIPT
