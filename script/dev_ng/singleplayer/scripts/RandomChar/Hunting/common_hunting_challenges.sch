USING "common_hunting.sch"



FUNC INT GET_HUNTING_CHALLENGE_SPEED_RANK_TARGET(HUNTING_CHALLENGE_RANK hcRank)
	SWITCH hcRank 
		CASE HCR_RANK_4	RETURN 240000 BREAK
		CASE HCR_RANK_5	RETURN 500 BREAK
	ENDSWITCH
	RETURN 1000
ENDFUNC

FUNC STRING GET_TEXT_MESSAGE_FOR_CHALLENGE_UNLOCK(HUNTING_CHALLENGE thisChallenge)
	SWITCH thisChallenge
		CASE HC_PHOTOJOURNALIST 	RETURN "HTXT_PHOTO" BREAK
		CASE HC_MASTER_HUNTER 		RETURN "HTXT_HUNTER" BREAK
		CASE HC_WEAPONS_MASTER		RETURN "HTXT_WEAPON" BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC


#IF IS_DEBUG_BUILD
USING "flow_structs_core.sch"
USING "flow_info_structs_core.sch"
USING "flow_info_utils_core.sch"

FUNC STRING GET_HUNTING_CHALLENGE_DISPLAY_NAME(HUNTING_CHALLENGE thisChallenge)
	SWITCH thisChallenge
		CASE HC_PHOTOJOURNALIST		RETURN "Photo" BREAK
		CASE HC_MASTER_HUNTER		RETURN "Hunt" BREAK
		CASE HC_WEAPONS_MASTER		RETURN "Weap" BREAK
	ENDSWITCH
	RETURN "Unknown Challenge"
ENDFUNC


PROC DISPLAY_HUNTING_SCREEN_TITLES()
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_TITLE_FLOW_XPOS, 0.04, "AMBIENT CHALLENGES")
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_TITLE_LAUNCH_XPOS, 0.04, "CHALLENGE TRACKING")
ENDPROC

PROC DISPLAY_HUNTING_CHALLENGE(HUNTING_CHALLENGE thisChallenge, FLOAT drawY)
	// Display Strand Name in appropriate colour
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_BRIGHT, FLOW_INFO_COMMAND_ARRAY_POS_XPOS, drawY, GET_HUNTING_CHALLENGE_DISPLAY_NAME(thisChallenge))
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_DESCRIPTION_XPOS, drawY, "number", ENUM_TO_INT(Player_Hunt_Data.challengeData[thisChallenge].hcStatus))
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_PARAM1_XPOS, drawY, "number", ENUM_TO_INT(Player_Hunt_Data.challengeData[thisChallenge].hcRank))
ENDPROC



PROC DISPLAY_HUNTING_CHALLENGE_DETAILS()
	FLOAT theY = FLOW_INFO_START_YPOS
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_COMMAND_ARRAY_POS_XPOS, theY, "Chall")
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_DESCRIPTION_XPOS, theY, "Status")
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_PARAM1_XPOS, theY, "Rank")

	theY += FLOW_INFO_ADD_Y
	
	HUNTING_CHALLENGE challengeIndex = HC_MASTER_HUNTER
	
	REPEAT COUNT_OF(HUNTING_CHALLENGE) challengeIndex
		
		DISPLAY_HUNTING_CHALLENGE(challengeIndex, theY)
		
		theY += FLOW_INFO_ADD_Y
	ENDREPEAT
ENDPROC

PROC DISPLAY_CHALLENGE_TRACKING_DETAILS()
	FLOAT theY = FLOW_INFO_START_YPOS
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "SpookStreak")
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", Player_Hunt_Data.iKillsSinceSpooked)
	
	theY += FLOW_INFO_ADD_Y

	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "HeartStreak")
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", Player_Hunt_Data.iKillStreakHeartshot)
	
	theY += FLOW_INFO_ADD_Y
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "CallStreak")
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", Player_Hunt_Data.iKillsSinceElkCall)
	
	theY += FLOW_INFO_ADD_Y
	INT iElkTimeIndex
	REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) iElkTimeIndex
		DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "KillTime")
		DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", Player_Hunt_Data.iKillTimes[iElkTimeIndex])
		theY += FLOW_INFO_ADD_Y
	ENDREPEAT
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "CurTime")
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", GET_GAME_TIMER())
	theY += FLOW_INFO_ADD_Y
	DISPLAY_FLOW_INFO_LITERAL_TEXT(COL_MID, FLOW_INFO_HELP_QUEUE_TITLE_XPOS, theY, "CutoffTime")
	DISPLAY_FLOW_INFO_TEXTLABEL_WITH_ONE_NUMBER(COL_BRIGHT, FLOW_INFO_HELP_QUEUE_START_XPOS, theY, "number", GET_GAME_TIMER() - GET_HUNTING_CHALLENGE_SPEED_RANK_TARGET(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank))
	theY += FLOW_INFO_ADD_Y
	
ENDPROC


PROC UPDATE_CHALLENGE_DEBUG()
	DISPLAY_BACKGROUND()
	DISPLAY_HUNTING_SCREEN_TITLES()
	DISPLAY_HUNTING_CHALLENGE_DETAILS()
	DISPLAY_CHALLENGE_TRACKING_DETAILS()
ENDPROC
#ENDIF

FUNC INT GET_HUNTING_CHALLENGE_RANK_TARGET(HUNTING_CHALLENGE hcIndex, HUNTING_CHALLENGE_RANK hcRank)
	SWITCH hcIndex
		CASE HC_MASTER_HUNTER
			SWITCH hcRank 
				CASE HCR_RANK_1	RETURN 1 BREAK
				CASE HCR_RANK_2	RETURN 2 BREAK
				CASE HCR_RANK_3	RETURN 3 BREAK
				CASE HCR_RANK_4	RETURN 4 BREAK
				CASE HCR_RANK_5	RETURN 2 BREAK
			ENDSWITCH
		BREAK
		CASE HC_PHOTOJOURNALIST
			SWITCH hcRank 
				CASE HCR_RANK_1	RETURN 1 BREAK
				CASE HCR_RANK_2	RETURN 1 BREAK
				CASE HCR_RANK_3	RETURN 1 BREAK
				CASE HCR_RANK_4	RETURN 1 BREAK
				CASE HCR_RANK_5	RETURN 1 BREAK
			ENDSWITCH
		BREAK
		CASE HC_WEAPONS_MASTER
			SWITCH hcRank 
				CASE HCR_RANK_1	RETURN 1 BREAK
				CASE HCR_RANK_2	RETURN 1 BREAK
				CASE HCR_RANK_3	RETURN 1 BREAK
				CASE HCR_RANK_4	RETURN 1 BREAK
				CASE HCR_RANK_5	RETURN 1 BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Adds all challenge progress to the brief!
PROC SETUP_CHALLENGE_BRIEFS()
	CPRINTLN(DEBUG_HUNTING, "-- SETUP_CHALLENGE_BRIEFS --")
	CLEAR_BRIEF()
	
	BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS("AHT_RTIT")
	END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
	
	TEXT_LABEL_15 challengeDesc			
	IF Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank != HCR_LOCKED
		challengeDesc = "HUNTT_HUNTER_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
		
		challengeDesc = "HUNTC_HUNTER_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
		
		CPRINTLN(DEBUG_HUNTING, "SETUP_CHALLENGE_BRIEFS - Added brief HUNTER - ", ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank) + 1)
		IF (Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank != HCR_COMPLETE)
			PLAY_SOUND_FRONTEND(-1, "CHALLENGE_UNLOCKED", "HUD_AWARDS")
		ENDIF
	ENDIF
	
	IF Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank != HCR_LOCKED
		challengeDesc = "HUNTT_WEAPON_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
		
		challengeDesc = "HUNTC_WEAPON_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)

		CPRINTLN(DEBUG_HUNTING, "SETUP_CHALLENGE_BRIEFS - Added brief WEAPONS MASTER - ", ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank) + 1)
		IF (Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank != HCR_COMPLETE)
			PLAY_SOUND_FRONTEND(-1, "CHALLENGE_UNLOCKED", "HUD_AWARDS")
		ENDIF
	ENDIF

	IF Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank != HCR_LOCKED
		challengeDesc = "HUNTT_PHOTO_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
		
		challengeDesc = "HUNTC_PHOTO_"
		challengeDesc += ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank) + 1
		BEGIN_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(challengeDesc)
		END_TEXT_COMMAND_ADD_DIRECTLY_TO_PREVIOUS_BRIEFS(FALSE)
		
		CPRINTLN(DEBUG_HUNTING, "SETUP_CHALLENGE_BRIEFS - Added brief PHOTOJOURNALIST - ", ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank) + 1)
		IF (Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank != HCR_COMPLETE)
			PLAY_SOUND_FRONTEND(-1, "CHALLENGE_UNLOCKED", "HUD_AWARDS")
		ENDIF
	ENDIF
ENDPROC
		
		
PROC UPDATE_HUNTING_CHALLENGE_MASTER_HUNTER(HT_PLAYER_DATA &huntingData, HUNTING_CHALLENGE hcIndex)
	INT iElkTimeIndex, iTimeGoal
	
	SWITCH huntingData.challengeData[hcIndex].hcStatus
		CASE HCS_RANK_INIT
			// Print Objective
			SWITCH huntingData.challengeData[hcIndex].hcRank
				CASE HCR_RANK_1
					huntingData.iKillStreakHeartshot = 0
				BREAK
				CASE HCR_RANK_2
					huntingData.iKillsSinceElkCall = 0
				BREAK
				CASE HCR_RANK_3
					huntingData.iKillsSinceSpooked = 0
				BREAK
				CASE HCR_RANK_4
					REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) iElkTimeIndex
						Player_Hunt_Data.iKillTimes[iElkTimeIndex] = 0
					ENDREPEAT
				BREAK
				CASE HCR_RANK_5
					REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) iElkTimeIndex
						Player_Hunt_Data.iKillTimes[iElkTimeIndex] = 0
					ENDREPEAT
				BREAK
			ENDSWITCH
			Player_Hunt_Data.bChallengeScreenSetup = FALSE
			huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_UPDATE
		BREAK
		CASE HCS_RANK_UPDATE
			SWITCH huntingData.challengeData[hcIndex].hcRank
				CASE HCR_RANK_1
					IF huntingData.iKillStreakHeartshot > huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
					ELIF huntingData.iKillStreakHeartshot < huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						CHALLENGE_NOTIFICATION("HUNTNOT_HUNTER_1", huntingData.iKillStreakHeartshot, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
					ENDIF
					huntingData.challengeData[hcIndex].iPrevProgress = huntingData.iKillStreakHeartshot
					IF huntingData.iKillStreakHeartshot >= GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
						huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_COMPLETE_TOAST
					ENDIF
				BREAK
				CASE HCR_RANK_2
					IF huntingData.iKillsSinceElkCall > huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						IF huntingData.iKillsSinceElkCall < GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
							CHALLENGE_NOTIFICATION("HUNTNOT_HUNTER_2", huntingData.iKillsSinceElkCall, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
						ENDIF
					ELIF huntingData.iKillsSinceElkCall < huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						CHALLENGE_NOTIFICATION("HUNTNOT_HUNTER_2", huntingData.iKillsSinceElkCall, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
					ENDIF
					huntingData.challengeData[hcIndex].iPrevProgress = huntingData.iKillsSinceElkCall
					IF huntingData.iKillsSinceElkCall >= GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
						huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_COMPLETE_TOAST
					ENDIF
				BREAK
				CASE HCR_RANK_3
					IF huntingData.iKillsSinceSpooked > huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						IF huntingData.iKillsSinceSpooked < GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
							CHALLENGE_NOTIFICATION("HUNTNOT_HUNTER_3", huntingData.iKillsSinceSpooked, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
						ENDIF
					ELIF huntingData.iKillsSinceSpooked < huntingData.challengeData[hcIndex].iPrevProgress
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						CHALLENGE_NOTIFICATION("HUNTNOT_HUNTER_3", huntingData.iKillsSinceSpooked, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
					ENDIF
					huntingData.challengeData[hcIndex].iPrevProgress = huntingData.iKillsSinceSpooked
					IF huntingData.iKillsSinceSpooked >= GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
						huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_COMPLETE_TOAST
					ENDIF
				BREAK
				CASE HCR_RANK_4
					huntingData.iKillsTimed = 0
					Player_Hunt_Data.iKillsTimeRemaining = 0
					Player_Hunt_Data.iChallengeDisplayTime = GET_GAME_TIMER() + CHALLENGE_DISPLAY_DELAY
					
					iTimeGoal = GET_HUNTING_CHALLENGE_SPEED_RANK_TARGET(huntingData.challengeData[hcIndex].hcRank)
					REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) iElkTimeIndex
						IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] != 0
							IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] + iTimeGoal >= GET_GAME_TIMER()
								huntingData.iKillsTimed++
								Player_Hunt_Data.iChallengeDisplayTime = GET_GAME_TIMER() + CHALLENGE_DISPLAY_DELAY
								
								IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] < Player_Hunt_Data.iKillsTimeRemaining OR Player_Hunt_Data.iKillsTimeRemaining = 0
									Player_Hunt_Data.iKillsTimeRemaining = Player_Hunt_Data.iKillTimes[iElkTimeIndex]
								ENDIF
							ELSE
								Player_Hunt_Data.iKillTimes[iElkTimeIndex] = 0 // Clear out old kills
							ENDIF
						ENDIF
					ENDREPEAT

					IF huntingData.iKillsTimed != huntingData.challengeData[hcIndex].iPrevProgress
						TEXT_LABEL_31 challengeNot
						challengeNot = "HUNTNOT_HUNTER_"
						challengeNot += ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1
						Player_Hunt_Data.bChallengeScreenSetup = FALSE
						IF huntingData.iKillsTimed < GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
							CHALLENGE_NOTIFICATION(challengeNot, huntingData.iKillsTimed, GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank))
						ENDIF
					ENDIF
					huntingData.challengeData[hcIndex].iPrevProgress = huntingData.iKillsTimed
					IF huntingData.iKillsTimed >= GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
						huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_COMPLETE_TOAST
					ENDIF
				BREAK
				CASE HCR_RANK_5
					huntingData.iKillsTimed = 0
					Player_Hunt_Data.iKillsTimeRemaining = 0
					
					iTimeGoal = GET_HUNTING_CHALLENGE_SPEED_RANK_TARGET(huntingData.challengeData[hcIndex].hcRank)
					REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) iElkTimeIndex
						IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] != 0
							IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] + iTimeGoal >= GET_GAME_TIMER()
								huntingData.iKillsTimed++
								Player_Hunt_Data.iChallengeDisplayTime = GET_GAME_TIMER() + CHALLENGE_DISPLAY_DELAY
								
								IF Player_Hunt_Data.iKillTimes[iElkTimeIndex] < Player_Hunt_Data.iKillsTimeRemaining OR Player_Hunt_Data.iKillsTimeRemaining = 0
									Player_Hunt_Data.iKillsTimeRemaining = Player_Hunt_Data.iKillTimes[iElkTimeIndex]
								ENDIF
							ELSE
								Player_Hunt_Data.iKillTimes[iElkTimeIndex] = 0 // Clear out old kills
							ENDIF
						ENDIF
					ENDREPEAT

					huntingData.challengeData[hcIndex].iPrevProgress = huntingData.iKillsTimed
					IF huntingData.iKillsTimed >= GET_HUNTING_CHALLENGE_RANK_TARGET(hcIndex, huntingData.challengeData[hcIndex].hcRank)
						huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_COMPLETE_TOAST
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE HCS_RANK_COMPLETE_TOAST
			// Toast
			// Slight delay needs to pass before display.
			IF (huntingData.iChallengeDisplayTime != -1)
				IF (huntingData.iChallengeDisplayTime > GET_GAME_TIMER())
					EXIT
				ELSE
					// We've hit here, which means it's time to display. Wipe our time, and continue on.
					huntingData.iChallengeDisplayTime = -1
				ENDIF
			ENDIF
			
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI) AND NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE()// AND Player_Hunt_Data.bCanDisplayChallengeToast
				TEXT_LABEL_15 toastMsg
				toastMsg = "HUNTUI_RANKUP"
				IF huntingData.challengeData[hcIndex].hcRank = HCR_RANK_5
					toastMsg = "HUNTUI_COMPLETE"
				ENDIF
//				SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_HUNTER_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000)
				SET_SHARD_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_HUNTER_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_NEXT
			ENDIF

		BREAK
		CASE HCS_RANK_NEXT
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI)
				// Reward
				DO_CHALLENGE_RANK_SCORING(huntingData.challengeData[hcIndex].hcRank)
				
				// Master hunter challenges all involve shooting something. Give shooting skill for that.
				INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_SHOOTING_ABILITY, 2)
				
				// In addition to this, master hunter 3 is a stealth challenge. Gvie some stealth for that.
				IF (huntingData.challengeData[hcIndex].hcRank = HCR_RANK_3)
					INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_STEALTH_ABILITY, 1)
				ENDIF

				huntingData.challengeData[hcIndex].hcRank += INT_TO_ENUM(HUNTING_CHALLENGE_RANK, 1)
				
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_INIT
				huntingData.challengeData[hcIndex].iPrevProgress = 0
				SETUP_CHALLENGE_BRIEFS()
				//huntingData.iKillStreakHeartshot = 0

				// Reward
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_HUNTING_CHALLENGE_PHOTOJOURNALIST(HT_PLAYER_DATA &huntingData, HUNTING_CHALLENGE hcIndex)

	SWITCH huntingData.challengeData[hcIndex].hcStatus
		CASE HCS_RANK_INIT
			// Print Objective
			huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_UPDATE
			Player_Hunt_Data.bChallengeScreenSetup = FALSE
		BREAK
		CASE HCS_RANK_UPDATE
			// We don't do any updates here!
		BREAK
		CASE HCS_RANK_COMPLETE_TOAST
			// Toast
			// Slight delay needs to pass before display.
			IF (huntingData.iChallengeDisplayTime != -1)
				IF (huntingData.iChallengeDisplayTime > GET_GAME_TIMER())
					EXIT
				ELSE
					// We've hit here, which means it's time to display. Wipe our time, and continue on.
					huntingData.iChallengeDisplayTime = -1
				ENDIF
			ENDIF
			
			// Toast
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI) AND NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE()
				TEXT_LABEL_15 toastMsg
				toastMsg = "HUNTUI_RANKUP"
				IF huntingData.challengeData[hcIndex].hcRank = HCR_RANK_5
					toastMsg = "HUNTUI_COMPLETE"
				ENDIF
//				SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_PHOTO_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000)
				SET_SHARD_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_PHOTO_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_NEXT
			ENDIF

		BREAK
		CASE HCS_RANK_NEXT
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI)
				// Reward
				DO_CHALLENGE_RANK_SCORING(huntingData.challengeData[hcIndex].hcRank)
				
				// Photo journalist 1 is a picture of a live, grazing doe. Give some stealth for that.
				IF (huntingData.challengeData[hcIndex].hcRank = HCR_RANK_1)
					INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_STEALTH_ABILITY, 2)
				ENDIF

				huntingData.challengeData[hcIndex].hcRank += INT_TO_ENUM(HUNTING_CHALLENGE_RANK, 1)
				
				SETUP_CHALLENGE_BRIEFS()
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_INIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



PROC UPDATE_HUNTING_CHALLENGE_WEAPONS_MASTER(HT_PLAYER_DATA &huntingData, HUNTING_CHALLENGE hcIndex)
	SWITCH huntingData.challengeData[hcIndex].hcStatus
		CASE HCS_RANK_INIT
			// Print Objective
			huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_UPDATE
			Player_Hunt_Data.bChallengeScreenSetup = FALSE
		BREAK
		CASE HCS_RANK_UPDATE
			// We don't do any updates here!
		BREAK
		CASE HCS_RANK_COMPLETE_TOAST
			// Toast
			// Slight delay needs to pass before display.
			IF (huntingData.iChallengeDisplayTime != -1)
				IF (huntingData.iChallengeDisplayTime > GET_GAME_TIMER())
					EXIT
				ELSE
					// We've hit here, which means it's time to display. Wipe our time, and continue on.
					huntingData.iChallengeDisplayTime = -1
				ENDIF
			ENDIF
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI) AND NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE()// AND Player_Hunt_Data.bCanDisplayChallengeToast
				TEXT_LABEL_15 toastMsg
				toastMsg = "HUNTUI_RANKUP"
				IF huntingData.challengeData[hcIndex].hcRank = HCR_RANK_5
					toastMsg = "HUNTUI_COMPLETE"
				ENDIF
//				SET_SCALEFORM_MIDSIZED_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_WEAPON_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000)
				SET_SHARD_BIG_MESSAGE_WITH_NUMBER_IN_STRAPLINE(Player_Hunt_Data.splashUI, "HUNTUI_WEAPON_T", ENUM_TO_INT(huntingData.challengeData[hcIndex].hcRank)+1, toastMsg, 4000, SHARD_MESSAGE_MIDSIZED, HUD_COLOUR_WHITE)
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_NEXT
			ENDIF

		BREAK
		CASE HCS_RANK_NEXT
			IF NOT WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI)
				// Reward
				DO_CHALLENGE_RANK_SCORING(huntingData.challengeData[hcIndex].hcRank)
				
				// Weapons master is all shooting things, except rank 2, which requires you to run over a boar.
				IF (huntingData.challengeData[hcIndex].hcRank != HCR_RANK_2)
					INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_SHOOTING_ABILITY, 2)
				ELSE
					INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR, PS_DRIVING_ABILITY, 2)
				ENDIF

				huntingData.challengeData[hcIndex].hcRank += INT_TO_ENUM(HUNTING_CHALLENGE_RANK, 1)
				
				SETUP_CHALLENGE_BRIEFS()
				huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_INIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC UPDATE_HUNTING_CHALLENGES(HT_PLAYER_DATA &huntingData)

	// Loop through all challenges and update them now.
	HUNTING_CHALLENGE hcIndex
	REPEAT COUNT_OF(huntingData.challengeData) hcIndex
		IF huntingData.challengeData[hcIndex].hcRank = HCR_LOCKED
			IF huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_NEXT AND NOT IS_FIRST_PERSON_AIM_CAM_ACTIVE() // We've been told to unlock this
				IF TIMER_DO_ONCE_WHEN_READY(huntingData.unlockTimer, 21.5)
					CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_TREVOR)
					huntingData.challengeData[hcIndex].hcRank = HCR_RANK_1
					huntingData.challengeData[hcIndex].hcStatus = HCS_RANK_INIT
					Player_Hunt_Data.bSetupHuntingControls = FALSE
					IF NOT Player_Hunt_Data.bSentChalUnlockHelp
						IF NOT g_SavedGlobals.sAmbient.bChallengeHelpDisplayed
							g_SavedGlobals.sAmbient.bChallengeHelpDisplayed = TRUE
							PRINT_HELP("AHT_CHAL_UNLOCKED")
						ENDIF
						Player_Hunt_Data.bSentChalUnlockHelp = TRUE
					ENDIF
					SETUP_CHALLENGE_BRIEFS()
					
				ELIF TIMER_DO_WHEN_READY(huntingData.unlockTimer, 20) AND NOT huntingData.bSentUnlockText
					STRING sTemp = GET_TEXT_MESSAGE_FOR_CHALLENGE_UNLOCK(hcIndex)
					SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_HUNTER, sTemp, TXTMSG_LOCKED)
					huntingData.bSentUnlockText = TRUE
				ELIF NOT IS_TIMER_STARTED(huntingData.unlockTimer)
					START_TIMER_NOW(huntingData.unlockTimer)
					huntingData.bSentUnlockText = FALSE
				ENDIF
			ENDIF
		ELIF huntingData.challengeData[hcIndex].hcRank != HCR_COMPLETE
			SWITCH hcIndex
				CASE HC_MASTER_HUNTER
					UPDATE_HUNTING_CHALLENGE_MASTER_HUNTER(huntingData, hcIndex)
				BREAK
				CASE HC_PHOTOJOURNALIST
					UPDATE_HUNTING_CHALLENGE_PHOTOJOURNALIST(huntingData, hcIndex)
				BREAK
				CASE HC_WEAPONS_MASTER
					UPDATE_HUNTING_CHALLENGE_WEAPONS_MASTER(huntingData, hcIndex)
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Wipes all hunting tracking progress.
PROC HUNTING_CHALLENGES_CLEAR_PROGRESS(HT_PLAYER_DATA &huntingData)
	huntingData.iKillsSinceElkCall = 0
	huntingData.iKillStreakHeartshot = 0
	huntingData.iKillsTimed = 0
	huntingData.iKillsSinceSpooked  = 0
	
	huntingData.iChallengeDisplayTime = -1
ENDPROC

/// PURPOSE:
///    Sends all challenge texts to the player at the start of hunting.
PROC INITIALIZE_CHALLENGES()
	HUNTING_CHALLENGE hcIndex
	REPEAT COUNT_OF(HUNTING_CHALLENGE) hcIndex
		// If this is the first time we are playing ambient hunting, set the challenges to be locked
		IF g_SavedGlobals.sAmbient.iHighScoreFreeMode = 0
			g_SavedGlobals.sAmbient.iChallengeRankFreeMode[hcIndex] = ENUM_TO_INT(HCR_LOCKED)
			PRINTLN("INIT_VARS_FOR_THIS_MODE - Challenge being locked because we didn't have a high score: ", hcIndex)
		ENDIF
		
		PRINTLN("INIT_VARS_FOR_THIS_MODE - Logging rank for challenge: ", hcIndex, " as ", INT_TO_ENUM(HUNTING_CHALLENGE_RANK, g_SavedGlobals.sAmbient.iChallengeRankFreeMode[hcIndex]))
		Player_Hunt_Data.challengeData[hcIndex].hcRank = INT_TO_ENUM(HUNTING_CHALLENGE_RANK, g_SavedGlobals.sAmbient.iChallengeRankFreeMode[hcIndex])
	ENDREPEAT
	
	// Setup the brief!
	SETUP_CHALLENGE_BRIEFS()
ENDPROC



