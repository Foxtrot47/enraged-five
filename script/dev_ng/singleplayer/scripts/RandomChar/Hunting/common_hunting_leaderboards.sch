USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"
USING "common_hunting.sch"

//Name: MINI_GAMES_HUNTING
//ID: 811 - LEADERBOARD_MINI_GAMES_HUNTING
//Inputs: 7
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_MEDAL (int)
//	LB_INPUT_COL_NUM_ELK_KILLED (int)
//	LB_INPUT_COL_NUM_HEART_SHOTS (int)
//	LB_INPUT_COL_NUM_PHOTO_TEXT (int)
//	LB_INPUT_COL_TOTAL_TIME (long)
//	LB_INPUT_COL_CASH (int)
//Columns: 7
//	SCORE_COLUMN ( AGG_Max ) - InputId: LB_INPUT_COL_SCORE
//	MEDAL_COLUMN ( AGG_Last ) - InputId: LB_INPUT_COL_MEDAL
//	NUM_ELK_KILLED ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_ELK_KILLED
//	NUM_HEART_SHOTS ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_HEART_SHOTS
//	NUM_PHOTO_TEXT ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_PHOTO_TEXT
//	GOLD_MEDAL_TIME ( AGG_Min ) - InputId: LB_INPUT_COL_TOTAL_TIME
//	CASH ( AGG_Sum ) - InputId: LB_INPUT_COL_CASH
//Instances: 2
//	Challenge
//	ScEvent


PROC READ_HUNTING_SOCIAL_CLUB_LEADERBOARD()
	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(Player_Hunt_Data.huntLB_control, FMMC_TYPE_MG_HUNTING, "", "")
ENDPROC

PROC DISPLAY_HUNTING_SOCIAL_CLUB_LEADERBOARD()
	DRAW_SC_SCALEFORM_LEADERBOARD(Player_Hunt_Data.huntLB_UI, Player_Hunt_Data.huntLB_control)
ENDPROC

PROC HUNTING_WRITE_TO_LEADERBOARD()
	//Why 3 categories? Do we not just want 1 MP or SP??
	TEXT_LABEL_31 categoryNames[1] 
    TEXT_LABEL_23 uniqueIdentifiers[1]
    
	INT iMedalEarned
	INT iTimeToGoldMS = g_SavedGlobals.sAmbient.iLowestGoldTime
	IF (iTimeToGoldMS = 0)
		iTimeToGoldMS = HIGHEST_INT 	// This shows --:--:--- on the leaderboard.
	ENDIF
	
	// Begin setting the leaderboard data types
	PRINTLN("HUNTING_WRITE_TO_LEADERBOARD - Attempting to INIT_LEADERBOARD_WRITE...")
	
	// Need to check if he's online. INIT_LEADERBOARD_WRITE does this, but it asserts instead of somply returning false.
	IF CAN_WRITE_TO_LEADERBOARD()
	AND INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_HUNTING, uniqueIdentifiers, categoryNames, 0, DEFAULT, TRUE)
		PRINTLN("HUNTING_WRITE_TO_LEADERBOARD - Writing to leaderboard.")
		
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold
			iMedalEarned = 3
			iTimeToGoldMS = ROUND(GET_TIMER_IN_SECONDS(Player_Hunt_Data.tTimeToGold) * 1000)
			IF (iTimeToGoldMS < g_SavedGlobals.sAmbient.iLowestGoldTime)
			OR (g_SavedGlobals.sAmbient.iLowestGoldTime = 0)
				g_SavedGlobals.sAmbient.iLowestGoldTime = iTimeToGoldMS
			ENDIF
		ELIF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForSilver
			iMedalEarned = 2
		ELIF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForBronze
			iMedalEarned = 1
		ENDIF	
		
		// Score, gold time, elks killed, photos sent, money
		// High scores
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_SCORE, Player_Hunt_Data.iScore, 				0.0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_TOTAL_TIME, iTimeToGoldMS,  					0.0)

		INT iElkKills = Player_Hunt_Data.iKillCount
		iElkKills -= Player_Hunt_Data.iCougarKills + Player_Hunt_Data.iBoarKills + Player_Hunt_Data.iCoyoteKills
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_NUM_ELK_KILLED, iElkKills, 					0.0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_NUM_PHOTO_TEXT, Player_hunt_data.iPhotosSent, 0.0)
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_CASH, Player_hunt_data.iMoney,  	 	  		0.0)
		
		// Not displayed
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_NUM_HEART_SHOTS, Player_Hunt_Data.iHitsOnLocation[HL_HEART], 0.0)		
		WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_HUNTING), LB_INPUT_COL_MEDAL, iMedalEarned, 							0.0)
	ENDIF
ENDPROC


