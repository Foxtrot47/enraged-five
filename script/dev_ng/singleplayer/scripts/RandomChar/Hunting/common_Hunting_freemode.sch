USING "script_blips.sch"
USING "RC_Helper_Functions.sch"
USING "Shared_Hud_Displays.sch"
USING "RC_Area_public.sch"
USING "rc_threat_public.sch"
USING "commands_pad.sch"
USING "LauncherInclude.sch"
USING "rgeneral_include.sch"
USING "common_hunting_challenges.sch"

//REL_GROUP_HASH mAnimalGroup

// For the random animals
ENUM ANIMAL_STATE 
	AS_NULL = 0,
	AS_CHECK_CAN_SPAWN,
	AS_SPAWN_IN,
	AS_IDLE,
	AS_RUN_OFF,
	AS_RUNNING_OFF,
	AS_SET_STALKING_PLAYER,
	AS_STALKING_PLAYER,
	AS_SET_ATTACKING,
	AS_ATTACKING,
	AS_CLEAN_UP,
	AS_CLEAN_UP_DELETE
ENDENUM

ENUM ANIMAL_TYPE
	ANIMAL_TYPE_MOUNTAIN_LION = 0,
	ANIMAL_TYPE_COYOTE,
	ANIMAL_TYPE_BOAR
ENDENUM

CONST_FLOAT ANIMAL_WANDER_RADIUS			20.0
CONST_FLOAT	ANIMAL_MIN_WAIT_TIME			1.0
CONST_FLOAT ANIMAL_MAX_WAIT_TIME			6.0
CONST_FLOAT ANIMAL_CLEANUP_DISTANCE			135.0
CONST_FLOAT COUGAR_CLEANUP_DISTANCE			170.0
CONST_INT ANIMAL_SPAWN_DELAY_TIME_MIN		5000
CONST_INT ANIMAL_SPAWN_DELAY_TIME_MAX		20000

CONST_INT MAX_ANIMAL_SPAWN_POS				55
CONST_INT PLAYER_PROXIMITY_FLEE_RANGE		8
CONST_INT PLAYER_PROXIMITY_BOAR_FLEE_RANGE	18
CONST_INT PLAYER_SPOTTED_FLEE_RANGE			15
CONST_INT PLAYER_SPOTTED_BOAR_FLEE_RANGE	25
CONST_INT MAX_RANDOM_ANIMALS				3
CONST_INT MAX_COUGARS						2
CONST_INT INITIAL_COUGAR_SPAWN_TIME			210000		// Can't spawn a cougar for the first 210 seconds
CONST_INT MIN_TIME_BETWEEN_COUGARS			160000		// Spawn cougars every 160 - 270 seconds.
CONST_INT MAX_TIME_BETWEEN_COUGARS			270000
CONST_INT ANIMAL_CALL_MIN_DELAY				30000
CONST_INT ANIMAL_CALL_MAX_DELAY				60000

STRUCT MY_ANIMAL
	PED_INDEX mPed
	BLIP_INDEX mAnimalBlip

	INT iNextTestTime = 0
	
	BOOL bStopCallBlip = FALSE
	BOOL bAnimalSpotted = FALSE
	INT iBlipAlpha		= 0
	BOOL bIsSpooked 	= FALSE
	BOOL bCanFadeBlip	= TRUE
	BOOL bHardBlipped 	= FALSE
	INT iStartFadeTime
	INT iNextCall = 0
	
	MODEL_NAMES mModel
	ANIMAL_STATE animalState = AS_CHECK_CAN_SPAWN
	ANIMAL_TYPE	animalType
		
	//Elks spawn pos and scent offset position
	VECTOR vPos
	
	//Waiting time at each node
	INT iWaitTime
	INT iCurTime
	
	// ROUTE NODES
	VECTOR vHomePosition
	FLOAT fRandHead
	FLOAT fDist
	
	// Packs
	INT iPackLeader = -1
	
	INT iSpawnDelayTime = -1
	BOOL bDelayBeforeRespawn = TRUE	
	
	BOOL bCougarTaunted = FALSE
	
	INT iCougarReblipDelay = -1
ENDSTRUCT
MY_ANIMAL mAnimal[MAX_RANDOM_ANIMALS]
MY_ANIMAL mCougar[MAX_COUGARS]

BOOL bAnimalSpawn_Boar = FALSE

SHAPETEST_INDEX animalShapeTest
INT iAnimalTesting = -1

VECTOR vAnimalSpawnPos[MAX_ANIMAL_SPAWN_POS]
INT iLastAnimalSpawnIndex = -1

/// PURPOSE:
///    Loads the elk model.
PROC LOAD_RANDOM_ANIMAL_MODELS()

	REQUEST_MODEL(A_C_MTLION)
	REQUEST_MODEL(A_C_BOAR)
	REQUEST_MODEL(A_C_COYOTE)
	
	WHILE NOT HAS_MODEL_LOADED(A_C_MTLION)
	AND NOT HAS_MODEL_LOADED(A_C_BOAR)
	AND NOT HAS_MODEL_LOADED(A_C_COYOTE)
		WAIT(0)
	ENDWHILE
	
ENDPROC


/// PURPOSE:
///    Spawns an animal ped and returns true if it was succesful
///    Loads the model and unloads the model if the ped was created properly
/// PARAMS:
///    pedindex - The ped index to write the newly created ped to 
///    model - The model the ped should be created with
///    pos - The postion the ped should be created at
///    dir - The heading the ped should have
/// RETURNS:
///    TRUE if the ped was created sucessfully 
FUNC BOOL SPAWN_ANIMAL(PED_INDEX &animalIndex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnload = TRUE)
	IF NOT DOES_ENTITY_EXIST(animalIndex)
		IF REQUEST_AND_CHECK_MODEL(model)
			animalIndex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			SET_ENTITY_AS_MISSION_ENTITY(animalIndex)
			SET_ENTITY_LOD_DIST(animalIndex, 1500)
			SET_ENTITY_LOAD_COLLISION_FLAG(animalIndex, TRUE)
			
			IF bUnload
				SET_MODEL_AS_NO_LONGER_NEEDED(model)
			ENDIF
			
			IF DOES_ENTITY_EXIST(animalIndex)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SETUP_ANIMAL(MY_ANIMAL & animal, ANIMAL_STATE eAnimalState, BOOL bBlipFade = TRUE)
	IF DOES_ENTITY_EXIST (animal.mPed)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(animal.mPed, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(animal.mPed, CA_USE_COVER, FALSE)
		SET_PED_FLEE_ATTRIBUTES(animal.mPed, FA_USE_COVER, FALSE)	
		SET_ENTITY_HEALTH(animal.mPed, 100)
		SET_PED_BLOCKS_PATHING_WHEN_DEAD(animal.mPed, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(animal.mPed, TRUE)
		
		SET_PED_SEEING_RANGE(animal.mPed, SIGHT_RANGE_DUCKED)
		SET_PED_HEARING_RANGE(animal.mPed, HEAR_RANGE)
		
		SET_ENTITY_LOD_DIST(animal.mPed, 1500)
		SET_ENTITY_LOAD_COLLISION_FLAG(animal.mPed, TRUE)
		SET_PED_LOD_MULTIPLIER(animal.mPed, 5.0)
		
		IF (animal.animalType = ANIMAL_TYPE_MOUNTAIN_LION)
			SET_PED_CAN_BE_TARGETTED(animal.mPed, TRUE)
		ELSE
			SET_PED_CAN_BE_TARGETTED(animal.mPed, FALSE)
		ENDIF
		
		animal.bCanFadeBlip = bBlipFade
		animal.bIsSpooked = FALSE
		animal.bStopCallBlip = FALSE
		bUncleanKill = FALSE
		
		// Put it's next call time some time in the future.
		animal.iNextCall = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ANIMAL_CALL_MIN_DELAY, ANIMAL_CALL_MAX_DELAY)

		animal.animalState = eAnimalState
	ENDIF
ENDPROC


PROC ADD_ANIMAL_CORPSE(MY_ANIMAL &animal, WEAPON_TYPE eKillWeapon)
	KILL_TYPE eKillType
	
	SWITCH GET_ENTITY_MODEL(animal.mPed)
		CASE A_C_COYOTE
			eKillType = KILL_COYOTE_KILL
		BREAK
		CASE A_C_BOAR
			eKillType = KILL_BOAR_KILL
		BREAK
		CASE A_C_MTLION
			eKillType = KILL_MOUNTAIN_LION_KILL
		BREAK
	ENDSWITCH
	ADD_CORPSE_WITH_LOCATION(animal.mPed, eKillType, eKillWeapon)
ENDPROC

/// PURPOSE:
///    Checks if the player has just killed a random animal
///    Increments the kill counter and records where the animal was shot.
/// PARAMS:
///    i - the individual elk to check
PROC HAS_PLAYER_KILLED_RANDOM_ANIMAL( MY_ANIMAL &animal )
	IF NOT DOES_ENTITY_EXIST(animal.mPed)
		EXIT
	ENDIF
	
	IF NOT IS_PED_INJURED(animal.mPed)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(animal.mPed)
		// Don't really care about this, just need to prevent an error below.
	ENDIF
		
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(animal.mPed, PLAYER_PED_ID())
		bSuppressNearMissMessage = TRUE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AHT_MISS")
			CLEAR_HELP()
		ENDIF

		CHECK_WEAPONS_MASTER_CHALLENGE_FOR_ANIMAL(Player_Hunt_Data,animal.mPed)
		
		// Determine weapon type
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND HAS_PED_BEEN_DAMAGED_BY_WEAPON(animal.mPed, WEAPONTYPE_RUNOVERBYVEHICLE)
			Player_Hunt_Data.iKillCount += 1
			ADD_ANIMAL_CORPSE(animal, WEAPONTYPE_RUNOVERBYVEHICLE)
			IF IS_ENTITY_ALIVE(animal.mPed)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(animal.mPed)
			ENDIF
			EXIT
		ENDIF
	
		IF IS_ENTITY_ALIVE(animal.mPed)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(animal.mPed)
		ENDIF
									
		IF NOT bGotNearMissTime
			iMissMessageDelay = GET_GAME_TIMER() + MISS_MESSAGE_DELAY_TIME
			bSuppressNearMissMessage = TRUE
			bGotNearMissTime = TRUE
		ENDIF
		
		CPRINTLN(DEBUG_MISSION, "Kill made NOT in kill chanllenge..")
		Player_Hunt_Data.iKillCount += 1

		// Process this as a death.
		IF (animal.animalType = ANIMAL_TYPE_COYOTE)
			Player_Hunt_Data.iCoyoteKills++
		ELIF (animal.animalType = ANIMAL_TYPE_BOAR)
			Player_Hunt_Data.iBoarKills++
		ELIF (animal.animalType = ANIMAL_TYPE_MOUNTAIN_LION)
			Player_Hunt_Data.iCougarKills++
		ENDIF	
		
		
		WEAPON_TYPE killWeapon
		IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), killWeapon)
			killWeapon = WEAPONTYPE_INVALID
		ENDIF
		ADD_ANIMAL_CORPSE(animal, killWeapon)
		
		TRIGGER_HUNTING_AUDIO("HUNTING_KILL", TRUE)
		
		IF Player_Hunt_Data.iScore > 0
			#IF IS_DEBUG_BUILD
				PRINTLN("KILLCOUNT/SCORE = ", Player_Hunt_Data.iScore)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Ensures that this spawn position isn't too close to any existing animal.
FUNC BOOL VERIFY_ANIMAL_SPAWN_VS_OTHER_ANIMALS(INT iSpawnPtIndex)
	INT idx
	REPEAT MAX_RANDOM_ANIMALS idx
		IF DOES_ENTITY_EXIST(mAnimal[idx].mPed)
		AND NOT IS_PED_INJURED(mAnimal[idx].mPed)
			VECTOR vOtherAnimalCoords = GET_ENTITY_COORDS(mAnimal[idx].mPed)
			
			// Need to be further than 20m from the this animal.
			IF VDIST2(vOtherAnimalCoords, vAnimalSpawnPos[iSpawnPtIndex]) < 400.0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Finds a vaild animal spawn position that's not the same as the index last used.
FUNC BOOL GET_ANIMAL_SPAWN_POS(VECTOR &vPos, FLOAT &fHeading, BOOL bCougar = FALSE)
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	INT idx = GET_RANDOM_INT_IN_RANGE(0, MAX_ANIMAL_SPAWN_POS)
	IF (idx = iLastAnimalSpawnIndex)
		RETURN FALSE
	ENDIF
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vHuntingPlayerCoords, vAnimalSpawnPos[idx])
	FLOAT fMinDist = MIN_SAFE_SPAWN_DIST
	FLOAT fMaxDist = MAX_SAFE_SPAWN_DIST
	IF bCougar
		fMinDist = 60.0
		fMaxDist = 150.0
	ENDIF
	
	IF (fDist > fMinDist) AND (fDist < fMaxDist)
		IF (NOT IS_SPHERE_VISIBLE(vAnimalSpawnPos[idx], 1.5)
		OR IS_SCREEN_FADED_OUT())
		AND (bCougar OR WOULD_POINT_BE_IN_FRONT_OF_PLAYER(vAnimalSpawnPos[idx]))
		AND VERIFY_ANIMAL_SPAWN_VS_OTHER_ANIMALS(idx)
			//CPRINTLN(DEBUG_MISSION,"This pos is NOT visible ", i)
			vPos = vAnimalSpawnPos[idx]
			
			SNAP_3D_COORD_TO_GROUND(vPos)
			fHeading = GET_RANDOM_FLOAT_IN_RANGE(0, 359)
			iLastAnimalSpawnIndex = idx
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_ANIMAL_BEEN_SPOOKED(MY_ANIMAL &animal)
	// Mountain lions don't spook.
	IF (animal.animalType = ANIMAL_TYPE_MOUNTAIN_LION)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(animal.mPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_ANYONE_SHOOTING_NEAR_PED(animal.mPed)
		TRIGGER_HUNTING_AUDIO("HUNTING_MISSED")
		animal.bIsSpooked = TRUE
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(animal.mPed, PLAYER_PED_ID())
		animal.bIsSpooked = TRUE
		RETURN TRUE
	ENDIF
	
	IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), animal.mPed)
		animal.bIsSpooked = TRUE
		RETURN TRUE
	ENDIF

	FLOAT fDist
	fDist = GET_DISTANCE_BETWEEN_ENTITIES(animal.mPed, PLAYER_PED_ID())
	IF (animal.animalType = ANIMAL_TYPE_BOAR)
		// Boar are slightly more perceptive than other animals.
		IF CAN_PED_SEE_PED(animal.mPed, PLAYER_PED_ID())
		
		AND fDist < PLAYER_SPOTTED_BOAR_FLEE_RANGE
			animal.bIsSpooked = TRUE
			RETURN TRUE
		ENDIF
		
		IF (fDist < PLAYER_PROXIMITY_BOAR_FLEE_RANGE)
			animal.bIsSpooked = TRUE
			RETURN TRUE
		ENDIF			
	ELIF (animal.animalType != ANIMAL_TYPE_MOUNTAIN_LION)
		IF CAN_PED_SEE_PED(animal.mPed, PLAYER_PED_ID())
		AND fDist < PLAYER_SPOTTED_FLEE_RANGE
			animal.bIsSpooked = TRUE
			RETURN TRUE
		ENDIF
		
		IF (fDist < PLAYER_PROXIMITY_FLEE_RANGE)
			animal.bIsSpooked = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Runs a shapetest on an occluded ped.
/// RETURNS:
///    1 if the test passed. 0 to keep processing. -1 if it failed and is done.
FUNC INT RUN_SHAPE_TEST_FOR_ANIMAL(MY_ANIMAL & thisAnimal, VECTOR vAnimalCoord, FLOAT fDistToPlayer)
	IF (animalShapeTest = NULL)
		//PRINTLN("Starting shapetest on animal: ", iAnimalIdx)
		VECTOR vStart = vHuntingPlayerCoords + <<0,0,0.75>>
		animalShapeTest = START_SHAPE_TEST_LOS_PROBE(vStart, vAnimalCoord)
	ELSE
		INT iHit
		VECTOR vHitPos, vNormal
		ENTITY_INDEX entHit
		//PRINTLN("Processing shapetest on animal: ", iAnimalIdx)
		SHAPETEST_STATUS eResult = GET_SHAPE_TEST_RESULT(animalShapeTest, iHit, vHitPos, vNormal, entHit)
		IF (eResult = SHAPETEST_STATUS_RESULTS_READY)
			//PRINTLN("Shapetest results -- iHit: ", iHit, ", vPos: ", vHitPos, ", entHit: ", NATIVE_TO_INT(entHit))
			IF (iHit = 0)
				// PASSED!
				//PRINTLN("Animal passed shapetest: ", vAnimalCoord)
				thisAnimal.iNextTestTime = GET_GAME_TIMER() + GET_HARD_BLIP_DELAY_TIME(fDistToPlayer)
				iAnimalTesting = -1
				animalShapeTest = NULL
				
				RETURN 1
			ELSE
				// FAILED
				//PRINTLN("Animal failed shapetest: ", vAnimalCoord)
				thisAnimal.iNextTestTime = GET_GAME_TIMER() + GET_HARD_BLIP_DELAY_TIME(fDistToPlayer)
				iAnimalTesting = -1
				animalShapeTest = NULL
				
				RETURN -1
			ENDIF
		ELIF (eResult = SHAPETEST_STATUS_NONEXISTENT)
			//PRINTLN("No test result. Going to null: ", iAnimalIdx)
			//PRINTLN("No shapetest: ", vAnimalCoord)
			iAnimalTesting = -1
			animalShapeTest = NULL
			
			RETURN -1
		ENDIF
	ENDIF
	
	//PRINTLN("Processing shapetest: ", vAnimalCoord)
	RETURN 0
ENDFUNC



PROC MANAGE_ANIMAL_BLIP(MY_ANIMAL &thisAnimal, INT iAnimalIndex)
	IF NOT DOES_ENTITY_EXIST(thisAnimal.mPed)
		EXIT
	ENDIF
	IF IS_ENTITY_DEAD(thisAnimal.mPed)
		EXIT
	ENDIF
	IF thisAnimal.bIsSpooked
		EXIT
	ENDIF
	IF thisAnimal.bStopCallBlip
		EXIT
	ENDIF
	
	// Always going to need distance.
	VECTOR vAnimalCoord = GET_ENTITY_COORDS(thisAnimal.mPed, FALSE)
	FLOAT distToPlayer = GET_DISTANCE_BETWEEN_COORDS(vHuntingPlayerCoords, vAnimalCoord)
	BOOL bOnScreen = IS_ENTITY_ON_SCREEN(thisAnimal.mPed)
	
	// Manage hard blipping first.
	IF NOT thisAnimal.bHardBlipped
		BOOL bOccluded = TRUE	
		IF bOnScreen AND (thisAnimal.iNextTestTime < GET_GAME_TIMER()) 
		AND (distToPlayer < 210.0) 
		AND (iAnimalTesting = iAnimalIndex OR iAnimalTesting = -1)
			// Flag us as the tester!
			iAnimalTesting = iAnimalIndex
			
			INT iResult = RUN_SHAPE_TEST_FOR_ANIMAL(thisAnimal, vAnimalCoord, distToPlayer)
			IF (iResult = 1)
				// Passed!
				bOccluded = FALSE
			ENDIF
		ENDIF	
		
		// Blip the animal if he's on screen, not occluded, and "close enough" as defined by zoom level.
		IF bOnScreen AND NOT bOccluded
			FLOAT fScreenX, fScreenY
			GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(vAnimalCoord, fScreenX, fScreenY)
			IF (fScreenX > 0.33 AND fScreenX < 0.66 AND fScreenY > 0.33 AND fScreenY < 0.66)
			AND (distToPlayer < fDistanceThreshold)
				CPRINTLN(DEBUG_MISSION, "Removing existing animal blip before hard-blipping.") 
				SAFE_REMOVE_BLIP(thisAnimal.mAnimalBlip)
				
				CPRINTLN(DEBUG_MISSION, "Adding hard animal blip!")
				ADD_SAFE_BLIP_TO_PED(thisAnimal.mAnimalBlip, thisAnimal.mPed, TRUE)
				SET_BLIP_SHOW_CONE(thisAnimal.mAnimalBlip, TRUE)
				thisAnimal.bHardBlipped = TRUE
				
				TRIGGER_HUNTING_AUDIO("HUNTING_SPOT_ANIMAL")
				
				CPRINTLN(DEBUG_MISSION, "Adding hard blip on ANIMAL. fDist: ", distToPlayer, ", Zoom: ", fZoomLevel, ", ScreenX: ", fScreenX, ", ScreenY: ", fScreenY) 
				
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(thisAnimal.mAnimalBlip)
		// This would have the animal respond to the player. Let's not do that.
		IF thisAnimal.iNextCall < GET_GAME_TIMER()
			FLOAT fBlipRadius = distToPlayer * 0.5
			VECTOR vBlipPos = vAnimalCoord + <<GET_RANDOM_FLOAT_IN_RANGE(-3.0, 3.0), GET_RANDOM_FLOAT_IN_RANGE(-3.0, 3.0), 0>>
			
			CPRINTLN(DEBUG_MISSION, "Adding animal radius blip: ", fBlipRadius)
			thisAnimal.mAnimalBlip = ADD_BLIP_FOR_RADIUS(vBlipPos, fBlipRadius)
			SET_BLIP_ALPHA(thisAnimal.mAnimalBlip, 100)
			SET_BLIP_COLOUR(thisAnimal.mAnimalBlip, BLIP_COLOUR_RED)
			SHOW_HEIGHT_ON_BLIP(thisAnimal.mAnimalBlip, FALSE)
			
			thisAnimal.iStartFadeTime = GET_GAME_TIMER() + 10000
			thisAnimal.iNextCall = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ANIMAL_CALL_MIN_DELAY, ANIMAL_CALL_MAX_DELAY)
		ENDIF
	ELSE
		IF (GET_GAME_TIMER() > thisAnimal.iStartFadeTime AND distToPlayer > 35 AND thisAnimal.bCanFadeBlip)
			IF bOnScreen
				// See if we need to shapetest for the blip.
				IF (thisAnimal.iNextTestTime < GET_GAME_TIMER() 
				AND (iAnimalTesting = iAnimalIndex OR iAnimalTesting = -1)) 
				AND (distToPlayer < fDistanceThreshold)
					// Flag us as the tester!
					iAnimalTesting = iAnimalIndex
			
					INT iResult = RUN_SHAPE_TEST_FOR_ANIMAL(thisAnimal, vAnimalCoord, distToPlayer)
					IF (iResult = 1)
						CPRINTLN(DEBUG_MISSION, "Restoring hard animal blip alpha!")
						
						// Passed!
						IF thisAnimal.bHardBlipped
							SET_BLIP_ALPHA(thisAnimal.mAnimalBlip, 255)
						ELSE
							SET_BLIP_ALPHA(thisAnimal.mAnimalBlip, 100)
						ENDIF
						thisAnimal.iStartFadeTime = GET_GAME_TIMER() + 4000
						EXIT
						
					ELIF (iResult = -1)
						// Failed...							
					ELIF (iResult = 0)
						// Continue processing...
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		thisAnimal.iBlipAlpha = GET_BLIP_ALPHA(thisAnimal.mAnimalBlip)
		IF thisAnimal.iBlipAlpha > MIN_BLIP_ALPHA
			IF GET_RANDOM_INT_IN_RANGE(0, 1000) > 666
				thisAnimal.iBlipAlpha -= 1
				SET_BLIP_ALPHA(thisAnimal.mAnimalBlip, thisAnimal.iBlipAlpha)
			ENDIF
		ELSE
			SET_BLIP_SHOW_CONE(thisAnimal.mAnimalBlip, FALSE)
			
			CPRINTLN(DEBUG_MISSION, "Animal blip alpha'd out. Remove blip.") 
			SAFE_REMOVE_BLIP(thisAnimal.mAnimalBlip)
			thisAnimal.bHardBlipped = FALSE
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Removes any other blip on a spooked animal, and adds aa fleeing blip.
PROC SETUP_ANIMAL_SPOOKED_BLIP(MY_ANIMAL &animal)
	IF DOES_BLIP_EXIST(animal.mAnimalBlip)
		REMOVE_BLIP(animal.mAnimalBlip)
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "Adding animal spooked blip!")
	ADD_SAFE_BLIP_TO_PED(animal.mAnimalBlip, animal.mPed, TRUE, BLIP_SIZE_PED)
	SET_BLIP_ALPHA(animal.mAnimalBlip, 128)
	SET_BLIP_SHOW_CONE(animal.mAnimalBlip, TRUE)
ENDPROC


/// PURPOSE:
///    Snaps a ped to the ground.
PROC SET_PED_ON_GROUND_PROPERLY(PED_INDEX &pedIndex)
	VECTOR vGround = GET_ENTITY_COORDS(pedIndex, FALSE)
	GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(pedIndex, FALSE), vGround.Z)
	SET_ENTITY_COORDS(pedIndex, vGround, FALSE)
ENDPROC


/// PURPOSE:
///    Resets the timer on all animal calls.
PROC RESET_ALL_ANIMAL_CALLS()
	INT idx
	REPEAT MAX_RANDOM_ANIMALS idx
		mAnimal[idx].iNextCall = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ANIMAL_CALL_MIN_DELAY, ANIMAL_CALL_MAX_DELAY)
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Spawns a single coyote in a pack.
FUNC BOOL SPAWN_COYOTE_FOR_PACK(INT iCoyoteIdx, INT iLeaderIdx)
	VECTOR vOffset, vPosition
	FLOAT fHeading
	SWITCH iCoyoteIdx
		CASE 0		vOffset = << GET_RANDOM_FLOAT_IN_RANGE(-2.0, -1.0), GET_RANDOM_FLOAT_IN_RANGE(-2.0, -1.0), 0.0 >>		BREAK		
		CASE 1		vOffset = << GET_RANDOM_FLOAT_IN_RANGE(2.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(-2.0, -1.0), 0.0 >>			BREAK
		CASE 2		vOffset = << GET_RANDOM_FLOAT_IN_RANGE(2.0, 1.0), GET_RANDOM_FLOAT_IN_RANGE(2.0, 1.0), 0.0 >>			BREAK
	ENDSWITCH
	vPosition = GET_ENTITY_COORDS(mAnimal[iLeaderIdx].mPed)
	fHeading = GET_ENTITY_HEADING(mAnimal[iLeaderIdx].mPed)
	
	mAnimal[iCoyoteIdx].vHomePosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, fHeading, vOffset)
	SNAP_3D_COORD_TO_GROUND(mAnimal[iCoyoteIdx].vPos)
	mAnimal[iCoyoteIdx].fRandHead = fHeading + GET_RANDOM_FLOAT_IN_RANGE(-30.0, 30.0)
	mAnimal[iCoyoteIdx].bIsSpooked = FALSE
	mAnimal[iCoyoteIdx].mModel = A_C_COYOTE
	mAnimal[iCoyoteIdx].animalType = ANIMAL_TYPE_COYOTE
	mAnimal[iCoyoteIdx].iPackLeader = iLeaderIdx
	
	IF SETUP_PED(mAnimal[iCoyoteIdx].mPed, mAnimal[iCoyoteIdx].vHomePosition, mAnimal[iCoyoteIdx].fRandHead, A_C_COYOTE, FALSE) 
		SETUP_ANIMAL(mAnimal[iCoyoteIdx], AS_IDLE, TRUE)
		TASK_FOLLOW_TO_OFFSET_OF_ENTITY(mAnimal[iCoyoteIdx].mPed, mAnimal[iLeaderIdx].mPed, vOffset, PEDMOVEBLENDRATIO_WALK)

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Spawns 0-2 coyotes to follow the leader
FUNC INT SPAWN_COYOTE_PACK_FOR_LEADER(INT iLeaderIdx)
	INT i, iPackMembers = 0
	REPEAT MAX_RANDOM_ANIMALS i
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 75
			IF NOT DOES_ENTITY_EXIST(mAnimal[i].mPed)
			OR IS_PED_INJURED(mAnimal[i].mPed)
				IF SPAWN_COYOTE_FOR_PACK(i, iLeaderIdx)
					iPackMembers += 1
				ENDIF
			ENDIF
		ENDIF
		
		i += 1
	ENDREPEAT
	
	RETURN iPackMembers
ENDFUNC


/// PURPOSE:
///    Forces an entire pack to scatter, blips the pack, and sets the proper state.
PROC FORCE_COYOTE_PACK_SCATTER(MY_ANIMAL & thisAnimal)	
	// Okay, make this pack flee. First, get the leader.
	INT iLeaderIdx = thisAnimal.iPackLeader
	
	// Now, go through all animals. Anything that is tagged with this same leader needs to flee.
	INT idx
	REPEAT MAX_RANDOM_ANIMALS idx
		IF DOES_ENTITY_EXIST(mAnimal[idx].mPed)
		AND NOT IS_PED_INJURED(mAnimal[idx].mPed)
			IF (mAnimal[idx].iPackLeader = iLeaderIdx)
				// Okay, flee.
				mAnimal[idx].bIsSpooked = TRUE
				SETUP_ANIMAL_SPOOKED_BLIP(mAnimal[idx])				
				mAnimal[idx].animalState = AS_RUN_OFF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Cleans up an animal passed in.
PROC CLEAN_UP_ANIMAL(MY_ANIMAL & animal)
	CPRINTLN(DEBUG_MISSION, "Animal cleaning up. Removing blip.") 
			
	SAFE_REMOVE_BLIP(animal.mAnimalBlip)
	animal.bIsSpooked = FALSE
	animal.bAnimalSpotted = FALSE
	animal.bStopCallBlip = FALSE
	animal.iNextTestTime = 0
	animal.iPackLeader = -1
	animal.bHardBlipped = FALSE
	animal.bCougarTaunted = FALSE
	
	IF DOES_ENTITY_EXIST( animal.mPed)
		IF animal.animalState = AS_CLEAN_UP
			SET_PED_AS_NO_LONGER_NEEDED(animal.mPed)
		ELSE
			DELETE_PED(animal.mPed)
		ENDIF
	ENDIF
	
	// If this was a cougar, decrement the count of cougars active and delay the next cougar spawn.
	IF (animal.animalType = ANIMAL_TYPE_MOUNTAIN_LION)
		// This need to be shrunk depending on our medal...
		animal.iSpawnDelayTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(MIN_TIME_BETWEEN_COUGARS, MAX_TIME_BETWEEN_COUGARS)
		IF (Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold)
			animal.iSpawnDelayTime -= 35000
		ELIF (Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForSilver)
			animal.iSpawnDelayTime -= 20000
		ELIF (Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForBronze)
			animal.iSpawnDelayTime -= 10000
		ENDIF
	ELSE
		animal.iSpawnDelayTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ANIMAL_SPAWN_DELAY_TIME_MIN, ANIMAL_SPAWN_DELAY_TIME_MAX)
	ENDIF				
	
	animal.bDelayBeforeRespawn = TRUE
	animal.animalState = AS_CHECK_CAN_SPAWN
	animal.mPed = NULL
ENDPROC

/// PURPOSE:
///    Manages blipping of cougars.
PROC MANAGE_COUGAR_BLIP(MY_ANIMAL & cougar)
	// Distance is needed no matter what.
	VECTOR vCoord = GET_ENTITY_COORDS(cougar.mPed)
	FLOAT distToPlayer = GET_DISTANCE_BETWEEN_COORDS(vHuntingPlayerCoords, vCoord)
	BOOL bOnScreen = IS_ENTITY_ON_SCREEN(cougar.mPed)
	
	// Hard blipping will be checked first.
	IF NOT cougar.bHardBlipped
		// Manage the blob blip on the cougar.
		IF NOT DOES_BLIP_EXIST(cougar.mAnimalBlip)
			IF GET_GAME_TIMER() > cougar.iCougarReblipDelay 
				FLOAT fBlipRadius = distToPlayer * 0.5
				VECTOR vBlipPos = vCoord + <<GET_RANDOM_FLOAT_IN_RANGE(-3.0, 3.0), GET_RANDOM_FLOAT_IN_RANGE(-3.0, 3.0), 0>>
				
				CPRINTLN(DEBUG_MISSION, "Adding cougar radius blip: ", fBlipRadius)
				cougar.mAnimalBlip = ADD_BLIP_FOR_RADIUS(vBlipPos, fBlipRadius)
				SET_BLIP_ALPHA(cougar.mAnimalBlip, 100)
				SET_BLIP_COLOUR(cougar.mAnimalBlip, BLIP_COLOUR_RED)
				SHOW_HEIGHT_ON_BLIP(cougar.mAnimalBlip, FALSE)
				cougar.iStartFadeTime = GET_GAME_TIMER() + 1500
			ENDIF		
		ELSE
			// Cougar has a blob blip. Need to reduce the alpha, until it's gone, then remove it.
			INT iAlpha = GET_BLIP_ALPHA(cougar.mAnimalBlip)
			IF (iAlpha != 0)
				IF GET_GAME_TIMER() > cougar.iStartFadeTime
					iAlpha -= 1
					SET_BLIP_ALPHA(cougar.mAnimalBlip, iAlpha)
				ENDIF
			ELSE
				SAFE_REMOVE_BLIP(cougar.mAnimalBlip)
				cougar.iCougarReblipDelay = GET_GAME_TIMER() + 1500
			ENDIF
		ENDIF
		
		// Should the cougar be hard blipped?
		BOOL bOccluded = TRUE	
		IF bOnScreen AND (cougar.iNextTestTime < GET_GAME_TIMER()) 
		AND (distToPlayer < fDistanceThreshold)			
			INT iResult = RUN_SHAPE_TEST_FOR_ANIMAL(cougar, vCoord, distToPlayer)
			IF (iResult = 1)
				// Passed!
				bOccluded = FALSE
			ENDIF
		ENDIF
		
		// Blip the deer if he's on screen, ,not occluded, and "close enough" as defined by zoom level.
		IF bOnScreen AND NOT bOccluded
			FLOAT fScreenX, fScreenY
			GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(vCoord, fScreenX, fScreenY)
			IF (fScreenX > 0.3 AND fScreenX < 0.7 AND fScreenY > 0.3 AND fScreenY < 0.7)
			AND (distToPlayer < fDistanceThreshold)
				CPRINTLN(DEBUG_MISSION, "Removing any existing blip on elk before hard blipping.") 
				SAFE_REMOVE_BLIP(cougar.mAnimalBlip)
				ADD_SAFE_BLIP_TO_PED(cougar.mAnimalBlip, cougar.mPed, TRUE)
				SET_BLIP_SHOW_CONE(cougar.mAnimalBlip, TRUE)
				cougar.bHardBlipped = TRUE
				cougar.bAnimalSpotted = TRUE
				
				TRIGGER_HUNTING_AUDIO("HUNTING_SPOT_COUGAR")
				
				EXIT
			ENDIF
		ENDIF
	ELSE
		// Once hard blipped, do nothing...
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles cougar activity. This is separated from animals to provide a more even chance of getting cougars.
PROC PROCESS_COUGAR(MY_ANIMAL & cougar, INT iCougar)
	FLOAT fDistance = 9999.9	
	IF DOES_ENTITY_EXIST(cougar.mPed) 
		IF NOT IS_PED_INJURED(cougar.mPed)
			// Cougar is alive and well. Handle the blip.
			MANAGE_COUGAR_BLIP(cougar)			
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR vAnimalPos = GET_ENTITY_COORDS(cougar.mPed)
				fDistance = GET_DISTANCE_BETWEEN_COORDS(vHuntingPlayerCoords, vAnimalPos)
				
				IF fDistance > COUGAR_CLEANUP_DISTANCE
					IF NOT IS_SPHERE_VISIBLE(vAnimalPos, 2.0)
					OR IS_SCREEN_FADED_OUT()
						cougar.animalState = AS_CLEAN_UP_DELETE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			cougar.animalState = AS_CLEAN_UP
		ENDIF
	ENDIF
	
	
	// Print a cougar timer...
	//PRINTLN("Cougar in: ", cougar.iSpawnDelayTime - GET_GAME_TIMER(), " :: State is: ", cougar.animalState)
	
	SWITCH cougar.animalState		
		CASE AS_CHECK_CAN_SPAWN
			// Second cougar never moves himself into spawning.
			IF (iCougar != 0)
				EXIT
			ENDIF
			
			// Has the cougar EVER had a spawn delay time?
			IF (cougar.iSpawnDelayTime = -1)
				cougar.iSpawnDelayTime = GET_GAME_TIMER() + INITIAL_COUGAR_SPAWN_TIME
			ELIF (GET_GAME_TIMER() > cougar.iSpawnDelayTime)
				cougar.animalState = AS_SPAWN_IN
				
				// This cougar could spawn in. If the palyer is far enough along, and the second cougar doesn't exist, then set him to spawn too.
				IF (Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold)
				AND (NOT DOES_ENTITY_EXIST(mCougar[1].mPed) OR IS_PED_INJURED(mCougar[1].mPed))
				AND (mCougar[1].animalState != AS_SPAWN_IN)
					mCougar[1].animalState = AS_SPAWN_IN
				ENDIF
			ENDIF
		BREAK
		
		CASE AS_SPAWN_IN
			IF NOT DOES_ENTITY_EXIST(cougar.mPed)
				IF GET_ANIMAL_SPAWN_POS(cougar.vHomePosition, cougar.fRandHead, TRUE)	
					cougar.mModel = A_C_MTLION
					cougar.animalType = ANIMAL_TYPE_MOUNTAIN_LION
						
					// Create the animal and set it to wander a fixed area by default
					IF SETUP_PED(cougar.mPed, cougar.vHomePosition, GET_RANDOM_FLOAT_IN_RANGE(0.0,360.0), cougar.mModel, FALSE) 
						SETUP_ANIMAL(cougar, AS_IDLE, TRUE)
						SET_PED_ON_GROUND_PROPERLY(cougar.mPed)
						
						// Cougars need to be flagged not to taunt.
						SET_PED_CONFIG_FLAG(cougar.mPed, PCF_PreventAllMeleeTaunts, TRUE)
						
						cougar.animalState = AS_SET_STALKING_PLAYER
					ENDIF	
				ENDIF
			ELSE
				// How'd we get here?
				cougar.animalState = AS_SET_STALKING_PLAYER
			ENDIF
		BREAK						
							
		CASE AS_SET_STALKING_PLAYER
			SET_PED_COMBAT_ATTRIBUTES(cougar.mPed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
			GIVE_DELAYED_WEAPON_TO_PED(cougar.mPed, WEAPONTYPE_COUGAR, -1, TRUE)
			TASK_GO_TO_ENTITY(cougar.mPed, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 10.0,  PEDMOVEBLENDRATIO_WALK)
			cougar.animalState = AS_STALKING_PLAYER
		BREAK
		
		
		CASE AS_STALKING_PLAYER
			// Cougars actively stalk the player. When they're far, they'll move faster.
			IF (fDistance > 25.0)
				SET_PED_MIN_MOVE_BLEND_RATIO(cougar.mPed, PEDMOVEBLENDRATIO_RUN)
			ELIF (fDistance <= 25.0) AND (fDistance >= 15.0)
				SET_PED_MIN_MOVE_BLEND_RATIO(cougar.mPed, PEDMOVEBLENDRATIO_WALK)
			ELSE
				IF NOT cougar.bCougarTaunted
					cougar.bCougarTaunted = TRUE
					CLEAR_PED_TASKS(cougar.mPed)
					TASK_STAND_STILL(cougar.mPed, 3200)
					TASK_PLAY_ANIM(cougar.mPed, "creatures@cougar@melee@", "growling")
					
					// At this point, blip the cougar.
					SAFE_REMOVE_BLIP(cougar.mAnimalBlip)
					SETUP_ANIMAL_SPOOKED_BLIP(cougar)
				ELSE
					IF (GET_SCRIPT_TASK_STATUS(cougar.mPed, SCRIPT_TASK_STAND_STILL) = FINISHED_TASK)
					AND (GET_SCRIPT_TASK_STATUS(cougar.mPed, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK)
						SETUP_ANIMAL_SPOOKED_BLIP(cougar)
						cougar.animalState = AS_SET_ATTACKING
						EXIT
					ENDIF
				ENDIF
			ENDIF
		
			// If a mountain lion is close when spooked, it attacks.
			IF HAS_ANIMAL_BEEN_SPOOKED(cougar) 
				cougar.animalState = AS_SET_ATTACKING
			ELSE
				// When not spooked, they'll attack when close enough, or if they're at a distance and spotted by the player.
				IF (fDistance < 14.0) OR (fDistance < 24.0 AND cougar.bAnimalSpotted)
					SETUP_ANIMAL_SPOOKED_BLIP(cougar)
					cougar.animalState = AS_SET_ATTACKING
				ENDIF
			ENDIF
		BREAK
		
		
		CASE AS_SET_ATTACKING
			CLEAR_PED_TASKS(cougar.mPed)
			TASK_COMBAT_PED(cougar.mPed, PLAYER_PED_ID())
			cougar.animalState = AS_ATTACKING
		BREAK
		
		
		CASE AS_ATTACKING
			// At this point, it's to the death.
		BREAK
		
		
		CASE AS_CLEAN_UP
		CASE AS_CLEAN_UP_DELETE
			CLEAN_UP_ANIMAL(cougar)
		BREAK 
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Processes the animal spawning, behavior and cleanup
PROC PROCESS_ANIMAL(MY_ANIMAL &animal, INT iAnimalIdx)
	FLOAT fDistance = 9999.9
			
	IF DOES_ENTITY_EXIST(animal.mPed)
		IF NOT IS_PED_INJURED(animal.mPed)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				VECTOR vAnimalPos = GET_ENTITY_COORDS(animal.mPed)
				fDistance = GET_DISTANCE_BETWEEN_COORDS(vHuntingPlayerCoords, vAnimalPos)

				IF fDistance < 45.0
					IF IS_SPHERE_VISIBLE(vAnimalPos, 1.5) AND (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM) OR fDistance < 20.0)
						animal.bAnimalSpotted = TRUE
					ELSE
						animal.bAnimalSpotted = FALSE
					ENDIF
				ELSE
					animal.bAnimalSpotted = FALSE
				ENDIF

				IF fDistance > ANIMAL_CLEANUP_DISTANCE
					IF NOT IS_SPHERE_VISIBLE(vAnimalPos, 1.5)
					OR IS_SCREEN_FADED_OUT()
						animal.animalState = AS_CLEAN_UP_DELETE
					ENDIF
				ENDIF
			ENDIF			
		ELSE
			IF (animal.animalType = ANIMAL_TYPE_COYOTE)
				FORCE_COYOTE_PACK_SCATTER(animal)
			ENDIF	
			
			animal.animalState = AS_CLEAN_UP
		ENDIF
	ENDIF
	
	MANAGE_ANIMAL_BLIP(animal, iAnimalIdx)
	
	// Handle animal behaviour
	SWITCH animal.animalState		
		// Check if the animal can spawn in
		CASE AS_CHECK_CAN_SPAWN
			// Set a default random spawn time
			IF animal.iSpawnDelayTime = -1
				animal.iSpawnDelayTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(2000, 6000)
			ENDIF
			
			// If this animal isn't delayed, or the delay timer is up, allow spawning.
			IF NOT animal.bDelayBeforeRespawn
				CPRINTLN(DEBUG_MISSION,"ES_CHECK_CAN_SPAWN - SPAWNING IN")
				animal.animalState = AS_SPAWN_IN
			ELIF GET_GAME_TIMER() > animal.iSpawnDelayTime
				animal.animalState = AS_SPAWN_IN
			ENDIF
		BREAK
		
		// Try to spawn in the animal
		CASE AS_SPAWN_IN
			// Keep trying this every frame until the entity is created.
			IF NOT DOES_ENTITY_EXIST(animal.mPed)
				IF GET_ANIMAL_SPAWN_POS(animal.vHomePosition, animal.fRandHead)
					// Choose a random type. May add weighting to this in future		
					IF bAnimalSpawn_Boar
						animal.mModel = A_C_BOAR
						animal.animalType = ANIMAL_TYPE_BOAR
					ELSE
						animal.mModel = A_C_COYOTE
						animal.animalType = ANIMAL_TYPE_COYOTE
					ENDIF
									
					// Create the animal and set it to wander a fixed area by default
					IF SETUP_PED(animal.mPed, animal.vHomePosition, GET_RANDOM_FLOAT_IN_RANGE(0.0,360.0), animal.mModel, FALSE) 
						SETUP_ANIMAL(animal, AS_IDLE, TRUE)
						SET_PED_ON_GROUND_PROPERLY(animal.mPed)
						
						bAnimalSpawn_Boar = !bAnimalSpawn_Boar
	
						IF (animal.animalType = ANIMAL_TYPE_BOAR)
							// Boar are highly perceptive, but wander.
							SET_PED_SEEING_RANGE(animal.mPed, 35.0)
							SET_PED_ID_RANGE(animal.mPed, 40.0)
							SET_PED_HEARING_RANGE(animal.mPed, 20.0)
							SET_PED_HIGHLY_PERCEPTIVE(animal.mPed, TRUE)
							//SET_PED_VISUAL_FIELD_CENTER_ANGLE
							SET_PED_VISUAL_FIELD_MIN_ANGLE(animal.mPed, -80.0)
							SET_PED_VISUAL_FIELD_MAX_ANGLE(animal.mPed, 80.0)
							//SET_BLIP_SHOW_CONE
							
							TASK_WANDER_IN_AREA(animal.mPed,animal.vHomePosition, ANIMAL_WANDER_RADIUS, ANIMAL_MIN_WAIT_TIME, ANIMAL_MAX_WAIT_TIME)
							animal.animalState = AS_IDLE
							
						ELIF (animal.animalType = ANIMAL_TYPE_COYOTE)
							// Coyotes get a pack, but wanter as well.
							IF (SPAWN_COYOTE_PACK_FOR_LEADER(iAnimalIdx) != 0)
								animal.iPackLeader = iAnimalIdx
							ENDIF
							
							TASK_WANDER_IN_AREA(animal.mPed,animal.vHomePosition, ANIMAL_WANDER_RADIUS, ANIMAL_MIN_WAIT_TIME, ANIMAL_MAX_WAIT_TIME)
							animal.animalState = AS_IDLE
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF
		BREAK

		
		CASE AS_IDLE
			IF HAS_ANIMAL_BEEN_SPOOKED(animal)
				IF (animal.animalType = ANIMAL_TYPE_COYOTE)
					FORCE_COYOTE_PACK_SCATTER(animal)
				ENDIF	
				SETUP_ANIMAL_SPOOKED_BLIP(animal)
				animal.animalState = AS_RUN_OFF
			ENDIF
		BREAK


		CASE AS_RUN_OFF
			CLEAR_PED_TASKS(animal.mPed)
			TASK_SMART_FLEE_PED(animal.mPed, PLAYER_PED_ID(), 500.0, -1, FALSE)
			SET_PED_KEEP_TASK(animal.mPed, TRUE)
			animal.animalState = AS_RUNNING_OFF
		BREAK
		
		
		CASE AS_RUNNING_OFF
		BREAK

		CASE AS_CLEAN_UP
		CASE AS_CLEAN_UP_DELETE
			CLEAN_UP_ANIMAL(animal)
		BREAK 
	ENDSWITCH
ENDPROC



PROC CLEANUP_RANDOM_ANIMALS()
	INT idx
	REPEAT MAX_RANDOM_ANIMALS idx
		SAFE_REMOVE_BLIP(mAnimal[idx].mAnimalBlip)
		mAnimal[idx].bIsSpooked = FALSE
		mAnimal[idx].bAnimalSpotted = FALSE
		mAnimal[idx].iSpawnDelayTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ANIMAL_SPAWN_DELAY_TIME_MIN, ANIMAL_SPAWN_DELAY_TIME_MAX)
						
		mAnimal[idx].bDelayBeforeRespawn = TRUE
		mAnimal[idx].animalState = AS_CHECK_CAN_SPAWN
		
		mAnimal[idx].iNextTestTime = 0
			
		IF DOES_ENTITY_EXIST(mAnimal[idx].mPed)
			SET_PED_AS_NO_LONGER_NEEDED(mAnimal[idx].mPed)
		ENDIF
	ENDREPEAT
	
	// Cougar too.
	REPEAT MAX_COUGARS idx
		SAFE_REMOVE_BLIP(mCougar[idx].mAnimalBlip)
		mCougar[idx].bIsSpooked = FALSE
		mCougar[idx].bAnimalSpotted = FALSE
		mCougar[idx].iSpawnDelayTime = GET_GAME_TIMER() + INITIAL_COUGAR_SPAWN_TIME
						
		mCougar[idx].bDelayBeforeRespawn = TRUE
		mCougar[idx].animalState = AS_CHECK_CAN_SPAWN
		
		mCougar[idx].iNextTestTime = 0
			
		IF DOES_ENTITY_EXIST(mCougar[idx].mPed)
			SET_PED_AS_NO_LONGER_NEEDED(mCougar[idx].mPed)
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_RANDOM_ANIMALS()
	// No point in doing this if the player isn't available.
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	INT idx
	REPEAT MAX_RANDOM_ANIMALS idx
		PROCESS_ANIMAL(mAnimal[idx], idx)
	ENDREPEAT
	
	
	// Handle cougars.
	REPEAT MAX_COUGARS idx
		//If this is the second cougar, we don't even care about it until the player has a gold.
		IF (idx != 0 AND Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold)
		OR (idx = 0)
			PROCESS_COUGAR(mCougar[idx], idx)
		ENDIF
	ENDREPEAT
ENDPROC


PROC HANDLE_NON_MINIGAME_KILLS()
	INT iEventIndex
	STRUCT_ENTITY_ID eventData
	MODEL_NAMES		modelName
	WEAPON_TYPE killWeapon

	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iEventIndex
		SWITCH GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iEventIndex)
			CASE EVENT_ENTITY_DESTROYED
				GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iEventIndex, eventData, SIZE_OF(eventData))

				IF NOT DOES_ENTITY_EXIST(eventData.EntityId)
					CPRINTLN(DEBUG_MISSION, "Entity was killed, but the target entity doesn't exist!")
					EXIT
				ENDIF

				IF NOT IS_ENTITY_A_PED(eventData.EntityId)
					CPRINTLN(DEBUG_MISSION, "Entity was killed, but is not a ped!")
					EXIT
				ENDIF
				
				IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(eventData.EntityId, PLAYER_PED_ID() )
					CPRINTLN(DEBUG_MISSION, "Ped was killed, but not by player")
					EXIT
				ENDIF
				modelName =	GET_ENTITY_MODEL(eventData.EntityId)
				IF NOT GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), killWeapon)
					killWeapon = WEAPONTYPE_INVALID
				ENDIF
				SWITCH modelName		
					CASE A_C_CHICKENHAWK
					CASE A_C_CORMORANT
					CASE A_C_CROW
					CASE A_C_PIGEON
					CASE A_C_SEAGULL
						ADD_CORPSE_WITH_LOCATION(eventData.EntityId, KILL_BIRD_KILL, killWeapon)
						CHECK_WEAPONS_MASTER_CHALLENGE_FOR_ANIMAL(Player_Hunt_Data, eventData.EntityId)
					BREAK
					DEFAULT
						IF IS_PED_HUMAN(GET_PED_INDEX_FROM_ENTITY_INDEX(eventData.EntityId))
							ADD_CORPSE_WITH_LOCATION(eventData.EntityId, KILL_HUMAN_KILL, killWeapon)
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

