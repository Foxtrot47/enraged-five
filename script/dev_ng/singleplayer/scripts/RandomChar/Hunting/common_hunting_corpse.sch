USING "globals.sch"
USING "common_hunting.sch"


FUNC BOOL IS_ANIMAL_IN_PHOTO(ENTITY_INDEX entAnimal)
	IF NOT DOES_ENTITY_EXIST(entAnimal)
		RETURN FALSE
	ENDIF

	FLOAT  fScreenX, fScreenY
	VECTOR vEntPos = GET_ENTITY_COORDS(entAnimal, FALSE)
	FLOAT fDist2 = VDIST2(vEntPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	FLOAT fBaseDist2 = 100.0
	IF NOT DECOR_EXIST_ON(entAnimal,  "hunt_score")
		fBaseDist2 = 400.0
	ENDIF
	
	// Test to see if he's visible.
	
	IF (fDist2 < (fBaseDist2 * Player_Hunt_Data.fLastCamZoom * Player_Hunt_Data.fLastCamZoom))
	AND IS_SPHERE_VISIBLE(vEntPos, 0.5)
	AND GET_SCREEN_COORD_FROM_WORLD_COORD(vEntPos, fScreenX, fScreenY)
		IF (fScreenX > 0.25 AND fScreenX < 0.75 AND fScreenY > 0.25 AND fScreenY < 0.75)
			CDEBUG1LN(DEBUG_HUNTING, "Dist: ", fDist2, " ScreenX: ", fScreenX, " -- ScreenY: ", fScreenY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_PHOTOJOURNALIST_CHALLENGE_FOR_PHOTO(HT_PLAYER_DATA &huntingData)
	INT iNumInPhoto =  GET_ITEMSET_SIZE(huntingData.setPhoto)
	IF iNumInPhoto = 0
		RETURN FALSE
	ENDIF
	
	BOOL bElk1, bElk2, bCoyote, bCougar, bBoar
	INT iCorpseIndex
	ITEM_INDEX itemCorpse
	ENTITY_INDEX entCorpse
	REPEAT iNumInPhoto iCorpseIndex
		itemCorpse = GET_INDEXED_ITEM_IN_ITEMSET(iCorpseIndex, huntingData.setPhoto)
		
		IF IS_AN_ENTITY(itemCorpse)
			entCorpse = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemCorpse))
			KILL_TYPE eKillType
			IF DECOR_EXIST_ON(itemCorpse,  "hunt_score")
				eKillType = INT_TO_ENUM(KILL_TYPE, DECOR_GET_INT(itemCorpse, "hunt_score"))
			ELSE
				eKillType = KILL_LIVE_ANIMAL
			ENDIF
			
			CPRINTLN(DEBUG_MISSION, "Photo subject:", GET_DISPLAY_NAME_FOR_KILL_TYPE(entCorpse, eKillType))//, " killed with ", GET_DISPLAY_NAME_FOR_KILL_WEAPON())
			SWITCH huntingData.challengeData[HC_PHOTOJOURNALIST].hcRank
				CASE HCR_RANK_1
					IF eKillType = KILL_LIVE_ANIMAL
						IF IS_ELK_A_DOE(entCorpse)
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE HCR_RANK_2
					IF eKillType <= KILL_SPOOKED_SHOT // Anything spooked or below is an elk
						IF NOT bElk1
							bElk1 = TRUE
						ELSE
							bElk2 = TRUE
						ENDIF
					ENDIF
					IF bElk1 AND bElk2
						RETURN TRUE
					ENDIF
				BREAK
				CASE HCR_RANK_3
					IF eKillType = KILL_COYOTE_KILL 
						bCoyote = TRUE
					ELIF eKillType = KILL_BOAR_KILL 
						bBoar = TRUE
					ENDIF
					IF bCoyote AND bBoar
						RETURN TRUE
					ENDIF
				BREAK
				CASE HCR_RANK_4
					IF eKillType <= KILL_SPOOKED_SHOT // Anything spooked or below is an elk
						bElk1 = TRUE
					ELIF eKillType = KILL_LIVE_ANIMAL AND GET_ENTITY_MODEL(entCorpse) = A_C_MTLION
						bCougar = TRUE
					ENDIF
					IF bElk1 AND bCougar
						RETURN TRUE
					ENDIF
				BREAK
				CASE HCR_RANK_5
					IF eKillType <= KILL_SPOOKED_SHOT // Anything spooked or below is an elk
						bElk1 = TRUE
					ELIF eKillType = KILL_MOUNTAIN_LION_KILL 
						bCougar = TRUE
					ELIF eKillType = KILL_COYOTE_KILL 
						bCoyote = TRUE
					ELIF eKillType = KILL_BOAR_KILL 
						bBoar = TRUE
					ENDIF
					IF bElk1 AND bCougar AND bCoyote AND bBoar
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


PROC CLEAN_ALL_CORPSES()
	ITEM_INDEX itemTemp
	ENTITY_INDEX entTemp
	BLIP_INDEX blipTemp
	WHILE GET_ITEMSET_SIZE(Player_Hunt_data.setCorpses)>0
		itemTemp = GET_INDEXED_ITEM_IN_ITEMSET(0, Player_Hunt_data.setCorpses)
		REMOVE_FROM_ITEMSET(itemTemp, Player_Hunt_data.setCorpses)
		IF IS_AN_ENTITY(itemTemp)
			entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemTemp))
			blipTemp = GET_BLIP_FROM_ENTITY(entTemp)
			REMOVE_BLIP( blipTemp)
			SET_ENTITY_AS_NO_LONGER_NEEDED(entTemp)
		ENDIF	
		CLEAN_ITEMSET(Player_Hunt_data.setCorpses)
	ENDWHILE

	WHILE GET_ITEMSET_SIZE(Player_Hunt_data.setPhoto) > 0
		itemTemp = GET_INDEXED_ITEM_IN_ITEMSET(0, Player_Hunt_data.setPhoto)
		REMOVE_FROM_ITEMSET(itemTemp, Player_Hunt_data.setPhoto)
		IF IS_AN_ENTITY(itemTemp)
			entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemTemp))
			blipTemp = GET_BLIP_FROM_ENTITY(entTemp)
			REMOVE_BLIP( blipTemp)
			SET_ENTITY_AS_NO_LONGER_NEEDED(entTemp)
		ENDIF	
		CLEAN_ITEMSET(Player_Hunt_data.setPhoto)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Maintains the looting itemset and update data.
PROC CORPSE_MANAGER_UPDATE()
	
	IF NOT IS_ITEMSET_VALID(Player_Hunt_data.setCorpses)
//		CERRORLN(DEBUG_LOOTING, "LOOTING_MANAGER_UPDATE - Corpse itemset doesn't exist! Looting system fail.")
		EXIT
	ENDIF
	
	
	//Need some variableage.
//	INT iCount
	VECTOR vPlayerPos
	FLOAT fDist2
	ITEM_INDEX itemTemp
	ENTITY_INDEX entTemp
	BOOL bWasInRange
	TEXT_LABEL txtMsg

	INT itemIndex = 0
	enumCharacterList charTextedPhoto
	SWITCH Player_Hunt_data.eCorpseState
		CASE CS_UPDATE
			IF IS_CELLPHONE_CAMERA_IN_USE()
				Player_Hunt_Data.fLastCamZoom = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()
				CDEBUG1LN(DEBUG_HUNTING, "CS_UPDATE -> CS_TAKING_PHOTO")
				Player_Hunt_data.eCorpseState = CS_TAKING_PHOTO
			ELIF Player_Hunt_Data.iPhotosSent = 0 AND NOT Player_Hunt_data.bSentPhotoHelp
				IF GET_ITEMSET_SIZE(Player_Hunt_data.setCorpses) > 0
					IF NOT IS_TIMER_STARTED(Player_Hunt_data.textTimer)
						START_TIMER_NOW(Player_Hunt_data.textTimer)
					ELIF NOT IS_HELP_MESSAGE_ON_SCREEN()
						IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_data.textTimer, 1.5)
							PRINT_HELP("AHT_PHOTO")
							Player_Hunt_data.bSentPhotoHelp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			bWasInRange = Player_Hunt_data.bNearCorpse
			Player_Hunt_data.bNearCorpse = FALSE
			
		//	PRINTLN("CorpseSet tracking ", GET_ITEMSET_SIZE(Player_Hunt_data.setCorpses), " corpses")
			REPEAT GET_ITEMSET_SIZE(Player_Hunt_data.setCorpses) itemIndex
				itemTemp = GET_INDEXED_ITEM_IN_ITEMSET(itemIndex, Player_Hunt_data.setCorpses)
				IF IS_AN_ENTITY(itemTemp)
					entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemTemp))
					
					SET_ENTITY_AS_MISSION_ENTITY(entTemp)
					
					// Assert blocking.
					IF NOT IS_ENTITY_DEAD(entTemp)
						EXIT
					ENDIF

					//PRINTLN("Corpse ", NATIVE_TO_INT(entTemp), ": dist ", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(entTemp, FALSE)))
					fDist2 = VDIST2(GET_ENTITY_COORDS(entTemp, FALSE), vPlayerPos)
					
					IF fDist2 < 25.0 // 5x5
						Player_Hunt_data.bNearCorpse = TRUE
						IF Player_Hunt_Data.iPhotosSent = 0 AND NOT Player_Hunt_data.bSentPhotoNearHelp
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AHT_PHOTO")
								PRINT_HELP("AHT_PHOTO_NEAR")
							ENDIF
							Player_Hunt_data.bSentPhotoNearHelp = TRUE
						ENDIF
					ENDIF
					
					// If it JSUT died, wait a bit before continuing. If it's been long enough since this animal died, add flies.
					BOOL bAllowCorpseUpdate
					bAllowCorpseUpdate = TRUE
					IF DECOR_EXIST_ON(entTemp, "hunt_kill_time")
						INT iTime
						iTime = DECOR_GET_INT(entTemp, "hunt_kill_time")
						
						// Don't continue if this corpse is recently dead.
						IF GET_GAME_TIMER() < iTime + 250
							bAllowCorpseUpdate = FALSE
						ENDIF
						
						// Add flies.
						IF bAllowCorpseUpdate
							IF GET_GAME_TIMER() > iTime + 20000
								// Add flies, remove decor.
								START_PARTICLE_FX_LOOPED_ON_ENTITY("ent_amb_insect_plane", entTemp, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
								DECOR_REMOVE(entTemp, "hunt_kill_time")
							ENDIF
						ENDIF
					ENDIF
					
					// Add a blip to this corpse if it doesn't have one.
					IF bAllowCorpseUpdate
						IF NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(entTemp))
							CDEBUG3LN(DEBUG_HUNTING, "This corpse isn't blipped. Adding a blip now: ", NATIVE_TO_INT(entTemp))
							DEBUG_PRINTCALLSTACK()
							
							BLIP_INDEX blipTemp
							blipTemp = ADD_BLIP_FOR_ENTITY(entTemp)
							SET_BLIP_PRIORITY(blipTemp, BLIPPRIORITY_MED)
							SET_BLIP_SPRITE(blipTemp, RADAR_TRACE_DEAD)
							SET_BLIP_NAME_FROM_TEXT_FILE(blipTemp, "HUNT_KILL_BLIP")
							SET_BLIP_ALPHA(blipTemp, 75)
						ENDIF
					ENDIF
				ELSE
					CLOGLN(DEBUG_MISSION,CHANNEL_SEVERITY_ASSERT,  "We lost a corpse? ", NATIVE_TO_INT(itemTemp))
				ENDIF
			ENDREPEAT
			IF bWasInRange != Player_Hunt_data.bNearCorpse
				Player_Hunt_data.bSetupHuntingControls = FALSE
			ENDIF
		BREAK
		
			
		CASE CS_TAKING_PHOTO
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
			IF GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR() > 1.0
				Player_Hunt_Data.fLastCamZoom = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()
			ENDIF
			
			IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
			AND (Player_Hunt_data.iLastPhotoTime < GET_GAME_TIMER())
				// Don't take these photos rapid fire...
				Player_Hunt_data.iLastPhotoTime = GET_GAME_TIMER() + HUNTING_PHOTO_CATCH_DELAY
				
				CDEBUG1LN(DEBUG_HUNTING, "PHOTO CAUGHT AS TAKEN!")
				
				// Figure out the score for this picture
				bWasInRange = FALSE
				REPEAT GET_ITEMSET_SIZE(Player_Hunt_data.setCorpses) itemIndex
					itemTemp = GET_INDEXED_ITEM_IN_ITEMSET(itemIndex, Player_Hunt_data.setCorpses)
					IF IS_AN_ENTITY(itemTemp)
						entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemTemp))
						IF IS_ANIMAL_IN_PHOTO(entTemp)
							BLIP_INDEX blipTemp
							blipTemp = GET_BLIP_FROM_ENTITY(entTemp)
							REMOVE_BLIP(blipTemp)
							IF NOT IS_IN_ITEMSET(entTemp, Player_Hunt_data.setPhoto)
								ADD_TO_ITEMSET(entTemp, Player_Hunt_data.setPhoto)
							ENDIF
							bWasInRange = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				// Collect any live elk
				REPEAT COUNT_OF(mElk) itemIndex
					IF IS_ANIMAL_IN_PHOTO(mElk[itemIndex].mPed)
						IF NOT IS_IN_ITEMSET(mElk[itemIndex].mPed, Player_Hunt_data.setPhoto)
							ADD_TO_ITEMSET(mElk[itemIndex].mPed, Player_Hunt_data.setPhoto)
						ENDIF
						bWasInRange = TRUE
					ENDIF
				ENDREPEAT
				
				// Collect any live boars and coyotes
				REPEAT COUNT_OF(mAnimal) itemIndex
					IF IS_ANIMAL_IN_PHOTO(mAnimal[itemIndex].mPed)
						IF NOT IS_IN_ITEMSET(mAnimal[itemIndex].mPed, Player_Hunt_data.setPhoto)
							ADD_TO_ITEMSET(mAnimal[itemIndex].mPed, Player_Hunt_data.setPhoto)
						ENDIF
						bWasInRange = TRUE
					ENDIF
				ENDREPEAT
				
				// Collect any live mountain lions
				REPEAT COUNT_OF(mCougar) itemIndex
					IF IS_ANIMAL_IN_PHOTO(mCougar[itemIndex].mPed)
						IF NOT IS_IN_ITEMSET(mCougar[itemIndex].mPed, Player_Hunt_data.setPhoto)
							ADD_TO_ITEMSET(mCougar[itemIndex].mPed, Player_Hunt_data.setPhoto)
						ENDIF
						bWasInRange = TRUE
					ENDIF
				ENDREPEAT				
				
				IF bWasInRange
					IF Player_Hunt_Data.iPhotosSent = 0 AND NOT Player_Hunt_data.bSentPhotoSendHelp
						PRINT_HELP("AHT_PHOTO_SEND")
						Player_Hunt_data.bSentPhotoSendHelp = TRUE
					ENDIF
					ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
					
					CDEBUG1LN(DEBUG_HUNTING, "CS_TAKING_PHOTO -> CS_POST_PHOTO")
					Player_Hunt_data.eCorpseState = CS_POST_PHOTO
				ELSE
					CDEBUG1LN(DEBUG_HUNTING, "CS_TAKING_PHOTO -> CS_UPDATE")
					Player_Hunt_data.eCorpseState = CS_UPDATE
				ENDIF
			ELIF CHECK_FOR_APPLICATION_EXIT()
				CDEBUG1LN(DEBUG_HUNTING, "APP EXIT: CS_TAKING_PHOTO -> CS_UPDATE")
				Player_Hunt_data.eCorpseState = CS_UPDATE
			ENDIF
		BREAK
		
		CASE CS_POST_PHOTO
			// From here, we can either send, or continue, and continue takes you back to the camera...
			IF IS_CONTACTS_LIST_ON_SCREEN()
				CDEBUG1LN(DEBUG_HUNTING, "CS_POST_PHOTO -> CS_CONTACT_LIST")
				Player_Hunt_data.eCorpseState = CS_CONTACT_LIST
				
			ELIF Is_Phone_Control_Just_Pressed(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
				// Going back to phone camera
				BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
				
				CDEBUG1LN(DEBUG_HUNTING, "CS_POST_PHOTO -> CS_TAKING_PHOTO")
				Player_Hunt_data.eCorpseState = CS_TAKING_PHOTO
			ENDIF
		BREAK
		
		CASE CS_CONTACT_LIST
			IF HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT()
				HANG_UP_AND_PUT_AWAY_PHONE()
				BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
				START_TIMER_NOW(Player_Hunt_data.textTimer)
				IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_HUNTER)
					Player_Hunt_Data.iPhotosSent++
					CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_HUNTER)
					
					CDEBUG1LN(DEBUG_HUNTING, "CS_CONTACT_LIST -> CS_WAIT_MSG")
					Player_Hunt_data.eCorpseState = CS_WAIT_MSG
				ELSE
					CDEBUG1LN(DEBUG_HUNTING, "CS_CONTACT_LIST -> CS_WAIT_MSG_WRONG")
					Player_Hunt_data.eCorpseState = CS_WAIT_MSG_WRONG
				ENDIF
			ELIF NOT IS_CONTACTS_LIST_ON_SCREEN()
				BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
				WHILE (GET_ITEMSET_SIZE(Player_Hunt_data.setPhoto) > 0)
					REMOVE_FROM_ITEMSET(GET_INDEXED_ITEM_IN_ITEMSET(0, Player_Hunt_data.setPhoto), Player_Hunt_data.setPhoto)
				ENDWHILE
				
				CDEBUG1LN(DEBUG_HUNTING, "CS_CONTACT_LIST -> CS_UPDATE")
				Player_Hunt_data.eCorpseState = CS_UPDATE
			ENDIF
		BREAK
		CASE CS_WAIT_MSG_WRONG
			IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_data.textTimer, 3.5)
				WHILE (GET_ITEMSET_SIZE(Player_Hunt_data.setPhoto) > 0)
					REMOVE_FROM_ITEMSET(GET_INDEXED_ITEM_IN_ITEMSET(0, Player_Hunt_data.setPhoto), Player_Hunt_data.setPhoto)
				ENDWHILE
				
				INT iMaxCharacter
				iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
				REPEAT iMaxCharacter charTextedPhoto
					IF g_CharacterSheetNonSaved[charTextedPhoto].phone != NO_PHONE_CONTACT
					AND HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(charTextedPhoto)
						txtMsg = GET_TEXT_MESSAGE_FOR_WRONG_CONTACT(charTextedPhoto)
						IF NOT IS_STRING_NULL_OR_EMPTY(txtMsg)
							SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(charTextedPhoto, txtMsg, TXTMSG_UNLOCKED)
							//DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(txtMsg)
						ENDIF
						CLEAR_CONTACT_PICTURE_MESSAGE(charTextedPhoto)
					ENDIF
				ENDREPEAT	
				START_TIMER_NOW(Player_Hunt_data.textTimer)
				
				CDEBUG1LN(DEBUG_HUNTING, "CS_WAIT_MSG_WRONG -> CS_WAIT_MSG_WRONG_POST")
				Player_Hunt_data.eCorpseState = CS_WAIT_MSG_WRONG_POST
			ENDIF
		BREAK
		CASE CS_WAIT_MSG_WRONG_POST
			IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_data.textTimer, 0.5)
				CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_TREVOR)
				
				CDEBUG1LN(DEBUG_HUNTING, "CS_WAIT_MSG_WRONG_POST -> CS_UPDATE")
				Player_Hunt_data.eCorpseState = CS_UPDATE
			ENDIF
		BREAK
		
		CASE CS_WAIT_MSG
			IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_data.textTimer, 3.5)
				CDEBUG1LN(DEBUG_HUNTING, "CS_WAIT_MSG -> CS_DO_SCORE_AND_CHALLENGES")
				Player_Hunt_data.eCorpseState = CS_DO_SCORE_AND_CHALLENGES
				
				KILL_TYPE bestKill, thisKill
				WEAPON_TYPE	bestWeapon
				bestKill = KILL_NO_KILL
				bestWeapon = WEAPONTYPE_INVALID
				HUNTING_CHALLENGE hcUnlock
				hcUnlock = HC_INVALID
				ITEM_INDEX thisCorpse, bestCorpse
				INT iMoney
				// Sort for the best kill in the photo
				REPEAT GET_ITEMSET_SIZE(Player_Hunt_data.setPhoto) itemIndex
					thisCorpse = GET_INDEXED_ITEM_IN_ITEMSET(itemIndex, Player_Hunt_data.setPhoto)
					
					IF IS_AN_ENTITY(thisCorpse)
					
						IF DECOR_EXIST_ON(thisCorpse,  "hunt_score")
							thisKill = INT_TO_ENUM(KILL_TYPE, DECOR_GET_INT(thisCorpse, "hunt_score"))
						
							IF bestWeapon = WEAPONTYPE_INVALID 
								IF DECOR_EXIST_ON(thisCorpse, "hunt_weapon")
									bestWeapon = INT_TO_ENUM(WEAPON_TYPE, DECOR_GET_INT(thisCorpse, "hunt_weapon"))
								ENDIF
							ENDIF
						ELSE
							thisKill = KILL_LIVE_ANIMAL
							bestWeapon = WEAPONTYPE_INVALID
						ENDIF
						IF thisKill < bestKill
							bestKill = thisKill
							bestCorpse = thisCorpse
						ENDIF
						IF IS_AN_ENTITY(thisCorpse)
							entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(thisCorpse))
							iMoney += DO_CORPSE_PAYMENT(entTemp)
						ENDIF
					ENDIF
				ENDREPEAT
				
				SWITCH bestKill
					CASE KILL_HEART_SHOT
						IF Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank = HCR_LOCKED
							hcUnlock = HC_MASTER_HUNTER
						ENDIF
					BREAK
					CASE KILL_SPOOKED_SHOT			
					CASE KILL_ROAD_KILL				
					CASE KILL_MOUNTAIN_LION_KILL	
					CASE KILL_BOAR_KILL				
					CASE KILL_COYOTE_KILL
						IF Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank = HCR_LOCKED
							hcUnlock = HC_WEAPONS_MASTER
						ENDIF
					BREAK
					DEFAULT
						IF Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank = HCR_LOCKED AND hcUnlock = HC_INVALID
							hcUnlock = HC_PHOTOJOURNALIST
						ENDIF
					BREAK
				ENDSWITCH

				IF hcUnlock != HC_INVALID
					Player_Hunt_Data.challengeData[hcUnlock].hcStatus = HCS_RANK_NEXT
				ENDIF
				
				IF CHECK_PHOTOJOURNALIST_CHALLENGE_FOR_PHOTO(Player_Hunt_Data)
					txtMsg = GET_TEXT_MESSAGE_FOR_PHOTO_RANK(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank)
					
					// Need to add a little bit of money because we didn't kill anything here, and we want to get something.
					iMoney += 25
				ELSE
					txtMsg = GET_TEXT_MESSAGE_FOR_KILL(bestCorpse, bestKill, bestWeapon)
				ENDIF
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_HUNTER, txtMsg, TXTMSG_UNLOCKED, "", iMoney, "", NUMBER_COMPONENT)
				START_TIMER_NOW(Player_Hunt_data.textTimer)
			ENDIF
		BREAK
		CASE CS_DO_SCORE_AND_CHALLENGES
			IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_data.textTimer, 1.5)
				IF CHECK_PHOTOJOURNALIST_CHALLENGE_FOR_PHOTO(Player_Hunt_data)
					Player_Hunt_Data.bChallengeScreenSetup = FALSE
					Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcStatus = HCS_RANK_COMPLETE_TOAST
				ENDIF
				
				// We do this check a bit different, because removing seem to re-index things
				WHILE (itemIndex < GET_ITEMSET_SIZE(Player_Hunt_data.setPhoto))
					itemTemp = GET_INDEXED_ITEM_IN_ITEMSET(itemIndex, Player_Hunt_data.setPhoto)
					REMOVE_FROM_ITEMSET(itemTemp, Player_Hunt_data.setPhoto)
					IF IS_IN_ITEMSET(itemTemp, Player_Hunt_data.setCorpses)
						REMOVE_FROM_ITEMSET(itemTemp, Player_Hunt_data.setCorpses)
					ENDIF
					IF IS_AN_ENTITY(itemTemp)
						entTemp = INT_TO_NATIVE(ENTITY_INDEX, NATIVE_TO_INT(itemTemp))
						DO_CORPSE_SCORING(entTemp)
						IF DECOR_EXIST_ON(entTemp,  "hunt_score") // Only release corpses
							SET_ENTITY_AS_NO_LONGER_NEEDED(entTemp)
						ENDIF
					ENDIF	
				ENDWHILE
				CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_TREVOR)
				CLEAN_ITEMSET(Player_Hunt_data.setPhoto)
				CLEAN_ITEMSET(Player_Hunt_data.setCorpses)
				
				CDEBUG1LN(DEBUG_HUNTING, "CS_DO_SCORE_AND_CHALLENGES -> CS_UPDATE")
				Player_Hunt_data.eCorpseState = CS_UPDATE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

