USING "minigame_uiinputs.sch"
USING "hud_drawing.sch"
USING "screens_header.sch"
USING "UIUtil.sch"
USING "script_oddjob_funcs.sch"
USING "script_usecontext.sch"
USING "common_Hunting.sch"
USING "commands_stats.sch"
USING "end_screen.sch"
USING "common_hunting_leaderboards.sch"

// HUNTING LAUNCHER
ENUM HT_SCREEN_TEXT
	HT_MENU_MAIN_TITLE,
	HT_SELECTION_TITLE,
	HT_DESCRIPTION_TITLE,
	HT_AWARDS_TITLE,
	HT_DESCRIPTION_SUBTITLE,
	HT_AWARDS_SUBTITLE,
	HT_AWARDS_VALUE,
	HT_BEST_SUBTITLE,
	HT_BEST_TEXT,
	HT_MEDAL_SUBTITLE_1,
	HT_MEDAL_SUBTITLE_2,
	HT_MEDAL_SUBTITLE_3,
	HT_MEDAL_VALUE_1,
	HT_MEDAL_VALUE_2,
	HT_MEDAL_VALUE_3,
	HT_DESCRIPTION_INFO_TEXT,
	HT_SELECTION_ITEM_TITLE_1,
	HT_SELECTION_ITEM_TITLE_2,
	HT_SELECTION_ITEM_TITLE_3,
	HT_SELECTION_ITEM_TITLE_4
ENDENUM

ENUM HT_SCREEN_SPRITE
	HT_SECONDARY_BACKGROUND,
	HT_SELECTION_ITEM_SPRITE_1,
	HT_SELECTION_ITEM_SPRITE_2,
	HT_SELECTION_ITEM_SPRITE_3,
	HT_SELECTION_ITEM_SPRITE_4,
	HT_DESCRIPTION_IMAGE_SPRITE,
	HT_DESCRIPTION_INFO_SPRITE,
	HT_AWARDS_IMAGE_SPRITE,
	HT_AWARDS_MEDAL_SPRITE_1,
	HT_AWARDS_MEDAL_SPRITE_2,
	HT_AWARDS_MEDAL_SPRITE_3
ENDENUM

ENUM HT_SCREEN_RECT
	HT_SELECTION_BACKGROUND,
	HT_DESCRIPTION_BACKGROUND,
	HT_AWARDS_BACKGROUND,
	HT_SELECTION_EDGING,
	HT_DESCRIPTION_EDGING,
	HT_AWARDS_EDGING,
	HT_SELECTION_IMAGE_BACKGROUD,
	HT_DESCRIPTION_IMAGE_BACKGROUND,
	HT_AWARDS_IMAGE_BACKGROUND,
	HT_SELECTION_ITEM_BG_1,
	HT_SELECTION_ITEM_BG_2,
	HT_SELECTION_ITEM_BG_3,
	HT_SELECTION_ITEM_BG_4,
	HT_AWARDS_ITEM_BG_1,
	HT_AWARDS_ITEM_BG_2,
	HT_AWARDS_MEDAL_BG_1,
	HT_AWARDS_MEDAL_BG_2,
	HT_AWARDS_MEDAL_BG_3,
	HT_AWARDS_MEDAL_SUB_1,
	HT_AWARDS_MEDAL_SUB_2,
	HT_AWARDS_MEDAL_SUB_3,
	HT_DESCRIPTION_1_TITLE_BG,
	HT_DESCRIPTION_1_INFO_BG
ENDENUM

// HUNTING RESULTS
ENUM HTER_ENDSCREEN_TEXT
	HTER_MAIN_TITLE,
	HTER_SCORE_TITLE,
	HTER_DYNAMIC_ROW_LABEL_0,
	HTER_DYNAMIC_ROW_LABEL_1,
	HTER_DYNAMIC_ROW_LABEL_2,
	HTER_DYNAMIC_ROW_LABEL_3,
	HTER_DYNAMIC_ROW_LABEL_4,
	HTER_DYNAMIC_ROW_VALUE_0,
	HTER_DYNAMIC_ROW_VALUE_1,
	HTER_DYNAMIC_ROW_VALUE_2,
	HTER_DYNAMIC_ROW_VALUE_3,
	HTER_DYNAMIC_ROW_VALUE_4,
	HTER_MEDAL_BRONZE_SUB_TEXT,
	HTER_MEDAL_SILVER_SUB_TEXT,
	HTER_MEDAL_GOLD_SUB_TEXT,
	HTER_MEDAL_BRONZE_SUB_TIME,
	HTER_MEDAL_SILVER_SUB_TIME,
	HTER_MEDAL_GOLD_SUB_TIME,
	HTER_LAST_TEXT
ENDENUM

ENUM HTER_ENDSCREEN_SPRITE
	HTER_MAIN_BACKGROUND,
	HTER_SCORE_IMAGE_SPRITE,
	HTER_AWARD_ROW_SPRITE,
	HTER_MEDAL_BRONZE_SPRITE,
	HTER_MEDAL_SILVER_SPRITE,
	HTER_MEDAL_GOLD_SPRITE,
	HTER_LAST_SPRITE
ENDENUM

ENUM HTER_ENDSCREEN_RECT
	HTER_MAIN_RECT,
	HTER_SCORE_TITLE_RECT,
	HTER_SCORE_TITLE_EDGE_RECT,
	HTER_SCORE_IMAGE_RECT,
	HTER_DYNAMIC_ROW_RECT_0,
	HTER_DYNAMIC_ROW_RECT_1,
	HTER_DYNAMIC_ROW_RECT_2,
	HTER_DYNAMIC_ROW_RECT_3,
	HTER_DYNAMIC_ROW_RECT_4,
	HTER_MEDAL_BRONZE_RECT,
	HTER_MEDAL_SILVER_RECT,
	HTER_MEDAL_GOLD_RECT,
	HTER_MEDAL_BRONZE_SUB_RECT,
	HTER_MEDAL_SILVER_SUB_RECT,
	HTER_MEDAL_GOLD_SUB_RECT,
	HTER_LAST_RECT
ENDENUM

CONST_INT HUNT_SCORE_TIME_BRONZE(10)
CONST_INT HUNT_SCORE_TIME_SILVER(20)
CONST_INT HUNT_SCORE_TIME_GOLD(30)

CONST_INT HUNT_SCORE_KILL_BRONZE(10)
CONST_INT HUNT_SCORE_KILL_SILVER(20)
CONST_INT HUNT_SCORE_KILL_GOLD(30)

CONST_INT HUNT_SCORE_STEALTH_BRONZE(20)
CONST_INT HUNT_SCORE_STEALTH_SILVER(40)
CONST_INT HUNT_SCORE_STEALTH_GOLD(60)

CONST_INT HUNT_SCORE_FREE_BRONZE(30)
CONST_INT HUNT_SCORE_FREE_SILVER(60)
CONST_INT HUNT_SCORE_FREE_GOLD(90)

SIMPLE_USE_CONTEXT		resultsInstructions


// ==========================
// CONSTS
// ==========================
CONST_INT UI_QUIT_TRANS_TIME	1000

// ==========================
// VARIABLES 
// ==========================

BOOL bQuittingMG			= FALSE
INT iQuittingTimer 			= -1
BOOL bIsWaitingToQuit		= FALSE
BOOL bWaitingEndScreen		= FALSE		//Turns true when the pass screen is doing the transition_out

// Delaying the pad input for the results screen (so we don't unintentionally skip it if we're sprinting)
CONST_INT INPUT_DELAY_TIME	1000
BOOL bGotInputDelayTime		= FALSE
INT iInputDelayTime

BOOL bDisplayLeaderboard	= FALSE
BOOL bDisplayProfileButton	= FALSE
// ===========================
//  FUNCTIONS
// ===========================

/// PURPOSE:
///    Accessor. Sets the quitting timer
PROC HUNTING_SET_QUITTING_TIMER( INT iNewTimer )
	iQuittingTimer = iNewTimer
	CDEBUG3LN(DEBUG_HUNTING, "HUNTING_SET_QUITTING_TIMER set to ", iQuittingTimer)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Accessor. Gets the quitting timer. Used for transitions
FUNC INT HUNTING_GET_QUITTING_TIMER()
	RETURN iQuittingTimer
ENDFUNC


// Set up the button prompts for the results screen
PROC INIT_RESULT_SCREEN_BUTTONS(BOOL bDisableRetry = FALSE)
	CPRINTLN(DEBUG_MISSION, "INIT_RESULT_SCREEN_BUTTONS ")
	INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
	ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	IF NOT bDisableRetry
		ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
	ENDIF
	ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HLEADR",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
	SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
ENDPROC

PROC SET_UP_SCORES()
	Player_Hunt_Data.iScoreForBronze 	= HUNT_SCORE_FREE_BRONZE	
	Player_Hunt_Data.iScoreForSilver 	= HUNT_SCORE_FREE_SILVER	
	Player_Hunt_Data.iScoreForGold 		= HUNT_SCORE_FREE_GOLD
ENDPROC

/// PURPOSE:
///    Assigns the globally saved best times/killcounts to the local vars used to render the 
///    correct medals on the mode select and results screens.
PROC GRAB_GLOBAL_STATS()
	IF g_savedGlobals.sAmbient.iHighScoreFreeMode > 0
		//CPRINTLN(DEBUG_MISSION,"Best Free Mode score from global is", g_SavedGlobals.sAmbient.iHighScoreFreeMode)
		Player_Hunt_Data.iHighScore = g_SavedGlobals.sAmbient.iHighScoreFreeMode
	ENDIF
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_SELECT()
	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_BACK()
	PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
ENDPROC

/// PURPOSE:
///    Setup the hunting results screen.
PROC INIT_HUNTING_RESULTS_SCREEN(END_SCREEN_DATASET & resultsScreen, BOOL bPlayerDead = FALSE)
	RESET_ENDSCREEN(resultsScreen)
	
	// Draw the title, and the reason hunting ended.
	IF bPlayerDead
		SET_ENDSCREEN_DATASET_HEADER(resultsScreen, "RESPAWN_W", "HUNT_RESULT")
	ELSE
		// If the player has at least a bronze, he passed...
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForBronze
			SET_ENDSCREEN_DATASET_HEADER(resultsScreen, "REPLAY_PAS", "HUNT_RESULT")
		ELSE
			Player_Hunt_Data.bNeedLoserSound = TRUE
			SET_ENDSCREEN_DATASET_HEADER(resultsScreen, "REPLAY_TMG", "HUNT_RESULT")
		ENDIF
	ENDIF
	
	// If we don't set this, the results screen shrinks.
	resultsScreen.bHoldOnEnd = TRUE

	INT iItemsDrawing = 0
	
	// ADD THE MUST DRAW ITEMS HERE.
	// ============================
	// Draw the score.
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_SCRE", "", Player_Hunt_Data.iScore, 0, ESCM_NO_MARK)	

	// Time Hunted.
	FLOAT fTime = GET_TIMER_IN_SECONDS(Player_Hunt_Data.tTimeSpentHunting)
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_TIME_H_M_S, "AHT_TIME", "", ROUND(fTime * 1000.0), 0, ESCM_NO_MARK)
	
	// Photos sent.
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_PHOTOTAKEN", "", Player_Hunt_Data.iPhotosSent, 0, ESCM_NO_MARK)

	// Deer killed.
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_ELK_KILLS", "", Player_Hunt_Data.iKillCount - (Player_Hunt_Data.iCougarKills + Player_Hunt_Data.iBoarKills + Player_Hunt_Data.iCoyoteKills), 0, ESCM_NO_MARK)
	
	// Money earned.
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_DOLLAR_VALUE, "AHT_MONEYEARN", "", Player_Hunt_Data.iMoney, 0, ESCM_NO_MARK)
	iItemsDrawing = 5
	
	
	// ADD THE POSSIBLE DRAW ITEMS HERE. -- these are items that may or may not draw ddepending on how interesting they were.
	// ============================
	IF (Player_Hunt_Data.iCougarKills > 2)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_COU_KILLS", "", Player_Hunt_Data.iCougarKills, 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iBoarKills > 4)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_BOAR_KILLS", "", Player_Hunt_Data.iBoarKills, 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iCoyoteKills > 5)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_COY_KILLS", "", Player_Hunt_Data.iCoyoteKills, 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	
	// Hits zones hit.
	IF (Player_Hunt_Data.iHitsOnLocation[HL_HEART] > 1)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_HEART", "", Player_Hunt_Data.iHitsOnLocation[HL_HEART], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_HEAD] > 1)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_HEAD", "", Player_Hunt_Data.iHitsOnLocation[HL_HEAD], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_NECK] > 3) AND (iItemsDrawing < 8)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_NECK", "", Player_Hunt_Data.iHitsOnLocation[HL_NECK], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_ASS] > 3) AND (iItemsDrawing < 8)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_REAR", "", Player_Hunt_Data.iHitsOnLocation[HL_ASS], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_BODY] > 5) AND (iItemsDrawing < 8)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_TORSO", "", Player_Hunt_Data.iHitsOnLocation[HL_BODY], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_FORELEG] > 6) AND (iItemsDrawing < 8)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_FLEG", "", Player_Hunt_Data.iHitsOnLocation[HL_FORELEG], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF
	IF (Player_Hunt_Data.iHitsOnLocation[HL_HINDLEG] > 6) AND (iItemsDrawing < 8)
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(resultsScreen, ESEF_RAW_INTEGER, "AHT_HITS_BLEG", "", Player_Hunt_Data.iHitsOnLocation[HL_HINDLEG], 0, ESCM_NO_MARK)
		iItemsDrawing++
	ENDIF	
	
	
	// ALWAYS DRAW THIS.
	// =============================
	// Draw the medal earned.
	IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold
		SET_ENDSCREEN_COMPLETION_LINE_STATE(resultsScreen, TRUE, "AHT_MEDAL_EARN", 0, 0, INT_TO_ENUM(END_SCREEN_COMPLETION_TYPE, -1), ESMS_GOLD)
	ELIF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForSilver
		SET_ENDSCREEN_COMPLETION_LINE_STATE(resultsScreen, TRUE, "AHT_MEDAL_EARN", 0, 0, INT_TO_ENUM(END_SCREEN_COMPLETION_TYPE, -1), ESMS_SILVER)
	ELIF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForBronze
		SET_ENDSCREEN_COMPLETION_LINE_STATE(resultsScreen, TRUE, "AHT_MEDAL_EARN", 0, 0, INT_TO_ENUM(END_SCREEN_COMPLETION_TYPE, -1), ESMS_BRONZE)
	ELSE
		SET_ENDSCREEN_COMPLETION_LINE_STATE(resultsScreen, FALSE, "AHT_NO_MEDAL", 0, 0, INT_TO_ENUM(END_SCREEN_COMPLETION_TYPE, -1), ESMS_NO_MEDAL)
	ENDIF
	
	// Flag us as drawing.
	ENDSCREEN_PREPARE(resultsScreen)
	resultsScreen.bAlwaysShowStats = TRUE //B* - 2032981
	SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
ENDPROC


// Show the results screen, and monitor the player's input.
PROC DRAW_HUNT_RESULTS(END_SCREEN_DATASET & resultsScreen)
	// Draw the screen.
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)

	RENDER_ENDSCREEN(resultsScreen)
ENDPROC


FUNC BOOL PROCESS_HUNT_RESULTS_INPUT(BOOL &bIsContinuing, BOOL & bDisplayResults, BOOL bDisableRetry = FALSE, BOOL bAllowLeaderboard = FALSE)

	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)

	IF NOT bGotInputDelayTime
		IF bDisableRetry
			iInputDelayTime = GET_GAME_TIMER() + (INPUT_DELAY_TIME / 2)
		ELSE
			iInputDelayTime = GET_GAME_TIMER() + INPUT_DELAY_TIME
		ENDIF
		bGotInputDelayTime = TRUE
	ENDIF
	
	// If we were waiting on any transition, do nothing.
	IF (HUNTING_GET_QUITTING_TIMER() < GET_GAME_TIMER()) AND (HUNTING_GET_QUITTING_TIMER() != -1)
		HUNTING_SET_QUITTING_TIMER(-1)
		RETURN FALSE
	ENDIF
	
	IF bIsWaitingToQuit
		CPRINTLN(DEBUG_MISSION, "PROCESS HUNT RESULTS - bIsWaitingToQuit = TRUE")
		RETURN TRUE
	ENDIF
		
	// Are we supposed to be drawing the leaderboard?
	IF bDisplayLeaderboard
		bDisplayResults = FALSE
	ELSE
		bDisplayResults = TRUE
	ENDIF

	// This is the menu that shows results!
	// Stop the pad from taking input for a couple of seconds after the game has ended.
	IF GET_GAME_TIMER() > iInputDelayTime AND NOT bIsWaitingToQuit
		IF NOT bDisplayLeaderboard
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD) AND IS_SIMPLE_USE_CONTEXT_INPUT_ADDED(resultsInstructions, FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
				PLAY_SOUND_FRONTEND(-1, "LEADER_BOARD", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				bDisplayResults = FALSE
				bDisplayLeaderboard = TRUE
				IF IS_PLAYER_ONLINE()
					bDisplayProfileButton = FALSE
					// Resetup controls based on what we're to be drawing.
					INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",				FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					IF NOT bDisableRetry
						ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",			FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					ENDIF
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HT_BACK_PMT",		FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					IF SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(Player_Hunt_Data.huntLB_control)
						ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
						bDisplayProfileButton = TRUE
					ENDIF
					SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
				ENDIF
			ENDIF
		ELSE
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				bDisplayResults = TRUE
				bDisplayLeaderboard = FALSE
				//g_bBlockWastedTitle = FALSE
				
				// Resetup controls based on what we're to be drawing.
				INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				IF NOT bDisableRetry
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				ENDIF
				IF bAllowLeaderboard
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HLEADR",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
				ENDIF
				SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
			ELSE
				IF NOT bDisplayProfileButton
					IF SHOULD_PROFILE_BUTTON_BE_AVAILABLE_FOR_LEADERBOARD(Player_Hunt_Data.huntLB_control)
						IF IS_PLAYER_ONLINE()
							// Resetup controls based on what we're to be drawing.
							INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
							ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",			FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
							IF NOT bDisableRetry
								ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
							ENDIF
							ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HT_BACK_PMT",	FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "SCLB_PROFILE",	FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
							SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
							bDisplayProfileButton = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
			
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)				
			HUNTING_SET_QUITTING_TIMER(GET_GAME_TIMER() + UI_QUIT_TRANS_TIME)
			bIsWaitingToQuit 		= TRUE
			bIsContinuing 			= FALSE

			HT_MENU_SFX_PLAY_NAV_SELECT()
			
			// Player is trying to quit.
			RETURN FALSE
			
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND) AND NOT bDisableRetry // Retry - take to menu screen
			// So here, we've hit continue. This basically relaunches hunting.				
			bIsContinuing = TRUE
			HT_MENU_SFX_PLAY_NAV_SELECT()
			
			// Just selected continue!
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



