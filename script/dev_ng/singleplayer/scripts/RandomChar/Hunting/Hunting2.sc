
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_vehicle.sch"
USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch" 
USING "script_player.sch"
USING "rgeneral_include.sch"
USING "common_Hunting.sch"
USING "initial_scenes_Hunting.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Hunting2.sc
//		AUTHOR			:	Rob Taylor
//		DESCRIPTION		:	Hunting tutorial in wilderness
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

g_structRCScriptArgs sRCLauncherDataLocal 

																// ENUMS

ENUM MISSION_STATE
	MS_INIT_MISSION = 0,
	MS_RUN_INTRO,
	MS_SET_UP_AFTER_INTRO,
	MS_SET_UP_BEFORE_FIRST_ELK,
	MS_FOLLOW_TO_FIRST_ELK,
	MS_KILL_FIRST_ELK,
	MS_SET_UP_BEFORE_SECOND_ELK,
	MS_FOLLOW_TO_SECOND_ELK,
	MS_WHISTLE_TO_SECOND_ELK,
	MS_KILL_SECOND_ELK,
	MS_SET_UP_BEFORE_THIRD_ELK,
	MS_FOLLOW_TO_THIRD_ELK,
	MS_WHISTLE_TO_THIRD_ELK,
	MS_KILL_THIRD_ELK,
	MS_SET_UP_BEFORE_FOURTH_ELK,
	MS_HUNTER_LEAVES,
	MS_WHISTLE_TO_FOURTH_ELK,
	MS_KILL_FOURTH_ELK,
	MS_APPROACH_FOURTH_ELK,
	MS_PHOTO_FOURTH_ELK,
	MS_SEND_PHOTO,
	MS_WAIT_FOR_REPLY,
	MS_DO_FINAL_PHONECALL,
	MS_SET_UP_BEFORE_MISSION_PASS,
	MS_MISSION_PASSED,
	MS_MISSION_FAILED
ENDENUM

ENUM SUB_STATE
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

ENUM HUNTER_STATE
	HS_NULL = 0, 
	HS_LEAD_TO_FIRST_ELK,
	HS_WAIT_FOR_PLAYER_TO_RETURN,
	HS_TELL_ABOUT_FIRST_ELK,
	HS_WAIT_KILL_FIRST_ELK,
	HS_AFTER_KILL_FIRST_ELK,
	HS_WAIT_FIND_SECOND_ELK,
	HS_LEAD_TO_SECOND_ELK,
	HS_WAIT_KILL_SECOND_ELK,
	HS_AFTER_KILL_SECOND_ELK,
	HS_WAIT_FIND_THIRD_ELK,
	HS_LEAD_TO_THIRD_ELK,
	HS_WAIT_KILL_THIRD_ELK,
	HS_AFTER_KILL_THIRD_ELK,
	HS_RELEASE_WHEN_OFF_CAMERA,
	
	HS_YELL_AT_PLAYER_FOLLOWING
ENDENUM

ENUM HUNTER_SUB_STATE
	HSS_INIT,
	HSS_ACTIVE,
	HSS_CLEANUP
ENDENUM

ENUM FAIL_REASON
	FR_NONE = 0,
	FR_HE_SAW_YOU,
	FR_HE_SMELLED_YOU,
	FR_HE_HEARD_YOU,
	FR_HUNTER_KILLED,
	FR_HUNTER_INJURED,
	FR_LEFT_HUNTER,
	FR_LEFT_HUNT_AREA,
	FR_BAD_SHOT,
	FR_CAR_DEAD,
	FR_WRONG_GUN,
	FR_SHOT_DOE,
	FR_GUNSHOT,
	FR_INNOCENTS,
	FR_DISOBEYED,
	FAIL_CAR_IN_HUNT_AREA
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_FAIL_REASON(FAIL_REASON eReason)
	SWITCH eReason
		CASE FR_NONE               RETURN "FR_NONE"
		CASE FR_HE_SAW_YOU         RETURN "FR_HE_SAW_YOU"
		CASE FR_HE_SMELLED_YOU     RETURN "FR_HE_SMELLED_YOU"
		CASE FR_HE_HEARD_YOU       RETURN "FR_HE_HEARD_YOU"
		CASE FR_HUNTER_KILLED      RETURN "FR_HUNTER_KILLED"
		CASE FR_HUNTER_INJURED     RETURN "FR_HUNTER_INJURED"
		CASE FR_LEFT_HUNTER        RETURN "FR_LEFT_HUNTER"
		CASE FR_LEFT_HUNT_AREA     RETURN "FR_LEFT_HUNT_AREA"
		CASE FR_BAD_SHOT           RETURN "FR_BAD_SHOT"
		CASE FR_CAR_DEAD           RETURN "FR_CAR_DEAD"
		CASE FR_WRONG_GUN          RETURN "FR_WRONG_GUN"
		CASE FR_SHOT_DOE           RETURN "FR_SHOT_DOE"
		CASE FR_GUNSHOT            RETURN "FR_GUNSHOT"
		CASE FR_INNOCENTS          RETURN "FR_INNOCENTS"
		CASE FR_DISOBEYED          RETURN "FR_DISOBEYED"
		CASE FAIL_CAR_IN_HUNT_AREA RETURN "FAIL_CAR_IN_HUNT_AREA"
	ENDSWITCH
	RETURN "unknown FAIL_REASON"
ENDFUNC
#ENDIF

MISSION_STATE eMissionState			= MS_INIT_MISSION
SUB_STATE eSubState					= SS_INIT
HUNTER_STATE eHunterState			= HS_NULL
HUNTER_STATE ePreState				
HUNTER_SUB_STATE eHunterSubState 	= HSS_INIT
FAIL_REASON eFailReason

																			/// CONSTS

CONST_FLOAT SIGHT_RANGE_DUCK_WARN	55.0
CONST_FLOAT ACCEPTABLE_NOISE_LEVEL 	2.5
CONST_FLOAT NEAR_HUNTER_RANGE		15.0
CONST_FLOAT RETURN_HUNTER_RANGE		10.0
CONST_FLOAT CAR_NOISE_RANGE			80.0

CONST_INT CP_AFTER_INTRO			0
CONST_INT CP_AFTER_TUTORIAL			1
CONST_INT CP_AFTER_FIRST_ELK		2
CONST_INT CP_AFTER_SECOND_ELK		3
CONST_INT CP_AFTER_THIRD_ELK		4
CONST_INT CP_AFTER_FOURTH_ELK		5

// Hunter conversations
CONST_INT HTC_HUNTING_TUT_1			0	// HT_CUT - You got your wish boy, we're hunting elk now...
CONST_INT HTC_ARRIVE_FIRST_ELK		1	// HT2_HUNT - Right son, let's try and bag us three elk...
CONST_INT HTC_GOOD_SHOT_FIRST_ELK	2	// HT2_COMP1 - Boom, great kill son...
CONST_INT HTC_LEAD_TO_SECOND_ELK	3	// HT2_WHISTLE2 - Right, we don't know where the next elk is...
CONST_INT HTC_WHISTLE_TO_SECOND_ELK	4	// HT2_WLK3 - Ok, this'll do.
CONST_INT HTC_DISCOVER_SECOND_ELK	5	// //DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE - Same as before boy
CONST_INT HTC_GOOD_SHOT_SECOND_ELK	6	// HT2_COMP2 - Good shot son!
CONST_INT HTC_LEAD_TO_THIRD_ELK		7	// HT_PAIR - Follow me quietly, we might have a mating pair up ahead.
CONST_INT HTC_WHISTLE_TO_THIRD_ELK 	8	// HT_LEAD3 - Remember, we're only here for the male elk.
CONST_INT HTC_DISCOVER_THIRD_ELK	9	// HT2_ARR3 - Same as before boy
CONST_INT HTC_GOOD_SHOT_THIRD_ELK	10	// HT2_COMP1 - Boom! Great kill son.
CONST_INT HTC_TELL_ABOUT_TEXTING	11	// HT2_WLKOFF - Right, you're on your own for this lask elk
CONST_INT HTC_REPEAT_WHISTLE		12

CONST_INT MAX_HUNTER_CONVOS			13

CONST_INT FIRST_ELK					0
CONST_INT SECOND_ELK 				1
CONST_INT THIRD_ELK 				2
CONST_INT DOE_ELK					3
CONST_INT FOURTH_ELK 				4

CONST_INT MAX_WIND_HELP_STRINGS		3		// Number of help strings that are associated with the wind help

CONST_INT HELP_TEXT_TIME			5000	// 5 seconds
CONST_INT MAX_WARNING_CONVS 		6
CONST_INT WHISTLE_BUTTON_HELP 		3
CONST_INT WHISTLE_INACTIVITY_TIME	15000

CONST_INT CROUCH_WARNING			0
CONST_INT UPWIND_WARNING			1
CONST_INT BOTH_WARNING				2
CONST_INT RUNNING_WARNING			3
CONST_INT GUN_WARNING				4
CONST_INT CAR_WARNING				5

CONST_INT WIND_ARROW_HELP			0
CONST_INT CROUCH_HELP				1
CONST_INT WIND_CHANGE_HELP			2
CONST_INT FIRST_CALL_HELP			3
CONST_INT SECOND_CALL_HELP			4
CONST_INT THIRD_CALL_HELP			5
CONST_INT DONT_KILL_DOE_HELP		6

CONST_INT MAX_HELP_STRINGS			7		// Number of help strings
CONST_INT MAX_ELK 					5
CONST_INT NICE_LITTLE_PAUSE			1700
CONST_INT PLAYER_CALL_DURATION		3000

CONST_FLOAT PROMPT_AREA_WIDTH		20.0


#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH	5
#ENDIF

structPedsForConversation mHunterConv

																	// VARIABLES
																	
BLIP_INDEX mBlip
BLIP_INDEX mHunterBlip

BOOL bSetIntroExitState			= FALSE
BOOL bCanUpdateWindBlip			= FALSE
BOOL bCarNoise					= FALSE
BOOL bElkAssetsInMem			= FALSE
BOOL bFailForSpooking			= FALSE
BOOL bFinalElkFlee				= FALSE
BOOL bHunterStartedWalking		= FALSE
BOOL bGotBlipFadeTime			= FALSE
BOOL bGotFailDelay 				= FALSE
BOOL bGotFailTimer				= FALSE
//BOOL bGotReturnTimer			= FALSE
BOOL bGroupCreated				= FALSE
BOOL bGotResponseDelay			= FALSE
BOOL bIsTrevorLooking			= FALSE
BOOL bNoiseDetected				= FALSE
BOOL bSentTextMessage			= FALSE
BOOL bStartTextTimer			= FALSE
BOOL bShitSkipping				= FALSE
BOOL bShotFromWrongPlace 		= FALSE
BOOL bShotWithWrongGun 			= FALSE
BOOL bShownFleeWarning			= FALSE
BOOL bShownReturnToHunter		= FALSE
BOOL bWindCanChange				= FALSE
//BOOL bIsRetry					= FALSE
//BOOL bShowWindInCutscene		= FALSE
BOOL bHadClip					= FALSE
BOOL bHadLargeScope				= FALSE
BOOL bHadMaxScope				= FALSE

BOOL bHunterDoneConvo[MAX_HUNTER_CONVOS]
BOOL bIncreasedHeartStat[MAX_ELK]
BOOL bShownHuntElkGodText[MAX_ELK]
BOOL bDoneRightGunAimConvo[MAX_ELK]
BOOL bDoneWrongGunAimConvo[MAX_ELK]
BOOL bWarningConvGiven[MAX_ELK]
BOOL bShownHelp[MAX_HELP_STRINGS]

CAMERA_INDEX mCam

//FLOAT fHunterOutroHead		= 335.3803

// Warp positions for player
FLOAT fPlayerStartHead	 		= 301.6213
FLOAT fPlayerStartHuntHead		= 255.0691

FLOAT fPlayerSecondKillHead		= 280.2539
FLOAT fPlayerThirdKillHead		= 265.0211

FLOAT fHunterStartHead	 		= 301.6213
FLOAT fHunterStartHuntHead		= 255.0691

FLOAT fHunterSecondKillHead		= 320.8655
FLOAT fHunterThirdKillHead		= 209.3795

FLOAT fElkHead[MAX_ELK]

INT iBlipFadeTime
INT iCutSceneState				= 0
INT iDeadPeds					= 0
INT iFailTimer
INT iFailDelay
INT iNoiseFailDelay
INT iResponseDelay	
BOOL bGotReturnTime = FALSE
INT iReturnTime
INT iTimerDelay					= 0
INT iTint

//MODEL_NAMES mHunterModel		= IG_CLETUS //IG_HUNTER
MODEL_NAMES mElkModel			= A_C_DEER

OBJECT_INDEX mCletusGun
OBJECT_INDEX mTrevorGun

PED_INDEX mHunter

REL_GROUP_HASH relGroupFriendly

STRING sFailReason 				= NULL
STRING sTextBlock				= "HT2AUD"
STRING sTrevor					= "Trevor"
STRING sKillString

TEXT_LABEL_23 sCurConvo			= "HT_CUT"
TEXT_LABEL_23 sCurLine			= "HT_CUT_1"
TEXT_LABEL_23 txtLine 

VECTOR vPlayerStartPos			= << -680.2113, 5848.0083, 16.2569 >> // Outside the shops (after the mocap)
VECTOR vHunterStartPos 			= << -677.2093, 5847.6357, 16.3312 >>

VECTOR vPlayerStartHuntPos		= <<-623.4990, 5849.9761, 22.0710>> // Hunter on edge of woods, pointing into them.
VECTOR vHunterStartHuntPos		= <<-621.9225, 5849.9966, 22.1182>> 

VECTOR vPlayerFirstKillPos		= <<-607.6183, 5853.4829, 22.7782>>	// After palyer kills first elk, Trevor is here.
FLOAT fPlayerFirstKillHead		= 217.5379

VECTOR vHunterFirstKillPos		= <<-605.1650, 5856.5386, 22.8050>>  // After Trev kills first elk, hunter will be here.
FLOAT fHunterFirstKillHead 		= 174.8695

VECTOR vPlayerSecondKillPos		= <<-585.8595, 5854.5029, 27.1148>>	// Hunter's position at second kill 
VECTOR vHunterSecondKillPos		= <<-585.6373, 5857.5254, 27.2106>>

VECTOR vPlayerThirdKillPos		= <<-581.1332, 5844.7646, 28.2620>>
VECTOR vHunterThirdKillPos		= <<-585.0704, 5842.9277, 27.9043>>

VECTOR SCENT_RANGE_WARN			= << 0.0, 60.0, 0.0 >>

VECTOR vElkSpawn[MAX_ELK]
VECTOR vElkGoto_Second 			= <<-529.0620, 5865.1162, 32.6770>>

VECTOR vElkGoto_Third 			=  <<-572.70, 5786.51, 38.37>>
FLOAT fElkGoto_ThirdHeading 	= 90.01
VECTOR vElkGoto_Doe 			= <<-575.36, 5788.60, 37.59>>
FLOAT fElkGoto_DoeHeading 		= -92.72

VECTOR vElkGoto_Fourth 			= <<-618.4669, 5750.4575, 31.4123>>
FLOAT fElkGoto_FourthHeading 	= 11.9731

VECTOR vElkGoto_Fourth_alt			= <<-659.7115, 5711.5996, 23.6818>>
FLOAT fElkGoto_FourthHeading_alt	= 125.9914

// Elk warning area
VECTOR vElkOnePromptAreaOneA	= <<-626.5519, 5824.6006, 23.1475>>
VECTOR vElkOnePromptAreaOneB	= <<-588.3788, 5829.8975, 29.0621>>

VECTOR vElkTwoPromptAreaOneA	= <<-578.3123, 5876.2271, 27.3243>>
VECTOR vElkTwoPromptAreaOneB	= <<-536.2968, 5860.1050, 32.3228>>

// Resolve vehicle
VECTOR vResolveAreaA			= <<-680.5353, 5847.8730, 16.0>>
VECTOR vResolveAreaB			= <<-686.4902, 5841.4990, 19.0>>
VECTOR vResolveAreaC			= <<-612.2185, 5843.3315, 25.3576>>
VECTOR vResolvePos				= << -691.7810, 5835.6895, 15.9653 >>
FLOAT fResolveHead				= 318.4915

CONST_FLOAT RESOLVE_AREA_WIDTH			5.0
CONST_FLOAT RESOLVE_AREA_WIDTH_LARGE	25.0



BOOL			bHunter_InPosition1 = FALSE
BOOL			bWhistle_CaughtFirst = FALSE
BOOL			bWhistle_WaitingOnConvo = FALSE

BOOL			bHunter_PointedAtHuntingArea = FALSE

BOOL			bHunter_TaskedAfterKill2 = FALSE
BOOL			bHunter_PointedAtElk2 = FALSE

BOOL			bHunter_WaitWhistleKill3 = FALSE

CONST_FLOAT VERY_NEAR_HUNTER_RANGE 6.0
CONST_INT ELK1_NO_ACTIVITY_TIME_MIN 25000
CONST_INT ELK1_NO_ACTIVITY_TIME_MAX 45000	
INT iFirstElkTimer				= 0

CONST_INT CLETUS_FOLLOW_COMMENT_MIN	8000
CONST_INT CLETUS_FOLLOW_COMMENT_MAX 12000
INT iCletusFollowTimer			= 0

BOOL bHunter_WaitToLeave = FALSE

BOOL bSpawned1stElk = FALSE
BOOL bTriggered3rdElk = FALSE

INT iWPR_0_1_Progress			= 0
INT iWPR_1_2_Progress			= 0
INT iWPR_2_3_Progress			= 0









											// Debug 
															
#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
	INT i_debug_jump_stage
	
FUNC STRING GET_STRING_FROM_MISSION_STATE(MISSION_STATE eState)
	SWITCH eState
		CASE MS_INIT_MISSION					RETURN "MS_INIT_MISSION"
		CASE MS_RUN_INTRO						RETURN "MS_RUN_INTRO"
		CASE MS_SET_UP_AFTER_INTRO				RETURN "MS_SET_UP_AFTER_INTRO"
		CASE MS_SET_UP_BEFORE_FIRST_ELK			RETURN "MS_SET_UP_BEFORE_FIRST_ELK"
		CASE MS_FOLLOW_TO_FIRST_ELK				RETURN "MS_FOLLOW_TO_FIRST_ELK"
		CASE MS_KILL_FIRST_ELK					RETURN "MS_KILL_FIRST_ELK"
		CASE MS_SET_UP_BEFORE_SECOND_ELK		RETURN "MS_SET_UP_BEFORE_SECOND_ELK"
		CASE MS_FOLLOW_TO_SECOND_ELK			RETURN "MS_FOLLOW_TO_SECOND_ELK"
		CASE MS_WHISTLE_TO_SECOND_ELK			RETURN "MS_WHISTLE_TO_SECOND_ELK"
		CASE MS_KILL_SECOND_ELK					RETURN "MS_KILL_SECOND_ELK"
		CASE MS_SET_UP_BEFORE_THIRD_ELK			RETURN "MS_SET_UP_BEFORE_THIRD_ELK"
		CASE MS_FOLLOW_TO_THIRD_ELK				RETURN "MS_FOLLOW_TO_THIRD_ELK"
		CASE MS_WHISTLE_TO_THIRD_ELK			RETURN "MS_WHISTLE_TO_THIRD_ELK"
		CASE MS_KILL_THIRD_ELK					RETURN "MS_KILL_THIRD_ELK"
		CASE MS_SET_UP_BEFORE_FOURTH_ELK		RETURN "MS_SET_UP_BEFORE_FOURTH_ELK"
		CASE MS_HUNTER_LEAVES					RETURN "MS_HUNTER_LEAVES"
		CASE MS_WHISTLE_TO_FOURTH_ELK			RETURN "MS_WHISTLE_TO_FOURTH_ELK"
		CASE MS_KILL_FOURTH_ELK					RETURN "MS_KILL_FOURTH_ELK"
		CASE MS_APPROACH_FOURTH_ELK				RETURN "MS_APPROACH_FOURTH_ELK"
		CASE MS_PHOTO_FOURTH_ELK				RETURN "MS_PHOTO_FOURTH_ELK"
		CASE MS_SEND_PHOTO						RETURN "MS_SEND_PHOTO"
		CASE MS_WAIT_FOR_REPLY					RETURN "MS_WAIT_FOR_REPLY"
		CASE MS_DO_FINAL_PHONECALL				RETURN "MS_DO_FINAL_PHONECALL"
		CASE MS_SET_UP_BEFORE_MISSION_PASS		RETURN "MS_SET_UP_BEFORE_MISSION_PASS"
		CASE MS_MISSION_PASSED					RETURN "MS_MISSION_PASSED"
		CASE MS_MISSION_FAILED					RETURN "MS_MISSION_FAILED"
	ENDSWITCH
	
	RETURN "unknown mission state"
ENDFUNC

FUNC STRING GET_STRING_FROM_SUB_STATE(SUB_STATE eState)
	SWITCH eState
		CASE SS_INIT		RETURN "SS_INIT"
		CASE SS_ACTIVE		RETURN "SS_ACTIVE"
		CASE SS_CLEANUP		RETURN "SS_CLEANUP"
		CASE SS_SKIPPED		RETURN "SS_SKIPPED"
	ENDSWITCH
	RETURN "unknown sub state"
ENDFUNC

FUNC STRING GET_STRING_FROM_HUNTER_STATE(HUNTER_STATE eState)
	SWITCH eState
		CASE HS_NULL						RETURN "HS_NULL"
		CASE HS_LEAD_TO_FIRST_ELK			RETURN "HS_LEAD_TO_FIRST_ELK"
		CASE HS_WAIT_FOR_PLAYER_TO_RETURN	RETURN "HS_WAIT_FOR_PLAYER_TO_RETURN"
		CASE HS_TELL_ABOUT_FIRST_ELK		RETURN "HS_TELL_ABOUT_FIRST_ELK"
		CASE HS_WAIT_KILL_FIRST_ELK			RETURN "HS_WAIT_KILL_FIRST_ELK"
		CASE HS_AFTER_KILL_FIRST_ELK		RETURN "HS_AFTER_KILL_FIRST_ELK"
		CASE HS_WAIT_FIND_SECOND_ELK		RETURN "HS_WAIT_FIND_SECOND_ELK"
		CASE HS_LEAD_TO_SECOND_ELK			RETURN "HS_LEAD_TO_SECOND_ELK"
		CASE HS_WAIT_KILL_SECOND_ELK		RETURN "HS_WAIT_KILL_SECOND_ELK"
		CASE HS_AFTER_KILL_SECOND_ELK		RETURN "HS_AFTER_KILL_SECOND_ELK"
		CASE HS_WAIT_FIND_THIRD_ELK			RETURN "HS_WAIT_FIND_THIRD_ELK"
		CASE HS_LEAD_TO_THIRD_ELK			RETURN "HS_LEAD_TO_THIRD_ELK"
		CASE HS_WAIT_KILL_THIRD_ELK			RETURN "HS_WAIT_KILL_THIRD_ELK"
		CASE HS_AFTER_KILL_THIRD_ELK		RETURN "HS_AFTER_KILL_THIRD_ELK"
		CASE HS_RELEASE_WHEN_OFF_CAMERA		RETURN "HS_RELEASE_WHEN_OFF_CAMERA"
		CASE HS_YELL_AT_PLAYER_FOLLOWING	RETURN "HS_YELL_AT_PLAYER_FOLLOWING"
	ENDSWITCH
	RETURN "unknown hunter state"
ENDFUNC

FUNC STRING GET_STRING_FROM_HUNTER_SUB_STATE(HUNTER_SUB_STATE eState)
	SWITCH eState
		CASE HSS_INIT			RETURN "HSS_INIT"
		CASE HSS_ACTIVE			RETURN "HSS_ACTIVE"
		CASE HSS_CLEANUP		RETURN "HSS_CLEANUP"
	ENDSWITCH
	RETURN "unknown hunter sub state"
ENDFUNC

#ENDIF
															// General Functions

PROC RESET_CAM()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
ENDPROC

/// PURPOSE:
///    RESTORES THE PLAYERS PERSONAL SNIPER RIFLE AFTER MISSION
PROC LOCAL_RETURN_PLAYERS_GUN()
	
	CPRINTLN(DEBUG_MISSION, "RETURNING PLAYERS GUN")
		
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
				
		IF bHadSniperRifle
		
			CPRINTLN(DEBUG_MISSION, "Player had a sniper rifle with", iSniperAmmoBefore, "rounds")
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, iSniperAmmoBefore, FALSE, FALSE)
			
			// Remove infinite ammo.
			SET_PED_INFINITE_AMMO(PLAYER_PED_ID(), FALSE, WEAPONTYPE_SNIPERRIFLE)		
			SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), AMMOTYPE_SNIPER, iSniperAmmoBefore)
			
			IF bHadSuppressor
				CPRINTLN(DEBUG_MISSION, "with a supressor")
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_AR_SUPP_02)
			ENDIF
			IF iTint != -1
				CPRINTLN(DEBUG_MISSION, "setting tint")
				SET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, iTint)
			ENDIF
			IF bHadClip
				CPRINTLN(DEBUG_MISSION, "giving clip")
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_SNIPERRIFLE_CLIP_01)
			ENDIF
			IF bHadLargeScope
				CPRINTLN(DEBUG_MISSION, "giving large scope")
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_LARGE)
			ENDIF
			IF bHadMaxScope
				CPRINTLN(DEBUG_MISSION, "giving max scope")
				GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_MAX)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC LOCAL_RECORD_PLAYERS_GUN()
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, GENERALWEAPON_TYPE_INVALID)
			
		bHadSniperRifle = TRUE
				
		iTint = GET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
		CPRINTLN(DEBUG_MISSION, "iTint = ", iTint)
		
		iSniperAmmoBefore = GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), AMMOTYPE_SNIPER)
		CPRINTLN(DEBUG_MISSION, "iSniperAmmoBefore = ", iSniperAmmoBefore)
		
		IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_AR_SUPP_02)
			CPRINTLN(DEBUG_MISSION, "bHadSuppressor = TRUE")
			bHadSuppressor = TRUE
		ENDIF
					
		IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_LARGE)
			CPRINTLN(DEBUG_MISSION, "bHadLargeScope = TRUE")
			bHadLargeScope = TRUE
		ENDIF
		
		IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_SCOPE_MAX)
			CPRINTLN(DEBUG_MISSION, "bHadMaxScope = TRUE")
			bHadMaxScope = TRUE
		ENDIF
		
		IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_SNIPERRIFLE_CLIP_01)
			CPRINTLN(DEBUG_MISSION, "bHadClip = TRUE")
			bHadClip = TRUE
		ENDIF
		
	ENDIF
		
ENDPROC

/// PURPOSE:
///    Makes a ped stealth or stop stealth, checks to see if the ped is stealth first
///    TODO: MAKE THIS LESS ROBOTIC
PROC ALTER_STEALTH(PED_INDEX ped, BOOL bShouldDuck)
	IF bShouldDuck
		IF NOT GET_PED_STEALTH_MOVEMENT(ped) 
			CPRINTLN(DEBUG_HUNTING, "HUNTER DUCKING")
			SET_PED_STEALTH_MOVEMENT(ped, TRUE, "DEFAULT_ACTION")
		ENDIF
	ELSE
		IF GET_PED_STEALTH_MOVEMENT(ped) 
			CPRINTLN(DEBUG_HUNTING, "HUNTER STANDING UP")
			SET_PED_STEALTH_MOVEMENT(ped, FALSE)
		ENDIF
	ENDIF
ENDPROC 

/// PURPOSE:
///    Checks to see if the player is near an entity
FUNC BOOL IS_PLAYER_AT_ENTITY(ENTITY_INDEX entity, VECTOR vradius)
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(entity)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), entity, vradius)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SAFE_FADE_OUT()

	IF NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(500)
	ENDIF
			
ENDPROC

PROC SAFE_FADE_IN()
	
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(500)
	ENDIF
	
ENDPROC
			
PROC RESET_SOUNDS()

	RETURN_AUDIO()
	
	IF IS_AUDIO_SCENE_ACTIVE("HUNTING_02_SETTINGS")
		STOP_AUDIO_SCENE("HUNTING_02_SETTINGS")
		STOP_SOUND(iSoundID)
		RELEASE_SOUND_ID(iSoundID)
		iSoundID = -1
		iSoundDelay = 0
	ENDIF
		
	// Release audio
	RELEASE_MISSION_AUDIO_BANK()
	
ENDPROC

/// PURPOSE:
///    Prints objective to the screen if the objective hasnt already been printed
PROC PRINT_OBJ(String sString)
	PRINT_NOW(sString, DEFAULT_GOD_TEXT_TIME, 0)
ENDPROC


														// Clean up

/// PURPOSE:
///		Removes every blip in the mission 
PROC REMOVE_ALL_BLIPS()

	SAFE_REMOVE_BLIP(mBlip)
	
	INT i 
	REPEAT MAX_ELK i 
		SAFE_REMOVE_BLIP(mElk[i].mElkBlip)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
/// 
PROC DELETE_ALL()
	
	//DELETE PEDS	
	IF DOES_ENTITY_EXIST(mHunter)
		IF IS_ENTITY_ALIVE(mHunter)
			SET_ENTITY_LOAD_COLLISION_FLAG(mHunter, FALSE)
			IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
				IF IS_PED_GROUP_MEMBER(mHunter, PLAYER_GROUP_ID())
					REMOVE_PED_FROM_GROUP(mHunter)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SAFE_DELETE_PED(mHunter)
	
	INT i = 0
	REPEAT MAX_ELKS i 
		SAFE_DELETE_PED(mElk[i].mPed)
	ENDREPEAT
		
	IF DOES_CAM_EXIST(mCam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(mCam)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("HUNTING_02_SETTINGS")
		STOP_AUDIO_SCENE("HUNTING_02_SETTINGS")
		STOP_SOUND(iSoundID)
		RELEASE_SOUND_ID(iSoundID)
	ENDIF	
	
	RELEASE_MISSION_AUDIO_BANK()
	
ENDPROC

/// PURPOSE:
///    
PROC Script_Cleanup()

	// Clear the mission title
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER", TRUE)
	
	STOP_SNIPE_AUDIO()
	LOCAL_RETURN_PLAYERS_GUN()
	
	IF IS_AUDIO_SCENE_ACTIVE("HUNTING_02_TRAFFIC_SCENE")
		STOP_AUDIO_SCENE("HUNTING_02_TRAFFIC_SCENE")
	ENDIF
		
	REMOVE_PED_FOR_DIALOGUE(mHunterConv, 2)
	REMOVE_PED_FOR_DIALOGUE(mHunterConv, 3)
	
	//STOP_AUDIO_SCENE("AMBIENT_HUNTING_MIX")
	
	INT i = 0
	REPEAT MAX_ELKS i 
		IF DOES_ENTITY_EXIST(mElk[i].mPed)
			SET_PED_AS_NO_LONGER_NEEDED(mElk[i].mPed)
		ENDIF
	ENDREPEAT
	
	IF bElkAssetsInMem
		SET_MODEL_AS_NO_LONGER_NEEDED(A_C_DEER)
	ENDIF
	
	IF DOES_ENTITY_EXIST(mHunter)
		IF NOT IS_ENTITY_DEAD(mHunter)
			SET_ENTITY_LOAD_COLLISION_FLAG(mHunter, FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(mHunter)
			CLEAR_PED_TASKS(mHunter)
		ENDIF
		ALTER_STEALTH(mHunter, FALSE)
		SET_PED_AS_NO_LONGER_NEEDED(mHunter)
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())	
		ALTER_STEALTH(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SERVICES_TOGGLE(TRUE)
	STOP_AUDIO_SCENE("AMBIENT_HUNTING_MIX")
	
	DELETE_ALL()
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
	
	IF bGroupCreated
		REMOVE_RELATIONSHIP_GROUP(relGroupFriendly)
		REMOVE_RELATIONSHIP_GROUP(mElkGroup)
	ENDIF
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
ENDPROC


														//     PASS / FAIL

PROC Script_Passed()
	
	CLEAR_PRINTS()
	//SET_TIME_LAST_HUNTED() // TODO Don't forget to comment this back in!!!
	KILL_ANY_CONVERSATION()

	Random_Character_Passed(CP_OJ_HUN2)
	Script_Cleanup()
	TERMINATE_THIS_THREAD()
	
ENDPROC


/// PURPOSE:
///    Checks to see if the Target (entity) was damaged by the sniper rifle
PROC CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(ENTITY_INDEX index)
	
	IF DOES_ENTITY_EXIST(index) 
		
		VECTOR pos = GET_ENTITY_COORDS(index)
		
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, pos, 20)
			CPRINTLN(DEBUG_HUNTING, "Explosion in sphere..")
			bShotWithWrongGun = TRUE
		ENDIF
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(index, PLAYER_PED_ID())
				IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(index, WEAPONTYPE_SNIPERRIFLE)
				AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(index, WEAPONTYPE_HEAVYSNIPER)
					CPRINTLN(DEBUG_HUNTING, "Damaged by non-sniper weapon..")
					bShotWithWrongGun = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC


/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	
	SWITCH eSubState
		CASE SS_INIT
			CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[m_iTargetElk].mPed)
			
			IF NOT bGotFailTimer
				
				CLEAR_PRINTS()
				SAFE_CLEAR_THIS_PRINT("HT_CALL")
				REMOVE_TEXT_MESSAGE_FEED_ENTRY("HT_TXTMSG")
				bCanUpdateWindBlip = FALSE
								
				REMOVE_ALL_BLIPS()
				
				IF bFailForSpooking
					iFailTimer = GET_GAME_TIMER() + 3000
				ELSE
					iFailTimer = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
				ENDIF
				bGotFailTimer = TRUE
			ELSE	
				IF GET_GAME_TIMER() > iFailTimer
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					IF eHunterState <> HS_RELEASE_WHEN_OFF_CAMERA
					AND eHunterState <> HS_NULL
						IF NOT IS_PED_UNINJURED(mHunter)
							CPRINTLN(DEBUG_HUNTING, "MISSION_FAILED FR_HUNTER_INJURED")
							sFailReason = "HT_FAIL4A"
						ENDIF
						IF NOT IS_ENTITY_ALIVE(mHunter)
							CPRINTLN(DEBUG_HUNTING, "MISSION_FAILED FR_HUNTER_KILLED")
							sFailReason = "HT_FAIL4"
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(mElk[m_iTargetElk].mPed)
						IF NOT IS_ENTITY_ALIVE(mElk[m_iTargetElk].mPed)
							IF eFailReason = FR_BAD_SHOT
								IF bShotWithWrongGun
									CPRINTLN(DEBUG_HUNTING, "FAILED_WAIT_FOR_FADE changing fail reason. Elk[", m_iTargetElk, "] is dead and the fail reason was ", GET_STRING_FROM_FAIL_REASON(eFailReason))
									sFailReason = "HT_WGUN"
									eFailReason = FR_WRONG_GUN
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_STRING_NULL_OR_EMPTY(sFailReason)
						CPRINTLN(DEBUG_HUNTING, "Failed with no reason")
						Random_Character_Failed()
					ELSE
						CPRINTLN(DEBUG_HUNTING, "Failed with reason")
						Random_Character_Failed_With_Reason(sFailReason)		
					ENDIF

					eSubState = SS_ACTIVE
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< -675.7826, 5870.9463, 15.7000 >>, 171.0506)
				//SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-690.2787, 5835.7700, 15.9219>>, 301.0914)
			
				DELETE_ALL()
				Script_Cleanup()
				TERMINATE_THIS_THREAD()	
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.

			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Sets the fail reason enum for the mission failed message.
PROC SET_FAIL_REASON(INT iFailReason)
	
	SWITCH (iFailReason)
	
		CASE 0	eFailReason = FR_NONE			BREAK
		CASE 1 	eFailReason = FR_HE_SAW_YOU		BREAK
		CASE 2	eFailReason = FR_HE_HEARD_YOU	BREAK
		CASE 3	eFailReason = FR_HE_SMELLED_YOU	BREAK
		CASE 4 	eFailReason = FR_HE_SAW_YOU		BREAK
		CASE 5 	eFailReason = FR_BAD_SHOT		BREAK
			
	ENDSWITCH
	
	bFailForSpooking = TRUE
	
ENDPROC

/// PURPOSE:
///    Sets the mission failed reason and sets the mission state to be MS_MISSION_FAILED
PROC MISSION_FAILED(FAIL_REASON eFail)
		
	CPRINTLN(DEBUG_HUNTING, "MISSION FAILED")
	DEBUG_PRINTCALLSTACK()
	
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	STOP_SNIPE_AUDIO()
	REMOVE_ALL_BLIPS()
	
	IF eMissionState >= MS_KILL_FOURTH_ELK
		m_iTargetElk = FOURTH_ELK
	ELIF eMissionState >= MS_KILL_THIRD_ELK
		m_iTargetElk = THIRD_ELK
	ELIF eMissionState >= MS_KILL_SECOND_ELK
		m_iTargetElk = SECOND_ELK
	ELIF eMissionState >= MS_KILL_FIRST_ELK
		m_iTargetElk = FIRST_ELK
	ENDIF
		
	IF eHunterState <> HS_NULL
		IF DOES_ENTITY_EXIST(mHunter)
			IF NOT IS_PED_UNINJURED(mHunter)
				CPRINTLN(DEBUG_HUNTING, "MISSION_FAILED FR_HUNTER_INJURED")
				eFail = FR_HUNTER_INJURED
			ENDIF
			IF NOT IS_ENTITY_ALIVE(mHunter)
				CPRINTLN(DEBUG_HUNTING, "MISSION_FAILED FR_HUNTER_KILLED")
				eFail = FR_HUNTER_KILLED
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		eFail = FR_NONE
	ENDIF
	
	SWITCH eFail
		CASE FR_HE_SAW_YOU			sFailReason = "HT_FAIL2"	BREAK // The elk spotted Trevor.
		CASE FR_HE_SMELLED_YOU		sFailReason = "HT_FAIL1A"	BREAK // 
		CASE FR_HE_HEARD_YOU		sFailReason = "HT_FAIL3"	BREAK
		CASE FR_HUNTER_KILLED		sFailReason = "HT_FAIL4"	BREAK
		CASE FR_HUNTER_INJURED		sFailReason = "HT_FAIL4A"	BREAK
		CASE FR_LEFT_HUNTER			sFailReason = "HT_FAIL5"	BREAK
		CASE FR_LEFT_HUNT_AREA		sFailReason = "HH_FAIL6"	BREAK
		CASE FR_BAD_SHOT			sFailReason = "HT_MISS"		BREAK
		CASE FR_WRONG_GUN			sFailReason = "HT_WGUN"		BREAK
		CASE FR_CAR_DEAD			sFailReason = "HH_FAILCAR"	BREAK
		CASE FAIL_CAR_IN_HUNT_AREA	sFailReason = "HH_FAILCARA"	BREAK
		CASE FR_SHOT_DOE			sFailReason = "HT_DOE"		BREAK
		CASE FR_INNOCENTS			sFailReason = "HT_INNOCENT"	BREAK
		CASE FR_DISOBEYED			sFailReason = "HH_FAIL6A"	BREAK
		CASE FR_NONE											BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_HUNTING, "MISSION_FAILED reason = ", sFailReason)
	
	eSubState = SS_INIT
	eMissionState = MS_MISSION_FAILED
		
ENDPROC

														// ASSET REQUESTS

PROC REQUEST_AUDIO_ASSETS()
	START_AUDIO_SCENE("HUNTING_02_TRAFFIC_SCENE")
ENDPROC

PROC REQUEST_MISSION_TEXT()
	
	REQUEST_ADDITIONAL_TEXT("HUNT2", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT(sTextBlock, MISSION_DIALOGUE_TEXT_SLOT)

	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE	
			
ENDPROC

PROC REQUEST_ELK_ASSETS()
	
	REQUEST_MODEL(mElkModel)
	REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_CALLS")
	REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_VOCALS") 		
	REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_MAIN_A")
	REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@enter")
	REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@idle_a")
	REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@exit")
	REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@base")
		
	WHILE NOT HAS_MODEL_LOADED(mElkModel)
	OR NOT REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_CALLS")
	OR NOT REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_VOCALS") 		
	OR NOT REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_MAIN_A")
	OR NOT HAS_ANIM_DICT_LOADED("creatures@deer@amb@world_deer_grazing@enter")
	OR NOT HAS_ANIM_DICT_LOADED("creatures@deer@amb@world_deer_grazing@idle_a")
	OR NOT HAS_ANIM_DICT_LOADED("creatures@deer@amb@world_deer_grazing@exit")
	OR NOT HAS_ANIM_DICT_LOADED("creatures@deer@amb@world_deer_grazing@base")
		WAIT(0)
	ENDWHILE
	
	bElkAssetsInMem = TRUE
		
ENDPROC

															// CREATE FUNCTIONS

/// PURPOSE:
///    Makes the hunter friendly to the player and vice versa.
PROC CREATE_REL_GROUP()
	IF NOT bGroupCreated
		
		ADD_RELATIONSHIP_GROUP("FRIENDLIES", relGroupFriendly)
		SET_PED_RELATIONSHIP_GROUP_HASH(mHunter, relGroupFriendly)
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendly, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupFriendly)
		
		SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), FALSE)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupFriendly, RELGROUPHASH_HATES_PLAYER)
		
		//SET_PED_CONFIG_FLAG(mHunter, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		
		ADD_RELATIONSHIP_GROUP("Elk Group", mElkGroup)
		
		bGroupCreated = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
/// 	Forces the gender of a spawned elk
PROC FORCE_ELK_GENDER(PED_INDEX mElkIn, BOOL bIsMale = TRUE)

	IF bIsMale
		IF DECOR_EXIST_ON(mElkIn, "doe_elk")
			DECOR_REMOVE(mElkIn, "doe_elk")
		ENDIF
		IF IS_ENTITY_ALIVE(mElkIn)
			SET_PED_COMPONENT_VARIATION(mElkIn, PED_COMP_SPECIAL, 1, 0) // GIVE HIM ANTLERS				
		ENDIF
	ELSE
		IF NOT DECOR_EXIST_ON(mElkIn, "doe_elk")
			IF IS_ENTITY_ALIVE(mElkIn)
				SET_PED_COMPONENT_VARIATION(mElkIn, PED_COMP_SPECIAL, 0, 0)
			ENDIF
			DECOR_SET_BOOL(mElkIn, "doe_elk", TRUE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
/// 	Spawns elk at a passed in vector
FUNC BOOL SPAWN_ELK_WITH_POS(INT i, ELK_STATE elkstate, VECTOR pos, BOOL RandSpawn = FALSE, BOOL bBlipFade = TRUE)
	IF NOT DOES_ENTITY_EXIST(mElk[i].mPed)
		IF SPAWN_ELK(i, elkstate, RandSpawn, bBlipFade)
			SAFE_TELEPORT_ENTITY(mElk[i].mPed, pos)
			CPRINTLN(DEBUG_MISSION, "Spawning elk at: ", pos)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWNED_THIS_ELK(INT i)
	IF NOT DOES_ENTITY_EXIST(mElk[i].mPed)
		IF i = FIRST_ELK
			IF SPAWN_ELK_WITH_POS(i, ES_ENTER_GRAZE_AND_WAIT_TO_DIE, vElkSpawn[i], FALSE, FALSE)	// Don't allow the blip on the first elk to fade.
				IF IS_ENTITY_ALIVE(mElk[i].mPed)
					// Force reset the player's call...
					bPlayerCalling = FALSE
					
					SET_ENTITY_HEADING(mElk[i].mPed, fElkHead[i])
					FORCE_ELK_GENDER(mElk[i].mPed, TRUE) // First elk is always male
					//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mElk[i].mPed)
					SET_ELK_RANDOM_CALLS_ENABLED(mElk[i], FALSE)
					SET_ELK_MUST_REPLY_TO_CALL(mElk[i], TRUE)

					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mElk[i].mPed)
					
					CPRINTLN(DEBUG_HUNTING, "first elk has been spawned")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF SPAWN_ELK_WITH_POS(i, ES_ENTER_GRAZE_AND_WAIT_TO_DIE, vElkSpawn[i], FALSE, TRUE) // Dont blip these guys until the whistle has been blown.
				IF IS_ENTITY_ALIVE(mElk[i].mPed)
					// Force reset the player's call...
					bPlayerCalling = FALSE
					
					SET_ENTITY_HEADING(mElk[i].mPed, fElkHead[i])
					SET_ELK_RANDOM_CALLS_ENABLED(mElk[i], FALSE)
					
					SET_ENTITY_VISIBLE(mElk[i].mPed, FALSE)
					
					IF i = DOE_ELK
						CPRINTLN(DEBUG_HUNTING, "Forcing Doe")
						FORCE_ELK_GENDER(mElk[i].mPed, FALSE)
					ELSE
						CPRINTLN(DEBUG_HUNTING, "Forcing Buck")
						FORCE_ELK_GENDER(mElk[i].mPed, TRUE)
						SET_ELK_MUST_REPLY_TO_CALL(mElk[i], TRUE)
					ENDIF
					
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mElk[i].mPed)
					//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mElk[i].mPed)
										
					RETURN TRUE
				
				ENDIF				
			ENDIF
		ENDIF
	ELSE
		//CPRINTLN(DEBUG_MISSION, "SPAWNED_THIS_ELK - Spawned the elk: ", i)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MAKE_ELK_VISIBLE(INT i)
	IF DOES_ENTITY_EXIST(mElk[i].mPed)
	AND NOT IS_ENTITY_VISIBLE(mElk[i].mPed)
		SET_ENTITY_VISIBLE(mElk[i].mPed, TRUE)
	ENDIF
ENDPROC

														// ELK FUNCTIONS
/// PURPOSE:
///    Sets up the elk with thier spawns, heading and gives them a random node number.
PROC POPULATE_ELK()
	vElkSpawn[FIRST_ELK]	= <<-588.3788, 5829.8975, 29.0621>>
	fElkHead[FIRST_ELK] 	= 319.3601
	
	vElkSpawn[SECOND_ELK] 	= <<-493.6698, 5880.7144, 27.8661>>
	fElkHead[SECOND_ELK] 	= 48.4291
	
	vElkSpawn[THIRD_ELK] 	= <<-609.3810, 5715.2627, 29.0551>>
	fElkHead[THIRD_ELK] 	= 52.5746
	
	vElkSpawn[DOE_ELK]		= <<-609.9327, 5713.1172, 29.1666>>
	fElkHead[DOE_ELK]		= 57.7444
	
	vElkSpawn[FOURTH_ELK] 	= <<-607.8190, 5713.6392, 28.9654>>
	fElkHead[FOURTH_ELK]	= 60.3080
ENDPROC


/// PURPOSE:
/// 	Checks if the elk is in the player's line of sight when he's aiming
FUNC BOOL IS_ELK_IN_SIGHTS(INT iCurElk)
    IF IS_PED_UNINJURED(mElk[iCurElk].mPed)
		IF IS_ENTITY_ON_SCREEN(mElk[iCurElk].mPed)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), mElk[iCurElk].mPed)
				IF NOT WOULD_ENTITY_BE_OCCLUDED(mElk[iCurElk].mModel, GET_ENTITY_COORDS(mElk[iCurElk].mPed, FALSE))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
    ENDIF

	RETURN FALSE
ENDFUNC														


														// PLAYER FUNCTIONS
										
										
/// PURPOSE:
///    Fails the mission if the player leaves the hunter too far 
PROC CHECK_PLAYER_FLED()
	IF IS_PED_UNINJURED(mHunter)
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITIES(mHunter, PLAYER_PED_ID(), FALSE)
			
			IF fDist > 100.0
				IF NOT bShownFleeWarning
					PRINT_OBJ("HT_BAC")
					bShownFleeWarning = TRUE
				ENDIF
			ELSE
				IF bShownFleeWarning
					SAFE_CLEAR_THIS_PRINT("HT_BAC")
					bShownFleeWarning = FALSE
				ENDIF
			ENDIF
			
			IF fDist > 150.0
				MISSION_FAILED(FR_LEFT_HUNTER)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Checks if the player is making too much noise while hunting.
PROC CHECK_NOISY_PLAYER()
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	
		IF NOT bNoiseDetected
		AND NOT bCarNoise
	
			VECTOR vPos 
					
			vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vPos, 40.0)
				CLEAR_PRINTS()
				KILL_ANY_CONVERSATION()
				TASK_WANDER_STANDARD(mHunter)
				SET_PED_KEEP_TASK(mHunter, TRUE)
				eHunterState = HS_NULL
				iNoiseFailDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
				bNoiseDetected = TRUE
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vElkSpawn[FIRST_ELK]) < CAR_NOISE_RANGE
				OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vElkSpawn[SECOND_ELK]) < CAR_NOISE_RANGE
				OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vElkSpawn[THIRD_ELK]) < CAR_NOISE_RANGE
					CLEAR_PRINTS()
					KILL_ANY_CONVERSATION()
					TASK_WANDER_STANDARD(mHunter)
					SET_PED_KEEP_TASK(mHunter, TRUE)
					eHunterState = HS_NULL
					iNoiseFailDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
					bCarNoise = TRUE
				ENDIF
			ENDIF
		
		ELSE
				
			IF GET_GAME_TIMER() > iNoiseFailDelay
				IF bCarNoise
					MISSION_FAILED(FAIL_CAR_IN_HUNT_AREA)
				ENDIF
				
				IF bNoiseDetected
					MISSION_FAILED(FR_HE_HEARD_YOU)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC
														
/// PURPOSE:
///  Checks if the player is near the hunter.
FUNC BOOL IS_PLAYER_NEAR_HUNTER(FLOAT fRange)
	
	FLOAT fDist
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(mHunter)
		fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mHunter), FALSE)
		IF fDist < fRange
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///		Triggers the hunter's speech if the player is not using the sniper rifle.
FUNC BOOL DO_WRONG_GUN_LINE()
	IF IS_ELK_IN_SIGHTS() 
	AND IS_PLAYER_FREE_AIMING(PLAYER_ID())
	AND IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
	OR bPlayerCalling
		IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WGUN", CONV_PRIORITY_MEDIUM)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks that the player is using the correct gun for the mission.
FUNC BOOL IS_PLAYER_USING_SNIPER_RIFLE()

	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND GET_WEAPONTYPE_GROUP(GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())) = WEAPONGROUP_SNIPER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
/// 	Checks if the bPlayerKilledElkIncorrectly flag is true, and fails the mission 
///    	with the correct fail reason is so.
PROC CHECK_FOR_INCORRECT_KILLS()
	IF bPlayerKillElkIncorrectly
		MISSION_FAILED(FR_BAD_SHOT)
	ENDIF
ENDPROC
					
/// PURPOSE:
///    IF bShotFromWrongPlace is true the mission should fail
PROC MONITOR_WRONG_LOC_KILLS()	
	IF bShotFromWrongPlace
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			MISSION_FAILED(FR_BAD_SHOT)
		ELSE
			KILL_ANY_CONVERSATION() //Temporary - The hunter will say an angry line be fore the fail, like in hunting 1
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Moniters the players distance from the current elk in play (iCurElkSpot)
FUNC BOOL PLAYER_IN_CROUCH_DISTANCE(INT i)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(mElk[i].mPed)
		
		FLOAT fNoise = GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID())
		
		//SK_PRINT_FLOAT("NOISE LEVEL", fNoise)
		IF fNoise > ACCEPTABLE_NOISE_LEVEL
			FLOAT dist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mElk[i].mPed)
			
			IF dist <= SIGHT_RANGE_DUCK_WARN
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Moniters whether the elk is in an area that is drawn from the player to
///    a point at SCENT_RANGE_WARN (<<0,60,0>> 60m in front of the player)
///    if so the elk is up wind and will smell the player if they where closer.
FUNC BOOL PLAYER_APPROACH_UPWIND(INT i)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(mElk[i].mPed)
		VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		VECTOR vSmell = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fWindDirection, SCENT_RANGE_WARN)
		
		IF IS_ENTITY_IN_ANGLED_AREA(mElk[i].mPed, vPos, vSmell, 30.0, FALSE, FALSE, TM_ANY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_RUNNING()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_RUNNING(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Shows approproate help text either during the tutorial cutscene or in-game.
PROC SHOW_THIS_HELP(INT i)

	SWITCH i
		CASE 0	PRINT_HELP("HT_WIND")			BREAK	// The small arrow shows which direction the wind is blowing.
		CASE 1	PRINT_HELP("HT_DUCK")			BREAK 	// Press L3 to enter stealth mode
		CASE 2	PRINT_HELP("HT_CHANGE")			BREAK	// KEEP AN EYE ON THE WIND DIRECTION AS IT WILL CHANGE OVER TIME
		CASE 3	PRINT_HELP_FOREVER("HT_CALL")	BREAK 	// Use ~PAD_Y~ to call. This will highlight any elk in the area.
		CASE 4	PRINT_HELP_FOREVER("HT_CALL")	BREAK	// Use ~PAD_Y~ to call. This will highlight any elk in the area.
		CASE 5	PRINT_HELP_FOREVER("HT_CALL")	BREAK	// Use ~PAD_Y~ to call. This will highlight any elk in the area.
		CASE 6 	PRINT_HELP("HT_MALE")			BREAK	// Only kill male elk, the ones with antlers.
		
	ENDSWITCH
	
	//iHelpTextTime = GET_GAME_TIMER() + HELP_TEXT_TIME
	IF NOT bShownHelp[i]
		bShownHelp[i] = TRUE
	ENDIF
	
ENDPROC


													///    MISSION FUNCTIONS




/// PURPOSE:
///    Checks to see if PLAYER_IN_CROUCH_DISTANCE() || PLAYER_APPROACH_UPWIND()
///    have returned TRUE.
///    Then plays the correct conversation to warn the player that they are 
///    getting close to being spotted by the elk
///    Should only be active during the 1st and 2nd elk
//PROC MONITER_PLAYERS_APPROACH()
//
//	BOOL bCrouchWarn = PLAYER_IN_CROUCH_DISTANCE()
//	BOOL bWindWarn = PLAYER_APPROACH_UPWIND()
//	BOOL bRunningWarn = IS_PLAYER_RUNNING()
//		
//	IF bCrouchWarn
//	AND NOT bWindWarn
//		DO_WARNING_CONV(CROUCH_WARNING)
//	ELIF bWindWarn
//	AND NOT bCrouchWarn
//		DO_WARNING_CONV(UPWIND_WARNING)
//	ELIF bCrouchWarn
//	AND bWindWarn
//		DO_WARNING_CONV(BOTH_WARNING)
//	ELIF bRunningWarn
//	AND NOT bCrouchWarn
//	AND NOT bWindWarn
//		DO_WARNING_CONV(RUNNING_WARNING)
//	ENDIF
//	//	
//ENDPROC


																// HUNTER FUNCTIONS

/// PURPOSE:
///    Checks to see if the player has killed too many innocents.
PROC CHECK_FOR_MURDER()
	
	PED_INDEX mPed
	INT i = 0
		
	IF IS_PED_UNINJURED(mHunter) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		
		IF Has_Ped_Been_Injured()
		   
			REPEAT Get_Number_Of_Ped_Injured_Events() i
				IF DOES_ENTITY_EXIST(Get_Index_Of_Injured_Ped(i))
	            	mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Injured_Ped(i))
					IF mPed <> PLAYER_PED_ID() AND mPed <> mHunter
						IF NOT IS_ENTITY_A_MISSION_ENTITY(mPed) AND IS_ENTITY_A_PED(mPed) AND IS_PED_HUMAN(mPed) 
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mPed)
								iDeadPeds++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
									
			IF iDeadPeds = 1
				IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)			
					// Get the hunter to warn the player about killing innocents
					CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_INNO", CONV_PRIORITY_MEDIUM)
				ENDIF
			ELIF iDeadPeds = 2
				// If the hunter is close, get the hunter to do his angry fail speech Hunter walks off
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					MISSION_FAILED(FR_INNOCENTS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF				
ENDPROC

/// PURPOSE:
///    Toggles hunter following Player
PROC SET_HUNTER_TO_BUDDY_BEHAVIOUR(BOOL bBuddy)
	
	IF IS_PED_UNINJURED(mHunter)
		IF bBuddy
			IF DOES_GROUP_EXIST(PLAYER_GROUP_ID())
				IF NOT IS_PED_GROUP_MEMBER(mHunter, PLAYER_GROUP_ID())
					CLEAR_PED_TASKS(mHunter)
					SET_PED_AS_GROUP_MEMBER(mHunter, PLAYER_GROUP_ID())
					SET_PED_NEVER_LEAVES_GROUP(mHunter, TRUE)
				ENDIF
			ENDIF		
		ELSE
			// Remove hunter from players group.
			IF IS_PED_GROUP_MEMBER(mHunter, PLAYER_GROUP_ID())
				CLEAR_PED_TASKS(mHunter)
				REMOVE_PED_FROM_GROUP(mHunter)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds a friendly blip to the hunter
PROC BLIP_HUNTER(BOOL bBig = TRUE, BOOL bFlash = FALSE)
	
	IF NOT DOES_BLIP_EXIST(mHunterBlip)
		mHunterBlip = CREATE_PED_BLIP(mHunter, TRUE, TRUE)
	ENDIF
		
	IF DOES_BLIP_EXIST(mHunterBlip)
		IF NOT bBig
			SET_BLIP_SCALE(mHunterBlip, 0.75)
		ENDIF
	
		SET_BLIP_FLASHES(mHunterBlip, bFlash)
					
	ENDIF
		
ENDPROC

/// PURPOSE:
///    Resets all the hunters conversations flags to false
PROC RESET_ALL_HUNTER_CONVOS()
	
	INT i = 0
	REPEAT MAX_HUNTER_CONVOS i 
		bHunterDoneConvo[i] = FALSE
	ENDREPEAT
		
ENDPROC

PROC RESET_ALL_CONVO_FLAGS()
	
	INT i = 0
	REPEAT MAX_ELK i 
		bShownHuntElkGodText[i] 	= FALSE
		bDoneRightGunAimConvo[i]	= FALSE
		bDoneWrongGunAimConvo[i] 	= FALSE
		bWarningConvGiven[i] 		= FALSE
		bIncreasedHeartStat[i] 		= FALSE
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Plays the hunters advice line if the players approach is good and he is within range of the current target.
PROC DO_GUN_ADVICE_CONV(INT i)
	IF NOT bWarningConvGiven[i]

		IF bShownHuntElkGodText[i]
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND NOT IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ") // Make sure we don't trample over the "Hunt the Elk" godtext.
		AND IS_ENTITY_ON_SCREEN(mElk[i].mPed)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vElkOnePromptAreaOneA, vElkOnePromptAreaOneB, PROMPT_AREA_WIDTH)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vElkTwoPromptAreaOneA, vElkTwoPromptAreaOneB, PROMPT_AREA_WIDTH)
				IF NOT WOULD_ENTITY_BE_OCCLUDED(mElk[i].mModel, GET_ENTITY_COORDS(mElk[i].mPed, FALSE))
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						IF IS_PLAYER_USING_SNIPER_RIFLE()
							// That's a good distance there...
							IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_DIST", CONV_PRIORITY_MEDIUM)
								bWarningConvGiven[i] = TRUE
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WGUN", CONV_PRIORITY_MEDIUM)
								bWarningConvGiven[i] = TRUE
							ENDIF
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_NOWEAP", CONV_PRIORITY_MEDIUM)
							bWarningConvGiven[i] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF
ENDPROC

/// PURPOSE:
///    Will handle running encouragement lines for the player if he's inactive.
FUNC BOOL PROCESS_ENCOURAGEMENT_LINES(INT iElkIdx)
	IF NOT IS_PLAYER_NEAR_HUNTER(VERY_NEAR_HUNTER_RANGE)
		CDEBUG3LN(DEBUG_HUNTING, "PROCESS_ENCOURAGEMENT_LINES: Cannot encourage: Player too far to encourage.")
		iFirstElkTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ELK1_NO_ACTIVITY_TIME_MIN, ELK1_NO_ACTIVITY_TIME_MAX)
		RETURN TRUE
	ENDIF
	
	IF (iFirstElkTimer = 0)
		CDEBUG3LN(DEBUG_HUNTING, "PROCESS_ENCOURAGEMENT_LINES: Cannot encourage: Time 0!")
		iFirstElkTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ELK1_NO_ACTIVITY_TIME_MIN, ELK1_NO_ACTIVITY_TIME_MAX)
		RETURN TRUE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND (GET_GAME_TIMER() > iFirstElkTimer) 
		CDEBUG3LN(DEBUG_HUNTING, "PROCESS_ENCOURAGEMENT_LINES: Cannot encourage: Conversed recently.")
		iFirstElkTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ELK1_NO_ACTIVITY_TIME_MIN, ELK1_NO_ACTIVITY_TIME_MAX)
		RETURN TRUE
	ENDIF
	
	IF (GET_GAME_TIMER() > iFirstElkTimer) 
	AND NOT IS_ELK_IN_SIGHTS(iElkIdx)
		IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_ENC", CONV_PRIORITY_MEDIUM)
			CDEBUG3LN(DEBUG_HUNTING, "PROCESS_ENCOURAGEMENT_LINES: Encouraging player!")
			iFirstElkTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(ELK1_NO_ACTIVITY_TIME_MIN, ELK1_NO_ACTIVITY_TIME_MAX)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///		Triggers the hunters line when the player is aiming at the elk
///    Will deliver lines for right and wrong weapon
PROC HANDLE_AIM_CONVO(INT i)
	
	IF IS_ELK_IN_SIGHTS(i) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
		IF IS_PLAYER_USING_SNIPER_RIFLE()
			IF NOT bDoneRightGunAimConvo[i]
			
				SWITCH i 
				
					CASE FIRST_ELK
						// Go on, put him down
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_KILL1", CONV_PRIORITY_MEDIUM)
							bDoneRightGunAimConvo[i] = TRUE
						ENDIF
					BREAK
					
					CASE SECOND_ELK
						// You got him, pull the trigger
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_KILL2", CONV_PRIORITY_MEDIUM)
							bDoneRightGunAimConvo[i] = TRUE
						ENDIF
					BREAK
					
					CASE THIRD_ELK
						//Now lay him in the shade...
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_KILL3", CONV_PRIORITY_MEDIUM)
							bDoneRightGunAimConvo[i] = TRUE
						ENDIF
					BREAK
					
					CASE DOE_ELK
						// Not the flatheads!
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_DOE", CONV_PRIORITY_MEDIUM)
							bDoneRightGunAimConvo[i] = TRUE
						ENDIF
					BREAK
					
				ENDSWITCH
				
			ENDIF
		ELSE
			IF NOT bDoneWrongGunAimConvo[i]
				// Woah, use the sniper rifle!
				IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WGUN", CONV_PRIORITY_MEDIUM)
					bDoneWrongGunAimConvo[i] = TRUE
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SAFE_DELETE_ALL_ELK()
	SAFE_DELETE_PED(mElk[FIRST_ELK].mPed)
	SAFE_DELETE_PED(mElk[SECOND_ELK].mPed)
	SAFE_DELETE_PED(mElk[THIRD_ELK].mPed)
	SAFE_DELETE_PED(mElk[DOE_ELK].mPed)
ENDPROC

FUNC STRING GET_KILL_COMMENT(INT i)

	STRING sString = ""
	
	// Get the last damaged bonetag and saves that out to the player_hunt_data_struct ehitLocation.
	PED_BONETAG ePedBoneTag
	IF DOES_ENTITY_EXIST(mElk[i].mPed)
	
		GET_PED_LAST_DAMAGE_BONE(mElk[i].mPed, ePedBoneTag)
		INT iBoneID = ENUM_TO_INT(ePedBoneTag)
	
		HIT_LOCATION hitLoc = GET_HIT_LOCATION_FOR_ELK_BONE_ID(iBoneID)
		
		SWITCH hitLoc
		
			CASE HL_HEART
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> HEART SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_HEART1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_HEART2"	BREAK
					CASE THIRD_ELK	sString = "HT2_HEART3"	BREAK
				ENDSWITCH
				
				IF NOT bIncreasedHeartStat[i]
					CPRINTLN(DEBUG_MISSION, "INCREMENT HEART SHOT STAT FOR ELK NUMBER ", i)
					INFORM_MISSION_STATS_OF_INCREMENT(HU2_ELK_HEADSHOTS)
					bIncreasedHeartStat[i] = TRUE
				ENDIF
			BREAK
			
			CASE HL_NECK	
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> NECK SHOT <-")
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_NECK1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_NECK2"	BREAK
					CASE THIRD_ELK	sString = "HT2_NECK3"	BREAK
				ENDSWITCH
			BREAK
			
			CASE HL_HEAD
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> HEAD SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_HEAD1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_HEAD2"	BREAK
					CASE THIRD_ELK	sString = "HT2_HEAD3"	BREAK
				ENDSWITCH
			BREAK
			
			CASE HL_BODY	
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> BODY SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_BODY1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_BODY2"	BREAK
					CASE THIRD_ELK	sString = "HT2_BODY3"	BREAK
				ENDSWITCH		
			BREAK
			
			CASE HL_HINDLEG
			CASE HL_FORELEG 
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> LEG SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_LEG1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_LEG2"	BREAK
					CASE THIRD_ELK	sString = "HT2_LEG3"	BREAK
				ENDSWITCH
			BREAK
				
			CASE HL_ASS	
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> ASS SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_REAR1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_REAR2"	BREAK
					CASE THIRD_ELK	sString = "HT2_REAR3"	BREAK
				ENDSWITCH
			BREAK
			
			DEFAULT	
				CPRINTLN(DEBUG_HUNTING, "Hunter comment on -> DEFAULT SHOT <-")
				
				SWITCH i 
					CASE FIRST_ELK	sString = "HT2_COMP1"	BREAK 
					CASE SECOND_ELK	sString = "HT2_COMP2"	BREAK
					CASE THIRD_ELK	sString = "HT2_COMP3"	BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sString
ENDFUNC

PROC HANDLE_BLIP_FADE(INT i)

	IF DOES_BLIP_EXIST(mElk[i].mElkBlip)
		mElk[i].iBlipAlpha = GET_BLIP_ALPHA(mElk[i].mElkBlip)
		IF mElk[i].iBlipAlpha < 0.1
			IF NOT bGotBlipFadeTime
				iBlipFadeTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(4000, 10000)
				bGotBlipFadeTime = TRUE
			ELSE
				IF GET_GAME_TIMER() > iBlipFadeTime
					IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_BLOW", CONV_PRIORITY_MEDIUM)
						bGotBlipFadeTime = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
							
ENDPROC

 														// HUNTER STATES 
	
 /// PURPOSE:
 ///    The hunter's state machine.
PROC CHECK_HUNTER_STATES()
	CHECK_FOR_MURDER()
	
	SWITCH eHunterState
		CASE HS_NULL					
		BREAK
				
		CASE HS_LEAD_TO_FIRST_ELK	
			SWITCH eHunterSubState
				CASE HSS_INIT
					CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_FIRST_ELK - HSS_INIT state")
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_FIRST_ELK - Sending hunter to first position.")

						TASK_FOLLOW_WAYPOINT_RECORDING(mHunter, "HT2_CLE_0_1", iWPR_0_1_Progress, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHunter, FALSE)
						SETTIMERA(0)

						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_FIRST_ELK - HSS_INIT -> HSS_ACTIVE")
						eHunterSubState = HSS_ACTIVE
					ENDIF
				BREAK
				
				CASE HSS_ACTIVE				
					IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE) 
						// Don't start the convo until we are on our way (and the god text has cleared)
						IF NOT bHunterDoneConvo[HTC_HUNTING_TUT_1] 
							IF TIMERA() > 2500 AND NOT IS_MESSAGE_BEING_DISPLAYED()
								// What's this thing in my mouth?
								sCurConvo = "HT_CUT"
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(mHunterConv, sTextBlock, sCurConvo, sCurLine, CONV_PRIORITY_MEDIUM)
									// Hunter looks at player while talking
									IF NOT IS_PED_HEADTRACKING_PED(mHunter, PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
									ENDIF 
																		
									IF NOT IS_STRING_NULL_OR_EMPTY(sCurLine)
										sCurLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
									ENDIF
									
									CPRINTLN(DEBUG_HUNTING, "HTC_HUNTING_TUT_1 DONE")
									bHunterDoneConvo[HTC_HUNTING_TUT_1] = TRUE
								ENDIF
							ENDIF
						ELSE															
							IF NOT bShownHelp[WIND_ARROW_HELP]
								txtLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
								IF ARE_STRINGS_EQUAL(txtLine, "HT_CUT_6")
									CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_FIRST_ELK - Activating wind meter, printing help about wind.")
									bCanUpdateWindBlip = TRUE
									SHOW_THIS_HELP(WIND_ARROW_HELP)	
								ENDIF
							ELSE			
								IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = FINISHED_TASK
									SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHunter, TRUE)
									eHunterSubState = HSS_CLEANUP
								ENDIF
							ENDIF						
						ENDIF
					ELSE
						IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = PERFORMING_TASK
							iWPR_0_1_Progress = GET_PED_WAYPOINT_PROGRESS(mHunter)
						ENDIF
						
						BLIP_HUNTER(FALSE, FALSE)
						eHunterSubState = HSS_INIT
						ePreState = HS_LEAD_TO_FIRST_ELK
						eHunterState = HS_WAIT_FOR_PLAYER_TO_RETURN
						
						CDEBUG2LN(DEBUG_HUNTING, "FOLLOW_TO_FIRST_ELK -- Player too far! Will restore at waypoint: ", iWPR_0_1_Progress)
					ENDIF						
				BREAK
				
				CASE HSS_CLEANUP
					CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_FIRST_ELK - HSS_CLEANUP")
					eHunterSubState = HSS_INIT
					eHunterState = HS_TELL_ABOUT_FIRST_ELK
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HS_WAIT_FOR_PLAYER_TO_RETURN
			IF IS_PLAYER_NEAR_HUNTER(RETURN_HUNTER_RANGE)
				CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FOR_PLAYER_TO_RETURN - Player in range! Going back to route in pre-state: ", ePreState)
				// So we can re-trigger the convo when we return to the hunter
				KILL_ANY_CONVERSATION()
				bHunterDoneConvo[HTC_HUNTING_TUT_1] = FALSE 
				bHunterDoneConvo[HTC_LEAD_TO_SECOND_ELK] = FALSE
				
				BLIP_HUNTER(FALSE, FALSE)
				eHunterSubState = HSS_INIT
				eHunterState = ePreState
				
				bGotReturnTime = FALSE
			ELSE
				IF IS_THIS_PRINT_BEING_DISPLAYED("HT_BAC")
					// Reset our time now, so Cletus comments right away.
					IF NOT bGotReturnTime
						iReturnTime = 0
						bGotReturnTime = TRUE
					ENDIF
					IF (iReturnTime = 0)
						iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 10000)
						//bGotReturnTimer = TRUE
						
						TASK_STAND_STILL(mHunter, -1)
						CLEAR_PED_TASKS(mHunter)
						TASK_TURN_PED_TO_FACE_ENTITY(mHunter, PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FOR_PLAYER_TO_RETURN - Tasking hunter to clear tasks, and turn around")
											
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							sCurLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
				ENDIF

				IF GET_GAME_TIMER() > iReturnTime
					FLOAT fDistance
					fDistance = GET_DISTANCE_BETWEEN_PEDS(mHunter, PLAYER_PED_ID())
					
					// Disable subtitles while the objective is up.
					enumSubtitlesState eDisplaySubs
					IF IS_THIS_PRINT_BEING_DISPLAYED("HT_BAC")
						eDisplaySubs = DO_NOT_DISPLAY_SUBTITLES
					ELSE
						eDisplaySubs = DISPLAY_SUBTITLES
					ENDIF
					
					// Close enough prompts.
					IF fDistance < 20.0
						// Over here!
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WCAR", CONV_PRIORITY_MEDIUM, eDisplaySubs)
								iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
							ENDIF
						ELSE
							INT iRand
							iRand = GET_RANDOM_INT_IN_RANGE()
							IF (iRand < 20000)
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_TOOFAR", CONV_PRIORITY_MEDIUM, eDisplaySubs)
									iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
								ENDIF
							ELIF (iRand < 40000)
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_MOVE", CONV_PRIORITY_MEDIUM, eDisplaySubs)
									iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WLK1", CONV_PRIORITY_MEDIUM, eDisplaySubs)
									iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
								ENDIF
							ENDIF
						ENDIF
					ELIF fDistance < 50.0
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_LEAVE", CONV_PRIORITY_VERY_HIGH, eDisplaySubs)
							iReturnTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 12000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		// Hunter is set here when skipping
		CASE HS_TELL_ABOUT_FIRST_ELK
			IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
				IF NOT IS_HELP_MESSAGE_ON_SCREEN()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT bHunterDoneConvo[HTC_ARRIVE_FIRST_ELK] 
						// Alright, this is a prime area here...
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_HUNT", CONV_PRIORITY_MEDIUM)
							SETTIMERA(0)
							bHunterDoneConvo[HTC_ARRIVE_FIRST_ELK] = TRUE
							bHunter_PointedAtHuntingArea = FALSE
						ENDIF
					ELIF bHunterDoneConvo[HTC_ARRIVE_FIRST_ELK]
						IF NOT bHunter_PointedAtHuntingArea AND TIMERA() > 333
							IF DOES_ENTITY_EXIST(mHunter)
							AND NOT IS_ENTITY_DEAD(mHunter)
								SEQUENCE_INDEX siPoint
								OPEN_SEQUENCE_TASK(siPoint)
									TASK_TURN_PED_TO_FACE_COORD(NULL, <<-607.1139, 5851.3716, 23.0589>>)
									TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_point", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, AF_SECONDARY|AF_UPPERBODY)
								CLOSE_SEQUENCE_TASK(siPoint)
								
								TASK_PERFORM_SEQUENCE(mHunter, siPoint)
								CLEAR_SEQUENCE_TASK(siPoint)
							ENDIF
							TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1, SLF_SLOW_TURN_RATE)
							bHunter_PointedAtHuntingArea = TRUE
						ENDIF
					
						txtLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						IF ARE_STRINGS_EQUAL(txtLine, "HT2_HUNT_3")
						OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							ALTER_STEALTH(mHunter, TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, <<-605.3600, 5856.5493, 22.7741>>, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 239.6328)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHunter, FALSE)	
							
							CDEBUG1LN(DEBUG_HUNTING, "HS_TELL_ABOUT_FIRST_ELK - Hunter going to first elk wait pos.")
							eHunterState = HS_WAIT_KILL_FIRST_ELK
						ENDIF
					ENDIF						
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = PERFORMING_TASK
					iWPR_0_1_Progress = GET_PED_WAYPOINT_PROGRESS(mHunter)
				ENDIF
				
				eHunterSubState = HSS_INIT
				ePreState = HS_TELL_ABOUT_FIRST_ELK
				eHunterState = HS_WAIT_FOR_PLAYER_TO_RETURN
			ENDIF
			
			// If somehow the player kills the elk here, we need to FF so that he gets credit.
			IF bSpawned1stElk
				IF NOT IS_PED_UNINJURED(mElk[FIRST_ELK].mPed)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					bHunterDoneConvo[HTC_GOOD_SHOT_FIRST_ELK]= FALSE
					eHunterState = HS_WAIT_KILL_FIRST_ELK
					eHunterSubState = HSS_INIT
				ENDIF
			ENDIF
		BREAK
		
		CASE HS_WAIT_KILL_FIRST_ELK
			// We can't print an objective until the hunter is in position 1...
			IF NOT bHunter_InPosition1
				CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_KILL_FIRST_ELK - Waiting for hunter to be in position...")
				bHunter_InPosition1 = VDIST(GET_ENTITY_COORDS(mHunter), <<-605.3600, 5856.5493, 22.7741>>) < 4.0
				TASK_LOOK_AT_ENTITY(mHunter, mElk[FIRST_ELK].mPed, -1, SLF_SLOW_TURN_RATE)
			ENDIF
	
			// Player not has killed the elk yet....
			IF IS_PED_UNINJURED(mElk[FIRST_ELK].mPed)
				CDEBUG2LN(DEBUG_HUNTING, "HS_WAIT_KILL_FIRST_ELK - Elk not yet dead")
				
				IF NOT mElk[FIRST_ELK].bIsSpooked
					IF bShownHuntElkGodText[FIRST_ELK]
						// Comment if the player is using the right gun or not...
						DO_GUN_ADVICE_CONV(FIRST_ELK)
						HANDLE_AIM_CONVO(FIRST_ELK)
						IF bHunter_InPosition1
							CDEBUG2LN(DEBUG_HUNTING, "HS_WAIT_KILL_FIRST_ELK - Cletus in position, can encourage.")
							PROCESS_ENCOURAGEMENT_LINES(FIRST_ELK)
						ENDIF
					ENDIF
				ELSE
					// you spooked him!
					IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_SPOOK", CONV_PRIORITY_LOW)
						KILL_ANY_CONVERSATION()
						ALTER_STEALTH(mHunter, FALSE)
						eHunterState = HS_NULL
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_KILL_FIRST_ELK - Elk dead!")
				
				// Clear the god text
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FIRST_ELK].mPed)	
				
				IF NOT bShotWithWrongGun
				AND NOT mElk[FIRST_ELK].bIsSpooked
					
					IF NOT bHunterDoneConvo[HTC_GOOD_SHOT_FIRST_ELK]
									
						// Boom! Great shot son...
						sKillString = GET_KILL_COMMENT(FIRST_ELK)
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, sKillString, CONV_PRIORITY_MEDIUM)
							
							ALTER_STEALTH(mHunter, FALSE)
							TASK_CLEAR_LOOK_AT(mHunter)
							TASK_TURN_PED_TO_FACE_ENTITY(mHunter, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1)
							bHunterDoneConvo[HTC_GOOD_SHOT_FIRST_ELK] = TRUE
							eHunterState = HS_AFTER_KILL_FIRST_ELK
						ENDIF
					ENDIF
				ENDIF					
			ENDIF

		BREAK
		
		CASE HS_AFTER_KILL_FIRST_ELK
			IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
				IF NOT bHunterDoneConvo[HTC_LEAD_TO_SECOND_ELK]
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_PLAYER_FREE_AIMING(PLAYER_ID())
						CDEBUG1LN(DEBUG_HUNTING, "HS_AFTER_KILL_FIRST_ELK -> HS_LEAD_TO_SECOND_ELK")
						eHunterState = HS_LEAD_TO_SECOND_ELK
						eHunterSubState = HSS_INIT
					ENDIF
				ENDIF
			ELSE
				//bGotReturnTimer = FALSE
				eHunterSubState = HSS_INIT
				
				CDEBUG1LN(DEBUG_HUNTING, "HS_AFTER_KILL_FIRST_ELK -> HS_WAIT_FOR_PLAYER_TO_RETURN")
				ePreState = HS_AFTER_KILL_FIRST_ELK
				eHunterState = HS_WAIT_FOR_PLAYER_TO_RETURN
			ENDIF	
		BREAK
		
		CASE HS_LEAD_TO_SECOND_ELK
			SWITCH eHunterSubState
				CASE HSS_INIT
					// Alright, stick with me and lead us further in.
					IF (iWPR_1_2_Progress = 0)
						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Progress 0, will trigger convo about next elk.")
						
						IF NOT bHunterDoneConvo[HTC_LEAD_TO_SECOND_ELK]
							IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WHISTLE2", CONV_PRIORITY_MEDIUM)
								CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Convo triggered HT2_WHISTLE2")
								
								sCurConvo = "HT2_WHISTLE2"
								bHunterDoneConvo[HTC_LEAD_TO_SECOND_ELK] = TRUE
							ENDIF
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Progress not 0, we'll be picking up the conversation now.")
						CREATE_CONVERSATION_FROM_SPECIFIC_LINE(mHunterConv, sTextBlock, sCurConvo, sCurLine, CONV_PRIORITY_MEDIUM)
					ENDIF
					
					txtLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					IF ARE_STRINGS_EQUAL(txtLine, "HT2_WHISTLE2_1") OR (iWPR_1_2_Progress != 0)
						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Tasking the hunter to move while talking about whistling...")
						
						ALTER_STEALTH(mHunter, TRUE)
						TASK_CLEAR_LOOK_AT(mHunter)
						TASK_FOLLOW_WAYPOINT_RECORDING(mHunter, "HT2_CLE_1_2", iWPR_1_2_Progress, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_TASK_EXACTSTOP)
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHunter, FALSE)
						
						eHunterSubState = HSS_ACTIVE
					ENDIF
				BREAK
				
				CASE HSS_ACTIVE		
					// Has the hunter go to where he needs to be?
					IF NOT bHunterDoneConvo[HTC_WHISTLE_TO_SECOND_ELK]
						IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
							IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = FINISHED_TASK
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									// Alright, give it a good blow...
									IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WHISTLE1", CONV_PRIORITY_MEDIUM)
										CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Telling hunter to tell player to whistle...")
										ALTER_STEALTH(mHunter, FALSE)
										
										SEQUENCE_INDEX iSeq
										OPEN_SEQUENCE_TASK(iSeq)
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
											TASK_PAUSE(NULL, 5000)
											TASK_PLAY_ANIM(NULL, "amb@world_human_guard_patrol@male@idle_a", "idle_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
										CLOSE_SEQUENCE_TASK(iSeq)
										TASK_PERFORM_SEQUENCE(mHunter, iSeq)
										CLEAR_SEQUENCE_TASK(iSeq)
										
										bHunterDoneConvo[HTC_WHISTLE_TO_SECOND_ELK] = TRUE	
										bPlayerCalling = FALSE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							iWPR_1_2_Progress = GET_PED_WAYPOINT_PROGRESS(mHunter)
							//bGotReturnTimer = FALSE
							eHunterSubState = HSS_INIT
							ePreState = HS_LEAD_TO_SECOND_ELK
							eHunterState = HS_WAIT_FOR_PLAYER_TO_RETURN
						ENDIF
					ELSE
						// If the player has blown the whistle, we need to play the convo where he says it sounds funny.
						IF NOT bWhistle_CaughtFirst
							// Intercept the call. Don't allow the elk to respond yet.
							bWhistle_CaughtFirst = bPlayerCalling
							bPlayerCalling = FALSE
							SETTIMERA(0)
						ELIF bWhistle_CaughtFirst AND NOT bWhistle_WaitingOnConvo
							// Player's done calling.
							IF TIMERA() > 2500
								IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_COMM", CONV_PRIORITY_MEDIUM)
											
											CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Trevor commenting on elk whistle.")
											SETTIMERA(0)
											bWhistle_WaitingOnConvo = TRUE
										ENDIF
									ENDIF
								ELSE
									eHunterSubState = HSS_CLEANUP
								ENDIF
							ENDIF
						ELIF bWhistle_WaitingOnConvo
							IF TIMERA() > 5250
								CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK - Triggering elk to call back...")
								bPlayerCalling = TRUE
								eHunterSubState = HSS_CLEANUP
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE HSS_CLEANUP
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_SECOND_ELK -> HS_WAIT_FIND_SECOND_ELK")
						eHunterState = HS_WAIT_FIND_SECOND_ELK
						eHunterSubState = HSS_INIT
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE HS_WAIT_FIND_SECOND_ELK
			IF DOES_BLIP_EXIST(mElk[SECOND_ELK].mElkBlip)
				IF NOT bHunterDoneConvo[HTC_DISCOVER_SECOND_ELK]
					IF NOT bGotResponseDelay
						TASK_CLEAR_LOOK_AT(mHunter)
						TASK_TURN_PED_TO_FACE_COORD(mHunter, vElkSpawn[SECOND_ELK])
						ALTER_STEALTH(mHunter, FALSE)
						
						iResponseDelay = GET_GAME_TIMER() + 2000
						bGotResponseDelay = TRUE
					ELSE
						IF GET_GAME_TIMER() > iResponseDelay
							// Okay, you hear that? He's over there...
							IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WLK2", CONV_PRIORITY_LOW)
								
								REPLAY_RECORD_BACK_FOR_TIME(1.0, 5.0)
								
								CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_SECOND_ELK - Telling Trevor to track elk.")
								
								bGotBlipFadeTime = FALSE
								TASK_LOOK_AT_ENTITY(mHunter, mElk[SECOND_ELK].mPed, -1)
								SETTIMERA(0)
								
								bHunterDoneConvo[HTC_DISCOVER_SECOND_ELK] = TRUE
								CPRINTLN(DEBUG_HUNTING, "HS_WAIT_KILL_SECOND_ELK")
								eHunterState = HS_WAIT_KILL_SECOND_ELK
								eHunterSubState = HSS_INIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF		
			
			// If somehow the player kills the elk here, we need to FF so that he gets credit.
			IF NOT IS_PED_UNINJURED(mElk[SECOND_ELK].mPed)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				bHunterDoneConvo[HTC_GOOD_SHOT_SECOND_ELK] = FALSE
				bHunter_TaskedAfterKill2 = FALSE
				
				eHunterState = HS_WAIT_KILL_SECOND_ELK
				eHunterSubState = HSS_INIT
			ENDIF
		BREAK
				
		CASE HS_WAIT_KILL_SECOND_ELK
			IF IS_PED_UNINJURED(mElk[SECOND_ELK].mPed)
				IF NOT bHunter_PointedAtElk2
				AND TIMERA() > 1000
					VECTOR vBetween
					vBetween = NORMALISE_VECTOR(GET_ENTITY_COORDS(mElk[SECOND_ELK].mPed) - GET_ENTITY_COORDS(mHunter))
					
					VECTOR vFwd
					vFwd = GET_ENTITY_FORWARD_VECTOR(mHunter)
					FLOAT fDot
					fDot = DOT_PRODUCT(vFwd, vBetween)
					CDEBUG3LN(DEBUG_HUNTING, "Dot: ", fDot)
					IF (fDot > 0.92)
						TASK_PLAY_ANIM(mHunter, "oddjobs@hunter", "point_fwd", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, AF_SECONDARY|AF_UPPERBODY)
						bHunter_PointedAtElk2 = TRUE
						iFirstElkTimer = 0
					ENDIF
				ENDIF
				
				IF NOT mElk[SECOND_ELK].bIsSpooked
					// Hunter can only comment on players actions after the god text has displayed
					IF bShownHuntElkGodText[SECOND_ELK]
				
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HT_OBJ") // Don't trample the "Hunt the Elk" text.
						
							// Comment if the player is using the right gun or not...
							DO_GUN_ADVICE_CONV(SECOND_ELK)
							HANDLE_AIM_CONVO(SECOND_ELK)
							HANDLE_BLIP_FADE(SECOND_ELK)
							PROCESS_ENCOURAGEMENT_LINES(SECOND_ELK)
						ENDIF
					ENDIF
				ELSE
					// You spooked him!
					IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_SPOOK", CONV_PRIORITY_LOW)
						KILL_ANY_CONVERSATION()
						ALTER_STEALTH(mHunter, FALSE)
						eHunterState = HS_NULL
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_KILL_SECOND_ELK - Player killed second elk!")
				
				// Clear the god text
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[SECOND_ELK].mPed)
				
				// Player has just killed second elk...
				IF NOT bHunterDoneConvo[HTC_GOOD_SHOT_SECOND_ELK]
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT bShotWithWrongGun
						AND NOT mElk[SECOND_ELK].bIsSpooked
							IF NOT bHunter_TaskedAfterKill2
								ALTER_STEALTH(mHunter, FALSE)
								IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
									TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHunterThirdKillHead)
								ENDIF
								sKillString = GET_KILL_COMMENT(SECOND_ELK)
								bHunter_TaskedAfterKill2 = TRUE
							ELSE
								IF IS_PLAYER_NEAR_HUNTER(VERY_NEAR_HUNTER_RANGE)
									CLEAR_PED_TASKS(mHunter)
									TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1)
							
									// Good kill, son!
									IF CREATE_CONVERSATION(mHunterConv, sTextBlock, sKillString, CONV_PRIORITY_MEDIUM)
										iTimerDelay = GET_GAME_TIMER() + 10000
										bHunterDoneConvo[HTC_GOOD_SHOT_SECOND_ELK] = TRUE
										
										CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_KILL_SECOND_ELK - Cletus commenting on Elk Kill #2, -> HS_AFTER_KILL_SECOND_ELK")
										
										REPLAY_RECORD_BACK_FOR_TIME(2.5, 5.0, REPLAY_IMPORTANCE_HIGHEST)
										
										eHunterState = HS_AFTER_KILL_SECOND_ELK
										eHunterSubState = HSS_INIT
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE HS_AFTER_KILL_SECOND_ELK
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
					IF NOT bHunterDoneConvo[HTC_LEAD_TO_THIRD_ELK]
						IF NOT IS_PED_INJURED(mHunter)
							CLEAR_PED_TASKS(mHunter)
							TASK_CLEAR_LOOK_AT(mHunter)
						ENDIF
						
						eHunterSubState = HSS_INIT
						eHunterState = HS_LEAD_TO_THIRD_ELK
					ENDIF			
				ENDIF
			ENDIF
		BREAK
		
		CASE HS_LEAD_TO_THIRD_ELK
			SWITCH eHunterSubState
				CASE HSS_INIT
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
						// Alright, stick with me and lead us further in.
						IF (iWPR_2_3_Progress = 0)
							IF NOT bHunterDoneConvo[HTC_LEAD_TO_THIRD_ELK]
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT_PAIR", CONV_PRIORITY_MEDIUM)
									sCurConvo = "HT_PAIR"
									bHunterDoneConvo[HTC_LEAD_TO_THIRD_ELK] = TRUE
								ENDIF
							ENDIF
						ELSE
							CREATE_CONVERSATION_FROM_SPECIFIC_LINE(mHunterConv, sTextBlock, sCurConvo, sCurLine, CONV_PRIORITY_MEDIUM)
						ENDIF

						CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_THIRD_ELK - Telling Cletus to start moving...")	
						ALTER_STEALTH(mHunter, TRUE)
						TASK_LOOK_AT_COORD(mHunter, vElkSpawn[THIRD_ELK], -1)
						TASK_FOLLOW_WAYPOINT_RECORDING(mHunter, "HT2_CLE_2_3", iWPR_2_3_Progress, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
						
						eHunterSubState = HSS_ACTIVE
					ENDIF
				BREAK
				
				CASE HSS_ACTIVE
					// Remember, we're only here for the male elk..
					IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT bHunterDoneConvo[HTC_WHISTLE_TO_THIRD_ELK]
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT_LEAD3", CONV_PRIORITY_LOW)
									CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_THIRD_ELK - Cletus talking about Males only...")
									bHunterDoneConvo[HTC_WHISTLE_TO_THIRD_ELK] = TRUE	
								ENDIF
							ENDIF
						ENDIF
					
						IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = FINISHED_TASK
							IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
								TASK_CLEAR_LOOK_AT(mHunter)
								TASK_TURN_PED_TO_FACE_ENTITY(mHunter, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1)
								
								eHunterSubState = HSS_CLEANUP
							ENDIF
						ENDIF
					ELSE
						IF eMissionState = MS_FOLLOW_TO_THIRD_ELK
							iWPR_2_3_Progress = GET_PED_WAYPOINT_PROGRESS(mHunter)
							//bGotReturnTimer = FALSE
							eHunterSubState = HSS_INIT
							ePreState = HS_LEAD_TO_THIRD_ELK
							eHunterState = HS_WAIT_FOR_PLAYER_TO_RETURN
						ELSE
							CDEBUG1LN(DEBUG_HUNTING, "HS_LEAD_TO_THIRD_ELK moving to substate cleanup, mission is no longer in this state.")
							eHunterSubState = HSS_CLEANUP
						ENDIF
					ENDIF
				BREAK
				
				CASE HSS_CLEANUP
					eHunterSubState = HSS_INIT
					eHunterState = HS_WAIT_FIND_THIRD_ELK
				BREAK
			ENDSWITCH
		BREAK
				
		CASE HS_WAIT_FIND_THIRD_ELK
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND DOES_BLIP_EXIST(mElk[THIRD_ELK].mElkBlip)
				IF NOT bHunter_WaitWhistleKill3
					CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_THIRD_ELK - Caught whistle, triggering NICE_LITTLE_PAUSE")
					SETTIMERA(0)
					bHunter_WaitWhistleKill3 = TRUE
				ELIF TIMERA() > NICE_LITTLE_PAUSE
					IF NOT bHunterDoneConvo[HTC_DISCOVER_THIRD_ELK]
						// Same as before boy...
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_ARR3", CONV_PRIORITY_LOW)
							CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_THIRD_ELK - Cletus speech: 'Same as before...'")
						
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGH)
							
							SAFE_REMOVE_BLIP(mHunterBlip)
							bHunterDoneConvo[HTC_DISCOVER_THIRD_ELK] = TRUE
							
							ALTER_STEALTH(mHunter, TRUE)
							GROUP_INDEX giPlayer
							giPlayer = GET_PED_GROUP_INDEX(PLAYER_PED_ID())
							SET_PED_AS_GROUP_MEMBER(mHunter, giPlayer)
						//	IS_PED_IN_GROUP
						//	TASK_FOLLOW_TO_OFFSET_OF_ENTITY(mHunter, PLAYER_PED_ID(), <<0.1, -5.0, 0.0>>, PEDMOVEBLENDRATIO_WALK, -1, 7.0)
							iFirstElkTimer = 0
							
							eHunterSubState = HSS_INIT
							eHunterState = HS_WAIT_KILL_THIRD_ELK
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
			
			// If the player kills the elk before Cletus is ready...
			IF DOES_ENTITY_EXIST(mElk[THIRD_ELK].mPed)
				IF NOT IS_PED_UNINJURED(mElk[THIRD_ELK].mPed)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					bHunterDoneConvo[HTC_GOOD_SHOT_THIRD_ELK] = FALSE
					bHunter_TaskedAfterKill2 = FALSE
					
					eHunterState = HS_WAIT_KILL_THIRD_ELK
					eHunterSubState = HSS_INIT
					BREAK
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(mElk[DOE_ELK].mPed)
				IF NOT IS_PED_UNINJURED(mElk[DOE_ELK].mPed)
					// I said no females!
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_SHDOE", CONV_PRIORITY_MEDIUM) 
						IF NOT IS_PED_INJURED(mHunter)
							ALTER_STEALTH(mHunter, FALSE)
							CLEAR_PED_TASKS(mHunter)
							TASK_WANDER_STANDARD(mHunter)
							SET_PED_KEEP_TASK(mHunter, TRUE)
							
							MISSION_FAILED(FR_SHOT_DOE)
							eHunterState = HS_NULL
							BREAK
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
			
		CASE HS_WAIT_KILL_THIRD_ELK
			IF IS_PED_UNINJURED(mElk[THIRD_ELK].mPed)
				IF NOT mElk[THIRD_ELK].bIsSpooked
					// Make the hunter stand up and leg it if the player kill wrong elk.
					IF NOT IS_PED_UNINJURED(mElk[DOE_ELK].mPed)
						CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_THIRD_ELK - Player killed doe elk!!!")
						
						// I said no females!
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_SHDOE", CONV_PRIORITY_MEDIUM) 
							IF NOT IS_PED_INJURED(mHunter)
								ALTER_STEALTH(mHunter, FALSE)
								CLEAR_PED_TASKS(mHunter)
								TASK_WANDER_STANDARD(mHunter)
								SET_PED_KEEP_TASK(mHunter, TRUE)
								
								MISSION_FAILED(FR_SHOT_DOE)
								eHunterState = HS_NULL
							ENDIF
						ENDIF
					ENDIF
				
					IF bShownHuntElkGodText[THIRD_ELK]
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ")
						// Comment if the player is using the right gun or not...
						DO_GUN_ADVICE_CONV(THIRD_ELK)
						HANDLE_AIM_CONVO(THIRD_ELK)
						HANDLE_AIM_CONVO(DOE_ELK)
						HANDLE_BLIP_FADE(THIRD_ELK)
						
						PROCESS_ENCOURAGEMENT_LINES(THIRD_ELK)
					ENDIF
				ELSE
					// You spooked him!
					IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_SPOOK", CONV_PRIORITY_LOW)
						CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_THIRD_ELK - Player spooked third elk!")
						
						KILL_ANY_CONVERSATION()
						ALTER_STEALTH(mHunter, FALSE)
						eHunterState = HS_NULL
					ENDIF
				ENDIF
			ELSE
				
				CDEBUG1LN(DEBUG_HUNTING, "HS_WAIT_FIND_THIRD_ELK - Player killed third elk!")
				
				// Clear the god text
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[THIRD_ELK].mPed)
				
				IF NOT bShotWithWrongGun
					// Boom, good kill son!
					IF NOT bHunterDoneConvo[HTC_GOOD_SHOT_THIRD_ELK]
											
						sKillString = GET_KILL_COMMENT(THIRD_ELK)
						IF CREATE_CONVERSATION(mHunterConv, sTextBlock, sKillString, CONV_PRIORITY_MEDIUM)
							CLEAR_PED_TASKS_IMMEDIATELY(mHunter)
							ALTER_STEALTH(mHunter, FALSE)
							bHunterDoneConvo[HTC_GOOD_SHOT_THIRD_ELK] = TRUE
						ENDIF
					ELSE
						IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT bHunterDoneConvo[HTC_TELL_ABOUT_TEXTING]
								// Right, you're on your own for this last elk...
								IF CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_WLKOFF", CONV_PRIORITY_MEDIUM)
									IF NOT IS_PED_INJURED(mHunter)
										CLEAR_PED_TASKS(mHunter)
										TASK_TURN_PED_TO_FACE_ENTITY(mHunter, PLAYER_PED_ID(), -1)
										TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1)
										bHunterDoneConvo[HTC_TELL_ABOUT_TEXTING] = TRUE
									
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
										eHunterState = HS_AFTER_KILL_THIRD_ELK
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(mHunter, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
								TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, mElk[THIRD_ELK].vPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHunterThirdKillHead)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
						
		BREAK
		
		CASE HS_AFTER_KILL_THIRD_ELK
			txtLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL() 
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR ARE_STRINGS_EQUAL(txtLine, "HT2_WLKOFF_4")
				TASK_CLEAR_LOOK_AT(mHunter)
				SAFE_REMOVE_BLIP(mHunterBlip)
				ALTER_STEALTH(mHunter, FALSE)
				TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, vHunterStartPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHunterThirdKillHead)
				eHunterState = HS_RELEASE_WHEN_OFF_CAMERA
			ENDIF
		BREAK
			
		CASE HS_RELEASE_WHEN_OFF_CAMERA
			IF DOES_ENTITY_EXIST(mHunter)
				IF IS_ENTITY_OCCLUDED(mHunter)
				AND NOT IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
					SET_PED_AS_NO_LONGER_NEEDED(mHunter)
					SAFE_DELETE_PED(mHunter)
					eHunterState = HS_NULL
				ELSE
					IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
						// Monitor the player following Cletus...
						IF (iCletusFollowTimer = 0)
							iCletusFollowTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(CLETUS_FOLLOW_COMMENT_MIN, CLETUS_FOLLOW_COMMENT_MAX)
						ELSE
							IF iCletusFollowTimer < GET_GAME_TIMER()
								CLEAR_PED_TASKS(mHunter)
								TASK_TURN_PED_TO_FACE_ENTITY(mHunter, PLAYER_PED_ID(), -1)
								TASK_LOOK_AT_ENTITY(mHunter, PLAYER_PED_ID(), -1)
								SETTIMERA(0)
								eHunterState = HS_YELL_AT_PLAYER_FOLLOWING
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE HS_YELL_AT_PLAYER_FOLLOWING	
			IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
				VECTOR vBetween
				vBetween = NORMALISE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(mHunter))
				
				VECTOR vFwd
				vFwd = GET_ENTITY_FORWARD_VECTOR(mHunter)
				FLOAT fDot
				fDot = DOT_PRODUCT(vFwd, vBetween)
				CDEBUG3LN(DEBUG_HUNTING, "Dot: ", fDot)
				
				IF fDot > 0.92
					IF NOT bHunter_WaitToLeave
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
							CREATE_CONVERSATION(mHunterConv, sTextBlock, "HT2_PLYFOL", CONV_PRIORITY_MEDIUM)
							iCletusFollowTimer = 0
							SETTIMERA(0)
							bHunter_WaitToLeave = TRUE
						ENDIF
					ELSE
						IF TIMERA() > 1750
							TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, vHunterStartPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHunterThirdKillHead)
							eHunterState = HS_RELEASE_WHEN_OFF_CAMERA
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT bHunter_WaitToLeave
					SETTIMERA(0)
					bHunter_WaitToLeave = TRUE
				ELSE
					IF TIMERA() > 500
						TASK_FOLLOW_NAV_MESH_TO_COORD(mHunter, vHunterStartPos, PEDMOVEBLENDRATIO_WALK, 60000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fHunterThirdKillHead)
						eHunterState = HS_RELEASE_WHEN_OFF_CAMERA
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
			
	IF DOES_ENTITY_EXIST(mHunter)
		IF eMissionState != MS_MISSION_FAILED
//		AND eHunterState != HS_RELEASE_WHEN_OFF_CAMERA
		AND eHunterState != HS_NULL
			IF NOT bGotFailDelay	
				IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
				OR IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
					IF IS_PED_RAGDOLL(mHunter)
						KILL_ANY_CONVERSATION()
						iFailDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
						bGotFailDelay = TRUE
					ENDIF
				ENDIF
				
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mHunter, PLAYER_PED_ID())
				OR HAS_PED_BEEN_DAMAGED_BY_WEAPON(mHunter, WEAPONTYPE_INVALID , GENERALWEAPON_TYPE_ANYWEAPON)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(mHunter, FALSE), 10.0)
					KILL_ANY_CONVERSATION()
					iFailDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
					bGotFailDelay = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > iFailDelay
					IF NOT IS_PED_INJURED(mHunter)
						CLEAR_PED_TASKS(mHunter)
						TASK_SMART_FLEE_PED(mHunter, PLAYER_PED_ID(), 100, -1)
						SET_PED_KEEP_TASK(mHunter, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(mHunter)
						CPRINTLN(DEBUG_MISSION, "Mission Failed hunter injured Called from Hunter state ")
						MISSION_FAILED(FR_HUNTER_INJURED)
					ELSE
						CPRINTLN(DEBUG_MISSION, "Mission Failed hunter killed Called from hunter state ")
						MISSION_FAILED(FR_HUNTER_KILLED)
					ENDIF
					eHunterState = HS_NULL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

																// PLAYER STATES

/// PURPOSE:
///    Initiate the mission variables and states.
PROC INIT_MISSION()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	SET_PLAYER_CAN_BE_HASSLED_BY_GANGS(PLAYER_ID(), FALSE)
	SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_MOUNTAIN_LION_WANDER", FALSE)
	
	INIT_PC_SCRIPTED_CONTROLS("HUNTING ELK CALL")
	
	ADD_PED_FOR_DIALOGUE(mHunterConv, 2, PLAYER_PED_ID(), "TREVOR")
	ADD_PED_FOR_DIALOGUE(mHunterConv, 3, mHunter, "CLETUS")
	
	CREATE_REL_GROUP()
	POPULATE_ELK()
	REQUEST_MISSION_TEXT()
	REQUEST_ELK_ASSETS()
	SERVICES_TOGGLE(FALSE)
	RESET_ALL_HUNTER_CONVOS()
	RESET_ALL_CONVO_FLAGS()
	REQUEST_AUDIO_ASSETS() // Comment this back in once it made it into the build.
	
	//START_AUDIO_SCENE("AMBIENT_HUNTING_MIX")
	
	REQUEST_STREAMED_TEXTURE_DICT("Hunting")
	REQUEST_ANIM_DICT("facials@p_m_one@variations@elkcall")
	REQUEST_ANIM_DICT("gestures@m@standing@casual")
	REQUEST_ANIM_DICT("amb@world_human_guard_patrol@male@idle_a")
	REQUEST_ANIM_DICT("oddjobs@hunter")
	
	REQUEST_WAYPOINT_RECORDING("HT2_CLE_0_1")
	REQUEST_WAYPOINT_RECORDING("HT2_CLE_1_2")
	REQUEST_WAYPOINT_RECORDING("HT2_CLE_2_3")
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_HUNTER, TREVOR_BOOK, FALSE)	
	
	IF NOT IS_PED_INJURED(mHunter)
		SET_PED_DIES_IN_WATER(mHunter, FALSE)
		SET_PED_DEFAULT_COMPONENT_VARIATION(mHunter)				
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mHunter, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHunter, FALSE)
		GIVE_WEAPON_TO_PED(mHunter, WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, FALSE)	
	ENDIF
			
	bAllowCall = FALSE
	bSuppressNearMissMessage = TRUE

	fWindDirection = 0.0
	
	START_AUDIO_SCENE("AMBIENT_HUNTING_MIX")
	
	// Block all those damn bushes.
	ADD_NAVMESH_BLOCKING_OBJECT(<<-641.6511, 5846.5786, 20.3501>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-642.2120, 5851.6021, 19.9536>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-627.1213, 5833.1846, 22.5278>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-636.4395, 5829.7349, 22.0155>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-607.6405, 5834.4507, 24.6594>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-594.2007, 5834.4136, 27.2229>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-589.8816, 5832.7466, 28.4124>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-586.3449, 5834.0439, 29.0145>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-581.4914, 5839.4307, 29.0466>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-587.1396, 5844.6973, 27.3273>>, << 3, 3, 3>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-601.9974, 5850.2031, 24.0529>>, << 4, 4, 4>>, 0.0)	
	ADD_NAVMESH_BLOCKING_OBJECT(<<-634.3944, 5857.5425, 20.5354>>, << 4, 4, 4>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-637.6848, 5858.0264, 20.1080>>, << 2, 2, 2>>, 0.0)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-599.4448, 5841.0630, 25.5058>>, << 3, 3, 3>>, 0.0)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "STARTING WIND DIRECTION IS ", fWindDirection)
	#ENDIF
			
	#IF IS_DEBUG_BUILD
		s_skip_menu[0].sTxtLabel = "Go to Hunting Location"    
		s_skip_menu[1].sTxtLabel = "First Elk"
		s_skip_menu[2].sTxtLabel = "Second Elk"
		s_skip_menu[3].sTxtLabel = "Third Elk"
		s_skip_menu[4].sTxtLabel = "Fourth Elk"
	#ENDIF
	
	INT iReplayState
	
	IF IS_REPLAY_IN_PROGRESS()
		CPRINTLN(DEBUG_MISSION, "*Replay in progress*")
		
		iReplayState = GET_REPLAY_MID_MISSION_STAGE()
		
		eSubState = SS_INIT
		
		// Allow shit skipping past the intro.
		IF g_bShitSkipAccepted //AND iReplayState > CP_AFTER_INTRO
			iReplayState++
		ENDIF
		
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		RESET_ALL_HUNTER_CONVOS()
		RESET_ALL_CONVO_FLAGS()
		
		CPRINTLN(DEBUG_MISSION, "iReplay State = ", iReplayState)
		
		SWITCH iReplayState				
			CASE CP_AFTER_INTRO
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - MS_SET_UP_AFTER_INTRO")
				START_REPLAY_SETUP(vPlayerStartPos, fPlayerStartHead)		// needs to be a point after the mocap has played.
				eMissionState = MS_SET_UP_AFTER_INTRO	
			BREAK	
			
			CASE CP_AFTER_TUTORIAL
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - CP_AFTER_TUTORIAL")
				START_REPLAY_SETUP(vPlayerStartHuntPos, fPlayerStartHuntHead)
				eMissionState = MS_SET_UP_BEFORE_FIRST_ELK	
			BREAK
			
			CASE CP_AFTER_FIRST_ELK
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - CP_AFTER_FIRST_ELK")
				START_REPLAY_SETUP(vPlayerFirstKillPos, fPlayerFirstKillHead)	
				eMissionState = MS_SET_UP_BEFORE_SECOND_ELK	
			BREAK
			
			CASE CP_AFTER_SECOND_ELK	
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - CP_AFTER_SECOND_ELK")
				START_REPLAY_SETUP(vPlayerSecondKillPos, fPlayerSecondKillHead)
				eMissionState = MS_SET_UP_BEFORE_THIRD_ELK	
			BREAK
			
			CASE CP_AFTER_THIRD_ELK
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - CP_AFTER_THIRD_ELK")
				START_REPLAY_SETUP(vPlayerThirdKillPos, fPlayerThirdKillHead)
				eMissionState = MS_SET_UP_BEFORE_FOURTH_ELK		
			BREAK
			
			CASE CP_AFTER_FOURTH_ELK
				CPRINTLN(DEBUG_MISSION, "REPLAY IN PROGRESS - CP_AFTER_FOURTH_ELK")
				START_REPLAY_SETUP(vPlayerSecondKillPos, fPlayerSecondKillHead)
				eMissionState = MS_SET_UP_BEFORE_MISSION_PASS
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Replay in progress - unknown checkpoint selected")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_HUNTING, "MS_RUN_INTRO")
		eHunterState = HS_NULL
		eMissionState = MS_RUN_INTRO
		eSubState = SS_INIT
	ENDIF				
ENDPROC

															// Mission Player States

PROC RUN_INTRO()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-681.3436, 5843.4287, 16.9395>>) < 2.0
				iCutSceneState = 0
				RC_REQUEST_CUTSCENE("HUN_2_MCS_1")
				CPRINTLN(DEBUG_HUNTING, "RUN_INTRO Active")
				eSubState = SS_ACTIVE
			//ENDIF
		BREAK
		
		CASE SS_ACTIVE
			SWITCH iCutSceneState
				CASE 0
					CPRINTLN(DEBUG_HUNTING, "INTRO: SS_ACTIVE") 
							
					IF RC_IS_CUTSCENE_OK_TO_START()
						IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sTrevor, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							CPRINTLN(DEBUG_HUNTING, "Registered Trevor for intro")
						ENDIF
						
						IF IS_ENTITY_ALIVE(mHunter)
							REGISTER_ENTITY_FOR_CUTSCENE(mHunter, "Cletus", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Cletus", PLAYER_PED_ID())
							mCletusGun = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(mHunter, WEAPONTYPE_PUMPSHOTGUN)
							mTrevorGun = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(mHunter, WEAPONTYPE_SNIPERRIFLE)
							GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(mTrevorGun, WEAPONCOMPONENT_AT_AR_SUPP_02)
							CPRINTLN(DEBUG_HUNTING, "Registered Cletus for intro")
						ENDIF
												
						IF DOES_ENTITY_EXIST(mCletusGun)
							REGISTER_ENTITY_FOR_CUTSCENE(mCletusGun, "Cletus_Gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							CPRINTLN(DEBUG_HUNTING, "Registered Cletus gun for intro")
						ENDIF
												
						IF DOES_ENTITY_EXIST(mTrevorGun)
							REGISTER_ENTITY_FOR_CUTSCENE(mTrevorGun, "Trevors_Weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							CPRINTLN(DEBUG_HUNTING, "Registered trevors gun for intro")
						ENDIF
												
						// Start mocap scene
						RC_CLEANUP_LAUNCHER()
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						WAIT(0)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vResolveAreaA, vResolveAreaB, RESOLVE_AREA_WIDTH, vResolvePos, fResolveHead)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vResolveAreaA, vResolveAreaC, RESOLVE_AREA_WIDTH_LARGE, vResolvePos, fResolveHead)
						RC_START_CUTSCENE_MODE(vPlayerStartPos, TRUE, FALSE)
						iCutsceneState++
					ENDIF
				BREAK
				
				CASE 1
					
					CPRINTLN(DEBUG_HUNTING, "INTRO: CASE 1") 
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Cletus", P_CLETUS_NECKLACE_S)
						IF NOT bHunterStartedWalking
							CPRINTLN(DEBUG_HUNTING, "Hunter started walking in CASE 1")
							//SAFE_TELEPORT_ENTITY(mHunter, vHunterStartPos, fHunterStartHead)
							eHunterState = HS_LEAD_TO_FIRST_ELK
							bHunterStartedWalking = TRUE
						ENDIF						
					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevors_Weapon", GET_WEAPONTYPE_MODEL(WEAPONTYPE_SNIPERRIFLE))
						CPRINTLN(DEBUG_HUNTING, "giving weapon object to player ")
						GIVE_WEAPON_OBJECT_TO_PED(mTrevorGun, PLAYER_PED_ID())
						IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, GENERALWEAPON_TYPE_INVALID)
							SET_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 10)
						ENDIF
					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Cletus_Gun", GET_WEAPONTYPE_MODEL(WEAPONTYPE_SNIPERRIFLE))
						GIVE_WEAPON_OBJECT_TO_PED(mCletusGun, mHunter)
						FORCE_PED_MOTION_STATE(mHunter, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sTrevor)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 2500)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						bSetIntroExitState = TRUE
					ENDIF				
															
					// unfortunately the only way to ensure the mocap camera blends out to gameplay cam at a sensible position 
					// is to spam this until the player's character exit state has fired
					IF NOT bSetIntroExitState
						RESET_CAM()	
					ENDIF
					
					IF HAS_CUTSCENE_FINISHED()
						REPLAY_STOP_EVENT()
						CPRINTLN(DEBUG_HUNTING, "INTRO: CUTSCENE FINISHED IN CASE 1") 
						iCutsceneState++
					ENDIF
				BREAK
				
				CASE 2
					
					CPRINTLN(DEBUG_HUNTING, "INTRO: CASE 2") 
					
					IF WAS_CUTSCENE_SKIPPED()
					AND IS_SCREEN_FADED_OUT()
						CPRINTLN(DEBUG_HUNTING, "WARPING HUNTER TO CUTSCENE SKIP POS")
						SAFE_TELEPORT_ENTITY(mHunter, vHunterStartPos, fHunterStartHead)
						eHunterState = HS_LEAD_TO_FIRST_ELK
						//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, TRUE)
					ENDIF
					
					IF NOT IS_CUTSCENE_PLAYING()
						CPRINTLN(DEBUG_HUNTING, "CUTSCENE IS NOT PLAYING")
						iCutsceneState++
					ENDIF
				BREAK
				
				CASE 3
					
					CPRINTLN(DEBUG_HUNTING, "INTRO: CASE 3") 
					
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
	                ELSE
	                	IF HAS_CUTSCENE_LOADED()
	                    	REMOVE_CUTSCENE()
	                    ENDIF
						
						IF NOT bHunterStartedWalking
							CPRINTLN(DEBUG_HUNTING, "Hunter start walking from CASE 3") 
							eHunterState = HS_LEAD_TO_FIRST_ELK	
						ENDIF
						
						RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)
						RC_END_CUTSCENE_MODE(TRUE, FALSE)
						
						eSubState = SS_CLEANUP
					ENDIF				
				BREAK	
				
			ENDSWITCH
		
		BREAK
		
		CASE SS_SKIPPED
		
			SAFE_FADE_OUT()
			KILL_ANY_CONVERSATION()
		
			IF IS_SCREEN_FADED_OUT()
			
				IF IS_PED_UNINJURED(mHunter)
					IF NOT bHunterStartedWalking
						CLEAR_PED_TASKS(mHunter)
						IF HAS_PED_GOT_WEAPON(mHunter, WEAPONTYPE_PUMPSHOTGUN)
							SET_CURRENT_PED_WEAPON(mHunter, WEAPONTYPE_PUMPSHOTGUN, TRUE)
						ENDIF
						SAFE_TELEPORT_ENTITY(mHunter, vHunterStartPos, fHunterStartHead)
						eHunterState = HS_LEAD_TO_FIRST_ELK
						bHunterStartedWalking = TRUE
					ENDIF
				ENDIF
				
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				
				RESET_CAM()
				CPRINTLN(DEBUG_HUNTING, "SS_CLEANUP")
				eSubState = SS_CLEANUP			
				
			ENDIF
		BREAK
				
		CASE SS_CLEANUP
			
			//RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)			
			BLIP_HUNTER(TRUE, FALSE)					
			SAFE_FADE_IN()
			
			// Follow Cletus
			PRINT_NOW("HT_FOLLOW", 6500, 0)
						
			CPRINTLN(DEBUG_HUNTING, "SET_REPLAY_MID_MISSION_STAGE_WITH_NAME = CP_AFTER_INTRO")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_INTRO, "Mocap Intro Done")	
			
			CPRINTLN(DEBUG_HUNTING, "MS_FOLLOW_TO_FIRST_ELK")
			eMissionState = MS_FOLLOW_TO_FIRST_ELK
			eSubState = SS_INIT
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

// Should be a point after the intro mocap has played...
PROC SET_UP_AFTER_INTRO()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			SAFE_FADE_OUT()
			CPRINTLN(DEBUG_HUNTING, "SET_UP_AFTER_INTRO - SS_ACTIVE")
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			IF IS_SCREEN_FADED_OUT()
				IF NOT IS_REPLAY_BEING_SET_UP()
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerStartPos, fPlayerStartHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerStartPos)
					RESET_CAM()
				ENDIF
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_INTRO, "Mocap Intro Done")
				
				SAFE_TELEPORT_ENTITY(mHunter, vHunterStartPos, fHunterStartHead)
				RESET_ALL_HUNTER_CONVOS()
				SAFE_GIVE_SNIPER_TO_PLAYER()
				
				CPRINTLN(DEBUG_HUNTING, "SET_UP_AFTER_INTRO - SS_CLEANUP")
				eSubState = SS_CLEANUP
			ENDIF
			
		BREAK	
		
		CASE SS_CLEANUP
			END_REPLAY_SETUP()	
			SAFE_FADE_IN()	
			BLIP_HUNTER(TRUE, FALSE)
				
			eHunterState = HS_LEAD_TO_FIRST_ELK	
			eHunterSubState = HSS_INIT
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
			
			// Follow Cletus
			PRINT_NOW("HT_FOLLOW", 6500, 0)
												
			CPRINTLN(DEBUG_HUNTING, "eMission State = MS_FOLLOW_TO_FIRST_ELK")
			eMissionState = MS_FOLLOW_TO_FIRST_ELK
			eSubState = SS_INIT	
	
		BREAK
				
	ENDSWITCH
						
ENDPROC

PROC SET_UP_BEFORE_FIRST_ELK()
	CDEBUG1LN(DEBUG_HUNTING, "SET_UP_BEFORE_FIRST_ELK")
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			SAFE_FADE_OUT()
			SAFE_DELETE_ALL_ELK()
			
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			IF IS_SCREEN_FADED_OUT()				
				REMOVE_ALL_BLIPS()
				SAFE_GIVE_SNIPER_TO_PLAYER(FALSE)
				IF NOT IS_REPLAY_BEING_SET_UP() 
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerStartHuntPos, fPlayerStartHuntHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerStartHuntPos)
				ENDIF
				
				IF NOT IS_PED_INJURED(mHunter)
					CLEAR_PED_TASKS(mHunter)
				ENDIF
				
				SAFE_TELEPORT_ENTITY(mHunter, vHunterStartHuntPos, fHunterStartHuntHead)	
				eSubState = SS_CLEANUP						
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
				
				END_REPLAY_SETUP()		
				SAFE_FADE_IN()
				
				RESET_CAM()
				KILL_ANY_CONVERSATION()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

				// To make the scale form arrow appear.
				bCanUpdateWindBlip = TRUE
				SHOW_THIS_HELP(WIND_ARROW_HELP)
				
				CPRINTLN(DEBUG_HUNTING, "HS_TELL_ABOUT_FIRST_ELK from skip")
				eHunterSubState = HSS_INIT
				eHunterState = HS_TELL_ABOUT_FIRST_ELK
				
				CPRINTLN(DEBUG_HUNTING, "FOLLOW_TO_FIRST_ELK from skip")
				eSubState = SS_INIT
				eMissionState = MS_FOLLOW_TO_FIRST_ELK
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC FOLLOW_TO_FIRST_ELK()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF NOT mElk[FIRST_ELK].bIsSpooked
					IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
						IF bShownReturnToHunter
							BLIP_HUNTER(FALSE, FALSE)
							SAFE_CLEAR_THIS_PRINT("HT_BAC")
							bShownReturnToHunter = FALSE
						ENDIF
												
						// Right son, lets try and bag us three elk
						IF bHunterDoneConvo[HTC_ARRIVE_FIRST_ELK]
							bSpawned1stElk = FALSE
							IF SPAWNED_THIS_ELK(FIRST_ELK)
								bSpawned1stElk = TRUE
								CDEBUG1LN(DEBUG_HUNTING, "Spawned first elk!")
								eSubState = SS_CLEANUP
							ENDIF
						ENDIF
					ELSE
						CHECK_PLAYER_FLED()
						IF NOT bShownReturnToHunter
							BLIP_HUNTER(FALSE, FALSE)
							PRINT_OBJ("HT_BAC")
							bShownReturnToHunter = TRUE
						ENDIF
						
						IF bIsTrevorLooking
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)
							bIsTrevorLooking = FALSE
						ENDIF
					ENDIF
				ELSE
					FORCE_ELK_FLEE(FIRST_ELK)
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
					SET_FAIL_REASON(GET_SPOOKED_REASON(FIRST_ELK))
					CPRINTLN(DEBUG_MISSION, " Mission Failed spooked elk called from FOLLOW_TO_FIRST_ELK")
					MISSION_FAILED(eFailReason)
				ENDIF
			ENDIF
		
		BREAK
		
		CASE SS_CLEANUP
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)
									
			CPRINTLN(DEBUG_HUNTING, "MS_KILL_FIRST_ELK from mission state")
			eSubState = SS_INIT
			eMissionState = MS_KILL_FIRST_ELK
		BREAK
	ENDSWITCH
ENDPROC

PROC KILL_FIRST_ELK()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	CHECK_FOR_CALLS()
	CHECK_ELK_STATES(FIRST_ELK)
	
	SWITCH eSubState
		CASE SS_INIT
			CPRINTLN(DEBUG_HUNTING, "SET_REPLAY_MID_MISSION_STAGE_WITH_NAME = CP_AFTER_TUTORIAL")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_TUTORIAL, "Player has done Hunter's tutorial")
			
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			IF NOT bShownHuntElkGodText[FIRST_ELK]				// We haven't shown the god text yet...
				IF bHunterDoneConvo[HTC_ARRIVE_FIRST_ELK]			// The hunter has started talking...
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		// Wait until he stops...
				AND bHunter_InPosition1			
					CDEBUG1LN(DEBUG_HUNTING, "Printing objective to hunt first elk!")
					
					// Hunt the elk.
					PRINT_OBJ("HT_OBJ")	
					bPlayerCalling = TRUE // Blip the first elk automatically.
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ")
						bShownHuntElkGodText[FIRST_ELK] = TRUE		// Only flag this as having displayed once its actually on-screen.
						SAFE_REMOVE_BLIP(mHunterBlip)							
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ")
					IF NOT bShownHelp[CROUCH_HELP]
					AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_HUNTING, "Telling player to crouch")
						SHOW_THIS_HELP(CROUCH_HELP)
					ENDIF
				ENDIF
			ENDIF
					
			IF NOT mElk[FIRST_ELK].bIsSpooked
				IF NOT IS_PED_UNINJURED(mElk[FIRST_ELK].mPed)
					CDEBUG1LN(DEBUG_HUNTING, "FIRST ELK KILLED!!!")
					
					SAFE_CLEAR_THIS_PRINT("HT_OBJ")
					REMOVE_ALL_BLIPS()
					CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FIRST_ELK].mPed)
					
					IF NOT bShotWithWrongGun
						IF bHunterDoneConvo[HTC_GOOD_SHOT_FIRST_ELK]
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE) 	
									IF bShownReturnToHunter
										SAFE_CLEAR_THIS_PRINT("HT_BAC")
										bShownReturnToHunter = FALSE
									ENDIF
									
									BLIP_HUNTER(FALSE, FALSE)
									SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
									eSubState = SS_CLEANUP
								ELSE
									IF NOT bShownReturnToHunter AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										BLIP_HUNTER(FALSE, FALSE)
										PRINT_OBJ("HT_BAC")
										bShownReturnToHunter = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CLEAR_HELP()
						MISSION_FAILED(FR_WRONG_GUN)
					ENDIF
				ENDIF
			ELSE
				CLEAR_HELP()
				REMOVE_ALL_BLIPS()
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FIRST_ELK].mPed)
						
				IF bShotWithWrongGun
					MISSION_FAILED(FR_WRONG_GUN)
				ELSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
					SET_FAIL_REASON(GET_SPOOKED_REASON(FIRST_ELK))
					MISSION_FAILED(eFailReason)
				ENDIF
			ENDIF

		BREAK
		
		CASE SS_CLEANUP
							
			CPRINTLN(DEBUG_HUNTING, "SET_REPLAY_MID_MISSION_STAGE_WITH_NAME = CP_AFTER_FIRST_ELK")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_FIRST_ELK, "Player has done first elk")
				
			eSubState = SS_INIT
			CPRINTLN(DEBUG_HUNTING, "MS_WHISTLE_TO_SECOND_ELK from normal mission flow")
				
			eMissionState = MS_FOLLOW_TO_SECOND_ELK
			
		BREAK
		
	ENDSWITCH
		
ENDPROC

PROC SET_UP_BEFORE_SECOND_ELK()
	CDEBUG1LN(DEBUG_HUNTING, "SET_UP_BEFORE_SECOND_ELK")
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
	
		CASE SS_INIT
			CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_SECOND_ELK SS_INIT")
			
			SAFE_FADE_OUT()
			SAFE_DELETE_ALL_ELK()
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_SECOND_ELK SS_ACTIVE")
			
			IF IS_SCREEN_FADED_OUT()		
				REMOVE_ALL_BLIPS()
				
				IF NOT IS_PED_INJURED(mHunter)
					CLEAR_PED_TASKS(mHunter)
					SAFE_TELEPORT_ENTITY(mHunter, vHunterFirstKillPos, fHunterFirstKillHead)
					BLIP_HUNTER(FALSE, FALSE)
				ENDIF
				
				IF NOT IS_REPLAY_BEING_SET_UP() 		
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerFirstKillPos, fPlayerFirstKillHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerStartHuntPos)
				ENDIF
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_FIRST_ELK, "Player has done first elk")
				
				SAFE_GIVE_SNIPER_TO_PLAYER()
								
				bCanUpdateWindBlip = TRUE
				bAllowCall = FALSE
						
				eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_SECOND_ELK SS_CLEANUP")
				
			END_REPLAY_SETUP()
			SAFE_FADE_IN()
			
			bHunterDoneConvo[HTC_LEAD_TO_SECOND_ELK] = FALSE
			
			CPRINTLN(DEBUG_HUNTING, "HS_AFTER_KILL_FIRST_ELK from skipper")
			
			eHunterSubState = HSS_INIT
			eHunterState = HS_AFTER_KILL_FIRST_ELK
			
			CPRINTLN(DEBUG_HUNTING, "MS_WHISTLE_TO_SECOND_ELK from skipper")
			
			eSubState = SS_INIT
			eMissionState = MS_FOLLOW_TO_SECOND_ELK
		BREAK
	ENDSWITCH
ENDPROC

PROC FOLLOW_TO_SECOND_ELK()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
				// Clean up the return to hunter message...
				IF bShownReturnToHunter
					BLIP_HUNTER(FALSE, FALSE)
					SAFE_CLEAR_THIS_PRINT("HT_BAC")
					bShownReturnToHunter = FALSE
				ENDIF
				
				eSubState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			// Are we close enough to the hunter?
			IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
				bShownReturnToHunter = FALSE
				
				// Right, this'll do. Give a blow on that whistle
				IF bHunterDoneConvo[HTC_WHISTLE_TO_SECOND_ELK]
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						// Show the help prompt about calling elk
						IF NOT bShownHelp[FIRST_CALL_HELP]
							// So the player can whistle straight away
							iWhistleDelay = -1
							
							// Okay, this elk needs to walk out into the open, not just be made visible when the whistle is blown.
							IF SPAWNED_THIS_ELK(SECOND_ELK)
								CDEBUG1LN(DEBUG_HUNTING, "FOLLOW_TO_SECOND_ELK - Showing help to call the elk!")
								MAKE_ELK_VISIBLE(SECOND_ELK)
								
								mElk[SECOND_ELK].iDestNode = 0
								elkSpawn[0].vCoord = vElkGoto_Second
								mElk[SECOND_ELK].elkState = ES_TRAVEL_THEN_ENTER_GRAZE_AND_WAIT_TO_DIE
								
								SHOW_THIS_HELP(FIRST_CALL_HELP)
								bAllowCall = TRUE
							ENDIF
						ELSE
							// If we've tasked the player to return, but the help isn't up, draw it again.
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HT_CALL")
								IF IS_THIS_PRINT_BEING_DISPLAYED("HT_BAC")
									CLEAR_THIS_PRINT("HT_BAC")
								ENDIF
								
								SHOW_THIS_HELP(FIRST_CALL_HELP)
							ENDIF
							
							CHECK_FOR_CALLS()
							CHECK_ELK_STATES(SECOND_ELK)
							
							IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
								SAFE_REMOVE_BLIP(mHunterBlip)
								CLEAR_HELP(TRUE)
															
								eSubState = SS_CLEANUP
							ENDIF	
						ENDIF
					ENDIF				
				ENDIF
			ELSE
				CHECK_PLAYER_FLED()
		
				// Player needs to stay close to the hunter before being  told about the whistle.
				IF NOT bShownReturnToHunter
					CLEAR_HELP()
					
					BLIP_HUNTER(FALSE, FALSE)
					PRINT_OBJ("HT_BAC")
					bShownReturnToHunter = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vElkSpawn[SECOND_ELK], -1)
			
			TASK_FOLLOW_NAV_MESH_TO_COORD(mElk[SECOND_ELK].mPed, vElkGoto_Second, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_DEFAULT)

			// Time stamp this call, we don't want the Hunter replying too soon..
			iTimerDelay = GET_GAME_TIMER() + PLAYER_CALL_DURATION
			CDEBUG2LN(DEBUG_HUNTING, "FOLLOW_TO_SECOND_ELK - iTimerDelay is set...")
								
			CDEBUG1LN(DEBUG_HUNTING, "FOLLOW_TO_SECOND_ELK -> MS_KILL_SECOND_ELK")
			eMissionState = MS_KILL_SECOND_ELK
			eSubState = SS_INIT
		BREAK
	ENDSWITCH
ENDPROC

PROC KILL_SECOND_ELK()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	CHECK_FOR_CALLS()
	CHECK_ELK_STATES(SECOND_ELK)
	
	SWITCH eSubState
		CASE SS_INIT
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE	
			IF NOT mElk[SECOND_ELK].bIsSpooked
				IF NOT IS_PED_UNINJURED(mElk[SECOND_ELK].mPed)					
					IF bAllowCall
						REMOVE_ALL_BLIPS()	
						SAFE_CLEAR_THIS_PRINT("HT_OBJ")
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
						CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[SECOND_ELK].mPed)
						bAllowCall = FALSE
					ENDIF
										
					IF NOT bShotWithWrongGun
						IF bHunterDoneConvo[HTC_GOOD_SHOT_SECOND_ELK]
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								IF IS_PLAYER_NEAR_HUNTER(VERY_NEAR_HUNTER_RANGE) 	
									IF bShownReturnToHunter
										BLIP_HUNTER(FALSE, FALSE)
										SAFE_CLEAR_THIS_PRINT("HT_BAC")
										bShownReturnToHunter = FALSE
									ENDIF							
							
									// Timestamp this kill, we don't want the help text appearing instantly...
									iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE		
									eSubState = SS_CLEANUP
								ELSE
									IF NOT bShownReturnToHunter AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										BLIP_HUNTER(FALSE, FALSE)
										PRINT_OBJ("HT_BAC")
										bShownReturnToHunter = TRUE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ELSE
						MISSION_FAILED(FR_WRONG_GUN)
					ENDIF
				
				ELSE
					IF DOES_BLIP_EXIST(mElk[SECOND_ELK].mElkBlip)
					AND NOT bShownHuntElkGodText[SECOND_ELK]			// We haven't shown the godtext yet...						
					AND bHunterDoneConvo[HTC_DISCOVER_SECOND_ELK]		// The hunter has started talking ("Same as before boy")
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		// Wait until he stops...
						// Hunt the elk.
						PRINT_OBJ("HT_OBJ")
						bShownHuntElkGodText[SECOND_ELK] = TRUE	
					ENDIF
				ENDIF
			ELSE
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[SECOND_ELK].mPed)
						
				IF bShotWithWrongGun
					MISSION_FAILED(FR_WRONG_GUN)
				ELSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
					SET_FAIL_REASON(GET_SPOOKED_REASON(SECOND_ELK))
					MISSION_FAILED(eFailReason)
				ENDIF
			ENDIF

		BREAK
		
		CASE SS_CLEANUP
	
			IF GET_GAME_TIMER() > iTimerDelay
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_SECOND_ELK, "Player has done second elk")
				CPRINTLN(DEBUG_HUNTING, "CP_AFTER_SECOND_ELK")
				
				eSubState = SS_INIT
				CPRINTLN(DEBUG_HUNTING, "FOLLOW_TO_THIRD_ELK")
				eMissionState = MS_FOLLOW_TO_THIRD_ELK
			ENDIF
			
		BREAK
		
	ENDSWITCH
		
ENDPROC

PROC SET_UP_BEFORE_THIRD_ELK()
	CDEBUG1LN(DEBUG_HUNTING, "SET_UP_BEFORE_THIRD_ELK")
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			CPRINTLN(DEBUG_HUNTING, " SS_INIT SET_UP_BEFORE_THIRD_ELK")
			SAFE_FADE_OUT()
			SAFE_DELETE_ALL_ELK()
			
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			CPRINTLN(DEBUG_HUNTING, " SS_ACTIVE SET_UP_BEFORE_THIRD_ELK")
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			IF IS_SCREEN_FADED_OUT()			
				REMOVE_ALL_BLIPS()
				
				IF NOT IS_PED_INJURED(mHunter)
					CLEAR_PED_TASKS(mHunter)
					SAFE_TELEPORT_ENTITY(mHunter, vHunterSecondKillPos, fHunterSecondKillHead)
					BLIP_HUNTER(FALSE, FALSE)
				ENDIF
				
				IF NOT IS_REPLAY_BEING_SET_UP() 
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerSecondKillPos, fPlayerSecondKillHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerThirdKillPos)
				ENDIF
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_SECOND_ELK, "Player has done second elk")
									
				SAFE_GIVE_SNIPER_TO_PLAYER()
				RESET_ALL_HUNTER_CONVOS()		
				bAllowCall = FALSE
				bCanUpdateWindBlip = TRUE
				CPRINTLN(DEBUG_HUNTING, " SS_CLEANUP SET_UP_BEFORE_THIRD_ELK")			
				eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
				
			END_REPLAY_SETUP()
			RESET_CAM()
			SAFE_FADE_IN()
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
			
			// Initialise this so the hunter's comment about knowing where the elk are doesn't kick in too soon
			iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
			
			// Reset this so we can show the "call elk" help text again.
			bShownHelp[SECOND_CALL_HELP] = FALSE
			
			CPRINTLN(DEBUG_HUNTING, "eHunterState = HS_AFTER_KILL_SECOND_ELK")
			eHunterSubState = HSS_INIT
			eHunterState = HS_AFTER_KILL_SECOND_ELK
			
			CPRINTLN(DEBUG_HUNTING, "eMissionState = MS_WHISTLE_TO_THIRD_ELK")
			eSubState = SS_INIT
			eMissionState = MS_FOLLOW_TO_THIRD_ELK
					
		BREAK
	
	ENDSWITCH
		
ENDPROC

PROC FOLLOW_TO_THIRD_ELK()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	// Are we close enough to the hunter?
	IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE)
		
		BLIP_HUNTER(FALSE, FALSE)
		
		// Clean up the return to hunter message...
		IF bShownReturnToHunter
			SAFE_CLEAR_THIS_PRINT("HT_BAC")
			bShownReturnToHunter = FALSE
		ENDIF
		
		// Remember, we're only here for the male elk
		IF bHunterDoneConvo[HTC_WHISTLE_TO_THIRD_ELK] 
		AND eHunterState <> HS_WAIT_FOR_PLAYER_TO_RETURN
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF NOT bShownHelp[SECOND_CALL_HELP] AND NOT bAllowCall
					iWhistleDelay = -1
					bAllowCall = TRUE
					SHOW_THIS_HELP(SECOND_CALL_HELP)
				ELSE					
					IF NOT bTriggered3rdElk 
						CHECK_FOR_CALLS()
						
						IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
							
							MAKE_ELK_VISIBLE(THIRD_ELK)
							MAKE_ELK_VISIBLE(DOE_ELK)
							
							iTimerDelay = GET_GAME_TIMER() + PLAYER_CALL_DURATION
							CLEAR_HELP(TRUE)
							
							bTriggered3rdElk = TRUE
						ENDIF
					ELSE
						IF SPAWNED_THIS_ELK(THIRD_ELK)
						AND SPAWNED_THIS_ELK(DOE_ELK)
							SET_ELK_SPOOKED_BLIP_ENABLED(mElk[DOE_ELK], FALSE)

							CDEBUG1LN(DEBUG_HUNTING, "FOLLOW_TO_THIRD_ELK - Showing help to call the elk!")
							MAKE_ELK_VISIBLE(THIRD_ELK)
							MAKE_ELK_VISIBLE(DOE_ELK)
									
							mElk[THIRD_ELK].iDestNode = 0
							elkSpawn[0].vCoord = vElkGoto_Third
							mElk[THIRD_ELK].elkState = ES_TRAVEL_THEN_ENTER_GRAZE_AND_WAIT_TO_DIE
							
							mElk[DOE_ELK].iDestNode = 0
							elkSpawn[0].vCoord = vElkGoto_Doe
							mElk[DOE_ELK].elkState = ES_TRAVEL_THEN_ENTER_GRAZE_AND_WAIT_TO_DIE
							
							TASK_FOLLOW_NAV_MESH_TO_COORD(mElk[THIRD_ELK].mPed, vElkGoto_Third, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_DEFAULT, fElkGoto_ThirdHeading)
							TASK_FOLLOW_NAV_MESH_TO_COORD(mElk[DOE_ELK].mPed, vElkGoto_Doe, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_DEFAULT, fElkGoto_DoeHeading)
							
							CPRINTLN(DEBUG_HUNTING, "eMissionState = MS_FOLLOW_TO_THIRD_ELK")
							eMissionState = MS_KILL_THIRD_ELK
							eSubState = SS_INIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		
		CHECK_PLAYER_FLED()
		
		// Player needs to stay close to the hunter before being  told about the whistle.
		IF NOT bShownReturnToHunter
			bAllowCall = FALSE
			CLEAR_HELP(TRUE)
			bShownHelp[SECOND_CALL_HELP] = FALSE
			BLIP_HUNTER(FALSE, FALSE)
			PRINT_OBJ("HT_BAC")
			bShownReturnToHunter = TRUE
		ENDIF
		
	ENDIF		
	
ENDPROC

PROC KILL_THIRD_ELK()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	CHECK_FOR_CALLS()
	CHECK_ELK_STATES(THIRD_ELK)
	CHECK_ELK_STATES(DOE_ELK)
		
	SWITCH eSubState
	
		CASE SS_INIT
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
						
			// Monitor the target elk
			IF NOT mElk[THIRD_ELK].bIsSpooked
			
				IF NOT IS_PED_UNINJURED(mElk[THIRD_ELK].mPed)
										
					IF bAllowCall
						REMOVE_ALL_BLIPS()
						CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[THIRD_ELK].mPed)
						bAllowCall = FALSE
					ENDIF					
					
					IF NOT bFinalElkFlee
						// Get the doe to flee
						IF IS_PED_UNINJURED(mElk[DOE_ELK].mPed)
							FORCE_ELK_FLEE(DOE_ELK)
						ENDIF
						bFinalElkFlee = TRUE
					ENDIF
					
					IF NOT bShotWithWrongGun
						IF IS_PLAYER_NEAR_HUNTER(NEAR_HUNTER_RANGE) 
							
							CLEAR_HELP()
							
							IF bShownReturnToHunter
								BLIP_HUNTER(FALSE, FALSE)
								SAFE_CLEAR_THIS_PRINT("HT_BAC")
								bShownReturnToHunter = FALSE
							ENDIF
							
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
								
							// Timestamp this kill, we don't want the help text appearing instantly...
							iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE		
							eSubState = SS_CLEANUP
						ELSE
							IF NOT bShownReturnToHunter AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
								CLEAR_HELP()
								
								BLIP_HUNTER(FALSE, FALSE)
								PRINT_OBJ("HT_BAC")
								bShownReturnToHunter = TRUE
								
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
							
							ENDIF
						ENDIF
								
					ELSE
						CPRINTLN(DEBUG_HUNTING, "Mission Failed from KILL_THIRD_ELK - Wrong Gun")
						CLEAR_HELP()
						MISSION_FAILED(FR_WRONG_GUN)
					ENDIF
				ELSE
				
					IF NOT bShownHuntElkGodText[THIRD_ELK]					// We haven't shown the godtext yet...
						IF bHunterDoneConvo[HTC_DISCOVER_THIRD_ELK]			// The hunter has started talking ("Same as before, boy...")
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		// Wait until he stops...
							
							// Hunt the elk.
							PRINT_OBJ("HT_OBJ")
							SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)
							TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vElkSpawn[THIRD_ELK], -1)
							bShownHuntElkGodText[THIRD_ELK] = TRUE	
						ENDIF
					ELSE
						
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ")
							IF NOT bShownHelp[DONT_KILL_DOE_HELP]
								SHOW_THIS_HELP(DONT_KILL_DOE_HELP)
							ENDIF
						ENDIF
						
					ENDIF
					
					// Monitor the doe
					IF NOT IS_PED_UNINJURED(mElk[DOE_ELK].mPed)
						IF NOT bFinalElkFlee
							FORCE_ELK_FLEE(THIRD_ELK)
							bFinalElkFlee = TRUE
							
							MISSION_FAILED(FR_HE_SAW_YOU)
						ELSE

							// Wait for the hunter to finish his line...
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CPRINTLN(DEBUG_HUNTING, "Mission Failed from KILL_THIRD_ELK")
								MISSION_FAILED(FR_SHOT_DOE)
							ENDIF
						ENDIF
					ENDIF
			
				ENDIF
			ELSE
				
				CLEAR_HELP()
			 	CDEBUG2LN(DEBUG_HUNTING, "Elk three has been spooked!")
				
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[THIRD_ELK].mPed)
						
				IF bShotWithWrongGun
					MISSION_FAILED(FR_WRONG_GUN)
				ELSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
					SET_FAIL_REASON(GET_SPOOKED_REASON(THIRD_ELK))
					MISSION_FAILED(eFailReason)
				ENDIF
				
			ENDIF	
			
		BREAK
		
		CASE SS_CLEANUP
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HT_MISS")
				CLEAR_HELP()				
			ENDIF
			
			IF NOT bFinalElkFlee
				
				IF IS_PED_UNINJURED(mElk[DOE_ELK].mPed)
					FORCE_ELK_FLEE(DOE_ELK)
				ENDIF
				
				bFinalElkFlee = TRUE
			ENDIF
			
			IF GET_GAME_TIMER() > iTimerDelay
				
				SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), FALSE)
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_THIRD_ELK, "Player has done third elk", TRUE)
				CDEBUG2LN(DEBUG_HUNTING, "CP_AFTER_THIRD_ELK")
							
				eSubState = SS_INIT
				eMissionState = MS_HUNTER_LEAVES 
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC SET_UP_BEFORE_FOURTH_ELK()
	CDEBUG1LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK")
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
		CASE SS_INIT
			CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK - SS_INIT")
			
			SAFE_FADE_OUT()
			SAFE_DELETE_ALL_ELK()
			
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK -> SS_ACTIVE ")
			
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
			CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK - SS_ACTIVE")
			
			IF IS_SCREEN_FADED_OUT()
				REMOVE_ALL_BLIPS()
				
				IF IS_PED_UNINJURED(mHunter)
					CLEAR_PED_TASKS(mHunter)
					SAFE_TELEPORT_ENTITY(mHunter, vHunterThirdKillPos, fHunterThirdKillHead)
					BLIP_HUNTER(FALSE, FALSE)
				ENDIF
				
				IF NOT IS_REPLAY_BEING_SET_UP() 
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerThirdKillPos, fPlayerThirdKillHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerThirdKillPos)
				ENDIF
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_THIRD_ELK, "Player has done third elk", TRUE)
				
				SAFE_GIVE_SNIPER_TO_PLAYER()
				RESET_ALL_HUNTER_CONVOS()		
				bAllowCall = FALSE
				bCanUpdateWindBlip = TRUE
				CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK -> SS_CLEANUP")			
				eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK - SS_CLEANUP")
			
			END_REPLAY_SETUP()
			RESET_CAM()
			SAFE_FADE_IN()
			
			SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
			
			// Initialise this so the hunter's comment about knowing where the elk are doesn't kick in too soon
			iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE
			
			// Reset this so we can show the "call elk" help text again.
			bShownHelp[THIRD_CALL_HELP] = FALSE
			
			// So the hunter goes straigt into his explainination
			bHunterDoneConvo[HTC_GOOD_SHOT_THIRD_ELK] = TRUE
			
			CDEBUG2LN(DEBUG_HUNTING, "eHunterState = HS_WAIT_KILL_THIRD_ELK")
			eHunterSubState = HSS_INIT
			eHunterState = HS_WAIT_KILL_THIRD_ELK
			
			CDEBUG2LN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK - SS_INIT")
			CDEBUG2LN(DEBUG_HUNTING, "eMissionState = MS_HUNTER_LEAVES")
			eSubState = SS_INIT
			eMissionState = MS_HUNTER_LEAVES	
		BREAK
	ENDSWITCH
ENDPROC

PROC HUNTER_LEAVES()				
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	// Has he said his lines?
	IF bHunterDoneConvo[HTC_TELL_ABOUT_TEXTING]
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bShownHelp[THIRD_CALL_HELP]
				IF SPAWNED_THIS_ELK(FOURTH_ELK)
					CDEBUG1LN(DEBUG_HUNTING, "HUNTER_LEAVES - Setting up 4th elk.")
					MAKE_ELK_VISIBLE(FOURTH_ELK)
					
					VECTOR vElkGoto = vElkGoto_Fourth
					FLOAT fElkGoto = fElkGoto_FourthHeading
					FLOAT fRedirectElkRange = k_ELK_SPOOK_RANGE * k_ELK_SPOOK_RANGE_MULT
					
					#IF IS_DEBUG_BUILD
					DEBUG_RECORD_SPHERE( "vElkGoto", vElkGoto, fRedirectElkRange, (<< 150, 150, 150 >>) )
					DEBUG_RECORD_SPHERE( "PlayerPos", GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), 1.0, (<< 255, 255, 0 >>) )
					#ENDIF
					
					IF VDIST( GET_ENTITY_COORDS( PLAYER_PED_ID(), FALSE ), vElkGoto ) <= fRedirectElkRange
						CDEBUG2LN(DEBUG_HUNTING, "HUNTER_LEAVES picking alt fourth position, player is too close to original spawn.")
						vElkGoto = vElkGoto_Fourth_alt
						fElkGoto = fElkGoto_FourthHeading_alt
					ENDIF
					
					TASK_FOLLOW_NAV_MESH_TO_COORD(mElk[FOURTH_ELK].mPed, vElkGoto, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_DEFAULT, fElkGoto)
					
					mElk[FOURTH_ELK].iDestNode = 0
					elkSpawn[0].vCoord = vElkGoto
					mElk[FOURTH_ELK].elkState = ES_TRAVEL_THEN_ENTER_GRAZE_AND_WAIT_TO_DIE
						
					iWhistleDelay = -1
					SHOW_THIS_HELP(THIRD_CALL_HELP)
					bAllowCall = TRUE
				ENDIF
			ELSE
				CHECK_ELK_STATES(FOURTH_ELK)
				CHECK_FOR_CALLS()
				
				IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					CLEAR_HELP(TRUE)
												
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vElkSpawn[FOURTH_ELK], -1)
					
					// Time stamp this call, we don't want the Hunter replying too soon..
					iTimerDelay = GET_GAME_TIMER() + PLAYER_CALL_DURATION
					CPRINTLN(DEBUG_HUNTING, "iTimerDelay is set...")
										
					CPRINTLN(DEBUG_HUNTING, "MS_KILL_FOURTH_ELK")
					eMissionState = MS_KILL_FOURTH_ELK
					eSubState = SS_ACTIVE
					
				ELSE
					IF mElk[FOURTH_ELK].bIsSpooked
						CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FOURTH_ELK].mPed)
						IF bShotWithWrongGun
							MISSION_FAILED(FR_WRONG_GUN)
						ELSE
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
							SET_FAIL_REASON(GET_SPOOKED_REASON(FOURTH_ELK))
							MISSION_FAILED(eFailReason)
						ENDIF
					
					ELIF NOT IS_PED_UNINJURED(mElk[FOURTH_ELK].mPed) 
						CPRINTLN(DEBUG_HUNTING, "Fourth Elk is dead...")
						CLEAR_HELP()
						eMissionState = MS_KILL_FOURTH_ELK
					
						// remove the kill objective
						SAFE_CLEAR_THIS_PRINT("HT_OBJ")
						REMOVE_ALL_BLIPS()
						CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FOURTH_ELK].mPed)
						bAllowCall = FALSE
						
						IF NOT bShotWithWrongGun			
							// Timestamp this kill, we don't want the help text appearing instantly...
							iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE		
							eSubState = SS_CLEANUP						
						ELSE
							MISSION_FAILED(FR_WRONG_GUN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF				
	
ENDPROC

PROC KILL_FOURTH_ELK()	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	CHECK_FOR_CALLS()
	
	IF (eSubState != SS_CLEANUP)
		CHECK_ELK_STATES(FOURTH_ELK)
	ENDIF
	
	SWITCH eSubState
	
		CASE SS_ACTIVE
								
			IF NOT mElk[FOURTH_ELK].bIsSpooked
				IF NOT IS_PED_UNINJURED(mElk[FOURTH_ELK].mPed)
					
					CPRINTLN(DEBUG_HUNTING, "Fourth Elk is dead...")
					
					// remove the kill objective
					SAFE_CLEAR_THIS_PRINT("HT_OBJ")
					REMOVE_ALL_BLIPS()
					CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FOURTH_ELK].mPed)
					bAllowCall = FALSE
					
					IF NOT bShotWithWrongGun		
						// Timestamp this kill, we don't want the help text appearing instantly...
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						iTimerDelay = GET_GAME_TIMER() + NICE_LITTLE_PAUSE		
						eSubState = SS_CLEANUP						
					ELSE
						MISSION_FAILED(FR_WRONG_GUN)
					ENDIF
				
				ELSE
				
					// Elk is still alive and not spooked
					IF NOT bShownHuntElkGodText[FOURTH_ELK]				// We haven't shown the godtext yet...
						IF DOES_BLIP_EXIST(mElk[FOURTH_ELK].mElkBlip)
							CPRINTLN(DEBUG_HUNTING, "Show God Text - hunt fourth elk....")
							// Hunt the elk.
							PRINT_OBJ("HT_OBJ")
							bShownHuntElkGodText[FOURTH_ELK] = TRUE					
						ENDIF
					ELSE
						IF NOT bShownHelp[WIND_CHANGE_HELP]
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HT_OBJ")
								// Keep an eye on the wind direction as it will change over time...
								SHOW_THIS_HELP(WIND_CHANGE_HELP)
								bWindCanChange = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CHECK_TARGET_DAMAGED_BY_SNIPER_RIFLE(mElk[FOURTH_ELK].mPed)
						
				IF bShotWithWrongGun
					MISSION_FAILED(FR_WRONG_GUN)
				ELSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(HU2_DETECTED_BY_ELK)
					SET_FAIL_REASON(GET_SPOOKED_REASON(FOURTH_ELK))
					MISSION_FAILED(eFailReason)
				ENDIF
			ENDIF

		BREAK
		
		CASE SS_CLEANUP
			IF GET_GAME_TIMER() > iTimerDelay
				SET_ENTITY_AS_MISSION_ENTITY(mElk[FOURTH_ELK].mPed, TRUE, TRUE)
				
				mBlip = ADD_BLIP_FOR_ENTITY(mElk[FOURTH_ELK].mPed)
				IF DOES_BLIP_EXIST(mBlip)
					SET_BLIP_COLOUR(mBlip, BLIP_COLOUR_RED)
				ENDIF
				
				//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AFTER_FOURTH_ELK, "Player has killed fourth elk", TRUE)
				
				// No need to show the wind direction...
				bCanUpdateWindBlip = FALSE
				
				// Approach the elk's corpse.
				PRINT_OBJ("HT_APP")
				CPRINTLN(DEBUG_HUNTING, "APPROACH_FOURTH_ELK")
				eMissionState = MS_APPROACH_FOURTH_ELK
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC APPROACH_FOURTH_ELK()		
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.25)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	VECTOR vElk
	IF DOES_ENTITY_EXIST(mElk[FOURTH_ELK].mPed)
		vElk = GET_ENTITY_COORDS(mElk[FOURTH_ELK].mPed, FALSE)
	ENDIF
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vElk, FALSE) < 5.0
		
		// Reset the 
		Player_Hunt_Data.fLastCamZoom = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()

		ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
		BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
		
		// Photograph the dead elk.
		PRINT_OBJ("HT_PHOTO")
		eMissionState = MS_PHOTO_FOURTH_ELK 
	ENDIF
ENDPROC

PROC PHOTO_FOURTH_ELK()			
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	VECTOR vElk
	IF DOES_ENTITY_EXIST(mElk[FOURTH_ELK].mPed)
		vElk = GET_ENTITY_COORDS(mElk[FOURTH_ELK].mPed, FALSE)
	ENDIF
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vElk, FALSE) < 40.0
		Player_Hunt_Data.fLastCamZoom = GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR()
		
		IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
			FLOAT fDist2
			FLOAT fScreenX, fScreenY
			fDist2 = VDIST2(vElk, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			IF GET_SCREEN_COORD_FROM_WORLD_COORD(vElk, fScreenX, fScreenY)
				CPRINTLN(DEBUG_MISSION, "fScreenX = ", fScreenX )
				CPRINTLN(DEBUG_MISSION, "fScreenY = ", fScreenY )
				CPRINTLN(DEBUG_MISSION, "fDist2  = ", fDist2 )
				CPRINTLN(DEBUG_MISSION, "all the other shit = ", 100.0 * Player_Hunt_Data.fLastCamZoom * Player_Hunt_Data.fLastCamZoom) 

				IF (fScreenX > 0.1 AND fScreenX < 0.9 AND fScreenY > 0.1 AND fScreenY < 0.9)
				AND (fDist2 < (100.0 * Player_Hunt_Data.fLastCamZoom * Player_Hunt_Data.fLastCamZoom))
					ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
					BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HT_NOGOOD")
						CLEAR_PRINTS()
					ENDIF
					
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					SAFE_REMOVE_BLIP(mBlip)
					// Send this picture to Cletus...
					PRINT_OBJ("HT_SENDPIC")
					eMissionState = MS_SEND_PHOTO 
				ELSE		
					PRINT_HELP("HT_NOGOOD")
				ENDIF
			ELSE
				PRINT_HELP("HT_NOGOOD")
			ENDIF
		ENDIF
			
	ELSE
		BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
		// Return to the elk's corpse.
		PRINT_OBJ("HT_RETURN")
		eMissionState = MS_APPROACH_FOURTH_ELK
	ENDIF
		
ENDPROC

PROC SEND_PHOTO()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	// If we're here, and we go back to the camera, need to remove the prompt to send photo.
	IF NOT IS_CONTACTS_LIST_ON_SCREEN()
		IF Is_Phone_Control_Just_Pressed(FRONTEND_CONTROL, INT_TO_ENUM (CONTROL_ACTION, PHONE_POSITIVE_INPUT))
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
			ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
			
			mBlip = ADD_BLIP_FOR_ENTITY(mElk[FOURTH_ELK].mPed)
			IF DOES_BLIP_EXIST(mBlip)
				SET_BLIP_COLOUR(mBlip, BLIP_COLOUR_RED)
			ENDIF
			
			Cellphone_Pic_Just_Taken = FALSE
			PRINT_OBJ("HT_PHOTO")
			eMissionState = MS_PHOTO_FOURTH_ELK
			
			EXIT
		ENDIF
	ENDIF
	
	IF HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT()
		IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_HUNTER)
			HANG_UP_AND_PUT_AWAY_PHONE()
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
			ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)		
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
			
			iTimerDelay = GET_GAME_TIMER() + 3000
			
			eMissionState = MS_WAIT_FOR_REPLY
		ELSE
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
			ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
			
			mBlip = ADD_BLIP_FOR_ENTITY(mElk[FOURTH_ELK].mPed)
			IF DOES_BLIP_EXIST(mBlip)
				SET_BLIP_COLOUR(mBlip, BLIP_COLOUR_RED)
			ENDIF
			
			Cellphone_Pic_Just_Taken = FALSE
			PRINT_OBJ("HT_PHOTO")
			PRINT_HELP("HT_BADREC")
			eMissionState = MS_PHOTO_FOURTH_ELK
		ENDIF
	ENDIF
ENDPROC

PROC SET_UP_BEFORE_MISSION_PASS()
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	SWITCH eSubState
	
		CASE SS_INIT
			
			SAFE_FADE_OUT()
			SAFE_DELETE_ALL_ELK()
			
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK  SS_ACTIVE ")
			
			eSubState = SS_ACTIVE
		BREAK
		
		CASE SS_ACTIVE
		
			CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK SS_ACTIVE")
			
			IF IS_SCREEN_FADED_OUT()
									
				REMOVE_ALL_BLIPS()
				
				IF IS_PED_UNINJURED(mHunter)
					CLEAR_PED_TASKS(mHunter)
					SAFE_DELETE_PED(mHunter)
				ENDIF
				
				IF NOT IS_REPLAY_BEING_SET_UP() 
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerThirdKillPos, fPlayerThirdKillHead)
					WAIT_FOR_WORLD_TO_LOAD(vPlayerThirdKillPos)
				ENDIF
									
				SAFE_GIVE_SNIPER_TO_PLAYER()
				RESET_ALL_HUNTER_CONVOS()		
				bAllowCall = FALSE
				bCanUpdateWindBlip = FALSE
				CPRINTLN(DEBUG_HUNTING, "SET_UP_BEFORE_FOURTH_ELK - SS_CLEANUP")			
				eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
						
			END_REPLAY_SETUP()
			RESET_CAM()
			SAFE_FADE_IN()
						
			// Initialise this so the hunter's comment about knowing where the elk are doesn't kick in too soon
			iTimerDelay = GET_GAME_TIMER() + 3500
			bShitSkipping = TRUE
						
			CPRINTLN(DEBUG_HUNTING, "eMissionState = MS_WAIT_FOR_REPLY")
			eMissionState = MS_WAIT_FOR_REPLY
					
		BREAK
	
	ENDSWITCH
		
ENDPROC


PROC WAIT_FOR_REPLY()			
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	IF GET_GAME_TIMER() > iTimerDelay
		IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_HUNTER)
		OR bShitSkipping
			IF NOT bStartTextTimer
				iTimerDelay = GET_GAME_TIMER() + 8000
				bStartTextTimer = TRUE
			ELIF NOT bSentTextMessage
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_HUNTER, "HT_TXTMSG", TXTMSG_UNLOCKED) 
				iTimerDelay = GET_GAME_TIMER() + 10000
				bSentTextMessage = TRUE
			ELSE
				ADD_PED_FOR_DIALOGUE(mHunterConv, 2, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(mHunterConv, 3, NULL, "CLETUS")
				IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(mHunterConv, CHAR_HUNTER, sTextBlock, "HT2_END", CONV_PRIORITY_HIGH)
					REMOVE_TEXT_MESSAGE_FEED_ENTRY("HT_TXTMSG")
					eMissionState = MS_DO_FINAL_PHONECALL
				ENDIF
			ENDIF
		ELSE
			PRINT_HELP("HT_BADREC")
		ENDIF
	ENDIF
ENDPROC

PROC DO_FINAL_PHONECALL()
	
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	IF HAS_CELLPHONE_CALL_FINISHED()
		eMissionState = MS_MISSION_PASSED 
	ENDIF
	
ENDPROC

PROC MISSION_PASSED()
	SAFE_FADE_IN()
	Script_Passed()
ENDPROC
				
																	// DEBUG MENU

#IF IS_DEBUG_BUILD

/// PURPOSE: Check for Forced Pass or Fail
PROC DEBUG_Check_Debug_Keys()
		
	INT iMissionState
	
	// Check for Pass
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		WAIT_FOR_CUTSCENE_TO_STOP()
		Script_Passed()
	ENDIF

	// Check for Fail
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		WAIT_FOR_CUTSCENE_TO_STOP()
		MISSION_FAILED(FR_NONE)
	ENDIF
		
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) AND eMissionState != MS_INIT_MISSION
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		RESET_ALL_HUNTER_CONVOS()
		RESET_ALL_CONVO_FLAGS()
		
		iMissionState = ENUM_TO_INT(eMissionState)
		
		CPRINTLN(DEBUG_HUNTING, "** J SKIPPING **")
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_HUNTING, "iMisisonState = ", iMissionState)
		#ENDIF
		
		eSubState = SS_INIT
		IF iMissionState < ENUM_TO_INT(MS_KILL_FIRST_ELK)
			CDEBUG1LN(DEBUG_HUNTING, "Debug skipping to MS_SET_UP_BEFORE_FIRST_ELK")
			eMissionState = MS_SET_UP_BEFORE_FIRST_ELK
		ELIF iMissionState < ENUM_TO_INT(MS_SET_UP_BEFORE_SECOND_ELK)
			CDEBUG1LN(DEBUG_HUNTING, "Debug skipping to MS_SET_UP_BEFORE_SECOND_ELK")
			eMissionState = MS_SET_UP_BEFORE_SECOND_ELK
		ELIF iMissionState < ENUM_TO_INT(MS_SET_UP_BEFORE_THIRD_ELK)
			CDEBUG1LN(DEBUG_HUNTING, "Debug skipping to MS_SET_UP_BEFORE_THIRD_ELK")
			eMissionState = MS_SET_UP_BEFORE_THIRD_ELK
		ELIF iMissionState < ENUM_TO_INT(MS_HUNTER_LEAVES)
			CDEBUG1LN(DEBUG_HUNTING, "Debug skipping to MS_SET_UP_BEFORE_OUTRO")
			eMissionState = MS_HUNTER_LEAVES
		ELIF iMissionState < ENUM_TO_INT(MS_WAIT_FOR_REPLY)
			CDEBUG1LN(DEBUG_HUNTING, "Debug Skipping to MS_WAIT_FOR_REPLY")
			eMissionState = MS_WAIT_FOR_REPLY
		ELSE
			CDEBUG1LN(DEBUG_HUNTING, "Debug skipping to Script_Passed")
			Script_Passed()
		ENDIF
		
		IF DOES_CAM_EXIST(mCam)
			SET_CAM_ACTIVE(mCam, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(mCam)
		ENDIF
				
	ENDIF	
	
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) AND eMissionState <> MS_INIT_MISSION AND eMissionState <> MS_RUN_INTRO
		
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		RESET_ALL_HUNTER_CONVOS()
		RESET_ALL_CONVO_FLAGS()
		
		iMissionState = ENUM_TO_INT(eMissionState)
		
		CPRINTLN(DEBUG_HUNTING, " *** DEBUG SKIP P *** ")
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_HUNTING, "iMisisonState = ", iMissionState)
		#ENDIF
		
		eSubState = SS_INIT
		
		IF iMissionState >= ENUM_TO_INT(MS_KILL_FOURTH_ELK)
			eMissionState = MS_SET_UP_BEFORE_FOURTH_ELK
		ELIF iMissionState >= ENUM_TO_INT(MS_KILL_THIRD_ELK)
			eMissionState = MS_SET_UP_BEFORE_SECOND_ELK
		ELIF iMissionState >= ENUM_TO_INT(MS_KILL_SECOND_ELK)
			eMissionState = MS_SET_UP_BEFORE_FIRST_ELK
		ELSE
			CPRINTLN(DEBUG_MISSION, "MS_SET_UP_AFTER_INTRO")
			eMissionState = MS_SET_UP_AFTER_INTRO
		ENDIF
		
		IF DOES_CAM_EXIST(mCam)
			SET_CAM_ACTIVE(mCam, FALSE)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(mCam)
		ENDIF
		
	ENDIF

    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		RESET_ALL_HUNTER_CONVOS()
		RESET_ALL_CONVO_FLAGS()
		
		SWITCH i_debug_jump_stage
			CASE 0	i_debug_jump_stage = ENUM_TO_INT(MS_SET_UP_AFTER_INTRO)			BREAK // Set up hunter walking off at start
			CASE 1	i_debug_jump_stage = ENUM_TO_INT(MS_SET_UP_BEFORE_FIRST_ELK)	BREAK // Set up first elk to shoot
			CASE 2	i_debug_jump_stage = ENUM_TO_INT(MS_SET_UP_BEFORE_SECOND_ELK)	BREAK // Set up second elk
			CASE 3	i_debug_jump_stage = ENUM_TO_INT(MS_SET_UP_BEFORE_THIRD_ELK)	BREAK // Set up third elk
			CASE 4	i_debug_jump_stage = ENUM_TO_INT(MS_SET_UP_BEFORE_FOURTH_ELK)	BREAK // Outro
		ENDSWITCH
	
		MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
		eSubState = SS_INIT
    	eMissionState = e_stage
    ENDIF
ENDPROC

PROC HUNTING_PRINT_DEBUG_INFO_TO_SCREEN()
	FLOAT fDisplayAtX	= 0.7
	FLOAT fDisplayAtY	= 0.1
	FLOAT fDisplayAdd	= 0.0125
	TEXT_LABEL_63 texDisplay
	
	texDisplay = "eMissionState: "
	texDisplay += GET_STRING_FROM_MISSION_STATE(eMissionState)
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	texDisplay = "eSubState: "
	texDisplay += GET_STRING_FROM_SUB_STATE(eSubState)
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	texDisplay = "eHunterSubState: "
	texDisplay += GET_STRING_FROM_HUNTER_SUB_STATE(eHunterSubState)
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	texDisplay = "eHunterState: "
	texDisplay += GET_STRING_FROM_HUNTER_STATE(eHunterState)
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	texDisplay = "ePreState: "
	texDisplay += GET_STRING_FROM_HUNTER_STATE(ePreState)
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
ENDPROC

/// PURPOSE:
///    Handles all debug recording of Hunting mission
PROC HUNTING_DEBUG_RECORDS()
	DEBUG_RECORD_CLEAR_ENTITIES()
	DEBUG_RECORD_ENTITY( PLAYER_PED_ID() )
	INT i
	REPEAT MAX_ELKS i
		IF IS_PED_UNINJURED( mElk[i].mPed )
			DEBUG_RECORD_ENTITY( mElk[i].mPed )
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF

// SCRIPT LOOP
SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	// Now on mission...
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
		TERMINATE_THIS_THREAD()	
	ENDIF
	
	LOCAL_RECORD_PLAYERS_GUN()
	SAFE_GIVE_SNIPER_TO_PLAYER(FALSE)
	RANDOM_CHAR_DISPLAY_MISSION_TITLE(RC_HUNTING_2)
			
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		CPRINTLN(DEBUG_HUNTING, "REPLAY IN PROGRESS")
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_HUNTING_2(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	mHunter = sRCLauncherDataLocal.pedID[0]
		
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		#IF IS_DEBUG_BUILD
		HUNTING_PRINT_DEBUG_INFO_TO_SCREEN()
		HUNTING_DEBUG_RECORDS()
		#ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_FG")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SET_DISABLE_RANDOM_TRAINS_THIS_FRAME(TRUE)
				
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			// This is needed so that common_hunting can put proper sized blips on the random elk calls.
			vHuntingPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF eMissionState != MS_MISSION_FAILED
			AND eMissionState != MS_INIT_MISSION
			AND eMissionState != MS_SET_UP_AFTER_INTRO
				CHECK_HUNTER_STATES()
				MONITER_PLAYER_AIMING_SOUNDS()
			ENDIF
			
			// We don't need to monitor the players noise levels after last elk has died.
			IF eMissionState != MS_MISSION_FAILED
			AND eMissionState != MS_INIT_MISSION
			AND eMissionState != MS_APPROACH_FOURTH_ELK
			AND eMissionState != MS_PHOTO_FOURTH_ELK
			AND eMissionState != MS_SEND_PHOTO
			AND eMissionState != MS_WAIT_FOR_REPLY
			AND eMissionState != MS_DO_FINAL_PHONECALL
				CHECK_NOISY_PLAYER()
			ENDIF
			
			IF bCanUpdateWindBlip
				SET_SNIPER_SCOPE_UNDER_HUD_THIS_FRAME()
				IF bWindCanChange
					UPDATE_WIND_BLIP(FALSE)
				ELSE
					UPDATE_WIND_BLIP(TRUE, 100.0)
				ENDIF
			ENDIF
			
			
			
			SWITCH eMissionState
				CASE MS_INIT_MISSION				INIT_MISSION()				BREAK
				CASE MS_RUN_INTRO					RUN_INTRO()					BREAK	
				CASE MS_SET_UP_AFTER_INTRO			SET_UP_AFTER_INTRO()		BREAK
				CASE MS_SET_UP_BEFORE_FIRST_ELK		SET_UP_BEFORE_FIRST_ELK()	BREAK
				CASE MS_FOLLOW_TO_FIRST_ELK			FOLLOW_TO_FIRST_ELK()		BREAK				
				CASE MS_KILL_FIRST_ELK				KILL_FIRST_ELK()			BREAK
				CASE MS_SET_UP_BEFORE_SECOND_ELK	SET_UP_BEFORE_SECOND_ELK()	BREAK
				CASE MS_FOLLOW_TO_SECOND_ELK		FOLLOW_TO_SECOND_ELK()		BREAK
				CASE MS_KILL_SECOND_ELK				KILL_SECOND_ELK()			BREAK
				CASE MS_SET_UP_BEFORE_THIRD_ELK		SET_UP_BEFORE_THIRD_ELK()	BREAK
				CASE MS_FOLLOW_TO_THIRD_ELK			FOLLOW_TO_THIRD_ELK()		BREAK
				CASE MS_KILL_THIRD_ELK				KILL_THIRD_ELK()			BREAK
				CASE MS_SET_UP_BEFORE_FOURTH_ELK	SET_UP_BEFORE_FOURTH_ELK()	BREAK
				CASE MS_HUNTER_LEAVES				HUNTER_LEAVES()				BREAK
				CASE MS_KILL_FOURTH_ELK				KILL_FOURTH_ELK()			BREAK
				CASE MS_APPROACH_FOURTH_ELK			APPROACH_FOURTH_ELK()		BREAK
				CASE MS_PHOTO_FOURTH_ELK			PHOTO_FOURTH_ELK()			BREAK
				CASE MS_SEND_PHOTO					SEND_PHOTO()				BREAK
				CASE MS_WAIT_FOR_REPLY				WAIT_FOR_REPLY()			BREAK
				CASE MS_DO_FINAL_PHONECALL			DO_FINAL_PHONECALL()		BREAK
				CASE MS_SET_UP_BEFORE_MISSION_PASS	SET_UP_BEFORE_MISSION_PASS()BREAK
				CASE MS_MISSION_PASSED				MISSION_PASSED()			BREAK
				CASE MS_MISSION_FAILED				FAILED_WAIT_FOR_FADE()		BREAK
			ENDSWITCH
					
			WAIT(0)

			#IF IS_DEBUG_BUILD
				IF eMissionState != MS_MISSION_FAILED
				AND eMissionState != MS_INIT_MISSION
					DEBUG_Check_Debug_Keys()
				ENDIF
			#ENDIF
		ENDIF
	ENDWHILE

#IF IS_DEBUG_BUILD
	//COMPILER HACK
	CHALLENGE_BACK_X = CHALLENGE_BACK_X
	CHALLENGE_BACK_Y = CHALLENGE_BACK_Y
	CHALLENGE_BACK_WIDTH = CHALLENGE_BACK_WIDTH
	CHALLENGE_BACK_HEIGHT = CHALLENGE_BACK_HEIGHT
	CHALLENGE_TEXT_OFFSET_X = CHALLENGE_TEXT_OFFSET_X
	CHALLENGE_TEXT_OFFSET_Y = CHALLENGE_TEXT_OFFSET_Y
	CHALLENGE_TEXT_TABBED_OFFSET_X = CHALLENGE_TEXT_TABBED_OFFSET_X
	CHALLENGE_TEXT_GOAL_BUFFER_Y = CHALLENGE_TEXT_GOAL_BUFFER_Y
	CHALLENGE_TEXT_OBJ_BUFFER_Y = CHALLENGE_TEXT_OBJ_BUFFER_Y
#ENDIF

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

