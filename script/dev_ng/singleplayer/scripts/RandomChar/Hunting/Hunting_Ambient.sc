

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_brains.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "common_Hunting.sch"
USING "common_Hunting_freemode.sch"
USING "common_hunting_challenges.sch"
USING "common_hunting_corpse.sch"
USING "common_hunting_leaderboards.sch"

USING "flow_public_game.sch"
USING "mission_control_public.sch"
USING "randomChar_private.sch"
USING "commands_pad.sch"

USING "minigame_uiinputs.sch"
USING "hud_drawing.sch"
USING "screens_header.sch"
USING "UIUtil.sch"
USING "script_oddjob_funcs.sch"
USING "script_usecontext.sch"
USING "Amb_Select_Screen.sch"
USING "script_camera.sch"
USING "script_drawing.sch"

USING "LauncherInclude.sch"
USING "mission_titles_private.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Hunting_Ambient.sc
//		AUTHOR			:	Rob Taylor
//		DESCRIPTION		:	The main script which handles all ambient hunting events.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//HUNT CREATOR
#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID widgetGroup
	BOOL bDebugGrazePoints = FALSE
	BOOL bDebugAnimalSpawns = FALSE
	BOOL bDebugTempPoints = FALSE
	BOOL bDebugTempArea = FALSE
	BOOL bDisplayChallengeTracking = FALSE
	BOOL bCreateGrazePos[MAX_GRAZE_AREAS]
	BOOL bPrintTempPoint[MAX_GRAZE_AREAS]
	
	BOOL bDisplayingElk = FALSE
	BOOL bDisplayingAnimals = FALSE
	BOOL bDisplayCoyotes = FALSE
	
	BOOL bCreateTempAreaPos[MAX_POLY_TEST_VERTS]
	BOOL bPrintTempArea[MAX_POLY_TEST_VERTS]
	BOOL bRecreateHuntArea = FALSE
	BOOL bDoHuntCreation = FALSE
	VECTOR vTempGrazePos[MAX_GRAZE_AREAS]
	VECTOR vTempArea[MAX_POLY_TEST_VERTS]
	VECTOR vTempAnimalSpawn[MAX_ANIMAL_SPAWN_POS]
	INT iHunterRank, iWeaponRank, iPhotoRank
	INT iHunterPrevRank, iWeaponPrevRank, iPhotoPrevRank, iDebugMoneyAddAmount
	BOOL bAddPoint, bAutoGold, bAdd10Pts, bAddMoney
	BOOL bForceSpawnCougar
	
	BOOL bDebugHuntArea = FALSE
	
	PROC SETUP_HUNT_CREATOR_WIDGET()
		START_WIDGET_GROUP("HUNT CREATOR")
			ADD_WIDGET_INT_SLIDER("Hunter Challenge Rank", 		iHunterRank, 0, 5, 1)
			ADD_WIDGET_INT_SLIDER("Weapon Challenge Rank", 		iWeaponRank, 0, 5, 1)
			ADD_WIDGET_INT_SLIDER("Photo Challenge Rank", 		iPhotoRank, 0, 5, 1)

			ADD_WIDGET_BOOL("Dislay Challenge Tracking", 	bDisplayChallengeTracking)
			//ADD_WIDGET_FLOAT_SLIDER("Wind Meter X", 	WIND_METER_X, -1.0, 1.0, 0.001)
			//ADD_WIDGET_FLOAT_SLIDER("Wind Meter Y", 	WIND_METER_Y, -1.0, 1.0, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("Chall Back X", 	CHALLENGE_BACK_X, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Back Y", 	CHALLENGE_BACK_Y, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Back W", 	CHALLENGE_BACK_WIDTH, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Back H", 	CHALLENGE_BACK_HEIGHT, 0.0, 1.0, 0.001)

			ADD_WIDGET_FLOAT_SLIDER("Chall Text X", 	CHALLENGE_TEXT_OFFSET_X, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Text Y", 	CHALLENGE_TEXT_OFFSET_Y, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Text Tab X", CHALLENGE_TEXT_TABBED_OFFSET_X, 0.0, 1.0, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("Chall Text Goal Buff Y", 	CHALLENGE_TEXT_GOAL_BUFFER_Y, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Chall Text Ob Buff Y", 	CHALLENGE_TEXT_OBJ_BUFFER_Y, 0.0, 1.0, 0.001)
			
			ADD_WIDGET_BOOL("Give Player a Point", bAddPoint)
			ADD_WIDGET_BOOL("Give Player 10 Points", bAdd10Pts)
			ADD_WIDGET_BOOL("Set Player To Gold", bAutoGold)
			ADD_WIDGET_INT_SLIDER("Debug Money Addition", iDebugMoneyAddAmount, 0, 9999999, 5)
			ADD_WIDGET_BOOL("Give Player Money", bAddMoney)
			
			ADD_WIDGET_BOOL("Do Hunt Creation", 	bDoHuntCreation)

			ADD_WIDGET_BOOL("Debug Graze Points", 	bDebugGrazePoints)
			ADD_WIDGET_BOOL("Debug Temp Points", 	bDebugTempPoints)
			ADD_WIDGET_BOOL("Debug Hunt Area", 		bDebugHuntArea)
			ADD_WIDGET_BOOL("Debug Temp Hunt Area", bDebugTempArea)
			ADD_WIDGET_BOOL("Debug Animal Spawns", 	bDebugAnimalSpawns)
			
			ADD_WIDGET_BOOL("Debug Draw Elk", 		bDisplayingElk)
			ADD_WIDGET_BOOL("Debug Draw Animals", 	bDisplayingAnimals)
			ADD_WIDGET_BOOL("Debug Draw Coyotes", 	bDisplayCoyotes)
			
			ADD_WIDGET_BOOL("Force Spawn Cougar", 	bForceSpawnCougar)
			
			
			START_WIDGET_GROUP("GRAZE POSITIONS")
				INT idx
				START_WIDGET_GROUP("Positions 0 - 4")
					FOR idx = 0 TO 4
						TEXT_LABEL_31 txtToDraw = "Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_VECTOR_SLIDER(txtToDraw, vTempGrazePos[idx], -7000.0, 7000.0, 0.1)
						
						txtToDraw = "Create Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bCreateGrazePos[idx])
						
						txtToDraw = "Print Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bPrintTempPoint[idx])
					ENDFOR
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Positions 5 - 9")
					FOR idx = 5 TO 9
						TEXT_LABEL_31 txtToDraw = "Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_VECTOR_SLIDER(txtToDraw, vTempGrazePos[idx], -7000.0, 7000.0, 0.1)
						
						txtToDraw = "Create Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bCreateGrazePos[idx])
						
						txtToDraw = "Print Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bPrintTempPoint[idx])
					ENDFOR
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Positions 10 - 13")
					FOR idx = 10 TO 13
						TEXT_LABEL_31 txtToDraw = "Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_VECTOR_SLIDER(txtToDraw, vTempGrazePos[idx], -7000.0, 7000.0, 0.1)
						
						txtToDraw = "Create Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bCreateGrazePos[idx])
						
						txtToDraw = "Print Graze Pos "
						txtToDraw += idx
						ADD_WIDGET_BOOL(txtToDraw, bPrintTempPoint[idx])
					ENDFOR
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HUNT AREA")
				FOR idx = 0 TO 7
					TEXT_LABEL_31 txtToDraw = "Hunt Area Pos "
					txtToDraw += idx
					ADD_WIDGET_VECTOR_SLIDER(txtToDraw, vTempArea[idx], -7000.0, 7000.0, 0.1)
					
					txtToDraw = "Create Hunt Area Pos "
					txtToDraw += idx
					ADD_WIDGET_BOOL(txtToDraw, bCreateTempAreaPos[idx])
					
					txtToDraw = "Print Hunt Area Pos "
					txtToDraw += idx
					ADD_WIDGET_BOOL(txtToDraw, bPrintTempArea[idx])
				ENDFOR			
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("ANIMAL SPAWNS")
				FOR idx = 0 TO MAX_ANIMAL_SPAWN_POS-1
					TEXT_LABEL_31 txtToDraw = "Animal Spawn Pos "
					txtToDraw += idx
					ADD_WIDGET_VECTOR_SLIDER(txtToDraw, vTempAnimalSpawn[idx], -7000.0, 7000.0, 0.1)
				ENDFOR			
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	
	ENDPROC

	/// PURPOSE:
	///    Creates mission widget and adds the needed sliders etc.
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("MISSION WIDG")
			SETUP_HUNT_CREATOR_WIDGET()
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    Deletes the widget group
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Saves a vector to the debug file
	/// PARAMS:
	///    name - Name of the vector 
	///    thing - the VECTOR you want to save
	PROC WRITE_VECTOR_TO_DEBUG(STRING name, VECTOR thing)
		SAVE_STRING_TO_DEBUG_FILE(name)
		SAVE_VECTOR_TO_DEBUG_FILE(thing)
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	ENDPROC


	/// PURPOSE:
	///    Moniters for changes to the widgets
	PROC UPDATE_RAG_WIDGETS()
		INT idx
		IF bDoHuntCreation
			IF NOT bDebugTempPoints
				bDebugTempPoints = TRUE
			ENDIF
			
			IF NOT bDebugHuntArea
				bDebugHuntArea = TRUE
			ENDIF

			REPEAT MAX_GRAZE_AREAS idx		
				IF bCreateGrazePos[idx]			
					vTempGrazePos[idx] = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
			ENDREPEAT

			FOR idx = 0 TO (MAX_POLY_TEST_VERTS-1)				
				IF bCreateTempAreaPos[idx]			
					vTempArea[idx] = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
			ENDFOR
			
			IF bRecreateHuntArea
				POPULATE_POLY_CHECKS(vTempArea)
				bRecreateHuntArea = FALSE
			ENDIF
		ENDIF
		
		IF bAddPoint
			bAddPoint = FALSE
			Player_Hunt_Data.iScore++
		ENDIF
		IF bAdd10Pts
			bAdd10Pts = FALSE
			Player_Hunt_Data.iScore += 10
		ENDIF
		IF bAutoGold
			bAutoGold = FALSE
			Player_Hunt_Data.iScore = Player_Hunt_Data.iScoreForGold
		ENDIF
		IF bAddMoney
			bAddMoney = FALSE
			Player_Hunt_Data.iMoney += iDebugMoneyAddAmount
		ENDIF
		
		IF bForceSpawnCougar
			bForceSpawnCougar = FALSE
			IF NOT DOES_ENTITY_EXIST(mCougar[0].mPed)
				mCougar[0].iSpawnDelayTime = 0
			ELIF NOT DOES_ENTITY_EXIST(mCougar[1].mPed)
				mCougar[1].iSpawnDelayTime = 0
			ENDIF
		ENDIF
		
		IF bDebugTempPoints
			REPEAT MAX_GRAZE_AREAS idx	
				DRAW_DEBUG_SPHERE(vTempGrazePos[idx], 2,0,0,255,100)
				
				IF bPrintTempPoint[idx]
					TEXT_LABEL GrazeName = "vGrazePos["
					GrazeName += idx
					GrazeName += "] = "
					OPEN_DEBUG_FILE()
						WRITE_VECTOR_TO_DEBUG(GrazeName, vTempGrazePos[idx])
					CLOSE_DEBUG_FILE()
					bPrintTempPoint[idx] = FALSE
				ENDIF
			ENDREPEAT
		ENDIF
		
		FOR idx = 0 TO MAX_POLY_TEST_VERTS-1
			IF bDebugTempArea
				DRAW_DEBUG_SPHERE(vTempArea[idx], 5, 255, 255, 0)
			ENDIF
			
			IF bPrintTempArea[idx]
				TEXT_LABEL AreaName = "vPoly["
				AreaName += idx
				AreaName += "] = "
				OPEN_DEBUG_FILE()
					WRITE_VECTOR_TO_DEBUG(AreaName, vTempArea[idx])
				CLOSE_DEBUG_FILE()
				bPrintTempArea[idx] = FALSE
			ENDIF
		ENDFOR

		IF bDebugGrazePoints
			REPEAT MAX_GRAZE_AREAS idx	
				DRAW_DEBUG_SPHERE(elkSpawn[idx].vCoord, 5, 255,0,0,100)
				
				TEXT_LABEL_31 txtTemp = "Can spawn in: "
				txtTemp += elkSpawn[idx].iNextSpawnTime - GET_GAME_TIMER()
				DRAW_DEBUG_TEXT(txtTemp, elkSpawn[idx].vCoord + <<0,0,3>>)
			ENDREPEAT
		ENDIF
		
		IF bDebugHuntArea
			FOR idx = 0 TO mAreaCheck1.iVertCount-1
				DRAW_DEBUG_SPHERE(mAreaCheck1.v[idx], 5, 0, 255, 0)
			ENDFOR
		ENDIF
		
		IF bDebugAnimalSpawns
			FOR idx = 0 TO MAX_ANIMAL_SPAWN_POS-1
				DRAW_DEBUG_SPHERE(vAnimalSpawnPos[idx], 5, 255, 255, 0, 100)
			ENDFOR
		ENDIF
		
		IF bDisplayCoyotes
			REPEAT MAX_RANDOM_ANIMALS idx
				IF (mAnimal[idx].animalType = ANIMAL_TYPE_COYOTE)
					IF DOES_ENTITY_EXIST(mAnimal[idx].mPed)
					AND NOT IS_ENTITY_DEAD(mAnimal[idx].mPed)
						VECTOR vCoord = GET_ENTITY_COORDS(mAnimal[idx].mPed)
						DRAW_DEBUG_SPHERE(vCoord, 2, 0, 255, 255, 100)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
	
		IF iHunterRank != ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank)
			IF iHunterRank = iHunterPrevRank // The player ranked up on his own
				iHunterRank = ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank)
			ELSE	// The widget hit
				Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank = INT_TO_ENUM(HUNTING_CHALLENGE_RANK, iHunterRank)
				Player_Hunt_Data.bChallengeScreenSetup = FALSE
			ENDIF
		ENDIF
		iHunterPrevRank = iHunterRank

		IF iPhotoRank != ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank)
			IF iPhotoRank = iPhotoPrevRank // The player ranked up on his own
				iPhotoRank = ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank)
			ELSE	// The widget hit
				Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank = INT_TO_ENUM(HUNTING_CHALLENGE_RANK, iPhotoRank)
				Player_Hunt_Data.bChallengeScreenSetup = FALSE
			ENDIF
		ENDIF
		iPhotoPrevRank = iPhotoRank
		
		IF iWeaponRank != ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank)
			IF iWeaponRank = iWeaponPrevRank // The player ranked up on his own
				iWeaponRank = ENUM_TO_INT(Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank)
			ELSE	// The widget hit
				Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank = INT_TO_ENUM(HUNTING_CHALLENGE_RANK, iWeaponRank)
				Player_Hunt_Data.bChallengeScreenSetup = FALSE
			ENDIF
		ENDIF
		iWeaponPrevRank = iWeaponRank
		
		IF bDisplayingElk
			REPEAT MAX_ELKS idx
				IF DOES_ENTITY_EXIST(mElk[idx].mPed)
				AND NOT IS_ENTITY_DEAD(mElk[idx].mPed)
					VECTOR vElkCoord = GET_ENTITY_COORDS(mElk[idx].mPed)
					IF mElk[idx].bIsHerdLeader 
						DRAW_DEBUG_SPHERE(vElkCoord, 2, 0, 0, 255, 255)
					ELIF mElk[idx].bIsHerdMember
						DRAW_DEBUG_SPHERE(vElkCoord, 2, 0, 255, 0, 255)
					ELSE
						DRAW_DEBUG_SPHERE(vElkCoord, 2, 255, 0, 0, 255)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF bDisplayingAnimals
			REPEAT MAX_RANDOM_ANIMALS idx
				IF DOES_ENTITY_EXIST(mAnimal[idx].mPed)
				AND NOT IS_ENTITY_DEAD(mAnimal[idx].mPed)
					VECTOR vAnimalCoord = GET_ENTITY_COORDS(mAnimal[idx].mPed)
					DRAW_DEBUG_SPHERE(vAnimalCoord, 2, 0, 255, 0, 255)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDPROC
#ENDIF

INT iHuntCandidateID = NO_CANDIDATE_ID

ENUM HUNTSTATE
	HS_SETUP,
	HS_HUNT,
	HS_RESULTS
ENDENUM

ENUM STATE_STAGE
	SS_INIT,
	SS_POST_INIT,
	SS_ACTIVE,
	SS_TRANSIT,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

ENUM MISSION_REQ
	RQ_NONE,
	RQ_INTRO,
	RQ_ANIMS,
	RQ_DEER_MODEL,
	RQ_RIFLE,
	RQ_CAMERA,
	RQ_CHALLENGES,
	RQ_SOUND,
	RQ_TEXTURES
ENDENUM

ENUM FINISH_STATE
	FS_NULL,
	FS_PASSED,
	FS_FAILED_TIME,
	FS_FAILED_SPOOKED,
	FS_FAILED_LEFT,
	FS_FAILED_WRONG_WEAPON,
	FS_FAILED_WANTED,
	FS_CANCELLED
ENDENUM


HUNTSTATE eHuntState = HS_SETUP
STATE_STAGE eStage = SS_INIT
FINISH_STATE eFinishState = FS_NULL

// ===========================
// 		VARIABLES
// ===========================

structTimer				tCutsceneTimer
INT iMissionState		= 0				//Used in NEXT_STAGE( BOOL bReverse = FALSE) and during jump skips
INT iMissionEndTime		= 0

BOOL bShowingCancelScreen	= FALSE
BOOL bMissionEnded			= FALSE
BOOL bIsContinuing 			= FALSE		// RSP: Results menu.
BOOL			bSentFailText				= FALSE
BOOL			bSentWarnText				= FALSE
BOOL			bGaveRifle = FALSE
BOOL bHuntTitleReady		= FALSE
VECTOR vIntroCamRot

VECTOR vPlayerStartPos 
FLOAT fPlayerStartDir 
VEHICLE_INDEX 				vehTruck 
BOOL						bTruckGrabbedAtStart = FALSE
STREAMVOL_ID				streamVolResults

// HUD TIMER
INT							iMissionTitleState = 0

INT							iNavMeshLoadTime = 0
CONST_INT					HUNTING_NAVMESH_LOAD_DELAY		5000

SCENARIO_BLOCKING_INDEX 	mBlockArea

BOOL bShownBronzeToast 		= FALSE
BOOL bShownSilverToast 		= FALSE
BOOL bShownGoldToast 		= FALSE

// INTRO DATA
INT iSceneID
CAMERA_INDEX camScene
CAMERA_INDEX camSky
INT iQuitSceneState = 0
INT iQuitMenuDelay = 0

INT iLBDWarning = 0
BOOL bPredictionSetup = FALSE

// ==========================
// MISSION FLOW CONTROL
// ==========================

/// PURPOSE:
///    Advances or reverses the mission stage  
/// PARAMS:
///    bReverse - if true the mission state is reversed
PROC NEXT_STAGE( BOOL bReverse = FALSE)

	iMissionState = ENUM_TO_INT(eHuntState)
	
	IF NOT bReverse
		eHuntState = INT_TO_ENUM(HUNTSTATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eHuntState = INT_TO_ENUM(HUNTSTATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	
	eStage = SS_INIT	
ENDPROC

PROC DELETE_ALL_HUNTING_TEXT_MESSAGES(BOOL bOnlyCletusTexts = TRUE)
	INT iMsgIdx                                              
    REPEAT MAX_TEXT_MESSAGES iMsgIdx
		IF NOT IS_STRING_NULL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[iMsgIdx].TxtMsgLabel)
			IF (bOnlyCletusTexts 
			AND (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[iMsgIdx].TxtMsgSender = CHAR_HUNTER))
			OR NOT bOnlyCletusTexts
				DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[iMsgIdx].TxtMsgLabel)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Cleanup(BOOL bForceCleanup = FALSE)
	PRINTLN("...Ambient Hunting Cleanup")

	// Emergency write, in the event that the script shuts down, but we haven't finished our predictive write.
	IF bPredictionSetup
	AND NOT scLB_rank_predict.bFinishedWrite
		HUNTING_WRITE_TO_LEADERBOARD()
	ENDIF
	CLEAR_RANK_REDICTION_DETAILS()
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(Player_Hunt_Data.huntLB_control)
	
	// Restores the paler's gun state, and takes away infinite ammo.
	RETURN_PLAYERS_GUN(bForceCleanup)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(A_C_DEER)
	
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("AR_knife_low_kick_far"), TRUE)
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("AR_knife_low_kick_close"), TRUE)
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_close"), TRUE)
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_far"), TRUE)
	
	CLEANUP_RANDOM_ANIMALS()
	
	SET_RANDOM_TRAINS(TRUE)
	REMOVE_SCENARIO_BLOCKING_AREA(mBlockArea)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_RELATIONSHIP_GROUP(mElkGroup)
	
	IF IS_AUDIO_SCENE_ACTIVE("HUNTING_02_SETTINGS")
		STOP_AUDIO_SCENE("HUNTING_02_SETTINGS")
		STOP_SOUND(iSoundID)
		RELEASE_SOUND_ID(iSoundID)
	ENDIF
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\HUNTING_MAIN_A")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_CALLS")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_VOCALS")
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	IF DOES_ENTITY_EXIST(vehTruck)
	AND NOT IS_ENTITY_DEAD(vehTruck)
		SET_VEHICLE_HAS_STRONG_AXLES(vehTruck, TRUE)
	ENDIF

	REMOVE_NAVMESH_REQUIRED_REGIONS()

	RC_END_CUTSCENE_MODE()
	STOP_AUDIO_SCENE("AMBIENT_HUNTING_MIX")
	
	DELETE_ALL_HUNTING_TEXT_MESSAGES()

	#IF IS_DEBUG_BUILD
		CLEANUP_OBJECT_WIDGETS()
	#ENDIF

	MISSION_OVER(iHuntCandidateID)
	RC_MISSION_OVER(NO_RC_MISSION, FALSE)
	DISABLE_CELLPHONE(FALSE)
	RELEASE_PRELOADED_OUTFIT(PLAYER_PED_ID())
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)

	// Allow Hunting to be launched again as likely to be within trigger distance of the world point
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("launcher_Hunting_Ambient")

	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	
	SET_AUDIO_FLAG("AllowPlayerAIOnMission", FALSE)
	PAUSE_CLOCK(FALSE)
	
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	
	SHUTDOWN_PC_SCRIPTED_CONTROLS()

	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Passed()

	PRINTLN("...Ambient Hunting Passed")
	
	// Play mission passed audio
	//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
	Mission_Cleanup()

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Failed()
	PRINTLN("...Ambient Hunting Failed")
	Mission_Cleanup()
ENDPROC

/// PURPOSE:
///    Creates a camera - Used for cutscenes
/// PARAMS:
///    pos - The position to create the camera at 
///    rot - The rotation of the camera (VECTOR not Matrix)
/// RETURNS:
///    TRUE if the camera was created
FUNC BOOL SETUP_CAMERA(VECTOR pos, VECTOR rot)
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF NOT DOES_CAM_EXIST(camScene)
			camScene = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",pos,rot, 50, TRUE)			
			//RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF DOES_CAM_EXIST(camScene)
				SET_CAM_PARAMS(camScene,pos,rot,50.000000)
				//SHAKE_CAM(camMain, "HAND_SHAKE")
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Sets up poly area and graze positions for the specific hunt location
PROC SET_UP_HUNT_AREA()
	// Chilead Mountain Hunting
	VECTOR vPoly[MAX_POLY_TEST_VERTS]
	vPoly[0] 		= <<-2151.1272, 4431.3477, 61.9329>>
	vPoly[1] 		= <<-1899.1481, 4044.1296, 246.3255>>
	vPoly[2] 		= <<-224.7574, 3868.3560, 37.5636>>
	vPoly[3] 		= <<-58.7894, 4365.2559, 52.7727>>
	vPoly[4] 		= <<-316.4930, 4684.7959, 248.4118>>
	vPoly[5] 		= <<-884.8384, 4907.8447, 273.6012>>
	vPoly[6] 		= <<-1263.8662, 5396.5322, -0.0613>>
	vPoly[7] 		= <<-1753.4866, 5058.2876, 20.8162>>
	POPULATE_POLY_CHECKS(vPoly, MAX_POLY_TEST_VERTS)
	
	elkSpawn[0].vCoord = <<-1511.7775, 4784.5337, 59.8112>>
	elkSpawn[1].vCoord = <<-1398.8132, 4454.7363, 23.8370>>
	elkSpawn[2].vCoord = <<-1352.4169, 4698.8076, 64.4277>>
	elkSpawn[3].vCoord = <<-1461.1115, 4598.0430, 47.5845>>
	elkSpawn[4].vCoord = <<-1599.1580, 4833.4448, 55.0841>>
	elkSpawn[5].vCoord = <<-1937.9379, 4543.3823, 9.0519>>
	elkSpawn[6].vCoord = <<-1517.0697, 4718.6260, 45.1539>>
	elkSpawn[7].vCoord = <<-1339.6239, 4434.0605, 26.5921>>
	elkSpawn[8].vCoord = <<-1493.7471, 4438.7080, 17.6355>>
	elkSpawn[9].vCoord = <<-1553.9728, 4448.3848, 11.3850>>
	elkSpawn[10].vCoord = <<-1671.2791, 4772.9170, 45.7077>>
	elkSpawn[11].vCoord = <<-1586.5284, 4522.0059, 16.6437>>
	elkSpawn[12].vCoord = <<-1437.8397, 4718.1875, 41.1638>>
	elkSpawn[13].vCoord = <<-1660.6385, 4558.4946, 40.7479>>
	elkSpawn[14].vCoord = <<-1663.3369, 4601.2656, 46.9536>>
	elkSpawn[15].vCoord = <<-1722.6177, 4690.5630, 31.0306>>
	elkSpawn[16].vCoord = <<-1522.1996, 4530.8843, 44.9653>>
	elkSpawn[17].vCoord = <<-1550.4271, 4770.5918, 61.3469>>
	elkSpawn[18].vCoord = <<-1583.5095, 4669.6318, 44.7582>>
	elkSpawn[19].vCoord = <<-1738.6082, 4630.1387, 29.7174>>
	elkSpawn[20].vCoord = <<-425.1335, 4331.4390, 58.2179>>
	elkSpawn[21].vCoord = <<-1227.6572, 4959.6426, 171.8550>>
	elkSpawn[22].vCoord = <<-756.1689, 4744.0537, 229.8765>>
	elkSpawn[23].vCoord = <<-237.4276, 4428.7231, 54.4526>>
	elkSpawn[24].vCoord = <<-952.6023, 4363.2085, 10.8284>>
	elkSpawn[25].vCoord = <<-1415.1010, 4398.0420, 44.0559>>
	elkSpawn[26].vCoord = <<-1267.3019, 4411.9858, 9.6374>>
	elkSpawn[27].vCoord = <<-1259.8344, 4347.8838, 7.0597>>
	elkSpawn[28].vCoord = <<-1655.3364, 4453.8853, 0.6491>>
	elkSpawn[29].vCoord = <<-1817.8634, 4509.4414, 25.4081>>
	elkSpawn[30].vCoord = <<-1815.6727, 4420.2090, 45.1439>>
	elkSpawn[31].vCoord = <<-1657.9868, 4281.4907, 80.7132>>
	elkSpawn[32].vCoord = <<-1518.7003, 4235.5576, 63.0872>>
	elkSpawn[33].vCoord = <<-1405.1924, 4251.4434, 42.5107>>
	elkSpawn[34].vCoord = <<-1148.8406, 4580.3638, 140.3432>>
	elkSpawn[35].vCoord = <<-1314.3433, 4523.1592, 28.3174>>
	elkSpawn[36].vCoord = <<-1269.1595, 4665.2861, 92.2077>>
	elkSpawn[37].vCoord = <<-1060.3892, 4461.9741, 85.3426>>
	elkSpawn[38].vCoord = <<-960.9619, 4404.7988, 15.1577>>
	elkSpawn[39].vCoord = <<-798.0543, 4421.8691, 16.6733>>
	elkSpawn[40].vCoord = <<-765.4644, 4600.2295, 123.3193>>
	elkSpawn[41].vCoord = <<-729.2116, 4628.9199, 128.4458>>
	elkSpawn[42].vCoord = <<-622.1554, 4514.7295, 82.0527>>
	elkSpawn[43].vCoord = <<-368.5648, 4375.5923, 49.8344>>
	elkSpawn[44].vCoord = <<-299.4890, 4294.9604, 40.4718>>
	elkSpawn[45].vCoord = <<-291.6694, 4600.0454, 139.0713>>
	elkSpawn[46].vCoord = <<-167.2124, 4336.2563, 49.6604>>
	elkSpawn[47].vCoord = <<-160.3564, 4279.9766, 35.9502>>
	elkSpawn[48].vCoord = <<-326.4175, 4210.2095, 48.8774>>
	elkSpawn[49].vCoord = <<-353.8134, 4276.1997, 46.2023>>
	elkSpawn[50].vCoord = <<-304.5437, 4335.7002, 31.2610>>
	elkSpawn[51].vCoord = <<-1211.7333, 4801.1953, 212.5187>>
	elkSpawn[52].vCoord = <<-1116.1746, 4790.0840, 222.5111>>
	elkSpawn[53].vCoord = <<-973.5195, 4338.3721, 13.6037>>
	elkSpawn[54].vCoord = <<-755.8174, 4505.7480, 83.2461>>
	elkSpawn[55].vCoord = <<-1175.0583, 4435.0938, 26.3625>>
	elkSpawn[56].vCoord = <<-1223.8759, 4458.6499, 29.3014>>
	elkSpawn[57].vCoord = <<-173.0567, 4480.7612, 69.3354>>
	
	vAnimalSpawnPos[0] = <<-1381.4938, 4416.4858, 27.8185>>
	vAnimalSpawnPos[1] = <<-1538.3176, 4823.2402, 72.7811>>
	vAnimalSpawnPos[2] = <<-1499.8807, 4563.8755, 36.7863>>
	vAnimalSpawnPos[3] = <<-1636.7708, 4576.7969, 42.0251>>
	vAnimalSpawnPos[4] = <<-1700.8790, 4598.8154, 45.4030>>
	vAnimalSpawnPos[5] = <<-1643.5792, 4651.7920, 29.0423>>
	vAnimalSpawnPos[6] = <<-1817.5378, 4509.4966, 25.4984>>
	vAnimalSpawnPos[7] = <<-1479.5502, 4654.1011, 43.9694>>
	vAnimalSpawnPos[8] = <<-1492.2996, 4481.0200, 17.2195>>
	vAnimalSpawnPos[9] = <<-1474.0715, 4409.0679, 22.5295>>
	vAnimalSpawnPos[10] = <<-1633.4824, 4734.9858, 52.2606>>
	vAnimalSpawnPos[11] = <<-1607.5784, 4641.0571, 47.0786>>
	vAnimalSpawnPos[12] = <<-1532.2561, 4566.3496, 39.2169>>
	vAnimalSpawnPos[13] = <<-1430.2557, 4409.0273, 45.7995>>
	vAnimalSpawnPos[14] = <<-1531.6063, 4613.4048, 28.4776>>
	vAnimalSpawnPos[15] = <<-1583.5251, 4506.5020, 19.5027>>
	vAnimalSpawnPos[16] = <<-1625.8840, 4792.8696, 50.1408>>
	vAnimalSpawnPos[17] = <<-1496.5253, 4736.5146, 54.6202>>
	vAnimalSpawnPos[18] = <<-1518.8767, 4423.6802, 11.3467>>
	vAnimalSpawnPos[19] = <<-1706.0890, 4717.6768, 39.8586>>
	vAnimalSpawnPos[20] = <<-354.4631, 3932.3337, 72.4634>>
	vAnimalSpawnPos[21] = <<-319.2273, 4281.0005, 48.6787>>
	vAnimalSpawnPos[22] = <<-1659.1747, 5006.9360, 35.1188>>
	vAnimalSpawnPos[23] = <<-906.1866, 4822.8911, 304.3815>>
	vAnimalSpawnPos[24] = <<-964.5157, 4402.6396, 15.0070>>
	vAnimalSpawnPos[25] = <<-1216.5475, 4454.5537, 29.5726>>
	vAnimalSpawnPos[26] = <<-1259.2340, 4675.2837, 88.2272>>
	vAnimalSpawnPos[27] = <<-1295.1776, 4747.4429, 95.7002>>
	vAnimalSpawnPos[28] = <<-1257.1818, 4920.1128, 176.1196>>
	vAnimalSpawnPos[29] = <<-764.3734, 4216.8989, 179.9306>>
	vAnimalSpawnPos[30] = <<-1294.6312, 4605.9229, 120.0947>>
	vAnimalSpawnPos[31] = <<-1289.1989, 4495.6924, 13.2293>>
	vAnimalSpawnPos[32] = <<-1930.0492, 4541.5229, 9.7390>>
	vAnimalSpawnPos[33] = <<-1886.6348, 4489.0132, 26.3309>>
	vAnimalSpawnPos[34] = <<-1682.3026, 4422.8516, 10.8683>>
	vAnimalSpawnPos[35] = <<-1543.6766, 4341.0200, 0.9957>>
	vAnimalSpawnPos[36] = <<-1407.6846, 4309.8545, 3.1902>>
	vAnimalSpawnPos[37] = <<-1272.5139, 4401.5811, 9.3275>>
	vAnimalSpawnPos[38] = <<-1215.9462, 4304.0557, 73.9030>>
	vAnimalSpawnPos[39] = <<-1240.0525, 4302.0342, 68.3904>>
	vAnimalSpawnPos[40] = <<-1078.8168, 4554.2168, 94.6354>>
	vAnimalSpawnPos[41] = <<-1058.0194, 4463.2783, 85.9198>>
	vAnimalSpawnPos[42] = <<-808.1082, 4427.5737, 15.6468>>
	vAnimalSpawnPos[43] = <<-688.2943, 4412.1401, 16.9693>>
	vAnimalSpawnPos[44] = <<-555.9728, 4502.3765, 68.8630>>
	vAnimalSpawnPos[45] = <<-644.8114, 4566.9487, 123.9569>>
	vAnimalSpawnPos[46] = <<-410.4391, 4655.3677, 238.8684>>
	vAnimalSpawnPos[47] = <<-226.5519, 4488.9829, 51.3667>>
	vAnimalSpawnPos[48] = <<-189.8527, 4433.2510, 45.4967>>
	vAnimalSpawnPos[49] = <<-121.9785, 4376.0630, 67.7734>>
	vAnimalSpawnPos[50] = <<-121.9785, 4376.0630, 67.7734>>
	vAnimalSpawnPos[51] = <<-121.9785, 4376.0630, 67.7734>>
	vAnimalSpawnPos[52] = <<-121.9785, 4376.0630, 67.7734>>
	vAnimalSpawnPos[53] = <<-121.9785, 4376.0630, 67.7734>>
	vAnimalSpawnPos[54] = <<-398.0512, 4352.7363, 54.0697>>
	
	vPlayerStartPos	= <<-1706.6096, 4657.9590, 21.9050>>
	fPlayerStartDir	= 327.0001
	
	CPRINTLN(DEBUG_MISSION,"SET UP HUNT AREA = HUNT 3")
ENDPROC


/// PURPOSE:
///    Set the additional elk to be herd members that only spawn in when specifically requested.
PROC SETUP_HERD_STATES()
	INT i = HERD_MEMBER_INDEX_START
	WHILE i < MAX_ELKS
		mElk[i].elkState = ES_NULL
		++i
	ENDWHILE
ENDPROC


/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq, VECTOR vPosition, BOOL bHideHUD = TRUE)
	IF bHideHUD
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	SWITCH missionReq
		CASE RQ_INTRO
			REQUEST_ANIM_DICT("oddjobs@hunterIntro")
			REQUEST_ANIM_DICT("oddjobs@hunterOutro")
			IF HAS_ANIM_DICT_LOADED("oddjobs@hunterIntro")
			AND HAS_ANIM_DICT_LOADED("oddjobs@hunterOutro")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_ANIMS
			REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@enter")
			REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@idle_a")
			REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@exit")
			REQUEST_ANIM_DICT("creatures@deer@amb@world_deer_grazing@base")
			REQUEST_ANIM_DICT("creatures@cougar@melee@")
			REQUEST_ANIM_DICT("facials@p_m_one@variations@elkcall")
			IF HAS_ANIM_DICT_LOADED ("creatures@deer@amb@world_deer_grazing@enter") //amb@medic@standing@kneel@enter")
			AND HAS_ANIM_DICT_LOADED ("creatures@deer@amb@world_deer_grazing@idle_a") //amb@medic@standing@kneel@enter")
			AND HAS_ANIM_DICT_LOADED ("creatures@deer@amb@world_deer_grazing@exit") //amb@medic@standing@kneel@enter")
			AND HAS_ANIM_DICT_LOADED ("creatures@deer@amb@world_deer_grazing@base") //amb@medic@standing@kneel@enter")
			AND HAS_ANIM_DICT_LOADED("creatures@cougar@melee@")
			AND HAS_ANIM_DICT_LOADED("facials@p_m_one@variations@elkcall")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_CAMERA
			IF SETUP_CAMERA(vPosition, vIntroCamRot)
				RETURN TRUE
			ENDIF
		BREAK

		CASE RQ_DEER_MODEL
			CPRINTLN(DEBUG_MISSION,"REQUESTING MODELS... DEER")
			REQUEST_MODEL (A_C_DEER)
			IF HAS_MODEL_LOADED(A_C_DEER)
				CPRINTLN(DEBUG_MISSION,"LOADED MODELS ... DEER")
				RETURN TRUE
			ENDIF
		BREAK

		CASE RQ_RIFLE
			SAFE_GIVE_SNIPER_TO_PLAYER()
			RETURN TRUE
		BREAK
		
		CASE RQ_CHALLENGES
			CPRINTLN(DEBUG_MISSION,"REQUESTING SCALEFORM MOVIE FOR CHALLENGES..")
			REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
			Player_Hunt_Data.challengeScreen.splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
			IF HAS_SCALEFORM_MOVIE_LOADED(Player_Hunt_Data.challengeScreen.splash)
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu")
				CPRINTLN(DEBUG_MISSION,"LOADED .... SCALEFORM MOVIE")
				RETURN TRUE
			ENDIF		
		BREAK

		CASE RQ_SOUND
			CPRINTLN(DEBUG_MISSION,"REQUESTING AUDIO...")
			IF REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_CALLS")
			AND REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_2_ELK_VOCALS") 		
			AND REQUEST_MISSION_AUDIO_BANK("SCRIPT\\HUNTING_MAIN_A")
				CPRINTLN(DEBUG_MISSION,"LOADED ... AUDIO")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXTURES
			CPRINTLN(DEBUG_MISSION,"REQUESTING TEXTURES...")
			REQUEST_PTFX_ASSET()
			REQUEST_STREAMED_TEXTURE_DICT("Hunting")
			REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
			REQUEST_STREAMED_TEXTURE_DICT("MPHud")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("Hunting")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
			AND HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud")
			AND HAS_PTFX_ASSET_LOADED()
				CPRINTLN(DEBUG_MISSION,"TEXTURES LOADED")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Basic setup of the mission 
///    turns of player control
///    creates poly area
///    populates graze points
///    turns off police
///    sets the ambient flag for the hunting common.sch file
PROC INIT_HUNT()
	CPRINTLN(DEBUG_MISSION,"INIT_HUNT")
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_HUNTER, TREVOR_BOOK, FALSE)	

	SET_UP_HUNT_AREA()
	SET_UP_SCORES()
	UNBOOK_EVERY_GRAZE_POS()
	bIsAmbient = TRUE
	iMissionTitleState = 0
	bSentFailText = FALSE
	bSentWarnText = FALSE
	
	START_AUDIO_SCENE("AMBIENT_HUNTING_MIX")

	ADD_RELATIONSHIP_GROUP("Elk Group", mElkGroup)
	
	INIT_PC_SCRIPTED_CONTROLS("HUNTING ELK CALL")
	
	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	
	Player_Hunt_Data.splashUI.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	SETUP_HERD_STATES()
		
	SET_AUDIO_FLAG("AllowPlayerAIOnMission", TRUE)
	
	// If the palyer is trying to be cheeky and drops a grenade in the vicinity before the mission, just delete it.
	CLEAR_AREA_OF_PROJECTILES(<< -1702.6951, 4666.9414, 22.7091>>, 50.0)
	
	// Set next random bleat in the future.
	iBleatInitiateTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(BLEAT_TRIGGER_MIN_DELAY, BLEAT_TRIGGER_MAX_DELAY)
	
	REQUEST_ADDITIONAL_TEXT("AMBHT", MISSION_TEXT_SLOT)
	REQUEST_SCRIPT_AUDIO_BANK ("HUD_AWARDS")	
	//CANCEL_COMMUNICATION(TEXT_SRANGE_UNLOCK)
	
	// We need the intro animations immediately! request them here!
	SETUP_STAGE_REQUIREMENTS(RQ_INTRO, <<0,0,0>>)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_HUNTING)
	ENDIF
	
	UNUSED_PARAMETER(m_iTargetElk)
ENDPROC


/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    
FUNC BOOL SETUP_MISSION_STAGE(HUNTSTATE Stage, BOOL bJumped = FALSE)
	SWITCH Stage
		CASE HS_SETUP
			IF bJumped
				bJumped = FALSE
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_DEER_MODEL, <<0,0,0>>)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CHALLENGES, <<0,0,0>>)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SOUND, <<0,0,0>>)
				AND SETUP_STAGE_REQUIREMENTS(RQ_ANIMS, <<0,0,0>>)
				AND SETUP_STAGE_REQUIREMENTS(RQ_TEXTURES, <<0,0,0>>)
					CPRINTLN(DEBUG_MISSION,"All Assets loaded")
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Updates the player's selection on the results screen.
/// RETURNS:
///    TRUE when we're done processing. FALSE to continue processing.
FUNC BOOL HAS_PLAYER_CHOSEN_RESULTS(BOOL bDisableRetry = FALSE)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	BOOL bDisplayResults
	BOOL bRetVal = PROCESS_HUNT_RESULTS_INPUT(bIsContinuing, bDisplayResults, bDisableRetry, bPredictionSetup)
	
	IF IS_SCREEN_FADED_IN()
		// Pad instructions
		IF NOT bIsWaitingToQuit AND (GET_GAME_TIMER() > iInputDelayTime) AND (iInputDelayTime != 0)
			UPDATE_SIMPLE_USE_CONTEXT(resultsInstructions)
		ENDIF
		
		// Okay, so if the leaderboard is done predictions, re-create the prompts to allow for social club.
		IF IS_PLAYER_ONLINE()
			// If the palyer wasn't online for the prediction setup, do that now.
			IF NOT Player_Hunt_Data.bOnlineForPrediction 
				Player_Hunt_Data.bOnlineForPrediction = TRUE
				bPredictionSetup = FALSE
				READ_HUNTING_SOCIAL_CLUB_LEADERBOARD()
				
				// Redo our prompts, as there's no leaderboard access for a bit.
				INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
				ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT", FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
				IF NOT bDisableRetry
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY", FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
				ENDIF
				SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
			ENDIF
			
			IF NOT bPredictionSetup
				IF scLB_rank_predict.bFinishedRead
				AND NOT scLB_rank_predict.bFinishedWrite
					HUNTING_WRITE_TO_LEADERBOARD()
					scLB_rank_predict.bFinishedWrite = TRUE
				ENDIF
				IF GET_RANK_PREDICTION_DETAILS(Player_Hunt_Data.huntLB_control)
					CDEBUG1LN(DEBUG_HUNTING, "HAS_PLAYER_CHOSEN_RESULTS - Prediction done! Adding leaderboard button!")
					
					// Resetup controls				
					INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					IF NOT bDisableRetry
						ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					ENDIF
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HLEADR",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
					SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
					bPredictionSetup = TRUE		
					sclb_useRankPrediction = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF bDisplayLeaderboard
			IF IS_PLAYER_ONLINE()
				DISPLAY_HUNTING_SOCIAL_CLUB_LEADERBOARD()
			ELSE
				IF DO_SIGNED_OUT_WARNING(iLBDWarning)
					iLBDWarning = 0
					bDisplayLeaderboard = FALSE
					
					// Resetup controls				
					INIT_SIMPLE_USE_CONTEXT(resultsInstructions, FALSE, FALSE, TRUE, TRUE)
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HCONT",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					IF NOT bDisableRetry
						ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HRETRY",	FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
					ENDIF
					ADD_SIMPLE_USE_CONTEXT_INPUT(resultsInstructions, "HLEADR",		FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
					SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(resultsInstructions)
				ENDIF
				
				HUNTING_SET_QUITTING_TIMER(0)
				bIsWaitingToQuit 		= FALSE
				bIsContinuing 			= FALSE
			
				// Can't return TRUE until we're done with the warning screen
				RETURN FALSE
			ENDIF
		ELIF NOT bIsWaitingToQuit AND bDisplayResults
			IF Player_Hunt_Data.bNeedLoserSound
				PLAY_SOUND_FRONTEND(-1,"LOSER", "HUD_AWARDS")
				Player_Hunt_Data.bNeedLoserSound = FALSE
			ENDIF
			
			DRAW_HUNT_RESULTS(Player_Hunt_Data.challengeScreen)
		ENDIF
	ENDIF
	
	// Keep going till we either push the button to Launch a challenge or Exit.
	RETURN bRetVal
ENDFUNC

FUNC BOOL HAS_PLAYER_CANCELLED()
	IF NOT bShowingCancelScreen
		IF IS_PHONE_ONSCREEN() 
		OR IS_BROWSER_OPEN()
		OR IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
		OR IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			PRINTLN("Blocking the input for quitting hunting!")
			iQuitMenuDelay = GET_GAME_TIMER() + 1000
			RETURN FALSE
		ENDIF
		
		IF GET_GAME_TIMER() > iQuitMenuDelay
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
			IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				// Disable the cinematic cam.
				SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				
				DISABLE_CELLPHONE(TRUE)
				HT_MENU_SFX_PLAY_NAV_BACK()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_GAME_PAUSED(TRUE)

				bPauseTimer = TRUE
				bShowingCancelScreen = TRUE
			ENDIF
		ENDIF
	ELSE
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SET_WARNING_MESSAGE_WITH_HEADER("HT_QTITLE", "HT_QUIT", (FE_WARNING_YES | FE_WARNING_NO))
		
		// Clear any help text while the cancel session screen is displaying.
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AHT_CALL")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AHT_CANC1")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("AHT_CANC2")
			CLEAR_HELP()
		ENDIF

		// Cancel the session
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			INIT_RESULT_SCREEN_BUTTONS()
			INIT_HUNTING_RESULTS_SCREEN(Player_Hunt_Data.challengeScreen)
			SET_GAME_PAUSED(FALSE)

			Player_Hunt_Data.sFailReason = "HCHEND" // You ended the hunting session.
			eFinishState = FS_CANCELLED
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			RETURN TRUE
			
		// Choose not to cancel...
		ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_GAME_PAUSED(FALSE)
			
			//PLAY_SOUND_FRONTEND(-1, "NO", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			bPauseTimer = FALSE
			bShowingCancelScreen = FALSE
			DISABLE_CELLPHONE(FALSE)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_FAILED_MISSION()
	IF DID_PLAYER_LEAVE_AREA()
		CPRINTLN(DEBUG_MISSION,"MISSION FAILED - PLAYER LEFT AREA")
		Player_Hunt_Data.sFailReason = "HLEFT"
		eFinishState = FS_FAILED_LEFT 
		THEFEED_FLUSH_QUEUE()
		RETURN TRUE
	ENDIF
	
	// Shooting an elk from outside the hunt area?
	IF bPlayerKillElkIncorrectly
		Player_Hunt_Data.sFailReason = "HWRONG"
		eFinishState = FS_FAILED_WRONG_WEAPON
		THEFEED_FLUSH_QUEUE()
		RETURN TRUE
	ENDIF
	
	TEXT_LABEL txtMsg
	INT iHour = GET_CLOCK_HOURS()
	IF (iHour > 19 OR iHour < 5)  AND bIsAmbient 
		IF (iHour > 20 OR iHour < 5)
			IF NOT bSentFailText
				txtMsg = GET_HUNTING_TEXT_VARIATION("LATE_QUIT", 5)
				SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_HUNTER, txtMsg, TXTMSG_UNLOCKED)
				bSentFailText = TRUE
			ELSE
				IF NOT IS_TIMER_STARTED(Player_Hunt_Data.textTimer)
					START_TIMER_NOW(Player_Hunt_Data.textTimer)
				ENDIF
				IF TIMER_DO_ONCE_WHEN_READY(Player_Hunt_Data.textTimer, 5.0)
					CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_TREVOR)
					Player_Hunt_Data.sFailReason = "HNOTIME"
					eFinishState = FS_FAILED_TIME
					THEFEED_FLUSH_QUEUE()
					RETURN TRUE
				ENDIF
			ENDIF
		ELIF NOT bSentWarnText
			txtMsg = GET_HUNTING_TEXT_VARIATION("LATE", 5)
			SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_HUNTER, txtMsg, TXTMSG_UNLOCKED)
			bSentWarnText = TRUE
			CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER(CHAR_TREVOR)
		ENDIF
	ENDIF

	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		Player_Hunt_Data.sFailReason = "REASON_WANTED"
		eFinishState = FS_FAILED_WANTED
		THEFEED_FLUSH_QUEUE()
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initialises variables for all modes and sets 
///    up how many elk allowed to spawn at once, or kills required for the seperate challenges.
///    Gets used to re-set the game if the challenge is cancelled, failed
///    or re-tried.
PROC INIT_VARS_FOR_THIS_MODE()
	bAllowCall 				= TRUE
	bMissionEnded 			= FALSE
	bShowingCancelScreen 	= FALSE
	bPauseTimer 			= FALSE
	bGotPauseTime			= FALSE
	bPlayerCalling			= FALSE
	bSentFailText			= FALSE
	bSentWarnText			= FALSE
	iEndTime = 0
	
	bShownBronzeToast 		= FALSE
	bShownSilverToast 		= FALSE
	bShownGoldToast 		= FALSE
	
	//bDisplayResults = TRUE
	bDisplayLeaderboard = FALSE
	
	// Set our first bleat initiator sometime in the future.
	iBleatInitiateTime = GET_GAME_TIMER() + 5000

	Player_Hunt_Data.iScore 		= 0
	Player_Hunt_Data.iKillCount 	= 0
	Player_Hunt_Data.iMoney			= 0
	Player_Hunt_Data.bGivenMoney 	= FALSE
		
	INT i = 0
	REPEAT NUM_HIT_LOCATIONS i 
		Player_Hunt_Data.iHitsOnLocation[i] = 0
	ENDREPEAT

	Player_Hunt_Data.iPhotosSent = 0
	Player_Hunt_Data.iCougarKills = 0
	Player_Hunt_Data.iBoarKills = 0
	Player_Hunt_Data.iCoyoteKills = 0
	Player_Hunt_Data.bGoldAchieved = FALSE
	IF Player_Hunt_Data.todHuntStarted = INVALID_TIMEOFDAY
		Player_Hunt_Data.todHuntStarted = GET_CURRENT_TIMEOFDAY()
	ENDIF

	IF eFinishState <> FS_NULL
		eFinishState = FS_NULL
	ENDIF
	
	INITIALIZE_CHALLENGES()

	REPEAT COUNT_OF(Player_Hunt_Data.iKillTimes) i
		Player_Hunt_Data.iKillTimes[i] = 0
	ENDREPEAT
	
	// Clean up the itemsets for photos and corpses.
	IF IS_ITEMSET_VALID(Player_Hunt_Data.setCorpses)
		CLEAN_ITEMSET(Player_Hunt_Data.setCorpses)
	ELSE
		Player_Hunt_Data.setCorpses = CREATE_ITEMSET(FALSE)
	ENDIF
	IF IS_ITEMSET_VALID(Player_Hunt_Data.setPhoto)
		CLEAN_ITEMSET(Player_Hunt_Data.setPhoto)
	ELSE
		Player_Hunt_Data.setPhoto = CREATE_ITEMSET(FALSE)
	ENDIF
	
	// Ensure a clean hunting ground.
	REMOVE_DECALS_IN_RANGE(mAreaCheck1.vCentre, 800.0)
	CLEAR_AREA_OF_PEDS(mAreaCheck1.vCentre, 800.0)
ENDPROC


// Checks the players kill count and time taken. 
// If the values are better than bronze, or better than previously, record them.
PROC GRAB_STATS()
	BOOL bNeedAutosave = FALSE
	BOOL bAwesomeHunt = FALSE
	
	CPRINTLN(DEBUG_MISSION, "Grabbing stats... for Free Mode")
	IF Player_Hunt_Data.iScore > g_SavedGlobals.sAmbient.iHighScoreFreeMode
		CPRINTLN(DEBUG_MISSION, "Kills total was higher that prev record")
		g_SavedGlobals.sAmbient.iHighScoreFreeMode = Player_Hunt_Data.iScore
		Player_Hunt_Data.iHighScore = g_SavedGlobals.sAmbient.iHighScoreFreeMode

		bNeedAutosave = TRUE
	ENDIF
	
	// Next time we call Cletus, we will trigger this.
	IF (Player_Hunt_Data.iScore > Player_Hunt_Data.iScoreForGold)
		bAwesomeHunt = TRUE
	ENDIF
	
	HUNTING_CHALLENGE challengeIndex
	REPEAT COUNT_OF(HUNTING_CHALLENGE) challengeIndex
		IF ENUM_TO_INT(Player_Hunt_Data.challengeData[challengeIndex].hcRank) > g_SavedGlobals.sAmbient.iChallengeRankFreeMode[challengeIndex]
			CPRINTLN(DEBUG_MISSION, "Challenge ", challengeIndex, " was higher than previous. Storing it.")
			g_SavedGlobals.sAmbient.iChallengeRankFreeMode[challengeIndex] = ENUM_TO_INT(Player_Hunt_Data.challengeData[challengeIndex].hcRank)
			bNeedAutosave = TRUE
		ENDIF
	ENDREPEAT
	
	// Log that we've recently hunted, only if we've managed to kill something.
	// Conversations all have to do with Cletus getting stuff from Trevor for hunting.
	IF (Player_Hunt_Data.iScore > 0)
		SET_TIME_LAST_HUNTED(bAwesomeHunt)
	ENDIF
	
	// We do this so that we only trigger one autosave, rather than 1 for the hich score, then 1 for each challenge.
	IF bNeedAutosave
		MAKE_AUTOSAVE_REQUEST()
	ENDIF
	
	// Process completion percentages.
	IF g_SavedGlobals.sAmbient.iHighScoreFreeMode >= Player_Hunt_Data.iScoreForBronze
		CPRINTLN(DEBUG_MISSION,"REGISTERING SCRIPT IN COMPLETION PERCENTAGE TOTAL")
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_OJ_HUNTCHALL)
	ELSE
		CPRINTLN(DEBUG_MISSION,"CONDITIONS FOR REGISTERING COMPLETION % HAVE NOT YET BEEN MET")
	ENDIF
ENDPROC


/// PURPOSE:
///    Sets the player back to the start point of the challenge
///    Takes him out of stealth mode and resets the camera behind him.
PROC RESET_PLAYER_TO_SPAWN()
	CPRINTLN(DEBUG_MISSION," RESETTING PLAYER TO SPAWN ")
	PED_INDEX pedPlayer = PLAYER_PED_ID()
	
	// Warp the player back to the start point
	// and set his heading
	SET_ENTITY_COORDS(pedPlayer, vPlayerStartPos)
	SET_ENTITY_HEADING(pedPlayer, fPlayerStartDir)
	
	// Clear any decals on the player.
	CLEAR_PED_BLOOD_DAMAGE(pedPlayer)
	RESET_PED_VISIBLE_DAMAGE(pedPlayer)
	CLEAR_PED_WETNESS(pedPlayer)

	// Stand him up
	IF GET_PED_STEALTH_MOVEMENT(pedPlayer) 
		SET_PED_STEALTH_MOVEMENT(pedPlayer, FALSE)
	ENDIF
	
	// reset camera behind player
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
ENDPROC

/// PURPOSE:
///    Destroys the main camera.
PROC DESTROY_MAIN_CAM()
	IF DOES_CAM_EXIST(camScene)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camScene)
	ENDIF
ENDPROC


/// PURPOSE:
///    When hunting starts, we kick off a timer that's meant to track how long it takes the player to achieve gold medal status.
///    When the player gets there, we pause the timer so that it can be read for leaderboards.
PROC UPDATE_GOLD_MEDAL_TIMER()
	IF NOT Player_Hunt_Data.bGoldAchieved
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold
			// We're done tracking!
			PAUSE_TIMER(Player_Hunt_Data.tTimeToGold)
			Player_Hunt_Data.bGoldAchieved = TRUE
			
			PRINTLN("======================= GOLD ACHIEVED =======================")
			
			#IF IS_DEBUG_BUILD
				INT iTotalMS = ROUND(GET_TIMER_IN_SECONDS(Player_Hunt_Data.tTimeToGold) * 1000.0)
				INT iHours = iTotalMS / (1000 * 60 * 60)
				iTotalMS -= iHours * 60 * 60 * 1000
				
				INT iMins = iTotalMS / (1000 * 60)
				iTotalMS -= iMins * 60 * 1000
				
				INT iSecs = iTotalMS / 1000
				iTotalMS -= iSecs * 1000
				
				INT iMS = iTotalMS
				
				PRINTLN("UPDATE_GOLD_MEDAL_TIMER - Player achieved gold status! Time was ", iHours, ":", iMins, ":", iSecs, ":", iMS)
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Handles medal splashes and hunting timers.
PROC UPDATE_HUNTING_MEDALS()
	IF NOT bShownBronzeToast
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForBronze
			BEGIN_TEXT_COMMAND_THEFEED_POST("HUNT")
			END_TEXT_COMMAND_THEFEED_POST_AWARD("Hunting", "Hunting_Bronze_128", 0, HUD_COLOUR_BRONZE, "HUD_MED_UNLKED")
			PLAY_SOUND_FRONTEND(-1, "MEDAL_BRONZE", "HUD_AWARDS")
			bShownBronzeToast = TRUE
		ENDIF
	ELIF NOT bShownSilverToast
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForSilver
			BEGIN_TEXT_COMMAND_THEFEED_POST("HUNT")
			END_TEXT_COMMAND_THEFEED_POST_AWARD("Hunting", "Hunting_Silver_128", 0, HUD_COLOUR_SILVER, "HUD_MED_UNLKED")
			PLAY_SOUND_FRONTEND(-1, "MEDAL_SILVER", "HUD_AWARDS")
			bShownSilverToast = TRUE
		ENDIF
	ELIF NOT bShownGoldToast
		IF Player_Hunt_Data.iScore >= Player_Hunt_Data.iScoreForGold
			BEGIN_TEXT_COMMAND_THEFEED_POST("HUNT")
			END_TEXT_COMMAND_THEFEED_POST_AWARD("Hunting", "Hunting_Gold_128", 0, HUD_COLOUR_GOLD, "HUD_MED_UNLKED")
			PLAY_SOUND_FRONTEND(-1, "MEDAL_GOLD", "HUD_AWARDS")
			bShownGoldToast = TRUE
		ENDIF
	ENDIF

	// Always process the gold medal timer.
	UPDATE_GOLD_MEDAL_TIMER()
ENDPROC

/// PURPOSE:
///    Grabs all peds in the area, and attempts to use them in hunting. If they're not onscreen and they're far away, simply removes them.
PROC HUNTING_INTEGRATE_LOCAL_WILDLIFE()
	INT iAnimalsAdded = 0
	INT iElkAdded = 0

	PED_INDEX pedNearby[30]
	
	INT iFound
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		iFound = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedNearby)
	ENDIF
	
	IF (iFound != 0)
		INT iPed
		REPEAT iFound iPed
			IF NOT IS_PED_INJURED(pedNearby[iPed])
				// Okay, now go through all of those peds, and add the ones that are at least visible.
				MODEL_NAMES modelPed = GET_ENTITY_MODEL(pedNearby[iPed])
				IF (modelPed = A_C_COYOTE OR modelPed = A_C_BOAR) 
					IF (iAnimalsAdded < MAX_RANDOM_ANIMALS)
						// If it's visible, it can be an animal.
						VECTOR vPos = GET_ENTITY_COORDS(pedNearby[iPed], FALSE)
						IF IS_SPHERE_VISIBLE(vPos, 4.0)
							SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
							mAnimal[iAnimalsAdded].mPed = pedNearby[iPed]
							SETUP_ANIMAL(mAnimal[iAnimalsAdded], AS_IDLE, TRUE)
							iAnimalsAdded++
						ELSE
							SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
							DELETE_PED(pedNearby[iPed])
						ENDIF
					ELSE
						// Don't delete, simply take them, make them flee the player, and release.
						SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
						SET_PED_KEEP_TASK(pedNearby[iPed], TRUE)
						TASK_SMART_FLEE_PED(pedNearby[iPed], PLAYER_PED_ID(), 1000.0, -1)
						SET_PED_AS_NO_LONGER_NEEDED(pedNearby[iPed])
					ENDIF

				ELIF (modelPed = A_C_DEER) 
					IF (iElkAdded < MAX_ELKS)
						VECTOR vPos = GET_ENTITY_COORDS(pedNearby[iPed], FALSE)
						IF IS_SPHERE_VISIBLE(vPos, 4.0) 
							SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
							mElk[iElkAdded].mPed = pedNearby[iPed]
							SETUP_ELK(mElk[iElkAdded], ES_PAUSE_THEN_GRAZE)
							mElk[iElkAdded].iCurNode = 0
							SET_ELK_SPOOKED(iElkAdded, FALSE)
							
							// Give it a further 7.5 second delay, because from here, the intro takes another 7.5 seconds.
							mElk[iElkAdded].iWaitTime = GET_GAME_TIMER() + 7500 + (iElkAdded * 666)
							
							// Need to determine if it's a doe or not...
							INT iVariant = GET_PED_DRAWABLE_VARIATION(pedNearby[iPed], PED_COMP_SPECIAL)
							IF (iVariant = 0)
								// It's a doe!
								DECOR_SET_BOOL(pedNearby[iPed], "doe_elk", TRUE)
								iDoeInPlay++
							ENDIF
							iElksInPlay++
							iElkAdded++
						ELSE
							SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
							DELETE_PED(pedNearby[iPed])
						ENDIF
					ELSE
						// Don't delete, simply take them, make them flee the player, and release.
						SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
						SET_PED_KEEP_TASK(pedNearby[iPed], TRUE)
						TASK_SMART_FLEE_PED(pedNearby[iPed], PLAYER_PED_ID(), 1000.0, -1)
						SET_PED_AS_NO_LONGER_NEEDED(pedNearby[iPed])
					ENDIF
					
				ELIF (modelPed = A_C_MTLION)
					// Don't delete, simply take them, make them flee the player, and release.
					SET_ENTITY_AS_MISSION_ENTITY(pedNearby[iPed], TRUE, TRUE)
					SET_PED_KEEP_TASK(pedNearby[iPed], TRUE)
					TASK_SMART_FLEE_PED(pedNearby[iPed], PLAYER_PED_ID(), 1000.0, -1)
					SET_PED_AS_NO_LONGER_NEEDED(pedNearby[iPed])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


/// PURPOSE:
///    PRocesses the title for hunting when the player first start
FUNC BOOL MANAGE_HUNTING_TITLE()
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		SWITCH iMissionTitleState
			CASE 0
				MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_HUNTING))
				iMissionTitleState = 1
			BREAK
			
			CASE 1
				TEXT_LABEL_23 txtEmpty
				UPDATE_MISSION_NAME_DISPLAYING(txtEmpty)
				
				IF (g_iFlowTimeBeganDisplayMissionTitle >= GET_GAME_TIMER())
					// Don't notify the player about challenges in the opening help unless they've been unlocked.
					IF NOT g_savedGlobals.sAmbient.bChallengeHelpDisplayed
						PRINT_HELP("AHT_WLCOME")
					ELSE
						PRINT_HELP("AHT_WLCOME_CHAL")
					ENDIF
					bHuntTitleReady = TRUE
				ENDIF
				
				IF (g_eMissionTitleState = MTS_DONE)
					// All done!
					MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
					iMissionTitleState = 2
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ==============
// HUNTING STATES
// ============== 

PROC SETUP_INTRO_SCENE(HUNTING_LAUNCH_STRUCT & launchData)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
				
	SWITCH eStage
		CASE SS_INIT
			PRINTLN("SS_INIT")
					
			// Get the truck.
			IF NOT DOES_ENTITY_EXIST(vehTruck)
				// Handle the vehicle.
				vehTruck = launchData.vehTrailer
				IF DOES_ENTITY_EXIST(vehTruck)
				AND NOT IS_ENTITY_DEAD(vehTruck)
					// Awesome. Trigger the intro.
					SET_ENTITY_AS_MISSION_ENTITY(vehTruck, TRUE, TRUE)
					bTruckGrabbedAtStart = TRUE
				ELSE
					// No journey? Uh oh...
					//IF SETUP_MISSION_STAGE(HS_SETUP)
					//	NEXT_STAGE()
					//	EXIT
					//ENDIF
					// Trigger streaming here... this'll get us started at least.
					SETUP_MISSION_STAGE(HS_SETUP)
					EXIT
				ENDIF
				
			ELSE
				IF NOT IS_ENTITY_DEAD(vehTruck)
					// Okay, we've grabbed the truck, make sure we're ready to process the intro.
					// Wait here for the printing stuff to be loaded.
					//IF SETUP_STAGE_REQUIREMENTS(RQ_INTRO, <<0,0,0>>) AND bHuntTitleReady
						bHuntTitleReady = bHuntTitleReady
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						
						STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 25.0)
						STOP_ENTITY_FIRE(vehTruck)
				
						VECTOR vSceneCoord
						vSceneCoord = GET_ENTITY_COORDS(vehTruck, FALSE)
						VECTOR vSceneOrient
						vSceneOrient = GET_ENTITY_ROTATION(vehTruck)
						iSceneID = CREATE_SYNCHRONIZED_SCENE(vSceneCoord, vSceneOrient)
						
						camScene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
						PLAY_SYNCHRONIZED_CAM_ANIM(camScene, iSceneID, "_Trevor_cam", "oddjobs@hunterIntro")
						SET_CAM_ACTIVE(camScene, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1703.123, 4665.463, 21.2>>, <<-1704.077, 4660.555, 25.4>>,
								7.0, <<-1710.8226, 4656.9639, 21.8008>>, 82.9948)
						
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@hunterIntro", "_Trevor", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(vehTruck, iSceneID, "_Trevor_journey", "oddjobs@hunterIntro", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
											
						eStage = SS_POST_INIT
					//ENDIF
				ELSE
					// Journey is dead... Just move on.
					NEXT_STAGE()
					EXIT
				ENDIF
			ENDIF
			
			// Try to get that quad...
			IF NOT DOES_ENTITY_EXIST(Player_Hunt_Data.vehQuad)
				Player_Hunt_Data.vehQuad = launchData.vehQuad
				
				IF DOES_ENTITY_EXIST(Player_Hunt_Data.vehQuad)
				AND NOT IS_ENTITY_DEAD(Player_Hunt_Data.vehQuad)
					SET_ENTITY_AS_MISSION_ENTITY(Player_Hunt_Data.vehQuad, TRUE, TRUE)
					SET_VEHICLE_UNDRIVEABLE(Player_Hunt_Data.vehQuad, FALSE)
					
					IF NOT IS_SPHERE_VISIBLE(<<-1707.4337, 4666.5625, 22.1095>>, 3.0)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(Player_Hunt_Data.vehQuad, <<-1707.4337, 4666.5625, 22.1095>>) > 5.0					CLEAR_AREA_OF_VEHICLES(<<-1707.4337, 4666.5625, 22.1095>>, 2.0)
						SET_ENTITY_COORDS(Player_Hunt_Data.vehQuad, <<-1707.4337, 4666.5625, 22.1095>>)
						SET_ENTITY_HEADING(Player_Hunt_Data.vehQuad, 323.2491)
						SET_ENTITY_HEALTH(Player_Hunt_Data.vehQuad, GET_ENTITY_MAX_HEALTH(Player_Hunt_Data.vehQuad))
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_POST_INIT
			PRINTLN("SS_POST_INIT")
			
			// Here, we load everything we'll need for the game. Menus and all.
			IF SETUP_MISSION_STAGE(eHuntState)	
				LOAD_RANDOM_ANIMAL_MODELS()
				LOAD_ELK_MODEL()
	
				// Need to grab and use as many local animals as we can.
				HUNTING_INTEGRATE_LOCAL_WILDLIFE()
				
				CLEAR_RANK_REDICTION_DETAILS()
				CLEANUP_SOCIAL_CLUB_LEADERBOARD(Player_Hunt_Data.huntLB_control)
		
				VECTOR vMin, vMax
				vMin = mAreaCheck1.vCentre - <<700.0, 700.0, 200.0>>
				vMax = mAreaCheck1.vCentre + <<700.0, 700.0, 200.0>>
				mBlockArea = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
				SET_PED_NON_CREATION_AREA(vMin, vMax)
				
				REMOVE_TEXT_MESSAGE_FEED_ENTRY("CHI2_U")

				eStage = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			PRINTLN("SS_ACTIVE")
			// Waiting until the right time to trigger Trevor's clothing change...
			IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.99
				IS_ENTITY_DEAD(vehTruck)	// Eat an assert.
				
				// Set Trevor into hunting outfit
				STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_HUNTING, FALSE)

				VECTOR vSceneCoord
				vSceneCoord = GET_ENTITY_COORDS(vehTruck, FALSE)
				VECTOR vSceneOrient
				vSceneOrient = GET_ENTITY_ROTATION(vehTruck)
				iSceneID = CREATE_SYNCHRONIZED_SCENE(vSceneCoord, vSceneOrient)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID, FALSE)
		
				camScene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(camScene, iSceneID, "_Trevor_cam", "oddjobs@hunterOutro")
				SET_CAM_ACTIVE(camScene, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@hunterOutro", "_Trevor", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(vehTruck, iSceneID, "_Trevor_journey", "oddjobs@hunterOutro", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			
				eStage = SS_TRANSIT
			ENDIF
		BREAK
		
		CASE SS_TRANSIT
			PRINTLN("SS_TRANSIT")
			IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.99
				eStage = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			PRINTLN("SS_CLEANUP")

			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			
			//CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(vehTruck)
			AND NOT IS_ENTITY_DEAD(vehTruck)
				STOP_SYNCHRONIZED_ENTITY_ANIM(vehTruck, INSTANT_BLEND_IN, TRUE)
			ENDIF

			// All done!
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			CLEAR_PRINTS()
			eStage = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

PROC HUNTING_MONITOR_CHALLENGES_HELP()
	IF NOT g_SavedGlobals.sAmbient.bChallengeHelpDisplayed
	AND NOT IS_HELP_MESSAGE_ON_SCREEN("")
		IF Player_Hunt_Data.challengeData[HC_MASTER_HUNTER].hcRank != HCR_LOCKED
		OR Player_Hunt_Data.challengeData[HC_PHOTOJOURNALIST].hcRank != HCR_LOCKED
		OR Player_Hunt_Data.challengeData[HC_WEAPONS_MASTER].hcRank != HCR_LOCKED
			g_SavedGlobals.sAmbient.bChallengeHelpDisplayed = TRUE
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs updates for all huntign wildlife.
PROC HUNTING_UPDATE_WILDLIFE()
	INT idx
	
	// Update animal death.
	REPEAT MAX_RANDOM_ANIMALS idx
		HAS_PLAYER_KILLED_RANDOM_ANIMAL(mAnimal[idx])
	ENDREPEAT
	
	// Update cougar death.
	REPEAT MAX_COUGARS idx
		HAS_PLAYER_KILLED_RANDOM_ANIMAL(mCougar[idx])
	ENDREPEAT
	
	// Update all elk.
	UPDATE_HUNTING_ELK(DEFAULT, iMissionTitleState >= 2)
ENDPROC


PROC HUNT()
	SWITCH eStage
		CASE SS_INIT
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			CPRINTLN(DEBUG_MISSION, "HUNTING START!!!!!!!!!!!!!")
			
			//  *** MUST *** do this first or we won't know how mant elk to spawn
			// Also initializes the end time for the timed challenges.
			INIT_VARS_FOR_THIS_MODE()
			SPAWN_FIRST_ELK()
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bPlayerKillElkIncorrectly = FALSE
			ALLOW_SONAR_BLIPS(TRUE)
			// Disable knife kicking mode so that knife kills work.
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("AR_knife_low_kick_far"), FALSE)
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("AR_knife_low_kick_close"), FALSE)
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_close"), FALSE)
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_far"), FALSE)
			
			// Restart the timer to gold.
			RESTART_TIMER_NOW(Player_Hunt_Data.tTimeToGold)
			
			SETTIMERA(0)
			bGaveRifle = FALSE
			
			// Restart time spent hunting timer.
			RESTART_TIMER_NOW(Player_Hunt_Data.tTimeSpentHunting)
			
			eStage = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			DIAL_DOWN_TRAFFIC()
			UPDATE_HUNTING_MEDALS()
			
			IF NOT bGaveRifle
				IF TIMERA() > 500
					// Give him the gun.
					SETUP_STAGE_REQUIREMENTS(RQ_RIFLE, <<0,0,0>>, FALSE)
					bGaveRifle = TRUE
				ENDIF
			ENDIF
			
			// Make sure the palyer isn't attacking his ride...
			IF bTruckGrabbedAtStart
				IF IS_ENTITY_DEAD(vehTruck)
					SET_MAX_WANTED_LEVEL(1)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1, FALSE)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
			ENDIF

			// Have we passed or failed?
			IF NOT bMissionEnded
				FORCE_SONAR_BLIPS_THIS_FRAME()
				
				
				HUNTING_UPDATE_WILDLIFE()
				HANDLE_NON_MINIGAME_KILLS()
				CORPSE_MANAGER_UPDATE()
				
				UPDATE_HUNTING_CHALLENGES(Player_Hunt_Data)				
				MONITER_PLAYER_AIMING_SOUNDS()
				
				IF WOULD_SCALEFORM_SHARD_MESSAGE_BE_VISIBLE(Player_Hunt_Data.splashUI)
					IF IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
						CANCEL_TIMER(Player_Hunt_Data.splashUI.movieTimer)
					ELSE
						UPDATE_SHARD_BIG_MESSAGE(Player_Hunt_Data.splashUI)
					ENDIF
				ENDIF
				
				IF HAS_PLAYER_FAILED_MISSION()
					CPRINTLN(DEBUG_MISSION,"MISSION FAILED")
					
					// Pause the timer
					iMissionEndTime = GET_GAME_TIMER() + 2000
				
					IF eFinishState = FS_FAILED_LEFT
						iMissionEndTime = GET_GAME_TIMER() + 5000
						PRINT_HELP("HLEFT1", 10000) // Hunting abandoned because you left the area.
						CLEAR_PRINTS()
					ELIF eFinishState = FS_FAILED_WANTED
						CLEAR_PRINTS()
						PRINT_HELP("HWANTED", 10000) // Hunting abandoned because you left the area.
					ELSE
						INIT_RESULT_SCREEN_BUTTONS()
					ENDIF
					
					//IF IS_SCREEN_FADED_OUT()
						Player_Hunt_data.eCorpseState = CS_UPDATE
						Player_Hunt_data.bNearCorpse = FALSE
						
						THEFEED_FLUSH_QUEUE()
						DELETE_ALL_HUNTING_TEXT_MESSAGES()
					//ENDIF
					bMissionEnded = TRUE
										
				// Have we cancelled? This should be immediate - no delay when cancelling...
				ELIF HAS_PLAYER_CANCELLED()
					CPRINTLN(DEBUG_MISSION,"MISSION ENDED - CANCELLED")
					RESTART_TIMER_NOW(tCutsceneTimer)
					DO_SCREEN_FADE_OUT(0)
					eStage = SS_CLEANUP				
				ENDIF
			ELSE
				// Keep showing the hud, with the timer paused if applicable.
				HUNTING_UPDATE_WILDLIFE()

				IF GET_GAME_TIMER() > iMissionEndTime
					// If the player has left for any reason other than going wanted, transition with a fade.
					// Reason being that we don't want to punish the player by doing fancy transitions for being wanted.
					IF (eFinishState != FS_FAILED_WANTED)
					AND (eFinishState != FS_FAILED_LEFT)
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					
					CPRINTLN(DEBUG_MISSION, "POP THE RESULTS SCREEN AFTER A SMALL DELAY")
					RESTART_TIMER_NOW(tCutsceneTimer)
					
					eStage = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_TRANSIT
			IF TIMER_DO_WHEN_READY(tCutsceneTimer, 1.0)
				eStage = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION," *** CLEAN UP HUNTING STATE *** ")
			
			// Stop the sniper aiming audio from playing on the results screen
			// if the player was aiming when hunting ended
			STOP_SNIPE_AUDIO()
			
			GRAB_STATS()
			CLEAR_PRINTS()
			REMOVE_ALL_ELK()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL HUNTING_RUN_LEFT_AREA_CUTSCENE()
	SWITCH iQuitSceneState
		CASE 0
			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, FALSE)
			iQuitSceneState = 1
		BREAK
		
		CASE 1
			IF bQuittingMG
	            ANIMPOSTFX_STOP("MinigameTransitionIn") 
	            ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
				iQuitSceneState = 2
			ENDIF
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HUNTING_RUN_QUITTING_CUTSCENE()
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 texDisplay
	FLOAT fDisplayAtX = 0.5
	FLOAT fDisplayAtY = 0.5
	FLOAT fDisplayAdd = 0.0125
	
	texDisplay = "iQuitSceneState = "
	texDisplay += iQuitSceneState
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneID)
		texDisplay = "scene phase = "
		texDisplay += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iSceneID))
		DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
		fDisplayAtY += fDisplayAdd
	ENDIF
	
	texDisplay = "iQuittingTimer = "
	texDisplay += HUNTING_GET_QUITTING_TIMER()
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	
	texDisplay = "GET_GAME_TIMER() = "
	texDisplay += GET_GAME_TIMER()
	DRAW_DEBUG_TEXT_2D(texDisplay, (<< fDisplayAtX, fDisplayAtY, 0.0 >>))
	fDisplayAtY += fDisplayAdd
	#ENDIF
	
	SWITCH iQuitSceneState
		CASE 0
			CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Setting up quitting cutscene!")
			
			IF DOES_CAM_EXIST(camScene)
				DESTROY_CAM(camScene)
			ENDIF
			IF DOES_CAM_EXIST(camSky)
				DESTROY_CAM(camSky)
			ENDIF

			ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, FALSE) 
			camSky = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-1705.3109, 4664.6797, 38.8278>>, <<10.2934, -1.8316, -85.2144>>, 42.79, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			streamVolResults = STREAMVOL_CREATE_FRUSTUM(<<-1708.5167, 4663.8687, 44.9329>>, <<6.5082, -0.0, -85.7712>>, 200.0, FLAG_MAPDATA)
			
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1703.123, 4665.463, 21.2>>, <<-1704.077, 4660.555, 25.4>>,
								7.0, <<-1710.8226, 4656.9639, 21.8008>>, 82.9948)
			
			iQuitSceneState = 1
		BREAK
		
		CASE 1
			IF STREAMVOL_IS_VALID(streamVolResults)
				CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Streaming cutscene view.")
				IF STREAMVOL_HAS_LOADED(streamVolResults)
				OR HUNTING_GET_QUITTING_TIMER() < GET_GAME_TIMER()
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					STREAMVOL_DELETE(streamVolResults)
					
					// Stop the pad from taking input for a couple of seconds, so the results screen
					// doesn't get accidently skipped.
					bGotInputDelayTime = FALSE
					
					iQuitSceneState = 2
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF bQuittingMG
				CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Removing weapons & setting a delay.")
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)

				STOP_SNIPE_AUDIO()
				CLEAR_PRINTS()
				REMOVE_ALL_ELK()
				HUNTING_SET_QUITTING_TIMER(GET_GAME_TIMER() + 500)
				
				iQuitSceneState = 3
			ENDIF
		BREAK
		
		CASE 3
			IF HUNTING_GET_QUITTING_TIMER() < GET_GAME_TIMER()
				CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Triggering scene!")
				IF NOT DOES_ENTITY_EXIST(vehTruck)
				OR IS_ENTITY_DEAD(vehTruck)
					ANIMPOSTFX_STOP("MinigameTransitionIn") 
	           	 	ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
					RETURN TRUE
				ENDIF
				
				// Trigger the outro scene here, and drop the cam down.
				VECTOR vSceneCoord
				vSceneCoord = GET_ENTITY_COORDS(vehTruck, FALSE)
				VECTOR vSceneOrient
				vSceneOrient = GET_ENTITY_ROTATION(vehTruck)
				iSceneID = CREATE_SYNCHRONIZED_SCENE(vSceneCoord, vSceneOrient)	
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID, FALSE)
				
				camScene = CREATE_CAMERA(CAMTYPE_ANIMATED)
				PLAY_SYNCHRONIZED_CAM_ANIM(camScene, iSceneID, "_Trevor_cam", "oddjobs@hunterOutro")
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneID, "oddjobs@hunterOutro", "_Trevor", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(), TRUE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(vehTruck, iSceneID, "_Trevor_journey", "oddjobs@hunterOutro", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
				
				SET_CAM_ACTIVE_WITH_INTERP(camScene, camSky, 700)
				
				PLAY_SOUND_FRONTEND(-1, "QUIT_WHOOSH", "HUD_MINI_GAME_SOUNDSET")
				
				ANIMPOSTFX_STOP("MinigameTransitionIn") 
	            ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)

				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				
				HUNTING_SET_QUITTING_TIMER(GET_GAME_TIMER() + 15000)

				iQuitSceneState = 4
			ENDIF
		BREAK
		
		CASE 4			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneID) > 0.99
			OR (HUNTING_GET_QUITTING_TIMER() < GET_GAME_TIMER())
				CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Cutscene over!")
				//CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(vehTruck)
				AND NOT IS_ENTITY_DEAD(vehTruck)
					STOP_SYNCHRONIZED_ENTITY_ANIM(vehTruck, INSTANT_BLEND_IN, TRUE)
				ENDIF
				
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				RETURN TRUE
				
			ELSE
				CDEBUG1LN(DEBUG_HUNTING, "HUNTING_RUN_QUITTING_CUTSCENE - Cutscene phase at: ", GET_SYNCHRONIZED_SCENE_PHASE(iSceneID))
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

			
PROC RESULTS(BOOL bForDeath = FALSE, BOOL bOutOfTime = FALSE, BOOL bWanted = FALSE, BOOL bLeftArea = FALSE, BOOL bCancelled = FALSE)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	
	SWITCH eStage
		CASE SS_INIT
			IF NOT IS_SCREEN_FADING_OUT()
				// Show the results screen against the appropriate backdrop
				CLEAR_HELP()
				DISABLE_CELLPHONE(TRUE)
				
				INIT_RESULT_SCREEN_BUTTONS(bForDeath | bOutOfTime | bWanted | bLeftArea)
				INIT_HUNTING_RESULTS_SCREEN(Player_Hunt_Data.challengeScreen, bForDeath)
				IF IS_PLAYER_ONLINE()
					READ_HUNTING_SOCIAL_CLUB_LEADERBOARD()
					Player_Hunt_Data.bOnlineForPrediction = TRUE
				ELSE
					Player_Hunt_Data.bOnlineForPrediction = FALSE
				ENDIF
				
				HUNTING_SET_QUITTING_TIMER(GET_GAME_TIMER() + 10000)
				iInputDelayTime = 0
				bGotInputDelayTime = FALSE
				iQuitSceneState = 0
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				PAUSE_CLOCK(TRUE)
				
				// Wanted and out of time apply the same effect.
				IF bWanted
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, FALSE) 
				ENDIF

				eStage = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			// Add money, once.
			IF (Player_Hunt_Data.iMoney != 0) AND NOT Player_Hunt_Data.bGivenMoney
				IF IS_SCREEN_FADED_IN()
					Player_Hunt_Data.bGivenMoney = TRUE
					DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR, BAA_CREDIT, BAAC_HUNTING, Player_Hunt_Data.iMoney, TRUE)
				ENDIF
			ENDIF
			
			// Need to handle the cutscene differently depending on what it's for.
			IF bCancelled OR bOutOfTime
				// Player chose to quit.
				IF HUNTING_RUN_QUITTING_CUTSCENE()
					Mission_Cleanup()
				ENDIF
				
			ELIF bLeftArea
				IF HUNTING_RUN_LEFT_AREA_CUTSCENE()
					Mission_Cleanup()
				ENDIF
			ENDIF

			// Display results, leaderboards, poll on input.
			IF NOT bQuittingMG
				IF HAS_PLAYER_CHOSEN_RESULTS(bForDeath | bOutOfTime | bWanted | bLeftArea)
					//Set endscreen to animate out, wait till done
					ENDSCREEN_START_TRANSITION_OUT(Player_Hunt_Data.challengeScreen)
					bQuittingMG = TRUE
					bWaitingEndScreen = TRUE
				ENDIF
			ENDIF
			
			IF bWaitingEndScreen
				
				IF RENDER_ENDSCREEN(Player_Hunt_Data.challengeScreen)
					PAUSE_CLOCK(FALSE)
				
					IF bForDeath
						CPRINTLN(DEBUG_MISSION, "Player is quitting due to death!")
						DO_SCREEN_FADE_OUT(500)
						Mission_Cleanup()
					ELSE
						IF bIsContinuing
							// Player has chosen continue from the menu.
							CPRINTLN(DEBUG_MISSION, "Player has opted to continue.")
							DO_SCREEN_FADE_OUT(500)

							iInputDelayTime = 0
							HUNTING_SET_QUITTING_TIMER(-1)
							bQuittingMG = FALSE
							
							eStage = SS_TRANSIT
						ELSE
							// The player has opted to cancel from the results menu.
							iInputDelayTime = 0
//							HUNTING_SET_QUITTING_TIMER(-1)
							
							IF bWanted
								ANIMPOSTFX_STOP("MinigameTransitionIn") 
		            			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
								eStage = SS_TRANSIT
							ENDIF
						ENDIF
					ENDIF
					bWaitingEndScreen = FALSE
				ENDIF
			ENDIF
				
		BREAK
		
		CASE SS_TRANSIT
			IF bQuittingMG
				Mission_Cleanup() 
			ELSE
				// We're here due to a replay. Handle that.
				IF IS_SCREEN_FADED_OUT()
					IF (HUNTING_GET_QUITTING_TIMER() = -1)
						// Blitz all random animals and deer.
						STOP_SNIPE_AUDIO()
						CLEANUP_RANDOM_ANIMALS()
						REMOVE_ALL_ELK()
						
						ANIMPOSTFX_STOP("MinigameTransitionIn") 
		            	ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)

						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)

						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						CLEAR_AREA_OF_PEDS(mAreaCheck1.vCentre, 1000.0, TRUE)
						CLEAR_AREA(mAreaCheck1.vCentre, 1000.0, TRUE)
						
						// Restore the time of day before the retry.
						IF (Player_Hunt_Data.todHuntStarted != INVALID_TIMEOFDAY)
							SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(Player_Hunt_Data.todHuntStarted), GET_TIMEOFDAY_MINUTE(Player_Hunt_Data.todHuntStarted), GET_TIMEOFDAY_SECOND(Player_Hunt_Data.todHuntStarted))
						ENDIF
						
						CANCEL_TIMER(Player_Hunt_data.textTimer)
						Player_Hunt_data.eCorpseState = CS_UPDATE
						Player_Hunt_data.bNearCorpse = FALSE
						
						// Setup the LBD for a restart.
						CLEAR_RANK_REDICTION_DETAILS()
						CLEANUP_SOCIAL_CLUB_LEADERBOARD(Player_Hunt_Data.huntLB_control)
						bPredictionSetup = FALSE
						
						THEFEED_FLUSH_QUEUE()
						DELETE_ALL_HUNTING_TEXT_MESSAGES()

						RESET_PLAYER_TO_SPAWN()
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						DESTROY_MAIN_CAM()
						INIT_VARS_FOR_THIS_MODE()
						DISABLE_CELLPHONE(FALSE)
						
						Player_Hunt_Data.bSetupHuntingControls = FALSE					
						Player_Hunt_Data.bChallengeScreenSetup = FALSE

						// Reset all challenge progress.
						HUNTING_CHALLENGES_CLEAR_PROGRESS(Player_Hunt_Data)

						IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE)
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, TRUE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(Player_Hunt_Data.vehQuad)
							DELETE_VEHICLE(Player_Hunt_Data.vehQuad)
						ENDIF
						
						// Respawn that blazer...
						REQUEST_MODEL(BLAZER)
						WHILE NOT HAS_MODEL_LOADED(BLAZER)
							WAIT(0)
						ENDWHILE
						// We should probably make the quad here...
						CLEAR_AREA_OF_VEHICLES(<<-1707.4337, 4666.5625, 22.1095>>, 2.0)
						Player_Hunt_Data.vehQuad = CREATE_VEHICLE(BLAZER, <<-1707.4337, 4666.5625, 22.1095>>, 323.2491)
						SET_MODEL_AS_NO_LONGER_NEEDED(BLAZER)
						
						CLEAN_ALL_CORPSES()
						HUNTING_SET_QUITTING_TIMER(GET_GAME_TIMER() + 1000)
						
					ELIF HUNTING_GET_QUITTING_TIMER() < GET_GAME_TIMER()
						DO_SCREEN_FADE_IN(500)
						eHuntState = HS_HUNT
						eStage = SS_INIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE SS_SKIPPED
			PRINTLN("RESULTS() :: SS_SKIPPED")
		BREAK
	ENDSWITCH
ENDPROC


#IF IS_DEBUG_BUILD
	PROC DEBUG_MONITOR()
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			Mission_Failed()
		ENDIF
		
		UPDATE_RAG_WIDGETS()
		IF bDisplayChallengeTracking
			UPDATE_CHALLENGE_DEBUG()
		ENDIF
	ENDPROC
	
	PROC HUNTING_UPDATE_DEBUG()
	
		IF IS_DEBUG_KEY_JUST_PRESSED( KEY_E, KEYBOARD_MODIFIER_SHIFT, "removeElk" )
			REMOVE_ALL_ELK()
		ENDIF
		
	ENDPROC
#ENDIF

CONST_INT RADAR_ZOOM_STEALTH	1
CONST_INT RADAR_ZOOM_NORMAL		400
CONST_INT RADAR_ZOOM_CALLING	700



PROC MANAGE_HUNTING_RADAR()
	INT radarZoom = RADAR_ZOOM_NORMAL
	
	IF bPlayerCalling
		radarZoom = RADAR_ZOOM_CALLING
	ELIF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		radarZoom = RADAR_ZOOM_STEALTH
	ENDIF
	
	DONT_ZOOM_MINIMAP_WHEN_RUNNING_THIS_FRAME()
	SET_RADAR_ZOOM(radarZoom)
ENDPROC


/// PURPOSE:
///    We need to ensure that the navmesh around the palyer is loaded, so that animals move.
///    To do this, every 5 seconds we remove all required navmesh, then request a new area around the player.
///    Previously, we were loading a 1km square area, but this blocks streaming.
PROC MANAGE_HUNTING_REQUIRED_NAVMESH()
	IF iNavMeshLoadTime < GET_GAME_TIMER()
		REMOVE_NAVMESH_REQUIRED_REGIONS()
		ADD_NAVMESH_REQUIRED_REGION(vHuntingPlayerCoords.x, vHuntingPlayerCoords.y, 225.0)

		iNavMeshLoadTime = GET_GAME_TIMER() + HUNTING_NAVMESH_LOAD_DELAY
	ENDIF
ENDPROC

PROC MANAGE_HUNTING_EXTERNAL_UI()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	
	DISABLE_SELECTOR_THIS_FRAME()
	SUPRESS_HELP_TEXT()
ENDPROC




// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(HUNTING_LAUNCH_STRUCT structHuntData)
	PRINTLN("***** hunting_ambient.sc : SCRIPT STARTED *****")
	
	iHuntCandidateID = structHuntData.iCandidateID
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SET_RANDOM_TRAINS(FALSE)
			
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		PRINTLN("***** hunting_ambient.sc : FORCE CLEANUP *****")
		Mission_Cleanup(TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)

	INIT_HUNT()
		
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
	#ENDIF
	
	WHILE (TRUE)		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
				Mission_Cleanup()
			ENDIF

			vHuntingPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			MANAGE_HUNTING_REQUIRED_NAVMESH()
			DIAL_DOWN_TRAFFIC()
			MANAGE_HUNTING_TITLE()
			MANAGE_HUNTING_EXTERNAL_UI()

			SWITCH eHuntState
				CASE HS_SETUP
					SETUP_INTRO_SCENE(structHuntData)
				BREAK
				
				CASE HS_HUNT
					HUNT()
					HANDLE_RANDOM_ANIMALS()		
				BREAK
				
				CASE HS_RESULTS
					RESULTS(FALSE, GET_CLOCK_HOURS() > 20 OR GET_CLOCK_HOURS() < 5, eFinishState = FS_FAILED_WANTED, eFinishState = FS_FAILED_LEFT, eFinishState = FS_CANCELLED)	// Dont show 'rety' if we are too late				
				BREAK
			ENDSWITCH	
			
			#IF IS_DEBUG_BUILD
				DEBUG_MONITOR()
				HUNTING_UPDATE_DEBUG()
			#ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
