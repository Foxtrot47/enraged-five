USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage   = IS_REQUEST_SCENE

MODEL_NAMES         mContactModel        = GET_NPC_PED_MODEL(CHAR_HUNTER)
MODEL_NAMES         mContactVehicleModel = DUNE
MODEL_NAMES		    mContactWeaponModel	 = GET_WEAPONTYPE_MODEL(WEAPONTYPE_SNIPERRIFLE)
MODEL_NAMES			mSuppressorModel = GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_AT_SCOPE_LARGE)
MODEL_NAMES			mScopeModel = GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_AT_AR_SUPP_02)
VECTOR              vSniperSpawn		 = <<-683.8261, 5840.8096, 16.566>>
OBJECT_INDEX	    mRifleProp
//INT 			    mScene

/// PURPOSE:
///    Creates the hunters vehicle.  Always use this Function when creating it so it remains consistent.
/// PARAMS:
///    mVehicle - vehicle index to use
///    vPos - where to create it
///    fHeading - heading to use
///    bLockDoors - lock the doors on creation?
///    bWaitForLoad - wait for the model to load?
/// RETURNS:
///    TRUE if vehicle created, FALSE otherwise
PROC Create_Hunters_Car(VEHICLE_INDEX &mVehicle, VECTOR vPos, FLOAT fHeading, BOOL bLockDoors = FALSE) 
	
	CREATE_SCENE_VEHICLE(mVehicle, mContactVehicleModel, vPos, fHeading)
	IF DOES_ENTITY_EXIST(mVehicle)
		SET_VEHICLE_ON_GROUND_PROPERLY(mVehicle)
		SET_VEHICLE_EXTRA(mVehicle, 1, TRUE)
		SET_VEHICLE_EXTRA(mVehicle, 2, FALSE)
		SET_VEHICLE_EXTRA(mVehicle, 3, TRUE)
		SET_VEHICLE_COLOUR_COMBINATION(mVehicle, 0)
		SET_VEHICLE_HAS_STRONG_AXLES(mVehicle, TRUE)
		IF bLockDoors = TRUE
			SET_VEHICLE_DOORS_LOCKED(mVehicle, VEHICLELOCK_LOCKED)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Creates initial scene for Hunting tutorial 1.
FUNC BOOL SetupScene_HUNTING_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  		0
	CONST_INT MODEL_CONTACT_VEHICLE 1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.activationRange = 15.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "CLETUS_MCS_1_concat"
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request animations
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "special_ped@cletus@base", "cletus_base")
			REQUEST_ANIM_DICT("special_ped@cletus@base")

			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@cletus@base")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Cletus
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_HUNTER, << 1804.33, 3931.33, 33.83 >>, 91.63, "RC HUNTER 1")
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_SNIPERRIFLE, 0, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Hunter's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				Create_Hunters_Car(sRCLauncherData.vehID[0], << 1806.7319, 3934.3525, 33.3275 >>, 76.1155, FALSE)
				CLEAR_AREA_OF_VEHICLES(<< 1804.5009, 3922.9170, 32.8091 >>, 2.0)
			ENDIF
	
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
				
		CASE IS_COMPLETE_SCENE
			
			// Apply animation on Cletus
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				GIVE_WEAPON_COMPONENT_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_SNIPERRIFLE, WEAPONCOMPONENT_AT_AR_SUPP_02)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0], PCF_OpenDoorArmIK, TRUE )
				
				SET_CURRENT_PED_WEAPON(sRCLauncherData.pedID[0], WEAPONTYPE_SNIPERRIFLE, TRUE)
				
				IF HAS_ANIM_DICT_LOADED("special_ped@cletus@base")
					CPRINTLN(DEBUG_MISSION, "Play the anim...")
					TASK_PLAY_ANIM(sRCLauncherData.pedID[0], "special_ped@cletus@base", "cletus_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
			ENDIF
			
			// Remove scenario peds at the bottom of the balcony stairs
			CLEAR_AREA_OF_PEDS(<< 1801.1593, 3919.6062, 33.0662 >>, 5.0)
			SET_PED_NON_CREATION_AREA(<< 1803.6034, 3920.1909, 32.9855 >>, << 1798.4159, 3918.2910, 35.0579 >>)

			// Remove the scenario peds by the fence.
			CLEAR_AREA_OF_PEDS(<< 1797.2856, 3927.3591, 33.0176 >>, 4.0)
			SET_PED_NON_CREATION_AREA(<< 1793.7654, 3926.7485, 35.1276 >>, << 1799.7296, 3927.8149, 32.9875 >>)
			
			// Disable scenarios
			IF IS_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HANG_OUT_STREET")
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HANG_OUT_STREET", FALSE)
			ENDIF
			
			IF IS_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_DRINKING")
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_DRINKING", FALSE)
			ENDIF
			
			IF IS_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_SMOKING")
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_SMOKING", FALSE)
			ENDIF

			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: Creates initial scene for Hunting tutorial 2.
FUNC BOOL SetupScene_HUNTING_2(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  			0
	CONST_INT MODEL_CONTACT_VEHICLE  	1
	CONST_INT MODEL_CONTACT_WEAPON		2
	CONST_INT MODEL_WEAPON_SUPP			3
	CONST_INT MODEL_WEAPON_SCOPE		4
	
	// Variables
	MODEL_NAMES mModel[5]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = mContactVehicleModel
	mModel[MODEL_CONTACT_WEAPON]  = mContactWeaponModel
	mModel[MODEL_WEAPON_SUPP]  = mSuppressorModel
	mModel[MODEL_WEAPON_SCOPE]  = mScopeModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_CHAR
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "HUN_2_MCS_1"
			sRCLauncherData.activationRange = 4.0
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			REQUEST_ANIM_DICT("rcmhunting2")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE	
	
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmhunting2")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_HUNTER, << -683.3276, 5841.0420, 17.2170 >>, 8.33, "RC HUNTER 2")
					GIVE_WEAPON_TO_PED(sRCLauncherData.pedID[0], WEAPONTYPE_PUMPSHOTGUN, 0, TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Hunter's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				Create_Hunters_Car(sRCLauncherData.vehID[0], <<-685.2281, 5833.9824, 16.3310>>, 313.2204, FALSE)
			ENDIF
			
			// Create rifle
			IF NOT DOES_ENTITY_EXIST(mRifleProp)
				mRifleProp = CREATE_WEAPON_OBJECT(WEAPONTYPE_SNIPERRIFLE, 0, vSniperSpawn, FALSE)
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(mRifleProp, WEAPONCOMPONENT_AT_SCOPE_LARGE)
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(mRifleProp, WEAPONCOMPONENT_AT_AR_SUPP_02)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE

			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				IF HAS_ANIM_DICT_LOADED("rcmhunting2")
					CPRINTLN(DEBUG_MISSION, "Play the anim...")
					TASK_PLAY_ANIM_ADVANCED(sRCLauncherData.pedID[0], "rcmhunting2", "_idle_loop", 
						<<-683.152, 5841.281, 17.320>>, <<0.0, 0.0, 17.024>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, 
						AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION, 0 , EULER_YXZ, AIK_DISABLE_LEG_IK)
				ENDIF
			ENDIF
			
			// Make him track the player.
			TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_YAW_LIMIT|SLF_WHILE_NOT_IN_FOV)
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vSniperSpawn, 1.0, mContactWeaponModel)
				SET_ENTITY_ROTATION(mRifleProp, <<0.0, -90.0, -162.64>>)
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
