USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

/// PURPOSE: 
///    Sets up locate and cutscene for Mrs. Philips 1
PROC SetupScene_MRS_PHILIPS_1(g_structRCScriptArgs& sRCLauncherData)
	
	sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS	
	sRCLauncherData.triggerLocate[0] = <<1973.323730,3815.593994,31.429359>>
	sRCLauncherData.triggerLocate[1] = <<1974.935791,3812.747314,34.438004>>
	sRCLauncherData.triggerWidth = 6.5
	sRCLauncherData.bAllowVehicleActivation = FALSE
	sRCLauncherData.sIntroCutscene = "TMOM_1_RCM"

ENDPROC
