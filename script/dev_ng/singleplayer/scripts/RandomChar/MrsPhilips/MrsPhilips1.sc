
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "initial_scenes_MrsPhilips.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	MrsPhilips1.sc
//		AUTHOR			:	David Roberts
//		DESCRIPTION		:	Trevor encounters his mother at his trailer
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM eRC_MainState
	RC_MEET_MRS_PHILIPS
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Mission state
eRC_MainState m_eState         = RC_MEET_MRS_PHILIPS
eRC_SubState  m_eSubState      = SS_SETUP

INT iCutsceneStage
BOOL bCutsceneSkipped

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup()

	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()

	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
	ENDIF

	// Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS))
		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), v_ilev_trevtraildr, <<1972.77, 3815.37, 33.66>>)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), 0.0)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), DOORSTATE_LOCKED) // B*1292771
		CPRINTLN(DEBUG_MISSION, "Locked door into Trevor's trailer")
	ENDIF
	Random_Character_Passed(CP_RAND_C_MRS1)
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	
	// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(FALSE, 0)
			ENDIF
			CLEAR_PRINTS()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CLEAR_HELP()
			CLEAR_PRINTS()
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE: 
///    Triggers the mocap cutscene, places Trevor outside the trailer, locks the door and completes mission
PROC STATE_MeetMrsPhilips()

	SWITCH m_eSubState
		CASE SS_SETUP
			RC_REQUEST_CUTSCENE("TMOM_1_RCM")
			iCutsceneStage = 0
			bCutsceneSkipped = FALSE
			m_eSubState = SS_UPDATE
		BREAK
		CASE SS_UPDATE
			SWITCH iCutsceneStage
				CASE 0
					IF RC_IS_CUTSCENE_OK_TO_START()
						RC_CLEANUP_LAUNCHER()
						IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS))
							ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), v_ilev_trevtraildr, <<1972.77, 3815.37, 33.66>>)
						ENDIF
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS))
							DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), 0.0)
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), DOORSTATE_UNLOCKED) // B*1556640
							CPRINTLN(DEBUG_MISSION, "Unlocked door into Trevor's trailer")
						ENDIF
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						RC_START_CUTSCENE_MODE(<< 1974.5129, 3814.5791, 32.4266 >>, TRUE, TRUE, TRUE, TRUE, FALSE) // B*1901880 - don't clear objects in order to prevent broken fences being immediately respawned
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<1974.5129, 3814.5791, 32.4266>>, 270) // Teleport Trevor back outside the trailer
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 4000)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
							CPRINTLN(DEBUG_MISSION, "Making Trevor walk forwards after intro")
						ENDIF
						IF WAS_CUTSCENE_SKIPPED()
							bCutsceneSkipped = TRUE
						ENDIF
					ELSE
						IF bCutsceneSkipped = TRUE
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						ENDIF
						REPLAY_STOP_EVENT()
						RC_END_CUTSCENE_MODE()
						RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
						Script_Passed()
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
		
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_MrsPhilips")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197988
		
		SWITCH(m_eState)
			CASE RC_MEET_MRS_PHILIPS
				STATE_MeetMrsPhilips()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

