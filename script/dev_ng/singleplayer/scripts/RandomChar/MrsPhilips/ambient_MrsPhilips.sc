//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	ambient_MrsPhilips.sc										//
//		AUTHOR			:																//
//		DESCRIPTION		:	Detects the player getting into a cargen with				//
//                          the Deludamol pharmaceuticals								//
//																						//
////////////////////////////////////////////////////////////////////////////////////////// 

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "commands_entity.sch"
USING "RC_helper_functions.sch"
USING "RC_launcher_public.sch"
USING "rich_presence_public.sch"
USING "savegame_public.sch"

// Enums
ENUM MAIN_STATE
	MS_INIT,
	MS_CHECK_FOR_VAN,
	MS_LAUNCH_MISSION,
	MS_ON_MISSION,
	MS_CLEANUP
ENDENUM

ENUM SUB_STATE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Consts
CONST_INT XVERSION_NUMBER(101)

// Variables
MAIN_STATE      eStage = MS_INIT
SUB_STATE		sProgress = SS_SETUP
MODEL_NAMES 	drugVanModel = RUMPO2
BOOL			bAttemptToLaunchScript = FALSE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID mWidgetGroup
	BOOL            bWarpIntoVan
	BOOL			bDebugQuitScript
#ENDIF

// -----------------------------------------------------------------------------------------------------------
//		Debug
// -----------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    Setups Debug Widgets
	PROC SETUP_DEBUG_WIDGETS()
	
		mWidgetGroup = START_WIDGET_GROUP("Ambient: Mrs Philips")
			ADD_WIDGET_BOOL("Warp Player Into Van", bWarpIntoVan)
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    Updates all The Widgets
	PROC UPDATE_DEBUG_WIDGETS()
		IF (bWarpIntoVan)
		
			MODEL_NAMES vanModel = RUMPO2
			
			// Request van
			REQUEST_MODEL(vanModel)
			WHILE NOT HAS_MODEL_LOADED(vanModel)
				WAIT(0)
			ENDWHILE
				
			// Take out of existing vehicle and place into van
			IF IS_PLAYER_PLAYING(PLAYER_ID())

				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX oldCar 
					oldCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(oldCar)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(oldCar)
							SET_ENTITY_AS_MISSION_ENTITY(oldCar)
						ENDIF
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
						DELETE_VEHICLE(oldCar)
					ENDIF
				ENDIF
					
				VEHICLE_INDEX newCar 
				newCar = CREATE_VEHICLE(vanModel, <<2031.3892, 3777.4849, 31.3308>>, 32.5615)
				IF DOES_ENTITY_EXIST(newCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), newCar)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(newCar)
				ENDIF
			ENDIF

			// Release model
			SET_MODEL_AS_NO_LONGER_NEEDED(vanModel)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)

			bWarpIntoVan = FALSE
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Cleans up All The Widgets
	PROC CLEANUP_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(mWidgetGroup)
			DELETE_WIDGET_GROUP(mWidgetGroup)
		ENDIF
	ENDPROC 
#ENDIF

/// PURPOSE:
///    Turn on or off mission vehicle gens
PROC SET_MISSION_VEHICLE_GENS_AVAILABLE(BOOL b_turn_on)
	CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: mission vehicle gens are : ", b_turn_on) 
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_PHARMACY1, b_turn_on)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_PHARMACY2, b_turn_on)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL1, b_turn_on)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL2, b_turn_on)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL3, b_turn_on)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL4, b_turn_on)
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Script Cleanup
PROC SCRIPT_CLEANUP()

	CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Cleanup!")

	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	SET_MISSION_VEHICLE_GENS_AVAILABLE(FALSE)
	
	// No longer require the script to be relaunched when loading from a savegame.
	IF IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MRS_PHILIPS_2)
		Remove_Script_From_Relaunch_List(LAUNCH_BIT_RC_AMB_MRS_PHILIPS)
	ENDIF

	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Request and launch RCM script
PROC CUSTOM_REQUEST_AND_LAUNCH_RC()

	// Get correct script name
	STRING sScriptName = "MrsPhilips2"

	// Load script
	REQUEST_SCRIPT(sScriptName)
	WHILE NOT HAS_SCRIPT_LOADED(sScriptName)
		REQUEST_SCRIPT(sScriptName)
		WAIT(0)
	ENDWHILE
	
	// Setup common requirements
	RC_MISSION_STARTING(RC_MRS_PHILIPS_2)
		
	// Launch script
	START_NEW_SCRIPT(sScriptName, MISSION_STACK_SIZE)
		
	// Allow script to be released when no longer running
	SET_SCRIPT_AS_NO_LONGER_NEEDED(sScriptName)

	#IF IS_DEBUG_BUILD
		//In debug build inform the autoplay system we are on a RC mission now.
		g_txtFlowAutoplayRunningMission = sScriptName
	#ENDIF

ENDPROC

/// PURPOSE:
///    Launch RCM through entering the drug van
FUNC BOOL CUSTOM_LAUNCH_RC_MISSION()

	// NOTE: Need to stay in this loop until the mission is given permission to run.
	//       After first asking permission to run, a decision is usually made
	//		 within a few frames - if it returns FALSE, check if it should terminate or not.
	WHILE NOT RC_REQUEST_PERMISSION_TO_RUN(RC_MRS_PHILIPS_2)
		
		// Not yet given permission to run, should the script terminate?
		IF SHOULD_RC_TERMINATE(RC_MRS_PHILIPS_2)
			PRINT_LAUNCHER_DEBUG("LAUNCH_RC_MISSION - Script denied by RC Controller")
			RETURN FALSE
		ENDIF
	
		WAIT(0)
	ENDWHILE

	// NOTE: The Random Character mission is now safe to start so request the script and launch.
	// 	     The activity MUST trigger because scripts now think the game is OnMission.
	CUSTOM_REQUEST_AND_LAUNCH_RC()
	
	// Tell the playstats system that we're starting an RC mission.
	g_structRCMissionsStatic sRCMissionDetails
	Retrieve_Random_Character_Static_Mission_Details(RC_MRS_PHILIPS_2, sRCMissionDetails)
	TEXT_LABEL tStatLabel = GET_RC_STAT_ID(RC_MRS_PHILIPS_2)
	SCRIPT_PLAYSTATS_MISSION_STARTED(tStatLabel, sRCMissionDetails.rcStatVariation)
	CPRINTLN(DEBUG_FLOW, "Flagging playstat MISSION STARTED for RC mission. Script:", sRCMissionDetails.rcScriptName, " Variation:", sRCMissionDetails.rcStatVariation, " RC Stat ID = ",tStatLabel)

	// Set console rich presence values as we go on mission (friend dashboard status message).
	SET_RICH_PRESENCE_FOR_SP_RC_MISSION(RC_MRS_PHILIPS_2)
	
	// Store start snapshot if this isn't a replay
	IF NOT IS_REPLAY_IN_PROGRESS()
		Store_RC_Replay_Starting_Snapshot("MrsPhilips2", TRUE)
	ENDIF
	
	// Setup mission ready for stats
	PRIME_STATS_FOR_RC_MISSION(RC_MRS_PHILIPS_2, sRCMissionDetails.rcScriptName)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates the mission state and substate
PROC SET_STAGE(MAIN_STATE state)
	eStage 	  = state
	sProgress = SS_SETUP
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		State Functions
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Initialization
PROC DO_INITALIZE()
	
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	SET_MISSION_VEHICLE_GENS_AVAILABLE(TRUE)
		
	CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Setting stage to MS_CHECK_FOR_VAN")
	SET_STAGE(MS_CHECK_FOR_VAN)
ENDPROC	

/// PURPOSE:
///    Detect Trevor getting into a suitable van
PROC STAGE_CHECK_FOR_VAN()

	SWITCH sProgress
		
		CASE SS_SETUP
			bAttemptToLaunchScript = FALSE
			sProgress = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
				
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_RC_MRS_PHILIPS_FIND_VAN)
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS() // B*1260505 Don't display while switching
				CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Displaying MRSP_01_HELP help text")
				ADD_HELP_TO_FLOW_QUEUE("MRSP_01_HELP", FHP_MEDIUM, 0, FLOW_HELP_NEVER_EXPIRES, DEFAULT_HELP_TEXT_TIME, BIT_TREVOR) // Find a van with the Deludamol pharmaceuticals logo.
				SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_RC_MRS_PHILIPS_FIND_VAN)
			ENDIF
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				// Player is in drug van - attempt to launch mission
				MODEL_NAMES vehModel 
				vehModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF (vehModel = drugVanModel)
					bAttemptToLaunchScript = TRUE
					SET_STAGE(MS_LAUNCH_MISSION)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Attempt to launch Mrs Philips 2
PROC STAGE_LAUNCH_MISSION()

	// Is the player close enough to start the mission off
	IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_RANDOM_CHARACTER)

		// Launch the mission script
		IF CUSTOM_LAUNCH_RC_MISSION()
						
			// We are now on mission - keep ambient script running to detect completion
			CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Setting stage to MS_ON_MISSION()")
			bAttemptToLaunchScript = FALSE
			SET_STAGE(MS_ON_MISSION)
		ELSE
			SET_STAGE(MS_CHECK_FOR_VAN)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Monitor mission script whilst it is running
PROC STAGE_MONITOR_MISSION()

	// Mission script has finished...
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)

		// Phone call missions have been completed
		IF IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MRS_PHILIPS_2)
			CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Setting stage to MS_CLEANUP()")
			SET_STAGE(MS_CLEANUP)
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Setting stage to MS_CHECK_FOR_VAN")
			SET_STAGE(MS_CHECK_FOR_VAN)
		ENDIF
	ENDIF
ENDPROC
	
// -----------------------------------------------------------------------------------------------------------
//		Main Script
// -----------------------------------------------------------------------------------------------------------
SCRIPT
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: FORCE CLEANUP")
		SCRIPT_CLEANUP()
	ENDIF
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("ambient_MrsPhilips")) > 1
    	PRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Attempting to launch with an instance already active...")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Register the script so that it can be relaunched when loading from a savegame.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_RC_AMB_MRS_PHILIPS)

	// We are a go-go!
	CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Initializing version ", XVERSION_NUMBER)

	// Main loop
	WHILE (TRUE)

		// Entity check
		IS_PLAYER_PLAYING(PLAYER_ID())
		
		// Update current player
		enumCharacterList ePlayer	
		ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
		
		FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 5.0
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<1973.323730, 3815.593994, 31.429359>>) < (fReplayBlockRange*fReplayBlockRange) //30.0f range for replay recording
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197988
		ENDIF
		
		// Current player is Franklin or Michael
		IF ePlayer <> CHAR_TREVOR
		
			SWITCH eStage
				CASE MS_INIT
				CASE MS_CHECK_FOR_VAN
				CASE MS_LAUNCH_MISSION
					// No need to run these states while not Trevor. Sleep for 5 seconds.
					WAIT(5000)
				BREAK

				CASE MS_ON_MISSION
					STAGE_MONITOR_MISSION()
					WAIT(0)
				BREAK

				CASE MS_CLEANUP
					SCRIPT_CLEANUP()
					WAIT(0)
				BREAK
			ENDSWITCH

		// Only allow mission to trigger if we are Franklin
		ELSE
			SWITCH eStage
				CASE MS_INIT
					DO_INITALIZE()
				BREAK
				
				CASE MS_CHECK_FOR_VAN
					STAGE_CHECK_FOR_VAN()
				BREAK
				
				CASE MS_LAUNCH_MISSION
					STAGE_LAUNCH_MISSION()
				BREAK
				
				CASE MS_ON_MISSION
					STAGE_MONITOR_MISSION()
				BREAK

				CASE MS_CLEANUP
					SCRIPT_CLEANUP()
				BREAK
			ENDSWITCH
			
			// Debug widgets
			#IF IS_DEBUG_BUILD
				UPDATE_DEBUG_WIDGETS() 
				IF (bDebugQuitScript)
					CPRINTLN(DEBUG_AMBIENT, "Ambient - Mrs Philips: Quitting script via debug...")
					SCRIPT_CLEANUP()
				ENDIF	
			#ENDIF
			
			// No need to update every frame
			IF NOT bAttemptToLaunchScript
				WAIT(1000)
			ELSE
				WAIT(0)
			ENDIF
		ENDIF
	ENDWHILE
ENDSCRIPT
