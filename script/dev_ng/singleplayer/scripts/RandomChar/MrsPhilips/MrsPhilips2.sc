
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "player_ped_scenes.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	MrsPhilips2.sc
//		AUTHOR			:	David Roberts
//		DESCRIPTION		:	Trevor must deliver the truck back to his mother at the trailer
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM eRC_MainState
	RC_DELIVER_TRUCK,
	RC_GO_IN_TRAILER,
	RC_MEET_MRS_PHILIPS,
	RC_MISSION_FAILED
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Mission state
eRC_MainState m_eState = RC_DELIVER_TRUCK
eRC_SubState  m_eSubState = SS_SETUP

INT iCutsceneStage

VEHICLE_INDEX vehDrugsVan
BLIP_INDEX blipDrugsVan
BLIP_INDEX blipTrailer
VECTOR vParkOutsideTrailer = <<1990.67, 3810.16, 31.24>>
VECTOR vWalkInTrailer = <<1973.84, 3814.89, 32.43>>

BOOL bDisplayedGoToTrailer = FALSE
BOOL bDisplayedGetInVan = FALSE
BOOL bDisplayedGoInTrailer = FALSE

INT iTrevorSynchedScene

structPedsForConversation sConversation

CAMERA_INDEX camDownAtTrevor

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: ...Random Character Script was triggered so additional cleanup required")
	ENDIF
	
	REMOVE_ANIM_DICT("rcmtmom_2leadinout")
	
	// Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed(BOOL b_do_switch_scene)
	IF b_do_switch_scene = TRUE
		Trigger_Specific_Switch_And_Wait(PR_SCENE_F_CLUB, ENUM_TO_INT(SWITCH_FLAG_SKIP_INTRO)) // B*1482800
		RC_END_CUTSCENE_MODE(DEFAULT, FALSE, FALSE, FALSE) // B*1546355 - Don't turn radar etc back on prior to the switch scene, the switch scene will turn them back on when it's finished
	ELSE
		RC_END_CUTSCENE_MODE()
	ENDIF
	Random_Character_Passed(CP_RAND_C_MRS2)
	
	//forces post-mission switch state for switching back to Trevor
	UpdatePostMissionInfo(g_savedGlobals.sPlayerData.sInfo, CHAR_TREVOR, PR_SCENE_Ta_RC_MRSP2)
	
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	
	// PURPOSE:	Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(FALSE, 0)
			ENDIF
			CLEAR_PRINTS()
			Script_Passed(FALSE)
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			CLEAR_HELP()
			CLEAR_PRINTS()
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================
/// PURPOSE: Sets the new mission state and initialises the substate.
PROC SetState(eRC_MainState in)
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

/// PURPOSE: 
///    Handles the player delivering the truck
PROC STATE_DeliverTruck()

	SWITCH m_eSubState
		
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Initialising STATE_DeliverTruck")
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			REQUEST_ADDITIONAL_TEXT("MRSP2", MISSION_TEXT_SLOT)
			WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				WAIT(0)
			ENDWHILE
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF IS_ENTITY_ALIVE(vehDrugsVan)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDrugsVan)
					SAFE_REMOVE_BLIP(blipDrugsVan)
					IF NOT DOES_BLIP_EXIST(blipTrailer)
						CLEAR_PRINTS()
						blipTrailer = CREATE_COORD_BLIP(vParkOutsideTrailer)
					ENDIF
					IF bDisplayedGoToTrailer = FALSE
						PRINT_NOW("MRSP2_OBJ_01", DEFAULT_GOD_TEXT_TIME, 1) // Go to the ~y~trailer.
						bDisplayedGoToTrailer = TRUE
					ENDIF
					IF IS_ENTITY_AT_COORD(vehDrugsVan, vParkOutsideTrailer, g_vAnyMeansLocate, TRUE)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0, REPLAY_IMPORTANCE_LOW)
						BRING_VEHICLE_TO_HALT(vehDrugsVan, 10.0, 1)
						m_eSubState = SS_CLEANUP
					ENDIF
				ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // Player has got into something other than the drugs van
					IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = RUMPO2 // Check if he's got into another drugs van
						SAFE_REMOVE_BLIP(blipDrugsVan)
						SAFE_RELEASE_VEHICLE(vehDrugsVan)
						vehDrugsVan = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Player got into a different drugs van")
					ENDIF
				ELSE
					SAFE_REMOVE_BLIP(blipTrailer)
					IF NOT DOES_BLIP_EXIST(blipDrugsVan)
						CLEAR_PRINTS()
						blipDrugsVan = CREATE_VEHICLE_BLIP(vehDrugsVan)
					ENDIF
					IF bDisplayedGetInVan = FALSE
						PRINT_NOW("MRSP2_OBJ_02", DEFAULT_GOD_TEXT_TIME, 1) // Get back in the ~b~van.
						bDisplayedGetInVan = TRUE
					ENDIF
				ENDIF
			ELSE
				SetState(RC_MISSION_FAILED)
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Cleaning up STATE_DeliverTruck")
			SAFE_REMOVE_BLIP(blipTrailer)
			SAFE_REMOVE_BLIP(blipDrugsVan)
			SetState(RC_GO_IN_TRAILER)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///    Handles the player going in the trailer
PROC STATE_GoInTrailer()

	SWITCH m_eSubState
		
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Initialising STATE_GoInTrailer")
			IF bDisplayedGoInTrailer = FALSE
				PRINT_NOW("MRSP2_OBJ_03", DEFAULT_GOD_TEXT_TIME, 1) // Go into the ~y~trailer.
				bDisplayedGoInTrailer = TRUE
			ENDIF
			blipTrailer = CREATE_COORD_BLIP(vWalkInTrailer, BLIPPRIORITY_MED, FALSE)
			REQUEST_CUTSCENE("TMOM_2_RCM")
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF IS_ENTITY_ALIVE(vehDrugsVan)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWalkInTrailer, <<1,1,g_vOnFootLocate.z>>, TRUE) // This is just for the visible locate
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1973.323730,3815.593994,31.429359>>, <<1974.935791,3812.747314,34.438004>>, 6.5)
					m_eSubState = SS_CLEANUP
				ELIF NOT IS_ENTITY_AT_COORD(vehDrugsVan, vParkOutsideTrailer, <<10,10,10>>, FALSE)
					REMOVE_CUTSCENE()
					SAFE_REMOVE_BLIP(blipTrailer)
					SetState(RC_DELIVER_TRUCK)
				ENDIF
			ELSE
				SetState(RC_MISSION_FAILED)
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Cleaning up STATE_GoInTrailer")
			SAFE_REMOVE_BLIP(blipTrailer)
			IF IS_FLOW_HELP_MESSAGE_QUEUED("MRSP_01_HELP")
				REMOVE_HELP_FROM_FLOW_QUEUE("MRSP_01_HELP") // B*2083119 Remove help text from the flow queue if it's still queued for whatever reason
				CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Removed MRSP_01_HELP from flow queue")
			ELSE
				CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: MRSP_01_HELP already displayed so no need to remove it from flow queue")
			ENDIF
			SetState(RC_MEET_MRS_PHILIPS)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///    Triggers the mocap cutscene upon delivering the truck
PROC STATE_MeetMrsPhilips()

	SWITCH m_eSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Initialising STATE_MeetMrsPhilips")
			RC_REQUEST_CUTSCENE("TMOM_2_RCM")
			iCutsceneStage = 0
			m_eSubState = SS_UPDATE
		BREAK
		CASE SS_UPDATE
			SWITCH iCutsceneStage
				CASE 0
					REQUEST_ANIM_DICT("rcmtmom_2leadinout")
					IF RC_IS_CUTSCENE_OK_TO_START()
					AND HAS_ANIM_DICT_LOADED("rcmtmom_2leadinout")
						IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS))
							ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_T_TRAILER_CS), v_ilev_trevtraildr, <<1972.77, 3815.37, 33.66>>)
						ENDIF
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 1
					IF IS_CUTSCENE_PLAYING()
						RC_START_CUTSCENE_MODE(vWalkInTrailer)
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							REPLAY_STOP_EVENT()
							camDownAtTrevor = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							SET_CAM_PARAMS(camDownAtTrevor, <<1973.8564, 3818.2217, 34.3958>>, <<-89.4995, 0.0000, 65.3191>>, 50.0)
							SET_CAM_PARAMS(camDownAtTrevor, <<1973.8564, 3818.2217, 34.3958>>, <<-89.4995, 0.0000, 65.3191>>, 50.0)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
							iTrevorSynchedScene = CREATE_SYNCHRONIZED_SCENE(<<1973.909, 3817.762, 33.438>>, <<-0.000, 0.000, -59.400>>)
							SET_SYNCHRONIZED_SCENE_LOOPED(iTrevorSynchedScene, FALSE)
							TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iTrevorSynchedScene, "rcmtmom_2leadinout", "tmom_2_leadout_loop", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Playing anim on Trevor")
							ADD_PED_FOR_DIALOGUE(sConversation, 0, PLAYER_PED_ID(), "TREVOR")
						ENDIF
					ELSE
						//CREATE_CONVERSATION(sConversation, "TMOMAUD", "TMOM_2_RCMLO", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(sConversation, "TMOMAUD", "TMOM_2_RCMLO", CONV_PRIORITY_AMBIENT_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						Script_Passed(TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Starts the mission failed sequence.
PROC STATE_Failed()
	SWITCH m_eSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Initialising STATE_Failed")
			CLEAR_PRINTS()
			IF IS_ENTITY_ALIVE(vehDrugsVan)
				Random_Character_Failed()
			ELSE
				CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Drugs van was destroyed")
				Random_Character_Failed_With_Reason("MRSP2_F_01")
			ENDIF
			m_eSubState = SS_UPDATE
		BREAK
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ELSE
				// not finished fading out: handle dialogue etc here
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()

	SET_MISSION_FLAG(TRUE)

	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PRINT_LAUNCHER_DEBUG("Player not in a vehicle [TERMINATING]")
			Random_Character_Failed()
			Script_Cleanup()
		ELSE
			vehDrugsVan = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			// Find out which veh gen the player got in
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<68.169144,-1558.958130,29.469042>>) < 10 // VEHGEN_MRSP_PHARMACY1
				g_replay.iReplayInt[0] = 0
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<589.439880,2736.707764,42.033165>>) < 10 // VEHGEN_MRSP_PHARMACY2
				g_replay.iReplayInt[0] = 1
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-488.773956,-344.572052,34.363564>>) < 10 // VEHGEN_MRSP_HOSPITAL1
				g_replay.iReplayInt[0] = 2
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<288.880829,-585.472839,43.154282>>) < 10 // VEHGEN_MRSP_HOSPITAL2
				g_replay.iReplayInt[0] = 3
			ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<304.829376,-1383.674194,31.677443>>) < 10 // VEHGEN_MRSP_HOSPITAL3
				g_replay.iReplayInt[0] = 4
			ELSE // VEHGEN_MRSP_HOSPITAL4
				g_replay.iReplayInt[0] = 5
			ENDIF
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Storing g_replay.iReplayInt[0] = ", g_replay.iReplayInt[0])
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Starting setting up MrsPhilips2 replay")
		// Put the player back in the same van
		CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Retrieving g_replay.iReplayInt[0] = ", g_replay.iReplayInt[0])
		VECTOR v_replay_start
		FLOAT f_replay_start
		IF g_replay.iReplayInt[0] = 0 // VEHGEN_MRSP_PHARMACY1
			v_replay_start = <<68.169144,-1558.958130,29.469042>>
			f_replay_start = 49.905754
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_PHARMACY1, FALSE) // B*1569619 Prevent veh gen appearing in the same place
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_PHARMACY1 veh gen off")
		ELIF g_replay.iReplayInt[0] = 1 // VEHGEN_MRSP_PHARMACY2
			v_replay_start = <<589.439880,2736.707764,42.033165>>
			f_replay_start = -175.710495
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_PHARMACY2, FALSE)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_PHARMACY2 veh gen off")
		ELIF g_replay.iReplayInt[0] = 2 // VEHGEN_MRSP_HOSPITAL1
			v_replay_start = <<-488.773956,-344.572052,34.363564>>
			f_replay_start = 82.404198
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL1, FALSE)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_HOSPITAL1 veh gen off")
		ELIF g_replay.iReplayInt[0] = 3 // VEHGEN_MRSP_HOSPITAL2
			v_replay_start = <<288.880829,-585.472839,43.154282>>
			f_replay_start = -20.807068
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL2, FALSE)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_HOSPITAL2 veh gen off")
		ELIF g_replay.iReplayInt[0] = 4 // VEHGEN_MRSP_HOSPITAL3
			v_replay_start = <<304.829376,-1383.674194,31.677443>>
			f_replay_start = -41.116028
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL3, FALSE)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_HOSPITAL3 veh gen off")
		ELSE // VEHGEN_MRSP_HOSPITAL4
			v_replay_start = <<1126.194336,-1481.485962,34.701603>>
			f_replay_start = -91.433693
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_MRSP_HOSPITAL4, FALSE)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Set VEHGEN_MRSP_HOSPITAL4 veh gen off")
		ENDIF
		START_REPLAY_SETUP(v_replay_start, f_replay_start)
		RC_START_Z_SKIP()
		REQUEST_MODEL(RUMPO2)
		WHILE NOT HAS_MODEL_LOADED(RUMPO2)
			CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Loading RUMPO2")
			WAIT(0)
		ENDWHILE
		CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Loaded RUMPO2")
		CLEAR_AREA(v_replay_start, 10.0, TRUE)
		vehDrugsVan = CREATE_VEHICLE(RUMPO2, v_replay_start, f_replay_start)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehDrugsVan)
		END_REPLAY_SETUP(vehDrugsVan)
		SetState(RC_DELIVER_TRUCK)
		bDisplayedGoToTrailer = FALSE
		bDisplayedGetInVan = FALSE
		bDisplayedGoInTrailer = FALSE
		WAIT(250) // Wait for van to settle
		CPRINTLN(DEBUG_MISSION, "MRSPHILIPS2: Finished setting up MrsPhilips2 replay")
		RC_END_Z_SKIP()
	ENDIF
		
	WHILE(TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_DG")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2197988
		
		SWITCH(m_eState)

			CASE RC_DELIVER_TRUCK
				STATE_DeliverTruck()
			BREAK
			
			CASE RC_GO_IN_TRAILER
				STATE_GoInTrailer()
			BREAK
			
			CASE RC_MEET_MRS_PHILIPS
				STATE_MeetMrsPhilips()
			BREAK
			
			CASE RC_MISSION_FAILED
				STATE_Failed()
			BREAK
			
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
