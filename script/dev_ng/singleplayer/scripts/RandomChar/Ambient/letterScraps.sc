//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	letterScraps.sc												//
//		AUTHOR			:	Joanna Wright												//
//		DESCRIPTION		:	Sets up 50 letter scraps around the map for the  			//
//							player to collect											//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//----------------------
//	INCLUDES
//----------------------
USING "email_public.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "CompletionPercentage_public.sch"
USING "savegame_public.sch"
USING "scrap_common.sch"
USING "pickups_letter.sch"
USING "net_system_activity_feed.sch"
 
//----------------------
//	ENUMS
//----------------------
ENUM LScrap_MainState
	MISSION_COLLECT_SCRAPS = 0,
	MISSION_READ_FINISHED_NOTE
ENDENUM

ENUM LScrap_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

//----------------------
//	CONSTS
//----------------------
CONST_INT XVERSION_NUMBER 105
CONST_INT MAX_PAGES 4

//---------------------
//	VARIABLES
//----------------------
LScrap_MainState   m_eState = MISSION_COLLECT_SCRAPS
LScrap_SubState    m_eSubState = SS_SETUP
SCRAP_MISSION_DATA sMissionData
SCRAP_PICKUP_DATA  sPickupData[NUMBER_OF_LETTER_SCRAPS]
INT                scrapInInterior = 4
STRING 			   interiorName = "id1_11_tunnel6_int"
INT 			   iMessageStage = 0
SCALEFORM_INDEX    siMessage
SCALEFORM_INDEX    mov
INT 			   iPage = 0
STRING 			   pageTextArray[MAX_PAGES]
BOOL 			   bButtonPressed = FALSE
BOOL			   bDebugAllClearCheck = FALSE
BOOL			   bHasMissionBeenSetup = FALSE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID mWidgetGroup
	BOOL 			bMapAllScraps
	BOOL 			bCollectAllScraps
	BOOL 			bResetScrapCollection
	BOOL 			bDebugAllowWarping
	BOOL 			bWarpToScrap[NUMBER_OF_LETTER_SCRAPS]
	BOOL 			bDebugQuitScript
	BOOL 			bSimulateScrapCollect
	INT  			iSimulateScrapCollect = 1
	BOOL			bUpdateDebugWidgets
#ENDIF

//----------------------
//	FUNCTIONS
//----------------------
/// PURPOSE:
///    Sets the required state and sets it to SS_SETUP
///  PARAMS:
///    TheState - the wated state
PROC SET_STAGE(LScrap_MainState TheState)
	m_eState = TheState
	m_eSubState = SS_SETUP
ENDPROC

/// PURPOSE:
///    Setup letter scrap position and headings - set global variable for the mission being active
PROC INITIALISE_LETTER_SCRAPS()

	// Mission is running
	g_savedGlobals.sAmbient.sLetterScrapData.bMissionActive = TRUE

	// Initialise letter scraps
	INT i
	REPEAT NUMBER_OF_LETTER_SCRAPS i
		sPickupData[i].vCoords = GET_LETTERSCRAP_PICKUP_COORDS(i)
		sPickupData[i].bActive = FALSE
	ENDREPEAT

	iMessageStage = 0
	
	sPickupData[0].fHeading  = 179.4746
	sPickupData[1].vRot      = << 0.0, 0.0, 198.0 >>
	sPickupData[2].fHeading  = 104.0000
	sPickupData[3].fHeading  = 321.5000		
	sPickupData[4].vRot      = << 0.0, 0.0, 49.0 >>	
	sPickupData[5].vRot      = << 0.0, 0.0, 286.5000 >>	
	sPickupData[6].fHeading  = 278.0092		
	sPickupData[7].vRot      = << 5.300, -1801.400, 15.570 >>
	sPickupData[8].fHeading  = 80.5000 
	sPickupData[9].fHeading  = 116.0000		
	sPickupData[10].fHeading = 15.0000
	
	sPickupData[11].fHeading = 305.5000
	sPickupData[12].fHeading = 0.0000
	sPickupData[13].fHeading = 95.0000	
	sPickupData[13].vRot     = << -0.91, -0.03, -0.81 >>
	sPickupData[14].fHeading = 40.0000
	sPickupData[15].fHeading = 40.0000
	sPickupData[16].fHeading = 40.0000
	sPickupData[17].fHeading = 90.0000	
	sPickupData[17].vRot     = << -0.55, 0.0, 0.78 >>
	sPickupData[18].vRot     = << 0.0, 0.0, 0.0 >>
	sPickupData[19].fHeading = 40.0000
	sPickupData[20].vRot     = << 0.0, 2.0, 15.000 >>
	
	sPickupData[21].fHeading = 40.0000
	sPickupData[22].fHeading = 40.0000
	sPickupData[23].fHeading = 40.0000
	sPickupData[24].fHeading = 40.0000
	sPickupData[25].fHeading = 40.0000
	sPickupData[26].fHeading = 40.0000	
	sPickupData[26].vRot     = << -6.0, 10.0, 0.0 >>
	sPickupData[27].fHeading = 40.0000	
	sPickupData[27].vRot     = << -4.66, 8.70, 67.03 >>
	sPickupData[28].fHeading = 40.0000	
	sPickupData[28].vRot     = << 1.54, -8.31, -19.94 >>
	sPickupData[29].fHeading = 40.0000
	sPickupData[30].fHeading = 40.0000	
	sPickupData[30].vRot     = << 27.21, -3.11, -2.09 >>
	
	sPickupData[31].fHeading = 40.0000
	sPickupData[32].fHeading = 40.0000
	sPickupData[33].fHeading = 40.0000	
	sPickupData[33].vRot     = << -9.89, 0.30, -0.01 >>
	sPickupData[34].fHeading = 40.0000
	sPickupData[35].fHeading = 40.0000
	sPickupData[36].fHeading = 40.0000	
	sPickupData[36].vRot     = << 6.50, -4.41, -0.62 >>
	sPickupData[37].vRot     = << -9.0, 5.3, -2.0 >>
	sPickupData[38].fHeading = 40.0000	
	sPickupData[38].vRot     = << -13.32, -0.57, -0.15 >>
	sPickupData[39].fHeading = 40.0000	
	sPickupData[39].vRot     = << 4.71, -21.26, -0.06 >>
	sPickupData[40].fHeading = 40.0000
	
	sPickupData[41].fHeading = 80.0000
	sPickupData[42].vRot     = << -2.0, 9.0, 1.0 >>
	sPickupData[43].fHeading = 198.0000
	sPickupData[44].fHeading = 198.0000	
	sPickupData[44].vRot     = << 0.0, -20.0, 0.0 >>
	sPickupData[45].fHeading = 198.0000
	sPickupData[46].fHeading = 198.0000
	sPickupData[47].fHeading = 198.0000
	sPickupData[48].fHeading = 198.0000
	sPickupData[49].fHeading = 198.0000
ENDPROC

/// PURPOSE:
///    Updates the scrap array
/// PARAMS:
///    missionData - mission scrap data reference
///    missionPickup - reference to mission pickup array
FUNC BOOL UPDATE_LETTER_SCRAPS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
	
	INT    i
	BOOL   bScrapCollected
	VECTOR vPlayerPos 
	BOOL bShowHelp = (NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()) AND (NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE())
	
	// cache the player position and do is injured check to stop asserts
	IS_PED_INJURED(PLAYER_PED_ID())
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	REPEAT SCRAPS_TO_CHECK_PER_FRAME i
	
		// Has this scrap already been collected?
		bScrapCollected = HAS_SCRAP_BEEN_COLLECTED(missionData.scrapData, missionData.iCurrentCheckIndex)
		
		// If the scrap isn't active...
		IF NOT pickupData[missionData.iCurrentCheckIndex].bActive

			// ...and hasn't been collected
			IF NOT bScrapCollected
			
				// Create pickup
				IF missionData.iCurrentCheckIndex = 1
				OR missionData.iCurrentCheckIndex = 5
				OR missionData.iCurrentCheckIndex = 7
				OR missionData.iCurrentCheckIndex = 13
				OR missionData.iCurrentCheckIndex = 18
				OR missionData.iCurrentCheckIndex = 20
				OR missionData.iCurrentCheckIndex = 37
				OR missionData.iCurrentCheckIndex = 42
					CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl, PICKUP_CUSTOM_SCRIPT, FALSE, TRUE)
				
				// Scrap is within interior
				ELIF missionData.iCurrentCheckIndex = scrapInInterior
					CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl, PICKUP_CUSTOM_SCRIPT, FALSE, TRUE)
					IF DOES_PICKUP_EXIST(pickupData[missionData.iCurrentCheckIndex].pickup)
						ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(pickupData[missionData.iCurrentCheckIndex].pickup, interiorName)
					ENDIF
				ELSE
					CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl)
				ENDIF
			ENDIF
		ELSE
			// Update pickup state
			IF NOT bScrapCollected
				IF UPDATE_SCRAP_PICKUP(missionData, pickupData, vPlayerPos)

					// Only display help text when not on mission
					IF bShowHelp
						IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
							CPRINTLN(DEBUG_AMBIENT, "letters help displaying")
							ADD_HELP_TO_FLOW_QUEUE("LETTERS_FIRST", FHP_MEDIUM)
							SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Increase the current index - make sure it doesn't overrun
		missionData.iCurrentCheckIndex++
		IF (missionData.iCurrentCheckIndex >= COUNT_OF(pickupData))
			missionData.iCurrentCheckIndex = 0
		ENDIF
	ENDREPEAT
	
	IF missionData.bDisplayMessage
		CDEBUG1LN(debug_ambient,"Displaying letter scrap message")
		IF NOT g_bResultScreenDisplaying
			UPDATE_DISPLAY_MESSAGE(missionData.bDisplayMessage, missionData.bMessageOnDisplay, missionData.imessageTimer, SCRAP_LETTER, iMessageStage, siMessage, "LETTERS_TITLE", "LETTERS_COLLECT")
		ELSE
			CDEBUG1LN(debug_ambient,"Can't display, results screen is currently active")
		ENDIF
	ENDIF
		
	// Send the complete flag once everything is collected and message has finished
	IF NOT missionData.bMessageOnDisplay
	AND NOT missionData.bDisplayMessage
		RETURN (missionData.scrapData.iScrapsCollected >= missionData.scrapData.iMaxScraps)
	ENDIF

	RETURN FALSE
ENDFUNC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    Setup Debug Widgets
	PROC SETUP_DEBUG_WIDGETS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
		
		INT           iScrap
		TEXT_LABEL_63 tlTemp
		
		mWidgetGroup = START_WIDGET_GROUP("Letter Scraps")	
			
			ADD_WIDGET_BOOL("Show all scraps on map", bMapAllScraps)
			ADD_WIDGET_BOOL("Force Quit Script", bDebugQuitScript)
			ADD_WIDGET_BOOL("Allow debug warp", bDebugAllowWarping)
			ADD_WIDGET_BOOL("Allow debug tty", bShowScrapDebugTTY)
			ADD_WIDGET_BOOL("Update Widgets", bUpdateDebugWidgets)
			
			START_WIDGET_GROUP("Collection")
				ADD_WIDGET_BOOL("Collect all scraps", bCollectAllScraps)
				ADD_WIDGET_BOOL("Reset scraps collection", bResetScrapCollection)
				ADD_WIDGET_BOOL("Simulate Scrap Collect", bSimulateScrapCollect)
				ADD_WIDGET_INT_SLIDER("Sim Scraps to Collect", iSimulateScrapCollect, 1, COUNT_OF(pickupData), 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Stats")
				ADD_WIDGET_INT_READ_ONLY("Scraps Collected", missionData.scrapData.iScrapsCollected)
				ADD_WIDGET_INT_READ_ONLY("Total Scraps", missionData.scrapData.iMaxScraps)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Positions")

				REPEAT COUNT_OF(pickupData) iScrap
					tlTemp = "Scrap " 
					tlTemp += iScrap
						
					START_WIDGET_GROUP(tlTemp)
						ADD_WIDGET_BOOL("bWarp", bWarpToScrap[iScrap])
						ADD_WIDGET_FLOAT_READ_ONLY("X Position", pickupData[iScrap].vCoords.x)
						ADD_WIDGET_FLOAT_READ_ONLY("Y Position", pickupData[iScrap].vCoords.y)
						ADD_WIDGET_FLOAT_READ_ONLY("Z Position", pickupData[iScrap].vCoords.z)
					STOP_WIDGET_GROUP()
						
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    Updates all The Widgets
	PROC UPDATE_DEBUG_WIDGETS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
		
		INT i 
		INT cnt = 0
		
		IF bUpdateDebugWidgets = FALSE
			EXIT
		ENDIF
		
		IF (bResetScrapCollection)
			
			CPRINTLN(DEBUG_AMBIENT, "Resetting Scrap Collection")
			STAT_SET_INT(missionData.packageStat, 0)
			
			REPEAT COUNT_OF(pickupData) i
				
				SAFE_REMOVE_PICKUP(pickupData[i].pickup)
				SAFE_REMOVE_BLIP(pickupData[i].blip)
				SET_SCRAP_AS_COLLECTED(missionData.scrapData, i, FALSE)
				UPDATE_GLOBAL_SCRAP_DATA(missionData.missionType, i, FALSE)
				pickupData[i].pickup  = NULL
				pickupData[i].bActive = FALSE
			ENDREPEAT
			
			SET_PACKED_STATS_FROM_SCRAP_COLLECT_DATA(missionData.scrapData)
			bResetScrapCollection = FALSE
			bMapAllScraps = TRUE
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		
		IF (bSimulateScrapCollect)
			
			CPRINTLN(DEBUG_AMBIENT, "Simulating Collecting ", iSimulateScrapCollect, "Scraps")
			
			REPEAT COUNT_OF(pickupData) i
				
				IF NOT HAS_SCRAP_BEEN_COLLECTED(missionData.scrapData, i)	
					COLLECT_SCRAP(missionData, pickupData, i)
					
					// Display help for informing Player about the letter
					IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
						CPRINTLN(DEBUG_AMBIENT, "letters help displaying")
						ADD_HELP_TO_FLOW_QUEUE("LETTERS_FIRST", FHP_MEDIUM)
						SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
					ENDIF
					
					cnt++
				ENDIF
				
				IF (cnt >= iSimulateScrapCollect)
					i = COUNT_OF(pickupData) + 1 // so we break out of array
				ENDIF
			ENDREPEAT
			
			bMapAllScraps = TRUE
			bSimulateScrapCollect = FALSE
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		
		IF (bCollectAllScraps)
			
			CPRINTLN(DEBUG_AMBIENT, "Collecting All Scraps")
			STAT_SET_INT(missionData.packageStat, 0)
		
			REPEAT COUNT_OF(pickupData) i
				COLLECT_SCRAP(missionData, pickupData, i)
			ENDREPEAT
			
			// Display help for informing Player about the letter
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
				CPRINTLN(DEBUG_AMBIENT, "letters help displaying")
				ADD_HELP_TO_FLOW_QUEUE("LETTERS_FIRST", FHP_MEDIUM)
				SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_LETTER_INFO_DISPLAYED)
			ENDIF
			
			bCollectAllScraps = FALSE
			bMapAllScraps = TRUE
			bDebugAllClearCheck = TRUE
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		
		IF (bMapAllScraps)
			BLIP_SCRAP_POSITIONS(missionData, pickupData)
			bMapAllScraps = FALSE
		ENDIF
		
		// Handle warping
		IF (bDebugAllowWarping = FALSE)
			EXIT
		ENDIF
		
		REPEAT COUNT_OF(pickupData) i
		
			IF (bWarpToScrap[i])
				CPRINTLN(DEBUG_AMBIENT, "Warping To Scrap:", i)
				LOAD_SCENE(pickupData[i].vCoords)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF i = scrapInInterior
						CPRINTLN(DEBUG_AMBIENT, "Scrap coords are... ", pickupData[i].vCoords)
						INTERIOR_INSTANCE_INDEX	intScrap = GET_INTERIOR_AT_COORDS_WITH_TYPE(pickupData[i].vCoords, interiorName) 
						CPRINTLN(DEBUG_AMBIENT, "Scrap interior is... ", interiorName)
						PIN_INTERIOR_IN_MEMORY(intScrap)
						CPRINTLN(DEBUG_AMBIENT, "Pinned interior... ")
						WHILE NOT IS_INTERIOR_READY(intScrap)
							CPRINTLN(DEBUG_MISSION, "Waiting for interior")
							WAIT(0)
						ENDWHILE
						
						CPRINTLN(DEBUG_MISSION, "Waiting over")
						//CHECK_WARP_TO_PACKAGE(bWarpToScrap[i], pickupData[i].vCoords)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), pickupData[i].vCoords+<<0.0, -1.5, 0.0>>)
						ENDIF
						WAIT(0)
						UNPIN_INTERIOR(intScrap)
					// Special cases of delicate placing require other ped-placing coords, and terrain needing time to load)
					ELIF i = 13 
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << -112.4500, -972.5109, 296.2896 >>)
					ELIF i = 24
						SET_ENTITY_COORDS(PLAYER_PED_ID(), pickupData[i].vCoords+<<1.5, 0.0, 0.0>>)
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), pickupData[i].vCoords+<<0.0, -1.5, 0.0>>)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
					ENDIF
				ENDIF
				
				bWarpToScrap[i] = FALSE
			ENDIF			
		ENDREPEAT
	ENDPROC

	/// PURPOSE:
	///    Cleans up All The Widgets
	PROC CLEANUP_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(mWidgetGroup)
			DELETE_WIDGET_GROUP(mWidgetGroup)
		ENDIF
	ENDPROC 
#ENDIF

//----------------------
//	SCRIPT FUNCTIONS
//----------------------

/// PURPOSE:
///    Script Cleanup
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_MISSION, "Cleanup Letter Scrap Collection")
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	SET_GAME_PAUSED(FALSE)
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	// Mission script is no longer active
	g_savedGlobals.sAmbient.sLetterScrapData.bMissionActive = FALSE
	
	// Cleanup existing pickups
	INT i
	REPEAT COUNT_OF(sPickupData) i 
		SAFE_REMOVE_BLIP(sPickupData[i].blip)
		SAFE_REMOVE_PICKUP(sPickupData[i].pickup)
		sPickupData[i].pickup = NULL
	ENDREPEAT
	
	// Have we collected all letter pickups? (or selected Dreyfuss 1 from debug menu)
	IF (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE) = TRUE)
		// Set the flow flag so the Dreyfuss1 mission can be started
		Set_Mission_Flow_Flag_State(FLOWFLAG_LETTER_SCRAPS_DONE, TRUE)
		
		// Push a message to the PS4 activity feed.
		REQUEST_SYSTEM_ACTIVITY_TYPE_ALL_LETTER_SCRAPS()
		
		// No longer require the script to be relaunched when loading from a savegame.
		Remove_Script_From_Relaunch_List(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)	
	ELIF HAVE_ALL_SCRAPS_BEEN_COLLECTED(sMissionData.scrapData)

		// Set the flow flag so the Dreyfuss1 mission can be started
		Set_Mission_Flow_Flag_State(FLOWFLAG_LETTER_SCRAPS_DONE, TRUE)
		
		// Push a message to the PS4 activity feed.
		REQUEST_SYSTEM_ACTIVITY_TYPE_ALL_LETTER_SCRAPS()
		
		// No longer require the script to be relaunched when loading from a savegame.
		Remove_Script_From_Relaunch_List(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)
	ELSE
		// Register the script so that it can be relaunched when loading from a savegame.
		Register_Script_To_Relaunch_List(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)
	ENDIF
	
	IF (bHasMissionBeenSetup)
		SET_MODEL_AS_NO_LONGER_NEEDED(sMissionData.packageMdl)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Setup pickup positions and initialise mission details and stats, and debug widgets if needed
///    Sort letter scrap collection and increase substate if all collected
PROC STATE_CollectScraps()
	SWITCH(m_eSubState)
		CASE SS_SETUP
			INITIALISE_LETTER_SCRAPS()
			SETUP_SCRAP_MISSION(sMissionData, SCRAP_LETTER, PROP_LD_SCRAP, "LETTERS_COLLECT")
			SETUP_SCRAP_MISSION_STATS(sMissionData, NUM_HIDDEN_PACKAGES_0, PACKSTAT_LETTER_START, NUMBER_OF_LETTER_SCRAPS)
			
			// Setup debug widgets
			#IF IS_DEBUG_BUILD
				//SET_PROFILING_OF_THIS_SCRIPT(TRUE)
				SETUP_DEBUG_WIDGETS(sMissionData, sPickupData)
			#ENDIF
			
			m_eSubState = SS_UPDATE
			bHasMissionBeenSetup = TRUE
		BREAK
		
		CASE SS_UPDATE
			IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			AND NOT IS_SCREEN_FADED_OUT()
				// Has the player collected all of the letter scraps?
				IF UPDATE_LETTER_SCRAPS(sMissionData, sPickupData) OR (bDebugAllClearCheck)
					CPRINTLN(DEBUG_AMBIENT, "All scraps have been collected - Game Time:", GET_GAME_TIMER())
					m_eSubState = SS_CLEANUP
				ENDIF
				
				// don't wait for the message to be displayed - force set the flow flag so that i don't break dan's stuff
				IF (sMissionData.scrapData.iScrapsCollected = sMissionData.scrapData.iMaxScraps)
					IF (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE) = FALSE)
						CPRINTLN(DEBUG_AMBIENT, "Don't wait for message, set the flag and execute the code ID:", GET_GAME_TIMER())
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE, TRUE)
						Execute_Code_ID(CID_DREYFUSS1_COMPLETE_LETTER_SCRAPS, 0)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE, TRUE)
			Execute_Code_ID(CID_DREYFUSS1_COMPLETE_LETTER_SCRAPS, 0)
			SET_STAGE(MISSION_READ_FINISHED_NOTE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Returns whether certain buttons used to display the confession letter are being used
///  RETURNS:
///    TRUE if at least one is, else FALSE 
FUNC BOOL IS_CONFESSION_LETTER_BUTTON_PRESSED()
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STATE_ReadFinishedNote()
	SWITCH(m_eSubState)
		CASE SS_SETUP
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) > 0
				CPRINTLN(DEBUG_AMBIENT, "Letter scraps read note trying to run on game startup. Jumping to cleanup phase.")
				m_eSubState = SS_CLEANUP
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_SCREEN_FADED_IN()
						IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
							IF NOT IS_MOBILE_PHONE_CALL_ONGOING()
							//AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							//AND IS_FLOW_HELP_QUEUE_EMPTY()
							AND NOT IS_ANY_FLOATING_HELP_BEING_DISPLAYED()
							AND NOT HAS_ANY_SAVE_BEEN_REQUESTED() // B*1947128 - Prevent letter appearing game saving has been requested
							AND NOT IS_SAVE_IN_PROGRESS() // B*1947128 - Prevent letter appearing while the game is saving
								CPRINTLN(DEBUG_AMBIENT, "Free from mobile phone and help. Init STATE_ReadFinishedNote - Game Time:", GET_GAME_TIMER())
								bButtonPressed 		= FALSE
								iPage 				= 0
								pageTextArray[0] 	= "LETTERS_PAGE_ONE"
								pageTextArray[1] 	= "LETTERS_PAGE_TWO"
								pageTextArray[2] 	= "LETTERS_PAGE_THREE"
								pageTextArray[3] 	= "LETTERS_PAGE_FOUR"
								mov = REQUEST_SCALEFORM_MOVIE("LETTER_SCRAPS")
								WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
								      WAIT(0)
								ENDWHILE
								
								BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_LETTER_TEXT") 
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pageTextArray[0]) 
								END_SCALEFORM_MOVIE_METHOD()
								SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
								SET_MULTIHEAD_SAFE(TRUE,TRUE)
								DISPLAY_HELP_TEXT_THIS_FRAME("LETTERS_HELP2", FALSE)
								SET_GAME_PAUSED(TRUE)
								DISPLAY_RADAR(FALSE)
								m_eSubState = SS_UPDATE
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_AMBIENT, "Waiting to be off mission before displaying letter scrap note.")
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_AMBIENT, "Waiting for the screen to fade in before displaying letter scrap note.")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_AMBIENT, "Waiting for the player to be alive before displaying letter scrap note.")
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_FRONTEND_THIS_FRAME() // B*1947144 Prevent being able to display the menu
			//allows help text to display over scaleform
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			//DISPLAY_HUD_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
			//DISPLAY_HUD_WHEN_PAUSED_THIS_FRAME()
			//DISPLAY_HELP_TEXT_THIS_FRAME("LETTERS_HELP2", FALSE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
			IF (iPage < MAX_PAGES)
				//x:\gta5\script\dev\singleplayer\scripts\temp\scaleformGraphicTest.sc
				IF NOT bButtonPressed
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						IF (iPage >= 0)
						AND (iPage < (MAX_PAGES-1))
							iPage++
							BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_LETTER_TEXT") 
							    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pageTextArray[iPage]) 
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
						bButtonPressed = TRUE
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						IF (iPage > 0)
						AND (iPage < MAX_PAGES)
							iPage--
							BEGIN_SCALEFORM_MOVIE_METHOD(mov, "SET_LETTER_TEXT") 
							    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pageTextArray[iPage]) 
							END_SCALEFORM_MOVIE_METHOD()
						ENDIF
						bButtonPressed = TRUE
					ELIF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) // B*1946713 - Wait until player releases B to avoid latent triggering of the cinematic camera
						m_eSubState = SS_CLEANUP
					ENDIF
				ELSE
					IF NOT IS_CONFESSION_LETTER_BUTTON_PRESSED()
						bButtonPressed = FALSE
					ENDIF
				ENDIF
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(mov, 255,255,255,255)
			ELSE
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			DISPLAY_RADAR(TRUE)
			//B* 1751172: Activate Completion percentage as soon as last scrap is collected
			//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_DLS1)
			CLEAR_HELP() //so help isn't hanging around
			SET_MULTIHEAD_SAFE(FALSE,TRUE)
			REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)
			SCRIPT_CLEANUP()
		BREAK
	ENDSWITCH
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT
	
	// Setup callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		SCRIPT_CLEANUP()
	ENDIF
	
	// Close down in the event that this script is already running
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("letterScraps")) > 1
    	CPRINTLN(DEBUG_AMBIENT, "Letter Scraps is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Register the script so that it can be relaunched when loading from a savegame.
	REGISTER_SCRIPT_TO_RELAUNCH_LIST(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)

	// Setup arrays and everything else
	CPRINTLN(DEBUG_AMBIENT, "Initializing Letter Scrap Collection - Version:", XVERSION_NUMBER)
	IF (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE) = TRUE)
	    CPRINTLN(DEBUG_AMBIENT, "We've picked up scraps already - Abort")
		REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_RC_AMB_LETTER_SCRAPS)
        REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_DLS1)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_LETTER_SCRAPS_DONE, TRUE)
		Execute_Code_ID(CID_DREYFUSS1_COMPLETE_LETTER_SCRAPS, 0)
        SCRIPT_CLEANUP()
	ENDIF
	
	// Main loop
	WHILE (TRUE)
		
		WAIT(0)
		
		IS_ENTITY_OK(PLAYER_PED_ID())
		SWITCH(m_eState)
			CASE MISSION_COLLECT_SCRAPS
				STATE_CollectScraps()
			BREAK
			
			CASE MISSION_READ_FINISHED_NOTE
				STATE_ReadFinishedNote()
			BREAK
		ENDSWITCH

		// Debug widgets
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS(sMissionData, sPickupData)
			IF bDebugQuitScript
				SCRIPT_CLEANUP()
			ENDIF	
		#ENDIF	
	ENDWHILE
ENDSCRIPT
