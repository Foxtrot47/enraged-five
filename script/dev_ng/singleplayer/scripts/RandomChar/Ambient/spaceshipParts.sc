//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	spaceshipScraps.sc											//
//		AUTHOR			:	Joanna Wright												//
//		DESCRIPTION		:	Sets up 50 spaceship scraps around the map for the  		//
//							player to collect											//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//----------------------
//	INCLUDES
//----------------------
USING "email_public.sch"
USING "rgeneral_include.sch"
USING "RC_Helper_Functions.sch"
USING "CompletionPercentage_public.sch"
USING "savegame_public.sch"
USING "scrap_common.sch"
USING "pickups_spaceship.sch"
USING "achievement_public.sch"
USING "net_system_activity_feed.sch"

//----------------------
//	CONSTS
//---------------------- 
CONST_INT XVERSION_NUMBER 102

//---------------------
//	VARIABLES
//----------------------
SCRAP_MISSION_DATA   sMissionData
SCRAP_PICKUP_DATA    sPickupData[NUMBER_OF_SPACESHIP_PARTS]
INT scrapInInterior  = 15 
INT iMessageStage 	 = 0
BOOL bFinalTextRegistered = FALSE
SCALEFORM_INDEX siMessage

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID mWidgetGroup
	BOOL bMapAllScraps
	BOOL bCollectAllScraps
	BOOL bResetScrapCollection
	BOOL bDebugAllowWarping
	BOOL bWarpToScrap[NUMBER_OF_SPACESHIP_PARTS]
	BOOL bDebugQuitScript = FALSE
	
	BOOL bSimulateScrapCollect
	INT  iSimulateScrapCollect = 1
#ENDIF

//----------------------
//	FUNCTIONS
//----------------------
		
PROC INITIALIZE_SPACESHIP_PARTS()
	iMessageStage = 0
	// Mission is running
	g_savedGlobals.sAmbient.sSpaceshipPartData.bMissionActive = TRUE
	
	// Initialise spaceship parts
	INT i
	REPEAT NUMBER_OF_SPACESHIP_PARTS i
		sPickupData[i].vCoords = GET_UFOSCRAP_PICKUP_COORDS(i)
		sPickupData[i].bActive = FALSE 
	ENDREPEAT
	
	sPickupData[0].fHeading  = 179.4746
	sPickupData[1].fHeading  = 198.0000		
	sPickupData[2].fHeading  = 104.0000
	sPickupData[3].fHeading  = 321.5000		
	sPickupData[4].fHeading  = 256.2500		
	sPickupData[5].fHeading  = 286.5000		
	sPickupData[6].fHeading  = 0.00
	sPickupData[6].vRot		 = << 0.0, 0.0, 1.0 >>
	sPickupData[7].fHeading  = -165.6051	
	sPickupData[8].fHeading  = 91.5000
	sPickupData[9].fHeading  = 116.0000		
	sPickupData[10].fHeading = 15.0000
	
	sPickupData[11].fHeading = 305.5000
	sPickupData[12].fHeading = 0.0000
	sPickupData[13].fHeading = 95.0000
	sPickupData[14].fHeading = 40.0000
	sPickupData[15].fHeading = 40.0000
	sPickupData[16].fHeading = 40.0000
	sPickupData[17].fHeading = 40.0000
	sPickupData[18].fHeading = 40.0000
	sPickupData[19].fHeading = 40.0000
	sPickupData[20].fHeading = 40.0000
	
	sPickupData[21].vRot 	 = << 90.00, 0.00, 57.00 >>
	sPickupData[22].fHeading = 40.0000
	sPickupData[23].fHeading = 40.0000
	sPickupData[24].fHeading = 40.0000
	sPickupData[25].fHeading = 40.0000
	sPickupData[26].fHeading = 40.0000
	sPickupData[27].fHeading = 40.0000
	sPickupData[28].fHeading = 40.0000
	sPickupData[29].fHeading = 40.0000	
	sPickupData[29].vRot     = << -24.35, 15.74, 14.31 >>
	sPickupData[30].fHeading = 40.0000
	
	sPickupData[31].fHeading = 40.0000
	sPickupData[32].fHeading = 40.0000
	sPickupData[33].fHeading = 40.0000
	sPickupData[34].vRot     = << 0.00, 0.00, -15.00 >>
	sPickupData[35].fHeading = 40.0000
	sPickupData[36].fHeading = 40.0000
	sPickupData[37].fHeading = 40.0000
	sPickupData[38].fHeading = 40.0000	
	sPickupData[38].vRot     = << 20.18, -0.87, 20.46 >>
	sPickupData[39].vRot     = << 0.0, 0.0, -4.39 >>
	sPickupData[40].fHeading = 40.0000
	
	sPickupData[41].fHeading = 80.0000
	sPickupData[42].fHeading = 198.0000
	sPickupData[43].fHeading = 198.0000
	sPickupData[44].fHeading = 198.0000
	sPickupData[45].fHeading = 198.0000
	sPickupData[46].fHeading = 198.0000
	sPickupData[47].fHeading = 198.0000
	sPickupData[48].fHeading = 198.0000
	sPickupData[49].fHeading = 198.0000
ENDPROC


/// PURPOSE:
///    Updates the spaceship part array
FUNC BOOL UPDATE_SPACESHIP_PARTS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
	
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	AND NOT IS_SCREEN_FADED_OUT()
		INT    i
		BOOL   bScrapCollected
		VECTOR vPlayerPos 
		
		// cache the player position and do is injured check to stop asserts
		IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		// Check through spaceship parts...
		REPEAT SCRAPS_TO_CHECK_PER_FRAME i
			bScrapCollected = HAS_SCRAP_BEEN_COLLECTED(missionData.scrapData, missionData.iCurrentCheckIndex)
			
			// If scrap isn't active...
			IF (NOT pickupData[missionData.iCurrentCheckIndex].bActive) 
				
				// And hasn't been collected..
				IF NOT bScrapCollected
					
					// Interior check
					IF missionData.iCurrentCheckIndex = scrapInInterior
						
						CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl, PICKUP_CUSTOM_SCRIPT, TRUE)
						IF DOES_PICKUP_EXIST(pickupData[missionData.iCurrentCheckIndex].pickup)
							ADD_PICKUP_TO_INTERIOR_ROOM_BY_NAME(pickupData[missionData.iCurrentCheckIndex].pickup, "GtaMloRoomTun5")
						ENDIF
					
					// Need to force rotation
					ELIF missionData.iCurrentCheckIndex = 6
					OR   missionData.iCurrentCheckIndex = 14
					OR   missionData.iCurrentCheckIndex = 21
					OR   missionData.iCurrentCheckIndex = 34
					OR   missionData.iCurrentCheckIndex = 39
						CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl, PICKUP_CUSTOM_SCRIPT, FALSE, TRUE)
					ELSE
						CREATE_SCRAP_WHEN_IN_RANGE(pickupData[missionData.iCurrentCheckIndex], missionData.packageMdl)
					ENDIF
				ENDIF
			ELSE
				// Check for collection...
				IF (NOT bScrapCollected)
					UPDATE_SCRAP_PICKUP(missionData, pickupData, vPlayerPos)
				ENDIF
			ENDIF
			
			// Increase the current index - make sure it doesn't over run
			missionData.iCurrentCheckIndex++
			IF (missionData.iCurrentCheckIndex >= COUNT_OF(pickupData))
				missionData.iCurrentCheckIndex = 0
			ENDIF
		ENDREPEAT
		
		IF NOT g_bResultScreenDisplaying
			UPDATE_DISPLAY_MESSAGE(missionData.bDisplayMessage, missionData.bMessageOnDisplay, missionData.iMessageTimer,  SCRAP_SHIP, iMessageStage, siMessage, "SSHIP_TITLE", "SSHIP_COLLECT")
		ELSE
			CDEBUG1LN(debug_ambient,"Can't display spaceship parts message, results screen is currently active")
		ENDIF
		
		IF NOT bFinalTextRegistered
			IF missionData.scrapData.iScrapsCollected >= missionData.scrapData.iMaxScraps
				bFinalTextRegistered = REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_UFOPARTS_DONE, CT_FLOW, BIT_FRANKLIN, CHAR_OMEGA, 5000, 10000, VID_BLANK, CID_OMEGA1_COMPLETE_SPACESHIP_PARTS, FLOW_CHECK_NONE, COMM_FLAG_ADD_CONTACT)
			ENDIF
		ENDIF
		
		// Send the complete flag once everything is collected and messsage has stopped
		IF NOT missionData.bMessageOnDisplay
		AND NOT missionData.bDisplayMessage
			RETURN (missionData.scrapData.iScrapsCollected >= missionData.scrapData.iMaxScraps)
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    Setups Debug Widgets
	PROC SETUP_DEBUG_WIDGETS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
		INT iScrap
		TEXT_LABEL_63 tlTemp
		
		mWidgetGroup = START_WIDGET_GROUP("Spaceship Parts")	
			ADD_WIDGET_BOOL("Show all scraps on map", bMapAllScraps)
			ADD_WIDGET_BOOL("Force Quit Script", bDebugQuitScript)
			ADD_WIDGET_BOOL("Allow debug warp", bDebugAllowWarping)
			ADD_WIDGET_BOOL("Allow debug tty", bShowScrapDebugTTY)
			
			START_WIDGET_GROUP("Collection")
				ADD_WIDGET_BOOL("Collect all scraps", bCollectAllScraps)
				ADD_WIDGET_BOOL("Reset scraps collection", bResetScrapCollection)
				ADD_WIDGET_BOOL("Simulate Scrap Collect", bSimulateScrapCollect)
				ADD_WIDGET_INT_SLIDER("Sim Scraps to Collect", iSimulateScrapCollect, 1, COUNT_OF(pickupData), 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Stats")
				ADD_WIDGET_INT_READ_ONLY("Scraps Collected", missionData.scrapData.iScrapsCollected)
				ADD_WIDGET_INT_READ_ONLY("Total Scraps", missionData.scrapData.iMaxScraps)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Positions")

				REPEAT COUNT_OF(pickupData) iScrap
					tlTemp = "Scrap " 
					tlTemp += iScrap
						
					START_WIDGET_GROUP(tlTemp)
						ADD_WIDGET_BOOL("bWarp", bWarpToScrap[iScrap])
						ADD_WIDGET_FLOAT_READ_ONLY("X Position", pickupData[iScrap].vCoords.x)
						ADD_WIDGET_FLOAT_READ_ONLY("Y Position", pickupData[iScrap].vCoords.y)
						ADD_WIDGET_FLOAT_READ_ONLY("Z Position", pickupData[iScrap].vCoords.z)
					STOP_WIDGET_GROUP()
						
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    Updates all The Widgets
	PROC UPDATE_DEBUG_WIDGETS(SCRAP_MISSION_DATA &missionData, SCRAP_PICKUP_DATA &pickupData[])
		INT i 
		INT cnt = 0
		IF (bResetScrapCollection)
			CPRINTLN(DEBUG_AMBIENT, "Resetting Scrap Collection")
			STAT_SET_INT(missionData.packageStat, 0)
			REPEAT COUNT_OF(pickupData) i
				SAFE_REMOVE_PICKUP(pickupData[i].pickup)
				SAFE_REMOVE_BLIP(pickupData[i].blip)
				SET_SCRAP_AS_COLLECTED(missionData.scrapData, i, FALSE)
				UPDATE_GLOBAL_SCRAP_DATA(missionData.missionType, i, FALSE)
				pickupData[i].bActive = FALSE
			ENDREPEAT
			
			SET_PACKED_STATS_FROM_SCRAP_COLLECT_DATA(missionData.scrapData)
			bResetScrapCollection = FALSE
			bMapAllScraps = TRUE
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		
		IF (bSimulateScrapCollect)
			CPRINTLN(DEBUG_AMBIENT, "Simulating Collecting ", iSimulateScrapCollect, "Scraps")
			
			REPEAT COUNT_OF(pickupData) i
				IF NOT HAS_SCRAP_BEEN_COLLECTED(missionData.scrapData, i)	
					COLLECT_SCRAP(missionData, pickupData, i)
					cnt ++
				ENDIF
				
				IF (cnt >= iSimulateScrapCollect)
					i = COUNT_OF(pickupData) + 1 // so we break out of array
				ENDIF
			ENDREPEAT
			
			bMapAllScraps = TRUE

			MAKE_AUTOSAVE_REQUEST()
			bSimulateScrapCollect = FALSE
		ENDIF
		
		IF (bCollectAllScraps)
			CPRINTLN(DEBUG_AMBIENT, "Collecting All Scraps")
			STAT_SET_INT(missionData.packageStat, 0)
		
			REPEAT COUNT_OF(pickupData) i
				COLLECT_SCRAP(missionData, pickupData, i)
			ENDREPEAT
			
			bCollectAllScraps = FALSE
			bMapAllScraps = TRUE
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		
		IF (bMapAllScraps)
			BLIP_SCRAP_POSITIONS(missionData, pickupData)
			bMapAllScraps = FALSE
		ENDIF
		
		// handle warping
		IF (bDebugAllowWarping = FALSE)
			EXIT
		ENDIF
		
		REPEAT COUNT_OF(pickupData) i
			IF (bWarpToScrap[i])
				CPRINTLN(DEBUG_AMBIENT, "Warping To Scrap:", i)
				
				IF i = scrapInInterior
					
					INTERIOR_INSTANCE_INDEX	intScrap = GET_INTERIOR_AT_COORDS(pickupData[i].vCoords)		
					PIN_INTERIOR_IN_MEMORY(intScrap)
					
					WHILE NOT IS_INTERIOR_READY(intScrap)
						WAIT(0)
					ENDWHILE

					CHECK_WARP_TO_PACKAGE(bWarpToScrap[i], pickupData[i].vCoords)

					WAIT(0)
					UNPIN_INTERIOR(intScrap)
					
					bWarpToScrap[i] = FALSE

				ELSE //normal warp
					IF i = 22
						CHECK_WARP_TO_PACKAGE(bWarpToScrap[i], <<28.3896, 646.5005, 189.6085>>)
					ELIF i = 1 //underwater warps not behaving
					OR i = 20
					OR i = 22
					OR i = 40
						CHECK_WARP_TO_PACKAGE(bWarpToScrap[i], (pickupData[i].vCoords+ <<0.0, -4.5, 0.0>>))
					ELSE
						CHECK_WARP_TO_PACKAGE(bWarpToScrap[i], pickupData[i].vCoords)	
					ENDIF
				ENDIF
				
				bWarpToScrap[i] = FALSE
			ENDIF			
		ENDREPEAT
	ENDPROC

	/// PURPOSE:
	///    Cleans up All The Widgets
	PROC CLEANUP_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(mWidgetGroup)
			DELETE_WIDGET_GROUP(mWidgetGroup)
		ENDIF
	ENDPROC 
#ENDIF

//----------------------
//	SCRIPT FUNCTIONS
//----------------------

/// PURPOSE:
///    Script Cleanup
PROC SCRIPT_CLEANUP()
	INT i

	CPRINTLN(DEBUG_AMBIENT, "Cleanup Spaceship Part Collection")
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	// Mission script is no longer active
	g_savedGlobals.sAmbient.sSpaceshipPartData.bMissionActive = FALSE
	
	REPEAT COUNT_OF(sPickupData) i 
		SAFE_REMOVE_BLIP(sPickupData[i].blip)
		SAFE_REMOVE_PICKUP(sPickupData[i].pickup)
	ENDREPEAT
	
	IF HAVE_ALL_SCRAPS_BEEN_COLLECTED(sMissionData.scrapData)
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SPACESHIP_PARTS_DONE) = TRUE 

		// Set the flow flag to allow Omega 2 to proceed
		Set_Mission_Flow_Flag_State(FLOWFLAG_SPACESHIP_PARTS_DONE, TRUE)
		
		// Push a message to the PS4 activity feed.
		REQUEST_SYSTEM_ACTIVITY_TYPE_COLLECTED_SPACESHIP()
	
		// No longer require the script to be relaunched when loading from a savegame.
		Remove_Script_From_Relaunch_List(LAUNCH_BIT_RC_AMB_SPACESHIP_PARTS)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sMissionData.packageMdl)
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT
	
	// Setup callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		SCRIPT_CLEANUP()
	ENDIF
	
	// Close down in the event that this script is already running
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("spaceshipParts")) > 1
    	CPRINTLN(DEBUG_AMBIENT, "Spaceship Parts is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Register the script so that it can be relaunched when loading from a savegame.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_RC_AMB_SPACESHIP_PARTS)
	
	// Setup arrays and everything else
	CPRINTLN(DEBUG_AMBIENT, "Initializing Spaceship Part Collection - Version:", XVERSION_NUMBER)
	INITIALIZE_SPACESHIP_PARTS()
	SETUP_SCRAP_MISSION(sMissionData, SCRAP_SHIP, PROP_POWER_CELL, "SSHIP_COLLECT")
	SETUP_SCRAP_MISSION_STATS(sMissionData, NUM_HIDDEN_PACKAGES_1, PACKSTAT_UFO_START, NUMBER_OF_SPACESHIP_PARTS)

	#IF IS_DEBUG_BUILD
		//SET_PROFILING_OF_THIS_SCRIPT(TRUE)
		SETUP_DEBUG_WIDGETS(sMissionData, sPickupData)
	#ENDIF
	
	// main loop
	WHILE (TRUE)
		
		WAIT(0)
		
		IS_ENTITY_OK(PLAYER_PED_ID())
		
		// Clean up when all items are collected
		IF UPDATE_SPACESHIP_PARTS(sMissionData, sPickupData) 
		OR (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_SPACESHIP_PARTS_DONE) = TRUE)
		
			CPRINTLN(DEBUG_AMBIENT, "Spaceship Parts: All scraps have been collected")
			IF NOT bFinalTextRegistered
				bFinalTextRegistered = REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_UFOPARTS_DONE, CT_FLOW, BIT_FRANKLIN, CHAR_OMEGA, 5000, 10000, VID_BLANK, CID_OMEGA1_COMPLETE_SPACESHIP_PARTS, FLOW_CHECK_NONE, COMM_FLAG_ADD_CONTACT)
			ENDIF
			IF bFinalTextRegistered
				IF NOT (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
					ADD_HELP_TO_FLOW_QUEUE("SSHIP_SWITCH", FHP_MEDIUM, 0, 20000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL|BIT_FRANKLIN)
				ENDIF
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF

		// Update debug
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS(sMissionData, sPickupData)
		#ENDIF	
	ENDWHILE
ENDSCRIPT

