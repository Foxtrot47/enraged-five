USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE
MODEL_NAMES         mContactModel      = GET_NPC_PED_MODEL(CHAR_OMEGA)

/// PURPOSE: 
///    Creates the scene for Omega 1  
FUNC BOOL SetupScene_OMEGA_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  0
	CONST_INT MODEL_SCANNER	 1
	CONST_INT MODEL_RADIO	 2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_SCANNER] = GET_WEAPONTYPE_MODEL(WEAPONTYPE_DIGISCANNER)
	mModel[MODEL_RADIO]   = PROP_CS_WALKIE_TALKIE
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType 			= RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0]		= <<2463.224609,3444.113037,48.593990>>
			sRCLauncherData.triggerLocate[1]		= <<2475.220215,3428.097656,50.990948>>
			sRCLauncherData.triggerWidth			= 17.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene 			= "SCRAP_1_RCM"
			sRCLauncherData.bPedsCritical 			= TRUE
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmrc_omega_1", "omega_idle_geiger_counter")

			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	

		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Omega
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_OMEGA, <<2468.51, 3437.39, 49.90>>, 180, "OMEGA LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create scanner and radio
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
				sRCLauncherData.objID[0] = CREATE_OBJECT(mModel[MODEL_SCANNER], <<2468.51, 3437.39, 55.0>>)
			ENDIF
	
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[1])
				sRCLauncherData.objID[1] = CREATE_OBJECT(mModel[MODEL_RADIO], <<2468.51, 3437.39, 57.0>>)
			ENDIF

			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
			AND DOES_ENTITY_EXIST(sRCLauncherData.objID[0])
			AND DOES_ENTITY_EXIST(sRCLauncherData.objID[1])
				ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[0], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_L_HAND), <<0.09, 0.072, -0.006>>, <<298.000, 194.000, 178.000>>)
				ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[1], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_L_CLAVICLE), << 0.1, 0.08, 0.0 >>, << 162.72, 5.4, 1.8 >>)
			ELSE
				bCreatedScene = FALSE
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK

		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Creates the scene for Omega 2
FUNC BOOL SetupScene_OMEGA_2(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  0
	CONST_INT MODEL_VEHICLE  1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] = mContactModel
	mModel[MODEL_VEHICLE] = DUNE2
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
			
			// Setup launcher data
			sRCLauncherData.triggerType 			= RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0]		= <<2337.376221,2585.721924,45.110653>>
			sRCLauncherData.triggerLocate[1]		= <<2302.157959,2586.505371,47.999573>>
			sRCLauncherData.triggerWidth			= 26.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene 			= "SCRAP_2_RCM"
			sRCLauncherData.bPedsCritical 			= TRUE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmrc_omega_2", "omega_idle_looking_around")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
	
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Omega
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_OMEGA, <<2319.4431, 2583.5881, 46.7663>>, 220, "OMEGA LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create alien buggy
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_VEHICLE], << 2331.08, 2573.53, 46.13 >>, 358.61)
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
		
			// Clear vehicles around Omega
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<2314.5459, 2576.3296, 44.7>>, <<2326.4463, 2579.6082, 47.6645>>, FALSE)
			CLEAR_AREA_OF_VEHICLES(<<2321.2522, 2578.8513, 46.7>>, 10.0)
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
