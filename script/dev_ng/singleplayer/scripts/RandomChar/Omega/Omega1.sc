
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	1 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"

USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "script_player.sch"
USING "commands_recording.sch"

USING "initial_scenes_Omega.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Omega1.sc
//		AUTHOR			:	Joanna Wright
//		DESCRIPTION		:	Franklin meets Omega and is asked to collect spaceship scraps
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

// Constants
CONST_INT OMEGA 0
CONST_INT FOCUS_STAGE_TIME 3500

// Enums
ENUM eRC_MainState
	RC_INIT = 0,
	RC_FOCUS_PUSH,
	RC_MEET_OMEGA,
	RC_LEADOUT,
	RC_LEAVE_AREA,
	RC_FAILED
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM eRC_FailReason
	FAILED_GENERIC,
	FAILED_OMEGA_DIED,
	FAILED_OMEGA_HURT,
	FAILED_OMEGA_SCARED
ENDENUM

// Mission state
eRC_MainState  			  m_eState    = RC_INIT
eRC_SubState   			  m_eSubState = SS_SETUP
eRC_FailReason 			  m_eFailReason= FAILED_GENERIC

BOOL 					  bPostCutsceneConv = FALSE
BOOL 					  bHangAroundConv = FALSE
BOOL 					  bConvStopped = FALSE
BOOL					  bCalledOmegaAFreak = FALSE
FLOAT 					  leaveAreaDistance = 100.0
FLOAT 					  hearTalkingDistance = 20.0
structPedsForConversation sDialogue


STRING 					  sSceneHandleOmega = "Omega"
MODEL_NAMES 			  digiModel = GET_WEAPONTYPE_MODEL(WEAPONTYPE_DIGISCANNER)
MODEL_NAMES				  radioModel = PROP_CS_WALKIE_TALKIE
OBJECT_INDEX			  oiRadio
VECTOR					  vecRadioOffset = << 0.1, 0.08, 0.0 >>
VECTOR					  vecRadioAngles = << 162.72, 5.4, 1.8 >>
SEQUENCE_INDEX 			  seqLeadOut
INT						  iFocusTimeOut
INT 					  iLeadOutTimer
REL_GROUP_HASH 			  relGroupPlayer
INT 				      iObjectiveTimer = 0

VECTOR					  vClearArea[2]
FLOAT					  fClearArea

#IF IS_DEBUG_BUILD
	BOOL bDebugReattach
	WIDGET_GROUP_ID widgetGroup
#ENDIF

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	#IF IS_DEBUG_BUILD
		// Widgets
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	#ENDIF

	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF
	
	PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 1)
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[OMEGA])

	// Cleanup the scene created by the launcher
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
		SET_OBJECT_AS_NO_LONGER_NEEDED(sRCLauncherDataLocal.objID[0])
	ENDIF
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[1])
		SET_OBJECT_AS_NO_LONGER_NEEDED(sRCLauncherDataLocal.objID[1])
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(digiModel)
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
/// PARAMS:
///    None.
/// RETURNS:
///    N/A
PROC Script_Passed()
	ADD_HELP_TO_FLOW_QUEUE("SSHIP_HELP1", FHP_MEDIUM)
	Random_Character_Passed(CP_RAND_C_OMG1)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Sets the new mission state and initialises the substate.
/// PARAMS:
///    eRC_MainState in = new main state
/// RETURNS:
///    N/A
PROC SetState(eRC_MainState in)
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check debug skip functionality
	/// PARAMS:
	///    None.
	/// RETURNS:
	///    N/A
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF
		
//		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
//			WAIT_FOR_CUTSCENE_TO_STOP()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			SetState(RC_INIT)
//		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
		
		// Widget handling
		IF bDebugReattach
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			AND DOES_ENTITY_EXIST(oiRadio)
				CPRINTLN(DEBUG_MISSION, "attach radio")
				DETACH_ENTITY(oiRadio, FALSE)
				ATTACH_ENTITY_TO_ENTITY(oiRadio, sRCLauncherDataLocal.pedID[OMEGA], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[0], BONETAG_L_CLAVICLE), vecRadioOffset, vecRadioAngles)
				bDebugReattach = FALSE
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Sets the required state and sets it to SS_SETUP
PROC SET_STAGE(eRC_MainState TheState)
	m_eState = TheState
	m_eSubState = SS_SETUP
ENDPROC

/// PURPOSE:
///    Checks for Omega being killed - returns FALSE when mission has failed
FUNC BOOL HANDLE_FAIL_CHECKS()
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		// Omega killed
		IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
			CPRINTLN(DEBUG_MISSION, "HANDLE_FAIL_CHECKS - Omega died")
			m_eFailReason = FAILED_OMEGA_DIED
			SetState(RC_FAILED)
			RETURN FALSE
		
		// Omega injured
		ELIF IS_PED_INJURED(sRCLauncherDataLocal.pedID[OMEGA])
			CPRINTLN(DEBUG_MISSION, "HANDLE_FAIL_CHECKS - Omega injured")
			m_eFailReason = FAILED_OMEGA_HURT
			SetState(RC_FAILED)
			RETURN FALSE
		
		// Threatened
		ELIF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[OMEGA])
			CPRINTLN(DEBUG_MISSION, "HANDLE_FAIL_CHECKS - Omega threatened")
			TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[OMEGA], PLAYER_PED_ID(), 500, -1)
			m_eFailReason = FAILED_OMEGA_SCARED
			SetState(RC_FAILED)
			RETURN FALSE
		
		// Vehicle grief checks
		ELIF HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], TRUE)
			CPRINTLN(DEBUG_MISSION, "HANDLE_FAIL_CHECKS - Vehicle grief")
			TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[OMEGA], PLAYER_PED_ID(), 500, -1)
			m_eFailReason = FAILED_OMEGA_SCARED
			SetState(RC_FAILED)
			RETURN FALSE
			
		// Omega ragdoll - will be from ped-ped bump if we get here
		ELIF IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[OMEGA])
			CPRINTLN(DEBUG_MISSION, "HANDLE_FAIL_CHECKS - Omega spooked by ped bump")
			m_eFailReason = FAILED_OMEGA_SCARED
			SetState(RC_FAILED)
			RETURN FALSE
			
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    If the player has an oversized vehicle in the cutscene clear area, move it to a safe location nearby
PROC REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA(VEHICLE_INDEX vehMS = NULL)
	IF NOT IS_ENTITY_ALIVE(vehMS)
		CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA not passed a vehicle, trying to grab mission start vehicle")
		vehMS = GET_MISSION_START_VEHICLE_INDEX()
	ELSE
		CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA was passed a valid vehicle")
	ENDIF
	VECTOR vVehDim[3]
	VECTOR vVehAllowedDim
	BOOL bNeedToReposition = FALSE
	MODEL_NAMES eModel
	IF IS_ENTITY_ALIVE(vehMS)
		IF IS_ENTITY_IN_ANGLED_AREA(vehMS, vClearArea[0], vClearArea[1], fClearArea)
			eModel = GET_ENTITY_MODEL(vehMS)
			IF IS_MODEL_VALID(eModel)
				GET_MODEL_DIMENSIONS(eModel, vVehDim[1], vVehDim[2])
				vVehDim[0] = << vVehDim[2].x-vVehDim[1].x, vVehDim[2].y-vVehDim[1].y, vVehDim[2].z-vVehDim[1].z >>
				vVehAllowedDim = GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
				
				IF ENUM_TO_INT(eModel) = HASH("zentorno")	//#1696370
				OR ENUM_TO_INT(eModel) = HASH("btype")		//#1738925
				OR ENUM_TO_INT(eModel) = HASH("dubsta3")	//#1846988
				OR ENUM_TO_INT(eModel) = HASH("Monster")	//#1891164
					vVehAllowedDim *= 1.1
				ELIF ENUM_TO_INT(eModel) = HASH("t20")		//#2333237 & 2335303
				OR ENUM_TO_INT(eModel) = HASH("virgo")		//#2345087
					vVehAllowedDim *= 1.2
				ENDIF
				
				// Check if vehicle is too big on any axis
				IF vVehDim[0].x > vVehAllowedDim.x
					CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA found a vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehMS), " too big in x, will set as vehicle gen a ways away")
					bNeedToReposition = TRUE
				ELIF vVehDim[0].y > vVehAllowedDim.y
					CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA found a vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehMS), " too big in y, will set as vehicle gen a ways away")
					bNeedToReposition = TRUE
				ELIF vVehDim[0].z > vVehAllowedDim.z
					CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA found a vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehMS), " too big in z, will set as vehicle gen a ways away")
					bNeedToReposition = TRUE
				ELSE
					CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA checked vehicle ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehMS), " but it didn't appear to be too big")
				ENDIF
				
				// Set the vehicle out of the way if needed
				IF bNeedToReposition
					SET_ENTITY_HEADING(vehMS, 248.4908)
					SET_ENTITY_COORDS(vehMS, <<2531.0803, 3357.1709, 50.9746>>)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA has a vehicle but couldn't get a valid model for it")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA has a vehicle but it does not appear to be in the restricted area")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA vehicle entity does not exist?")
	ENDIF
ENDPROC

PROC DATA_INIT()
	vClearArea[0] = <<2453.513428,3424.646484,48.442322>>
	vClearArea[1] = <<2490.059570,3434.194824,58.261032>>
	fClearArea = 32.75
ENDPROC

/// PURPOSE:
///    If a taxi is found near the player, move it down the road
PROC REMOVE_TAXIS()
	INT iVehCount
	VEHICLE_INDEX vehTemp[16]
	GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehTemp)
	REPEAT 16 iVehCount
		IF IS_ENTITY_ALIVE(vehTemp[iVehCount])
		AND IS_ENTITY_IN_ANGLED_AREA(vehTemp[iVehCount], vClearArea[0], vClearArea[1], fClearArea)
			IF GET_ENTITY_MODEL(vehTemp[iVehCount]) = TAXI
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehTemp[iVehCount], VS_DRIVER))
					CPRINTLN(DEBUG_MISSION, "REMOVE_TAXIS found occupied taxi inside area at array index ", iVehCount)
					SET_ENTITY_HEADING(vehTemp[iVehCount], 217.6790)
					SET_ENTITY_COORDS(vehTemp[iVehCount], <<2479.2490, 3401.2341, 48.9551>>)
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_MISSION, "REMOVE_TAXIS found no driven taxis near the player")
ENDPROC

/// PURPOSE:
///    Sets up variables etc
PROC STATE_Init()
	
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	REQUEST_ANIM_DICT("rcmrc_omega_1leadinoutscrap_1_rcm")
	REQUEST_ANIM_DICT("rcmrc_omega_1")
	REQUEST_ADDITIONAL_TEXT("OMEGA1", MISSION_TEXT_SLOT)
	REQUEST_MODEL(digiModel)
	REQUEST_MODEL(radioModel)
	
	IF HANDLE_FAIL_CHECKS()
		
		IF HAS_ANIM_DICT_LOADED("rcmrc_omega_1leadinoutscrap_1_rcm")
		AND	HAS_ANIM_DICT_LOADED("rcmrc_omega_1")
		AND	HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		AND HAS_MODEL_LOADED(digiModel)
		AND HAS_MODEL_LOADED(radioModel)
			
			OPEN_SEQUENCE_TASK(seqLeadOut)
				TASK_PLAY_ANIM(NULL, "rcmrc_omega_1leadinoutscrap_1_rcm", "leadout_scrap_1_rcm_omega", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
				TASK_PLAY_ANIM(NULL, "rcmrc_omega_1", "omega_idle_geiger_counter", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_USE_KINEMATIC_PHYSICS)
			CLOSE_SEQUENCE_TASK(seqLeadOut)
			
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[OMEGA], TRUE)
				SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[OMEGA], TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherDataLocal.pedID[OMEGA], FALSE)
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[OMEGA], PCF_UseKinematicModeWhenStationary, TRUE)
			ENDIF
			
			// Get the PLAYER relationship group, turn Franklin to face him
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
			AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[OMEGA])
				relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[OMEGA], relGroupPlayer)
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[OMEGA], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
				ADD_PED_FOR_DIALOGUE(sDialogue, 3, sRCLauncherDataLocal.pedID[OMEGA], "OMEGA")
				ADD_PED_FOR_DIALOGUE(sDialogue, 1, PLAYER_PED_ID(), "FRANKLIN")
			//	TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA])
			ENDIF
			
			// Set the focus push timer
			iFocusTimeOut = GET_GAME_TIMER() + FOCUS_STAGE_TIME
			
			#IF IS_DEBUG_BUILD
				IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
					widgetGroup = START_WIDGET_GROUP("Omega 1")
					ADD_WIDGET_BOOL("Reattach radio", bDebugReattach)
					ADD_WIDGET_VECTOR_SLIDER("Radio attachment offset", vecRadioOffset, -1, 1, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("Radio attachment rotation", vecRadioAngles, -180, 180, 0.1)
					STOP_WIDGET_GROUP()
				ENDIF
			#ENDIF
			
			SET_STAGE(RC_FOCUS_PUSH)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Watch Omega for a few seconds
PROC STATE_FocusPush()

	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()

	IF HANDLE_FAIL_CHECKS()
		IF GET_GAME_TIMER() < iFocusTimeOut
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], 4.0)
			SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[OMEGA], (<<0, 0, 0>>), TRUE, -1, 2500, DEFAULT_INTERP_OUT_TIME)
			SET_GAMEPLAY_HINT_FOV(35.0)
			SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.30)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.02)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.05)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], 2000)
		ELSE
			SET_STAGE(RC_MEET_OMEGA)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sort and play intro cutscene state
PROC STATE_MeetOmega()
	
	SWITCH m_eSubState
		
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Init: RC_MEET_OMEGA") 

			RC_REQUEST_CUTSCENE("SCRAP_1_RCM")

			IF RC_IS_CUTSCENE_OK_TO_START()
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[OMEGA], sSceneHandleOmega, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
			
				// Start mocap scene
				RC_CLEANUP_LAUNCHER()
				SET_MULTIHEAD_SAFE(TRUE)
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				WAIT(0)
				
				REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA()
				REMOVE_TAXIS()
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vClearArea[0], vClearArea[1], fClearArea, <<2455.8860, 3432.4453, 49.2697>>, 189.77)
				RC_START_CUTSCENE_MODE(<<2468.51, 3437.39, 49.90>>)
	
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
					DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
				ENDIF
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[1])
					DELETE_OBJECT(sRCLauncherDataLocal.objID[1])
				ENDIF

				m_eSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT(TRUE)
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
			
				RC_END_CUTSCENE_MODE()
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				
				IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
					CPRINTLN(DEBUG_MISSION, "Creating digiscanner again") 
					sRCLauncherDataLocal.objID[0] = CREATE_OBJECT(digiModel, <<2468.51, 3437.39, 55.0>>)
				ENDIF
				
				oiRadio = CREATE_OBJECT(radioModel, <<2472.3, 3437.3, 49.3>>)
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
					CPRINTLN(DEBUG_MISSION, "reattach digiscanner") 
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.objID[0], sRCLauncherDataLocal.pedID[OMEGA], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[0], BONETAG_L_HAND), <<0.09, 0.072, -0.006>>, <<298.000, 194.000, 178.000>>) 
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				AND DOES_ENTITY_EXIST(oiRadio)
					CPRINTLN(DEBUG_MISSION, "attach radio") 
					ATTACH_ENTITY_TO_ENTITY(oiRadio, sRCLauncherDataLocal.pedID[OMEGA], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[0], BONETAG_L_CLAVICLE), vecRadioOffset, vecRadioAngles)
				ENDIF
				REPLAY_STOP_EVENT()	
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			
			CPRINTLN(DEBUG_MISSION, "Cleaning up RC_MEET_OMEGA") 
			SET_MULTIHEAD_SAFE(FALSE)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
				IF IS_ENTITY_ATTACHED_TO_ENTITY(sRCLauncherDataLocal.objID[0], sRCLauncherDataLocal.pedID[OMEGA])
					CPRINTLN(DEBUG_MISSION, "entity attached, lead out") 
					SET_STAGE(RC_LEADOUT)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC STATE_Leadout()
	
	// Check for Omega
	IF HANDLE_FAIL_CHECKS()
		
		// State update
		SWITCH m_eSubState
			
			CASE SS_SETUP
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
					CPRINTLN(DEBUG_MISSION, "Omega - TASK_PERFORM_SEQUENCE()") 
					TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[OMEGA], seqLeadOut)
				ENDIF
				
				PRINT_NOW("O1_LEAVEAREA", DEFAULT_GOD_TEXT_TIME, 1)
				iObjectiveTimer = GET_GAME_TIMER()
				
				iLeadOutTimer = GET_GAME_TIMER()
				m_eSubState = SS_UPDATE
			BREAK
			
			CASE SS_UPDATE
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
					IF GET_GAME_TIMER() - iLeadOutTimer > 3000
						m_eSubState = SS_CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE SS_CLEANUP
				SET_STAGE(RC_LEAVE_AREA)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC STATE_Leave_Area()
	
	// Check for Omega
	IF HANDLE_FAIL_CHECKS()

		// State update
		SWITCH m_eSubState
			
			CASE SS_SETUP
				bPostCutsceneConv 	= FALSE
				bHangAroundConv 	= FALSE
				bConvStopped 		= FALSE
				bCalledOmegaAFreak  = FALSE
				m_eSubState 		= SS_UPDATE
			BREAK
			
			CASE SS_UPDATE
			
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						IF NOT bConvStopped
							IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA]) > hearTalkingDistance
								KILL_FACE_TO_FACE_CONVERSATION()
								bConvStopped = TRUE
							ELSE
								IF (GET_GAME_TIMER() - iObjectiveTimer) > DEFAULT_GOD_TEXT_TIME
									IF NOT bPostCutsceneConv
										bPostCutsceneConv = CREATE_CONVERSATION(sDialogue, "SCRAPAU", "SCRAP_1_AMB2", CONV_PRIORITY_MEDIUM)
									ELIF NOT bHangAroundConv
										bHangAroundConv = CREATE_CONVERSATION(sDialogue, "SCRAPAU", "SCRAP_1_HANG", CONV_PRIORITY_MEDIUM)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT bCalledOmegaAFreak
							AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA]) > 50.0
								bCalledOmegaAFreak = CREATE_CONVERSATION(sDialogue, "SCRAPAU", "SCRAP_WALK", CONV_PRIORITY_MEDIUM)
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA]) > leaveAreaDistance
							m_eSubState = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE SS_CLEANUP
				Script_Passed()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Main mission failed fade state
PROC STATE_Failed()

	SWITCH m_eSubState
		
		CASE SS_SETUP
			
			CLEAR_PRINTS()
			
			STRING sFailReason
	
			SWITCH m_eFailReason
			
				CASE FAILED_GENERIC
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_GENERIC")
				BREAK
				CASE FAILED_OMEGA_DIED
					sFailReason = "O1_FAILKILL"  	// Omega died.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_OMEGA_DIED")
				BREAK
				CASE FAILED_OMEGA_HURT
					sFailReason = "O1_FAILHURT" 	// Omega was hurt.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_OMEGA_HURT")
				BREAK
				CASE FAILED_OMEGA_SCARED
					sFailReason = "O1_FAILSCARE"  	// Omega was scared.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_WOMAN_SCARED")
				BREAK
			ENDSWITCH
	
			IF m_eFailReason = FAILED_GENERIC
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	// Take ownership of initial scene entities
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	// Now on mission
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	DATA_INIT()
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
		CPRINTLN(DEBUG_MISSION, "Replay in progress...")
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		VEHICLE_INDEX vTmp
		CREATE_VEHICLE_FOR_REPLAY(vTmp, <<2455.8860, 3432.4453, 49.2697>>, 189.77, FALSE, FALSE, FALSE, FALSE, FALSE)
		REPOSITION_OVERSIZED_START_VEHICLE_IN_AREA(vTmp)
		WHILE NOT SetupScene_OMEGA_1(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	RC_END_CUTSCENE_MODE()
	
	WHILE (TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Omega1")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			
			CASE RC_INIT
				STATE_Init()
			BREAK
			
			CASE RC_FOCUS_PUSH
				STATE_FocusPush()
			BREAK
			
			CASE RC_MEET_OMEGA
				STATE_MeetOmega()
			BREAK
			
			CASE RC_LEADOUT
				STATE_Leadout()
			BREAK
			
			CASE RC_LEAVE_AREA
				STATE_Leave_Area()
			BREAK
			
			CASE RC_FAILED
				STATE_Failed()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
