
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"

USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "script_player.sch"
USING "initial_scenes_Omega.sch"
USING "achievement_public.sch"
USING "commands_recording.sch"
USING "cheat_controller_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Omega2.sc
//		AUTHOR			:	Joanna Wright
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM eRC_MainState
	RC_INIT = 0,
	RC_FOCUS_PUSH,
	RC_MEET_OMEGA,
	RC_WAIT_FOR_PLAYER_TO_EXIT,
	RC_FAILED
ENDENUM

ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM eRC_FailReason
	FAILED_GENERIC,
	FAILED_OMEGA_DIED,
	FAILED_OMEGA_HURT,
	FAILED_OMEGA_SCARED
ENDENUM

// Mission state
eRC_MainState 	m_eState    = RC_INIT
eRC_SubState  	m_eSubState = SS_SETUP
eRC_FailReason 	m_eFailReason= FAILED_GENERIC

REL_GROUP_HASH  relGroupPlayer
INT iFocusTimeOut

CONST_INT OMEGA 0
CONST_INT FOCUS_STAGE_TIME 3500
STRING sSceneHandleOmega = "Omega"
STRING sFranklin = "FRANKLIN"

CONST_INT OMEGA_DOOR_LEFT 0
CONST_INT OMEGA_DOOR_RIGHT 1
CONST_INT OMEGA_DOOR_LATCH 2
STRING sDoors[3]				// Cutscene labels
OBJECT_INDEX oiDoors[3]
STRING sTinySpaceShip = "Flying_Saucer"
OBJECT_INDEX oiTinySpaceShip
//STRING sPowerCell = "Scrap_Power_Cell"
OBJECT_INDEX oiPowerCell
VECTOR vPowerCellCoord = << 2328.6005, 2570.3369, 46.8080 >>
VECTOR vPowerCellAngles = << 90.0, 0.0, -45.3580 >>
MODEL_NAMES mnPowerCell = PROP_CS_POWER_CELL

// Vehicle setup
BOOL bGotID = FALSE
BOOL bInteriorReady = FALSE
BOOL bInsertedCar = FALSE
INTERIOR_INSTANCE_INDEX iiiGarageInterior
VEHICLE_INDEX vehDocker
structPedsForConversation	conversation
BOOL bDocked
INT iOldCheatStateVariable = 0

/// PURPOSE:
///    Sets the new mission state and initialises the substate.
/// PARAMS:
///    eRC_MainState in = new mission state
PROC SetState(eRC_MainState in)
	// Setup new mission state
	m_eState = in
	m_eSubState = SS_SETUP
ENDPROC

///// PURPOSE:
/////    grab and keep open the doors
//PROC SORT_DOORS()
////	SET_DOOR_STATE(DOORNAME_OMEGA_SHED_L, DOORSTATE_UNLOCKED)
////	SET_DOOR_STATE(DOORNAME_OMEGA_SHED_R, DOORSTATE_UNLOCKED)
//	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_OMEGA_SHED_L), -1.0, FALSE, FALSE)
//    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_OMEGA_SHED_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_OMEGA_SHED_R), 1.0, FALSE, FALSE)
//    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_OMEGA_SHED_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//ENDPROC

/// PURPOSE:
///    Grab the doors and set up the labels
PROC GRAB_DOOR_ENTITIES()

	oiDoors[OMEGA_DOOR_LEFT]  = GET_CLOSEST_OBJECT_OF_TYPE(<< 2333.23, 2574.97, 47.03 >>, 5.0, Prop_CH3_01_trlrDoor_L)
	oiDoors[OMEGA_DOOR_RIGHT] = GET_CLOSEST_OBJECT_OF_TYPE(<< 2329.65, 2576.64, 47.03 >>, 5.0, Prop_CH3_01_trlrDoor_R)
	sDoors[OMEGA_DOOR_LEFT]  = "Scrap_Door_L"
	sDoors[OMEGA_DOOR_RIGHT] = "Scrap_Door_R"
	sDoors[OMEGA_DOOR_LATCH] = "Scrap_Latch"
	
ENDPROC

/// PURPOSE:
///    Checks for Omega being killed - returns FALSE when mission has failed
FUNC BOOL HANDLE_FAIL_CHECKS()
	
	// Omega killed
	IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])			
		m_eFailReason = FAILED_OMEGA_DIED
		SetState(RC_FAILED)
		RETURN FALSE
	ELSE
		// Omega injured
		IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[OMEGA])
		OR IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[OMEGA])
			m_eFailReason = FAILED_OMEGA_HURT
			SetState(RC_FAILED)
			RETURN FALSE
		ELSE
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[OMEGA])
				OR HAS_PED_BUMPED_PED_WITH_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], TRUE)
					TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[OMEGA], PLAYER_PED_ID(), 500, -1)
					m_eFailReason = FAILED_OMEGA_SCARED
					SetState(RC_FAILED)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Grab the garage interior so we can stick the car to it
PROC PIN_GARAGE_INTERIOR()

	IF NOT bGotID
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			iiiGarageInterior = GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0]))
			IF IS_VALID_INTERIOR(iiiGarageInterior)
				CPRINTLN(DEBUG_MISSION, "... Grabbed the garage interior")
				PIN_INTERIOR_IN_MEMORY(iiiGarageInterior)
				bGotID = TRUE
			ENDIF
		ENDIF
		
	ELIF NOT bInteriorReady

		IF IS_INTERIOR_READY(iiiGarageInterior)
			CPRINTLN(DEBUG_MISSION, "... Garage interior is ready")
			bInteriorReady = TRUE
		ENDIF

	ELIF NOT bInsertedCar
		
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[0])
			CPRINTLN(DEBUG_MISSION, "... Sticking car to garage interior")
			VECTOR vTmp = GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[0])
			FLOAT fTmp = GET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[0])
			SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
			sRCLauncherDataLocal.vehID[0] = CREATE_VEHICLE(DUNE2, vTmp, fTmp)
			UNPIN_INTERIOR(iiiGarageInterior)
			bInsertedCar = TRUE
		ENDIF
	ENDIF

ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()

	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
	ENDIF
	
	INT iCount
	FOR iCount = 0 TO 2
		SAFE_RELEASE_OBJECT(oiDoors[iCount])
	ENDFOR
	SAFE_RELEASE_OBJECT(oiTinySpaceShip)
	SAFE_RELEASE_OBJECT(oiPowerCell)
	SAFE_RELEASE_VEHICLE(vehDocker)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<2314.5459, 2576.3296, 44.7>>, <<2326.4463, 2579.6082, 47.6645>>, TRUE)
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Script_Passed called")
	Random_Character_Passed(CP_RAND_C_OMG2)
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check debug skip functionality
	PROC DEBUG_Check_Debug_Keys()

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			RC_CleanupSceneEntities(sRCLauncherDataLocal)
			Script_Passed()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			WAIT_FOR_CUTSCENE_TO_STOP()
			RC_CleanupSceneEntities(sRCLauncherDataLocal)
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			IF m_eState <> RC_WAIT_FOR_PLAYER_TO_EXIT // player has passed the mission once he gets to this state
				Random_Character_Failed()
			ENDIF
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Sets up variables etc
PROC STATE_Init()
	
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	PIN_GARAGE_INTERIOR()
	REQUEST_ADDITIONAL_TEXT("OMEGA2", MISSION_TEXT_SLOT)
	REQUEST_MODEL(mnPowerCell)
	
	IF HANDLE_FAIL_CHECKS()
		IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[OMEGA], TRUE)
				SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[OMEGA], TRUE)
			ENDIF
			
			// Get the PLAYER relationship group, turn Franklin to face him
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
			AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[OMEGA])
				relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
				SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[OMEGA], relGroupPlayer)
			ENDIF
			
			// Set the focus push timer
			iFocusTimeOut = GET_GAME_TIMER() + FOCUS_STAGE_TIME
			
			GRAB_DOOR_ENTITIES()
			
			SetState(RC_FOCUS_PUSH)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Watch Omega for a few seconds
PROC STATE_FocusPush()

	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	PIN_GARAGE_INTERIOR()

	IF HANDLE_FAIL_CHECKS()
		IF GET_GAME_TIMER() < iFocusTimeOut
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], 5.0)
			SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[OMEGA], (<<0, 0, 0>>), TRUE, 2000, 2500, DEFAULT_INTERP_OUT_TIME)
			SET_GAMEPLAY_HINT_FOV(35.0)
			SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.30)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.02)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.05)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[OMEGA], 2000)
		ELSE
			SetState(RC_MEET_OMEGA)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    sort and play the intro cutscene stage
PROC STATE_MeetOmega()

	SWITCH m_eSubState
		
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Init RC_MEET_OMEGA")

			// B*1918285 - Trophy needs to unlock even with cheats on as we can't replay this mission and it's only a cutscene
			CPRINTLN(DEBUG_MISSION, "Disabling all Cheats - Current Cheat Variable:", g_iBitsetCheatsUsedThisSession)
			DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
			iOldCheatStateVariable = g_iBitsetCheatsUsedThisSession
			g_iBitsetCheatsUsedThisSession = 0	
			
			RC_REQUEST_CUTSCENE("SCRAP_2_RCM")
			IF RC_IS_CUTSCENE_OK_TO_START()
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[OMEGA])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[OMEGA], sSceneHandleOmega, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_ENTITY_ALIVE(oiDoors[OMEGA_DOOR_LATCH])
					REGISTER_ENTITY_FOR_CUTSCENE(oiDoors[OMEGA_DOOR_LATCH], sDoors[OMEGA_DOOR_LATCH], CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_ENTITY_ALIVE(oiDoors[OMEGA_DOOR_LEFT])
					REGISTER_ENTITY_FOR_CUTSCENE(oiDoors[OMEGA_DOOR_LEFT], sDoors[OMEGA_DOOR_LEFT], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_ENTITY_ALIVE(oiDoors[OMEGA_DOOR_RIGHT])
					REGISTER_ENTITY_FOR_CUTSCENE(oiDoors[OMEGA_DOOR_RIGHT], sDoors[OMEGA_DOOR_RIGHT], CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sFranklin, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(oiTinySpaceShip, sTinySpaceShip, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, P_CS_Saucer_01_S)
//				REGISTER_ENTITY_FOR_CUTSCENE(oiPowerCell, sPowerCell, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Prop_CS_Power_Cell)
				
				// Start mocap scene
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
				
				WAIT(0)
				
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<2314.60059, 2592.44189, 43.64770>>, <<2331.97607, 2571.05786, 50.62959>>, 19.0, 
													<<2313.3340, 2600.8425, 45.8162>>, 352.0448)
				RC_START_CUTSCENE_MODE(<<2486.81, 3433.98, 50.09>>)
				
				CLEAR_AREA_OF_OBJECTS(<<2333.8325, 2576.7463, 45.6678>>, 3.0)
				m_eSubState = SS_UPDATE // Monitor cutscene
			ENDIF
		BREAK
		
		CASE SS_UPDATE
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()						
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()		
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sDoors[OMEGA_DOOR_LEFT])
				FREEZE_ENTITY_POSITION(oiDoors[OMEGA_DOOR_LEFT], TRUE)
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sDoors[OMEGA_DOOR_RIGHT])
				FREEZE_ENTITY_POSITION(oiDoors[OMEGA_DOOR_RIGHT], TRUE)
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sFranklin)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE, FAUS_DEFAULT, TRUE)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(oiTinySpaceShip)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sTinySpaceShip))
					oiTinySpaceShip = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sTinySpaceShip))
				ENDIF
			ELIF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sTinySpaceShip)
				FREEZE_ENTITY_POSITION(oiTinySpaceShip, TRUE)
			ENDIF
	
			IF HAS_CUTSCENE_FINISHED()
				
				RC_END_CUTSCENE_MODE()
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT(TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(oiPowerCell)
				AND HAS_MODEL_LOADED(mnPowerCell)
					oiPowerCell = CREATE_OBJECT_NO_OFFSET(mnPowerCell, vPowerCellCoord)
					SET_ENTITY_ROTATION(oiPowerCell, vPowerCellAngles, EULER_YXZ, FALSE)
				ENDIF
				
				// [AG]: Achievement Unlocked
				// From Beyond The Stars
				AWARD_ACHIEVEMENT_FOR_MISSION(ACH15)

				// B*1918285 - Trophy needs to unlock even with cheats on as we can't replay this mission and it's only a cutscene
				CPRINTLN(DEBUG_MISSION, "                    RESTORING CHEAT VARIABLE:", iOldCheatStateVariable)
				DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
				g_iBitsetCheatsUsedThisSession = iOldCheatStateVariable
		
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_STOP_EVENT()
			CPRINTLN(DEBUG_MISSION, "Cleaning up RC_MEET_OMEGA") 
			SetState(RC_WAIT_FOR_PLAYER_TO_EXIT)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Pass mission but wait for Player to exit the shop door before completely closing the mission
PROC STATE_WaitForPlayerToExit()
	
	SWITCH m_eSubState
	
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Init RC_WAIT_FOR_PLAYER_TO_EXIT")
			ADD_PED_FOR_DIALOGUE(conversation, 1, PLAYER_PED_ID(), "FRANKLIN")
			ASSIGN_VEHICLE_INDEX(vehDocker, sRCLauncherDataLocal.vehID[0])
			bDocked = FALSE
			RC_CleanupSceneEntities(sRCLauncherDataLocal)
			m_eSubState = SS_UPDATE

		BREAK
		
		CASE SS_UPDATE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2326.2996, 2564.4453, 40.6678>>, <<2333.7081, 2581.9390, 65.6678>>, 9.000000)
					Script_Passed()
					m_eSubState = SS_CLEANUP
					
				ELIF NOT bDocked
					IF IS_ENTITY_ALIVE(vehDocker)
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehDocker, TRUE)
						bDocked = CREATE_CONVERSATION(conversation, "SCRAPAU", "SCRAP_DOCK", CONV_PRIORITY_MEDIUM)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Clean up RC_WAIT_FOR_PLAYER_TO_EXIT") 
			Script_Cleanup()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Main mission failed fade state
PROC STATE_Failed()

	SWITCH m_eSubState
		
		CASE SS_SETUP
			
			CLEAR_PRINTS()
			
			STRING sFailReason
	
			SWITCH m_eFailReason
			
				CASE FAILED_GENERIC
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_GENERIC")
				BREAK
				CASE FAILED_OMEGA_DIED
					sFailReason = "O2_FAILKILL"  	// Omega died.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_OMEGA_DIED")
				BREAK
				CASE FAILED_OMEGA_HURT
					sFailReason = "O2_FAILHURT" 	// Omega was hurt.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_OMEGA_HURT")
				BREAK
				CASE FAILED_OMEGA_SCARED
					sFailReason = "O2_FAILSCARE"  	// Omega was scared.
					CPRINTLN(DEBUG_MISSION,"MISSION_FAILED: Reason=FAILED_OMEGA_SCARED")
				BREAK
			ENDSWITCH
	
			IF m_eFailReason = FAILED_GENERIC
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		IF m_eState <> RC_WAIT_FOR_PLAYER_TO_EXIT 
			Random_Character_Failed()
		ELSE
			Script_Passed()
		ENDIF
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress()
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_OMEGA_2(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF

	RC_END_CUTSCENE_MODE()
		
	WHILE (TRUE)
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Omega2")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		SWITCH(m_eState)
			
			CASE RC_INIT
				STATE_Init()
			BREAK
			
			CASE RC_FOCUS_PUSH
				STATE_FocusPush()
			BREAK
			
			CASE RC_MEET_OMEGA
				STATE_MeetOmega()
			BREAK
			
			CASE RC_WAIT_FOR_PLAYER_TO_EXIT
				STATE_WaitForPlayerToExit()
			BREAK

			CASE RC_FAILED
				STATE_Failed()
			BREAK
		ENDSWITCH
		
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
