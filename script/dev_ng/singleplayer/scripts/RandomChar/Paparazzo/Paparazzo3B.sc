
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "CompletionPercentage_public.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "cutscene_public.sch"
USING "script_player.sch"
USING "commands_shapetest.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_public.sch"
USING "RC_Area_public.sch"
USING "RC_Pedsight_public.sch"
USING "commands_recording.sch"

USING "mp_scaleform_functions.sch"

USING "initial_scenes_Paparazzo.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo3B.sc
//		AUTHOR			:	Joe Binks/Tom Kingsley
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

g_structRCScriptArgs sRCLauncherDataLocal

ENUM MISSION_STAGE
	MS_INIT,
	MS_INITIAL_PHONE,
	MS_GO_TO_LOCATION,
	MS_MEET_CONTACT,
	MS_TAKE_PHOTO,
	MS_LEAVE_AREA,
	MS_ESCAPE_SECURITY,
	MS_END_PHONE_CALL,
	MS_MISSION_FAILED
ENDENUM

ENUM SUB_STAGE
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Fail reasons
ENUM FAIL_REASON
	FAIL_DEFAULT,
	FAIL_PHOTO_LOST,
	FAIL_SPOTTED,
	FAIL_PRINCESS_INJURED,
	FAIL_PRINCESS_KILLED,
	FAIL_CONTACT_THREAT,
	FAIL_CONTACT_KILLED
ENDENUM

ENUM SECURITY_STATE
	
	SS_GUARD_AREA,
	SS_WARN_PLAYER,
	SS_ENTER_VEHICLE,
	SS_DRIVER_TO_PLAYER,
	SS_LEAVE_VEHICLE,
	SS_LEAVE_AREA,
	SS_DRIVE_AWAY_FROM_AREA,
	SS_DRIVE_OUT_OF_ALLEY,
	SS_DRIVE_WANDER,
	SS_WAIT_AIM_AT_GOONS,
	SS_AIM_AT_GOONS,
	SS_INVESTIGATE,
	SS_ATTACK_GOONS,
	SS_ATTACK_PLAYER_MELEE,
	SS_ATTACK_PLAYER_GUNS,
	SS_MOVING_TO_GOONS,
	SS_ATTACK_PLAYER_CAR

ENDENUM

ENUM PRINCESS_STATE
	
	SP_WAIT,
	SP_SMOKE,
	SP_ENTER_VEHICLE,
	SP_FLEE_IN_VEHICLE,
	SP_FLEE_ON_FOOT,
	SP_WANDER,
	SP_LOOK_AT_PLAYER

ENDENUM

ENUM GOON_STATE
	
	GS_WAIT,
	GS_WAIT_AIM_AT_SECURITY,
	GS_AIM_AT_SECURITY,
	GS_SHOOT_SECURITY,
	GS_FLEE_AREA
	
ENDENUM

ENUM DEALER_STATE
	
	DS_WAIT,
	DS_FLEE,
	DS_CALM_DOWN,
	DS_WAIT_FOR_FLEE,
	DS_LEAVE_AREA

ENDENUM

// MISSION PED
STRUCT MISSION_PED
	PED_INDEX mPed
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
	STRING	sIntialAnim
	BOOL	bImDead
	BLIP_INDEX	mBlip
ENDSTRUCT


// MISSION VEHICLE
STRUCT MISSION_VEHICLE
	VEHICLE_INDEX mVehicle
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT

// MISSION PROPS
STRUCT MISSION_PROP
	OBJECT_INDEX mObject
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT


MISSION_STAGE missionStage = MS_INIT
SUB_STAGE  eSubStage = SS_SETUP
PRINCESS_STATE ePrincessState = 	SP_WAIT
DEALER_STATE   eDealerState = 	DS_WAIT

// STAGE SKIPPING
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 5
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

MISSION_STAGE eTargetStage
BOOL bFinishedSkipping

//----------------------
//	CHECKPOINTS
//----------------------
CONST_INT CP_PHONE_CALL	0
CONST_INT CP_AT_PRINCESS_LOCATION 1
CONST_INT CP_MISSION_PASSED 2

CONST_INT Z_SKIP_PHONE_CALL					1

CONST_INT Z_SKIP_AT_PRINCESS_LOCATION		3

TEXT_LABEL_23 labelRetrig
INT iSubsDealer

CONST_INT		NUM_OF_SECURITY_PEDS	4
CONST_INT		NUM_OF_SECURITY_CARS	2
CONST_INT		NUM_OF_DEALER_GOONS		2

//BLIPS
BLIP_INDEX		biMissionBlip

SCENARIO_BLOCKING_INDEX sbiChicoRear
SCENARIO_BLOCKING_INDEX sbiVehAttractor

OBJECT_INDEX objWeedBag
OBJECT_INDEX objCash

//OBJECT_INDEX objFakeGuard
//OBJECT_INDEX objFakeDealer

INT issDealidles
INT issDeal

//PEDS
MISSION_PED		mSecurityPed[NUM_OF_SECURITY_PEDS]
MISSION_PED		mpPrincess
MISSION_PED		mpDrugDealer
MISSION_PED		mpDealerGoons[NUM_OF_DEALER_GOONS]
MISSION_PED		mpDealerContact

//VEHICLES
MISSION_VEHICLE	mvSecurityCars[NUM_OF_SECURITY_CARS]
MISSION_VEHICLE	mvDealerCar
MISSION_VEHICLE mvClimbingTruck

//PROPS
MISSION_PROP	oiSmokeWeed

//BOOLS
BOOL			bMainObjectiveDisplayed	//HAS THE MAIN OBJECTIVE BEEN DISPLAYED
BOOL			bPrincessSceneCreated	//HAS THE SCENE BEEN CREATED
BOOL			b_Correct_Pic_Taken		//WAS THE CORRECT PICTURE TAKEN
BOOL			bPhotoSent				//HAS THE PHOTO BEEN SENT TO BEVERLY
BOOL			bSecurityAttacking		//IF THE SECURITY ARE ATTACKING THE PLAYER
BOOL			bSecurityNoticedPlayer 	//IF THE SECURITY HAVE NOTICED THE PLAYER
BOOL			bConversationActive 	//IF A CONVERSATION IS ACTIVE
BOOL			bHadEndPhoneCall 		//IF THE PLAYER HAS HAD END PHONECALL
BOOL			bLeavingDrugScene 		//IF THE PEDS ARE LEAVING DRUG SCENE
BOOL			bPlayerSpotted 			//IF THE PLAYER HAS BEEN SPOTTED BY SECURITY
BOOL			bBeverlyCalled			//IF THE PLAYER HANGS AROUND LONG ENOUGH FOR BEVERLY TO CALL
BOOL			bFailDialogue 			// DIALOGUE PLAYER AT END OF MISSION WHEN FAILING
//BOOL			bAlreadyLeft			// the player left the area before sending the photo
BOOL			bEnteredAlley			// has the player entered the alley area? Used for failing if the player hangs around too long
BOOL			bSecurityWarned			// Have the security guys on the left hand side warned the player?
BOOL			bForceCrouching			// Should we force the player to crouch when approaching the alley?
BOOL bSetRel
BOOL bKickedOffDealerConvo
//BOOL bBadPicSent
BOOL bCarAlarm	
BOOL bAlleyWarning
BOOL bWhosThatGuy
BOOL bPicTaken
BOOL bTooFarAway
BOOL bSweetTxtSent
BOOL bPicSent
BOOL bLeavingScene 
BOOL bPrintLoseCops
BOOL bPrintLeaveArea
BOOL bSecPed3AchHead
BOOL bWrongContact
BOOL bGoGoDealerConvo
BOOL bDoFirstConvoLine
BOOL bSkippedSS

BOOL bSimpleHelp
INT iHelp
BOOL bHelpPrint1
BOOL bHelpPrint2
BOOL bHelpPrint3
BOOL bPrintNonSimpleHelp
BOOL bHelpPrint8
BOOL bMissionFailed
BOOL bDealersTempEventsUnblocked
BOOL bPrincessStartedSyncedScene

//TIMERS
INT				iLeaveSceneTimer		// Makes everyone leave the drug deal after an amount of time
INT				iNoticeInAlleyTimer		// Makes security notice the player if they're in the alley for too long
INT				iTriggerPhoneTimer		// Makes Beverly phone if the player doesn't leave the deal quickly
INT				iGoonTimer				// Makes the drug goons attack the security
INT				iSecurityTimer			// Makes the security aim at the drug goons
INT				iIndividualLeaveTimer	// Delays each ped leaving the drug deal to make it look more natural
//INT				iBumpContactTimer		// The check for the player bumping into the contact
INT iTimerPicTaken
INT iSecurityEvaded
INT iSecurityDead
INT iSecurityBlipped
INT iFramesPlayerNotInCoverNextToCar
INT iFramesPlayerMakingNoise
INT iFramesPlayerOnRoof
INT iTimerSecWary
INT iTimerSmokeIdle
INT iBevTxtsBadPic
INT iBevTxtsTooFarAway
INT iSeqGuyDriveRoundBack
INT iPlayerInLeftAlley
INT iSeqSecThreaten
INT iPrincessShoutLines
INT iPrincessShoutTimer
INT iTimerTakePhotoStage
INT iSeqDeal
INT iSubSwitch
INT iRetrigger
INT iCounterContactWalkOff
INT iTimerGuard3Investigate
INT iCounterGaurd3Investigate

INT				iPanicConvo             // Counter for playing a couple lines if player gets spotted

//STRING sBumpLines[2]
//INT	iCurrentBumpLine = 0

//STATES
SECURITY_STATE	eSecurityState[NUM_OF_SECURITY_PEDS]
GOON_STATE		eGoonState[NUM_OF_DEALER_GOONS]

//STRINGS
FAIL_REASON 		eFailReason

//VECTORS
VECTOR			vPrincessInitialLocation	=	<<1070.36, -776.11, 58.25>>
VECTOR			vAlleyInvestigateLoc		=	<< 1068.5176, -789.5724, 57.2626 >>
VECTOR vLastPlayerCoords

//FLOATS
CONST_FLOAT		STOP_DIS	20.0
CONST_FLOAT		CONTACT_DIS	5.0
CONST_FLOAT		LEAVE_DIS	50.0

//POLY AREAS
TEST_POLY		tpSpotArea		//USED TO CREATE AN AREA WHERE PLAYER CAN BE SPOTTED
TEST_POLY		tpLeaveArea		//USED TO SEE IF THE PLAYER HAS LEFT THE AREA
TEST_POLY		tpBackAwayArea	//USED IF PLAYER IS TRYING TO ENTER LEFT ENTRANCE
TEST_POLY		tpAlleyArea		//USED IF THE PLAYER IS IN THE ALLEY
TEST_POLY		tpCrouchArea	//USED TO MAKE THE PLAYER CROUCH WHEN APPROACHING THE ALLEY

//Relationship groups
//REL_GROUP_HASH 		relHatePlayer

REL_GROUP_HASH 		relPrincess
REL_GROUP_HASH 		relDealers

// CONVERSATIONS
structPedsForConversation sSpeach
CONST_INT FRANKLIN_ID 1
CONST_INT BEVERLY_ID 3
CONST_INT GUARD_ONE_ID 4
CONST_INT GUARD_TWO_ID 7
CONST_INT PRINCESS_ID 5
CONST_INT DEALER_ID 6
CONST_INT GOON_ID 8

STRING ANIM_DICT = "rcmpaparazzo_3b"
STRING DEALER_ANIM_DICT = "amb@world_human_stand_impatient@male@no_sign@idle_a"
STRING DEALER_ANIM_DICT_2 = "amb@world_human_stand_impatient@male@no_sign@base"
//STRING PRINCESS_ANIM = "idle_b"
//STRING DEALER_ANIM = "idle_a"
//STRING CONTACT_ANIM = "idle_c"
//STRING GOON_0_ANIM = "base"
//STRING GOON_1_ANIM = "idle_b"

INT iSightTestID = -1

//INIT ARRAYS
/// PURPOSE:
///    Loads all the inital arrays - PED LOCATIONS etc. 
PROC INIT_ARRAYS
	
	//PEDS

	//SECURITY
	mSecurityPed[0].vStartPos = <<1083.619141,-795.724792,58.307529>>
	mSecurityPed[0].fStartHeading = 6.896940//352.0
	mSecurityPed[0].mModel = S_M_M_HIGHSEC_02
	mSecurityPed[1].vStartPos = <<1087.099854,-792.740479,58.274784>>//<<1087.332031,-793.597656,58.278053>>//<< 1085.7023, -792.9296, 57.2656 >>
	mSecurityPed[1].fStartHeading = 94.733353//34.0
	mSecurityPed[1].mModel = S_M_M_HIGHSEC_01
	mSecurityPed[2].vStartPos = << 1110.2434, -781.5825, 57.2626 >>
	mSecurityPed[2].fStartHeading = 8.0
	mSecurityPed[2].mModel = S_M_M_HIGHSEC_02
	mSecurityPed[3].vStartPos = <<1108.882690,-782.282959,58.262680>>//<< 1107.3772, -781.2589, 57.2626 >>
	mSecurityPed[3].fStartHeading = -0.550270
	mSecurityPed[3].mModel = S_M_M_HIGHSEC_01
	
	//PRINCESS
	mpPrincess.vStartPos = << 1084.57, -794.883, 58.3022 >>
	mpPrincess.fStartHeading = 114.527542
	mpPrincess.mModel = U_F_Y_Princess
	
	//DRUG DEALER
	mpDrugDealer.vStartPos = <<1084.756348,-793.635803,58.290344>>
	mpDrugDealer.fStartHeading = -160.000000
	mpDrugDealer.mModel = S_M_Y_DEALER_01//G_M_Y_MexGoon_03
	
	//DEALER GOONS
	mpDealerGoons[0].vStartPos = <<1074.095215,-789.950806,58.284847>>
	mpDealerGoons[0].fStartHeading = -101.459152
	mpDealerGoons[0].mModel = G_M_Y_MexGoon_03
	mpDealerGoons[1].vStartPos = <<1083.942749,-789.526184,58.273258>>
	mpDealerGoons[1].fStartHeading = -171.793823
	mpDealerGoons[1].mModel = G_M_Y_MexGoon_03
	
	// PLAYER'S CONTACT
	mpDealerContact.vStartPos = vPrincessInitialLocation
	mpDealerContact.fStartHeading = 35.52//0.0
	mpDealerContact.mModel = G_M_Y_MEXGOON_03
	
	//VEHICLES

	//SECURITY CARS
	mvSecurityCars[0].vStartPos = <<1086.702515,-796.687683,57.768784>>//<<1086.236694,-796.235229,57.770428>>//<< 1085.5811, -794.9450, 57.2865 >>
	mvSecurityCars[0].fStartHeading = -58.433346//-61.270905
	mvSecurityCars[0].mModel = landstalker
	
	mvSecurityCars[1].vStartPos = <<1110.9280, -789.2598, 57.2627>>//<<1112.42, -784.03, 57.76>>//<< 1112.6432, -781.8356, 57.2626 >>
	mvSecurityCars[1].fStartHeading = 0//1.5
	mvSecurityCars[1].mModel = landstalker
	
	mvDealerCar.vStartPos = <<1071.817627,-790.166748,57.620956>>//<< 1071.9553, -790.5126, 57.2626 >>
	mvDealerCar.fStartHeading = 176.838257//194.0
	mvDealerCar.mModel = buccaneer
	/*
	mvDealerCar.vStartPos = <<1072.3691, -790.0135, 57.2831>>
	mvDealerCar.fStartHeading = 181.5556
	mvDealerCar.mModel = BURRITO
	*/
	mvClimbingTruck.vStartPos = << 1137.3795, -792.8448, 56.6033 >>
	mvClimbingTruck.fStartHeading = 89.9365
	mvClimbingTruck.mModel = BENSON
	
	//PROPS
	oiSmokeWeed.vStartPos = << 1084.0536, -792.4236, 57.2626 >>
	oiSmokeWeed.mModel = P_AMB_JOINT_01
		
ENDPROC

/// PURPOSE:
///    Sets all models as no longer needed, removes all anim dicts, removes all vehicle recordings etc.
PROC UnloadEverything()

	SET_MODEL_AS_NO_LONGER_NEEDED(mpPrincess.mModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(mpDrugDealer.mModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(oiSmokeWeed.mModel)
	
	INT i
	
	FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(mSecurityPed[i].mModel)
	ENDFOR
	
	FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
		SET_MODEL_AS_NO_LONGER_NEEDED(mvSecurityCars[i].mModel)
	ENDFOR
ENDPROC

PROC DO_ANNOYING_SUBTITLE_SWITCH()
	
	TEXT_LABEL_23 rootDeal = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
	TEXT_LABEL_23 labelDeal
	
	IF NOT IS_STRING_NULL_OR_EMPTY(rootDeal)
		IF ARE_STRINGS_EQUAL(rootDeal,"PAP3_DEALER")	
		AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 0	
			IF iSubSwitch = 0
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPrincess.mPed) > 40
					iSubsDealer = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
					CPRINTLN(DEBUG_MISSION, "<CONVO> LEFT AREA SET SUBS OFF")
					iSubSwitch = 1
					iRetrigger = 1
				ENDIF
			ELIF iSubSwitch = 1
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPrincess.mPed) <= 40
					iSubsDealer = ENUM_TO_INT(DISPLAY_SUBTITLES)
					CPRINTLN(DEBUG_MISSION, "<CONVO> ENTER AREA SET SUBS ON")
					iSubSwitch = 0
					iRetrigger = 1
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iRetrigger = 1
		labelDeal = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		IF NOT IS_STRING_NULL_OR_EMPTY(labelDeal)
			CPRINTLN(DEBUG_MISSION, "<CONVO> LABEL IS NOT NULL")
			IF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_1")	
				labelRetrig = "PAP3_DEALER_2"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_2")	
				labelRetrig = "PAP3_DEALER_3"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_3")	
				labelRetrig = "PAP3_DEALER_4"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_4")	
				labelRetrig = "PAP3_DEALER_5"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_5")	
				labelRetrig = "PAP3_DEALER_6"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_6")	
				labelRetrig = "PAP3_DEALER_7"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_7")	
				labelRetrig = "PAP3_DEALER_8"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_8")	
				labelRetrig = "PAP3_DEALER_9"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_9")	
				labelRetrig = "PAP3_DEALER_10"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_10")	
				labelRetrig = "PAP3_DEALER_11"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_11")	
				labelRetrig = "PAP3_DEALER_12"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_12")	
				labelRetrig = "PAP3_DEALER_13"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_13")	
				labelRetrig = "PAP3_DEALER_14"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_14")	
				labelRetrig = "PAP3_DEALER_15"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_15")	
				labelRetrig = "PAP3_DEALER_16"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_16")	
				labelRetrig = "PAP3_DEALER_17"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_17")	
				labelRetrig = "PAP3_DEALER_18"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_18")	
				labelRetrig = "PAP3_DEALER_19"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_19")	
				labelRetrig = "PAP3_DEALER_20"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_20")
				labelRetrig = "PAP3_DEALER_21"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_21")
				labelRetrig = "PAP3_DEALER_22"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_22")
				labelRetrig = "PAP3_DEALER_23"
			ELIF ARE_STRINGS_EQUAL(labelDeal,"PAP3_DEALER_23")	
				CPRINTLN(DEBUG_MISSION, "<CONVO> LAST LINE SO JUST BAIL")
				iRetrigger = 4
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION()
			CPRINTLN(DEBUG_MISSION, "<CONVO> KILL")
			iRetrigger = 2
		ELSE
			CPRINTLN(DEBUG_MISSION, "<CONVO> CHECKED LABEL IS NULL OR EMPTY")
		ENDIF
	ELIF iRetrigger = 2
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			CPRINTLN(DEBUG_MISSION, "<CONVO> LINE FINISHED AFTER KILL")
			IF NOT IS_STRING_NULL_OR_EMPTY(labelRetrig)
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "pap3bau", "PAP3_DEALER", labelRetrig, CONV_PRIORITY_HIGH,INT_TO_ENUM(enumSubtitlesState,iSubsDealer))
					CPRINTLN(DEBUG_MISSION, "<CONVO> RETRIGGER")
					iRetrigger = 3
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "<CONVO> RETRIGGER LABEL IS NULL - BAIL")
				iRetrigger = 3
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_PRINCESS_SMOKING_IDLES()

	IF HAS_ANIM_DICT_LOADED("amb@world_human_smoking@female@idle_a")
	AND HAS_ANIM_DICT_LOADED("amb@world_human_smoking@female@base")
		IF GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK	
		//IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@base","base")	
			IF GET_GAME_TIMER() > iTimerSmokeIdle + 10000
				IF NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a")
				AND NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b")
				AND NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c")	
					//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mpPrincess.mPed)
					INT iRand = GET_RANDOM_INT_IN_RANGE(0,3)
					IF iRand = 0	
						TASK_PLAY_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY)
					ELIF iRand = 1
						TASK_PLAY_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY)
					ELSE
						TASK_PLAY_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY)
					ENDIF
					iTimerSmokeIdle = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a")
			AND NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b")
			AND NOT IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c")
				//TASK_PLAY_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@base","base",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
				//TASK_START_SCENARIO_IN_PLACE(mpPrincess.mPed,"WORLD_HUMAN_SMOKING")
			ENDIF
		ENDIF
	ENDIF
			//OVERRIDE_TASKED_SCENARIO_BASE_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a")
			//OVERRIDE_TASKED_SCENARIO_BASE_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b")
			//OVERRIDE_TASKED_SCENARIO_BASE_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c")
			//OVERRIDE_TASKED_SCENARIO_BASE_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@base","base")

ENDPROC

FUNC BOOL IS_PRINCESS_TAKING_A_TOKE()

	/*
	IF IS_PED_UNINJURED(mpPrincess.mPed)	
		IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a")
			IF GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a") > 0.255
			AND GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a") < 0.848
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b")
			IF GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b") > 0.527
			AND GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b") < 0.746
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c")	
			IF GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c") > 0.060
			AND GET_ENTITY_ANIM_CURRENT_TIME(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c") < 0.209
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	*/
	//IF GET_DISTANCE_BETWEEN_ENTITIES(mpPrincess.mPed,PLAYER_PED_ID()) < 15
	//	RETURN TRUE
	//ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(issDeal)
		IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.803
		AND GET_SYNCHRONIZED_SCENE_PHASE(issDeal) < 0.871
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DO_PRINCESS_SHOUTING()

	IF iPrincessShoutLines < 2	
		IF GET_GAME_TIMER() > iPrincessShoutTimer + 10000	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPrincess.mPed) < 50
					IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SHOCK", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						++iPrincessShoutLines
						iPrincessShoutTimer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,4000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_PED_NEAR_MIDDLE_OF_SCREEN(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		INT i = GET_PED_BONE_INDEX(ped, BONETAG_SPINE)
		FLOAT fScreenX
		FLOAT fScreenY
		IF i <> -1
			VECTOR pos = GET_WORLD_POSITION_OF_ENTITY_BONE(ped,i)			

			GET_SCREEN_COORD_FROM_WORLD_COORD(pos, fScreenX, fScreenY)
			
			IF fScreenX > 0.2 
				AND fScreenX < 0.8
				AND fScreenY > 0.2 
				AND fScreenY < 0.8
				CPRINTLN(DEBUG_MISSION, "TK PRINCESS SPINE NEAR MIDDLE OF THE SCREEN")
				RETURN TRUE
			ENDIF							

		ENDIF
	ENDIF
	CPRINTLN(DEBUG_MISSION, "TK PRINCESS SPINE NOT NEAR MIDDLE OF THE SCREEN")
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_PRINCESS_NOT_OCCLUDED_FROM_ROOF()	
	IF NOT IS_ENTITY_OCCLUDED(mpPrincess.mPed)
	//AND GET_FOCUS_PED_ON_SCREEN(100,BONETAG_HEAD,0.42,0.26,0.01,50,0.2,BONETAG_R_FOOT,BONETAG_L_FOOT) = mpPrincess.mPed	
		IF GET_FOCUS_PED_ON_SCREEN(100,BONETAG_HEAD,0.8,0.8,0.25,50,0.8,BONETAG_R_FOOT,BONETAG_L_FOOT) = mpPrincess.mPed	
		OR GET_FOCUS_PED_ON_SCREEN(100,BONETAG_HEAD,0.8,0.8,0.25,50,0.8,BONETAG_R_FOOT,BONETAG_L_FOOT) = mpDrugDealer.mPed	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PHOTO_OK()

	//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1071.160889,-786.458801,57.465942>>, <<1069.558105,-785.423584,58.262592>>, 1.2) //goes weird on this corner
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1060.735962,-776.372925,56.762596>>, <<1087.982544,-792.096497,59.312592>>, 8.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1051.625977,-768.425110,56.185852>>, <<1104.971436,-768.815674,60.036358>>, 28.500000)	
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1057.152832,-785.157837,59.006058>>, <<1057.152832,-782.090332,60.706692>>, 1.950000)	//Shelf thing B*1988982
		RETURN FALSE
	ENDIF
	
	IF IS_PED_UNINJURED(mpPrincess.mPed)	
		IF NOT IS_PED_NEAR_MIDDLE_OF_SCREEN(mpPrincess.mPed)	
			RETURN FALSE
		ENDIF
		IF IS_PED_UNINJURED(mpDrugDealer.mPed)		
		AND IS_ENTITY_ON_SCREEN(mpPrincess.mPed)	
		AND IS_ENTITY_ON_SCREEN(mpDrugDealer.mPed)	
		AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(mpPrincess.mPed),1)	
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1095.333984,-783.023560,61.678375>>, <<1078.833618,-783.144409,64.366570>>, 15.750000)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(),mpPrincess.mPed,LOS_FLAGS_BOUNDING_BOX)	
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(),mpDrugDealer.mPed)	
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ELSE	
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(),mpPrincess.mPed,LOS_FLAGS_BOUNDING_BOX)	
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(),mpDrugDealer.mPed)	
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(mpPrincess.mPed,PLAYER_PED_ID(),50)  //34
						bTooFarAway = TRUE
						RETURN FALSE
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
					
ENDFUNC

FUNC BOOL ALL_GUARDS_LOST()	

	INT i
	
	FOR i = 0 TO  NUM_OF_SECURITY_PEDS -1
		IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mSecurityPed[i].mPed,200)
				IF DOES_BLIP_EXIST(mSecurityPed[i].mBlip)
					SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
					SAFE_DELETE_PED(mSecurityPed[i].mPed)
					IF i = 2
						SAFE_RELEASE_VEHICLE(mvSecurityCars[1].mVehicle)
					ENDIF
					++iSecurityEvaded
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(mSecurityPed[i].mBlip)
					IF IS_PED_IN_ANY_VEHICLE(mSecurityPed[i].mPed)
						SET_BLIP_SCALE(mSecurityPed[i].mBlip,BLIP_SIZE_VEHICLE)
					ELSE
						SET_BLIP_SCALE(mSecurityPed[i].mBlip,BLIP_SIZE_PED)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(mSecurityPed[i].mBlip)
				SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
				++iSecurityDead
				++iSecurityEvaded
			ENDIF
		ENDIF
	ENDFOR
	
	IF iSecurityEvaded >= iSecurityBlipped
		RETURN TRUE
	ENDIF
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Makes the PED passed in flee from the player
/// PARAMS:
///    mPedFlee - Makes this ped flee from the player. 
PROC MAKE_PED_FLEE(PED_INDEX mPedFlee, BOOL OnFoot = FALSE)

	IF IS_PED_UNINJURED(mPedFlee)
		IF OnFoot = FALSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			TASK_SMART_FLEE_PED(mPedFlee, PLAYER_PED_ID(), 200, -1,FALSE)	
			SET_PED_KEEP_TASK(mPedFlee, TRUE)
		ELSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			TASK_SMART_FLEE_PED(mPedFlee, PLAYER_PED_ID(), 200, -1,TRUE)	
			SET_PED_KEEP_TASK(mPedFlee, TRUE)
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

/// PURPOSE:
///    Cleanup that needs to be done as soon as mission is passed or failed. Blip removal etc
/// PARAMS:
///    bClearText - Should objective text be cleared?
PROC MissionCleanup(bool bClearText = TRUE)

	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
	ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF
	
	IF bClearText = TRUE // clear all text
		CLEAR_PRINTS()
	ENDIF
	CLEAR_HELP(TRUE)
	
ENDPROC

/// PURPOSE:
///    Did the player bypass the contact
FUNC BOOL DID_PLAYER_GO_STRAIGHT_TO_PRINCESS()	
	IF IS_PED_UNINJURED(mpPrincess.mPed)
	AND IS_ENTITY_IN_RANGE_ENTITY(mpPrincess.mPed,PLAYER_PED_ID(),55)
	AND IS_ENTITY_ON_SCREEN(mpPrincess.mPed)
	AND IS_SPHERE_VISIBLE(GET_WORLD_POSITION_OF_ENTITY_BONE(mpPrincess.mPed,GET_PED_BONE_INDEX(mpPrincess.mPed, BONETAG_HEAD)),0.1)
	AND NOT IS_ENTITY_OCCLUDED(mpPrincess.mPed)	
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.023804,-758.650391,54.945763>>, <<1054.395996,-758.632629,64.537048>>, 47.500000)	
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1046.049805,-754.827087,56.041855>>, <<1144.919678,-755.214111,59.983875>>, 20.750000) 	//road	
		RETURN TRUE
	ENDIF		
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1051.437500,-791.649353,56.685829>>, <<1114.934814,-791.440918,66.168022>>, 15.500000)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the new mission stage and initialises the substate
/// PARAMS:
///    eNewStage - The mission stage to change to
PROC SetStage(MISSION_STAGE eNewStage)
	bMainObjectiveDisplayed = FALSE
	missionStage = eNewStage // Setup new mission state
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///    Sets the fail reason and mission stage to be fail fade out.
/// PARAMS:
///    eNewFailReason - The fail reason
PROC SET_FAIL_REASON(FAIL_REASON eNewFailReason)
	
	bMissionFailed = TRUE
	eFailReason = eNewFailReason
	SETSTAGE(MS_MISSION_FAILED)
	
ENDPROC

/// PURPOSE:
///    Forces the drug deal peds to drive away
PROC ForceDriveAway()
	IF IS_PED_UNINJURED(mpPrincess.mPed) AND IS_ENTITY_ALIVE(mvSecurityCars[0].mVehicle)
		/*
		IF NOT IsPedPerformingTask(mpPrincess.mPed, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) AND NOT IsPedPerformingTask(mpPrincess.mPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
			FREEZE_ENTITY_POSITION(mvSecurityCars[0].mVehicle, FALSE)
			IF IS_PED_IN_VEHICLE(mpPrincess.mPed, mvSecurityCars[0].mVehicle)
				TASK_VEHICLE_DRIVE_WANDER(mpPrincess.mPed, mvSecurityCars[0].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
			ELSE
				SEQUENCE_INDEX siDriveOff
				OPEN_SEQUENCE_TASK(siDriveOff)
					TASK_ENTER_VEHICLE(NULL, mvSecurityCars[0].mVehicle)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
					//TASK_VEHICLE_DRIVE_WANDER(NULL, mvSecurityCars[0].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
				CLOSE_SEQUENCE_TASK(siDriveOff)
				TASK_PERFORM_SEQUENCE(mpPrincess.mPed, siDriveOff)
				CLEAR_SEQUENCE_TASK(siDriveOff)
				ePrincessState = SP_WANDER
			ENDIF
		ENDIF
		*/
		IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
		AND IS_PED_UNINJURED(mpPrincess.mPed)	
			IF IS_PED_IN_VEHICLE(mSecurityPed[0].mPed, mvSecurityCars[0].mVehicle)
				IF GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> WAITING_TO_START_TASK
				AND GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
					IF IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					AND IS_PED_IN_VEHICLE(mSecurityPed[1].mPed,mvSecurityCars[0].mVehicle)	
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mSecurityPed[0].mPed,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
					ENDIF
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
					TASK_ENTER_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mSecurityPed[2].mPed) AND IS_ENTITY_ALIVE(mvSecurityCars[1].mVehicle)
			IF IS_PED_IN_VEHICLE(mSecurityPed[2].mPed, mvSecurityCars[1].mVehicle)
				IF GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
					IF IS_PED_FACING_PED(mSecurityPed[2].mPed,mpPrincess.mPed,70)
						IF GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> WAITING_TO_START_TASK	
							//SET_ROADS_IN_AREA(<<1109.490112,-778.548096,58.762684>>,<<3.750000,13.500000,2.250000>>,TRUE)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
		/*
		IF NOT IsPedPerformingTask(mSecurityPed[2].mPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
			IF IS_PED_IN_VEHICLE(mSecurityPed[2].mPed, mvSecurityCars[1].mVehicle)
				IF IS_PED_UNINJURED(mpPrincess.mPed)
					SEQUENCE_INDEX siDriveOff
					OPEN_SEQUENCE_TASK(siDriveOff)
						TASK_PAUSE(NULL, 3000)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, mvSecurityCars[1].mVehicle, mpPrincess.mPed, MISSION_FOLLOW, 20.0, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
					CLOSE_SEQUENCE_TASK(siDriveOff)
					TASK_PERFORM_SEQUENCE(mSecurityPed[2].mPed, siDriveOff)
					CLEAR_SEQUENCE_TASK(siDriveOff)
				ELSE
					TASK_VEHICLE_DRIVE_WANDER(mSecurityPed[2].mPed, mvSecurityCars[1].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
				ENDIF
			ELSE
				SEQUENCE_INDEX siDriveOff
				OPEN_SEQUENCE_TASK(siDriveOff)
					TASK_ENTER_VEHICLE(NULL, mvSecurityCars[1].mVehicle)
					IF IS_PED_UNINJURED(mpPrincess.mPed)
						TASK_PAUSE(NULL, 3000)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, mvSecurityCars[1].mVehicle, mpPrincess.mPed, MISSION_FOLLOW, 20.0, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
					ELSE
						TASK_VEHICLE_DRIVE_WANDER(NULL, mvSecurityCars[1].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
					ENDIF
				CLOSE_SEQUENCE_TASK(siDriveOff)
				TASK_PERFORM_SEQUENCE(mSecurityPed[2].mPed, siDriveOff)
				CLEAR_SEQUENCE_TASK(siDriveOff)
			ENDIF
		ENDIF
		*/
		
		IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
		AND IS_PED_IN_ANY_VEHICLE(mSecurityPed[0].mPed)	
			IF IS_ENTITY_IN_ANGLED_AREA(mSecurityPed[0].mPed, <<1158.520142,-759.373901,56.322784>>, <<1116.868652,-759.690796,61.706322>>, 13.750000)
				IF GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					SET_PED_KEEP_TASK(mSecurityPed[0].mPed,TRUE)
					TASK_VEHICLE_MISSION_PED_TARGET(mSecurityPed[0].mPed,GET_VEHICLE_PED_IS_IN(mSecurityPed[0].mPed),PLAYER_PED_ID(),MISSION_FLEE,25.0,DRIVINGMODE_AVOIDCARS_RECKLESS,20.0,20.0)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)	
			AND IS_ENTITY_ALIVE(mvSecurityCars[1].mVehicle)	
				IF IS_ENTITY_IN_ANGLED_AREA(mSecurityPed[2].mPed, <<1158.520142,-759.373901,56.322784>>, <<1116.868652,-759.690796,61.706322>>, 13.750000)	
					IF GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
						SET_PED_KEEP_TASK(mSecurityPed[2].mPed,TRUE)
						TASK_VEHICLE_MISSION_PED_TARGET(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,mpPrincess.mPed,MISSION_ESCORT_REAR,25,DRIVINGMODE_AVOIDCARS,20,10)
						IF bPhotoSent = FALSE
							SET_FAIL_REASON(FAIL_PHOTO_LOST)
						ENDIF
						ePrincessState = SP_WANDER
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up the script
/// PARAMS:
///    bClearText - Should objective text be cleared?
PROC Script_Cleanup(BOOL bClearText = TRUE)
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required")
		MissionCleanup(bClearText)
		
		REMOVE_SCENARIO_BLOCKING_AREA(sbiChicoRear)
		REMOVE_SCENARIO_BLOCKING_AREA(sbiVehAttractor)
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		
		UnloadEverything()
		
		ForceDriveAway()
		
		SAFE_RELEASE_VEHICLE(mvDealerCar.mVehicle)
		SAFE_RELEASE_VEHICLE(mvClimbingTruck.mVehicle)
	
		INT i
		
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
			SAFE_RELEASE_PED(mSecurityPed[i].mPed)
		ENDFOR
		
		FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
			SAFE_RELEASE_VEHICLE(mvSecurityCars[i].mVehicle)
		ENDFOR
		
		SAFE_RELEASE_PED(mpPrincess.mPed)
		SAFE_RELEASE_PED(mpDrugDealer.mPed)
		
		//Delete text message from players phone inbox
		DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("PAP3_16")
		
	ENDIF
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE) //Cleanup the scene created by the launcher
	
	REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES(FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Passes the mission
PROC Script_Passed()
//	CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_UNLOGGED_SMALL_ACTION, 500) // Initially requested on B*1237043 - Removed on request as B*1317037
	MissionCleanup()
	Random_Character_Passed(CP_RAND_C_PAP3)
	Script_Cleanup()
ENDPROC


/// PURPOSE:
///    Resets the gameplay camera behind the player
PROC ResetCamBehindPlayer()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
ENDPROC

/// PURPOSE:
///    toggles cutscene mode [sets scene safe, toggles radar etc]
/// PARAMS:
///    bOn - Turn cutscene mode on or off
PROC SetCutsceneMode(BOOL bOn)

	if bOn = TRUE
		// start of cutscene
		RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>)
	ELSE
		// end of cutscene
		RC_END_CUTSCENE_MODE()
		ResetCamBehindPlayer() // go back to gameplay camera
		SAFE_FADE_SCREEN_IN_FROM_BLACK()
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates ped and sets heading
/// PARAMS:
///    mMissionPed - The ped to create
///    bUnloadModel - Should we unload the model after creating the ped
PROC CreateMissionPed(MISSION_PED &mMissionPed, BOOL bUnloadModel = TRUE)
	if HAS_MODEL_LOADED(mMissionPed.mModel)
		mMissionPed.mPed = CREATE_PED(PEDTYPE_MISSION, mMissionPed.mModel, mMissionPed.vStartPos, mMissionPed.fStartHeading)
		if bUnloadModel = TRUE
			SET_MODEL_AS_NO_LONGER_NEEDED(mMissionPed.mModel)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates vehicle and sets heading and colours
/// PARAMS:
///    mMissionVehicle - The vehicle to create
///    bRemoveModel - Should we remove the model after creating the vehicle
PROC CreateMissionVehicle(MISSION_VEHICLE &mMissionVehicle, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionVehicle.mModel)
		mMissionVehicle.mVehicle = CREATE_VEHICLE(mMissionVehicle.mModel, mMissionVehicle.vStartPos, mMissionVehicle.fStartHeading)
	ENDIF

	if bRemoveModel = TRUE
		SET_MODEL_AS_NO_LONGER_NEEDED(mMissionVehicle.mModel)
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates props and sets heading
/// PARAMS:
///    mMissionProp - The prop to create
///    bRemoveModel - Should we remove the model after creating the prop
PROC CreateMissionProp(MISSION_PROP &mMissionProp, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionProp.mModel)
		mMissionProp.mObject = CREATE_OBJECT(mMissionProp.mModel, mMissionProp.vStartPos)
		if DOES_ENTITY_EXIST(mMissionProp.mObject)
			SET_ENTITY_HEADING(mMissionProp.mObject,mMissionProp.fStartHeading)
		ENDIF
	ENDIF

	if bRemoveModel = TRUE
		SET_MODEL_AS_NO_LONGER_NEEDED(mMissionProp.mModel)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player has tried to enter the left entrance where security are.
/// RETURNS:
///    TRUE if the player is in area
FUNC BOOL HAS_PLAYER_TRIED_TO_ENTER()

	IF IS_POINT_IN_POLY_2D(tpBackAwayArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks to see if the princess has left the area.
/// RETURNS:
///    TRUE if the princess has left
FUNC BOOL HAS_PRINCESS_LEFT_THE_AREA()

	IF IS_ENTITY_ALIVE(mpPrincess.mPed)
		IF IS_POINT_IN_POLY_2D(tpLeaveArea, GET_ENTITY_COORDS(mpPrincess.mPed))
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///   Checks if player is inside the spotted area 
/// RETURNS:
///    True - If player is inside the area, false otherwise
FUNC BOOL IS_PLAYER_INSIDE_SPOT_AREA()

	IF IS_POINT_IN_POLY_2D(tpSpotArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF vPlayerPos.z < 61.0
			CPRINTLN(DEBUG_MISSION, "PLAYER INSIDE SPOT AREA")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks to see if player has left the area
/// RETURNS:
///    TRUE if the player has left the area, false otherwise.
FUNC BOOL IS_PLAYER_OUT_LEAVE_AREA()

	IF IS_POINT_IN_POLY_2D(tpLeaveArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Checks if the player is in the alley area
/// RETURNS:
///    TRUE if the player is in the alley area, FALSE otherwise
FUNC BOOL IS_PLAYER_IN_ALLEY_NOTICE_AREA()

	IF IS_POINT_IN_POLY_2D(tpAlleyArea, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Makes the player automatically crouch when they approach the alley area, allows them to stop crouching if they really want to
PROC HANDLE_AUTO_CROUCHING()
	VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF IS_POINT_IN_POLY_2D(tpCrouchArea, vPlayer)
	AND vPlayer.z < 61	
		IF bForceCrouching
			IF NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),TRUE)
			ENDIF
			bForceCrouching = FALSE	// only force the player to crouch when the enter this area, if they want to stop crouching, that's their choice
		ENDIF
	ELSE
		IF bForceCrouching = FALSE
			bForceCrouching = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if any security can see the player
/// RETURNS:
///    True if they can see him, false otherwise
FUNC BOOL CAN_SECURITY_SEE_PLAYER()
	
	INT i
	
	FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[i].mPed,TRUE)
			IF CAN_PED_SEE_HATED_PED(mSecurityPed[i].mPed,PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "I CAN SEE THE PLAYER")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
	
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is unarmed - Including objects
///    Used to see if PEDS should attack player
/// RETURNS:
///    TRUE - If player is unarmed, false otherwise
FUNC BOOL IS_PLAYER_UNARMED()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED 
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT //CHECK FOR MOBILE PHONE AS THIS WILL TRIGGER
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Checks if the player can see a point
/// PARAMS:
///    vPos - The point the player wants to see
///    fRadius - A radius around the point to check if it's visible
///    distance - The maximum distance for the point to be judged visible
/// RETURNS:
///    TRUE if the player can see the point, FALSE otherwise
FUNC BOOL CAN_PLAYER_SEE_POINT(VECTOR vPos, FLOAT fRadius = 5.0, INT distance = 120)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	      IF GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(PLAYER_PED_ID()), vPos) <= distance
	            IF IS_SPHERE_VISIBLE(vPos, fRadius)      
	                  RETURN TRUE
	            ENDIF
	      ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    When a photo has been taken, check if a certain ped is visible
/// PARAMS:
///    _Ped - The ped we want to check
/// RETURNS:
///    TRUE if the ped is visible, FALSE otherwise
FUNC BOOL MONITOR_PHOTO(PED_INDEX _Ped)
	IF DID_SIGHTTEST_PASS(iSightTestID)
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), _Ped) < 500
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if the princess is in a photo of another ped
/// PARAMS:
///    piTest - The ped who the princess might be near
/// RETURNS:
///    TRUE if the princess is in the photo
FUNC BOOL IS_PRINCESS_IN_PHOTO(PED_INDEX piTest)
	INT iCount = 0
	INT i = 0
	BOOL bPrincessPhoto = FALSE
	IF IS_PED_UNINJURED(piTest)
		PED_INDEX piNearbyGuys[25]
		iCount = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), piNearbyGuys)
		REPEAT iCount i
			IF IS_PED_UNINJURED(piNearbyGuys[i])
				IF piNearbyGuys[i] = mpPrincess.mPed AND IS_ENTITY_ON_SCREEN(piNearbyGuys[i]) AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), piNearbyGuys[i])
				AND GET_DISTANCE_BETWEEN_ENTITIES(piTest, piNearbyGuys[i]) < 5
					bPrincessPhoto = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN bPrincessPhoto
ENDFUNC

/// PURPOSE:
///    Initialises mission variables
PROC InitVariables()
	CPRINTLN(DEBUG_MISSION, "init variables ************************")
	
	bDealersTempEventsUnblocked = FALSE
	
	INIT_ARRAYS()
	
	//SET BOOLS
	bMainObjectiveDisplayed 	= 	FALSE
	bPrincessSceneCreated 		= 	FALSE
	b_Correct_Pic_Taken			=	FALSE
	bPhotoSent					=	FALSE
	bSecurityAttacking			=	FALSE
	bSecurityNoticedPlayer      =	FALSE
	bConversationActive			=	FALSE
	bLeavingDrugScene			=	FALSE
	bPlayerSpotted				=	FALSE
	
	iIndividualLeaveTimer = 0
	
	REQUEST_ANIM_DICT("amb@world_human_smoking@female@idle_a")
	REQUEST_ANIM_DICT("amb@world_human_smoking@female@base")
	REQUEST_ANIM_DICT(ANIM_DICT)
	REQUEST_ANIM_DICT(DEALER_ANIM_DICT)
	REQUEST_ANIM_DICT(DEALER_ANIM_DICT_2)
	
	
	//ADD_RELATIONSHIP_GROUP("SecurityGroup", relHatePlayer)
	ADD_RELATIONSHIP_GROUP("PrincessGroup", relPrincess)
	ADD_RELATIONSHIP_GROUP("DealersGroup", relDealers)
	
	//ADD BEVERLY CONTACT
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)
	
	//AREA TEST POLYS
	
	//SPOTTED AREA
	OPEN_TEST_POLY(tpSpotArea)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1073.5927, -787.5071, 57.2626 >>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1071.3456, -796.4704, 57.3459 >>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1074.5679, -798.8521, 57.3658 >>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1091.1027, -798.9657, 57.2626 >>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1101.3662, -794.2939, 57.2626 >>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1114.6600, -794.1965, 57.3977 >>) 
	ADD_TEST_POLY_VERT(tpSpotArea, <<1114.48, -781.67, 58.42>>)
	ADD_TEST_POLY_VERT(tpSpotArea, <<1106.92, -781.76, 58.26>>)
	ADD_TEST_POLY_VERT(tpSpotArea, << 1096.1490, -782.6533, 57.2632 >>)
	CLOSE_TEST_POLY(tpSpotArea)
	
	//LEAVE AREA
	OPEN_TEST_POLY(tpLeaveArea)
	ADD_TEST_POLY_VERT(tpLeaveArea, << 1051.8142, -777.9432, 57.3783 >>)
	ADD_TEST_POLY_VERT(tpLeaveArea, << 1042.5559, -810.4073, 56.8677 >>)
	ADD_TEST_POLY_VERT(tpLeaveArea, << 1121.7858, -815.4036, 55.5302 >>)
	ADD_TEST_POLY_VERT(tpLeaveArea, << 1124.2711, -771.0333, 56.7566 >>)
	ADD_TEST_POLY_VERT(tpLeaveArea, <<1103.765381,-766.337402,56.663002>>)
	ADD_TEST_POLY_VERT(tpLeaveArea, <<1072.296265,-766.390625,56.705795>>)
	CLOSE_TEST_POLY(tpLeaveArea)
	
	OPEN_TEST_POLY(tpBackAwayArea)
	ADD_TEST_POLY_VERT(tpBackAwayArea, << 1104.4590, -775.0743, 57.3526 >>)
	ADD_TEST_POLY_VERT(tpBackAwayArea, << 1114.6810, -775.0476, 57.3626 >>)
	ADD_TEST_POLY_VERT(tpBackAwayArea, << 1114.6786, -781.0639, 57.4212 >>)
	ADD_TEST_POLY_VERT(tpBackAwayArea, << 1103.5934, -782.3363, 57.2626 >>)
	CLOSE_TEST_POLY(tpBackAwayArea)	
	
	OPEN_TEST_POLY(tpAlleyArea)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1070.7058, -786.0175, 57.2632 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1063.4452, -781.0466, 57.2632 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1051.7117, -785.5277, 57.3895 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1051.7520, -797.8251, 57.3918 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1055.2402, -798.9913, 57.2626 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1069.9708, -796.5669, 57.3245 >>)
	ADD_TEST_POLY_VERT(tpAlleyArea, << 1075.0953, -793.6094, 57.3137 >>)
	CLOSE_TEST_POLY(tpAlleyArea)
	
	COPY_EXPANDED_POLY(tpCrouchArea, tpAlleyArea, 5.0)
	
	// other requests here
	#IF IS_DEBUG_BUILD // stage skipping
		bFinishedSkipping = TRUE
	
		mSkipMenu[0].sTxtLabel = "Initial phone call"
		mSkipMenu[Z_SKIP_PHONE_CALL].sTxtLabel = "Go to location"
		mSkipMenu[2].sTxtLabel = "Meet contact"
		mSkipMenu[Z_SKIP_AT_PRINCESS_LOCATION].sTxtLabel = "Take photo"
		mSkipMenu[4].sTxtLabel = "End phone call"
		
	#ENDIF
	
	REQUEST_ADDITIONAL_TEXT("PAP3BAU", MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("PAP3",MISSION_TEXT_SLOT)
ENDPROC

/// PURPOSE:
///    Checks if the mission has been initialised
/// PARAMS:
///    bWait - Should we wait for the mission to be initialised
/// RETURNS:
///    TRUE if the mission is initialised, FALSE otherwise
FUNC BOOL IS_MISSION_INITIALISED(BOOL bWait = FALSE)
	IF bWait	// wait for everything to be loaded, used with debug skips
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		RETURN TRUE
	ELSE
		IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT) AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Deletes everything regardless
PROC DELETE_EVERYTHING()

	SAFE_DELETE_PED(mpPrincess.mPed)
	
	SAFE_DELETE_PED(mpDrugDealer.mPed)
	
	SAFE_DELETE_PED(mpDealerContact.mPed)
	
	INT i
	
	FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		SAFE_DELETE_PED(mSecurityPed[i].mPed)
		SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
	ENDFOR
	
	FOR i = 0 TO NUM_OF_DEALER_GOONS- 1
		SAFE_DELETE_PED(mpDealerGoons[i].mPed)
		
	ENDFOR
	
	FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
		SAFE_DELETE_VEHICLE(mvSecurityCars[i].mVehicle)
	ENDFOR
	
	SAFE_DELETE_VEHICLE(mvDealerCar.mVehicle)
	SAFE_DELETE_VEHICLE(mvClimbingTruck.mVehicle)
	
	SAFE_REMOVE_BLIP(biMissionBlip)
	
	SAFE_DELETE_OBJECT(oiSmokeWeed.mObject)

	//REMOVE_RELATIONSHIP_GROUP(relHatePlayer)
	REMOVE_RELATIONSHIP_GROUP(relPrincess)
	REMOVE_RELATIONSHIP_GROUP(relDealers)
	
ENDPROC

/// PURPOSE:
///    Deletes all entities and unloads everything to put the mission back to its starting state
PROC ResetMission()
	
	
	CLEAR_PRINTS()
	
	DELETE_EVERYTHING()
	
	//Set player wanted level to 0
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF

	UnloadEverything()
	
	InitVariables()
	IS_MISSION_INITIALISED(TRUE)
	
	CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
	
	// put peds etc in start positions
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1039.0956, -543.0302, 59.4935 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),171.0)
		ResetCamBehindPlayer()
	ENDIF
	
	SetStage(MS_INITIAL_PHONE)
	
ENDPROC

/// PURPOSE:
///    Skips the current stage
PROC SkipStage()
	
	// stop player and buddy from doing whatever they were doing before skip
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
	ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
	
	SWITCH missionStage
	
		CASE MS_INITIAL_PHONE
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					KILL_PHONE_CONVERSATION()
				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1039.9441, -536.3055, 60.0634 >>) //TELEPORT PLAYER TO BE FACING CORRECT WAY
				SET_ENTITY_HEADING(PLAYER_PED_ID(),173.0)
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
	
		CASE MS_GO_TO_LOCATION
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1078.4886, -767.2433, 56.7781 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),141.0)
			ENDIF
			ResetCamBehindPlayer()
		BREAK
		
		CASE MS_MEET_CONTACT
			IF eSubStage = SS_CLEANUP
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_PHONE_CONVERSATION()
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1071.9421, -773.5490, 57.0645 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),172.0769)
				ResetCamBehindPlayer()
			ENDIF
		BREAK
		
		CASE MS_TAKE_PHOTO
			b_Correct_Pic_Taken = TRUE
			eSubStage = SS_CLEANUP
		BREAK
		
		CASE MS_LEAVE_AREA
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1057.9576, -746.6598, 57.0441 >>)
			ResetCamBehindPlayer()
		BREAK
		
		CASE MS_END_PHONE_CALL
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					KILL_PHONE_CONVERSATION()
				ENDIF
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE MS_ESCAPE_SECURITY
			INT i
			FOR i = 0 TO NUM_OF_SECURITY_PEDS
				IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
					SET_ENTITY_HEALTH(mSecurityPed[i].mPed,5)
				ENDIF
			ENDFOR
		BREAK
					
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
/// PARAMS:
///    eNewStage - The stage to jump to
PROC JumpToStage(MISSION_STAGE eNewStage)

	IF missionStage = eNewStage  // skip current stage
		bFinishedSkipping = TRUE
		IF IS_REPLAY_IN_PROGRESS()
			VEHICLE_INDEX viTemp
			IF missionStage = MS_GO_TO_LOCATION
				CREATE_VEHICLE_FOR_REPLAY(viTemp, <<1025.7745, -556.0987, 58.9872>>, 178.7053, TRUE, FALSE, FALSE, TRUE, TRUE, ASTEROPE)   //<<1063.70, -528.63, 61.75>>, 99.04
			ELSE
				CREATE_VEHICLE_FOR_REPLAY(viTemp, <<1075.01, -764.27, 57.18>>, 269.48, FALSE, FALSE, FALSE, TRUE, TRUE, ASTEROPE)
			ENDIF
		ELSE
			IF IS_REPEAT_PLAY_ACTIVE()
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
			ENDIF
		ENDIF
		RC_END_Z_SKIP()
	ELSE
		SkipStage() 
	ENDIF
ENDPROC
	
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Goes back to the previous stage
    PROC PreviousStage()
        int iNewStage
        
	
        SWITCH missionStage   
			CASE MS_INITIAL_PHONE
				iNewStage = ENUM_TO_INT(MS_INITIAL_PHONE)
			BREAK
		
			CASE MS_ESCAPE_SECURITY
				 iNewStage = ENUM_TO_INT(MS_LEAVE_AREA)
			BREAK
			
			CASE MS_MEET_CONTACT
			CASE MS_TAKE_PHOTO
				 iNewStage = ENUM_TO_INT(MS_GO_TO_LOCATION)
			BREAK
			
			CASE MS_END_PHONE_CALL
				iNewStage = ENUM_TO_INT(MS_TAKE_PHOTO)
			BREAK
			
			DEFAULT
                iNewStage = ENUM_TO_INT(MissionStage)-1 
            BREAK
			
        ENDSWITCH
	
		
        IF iNewStage >-1 
            //we can skip to a previous stage
            eTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
            ResetMission()
            bFinishedSkipping = FALSE
            JumpToStage(eTargetStage)
        ENDIF
    ENDPROC

#ENDIF

/// PURPOSE:
///    Starts a Z skip
/// PARAMS:
///    iTargetStage - The stage to skip to
///    bResetMission - Should the mission be reset to its starting state
PROC Do_Z_Skip(INT iTargetStage, BOOL bResetMission = FALSE)

	RC_START_Z_SKIP()
	
	IF bResetMission = TRUE
		ResetMission()
	ENDIF
	
	eTargetStage = INT_TO_ENUM(MISSION_STAGE, iTargetStage)
	bFinishedSkipping = FALSE
	JumpToStage(eTargetStage)
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for Forced Pass or Fail or for mission skipping
	PROC DEBUG_Check_Debug_Keys()
		IF missionStage <> MS_MISSION_FAILED
			int iNewStage

			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) // Check for Pass
				WAIT_FOR_CUTSCENE_TO_STOP()
				Script_Passed()
			ENDIF

			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)) // Check for Fail
				WAIT_FOR_CUTSCENE_TO_STOP()
				SET_FAIL_REASON(FAIL_DEFAULT)
			ENDIF
			
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) // Check for Skip forward
				SkipStage()
			ENDIF
			
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) // Check for Skip backwards
				PreviousStage()
			ENDIF
			
			IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage) // Check for jumping to stage
				// iNewStage doesn't correspond to the mission stages, need to correct it here
				SWITCH iNewStage
					CASE 0	// Initial phone call
						iNewStage = ENUM_TO_INT(MS_INITIAL_PHONE)	// working
					BREAK
					CASE 1	// Go to location
						iNewStage = ENUM_TO_INT(MS_GO_TO_LOCATION)	// working
					BREAK
					CASE 2	// Meet contact
						iNewStage = ENUM_TO_INT(MS_MEET_CONTACT)	// working
					BREAK
					CASE 3	// Take photo
						iNewStage = ENUM_TO_INT(MS_TAKE_PHOTO)		// working
					BREAK
					CASE 4/*	// Leave Area
						iNewStage = ENUM_TO_INT(MS_LEAVE_AREA)		// broken
					BREAK
					CASE 5*/	// End phone call in the menu
						iNewStage = ENUM_TO_INT(MS_END_PHONE_CALL)	// working
					BREAK
				ENDSWITCH
				
				Do_Z_Skip(iNewStage, TRUE)
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF


// --------------------------------------------------------------
//     STAGE FUNCTIONS
// --------------------------------------------------------------

/// PURPOSE:
///    Makes sure Princess scene models have loaded
/// PARAMS:
///    bWaitForLoad - If true, will wait for all models to be loaded
/// RETURNS:
///    TRUE if all mdoels loaded, false otherwise. 
FUNC BOOL HAS_PRINCESS_SCENE_LOADED(BOOL bWaitForLoad = FALSE)
	
	//IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),mpPrincess.vStartPos,209)
	//	RETURN FALSE
	//ENDIF
		
	INT i
	
	//SECURITY MODELS
	FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		REQUEST_MODEL(mSecurityPed[i].mModel)
	ENDFOR
	
	//DEALER GOONS
	FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
		REQUEST_MODEL(mpDealerGoons[i].mModel)
	ENDFOR

	//PRINCESS MODEL
	REQUEST_MODEL(mpPrincess.mModel)
	
	//DRUG DEALER MODEL
	REQUEST_MODEL(mpDrugDealer.mModel)
	
	//SECURITY CAR MODELS
	FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
	
		REQUEST_MODEL(mvSecurityCars[i].mModel)
		
	ENDFOR
	
	//DRUG DEALER CAR MODEL
	REQUEST_MODEL(mvDealerCar.mModel)
	
	REQUEST_MODEL(mvClimbingTruck.mModel)
	
	//REQUEST VEHICLE RECORDINGS
	REQUEST_VEHICLE_RECORDING(103,"PAP3Security1")
	
	//REQUEST WAYPOINT RECORDINGS
	REQUEST_WAYPOINT_RECORDING("PAP3_Security1")
	
	//REQUEST PROPS
	REQUEST_MODEL(oiSmokeWeed.mModel)
	REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
	REQUEST_MODEL(PROP_CASH_PILE_02)
	REQUEST_ANIM_DICT("rcmpaparazzo_3big_1")
	
	IF bWaitForLoad = FALSE
	
        // HAVE ALL MODELS LOADED
        FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
            IF NOT HAS_MODEL_LOADED(mSecurityPed[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
	
			IF NOT HAS_MODEL_LOADED(mpDealerGoons[i].mModel)
                RETURN FALSE
            ENDIF
		
		ENDFOR
		
		IF NOT HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
            RETURN FALSE
        ENDIF
		
		IF NOT HAS_MODEL_LOADED(PROP_CASH_PILE_02)
            RETURN FALSE
        ENDIF
		
		IF NOT HAS_MODEL_LOADED(mpPrincess.mModel)
            RETURN FALSE
        ENDIF
		
		IF NOT HAS_MODEL_LOADED(mpDrugDealer.mModel)
            RETURN FALSE
        ENDIF
		
		IF NOT HAS_MODEL_LOADED(mvDealerCar.mModel)
            RETURN FALSE
        ENDIF
		
		IF NOT HAS_MODEL_LOADED(mvClimbingTruck.mModel)
            RETURN FALSE
        ENDIF
		
		FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
            IF NOT HAS_MODEL_LOADED(mvSecurityCars[i].mModel)
                RETURN FALSE
            ENDIF
        ENDFOR
		
		// HAVE RECORDINGS LOADED
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(103,"PAP3Security1")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(oiSmokeWeed.mModel)
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_3big_1")
			RETURN FALSE
		ENDIF
		
	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
		
			bEverythingLoaded = TRUE
              
            // HAVE ALL MODELS LOADED
            FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
                IF NOT HAS_MODEL_LOADED(mSecurityPed[i].mModel)
                    bEverythingLoaded = FALSE
                ENDIF
            ENDFOR
			
			FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
				IF NOT HAS_MODEL_LOADED(mpDealerGoons[i].mModel)
               	 	bEverythingLoaded = FALSE
           		ENDIF
			ENDFOR
			
			IF NOT HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
            	bEverythingLoaded = FALSE
        	ENDIF
		
			IF NOT HAS_MODEL_LOADED(PROP_CASH_PILE_02)
            	bEverythingLoaded = FALSE
       		ENDIF
			
			IF NOT HAS_MODEL_LOADED(mpPrincess.mModel)
           		bEverythingLoaded = FALSE
        	ENDIF
			
			IF NOT HAS_MODEL_LOADED(mpDrugDealer.mModel)
           		bEverythingLoaded = FALSE
       		ENDIF
			
			FOR i = 0 TO NUM_OF_SECURITY_CARS - 1
            	IF NOT HAS_MODEL_LOADED(mvSecurityCars[i].mModel)
               		bEverythingLoaded = FALSE
            	ENDIF
        	ENDFOR
			
			IF NOT HAS_MODEL_LOADED(mvDealerCar.mModel)
           		bEverythingLoaded = FALSE
        	ENDIF
			
			IF NOT HAS_MODEL_LOADED(mvClimbingTruck.mModel)
           		bEverythingLoaded = FALSE
        	ENDIF
			
			// HAVE RECORDINGS LOADED
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(103,"PAP3Security1")
				bEverythingLoaded = FALSE
			ENDIF
			
			IF NOT HAS_MODEL_LOADED(oiSmokeWeed.mModel)
				bEverythingLoaded = FALSE
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_3big_1")
				bEverythingLoaded = FALSE
			ENDIF
			
        ENDWHILE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Security notices the player, then react accordingly.
/// PARAMS:
///    bCanSeePlayer - Whether or not the Security ped can see the player
PROC NOTICE_PLAYER_EVENT(BOOL bCanSeePlayer)

	TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	IF bCanSeePlayer = TRUE	
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
			// Kill dealer convo if it's still ongoing
			IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		ENDIF
	ENDIF

	IF bSecurityNoticedPlayer = FALSE
	
		//WHAT SECURITY DO WHEN THEY SEE PLAYER
		
		//DRAW GUNS ON THE GOONS
		IF bCanSeePlayer = TRUE
			IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
				IF IS_PED_UNINJURED(mpDealerGoons[0].mPed)
					//GIVE_WEAPON_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
					TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,FALSE)
					eSecurityState[0] = SS_WAIT_AIM_AT_GOONS
					eGoonState[0] = GS_WAIT_AIM_AT_SECURITY
					iSecurityTimer = GET_GAME_TIMER() + 3000
					iGoonTimer = GET_GAME_TIMER() + 2000
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
				IF IS_PED_UNINJURED(mpDealerGoons[0].mPed)
					//GIVE_WEAPON_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
					TASK_GO_STRAIGHT_TO_COORD(mSecurityPed[0].mPed,vAlleyInvestigateLoc,3.0,DEFAULT_TIME_BEFORE_WARP,353.0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[0].mPed,TRUE)
					eSecurityState[0] = SS_INVESTIGATE
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
			IF IS_PED_UNINJURED(mpDealerGoons[1].mPed)
				//GIVE_WEAPON_TO_PED(mSecurityPed[1].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
				TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[1].mPed,PLAYER_PED_ID(),-1)
				eSecurityState[1] = SS_WAIT_AIM_AT_GOONS
				eGoonState[1] = GS_WAIT_AIM_AT_SECURITY
				iSecurityTimer = GET_GAME_TIMER() + 3000
				iGoonTimer = GET_GAME_TIMER() + 2000
			ENDIF
		ENDIF
		
		//MAKE ONE SECURITY GET INTO VEHICLE AND DRIVE AROUND BACK
		IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[1].mVehicle, VS_DRIVER)
				CLEAR_PED_TASKS(mSecurityPed[2].mPed)
				TASK_ENTER_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,3.0)
				eSecurityState[2] = SS_ENTER_VEHICLE
			ENDIF
		ENDIF
		
		//FRONT ENTRANCE SECURITY MOVE TO BACK
		IF IS_PED_UNINJURED(mSecurityPed[3].mPed)
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				//GIVE_WEAPON_TO_PED(mSecurityPed[3].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(mSecurityPed[3].mPed,<< 1094.2454, -791.3043, 57.2632 >>,PLAYER_PED_ID(),3.0,FALSE,9.0,15.0,TRUE)
				eSecurityState[3] = SS_AIM_AT_GOONS
			ENDIF
		ENDIF
		
		//MAKES PRINCESS FLEE
		IF IS_PED_UNINJURED(mpPrincess.mPed)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				// Kill dealer convo if it's still ongoing
				IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_DRIVER)
				IF NOT IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					CLEAR_PED_TASKS(mpPrincess.mPed)
					TASK_ENTER_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,3.0)
					ePrincessState = SP_ENTER_VEHICLE
				ENDIF
			ELSE
				MAKE_PED_FLEE(mpPrincess.mPed,FALSE)
				ePrincessState = SP_FLEE_ON_FOOT
			ENDIF			
		ENDIF
		
		//MAKES DEALER FLEE
		
		IF IS_PED_UNINJURED(mpDrugDealer.mPed)
			IF bCanSeePlayer = TRUE
				eDealerState = DS_CALM_DOWN
			ELSE
				eDealerState = DS_WAIT_FOR_FLEE
			ENDIF
		ENDIF
		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3B_PHOTO_SPOTTED)
		bSecurityNoticedPlayer = TRUE
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if player is near security - Used when trying to escape them
/// PARAMS:
///    fDistance - The distance the player is classed as having escaped
/// RETURNS:
///    True - If player is away from security, false otherwise. 
FUNC BOOL IS_PLAYER_AWAY_FROM_SECURITY(FLOAT fDistance = 50.0)

	INT i
	
	FOR i = 0 TO  NUM_OF_SECURITY_PEDS -1
		IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mSecurityPed[i].mPed,<<fDistance,fDistance,fDistance>>)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Checks if all the security are dead - Used when trying to escape them
/// RETURNS:
///    True - If all security are dead, false otherwise
FUNC BOOL ARE_GUARDS_DEAD()

	INT i
	
	FOR i = 0 TO  NUM_OF_SECURITY_PEDS -1
		IF mSecurityPed[i].bImDead = FALSE
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if one security is dead
/// RETURNS:
///    True is one security is dead
FUNC BOOL IS_ONE_GUARD_DEAD()
	INT i
	
	REPEAT  NUM_OF_SECURITY_PEDS i
		IF mSecurityPed[i].bImDead
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Makes secuirty PEDS attack the player
///    Makes princess leave the area
///    Makes dealer flee
///    Makes dealer goons attack security
PROC MAKE_SECURITY_ATTACK()

	TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	IF bSecurityAttacking = FALSE
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relDealers,relPrincess)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,relDealers)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relDealers,RELGROUPHASH_PLAYER)
		
		//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		/*
		// Kill dealer convo if it's still ongoing
		TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
			KILL_ANY_CONVERSATION()
		ENDIF
		*/	
		INT i
		
		FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
			IF IS_PED_UNINJURED(mpDealerGoons[i].mPed)
				IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
					//CLEAR_PED_TASKS(mpDealerGoons[i].mPed)
					//TASK_COMBAT_PED(mpDealerGoons[i].mPed,mSecurityPed[i].mPed)
					//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerGoons[i].mPed,FALSE)
				ENDIF
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
			IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
				//GIVE_WEAPON_TO_PED(mSecurityPed[i].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
				IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[i].mPed,FALSE)
					//TASK_COMBAT_PED(mSecurityPed[i].mPed,PLAYER_PED_ID())
					eSecurityState[i] = SS_ATTACK_PLAYER_GUNS
				ENDIF
				//BLIP SECURITY
				IF NOT DOES_BLIP_EXIST(mSecurityPed[i].mBlip)
					mSecurityPed[i].mBlip = CREATE_PED_BLIP(mSecurityPed[i].mPed,FALSE,FALSE,BLIPPRIORITY_MED)
				ENDIF
			ENDIF
		ENDFOR
		
		//MAKE ONE SECURITY GET INTO VEHICLE AND DRIVE AROUND BACK
		IF bSecurityNoticedPlayer = FALSE
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
				IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[1].mVehicle, VS_DRIVER)
					CLEAR_PED_TASKS(mSecurityPed[2].mPed)
					TASK_ENTER_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,3.0)
					eSecurityState[2] = SS_ENTER_VEHICLE
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
				//GIVE_WEAPON_TO_PED(mSecurityPed[2].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
				//TASK_COMBAT_PED(mSecurityPed[2].mPed,PLAYER_PED_ID())
				eSecurityState[2] = SS_ATTACK_PLAYER_GUNS
			ENDIF
		ENDIF
		
		//MAKES PRINCESS FLEE
		IF IS_PED_UNINJURED(mpPrincess.mPed)
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				// Kill dealer convo if it's still ongoing
				IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_DRIVER)
				IF NOT IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					CLEAR_PED_TASKS(mpPrincess.mPed)
					TASK_ENTER_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,3.0)
					ePrincessState = SP_ENTER_VEHICLE
				ENDIF
			ELSE
				MAKE_PED_FLEE(mpPrincess.mPed,FALSE)
				ePrincessState = SP_FLEE_ON_FOOT
			ENDIF			
		ENDIF
		
		//MAKES DEALER FLEE
		IF IS_PED_UNINJURED(mpDrugDealer.mPed)
			CLEAR_PED_TASKS(mpDrugDealer.mPed)
			MAKE_PED_FLEE(mpDrugDealer.mPed)
			eDealerState = DS_FLEE
		ENDIF

		IF b_Correct_Pic_Taken = FALSE //IF PLAYER HAS NOT TAKEN PHOTO, FAIL.
			SET_FAIL_REASON(FAIL_SPOTTED)
		ENDIF
		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3B_PHOTO_SPOTTED)
		bSecurityAttacking = TRUE
	ENDIF

ENDPROC

/// PURPOSE:
///  Makes all the people at the drug scene get ready to leave.
///  Gets them into the vehicles etc.
PROC LEAVE_DRUG_SCENE()
	
	IF bLeavingDrugScene = FALSE
		IF iIndividualLeaveTimer = 0
			iIndividualLeaveTimer = GET_GAME_TIMER()
		ENDIF
		
		// Kill dealer convo if it's still ongoing
		TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
			KILL_ANY_CONVERSATION()
		ENDIF
		
		//	PRINCESS GET INTO VEHICLE
		IF IS_PED_UNINJURED(mpPrincess.mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_BACK_LEFT)
				IF NOT IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					IF NOT IsPedPerformingTask(mpPrincess.mPed,SCRIPT_TASK_ENTER_VEHICLE)
						TASK_ENTER_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,1.0)
						ePrincessState = SP_ENTER_VEHICLE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// TWO GUARDS CLOSE TO PRINCESS GET INTO SAME VEHICLE AS PRINCESS
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 20
			IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
				IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_DRIVER)
					IF NOT IS_PED_IN_VEHICLE(mSecurityPed[0].mPed,mvSecurityCars[0].mVehicle)
						IF NOT IsPedPerformingTask(mSecurityPed[0].mPed,SCRIPT_TASK_ENTER_VEHICLE)
							TASK_ENTER_VEHICLE(mSecurityPed[0].mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVE_WALK)
							eSecurityState[0] = SS_LEAVE_AREA
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 400
			IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
				IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_FRONT_RIGHT)
					IF NOT IS_PED_IN_VEHICLE(mSecurityPed[1].mPed,mvSecurityCars[0].mVehicle)
						IF NOT IsPedPerformingTask(mSecurityPed[1].mPed,SCRIPT_TASK_ENTER_VEHICLE)
							TASK_ENTER_VEHICLE(mSecurityPed[1].mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_FRONT_RIGHT,1.0)
							eSecurityState[1] = SS_LEAVE_AREA
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//
		
		// TWO GUARDS AT ENTRANCE ON LEFT GET INTO SAME VEHICLE
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 600
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
				IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[1].mVehicle, VS_DRIVER)
					IF NOT IS_PED_IN_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle)
						IF NOT IsPedPerformingTask(mSecurityPed[2].mPed,SCRIPT_TASK_ENTER_VEHICLE)
							TASK_ENTER_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,1.0)
							eSecurityState[2] = SS_LEAVE_AREA
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 500
			IF IS_PED_UNINJURED(mSecurityPed[3].mPed)
				IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[1].mVehicle, VS_BACK_LEFT)
					IF NOT IS_PED_IN_VEHICLE(mSecurityPed[3].mPed,mvSecurityCars[1].mVehicle)
						IF NOT IsPedPerformingTask(mSecurityPed[3].mPed,SCRIPT_TASK_ENTER_VEHICLE)
							TASK_ENTER_VEHICLE(mSecurityPed[3].mPed,mvSecurityCars[1].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT,1.0)
							eSecurityState[3] = SS_LEAVE_AREA
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//
		
		//DRUG DEALER
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 100
			IF IS_PED_UNINJURED(mpDrugDealer.mPed) AND NOT IsPedPerformingTask(mpDrugDealer.mPed,SCRIPT_TASK_PAUSE)
				IF GET_SCRIPT_TASK_STATUS(mpDrugDealer.mPed,SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mpDrugDealer.mPed,SCRIPT_TASK_WANDER_STANDARD) <> WAITING_TO_START_TASK
					FORCE_PED_MOTION_STATE(mpDrugDealer.mPed,MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
					SET_PED_MIN_MOVE_BLEND_RATIO(mpDrugDealer.mPed,PEDMOVE_WALK)
					TASK_WANDER_STANDARD(mpDrugDealer.mPed)
				ENDIF
				eDealerState = DS_LEAVE_AREA
			ENDIF
		ENDIF
		
		//DEALER GOONS
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 350
			IF IS_PED_UNINJURED(mpDealerGoons[0].mPed) AND NOT IsPedPerformingTask(mpDealerGoons[0].mPed,SCRIPT_TASK_WANDER_STANDARD)
				TASK_WANDER_STANDARD(mpDealerGoons[0].mPed)
				eGoonState[0] = GS_FLEE_AREA
			ENDIF
		ENDIF
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 600
			IF IS_PED_UNINJURED(mpDealerGoons[1].mPed) AND NOT IsPedPerformingTask(mpDealerGoons[1].mPed,SCRIPT_TASK_WANDER_STANDARD)
				IF GET_SCRIPT_TASK_STATUS(mpDealerGoons[1].mPed,SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(mpDealerGoons[1].mPed,SCRIPT_TASK_WANDER_STANDARD) <> WAITING_TO_START_TASK	
					FORCE_PED_MOTION_STATE(mpDealerGoons[1].mPed,MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
					SET_PED_MIN_MOVE_BLEND_RATIO(mpDealerGoons[1].mPed,PEDMOVE_WALK)
					TASK_WANDER_STANDARD(mpDealerGoons[1].mPed)
				ENDIF
				eGoonState[1] = GS_FLEE_AREA
			ENDIF
		ENDIF
		
		IF GET_GAME_TIMER() - iIndividualLeaveTimer > 5000
			//IF PLAYER HAS NOT GOT THE PHOTO AND THEY ARE LEAVING - FAIL
			IF b_Correct_Pic_Taken = FALSE
				SET_FAIL_REASON(FAIL_PHOTO_LOST)
			ENDIF
		
			bLeavingDrugScene = TRUE
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///  Monitors the DRUG DEALER and deals with their behaviours
PROC Monitor_Drug_Dealer()
	
	SWITCH eDealerState
		
		CASE DS_WAIT
			IF HAS_PLAYER_THREATENED_PED(mpDrugDealer.mPed)
			OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(mpDrugDealer.mPed),20.0,FALSE)
				MAKE_PED_FLEE(mpDrugDealer.mPed)
				MAKE_SECURITY_ATTACK()
				eDealerState = DS_FLEE
			ENDIF
		BREAK
		
		CASE DS_CALM_DOWN
			IF (missionStage = MS_LEAVE_AREA OR missionStage = MS_ESCAPE_SECURITY) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[0].mPed, "Paparazzo3BBodyGuard1")
				ADD_PED_FOR_DIALOGUE(sSpeach, DEALER_ID, mpDrugDealer.mPed, "Paparazzo3BDrugDealer")
				ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_TWO_ID, mSecurityPed[1].mPed, "Paparazzo3BBodyGuard2")
				ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerGoons[0].mPed, "Paparazzo3BDealerGoon1")
				//IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR2", CONV_PRIORITY_HIGH)
					eDealerState = DS_WAIT_FOR_FLEE
				//ENDIF
			ENDIF
			//ADD STUFF MAKES DEALER TRY TO CALM THE SITUATION
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(mpDrugDealer.mPed),20.0,FALSE)
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				MAKE_PED_FLEE(mpDrugDealer.mPed)
				eDealerState = DS_FLEE
			ENDIF
		BREAK
		
		CASE DS_WAIT_FOR_FLEE
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(mpDrugDealer.mPed),20.0,FALSE)
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				MAKE_PED_FLEE(mpDrugDealer.mPed)
				eDealerState = DS_FLEE
			ENDIF
		BREAK
		
		CASE DS_FLEE
			//FLEEING THE AREA
		BREAK
		
		CASE DS_LEAVE_AREA
		BREAK
		
	ENDSWITCH


ENDPROC

//// PURPOSE:
 ///    Monitors drug goons, handles their behaviours
 /// PARAMS:
 ///    iPedNum - The index of the goon we're monitoring
PROC Monitor_Drug_Goons(INT iPedNum)

	
	SWITCH eGoonState[iPedNum]
		
		CASE GS_WAIT
			
		BREAK
		
		CASE GS_WAIT_AIM_AT_SECURITY
			IF GET_GAME_TIMER() > iGoonTimer
				IF IS_ENTITY_ALIVE(mSecurityPed[iPedNum].mPed)
					TASK_AIM_GUN_AT_ENTITY(mpDealerGoons[iPedNum].mPed,mSecurityPed[iPedNum].mPed,-1,FALSE)
					iGoonTimer = GET_GAME_TIMER() + 15000
					eGoonState[iPedNum] = GS_AIM_AT_SECURITY
				ENDIF
			ENDIF
		BREAK
		
		CASE GS_AIM_AT_SECURITY
			IF GET_GAME_TIMER() > iGoonTimer
				CLEAR_PED_TASKS(mpDealerGoons[iPedNum].mPed)
				//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,relHatePlayer)
				eGoonState[iPedNum] = GS_SHOOT_SECURITY
			ENDIF
		BREAK
		
		CASE GS_SHOOT_SECURITY
			IF ARE_GUARDS_DEAD()
				MAKE_PED_FLEE(mpDealerGoons[iPedNum].mPed)
				eGoonState[iPedNum] = GS_FLEE_AREA
			ENDIF
		BREAK
		
		CASE GS_FLEE_AREA
			//FLEEING AREA
		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC


/// PURPOSE:
///  Monitors the PRINCESS and deals with her behaviours
PROC Monitor_Princess()

	SWITCH ePrincessState
		
		CASE SP_WAIT
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF missionStage = MS_TAKE_PHOTO
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					/*
					ADD_PED_FOR_DIALOGUE(sSpeach, PRINCESS_ID, mpPrincess.mPed, "PRINCESS")
					ADD_PED_FOR_DIALOGUE(sSpeach, DEALER_ID, mpDrugDealer.mPed, "Paparazzo3BDrugDealer")
					IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_DEALER", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
						//TASK_PLAY_ANIM(mpPrincess.mPed,ANIM_DICT,PRINCESS_ANIM,NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
						ePrincessState = SP_SMOKE
					ENDIF
					*/
				ENDIF
			ENDIF	
		BREAK
		
		CASE SP_SMOKE
			IF HAS_PLAYER_THREATENED_PED(mpPrincess.mPed)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				IF b_Correct_Pic_Taken = FALSE
					SET_FAIL_REASON(FAIL_SPOTTED)
				ENDIF
				IF IS_PED_UNINJURED(mpPrincess.mPed)
					IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle) AND IS_VEHICLE_SEAT_FREE(mvSecurityCars[0].mVehicle, VS_DRIVER)
						CLEAR_PED_TASKS(mpPrincess.mPed)
						TASK_ENTER_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,3.0)
						ePrincessState = SP_ENTER_VEHICLE
					ELSE
						MAKE_PED_FLEE(mpPrincess.mPed,FALSE)
						ePrincessState = SP_FLEE_ON_FOOT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SP_ENTER_VEHICLE
			//DROP WEED
			IF IS_ENTITY_ATTACHED(oiSmokeWeed.mObject)
				DETACH_ENTITY(oiSmokeWeed.mObject,TRUE,FALSE)
			ENDIF
			//
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					FREEZE_ENTITY_POSITION(mvSecurityCars[0].mVehicle, FALSE)
					IF bSecurityAttacking = TRUE 
					OR bSecurityNoticedPlayer = TRUE
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mpPrincess.mPed,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
						/*SEQUENCE_INDEX siDriveOff
						OPEN_SEQUENCE_TASK(siDriveOff)
							//TASK_ENTER_VEHICLE(NULL, mvSecurityCars[0].mVehicle)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							TASK_VEHICLE_DRIVE_WANDER(NULL, mvSecurityCars[0].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
						CLOSE_SEQUENCE_TASK(siDriveOff)
						TASK_PERFORM_SEQUENCE(mpPrincess.mPed, siDriveOff)
						CLEAR_SEQUENCE_TASK(siDriveOff)*/
						ePrincessState = SP_WANDER
					ELSE
						IF eSecurityState[0] = SS_DRIVE_AWAY_FROM_AREA AND eSecurityState[1] = SS_DRIVE_AWAY_FROM_AREA
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mpPrincess.mPed,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							/*SEQUENCE_INDEX siDriveOff
							OPEN_SEQUENCE_TASK(siDriveOff)
								//TASK_ENTER_VEHICLE(NULL, mvSecurityCars[0].mVehicle)
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
								TASK_VEHICLE_DRIVE_WANDER(NULL, mvSecurityCars[0].mVehicle, 20, DRIVINGMODE_PLOUGHTHROUGH)
							CLOSE_SEQUENCE_TASK(siDriveOff)
							TASK_PERFORM_SEQUENCE(mpPrincess.mPed, siDriveOff)
							CLEAR_SEQUENCE_TASK(siDriveOff)*/
							ePrincessState = SP_WANDER
						ENDIF
					ENDIF
				ENDIF
			ELSE
				MAKE_PED_FLEE(mpPrincess.mPed,FALSE)
				ePrincessState = SP_FLEE_ON_FOOT
			ENDIF
		BREAK
		
		CASE SP_FLEE_IN_VEHICLE
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF GET_VEHICLE_WAYPOINT_PROGRESS(mvSecurityCars[0].mVehicle) >= 12
					IF IS_PED_UNINJURED(mpPrincess.mPed)
						IF bSecurityAttacking = TRUE 
						OR bSecurityNoticedPlayer = TRUE
							TASK_VEHICLE_DRIVE_WANDER(mpPrincess.mPed,mvSecurityCars[0].mVehicle,80.0,DRIVINGMODE_AVOIDCARS_RECKLESS)
							ePrincessState = SP_WANDER
						ELSE
							TASK_VEHICLE_DRIVE_WANDER(mpPrincess.mPed,mvSecurityCars[0].mVehicle,10.0,DRIVINGMODE_AVOIDCARS)
							ePrincessState = SP_WANDER
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_UNINJURED(mpPrincess.mPed)
					TASK_WANDER_STANDARD(mpPrincess.mPed)
					ePrincessState = SP_WANDER
				ENDIF
			ENDIF
			//FLEEING PLAYER IN VEHICLE
		BREAK
		
		CASE SP_FLEE_ON_FOOT
			//FLEEING PLAYER ON FOOT
		BREAK
		
		CASE SP_WANDER
			// WANDERING AROUND
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF GET_VEHICLE_WAYPOINT_PROGRESS(mvSecurityCars[0].mVehicle) >= 14
					IF IS_PED_UNINJURED(mpPrincess.mPed) AND NOT IsPedPerformingTask(mpPrincess.mPed, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
						TASK_VEHICLE_DRIVE_WANDER(mpPrincess.mPed,mvSecurityCars[0].mVehicle,10.0,DRIVINGMODE_AVOIDCARS)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH


ENDPROC

/// PURPOSE:
///    Monitors the Security and deals with their behaviours
/// PARAMS:
///    iPedNum - The ped we are checking
PROC Monitor_Security(INT iPedNum)
	
	
	INT iGoonNum

	IF iPedNum = 0
		iGoonNum = 0
	ELSE
		iGoonNum = 1
	ENDIF
	
	SWITCH eSecurityState[iPedNum]
		
		CASE SS_GUARD_AREA
			VEHICLE_INDEX viTemp
			viTemp = GET_PLAYERS_LAST_VEHICLE()
			IF HAS_PLAYER_THREATENED_PED(mSecurityPed[iPedNum].mPed) OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), mSecurityPed[iPedNum].mPed)
			OR (IS_VEHICLE_OK(viTemp) AND IS_ENTITY_TOUCHING_ENTITY(viTemp, mSecurityPed[iPedNum].mPed))
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[iPedNum].mPed, "Paparazzo3BBodyGuard1")
				IF IS_PLAYER_UNARMED()
					MAKE_SECURITY_ATTACK()
					eSecurityState[iPedNum] = SS_ATTACK_PLAYER_GUNS
				ELSE
					// not using a IF statement here as it delays the security attacking too much
					IF (missionStage = MS_LEAVE_AREA OR missionStage = MS_ESCAPE_SECURITY)
						CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR3", CONV_PRIORITY_HIGH)
					ENDIF
					MAKE_SECURITY_ATTACK()
					eSecurityState[iPedNum] = SS_ATTACK_PLAYER_GUNS
				ENDIF
			ELSE
				IF HAS_PLAYER_TRIED_TO_ENTER()
					IF iPedNum = 2 // SECURITY PED GUARDING THE SIDE ENTRANCE
						IF IS_PLAYER_UNARMED()
							IF bSecurityWarned
								TASK_LOOK_AT_ENTITY(mSecurityPed[iPedNum].mPed, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
								eSecurityState[iPedNum] = SS_WARN_PLAYER
							ELSE
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(mSecurityPed[iPedNum].mPed, "PAP3_ANAA", "Paparazzo3BBodyGuard1", SPEECH_PARAMS_STANDARD)
								TASK_LOOK_AT_ENTITY(mSecurityPed[iPedNum].mPed, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
								bSecurityWarned = TRUE
								eSecurityState[iPedNum] = SS_WARN_PLAYER
							ENDIF
						ELSE 		//ATTACK PLAYER HAS A WEAPON
							//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[iPedNum].mPed, "Paparazzo3BBodyGuard1")
							// not using a IF statement here as it delays the security attacking too much
							IF (missionStage = MS_LEAVE_AREA OR missionStage = MS_ESCAPE_SECURITY)
								CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR3", CONV_PRIORITY_HIGH)
							ENDIF
							MAKE_SECURITY_ATTACK()
							eSecurityState[iPedNum] = SS_ATTACK_PLAYER_GUNS
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_WARN_PLAYER
			//WARNED PLAYER ABOUT TRYING TO ENTER
			IF iPedNum = 2 // SECURITY PED GUARDING THE SIDE ENTRANCE
				IF NOT HAS_PLAYER_TRIED_TO_ENTER()
					TASK_CLEAR_LOOK_AT(mSecurityPed[iPedNum].mPed)
					eSecurityState[iPedNum] = SS_GUARD_AREA
				ENDIF
			ENDIF
		BREAK
					
		CASE SS_ATTACK_PLAYER_GUNS
			//ATTACKING PLAYER WITH GUNS
		BREAK
		

		CASE SS_WAIT_AIM_AT_GOONS
			IF GET_GAME_TIMER() > iSecurityTimer
			OR HAS_PLAYER_THREATENED_PED(mSecurityPed[iPedNum].mPed)
				IF IS_ENTITY_ALIVE(mpDealerGoons[iGoonNum].mPed)
					CLEAR_PED_TASKS(mSecurityPed[iPedNum].mPed)
					TASK_AIM_GUN_AT_ENTITY(mSecurityPed[iPedNum].mPed,mpDealerGoons[iGoonNum].mPed,-1,FALSE)
					iSecurityTimer = GET_GAME_TIMER() + 1000
					eSecurityState[iPedNum] = SS_AIM_AT_GOONS
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_INVESTIGATE // GOES INTO HERE IF PLAYER GETS PHONECALL FROM BEVERLY WHILE DOWN ALLEY OR NEAR GUARDS
			IF CAN_SECURITY_SEE_PLAYER()
			OR IS_ENTITY_AT_COORD(mSecurityPed[iPedNum].mPed,vAlleyInvestigateLoc,<<1.5,1.5,1.5>>)
			OR IS_PED_RUNNING(PLAYER_PED_ID())
			OR HAS_PLAYER_THREATENED_PED(mSecurityPed[iPedNum].mPed)
				ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[0].mPed, "Paparazzo3BBodyGuard1")
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					KILL_ANY_CONVERSATION()
				ENDIF
				IF (missionStage = MS_LEAVE_AREA OR missionStage = MS_ESCAPE_SECURITY)
					//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR4","PAP3_SECUR4_3",CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						MAKE_SECURITY_ATTACK() 
					//ENDIF
				ELSE
					MAKE_SECURITY_ATTACK()
				ENDIF
			ENDIF
			//SECURITY IS INVESTIGATING THE ALLEY
		BREAK
		
		CASE SS_AIM_AT_GOONS
			IF GET_GAME_TIMER() > iSecurityTimer
			OR HAS_PLAYER_THREATENED_PED(mSecurityPed[iPedNum].mPed)
				CLEAR_PED_TASKS(mSecurityPed[iPedNum].mPed)
				//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relHatePlayer,RELGROUPHASH_PLAYER)
				//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,relHatePlayer)
				eSecurityState[iPedNum] = SS_ATTACK_GOONS	
			ENDIF
		BREAK
		
		CASE SS_ATTACK_GOONS
			MAKE_SECURITY_ATTACK()
			//SHOOTING AT DRUG DEALERS
		BREAK
		
		CASE SS_ENTER_VEHICLE
		
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				IF IS_PED_IN_VEHICLE(mSecurityPed[iPedNum].mPed, mvSecurityCars[1].mVehicle)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvSecurityCars[1].mVehicle)
						FREEZE_ENTITY_POSITION(mvSecurityCars[1].mVehicle, FALSE)
						START_PLAYBACK_RECORDED_VEHICLE(mvSecurityCars[1].mVehicle, 103 ,"PAP3Security1" )
						SET_PLAYBACK_SPEED(mvSecurityCars[1].mVehicle,1.2)
						CPRINTLN(DEBUG_MISSION, "STARTING PLAYBACK NOW")
						eSecurityState[iPedNum] = SS_DRIVER_TO_PLAYER
					ENDIF
				ENDIF
			ENDIF		
		BREAK		
		
		CASE SS_DRIVER_TO_PLAYER

			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvSecurityCars[1].mVehicle)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PLAYER_IN_ALLEY_NOTICE_AREA()
						//TASK_COMBAT_PED(mSecurityPed[iPedNum].mPed,PLAYER_PED_ID())
						eSecurityState[iPedNum] = SS_ATTACK_PLAYER_CAR
					ELSE
						TASK_LEAVE_ANY_VEHICLE(mSecurityPed[iPedNum].mPed,0)
						eSecurityState[iPedNum] = SS_LEAVE_VEHICLE
					ENDIF
				ENDIF
			ENDIF
			//DRIVING AROUND TO BLOCK THE PLAYER IN
		BREAK
				
		CASE SS_LEAVE_VEHICLE
			IF NOT IS_PED_IN_ANY_VEHICLE(mSecurityPed[iPedNum].mPed)
				//TASK_COMBAT_PED(mSecurityPed[iPedNum].mPed,PLAYER_PED_ID())
				eSecurityState[iPedNum] = SS_ATTACK_PLAYER_GUNS
			ENDIF
		BREAK
		
		CASE SS_ATTACK_PLAYER_CAR
			//ATTACKING PLAYER IN A VEHICLE
		BREAK
		
		CASE SS_LEAVE_AREA
			IF IS_PED_IN_ANY_VEHICLE(mSecurityPed[iPedNum].mPed,FALSE)
				eSecurityState[iPedNum] = SS_DRIVE_AWAY_FROM_AREA
			ENDIF
		BREAK
		
		CASE SS_DRIVE_AWAY_FROM_AREA
			IF ePrincessState = SP_FLEE_IN_VEHICLE OR ePrincessState = SP_WANDER
				IF iPedNum = 2
					IF IS_PED_UNINJURED(mSecurityPed[iPedNum].mPed)
						IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
							FREEZE_ENTITY_POSITION(mvSecurityCars[1].mVehicle, FALSE)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mSecurityPed[iPedNum].mPed,mvSecurityCars[1].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
							eSecurityState[iPedNum] = SS_DRIVE_OUT_OF_ALLEY
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_DRIVE_OUT_OF_ALLEY
			IF iPedNum = 2
				IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
					IF GET_VEHICLE_WAYPOINT_PROGRESS(mvSecurityCars[1].mVehicle) >= 14
						IF IS_PED_UNINJURED(mSecurityPed[iPedNum].mPed)
							TASK_VEHICLE_DRIVE_WANDER(mSecurityPed[iPedNum].mPed,mvSecurityCars[1].mVehicle,10.0,DRIVINGMODE_AVOIDCARS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_DRIVE_WANDER
		
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///   Monitors the Princess scene.
PROC Monitor_Princess_Scene()

	INT i
	
	//SECURITY
	FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
			Monitor_Security(i)
		ELSE
			SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
			mSecurityPed[i].bImDead = TRUE // USED TO CHECK IF ALL SECURITY ARE DEAD
		ENDIF
	ENDFOR
	IF bSecurityAttacking AND missionStage = MS_TAKE_PHOTO
		IF IS_PLAYER_AWAY_FROM_SECURITY(150.0)
			i = 0
			REPEAT NUM_OF_SECURITY_PEDS i
				SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
				SAFE_RELEASE_PED(mSecurityPed[i].mPed, FALSE)
			ENDREPEAT
			bSecurityAttacking = FALSE
		ENDIF
	ENDIF
	
	//PRINCESS
	IF IS_ENTITY_ALIVE(mpPrincess.mPed)
		Monitor_Princess()
	ELSE
		MAKE_SECURITY_ATTACK()
		SET_FAIL_REASON(FAIL_PRINCESS_KILLED) //PRINCESS HAS BEEN KILLED
	ENDIF
	
	//DRUG DEALER
	IF IS_ENTITY_ALIVE(mpDrugDealer.mPed)
		Monitor_Drug_Dealer()
	ENDIF
	
	//DRUG DEALER GOONS
	FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
		IF IS_ENTITY_ALIVE(mpDealerGoons[i].mPed)
			Monitor_Drug_Goons(i)
		ENDIF
	ENDFOR
	
	IF IS_VEHICLE_OK(mvDealerCar.mVehicle) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mvDealerCar.mVehicle)
			START_VEHICLE_ALARM(mvDealerCar.mVehicle)
			MAKE_SECURITY_ATTACK()
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF b_Correct_Pic_Taken = FALSE
				SET_FAIL_REASON(FAIL_SPOTTED) // PLAYER WAS SPOTTED
			ENDIF
		ENDIF
	ENDIF
	
	IF bSecurityAttacking = FALSE
		//AREA CHECK - INSIDE A SPOT AREA CLOSE TO GUARDS
		IF ePrincessState <> SP_FLEE_IN_VEHICLE
		AND ePrincessState <> SP_WANDER
			IF IS_PLAYER_INSIDE_SPOT_AREA() OR IS_ONE_GUARD_DEAD()
				IF IS_PED_RUNNING(PLAYER_PED_ID())
				OR IS_PED_SPRINTING(PLAYER_PED_ID())
				OR IS_ONE_GUARD_DEAD()
				OR NOT IS_PLAYER_UNARMED()
					MAKE_SECURITY_ATTACK()
				ELSE
					NOTICE_PLAYER_EVENT(TRUE)
				ENDIF
				IF b_Correct_Pic_Taken = FALSE //IF PLAYER HAS NOT TAKEN PHOTO BUT ENTERS AREA, FAIL FOR LOSING PHOTO
					SET_FAIL_REASON(FAIL_SPOTTED) // PLAYER WAS SPOTTED
				ENDIF
			ENDIF
		ENDIF
		
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
		//PLAYER IS IN THE ALLEY AREA - ONLY WANT TO DO THIS IF PRINCESS IS NOT ALREADY LEAVING AREA
			IF ePrincessState <> SP_FLEE_IN_VEHICLE
			AND ePrincessState <> SP_WANDER
				IF IS_PLAYER_IN_ALLEY_NOTICE_AREA()
					IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
						IF (NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID()) AND (CAN_SECURITY_SEE_PLAYER() OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())))
						OR (bEnteredAlley AND GET_GAME_TIMER() > iNoticeInAlleyTimer AND CAN_SECURITY_SEE_PLAYER())
							//IF PLAYER HAS NOT TAKEN PHOTO BUT ENTERS AREA, FAIL FOR LOSING PHOTO
							IF b_Correct_Pic_Taken = FALSE
								NOTICE_PLAYER_EVENT(TRUE)
								SET_FAIL_REASON(FAIL_SPOTTED) // PLAYER WAS SPOTTED
							ELSE
								NOTICE_PLAYER_EVENT(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

/// PURPOSE:
///  Used to put PEDS into vehicles at end of mission to drive away
///  Only used if player cannot see any of the PEDS or Vehicles
///  Also added makes drug dealer and goons walk away from area.
PROC TELEPORT_PEDS_INTO_VEHICLES()

	IF bLeavingDrugScene = FALSE
		IF IS_PED_UNINJURED(mpPrincess.mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF NOT IS_PED_IN_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle)
					CLEAR_PED_TASKS(mpPrincess.mPed)
					SET_PED_INTO_VEHICLE(mpPrincess.mPed,mvSecurityCars[0].mVehicle,VS_DRIVER)
					ePrincessState = SP_ENTER_VEHICLE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF NOT IS_PED_IN_VEHICLE(mSecurityPed[0].mPed,mvSecurityCars[0].mVehicle)
					CLEAR_PED_TASKS(mSecurityPed[0].mPed)
					SET_PED_INTO_VEHICLE(mSecurityPed[0].mPed,mvSecurityCars[0].mVehicle,VS_BACK_LEFT)
					eSecurityState[0] = SS_LEAVE_AREA
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				IF NOT IS_PED_IN_VEHICLE(mSecurityPed[1].mPed,mvSecurityCars[0].mVehicle)
					CLEAR_PED_TASKS(mSecurityPed[1].mPed)
					SET_PED_INTO_VEHICLE(mSecurityPed[1].mPed,mvSecurityCars[0].mVehicle,VS_FRONT_RIGHT)
					eSecurityState[1] = SS_LEAVE_AREA
				ENDIF
			ENDIF
		ENDIF

		IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				IF NOT IS_PED_IN_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle)
					CLEAR_PED_TASKS(mSecurityPed[2].mPed)
					SET_PED_INTO_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,VS_DRIVER)
					eSecurityState[1] = SS_LEAVE_AREA
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mSecurityPed[3].mPed)
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				IF NOT IS_PED_IN_VEHICLE(mSecurityPed[3].mPed,mvSecurityCars[1].mVehicle)
					CLEAR_PED_TASKS(mSecurityPed[3].mPed)
					SET_PED_INTO_VEHICLE(mSecurityPed[3].mPed,mvSecurityCars[1].mVehicle,VS_BACK_LEFT)
					eSecurityState[1] = SS_LEAVE_AREA
				ENDIF
			ENDIF
		ENDIF
		
		
		//DRUG DEALER
		IF IS_PED_UNINJURED(mpDrugDealer.mPed)
			TASK_WANDER_STANDARD(mpDrugDealer.mPed)
			eDealerState = DS_LEAVE_AREA
		ENDIF
		
		//DEALER GOONS
		IF IS_PED_UNINJURED(mpDealerGoons[0].mPed)
			TASK_WANDER_STANDARD(mpDealerGoons[0].mPed)
			eGoonState[0] = GS_FLEE_AREA
		ENDIF
		IF IS_PED_UNINJURED(mpDealerGoons[1].mPed)
			TASK_WANDER_STANDARD(mpDealerGoons[1].mPed)
			eGoonState[1] = GS_FLEE_AREA
		ENDIF
	
		bLeavingDrugScene = TRUE
	ENDIF

ENDPROC

/// PURPOSE:
///    Unfreezes the mission vehicles
PROC UnfreezeVehicles()
	IF IS_VEHICLE_OK(mvClimbingTruck.mVehicle)
		FREEZE_ENTITY_POSITION(mvClimbingTruck.mVehicle,FALSE)
	ENDIF
	IF IS_VEHICLE_OK(mvDealerCar.mVehicle)
		FREEZE_ENTITY_POSITION(mvDealerCar.mVehicle,FALSE)
	ENDIF
	IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
		FREEZE_ENTITY_POSITION(mvSecurityCars[0].mVehicle,FALSE)
	ENDIF
	IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
		FREEZE_ENTITY_POSITION(mvSecurityCars[1].mVehicle,FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///  Manages the phone help text for the player   
PROC MANAGE_PHONE_HELP()

	IF bSimpleHelp = FALSE
	
		SWITCH iHelp 
			
			CASE 0 //Tells player to access the phone
				
				bHelpPrint2 = FALSE
				bHelpPrint3 = FALSE
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP1")
						IF bHelpPrint1 = FALSE	
							PRINT_HELP("PAP3_HELP1") // Access the phone using ~PAD_DPAD_UP~.
							bHelpPrint1 = TRUE
						ENDIF
					ENDIF
				ELSE
					bHelpPrint1 = FALSE	
					iHelp ++
				ENDIF
				
			BREAK
			
			CASE 1 // Tells player to select the camera once phone is up
			
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) = 0
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP2")
						IF bHelpPrint2 = FALSE	
							PRINT_HELP("PAP3_HELP2") // Select the camera from the phone with ~PAD_DPAD_ALL~.
							bHelpPrint2 = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP2") 
						CLEAR_HELP()
					ENDIF
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						iHelp ++
					ENDIF
				ENDIF
				IF NOT IS_PHONE_ONSCREEN()
					iHelp = 0
				ENDIF
				
			BREAK
				
			CASE 2 // Tells player to frame the shot and take picture
			
				IF b_Correct_Pic_Taken = TRUE
					iHelp ++
				ELSE
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) = 0
						bHelpPrint2 = FALSE
						bHelpPrint3 = FALSE
						iHelp = 1	
					ELIF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP3")
							IF bHelpPrint3 = FALSE	
								//PRINT_HELP("PAP3_HELP3")//Frame your shot then take the picture with ~PAD_A~.
								bHelpPrint3 = TRUE
							ENDIF
						//ENDIF
					ElIF NOT IS_PHONE_ONSCREEN()
						iHelp = 0
					ENDIF
				ENDIF
					
			BREAK
				
			CASE 3 // PHOTO HAS BEEN TAKEN - WAITING FOR PLAYER TO SEND
			BREAK
		
		ENDSWITCH
	ELSE // Switches to simple phone help if player has used camera before.
		IF b_Correct_Pic_Taken = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP6") 
					IF bPrintNonSimpleHelp = FALSE	
						PRINT_HELP("PAP3_HELP6") // Use you phone to take the picture
						bPrintNonSimpleHelp = TRUE	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP6") // Use you phone to take the picture
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///  Handles Princess scene - Loads models, checks they are loaded.
///  Also checks if everything is ok once created.
PROC HANDLE_PRINCESS_SCENE()

	INT i
  
  	IF bPrincessSceneCreated = FALSE
  
   		IF HAS_PRINCESS_SCENE_LOADED()
		
	      	// CREATE SECURITY
	      	FOR i = 0 TO NUM_OF_SECURITY_PEDS -1
	            CreateMissionPed(mSecurityPed[i],FALSE)
				SET_ENTITY_IS_TARGET_PRIORITY(mSecurityPed[i].mPed,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(mSecurityPed[i].mPed,relPrincess)
				IF i = 3	
					TASK_START_SCENARIO_IN_PLACE(mSecurityPed[i].mPed,"WORLD_HUMAN_GUARD_STAND")
					SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[i].mPed,CA_AGGRESSIVE,TRUE)
					SET_PED_COMBAT_MOVEMENT(mSecurityPed[i].mPed,CM_WILLADVANCE)
				ENDIF
				IF i = 0
				OR i = 3
					GIVE_WEAPON_TO_PED(mSecurityPed[i].mPed,WEAPONTYPE_APPISTOL,-1,FALSE,FALSE)
					GIVE_WEAPON_COMPONENT_TO_PED(mSecurityPed[i].mPed,WEAPONTYPE_APPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[i].mPed)
				ELSE
					GIVE_WEAPON_TO_PED(mSecurityPed[i].mPed,WEAPONTYPE_APPISTOL,-1,FALSE,FALSE)
					IF i = 1
						SET_PED_COMBAT_MOVEMENT(mSecurityPed[i].mPed,CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[i].mPed,CA_AGGRESSIVE,TRUE)
						SET_PED_COMBAT_RANGE(mSecurityPed[i].mPed,CR_NEAR)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[1].mPed,TRUE)
					ELSE
						SET_PED_ACCURACY(mSecurityPed[i].mPed,25)
					ENDIF
				ENDIF
				IF i = 0
				OR i = 1
					SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[i].mPed,CA_USE_COVER,FALSE)
				ENDIF
				ADD_ARMOUR_TO_PED(mSecurityPed[i].mPed,70)
				SET_PED_CONFIG_FLAG(mSecurityPed[i].mPed,PCF_DontBehaveLikeLaw,TRUE)
				IF i = 0
					SET_PED_SEEING_RANGE(mSecurityPed[i].mPed,5)
				ELSE
					SET_PED_SEEING_RANGE(mSecurityPed[i].mPed,20)
				ENDIF
				IF i = 0
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,11), 1, 0, 0) //(jbib)
				ELIF i = 1
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,11), 0, 2, 0) //(jbib)
				ELIF i = 2
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,11), 1, 1, 0) //(jbib)
				ELIF i = 3
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(mSecurityPed[i].mPed, INT_TO_ENUM(PED_COMPONENT,11), 0, 1, 0) //(jbib)
				ENDIF
	      	ENDFOR
			
			// CREATE PRINCESS
	        CreateMissionPed(mpPrincess,TRUE)
	        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPrincess.mPed,TRUE) 
			SET_PED_RELATIONSHIP_GROUP_HASH(mpPrincess.mPed,relPrincess)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mpPrincess.mPed,TRUE)
			TASK_START_SCENARIO_IN_PLACE(mpPrincess.mPed,"WORLD_HUMAN_SMOKING")
			SET_PED_PROP_INDEX(mpPrincess.mPed, ANCHOR_EYES, 0)
	
			// CREATE DRUG DEALER
			CreateMissionPed(mpDrugDealer,FALSE)
			SET_ENTITY_HEALTH(mpDrugDealer.mPed,125)
			//TASK_START_SCENARIO_IN_PLACE(mpDrugDealer.mPed,"WORLD_HUMAN_DRUG_DEALER_HARD")
			SET_PED_COMBAT_ATTRIBUTES(mpDrugDealer.mPed,CA_ALWAYS_FLEE,TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(mpDrugDealer.mPed,relDealers)
			SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mpDrugDealer.mPed)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDrugDealer.mPed,TRUE)
			/*
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(mpDrugDealer.mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			*/
			//CREATE DEALER GOONS
			FOR i = 0 TO NUM_OF_DEALER_GOONS -1
	            CreateMissionPed(mpDealerGoons[i],FALSE)
				SET_ENTITY_HEALTH(mpDealerGoons[i].mPed,125)
				SET_PED_ACCURACY(mpDealerGoons[i].mPed,1)
				IF i = 0
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
					GIVE_WEAPON_TO_PED(mpDealerGoons[i].mPed,WEAPONTYPE_MICROSMG,-1,FALSE,TRUE)
					IF IS_ENTITY_ALIVE(mpDealerGoons[i].mPed)
						TASK_LOOK_AT_ENTITY(mpDealerGoons[i].mPed,mpPrincess.mPed,-1)
					ENDIF
					SET_PED_COMBAT_MOVEMENT(mpDealerGoons[i].mPed,CM_WILLRETREAT)
				ELSE
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(mpDealerGoons[i].mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
				ENDIF

				SET_PED_RELATIONSHIP_GROUP_HASH(mpDealerGoons[i].mPed,relDealers)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerGoons[i].mPed,TRUE)
				
				IF i = 0
					TASK_START_SCENARIO_IN_PLACE(mpDealerGoons[i].mPed,"WORLD_HUMAN_STAND_IMPATIENT")
				ELSE
					//TASK_START_SCENARIO_IN_PLACE(mpDealerGoons[i].mPed,"WORLD_HUMAN_DRINKING")
				ENDIF
				
	      	ENDFOR
			
			// Player's contact
			CreateMissionPed(mpDealerContact, FALSE)
			SET_PED_CAN_BE_TARGETTED(mpDealerContact.mPed,FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,TRUE)
			SET_PED_FLEE_ATTRIBUTES(mpDealerContact.mPed, FA_DISABLE_HANDS_UP, TRUE)
			SET_ENTITY_COORDS_NO_OFFSET(mpDealerContact.mPed, mpDealerContact.vStartPos)
			//SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mpDealerContact.mPed, FALSE)
			GIVE_WEAPON_TO_PED(mpDealerContact.mPed,WEAPONTYPE_KNIFE,-1)
			TASK_START_SCENARIO_IN_PLACE(mpDealerContact.mPed,"WORLD_HUMAN_SMOKING")
			SET_PED_COMPONENT_VARIATION(mpDealerContact.mPed, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mpDealerContact.mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mpDealerContact.mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(mpDealerContact.mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			
			// CREATE SECURITY VEHICLES
			FOR i = 0 TO NUM_OF_SECURITY_CARS -1
	           	CreateMissionVehicle(mvSecurityCars[i],FALSE)
	           	IF i = 0
					SET_VEHICLE_DOORS_LOCKED(mvSecurityCars[i].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				ENDIF
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvSecurityCars[i].mModel,TRUE)
				SET_VEHICLE_COLOUR_COMBINATION(mvSecurityCars[i].mVehicle, 0)
				IF i = 1
					IF IS_ENTITY_ALIVE(mSecurityPed[2].mPed)
						SET_PED_INTO_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[i].mVehicle)
						SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[2].mPed,CA_USE_VEHICLE,TRUE)
					ENDIF
				ENDIF
	      	ENDFOR
			
			CreateMissionVehicle(mvDealerCar)
			SET_VEHICLE_ALARM(mvDealerCar.mVehicle, TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mvDealerCar.mModel,TRUE)
			SET_VEHICLE_COLOUR_COMBINATION(mvDealerCar.mVehicle, 3)
			
			CreateMissionVehicle(mvClimbingTruck)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mvClimbingTruck.mModel,TRUE)
			SET_VEHICLE_COLOUR_COMBINATION(mvClimbingTruck.mVehicle, 0)
			
			// Just letting the scenario spawn the prop now
			//CREATE WEED PROP
			//CreateMissionProp(oiSmokeWeed)
			//ATTACH_ENTITY_TO_ENTITY(oiSmokeWeed.mObject,mpPrincess.mPed, GET_PED_BONE_INDEX(mpPrincess.mPed, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
			
	      	bPrincessSceneCreated = TRUE
			
			//Unload PED models
			FOR i = 0 TO NUM_OF_SECURITY_PEDS -1
	            SET_MODEL_AS_NO_LONGER_NEEDED(mSecurityPed[i].mModel)
	      	ENDFOR
			
			FOR i = 0 TO NUM_OF_SECURITY_CARS -1
	            SET_MODEL_AS_NO_LONGER_NEEDED(mvSecurityCars[i].mModel)
	      	ENDFOR
			
			SET_MODEL_AS_NO_LONGER_NEEDED(mpDrugDealer.mModel)

			issDealidles = CREATE_SYNCHRONIZED_SCENE(mvSecurityCars[0].vStartPos,<<0,0,mvSecurityCars[0].fStartHeading>>)
			SET_SYNCHRONIZED_SCENE_LOOPED(issDealidles,TRUE)
			//TASK_SYNCHRONIZED_SCENE(mpPrincess.mPed, issDealidles, "rcmpaparazzo_3big_1", "_idle_princess", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
			TASK_SYNCHRONIZED_SCENE(mpDrugDealer.mPed, issDealidles, "rcmpaparazzo_3big_1", "_idle_dealer_a", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE| SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(mpDealerGoons[1].mPed, issDealidles, "rcmpaparazzo_3big_1", "_idle_dealer_b", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE| SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(mSecurityPed[0].mPed, issDealidles, "rcmpaparazzo_3big_1", "_idle_guard_a", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE| SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(mSecurityPed[1].mPed, issDealidles, "rcmpaparazzo_3big_1", "_idle_guard_b", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE| SYNCED_SCENE_USE_PHYSICS)
			
    	ENDIF
  	ELSE
  		//Monitor_Princess_Scene()
  	ENDIF
	
ENDPROC

/// PURPOSE:
///    Controls the dealer contact's behaviour
PROC HANDLE_CONTACT()
	IF DOES_ENTITY_EXIST(mpDealerContact.mPed)
		IF NOT IS_ENTITY_DEAD(mpDealerContact.mPed)	
			IF IS_PED_INJURED(mpDealerContact.mPed)
				SET_FAIL_REASON(FAIL_CONTACT_KILLED)
			ELIF HAS_PLAYER_THREATENED_PED(mpDealerContact.mPed)
				//IF IS_ENTITY_PLAYING_ANIM(mpDealerContact.mPed, DEALER_ANIM_DICT, CONTACT_ANIM)
				//	STOP_ANIM_TASK(mpDealerContact.mPed, DEALER_ANIM_DICT, CONTACT_ANIM)
				//ENDIF
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mpDealerContact.mPed)
				IF NOT IS_PED_FLEEING(mpDealerContact.mPed)
					TASK_SMART_FLEE_PED(mpDealerContact.mPed, PLAYER_PED_ID(), 500, -1)
				ENDIF
				SET_FAIL_REASON(FAIL_CONTACT_THREAT)
			ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpDealerContact.mPed,PLAYER_PED_ID())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mpDealerContact.mPed)
				IF NOT IS_PED_FLEEING(mpDealerContact.mPed)
					TASK_SMART_FLEE_PED(mpDealerContact.mPed, PLAYER_PED_ID(), 500, -1)
				ENDIF
				SET_FAIL_REASON(FAIL_CONTACT_THREAT)
			ELIF (IS_ENTITY_ALIVE(GET_PLAYERS_LAST_VEHICLE()) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpDealerContact.mPed,GET_PLAYERS_LAST_VEHICLE()))
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mpDealerContact.mPed)
				IF NOT IS_PED_FLEEING(mpDealerContact.mPed)
					TASK_SMART_FLEE_PED(mpDealerContact.mPed, PLAYER_PED_ID(), 500, -1)
				ENDIF
				SET_FAIL_REASON(FAIL_CONTACT_THREAT)	
			/*
			ELIF GET_GAME_TIMER() > iBumpContactTimer AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), mpDealerContact.mPed)
				TEXT_LABEL_23 tlConv
				tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				STRING sConvABEL_TO_STRING(tlConv)
				IF NOT ARE_STRINGS_EQUA
				sConv = CONVERT_TEXT_LL(sConv, "PAP3_CONTACT")
					REMOVE_PED_FOR_DIALOGUE(sSpeach, GOON_ID)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(mpDealerContact.mPed, sBumpLines[iCurrentBumpLine], "JOE", SPEECH_PARAMS_STANDARD)
					IF iCurrentBumpLine = 0
						iCurrentBumpLine = 1
					ELSE
						iCurrentBumpLine = 0
					ENDIF
					iBumpContactTimer = GET_GAME_TIMER() + 5000
				ENDIF
				*/
			ENDIF
		ELSE
			SET_FAIL_REASON(FAIL_CONTACT_KILLED)
		ENDIF
	ENDIF
ENDPROC


///
///    MAIN MISSION FLOW FUNCTIONS / PROCEDURES
///    
///

/// PURPOSE:
///    Initialises the mission
PROC STAGE_INIT()

	IF IS_MISSION_INITIALISED() // initialise everything
		
		sbiChicoRear = ADD_SCENARIO_BLOCKING_AREA(<<1051.8594, -799.0458, 53.0>>,<<1114.5818, -779.1284, 60.0>>)
		sbiVehAttractor = ADD_SCENARIO_BLOCKING_AREA(<<1090.2480, -761.9150, 56.7705>>-<<5,5,5>>,<<1090.2480, -761.9150, 56.7705>>+<<5,5,5>>)
	
		// handle replay checkpoints
		IF Is_Replay_In_Progress()
			VEHICLE_INDEX viTemp
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				iReplayStage++ // player is skipping this stage
			ENDIF
			
			SWITCH iReplayStage
			
				CASE CP_PHONE_CALL
					//Do_Z_Skip(Z_SKIP_PHONE_CALL)	 // skip the mocap intro
					START_REPLAY_SETUP(<< 1039.8661, -536.2775, 60.0808 >>,173.0)
					RC_START_Z_SKIP()
					WHILE NOT HAS_PRINCESS_SCENE_LOADED(TRUE)
						WAIT(0)
					ENDWHILE
					bPrincessSceneCreated = FALSE
					HANDLE_PRINCESS_SCENE()
					CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
					//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1039.8661, -536.2775, 60.0808 >>)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(),173.0)
					CREATE_VEHICLE_FOR_REPLAY(viTemp, <<1027.5906, -550.1744, 59.2083>>, 175.1857, TRUE, FALSE, FALSE, TRUE, TRUE, ASTEROPE)   //<<1063.70, -528.63, 61.75>>, 99.04
					//CREATE_VEHICLE_FOR_REPLAY(viTemp, <<1063.70, -528.63, 61.75>>, 99.04, FALSE, FALSE, FALSE, TRUE, TRUE, BUFFALO)
					END_REPLAY_SETUP(viTemp)
					ResetCamBehindPlayer()
					eSubStage = SS_CLEANUP
					missionStage = MS_INITIAL_PHONE
					//WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
					FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
					RC_END_Z_SKIP()
				BREAK
				
				CASE CP_AT_PRINCESS_LOCATION
					CLEAR_AREA_OF_OBJECTS(<<1077.0671, -797.3167, 57.3309>>,150,CLEAROBJ_FLAG_FORCE)
					START_REPLAY_SETUP(<<1066.6431, -774.0832, 57.1322>>, 245.0544)
					RC_START_Z_SKIP()
					WHILE NOT HAS_PRINCESS_SCENE_LOADED(TRUE)
						WAIT(0)
					ENDWHILE
					bPrincessSceneCreated = FALSE
					HANDLE_PRINCESS_SCENE()
					CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
					//SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1071.6343, -774.4527, 57.1467 >>)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(),157.2742)
					CREATE_VEHICLE_FOR_REPLAY(viTemp, <<1077.7363, -765.1624, 56.6271>>, 271.4277, FALSE, FALSE, FALSE, TRUE, TRUE, BUFFALO)
					eSubStage = SS_CLEANUP
					missionStage = MS_MEET_CONTACT
					END_REPLAY_SETUP()
					//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1072.322632,-773.616272,56.325531>>, <<1062.145874,-773.913025,59.443981>>, 7.250000,<<1077.7363, -765.1624, 56.6271>>, 271.4277)
					SAFE_DELETE_PED(mpDealerContact.mPed)
					ResetCamBehindPlayer()
					CLEAR_AREA(<<1075.6421, -793.8809, 57.3145>>,40,TRUE)
					//WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
					FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
					RC_END_Z_SKIP()
				BREAK
				
				CASE CP_MISSION_PASSED
					START_REPLAY_SETUP(<<1066.6431, -774.0832, 57.1322>>, 245.0544)
					CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
					//WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					END_REPLAY_SETUP()
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
					WAIT(600)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					RC_END_Z_SKIP()
					Script_Passed()
				BREAK

				DEFAULT
					SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
	            BREAK
			ENDSWITCH
		ELSE
			IF IS_REPEAT_PLAY_ACTIVE()
				eInitialSceneStage = IS_REQUEST_SCENE
				WHILE NOT SetupScene_PAPARAZZO_3B(sRCLauncherDataLocal)
					WAIT(0)
				ENDWHILE
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
			ENDIF
			eSubStage = SS_SETUP
			missionStage = MS_INITIAL_PHONE
		ENDIF
	ENDIF
ENDPROC
   
/// PURPOSE:
/// Handles the initial phone call to the player
PROC INITIAL_PHONE_CALL() 
	
	INT i	
		
	IF bPrincessSceneCreated	
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
			IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
				IF IS_PED_IN_COMBAT(mSecurityPed[i].mPed,PLAYER_PED_ID())	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[i].mPed,FALSE)
					SET_FAIL_REASON(FAIL_SPOTTED)
					EXIT
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(mSecurityPed[i].mPed,PLAYER_PED_ID(),5)
						TASK_COMBAT_PED(mSecurityPed[i].mPed,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ELSE
				SET_FAIL_REASON(FAIL_SPOTTED)
				EXIT
			ENDIF
		ENDFOR	
	ENDIF
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			CPRINTLN(DEBUG_MISSION, "ENTERING PHONE CALL 30 ")
			
			ADD_PED_FOR_DIALOGUE(sSpeach, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(sSpeach, BEVERLY_ID, NULL, "BEVERLY")
			
			IF bFinishedSkipping = TRUE
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				HANG_UP_AND_PUT_AWAY_PHONE()
			ENDIF
					
			bConversationActive = FALSE
			
			SAFE_REMOVE_BLIP(biMissionBlip)

			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			HANDLE_PRINCESS_SCENE()
			HANDLE_CONTACT()
			IF bMissionFailed
				EXIT
			ENDIF
			IF bPrincessSceneCreated = TRUE	
				IF DID_PLAYER_GO_STRAIGHT_TO_PRINCESS()
					HANG_UP_AND_PUT_AWAY_PHONE()
					CLEAR_PRINTS()
					bMainObjectiveDisplayed = FALSE
					UnfreezeVehicles()
					SetStage(MS_MEET_CONTACT)
				ENDIF
			ENDIF
			IF NOT bConversationActive
				IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(sSpeach, CHAR_BEVERLY, "PAP3BAU", "PAP3_INTRO", CONV_PRIORITY_VERY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 5.0, REPLAY_IMPORTANCE_LOW)
					bConversationActive = TRUE
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 6 AND NOT DOES_BLIP_EXIST(biMissionBlip) AND IS_PED_UNINJURED(mpDealerContact.mPed)   //> 0
						biMissionBlip = CREATE_PED_BLIP(mpDealerContact.mPed, TRUE, TRUE)
						IF DOES_BLIP_EXIST(biMissionBlip)
							SET_BLIP_ROUTE(biMissionBlip,true)
						ENDIF
					ENDIF
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPrincessInitialLocation,<<STOP_DIS,STOP_DIS,2>>)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1046.049805,-754.827087,56.041855>>, <<1144.919678,-755.214111,59.983875>>, 20.750000) 	//road
						HANG_UP_AND_PUT_AWAY_PHONE()
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 5.0
								//BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),8.0,1)
							ELSE
								CLEAR_PRINTS()
								bMainObjectiveDisplayed = FALSE
								UnfreezeVehicles()
								SetStage(MS_MEET_CONTACT)
							ENDIF
						ELSE
							CLEAR_PRINTS()
							bMainObjectiveDisplayed = FALSE
							UnfreezeVehicles()
							SetStage(MS_MEET_CONTACT)
						ENDIF		
					ENDIF
				ELSE
					IF HAS_CELLPHONE_CALL_FINISHED()
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
		
			//CLEAR_PRINTS()
			
			bMainObjectiveDisplayed = FALSE
			
			SetStage(MS_GO_TO_LOCATION)
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player going to the inital area location
PROC GO_TO_LOCATION() 
	
	INT i	
	
	IF bPrincessSceneCreated	
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
			IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
				IF IS_PED_IN_COMBAT(mSecurityPed[i].mPed,PLAYER_PED_ID())	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[i].mPed,FALSE)
					SET_FAIL_REASON(FAIL_SPOTTED)
					EXIT
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(mSecurityPed[i].mPed,PLAYER_PED_ID(),5)
						TASK_COMBAT_PED(mSecurityPed[i].mPed,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ELSE
				SET_FAIL_REASON(FAIL_SPOTTED)
				EXIT
			ENDIF
		ENDFOR	
	ENDIF
		
	SWITCH eSubStage
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "ENTERING GO TO LOCATION 1")
			
			//SAFE_REMOVE_BLIP(biMissionBlip)
			
			IF NOT DOES_BLIP_EXIST(biMissionBlip)
				biMissionBlip = CREATE_PED_BLIP(mpDealerContact.mPed, TRUE, TRUE)
				SET_BLIP_ROUTE(biMissionBlip,true)
			ENDIF
			
			PRINT_NOW("PAP3_CON", DEFAULT_GOD_TEXT_TIME, 1) // Meet Beverly's ~b~contact.~s~
			
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			HANDLE_PRINCESS_SCENE()
			HANDLE_CONTACT()
			IF bMissionFailed
				EXIT
			ENDIF
			IF bPrincessSceneCreated = TRUE
				IF DID_PLAYER_GO_STRAIGHT_TO_PRINCESS()
					eSubStage = SS_CLEANUP
				ENDIF
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPrincessInitialLocation,<<STOP_DIS,STOP_DIS,2>>)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 5.0
							//BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),8.0,1)
						ELSE
							eSubStage = SS_CLEANUP
						ENDIF
					ELSE
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP	
			
			bMainObjectiveDisplayed = FALSE
			
			UnfreezeVehicles()
			
			SetStage(MS_MEET_CONTACT)
		BREAK
	ENDSWITCH
ENDPROC

PROC MEET_CONTACT()

	TEXT_LABEL_23 label
	
	INT i	
	
	IF bPrincessSceneCreated	
		FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
			IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
				IF IS_PED_IN_COMBAT(mSecurityPed[i].mPed,PLAYER_PED_ID())	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[i].mPed,FALSE)
					SET_FAIL_REASON(FAIL_SPOTTED)
					EXIT
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(mSecurityPed[i].mPed,PLAYER_PED_ID(),5)
						TASK_COMBAT_PED(mSecurityPed[i].mPed,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ELSE
				SET_FAIL_REASON(FAIL_SPOTTED)
				EXIT
			ENDIF
		ENDFOR	
	ENDIF
		
	IF IS_PED_UNINJURED(mpDealerContact.mPed)	
		IF NOT IS_PED_HEADTRACKING_PED(mpDealerContact.mPed, PLAYER_PED_ID())
			TASK_LOOK_AT_ENTITY(mpDealerContact.mPed, PLAYER_PED_ID(), -1)
		ENDIF	
	ENDIF
		
	SWITCH eSubStage
		CASE SS_SETUP
			IF NOT DID_PLAYER_GO_STRAIGHT_TO_PRINCESS()
				IF DOES_BLIP_EXIST(biMissionBlip)
					SET_BLIP_ROUTE(biMissionBlip, FALSE)
				ELSE
					biMissionBlip = CREATE_PED_BLIP(mpDealerContact.mPed, TRUE, TRUE)
					PRINT_NOW("PAP3_CON", DEFAULT_GOD_TEXT_TIME, 1) // Meet Beverly's ~b~contact.~s~
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				FREEZE_ENTITY_POSITION(mvSecurityCars[0].mVehicle, FALSE)
			ENDIF
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				FREEZE_ENTITY_POSITION(mvSecurityCars[1].mVehicle, FALSE)
			ENDIF
			IF IS_VEHICLE_OK(mvDealerCar.mVehicle)
				FREEZE_ENTITY_POSITION(mvDealerCar.mVehicle, FALSE)
			ENDIF
			ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerContact.mPed, "Paparazzo3BDealerGoon1")
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			HANDLE_PRINCESS_SCENE()
			HANDLE_CONTACT()
			IF bMissionFailed
				EXIT
			ENDIF
			HANDLE_AUTO_CROUCHING()
			IF bPrincessSceneCreated = TRUE
				IF DID_PLAYER_GO_STRAIGHT_TO_PRINCESS()	
					SAFE_REMOVE_BLIP(biMissionBlip)
					eSubStage = SS_CLEANUP
				ENDIF
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPrincessInitialLocation,<<CONTACT_DIS,CONTACT_DIS,2>>) AND IS_PED_UNINJURED(mpDealerContact.mPed)
					IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())	
						ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerContact.mPed, "Paparazzo3BDealerGoon1")
						ADD_PED_FOR_DIALOGUE(sSpeach, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
						IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_CONTACT", CONV_PRIORITY_HIGH)
							
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOW)
							
							TASK_TURN_PED_TO_FACE_ENTITY(mpDealerContact.mPed,PLAYER_PED_ID(),-1)
							SAFE_REMOVE_BLIP(biMissionBlip)
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPrincessInitialLocation,<<15,15,2>>) AND IS_PED_UNINJURED(mpDealerContact.mPed)
				AND GET_SCRIPT_TASK_STATUS(mpDealerContact.mPed,SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK
					CLEAR_PED_TASKS(mpDealerContact.mPed)
					TASK_LOOK_AT_ENTITY(mpDealerContact.mPed,PLAYER_PED_ID(),-1)
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			HANDLE_PRINCESS_SCENE()
			HANDLE_CONTACT()
			IF bMissionFailed
				EXIT
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpDealerContact.mPed) > (CONTACT_DIS*2)) OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1071.163818,-783.058472,53.730213>>, <<1062.914917,-782.847290,59.343300>>, 3.000000)
				KILL_FACE_TO_FACE_CONVERSATION()
				IF IS_PED_UNINJURED(mpDealerContact.mPed) AND NOT IS_PED_FLEEING(mpDealerContact.mPed)
					IF GET_SCRIPT_TASK_STATUS(mpDealerContact.mPed,SCRIPT_TASK_WANDER_STANDARD)	<> PERFORMING_TASK
						//SET_PED_COMBAT_ATTRIBUTES(mpDealerContact.mPed,CA_ALWAYS_FLEE,TRUE)
						SET_PED_CAN_BE_TARGETTED(mpDealerContact.mPed,TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
						TASK_WANDER_STANDARD(mpDealerContact.mPed)
					ENDIF
				ENDIF
				SetStage(MS_TAKE_PHOTO)
			ELSE
				label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()	
				IF ARE_STRINGS_EQUAL(label,"PAP3_CONTACT_4") 
				OR ARE_STRINGS_EQUAL(label,"PAP3_CONTACT_5") 		
					++iCounterContactWalkOff
					IF iCounterContactWalkOff > 30		
						IF IS_PED_UNINJURED(mpDealerContact.mPed)	
							IF GET_SCRIPT_TASK_STATUS(mpDealerContact.mPed,SCRIPT_TASK_WANDER_STANDARD)	<> PERFORMING_TASK
								//SET_PED_COMBAT_ATTRIBUTES(mpDealerContact.mPed,CA_ALWAYS_FLEE,TRUE)
								SET_PED_CAN_BE_TARGETTED(mpDealerContact.mPed,TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
								TASK_WANDER_STANDARD(mpDealerContact.mPed,281.0714)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				/*
				IF ARE_STRINGS_EQUAL(label,"PAP3_CONTACT_3") 
					++iCounterContactWalkOff
					IF iCounterContactWalkOff > 70	
						IF GET_SCRIPT_TASK_STATUS(mpDealerContact.mPed,SCRIPT_TASK_WANDER_STANDARD)	<> PERFORMING_TASK
							SET_PED_COMBAT_ATTRIBUTES(mpDealerContact.mPed,CA_ALWAYS_FLEE,TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerContact.mPed,FALSE)
							TASK_WANDER_STANDARD(mpDealerContact.mPed)
						ENDIF
					ENDIF
				ENDIF
				*/
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player taking a photo of the princess
PROC TAKE_PHOTO() 
	
	//PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mSecurityPed[0].mPed))
	//PRINTNL()
	//PRINTFLOAT(GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID()))  // 20
	//PRINTNL()
	
	TEXT_LABEL_23 label
	TEXT_LABEL_23 root
		
	//FLOAT fStartSpawnWeed
	//FLOAT fEndSpawnWeed
		
	//VECTOR vWeed
	//VECTOR vCash
	//VECTOR vRotWeed
	//VECTOR vRotCash
		
	SWITCH eSubStage
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "ENTERING TAKE PHOTO OF PRINCESS")
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("PAP3_PRIN", DEFAULT_GOD_TEXT_TIME, 1) //"Take a photo of the ~b~princess."
				bMainObjectiveDisplayed = TRUE
			ENDIF
			
			CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
			
			iLeaveSceneTimer = GET_GAME_TIMER() + 80000
			bEnteredAlley = FALSE
			//bAlreadyLeft = FALSE
			bSecurityWarned = FALSE
			bForceCrouching = TRUE
			bSetRel = FALSE
			bKickedOffDealerConvo = FALSE
			//bBadPicSent = FALSE	
			bCarAlarm = FALSE	
			bAlleyWarning = FALSE
			iFramesPlayerNotInCoverNextToCar = 0
			iFramesPlayerMakingNoise = 0
			iFramesPlayerOnRoof = 0
			bPicTaken = FALSE
			bTooFarAway = FALSE
			bSweetTxtSent = FALSE
			iBevTxtsBadPic = 0
			iBevTxtsTooFarAway = 0
			bPicSent = FALSE
			bLeavingScene = FALSE
			bPrintLeaveArea = FALSE
			bSecPed3AchHead = FALSE
			bWrongContact = FALSE
			bGoGoDealerConvo = FALSE
			bDoFirstConvoLine = FALSE
			bSkippedSS = FALSE
			iPlayerInLeftAlley = 0
			iSeqSecThreaten = 0
			iSubSwitch = 0
			iRetrigger = 0
			iCounterGaurd3Investigate = 0
			bPrincessStartedSyncedScene = FALSE
			
			//ADD_COVER_BLOCKING_AREA(<<1086.668823,-796.542725,58.347771>>,<<3.500000,2.750000,1.250000>>,TRUE,TRUE,TRUE)
			
			SAFE_REMOVE_BLIP(biMissionBlip)
			
			biMissionBlip = CREATE_PED_BLIP(mpPrincess.mPed,TRUE,TRUE)
			
			IF IS_PED_UNINJURED(mpPrincess.mPed)
				iSightTestID = REGISTER_PEDSIGHT_TEST(PLAYER_PED_ID(), mpPrincess.mPed, 0)
			ENDIF
			
			IF IS_VEHICLE_OK(mvSecurityCars[0].mVehicle)
				FREEZE_ENTITY_POSITION(mvSecurityCars[0].mVehicle, FALSE)
			ENDIF
			IF IS_VEHICLE_OK(mvSecurityCars[1].mVehicle)
				FREEZE_ENTITY_POSITION(mvSecurityCars[1].mVehicle, FALSE)
			ENDIF
			IF IS_VEHICLE_OK(mvDealerCar.mVehicle)
				FREEZE_ENTITY_POSITION(mvDealerCar.mVehicle, FALSE)
			ENDIF
	
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_PRINCESS_LOCATION, "At Princess Location", TRUE) // mocap done, set checkpoint
	
			HANDLE_PRINCESS_SCENE()
			//HANDLE_CONTACT()
	
			iSeqDeal = 0
	
			ADD_PED_FOR_DIALOGUE(sSpeach, PRINCESS_ID, mpPrincess.mPed, "PRINCESS")
			ADD_PED_FOR_DIALOGUE(sSpeach, DEALER_ID, mpDrugDealer.mPed, "Paparazzo3BDrugDealer")		
			
			iTimerTakePhotoStage = GET_GAME_TIMER()			
						
			eSubStage = SS_UPDATE
			ePrincessState = SP_SMOKE
			
		BREAK
		
		CASE SS_UPDATE

			IF bKickedOffDealerConvo	
			AND NOT bSweetTxtSent	
			AND NOT bWhosThatGuy
				DO_ANNOYING_SUBTITLE_SWITCH()
			ENDIF
			
			IF IS_CELLPHONE_CAMERA_IN_USE()
				SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
			ENDIF
			
			IF IS_PED_UNINJURED(mpDealerContact.mPed)
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpDealerContact.mPed,PLAYER_PED_ID())
			AND NOT IS_ANY_SPEECH_PLAYING(sRCLauncherDataLocal.pedID[0])
				PLAY_PED_AMBIENT_SPEECH(sRCLauncherDataLocal.pedID[0], "GENERIC_CURSE_HIGH",SPEECH_PARAMS_FORCE)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(issDeal)
				IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
				AND IS_PED_UNINJURED(mpDealerGoons[1].mPed)
					IF iSeqDeal = 0
						//IF FIND_ANIM_EVENT_PHASE("rcmpaparazzo_3big_1","_action_dealer_b","spawn_drugs",fStartSpawnWeed,fEndSpawnWeed)
						//AND GET_ENTITY_ANIM_CURRENT_TIME(mpDealerGoons[1].mPed,"rcmpaparazzo_3big_1","_action_dealer_b") >= fStartSpawnWeed
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.259 //0.261
							objWeedBag = CREATE_OBJECT_NO_OFFSET(prop_drug_package_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mpDealerGoons[1].mPed,<<0.0,0.0,10.0>>))  //prop_drug_package_02
							//PLAY_SYNCHRONIZED_ENTITY_ANIM(objWeedBag,issDeal,"","rcmpaparazzo_3big_1",INSTANT_BLEND_IN,8)
							
							ATTACH_ENTITY_TO_ENTITY(objWeedBag, mpDealerGoons[1].mPed, GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_R_HAND), <<0.135,0,-0.05>>, <<0,0,0>>, TRUE, TRUE)
							SET_ENTITY_VISIBLE(objWeedBag,TRUE)
							//ATTACH_ENTITY_TO_ENTITY(objWeedBag, mpDealerGoons[1].mPed, GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							
							//objFakeGuard = CREATE_OBJECT_NO_OFFSET(PROP_DRUG_PACKAGE_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mpDealerGoons[1].mPed,<<0.0,0.0,11.0>>))
							//objFakeDealer = CREATE_OBJECT_NO_OFFSET(PROP_DRUG_PACKAGE_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mpDealerGoons[1].mPed,<<0.0,0.0,12.0>>))
							//SET_ENTITY_VISIBLE(objFakeGuard,FALSE)
							//SET_ENTITY_VISIBLE(objFakeDealer,FALSE)
							//ATTACH_ENTITY_TO_ENTITY(objFakeGuard, mSecurityPed[1].mPed, GET_PED_BONE_INDEX(mSecurityPed[1].mPed, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							//ATTACH_ENTITY_TO_ENTITY(objFakeDealer, mpDealerGoons[1].mPed, GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							iSeqDeal = 1
						ENDIF	
					ELIF iSeqDeal = 1
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.821
							objCash = CREATE_OBJECT_NO_OFFSET(PROP_CASH_PILE_02, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mSecurityPed[1].mPed,<<0.0,0.0,10.0>>))
							ATTACH_ENTITY_TO_ENTITY(objCash, mSecurityPed[1].mPed, GET_PED_BONE_INDEX(mSecurityPed[1].mPed, BONETAG_PH_R_HAND), <<0.07,0,-0.04>>, <<0,0,-30>>, TRUE, TRUE)
							iSeqDeal = 2
						ENDIF
					ELIF iSeqDeal = 2
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.844 //0.846
							PROCESS_ENTITY_ATTACHMENTS(mSecurityPed[1].mPed)
							PROCESS_ENTITY_ATTACHMENTS(mpDealerGoons[1].mPed)
							//vWeed = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objFakeGuard,GET_ENTITY_COORDS(objWeedBag))
							//vWeed = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_WORLD_POSITION_OF_ENTITY_BONE(mSecurityPed[1].mPed,GET_PED_BONE_INDEX(mSecurityPed[1].mPed, BONETAG_PH_L_HAND)),GET_ENTITY_HEADING(mSecurityPed[1].mPed),GET_ENTITY_COORDS(objWeedBag))
							//DELETE_OBJECT(objFakeGuard)
							//vRotWeed = GET_ENTITY_ROTATION(objWeedBag)
							DETACH_ENTITY(objWeedBag)
							//ATTACH_ENTITY_TO_ENTITY(objWeedBag, mSecurityPed[1].mPed, GET_PED_BONE_INDEX(mSecurityPed[1].mPed, BONETAG_PH_L_HAND), vWeed, vRotWeed, TRUE, TRUE, FALSE, FALSE, EULER_YXZ, FALSE)
							SET_ENTITY_VISIBLE(objWeedBag,FALSE)
							ATTACH_ENTITY_TO_ENTITY(objWeedBag, mSecurityPed[1].mPed, GET_PED_BONE_INDEX(mSecurityPed[1].mPed, BONETAG_PH_L_HAND), <<0.1,0,0.05>>, <<0,0,0>>, TRUE, TRUE)
							iSeqDeal = 3
						ENDIF
					ELIF iSeqDeal = 3
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.849
							PROCESS_ENTITY_ATTACHMENTS(mSecurityPed[1].mPed)
							PROCESS_ENTITY_ATTACHMENTS(mpDealerGoons[1].mPed)
							//vCash = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objFakeDealer,GET_ENTITY_COORDS(objCash))
							//vCash = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_WORLD_POSITION_OF_ENTITY_BONE(mpDealerGoons[1].mPed,GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_R_HAND)),GET_ENTITY_HEADING(mpDealerGoons[1].mPed),GET_ENTITY_COORDS(objCash))
							//DELETE_OBJECT(objFakeDealer)
							//vRotCash = GET_ENTITY_ROTATION(objCash)
							DETACH_ENTITY(objCash)
							//ATTACH_ENTITY_TO_ENTITY(objCash, mpDealerGoons[1].mPed, GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_R_HAND), vCash, vRotCash, TRUE, TRUE, FALSE, FALSE, EULER_YXZ, FALSE)
							SET_ENTITY_VISIBLE(objCash,FALSE)
							ATTACH_ENTITY_TO_ENTITY(objCash, mpDealerGoons[1].mPed, GET_PED_BONE_INDEX(mpDealerGoons[1].mPed, BONETAG_PH_R_HAND), <<0.07,0,0>>, <<0,0,90>>, TRUE, TRUE)
							DETACH_ENTITY(objWeedBag)
							DELETE_OBJECT(objWeedBag)
							iSeqDeal = 4
						ENDIF
					ELIF iSeqDeal = 4
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.861
							PROCESS_ENTITY_ATTACHMENTS(mSecurityPed[1].mPed)
							PROCESS_ENTITY_ATTACHMENTS(mpDealerGoons[1].mPed)
							DETACH_ENTITY(objCash)
							DELETE_OBJECT(objCash)
							iSeqDeal = 5
						ENDIF
					ELIF iSeqDeal = 5
						IF GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.865
							//PROCESS_ENTITY_ATTACHMENTS(mSecurityPed[1].mPed)
							//PROCESS_ENTITY_ATTACHMENTS(mpDealerGoons[1].mPed)
							//DETACH_ENTITY(objWeedBag)
							//DELETE_OBJECT(objWeedBag)
							iSeqDeal = 6
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//0.261 - dealer get weed out
			//0.846 - dealer pass weed
			
			//0.821 - guard get cash out
			//0.849 - guard pass cash
			
			//0.861 - dealer put cash away
			//0.865 - guard put weed away
			/*
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1071.561768,-787.900269,56.772301>>, <<1071.649658,-793.435669,59.302433>>, 3.250000)
			AND IS_PED_IN_COVER(PLAYER_PED_ID())
			AND IS_CELLPHONE_CAMERA_IN_USE()
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
				FREEZE_ENTITY_POSITION(mvDealerCar.mVehicle,TRUE)
			ELSE
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				FREEZE_ENTITY_POSITION(mvDealerCar.mVehicle,FALSE)
			ENDIF
			*/
			IF bDoFirstConvoLine = FALSE
				IF GET_GAME_TIMER() > iTimerTakePhotoStage + 1000
					//PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_DEALER", "PAP3_DEALER_1", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
					bDoFirstConvoLine = TRUE
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_PRIN")		
					bGoGoDealerConvo = TRUE
				ENDIF
			ENDIF
			
			IF bGoGoDealerConvo	
				IF bKickedOffDealerConvo = FALSE
					//IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "pap3bau", "PAP3_DEALER", "PAP3_DEALER_2",CONV_PRIORITY_HIGH)
					IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_DEALER", CONV_PRIORITY_HIGH)
						issDeal = CREATE_SYNCHRONIZED_SCENE(mvSecurityCars[0].vStartPos,<<0,0,mvSecurityCars[0].fStartHeading>>)
						IF IS_PED_UNINJURED(mpDrugDealer.mPed)	
							TASK_SYNCHRONIZED_SCENE(mpDrugDealer.mPed, issDeal, "rcmpaparazzo_3big_1", "_action_dealer_a", 2, REALLY_SLOW_BLEND_OUT)
						ENDIF
						IF IS_PED_UNINJURED(mpDealerGoons[1].mPed)		
							TASK_SYNCHRONIZED_SCENE(mpDealerGoons[1].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_dealer_b", 2, REALLY_SLOW_BLEND_OUT)
						ENDIF	
						IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
							TASK_SYNCHRONIZED_SCENE(mSecurityPed[0].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_guard_a", 2, REALLY_SLOW_BLEND_OUT)
						ENDIF
						IF IS_PED_UNINJURED(mSecurityPed[1].mPed)		
							TASK_SYNCHRONIZED_SCENE(mSecurityPed[1].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_guard_b", 2, REALLY_SLOW_BLEND_OUT)
						ENDIF
						bKickedOffDealerConvo = TRUE
					ENDIF
				ELSE
					// Start the princess a little bit after the rest of the peds, so she has time to finish playing her scenario.
					IF NOT bPrincessStartedSyncedScene
						IF IS_SYNCHRONIZED_SCENE_RUNNING(issDeal)
						AND GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.04//0.045753
							IF IS_PED_UNINJURED(mpPrincess.mPed)
								TASK_SYNCHRONIZED_SCENE(mpPrincess.mPed, issDeal, "rcmpaparazzo_3big_1", "_action_princess", 1, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, 1)
								bPrincessStartedSyncedScene = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(issDeal)
					OR GET_SYNCHRONIZED_SCENE_PHASE(issDeal) >= 0.893 //0.898
						bLeavingScene = TRUE
						CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						LEAVE_DRUG_SCENE()	
						ForceDriveAway()
					ENDIF
					/*
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT bWhosThatGuy	
						bLeavingScene = TRUE
						CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						LEAVE_DRUG_SCENE()	
						ForceDriveAway()
					ENDIF
					*/
					
					IF bPhotoSent
					AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<<1087.2966, -791.3091, 57.2626>>,60)
						bLeavingScene = TRUE
						CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						LEAVE_DRUG_SCENE()	
						ForceDriveAway()
					ENDIF
					
					label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()	
					IF ARE_STRINGS_EQUAL(label,"PAP3_DEALER_23") 
						KILL_FACE_TO_FACE_CONVERSATION()			
					ENDIF
					IF NOT bSkippedSS	
						IF ARE_STRINGS_EQUAL(label,"PAP3_DEALER_20") 
						AND IS_SYNCHRONIZED_SCENE_RUNNING(issDeal)
						AND GET_SYNCHRONIZED_SCENE_PHASE(issDeal) < 0.758	
							IF IS_PED_UNINJURED(mpPrincess.mPed)
								TASK_SYNCHRONIZED_SCENE(mpPrincess.mPed, issDeal, "rcmpaparazzo_3big_1", "_action_princess", 1, REALLY_SLOW_BLEND_OUT)
							ENDIF
							IF IS_PED_UNINJURED(mpDrugDealer.mPed)	
								TASK_SYNCHRONIZED_SCENE(mpDrugDealer.mPed, issDeal, "rcmpaparazzo_3big_1", "_action_dealer_a", 1, REALLY_SLOW_BLEND_OUT)
							ENDIF
							IF IS_PED_UNINJURED(mpDealerGoons[1].mPed)		
								TASK_SYNCHRONIZED_SCENE(mpDealerGoons[1].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_dealer_b", 1, REALLY_SLOW_BLEND_OUT)
							ENDIF	
							IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
								TASK_SYNCHRONIZED_SCENE(mSecurityPed[0].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_guard_a", 1, REALLY_SLOW_BLEND_OUT)
							ENDIF
							IF IS_PED_UNINJURED(mSecurityPed[1].mPed)		
								TASK_SYNCHRONIZED_SCENE(mSecurityPed[1].mPed, issDeal, "rcmpaparazzo_3big_1", "_action_guard_b", 1, REALLY_SLOW_BLEND_OUT)
							ENDIF
							SET_SYNCHRONIZED_SCENE_PHASE(issDeal,0.758)
							bSkippedSS = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mpPrincess.mPed)) 
			//PRINTNL()
			//PRINTFLOAT(GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID())) 
			//PRINTNL()
			//restricted
			//<<1071.380859,-793.385376,57.051506>>, <<1101.542725,-793.399780,61.012684>>, 11.750000  //main
			//<<1095.905151,-786.595581,57.103203>>, <<1114.590210,-786.716309,60.150230>>, 15.250000  //round left side
			//partly restricted
			//<<1051.790771,-793.230713,57.141796>>, <<1071.547852,-793.179810,60.550629>>, 11.750000  //box past dealer car
			//<<1064.095337,-784.569458,56.837673>>, <<1061.792358,-789.858826,60.012680>>, 11.750000  //angle check alley1
			//<<1058.489624,-785.692810,53.262684>>, <<1057.049683,-791.673950,60.012684>>, 9.500000   //angle check alley1

			IF bLeavingScene = FALSE
				
				IF b_Correct_Pic_Taken	
					IF NOT bPhotoSent	
						IF GET_GAME_TIMER() > iTimerPicTaken + 15000
							IF NOT bHelpPrint8
							
								REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0, REPLAY_IMPORTANCE_LOW)
							
								PRINT_HELP("PAP3_HELP8") // Send the photo to Beverly.
								bHelpPrint8 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bSetRel
					IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
						SET_PED_SEEING_RANGE(mSecurityPed[0].mPed,150)
					ENDIF
					IF IS_PED_UNINJURED(mSecurityPed[1].mPed)	
						SET_PED_SEEING_RANGE(mSecurityPed[1].mPed,150)
					ENDIF	
					IF IS_PED_UNINJURED(mSecurityPed[2].mPed)	
						SET_PED_SEEING_RANGE(mSecurityPed[2].mPed,150)
					ENDIF
					IF IS_PED_UNINJURED(mSecurityPed[3].mPed)	
						SET_PED_SEEING_RANGE(mSecurityPed[3].mPed,150)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_ALIVE(mpPrincess.mPed)
				AND NOT IS_ENTITY_OCCLUDED(mpPrincess.mPed)
					MANAGE_PHONE_HELP()
				ENDIF
					
				//left alley
				IF bAlleyWarning = FALSE	
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.693970,-779.025024,54.929989>>, <<1104.670044,-779.171387,60.324333>>, 5.000000)  
						IF IS_PED_UNINJURED(mSecurityPed[3].mPed)
							TASK_LOOK_AT_ENTITY(mSecurityPed[3].mPed,PLAYER_PED_ID(),-1)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mSecurityPed[3].mPed,"PAP3B_BCAA","Paparazzo3BBodyGuard1","SPEECH_PARAMS_STANDARD")
							bAlleyWarning = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.693970,-779.025024,54.929989>>, <<1104.670044,-779.171387,60.324333>>, 5.000000)  
						bSecPed3AchHead = FALSE
						++iPlayerInLeftAlley
						IF iSeqSecThreaten = 0
							IF iPlayerInLeftAlley >= 50
								IF IS_PED_UNINJURED(mSecurityPed[3].mPed)	
									GIVE_WEAPON_TO_PED(mSecurityPed[3].mPed,WEAPONTYPE_COMBATPISTOL,-1,TRUE,TRUE)
									GIVE_WEAPON_COMPONENT_TO_PED(mSecurityPed[3].mPed,WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
									TASK_AIM_GUN_AT_ENTITY(mSecurityPed[3].mPed,PLAYER_PED_ID(),8000)
									iSeqSecThreaten = 1
								ENDIF
							ENDIF
						ELIF iSeqSecThreaten = 1
							IF iPlayerInLeftAlley >= 150
								IF IS_PED_UNINJURED(mSecurityPed[3].mPed)	
									TASK_COMBAT_PED(mSecurityPed[3].mPed,PLAYER_PED_ID())
								ENDIF
								IF bSetRel = FALSE	
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
									bSetRel = TRUE
								ENDIF
								iSeqSecThreaten = 2
							ENDIF
						ENDIF
					ELSE
						IF NOT bSecPed3AchHead
						AND NOT bSetRel	
							IF IS_PED_UNINJURED(mSecurityPed[3].mPed)		
								IF GET_SCRIPT_TASK_STATUS(mSecurityPed[3].mPed,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
									TASK_ACHIEVE_HEADING(mSecurityPed[3].mPed,0)
									iSeqSecThreaten = 0
									bSecPed3AchHead = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//Jump down off roof - left side
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.718994,-786.146667,57.152645>>, <<1095.790283,-786.231689,60.532429>>, 9.750000)
				AND (GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID()) > 1 OR IS_PED_FACING_PED(mSecurityPed[3].mPed,PLAYER_PED_ID(),90))
					IF bSetRel = FALSE
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF
					IF IS_PED_UNINJURED(mSecurityPed[3].mPed)		
						IF NOT IS_PED_FACING_PED(mSecurityPed[3].mPed,PLAYER_PED_ID(),30)
							TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[3].mPed,PLAYER_PED_ID())
							vLastPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							iTimerGuard3Investigate = GET_GAME_TIMER()
							iCounterGaurd3Investigate = 1
						ENDIF
					ENDIF
				ENDIF
				
				IF iCounterGaurd3Investigate = 1
				AND GET_GAME_TIMER() > iTimerGuard3Investigate + 1000
				AND IS_PED_UNINJURED(mSecurityPed[3].mPed)			
					TASK_FOLLOW_NAV_MESH_TO_COORD(mSecurityPed[3].mPed,vLastPlayerCoords,1,-1,0.25)
					iCounterGaurd3Investigate = 2
				ENDIF
					
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1078.844604,-796.393738,57.012592>>, <<1091.735229,-796.596375,60.382118>>, 6.000000)	//near landstalker
					IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
						IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
							TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
						ENDIF
					ENDIF
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
					bSetRel = TRUE
				ENDIF	
					
				IF bSetRel = FALSE	
					IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<1114.696777,-787.084778,56.648468>>, <<1051.763672,-787.160950,64.140556>>, 23.000000) //Whole of the hypermarket
						IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
							IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
								TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
							ENDIF
						ENDIF
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1071.380859,-793.385376,57.051506>>, <<1101.542725,-793.399780,61.012684>>, 11.750000) //main
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1095.905151,-786.595581,57.103203>>, <<1114.590210,-786.716309,60.150230>>, 15.250000) //round left side
					OR (DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<1071.380859,-793.385376,57.051506>>, <<1101.542725,-793.399780,61.012684>>, 11.750000)) //main
					OR (DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<1095.905151,-786.595581,57.103203>>, <<1114.590210,-786.716309,60.150230>>, 15.250000)) //round left side
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF
					/*
					IF IS_PED_SHOOTING(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.696777,-787.084778,56.648468>>, <<1051.763672,-787.160950,64.140556>>, 23.000000)  //Whole of the hypermarket
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
							bSetRel = TRUE
						ENDIF
					ENDIF
					*/
					IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())	
						//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1117.922729,-791.346252,55.425465>>, <<1060.041992,-791.870850,76.262589>>, 31.500000)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1051.457886,-794.515564,55.685654>>, <<1117.848267,-794.298706,77.441833>>, 33.750000)
							IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
								IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
									TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
								ENDIF
							ENDIF
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
							bSetRel = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mSecurityPed[0].mPed) < 12
					AND GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID()) > 20
						IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
							TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
						ENDIF
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF
				ENDIF
			
				IF bWhosThatGuy = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1051.790771,-793.230713,57.141796>>, <<1071.547852,-793.179810,60.550629>>, 11.750000) //box past dealer car
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1064.095337,-784.569458,56.837673>>, <<1061.792358,-789.858826,60.012680>>, 11.750000) //angle check alley1
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1058.489624,-785.692810,53.262684>>, <<1057.049683,-791.673950,60.012684>>, 9.500000) //angle check alley1
						IF GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID()) > 1.0	
							iFramesPlayerMakingNoise += 5 //10
						ENDIF
						IF bSweetTxtSent = TRUE
							iFramesPlayerMakingNoise += 3 //5
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1051.506592,-792.271851,60.622086>>, <<1070.429443,-792.266907,57.012592>>, 9.500000)  //Out in the open
							iFramesPlayerMakingNoise += 2 //3
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								iFramesPlayerMakingNoise += 50 //5
							ENDIF
						ENDIF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							iFramesPlayerMakingNoise += 5
						ENDIF
						iFramesPlayerMakingNoise += 3  //2
						IF iFramesPlayerMakingNoise > 1000		
							IF bWhosThatGuy = FALSE
								IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
									//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
									TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
									//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
									IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
										TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
									ENDIF
									iTimerSecWary = GET_GAME_TIMER()
								ENDIF
								bWhosThatGuy = TRUE
							ENDIF
						ENDIF
					ENDIF
					//next to dealer car
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1067.674805,-788.586304,53.271336>>, <<1067.798218,-798.870117,60.040825>>, 8.250000) 
						IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
							++iFramesPlayerNotInCoverNextToCar
							IF iFramesPlayerNotInCoverNextToCar > 70		
								IF bWhosThatGuy = FALSE
									IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
										//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
										TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
										//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
										IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
											TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
										ENDIF
										iTimerSecWary = GET_GAME_TIMER()
									ENDIF
									bWhosThatGuy = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ENTITY_ALIVE(mvDealerCar.mVehicle)
					AND IS_VEHICLE_ALARM_ACTIVATED(mvDealerCar.mVehicle)
						IF bWhosThatGuy = FALSE
							IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
								//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
								TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
								//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
								IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
									TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
								ENDIF
								iTimerSecWary = GET_GAME_TIMER()
							ENDIF
							bWhosThatGuy = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1084.464966,-800.133667,57.396507>>, <<1081.144897,-800.167725,59.990376>>, 1.500000)
					AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
						IF bWhosThatGuy = FALSE
							IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
								//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
								TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
								//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
								IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
									TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
								ENDIF
								iTimerSecWary = GET_GAME_TIMER()
							ENDIF
							bWhosThatGuy = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//Shed area round back
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1087.937256,-793.123901,54.772804>>, <<1100.255493,-793.127258,61.012684>>, 12.250000) 
				OR (DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<1087.937256,-793.123901,54.772804>>, <<1100.255493,-793.127258,61.012684>>, 12.250000)) 	
					IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
						IF NOT IS_PED_FACING_PED(mSecurityPed[1].mPed,PLAYER_PED_ID(),45)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[1].mPed,FALSE)
							TASK_LOOK_AT_ENTITY(mSecurityPed[1].mPed,PLAYER_PED_ID(),-1)
							TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[1].mPed,PLAYER_PED_ID(),-1)
						ENDIF
					ENDIF
				ENDIF

				//If done warning - kick off after a few secs
				IF bWhosThatGuy = TRUE
					IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
						SET_PED_SEEING_RANGE(mSecurityPed[0].mPed,100)
					ENDIF
					IF GET_GAME_TIMER() > iTimerSecWary + 3500//2500
						IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
							IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
								TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
							ENDIF
						ENDIF
						IF bSetRel = FALSE
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
							bSetRel = TRUE
						ENDIF
					ELSE
						IF GET_GAME_TIMER() > iTimerSecWary + 1200
							IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
								IF IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),50)
									IF GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK
										IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
											CLEAR_PED_TASKS(mSecurityPed[1].mPed)
											TASK_LOOK_AT_ENTITY(mSecurityPed[1].mPed,PLAYER_PED_ID(),-1)
										ENDIF
										GIVE_WEAPON_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_COMBATPISTOL,-1,TRUE,TRUE)
										GIVE_WEAPON_COMPONENT_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
										TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),5000)
										//KILL_FACE_TO_FACE_CONVERSATION()
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mSecurityPed[0].mPed,"PAP3B_BDAA","Paparazzo3BBodyGuard1","SPEECH_PARAMS_FORCE")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ELSE	
				IF NOT bPhotoSent	
					IF b_Correct_Pic_Taken
						IF NOT bHelpPrint8
							REPLAY_RECORD_BACK_FOR_TIME(6.0, 6.0, REPLAY_IMPORTANCE_LOW)
							PRINT_HELP("PAP3_HELP8") // Send the photo to Beverly.
							bHelpPrint8 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			INT i

			FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
				IF IS_PED_UNINJURED(mpDealerGoons[i].mPed)
					IF IS_PED_IN_COMBAT(mpDealerGoons[i].mPed)
						IF bSetRel = FALSE
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
							bSetRel = TRUE
						ENDIF
						IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
							IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed)
								TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bSetRel = FALSE
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF	
					IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
						IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed)
							TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
				
			FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
				IF IS_PED_UNINJURED(mSecurityPed[i].mPed)
					IF bSetRel = FALSE
						SET_PED_RESET_FLAG(mSecurityPed[i].mPed,PRF_DisableSeeThroughChecksWhenTargeting,TRUE)
					ENDIF
					IF IS_PED_IN_COMBAT(mSecurityPed[i].mPed)	
						IF IS_PED_UNINJURED(mpPrincess.mPed)	
							IF IS_ENTITY_ALIVE(mvSecurityCars[0].mVehicle)	
								IF GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> WAITING_TO_START_TASK	
									TASK_LOOK_AT_ENTITY(mpPrincess.mPed,PLAYER_PED_ID(),-1)
									//SET_PED_PANIC_EXIT_SCENARIO(mpPrincess.mPed,GET_ENTITY_COORDS(PLAYER_PED_ID()))
									TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(mpPrincess.mPed,mvSecurityCars[0].mVehicle,"PAP3_Security1",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
									IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a")
										STOP_ANIM_TASK(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_a",NORMAL_BLEND_OUT)
									ENDIF
									IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b")
										STOP_ANIM_TASK(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_b",NORMAL_BLEND_OUT)
									ENDIF
									IF IS_ENTITY_PLAYING_ANIM(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c")
										STOP_ANIM_TASK(mpPrincess.mPed,"amb@world_human_smoking@female@idle_a","idle_c",NORMAL_BLEND_OUT)
									ENDIF
									IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
										SET_PED_SEEING_RANGE(mSecurityPed[0].mPed,150)
									ENDIF
									IF IS_PED_UNINJURED(mSecurityPed[1].mPed)	
										SET_PED_SEEING_RANGE(mSecurityPed[1].mPed,150)
									ENDIF	
									IF IS_PED_UNINJURED(mSecurityPed[2].mPed)	
										SET_PED_SEEING_RANGE(mSecurityPed[2].mPed,150)
									ENDIF
									IF IS_PED_UNINJURED(mSecurityPed[3].mPed)	
										SET_PED_SEEING_RANGE(mSecurityPed[3].mPed,150)
									ENDIF
									SAFE_DELETE_OBJECT(objCash)
									//SAFE_DELETE_OBJECT(objWeedBag)
									SAFE_REMOVE_BLIP(biMissionBlip)
									bConversationActive = FALSE
									bMainObjectiveDisplayed = FALSE
									IF IS_PED_UNINJURED(mSecurityPed[2].mPed)
										TASK_COMBAT_PED(mSecurityPed[2].mPed,PLAYER_PED_ID(),COMBAT_PED_PREVENT_CHANGING_TARGET)
									ENDIF
									IF bPhotoSent = TRUE	
										MissionStage = MS_ESCAPE_SECURITY
										eSubStage = SS_SETUP
									ELSE
										IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mSecurityPed[1].mPed,FALSE)
										ENDIF
										SET_FAIL_REASON(FAIL_SPOTTED)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bSetRel = FALSE
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
						bSetRel = TRUE
					ENDIF
				ENDIF
			ENDFOR
			
			HANDLE_AUTO_CROUCHING()
		
			IF b_Correct_Pic_Taken = TRUE
				IF bPhotoSent = FALSE
					IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_BEVERLY)
						iTimerPicTaken = GET_GAME_TIMER()
						HANG_UP_AND_PUT_AWAY_PHONE()
						BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
						ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
						//PRINT_NOW("PAP3_LEAVE", DEFAULT_GOD_TEXT_TIME, 1) //"Leave the area."
						bPhotoSent = TRUE
					ELSE
						IF bPicTaken = TRUE	
							IF bWrongContact = FALSE
								IF HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT()	
									PRINT_HELP("PAP3_HELP7") // The picture was sent to the wrong contact.
									iTimerPicTaken = GET_GAME_TIMER()
									bWrongContact = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bLeavingScene = FALSE	
						IF bSweetTxtSent = FALSE
							IF GET_GAME_TIMER() > iTimerPicTaken + 6000 //4000	
								IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT5",TXTMSG_UNLOCKED)	//Sweeeeet! Get the fuck out of there!
									bSweetTxtSent = TRUE
								ENDIF
							ENDIF
						ELSE
							IF bPrintLeaveArea = FALSE
								IF GET_GAME_TIMER() > iTimerPicTaken + 4800
									//PRINT_NOW("PAP3_LEAVE", DEFAULT_GOD_TEXT_TIME, 1) //"Leave the area."
									bPrintLeaveArea = TRUE
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(mpPrincess.mPed,PLAYER_PED_ID(),60)
								root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
								IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER") 	
									KILL_FACE_TO_FACE_CONVERSATION()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF bLeavingScene = FALSE	
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_LEAVE")	
						IF bSweetTxtSent = TRUE
						AND GET_GAME_TIMER() > iTimerPicTaken + 7500	
							//Player hanging around next to the car after sending photo - alarm goes off
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1071.561768,-787.900269,56.772301>>, <<1071.649658,-793.435669,59.302433>>, 3.250000)
							AND IS_PED_IN_COVER(PLAYER_PED_ID())
								/*
								IF bSetRel = FALSE
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPrincess,RELGROUPHASH_PLAYER)
									bSetRel = TRUE
								ENDIF
								*/
								IF IS_ENTITY_ALIVE(mvDealerCar.mVehicle)	
									IF bCarAlarm = FALSE	
										START_VEHICLE_ALARM(mvDealerCar.mVehicle)
										IF bWhosThatGuy = FALSE
											IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
												//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
												TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
												//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
												IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
													TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
												ENDIF
												iTimerSecWary = GET_GAME_TIMER()
											ENDIF
											bWhosThatGuy = TRUE
										ENDIF
										bCarAlarm = TRUE
									ENDIF
								ENDIF
							ENDIF
							//Player hanging around on the roof after sending photo
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1095.302612,-789.975098,62.402073>>, <<1078.775879,-789.993408,64.409988>>, 1.500000) //roof (top)
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1078.399170,-785.052551,60.169865>>, <<1071.268188,-785.085938,62.198814>>, 4.250000) //roof (right side)
								++iFramesPlayerOnRoof	
								IF iFramesPlayerOnRoof > 50			
									IF bWhosThatGuy = FALSE
										IF IS_ENTITY_ALIVE(mSecurityPed[0].mPed)
											//SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mSecurityPed[0].mPed)
											TASK_LOOK_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
											//TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1,TRUE)
											IF NOT IS_PED_FACING_PED(mSecurityPed[0].mPed,PLAYER_PED_ID(),20)
												TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
											ENDIF
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mSecurityPed[0].mPed,"PAP3B_BDAA","Paparazzo3BBodyGuard1","SPEECH_PARAMS_STANDARD")
											iTimerSecWary = GET_GAME_TIMER()
										ENDIF
										bWhosThatGuy = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					/*
					IF IS_PED_UNINJURED(mpPrincess.mPed)
					AND IS_PED_IN_ANY_VEHICLE(mpPrincess.mPed)	
						IF IS_ENTITY_IN_ANGLED_AREA(mpPrincess.mPed, <<1158.520142,-759.373901,56.322784>>, <<1116.868652,-759.690796,61.706322>>, 13.750000)
							IF GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								TASK_VEHICLE_MISSION_PED_TARGET(mpPrincess.mPed,GET_VEHICLE_PED_IS_IN(mpPrincess.mPed),PLAYER_PED_ID(),MISSION_FLEE,50.0,DRIVINGMODE_AVOIDCARS,20.0,20.0)
								ePrincessState = SP_WANDER
								MissionStage = MS_END_PHONE_CALL
								eSubStage = SS_SETUP
							ENDIF
						ENDIF
					ENDIF
					*/
					IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
					AND IS_PED_IN_ANY_VEHICLE(mSecurityPed[0].mPed)	
						IF IS_ENTITY_IN_ANGLED_AREA(mSecurityPed[0].mPed, <<1158.520142,-759.373901,56.322784>>, <<1116.868652,-759.690796,61.706322>>, 13.750000)
							IF GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								TASK_VEHICLE_MISSION_PED_TARGET(mSecurityPed[0].mPed,GET_VEHICLE_PED_IS_IN(mSecurityPed[0].mPed),PLAYER_PED_ID(),MISSION_FLEE,50.0,DRIVINGMODE_AVOIDCARS,20.0,20.0)
								ePrincessState = SP_WANDER
								MissionStage = MS_END_PHONE_CALL
								eSubStage = SS_SETUP
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(mpPrincess.mPed)
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPrincess.mPed,209)
						MissionStage = MS_END_PHONE_CALL
						eSubStage = SS_SETUP
					ENDIF
				ENDIF
			ELSE
				IF bPicTaken = TRUE
					IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_BEVERLY)
						IF bPicSent = FALSE
							iTimerPicTaken = GET_GAME_TIMER()
							bPicSent = TRUE
						ELSE
							IF GET_GAME_TIMER() > iTimerPicTaken + 4000	
								IF bTooFarAway = FALSE	
									IF iBevTxtsBadPic = 0
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT1",TXTMSG_UNLOCKED)	//WTF is that? Get a decent pic.
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsBadPic = 1
										ENDIF
									ELIF iBevTxtsBadPic = 1
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT2",TXTMSG_UNLOCKED)	//Come on man. Get another one
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsBadPic = 2
										ENDIF
									ELIF iBevTxtsBadPic = 2
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT3",TXTMSG_UNLOCKED)	//What's that meant to be?
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsBadPic = 3
										ENDIF
									ELIF iBevTxtsBadPic = 3
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT4",TXTMSG_UNLOCKED)	 //You're not even tring now.
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsBadPic = 4
										ENDIF
									ENDIF	
								ELSE
									IF iBevTxtsTooFarAway = 0
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT8",TXTMSG_UNLOCKED)	//Can you get any closer man?
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsTooFarAway = 1
										ENDIF
									ELIF iBevTxtsTooFarAway = 1
										IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT7",TXTMSG_UNLOCKED)	//I can hardly see her! Get closer
											CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
											bPicTaken = FALSE
											iBevTxtsTooFarAway = 2
										ENDIF
									ENDIF
									bTooFarAway = FALSE	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
					IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
						CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
						bPicTaken = TRUE
						bPicSent = FALSE
						IF IS_ENTITY_ON_SCREEN(mpPrincess.mPed)	
							IF IS_PHOTO_OK()	
								IF IS_PRINCESS_TAKING_A_TOKE()
									#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_MISSION, "STATS >>>> PHOTO OF DEAL <<<< ")
									#ENDIF		
									INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3B_PHOTO_OF_USE)
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST) //Princess buying drugs (whilst being photographed).
								ENDIF
								BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
								ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
								SAFE_REMOVE_BLIP(biMissionBlip)
								iTimerPicTaken = GET_GAME_TIMER()
								b_Correct_Pic_Taken = TRUE
							ELSE
								BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
								ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
								//PRINT_NOW("PAP3_NOPRIN",DEFAULT_GOD_TEXT_TIME,5) // The princess is not in the photo. (Doing this with texts from Beverly)
								CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
								iTimerPicTaken = GET_GAME_TIMER()
							ENDIF
						ELSE
							BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
							ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
							//PRINT_NOW("PAP3_CENT1",DEFAULT_GOD_TEXT_TIME,5) // The princess needs to be in the center of the photo. (Doing this with texts from Beverly)
							CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
							iTimerPicTaken = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPrincessSceneCreated = TRUE
				IF NOT IS_ENTITY_ALIVE(mpPrincess.mPed)	
					SET_FAIL_REASON(FAIL_PRINCESS_KILLED)
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpPrincess.mPed,PLAYER_PED_ID())
						SET_FAIL_REASON(FAIL_PRINCESS_INJURED)
					ENDIF
				ENDIF
			ENDIF
				
		BREAK
		
		CASE SS_CLEANUP	
		
			CLEAR_PRINTS()
			
			SAFE_REMOVE_BLIP(biMissionBlip)
			
			bConversationActive = FALSE
			
			bMainObjectiveDisplayed = FALSE
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player leaving ara undetected after taking the photo //This stage isn't used anymore - didn't want to remove it
PROC LEAVE_AREA() 
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			CPRINTLN(DEBUG_MISSION, "ENTERING LEAVE THE AREA 2000")
			IF bMainObjectiveDisplayed = FALSE
				//PRINT_NOW("PAP3_LEAVE", DEFAULT_GOD_TEXT_TIME, 1) //"Leave the area undetected."
				bMainObjectiveDisplayed = TRUE
			ENDIF
				
			iLeaveSceneTimer = GET_GAME_TIMER() + 15000
			iNoticeInAlleyTimer = GET_GAME_TIMER() + 5000 //1000
			iTriggerPhoneTimer = GET_GAME_TIMER() + 10000
			
			bBeverlyCalled = FALSE
			
			SAFE_REMOVE_BLIP(biMissionBlip)
	
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			
			HANDLE_PRINCESS_SCENE()
			HANDLE_CONTACT()
			IF bMissionFailed
				EXIT
			ENDIF
			IF bSecurityAttacking = FALSE AND bSecurityNoticedPlayer = FALSE
				IF GET_GAME_TIMER() > iLeaveSceneTimer AND bLeavingDrugScene = FALSE //MAKE PEDS LEAVE THE AREA AFTER THIS TIME
					LEAVE_DRUG_SCENE()
					eSubStage = SS_CLEANUP
				ENDIF
			ELSE
				//PLAYER HAS BEEN NOTICED OR THEY ARE ATTACKING, MOVE TO ESCAPE SECURITY
				eSubStage = SS_CLEANUP
			ENDIF
			
			IF (GET_GAME_TIMER() > iNoticeInAlleyTimer AND IS_PLAYER_IN_ALLEY_NOTICE_AREA()) // If the player is in the alley, notice after one second
			OR (GET_GAME_TIMER() > iTriggerPhoneTimer AND (IS_PLAYER_IN_ALLEY_NOTICE_AREA() OR bSecurityNoticedPlayer)) //IF PLAYER STAYS IN SAME LOCATION FOR 10 SECONDS SECURITY WILL NOTICE HIM 
			OR IS_PLAYER_OUT_LEAVE_AREA()
			
				IF bConversationActive = FALSE
					ADD_PED_FOR_DIALOGUE(sSpeach, BEVERLY_ID, NULL, "BEVERLY")
					ADD_PED_FOR_DIALOGUE(sSpeach, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
					IF IS_PLAYER_IN_ALLEY_NOTICE_AREA() OR bSecurityNoticedPlayer = TRUE
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_LEAVE")	
							IF PLAYER_CALL_CHAR_CELLPHONE(sSpeach, CHAR_BEVERLY, "pap3bau", "PAP3_ENDCAL1", CONV_PRIORITY_VERY_HIGH)
								bConversationActive = TRUE
								bBeverlyCalled = TRUE
								CPRINTLN(DEBUG_MISSION, "PLAYING PHONE CALL")
								IF IS_PED_UNINJURED(mSecurityPed[0].mPed) //MAKE ONE SECURITY GUARD TURN TO LOOK
									CLEAR_PED_TASKS(mSecurityPed[0].mPed)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mSecurityPed[0].mPed,"PAP3B_BFAA","Paparazzo3BBodyGuard1","SPEECH_PARAMS_STANDARD")  //Hey, you hear that? Who's down there? 
									TASK_TURN_PED_TO_FACE_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
								ENDIF
							ENDIF
						//ENDIF
					ELSE
						IF bSecurityAttacking = FALSE AND bSecurityNoticedPlayer = FALSE
							LEAVE_DRUG_SCENE()
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
				ELSE
					IF bPlayerSpotted = FALSE
						if CAN_SECURITY_SEE_PLAYER()
							bPlayerSpotted = TRUE
						ELSE
							ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[0].mPed, "Paparazzo3BBodyGuard1")
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR4","PAP3_SECUR4_1",CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								NOTICE_PLAYER_EVENT(FALSE)
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
					ELSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ADD_PED_FOR_DIALOGUE(sSpeach, PRINCESS_ID, mpPrincess.mPed, "PRINCESS")
						IF CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_PANIC", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
							NOTICE_PLAYER_EVENT(TRUE) //MAKES SECURITY NOTICE THE PLAYER
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF			
		BREAK
		
		CASE SS_CLEANUP	
		
			CLEAR_PRINTS()
			
			SAFE_REMOVE_BLIP(biMissionBlip)
			
			bMainObjectiveDisplayed = FALSE
						
			IF bSecurityAttacking = TRUE OR bSecurityNoticedPlayer = TRUE
				SetStage(MS_ESCAPE_SECURITY)
			ELSE
				LEAVE_DRUG_SCENE()
				SetStage(MS_END_PHONE_CALL)
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player having to escape the security PEDS
PROC ESCAPE_SECURITY() 
	
	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()	
	TEXT_LABEL_23 root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
		// Kill dealer convo if it's still ongoing
		IF ARE_STRINGS_EQUAL(root,"PAP3_DEALER")	
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
		
	INT iSubs
	IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_10")		
	//AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_LEAVE")
		iSubs = ENUM_TO_INT(DISPLAY_SUBTITLES)
	ELSE
		iSubs = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
	ENDIF
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			CPRINTLN(DEBUG_MISSION, "ENTERING ESCAPE SECURITY")
			IF bMainObjectiveDisplayed = FALSE
				PRINT_NOW("PAP3_10", DEFAULT_GOD_TEXT_TIME, 1) //"Lose the ~r~bodyguards.~s~"
				bMainObjectiveDisplayed = TRUE
			ENDIF
			
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3B_PHOTO_SPOTTED)
			
			iSecurityDead = iSecurityDead
			iSecurityBlipped = 0
			iSecurityEvaded = 0
			iSeqGuyDriveRoundBack = 0
			iPrincessShoutLines = 0
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			INT i
			FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
				IF DOES_BLIP_EXIST(mSecurityPed[i].mBlip)
					//SET_BLIP_SCALE(mSecurityPed[i].mBlip,0.7)
				ELSE
					IF IS_ENTITY_ALIVE(mSecurityPed[i].mPed)
						mSecurityPed[i].mBlip = CREATE_PED_BLIP(mSecurityPed[i].mPed,TRUE,FALSE,BLIPPRIORITY_MED)
						++iSecurityBlipped
					ENDIF
				ENDIF
			ENDFOR
			
			IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
			ENDIF
			
			//SET_ROADS_IN_AREA(<<1109.490112,-778.548096,58.762684>>,<<3.750000,13.500000,2.250000>>,TRUE)
			
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)	
			AND IS_ENTITY_ALIVE(mvSecurityCars[1].mVehicle)	
				SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle,<<0,0,0>>,5)
			ENDIF
			
			SEQUENCE_INDEX seq
			
			OPEN_SEQUENCE_TASK(seq)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
				IF IS_PED_UNINJURED(mpDealerGoons[0].mPed)
					TASK_COMBAT_PED(NULL,mpDealerGoons[0].mPed,COMBAT_PED_PREVENT_CHANGING_TARGET)
				ENDIF
				//IF IS_PED_UNINJURED(mpDrugDealer.mPed)
				//	TASK_COMBAT_PED(NULL,mpDrugDealer.mPed,COMBAT_PED_PREVENT_CHANGING_TARGET)
				//ENDIF
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,FALSE)
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					TASK_COMBAT_PED(NULL,PLAYER_PED_ID())
				ENDIF
			CLOSE_SEQUENCE_TASK(seq)
			
			IF IS_PED_UNINJURED(mSecurityPed[1].mPed)	
				SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[1].mPed,CA_CAN_CHARGE,TRUE)
				TASK_PERFORM_SEQUENCE(mSecurityPed[1].mPed,seq)
			ENDIF
			
			CLEAR_SEQUENCE_TASK(seq)
			
			iPanicConvo = 0
			bConversationActive = FALSE			
			eSubStage = SS_UPDATE

		BREAK
		
		CASE SS_UPDATE
			
			IF IS_PED_UNINJURED(mSecurityPed[0].mPed)	
				IF NOT IS_PED_IN_COMBAT(mSecurityPed[0].mPed,PLAYER_PED_ID())
					TASK_COMBAT_PED(mSecurityPed[0].mPed,PLAYER_PED_ID())
				ENDIF
			ENDIF
				
			IF IS_PED_UNINJURED(mSecurityPed[3].mPed)	
				IF NOT IS_PED_IN_COMBAT(mSecurityPed[3].mPed,PLAYER_PED_ID())
					TASK_COMBAT_PED(mSecurityPed[3].mPed,PLAYER_PED_ID())
				ENDIF
			ENDIF	
			
			IF NOT IS_PED_UNINJURED(mpDealerGoons[0].mPed)
				IF IS_PED_UNINJURED(mSecurityPed[1].mPed)	
					IF NOT IS_PED_IN_COMBAT(mSecurityPed[1].mPed,PLAYER_PED_ID())
						TASK_COMBAT_PED(mSecurityPed[1].mPed,PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
			
			//Guy in the landstalker
			IF IS_PED_UNINJURED(mSecurityPed[2].mPed)	
				IF iSeqGuyDriveRoundBack = 0
					//If player leaves area chase in vehicle
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1114.696777,-787.084778,56.648468>>, <<1051.763672,-787.160950,64.140556>>, 23.000000)  //Whole of the hypermarket
						IF IS_ENTITY_ALIVE(mvSecurityCars[1].mVehicle)	
							IF NOT IS_PED_IN_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle)	
								IF GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(mSecurityPed[2].mPed,SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
									TASK_ENTER_VEHICLE(mSecurityPed[2].mPed,mvSecurityCars[1].mVehicle)
								ENDIF
							ELSE
								//SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[2].mPed,CA_LEAVE_VEHICLES,FALSE)
								TASK_COMBAT_PED(mSecurityPed[2].mPed,PLAYER_PED_ID())
								iSeqGuyDriveRoundBack = 1
							ENDIF
						ENDIF
					ENDIF
				ELIF iSeqGuyDriveRoundBack = 1
					//He's chasing player, let him out if he wants
					IF NOT IS_ENTITY_IN_RANGE_COORDS(mSecurityPed[2].mPed,vPrincessInitialLocation,100)
						//SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[2].mPed,CA_USE_VEHICLE,TRUE)
						//SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[2].mPed,CA_LEAVE_VEHICLES,TRUE)
						iSeqGuyDriveRoundBack = 2
					ENDIF
				ENDIF
			ENDIF
					
			IF bPrincessSceneCreated = TRUE
				IF NOT IS_ENTITY_ALIVE(mpPrincess.mPed)	
					SET_FAIL_REASON(FAIL_PRINCESS_KILLED)
				ELSE                                                                 
					//Stopping guys taking cover by the princess' car before she drives off 
					IF NOT IS_ENTITY_IN_ANGLED_AREA(mpPrincess.mPed, <<1083.386353,-793.401428,55.294998>>, <<1092.125854,-793.567993,60.512684>>, 11.000000)
						IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
							SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[0].mPed,CA_USE_COVER,TRUE)
						ENDIF
						IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
							SET_PED_COMBAT_ATTRIBUTES(mSecurityPed[1].mPed,CA_USE_COVER,TRUE)
						ENDIF
					ENDIF
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpPrincess.mPed,PLAYER_PED_ID())
						SET_FAIL_REASON(FAIL_PRINCESS_INJURED)
					ENDIF
					IF iPanicConvo = 2	
						DO_PRINCESS_SHOUTING()
					ENDIF
				ENDIF
			ENDIF
			
			IF ARE_STRINGS_EQUAL(label, "PAP3_SECUR2_2")
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF iPanicConvo = 1
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR4", "PAP3_SECUR4_3", CONV_PRIORITY_HIGH,INT_TO_ENUM(enumSubtitlesState,iSubs))
						iPanicConvo = 2
					ENDIF
				ELIF iPanicConvo = 0 	
					ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[0].mPed, "Paparazzo3BBodyGuard1")
					ADD_PED_FOR_DIALOGUE(sSpeach, DEALER_ID, mpDrugDealer.mPed, "Paparazzo3BDrugDealer")
					ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_TWO_ID, mSecurityPed[1].mPed, "Paparazzo3BBodyGuard2")
					ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerGoons[0].mPed, "Paparazzo3BDealerGoon1")
					IF IS_ENTITY_ALIVE(mpPrincess.mPed)	
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_SHOCK", "PAP3_SHOCK_5", CONV_PRIORITY_HIGH,INT_TO_ENUM(enumSubtitlesState,iSubs))
							iPanicConvo = 1 	
						ENDIF
					ELSE
						iPanicConvo = 1 	
					ENDIF
				ENDIF
			ENDIF
			
			IF ALL_GUARDS_LOST()	
				eSubStage = SS_CLEANUP
			ENDIF
			
			//Princess flee player after waypoints
			IF IS_PED_UNINJURED(mpPrincess.mPed)
			AND IS_PED_IN_ANY_VEHICLE(mpPrincess.mPed)	
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(GET_VEHICLE_PED_IS_IN(mpPrincess.mPed))
					VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(GET_VEHICLE_PED_IS_IN(mpPrincess.mPed),20)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(mpPrincess.mPed, <<1158.520142,-759.373901,56.322784>>, <<1116.868652,-759.690796,61.706322>>, 13.750000)
					IF GET_SCRIPT_TASK_STATUS(mpPrincess.mPed,SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
						TASK_VEHICLE_MISSION_PED_TARGET(mpPrincess.mPed,GET_VEHICLE_PED_IS_IN(mpPrincess.mPed),PLAYER_PED_ID(),MISSION_FLEE,50.0,DRIVINGMODE_AVOIDCARS,20.0,20.0)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
		
			CLEAR_PRINTS()
			/*
			FOR i = 0 TO NUM_OF_SECURITY_PEDS - 1
				SAFE_REMOVE_BLIP(mSecurityPed[i].mBlip)
				MAKE_PED_FLEE(mSecurityPed[i].mPed)
			ENDFOR
			
			FOR i = 0 TO NUM_OF_DEALER_GOONS - 1
				MAKE_PED_FLEE(mpDealerGoons[i].mPed)
			ENDFOR
			*/
			bMainObjectiveDisplayed = FALSE
			
			//IF PLAYER HAS NOT HAD ENDPHONE CALL THEN DO IT NOW - ELSE PASS MISSION
			IF bHadEndPhoneCall = FALSE
				SetStage(MS_END_PHONE_CALL)
			ELSE
				Script_Passed()
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Deals with the end phone call to player
PROC END_PHONE_CALL()
	
	LEAVE_DRUG_SCENE()
	
	SWITCH eSubStage
		CASE SS_SETUP
	
			CPRINTLN(DEBUG_MISSION, "ENTERING END PHONE CALL 2000")
		 	bHadEndPhoneCall = FALSE
			eSubStage = SS_UPDATE
			IF IS_PED_UNINJURED(mpPrincess.mPed)
			AND IS_ENTITY_IN_RANGE_ENTITY(mpPrincess.mPed,PLAYER_PED_ID(),150)
			AND bLeavingScene = TRUE	
				iLeaveSceneTimer = GET_GAME_TIMER() + 15000
			ELSE	
				iLeaveSceneTimer = GET_GAME_TIMER() + 4001
			ENDIF
			bPrintLoseCops = FALSE
			//ForceDriveAway()
			
		BREAK
		
		CASE SS_UPDATE
			
			//HANDLE_PRINCESS_SCENE()
			
			ForceDriveAway()
			
			//IF ePrincessState = SP_WANDER OR (GET_GAME_TIMER() > iLeaveSceneTimer)
			IF (GET_GAME_TIMER() > iLeaveSceneTimer)
			OR ePrincessState = SP_WANDER	
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID())	= 0
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())AND NOT IS_PED_GETTING_UP(PLAYER_PED_ID()) AND NOT IS_PED_PRONE(PLAYER_PED_ID())
					AND NOT IS_PED_FALLING(PLAYER_PED_ID())
						IF NOT bHadEndPhoneCall
							ADD_PED_FOR_DIALOGUE(sSpeach, BEVERLY_ID, NULL, "BEVERLY")
							ADD_PED_FOR_DIALOGUE(sSpeach, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
							IF bBeverlyCalled
								IF PLAYER_CALL_CHAR_CELLPHONE(sSpeach, CHAR_BEVERLY, "pap3bau", "PAP3_ENDCAL2", CONV_PRIORITY_VERY_HIGH)
									REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.5, REPLAY_IMPORTANCE_LOWEST)
									bHadEndPhoneCall = TRUE
								ENDIF
							ELSE
								IF PLAYER_CALL_CHAR_CELLPHONE(sSpeach, CHAR_BEVERLY, "pap3bau", "PAP3_ENDCALL", CONV_PRIORITY_VERY_HIGH)
									REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.5, REPLAY_IMPORTANCE_LOWEST)
									bHadEndPhoneCall = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF HAS_CELLPHONE_CALL_FINISHED()
									eSubStage = SS_CLEANUP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iLeaveSceneTimer = GET_GAME_TIMER() + 3000
					IF bPrintLoseCops = FALSE
						PRINT_NOW("PAP3B_COPS",DEFAULT_GOD_TEXT_TIME,0)  //Lose the cops.
						bPrintLoseCops = TRUE
					ENDIF
				ENDIF	
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
		
			CLEAR_PRINTS()

			bMainObjectiveDisplayed = FALSE
			
			Script_Passed()
			
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///  Waits for the screen to fade out, then updates the fail reason for the mission    
PROC FAIL_WAIT_FOR_FADE()
	
	SWITCH eSubStage
	
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			
			// remove blips
			SAFE_REMOVE_BLIP(biMissionBlip)
			
			STRING sFailReason
	
			SWITCH eFailReason // print fail reason
			
				CASE FAIL_DEFAULT // no fail reason to display
				BREAK
				
				CASE FAIL_PHOTO_LOST
					sFailReason = "PAP3_08" //The photo opportunity was lost.
				BREAK
				
				CASE FAIL_SPOTTED
					sFailReason = "PAP3_ALERT" //The security was alerted.
				BREAK
				
				CASE FAIL_PRINCESS_INJURED
					sFailReason = "PAP3_INJUR2" //The princess was injured.
				BREAK
				
				CASE FAIL_PRINCESS_KILLED
					sFailReason = "PAP3_KILL1" //The princess was killed.
				BREAK
				
				CASE FAIL_CONTACT_KILLED
					sFailReason = "PAP3_KILL3" //You killed your contact.
				BREAK
				
				CASE FAIL_CONTACT_THREAT
					sFailReason = "PAP3_THREAT" // You threatened your contact.
				BREAK
				
			ENDSWITCH

			IF eFailReason = FAIL_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			bFailDialogue = FALSE
			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			/*	
			IF IS_PED_UNINJURED(mSecurityPed[0].mPed)
				IF GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> PERFORMING_TASK	
				AND	GET_SCRIPT_TASK_STATUS(mSecurityPed[0].mPed,SCRIPT_TASK_AIM_GUN_AT_ENTITY) <> WAITING_TO_START_TASK
					GIVE_WEAPON_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_COMBATPISTOL,-1,TRUE,TRUE)
					GIVE_WEAPON_COMPONENT_TO_PED(mSecurityPed[0].mPed,WEAPONTYPE_COMBATPISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
					TASK_AIM_GUN_AT_ENTITY(mSecurityPed[0].mPed,PLAYER_PED_ID(),-1)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(mSecurityPed[1].mPed)
				IF NOT IS_PED_HEADTRACKING_PED(mSecurityPed[1].mPed,PLAYER_PED_ID())	
					TASK_LOOK_AT_ENTITY(mSecurityPed[1].mPed,PLAYER_PED_ID(),-1)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(mpDrugDealer.mPed)	
				IF NOT IS_PED_HEADTRACKING_PED(mpDrugDealer.mPed,PLAYER_PED_ID())		
					TASK_LOOK_AT_ENTITY(mpDrugDealer.mPed,PLAYER_PED_ID(),-1)
				ENDIF
			ENDIF
			*/
			/*IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					// delete everything here!
					MissionCleanup()
					DELETE_EVERYTHING()
						
					IF IS_SCRIPTED_CONVERSATION_ONGOING()
						KILL_ANY_CONVERSATION()
					ENDIF

					Script_Cleanup(FALSE)
				ENDIF
	
			ELSE*/
				IF bFailDialogue
					IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							// delete everything here!
							MissionCleanup()
							DELETE_EVERYTHING()
								
							IF IS_SCRIPTED_CONVERSATION_ONGOING()
								KILL_ANY_CONVERSATION()
							ENDIF

							Script_Cleanup(FALSE)
						ENDIF
					ENDIF
				ELSE
					IF eFailReason = FAIL_SPOTTED
						IF bSecurityNoticedPlayer
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[0].mPed, "Paparazzo3BBodyGuard1")
								ADD_PED_FOR_DIALOGUE(sSpeach, DEALER_ID, mpDrugDealer.mPed, "Paparazzo3BDrugDealer")
								ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_TWO_ID, mSecurityPed[1].mPed, "Paparazzo3BBodyGuard2")
								ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerGoons[0].mPed, "Paparazzo3BDealerGoon1")
								bFailDialogue = CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR2", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							ENDIF
						ELSE
							//IF IS_PLAYER_UNARMED() // IF PLAYER IS UNARMED SAY THIS DIALOGUE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[1].mPed, "Paparazzo3BBodyGuard1")
									bFailDialogue = PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR4","PAP3_SECUR4_3",CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								ENDIF
							//ELSE // IF PLAYER HAS WEAPON SAY THIS DIALOGUE.
							//	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							//		ADD_PED_FOR_DIALOGUE(sSpeach, GUARD_ONE_ID, mSecurityPed[1].mPed, "Paparazzo3BBodyGuard1")
							//		bFailDialogue = CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_SECUR3",CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
							//	ENDIF
							//ENDIF
						ENDIF
					ELIF eFailReason = FAIL_CONTACT_THREAT
						IF IS_PED_UNINJURED(mpDealerContact.mPed)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ADD_PED_FOR_DIALOGUE(sSpeach, GOON_ID, mpDealerContact.mPed, "Paparazzo3BDealerGoon1")
							ADD_PED_FOR_DIALOGUE(sSpeach, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
							bFailDialogue = CREATE_CONVERSATION(sSpeach, "pap3bau", "PAP3_FAIL", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ELSE
						bFailDialogue = TRUE
					ENDIF
				ENDIF
				// not finished fading out
				// you may want to handle dialogue etc here.
			//ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

//Blocking temp events on dealers and then unblocking when security kick off (or deal is done).
PROC UNBLOCK_DEALERS_TEMP_EVENTS_WHEN_KICKING_OFF()
	
	INT i	
	
	IF bSetRel	
	OR bLeavingScene	
		IF NOT bDealersTempEventsUnblocked	
			IF IS_ENTITY_ALIVE(mpDrugDealer.mPed)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDrugDealer.mPed,FALSE)
			ENDIF
			REPEAT NUM_OF_DEALER_GOONS i 
				IF IS_ENTITY_ALIVE(mpDealerGoons[i].mPed)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpDealerGoons[i].mPed,FALSE)
				ENDIF
			ENDREPEAT
			bDealersTempEventsUnblocked = TRUE
		ENDIF
	ENDIF

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_3B(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	InitVariables()

	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_1) = TRUE	
	OR IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_3A)
		bSimpleHelp = TRUE
	ELSE
		bSimpleHelp = FALSE
	ENDIF

	REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES(TRUE)

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
	
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TH")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		UNBLOCK_DEALERS_TEMP_EVENTS_WHEN_KICKING_OFF()
		
		IF MissionStage = MS_MISSION_FAILED
			FAIL_WAIT_FOR_FADE()
		ELSE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SWITCH missionStage //MAIN MISSION LOOP
				
					CASE MS_INIT
						STAGE_INIT()
					BREAK
					CASE MS_INITIAL_PHONE
						INITIAL_PHONE_CALL()
					BREAK
					
					CASE MS_GO_TO_LOCATION
						GO_TO_LOCATION()
					BREAK
					
					CASE MS_MEET_CONTACT
						MEET_CONTACT()
					BREAK
					
					CASE MS_TAKE_PHOTO
						TAKE_PHOTO()
					BREAK
					
					CASE MS_LEAVE_AREA
						LEAVE_AREA()
					BREAK
					
					CASE MS_ESCAPE_SECURITY
						ESCAPE_SECURITY()
					BREAK
					
					CASE MS_END_PHONE_CALL
						END_PHONE_CALL()
					BREAK
				ENDSWITCH
			ELSE
				SET_FAIL_REASON(FAIL_DEFAULT)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD // STAGE SKIPPING
			if bFinishedSkipping = TRUE
				DEBUG_Check_Debug_Keys() // not skipping stages, check for debug keys
			ELSE
				JumpToStage(eTargetStage) // still skipping stages
			ENDIF
		#ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

