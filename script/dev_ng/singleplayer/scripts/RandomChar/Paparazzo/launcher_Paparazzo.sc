

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Paparazzo.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Paparazzo.sc
//
//		AUTHOR		:	David Roberts / Andrew Minghella
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
//
// 						If multiple missions share same coord or are within close proximity,
//						we should just add one world point, and adjust the tolerance float.
// *****************************************************************************************

// ------------Variables----------------------
CONST_FLOAT 			WORLD_POINT_COORD_TOLERANCE 5.0								// Increased for PAP3 blip offset
INT 					iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST 	// ID to register off-mission cutscene load request with cutscene_controller.
SCENARIO_BLOCKING_INDEX mScenarioBlocker
BLIP_INDEX 				biLeadIn

// ----------Functions -------------------------

/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities=TRUE)

	IF bCleanupEntities
		
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Cleaning up entities in Launcher")
		
		// This means the mission hasn't launched
		// Custom cleanup
		SWITCH sData.eMissionID
		
			CASE RC_PAPARAZZO_1
				// Release bike model if mission didn't launch
				SET_MODEL_AS_NO_LONGER_NEEDED(BEVERLY_BIKE)
				RC_CleanupSceneEntities(sData, FALSE)
			BREAK
			
			CASE RC_PAPARAZZO_2
				
				// Renable paparazzo and scenario areas
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_PAPARAZZI", TRUE)
				REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
				RC_CleanupSceneEntities(sData, FALSE)
			BREAK
			
			CASE RC_PAPARAZZO_3
				
				// Sort Beverly and dumpster objects
				REMOVE_MODEL_HIDE(<< -260.62, 292.13, 90.60 >>, 10.0, PROP_CS_DUMPSTER_01A, TRUE)
				REMOVE_MODEL_HIDE(<< -261.58, 291.66, 92.06 >>, 10.0, PROP_CS_DUMPSTER_LIDR, TRUE)
				REMOVE_MODEL_HIDE(<< -261.54, 292.54, 92.10 >>, 10.0, PROP_CS_DUMPSTER_LIDL, TRUE)
				// Delete Beverly's camera now - the dumpster objects should be released
				SAFE_DELETE_OBJECT(sData.objID[3])
				// Delete everything else
				RC_CleanupSceneEntities(sData, TRUE, FALSE, FALSE)
				
				// Enable paparazzo scenarios 
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_PAPARAZZI", TRUE)
			BREAK
			
			CASE RC_PAPARAZZO_4
				
				// Anim dictionary loaded by initial scene, clean up if mission didn't launch
				REMOVE_ANIM_DICT("rcmpaparazzo_4")
				RC_CleanupSceneEntities(sData, FALSE)
			BREAK
			
			DEFAULT
				// No custom cleanup required for Paparazzo 3A and 3B
			BREAK
		ENDSWITCH
	ENDIF
	
	// Ensure mission areas and scenarios are restored
	SETUP_AREA_FOR_MISSION(sData.eMissionID, FALSE)
	
	// Remove lead-in blip
	IF DOES_BLIP_EXIST(biLeadIn)
		REMOVE_BLIP(biLeadIn)
	ENDIF
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF
	
	// Unload launcher animation dictionary
	REMOVE_LAUNCHER_ANIM_DICT(sData.sAnims)
	
	// Launcher terminated from mission
	IF NOT bCleanupEntities
		
		// Keep paparazzo 3 launcher running whilst mission is active to prevent
		// dumpster popping state (it is hidden in launcher - cannot be passed to mission)
		IF sData.eMissionID = RC_PAPARAZZO_3
			PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Holding on to launcher until Paparazzo 3 cleans up...")
			WHILE GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Paparazzo3")) > 0
				WAIT(0)
			ENDWHILE
		ENDIF
	ENDIF
	
	//B*1574385 - Force update to blip in RandChar Controller if mission wasn't launched
	IF bCleanupEntities
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Creates the initial scene
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)

	
	//Setup the correct initial scene
	SWITCH sData.eMissionID
		CASE RC_PAPARAZZO_1
			IF NOT SetupScene_PAPARAZZO_1(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_PAPARAZZO_2
			IF NOT SetupScene_PAPARAZZO_2(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_PAPARAZZO_3
			IF NOT SetupScene_PAPARAZZO_3(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_PAPARAZZO_3A
			IF NOT SetupScene_PAPARAZZO_3A(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_PAPARAZZO_3B
			IF NOT SetupScene_PAPARAZZO_3B(sData)
				RETURN FALSE
			ENDIF
		BREAK
		CASE RC_PAPARAZZO_4			
			IF NOT SetupScene_PAPARAZZO_4(sData)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)

	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs 	sRCLauncherData			// Scene information to pass to mission script
	VECTOR				 	vInCoords = <<0,0,0>>	// Stores world point location
	
	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update world point
	vInCoords = in_coords.vec_coord[0]

	// Determine which RC mission we are attempting to launch (currently done on a launcher by launcher basis)
	g_eRC_MissionIDs eRCMissions[6]
	eRCMissions[0] = RC_PAPARAZZO_1
	eRCMissions[1] = RC_PAPARAZZO_2
	eRCMissions[2] = RC_PAPARAZZO_3
	eRCMissions[3] = RC_PAPARAZZO_3A
	eRCMissions[4] = RC_PAPARAZZO_3B
	eRCMissions[5] = RC_PAPARAZZO_4
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF
	
	//Pick which mission activated us
	IF NOT DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vInCoords, WORLD_POINT_COORD_TOLERANCE)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
		
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
	
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
	
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF	
	
	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE

	IF sRCLauncherData.eMissionID = RC_PAPARAZZO_2
		mScenarioBlocker = Pap2_Scenario_Blocker()
	ENDIF

	// Clears area of non-mission entities and blood decals
	IF sRCLauncherData.eMissionID != RC_PAPARAZZO_3A			// B*1561087 - area blip so activationRange is set to 90m which causes visible clear area (not needed since only phonecall occurs here)
	AND sRCLauncherData.eMissionID != RC_PAPARAZZO_3B	
		CLEAR_AREA(vInCoords, sRCLauncherData.activationRange, TRUE)
	ENDIF
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		
		WAIT(0)

		//Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
			Script_Cleanup(sRCLauncherData)
		ENDIF
		
		IF sRCLauncherData.eMissionID = RC_PAPARAZZO_2
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "PAP2AUD", "PAP2_AMB", 3, "BEVERLY", 5000)
		ELIF sRCLauncherData.eMissionID = RC_PAPARAZZO_4
			PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "PAP4AUD", "PAP4_AMB", 3, "BEVERLY", 5000)
		ENDIF
		
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vInCoords)
		
		//Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)
			
			// Create mission blip that persists for lead-in scene
			// Original will be removed when RC controller shuts down
			IF DOES_RC_MISSION_HAVE_LEAD_IN(sRCLauncherData.eMissionID)
				CREATE_BLIP_FOR_LEAD_IN(sRCLauncherData.eMissionID, biLeadIn)
			ENDIF
			
			// Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			// Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				
				// No need to clean up entities or cutscene as we should have 
				// passed them over to the mission script
				Script_Cleanup(sRCLauncherData, FALSE)			
			ENDIF	
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

