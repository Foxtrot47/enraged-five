
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "rgeneral_include.sch"
USING "commands_vehicle.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch"
USING "randomChar_public.sch"
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "taxi_functions.sch"
USING "commands_event.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
	  USING "shared_debug.sch"
#ENDIF

USING "CompletionPercentage_public.sch"
USING "RC_Threat_public.sch"

USING "initial_scenes_Paparazzo.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo4.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Franklin bumps into Beverly. 
//							He is filming his reality show and annoys Franklin. 
//							Leave, beat him up or shoot him.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





g_structRCScriptArgs sRCLauncherDataLocal

//********************************************************************************************************************
//											:ENUMS:
//********************************************************************************************************************
// Mission stages
ENUM MISSION_STAGE
	MS_INIT = 0,
	MS_INTRO,
	MS_BEV_ON_FOOT,
	MS_BEV_IN_CAR,
	MS_PASSED
ENDENUM

ENUM MISSION_REQ
	RQ_NONE,
	RQ_CAMERA_CUTSCENE
ENDENUM

ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_MESKIPPED
ENDENUM

ENUM PED_ENUMS
	BEV = 0,
	CAMERAMAN = 1,
	MAKEUPWMAN = 2
ENDENUM

ENUM CAR_STEAL_STAGE
	CSS_READY = 0,
	CSS_START,
	CSS_STOLEN,
	CSS_RETALIATE,
	CSS_RETURN,
	CSS_DONE
ENDENUM

ENUM INTERRUPT_STAGE
	IS_READY,
	IS_INTERRUPTION,
	IS_RESTART,
	IS_DONE
ENDENUM

//****************************************************************************************************************************
//													:CONSTANTS:
//****************************************************************************************************************************
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 2
#ENDIF

CONST_INT FRANKLIN_ID 1
CONST_INT BEVERLY_ID 3
CONST_INT CAMERA_ID 4

CONST_INT BEVS_CASH 2000

CONST_INT BEV_WAIT_TIME 10000

//****************************************************************************************************************************
//													:INDICES, STRUCTs, and anything else:
//****************************************************************************************************************************

//**********************************************INDICES
//BLIP INDICIES
BLIP_INDEX biGOTO

//**********************************************STRUCTs
//Dialogue struc
structPedsForConversation conversationPeds

//DEBUG DECLARATIONS
#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
	INT i_debug_jump_stage
#ENDIF
//****************************************************************************************************************************
//													:MISSION FLOW VARIABLES:
//****************************************************************************************************************************
INT iMissionState = 0
MISSION_STAGE eMissionStage = MS_INIT					//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT

VECTOR vIntroAreaPos1 = <<-498.843964,230.908646,81.103127>>
VECTOR vIntroAreaPos2 = <<-487.096893,230.157562,84.066666>> 
FLOAT fIntroAreaWidth = 11.0

INT iControlTimer = 0
INT iTextTimer = 0
INT iCheckDoorsTimer = 0
INT iPassTimer = 0

CONST_INT NUM_CAMERA_LINES 4
STRING sCameraTalk[NUM_CAMERA_LINES]
INT iCurrentCameraLine = 0
CONST_INT iCameraLineDelay 15000
INT iCameraLineTimer = 0

VECTOR vBevOffset = <<0,0,0>>//<<0.1561, -0.0030, -0.0344>>
VECTOR vBevRot = <<0,0,0>>//<<202.8703, -124.4300, -121.5398>>
VECTOR vCamOffset = <<0.1181, 0.2229, -0.1535>>
VECTOR vCamRot = <<259.2501, 24.0800, -51.2899>>

INT iLeadInTimer = -1
VECTOR vAlternateApproachPos1 = <<-512.978821,225.166489,80.203293>>
VECTOR vAlternateApproachPos2 = <<-463.980530,219.813110,84.147079>> 
FLOAT fAlternateApproachWidth = 18.5
BOOL bShowPlayer = FALSE
INT iShowPlayerTimer = -1

CONST_INT NUM_SHOOTING_LINES 3
STRING sBevShootLines[NUM_SHOOTING_LINES]
INT iCurrentBevShootLine = 0
CONST_INT iShootLineDelay 15000
INT iShootLineTimer = 0

CONST_INT FIGHT_LINE_TIME 5000
INT iFightLineTimer = 0

INT iCameraWalkTimer = 0
BOOL bCameraGuyWalks = FALSE

STRING sMissionAnims = "rcmpaparazzo_4"
STRING sHandsUpAnim = "lift_hands_in_air_loop"
STRING sHandsupFlinch = "MISSCOMMON@HANDS_UP_FLINCH"
STRING sMakeupScaredAnims = "reaction@back_away@f"
STRING sWeaponMoveClipset = "random@escape_paparazzi@standing@"
STRING sBackAwayAnim = "0"
INT iFlinchStage = 0
BOOL bDoFlinchReaction = FALSE
BOOL bNotScaredYet = TRUE
BOOL bFleeFromWeapon = FALSE
BOOL bPlayerAggro = FALSE
BOOL bStopCheckingNewsvan = FALSE
BOOL bBeverlyFleeing = FALSE

INT iBeverlyFilmingScene
BOOL bAllowSynchronisedScene = TRUE
STRING sBevFilmBase = "base_pap"
STRING sCamFilmBase = "base_camman"
STRING sProdFilmBase = "base_prod"
STRING sBevFilmGesture = "gesture_to_cam_pap"
STRING sCamFilmGesture = "gesture_to_cam_camman"
STRING sProdFilmGesture = "gesture_to_cam_prod"
STRING sBevFilmIdle = "idle_pap"
STRING sCamFilmIdle = "idle_camman"
STRING sProdFilmIdle = "idle_prod"
VECTOR vFilmingScenePos = <<-493.764358, 232.17636, 82.057273>>//<< -491.86, 232.22,  82.02 >>//<<-490.894592,232.362991,82.017174>>//<<-490.358093,232.436035,82.032570>>
VECTOR vFilmingSceneRot = <<0,0,0>>

BOOL bStartShootingReaction = TRUE
//BOOL bBeverlyWaiting = TRUE
BOOL bAllowThreatConv = TRUE
//INT iBevWaitTimer
//CONST_INT iBevWaitDelay 2000

CONST_INT CAM_MOVE_TIME 10000
INT iCamMoveTimer = 0
CONST_INT CAM_LOOK_TIME 4000
INT iCamLookTimer = 0
BOOL bFilmBev = FALSE

VECTOR vBevReset = <<-489.379822,232.071320,82.052628>>
VECTOR vCamReset = <<-488.27, 229.89, 83.23>>
VECTOR vProdReset = << -493.3992, 226.2236, 82.1179 >>
VECTOR vBevCarPos = <<-497.99, 224.97, 82.67>>//<<-498.95, 223.78, 82.78>>
VECTOR vVanPos = <<-501.55, 230.70, 83.10>>//<<-508.14, 232.87, 83.06>>
FLOAT fBevReset = 115.15
FLOAT fCamReset = -64.85
FLOAT fProdReset = 296.4216
FLOAT fBevCarHeading = 266.50//265.96
FLOAT fVanHeading = 233.87
VECTOR vPlayerReset = <<-489.4534, 233.7212, 82.0251>>
FLOAT fPlayerReset = 294.5672

CONST_FLOAT fBevEscapeDist 150.0

CAR_STEAL_STAGE eCarStealStage = CSS_READY
INT iCarStealTimer = 0
CONST_INT iCarStealTimeOut 3000
CONST_FLOAT fCarStealDist 20.0
CONST_FLOAT fCarStealReturnDist 10.0
VECTOR vBevRunToPos = <<-494.345795,229.292160,82.114258>>//<<-484.0395, 234.6226, 82.0242>>
VECTOR vMWRunToPos = <<-499.68, 232.26, 82.10>>
BOOL bDoneRetalConv = FALSE
BOOL bDoneReturnConv = FALSE

//GROUP_INDEX giBevGroup

CONST_INT NUM_RESTARTS 3
CONST_INT NUM_INTERRUPTIONS 4
STRING sRestartConv[NUM_RESTARTS]
STRING sInterruptConv[NUM_INTERRUPTIONS]
INT iFinalLine[NUM_INTERRUPTIONS]
INTERRUPT_STAGE eInterruptionStage = IS_READY
BOOL bAllowLeaving = TRUE
INT iCurrentRestart = 0
INT iCurrentInterruption = 0

BOOL bCamermanFleeing = FALSE
BOOL bMakeupWomanFleeing = FALSE
BOOL bFirstTimeScared = TRUE

VECTOR vBlockAreaMin = <<-477.670807,236.909576,84.024628>>
VECTOR vBlockAreaMax = <<-488.682922,232.335739,81.017181>>

VECTOR vRoadOffOne = <<-526.328308,245.778336,80.972473>>
VECTOR vRoadOffTwo = <<-439.646210,235.061676,84.045418>>
FLOAT fRoadOffWidth = 6.0

STRING sWaypointRecBev = "pap4_BevRoute"
STRING sWaypointRecCam = "pap4_CamRoute"
INT iBevPenultimatePoint = 20
INT iCamPenultimatePoint = 15

VECTOR vExplosionCentre = <<-492.677856,227.602188,82.134773>>
FLOAT fExplosionRadius = 15.0

// variables for the camera audio
INT iCameraSoundID
STRING sCameraSoundset = "PAPARAZZO_04_SOUNDSET"
STRING sCameraSound = "CAMERA"

// variables to check for mission passed stats
BOOL bAllKilledAtOnce = FALSE
INT iKillAllTimer = -1
CONST_INT KILL_ALL_TIME 500

BOOL bAllowCameraSpamming = TRUE

//INT iShockingEventId = -1

//*************************************************************************************************************************************************
//													:HELPER FUNCTIONS:
//*************************************************************************************************************************************************

//MISSION FLOW CONTROL

/// PURPOSE:
///    Advances or rolls back the mission stage
/// PARAMS:
///    bReverse - should the mission stage roll back?
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	iMissionState = ENUM_TO_INT(eMissionStage)
	IF NOT bReverse
		eMissionStage = INT_TO_ENUM(MISSION_STAGE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionStage = INT_TO_ENUM(MISSION_STAGE, (iMissionState - 1))		
		ENDIF
	ENDIF
	eState = SS_INIT	
ENDPROC

/// PURPOSE:
///    Turn the navmesh surrounding Beverly on or off to prevent ambient peds walking through/into the scene
/// PARAMS:
///    bEnable - should the navmesh be enabled or disabled?
PROC SetNavmeshState(BOOL bEnable)
	IF bEnable
		SET_PED_PATHS_IN_AREA(vBlockAreaMin, vBlockAreaMax, TRUE)
	ELSE
		SET_PED_PATHS_IN_AREA(vBlockAreaMin, vBlockAreaMax, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts the synchronised scene used for the filming
PROC StartFilmingScene()
	CPRINTLN(DEBUG_MISSION,"Pap 4: Synch scene on cutscene blend")
	iBeverlyFilmingScene = CREATE_SYNCHRONIZED_SCENE(vFilmingScenePos, vFilmingSceneRot)
	SET_SYNCHRONIZED_SCENE_LOOPED(iBeverlyFilmingScene,FALSE)
	SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iBeverlyFilmingScene,FALSE)
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iBeverlyFilmingScene, sMissionAnims, sBevFilmBase, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[CAMERAMAN], iBeverlyFilmingScene, sMissionAnims, sCamFilmBase, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], iBeverlyFilmingScene, sMissionAnims, sProdFilmBase, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	ENDIF
	
	bAllowSynchronisedScene = TRUE
ENDPROC

/// PURPOSE:
///    Switches between the three synchronised scenes used by Beverly, the cameraman and the makeup woman
PROC ControlSynchronisedScenes(BOOL bInstantBlendIn = FALSE)
	IF bAllowSynchronisedScene
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
			iBeverlyFilmingScene = CREATE_SYNCHRONIZED_SCENE(vFilmingScenePos, vFilmingSceneRot)
			SET_SYNCHRONIZED_SCENE_LOOPED(iBeverlyFilmingScene,FALSE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iBeverlyFilmingScene,FALSE)
			
			INT iScene = GET_RANDOM_INT_IN_RANGE(0, 3)
			STRING sBevAnim
			STRING sCameraAnim
			STRING sMakeupAnim
			SWITCH iScene
				CASE 0
					sBevAnim = sBevFilmBase
					sCameraAnim = sCamFilmBase
					sMakeupAnim = sProdFilmBase
				BREAK
				
				CASE 1
					sBevAnim = sBevFilmGesture
					sCameraAnim = sCamFilmGesture
					sMakeupAnim = sProdFilmGesture
				BREAK
				
				DEFAULT
					sBevAnim = sBevFilmIdle
					sCameraAnim = sCamFilmIdle
					sMakeupAnim = sProdFilmIdle
				BREAK
			ENDSWITCH
			
			FLOAT BlendValue
			IF bInstantBlendIn
				BlendValue = INSTANT_BLEND_IN
			ELSE
				BlendValue = SLOW_BLEND_IN
			ENDIF
			
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iBeverlyFilmingScene, sMissionAnims, sBevAnim, BlendValue, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
			ENDIF
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[CAMERAMAN], iBeverlyFilmingScene, sMissionAnims, sCameraAnim, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
			ENDIF
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], iBeverlyFilmingScene, sMissionAnims, sMakeupAnim, BlendValue, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
			ENDIF
		ENDIF
	ELSE
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[BEV], SLOW_BLEND_OUT, TRUE)
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], REALLY_SLOW_BLEND_OUT, TRUE)
			IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
			AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
			AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_SMART_FLEE_PED)
			AND NOT bCamermanFleeing
			AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
			AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
			AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[CAMERAMAN]) //AND IS_ENTITY_ATTACHED_TO_ANY_OBJECT(sRCLauncherDataLocal.pedID[CAMERAMAN])
				TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[CAMERAMAN])
				//PRINT_NOW("Playing camera anim after synced scene", 7000, 1)
				CPRINTLN(DEBUG_MISSION,"Forcing ped anim update on cameraman 1")
			ENDIF
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SLOW_BLEND_OUT, TRUE)
			SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Passes the mission if Beverly is dead or far away
PROC CheckMissionPassed()
	IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	OR (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV]) > fBevEscapeDist
		AND (NOT IS_ENTITY_ON_SCREEN(sRCLauncherDataLocal.pedID[BEV]) OR NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV])))
		IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST)	// Record the player killing Beverly
		ENDIF
		eState = SS_INIT
		eMissionStage = MS_PASSED
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is (probably) stealth killing a ped
///    There isn't a function to check this directly so it checks if the ped is being stealth killed and if the player is performing a stealth kill.
///    Unless another ped in the mission is performing a stealth kill, this should be safe
/// PARAMS:
///    piTest - The ped who might be getting stealth killed by the player
/// RETURNS:
///    True if the player is performing a stealth kill and the test ped is being stealth killed
FUNC BOOL IsPlayerStealthKillingPed(PED_INDEX piTest)
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piTest)
		IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID()) AND IS_PED_BEING_STEALTH_KILLED(piTest)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Makes the cameraman drop his camera and flee
PROC CameramanFlees()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF IS_PED_IN_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
			REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
		ENDIF
		//CLEAR_PED_SECONDARY_TASK(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
				STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], SLOW_BLEND_OUT, TRUE)
				SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
			ELSE
				STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
			STOP_SOUND(iCameraSoundID)
			SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
		ENDIF
		bCamermanFleeing = TRUE
		
		bAllowSynchronisedScene = FALSE
		eInterruptionStage = IS_DONE
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes a ped flee the player after a delay
/// PARAMS:
///    pedIn - the ped we want to flee
///    waitTime - the amount of time to wait for (milliseconds)
PROC DELAYED_FLEE(PED_INDEX & pedIn, INT waitTime, BOOL shockedFirst = FALSE)
	SEQUENCE_INDEX siFlee
	/*IF iShockingEventId < 0
		iShockingEventId = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_VISIBLE_WEAPON, PLAYER_PED_ID())
	ENDIF*/
	OPEN_SEQUENCE_TASK(siFlee)
		TASK_PAUSE(NULL, waitTime)
		IF shockedFirst AND HAS_ANIM_DICT_LOADED(sMakeupScaredAnims) AND bFirstTimeScared //AND iShockingEventId > 0
			//TASK_SHOCKING_EVENT_BACK_AWAY(NULL, iShockingEventId)
			TASK_PLAY_ANIM(NULL, sMakeupScaredAnims, sBackAwayAnim,NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXIT_AFTER_INTERRUPTED)
			bFirstTimeScared = FALSE
		ENDIF
		TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
	CLOSE_SEQUENCE_TASK(siFlee)
	TASK_PERFORM_SEQUENCE(pedIn, siFlee)
	CLEAR_SEQUENCE_TASK(siFlee)
ENDPROC

/// PURPOSE:
///    Allows the cameraman to flee once he's finished ragdolling
PROC HandleCamermanFleeing()
	IF bCamermanFleeing AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
		AND NOT IS_PED_GETTING_UP(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_PRONE(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_PAUSE)
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_PERFORM_SEQUENCE)
			SetNavmeshState(TRUE)
			SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[CAMERAMAN], FA_DISABLE_HANDS_UP, TRUE)
			//PRINT_NOW("Making the cameraman flee", 7000, 1)
			//DELAYED_FLEE(sRCLauncherDataLocal.pedID[CAMERAMAN], 500)
			TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID(), 500, -1)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			eInterruptionStage = IS_DONE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Allows the makeup woman to flee once she's finished ragdolling
PROC HandleMakeupWomanFleeing()
	IF bMakeupWomanFleeing
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
	AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IS_PED_GETTING_UP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IS_PED_PRONE(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SCRIPT_TASK_PAUSE)
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SCRIPT_TASK_PERFORM_SEQUENCE)
			IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
			OR (IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[CAMERAMAN], TRUE))
			OR (IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV]) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[BEV], TRUE))
				SetNavmeshState(TRUE)
				IF IS_PED_IN_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
					REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				ENDIF
				SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[MAKEUPWMAN], FA_DISABLE_HANDS_UP, TRUE)
				TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 100, -1)
				//DELAYED_FLEE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], 350, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Will make Beverly flee if the player aims a weapon at him while he's is in melee combat
PROC BeverlyFleeReaction()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	
		IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV], TRUE)
			IF NOT bFleeFromWeapon
			AND bNotScaredYet
			AND NOT IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				// temporary until we have decent defensive behaviour
				IF IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				OR IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[BEV])
				ENDIF
				SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[BEV], FA_USE_VEHICLE, FALSE)
				SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				bBeverlyFleeing = TRUE
				TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 400, -1, TRUE)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				eInterruptionStage = IS_DONE
				bNotScaredYet = FALSE
				CPRINTLN(DEBUG_MISSION,"Pap 4: bNotScaredYet = FALSE, BeverlyFleeReaction()")
				bFleeFromWeapon = TRUE
			ENDIF
		ELSE
			IF NOT bFleeFromWeapon
			AND bNotScaredYet
			AND NOT IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				// temporary until we have decent defensive behaviour
				IF IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				OR IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[BEV])
				ENDIF
				SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[BEV], FA_USE_VEHICLE, TRUE)
				TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 400, -1, TRUE)
				SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				eInterruptionStage = IS_DONE
				bNotScaredYet = FALSE
				CPRINTLN(DEBUG_MISSION,"Pap 4: bNotScaredYet = FALSE, making Bev flee in vehicle")
				bFleeFromWeapon = TRUE
			ENDIF
		ENDIF
	iCameraLineTimer = GET_GAME_TIMER() + iCameraLineDelay
	ENDIF

ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Releases everything used by the mission
PROC CLEANUP()
	PRINTSTRING("STAGE CLEANUP") PRINTNL()
	SetNavmeshState(TRUE)
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherDataLocal.pedID[BEV], FALSE)
		IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
			STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
		ENDIF
		IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV])
			TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[BEV])
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
				TASK_VEHICLE_DRIVE_WANDER(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], 20, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
			ELSE
				IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
					CameramanFlees()
					SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[CAMERAMAN], FA_DISABLE_HANDS_UP, TRUE)
					//TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID(), 500, -1)
					DELAYED_FLEE(sRCLauncherDataLocal.pedID[CAMERAMAN], 500)
				ENDIF
			ENDIF
		ELSE
			TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[CAMERAMAN])
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		IF IS_PED_IN_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		ENDIF
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
				VEHICLE_INDEX viMakeup = GET_VEHICLE_PED_IS_IN(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
				IF IS_VEHICLE_OK(viMakeup) AND
				(IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viMakeup, TRUE) OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]))
					SEQUENCE_INDEX siLeave
					OPEN_SEQUENCE_TASK(siLeave)
						TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_DONT_CLOSE_DOOR)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
					CLOSE_SEQUENCE_TASK(siLeave)
					TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], siLeave)
					CLEAR_SEQUENCE_TASK(siLeave)
				ENDIF
			ELIF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[MAKEUPWMAN], FA_DISABLE_HANDS_UP, TRUE)
				//TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 500, -1)
				DELAYED_FLEE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], 350, TRUE)
			ENDIF
		ELSE
			TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		ENDIF
	ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_a"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_heavy"), TRUE) 
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_armed_takedown"), TRUE)
	
	REMOVE_ANIM_DICT(sMissionAnims)
	REMOVE_ANIM_DICT(sMakeupScaredAnims)
	
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecBev)
		REMOVE_WAYPOINT_RECORDING(sWaypointRecBev)
	ENDIF
	IF GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecCam)
		REMOVE_WAYPOINT_RECORDING(sWaypointRecCam)
	ENDIF
	
	STOP_SOUND(iCameraSoundID)
	
	SAFE_REMOVE_BLIP(biGOTO)
	SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
	//SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
		SET_OBJECT_AS_NO_LONGER_NEEDED(sRCLauncherDataLocal.ObjID[CAMERAMAN])
	ENDIF
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[BEV])
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[CAMERAMAN])
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
	SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
	SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[CAMERAMAN])
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth)
	KILL_FACE_TO_FACE_CONVERSATION()
ENDPROC

/// PURPOSE:
///    Finishes the mission
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CLEANUP()
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Passes the mission
PROC Script_Passed()

	KILL_ANY_CONVERSATION()
	Random_Character_Passed(CP_RAND_C_PAP4)
	CLEAR_PRINTS()

	IF bAllKilledAtOnce
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP4_ENTIRE_CREW_KILLED_IN_ONE)
	ENDIF
	Script_Cleanup()		
ENDPROC

// ===========================================================================================================
//		DEBUG/RESTART FUNCTIONS
// ===========================================================================================================
/// PURPOSE:
///    Starts the mission from the checkpoint after the intro
PROC RestartCheckpoint()
	RC_START_Z_SKIP()
	eMissionStage = MS_INTRO
	eState = SS_CLEANUP
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[CAMERAMAN], vCamReset)
		SET_ENTITY_HEADING(sRCLauncherDataLocal.pedID[CAMERAMAN], fCamReset)
	ENDIF
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND NOT IS_REPLAY_BEING_SET_UP()
		//SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-484.5429, 233.7179, 82.0252>>)
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vPlayerReset)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerReset)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	ENDIF
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
		SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.vehID[BEV], vBevCarPos, fBevCarHeading)
		SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[BEV])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[BEV], VEHICLELOCK_UNLOCKED)
	ENDIF

	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
		SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.vehID[CAMERAMAN], vVanPos, fVanHeading)//<<-493.66, 240.93, 82.94>>, 263.55)
		SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[CAMERAMAN])
		SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[CAMERAMAN], VEHICLELOCK_UNLOCKED)
	ENDIF
	
	CLEAR_AREA_OF_VEHICLES(vBevReset, 50)
	
	StartFilmingScene()
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	ELSE
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Adds Beverly and the cameraman to the conversation struct. Also sets Beverly to drop money when he dies
PROC SetupDialogue()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		SET_PED_MONEY(sRCLauncherDataLocal.pedID[BEV], BEVS_CASH)
		SET_PED_DIES_WHEN_INJURED(sRCLauncherDataLocal.pedID[BEV], TRUE)
		ADD_PED_FOR_DIALOGUE(ConversationPeds, BEVERLY_ID, sRCLauncherDataLocal.pedID[BEV], "BEVERLY")
	ENDIF
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		ADD_PED_FOR_DIALOGUE(ConversationPeds, CAMERA_ID, sRCLauncherDataLocal.pedID[CAMERAMAN], "Pap4Cameraman")
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes everything and recreates it at the proper start points. Used when debug skipping
PROC ResetMission()
	IF eMissionStage = MS_BEV_IN_CAR
		eMissionStage = MS_BEV_ON_FOOT
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -476.3108, 233.9309, 82.0245 >>, 103.3318)
	ENDIF
	
	STOP_SOUND(iCameraSoundID)
	
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[BEV])
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[CAMERAMAN])
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[CAMERAMAN])
	SAFE_DELETE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
	SAFE_DELETE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
	
	REQUEST_MODEL(S_M_Y_GRIP_01)
	REQUEST_MODEL(A_F_Y_BEVHILLS_02)
	REQUEST_MODEL(PROP_PAP_CAMERA_01)
	REQUEST_MODEL(PROP_V_CAM_01)
	REQUEST_MODEL(ISSI2)
	REQUEST_MODEL(RUMPO)
	
	WHILE NOT RC_CREATE_NPC_PED(sRCLauncherDataLocal.pedID[BEV], CHAR_BEVERLY, vBevReset, fBevReset, "PAPARAZZO LAUNCHER RC", FALSE)
		WAIT(0)
	ENDWHILE
	SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[BEV], vBevReset)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[BEV], TRUE)
	
	WHILE NOT HAS_MODEL_LOADED(S_M_Y_GRIP_01)
		WAIT(0)
	ENDWHILE
	sRCLauncherDataLocal.pedID[CAMERAMAN] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_GRIP_01, vCamReset, fCamReset)
	WHILE NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		WAIT(0)
	ENDWHILE
	SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[CAMERAMAN], vCamReset)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_GRIP_01)
	
	WHILE NOT HAS_MODEL_LOADED(A_F_Y_BEVHILLS_02)
		WAIT(0)
	ENDWHILE
	sRCLauncherDataLocal.pedID[MAKEUPWMAN] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BEVHILLS_02, vProdReset, fProdReset)
	WHILE NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		WAIT(0)
	ENDWHILE
	SET_ENTITY_COORDS_NO_OFFSET(sRCLauncherDataLocal.pedID[MAKEUPWMAN], vProdReset)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BEVHILLS_02)
	
	WHILE NOT HAS_MODEL_LOADED(PROP_PAP_CAMERA_01)
		WAIT(0)
	ENDWHILE	
	sRCLauncherDataLocal.ObjID[BEV] = CREATE_OBJECT_NO_OFFSET(PROP_PAP_CAMERA_01, <<-490.11, 233.15, 82.10>>)
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[BEV])
		//ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[BEV], sRCLauncherDataLocal.pedID[BEV], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[BEV], BONETAG_R_HAND), vBevOffset, vBevRot)
		ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[BEV], sRCLauncherDataLocal.pedID[BEV], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[BEV], BONETAG_PH_R_HAND), vBevOffset, vBevRot)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PAP_CAMERA_01)
	
	WHILE NOT HAS_MODEL_LOADED(PROP_V_CAM_01)
		WAIT(0)
	ENDWHILE	
	sRCLauncherDataLocal.ObjID[CAMERAMAN] = CREATE_OBJECT_NO_OFFSET(PROP_V_CAM_01, <<-490.11, 233.15, 82.10>>)
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
		ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[CAMERAMAN], sRCLauncherDataLocal.pedID[CAMERAMAN], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[CAMERAMAN], BONETAG_R_HAND), vCamOffset, vCamRot)
		PLAY_SOUND_FROM_ENTITY(iCameraSoundID, sCameraSound, sRCLauncherDataLocal.ObjID[CAMERAMAN], sCameraSoundset)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_V_CAM_01)
	StartFilmingScene()
	
	WHILE NOT HAS_MODEL_LOADED(ISSI2)
		WAIT(0)
	ENDWHILE
	sRCLauncherDataLocal.vehID[BEV] = CREATE_VEHICLE(ISSI2, vBevCarPos, fBevCarHeading)
	WHILE NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
		WAIT(0)
	ENDWHILE
	LOWER_CONVERTIBLE_ROOF(sRCLauncherDataLocal.vehID[BEV], TRUE)
	SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[BEV], 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(ISSI2)
	
	WHILE NOT HAS_MODEL_LOADED(RUMPO)
		WAIT(0)
	ENDWHILE
	sRCLauncherDataLocal.vehID[CAMERAMAN] = CREATE_VEHICLE(RUMPO, vVanPos, fVanHeading)
	WHILE NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
		WAIT(0)
	ENDWHILE
	SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[CAMERAMAN], 1)
	SET_VEHICLE_LIVERY(sRCLauncherDataLocal.vehID[CAMERAMAN], 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(RUMPO)
	
	SetNavmeshState(FALSE)
	
	bNotScaredYet = TRUE
	bPlayerAggro = FALSE
	bFleeFromWeapon = FALSE
	bStopCheckingNewsvan = FALSE
	
	SetupDialogue()
ENDPROC

/// PURPOSE:
///    Jumps to a given stage in the mission
/// PARAMS:
///    stage - The stage to jump to
PROC JUMP_TO_STAGE(MISSION_STAGE stage)
	RC_START_Z_SKIP()
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE()
	ENDIF
	WHILE IS_CUTSCENE_ACTIVE()
		WAIT(0)
	ENDWHILE
	eCarStealStage = CSS_READY
	bDoneRetalConv = FALSE
	bDoneReturnConv = FALSE
	eMissionStage = stage
	
	ResetMission()
	
	eState = SS_INIT
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Check for Forced Pass or Fail
#IF IS_DEBUG_BUILD
	PROC DEBUG_Check_Debug_Keys()

	// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			WAIT_FOR_CUTSCENE_TO_STOP()
			eMissionStage = MS_PASSED
			eState = SS_INIT
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			SAFE_REMOVE_BLIP(biGOTO)
			CLEAR_PRINTS()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
			
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) AND eMissionStage <> MS_INIT 
			eState = SS_MESKIPPED
		ENDIF	
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) AND eMissionStage <> MS_INIT
			//Work out which stage we want to reach based on the current stage
			iMissionState = ENUM_TO_INT(eMissionStage)
			
			IF iMissionState > 0	
				MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, iMissionState - 1)
				JUMP_TO_STAGE(e_stage)
			ENDIF
		ENDIF
		
		IF eMissionStage <> MS_INTRO
		    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
				  i_debug_jump_stage += 1
		          MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, i_debug_jump_stage)
		          JUMP_TO_STAGE(e_stage)
		    ENDIF
		ENDIF
		
	ENDPROC
#ENDIF


//*************************************************************************************************************************************************
//													:MISSION STAGE HELPER FUNCTIONS:
//*************************************************************************************************************************************************
/// PURPOSE:
///    Makes the cameraman walk to a suitable spot and film Beverly as he gets into his car. The cameraman then gets in his van and drives off
PROC CameramanFilmsCar()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_PERFORM_SEQUENCE)
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecCam)
		AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN])
			SEQUENCE_INDEX siWalkAndFilm
			OPEN_SEQUENCE_TASK(siWalkAndFilm)
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[BEV], 6000)//5000)
				ENDIF
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[CAMERAMAN], VS_DRIVER)
					SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, TRUE)
					TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[CAMERAMAN], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
				ENDIF
			CLOSE_SEQUENCE_TASK(siWalkAndFilm)
			TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[CAMERAMAN], siWalkAndFilm)
			CLEAR_SEQUENCE_TASK(siWalkAndFilm)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes Beverly get in the camera van. Only used if the player has blocked both doors for Beverly's car
PROC StartBevToCarAlt()
	IF bAllowSynchronisedScene
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND bStartShootingReaction AND bNotScaredYet
			IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV]) AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV], SCRIPT_TASK_WANDER_STANDARD)
			AND NOT bBeverlyFleeing
				IF CREATE_CONVERSATION(conversationPeds, "PAP4AUD", "PAP4_BLOCKED", CONV_PRIORITY_MEDIUM)
					SetNavmeshState(TRUE)
					IF IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[CAMERAMAN], VS_FRONT_RIGHT)
						TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[CAMERAMAN], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						/*IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[CAMERAMAN], giBevGroup)
							REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
							TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], -1)
						ENDIF*/
						
						// Makeup woman walks off as there's no more space in the van
						IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
							TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
							/*IF IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[MAKEUPWMAN], giBevGroup)
								REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
							ENDIF*/
						ENDIF
					ELSE
						TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[BEV])
					ENDIF
					iCameraWalkTimer = GET_GAME_TIMER() + 1500
					bCameraGuyWalks = TRUE
					eInterruptionStage = IS_DONE
					bAllowSynchronisedScene = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the car doors and makes Beverly get into the cameraman's van if necessary
FUNC BOOL CarDoorsBlocked()
	BOOL bDriversSeatFree = IS_ENTRY_POINT_FOR_SEAT_CLEAR(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], VS_DRIVER)
	BOOL bPassengerSeatFree = IS_ENTRY_POINT_FOR_SEAT_CLEAR(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], VS_FRONT_RIGHT)
	
	IF NOT bDriversSeatFree AND NOT bPassengerSeatFree
		// Beverly's car doors are blocked. Tells player to fuck off and gets in the van passenger seat. Makeup woman gets in back if possible, wanders if not
		bAllowLeaving = FALSE	// don't want Beverly to use the regular leave behaviour
		eInterruptionStage = IS_DONE
		bAllowSynchronisedScene = FALSE
		//StartBevToCarAlt()
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
		AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
			TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
			SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
			TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID())
			SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
		ENDIF
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 200, -1)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player has blocked Beverly's car with another vehicle
/// RETURNS:
///    TRUE if Beverly's car is blocked by another vehicle
FUNC BOOL BeverlysCarBlockedIn()
	// Don't want Beverly to do his blocked in behaviour if he's fleeing
	IF NOT bStartShootingReaction
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viTemp
	BOOL bPlayerBlocking = FALSE
	
	viTemp = GET_RANDOM_VEHICLE_IN_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[BEV], <<0.0, 2.0, 0.0>>), 2.0, DUMMY_MODEL_FOR_SCRIPT,
		VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING)
	IF DOES_ENTITY_EXIST(viTemp)
		IF NOT (viTemp = sRCLauncherDataLocal.vehID[BEV])
			IF (viTemp = GET_PLAYERS_LAST_VEHICLE()) /*OR (viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))*/ OR (IS_VEHICLE_DRIVEABLE(viTemp) AND IS_VEHICLE_STOPPED(viTemp))
				bAllowSynchronisedScene = FALSE
				bPlayerBlocking = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THERE_A_CAR_BLOCKING_THIS_CAR(sRCLauncherDataLocal.vehID[BEV]) OR bPlayerBlocking
	//OR IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.vehID[BEV], <<0.0, 2.0, 0.0>>), 2.0)
		// Beverly's car is blocked. Tells player to fuck off and gets in the van passenger seat. Makeup woman gets in back if possible, wanders if not
		IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV])
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
				IF CREATE_CONVERSATION(conversationPeds, "PAP4AUD", "PAP4_BLOCKED", CONV_PRIORITY_MEDIUM)
					/*SEQUENCE_INDEX siLeave
					OPEN_SEQUENCE_TASK(siLeave)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(siLeave)
					TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[BEV], siLeave)
					CLEAR_SEQUENCE_TASK(siLeave)*/
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
					AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
					AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
					AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
						TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
						SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
					ENDIF
					IF DOES_BLIP_EXIST(biGOTO)
						SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV])
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				bAllowLeaving = FALSE	// don't want Beverly to use the regular leave behaviour
				eInterruptionStage = IS_DONE
				//bAllowSynchronisedScene = FALSE
				//StartBevToCarAlt()
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
					TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				ENDIF
			ENDIF
		ENDIF
		bAllowSynchronisedScene = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Makes Beverly walk to his car and get in
/// PARAMS:
///    bForceStart - Should Beverly go to his car immediately or wait until any conversations have finished
PROC StartBevToCar(BOOL bForceStart = FALSE, BOOL bRunInstead = FALSE)
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND bStartShootingReaction
	AND bNotScaredYet
	AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV]) AND NOT bBeverlyFleeing
		IF bCameraGuyWalks AND GET_GAME_TIMER() > iCameraWalkTimer
			IF eCarStealStage = CSS_READY
				// camera guy walks to spot closer to car and turns to face Beverly
				CameramanFilmsCar()
			ENDIF
		ELSE
			IF bForceStart OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV], 30.0)
				IF bAllowLeaving AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV],SCRIPT_TASK_ENTER_VEHICLE)
				AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV],SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
				AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[BEV]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV]) AND NOT bBeverlyFleeing
					IF NOT CarDoorsBlocked() AND NOT BeverlysCarBlockedIn()
						BOOL bConvPlayed = FALSE
						IF NOT bForceStart
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()//_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							enumSubtitlesState eSubtitles = DISPLAY_SUBTITLES
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV], 30.0)
								eSubtitles = DO_NOT_DISPLAY_SUBTITLES
							ENDIF
							IF iCurrentInterruption > 1
							 bConvPlayed = CREATE_CONVERSATION(conversationPeds, "PAP4AUD", "PAP4_LEAVE2", CONV_PRIORITY_MEDIUM, eSubtitles)
							ELSE
							 bConvPlayed = CREATE_CONVERSATION(conversationPeds, "PAP4AUD", "PAP4_LEAVE", CONV_PRIORITY_MEDIUM, eSubtitles)
							ENDIF
						ENDIF
						IF bConvPlayed
						OR bForceStart
							SetNavmeshState(TRUE)
							SET_PED_WEAPON_MOVEMENT_CLIPSET(sRCLauncherDataLocal.pedID[BEV],sWeaponMoveClipset)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[BEV])
							IF bRunInstead
								TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
								CPRINTLN(DEBUG_MISSION,"Pap 4: Bev should run to car")
							ELSE
								TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
								CPRINTLN(DEBUG_MISSION,"Pap 4: Bev should walk to car")
							ENDIF
							
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) //AND IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[CAMERAMAN], giBevGroup)
								//REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
								TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], -1)
							ENDIF
							
							// Makeup woman gets in the film van at the same time
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT bPlayerAggro
							AND NOT bMakeupWomanFleeing AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
								IF bForceStart OR (IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[CAMERAMAN], TRUE))
									// Player has stolen the camera van so she gets into Bev's car
									IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[BEV]) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID())
										TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 500, -1)
									ELIF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
										SEQUENCE_INDEX siLeave
										OPEN_SEQUENCE_TASK(siLeave)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
											TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[BEV], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
										CLOSE_SEQUENCE_TASK(siLeave)
										TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], siLeave)
										CLEAR_SEQUENCE_TASK(siLeave)
									ELSE
										TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 500, -1)
										//TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID())
									ENDIF
								ELSE
									IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[CAMERAMAN], VS_FRONT_RIGHT)
										TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
									ENDIF
									/*IF IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[MAKEUPWMAN], giBevGroup)
										REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
									ENDIF*/
								ENDIF
							ENDIF
							iCameraWalkTimer = GET_GAME_TIMER() + 1500
							bCameraGuyWalks = TRUE
							eInterruptionStage = IS_DONE
							bAllowSynchronisedScene = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    When Beverly is in his car, changes his blip size, makes him drive off and advances the stage
PROC CheckBevInCar()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV]) AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecBev)
			IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV])
			AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV],SCRIPT_TASK_VEHICLE_MISSION)
				SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, TRUE)
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV],sWaypointRecBev,DRIVINGMODE_AVOIDCARS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
				IF DOES_BLIP_EXIST(biGOTO)
					SET_BLIP_SCALE(biGOTO, BLIP_SIZE_VEHICLE)
				ENDIF
				NEXT_STAGE()
			ELIF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], TRUE)
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[BEV])
			ENDIF
		ENDIF
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
			IF IS_PED_SITTING_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[CAMERAMAN])
				IF DOES_BLIP_EXIST(biGOTO)
					SET_BLIP_SCALE(biGOTO, BLIP_SIZE_VEHICLE)
				ENDIF
				NEXT_STAGE()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the cameraman drive off after Beverly
PROC CheckCameramanInCar()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
		IF IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN])
			/*IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV])
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN],sWaypointRecCam,DRIVINGMODE_AVOIDCARS_RECKLESS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
			ELIF (IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[CAMERAMAN]))
			OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
				TASK_VEHICLE_MISSION_PED_TARGET(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], PLAYER_PED_ID(), MISSION_FLEE, 50.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
			ENDIF*/
			TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN],sWaypointRecCam,DRIVINGMODE_AVOIDCARS_OBEYLIGHTS,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Switches Beverly and the cameraman from waypoint recordings to AI driving when they get hear the end of their routes
PROC SwitchWaypointToAI()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV]) AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecBev)
		//IF IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV], SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
			IF GET_VEHICLE_WAYPOINT_PROGRESS(sRCLauncherDataLocal.vehID[BEV]) >= iBevPenultimatePoint
				IF bStartShootingReaction
					// Player hasn't been aggressive
					TASK_VEHICLE_MISSION_PED_TARGET(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID(), MISSION_FLEE, 15.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
				ELSE
					// player has been aggressive
					TASK_VEHICLE_MISSION_PED_TARGET(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID(), MISSION_FLEE, 40.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN]) AND GET_IS_WAYPOINT_RECORDING_LOADED(sWaypointRecCam)
		//IF IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[CAMERAMAN])
			IF GET_VEHICLE_WAYPOINT_PROGRESS(sRCLauncherDataLocal.vehID[CAMERAMAN]) >= iCamPenultimatePoint
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
					IF bStartShootingReaction
						// Player hasn't been aggressive
						TASK_VEHICLE_MISSION_PED_TARGET(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], MISSION_FOLLOW, 15.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
					ELSE
						TASK_VEHICLE_MISSION_PED_TARGET(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], MISSION_FOLLOW, 40.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
					ENDIF
				ELSE
					TASK_VEHICLE_DRIVE_WANDER(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.vehID[CAMERAMAN], 15.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is threatening Beverly in any way
/// RETURNS:
///    True if the player is threatening Beverly
/*FUNC BOOL IsPlayerThreateningBeverley()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF (IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[BEV]) 
				AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT)
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[BEV])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC*/

/// PURPOSE:
///    Makes Beverly look at the player and starts a timer if the player steals his car, drives off then returns
PROC CarStealReturnConvDone()
	TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), -1)
	iCarStealTimer = GET_GAME_TIMER() + iCarStealTimeOut
	IF DOES_BLIP_EXIST(biGOTO)
		SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
	ENDIF
	eCarStealStage = CSS_STOLEN
	CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_STOLEN")
ENDPROC

/// PURPOSE:
///    Triggers the appropriate behaviour if the player steals Beverly's car, drives off then returns. Plays a conversation if it's the first time
PROC CommonCarStealReturnBehaviour()
	IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV]) < fCarStealReturnDist)
		IF bDoneReturnConv OR NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[BEV])
			CarStealReturnConvDone()
		ELIF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_RETURN", CONV_PRIORITY_MEDIUM)
			bDoneReturnConv = TRUE
			CarStealReturnConvDone()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the cameraman if the player steals Beverly's car
PROC CarStealCameramanControl()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
	AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		FLOAT fCamBevDistance = GET_DISTANCE_BETWEEN_PEDS(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV])
		IF fCamBevDistance > 20.0
			IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_GOTO_ENTITY_OFFSET)
				TASK_GOTO_ENTITY_OFFSET(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], DEFAULT_TIME_BEFORE_WARP, DEFAULT_SEEK_RADIUS, 0, PEDMOVE_RUN, ESEEK_KEEP_TO_PAVEMENTS)
			ENDIF
		ELIF fCamBevDistance < 10.0
			IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
				TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], -1)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    State machine that controls Beverly if the player steals his car
PROC CarStealReaction()
	SWITCH eCarStealStage
		CASE CSS_START
			//bAllowLeaving = TRUE
			bAllowLeaving = FALSE
			bAllowSynchronisedScene = FALSE
			eInterruptionStage = IS_DONE
			BOOL bConvDone
			bConvDone = FALSE
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID())
			AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV], 5.0)
				bConvDone = CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_CARSHOT", CONV_PRIORITY_MEDIUM)
			ELSE
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[BEV]) OR IS_PLAYER_TOWING_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
					bConvDone = CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_STEAL", CONV_PRIORITY_MEDIUM)
				ELSE
					bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(conversationPeds, "PAP4AUD", "PAP4_STEAL", "PAP4_STEAL_1", CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
			IF bConvDone
				IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
					CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[BEV])
					CLEAR_PED_SECONDARY_TASK(sRCLauncherDataLocal.pedID[BEV])
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(sRCLauncherDataLocal.pedID[BEV])
					CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_START, stopping Bev's camera anim")
				ENDIF
				SEQUENCE_INDEX siBev
				OPEN_SEQUENCE_TASK(siBev)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[BEV])
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBevRunToPos, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP)
					ENDIF
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
				CLOSE_SEQUENCE_TASK(siBev)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[BEV], siBev)
				CLEAR_SEQUENCE_TASK(siBev)
				//TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), -1)
				SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) 
				AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
					IF IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN])
						TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN])
					ELSE
						TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], -1)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT bPlayerAggro
				AND NOT bMakeupWomanFleeing AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID())
						TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 500, -1)
					ELIF IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
						TASK_LEAVE_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT| ECF_DONT_CLOSE_DOOR)
					ELSE
						TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), -1)
					ENDIF
				ENDIF
				iCarStealTimer = GET_GAME_TIMER() + iCarStealTimeOut
				IF DOES_BLIP_EXIST(biGOTO)
					SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
				ENDIF
				eCarStealStage = CSS_STOLEN
				CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_STOLEN")
			ENDIF
		BREAK
		CASE CSS_STOLEN
			IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV]) > fCarStealDist)
				eCarStealStage = CSS_RETURN
				CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_RETURN")
			ELIF GET_GAME_TIMER() > iCarStealTimer
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				AND bNotScaredYet
					IF bDoneRetalConv
						TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
						SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
						eCarStealStage = CSS_RETALIATE
						CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_RETALIATE")
					ELIF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_RETAL", CONV_PRIORITY_MEDIUM)
						bDoneRetalConv = TRUE
						TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
						SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
						eCarStealStage = CSS_RETALIATE
						CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_RETALIATE")
					ENDIF
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT bPlayerAggro
					AND NOT bMakeupWomanFleeing AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
						SEQUENCE_INDEX siMW
						OPEN_SEQUENCE_TASK(siMW)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vMWRunToPos, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						CLOSE_SEQUENCE_TASK(siMW)
						TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], siMW)
						CLEAR_SEQUENCE_TASK(siMW)
					ENDIF
					IF DOES_BLIP_EXIST(biGOTO)
						SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION,"Pap 4: Can't retaliate, Bev is in combat or bNotScaredYet = FALSE...")
					IF bNotScaredYet = FALSE AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV], TRUE)
						IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
							CPRINTLN(DEBUG_MISSION,"Pap 4: Beverly needs to flee now...")
							SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[BEV], FA_USE_VEHICLE, FALSE)
							bBeverlyFleeing = TRUE
							TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 500, -1)
							SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							eInterruptionStage = IS_DONE
							eCarStealStage = CSS_DONE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION,"Pap 4: Waiting to retaliate...")
			ENDIF
			CarStealCameramanControl()
		BREAK
		CASE CSS_RETALIATE
			IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV]) > fCarStealDist)
				SEQUENCE_INDEX siBev
				OPEN_SEQUENCE_TASK(siBev)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBevRunToPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP)
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[CAMERAMAN], -1)
					ELSE
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
					ENDIF
				CLOSE_SEQUENCE_TASK(siBev)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[BEV], siBev)
				CLEAR_SEQUENCE_TASK(siBev)
				IF DOES_BLIP_EXIST(biGOTO)
					SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
				ENDIF
				eCarStealStage = CSS_RETURN
				CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_RETURN")
			ENDIF
			CarStealCameramanControl()
		BREAK
		CASE CSS_RETURN
			CommonCarStealReturnBehaviour()
			CarStealCameramanControl()
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Tells Beverly what to do if the player steals his car, makes him flee if they destroy it
PROC CarStolenOrDestroyed()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
	AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[BEV])
	AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
			IF eMissionStage = MS_BEV_ON_FOOT
				IF eCarStealStage = CSS_READY
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[BEV])
					OR IS_PED_BEING_JACKED(sRCLauncherDataLocal.pedID[BEV])
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID())
					OR IS_PLAYER_TOWING_VEHICLE(sRCLauncherDataLocal.vehID[BEV])
						eCarStealStage = CSS_START
						CPRINTLN(DEBUG_MISSION,"Pap 4: eCarStealStage = CSS_START")
						eMissionStage = MS_BEV_ON_FOOT
					ENDIF
				ELSE 
					CarStealReaction()
				ENDIF
			ELIF eMissionStage = MS_BEV_IN_CAR
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[BEV], PLAYER_PED_ID())
					BeverlyFleeReaction()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
				SetNavmeshState(TRUE)
				IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
					STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
				ENDIF
				SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[BEV], FA_USE_VEHICLE, FALSE)
				bBeverlyFleeing = TRUE
				TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 500, -1)
				SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				eInterruptionStage = IS_DONE
				bAllowSynchronisedScene = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells Beverly what to do if the player blocks both doors for his car
PROC CheckCarDoors()
	IF bStartShootingReaction AND bNotScaredYet AND bAllowLeaving AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV],SCRIPT_TASK_ENTER_VEHICLE)
		// Use a timer here because IS_ENTRY_POINT_FOR_SEAT_CLEAR() is very expensive, don't want to check it too often
		IF GET_GAME_TIMER() > iCheckDoorsTimer
			CarDoorsBlocked()
			iCheckDoorsTimer = GET_GAME_TIMER() + 1000
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the cameraman attack and the makeup woman flee if the player steals the news van
PROC NewsVanStolen()
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
	AND	(IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[CAMERAMAN]) 
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[CAMERAMAN], PLAYER_PED_ID()))
	AND NOT bStopCheckingNewsvan
		// camerman attacks player if they try to steal the van
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			CPRINTLN(DEBUG_MISSION,"Pap 4: Start convo PAP4_VSTEAL")
			//IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_VSTEAL", CONV_PRIORITY_HIGH)
			CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_VSTEAL", CONV_PRIORITY_HIGH)
				SetNavmeshState(TRUE)
				IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
						STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], SLOW_BLEND_OUT, TRUE)
						//SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
					ELSE
						STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
					STOP_SOUND(iCameraSoundID)
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
				ENDIF
				TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID())
				bStopCheckingNewsvan = TRUE
			//ENDIF
		ENDIF
		
		
		
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[CAMERAMAN])
		
		// makeup woman flees
		/*IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SCRIPT_TASK_ENTER_VEHICLE)
			SetNavmeshState(TRUE)
			//bMakeupWomanFleeing = TRUE
			//TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID())
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
				SEQUENCE_INDEX siLeave
				OPEN_SEQUENCE_TASK(siLeave)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1000)
					TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[BEV], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
				CLOSE_SEQUENCE_TASK(siLeave)
				TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], siLeave)
				CLEAR_SEQUENCE_TASK(siLeave)
			ELSE
				TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID())
			ENDIF
		ENDIF*/
		
		bAllowSynchronisedScene = FALSE
		eInterruptionStage = IS_DONE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CPRINTLN(DEBUG_MISSION,"Pap 4: Should be making Bev run to his car...")
		//StartBevToCar(TRUE, TRUE) // Make Beverly run to his car instead of walk
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
			IF NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
			AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
				IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
					STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
					TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 500, -1)
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				ELSE
					TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player has an explosive weapon equipped
/// RETURNS:
///    True if the player has an explosive weapon equipped
FUNC BOOL PlayerUsingExplosives()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_MOLOTOV OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_STICKYBOMB
		OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_GRENADE OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_GRENADELAUNCHER
		OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_RPG
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Beverly, the cameraman or the makeup artist can see the player with a sticky bomb
/// RETURNS:
///    True if the player is using sticky bombs and any of the peds can see him, also true if they're not using a sticky bomb
FUNC BOOL CanSeeStickyBombUse()
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_STICKYBOMB
		IF CAN_PED_SEE_PLAYER(sRCLauncherDataLocal.pedID[BEV]) OR CAN_PED_SEE_PLAYER(sRCLauncherDataLocal.pedID[CAMERAMAN])
		OR CAN_PED_SEE_PLAYER(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player has driven into a ped, possibly without damaging them
/// PARAMS:
///    pedID - The ped we're testing
/// RETURNS:
///    TRUE if the pkayer has driven into the ped
FUNC BOOL PlayerNudgedPedWithCar(PED_INDEX pedID)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayer)
			IF IS_ENTITY_TOUCHING_ENTITY(pedID, viPlayer) AND NOT IS_VEHICLE_ALMOST_STOPPED(viPlayer)
				TASK_SMART_FLEE_PED(pedID, PLAYER_PED_ID(), 100, -1)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Makes Beverly put his hands up and turn to face the player
PROC BeverlyScaredReaction()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV], TRUE)
		AND bNotScaredYet
		AND NOT bFleeFromWeapon // If we've started Bev fleeing, don't make him hands up
		AND NOT IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
		AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[BEV])
		AND NOT IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
		AND eCarStealStage = CSS_READY
			// temporary until we have decent defensive behaviour
			IF IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
			OR IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[BEV])
				CLEAR_PED_TASKS(sRCLauncherDataLocal.pedID[BEV])
			ENDIF
			IF HAS_ANIM_DICT_LOADED(sMissionAnims)
				TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
			ENDIF
			TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), -1)
			bNotScaredYet = FALSE
			CPRINTLN(DEBUG_MISSION,"Pap 4: bNotScaredYet = FALSE, BeverlyScaredReaction()")
		ENDIF
	ENDIF
	iCameraLineTimer = GET_GAME_TIMER() + iCameraLineDelay
	//bBeverlyWaiting = FALSE
ENDPROC

PROC SendCameramanToSuitablePlace()
	IF NOT bCamermanFleeing AND IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) 
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		VECTOR vDestOne = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[BEV], <<3,0,0>>)
		VECTOR vDestTwo = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherDataLocal.pedID[BEV], <<-3,0,0>>)
		FLOAT fDistOne = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDestOne)
		FLOAT fDistTwo = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vDestTwo)
		VECTOR vDest
		IF fDistOne < fDistTwo
			vDest = vDestTwo
		ELSE
			vDest = vDestOne
		ENDIF
		/*IF IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[CAMERAMAN], giBevGroup)
			REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
		ENDIF*/
		SEQUENCE_INDEX siCam
		OPEN_SEQUENCE_TASK(siCam)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vDest, PEDMOVE_SPRINT)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, sRCLauncherDataLocal.pedID[BEV])
		CLOSE_SEQUENCE_TASK(siCam)
		TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[CAMERAMAN], siCam)
		CLEAR_SEQUENCE_TASK(siCam)
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers appropriate responses from the peds if the player becomes aggressive
PROC PlayerIsAggressive()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) 
		IF bStartShootingReaction
			IF (IS_PED_SHOOTING(PLAYER_PED_ID())
				AND CanSeeStickyBombUse())
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vExplosionCentre, fExplosionRadius)
			OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
			OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				bAllowLeaving = TRUE
				eInterruptionStage = IS_DONE
				eCarStealStage = CSS_DONE
				BeverlyFleeReaction()
				bAllowSynchronisedScene = FALSE
				bStartShootingReaction = FALSE
				SendCameramanToSuitablePlace()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
					IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
				SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
			ELIF (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID()))
			AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
			AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT
				BeverlyFleeReaction()
				bAllowLeaving = TRUE
				eInterruptionStage = IS_DONE
				eCarStealStage = CSS_DONE
				bAllowSynchronisedScene = FALSE
				bStartShootingReaction = FALSE
				bAllowThreatConv = FALSE
				//bBeverlyWaiting = FALSE
				SendCameramanToSuitablePlace()
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
					IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			//ELIF ((HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[BEV], FALSE) OR IsPlayerThreateningBeverley()) AND CanSeeStickyBombUse())
			ELIF (IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[BEV])  AND CanSeeStickyBombUse()
				AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT)
			OR IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID()) //OR IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[BEV])
				IF IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV], SCRIPT_TASK_COMBAT) //COMBAT IS OCCURRING
					bAllowLeaving = TRUE
					eInterruptionStage = IS_DONE
					eCarStealStage = CSS_DONE
					//iBevWaitTimer = GET_GAME_TIMER() + iBevWaitDelay
					bAllowSynchronisedScene = FALSE
					bStartShootingReaction = FALSE
					BeverlyFleeReaction()
					SendCameramanToSuitablePlace()
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
						IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				ELSE
					// same reaction as if the player's shooting but delayed
					bAllowLeaving = TRUE
					eInterruptionStage = IS_DONE
					eCarStealStage = CSS_DONE
					//iBevWaitTimer = GET_GAME_TIMER() + iBevWaitDelay
					bAllowSynchronisedScene = FALSE
					bStartShootingReaction = FALSE
					BeverlyScaredReaction()
					SendCameramanToSuitablePlace()
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
						IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
				ENDIF
				
				
			ELIF (IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[BEV]) 
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
				OR bPlayerAggro)
			AND (GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED OR GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_OBJECT)
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
					TASK_COMBAT_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID())
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
					bAllowSynchronisedScene = FALSE
					iFightLineTimer = GET_GAME_TIMER()
					/*IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[CAMERAMAN], giBevGroup)
						REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN])
					ENDIF*/
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.objID[BEV])
				ENDIF
			ENDIF
		ELSE
			IF bAllowThreatConv
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND NOT IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
					IF IS_MESSAGE_BEING_DISPLAYED()
						IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_THREAT", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
							REPLAY_RECORD_BACK_FOR_TIME(2.0,2.0, REPLAY_IMPORTANCE_LOWEST)
							bAllowThreatConv = FALSE
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_THREAT", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
							REPLAY_RECORD_BACK_FOR_TIME(2.0,2.0, REPLAY_IMPORTANCE_LOWEST)
							bAllowThreatConv = FALSE
						ENDIF
					ENDIF
				ENDIF
			/*ELIF bBeverlyWaiting
				IF GET_GAME_TIMER() > iBevWaitTimer AND (NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) OR
				(IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_GETTING_UP(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_PRONE(sRCLauncherDataLocal.pedID[BEV])
				AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[BEV])))
					BeverlyScaredReaction()
				ENDIF*/
			ELIF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) 
			AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[BEV])
			AND ((GET_ENTITY_HEALTH(sRCLauncherDataLocal.pedID[BEV]) < 150 OR (IS_PLAYER_SHOOTING_NEAR_PED(sRCLauncherDataLocal.pedID[BEV], FALSE) AND CanSeeStickyBombUse())
				OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID()) AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_STUNGUN)
				OR NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV], 20))
			OR (IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN]))
			OR (IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID()))
			OR (IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID()))
			OR bDoFlinchReaction)
				IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
					bDoFlinchReaction = TRUE // Check that Bev has his hands up to do a flinch
				ENDIF
				switch iFlinchStage
					CASE 0
						IF bDoFlinchReaction = FALSE
							iFlinchStage++ // No need for a flinch reaction since Bev didn't have his hands up in the first place
						ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sHandsupFlinch, "flinch_additive_a")
							TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[BEV], sHandsupFlinch, "flinch_additive_a", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ADDITIVE|AF_UPPERBODY)
						ELIF GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[BEV], sHandsupFlinch, "flinch_additive_a") > 0.7
							CPRINTLN(DEBUG_MISSION,"Pap 4: Trying to stop flinch")
							STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sHandsupFlinch, "flinch_additive_a")
							iFlinchStage++
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV])
							// run to the car
							SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
							bNotScaredYet = FALSE
							CPRINTLN(DEBUG_MISSION,"Pap 4: bNotScaredYet = FALSE, PlayerIsAggressive()")
							/*IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[BEV]) AND NOT PlayerUsingExplosives()
							AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV]) AND NOT bBeverlyFleeing
								IF NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV], SCRIPT_TASK_ENTER_VEHICLE)
									TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[BEV], sRCLauncherDataLocal.vehID[BEV], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT, ECF_RESUME_IF_INTERRUPTED | ECF_DONT_CLOSE_DOOR)
								ENDIF
								// Camera guy walks to a point near the car
								CameramanFilmsCar()
							ELSE*/
								IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV])
									CLEAR_PED_SECONDARY_TASK(sRCLauncherDataLocal.pedID[BEV])
									IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
										STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
									ENDIF
									SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[BEV])
									SetNavmeshState(TRUE)
									SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[BEV], FA_USE_VEHICLE, FALSE)
									bBeverlyFleeing = TRUE
									TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[BEV], PLAYER_PED_ID(), 500, -1)
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									eInterruptionStage = IS_DONE
									eCarStealStage = CSS_DONE
								ENDIF
							//ENDIF
						ENDIF
						iFlinchStage++
					BREAK
				ENDSWITCH
			ENDIF
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.pedID[BEV])
		ENDIF
		
		// Cameraman only flees if he's directly threatened
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
			IF (IS_PLAYER_SHOOTING_NEAR_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], FALSE) AND CanSeeStickyBombUse())
			OR (HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID()) AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT)
			OR (IS_PLAYER_VISIBLY_TARGETTING_PED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT)
			OR NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
			OR IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[CAMERAMAN])
			OR PlayerNudgedPedWithCar(sRCLauncherDataLocal.pedID[CAMERAMAN])
			OR IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
			OR IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vExplosionCentre, fExplosionRadius)
				IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN])
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
						IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT") AND NOT ARE_STRINGS_EQUAL(sConv, "PAP4_ATTACK")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_CAMFLEE", CONV_PRIORITY_MEDIUM)//, DO_NOT_DISPLAY_SUBTITLES)
					CameramanFlees()
					bPlayerAggro = TRUE
					/*IF IS_THERE_A_CAR_BLOCKING_THIS_CAR(sRCLauncherDataLocal.vehID[BEV])
					OR CarDoorsBlocked()
						BeverlyFleeReaction()
					ELSE
						StartBevToCar(TRUE, TRUE)
					ENDIF*/
					BeverlyFleeReaction()
				ENDIF
			ENDIF
		ENDIF
		
		// Makeup woman flees more easily than the cameraman
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT bMakeupWomanFleeing
			IF (HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND CanSeeStickyBombUse())
			OR (HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[BEV]) AND CanSeeStickyBombUse())
			OR (HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND CanSeeStickyBombUse())
			OR PlayerNudgedPedWithCar(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			OR PlayerNudgedPedWithCar(sRCLauncherDataLocal.pedID[BEV])
			OR PlayerNudgedPedWithCar(sRCLauncherDataLocal.pedID[CAMERAMAN])
			OR IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			OR NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vExplosionCentre, fExplosionRadius)
			OR IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[BEV])
			OR IsPlayerStealthKillingPed(sRCLauncherDataLocal.pedID[CAMERAMAN])
				IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[MAKEUPWMAN]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
					/*IF IS_PED_GROUP_MEMBER(sRCLauncherDataLocal.pedID[MAKEUPWMAN], giBevGroup)
						REMOVE_PED_FROM_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
					ENDIF*/
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TEXT_LABEL_23 tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						STRING sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
						IF NOT ARE_STRINGS_EQUAL(sConv, "PAP4_THREAT") AND NOT ARE_STRINGS_EQUAL(sConv, "PAP4_ATTACK")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					SetNavmeshState(TRUE)
					bMakeupWomanFleeing = TRUE
					bAllowSynchronisedScene = FALSE
					bPlayerAggro = TRUE // Player aggro'd the scene, use this bool to ensure Bev attacks player
					IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
						BeverlyFleeReaction()
					ENDIF
					/*IF IS_THERE_A_CAR_BLOCKING_THIS_CAR(sRCLauncherDataLocal.vehID[BEV])
					OR CarDoorsBlocked()
						BeverlyFleeReaction()
					ELSE
						StartBevToCar(TRUE, TRUE)
					ENDIF*/
					eInterruptionStage = IS_DONE
				ENDIF
			ENDIF
		ENDIF
		
		IF (IS_PED_SHOOTING(PLAYER_PED_ID()) )//AND CanSeeStickyBombUse())
			BeverlyFleeReaction()
			CameramanFlees()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the bool bAllKilledAtOnce to be FALSE if the player doesn't kill all peds in one go
PROC CheckForAllKilled()
	IF NOT bAllKilledAtOnce
		IF iKillAllTimer = -1
			// timer is -1, means that no peds have been killed yet
			IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
			OR NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				iKillAllTimer = GET_GAME_TIMER() + KILL_ALL_TIME
			ENDIF
		ELSE
			// timer has been set, at least one ped has been killed
			IF iKillAllTimer > GET_GAME_TIMER()
				// less than 500ms have passed so continue to check the other peds
				IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
				AND NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
					bAllKilledAtOnce = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers conversations based on the player's aggressive behaviour
PROC PlayerShootingDialogue()
	IF NOT bAllowThreatConv
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
	AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV], 5.0)
		IF IS_PED_SHOOTING(PLAYER_PED_ID()) OR HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[BEV]) OR HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[CAMERAMAN])
			IF GET_GAME_TIMER() > iShootLineTimer
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF iCurrentBevShootLine < NUM_SHOOTING_LINES
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_ATTACK", sBevShootLines[iCurrentBevShootLine], CONV_PRIORITY_MEDIUM)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.pedID[BEV])
							iCurrentBevShootLine++
							iShootLineTimer = GET_GAME_TIMER() + iShootLineDelay
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_BEG", CONV_PRIORITY_MEDIUM)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.pedID[BEV])
							iShootLineTimer = GET_GAME_TIMER() + iShootLineDelay
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is stood between Beverly and the cameraman, blocking filming
/// RETURNS:
///    True if the player is in the area between Beverly and the cameraman
FUNC BOOL IsPlayerBlockingFilming()
	VECTOR vBevPos = GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[BEV]) + <<0,0,1>>
	VECTOR vCamPos = GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[CAMERAMAN]) - <<0,0,1>>
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBevPos, vCamPos, 0.5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Beverly and the cameraman are on the final line of their conversation. Don't want to do the interruption in this case
/// RETURNS:
///    TRUE if their conversation is playing anything other than the final line
FUNC BOOL NotOnFinalLine()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() < iFinalLine[iCurrentRestart]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    State machine to deal with reactions to the player interrupting Beverly's filming by walking in front of the camera
PROC PlayerBlocksFilming()
	IF eInterruptionStage <> IS_DONE AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
		SWITCH eInterruptionStage
			CASE IS_READY
				IF iCurrentRestart < NUM_INTERRUPTIONS-1
					IF IsPlayerBlockingFilming() AND NotOnFinalLine()
						bAllowLeaving = FALSE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eInterruptionStage = IS_INTERRUPTION
					ENDIF
				ELSE
					bAllowLeaving = TRUE
					eInterruptionStage = IS_DONE
				ENDIF
			BREAK
			
			CASE IS_INTERRUPTION
				IF iCurrentInterruption < NUM_INTERRUPTIONS
					enumSubtitlesState eSubtitles
					IF IS_MESSAGE_BEING_DISPLAYED()
						eSubtitles = DO_NOT_DISPLAY_SUBTITLES
					ELSE
						eSubtitles = DISPLAY_SUBTITLES
					ENDIF
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_BLOCK", sInterruptConv[iCurrentInterruption], CONV_PRIORITY_MEDIUM, eSubtitles)
						iCurrentInterruption++
						eInterruptionStage = IS_RESTART
					ENDIF
				ELSE
					bAllowLeaving = TRUE
					eInterruptionStage = IS_DONE
				ENDIF
			BREAK
			
			CASE IS_RESTART
				
				/*IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
					bAllowSynchronisedScene = FALSE
					STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[BEV], SLOW_BLEND_OUT, TRUE)
					IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
						STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], REALLY_SLOW_BLEND_OUT, TRUE)
						TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[CAMERAMAN])
						CPRINTLN(DEBUG_MISSION,"Forcing ped anim update on cameraman 2")
					ENDIF
					STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[MAKEUPWMAN], SLOW_BLEND_OUT, TRUE)
					TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[MAKEUPWMAN], PLAYER_PED_ID(), 2800)
					SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
				ENDIF*/
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IsPlayerBlockingFilming()
						bAllowLeaving = FALSE
						eInterruptionStage = IS_INTERRUPTION
					ELSE
						IF iCurrentRestart < NUM_RESTARTS
							enumSubtitlesState eSubtitles
							IF IS_MESSAGE_BEING_DISPLAYED()
								eSubtitles = DO_NOT_DISPLAY_SUBTITLES
							ELSE
								eSubtitles = DISPLAY_SUBTITLES
							ENDIF
							IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", sRestartConv[iCurrentRestart], CONV_PRIORITY_MEDIUM, eSubtitles)
								bAllowLeaving = TRUE
								iCurrentRestart++
								eInterruptionStage = IS_READY
							ENDIF
						ELSE
							bAllowLeaving = TRUE
							eInterruptionStage = IS_DONE
						ENDIF
						//bAllowSynchronisedScene = TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers dialogue from the cameraman if the player threatens Beverly
PROC CameramanDialogue()
	IF NOT bAllowThreatConv AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF iCurrentCameraLine < NUM_CAMERA_LINES AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_GAME_TIMER() > iCameraLineTimer
		AND GET_DISTANCE_BETWEEN_PEDS(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID()) < 10.0 AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_CAMERA", sCameraTalk[iCurrentCameraLine], CONV_PRIORITY_MEDIUM)
				iCurrentCameraLine++
				iCameraLineTimer = GET_GAME_TIMER() + iCameraLineDelay
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC FightBevDialogue()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
			IF GET_GAME_TIMER() > iFightLineTimer
				CPRINTLN(DEBUG_MISSION,"Ready for fighting line")
				IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_FIST", CONV_PRIORITY_MEDIUM)
					iFightLineTimer = GET_GAME_TIMER() + FIGHT_LINE_TIME
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the cameraman play his holding a video camera anim if he needs to, stops him playing it if it's appropriate
PROC ControlCameramanAnims()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[CAMERAMAN]) //AND IS_ENTITY_ATTACHED_TO_ANY_OBJECT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
			IF IS_PED_BEING_STEALTH_KILLED(sRCLauncherDataLocal.pedID[CAMERAMAN])
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
					STOP_SOUND(iCameraSoundID)
					SAFE_RELEASE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], SLOW_BLEND_OUT, TRUE)
					SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
				ELSE
					STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
				ENDIF
			ELIF IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
				STOP_SOUND(iCameraSoundID)
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.ObjID[CAMERAMAN])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iBeverlyFilmingScene)
					STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], SLOW_BLEND_OUT, TRUE)
					SET_SYNCHRONIZED_SCENE_PHASE(iBeverlyFilmingScene, 1.0)
				ELSE
					STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase)
				ENDIF
			ENDIF
		ELIF NOT bCamermanFleeing 
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_SYNCHRONIZED_SCENE)
		AND NOT IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_MELEE_COMBAT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
		AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
		AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN])
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[CAMERAMAN], SCRIPT_TASK_SMART_FLEE_PED)
		AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[CAMERAMAN])
			//PRINT_NOW("Playing camera anim after threat", 7000, 1)
			TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[CAMERAMAN], sMissionAnims, sCamFilmBase, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops Beverly playing the hands up anim if the player knocks him over, restarts it once he's back onhis feet
PROC ControlBevHandsUp()
	IF bNotScaredYet = FALSE AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND eCarStealStage = CSS_READY
		IF (NOT IS_PED_GETTING_UP(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_PRONE(sRCLauncherDataLocal.pedID[BEV])
		AND NOT IS_PED_RAGDOLL(sRCLauncherDataLocal.pedID[BEV]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[BEV], TRUE)
		AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[BEV], SCRIPT_TASK_ENTER_VEHICLE) AND NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[BEV]))
		OR IS_PED_BEING_STEALTH_KILLED(sRCLauncherDataLocal.pedID[BEV])
			IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim) AND HAS_ANIM_DICT_LOADED(sMissionAnims)
				TASK_PLAY_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim, SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
			ENDIF
		ELSE
			IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
				STOP_ANIM_TASK(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ControlFilmingBrawl()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_UNINJURED(PLAYER_PED_ID())
	AND NOT bStopCheckingNewsvan AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[CAMERAMAN]) //AND IS_ENTITY_ATTACHED_TO_ANY_OBJECT(sRCLauncherDataLocal.pedID[CAMERAMAN])
		IF IS_PED_IN_COMBAT(sRCLauncherDataLocal.pedID[BEV])
			IF GET_GAME_TIMER() > iCamMoveTimer
				SendCameramanToSuitablePlace()
				iCamMoveTimer = GET_GAME_TIMER() + CAM_MOVE_TIME
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[BEV], sMissionAnims, sHandsUpAnim)
			IF GET_GAME_TIMER() > iCamLookTimer
				IF bFilmBev
					TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], sRCLauncherDataLocal.pedID[BEV], -1)
					bFilmBev = FALSE
				ELSE
					TASK_TURN_PED_TO_FACE_ENTITY(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID(), -1)
					bFilmBev = TRUE
				ENDIF
				iCamLookTimer = GET_GAME_TIMER() + CAM_LOOK_TIME + (GET_RANDOM_INT_IN_RANGE(0, 20) * 100)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//*************************************************************************************************************************************************
//													:INITAL SETUP FUNCTIONS:
//*************************************************************************************************************************************************

/// PURPOSE:
///    Initiate the mission and load text and anims
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			#IF IS_DEBUG_BUILD
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
			#ENDIF
				
			REQUEST_ADDITIONAL_TEXT("PAP4", MISSION_TEXT_SLOT)
			
			REQUEST_ANIM_DICT(sMissionAnims)
			REQUEST_ANIM_DICT(sMakeupScaredAnims)
			REQUEST_ANIM_DICT(sHandsupFlinch)
			REQUEST_CLIP_SET(sWeaponMoveClipset)
			
			REQUEST_WAYPOINT_RECORDING(sWaypointRecBev)
			REQUEST_WAYPOINT_RECORDING(sWaypointRecCam)
			
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				CPRINTLN(DEBUG_MISSION,"Mission Stage INITMISSION")
				
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(ConversationPeds, FRANKLIN_ID, PLAYER_PED_ID(), "FRANKLIN")
				ENDIF
				SetupDialogue()
				
				SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, FALSE)
	
				ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_a"), FALSE) 
				ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_takedown_heavy"), FALSE) 
				ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_armed_takedown"), FALSE)
				
				sBevShootLines[0] = "PAP4_ATTACK_1"
				sBevShootLines[1] = "PAP4_ATTACK_2"
				sBevShootLines[2] = "PAP4_ATTACK_3"
				
				sCameraTalk[0] = "PAP4_CAMERA_1"
				sCameraTalk[1] = "PAP4_CAMERA_2"
				sCameraTalk[2] = "PAP4_CAMERA_3"
				sCameraTalk[3] = "PAP4_CAMERA_4"
				
				sRestartConv[0] = "PAP4_TV_2"
				sRestartConv[1] = "PAP4_TV_3"
				sRestartConv[2] = "PAP4_TV_4"
				sInterruptConv[0] = "PAP4_BLOCK_1"
				sInterruptConv[1] = "PAP4_BLOCK_2"
				sInterruptConv[2] = "PAP4_BLOCK_3"
				sInterruptConv[3] = "PAP4_BLOCK_4"
				iFinalLine[0] = 17
				iFinalLine[1] = 8
				iFinalLine[2] = 5
				iFinalLine[3] = 2
				
				SetNavmeshState(FALSE)
				
				iCameraSoundID = GET_SOUND_ID()
				IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
					PLAY_SOUND_FROM_ENTITY(iCameraSoundID, sCameraSound, sRCLauncherDataLocal.ObjID[CAMERAMAN], sCameraSoundset)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "INTRO"    
					s_skip_menu[1].sTxtLabel = "DEAL WITH BEV"					
				#ENDIF
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP	
			IF IS_REPLAY_IN_PROGRESS()
				START_REPLAY_SETUP(vPlayerReset, fPlayerReset)
				RestartCheckpoint()
			ELSE
				IF IS_REPEAT_PLAY_ACTIVE()
					REQUEST_CUTSCENE("pap_4_rcm")
					WHILE NOT HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						WAIT(0)
					ENDWHILE
					SetupDialogue()
				ENDIF
				
				// Setup pushin camera
				IF IS_SCREEN_FADED_IN() // Don't do gameplay hint if skipping to intro
				AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) > 6
					SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0, 0, 0>>), TRUE, -1, 3000)															
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.45)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.01)
	                SET_GAMEPLAY_HINT_FOV(30.00)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0], -1)
					iLeadInTimer = GET_GAME_TIMER() + 3000
				ELSE
					iLeadInTimer = -1
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "PAP4: Not doing leadin focus camera") #ENDIF
				ENDIF
				NEXT_STAGE()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Loads and plays the intro cutscene
PROC INTRO()
	SWITCH eState
		CASE SS_INIT
			SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
			
			IF GET_GAME_TIMER() > iLeadInTimer
				RC_REQUEST_CUTSCENE("pap_4_rcm")	
				IF RC_IS_CUTSCENE_OK_TO_START()
					CPRINTLN(DEBUG_MISSION,"Mission Stage INTRO")
					
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAlternateApproachPos1, vAlternateApproachPos2, fAlternateApproachWidth)
							bShowPlayer = TRUE
						ENDIF
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[BEV], "Beverley", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[CAMERAMAN], "film_guy", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[MAKEUPWMAN], "makeup_artist", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					// hide the two cameras, easier than getting the cutscene set up to link to them
					IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[BEV])
						//SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[BEV], FALSE)
						DETACH_ENTITY(sRCLauncherDataLocal.ObjID[BEV], FALSE)
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.ObjID[BEV], "Beverlys_camera", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
						//SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[CAMERAMAN], FALSE)
						DETACH_ENTITY(sRCLauncherDataLocal.ObjID[CAMERAMAN], FALSE)
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.ObjID[CAMERAMAN], "Video_camera_forMan", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					RC_CLEANUP_LAUNCHER()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
					
					START_CUTSCENE()
					WAIT(0)
					
					IF bShowPlayer
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						iShowPlayerTimer = -1
					ENDIF
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
					
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[BEV])
						SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.vehID[BEV], vBevCarPos, fBevCarHeading)
						SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[BEV])
						SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[BEV], VEHICLELOCK_UNLOCKED)
					ENDIF
				
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[CAMERAMAN])
						SAFE_TELEPORT_ENTITY(sRCLauncherDataLocal.vehID[CAMERAMAN], vVanPos, fVanHeading)//<<-493.66, 240.93, 82.94>>, 263.55)
						SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[CAMERAMAN])
						SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[CAMERAMAN], VEHICLELOCK_UNLOCKED)
					ENDIF
					//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaPos1, vIntroAreaPos2, fIntroAreaWidth, <<-500.71, 241.83, 82.46>>, 262.59, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(vIntroAreaPos1, vIntroAreaPos2, fIntroAreaWidth, <<-500.71, 241.83, 82.46>>, 262.59)
					RC_START_CUTSCENE_MODE(<<-484.20, 229.68, 82.21>>, TRUE, TRUE, TRUE, FALSE)
					CLEAR_AREA_OF_PEDS(<<-483.503723,231.055862,82.145599>>, 15)
					CLEAR_AREA_OF_OBJECTS(<<-483.503723,231.055862,82.145599>>, 15)
					
					bAllowCameraSpamming = TRUE
					
					eState = SS_ACTIVE
				ENDIF
			ELSE
				RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 3000)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ELIF bAllowCameraSpamming
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					bAllowCameraSpamming = FALSE
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
			AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[BEV]) AND DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley") AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("film_guy") AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("makeup_artist")
				AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverlys_camera") AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Video_camera_forMan")
					//ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[BEV], sRCLauncherDataLocal.pedID[BEV], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[BEV], BONETAG_R_HAND), vBevOffset, vBevRot)
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[BEV], sRCLauncherDataLocal.pedID[BEV], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[BEV], BONETAG_PH_R_HAND), vBevOffset, vBevRot)
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherDataLocal.ObjID[CAMERAMAN], sRCLauncherDataLocal.pedID[CAMERAMAN], GET_PED_BONE_INDEX(sRCLauncherDataLocal.pedID[CAMERAMAN], BONETAG_R_HAND), vCamOffset, vCamRot)
					StartFilmingScene()
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()//-136.0613)//141.5441)//-53.6125)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()//2.3862)//-19.0425)//-25.2968)
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				
				IF bShowPlayer AND GET_GAME_TIMER() > iShowPlayerTimer
					IF iShowPlayerTimer < 0
						iShowPlayerTimer = GET_GAME_TIMER() + 4000
					ELSE
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						bShowPlayer = FALSE
					ENDIF
				ENDIF
			ELSE
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_MESKIPPED
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
			ELSE
				eState = SS_CLEANUP
			ENDIF
		BREAK
			
		CASE SS_CLEANUP
			SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
						
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[MAKEUPWMAN])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[BEV], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
				//SET_PED_SUFFERS_CRITICAL_HITS(sRCLauncherDataLocal.pedID[BEV], FALSE)
				
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[BEV], PCF_PedsJackingMeDontGetIn, TRUE)
				SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[BEV], PCF_AllowToBeTargetedInAVehicle, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherDataLocal.pedID[BEV], TRUE)
				
				SET_PED_LOD_MULTIPLIER(sRCLauncherDataLocal.pedID[BEV], 1.5)
				SET_PED_LOD_MULTIPLIER(sRCLauncherDataLocal.pedID[CAMERAMAN], 1.5)
				SET_PED_LOD_MULTIPLIER(sRCLauncherDataLocal.pedID[MAKEUPWMAN], 1.5)
					
				/*giBevGroup = CREATE_GROUP(DEFAULT_TASK_ALLOCATOR_FOLLOW_ANY_MEANS)
				SET_PED_AS_GROUP_LEADER(sRCLauncherDataLocal.pedID[BEV], giBevGroup)
				SET_PED_NEVER_LEAVES_GROUP(sRCLauncherDataLocal.pedID[BEV], TRUE)
				SET_PED_AS_GROUP_MEMBER(sRCLauncherDataLocal.pedID[CAMERAMAN], giBevGroup)
				SET_PED_AS_GROUP_MEMBER(sRCLauncherDataLocal.pedID[MAKEUPWMAN], giBevGroup)
				SET_PED_NEVER_LEAVES_GROUP(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
				SET_PED_NEVER_LEAVES_GROUP(sRCLauncherDataLocal.pedID[MAKEUPWMAN], TRUE)
				SET_GROUP_FORMATION(giBevGroup, FORMATION_SURROUND_FACING_INWARDS)
				SET_GROUP_FORMATION_SPACING(giBevGroup, 5.0)*/
				
				//StartFilmingScene()
				
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) //AND NOT WAS_CUTSCENE_SKIPPED()
					IF WAS_CUTSCENE_SKIPPED()
						//SET_ENTITY_HEADING(PLAYER_PED_ID(), 103.3318)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					/*ELSE
						TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[BEV])*/
					ENDIF
				ENDIF
			ENDIF
			// show the two cameras again
			/*IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[BEV])
				SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[BEV], TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.ObjID[CAMERAMAN])
				SET_ENTITY_VISIBLE(sRCLauncherDataLocal.ObjID[CAMERAMAN], TRUE)
			ENDIF*/
			bCamermanFleeing = FALSE
			bMakeupWomanFleeing = FALSE
			
			/*WHILE IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
				WAIT(0)
			ENDWHILE*/
			RC_END_CUTSCENE_MODE()
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			NEXT_STAGE()
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Deals with the on foot section of the mission
PROC BEV_ON_FOOT()
	SWITCH eState
		CASE SS_INIT
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN])
				IF CREATE_CONVERSATION(ConversationPeds, "PAP4AUD", "PAP4_TV_TALK", CONV_PRIORITY_MEDIUM)
					CPRINTLN(DEBUG_MISSION,"Mission Stage BEV_ON_FOOT")
					biGOTO = CREATE_PED_BLIP(sRCLauncherDataLocal.pedID[BEV])
					INIT_FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, TRUE, FALSE)
					//PRINT_HELP("PAP4_OBJ")
					SET_PED_DIES_WHEN_INJURED(sRCLauncherDataLocal.pedID[BEV], TRUE)
					iCurrentBevShootLine = 0
					iCurrentCameraLine = 0
					bStartShootingReaction = TRUE
					//bBeverlyWaiting = TRUE
					bAllowThreatConv = TRUE
					bNotScaredYet = TRUE
					eInterruptionStage = IS_READY
					bAllowLeaving = TRUE
					iCurrentRestart = 0
					iCurrentInterruption = 0
					iShootLineTimer = 0
					iCheckDoorsTimer = 0
					//iShockingEventId = -1
					bCameraGuyWalks = FALSE
					bFirstTimeScared = TRUE
					bBeverlyFleeing = FALSE
					eState = SS_ACTIVE
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			StartBevToCar()
			CheckBevInCar()
			CheckCameramanInCar()
			CarStolenOrDestroyed()
			CheckCarDoors()
			BeverlysCarBlockedIn()
			NewsVanStolen()
			PlayerIsAggressive()
			FightBevDialogue()
			PlayerShootingDialogue()
			CameramanDialogue()
			ControlSynchronisedScenes()
			PlayerBlocksFilming()
			FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, FALSE)
			CheckMissionPassed()
			ControlCameramanAnims()
			ControlBevHandsUp()
			ControlFilmingBrawl()
			HandleCamermanFleeing()
			HandleMakeupWomanFleeing()
			CheckForAllKilled()
		BREAK
		
		CASE SS_CLEANUP
			NEXT_STAGE()
			SAFE_REMOVE_BLIP(biGOTO)
			KILL_FACE_TO_FACE_CONVERSATION()
		BREAK
		
		CASE SS_MESKIPPED
			eMissionStage = MS_PASSED
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Deals with the in car section of the mission
PROC BEV_IN_CAR()
	SWITCH eState
		CASE SS_INIT
			CPRINTLN(DEBUG_MISSION,"Mission Stage BEV_IN_CAR")
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			CarStolenOrDestroyed()
			NewsVanStolen()
			CheckCameramanInCar()
			PlayerShootingDialogue()
			BeverlysCarBlockedIn()
			CameramanDialogue()
			FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, FALSE)
			CheckMissionPassed()
			ControlCameramanAnims()
			HandleCamermanFleeing()
			HandleMakeupWomanFleeing()
			SwitchWaypointToAI()
			CheckForAllKilled()
		BREAK
		
		CASE SS_CLEANUP
			NEXT_STAGE()
			SAFE_REMOVE_BLIP(biGOTO)
			KILL_FACE_TO_FACE_CONVERSATION()
		BREAK
		
		CASE SS_MESKIPPED
			eMissionStage = MS_PASSED
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    delays passing the mission by a short amount to allow checking for all peds being killed in one go
PROC PASS_DELAY
	SWITCH eState
		CASE SS_INIT
			
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_LOW)
			
			CPRINTLN(DEBUG_MISSION,"Mission Stage MS_PASSED")
			iPassTimer = GET_GAME_TIMER() + KILL_ALL_TIME
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			CheckForAllKilled()
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_ENTITY_ON_FIRE(sRCLauncherDataLocal.pedID[CAMERAMAN])
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF NOT IS_PED_FLEEING(sRCLauncherDataLocal.pedID[CAMERAMAN]) AND NOT IS_PED_IN_ANY_VEHICLE(sRCLauncherDataLocal.pedID[CAMERAMAN], TRUE)
						SET_PED_FLEE_ATTRIBUTES(sRCLauncherDataLocal.pedID[CAMERAMAN], FA_DISABLE_HANDS_UP, TRUE)
						TASK_SMART_FLEE_PED(sRCLauncherDataLocal.pedID[CAMERAMAN], PLAYER_PED_ID(), 500, -1)
					ENDIF
				ELSE
					TASK_WANDER_STANDARD(sRCLauncherDataLocal.pedID[CAMERAMAN])
				ENDIF
			ENDIF
			IF GET_GAME_TIMER() > iPassTimer
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CheckForAllKilled()
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_4(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_PRC")
		WAIT(0)

		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
		
		SWITCH eMissionStage
			CASE MS_INIT
				INITMISSION()
			BREAK
			
			CASE MS_INTRO
				INTRO()
			BREAK
			
			CASE MS_BEV_ON_FOOT
				BEV_ON_FOOT()
			BREAK
			
			CASE MS_BEV_IN_CAR
				BEV_IN_CAR()
			BREAK
			
			CASE MS_PASSED
				PASS_DELAY()
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			// Check debug completion/failure
			DEBUG_Check_Debug_Keys()
		#ENDIF	
	ENDWHILE
	

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

