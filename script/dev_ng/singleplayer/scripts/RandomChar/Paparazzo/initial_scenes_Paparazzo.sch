USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE
MODEL_NAMES         mContactModel      = GET_NPC_PED_MODEL(CHAR_BEVERLY)
MODEL_NAMES         BEVERLY_BIKE       = PCJ
INT 			    iSynchScene

/// PURPOSE:
///    Blocks scenarios near Bev Pap2 (security guard)
FUNC SCENARIO_BLOCKING_INDEX Pap2_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<-74.8392, 300.5241, 104.0>>, <<-62.4670, 318.6961, 109.0>>)
ENDFUNC

/// PURPOSE:
///    Handles area clearance for initial scenes where needed
PROC SETUP_AREA_FOR_MISSION(g_eRC_MissionIDs eMissionID, BOOL bEnable)
	
	SWITCH eMissionID
		CASE RC_PAPARAZZO_1
			IF bEnable
				// B*1441870 - ensure ambient veh doesn't park where player veh gets repositioned to
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>, FALSE)
				REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>)		
			ELSE
				// Restore everything to normal
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>, TRUE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 
///    Setup scene for Paparazzo 1
FUNC BOOL SetupScene_PAPARAZZO_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  			0
	CONST_INT MODEL_CONTACT_VEHICLE  	1
	CONST_INT MODEL_CONTACT_CAMERA		2
	CONST_INT OBJECT_CONTACT_CAMERA		0
	
	// Variables
	MODEL_NAMES    mModel[3]
	INT            iCount
	BOOL           bCreatedScene
	
	// Idle anims for Beverly...
	// Sequence setup here since he has 4 idle anims but no beckon animations
	SEQUENCE_INDEX seqIndexAnims
	STRING         sAnimDict_BeverlyIdles = "rcmpaparazzo1beckon"
	
	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = BEVERLY_BIKE
	mModel[MODEL_CONTACT_CAMERA]  = PROP_PAP_CAMERA_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			// NOTE: for 1426354 - added an exclusion zone for this area which is defined in ARE_RC_TRIGGER_CONDITIONS_MET() in rc_launcher_public.sch
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-154.981628,287.043152,96.638229>> //<<-154.981628,287.043152,96.638229>> //<<-153.115784,287.325256,91.080650>>
			sRCLauncherData.triggerLocate[1] = <<-155.494583,274.406738,90.823593>> //<<-155.480530,274.983063,90.802071>> //<<-153.106110,277.018097,96.666283>>
			sRCLauncherData.triggerWidth = 15.000000 //14.000000 //10.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE				// B*1352466
			sRCLauncherData.bVehsCritical = FALSE				// B*1278353
			sRCLauncherData.sIntroCutscene = "PAP_1_RCM"
			
			// Only set the dictionary so the launcher will remove the 
			// animation dictionary if the script has to terminate...
			sRCLauncherData.sAnims.sDictionary = sAnimDict_BeverlyIdles
			
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT(sAnimDict_BeverlyIdles)	
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sAnimDict_BeverlyIdles)
				RETURN FALSE
			ENDIF
			
			// Setup scene
			SETUP_AREA_FOR_MISSION(RC_PAPARAZZO_1, TRUE)
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
		
			// Create Beverly
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BEVERLY, << -149.75, 285.81, 93.67 >> , 135.0, "PAPARAZZO LAUNCHER RC")
					
					// Set Beverly cap is now part of his default variation 
					// as it's been moved from P_head_000 (hat) is now called berd_000
					// See B*1473487
					SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[0])
					
					// Removes the additional camera around his neck
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0)
				ELSE
					bCreatedScene = FALSE			
				ENDIF
			ENDIF
		
			// Setup Beverly with his camera
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					CREATE_SCENE_PROP(sRCLauncherData.ObjID[OBJECT_CONTACT_CAMERA], mModel[MODEL_CONTACT_CAMERA], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherData.pedID[0], << 0.0, 0.0, 2.5 >>), 0.0)
					IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
						ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						SET_MODEL_AS_NO_LONGER_NEEDED(mModel[MODEL_CONTACT_CAMERA])
					ELSE
						bCreatedScene = FALSE	
					ENDIF
				ELSE
					bCreatedScene = FALSE	
				ENDIF
			ENDIF
				
			// Beverly's bike
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CONTACT_VEHICLE], << -159.56, 275.41, 93.14 >>, 102.75)
				IF IS_VEHICLE_OK(sRCLauncherData.vehID[0])
					SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0],62,62)	// Blue
				ELSE
					bCreatedScene = FALSE	
				ENDIF
			ENDIF
			
			// Apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Not using SETUP_LAUNCHER_ANIMS because Beverly has 4 idle anims and no beckon anims
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				IF HAS_ANIM_DICT_LOADED("rcmpaparazzo1beckon")
					OPEN_SEQUENCE_TASK(seqIndexAnims)
						TASK_PLAY_ANIM(NULL, sAnimDict_BeverlyIdles, "pap_idle_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, sAnimDict_BeverlyIdles, "pap_idle_action_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, sAnimDict_BeverlyIdles, "pap_idle_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, sAnimDict_BeverlyIdles, "pap_idle_action_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						SET_SEQUENCE_TO_REPEAT(seqIndexAnims, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seqIndexAnims)												
					TASK_PERFORM_SEQUENCE(sRCLauncherData.pedID[0], seqIndexAnims)
					CLEAR_SEQUENCE_TASK(seqIndexAnims)
				ENDIF
			ENDIF

			// Unload anims
			REMOVE_ANIM_DICT(sAnimDict_BeverlyIdles)
			
			// Release models no longer needed
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				IF iCount <> MODEL_CONTACT_VEHICLE
					// release all models except Beverly's bike...this is needed for the mission script.
					SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
				ENDIF
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Paparazzo 2
FUNC BOOL SetupScene_PAPARAZZO_2(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT  			0
	CONST_INT MODEL_CONTACT_VEHICLE  	1
	CONST_INT MODEL_CONTACT_CAMERA		2
	CONST_INT OBJECT_CONTACT_CAMERA		0
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = FQ2
	mModel[MODEL_CONTACT_CAMERA]  = PROP_PAP_CAMERA_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-82.641426,302.730652,104.634941>>//<<-79.494011,302.703308,104.774582>>
			sRCLauncherData.triggerLocate[1] = <<-68.784050,296.631714,107.895782>>//<<-71.261536,298.654968,108.056557>>
			sRCLauncherData.triggerWidth = 11.000000//10.500
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "PAP_2_RCM_P2"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT("rcmpaparazzo_2")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_2")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Beverly
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BEVERLY, << -71.283, 301.557, 106.711 >>, 4.659, "PAPARAZZO LAUNCHER RC")
					
					// Set Beverly cap is now part of his default variation 
					// as it's been moved from P_head_000 (hat) is now called berd_000
					// See B*1473487
					SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[0])
					
					// Removes the additional camera around his neck
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0)
				ELSE	
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Setup Beverly's camera
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
				IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
					CREATE_SCENE_PROP(sRCLauncherData.ObjID[OBJECT_CONTACT_CAMERA], mModel[MODEL_CONTACT_CAMERA], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherData.pedID[0], << 0.0, 0.0, 2.5 >>), 0.0)
					IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
						ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						SET_MODEL_AS_NO_LONGER_NEEDED(mModel[MODEL_CONTACT_CAMERA])
					ELSE
						bCreatedScene = FALSE
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
		
			// Beverly's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CONTACT_VEHICLE], << -78.0873, 299.0679, 105.3972 >>, 249.8696 )
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0],7)
				SET_VEHICLE_CAN_LEAK_OIL(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_CAN_LEAK_PETROL(sRCLauncherData.vehID[0], FALSE)
				SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(sRCLauncherData.vehID[0], 0) 
				SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[0],"P4P4R4Z0")
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			ENDIF
			
			// Ready to aplly anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE	
		
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0]) AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0],PLAYER_PED_ID(),-1)
			
				iSynchScene = CREATE_SYNCHRONIZED_SCENE(<<-71.283, 301.557, 106.711>>,<<0.000, -0.000, 4.659>>)
				SET_SYNCHRONIZED_SCENE_LOOPED(iSynchScene, TRUE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSynchScene, "rcmpaparazzo_2", "pap_2_rcm_base", 2, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sRCLauncherData.pedID[0],FALSE)
				SET_PED_CONFIG_FLAG(sRCLauncherData.pedID[0],PCF_UseKinematicModeWhenStationary,TRUE)
			ENDIF
			
			// Unload anims
			REMOVE_ANIM_DICT("rcmpaparazzo_2")

			// Removal zones
			// This gets re-enabled in launcher / mission when needed
			SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_PAPARAZZI", FALSE) 
			CLEAR_AREA_OF_PEDS(<< -70.12, 298.33, 105.25 >>, 30)

			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Paparazzo 3
FUNC BOOL SetupScene_PAPARAZZO_3(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	// Models
	CONST_INT MODEL_CONTACT  		0
	CONST_INT MODEL_CONTACT_CAMERA 	1
	CONST_INT MODEL_DUMPSTER 		2
	CONST_INT MODEL_LID_LEFT 		3
	CONST_INT MODEL_LID_RIGHT 		4
	
	// Objects
	CONST_INT OBJ_DUMPSTER			0
	CONST_INT OBJ_LIDL				1
	CONST_INT OBJ_LIDR				2
	CONST_INT OBJECT_CONTACT_CAMERA 3
	
	// Variables
	MODEL_NAMES  mModel[5]
	OBJECT_INDEX iObject[3]
	VECTOR       vPos[3]
	VECTOR       vRot[3]
	STRING       sAnimDict = "rcmpaparazzo_3leadinoutpap_3_rcm"
	INT          iCount
	BOOL         bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT] 		 = mContactModel
	mModel[MODEL_CONTACT_CAMERA] = PROP_PAP_CAMERA_01
	mModel[MODEL_DUMPSTER]  	 = PROP_CS_DUMPSTER_01A
	mModel[MODEL_LID_LEFT]  	 = PROP_CS_DUMPSTER_LIDL
	mModel[MODEL_LID_RIGHT]  	 = PROP_CS_DUMPSTER_LIDR
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-258.505188,287.597595,90.178276>>
			sRCLauncherData.triggerLocate[1] = <<-258.526306,298.090485,93.694559>>
			sRCLauncherData.triggerWidth = 7.0
			sRCLauncherData.bAllowVehicleActivation = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "PAP_3_RCM"
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT(sAnimDict)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE

			// Create Beverly
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BEVERLY, << -261.0, 292.22, 90.89 >> , 10.53, "PAPARAZZO LAUNCHER RC")
					
					// Set Beverly cap is now part of his default variation 
					// as it's been moved from P_head_000 (hat) is now called berd_000
					// See B*1473487
					SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[0])
					
					// Removes the additional camera around his neck
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// Setup Beverly's camera
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
				IF NOT DOES_ENTITY_EXIST(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
					CREATE_SCENE_PROP(sRCLauncherData.ObjID[OBJECT_CONTACT_CAMERA], mModel[MODEL_CONTACT_CAMERA], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRCLauncherData.pedID[0], << 0.0, 0.0, 2.5 >>), 0.0)
					IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA])
						ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.objID[OBJECT_CONTACT_CAMERA], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						SET_MODEL_AS_NO_LONGER_NEEDED(mModel[MODEL_CONTACT_CAMERA])
					ELSE
						bCreatedScene = FALSE
					ENDIF
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to proceed..
			IF bCreatedScene
				
				// Store values of existing dumpster and lids
				// Replaced the GET_ENTITY... commands with fixed coordinates B*1525906
				iObject[OBJ_DUMPSTER] = GET_CLOSEST_OBJECT_OF_TYPE(<< -260.62, 292.1391, 90.604 >>, 10.0, PROP_CS_DUMPSTER_01A)
				vPos[OBJ_DUMPSTER] = << -260.614014, 292.105988, 91.126999 >>
				vRot[OBJ_DUMPSTER] = << 0.000072,-2.500116,89.639977 >>
				
				iObject[OBJ_LIDL] = GET_CLOSEST_OBJECT_OF_TYPE(<< -261.58, 291.66, 92.06 >>, 10.0, PROP_CS_DUMPSTER_LIDL)
				vPos[OBJ_LIDL] = << -261.107971, 292.506165, 92.036453 >>
				vRot[OBJ_LIDL] = << -0.656039, -2.500180, 89.611404 >>
				
				iObject[OBJ_LIDR] = GET_CLOSEST_OBJECT_OF_TYPE(<< -261.5835, 291.6671, 92.0660 >>, 10.0, PROP_CS_DUMPSTER_LIDR)
				vPos[OBJ_LIDR] = << -261.113525, 291.624939, 91.997650 >>
				vRot[OBJ_LIDR] = << -0.281056, -2.500049, 89.627686 >>
				
				// Release references
				FOR iCount=0 TO 2
					SET_OBJECT_AS_NO_LONGER_NEEDED(iObject[iCount])
				ENDFOR
	
				// Hide existing dumpster
				CREATE_MODEL_HIDE(<< -260.62, 292.13, 90.60 >>, 10.0, PROP_CS_DUMPSTER_01A, TRUE)
				CREATE_MODEL_HIDE(<< -261.58, 291.66, 92.06 >>, 10.0, PROP_CS_DUMPSTER_LIDR, TRUE)
				CREATE_MODEL_HIDE(<< -261.54, 292.54, 92.10 >>, 10.0, PROP_CS_DUMPSTER_LIDL, TRUE)
				
				// Recreate dumpster
				sRCLauncherData.objID[OBJ_DUMPSTER] = CREATE_OBJECT_NO_OFFSET(PROP_CS_DUMPSTER_01A, vPos[OBJ_DUMPSTER])
				SET_ENTITY_ROTATION(sRCLauncherData.objID[OBJ_DUMPSTER], vRot[OBJ_DUMPSTER])
				SET_CAN_CLIMB_ON_ENTITY(sRCLauncherData.objID[OBJ_DUMPSTER], FALSE)

				sRCLauncherData.objID[OBJ_LIDL] = CREATE_OBJECT_NO_OFFSET(PROP_CS_DUMPSTER_LIDL, vPos[OBJ_LIDL])
				SET_ENTITY_ROTATION(sRCLauncherData.objID[OBJ_LIDL], vRot[OBJ_LIDL])
				SET_CAN_CLIMB_ON_ENTITY(sRCLauncherData.objID[OBJ_LIDL], FALSE)
				
				sRCLauncherData.objID[OBJ_LIDR] = CREATE_OBJECT_NO_OFFSET(PROP_CS_DUMPSTER_LIDR, vPos[OBJ_LIDR])
				SET_ENTITY_ROTATION(sRCLauncherData.objID[OBJ_LIDR], vRot[OBJ_LIDR])
				SET_CAN_CLIMB_ON_ENTITY(sRCLauncherData.objID[OBJ_LIDR], FALSE)
				
				// Ready to recreate apply anims
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
			
		CASE IS_COMPLETE_SCENE
			
			// Create synchronised scene
			IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJ_DUMPSTER])
				
				iSynchScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(sRCLauncherData.objID[OBJ_DUMPSTER]), GET_ENTITY_ROTATION(sRCLauncherData.objID[OBJ_DUMPSTER]))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.objID[OBJ_DUMPSTER], iSynchScene, "idle_closed_pap_3_rcm_dumpster", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_LOOPED(iSynchScene, TRUE)
				
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
					TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSynchScene, sAnimDict, "idle_closed_pap_3_rcm_beverly", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJ_LIDL])
					PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.objID[OBJ_LIDL], iSynchScene, "idle_closed_pap_3_rcm_lid_l", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				IF IS_ENTITY_ALIVE(sRCLauncherData.objID[OBJ_LIDR])
					PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.objID[OBJ_LIDR], iSynchScene, "idle_closed_pap_3_rcm_lid_r", sAnimDict, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Unload anims
			REMOVE_ANIM_DICT(sAnimDict)
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH

	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Paparazzo 3A
FUNC BOOL SetupScene_PAPARAZZO_3A(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CAR 0
	
	// Variables
	MODEL_NAMES mModel[1]
	INT         iCount

	// Assign model names
	mModel[MODEL_CAR] = JACKAL
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_POINT
			sRCLauncherData.triggerLocate[0] = <<301.85, 138.16, 102.84>>
			sRCLauncherData.activationRange = 90.0
			sRCLauncherData.bAllowVehicleActivation = TRUE

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], <<307.02, 143.17, 103.30>>, 250.08)
			SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 2)
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Paparazzo 3B
FUNC BOOL SetupScene_PAPARAZZO_3B(g_structRCScriptArgs& sRCLauncherData)

	sRCLauncherData.triggerType = RC_TRIG_LOCATE_POINT
	sRCLauncherData.triggerLocate[0] = <<1040.96, -534.42, 60.17>>
	sRCLauncherData.activationRange = 90.0
	sRCLauncherData.bAllowVehicleActivation = TRUE 
	
	// Scene is good to go!
	RETURN TRUE
ENDFUNC

/// PURPOSE: 
///    Setup scene for Paparazzo 4
FUNC BOOL SetupScene_PAPARAZZO_4(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_CONTACT 		0
	CONST_INT MODEL_CONTACT_VEHICLE 1
	CONST_INT MODEL_VAN 			2
	CONST_INT MODEL_CAMERAMAN 		3
	CONST_INT MODEL_MAKEUP_LADY 	4
	CONST_INT MODEL_CAMERA1 		5
	CONST_INT MODEL_CAMERA2			6
	
	// Objects
	CONST_INT OBJ_DUMPSTER			0
	CONST_INT OBJ_LIDL				1
	CONST_INT OBJ_LIDR				2
	CONST_INT OBJECT_CONTACT_CAMERA 3
	
	// Variables
	MODEL_NAMES mModel[7]
	STRING      sAnimDict = "rcmpaparazzo_4"
	INT         iCount
	INT         iSceneID
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_CONTACT]         = mContactModel
	mModel[MODEL_CONTACT_VEHICLE] = ISSI2
	mModel[MODEL_VAN]             = RUMPO
	mModel[MODEL_CAMERAMAN]       = S_M_Y_GRIP_01
	mModel[MODEL_MAKEUP_LADY]     = A_F_Y_BEVHILLS_02
	mModel[MODEL_CAMERA1]         = PROP_PAP_CAMERA_01
	mModel[MODEL_CAMERA2]         = PROP_V_CAM_01
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-476.368622,229.574371,82.070770>>
			sRCLauncherData.triggerLocate[1] = <<-513.688293,234.216507,87.289368>> 
			sRCLauncherData.triggerWidth = 25.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bVehsCritical = TRUE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "PAP_4_RCM"

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request animations
			REQUEST_ANIM_DICT(sAnimDict)
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK	
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Beverly
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_BEVERLY, <<-490.809, 232.390, 82.018>>, 33.840, "PAPARAZZO LAUNCHER RC", FALSE)
					
					// Set Beverly cap is now part of his default variation 
					// as it's been moved from P_head_000 (hat) is now called berd_000
					// See B*1473487
					SET_PED_DEFAULT_COMPONENT_VARIATION(sRCLauncherData.pedID[0])
					
					// Removes the additional camera around his neck
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				
					iSceneID = CREATE_SYNCHRONIZED_SCENE(<< -490.809, 232.390, 82.018 >>, << -0.000, 0.000, 33.840 >>)
					SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID,TRUE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID,FALSE)
					TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSceneID, "rcmpaparazzo_4", "pap_4_rcm_leadin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Cameraman
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[1])
				CREATE_SCENE_PED(sRCLauncherData.pedID[1], mModel[MODEL_CAMERAMAN], <<-498.165649,228.628387,82.099480>>, 292.9763)
				IF IS_PED_UNINJURED(sRCLauncherData.pedID[1])
					SET_PED_NAME_DEBUG(sRCLauncherData.pedID[1], "Camera Man")
					TASK_PLAY_ANIM(sRCLauncherData.pedID[1], "rcmpaparazzo_4", "Idle_Camman", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[1], TRUE)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_HEAD, 0, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_HAIR, 0, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_TORSO, 0, 0, 1)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_LEG, 0, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[1], PED_COMP_SPECIAL, 0, 0, 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Makeup woman	
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[2])	
				CREATE_SCENE_PED(sRCLauncherData.pedID[2], mModel[MODEL_MAKEUP_LADY], <<-498.309021,227.911163,82.105995>>, 296.4216)
				IF IS_PED_UNINJURED(sRCLauncherData.pedID[2])
					SET_PED_NAME_DEBUG(sRCLauncherData.pedID[2], "Makeup Woman")
					TASK_PLAY_ANIM(sRCLauncherData.pedID[2], "rcmpaparazzo_4", "Idle_Prod", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_UPPERBODY|AF_SECONDARY)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[2], TRUE)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_HEAD, 1, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_HAIR, 3, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_TORSO, 1, 2, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_LEG, 0, 1, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_SPECIAL, 0, 0, 0)
					SET_PED_COMPONENT_VARIATION(sRCLauncherData.pedID[2], PED_COMP_SPECIAL2, 0, 0, 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
		
			// Beverly's vehicle
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])	
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CONTACT_VEHICLE], <<-497.99, 224.97, 82.67>>, 266.50)
				IF IS_VEHICLE_OK(sRCLauncherData.vehID[0])
					LOWER_CONVERTIBLE_ROOF(sRCLauncherData.vehID[0], TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 0)
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKED)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
	
			// TV van
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])	
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_VAN], <<-501.55, 230.70, 83.10>>, 233.93)//<<-508.14, 232.87, 83.06>>, 233.93)
				IF IS_VEHICLE_OK(sRCLauncherData.vehID[1])
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[1], 1)
					SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[1], VEHICLELOCK_LOCKED)
					SET_VEHICLE_LIVERY(sRCLauncherData.vehID[1], 0)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Prop - Beverly's camera
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[0])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[0], mModel[MODEL_CAMERA1], <<-490.11, 233.15, 82.10>>, 0)
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0]) AND DOES_ENTITY_EXIST(sRCLauncherData.ObjID[0])
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.ObjID[0], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_R_HAND), <<0.1561, -0.0030, -0.0344>>, <<202.8703, -124.4300, -121.5398>>)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Prop - Cameraman's video camera
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[1])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[1], mModel[MODEL_CAMERA2], <<-490.11, 233.15, 82.10>>, 0)
				IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[1]) AND DOES_ENTITY_EXIST(sRCLauncherData.ObjID[1])
					ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.ObjID[1], sRCLauncherData.pedID[1], GET_PED_BONE_INDEX(sRCLauncherData.pedID[1], BONETAG_R_HAND), <<0.1181, 0.2229, -0.1535>>, <<259.2501, 24.0800, -51.2899>>)
				ELSE
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
