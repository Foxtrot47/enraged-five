
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "CompletionPercentage_public.sch"
USING "randomChar_public.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Paparazzo.sch"
USING "chase_hint_cam.sch"
USING "commands_event.sch"
USING "event_public.sch"
USING "commands_recording.sch"

CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					100
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					15	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				25	

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		22
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		12

CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		6

USING "traffic.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

USING "RC_helper_functions.sch"
USING "RC_Threat_public.sch"

USING "taxi_functions.sch" //For B*1860244 - Blocking taxis during mission.

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo2.sc
//		AUTHOR			:	Ste Kerrigan/Tom Kingsley
//		DESCRIPTION		:	Beverly asks Franklin to help him get some 
//							filthy video footage of Vinewood starlet Poppy Michelle.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// The Random Character - sRCLauncherDataLocal.pedID[0]
g_structRCScriptArgs sRCLauncherDataLocal

//INT iTestSound

VECTOR 		  m_vCharPos     = << -70.3883, 301.2539, 105.8003 >>   //<< -70.3368, 302.4749, 106.0909 >> 
FlOAT 		  m_fCharHeading = 66.86 //336.2507

//----------------------
//    CHECKPOINTS
//----------------------
//CONST_INT CP_AFTER_MOCAP                1 //following beverly down the road
CONST_INT CP_FILMING       			    1 //Filming Poppy
CONST_INT CP_GET_TO_CAR      			2 //Running to the car
CONST_INT CP_CHASE       			    3 //The car chase
CONST_INT CP_OUTRO                      4 //Outro

// Mission stages
ENUM MISSION_STAGE
	MS_INIT = 0,
	MS_INTRO,				//Mocap
	MS_FOLLOW_BEV_ROAD,		//Running down the road with beverly
	MS_FOLLOW_BEV_GUARDS,	//Walking through the court yard past the hotel staff
	MS_FOLLOW_TO_POPPY,		//Run accross the pool
	MS_CAMERA_TUTORIAL,		//Bev explains the face recog
	MS_FILM_POPPY,			//film poppy zoomed in on her face
	MS_GET_IN_ESCAPE_CAR,	//player and bev run and get in bev's car
	MS_CAR_CHASE,			//player films poppy chasing them out the boot of the car
	MS_OUTRO,				//bev pulls over and takes the camera off franklin
	MS_PASSED,				//mission passed ---- not needed and should just be a call to the script_passed() func
	MS_FAILED,				//mission failed
	MS_UBER_RECORD,			//record uber recording ---- needs to be debug only
	MS_POPPY_AND_UBA,		//Play uber recording and record poppy  ---- needs to be debug only
	MS_SET_PIECE_RECORDING	//Play uber recording and record setpeice car ---- needs to be debug only
ENDENUM

ENUM MISSION_REQ
	RQ_CUTSCENE,
	RQ_BUDDY,
	RQ_POPPY,
	RQ_SHAGGER,
	RQ_BODYGUARD,
	RQ_BODYGUARD_CARS,
	RQ_HOTEL_STAFF,
	RQ_BROOM,
	RQ_RAG,
	RQ_PLAYER_CAR,
	RQ_ANIMS,
	RQ_STAFF_ANIMS,
	RQ_BEV_PED_RECORDING,
	RQ_CAMERA,
	RQ_CAMERA_CUTSCENE,
	RQ_OUTRO_ANIMS,
	RQ_CAMERA_WALK,
	RQ_SPRITES,
	RQ_UBER_PLAYER_CAR_REC,
	RQ_POPPY_CAR_REC,
	RQ_BODYGUARD_1_REC,
	RQ_BODYGUARD_2_REC,
	RQ_UBER_CAR_RECORDING,
	RQ_POPPY_RECORDING,
	RQ_SET_PEICE_RECORDING,
	RQ_CHASE_SOUNDS,
	RQ_TEXT
ENDENUM

ENUM SCRIPTED_CINE_CAM
	SCC_START,
	SCC_CONSTRUCTION_FOOR_CAM,
	SCC_PAVEMENT_CAM,
	SCC_CONSTRUCTION_CAM,
	SCC_HOTEL_CAM,
	SCC_CAM_PAN
ENDENUM

ENUM CAR_ENUMS
	POPPY_CAR = 0,
	SHAGGER_CAR = 1,
	BODYGUARD_CAR = 2
ENDENUM

ENUM CHASE_CONVO_STAGE
	CC_STAGE_1,
	CC_STAGE_2,
	CC_STAGE_3,
	CC_NONE
ENDENUM

ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

ENUM FAILED_REASONS
	FR_POPPY_SCARED,
	FR_BUDDY_DEAD,
	FR_BUDDY_HARMED,
	FR_BUDDY_THREAT,
	FR_BADCAM,
	FR_ATTENTION,
	FR_SPOTTED,
	FR_KILLED_INOCC,
	FR_KILLED_POP,
	FR_HARMED_POP,
	FR_KILLED_SHAG,
	FR_HARMED_SHAG,
	FR_LEFT_AREA,
	FR_LEFT_BUDDY,
	FR_CAR_DEAD,
	FR_WANTED,
	FR_TOOK_TOO_LONG,
	FR_BEV_LEFT_YOU,
	FR_FOOTAGE_RUINED,
	FR_NONE
ENDENUM

ENUM OUTRO_CUTSCENE_STAGES
	OCS_CAMERA_CUT_1,
	OCS_CAMERA_PAN,
	OCS_CAMERA_PANING,
	OCS_FIN
ENDENUM

ENUM CAR_CHASE_STAGE
	CCS_PULL_OUT_CAR_1 = 5,
	CCS_PULL_OUT_CAR_2 = 6,
	CCS_CRASH_CAR_1 = 8,
	CCS_CHASE_FIN = -1
ENDENUM

ENUM CHASE_SOUND_ORIGIN
	CSO_POPPY,
	CSO_SHAGGER,
	CSO_BODYGUARD,
	CSO_PLAYER
ENDENUM

ENUM STAFF_STATES
	S_NULL,
	S_WAIT_ACTIVE,
	S_WALK_TALK,
	S_STAND_TALK,
	S_AMBIENT_BEHAVIOUR,
	S_SEEN_GUN,
	S_THREATENED,
	S_THREATENED_WITH_GUN,
	S_BUMPED,
	S_RUN
ENDENUM

ENUM MAID_AMBIENT_STATE
	M_AMB_WALKING_TO_POINT,
	M_AMB_WASHING_WINDOW,
	M_AMB_SMOKEING
ENDENUM

ENUM BOY_AMBIENT_STATE
	B_AMB_WALKING_TO_POINT,
	B_AMB_SWEEPING
ENDENUM

ENUM CAMERA_TUT_STAGE
	CTS_INIT_EXPLAIN,
	CTS_EXPLAINING,
	CTS_WAIT_DONE
ENDENUM

ENUM CUTSCENE_STAGE
	eCutInit,
	eCutUpdate,
	eCutCleanup
ENDENUM	

CUTSCENE_STAGE eCutsceneState = eCutInit

//*************************************************************************************************************************************************
//													:CONSTANTS:
//*************************************************************************************************************************************************

// Bev Waypoint Constants
CONST_INT MAX_BEV_WAYPOINTS					8
CONST_INT ROUTE_ROAD_START					0
CONST_INT ROUTE_STAIRS_1_BOTTOM				1
CONST_INT ROUTE_STAIRS_1_MIDDLE				2
CONST_INT ROUTE_STAIRS_1_TOP				3
CONST_INT ROUTE_ARCH_1						4
CONST_INT ROUTE_STAIRS_2_BOTTOM				5
CONST_INT ROUTE_POOL_STAIRS					6
CONST_INT ROUTE_POPPY						7

//General setup constants
CONST_INT MAX_CARS							3
CONST_INT MAX_STAFF  						2
CONST_INT MAX_MAID_WORK_POINTS				5
CONST_INT MAX_BOY_WORK_POINTS				3

#IF IS_DEBUG_BUILD
	CONST_INT MAX_CHASE_POINTS 					34
	VECTOR vChaseRoute[MAX_CHASE_POINTS]
	INT iCurrentChasePos = 0						//only used when recording a new route
	VECTOR vRouteCheckSize = <<9,9,10>> //used when recording uba chase, blips are created along the route and disapear when the player ped enters
#ENDIF

CONST_INT MAX_BEV_RECORDINGS 				7

//camera control and filming
CONST_INT iFramesBeforeFilmingFail			60
CONST_INT DEAD_ZONE_CAM						0
CONST_INT NUM_ONSCREEN	 					-3
CONST_INT NUM_ONSCREEN_CENTER 				3//2//3
CONST_INT NUM_ONSCREEN_ZOOM_CENTER 			4
CONST_INT NUM_OFF_SCREEN 					5
CONST_INT MAX_FILM_FAILS_CHASE				6
CONST_INT MAX_FILM_FAILS 					7//4
CONST_FLOAT CAM_TURN_RATE 					0.0117 //0.0112
CONST_FLOAT CAM_MOUSE_TURN_RATE 			20.0
CONST_FLOAT CAM_FOV_RATE 					5.0
CONST_FLOAT ONSCREEN_CENTER 				2.0
CONST_FLOAT ONSCREEN_JUST 					1.5
CONST_FLOAT OFF_SCREEN 						1.0

//Uba Recording
CONST_FLOAT POPPY_PLAYBACK_SPEED 			0.9

//Prop Constant
CONST_INT MAX_SEX_SCENE_PROPS 				4

//Dialogue constants
CONST_INT MAX_BAD_FILM_LINES1 				4
CONST_INT MAX_ESCAPE_CHATTER_LINES 			3
CONST_INT MAX_CHASE_CONVO_LINES2			17

CONST_FLOAT VIEW_ANGLE						78.0//90.0

//Sounds
CONST_INT MAX_SOUNDS	2

CONST_INT MAX_RUNNING_TIME_BEFORE_ATTENTION 78 //Max time the player can run around by the hotel staff before they fail

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 12
	BOOL bPrintWidgetValuesToDebug = FALSE
#ENDIF
//*************************************************************************************************************************************************
//													:VARIABLES:
//*************************************************************************************************************************************************

#IF NOT IS_JAPANESE_BUILD
	INT poppy_shagging_scene
	INT iBadFilmNoBev = 0
#ENDIF
#IF IS_JAPANESE_BUILD 
	INT iSeqJapaneseVersionConvo
	INT iTimerJapaneseVersionNoticePlayer
	BOOL bConvoJapaneseVersion
#ENDIF
INT poppy_shagging_scene2
INT poppy_shagging_scene_breakout
INT iSeqBadCamMissionFail
INT	iTimerBadCamMissionFail
INT iTimerPoppyNoticePlayer
INT iTimerPlayerSentToCover
INT iTimerMaidUsingPhone
INT iTimerDrivebyReset
INT iTimerPlayerInEscapeCar
INT iTimerBadFilmConvo
INT iTimerCheckPlayerInRightCar
INT iTimerBadCam
INT iTimerStartStage
INT iTimerEscapeChatter
INT iTimerBevTut
INT iTimerBevInCar
INT iTimerMaidWalkingTalking
INT iTimerCutsceneFinished
INT iMaxRecogPercent
INT iZoomFunc
INT iTimerCamSwitch
INT iSeqTraffic
INT iReplayStage
INT iDoFixForStream = 0
INT iCoverSeq = 0
INT iFramesPlayerInCoverSpot = 0
INT iTimerGetInCarStage
INT iTimerBevSetOffWalking
INT iBadFilming = 0
INT iTimerBevSentToCover
INT iTimerStartedFilming
INT iPoppyAnimSeq
INT iCounterBevWaiting
INT iSeqBevRagdoll
INT iTimerBevRagdoll
INT iSeqMusic = 0
INT iCounterHelp
INT iTimerHelp
INT iClosestWaypointToPlayer
INT iSeqMaid = 0
INT iSeqPlayerHoldCam = 0
INT iFilmingStats
INT iTimerBevCoverTask
#IF NOT IS_JAPANESE_BUILD
	INT iSeqPoppySubs
#ENDIF
INT iTimerBevIdleLines
INT iBevIdleLines	

INT iShockBG
INT iShockPoppy
INT iShockBev
INT iShockJustin
INT iShockPoppyCrash
INT iFailsafe

BOOL bPlayerSentToCover
BOOL bPoppyCombatTask
BOOL bShaggerCombatTask
BOOL bPoppyCreated = FALSE	
BOOL bOkForBevToTurn = FALSE	
BOOL bConvoDontInterfere = FALSE	
BOOL bConvoNowDude = FALSE	
BOOL bLegIt = FALSE	
BOOL bComeOn = FALSE	
BOOL bDoingEndCut = FALSE
BOOL bBevStealth = FALSE
BOOL bBevSentToCover = FALSE
BOOL bPlayerGettingInCar = FALSE
BOOL bPassCamConvo = FALSE
BOOL bDialogueEndCut = FALSE	
BOOL bSmallCrash 
BOOL bPoppySmallCrash
BOOL bReachedChase = FALSE
BOOL bSetAudioPos = FALSE
BOOL bPoppyCrashDialogue = FALSE	
BOOL bDoneSyncSceneBreakout = FALSE	
BOOL bOpenBoot = FALSE	
//BOOL bPassCar = FALSE
BOOL bPoppyGettingInCar = FALSE
BOOL bBevDrivingOff = FALSE
BOOL bPutCamInFranksHand = FALSE
BOOL bBevHitHisHead = FALSE
BOOL bHighRevSound = FALSE
BOOL bGoToBev = FALSE
BOOL bSetCarVisible = FALSE
BOOL bCSSetExitCam
BOOL bCSSetExitBev
BOOL bCSSetExitCar
BOOL bCSSetExitFranklin
BOOL bClearCoverTasks = FALSE
BOOL bCSSkipped = FALSE
BOOL bMakeCloudy = FALSE
BOOL bLockBootOpen = FALSE	
BOOL bDoDelayedfade = FALSE
//BOOL bReplayUsed
BOOL bJumpedInPool
BOOL bDeckedGuard
BOOL bStaffThreatened
BOOL bPoppyBigCrash
BOOL                        bDStraight
BOOL						bDTrunk
BOOL						bDWorse
BOOL						bDWhoa
BOOL						bDMFer
BOOL						bDRight
BOOL						bDLeft
BOOL						bDFuckU
BOOL						bDMove
BOOL						bDCrazy
BOOL						bDTraffic
BOOL						bDSteady
BOOL						bDClose
BOOL						bDJesus
BOOL						bDConst

BOOL bSkipToEndOfChase
BOOL bDoneFirstPoppyCarOffset
BOOL bSwapCamPos
BOOL bOkToPassCam
BOOL bCamZooming
BOOL bRotRight
BOOL bMovePlayerCar
BOOL bBentleyGotBevStuck
FLOAT fCamFov
FLOAT fTimePosInRec
//FLOAT fCamZoomStrength
FLOAT fZOffest
FLOAT fXOffest
FLOAT fXOffestPoppy
FLOAT fXOffestBev
#IF NOT IS_JAPANESE_BUILD
	FLOAT fSSPhase
	FLOAT fGruntVol
	BOOL bDone1stPersonFlash
#ENDIF
FLOAT fZoomAlpha 
//FLOAT fTempCamZ
//FLOAT fTempCamZAlpha
FLOAT fLodScale = 1.0

STREAMVOL_ID StreamVol
BOOL bStreamvolCreated = FALSE

//FLOAT fCrashShockRange

OBJECT_INDEX objShagTable
OBJECT_INDEX objShagParasol
OBJECT_INDEX ObjPhone
OBJECT_INDEX objCam
OBJECT_INDEX objPipe1

PED_INDEX pedHotel[6]
PED_INDEX pedBuilder[5]
BOOL bHotelPedsSpawned
BOOL bHotelModelsRequested

BOOL bDoingFocusPush
INT iTimerFocusPush

VEHICLE_INDEX viCrashFuto
VEHICLE_INDEX vehTraffic[15]
PED_INDEX pedRandomDriver[15]
PED_INDEX pedTemp
PED_INDEX pedTempJustin
PED_INDEX pedTempBev

SCENARIO_BLOCKING_INDEX sbiPoppy
SCENARIO_BLOCKING_INDEX sbiClipboardGuys
SCENARIO_BLOCKING_INDEX mScenarioBlocker
SCENARIO_BLOCKING_INDEX sbiBuilders

INT iZoomSound = GET_SOUND_ID()
INT iSoundPoppyRevs = GET_SOUND_ID()

#IF NOT IS_JAPANESE_BUILD	
	INT iSexSound = GET_SOUND_ID()
	INT iGruntSounds = GET_SOUND_ID()
#ENDIF

BOOL bDumpedBank

AI_BLIP_STRUCT		bsHotelPed1
AI_BLIP_STRUCT		bsHotelPed2
AI_BLIP_STRUCT		bsHotelPed3
AI_BLIP_STRUCT		bsHotelPed4
AI_BLIP_STRUCT		bsHotelPed5
AI_BLIP_STRUCT		bsPoppy
AI_BLIP_STRUCT		bsJustin
AI_BLIP_STRUCT		bsSecurity
AI_BLIP_STRUCT		bsStaff1
AI_BLIP_STRUCT		bsStaff2

TIMEOFDAY sTimeOfDay
INT iHour

VECTOR vPlayerStartPos = <<-74.1683, 299.9258, 105.4432>>//<< -71.8997, 299.1354, 105.3285 >> 
VECTOR vAlleyPos = << -74, 300, 102>> 
VECTOR vPlayerCarStartPos =   << -78.9112, 299.3701, 105.4327 >>
VECTOR vBodyGuardCarPos[MAX_CARS]
VECTOR vSafeVec = <<0,0,0>> //used mostly in setup functions that require a vector in the args but dont neccessarily need one 
VECTOR vCameraOffset = <<0,-2.1,0.6>> //<<0,-1.9,0.6>>
VECTOR vSexScene = << -29.4167, 302.7094, 111.6957 >>	//position of the sex scene
VECTOR vAlleyPoint1 = << -75.1476, 359.3089, 111.4346 >>
VECTOR vAlleyPoint2 = << 41.4266, 312.7768, 140.2310 >> 
VECTOR vBevRoute[MAX_BEV_WAYPOINTS]

FLOAT fPlayerDir = 235.4094 //starting player direction
FLOAT fPlayerCarDir = 249.8696
FLOAT fBodyGuardCarDir[MAX_CARS]

COVERPOINT_INDEX m_ciPap2 //For B*1819613

//VECTOR vPosRespot
//FLOAT fHeadingRespot

STRUCT A_PED
	PED_INDEX 				piPed
	VECTOR 					vPos
	FLOAT 					fDir
ENDSTRUCT

/// PURPOSE: Stores info for spawining a prop
STRUCT A_PROP
	OBJECT_INDEX  oiProp
	VECTOR vPos
	VECTOR vRot
	MODEL_NAMES mnModle	
ENDSTRUCT

/// PURPOSE: The colour for the face tag
STRUCT BOXTAG_COLOUR
	INT R  = 0
	INT G  = 0
	INT B  = 255
ENDSTRUCT

/// PURPOSE: Holds chase sound relevent vars
STRUCT CHASE_SOUND
	INT 				iExecuteTime
	BOOL 				bPlayed
	STRING 				sSoundName
	CHASE_SOUND_ORIGIN 	eOrigin
ENDSTRUCT

STRING sSoundSet = "PAPARAZZO_02_SOUNDSETS"

STRUCT MAID_WORK_POINT
	VECTOR vGOTO
	FLOAT fFaceing
	MAID_AMBIENT_STATE eStateToEnter
ENDSTRUCT

//Struct memeber vars
BOXTAG_COLOUR mBoxCol
A_PROP mSexSceneProps[MAX_SEX_SCENE_PROPS]
A_PED mBuddy
A_PED mPoppy
A_PED mShagger
A_PED mBodyGuard
A_PED mHotelStaff[MAX_STAFF]
CHASE_SOUND mFinalChaseSound[MAX_SOUNDS] //struct for the pipes

MAID_WORK_POINT mMaidWorkPoint[MAX_MAID_WORK_POINTS]
VECTOR vBoyWorkPoint[MAX_BOY_WORK_POINTS]

INT iMaidActiveWorkPoint = GET_RANDOM_INT_IN_RANGE(0, MAX_MAID_WORK_POINTS)
INT iBoyActiveWorkPoint = GET_RANDOM_INT_IN_RANGE(0, MAX_BOY_WORK_POINTS)

structPedsForConversation s_conversation_peds

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

MODEL_NAMES mnPlayerCar = FQ2
MODEL_NAMES mnBodyGuardChaseCar[MAX_CARS]

#IF NOT IS_JAPANESE_BUILD
	MODEL_NAMES mnShagger = U_M_Y_Justin //A_M_Y_BusiCas_01  //
#ENDIF

#IF IS_JAPANESE_BUILD
	MODEL_NAMES mnShagger = A_M_Y_BusiCas_01  //
#ENDIF

MODEL_NAMES mnPoppy = U_F_Y_PoppyMich
MODEL_NAMES mnBodyGuards = S_M_M_BOUNCER_01//S_M_M_SECURITY_01// S_M_M_HighSec_01
MODEL_NAMES mnHotelStaff[MAX_STAFF]

BLIP_INDEX biGOTO
BLIP_INDEX biBuddyBlip

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
#ENDIF

SEQUENCE_INDEX SeqMain

CAMERA_INDEX camMain
CAMERA_INDEX camMainCS
CAMERA_INDEX camInterp

CAMERA_INDEX camFrankFilm
CAMERA_INDEX camFrankFilm2

BOOL bCamInterp = FALSE

OBJECT_INDEX oiBroom
OBJECT_INDEX oiWindowRag

//****************************************************//
//				Camera Variables					  //
//***************************************************//
//Used for controling the camera
FLOAT fCamHeadingMod = 0  //0
FLOAT fCamPitchMod = 0    //0
BOOL bUseLockedCam = FALSE
BOOL bUseFace = TRUE

//Camera Crosshair
//FLOAT fCross1HairHeight = 0.0020
//FLOAT fCross1HairWidth = 0.0300
//FLOAT fCross2HairHeight = 0.0440
//FLOAT fCross2HairWidth = 0.0020

FLOAT fCursorHeight = 0.013
FLOAT fCursorWidth = 0.013
FLOAT fCursorX1 = 0.4600
FLOAT fCursorY1 = 0.4570
FLOAT fCursorX2 = 0.5400
FLOAT fCursorY2 = 0.4570
FLOAT fCursorX3 = 0.4600
FLOAT fCursorY3 = 0.5480
FLOAT fCursorX4 = 0.5400
FLOAT fCursorY4 = 0.5480

INT iScaleMulti = 1500
FLOAT fMaxBox = 1.0
FLOAT fMinBox = 0.4
FLOAT fFaceOffset =  0
FLOAT fFaceOffsetz = 0

#IF NOT IS_JAPANESE_BUILD
	FLOAT fBreakoutPhase = 0.877000//0.888042
#ENDIF

REL_GROUP_HASH relPoppyGroup

//*********************************************/ MISSION FLOW VARS /**************************************************//
//MISSION ENUMS
MISSION_STAGE eMissionStage = MS_INIT						//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT								//Internal state tracking for mission stages
CHASE_CONVO_STAGE eChaseConvo = CC_STAGE_1					//What stage the chase dialogue is up to 
SCRIPTED_CINE_CAM eScriptCamState = SCC_START				//State for the cinematic camera cuts 
CAMERA_TUT_STAGE eCamTutStage = CTS_INIT_EXPLAIN			//Stage for the camera tutorial

STAFF_STATES eStaffState[MAX_STAFF]

MAID_AMBIENT_STATE eMaidAMBState = M_AMB_WALKING_TO_POINT	//Maid state tracker
BOY_AMBIENT_STATE eBoyAMBState = B_AMB_WALKING_TO_POINT		//Busboy state tracker

STRING sFailReason = NULL
MODEL_NAMES mnFailCarForPlayer = SADLER						//car spawned for play when they fail --- curent replay system deletes this 

BOOL bObjectiveShown = FALSE								//flag for if the objective has been shown 
BOOL bJumpSkip = FALSE										//flag for if current MISSION state should clean up and move to the next state
BOOL bDebugSkipping
BOOL bExpireBevSpeech1 = FALSE
BOOL bExpireBevSpeech2 = FALSE
BOOL bExpireStaffSpeech = FALSE				//Staff conversation tracking flag
BOOL bExpireSpottedSpeech 	= FALSE			//If the player is spotted being suspisions
BOOL bEscapeConvExpired 	= FALSE			//Conversation that happens while the player is running to the car
BOOL bEscapeConvExpired1 	= FALSE			//Second conversation that happens while the player is running to the car
BOOL bExpireBudThreatSpeech = FALSE			//beverly running away
BOOL bExpireInactive = TRUE					//This conv is expired to start and is reactivated if the player has been away from bev for a time
BOOL bWaving = TRUE							//Track if Beverly is waving the player over
BOOL bCloseTooWarning = FALSE			//Player gets too close to poppy
BOOL bExpireWarning = FALSE				//Expires a state sensitive warning to the player 
BOOL bExpireGetInCarText = FALSE		//God text telling the player to get in bev's car
BOOL bExpireGetInCarFail = FALSE		//Bev enters the car shouting at the player
BOOL bCreatedProps = FALSE
BOOL bCanCreateProps = FALSE
BOOL bCanCreatePoppy = FALSE
BOOL bPictureTaken = FALSE					//Has Enough film has been taken 
BOOL bLoadingFinIntro = FALSE				//If loading has finished under the intro
BOOL bForceTutFin = FALSE					//If the player doesn't keep the camera on poppy during the cam tut we force pass it so the mission can continue
BOOL bWrongCarTimerActivate = FALSE			//has the timer started - used when the player enters a car that isnt the escape car
BOOL bIsBuddyInCarBeforePlayer = FALSE		//has bev entered the car before bev
BOOL bSpotted = FALSE				//has the player been spotted
BOOL bBudThreat = FALSE				//has the player threatened bev
BOOL bPlayerFailed = FALSE			//has the player failed
BOOL bStartScriptCineCam = FALSE	//have we started the cam
BOOL bStartChaseCam = FALSE			//Should we start the camera for the chase
BOOL bMiniCutDone = FALSE			//Tracker bool to see if the drive off cutscene is finished
BOOL bDisableFail = FALSE

INT iRunningTime = 0 
INT iCount										//Used for loops - should really get rid of this and switch to a local var
INT iMissionState = 0							//Used in the jump stage function
INT iPlaybackProgress = 0						//Used to play back the UBER recording
INT iRedBlink = 0								//Used to turn the red REC on and off
INT iNumFilmFails = 0							//track the number of times the play has had a bad filming line
INT iFilmTime = 0								//Track how long the player has correctly filmed poppy
INT iInactivityTimer = 0						//Time how long the player has spent away from beverly
INT iWaveInactivityTimer = 0					//Used to trigger beverly waving to the player
INT iFailInactivityTimer = 0					//When this is active the player fails after X amount of time
INT iGetInCarTimer = 0 							//Times the player when he should be in the escape car but isnt
INT iBevRoute = ROUTE_ROAD_START				//Tracker for beverly's current waypoint 
INT iBevShoutLines = 0
INT iCounterNotOnScreen = 0

FLOAT fPlaybackSpeed = POPPY_PLAYBACK_SPEED		//Speed of the uba recording playback
BOOL bDoingDriveBy = FALSE						//Track whether or not poppy is performing a drive by

//TEXT_LABEL sBeverlyPedRecording = "pap2_Bev_route7" //run round the path
//TEXT_LABEL sBeverlyPedRecording = "pap2_Bev_route8" //climb over other wall (blocked by fence)
//TEXT_LABEL sBeverlyPedRecording = "pap2_Bev_routeB" //climb over wall 2
TEXT_LABEL sBeverlyPedRecording = "pap2_Bev_routeC"   //Run past poppy, go right and jump off wall
TEXT_LABEL sBeverlyPedRecording2 = "pap2_Bev_route9"

STRING sTextBlock = "PAP2AUD"						//Variable for storing the mission Dialogue text block
//STRING sBadFilmLines[MAX_BAD_FILM_LINES1]			//Bad film lines in the on foot section get more irrate as they go on so we iterate through them tracking how many times these are triggered
//STRING sChaseConvoLines[MAX_CHASE_CONVO_LINES2]		//Chase lines happen at specific times so we play them in order

STRING sWeaponMoveClipset = "random@escape_paparazzi@standing@"//"move_ped_wpn_jerrycan_generic"
STRING sWeaponMoveClipsetFrank = "random@escape_paparazzi@standing@"//"move_ped_wpn_jerrycan_franklin"

INT iCurrentLine = 0
BOOL bLineSet = FALSE

#IF IS_DEBUG_BUILD
	INT i_debug_jump_stage
	INT iRecordingProgress = 0								//Used for the UBER recording
#ENDIF

BOOL bSkipPassMission = FALSE

VEHICLE_INDEX viPassCar							//Car to spawn when the player has passed 
VEHICLE_INDEX viChaseCars[MAX_CARS]				//Poppy and bodyguards cars
VEHICLE_INDEX viPlayerCar						//beverly's car

BOOL bDialogueGiveMe
BOOL bDialogueIllSue
BOOL bDialogueVirgin
BOOL bDialogueAnimals
BOOL bDialogueLosers
BOOL bDialogueYoga

INT iTimerPoppyChaseLine

/// PURPOSE:
///    Resets all mission variables
PROC RESET_MISSION_FLOW_VARS()
	bExpireBevSpeech1 = FALSE
	bExpireBevSpeech2 = FALSE
	bExpireStaffSpeech = FALSE
	bEscapeConvExpired = FALSE
	bEscapeConvExpired1 = FALSE
	bLineSet = FALSE
	bDialogueGiveMe = FALSE
	bDialogueIllSue = FALSE
	bDialogueVirgin = FALSE
	bDialogueAnimals = FALSE
	bDialogueLosers = FALSE
	bDialogueYoga = FALSE
	bPassCamConvo = FALSE
	bDialogueEndCut = FALSE	
	bPlayerGettingInCar = FALSE
	bReachedChase = FALSE
	bSmallCrash = FALSE
	bPoppySmallCrash = FALSE
	bPoppyCrashDialogue = FALSE	
	bDoneSyncSceneBreakout = FALSE	
	bOpenBoot = FALSE	
	bBevDrivingOff = FALSE
	bBevHitHisHead = FALSE
	bStreamvolCreated = FALSE
	bHighRevSound = FALSE
	bGoToBev = FALSE
	bSetCarVisible = FALSE
	//bPutCamInFranksHand = FALSE
	bObjectiveShown = FALSE
	bCloseTooWarning = FALSE
	bExpireWarning = FALSE
	bExpireGetInCarText = FALSE
	bExpireGetInCarFail = FALSE
	bConvoNowDude = FALSE
	bConvoDontInterfere = FALSE
	bLegIt = FALSE	
	bComeOn = FALSE	
	bCreatedProps = FALSE
	bCanCreateProps = FALSE
	bCanCreatePoppy = FALSE
	bPoppyCreated = FALSE	
	bPictureTaken = FALSE
	bLoadingFinIntro = FALSE
	bForceTutFin = FALSE
	bWrongCarTimerActivate = FALSE
	bIsBuddyInCarBeforePlayer = FALSE	
	bStartScriptCineCam = FALSE
	bStartChaseCam = FALSE
	bMiniCutDone = FALSE
	iRunningTime = 0 
	iPlaybackProgress = 0
	iInactivityTimer = 0
	iFilmTime = 0
	iCurrentLine = 0
	iSeqMaid = 0
	iCoverSeq = 0
	iBevRoute = ROUTE_ROAD_START
	iFramesPlayerInCoverSpot = 0
	iBadFilming = 0
	iBevShoutLines = 0
	iSeqMusic = 0
	iSeqPlayerHoldCam = 0
	bPlayerSentToCover = FALSE
	bHotelModelsRequested = FALSE
	bHotelPedsSpawned = FALSE
	bBevSentToCover = FALSE
	bClearCoverTasks = FALSE
	bCSSkipped = FALSE
	bOkToPassCam = FALSE
	#IF NOT IS_JAPANESE_BUILD
		iBadFilmNoBev = 0
	#ENDIF
	eChaseConvo = CC_STAGE_1
 	eScriptCamState = SCC_START	
	eCamTutStage = CTS_INIT_EXPLAIN

ENDPROC

/// PURPOSE:
///    Initialise uba traffic data
PROC UBER_TRAFFIC_DATA()

TrafficCarPos[0] = <<25.6387, 266.4447, 109.3633>>  //108.8633
TrafficCarQuatX[0] = -0.0002
TrafficCarQuatY[0] = -0.0004
TrafficCarQuatZ[0] = 0.6374
TrafficCarQuatW[0] = 0.7705
TrafficCarRecording[0] = 2
TrafficCarStartime[0] = 3498.0000
TrafficCarModel[0] = Felon //Blista //CARBONIZZARE //DUMMY_MODEL_FOR_SCRIPT //TAXI //CARBONIZZARE
//TrafficCarID[0]

TrafficCarPos[1] = <<53.3845, 259.2507, 109.0939>>
TrafficCarQuatX[1] = 0.0028
TrafficCarQuatY[1] = 0.0014
TrafficCarQuatZ[1] = 0.5719
TrafficCarQuatW[1] = 0.8203
TrafficCarRecording[1] = 4
TrafficCarStartime[1] = 5346.0000
TrafficCarModel[1] = DUMMY_MODEL_FOR_SCRIPT //futo

TrafficCarPos[2] = <<54.6990, 249.2125, 109.5105>>
TrafficCarQuatX[2] = -0.0053
TrafficCarQuatY[2] = -0.0038
TrafficCarQuatZ[2] = 0.5759
TrafficCarQuatW[2] = 0.8175
TrafficCarRecording[2] = 5
TrafficCarStartime[2] = 5478.0000
TrafficCarModel[2] = rocoto

TrafficCarPos[3] = <<61.6959, 252.0383, 109.0729>>
TrafficCarQuatX[3] = 0.0073
TrafficCarQuatY[3] = 0.0053
TrafficCarQuatZ[3] = 0.5770
TrafficCarQuatW[3] = 0.8167
TrafficCarRecording[3] = 6
TrafficCarStartime[3] = 5742.0000
TrafficCarModel[3] = infernus

TrafficCarPos[4] = <<69.9301, 249.1113, 108.8189>>
TrafficCarQuatX[4] = 0.0138
TrafficCarQuatY[4] = 0.0098
TrafficCarQuatZ[4] = 0.5760
TrafficCarQuatW[4] = 0.8173
TrafficCarRecording[4] = 7
TrafficCarStartime[4] = 6336.0000
TrafficCarModel[4] = DUMMY_MODEL_FOR_SCRIPT //taxi !!!

TrafficCarPos[5] = <<69.6693, 243.7877, 108.6399>>
TrafficCarQuatX[5] = 0.0136
TrafficCarQuatY[5] = 0.0100
TrafficCarQuatZ[5] = 0.5746
TrafficCarQuatW[5] = 0.8183
TrafficCarRecording[5] = 8
TrafficCarStartime[5] = 6402.0000
TrafficCarModel[5] = TAXI //CARBONIZZARE

TrafficCarPos[6] = <<237.0279, 346.2748, 105.4462>>
TrafficCarQuatX[6] = 0.0020
TrafficCarQuatY[6] = -0.0029
TrafficCarQuatZ[6] = 0.8184
TrafficCarQuatW[6] = -0.5746
TrafficCarRecording[6] = 9
TrafficCarStartime[6] = 16434.0000
TrafficCarModel[6] = rocoto

TrafficCarPos[7] = <<347.3820, 320.5341, 103.8621>>
TrafficCarQuatX[7] = 0.0011
TrafficCarQuatY[7] = 0.0012
TrafficCarQuatZ[7] = 0.6188
TrafficCarQuatW[7] = 0.7855
TrafficCarRecording[7] = 10
TrafficCarStartime[7] = 20328.0000
TrafficCarModel[7] = bus

TrafficCarPos[8] = <<424.6772, 305.5897, 102.4288>>
TrafficCarQuatX[8] = -0.0055
TrafficCarQuatY[8] = 0.0230
TrafficCarQuatZ[8] = 0.4000
TrafficCarQuatW[8] = 0.9162
TrafficCarRecording[8] = 11
TrafficCarStartime[8] = 23958.0000
TrafficCarModel[8] = penumbra

TrafficCarPos[9] = <<423.8190, 290.4329, 102.8873>>
TrafficCarQuatX[9] = -0.0257
TrafficCarQuatY[9] = -0.0017
TrafficCarQuatZ[9] = 0.8050
TrafficCarQuatW[9] = -0.5927
TrafficCarRecording[9] = 12
TrafficCarStartime[9] = 24024.0000
TrafficCarModel[9] = rocoto

TrafficCarPos[10] = <<418.2715, 279.2705, 102.5196>>
TrafficCarQuatX[10] = 0.0001
TrafficCarQuatY[10] = 0.0000
TrafficCarQuatZ[10] = -0.1905
TrafficCarQuatW[10] = 0.9817
TrafficCarRecording[10] = 13
TrafficCarStartime[10] = 24486.0000
TrafficCarModel[10] = futo //taxi //pcj
TrafficCarID[10] = viCrashFuto

TrafficCarPos[11] = <<436.6275, 293.4052, 102.9222>>
TrafficCarQuatX[11] = 0.0057
TrafficCarQuatY[11] = 0.0037
TrafficCarQuatZ[11] = 0.5851
TrafficCarQuatW[11] = 0.8109
TrafficCarRecording[11] = 14
TrafficCarStartime[11] = 24552.0000
TrafficCarModel[11] = rocoto

TrafficCarPos[12] = <<568.5327, 245.2966, 102.6008>>
TrafficCarQuatX[12] = -0.0038
TrafficCarQuatY[12] = 0.0103
TrafficCarQuatZ[12] = 0.6916
TrafficCarQuatW[12] = 0.7222
TrafficCarRecording[12] = 15
TrafficCarStartime[12] = 30294.0000
TrafficCarModel[12] = futo

TrafficCarPos[13] = <<563.3254, 226.8358, 102.5454>>
TrafficCarQuatX[13] = 0.0315
TrafficCarQuatY[13] = -0.0056
TrafficCarQuatZ[13] = -0.1653
TrafficCarQuatW[13] = 0.9857
TrafficCarRecording[13] = 16
TrafficCarStartime[13] = 30360.0000
TrafficCarModel[13] = taxi

TrafficCarPos[14] = <<585.7031, 250.9805, 102.9546>>
TrafficCarQuatX[14] = 0.0005
TrafficCarQuatY[14] = -0.0193
TrafficCarQuatZ[14] = 0.3598
TrafficCarQuatW[14] = 0.9328
TrafficCarRecording[14] = 17
TrafficCarStartime[14] = 30888.0000
TrafficCarModel[14] = rocoto

TrafficCarPos[15] = <<641.6404, 224.2890, 97.9900>>
TrafficCarQuatX[15] = 0.0522
TrafficCarQuatY[15] = 0.0380
TrafficCarQuatZ[15] = 0.6146
TrafficCarQuatW[15] = 0.7862
TrafficCarRecording[15] = 18
TrafficCarStartime[15] = 33264.0000
TrafficCarModel[15] = penumbra

TrafficCarPos[16] = <<651.6318, 221.3143, 96.7827>>
TrafficCarQuatX[16] = 0.0519
TrafficCarQuatY[16] = 0.0379
TrafficCarQuatZ[16] = 0.5888
TrafficCarQuatW[16] = 0.8057
TrafficCarRecording[16] = 19
TrafficCarStartime[16] = 33726.0000
TrafficCarModel[16] = taxi

TrafficCarPos[17] = <<686.7967, 203.5349, 92.1762>>
TrafficCarQuatX[17] = 0.0316
TrafficCarQuatY[17] = -0.0462
TrafficCarQuatZ[17] = 0.8192
TrafficCarQuatW[17] = -0.5708
TrafficCarRecording[17] = 20
TrafficCarStartime[17] = 35244.0000
TrafficCarModel[17] = bus

TrafficCarPos[18] = <<558.3951, 44.7597, 94.2669>>
TrafficCarQuatX[18] = -0.0174
TrafficCarQuatY[18] = -0.0487
TrafficCarQuatZ[18] = 0.7204
TrafficCarQuatW[18] = -0.6916
TrafficCarRecording[18] = 110
TrafficCarStartime[18] = 44000.0000
TrafficCarModel[18] = bus

TrafficCarPos[19] = <<648.9051, 39.3727, 86.1631>>
TrafficCarQuatX[19] = 0.0453
TrafficCarQuatY[19] = 0.0236
TrafficCarQuatZ[19] = 0.5662
TrafficCarQuatW[19] = 0.8227
TrafficCarRecording[19] = 111
TrafficCarStartime[19] = 44000.0000
TrafficCarModel[19] = DUMMY_MODEL_FOR_SCRIPT

TrafficCarPos[20] = <<657.6167, 23.9452, 84.8269>>
TrafficCarQuatX[20] = 0.0184
TrafficCarQuatY[20] = -0.0273
TrafficCarQuatZ[20] = 0.8442
TrafficCarQuatW[20] = -0.5349
TrafficCarRecording[20] = 21
TrafficCarStartime[20] = 46596.0000
TrafficCarModel[20] = stratum

TrafficCarPos[21] = <<702.3966, 16.4575, 83.7715>>
TrafficCarQuatX[21] = 0.0095
TrafficCarQuatY[21] = 0.0053
TrafficCarQuatZ[21] = 0.4853
TrafficCarQuatW[21] = 0.8743
TrafficCarRecording[21] = 22
TrafficCarStartime[21] = 50292.0000
TrafficCarModel[21] = DUMMY_MODEL_FOR_SCRIPT //taxi

TrafficCarPos[22] = <<685.0597, -109.3147, 74.1404>>
TrafficCarQuatX[22] = -0.0189
TrafficCarQuatY[22] = 0.0245
TrafficCarQuatZ[22] = 0.4845
TrafficCarQuatW[22] = 0.8742
TrafficCarRecording[22] = 114
TrafficCarStartime[22] = 50610.3008
TrafficCarModel[22] = rocoto

TrafficCarPos[23] = <<702.9434, 9.8203, 83.7822>>
TrafficCarQuatX[23] = 0.0098
TrafficCarQuatY[23] = 0.0046
TrafficCarQuatZ[23] = 0.4938
TrafficCarQuatW[23] = 0.8695
TrafficCarRecording[23] = 23
TrafficCarStartime[23] = 51612.0000
TrafficCarModel[23] = DUMMY_MODEL_FOR_SCRIPT //taxi

TrafficCarPos[24] = <<611.2632, -68.2602, 72.7379>>  //
TrafficCarQuatX[24] = 0.0478
TrafficCarQuatY[24] = -0.0234
TrafficCarQuatZ[24] = -0.5413
TrafficCarQuatW[24] = 0.8391
TrafficCarRecording[24] = 24
TrafficCarStartime[24] = 55506.0000
TrafficCarModel[24] = Felon //Blista //CARBONIZZARE  

TrafficCarPos[25] = <<558.8278, -89.5658, 67.1954>>
TrafficCarQuatX[25] = 0.0763
TrafficCarQuatY[25] = 0.0202
TrafficCarQuatZ[25] = -0.5700
TrafficCarQuatW[25] = 0.8179
TrafficCarRecording[25] = 112
TrafficCarStartime[25] = 55610.3008
TrafficCarModel[25] = taxi

TrafficCarPos[26] = <<525.6269, -124.3243, 60.7692>>
TrafficCarQuatX[26] = 0.0799
TrafficCarQuatY[26] = 0.0173
TrafficCarQuatZ[26] = -0.2839
TrafficCarQuatW[26] = 0.9554
TrafficCarRecording[26] = 113
TrafficCarStartime[26] = 55610.3008
TrafficCarModel[26] = bus

TrafficCarPos[27] = <<546.6170, -83.7620, 67.0976>>
TrafficCarQuatX[27] = 0.1902
TrafficCarQuatY[27] = -0.1282
TrafficCarQuatZ[27] = 0.8789
TrafficCarQuatW[27] = 0.4182
TrafficCarRecording[27] = 25
TrafficCarStartime[27] = 58542.0000
TrafficCarModel[27] = taxi //PCJ

TrafficCarPos[28] = <<512.8046, -125.0119, 60.1147>>
TrafficCarQuatX[28] = 0.0016
TrafficCarQuatY[28] = -0.0689
TrafficCarQuatZ[28] = 0.9713
TrafficCarQuatW[28] = 0.2277
TrafficCarRecording[28] = 26
TrafficCarStartime[28] = 60588.0000
TrafficCarModel[28] = DUMMY_MODEL_FOR_SCRIPT //futo //BMX

TrafficCarPos[29] = <<321.9392, -109.2699, 67.8426>>
TrafficCarQuatX[29] = 0.0084
TrafficCarQuatY[29] = -0.0199
TrafficCarQuatZ[29] = 0.8069
TrafficCarQuatW[29] = -0.5903
TrafficCarRecording[29] = 27
TrafficCarStartime[29] = 69234.0000
TrafficCarModel[29] = CARBONIZZARE //penumbra

TrafficCarPos[30] = <<278.0286, -73.8002, 69.5330>>
TrafficCarQuatX[30] = 0.0043
TrafficCarQuatY[30] = 0.0017
TrafficCarQuatZ[30] = 0.9776
TrafficCarQuatW[30] = 0.2105
TrafficCarRecording[30] = 28
TrafficCarStartime[30] = 71346.0000
TrafficCarModel[30] = penumbra

TrafficCarPos[31] = <<276.3335, -111.5560, 69.3416>>
TrafficCarQuatX[31] = 0.0230
TrafficCarQuatY[31] = -0.0041
TrafficCarQuatZ[31] = -0.1834
TrafficCarQuatW[31] = 0.9828
TrafficCarRecording[31] = 29
TrafficCarStartime[31] = 71676.0000
TrafficCarModel[31] = futo

TrafficCarPos[32] = <<254.9427, -76.6293, 69.4554>>
TrafficCarQuatX[32] = -0.0030
TrafficCarQuatY[32] = 0.0042
TrafficCarQuatZ[32] = 0.8139
TrafficCarQuatW[32] = -0.5810
TrafficCarRecording[32] = 30
TrafficCarStartime[32] = 72204.0000
TrafficCarModel[32] = penumbra

TrafficCarPos[33] = <<236.3161, -186.5169, 54.7485>>
TrafficCarQuatX[33] = -0.0102
TrafficCarQuatY[33] = -0.0531
TrafficCarQuatZ[33] = 0.9836
TrafficCarQuatW[33] = 0.1720
TrafficCarRecording[33] = 31
TrafficCarStartime[33] = 76230.0000
TrafficCarModel[33] = futo

TrafficCarPos[34] = <<229.2940, -204.6162, 53.4777>>
TrafficCarQuatX[34] = 0.0007
TrafficCarQuatY[34] = -0.0002
TrafficCarQuatZ[34] = 0.9141
TrafficCarQuatW[34] = 0.4055
TrafficCarRecording[34] = 32
TrafficCarStartime[34] = 76230.0000
TrafficCarModel[34] = penumbra

TrafficCarPos[35] = <<234.9658, -223.4058, 53.6191>>
TrafficCarQuatX[35] = 0.0056
TrafficCarQuatY[35] = 0.0012
TrafficCarQuatZ[35] = 0.5698
TrafficCarQuatW[35] = 0.8218
TrafficCarRecording[35] = 34
TrafficCarStartime[35] = 77352.0000
TrafficCarModel[35] = taxi

TrafficCarPos[36] = <<221.0836, -222.9086, 54.1788>>
TrafficCarQuatX[36] = -0.0031
TrafficCarQuatY[36] = 0.0061
TrafficCarQuatZ[36] = 0.8252
TrafficCarQuatW[36] = -0.5647
TrafficCarRecording[36] = 35
TrafficCarStartime[36] = 77682.0000
TrafficCarModel[36] = mule

TrafficCarPos[37] = <<220.7465, -228.1144, 53.4656>>
TrafficCarQuatX[37] = -0.0019
TrafficCarQuatY[37] = 0.0030
TrafficCarQuatZ[37] = 0.8178
TrafficCarQuatW[37] = -0.5754
TrafficCarRecording[37] = 36
TrafficCarStartime[37] = 79134.0000
TrafficCarModel[37] = penumbra

TrafficCarPos[38] = <<203.5953, -342.6908, 43.6340>>
TrafficCarQuatX[38] = 0.0020
TrafficCarQuatY[38] = 0.0006
TrafficCarQuatZ[38] = 0.5637
TrafficCarQuatW[38] = 0.8260
TrafficCarRecording[38] = 37
TrafficCarStartime[38] = 82632.0000
TrafficCarModel[38] = taxi

TrafficCarPos[39] = <<176.7323, -365.8312, 43.0022>>
TrafficCarQuatX[39] = 0.0245
TrafficCarQuatY[39] = -0.0051
TrafficCarQuatZ[39] = -0.2020
TrafficCarQuatW[39] = 0.9791
TrafficCarRecording[39] = 38
TrafficCarStartime[39] = 84018.0000
TrafficCarModel[39] = dubsta

TrafficCarPos[40] = <<168.5778, -336.4375, 43.5013>>
TrafficCarQuatX[40] = -0.0022
TrafficCarQuatY[40] = 0.0015
TrafficCarQuatZ[40] = 0.8002
TrafficCarQuatW[40] = -0.5997
TrafficCarRecording[40] = 39
TrafficCarStartime[40] = 84084.0000
TrafficCarModel[40] = penumbra

TrafficCarPos[41] = <<4.1466, -372.4675, 39.3020>>
TrafficCarQuatX[41] = 0.0469
TrafficCarQuatY[41] = 0.0063
TrafficCarQuatZ[41] = -0.1880
TrafficCarQuatW[41] = 0.9810
TrafficCarRecording[41] = 40
TrafficCarStartime[41] = 93060.0000
TrafficCarModel[41] = taxi

TrafficCarPos[42] = <<14.6536, -491.7607, 33.9844>>
TrafficCarQuatX[42] = -0.0010
TrafficCarQuatY[42] = -0.0049
TrafficCarQuatZ[42] = 0.7513
TrafficCarQuatW[42] = 0.6599
TrafficCarRecording[42] = 41
TrafficCarStartime[42] = 96624.0000
TrafficCarModel[42] = bus

TrafficCarPos[43] = <<-10.2838, -420.3103, 39.1597>>
TrafficCarQuatX[43] = -0.0141
TrafficCarQuatY[43] = 0.0241
TrafficCarQuatZ[43] = -0.1440
TrafficCarQuatW[43] = 0.9892
TrafficCarRecording[43] = 42
TrafficCarStartime[43] = 97350.0000
TrafficCarModel[43] = tornado3

TrafficCarPos[44] = <<-14.9425, -417.0332, 39.2908>>
TrafficCarQuatX[44] = -0.0119
TrafficCarQuatY[44] = 0.0229
TrafficCarQuatZ[44] = -0.1418
TrafficCarQuatW[44] = 0.9896
TrafficCarRecording[44] = 43
TrafficCarStartime[44] = 97746.0000
TrafficCarModel[44] = tornado3

ENDPROC


/// PURPOSE:
///    Initialise the setpiece cars for the uba
PROC POPPY_SETPIECE_CAR()
// ****  UBER RECORDED SET PIECE CARS  ****  !!

	SetPieceCarPos[0] = <<-61.5962, 337.5525, 111.1293>>
	SetPieceCarQuatX[0] = -0.0046
	SetPieceCarQuatY[0] = -0.0506
	SetPieceCarQuatZ[0] = 0.9800
	SetPieceCarQuatW[0] = 0.1923
	SetPieceCarRecording[0] = 101
	SetPieceCarStartime[0] = 59.4000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = rapidgt2
	SetPieceCarID[0] = viChaseCars[POPPY_CAR]
	
	SetPieceCarPos[1] = <<-62.3594, 322.6118, 109.7076>>
	SetPieceCarQuatX[1] = -0.0241
	SetPieceCarQuatY[1] = -0.0644
	SetPieceCarQuatZ[1] = 0.9804
	SetPieceCarQuatW[1] = 0.1845
	SetPieceCarRecording[1] = 102
	SetPieceCarStartime[1] = 1961.6000
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = landstalker
	SetPieceCarID[1] = viChaseCars[SHAGGER_CAR]

	SetPieceCarPos[2] = <<-68.0978, 309.2655, 107.7569>>
	SetPieceCarQuatX[2] = -0.0147
	SetPieceCarQuatY[2] = -0.0700
	SetPieceCarQuatZ[2] = 0.9755
	SetPieceCarQuatW[2] = 0.2082
	SetPieceCarRecording[2] = 103
	SetPieceCarStartime[2] = 5416.1958
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = landstalker
	SetPieceCarID[2] = viChaseCars[BODYGUARD_CAR]
	
	SetPieceCarPos[3] = <<654.7409, 37.7855, 85.6549>>
	SetPieceCarQuatX[3] = 0.0329
	SetPieceCarQuatY[3] = 0.0239
	SetPieceCarQuatZ[3] = 0.5549
	SetPieceCarQuatW[3] = 0.8309
	SetPieceCarRecording[3] = 125
	SetPieceCarStartime[3] = 44719.9922
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = taxi
	
	SetPieceCarPos[4] = <<435.1409, -57.2070, 74.1370>>
	SetPieceCarQuatX[4] = -0.0062
	SetPieceCarQuatY[4] = -0.0807
	SetPieceCarQuatZ[4] = 0.9560
	SetPieceCarQuatW[4] = 0.2818
	SetPieceCarRecording[4] = 150
	SetPieceCarStartime[4] = 60000.0000
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = taxi
	
	SetPieceCarPos[5] = <<371.1497, -244.7208, 53.7699>>
	SetPieceCarQuatX[5] = 0.0223
	SetPieceCarQuatY[5] = -0.0044
	SetPieceCarQuatZ[5] = -0.1527
	SetPieceCarQuatW[5] = 0.9880
	SetPieceCarRecording[5] = 151
	SetPieceCarStartime[5] = 60000.0000
	SetPieceCarRecordingSpeed[5] = 1.0000
	SetPieceCarModel[5] = fq2
	
	SetPieceCarPos[6] = <<443.9521, -32.4616, 78.8341>>
	SetPieceCarQuatX[6] = -0.0301
	SetPieceCarQuatY[6] = -0.0745
	SetPieceCarQuatZ[6] = 0.9603
	SetPieceCarQuatW[6] = 0.2672
	SetPieceCarRecording[6] = 152
	SetPieceCarStartime[6] = 60000.0000
	SetPieceCarRecordingSpeed[6] = 1.0000
	SetPieceCarModel[6] = mule
	
	SetPieceCarPos[7] = <<540.6125, -93.1815, 65.6238>>
	SetPieceCarQuatX[7] = 0.1322
	SetPieceCarQuatY[7] = -0.1121
	SetPieceCarQuatZ[7] = 0.8787
	SetPieceCarQuatW[7] = 0.4447
	SetPieceCarRecording[7] = 120
	SetPieceCarStartime[7] = 60610.3008
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = faggio2
	//SetPieceCarID[7] = vehFaggio
/*
	SetPieceCarPos[8] = <<546.8231, -89.1675, 66.5121>>
	SetPieceCarQuatX[8] = 0.1339
	SetPieceCarQuatY[8] = -0.1154
	SetPieceCarQuatZ[8] = 0.8577
	SetPieceCarQuatW[8] = 0.4828
	SetPieceCarRecording[8] = 121
	SetPieceCarStartime[8] = 60610.3008
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = DUMMY_MODEL_FOR_SCRIPT
*/	
	SetPieceCarPos[8] = <<243.4510, -202.3356, 53.8657>>
	SetPieceCarQuatX[8] = 0.0356
	SetPieceCarQuatY[8] = 0.0122
	SetPieceCarQuatZ[8] = -0.1815
	SetPieceCarQuatW[8] = 0.9827
	SetPieceCarRecording[8] = 130
	SetPieceCarStartime[8] = 70000.0000
	SetPieceCarRecordingSpeed[8] = 1.0000
	SetPieceCarModel[8] = SURFER2
/*
	SetPieceCarPos[10] = <<179.6045, -371.7909, 43.0346>>
	SetPieceCarQuatX[10] = 0.0282
	SetPieceCarQuatY[10] = -0.0034
	SetPieceCarQuatZ[10] = -0.2161
	SetPieceCarQuatW[10] = 0.9760
	SetPieceCarRecording[10] = 131
	SetPieceCarStartime[10] = 72000.0000
	SetPieceCarRecordingSpeed[10] = 1.0000
	SetPieceCarModel[10] = DUMMY_MODEL_FOR_SCRIPT
*/	
	
ENDPROC


PROC DO_FOCUS_PUSH()

	FLOAT fHintFov = 35.0
	FLOAT fHintFollow = 0.4//0.35
	FLOAT fHintPitchOrbit = 0.000
	FLOAT fHintSide = 0.1//0.07  //0.25 //-0.01
	FLOAT fHintVert = 0.02//0.07 //-0.050

	IF NOT IS_REPLAY_IN_PROGRESS()
	AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	
	AND IS_PED_UNINJURED(PLAYER_PED_ID())		
	AND bDoingFocusPush = TRUE	
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			/*
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],6)
				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],<<0,0,0>>,1,-1,0.5)
			ELSE	
				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
			ENDIF
			*/
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
			SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], <<0,0,0>>, TRUE, 30000)
			SET_GAMEPLAY_HINT_FOV(fHintFov)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
			SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
			iTimerFocusPush = GET_GAME_TIMER()
			BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
		ELSE
			/*
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],6)	
				//IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) = PERFORMING_TASK
				//	iTimerFocusPush -= 500
				//ENDIF
				//IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],45)
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK	
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],-1)
				ENDIF
			ENDIF
			*/
			UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],3)	
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-69.110252,302.947449,105.334381>>, <<-77.485947,306.753998,110.556976>>, 7.000000)	
				iTimerFocusPush -= 5000
			ENDIF
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
			STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC MAKE_CLOUDY_IF_NIGHT()

	IF NOT bMakeCloudy	
		sTimeOfDay = GET_CURRENT_TIMEOFDAY()
		iHour = GET_TIMEOFDAY_HOUR(sTimeOfDay)
		IF iHour > 22
		OR iHour < 5
			CPRINTLN(DEBUG_MISSION, "MAKE CLOUDY")
			CLEAR_WEATHER_TYPE_PERSIST()
			SET_WEATHER_TYPE_OVERTIME_PERSIST("OVERCAST",10)
			bMakeCloudy = TRUE
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL HAS_PLAYER_KILLED_SOMEONE()
	
	INT i = 0
	IF Has_Ped_Been_Killed()
		REPEAT Get_Number_Of_Ped_Killed_Events() i
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Get_Index_Of_Killed_Ped(i), PLAYER_PED_ID(), FALSE)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_INJURED_SOMEONE()
	
	INT i = 0
	IF Has_Ped_Been_Killed()
		REPEAT Get_Number_Of_Ped_Killed_Events() i
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(Get_Index_Of_Killed_Ped(i), PLAYER_PED_ID(), FALSE)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_BUILDER_PEDS()

	IF NOT DOES_ENTITY_EXIST(pedBuilder[0])	
		REQUEST_MODEL(S_M_Y_Construct_01)
		IF NOT HAS_MODEL_LOADED(S_M_Y_Construct_01)
			EXIT
		ELSE
			pedBuilder[0] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_Construct_01,<<76.0832, -346.6293, 41.6255>>, 247.2738)
			pedBuilder[1] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_Construct_01,<<94.7224, -361.5001, 41.5291>>, 247.5127)
			TASK_GO_TO_ENTITY(pedBuilder[0],pedBuilder[1],-1,1.0,1)
			TASK_LOOK_AT_ENTITY(pedBuilder[1],PLAYER_PED_ID(),-1)
			pedBuilder[2] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_Construct_01,<<76.8379, -378.1883, 38.9214>>, 124.7959)
			TASK_START_SCENARIO_IN_PLACE(pedBuilder[2],"WORLD_HUMAN_CLIPBOARD")
			pedBuilder[3] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_Construct_01,<<74.4935, -379.7711, 38.9214>>, 291.4748)
			TASK_START_SCENARIO_IN_PLACE(pedBuilder[0],"WORLD_HUMAN_STAND_MOBILE")
			pedBuilder[4] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_Construct_01,<<68.7581, -402.9993, 38.9192>>, 291.2518)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_Construct_01)
		ENDIF
	ENDIF
		
ENDPROC

//0.835512 //Sync scene phase where they notice Franklin
PROC BREAKOUT_SYNC_SCENE_SPOT_PLAYER()

	IF bDoneSyncSceneBreakout = FALSE	
		#IF NOT IS_JAPANESE_BUILD
			poppy_shagging_scene_breakout = CREATE_SYNCHRONIZED_SCENE(<<-30.990408,301.092346,111.681885>>,<<0,0,0>>)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(poppy_shagging_scene_breakout,FALSE)
		#ENDIF
		IF IS_PED_UNINJURED(mPoppy.piPed)
			IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPoppy.piPed,PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1)
				#IF NOT IS_JAPANESE_BUILD	
					TASK_SYNCHRONIZED_SCENE (mPoppy.piPed, poppy_shagging_scene_breakout, "rcmpaparazzo_2", "shag_action_poppy", SLOW_BLEND_IN, slow_BLEND_OUT)
				#ENDIF
				#IF IS_JAPANESE_BUILD
					TASK_TURN_PED_TO_FACE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1)
				#ENDIF
			ELSE
				TASK_SMART_FLEE_PED(mPoppy.piPed,PLAYER_PED_ID(),300,-1)
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mShagger.piPed)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed,TRUE)			
			IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mShagger.piPed,PLAYER_PED_ID())	
				TASK_LOOK_AT_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
				#IF NOT IS_JAPANESE_BUILD
					TASK_SYNCHRONIZED_SCENE (mShagger.piPed, poppy_shagging_scene_breakout, "rcmpaparazzo_2", "shag_action_a", SLOW_BLEND_IN, -2)
				#ENDIF
				#IF IS_JAPANESE_BUILD
					TASK_TURN_PED_TO_FACE_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
				#ENDIF
			ELSE
				TASK_SMART_FLEE_PED(mShagger.piPed,PLAYER_PED_ID(),300,-1)
			ENDIF
		ENDIF
		#IF NOT IS_JAPANESE_BUILD
			SET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene_breakout,0.835512)
		#ENDIF
		bDoneSyncSceneBreakout = TRUE
	ENDIF

ENDPROC

/// PURPOSE:
/// Grabs the table that Poppy is getting shagged over and repositions it   
PROC REPOSITION_SHAG_TABLE()

	REQUEST_MODEL(prop_table_06)
	REQUEST_MODEL(prop_parasol_03)

	IF NOT DOES_ENTITY_EXIST(objShagTable)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-30.860102,301.191986,112.116371>>,5.0,prop_table_06)	
			objShagTable = GET_CLOSEST_OBJECT_OF_TYPE(<<-30.860102,301.191986,112.116371>>,5.0,prop_table_06)
			SET_ENTITY_COORDS(objShagTable,<<-30.860102,301.191986,112.027371>>)  //<<-30.860102,301.191986,112.116371>>  //112.050371   ---> //112.043371 <---
			SET_ENTITY_ROTATION(objShagTable,<<-0.100693,-0.038488,80.437294>>)
			FREEZE_ENTITY_POSITION(objShagTable,TRUE)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(objShagParasol)
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-30.860102,301.191986,112.116371>>,5.0,prop_parasol_03)	
			objShagParasol = GET_CLOSEST_OBJECT_OF_TYPE(<<-30.860102,301.191986,112.116371>>,5.0,prop_parasol_03)
			SET_ENTITY_COORDS(objShagParasol,<<-30.860102,301.191986,111.680336>>)
			SET_ENTITY_ROTATION(objShagParasol,<<-0.100693,-0.038488,80.437294>>)
			FREEZE_ENTITY_POSITION(objShagParasol,TRUE)
		ENDIF
	ENDIF
	/*
	IF NOT DOES_ENTITY_EXIST(objBin)
		objBin = CREATE_OBJECT (prop_cs_bin_01, <<-37.376980,321.238312,111.706215>>)
		SET_ENTITY_COORDS (objBin, <<-37.376980,321.238312,111.706215>>)
		SET_ENTITY_ROTATION (objBin, <<-0.098811,0.017827,-24.255245>>)
		FREEZE_ENTITY_POSITION(objBin,TRUE)
	ENDIF
	*/
	
ENDPROC

/// PURPOSE:
///    Print a string and put in a carriage return
/// PARAMS:
///    s - the string to print
PROC SK_PRINT(String s)
	CPRINTLN(DEBUG_MISSION,s)
ENDPROC

PROC CLEAR_PED_AREAS()

	//Ped clear areas
	// 26.13, 275.37, 108.55     7244.102051  -  14124.618164    //after cs
	// 672.90, 202.11, 92.69     36326.589844  -  45647.019531   //alley entrance
	// 622.15, 63.69, 89.07      44000.0          -   50000.0          //alley exit
	// 670.06, 2.94, 83.10      49000.0        -       56000.0            //corner of police station
	// 493.26, -138.52, 58.97    58000.0    -    67694.289063      //badger
	// 299.61, -111.56, 68.51    71936.148438  -  75888.992188     //corner top of hill (Poppy)

	IF fTimePosInRec > 7244.102051
	AND fTimePosInRec < 14124.618164
		IF NOT IS_SPHERE_VISIBLE(<<26.13, 275.37, 108.55>>,25)	   //after cs
			CLEAR_AREA_OF_PEDS(<<26.13, 275.37, 108.55>>,25)
		ENDIF
	ENDIF
	
	IF fTimePosInRec > 36326.589844
	AND fTimePosInRec < 45647.019531
		IF NOT IS_SPHERE_VISIBLE(<<672.90, 202.11, 92.69>>,15)	//alley entrance
			CLEAR_AREA_OF_PEDS(<<672.90, 202.11, 92.69>>,15)
		ENDIF
	ENDIF

	IF fTimePosInRec > 44000.0
	AND fTimePosInRec < 50000.0 
		IF NOT IS_SPHERE_VISIBLE(<<622.15, 63.69, 89.07>>,15)	//alley exit
			CLEAR_AREA_OF_PEDS(<<622.15, 63.69, 89.07>>,15)
		ENDIF
	ENDIF
	
	IF fTimePosInRec > 49000.0
	AND fTimePosInRec < 56000.0
		IF NOT IS_SPHERE_VISIBLE(<<670.06, 2.94, 83.10>>,20)	//corner of police station
			CLEAR_AREA_OF_PEDS(<<670.06, 2.94, 83.10>>,20)
		ENDIF
	ENDIF
	
	IF fTimePosInRec > 58000.0
	AND fTimePosInRec < 67694.289063
		IF NOT IS_SPHERE_VISIBLE(<<493.26, -138.52, 58.97>>,15)	//badger building
			CLEAR_AREA_OF_PEDS(<<493.26, -138.52, 58.97>>,15)
		ENDIF
	ENDIF
	
	IF fTimePosInRec > 70936.148438
	AND fTimePosInRec < 76888.992188
		IF NOT IS_SPHERE_VISIBLE(<<299.61, -111.56, 68.51>>,15)	//corner top of hill (Poppy)
			CLEAR_AREA_OF_PEDS(<<299.61, -111.56, 68.51>>,15)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Creates and attaches Poppy's phone
PROC ATTACH_POPPY_PHONE()
	
	IF NOT DOES_ENTITY_EXIST(ObjPhone)
		ObjPhone = CREATE_OBJECT_NO_OFFSET(Prop_NPC_Phone, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mPoppy.piPed,<<0.0,0.0,10.0>>))	
		SET_ENTITY_LOD_DIST(ObjPhone,100)
	ENDIF
	IF DOES_ENTITY_EXIST(ObjPhone)
		IF NOT IS_ENTITY_ATTACHED(ObjPhone)
			ATTACH_ENTITY_TO_ENTITY(ObjPhone, mPoppy.piPed, GET_PED_BONE_INDEX(mPoppy.piPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
		ENDIF
	ENDIF

ENDPROC

PROC POPPY_ANGRY_ANIMS()

	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2) 	
	AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout) 	 	
		IF iPoppyAnimSeq = 0		
			REQUEST_ANIM_DICT("RANDOM@CAR_THIEF@waiting_ig_4")	
			iPoppyAnimSeq = 1
		ELIF iPoppyAnimSeq = 1
			IF HAS_ANIM_DICT_LOADED("RANDOM@CAR_THIEF@waiting_ig_4")
				iPoppyAnimSeq = 2
				TASK_PLAY_ANIM(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1, AF_SECONDARY)
			ENDIF
		ELIF iPoppyAnimSeq = 2
			IF NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting")
				TASK_PLAY_ANIM(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1, AF_SECONDARY)
			ENDIF	
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Plays the sex anims on Poppy and Justin
/// PARAMS:
///    i - 0 is the looped anim and 1 is the scene
PROC PLAY_POPPY_SHAGGER_ANIM(INT i)
	IF IS_PED_UNINJURED(mPoppy.piPed) AND IS_PED_UNINJURED(mShagger.piPed)
		SWITCH  i
			CASE 0
				
				#IF NOT IS_JAPANESE_BUILD	
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene)
						REPOSITION_SHAG_TABLE()
						ATTACH_POPPY_PHONE()
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed,TRUE)
						poppy_shagging_scene = CREATE_SYNCHRONIZED_SCENE(<<-30.990408,301.092346,111.631885>>,<<0,0,0>>)  //111.681885
						SET_SYNCHRONIZED_SCENE_LOOPED(poppy_shagging_scene,TRUE)
						TASK_SYNCHRONIZED_SCENE (mPoppy.piPed, poppy_shagging_scene, "rcmpaparazzo_2", "shag_loop_poppy", INSTANT_BLEND_IN, slow_BLEND_OUT ,SYNCED_SCENE_LOOP_WITHIN_SCENE)
						TASK_SYNCHRONIZED_SCENE (mShagger.piPed, poppy_shagging_scene, "rcmpaparazzo_2", "shag_loop_a", INSTANT_BLEND_IN, slow_BLEND_OUT ,SYNCED_SCENE_LOOP_WITHIN_SCENE)
						SK_PRINT("TK************ SYNCH SCENE1 STARTED ************TK")
					ENDIF
				#ENDIF
				#IF IS_JAPANESE_BUILD	
					IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_START_SCENARIO_AT_POSITION) <> PERFORMING_TASK
						//SAFE_TELEPORT_ENTITY(mPoppy.piPed,<<-28.95, 300.81, 112.69>>,100.35)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mPoppy.piPed,TRUE)
						TASK_START_SCENARIO_AT_POSITION(mPoppy.piPed,"WORLD_HUMAN_SMOKING",<<-28.95, 300.81, 112.69>>,100.35)
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(mShagger.piPed,SCRIPT_TASK_START_SCENARIO_AT_POSITION) <> PERFORMING_TASK
						//SAFE_TELEPORT_ENTITY(mShagger.piPed,<<-29.87, 300.57, 112.69>>,-53.25)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed,TRUE)
						TASK_START_SCENARIO_AT_POSITION(mShagger.piPed,"WORLD_HUMAN_SMOKING",<<-29.87, 300.57, 112.69>>,-53.25)
					ENDIF
				#ENDIF
				
			BREAK
			
			CASE 1
				
				#IF NOT IS_JAPANESE_BUILD		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
						REPOSITION_SHAG_TABLE()
						ATTACH_POPPY_PHONE()
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed,TRUE)
						poppy_shagging_scene2 = CREATE_SYNCHRONIZED_SCENE(<<-30.990408,301.092346,111.681885>>,<<0,0,0>>)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(poppy_shagging_scene2,FALSE)
						TASK_SYNCHRONIZED_SCENE (mPoppy.piPed, poppy_shagging_scene2, "rcmpaparazzo_2", "shag_action_poppy", INSTANT_BLEND_IN, slow_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE (mShagger.piPed, poppy_shagging_scene2, "rcmpaparazzo_2", "shag_action_a", INSTANT_BLEND_IN, -2)
						PLAY_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
						//PLAY_STREAM_FROM_PED(mPoppy.piPed)
						bSetAudioPos = FALSE
						SK_PRINT("TK************ SYNCH SCENE2 STARTED - RESET AUDIO POSITION BOOL ************TK")
					ENDIF
				#ENDIF
				#IF IS_JAPANESE_BUILD
					IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_START_SCENARIO_AT_POSITION) <> PERFORMING_TASK
						//SAFE_TELEPORT_ENTITY(mPoppy.piPed,<<-28.95, 300.81, 112.69>>,100.35)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mPoppy.piPed,TRUE)
						TASK_START_SCENARIO_AT_POSITION(mPoppy.piPed,"WORLD_HUMAN_SMOKING",<<-28.95, 300.81, 112.69>>,100.35)
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(mShagger.piPed,SCRIPT_TASK_START_SCENARIO_AT_POSITION) <> PERFORMING_TASK
						//SAFE_TELEPORT_ENTITY(mShagger.piPed,<<-29.87, 300.57, 112.69>>,-53.25)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed,TRUE)
						TASK_START_SCENARIO_AT_POSITION(mShagger.piPed,"WORLD_HUMAN_SMOKING",<<-29.87, 300.57, 112.69>>,-53.25)
					ENDIF
				#ENDIF
			
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

PROC SETUP_SYNC_SCENES()	
	
	iDoFixForStream = 2
	
	IF bPoppyCreated = TRUE
		IF iDoFixForStream = 1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
				STOP_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
				//STOP_STREAM()
				iDoFixForStream = 2
			ENDIF
		ELIF iDoFixForStream = 0
			IF bSetAudioPos = FALSE
				//INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("PAP2_IG1_POPPYSEX",<<-30.990408,301.092346,112.681885>>)
				INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("PAP2_IG1_POPPYSEX",mPoppy.piPed)
				SK_PRINT("TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK")
				bSetAudioPos = TRUE
			ENDIF
			IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("PAP2_IG1_POPPYSEX",0) = TRUE	
			//IF LOAD_STREAM("CUTSCENES_PAP2_IG1_POPPYSEX_CENTER")	
				PLAY_POPPY_SHAGGER_ANIM(1)
				iDoFixForStream = 1
			ENDIF
		ELIF iDoFixForStream = 2	
			IF IS_PED_UNINJURED(mPoppy.piPed)		
				IF bPlayerSentToCover = FALSE
					PLAY_POPPY_SHAGGER_ANIM(0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Create a load of traffic/parked cars
PROC DO_SCRIPTED_TRAFFIC()

	IF iSeqTraffic = 0
		REQUEST_MODEL(TAXI)
		REQUEST_MODEL(BUS)
		//REQUEST_MODEL(BLISTA)
		//REQUEST_MODEL(MULE)
		
		IF HAS_MODEL_LOADED(TAXI)
		AND HAS_MODEL_LOADED(BUS)
		//AND HAS_MODEL_LOADED(BLISTA)
		//AND HAS_MODEL_LOADED(MULE)
			iSeqTraffic = 1
		ENDIF
	
	ELIF iSeqTraffic = 1
		
		vehTraffic[0] = CREATE_VEHICLE(TAXI,<<-61.5700, 263.6960, 102.3952>>, 96.8514)
		pedRandomDriver[0] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[0])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[0])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[0],vehTraffic[0],<<-196.7214, 263.6796, 91.2333>>,8.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
		
		vehTraffic[1] = CREATE_VEHICLE(BUS,<<-129.3595, 257.7685, 94.9980>>, 87.6350)
		pedRandomDriver[1] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[1])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[1])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[1],vehTraffic[1],<<-196.7214, 263.6796, 91.2333>>,8.0,DRIVINGSTYLE_NORMAL,BUS,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
/*
		vehTraffic[2] = CREATE_VEHICLE(BLISTA,<<-114.7640, 263.0628, 96.0816>>, 91.6822)
		pedRandomDriver[2] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[2])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[2])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[2],vehTraffic[2],<<-213.4495, 269.5216, 90.9854>>,8.0,DRIVINGSTYLE_NORMAL,BLISTA,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
*/		
		vehTraffic[3] = CREATE_VEHICLE(TAXI,<<-189.8024, 253.4723, 91.4575>>, 262.6279)
		pedRandomDriver[3] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[3])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[3])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[3],vehTraffic[3],<<13.1838, 255.6287, 108.5748>>,8.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
/*
		vehTraffic[4] = CREATE_VEHICLE(MULE,<<-141.6916, 249.0392, 94.1239>>, 270.6987)
		pedRandomDriver[4] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[4])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[4])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[4],vehTraffic[4],<<13.1838, 255.6287, 108.5748>>,8.0,DRIVINGSTYLE_NORMAL,MULE,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)	
		
		vehTraffic[5] = CREATE_VEHICLE(BLISTA,<<-70.2928, 254.0054, 101.2819>>, 276.1490)
		pedRandomDriver[5] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[5])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[5])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[5],vehTraffic[5],<<13.1838, 255.6287, 108.5748>>,8.0,DRIVINGSTYLE_NORMAL,BLISTA,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
*/	
		vehTraffic[6] = CREATE_VEHICLE(BUS,<<-175.1252, 247.2091, 91.9910>>, 265.0136)
		pedRandomDriver[6] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[6])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[6])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[6],vehTraffic[6],<<7.4758, 252.7416, 108.4110>>,8.0,DRIVINGSTYLE_NORMAL,BUS,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
		
		vehTraffic[7] = CREATE_VEHICLE(TAXI,<<-98.9022, 246.2435, 97.7797>>, 274.0935)
		pedRandomDriver[7] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[7])	
		SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[7])
		TASK_VEHICLE_DRIVE_TO_COORD(pedRandomDriver[7],vehTraffic[7],<<7.4758, 252.7416, 108.4110>>,8.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
		
		iSeqTraffic = 2
					
	ELIF iSeqTraffic = 2
		IF fTimePosInRec > 15815.714844
			
			SAFE_RELEASE_PED(pedRandomDriver[0])
			SAFE_RELEASE_PED(pedRandomDriver[1])
			SAFE_RELEASE_PED(pedRandomDriver[2])
			SAFE_RELEASE_PED(pedRandomDriver[3])
			SAFE_RELEASE_PED(pedRandomDriver[4])
			SAFE_RELEASE_PED(pedRandomDriver[5])
			SAFE_RELEASE_PED(pedRandomDriver[6])
			SAFE_RELEASE_PED(pedRandomDriver[7])
			
			SAFE_RELEASE_VEHICLE(vehTraffic[0])
			SAFE_RELEASE_VEHICLE(vehTraffic[1])
			SAFE_RELEASE_VEHICLE(vehTraffic[2])
			SAFE_RELEASE_VEHICLE(vehTraffic[3])
			SAFE_RELEASE_VEHICLE(vehTraffic[4])
			SAFE_RELEASE_VEHICLE(vehTraffic[5])
			SAFE_RELEASE_VEHICLE(vehTraffic[6])
			SAFE_RELEASE_VEHICLE(vehTraffic[7])
			
			SET_MODEL_AS_NO_LONGER_NEEDED(BUS)
			SET_MODEL_AS_NO_LONGER_NEEDED(TAXI)
			
			iSeqTraffic = 7  //3
			
		ENDIF
	ELIF iSeqTraffic = 3
		IF fTimePosInRec > 41719.378906
			
			vehTraffic[8] = CREATE_VEHICLE(BUS,<<706.0617, 35.9678, 83.2352>>, 146.9535)
			pedRandomDriver[8] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[8])	
			SET_VEHICLE_ENGINE_ON(vehTraffic[8],TRUE,TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[8])
			
			vehTraffic[9] = CREATE_VEHICLE(BLISTA,<<699.3248, 35.4587, 83.1331>>, 147.9884)
			pedRandomDriver[9] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[9])	
			SET_VEHICLE_ENGINE_ON(vehTraffic[9],TRUE,TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[9])
			
			iSeqTraffic = 4
			
		ENDIF
	ELIF iSeqTraffic = 4
		IF fTimePosInRec > 58743.738281
		
			SAFE_RELEASE_PED(pedRandomDriver[8])
			SAFE_RELEASE_PED(pedRandomDriver[9])
	
			SAFE_RELEASE_VEHICLE(vehTraffic[8])
			SAFE_RELEASE_VEHICLE(vehTraffic[9])
			
			iSeqTraffic = 5
			
		ENDIF
	ELIF iSeqTraffic = 5
		IF fTimePosInRec > 58743.738281
			
			vehTraffic[10] = CREATE_VEHICLE(TAXI,<<511.5198, -180.5710, 52.8702>>, 10.0630)
			pedRandomDriver[10] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[10])	
			SET_VEHICLE_ENGINE_ON(vehTraffic[10],TRUE,TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[10])
			
			vehTraffic[11] = CREATE_VEHICLE(BLISTA,<<516.6854, -179.7204, 53.0255>>, 11.7232)
			pedRandomDriver[11] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[11])	
			SET_VEHICLE_ENGINE_ON(vehTraffic[11],TRUE,TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[11])
			
			iSeqTraffic = 6
			
		ENDIF
	ELIF iSeqTraffic = 6
		IF fTimePosInRec > 72153.648438
		
			SAFE_RELEASE_PED(pedRandomDriver[10])
			SAFE_RELEASE_PED(pedRandomDriver[11])
	
			SAFE_RELEASE_VEHICLE(vehTraffic[10])
			SAFE_RELEASE_VEHICLE(vehTraffic[11])
			
			iSeqTraffic = 7
			
		ENDIF
	ENDIF
	
	/*
	1 start 0.0              //outside hotel
	cleanup 15815.714844
	
	<<-61.5700, 263.6960, 102.3952>>, 96.8514    
	<<-129.3595, 257.7685, 94.9980>>, 87.6350    goto   <<-196.7214, 263.6796, 91.2333>>
	
	<<-114.7640, 263.0628, 96.0816>>, 91.6822    goto    <<-213.4495, 269.5216, 90.9854>>
	
	<<-189.8024, 253.4723, 91.4575>>, 262.6279
	<<-141.6916, 249.0392, 94.1239>>, 270.6987
	<<-70.2928, 254.0054, 101.2819>>, 276.1490    goto   <<13.1838, 255.6287, 108.5748>>
	
	<<-175.1252, 247.2091, 91.9910>>, 265.0136
	<<-98.9022, 246.2435, 97.7797>>, 274.0935     goto    <<7.4758, 252.7416, 108.4110>>
	
	
	
	2 start 41719.378906   //end of alley & junction
	cleanup 58743.738281
	
	<<706.0617, 35.9678, 83.2352>>, 146.9535    //bus parked
	<<699.3248, 35.4587, 83.1331>>, 147.9884    //blista parked
	
	3 start 58743.738281       //next junction
	cleanup 72153.648438
	
	<<511.5198, -180.5710, 52.8702>>, 10.0630    //taxi parked
	<<516.6854, -179.7204, 53.0255>>, 11.7232     //carbonizz parked

	//don't do bad cam lines in building site
	*/
	
ENDPROC

PROC DELETE_SCRIPTED_TRAFFIC()

	SAFE_DELETE_PED(pedRandomDriver[0])
	SAFE_DELETE_PED(pedRandomDriver[1])
	SAFE_DELETE_PED(pedRandomDriver[2])
	SAFE_DELETE_PED(pedRandomDriver[3])
	SAFE_DELETE_PED(pedRandomDriver[4])
	SAFE_DELETE_PED(pedRandomDriver[5])
	SAFE_DELETE_PED(pedRandomDriver[6])
	SAFE_DELETE_PED(pedRandomDriver[7])
	
	SAFE_DELETE_VEHICLE(vehTraffic[0])
	SAFE_DELETE_VEHICLE(vehTraffic[1])
	SAFE_DELETE_VEHICLE(vehTraffic[2])
	SAFE_DELETE_VEHICLE(vehTraffic[3])
	SAFE_DELETE_VEHICLE(vehTraffic[4])
	SAFE_DELETE_VEHICLE(vehTraffic[5])
	SAFE_DELETE_VEHICLE(vehTraffic[6])
	SAFE_DELETE_VEHICLE(vehTraffic[7])

ENDPROC

PROC DO_BEV_COVER_ANIMS()			
	
	INT iRand
	SEQUENCE_INDEX seqBevCov

	IF IS_PED_IN_COVER(mBuddy.piPed)
		IF eMissionStage = MS_FOLLOW_TO_POPPY	
		OR eMissionStage = MS_FOLLOW_BEV_GUARDS
			IF bPassCamConvo = FALSE
				IF NOT bPlayerSentToCover		
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_PASS", CONV_PRIORITY_HIGH) //Take my camera and film this shit, I'll keep a look out.
						bPassCamConvo = TRUE
					ENDIF
				ELSE
					bPassCamConvo = TRUE
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF bGoToBev = FALSE
						IF bPlayerSentToCover = FALSE
							PRINT_NOW("PAP2_08", DEFAULT_GOD_TEXT_TIME,0)  //Go to ~b~Beverly
							bGoToBev = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iCoverSeq = 0	
			REQUEST_ANIM_DICT("rcmpaparazzo_2")		
			REQUEST_CLIP_SET(sWeaponMoveClipset)
			REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
			iCoverSeq = 1
		ELIF iCoverSeq = 1
			IF HAS_ANIM_DICT_LOADED("rcmpaparazzo_2")
			//AND GET_GAME_TIMER() > iTimerBevCoverTask + 2000
			//AND HAS_CLIP_SET_LOADED(sWeaponMoveClipset)	
				iCoverSeq = 2
				
				OPEN_SEQUENCE_TASK(seqBevCov)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_c",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_c",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_l_corner_c",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
					SET_SEQUENCE_TO_REPEAT(seqBevCov,REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(seqBevCov)
				TASK_PERFORM_SEQUENCE(mBuddy.piPed,seqBevCov)
				
			ENDIF
		ELIF iCoverSeq = 2
			IF NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_a")
			AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_b")
			AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_c")
				iRand = GET_RANDOM_INT_IN_RANGE(0,40)
				IF iRand = 1
					TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
				ENDIF
				IF iRand = 2
					TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
				ENDIF
				IF iRand = 3
					TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_c",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

ENDPROC

//*************************************************************************************************************************************************
//													:DEBUG FUNCTIONS:
//*************************************************************************************************************************************************

/// PURPOSE:
///    Prints a string then an int used for debuging vars
/// PARAMS:
///    s - String to print 
///    i - The int to print
PROC SK_PRINT_INT(String s, INT i)
	CPRINTLN(DEBUG_MISSION,s)
	PRINTINT(i)
ENDPROC

/// PURPOSE:
///    Prints a string then a float used for debuging vars
/// PARAMS:
///    s - String to print
///    f - the float to print
PROC SK_PRINT_FLOAT(String s, FLOAT f)
	CPRINTLN(DEBUG_MISSION,s)
	PRINTFLOAT(f)
ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Creates widgets for rag
PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("STE PAP 2 WID")
			START_WIDGET_GROUP("CAMERA TWEAKER")
				ADD_WIDGET_BOOL("Use Locked Cam", bUseLockedCam)
				ADD_WIDGET_BOOL("Use Face Recognition", bUseFace)
				START_WIDGET_GROUP("CAMERA Variables")
					/*
					ADD_WIDGET_BOOL("Is Security", bIsSecurity)
					ADD_WIDGET_BOOL("Is Low Tech", bIsLowTech)
					ADD_WIDGET_BOOL("Show Rec", bShowRec)
					ADD_WIDGET_INT_SLIDER("Max Static", iMaxStatic, 0, 100, 1)				
					ADD_WIDGET_INT_SLIDER("Min Static", iMinStatic, 0, 100, 1)
					
					START_WIDGET_GROUP("Alt Cam OverLay")
						ADD_WIDGET_BOOL("Just use scan", bJustUseScan)
						ADD_WIDGET_BOOL("ON/Off Scan Lines", bOnScanLines)
						ADD_WIDGET_INT_SLIDER("iScanAlpha", iScanAlpha, 0, 500, 1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("FACE BOX TAG")
						ADD_WIDGET_INT_SLIDER("Scale Multi", iScaleMulti, 0, 3000, 1)
						ADD_WIDGET_FLOAT_SLIDER("Max Box", fMaxBox, 0, 15, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("Min Box", fMinBox, 0, 15, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("Face Offset", fFaceOffset , -15, 15, 0.001)
					STOP_WIDGET_GROUP()
					
					
					START_WIDGET_GROUP("CURSOR")
						ADD_WIDGET_FLOAT_SLIDER("Cursor Height", fCursorHeight, 0, 2, 0.001)
						ADD_WIDGET_FLOAT_SLIDER("Cursor Width", fCursorWidth, 0, 2, 0.001)

						START_WIDGET_GROUP("Camera Crosshair 1")
							//ADD_WIDGET_FLOAT_SLIDER("Cross 1 Hair Height", fCross1HairHeight, 0, 1, 0.001)
							//ADD_WIDGET_FLOAT_SLIDER("Cross 1 Hair Width", fCross1HairWidth, 0, 1, 0.001)
						STOP_WIDGET_GROUP()
					
						START_WIDGET_GROUP("Camera Crosshair 2")
							//ADD_WIDGET_FLOAT_SLIDER("Cross 2 Hair Height", fCross2HairHeight, 0, 1, 0.001)
							//ADD_WIDGET_FLOAT_SLIDER("Cross 2 Hair Width", fCross2HairWidth, 0, 1, 0.001)
						STOP_WIDGET_GROUP()
						
						//Cursor corner 1
						START_WIDGET_GROUP("Cursor corner 1")
							ADD_WIDGET_FLOAT_SLIDER("Cursor Height", fCursorX1, 0, 1, 0.001)
							ADD_WIDGET_FLOAT_SLIDER("Cursor Width", fCursorY1, 0, 1, 0.001)
						STOP_WIDGET_GROUP()

						//Cursor corner 2					
						START_WIDGET_GROUP("Cursor corner 2")
							ADD_WIDGET_FLOAT_SLIDER("Cursor Height", fCursorX2, 0, 1, 0.001)
							ADD_WIDGET_FLOAT_SLIDER("Cursor Width", fCursorY2, 0, 1, 0.001)
						STOP_WIDGET_GROUP()

						//Cursor corner 3					
						START_WIDGET_GROUP("Cursor corner 3")
							ADD_WIDGET_FLOAT_SLIDER("Cursor Height", fCursorX3, 0, 1, 0.001)
							ADD_WIDGET_FLOAT_SLIDER("Cursor Width", fCursorY3, 0, 1, 0.001)
						STOP_WIDGET_GROUP()

						//Cursor corner 4					
						START_WIDGET_GROUP("Cursor corner 4")
							ADD_WIDGET_FLOAT_SLIDER("Cursor Height", fCursorX4, 0, 1, 0.001)
							ADD_WIDGET_FLOAT_SLIDER("Cursor Width", fCursorY4, 0, 1, 0.001)
						STOP_WIDGET_GROUP()

					STOP_WIDGET_GROUP()
					*/
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("PRINTER")
				ADD_WIDGET_BOOL("Print to debug file", bPrintWidgetValuesToDebug)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("MISC")
				ADD_WIDGET_BOOL("DISABLE FAILING DURING CHASE", bDisableFail)
			STOP_WIDGET_GROUP()
			
			SET_UBER_PARENT_WIDGET_GROUP(widgetGroup) 
		STOP_WIDGET_GROUP()

ENDPROC
#ENDIF
//*************************************************************************************************************************************************
//													:HELPER FUNCTIONS:
//*************************************************************************************************************************************************

//MISSION FLOW CONTROL

/// PURPOSE:
///    Advances or reverses the mission stage
///    Used in the clean up internal state of a mission stage
/// PARAMS:
///    bReverse - if true we go backwards through the mission stages
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	iMissionState = ENUM_TO_INT(eMissionStage)
	IF NOT bReverse
		eMissionStage = INT_TO_ENUM(MISSION_STAGE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionStage = INT_TO_ENUM(MISSION_STAGE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC

//General help

/// PURPOSE:
///    Checks to see if the player has injured a ped
///    Jump skip safe
/// PARAMS:
///    ped - The ped to be used in the checks 
/// RETURNS:
///    TRUE if the player has damaged the ped 
FUNC BOOL SAFE_MONITER_PED(PED_INDEX ped)
	IF NOT bJumpSkip
		IF DOES_ENTITY_EXIST(ped)				
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Jump skip safe is drivable check 
/// PARAMS:
///    vh - The vehicle to check
/// RETURNS:
///    TRUE if the vehicle is undriveable
FUNC BOOL SAFE_MONITER_CAR(VEHICLE_INDEX vh)
	IF NOT bJumpSkip
		IF DOES_ENTITY_EXIST(vh)
			IF NOT IS_VEHICLE_DRIVEABLE(vh) 
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)

	REQUEST_MODEL(_modname)
	IF i = 9999
		SK_PRINT_INT(_debugstring, i)
	ENDIF
	IF HAS_MODEL_LOADED(_modname)
		//SK_PRINT("MODEL LOADED")
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname)
	IF HAS_MODEL_LOADED(_modname)
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			
			IF DOES_ENTITY_EXIST(pedindex)
				UNLOAD_MODEL(model)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				UNLOAD_MODEL(model)
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0)
	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			objectindex = CREATE_OBJECT(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
				SET_ENTITY_HEADING(objectindex, dir)
				ACTIVATE_PHYSICS(objectindex)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Makes a ped enter stealth movement or stop stealth movement, checks to see if the ped is in stealth movement first
/// PARAMS:
///    ped - The ped to stop or start stealth movement on
///    bShouldDuck - If true the function makes the ped enter stealth mode if they aren't already doing it
PROC ALTER_STEALTH(PED_INDEX ped, BOOL bShouldDuck)
	
	IF bShouldDuck
		SET_PED_STEALTH_MOVEMENT(ped,TRUE)
	ELSE
		SET_PED_STEALTH_MOVEMENT(ped,FALSE)
	ENDIF

ENDPROC

/// PURPOSE:
///    Prints objective to the screen if the objective hasnt already been printed
/// PARAMS:
///    objstr - The text key to print
PROC PRINT_OBJ(String objstr)
	IF NOT bObjectiveShown
		PRINT_NOW(objstr, DEFAULT_GOD_TEXT_TIME,0)
		bObjectiveShown = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is at a given coord returns TRUE if the player is at the coords
/// PARAMS:
///    vpos - The position to check 
///    vradius - The size of the area to check
/// RETURNS:
///    TRUE if the player is at the coords
FUNC BOOL IS_PLAYER_AT_COORDS(VECTOR vpos, VECTOR vradius)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vpos, vradius)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

 
/// PURPOSE:
///    Checks to see if the player is near an entity returns TRUE if the player is near the entity
/// PARAMS:
///    entity - The enity to check
///    vradius - The size of the area to check 
/// RETURNS:
///    TRUE if the player is at the entity in question, within the vradius
FUNC BOOL IS_PLAYER_AT_ENTITY(ENTITY_INDEX entity, VECTOR vradius)
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(entity)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), entity, vradius)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: 
/// PURPOSE:
///    Checkes to see if a ped is in a given vehicle, also does alive checks. 
///    Returns TRUE if the given ped is in the given vehicle and they are all alive and well
/// PARAMS:
///    ped - Ped to check
///    veh - The vehicle to check they are in
/// RETURNS:
///    TRUE if the ped is in the vehicle and they are both alive and well
FUNC BOOL IS_SAFE_PED_IN_VEHICLE(PED_INDEX ped, VEHICLE_INDEX veh)
	IF veh != NULL
		IF IS_ENTITY_ALIVE(veh)
			IF IS_PED_UNINJURED(ped)
				IF IS_PED_IN_VEHICLE(ped,veh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if ped is in any vehichle and if vehichle is alive
/// PARAMS:
///    index - The ped to check
/// RETURNS:
///    TRUE if the ped is in a vehilce
FUNC  VEHICLE_INDEX GET_SAFE_ANY_VEHICLE_PED_IN(PED_INDEX index)
	IF IS_PED_IN_ANY_VEHICLE(index)
		VEHICLE_INDEX currentVhc = GET_VEHICLE_PED_IS_IN(index)
		IF IS_ENTITY_ALIVE(currentVhc)
			RETURN currentVhc
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

PROC DO_PLAYER_HOLDING_CAMERA()	
	
	IF iSeqPlayerHoldCam = 0
		REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
		REQUEST_MODEL(Prop_Pap_Camera_01)
		iSeqPlayerHoldCam = 1
	ELIF iSeqPlayerHoldCam = 1
		IF HAS_CLIP_SET_LOADED(sWeaponMoveClipsetFrank)
		AND HAS_MODEL_LOADED(Prop_Pap_Camera_01)
			iSeqPlayerHoldCam = 2
		ENDIF
	ELIF iSeqPlayerHoldCam = 2
		IF NOT DOES_ENTITY_EXIST(objCam)
			objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.0,0.0,10.0>>))	
			SET_ENTITY_VISIBLE(objCam,FALSE)
			SET_ENTITY_COLLISION(objCam,FALSE)
		ELSE
			IF HAS_CLIP_SET_LOADED(sWeaponMoveClipsetFrank)
				SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(),sWeaponMoveClipsetFrank)
			ELSE
				REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
				iSeqPlayerHoldCam = 1
			ENDIF
			
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam,PLAYER_PED_ID())
				IF IS_ENTITY_ATTACHED(objCam)	
					DETACH_ENTITY(objCam)
				ENDIF	
				
				//ATTACH_ENTITY_TO_ENTITY(objCam, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE)
				SET_ENTITY_VISIBLE(objCam,TRUE)
				ATTACH_ENTITY_TO_ENTITY(objCam, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
			
			ENDIF
			/*
			IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "rcmpaparazzo1ig_1", "cam_pos_hand_override")	
			AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT) //IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
			AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY) //IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())	
				//TASK_PLAY_ANIM(PLAYER_PED_ID(), "rcmpaparazzo1ig_1", "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
			ENDIF
			*/
		ENDIF
	ENDIF

ENDPROC

PROC SETUP_DYNAMIC_MIXING()

	IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_CHASE")
		IF IS_VEHICLE_OK(viChaseCars[POPPY_CAR])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(viChaseCars[POPPY_CAR], "PAPARAZZO_02_POPPY_CARS") 
		ENDIF	
		IF IS_VEHICLE_OK(viChaseCars[SHAGGER_CAR])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(viChaseCars[SHAGGER_CAR], "PAPARAZZO_02_CHASE_JUSTIN_CAR")
			//ADD_ENTITY_TO_AUDIO_MIX_GROUP(viChaseCars[SHAGGER_CAR], "PAPARAZZO_02_POPPY_CARS")
		ENDIF	
		IF IS_VEHICLE_OK(viChaseCars[BODYGUARD_CAR])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(viChaseCars[BODYGUARD_CAR], "PAPARAZZO_02_CHASE_SECURITY_CAR")
			//ADD_ENTITY_TO_AUDIO_MIX_GROUP(viChaseCars[SHAGGER_CAR], "PAPARAZZO_02_POPPY_CARS")
		ENDIF
		START_AUDIO_SCENE("PAPARAZZO_02_CHASE")
	ENDIF

ENDPROC

PROC REQUEST_HOTEL_MODELS()

	IF bHotelModelsRequested = FALSE
		REQUEST_MODEL(S_M_Y_BUSBOY_01)
		REQUEST_MODEL(S_F_M_MAID_01)
		REQUEST_MODEL(A_M_M_BEACH_02)
		REQUEST_CLIP_SET("move_f@sexy@a")
		REQUEST_WAYPOINT_RECORDING("Pap2_Maid")
		bHotelModelsRequested = TRUE
	ENDIF

ENDPROC

PROC SPAWN_HOTEL_PEDS()
	
	IF IS_PED_UNINJURED(mHotelStaff[1].piPed)
	AND IS_PED_UNINJURED(mHotelStaff[0].piPed)
		IF HAS_MODEL_LOADED(S_M_Y_BUSBOY_01)
		AND HAS_MODEL_LOADED(S_F_M_MAID_01)
		AND HAS_MODEL_LOADED(A_M_M_BEACH_02)
		AND HAS_CLIP_SET_LOADED("move_f@sexy@a")
			
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(mHotelStaff[1].piPed, "PAPARAZZO_HECTOR") 
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_INTRO")
				START_AUDIO_SCENE("PAPARAZZO_02_INTRO")
			ENDIF
			
			pedHotel[2] = CREATE_PED(PEDTYPE_MISSION,S_M_Y_BUSBOY_01,<< 15.1052, 336.1419, 109.9186 >>, 237.5365)    //busboy2
			SET_PED_COMPONENT_VARIATION(pedHotel[2],PED_COMP_HEAD,0,1,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[2],PED_COMP_TORSO,1,1,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[2],PED_COMP_LEG,0,0,0)
			TASK_START_SCENARIO_IN_PLACE(pedHotel[2],"WORLD_HUMAN_SMOKING")
			
			pedHotel[3] = CREATE_PED(PEDTYPE_MISSION,S_F_M_MAID_01,<<16.381054,314.905975,110.918671>>, -119.928345)    //maid2
			SET_PED_COMPONENT_VARIATION(pedHotel[3],PED_COMP_HEAD,0,0,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[3],PED_COMP_HAIR,0,1,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[3],PED_COMP_TORSO,0,2,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[3],PED_COMP_LEG,0,2,0)
			TASK_START_SCENARIO_IN_PLACE(pedHotel[3],"WORLD_HUMAN_MAID_CLEAN")
			
			pedHotel[4] = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEACH_02,<< 20.0922, 336.7069, 114.3914 >>, 154.9176)    //tourist lean
			SET_PED_COMPONENT_VARIATION(pedHotel[4],PED_COMP_HEAD,1,2,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[4],PED_COMP_TORSO,1,4,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[4],PED_COMP_LEG,0,5,0)
			TASK_START_SCENARIO_IN_PLACE(pedHotel[4],"WORLD_HUMAN_AA_COFFEE")
			
			pedHotel[5] = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEACH_02,<< -8.6062, 337.2177, 112.1610 >>, 206.6986)    //guy drinking and perving
			SET_PED_COMPONENT_VARIATION(pedHotel[5],PED_COMP_HEAD,0,0,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[5],PED_COMP_TORSO,1,2,0)
			SET_PED_COMPONENT_VARIATION(pedHotel[5],PED_COMP_LEG,0,0,0)
			TASK_START_SCENARIO_IN_PLACE(pedHotel[5],"WORLD_HUMAN_DRINKING")
			
			TASK_START_SCENARIO_AT_POSITION(mHotelStaff[1].piPed,"WORLD_HUMAN_SEAT_STEPS",<< 16.9840, 336.9486, 110.3190 >>, 157.6965)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHotelStaff[1].piPed,FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BUSBOY_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_F_M_MAID_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BEACH_02)
			
			SET_PED_MOVEMENT_CLIPSET(mHotelStaff[0].piPed,"move_f@sexy@a")
			
			bHotelPedsSpawned = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC DO_MAID_STUFF()

	IF bHotelPedsSpawned = TRUE	
		IF IS_PED_UNINJURED(mHotelStaff[0].piPed)	
			TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			IF iSeqMaid = 0	
				IF IS_PED_UNINJURED(mBuddy.piPed)	
				AND NOT bBentleyGotBevStuck	
					IF IS_ENTITY_IN_RANGE_ENTITY(mHotelStaff[0].piPed,mBuddy.piPed,12.0)
					OR IS_ENTITY_IN_RANGE_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID(),12.0)	
						//IF ARE_STRINGS_EQUAL(root, "PAP2_STAFF") 	
							iTimerMaidWalkingTalking = GET_GAME_TIMER()
							//ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(mHotelStaff[0].piPed, << 15.7379, 334.2191, 109.9186 >>)//MAID
							TASK_FOLLOW_NAV_MESH_TO_COORD(mHotelStaff[0].piPed,<<16.6655, 332.9557, 109.9194>>,1.3,-1,2.0,ENAV_ACCURATE_WALKRUN_START,351.8812)  //<< 15.7379, 334.2191, 109.9186 >>
							iSeqMaid = 1
						//ENDIF
					ENDIF
				ENDIF
			ENDIF	
			IF iSeqMaid = 1
				//IF NOT IS_STRING_NULL_OR_EMPTY(root)
				IF ARE_STRINGS_EQUAL(root, "PAP2_STAFF")
					IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 2
						//TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed,FALSE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mHotelStaff[0].piPed,<<16.6655, 332.9557, 109.9194>>,1.3,-1,2.0,ENAV_ACCURATE_WALKRUN_START,351.8812)
						iSeqMaid = 2
					ENDIF
				ENDIF
				//ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TASK_FOLLOW_NAV_MESH_TO_COORD(mHotelStaff[0].piPed,<<16.6655, 332.9557, 109.9194>>,1.3,-1,2.0,ENAV_ACCURATE_WALKRUN_START, 351.8812)
					iSeqMaid = 2
				ENDIF
			ENDIF
			IF iSeqMaid = 2 
				IF GET_IS_WAYPOINT_RECORDING_LOADED("Pap2_Maid")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND GET_GAME_TIMER() > iTimerMaidWalkingTalking + 5000
						TASK_FOLLOW_WAYPOINT_RECORDING(mHotelStaff[0].piPed,"Pap2_Maid",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
						iSeqMaid = 3 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MONITOR_HOTEL_PEDS()

	IF DOES_ENTITY_EXIST(pedHotel[5])	
		IF IS_PED_UNINJURED(pedHotel[5])
			UPDATE_AI_PED_BLIP(pedHotel[5],bsHotelPed5)
			IF IS_PED_RAGDOLL(pedHotel[5])
			OR HAS_PLAYER_THREATENED_PED(pedHotel[5])
			OR IS_PED_FLEEING(pedHotel[5])	
				bStaffThreatened = TRUE
				bSpotted = TRUE
			ENDIF
		ELSE
			bStaffThreatened = TRUE
			bSpotted = TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedHotel[1])	
		IF IS_PED_UNINJURED(pedHotel[1])
			UPDATE_AI_PED_BLIP(pedHotel[1],bsHotelPed1)
			IF IS_PED_RAGDOLL(pedHotel[1])
			OR HAS_PLAYER_THREATENED_PED(pedHotel[1])
			OR IS_PED_FLEEING(pedHotel[1])		
				bStaffThreatened = TRUE
				bSpotted = TRUE
			ENDIF
		ELSE
			bStaffThreatened = TRUE
			bSpotted = TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedHotel[3])	
		IF IS_PED_UNINJURED(pedHotel[3])
			UPDATE_AI_PED_BLIP(pedHotel[3],bsHotelPed3)
			IF IS_PED_RAGDOLL(pedHotel[3])
			OR HAS_PLAYER_THREATENED_PED(pedHotel[3])
			OR IS_PED_FLEEING(pedHotel[3])			
				bStaffThreatened = TRUE
				bSpotted = TRUE
			ENDIF
		ELSE
			bStaffThreatened = TRUE
			bSpotted = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC RELEASE_HOTEL_PEDS()

	SAFE_RELEASE_PED(mHotelStaff[0].piPed)
	SAFE_RELEASE_PED(mHotelStaff[1].piPed)
	SAFE_RELEASE_PED(pedHotel[0])
	SAFE_RELEASE_PED(pedHotel[1])
	SAFE_RELEASE_PED(pedHotel[2])
	SAFE_RELEASE_PED(pedHotel[3])
	SAFE_RELEASE_PED(pedHotel[4])
	SAFE_RELEASE_PED(pedHotel[5])
	
ENDPROC

PROC CLEANUP_HOTEL_PEDS()

	SAFE_DELETE_PED(mHotelStaff[0].piPed)
	SAFE_DELETE_PED(mHotelStaff[1].piPed)
	SAFE_DELETE_PED(pedHotel[0])
	SAFE_DELETE_PED(pedHotel[1])
	SAFE_DELETE_PED(pedHotel[2])
	SAFE_DELETE_PED(pedHotel[3])
	SAFE_DELETE_PED(pedHotel[4])
	SAFE_DELETE_PED(pedHotel[5])
	
ENDPROC

/// PURPOSE:
///    Spawns and sets up the ped with a debug name
/// PARAMS:
///    index - The PED_INDEX to write the newly created ped to 
///    pos - The postion to create the ped at
///    fDir - The heading to give the new ped
///    modelName - The model to create the ped with
///    i - Debug number for Debug name
///    _name - The Debug name 
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SETUP_PED(PED_INDEX &index, VECTOR pos, FLOAT fDir, MODEL_NAMES modelName, INT i, STRING _name ) 
	
	IF SPAWN_PED(index, modelName, pos, fDir)
		//SK_PRINT("SPAWNED")
	
		IF IS_PED_UNINJURED(index)
			TEXT_LABEL tDebugName = _name
			tDebugName += i
			//SK_PRINT(tDebugName)
			SET_PED_NAME_DEBUG(index, tDebugName)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Function to make beverly pull over and let the player out of the car
///    when the player has failed the car chase section of the mission
/// RETURNS:
///    TRUE when the player is outside of the car
FUNC BOOL PULLOVER_PLAYER_FAILED()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		
		IF iSeqBadCamMissionFail = 0
			iTimerBadCamMissionFail = GET_GAME_TIMER()
			iSeqBadCamMissionFail = 1
		ELIF iSeqBadCamMissionFail = 1
			IF GET_GAME_TIMER() > iTimerBadCamMissionFail + 3000
				RETURN TRUE
			ENDIF
		ENDIF
			
	ENDIF
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Cleans up the scripted video camera the player uses to film poppy
/// PARAMS:
///    walk - If false the player needs to be set to be visible 
PROC CLEANUP_CAMERA(BOOL walk)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF DOES_CAM_EXIST(camMain)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camMain)
			//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		ENDIF

		IF NOT walk
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
		ELSE
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
		ENDIF
	ENDIF
	CLEAR_TIMECYCLE_MODIFIER()
	CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE(-1) 
	CASCADE_SHADOWS_INIT_SESSION()
ENDPROC

/// PURPOSE:
///    Tells Poppy and Lover to play their exit anims for the sex scene 
PROC POPPY_SHAGGER_CHASE_PLAYER()
	IF IS_PED_UNINJURED(mPoppy.piPed) 
	AND IS_PED_UNINJURED(mShagger.piPed) 
		FREEZE_ENTITY_POSITION(mPoppy.piPed, FALSE)
		FREEZE_ENTITY_POSITION(mShagger.piPed, FALSE)
		//poppy_end_shagging_scene = CREATE_SYNCHRONIZED_SCENE(<<-30.990408,301.092346,111.681885>>,<<0,0,0>>)	
		//TASK_SYNCHRONIZED_SCENE (mPoppy.piPed, poppy_end_shagging_scene, "rcmpaparazzo_2", "shag_notice_player_poppy", SLOW_BLEND_IN, slow_BLEND_OUT )
		//TASK_SYNCHRONIZED_SCENE (mShagger.piPed, poppy_end_shagging_scene, "rcmpaparazzo_2", "shag_notice_player_a", SLOW_BLEND_IN, slow_BLEND_OUT )
		iTimerPoppyNoticePlayer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the body guard to attack the player
PROC BODYGUARDS_TO_CAR()
	IF IS_PED_UNINJURED(mBodyGuard.piPed) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		TASK_COMBAT_PED(mBodyGuard.piPed, PLAYER_PED_ID())
	ENDIF
ENDPROC

/// PURPOSE:
///    If no one is in the correct car then we warp them in to the correct car
///    - Could be functionised more
PROC WARP_EVERYONE_TO_CARS()
	IF NOT IS_SAFE_PED_IN_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
		IF IS_PED_UNINJURED(mBodyGuard.piPed)
		AND IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
			CLEAR_PED_TASKS_IMMEDIATELY(mBodyGuard.piPed)
			TASK_WARP_PED_INTO_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
		ENDIF
	ENDIF
	IF NOT IS_SAFE_PED_IN_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
		IF IS_PED_UNINJURED(mShagger.piPed)
		AND IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
			CLEAR_PED_TASKS_IMMEDIATELY(mShagger.piPed)
			TASK_WARP_PED_INTO_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
		ENDIF
	ENDIF	
	IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
		IF IS_PED_UNINJURED(mBuddy.piPed)
		AND IS_ENTITY_ALIVE(viPlayerCar)
			CLEAR_PED_TASKS_IMMEDIATELY(mBuddy.piPed)
			TASK_WARP_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    IF anyone (including the player) isnt in their correct
///    car then we warp them in to the car
///    As this is used in skips we use the clear immediately
PROC WARP_EVERYONE_TO_CARS_SKIP()
	
	IF bDoingEndCut = FALSE	
		IF NOT IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar) 
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viPlayerCar, VS_BACK_LEFT)
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mBuddy.piPed)
		IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
			CLEAR_PED_TASKS_IMMEDIATELY(mBuddy.piPed)
			SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mPoppy.piPed)
		IF NOT IS_SAFE_PED_IN_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR]) 
			CLEAR_PED_TASKS_IMMEDIATELY(mPoppy.piPed)
			SET_PED_INTO_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR])
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mShagger.piPed)
		IF NOT IS_SAFE_PED_IN_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
			CLEAR_PED_TASKS_IMMEDIATELY(mShagger.piPed)
			SET_PED_INTO_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(mBodyGuard.piPed)
		IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])
			IF NOT IS_SAFE_PED_IN_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
				CLEAR_PED_TASKS_IMMEDIATELY(mBodyGuard.piPed)
				SET_PED_INTO_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks to see if the player is in a given area
///    -Can be deleted not used anymore
/// PARAMS:
///    vPointA - First point in the area
///    vPointB - Second point in the area
/// RETURNS:
///    TRUE if the player is in the area
FUNC BOOL IS_PLAYER_IN_AREA(VECTOR vPointA, VECTOR vPointB)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPointA, vPointB)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is in a car and in a specified area
/// PARAMS:
///    vPointA - First point in the area 
///    vPointB - Second point in the area 
/// RETURNS:
///    TRUE if the player is in a car and in the area
FUNC BOOL PLAYER_ENTERED_AREA_IN_VEHICLE(VECTOR vPointA, VECTOR vPointB)
	IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_SAFE_ANY_VEHICLE_PED_IN(PLAYER_PED_ID()))
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPointA, vPointB, FALSE, TRUE, TM_IN_VEHICLE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays a ped recording back on a ped
///    Checks they are a alive and not already running a recording be for applying the recording
/// PARAMS:
///    _ped - The ped to play the recording on 
///    _recordingName - The name of the recording
/// RETURNS:
///    TRUE if the recording was applied 
FUNC BOOL PLAY_PED_RECORDING_ON_PED(PED_INDEX _ped, TEXT_LABEL _recordingName)
	IF IS_PED_UNINJURED(_ped) 
		IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(_ped)
			IF NOT IS_STRING_NULL(_recordingName)			
				IF GET_IS_WAYPOINT_RECORDING_LOADED(_recordingName)
					TASK_FOLLOW_WAYPOINT_RECORDING(_ped, _recordingName,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Loads a specified recording
/// PARAMS:
///    _recordingName - The name of the recording
/// RETURNS:
///    TRUE if the recording was loaded
FUNC BOOL LOAD_PED_RECORDING(TEXT_LABEL _recordingName)
	IF NOT IS_STRING_NULL(_recordingName)
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(_recordingName)
			REQUEST_WAYPOINT_RECORDING(_recordingName)
			IF  GET_IS_WAYPOINT_RECORDING_LOADED(_recordingName)
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Unloads a specific recording
/// PARAMS:
///    _recordingName - The name of the recording
///    _id - The id of the recording - this is used here because a create the file string by 
///    adding the recording name and the index together
PROC REMOVE_RECORDING(TEXT_LABEL _recordingName, INT _id = -1)
	IF _id <> -1
		_recordingName += _id
	ENDIF
	IF NOT IS_STRING_NULL(_recordingName)
		IF GET_IS_WAYPOINT_RECORDING_LOADED(_recordingName)
			REMOVE_WAYPOINT_RECORDING(_recordingName)
		ENDIF
	ENDIF
ENDPROC

 /// PURPOSE:
 ///    Creates a conversation at a specific point in a car recording (Time in recording > RecTime).
 ///     If there is already a conversation going on the convo will not play.
 ///     If it misses its window in the recording to play (Time in recording > fLimit) it will return true 
 ///     as if the convo had taken place.
 /// PARAMS:
 ///    ped - The ped to check is in the car
 ///    vhl - The car to check is playing a car recording and has the ped in it
 ///    RecTime - The time of the recording the conversation should play at.
 ///    fLimit - The cut off point where the time to play the conversation has expired
 ///    lineID - The line of dialogue to play
 /// RETURNS:
 ///    TRUE if the conversation has been created or if the convosation window has been missed
FUNC BOOL CREATE_CAR_CONVERSATION(PED_INDEX ped, VEHICLE_INDEX vhl, FLOAT RecTime, FLOAT fLimit, STRING lineID)
	//The car convosations can be overwritten by the bad filming lines hence the limited time it has to be played
	IF NOT bLineSet
		FLOAT fCurrent = 0.0
		IF IS_ENTITY_ALIVE(vhl)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vhl)
				fCurrent = GET_TIME_POSITION_IN_RECORDING(vhl)	
				IF IS_SAFE_PED_IN_VEHICLE(ped, vhl)
					IF fCurrent >= RecTime //Is time in recording greater than the RecTime
					AND fCurrent < fLimit // And is time in recording less than fLimit
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", lineID, CONV_PRIORITY_MEDIUM)
							RETURN TRUE
						ENDIF
					ELIF fCurrent > RecTime //If time in recording is greater than both RecTime and fLimit
					AND fCurrent > fLimit   //return true as if the convo was played - allows the conversation to continue normally 
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_POPPY_CHASE_DIALOGUE()

	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_PED_UNINJURED(mPoppy.piPed)	
			IF IS_ENTITY_IN_RANGE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),16.0)	
				IF IS_VEHICLE_OK(viPlayerCar)	
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
						IF fTimePosInRec > 50000
						AND fTimePosInRec < 60000
						//Pathetic fucking losers!
							IF bDialogueLosers = FALSE	
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_6", CONV_PRIORITY_MEDIUM)
									TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_fuckinlosers", NORMAL_BLEND_IN, -2, -1)  //,AF_SECONDARY | AF_UPPERBODY
									bDialogueLosers = TRUE	
									iTimerPoppyChaseLine = GET_GAME_TIMER()
									EXIT
								ENDIF
							ENDIF
						ENDIF
						IF fTimePosInRec > 72000
						AND fTimePosInRec < 84000
						//You're animals!  I hate you!
							IF bDialogueAnimals = FALSE	
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_5", CONV_PRIORITY_MEDIUM)
									TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_ih8u", NORMAL_BLEND_IN, -2, -1)
									bDialogueAnimals = TRUE	
									iTimerPoppyChaseLine = GET_GAME_TIMER()
									EXIT
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_RANGE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),12.0)
				//Give me that fucking camera!
				IF bDialogueGiveMe = FALSE	     
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_1", CONV_PRIORITY_MEDIUM)
						TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_gimmethtcam", NORMAL_BLEND_IN, -2, -1)
						bDialogueGiveMe = TRUE	
						iTimerPoppyChaseLine = GET_GAME_TIMER()
						EXIT
					ENDIF
				ENDIF
				
				IF GET_GAME_TIMER()	> iTimerPoppyChaseLine + 9000
					
					//I'll sue you if you post that shit!
					IF bDialogueIllSue = FALSE	
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_2", CONV_PRIORITY_MEDIUM)
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_illsueyou", NORMAL_BLEND_IN, -2, -1)
							bDialogueIllSue = TRUE	
							iTimerPoppyChaseLine = GET_GAME_TIMER()
							EXIT
						ENDIF
					ENDIF
					
					/*
						//Not that we were doing anything!
						IF bDialogueNotThat = FALSE	
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_3", CONV_PRIORITY_MEDIUM)
								TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_notdoinanythin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
								bDialogueNotThat = TRUE	
								iTimerPoppyChaseLine = GET_GAME_TIMER()
								EXIT
							ENDIF
						ENDIF
					*/
					
					//I'm a virgin!  He was helping me with homework!
					IF bDialogueVirgin = FALSE	
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_4", CONV_PRIORITY_MEDIUM)
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_imavirgin", NORMAL_BLEND_IN, -2, -1)
							bDialogueVirgin = TRUE	
							iTimerPoppyChaseLine = GET_GAME_TIMER()
							EXIT
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_OK(viPlayerCar)	
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
							IF fTimePosInRec > 80000	
								//It was just yoga!
								IF bDialogueYoga = FALSE	
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_POP", "PAP2_POP_7", CONV_PRIORITY_MEDIUM)
										TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_itwasyoga", NORMAL_BLEND_IN, -2, -1)
										bDialogueYoga = TRUE	
										iTimerPoppyChaseLine = GET_GAME_TIMER()
										EXIT
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Manage playing the first and last chase conversations 
///    The lines for filming are managed in MONITER_PED_ON_SCREEN
///    The fail lines can override any of these lines
PROC MANAGE_CHASE_CONVO()
	IF eChaseConvo <> CC_NONE //AND bObjectiveShown		
		//IF NOT bLineSet
			INT iConvo = ENUM_TO_INT(eChaseConvo)
			SWITCH eChaseConvo
				CASE CC_STAGE_1
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_CHASE1", CONV_PRIORITY_MEDIUM)
						eChaseConvo = INT_TO_ENUM(CHASE_CONVO_STAGE, iConvo + 1) //we progress the conversation state
						//SK_PRINT("TK************ SHOULD DO CONVO ************")
					ELSE
						//SK_PRINT("TK************ CONVO DIDNT TRIGGER ************")
					ENDIF
				BREAK
				
				CASE CC_STAGE_2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						IF fTimePosInRec > 11596.00
						AND fTimePosInRec < 14864.00
							IF NOT bDStraight	      //Straight road coming up, this is your chance to get a good shot of her. 
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_1", CONV_PRIORITY_MEDIUM)
									bDStraight = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 15284.760
						AND fTimePosInRec < 21515.00
							IF NOT bDTrunk	      //Man, you're not the one hanging out of the fucking trunk!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_2", CONV_PRIORITY_MEDIUM)
									bDTrunk = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 21655.770
						AND fTimePosInRec < 25915.00
							IF NOT bDWorse	      //Man, this is gonna get worse before it gets better.
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_3", CONV_PRIORITY_MEDIUM)
									bDWorse = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 27990.730
						AND fTimePosInRec < 28915.00
							IF NOT bDWhoa	      //Whoa whoa, what the hell, man!?
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_4", CONV_PRIORITY_MEDIUM)
									bDWhoa = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 31990.730
						AND fTimePosInRec < 32990.00
							IF NOT bDMFer	      //That motherfucker's right on top of us, dog!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_5", CONV_PRIORITY_MEDIUM)
									bDMFer = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 34769.360
						AND fTimePosInRec < 38562.850
							IF NOT bDRight	      //I'm taking a sharp right. Keep that camera on her!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_6", CONV_PRIORITY_MEDIUM)
									bDRight = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 45138.230
						AND fTimePosInRec < 46245.230
							IF NOT bDLeft	      //I'm gonna go left and cut across the intersection, hold on!  
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_7", CONV_PRIORITY_MEDIUM)
									bDLeft = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 49645.430
						AND fTimePosInRec < 51045.230
							IF NOT bDFuckU	      //Yeah fuck you too, man!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_8", CONV_PRIORITY_MEDIUM)
									bDFuckU = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 53299.310
						AND fTimePosInRec < 59399.230
							IF NOT bDMove	      //Move, come on, man! Hit the gas!  
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_9", CONV_PRIORITY_MEDIUM)
									bDMove = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 64999.310
						AND fTimePosInRec < 66320.230
							IF NOT bDCrazy	      //This crazy-ass bitch!  
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_10", CONV_PRIORITY_MEDIUM)
									bDCrazy = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 67320.230
						AND fTimePosInRec < 69648.890
							IF NOT bDTraffic	      //Shit, we've got traffic ahead
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_11", CONV_PRIORITY_MEDIUM)
									bDTraffic = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 71493.890
						AND fTimePosInRec < 73176.110
							IF NOT bDSteady	      //Going left, keep it steady.
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_12", CONV_PRIORITY_MEDIUM)
									bDSteady = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 75924.030
						AND fTimePosInRec < 77366.810
							IF NOT bDClose	      //Damn, that was close!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_13", CONV_PRIORITY_MEDIUM)
									bDClose = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 79366.810
						AND fTimePosInRec < 81397.300
							IF NOT bDJesus	      //Jesus, she's still on us!
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_14", CONV_PRIORITY_MEDIUM)
									bDJesus = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 83615.890
						AND fTimePosInRec < 85489.720
							IF NOT bDConst	      //Screw this, I'm going through the construction site.  
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds,sTextBlock,"PAP2_CHASE2", "PAP2_CHASE2_15", CONV_PRIORITY_MEDIUM)
									bDConst = TRUE
									eChaseConvo = CC_STAGE_3
								ENDIF
							ENDIF
						ENDIF
						
						IF fTimePosInRec > 90000.0
							eChaseConvo = CC_STAGE_3
						ENDIF
						/*
						SWITCH iCurrentLine
							CASE 0
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 11596.00, 14864.00,sChaseConvoLines[iCurrentLine] )//We got a long straight bit...
									iCurrentLine++ // progress the line index
									bLineSet = TRUE //set the line flage to true stopping any other conversations trying to start (unless they ignore the line set flag such as the failing dialogue)
								ENDIF
							BREAK
							
							CASE 1
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 15284.760, 20915.00, sChaseConvoLines[iCurrentLine] )//You're not the one hanging out...
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK
							
							CASE 2
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 21655.770, 25915.00,sChaseConvoLines[iCurrentLine] ) //This is goin' to get worse before it gets better.
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK	
							
							CASE 3
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 27990.730, 28915.00,sChaseConvoLines[iCurrentLine] ) //Woah woah, fuck!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK
							
							CASE 4
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 31990.730, 32990.00,sChaseConvoLines[iCurrentLine] ) //This dick is catching up fast!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK							
							
							CASE 5
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 34769.360, 38562.850,sChaseConvoLines[iCurrentLine] ) // Goin' to take a sharp right down...
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK
							
							
							CASE 6
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 45138.230, 46245.230,sChaseConvoLines[iCurrentLine] ) //Turning left then am goin to cut right across...
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 7
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 49645.430, 50045.230,sChaseConvoLines[iCurrentLine] ) //Yeah fuck you too bitch!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 8
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 53299.310, 59399.230,sChaseConvoLines[iCurrentLine] ) //Speed the fuck up!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 9
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 64999.310, 67320.230,sChaseConvoLines[iCurrentLine] ) //Crazy bitch!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 10
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 66823.250, 68648.890,sChaseConvoLines[iCurrentLine] ) // We got some traffic coming up.
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK

							CASE 11
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 71493.890, 73176.110,sChaseConvoLines[iCurrentLine] )//Taking this left, keep it steady. 
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
		
							CASE 12
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 75924.030, 77366.810,sChaseConvoLines[iCurrentLine] ) // Shit the bed, that was close!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
								
							BREAK	
							
							CASE 13
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 79366.810, 81397.300,sChaseConvoLines[iCurrentLine] ) //Fucking hell she's still on us!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 14
								IF CREATE_CAR_CONVERSATION(mBuddy.piPed, viPlayerCar, 83615.890, 85489.720,sChaseConvoLines[iCurrentLine] ) //Heading through the construction site...
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 15
								IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 88000.460, 89000.720,sChaseConvoLines[iCurrentLine] ) //Fly mother fucker, fly!
									iCurrentLine++
									bLineSet = TRUE
								ENDIF
							BREAK
							
							CASE 16
								//IF CREATE_CAR_CONVERSATION(PLAYER_PED_ID(), viPlayerCar, 92372.300, 93000.720,sChaseConvoLines[iCurrentLine] ) //Boom! Stay down!
									iCurrentLine++
									bLineSet = TRUE
								//ENDIF
							BREAK
							
							CASE 17
								eChaseConvo = INT_TO_ENUM(CHASE_CONVO_STAGE, iConvo + 1)
							BREAK
						ENDSWITCH
						*/
					ENDIF
				BREAK

				CASE CC_STAGE_3
					IF IS_ENTITY_ALIVE(viPlayerCar)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
							IF GET_TIME_POSITION_IN_RECORDING(viPlayerCar) >= 94365.810
								IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_CHASE3", CONV_PRIORITY_VERY_HIGH)
								OR GET_TIME_POSITION_IN_RECORDING(viPlayerCar) >= 100000.0
									REPLAY_RECORD_BACK_FOR_TIME(10.0, 3.0, REPLAY_IMPORTANCE_LOW)
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									bStartScriptCineCam = TRUE
									eChaseConvo = INT_TO_ENUM(CHASE_CONVO_STAGE, iConvo + 1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK		
				
				DEFAULT
				BREAK
			
			ENDSWITCH
		//ELSE
			//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//	bLineSet = FALSE
			//ENDIF
		//ENDIF
	ENDIF
	/*
	IF eChaseConvo < CC_STAGE_3
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
			IF GET_TIME_POSITION_IN_RECORDING(viPlayerCar) >= 95000.0  //something went wrong so bail
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CPRINTLN(DEBUG_MISSION, "SOMETHING WENT WRONG WITH CONVO - BAIL!")
				eChaseConvo = CC_STAGE_3
			ENDIF
		ENDIF
	ENDIF
	*/
ENDPROC




/// PURPOSE:
///    Checks to see if the maid dialogue has reached the point she should hang up her phone
///    Then hangs up her phone and tells her to go to a position by hector
PROC MONITER_MAID_PHONE()
	IF GET_GAME_TIMER() > iTimerMaidUsingPhone + 13000
	AND GET_GAME_TIMER() < iTimerMaidUsingPhone + 15000
		IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
			IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				SEQUENCE_INDEX Seq
				OPEN_SEQUENCE_TASK(Seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 15.7815, 333.5496, 109.9190 >>,1.2, 30000)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(mHotelStaff[0].piPed, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
			ENDIF
		ENDIF
	ENDIF
ENDPROC



////*****************************************************************************////
////**************************MISSION STATE FUNCTIONS****************************////
////*****************************************************************************////
/// PURPOSE:
///    Checks that the player can see a point in the world
///    If the player is close enough to the point before checking that they can see it
/// PARAMS:
///    vPos - The position to check
///    fRadius - The size of the sphere to check the player can see
///    distance - The max distance the player can be from the 
///    position for the the visibility check to be considered 
/// RETURNS:
///    TRUE if the player is close enough to the point and can see the point
FUNC BOOL CAN_PLAYER_SEE_POINT(VECTOR vPos, FLOAT fRadius = 5.0, INT distance = 120)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(PLAYER_PED_ID()), vPos) <= distance
			IF IS_SPHERE_VISIBLE(vPos, fRadius)      
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Moniters what point in the route the players car is
	///    Can draw a debug sphere around the points on the route
	PROC MONITER_ROUTE()//not really needed unless re-recording the route
		IF IS_ENTITY_ALIVE(mBuddy.piPed)
			IF IS_ENTITY_AT_COORD(mBuddy.piPed, vChaseRoute[iCurrentChasePos], vRouteCheckSize)
				iCurrentChasePos++
				IF iCurrentChasePos = MAX_CHASE_POINTS
					iCurrentChasePos = -1
				ELSE
					//SAFE_REMOVE_BLIP(biGOTO)
					//ADD_BLIP_LOCATION( biGOTO, vChaseRoute[iCurrentChasePos], FALSE)
				ENDIF
			ELSE
				//SK_PRINT("DRAW SPHERE")
				IF iCurrentChasePos <> -1
					//DRAW_DEBUG_SPHERE(vChaseRoute[iCurrentChasePos], 5, 0, 255, 0, 100)
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

/// PURPOSE:
///    Checks to see if a peds head is in the middle of the screen 
/// PARAMS:
///    ped - The ped who's head we are going to check is on screen 
///    fScreenX - The X screen position to write the X screen position of the peds head
///    fScreenY - The Y screen position to write the Y screen position of the peds head
/// RETURNS:
///    TRUE if the peds head is on screen 
FUNC BOOL IS_PED_IN_MIDDLE_OF_SCREEN(PED_INDEX ped, FLOAT &fScreenX , FLOAT &fScreenY )
	IF IS_PED_UNINJURED(ped)
		INT i = GET_PED_BONE_INDEX(ped, BONETAG_HEAD)
		
		IF i <> -1
			VECTOR pos = GET_WORLD_POSITION_OF_ENTITY_BONE(ped,i)			

			IF CAN_PLAYER_SEE_POINT(pos,2, 150)
				GET_SCREEN_COORD_FROM_WORLD_COORD(pos, fScreenX, fScreenY)
				IF DOES_CAM_EXIST(camMain)
				
					IF GET_CAM_FOV(camMain) < 10
					AND eMissionStage = MS_CAMERA_TUTORIAL
						IF fScreenX > 0.2 
							AND fScreenX <0.8
							AND fScreenY > 0.2 
							AND fScreenY < 0.8
							RETURN TRUE
						ENDIF						
					ELSE
						IF fScreenX > 0.35 
							AND fScreenX <0.65 
							AND fScreenY > 0.35 
							AND fScreenY < 0.65
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Moniters whether the player and poppy are with in 10 meters of each other 
///    and then gives her a driveby task so she flips the player the bird
PROC MONITER_POPPY_DRIVEBY()
	IF NOT bDoingDriveBy
		IF GET_DISTANCE_BETWEEN_ENTITIES(mPoppy.piPed, PLAYER_PED_ID()) < 10
			IF IS_PED_UNINJURED(mPoppy.piPed)
			AND IS_PED_UNINJURED(PLAYER_PED_ID())
				IF NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_fuckinlosers")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_gimmethtcam")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_ih8u")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_illsueyou")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_imavirgin")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_itwasyoga")
				AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_notdoinanythin")
					IF IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(mPoppy.piPed)
						CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(mPoppy.piPed)
					ENDIF
					TASK_DRIVE_BY(mPoppy.piPed, PLAYER_PED_ID(), NULL, vSafeVec, 11,100, TRUE)//viPlayerCar
					iTimerDrivebyReset = GET_GAME_TIMER()
					bDoingDriveBy = TRUE //we are doing a drive by
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF GET_GAME_TIMER()	> iTimerDrivebyReset + 10000
			IF IS_PED_UNINJURED(mPoppy.piPed)
				IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_DRIVE_BY) = PERFORMING_TASK	
					CLEAR_PED_TASKS(mPoppy.piPed)
					bDoingDriveBy = FALSE //reset doing drive by flag
				ENDIF
				IF IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(mPoppy.piPed)
					CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(mPoppy.piPed) //if there is already a driveby task present remove it
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Increase the filming stats for the face recog box
///    the individual colours are changed over time till
///    the box is green. 
///    If bCenter and bZoomed are both true the box locks on quicker
/// PARAMS:
///    bCenter - The camera is centered on targets face
///    bZoomed - The camera is zoomed in - only counts if the camera is centered as well
PROC INCREASE_FILMING_STATS(BOOL bCenter,BOOL bZoomed)
	
	//FLOAT fRecog
	//INT iRecogPercent
	
	IF bCenter
		IF bZoomed
			mBoxCol.G += NUM_ONSCREEN_ZOOM_CENTER
			mBoxCol.B += NUM_ONSCREEN_ZOOM_CENTER 
			mBoxCol.R -= NUM_ONSCREEN_ZOOM_CENTER
		ELSE
			mBoxCol.G += NUM_ONSCREEN_CENTER
			mBoxCol.B += NUM_ONSCREEN_CENTER 
			mBoxCol.R -= NUM_ONSCREEN_CENTER
		ENDIF
		IF mBoxCol.R <= 0
			mBoxCol.R = 0
		ENDIF
		
		IF mBoxCol.G >= 255
			mBoxCol.G = 255
		ENDIF	
		
		IF mBoxCol.B >= 123
			mBoxCol.B = 123
		ENDIF
		
	ELSE
		mBoxCol.G += NUM_ONSCREEN
		mBoxCol.B += NUM_ONSCREEN 
		mBoxCol.R -= NUM_ONSCREEN

		IF mBoxCol.G >= 255
			mBoxCol.G = 255
		ENDIF	
		
		IF mBoxCol.B >= 123
			mBoxCol.B = 123
		ENDIF
		
		IF mBoxCol.R <= 0
			mBoxCol.R = 0
		ENDIF
	ENDIF
	
	IF mBoxCol.G = 255
		++iFilmingStats
	ENDIF
	/*
	IF eMissionStage = MS_CAR_CHASE
		
		fRecog = TO_FLOAT(mBoxCol.G) 
		
		fRecog /= 255
		fRecog *= 100
		
		iRecogPercent = ROUND(fRecog) 
		
		IF iRecogPercent > iMaxRecogPercent
			iMaxRecogPercent = iRecogPercent
			//PRINTNL()
			//PRINTINT(iMaxRecogPercent)
		ENDIF
		
	ENDIF
	*/
		
ENDPROC

/// PURPOSE:
///    Reduce the filming stats for the face recog box tag
///    The colour of the box is turned to red
PROC DECREASE_FILMING_STATS()
	//fOffScreen += OFF_SCREEN
	mBoxCol.G -= NUM_OFF_SCREEN //Take away green
	mBoxCol.B -= NUM_OFF_SCREEN //Take away blue
	mBoxCol.R += NUM_OFF_SCREEN //add red
	
	//clamp
	IF mBoxCol.R >= 255
		mBoxCol.R = 255
	ENDIF

	//clamp
	IF mBoxCol.G <= 0
		mBoxCol.G = 0
	ENDIF
	
	//clamp
	IF mBoxCol.B <= 0
		mBoxCol.B = 0
	ENDIF
	
	++iBadFilming

ENDPROC

/// PURPOSE:
///    Reset the box tag to red
PROC RESET_BOX_COL()
	mBoxCol.R  = 255
	mBoxCol.G  = 0
	mBoxCol.B  = 0
ENDPROC

/// PURPOSE:
///    Moniters whether a ped is on screen or not and adjusts their face box accordingly
/// PARAMS:
///    ped - The ped who's face we want to moniter
///    bOnScreen - The bool to write to with the result of IS_PED_IN_MIDDLE_OF_SCREEN
///    bChase - Are we doing the car chase
PROC MONITER_PED_ON_SCREEN(PED_INDEX ped, BOOL &bOnScreen, BOOL bChase = TRUE)
	FLOAT fScreenX = 0.0
	FLOAT fScreenY = 0.0
	
	bOnscreen = IS_PED_IN_MIDDLE_OF_SCREEN(ped, fScreenX, fScreenY) //Checks the ped is in screen 
	
	IF bOnscreen //it is on screen
		IF bChase
			//MONITER_POPPY_DRIVEBY()
		ENDIF

		IF DOES_CAM_EXIST(camMain)
			IF bChase
				IF GET_CAM_FOV(camMain) < 13
					INCREASE_FILMING_STATS(TRUE, TRUE)
					--iCounterNotOnScreen
					--iCounterNotOnScreen
				ELSE
					INCREASE_FILMING_STATS(TRUE, FALSE)
					--iCounterNotOnScreen
				ENDIF
			ELSE
				IF GET_CAM_FOV(camMain) < 13
					INCREASE_FILMING_STATS(TRUE, TRUE)
				ELSE
					INCREASE_FILMING_STATS(TRUE, FALSE)
				ENDIF
			ENDIF
		ENDIF		
	ELSE //its not on screen
		IF IS_PED_UNINJURED(ped)
			
			IF IS_ENTITY_ON_SCREEN(ped)
				INCREASE_FILMING_STATS(FALSE, FALSE)
				--iCounterNotOnScreen
				/*
				FLOAT fSX
				FLOAT fSY
				IF GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(ped), fSX, fSY)
					IF fSX > 0.4
					//AND fSX < 0.6
					AND fSY > 0.4
					//AND fSY < 0.6
						--iCounterNotOnScreen
					ENDIF
				ENDIF
				*/
			ELSE
				DECREASE_FILMING_STATS()
				++iCounterNotOnScreen
			ENDIF
		ENDIF
	ENDIF
	
	IF iCounterNotOnScreen < 0
		iCounterNotOnScreen = 0
	ENDIF
	
	IF iCounterNotOnScreen > iFramesBeforeFilmingFail + 1
		iCounterNotOnScreen = iFramesBeforeFilmingFail + 1
	ENDIF
	
	//PRINTINT(iCounterNotOnScreen)
	//PRINTNL()
	
ENDPROC

/// PURPOSE:
///    Check the film time is at its limit
/// PARAMS:
///    Limit - The time you need to film for
/// RETURNS:
///    TRUE if film time is over the limit
FUNC BOOL IS_PIC_UNIQUE(INT Limit = 50)
	IF iFilmTime > Limit
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays a sound from an entity
/// PARAMS:
///    i - The index of the Sound struct to access
///    Origin - The entity to play the sound from
PROC PLAY_CHASE_SOUND_ENTITY(INT i, ENTITY_INDEX Origin)
	IF IS_ENTITY_ALIVE(Origin)
		PLAY_SOUND_FROM_ENTITY(-1, mFinalChaseSound[i].sSoundName, Origin, sSoundSet)
		mFinalChaseSound[i].bPlayed = TRUE
	ENDIF
ENDPROC


/// PURPOSE:
///    Triggers the sound needed at the correct time in the car recording
/// PARAMS:
///    iChaseProgress - The current time in to the recording 

//VECTOR vPoppyForward

PROC MONITER_SOUNDS(INT iChaseProgress)
	INT i
	FOR i = 0 TO (MAX_SOUNDS - 1)	
		IF NOT mFinalChaseSound[i].bPlayed
			IF iChaseProgress >= mFinalChaseSound[i].iExecuteTime
				SWITCH mFinalChaseSound[i].eOrigin
					CASE CSO_POPPY
						//IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<100.02, -397.11, 40.77>>,5,prop_pipes_02b)
							//vPoppyForward = GET_ENTITY_FORWARD_VECTOR(viChaseCars[POPPY_CAR])
							//objPipe1 = GET_CLOSEST_OBJECT_OF_TYPE(<<100.02, -397.11, 40.77>>,5,prop_pipes_02b)
							//APPLY_FORCE_TO_ENTITY(objPipe1,APPLY_TYPE_EXTERNAL_FORCE,vPoppyForward,<<0,0,0>>,0,TRUE,TRUE,TRUE)
							SMASH_VEHICLE_WINDOW(viChaseCars[POPPY_CAR],SC_WINDSCREEN_FRONT)
							SET_VEHICLE_DAMAGE(viChaseCars[POPPY_CAR],<<99.92, -396.72, 40.74>>,200, 200, FALSE)
							SET_VEHICLE_TYRE_BURST(viChaseCars[POPPY_CAR],SC_WHEEL_CAR_FRONT_LEFT)
							SET_VEHICLE_TYRE_BURST(viChaseCars[POPPY_CAR],SC_WHEEL_CAR_FRONT_RIGHT)
							TRIGGER_MUSIC_EVENT("PAP2_STOP")	
							SK_PRINT("++++++ APPLYING FORCE TO PIPES ++++++")
						//ENDIF
						PLAY_CHASE_SOUND_ENTITY(i, mPoppy.piPed)
					BREAK

					CASE CSO_PLAYER
						//PLAY_CHASE_SOUND_ENTITY(i, PLAYER_PED_ID())
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

/// PURPOSE:
///    Sets up the FPS style filming camera 
/// PARAMS:
///    walk - Should it be the walking camera or the car chase camera
/// RETURNS:
///    TRUE if the camera was created
FUNC BOOL SETUP_CAMERA(BOOL walk)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF walk
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<<-33.4707, 318.6031, 113.3355>>, <<-1.6576, 0.0000, -170.9825>>, 45, TRUE)
				IF eMissionStage <> MS_CAMERA_TUTORIAL
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

				IF DOES_CAM_EXIST(camMain)
					CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(TRUE)
					CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE(16.0)
					RETURN TRUE
				ENDIF
			ELSE
				CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_MODE(TRUE)
				CASCADE_SHADOWS_SET_DYNAMIC_DEPTH_VALUE(16.0)
				RETURN TRUE
			ENDIF
		ELSE	
			IF NOT DOES_CAM_EXIST(camMain)
				IF IS_ENTITY_ALIVE(viPlayerCar) AND IS_PED_UNINJURED(PLAYER_PED_ID())
					camMain = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar, vCameraOffset), <<0,0, -GET_ENTITY_HEADING(viPlayerCar)>>, 50, TRUE)			
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					IF DOES_CAM_EXIST(camMain)
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(viPlayerCar) AND IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_CAM_PARAMS(camMain,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar, vCameraOffset), <<0,0, -GET_ENTITY_HEADING(viPlayerCar)>>, 50)			
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: WIP dof/motion blur effects
PROC DO_CAM_DOF_EFFECT()
	
	/*
	fCamFov = GET_CAM_FOV(camMain)
	IF eMissionStage = MS_CAR_CHASE	
		IF fCamFov >= 45
		OR fCamFov <= 25
			fCamZoomStrength = 0
		ENDIF
	ELSE
		IF fCamFov >= 45
		OR fCamFov <= 7.5
			fCamZoomStrength = 0
		ENDIF
	ENDIF
	IF fCamZoomStrength > 0.4
		fCamZoomStrength = 0.4
	ENDIF
	IF fCamZoomStrength < 0
		fCamZoomStrength = 0
	ENDIF
	*/
	/*
	SET_CAM_MOTION_BLUR_STRENGTH(camMain,COSINE_INTERP_FLOAT(GET_CAM_MOTION_BLUR_STRENGTH(camMain),fCamZoomStrength/3,0.15))
	SET_CAM_NEAR_DOF(camMain,0)
	SET_CAM_FAR_DOF(camMain,COSINE_INTERP_FLOAT(GET_CAM_FAR_DOF(camMain),GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mPoppy.piPed) + 10.0,0.2))
	SET_CAM_DOF_STRENGTH(camMain,COSINE_INTERP_FLOAT(GET_CAM_DOF_STRENGTH(camMain),fCamZoomStrength,0.2))
	SET_USE_HI_DOF()
	*/
	
	IF bCamZooming
		fZoomAlpha = fZoomAlpha - 0.2
	ELSE
		fZoomAlpha = fZoomAlpha + 0.1
	ENDIF
	IF fZoomAlpha > 1.0
		fZoomAlpha = 1.0
	ENDIF
	IF fZoomAlpha < 0.0
		fZoomAlpha = 0.0
	ENDIF
		
	SET_CAM_NEAR_DOF(camMain,2)
	SET_CAM_FAR_DOF(camMain,COSINE_INTERP_FLOAT(20.0,60.0,fZoomAlpha))
	SET_CAM_DOF_STRENGTH(camMain,0.5)
	SET_USE_HI_DOF()

ENDPROC

/// PURPOSE:
///    Caps the pitch of the camera
/// PARAMS:
///    fPitch - The value of pitch to cap
///    min - the minimum value fPitch can be before its capped
///    max - the maximum value fPitch can be before its capped
/// RETURNS:
///    the capped pitch
FUNC FLOAT CAP_PITCH(FLOAT fPitch, FLOAT min= -50.0, FLOAT max= 40.0)
	IF fPitch < min
		fPitch = min
	ENDIF
	
	IF fPitch > max
		fPitch = max
	ENDIF 

	return fPitch
ENDFUNC

/// PURPOSE:
///    Caps the yaw of the camera 
/// PARAMS:
///    fYaw - The value of yaw to cap
///    min - the minimum value fYaw can be before its capped 
///    max -the maximum value fYaw can be before its capped
/// RETURNS:
///    The capped value of fYaw
FUNC FLOAT CAP_YAW(FLOAT fYaw, FLOAT min= -50.0, FLOAT max= 20.0)
	IF fYaw < min
		SK_PRINT("YAW CAPPED ----------")
		fYaw = min
	ENDIF
	
	IF fYaw > max
		SK_PRINT("YAW CAPPED ++++++++++")
		fYaw = max
	ENDIF
	
	RETURN fYaw
ENDFUNC

 /// PURPOSE:
 ///    Caps the angle the player can turn the camera
 /// PARAMS:
 ///    fVal - The value to be capped
 /// RETURNS:
 ///    The capped value
FUNC FLOAT CAP_TURNING_VALUE(FLOAT fVal)
	IF fVal < -VIEW_ANGLE
		fVal = -VIEW_ANGLE
	ENDIF
	
	IF fVal > VIEW_ANGLE
		fVal = VIEW_ANGLE
	ENDIF
	
	RETURN fVal
ENDFUNC

/// PURPOSE:
///    uses input from the analog sticks to addjust the way the camera is looking
/// PARAMS:
///    vCam - The VECTOR to write the modified look at to 
///    fFOV - The FLOAT to write the modified Zoom value
PROC DO_CAM_CONTROLS(VECTOR &vCam, FLOAT &fFOV, BOOL bIsOnFootCam = FALSE)
	INT iStickPitch, iStickYaw, iIgnore, iZoom
	FLOAT fStickPitch, fStickYaw

	FLOAT fZoomSpeed
	
	FLOAT fMaxZoom
	fMaxZoom = 25
	
	IF bIsOnFootCam
		fMaxZoom = 7.5
	ENDIF

	IF IS_SNIPER_INVERTED()
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iStickYaw,iStickPitch,iIgnore,iZoom)
	ELSE
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iIgnore,iZoom,iStickYaw,iStickPitch)
	ENDIF
	IF NOT IS_LOOK_INVERTED()
		iStickPitch *= -1
	ENDIF	
	
	IF NOT bIsOnFootCam
		IF IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar,<<-2,-2.6,0>>),1.0) 
			bRotRight = TRUE
			//SK_PRINT("FUCK YEAH")
		ENDIF
	ENDIF

	// Use the sniper zoom so the zoom works on mousewheel
	iZoom = FLOOR(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM) * 127)
	
	IF (iZoom < -DEAD_ZONE_CAM OR iZoom > DEAD_ZONE_CAM)
	//IF iZoom = 
		fFOV += (TO_FLOAT(iZoom) / CAM_FOV_RATE) * TIMESTEP()
		
		IF fFOV < fMaxZoom //The smaller the FOV the more zoomed in we are
			fFOV = fMaxZoom
		ELIF fFOV > 45
			fFOV = 45
		ENDIF
	ENDIF

	fZoomSpeed = ABSF(TO_FLOAT(iZoom) / 127)
	
	#IF NOT IS_JAPANESE_BUILD
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
			FLOAT fZoomamount
			fZoomamount = fFOV - 7.5
			fZoomamount = fZoomamount/37.5
			IF fZoomamount > 0.6
				fZoomamount = 0.6
			ENDIF
			
			//SET_VARIABLE_ON_SYNCH_SCENE_AUDIO("CAMERA_USED",fZoomamount) //it never worked

		ENDIF
	#ENDIF
		
	IF fZoomSpeed < 0
		fZoomSpeed = 0
	ENDIF
	IF fZoomSpeed > 1
		fZoomSpeed = 1
	ENDIF

	IF fCamFov > fFOV
		IF iZoomFunc = 1	
			IF NOT HAS_SOUND_FINISHED(iZoomSound)
				STOP_SOUND(iZoomSound)
			ENDIF
		ENDIF
		SET_VARIABLE_ON_SOUND(iZoomSound,"ZOOM_FUNCTION",0)
		SET_VARIABLE_ON_SOUND(iZoomSound,"ZOOM_SPEED",fZoomSpeed)
		IF HAS_SOUND_FINISHED(iZoomSound)	
			PLAY_SOUND_FRONTEND(iZoomSound,"ZOOM","PAPARAZZO_02_SOUNDSETS")
			iZoomFunc = 0
		ENDIF
		bCamZooming = TRUE
	ELIF fCamFov < fFOV
		IF iZoomFunc = 0	
			IF NOT HAS_SOUND_FINISHED(iZoomSound)
				STOP_SOUND(iZoomSound)
			ENDIF
		ENDIF
		SET_VARIABLE_ON_SOUND(iZoomSound,"ZOOM_FUNCTION",1)
		SET_VARIABLE_ON_SOUND(iZoomSound,"ZOOM_SPEED",fZoomSpeed)
		IF HAS_SOUND_FINISHED(iZoomSound)	
			PLAY_SOUND_FRONTEND(iZoomSound,"ZOOM","PAPARAZZO_02_SOUNDSETS")
			iZoomFunc = 1
		ENDIF
		bCamZooming = TRUE
	ELIF fCamFov = fFOV
		IF NOT HAS_SOUND_FINISHED(iZoomSound)
			STOP_SOUND(iZoomSound)
		ENDIF
		bCamZooming = FALSE
	ENDIF

	IF (iStickYaw < -DEAD_ZONE_CAM OR iStickYaw > DEAD_ZONE_CAM OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) )
		
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			fStickYaw = GET_DISABLED_CONTROL_UNBOUND_NORMAL( PLAYER_CONTROL, INPUT_SCALED_LOOK_LR ) * CAM_MOUSE_TURN_RATE								
		ELSE
			fStickYaw = ((TO_FLOAT(iStickYaw) * fFOV) * CAM_TURN_RATE) * TIMESTEP()
		ENDIF
		//
		fCamHeadingMod += fStickYaw
		//fCamHeadingMod  = CAP_YAW(fCamHeadingMod)
		
		//SF_PRINT_FLOAT("fCamHeadingMod*********************= ", fCamHeadingMod)
		
		fCamHeadingMod = CAP_TURNING_VALUE(fCamHeadingMod)
		/*SF_PRINT_FLOAT("fMaxTurning------------=== ", fMaxTurning)
		SF_PRINT_FLOAT("fMinTurning--------------=== ", fMinTurning)
		SF_PRINT_FLOAT("fCamHeadingMod-------------=== ", fCamHeadingMod)
		*/
		//SF_PRINT_FLOAT("CAPPED YAW ++++++++++++++++++++++++++++++++ ", fCamHeadingMod)
	
	ENDIF
	
	IF (iStickPitch < -DEAD_ZONE_CAM OR iStickPitch > DEAD_ZONE_CAM OR IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL))
	
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			fStickPitch = GET_DISABLED_CONTROL_UNBOUND_NORMAL( PLAYER_CONTROL, INPUT_SCALED_LOOK_UD ) * -CAM_MOUSE_TURN_RATE							
		ELSE
			fStickPitch = ((TO_FLOAT(iStickPitch) * fFOV) * CAM_TURN_RATE) * TIMESTEP()
		ENDIF
			
		//fStickPitch = CAP_PITCH(fStickPitch)
		//SF_PRINT_FLOAT("CSTICK PITCH ", fStickPitch)
		fCamPitchMod += fStickPitch
		//SF_PRINT_FLOAT("fCamPitchMod1 ", fCamPitchMod)
		fCamPitchMod = CAP_PITCH(fCamPitchMod)
		//SF_PRINT_FLOAT("fCamPitchMod2 ", fCamPitchMod)	
	ENDIF
	//SF_PRINT_FLOAT("WORLD YAW----------------------------------=== ", fWorldYaw)
	IF eMissionStage = MS_CAR_CHASE
		IF bUseLockedCam
			vCam += <<fCamPitchMod,0, -fCamHeadingMod>>
		ELSE
			vCam += <<fStickPitch,0, -fStickYaw>>
		ENDIF
	ELSE
		vCam += <<fStickPitch,0, -fStickYaw>>
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the cameras position and lookat for the on foot style camera
PROC MANAGE_WALK_CAM()

	SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)

	VECTOR vCameraRot = GET_CAM_ROT(camMain)
	
	FLOAT fCameraFOV = GET_CAM_FOV(camMain)
	
	DO_CAM_CONTROLS(vCameraRot, fCameraFOV, TRUE)
	vCameraRot.x =  CAP_PITCH(vCameraRot.x)
	//SET_ENTITY_HEADING(PLAYER_PED_ID(), vCameraRot.z)
	//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	SET_CAM_PARAMS(camMain,
	<< -33.4707, 318.6031, 113.3355 >>,
	<<vCameraRot.x, GET_ENTITY_ROLL(PLAYER_PED_ID()), vCameraRot.z>>,
	fCameraFOV)

ENDPROC

FUNC VECTOR GET_VECTOR_FROM_HEADING(FLOAT fHeading)
	RETURN <<SIN(fHeading), COS(fHeading), 0.0>>
ENDFUNC

/// PURPOSE:
///    Updates the cam position and lookat during the chase 
PROC MANAGE_CAR_CAM()
	VECTOR vCameraRot = GET_CAM_ROT(camMain)
	FLOAT fCameraFOV = GET_CAM_FOV(camMain)
	
	FLOAT fRevCarHeading = GET_ENTITY_HEADING(viPlayerCar) - 180
	
	VECTOR vCarForward 

	IF IS_ENTITY_ALIVE(viPlayerCar)
		vCarForward = GET_ENTITY_FORWARD_VECTOR(viPlayerCar)
		PROCESS_ENTITY_ATTACHMENTS(viPlayerCar)
	ENDIF

	VECTOR newf
	vCarForward.x *= -1.0
	vCarForward.y *= -1.0
	vCarForward.z *= -1.0
	newf.z = (GET_HEADING_FROM_VECTOR_2D(vCarForward.x, vCarForward.y)) //+ (3.142 / 2)

	FLOAT fMaxTurning, fMinTurning
	
	fMaxTurning = newf.z + VIEW_ANGLE
	fMinTurning = newf.z - VIEW_ANGLE
	
	VECTOR vCam	= GET_VECTOR_FROM_HEADING(vCameraRot.z)
	VECTOR vCar	= GET_VECTOR_FROM_HEADING(fRevCarHeading)
	FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vCam.x,vCam.y,vCar.x,vCar.y)
		
	IF fAngle >= VIEW_ANGLE
		//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_SCRIPT_RIGHT_AXIS_X)
		//DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_RRIGHT)
		/*
		VECTOR vCarMinLock	= GET_VECTOR_FROM_HEADING(fRevCarHeading - VIEW_ANGLE)
		VECTOR vCarMaxLock	= GET_VECTOR_FROM_HEADING(fRevCarHeading + VIEW_ANGLE)
		FLOAT fClosestLockHeading
		IF GET_ANGLE_BETWEEN_2D_VECTORS(vCam.x,vCam.y,vCarMinLock.x,vCarMinLock.y) < GET_ANGLE_BETWEEN_2D_VECTORS(vCam.x,vCam.y,vCarMaxLock.x,vCarMaxLock.y)
			fClosestLockHeading = GET_HEADING_FROM_VECTOR_2D(vCarMinLock.x, vCarMinLock.y)
		ELSE
			fClosestLockHeading = GET_HEADING_FROM_VECTOR_2D(vCarMaxLock.x, vCarMaxLock.y)
		ENDIF
		vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fClosestLockHeading,0.5)
		*/
		/*
		IF fAngle > VIEW_ANGLE + 5
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.14)
		ELIF fAngle > VIEW_ANGLE + 4 
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.13)
		ELIF fAngle > VIEW_ANGLE + 3 
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.12)
		ELIF fAngle > VIEW_ANGLE + 2 
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.11)
		ELIF fAngle > VIEW_ANGLE + 1 
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.1)
		ELSE
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,0.09)
		ENDIF
		*/
		
		//vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fRevCarHeading,fCamConstrainAlpha)
		
		IF bRotRight 
			//fTempCamZ = COSINE_INTERP_FLOAT(vCameraRot.z,vCameraRot.z + fAngle,fCamConstrainAlpha)
			//fTempCamZ = vCameraRot.z + fAngle/5//10
			vCameraRot.z = fMinTurning
			//vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fMinTurning,0.5)
		ELSE
			//fTempCamZ = COSINE_INTERP_FLOAT(vCameraRot.z,vCameraRot.z - fAngle,fCamConstrainAlpha)
			//fTempCamZ = vCameraRot.z - fAngle/5//10
			vCameraRot.z = fMaxTurning
			//vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fMaxTurning,0.5)
		ENDIF
		
		//vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fTempCamZ,fAngle/200)
		/*
		IF fTempCamZAlpha > 1.0
			fTempCamZAlpha = 1.0
		ELSE
			fTempCamZAlpha = fTempCamZAlpha + 1//0.1
		ENDIF
		*/
	
	ELSE
		/*
		IF fTempCamZ <> 0
		AND fTempCamZAlpha > 0
		AND fTempCamZ <> vCameraRot.z
			fTempCamZ = COSINE_INTERP_FLOAT(vCameraRot.z,fTempCamZ,fTempCamZAlpha)
			vCameraRot.z = COSINE_INTERP_FLOAT(vCameraRot.z,fTempCamZ,0.1)
			fTempCamZAlpha = fTempCamZAlpha - 1//0.1
		ENDIF
		*/
		//ENABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_SCRIPT_RIGHT_AXIS_X)
		//fCamConstrainAlpha = 0.18
		bRotRight = FALSE
		//ENABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_RRIGHT)
	ENDIF
	
	IF bUseLockedCam
		DO_CAM_CONTROLS(newf, fCameraFOV)
		newf.z  = CAP_YAW(newf.z, fMinTurning,fMaxTurning)
		newf.x  = CAP_PITCH(newf.x)
		newf.y = GET_ENTITY_ROLL(viPlayerCar)
	ELSE
		DO_CAM_CONTROLS(vCameraRot, fCameraFOV)
		//vCameraRot.z = CAP_YAW(vCameraRot.z, fMinTurning,fMaxTurning)
		vCameraRot.x  = CAP_PITCH(vCameraRot.x,-30,30)
		vCameraRot.y = GET_ENTITY_ROLL(viPlayerCar)
	ENDIF
	
	VECTOR _Offset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar, vCameraOffset)
	IF bUseLockedCam
		SET_CAM_PARAMS(camMain, _Offset, newf, fCameraFOV)
	ELSE
		SET_CAM_PARAMS(camMain, _Offset, <<vCameraRot.x,vCameraRot.y,vCameraRot.z>>, fCameraFOV)		
	ENDIF

ENDPROC

/// PURPOSE:
///    Works out the scale factor of the box tag based on the cameras zoom and 
///    The players distance from the ped he's filming
///    And also gets the position of the peds head
/// PARAMS:
///    aPed - The ped the box tag is used on 
///    pos - The position to write the position of the peds head
/// RETURNS:
///    The scale factor of the face recog box
FUNC FLOAT GET_BOX_TAG_SIZE(PED_INDEX aPed, VECTOR &pos)
	
	IF IS_PED_UNINJURED(aPed)
		INT i = GET_PED_BONE_INDEX(aPed, BONETAG_HEAD)

		IF i <> -1
			pos = GET_WORLD_POSITION_OF_ENTITY_BONE(aPed,i)		
			IF eMissionStage = MS_CAMERA_TUTORIAL
			OR eMissionStage = MS_FILM_POPPY
				#IF NOT IS_JAPANESE_BUILD
					pos += <<fFaceOffset,0,fFaceOffsetZ>>
				#ENDIF
				#IF IS_JAPANESE_BUILD
					pos += <<0,0,0.085>>
				#ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(aPed)	
					//VECTOR vPoppyHead = GET_WORLD_POSITION_OF_ENTITY_BONE(aPed,i)		
					
					IF IS_PED_IN_ANY_VEHICLE(aPed)	
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_fuckinlosers")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_gimmethtcam")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_ih8u")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_illsueyou")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_imavirgin")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_itwasyoga")
					AND NOT IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_notdoinanythin")	
						PROCESS_ENTITY_ATTACHMENTS(aPed)
						pos = GET_PED_BONE_COORDS(aPed,BONETAG_HEAD,<<0,0,0>>)
						pos.z = pos.z + 0.08
					ELSE
						/*
						IF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_gimmethtcam")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.485,-0.28,0.40>>)
						ELIF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_illsueyou")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.38,-0.28,0.39>>)
						ELIF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_imavirgin")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.4,-0.17,0.40>>)
						ELIF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_fuckinlosers")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS(aPed),<<-0.48,-0.17,0.44>>)
						ELIF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_ih8u")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.46,-0.21,0.40>>)
						ELIF IS_ENTITY_PLAYING_ANIM(aPed,"rcmpaparazzo_2ig_3","pm_incar_itwasyoga")
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.48,-0.17,0.43>>)
						ELSE
							pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(aPed),<<-0.44,-0.3,0.40>>)
						ENDIF
						*/
						PROCESS_ENTITY_ATTACHMENTS(aPed)
						pos = GET_PED_BONE_COORDS(aPed,BONETAG_HEAD,<<0,0,0>>)
						pos.z = pos.z + 0.08
					ENDIF
					//VECTOR vSpeed = GET_ENTITY_SPEED_VECTOR(GET_VEHICLE_PED_IS_IN(aPed))
					//pos = GET_PED_BONE_COORDS(aPed,BONETAG_HEAD,<<vSpeed.x/10,vSpeed.y/10,vSpeed.z/100>>)
					/*
					IF WAS_PED_SKELETON_UPDATED(aPed)
						pos = GET_PED_BONE_COORDS(aPed,BONETAG_HEAD,<<0,0,0>>)
					ENDIF
					*/
				ENDIF
			ENDIF

			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				IF DOES_CAM_EXIST(camMain)
					float fPedRange = GET_DISTANCE_BETWEEN_COORDS(pos,GET_ENTITY_COORDS(PLAYER_PED_ID()))
					float fBoxScaler = iScaleMulti / (GET_CAM_FOV(camMain) * fPedRange)
					if fBoxScaler < fMinBox
						fBoxScaler = fMinBox
					ENDIF
					if fBoxScaler > fMaxBox
						fBoxScaler = fMaxBox
					ENDIF
					
					RETURN fBoxScaler
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN 0.0
ENDFUNC	

/// PURPOSE:
///    Draws the face tag box around the ped that needs filming
PROC DO_FACE_TAG(PED_INDEX ped)
	VECTOR vPos
	FLOAT fBoxTagScale 
	
	//fBoxTagScale = GET_BOX_TAG_SIZE(mPoppy.piPed, vPos)
	fBoxTagScale = GET_BOX_TAG_SIZE(ped, vPos)

	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)

	SET_DRAW_ORIGIN(vPos)	
		fBoxTagScale *= 0.03				
		DRAW_SPRITE("helicopterhud", "hud_corner",-fBoxTagScale*0.5, -fBoxTagScale,0.013,0.013, 0, mBoxCol.R,mBoxCol.G,mBoxCol.B, 200,TRUE)
		DRAW_SPRITE("helicopterhud", "hud_corner",fBoxTagScale*0.5, -fBoxTagScale,0.013,0.013,90, mBoxCol.R,mBoxCol.G,mBoxCol.B, 200,TRUE)
		DRAW_SPRITE("helicopterhud", "hud_corner",-fBoxTagScale*0.5, fBoxTagScale,0.013,0.013,270, mBoxCol.R,mBoxCol.G,mBoxCol.B, 200,TRUE)
		DRAW_SPRITE("helicopterhud", "hud_corner",fBoxTagScale*0.5, fBoxTagScale,0.013,0.013,180, mBoxCol.R,mBoxCol.G,mBoxCol.B, 200,TRUE)
	CLEAR_DRAW_ORIGIN()
	
	RESET_SCRIPT_GFX_ALIGN()
	
ENDPROC

/// PURPOSE:
///    Handles all the drawing of the camera UI 
PROC DO_CAM_UI()
	
	IF iRedBlink = 0
	ENDIF
	
	//IF iRedBlink > 10
	//	iRedBlink = 0
	//ELSE
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT(0.8, 0.78, "PAP2_REC")  //(0.8, 0.8, "PAP2_REC")
		SET_TEXT_COLOUR(255,255,255,255)
		iRedBlink++
	//ENDIF
	
	//DRAW_RECT(0.5, 0.5, fCross1HairWidth, fCross1HairHeight, 255, 255, 255, 150)
    //DRAW_RECT(0.5, 0.5, fCross2HairWidth, fCross2HairHeight, 255, 255, 255, 150)
	
	DRAW_SPRITE("helicopterhud", "hud_corner", fCursorX1, fCursorY1,fCursorWidth,fCursorHeight, 0, 255,255,255, 200,TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", fCursorX2, fCursorY2,fCursorWidth,fCursorHeight,90, 255,255,255, 200,TRUE)
	
	DRAW_SPRITE("helicopterhud", "hud_corner", fCursorX3, fCursorY3,fCursorWidth,fCursorHeight,270, 255,255,255, 200,TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", fCursorX4, fCursorY4,fCursorWidth,fCursorHeight,180, 255,255,255, 200,TRUE)

ENDPROC

/// PURPOSE:
///    Main camera manager, contains all the functions used to maintain the camera
///    
/// PARAMS:
///    bWalk - Is the camera on foot or in the car of the car chase
PROC MANAGE_CAMERA(BOOL bWalk = FALSE)
	
	IF DOES_CAM_EXIST(camMain) 
	AND IS_ENTITY_ALIVE(viPlayerCar)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
	
		IF DOES_CAM_EXIST(camMainCS)
			DESTROY_CAM(camMainCS)
		ENDIF
		IF DOES_CAM_EXIST(camFrankFilm)
			DESTROY_CAM(camFrankFilm)
		ENDIF
		IF DOES_CAM_EXIST(camFrankFilm2)
			DESTROY_CAM(camFrankFilm2)
		ENDIF
		
		SET_TIMECYCLE_MODIFIER("player_transition_scanlines")  //scanline_cam  //player_transition_no_scanlines //player_transition_scanlines
		SET_TIMECYCLE_MODIFIER_STRENGTH(0.15)
		
		IF bWalk
			MANAGE_WALK_CAM()
		ELSE
			MANAGE_CAR_CAM()
			//IF fTimePosInRec < 85000.0	
				//OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			//ENDIF
		ENDIF
		
		DO_CAM_UI()
		
		IF bUseFace
		AND NOT IS_PAUSE_MENU_ACTIVE()	
			DO_FACE_TAG(mPoppy.piPed)
		ENDIF
		
		SET_CAM_NEAR_DOF(camMain,2)
		//SET_CAM_FAR_DOF(camMain,COSINE_INTERP_FLOAT(GET_CAM_FAR_DOF(camMain),GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mPoppy.piPed) + 10.0,0.2))
		//SET_CAM_DOF_STRENGTH(camMain,COSINE_INTERP_FLOAT(GET_CAM_DOF_STRENGTH(camMain),fCamZoomStrength,0.2))
		SET_CAM_FAR_DOF(camMain,60.0)
		SET_CAM_DOF_STRENGTH(camMain,0.5) //0.5
		SET_USE_HI_DOF()
		
		//DO_CAM_DOF_EFFECT()
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail)
	
	IF iSeqBadCamMissionFail = 0
		CLEANUP_CAMERA(FALSE)
	ELSE
		IF DOES_CAM_EXIST(camMain)
			fCamFov = GET_CAM_FOV(camMain)
		ENDIF
		MANAGE_CAMERA()
	ENDIF
	
	IF fail <> FR_BADCAM
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	SAFE_REMOVE_BLIP(biGOTO)
	SAFE_REMOVE_BLIP(biBuddyBlip)

	bPlayerFailed = TRUE
	SWITCH fail
		CASE FR_BUDDY_HARMED
			sFailReason = "PAP2_FAILBUD" //~r~You harmed Beverly.
		BREAK
		
		CASE FR_BUDDY_DEAD
			sFailReason = "PAP2_FAILBD" //~r~You killed Beverly.
		BREAK
		
		CASE FR_BUDDY_THREAT
			sFailReason = "PAP2_FAILTHR" //~r~You scared Beverly off.
		BREAK
		
		CASE FR_BADCAM
			SK_PRINT("TRYING TO PULL OVER")
			sFailReason = "PAP2_BADCAM" //~r~You failed to get enough footage of Poppy.
		BREAK
		
		CASE FR_POPPY_SCARED
			BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			sFailReason = "PAP2_FAILPAT" //~r~You scared Poppy.
		BREAK
		
		CASE FR_SPOTTED
			BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			sFailReason = "PAP2_FAILATT"//"PAP2_FAILSPOTED" //~r~You were spotted.
		BREAK
		
		CASE FR_ATTENTION
			sFailReason = "PAP2_FAILATT" //~r~You attracted attention.
		BREAK
		
		CASE FR_KILLED_INOCC
			sFailReason = "PAP2_FAILATT"//"PAP2_FAILKIL" //~r~You harmed an innocent.
		BREAK
		
		CASE FR_KILLED_POP
			IF eMissionStage < MS_CAMERA_TUTORIAL
				BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			ENDIF
			sFailReason = "PAP2_FAILKPOP" //~r~You Killed Poppy.
		BREAK
		
		CASE FR_HARMED_POP
			IF eMissionStage < MS_CAMERA_TUTORIAL
				BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			ENDIF
			sFailReason = "PAP2_FAILPOP" //~r~You harmed Poppy.
		BREAK
		
		//CASE FR_LEFT_AREA
		//	sFailReason = "PAP2_FAILAREA" //~r~You left the area.
		//BREAK
		
		CASE FR_CAR_DEAD
			sFailReason = "PAP2_FAILCAR" //~r~Beverly's ride was wrecked.
		BREAK
		
		CASE FR_WANTED
			sFailReason = "PAP2_FAILWANT" //~r~You have attracted police attention.
		BREAK
		
		CASE FR_KILLED_SHAG
			IF eMissionStage < MS_CAMERA_TUTORIAL
				BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			ENDIF
			sFailReason = "PAP2_FAILKLOV" //~r~You killed Poppy's lover.
		BREAK
		
		CASE FR_HARMED_SHAG
			IF eMissionStage < MS_CAMERA_TUTORIAL
				BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
			ENDIF
			sFailReason = "PAP2_FAILHLOV" //~r~You harmed Poppy's lover.
		BREAK
		
		CASE FR_LEFT_BUDDY
			sFailReason = "PAP2_FAILLEFT" //~r~You left Beverly.
		BREAK
		
		CASE FR_TOOK_TOO_LONG
			sFailReason = "PAP2_FAILONG" //~r~You took too long.
		BREAK
		
		CASE FR_BEV_LEFT_YOU
			sFailReason = "PAP2_FAILHIND" //~r~Beverly left you behind.
		BREAK
		
		CASE FR_FOOTAGE_RUINED
			sFailReason = "PAP2_FAILFR" //The footage was ruined.
		BREAK
		
		CASE FR_NONE
			
		BREAK
		
	ENDSWITCH
	
	eMissionStage = MS_FAILED
	eState = SS_INIT
	
ENDPROC

/// PURPOSE:
///    Creates a car for the player to use after passing the mission
PROC SPAWN_PLAYER_CAR_FOR_PASS()
	IF NOT DOES_ENTITY_EXIST(viPassCar)
		IF SPAWN_VEHICLE(viPassCar, mnFailCarForPlayer, << -84.9445, -570.4659, 35.8701 >>, 160.6980) //<< -103.2452, -625.9245, 35.1262 >>, 159.4764)
			SK_PRINT ("LOADED... CAR")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	INT iCounter = 0
	SWITCH missionReq
	
		CASE RQ_CUTSCENE	
			IF NOT HAS_THIS_CUTSCENE_LOADED("PAP_2_RCM_P2")
				RC_REQUEST_CUTSCENE("PAP_2_RCM_P2")
				WHILE NOT HAS_CUTSCENE_LOADED()
				WAIT(0)
				ENDWHILE
				RETURN TRUE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK

		CASE RQ_BUDDY
			IF IS_PED_UNINJURED(mBuddy.piPed)
				IF NOT DOES_BLIP_EXIST(biBuddyBlip)
					biBuddyBlip = CREATE_PED_BLIP(mBuddy.piPed,TRUE,TRUE)
				ENDIF
				SET_BLIP_SCALE(biBuddyBlip, BLIP_SIZE_PED)
				SET_PED_CAN_ARM_IK(mBuddy.piPed,TRUE)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mBuddy.piPed, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mBuddy.piPed, FALSE)
				STOP_PED_SPEAKING(mBuddy.piPed,TRUE)
				DISABLE_PED_PAIN_AUDIO(mBuddy.piPed,TRUE)
				SET_ENTITY_PROOFS(mBuddy.piPed,FALSE,FALSE,FALSE,TRUE,FALSE)
				SAFE_TELEPORT_ENTITY(mBuddy.piPed, pos, dir)
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, mBuddy.piPed, "BEVERLY")
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 1, PLAYER_PED_ID(), "FRANKLIN")
				WHILE NOT LOAD_PED_RECORDING(sBeverlyPedRecording2)	
					WAIT(0)
				ENDWHILE
				//SK_PRINT("TK************ BEV SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK

		CASE RQ_POPPY
			IF SETUP_PED(mPoppy.piPed, pos, dir, mnPoppy, 0, "POPPY")
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, mPoppy.piPed, "POPPY")
				SET_PED_RELATIONSHIP_GROUP_HASH(mPoppy.piPed,relPoppyGroup)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mPoppy.piPed, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(mPoppy.piPed, CA_CAN_TAUNT_IN_VEHICLE, TRUE)
				STOP_PED_SPEAKING(mPoppy.piPed,TRUE)
				SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				#IF NOT IS_JAPANESE_BUILD	
					IF IS_REPLAY_IN_PROGRESS()
					AND GET_REPLAY_MID_MISSION_STAGE() >= 2
						SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
					ELSE
						SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,4), 2, 0, 0) //(lowr)
					ENDIF
				#ENDIF
				#IF IS_JAPANESE_BUILD
					SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				#ENDIF
				SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_LOD_MULTIPLIER(mPoppy.piPed,2.0)
				//SK_PRINT("TK************ POPPY SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
			
		BREAK
		
		CASE RQ_SHAGGER
			IF SETUP_PED(mShagger.piPed, pos, dir, mnShagger, 0, "SHAGGER")
				SET_PED_RELATIONSHIP_GROUP_HASH(mShagger.piPed,relPoppyGroup)
				TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mShagger.piPed, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(mShagger.piPed, CA_CAN_TAUNT_IN_VEHICLE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(mShagger.piPed, CA_USE_COVER, FALSE)
				SET_PED_CONFIG_FLAG(mShagger.piPed, PCF_ForceControlledKnockout, TRUE)
				SET_ENTITY_PROOFS(mShagger.piPed,FALSE,FALSE,FALSE,TRUE,FALSE)
				#IF IS_JAPANESE_BUILD
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				#ENDIF
				#IF NOT IS_JAPANESE_BUILD
					SET_PED_PROP_INDEX(mShagger.piPed, ANCHOR_EYES, 0)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
					IF IS_REPLAY_IN_PROGRESS()
					AND GET_REPLAY_MID_MISSION_STAGE() >= 2
						SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
					ELSE
						SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
					ENDIF
					SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				#ENDIF
				ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, mShagger.piPed, "pap2lover")
				SET_PED_LOD_MULTIPLIER(mShagger.piPed,2.0)
				//SK_PRINT("TK************ JUSTIN SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_BODYGUARD
			IF SETUP_PED(mBodyGuard.piPed, mBodyGuard.vPos, mBodyGuard.fDir, mnBodyGuards, 0, "BODYGUARD ")
				SET_PED_SEEING_RANGE(mBodyGuard.piPed,50.0)
				SET_PED_HEARING_RANGE(mBodyGuard.piPed,15.0)
				SET_PED_ID_RANGE(mBodyGuard.piPed,20.0)	
				SET_PED_RELATIONSHIP_GROUP_HASH(mBodyGuard.piPed,relPoppyGroup)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mBodyGuard.piPed, TRUE)
				SET_PED_DEFAULT_COMPONENT_VARIATION(mBodyGuard.piPed)
				SET_PED_CONFIG_FLAG(mBodyGuard.piPed, PCF_ForceControlledKnockout, TRUE)
				STOP_PED_SPEAKING(mBodyGuard.piPed,TRUE)
				SET_PED_COMBAT_ATTRIBUTES(mBodyGuard.piPed, CA_USE_COVER, FALSE)
				SET_ENTITY_PROOFS(mBodyGuard.piPed,FALSE,FALSE,FALSE,TRUE,FALSE)
				//SK_PRINT("TK************ BODYGUARD SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_BODYGUARD_CARS
			FOR iCount = 0 TO (MAX_CARS-1)
				IF iCount = 1
					IF SPAWN_VEHICLE(viChaseCars[POPPY_CAR], mnBodyGuardChaseCar[POPPY_CAR], vBodyGuardCarPos[POPPY_CAR], fBodyGuardCarDir[POPPY_CAR])
						SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[POPPY_CAR], TRUE)
							
						IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])
							SET_VEHICLE_COLOURS(viChaseCars[POPPY_CAR], 87,87)    //78, 81, 87
							SET_VEHICLE_EXTRA_COLOURS(viChaseCars[POPPY_CAR], 87,87)
							SET_VEHICLE_CAN_LEAK_OIL(viChaseCars[POPPY_CAR], FALSE)
							SET_VEHICLE_CAN_LEAK_PETROL(viChaseCars[POPPY_CAR], FALSE)
							SET_VEHICLE_DOORS_LOCKED(viChaseCars[POPPY_CAR], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
							SET_ENTITY_PROOFS(viChaseCars[POPPY_CAR],FALSE,FALSE,FALSE,FALSE,FALSE)
							SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(viChaseCars[POPPY_CAR], 0) 
							SET_VEHICLE_NUMBER_PLATE_TEXT(viChaseCars[POPPY_CAR],"P0PPYM14")
							iCounter++
						ENDIF
					ENDIF	
				ELIF iCount = 2
					IF SPAWN_VEHICLE(viChaseCars[SHAGGER_CAR], mnBodyGuardChaseCar[SHAGGER_CAR], vBodyGuardCarPos[SHAGGER_CAR], fBodyGuardCarDir[SHAGGER_CAR])
						SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[SHAGGER_CAR], TRUE)
						
						IF IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
							SET_VEHICLE_COLOURS(viChaseCars[SHAGGER_CAR], 0, 0)
							SET_VEHICLE_EXTRA_COLOURS(viChaseCars[SHAGGER_CAR], 0, 0)
							SET_VEHICLE_CAN_LEAK_OIL(viChaseCars[SHAGGER_CAR], FALSE)
							SET_VEHICLE_CAN_LEAK_PETROL(viChaseCars[SHAGGER_CAR], FALSE)
							SET_VEHICLE_DOORS_LOCKED(viChaseCars[SHAGGER_CAR], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
							SET_ENTITY_PROOFS(viChaseCars[SHAGGER_CAR],FALSE,FALSE,FALSE,FALSE,FALSE)
							iCounter++
						ENDIF
					ENDIF
				ELSE
					IF SPAWN_VEHICLE(viChaseCars[BODYGUARD_CAR], mnBodyGuardChaseCar[BODYGUARD_CAR], vBodyGuardCarPos[BODYGUARD_CAR], fBodyGuardCarDir[BODYGUARD_CAR])
						SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[BODYGUARD_CAR], TRUE)
						
						IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])
							SET_VEHICLE_DOORS_LOCKED(viChaseCars[BODYGUARD_CAR], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
							SET_VEHICLE_CAN_LEAK_OIL(viChaseCars[BODYGUARD_CAR], FALSE)
							SET_VEHICLE_CAN_LEAK_PETROL(viChaseCars[BODYGUARD_CAR], FALSE)
							SET_VEHICLE_COLOURS(viChaseCars[BODYGUARD_CAR], 0, 0)
							SET_VEHICLE_EXTRA_COLOURS(viChaseCars[BODYGUARD_CAR], 0, 0)
							SET_ENTITY_PROOFS(viChaseCars[BODYGUARD_CAR],FALSE,FALSE,FALSE,FALSE,FALSE)
							iCounter++
						ENDIF
					ENDIF
				ENDIF
				
				IF iCounter = (MAX_CARS)
					//SK_PRINT("TK************ VEHICLES SETUP ************TK")
					RETURN TRUE
				ENDIF
				
			ENDFOR
			RETURN FALSE
		BREAK
		
		CASE RQ_HOTEL_STAFF
			MODEL_NAMES modName
			modName = DUMMY_MODEL_FOR_SCRIPT
			
			 
			FOR iCount = 0 TO (MAX_STAFF - 1)
				IF iCount = 0
					modName = mnHotelStaff[0]
				ELSE
					modName = mnHotelStaff[1]
				ENDIF
				
				IF modName <> DUMMY_MODEL_FOR_SCRIPT
					IF SETUP_PED(mHotelStaff[iCount].piPed, mHotelStaff[iCount].vPos, mHotelStaff[iCount].fDir, modName, iCount, "HOTEL STAFF ")
						IF iCount = 0
							TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed, TRUE)
							ADD_PED_FOR_DIALOGUE(s_conversation_peds, 4, mHotelStaff[iCount].piPed, "Pap2Maid")
						ELSE
							ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, mHotelStaff[iCount].piPed, "Pap2BusBoy")
						ENDIF
						SET_PED_RELATIONSHIP_GROUP_HASH(mHotelStaff[iCount].piPed,relPoppyGroup)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mHotelStaff[iCount].piPed, TRUE)
						iCounter++
						IF iCounter >= (MAX_STAFF)
							//SK_PRINT("TK************ HOTEL STAFF SETUP ************TK")
							RETURN TRUE
						ENDIF	
					ENDIF
				ENDIF
			ENDFOR
			
		BREAK
		
		CASE RQ_BROOM
			IF IS_PED_UNINJURED(mHotelStaff[1].piPed)
				IF SPAWN_OBJECT(oiBroom, PROP_TOOL_BROOM, mHotelStaff[1].vPos)		
					IF NOT IS_ENTITY_DEAD(oiBroom)
						ATTACH_ENTITY_TO_ENTITY(oiBroom, mHotelStaff[1].piPed, GET_PED_BONE_INDEX(mHotelStaff[1].piPed, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						IF IS_ENTITY_ATTACHED(oiBroom)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE RQ_RAG
			IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
				IF SPAWN_OBJECT(oiWindowRag, PROP_RAG_01, mHotelStaff[0].vPos)		
					IF NOT IS_ENTITY_DEAD(oiWindowRag)
						ATTACH_ENTITY_TO_ENTITY(oiWindowRag, mHotelStaff[0].piPed, GET_PED_BONE_INDEX(mHotelStaff[0].piPed, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
						IF IS_ENTITY_ATTACHED(oiWindowRag)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE RQ_PLAYER_CAR
			IF SPAWN_VEHICLE(viPlayerCar, mnPlayerCar, pos,dir)
				SET_VEHICLE_AS_RESTRICTED(viPlayerCar,0)
				IF IS_ENTITY_ALIVE(viPlayerCar)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(mnPlayerCar, TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(viPlayerCar,7)
					SET_VEHICLE_CAN_LEAK_OIL(viPlayerCar, FALSE)
					SET_VEHICLE_CAN_LEAK_PETROL(viPlayerCar, FALSE)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(viPlayerCar, 0) 
					SET_VEHICLE_NUMBER_PLATE_TEXT(viPlayerCar,"P4P4R4Z0")
					SET_ENTITY_PROOFS(viPlayerCar,FALSE,FALSE,FALSE,FALSE,FALSE)
					IF eMissionStage <> MS_GET_IN_ESCAPE_CAR
						SET_VEHICLE_DOORS_LOCKED(viPlayerCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					ENDIF
					//SK_PRINT("TK************ PLAYER CAR SETUP ************TK")
					RETURN TRUE
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_ANIMS
			REQUEST_ANIM_DICT("rcmpaparazzo_2")
			#IF NOT IS_JAPANESE_BUILD	
				REQUEST_MODEL(Prop_NPC_Phone)
				IF HAS_ANIM_DICT_LOADED("rcmpaparazzo_2")
				AND HAS_MODEL_LOADED(Prop_NPC_Phone)
					RETURN TRUE
				ENDIF
			#ENDIF
			#IF IS_JAPANESE_BUILD	
				IF HAS_ANIM_DICT_LOADED("rcmpaparazzo_2")
					RETURN TRUE
				ENDIF
			#ENDIF	
			
			RETURN FALSE
		BREAK
		
		CASE RQ_STAFF_ANIMS
			RETURN TRUE
		BREAK
		
		CASE RQ_BEV_PED_RECORDING
			IF LOAD_PED_RECORDING(sBeverlyPedRecording)	
				//SK_PRINT("TK************ WAYPOINTREC LOADED ************TK")
				RETURN TRUE	
			ENDIF
			//SK_PRINT("TK************ WAYPOINTREC LOAD FAILED! ************TK")
			RETURN FALSE
		BREAK
		
		CASE RQ_CAMERA_WALK
			IF SETUP_CAMERA(TRUE)
				//SK_PRINT("TK************ WALK CAM SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_CAMERA
			IF SETUP_CAMERA(FALSE)
				//SK_PRINT("TK************ CAM SETUP ************TK")
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_CAMERA_CUTSCENE
			IF NOT DOES_CAM_EXIST(camMain)
				camMain = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", pos, <<0,0, dir>>, 50, TRUE)			
				IF DOES_CAM_EXIST(camMain)
					//SK_PRINT("TK************ CUTSCENE CAM SETUP ************TK")
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_SPRITES
			REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")				
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
				//SK_PRINT("TK************ HUD SETUP ************TK")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_OUTRO_ANIMS
			REQUEST_ANIM_DICT("veh@std@ds@base")
			IF HAS_ANIM_DICT_LOADED("veh@std@ds@base")
				RETURN TRUE
			ENDIF

			RETURN FALSE			
		BREAK
		
		CASE RQ_UBER_PLAYER_CAR_REC
			REQUEST_VEHICLE_RECORDING(1, "PAP2UBA")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"PAP2UBA")
				IF IS_ENTITY_ALIVE(viPlayerCar)
					SET_ENTITY_COORDS(viPlayerCar, vPlayerCarStartPos)
					SET_ENTITY_QUATERNION(viPlayerCar, -0.0022, -0.0289, 0.7901, -0.6123)
					//SK_PRINT("TK************ PLAYER CARREC SETUP ************TK")
					RETURN TRUE
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_POPPY_CAR_REC
			REQUEST_VEHICLE_RECORDING(101,"PAP2UBA")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101,"PAP2UBA")  
				IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])
					SET_ENTITY_COORDS(viChaseCars[POPPY_CAR], << -61.5790, 337.5999, 111.1908 >>)
					SET_ENTITY_QUATERNION(viChaseCars[POPPY_CAR], -0.0013, -0.0482, 0.9656, 0.2557)
					//SK_PRINT("TK************ POPPY CARREC SETUP ************TK")
					RETURN TRUE
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_BODYGUARD_1_REC
			REQUEST_VEHICLE_RECORDING(102,"PAP2UBA")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(102,"PAP2UBA")  
				IF IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
					SET_ENTITY_COORDS(viChaseCars[SHAGGER_CAR], << -62.3523, 322.6747, 109.7139 >>)
					SET_ENTITY_QUATERNION(viChaseCars[SHAGGER_CAR], -0.0230, -0.0644, 0.9756, 0.2086)
					//SK_PRINT("TK************ BODYGUARD1 CARREC SETUP ************TK")
					RETURN TRUE
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_BODYGUARD_2_REC
			REQUEST_VEHICLE_RECORDING(103,"PAP2UBA")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(103,"PAP2UBA")  
				IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])
					SET_ENTITY_COORDS(viChaseCars[BODYGUARD_CAR], <<-68.0034, 309.4698, 107.7925>>)
					SET_ENTITY_QUATERNION(viChaseCars[BODYGUARD_CAR], -0.0176, -0.0678, 0.9756, 0.2082)
					//SK_PRINT("TK************ BODYGUARD2 CARREC SETUP ************TK")
					RETURN TRUE
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
	
		CASE RQ_UBER_CAR_RECORDING
			IF IS_ENTITY_ALIVE(viPlayerCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
				IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
					#IF IS_DEBUG_BUILD
						INIT_UBER_RECORDING("PAP2UBA")
						iRecordingProgress++
						//SK_PRINT("TK************ UBER CARREC SETUP ************TK")
						RETURN TRUE
					#ENDIF
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_POPPY_RECORDING
			REQUEST_VEHICLE_RECORDING(1, "PAP2UBA")
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "PAP2UBA")
			AND REQUEST_AND_CHECK_MODEL(mnBodyGuardChaseCar[0], "POPPY CAR")
				IF SPAWN_VEHICLE(viChaseCars[POPPY_CAR],mnBodyGuardChaseCar[0], << -61.5607, 337.6627, 110.5456 >>, 150.2983)
					IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])
					AND IS_ENTITY_ALIVE(viPlayerCar)
						IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(mBuddy.piPed)
							SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viChaseCars[POPPY_CAR])
							IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viChaseCars[POPPY_CAR])
							AND IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
								//SK_PRINT("TK************ POPPY UBER CARREC SETUP ************TK")
								RETURN TRUE
							ENDIF
						ENDIF					
					ENDIF
				ENDIF
			ENDIF

			RETURN FALSE
		BREAK
		
		CASE RQ_SET_PEICE_RECORDING
			
			IF IS_ENTITY_ALIVE(viPlayerCar)
				IF IS_PED_UNINJURED(mBuddy.piPed)
					SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
					IF IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
						RETURN TRUE
					ENDIF
				ENDIF					
			ENDIF
		BREAK

		CASE RQ_CHASE_SOUNDS
			IF NOT bDumpedBank
				RETURN TRUE
			ENDIF
			IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\PAPARAZZO_02_A")
			AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\PAPARAZZO_02_B") 		
			AND REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\PAPARAZZO_02_C") 	
				//SK_PRINT("TK************ SOUNDS SETUP ************TK")
				RETURN TRUE
			ENDIF			
		BREAK
		
		CASE RQ_TEXT
			REQUEST_ADDITIONAL_TEXT("PAP2", MISSION_TEXT_SLOT)
			//REQUEST_ADDITIONAL_TEXT("PAP2AUD", MISSION_DIALOGUE_TEXT_SLOT)
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			//AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STAGE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_INIT
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")	
					RETURN TRUE //all mission stage requirements set up return true and activate stage
				ENDIF
			ENDIF
		BREAK

		CASE MS_INTRO
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_CUTSCENE, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)	
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")	
					RESET_MISSION_FLOW_VARS()
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
					ENDIF
					bJumpSkip = FALSE
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, m_fCharHeading)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-10.662442,283.070343,110.237289>>, <<-76.664360,301.181580,104.585754>>, 6.000000,<<-88.6147, 303.8292, 105.9208>>, 245.3452)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-86.616211,297.277191,102.943222>>, <<-55.158104,286.539825,109.669701>>, 16.750000,<<-88.6147, 303.8292, 105.9208>>, 245.3452)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0,TRUE,CHAR_FRANKLIN)
					RC_START_CUTSCENE_MODE(m_vCharPos,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
					iTimerStartStage = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

		CASE MS_FOLLOW_BEV_ROAD
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, 124)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)	
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")	
					REPOSITION_SHAG_TABLE()
					IF NOT IS_REPLAY_BEING_SET_UP()	
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-72.5612, 298.2493, 105.2336>>, 247.2199)
					ENDIF
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					KILL_ANY_CONVERSATION()
					RESET_MISSION_FLOW_VARS()
					CLEAR_AREA_OF_PEDS(<<-73.9858, 299.3878, 105.4073>>, 80.0)// Area at start
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
						SAFE_TELEPORT_ENTITY(mBuddy.piPed, <<-70.7533, 299.0078, 105.3157>>, 247.7592,TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-60.9116, 295.3501, 105.0975>>,1)
					ENDIF
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-70.9722, 297.7738, 105.2184>>, 247.6793,TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-63.1068, 294.9655, 105.0442>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
					bJumpSkip = FALSE
					END_REPLAY_SETUP()
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					RC_END_Z_SKIP(TRUE,FALSE)
					iTimerStartStage = GET_GAME_TIMER()
					bDoDelayedfade = TRUE
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_STAFF_ANIMS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")		
					iTimerStartStage = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE MS_FOLLOW_BEV_GUARDS
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, << -13.3842, 282.4926, 107.2337 >>, 257.8720)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")	
					REPOSITION_SHAG_TABLE()
					IF NOT IS_REPLAY_BEING_SET_UP()		
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -14.1493, 280.7966, 107.2078 >>, 317.7051)
					ENDIF
					KILL_ANY_CONVERSATION()
					RESET_MISSION_FLOW_VARS()
					iBevRoute = ROUTE_STAIRS_1_TOP
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
					ENDIF
					bJumpSkip = FALSE
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				iTimerStartStage = GET_GAME_TIMER()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE MS_FOLLOW_TO_POPPY
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, << -3.2270, 318.2857, 109.9195 >>, 63.3365)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_HOTEL_STAFF, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")	
					REPOSITION_SHAG_TABLE()
					IF NOT IS_REPLAY_BEING_SET_UP()		
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -2.2002, 317.2632, 109.9189 >>, 60.7242)
					ENDIF
					SAFE_TELEPORT_ENTITY(mHotelStaff[0].piPed, << 11.2819, 324.9571, 109.9188 >>, 252.0409)
					SAFE_TELEPORT_ENTITY(mHotelStaff[1].piPed, << 12.4255, 323.3313, 109.9188 >>, 54.7387)
					KILL_ANY_CONVERSATION()
					RESET_MISSION_FLOW_VARS()
					iBevRoute = ROUTE_POOL_STAIRS
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
					ENDIF
					RC_END_Z_SKIP()
					bJumpSkip = FALSE
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)
				//AND REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_02_INTRO")		
					bCanCreatePoppy = TRUE
					bCanCreateProps = TRUE
					bExpireStaffSpeech = TRUE
					iTimerStartStage = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE MS_CAMERA_TUTORIAL
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY,  << -36.7532, 323.5341, 111.6957 >>, 165.7917)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_ANIMS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CAMERA_WALK, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)			
					IF bSetAudioPos = FALSE
						END_REPLAY_SETUP()
						#IF NOT IS_JAPANESE_BUILD	
							IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
								STOP_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
								//STOP_STREAM()
							ENDIF
							//INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("PAP2_IG1_POPPYSEX",<<-30.990408,301.092346,112.681885>>)
							IF IS_ENTITY_ALIVE(mPoppy.piPed)
								INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("PAP2_IG1_POPPYSEX",mPoppy.piPed)
							ENDIF
						#ENDIF
						SK_PRINT("TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK")
						bSetAudioPos = TRUE
					ENDIF
					#IF NOT IS_JAPANESE_BUILD		
						IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("PAP2_IG1_POPPYSEX",0)	
						//IF LOAD_STREAM("CUTSCENES_PAP2_IG1_POPPYSEX_CENTER")		
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							CLEAR_PED_WETNESS(PLAYER_PED_ID())
							IF NOT IS_REPLAY_BEING_SET_UP()		
								SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-33.6810, 320.9442, 111.6958>>, 182.6031)
							ENDIF
							//END_REPLAY_SETUP()
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-33.8491, 319.3350, 111.6960>>,184.4715)
							TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING,0.4)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
							WAIT(2000)
							RESET_MISSION_FLOW_VARS()
							REQUEST_ANIM_DICT("rcmpaparazzo_2")		
							REQUEST_CLIP_SET(sWeaponMoveClipset)
							REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
							iCoverSeq = 1
							DO_PLAYER_HOLDING_CAMERA()
							REPOSITION_SHAG_TABLE()
							PLAY_POPPY_SHAGGER_ANIM(1)
							SETUP_CAMERA(TRUE)
							IF DOES_CAM_EXIST(camMain)
								fCamFov = GET_CAM_FOV(camMain)
							ENDIF
							MANAGE_CAMERA(TRUE)
							IF DOES_CAM_EXIST(camMain)
							AND IS_PED_UNINJURED(mPoppy.piPed)
								POINT_CAM_AT_ENTITY(camMain,mPoppy.piPed, vSafeVec)
							ENDIF
							TRIGGER_MUSIC_EVENT("PAP2_START")	
							bJumpSkip = FALSE
						ELSE
							SK_PRINT("TK************ PREPARE_SYNCHRONIZED_AUDIO_EVENT FAILED! ************TK")
						ENDIF
					#ENDIF
					#IF IS_JAPANESE_BUILD		
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PED_WETNESS(PLAYER_PED_ID())
						//IF NOT IS_REPLAY_BEING_SET_UP()		
						//	SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-33.6810, 320.9442, 111.6958>>, 182.6031)
						//ENDIF
						END_REPLAY_SETUP()
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-33.8491, 319.3350, 111.6960>>,184.4715)
						TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING,0.4)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
						WAIT(2000)
						RESET_MISSION_FLOW_VARS()
						REQUEST_ANIM_DICT("rcmpaparazzo_2")		
						REQUEST_CLIP_SET(sWeaponMoveClipset)
						REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
						iCoverSeq = 1
						DO_PLAYER_HOLDING_CAMERA()
						REPOSITION_SHAG_TABLE()
						PLAY_POPPY_SHAGGER_ANIM(1)
						SETUP_CAMERA(TRUE)
						IF DOES_CAM_EXIST(camMain)
							fCamFov = GET_CAM_FOV(camMain)
						ENDIF
						MANAGE_CAMERA(TRUE)
						IF DOES_CAM_EXIST(camMain)
						AND IS_PED_UNINJURED(mPoppy.piPed)
							POINT_CAM_AT_ENTITY(camMain,mPoppy.piPed, vSafeVec)
						ENDIF
						TRIGGER_MUSIC_EVENT("PAP2_START")	
						bJumpSkip = FALSE
					#ENDIF
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_CAMERA_WALK, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK	
		
		CASE MS_FILM_POPPY
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY,  << -36.7532, 323.5341, 111.6957 >>, 165.7917)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD, mBodyGuard.vPos)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_ANIMS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CAMERA_WALK, vSafeVec)
				//AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)				
					IF bSetAudioPos = FALSE
						END_REPLAY_SETUP()
						#IF NOT IS_JAPANESE_BUILD	
							IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
								STOP_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
								//STOP_STREAM()
							ENDIF
							//INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("PAP2_IG1_POPPYSEX",<<-30.990408,301.092346,112.681885>>)
							INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("PAP2_IG1_POPPYSEX",mPoppy.piPed)
						#ENDIF
						SK_PRINT("TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK")
						bSetAudioPos = TRUE
					ENDIF
					#IF NOT IS_JAPANESE_BUILD		 
						IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("PAP2_IG1_POPPYSEX",0)		
						//IF LOAD_STREAM("CUTSCENES_PAP2_IG1_POPPYSEX_CENTER")		
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							CLEAR_PED_WETNESS(PLAYER_PED_ID())
							//IF NOT IS_REPLAY_BEING_SET_UP()	
							//	SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-33.6810, 320.9442, 111.6958>>, 182.6031)
							//ENDIF
							//END_REPLAY_SETUP()
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-33.8491, 319.3350, 111.6960>>,184.4715)
							TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING,0.4)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
							WAIT(2000)
							RESET_MISSION_FLOW_VARS()
							DO_PLAYER_HOLDING_CAMERA()
							REPOSITION_SHAG_TABLE()
							PLAY_POPPY_SHAGGER_ANIM(1)
							RESET_BOX_COL()
							IF DOES_CAM_EXIST(camMain)
								fCamFov = GET_CAM_FOV(camMain)
							ENDIF
							MANAGE_CAMERA(TRUE)
							IF DOES_CAM_EXIST(camMain)
							AND IS_PED_UNINJURED(mPoppy.piPed)
								POINT_CAM_AT_ENTITY(camMain,mPoppy.piPed, vSafeVec)
							ENDIF
							TRIGGER_MUSIC_EVENT("PAP2_START")	
							SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
							bJumpSkip = FALSE
						ELSE
							SK_PRINT("TK************ PREPARE_SYNCHRONIZED_AUDIO_EVENT FAILED! ************TK")
						ENDIF
					#ENDIF
					#IF IS_JAPANESE_BUILD		
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PED_WETNESS(PLAYER_PED_ID())
						IF NOT IS_REPLAY_BEING_SET_UP()	
							SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-33.6810, 320.9442, 111.6958>>, 182.6031)
						ENDIF
						END_REPLAY_SETUP()
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-33.8491, 319.3350, 111.6960>>,184.4715)
						TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING,0.4)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
						WAIT(2000)
						RESET_MISSION_FLOW_VARS()
						DO_PLAYER_HOLDING_CAMERA()
						REPOSITION_SHAG_TABLE()
						PLAY_POPPY_SHAGGER_ANIM(1)
						RESET_BOX_COL()
						IF DOES_CAM_EXIST(camMain)
							fCamFov = GET_CAM_FOV(camMain)
						ENDIF
						MANAGE_CAMERA(TRUE)
						IF DOES_CAM_EXIST(camMain)
						AND IS_PED_UNINJURED(mPoppy.piPed)
							POINT_CAM_AT_ENTITY(camMain,mPoppy.piPed, vSafeVec)
						ENDIF
						TRIGGER_MUSIC_EVENT("PAP2_START")	
						SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
						bJumpSkip = FALSE
					#ENDIF
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_BEV_PED_RECORDING, vSafeVec)	
					IF IS_ENTITY_ALIVE(viPlayerCar)
						SET_ENTITY_COORDS(viPlayerCar, vPlayerCarStartPos)
						SET_ENTITY_QUATERNION(viPlayerCar, -0.0022, -0.0289, 0.7901, -0.6123)
					ENDIF
					
					eChaseConvo = CC_STAGE_1
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	
		CASE MS_GET_IN_ESCAPE_CAR
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, << -33.2434, 319.0200, 111.6952 >>, 154.2353)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BEV_PED_RECORDING, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_ANIMS, vSafeVec)
					SAFE_TELEPORT_ENTITY(mBuddy.piPed, <<-36.5195, 326.0930, 111.6962>>, 187.1461)	
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					IF NOT IS_REPLAY_BEING_SET_UP()		
						SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-33.1227, 318.9001, 111.6958>>, 154.1067)
					ENDIF
					END_REPLAY_SETUP()
					SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-33.8491, 319.3350, 111.6960>>,184.4715)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
					#IF IS_JAPANESE_BUILD
						SAFE_TELEPORT_ENTITY(mPoppy.piPed,<<-28.95, 300.81, 112.69>>,100.35)
						SAFE_TELEPORT_ENTITY(mShagger.piPed,<<-29.87, 300.57, 112.69>>,-53.25)
						PLAY_POPPY_SHAGGER_ANIM(1)
					#ENDIF
					WAIT(2000)
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(32.2338)
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.3076)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(10.7485)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-9.8457)
					KILL_ANY_CONVERSATION()					
					RESET_MISSION_FLOW_VARS()					
					DO_PLAYER_HOLDING_CAMERA()
					BREAKOUT_SYNC_SCENE_SPOT_PLAYER()
					#IF NOT IS_JAPANESE_BUILD
						SET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene_breakout,fBreakoutPhase)
					#ENDIF
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					TRIGGER_MUSIC_EVENT("PAP2_SPOTTED_RESTART")	
					//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(),<<-33.1290, 318.9112, 111.6962>>,-1,TRUE,0,TRUE,FALSE,NULL)
					TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT,0.739)
					WAIT(100)
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(32.2338)
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.3076)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(10.7485)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-9.8457)
					bJumpSkip = FALSE
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(biBuddyBlip)
					biBuddyBlip = CREATE_PED_BLIP(mBuddy.piPed,TRUE,TRUE)
				ENDIF
				IF IS_ENTITY_ALIVE(viPlayerCar)
					SET_VEHICLE_DOORS_LOCKED(viPlayerCar, VEHICLELOCK_UNLOCKED)
					SAFELY_SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerCar)
				ENDIF
				
				IF SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY_CAR_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_UBER_PLAYER_CAR_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_1_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)
					iTimerStartStage = GET_GAME_TIMER()
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK

		CASE MS_CAR_CHASE
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, 344.7485)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD, mBodyGuard.vPos, mBodyGuard.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_UBER_PLAYER_CAR_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY_CAR_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_1_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SPRITES, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
					RESET_MISSION_FLOW_VARS()
					SAFE_TELEPORT_ENTITY(mBuddy.piPed, m_vCharPos, 344.7485)
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 3, mBuddy.piPed, "BEVERLY")
					ADD_PED_FOR_DIALOGUE(s_conversation_peds, 1, PLAYER_PED_ID(), "FRANKLIN")
					END_REPLAY_SETUP()
					WARP_EVERYONE_TO_CARS_SKIP()
					IF IS_ENTITY_ALIVE(viPlayerCar)
						SET_ENTITY_QUATERNION(viPlayerCar, -0.0022, -0.0289, 0.7901, -0.6123)
					ENDIF
					IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
						SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
					ENDIF
					CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()),200,TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					iTimerPlayerInEscapeCar = GET_GAME_TIMER()
					iTimerPlayerInEscapeCar = iTimerPlayerInEscapeCar + 1000
					iPlaybackProgress = 0
					WAIT(100)
					CLEAR_AREA_OF_PEDS(<<-73.9858, 299.3878, 105.4073>>, 80.0)// Area at start
					RC_END_Z_SKIP(FALSE,FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					TRIGGER_MUSIC_EVENT("PAP2_CAR_RESTART")
					bJumpSkip = FALSE
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_CHASE_SOUNDS, vSafeVec)
					SAFE_REMOVE_BLIP(biGOTO)
					iTimerStartStage = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	
		CASE MS_OUTRO
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, << -69.2061, -523.7703, 39.2051 >>, 163.9675)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					END_REPLAY_SETUP()
					IF NOT IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar) 
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viPlayerCar, VS_BACK_RIGHT)
					ENDIF
					IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
						SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
					ENDIF
					REQUEST_CUTSCENE("pap_2_mcs_1")
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "**** Requested CS ****")
					#ENDIF
					IF IS_ENTITY_ALIVE(viPlayerCar)
						SET_ENTITY_COORDS(viPlayerCar, << -69.2061, -523.7703, 39.2051 >>)
						SET_ENTITY_HEADING(viPlayerCar, 163.9675)
					ENDIF
					RESET_MISSION_FLOW_VARS()
					bJumpSkip = FALSE					
				ENDIF
			ELSE
				iTimerStartStage = GET_GAME_TIMER()
				RETURN TRUE
			ENDIF
		BREAK

		CASE MS_UBER_RECORD
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS,vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, 344.7485)
					iPlaybackProgress = 0
					RC_END_Z_SKIP()
					bJumpSkip = FALSE
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, 344.7485)
				AND SETUP_STAGE_REQUIREMENTS(RQ_UBER_CAR_RECORDING, vSafeVec)
					IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
						TASK_WARP_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar, VS_BACK_LEFT)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
			RETURN FALSE			
		BREAK
		
		CASE MS_POPPY_AND_UBA
			IF bJumped
				IF SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS,vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, m_vCharPos, 344.7485)
					iPlaybackProgress = 0
					RC_END_Z_SKIP()
					bJumpSkip = FALSE
				ENDIF
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, vAlleyPos)
				AND SETUP_STAGE_REQUIREMENTS(RQ_POPPY_RECORDING, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_UBER_PLAYER_CAR_REC, vSafeVec)
					IF IS_ENTITY_ALIVE(viPlayerCar)
						SET_ENTITY_COORDS(viPlayerCar, vPlayerCarStartPos)
						SET_ENTITY_QUATERNION(viPlayerCar, -0.0022, -0.0289, 0.7901, -0.6123)
						RETURN TRUE
					ENDIF
				ENDIF			
			ENDIF
			RETURN FALSE
		BREAK

		CASE MS_SET_PIECE_RECORDING
			IF bJumped
				SK_PRINT("JUMPED")
				iPlaybackProgress = 0
				RC_END_Z_SKIP()
				bJumpSkip = FALSE
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_BUDDY, vAlleyPos)
				AND SETUP_STAGE_REQUIREMENTS(RQ_UBER_PLAYER_CAR_REC, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SET_PEICE_RECORDING, vSafeVec)
					IF IS_ENTITY_ALIVE(viPlayerCar) 		
						SET_ENTITY_COORDS(viPlayerCar, vPlayerCarStartPos)
						SET_ENTITY_QUATERNION(viPlayerCar, -0.0022, -0.0289, 0.7901, -0.6123)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_PAPARAZZI", bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(1)
		SET_WANTED_LEVEL_MULTIPLIER(1)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
PROC CLEANUP()
	
	ENABLE_SELECTOR()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	PRINTSTRING("STAGE CLEANUP") PRINTNL()
	
	CLEANUP_HOTEL_PEDS()
	
	IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
		IF GET_VEHICLE_DOOR_LOCK_STATUS(viPlayerCar) = VEHICLELOCK_LOCKED_PLAYER_INSIDE
			SET_VEHICLE_DOORS_LOCKED(viPlayerCar, VEHICLELOCK_UNLOCKED)
		ENDIF
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), vPlayerStartPos, fPlayerDir)
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(viPlayerCar)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(mnPlayerCar, FALSE)
		SAFE_DELETE_VEHICLE(viPlayerCar)
	ENDIF

	IF IS_PED_UNINJURED(mBuddy.piPed)
		CLEAR_PED_TASKS_IMMEDIATELY(mBuddy.piPed)
	ENDIF
	SAFE_DELETE_PED(mPoppy.piPed)
	SAFE_DELETE_PED(mBodyGuard.piPed)
	SAFE_DELETE_PED(mShagger.piPed)
	
	FOR iCount=0 TO (MAX_STAFF-1)
		SAFE_DELETE_PED(mHotelStaff[iCount].piPed)
	ENDFOR

	IF bReachedChase = FALSE
		FOR iCount=0 TO (MAX_CARS-1)
			IF DOES_ENTITY_EXIST(viChaseCars[iCount])
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[iCount], FALSE)
				IF IS_ENTITY_A_MISSION_ENTITY(viChaseCars[iCount])
					SAFE_DELETE_VEHICLE(viChaseCars[iCount])
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[iCount], FALSE)
	ENDIF

	IF NOT IS_STRING_NULL(sBeverlyPedRecording)
		IF GET_IS_WAYPOINT_RECORDING_LOADED(sBeverlyPedRecording)
			REMOVE_WAYPOINT_RECORDING(sBeverlyPedRecording)
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL(sBeverlyPedRecording2)
		IF GET_IS_WAYPOINT_RECORDING_LOADED(sBeverlyPedRecording2)
			REMOVE_WAYPOINT_RECORDING(sBeverlyPedRecording2)
		ENDIF
	ENDIF

	CLEANUP_CAMERA(TRUE)
	CLEANUP_CAMERA(FALSE)
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 0)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 1)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 2)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 3)
	REMOVE_PED_FOR_DIALOGUE(s_conversation_peds, 4)
	SAFE_REMOVE_BLIP(biGOTO)

	SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
	CLEANUP_UBER_PLAYBACK(TRUE)

	//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
ENDPROC

PROC Script_Cleanup()
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
		STOP_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
	ENDIF
	
	CLEAR_PED_NON_CREATION_AREA()
	
	//CASCADE_SHADOWS_INIT_SESSION()
	
	TRIGGER_MUSIC_EVENT("PAP2_FAIL")	
	
	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	ENABLE_SELECTOR()
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	RESET_SCRIPT_GFX_ALIGN()
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),FALSE)
	ENDIF
	
	IF bStreamvolCreated = TRUE
		STREAMVOL_DELETE(StreamVol)
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_CHASE")
		STOP_AUDIO_SCENE("PAPARAZZO_02_CHASE")
	ENDIF	
	
	IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_INTRO")
		STOP_AUDIO_SCENE("PAPARAZZO_02_INTRO")
	ENDIF	

	SET_AMBIENT_ZONE_STATE("AZ_PAPARAZZO_02_AMBIENCE",FALSE,TRUE)
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
	
	SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
	
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	
	REMOVE_COVER_POINT(m_ciPap2) //For B*1819613
	
	DISABLE_TAXI_HAILING(FALSE) //For B*1860244 - Blocking taxis during mission.
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		IF (DOES_ENTITY_EXIST(mBuddy.piPed))
			IF IS_PED_UNINJURED(mBuddy.piPed)
				IF NOT IS_PED_IN_ANY_VEHICLE(mBuddy.piPed)
					TASK_WANDER_STANDARD(mBuddy.piPed)
				ENDIF
				SET_PED_KEEP_TASK(mBuddy.piPed, TRUE)
			ENDIF
			SAFE_RELEASE_PED(mBuddy.piPed)
		ENDIF
		
		DELETE_SCRIPTED_TRAFFIC()
		
		STOP_CUTSCENE_IMMEDIATELY()
		
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_ss1_14_garage_door,<<-62.22, 352.75, 113.01>>,FALSE)

		STOP_SOUND(iZoomSound)
		//SK_PRINT("TK************ STOP ZOOM SOUND ************")
			
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_COVER)

		SAFE_DELETE_OBJECT(ObjPhone)
		
		SAFE_RELEASE_OBJECT(objPipe1)
		
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
			IF NOT IS_ENTITY_DEAD(sRCLauncherDataLocal.objID[0])
				DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(objCam)
			IF NOT IS_ENTITY_DEAD(objCam)
				DELETE_OBJECT(objCam)
			ENDIF
		ENDIF

		//ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_CHARACTER_WHEEL)

		REMOVE_SCENARIO_BLOCKING_AREA(sbiBuilders)
		REMOVE_SCENARIO_BLOCKING_AREA(sbiPoppy)
		REMOVE_SCENARIO_BLOCKING_AREA(sbiClipboardGuys)

		CLEAR_WEATHER_TYPE_PERSIST()

		IF DOES_ENTITY_EXIST(viPlayerCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnPlayerCar, FALSE)
			SAFE_RELEASE_VEHICLE(viPlayerCar)
		ENDIF
		/*
		IF DOES_ENTITY_EXIST(viPassCar)
			SAFE_RELEASE_VEHICLE(viPassCar)
		ENDIF
		*/
		IF DOES_ENTITY_EXIST(mShagger.piPed)
			SAFE_RELEASE_PED(mShagger.piPed)
		ENDIF

		IF DOES_ENTITY_EXIST(mPoppy.piPed)
			SAFE_RELEASE_PED(mPoppy.piPed)
		ENDIF

		IF DOES_ENTITY_EXIST(mBodyGuard.piPed)
			SAFE_RELEASE_PED(mBodyGuard.piPed)
		ENDIF

		FOR iCount=0 TO (MAX_STAFF-1)
			IF DOES_ENTITY_EXIST(mHotelStaff[iCount].piPed)
				SAFE_RELEASE_PED(mHotelStaff[iCount].piPed)
			ENDIF
		ENDFOR
		
		IF IS_VEHICLE_OK(viChaseCars[POPPY_CAR])
			SET_VEHICLE_DOORS_LOCKED(viChaseCars[POPPY_CAR], VEHICLELOCK_UNLOCKED)
		ENDIF
		
		FOR iCount=0 TO (MAX_CARS-1)
			IF DOES_ENTITY_EXIST(viChaseCars[iCount]) // NEEDS TO WORK WITH NEW INITAL SCENE STUFF
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mnBodyGuardChaseCar[iCount], FALSE)
				IF IS_VEHICLE_OK(viChaseCars[iCount])
					SET_VEHICLE_DOORS_LOCKED(viChaseCars[iCount],VEHICLELOCK_UNLOCKED)
				ENDIF
				//SAFE_RELEASE_VEHICLE(viChaseCars[iCount])
			ENDIF
		ENDFOR
		
		IF DOES_ENTITY_EXIST(oiBroom)
			SAFE_RELEASE_OBJECT(oiBroom)
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiWindowRag)
			SAFE_RELEASE_OBJECT(oiWindowRag)
		ENDIF
		
		REMOVE_RELATIONSHIP_GROUP(relPoppyGroup)

		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		
		CLEAR_PED_NON_CREATION_AREA()
		SERVICES_TOGGLE(TRUE)
		CLEANUP()
		UNREGISTER_SCRIPT_WITH_AUDIO()
	ELSE
		RC_CleanupSceneEntities(sRCLauncherDataLocal)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBuddyBlip)
	SAFE_REMOVE_BLIP(biGOTO)
ENDPROC

/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	INT i
	
	//DELETE PEDS	
	SAFE_DELETE_PED(mBuddy.piPed)
	SAFE_DELETE_PED(mPoppy.piPed)
	SAFE_DELETE_PED(mShagger.piPed)
	SAFE_DELETE_PED(mBodyGuard.piPed)

	FOR i=0 TO (MAX_STAFF-1)
		SAFE_DELETE_PED(mHotelStaff[i].piPed)
	ENDFOR

	//VEHICLES
	IF bReachedChase = TRUE	
		FOR i=0 TO (MAX_CARS-1)
			SAFE_DELETE_VEHICLE(viChaseCars[i])
		ENDFOR
	ENDIF
	
	IF bReachedChase = TRUE	
		SAFE_DELETE_VEHICLE(viPlayerCar)
	ENDIF
	
	//OBJECTS
	FOR i=0 TO (MAX_SEX_SCENE_PROPS-1)
		SAFE_DELETE_OBJECT(mSexSceneProps[i].oiProp)
	ENDFOR
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	SAFE_REMOVE_BLIP(biGOTO)
	SAFE_REMOVE_BLIP(biBuddyBlip)
//	CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_UNLOGGED_SMALL_ACTION, 500) // Initially requested on B*1237043 - Removed on request as B*1317037
	//IF bReplayUsed = FALSE	
		IF bJumpedInPool	
			//INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP2_POOL_JUMP) 
		ENDIF
		IF bDeckedGuard
			//INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP2_SECURITY_GUARD_CAR_HIT) 
		ENDIF
		//2271 my max score
		iMaxRecogPercent = ROUND(TO_FLOAT(iFilmingStats) /2271.0 * 100) 
		IF iMaxRecogPercent > 100
			iMaxRecogPercent = 100
		ENDIF
		//IF iMaxRecogPercent > 50	
			INFORM_MISSION_STATS_OF_INCREMENT(PAP2_FACE_RECOG_PERCENT,iMaxRecogPercent)
		//ENDIF
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "STATS FACE RECOG")
			PRINTNL()
			PRINTINT(iMaxRecogPercent)
		#ENDIF
	//ENDIF
	IF bSkipPassMission
		IF IS_SCREEN_FADED_OUT()
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -71.0875, -522.2567, 39.3243 >>, 255.9742)
			SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
			bSkipPassMission = FALSE
		ENDIF
	ELSE
		Random_Character_Passed(CP_RAND_C_PAP2)
		Script_Cleanup()

		IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
			TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
		ENDIF
		
		TERMINATE_THIS_THREAD()	
	ENDIF

ENDPROC

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	
	IF iSeqBadCamMissionFail <> 0
		IF DOES_CAM_EXIST(camMain)
			fCamFov = GET_CAM_FOV(camMain)
		ENDIF
		MANAGE_CAMERA()
	ENDIF
	
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			TRIGGER_MUSIC_EVENT("PAP2_FAIL")	
			
			IF bPutCamInFranksHand = TRUE	
				SK_PRINT("TK bPutCamInFranksHand = TRUE")
				IF DOES_ENTITY_EXIST(objCam)
					SK_PRINT("TK CAM EXISTS")
					DETACH_ENTITY(objCam)	
					ATTACH_ENTITY_TO_ENTITY(objCam, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_SOUND_FINISHED(iZoomSound)
				STOP_SOUND(iZoomSound)
			ENDIF
	
			REMOVE_BLIPS()

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				Random_Character_Failed_With_Reason(sFailReason)
			ELSE
				Random_Character_Failed()
			ENDIF
			
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				// MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< -107.4400, 314.5400, 107.7151 >>, 239.2000)
  				// SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< -101.5398, 308.9274, 106.9288 >>, 241.0294)

				DELETE_ALL()
			
				Script_Cleanup()
				TERMINATE_THIS_THREAD()	
			ELSE
				//SET_IK_TARGET(PLAYER_PED_ID(),IK_PART_ARM_RIGHT,mBuddy.piPed,GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND),<<0,0,0>>,ITF_ARM_TARGET_WRT_POINTHELPER)
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//*************************************************************************************************************************************************
//													:SETUP FUNCTIONS:
//*************************************************************************************************************************************************

/// PURPOSE:
///    Passes strings in to the array of sBadFilmLines
PROC POPULATE_BADFILM_LINES()
	/*
	sBadFilmLines[0] = "PAP2_ALAA"
	sBadFilmLines[1] = "PAP2_ALAB"
	sBadFilmLines[2] = "PAP2_ALAD"
	sBadFilmLines[3] = "PAP2_ALAC"
	*/
ENDPROC

/// PURPOSE:
///    Passes strings in to the array of sChaseConvoLines
PROC POPULATE_CHASE_CONVO_LINES()
	/*
	sChaseConvoLines[0] = "PAP2_CHASE2_1"
	sChaseConvoLines[1] = "PAP2_CHASE2_2"
	sChaseConvoLines[2] = "PAP2_CHASE2_3"
	sChaseConvoLines[3] = "PAP2_CHASE2_4"
	sChaseConvoLines[4] = "PAP2_CHASE2_5"
	sChaseConvoLines[5] = "PAP2_CHASE2_6"
	sChaseConvoLines[6] = "PAP2_CHASE2_7"
	sChaseConvoLines[7] = "PAP2_CHASE2_8"
	sChaseConvoLines[8] = "PAP2_CHASE2_9"
	sChaseConvoLines[9] = "PAP2_CHASE2_10"
	sChaseConvoLines[10] = "PAP2_CHASE2_11"
	sChaseConvoLines[11] = "PAP2_CHASE2_12"
	sChaseConvoLines[12] = "PAP2_CHASE2_13"
	sChaseConvoLines[13] = "PAP2_CHASE2_14"
	sChaseConvoLines[14] = "PAP2_CHASE2_15"
	sChaseConvoLines[15] = "PAP2_CHASE2_16"
	*/
ENDPROC

#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    Initalises the positions of the chase route
	PROC CHASE_ROUTE()
		vChaseRoute[0] = << -68.7154, 295.6518, 104.9243 >>
		vChaseRoute[1] = << -11.9200, 274.6289, 107.2471 >>
		vChaseRoute[2] = << 15.7054, 276.2363, 108.4940 >>
		vChaseRoute[3] = << 47.0992, 299.9255, 109.7315 >>
		vChaseRoute[4] = << 116.9425, 349.5923, 111.5336 >>
		vChaseRoute[5] = << 213.7966, 357.8576, 105.0071 >>
		vChaseRoute[6] = << 306.2962, 326.7704, 104.5107 >>
		vChaseRoute[7] = << 394.2304, 304.7151, 102.0353 >>
		vChaseRoute[8] = << 487.9755, 273.3113, 102.0741 >>
		vChaseRoute[9] = << 565.4470, 244.4710, 102.1545 >>
		vChaseRoute[10] = << 668.1558, 207.8624, 93.4267 >>
		vChaseRoute[11] = << 664.5176, 180.0834, 91.9847 >>
		vChaseRoute[12] = << 648.3553, 131.5264, 90.0370 >>
		vChaseRoute[13] = << 624.1434, 68.8094, 89.0257 >>
		vChaseRoute[14] = << 624.9630, 52.6268, 88.2900 >>
		vChaseRoute[15] = << 642.0527, 36.8763, 86.0646 >>
		vChaseRoute[16] = << 674.7148, 1.8389, 83.0225 >>
		vChaseRoute[17] = << 561.0575, -85.5142, 67.2631 >>
		vChaseRoute[18] = << 493.5237, -144.0617, 56.3878 >>
		vChaseRoute[19] = << 416.6940, -135.0656, 63.6700 >>
		vChaseRoute[20] = << 320.6953, -103.9111, 67.6914 >>
		vChaseRoute[21] = << 270.8412, -116.1302, 68.4322 >>
		vChaseRoute[22] = << 241.8702, -173.7106, 56.5527 >>
		vChaseRoute[23] = << 215.7430, -253.0528, 52.0439 >>
		vChaseRoute[24] = << 187.6513, -317.3376, 42.8785 >>
		vChaseRoute[25] = << 151.8253, -346.5048, 43.3050 >>
		vChaseRoute[26] = << 121.2551, -368.7065, 41.5393 >>
		vChaseRoute[27] = << 99.3367, -386.6132, 40.2811 >>
		vChaseRoute[28] = << 71.2662, -386.8159, 38.9204 >>
		vChaseRoute[29] = << 61.8406, -407.3744, 38.9198 >>
		vChaseRoute[30] = << 35.8686, -400.7651, 38.9213 >>
		vChaseRoute[31] = << -2.8669, -400.0531, 38.3989 >>
		vChaseRoute[32] = << -26.6296, -418.6472, 38.6388 >>
		vChaseRoute[33] = << -104.7511, -606.8694, 35.0632 >>
	ENDPROC
#ENDIF

/// PURPOSE:
///    Initialises the points that beverly follows on the way to poppy
PROC BEV_ROUTE()
	//Point just in front of bevs car 
	vBevRoute[0] = << -70.2823, 298.6007, 105.2842 >>

	//Point at the bottom of the stairs 
	vBevRoute[1] = << -12.8842, 283.1092, 107.2567 >> 
	
	//Point in the middle of the first stair set
	vBevRoute[2] = << -9.7370, 286.8741, 108.4575 >>
	
	//Point at the top of the stairs just before the opening to the courtyard
	vBevRoute[3] = << -2.8843, 291.4941, 109.9209 >> 
	
	//Point just in front of the first archways
	vBevRoute[4] = << 5.6191, 309.9637, 109.9198 >> 
	
	//Point through the two arch ways by the stair that lead to the pool area
	vBevRoute[5] = << -4.6429, 318.5697, 109.9198 >> 

	//point across the pool by the last stair set by the house poppy is at
	vBevRoute[6] = << -34.2534, 332.7881, 112.1618 >> //<< -36.0161, 333.0677, 112.1623 >> 
	
	//point looking at poppy
	vBevRoute[7] = << -33.4322, 319.0293, 111.6957 >> 
ENDPROC

/// PURPOSE:
///    Main dialogue array filler function
///    any new dialogue filler functions are put in here 
///    keeps it tidy
PROC POPULATE_ALL_CONVOS()
	POPULATE_BADFILM_LINES()
	POPULATE_CHASE_CONVO_LINES()
ENDPROC

/// PURPOSE:
///    Initialises a new A_PROP struct and returns it so the global script variable
///    can be initalised. stops copy and pasting and looks tidier
/// PARAMS:
///    pos - The position the prop will be created at
///    rot - The rotation the prop will be created at
///    mod - The model to create
/// RETURNS:
///    An initalised A_PROP struct
FUNC A_PROP FILL_PROP(VECTOR pos, VECTOR rot, MODEL_NAMES mod)
	A_PROP mReturnProp
	
	mReturnProp.vPos = pos
	mReturnProp.vRot = rot
	mReturnProp.mnModle = mod
	
	RETURN mReturnProp
ENDFUNC

/// PURPOSE:
///    Initiialises the prop array for the sex scene setup
///    The array is an array of A_PROP structs
PROC POPULATE_PROPS()
	mSexSceneProps[0] = FILL_PROP( <<-27.414112,310.126892,111.71678>>, <<0.000000,-0.000000,0.000000>>, prop_bbq_5)

	mSexSceneProps[1] = FILL_PROP( <<-31.384937,313.450836,111.70890>>, <<0.000000,-0.000000,0.000000>>, prop_cs_panties)
	
	mSexSceneProps[2] = FILL_PROP( <<-32.917381,311.863129,112.08356>>, <<0.000000,0.001641,0.006936>>, prop_cs_panties_02)

	mSexSceneProps[3] = FILL_PROP( <<-33.055721,311.614716,111.71154>>, <<0.000000,-2.880545,-1.247135>>, prop_boombox_01)

ENDPROC

///PURPOSE:
///   Populate the cop car models
PROC BODYGUARD_CAR_MODELS()
	mnBodyGuardChaseCar[0] = RapidGT2
	mnBodyGuardChaseCar[1] = landstalker
	mnBodyGuardChaseCar[2] = landstalker
	
	vBodyGuardCarPos[0] = << -61.5802, 337.5996, 111.1479 >>
	fBodyGuardCarDir[0] =  157.8698
	
	vBodyGuardCarPos[1] =  << -62.3344, 322.6908, 109.2141 >>
	fBodyGuardCarDir[1] =  155.9353
	
	vBodyGuardCarPos[2] =  << -68.0634, 309.3348, 107.2594 >>
	fBodyGuardCarDir[2] =  155.9353
ENDPROC

/// PURPOSE:
///    Initialises a new MAID_WORK_POINT struct and returns it so the global script variable
///    can be initalised. stops copy and pasting and looks tidier
/// PARAMS:
///    pos - position the work point is at
///    dir - the direction to face when working at this point
///    state - the state the maid should enter when arriving at this point
/// RETURNS:
///    An initialised MAID_WORK_POINT
FUNC MAID_WORK_POINT FILL_MAID_WORK_POINT(VECTOR pos, FLOAT dir, MAID_AMBIENT_STATE state)
	MAID_WORK_POINT mReturnPoint
	
	mReturnPoint.vGOTO = pos
	mReturnPoint.fFaceing = dir
	mReturnPoint.eStateToEnter = state
	
	RETURN mReturnPoint
	
ENDFUNC

/// PURPOSE:
///    Initialises the points that the hotel staff use when they enter their 
///    ambient behaviour
PROC POPULATE_STAFF_WORK_POINTS()
	mMaidWorkPoint[0] = FILL_MAID_WORK_POINT(<< 17.7501, 339.8129, 114.3926 >>, 157.8568, M_AMB_SMOKEING)
	
	mMaidWorkPoint[1] = FILL_MAID_WORK_POINT(<< 21.1623, 341.5796, 114.3926 >>, 338.0048, M_AMB_WASHING_WINDOW)

	mMaidWorkPoint[2] = FILL_MAID_WORK_POINT(<< 38.4316, 340.2473, 114.3926 >>, 66.9258, M_AMB_WASHING_WINDOW)
	
	mMaidWorkPoint[3] = FILL_MAID_WORK_POINT(<< 11.1311, 327.7826, 109.9198 >>, 110.9247, M_AMB_WASHING_WINDOW)

	mMaidWorkPoint[4] = FILL_MAID_WORK_POINT(<< 22.3298, 305.7970, 109.9198 >>, 255.7478, M_AMB_SMOKEING)

	
	vBoyWorkPoint[0] = << 1.7382, 301.8018, 109.9198 >>
	vBoyWorkPoint[1] = << 19.2729, 326.7421, 109.9198 >>
	vBoyWorkPoint[2] = << 12.4255, 323.3313, 109.9188 >>

ENDPROC

///PURPOSE: 
///    Populate All Peds
PROC POPULATE_PEDS()

	mPoppy.vPos = << -30.5144, 308.4476, 111.6949 >>   //<< -49.7213, 355.3630, 112.0601 >>
	mPoppy.fDir = 50.6254 //248.7763  
	
	mShagger.vPos = << -31.5840, 306.8474, 111.6949 >> //<< -48.7962, 356.5141, 112.0601 >>
	mShagger.fDir = 21.4212 //113.0651

	mHotelStaff[0].vPos =  << 1.1046, 300.3212, 109.9190 >> //<< -48.3062, 352.5123, 112.0579 >>  //<< -61.0610, 357.8770, 112.0710 >>
	mHotelStaff[0].fDir = 335.6462 //63.5233  //245.6784
	//mHotelStaff[0].vPos =  << -4.4094, 294.3342, 109.9184 >> //<< -48.3062, 352.5123, 112.0579 >>  //<< -61.0610, 357.8770, 112.0710 >>
	//mHotelStaff[0].fDir = 239.4434 //63.5233  //245.6784
	
	mHotelStaff[1].vPos = << 16.9840, 336.9486, 110.3190 >> //<< -60.4740, 339.8489, 110.7652 >>  // //<< -48.2065, 347.9851, 114.7495 >>
	mHotelStaff[1].fDir = 157.6965 //333.0786 // //336.7244
	//mHotelStaff[1].vPos = << 12.8504, 309.3915, 109.9191 >> //<< -60.4740, 339.8489, 110.7652 >>  // //<< -48.2065, 347.9851, 114.7495 >>
	//mHotelStaff[1].fDir = 332.7813 //333.0786 // //336.7244
	
	mBodyGuard.vPos = <<-46.9907, 328.4976, 111.6962>>//<<-55.2321, 316.8002, 111.6962>>//
	mBodyGuard.fDir = 303.7287//319.4445//
	
	mnHotelStaff[0] = S_F_M_Maid_01
	eStaffState[0] = S_NULL
	
	mnHotelStaff[1] = S_M_Y_Busboy_01 //S_M_Y_Valet_01 //Valet has a slight save in memory 
	eStaffState[1] = S_NULL
	
	POPULATE_STAFF_WORK_POINTS()
ENDPROC

/// PURPOSE:
///    Initialises a new CHASE_SOUND struct and returns it 
/// PARAMS:
///    iExecuteTime - The time during the chase that the sound should be played
///    sName - The name of the sound
///    Origin - The origin is an enum that is used in a switch to choose the entity to play the sound from
/// RETURNS:
///    An initialised CHASE_SOUND struct
FUNC CHASE_SOUND FILL_SOUNDS(INT iExecuteTime, STRING sName, CHASE_SOUND_ORIGIN Origin)
	CHASE_SOUND NewSound
	
	NewSound.iExecuteTime = iExecuteTime
	NewSound.sSoundName = sName
	NewSound.eOrigin = Origin
	NewSound.bPlayed = FALSE
	
	return NewSound
ENDFUNC

/// PURPOSE:
///    Initialises the array of final chase sounds
PROC POPULATE_SOUNDS()
	mFinalChaseSound[0] = FILL_SOUNDS(91798, "BUILDING_SITE_CRASH", CSO_PLAYER) //player construc crash
	mFinalChaseSound[1] = FILL_SOUNDS(92411, "BUILDING_SITE_CRASH", CSO_POPPY) //poppy construct crash

ENDPROC

///PURPOSE: 
///    Function to group all population functions together
///    So that this function is called in the main init of the mission
PROC POPULATE_STUFF()
	BODYGUARD_CAR_MODELS()
	POPULATE_PEDS()
	#IF IS_DEBUG_BUILD
		CHASE_ROUTE()
	#ENDIF
	BEV_ROUTE()
	//POPULATE_PROPS()
	POPULATE_ALL_CONVOS()
	POPULATE_SOUNDS()
ENDPROC

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STAGE stage)
	//STOP_CUTSCENE_IMMEDIATELY()
	//WAIT_FOR_CUTSCENE_TO_STOP()
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionStage = stage 
	IF eMissionStage = MS_INIT
		eMissionStage = MS_INTRO
	ENDIF
	iDoFixForStream = 0
	bLoadingFinIntro = FALSE
	bObjectiveShown = FALSE
	bDebugSkipping = TRUE
	eState = SS_INIT
	eCutsceneState = eCutInit
	CLEANUP() //delete everything
ENDPROC

///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()

	//VEHICLE_INDEX viTemp

	DO_FOCUS_PUSH()

	SWITCH eState
		CASE SS_INIT
			POPULATE_STUFF()
		
			//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)	
			
			IF SETUP_MISSION_STAGE(MS_INIT) 
				
				SERVICES_TOGGLE(FALSE)

				SET_PED_NON_CREATION_AREA(vAlleyPoint1, vAlleyPoint2)
				
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
				ENDIF
				
				//GuardSpawn = ADD_SCENARIO_BLOCKING_AREA(<< -54.0, 348.0, 108.0 >>,<< -48.0, 357.0, 120.0 >>)
				sbiPoppy = ADD_SCENARIO_BLOCKING_AREA(<<-66.2521, 288.0, 109.0>>,<<-14.3327, 334.0, 114.0>>)  //<<-66.2521, 300.7945, 109.0>>,<<-14.3327, 313.3854, 114.0>>
				sbiClipboardGuys = ADD_SCENARIO_BLOCKING_AREA(<<-67.5738, 297.3521, 105.1975>> - <<10,10,10>>,<<-67.5738, 297.3521, 105.1975>> + <<10,10,10>>)
				sbiBuilders = ADD_SCENARIO_BLOCKING_AREA(<<82.1350, -391.7834, 39.8813>>-<<80,80,20>>,<<82.1350, -391.7834, 39.8813>>+<<80,80,20>>)
				
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "INTRO"
					s_skip_menu[1].sTxtLabel = "FOLLOW BEVERLY" 
					s_skip_menu[2].sTxtLabel = "CAMERA TUTORIAL"
					s_skip_menu[3].sTxtLabel = "FILM POPPY"                
					s_skip_menu[4].sTxtLabel = "GET IN ESCAPE CAR"
					s_skip_menu[5].sTxtLabel = "CHASE"
					s_skip_menu[6].sTxtLabel = "OUTRO"
					s_skip_menu[7].sTxtLabel = "UBA REC"
					s_skip_menu[8].sTxtLabel = "POPPY AND UBA"
					s_skip_menu[9].sTxtLabel = "SET PIECE RECORDING"
					s_skip_menu[10].sTxtLabel = "CHASE - Cam rotation locked to car"
					s_skip_menu[11].sTxtLabel = "JUMP TO END OF CHASE"
				#ENDIF
				
				//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				IF IS_REPLAY_IN_PROGRESS()
					
					//bReplayUsed = TRUE
					
					IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
						IF NOT IS_ENTITY_DEAD(sRCLauncherDataLocal.objID[0])
							DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(objCam)
						IF NOT IS_ENTITY_DEAD(objCam)
							DELETE_OBJECT(objCam)
						ENDIF
					ENDIF
					
					iReplayStage = GET_REPLAY_MID_MISSION_STAGE()	
						
					IF g_bShitskipAccepted = TRUE
						iReplayStage++ // player is skipping this stage
					ENDIF
				
					SWITCH iReplayStage
						
						CASE 0
							//CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-88.6147, 303.8292, 105.9208>>, 245.3452, FALSE, FALSE, TRUE, TRUE, FALSE) //B*1727319 - This is not needed, SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN handles it.
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0,TRUE,CHAR_FRANKLIN)
							CLEAR_AREA_OF_PEDS(<< -28.7403, 303.3516, 111.6961 >>, 40.0)// Area around poppy
							CLEAR_AREA_OF_PEDS(<< -17.3155, 340.0973, 110.4743 >>, 50.0)// Area around pool
							CLEAR_AREA_OF_PEDS(<< 12.2397, 319.1199, 109.8446 >>, 50.0)// Area around Hotel staff
							CLEAR_AREA_OF_VEHICLES(<< -12.2142, 280.2522, 107.3232 >>, 20)
							CLEAR_AREA_OF_VEHICLES(<< -48.1975, 352.4658, 112.0536 >>, 5)
							START_REPLAY_SETUP(<<-72.5612, 298.2493, 105.2336>>, 247.2199)
							JUMP_TO_STAGE(MS_FOLLOW_BEV_ROAD)
						BREAK
						
						CASE CP_FILMING
							bDumpedBank = TRUE
							START_REPLAY_SETUP(<<-33.6810, 320.9442, 111.6958>>, 182.6031)
							JUMP_TO_STAGE(MS_CAMERA_TUTORIAL)
						BREAK
						
						CASE CP_GET_TO_CAR
							bDumpedBank = TRUE
							START_REPLAY_SETUP(<<-33.1227, 318.9001, 111.6958>>, 154.1067)
							JUMP_TO_STAGE(MS_GET_IN_ESCAPE_CAR)
						BREAK
						
						CASE CP_CHASE
							bDumpedBank = TRUE
							START_REPLAY_SETUP(<<-79.0609, 296.3315, 105.3698>>, 69.2271)
							JUMP_TO_STAGE(MS_CAR_CHASE)
						BREAK
						
						CASE CP_OUTRO
							bDumpedBank = TRUE
							START_REPLAY_SETUP(<< -69.2061, -523.7703, 39.2051 >>, 163.9675)
							JUMP_TO_STAGE(MS_OUTRO)
						BREAK

					ENDSWITCH	
					
				ELSE
					IF SETUP_STAGE_REQUIREMENTS(RQ_CUTSCENE, vSafeVec)
						eState = SS_CLEANUP
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

//*************************************************************************************************************************************************
//													: MISSION FUNCS :
//*************************************************************************************************************************************************

/// PURPOSE:
///    Checks to see if a ped has seen a hated player
/// PARAMS:
///    ped - The ped checking to see if they have seen a hated player
/// RETURNS:
///    TRUE if the ped has seen the player and hates him
FUNC BOOL HAS_PLAYER_TOUCHED_PED(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		IF IS_ENTITY_TOUCHING_ENTITY(ped, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gives a ped an attack order after the player has been spotted
///    Only called when the player fails the mission for getting spotted or attacking a ped
///    and possibly when the player has completed the sex scene section
/// PARAMS:
///    ped - The ped to give the order to
PROC GIVE_ATTACK_ORDER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		CLEAR_PED_TASKS(ped)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(ped, 100)
		SET_PED_KEEP_TASK(ped, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped a flee order after the player has been spotted
///    Only called when the player fails the mission for getting spotted or attacking a ped
/// PARAMS:
///    ped - The ped to give the order to
PROC GIVE_FLEE_ORDER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		IF IS_PED_IN_ANY_VEHICLE(ped, TRUE)
			IF IS_VEHICLE_OK(GET_VEHICLE_PED_IS_IN(ped, TRUE))
				TASK_VEHICLE_MISSION_PED_TARGET(ped, GET_VEHICLE_PED_IS_IN(ped, TRUE), PLAYER_PED_ID(), MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS,100,  100)
			ENDIF
		ELSE			
			SET_PED_FLEE_ATTRIBUTES(ped, FA_CAN_SCREAM, TRUE)
			IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
				CLEAR_PED_TASKS(ped)
				TASK_SMART_FLEE_PED(ped, PLAYER_PED_ID(),100,-1, FALSE,TRUE)
			ENDIF
		ENDIF
		SET_PED_KEEP_TASK(ped, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls when the player can fail for being spotted by mission peds
///    and makes the peds respond in the appropriate manner
PROC MONITER_PEDS_PERCEPTION()
	IF NOT bSpotted
	AND NOT bBudThreat
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF eMissionStage <> MS_FAILED
				
				IF IS_PED_UNINJURED(mBodyGuard.piPed)	
					IF HAS_PLAYER_THREATENED_PED(mBodyGuard.piPed)
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
							//GIVE_FLEE_ORDER(mBodyGuard.piPed)
							TASK_SMART_FLEE_PED(mBodyGuard.piPed,PLAYER_PED_ID(),300,-1)
						ELSE
							//GIVE_ATTACK_ORDER(mBodyGuard.piPed)
							TASK_COMBAT_PED(mBodyGuard.piPed,PLAYER_PED_ID())
						ENDIF
						bSpotted = TRUE
					ENDIF
				ENDIF
				
				IF IS_PED_UNINJURED(mPoppy.piPed)
					IF IS_PED_UNINJURED(mShagger.piPed)
						IF HAS_PLAYER_THREATENED_PED(mPoppy.piPed)
							//IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
								//GIVE_ATTACK_ORDER(mPoppy.piPed)
								//GIVE_ATTACK_ORDER(mShagger.piPed)
								//TASK_COMBAT_PED(mPoppy.piPed,PLAYER_PED_ID())
								//TASK_GO_TO_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1,12)
								//TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
							//ELSE
								//GIVE_FLEE_ORDER(mPoppy.piPed)
								//GIVE_FLEE_ORDER(mShagger.piPed)
								//TASK_SMART_FLEE_PED(mPoppy.piPed,PLAYER_PED_ID(),300,-1)
								//TASK_SMART_FLEE_PED(mShagger.piPed,PLAYER_PED_ID(),300,-1)
							//ENDIF
							bSpotted = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_PED_UNINJURED(mPoppy.piPed)
				IF IS_PLAYER_AT_ENTITY(mPoppy.piPed, <<10,10,1.5>>) 
				AND eMissionStage <> MS_FAILED 
				AND NOT bCloseTooWarning
					CLEAR_PRINTS()
					//PRINT_NOW("PAP2_03", 5000,0)
					bCloseTooWarning = TRUE
				ENDIF
			ENDIF

			IF IS_PED_UNINJURED(mPoppy.piPed)
				IF IS_PED_UNINJURED(mShagger.piPed)
					IF IS_PLAYER_AT_ENTITY(mPoppy.piPed, <<6,6,2.5>>)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -42.5662, 301.8173, 111.6957 >>, << -37.5690, 313.1216, 111.6957 >>, 5, FALSE, FALSE)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -44.4977, 296.7267, 111.5461 >>, << -23.9906, 291.6258, 111.5461 >>, 9.5, FALSE, FALSE)
						FREEZE_ENTITY_POSITION(mPoppy.piPed, FALSE)
						FREEZE_ENTITY_POSITION(mShagger.piPed, FALSE)
						/*
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
							//GIVE_ATTACK_ORDER(mPoppy.piPed)
							//GIVE_ATTACK_ORDER(mShagger.piPed)
							TASK_COMBAT_PED(mPoppy.piPed,PLAYER_PED_ID())
							//TASK_GO_TO_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1,12)
							TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
						ELSE
							//GIVE_FLEE_ORDER(mPoppy.piPed)
							//GIVE_FLEE_ORDER(mShagger.piPed)
							TASK_SMART_FLEE_PED(mPoppy.piPed,PLAYER_PED_ID(),300,-1)
							TASK_SMART_FLEE_PED(mShagger.piPed,PLAYER_PED_ID(),300,-1)
						ENDIF
						*/
						bSpotted = TRUE
					ENDIF
					//IF iCoverSeq = 0 
					IF bBevSentToCover = FALSE
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-33.333542,324.251465,111.674744>>, <<-36.291592,317.228241,115.445808>>, 7.750000, FALSE, FALSE)
						bSpotted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bSpotted
		OR bBudThreat
			KILL_ANY_CONVERSATION()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player is running around inside the angled area that the hotel staff are in 
///    then we increment a timer. if the timer is greater than MAX_RUNNING_TIME_BEFORE_ATTENTION
///    then the player has been spotted (bSpotted = true). 
PROC MONITER_PLAYER_RUNNING_AROUND()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_RUNNING(PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),  << 20.4540, 334.6303, 109.9198 >>,  << -1.6518, 292.0331, 109.9198 >>, 20, FALSE, FALSE)
			iRunningTime++
			SK_PRINT_INT("RUNNING ========== " , iRunningTime)
			IF iRunningTime > MAX_RUNNING_TIME_BEFORE_ATTENTION
				KILL_ANY_CONVERSATION()
				CLEAR_PRINTS()
				
				bSpotted = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Increase beverly's speed if the player is too close to him 
///    -- Needs testing to see if this is still needed [TODO]
PROC MONITER_RUBBER_BANDING()
	IF IS_PED_UNINJURED(mBuddy.piPed)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<11,11,5>>)
			SET_PED_MOVE_RATE_OVERRIDE(mBuddy.piPed, 1.15)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player shoots in the area or enters the area in a vehicle 
///    fail him
PROC MONITER_PLAYER_FAILING()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), vAlleyPoint1, vAlleyPoint2, FALSE)
			MISSION_FAILED(FR_SPOTTED)
		ENDIF	
	ENDIF

	IF PLAYER_ENTERED_AREA_IN_VEHICLE(vAlleyPoint1,vAlleyPoint2)
		MISSION_FAILED(FR_SPOTTED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Used while beverly is traveling along his route through the hotel grounds
///    Depending on what position he is traveling to (iBevRoute) we return a PEDMOVEBLENDRATIO
/// RETURNS:
///    A FLOAT set uning one of the PEDMOVEBLENDRATIO defines
FUNC FLOAT SET_SPEED_FOR_ROUTE()
	FLOAT fSpeed = PEDMOVEBLENDRATIO_WALK

	IF iBevRoute = ROUTE_POOL_STAIRS
		fSpeed = PEDMOVEBLENDRATIO_RUN
	ELIF iBevRoute = ROUTE_STAIRS_1_BOTTOM 
		fSpeed = PEDMOVE_SPRINT
	ENDIF
	
	RETURN fSpeed
ENDFUNC

/// PURPOSE:
///    If beverly is waiting for the player at a point and the inactivity timer has reached its limit
///    then we play a wave anim - "gestures@male", "come_here"
PROC PLAY_WAVE_TO_ANIM()
	IF NOT bWaving
		IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
			SEQUENCE_INDEX Seq
			OPEN_SEQUENCE_TASK(Seq)
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
				IF NOT IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<10,10,10>>)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					//TASK_PLAY_ANIM(NULL, "gestures@male", "come_here", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
				ENDIF
				
				IF iBevRoute = ROUTE_POPPY
					TASK_TURN_PED_TO_FACE_COORD(NULL, vSexScene)
				ENDIF
			CLOSE_SEQUENCE_TASK(Seq)
			TASK_PERFORM_SEQUENCE(mBuddy.piPed, Seq)
			CLEAR_SEQUENCE_TASK(Seq)
			
		ENDIF
		bWaving = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Monitors where in the route through the hotel grounds beverly is. 
///    If he has reached the current point then we increment iBevRoute and give him a FOLLOW_NAV_MESH task
///    to the new point. The arguments are used to have beverly wait for the player at a 
///    specific point.
///    -- There is a bug in this function, the relationship between bWaitAtPoint and iPointToWaitAt
///    needs defining better [TODO]
/// PARAMS:
///    bWaitAtPoint - If true bev will wait at a point
///    iPointToWaitAt - The point bev should wait at
PROC PROGRESS_BEV_ROUTE(BOOL bWaitAtPoint = FALSE, INT iPointToWaitAt = -1)
	
	VECTOR vBevWaitRadius

	IF IS_PED_UNINJURED(mBuddy.piPed)
	AND NOT bBudThreat
	AND NOT bBevSentToCover	
		IF iBevRoute < MAX_BEV_WAYPOINTS
			
			IF bOkForBevToTurn = TRUE	
				
				//IF GET_ENTITY_SPEED(mBuddy.piPed) < 0.2	
					//IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
					//OR GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> WAITING_TO_START_TASK
					//IF NOT IS_PED_FACING_PED(mBuddy.piPed,PLAYER_PED_ID(),90)	//30
					//	TASK_TURN_PED_TO_FACE_ENTITY(mBuddy.piPed, PLAYER_PED_ID(),-1)
					//ENDIF
					//IF IS_PED_FACING_PED(mBuddy.piPed,PLAYER_PED_ID(),30)	//30	
					//	CLEAR_PED_TASKS(mBuddy.piPed)
					//ENDIF
				//ENDIF

			ENDIF
			
			IF iBevRoute < ROUTE_STAIRS_1_MIDDLE
				vBevWaitRadius = <<4,4,2.5>>
			ELSE
				vBevWaitRadius = <<2,2,2.5>>
			ENDIF
			
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)	
				IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) >= 105
				AND GET_GAME_TIMER() > iTimerBevSetOffWalking + 1200
					
					IF bBevStealth = FALSE
						//ALTER_STEALTH(mBuddy.piPed, TRUE)
					ENDIF
					IF NOT GET_PED_STEALTH_MOVEMENT(mBuddy.piPed)
						bSwapCamPos = TRUE
						SET_PED_STEALTH_MOVEMENT(mBuddy.piPed,TRUE,"DEFAULT_ACTION")
						TASK_FOLLOW_WAYPOINT_RECORDING(mBuddy.piPed,sBeverlyPedRecording2,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_ALLOW_STEERING_AROUND_PEDS)
						SK_PRINT("MAKE BEV STEALTH")
					ELSE
						IF bSwapCamPos	
							//DETACH_ENTITY(objCam)  //COMMENT IN FOR OLD
							//ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) //COMMENT IN FOR OLD
							bSwapCamPos = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) >= 107
					//RESET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed)  //COMMENT IN FOR OLD
				ENDIF
				
				IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 100
					SET_PED_RESET_FLAG(mBuddy.piPed,PRF_UseProbeSlopeStairsDetection,TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_AT_COORD(mBuddy.piPed, vBevRoute[iBevRoute], vBevWaitRadius)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)	
				AND NOT WAYPOINT_PLAYBACK_GET_IS_PAUSED(mBuddy.piPed)	
					/*
					IF iBevRoute = ROUTE_POPPY
						IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) >= 107  //105
							ALTER_STEALTH(mBuddy.piPed, TRUE)
						ENDIF
					ENDIF
					*/
					IF iBevRoute < ROUTE_STAIRS_1_MIDDLE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
						AND GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 41	
							WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(mBuddy.piPed)
						ELSE		
							IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 1.9
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,5.0)
								AND GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 35
								AND NOT IS_PED_FACING_PED(mBuddy.piPed,PLAYER_PED_ID(),90)	//120
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,2.0)
								ELSE
									WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(mBuddy.piPed)
								ENDIF
							ELSE
								IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 35//7	
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,1.0)
								ENDIF
								/*
								IF NOT IS_PED_FACING_PED(mBuddy.piPed,PLAYER_PED_ID(),90)  //120	
								OR GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 35//7	
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,1.0)
								ELSE
									WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(mBuddy.piPed)
								ENDIF
								*/
							ENDIF
						ENDIF
					ELSE	
						IF IS_PED_UNINJURED(mHotelStaff[0].piPed)	
							IF IS_ENTITY_IN_RANGE_ENTITY(mBuddy.piPed,mHotelStaff[0].piPed,25)	
								TASK_LOOK_AT_ENTITY(mBuddy.piPed,mHotelStaff[0].piPed,8000)
							ENDIF
							IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sBeverlyPedRecording2,GET_ENTITY_COORDS(PLAYER_PED_ID()),iClosestWaypointToPlayer)
								IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < iClosestWaypointToPlayer
								AND NOT IS_ENTITY_IN_RANGE_ENTITY(mBuddy.piPed,mHotelStaff[0].piPed,8)	
									WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,2.0)
								ELSE
									WAYPOINT_PLAYBACK_USE_DEFAULT_SPEED(mBuddy.piPed)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE	
					IF IS_ENTITY_IN_RANGE_ENTITY(mBuddy.piPed,PLAYER_PED_ID(),7)	
						//IF HAS_CLIP_SET_LOADED("move_m@casual@f")	
						IF HAS_CLIP_SET_LOADED(sWeaponMoveClipset)	
							IF NOT IS_ENTITY_IN_ANGLED_AREA(mBuddy.piPed, <<-38.226002,320.318573,111.051186>>, <<-36.413120,323.970398,113.695358>>, 4.750000)
								TASK_CLEAR_LOOK_AT(mBuddy.piPed)
								//SET_PED_MOVEMENT_CLIPSET(mBuddy.piPed,"move_m@casual@f")
								SET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed,sWeaponMoveClipset)
								TASK_FOLLOW_WAYPOINT_RECORDING(mBuddy.piPed,sBeverlyPedRecording2,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
								iTimerBevSetOffWalking = GET_GAME_TIMER()
								bOkForBevToTurn = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ELSE
				IF NOT bWaitAtPoint
					IF iBevRoute < MAX_BEV_WAYPOINTS
						iBevRoute++
					ENDIF
				ELSE
					IF iPointToWaitAt > -1
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)	
						AND GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) >= 112
							/*
							IF NOT IS_PED_IN_COVER(mBuddy.piPed)	
								IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_SEEK_COVER_FROM_POS) <> PERFORMING_TASK
									//TASK_SEEK_COVER_TO_COORDS(mBuddy.piPed,<< -32.3128, 320.5045, 111.6957 >>,<< -24.1590, 312.4199, 111.5455 >>,-1,TRUE)
									TASK_SEEK_COVER_FROM_POS(mBuddy.piPed,<< -24.1173, 304.6172, 111.6957 >>,-1,TRUE)
								ENDIF
							ENDIF
							*/
							/*
							IF NOT IS_PED_IN_COVER(mBuddy.piPed)	
								IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> PERFORMING_TASK
									//TASK_SEEK_COVER_TO_COORDS(mBuddy.piPed,<< -33.1418, 318.9297, 111.6957 >>,<< -24.1590, 312.4199, 111.5455 >>,-1,TRUE)
									TASK_PUT_PED_DIRECTLY_INTO_COVER(mBuddy.piPed,<<-37.24, 321.48, 112.31>>,-1,TRUE,0,TRUE,FALSE,NULL)
									//TASK_SEEK_COVER_FROM_POS(mBuddy.piPed,<< -24.1173, 304.6172, 111.6957 >>,-1,TRUE)
								ENDIF
							ENDIF
							*/
						ELSE
							IF NOT IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<7.0,7.0,3.5>>)
							OR NOT IS_PLAYER_AT_COORDS(vBevRoute[iBevRoute], <<13,13,5>>)
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-41.702267,336.294037,111.171974>>, <<-17.278280,324.682190,115.137947>>, 12.500000)	
									IF NOT IS_ENTITY_IN_ARC_2D(mBuddy.piPed, PLAYER_PED_ID(), 40)
									AND iBevRoute <= ROUTE_POOL_STAIRS
										IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)	
											TASK_LOOK_AT_ENTITY(mBuddy.piPed,PLAYER_PED_ID(),-1)
											//WAYPOINT_PLAYBACK_PAUSE(mBuddy.piPed,FALSE)
											WAYPOINT_PLAYBACK_PAUSE(mBuddy.piPed,TRUE)
										ENDIF
										bOkForBevToTurn = TRUE
									ENDIF
								ENDIF
								
								//Beverly is waiting for the player so we add
								//time to the Inactivity timer 
								IF bExpireInactive
									iInactivityTimer++
									IF iInactivityTimer > 125
										bExpireInactive = FALSE //re-enabled 
										iInactivityTimer = 0
										//PLAY_WAVE_TO_ANIM()
									ENDIF
								ELSE
									IF bWaving
										iWaveInactivityTimer++
										IF iWaveInactivityTimer > 220
											bWaving = FALSE
											iWaveInactivityTimer = 0
											//PLAY_WAVE_TO_ANIM()
										ENDIF
									ENDIF
								ENDIF
							ELSE
								iInactivityTimer = 0
								IF iBevRoute < MAX_BEV_WAYPOINTS
									iBevRoute++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Monitors whether the current MS_FOLLOW_BEV_... state should clean up.
///    This is defined as whether beverly or the player is within a radius of a
///    specified point. We can also specify that the state shouldn't continue unless
///    Bev and the player are together at the specific point.
///    If beverly is waiting for the player (bCheckPlayerAndBevTogether) then we moniter inactivity 
///    this is where the Wave animation is triggered from
/// PARAMS:
///    iPointIndex - The index of the point to check
///    vRadius - The size of the check to perform
///    bCheckPlayerAndBevTogether - Should we check for the player and bev being together at the point
/// RETURNS:
///    TRUE if the player or beverly are at the specified point
///    TRUE if the player and beverly are at the specified point but only if bCheckPlayerAndBevTogether
FUNC BOOL SHOULD_PROGRESS_FOLLOW_STATE(INT iPointIndex, VECTOR vRadius, BOOL bCheckPlayerAndBevTogether = FALSE)
	IF NOT bCheckPlayerAndBevTogether
		IF IS_ENTITY_AT_COORD(mBuddy.piPed, vBevRoute[iPointIndex], vRadius)
		OR IS_PLAYER_AT_COORDS(vBevRoute[iPointIndex], vRadius)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_ENTITY_AT_COORD(mBuddy.piPed, vBevRoute[iPointIndex], <<2.5,2.5,2.5>> )  //<<3.0,2.0,2.5>>
			IF IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<2.5,2.5,2.5>>)  //<<5,2,2.5>>
				iInactivityTimer = 0
				RETURN TRUE
			ELSE
				//Beverly is waiting for the player so we add
				//time to the Inactivity timer 
				IF bExpireInactive
				AND bObjectiveShown
					iInactivityTimer++
					IF iInactivityTimer > 100
						bExpireInactive = FALSE //re-enabled 
						iInactivityTimer = 0
					ENDIF
				ENDIF
				
				IF NOT bWaving
					iWaveInactivityTimer++
					IF iWaveInactivityTimer > 175
						PLAY_WAVE_TO_ANIM()
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
						bWaving = FALSE
						iWaveInactivityTimer = 0
					ENDIF
				ENDIF
				IF bPlayerSentToCover = FALSE		
					PRINT_OBJ("PAP2_08") //Go over to ~b~Beverly.
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Triggers a conversation, shows objective and then plays another conversation.
///    The first conversation in this function is displayed with out subtitles 
///    Then the objective is printed
///    Then the next convo is started after the objective is finished this has subtitles
///    
/// PARAMS:
///    &bExpire - A global bool is passed to the func which we write back to when the first conversation has been triggered
///    &bExpireAfterConv - A global bool is passed to the func which we write back to when the second conversation has been triggered
///    sConvo - The Root of the first conversation to play
///    sConvToPlayAfterObj - The Root of the second conversation to play
///    sObj - The objective to print
PROC DO_BEV_CONVO( BOOL &bExpire, BOOL &bExpireAfterConv, STRING sConvo, STRING sConvToPlayAfterObj, STRING sObj )
	IF NOT bObjectiveShown
		IF NOT bExpire
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
			ENDIF
		ELSE
			PRINT_OBJ(sObj) 
		ENDIF
	ELSE
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sObj)
			IF NOT bExpireAfterConv
				IF IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<10,10,10>>)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
						bExpireAfterConv = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvToPlayAfterObj, CONV_PRIORITY_MEDIUM)
					ENDIF
				ELIF IS_PLAYER_AT_COORDS(vBevRoute[ROUTE_STAIRS_1_MIDDLE], <<10,10,10>>)
					bExpireAfterConv = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///		if the player is further away then a set distance and the staff conversation is 
///    	still playing then we kill that convo with out finishing the last line
PROC STOP_STAFF_CONV_CHECK()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) > 28  //23
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-4.814245,316.417358,109.398376>>, <<2.642797,313.337494,113.155197>>, 5.500000)
			TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			IF NOT IS_STRING_NULL_OR_EMPTY(root)
				IF ARE_STRINGS_EQUAL(root, "PAP2_STAFF")
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls playing the staff conversation and includes the convo where bev tells the player to 
///    pipe down because he can hear someone. This also starts the maid using her phone
/// PARAMS:
///    &bExpire - A global script bool is passed to the function which we write back too when the first conversation is triggered
///    &bExpireSecondConv - A global script bool is passed to the function which we write back too when the second conversation is triggered
PROC DO_STAFF_CONVO(BOOL &bExpire, BOOL &bExpireSecondConv)
	IF NOT bExpire
		IF iBevRoute = ROUTE_STAIRS_1_TOP
		OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mHotelStaff[0].piPed,12)	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mHotelStaff[0].piPed,12)	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_STAIRS", CONV_PRIORITY_MEDIUM)				
						IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
							//TASK_USE_MOBILE_PHONE_TIMED(mHotelStaff[0].piPed, 19000) //14000
							//TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed,TRUE)
							iTimerMaidUsingPhone = GET_GAME_TIMER()
							eStaffState[0] = S_WAIT_ACTIVE
							eStaffState[1] = S_WAIT_ACTIVE
							bExpire = TRUE
						ENDIF	
					ENDIF
				ELSE
					IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
						//TASK_USE_MOBILE_PHONE_TIMED(mHotelStaff[0].piPed, 14000) //MAID -- this needs to be triggerd from dialogue not timed 
						//TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed,TRUE) //MAID -- this needs to be triggerd from dialogue not timed 
						iTimerMaidUsingPhone = GET_GAME_TIMER()
						eStaffState[0] = S_WAIT_ACTIVE
						eStaffState[1] = S_WAIT_ACTIVE
						bExpire = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT bExpireSecondConv
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
				IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) < 23
					bExpireSecondConv =  CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_STAFF", CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF	
		ELSE
			STOP_STAFF_CONV_CHECK()
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    This plays the conversations needed for when the player and bev are 
///    on the last straight to poppy. The first line is said just before running
///    across the pool area. And then the second is as they round the final set of
///    stairs. Contains distance checks
/// PARAMS:
///    &bExpire - A global script bool is passed to the function which we write back too when the first conversation is triggered
///    &bExpireSecondConv - A global script bool is passed to the function which we write back too when the second conversation is triggered
PROC DO_POPPY_CONVO(BOOL &bExpire, BOOL &bExpireSecondConv)
	
	IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)
		IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) >= 99
			IF NOT bExpire
				bExpire = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bExpire

		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) <= 16
			AND NOT IS_PED_FACING_PED(mBuddy.piPed, PLAYER_PED_ID(),90)	
				bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_GO", CONV_PRIORITY_HIGH)				
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) >= 23
				TEXT_LABEL_23 lable =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
				IF NOT IS_STRING_NULL_OR_EMPTY(lable)
					IF ARE_STRINGS_EQUAL(lable, "PAP2_STAFF")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF iBevRoute = ROUTE_POPPY
			IF NOT bExpireSecondConv
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
					bExpireSecondConv = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_PREFILM", CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays chatter for Poppy and her crew
PROC DO_ESCAPE_CHATTER_CONV()
	
	//IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, viPlayerCar) > 15
	//AND NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
	IF iSeqBevRagdoll < 3
		IF bEscapeConvExpired1
			IF GET_GAME_TIMER() > iTimerStartStage + 5000
			AND GET_GAME_TIMER() > iTimerEscapeChatter + 5000
				IF iCurrentLine < MAX_ESCAPE_CHATTER_LINES
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
					AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_00")
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ESCCHAT", CONV_PRIORITY_LOW)
							iCurrentLine++
							iTimerEscapeChatter = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ELSE
					//Possibly set a flag here if this needs turning off
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC DO_BEV_SHOUTING()

	IF iBevShoutLines < 3	
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
			IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, viPlayerCar) >= 15
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BSH", CONV_PRIORITY_MEDIUM)  //Fucking run!
					++iBevShoutLines
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_BEV_IDLE_AT_FILM_POS_LINES()

	IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_08")	
	AND NOT bPlayerSentToCover	
		IF iBevIdleLines < 4	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				IF GET_GAME_TIMER()	> iTimerBevIdleLines + 10000
					IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) < 30
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_INACT2", CONV_PRIORITY_MEDIUM)  //Quick, we need to get this on film!  
							iTimerBevIdleLines = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
							++iBevIdleLines
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				IF GET_GAME_TIMER()	> iTimerBevIdleLines + 10000
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FAILCAM", CONV_PRIORITY_MEDIUM)
					ENDIF
					MISSION_FAILED(FR_BADCAM)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    plays lines for beverly shouting at the player to get in the car. 
PROC DO_BEV_GET_IN_CAR_LINES()
	
	INT iSubs

	IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_00")		
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_06")
	//AND bExpireGetInCarText = TRUE	
		iSubs = ENUM_TO_INT(DISPLAY_SUBTITLES)
	ELSE
		iSubs = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
	ENDIF
	
	IF bExpireGetInCarText = FALSE
	AND GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, viPlayerCar) < 4
		iSubs = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar, TRUE)
		INT iTime = GET_GAME_TIMER()
		IF IS_PED_UNINJURED(mBuddy.piPed)
			IF bPlayerGettingInCar = FALSE	
				IF iTime > iGetInCarTimer + 6000
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-50.615166,329.184814,111.507881>>, <<-65.104324,297.998444,115.673424>>, 11.350000)
						
						IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, viPlayerCar) < 15
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								TEXT_LABEL_23 sRoot 
								sRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								IF NOT IS_STRING_NULL_OR_EMPTY(sRoot)
									IF ARE_STRINGS_EQUAL(sRoot, "PAP2_ESCCHAT")
										KILL_FACE_TO_FACE_CONVERSATION()
									ENDIF
								ENDIF
							ENDIF

							//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_00")
							//AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_06")
							IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) <= 60//12
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
									IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ESCCAR", CONV_PRIORITY_MEDIUM,INT_TO_ENUM(enumSubtitlesState,iSubs))
										iGetInCarTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player doesnt keep up with beverly for a period of time then 
///    bev will say a line when the player is with in a distance. The distance and the 
///    dialogue change when the pair are about to see poppy
PROC DO_INACTIVE_CONV()
	INT iDistance = 12
	STRING sLabel = "PAP2_INACT"

	IF iBevRoute = ROUTE_POPPY
		sLabel = "PAP2_INACT2"
		iDistance = 5
	ENDIF

	IF NOT bExpireInactive
		IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) < iDistance
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF iBevRoute > ROUTE_STAIRS_1_TOP
					bExpireInactive = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sLabel, CONV_PRIORITY_VERY_HIGH)
				ELSE
					bExpireInactive = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Loads and creates the props needed for the sexscene
PROC LOAD_AND_CREATE_PROPS()
	INT iCounter = 0
	IF NOT bCreatedProps
	AND bCanCreateProps
		FOR iCount = 0 TO (MAX_SEX_SCENE_PROPS - 1)
			IF SPAWN_OBJECT(mSexSceneProps[iCount].oiProp, mSexSceneProps[iCount].mnModle, mSexSceneProps[iCount].vPos, mSexSceneProps[iCount].vRot.z)
				iCounter++
				IF iCounter = MAX_SEX_SCENE_PROPS
					bCreatedProps = TRUE
					bCanCreateProps = FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE:
///    loads and creates poppy and the lover and starts their animations going
///    This happens here instead of the main stage setup function so it can be 
///    started when the player is close to when the pair are (memory management)
PROC LOAD_AND_CREATE_POP()
	
		IF bCanCreatePoppy
			IF SETUP_STAGE_REQUIREMENTS(RQ_ANIMS, vSafeVec)
				IF SETUP_STAGE_REQUIREMENTS(RQ_POPPY, mPoppy.vPos, mPoppy.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_SHAGGER, mShagger.vPos, mShagger.fDir)
					//IF bPlayerSentToCover = FALSE
					//	PLAY_POPPY_SHAGGER_ANIM(0)
					//ENDIF
					bCanCreatePoppy = FALSE
					bPoppyCreated = TRUE
				ENDIF
			ENDIF
		ENDIF

ENDPROC 

/// PURPOSE:
///    Plays a line telling the player off for bad filming during the sexscene
///    Every time a fail line is said we increase iNumFilmFails if this hits 
///    above MAX_FILM_FAILS then the player has failed.
///    The failing stuff is tracked else where (probably in the FILM_POPPY state)
PROC DO_BAD_FILM_CONV()
	#IF NOT IS_JAPANESE_BUILD
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)	
		AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2) > 0.108
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_07")
					IF iNumFilmFails < MAX_FILM_FAILS
						IF IS_PED_UNINJURED(mBuddy.piPed)	
							IF IS_ENTITY_ON_SCREEN(mBuddy.piPed)	
								IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_NOTME", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
									//SK_PRINT("TK************BEV SHOULD BE SPEAKING************")
									iNumFilmFails++ //increase the number of fails
									iTimerBadFilmConvo = GET_GAME_TIMER()
									iTimerBevTut = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF iBadFilmNoBev = 3
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM1", "PAP2_BADCAM1_4", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)//What are you filming? They're over there. 
										iBadFilmNoBev = 4
									ENDIF
								ELIF iBadFilmNoBev = 2
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM1", "PAP2_BADCAM1_3", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)//Please, don't let me down. I need this.   
										iBadFilmNoBev = 3
									ENDIF
								ELIF iBadFilmNoBev = 1
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM1", "PAP2_BADCAM1_2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)//Come on, man! Are you getting any of this?  
										iBadFilmNoBev = 2
									ENDIF
								ELIF iBadFilmNoBev = 0
									IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM1", "PAP2_BADCAM1_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)//Try pointing the camera towards the celebrity.
										iBadFilmNoBev = 1
									ENDIF
								ENDIF
								//SK_PRINT("TK************BEV SHOULD BE SPEAKING************")
								iNumFilmFails++ //increase the number of fails
								iTimerBadFilmConvo = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				//ENDIF	
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    Plays a line telling the player off for bad filming during the tutorial
///    Every time a fail line is said we increase iNumFilmFails if this hits 
///    above MAX_FILM_FAILS then the we force the turorial to finish, bForceTutFin = TRUE.   
PROC DO_BAD_FILM_CONV_TUT()
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_07")
	AND NOT bForceTutFin
		IF iNumFilmFails < MAX_FILM_FAILS
			IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM", CONV_PRIORITY_MEDIUM)
			//PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM1", sBadFilmLines[iNumFilmFails], CONV_PRIORITY_MEDIUM)
				iNumFilmFails++ //increase the number of fails
				iTimerBadFilmConvo = GET_GAME_TIMER()
				iTimerBevTut = GET_GAME_TIMER()
			ENDIF
		ELSE
			IF iNumFilmFails >= MAX_FILM_FAILS
				bForceTutFin = TRUE
				iNumFilmFails = 0
			ENDIF	
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    If the player points a gun at bev (and he sees) then this proc plays a line of dialogue and then fails the mission after
///    the line has finished
///    -- This might be incorrect behaviour for the failing mission standards! Needs looking at [TODO]
PROC DO_BEV_THRETENED_CONV()
	IF NOT bExpireBudThreatSpeech
		bExpireBudThreatSpeech = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FLEE", CONV_PRIORITY_VERY_HIGH)
	ELSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
				MISSION_FAILED(FR_ATTENTION)
			ELSE	
				MISSION_FAILED(FR_BUDDY_THREAT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    container function for switching between the tutorial bad lines and 
///    the sex scene
///    bUseFAce is a RAG bool that can be used so that the mission wont use the face tag stuff
///    -- This should probably not have the bUseFace tag bool and the bool should be removed [TODO]
PROC MONITER_FOR_BAD_FILMING()
	IF bUseFace		
		IF mBoxCol.G < 200
			IF GET_GAME_TIMER() > iTimerBadFilmConvo + GET_RANDOM_INT_IN_RANGE(6000,7000)
				IF eMissionStage <> MS_CAMERA_TUTORIAL
					DO_BAD_FILM_CONV()
				ELSE
					DO_BAD_FILM_CONV()
				ENDIF
			ENDIF
		ENDIF

	ELSE
		IF iFilmTime > 20 
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bExpireWarning
				//PRINT_NOW("PAP2_07",7500, 0) //Keep Poppy in shot.
				bExpireWarning = TRUE			
			ENDIF
			IF iNumFilmFails < MAX_FILM_FAILS
				iNumFilmFails++ //increase the number of fails
				iTimerBadFilmConvo = GET_GAME_TIMER()
			ENDIF
			DO_BAD_FILM_CONV()
		ENDIF
	ENDIF

	IF iNumFilmFails >= MAX_FILM_FAILS
		bPlayerFailed = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is zoomed in to poppy during the sex scene filming
///    Triggers a line of dialogue when the camera has not been zoomed in for 10s
/// PARAMS:
///    bZoomed - The bool to write back too 
PROC MONITER_FOR_NO_ZOOM(BOOL &bZoomed)
	IF DOES_CAM_EXIST(camMain)
		IF GET_CAM_FOV(camMain) > 13
			IF GET_GAME_TIMER() > iTimerStartStage + 10000
			AND GET_GAME_TIMER() > iTimerBevTut + 10000
				#IF NOT IS_JAPANESE_BUILD	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAMA", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						//SK_PRINT("TK************BEV SHOULD BE SPEAKING ZOOM************")
						iTimerBevTut = GET_GAME_TIMER()
					ENDIF
				#ENDIF
			ENDIF
			bZoomed = FALSE
		ELSE
			bZoomed = TRUE
			iTimerBevTut = GET_GAME_TIMER()
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    increment iFilmTime
PROC UPDATE_FILMING_TIME()
	IF bUseFace
		IF mBoxCol.G > 200
			iFilmTime += 1
		ENDIF
	ELSE
		iFilmTime += 1
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if bev is near his car 
///    if he is he's told to get in as the driver. We also clear his waypoint task
PROC CHECK_BUDDY_NEAR_CAR()
	IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
		IF IS_PED_UNINJURED(mBuddy.piPed)
		AND IS_ENTITY_ALIVE(viPlayerCar)
			IF iSeqBevRagdoll <> 3
				SET_PED_CAN_RAGDOLL(mBuddy.piPed,FALSE)
			ENDIF
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)
				IF iSeqBevRagdoll = 0
					IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) > 31
						iTimerBevRagdoll = GET_GAME_TIMER() 
						iSeqBevRagdoll = 1
					ENDIF
				ELIF iSeqBevRagdoll = 1
					IF GET_GAME_TIMER()	> iTimerBevRagdoll + 2000
					AND IS_ENTITY_IN_ANGLED_AREA(mBuddy.piPed, <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley		
						//SET_PED_TO_RAGDOLL(mBuddy.piPed,3000,3000,TASK_RELAX)
						SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", TRUE) 
						iSeqBevRagdoll = 2
					ENDIF
				ELIF iSeqBevRagdoll = 2
					IF NOT IS_PED_RAGDOLL(mBuddy.piPed)
						iSeqBevRagdoll = 3
					ENDIF
				ELIF iSeqBevRagdoll = 3
					IF IS_PED_GETTING_UP(mBuddy.piPed)	
					OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(mBuddy.piPed)	
						IF bBevHitHisHead = FALSE
							//IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FACE", CONV_PRIORITY_MEDIUM)
								bBevHitHisHead = TRUE
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_ENTITY_AT_ENTITY(mBuddy.piPed, viPlayerCar, <<20,20,2>>)
					IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
					AND GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_ENTER_VEHICLE) <> WAITING_TO_START_TASK
						//IF iSeqBevRagdoll = 2
						IF NOT IS_PED_RAGDOLL(mBuddy.piPed)
						AND NOT IS_PED_GETTING_UP(mBuddy.piPed)	
							WAYPOINT_PLAYBACK_PAUSE(mBuddy.piPed, FALSE)
							//CLEAR_PED_TASKS(mBuddy.piPed)
							TASK_ENTER_VEHICLE(mBuddy.piPed, viPlayerCar, DEFAULT_TIME_BEFORE_WARP)
							SET_AUDIO_FLAG("DisableAbortConversationForRagdoll", FALSE) 
							SET_PED_CAN_RAGDOLL(mBuddy.piPed,TRUE)
							iSeqBevRagdoll = 3
						ENDIF
					ENDIF
				ELSE	
					IF iSeqBevRagdoll = 0	
						IF GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) = 29
						//AND GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed) < 31
						AND NOT IS_ENTITY_IN_RANGE_ENTITY(mBuddy.piPed,PLAYER_PED_ID(),17)
						AND iCounterBevWaiting < 150	
							++iCounterBevWaiting
							IF IS_PED_UNINJURED(mBodyGuard.piPed)
								IF IS_ENTITY_IN_RANGE_ENTITY(mBodyGuard.piPed,mBuddy.piPed,10)
									iCounterBevWaiting = 600
								ENDIF
							ENDIF
							IF NOT WAYPOINT_PLAYBACK_GET_IS_PAUSED(mBuddy.piPed)	
								WAYPOINT_PLAYBACK_PAUSE(mBuddy.piPed,TRUE)
							ENDIF
						ELSE
							IF WAYPOINT_PLAYBACK_GET_IS_PAUSED(mBuddy.piPed)
								WAYPOINT_PLAYBACK_RESUME(mBuddy.piPed,FALSE,31)
								WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,3)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() > iTimerGetInCarStage + 30000
				SET_PED_CAN_RAGDOLL(mBuddy.piPed,TRUE)
				SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_AND_NOT_JUSTIN_IN_ALLEY()

	IF IS_PED_UNINJURED(mShagger.piPed)
	AND NOT IS_ENTITY_IN_ANGLED_AREA(mShagger.piPed, <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley	
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley	
		RETURN TRUE
	ENDIF

RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AND_NOT_BODYGUARD_IN_ALLEY()

	IF IS_PED_UNINJURED(mBodyGuard.piPed)
	AND NOT IS_ENTITY_IN_ANGLED_AREA(mBodyGuard.piPed, <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley	
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley	
		RETURN TRUE
	ENDIF

RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if Justin is near his car 
///    if he is he's told to get in as the driver
PROC CHECK_SHAGGER_NEAR_CAR()
	IF NOT IS_SAFE_PED_IN_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR] )
		IF IS_PED_UNINJURED(mShagger.piPed)
		AND IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
			//IF IS_ENTITY_AT_ENTITY(mShagger.piPed, viChaseCars[SHAGGER_CAR], <<5,5,2.5>>)
			//OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), viPlayerCar, <<20,20,2>>)	
			//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley	
			//OR IS_PLAYER_AND_NOT_JUSTIN_IN_ALLEY()
			IF bPlayerGettingInCar
				/*
				IF GET_SCRIPT_TASK_STATUS(mShagger.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					SEQUENCE_INDEX seq
					OPEN_SEQUENCE_TASK(seq)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-47.9138, 332.5389, 112.4607>>,3,-1)
						TASK_ENTER_VEHICLE(NULL, viChaseCars[SHAGGER_CAR], -1, VS_FRONT_RIGHT)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(mShagger.piPed,seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				*/
				IF bPlayerGettingInCar
				AND NOT IS_ENTITY_AT_COORD(mShagger.piPed,<<-63.2851, 327.3911, 109.5810>>,<<7,7,7>>)
					SAFE_TELEPORT_ENTITY(mShagger.piPed,<<-63.2851, 327.3911, 109.5810>>)
				ENDIF
				
			ELSE
				IF GET_SCRIPT_TASK_STATUS(mShagger.piPed, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
					TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if Poppy is near her car 
///    if she is she's told to get in as the driver
PROC CHECK_POPPY_NEAR_CAR()
	IF NOT IS_SAFE_PED_IN_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR] )
		IF IS_PED_UNINJURED(mPoppy.piPed)
		AND IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])
			IF NOT bPoppyGettingInCar
				IF IS_ENTITY_AT_ENTITY(mPoppy.piPed, viChaseCars[POPPY_CAR], <<5,5,2.5>>)
				OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), viPlayerCar, <<20,20,2>>)		
					bPoppyGettingInCar = TRUE
				ENDIF
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)	
				AND GET_PED_WAYPOINT_PROGRESS(mBuddy.piPed)	> 24 //11
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-43.129189,298.264679,111.394432>>, <<-67.070282,306.125580,114.898727>>, 9.250000)	
					bPoppyGettingInCar = TRUE
				ENDIF
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting")
					STOP_ANIM_TASK(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting",SLOW_BLEND_OUT)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					TASK_ENTER_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR],-1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if Bodyguard is near his car 
///    if he is he's told to get in as the driver
PROC CHECK_BODYGUARD_NEAR_CAR()
	IF NOT IS_SAFE_PED_IN_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR] )
		IF IS_PED_UNINJURED(mBodyGuard.piPed) AND IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])
			//IF IS_ENTITY_AT_ENTITY(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR], <<5,5,2>>)
			//OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), viPlayerCar, <<20,20,2>>)		
			//OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-67.827629,304.866913,104.543030>>, <<-76.682930,308.606079,115.562187>>, 14.750000)	//jumped into alley
			//OR IS_PLAYER_AND_NOT_BODYGUARD_IN_ALLEY()
			IF bPlayerGettingInCar
				/*
				IF GET_SCRIPT_TASK_STATUS(mBodyGuard.piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					SEQUENCE_INDEX seq
					OPEN_SEQUENCE_TASK(seq)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-47.9138, 332.5389, 112.4607>>,3,-1)
						TASK_ENTER_VEHICLE(NULL, viChaseCars[BODYGUARD_CAR], -1, VS_FRONT_RIGHT)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(mBodyGuard.piPed,seq)
					CLEAR_SEQUENCE_TASK(seq)
				ENDIF
				*/
				IF bPlayerGettingInCar
				AND NOT IS_ENTITY_AT_COORD(mBodyGuard.piPed,<<-68.9113, 314.4095, 107.8141>>,<<7,7,7>>)
					SAFE_TELEPORT_ENTITY(mBodyGuard.piPed,<<-68.9113, 314.4095, 107.8141>>)
				ENDIF
				
			ELSE
				IF GET_SCRIPT_TASK_STATUS(mBodyGuard.piPed, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
					TASK_COMBAT_PED(mBodyGuard.piPed,PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(mBodyGuard.piPed,PLAYER_PED_ID(),-1)
				ENDIF 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is near the getaway car and if he has pressed enter car input
///    hes then given a task to get in the car as a passenger
PROC CHECK_PLAYER_AT_CAR_GET_IN()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_ENTITY_ALIVE(viPlayerCar)
		
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viPlayerCar, 12)
		AND GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) <> PERFORMING_TASK
			IF SETUP_STAGE_REQUIREMENTS(RQ_CAMERA_CUTSCENE, <<-81.454941,296.807617,107.028061>>, -49.934269)
				IF DOES_CAM_EXIST(camMain)
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						//SET_CAM_PARAMS(CamMain,<<-81.454941,296.807617,107.028061>>,<<0.257009,0.000000,-49.934269>>,50.000000)  //setup camera at offset from car and make it point oiut the back of the car
					ENDIF
				ENDIF			
			ENDIF
		ENDIF
		
		IF bPlayerGettingInCar
		AND GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) <> PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) <> WAITING_TO_START_TASK
			TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viPlayerCar, DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVEBLENDRATIO_RUN) //ECF_USE_LEFT_ENTRY
		ENDIF

		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), viPlayerCar, <<10,10,3>>)
			IF GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) <> PERFORMING_TASK
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					THEFEED_HIDE_THIS_FRAME()
					IF iSeqMusic <> 3
						IF TRIGGER_MUSIC_EVENT("PAP2_CAR")	
							iSeqMusic = 3
						ENDIF
					ENDIF
					bPlayerGettingInCar = TRUE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-78.247871,299.375580,109.261192>>, <<-81.985039,289.756134,103.689636>>, 26.250000)
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
						ENDIF
						IF DOES_ENTITY_EXIST(objCam)
							IF IS_ENTITY_ATTACHED(objCam)
								DETACH_ENTITY(objCam)
							ENDIF
							DELETE_OBJECT(objCam)
						ENDIF
					ENDIF
					IF DOES_CAM_EXIST(camMain)
						STOP_CAM_POINTING(camMain)
								
						IF NOT DOES_CAM_EXIST(camInterp)
							camInterp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
						ELSE
							STOP_CAM_POINTING(camInterp)
						ENDIF
						
						camMainCS = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
						
						//SET_CAM_PARAMS(camMain,<< -81.7540, 296.0858, 106.8197 >>, << -4.5487, 0.0000, -31.5931 >>,49.9623)
						//SET_CAM_PARAMS(camInterp,<< -81.7540, 296.0858, 106.8197 >>, << -9.0737, 0.0000, -35.4270 >>,43.5051)
						SET_CAM_PARAMS(camMainCS,<< -74.7, 297.0, 106.0 >>, <<8.7, 0.0, 52.9>>,34.5)
						SET_CAM_PARAMS(camInterp,<< -76.2, 295.9, 106.1 >>, <<6.3, 0.0, 38.8>>,34.5)
						//SHAKE_CAM(camMainCS,"VIBRATE_SHAKE",2.5)
						//SHAKE_CAM(camInterp,"VIBRATE_SHAKE",2.5)
						//SHAKE_CAM(camMainCS,"HAND_SHAKE",1)
						//SHAKE_CAM(camInterp,"HAND_SHAKE",1)
						SET_CAM_ACTIVE_WITH_INTERP(camInterp,camMainCS,8000,GRAPH_TYPE_LINEAR)
						SET_CAM_ACTIVE(camMainCS,TRUE)
						SET_CAM_ACTIVE(camInterp,TRUE)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
						IF DOES_CAM_EXIST(camMainCS)
							SHAKE_CAM(camMainCS,"HAND_SHAKE",1.0)
						ENDIF
						IF DOES_CAM_EXIST(camInterp)
							SHAKE_CAM(camInterp,"HAND_SHAKE",1.0)
						ENDIF
						CLEAR_AREA_OF_VEHICLES(vPlayerCarStartPos, 200) //Clear the cars that might be around so when the uba starts you dont see all the cars get cleared.
						CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()),200,TRUE)
						iTimerCheckPlayerInRightCar = GET_GAME_TIMER()
						IF NOT IS_SAFE_PED_IN_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR] )
							SAFE_TELEPORT_ENTITY(mPoppy.piPed,<<-46.3393, 351.7322, 112.5621>>, 62.2187)
							IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
								TASK_ENTER_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR],-1)
							ENDIF
						ENDIF
						//<<-46.3393, 351.7322, 112.5621>>, 62.2187 teleport poppy
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), viPlayerCar, DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVEBLENDRATIO_RUN)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 7.5, REPLAY_IMPORTANCE_LOW)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF bPlayerGettingInCar = TRUE
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player and the buddy are in the car
/// RETURNS:
///    TRUE if they are in the car together
FUNC BOOL ARE_PLAYER_AND_BUDDY_IN_CAR()
	//IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar) 
	//AND IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar) 
	AND IS_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    If the player takes too long to get in the car
///    we play some dialogue and bev drives off
PROC CHECK_FAIL_GET_IN_CAR()
	IF NOT bExpireGetInCarFail
		//IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-50.615166,329.184814,111.507881>>, <<-65.104324,297.998444,115.673424>>, 11.350000)
		IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) <= 40	
		AND bPlayerGettingInCar = FALSE	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_GETCAR", "PAP2_GETCAR_2",CONV_PRIORITY_MEDIUM)
					bExpireGetInCarFail = TRUE
					IF IS_PED_UNINJURED(mBuddy.piPed)
						TASK_VEHICLE_DRIVE_WANDER(mBuddy.piPed, viPlayerCar, 150, DRIVINGMODE_PLOUGHTHROUGH)
						bBevDrivingOff = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				IF bPlayerGettingInCar = FALSE	
					bExpireGetInCarFail = TRUE
					IF IS_PED_UNINJURED(mBuddy.piPed)
						TASK_VEHICLE_DRIVE_WANDER(mBuddy.piPed, viPlayerCar, 150, DRIVINGMODE_PLOUGHTHROUGH)
						bBevDrivingOff = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the player getting in a different car to the escape car
PROC CHECK_PLAYER_DICKING_AROUND()
	IF NOT bWrongCarTimerActivate
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> viPlayerCar
					iTimerCheckPlayerInRightCar = GET_GAME_TIMER()
					bWrongCarTimerActivate = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF GET_GAME_TIMER() > iTimerCheckPlayerInRightCar + 10000	
			CHECK_FAIL_GET_IN_CAR()
		ENDIF
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = viPlayerCar
					bWrongCarTimerActivate = FALSE
					iTimerCheckPlayerInRightCar = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bExpireGetInCarFail	
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			MISSION_FAILED(FR_TOOK_TOO_LONG) 
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    This moniters the player not getting in the escape car when he should of
PROC CHECK_BUDDY_IN_CAR_BEFORE_PLAYER()
	IF NOT bIsBuddyInCarBeforePlayer
		IF IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
		AND NOT IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
			SAFE_REMOVE_BLIP(biBuddyBlip)
			IF NOT DOES_BLIP_EXIST(biBuddyBlip)
				biGOTO = CREATE_VEHICLE_BLIP(viPlayerCar,TRUE,BLIPPRIORITY_HIGH)
			ENDIF
			SET_BLIP_AS_FRIENDLY(biGOTO, TRUE)
			TASK_LOOK_AT_ENTITY(mBuddy.piPed, PLAYER_PED_ID(),-1)
			IF NOT bExpireGetInCarText
				PRINT_NOW("PAP2_00", DEFAULT_GOD_TEXT_TIME, 0) //Get in to Beverly's ~b~car.
				bExpireGetInCarText = TRUE
			ENDIF
			bIsBuddyInCarBeforePlayer = TRUE
			iTimerBevInCar = GET_GAME_TIMER()
		ENDIF
	ELSE
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_00")		
			IF GET_GAME_TIMER() > iTimerBevInCar + 12000
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
					AND bPlayerGettingInCar = FALSE	
					AND NOT IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)	
						CHECK_FAIL_GET_IN_CAR()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		/*
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_00")	
			IF GET_GAME_TIMER() > iTimerBevInCar + 12000
				CHECK_FAIL_GET_IN_CAR()
				IF IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
				AND NOT IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
					//KILL_FACE_TO_FACE_CONVERSATION()
					bIsBuddyInCarBeforePlayer = FALSE
					iTimerBevInCar = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
		*/
	ENDIF
ENDPROC

/// PURPOSE:
///    updates the camera for the mini cutscene and  plays the conversation
///    It also warps everyone in to thier cars after the camera changes position
/// RETURNS:
///    TRUE when the conversation has reached its third line
FUNC BOOL DO_MINI_CAR_CUTSCENE()
	IF IS_ENTITY_ALIVE(viPlayerCar)
	 	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
			
			SET_ENTITY_VISIBLE(viPlayerCar,FALSE)
			
			IF DOES_CAM_EXIST(camInterp)
				DESTROY_CAM(camInterp)
			ENDIF	
			/*
			IF DOES_CAM_EXIST(camMain)
				DESTROY_CAM(camMain)
			ENDIF	
			
			camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
			*/		
			SETUP_CAMERA(FALSE)

			bUseFace = FALSE
			
			IF DOES_CAM_EXIST(camMain)
				
				SET_CAM_COORD(CamMain,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar, vCameraOffset))
				//<< -81.4549, 296.808, 107.028 >>
				//<< -80.9626, 299.923, 106.761 >>
				
				//SET_CAM_COORD(CamMain,<< -80.9626, 299.923, 106.761 >>)
				
				POINT_CAM_AT_COORD(CamMain,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar,<<0,-5,0.6>>))
				//<< -0, 0, -49.9343 >>
				//<< 1.6265, -1.74941e-008, 75.5606 >>
				
				//SET_CAM_ROT(CamMain,<< 1.6265, -1.74941e-008, 75.5606 >>)
				
				SET_CAM_FOV(camMain,45)
				//SHAKE_CAM(camMain, "HAND_SHAKE", 0.1)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			IF IS_VEHICLE_OK(viChaseCars[POPPY_CAR])
				LOWER_CONVERTIBLE_ROOF(viChaseCars[POPPY_CAR], TRUE)
			ENDIF
			
			MANAGE_CAMERA()
			
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
			bStartChaseCam = TRUE
		
			RETURN TRUE
		
			IF bCamInterp
			ENDIF
			
		ENDIF
	ENDIF
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	MANAGE_CHASE_CONVO()
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Uber recording management
PROC MANAGE_UBER_RECORDING_FOR_CHASE()
      SWITCH iPlaybackProgress
 
          // **** INITIALISE UBER PLAYBACK ****
      CASE 0
      		INITIALISE_UBER_PLAYBACK("PAP2UBA", 1)
	
           	// paste array of data here
			UBER_TRAFFIC_DATA()
			POPPY_SETPIECE_CAR()
			
			//bTrafficForceDefaultPedModel = TRUE
			
	        iPlaybackProgress++		
	   BREAK
	   
          // **** START PLAYBACK OF MAIN CAR ****
	   CASE 1 
    	    /*
			IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])  //101
			AND IS_ENTITY_ALIVE(mPoppy.piPed)	
				IF NOT IS_PED_IN_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR])
					SET_PED_INTO_VEHICLE(mPoppy.piPed, viChaseCars[POPPY_CAR])
				ENDIF
				START_PLAYBACK_RECORDED_VEHICLE(viChaseCars[POPPY_CAR], 101,"PAP2UBA")
				SET_VEHICLE_ENGINE_ON(viChaseCars[POPPY_CAR],TRUE,TRUE)
			ENDIF
			IF IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])  //102
			AND IS_ENTITY_ALIVE(mShagger.piPed)	
				IF NOT IS_PED_IN_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
					SET_PED_INTO_VEHICLE(mShagger.piPed, viChaseCars[SHAGGER_CAR])
				ENDIF
				START_PLAYBACK_RECORDED_VEHICLE(viChaseCars[SHAGGER_CAR], 102,"PAP2UBA")
				SET_VEHICLE_ENGINE_ON(viChaseCars[SHAGGER_CAR],TRUE,TRUE)
			ENDIF
			IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])  //103
			AND IS_ENTITY_ALIVE(mBodyGuard.piPed)	
				IF NOT IS_PED_IN_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
					SET_PED_INTO_VEHICLE(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR])
				ENDIF
				START_PLAYBACK_RECORDED_VEHICLE(viChaseCars[BODYGUARD_CAR], 103,"PAP2UBA")
				SET_VEHICLE_ENGINE_ON(viChaseCars[BODYGUARD_CAR],TRUE,TRUE)
			ENDIF
			*/
			IF IS_VEHICLE_DRIVEABLE(viPlayerCar)	
	      		START_PLAYBACK_RECORDED_VEHICLE(viPlayerCar, 1,"PAP2UBA")
				SET_PLAYBACK_SPEED(viPlayerCar, fPlaybackSpeed)
       			DO_MINI_CAR_CUTSCENE()
				iPlaybackProgress++		
			ENDIF
	   BREAK
	   
           // **** UPDATE UBER PLAYBACK ****
	    CASE 2
			WARP_EVERYONE_TO_CARS_SKIP()
			IF eMissionStage = MS_POPPY_AND_UBA
			OR eMissionStage = MS_SET_PIECE_RECORDING
				UPDATE_UBER_PLAYBACK(viPlayerCar, fPlaybackSpeed)
			ELSE
				IF NOT bPlayerFailed
			        IF IS_VEHICLE_DRIVEABLE(viPlayerCar) AND IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR]) AND IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
				     	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
			                IF eMissionStage = MS_CAR_CHASE
								//MONITER_SOUNDS(CEIL( GET_TIME_POSITION_IN_RECORDING(viPlayerCar)))
								UPDATE_UBER_PLAYBACK(viPlayerCar, fPlaybackSpeed)	
								
							ELIF eMissionStage = MS_POPPY_AND_UBA
							OR eMissionStage = MS_SET_PIECE_RECORDING							
								UPDATE_UBER_PLAYBACK(viPlayerCar, fPlaybackSpeed)
							ENDIF
				     	ELSE
							// finish	
						 	eState = SS_CLEANUP
						 	CLEANUP_UBER_PLAYBACK(TRUE)
				    	 ENDIF
				 	ENDIF
				ELSE
			        IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR]) 
					AND IS_PED_UNINJURED(mPoppy.piPed)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viChaseCars[POPPY_CAR])
							//STOP_PLAYBACK_RECORDED_VEHICLE(viChaseCars[POPPY_CAR])
							//TASK_VEHICLE_DRIVE_WANDER(mPoppy.piPed, viChaseCars[POPPY_CAR], 150, DRIVINGMODE_AVOIDCARS_RECKLESS)
						ENDIF
					ENDIF
					
					IF IS_ENTITY_ALIVE(viChaseCars[SHAGGER_CAR])
					AND IS_PED_UNINJURED(mShagger.piPed)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viChaseCars[SHAGGER_CAR])
							//STOP_PLAYBACK_RECORDED_VEHICLE(viChaseCars[SHAGGER_CAR])
							//TASK_VEHICLE_DRIVE_WANDER(mShagger.piPed, viChaseCars[SHAGGER_CAR], 150, DRIVINGMODE_AVOIDCARS_RECKLESS)
						ENDIF
					ENDIF	
					
					IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])
					AND IS_PED_UNINJURED(mBodyGuard.piPed)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viChaseCars[BODYGUARD_CAR])
							//STOP_PLAYBACK_RECORDED_VEHICLE(viChaseCars[BODYGUARD_CAR])
							//TASK_VEHICLE_DRIVE_WANDER(mBodyGuard.piPed, viChaseCars[BODYGUARD_CAR], 150, DRIVINGMODE_AVOIDCARS_RECKLESS)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
	     BREAK

	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Moniters the player not keeping poppy in shot 
///    If the box colours are at a certain level then the we kill any chase line that is playing
///    then play a fail line aslong as the last fail line was over 8seconds ago
///    We then increment iNumFilmFails if the number of fails is greater than MAX_FILM_FAILS_CHASE
///    Then the player has failed so we play a line and set bPlayerFailed = true
PROC MONITER_FOR_BAD_FILMING_CHASE()
	IF mBoxCol.R > 200.0
	AND mBoxCol.G < 100.0
	AND mBoxCol.B < 100.0
	//IF iCounterNotOnScreen > iFramesBeforeFilmingFail/2
		TEXT_LABEL_23 test = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		IF ARE_STRINGS_EQUAL( test, "PAP2_CHASE2") 
			KILL_FACE_TO_FACE_CONVERSATION()
			bLineSet = FALSE
		ENDIF

		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF iNumFilmFails < MAX_FILM_FAILS_CHASE
				IF GET_GAME_TIMER() > iTimerBadCam + 8000	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BADCAM2", CONV_PRIORITY_MEDIUM)
						iNumFilmFails++
						bLineSet = TRUE
						iTimerBadCam = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELIF iNumFilmFails >= MAX_FILM_FAILS_CHASE 
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FAILCAM", CONV_PRIORITY_MEDIUM)
					IF iNumFilmFails >= MAX_FILM_FAILS_CHASE
						bPlayerFailed = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF		
ENDPROC

/// PURPOSE:
///    Maintains the Scripted camera that follows the players car 
///    at the end of the chase
PROC DO_SCRIPTED_CINE_CAM()
	IF bStartScriptCineCam
		//IF DOES_CAM_EXIST(camMain)
			IF IS_ENTITY_ALIVE(viPlayerCar)
				SET_USE_HI_DOF()
				SWITCH eScriptCamState
					CASE SCC_START
						//IF IS_ENTITY_IN_ANGLED_AREA(viPlayerCar,<<66.603821,-390.643799,33.920174>>, <<45.938286,-450.393372,43.919422>>, 15.750000, FALSE, FALSE )
							bUseFace = FALSE
							bPlayerFailed = FALSE
							bDisableFail = TRUE
							//REQUEST_CUTSCENE("pap_2_mcs_1")
							bDoingEndCut = TRUE
							IF IS_PED_UNINJURED(mBuddy.piPed)
								TASK_CLEAR_LOOK_AT(mBuddy.piPed)
							ENDIF
							iTimerCamSwitch = GET_GAME_TIMER()
							eScriptCamState = SCC_CONSTRUCTION_FOOR_CAM
						//ENDIF
					BREAK
				
					CASE SCC_CONSTRUCTION_FOOR_CAM
						
						IF bDialogueEndCut = FALSE	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ENDCUT", CONV_PRIORITY_VERY_HIGH)   //I think I'm gonna throw up. This better be worth it.
									bDialogueEndCut = TRUE
								ENDIF
							ENDIF
						ENDIF
							
						IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
							IF IS_VEHICLE_SEAT_FREE(viPlayerCar, VS_BACK_RIGHT)
								TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viPlayerCar)
							ENDIF
						ENDIF	
							
						IF GET_GAME_TIMER() > iTimerCamSwitch + 2000
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
							OR GET_TIME_POSITION_IN_RECORDING(viPlayerCar) > 101275.632813 //99874.835938 //100622.875000 //101636.492188
								
								CLEAR_TIMECYCLE_MODIFIER()
								
								IF DOES_CAM_EXIST(camMain)
									DESTROY_CAM(camMain)
								ENDIF
								
								IF NOT DOES_CAM_EXIST(camMain)
									camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
								ENDIF
								
								//SET_CAM_PARAMS(camMain,<< -74.2735, -532.8775, 39.9423 >>, << 2.1752, 0.0075, -32.3615 >>,21.0179)
								SET_CAM_PARAMS(camMain,<< -66.1, -515.3, 42.3 >>, << 1.3, 0.0, -27.0 >>,22.5)
								SET_CAM_NEAR_DOF(camMain,0)
								SET_CAM_FAR_DOF(camMain,200)
								SET_CAM_DOF_STRENGTH(camMain,0.5)
								
								STOP_SOUND(iZoomSound)
									
								IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_CHASE")
									STOP_AUDIO_SCENE("PAPARAZZO_02_CHASE")
								ENDIF	

								IF IS_VEHICLE_OK(viPlayerCar)
									SET_VEHICLE_FIXED(viPlayerCar)
								ENDIF
								
								IF DOES_CAM_EXIST(camInterp)
									DESTROY_CAM(camInterp)
								ENDIF
								
								IF NOT DOES_CAM_EXIST(camInterp)
									camInterp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
								ENDIF
								
								//SET_CAM_PARAMS(camInterp,<< -74.2635, -532.8675, 39.9413 >>, << 2.1762, 0.0074, -32.3605 >>,21.0279)
								SET_CAM_PARAMS(camInterp,<< -71.4, -529.0, 39.8 >>, << 0.8, 0.0, -25.9 >>,22.5)
								SET_CAM_NEAR_DOF(camInterp,0)
								SET_CAM_FAR_DOF(camInterp,200)
								SET_CAM_DOF_STRENGTH(camInterp,0.5)
								
								//SHAKE_CAM(camMain,"VIBRATE_SHAKE",1.0)
								//SHAKE_CAM(camInterp,"VIBRATE_SHAKE",1.0)
						
								/*
								SET_CAM_NEAR_DOF(camMain,0)
								SET_CAM_FAR_DOF(camMain,100)
								SET_CAM_DOF_STRENGTH(camMain,0.5)
								SET_CAM_NEAR_DOF(camInterp,0)
								SET_CAM_FAR_DOF(camInterp,100)
								SET_CAM_DOF_STRENGTH(camInterp,0.5)
								SET_USE_HI_DOF()
								OVERRIDE_LODSCALE_THIS_FRAME(0.8)		
								*/
								
								SET_CAM_ACTIVE(camMain,TRUE)
								SET_CAM_ACTIVE(camInterp,TRUE)
								
								SET_CAM_ACTIVE_WITH_INTERP(camInterp,camMain,7000,GRAPH_TYPE_DECEL)
								RENDER_SCRIPT_CAMS(TRUE,FALSE)
								
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
								
								IF DOES_CAM_EXIST(camMain)
									SHAKE_CAM(camMain,"HAND_SHAKE",0.6)
								ENDIF
								IF DOES_CAM_EXIST(camInterp)
									SHAKE_CAM(camInterp,"HAND_SHAKE",0.6)
								ENDIF
								eScriptCamState = SCC_PAVEMENT_CAM
							
							ELSE
								IF DOES_CAM_EXIST(camMain)
									STOP_CAM_POINTING(camMain)
								ENDIF
								IF DOES_CAM_EXIST(camInterp)
									DESTROY_CAM(camInterp)
								ENDIF
								IF DOES_CAM_EXIST(camMain)
									fCamFov = GET_CAM_FOV(camMain)
								ENDIF
								MANAGE_CAMERA()
							ENDIF
						ELSE
							IF DOES_CAM_EXIST(camMain)
								STOP_CAM_POINTING(camMain)
							ENDIF
							IF DOES_CAM_EXIST(camInterp)
								DESTROY_CAM(camInterp)
							ENDIF
							IF DOES_CAM_EXIST(camMain)
								fCamFov = GET_CAM_FOV(camMain)
							ENDIF
							MANAGE_CAMERA()
						ENDIF
					BREAK
					
					CASE SCC_PAVEMENT_CAM
						/*
						SET_CAM_NEAR_DOF(camMain,0)
						SET_CAM_FAR_DOF(camMain,100)
						SET_CAM_DOF_STRENGTH(camMain,0.5)
						SET_CAM_NEAR_DOF(camInterp,0)
						SET_CAM_FAR_DOF(camInterp,100)
						SET_CAM_DOF_STRENGTH(camInterp,0.5)
						SET_USE_HI_DOF()
						OVERRIDE_LODSCALE_THIS_FRAME(0.8)		
						*/
						//PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(viPlayerCar))
						//PRINTNL()
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
						OR GET_TIME_POSITION_IN_RECORDING(viPlayerCar) > 108426.351563
							
							WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viPlayerCar,3)
							AND iFailsafe < 500	
								WAIT(0)
								++iFailsafe
								HIDE_HUD_AND_RADAR_THIS_FRAME()
								THEFEED_HIDE_THIS_FRAME()
							ENDWHILE
								
							IF IS_VEHICLE_STOPPED(viPlayerCar)
							AND HAS_CUTSCENE_LOADED()						
								
								eMissionStage = MS_OUTRO
							
								EXIT
							ENDIF
						
						ENDIF
					BREAK
					
					CASE SCC_CONSTRUCTION_CAM
						IF IS_ENTITY_IN_ANGLED_AREA(viPlayerCar,<< -25.8393, -481.6992, 39.4160 >>, << -58.2834, -470.9109, 39.4160 >>, 5, FALSE, FALSE )
							eScriptCamState = SCC_HOTEL_CAM
						ENDIF
					BREAK
					
					CASE SCC_HOTEL_CAM
						IF IS_ENTITY_IN_ANGLED_AREA(viPlayerCar,<< -62.2864, -491.9415, 39.4151 >>, << -30.9310, -504.4111, 39.4160 >>, 5, FALSE, FALSE )
							eScriptCamState = SCC_CAM_PAN
						ENDIF
					BREAK
					
					CASE SCC_CAM_PAN
						
					BREAK
				ENDSWITCH
			ENDIF
		//ELSE
			//camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)	
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Plays random anims on poppy in her car, this is just first pass. Should make these not random and trigger dialogue with them at appropriate times
PROC MANAGE_POPPY_IN_CAR_ANIMS()

	INT iRand

	IF HAS_ANIM_DICT_LOADED("rcmpaparazzo_2ig_3")	
		IF IS_PED_UNINJURED(mPoppy.piPed)	
			IF IS_PED_IN_ANY_VEHICLE(mPoppy.piPed)	
				IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_DRIVE_BY) <> PERFORMING_TASK	
					IF NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_fuckinlosers")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_gimmethtcam")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_ih8u")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_illsueyou")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_imavirgin")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_itwasyoga")
					AND NOT IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"rcmpaparazzo_2ig_3","pm_incar_notdoinanythin")
						iRand = GET_RANDOM_INT_IN_RANGE(0,800)
						IF iRand = 1
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_fuckinlosers", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF
						IF iRand = 2
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_gimmethtcam", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF	
						IF iRand = 3	
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_ih8u", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF	
						IF iRand = 4	
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_illsueyou", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF	
						IF iRand = 5	
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_imavirgin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF	
						IF iRand = 6	
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_itwasyoga", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF	
						IF iRand = 7	
							TASK_PLAY_ANIM(mPoppy.piPed, "rcmpaparazzo_2ig_3", "pm_incar_notdoinanythin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,AF_SECONDARY | AF_UPPERBODY)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
					
ENDPROC

/// PURPOSE:
///    Controls the maids reactions to the player, if the player is doing something to draw attention to
///    themselves ie holding a gun, thretening behaviour, bumping the maid
PROC MAID_PRECEPTION()
//	IF eMissionStage <> MS_FAILED 	
	IF eStaffState[0] <= S_AMBIENT_BEHAVIOUR
	AND eStaffState[1] <= S_AMBIENT_BEHAVIOUR
		IF HAS_PLAYER_TOUCHED_PED(mHotelStaff[0].piPed)
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				eStaffState[0] = S_SEEN_GUN
			ELSE
				eStaffState[0] = S_BUMPED
			ENDIF	
			TASK_LOOK_AT_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID(),-1)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ELSE
			IF IS_PED_FACING_PED(mHotelStaff[0].piPed,PLAYER_PED_ID(),90)
			AND IS_ENTITY_IN_RANGE_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID(),15)
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					eStaffState[0] = S_SEEN_GUN
				ELSE
					eStaffState[0] = S_BUMPED
				ENDIF	
				TASK_LOOK_AT_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID(),-1)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
		ENDIF
		
		/*
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			IF IS_ENTITY_IN_ARC_2D(mHotelStaff[0].piPed, PLAYER_PED_ID(), 45)
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				eStaffState[0] = S_SEEN_GUN
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()		
			ENDIF
		ENDIF
		*/
		IF HAS_PLAYER_THREATENED_PED(mHotelStaff[0].piPed)
		
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				eStaffState[0] = S_THREATENED_WITH_GUN
			ELSE
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				GIVE_ATTACK_ORDER(mHotelStaff[1].piPed)
				eStaffState[0] = S_THREATENED
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    When the maid is stood talking to the bus boy this function makes her play different
///    anims based on which line in the convo we are up to
/// PARAMS:
///    _root - we pas in the currently playing conversation root and its checked that it matches the staff root
PROC MANAGE_MAID_ANIMS_CONV(TEXT_LABEL_23 _root)
	IF ARE_STRINGS_EQUAL(_root, "PAP2_STAFF")
		INT i = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
		IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
			SWITCH i
				CASE 5 //Hi Hector.
					
				BREAK
				
//				CASE 7 //No, was it bad?
//					
//				BREAK
				
				CASE 9 //Porridge?
					
				BREAK
				
				CASE 11 //Well at least someone is getting some.
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    used in the maids state machine to tell her to walk to a position using the 
///    navmesh. Then sets her state to M_AMB_WALKING_TO_POINT which moniters for her arrivel at the point
/// PARAMS:
///    vPos - the position to walk to 
PROC GIVE_MAID_GOTO(VECTOR vPos)
	TASK_FOLLOW_NAV_MESH_TO_COORD(mHotelStaff[0].piPed, vPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
	eMaidAMBState = M_AMB_WALKING_TO_POINT
ENDPROC

/// PURPOSE:
///    State Machine for the maid
PROC MAID_STATES() //the maid
	MAID_PRECEPTION()
	//MANAGE_WINDOW_RAG()
	
	TEXT_LABEL_23 sRoot 
	TEXT_LABEL_23 sLabel 
	sRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	sLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()

	SWITCH eStaffState[0]
		CASE S_NULL
			//HOLDING AREA
		BREAK
		/// Stripted movment so the maid walk out infront of the player
		CASE S_WAIT_ACTIVE
			IF iBevRoute =  ROUTE_ARCH_1
			OR IS_PLAYER_AT_COORDS(vBevRoute[ROUTE_STAIRS_1_TOP], <<5,2,2.5>>)
				//ADD_FOLLOW_NAVMESH_TO_PHONE_TASK(mHotelStaff[0].piPed, << 15.7815, 333.5496, 109.9190 >>)//MAID
				//eStaffState[0] = S_WALK_TALK		
			ENDIF
		BREAK
		
		CASE S_WALK_TALK
			MONITER_MAID_PHONE()
			IF NOT IS_STRING_NULL_OR_EMPTY(sRoot)
				MANAGE_MAID_ANIMS_CONV(sRoot)
			ENDIF

			IF IS_ENTITY_AT_COORD(mHotelStaff[0].piPed, << 15.7815, 333.5496, 109.9190 >>, <<4,4,2>>)
				TASK_LOOK_AT_ENTITY(mHotelStaff[0].piPed, mHotelStaff[1].piPed, -1)
				eStaffState[0] = S_STAND_TALK
			ENDIF
		BREAK
		
		CASE S_STAND_TALK
			IF NOT IS_STRING_NULL_OR_EMPTY(sRoot)
			AND NOT IS_STRING_NULL_OR_EMPTY(sLabel)
				MANAGE_MAID_ANIMS_CONV(sRoot)
				IF ARE_STRINGS_EQUAL(sLabel, "PAP2_STAFF_11")
				OR NOT ARE_STRINGS_EQUAL(sRoot, "PAP2_STAFF")
					IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						TASK_CLEAR_LOOK_AT(mHotelStaff[0].piPed)
						SEQUENCE_INDEX Seq
						OPEN_SEQUENCE_TASK(Seq)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 12.7280, 346.7623, 114.3918 >>,1.0,DEFAULT_TIME_NEVER_WARP)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -25.5226, 367.3178, 113.7493 >>,1.0,DEFAULT_TIME_NEVER_WARP)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 18.6864, 334.4601, 109.9186 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 22.6585, 333.7577, 109.9186 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 24.7426, 334.8422, 110.4900 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 4.3274, 356.6412, 112.5507 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< -1.3367, 360.2300, 113.7494 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 31.3388, 335.7056, 114.3914 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 11.6968, 345.9524, 114.3914 >>,1.0,DEFAULT_TIME_NEVER_WARP,1.0,ENAV_NO_STOPPING)
						CLOSE_SEQUENCE_TASK(Seq)
						TASK_PERFORM_SEQUENCE(mHotelStaff[0].piPed,seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		///S_AMBIENT_BEHAVIOUR has its own state machine so that the maid can be kicked in to this state
		///      and then it can be left to run only 3 behaviours atm goto smoke and wash windows
		CASE S_AMBIENT_BEHAVIOUR
			SWITCH eMaidAMBState
				CASE M_AMB_WALKING_TO_POINT
					IF IS_ENTITY_AT_COORD(mHotelStaff[0].piPed, mMaidWorkPoint[iMaidActiveWorkPoint].vGOTO,<<1,1,1.5>>)
						SWITCH 	mMaidWorkPoint[iMaidActiveWorkPoint].eStateToEnter
							CASE M_AMB_WASHING_WINDOW

							BREAK
							
							CASE M_AMB_SMOKEING

							BREAK
						ENDSWITCH
					ELIF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
						//GIVE_MAID_GOTO(mMaidWorkPoint[iMaidActiveWorkPoint].vGOTO)
					ENDIF
				BREAK
				
				CASE M_AMB_SMOKEING
					IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
						iMaidActiveWorkPoint = GET_RANDOM_INT_IN_RANGE(0, MAX_MAID_WORK_POINTS)
						//GIVE_MAID_GOTO(mMaidWorkPoint[iMaidActiveWorkPoint].vGOTO)
					ENDIF
				BREAK
				
				CASE M_AMB_WASHING_WINDOW
					IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
						iMaidActiveWorkPoint = GET_RANDOM_INT_IN_RANGE(0, MAX_MAID_WORK_POINTS)
						//GIVE_MAID_GOTO(mMaidWorkPoint[iMaidActiveWorkPoint].vGOTO)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		/// Maid reaction states
		CASE S_BUMPED
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_MDFLEEB", CONV_PRIORITY_VERY_HIGH)
					eStaffState[0] = S_AMBIENT_BEHAVIOUR
					eStaffState[1] = S_AMBIENT_BEHAVIOUR
					bSpotted = TRUE
				ENDIF
			ELSE
				eStaffState[0] = S_AMBIENT_BEHAVIOUR
				eStaffState[1] = S_AMBIENT_BEHAVIOUR
				bSpotted = TRUE
			ENDIF
		BREAK
		
		CASE S_SEEN_GUN
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_MDFLEE1", CONV_PRIORITY_VERY_HIGH)
					GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
				ENDIF
			ELSE
				GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN
			ENDIF
		BREAK
		
		CASE S_THREATENED_WITH_GUN
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_MDFLEE2", CONV_PRIORITY_VERY_HIGH)
					GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
				ENDIF
			ELSE
				GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN	
			ENDIF
		BREAK
		
		CASE S_THREATENED
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[0].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_MDFLEE3", CONV_PRIORITY_VERY_HIGH)
					GIVE_ATTACK_ORDER(mHotelStaff[1].piPed)
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
				ENDIF
			ELSE
				GIVE_ATTACK_ORDER(mHotelStaff[1].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN

			ENDIF
		BREAK
		
		CASE S_RUN
			bSpotted = TRUE
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    When the bus boy is stood talking to the maid this function makes her play different
///    anims based on which line in the convo we are up to
/// PARAMS:
///    _root - we pas in the currently playing conversation root and its checked that it matches the staff root
PROC MANAGE_BUSBOY_ANIMS_CONV(TEXT_LABEL_23 _root)
	IF ARE_STRINGS_EQUAL(_root, "PAP2_STAFF")
		INT i = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
		IF GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK
			SWITCH i
//				CASE 4 //Hey there Maria.
//					
//				BREAK
			
				CASE 6 //Did you see the state of bungalow 3, before Mandy had to clean it?
					
				BREAK
				
				CASE 8 //Bad? It was horrific! Porridge everywhere!
					
				BREAK
				
//				CASE 10 //Yer loads of the stuff, Mandy seemed to think they had been using it for sex play!
//					
//				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the bus boy reactions to the player, if the player is doing something to draw attention to
///    themselves ie holding a gun, thretening behaviour, bumping the bus boy
PROC BUSBOY_PERCEPTION()
//	IF eMissionStage <> MS_FAILED 	
	IF eStaffState[0] <= S_AMBIENT_BEHAVIOUR
	AND eStaffState[1] <= S_AMBIENT_BEHAVIOUR
		IF HAS_PLAYER_TOUCHED_PED(mHotelStaff[1].piPed)
		OR IS_ENTITY_IN_RANGE_ENTITY(mHotelStaff[1].piPed,PLAYER_PED_ID(),12.0)	
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<14.255188,335.391235,109.130768>>, <<24.290331,330.667023,113.985596>>, 3.500000)	
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				eStaffState[1] = S_SEEN_GUN
			ELSE
				eStaffState[1] = S_BUMPED
			ENDIF	
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		/*
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			IF IS_ENTITY_IN_ARC_2D(mHotelStaff[1].piPed, PLAYER_PED_ID(), 45)
				GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
				eStaffState[1] = S_SEEN_GUN
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()		
			ENDIF
		ENDIF
		*/
		IF HAS_PLAYER_THREATENED_PED(mHotelStaff[1].piPed)
		
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
				eStaffState[1] = S_THREATENED_WITH_GUN
			ELSE
				GIVE_ATTACK_ORDER(mHotelStaff[1].piPed)
				eStaffState[1] = S_THREATENED
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    used in the bus boy state machine to tell him to walk to a position using the 
///    navmesh. Then sets his state to B_AMB_WALKING_TO_POINT which moniters for his arrival at the point
/// PARAMS:
///    vPos - the position to walk to 
PROC GIVE_BUSBOY_GOTO(VECTOR vPos)
	TASK_FOLLOW_NAV_MESH_TO_COORD(mHotelStaff[1].piPed, vPos, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
	eBoyAMBState = B_AMB_WALKING_TO_POINT
ENDPROC


/// PURPOSE:
///    State Machine for the bus boy
PROC BUSBOY_STATES()
	BUSBOY_PERCEPTION()
	TEXT_LABEL_23 sRoot 
	sRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	SWITCH eStaffState[1]
		CASE S_NULL
			//HOLDING AREA
		BREAK
		/// Stripted movment so the bus boy and maid meet up
		CASE S_WAIT_ACTIVE
			//SK_PRINT("WAIT BUSBOY")
			//TASK_START_SCENARIO_AT_POSITION(mHotelStaff[1].piPed,"WORLD_HUMAN_SEAT_STEPS",<< 16.9840, 336.9486, 110.3190 >>, 157.6965)
			//SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(mHotelStaff[1].piPed,FALSE)
			//eStaffState[1] = S_WALK_TALK
		BREAK
		
		CASE S_WALK_TALK
			IF NOT IS_STRING_NULL_OR_EMPTY(sRoot)
				MANAGE_BUSBOY_ANIMS_CONV(sRoot)
			ENDIF
			
			IF IS_ENTITY_AT_COORD(mHotelStaff[1].piPed, << 15.7815, 333.5496, 109.9190 >>, <<4,4,2>>)
				//eStaffState[1] = S_STAND_TALK
			ENDIF
		BREAK
		
		CASE S_STAND_TALK
			IF NOT IS_STRING_NULL_OR_EMPTY(sRoot)
				MANAGE_BUSBOY_ANIMS_CONV(sRoot)
				IF NOT ARE_STRINGS_EQUAL(sRoot, "PAP2_")
					IF  SETUP_STAGE_REQUIREMENTS(RQ_BROOM, vSafeVec)
						CLEAR_PED_TASKS(mHotelStaff[1].piPed)
						GIVE_BUSBOY_GOTO(vBoyWorkPoint[iBoyActiveWorkPoint])
						
						eStaffState[1] = S_AMBIENT_BEHAVIOUR
					ENDIF
				ENDIF
			ENDIF
		BREAK
		///S_AMBIENT_BEHAVIOUR has its own state machine so that the bus boy can be kicked in to this state
		///      and then it can be left to run only 2 behaviours atm goto and sweep
		CASE S_AMBIENT_BEHAVIOUR
			SWITCH eBoyAMBState
				CASE B_AMB_WALKING_TO_POINT
					IF IS_ENTITY_AT_COORD(mHotelStaff[1].piPed, vBoyWorkPoint[iBoyActiveWorkPoint],<<1,1,1.5>>)
						//TELL_BOY_SWEEP()
					ELIF GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
						GIVE_BUSBOY_GOTO(vBoyWorkPoint[iBoyActiveWorkPoint])
					ENDIF
				BREAK
				
				CASE B_AMB_SWEEPING
					IF GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
						iBoyActiveWorkPoint = GET_RANDOM_INT_IN_RANGE(0, MAX_BOY_WORK_POINTS)
						GIVE_BUSBOY_GOTO(vBoyWorkPoint[iBoyActiveWorkPoint])
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		/// bus boy reaction states
		CASE S_BUMPED
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[1].piPed, PLAYER_PED_ID()) < 20//12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BSFLEEB", CONV_PRIORITY_VERY_HIGH)
					eStaffState[0] = S_AMBIENT_BEHAVIOUR
					eStaffState[1] = S_AMBIENT_BEHAVIOUR
					bSpotted = TRUE
				ENDIF
			ELSE
				eStaffState[0] = S_AMBIENT_BEHAVIOUR
				eStaffState[1] = S_AMBIENT_BEHAVIOUR
				bSpotted = TRUE
			ENDIF
		BREAK
		
		CASE S_SEEN_GUN
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[1].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BSFLEE1", CONV_PRIORITY_VERY_HIGH)
					GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
					bStaffThreatened = TRUE
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
				ENDIF
			ELSE
				bStaffThreatened = TRUE
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN

			ENDIF
		BREAK
		
		CASE S_THREATENED_WITH_GUN
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[1].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BSFLEE2", CONV_PRIORITY_VERY_HIGH)
					bStaffThreatened = TRUE
					GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
					
				ENDIF
			ELSE
				bStaffThreatened = TRUE
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN
			ENDIF
		BREAK
		
		CASE S_THREATENED
			IF GET_DISTANCE_BETWEEN_ENTITIES(mHotelStaff[1].piPed, PLAYER_PED_ID()) < 12
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_BSFLEE3", CONV_PRIORITY_VERY_HIGH)
					bStaffThreatened = TRUE
					GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
					eStaffState[0] = S_RUN
					eStaffState[1] = S_RUN
				ENDIF
			ELSE
				bStaffThreatened = TRUE
				GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
				eStaffState[0] = S_RUN
				eStaffState[1] = S_RUN
			ENDIF
		BREAK		

		CASE S_RUN
			bSpotted = TRUE
		BREAK			
	ENDSWITCH
	
ENDPROC

//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

FUNC BOOL IS_ANIM_PHASE_OK_FOR_CUTSCENE()

	//PRINTFLOAT(GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmpaparazzo_2", "pap_2_rcm_base"))
	//PRINTNL()
	//OR GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) > 0.819200
	//OR GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) < 0.305006 //0.02
	//IF NOT IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0],"rcmpaparazzo_2", "pap_2_rcm_base")
	//OR GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmpaparazzo_2", "pap_2_rcm_base") > 0.819200
	//OR GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0],"rcmpaparazzo_2", "pap_2_rcm_base") < 0.305006 //0.02
	//FLOAT fStartTagOut
	//FLOAT fEndTagOut
	
	IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[0])
		RETURN TRUE
	ELSE
		IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])		
			//IF FIND_ANIM_EVENT_PHASE("rcmpaparazzo_2", "pap_2_rcm_base","START_CUTSCENE",fStartTagOut,fEndTagOut)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSynchScene) 
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) >= 0.21//fStartTagOut
					AND GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) <= 0.91//fEndTagOut
					//IF GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) <= 0.21//fStartTagOut
					//OR GET_SYNCHRONIZED_SCENE_PHASE(iSynchScene) >= 0.91//fEndTagOut
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			//ELSE
			//	RETURN TRUE
			//ENDIF
		ENDIF
	ENDIF
	RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()		
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-69.110252,302.947449,105.334381>>, <<-77.485947,306.753998,110.556976>>, 7.000000)	
	OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],3)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    State function that runs the mocap scene
PROC INTRO()

	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	DO_FOCUS_PUSH()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	
	IF NOT IS_ANIM_PHASE_OK_FOR_CUTSCENE()
		EXIT
	ENDIF
	
	MODEL_NAMES mnVehPlayer
	VECTOR vMovePosVehPlayer
	FLOAT fMoveHeadingVehPlayer
	/*
	//Cant skip the cutscene until the stage is active and the loading has finished
	IF eState = SS_ACTIVE
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() AND bLoadingFinIntro
			eState = SS_SKIPPED
		ENDIF
	ENDIF
	*/
	//Load everything for the next stage while the cutscene is playing 
	IF NOT bLoadingFinIntro
		IF SETUP_MISSION_STAGE(MS_INTRO, bJumpSkip)
			bLoadingFinIntro = TRUE //loading done
		ENDIF
	ENDIF

	IF WAS_CUTSCENE_SKIPPED()
	OR IS_SCREEN_FADED_OUT()	
	OR IS_SCREEN_FADING_OUT()
		bCSSkipped = TRUE
	ENDIF

	SWITCH eState
		CASE SS_INIT
			IF CAN_PLAYER_START_CUTSCENE(TRUE)
				IF RC_IS_CUTSCENE_OK_TO_START()		
					IF NOT bDoingFocusPush
					OR GET_GAME_TIMER() > iTimerFocusPush + 3000
						IF bDebugSkipping = FALSE	
							mBuddy.piPed = sRCLauncherDataLocal.pedID[0]
							sRCLauncherDataLocal.pedID[0] = NULL
							IF IS_PED_UNINJURED(mBuddy.piPed)	
								SET_PED_LEG_IK_MODE(mBuddy.piPed, LEG_IK_FULL)
							ENDIF
						ENDIF
						bCSSetExitCam = FALSE
						bCSSetExitBev = FALSE
						bCSSetExitFranklin = FALSE
						IF DOES_ENTITY_EXIST(mBuddy.piPed)
							IF NOT IS_ENTITY_DEAD(mBuddy.piPed)
								REGISTER_ENTITY_FOR_CUTSCENE(mBuddy.piPed, "Beverley", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
						ENDIF
						/*
						IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
							IF NOT IS_ENTITY_DEAD(sRCLauncherDataLocal.objID[0])
								DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
							ENDIF
						ENDIF
						*/
						IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])	
							sRCLauncherDataLocal.objID[0] = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,20.0>>))	
						ENDIF
						IF IS_ENTITY_ATTACHED(sRCLauncherDataLocal.objID[0])
							DETACH_ENTITY(sRCLauncherDataLocal.objID[0])
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.objID[0], "Beverlys_camera", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PROP_PAP_CAMERA_01)
						
						// Cleanup launcher to remove lead-in blip
						RC_CLEANUP_LAUNCHER()
						
						// Showtime!
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						/*
						IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
							IF NOT IS_ENTITY_DEAD(sRCLauncherDataLocal.objID[0])
								DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
							ENDIF
						ENDIF
						*/
						WAIT(0)
						/*
						IF DOES_ENTITY_EXIST(objCam)
							IF NOT IS_ENTITY_DEAD(objCam)
								DELETE_OBJECT(objCam)
							ENDIF
						ENDIF
						*/
						//SETUP_STAGE_REQUIREMENTS(RQ_BODYGUARD_CARS, vSafeVec)
						//SETUP_STAGE_REQUIREMENTS(RQ_PLAYER_CAR, vPlayerCarStartPos, fPlayerCarDir)
						//viPassCar = GET_PLAYERS_LAST_VEHICLE()
						//IF NOT IS_ENTITY_A_MISSION_ENTITY(viPassCar)
						//	SET_ENTITY_AS_MISSION_ENTITY(viPassCar, TRUE)
						//ENDIF
						CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
						STOP_GAMEPLAY_HINT()
						IF IS_ENTITY_ALIVE(viPlayerCar)
							SAFE_TELEPORT_ENTITY(viPlayerCar,<<-78.101295,298.967560,106.047028>>, -109.118706)
							SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerCar)
						ENDIF
						mnVehPlayer = GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())
						IF IS_THIS_MODEL_A_HELI(mnVehPlayer)
						OR IS_THIS_MODEL_A_PLANE(mnVehPlayer)
							vMovePosVehPlayer = <<-201.52, 307.55, 96.84>>    //Move to car park
							fMoveHeadingVehPlayer = 261.18
						ELIF (mnVehPlayer = BLIMP OR mnVehPlayer = BLIMP2)
							vMovePosVehPlayer = <<-211.02, 474.41, 133.36>>
							fMoveHeadingVehPlayer = 281.42
						ELSE
							vMovePosVehPlayer = <<-88.6147, 303.8292, 105.9208>>
							fMoveHeadingVehPlayer = 245.3452
						ENDIF
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-10.662442,283.070343,110.237289>>, <<-76.664360,301.181580,104.585754>>, 6.000000, vMovePosVehPlayer, fMoveHeadingVehPlayer)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-86.616211,297.277191,102.943222>>, <<-55.158104,286.539825,109.669701>>, 16.750000,vMovePosVehPlayer, fMoveHeadingVehPlayer)
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-55.969414,318.223999,109.394379>>, <<14.169346,300.160767,117.998123>>, 41.750000, vMovePosVehPlayer, fMoveHeadingVehPlayer)   //To fix B*1965914
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,0,TRUE,CHAR_FRANKLIN)
						RC_START_CUTSCENE_MODE(m_vCharPos,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE)
						CLEAR_AREA_OF_PEDS(<< -28.7403, 303.3516, 111.6961 >>, 40.0)// Area around poppy
						CLEAR_AREA_OF_PEDS(<< -17.3155, 340.0973, 110.4743 >>, 50.0)// Area around pool
						CLEAR_AREA_OF_PEDS(<< 12.2397, 319.1199, 109.8446 >>, 50.0)// Area around Hotel staff
						CLEAR_AREA_OF_VEHICLES(<< -12.2142, 280.2522, 107.3232 >>, 20)
						CLEAR_AREA_OF_VEHICLES(<< -48.1975, 352.4658, 112.0536 >>, 5)
						CLEAR_ANGLED_AREA_OF_VEHICLES(<<-55.969414,318.223999,109.394379>>, <<14.169346,300.160767,117.998123>>, 41.750000)  //To fix B*1965914
						IF IS_SCREEN_FADED_OUT()
						    IF NOT IS_SCREEN_FADING_IN()
						    	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
							ENDIF
						ENDIF
						eState = SS_ACTIVE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
				
			//IF bCSSetExitBev = FALSE	
				IF NOT DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
						sRCLauncherDataLocal.objID[0] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
						SK_PRINT("BEVERLYS CAMERA - GOT HANDLE FROM CS")	
					ENDIF
				ENDIF	
				IF NOT DOES_ENTITY_EXIST(mBuddy.piPed)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverley"))
						mBuddy.piPed = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverley"))
					ENDIF
				ELSE
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")
						IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[0])
							//SET_ENTITY_VISIBLE_IN_CUTSCENE(sRCLauncherDataLocal.objID[0],FALSE)
							DETACH_ENTITY(sRCLauncherDataLocal.objID[0])
							DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
						ELSE
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
								sRCLauncherDataLocal.objID[0] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
								//SET_ENTITY_VISIBLE_IN_CUTSCENE(sRCLauncherDataLocal.objID[0],FALSE)
								DETACH_ENTITY(sRCLauncherDataLocal.objID[0])
								DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
							ENDIF
						ENDIF
						IF NOT DOES_ENTITY_EXIST(objCam)	
							objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,20.0>>))	
						ENDIF
						IF IS_ENTITY_ALIVE(mBuddy.piPed)

								IF IS_ENTITY_ALIVE(objCam)

									//ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>

									ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
								
								ENDIF
								//IF HAS_CLIP_SET_LOADED("move_m@casual@f")
									//SET_PED_MOVEMENT_CLIPSET(mBuddy.piPed,"move_m@casual@f")
								//ENDIF
								IF HAS_CLIP_SET_LOADED(sWeaponMoveClipset)	
									SET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed,sWeaponMoveClipset)
								ENDIF
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mBuddy.piPed)
								TASK_LOOK_AT_ENTITY(mBuddy.piPed,PLAYER_PED_ID(),-1)
								TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-60.9116, 295.3501, 105.0975>>,1)
								bCSSetExitBev = TRUE

							//IF IS_SCREEN_FADED_OUT()
							
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
			/*
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverlys_camera")
				SK_PRINT("BEVERLYS CAMERA - CAN SET EXIT STATE")
				ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
			ENDIF
			*/
			//IF GET_CUTSCENE_TIME()	< 5000
			/*	
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
					objCam = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
					SET_ENTITY_COLLISION(objCam,FALSE)
				ENDIF
			*/
			//ENDIF
			
			IF bCSSetExitCam = FALSE	
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()	
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(),247.7034)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(16.1556)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.6252)
					bCSSetExitCam = TRUE
				ENDIF
			ENDIF
			/*
			IF bCSSetExitBev = FALSE
				IF IS_PED_UNINJURED(mBuddy.piPed)	
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(mBuddy.piPed)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-60.9116, 295.3501, 105.0975>>,1)
						//TASK_PLAY_ANIM(mBuddy.piPed,"RCMPaparazzo1IG_1","idle",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
						//SET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed,sWeaponMoveClipset)
						//TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","pap_base",INSTANT_BLEND_IN,SLOW_BLEND_OUT,500,AF_SECONDARY | AF_UPPERBODY)
						IF NOT DOES_ENTITY_EXIST(objCam)
							objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,20.0>>))	
							SET_ENTITY_COLLISION(objCam,FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(objCam)
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
								ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
							ENDIF
						ENDIF
						bCSSetExitBev = TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			IF bCSSetExitFranklin = FALSE
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")	
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
					SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),PEDMOVE_WALK)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-63.1068, 294.9655, 105.0442>>,PEDMOVEBLENDRATIO_WALK,-1,1,ENAV_NO_STOPPING | ENAV_DONT_AVOID_PEDS | ENAV_DONT_AVOID_OBJECTS)
					//TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),<<-63.1068, 294.9655, 105.0442>>,0.7,-1)
					bCSSetExitFranklin = TRUE
				ENDIF
			ENDIF
				
			IF bCSSetExitCam = TRUE
			AND	bCSSetExitBev = TRUE
			AND	bCSSetExitFranklin = TRUE
			AND NOT IS_SCREEN_FADING_OUT()
			//AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverlys_camera")	
				eState = SS_CLEANUP
			ENDIF
			
			/*
			IF IS_CUTSCENE_PLAYING()
					
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()	
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_ENTITY_HEADING(PLAYER_PED_ID()) - 247.6827)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),247.7034)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-12)
					eState = SS_CLEANUP
				ENDIF
			ELSE
				eState = SS_CLEANUP
			ENDIF
		
			IF IS_PED_UNINJURED(mBuddy.piPed)	
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")	
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
						objCam = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
					ENDIF
					IF NOT DOES_ENTITY_EXIST(objCam)
						objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,10.0>>))	
					ENDIF
					IF DOES_ENTITY_EXIST(objCam)
						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
							ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
		BREAK

		CASE SS_CLEANUP
			
			/*
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
				objCam = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
			ENDIF
			*/
			/*
			IF bCSSetExitBev = FALSE
				IF IS_PED_UNINJURED(mBuddy.piPed)	
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")	
						FORCE_PED_AI_AND_ANIMATION_UPDATE(mBuddy.piPed)
						//SET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed,sWeaponMoveClipset)
						//TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","pap_base",INSTANT_BLEND_IN,SLOW_BLEND_OUT,500,AF_SECONDARY | AF_UPPERBODY)
						IF NOT DOES_ENTITY_EXIST(objCam)
							objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,10.0>>))	
						ENDIF
						IF DOES_ENTITY_EXIST(objCam)
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
								ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
							ENDIF
						ENDIF
						bCSSetExitBev = TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			/*
			IF bCSSetExitBev = FALSE
				IF IS_PED_UNINJURED(mBuddy.piPed)	
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(mBuddy.piPed)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-60.9116, 295.3501, 105.0975>>,1)
						//TASK_PLAY_ANIM(mBuddy.piPed,"RCMPaparazzo1IG_1","idle",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_SECONDARY | AF_UPPERBODY)
						//SET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed,sWeaponMoveClipset)
						//TASK_PLAY_ANIM(mBuddy.piPed,"rcmpaparazzo_2","pap_base",INSTANT_BLEND_IN,SLOW_BLEND_OUT,500,AF_SECONDARY | AF_UPPERBODY)
						IF NOT DOES_ENTITY_EXIST(objCam)
							objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,10.0>>))	
							SET_ENTITY_COLLISION(objCam,FALSE)
						ENDIF
						IF DOES_ENTITY_EXIST(objCam)
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
								ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
							ENDIF
						ENDIF
						bCSSetExitBev = TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			//STOP_CUTSCENE()
			/*
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
				objCam = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverlys_camera"))
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
				//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-74.6184, 299.8621, 105.4468>>, 247.7034)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_ENTITY_HEADING(PLAYER_PED_ID()) - 247.6827)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),247.7034)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-12)
			ENDIF
		
			IF IS_PED_UNINJURED(mBuddy.piPed)
			AND IS_PED_UNINJURED(PLAYER_PED_ID())

				//TASK_ACHIEVE_HEADING(mBuddy.piPed, 161.8124)
				TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 247.7034)
			
			ENDIF
			*/
			
			//IF WAS_CUTSCENE_SKIPPED()
			//OR IS_SCREEN_FADED_OUT()	
			//WAIT_FOR_CUTSCENE_TO_STOP()
			//IF WAS_CUTSCENE_SKIPPED()
			
			RC_END_CUTSCENE_MODE()
			
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			
			iTimerCutsceneFinished = GET_GAME_TIMER()
			
			/*
			IF IS_PED_UNINJURED(mBuddy.piPed)
				IF NOT DOES_ENTITY_EXIST(objCam)
					objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,10.0>>))	
				ENDIF
				IF DOES_ENTITY_EXIST(objCam)
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
						ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
					ENDIF
				ENDIF
			ENDIF
			*/
			IF bCSSkipped = TRUE
				/*
				IF NOT DOES_ENTITY_EXIST(objCam)	
					objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,20.0>>))	
				ENDIF
				IF NOT DOES_ENTITY_EXIST(mBuddy.piPed)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverley"))
						mBuddy.piPed = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Beverley"))
					ENDIF
				ELSE
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverley")	
					
					IF IS_ENTITY_ALIVE(mBuddy.piPed)
						IF IS_ENTITY_ALIVE(objCam)
						//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverlys_camera")
							ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
						//ENDIF
						ENDIF
						SAFE_TELEPORT_ENTITY(mBuddy.piPed,<<-68.5685, 298.0157, 105.2401>>, 247.9095)
						IF HAS_CLIP_SET_LOADED("move_m@casual@f")
							SET_PED_MOVEMENT_CLIPSET(mBuddy.piPed,"move_m@casual@f")
						ENDIF
						FORCE_PED_MOTION_STATE(mBuddy.piPed,MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
						SET_PED_MIN_MOVE_BLEND_RATIO(mBuddy.piPed,PEDMOVE_WALK)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-60.9116, 295.3501, 105.0975>>,1)
						bCSSetExitBev = TRUE
					ENDIF
				ENDIF
				*/
			ENDIF
			NEXT_STAGE() //advance stage
		BREAK
		
		CASE SS_SKIPPED
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

#IF NOT IS_JAPANESE_BUILD	
	FUNC BOOL IS_JUSTIN_THRUSTIN()

		FLOAT fPhaseForSound
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene)
			fPhaseForSound = GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene)
			IF fPhaseForSound >= 0.069
			AND fPhaseForSound < 0.069 + 0.05
				RETURN TRUE
			ENDIF
			IF fPhaseForSound >= 0.249
			AND fPhaseForSound < 0.249 + 0.05
				RETURN TRUE
			ENDIF
			IF fPhaseForSound >= 0.486
			AND fPhaseForSound < 0.486 + 0.05
				RETURN TRUE
			ENDIF
			IF fPhaseForSound >= 0.668
			AND fPhaseForSound < 0.668 + 0.05
				RETURN TRUE
			ENDIF
			IF fPhaseForSound >= 0.881
			AND fPhaseForSound < 0.881 + 0.05
				RETURN TRUE
			ENDIF
		ENDIF
		
	RETURN FALSE
	ENDFUNC
#ENDIF

/// PURPOSE:
///    Flowing bev around hotel grounds update
///    Performs update based on the current mission stage
///    Checks for failing and objective completion
/// PARAMS:
///    stage - The stage to update
PROC UPDATE_SPECIFIC_FOLLOW(MISSION_STAGE stage)
	MONITER_PLAYER_FAILING()

	IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
	AND IS_PED_UNINJURED(mHotelStaff[1].piPed)
		MAID_STATES()
		BUSBOY_STATES()
	ENDIF
	
	VECTOR vCurrentPos
	VECTOR vCorrectPos
	
	TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	SWITCH stage
		CASE MS_FOLLOW_BEV_ROAD
			IF NOT IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0,3,0>>),0.1,FALSE,TRUE,FALSE,FALSE,FALSE)	
				bBentleyGotBevStuck = FALSE
				IF SHOULD_PROGRESS_FOLLOW_STATE(ROUTE_STAIRS_1_MIDDLE, <<2.5,2.5,2.5>>) //<<2.5,2.5,2.5>>
					eState = SS_CLEANUP
				ENDIF
				IF bConvoNowDude	= FALSE
				AND IS_ENTITY_IN_ANGLED_AREA( mBuddy.piPed, <<-70.084908,298.558319,104.780319>>, <<-62.144356,295.838318,108.372864>>, 5.500000)
					IF IS_SCREEN_FADED_IN()	
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_WALKA", CONV_PRIORITY_MEDIUM)
							bConvoNowDude = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,25)
					AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mHotelStaff[0].piPed,15) 	
						IF bConvoDontInterfere = FALSE
							bConvoDontInterfere = TRUE
						ELSE
							IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mHotelStaff[0].piPed,15) 
								IF NOT IS_STRING_NULL_OR_EMPTY(root)
									IF ARE_STRINGS_EQUAL(root, "PAP2_WALKB")
										KILL_ANY_CONVERSATION()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
				PROGRESS_BEV_ROUTE(TRUE, ROUTE_STAIRS_1_BOTTOM)
			ELSE
				bBentleyGotBevStuck = TRUE
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(mBuddy.piPed,0)
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2.997917,292.149750,109.163933>>, <<7.916159,316.194916,116.919395>>, 40.750000)
					IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
						TASK_LOOK_AT_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID(),-1)
						TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed,FALSE)
						TASK_TURN_PED_TO_FACE_ENTITY(mHotelStaff[0].piPed,PLAYER_PED_ID())
						MISSION_FAILED(FR_ATTENTION)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE MS_FOLLOW_BEV_GUARDS
			IF SHOULD_PROGRESS_FOLLOW_STATE(ROUTE_STAIRS_2_BOTTOM, <<2.5,2.5,2.5>>)
				SK_PRINT("CLEANUP FOLLOW BEV GUARDS")
				eState = SS_CLEANUP
			ENDIF
			DO_STAFF_CONVO(bExpireBevSpeech1, bExpireStaffSpeech)
			PROGRESS_BEV_ROUTE()
		BREAK

		CASE MS_FOLLOW_TO_POPPY
			REQUEST_ANIM_DICT("cover@move@base@core") // preload cover anims B*1441589
			IF fLodScale < 1.0
				fLodScale = 0
			ELIF fLodScale < 1.8
				fLodScale += 0.005
			ENDIF
			IF fLodScale > 1.8
				fLodScale = 1.8
			ENDIF
			OVERRIDE_LODSCALE_THIS_FRAME(fLodScale)
			STOP_STAFF_CONV_CHECK()
			IF bPlayerSentToCover = TRUE
				#IF NOT IS_JAPANESE_BUILD
					IF IS_JUSTIN_THRUSTIN()
						IF HAS_SOUND_FINISHED(iGruntSounds)
							PLAY_SOUND_FROM_ENTITY(iGruntSounds,"AMBIENT_SEX",mPoppy.piPed,"PAPARAZZO_02_SOUNDSETS")
						ENDIF
					ENDIF
					fGruntVol += 0.005
					IF fGruntVol > 0.6
						fGruntVol = 0.6
					ENDIF
					//#IF IS_DEBUG_BUILD	
						//PRINTFLOAT(fGruntVol)
						//PRINTNL()
					//#ENDIF
					SET_VARIABLE_ON_SOUND(iGruntSounds,"ONESHOT_VOLUME",fGruntVol)
				#ENDIF	
				DO_PLAYER_HOLDING_CAMERA()
				IF HAS_CLIP_SET_LOADED(sWeaponMoveClipsetFrank)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(),sWeaponMoveClipsetFrank)	
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.757969,319.505890,111.495361>>, <<-33.262951,318.437958,113.495361>>, 1.000000)
					SK_PRINT("TK~~~~Player in cover spot~~~~")
					++iFramesPlayerInCoverSpot
				ENDIF
				IF GET_GAME_TIMER() > iTimerPlayerSentToCover + 200		
					iFramesPlayerInCoverSpot = iFramesPlayerInCoverSpot
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d")	
					AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d") >= 0.4
						#IF NOT IS_JAPANESE_BUILD
							IF bSetAudioPos = FALSE
								//INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("PAP2_IG1_POPPYSEX",<<-30.990408,301.092346,112.681885>>)
								INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("PAP2_IG1_POPPYSEX",mPoppy.piPed)
								SK_PRINT("TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK")
								bSetAudioPos = TRUE
							ENDIF
						#ENDIF
						#IF NOT IS_JAPANESE_BUILD	
							IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("PAP2_IG1_POPPYSEX",0) = TRUE
							//IF LOAD_STREAM("CUTSCENES_PAP2_IG1_POPPYSEX_CENTER")		
								SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
								SET_PED_CAN_TORSO_IK(PLAYER_PED_ID(),TRUE)
								SET_PED_CAN_TORSO_REACT_IK(PLAYER_PED_ID(),TRUE)
								eState = SS_CLEANUP
							ENDIF
						#ENDIF
						#IF IS_JAPANESE_BUILD	
							SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
							SET_PED_CAN_TORSO_IK(PLAYER_PED_ID(),TRUE)
							SET_PED_CAN_TORSO_REACT_IK(PLAYER_PED_ID(),TRUE)
							eState = SS_CLEANUP
						#ENDIF
					ENDIF
					IF GET_GAME_TIMER() > iTimerPlayerSentToCover + 10000	
						#IF NOT IS_JAPANESE_BUILD
							IF bSetAudioPos = FALSE
								//INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("PAP2_IG1_POPPYSEX",<<-30.990408,301.092346,112.681885>>)
								INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("PAP2_IG1_POPPYSEX",mPoppy.piPed)
								SK_PRINT("TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK")
								bSetAudioPos = TRUE
							ENDIF
						#ENDIF
						#IF NOT IS_JAPANESE_BUILD	
							IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("PAP2_IG1_POPPYSEX",0) = TRUE
							//IF LOAD_STREAM("CUTSCENES_PAP2_IG1_POPPYSEX_CENTER")		
								SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
								SET_PED_CAN_TORSO_IK(PLAYER_PED_ID(),TRUE)
								SET_PED_CAN_TORSO_REACT_IK(PLAYER_PED_ID(),TRUE)
								eState = SS_CLEANUP
							ENDIF
						#ENDIF
						#IF IS_JAPANESE_BUILD	
							SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
							SET_PED_CAN_TORSO_IK(PLAYER_PED_ID(),TRUE)
							SET_PED_CAN_TORSO_REACT_IK(PLAYER_PED_ID(),TRUE)
							eState = SS_CLEANUP
						#ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF NOT bOkToPassCam		
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-35.765366,324.991486,111.696045>>, <<-37.292095,327.326324,113.446045>>, 1.000000)  //player went ahead
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-38.161537,326.245728,111.696045>>, <<-36.300270,325.416077,113.446045>>, 1.250000)
				//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-37.256691,325.465210,112.696281>>,<<0.1,0.1,0.1>>)
				//AND IS_POSITION_OCCUPIED(<<-37.256691,325.465210,112.696281>>,0.1,FALSE,FALSE,TRUE,FALSE,FALSE,mBuddy.piPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,2)	//6
						//MISSION_FAILED(FR_POPPY_SCARED)
						TASK_STAND_STILL(mBuddy.piPed,-1)
						IF bPassCamConvo = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
								IF NOT bPlayerSentToCover
									IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_PASS", CONV_PRIORITY_HIGH) //Take my camera and film this shit, I'll keep a look out.
										bPassCamConvo = TRUE
									ENDIF
								ELSE
									bPassCamConvo = TRUE
								ENDIF
							ENDIF
						ENDIF
						bBevSentToCover = TRUE
						bOkToPassCam = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// For B*2029356
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-36.526478,324.284668,111.445259>>, <<-38.700287,327.491364,114.195259>>, 3.000000)
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			ENDIF
		
			IF bPoppyCreated = TRUE	
				//IF iCoverSeq > 0					
				//IF bBevSentToCover	
				IF bOkToPassCam	
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-37.842674,323.426697,100.180809>>, <<-31.976759,320.666077,113.695953>>, 5.250000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-35.976223,326.202057,111.196075>>, <<-37.204895,325.147552,113.495361>>, 1.000000)	
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,1.8)	
						IF bPlayerSentToCover = FALSE	
							//SET_PLAYER_CAN_USE_COVER(PLAYER_ID(),TRUE)
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH)
							CLEAR_PRINTS()
							SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),FALSE)
							SET_PED_CAN_TORSO_IK(PLAYER_PED_ID(),FALSE)
							SET_PED_CAN_TORSO_REACT_IK(PLAYER_PED_ID(),FALSE)
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
							IF HAS_CLIP_SET_LOADED(sWeaponMoveClipsetFrank)
								SET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID(),sWeaponMoveClipsetFrank)
							ELSE
								REQUEST_CLIP_SET(sWeaponMoveClipsetFrank)
							ENDIF
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
							DO_PLAYER_HOLDING_CAMERA()
							//CLEAR_PED_TASKS(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(objCam)
								
								//ATTACH_ENTITY_TO_ENTITY(objCam, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE)
							
								ATTACH_ENTITY_TO_ENTITY(objCam, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
							
							ENDIF
							
							IF NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())	
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),TRUE,"DEFAULT_ACTION")
							ENDIF
							
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-35.552845,324.443054,111.696045>>, <<-37.763191,327.745605,113.696045>>, 1.000000)
							OR IS_PED_RUNNING(PLAYER_PED_ID())
							OR IS_PED_SPRINTING(PLAYER_PED_ID())
							OR IS_PED_JUMPING(PLAYER_PED_ID())
							OR IS_PED_RAGDOLL(PLAYER_PED_ID())
								SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-37.0561, 326.6470, 111.6960>>, 203.1247)
								//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_WALK,FALSE,FAUS_CUTSCENE_EXIT)
							ENDIF
							
							vCurrentPos = GET_ENTITY_COORDS(mBuddy.piPed)
							vCorrectPos = <<-37.224827,325.531250,112.696030>>
							IF vCurrentPos.x < vCorrectPos.x - 0.2
							OR vCurrentPos.x > vCorrectPos.x + 0.2
							OR vCurrentPos.y < vCorrectPos.y - 0.2
							OR vCurrentPos.y > vCorrectPos.y + 0.2		
								SAFE_TELEPORT_ENTITY(mBuddy.piPed,<<-39.2954, 328.3034, 111.6960>>, 218.8305) //warp him back a bit, something got fucked
							ENDIF
							
							//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-36.1845, 325.5363, 111.6960>>, 195.2352)
							//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(),MS_ON_FOOT_RUN,FALSE,FAUS_CUTSCENE_EXIT)
							
							SEQUENCE_INDEX seqFrankFilm
								OPEN_SEQUENCE_TASK(seqFrankFilm)
								//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-33.8491, 319.3350, 111.6960>>,2,-1,1,ENAV_ACCURATE_WALKRUN_START,184.4715) //184.4715
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-33.8491, 319.3350, 111.6960>>,1.4,-1,1,ENAV_ACCURATE_WALKRUN_START,184.4715) //184.4715
								TASK_PLAY_ANIM(NULL,"rcmpaparazzo_2","idle_d",2,INSTANT_BLEND_OUT,-1,AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seqFrankFilm)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(),seqFrankFilm)
							CLEAR_SEQUENCE_TASK(seqFrankFilm)
							
							IF DOES_CAM_EXIST(camFrankFilm)
								DESTROY_CAM(camFrankFilm)
							ENDIF
							IF DOES_CAM_EXIST(camFrankFilm2)
								DESTROY_CAM(camFrankFilm2)
							ENDIF
							
							camFrankFilm = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
							camFrankFilm2 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE)
						
							//SET_CAM_PARAMS(camFrankFilm,<<-37.9019, 327.7973, 113.3252>>, <<-1.8838, -0.0000, -147.4014>>,34.0)
							SET_CAM_PARAMS(camFrankFilm,<<-37.8130, 328.5478, 113.3867>>, <<-5.9528, 0.0001, -163.8669>>,23.8)

							SET_CAM_PARAMS(camFrankFilm2,<<-34.8325, 320.8304, 113.3748>>, <<-6.6594, -0.0146, -161.6700>>,34.0)
							
							SET_CAM_NEAR_DOF(camFrankFilm,2)
							SET_CAM_FAR_DOF(camFrankFilm,40)
							SET_CAM_DOF_STRENGTH(camFrankFilm,0.5)
							SET_CAM_NEAR_DOF(camFrankFilm2,2)
							SET_CAM_FAR_DOF(camFrankFilm2,40)
							SET_CAM_DOF_STRENGTH(camFrankFilm2,0.5)
							SET_CAM_ACTIVE(camFrankFilm2,TRUE)
							SET_CAM_ACTIVE(camFrankFilm,TRUE)
							//SET_CAM_ACTIVE_WITH_INTERP(camFrankFilm2,camFrankFilm,3600,GRAPH_TYPE_SIN_ACCEL_DECEL)
							SET_CAM_ACTIVE_WITH_INTERP(camFrankFilm2,camFrankFilm,7000,GRAPH_TYPE_SIN_ACCEL_DECEL)
							RENDER_SCRIPT_CAMS(TRUE,FALSE)
							IF DOES_CAM_EXIST(camFrankFilm)
								SHAKE_CAM(camFrankFilm,"HAND_SHAKE",1.0)
							ENDIF
							IF DOES_CAM_EXIST(camFrankFilm2)
								SHAKE_CAM(camFrankFilm2,"HAND_SHAKE",1.0)
							ENDIF
							
							//TASK_SEEK_COVER_TO_COORDS(PLAYER_PED_ID(),<< -33.1418, 318.9297, 111.6957 >>,<< -24.1590, 312.4199, 111.5455 >>,-1,TRUE)
							iTimerPlayerSentToCover = GET_GAME_TIMER()
							bPlayerSentToCover = TRUE
							
							#IF NOT IS_JAPANESE_BUILD
								fGruntVol = 0.1
							#ENDIF
							
							PLAY_SOUND_FROM_ENTITY(-1,"CAMERA_FOLEY",PLAYER_PED_ID(),"PAPARAZZO_02_SOUNDSETS")
							
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							THEFEED_HIDE_THIS_FRAME()
							
							//OVERRIDE_LODSCALE_THIS_FRAME(1)	
							
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
			
			IF bExpireInactive
				DO_POPPY_CONVO(bExpireBevSpeech1, bExpireBevSpeech2)
			ENDIF
			
			PROGRESS_BEV_ROUTE(TRUE, ROUTE_POOL_STAIRS)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Skip a specific follow stage
/// PARAMS:
///    stage - Stage to skip
PROC SKIP_SPECIFIC_FOLLOW(MISSION_STAGE stage)
	SWITCH stage
		CASE MS_FOLLOW_BEV_ROAD
			IF IS_PED_UNINJURED(mBuddy.piPed)
				CLEAR_PED_TASKS(mBuddy.piPed)
			ENDIF
			iBevRoute = ROUTE_STAIRS_1_TOP
			SAFE_TELEPORT_ENTITY(mBuddy.piPed, vBevRoute[ROUTE_STAIRS_1_MIDDLE], 338.4024) //<<-11.4650, 283.2339, 107.3171>>
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -14.1493, 280.7966, 107.2078 >>, 317.7051)
			bExpireBevSpeech2 = TRUE
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			eState = SS_ACTIVE
		BREAK
		
		CASE MS_FOLLOW_BEV_GUARDS
			IF IS_PED_UNINJURED(mBuddy.piPed)
				CLEAR_PED_TASKS(mBuddy.piPed)
			ENDIF
			INT i 
			FOR i = 0 TO (MAX_STAFF-1)
				IF IS_PED_UNINJURED(mHotelStaff[i].piPed)
					CLEAR_PED_TASKS(mHotelStaff[i].piPed)
				ENDIF
			ENDFOR
			SAFE_TELEPORT_ENTITY(mBuddy.piPed, vBevRoute[ROUTE_STAIRS_2_BOTTOM], 63.3365)
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -2.2002, 317.2632, 109.9189 >>, 60.7242)
			iBevRoute = ROUTE_POOL_STAIRS
			bExpireStaffSpeech = TRUE
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			eState = SS_ACTIVE
		BREAK

		CASE MS_FOLLOW_TO_POPPY
			IF IS_PED_UNINJURED(mBuddy.piPed)
				CLEAR_PED_TASKS(mBuddy.piPed)
			ENDIF
			bCanCreatePoppy = TRUE
			LOAD_AND_CREATE_POP()
			iBevRoute = ROUTE_POPPY
			SAFE_TELEPORT_ENTITY(mBuddy.piPed, << -36.7532, 323.5341, 111.6957 >>, 165.7917)
			SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), << -35.1361, 320.0255, 111.6955 >>, 199.6725)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			eState = SS_ACTIVE		
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Cleanup function  for follow states
PROC SKIP_SPECIFIC_CLEANUP()
	IF eMissionStage = MS_FOLLOW_TO_POPPY
		INT i
		FOR i=0 TO (MAX_STAFF-1)
			SAFE_DELETE_PED(mHotelStaff[i].piPed)
		ENDFOR	
	ENDIF
	
	bExpireBevSpeech1 = FALSE
	bExpireBevSpeech2 = FALSE
	NEXT_STAGE()

ENDPROC

/// PURPOSE:
///    This function contains all the common checks and updates
///    that are used for the follow to poppy mission stages
///    something goes wrong in those stages check here first
PROC FOLLOWING_BEV()

	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	IF IS_REPLAY_IN_PROGRESS()
	AND bDoDelayedfade
	AND GET_GAME_TIMER() > iTimerStartStage + 900
	AND IS_SCREEN_FADED_OUT()
		SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	ENDIF
	
	IF iSeqMusic = 0
		TRIGGER_MUSIC_EVENT("PAP2_START")	
		iSeqMusic = 1
	ENDIF
	
	IF (eMissionStage = MS_FOLLOW_BEV_ROAD AND IS_PED_RAGDOLL(mBuddy.piPed))	
		MISSION_FAILED(FR_BUDDY_HARMED)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<26.145760,302.110504,109.463455>>, <<-51.901978,337.526123,124.382706>>, 40.750000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-6.873048,293.624451,111.962662>>, <<-11.449873,283.594482,107.314468>>, 2.000000) //Steps B*1935724
			MISSION_FAILED(FR_ATTENTION)
		ENDIF	
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-6.007325,363.131378,112.006630>>, <<-24.759676,322.220612,115.753799>>, 27.500000)
		IF HAS_PLAYER_KILLED_SOMEONE()
		OR HAS_PLAYER_INJURED_SOMEONE()	
			IF IS_PED_UNINJURED(pedHotel[5])
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedHotel[5])
				TASK_REACT_AND_FLEE_PED(pedHotel[5],PLAYER_PED_ID())
			ENDIF	
			MISSION_FAILED(FR_ATTENTION)
		ENDIF
	ENDIF
	
	IF iBevRoute < 2
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
			IF IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_LR)	
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UD)	
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ENTER)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_SPRINT)
			OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_JUMP)
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_LR))
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_LOOK_UD))	
			OR (IS_PLAYER_IN_FIRST_PERSON_CAMERA() AND IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UP_ONLY))		
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)

	IF IS_PED_UNINJURED(pedHotel[1])	
		UPDATE_AI_PED_BLIP(pedHotel[1],bsHotelPed1)
	ENDIF
	IF IS_PED_UNINJURED(pedHotel[2])	
		UPDATE_AI_PED_BLIP(pedHotel[2],bsHotelPed2)
	ENDIF
	IF IS_PED_UNINJURED(pedHotel[3])	
		UPDATE_AI_PED_BLIP(pedHotel[3],bsHotelPed3)
	ENDIF
	IF IS_PED_UNINJURED(pedHotel[4])	
		UPDATE_AI_PED_BLIP(pedHotel[4],bsHotelPed4)
	ENDIF
	/*
	IF IS_PED_UNINJURED(pedHotel[5])	
		UPDATE_AI_PED_BLIP(pedHotel[5],bsHotelPed5)
	ENDIF
	*/
	IF IS_PED_UNINJURED(mHotelStaff[0].piPed)	
		UPDATE_AI_PED_BLIP(mHotelStaff[0].piPed,bsStaff1)
	ENDIF
	IF IS_PED_UNINJURED(mHotelStaff[1].piPed)	
		UPDATE_AI_PED_BLIP(mHotelStaff[1].piPed,bsStaff2)
	ENDIF
	IF IS_PED_UNINJURED(mPoppy.piPed)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mPoppy.piPed,<<18,18,2>>)
		AND IS_PED_FACING_PED(mPoppy.piPed,PLAYER_PED_ID(),60)
			MISSION_FAILED(FR_POPPY_SCARED)
		ELSE
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mPoppy.piPed,<<7,7,2>>)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-20.678993,298.966248,111.262657>>, <<-35.815018,306.085938,115.481369>>, 24.000000)  //Garden
				MISSION_FAILED(FR_POPPY_SCARED)
			ENDIF
		ENDIF
		UPDATE_AI_PED_BLIP(mPoppy.piPed,bsPoppy)
	ENDIF
	IF IS_PED_UNINJURED(mShagger.piPed)
		UPDATE_AI_PED_BLIP(mShagger.piPed,bsJustin)
	ENDIF	
	IF IS_PED_UNINJURED(mBodyGuard.piPed)	
		UPDATE_AI_PED_BLIP(mBodyGuard.piPed,bsSecurity)
	ENDIF
			
	IF IS_PED_UNINJURED(mBuddy.piPed)
		SET_PED_INCREASED_AVOIDANCE_RADIUS(mBuddy.piPed)
		SET_PED_TO_LOAD_COVER(mBuddy.piPed,TRUE)
		IF IS_ENTITY_IN_ANGLED_AREA( mBuddy.piPed, <<-7.914861,290.702606,106.425247>>, <<-13.110273,279.689789,112.533493>>, 5.500000) 	
			TEXT_LABEL_23 root =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			IF NOT IS_STRING_NULL_OR_EMPTY(root)
				IF ARE_STRINGS_EQUAL(root,"PAP2_WALKA")				
					KILL_ANY_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
		IF eMissionStage = MS_FOLLOW_BEV_ROAD
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,20)
				TEXT_LABEL_23 rootWalk =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				IF NOT IS_STRING_NULL_OR_EMPTY(rootWalk)
					IF ARE_STRINGS_EQUAL(rootWalk,"PAP2_WALKA")
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
			TEXT_LABEL_23 labelMaid =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
			IF NOT IS_STRING_NULL_OR_EMPTY(labelMaid)
				IF ARE_STRINGS_EQUAL(labelMaid,"PAP2_STAFF_7")	//4			
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(mHotelStaff[0].piPed)
						TASK_USE_MOBILE_PHONE(mHotelStaff[0].piPed,FALSE)
						//CLEAR_PED_SECONDARY_TASK(mHotelStaff[0].piPed)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,30)	
			AND IS_PED_FACING_PED(PLAYER_PED_ID(),mBuddy.piPed,70)
			AND NOT IS_PED_HEADTRACKING_PED(mBuddy.piPed,mHotelStaff[0].piPed)	
				IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(),mBuddy.piPed)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,-1)
				ENDIF
			ELSE
				IF IS_PED_HEADTRACKING_PED(mBuddy.piPed,mHotelStaff[0].piPed)		
					IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(),mHotelStaff[0].piPed)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mHotelStaff[0].piPed,-1)
					ENDIF
				ELSE	
					IF IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(),mBuddy.piPed)
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		DO_BEV_COVER_ANIMS()  //COMMENT IN FOR OLD
		IF bBevSentToCover
			IF GET_GAME_TIMER() > iTimerBevSentToCover + 1000	
				bOkToPassCam = TRUE
			ENDIF
		ENDIF
		IF IS_ENTITY_IN_RANGE_COORDS(mBuddy.piPed,<<-37.2030, 325.4259, 112.31>>,2)	
		AND bSpotted = FALSE	
			IF bPassCamConvo = FALSE
				IF NOT bPlayerSentToCover	
				AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<<-37.2030, 325.4259, 112.31>>,2)	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_PASS", CONV_PRIORITY_HIGH) //Take my camera and film this shit, I'll keep a look out.
						
						REPLAY_RECORD_BACK_FOR_TIME(9.0, 4.0, REPLAY_IMPORTANCE_LOW)
						
						bPassCamConvo = TRUE
						//bBevSentToCover = TRUE
						iTimerBevIdleLines = GET_GAME_TIMER()
					ENDIF
				ELSE
					bPassCamConvo = TRUE
					iTimerBevIdleLines = GET_GAME_TIMER()
				ENDIF
			ENDIF
			//IF GET_GAME_TIMER() > iTimerBevSentToCover + 1600
				//IF bPlayerSentToCover = FALSE		
					//SET_PED_CAN_ARM_IK(mBuddy.piPed,TRUE)
					//SET_IK_TARGET(mBuddy.piPed,IK_PART_ARM_RIGHT,NULL,-1,<<-36.85, 325.78, 112.21>>,ITF_ARM_TARGET_WRT_IKHELPER,-1,-1) //<<-36.83, 325.88, 112.50>>
					/*
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<<-37.2030, 325.4259, 112.31>>,4)	
					AND IS_PED_FACING_PED(PLAYER_PED_ID(),mBuddy.piPed,30)
						SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
						SET_IK_TARGET(PLAYER_PED_ID(),IK_PART_ARM_RIGHT,mBuddy.piPed,GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND),<<0,0,-0.25>>,ITF_ARM_TARGET_WRT_IKHELPER,-1,-1)
					ENDIF
					*/
				//ENDIF
			//ENDIF
			//IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,12)	
				IF NOT IS_PED_IN_COVER(mBuddy.piPed)	
					IF NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_a")
					AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_b")
					AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_c")
						//IF IS_POSITION_OCCUPIED(<<-37.256691,325.465210,112.696281>>,0.1,FALSE,FALSE,TRUE,FALSE,FALSE,mBuddy.piPed)		
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-37.256691,325.465210,112.696281>>,<<0.05,0.05,0.05>>)	
							/*
							IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
								TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-37.6480, 326.9788, 111.6960>>,1,-1,0.5,ENAV_DEFAULT,207.8044)
							ENDIF
							*/
						ELSE
							/*
							IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
							AND NOT IS_ENTITY_AT_COORD(mBuddy.piPed,<<-37.5661, 325.8084, 111.6960>>,<<0.5,0.5,0.5>>)	
								TASK_FOLLOW_NAV_MESH_TO_COORD(mBuddy.piPed,<<-37.5661, 325.8084, 111.6960>>,1,-1,0.5,ENAV_DEFAULT,333.9539)
								iTimerBevCoverTask = GET_GAME_TIMER()
								iTimerBevCoverTask = iTimerBevCoverTask
								IF bBevSentToCover = FALSE
									iTimerBevSentToCover = GET_GAME_TIMER()
								ENDIF
								bBevSentToCover = TRUE
							ENDIF
							*/
							    //COMMENT IN FOR OLD
							IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> WAITING_TO_START_TASK
								//TASK_PUT_PED_DIRECTLY_INTO_COVER(mBuddy.piPed,<<-37.24, 321.48, 112.31>>,-1,TRUE,0,TRUE,TRUE,NULL)
								IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-37.2396, 325.3983, 111.6961>>)	//For B*1819613 - making sure Bev gets into cover
									m_ciPap2 = ADD_COVER_POINT(<<-37.2396, 325.3983, 111.6961>>, 156.6960,COVUSE_WALLTOLEFT,COVHEIGHT_TOOHIGH,COVARC_90)
								ENDIF
								TASK_PUT_PED_DIRECTLY_INTO_COVER(mBuddy.piPed,<<-37.2022, 325.4253, 111.6954>>,-1,TRUE,0,TRUE,TRUE,m_ciPap2)  //<<-37.2030, 325.4259, 112.31>>
								IF IS_ENTITY_ALIVE(objCam)	
									DETACH_ENTITY(objCam)  //COMMENT IN FOR OLD
									ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) //COMMENT IN FOR OLD
								ENDIF
								RESET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed)  //COMMENT IN FOR OLD
								iTimerBevCoverTask = GET_GAME_TIMER()
								IF bBevSentToCover = FALSE
									iTimerBevSentToCover = GET_GAME_TIMER()
								ENDIF
								bBevSentToCover = TRUE
							ELSE
								IF GET_GAME_TIMER() > iTimerBevCoverTask + 2000
									CLEAR_PED_TASKS(mBuddy.piPed)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					//IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,5)
					IF IS_ENTITY_AT_COORD(mBuddy.piPed,<<-37.256691,325.465210,112.696281>>,<<0.1,0.1,0.1>>)	
						FREEZE_ENTITY_POSITION(mBuddy.piPed,TRUE)
					ENDIF
				ENDIF
			//ELSE
				//IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_STAND_STILL) <> PERFORMING_TASK
				//	TASK_STAND_STILL(mBuddy.piPed,-1)
				//ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	IF bPlayerSentToCover = FALSE	
		IF NOT DOES_ENTITY_EXIST(objCam)
			objCam = CREATE_OBJECT_NO_OFFSET(Prop_Pap_Camera_01, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mBuddy.piPed,<<0.0,0.0,10.0>>))	
		ENDIF
		IF DOES_ENTITY_EXIST(objCam)
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(objCam, mBuddy.piPed)
				
				//ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0.06,0,-0.06>>, <<0,0,90>>, TRUE, TRUE) // <<0.06,0,-0.06>>, <<0,0,90>>
				
				ATTACH_ENTITY_TO_ENTITY(objCam, mBuddy.piPed, GET_PED_BONE_INDEX(mBuddy.piPed, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
			
			ENDIF
		ENDIF
	ENDIF
	
	#IF NOT IS_JAPANESE_BUILD	
		SETUP_SYNC_SCENES()
		/*
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene)	
			IF HAS_SOUND_FINISHED(iSexSound)
				PLAY_SOUND_FROM_ENTITY(iSexSound,"AMBIENT_SEX",mPoppy.piPed,"PAPARAZZO_02_SOUNDSETS")
				SK_PRINT("TK************ SEX SOUNDS ************")
			ENDIF
		ENDIF
		*/
	#ENDIF
	#IF IS_JAPANESE_BUILD	
		PLAY_POPPY_SHAGGER_ANIM(0)
	#ENDIF
	
	IF bBevSentToCover
		DO_BEV_IDLE_AT_FILM_POS_LINES()
	ENDIF
	
	//CLEAR_AREA_OF_PEDS(<< -17.3155, 340.0973, 110.4743 >>, 200.0)// Area around pool
	IF bPlayerSentToCover = TRUE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
	ENDIF

	SWITCH eState
		CASE SS_INIT
			bCanCreatePoppy = TRUE
			IF SETUP_MISSION_STAGE(eMissionStage, bJumpSkip)
				FREEZE_ENTITY_POSITION(mBuddy.piPed,FALSE)
				eState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			REQUEST_HOTEL_MODELS()
			IF bHotelPedsSpawned = FALSE
				SPAWN_HOTEL_PEDS()
			ELSE
				MONITOR_HOTEL_PEDS()
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-11.128758,354.317535,109.160736>>, <<-24.442318,326.814362,114.161804>>, 18.750000)
			AND IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				IF NOT bJumpedInPool
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP2_POOL_JUMP) 
					bJumpedInPool = TRUE
				ENDIF
			ENDIF
			
			DO_MAID_STUFF()
			
			LOAD_AND_CREATE_POP()
			
			IF NOT bSpotted
			AND NOT bBudThreat
				DO_INACTIVE_CONV()
				MONITER_PEDS_PERCEPTION()
				IF GET_GAME_TIMER() > iTimerCutsceneFinished + 0	
					UPDATE_SPECIFIC_FOLLOW(eMissionStage)
				ENDIF
				SET_PED_MAX_MOVE_BLEND_RATIO(mBuddy.piPed,2)
				REPOSITION_SHAG_TABLE()
				//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-35.244846,323.518188,108.756752>>, <<-36.760059,326.002686,113.542892>>, 1.550000)
				IF NOT bPlayerSentToCover	
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-36.275143,328.586426,110.583931>>, <<-37.693676,325.320251,113.739784>>, 3.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.621841,328.434387,109.445808>>, <<-41.964497,330.402069,113.445808>>, 5.750000)	
						//SET_PLAYER_CAN_USE_COVER(PLAYER_ID(),FALSE)
						/*
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_COVER)
						IF IS_PED_IN_COVER(PLAYER_PED_ID())
							TASK_EXIT_COVER(PLAYER_PED_ID(),IDLE_COVER_EXIT,GET_ENTITY_COORDS(PLAYER_PED_ID()))
						ENDIF
						*/
					ELSE
						SET_PLAYER_CAN_USE_COVER(PLAYER_ID(),TRUE)
					ENDIF
				ELSE
					SET_PLAYER_CAN_USE_COVER(PLAYER_ID(),TRUE)
				ENDIF
			ELSE
				IF bBudThreat
					IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mHotelStaff[0].piPed, FALSE)
					ENDIF
					IF IS_PED_UNINJURED(mHotelStaff[1].piPed)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mHotelStaff[1].piPed, FALSE)
					ENDIF
					DO_BEV_THRETENED_CONV()
				ELIF bSpotted
					IF IS_PED_UNINJURED(mHotelStaff[0].piPed)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mHotelStaff[0].piPed, FALSE)
						IF bStaffThreatened
							IF GET_SCRIPT_TASK_STATUS(mHotelStaff[0].piPed,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK
								TASK_REACT_AND_FLEE_PED(mHotelStaff[0].piPed,PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_UNINJURED(mHotelStaff[1].piPed)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mHotelStaff[1].piPed, FALSE)
						IF bStaffThreatened
							IF GET_SCRIPT_TASK_STATUS(mHotelStaff[1].piPed,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK
								TASK_REACT_AND_FLEE_PED(mHotelStaff[1].piPed,PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					IF bStaffThreatened
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					IF NOT bOkToPassCam		
						IF NOT bExpireSpottedSpeech
							IF GET_DISTANCE_BETWEEN_ENTITIES(mBuddy.piPed, PLAYER_PED_ID()) < 12
								bExpireSpottedSpeech = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_SPOT", CONV_PRIORITY_HIGH)
							ELSE
								bExpireSpottedSpeech = TRUE
							ENDIF
						ELSE	
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-21.921995,295.476227,111.571716>>, <<-62.670685,314.495544,113.836311>>, 33.750000)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-31.052395,322.647614,111.402245>>, <<-53.921097,333.265991,114.377518>>, 17.750000)	
									MISSION_FAILED(FR_POPPY_SCARED)
								ELSE
									MISSION_FAILED(FR_ATTENTION)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(biBuddyBlip)
				IF IS_THIS_PRINT_BEING_DISPLAYED("PAP2_06")
				OR NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,70)	
					SET_BLIP_FLASHES(biBuddyBlip,TRUE)
				ELSE
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,70)
						SET_BLIP_FLASHES(biBuddyBlip,FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			SKIP_SPECIFIC_CLEANUP()
		BREAK

		CASE SS_SKIPPED
			SKIP_SPECIFIC_FOLLOW(eMissionStage)
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///   Beverly explains the face recog system and the player can get used to the camera
PROC CAMERA_TUTORIAL() 

	//PRINTINT(iBadFilming)
	//PRINTNL()
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	OVERRIDE_LODSCALE_THIS_FRAME(1.8)	
	
	REPOSITION_SHAG_TABLE()

	IF iBadFilming > 500
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF iNumFilmFails <> MAX_FILM_FAILS	
				IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FAILFIL", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES) //Give me back that camera! You were meant to film the people fucking!
					bPutCamInFranksHand = TRUE	
					iNumFilmFails = MAX_FILM_FAILS
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)

	#IF NOT IS_JAPANESE_BUILD	
		IF NOT HAS_SOUND_FINISHED(iSexSound)	
			STOP_SOUND(iSexSound)
			SK_PRINT("TK************ STOP SEX SOUNDS ************")
		ENDIF
	#ENDIF

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF eState > SS_INIT
	AND eCamTutStage > CTS_INIT_EXPLAIN
		IF DOES_CAM_EXIST(camMain)
			STOP_CAM_POINTING(camMain)
		ENDIF
		IF DOES_CAM_EXIST(camMain)
			fCamFov = GET_CAM_FOV(camMain)
		ENDIF
		MANAGE_CAMERA(TRUE)
	ENDIF
	
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_CAMERA_TUTORIAL, bJumpSkip)
				DISABLE_SELECTOR()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_FILMING, "FILM POPPY") //Filming started, set checkpoint
				CLEAR_PRINTS()
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),TRUE)
				IF DOES_CAM_EXIST(camMain)
				AND IS_PED_UNINJURED(mPoppy.piPed)
					SET_CAM_ROT(camMain,<<-1.6576, 0.0000, -170.9825>>)
				ENDIF
				
				#IF NOT IS_JAPANESE_BUILD	
					STOP_SOUND(iGruntSounds)
				#ENDIF
				
				RELEASE_SCRIPT_AUDIO_BANK()
				bDumpedBank = TRUE
				
				PLAY_POPPY_SHAGGER_ANIM(1)
				
				RESET_BOX_COL()
				
				bForceTutFin = FALSE
				
				#IF NOT IS_JAPANESE_BUILD
					iBadFilmNoBev = 0
				#ENDIF
				
				fFaceOffset = -0.05
				fFaceOffsetz = -0.03
				
				iTimerStartStage = GET_GAME_TIMER()
				iTimerStartedFilming = GET_GAME_TIMER()
				
				IF IS_PED_UNINJURED(mBuddy.piPed)
					SAFE_TELEPORT_ENTITY(mBuddy.piPed,<<-37.2184, 325.3970, 111.6958>>, 242.3437)
				ENDIF
				
				eState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE				
			SWITCH eCamTutStage
				CASE CTS_INIT_EXPLAIN
					
					IF DOES_CAM_EXIST(camMain)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF

					IF IS_PED_UNINJURED(mBuddy.piPed)
					AND IS_PED_UNINJURED(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(mBuddy.piPed, mPoppy.piPed,-1)
					ENDIF 

					bObjectiveShown = FALSE
					eCamTutStage = CTS_EXPLAINING
				
				BREAK
				
				CASE CTS_EXPLAINING
					eCamTutStage = CTS_WAIT_DONE
				BREAK
				
				CASE CTS_WAIT_DONE
					eState = SS_CLEANUP
				BREAK
				
			ENDSWITCH			
		BREAK
		
		CASE SS_CLEANUP
			bLineSet = FALSE
			bPictureTaken = FALSE
			iFilmTime = 0
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			eState = SS_CLEANUP
			KILL_ANY_CONVERSATION()
			bPictureTaken = TRUE	
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Player films sexscene
///  --The camera tutorial and film poppy states are quite 
///    similar could maybe functionise them like the 
///    follow states
PROC FILM_POPPY()
	
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	
	OVERRIDE_LODSCALE_THIS_FRAME(1.8)	
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	#IF NOT IS_JAPANESE_BUILD	
		//PRINTINT(iBadFilming)
		//PRINTNL()
		DO_PLAYER_HOLDING_CAMERA()
		//OVERRIDE_LODSCALE_THIS_FRAME(1.4)	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)	
			
			fSSPhase = GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2)
			
			//PRINTNL()
			//PRINTFLOAT(fSSPhase)
			
			IF NOT bDone1stPersonFlash
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					IF fSSPhase > 0.872
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bDone1stPersonFlash = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF fSSPhase > 0.835512
				IF iSeqMusic <> 2
					TRIGGER_MUSIC_EVENT("PAP2_SPOTTED")	
					iSeqMusic = 2
				ENDIF
				bPlayerFailed = FALSE
			ENDIF
			IF fSSPhase >= 0.97
				IF IS_PED_UNINJURED(mShagger.piPed)	
					IF GET_SCRIPT_TASK_STATUS(mShagger.piPed, SCRIPT_TASK_COMBAT) <> PERFORMING_TASK
						TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
					ENDIF 
				ENDIF
				bPictureTaken = TRUE
			ENDIF
			IF fSSPhase >= 0.972620
				bPictureTaken = TRUE
			ENDIF
			IF fSSPhase > fBreakoutPhase //0.888042//0.835512//0.947229 //0.989654
				eState = SS_CLEANUP
			ENDIF
		ELSE
			
			// The synced scene has stopped playing before reaching the breakout point 
			// Something probably went wrong with the audio stream eg: B*2217471 - need to cleanup
			IF eState >	SS_INIT
				IF GET_GAME_TIMER() > iTimerStartStage + 6000
					SK_PRINT(" IS_SYNCHRONIZED_SCENE_RUNNING = FALSE! Something went wrong, probably the audio stream ")
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		
		ENDIF
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)
			IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene_breakout) > fBreakoutPhase//0.947229
				
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 3.0, REPLAY_IMPORTANCE_LOW)
				
				eState = SS_CLEANUP
			ENDIF
		ENDIF
		//0.960587 - justin come out
		//0.989654 - poppy come out
		REPOSITION_SHAG_TABLE()
		
		IF iBadFilming > 500
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF iNumFilmFails <> MAX_FILM_FAILS	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_FAILFIL", CONV_PRIORITY_HIGH) //Give me back that camera! You were meant to film the people fucking!
						bPutCamInFranksHand = TRUE
						iNumFilmFails = MAX_FILM_FAILS
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	#IF IS_JAPANESE_BUILD	
		//UNUSED_PARAMETER(bDone1stPersonFlash)
	
		IF NOT bConvoJapaneseVersion
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, sTextBlock, "PAP2_SEX", "PAP2_SEX_19", CONV_PRIORITY_VERY_HIGH) //I'm not releasing a sex tape for at least three years, unless my agent tells me to.
				bConvoJapaneseVersion = TRUE
				iSeqJapaneseVersionConvo = 0
				SK_PRINT("TK************ JAPANESE VERSION - TRIGGER CONVO FILM POPPY ************TK")
			ELSE
				SK_PRINT("TK************ CONVO WONT TRIGGER? FILM POPPY ************TK")
			ENDIF
		ELSE
			TEXT_LABEL_23 labelJapaneseVersion = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
			IF iSeqJapaneseVersionConvo = 0	
				IF ARE_STRINGS_EQUAL(labelJapaneseVersion, "PAP2_SEX_26") 
				OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					KILL_FACE_TO_FACE_CONVERSATION()
					iSeqJapaneseVersionConvo = 1	
				ENDIF
			ELIF iSeqJapaneseVersionConvo = 1	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ESCAPE2", CONV_PRIORITY_VERY_HIGH) //There's someone watching! Creepy! Creepy!
						iTimerJapaneseVersionNoticePlayer = GET_GAME_TIMER()
						TASK_LOOK_AT_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
						iSeqJapaneseVersionConvo = 2
						SK_PRINT("TK************ JAPANESE VERSION - TRIGGER CONVO2 FILM POPPY ************TK")
					ELSE
						SK_PRINT("TK************ CONVO2 WONT TRIGGER? FILM POPPY ************TK")
					ENDIF
				ENDIF
			ELIF iSeqJapaneseVersionConvo = 2
				IF GET_GAME_TIMER() > iTimerJapaneseVersionNoticePlayer + 600
					IF NOT IS_PED_HEADTRACKING_PED(mPoppy.piPed,PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1)
					ENDIF
				ENDIF
				IF GET_GAME_TIMER() > iTimerJapaneseVersionNoticePlayer + 2800
					IF NOT IS_PED_FACING_PED(mShagger.piPed,PLAYER_PED_ID(),40)
						IF GET_SCRIPT_TASK_STATUS(mShagger.piPed,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(mShagger.piPed,PLAYER_PED_ID(),-1)
						ENDIF
					ENDIF
				ENDIF
				IF GET_GAME_TIMER() > iTimerJapaneseVersionNoticePlayer + 2500
					IF NOT IS_PED_FACING_PED(mPoppy.piPed,PLAYER_PED_ID(),40)
						IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
	
	#IF NOT IS_JAPANESE_BUILD	
		IF NOT HAS_SOUND_FINISHED(iSexSound)	
			STOP_SOUND(iSexSound)
			SK_PRINT("TK************ STOP SEX SOUNDS ************")
		ENDIF
	#ENDIF
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF DOES_CAM_EXIST(camMain)
		fCamFov = GET_CAM_FOV(camMain)
	ENDIF
	IF eState <> SS_CLEANUP
		MANAGE_CAMERA(TRUE)
	ENDIF
	
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_FILM_POPPY, bJumpSkip)
				DISABLE_SELECTOR()
		
				IF DOES_CAM_EXIST(camMain)
					STOP_CAM_POINTING(camMain)
				ENDIF
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),TRUE)
				
				#IF NOT IS_JAPANESE_BUILD	
					STOP_SOUND(iGruntSounds)
				#ENDIF
				
				RELEASE_SCRIPT_AUDIO_BANK()
				bDumpedBank = TRUE
				
				RELEASE_HOTEL_PEDS()
				
				iTimerStartStage = GET_GAME_TIMER()
				iTimerStartedFilming = GET_GAME_TIMER()
			
				#IF NOT IS_JAPANESE_BUILD		
					fFaceOffset = -0.05
					fFaceOffsetz = -0.03
					iBadFilmNoBev = 0
					iSeqPoppySubs = 0
				#ENDIF
				#IF IS_JAPANESE_BUILD
					fFaceOffset = 0
					fFaceOffsetz = 0
				#ENDIF
				
				SK_PRINT("TK************ INIT FILM POPPY ************TK")
				
				iCounterHelp = 0
				
				IF IS_PED_UNINJURED(mBuddy.piPed)
					SAFE_TELEPORT_ENTITY(mBuddy.piPed,<<-37.2184, 325.3970, 111.6958>>, 242.3437)
				ENDIF
				
				#IF NOT IS_JAPANESE_BUILD
					bDone1stPersonFlash = FALSE
				#ENDIF
				
				eState = SS_ACTIVE
			
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			
			#IF NOT IS_JAPANESE_BUILD	
				IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)	
					
					IF IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						
						IF iSeqPoppySubs = 0
							IF fSSPhase > 0.029383
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)   //Is that good, babe? //0.029383
								PRINT_NOW("PAP2_SEX1", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 1
							IF fSSPhase > 0.051792 //0.052274
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)  //Ooh, ooh, it's fantastic. //0.052274
								PRINT_NOW("PAP2_SEX2", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 2
							IF fSSPhase > 0.095256 //0.097741
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)  //Really? //0.097741
								PRINT_NOW("PAP2_SEX3", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 3
							IF fSSPhase > 0.111642
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)  //Yes... ooh, you're so big. //0.111642
								PRINT_NOW("PAP2_SEX4", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 4
							IF fSSPhase > 0.160527 //0.168825
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)  //I know... I must say, for a virgin, you're not really that into this. //0.168825
								PRINT_NOW("PAP2_SEX5", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 5
							IF fSSPhase > 0.250271
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //I'm only a virgin for TV. //0.250271
								PRINT_NOW("PAP2_SEX6", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 6
							IF fSSPhase > 0.296955 //0.302319
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE)  //Really? //0.302319
								PRINT_NOW("PAP2_SEX7", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 7
							IF fSSPhase > 0.320319
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //I mean, I always wash afterward and jump up and down so it doesn't count. //0.302319
								PRINT_NOW("PAP2_SEX8", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 8
							IF fSSPhase > 0.376536
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //What? //0.376536
								PRINT_NOW("PAP2_SEX9", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 9
							IF fSSPhase > 0.397816
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //I'm not releasing a sex tape for at least three years, unless my agent tells me to. //0.397816
								PRINT_NOW("PAP2_SEX10", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 10
							IF fSSPhase > 0.477816
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //What? I thought this... I love you Poppy. //0.477816
								PRINT_NOW("PAP2_SEX11", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 11
							IF fSSPhase > 0.549232
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //I really want us to go places... I thought we could be the cutest teen couple. //0.549232
								PRINT_NOW("PAP2_SEX12", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 12
							IF fSSPhase > 0.608102
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //But Justin, you're 21 and I'm 24. //0.608102
								PRINT_NOW("PAP2_SEX13", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 13
							IF fSSPhase > 0.650271 //0.641114
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //We only say we're teens for marketing purposes. //0.641114
								PRINT_NOW("PAP2_SEX14", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 14
							IF fSSPhase > 0.685452
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //That's not true. //0.685452
								PRINT_NOW("PAP2_SEX15", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 15
							IF fSSPhase > 0.707621
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //Okay... just get on with it will you? I'm hungry. //0.707621
								PRINT_NOW("PAP2_SEX16", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ELIF iSeqPoppySubs = 16
							IF fSSPhase > 0.860467
								FORCE_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS_LIST(PREVIOUS_BRIEF_FORCE_DIALOGUE) //There's someone watching! //0.860467
								PRINT_NOW("PAP2_SEX17", DEFAULT_GOD_TEXT_TIME,0)
								++iSeqPoppySubs
							ENDIF
						ENDIF
			
					ENDIF
					
					IF fSSPhase > 0.6
						SETUP_MISSION_STAGE(MS_GET_IN_ESCAPE_CAR, FALSE)
					ENDIF
					
				ENDIF
			
				//SUBS
				//Is that good, babe? //0.029383
				//Ooh, ooh, it's fantastic. //0.052274
				//Really? //0.097741
				//Yes... ooh, you're so big. //0.111642
				//I know... I must say, for a virgin, you're not really that into this. //0.168825
				//I'm only a virgin for TV. //0.250271
				//Really? //0.302319
				//I mean, I always wash afterward and jump up and down so it doesn't count. //0.302319
				//What? //0.376536
				//I'm not releasing a sex tape for at least three years, unless my agent tells me to. //0.397816
				//What? I thought this... I love you Poppy. //0.477816
				//I really want us to go places... I thought we could be the cutest teen couple. //0.549232
				//But Justin, you're 21 and I'm 24. //0.608102
				//We only say we're teens for marketing purposes. //0.641114
				//That's not true. //0.685452
				//Okay... just get on with it will you? I'm hungry. //0.707621
				//There's someone watching! //0.860467
				//Creepy! Creepy! 
				//They've got a camera! //
				//You're fucking dead motherfuckers! //
			
			#ENDIF
			
			IF NOT bPlayerFailed
				IF IS_PLAYER_AT_ENTITY(mPoppy.piPed, <<5,5,1.5>>) 
				AND eMissionStage <> MS_FAILED
					MISSION_FAILED(FR_POPPY_SCARED)
				ENDIF
				
				IF GET_GAME_TIMER() > iTimerStartedFilming + 0
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
					ENDIF
				ENDIF
				
				IF iCounterHelp = 0
					PRINT_HELP("PAP2_HELP1")//Zoom in and out with.
					iTimerHelp = GET_GAME_TIMER()
					iCounterHelp = 1
				ELIF iCounterHelp = 1
					IF GET_GAME_TIMER() > iTimerHelp + 7000	
						PRINT_HELP("PAP2_HELP2")//Use to point the camera.
						iTimerHelp = GET_GAME_TIMER()
						iCounterHelp = 2
					ENDIF
				ELIF iCounterHelp = 2
					IF GET_GAME_TIMER() > iTimerHelp + 7000	
						IF mBoxCol.G <> 255  //If mBoxCol.G is 255 they already did it
							PRINT_HELP("PAP2_HELP3")//Zoom in on Poppy's face until the facial recognition box turns green.
							iCounterHelp = 3
						ELSE
							iCounterHelp = 3
						ENDIF
					ENDIF
				ENDIF

				IF IS_PED_UNINJURED(mBuddy.piPed)
					DO_BEV_COVER_ANIMS()
					IF NOT IS_PED_IN_COVER(mBuddy.piPed)	
						IF NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_a")
						AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_b")
						AND NOT IS_ENTITY_PLAYING_ANIM(mBuddy.piPed,"rcmpaparazzo_2","idle_l_corner_c")	
							IF GET_SCRIPT_TASK_STATUS(mBuddy.piPed, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER) <> PERFORMING_TASK
								SET_PED_TO_LOAD_COVER(mBuddy.piPed,TRUE)
								IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(<<-37.2396, 325.3983, 111.6961>>)	//For B*1819613 - making sure Bev gets into cover
									m_ciPap2 = ADD_COVER_POINT(<<-37.2396, 325.3983, 111.6961>>, 156.6960,COVUSE_WALLTOLEFT,COVHEIGHT_TOOHIGH,COVARC_90)
								ENDIF
								TASK_PUT_PED_DIRECTLY_INTO_COVER(mBuddy.piPed,<<-37.2022, 325.4253, 111.6954>>,-1,TRUE,0,TRUE,TRUE,m_ciPap2)
								SET_PED_CAN_PEEK_IN_COVER(mBuddy.piPed,TRUE)
								SET_PED_COMBAT_ATTRIBUTES(mBuddy.piPed,CA_CAN_USE_PEEKING_VARIATIONS,TRUE) 
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_IN_COVER(PLAYER_PED_ID())
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-32.757969,319.505890,111.495361>>, <<-33.262951,318.437958,113.495361>>, 1.500000)	
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(32.2338)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.3076)
					ENDIF
				ENDIF
				
				IF NOT bPictureTaken
					BOOL bOnScreen, bZoomed
					MONITER_PED_ON_SCREEN(mPoppy.piPed,bOnScreen, FALSE)
					IF bOnScreen
						MONITER_FOR_NO_ZOOM(bZoomed)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)	
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2) > 0.85  //0.82
							POPPY_SHAGGER_CHASE_PLAYER()
							bPictureTaken = TRUE
						ENDIF
						IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)	
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2) > 0.82
							fFaceOffset = fFaceOffset + 0.00067
							IF fFaceOffset > 0
								fFaceOffset = 0
							ENDIF
							fFaceOffsetZ = fFaceOffsetZ + 0.00067
							IF fFaceOffsetZ > 0
								fFaceOffsetZ = 0
							ENDIF
						ENDIF
						IF bZoomed
							UPDATE_FILMING_TIME()
						ENDIF
					ELSE
						MONITER_FOR_BAD_FILMING()
					ENDIF
				ELSE
					IF IS_PED_UNINJURED(mBuddy.piPed)
						ALTER_STEALTH(mBuddy.piPed, FALSE)
					ENDIF
			
					IF GET_GAME_TIMER() > iTimerPoppyNoticePlayer + 8000 //7500	
					ENDIF	
					
					BOOL bOnScreen
					MONITER_PED_ON_SCREEN(mPoppy.piPed,bOnScreen, FALSE)
					
					fFaceOffset = fFaceOffset + 0.00067
					IF fFaceOffset > 0
						fFaceOffset = 0
					ENDIF
					
					fFaceOffsetZ = fFaceOffsetZ + 0.00067
					IF fFaceOffsetZ > 0
						fFaceOffsetZ = 0
					ENDIF
					
					IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE()	> 0
						IF DOES_ENTITY_EXIST(ObjPhone)
							IF IS_ENTITY_ATTACHED_TO_ANY_PED(ObjPhone)
								DETACH_ENTITY(ObjPhone)
							ENDIF
						ENDIF
					ENDIF

				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					MISSION_FAILED(FR_BADCAM)
				ENDIF
			ENDIF
		
		BREAK
		
		CASE SS_CLEANUP
			IF IS_PED_UNINJURED(mPoppy.piPed)
				SET_PED_COMPONENT_VARIATION(mPoppy.piPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			ENDIF
			IF IS_PED_UNINJURED(mShagger.piPed)	
				SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(mShagger.piPed, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			TASK_PLAY_ANIM(PLAYER_PED_ID(),"rcmpaparazzo_2","idle_d",INSTANT_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT,0.739)
			CLEANUP_CAMERA(TRUE)
			CLEAR_PRINTS()
			//SET_GAMEPLAY_CAM_RELATIVE_HEADING(32.2338)
			//SET_GAMEPLAY_CAM_RELATIVE_PITCH(-13.3076)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(10.7485)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-9.8457)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bLineSet = FALSE
			CLEAR_HELP(TRUE)

			IF DOES_ENTITY_EXIST(ObjPhone)
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(ObjPhone)
					DETACH_ENTITY(ObjPhone)
				ENDIF
			ENDIF
			
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			eState = SS_CLEANUP
			KILL_ANY_CONVERSATION()
			POPPY_SHAGGER_CHASE_PLAYER()
			bPictureTaken = TRUE
		BREAK
	ENDSWITCH
ENDPROC 

/// PURPOSE:
///    Player and beverly run away from poppy and get in to bev's car
PROC GET_IN_ESCAPE_CAR()
	
	IF bPlayerGettingInCar = TRUE
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		THEFEED_HIDE_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	
	IF NOT IS_SPHERE_VISIBLE(<<-75.6080, 300.6798, 105.5206>>,30)	
		CLEAR_AREA_OF_PEDS(<<-75.6080, 300.6798, 105.5206>>,30)
	ENDIF
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(prop_ss1_14_garage_door,<<-62.22, 352.75, 113.01>>,TRUE)
	
	UPDATE_AI_PED_BLIP(mPoppy.piPed,bsPoppy,-1,NULL,TRUE)
	UPDATE_AI_PED_BLIP(mShagger.piPed,bsJustin)
	UPDATE_AI_PED_BLIP(mBodyGuard.piPed,bsSecurity)
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.3)

	FREEZE_ENTITY_POSITION(mBuddy.piPed,FALSE)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF bClearCoverTasks = FALSE
		IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_LR)	
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_MOVE_UD)	
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			bClearCoverTasks = TRUE
		ENDIF
	ENDIF
	
	IF bPlayerGettingInCar = FALSE	
		DO_PLAYER_HOLDING_CAMERA()
	ENDIF
	
	IF bPlayerGettingInCar = FALSE	
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_COVER)
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		ENDIF
	ELSE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_COVER)
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
		ENDIF
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(iZoomSound)
		STOP_SOUND(iZoomSound)
	ENDIF
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPoppyGroup, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()))	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()), relPoppyGroup)
	
	CLEAR_AREA_OF_PEDS(<< -47.8788, 353.6657, 112.0601 >>, 20.0)
	
	IF DOES_ENTITY_EXIST(ObjPhone)
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(ObjPhone)
			DETACH_ENTITY(ObjPhone)
		ENDIF
	ENDIF
	
	SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_DontRaiseFistsWhenLockedOn,TRUE) 
	
	
	IF IS_ENTITY_ALIVE(viPlayerCar)	
		REQUEST_VEHICLE_HIGH_DETAIL_MODEL(viPlayerCar)
		/*
		SET_ENTITY_LOD_DIST(viPlayerCar,200)
		SET_VEHICLE_LOD_MULTIPLIER(viPlayerCar,2.0)
		IF NOT IS_VEHICLE_HIGH_DETAIL(viPlayerCar)
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(viPlayerCar)
		ENDIF
		*/
	ENDIF
	
	//INT iRand2
	
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_GET_IN_ESCAPE_CAR, bJumpSkip)
				IF IS_PED_UNINJURED(mBuddy.piPed)		
					IF PLAY_PED_RECORDING_ON_PED(mBuddy.piPed, sBeverlyPedRecording)
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(mBuddy.piPed)
							SET_PED_TO_LOAD_COVER(mBuddy.piPed,FALSE)
							SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE,"DEFAULT_ACTION")
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_GET_TO_CAR, "GET TO THE CAR") // Running to escape car, set checkpoint
							REQUEST_VEHICLE_ASSET(FQ2)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),FALSE)
							DISPLAY_HUD(TRUE)
							DISPLAY_RADAR(TRUE)
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed)
							SAFE_RELEASE_OBJECT(objShagTable)
							SAFE_RELEASE_OBJECT(objShagParasol)
							SAFE_RELEASE_OBJECT(ObjPhone)
							ENABLE_SELECTOR()
							TASK_CLEAR_LOOK_AT(mBuddy.piPed)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mBuddy.piPed,-1)
							iTimerGetInCarStage = GET_GAME_TIMER()
							iCounterBevWaiting = 0
							bPoppyGettingInCar = FALSE
							bPoppyCombatTask = FALSE
							bShaggerCombatTask = FALSE
							bBevDrivingOff = FALSE
							iBadFilming = 0
							iSeqBevRagdoll = 0
							iPoppyAnimSeq = 0	
							CLEAR_AREA_OF_VEHICLES(<<-64.1056, 325.3524, 109.3568>>, 100)
							SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),TRUE,-1)
							//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_DisableMelee,TRUE)
							REPLAY_RECORD_BACK_FOR_TIME(8.0, 5, REPLAY_IMPORTANCE_LOWEST) //Poppy notices Franklin filming her having sex with Justin.
							eState = SS_ACTIVE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			
			/*
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_close"), TRUE) 
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_far"), TRUE) 
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_low_kick_far_quad"), TRUE) 
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_ground_close"), TRUE) 
			ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_ground_far"), TRUE) 
			*/
			/*
			IF GET_GAME_TIMER() > iTimerGetInCarStage + 2000	
				IF IS_PED_UNINJURED(mBodyGuard.piPed)
					//STOP_PED_SPEAKING(mBodyGuard.piPed,FALSE)
					IF NOT IS_ANY_SPEECH_PLAYING(mBodyGuard.piPed)
					AND IS_PED_IN_MELEE_COMBAT(mBodyGuard.piPed)	
						iRand2 = GET_RANDOM_INT_IN_RANGE(0,300)
						IF iRand2 = 0	
							PLAY_PED_AMBIENT_SPEECH(mBodyGuard.piPed, "FIGHT")
						ELIF iRand2 = 1	
							//PLAY_PED_AMBIENT_SPEECH(mBodyGuard.piPed, "GENERIC_INSULT_HIGH")
						ELIF iRand2 = 2	
							//PLAY_PED_AMBIENT_SPEECH(mBodyGuard.piPed, "PROVOKE_GENERIC")
						ELIF iRand2 = 3	
							//PLAY_PED_AMBIENT_SPEECH(mBodyGuard.piPed, "CHALLENGE_THREATEN")
						ELSE
						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
			//IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"random@escape_paparazzi@standing@","idle_d")
			//	STOP_ANIM_TASK(PLAYER_PED_ID(),"random@escape_paparazzi@standing@","idle_d",-2)
			//ENDIF
			
			IF IS_VEHICLE_OK(viChaseCars[POPPY_CAR])	
				IF NOT IS_ENTITY_AT_ENTITY(mPoppy.piPed, viChaseCars[POPPY_CAR], <<5,5,2.5>>)
					IF bPoppyCombatTask = FALSE
						IF IS_PED_UNINJURED(mPoppy.piPed)	
							IF IS_ENTITY_IN_RANGE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),2.0)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
								OR IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)	//To fix B*1753125 - clear Poppy's tasks if synced scene still running.
									CLEAR_PED_TASKS(mPoppy.piPed)
								ENDIF
								SET_ENTITY_COLLISION(mPoppy.piPed,TRUE)
							ENDIF
							IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
								IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2) >= 0.99//0.989654
									bPoppyCombatTask = TRUE
								ENDIF
							ENDIF
							IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)
								IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene_breakout) >= 0.99//0.989654
									bPoppyCombatTask = TRUE
								ENDIF
							ENDIF
	
							//0.960587 - justin come out
							//0.989654 - poppy come out
								
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
							AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)
								TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
								bShaggerCombatTask = TRUE
								bPoppyCombatTask = TRUE
							ENDIF	
						ENDIF
					ELSE
						IF NOT bPoppyGettingInCar	
							IF IS_PED_UNINJURED(mPoppy.piPed)		
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),20)
									IF GET_SCRIPT_TASK_STATUS(mPoppy.piPed,SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
										IF IS_ENTITY_PLAYING_ANIM(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting")
											STOP_ANIM_TASK(mPoppy.piPed,"RANDOM@CAR_THIEF@waiting_ig_4","waiting",SLOW_BLEND_OUT)
										ENDIF
										TASK_GO_TO_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1,8)
									ENDIF
								ELSE
									IF NOT IS_PED_HEADTRACKING_PED(mPoppy.piPed,PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1)
									ENDIF	
									IF NOT IS_PED_FACING_PED(mPoppy.piPed,PLAYER_PED_ID(),30)
										TASK_TURN_PED_TO_FACE_ENTITY(mPoppy.piPed,PLAYER_PED_ID())
									ELSE
										POPPY_ANGRY_ANIMS()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF bShaggerCombatTask = FALSE
				IF IS_PED_UNINJURED(mShagger.piPed)		
					IF IS_ENTITY_IN_RANGE_ENTITY(mShagger.piPed,PLAYER_PED_ID(),3.0)
						TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
						bShaggerCombatTask = TRUE
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
						IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene2) >= 0.97//0.960587
							TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
							bShaggerCombatTask = TRUE
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)
						IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_shagging_scene_breakout) >= 0.97//0.960587
							TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
							bShaggerCombatTask = TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
					AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene_breakout)
						TASK_COMBAT_PED(mShagger.piPed,PLAYER_PED_ID())
						bShaggerCombatTask = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bBudThreat
				DO_BEV_THRETENED_CONV()
			ELSE
				IF bBevDrivingOff = FALSE
					CHECK_PLAYER_AT_CAR_GET_IN()
				ENDIF
				CHECK_POPPY_NEAR_CAR()
				IF bShaggerCombatTask = TRUE
					CHECK_SHAGGER_NEAR_CAR()
				ENDIF
				CHECK_BODYGUARD_NEAR_CAR()
				CHECK_BUDDY_NEAR_CAR()
				IF ARE_PLAYER_AND_BUDDY_IN_CAR()
					SET_VEHICLE_DOORS_LOCKED(viPlayerCar, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
					bIsBuddyInCarBeforePlayer = FALSE
					IF DOES_ENTITY_EXIST(objCam)
						IF IS_ENTITY_ATTACHED(objCam)
							DETACH_ENTITY(objCam)
						ENDIF
						DELETE_OBJECT(objCam)
					ENDIF
					eState = SS_CLEANUP
				ENDIF
				CHECK_BUDDY_IN_CAR_BEFORE_PLAYER()
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-50.615166,329.184814,111.507881>>, <<-65.104324,297.998444,115.673424>>, 11.350000)
					IF bLegIt = FALSE
						IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ESCAPE", CONV_PRIORITY_MEDIUM)
							bLegIt = TRUE
						ENDIF
					ELSE
						IF bComeOn = FALSE
							IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_ESCAPEA", CONV_PRIORITY_MEDIUM)
								bComeOn = TRUE
								bEscapeConvExpired1 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bEscapeConvExpired = FALSE
				ENDIF
				IF iSeqBevRagdoll < 2	
					DO_ESCAPE_CHATTER_CONV()
				ENDIF
				IF NOT IS_PED_RAGDOLL(mBuddy.piPed)
				AND NOT IS_PED_GETTING_UP(mBuddy.piPed)	
				AND NOT bBevDrivingOff	
					IF iSeqBevRagdoll > 2
						DO_BEV_GET_IN_CAR_LINES()
					ENDIF
					IF iSeqBevRagdoll < 2
						DO_BEV_SHOUTING()
					ENDIF
				ENDIF	
			ENDIF
		
			IF bBevDrivingOff	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					MISSION_FAILED(FR_TOOK_TOO_LONG) 
				ENDIF
			ENDIF
		
			IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				MISSION_FAILED(FR_FOOTAGE_RUINED) 
			ENDIF
		
		BREAK
		
		CASE SS_CLEANUP
			
			IF IS_ENTITY_DEAD(mBodyGuard.piPed)
				bDeckedGuard = TRUE 
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mBodyGuard.piPed,PLAYER_PED_ID(),FALSE)
					bDeckedGuard = TRUE
				ENDIF
			ENDIF
			
			NEXT_STAGE()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
			REMOVE_WAYPOINT_RECORDING(sBeverlyPedRecording)

			REMOVE_ANIM_DICT("rcmpaparazzo_2")

			INT i
			FOR i=0 TO (MAX_SEX_SCENE_PROPS-1)
				SAFE_DELETE_OBJECT(mSexSceneProps[i].oiProp)
			ENDFOR
		BREAK

		CASE SS_SKIPPED
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			WARP_EVERYONE_TO_CARS_SKIP()
			
			TASK_CLEAR_LOOK_AT(mBuddy.piPed)
			
			IF SETUP_STAGE_REQUIREMENTS(RQ_CAMERA_CUTSCENE, <<-81.454941,296.807617,107.028061>>, -49.934269)
				IF DOES_CAM_EXIST(camMain)
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						iTimerPlayerInEscapeCar = GET_GAME_TIMER()
					ENDIF
				ENDIF		
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 3.0, REPLAY_IMPORTANCE_LOW) //getting into car
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC 

/// PURPOSE:
///    Player films poppy out the back of the car 
PROC CAR_CHASE()
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	/*
	IF IS_VEHICLE_OK(viPlayerCar)		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)	
			PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(viPlayerCar))		
			PRINTNL()
		ENDIF
	ENDIF
	*/
	//PRINTVECTOR(GET_CAM_ROT(camMain)) 
	//PRINTNL()
	
	TEXT_LABEL_23 label = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	IF ARE_STRINGS_EQUAL( label, "PAP2_CHASE1_3") 
		KILL_FACE_TO_FACE_CONVERSATION()
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_shagging_scene2)
		STOP_SYNCHRONIZED_AUDIO_EVENT(poppy_shagging_scene2)
	ENDIF
	//SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(1)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()

	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(),<<-60.2805, -493.5712, 39.2610>>,DEFAULT_CUTSCENE_LOAD_DIST)
		REQUEST_CUTSCENE("pap_2_mcs_1")
	ENDIF
	
	IF NOT bStartScriptCineCam
	AND IS_ENTITY_VISIBLE(PLAYER_PED_ID())	
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
	ENDIF
		
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_CAR_CHASE, bJumpSkip)
				CLEAR_PRINTS()
				STOP_CUTSCENE_IMMEDIATELY()
				IF IS_PED_UNINJURED(mBuddy.piPed)
					//RESET_PED_MOVEMENT_CLIPSET(mBuddy.piPed)
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(mBuddy.piPed)
				ENDIF
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(PLAYER_PED_ID())
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_02_INTRO")
					STOP_AUDIO_SCENE("PAPARAZZO_02_INTRO")
				ENDIF	
				IF DOES_ENTITY_EXIST(objCam)
					IF IS_ENTITY_ATTACHED(objCam)
						DETACH_ENTITY(objCam)
					ENDIF
					DELETE_OBJECT(objCam)
				ENDIF
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				SAFE_RELEASE_OBJECT(ObjPhone)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PAP_CAMERA_01)
				#IF NOT IS_JAPANESE_BUILD	
					SET_MODEL_AS_NO_LONGER_NEEDED(Prop_NPC_Phone)
				#ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_table_06)
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_parasol_03)
				//REMOVE_CLIP_SET("move_m@casual@f")
				REMOVE_CLIP_SET("move_f@sexy@a")
				REMOVE_CLIP_SET(sWeaponMoveClipset)
				REMOVE_CLIP_SET(sWeaponMoveClipsetFrank)
				REMOVE_ANIM_DICT("RANDOM@CAR_THIEF@waiting_ig_4")
				REMOVE_ANIM_DICT("cover@move@base@core")
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(),FALSE,-1)
				DISABLE_SELECTOR()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_CHASE, "CHASE",TRUE) // Chase started, set checkpoint
				IF IS_PED_UNINJURED(mPoppy.piPed)
					TASK_LOOK_AT_ENTITY(mPoppy.piPed,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
				ENDIF
				IF IS_VEHICLE_OK(viPlayerCar)
					SET_ENTITY_INVINCIBLE(viPlayerCar,TRUE)
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL(viPlayerCar)
				ENDIF
				REQUEST_ANIM_DICT("rcmpaparazzo_2ig_3")
				eScriptCamState = SCC_START
				bReachedChase = TRUE
				eState = SS_ACTIVE
				eChaseConvo = CC_STAGE_1
				CLEANUP_HOTEL_PEDS()
				iCurrentLine = 0
				fFaceOffset = 0
				fFaceOffsetz = 0
				iMaxRecogPercent = 0
				iSeqTraffic = 0
				iBadFilming = 0
				iNumFilmFails = 0
				iCounterNotOnScreen = 0
				fZOffest = 0.0
				fXOffest = 0.0
				fXOffestPoppy = 0.0
				iFailsafe = 0
				bLineSet = FALSE
				bCamInterp = FALSE
				bDoingEndCut = FALSE
				bStreamvolCreated = FALSE
				bSetCarVisible = FALSE
				bLockBootOpen = FALSE	
				bPoppyBigCrash = FALSE
				RESET_BOX_COL()
				SETUP_DYNAMIC_MIXING()
				bTrafficDontSwitchToAI = TRUE
				bSetPieceCarsDontSwitchToAI = TRUE
				bDStraight = FALSE
				bDTrunk = FALSE
				bDWorse = FALSE
				bDWhoa = FALSE
				bDMFer = FALSE
				bDRight = FALSE
				bDLeft = FALSE
				bDFuckU = FALSE
				bDMove = FALSE
				bDCrazy = FALSE
				bDTraffic = FALSE
				bDSteady = FALSE
				bDClose = FALSE
				bDJesus = FALSE
				bDConst = FALSE
				bDoneFirstPoppyCarOffset = FALSE	
				iShockBG = 0
				iShockPoppy = 0
				iShockBev = 0
				iShockJustin = 0
				bMovePlayerCar = FALSE
				//fCrashShockRange = 10.0
				MANAGE_UBER_RECORDING_FOR_CHASE()
				SET_PED_NON_CREATION_AREA(<<252.7712, -160.2005, 60.0327>>-<<100,100,50>>,<<252.7712, -160.2005, 60.0327>>+<<100,100,50>>)
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			
			/*
			PRINTINT(iBadFilming)
			PRINTNL()
	
			IF iBadFilming > 500
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iNumFilmFails = MAX_FILM_FAILS
				ENDIF
			ENDIF
			*/
			
			/*
			104057.679688
	
			104591.445313   //start slide
			104848.835938   //end slide
			*/
			
			IF IS_ENTITY_ALIVE(viPlayerCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
					IF IS_REPLAY_IN_PROGRESS()	
						IF NOT bMovePlayerCar
							IF fTimePosInRec < 8847.923828
								//SET_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG()
								//DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
								//bMovePlayerCar = TRUE
							ELSE
								//DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
								bMovePlayerCar = TRUE
							ENDIF
						ENDIF
					ENDIF
					/*
					IF bStartScriptCineCam
						PRINTFLOAT(fXOffestBev)
						PRINTNL()
						IF fTimePosInRec >= 104591.445313
						AND fTimePosInRec < 104848.835938
							IF fXOffestBev > -2.0
								fXOffestBev = fXOffestBev - 0.2
							ENDIF
							
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
								SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viPlayerCar,<<0,fXOffestBev,0>>)
							ENDIF

						ELSE
							IF fXOffestBev <> 0.0	
								IF fXOffestBev >= 104848.835938
									IF fXOffestBev < 0.0
										fXOffestBev = fXOffestBev + 0.1
									ENDIF
									IF fXOffestBev > 0.0
										fXOffestBev = 0.0
									ENDIF
					
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
										SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viPlayerCar,<<0,fXOffestBev,0>>)
									ENDIF

								ENDIF
							ENDIF
						ENDIF
					ENDIF
					*/
					fXOffestBev = fXOffestBev
					IF iShockBev = 0
						iShockBev = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DRIVING_ON_PAVEMENT,viPlayerCar)
					ENDIF
					IF fTimePosInRec > 10000	
					AND NOT bStartScriptCineCam
						pedTempBev = GET_RANDOM_PED_AT_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar,<<0,10,0>>),<<4,4,4>>,PEDTYPE_MISSION)
						IF IS_PED_UNINJURED(pedTempBev)
						AND GET_SCRIPT_TASK_STATUS(pedTempBev,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedTempBev,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK
							CLEAR_PED_TASKS(pedTempBev)
							SET_PED_KEEP_TASK(pedTempBev,TRUE)
							IF IS_ENTITY_ALIVE(mBuddy.piPed)
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									TASK_REACT_AND_FLEE_PED(pedTempBev,mBuddy.piPed)
									SK_PRINT("PED REACT AND FLEE FROM BEVERLY")
								ELSE
									TASK_SMART_FLEE_PED(pedTempBev,mBuddy.piPed,50,-1,TRUE)
									SK_PRINT("PED SMART FLEE FROM BEVERLY")
								ENDIF
							ENDIF
							SET_PED_AS_NO_LONGER_NEEDED(pedTempBev)
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])	
						IF iShockBG = 0
							iShockBG = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DRIVING_ON_PAVEMENT,viChaseCars[BODYGUARD_CAR])
						ENDIF
					ENDIF
					fTimePosInRec = GET_TIME_POSITION_IN_RECORDING(viPlayerCar)
					IF IS_VEHICLE_OK(viChaseCars[SHAGGER_CAR])	 	
						IF iShockJustin = 0
							iShockJustin = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DRIVING_ON_PAVEMENT,viChaseCars[SHAGGER_CAR])
						ENDIF
						pedTempJustin = GET_RANDOM_PED_AT_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viChaseCars[SHAGGER_CAR],<<0,10,0>>),<<4,4,4>>,PEDTYPE_MISSION)
						IF IS_PED_UNINJURED(pedTempJustin)
						AND GET_SCRIPT_TASK_STATUS(pedTempJustin,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedTempJustin,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK	
							CLEAR_PED_TASKS(pedTempJustin)
							SET_PED_KEEP_TASK(pedTempJustin,TRUE)
							IF IS_ENTITY_ALIVE(mShagger.piPed)
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									TASK_REACT_AND_FLEE_PED(pedTempJustin,mShagger.piPed)
									SK_PRINT("PED REACT AND FLEE FROM JUSTIN")
								ELSE
									TASK_SMART_FLEE_PED(pedTempJustin,mShagger.piPed,50,-1,TRUE)
									SK_PRINT("PED SMART FLEE FROM JUSTIN")
								ENDIF
							ENDIF
							SET_PED_AS_NO_LONGER_NEEDED(pedTempJustin)
						ENDIF
						IF bSmallCrash = FALSE	
							IF fTimePosInRec >= 28753.052734
								SET_VEHICLE_DAMAGE(viChaseCars[SHAGGER_CAR],<<431.10, 295.63, 102.80>>,150, 110, FALSE)
								IF NOT IS_ENTITY_DEAD(viCrashFuto)	
									SET_VEHICLE_DAMAGE(viCrashFuto,<<431.10, 295.63, 103.00>>,200, 200, FALSE)
									SMASH_VEHICLE_WINDOW(viCrashFuto,SC_WINDOW_FRONT_LEFT)
								ENDIF
								PLAY_SOUND_FROM_COORD(-1,"SMALL_CRASH",<<431.10, 295.63, 102.80>>,"PAPARAZZO_02_SOUNDSETS")
								bSmallCrash = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(viChaseCars[POPPY_CAR])		
						IF iShockPoppy = 0
							iShockPoppy = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DRIVING_ON_PAVEMENT,viChaseCars[POPPY_CAR])
						ENDIF
						IF bPoppySmallCrash = FALSE		 	
							IF fTimePosInRec >= 49422.613281
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_LOW)
								SET_VEHICLE_DAMAGE(viChaseCars[POPPY_CAR],<<643.32, 33.03, 86.0>>,200, 200, FALSE)
								bPoppySmallCrash = TRUE
							ENDIF
						ENDIF
						IF bPoppyBigCrash = FALSE
							IF HAS_SOUND_FINISHED(iSoundPoppyRevs)
								PLAY_SOUND_FROM_ENTITY(iSoundPoppyRevs, "LOOP_REV", viChaseCars[POPPY_CAR], "PAPARAZZO_02_SOUNDSETS")
							ENDIF
							IF iShockPoppy = 0
								iShockPoppy = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_DRIVING_ON_PAVEMENT,viChaseCars[POPPY_CAR])
							ENDIF
							pedTemp = GET_RANDOM_PED_AT_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viChaseCars[POPPY_CAR],<<0,10,0>>),<<4,4,4>>,PEDTYPE_MISSION)
							IF IS_PED_UNINJURED(pedTemp)
							AND GET_SCRIPT_TASK_STATUS(pedTemp,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedTemp,SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK	
								CLEAR_PED_TASKS(pedTemp)
								SET_PED_KEEP_TASK(pedTemp,TRUE)
								IF IS_ENTITY_ALIVE(mPoppy.piPed)
									IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
										TASK_REACT_AND_FLEE_PED(pedTemp,mPoppy.piPed)
										SK_PRINT("PED REACT AND FLEE FROM POPPY")
									ELSE
										TASK_SMART_FLEE_PED(pedTemp,mPoppy.piPed,50,-1,TRUE)
										SK_PRINT("PED SMART FLEE FROM POPPY")
									ENDIF
								ENDIF
								SET_PED_AS_NO_LONGER_NEEDED(pedTemp)
							ENDIF
							IF fTimePosInRec >= 92227.953125
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_LOWEST)
								SMASH_VEHICLE_WINDOW(viChaseCars[POPPY_CAR],SC_WINDSCREEN_FRONT)
								SET_VEHICLE_DAMAGE(viChaseCars[POPPY_CAR],<<99.92, -396.72, 40.74>>,200, 200, FALSE)
								SET_VEHICLE_TYRE_BURST(viChaseCars[POPPY_CAR],SC_WHEEL_CAR_FRONT_LEFT)
								SET_VEHICLE_TYRE_BURST(viChaseCars[POPPY_CAR],SC_WHEEL_CAR_FRONT_RIGHT)
								PLAY_SOUND_FROM_ENTITY(-1,"BUILDING_SITE_CRASH",viChaseCars[POPPY_CAR],"PAPARAZZO_02_SOUNDSETS")
								TRIGGER_MUSIC_EVENT("PAP2_STOP")	
								IF IS_PED_UNINJURED(pedBuilder[0])
									TASK_GO_TO_ENTITY(pedBuilder[0],viChaseCars[POPPY_CAR],-1,10)
								ENDIF
								IF IS_PED_UNINJURED(pedBuilder[1])
									TASK_GO_TO_ENTITY(pedBuilder[1],viChaseCars[POPPY_CAR],-1,5)
								ENDIF
								IF IS_PED_UNINJURED(pedBuilder[2])
									TASK_GO_TO_ENTITY(pedBuilder[2],viChaseCars[POPPY_CAR],-1,15)
								ENDIF
								IF IS_PED_UNINJURED(pedBuilder[3])
									TASK_GO_TO_ENTITY(pedBuilder[3],viChaseCars[POPPY_CAR],-1,10)
								ENDIF
								IF IS_PED_UNINJURED(pedBuilder[4])
									TASK_START_SCENARIO_IN_PLACE(pedBuilder[4],"WORLD_HUMAN_MOBILE_FILM_SHOCKING",-1,TRUE)
								ENDIF
								iShockPoppy = 0
								SAFE_RELEASE_PED(pedTemp)
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 0, REPLAY_IMPORTANCE_LOWEST) //Poppy crashes her car.
								bPoppyBigCrash = TRUE
							ENDIF
						ELSE
							IF NOT HAS_SOUND_FINISHED(iSoundPoppyRevs)
								STOP_SOUND(iSoundPoppyRevs)
							ENDIF
							IF iShockPoppyCrash = 0
								iShockPoppyCrash = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_CAR_CRASH,viChaseCars[POPPY_CAR])
							ENDIF
							/*
							fCrashShockRange += 20
							IF fCrashShockRange > 100
								fCrashShockRange = 100
							ENDIF
							pedTemp = GET_RANDOM_PED_AT_COORD(<<99.92, -396.72, 40.74>>,<<fCrashShockRange,fCrashShockRange,fCrashShockRange>>,PEDTYPE_MISSION)
							IF IS_PED_UNINJURED(pedTemp)
							AND GET_SCRIPT_TASK_STATUS(pedTemp,SCRIPT_TASK_GO_TO_ENTITY) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedTemp,SCRIPT_TASK_GO_TO_ENTITY) <> WAITING_TO_START_TASK
								CLEAR_PED_TASKS(pedTemp)
								SET_PED_KEEP_TASK(pedTemp,TRUE)
								TASK_GO_TO_ENTITY(pedTemp,viChaseCars[POPPY_CAR],-1,5)
								SK_PRINT("PED TASKED TO RUN TOWARDS POPPY")
								SET_PED_AS_NO_LONGER_NEEDED(pedTemp)
							ENDIF
							*/
						ENDIF
					ENDIF
					
					IF fTimePosInRec >= 40577.410156//41177.410156
					AND fTimePosInRec < 42025.582031 //42525.582031
						IF fXOffest < 2.5
							fXOffest = fXOffest + 0.1
						ENDIF
						IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])	
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viChaseCars[BODYGUARD_CAR])
								SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viChaseCars[BODYGUARD_CAR],<<fXOffest,0,0>>)
							ENDIF
						ENDIF
					ELSE
						IF fXOffest <> 0.0	
							IF fTimePosInRec >= 42025.582031//42525.582031
								IF fXOffest > 0.0
									fXOffest = fXOffest - 0.1
								ENDIF
								IF fXOffest < 0.0
									fXOffest = 0.0
								ENDIF
								IF IS_ENTITY_ALIVE(viChaseCars[BODYGUARD_CAR])		
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viChaseCars[BODYGUARD_CAR])
										SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viChaseCars[BODYGUARD_CAR],<<fXOffest,0,0>>)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bDoneFirstPoppyCarOffset	
						IF fTimePosInRec >= 39807.453125
						AND fTimePosInRec < 41084.421875
							IF fZOffest < 0.1
								fZOffest = fZOffest + 0.01
							ENDIF
							IF fXOffestPoppy > -0.3
								fXOffestPoppy = fXOffestPoppy - 0.01
							ENDIF
							SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viChaseCars[POPPY_CAR],<<fXOffestPoppy,0,fZOffest>>)
						ELSE
							IF fTimePosInRec >= 41084.421875
								bDoneFirstPoppyCarOffset = TRUE	
							ENDIF
						ENDIF
					ELSE	
						IF fTimePosInRec >= 45980.902344
						AND fTimePosInRec < 48093.191406
							IF fZOffest < 0.1
								fZOffest = fZOffest + 0.02
							ENDIF
							SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viChaseCars[POPPY_CAR],<<fXOffestPoppy,0,fZOffest>>)
						ELSE
							IF fZOffest <> 0.0	
								IF fZOffest > 0.0
									fZOffest = fZOffest - 0.02
								ENDIF
								IF fZOffest < 0.0
									fZOffest = 0.0
								ENDIF
							ENDIF
							IF fXOffestPoppy <> 0.0	
								IF fXOffestPoppy < 0.0
									fXOffestPoppy = fXOffestPoppy + 0.01
								ENDIF
								IF fXOffestPoppy > 0.0
									fXOffestPoppy = 0.0
								ENDIF
							ENDIF	
							IF fZOffest <> 0.0	
							OR fXOffestPoppy <> 0.0
								SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(viChaseCars[POPPY_CAR],<<fXOffestPoppy,0,fZOffest>>)	
							ENDIF
						ENDIF
					ENDIF
						
					IF fTimePosInRec >= 3472.197510
					AND fTimePosInRec < 94887.289063
						bUseFace = TRUE
					ELSE
						bUseFace = FALSE
					ENDIF
					//IF fTimePosInRec >= 16500.0
					//AND fTimePosInRec < 20020.544922
						//OVERRIDE_LODSCALE_THIS_FRAME(0.95)		
					//ENDIF
					IF IS_VEHICLE_OK(viChaseCars[POPPY_CAR])
						IF fTimePosInRec >= 48117.371094
						AND	fTimePosInRec < 50493.308594  
							ACTIVATE_PHYSICS(viChaseCars[POPPY_CAR])
						ENDIF
						IF fTimePosInRec >= 87079.421875
							ACTIVATE_PHYSICS(viChaseCars[POPPY_CAR])
						ENDIF
					ENDIF
					IF IS_VEHICLE_OK(viChaseCars[SHAGGER_CAR])
						IF fTimePosInRec >= 27450.808594
						AND	fTimePosInRec < 30024.781250  	
							IF IS_VEHICLE_OK(viCrashFuto)
								ACTIVATE_PHYSICS(viCrashFuto)
							ENDIF
							ACTIVATE_PHYSICS(viChaseCars[SHAGGER_CAR])
						ENDIF
						IF fTimePosInRec >= 87079.421875
							ACTIVATE_PHYSICS(viChaseCars[SHAGGER_CAR])
						ENDIF
						IF bHighRevSound = FALSE
							IF fTimePosInRec > 88060.507813
							AND	fTimePosInRec < 89076.585938
								PLAY_SOUND_FROM_ENTITY(-1,"HIGHREV",viChaseCars[SHAGGER_CAR],"PAPARAZZO_02_SOUNDSETS")
								SK_PRINT("HIGH REV SOUND")
								bHighRevSound = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF IS_VEHICLE_OK(viChaseCars[BODYGUARD_CAR])
						IF fTimePosInRec >= 52142.996094
						AND	fTimePosInRec < 56000.0
							ACTIVATE_PHYSICS(viChaseCars[BODYGUARD_CAR])
						ENDIF
						IF fTimePosInRec >= 64722.496094
						AND	fTimePosInRec < 67652.062500
							ACTIVATE_PHYSICS(viChaseCars[BODYGUARD_CAR])
						ENDIF
						IF fTimePosInRec >= 87079.421875
							ACTIVATE_PHYSICS(viChaseCars[BODYGUARD_CAR])
						ENDIF
					ENDIF
					IF fTimePosInRec >= 500
					AND iSeqTraffic <> 7	
						DO_SCRIPTED_TRAFFIC()
					ENDIF
					/*
					IF fTimePosInRec > 53000
					AND	fTimePosInRec < 68000
						bTrafficForceDefaultPedModel = TRUE
					ELSE
						bTrafficForceDefaultPedModel = FALSE
					ENDIF
					*/
					IF iSeqBadCamMissionFail = 0
						IF fTimePosInRec >= 91365.810
							bPlayerFailed = FALSE
							bDisableFail = TRUE
						ENDIF
					ENDIF
					IF fTimePosInRec > 0
						IF bOpenBoot = FALSE	
							SET_VEHICLE_DOOR_BROKEN(viPlayerCar,SC_DOOR_BOOT,TRUE)
							SET_ENTITY_VISIBLE(viPlayerCar,TRUE)
							//SET_CAR_BOOT_OPEN(viPlayerCar)
							//SET_VEHICLE_DOOR_OPEN(viPlayerCar,SC_DOOR_BOOT,FALSE,TRUE)
							//#IF IS_DEBUG_BUILD
							//	CPRINTLN(DEBUG_MISSION, "<><> SET CAR BOOT OPEN <><>")
							//#ENDIF
							bOpenBoot = TRUE
						ELSE
							IF bSetCarVisible = FALSE
								//IF GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_BOOT) >= 0.7
									//SET_ENTITY_VISIBLE(viPlayerCar,TRUE)
									//SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
									bSetCarVisible = TRUE
								//ENDIF
							ELSE	
								IF bLockBootOpen = FALSE	
									//IF GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_BOOT) >= 0.99
									//	SET_VEHICLE_DOOR_OPEN(viPlayerCar,SC_DOOR_BOOT,FALSE,TRUE)
									//	CPRINTLN(DEBUG_MISSION, "CAR BOOT LOCKED OPEN")
										bLockBootOpen = TRUE	
									//ENDIF
								ENDIF
								/*
								IF NOT bStartScriptCineCam	
									IF GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_BOOT) < 0.2
										SET_VEHICLE_DOOR_BROKEN(viPlayerCar,SC_DOOR_BOOT,FALSE)
										CPRINTLN(DEBUG_MISSION, "CAR BOOT SHUT ITSELF! KNOCKING IT OFF")
									ENDIF
								ENDIF
								*/
							ENDIF
						ENDIF
					ENDIF
					IF bPoppyCrashDialogue = FALSE	
						IF fTimePosInRec >=	91715.726563
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(s_conversation_peds, sTextBlock, "PAP2_PIPES", CONV_PRIORITY_HIGH) //Oh shit! Noooooooo!
									bPoppyCrashDialogue = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF bStreamvolCreated = FALSE
						IF fTimePosInRec >=	85000.0	
							//StreamVol = STREAMVOL_CREATE_FRUSTUM(<< -74.2735, -532.8775, 39.9423 >>,<< 2.1752, 0.0075, -32.3615 >>,200,FLAG_COLLISIONS_MOVER | FLAG_MAPDATA, STREAMVOL_LOD_FLAG_ALL)
							StreamVol = STREAMVOL_CREATE_FRUSTUM(<<-71.5438, -549.9070, 40.5579>>, <<2.4313, 1.2491, -20.7899>>,200,FLAG_COLLISIONS_MOVER | FLAG_MAPDATA, STREAMVOL_LOD_FLAG_ALL)
							//NEW_LOAD_SCENE_START(<< -74.2735, -532.8775, 39.9423 >>,<< 2.1752, 0.0075, -32.3615 >>,100)
							REQUEST_MODEL(prop_pipes_02b)
							REQUEST_MODEL(prop_cablespool_02)
							REQUEST_MODEL(prop_barrier_wat_04c)
							REQUEST_MODEL(prop_consign_01a)
							REQUEST_MODEL(prop_barrier_work06a)
							REQUEST_MODEL(prop_sign_road_03g)
							REQUEST_MODEL(prop_sign_road_03m)
							SET_PED_NON_CREATION_AREA(<<1.9758, -403.4291, 38.4052>>-<<30,30,10>>,<<1.9758, -403.4291, 38.4052>>+<<30,30,10>>)
							bStreamvolCreated = TRUE
						ENDIF
					ENDIF
					IF fTimePosInRec >=	80000.0	
						CREATE_BUILDER_PEDS()
					ENDIF
					/*
					IF fTimePosInRec < 90000.0	
						IF iSeqMusic <> 3
							IF TRIGGER_MUSIC_EVENT("PAP2_CAR")	
								iSeqMusic = 3
							ENDIF
						ENDIF
					ENDIF
					*/
					IF fTimePosInRec < 15000.0		
						IF IS_SCREEN_FADED_OUT()
					  	  IF NOT IS_SCREEN_FADING_IN()
					    		SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
							ENDIF
						ENDIF
					ENDIF
					CLEAR_PED_AREAS()
				
					IF IS_PED_UNINJURED(mPoppy.piPed)		
						SET_PED_LOD_MULTIPLIER(mPoppy.piPed,2.0)
						IF fTimePosInRec >= 47849.195313
						AND	fTimePosInRec < 49546.554688
							IF NOT IS_PED_DOING_DRIVEBY(mPoppy.piPed)
								TASK_DRIVE_BY(mPoppy.piPed,PLAYER_PED_ID(),NULL,<<0,0,0>>,7,100)
							ENDIF
						ENDIF
					ENDIF
					IF bSkipToEndOfChase = TRUE
						IF GET_TIME_POSITION_IN_RECORDING(viPlayerCar) < 80000
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viPlayerCar,80000) //!!!!!!!!!!!! USE TO SKIP
						ENDIF
					ENDIF
				ENDIF
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_REAR_LEFT) > 0
					SET_VEHICLE_DOOR_CONTROL(viPlayerCar,SC_DOOR_REAR_LEFT,DT_DOOR_INTACT,GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_REAR_LEFT) - 0.08)
				ENDIF
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_REAR_RIGHT) > 0
					SET_VEHICLE_DOOR_CONTROL(viPlayerCar,SC_DOOR_REAR_RIGHT,DT_DOOR_INTACT,GET_VEHICLE_DOOR_ANGLE_RATIO(viPlayerCar,SC_DOOR_REAR_RIGHT) - 0.08)
				ENDIF
			ENDIF
			IF NOT bPlayerFailed
				IF NOT bMiniCutDone
					//IF DO_MINI_CAR_CUTSCENE()
						bMiniCutDone = TRUE
					//ENDIF
				ELSE
					//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_BADCAM")
						IF bStartChaseCam
							IF DOES_CAM_EXIST(camMain)
								STOP_CAM_POINTING(camMain)
							ENDIF
							IF NOT bStartScriptCineCam
								IF DOES_CAM_EXIST(camInterp)
									DESTROY_CAM(camInterp)
								ENDIF
								IF DOES_CAM_EXIST(camMain)
									fCamFov = GET_CAM_FOV(camMain)
								ENDIF
								IF NOT bDoingEndCut
									MANAGE_CAMERA()
								ENDIF
							ENDIF
						ELSE
							IF bOpenBoot = TRUE			
								IF bStartChaseCam = FALSE	
									IF SETUP_STAGE_REQUIREMENTS(RQ_CAMERA, vSafeVec)
										IF DOES_CAM_EXIST(camMain)
											//POINT_CAM_AT_COORD(CamMain,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerCar,<<0,-5,0.6>>))
											SHAKE_CAM(camMain, "HAND_SHAKE", 0.1)
											RENDER_SCRIPT_CAMS(TRUE, FALSE)
										ENDIF
										MANAGE_CAMERA()
										SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
										bStartChaseCam = TRUE
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP2_05")
							MANAGE_CHASE_CONVO()
							IF bStartChaseCam
								IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
										IF GET_TIME_POSITION_IN_RECORDING(viPlayerCar) <= 86982.210938 //95365.810 //96333.600
											IF GET_TIME_POSITION_IN_RECORDING(viPlayerCar) >= 1000
												BOOL bOnScreen
												MONITER_PED_ON_SCREEN(mPoppy.piPed, bOnScreen)
												IF NOT bOnScreen									
													IF NOT bDisableFail
														MONITER_FOR_BAD_FILMING_CHASE()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//ENDIF
						DO_SCRIPTED_CINE_CAM()
					//ENDIF
				ENDIF
			ELSE
				IF DOES_CAM_EXIST(camMain)
					fCamFov = GET_CAM_FOV(camMain)
				ENDIF
				MANAGE_CAMERA()
				IF PULLOVER_PLAYER_FAILED()
					MISSION_FAILED(FR_BADCAM)
				ENDIF
			ENDIF
			HANDLE_POPPY_CHASE_DIALOGUE()
			MANAGE_UBER_RECORDING_FOR_CHASE()
		BREAK
		
		CASE SS_CLEANUP
			NEXT_STAGE()
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
			SAFE_REMOVE_BLIP(biGOTO)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		BREAK
		
		CASE SS_SKIPPED
			#IF IS_DEBUG_BUILD
				iCurrentChasePos = MAX_CHASE_POINTS - 1 
			#ENDIF
			IF IS_SAFE_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayerCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(viPlayerCar)
					SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(viPlayerCar)
					IF IS_VEHICLE_SEAT_FREE(viPlayerCar, VS_BACK_RIGHT)
						TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(PLAYER_PED_ID(), viPlayerCar)
					ENDIF
					REQUEST_CUTSCENE("pap_2_mcs_1")		
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC 

/// PURPOSE:
///    Player and bev exchange brief words then player gets out of the car 
///    and bev drives off
PROC OUTRO_NEW()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	SET_USE_HI_DOF()
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		
	ENDIF	
	
	SWITCH eCutsceneState
		
		CASE eCutInit

			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "**** Init Outro ****")
			#ENDIF

			IF SETUP_MISSION_STAGE(MS_OUTRO, bJumpSkip)
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				
					bCSSetExitCam = FALSE
					bCSSetExitBev = FALSE
					bCSSetExitCar = FALSE
					bCSSetExitFranklin = FALSE
					
					IF IS_PED_UNINJURED(mBuddy.piPed)
						REGISTER_ENTITY_FOR_CUTSCENE(mBuddy.piPed,"Beverly",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF

					IF IS_VEHICLE_OK(viPlayerCar)	
						REGISTER_ENTITY_FOR_CUTSCENE(viPlayerCar,"Beverlys_Car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY,FQ2)
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_LOWEST)
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
					
					START_CUTSCENE()
					
					WAIT(0)
					
					IF bStreamvolCreated = TRUE
						STREAMVOL_DELETE(StreamVol)
					ENDIF
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
					
					eCutsceneState = eCutUpdate
					
				ENDIF
			ENDIF
	
		BREAK	
	
		CASE eCutUpdate
			
			IF bCSSetExitFranklin = FALSE
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")		
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					bCSSetExitFranklin = TRUE
				ENDIF
			ENDIF
			
			IF bCSSetExitCam = FALSE
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					REPLAY_STOP_EVENT()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					bCSSetExitCam = TRUE
				ENDIF
			ENDIF
			
			IF bCSSetExitCar = FALSE
			AND bCSSetExitBev = FALSE
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverly")	
				AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Beverlys_Car")		
					IF NOT IS_SAFE_PED_IN_VEHICLE(mBuddy.piPed, viPlayerCar)
						SET_PED_INTO_VEHICLE(mBuddy.piPed, viPlayerCar)
					ENDIF	
					FORCE_PED_AI_AND_ANIMATION_UPDATE(mBuddy.piPed)
					SET_DISABLE_PRETEND_OCCUPANTS(viPlayerCar,TRUE)
					SET_VEHICLE_FIXED(viPlayerCar)
					SET_VEHICLE_DOOR_LATCHED(viPlayerCar,SC_DOOR_REAR_RIGHT,TRUE,TRUE)
					OPEN_SEQUENCE_TASK(SeqMain)
						TASK_VEHICLE_DRIVE_WANDER(NULL, viPlayerCar,100, DRIVINGMODE_AVOIDCARS_RECKLESS)
					CLOSE_SEQUENCE_TASK(SeqMain)
					SET_PED_KEEP_TASK(mBuddy.piPed, TRUE)
					TASK_PERFORM_SEQUENCE(mBuddy.piPed, SeqMain)
					CLEAR_SEQUENCE_TASK(SeqMain)
					bCSSetExitCar = TRUE
					bCSSetExitBev = TRUE
				ENDIF
			ENDIF

			IF bCSSetExitCar = TRUE
			AND bCSSetExitBev = TRUE
			AND bCSSetExitCam = TRUE
			AND bCSSetExitFranklin = TRUE	
				eCutsceneState = eCutCleanup
			ELSE	
				
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)	
						STOP_CUTSCENE_IMMEDIATELY()

					ENDIF
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)	
						STOP_CUTSCENE_IMMEDIATELY()
						JUMP_TO_STAGE(MS_CAR_CHASE)
					ENDIF
				#ENDIF
				
				IF DOES_CAM_EXIST(camMain)
					DESTROY_CAM(camMain)
				ENDIF
				
				IF DOES_CAM_EXIST(camInterp)
					DESTROY_CAM(camInterp)
				ENDIF
				
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
				
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			
			ENDIF
			
		BREAK
		
		CASE eCutCleanup
			
			eMissionStage = MS_PASSED
			
			RC_END_CUTSCENE_MODE()
		
		BREAK
	
	ENDSWITCH
	
ENDPROC

//PURPOSE: RECORD UBER CAR REC
PROC RECORD_UBER_CAR()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_UBER_RECORD, bJumpSkip)
				eState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			#IF IS_DEBUG_BUILD
				IF iRecordingProgress > 0
		  			UPDATE_UBER_RECORDING()
				ENDIF
		  	#ENDIF	

		BREAK
		
		CASE SS_CLEANUP
			
		BREAK
		
		CASE SS_SKIPPED
			eMissionStage = MS_PASSED
		BREAK
	ENDSWITCH
ENDPROC

PROC POPPY_AND_UBA()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_POPPY_AND_UBA, bJumpSkip)
				eState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			MANAGE_UBER_RECORDING_FOR_CHASE()
		BREAK
		
		CASE SS_CLEANUP
		
		BREAK
		
		CASE SS_SKIPPED
			bObjectiveShown = FALSE
			eMissionStage = MS_PASSED
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PIECE_RECORDING()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(MS_SET_PIECE_RECORDING, bJumpSkip)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				eState = SS_ACTIVE
			ENDIF
		BREAK
		
		CASE SS_ACTIVE
			MANAGE_UBER_RECORDING_FOR_CHASE()
		BREAK
		
		CASE SS_CLEANUP
			
		BREAK
		
		CASE SS_SKIPPED
			bObjectiveShown = FALSE
			eMissionStage = MS_PASSED
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks to make sure the player hasn't killed any of the peds in the mission
PROC MONITER_PLAYER_KILLED()
	SWITCH eMissionStage
		CASE MS_FOLLOW_BEV_ROAD
		FALLTHRU
		CASE MS_FOLLOW_BEV_GUARDS
		FALLTHRU
		CASE MS_FOLLOW_TO_POPPY
		FALLTHRU
		CASE MS_FILM_POPPY
		FALLTHRU
		CASE MS_GET_IN_ESCAPE_CAR
			IF SAFE_MONITER_PED(mBuddy.piPed)
				IF IS_ENTITY_DEAD(mBuddy.piPed)
					MISSION_FAILED(FR_BUDDY_DEAD)
				ELSE
					GIVE_FLEE_ORDER(mBuddy.piPed)
					MISSION_FAILED(FR_BUDDY_HARMED)
				ENDIF
			ENDIF

			IF SAFE_MONITER_PED(mPoppy.piPed) 
				IF  IS_ENTITY_DEAD(mPoppy.piPed)
					MISSION_FAILED(FR_KILLED_POP)
					
					IF IS_PED_UNINJURED(mShagger.piPed)
						GIVE_FLEE_ORDER(mShagger.piPed)
					ENDIF
				ELSE
					IF IS_PED_UNINJURED(mPoppy.piPed)
						GIVE_FLEE_ORDER(mPoppy.piPed)
					ENDIF
					
					IF IS_PED_UNINJURED(mShagger.piPed)
						GIVE_FLEE_ORDER(mShagger.piPed)
					ENDIF
					MISSION_FAILED(FR_HARMED_POP)
				ENDIF
			ENDIF
			
			IF SAFE_MONITER_PED(mShagger.piPed)
				IF  IS_ENTITY_DEAD(mShagger.piPed)
					MISSION_FAILED(FR_KILLED_SHAG)
					IF IS_PED_UNINJURED(mShagger.piPed)
						GIVE_FLEE_ORDER(mShagger.piPed)
					ENDIF
				ENDIF
			ENDIF

			IF SAFE_MONITER_CAR(viPlayerCar)
				MISSION_FAILED(FR_CAR_DEAD)
			ENDIF

			IF SAFE_MONITER_CAR(viChaseCars[POPPY_CAR])
			OR SAFE_MONITER_CAR(viChaseCars[SHAGGER_CAR])
			OR SAFE_MONITER_CAR(viChaseCars[BODYGUARD_CAR])
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				MISSION_FAILED(FR_WANTED)
			ENDIF	

			FOR iCount = 0 TO (MAX_STAFF - 1)
				IF SAFE_MONITER_PED(mHotelStaff[iCount].piPed)
					MISSION_FAILED(FR_KILLED_INOCC)
					GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
					GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
					BREAK
				ENDIF
			ENDFOR
		BREAK
		
		CASE MS_CAR_CHASE
			IF SAFE_MONITER_PED(mPoppy.piPed) 
				IF  IS_ENTITY_DEAD(mPoppy.piPed)
					MISSION_FAILED(FR_KILLED_POP)
				ELSE
					MISSION_FAILED(FR_HARMED_POP)
				ENDIF
			ENDIF
			
			IF SAFE_MONITER_PED(mShagger.piPed)
				IF  IS_ENTITY_DEAD(mShagger.piPed)
					MISSION_FAILED(FR_KILLED_SHAG)
				ENDIF
			ENDIF			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    checks that the player is in the area and/or near beverly
PROC MONITER_PLAYER_IN_AREA()
	IF NOT bJumpSkip
		SWITCH eMissionStage
			CASE MS_FOLLOW_BEV_ROAD
				IF IS_PED_UNINJURED(mBuddy.piPed)	
					IF IS_PLAYER_AT_COORDS(<< -46.2972, 351.4437, 112.5526 >>, <<3,3,5>>)
					OR NOT IS_ENTITY_IN_RANGE_ENTITY(mBuddy.piPed,PLAYER_PED_ID(),120)	
						MISSION_FAILED(FR_LEFT_BUDDY)
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<< -51.9864, 340.5381, 111.0746 >>, << -62.1780, 344.6878, 114.3459 >>, 20)
					IF NOT bExpireWarning	
						CLEAR_PRINTS()
						KILL_ANY_CONVERSATION()
						PRINT_NOW("PAP2_06", DEFAULT_GOD_TEXT_TIME,0)
						bExpireWarning = TRUE
					ENDIF
				ENDIF
			FALLTHRU
			CASE MS_FOLLOW_BEV_GUARDS
			FALLTHRU
			CASE MS_FOLLOW_TO_POPPY
			FALLTHRU
			CASE MS_FILM_POPPY
			FALLTHRU
			CASE MS_GET_IN_ESCAPE_CAR	
				
				IF eMissionStage >= MS_FOLLOW_BEV_GUARDS
					IF NOT IS_PLAYER_AT_ENTITY(mBuddy.piPed, <<25,25,40>>)
						IF iFailInactivityTimer > 300
							IF eMissionStage = MS_GET_IN_ESCAPE_CAR
								//MISSION_FAILED(FR_TOOK_TOO_LONG)
							ELSE
								MISSION_FAILED(FR_BEV_LEFT_YOU)
							ENDIF
						ELSE
							iFailInactivityTimer++
						ENDIF
						
						IF eMissionStage <> MS_GET_IN_ESCAPE_CAR
							IF NOT bExpireWarning	
								PRINT_NOW("PAP2_06", DEFAULT_GOD_TEXT_TIME,0)
								bExpireWarning = TRUE
							ENDIF
						ENDIF
					ELSE
						iFailInactivityTimer = 0
					ENDIF
				ENDIF
				
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks for fail conditions
PROC CHECK_FOR_FAIL()
	IF eMissionStage < MS_PASSED
		MONITER_PLAYER_KILLED()
		MONITER_PLAYER_IN_AREA()
		IF NOT bBudThreat
			IF HAS_PLAYER_THREATENED_PED(mBuddy.piPed)
			OR (IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0) AND eMissionStage < MS_CAMERA_TUTORIAL)	
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				GIVE_FLEE_ORDER(mBuddy.piPed)
				//IF IS_ANYONE_SHOOTING_NEAR_PED(mBuddy.piPed)
					GIVE_FLEE_ORDER(mHotelStaff[0].piPed)
					GIVE_FLEE_ORDER(mHotelStaff[1].piPed)
					GIVE_FLEE_ORDER(mPoppy.piPed)
					GIVE_FLEE_ORDER(mShagger.piPed)			
					GIVE_FLEE_ORDER(mBodyGuard.piPed)			
				//ENDIF
				bBudThreat = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD

/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		IF eState = SS_ACTIVE
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
				WAIT_FOR_CUTSCENE_TO_STOP()
				TRIGGER_MUSIC_EVENT("PAP2_FAIL")	
				//IF eMissionStage = MS_CAR_CHASE
					IF NOT IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_OUT()
						SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
						bSkipPassMission = TRUE
					ENDIF
				//ENDIF
				
				eMissionStage = MS_PASSED
			ENDIF
			
			// Check for Fail
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				WAIT_FOR_CUTSCENE_TO_STOP()
				TRIGGER_MUSIC_EVENT("PAP2_FAIL")	
				IF eMissionStage <> MS_CAR_CHASE
					MISSION_FAILED(FR_NONE)
				ELSE
					bPlayerFailed = TRUE
				ENDIF
			ENDIF
				
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) AND eMissionStage <> MS_INIT
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF eMissionStage = MS_FOLLOW_TO_POPPY
					JUMP_TO_STAGE(MS_CAMERA_TUTORIAL)
				ELSE
					eState = SS_SKIPPED
				ENDIF
			ENDIF	
			
			IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
			AND eMissionStage <> MS_INIT 
			AND eMissionStage <> MS_INTRO //AND eMissionStage <> MS_CAR_CHASE
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//Work out which stage we want to reach based on the current stage
				iMissionState = ENUM_TO_INT(eMissionStage)
				
				IF eMissionStage = MS_FILM_POPPY
					MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, iMissionState - 2)
					JUMP_TO_STAGE(e_stage)
				ELSE
					IF iMissionState > 0	
						MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			ENDIF
		
		    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
				
				SWITCH i_debug_jump_stage
					CASE 0
						i_debug_jump_stage = 1
					BREAK	
					
					CASE 1
						i_debug_jump_stage = 2
					BREAK
					
					CASE 2
						i_debug_jump_stage = 5
					BREAK
					
					CASE 3
						i_debug_jump_stage = 6
					BREAK
					
					CASE 4
						i_debug_jump_stage = 7
					BREAK
					
					CASE 5
						bUseLockedCam = FALSE
						i_debug_jump_stage = 8
					BREAK
					
					CASE 6
						i_debug_jump_stage = 9
					BREAK
					
					CASE 7
						i_debug_jump_stage = 12
						SK_PRINT("JUMP to UBA REC")
					BREAK
					
					CASE 8
						i_debug_jump_stage = 13
						SK_PRINT("JUMP to POPPY AND UBA")
					BREAK
						
					CASE 9
						i_debug_jump_stage = 14
						SK_PRINT("JUMP to SET PIECE RECORDING")
					BREAK
					
					CASE 10
						bUseLockedCam = TRUE
						i_debug_jump_stage = 8
					BREAK
					
					CASE 11
						bSkipToEndOfChase = TRUE
						i_debug_jump_stage = 8
					BREAK
					
				ENDSWITCH

				MISSION_STAGE e_stage = INT_TO_ENUM(MISSION_STAGE, i_debug_jump_stage)
		      
				//STOP_CUTSCENE()
				//WAIT_FOR_CUTSCENE_TO_STOP()

				JUMP_TO_STAGE(e_stage)
		    ENDIF
		ENDIF
	ENDPROC
	
	PROC WRITE_BOOL_TO_DEBUG(STRING name, BOOL bOn)
		SAVE_STRING_TO_DEBUG_FILE(name)
		SAVE_BOOL_TO_DEBUG_FILE(bOn)
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	ENDPROC
	
	PROC WRITE_FLOAT_TO_DEBUG(STRING name, FLOAT thing)
		SAVE_STRING_TO_DEBUG_FILE(name)
		SAVE_FLOAT_TO_DEBUG_FILE(thing)
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	ENDPROC
	
	PROC WRITE_INT_TO_DEBUG(STRING name, INT thing)
		SAVE_STRING_TO_DEBUG_FILE(name)
		SAVE_INT_TO_DEBUG_FILE(thing)
		SAVE_NEWLINE_TO_DEBUG_FILE()		
	ENDPROC	
	
	PROC UPDATE_WIDGETS()
		IF bPrintWidgetValuesToDebug
			OPEN_DEBUG_FILE()
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorX1 = ", fCursorX1)
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorY1 = ", fCursorY1)
				
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorX2 = ", fCursorX2)
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorY2 = ", fCursorY2)
				
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorX3 = ", fCursorX3)
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorY3 = ", fCursorY3)

				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorX4 = ", fCursorX4)
				WRITE_FLOAT_TO_DEBUG("FLOAT fCursorY4 = ", fCursorY4)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			bPrintWidgetValuesToDebug = FALSE
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	mScenarioBlocker = Pap2_Scenario_Blocker()
	
	SET_MISSION_FLAG(TRUE)

	DISABLE_TAXI_HAILING(TRUE) //For B*1860244 - Blocking taxis during mission.

	REQUEST_ANIM_DICT("rcmpaparazzo_2")
	
	IF IS_REPEAT_PLAY_ACTIVE()
		WHILE NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_2")
			WAIT(0)
		ENDWHILE
	ENDIF

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_2(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF	
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iSynchScene)	
		TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(iSynchScene)	
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "PAP2 TAKE_OWNERSHIP_OF_SYNCHRONIZED_SCENE(iSynchScene)")
		#ENDIF
	ENDIF
	
	viPlayerCar = sRCLauncherDataLocal.vehID[0]
	sRCLauncherDataLocal.vehID[0] = NULL
	
	REQUEST_MODEL(Prop_Pap_Camera_01)
	REQUEST_CLIP_SET(sWeaponMoveClipset)
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE) 

	SET_AMBIENT_ZONE_STATE("AZ_PAPARAZZO_02_AMBIENCE",TRUE,TRUE)

	CLEAR_AREA_OF_OBJECTS(<<-30.860102,301.191986,112.116371>>,10)

	IF IS_REPLAY_IN_PROGRESS()	
	AND bDebugSkipping = FALSE	
		mBuddy.piPed = sRCLauncherDataLocal.pedID[0]
		sRCLauncherDataLocal.pedID[0] = NULL
		IF IS_PED_UNINJURED(mBuddy.piPed)	
			SET_PED_LEG_IK_MODE(mBuddy.piPed, LEG_IK_PARTIAL)
		ENDIF
	ELSE
		IF NOT IS_REPLAY_IN_PROGRESS()	
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])	
			AND IS_PED_UNINJURED(PLAYER_PED_ID())	
			AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),sRCLauncherDataLocal.pedID[0],4)	
			//AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 3	
				bDoingFocusPush = TRUE
			ENDIF
		ENDIF
	ENDIF
				
	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF

	ADD_RELATIONSHIP_GROUP("POPPY_GROUP", relPoppyGroup)
	
	IF IS_REPLAY_IN_PROGRESS()
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	ELSE
		SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY",10)
	ENDIF

	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)

	WHILE(TRUE)
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TST")
		
		MAKE_CLOUDY_IF_NIGHT()
		
		IF DOES_CAM_EXIST(camFrankFilm)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			SET_USE_HI_DOF()
		ENDIF
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			IF eMissionStage <> MS_FAILED
				DEBUG_Check_Debug_Keys()
			ENDIF
		#ENDIF
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			IF eMissionStage <> MS_FAILED
			AND eMissionStage <> MS_INIT
			AND eMissionStage <> MS_INTRO
				CHECK_FOR_FAIL()
			ENDIF
			
			SWITCH eMissionStage
				CASE MS_INIT //0
					INITMISSION()
				BREAK
				
				CASE MS_INTRO //1
					INTRO()
				BREAK
				
				CASE MS_FOLLOW_BEV_ROAD //2
					FOLLOWING_BEV()
				BREAK
				
				CASE MS_FOLLOW_BEV_GUARDS //3
					FOLLOWING_BEV()
				BREAK
				
				CASE MS_FOLLOW_TO_POPPY //4
					FOLLOWING_BEV()
				BREAK
				
				CASE MS_CAMERA_TUTORIAL//5
					CAMERA_TUTORIAL()
				BREAK
				
				CASE MS_FILM_POPPY //6
					FILM_POPPY()
				BREAK

				CASE MS_GET_IN_ESCAPE_CAR //7
					GET_IN_ESCAPE_CAR()
				BREAK

				CASE MS_CAR_CHASE//8
					CAR_CHASE()
				BREAK

				CASE MS_OUTRO //9
					OUTRO_NEW()
				BREAK

				CASE MS_PASSED //10
					Script_Passed()
				BREAK
				
				CASE MS_FAILED //11
					FAILED_WAIT_FOR_FADE()
				BREAK

				CASE MS_UBER_RECORD //12
					#IF IS_DEBUG_BUILD
						RECORD_UBER_CAR()
					#ENDIF
				BREAK
				
				CASE MS_POPPY_AND_UBA //13
					#IF IS_DEBUG_BUILD
						POPPY_AND_UBA()
					#ENDIF
				BREAK

				CASE MS_SET_PIECE_RECORDING //14
					#IF IS_DEBUG_BUILD
						SET_PIECE_RECORDING()
					#ENDIF
				BREAK
				
			ENDSWITCH
			
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
