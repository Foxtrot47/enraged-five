
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "CompletionPercentage_public.sch"
USING "RC_Helper_Functions.sch"
USING "chase_hint_cam.sch"
USING "RC_Threat_public.sch"
USING "RC_Area_public.sch"
USING "traffic_default_values.sch"
USING "traffic.sch"
USING "RC_Launcher_public.sch"
USING "initial_scenes_Paparazzo.sch"
USING "commands_event.sch"
USING "commands_recording.sch"

USING "mp_scaleform_functions.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF

USING "taxi_functions.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo3A.sc
//		AUTHOR			:	Ben Hinchliffe/Tom Kingsley
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// The Random Character - sRCLauncherDataLocal.mPedID[0]
g_structRCScriptArgs sRCLauncherDataLocal

ENUM MISSION_STAGE

	MS_INITIAL_PHONE,
	MS_GO_TO_LOCATION,
	MS_CHASE_POPPY,
	MS_GO_TO_CRASH,
	MS_TAKE_PHOTO,
	MS_END_PHONECALL,
	MS_MISSION_FAILED
	
ENDENUM

ENUM SUB_STAGE
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

ENUM SUB_REC_STAGE
	SSRP_SETUP,
	SSRP_UPDATE,
	SSRP_CLEANUP
ENDENUM

// Fail reasons
ENUM FAIL_REASON
	FAIL_DEFAULT,
	FAIL_PHOTO_LOST,
	FAIL_POPPY_INJURED,
	FAIL_POPPY_KILLED,
	FAIL_POPPY_LOST,
	FAIL_POLICE_INTER
ENDENUM

ENUM POPPY_STATE
	PS_WAIT_FOR_PLAYER,
	PS_DRIVING,
	PS_LEAVE_VEHICLE,
	PS_SURRENDER,
	PS_ARRESTED,
	PS_GET_IN_VEHICLE,
	PS_POPPY_IN_COP_CAR,
	PS_FLEE_AREA
ENDENUM

ENUM STANDING_STATE
	SS_STANDING_AND_WAIT,
	SS_AIM_GUN_AT_POPPY,
	SS_AIM_GUN_AT_PLAYER,
	SS_ATTACK_PLAYER,
	SS_GET_INTO_VEHICLE,
	SS_WANDER_IN_AREA,
	SS_FLEE_FROM_CARS
ENDENUM

ENUM COP_STATE
	CS_DRIVING,
	CS_LEAVE_VEHICLE,
	CS_AIM_AT_POPPY,
	CS_MOVE_TO_POPPY,
	CS_ARREST_POPPY,
	CS_ENTER_COP_CAR,
	CS_APPROAH_CRASHED_CAR,
	CS_LEAVE_AREA,
	CS_AREA_WARN_PLAYER,
	CS_ATTACK_PLAYER
ENDENUM

//Player's car
VEHICLE_INDEX vehPlayer
VEHICLE_INDEX vehLimo
VEHICLE_INDEX vehPoliceChopper

PED_INDEX pedChopperPilot
PED_INDEX pedValet

// MISSION PED
STRUCT MISSION_PED
	PED_INDEX mPed
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
	STRING	sIntialAnim
	BOOL	bImDead
	BLIP_INDEX	mBlip
ENDSTRUCT


// MISSION VEHICLE
STRUCT MISSION_VEHICLE
	VEHICLE_INDEX mVehicle
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT

// MISSION PROPS
STRUCT MISSION_PROP
	OBJECT_INDEX mObject
	VECTOR vStartPos
	FLOAT fStartHeading
	MODEL_NAMES mModel
ENDSTRUCT

VECTOR vCop
	
SEQUENCE_INDEX seqAvoidCarDoor
SEQUENCE_INDEX seqPlayerExitCarFaceCrash

CAMERA_INDEX camPoppy
CAMERA_INDEX camPoppy2

BLIP_INDEX blipPoppy
BLIP_INDEX blipPoppyCar

VEHICLE_INDEX vehTraffic[10]
PED_INDEX pedRandomDriver[10]
SEQUENCE_INDEX seqTraffic[10]

INT iShockingEvent

//STREAMVOL_ID stream_volume

INT iTimerCorrectPicSent		
INT iTimerPicTaken
INT iTimerCopsLeavingArea
INT iRandomTrafficSeq
INT poppy_arrest_scene
INT iBevTxtsBadPic
INT iBevTxtsTooFarAway
INT iSeqCasino
INT iSeqPoppyDialogue
INT iWanted
INT iRammingCops
INT iTimerCheckDamage

INT iSubSwitch
INT iRetrigger
INT iSubsPoppy

INT iFramesCheckingPhotoOk
INT iPhotoScore

TEXT_LABEL_23 labelRetrig

BOOL bHelpPrint1
BOOL bHelpPrint2
BOOL bHelpPrint3
BOOL bPlayerLeftArea
BOOL bPrintCatchUp
BOOL bDoneFakeStart
BOOL bPrintLeaveArea
BOOL bCrashSoundPlayed
BOOL bPoppyCarSmashed
BOOL bCarStopping	
//BOOL bBadPicSent
BOOL bRegisterBevCall
BOOL bPrintNonSimpleHelp = FALSE	
BOOL bPrintWantedHelp = FALSE
BOOL bPhotoInCuffs = FALSE
BOOL bStatsFarFromPoppy = FALSE
BOOL bReplay
BOOL bHeliReplay
BOOL bWrongContact = FALSE
BOOL bTooFarAway
BOOL bPicTaken
BOOL bPicSent
BOOL bSweetTxtSent
BOOL bKickedOffDialogueAgain
BOOL bPhotoPoppyObj
//BOOL bSkipMore
BOOL bDelayFade
BOOL bCopWarning
BOOL bPlayerStopped
BOOL bReplayTaskHeli

INT iDoDelayedLastLine

BOOL bDelayMusic
INT iTimerDelayMusic
INT iCopCrashSeq1
//BOOL bEngineSwitch

//INT  iPlaybackProgress

VECTOR vPoppy
VECTOR vPoliceCarPos
VECTOR vPitchersJunc
VECTOR vPawnJunc

BOOL bCrashCutDone
BOOL bPoppyAndCopDrivingOff

INT iTimerReplayStarted
BOOL bDoneDelayedFadeIn
//FLOAT fAlpha = 1

FLOAT fPhase
FLOAT fTimePosInRec
FLOAT fLowPlaybackSpeedClamp
FLOAT fHighPlaybackSpeedClamp = 1.0
FLOAT fMaxDist
//VECTOR vBlip
//FLOAT fSlowFact = 300
//FLOAT fSlidy

MISSION_STAGE missionStage = MS_INITIAL_PHONE
SUB_STAGE  eSubStage = SS_SETUP
SUB_REC_STAGE	eSubStageRec = SSRP_SETUP
POPPY_STATE ePoppyState = PS_WAIT_FOR_PLAYER

// STAGE SKIPPING
// STAGE SKIPPING
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 6
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

MISSION_STAGE eTargetStage 	// now used in release build too for checkpoint stuff
BOOL bFinishedSkipping  	// now used in release build too for checkpoint stuff

//----------------------
//	CHECKPOINTS
//----------------------

CONST_INT CP_AT_POPPY_LOCATION			1
CONST_INT CP_AT_CRASH_LOCATION			2
CONST_INT CP_END						3

CONST_INT Z_SKIP_PHONECALL				1
CONST_INT Z_SKIP_AT_POPPY_LOCATION		2
CONST_INT Z_SKIP_AT_CRASH_LOCATION		4

CONST_INT		NUM_OF_POLICE_CARS		3
CONST_INT		NUM_OF_PARKED_CARS 		10
CONST_INT		NUM_OF_POLICE_PEDS		4
CONST_INT		NUM_OF_STANDING_PEDS 	6 //COPS AND BUILDERS STANDING DURING CHASE
CONST_INT		NUM_OF_CHASE_PROPS		10

COP_STATE		eCopState[NUM_OF_POLICE_PEDS]
STANDING_STATE 	eStandingState[NUM_OF_STANDING_PEDS] 

#IF IS_DEBUG_BUILD
	INT				iRecordingProgress
#ENDIF


INT				iHelp
INT				iDialogueToPlay			//USED FOR DIALOGUE DURING CHASE
INT 			iSeqCarDoors
INT				iTimerSyncSceneStarted

//BLIPS
BLIP_INDEX		biMissionBlip

MISSION_VEHICLE		mvPoliceCars[NUM_OF_POLICE_CARS]
MISSION_VEHICLE		mvPoppyCar
MISSION_VEHICLE		mvParkedCar[NUM_OF_PARKED_CARS] //CAR PARKED NEAR CHASE START FOR PLAYER CONVENEINCE

MISSION_PED			mpPolicePed[NUM_OF_POLICE_PEDS]
MISSION_PED			mpPoppyPed

MISSION_PED			mpStandingPeds[NUM_OF_STANDING_PEDS]

MISSION_PROP		mpsChaseProps[NUM_OF_CHASE_PROPS]

//BOOLS
BOOL			bMainObjectiveDisplayed
BOOL			bPoliceChaseCreated
BOOL			bCopsAreLeaving 
BOOL			bCopCrashed
BOOL			b_Correct_Pic_Taken
BOOL			bPhotoSent
BOOL			bPoppyHasCrashed
BOOL			bConversationActive
BOOL			bPoliceAttacking
BOOL			bSimpleHelp
BOOL			bFailDialogue
BOOL			bAmbDialogue
//BOOL            bPlayerForcedOutAtCrash
BOOL bDoingRollingReplay
BOOL			bDone1stPersonFlash

//FLOATS
FLOAT			fPlayBackSpeed
//FLOAT           fPlaybackAccel = 0.0
FLOAT           fCopCarFrontDoorOpenRatio = 0
FLOAT           fCopCarRearDoorOpenRatio = 0
FLOAT			fPoppyCarFrontDoorOpenRatio = 0

//CONST FLOATS
CONST_FLOAT		FOUND_DIS	700.0//90.0
CONST_FLOAT		FOUND_DIS2 	700.0//180.0
CONST_FLOAT		LOSE_DISTANCE		220.0 //180.0//150.0

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 	wgUberChase
#ENDIF

//VECTORS
VECTOR			vPoppyInitialLocation		=		<< 695.8621, 12.6177, 83.1933 >>
VECTOR			vCrashLocation				=		<< 341.9170, -409.2796, 44.1959 >>
VECTOR 			vPoppySurrenderPos 			= 		<< 345.3037, -406.1878, 44.1304 >>//<< 345.2901, -407.4490, 44.0822 >>

//POLY TESTS
TEST_POLY		tpPoliceBlock

//STRINGS
FAIL_REASON 		eFailReason

// CONVERSATIONS
structPedsForConversation sSpeach 
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

//SEQUENCE_INDEX		si_Poppy1			//POPPY'S arrest sequence at crash scene

//Relationship groups
REL_GROUP_HASH 		relCopGroup
REL_GROUP_HASH		relPoppy

//USED FOR ROLLING START - REPLAY
BOOL bDoRollingStart
BOOL bDoneRollingStart
INT  iRollingStartTimer

/// PURPOSE:
///    Loads all the inital arrays - PED LOCATIONS etc. 
PROC INIT_ARRAYS
			
	//VEHICLES
	
	//CELEBRITY - POPPY'S CAR
	mvPoppyCar.vStartPos = <<722.702881,65.287430,82.590530>>//<< 735.4008, 85.3099, 80.9257 >>
	mvPoppyCar.fStartHeading = 141.0
	mvPoppyCar.mModel = COGCABRIO //Zion2(cool) //sentinel2 //CARBONIZZARE //sentinel
	
	//PARKED CAR
	mvParkedCar[0].vStartPos = <<703.1887, 230.0058, 91.6438>>//<< 621.6558, 61.9796, 89.0208 >>
	mvParkedCar[0].fStartHeading = 59.6823//152.0
	mvParkedCar[0].mModel = JACKAL
	mvParkedCar[1].vStartPos = << 754.2217, -163.2057, 73.7662 >>
	mvParkedCar[1].fStartHeading = 238.0
	mvParkedCar[1].mModel = sentinel  //JACKAL
	mvParkedCar[6].vStartPos = << 881.5699, 39.6837, 77.4769 >>
	mvParkedCar[6].fStartHeading = 314.0
	mvParkedCar[6].mModel = pounder
	mvParkedCar[7].vStartPos = << 806.9825, -83.9695, 79.5911 >>
	mvParkedCar[7].fStartHeading = 55.0
	mvParkedCar[7].mModel = pounder
	mvParkedCar[8].vStartPos = << 894.0145, -38.8992, 77.7647 >>
	mvParkedCar[8].fStartHeading = 238.0
	mvParkedCar[8].mModel = JACKAL
	mvParkedCar[9].vStartPos = << 903.1145, -65.7640, 77.7647 >>
	mvParkedCar[9].fStartHeading = 57.0
	mvParkedCar[9].mModel = sentinel  //taxi
	//PARKED TRUCKS
	mvParkedCar[2].vStartPos = << 639.1010, -97.6283, 73.5104 >>
	mvParkedCar[2].fStartHeading = 230.0
	mvParkedCar[2].mModel = tiptruck
	mvParkedCar[3].vStartPos = << 637.8901, -75.7718, 73.9779 >>
	mvParkedCar[3].fStartHeading = 312.0
	mvParkedCar[3].mModel = tiptruck
	//PARKED COP CARS
	mvParkedCar[4].vStartPos = << 813.1224, -60.5906, 79.6415 >>
	mvParkedCar[4].fStartHeading = 151.0
	mvParkedCar[4].mModel = police3
	mvParkedCar[5].vStartPos = << 806.4680, -69.2880, 79.6412 >>
	mvParkedCar[5].fStartHeading = 325.0
	mvParkedCar[5].mModel = police3
	
	//POLICE CARS
	mvPoliceCars[0].vStartPos = <<535.0950, 181.3821, 99.4294>>//<< 735.9570, 78.9067, 81.3816 >>
	mvPoliceCars[0].fStartHeading = 340.6452//141.0
	mvPoliceCars[0].mModel = police3
	mvPoliceCars[1].vStartPos = <<699.5699, 225.2489, 91.5223>>//<< 726.9023, 73.2327, 82.1510 >>
	mvPoliceCars[1].fStartHeading = 240.1702//141.0
	mvPoliceCars[1].mModel = police3
	mvPoliceCars[2].vStartPos = << 827.4832, -55.2525, 79.5899 >>
	mvPoliceCars[2].fStartHeading = 77.0
	mvPoliceCars[2].mModel = police3
	
	//PEDS
	
	//POLICE
	mpPolicePed[0].mModel = S_M_Y_Cop_01
	mpPolicePed[1].mModel =	S_M_Y_Cop_01
	mpPolicePed[2].mModel =	S_M_Y_Cop_01
	mpPolicePed[3].mModel =	S_M_Y_Cop_01
	
	//POPPY
	mpPoppyPed.mModel = U_F_Y_PoppyMich
	
	//STANDING PEDS
	mpStandingPeds[0].vStartPos = << 798.3842, -76.0535, 79.5907 >>
	mpStandingPeds[0].fStartHeading = 46.0
	mpStandingPeds[0].mModel = S_M_Y_Cop_01
	mpStandingPeds[1].vStartPos = << 805.3050, -75.5530, 79.4485 >>
	mpStandingPeds[1].fStartHeading = 46.0
	mpStandingPeds[1].mModel = S_M_Y_Cop_01
	mpStandingPeds[2].vStartPos = << 627.6722, -84.7451, 73.0863 >>
	mpStandingPeds[2].fStartHeading = 123.0
	mpStandingPeds[2].mModel = S_M_Y_Cop_01 //S_M_Y_Construct_02
	mpStandingPeds[3].vStartPos = << 631.7634, -77.7747, 73.5139 >>
	mpStandingPeds[3].fStartHeading = 270.0
	mpStandingPeds[3].mModel = S_M_Y_Cop_01 //S_M_Y_Construct_02
	mpStandingPeds[4].vStartPos = << 826.9551, -57.2431, 79.5911 >>
	mpStandingPeds[4].fStartHeading = 48.0
	mpStandingPeds[4].mModel = S_M_Y_Cop_01
	mpStandingPeds[5].vStartPos = << 814.1181, -64.4872, 79.6434 >>
	mpStandingPeds[5].fStartHeading = 52.0
	mpStandingPeds[5].mModel = S_M_Y_Cop_01
	
	
	//PROPS
	mpsChaseProps[0].vStartPos = << 634.4785, -68.8406, 74.2911 >>
	mpsChaseProps[0].fStartHeading = 24.0
	mpsChaseProps[0].mModel = PROP_BARRIER_WORK04A
	mpsChaseProps[1].vStartPos = << 627.1672, -73.8742, 73.3072 >>
	mpsChaseProps[1].fStartHeading = 33.0
	mpsChaseProps[1].mModel = PROP_BARRIER_WORK04A
	mpsChaseProps[2].vStartPos = << 653.2055, -96.2269, 73.4953 >>
	mpsChaseProps[2].fStartHeading = 238.0
	mpsChaseProps[2].mModel = PROP_BARRIER_WORK04A
	mpsChaseProps[3].vStartPos = << 657.0881, -91.6722, 73.5133 >>
	mpsChaseProps[3].fStartHeading = 58.0
	mpsChaseProps[3].mModel = PROP_BARRIER_WORK04A
	mpsChaseProps[4].vStartPos = << 626.6002, -89.5324, 72.7673 >>
	mpsChaseProps[4].fStartHeading = 72.0
	mpsChaseProps[4].mModel = PROP_BARREL_01A
	mpsChaseProps[5].vStartPos = << 624.3754, -88.3926, 72.4953 >>
	mpsChaseProps[5].fStartHeading = 14.0
	mpsChaseProps[5].mModel = PROP_BARREL_01A
	mpsChaseProps[6].vStartPos = << 626.1615, -85.4177, 72.9135 >>
	mpsChaseProps[6].fStartHeading = 323.0
	mpsChaseProps[6].mModel = PROP_BARREL_01A
	mpsChaseProps[7].vStartPos = << 809.2114, -64.1885, 79.6407 >>
	mpsChaseProps[7].fStartHeading = 236.0
	mpsChaseProps[7].mModel = PROP_BARRIER_WORK06A
	mpsChaseProps[8].vStartPos = << 815.4871, -54.4268, 79.5902 >>
	mpsChaseProps[8].fStartHeading = 233.0
	mpsChaseProps[8].mModel = PROP_BARRIER_WORK06A
	mpsChaseProps[9].vStartPos = << 802.5724, -72.9779, 79.4857 >>
	mpsChaseProps[9].fStartHeading = 232.0
	mpsChaseProps[9].mModel = PROP_BARRIER_WORK06A
	
ENDPROC

PROC SK_PRINT_FLOAT(String s, FLOAT f)
	CPRINTLN(DEBUG_MISSION,s)
	PRINTFLOAT(f)
ENDPROC

/// PURPOSE: Request and load uber recording
PROC PRELOAD_CAR_RECORDINGS_FOR_UBER_CHASE()

	//REQUEST_VEHICLE_RECORDING(250, "PAP3U") //POPPY CAR
	REQUEST_VEHICLE_RECORDING(300, "PAP3A2") //POPPY CAR

	WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(300, "PAP3A2") //POPPY CAR
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE: Load data for uber chase
PROC LOAD_UBER_DATA

	TrafficCarPos[2] = <<682.9803, -10.3641, 83.6767>>
	TrafficCarQuatX[2] = 0.0047
	TrafficCarQuatY[2] = -0.0004
	TrafficCarQuatZ[2] = -0.2797
	TrafficCarQuatW[2] = 0.9601
	TrafficCarRecording[2] = 4
	TrafficCarStartime[2] = 396.0000
	TrafficCarModel[2] = taxi

	/*
	TrafficCarPos[3] = <<767.3616, -48.5691, 80.2328>>
	TrafficCarQuatX[3] = 0.0093
	TrafficCarQuatY[3] = -0.0177
	TrafficCarQuatZ[3] = 0.8729
	TrafficCarQuatW[3] = -0.4874
	TrafficCarRecording[3] = 5
	TrafficCarStartime[3] = 3432.0000
	TrafficCarModel[3] = sentinel
	*/

	TrafficCarPos[4] = <<789.4747, -50.4215, 80.1686>>
	TrafficCarQuatX[4] = 0.0029
	TrafficCarQuatY[4] = -0.0066
	TrafficCarQuatZ[4] = 0.8867
	TrafficCarQuatW[4] = -0.4623
	TrafficCarRecording[4] = 6
	TrafficCarStartime[4] = 4224.0000
	TrafficCarModel[4] = fq2

	/*
	TrafficCarPos[5] = <<844.4269, 12.6046, 79.1439>>
	TrafficCarQuatX[5] = 0.0058
	TrafficCarQuatY[5] = 0.0154
	TrafficCarQuatZ[5] = 0.9275
	TrafficCarQuatW[5] = 0.3735
	TrafficCarRecording[5] = 7
	TrafficCarStartime[5] = 7920.0000
	TrafficCarModel[5] = fq2
	*/
	
	/*
	TrafficCarPos[6] = <<821.6198, -83.4346, 80.2198>>
	TrafficCarQuatX[6] = -0.0070
	TrafficCarQuatY[6] = -0.0036
	TrafficCarQuatZ[6] = 0.8792
	TrafficCarQuatW[6] = -0.4763
	TrafficCarRecording[6] = 8
	TrafficCarStartime[6] = 16104.0000
	TrafficCarModel[6] = taxi


	ParkedCarPos[0] = <<851.5927, -124.9243, 78.6984>>
	ParkedCarQuatX[0] = -0.0207
	ParkedCarQuatY[0] = -0.0259
	ParkedCarQuatZ[0] = 0.9615
	ParkedCarQuatW[0] = 0.2728
	ParkedCarModel[0] = taxi
	*/
	
	/*
	ParkedCarPos[2] = <<835.2613, -169.3098, 73.1024>>
	ParkedCarQuatX[2] = 0.0548
	ParkedCarQuatY[2] = -0.0154
	ParkedCarQuatZ[2] = -0.2750
	ParkedCarQuatW[2] = 0.9597
	ParkedCarModel[2] = youga
	*/

	TrafficCarPos[7] = <<806.2521, -187.2793, 72.3050>>
	TrafficCarQuatX[7] = -0.0038
	TrafficCarQuatY[7] = 0.0080
	TrafficCarQuatZ[7] = 0.8685
	TrafficCarQuatW[7] = -0.4957
	TrafficCarRecording[7] = 9
	TrafficCarStartime[7] = 19404.0000
	TrafficCarModel[7] = youga
/*
	ParkedCarPos[4] = <<835.3287, -210.6532, 71.9030>>
	ParkedCarQuatX[4] = 0.0087
	ParkedCarQuatY[4] = 0.0150
	ParkedCarQuatZ[4] = -0.0215
	ParkedCarQuatW[4] = 0.9996
	ParkedCarModel[4] = youga

	ParkedCarPos[5] = <<838.9982, -213.1142, 71.7213>>
	ParkedCarQuatX[5] = 0.0238
	ParkedCarQuatY[5] = -0.0129
	ParkedCarQuatZ[5] = 0.9995
	ParkedCarQuatW[5] = 0.0148
	ParkedCarModel[5] = youga
*/
	/*
	ParkedCarPos[6] = <<768.3843, -159.2555, 73.8261>>
	ParkedCarQuatX[6] = -0.0177
	ParkedCarQuatY[6] = 0.0319
	ParkedCarQuatZ[6] = 0.4678
	ParkedCarQuatW[6] = 0.8831
	ParkedCarModel[6] = sentinel
	*/

	/*
	TrafficCarPos[9] = <<738.7187, -149.3346, 74.3255>>
	TrafficCarQuatX[9] = 0.0144
	TrafficCarQuatY[9] = 0.0090
	TrafficCarQuatZ[9] = 0.8748
	TrafficCarQuatW[9] = -0.4842
	TrafficCarRecording[9] = 11
	TrafficCarStartime[9] = 23298.0000
	TrafficCarModel[9] = fq2
	*/


	TrafficCarPos[12] = <<509.1129, -205.9820, 50.8209>>
	TrafficCarQuatX[12] = -0.0385
	TrafficCarQuatY[12] = -0.0418
	TrafficCarQuatZ[12] = 0.9776
	TrafficCarQuatW[12] = -0.2024
	TrafficCarRecording[12] = 14
	TrafficCarStartime[12] = 37422.0000
	TrafficCarModel[12] = fq2

	TrafficCarPos[13] = <<513.0486, -201.2000, 51.4668>>
	TrafficCarQuatX[13] = -0.0373
	TrafficCarQuatY[13] = -0.0389
	TrafficCarQuatZ[13] = 0.9802
	TrafficCarQuatW[13] = -0.1905
	TrafficCarRecording[13] = 15
	TrafficCarStartime[13] = 37488.0000
	TrafficCarModel[13] = DUMMY_MODEL_FOR_SCRIPT

	TrafficCarPos[14] = <<519.2273, -187.7329, 52.8663>>
	TrafficCarQuatX[14] = 0.0475
	TrafficCarQuatY[14] = -0.0225
	TrafficCarQuatZ[14] = 0.1633
	TrafficCarQuatW[14] = 0.9852
	TrafficCarRecording[14] = 16
	TrafficCarStartime[14] = 37686.0000
	TrafficCarModel[14] = tiptruck

	TrafficCarPos[15] = <<524.1423, -238.5447, 48.7967>>
	TrafficCarQuatX[15] = 0.0388
	TrafficCarQuatY[15] = 0.0264
	TrafficCarQuatZ[15] = -0.0729
	TrafficCarQuatW[15] = 0.9962
	TrafficCarRecording[15] = 17
	TrafficCarStartime[15] = 38346.0000
	TrafficCarModel[15] = JACKAL

	TrafficCarPos[19] = <<360.4018, -401.2135, 45.2193>>
	TrafficCarQuatX[19] = -0.0046
	TrafficCarQuatY[19] = 0.0053
	TrafficCarQuatZ[19] = 0.7529
	TrafficCarQuatW[19] = -0.6581
	TrafficCarRecording[19] = 21
	TrafficCarStartime[19] = 47850.0000
	TrafficCarModel[19] = DUMMY_MODEL_FOR_SCRIPT

	TrafficCarPos[20] = <<338.5053, -391.3521, 44.8582>>
	TrafficCarQuatX[20] = -0.0030
	TrafficCarQuatY[20] = -0.0025
	TrafficCarQuatZ[20] = 0.6131
	TrafficCarQuatW[20] = 0.7900
	TrafficCarRecording[20] = 22
	TrafficCarStartime[20] = 48180.0000
	TrafficCarModel[20] = DUMMY_MODEL_FOR_SCRIPT
/*		
	SetPieceCarPos[1] = <<461.0806, -157.3552, 59.6509>>
	SetPieceCarQuatX[1] = 0.0134
	SetPieceCarQuatY[1] = -0.0977
	SetPieceCarQuatZ[1] = 0.7705
	SetPieceCarQuatW[1] = -0.6298
	SetPieceCarRecording[1] = 260
	SetPieceCarStartime[1] = 28637.2305
	SetPieceCarRecordingSpeed[1] = 1.0000
	SetPieceCarModel[1] = TAXI

	SetPieceCarPos[2] = <<578.6077, -355.2841, 44.4459>>
	SetPieceCarQuatX[2] = -0.0037
	SetPieceCarQuatY[2] = -0.0013
	SetPieceCarQuatZ[2] = 0.5335
	SetPieceCarQuatW[2] = 0.8458
	SetPieceCarRecording[2] = 261
	SetPieceCarStartime[2] = 33576.3008
	SetPieceCarRecordingSpeed[2] = 1.0000
	SetPieceCarModel[2] = POUNDER

	SetPieceCarPos[3] = <<372.6310, -287.3797, 53.3393>>
	SetPieceCarQuatX[3] = -0.0067
	SetPieceCarQuatY[3] = -0.0259
	SetPieceCarQuatZ[3] = 0.8329
	SetPieceCarQuatW[3] = -0.5528
	SetPieceCarRecording[3] = 262
	SetPieceCarStartime[3] = 33576.3008
	SetPieceCarRecordingSpeed[3] = 1.0000
	SetPieceCarModel[3] = TAXI

	SetPieceCarPos[4] = <<384.3606, -408.1361, 45.7654>>
	SetPieceCarQuatX[4] = -0.0376
	SetPieceCarQuatY[4] = -0.1187
	SetPieceCarQuatZ[4] = -0.6041
	SetPieceCarQuatW[4] = 0.7872
	SetPieceCarRecording[4] = 263
	SetPieceCarStartime[4] = 40576.3008
	SetPieceCarRecordingSpeed[4] = 1.0000
	SetPieceCarModel[4] = faggio2
*/	
ENDPROC	

PROC LOAD_UBER_DATA_NEW()

TrafficCarPos[0] = <<511.2060, 261.3451, 102.3948>>
TrafficCarQuatX[0] = -0.0108
TrafficCarQuatY[0] = -0.0073
TrafficCarQuatZ[0] = 0.8187
TrafficCarQuatW[0] = -0.5740
TrafficCarRecording[0] = 1
TrafficCarStartime[0] = 4830.0000
TrafficCarModel[0] = DUMMY_MODEL_FOR_SCRIPT //buccaneer

TrafficCarPos[1] = <<439.2651, 292.9166, 102.4979>>
TrafficCarQuatX[1] = -0.0095
TrafficCarQuatY[1] = 0.0129
TrafficCarQuatZ[1] = 0.5734
TrafficCarQuatW[1] = 0.8191
TrafficCarRecording[1] = 2
TrafficCarStartime[1] = 7272.0000
TrafficCarModel[1] = washington

TrafficCarPos[2] = <<402.5407, 299.8293, 102.4301>>
TrafficCarQuatX[2] = -0.0149
TrafficCarQuatY[2] = -0.0090
TrafficCarQuatZ[2] = 0.8170
TrafficCarQuatW[2] = -0.5764
TrafficCarRecording[2] = 3
TrafficCarStartime[2] = 8262.0000
TrafficCarModel[2] = sentinel //cogcabrio

TrafficCarPos[3] = <<366.4927, 309.5941, 103.0008>>
TrafficCarQuatX[3] = -0.0070
TrafficCarQuatY[3] = -0.0165
TrafficCarQuatZ[3] = 0.7886
TrafficCarQuatW[3] = -0.6146
TrafficCarRecording[3] = 4
TrafficCarStartime[3] = 9318.0000
IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())		
	TrafficCarModel[3] = DUMMY_MODEL_FOR_SCRIPT
ELSE	
	TrafficCarModel[3] = asterope
ENDIF

TrafficCarPos[4] = <<216.6715, 353.7352, 105.7779>>
TrafficCarQuatX[4] = -0.0200
TrafficCarQuatY[4] = -0.0268
TrafficCarQuatZ[4] = 0.8125
TrafficCarQuatW[4] = -0.5820
TrafficCarRecording[4] = 5
TrafficCarStartime[4] = 13542.0000
TrafficCarModel[4] = Baller

TrafficCarPos[5] = <<277.4835, 419.0747, 118.9695>>
TrafficCarQuatX[5] = -0.0142
TrafficCarQuatY[5] = -0.0284
TrafficCarQuatZ[5] = 0.8705
TrafficCarQuatW[5] = -0.4911
TrafficCarRecording[5] = 6
TrafficCarStartime[5] = 15984.0000
TrafficCarModel[5] = asterope

TrafficCarPos[6] = <<237.4069, 472.6873, 124.6924>>
TrafficCarQuatX[6] = -0.0221
TrafficCarQuatY[6] = -0.0790
TrafficCarQuatZ[6] = 0.9966
TrafficCarQuatW[6] = -0.0111
TrafficCarRecording[6] = 7
TrafficCarStartime[6] = 17370.0000
TrafficCarModel[6] = bullet

TrafficCarPos[7] = <<256.0805, 540.5441, 140.1842>>
TrafficCarQuatX[7] = -0.0117
TrafficCarQuatY[7] = -0.0054
TrafficCarQuatZ[7] = 0.8057
TrafficCarQuatW[7] = 0.5922
TrafficCarRecording[7] = 8
TrafficCarStartime[7] = 20208.0000
TrafficCarModel[7] = taxi

TrafficCarPos[8] = <<282.6450, 612.2068, 154.1533>>
TrafficCarQuatX[8] = 0.0177
TrafficCarQuatY[8] = -0.0114
TrafficCarQuatZ[8] = 0.4243
TrafficCarQuatW[8] = 0.9053
TrafficCarRecording[8] = 9
TrafficCarStartime[8] = 23177.0000
TrafficCarModel[8] = asterope

TrafficCarPos[9] = <<273.0108, 629.6464, 155.7711>>
TrafficCarQuatX[9] = -0.0256
TrafficCarQuatY[9] = -0.0694
TrafficCarQuatZ[9] = 0.9907
TrafficCarQuatW[9] = 0.1141
TrafficCarRecording[9] = 10
TrafficCarStartime[9] = 23639.0000
TrafficCarModel[9] = Baller

TrafficCarPos[10] = <<283.0059, 647.7762, 158.7737>>
TrafficCarQuatX[10] = 0.1093
TrafficCarQuatY[10] = -0.0014
TrafficCarQuatZ[10] = -0.1659
TrafficCarQuatW[10] = 0.9801
TrafficCarRecording[10] = 11
TrafficCarStartime[10] = 24629.0000
TrafficCarModel[10] = buccaneer

TrafficCarPos[11] = <<279.8875, 654.1951, 160.2324>>
TrafficCarQuatX[11] = -0.0411
TrafficCarQuatY[11] = -0.1096
TrafficCarQuatZ[11] = 0.9777
TrafficCarQuatW[11] = 0.1744
TrafficCarRecording[11] = 12
TrafficCarStartime[11] = 24893.0000
TrafficCarModel[11] = feltzer2

TrafficCarPos[12] = <<339.0951, 462.1592, 148.3102>>
TrafficCarQuatX[12] = 0.0028
TrafficCarQuatY[12] = -0.0627
TrafficCarQuatZ[12] = 0.9664
TrafficCarQuatW[12] = -0.2492
TrafficCarRecording[12] = 13
TrafficCarStartime[12] = 30523.0000
TrafficCarModel[12] = baller2

TrafficCarPos[13] = <<472.2878, 393.8821, 138.2691>>
TrafficCarQuatX[13] = 0.0207
TrafficCarQuatY[13] = 0.0216
TrafficCarQuatZ[13] = 0.2261
TrafficCarQuatW[13] = 0.9736
TrafficCarRecording[13] = 14
TrafficCarStartime[13] = 37056.0000
TrafficCarModel[13] = feltzer2

TrafficCarPos[14] = <<511.7101, 333.8549, 131.9026>>
TrafficCarQuatX[14] = 0.0424
TrafficCarQuatY[14] = 0.0430
TrafficCarQuatZ[14] = 0.6257
TrafficCarQuatW[14] = 0.7777
TrafficCarRecording[14] = 15
TrafficCarStartime[14] = 38508.0000
TrafficCarModel[14] = sentinel //cogcabrio

TrafficCarPos[15] = <<602.7618, 355.3197, 118.5270>>
TrafficCarQuatX[15] = -0.0574
TrafficCarQuatY[15] = 0.0440
TrafficCarQuatZ[15] = -0.5245
TrafficCarQuatW[15] = 0.8483
TrafficCarRecording[15] = 16
TrafficCarStartime[15] = 44316.0000
TrafficCarModel[15] = asterope

TrafficCarPos[16] = <<785.3267, 340.3435, 115.3910>>
TrafficCarQuatX[16] = -0.0268
TrafficCarQuatY[16] = -0.0051
TrafficCarQuatZ[16] = 0.7557
TrafficCarQuatW[16] = 0.6544
TrafficCarRecording[16] = 17
TrafficCarStartime[16] = 49794.0000
TrafficCarModel[16] = DUMMY_MODEL_FOR_SCRIPT //Baller

TrafficCarPos[17] = <<845.4529, 360.0610, 117.3146>>
TrafficCarQuatX[17] = 0.0148
TrafficCarQuatY[17] = 0.0047
TrafficCarQuatZ[17] = -0.4289
TrafficCarQuatW[17] = 0.9032
TrafficCarRecording[17] = 18
TrafficCarStartime[17] = 51642.0000
TrafficCarModel[17] = buccaneer  //cogcabrio

TrafficCarPos[18] = <<880.3807, 403.1809, 118.6872>>
TrafficCarQuatX[18] = -0.0194
TrafficCarQuatY[18] = -0.0024
TrafficCarQuatZ[18] = 0.9411
TrafficCarQuatW[18] = 0.3375
TrafficCarRecording[18] = 19
TrafficCarStartime[18] = 53292.0000
TrafficCarModel[18] = baller2

TrafficCarPos[19] = <<911.8809, 467.8841, 120.2237>>
TrafficCarQuatX[19] = -0.0008
TrafficCarQuatY[19] = -0.0055
TrafficCarQuatZ[19] = 0.9990
TrafficCarQuatW[19] = 0.0435
TrafficCarRecording[19] = 20
TrafficCarStartime[19] = 55536.0000
TrafficCarModel[19] = coquette

TrafficCarPos[20] = <<916.1319, 504.1710, 119.8686>>
TrafficCarQuatX[20] = -0.0102
TrafficCarQuatY[20] = 0.0531
TrafficCarQuatZ[20] = 0.0939
TrafficCarQuatW[20] = 0.9941
TrafficCarRecording[20] = 21
TrafficCarStartime[20] = 56657.0000
TrafficCarModel[20] = sentinel //cogcabrio

TrafficCarPos[21] = <<1072.1567, 441.7333, 91.5848>>
TrafficCarQuatX[21] = 0.0051
TrafficCarQuatY[21] = -0.0205
TrafficCarQuatZ[21] = 0.9189
TrafficCarQuatW[21] = -0.3940
TrafficCarRecording[21] = 22
TrafficCarStartime[21] = 65264.0000
TrafficCarModel[21] = feltzer2

TrafficCarPos[22] = <<1109.0264, 416.9391, 83.1078>>
TrafficCarQuatX[22] = 0.0054
TrafficCarQuatY[22] = -0.0016
TrafficCarQuatZ[22] = 0.9268
TrafficCarQuatW[22] = 0.3756
TrafficCarRecording[22] = 23
TrafficCarStartime[22] = 66584.0000
TrafficCarModel[22] = sentinel //cogcabrio

TrafficCarPos[23] = <<1120.3549, 421.6550, 82.8984>>
TrafficCarQuatX[23] = 0.0164
TrafficCarQuatY[23] = 0.0015
TrafficCarQuatZ[23] = 0.9267
TrafficCarQuatW[23] = 0.3754
TrafficCarRecording[23] = 24
TrafficCarStartime[23] = 66980.0000
TrafficCarModel[23] = coquette

TrafficCarPos[24] = <<1119.2665, 402.3738, 83.0242>>
TrafficCarQuatX[24] = -0.0020
TrafficCarQuatY[24] = -0.0141
TrafficCarQuatZ[24] = -0.3897
TrafficCarQuatW[24] = 0.9208
TrafficCarRecording[24] = 25
TrafficCarStartime[24] = 67046.0000
TrafficCarModel[24] = sentinel //cogcabrio

TrafficCarPos[25] = <<1154.8197, 355.7718, 90.8335>>
TrafficCarQuatX[25] = -0.0158
TrafficCarQuatY[25] = 0.0060
TrafficCarQuatZ[25] = 0.9599
TrafficCarQuatW[25] = -0.2799
TrafficCarRecording[25] = 26
TrafficCarStartime[25] = 68564.0000
TrafficCarModel[25] = coquette

TrafficCarPos[26] = <<983.0621, 173.4691, 80.5089>>
TrafficCarQuatX[26] = -0.0049
TrafficCarQuatY[26] = 0.0019
TrafficCarQuatZ[26] = 0.9411
TrafficCarQuatW[26] = 0.3380
TrafficCarRecording[26] = 27
TrafficCarStartime[26] = 77247.0000
TrafficCarModel[26] = feltzer2

ParkedCarPos[0] = <<852.1576, -124.6406, 78.5719>>
ParkedCarQuatX[0] = -0.0278
ParkedCarQuatY[0] = -0.0124
ParkedCarQuatZ[0] = 0.9574
ParkedCarQuatW[0] = 0.2872
ParkedCarModel[0] = sentinel //cogcabrio

TrafficCarPos[27] = <<812.6069, -185.2938, 72.7078>>
TrafficCarQuatX[27] = -0.0002
TrafficCarQuatY[27] = 0.0004
TrafficCarQuatZ[27] = 0.4820
TrafficCarQuatW[27] = 0.8762
TrafficCarRecording[27] = 28
TrafficCarStartime[27] = 89655.0000
TrafficCarModel[27] = BjXL

TrafficCarPos[28] = <<718.7026, -136.8279, 74.7098>>
TrafficCarQuatX[28] = -0.0057
TrafficCarQuatY[28] = -0.0031
TrafficCarQuatZ[28] = 0.8765
TrafficCarQuatW[28] = -0.4813
TrafficCarRecording[28] = 29
TrafficCarStartime[28] = 93417.0000
IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())		
	TrafficCarModel[28] = DUMMY_MODEL_FOR_SCRIPT
ELSE	
	TrafficCarModel[28] = BUS
ENDIF

TrafficCarPos[29] = <<708.8309, -123.9532, 74.4544>>
TrafficCarQuatX[29] = -0.0127
TrafficCarQuatY[29] = 0.0230
TrafficCarQuatZ[29] = 0.4820
TrafficCarQuatW[29] = 0.8758
TrafficCarRecording[29] = 30
TrafficCarStartime[29] = 93945.0000
TrafficCarModel[29] = baller2

TrafficCarPos[30] = <<686.3724, -110.2447, 74.0336>>
TrafficCarQuatX[30] = -0.0118
TrafficCarQuatY[30] = 0.0160
TrafficCarQuatZ[30] = 0.4839
TrafficCarQuatW[30] = 0.8749
TrafficCarRecording[30] = 31
TrafficCarStartime[30] = 94869.0000
TrafficCarModel[30] = asterope

TrafficCarPos[31] = <<612.1208, -67.3511, 73.1600>>
TrafficCarQuatX[31] = 0.0707
TrafficCarQuatY[31] = 0.0064
TrafficCarQuatZ[31] = -0.5441
TrafficCarQuatW[31] = 0.8360
TrafficCarRecording[31] = 32
TrafficCarStartime[31] = 97311.0000
TrafficCarModel[31] = DUMMY_MODEL_FOR_SCRIPT //asterope

TrafficCarPos[32] = <<605.6122, -70.8522, 72.6917>>
TrafficCarQuatX[32] = 0.0678
TrafficCarQuatY[32] = 0.0099
TrafficCarQuatZ[32] = -0.5536
TrafficCarQuatW[32] = 0.8300
TrafficCarRecording[32] = 33
TrafficCarStartime[32] = 97443.0000
TrafficCarModel[32] = DUMMY_MODEL_FOR_SCRIPT //baller2

TrafficCarPos[33] = <<523.7166, -108.7238, 63.2222>>
TrafficCarQuatX[33] = 0.0044
TrafficCarQuatY[33] = -0.0835
TrafficCarQuatZ[33] = 0.9375
TrafficCarQuatW[33] = 0.3377
TrafficCarRecording[33] = 34
TrafficCarStartime[33] = 100809.0000
TrafficCarModel[33] = Baller

TrafficCarPos[34] = <<505.7021, -127.9252, 59.7848>>
TrafficCarQuatX[34] = -0.0227
TrafficCarQuatY[34] = -0.0599
TrafficCarQuatZ[34] = 0.9805
TrafficCarQuatW[34] = 0.1858
TrafficCarRecording[34] = 35
TrafficCarStartime[34] = 100941.0000
TrafficCarModel[34] = BjXL

TrafficCarPos[35] = <<510.6351, -130.4515, 59.3621>>
TrafficCarQuatX[35] = 0.0002
TrafficCarQuatY[35] = -0.0620
TrafficCarQuatZ[35] = 0.9802
TrafficCarQuatW[35] = 0.1878
TrafficCarRecording[35] = 36
TrafficCarStartime[35] = 101139.0000
TrafficCarModel[35] = banshee

TrafficCarPos[36] = <<513.6044, -124.3921, 60.1873>>
TrafficCarQuatX[36] = 0.0026
TrafficCarQuatY[36] = -0.0670
TrafficCarQuatZ[36] = 0.9725
TrafficCarQuatW[36] = 0.2230
TrafficCarRecording[36] = 37
TrafficCarStartime[36] = 101337.0000
TrafficCarModel[36] = asterope

TrafficCarPos[37] = <<484.3174, -155.8481, 56.6016>>
TrafficCarQuatX[37] = 0.0008
TrafficCarQuatY[37] = -0.1155
TrafficCarQuatZ[37] = 0.7595
TrafficCarQuatW[37] = -0.6402
TrafficCarRecording[37] = 38
TrafficCarStartime[37] = 101865.0000
TrafficCarModel[37] = Baller

TrafficCarPos[38] = <<515.3390, -244.6399, 48.6464>>
TrafficCarQuatX[38] = -0.0232
TrafficCarQuatY[38] = -0.0304
TrafficCarQuatZ[38] = 0.9764
TrafficCarQuatW[38] = 0.2127
TrafficCarRecording[38] = 39
TrafficCarStartime[38] = 103845.0000
TrafficCarModel[38] = Baller

TrafficCarPos[39] = <<508.6511, -280.4524, 46.7813>>
TrafficCarQuatX[39] = 0.0219
TrafficCarQuatY[39] = 0.0130
TrafficCarQuatZ[39] = -0.2077
TrafficCarQuatW[39] = 0.9779
TrafficCarRecording[39] = 40
TrafficCarStartime[39] = 105165.0000
TrafficCarModel[39] = BUS

TrafficCarPos[40] = <<496.9088, -318.2034, 44.9216>>
TrafficCarQuatX[40] = 0.0249
TrafficCarQuatY[40] = 0.0114
TrafficCarQuatZ[40] = 0.5613
TrafficCarQuatW[40] = 0.8271
TrafficCarRecording[40] = 41
TrafficCarStartime[40] = 105957.0000
TrafficCarModel[40] = asterope

TrafficCarPos[41] = <<482.1582, -326.7783, 45.4484>>
TrafficCarQuatX[41] = 0.0144
TrafficCarQuatY[41] = -0.0290
TrafficCarQuatZ[41] = 0.8114
TrafficCarQuatW[41] = -0.5836
TrafficCarRecording[41] = 42
TrafficCarStartime[41] = 106287.0000
TrafficCarModel[41] = asterope

TrafficCarPos[42] = <<464.0194, -319.4027, 47.2238>>
TrafficCarQuatX[42] = 0.0314
TrafficCarQuatY[42] = -0.0059
TrafficCarQuatZ[42] = 0.9494
TrafficCarQuatW[42] = 0.3125
TrafficCarRecording[42] = 43
TrafficCarStartime[42] = 106353.0000
TrafficCarModel[42] = Baller

TrafficCarPos[43] = <<459.4142, -337.1878, 47.1687>>
TrafficCarQuatX[43] = 0.0061
TrafficCarQuatY[43] = 0.0760
TrafficCarQuatZ[43] = -0.2966
TrafficCarQuatW[43] = 0.9520
TrafficCarRecording[43] = 44
TrafficCarStartime[43] = 106815.0000
TrafficCarModel[43] = baller2

TrafficCarPos[44] = <<386.0724, -405.9861, 46.3795>>
TrafficCarQuatX[44] = 0.0131
TrafficCarQuatY[44] = -0.0083
TrafficCarQuatZ[44] = -0.5920
TrafficCarQuatW[44] = 0.8058
TrafficCarRecording[44] = 45
TrafficCarStartime[44] = 109389.0000
TrafficCarModel[44] = DUMMY_MODEL_FOR_SCRIPT //Baller

TrafficCarPos[45] = <<344.1942, -423.8685, 44.2940>>
TrafficCarQuatX[45] = 0.0281
TrafficCarQuatY[45] = -0.0016
TrafficCarQuatZ[45] = -0.3989
TrafficCarQuatW[45] = 0.9166
TrafficCarRecording[45] = 46
TrafficCarStartime[45] = 110643.0000
TrafficCarModel[45] = DUMMY_MODEL_FOR_SCRIPT //Vader

TrafficCarPos[46] = <<295.9982, -449.7546, 42.9940>>
TrafficCarQuatX[46] = -0.0033
TrafficCarQuatY[46] = -0.0207
TrafficCarQuatZ[46] = 0.9991
TrafficCarQuatW[46] = 0.0380
TrafficCarRecording[46] = 47
TrafficCarStartime[46] = 113019.0000
TrafficCarModel[46] = DUMMY_MODEL_FOR_SCRIPT //asterope

SetPieceCarPos[0] = <<636.0844, 221.4270, 98.7137>>
SetPieceCarQuatX[0] = 0.0558
SetPieceCarQuatY[0] = 0.0284
SetPieceCarQuatZ[0] = 0.6062
SetPieceCarQuatW[0] = 0.7928
SetPieceCarRecording[0] = 499
SetPieceCarStartime[0] = 900.0000  //500.0000
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = police3
SetPieceCarID[0] = mvPoliceCars[1].mVehicle

SetPieceCarPos[1] = <<891.8881, 426.6213, 119.3326>>
SetPieceCarQuatX[1] = -0.0052
SetPieceCarQuatY[1] = -0.0170
SetPieceCarQuatZ[1] = 0.9645
SetPieceCarQuatW[1] = 0.2634
SetPieceCarRecording[1] = 600
SetPieceCarStartime[1] = 35000.0 //37000.0000
SetPieceCarRecordingSpeed[1] = 1.0000
SetPieceCarModel[1] = Pounder

SetPieceCarPos[2] = <<545.7987, 195.0462, 100.8242>>
SetPieceCarQuatX[2] = 0.0227
SetPieceCarQuatY[2] = -0.0209
SetPieceCarQuatZ[2] = -0.2162
SetPieceCarQuatW[2] = 0.9759
SetPieceCarRecording[2] = 500
SetPieceCarStartime[2] = 40200.0000  //40000.0000
SetPieceCarRecordingSpeed[2] = 1.0000
SetPieceCarModel[2] = police3
SetPieceCarID[2] = mvPoliceCars[0].mVehicle

SetPieceCarPos[3] = <<1151.9595, 370.9727, 91.3383>>
SetPieceCarQuatX[3] = -0.0041
SetPieceCarQuatY[3] = -0.0063
SetPieceCarQuatZ[3] = 0.3161
SetPieceCarQuatW[3] = 0.9487
SetPieceCarRecording[3] = 601
SetPieceCarStartime[3] = 57000.0000
SetPieceCarRecordingSpeed[3] = 1.0000
SetPieceCarModel[3] = scrap

SetPieceCarPos[4] = <<649.6518, -14.6478, 82.1775>>
SetPieceCarQuatX[4] = -0.0505
SetPieceCarQuatY[4] = -0.0332
SetPieceCarQuatZ[4] = 0.9261
SetPieceCarQuatW[4] = -0.3725
SetPieceCarRecording[4] = 602
SetPieceCarStartime[4] = 92000.0000
SetPieceCarRecordingSpeed[4] = 1.0000
SetPieceCarModel[4] = SURFER

SetPieceCarPos[5] = <<187.4046, -359.7074, 43.5742>>
SetPieceCarQuatX[5] = 0.0259
SetPieceCarQuatY[5] = 0.0031
SetPieceCarQuatZ[5] = -0.3841
SetPieceCarQuatW[5] = 0.9229
SetPieceCarRecording[5] = 501
SetPieceCarStartime[5] = 104000.0000
SetPieceCarRecordingSpeed[5] = 1.0000
SetPieceCarModel[5] = police3
SetPieceCarID[5] = mvPoliceCars[2].mVehicle

ENDPROC

PROC DO_ANNOYING_SUBTITLE_SWITCH()
	
	TEXT_LABEL_23 rootPoppy = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
	TEXT_LABEL_23 labelPoppy
	
	IF NOT IS_STRING_NULL_OR_EMPTY(rootPoppy)
		IF ARE_STRINGS_EQUAL(rootPoppy,"PAP3A_ARREST")	
		AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 0	
			IF iSubSwitch = 0
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPoppyPed.mPed) > 50
					iSubsPoppy = ENUM_TO_INT(DO_NOT_DISPLAY_SUBTITLES)
					CPRINTLN(DEBUG_MISSION, "<CONVO> LEFT AREA SET SUBS OFF")
					iSubSwitch = 1
					iRetrigger = 1
				ENDIF
			ELIF iSubSwitch = 1
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPoppyPed.mPed) <= 50
					iSubsPoppy = ENUM_TO_INT(DISPLAY_SUBTITLES)
					CPRINTLN(DEBUG_MISSION, "<CONVO> ENTER AREA SET SUBS ON")
					iSubSwitch = 0
					iRetrigger = 1
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF iRetrigger = 1
		labelPoppy = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		IF NOT IS_STRING_NULL_OR_EMPTY(labelPoppy)
			CPRINTLN(DEBUG_MISSION, "<CONVO> LABEL IS NOT NULL")
			IF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_1")	
				labelRetrig = "PAP3A_ARREST_2"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_2")	
				labelRetrig = "PAP3A_ARREST_4"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_3")	
				labelRetrig = "PAP3A_ARREST_4"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_4")	
				labelRetrig = "PAP3A_ARREST_6"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_5")	
				labelRetrig = "PAP3A_ARREST_6"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_6")	
				labelRetrig = "PAP3A_ARREST_8"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_7")	
				labelRetrig = "PAP3A_ARREST_8"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_8")	
				labelRetrig = "PAP3A_ARREST_10"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_9")	
				labelRetrig = "PAP3A_ARREST_10"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_10")	
				labelRetrig = "PAP3A_ARREST_11"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_11")	
				labelRetrig = "PAP3A_ARREST_13"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_12")	
				labelRetrig = "PAP3A_ARREST_13"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_13")	
				labelRetrig = "PAP3A_ARREST_15"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_14")	
				labelRetrig = "PAP3A_ARREST_15"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_15")	
				labelRetrig = "PAP3A_ARREST_17"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_16")	
				labelRetrig = "PAP3A_ARREST_17"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_17")	
				labelRetrig = "PAP3A_ARREST_19"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_18")	
				labelRetrig = "PAP3A_ARREST_19"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_19")	
				labelRetrig = "PAP3A_ARREST_21"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_20")
				labelRetrig = "PAP3A_ARREST_21"
			ELIF ARE_STRINGS_EQUAL(labelPoppy,"PAP3A_ARREST_21")
				iRetrigger = 4
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION()
			CPRINTLN(DEBUG_MISSION, "<CONVO> KILL")
			iRetrigger = 2
		ELSE
			CPRINTLN(DEBUG_MISSION, "<CONVO> CHECKED LABEL IS NULL OR EMPTY")
		ENDIF
	ELIF iRetrigger = 2
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
			CPRINTLN(DEBUG_MISSION, "<CONVO> LINE FINISHED AFTER KILL")
			IF NOT IS_STRING_NULL_OR_EMPTY(labelRetrig)
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "PAP3AAU", "PAP3A_ARREST", labelRetrig, CONV_PRIORITY_HIGH,INT_TO_ENUM(enumSubtitlesState,iSubsPoppy))
					CPRINTLN(DEBUG_MISSION, "<CONVO> RETRIGGER")
					iRetrigger = 3
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "<CONVO> RETRIGGER LABEL IS NULL - BAIL")
				iRetrigger = 3
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DO_AMBIENT_CASINO_STUFF()

	SWITCH iSeqCasino
		
		CASE 0
			IF fTimePosInRec > 60000	
				REQUEST_MODEL(STRETCH)
				REQUEST_MODEL(S_M_Y_VALET_01)
				iSeqCasino = 1
			ENDIF
		BREAK	
		
		CASE 1
			IF HAS_MODEL_LOADED(STRETCH)
			AND HAS_MODEL_LOADED(S_M_Y_VALET_01)	
				vehLimo = CREATE_VEHICLE(STRETCH,<<922.4766, 45.2672, 79.7644>>, 329.2225)
				pedValet = CREATE_PED(PEDTYPE_MISSION,S_M_Y_VALET_01,<<929.0692, 51.5874, 79.9063>>, 57.9660)  
				iSeqCasino = 2
			ENDIF
		BREAK	
		
		CASE 2
			IF IS_ENTITY_ALIVE(pedValet)
				IF IS_ENTITY_ALIVE(mpPoppyPed.mPed)	
					IF IS_ENTITY_IN_RANGE_ENTITY(pedValet,mpPoppyPed.mPed,100) 
						iShockingEvent = ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MAD_DRIVER, mpPoppyPed.mPed)
						SET_PED_CAN_EVASIVE_DIVE(pedValet,TRUE)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
						iSeqCasino = 3
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		CASE 3
			IF IS_ENTITY_ALIVE(pedValet)
				IF IS_ENTITY_ALIVE(mpPoppyPed.mPed)	
					IF IS_ENTITY_IN_RANGE_ENTITY(pedValet,mpPoppyPed.mPed,30) 
						TASK_SHOCKING_EVENT_REACT(pedValet,iShockingEvent)
						PLAY_PED_AMBIENT_SPEECH_NATIVE(pedValet, "GENERIC_SHOCKED_HIGH","SPEECH_PARAMS_STANDARD")
						iSeqCasino = 4
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		CASE 4
			IF IS_ENTITY_ALIVE(pedValet)
				IF IS_ENTITY_ALIVE(mpPoppyPed.mPed)	
					IF NOT IS_ANY_SPEECH_PLAYING(pedValet)
						PLAY_PED_AMBIENT_SPEECH_NATIVE(pedValet, "GENERIC_SHOCKED_HIGH","SPEECH_PARAMS_STANDARD")
					ENDIF
					IF fTimePosInRec > 90000
						iSeqCasino = 5
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		CASE 5
			SAFE_RELEASE_PED(pedValet)
			SAFE_RELEASE_VEHICLE(vehLimo)
			SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_VALET_01)
		BREAK	
	
	ENDSWITCH		
			

ENDPROC

/// PURPOSE: Creates police helicopter that hovers over Poppy's car
PROC CreatePoliceChopper()

	IF NOT DOES_ENTITY_EXIST(vehPoliceChopper)
		IF NOT HAS_MODEL_LOADED(POLMAV)
			REQUEST_MODEL(POLMAV)
		ELSE
			vehPoliceChopper = CREATE_VEHICLE(POLMAV,<< 351.3166, -589.8016, 73.1664 >>, 336.3177)
			//vehPoliceChopper = CREATE_VEHICLE(POLMAV,<<611.8760, 234.6003, 130.0>>, 73.2507)
			pedChopperPilot = CREATE_PED_INSIDE_VEHICLE(vehPoliceChopper,PEDTYPE_COP,S_M_Y_Cop_01)
			SET_VEHICLE_LIVERY(vehPoliceChopper,0)
			SET_HELI_BLADES_FULL_SPEED(vehPoliceChopper)
			SET_VEHICLE_ENGINE_ON(vehPoliceChopper,TRUE,TRUE)
			SET_VEHICLE_SEARCHLIGHT(vehPoliceChopper,TRUE)
			SET_HELI_TURBULENCE_SCALAR(vehPoliceChopper,0.2)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehPoliceChopper,TRUE)
			IF IS_PED_UNINJURED(mpPoppyPed.mPed)	
				//TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,GET_ENTITY_COORDS(mpPoppyPed.mPed),MISSION_POLICE_BEHAVIOUR,100,60.0,-1,40,35)
				TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,<<0,50,60>>,MISSION_POLICE_BEHAVIOUR,30.0,60.0,-1,60,55)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TurnOffRoads()

	//BLOCK OFF ROAD AT CRASH SITE - DISABLE NODES
	SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, FALSE)
	//JUNCTION NEAR PARKED POLICE
	SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, FALSE)
	//JUNCTION AT START OF CHASE
	SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, FALSE)
	//POLICE ROAD BLOCK
	SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, FALSE)

ENDPROC

/// PURPOSE: Create scripted traffic
PROC CreateScriptedTraffic()
	
	FLOAT fTimeOffset
	
	IF fPlaybackSpeed > 1.2 
		fTimeOffset = 0
	ENDIF
	IF fPlaybackSpeed <= 1.2 
	AND fPlaybackSpeed > 1.1 
		fTimeOffset = 700
	ENDIF
	IF fPlaybackSpeed <= 1.1 
	AND fPlaybackSpeed > 1.0 
		fTimeOffset = 1400
	ENDIF
	IF fPlaybackSpeed <= 1.0 
	AND fPlaybackSpeed > 0.9 
		fTimeOffset = 2100
	ENDIF
	IF fPlaybackSpeed <= 0.9 
	AND fPlaybackSpeed > 0.8 
		fTimeOffset = 2800
	ENDIF
	IF fPlaybackSpeed <= 0.8 
		fTimeOffset = 3500
	ENDIF
	
	IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
		//IF CAN_CREATE_RANDOM_DRIVER()	
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
				IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 22000.0 + fTimeOffset     //16637.2305
					IF iRandomTrafficSeq = 0
						REQUEST_MODEL(TAXI)
						REQUEST_MODEL(POUNDER)
						REQUEST_MODEL(FAGGIO2)
						//REQUEST_MODEL(A_M_Y_HIPSTER_01)
						iRandomTrafficSeq = 1
					ELIF iRandomTrafficSeq = 1
						IF HAS_MODEL_LOADED(TAXI)
							IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 26637.2305 + fTimeOffset
								vehTraffic[0] = CREATE_VEHICLE(TAXI,<< 421.3843, -145.6329, 63.5494 >>, 250.1559)
								pedRandomDriver[0] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[0])	
								SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[0],TRUE)
								OPEN_SEQUENCE_TASK(seqTraffic[0])	
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[0],<< 505.4681, -195.9427, 51.1217 >>,12.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
									TASK_VEHICLE_TEMP_ACTION(NULL,vehTraffic[0],TEMPACT_BRAKE,30000)
								CLOSE_SEQUENCE_TASK(seqTraffic[0])
								TASK_PERFORM_SEQUENCE(pedRandomDriver[0],seqTraffic[0])
								vehTraffic[5] = CREATE_VEHICLE(TAXI,<< 654.8176, -27.8639, 80.4690 >>, 132.0047)
								pedRandomDriver[5] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[5])	
								SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[5],TRUE)
								OPEN_SEQUENCE_TASK(seqTraffic[5])	
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[5],<< 510.9024, -117.9431, 60.3864 >>,14.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[5],<< 428.1450, -135.9952, 63.5564 >>,14.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
									TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[5],12.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
								CLOSE_SEQUENCE_TASK(seqTraffic[5])
								TASK_PERFORM_SEQUENCE(pedRandomDriver[5],seqTraffic[5])
								IF HAS_MODEL_LOADED(POUNDER)	
									vehTraffic[7] = CREATE_VEHICLE(POUNDER,<< 501.1890, -312.7863, 44.1513 >>, 67.3206)
									pedRandomDriver[7] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[7])	
									SET_VEHICLE_ON_GROUND_PROPERLY(vehTraffic[7])
									SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[7],TRUE)
						 		ENDIF
								iRandomTrafficSeq = 2
							ENDIF
						ENDIF
					ELIF iRandomTrafficSeq = 2
						IF HAS_MODEL_LOADED(POUNDER)
							IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 29076.3008 + fTimeOffset   //27076.3008 //28576.3008
								/*
								vehTraffic[1] = CREATE_VEHICLE(POUNDER,<< 571.6310, -351.4838, 42.5852 >>, 64.4023)
								pedRandomDriver[1] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[1])	
								SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[1],TRUE)
								OPEN_SEQUENCE_TASK(seqTraffic[1])	
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[1],<< 399.3476, -280.2910, 51.5046 >>,50.0,DRIVINGSTYLE_NORMAL,POUNDER,DRIVINGMODE_PLOUGHTHROUGH,2.0,5.0)
									TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[1],11.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
								CLOSE_SEQUENCE_TASK(seqTraffic[1])
								TASK_PERFORM_SEQUENCE(pedRandomDriver[1],seqTraffic[1])
								*/
								vehTraffic[6] = CREATE_VEHICLE(TAXI,<< 650.6729, -391.0377, 41.1159 >>, 53.9120)
								pedRandomDriver[6] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[6])	
								SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[6],TRUE)
								OPEN_SEQUENCE_TASK(seqTraffic[6])	
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[6],<< 509.6324, -323.0786, 43.2076 >>,16.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS,5.0,5.0)
									TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[6],<< 414.3617, -286.4584, 50.3817 >>,15.0,DRIVINGSTYLE_NORMAL,TAXI,DRIVINGMODE_STOPFORCARS_IGNORELIGHTS,5.0,5.0)
									TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[6],12.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
								CLOSE_SEQUENCE_TASK(seqTraffic[6])
								TASK_PERFORM_SEQUENCE(pedRandomDriver[6],seqTraffic[6])
								//IF CAN_CREATE_RANDOM_DRIVER()		
									IF HAS_MODEL_LOADED(FAGGIO2)
									//AND	HAS_MODEL_LOADED(A_M_Y_HIPSTER_01)	
										vehTraffic[8] = CREATE_VEHICLE(FAGGIO2,<< 555.2825, -337.5017, 42.5302 >>, 63.2771)
										//pedRandomDriver[8] = CREATE_PED_INSIDE_VEHICLE(vehTraffic[8],PEDTYPE_CIVMALE,A_M_Y_HIPSTER_01)	
										pedRandomDriver[8] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[8])
										SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[8],TRUE)
										OPEN_SEQUENCE_TASK(seqTraffic[8])	
											TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[8],<< 511.2531, -274.8205, 46.0294 >>,18.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
											TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[8],<< 518.7288, -188.3010, 52.2624 >>,19.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,5.0,5.0)
											TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[8],15.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
										CLOSE_SEQUENCE_TASK(seqTraffic[8])
										TASK_PERFORM_SEQUENCE(pedRandomDriver[8],seqTraffic[8])
									ENDIF
								//ENDIF
								iRandomTrafficSeq = 3
							ENDIF
						ENDIF
					ELIF iRandomTrafficSeq = 3
						IF HAS_MODEL_LOADED(FAGGIO2)
						//AND	HAS_MODEL_LOADED(A_M_Y_HIPSTER_01)	
							IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 33576.3008 + fTimeOffset
								//IF CAN_CREATE_RANDOM_DRIVER()	
									vehTraffic[3] = CREATE_VEHICLE(FAGGIO2,<< 367.2433, -285.2760, 52.7999 >>, 248.9247)
									vehTraffic[4] = CREATE_VEHICLE(FAGGIO2,<< 380.3846, -290.7169, 52.2115 >>, 249.4217)	
									//pedRandomDriver[3] = CREATE_PED_INSIDE_VEHICLE(vehTraffic[3],PEDTYPE_CIVMALE,A_M_Y_HIPSTER_01)	
									//pedRandomDriver[4] = CREATE_PED_INSIDE_VEHICLE(vehTraffic[4],PEDTYPE_CIVMALE,A_M_Y_HIPSTER_01)	
									pedRandomDriver[3] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[3])
									pedRandomDriver[4] = CREATE_RANDOM_PED_AS_DRIVER(vehTraffic[4])
									SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[3],TRUE)
									SET_ENTITY_LOAD_COLLISION_FLAG(vehTraffic[4],TRUE)
									OPEN_SEQUENCE_TASK(seqTraffic[3])	
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[3],<< 439.2083, -353.8017, 46.3140 >>,50.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,12.0,5.0)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[3],<< 205.5381, -341.2657, 43.0355 >>,50.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,12.0,5.0)
										TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[3],11.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
									CLOSE_SEQUENCE_TASK(seqTraffic[3])
									OPEN_SEQUENCE_TASK(seqTraffic[4])	
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[4],<< 439.2083, -353.8017, 46.3140 >>,50.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,12.0,5.0)
										TASK_VEHICLE_DRIVE_TO_COORD(NULL,vehTraffic[4],<< 205.5381, -341.2657, 43.0355 >>,50.0,DRIVINGSTYLE_NORMAL,FAGGIO2,DRIVINGMODE_PLOUGHTHROUGH,12.0,5.0)
										TASK_VEHICLE_DRIVE_WANDER(NULL,vehTraffic[4],11.0,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
									CLOSE_SEQUENCE_TASK(seqTraffic[4])
									TASK_PERFORM_SEQUENCE(pedRandomDriver[3],seqTraffic[3])
									TASK_PERFORM_SEQUENCE(pedRandomDriver[4],seqTraffic[4])
									iRandomTrafficSeq = 4
								//ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		//ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Sets all models as no longer needed, removes all anim dicts, removes all vehicle recordings etc.
PROC UnloadEverything()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mpPoppyPed.mModel)
	
	//SET_MODEL_AS_NO_LONGER_NEEDED(mvPoppyCar.mModel)
	
	INT i
	
	FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
	
		SET_MODEL_AS_NO_LONGER_NEEDED(mpPolicePed[i].mModel)

	ENDFOR
	
	FOR i = 0 TO NUM_OF_PARKED_CARS - 1
	
		SET_MODEL_AS_NO_LONGER_NEEDED(mvParkedCar[i].mModel)
		
	ENDFOR
	
	FOR i = 0 TO NUM_OF_POLICE_CARS - 1

		SET_MODEL_AS_NO_LONGER_NEEDED(mvPoliceCars[i].mModel)
	
	ENDFOR
			
ENDPROC

/// PURPOSE:
///    Makes the PED passed in flee from the player
/// PARAMS:
///    mPedFlee - Makes this ped flee from the player. 
PROC MAKE_PED_FLEE(PED_INDEX mPedFlee, BOOL OnFoot = FALSE)

	IF IS_PED_UNINJURED(mPedFlee)
		IF OnFoot = FALSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			TASK_SMART_FLEE_PED(mPedFlee, PLAYER_PED_ID(), 200, -1,FALSE)	
		ELSE
			CLEAR_PED_TASKS(mPedFlee)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_AGGRESSIVE,FALSE)
			SET_PED_COMBAT_ATTRIBUTES(mPedFlee,CA_ALWAYS_FLEE,TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_VEHICLE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_PREFER_PAVEMENTS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_USE_COVER, FALSE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_LOOK_FOR_CROWDS, TRUE)
			SET_PED_FLEE_ATTRIBUTES(mPedFlee, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
			TASK_SMART_FLEE_PED(mPedFlee, PLAYER_PED_ID(), 200, -1,TRUE)	
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

/// PURPOSE: Cleanup that needs to be done as soon as mission is passed or failed. Blip removal etc
PROC MissionCleanup(bool bClearText = TRUE)

	SAFE_REMOVE_BLIP(biMissionBlip)
	SAFE_REMOVE_BLIP(blipPoppy)
	SAFE_REMOVE_BLIP(blipPoppyCar)

	INT i
	
	FOR i = 0 TO NUM_OF_POLICE_PEDS - 1	
		IF IS_ENTITY_ALIVE(mpPolicePed[i].mPed)
			//IF i <> 0
			//AND i <> 1
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[i].mPed,FALSE)
				SET_PED_AS_COP(mpPolicePed[i].mPed)
			//ELSE
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[i].mPed,TRUE)
			//ENDIF
		ENDIF		
	ENDFOR
	
	IF IS_PED_UNINJURED(mpPoppyPed.mPed)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPoppyPed.mPed,FALSE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(wgUberChase)
			DELETE_WIDGET_GROUP(wgUberChase)
		ENDIF
	#ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),FALSE)
	ENDIF

	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
	ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
		
	IF bClearText = TRUE // clear all text
		CLEAR_PRINTS()
	ENDIF
	CLEAR_HELP(TRUE)
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Script_Cleanup(BOOL bClearText = TRUE)
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MissionCleanup(bClearText)
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
		
		TRIGGER_MUSIC_EVENT("PAP3_FAIL")
		
		IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_POLICE_CHASE")
			STOP_AUDIO_SCENE("PAPARAZZO_3A_POLICE_CHASE")
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_PHOTO_SCENE")
			STOP_AUDIO_SCENE("PAPARAZZO_3A_PHOTO_SCENE")
		ENDIF
		
		UnloadEverything()
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		SAFE_RELEASE_VEHICLE(vehTraffic[0])
		SAFE_RELEASE_VEHICLE(vehTraffic[1])
		SAFE_RELEASE_VEHICLE(vehTraffic[2])
		SAFE_RELEASE_VEHICLE(vehTraffic[3])
		SAFE_RELEASE_VEHICLE(vehTraffic[4])
		SAFE_RELEASE_VEHICLE(vehTraffic[5])
		SAFE_RELEASE_VEHICLE(vehTraffic[6])
		SAFE_RELEASE_VEHICLE(vehTraffic[7])
		SAFE_RELEASE_VEHICLE(vehTraffic[8])
		SAFE_RELEASE_VEHICLE(vehTraffic[9])
		
		SAFE_RELEASE_PED(pedRandomDriver[0])
		SAFE_RELEASE_PED(pedRandomDriver[1])
		SAFE_RELEASE_PED(pedRandomDriver[2])
		SAFE_RELEASE_PED(pedRandomDriver[3])
		SAFE_RELEASE_PED(pedRandomDriver[4])
		SAFE_RELEASE_PED(pedRandomDriver[5])
		SAFE_RELEASE_PED(pedRandomDriver[6])
		SAFE_RELEASE_PED(pedRandomDriver[7])
		SAFE_RELEASE_PED(pedRandomDriver[8])
		SAFE_RELEASE_PED(pedRandomDriver[9])
		
		SAFE_RELEASE_PED(mpPoppyPed.mPed)
	
		//SAFE_RELEASE_VEHICLE(mvPoppyCar.mVehicle)
		
		SAFE_RELEASE_PED(pedChopperPilot)
		SAFE_RELEASE_VEHICLE(vehPoliceChopper)
		
		IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
			FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
		ENDIF
		
		INT i
		
		FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
			SAFE_RELEASE_PED(mpPolicePed[i].mPed)
		ENDFOR
		
		FOR i = 0 TO NUM_OF_PARKED_CARS - 1
			SAFE_RELEASE_VEHICLE(mvParkedCar[i].mVehicle)
		ENDFOR
		
		FOR i = 0 TO NUM_OF_POLICE_CARS - 1
			SAFE_RELEASE_VEHICLE(mvPoliceCars[i].mVehicle)
		ENDFOR
		
		//Reset any speed modifier
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			MODIFY_VEHICLE_TOP_SPEED(vehPlayer,0)
		ENDIF
		
		SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, TRUE)
		SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, TRUE)
		SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, TRUE)
		SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, TRUE)
		
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		CLEANUP_UBER_PLAYBACK()		
		
	ENDIF
	
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE) //Cleanup the scene created by the launcher
	
	IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)
		SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(mvPoppyCar.mVehicle,FALSE)
		SET_VEHICLE_EXTENDED_REMOVAL_RANGE(mvPoppyCar.mVehicle,100)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION, "SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(mvPoppyCar.mVehicle,FALSE)")
			CPRINTLN(DEBUG_MISSION, "SET_VEHICLE_EXTENDED_REMOVAL_RANGE(mvPoppyCar.mVehicle,100)")
		#ENDIF
	ENDIF
	
	REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES(FALSE)
	
	DISABLE_TAXI_HAILING(FALSE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
PROC Script_Passed()
	IF bReplay = FALSE	
		IF bPhotoInCuffs = TRUE
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3A_PHOTO_IN_CUFFS) 
		ENDIF
	ENDIF
	IF bStatsFarFromPoppy = TRUE
	OR bReplay = TRUE		
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3A_FAR_FROM_POPPY) 
	ENDIF
//	CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_UNLOGGED_SMALL_ACTION, 500) // Initially requested on B*1237043 - Removed on request as B*1317037
	MissionCleanup()
	Random_Character_Passed(CP_RAND_C_PAP3)
	Script_Cleanup()
ENDPROC

/// PURPOSE: Sets the new mission stage and initialises the substate
PROC SetStage(MISSION_STAGE eNewStage)
	bMainObjectiveDisplayed = FALSE
	missionStage = eNewStage // Setup new mission state
	eSubStage = SS_SETUP
ENDPROC

/// PURPOSE:
///  Sets the fail reason and mission stage to be fail fade out. 
PROC SET_FAIL_REASON(FAIL_REASON eNewFailReason)

	eFailReason = eNewFailReason
	SETSTAGE(MS_MISSION_FAILED)

ENDPROC

/// PURPOSE:
///   Checks if player is inside the police barricade area
/// RETURNS:
///    True - If player is inside the area, false otherwise
FUNC BOOL IS_PLAYER_INSIDE_ROAD_BLOCK()

	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<834.418884,-88.170197,78.144989>>, <<802.457581,-62.110813,87.644989>>, 47.750000)
		RETURN TRUE	
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<767.238342,-101.369659,75.284279>>, <<784.822693,-72.584816,83.212868>>, 20.000000)
		RETURN TRUE	
	ENDIF
	//IF IS_POINT_IN_POLY_2D(tpPoliceBlock, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	//	RETURN TRUE
	//ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Resets the gameplay camera behind the player
PROC ResetCamBehindPlayer()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
ENDPROC

/// PURPOSE: creates ped and sets heading
PROC CreateMissionPed(MISSION_PED &mMissionPed, BOOL bUnloadModel = TRUE)
	if HAS_MODEL_LOADED(mMissionPed.mModel)
		mMissionPed.mPed = CREATE_PED(PEDTYPE_MISSION, mMissionPed.mModel, mMissionPed.vStartPos)
		if IS_ENTITY_ALIVE(mMissionPed.mPed)
			SET_ENTITY_HEADING(mMissionPed.mPed, mMissionPed.fStartHeading)
		ENDIF
		if bUnloadModel = TRUE
			//SET_MODEL_AS_NO_LONGER_NEEDED(mMissionPed.mModel)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: creates ped inside a vehicle
PROC CreateMissionPedInVehicle(MISSION_PED &mMissionPed, MISSION_VEHICLE &mvMissionVec, PED_TYPE ptPedType, BOOL bUnloadModel = TRUE, VEHICLE_SEAT SeatPosition = VS_DRIVER)
	if HAS_MODEL_LOADED(mMissionPed.mModel)
		mMissionPed.mPed = CREATE_PED_INSIDE_VEHICLE(mvMissionVec.mVehicle,ptPedType, mMissionPed.mModel, SeatPosition)
		if bUnloadModel = TRUE
			//SET_MODEL_AS_NO_LONGER_NEEDED(mMissionPed.mModel)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: creates vehicle and sets heading and colours
PROC CreateMissionVehicle(MISSION_VEHICLE &mMissionVehicle, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionVehicle.mModel)
		mMissionVehicle.mVehicle = CREATE_VEHICLE(mMissionVehicle.mModel, mMissionVehicle.vStartPos, mMissionVehicle.fStartHeading)
	ENDIF

	if bRemoveModel = TRUE
		//SET_MODEL_AS_NO_LONGER_NEEDED(mMissionVehicle.mModel)
	ENDIF
ENDPROC

/// PURPOSE: creates props and sets heading
PROC CreateMissionProp(MISSION_PROP &mMissionProp, BOOL bRemoveModel = TRUE)
	if HAS_MODEL_LOADED(mMissionProp.mModel)
		mMissionProp.mObject = CREATE_OBJECT(mMissionProp.mModel, mMissionProp.vStartPos)
		if DOES_ENTITY_EXIST(mMissionProp.mObject)
			SET_ENTITY_HEADING(mMissionProp.mObject,mMissionProp.fStartHeading)
			//SET_ENTITY_LOAD_COLLISION_FLAG(mMissionProp.mObject,TRUE)
			//PLACE_OBJECT_ON_GROUND_PROPERLY(mMissionProp.mObject)
			ACTIVATE_PHYSICS(mMissionProp.mObject)
		ENDIF
	ENDIF

	if bRemoveModel = TRUE
		//SET_MODEL_AS_NO_LONGER_NEEDED(mMissionProp.mModel)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is unarmed - Including objects
///    Used to see if PEDS should attack player
/// RETURNS:
///    TRUE - If player is unarmed, false otherwise
FUNC BOOL IS_PLAYER_UNARMED()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED 
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT //CHECK FOR MOBILE PHONE AS THIS WILL TRIGGER
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE: Checks if a point is onscreen
FUNC BOOL CAN_PLAYER_SEE_POINT(VECTOR vPos, FLOAT fRadius = 5.0, INT distance = 120)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	      IF GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(PLAYER_PED_ID()), vPos) <= distance
	            IF IS_SPHERE_VISIBLE(vPos, fRadius)      
	                  RETURN TRUE
	            ENDIF
	      ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks if a ped is in the middle of the screen
FUNC BOOL MONITOR_PHOTO(PED_INDEX _Ped)
	
	IF IS_ENTITY_ALIVE(_Ped)
		FLOAT fScreenX = 0.0
		FLOAT fScreenY = 0.0
		VECTOR vPedPos = GET_ENTITY_COORDS(_Ped)

		IF CAN_PLAYER_SEE_POINT(vPedPos,3.0, 500)
			GET_SCREEN_COORD_FROM_WORLD_COORD(vPedPos, fScreenX, fScreenY)

			IF fScreenX > 0.2 
			AND fScreenX <0.8 
			AND fScreenY > 0.2 
			AND fScreenY <0.8
				RETURN TRUE
				//the person is in the center of the screen 
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_NEAR_MIDDLE_OF_SCREEN(PED_INDEX ped)
	
	FLOAT fMinScreen = 0.2
	FLOAT fMaxScreen = 0.8

	IF IS_PED_UNINJURED(ped)
		INT i = GET_PED_BONE_INDEX(ped, BONETAG_SPINE)
		FLOAT fScreenX
		FLOAT fScreenY
		IF i <> -1
			VECTOR pos = GET_WORLD_POSITION_OF_ENTITY_BONE(ped,i)			

			GET_SCREEN_COORD_FROM_WORLD_COORD(pos, fScreenX, fScreenY)
			
			IF fScreenX > fMinScreen
			AND fScreenX < fMaxScreen
			AND fScreenY > fMinScreen
			AND fScreenY < fMaxScreen
				CPRINTLN(DEBUG_MISSION, "TK POPPY SPINE NEAR MIDDLE OF THE SCREEN")
				RETURN TRUE
			ENDIF							

		ENDIF
	ENDIF
	CPRINTLN(DEBUG_MISSION, "TK POPPY SPINE NOT NEAR MIDDLE OF THE SCREEN")
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_PHOTO_OK()
	
	IF iFramesCheckingPhotoOk < 20
		IF IS_PED_NEAR_MIDDLE_OF_SCREEN(mpPoppyPed.mPed)		
			IF IS_ENTITY_ON_SCREEN(mpPoppyPed.mPed)
			AND IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(mpPoppyPed.mPed),1)		
				IF NOT IS_ENTITY_OCCLUDED(mpPoppyPed.mPed)
					IF GET_FOCUS_PED_ON_SCREEN(100,BONETAG_HEAD,0.42,0.26,0.01,50,0.2) = mpPoppyPed.mPed
					OR GET_FOCUS_PED_ON_SCREEN(100,BONETAG_HEAD,0.42,0.26,0.01,50,0.2) = mpPolicePed[0].mPed
						IF NOT IS_ENTITY_IN_RANGE_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),50)
							bTooFarAway = TRUE
						ELSE
							++iPhotoScore
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		++iFramesCheckingPhotoOk
	ENDIF
	
	IF iPhotoScore > 5
		RETURN TRUE
	ELSE	
		RETURN FALSE
	ENDIF
	
ENDFUNC

#IF IS_DEBUG_BUILD
	
	BOOL bDebug_PrintPlayerVehicleInfo = FALSE

   	PROC SETUP_MISSION_WIDGET()
	
       wgUberChase = START_WIDGET_GROUP("ben WIDGETS")

       ADD_WIDGET_BOOL("Print player's current vehicle info, pos, quat and heading", bDebug_PrintPlayerVehicleInfo)
  
       SET_UBER_PARENT_WIDGET_GROUP(wgUberChase)

       STOP_WIDGET_GROUP()
    ENDPROC
#ENDIF

/// PURPOSE: initialises mission variables
PROC InitVariables()
	PRINTSTRING("init variables ************************")
	
	vPitchersJunc = <<260.3616, 339.5469, 104.5709>>
	vPawnJunc = <<421.7929, 295.6490, 102.0579>>
	
	//INFORM_MISSION_STATS_OF_MISSION_START_PAPARAZZO_3A()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
	REQUEST_MODEL(police3)
	REQUEST_MODEL(S_M_Y_Cop_01)
	REQUEST_MODEL(U_F_Y_PoppyMich)
	REQUEST_MODEL(COGCABRIO)
	
	REQUEST_VEHICLE_RECORDING(300,"PAP3A2")
	
	ADD_RELATIONSHIP_GROUP("SecurityGroup", relCopGroup)
	ADD_RELATIONSHIP_GROUP("PoppyGroup", relPoppy)
	
	CLEAR_AREA_OF_VEHICLES(vPitchersJunc,50,TRUE,TRUE)
	CLEAR_AREA_OF_VEHICLES(vPawnJunc,50,TRUE,TRUE)
	SET_ROADS_IN_ANGLED_AREA(<<234.214111,350.285187,113.387581>>, <<672.335144,212.360733,90.183990>>, 47.250000,FALSE,FALSE)   //road at the start
	
	INIT_ARRAYS()
	
	bMainObjectiveDisplayed = 	FALSE
	bPoliceChaseCreated		= 	FALSE
	bCopCrashed 			= 	FALSE
	bPoppyHasCrashed		=	FALSE
	b_Correct_Pic_Taken 	= 	FALSE
	bPhotoSent				= 	FALSE
	bConversationActive		=	FALSE
	bPoliceAttacking		=	FALSE
	bCopsAreLeaving			=	FALSE
	bSimpleHelp				= 	FALSE
	bDoRollingStart 		= 	FALSE
	bPoppyCarSmashed 		=   FALSE
	bCarStopping            =   FALSE
	//bBadPicSent             =   FALSE
	bDoingRollingReplay = FALSE
	bPoliceAttacking = FALSE
	iWanted = 0
	
	iDialogueToPlay			=	0
	
	bPlayerLeftArea = FALSE
	bDoneFakeStart = FALSE 
	bCrashCutDone = FALSE
	bCrashSoundPlayed = FALSE
	bPrintNonSimpleHelp = FALSE
	/*
	//BLOCK OFF ROAD AT CRASH SITE - DISABLE NODES
	SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, FALSE)
	//JUNCTION NEAR PARKED POLICE
	SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, FALSE)
	//JUNCTION AT START OF CHASE
	SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, FALSE)
	//POLICE ROAD BLOCK
	SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, FALSE)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(),TRUE)
	ENDIF
	*/
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)
	
	#IF IS_DEBUG_BUILD
		SETUP_MISSION_WIDGET()
		SET_UBER_PARENT_WIDGET_GROUP(wgUberChase)
	#ENDIF
	
	eSubStageRec = SSRP_SETUP //RESET UBER RECORDING STATE
	
	ePoppyState = PS_WAIT_FOR_PLAYER //RESET POPPY STATE
	
	INT i
	
	FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
		eCopState[i] = CS_DRIVING // RESET COP STATE
	ENDFOR
	
	//AREA TEST POLYS
	//SPOTTED AREA
	OPEN_TEST_POLY(tpPoliceBlock)
		ADD_TEST_POLY_VERT(tpPoliceBlock, << 820.2385, -46.3501, 79.5901 >>)
		ADD_TEST_POLY_VERT(tpPoliceBlock, << 796.7191, -82.9061, 79.5978 >>)
		ADD_TEST_POLY_VERT(tpPoliceBlock, << 802.1686, -86.5395, 79.5984 >>)
		ADD_TEST_POLY_VERT(tpPoliceBlock, << 827.0104, -50.6043, 79.5877 >>)
	CLOSE_TEST_POLY(tpPoliceBlock)
	
	//CHASE PLAYERBACK SPEED
	//fPlayBackSpeed = 1.0
	
	CLEAR_AREA_OF_VEHICLES(<< 691.2697, 10.5739, 83.1879 >>,100.0,FALSE) // CLEAR FIRST JUNCTION OF VEHICLES

	//REQUEST_ADDITIONAL_TEXT("PAP3AAU", MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("PAP3",MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	//USED FOR CAMERA HELP
	
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_1) = TRUE	
	OR IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_3B)
		bSimpleHelp = TRUE
	ELSE
		bSimpleHelp = FALSE
	ENDIF
	
	fMaxDist = 170.0
	
	// other requests here

	#IF IS_DEBUG_BUILD // stage skipping
		bFinishedSkipping = TRUE
	
		mSkipMenu[0].sTxtLabel = "MS_INITAL_PHONE"  
		mSkipMenu[Z_SKIP_PHONECALL].sTxtLabel = "MS_GO_TO_LOCATION"  
		mSkipMenu[Z_SKIP_AT_POPPY_LOCATION].sTxtLabel = "MS_CHASE_POPPY"
		mSkipMenu[3].sTxtLabel = "MS_GO_TO_CRASH"
		mSkipMenu[Z_SKIP_AT_CRASH_LOCATION].sTxtLabel = "MS_TAKE_PHOTO"
		mSkipMenu[5].sTxtLabel = "MS_END_PHONE_CALL"
		
	#ENDIF

ENDPROC

/// PURPOSE:
///    Deletes everything regardless
PROC DELETE_EVERYTHING()

	CLEANUP_UBER_PLAYBACK()

	REMOVE_RELATIONSHIP_GROUP(relCopGroup)
	REMOVE_RELATIONSHIP_GROUP(relPoppy)

	SAFE_DELETE_PED(mpPoppyPed.mPed)
	
	INT i
	
	FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
		SAFE_DELETE_PED(mpPolicePed[i].mPed)

	ENDFOR
	
	FOR i = 0 TO NUM_OF_POLICE_CARS - 1
		SAFE_DELETE_VEHICLE(mvPoliceCars[i].mVehicle)
	ENDFOR
	
	SAFE_DELETE_VEHICLE(mvPoppyCar.mVehicle)
	
	FOR i = 0 TO NUM_OF_PARKED_CARS - 1
		SAFE_DELETE_VEHICLE(mvParkedCar[i].mVehicle)
	ENDFOR
	
	FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
		SAFE_DELETE_OBJECT(mpsChaseProps[i].mObject)
	ENDFOR
	
	REMOVE_FORCED_OBJECT(<< 634.4785, -68.8406, 74.2911 >>,5,PROP_BARRIER_WORK04A)
	REMOVE_FORCED_OBJECT(<< 627.1672, -73.8742, 73.3072 >>,5,PROP_BARRIER_WORK04A)
	REMOVE_FORCED_OBJECT(<< 653.2055, -96.2269, 73.4953 >>,5,PROP_BARRIER_WORK04A)
	REMOVE_FORCED_OBJECT(<< 657.0881, -91.6722, 73.5133 >>,5,PROP_BARRIER_WORK04A)
	
	SAFE_REMOVE_BLIP(biMissionBlip)
	
	SAFE_DELETE_PED(pedRandomDriver[0])
	SAFE_DELETE_PED(pedRandomDriver[1])
	SAFE_DELETE_PED(pedRandomDriver[2])
	SAFE_DELETE_PED(pedRandomDriver[3])
	SAFE_DELETE_PED(pedRandomDriver[4])
	SAFE_DELETE_PED(pedRandomDriver[5])
	SAFE_DELETE_PED(pedRandomDriver[6])
	SAFE_DELETE_PED(pedRandomDriver[7])
	SAFE_DELETE_PED(pedRandomDriver[8])
	SAFE_DELETE_PED(pedRandomDriver[9])
	
	SAFE_DELETE_VEHICLE(vehTraffic[0])
	SAFE_DELETE_VEHICLE(vehTraffic[1])
	SAFE_DELETE_VEHICLE(vehTraffic[2])
	SAFE_DELETE_VEHICLE(vehTraffic[3])
	SAFE_DELETE_VEHICLE(vehTraffic[4])
	SAFE_DELETE_VEHICLE(vehTraffic[5])
	SAFE_DELETE_VEHICLE(vehTraffic[6])
	SAFE_DELETE_VEHICLE(vehTraffic[7])
	SAFE_DELETE_VEHICLE(vehTraffic[8])
	SAFE_DELETE_VEHICLE(vehTraffic[9])
	
	SAFE_DELETE_PED(pedChopperPilot)
	SAFE_DELETE_VEHICLE(vehPoliceChopper)
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(wgUberChase)
			DELETE_WIDGET_GROUP(wgUberChase)
		ENDIF
	#ENDIF
	
ENDPROC

/// PURPOSE: Deletes all entities and unloads everything
PROC ResetMission()
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	DISPLAY_HUD(TRUE)
	
	DISPLAY_RADAR(TRUE)
	
	CLEAR_PRINTS()
	
	DELETE_EVERYTHING()
	
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)

	UnloadEverything()
	
	InitVariables()
	
	iRandomTrafficSeq = 0
	
	CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< 408.5291, 139.5021, 100.8066 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),213.0)
	ENDIF
	
	SetStage(MS_INITIAL_PHONE)
	
ENDPROC

////////// DEBUG FUNCTIONS /////////////////////

PROC SET_VEHICLE_RECORDING_PLAYBACK_POSITION(VEHICLE_INDEX thisVehicle, FLOAT positionToSet)

	IF IS_ENTITY_ALIVE(thisVehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicle)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisVehicle, (GET_TIME_POSITION_IN_RECORDING(thisVehicle) * -1 ) + positionToSet )
		ELSE
			SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Playback not going on for Vehicle!")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Vehicle Dead!")
	ENDIF
	
ENDPROC

PROC SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(VEHICLE_INDEX thisVehicle, FLOAT percentage, INT recordingNumber, STRING RecName)

	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingNumber, RecName)
		IF percentage >= 0.0 AND percentage <= 100.00
			SET_VEHICLE_RECORDING_PLAYBACK_POSITION(thisVehicle,  ( GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(recordingNumber, RecName) / 100.00 )  * percentage  )
		#IF IS_DEBUG_BUILD
		ELSE
			SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Supplied Percentage out of range!!")
			PRINTSTRING("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Supplied Percentage out of range!!")
		#ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("Car rec not loaded")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Makes sure Police scene has loaded
/// PARAMS:
///    bWaitForLoad - If true, will wait for all models to be loaded
/// RETURNS:
///    TRUE if all models loaded, false otherwise. 
FUNC BOOL HAS_POLICE_SCENE_LOADED(BOOL bWaitForLoad = FALSE)
	
	//INT i
	
	//COP CARS
	//FOR i = 0 TO NUM_OF_POLICE_CARS - 1
	    REQUEST_MODEL(mvPoliceCars[0].mModel)
	//ENDFOR
	
	//CELEB CAR
	REQUEST_MODEL(mvPoppyCar.mModel)
	/*
	//PARKED CAR
	FOR i = 0 TO NUM_OF_PARKED_CARS - 1
		REQUEST_MODEL(mvParkedCar[i].mModel)
	ENDFOR
	*/
	//REQUEST PEDS
	
	//POLICE PED
	//FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
	    REQUEST_MODEL(mpPolicePed[0].mModel)
	//ENDFOR
	/*
	FOR i = 0 TO NUM_OF_STANDING_PEDS - 1
		REQUEST_MODEL(mpStandingPeds[i].mModel)
	ENDFOR
	*/
	/*
	//PROPS
	FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
		REQUEST_MODEL(mpsChaseProps[i].mModel)
	ENDFOR
	*/
	//POPPY PED
	REQUEST_MODEL(mpPoppyPed.mModel)

	//REQUEST_VEHICLE_RECORDING(121,"PAP3Cop2")
	//REQUEST_VEHICLE_RECORDING(130,"PAP3Cop2")
	//REQUEST_VEHICLE_RECORDING(111,"PAP3Cop1")
	//REQUEST_VEHICLE_RECORDING(161,"PAP3Cop3")
	//REQUEST_VEHICLE_RECORDING(1, "PAP3U")
	//REQUEST_VEHICLE_RECORDING(250,"PAP3U")
	
	IF bWaitForLoad = FALSE
		
		IF NOT HAS_MODEL_LOADED(mvPoliceCars[0].mModel)
		OR NOT HAS_MODEL_LOADED(mpPolicePed[0].mModel)
		OR NOT HAS_MODEL_LOADED(mvPoppyCar.mModel)
		OR NOT HAS_MODEL_LOADED(mpPoppyPed.mModel)
			RETURN FALSE
		ENDIF
		
		/*
    	FOR i = 0 TO NUM_OF_POLICE_CARS - 1
			IF NOT HAS_MODEL_LOADED(mvPoliceCars[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		IF NOT HAS_MODEL_LOADED(mvPoppyCar.mModel)
			RETURN FALSE
		ENDIF
		
		FOR i = 0 TO NUM_OF_PARKED_CARS - 1
			IF NOT HAS_MODEL_LOADED(mvParkedCar[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
			IF NOT HAS_MODEL_LOADED(mpPolicePed[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUM_OF_STANDING_PEDS - 1
			IF NOT HAS_MODEL_LOADED(mpStandingPeds[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
			IF NOT HAS_MODEL_LOADED(mpsChaseProps[i].mModel)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		IF NOT HAS_MODEL_LOADED(mpPoppyPed.mModel)
			RETURN FALSE
		ENDIF
		/*
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(121,"PAP3Cop2")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(130,"PAP3Cop2")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(111,"PAP3Cop1")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(161,"PAP3Cop3")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"PAP3U")
			RETURN FALSE
		ENDIF
		
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(250,"PAP3U")
			RETURN FALSE
		ENDIF
		*/
	ELSE
        // wait for everything to load- used in debug skips
        Bool bEverythingLoaded = FALSE
        
        WHILE bEverythingLoaded = FALSE
		
            WAIT(0)
			
			bEverythingLoaded = TRUE
			
			IF NOT HAS_MODEL_LOADED(mvPoliceCars[0].mModel)
			OR NOT HAS_MODEL_LOADED(mpPolicePed[0].mModel)
			OR NOT HAS_MODEL_LOADED(mvPoppyCar.mModel)
			OR NOT HAS_MODEL_LOADED(mpPoppyPed.mModel)
				bEverythingLoaded =  FALSE
			ENDIF
			
			/*
			bEverythingLoaded = TRUE
			
			FOR i = 0 TO NUM_OF_POLICE_CARS - 1
				IF NOT HAS_MODEL_LOADED(mvPoliceCars[i].mModel)
					bEverythingLoaded =  FALSE
				ENDIF
			ENDFOR
			
			IF NOT HAS_MODEL_LOADED(mvPoppyCar.mModel)
				bEverythingLoaded =  FALSE
			ENDIF
			
			FOR i = 0 TO NUM_OF_PARKED_CARS - 1
				IF NOT HAS_MODEL_LOADED(mvParkedCar[i].mModel)
					bEverythingLoaded =  FALSE
				ENDIF
			ENDFOR
			
			FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
				IF NOT HAS_MODEL_LOADED(mpsChaseProps[i].mModel)
					bEverythingLoaded =  FALSE
				ENDIF
			ENDFOR
			
			FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
				IF NOT HAS_MODEL_LOADED(mpPolicePed[i].mModel)
					bEverythingLoaded =  FALSE
				ENDIF
			ENDFOR
			
			FOR i = 0 TO NUM_OF_STANDING_PEDS - 1
				IF NOT HAS_MODEL_LOADED(mpStandingPeds[i].mModel)
					bEverythingLoaded =  FALSE
				ENDIF
			ENDFOR
			
			IF NOT HAS_MODEL_LOADED(mpPoppyPed.mModel)
				bEverythingLoaded =  FALSE
			ENDIF
			/*
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(121,"PAP3Cop2")
				bEverythingLoaded =  FALSE
			ENDIF
			
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(130,"PAP3Cop2")
				bEverythingLoaded =  FALSE
			ENDIF
		
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(111,"PAP3Cop1")
				bEverythingLoaded =  FALSE
			ENDIF
		
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(161,"PAP3Cop3")
				bEverythingLoaded =  FALSE
			ENDIF
			
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"PAP3U")
				bEverythingLoaded =  FALSE
			ENDIF
			
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(250,"PAP3U")
				bEverythingLoaded =  FALSE
			ENDIF
			*/
        ENDWHILE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Handles performing a rolling start on the player's vehicle.
PROC RCM_MANAGE_ROLLING_START()

	IF bDoneRollingStart = FALSE
		//IF HAS_VEHICLE_RECORDING_BEEN_LOADED(201, "PAP3ARS")
		IF GET_IS_WAYPOINT_RECORDING_LOADED("Pap3aRoll")	
			IF IS_VEHICLE_OK(vehPlayer)
				IF iRollingStartTimer = -1
					/*
					IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
							ePoppyState = PS_DRIVING
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,3000)
						ELSE
							ePoppyState = PS_DRIVING
							START_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle, 1 ,"PAP3U" )
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,3000)
						ENDIF
					ENDIF
					IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle,3000)
						ELSE
							START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle, 130 ,"PAP3Cop2" )
							SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,TRUE)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle,3000)
						ENDIF
					ENDIF
					IF IS_VEHICLE_OK(mvPoliceCars[2].mVehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[2].mVehicle)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[2].mVehicle,3000)
						ELSE
							START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[2].mVehicle, 161 ,"PAP3Cop3" )
							SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,TRUE)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[2].mVehicle,3000)
						ENDIF
					ENDIF
					*/
					//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)	
						CLEAR_AREA(<<377.5160, 169.3637, 102.0690>>,70,TRUE)
						//CLEAR_AREA(<<611.6465, 53.4393, 89.8014>>,70,TRUE)
						//START_PLAYBACK_RECORDED_VEHICLE(vehPlayer, 201, "PAP3ARS")
						//SET_PLAYBACK_SPEED(vehPlayer,0.9)
						IF bHeliReplay = FALSE
							//SAFE_TELEPORT_ENTITY(vehPlayer,<<377.5160, 169.3637, 102.0690>>, 340.2312)
							SAFE_TELEPORT_ENTITY(vehPlayer,<<364.0056, 129.8543, 102.1026>>, 341.4834)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
							SET_VEHICLE_FORWARD_SPEED(vehPlayer,21.84) //21.84
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(),vehPlayer,"Pap3aRoll",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE)
						ELSE
							SAFE_TELEPORT_ENTITY(vehPlayer,<<373.0085, 169.6102, 122.1141>>, 341.0669)
							//SET_VEHICLE_FORWARD_SPEED(vehPlayer,1)
							SET_HELI_BLADES_FULL_SPEED(vehPlayer)
							//TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(),vehPlayer,"Pap3aRoll",DRIVINGMODE_PLOUGHTHROUGH,0,EWAYPOINT_START_FROM_CLOSEST_POINT)
						ENDIF
					ENDIF
					iRollingStartTimer = GET_GAME_TIMER()
				ELSE
					//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
					OR IS_VEHICLE_SEAT_FREE(vehPlayer)
					OR (GET_GAME_TIMER() - iRollingStartTimer > 3000) 
						//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
						//IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)	
							//STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
							//VEHICLE_WAYPOINT_PLAYBACK_PAUSE(vehPlayer)
						//ENDIF
						IF bHeliReplay = FALSE
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							//SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							//SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						ENDIF
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehPlayer)
						//REMOVE_VEHICLE_RECORDING(201, "PAP3ARS")
						bDoneRollingStart = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//REQUEST_VEHICLE_RECORDING(201, "PAP3ARS")
			REQUEST_WAYPOINT_RECORDING("Pap3aRoll")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Skips the current stage
PROC SkipStage()
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
		KILL_ANY_CONVERSATION()
	ENDIF
	
	CLEAR_AREA(<< 346.6118, -404.7508, 44.2121 >>,200.0,TRUE)
	
	SWITCH missionStage
	
		CASE MS_INITIAL_PHONE
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//KILL_PHONE_CONVERSATION()
				ENDIF
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF
			//SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

			//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< 305.3899, 139.1082, 102.7873 >>, 335.5552)
			
			CLEAR_AREA(<<336.4843, 132.4985, 102.0139>>,30,TRUE)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(vehPlayer)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_ENTITY_COORDS(vehPlayer, <<336.4843, 132.4985, 102.0139>>)
					SET_ENTITY_HEADING(vehPlayer,249.9272)
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<336.4843, 132.4985, 102.0139>>, 249.9272,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<336.4843, 132.4985, 102.0139>>)
					SET_ENTITY_HEADING(vehPlayer,249.9272)
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ENDIF
			ELSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<336.4843, 132.4985, 102.0139>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),249.9272)
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			//LOAD_SCENE(<<336.4843, 132.4985, 102.0139>>)
			
			//SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
			
			//missionStage =  MS_INITIAL_PHONE

			//eSubStage = SS_SETUP
		BREAK
		
		CASE MS_GO_TO_LOCATION
				
			ResetCamBehindPlayer()	
			
			iTimerReplayStarted = GET_GAME_TIMER()		
					
			IF HAS_POLICE_SCENE_LOADED(TRUE)
				IF bPoliceChaseCreated = TRUE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_VEHICLE_OK(vehPlayer)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
								bHeliReplay = TRUE
							ELSE
								bHeliReplay = FALSE
							ENDIF
							//REQUEST_VEHICLE_RECORDING(201, "PAP3ARS")
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<393.7350, 218.8013, 102.0914>>)
							SET_ENTITY_HEADING(vehPlayer,338.9834)
						ELSE
							CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
							IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
								bHeliReplay = TRUE
							ELSE
								bHeliReplay = FALSE
							ENDIF
							//REQUEST_VEHICLE_RECORDING(201, "PAP3ARS")
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
							SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<393.7350, 218.8013, 102.0914>>)
							SET_ENTITY_HEADING(vehPlayer,338.9834)
						ENDIF
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<393.7350, 218.8013, 102.0914>>) //TELEPORT PLAYER TO LOCATION
						SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),338.9834)
					ENDIF
					/*
					IF Is_Replay_In_Progress()
						bDoRollingStart = TRUE
						bDoneRollingStart = FALSE
						iRollingStartTimer = -1
						RCM_MANAGE_ROLLING_START()
					ENDIF
					*/
				ENDIF				
			ENDIF
			
		BREAK
		
		CASE MS_CHASE_POPPY
			
			IF IS_VEHICLE_OK(vehPlayer)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayer)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayer)
				ENDIF
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(vehPlayer)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_ENTITY_COORDS(vehPlayer, << 398.9370, -399.1257, 45.7647 >>)
					SET_ENTITY_HEADING(vehPlayer,92.0)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					//REQUEST_VEHICLE_RECORDING(201, "PAP3ARS")
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 398.9370, -399.1257, 45.7647 >>)
					SET_ENTITY_HEADING(vehPlayer,92.0)
				ENDIF
			ELSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 398.9370, -399.1257, 45.7647 >>) //TELEPORT PLAYER TO LOCATION
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),92.0)
			ENDIF
			ResetCamBehindPlayer()
			IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
					ePoppyState = PS_DRIVING
					//SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoppyCar.mVehicle,99.0,1 ,"PAP3U")
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoppyCar.mVehicle,99.0,300 ,"PAP3A2")
				ELSE
					ePoppyState = PS_DRIVING
					START_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle, 300 ,"PAP3A2" )
					//SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoppyCar.mVehicle,99.0,1 ,"PAP3U")
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoppyCar.mVehicle,99.0,300 ,"PAP3A2")
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoliceCars[0].mVehicle,99.0,500 ,"PAP3A2")
				ELSE
					START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle, 500 ,"PAP3A2" )
					SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,TRUE)
					SET_PLAYBACK_SPEED(mvPoliceCars[0].mVehicle,fPlayBackSpeed)
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoliceCars[0].mVehicle,99.0,500 ,"PAP3A2")
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(mvPoliceCars[2].mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[2].mVehicle)
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoliceCars[2].mVehicle,99.0,501 ,"PAP3A2")
				ELSE
					START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[2].mVehicle, 501 ,"PAP3A2" )
					SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,TRUE)
					SET_PLAYBACK_SPEED(mvPoliceCars[2].mVehicle,fPlayBackSpeed)
					SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(mvPoliceCars[2].mVehicle,99.0,501 ,"PAP3A2")
				ENDIF
			ENDIF
			
		BREAK
		
		CASE MS_GO_TO_CRASH
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 365.6487, -399.4542, 44.7933 >>) //PUT PLAYER AT THE CRASH SCENE
			SET_ENTITY_HEADING(PLAYER_PED_ID(),104.0)
			IF IS_VEHICLE_OK(vehPlayer)
				SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
				SET_ENTITY_HEADING(vehPlayer,105.0)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer)
			ENDIF
		BREAK
		
		CASE MS_TAKE_PHOTO
			bPhotoSent = TRUE
			eSubStage = SS_CLEANUP
		BREAK
		
		CASE MS_END_PHONECALL
			IF eSubStage = SS_UPDATE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//KILL_PHONE_CONVERSATION()
				ENDIF
				bConversationActive = TRUE
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
				
	ENDSWITCH	
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ResetCamBehindPlayer()
	
ENDPROC

/// PURPOSE: Jumps to the stage selected
PROC JumpToStage(MISSION_STAGE eNewStage)

	IF missionStage = eNewStage  // skip current stage
		bFinishedSkipping = TRUE
		IF NOT IS_REPLAY_IN_PROGRESS()	
			RC_END_Z_SKIP()
		ENDIF
	ELSE
		SkipStage() 
	ENDIF
ENDPROC
	
#IF IS_DEBUG_BUILD
	/// PURPOSE: Goes back to the previous stage
      PROC PreviousStage()
            int iNewStage	
			
			 iNewStage = ENUM_TO_INT(MissionStage)-1 
			
            IF iNewStage >-1 
                //we can skip to a previous stage
                eTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
                ResetMission()
                bFinishedSkipping = FALSE
                JumpToStage(eTargetStage)
            ENDIF
      ENDPROC

#ENDIF

/// PURPOSE:
///    Starts a Z skip
/// PARAMS:
///    iTargetStage - 
PROC Do_Z_Skip(INT iTargetStage, BOOL bResetMission = FALSE)

	RC_START_Z_SKIP()
	
	IF bResetMission = TRUE
		ResetMission()
	ENDIF
	
	eTargetStage = INT_TO_ENUM(MISSION_STAGE, iTargetStage)
	bFinishedSkipping = FALSE
	JumpToStage(eTargetStage)
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		int iNewStage

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) // Check for Pass
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			Script_Passed()
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)) // Check for Fail
			WAIT_FOR_CUTSCENE_TO_STOP()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			SET_FAIL_REASON(FAIL_DEFAULT)
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) // Check for Skip forward
			SkipStage()
		ENDIF
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) // Check for Skip backwards
			PreviousStage()
		ENDIF
		
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage) // Check for jumping to stage
			Do_Z_Skip(iNewStage, TRUE)
		ENDIF
			
	ENDPROC
#ENDIF

#IF IS_DEBUG_BUILD
	PROC UBER_RECORD_STUFF

		SWITCH iRecordingProgress
		   CASE 0
		      INIT_UBER_RECORDING("Pap3U")
			  iRecordingProgress++
		   BREAK
		   
		   CASE 1
		    
			 WHILE TRUE
			  
				UPDATE_UBER_RECORDING()
				UPDATE_UBER_PLAYBACK(mvPoppyCar.mVehicle,fPlayBackSpeed)
			  	
		   		if bFinishedSkipping = TRUE
					DEBUG_Check_Debug_Keys() // not skipping stages, check for debug keys
				ELSE
					JumpToStage(eTargetStage) // still skipping stages
				ENDIF
				
				WAIT(0)
			
			ENDWHILE
		  
		   BREAK
		
		ENDSWITCH

	ENDPROC
#ENDIF

// --------------------------------------------------------------
//     STAGE FUNCTIONS
// --------------------------------------------------------------

FUNC BOOL IS_PLAYER_IN_FRONT()

	IF IS_PED_IN_FRONT_OF_CAR(PLAYER_PED_ID(),mvPoppyCar.mVehicle)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///  Rubberbanding
PROC CHECK_PLAYER_DISTANCE()
	
	FLOAT fPlayersSpeedModifier = 0
	
	IF bDoneFakeStart = FALSE 
		
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPawnJunc) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPitchersJunc)
			fPlayBackSpeed = (300.0 - (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPawnJunc)))
			fPlayBackSpeed = (fPlayBackSpeed / 300)
			IF fPlayBackSpeed < fLowPlaybackSpeedClamp
				fPlayBackSpeed = fLowPlaybackSpeedClamp
			ENDIF
			IF fPlayBackSpeed > 1
				fPlayBackSpeed = 1
			ENDIF
			IF fTimePosInRec > 10260.810547
			//OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPawnJunc)	
				bDoneFakeStart = TRUE
			ENDIF
		ELSE
			fPlayBackSpeed = (250.0 - (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPitchersJunc)))
			fPlayBackSpeed = (fPlayBackSpeed / 220)
			IF fPlayBackSpeed < fLowPlaybackSpeedClamp
				fPlayBackSpeed = fLowPlaybackSpeedClamp
			ENDIF
			IF fPlayBackSpeed > 1
				fPlayBackSpeed = 1
			ENDIF
			IF fTimePosInRec > 15158.107422
			//OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPitchersJunc)		
				bDoneFakeStart = TRUE
			ENDIF
		ENDIF 
		
		IF NOT DOES_BLIP_EXIST(blipPoppyCar)
			blipPoppyCar = CREATE_VEHICLE_BLIP(mvPoppyCar.mVehicle,TRUE)
		ENDIF
	
	ELSE	
		
		BOOL bDoBoost = FALSE
		BOOL bPlayerInFront = FALSE
		
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
			bDoBoost = TRUE
			fPlayersSpeedModifier = -30.0
		ENDIF
		
		IF fTimePosInRec >= 23715.677734
		AND fTimePosInRec < 28477.677734
			bDoBoost = TRUE
		ENDIF
		IF fTimePosInRec >= 58550.062500
		AND fTimePosInRec < 63927.898438
			bDoBoost = TRUE
		ENDIF
		IF fTimePosInRec >= 70515.734375
		AND fTimePosInRec < 76783.734375
			bDoBoost = TRUE
		ENDIF
		IF fTimePosInRec > 20000	
			IF IS_PLAYER_IN_FRONT()
				bPlayerInFront = TRUE
				bDoBoost = TRUE
			ENDIF
		ENDIF
		
		IF bDoBoost //lift the clamp
			fHighPlaybackSpeedClamp = fHighPlaybackSpeedClamp + 0.025
			IF fHighPlaybackSpeedClamp > 1.25
				fHighPlaybackSpeedClamp = 1.25
			ENDIF
		ELSE
			IF fHighPlaybackSpeedClamp > 1.0
				fHighPlaybackSpeedClamp = fHighPlaybackSpeedClamp - 0.025
			ENDIF
			IF fHighPlaybackSpeedClamp < 1.0
				fHighPlaybackSpeedClamp = 1.0
			ENDIF
		ENDIF
		
		FLOAT fRubberband
		
		fRubberband = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle)
		
		IF bPlayerInFront
			fMaxDist += 0.025
			IF fMaxDist > 180.0
				fMaxDist = 180.0
			ENDIF
			IF fRubberband > 5.0
				fRubberband = 5.0
			ENDIF
		ELSE
			fMaxDist -= 0.025
			IF fMaxDist < 170.0
				fMaxDist = 170.0
			ENDIF
		ENDIF
		
		fPlayBackSpeed = (fMaxDist - (fRubberband))
		fPlayBackSpeed = (fPlayBackSpeed / 130)
		IF fPlayBackSpeed < fLowPlaybackSpeedClamp
			fPlayBackSpeed = fLowPlaybackSpeedClamp
		ENDIF
		IF fPlayBackSpeed > fHighPlaybackSpeedClamp
			fPlayBackSpeed = fHighPlaybackSpeedClamp
		ENDIF
	
	ENDIF

	IF fLowPlaybackSpeedClamp < 0.7	
		IF fPlayBackSpeed > fLowPlaybackSpeedClamp
			fLowPlaybackSpeedClamp = fPlayBackSpeed
		ENDIF
		IF fTimePosInRec > 12000
			fLowPlaybackSpeedClamp = fLowPlaybackSpeedClamp + 0.001
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		MODIFY_VEHICLE_TOP_SPEED(vehPlayer, fPlayersSpeedModifier)
	ENDIF
	
	//CPRINTLN(DEBUG_MISSION, fPlayBackSpeed)
	//PRINTFLOAT(fTimePosInRec)
	//PRINTNL()
	//PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle))
	//PRINTNL()

ENDPROC

/// PURPOSE:
/// Checks if the player has damaged or attack cop cars during chase
FUNC BOOL CHECK_FOR_PLAYER_DAMAGE
	IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[0].mVehicle,PLAYER_PED_ID(),TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(mvPoliceCars[1].mVehicle)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[1].mVehicle,PLAYER_PED_ID(),TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(mvPoliceCars[2].mVehicle)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[2].mVehicle,PLAYER_PED_ID(),TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_HEADING_OK(FLOAT fHeading, FLOAT fIdealHeading, FLOAT fLeeway )
	FLOAT fUpperH
	FLOAT fLowerH

	fLowerH = fIdealHeading - fLeeway
	IF fLowerH < 0.0
		fLowerH += 360.0
	ENDIF
	fUpperH = fIdealHeading + fLeeway
	IF fUpperH >= 360.0
		fUpperH -= 360.0
	ENDIF
	IF fUpperH > fLowerH
		IF fHeading < fUpperH
		AND fHeading > fLowerH
			RETURN TRUE
		ENDIF
	ELSE
		IF fHeading < fUpperH
		OR fHeading > fLowerH
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
/// Manages cop cars during the chase
/// Handles playback speed etc. 
/*
PROC MANAGE_COP_CARS()
		
	IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle) AND GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 600
			START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle, 130 ,"PAP3Cop2" )     //!!!!!!!!!!!!!!!
			SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,TRUE)
			SET_PLAYBACK_SPEED(mvPoliceCars[0].mVehicle,fPlayBackSpeed)
			SET_ENTITY_INVINCIBLE(mvPoliceCars[0].mVehicle,FALSE)
		ELSE
			SET_PLAYBACK_SPEED(mvPoliceCars[0].mVehicle,fPlayBackSpeed)
			SET_ENTITY_INVINCIBLE(mvPoliceCars[0].mVehicle,TRUE)
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(mvPoliceCars[1].mVehicle) AND bCopCrashed = FALSE
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[1].mVehicle) AND GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 300
			START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[1].mVehicle, 111 ,"PAP3Cop1" )     //!!!!!!!!!!!!!!!
			SET_VEHICLE_SIREN(mvPoliceCars[1].mVehicle,TRUE)
			SET_PLAYBACK_SPEED(mvPoliceCars[1].mVehicle,fPlayBackSpeed)
		ELSE
			IF bCopCrashed = FALSE
				SET_PLAYBACK_SPEED(mvPoliceCars[1].mVehicle,fPlayBackSpeed)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[1].mVehicle)
					IF GET_TIME_POSITION_IN_RECORDING(mvPoliceCars[1].mVehicle) >= 32300
						STOP_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[1].mVehicle)
						SET_VEHICLE_ENGINE_HEALTH(mvPoliceCars[1].mVehicle,5.0)
						SET_VEHICLE_SIREN(mvPoliceCars[1].mVehicle,FALSE)
						SET_VEHICLE_ALARM(mvPoliceCars[1].mVehicle,TRUE)
						SET_VEHICLE_ENGINE_ON(mvPoliceCars[1].mVehicle,FALSE,TRUE)
						IF IS_PED_UNINJURED(mpPolicePed[2].mPed)
							SET_ENTITY_HEALTH(mpPolicePed[2].mPed,99)
						ENDIF
						bCopCrashed = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(mvPoliceCars[2].mVehicle)
		IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[2].mVehicle) AND GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 45000
			START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[2].mVehicle, 161 ,"PAP3Cop3" )  //!!!!!!!!!!!!!!!
			SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,TRUE)
			SET_PLAYBACK_SPEED(mvPoliceCars[2].mVehicle,fPlayBackSpeed)
		ELSE
			SET_PLAYBACK_SPEED(mvPoliceCars[2].mVehicle,fPlayBackSpeed)
		ENDIF
	ENDIF

ENDPROC
*/
/// PURPOSE:
///  Hanldes all the loading / playback of the chase uber recording. 
PROC PLAYBACK_UBER_RECORDING

	SWITCH eSubStageRec
	 	CASE SSRP_SETUP
				
			//INITIALISE_UBER_PLAYBACK("PAP3U",250,FALSE)
			fLowPlaybackSpeedClamp = 0.3
			INITIALISE_UBER_PLAYBACK("PAP3A2",300,FALSE)
			//LOAD_UBER_DATA()
			LOAD_UBER_DATA_NEW()
			PRELOAD_CAR_RECORDINGS_FOR_UBER_CHASE()
			eSubStageRec = SSRP_UPDATE
		BREAK
		
		CASE SSRP_UPDATE
				
			IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
					ePoppyState = PS_DRIVING                                            
					//START_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle, 250 ,"PAP3U" ) 
					START_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle, 300 ,"PAP3A2" ) 
					IF bDoingRollingReplay = FALSE	
						IF NOT IS_REPLAY_IN_PROGRESS()	
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<350.427979,143.832642,100.855240>>, <<271.727234,172.999466,117.883522>>, 52.500000)  //not really moved from outside whirligig
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,5000)
								ELSE	
									//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<610.134766,159.861359,95.472145>>, <<160.532639,319.985107,208.461792>>, 105.750000)  //driving down a side street
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<186.165924,299.047302,86.186226>>, <<653.020813,137.619247,179.561157>>, 133.500000)		
									AND IS_HEADING_OK(GET_ENTITY_HEADING(PLAYER_PED_ID()),339.0575,90)	
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPitchersJunc) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPawnJunc)
											SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,11000)
										ELSE
											SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,8000)
										ENDIF
									ELSE
										SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,7000)
									ENDIF
								ENDIF
							ELSE
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,2000)
							ENDIF
						ELSE
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,4000)
						ENDIF
					ELSE
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle,7600) //7000
					ENDIF
					SET_ENTITY_INVINCIBLE(mvPoppyCar.mVehicle,TRUE)                     
				ELSE
					//IF NOT IS_ENTITY_AT_COORD(mvPoppyCar.mVehicle,vCrashLocation,<<4.5,4.5,4.5>>) 
					IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) < 115000//113369 //119011 //113369
						//113336
						/*
						IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 113369
							IF bPoppyCarSmashed = FALSE
								SET_ENTITY_INVINCIBLE(mvPoppyCar.mVehicle,FALSE)   
								SET_VEHICLE_DAMAGE(mvPoppyCar.mVehicle, <<0, 2, 0>>, 300.0, 150.0, TRUE)
								bPoppyCarSmashed = TRUE
							ENDIF
						ENDIF
						*/
						IF bPoppyHasCrashed = FALSE
							//MANAGE_COP_CARS()
						ENDIF
						CHECK_PLAYER_DISTANCE()   
						IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())	
							IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 2000	
								fUberPlaybackMinCreationDistance = 300.0
								fUberPlaybackDensitySwitchOffRange = 50.0
							ENDIF
						ENDIF
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = FALSE//TRUE  
						bSetPieceCarsDontSwitchToAI = TRUE	
						PRELOAD_CAR_RECORDINGS_FOR_UBER_CHASE() 
						//CREATE_ALL_WAITING_UBER_CARS()
						UPDATE_UBER_PLAYBACK(mvPoppyCar.mVehicle,fPlayBackSpeed)
						IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) >= 113369
							IF bPoppyCarSmashed = FALSE
								SET_ENTITY_INVINCIBLE(mvPoppyCar.mVehicle,FALSE)   
								SET_VEHICLE_DAMAGE(mvPoppyCar.mVehicle, <<0, 2, 0>>, 400.0, 200.0, TRUE)
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 2.0, REPLAY_IMPORTANCE_LOWEST) //Poppy crashes her car into a tree.
								bPoppyCarSmashed = TRUE
							ENDIF
							IF bCrashSoundPlayed = FALSE
								PLAY_SOUND_FROM_ENTITY(-1,"CRASH",mvPoppyCar.mVehicle,"PAPARAZZO_03A")
								bCrashSoundPlayed = TRUE
							ENDIF
							IF bPlayerStopped
								IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
									TRIGGER_MUSIC_EVENT("PAP3_STOP")
								ENDIF
								eSubStageRec = SSRP_CLEANUP
							ENDIF	
						ENDIF
					ELSE
						//STOP_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle)   //!!!!!!!!!!!!!
						IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()	
							TRIGGER_MUSIC_EVENT("PAP3_STOP")
						ENDIF
						eSubStageRec = SSRP_CLEANUP
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SSRP_CLEANUP
			bPoppyHasCrashed = TRUE
			//SMOKE POPPYS CAR ENGINE
			IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)
				BRING_VEHICLE_TO_HALT(mvPoppyCar.mVehicle, 10.0, -1)
				SET_VEHICLE_ENGINE_HEALTH(mvPoppyCar.mVehicle,301.0)
				eSubStage = SS_CLEANUP
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///  Makes the police react to the player threatening them
PROC MAKE_POLICE_ATTACK_PLAYER()

	IF bPoliceAttacking = FALSE
		
		INT i
		
		IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.92	
				IF IS_PED_UNINJURED(mpPoppyPed.mPed)
				AND NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
					SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.975	
				IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
				AND NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
					SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
				STOP_SYNCHRONIZED_ENTITY_ANIM(mvPoliceCars[0].mVehicle,INSTANT_BLEND_OUT,FALSE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mvPoliceCars[0].mVehicle)
			ENDIF
		ENDIF
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF

		FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
			IF IS_ENTITY_ALIVE(mpPolicePed[i].mPed)
				IF NOT IS_PED_IN_ANY_VEHICLE(mpPolicePed[i].mPed)	
					TASK_COMBAT_PED(mpPolicePed[i].mPed,PLAYER_PED_ID())
					//ACTIVATE_PHYSICS(mpPolicePed[i].mPed)
					eCopState[i] = CS_ATTACK_PLAYER
				ENDIF
			ENDIF
		ENDFOR
		
		IF ePoppyState <> PS_POPPY_IN_COP_CAR
			IF IS_PED_UNINJURED(mpPoppyPed.mPed)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
					fPhase = GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene)
					IF fPhase < 0.514839	 //Poppy staggering/crawling about, retrigger synced anim with physics
						TASK_SYNCHRONIZED_SCENE (mpPoppyPed.mPed, poppy_arrest_scene, "rcmpaparazzo_3", "poppy_arrest_popm", INSTANT_BLEND_IN, -1.5, SYNCED_SCENE_USE_PHYSICS)
						SET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene,fPhase)
						TASK_LOOK_AT_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),-1)
					ENDIF
					IF fPhase >= 0.514839    //Poppy not handcuffed yet so normal flee
					AND fPhase < 0.667789	
						TASK_SMART_FLEE_PED(mpPoppyPed.mPed, PLAYER_PED_ID(), 200, -1)
					ENDIF
					IF fPhase >= 0.667789	 //Poppy is hancuffed so play scynced anim as an upperbody/secondary anim task and flee
					AND fPhase < 0.823092	
						TASK_PLAY_ANIM(mpPoppyPed.mPed, "rcmpaparazzo_3", "poppy_arrest_popm", NORMAL_BLEND_IN, -1.5, -1, AF_UPPERBODY | AF_SECONDARY, 0.615948, TRUE)
						//SET_ANIM_RATE(mpPoppyPed.mPed,0.2)
						TASK_LOOK_AT_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),-1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(mpPoppyPed.mPed,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,80,0>>),1.5,-1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),2)
		
		IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
			IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[0].mPed,PLAYER_PED_ID(),100)	
				IF NOT IS_PED_IN_COMBAT(mpPolicePed[0].mPed,PLAYER_PED_ID())	
					CLEAR_PED_TASKS(mpPolicePed[0].mPed)
					TASK_COMBAT_PED(mpPolicePed[0].mPed,PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(mpPolicePed[2].mPed)
			IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[2].mPed,PLAYER_PED_ID(),100)
				IF NOT IS_PED_IN_COMBAT(mpPolicePed[2].mPed,PLAYER_PED_ID())		
					CLEAR_PED_TASKS(mpPolicePed[2].mPed)
					TASK_COMBAT_PED(mpPolicePed[2].mPed,PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(mpPolicePed[3].mPed)
			IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[3].mPed,PLAYER_PED_ID(),100)
				IF NOT IS_PED_IN_COMBAT(mpPolicePed[3].mPed,PLAYER_PED_ID())	
					CLEAR_PED_TASKS(mpPolicePed[3].mPed)
					TASK_COMBAT_PED(mpPolicePed[3].mPed,PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
		
		bPoliceAttacking = TRUE
	
	ENDIF

ENDPROC

/// PURPOSE:
/// Handles all of POPPY'S behaviour states etc.
PROC Monitor_Poppy

	SWITCH ePoppyState
		
			CASE PS_WAIT_FOR_PLAYER
				//WAITS HERE TO BEGIN DRIVIGN SECTION
			BREAK
			
			CASE PS_DRIVING
				//STAYS IN HERE WHILE POPPY IS DURING CHASE
				//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELPPO")	
					IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
						CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mvPoppyCar.mVehicle)
					ENDIF
				//ENDIF
				
				IF missionStage = MS_TAKE_PHOTO
					IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
						ADD_PED_FOR_DIALOGUE(sSpeach, 4, mpPolicePed[0].mPed, "Paparazzo3ACop1")
					ENDIF
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),FALSE)
						IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
								STOP_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle)
							ENDIF
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mpPoppyPed.mPed)
						SET_FAIL_REASON(FAIL_POPPY_INJURED)
					ENDIF
				ENDIF
			BREAK

			CASE PS_LEAVE_VEHICLE
				
				IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(mpPoppyPed.mPed)
					IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
						ADD_PED_FOR_DIALOGUE(sSpeach, 4, mpPolicePed[0].mPed, "Paparazzo3ACop1")
					ENDIF
					ADD_PED_FOR_DIALOGUE(sSpeach, 6, mpPoppyPed.mPed, "POPPY")
					//IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_ARREST", CONV_PRIORITY_HIGH)
						ePoppyState = PS_SURRENDER		
					//ENDIF
				ENDIF
				
			BREAK
			
			CASE PS_SURRENDER
		
				IF IS_ENTITY_AT_COORD(mpPoppyPed.mPed,vPoppySurrenderPos,<<1.5,1.5,1.5>>)
					IF IS_PED_UNINJURED(mpPolicePed[3].mPed)
						ADD_PED_FOR_DIALOGUE(sSpeach, 5, mpPolicePed[3].mPed, "Paparazzo3ACop2")
					ENDIF
					//IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_COP3", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
						ePoppyState = PS_ARRESTED
					//ENDIF
				ENDIF
			
			BREAK
			
			CASE PS_ARRESTED
				
				IF eCopState[1] = CS_ARREST_POPPY //THIS IS CHANGED TO THIS WHEN POLICE GET CLOSE TO POPPY.
					IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
						ADD_PED_FOR_DIALOGUE(sSpeach, 4, mpPolicePed[0].mPed, "Paparazzo3ACop1")
					ENDIF
					ADD_PED_FOR_DIALOGUE(sSpeach, 6, mpPoppyPed.mPed, "POPPY")
					//IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_COP4", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
						/*
						CLEAR_PED_TASKS(mpPoppyPed.mPed)
						TASK_PLAY_ANIM(mpPoppyPed.mPed,"cop","armsup_loop",NORMAL_BLEND_IN,NORMAL_BLEND_OUT, 9000,AF_LOOPING | AF_UPPERBODY | AF_SECONDARY)	
						IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
							TASK_ENTER_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,-1,VS_BACK_RIGHT,PEDMOVE_WALK)
							ePoppyState = PS_GET_IN_VEHICLE
						ENDIF
						*/
						//IF NOT IS_ENTITY_PLAYING_ANIM(mpPoppyPed.mPed,"rcmpaparazzo_3","poppy_arrest_popm")
							//TASK_PLAY_ANIM(mpPoppyPed.mPed,"rcmpaparazzo_3","poppy_arrest_popm")
						//ENDIF
					//ENDIF
				ENDIF
				
			BREAK
			
			CASE PS_GET_IN_VEHICLE
			BREAK
			
			CASE PS_POPPY_IN_COP_CAR
			BREAK
			
			CASE PS_FLEE_AREA
			BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE: Handle the crash sequence
PROC HANDLE_CRASH_SEQUENCE()

	//TEXT_LABEL_23 label

	IF bCrashCutDone = TRUE
		IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)	
		AND IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
				IF iSeqCarDoors = 0
					CLEAR_PRINTS()
					CLEAR_HELP()
					IF IS_PED_UNINJURED(mpPoppyPed.mPed)	
						IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(mpPoppyPed.mPed)
							IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
								ADD_PED_FOR_DIALOGUE(sSpeach, 4, mpPolicePed[0].mPed, "Paparazzo3ACop1")
							ENDIF
							ADD_PED_FOR_DIALOGUE(sSpeach, 6, mpPoppyPed.mPed, "POPPY")
							IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_ARREST", CONV_PRIORITY_MEDIUM)
								ePoppyState = PS_SURRENDER		
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(vehPoliceChopper)
						SET_VEHICLE_SEARCHLIGHT(vehPoliceChopper,TRUE,TRUE)
						TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,<<0,50,60>>,MISSION_POLICE_BEHAVIOUR,0.1,60.0,-1,60,55)
					ENDIF
					WHILE GET_GAME_TIMER() <= iTimerSyncSceneStarted + 8500	
					AND NOT IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(1500)			
					//AND NOT ARE_STRINGS_EQUAL(label,"PAP3A_ARREST_1")			
						IF GET_GAME_TIMER() <= iTimerSyncSceneStarted + 500		
							IF bDelayFade	
								IF IS_REPLAY_IN_PROGRESS()	
									IF IS_SCREEN_FADED_OUT()
										SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
										CPRINTLN(DEBUG_MISSION, "PAP3A**** DELAYED FADE CS ****")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF NOT bDone1stPersonFlash
							IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								IF GET_GAME_TIMER() >= iTimerSyncSceneStarted + 8200	
									ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
									PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
									bDone1stPersonFlash = TRUE
								ENDIF
							ENDIF
						ENDIF
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)	
						AND IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)			
							FREEZE_ENTITY_POSITION(mvPoppyCar.mVehicle,FALSE)
							IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoppyCar.mVehicle,SC_DOOR_FRONT_LEFT) < 0.8
								fPoppyCarFrontDoorOpenRatio = fPoppyCarFrontDoorOpenRatio + 0.03
								SET_VEHICLE_DOOR_CONTROL(mvPoppyCar.mVehicle,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,fPoppyCarFrontDoorOpenRatio)
							ENDIF
							/*
							IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT) < 0.7
								fCopCarFrontDoorOpenRatio = fCopCarFrontDoorOpenRatio + 0.14
								SET_VEHICLE_DOOR_CONTROL(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,fCopCarFrontDoorOpenRatio)
								//SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[0].mVehicle)
							ENDIF
							*/
						ENDIF
						#IF IS_DEBUG_BUILD // STAGE SKIPPING
							IF MissionStage <> MS_MISSION_FAILED
								if bFinishedSkipping = TRUE
									DEBUG_Check_Debug_Keys() // not skipping stages, check for debug keys
								ELSE
									JumpToStage(eTargetStage) // still skipping stages
								ENDIF
							ENDIF
						#ENDIF
						//label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						IF IS_PED_UNINJURED(pedChopperPilot)
						AND IS_ENTITY_ALIVE(mpPoppyPed.mPed)
						AND IS_ENTITY_ALIVE(vehPoliceChopper)
							IF CONTROL_MOUNTED_WEAPON(pedChopperPilot)
								SET_MOUNTED_WEAPON_TARGET(pedChopperPilot,mpPoppyPed.mPed,NULL,<<0,0,0>>) 
							ENDIF
						ENDIF
						WAIT(0)
					ENDWHILE
					IF IS_SCREEN_FADED_OUT()
						SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
						CPRINTLN(DEBUG_MISSION, "PAP3A**** FADING IN AFTER CS, THIS IS PROBABLY A BAD THING ****")
					ENDIF
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,-1)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE,SPC_REENABLE_CONTROL_ON_DEATH)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					IF IS_ENTITY_ALIVE(vehPoliceChopper)
						FREEZE_ENTITY_POSITION(vehPoliceChopper,FALSE)
					ENDIF
					IF bPhotoPoppyObj = FALSE
						IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							PRINT_NOW("PAP3_POP", DEFAULT_GOD_TEXT_TIME, 1) //"Take a photo of poppy"
							bPhotoPoppyObj = TRUE
						ENDIF
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipPoppy)
						blipPoppy = CREATE_PED_BLIP(mpPoppyPed.mPed,TRUE,TRUE)
					ENDIF
					iSeqCarDoors = 4//1
				ELIF iSeqCarDoors = 1
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoppyCar.mVehicle,SC_DOOR_FRONT_LEFT) < 0.9
						fPoppyCarFrontDoorOpenRatio = fPoppyCarFrontDoorOpenRatio + 0.04
						SET_VEHICLE_DOOR_CONTROL(mvPoppyCar.mVehicle,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,fPoppyCarFrontDoorOpenRatio)
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT) < 0.8
						fCopCarFrontDoorOpenRatio = fCopCarFrontDoorOpenRatio + 0.14
						SET_VEHICLE_DOOR_CONTROL(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,fCopCarFrontDoorOpenRatio)
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) >= 0.810415
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT) < 0.8
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) < 0.823092
							fCopCarRearDoorOpenRatio = fCopCarRearDoorOpenRatio + 0.025
							SET_VEHICLE_DOOR_CONTROL(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT,DT_DOOR_INTACT,fPoppyCarFrontDoorOpenRatio)
						ELSE
							SET_VEHICLE_DOOR_OPEN(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT)
							iSeqCarDoors = 2
						ENDIF
					ENDIF
				
				ELIF iSeqCarDoors = 2
					IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) >= 0.918249
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT) > 0.1
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) < 0.930077	
							fCopCarRearDoorOpenRatio = fCopCarRearDoorOpenRatio - 0.05
							SET_VEHICLE_DOOR_CONTROL(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT,DT_DOOR_INTACT,fCopCarRearDoorOpenRatio)
						ELSE
							SET_VEHICLE_DOOR_SHUT(mvPoliceCars[0].mVehicle,SC_DOOR_REAR_LEFT)
							IF IS_PED_UNINJURED(mpPoppyPed.mPed)	
								IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
									SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
								ENDIF
							ENDIF
							iSeqCarDoors = 3
						ENDIF
					ENDIF
				
				ELIF iSeqCarDoors = 3
					IF GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) >= 0.970048	
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT) > 0.1
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) < 0.991134		
							fCopCarFrontDoorOpenRatio = fCopCarFrontDoorOpenRatio - 0.056
							SET_VEHICLE_DOOR_CONTROL(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,fCopCarFrontDoorOpenRatio)
						ELSE
							SET_VEHICLE_DOOR_SHUT(mvPoliceCars[0].mVehicle,SC_DOOR_FRONT_LEFT)
							IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
								IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
									SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
								ENDIF
							ENDIF
							iSeqCarDoors = 4
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Handles the police behaviours and also kicks off the crash sequence
/// PARAMS:
///    iPedNum - The number of the police ped we are checking
PROC MONITOR_POLICE_PEDS(INT iPedNum)
	
	VECTOR vPoliceCarOffset = << -0.428284, 0.205746, -0.156812 >>
	
	SWITCH eCopState[iPedNum]
	
		CASE CS_DRIVING
			IF missionStage = MS_TAKE_PHOTO OR missionStage = MS_GO_TO_CRASH
				IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
					IF iPedNum = 0		
						IF missionStage = MS_TAKE_PHOTO	
						AND RC_IS_CUTSCENE_OK_TO_START(FALSE)	
							IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
							AND IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)
							AND IS_PED_UNINJURED(mpPoppyPed.mPed)
							AND bCrashCutDone = FALSE
								
								CLEAR_PRINTS()
								
								//IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
								//	SET_VEHICLE_ENGINE_ON(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),FALSE,FALSE)
								//ENDIF
								
								IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_POLICE_CHASE")
									STOP_AUDIO_SCENE("PAPARAZZO_3A_POLICE_CHASE")
								ENDIF
								
								IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_PHOTO_SCENE")
									START_AUDIO_SCENE("PAPARAZZO_3A_PHOTO_SCENE")
								ENDIF
								
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
									STOP_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle)
								ENDIF
								
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
									STOP_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle)
								ENDIF
								
								SAFE_DELETE_PED(mpPolicePed[0].mPed)
								SAFE_DELETE_PED(mpPolicePed[1].mPed)
								//SAFE_DELETE_PED(mpPolicePed[3].mPed)
								
								SAFE_DELETE_VEHICLE(mvPoliceCars[0].mVehicle)
								//SAFE_DELETE_VEHICLE(mvPoliceCars[2].mVehicle)
								
								//<< 349.478, -401.681, 44.7238 >>  cop pos
								//mvPoliceCars[0].mVehicle = CREATE_VEHICLE(mvPoliceCars[0].mModel,<< 349.6943, -401.2252, 44.3462 >>,100.5072)    //previous
								
								vPoliceCarOffset.x *= -1.0
								vPoliceCarOffset.y *= -1.0
								vPoliceCarOffset.z *= -1.0
								
								vPoliceCarPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<< 349.478, -401.681, 44.7238 >>,106.141273,<<vPoliceCarOffset.x,vPoliceCarOffset.y,vPoliceCarOffset.z>>)
						
								//mvPoliceCars[0].mVehicle = CREATE_VEHICLE(mvPoliceCars[0].mModel,GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<< 349.478, -401.681, 44.7238 >>,106.141273,<<vPoliceCarOffset.x,vPoliceCarOffset.y,vPoliceCarOffset.z>>),106.141273)
								mvPoliceCars[0].mVehicle = CREATE_VEHICLE(mvPoliceCars[0].mModel,<<vPoliceCarPos.x,vPoliceCarPos.y,44.3462>>,106.141273)
								
								//mvPoliceCars[2].mVehicle = CREATE_VEHICLE(mvPoliceCars[2].mModel,<<328.869232,-399.050049,44.815384>>,-57.309715)
													
								mpPolicePed[0].mPed = CREATE_PED_INSIDE_VEHICLE(mvPoliceCars[0].mVehicle,PEDTYPE_COP,S_M_Y_Cop_01,VS_DRIVER)
								mpPolicePed[1].mPed = CREATE_PED_INSIDE_VEHICLE(mvPoliceCars[0].mVehicle,PEDTYPE_COP,S_M_Y_Cop_01,VS_FRONT_RIGHT)
								//mpPolicePed[3].mPed = CREATE_PED_INSIDE_VEHICLE(mvPoliceCars[2].mVehicle,PEDTYPE_COP,S_M_Y_Cop_01,VS_DRIVER)
													
								GIVE_WEAPON_TO_PED(mpPolicePed[0].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
								//GIVE_WEAPON_COMPONENT_TO_PED(mpPolicePed[0].mPed,WEAPONTYPE_PISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[0].mPed,TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(mpPolicePed[0].mPed,relCopGroup)
								
								GIVE_WEAPON_TO_PED(mpPolicePed[1].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
								//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[1].mPed,TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(mpPolicePed[1].mPed,relCopGroup)
								//TASK_SET_DECISION_MAKER(mpPolicePed[1].mPed,DECISION_MAKER_EMPTY)
								SET_PED_CONFIG_FLAG(mpPolicePed[1].mPed,PCF_PreventAutoShuffleToDriversSeat,TRUE)
								TASK_LOOK_AT_ENTITY(mpPolicePed[1].mPed,mpPoppyPed.mPed,-1)
								
								//GIVE_WEAPON_TO_PED(mpPolicePed[3].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
								//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[3].mPed,TRUE)
								//SET_PED_RELATIONSHIP_GROUP_HASH(mpPolicePed[3].mPed,relCopGroup)
			
								//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[0].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
								SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[0].mVehicle)
								//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[2].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
								//SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[2].mVehicle)
								SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,TRUE)
								//SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,TRUE)

								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
									OPEN_SEQUENCE_TASK(seqPlayerExitCarFaceCrash)
										TASK_LOOK_AT_ENTITY(NULL,mpPoppyPed.mPed,-1)
									CLOSE_SEQUENCE_TASK(seqPlayerExitCarFaceCrash)
								ELSE
									OPEN_SEQUENCE_TASK(seqPlayerExitCarFaceCrash)
										TASK_LOOK_AT_ENTITY(NULL,mpPoppyPed.mPed,-1)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL,mpPoppyPed.mPed,-1)
									CLOSE_SEQUENCE_TASK(seqPlayerExitCarFaceCrash)
								ENDIF
								
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(),seqPlayerExitCarFaceCrash)
								CLEAR_SEQUENCE_TASK(seqPlayerExitCarFaceCrash)
						
								SET_ENTITY_COORDS(mvPoppyCar.mVehicle,<<341.220337,-410.229462,44.635387>>)  //<< 341.2146, -410.2309, 44.1722 >>
								SET_ENTITY_HEADING(mvPoppyCar.mVehicle,109.615982)  //111.4817
								SET_VEHICLE_ON_GROUND_PROPERLY(mvPoppyCar.mVehicle)
							
								IF NOT IS_ENTITY_DEAD(mvPoliceCars[2].mVehicle)
									SET_ENTITY_COORDS(mvPoliceCars[2].mVehicle,<<328.869232,-399.050049,44.815384>>)
									SET_ENTITY_HEADING(mvPoliceCars[2].mVehicle,-57.309715)
									SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[2].mVehicle)
									SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,TRUE)
								ENDIF
								
								IF IS_PED_UNINJURED(mpPolicePed[3].mPed)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[3].mPed,TRUE)
									CLEAR_PED_TASKS(mpPolicePed[3].mPed)
									IF NOT IS_PED_IN_VEHICLE(mpPolicePed[3].mPed,mvPoliceCars[2].mVehicle)
										SET_PED_INTO_VEHICLE(mpPolicePed[3].mPed,mvPoliceCars[2].mVehicle)
									ENDIF
									TASK_LOOK_AT_ENTITY(mpPolicePed[3].mPed,mpPoppyPed.mPed,-1)
								ENDIF
								
								//FREEZE_ENTITY_POSITION(mvPoppyCar.mVehicle,TRUE)
								
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mvPoppyCar.mVehicle)
								
								CLEAR_PED_TASKS(mpPoppyPed.mPed)
								vPoppy = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mpPoppyPed.mPed,<<0,0,0>>)
								SET_ENTITY_COORDS(mpPoppyPed.mPed,<<vPoppy.x,vPoppy.y,vPoppy.z - 1.0>>)
								
								poppy_arrest_scene = CREATE_SYNCHRONIZED_SCENE(<< 339.0846, -411.1358, 44.0895 >>, <<0,0,0>>)
							
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(poppy_arrest_scene,FALSE)
							
								ePoppyState = PS_LEAVE_VEHICLE	
										
								CLEAR_PED_TASKS(mpPolicePed[0].mPed)
								vCop = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mpPolicePed[0].mPed,<<0,0,0>>)
								SET_ENTITY_COORDS(mpPolicePed[0].mPed,<<vCop.x,vCop.y,vCop.z - 1.0>>)
								
								eCopState[0] = CS_LEAVE_VEHICLE
								/*
								camPoppy = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
								SET_CAM_COORD(camPoppy,<< 337.9194, -413.7527, 45.2942 >>)
								SET_CAM_ROT(camPoppy,<< -9.1110, 0.0930, -40.2254 >>)
								SET_CAM_FOV(camPoppy,49.8293)
								*/
								camPoppy = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
								SET_CAM_COORD(camPoppy,<< 338.4, -413.7, 45.1 >>)
								SET_CAM_ROT(camPoppy,<< -0.4, 0.2, -43.5 >>)
								SET_CAM_FOV(camPoppy,49.8293)
								
								camPoppy2 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
								SET_CAM_COORD(camPoppy2,<< 337.8593, -413.8199, 46.2603 >>)
								SET_CAM_ROT(camPoppy2,<< -19.1264, 0.0930, -40.2055 >>)
								SET_CAM_FOV(camPoppy2,50.8293)
								
								SET_CAM_ACTIVE_WITH_INTERP(camPoppy2,camPoppy,10000)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								DISPLAY_HUD(FALSE)
								DISPLAY_RADAR(FALSE)
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_REENABLE_CONTROL_ON_DEATH)

								TASK_SYNCHRONIZED_SCENE (mpPoppyPed.mPed, poppy_arrest_scene, "rcmpaparazzo_3", "poppy_arrest_popm", INSTANT_BLEND_IN, -1.5)
								TASK_SYNCHRONIZED_SCENE (mpPolicePed[0].mPed, poppy_arrest_scene, "rcmpaparazzo_3", "poppy_arrest_cop", INSTANT_BLEND_IN, -1.5)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(mvPoliceCars[0].mVehicle, poppy_arrest_scene, "poppy_arrest_car", "rcmpaparazzo_3",  INSTANT_BLEND_IN, -1)
						
								SET_CURRENT_PED_WEAPON(mpPolicePed[0].mPed,WEAPONTYPE_PISTOL,TRUE)
								
								IF DOES_CAM_EXIST(camPoppy2)
									SHAKE_CAM(camPoppy2,"HAND_SHAKE",1.0)
								ENDIF
								IF DOES_CAM_EXIST(camPoppy)
									SHAKE_CAM(camPoppy,"HAND_SHAKE",1.0)
								ENDIF
								
								iTimerSyncSceneStarted = GET_GAME_TIMER()
								
								fCopCarFrontDoorOpenRatio = 0
								fCopCarRearDoorOpenRatio = 0
								fPoppyCarFrontDoorOpenRatio = 0
								
								iSeqCarDoors = 0
								
								//turn off roads at crash
								SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, FALSE)
								
								bCrashCutDone = TRUE
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CS_LEAVE_VEHICLE
			IF  NOT HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
				IF iPedNum <> 0	
					IF NOT IS_PED_IN_ANY_VEHICLE(mpPolicePed[iPedNum].mPed)
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							TASK_AIM_GUN_AT_ENTITY(mpPolicePed[iPedNum].mPed,mpPoppyPed.mPed,-1,FALSE)
							eCopState[iPedNum] = CS_AIM_AT_POPPY
						ENDIF
					ENDIF
				ENDIF
				IF iSeqCarDoors = 4	
					//IF GET_GAME_TIMER()	> iTimerSyncSceneStarted + 55000		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
					OR GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene)	= 1
						IF IS_PED_UNINJURED(mpPolicePed[0].mPed)	
						AND IS_PED_UNINJURED(mpPoppyPed.mPed)	
						AND IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)		
							IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mpPolicePed[0].mPed)
								SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
								SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
							ENDIF
							iSeqCarDoors = 5
							eCopState[iPedNum] = CS_ENTER_COP_CAR
							ePoppyState = PS_POPPY_IN_COP_CAR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE CS_AIM_AT_POPPY
			IF NOT HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
				IF ePoppyState = PS_SURRENDER
					IF iPedNum = 0
						//eCopState[iPedNum] = CS_MOVE_TO_POPPY
					ELIF iPedNum = 1
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							OPEN_SEQUENCE_TASK(seqAvoidCarDoor)
								TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, << 351.0502, -394.4243, 44.2688 >>, mpPoppyPed.mPed, 1.6, FALSE,2.0,0.0)
								TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL, mpPoppyPed.mPed, mpPoppyPed.mPed, 1.2, FALSE,0.0,4.0)
							CLOSE_SEQUENCE_TASK(seqAvoidCarDoor)
							TASK_PERFORM_SEQUENCE(mpPolicePed[iPedNum].mPed,seqAvoidCarDoor)
							eCopState[iPedNum] = CS_MOVE_TO_POPPY
						ENDIF
					ELIF iPedNum = 3
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(mpPolicePed[iPedNum].mPed, mpPoppyPed.mPed, mpPoppyPed.mPed, 1.5, FALSE,0.0,6.0)
							eCopState[iPedNum] = CS_MOVE_TO_POPPY
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE CS_MOVE_TO_POPPY
			
			IF NOT HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
				IF iPedNum = 0
					//eCopState[iPedNum] = CS_MOVE_TO_POPPY
				ELIF iPedNum = 1
					IF IS_ENTITY_AT_ENTITY(mpPolicePed[iPedNum].mPed,mpPoppyPed.mPed,<<3.0,3.0,3.0>>)
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							//TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(mpPolicePed[iPedNum].mPed, mpPoppyPed.mPed, mpPoppyPed.mPed, 2.0, FALSE,0.5,0.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(mpPolicePed[iPedNum].mPed, << 346.3328, -395.9672, 44.2922 >>, mpPoppyPed.mPed, 1.0, FALSE,0.0,0.0)
							eCopState[iPedNum] = CS_ARREST_POPPY
						ENDIF
					ENDIF
					
				ELIF iPedNum = 3
					IF IS_ENTITY_AT_ENTITY(mpPolicePed[iPedNum].mPed,mpPoppyPed.mPed,<<10.0,10.0,10.0>>)
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							TASK_AIM_GUN_AT_ENTITY(mpPolicePed[iPedNum].mPed,mpPoppyPed.mPed,-1)
							eCopState[iPedNum] = CS_ARREST_POPPY
						ENDIF
					ENDIF
				ENDIF
				IF iSeqCarDoors = 4	
					//IF GET_GAME_TIMER()	> iTimerSyncSceneStarted + 55000		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
					OR GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene)	= 1	
						IF IS_PED_UNINJURED(mpPolicePed[0].mPed)	
						AND IS_PED_UNINJURED(mpPoppyPed.mPed)	
						AND IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)		
							IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mpPolicePed[0].mPed)
								SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
								SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
							ENDIF
							iSeqCarDoors = 5
							eCopState[iPedNum] = CS_ENTER_COP_CAR
							ePoppyState = PS_POPPY_IN_COP_CAR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE CS_ARREST_POPPY
			IF NOT HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
	 			IF ePoppyState = PS_POPPY_IN_COP_CAR
					IF iPedNum = 0
						//STAY IN THE COP CAR
					ELIF iPedNum = 1
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
								TASK_ENTER_VEHICLE(mpPolicePed[iPedNum].mPed,mvPoliceCars[0].mVehicle,-1,VS_FRONT_RIGHT)
								eCopState[iPedNum] = CS_ENTER_COP_CAR
							ENDIF
						ENDIF
					ELIF iPedNum = 3
						IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
							CLEAR_PED_TASKS(mpPolicePed[iPedNum].mPed)
							IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
								TASK_GO_TO_ENTITY(mpPolicePed[iPedNum].mPed,mvPoppyCar.mVehicle,-1,2.5,PEDMOVEBLENDRATIO_WALK)
								eCopState[iPedNum] = CS_APPROAH_CRASHED_CAR
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF iSeqCarDoors = 4	
					//IF GET_GAME_TIMER()	> iTimerSyncSceneStarted + 55000		
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
					OR GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene)	= 1	
						IF IS_PED_UNINJURED(mpPolicePed[0].mPed)	
						AND IS_PED_UNINJURED(mpPoppyPed.mPed)	
						AND IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)		
							IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(mpPolicePed[0].mPed)
								SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
								SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
							ENDIF
							iSeqCarDoors = 5
							eCopState[iPedNum] = CS_ENTER_COP_CAR
							ePoppyState = PS_POPPY_IN_COP_CAR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK 
		
		CASE CS_ENTER_COP_CAR
			
			IF NOT HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
				//IF BOTH COPS ARE IN THE CAR, DRIVE AWAY
				IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
					IF IS_ENTITY_ALIVE(mpPolicePed[0].mPed) AND IS_ENTITY_ALIVE(mpPolicePed[1].mPed)
						IF IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle) 
						//AND IS_PED_IN_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle)
							IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
								
								IF b_Correct_Pic_Taken = FALSE
									bPoppyAndCopDrivingOff = TRUE
									eCopState[0] = CS_LEAVE_AREA
									eCopState[iPedNum] = CS_LEAVE_AREA
									iTimerCopsLeavingArea = GET_GAME_TIMER()
									EXIT
								ENDIF
								
								IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
									STOP_SYNCHRONIZED_ENTITY_ANIM(mvPoliceCars[0].mVehicle,-1,FALSE)
								ENDIF
								
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mvPoliceCars[0].mVehicle)
								
								SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,FALSE)
								SET_PED_KEEP_TASK(mpPolicePed[0].mPed,TRUE)

								iTimerCopsLeavingArea = GET_GAME_TIMER()
								
								IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
									FREEZE_ENTITY_POSITION(mvPoppyCar.mVehicle,FALSE)
								ENDIF
							
								//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[0].mVehicle,VEHICLELOCK_LOCKED)
							
								//RE-ACTIVATE ROADS - DO THIS HERE SO POLIE CAN LEAVE AREA CORRECTLY IN VEHICLE
								SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, TRUE)
								SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, TRUE)
								SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, TRUE)
								SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, TRUE)
								
								SET_DISABLE_PRETEND_OCCUPANTS(mvPoliceCars[0].mVehicle,TRUE)
								
								TASK_VEHICLE_DRIVE_TO_COORD(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,<< 231.6400, -347.1846, 43.2866 >>,30,DRIVINGSTYLE_STRAIGHTLINE,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_AVOIDCARS,20.0,150.0)		
								//TASK_VEHICLE_DRIVE_WANDER(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,50,DRIVINGMODE_AVOIDCARS_RECKLESS)
								
								//IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
								//	FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
								//ENDIF
								
								SET_VEHICLE_DOORS_SHUT(mvPoliceCars[0].mVehicle)
								
								bPoppyAndCopDrivingOff = TRUE
								
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								
								eCopState[0] = CS_LEAVE_AREA
								
								eCopState[iPedNum] = CS_LEAVE_AREA
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_APPROAH_CRASHED_CAR
			IF HAS_PLAYER_THREATENED_PED(mpPolicePed[iPedNum].mPed)
				//MAKE_POLICE_ATTACK_PLAYER()
			ENDIF
			//COP IS GOING TO CRASHED CAR
		BREAK
		
		CASE CS_LEAVE_AREA
		BREAK
		
		CASE CS_AREA_WARN_PLAYER
	
			IF GET_GAME_TIMER() >= iTimerPicTaken + 5000
				IF IS_ENTITY_ALIVE(mpPoppyPed.mPed)
					IF IS_PED_UNINJURED(mpPolicePed[iPedNum].mPed)
						eCopState[iPedNum] = CS_MOVE_TO_POPPY
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE CS_ATTACK_PLAYER
			//ATTACKING THE PLAYER
		BREAK
		
	ENDSWITCH

ENDPROC

/// PURPOSE:
///  Makes police at road block get into their vehicles once poppy has passed them 
PROC MAKE_COPS_GET_IN_CARS

	IF bCopsAreLeaving = FALSE
		//FIRST CAR
		IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
			IF IS_ENTITY_AT_COORD(mvPoppyCar.mVehicle,<< 859.6609, -122.3895, 78.4099 >>,<<15.0,15.0,15.0>>)
				IF IS_PED_UNINJURED(mpStandingPeds[0].mPed)
					IF IS_VEHICLE_OK(mvParkedCar[5].mVehicle)
						TASK_ENTER_VEHICLE(mpStandingPeds[0].mPed,mvParkedCar[5].mVehicle,-1,VS_DRIVER,3.0)
						eStandingState[0] = SS_GET_INTO_VEHICLE
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(mpStandingPeds[1].mPed)
					IF IS_VEHICLE_OK(mvParkedCar[5].mVehicle)
						TASK_ENTER_VEHICLE(mpStandingPeds[1].mPed,mvParkedCar[5].mVehicle,-1,VS_FRONT_RIGHT,3.0)
						eStandingState[1] = SS_GET_INTO_VEHICLE
					ENDIF
				ENDIF
				
				//SECOND CAR
				IF IS_PED_UNINJURED(mpStandingPeds[4].mPed)
					IF IS_VEHICLE_OK(mvParkedCar[4].mVehicle)
						TASK_ENTER_VEHICLE(mpStandingPeds[4].mPed,mvParkedCar[4].mVehicle,-1,VS_DRIVER,3.0)
						eStandingState[4] = SS_GET_INTO_VEHICLE
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(mpStandingPeds[5].mPed)
					IF IS_VEHICLE_OK(mvParkedCar[4].mVehicle)
						TASK_ENTER_VEHICLE(mpStandingPeds[5].mPed,mvParkedCar[4].mVehicle,-1,VS_FRONT_RIGHT,3.0)
						eStandingState[5] = SS_GET_INTO_VEHICLE
					ENDIF
				ENDIF
				bCopsAreLeaving = TRUE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///   Monitors and controls all the PEDS standing around during the chase.
///   Police at road block, builders etc. 
/// PARAMS:
///    iPedNum - The ped number we are checking
PROC Moinitor_Standing_Ped(INT iPedNum, MODEL_NAMES mnModelType )

	SWITCH eStandingState[iPedNum]
	
		CASE SS_STANDING_AND_WAIT
			IF IS_PED_UNINJURED(mpStandingPeds[iPedNum].mPed)
				IF mnModelType = S_M_Y_Cop_01 // IF THEY ARE POLICE DO BELOW
					IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
						IF IS_ENTITY_AT_ENTITY(mvPoppyCar.mVehicle,mpStandingPeds[iPedNum].mPed,<<70.0,70.0,70.0>>)

							SWITCH iPedNum
	
								CASE 0
									TASK_START_SCENARIO_IN_PLACE(mpStandingPeds[iPedNum].mPed,"WORLD_HUMAN_DRUG_DEALER_HARD")
									eStandingState[iPedNum] = SS_AIM_GUN_AT_POPPY
								BREAK
								
								CASE 1
									TASK_START_SCENARIO_IN_PLACE(mpStandingPeds[iPedNum].mPed,"CODE_HUMAN_POLICE_CROWD_CONTROL")
									eStandingState[iPedNum] = SS_AIM_GUN_AT_POPPY
								BREAK
								
								CASE 4
									TASK_START_SCENARIO_IN_PLACE(mpStandingPeds[iPedNum].mPed,"WORLD_HUMAN_DRUG_DEALER_HARD")
									eStandingState[iPedNum] = SS_AIM_GUN_AT_POPPY
								BREAK
								
								CASE 5
									TASK_START_SCENARIO_IN_PLACE(mpStandingPeds[iPedNum].mPed,"CODE_HUMAN_POLICE_CROWD_CONTROL")
									eStandingState[iPedNum] = SS_AIM_GUN_AT_POPPY
								BREAK

							ENDSWITCH
								
						ENDIF
					ENDIF
				ELSE // THEY ARE NOT POLICE SO NEED TO FLEE IF IN DANGER
					IF IS_ENTITY_AT_ENTITY(mvPoppyCar.mVehicle,mpStandingPeds[iPedNum].mPed,<<30.0,30.0,30.0>>)
					OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpStandingPeds[iPedNum].mPed,<<40.0,40.0,40.0>>)
						MAKE_PED_FLEE(mpStandingPeds[iPedNum].mPed,TRUE)
						eStandingState[iPedNum] = SS_FLEE_FROM_CARS
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_AIM_GUN_AT_POPPY
			//AIMING GUN AT ENTITY
			IF IS_PED_UNINJURED(mpStandingPeds[iPedNum].mPed)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpStandingPeds[iPedNum].mPed,<<15.0,15.0,15.0>>)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_AIM_GUN_AT_ENTITY(mpStandingPeds[iPedNum].mPed,PLAYER_PED_ID(),-1,FALSE)
						eStandingState[iPedNum] = SS_AIM_GUN_AT_PLAYER
					ELSE
						//TALK TO PLAYER
					ENDIF
				ENDIF
				IF bCopCrashed = TRUE
					TASK_WANDER_IN_AREA(mpStandingPeds[iPedNum].mPed,mpStandingPeds[iPedNum].vStartPos,30.0)
					eStandingState[iPedNum] = SS_WANDER_IN_AREA
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_AIM_GUN_AT_PLAYER
			IF IS_PED_UNINJURED(mpStandingPeds[iPedNum].mPed)
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpStandingPeds[iPedNum].mPed,<<40.0,40.0,40.0>>)
					CLEAR_PED_TASKS(mpStandingPeds[iPedNum].mPed)
					eStandingState[iPedNum] = SS_AIM_GUN_AT_POPPY
				ENDIF
				IF IS_PLAYER_INSIDE_ROAD_BLOCK()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_COMBAT_PED(mpStandingPeds[iPedNum].mPed,PLAYER_PED_ID())
						eStandingState[iPedNum] = SS_ATTACK_PLAYER
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ATTACK_PLAYER
			//ATTACKING PLAYER
		BREAK
		
		CASE SS_GET_INTO_VEHICLE
			IF IS_PED_UNINJURED(mpStandingPeds[0].mPed)
			AND IS_PED_UNINJURED(mpStandingPeds[1].mPed)
				IF IS_VEHICLE_OK(mvParkedCar[5].mVehicle)
					IF IS_PED_IN_VEHICLE(mpStandingPeds[0].mPed,mvParkedCar[5].mVehicle)
					AND IS_PED_IN_VEHICLE(mpStandingPeds[1].mPed,mvParkedCar[5].mVehicle)
						TASK_VEHICLE_DRIVE_WANDER(mpStandingPeds[0].mPed,mvParkedCar[5].mVehicle,40.0,DRIVINGMODE_AVOIDCARS)
						eStandingState[0] = SS_WANDER_IN_AREA
						eStandingState[1] = SS_WANDER_IN_AREA
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(mpStandingPeds[4].mPed)
			AND IS_PED_UNINJURED(mpStandingPeds[5].mPed)
				IF IS_VEHICLE_OK(mvParkedCar[4].mVehicle)
					IF IS_PED_IN_VEHICLE(mpStandingPeds[4].mPed,mvParkedCar[4].mVehicle)
					AND IS_PED_IN_VEHICLE(mpStandingPeds[5].mPed,mvParkedCar[4].mVehicle)
						TASK_VEHICLE_DRIVE_WANDER(mpStandingPeds[4].mPed,mvParkedCar[4].mVehicle,40.0,DRIVINGMODE_AVOIDCARS)
						eStandingState[4] = SS_WANDER_IN_AREA
						eStandingState[5] = SS_WANDER_IN_AREA
					ENDIF 
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SS_WANDER_IN_AREA
			//WANDERING
		BREAK
		
		CASE SS_FLEE_FROM_CARS
			//FLEEING FROM CARS
		BREAK
		
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
/// Controls monitoring indivudual PEDS etc
PROC Monitor_Police_Chase_Scene()

	//PRINTINT(iRammingCops)
	//PRINTNL()

	//CHECK PLAYER IS NOT WANTED
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
		MAKE_POLICE_ATTACK_PLAYER()
	ENDIF
	
	IF GET_GAME_TIMER() > iTimerCheckDamage
		IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)	
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoppyCar.mVehicle,PLAYER_PED_ID())	
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mvPoppyCar.mVehicle)
				++iRammingCops
				iTimerCheckDamage = GET_GAME_TIMER() + 2000
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)	
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[0].mVehicle,PLAYER_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mvPoliceCars[0].mVehicle)
				++iRammingCops
				iTimerCheckDamage = GET_GAME_TIMER() + 2000
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),mvPoliceCars[0].mVehicle)	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					++iRammingCops
					iTimerCheckDamage = GET_GAME_TIMER() + 2000
				ENDIF
			ENDIF
		ENDIF
		IF NOT bCopCrashed
			IF IS_ENTITY_ALIVE(mvPoliceCars[1].mVehicle)	
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[1].mVehicle,PLAYER_PED_ID())		
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mvPoliceCars[1].mVehicle)
					++iRammingCops
					iTimerCheckDamage = GET_GAME_TIMER() + 2000
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),mvPoliceCars[1].mVehicle)	
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						++iRammingCops
						iTimerCheckDamage = GET_GAME_TIMER() + 2000
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)	
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mvPoliceCars[2].mVehicle,PLAYER_PED_ID())			
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mvPoliceCars[2].mVehicle)
				++iRammingCops
				iTimerCheckDamage = GET_GAME_TIMER() + 2000
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),mvPoliceCars[2].mVehicle)	
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					++iRammingCops
					iTimerCheckDamage = GET_GAME_TIMER() + 2000
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF iRammingCops >= 5
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
			HANG_UP_AND_PUT_AWAY_PHONE()
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mpPoppyPed.mPed)	
		IF IS_ENTITY_ALIVE(mpPoppyPed.mPed)
			IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),TRUE)
				Monitor_Poppy()
				IF bPoppyHasCrashed = FALSE
					IF ePoppyState = PS_DRIVING
						IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
							AND GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 15000
								IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,<<LOSE_DISTANCE,LOSE_DISTANCE,LOSE_DISTANCE>>)
									SET_FAIL_REASON(FAIL_POPPY_LOST)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF ePoppyState <> PS_POPPY_IN_COP_CAR
					 	IF b_Correct_Pic_Taken = FALSE
							IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,<<LOSE_DISTANCE,LOSE_DISTANCE,LOSE_DISTANCE>>)
								SET_FAIL_REASON(FAIL_PHOTO_LOST)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				SET_FAIL_REASON(FAIL_POPPY_INJURED)
			ENDIF
		ELSE
			SET_FAIL_REASON(FAIL_POPPY_KILLED)
		ENDIF
	ENDIF
	
	//CHECK POLICE
	/*
	INT i
	
	FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
		IF IS_ENTITY_ALIVE(mpPolicePed[i].mPed)
			MONITOR_POLICE_PEDS(i)
		ENDIF
	ENDFOR
	*/
	/*
	FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
		IF DOES_ENTITY_EXIST(mpsChaseProps[i].mObject)
			IF NOT DOES_ENTITY_HAVE_PHYSICS(mpsChaseProps[i].mObject) = FALSE
				ACTIVATE_PHYSICS(mpsChaseProps[i].mObject)
			ENDIF
			//FREEZE_ENTITY_POSITION(mpsChaseProps[i].mObject,FALSE)
		ENDIF
	ENDFOR
	*/
	
	//MAKES ROAD BLOCK COPS GET INTO CARS
	//MAKE_COPS_GET_IN_CARS()
	
	//CHECK STANDING PEDS
	/*
	FOR i = 0 TO NUM_OF_STANDING_PEDS - 1
		IF IS_ENTITY_ALIVE(mpStandingPeds[i].mPed)
			Moinitor_Standing_Ped(i, mpStandingPeds[i].mModel )
		ENDIF
	ENDFOR
	*/	
ENDPROC

/// PURPOSE:
/// Manages dialogue that is said during the chase with Poppy. 
PROC CHASE_DIALOGUE()
	
	SWITCH iDialogueToPlay
	
		CASE 0
			IF IS_PED_UNINJURED(mpPoppyPed.mPed)
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,150.0)	
				AND NOT IS_ENTITY_OCCLUDED(mpPoppyPed.mPed)	
					ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
					//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_CAT")
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_03")
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_1", CONV_PRIORITY_MEDIUM)
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_POPPY_LOCATION,"At Poppy location") // mocap done, set checkpoint
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
							iDialogueToPlay++
							//bDoneFakeStart = TRUE
						ENDIF
					ELSE
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_1", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_POPPY_LOCATION,"At Poppy location") // mocap done, set checkpoint
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
							iDialogueToPlay++
							//bDoneFakeStart = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
						IF fTimePosInRec > 30000
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_POPPY_LOCATION,"At Poppy location") // mocap done, set checkpoint
							iDialogueToPlay++
							//bDoneFakeStart = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 1
			/*
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 748.0605, -23.1331, 81.2415 >>,<<50.0,50.0,50.0>>)
				ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_03")	
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_2", CONV_PRIORITY_MEDIUM)
						iDialogueToPlay++
					ENDIF
				ELSE
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_2", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
						iDialogueToPlay++
					ENDIF
				ENDIF
			ENDIF
			*/
			iDialogueToPlay++
		BREAK
		
		CASE 2
			IF IS_PED_UNINJURED(mpPoppyPed.mPed)
				//IF IS_ENTITY_AT_COORD(mpPoppyPed.mPed,<< 640.2135, -83.7324, 73.8663 >>,<<30.0,30.0,30.0>>)
				IF fTimePosInRec >= 83760.765625	
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,<<70.0,70.0,70.0>>)
						ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_03")		
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_3", CONV_PRIORITY_MEDIUM)
								iDialogueToPlay++
							ENDIF
						ELSE
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_3", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
								iDialogueToPlay++
							ENDIF
						ENDIF
					ELSE
						iDialogueToPlay++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_ENTITY_AT_COORD(mpPoppyPed.mPed,<< 512.8621, -187.7797, 52.1896 >>,<<30.0,30.0,30.0>>)
				IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
					ADD_PED_FOR_DIALOGUE(sSpeach, 7, mpPolicePed[0].mPed, "Paparazzo3ACop3")
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_03")	
						IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_COP7", CONV_PRIORITY_HIGH)
							iDialogueToPlay++
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_COP7", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
							iDialogueToPlay++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				REMOVE_PED_FOR_DIALOGUE(sSpeach,7)
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///  Handles slowing the player down in a vehicle and getting him out
///  Only works if player is near crash site
PROC HANDLE_STOPPING_BEFORE_CRASH()
 
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 28.0
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vCrashLocation,<<47.0,47.0,47.0>>)
				IF bCarStopping = FALSE
					//BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),20.0,1)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),37.0,1)  //20.0
					bCarStopping = TRUE
				ELSE
					IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.1
						bPlayerStopped = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vCrashLocation,<<40.0,40.0,40.0>>)
				IF bCarStopping = FALSE	
					//BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),20.0,1)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),30.0,1)
					bCarStopping = TRUE
				ELSE
					IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.1
						bPlayerStopped = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///  Handles Princess scene - Loads models, checks they are loaded.
///  Also checks if everything is ok once created.
PROC HANDLE_POLICE_CHASE()

	//INT i 

  	IF bPoliceChaseCreated = FALSE
  
   		IF HAS_POLICE_SCENE_LOADED()
			/*
			FOR i = 0 TO NUM_OF_POLICE_CARS - 1
	      		CreateMissionVehicle(mvPoliceCars[i],FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoliceCars[i].mModel,TRUE)
			ENDFOR
			*/
			CreateMissionVehicle(mvPoppyCar)
			//SET_VEHICLE_COLOUR_COMBINATION(mvPoppyCar.mVehicle,6) //5 (baby blue)  //6 (yellow)
			SET_VEHICLE_DOORS_LOCKED(mvPoppyCar.mVehicle,VEHICLELOCK_LOCKED)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoppyCar.mModel,TRUE)
			//SET_ENTITY_LOD_DIST(mvPoppyCar.mVehicle,220)
			SET_VEHICLE_LOD_MULTIPLIER(mvPoppyCar.mVehicle,2.5)
			
			SET_VEHICLE_MOD_KIT(mvPoppyCar.mVehicle,0)
			
			SET_VEHICLE_NUMBER_PLATE_TEXT(mvPoppyCar.mVehicle,"P0PPYM15")
			LOWER_CONVERTIBLE_ROOF(mvPoppyCar.mVehicle,TRUE)

			//SET_VEHICLE_COLOURS(mvPoppyCar.mVehicle,42,88)
			//SET_VEHICLE_EXTRA_COLOURS(mvPoppyCar.mVehicle,0,156)  //yellow
			SET_VEHICLE_COLOURS(mvPoppyCar.mVehicle,137,137)      //135 hot pink
			SET_VEHICLE_EXTRA_COLOURS(mvPoppyCar.mVehicle,0,0)
			
			SET_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_ENGINE,2)
			SET_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_BRAKES,0)
			SET_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_HORN,3)
			SET_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_WHEELS,6)

			TOGGLE_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_TOGGLE_TURBO,TRUE)
			
			CreateMissionVehicle(mvPoliceCars[0],FALSE)
			CreateMissionVehicle(mvPoliceCars[1],FALSE)
			CreateMissionVehicle(mvPoliceCars[2],FALSE)
			SET_ENTITY_INVINCIBLE(mvPoliceCars[0].mVehicle,TRUE)       
			SET_ENTITY_INVINCIBLE(mvPoliceCars[1].mVehicle,TRUE)       
			SET_ENTITY_INVINCIBLE(mvPoliceCars[2].mVehicle,TRUE)       
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoliceCars[0].mModel,TRUE)
			
			CreateMissionPedInVehicle(mpPolicePed[0],mvPoliceCars[0],PEDTYPE_COP,FALSE,VS_DRIVER)   //PEDTYPE_COP
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[0].mPed,TRUE)
			//SET_PED_CONFIG_FLAG(mpPolicePed[0].mPed,PCF_DontInfluenceWantedLevel,TRUE)
			CreateMissionPedInVehicle(mpPolicePed[1],mvPoliceCars[0],PEDTYPE_COP,FALSE,VS_FRONT_RIGHT)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[1].mPed,TRUE)
			//SET_PED_CONFIG_FLAG(mpPolicePed[1].mPed,PCF_DontInfluenceWantedLevel,TRUE)
			CreateMissionPedInVehicle(mpPolicePed[2],mvPoliceCars[1],PEDTYPE_COP,FALSE,VS_DRIVER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[2].mPed,TRUE)
			//SET_PED_CONFIG_FLAG(mpPolicePed[2].mPed,PCF_DontInfluenceWantedLevel,TRUE)
			CreateMissionPedInVehicle(mpPolicePed[3],mvPoliceCars[2],PEDTYPE_COP,FALSE,VS_DRIVER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[3].mPed,TRUE)
			//SET_PED_CONFIG_FLAG(mpPolicePed[3].mPed,PCF_DontBehaveLikeLaw,TRUE)
			
			//TOGGLE_VEHICLE_MOD(mvPoppyCar.mVehicle,MOD_TOGGLE_XENON_LIGHTS,TRUE)
			/*
			//ALL CARS THAT ARE PARKED DURING CHASE
			FOR i = 0 TO NUM_OF_PARKED_CARS - 1
				CreateMissionVehicle(mvParkedCar[i],FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvParkedCar[i].mModel,TRUE)
			ENDFOR
			*/
			/*
			//PROPS
			FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
				CreateMissionProp(mpsChaseProps[i],FALSE)
				//SET_VEHICLE_MODEL_IS_SUPPRESSED(mpsChaseProps[i].mModel,TRUE)
			ENDFOR
			
			CREATE_FORCED_OBJECT(<< 634.4785, -68.8406, 74.2911 >>,5,PROP_BARRIER_WORK04A,TRUE)
			CREATE_FORCED_OBJECT(<< 627.1672, -73.8742, 73.3072 >>,5,PROP_BARRIER_WORK04A,TRUE)
			CREATE_FORCED_OBJECT(<< 653.2055, -96.2269, 73.4953 >>,5,PROP_BARRIER_WORK04A,TRUE)
			CREATE_FORCED_OBJECT(<< 657.0881, -91.6722, 73.5133 >>,5,PROP_BARRIER_WORK04A,TRUE)
			*/
			/*
			SET_VEHICLE_SIREN(mvParkedCar[4].mVehicle,TRUE)
			SET_SIREN_WITH_NO_DRIVER(mvParkedCar[4].mVehicle,TRUE)
			SET_VEHICLE_SIREN(mvParkedCar[5].mVehicle,TRUE)
			SET_SIREN_WITH_NO_DRIVER(mvParkedCar[5].mVehicle,TRUE)
		  	*/
		  	//POLICE PEDS
		  	/*
			
			CreateMissionVehicle(mvPoliceCars[i],FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoliceCars[i].mModel,TRUE)
			
			CreateMissionPedInVehicle(mpPolicePed[0],mvPoliceCars[0],PEDTYPE_COP,FALSE,VS_DRIVER)
			CreateMissionPedInVehicle(mpPolicePed[1],mvPoliceCars[0],PEDTYPE_COP,FALSE,VS_FRONT_RIGHT)
			CreateMissionPedInVehicle(mpPolicePed[2],mvPoliceCars[1],PEDTYPE_COP,FALSE,VS_DRIVER)
			CreateMissionPedInVehicle(mpPolicePed[3],mvPoliceCars[2],PEDTYPE_COP,FALSE,VS_DRIVER)
			*/
			//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[0].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[1].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[2].mVehicle,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			/*
			FOR i = 0 TO NUM_OF_STANDING_PEDS - 1
				IF i <> 2
				AND i <> 3
					CreateMissionPed(mpStandingPeds[i],FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpStandingPeds[i].mPed,TRUE)
					IF mpStandingPeds[i].mModel = S_M_Y_Cop_01
						GIVE_WEAPON_TO_PED(mpStandingPeds[i].mPed,WEAPONTYPE_PISTOL,-1,TRUE)
					ENDIF
				ENDIF
			ENDFOR
			*/
			//EQUIP COPS WITH WEAPONS - USED DURING ARREST SCENE 
			/*
			FOR i = 0 TO NUM_OF_POLICE_PEDS - 1
				GIVE_WEAPON_TO_PED(mpPolicePed[i].mPed,WEAPONTYPE_PISTOL,-1,FALSE,TRUE)
				//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPolicePed[i].mPed,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(mpPolicePed[i].mPed,relCopGroup)
			ENDFOR
			*/
		  	//POPPY PED
			CreateMissionPedInVehicle(mpPoppyPed,mvPoppyCar,PEDTYPE_MISSION,FALSE,VS_DRIVER)
			SET_PED_RELATIONSHIP_GROUP_HASH(mpPoppyPed.mPed,relPoppy)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPoppyPed.mPed,TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(mpPoppyPed.mPed,relCopGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPoppy,RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,relPoppy)
			SET_ENTITY_LOAD_COLLISION_FLAG(mpPoppyPed.mPed,TRUE)
	      	SET_PED_MODEL_IS_SUPPRESSED(mpPoppyPed.mModel,TRUE)
			SET_PED_COMPONENT_VARIATION(mpPoppyPed.mPed, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mpPoppyPed.mPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(mpPoppyPed.mPed, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mpPoppyPed.mPed, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(mpPoppyPed.mPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			bPoliceChaseCreated = TRUE
			
    	ENDIF
  	ELSE
  		Monitor_Police_Chase_Scene()
  	ENDIF
	
ENDPROC

/// PURPOSE:
/// Checks to see if the player is interfering with the arrest scene
/// If he is cops will warn player, if he gets closer they will attack. 
PROC ARREST_SCENE_AREA_CHECK()

	VECTOR vCopCar
	FLOAT fAttackRange = 6
	
	IF bPhotoSent
		fAttackRange = 4.5
	ENDIF
	
	VEHICLE_INDEX vehPlayersLastVehicle = GET_PLAYERS_LAST_VEHICLE() //For B*1765742 - do the same area check for the player's last vehicle (or they can mess up the scene by bailing out)
	
	IF bPoppyAndCopDrivingOff = FALSE	
		IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)
			IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
				IF IS_PED_UNINJURED(mpPoppyPed.mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPolicePed[0].mPed,9)
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,9)
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle,9)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 343.1912, -402.2201, 44.2223 >>,<<9,9,9>>)	
						IF NOT bAmbDialogue
							//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mpPolicePed[0].mPed,"PAP3A_AHAA","Paparazzo3ACop3","SPEECH_PARAMS_STANDARD")
							bAmbDialogue = TRUE
						ENDIF
					ENDIF
					//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<347.348358,-414.645081,42.954491>>, <<350.870605,-397.467957,48.116890>>, 16.750000)
					
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
					OR GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) >= 0.514839
						IF HAS_PLAYER_THREATENED_PED(mpPoppyPed.mPed)	
							MAKE_POLICE_ATTACK_PLAYER()
						ENDIF
					ENDIF
					
					//For B*1765742 - do the same area check for the player's last vehicle (or they can mess up the scene by bailing out)
					IF DOES_ENTITY_EXIST(vehPlayersLastVehicle) 
					AND NOT IS_ENTITY_DEAD(vehPlayersLastVehicle)
						IF IS_ENTITY_IN_RANGE_ENTITY(vehPlayersLastVehicle,mpPolicePed[0].mPed,fAttackRange)
						OR IS_ENTITY_IN_RANGE_ENTITY(vehPlayersLastVehicle,mpPoppyPed.mPed,fAttackRange)
						OR IS_ENTITY_IN_RANGE_ENTITY(vehPlayersLastVehicle,mvPoliceCars[0].mVehicle,fAttackRange)
						OR IS_ENTITY_AT_COORD(vehPlayersLastVehicle,<< 343.1912, -402.2201, 44.2223 >>,<<fAttackRange -1.5,fAttackRange -1.5,fAttackRange -1.5>>)
							MAKE_POLICE_ATTACK_PLAYER()
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPolicePed[0].mPed,fAttackRange)
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,fAttackRange)
					OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle,fAttackRange)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 343.1912, -402.2201, 44.2223 >>,<<fAttackRange -1.5,fAttackRange -1.5,fAttackRange -1.5>>)
					OR HAS_PLAYER_THREATENED_PED(mpPolicePed[0].mPed)	
					OR HAS_PLAYER_THREATENED_PED(mpPolicePed[1].mPed)
						MAKE_POLICE_ATTACK_PLAYER()
					ENDIF
					IF iSeqCarDoors > 0
					AND bCrashCutDone = TRUE	
						IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
						AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) < 0.9	
							IF bCopWarning = FALSE
								//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<347.348358,-414.645081,42.954491>>, <<350.870605,-397.467957,48.116890>>, 25.750)

								IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPolicePed[0].mPed,11)
								OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,11)
								OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle,11)
								//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 343.1912, -402.2201, 44.2223 >>,<<8,8,8>>)
									
									IF NOT IS_ANY_SPEECH_PLAYING(mpPolicePed[1].mPed)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mpPolicePed[1].mPed,"PAP3A_APAA","Paparazzo3ACop3","SPEECH_PARAMS_FORCE")	
									ELSE
										bCopWarning = TRUE
									ENDIF
									
									/*
									GIVE_WEAPON_TO_PED(mpPolicePed[1].mPed,WEAPONTYPE_PISTOL,-1,FALSE,FALSE)
									GIVE_WEAPON_COMPONENT_TO_PED(mpPolicePed[1].mPed,WEAPONTYPE_PISTOL,WEAPONCOMPONENT_AT_PI_FLSH)
									SEQUENCE_INDEX seqCop
									OPEN_SEQUENCE_TASK(seqCop)
										TASK_LEAVE_ANY_VEHICLE(NULL)
										TASK_AIM_GUN_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
									CLOSE_SEQUENCE_TASK(seqCop)
									TASK_PERFORM_SEQUENCE(mpPolicePed[1].mPed,seqCop)
									CLEAR_SEQUENCE_TASK(seqCop)
									*/
									
								ENDIF
							ELSE
								//IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<347.348358,-414.645081,42.954491>>, <<350.870605,-397.467957,48.116890>>, 25.750)
								
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPolicePed[0].mPed,14)
								AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,14)
								AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle,14)
								//AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 343.1912, -402.2201, 44.2223 >>,<<8,8,8>>)
									IF NOT IS_PED_IN_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle)
										IF GET_SCRIPT_TASK_STATUS(mpPolicePed[1].mPed,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
											TASK_ENTER_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,-1,VS_FRONT_RIGHT)
											//bCopWarning = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_IN_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle)
								IF GET_SCRIPT_TASK_STATUS(mpPolicePed[1].mPed,SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK	
									TASK_ENTER_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,-1,VS_FRONT_RIGHT)
								ENDIF
							ENDIF
						ENDIF
						IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPolicePed[0].mPed,15)
						OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,15)	
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 14//12.0
									IF IS_PED_FACING_PED(PLAYER_PED_ID(),mpPolicePed[0].mPed,10)
									OR IS_PED_FACING_PED(PLAYER_PED_ID(),mpPoppyPed.mPed,10)
										MAKE_POLICE_ATTACK_PLAYER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bCrashCutDone = TRUE
				IF iSeqCarDoors < 4
					vCopCar = GET_ENTITY_COORDS(mvPoliceCars[0].mVehicle)
					IF vCopCar.x < vPoliceCarPos.x - 0.2
					OR vCopCar.x > vPoliceCarPos.x + 0.2
					OR vCopCar.y < vPoliceCarPos.y - 0.2
					OR vCopCar.y > vPoliceCarPos.y + 0.2	
						//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),1)
						MAKE_POLICE_ATTACK_PLAYER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
			MAKE_POLICE_ATTACK_PLAYER()
		ENDIF
	ENDIF
	
ENDPROC

/// ************PHONE HELP STUFF**************** ///

/// PURPOSE:
///  Manages the phone help text for the player   
PROC MANAGE_PHONE_HELP()

	IF bSimpleHelp = FALSE
	
		SWITCH iHelp 
			
			CASE 0 //Tells player to access the phone
				
				bHelpPrint2 = FALSE
				bHelpPrint3 = FALSE
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP1")
						IF bHelpPrint1 = FALSE	
							PRINT_HELP("PAP3_HELP1") // Access the phone using ~PAD_DPAD_UP~.
							bHelpPrint1 = TRUE
						ENDIF
					ENDIF
				ELSE
					bHelpPrint1 = FALSE	
					iHelp ++
				ENDIF
				
			BREAK
			
			CASE 1 // Tells player to select the camera once phone is up
			
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) = 0
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP2")
						IF bHelpPrint2 = FALSE	
							PRINT_HELP("PAP3_HELP2") // Select the camera from the phone with ~PAD_DPAD_ALL~.
							bHelpPrint2 = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP2") 
						CLEAR_HELP()
					ENDIF
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						iHelp ++
					ENDIF
				ENDIF
				IF NOT IS_PHONE_ONSCREEN()
					iHelp = 0
				ENDIF
				
			BREAK
				
			CASE 2 // Tells player to frame the shot and take picture
			
				IF b_Correct_Pic_Taken = TRUE
					iHelp ++
				ELSE
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) = 0
						bHelpPrint2 = FALSE
						bHelpPrint3 = FALSE
						iHelp = 1	
					ELIF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP3")
							IF bHelpPrint3 = FALSE	
								//PRINT_HELP("PAP3_HELP3")//Frame your shot then take the picture with ~PAD_A~.
								bHelpPrint3 = TRUE
							ENDIF
						//ENDIF
					ElIF NOT IS_PHONE_ONSCREEN()
						iHelp = 0
					ENDIF
				ENDIF
					
			BREAK
				
			CASE 3 // PHOTO HAS BEEN TAKEN - WAITING FOR PLAYER TO SEND
			BREAK
		
		ENDSWITCH
	ELSE // Switches to simple phone help if player has used camera before.
		IF b_Correct_Pic_Taken = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP6") 
					IF bPrintNonSimpleHelp = FALSE	
						PRINT_HELP("PAP3_HELP6") // Use you phone to take the picture
						bPrintNonSimpleHelp = TRUE	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELP6") // Use you phone to take the picture
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

///
///    MAIN MISSION FLOW FUNCTIONS / PROCEDURES
///    

/// PURPOSE:
/// Handles initial phone call to the player
PROC INITIAL_PHONE_CALL() 
/*
	iPlaybackProgress = iPlaybackProgress
	fPlayBackSpeed = 1
	SWITCH iRecordingProgress
	   CASE 0
	      INIT_UBER_RECORDING("PAP3A2")
	      iRecordingProgress++
	   BREAK
	   
	   CASE 1
	      UPDATE_UBER_RECORDING()
		  CLEAR_AREA_OF_VEHICLES(<<260.3405, 339.6353, 104.5707>>,40)
	   BREAK
	ENDSWITCH	
*/
/*
bSetPieceCarsDontSwitchToAI = TRUE	
		IF HAS_POLICE_SCENE_LOADED()
			REQUEST_VEHICLE_RECORDING(300,"PAP3A2")
			//REQUEST_VEHICLE_RECORDING(500,"PAP3A2")
		
			
			IF NOT DOES_ENTITY_EXIST(mvPoppyCar.mVehicle)	
				CreateMissionVehicle(mvPoppyCar)
				SET_VEHICLE_COLOUR_COMBINATION(mvPoppyCar.mVehicle,5) //4
				SET_VEHICLE_DOORS_LOCKED(mvPoppyCar.mVehicle,VEHICLELOCK_LOCKED)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoppyCar.mModel,TRUE)
				SET_ENTITY_LOD_DIST(mvPoppyCar.mVehicle,180)
				SET_VEHICLE_NUMBER_PLATE_TEXT(mvPoppyCar.mVehicle,"P0PPYM15")
				LOWER_CONVERTIBLE_ROOF(mvPoppyCar.mVehicle, TRUE)
				blipPoppyCar = CREATE_VEHICLE_BLIP(mvPoppyCar.mVehicle,TRUE)
				SET_BLIP_ROUTE(blipPoppyCar,TRUE)
			ENDIF
				
			IF NOT DOES_ENTITY_EXIST(mpPoppyPed.mPed)
				CreateMissionPedInVehicle(mpPoppyPed,mvPoppyCar,PEDTYPE_MISSION,FALSE,VS_DRIVER)
				SET_PED_RELATIONSHIP_GROUP_HASH(mpPoppyPed.mPed,relPoppy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mpPoppyPed.mPed,TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(mpPoppyPed.mPed,relCopGroup)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,relPoppy,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,RELGROUPHASH_PLAYER,relPoppy)
				SET_ENTITY_LOAD_COLLISION_FLAG(mpPoppyPed.mPed,TRUE)
		      	SET_PED_MODEL_IS_SUPPRESSED(mpPoppyPed.mModel,TRUE)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(mvPoliceCars[0].mVehicle)
	      		CreateMissionVehicle(mvPoliceCars[0],FALSE)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mvPoliceCars[0].mModel,TRUE)

			
		
		  		CreateMissionPedInVehicle(mpPolicePed[0],mvPoliceCars[0],PEDTYPE_COP,FALSE,VS_DRIVER)
			ENDIF
			
		ENDIF
			
			//fPlaybackSpeed = 0.9

//WHILE (TRUE)
IF DOES_ENTITY_EXIST(mvPoppyCar.mVehicle)	    
AND DOES_ENTITY_EXIST(mpPoppyPed.mPed)	 
AND DOES_ENTITY_EXIST(mvPoliceCars[0].mVehicle)	  
AND HAS_VEHICLE_RECORDING_BEEN_LOADED(300,"PAP3A2")	 
//AND HAS_VEHICLE_RECORDING_BEEN_LOADED(500,"PAP3A2")	
	  SWITCH iPlaybackProgress

          // **** INITIALISE UBER PLAYBACK ****
          CASE 0
	      bSetPieceCarsDontSwitchToAI = TRUE	
		  INIT_UBER_RECORDING("PAP3A2")      
		  bSetPieceCarsDontSwitchToAI = TRUE	 
		   INITIALISE_UBER_PLAYBACK("PAP3A2",300)
		bSetPieceCarsDontSwitchToAI = TRUE	


TrafficCarPos[0] = <<511.2060, 261.3451, 102.3948>>
TrafficCarQuatX[0] = -0.0108
TrafficCarQuatY[0] = -0.0073
TrafficCarQuatZ[0] = 0.8187
TrafficCarQuatW[0] = -0.5740
TrafficCarRecording[0] = 1
TrafficCarStartime[0] = 4830.0000
TrafficCarModel[0] = DUMMY_MODEL_FOR_SCRIPT //buccaneer

TrafficCarPos[1] = <<439.2651, 292.9166, 102.4979>>
TrafficCarQuatX[1] = -0.0095
TrafficCarQuatY[1] = 0.0129
TrafficCarQuatZ[1] = 0.5734
TrafficCarQuatW[1] = 0.8191
TrafficCarRecording[1] = 2
TrafficCarStartime[1] = 7272.0000
TrafficCarModel[1] = washington

TrafficCarPos[2] = <<402.5407, 299.8293, 102.4301>>
TrafficCarQuatX[2] = -0.0149
TrafficCarQuatY[2] = -0.0090
TrafficCarQuatZ[2] = 0.8170
TrafficCarQuatW[2] = -0.5764
TrafficCarRecording[2] = 3
TrafficCarStartime[2] = 8262.0000
TrafficCarModel[2] = cogcabrio

TrafficCarPos[3] = <<366.4927, 309.5941, 103.0008>>
TrafficCarQuatX[3] = -0.0070
TrafficCarQuatY[3] = -0.0165
TrafficCarQuatZ[3] = 0.7886
TrafficCarQuatW[3] = -0.6146
TrafficCarRecording[3] = 4
TrafficCarStartime[3] = 9318.0000
TrafficCarModel[3] = asterope

TrafficCarPos[4] = <<216.6715, 353.7352, 105.7779>>
TrafficCarQuatX[4] = -0.0200
TrafficCarQuatY[4] = -0.0268
TrafficCarQuatZ[4] = 0.8125
TrafficCarQuatW[4] = -0.5820
TrafficCarRecording[4] = 5
TrafficCarStartime[4] = 13542.0000
TrafficCarModel[4] = Baller

TrafficCarPos[5] = <<277.4835, 419.0747, 118.9695>>
TrafficCarQuatX[5] = -0.0142
TrafficCarQuatY[5] = -0.0284
TrafficCarQuatZ[5] = 0.8705
TrafficCarQuatW[5] = -0.4911
TrafficCarRecording[5] = 6
TrafficCarStartime[5] = 15984.0000
TrafficCarModel[5] = asterope

TrafficCarPos[6] = <<237.4069, 472.6873, 124.6924>>
TrafficCarQuatX[6] = -0.0221
TrafficCarQuatY[6] = -0.0790
TrafficCarQuatZ[6] = 0.9966
TrafficCarQuatW[6] = -0.0111
TrafficCarRecording[6] = 7
TrafficCarStartime[6] = 17370.0000
TrafficCarModel[6] = bullet

TrafficCarPos[7] = <<256.0805, 540.5441, 140.1842>>
TrafficCarQuatX[7] = -0.0117
TrafficCarQuatY[7] = -0.0054
TrafficCarQuatZ[7] = 0.8057
TrafficCarQuatW[7] = 0.5922
TrafficCarRecording[7] = 8
TrafficCarStartime[7] = 20208.0000
TrafficCarModel[7] = taxi

TrafficCarPos[8] = <<282.6450, 612.2068, 154.1533>>
TrafficCarQuatX[8] = 0.0177
TrafficCarQuatY[8] = -0.0114
TrafficCarQuatZ[8] = 0.4243
TrafficCarQuatW[8] = 0.9053
TrafficCarRecording[8] = 9
TrafficCarStartime[8] = 23177.0000
TrafficCarModel[8] = asterope

TrafficCarPos[9] = <<273.0108, 629.6464, 155.7711>>
TrafficCarQuatX[9] = -0.0256
TrafficCarQuatY[9] = -0.0694
TrafficCarQuatZ[9] = 0.9907
TrafficCarQuatW[9] = 0.1141
TrafficCarRecording[9] = 10
TrafficCarStartime[9] = 23639.0000
TrafficCarModel[9] = Baller

TrafficCarPos[10] = <<283.0059, 647.7762, 158.7737>>
TrafficCarQuatX[10] = 0.1093
TrafficCarQuatY[10] = -0.0014
TrafficCarQuatZ[10] = -0.1659
TrafficCarQuatW[10] = 0.9801
TrafficCarRecording[10] = 11
TrafficCarStartime[10] = 24629.0000
TrafficCarModel[10] = buccaneer

TrafficCarPos[11] = <<279.8875, 654.1951, 160.2324>>
TrafficCarQuatX[11] = -0.0411
TrafficCarQuatY[11] = -0.1096
TrafficCarQuatZ[11] = 0.9777
TrafficCarQuatW[11] = 0.1744
TrafficCarRecording[11] = 12
TrafficCarStartime[11] = 24893.0000
TrafficCarModel[11] = feltzer2

TrafficCarPos[12] = <<339.0951, 462.1592, 148.3102>>
TrafficCarQuatX[12] = 0.0028
TrafficCarQuatY[12] = -0.0627
TrafficCarQuatZ[12] = 0.9664
TrafficCarQuatW[12] = -0.2492
TrafficCarRecording[12] = 13
TrafficCarStartime[12] = 30523.0000
TrafficCarModel[12] = baller2

TrafficCarPos[13] = <<472.2878, 393.8821, 138.2691>>
TrafficCarQuatX[13] = 0.0207
TrafficCarQuatY[13] = 0.0216
TrafficCarQuatZ[13] = 0.2261
TrafficCarQuatW[13] = 0.9736
TrafficCarRecording[13] = 14
TrafficCarStartime[13] = 37056.0000
TrafficCarModel[13] = feltzer2

TrafficCarPos[14] = <<511.7101, 333.8549, 131.9026>>
TrafficCarQuatX[14] = 0.0424
TrafficCarQuatY[14] = 0.0430
TrafficCarQuatZ[14] = 0.6257
TrafficCarQuatW[14] = 0.7777
TrafficCarRecording[14] = 15
TrafficCarStartime[14] = 38508.0000
TrafficCarModel[14] = cogcabrio

TrafficCarPos[15] = <<602.7618, 355.3197, 118.5270>>
TrafficCarQuatX[15] = -0.0574
TrafficCarQuatY[15] = 0.0440
TrafficCarQuatZ[15] = -0.5245
TrafficCarQuatW[15] = 0.8483
TrafficCarRecording[15] = 16
TrafficCarStartime[15] = 44316.0000
TrafficCarModel[15] = asterope

TrafficCarPos[16] = <<785.3267, 340.3435, 115.3910>>
TrafficCarQuatX[16] = -0.0268
TrafficCarQuatY[16] = -0.0051
TrafficCarQuatZ[16] = 0.7557
TrafficCarQuatW[16] = 0.6544
TrafficCarRecording[16] = 17
TrafficCarStartime[16] = 49794.0000
TrafficCarModel[16] = DUMMY_MODEL_FOR_SCRIPT //Baller

TrafficCarPos[17] = <<845.4529, 360.0610, 117.3146>>
TrafficCarQuatX[17] = 0.0148
TrafficCarQuatY[17] = 0.0047
TrafficCarQuatZ[17] = -0.4289
TrafficCarQuatW[17] = 0.9032
TrafficCarRecording[17] = 18
TrafficCarStartime[17] = 51642.0000
TrafficCarModel[17] = cogcabrio

TrafficCarPos[18] = <<880.3807, 403.1809, 118.6872>>
TrafficCarQuatX[18] = -0.0194
TrafficCarQuatY[18] = -0.0024
TrafficCarQuatZ[18] = 0.9411
TrafficCarQuatW[18] = 0.3375
TrafficCarRecording[18] = 19
TrafficCarStartime[18] = 53292.0000
TrafficCarModel[18] = baller2

TrafficCarPos[19] = <<911.8809, 467.8841, 120.2237>>
TrafficCarQuatX[19] = -0.0008
TrafficCarQuatY[19] = -0.0055
TrafficCarQuatZ[19] = 0.9990
TrafficCarQuatW[19] = 0.0435
TrafficCarRecording[19] = 20
TrafficCarStartime[19] = 55536.0000
TrafficCarModel[19] = coquette

TrafficCarPos[20] = <<916.1319, 504.1710, 119.8686>>
TrafficCarQuatX[20] = -0.0102
TrafficCarQuatY[20] = 0.0531
TrafficCarQuatZ[20] = 0.0939
TrafficCarQuatW[20] = 0.9941
TrafficCarRecording[20] = 21
TrafficCarStartime[20] = 56657.0000
TrafficCarModel[20] = cogcabrio

TrafficCarPos[21] = <<1072.1567, 441.7333, 91.5848>>
TrafficCarQuatX[21] = 0.0051
TrafficCarQuatY[21] = -0.0205
TrafficCarQuatZ[21] = 0.9189
TrafficCarQuatW[21] = -0.3940
TrafficCarRecording[21] = 22
TrafficCarStartime[21] = 65264.0000
TrafficCarModel[21] = feltzer2

TrafficCarPos[22] = <<1109.0264, 416.9391, 83.1078>>
TrafficCarQuatX[22] = 0.0054
TrafficCarQuatY[22] = -0.0016
TrafficCarQuatZ[22] = 0.9268
TrafficCarQuatW[22] = 0.3756
TrafficCarRecording[22] = 23
TrafficCarStartime[22] = 66584.0000
TrafficCarModel[22] = cogcabrio

TrafficCarPos[23] = <<1120.3549, 421.6550, 82.8984>>
TrafficCarQuatX[23] = 0.0164
TrafficCarQuatY[23] = 0.0015
TrafficCarQuatZ[23] = 0.9267
TrafficCarQuatW[23] = 0.3754
TrafficCarRecording[23] = 24
TrafficCarStartime[23] = 66980.0000
TrafficCarModel[23] = coquette

TrafficCarPos[24] = <<1119.2665, 402.3738, 83.0242>>
TrafficCarQuatX[24] = -0.0020
TrafficCarQuatY[24] = -0.0141
TrafficCarQuatZ[24] = -0.3897
TrafficCarQuatW[24] = 0.9208
TrafficCarRecording[24] = 25
TrafficCarStartime[24] = 67046.0000
TrafficCarModel[24] = cogcabrio

TrafficCarPos[25] = <<1154.8197, 355.7718, 90.8335>>
TrafficCarQuatX[25] = -0.0158
TrafficCarQuatY[25] = 0.0060
TrafficCarQuatZ[25] = 0.9599
TrafficCarQuatW[25] = -0.2799
TrafficCarRecording[25] = 26
TrafficCarStartime[25] = 68564.0000
TrafficCarModel[25] = coquette

TrafficCarPos[26] = <<983.0621, 173.4691, 80.5089>>
TrafficCarQuatX[26] = -0.0049
TrafficCarQuatY[26] = 0.0019
TrafficCarQuatZ[26] = 0.9411
TrafficCarQuatW[26] = 0.3380
TrafficCarRecording[26] = 27
TrafficCarStartime[26] = 77247.0000
TrafficCarModel[26] = feltzer2

ParkedCarPos[0] = <<852.1576, -124.6406, 78.5719>>
ParkedCarQuatX[0] = -0.0278
ParkedCarQuatY[0] = -0.0124
ParkedCarQuatZ[0] = 0.9574
ParkedCarQuatW[0] = 0.2872
ParkedCarModel[0] = cogcabrio

TrafficCarPos[27] = <<812.6069, -185.2938, 72.7078>>
TrafficCarQuatX[27] = -0.0002
TrafficCarQuatY[27] = 0.0004
TrafficCarQuatZ[27] = 0.4820
TrafficCarQuatW[27] = 0.8762
TrafficCarRecording[27] = 28
TrafficCarStartime[27] = 89655.0000
TrafficCarModel[27] = BjXL

TrafficCarPos[28] = <<718.7026, -136.8279, 74.7098>>
TrafficCarQuatX[28] = -0.0057
TrafficCarQuatY[28] = -0.0031
TrafficCarQuatZ[28] = 0.8765
TrafficCarQuatW[28] = -0.4813
TrafficCarRecording[28] = 29
TrafficCarStartime[28] = 93417.0000
TrafficCarModel[28] = BUS

TrafficCarPos[29] = <<708.8309, -123.9532, 74.4544>>
TrafficCarQuatX[29] = -0.0127
TrafficCarQuatY[29] = 0.0230
TrafficCarQuatZ[29] = 0.4820
TrafficCarQuatW[29] = 0.8758
TrafficCarRecording[29] = 30
TrafficCarStartime[29] = 93945.0000
TrafficCarModel[29] = baller2

TrafficCarPos[30] = <<686.3724, -110.2447, 74.0336>>
TrafficCarQuatX[30] = -0.0118
TrafficCarQuatY[30] = 0.0160
TrafficCarQuatZ[30] = 0.4839
TrafficCarQuatW[30] = 0.8749
TrafficCarRecording[30] = 31
TrafficCarStartime[30] = 94869.0000
TrafficCarModel[30] = asterope

TrafficCarPos[31] = <<612.1208, -67.3511, 73.1600>>
TrafficCarQuatX[31] = 0.0707
TrafficCarQuatY[31] = 0.0064
TrafficCarQuatZ[31] = -0.5441
TrafficCarQuatW[31] = 0.8360
TrafficCarRecording[31] = 32
TrafficCarStartime[31] = 97311.0000
TrafficCarModel[31] = DUMMY_MODEL_FOR_SCRIPT //asterope

TrafficCarPos[32] = <<605.6122, -70.8522, 72.6917>>
TrafficCarQuatX[32] = 0.0678
TrafficCarQuatY[32] = 0.0099
TrafficCarQuatZ[32] = -0.5536
TrafficCarQuatW[32] = 0.8300
TrafficCarRecording[32] = 33
TrafficCarStartime[32] = 97443.0000
TrafficCarModel[32] = DUMMY_MODEL_FOR_SCRIPT //baller2

TrafficCarPos[33] = <<523.7166, -108.7238, 63.2222>>
TrafficCarQuatX[33] = 0.0044
TrafficCarQuatY[33] = -0.0835
TrafficCarQuatZ[33] = 0.9375
TrafficCarQuatW[33] = 0.3377
TrafficCarRecording[33] = 34
TrafficCarStartime[33] = 100809.0000
TrafficCarModel[33] = Baller

TrafficCarPos[34] = <<505.7021, -127.9252, 59.7848>>
TrafficCarQuatX[34] = -0.0227
TrafficCarQuatY[34] = -0.0599
TrafficCarQuatZ[34] = 0.9805
TrafficCarQuatW[34] = 0.1858
TrafficCarRecording[34] = 35
TrafficCarStartime[34] = 100941.0000
TrafficCarModel[34] = BjXL

TrafficCarPos[35] = <<510.6351, -130.4515, 59.3621>>
TrafficCarQuatX[35] = 0.0002
TrafficCarQuatY[35] = -0.0620
TrafficCarQuatZ[35] = 0.9802
TrafficCarQuatW[35] = 0.1878
TrafficCarRecording[35] = 36
TrafficCarStartime[35] = 101139.0000
TrafficCarModel[35] = banshee

TrafficCarPos[36] = <<513.6044, -124.3921, 60.1873>>
TrafficCarQuatX[36] = 0.0026
TrafficCarQuatY[36] = -0.0670
TrafficCarQuatZ[36] = 0.9725
TrafficCarQuatW[36] = 0.2230
TrafficCarRecording[36] = 37
TrafficCarStartime[36] = 101337.0000
TrafficCarModel[36] = asterope

TrafficCarPos[37] = <<484.3174, -155.8481, 56.6016>>
TrafficCarQuatX[37] = 0.0008
TrafficCarQuatY[37] = -0.1155
TrafficCarQuatZ[37] = 0.7595
TrafficCarQuatW[37] = -0.6402
TrafficCarRecording[37] = 38
TrafficCarStartime[37] = 101865.0000
TrafficCarModel[37] = Baller

TrafficCarPos[38] = <<515.3390, -244.6399, 48.6464>>
TrafficCarQuatX[38] = -0.0232
TrafficCarQuatY[38] = -0.0304
TrafficCarQuatZ[38] = 0.9764
TrafficCarQuatW[38] = 0.2127
TrafficCarRecording[38] = 39
TrafficCarStartime[38] = 103845.0000
TrafficCarModel[38] = Baller

TrafficCarPos[39] = <<508.6511, -280.4524, 46.7813>>
TrafficCarQuatX[39] = 0.0219
TrafficCarQuatY[39] = 0.0130
TrafficCarQuatZ[39] = -0.2077
TrafficCarQuatW[39] = 0.9779
TrafficCarRecording[39] = 40
TrafficCarStartime[39] = 105165.0000
TrafficCarModel[39] = BUS

TrafficCarPos[40] = <<496.9088, -318.2034, 44.9216>>
TrafficCarQuatX[40] = 0.0249
TrafficCarQuatY[40] = 0.0114
TrafficCarQuatZ[40] = 0.5613
TrafficCarQuatW[40] = 0.8271
TrafficCarRecording[40] = 41
TrafficCarStartime[40] = 105957.0000
TrafficCarModel[40] = asterope

TrafficCarPos[41] = <<482.1582, -326.7783, 45.4484>>
TrafficCarQuatX[41] = 0.0144
TrafficCarQuatY[41] = -0.0290
TrafficCarQuatZ[41] = 0.8114
TrafficCarQuatW[41] = -0.5836
TrafficCarRecording[41] = 42
TrafficCarStartime[41] = 106287.0000
TrafficCarModel[41] = asterope

TrafficCarPos[42] = <<464.0194, -319.4027, 47.2238>>
TrafficCarQuatX[42] = 0.0314
TrafficCarQuatY[42] = -0.0059
TrafficCarQuatZ[42] = 0.9494
TrafficCarQuatW[42] = 0.3125
TrafficCarRecording[42] = 43
TrafficCarStartime[42] = 106353.0000
TrafficCarModel[42] = Baller

TrafficCarPos[43] = <<459.4142, -337.1878, 47.1687>>
TrafficCarQuatX[43] = 0.0061
TrafficCarQuatY[43] = 0.0760
TrafficCarQuatZ[43] = -0.2966
TrafficCarQuatW[43] = 0.9520
TrafficCarRecording[43] = 44
TrafficCarStartime[43] = 106815.0000
TrafficCarModel[43] = baller2

TrafficCarPos[44] = <<386.0724, -405.9861, 46.3795>>
TrafficCarQuatX[44] = 0.0131
TrafficCarQuatY[44] = -0.0083
TrafficCarQuatZ[44] = -0.5920
TrafficCarQuatW[44] = 0.8058
TrafficCarRecording[44] = 45
TrafficCarStartime[44] = 109389.0000
TrafficCarModel[44] = Baller

TrafficCarPos[45] = <<344.1942, -423.8685, 44.2940>>
TrafficCarQuatX[45] = 0.0281
TrafficCarQuatY[45] = -0.0016
TrafficCarQuatZ[45] = -0.3989
TrafficCarQuatW[45] = 0.9166
TrafficCarRecording[45] = 46
TrafficCarStartime[45] = 110643.0000
TrafficCarModel[45] = Vader

TrafficCarPos[46] = <<295.9982, -449.7546, 42.9940>>
TrafficCarQuatX[46] = -0.0033
TrafficCarQuatY[46] = -0.0207
TrafficCarQuatZ[46] = 0.9991
TrafficCarQuatW[46] = 0.0380
TrafficCarRecording[46] = 47
TrafficCarStartime[46] = 113019.0000
TrafficCarModel[46] = asterope

SetPieceCarPos[0] = <<636.0844, 221.4270, 98.7137>>
SetPieceCarQuatX[0] = 0.0558
SetPieceCarQuatY[0] = 0.0284
SetPieceCarQuatZ[0] = 0.6062
SetPieceCarQuatW[0] = 0.7928
SetPieceCarRecording[0] = 499
SetPieceCarStartime[0] = 900.0000  //500.0000
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = police3


SetPieceCarPos[1] = <<545.7987, 195.0462, 100.8242>>
SetPieceCarQuatX[1] = 0.0227
SetPieceCarQuatY[1] = -0.0209
SetPieceCarQuatZ[1] = -0.2162
SetPieceCarQuatW[1] = 0.9759
SetPieceCarRecording[1] = 500
SetPieceCarStartime[1] = 40200.0000  //40000.0000
SetPieceCarRecordingSpeed[1] = 1.0000
SetPieceCarModel[1] = police3

SetPieceCarPos[2] = <<187.4046, -359.7074, 43.5742>>
SetPieceCarQuatX[2] = 0.0259
SetPieceCarQuatY[2] = 0.0031
SetPieceCarQuatZ[2] = -0.3841
SetPieceCarQuatW[2] = 0.9229
SetPieceCarRecording[2] = 501
SetPieceCarStartime[2] = 104000.0000
SetPieceCarRecordingSpeed[2] = 1.0000
SetPieceCarModel[2] = police3




SetPieceCarPos[] = <<891.8881, 426.6213, 119.3326>>
SetPieceCarQuatX[] = -0.0052
SetPieceCarQuatY[] = -0.0170
SetPieceCarQuatZ[] = 0.9645
SetPieceCarQuatW[] = 0.2634
SetPieceCarRecording[] = 600
SetPieceCarStartime[] = 37000.0000
SetPieceCarRecordingSpeed[] = 1.0000
SetPieceCarModel[] = Pounder



SetPieceCarPos[] = <<1151.9595, 370.9727, 91.3383>>
SetPieceCarQuatX[] = -0.0041
SetPieceCarQuatY[] = -0.0063
SetPieceCarQuatZ[] = 0.3161
SetPieceCarQuatW[] = 0.9487
SetPieceCarRecording[] = 601
SetPieceCarStartime[] = 57000.0000
SetPieceCarRecordingSpeed[] = 1.0000
SetPieceCarModel[] = scrap





               // paste array of data here
			
	        iPlaybackProgress++		
			bSetPieceCarsDontSwitchToAI = TRUE	
			bTrafficDontSwitchToAI = TRUE	
	   BREAK		
	

          // **** START PLAYBACK OF MAIN CAR ****
	   CASE 1 
 			 INIT_UBER_RECORDING("PAP3A2")            
			  START_PLAYBACK_RECORDED_VEHICLE(mvPoppyCar.mVehicle, 300,"PAP3A2")
			  //START_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle, 500,"PAP3A2")
	       		
		   iPlaybackProgress++		
		   bSetPieceCarsDontSwitchToAI = TRUE	
		   bTrafficDontSwitchToAI = TRUE	
	   BREAK
			

           // **** UPDATE UBER PLAYBACK ****
	    CASE 2
	
			bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE
			//UPDATE_UBER_RECORDING()
			bSetPieceCarsDontSwitchToAI = TRUE	
			bTrafficDontSwitchToAI = TRUE	
			CreatePoliceChopper()
			IF IS_VEHICLE_DRIVEABLE(mvPoppyCar.mVehicle)	
		     IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
		         // work out playback speed (using slowdown speed up etc. based on how far away player is etc)
                        // There is a useful function CALCULATE_PLAYBACK_SPEED in traffic.sch that does this			
                       
						//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle) < GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle)
						//	fPlayBackSpeed = (140.0 - (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle)))
						//ELSE
						//	fPlayBackSpeed = (140.0 - (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle)))
						//ENDIF 
						
						
					
					   // SET_PLAYBACK_SPEED(mvPoliceCars[0].mVehicle, fPlaybackSpeed)
						fPlaybackSpeed = 1
						SET_PLAYBACK_SPEED(mvPoppyCar.mVehicle, fPlaybackSpeed)
						//CREATE_ALL_WAITING_UBER_CARS()
						
               bSetPieceCarsDontSwitchToAI = TRUE	         
				UPDATE_UBER_RECORDING()
			 UPDATE_UBER_PLAYBACK(mvPoppyCar.mVehicle, fPlaybackSpeed)
				
				bSetPieceCarsDontSwitchToAI = TRUE		
		     ELSE
			 // finish	
                       
					 
		   			
			 ENDIF
		ENDIF
	     BREAK

	ENDSWITCH
ENDIF
*/
//WAIT(0)
//ENDWHILE
		
	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()			
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
			ADD_PED_FOR_DIALOGUE(sSpeach, 3, NULL, "BEVERLY")
			
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			
			CLEAR_AREA_OF_VEHICLES(<< 691.2697, 10.5739, 83.1879 >>,100.0,FALSE,TRUE) // CLEAR FIRST JUNCTION OF VEHICLES
			
			IF bFinishedSkipping = TRUE
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_IN()
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500,FALSE)
					ENDIF
				ENDIF
			ENDIF
			//fSlowFact = 250		
			bConversationActive = FALSE
			//fPlaybackAccel = 0.0
			SAFE_REMOVE_BLIP(biMissionBlip)

			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Init phonecall****")
			#ENDIF
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)	
			
			DISABLE_TAXI_HAILING(TRUE)
				
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			
			IF NOT bConversationActive
				IF PLAYER_CALL_CHAR_CELLPHONE(sSpeach, CHAR_BEVERLY, "PAP3AAU", "PAP3A_INTRO", CONV_PRIORITY_VERY_HIGH)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "PAP3A****TRIGGER PHONE****")
					#ENDIF
					REPLAY_RECORD_BACK_FOR_TIME(1.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
					bConversationActive = TRUE
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_CELLPHONE_CALL_FINISHED()
						eSubStage = SS_CLEANUP	
					ENDIF
				ENDIF	
				IF ARE_STRINGS_EQUAL(label,"PAP3A_INTRO_3")				
					eSubStage = SS_CLEANUP	
				ENDIF
				//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<610.134766,159.861359,95.472145>>, <<160.532639,319.985107,208.461792>>, 105.750000)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<186.165924,299.047302,86.186226>>, <<653.020813,137.619247,179.561157>>, 133.500000)	
				AND IS_HEADING_OK(GET_ENTITY_HEADING(PLAYER_PED_ID()),339.0575,90)		
					//IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<<260.3616, 339.5469, 104.5709>>) < GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<<421.7929, 295.6490, 102.0579>>)
					//	bSkipMore = TRUE 
					//ENDIF
					eSubStage = SS_CLEANUP	
				ENDIF
			ENDIF
			
			HANDLE_POLICE_CHASE()
			
			IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
				IF IS_ENTITY_IN_RANGE_ENTITY(mvPoppyCar.mVehicle,PLAYER_PED_ID(),200)
					eSubStage = SS_CLEANUP		
					/*
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "PAP3A****HANG UP PHONE****")
					#ENDIF
					HANG_UP_AND_PUT_AWAY_PHONE()
					*/
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
		
			//CLEAR_PRINTS()
			
			bMainObjectiveDisplayed = FALSE
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Cleanup phonecall****")
			#ENDIF
			
			SetStage(MS_GO_TO_LOCATION)
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player going to the inital area location
PROC GO_TO_LOCATION() 
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			//SAFE_REMOVE_BLIP(biMissionBlip)

			//CLEAR_AREA_OF_VEHICLES(<< 691.2697, 10.5739, 83.1879 >>,100.0,FALSE,TRUE) // CLEAR FIRST JUNCTION OF VEHICLES
	
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			
			HANDLE_POLICE_CHASE()
			
			IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)	
				IF NOT DOES_BLIP_EXIST(blipPoppyCar)
					//blipPoppyCar = CREATE_VEHICLE_BLIP(mvPoppyCar.mVehicle,TRUE)
					IF bMainObjectiveDisplayed = FALSE
						bMainObjectiveDisplayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bPoliceChaseCreated = TRUE
				IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)
					/*
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) <= 30.0 //IF PLAYER IS MOVING SLOW TO LOCATION, USE SMALL DISTANCE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPoppyInitialLocation,<<FOUND_DIS,FOUND_DIS,FOUND_DIS>>)
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 785.3011, 158.9101, 80.1365 >>,<<FOUND_DIS,FOUND_DIS,FOUND_DIS>>)
								eSubStage = SS_CLEANUP
							ENDIF
						ELSE // IF PLAYER IS TRAVELLING FAST TO LOCATION USE LARGER DETECTIION DISTANCE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPoppyInitialLocation,<<FOUND_DIS2,FOUND_DIS2,FOUND_DIS2>>)
							OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 785.3011, 158.9101, 80.1365 >>,<<FOUND_DIS2,FOUND_DIS2,FOUND_DIS2>>)
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vPoppyInitialLocation,<<FOUND_DIS,FOUND_DIS,FOUND_DIS>>)
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 785.3011, 158.9101, 80.1365 >>,<<FOUND_DIS,FOUND_DIS,FOUND_DIS>>)
							eSubStage = SS_CLEANUP
						ENDIF
					ENDIF
					*/
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
	
		BREAK
		
		CASE SS_CLEANUP	
			
			//CLEAR_PRINTS()
			
			bMainObjectiveDisplayed = FALSE
			
			SetStage(MS_CHASE_POPPY)
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player chasing POPPY's car
PROC CHASE_POPPY() 
	
	//PRINTFLOAT(GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mvPoppyCar.mVehicle))
	//PRINTNL()
	//FLOAT fRange
	
	IF bPoliceAttacking	
		++iWanted
		IF IS_ENTITY_DEAD(mpPoppyPed.mPed)	
		OR IS_PED_INJURED(mpPoppyPed.mPed)	
			SET_FAIL_REASON(FAIL_POPPY_KILLED)
			EXIT
		ELSE
			IF iWanted > 30
				SET_FAIL_REASON(FAIL_POLICE_INTER)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eSubStage
		CASE SS_SETUP
			
			bDelayMusic = FALSE
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				TRIGGER_MUSIC_EVENT("PAP3_START")
			ELSE
				TRIGGER_MUSIC_EVENT("PAP3_START_FORA")
				bDelayMusic = TRUE
				iTimerDelayMusic = GET_GAME_TIMER()
			ENDIF
			
			IF bMainObjectiveDisplayed = FALSE	
				IF NOT IS_REPLAY_IN_PROGRESS()	
					IF HAS_CELLPHONE_CALL_FINISHED()	
						PRINT_NOW("PAP3_03", DEFAULT_GOD_TEXT_TIME, 1) //"Persue poppy"
						bMainObjectiveDisplayed = TRUE
					ENDIF
				ELSE	
					IF GET_REPLAY_MID_MISSION_STAGE() < 1
						IF HAS_CELLPHONE_CALL_FINISHED()	
							PRINT_NOW("PAP3_03", DEFAULT_GOD_TEXT_TIME, 1) //"Persue poppy"
							bMainObjectiveDisplayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			bStatsFarFromPoppy = FALSE
			
			bPrintWantedHelp = FALSE
			
			PLAYBACK_UBER_RECORDING()
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)	
			
			REQUEST_MODEL(POLMAV)
			
			bDoneDelayedFadeIn = FALSE	
			
			bPrintCatchUp = FALSE
			
			iCopCrashSeq1 = 0
			
			iSeqCasino = 0
			
			fHighPlaybackSpeedClamp = 1.0
			
			iRandomTrafficSeq = 0
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Init chase poppy****")
			#ENDIF
		
			iShockingEvent = iShockingEvent
		
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			
			IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)		
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
					IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_POLICE_CHASE")
						START_AUDIO_SCENE("PAPARAZZO_3A_POLICE_CHASE")
					ENDIF
					fTimePosInRec = GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle)
					//PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle))
					//PRINTNL()
					/*
					48408.339844 //launch   //damage small 668.81, 357.02, 110.90
					
					49675.332031 //roof    //707.97, 340.51, 111.43
					
					51333.238281  //rear end (maybe not needed) //733.85, 326.47, 112.33
					*/
					DO_AMBIENT_CASINO_STUFF()
					IF fTimePosInRec > 15000
						IF NOT bStatsFarFromPoppy 	
							IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoppyCar.mVehicle,100)
								CPRINTLN(DEBUG_MISSION, "STATS **** Thick of it - Failed - ****")
								bStatsFarFromPoppy = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(mvPoliceCars[1].mVehicle)	
						IF iCopCrashSeq1 = 0
							IF fTimePosInRec >= 48408.339844		
								SET_ENTITY_INVINCIBLE(mvPoliceCars[1].mVehicle,FALSE)
								SET_VEHICLE_ENGINE_HEALTH(mvPoliceCars[1].mVehicle,5.0)
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "overheat")),100, 200, FALSE)
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,36),100, 200, FALSE)			
								PLAY_SOUND_FROM_ENTITY(-1,"POLICE_CRASH",mvPoliceCars[1].mVehicle,"PAPARAZZO_03A")
								CPRINTLN(DEBUG_MISSION, "SMASH 0")
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOWEST) //Set piece cop car crash.
								iCopCrashSeq1 = 1
							ENDIF
						ELIF iCopCrashSeq1 = 1
							IF fTimePosInRec >= 49675.332031		
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "misc_a")),100, 600, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,21),100, 600, FALSE)
								/*
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren7")),100, 500, FALSE)	
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren1")),100, 500, FALSE)		
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren2")),100, 500, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren3")),100, 500, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren4")),100, 500, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren5")),100, 500, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren6")),100, 500, FALSE)
								*/
								
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren_glass1")),100, 400, FALSE)
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren_glass2")),100, 400, FALSE)
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "siren_glass3")),100, 400, FALSE)
								
								SMASH_VEHICLE_WINDOW(mvPoliceCars[1].mVehicle,SC_WINDOW_FRONT_LEFT)
								SMASH_VEHICLE_WINDOW(mvPoliceCars[1].mVehicle,SC_WINDOW_FRONT_RIGHT)
								SET_VEHICLE_SIREN(mvPoliceCars[1].mVehicle,FALSE)
								//PLAY_SOUND_FROM_ENTITY(-1,"SIREN_BLIP",mvPoliceCars[1].mVehicle,"PAPARAZZO_03A")
								CPRINTLN(DEBUG_MISSION, "SMASH 1")
								iCopCrashSeq1 = 2
							ENDIF
						ELIF iCopCrashSeq1 = 2
							IF fTimePosInRec >= 51333.238281		
								//SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[1].mVehicle, "reversinglight_l")),100, 200, FALSE)
								SET_VEHICLE_DAMAGE(mvPoliceCars[1].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[1].mVehicle,73),100, 200, FALSE)		
								CPRINTLN(DEBUG_MISSION, "SMASH 2")
								REPLAY_STOP_EVENT()
								iCopCrashSeq1 = 3
							ENDIF			
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bDelayMusic = TRUE
			AND GET_GAME_TIMER() > iTimerDelayMusic + 6000		
				TRIGGER_MUSIC_EVENT("PAP3_START")
				bDelayMusic = FALSE
			ENDIF
			
			IF bMainObjectiveDisplayed = FALSE	
				IF NOT IS_REPLAY_IN_PROGRESS()	
					IF HAS_CELLPHONE_CALL_FINISHED()	
						PRINT_NOW("PAP3_03", DEFAULT_GOD_TEXT_TIME, 1) //"Persue poppy"
						bMainObjectiveDisplayed = TRUE
					ENDIF
				ELSE	
					IF GET_REPLAY_MID_MISSION_STAGE() < 1
						IF HAS_CELLPHONE_CALL_FINISHED()	
							PRINT_NOW("PAP3_03", DEFAULT_GOD_TEXT_TIME, 1) //"Persue poppy"
							bMainObjectiveDisplayed = TRUE
						ENDIF
					ENDIF
				ENDIF
				//bMainObjectiveDisplayed = TRUE
			ENDIF
			
			/*
			//REQUEST_COLLISION_AT_COORD(<<647.9958, -88.8860, 73.6843>>)
			//REQUEST_ADDITIONAL_COLLISION_AT_COORD(<<647.9958, -88.8860, 73.6843>>)
			
			REQUEST_COLLISION_AT_COORD(mpsChaseProps[0].vStartPos)
			REQUEST_ADDITIONAL_COLLISION_AT_COORD(mpsChaseProps[0].vStartPos)
			
			REQUEST_COLLISION_AT_COORD(mpsChaseProps[1].vStartPos)
			REQUEST_ADDITIONAL_COLLISION_AT_COORD(mpsChaseProps[1].vStartPos)
			
			REQUEST_COLLISION_AT_COORD(mpsChaseProps[2].vStartPos)
			REQUEST_ADDITIONAL_COLLISION_AT_COORD(mpsChaseProps[2].vStartPos)
			*/
			CreatePoliceChopper()
			
			IF DOES_BLIP_EXIST(blipPoppyCar)
				UPDATE_CHASE_BLIP(blipPoppyCar,mvPoppyCar.mVehicle,LOSE_DISTANCE,0.95) //150
			ENDIF
			/*
			IF bDoneFakeStart = TRUE
				IF DOES_BLIP_EXIST(blipPoppyCar)
					//UPDATE_CHASE_BLIP(blipPoppyCar,mvPoppyCar.mVehicle,200,0.7) //150
					IF IS_VEHICLE_OK(mvPoppyCar.mVehicle)		
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoppyCar.mVehicle)
							IF GET_TIME_POSITION_IN_RECORDING(mvPoppyCar.mVehicle) > 15000
								//fSlidy = 5
								//UPDATE_CHASE_BLIP(blipPoppyCar,mvPoppyCar.mVehicle,200,0.7) //150
								IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mvPoppyCar.mVehicle,100)
									//#IF IS_DEBUG_BUILD
									//	CPRINTLN(DEBUG_MISSION, "STATS **** Thick of it - Failed - ****")
									//#ENDIF
									bStatsFarFromPoppy = TRUE
									//INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP3A_FAR_FROM_POPPY) 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
			
			IF bDoRollingStart = TRUE
				RCM_MANAGE_ROLLING_START()
			ENDIF
			
			IF IS_REPLAY_IN_PROGRESS()	
				IF GET_REPLAY_MID_MISSION_STAGE() > 0	
					IF bDoneDelayedFadeIn = FALSE	
						IF GET_GAME_TIMER() > iTimerReplayStarted + 2000
							RC_END_Z_SKIP(FALSE)
							bDoneDelayedFadeIn = TRUE	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bPrintCatchUp = FALSE
				IF IS_PED_UNINJURED(mpPoppyPed.mPed)
					IF NOT IS_ENTITY_AT_COORD(mpPoppyPed.mPed,vPoppyInitialLocation,<<LOSE_DISTANCE,LOSE_DISTANCE,LOSE_DISTANCE>>)
						IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(),mpPoppyPed.mPed,100)
							//PRINT_NOW("PAP3_CAT", DEFAULT_GOD_TEXT_TIME, 1) //Catch up with ~b~Poppy.
							bPrintCatchUp = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			/*
			IF bCopsAreLeaving = FALSE
				IF IS_PLAYER_INSIDE_ROAD_BLOCK() //IF PLAYER TRIES TO GO THROUGH ROAD BLOCK, THEN FAIL
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(),2)
						SET_FAIL_REASON(FAIL_POLICE_INTER)
					ENDIF
				ENDIF
			ENDIF
			*/
			CHASE_DIALOGUE()
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				HANDLE_STOPPING_BEFORE_CRASH()
			ENDIF
			
			PLAYBACK_UBER_RECORDING()
			HANDLE_POLICE_CHASE()
			
			IF IS_PED_UNINJURED(mpPolicePed[0].mPed)	
				SET_ENTITY_LOAD_COLLISION_FLAG(mpPolicePed[0].mPed,TRUE)
			ENDIF
			/*
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
				//CreateScriptedTraffic()
			ENDIF
			*/
			IF NOT bPrintWantedHelp	
				//IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP3_HELPPO")
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						//PRINT_HELP("PAP3_HELPPO") // You will fail if you get a wanted rating
						bPrintWantedHelp = TRUE
					ENDIF
				//ENDIF
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
			
			//Reset any speed modifier
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				MODIFY_VEHICLE_TOP_SPEED(vehPlayer,0)
			ENDIF
			
			SAFE_RELEASE_VEHICLE(mvPoliceCars[1].mVehicle)
			SAFE_RELEASE_PED(mpPolicePed[2].mPed)
			/*
			IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)	
				FREEZE_ENTITY_POSITION(mvPoliceCars[0].mVehicle,TRUE)
			ENDIF
			IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)	
				FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,TRUE)
			ENDIF	
			*/
			CLEAR_PRINTS()
			
			REQUEST_ANIM_DICT("rcmpaparazzo_3")
			
			bMainObjectiveDisplayed = FALSE
			
			CLEANUP_UBER_PLAYBACK()
			
			//REQUEST_MODEL(S_M_Y_Cop_01)
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Cleanup chase poppy****")
			#ENDIF

			//stream_volume = STREAMVOL_CREATE_SPHERE(<< 345.9861, -409.6823, 44.2520 >>, 12.0, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER)	
			
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vCrashLocation,<<47.0,47.0,47.0>>)
				SetStage(MS_GO_TO_CRASH)
			ELSE
				SetStage(MS_TAKE_PHOTO)
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player going to crash site if he is not there
PROC GO_TO_CRASH() 
	
	IF bPoliceAttacking	
		++iWanted
		IF IS_ENTITY_DEAD(mpPoppyPed.mPed)	
		OR IS_PED_INJURED(mpPoppyPed.mPed)	
			SET_FAIL_REASON(FAIL_POPPY_KILLED)
			EXIT
		ELSE
			IF iWanted > 30
				SET_FAIL_REASON(FAIL_POLICE_INTER)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eSubStage
		CASE SS_SETUP
				
			//BLOCK OFF ROAD AT CRASH SITE
			SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, FALSE)
			
			IF bMainObjectiveDisplayed = FALSE
				IF NOT IS_REPLAY_IN_PROGRESS()	
					//PRINT_NOW("PAP3_GOPOP", DEFAULT_GOD_TEXT_TIME, 1) //"Go to crash site"
				ELSE
					IF GET_REPLAY_MID_MISSION_STAGE() < 2
						//PRINT_NOW("PAP3_GOPOP", DEFAULT_GOD_TEXT_TIME, 1) //"Go to crash site"
					ENDIF
				ENDIF
				bMainObjectiveDisplayed = TRUE
			ENDIF
		
			TurnOffRoads()
		
			//CLEANUP_UBER_PLAYBACK()
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Init go to crash****")
			#ENDIF
			
			eSubStage = SS_UPDATE
					
		BREAK
		
		CASE SS_UPDATE
			/*
			IF NOT IS_REPLAY_IN_PROGRESS()	
				IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)	
					IF GET_ENTITY_SPEED(mvPoppyCar.mVehicle) < 0.2	
						IF bCrashSoundPlayed = FALSE
							PLAY_SOUND_FROM_ENTITY(-1,"CRASH",mvPoppyCar.mVehicle,"PAPARAZZO_03A")
							bCrashSoundPlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			*/
			HANDLE_POLICE_CHASE()
					
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				HANDLE_STOPPING_BEFORE_CRASH()
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vCrashLocation,<<47.0,47.0,47.0>>)
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0,REPLAY_IMPORTANCE_LOWEST)
				eSubStage = SS_CLEANUP
			ENDIF
			
		BREAK
		
		CASE SS_CLEANUP	
		
			CLEAR_PRINTS()
			
			REQUEST_ANIM_DICT("rcmpaparazzo_3")
			
			SAFE_REMOVE_BLIP(blipPoppyCar)
			
			bMainObjectiveDisplayed = FALSE
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Cleanup go to crash****")
			#ENDIF
			
			SetStage(MS_TAKE_PHOTO)
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player taking photo of poppy during arrest scene
PROC TAKE_PHOTO() 
	/*
	IF IS_PED_UNINJURED(mpPolicePed[1].mPed)
		IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)	
			IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,VS_FRONT_RIGHT)
				SET_PED_INTO_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,VS_FRONT_RIGHT)
			ENDIF
		ENDIF
	ENDIF	
	*/
	
	IF bPoliceAttacking		
		IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
		AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle)	
			IF iWanted < 30
				iWanted = 30
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.92		
				IF IS_PED_UNINJURED(mpPoppyPed.mPed)
				AND NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
					SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
				ENDIF
			ENDIF
		ENDIF
		++iWanted
		IF IS_ENTITY_DEAD(mpPoppyPed.mPed)	
		OR IS_PED_INJURED(mpPoppyPed.mPed)	
			SET_FAIL_REASON(FAIL_POPPY_KILLED)
			EXIT
		ELSE
			IF iWanted > 30
				SET_FAIL_REASON(FAIL_POLICE_INTER)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	SWITCH eSubStage
		CASE SS_SETUP

			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_CRASH_LOCATION,"Arrived at crash scene",TRUE) // Has reaches crash point, set checkpoint
			
			//BLOCK OFF ROAD AT CRASH SITE
			SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, FALSE)
			
			SAFE_REMOVE_BLIP(blipPoppyCar)
			
			REMOVE_PED_FOR_DIALOGUE(sSpeach,1)
			
			REQUEST_ANIM_DICT("rcmpaparazzo_3")
			/*
			INT i
		
			FOR i = 0 TO NUM_OF_PARKED_CARS - 1
				SAFE_RELEASE_VEHICLE(mvParkedCar[i].mVehicle)
				SET_MODEL_AS_NO_LONGER_NEEDED(mvParkedCar[i].mModel)
			ENDFOR
			
			FOR i = 0 TO NUM_OF_CHASE_PROPS - 1
				SAFE_RELEASE_OBJECT(mpsChaseProps[i].mObject)
				SET_MODEL_AS_NO_LONGER_NEEDED(mpsChaseProps[i].mModel)
			ENDFOR
			*/
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			TurnOffRoads()
			
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			
			CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)			
			
			bPhotoInCuffs = FALSE
			
			b_Correct_Pic_Taken = FALSE
			
			bWrongContact = FALSE
			
			bPicTaken = FALSE
			
			bPicSent = FALSE
			
			bSweetTxtSent = FALSE
			
			bPhotoPoppyObj = FALSE
			
			iBevTxtsBadPic = 0
			
			iSeqPoppyDialogue = 0
			
			iDoDelayedLastLine = 0
			
			iBevTxtsTooFarAway = 0
			
			bReplayTaskHeli = FALSE
			
			bCopWarning = FALSE
			
			bKickedOffDialogueAgain = FALSE
			
			bDone1stPersonFlash = FALSE
			
			IF NOT bDelayFade
				SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)	
			ENDIF
			
			IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)	
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(mvPoppyCar.mVehicle)
				SET_VEHICLE_DOORS_LOCKED(mvPoppyCar.mVehicle,VEHICLELOCK_UNLOCKED)
			ENDIF
			
			IF IS_ENTITY_ALIVE(vehPlayer)	
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehPlayer)
			ENDIF
			
			IF bCrashCutDone = FALSE	
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					HANDLE_STOPPING_BEFORE_CRASH()
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Init take photo****")
			#ENDIF
			
			eSubStage = SS_UPDATE
			
			//TASK_VEHICLE_AIM_AT_PED(pedChopperPilot,mpPoppyPed.mPed)

			//SET_TIME_SCALE(0.5)

		BREAK
		
		CASE SS_UPDATE
			/*
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
				PRINTNL()
				PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene))
			ENDIF
			*/
			//PRINTVECTOR(GET_ENTITY_COORDS(mpPolicePed[0].mPed))
			//PRINTNL()
			/*
			PRINTFLOAT(GET_ENTITY_HEADING(mpPolicePed[0].mPed))
			PRINTNL()
			PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mvPoliceCars[0].mVehicle,GET_WORLD_POSITION_OF_ENTITY_BONE(mvPoliceCars[0].mVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(mvPoliceCars[0].mVehicle, "seat_dside_f"))))
			PRINTNL()
			*/
			//seat_dside_f
			//CreatePoliceChopper()
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
				IF b_Correct_Pic_Taken = FALSE				
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(poppy_arrest_scene,TRUE)
				ELSE
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(poppy_arrest_scene,FALSE)
				ENDIF
			ENDIF
			
			IF IS_CELLPHONE_CAMERA_IN_USE()
				SET_LOADING_ICON_SUBTITLES_OFFSET_SHIFT_THIS_FRAME()
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.85
				IF IS_PED_UNINJURED(pedChopperPilot)	
				AND IS_ENTITY_ALIVE(vehPoliceChopper)		
					TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,NULL,<<123.3842, -883.9507, 200>>,MISSION_GOTO,200.0,-1,-1,-1,100)
					//TASK_VEHICLE_SHOOT_AT_PED(pedChopperPilot,mpPoppyPed.mPed)
					SET_VEHICLE_SEARCHLIGHT(vehPoliceChopper,FALSE)
					SET_PED_KEEP_TASK(pedChopperPilot,TRUE)
					SAFE_RELEASE_PED(pedChopperPilot)
					SAFE_RELEASE_VEHICLE(vehPoliceChopper)
				ENDIF
			ENDIF
			
			IF bPoppyAndCopDrivingOff = FALSE
				
				IF iSeqCarDoors > 0
				//AND iSeqPoppyDialogue >= 2	
				//AND	b_Correct_Pic_Taken = FALSE
					MANAGE_PHONE_HELP()
				ENDIF
				
				IF bCrashCutDone = FALSE	
					/*
					IF NOT IS_REPLAY_IN_PROGRESS()	
						IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)	
							IF GET_ENTITY_SPEED(mvPoppyCar.mVehicle) < 0.2	
								IF bCrashSoundPlayed = FALSE
									PLAY_SOUND_FROM_ENTITY(-1,"CRASH",mvPoppyCar.mVehicle,"PAPARAZZO_03A")
									bCrashSoundPlayed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					*/
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						HANDLE_STOPPING_BEFORE_CRASH()
					ENDIF
				ENDIF
				
				IF HAS_ANIM_DICT_LOADED("rcmpaparazzo_3")	
					IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)		
						IF IS_VEHICLE_STOPPED(mvPoppyCar.mVehicle)		
							MONITOR_POLICE_PEDS(0)
							HANDLE_CRASH_SEQUENCE()
						ENDIF
					ENDIF
				ENDIF
				
				IF bCrashCutDone = TRUE 
					IF IS_REPLAY_IN_PROGRESS()
						IF IS_PED_UNINJURED(mpPoppyPed.mPed)
						AND IS_PED_UNINJURED(pedChopperPilot)		
						AND IS_ENTITY_ALIVE(vehPoliceChopper)		
							IF NOT bReplayTaskHeli
								//TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,<<0,0,0>>,MISSION_POLICE_BEHAVIOUR,0.01,1.0,-1,-1,-1)
								TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,<<0,50,60>>,MISSION_POLICE_BEHAVIOUR,0.1,60.0,-1,60,55)
								bReplayTaskHeli = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF iSeqPoppyDialogue = 0	
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
							IF ARE_STRINGS_EQUAL(label,"PAP3A_ARREST_8")		
								KILL_FACE_TO_FACE_CONVERSATION()
								iSeqPoppyDialogue = 1
							ENDIF
						ENDIF
					ELIF iSeqPoppyDialogue = 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF NOT b_Correct_Pic_Taken	
								IF bPhotoPoppyObj = FALSE	
									PRINT_NOW("PAP3_POP", DEFAULT_GOD_TEXT_TIME, 1) //"Take a photo of poppy"
									bPhotoPoppyObj = TRUE
								ENDIF
							ENDIF
							iSeqPoppyDialogue = 2
						ENDIF
					ELIF iSeqPoppyDialogue = 2
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP") 
							IF NOT bKickedOffDialogueAgain
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPoppyPed.mPed) <= 50	
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "PAP3AAU", "PAP3A_ARREST","PAP3A_ARREST_10", CONV_PRIORITY_HIGH)
										bKickedOffDialogueAgain	= TRUE
										iSeqPoppyDialogue = 3
										iSubSwitch = 0
										iRetrigger = 1
									ENDIF
								ELSE
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "PAP3AAU", "PAP3A_ARREST","PAP3A_ARREST_10", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
										bKickedOffDialogueAgain	= TRUE
										iSeqPoppyDialogue = 3
										iSubSwitch = 1
										iRetrigger = 1
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP")	
					IF iSeqPoppyDialogue = 0
					OR iSeqPoppyDialogue = 3
						DO_ANNOYING_SUBTITLE_SWITCH()
					ENDIF
				ENDIF
				
				IF iSeqCarDoors > 0
				AND bCrashCutDone = TRUE
				AND bKickedOffDialogueAgain	= TRUE	
					HANDLE_POLICE_CHASE()
					//CLEANUP_UBER_PLAYBACK()
					IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_PHOTO_SCENE")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()				
							STOP_AUDIO_SCENE("PAPARAZZO_3A_PHOTO_SCENE")
						ENDIF
					ENDIF
				ENDIF
				
				ARREST_SCENE_AREA_CHECK()
				
				IF b_Correct_Pic_Taken = TRUE
					IF bPhotoSent = FALSE
						IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_BEVERLY)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP") 	
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PLAYER_PED_ID(),"PAP3A_AOAF","FRANKLIN","SPEECH_PARAMS_STANDARD")
							ELSE
								CPRINTLN(DEBUG_MISSION, "PAP3A****PLAY SINGLE LINE WITH SUBS****")
								iDoDelayedLastLine = 1
							ENDIF
							HANG_UP_AND_PUT_AWAY_PHONE()
							BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
							ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
							eSubStage = SS_CLEANUP
							bPhotoSent = TRUE
						ELSE
							IF bPicTaken = TRUE		
								IF bWrongContact = FALSE
									IF HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT()	
										PRINT_HELP("PAP3_HELP7") // The picture was sent to the wrong contact.
										bWrongContact = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bPicTaken = TRUE
						IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_BEVERLY)
							IF bPicSent = FALSE
								iTimerPicTaken = GET_GAME_TIMER()
								bPicSent = TRUE
							ELSE
								IF GET_GAME_TIMER() > iTimerPicTaken + 4000	
									IF bTooFarAway = FALSE	
										IF iBevTxtsBadPic = 0
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXTB1",TXTMSG_UNLOCKED)	//Can you get a better one?
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsBadPic = 1
											ENDIF
										ELIF iBevTxtsBadPic = 1
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXTB3",TXTMSG_UNLOCKED)	//Come on man. Send a decent one.
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsBadPic = 2
											ENDIF
										ELIF iBevTxtsBadPic = 2
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXTB2",TXTMSG_UNLOCKED)	//:/
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsBadPic = 3
											ENDIF
										ELIF iBevTxtsBadPic = 3
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXTB4",TXTMSG_UNLOCKED)	 //Are you blind?
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsBadPic = 4
											ENDIF
										ENDIF	
									ELSE
										IF iBevTxtsTooFarAway = 0
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT8",TXTMSG_UNLOCKED)	//Can you get any closer man?
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsTooFarAway = 1
											ENDIF
										ELIF iBevTxtsTooFarAway = 1
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT7",TXTMSG_UNLOCKED)	//I can hardly see her! Get closer
												CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
												bPicTaken = FALSE
												iBevTxtsTooFarAway = 2
											ENDIF
										ENDIF
										bTooFarAway = FALSE	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
						IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()	
							CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_BEVERLY)
							bPicTaken = TRUE
							bPicSent = FALSE
							IF CHECK_PHOTO_OK() = FALSE	
								IF iFramesCheckingPhotoOk < 20
									BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
									ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
									iTimerPicTaken = GET_GAME_TIMER()
								ENDIF
							ELSE
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PLAYER_PED_ID(),"PAP3A_AOAE","FRANKLIN","SPEECH_PARAMS_FORCE_FRONTEND")
								BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE) 
								ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
								SAFE_REMOVE_BLIP(blipPoppy)
								TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
								iTimerPicTaken = GET_GAME_TIMER()
								b_Correct_Pic_Taken = TRUE
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_MISSION, "**** Decent pic of Poppy taken ****")
								#ENDIF
								IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
									fPhase = GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene)	
									IF fPhase >= 0.638
										#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_MISSION, "STATS **** DUI Diva ****")
										#ENDIF
										bPhotoInCuffs = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							iPhotoScore = 0
							iFramesCheckingPhotoOk = 0
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF b_Correct_Pic_Taken = FALSE
					eSubStage = SS_CLEANUP
				ELSE
					IF bPhotoSent = FALSE
						IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_BEVERLY)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP") 	
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PLAYER_PED_ID(),"PAP3A_AOAF","FRANKLIN","SPEECH_PARAMS_STANDARD")
							ELSE
								CPRINTLN(DEBUG_MISSION, "PAP3A****PLAY SINGLE LINE WITH SUBS****")
								iDoDelayedLastLine = 1
							ENDIF
							//IF IS_MESSAGE_BEING_DISPLAYED()
							//	CLEAR_PRINTS()
							//ENDIF
							HANG_UP_AND_PUT_AWAY_PHONE()
							BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE) 
							ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
							eSubStage = SS_CLEANUP
							iTimerCorrectPicSent = GET_GAME_TIMER()
							bPhotoSent = TRUE
						ELSE
							IF GET_GAME_TIMER() > iTimerCopsLeavingArea + 10000
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE SS_CLEANUP	
		
			//CLEAR_PRINTS()
			
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 6.0, REPLAY_IMPORTANCE_LOW)
			
			bMainObjectiveDisplayed = FALSE
			
			bConversationActive = FALSE
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Cleanup take photo****")
			#ENDIF
			
			IF bPhotoSent = TRUE
				SetStage(MS_END_PHONECALL)
			ELSE
				SET_FAIL_REASON(FAIL_PHOTO_LOST)
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Handles player getting end phone call from Beverly
PROC END_PHONECALL() 
	
	IF bPoliceAttacking		
		IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),mvPoliceCars[0].mVehicle)	
				IF iWanted < 30
					iWanted = 30
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
				AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.92	
					IF IS_PED_UNINJURED(mpPoppyPed.mPed)
					AND NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
						SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		++iWanted
		IF IS_ENTITY_DEAD(mpPoppyPed.mPed)	
		OR IS_PED_INJURED(mpPoppyPed.mPed)	
			SET_FAIL_REASON(FAIL_POPPY_KILLED)
			EXIT
		ELSE
			IF iWanted > 30
				SET_FAIL_REASON(FAIL_POLICE_INTER)
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	TEXT_LABEL_23 label =  GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()	
		
	IF IS_PED_UNINJURED(mpPolicePed[1].mPed)
		IF IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)	
			IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,VS_FRONT_RIGHT)
				SET_PED_INTO_VEHICLE(mpPolicePed[1].mPed,mvPoliceCars[0].mVehicle,VS_FRONT_RIGHT)
			ENDIF
		ENDIF
	ENDIF	
		
	SWITCH eSubStage
		CASE SS_SETUP
			
			bConversationActive = FALSE

			iTimerCorrectPicSent = GET_GAME_TIMER()
			
			bPrintLeaveArea = FALSE
			
			bRegisterBevCall = FALSE
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Init mission ending****")
			#ENDIF
				
			SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)	
				
			eSubStage = SS_UPDATE
			
		BREAK
		
		CASE SS_UPDATE
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)
			AND GET_SYNCHRONIZED_SCENE_PHASE(poppy_arrest_scene) > 0.85
				IF IS_PED_UNINJURED(pedChopperPilot)	
				AND IS_ENTITY_ALIVE(vehPoliceChopper)		
					TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,NULL,<<123.3842, -883.9507, 200>>,MISSION_GOTO,200.0,-1,-1,-1,100)
					//TASK_VEHICLE_SHOOT_AT_PED(pedChopperPilot,mpPoppyPed.mPed)
					SET_VEHICLE_SEARCHLIGHT(vehPoliceChopper,FALSE)
					SET_PED_KEEP_TASK(pedChopperPilot,TRUE)
					SAFE_RELEASE_PED(pedChopperPilot)
					SAFE_RELEASE_VEHICLE(vehPoliceChopper)
				ENDIF
			ENDIF
			
			MONITOR_POLICE_PEDS(0)
			
			HANDLE_CRASH_SEQUENCE()
			
			HANDLE_POLICE_CHASE()
			
			ARREST_SCENE_AREA_CHECK()
			
			IF iDoDelayedLastLine = 1
				IF NOT IS_PHONE_ONSCREEN()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK","PAP3A_FRANK_6", CONV_PRIORITY_MEDIUM)
						iDoDelayedLastLine = 2				
						CPRINTLN(DEBUG_MISSION, "PAP3A****PLAY SINGLE LINE WITH SUBS RETURNED TRUE****")
					ENDIF	
				ENDIF
			ENDIF
			
			IF bPoppyAndCopDrivingOff = FALSE	
				IF bSweetTxtSent = FALSE
					IF GET_GAME_TIMER() > iTimerCorrectPicSent + 3000 //4000	
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_BEVERLY,"PAP3A_TXT6",TXTMSG_UNLOCKED)	//Sweeeeet! Get the fuck out of there!
							bSweetTxtSent = TRUE
						ENDIF
					ENDIF
				ELSE
					IF bPrintLeaveArea = FALSE
						IF GET_GAME_TIMER() > iTimerCorrectPicSent + 4000
							//PRINT_NOW("PAP3_END", DEFAULT_GOD_TEXT_TIME, 1) //"Leave the area."
							bPrintLeaveArea = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			/*
			IF bPoppyAndCopDrivingOff = FALSE	
				IF bPrintLeaveArea = FALSE
					IF GET_GAME_TIMER() > iTimerCorrectPicSent + 1500
						PRINT_NOW("PAP3_END",DEFAULT_GOD_TEXT_TIME,5)
						bPrintLeaveArea = TRUE
					ENDIF
				ENDIF
			ENDIF
			*/
			IF iSeqCarDoors > 0
			AND bCrashCutDone = TRUE
				HANDLE_POLICE_CHASE()
				IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_3A_PHOTO_SCENE")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()				
						STOP_AUDIO_SCENE("PAPARAZZO_3A_PHOTO_SCENE")
					ENDIF
				ENDIF
			ENDIF

			IF bPlayerLeftArea = FALSE
				IF IS_PED_UNINJURED(mpPolicePed[0].mPed)	
				AND IS_PED_UNINJURED(mpPoppyPed.mPed)	
				AND IS_VEHICLE_OK(mvPoliceCars[0].mVehicle)		
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(mpPoppyPed.mPed,PLAYER_PED_ID(),100.0)	 
						IF NOT IS_ENTITY_ON_SCREEN(mpPoppyPed.mPed)
						AND NOT IS_ENTITY_ON_SCREEN(mpPolicePed[0].mPed)	
							
							// For B*2124928 release chopper if player leaves the area so it doesn't try to follow poppy after she gets cleaned up
							SAFE_RELEASE_PED(pedChopperPilot)
							SAFE_RELEASE_VEHICLE(vehPoliceChopper)
							
							IF IS_SYNCHRONIZED_SCENE_RUNNING(poppy_arrest_scene)	
								STOP_SYNCHRONIZED_ENTITY_ANIM(mvPoliceCars[0].mVehicle,-1,FALSE)
							ENDIF
							
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mvPoliceCars[0].mVehicle)
							
							CLEAR_PED_TASKS_IMMEDIATELY(mpPoppyPed.mPed)
							CLEAR_PED_TASKS_IMMEDIATELY(mpPolicePed[0].mPed)
							
							IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
								SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
								SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
							ENDIF
							
							SET_DISABLE_PRETEND_OCCUPANTS(mvPoliceCars[0].mVehicle,TRUE)
	
							//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[0].mVehicle,VEHICLELOCK_LOCKED)

							SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,FALSE)
			
							SET_PED_KEEP_TASK(mpPolicePed[0].mPed,TRUE)
							
							iTimerCopsLeavingArea = GET_GAME_TIMER()
							
							//RE-ACTIVATE ROADS - DO THIS HERE SO POLIE CAN LEAVE AREA CORRECTLY IN VEHICLE
							SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, TRUE)
							SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, TRUE)
							SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, TRUE)
							SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, TRUE)
						
							TASK_VEHICLE_DRIVE_TO_COORD(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,<< 231.6400, -347.1846, 43.2866 >>,30,DRIVINGSTYLE_STRAIGHTLINE,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_AVOIDCARS,20.0,150.0)		
							//TASK_VEHICLE_DRIVE_WANDER(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,50,DRIVINGMODE_AVOIDCARS_RECKLESS)
							
							bPoppyAndCopDrivingOff = TRUE
							
							//IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
							//	FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
							//ENDIF
							
							SET_VEHICLE_DOORS_SHUT(mvPoliceCars[0].mVehicle)
							
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
							bPlayerLeftArea = TRUE
							iSeqCarDoors = 5
							eCopState[0] = CS_LEAVE_AREA
							ePoppyState = PS_POPPY_IN_COP_CAR
						ENDIF
					ENDIF
				ENDIF	
			ENDIF

			IF eCopState[0] = CS_LEAVE_AREA
				IF GET_GAME_TIMER() >= iTimerCopsLeavingArea + 4000
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
						IF NOT bConversationActive
							bRegisterBevCall = TRUE
							bConversationActive = TRUE
						ELSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF HAS_CELLPHONE_CALL_FINISHED()
									IF iWanted = 0
										eSubStage = SS_CLEANUP
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				IF bPoliceAttacking = TRUE // IF POLICE CANNOT LEAVE THE AREA BECAUSE THEY ARE ATTACKING PLAYER THEN ACTIVATE PHONE CALL.
					IF GET_GAME_TIMER() >= iTimerCopsLeavingArea + 10000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
							IF NOT bConversationActive
								bRegisterBevCall = TRUE
								bConversationActive = TRUE
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF HAS_CELLPHONE_CALL_FINISHED()
										IF iWanted = 0
											eSubStage = SS_CLEANUP
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bCrashCutDone = TRUE 
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP")	
				IF iSeqPoppyDialogue = 0	
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
						IF ARE_STRINGS_EQUAL(label,"PAP3A_ARREST_8")		
							KILL_FACE_TO_FACE_CONVERSATION()
							iSeqPoppyDialogue = 1
						ENDIF
					ENDIF
				ELIF iSeqPoppyDialogue = 1
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						IF NOT b_Correct_Pic_Taken	
							IF bPhotoPoppyObj = FALSE	
								PRINT_NOW("PAP3_POP", DEFAULT_GOD_TEXT_TIME, 1) //"Take a photo of poppy"
								bPhotoPoppyObj = TRUE
							ENDIF
						ENDIF
						iSeqPoppyDialogue = 2
					ENDIF
				ELIF iSeqPoppyDialogue = 2
					IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP") 
						IF NOT bKickedOffDialogueAgain
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mpPoppyPed.mPed) <= 50	
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "PAP3AAU", "PAP3A_ARREST","PAP3A_ARREST_10", CONV_PRIORITY_HIGH)
									bKickedOffDialogueAgain	= TRUE
									iSeqPoppyDialogue = 3
									iSubSwitch = 0
									iRetrigger = 1
								ENDIF
							ELSE
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeach, "PAP3AAU", "PAP3A_ARREST","PAP3A_ARREST_10", CONV_PRIORITY_HIGH,DO_NOT_DISPLAY_SUBTITLES)
									bKickedOffDialogueAgain	= TRUE
									iSeqPoppyDialogue = 3
									iSubSwitch = 1
									iRetrigger = 1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP3_POP")	
				IF iSeqPoppyDialogue = 0
				OR iSeqPoppyDialogue = 3
					DO_ANNOYING_SUBTITLE_SWITCH()
				ENDIF
			ENDIF
				
		BREAK
		
		CASE SS_CLEANUP	
		
			IF bRegisterBevCall = TRUE	
				ADD_PED_FOR_DIALOGUE(sSpeach, 3, NULL, "BEVERLY")
				ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
				REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_PAP3A_DONE,CT_END_OF_MISSION,BIT_FRANKLIN,CHAR_BEVERLY,3,CC_END_OF_MISSION_QUEUE_TIME,10000) //Les bug 1533700 //CC_END_OF_MISSION_QUEUE_TIME
			ENDIF
		
			bMainObjectiveDisplayed = FALSE
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "PAP3A****Mission passed****")
			#ENDIF
			
			Script_Passed()
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///  Waits for the screen to fade out, then updates the fail reason for the mission    
PROC FAIL_WAIT_FOR_FADE()
	
	SWITCH eSubStage
	
		CASE SS_SETUP
			
			IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
				//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
				//	STOP_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle)
				//ENDIF
				FREEZE_ENTITY_POSITION(mvPoliceCars[0].mVehicle,FALSE)
			ENDIF
			
			IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
				//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
				//	STOP_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle)
				//ENDIF
				FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
			ENDIF
			
			IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)
				//IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)
				//	STOP_PLAYBACK_RECORDED_VEHICLE(mvPoliceCars[0].mVehicle)
				//ENDIF
				FREEZE_ENTITY_POSITION(mvPoppyCar.mVehicle,FALSE)
			ENDIF
			
			IF eFailReason = FAIL_POLICE_INTER
				IF IS_PED_UNINJURED(mpPolicePed[0].mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[0].mPed,PLAYER_PED_ID(),100)	
						IF NOT IS_PED_IN_COMBAT(mpPolicePed[0].mPed,PLAYER_PED_ID())	
							TASK_COMBAT_PED(mpPolicePed[0].mPed,PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
				
				//IF IS_PED_UNINJURED(mpPolicePed[1].mPed)
				//	TASK_COMBAT_PED(mpPolicePed[1].mPed,PLAYER_PED_ID())
				//ENDIF
				
				IF IS_PED_UNINJURED(mpPolicePed[2].mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[2].mPed,PLAYER_PED_ID(),100)
						IF NOT IS_PED_IN_COMBAT(mpPolicePed[2].mPed,PLAYER_PED_ID())		
							TASK_COMBAT_PED(mpPolicePed[2].mPed,PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_UNINJURED(mpPolicePed[3].mPed)
					IF IS_ENTITY_IN_RANGE_ENTITY(mpPolicePed[3].mPed,PLAYER_PED_ID(),100)
						IF NOT IS_PED_IN_COMBAT(mpPolicePed[3].mPed,PLAYER_PED_ID())	
							TASK_COMBAT_PED(mpPolicePed[3].mPed,PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			CLEAR_PRINTS()
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			
			TRIGGER_MUSIC_EVENT("PAP3_FAIL")	
			
			// remove blips
			SAFE_REMOVE_BLIP(biMissionBlip)

			STRING sFailReason
			
			SWITCH eFailReason // print fail reason
			
				CASE FAIL_DEFAULT // no fail reason to display
				BREAK
				
				CASE FAIL_PHOTO_LOST
					sFailReason = "PAP3_08" //"~r~The photo opportunity was lost."
				BREAK
				
				CASE FAIL_POPPY_LOST
					sFailReason = "PAP3_LOSTP" //"~r~You lost Poppy.."
				BREAK
				
				CASE FAIL_POLICE_INTER
					sFailReason = "PAP3_POLIC" //"~r~You interfered with the police. "
				BREAK
				
				CASE FAIL_POPPY_INJURED
					sFailReason = "PAP3_INJUR" //"~r~Poppy was injured."
				BREAK
				
				CASE FAIL_POPPY_KILLED
					sFailReason = "PAP3_KILL2" //"~r~Poppy was killed."
				BREAK
				
			ENDSWITCH
			
			IF eFailReason = FAIL_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			
			eSubStage = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				// MISSION_FLOW_SET_FAIL_WARP_LOCATION(<< 421.4590, 132.4802, 100.0324 >>, 68.6857)
 				// SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<< 419.2751, 130.6916, 99.9666 >>, 71.4842)

				DELETE_EVERYTHING()
			
				MissionCleanup()
			
				IF IS_SCRIPTED_CONVERSATION_ONGOING()
					KILL_ANY_CONVERSATION()
				ENDIF

				Script_Cleanup(FALSE)
				
			ELSE
				IF NOT bFailDialogue
					IF bPoppyHasCrashed = TRUE
						IF eFailReason = FAIL_POLICE_INTER
							ADD_PED_FOR_DIALOGUE(sSpeach, 1, PLAYER_PED_ID(), "FRANKLIN")
							bFailDialogue = PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeach, "PAP3AAU", "PAP3A_FRANK", "PAP3A_FRANK_7", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						ENDIF
					ENDIF
				ENDIF
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SkipCS()
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

	//SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<< 305.3899, 139.1082, 102.7873 >>, 335.5552)
	
	CLEAR_AREA(<<336.4843, 132.4985, 102.0139>>,30,TRUE)
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(vehPlayer)
			IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
				bHeliReplay = TRUE
			ELSE
				bHeliReplay = FALSE
			ENDIF
			/*
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
			IF bHeliReplay = FALSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<336.4843, 132.4985, 102.0139>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),249.9272)
			ELSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<348.7824, 138.0614, 140.0>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),247.2125)
				SET_HELI_BLADES_FULL_SPEED(vehPlayer)
				//SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),5)
			ENDIF
			*/
			SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
		ELSE
			CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<336.4843, 132.4985, 102.0139>>, 249.9272,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
			IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
				bHeliReplay = TRUE
			ELSE
				bHeliReplay = FALSE
			ENDIF
			/*
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
			IF bHeliReplay = FALSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<336.4843, 132.4985, 102.0139>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),249.9272)
			ELSE
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<348.7824, 138.0614, 140.0>>)
				SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),247.2125)
				SET_HELI_BLADES_FULL_SPEED(vehPlayer)
				//SET_VEHICLE_FORWARD_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),5)
			ENDIF
			*/
			SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
		ENDIF
	ELSE
		
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
			bHeliReplay = TRUE
		ELSE
			bHeliReplay = FALSE
		ENDIF
		
		/*
		IF bHeliReplay = FALSE
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<336.4843, 132.4985, 102.0139>>)
			SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),249.9272)
		ELSE
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<348.7824, 138.0614, 140.0>>)
			SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),247.2125)
		ENDIF
		*/
	ENDIF
	
	IF IS_ENTITY_ALIVE(vehPlayer)
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehPlayer))
			GIVE_PED_HELMET(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF
	
	END_REPLAY_SETUP(vehPlayer)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//LOAD_SCENE(<<336.4843, 132.4985, 102.0139>>)
	
	SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	missionStage =  MS_INITIAL_PHONE

	eSubStage = SS_SETUP

ENDPROC

PROC ReplaySkipToChase()

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "PAP3A**** REPLAY SKIP TO CHASE ****")
	#ENDIF
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)
	
	FLUSH_TEXT_MESSAGE_FEED_ENTRIES()
	
	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)		
			
	//REQUEST_VEHICLE_RECORDING(201, "PAP3ARS")	
	REQUEST_WAYPOINT_RECORDING("Pap3aRoll")
			
	//REQUEST_MODEL(POLMAV)		
			
	IF HAS_POLICE_SCENE_LOADED(TRUE)
		HANDLE_POLICE_CHASE()
		IF bPoliceChaseCreated = TRUE
			//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(vehPlayer)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					/*
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<375.0581, 162.2386, 102.0736>>)
					SET_ENTITY_HEADING(vehPlayer,342.4705)
					*/
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					/*
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<375.0581, 162.2386, 102.0736>>)
					SET_ENTITY_HEADING(vehPlayer,342.4705)
					*/
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ENDIF
			//ELSE
			//	SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 662.8113, 26.9195, 84.1722 >>) //TELEPORT PLAYER TO LOCATION
			//	SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),240.0)
			//ENDIF
		ENDIF				
	ENDIF
	
	bDoingRollingReplay = TRUE	
	bDoRollingStart = TRUE
	bDoneRollingStart = FALSE
	iRollingStartTimer = -1
	
	//WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(201, "PAP3ARS")
	//	WAIT(0)
	//ENDWHILE
	
	WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("Pap3aRoll")
		WAIT(0)
	ENDWHILE
	
	IF IS_ENTITY_ALIVE(vehPlayer)
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehPlayer))
			GIVE_PED_HELMET(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF
	
	END_REPLAY_SETUP(vehPlayer)
	
	RCM_MANAGE_ROLLING_START()
	
	//SET_GAMEPLAY_CAM_RELATIVE_HEADING()  //!!!!!!!!!
	//SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//LOAD_SCENE(<<375.0581, 162.2386, 102.0736>>)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)		
	
	HANDLE_POLICE_CHASE()
	
	WAIT(400)
	
	CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()),200)
	
	//SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)

	iTimerReplayStarted = GET_GAME_TIMER()		

	missionStage = MS_CHASE_POPPY
	
	eSubStage = SS_SETUP
	
ENDPROC

PROC ReplaySkipToPhoto()

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "PAP3A**** REPLAY SKIP TO PHOTO ****")
	#ENDIF
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

	FLUSH_TEXT_MESSAGE_FEED_ENTRIES()

	REQUEST_ANIM_DICT("rcmpaparazzo_3")
	
	REQUEST_MODEL(POLMAV)
	
	WHILE NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_3")
	OR NOT HAS_MODEL_LOADED(POLMAV)
		WAIT(0)
	ENDWHILE

	IF HAS_POLICE_SCENE_LOADED(TRUE)
		HANDLE_POLICE_CHASE()
		IF bPoliceChaseCreated = TRUE
			IF NOT IS_ENTITY_DEAD(mvPoppyCar.mVehicle)	
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(mvPoppyCar.mVehicle)
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(vehPlayer)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
					SET_ENTITY_HEADING(vehPlayer,105.0)
					
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 366.9862, -404.5475, 44.8031 >>)
					//SET_ENTITY_HEADING(vehPlayer,105.0)
					SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
					SET_ENTITY_HEADING(vehPlayer,105.0)
					
					SET_VEHICLE_ENGINE_ON(vehPlayer,TRUE,TRUE)
				ENDIF
			ELSE
				vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
				SET_ENTITY_HEADING(vehPlayer,105.0)
					
				//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 366.9862, -404.5475, 44.8031 >>) //TELEPORT PLAYER TO LOCATION
				//SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),105.0)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(mvPoppyCar.mVehicle)
		SET_ENTITY_COORDS(mvPoppyCar.mVehicle,<< 341.2146, -410.2309, 44.1722 >>)
		//SET_ENTITY_HEADING(mvPoppyCar.mVehicle,111.4817)
		//SET_ENTITY_COORDS(mvPoppyCar.mVehicle,<<341.4217, -410.1503, 44.1738>>)
		SET_ENTITY_HEADING(mvPoppyCar.mVehicle,111.4817)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoppyCar.mVehicle)
		FREEZE_ENTITY_POSITION(mvPoppyCar.mVehicle,TRUE)
		SET_VEHICLE_ENGINE_ON(mvPoppyCar.mVehicle,TRUE,TRUE)
		IF bPoppyCarSmashed = FALSE
			SET_ENTITY_INVINCIBLE(mvPoppyCar.mVehicle,FALSE)   
			SET_VEHICLE_DAMAGE(mvPoppyCar.mVehicle, <<0, 2, 0>>, 400.0, 200.0, TRUE)  //<<0, 2, 0>>, 300.0, 150.0
			bPoppyCarSmashed = TRUE
		ENDIF
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD(mvPoliceCars[0].mVehicle)
		SET_ENTITY_COORDS(mvPoliceCars[0].mVehicle,<< 349.6943, -401.2252, 44.3462 >>)
		SET_ENTITY_HEADING(mvPoliceCars[0].mVehicle,100.5072)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[0].mVehicle)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(mvPoliceCars[2].mVehicle)
		SET_ENTITY_COORDS(mvPoliceCars[2].mVehicle,<<328.869232,-399.050049,44.815384>>)
		SET_ENTITY_HEADING(mvPoliceCars[2].mVehicle,-57.309715)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[2].mVehicle)
	ENDIF
	
	//LOAD_SCENE(<< 366.9862, -404.5475, 44.8031 >>)
	
	IF IS_ENTITY_ALIVE(vehPlayer)
		IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehPlayer))
			GIVE_PED_HELMET(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehPoliceChopper)	
		vehPoliceChopper = CREATE_VEHICLE(POLMAV,<< 379.96, -400.05, 85.23 >>, 108.26)
		pedChopperPilot = CREATE_PED_INSIDE_VEHICLE(vehPoliceChopper,PEDTYPE_COP,S_M_Y_Cop_01)
		SET_VEHICLE_LIVERY(vehPoliceChopper,0)
		SET_HELI_BLADES_FULL_SPEED(vehPoliceChopper)
		SET_VEHICLE_ENGINE_ON(vehPoliceChopper,TRUE,TRUE)
		SET_VEHICLE_SEARCHLIGHT(vehPoliceChopper,TRUE,TRUE)
		TASK_HELI_MISSION(pedChopperPilot,vehPoliceChopper,NULL,mpPoppyPed.mPed,<<0,50,60>>,MISSION_POLICE_BEHAVIOUR,0.1,60.0,-1,60,55)
		FREEZE_ENTITY_POSITION(vehPoliceChopper,TRUE)
	ENDIF
	
	END_REPLAY_SETUP(vehPlayer)
	
	IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
		TRIGGER_MUSIC_EVENT("PAP3_STOP")
	ENDIF
	
	bDelayFade = TRUE
	
	//SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	iTimerReplayStarted = GET_GAME_TIMER()

	missionStage = MS_TAKE_PHOTO
	
	eSubStage = SS_SETUP

ENDPROC

PROC ReplaySkipToEnd()

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MISSION, "PAP3A**** REPLAY SKIP TO PHOTO ****")
	#ENDIF
	
	SAFE_FADE_SCREEN_OUT_TO_BLACK(0,FALSE)

	FLUSH_TEXT_MESSAGE_FEED_ENTRIES()

	REQUEST_ANIM_DICT("rcmpaparazzo_3")
	
	//REQUEST_MODEL(POLMAV)
	
	WHILE NOT HAS_ANIM_DICT_LOADED("rcmpaparazzo_3")
		WAIT(0)
	ENDWHILE

	IF HAS_POLICE_SCENE_LOADED(TRUE)
		HANDLE_POLICE_CHASE()
		IF bPoliceChaseCreated = TRUE
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(vehPlayer)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
					SET_ENTITY_HEADING(vehPlayer,105.0)
				ELSE
					CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
					IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
						bHeliReplay = TRUE
					ELSE
						bHeliReplay = FALSE
					ENDIF
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer,VS_DRIVER)
					SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
					SET_ENTITY_HEADING(vehPlayer,105.0)
				ENDIF
			ELSE
				vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << 366.9862, -404.5475, 44.8031 >>) //TELEPORT PLAYER TO LOCATION
				//SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),105.0)
				SET_ENTITY_COORDS(vehPlayer, << 366.9862, -404.5475, 44.8031 >>)
				SET_ENTITY_HEADING(vehPlayer,105.0)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(mvPoppyCar.mVehicle)
		SET_ENTITY_COORDS(mvPoppyCar.mVehicle,<< 341.2146, -410.2309, 44.1722 >>)
		//SET_ENTITY_COORDS(mvPoppyCar.mVehicle,<<341.4217, -410.1503, 44.1738>>)
		SET_ENTITY_HEADING(mvPoppyCar.mVehicle,111.4817)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoppyCar.mVehicle)
		IF bPoppyCarSmashed = FALSE
			SET_ENTITY_INVINCIBLE(mvPoppyCar.mVehicle,FALSE)   
			SET_VEHICLE_DAMAGE(mvPoppyCar.mVehicle, <<0, 2, 0>>, 400.0, 200.0, TRUE)
			bPoppyCarSmashed = TRUE
		ENDIF
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD(mvPoliceCars[0].mVehicle)
		SET_ENTITY_COORDS(mvPoliceCars[0].mVehicle,<< 349.6943, -401.2252, 44.3462 >>)
		SET_ENTITY_HEADING(mvPoliceCars[0].mVehicle,100.5072)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[0].mVehicle)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(mvPoliceCars[2].mVehicle)
		SET_ENTITY_COORDS(mvPoliceCars[2].mVehicle,<<328.869232,-399.050049,44.815384>>)
		SET_ENTITY_HEADING(mvPoliceCars[2].mVehicle,-57.309715)
		SET_VEHICLE_ON_GROUND_PROPERLY(mvPoliceCars[2].mVehicle)
	ENDIF
	
	b_Correct_Pic_Taken = TRUE
	bPhotoSent = TRUE
	bCrashCutDone = TRUE
	
	END_REPLAY_SETUP(vehPlayer)
	
	//LOAD_SCENE(<< 366.9862, -404.5475, 44.8031 >>)
	
	CLEAR_PED_TASKS_IMMEDIATELY(mpPoppyPed.mPed)
	CLEAR_PED_TASKS_IMMEDIATELY(mpPolicePed[0].mPed)
	
	IF NOT IS_PED_IN_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle)
		SET_PED_INTO_VEHICLE(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,VS_DRIVER)
	ENDIF
	IF NOT IS_PED_IN_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle)
		SET_PED_INTO_VEHICLE(mpPoppyPed.mPed,mvPoliceCars[0].mVehicle,VS_BACK_LEFT)
	ENDIF
	
	SET_DISABLE_PRETEND_OCCUPANTS(mvPoliceCars[0].mVehicle,TRUE)
	
	//SET_VEHICLE_DOORS_LOCKED(mvPoliceCars[0].mVehicle,VEHICLELOCK_LOCKED)
	
	SET_VEHICLE_SIREN(mvPoliceCars[0].mVehicle,FALSE)

	SET_PED_KEEP_TASK(mpPolicePed[0].mPed,TRUE)
	
	iTimerCopsLeavingArea = GET_GAME_TIMER()
	
	//RE-ACTIVATE ROADS - DO THIS HERE SO POLIE CAN LEAVE AREA CORRECTLY IN VEHICLE
	SET_ROADS_IN_AREA(<< 318.9400, -411.6963, 38.0267 >>,<< 421.5246, -363.0107, 52.0853 >>, TRUE)
	SET_ROADS_IN_AREA(<< 796.4662, -68.4078, 79.5220 >>,<< 974.3044, -152.9081, 72.6015 >>, TRUE)
	SET_ROADS_IN_AREA(<< 683.3696, 38.3284, 83.2770 >>, << 707.9796, -22.9872, 82.6540 >>, TRUE)
	SET_ROADS_IN_AREA(<< 812.2689, -40.7279, 79.4878 >>, << 858.4236, -119.0339, 78.3599 >>, TRUE)

	TASK_VEHICLE_DRIVE_TO_COORD(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,<< 231.6400, -347.1846, 43.2866 >>,30,DRIVINGSTYLE_STRAIGHTLINE,DUMMY_MODEL_FOR_SCRIPT,DRIVINGMODE_AVOIDCARS,20.0,150.0)		
	//TASK_VEHICLE_DRIVE_WANDER(mpPolicePed[0].mPed,mvPoliceCars[0].mVehicle,50,DRIVINGMODE_AVOIDCARS_RECKLESS)
	
	//IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
	//	FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
	//ENDIF
	
	SET_VEHICLE_DOORS_SHUT(mvPoliceCars[0].mVehicle)
	
	bPoppyAndCopDrivingOff = TRUE
	
	TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	bPlayerLeftArea = TRUE
	iSeqCarDoors = 5
	eCopState[0] = CS_LEAVE_AREA
	ePoppyState = PS_POPPY_IN_COP_CAR
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	//WAIT(1000)
	
	//SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME,FALSE)
	
	iTimerReplayStarted = GET_GAME_TIMER()

	missionStage = MS_END_PHONECALL
	
	eSubStage = SS_SETUP

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)	
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	RC_CLEANUP_LAUNCHER()

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_3A(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	InitVariables() // initialise everything
	
	// handle replay checkpoints
	IF Is_Replay_In_Progress()
		
		bReplay = TRUE
		
		CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<< 429.1563, 126.8527, 99.4028 >>, 70.3603,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
		//CREATE_VEHICLE_FOR_REPLAY(vehPlayer,<<375.0581, 162.2386, 102.0736>>, 342.4705,FALSE,FALSE,TRUE,TRUE,TRUE,CARBONIZZARE,1)
		IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer))
			bHeliReplay = TRUE
		ELSE
			bHeliReplay = FALSE
		ENDIF
		
		INT iReplayStage = 	GET_REPLAY_MID_MISSION_STAGE()
			
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF	
			
		SWITCH iReplayStage
			
			CASE 0
				START_REPLAY_SETUP(<<375.0581, 162.2386, 102.0736>>,342.4705)
				SkipCS()
			BREAK
			
			CASE CP_AT_POPPY_LOCATION
				//Do_Z_Skip(Z_SKIP_AT_POPPY_LOCATION)	 // skip to start of chase
				START_REPLAY_SETUP(<<336.4843, 132.4985, 102.0139>>,249.9272)
				ReplaySkipToChase()
			BREAK
			
			CASE CP_AT_CRASH_LOCATION
				//Do_Z_Skip(Z_SKIP_AT_CRASH_LOCATION)	 // skip the crash scene and take photo
				START_REPLAY_SETUP(<< 366.9862, -404.5475, 44.8031 >>,105.0,FALSE)
				ReplaySkipToPhoto()
			BREAK
			
			CASE CP_END
				//Do_Z_Skip(Z_SKIP_AT_CRASH_LOCATION)	 // skip to the end
				START_REPLAY_SETUP(<< 366.9862, -404.5475, 44.8031 >>,105.0)
				ReplaySkipToEnd()
			BREAK
		
		ENDSWITCH
	ENDIF

	REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES(TRUE)

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
	
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_TM")
		
		//SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
		//SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF bCrashCutDone = FALSE	
			IF IS_ENTITY_ALIVE(mvPoliceCars[0].mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[0].mVehicle)	//second guy 
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mvPoliceCars[0].mVehicle,<<0,0,0.12>>)
					FREEZE_ENTITY_POSITION(mvPoliceCars[0].mVehicle,FALSE)
				ELSE	
					FREEZE_ENTITY_POSITION(mvPoliceCars[0].mVehicle,TRUE)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(mvPoliceCars[1].mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[1].mVehicle)	//first guy 
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mvPoliceCars[1].mVehicle,<<0,0,0.12>>)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mvPoliceCars[2].mVehicle)	
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mvPoliceCars[2].mVehicle,<<0,0,0.12>>)
					FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
				ELSE	
					FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,TRUE)
				ENDIF
			ENDIF
		ENDIF
	
		IF bPoppyAndCopDrivingOff
		AND GET_GAME_TIMER() > iTimerCopsLeavingArea + 4000
			IF IS_ENTITY_ALIVE(mvPoliceCars[2].mVehicle)
				FREEZE_ENTITY_POSITION(mvPoliceCars[2].mVehicle,FALSE)
				SET_VEHICLE_SIREN(mvPoliceCars[2].mVehicle,FALSE)
			ENDIF
		ENDIF
		
		IF IS_PED_UNINJURED(pedChopperPilot)
		AND IS_ENTITY_ALIVE(mpPoppyPed.mPed)	
		AND IS_ENTITY_ALIVE(vehPoliceChopper)
		AND IS_PED_IN_VEHICLE(pedChopperPilot,vehPoliceChopper)	
			IF CONTROL_MOUNTED_WEAPON(pedChopperPilot)
				SET_MOUNTED_WEAPON_TARGET(pedChopperPilot,mpPoppyPed.mPed,NULL,<<0,0,0>>) 
			ENDIF
		ENDIF
		
		IF bPoppyCarSmashed
			IF IS_ENTITY_ALIVE(mvPoppyCar.mVehicle)	
				SET_VEHICLE_ENGINE_HEALTH(mvPoppyCar.mVehicle,51.0) //Smoke engine	(better to do it like this than with ptfx because it will continue to smoke after mission passes)
			ENDIF
		ENDIF
			
		IF MissionStage = MS_MISSION_FAILED
			FAIL_WAIT_FOR_FADE()
		ELSE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SWITCH missionStage
					
					CASE MS_INITIAL_PHONE
						INITIAL_PHONE_CALL()
					BREAK
					
					CASE MS_GO_TO_LOCATION
						GO_TO_LOCATION()
					BREAK
						
					CASE MS_CHASE_POPPY
						CHASE_POPPY()
					BREAK
						
					CASE MS_GO_TO_CRASH
						GO_TO_CRASH()
					BREAK
						
					CASE MS_TAKE_PHOTO
						TAKE_PHOTO()
					BREAK
					
					CASE MS_END_PHONECALL
						END_PHONECALL()
					BREAK

				ENDSWITCH
			ELSE
				SET_FAIL_REASON(FAIL_DEFAULT)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD // STAGE SKIPPING
			IF MissionStage <> MS_MISSION_FAILED
				if bFinishedSkipping = TRUE
					DEBUG_Check_Debug_Keys() // not skipping stages, check for debug keys
				ELSE
					JumpToStage(eTargetStage) // still skipping stages
				ENDIF
			ENDIF
		#ENDIF
	
	ENDWHILE
		
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

