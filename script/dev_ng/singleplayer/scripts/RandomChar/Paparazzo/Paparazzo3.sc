
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "CompletionPercentage_public.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "cutscene_public.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "RC_Helper_Functions.sch"
USING "initial_scenes_Paparazzo.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo3.sc
//		AUTHOR			:	Ben.Hinchliffe
//		DESCRIPTION		:	Player meets Beverly in a bin.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//*************************************************************************************************************************************************
//													:ENUMS:
//*************************************************************************************************************************************************
// Mission stages
ENUM eRC_SubState
	SS_SETUP = 0,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

// Mission state
ENUM MISSION_STAGE
	MS_INIT = -2,
	MS_LEAD_IN = -1,
	MS_INTRO   = 0,
	MS_LEADOUT,
	MS_FAIL
ENDENUM

//*************************************************************************************************************************************************
//													:CONSTANTS:
//*************************************************************************************************************************************************
CONST_INT BEV			 0
CONST_INT CP_AFTER_MOCAP 0	// Checkpoint
CONST_INT OBJ_DUMPSTER	 0
CONST_INT OBJ_LIDL		 0	// Changed to be index in this script's object array not launcher's
CONST_INT OBJ_LIDR		 1	// Changed to be index in this script's object array not launcher's
CONST_INT OBJ_CAM 		 3 	// Bev's camera

//*************************************************************************************************************************************************
//													:VARIABLES:
//*************************************************************************************************************************************************
MISSION_STAGE 			  missionStage = MS_INIT
eRC_SubState  			  m_eSubState = SS_SETUP

g_structRCScriptArgs 	  sRCLauncherDataLocal
REL_GROUP_HASH 		      relGroupPlayer

OBJECT_INDEX 	dumpsterBlue
OBJECT_INDEX	dumpsterGreen
OBJECT_INDEX	dumpsterLids[2]
MODEL_NAMES 	modDumpL = PROP_CS_DUMPSTER_LIDL
MODEL_NAMES 	modDumpR = PROP_CS_DUMPSTER_LIDR
MODEL_NAMES 	modDumpGreen = PROP_CS_DUMPSTER_01A
MODEL_NAMES 	modDumpBlue = PROP_DUMPSTER_02A

STRING 		dumpLidLSceneHandle = "Dumpster_Lid_L"
STRING 		dumpLidRSceneHandle = "Dumpster_Lid_R"
STRING		dumpGreenSceneHandle = "PAP_Dumpster"
STRING 		dumpBlueSceneHandle = "PAP_Dumpster_blue"
STRING 		sSceneHandle_Franklin = "Franklin"
STRING 		leadInOutAnimDict = "rcmpaparazzo_3leadinoutpap_3_rcm"
STRING 		sSceneHandleBev = "Beverley"

structPedsForConversation	conversation

BOOL 		bLeadInConv = FALSE
BOOL 		bLeadOutConv = FALSE
INT  		iSyncSceneLeadIn
INT  		iSyncSceneLeadInIdle
INT  		iSyncSceneLeadOut
// Use fixed coordinates for the sync scene
VECTOR		vecSyncSceneCoords = << -260.614014, 292.105988, 91.126999 >>
VECTOR 		vecSyncSceneRotation = << 0.000072,-2.500116,89.639977 >>

INT 					  iSoundId_CameraFlash
VECTOR 					  vCameraFlashFX_Offset = << 0.070, -0.130, 0.070 >>
STRING 					  sAudioBank_CameraFlashSoundFX = "Distant_Camera_Flash"
STRING 					  sCamSoundSetName = "PAPARAZZO_01_SOUNDSET"
BOOL 					  bFlashDone

STRING 					  sFailReason = "DEFAULT"

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "...Random Character Script was triggered so additional cleanup required") 
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_03_SCENE")
		STOP_AUDIO_SCENE("PAPARAZZO_03_SCENE")
	ENDIF
	
	RELEASE_SOUND_ID(iSoundId_CameraFlash)
	
	IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL])
		FREEZE_ENTITY_POSITION(dumpsterLids[OBJ_LIDL], TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR])
		FREEZE_ENTITY_POSITION(dumpsterLids[OBJ_LIDR], TRUE)
	ENDIF

	SAFE_RELEASE_OBJECT(dumpsterLids[OBJ_LIDL])
	SAFE_RELEASE_OBJECT(dumpsterLids[OBJ_LIDR])
	SAFE_RELEASE_OBJECT(dumpsterBlue)
	SAFE_RELEASE_OBJECT(dumpsterGreen)
	
	REMOVE_ANIM_DICT(leadInOutAnimDict)
	REMOVE_PTFX_ASSET()
	
	REMOVE_PED_FOR_DIALOGUE(conversation, ENUM_TO_INT(CHAR_FRANKLIN))
	REMOVE_PED_FOR_DIALOGUE(conversation, 3)
	
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	CLEAR_PRINTS()
	
	SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[BEV])
	RC_CleanupSceneEntities(sRCLauncherDataLocal, TRUE)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Sets the mission stage, and reset the sub state
/// PARAMS:
///    MISSION_STAGE eNewStage = new main stage
PROC SET_STAGE(MISSION_STAGE eNewStage)
	missionstage = eNewStage
	m_eSubState = SS_SETUP
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Mission passed")
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK, FALSE)
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[BEV]) //Bev will be inside the closed dumpster now, so delete him
	Random_Character_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Sets the fail reason, and goes to the fail stage
/// PARAMS:
///    sFail - the fail reason
PROC SET_MISSION_FAILED(STRING sFail)
	STOP_SCRIPTED_CONVERSATION(FALSE)
	CLEAR_PRINTS()
	
	IF missionStage <> MS_FAIL
		sFailReason = sFail
		SET_STAGE(MS_FAIL)
	ENDIF
ENDPROC

/// PURPOSE:
///    Safely fails and cleans up the script
PROC Script_Failed()
	Random_Character_Failed()
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Checks if conditions are met that will fail the mission, for example if Barry is scared away
///    Plays a fail conversation if needed
/// RETURNS:
///    TRUE if mission has failed, else return FALSE
FUNC BOOL FAIL_CHECKS()	
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN TRUE
		
	ELIF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[BEV])
	
		IF IS_ENTITY_DEAD(sRCLauncherDataLocal.pedID[BEV])
			SET_MISSION_FAILED("P3BEVDEAD") // Bev died.
			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: FAIL HURT BEV") 
			RETURN TRUE
		ELIF (GET_PED_MAX_HEALTH(sRCLauncherDataLocal.pedID[BEV]) > GET_ENTITY_HEALTH(sRCLauncherDataLocal.pedID[BEV]))
			SET_MISSION_FAILED("P3BEVHURT") // Bev was injured
			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: FAIL HURT BEV") 
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Fails and cleans up the mission correctly
PROC STAGE_FAIL()

	SWITCH m_eSubState	
	
		CASE SS_SETUP
			IF ARE_STRINGS_EQUAL(sFailReason,"DEFAULT")
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason, TRUE)
			ENDIF
			m_eSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE	
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()			
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Do we need to resolve the mission start vehicle to a location that is further away?
/// RETURNS:
///    TRUE if the vehicle is not a concern (small enough or doesn't exist)
FUNC BOOL IS_RC_PLAYER_VEHICLE_UNDER_SIZE_LIMIT()
	VECTOR vecVehicleSizes[2], vecMaxVehicleSize
	VEHICLE_INDEX vehTmp = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehTmp)
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTmp), vecVehicleSizes[0], vecVehicleSizes[1])
		vecMaxVehicleSize = GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
		IF vecVehicleSizes[1].x - vecVehicleSizes[0].x > vecMaxVehicleSize.x
			RETURN FALSE
		ENDIF
		IF vecVehicleSizes[1].y - vecVehicleSizes[0].y > vecMaxVehicleSize.y
			RETURN FALSE
		ENDIF
		IF vecVehicleSizes[1].z - vecVehicleSizes[0].z > vecMaxVehicleSize.z
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Create leadout as soon as cutscene has finished, or on checkpoint
PROC CREATE_LEADOUT()
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[BEV]) AND DOES_ENTITY_EXIST(dumpsterGreen)
			
		iSyncSceneLeadOut = CREATE_SYNCHRONIZED_SCENE(vecSyncSceneCoords, vecSyncSceneRotation)
		
		TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iSyncSceneLeadOut, leadInOutAnimDict, "leadout_pap_3_rcm_beverly", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_EXPLOSION)

		// Dumpster
		IF DOES_ENTITY_EXIST(dumpsterGreen) 
			PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterGreen, iSyncSceneLeadOut, "leadout_pap_3_rcm_dumpster", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		ENDIF
	
		// Dumpster lid (left)
		IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL]) 
			PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDL], iSyncSceneLeadOut, "leadout_pap_3_rcm_lid_l", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
		ENDIF
		
		// Dumpster lid (right)
		IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR]) 
			PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDR], iSyncSceneLeadOut, "leadout_pap_3_rcm_lid_r", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
		ENDIF
		
		// Skip a few frames so lids don't pop
		SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadOut, 0.02)

	ENDIF
	
	bLeadOutConv = FALSE
ENDPROC

/// PURPOSE:
///    Triggers the visual and sound effect for taking a picture
PROC BevTakePhoto()
	
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.objID[OBJ_CAM])
	AND NOT bFlashDone

		CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Camera exists") 
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_rcpap1_camera", GET_PED_BONE_COORDS(sRCLauncherDataLocal.pedID[BEV], BONETAG_PH_R_HAND, vCameraFlashFX_Offset), GET_ENTITY_ROTATION(sRCLauncherDataLocal.objID[OBJ_CAM]))
		
		IF NOT HAS_SOUND_FINISHED(iSoundId_CameraFlash)
			STOP_SOUND(iSoundId_CameraFlash)
		ENDIF 
		
		IF REQUEST_SCRIPT_AUDIO_BANK(sAudioBank_CameraFlashSoundFX)
			PLAY_SOUND_FROM_COORD(iSoundId_CameraFlash, "CAMERA", GET_PED_BONE_COORDS(sRCLauncherDataLocal.pedID[BEV], BONETAG_PH_R_HAND, vCameraFlashFX_Offset), sCamSoundSetName)
			bFlashDone = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Resets certain locations etc
PROC RestartCheckpoint()
	RC_START_Z_SKIP()
	CREATE_LEADOUT()
	SET_STAGE(MS_LEADOUT)
	SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-258.8661, 292.7882, 90.5764>>, 153.0393)
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Sets up important initial parts of mission
PROC INIT()
	
	bLeadInConv = FALSE
	
	// Get the PLAYER relationship group
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		relGroupPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
	ENDIF
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
		SET_PED_RELATIONSHIP_GROUP_HASH(sRCLauncherDataLocal.pedID[BEV], relGroupPlayer)
		SET_PED_CONFIG_FLAG(sRCLauncherDataLocal.pedID[BEV], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_KEEP_TASK(sRCLauncherDataLocal.pedID[BEV], TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[BEV], TRUE)
	ENDIF
	
	// Grab the entities
	IF NOT DOES_ENTITY_EXIST(dumpsterBlue)
		dumpsterBlue = GET_CLOSEST_OBJECT_OF_TYPE(<< -261.19, 292.22, 90.79 >>, 10.0, modDumpBlue)
		SET_CAN_CLIMB_ON_ENTITY(dumpsterBlue, FALSE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(dumpsterGreen)
		ASSIGN_OBJECT_INDEX(dumpsterGreen, sRCLauncherDataLocal.objID[OBJ_DUMPSTER])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL])
		ASSIGN_OBJECT_INDEX(dumpsterLids[OBJ_LIDL], sRCLauncherDataLocal.objID[1])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR])
		ASSIGN_OBJECT_INDEX(dumpsterLids[OBJ_LIDR], sRCLauncherDataLocal.objID[2])
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(conversation, ENUM_TO_INT(CHAR_FRANKLIN), PLAYER_PED_ID(), "FRANKLIN")
	ADD_PED_FOR_DIALOGUE(conversation, 3, sRCLauncherDataLocal.pedID[BEV], "BEVERLY")	

	// Request text
	REQUEST_ADDITIONAL_TEXT("PAP3", MISSION_TEXT_SLOT)

	// Animation dictionary
	REQUEST_ANIM_DICT(leadInOutAnimDict)

	// Particles
	REQUEST_PTFX_ASSET()
	
	WHILE NOT HAS_ANIM_DICT_LOADED(leadInOutAnimDict)
	OR NOT HAS_PTFX_ASSET_LOADED()
		RC_PLAYER_TRIGGER_SCENE_LOCK_IN() 
		WAIT(0)
	ENDWHILE
	
	iSoundId_CameraFlash = GET_SOUND_ID()

	IF IS_REPLAY_IN_PROGRESS()

		CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Replay in progress") 
		
		SWITCH GET_REPLAY_MID_MISSION_STAGE()
			
			CASE CP_AFTER_MOCAP
				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: CP_AFTER_MOCAP replay") 
				RestartCheckpoint()
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
			BREAK
		ENDSWITCH
	ELSE
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID())						
				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: STAGE_END_CUTSCENE_MOCAP - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE player")
			ENDIF
		ENDIF
		SET_STAGE(MS_LEAD_IN)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles head tracking between Franklin and Beverly
PROC UPDATE_HEAD_TRACKING()

	IF IS_PLAYER_PLAYING(PLAYER_ID()) 
	AND IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[BEV])
		SET_IK_TARGET(sRCLauncherDataLocal.pedID[BEV], IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
		SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, sRCLauncherDataLocal.pedID[BEV], ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Do focus push in first person only
PROC DO_FOCUS_PUSH()
	IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
		SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[BEV], (<<0, 0, 0>>), TRUE, -1, 1500, DEFAULT_INTERP_OUT_TIME)
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the lead in then triggers the mocap
PROC LEAD_IN()

	RC_PLAYER_TRIGGER_SCENE_LOCK_IN() 
	
	UPDATE_HEAD_TRACKING()
	
	SWITCH m_eSubState
		
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Start - SETUP LEAD_IN")
	
			IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_03_SCENE")
				START_AUDIO_SCENE("PAPARAZZO_03_SCENE")
			ENDIF
			
			bFlashDone = FALSE
			RC_REQUEST_CUTSCENE("PAP_3_RCM")

			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND DOES_ENTITY_EXIST(dumpsterGreen)
				
				// Create synchronised scene
				iSyncSceneLeadIn = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(dumpsterGreen), GET_ENTITY_ROTATION(dumpsterGreen))
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iSyncSceneLeadIn, leadInOutAnimDict, "leadin_pap_3_rcm_beverly", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				
				// Dumpster
				IF DOES_ENTITY_EXIST(dumpsterGreen) 
					PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterGreen, iSyncSceneLeadIn, "leadin_pap_3_rcm_dumpster", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				// Dumpster lid (left)
				IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL]) 
					PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDL], iSyncSceneLeadIn, "leadin_pap_3_rcm_lid_l", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				// Dumpster lid (right)
				IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR]) 
					PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDR], iSyncSceneLeadIn, "leadin_pap_3_rcm_lid_r", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Done lead-in animation")
				RC_END_CUTSCENE_MODE()
				m_eSubState = SS_UPDATE
			ENDIF

			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: End - SETUP LEAD_IN")
		BREAK
		
		CASE SS_UPDATE
		
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV]) AND DOES_ENTITY_EXIST(dumpsterGreen)
			
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadIn)
					IF (GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadIn) > 0.99)
						
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadInIdle)
							
							CPRINTLN(DEBUG_MISSION, "Setting up the idle synch scene")
							iSyncSceneLeadInIdle = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(dumpsterGreen), GET_ENTITY_ROTATION(dumpsterGreen))
							TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iSyncSceneLeadInIdle, leadInOutAnimDict, "idle_01_pap_3_rcm_beverly", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[BEV])
							
							// Dumpster
							IF DOES_ENTITY_EXIST(dumpsterGreen)
								PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterGreen, iSyncSceneLeadInIdle, "idle_01_pap_3_rcm_dumpster", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterGreen)
							ENDIF
							
							// Dumpster lid (left)
							IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDL], iSyncSceneLeadInIdle, "idle_01_pap_3_rcm_lid_l", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterLids[OBJ_LIDL])
							ENDIF
				
							// Dumpster lid (right)
							IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDR], iSyncSceneLeadInIdle, "idle_01_pap_3_rcm_lid_r", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
								FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterLids[OBJ_LIDR])
							ENDIF
						ENDIF
					ENDIF

					IF (GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadIn) > 0.3)
						IF NOT bLeadInConv
							bLeadInConv = CREATE_CONVERSATION(conversation, "PAP3AU", "PAP3_RCM_LI", CONV_PRIORITY_HIGH)
						ENDIF
					ENDIF
					
					IF(GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadIn) > 0.3)
						BevTakePhoto()
					ENDIF
					
					IF (GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadIn) > 0.05)
						DO_FOCUS_PUSH()
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Beverly not playing leadin anim anymore - skip to cutscene for safety")
					DO_FOCUS_PUSH()
					m_eSubState = SS_CLEANUP
				ENDIF
				
				IF bLeadInConv
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Beverly not conversing anymore - skip to cutscene")
						m_eSubState = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: CLEANUP LEAD_IN")
			DO_FOCUS_PUSH()
			SET_STAGE(MS_INTRO)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Plays the intro mocap and then passes the mission
PROC INTRO()

	SWITCH m_eSubState
	
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Init MS_INTRO")
			
			DO_FOCUS_PUSH()

			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID())
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE player")
				ENDIF
			ENDIF
			
			IF RC_IS_CUTSCENE_OK_TO_START()

				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: MS_INTRO cutscene ok to start")
				
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[BEV])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[BEV], sSceneHandleBev, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Franklin, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL])
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: lidl exists")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterLids[OBJ_LIDL], dumpLidLSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modDumpL)
				ELSE
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: lidl doesn't exist")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterLids[OBJ_LIDL], dumpLidLSceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modDumpL)
				ENDIF
				
				IF DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR])
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: lidr exists")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterLids[OBJ_LIDR], dumpLidRSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modDumpR)
				ELSE
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: lidr doesn't exist")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterLids[OBJ_LIDR], dumpLidRSceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modDumpR)
				ENDIF
				
				IF DOES_ENTITY_EXIST(dumpsterBlue)
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: dumpsterBlue exists")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterBlue, dumpBlueSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modDumpBlue)
				ELSE
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: dumpsterBlue doesn't exist")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterBlue, dumpBlueSceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modDumpBlue)
				ENDIF
				
				IF DOES_ENTITY_EXIST(dumpsterGreen)
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: dumpsterGreen exists")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterGreen, dumpGreenSceneHandle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, modDumpGreen)
				ELSE
					CPRINTLN(DEBUG_MISSION, "Paparazzo 3: dumpsterBlue doesn't exist")
					REGISTER_ENTITY_FOR_CUTSCENE(dumpsterGreen, dumpGreenSceneHandle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, modDumpGreen)
				ENDIF

				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_LOWEST)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)

				// Cleanup launcher to remove lead-in blip
				RC_CLEANUP_LAUNCHER()
				
				// Start mocap scene
				START_CUTSCENE()
				WAIT(0)
				
				// B*1988959 - handle unusually large vehicles
				IF IS_RC_PLAYER_VEHICLE_UNDER_SIZE_LIMIT()
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-262.08163,292.330,90.000>>, <<-254.652,292.335,93.500>>, 5.0, <<-257.08, 285.27, 90.43>>, 12.52)
				ELSE
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<< -254.8715, 316.4631, 88.6055 >>, << -255.2926, 270.6087, 103.2327 >>, 30.0, <<-239.7607, 267.4757, 91.0999>>, 86.5)
				ENDIF
				RC_START_CUTSCENE_MODE(<< -259.3334, 291.8268, 90.5229 >>)
				
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[OBJ_CAM])
				m_eSubState = SS_UPDATE// Monitor cutscene
			ENDIF
		BREAK
		
		CASE SS_UPDATE
		
			IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT(TRUE)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandleBev)
				iSyncSceneLeadOut = CREATE_SYNCHRONIZED_SCENE(vecSyncSceneCoords, vecSyncSceneRotation)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[BEV], iSyncSceneLeadOut, leadInOutAnimDict, "leadout_pap_3_rcm_beverly", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_EXPLOSION)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sRCLauncherDataLocal.pedID[BEV])
				CPRINTLN(DEBUG_MISSION, "Set exit state on beverley")
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Franklin)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-258.8661, 292.7882, 90.5764>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 153.0417)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(dumpsterGreen)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpGreenSceneHandle, modDumpGreen))
					dumpsterGreen = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpGreenSceneHandle, modDumpGreen))
				ENDIF
			ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadOut)
			AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(dumpGreenSceneHandle)
				SET_ENTITY_COORDS(dumpsterGreen, vecSyncSceneCoords, FALSE)
				SET_ENTITY_ROTATION(dumpsterGreen, vecSyncSceneRotation, EULER_YXZ, FALSE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterGreen, iSyncSceneLeadOut, "leadout_pap_3_rcm_dumpster", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterGreen)
				CPRINTLN(DEBUG_MISSION, "Set exit state on dumpsterGreen")
			ENDIF

			IF NOT DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDL])
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpLidLSceneHandle))
					dumpsterLids[OBJ_LIDL] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpLidLSceneHandle, modDumpL))
				ENDIF
			ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadOut)
			AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(dumpLidLSceneHandle)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDL], iSyncSceneLeadOut, "leadout_pap_3_rcm_lid_l", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterLids[OBJ_LIDL])
				CPRINTLN(DEBUG_MISSION, "Set exit state on dumpLidLSceneHandle")
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(dumpsterLids[OBJ_LIDR])
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpLidRSceneHandle))
					dumpsterLids[OBJ_LIDR] = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpLidRSceneHandle, modDumpR))
				ENDIF
			ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadOut)
			AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(dumpLidRSceneHandle)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(dumpsterLids[OBJ_LIDR], iSyncSceneLeadOut, "leadout_pap_3_rcm_lid_r", leadInOutAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterLids[OBJ_LIDR])
				CPRINTLN(DEBUG_MISSION, "Set exit state on dumpLidRSceneHandle")
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(dumpsterBlue)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpBlueSceneHandle))
					dumpsterBlue = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(dumpBlueSceneHandle, modDumpBlue))
				ENDIF
			ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadOut)
			AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(dumpBlueSceneHandle)
				SET_ENTITY_COORDS(dumpsterBlue, <<-261.113159,294.195557,90.686119>>, FALSE)
				SET_ENTITY_ROTATION(dumpsterBlue, <<-0.604164,-2.332095,89.8831>>, EULER_YXZ, FALSE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(dumpsterBlue)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_LOWEST)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				DISPLAY_RADAR(TRUE)
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED()
				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Cutscene finished")
				m_eSubState = SS_CLEANUP
			ENDIF
		BREAK
	ENDSWITCH
	
	// Need to create leadout RIGHT NOW - previous flow allowed a two frame gap
	IF m_eSubState = SS_CLEANUP
		CPRINTLN(DEBUG_MISSION, "Paparazzo 3: Cleaning up MS_INTRO") 
		RC_END_CUTSCENE_MODE()
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		SET_STAGE(MS_LEADOUT)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    plays lead out
PROC LEADOUT()
	
	UPDATE_HEAD_TRACKING()
	
	IF NOT HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[BEV])
		SWITCH m_eSubState
			
			CASE SS_SETUP
				CPRINTLN(DEBUG_MISSION, "Paparazzo 3: LEADOUT - setup")
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[BEV])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherDataLocal.pedID[BEV], TRUE)
				ENDIF
				m_eSubState  = SS_UPDATE
			BREAK
			
			CASE SS_UPDATE
				IF NOT bLeadOutConv
					bLeadOutConv = CREATE_CONVERSATION(conversation, "PAP3AU", "PAP3_RCM_LO", CONV_PRIORITY_HIGH)
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneLeadOut)
							IF(GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneLeadOut) >= 1.0)
								m_eSubState = SS_CLEANUP
							ENDIF
						ELSE
							m_eSubState = SS_CLEANUP
						ENDIF
					ENDIF
				ENDIF		
			BREAK
			
			CASE SS_CLEANUP
				CPRINTLN(DEBUG_MISSION, "LEADOUT - cleanup")
				Script_Passed()
			BREAK
		ENDSWITCH
	ELSE
		missionStage = MS_FAIL
		m_eSubState = SS_SETUP
		sFailReason = "P3BEVSCARED"
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()
		
		// check for P skip- restart intro
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			WAIT_FOR_CUTSCENE_TO_STOP()
			RC_START_Z_SKIP()
				SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[BEV]) // Delete Beverly
				SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[BEV]) // Delete Initial Scene car
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[OBJ_CAM])
				eInitialSceneStage = IS_REQUEST_SCENE
				WHILE NOT SetupScene_PAPARAZZO_3(sRCLauncherDataLocal)
					WAIT(0)
				ENDWHILE
				SET_STAGE(MS_INIT)
			RC_END_Z_SKIP()
		ENDIF
		
		// Check for Pass (j skip passes as mission is just a cut-scene)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)) OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			WAIT_FOR_CUTSCENE_TO_STOP()
			Random_Character_Failed()
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	eInitialSceneStage = IS_REQUEST_SCENE
		g_bSceneAutoTrigger = TRUE
		WHILE NOT SetupScene_PAPARAZZO_3(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		VEHICLE_INDEX vTmp
		CREATE_VEHICLE_FOR_REPLAY(vTmp, <<-257.08, 285.27, 90.43>>, 12.52, FALSE, FALSE, FALSE, FALSE, FALSE)
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		WAIT(0)

		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_PTP")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		IF missionStage = MS_FAIL
			STAGE_FAIL()
		ELSE
			IF NOT FAIL_CHECKS() //if not failed mission
				SWITCH missionStage
					CASE MS_INIT
						INIT()
					BREAK
					
					CASE MS_LEAD_IN
						LEAD_IN()
					BREAK
					
					CASE MS_INTRO
						INTRO()
					BREAK
					
					CASE MS_LEADOUT
						LEADOUT()
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		
		// Check debug keys
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
