// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Paparazzo1.sc
//		AUTHOR			:	Ahron Mason
//		DESCRIPTION		:	Car chase where Franklin drives for a Paparazzi photographer 
//							named Beverly, so he can get some photos of a celebrity.							
//
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct 
USING "initial_scenes_Paparazzo.sch"	//needed for call to SetupScene_PAPARAZZO_1 to setup scene again when z / p skipping back to start of the mission
USING "lineactivation.sch"
USING "taxi_functions.sch"
USING "commands_recording.sch"
#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//												 CONSTANTS FOR UBER PLAYBACK
//USING "traffic_default_values.sch" // this must be included before traffic.sch NOTE: when using uber, revert to below Constants instead
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      105
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       70    
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    50   
// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           20  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         4                            
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            5
USING "traffic.sch"

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	ENUMS:
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Mission stages
ENUM MISSION_STAGE
	MISSION_STAGE_INTRO_MOCAP_SCENE,  //breaks P skip if this is declared but not in use
	MISSION_STAGE_CHASE_CELEB_IN_VEHICLE,
	MISSION_STAGE_RIVAL_PAP_JOIN_CHASE,
	MISSION_STAGE_STOP_RIVAL_PAP,
	MISSION_STAGE_DROP_BEVERLY_OFF,
	MISSION_STAGE_END_CUTSCENE,	
	MISSION_STAGE_MISSION_PASSED,
	//Additional stages
	MISSION_STAGE_LOSE_THE_COPS,	
	MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE,
	//debug stages
	MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE,
	MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE,	
	MISSION_STAGE_DEBUG
ENDENUM

/// PURPOSE: each mission stage uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

/// PURPOSE: fail conditions
ENUM FAILED_REASON_ENUM
	FAILED_DEFAULT = 0,
	FAILED_CELEB_DIED,
	FAILED_CELEB_ESCAPED,
	FAILED_PLAYER_ATTACKED_CELEB,
	//FAILED_PLAYER_ATTACKED_BEVERLY,
	FAILED_BEVERLY_VEHICLE_WRECKED,
	FAILED_BEVERLY_DIED,
	FAILED_PAP_ESCAPED,
	FAILED_LEFT_BEVERLY_BEHIND,
	FAILED_BEVERLY_SPOOKED
ENDENUM

/// PURPOSE:  States in Beverly taking photos of the celeb state
ENUM PAP1_BEVELRY_TAKE_PHOTO_STATE
	PAP1_BTPS_SETUP,
	PAP1_BTPS_TAKING_PHOTO,
	PAP1_BTPS_SUCCESSFUL_PHOTO,
	PAP1_BTPS_SETUP_CHECKING_PHOTO,
	PAP1_BTPS_CHECKING_PHOTO,
	PAP1_BTPS_FINISHED_CHECKING_PHOTO
ENDENUM

/// PURPOSE: States in the Road Rash style attack
ENUM KNOCKOFF_BIKE_ATTACK_STATE
	KBAS_SETUP_ATTACK,
	KBAS_CHARGING_ATTACK,
	KBAS_RUNNING_ATTACK,
	KBAS_SUCCESSFUL_ATTACK,
	KBAS_CLEANUP_ATTACK
ENDENUM

/// PURPOSE: Player on Bev's bike states
ENUM PAP1_PLAYER_TRANSPORT_STATE_ENUM
	PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV,
	PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE,
	PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY
ENDENUM

/// PURPOSE: states for Miranda playing her shooting up animations
ENUM PAP1_PED_SHOOTING_UP_PROGRESS
	PAP1_PED_SHOOTING_UP_SETUP,
	PAP1_PED_SHOOTING_UP_UPDATE,
	PAP1_PED_SHOOTING_UP_CLEANUP
ENDENUM

ENUM PAP1_RIVAL_PAP_ON_BIKE_PROGRESS
	PAP1_RIVAL_PAP_ON_BIKE_TRIGGER_JOIN_CHASE,
	PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA,
	PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION,
	PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR,
	PAP1_RIVAL_PAP_ON_BIKE_CLEANUP
ENDENUM

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:STRUCTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

// used by scripted vehicles on the mission
STRUCT STRUCT_MISSION_VEHICLE
	VEHICLE_INDEX	vehicle
	MODEL_NAMES		model
	BLIP_INDEX		blip
	VECTOR			vSpawn
	FLOAT			fSpawnHeading
ENDSTRUCT

//used by scripted peds on the mission
STRUCT STRUCT_MISSION_PED
	PED_INDEX 		ped
	MODEL_NAMES		model
	BLIP_INDEX		blip
	VECTOR			vSpawn
	FLOAT			fSpawnHeading
ENDSTRUCT

//used by scripted objects on the mission
STRUCT STRUCT_OBJECT 
	OBJECT_INDEX 	object
	VECTOR 			vPosition
	VECTOR			vOffset
	VECTOR 			vRotation
	MODEL_NAMES		model
ENDSTRUCT

STRUCT PAP1_SHAPE_TEST_CAMERA_AIM
	SHAPETEST_INDEX mShapeTest
	VECTOR vCheckPos
	VECTOR vStartPos
	BOOL bActive
	#IF IS_DEBUG_BUILD
		VECTOR vStartPosDebug
		VECTOR vEndPosDebug
	#ENDIF
ENDSTRUCT

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:CONSTANTS
//-------------------------------------------------------------------------------------------------------------------------------------------------

CONST_FLOAT CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE						3.5
CONST_FLOAT CLOSE_ENOUGH_FOR_BEVERLY_KNOCK_OFF_DISTANCE				0.8				// 1.2
CONST_FLOAT KNOCKOFF_BIKER_ATTACK_HIT_EXECUTION_ZONE_WIDTH			1.6
CONST_FLOAT BEVERLY_KO_PAP_ZONE_START_ANIM_WIDTH					1.6
CONST_FLOAT PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK				0.93			// 1.0	reduced overall base speed by 7% for imran bug 363264
CONST_FLOAT UBER_PLAYBACK_TIME_RIVAL_PAP_JOIN_CHASE_STAGE			56000.000		//switch stages at this point in the recording		
CONST_FLOAT UBER_PLAYBACK_TIME_FOR_TRIGGER_RIVAL_PAP_RECORDING		60550.0			//rival pap trigger time
CONST_FLOAT UBER_PLAYBACK_TIME_STOP_RIVAL_PAP_STAGE					75250.0 		//74615.5	//switch stages at this point in the recording		
CONST_FLOAT BIKE_TOP_SPEED_MODIFIER 								-8.0		
CONST_FLOAT KO_BIKER_START_ATTACK_HEADING_THRESOLD					0.4
CONST_FLOAT KO_BIKER_EXECUTE_ATTACK_HEADING_THRESOLD				0.5
CONST_FLOAT SHAPE_TEST_PAP1_CAMERA_AIM_PROBE_HIT_DIST				1.3	//half car width   1.5 // size for actual ped position in rear passenger seat
CONST_FLOAT SHAPE_TEST_PAP1_CAMERA_AIM_VIEW_CONE					90.0	// 50.0 increased since this was often cause the check to fail when it looked as though it should register.
CONST_FLOAT PAP1_RIVAL_PAP_END_RECORDING_POINT						21000.0
CONST_FLOAT PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE 					0.68		// targets blip flashes when he gets further than this percentage away to CELEB_ESCAPED_DIST
CONST_FLOAT PAP1_RIVAL_PAP_CHASE_BLIP_FLASH_PERCENTAGE				0.65		// targets blip flashes when he gets further than this percentage away to RIVAL_PAP_ESCAPED_DIST
CONST_INT CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD					10			// grace period in frame count for can ped see ped tests
CONST_INT PLAYER_LEFT_BEVERLY_DISTANCE								60
CONST_INT BEVERLY_LEAVE_PLAYER_GROUP_DISTANCE						20
CONST_INT BEVERLY_REJOIN_PLAYER_GROUP_DISTANCE						8
CONST_INT CLOSE_TO_RIVAL_PAP_CAM_CUTSCENE_TRIGGER_DIST				28
CONST_INT CLOSE_ENOUGH_FOR_PLAYER_RIVAL_CONVO						35
CONST_INT CELEB_ESCAPED_DIST 										225
CONST_INT RIVAL_PAP_ESCAPED_DIST 									220
CONST_INT TOTAL_TOOK_PHOTO_DIALOGUE									6
CONST_INT TOTAL_CHASE_RIVAL_PAP_DIALOGUE							3
CONST_INT TOTAL_BEVERLY_TAKE_PHOTOS_AREAS							2
CONST_INT TOTAL_KNOCK_OFF_BIKER_ZONES								2
CONST_INT TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE					3000
CONST_INT TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE_LONG				10000
CONST_INT TIMER_TILL_RIVAL_PAP_FLEES								6000
CONST_INT DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE				20
CONST_INT DIST_LIMIT_PLAYER_CLOSE_DIST_TO_RIVAL_PAP_IN_CHASE		12
CONST_INT DIST_LIMIT_PLAYER_MEDIUM_DIST_TO_RIVAL_PAP_IN_CHASE		25
CONST_INT DIST_LIMIT_PLAYER_CLOSE_TO_CELEB_IN_CHASE_FOR_DIALOGUE	75
CONST_INT TIME_DELAY_STOP_PLAYER_VEHICLE_RECORDING_FOR_STAGE_SKIP	2000
CONST_INT TIME_DELAY_FOR_REPEATED_BEVERLY_FRUSTRATED_ANIM			3000
CONST_INT KNOCKOFF_BIKER_ATTACK_SIDE_NULL							-1
CONST_INT KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT							0
CONST_INT KNOCKOFF_BIKER_ATTACK_SIDE_LEFT							1
CONST_INT FINISH_UBER_RECORDING_EARLY_TIME							80132
CONST_INT CP_PAP1_CHASE_CELEB_IN_VEHICLE							0
CONST_INT CP_PAP1_STOP_RIVAL_PAP									1
CONST_INT CP_PAP1_DROP_BEVERLY_OFF									2
CONST_INT CP_PAP1_MISSION_PASSED									3
CONST_INT Z_SKIP_INTRO												0
CONST_INT Z_SKIP_CHASE_CELEB_IN_VEHICLE								1
CONST_INT Z_SKIP_ENTER_RIVAL_PAP									2
CONST_INT Z_SKIP_STOP_RIVAL_PAP										3
CONST_INT Z_SKIP_DROP_BEVERLY_OFF									4
CONST_INT Z_SKIP_OUTRO												5
CONST_INT Z_SKIP_MISSION_PASSED										6
CONST_INT NUM_PAP1_SHAPE_TESTS										4
CONST_INT SHAPETEST_PAP1_ID_INITIAL_TEST							0
CONST_INT SHAPETEST_PAP1_ID_SECONDARY_TEST							1
CONST_INT GRACE_PERIOD_FRAMES_LAST_SUCCESSFUL_SHAPE_TEST_CAMERA_AIM 8
CONST_INT PAP1_MAX_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE				2
CONST_INT REPLAY_PHOTOS_TAKEN										0 	//index to replay array
CONST_INT PAP1_MUSIC_EVENT_START									0	// Music cue
CONST_INT PAP1_MUSIC_EVENT_BIKE										1	// Music cue
CONST_INT PAP1_MUSIC_EVENT_RIVAL									2	// Music cue
CONST_INT PAP1_MUSIC_EVENT_STOP_TO_RADIO							3	// Music cue
CONST_INT PAP1_MUSIC_EVENT_RESET_AUDIO_FLAG							4	// Music cue
CONST_INT PAP1_MUSIC_EVENT_FAIL										5	// Music cue
CONST_INT PAP1_MUSIC_EVENT_MAX										6	// Max number of Music cue
//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:VARIABLES:
//-------------------------------------------------------------------------------------------------------------------------------------------------

MISSION_STAGE eMissionStage 					= MISSION_STAGE_INTRO_MOCAP_SCENE //   MISSION_STAGE_INTRO_MOCAP_SCENE   MISSION_STAGE_DEBUG    MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE    MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE
MISSION_STAGE eMissionSkipTargetStage			//used in mission checkpoint setup and debug stage skipping
SUB_STAGE eSubStage 							= SS_SETUP
g_structRCScriptArgs sRCLauncherDataLocal
#IF IS_DEBUG_BUILD			// stage skipping
      CONST_INT MAX_SKIP_MENU_LENGTH 7
      MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF
BOOL bAreEntitiesHeadingsCompleteOpposite_KO_Biker
BOOL bBeverly_SpottedRivalPap_Anim
BOOL bBeverly_InitialPointAtCeleb_Anim
BOOL bDoDelayForPhotoTaken
BOOL bDoneDialogue_BeverlyTookPhoto[TOTAL_TOOK_PHOTO_DIALOGUE]
BOOL bDoneDialogue_BeverlySayKeepBike
BOOL bDoneDialogue_DropOffBeverly
BOOL bDoneDialogue_DropOffBeverlyAdditionalChat
BOOL bDoneDialogue_InitialChaseDialogue	
BOOL bDoneDialogue_InitialGetOnBike
BOOL bDoneDialogue_LoseTheCops_InitialChat
BOOL bDoneDialogue_RivalPapCameraDestroyed
BOOL bDoneDialogue_RivalPapTookPhoto[TOTAL_CHASE_RIVAL_PAP_DIALOGUE]
BOOL bDoneDialogue_FailWarningAttackMiranda
BOOL bDoneDialogue_BeverlyMissedKnockOffAttempt
BOOL bDoneDialogue_BeverlyThrowsPunch
BOOL bDoneDialogue_PlayerPullsWheelie
BOOL bDoneDialogue_MissionFailed
BOOL bDoneDialogue_FranklinRespondToBeverlyBerating
BOOL bDoneHintCamForRival
BOOL bDoneObjective_DropBeverlyOff
BOOL bDoneObjective_GetBackOnBike
BOOL bDoneObjective_GetCloseToCeleb	
BOOL bDoneObjective_GetOnBike
BOOL bDoneObjective_LoseTheCops
BOOL bDoneObjective_ReturnToBeverly
BOOL bDoneObjective_StopTheRivalPaparazzi
BOOL bDoneRivalPapFlee
BOOL bDoneRivalPapRemovedFromDialogue
BOOL bDoneRivalPapSurrendered
BOOL bDoneSetupCelebForCleanup
BOOL bDoneSetupRivalPapDriverToActInjured
BOOL bDoneWaitForCameraDestroyedDialogue
BOOL bDone_KnockOff_Biker_SwipeSoundFX
BOOL bDone_ShowSpecialAbilityHelp
BOOL bFinishedStageSkipping	= TRUE					//used to determine if we are mission replay checkpoint skipping or debug skipping
BOOL bLoadedWorldForStageSkipping = FALSE	// flag to say if we haveloaded the world around the player when stage skipping
BOOL bHasBeverlyBeenRemovedFromPlayerGroup
BOOL bHadToFadeOutToLoadMissionAssets
BOOL bHasPlayerGotOnBike_FirstTime
BOOL bPausedDialogue_DropOffBeverly
BOOL bStatTracker_HasBeverlyMissedWithSwing
BOOl bHasPlayerShotRivalPapOffBike
BOOL bUseNewCelebChaseRubberbanding = TRUE	// NOTE: not currently reset in INIT_MISSION!
BOOL bHasSetExitStateFromIntroMocapForGameplayCam
BOOL bPlayerNeedsHelmentRestoreAfterOutroMocap
BOOL bRequestedOutroMocap
BOOL bCheckForPlayerHelmetChange
BLIP_INDEX blipIndexDropOffPoint
CAMERA_INDEX camCutsceneFinish
CAMERA_INDEX camCutsceneStart
FAILED_REASON_ENUM eP1_MissionFailedReason
FLOAT fRivalBikeSpeed	
FLOAT fCurrentChaseDistanceCeleb	
FLOAT fCurrentPlaybackTime
FLOAT fDistance_PlayerToBeverly
FLOAT fDistance_PlayerToRivalPap
FLOAT fKnockOff_Biker_Anim_HitDonePoint
FLOAT fKnockOff_Biker_Anim_HitStartPoint
FLOAT fKnockOff_Biker_Anim_SwipeSFXstartPoint
FLOAT fKnockOff_Biker_Anim_TotalTime
FLOAT fKnockOff_Biker_HitExecutionArea_Length
FLOAT fKnockOff_Biker_InitialAnimSpeed
FLOAT fKnockOff_Biker_StartAnimWidthModifier
FLOAT fKnockOff_Biker_UpdatedAnimSpeed
FLOAT fMainPlaybackSpeed
FLOAT fTimeInRivalPapRecording
INT iCheckChaseSpeedTimer	
INT iCheckForCelebtVehicleHornTimer
INT iCelebVehicleHornTime
INT iConsecutiveAimingTimer
INT iDialogueTimer_ChasingCeleb
INT iDialogueTimer_ChasingRivalPaparazzi
INT iDialogueTimer_LoseTheCopsAdditionalChat
INT iDialogueTimer_PlayerOffTheBike
INT iDialogueTimer_BevLeftBehind
INT iDialogueTimer_TakingLongTimeToReachDropOffPoint
INT iDialogueTimer_RivalPapAmbientLines
INT iFrameCountSinceLastSuccessfulShapeTestCameraAim
INT iGoodPhotosTakenCounter	
INT iKnockOff_Biker_Attack_AnimSide
INT iKnockOff_Biker_Attack_Side
INT iNumFlashesForRivalCamera
INT iPreLoadRecordingsTimer
INT iRivalPapCameraFlashTimer
INT iRivalTakesPhotosBurst
INT iShapeTestCamerAimIndex_SetupThisFrame
INT iSoundId_BikeAttack_MeleeHit
INT iSoundId_BikeAttack_MeleeSwipe
INT iSoundId_CameraFlash
INT iTimerDelay_Progress_BevTakingPhotos
INT iTimerDelay_RivalPapGestureAnim
INT iTimerIncreaseCelebSpeedForPhotoTaken
INT iTimerTillRepeatBevFrustratedAnim
INT iTimer_DelayStopPlayerVehicleRecording_ForMissionStageSkip
INT iMissionMusicEventTriggerCounter
INT iTimer_DelayResetAudioFlag_AllowScoreAndRadio
INT iTimer_DelayBeverlyInitialTurnToFaceFranklin
INT iTimer_BeverlyBikeNotOnAllWheelsTimer
INT iCounter_BeverlyPhotoBurst
INT iMax_BeverlyPhotoBurst
INT iDelay_BeverlyPhotoBurst
INT iFrameCountBeverlyLastSeenPlayer
INT iStoredPlayerHelmetPropTextureIndex
INT iStoredPlayerHelmetPropIndex
INT iDialogueLines_RivalPapCameraWrecked
INT iScenarioBlockingAreaStage
INT iIntroFocusPushStage
INT iIntroFocusPushTimer
INT iDelayInitialChaseComment
KNOCKOFF_BIKE_ATTACK_STATE eKnockOffBikerAttackState
OBJECT_INDEX objTrafficLightWestBound, objTrafficLightEastBound // used to make the remaining traffic flow out the area
PAP1_BEVELRY_TAKE_PHOTO_STATE eBeverlyTakePhotoState
PAP1_PED_SHOOTING_UP_PROGRESS ePedShootingUpProgress
PAP1_RIVAL_PAP_ON_BIKE_PROGRESS eRivalPapOnBikeProgress
PAP1_PLAYER_TRANSPORT_STATE_ENUM ePlayerTransportState
PAP1_SHAPE_TEST_CAMERA_AIM mShapeTestCameraAim[NUM_PAP1_SHAPE_TESTS]
REL_GROUP_HASH relGroupFriendly
REL_GROUP_HASH relGroupEnemy
SCENARIO_BLOCKING_INDEX scenarioBlockingIntroArea
SCENARIO_BLOCKING_INDEX scenarioBlockingChaseRoute[PAP1_MAX_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE]
STRING sAnimDictName_Pap1Specific = "rcmpaparazzo1ig_2"	//contains new pap attack anims, new camera script aim gun task anims, rival pap hit reactions, dialogue on bike anims
STRING sAnimDictName_Pap1HandOverride = "rcmpaparazzo1ig_1"	// anim dict containing camera in hand override
STRING sSceneHandle_Franklin = "Franklin"
STRING sSceneHandle_Beverly = "Beverly"
STRING sSceneHandle_BeverlysCamera = "Beverlys_camera"
STRING sSceneHandle_BeverlysCameraFlash = "Camera_Flash"
STRING sSceneHandle_BeverlysVehicle = "PAP_Bike"
STRING sSceneHandle_MirandasVehicle = "Miranda_Car"
STRING sSceneHandle_Miranda = "Miranda"
STRING sSceneHandle_MirandasDriver = "Miranda_Bodyguard"
STRING sAnimNameBeverlyKnockOff = "atk_punch_left_pap"
STRING sAnimSet_BeverlyCameraWeaponMovementOnFoot = "RCM_Paparazzo"
STRING sPap1_AudioBank_CameraFlashSoundFX = "Distant_Camera_Flash"
STRING sPap1_Dialogue_TextBlockName = "PAP1AUD"
STRING sPap1_SoundSetName =  "PAPARAZZO_01_SOUNDSET"
STRING sPap1_UberRecordingName = "Pap1U"
structPedsForConversation sDialogue
STRUCT_MISSION_PED sBeverlyPed
STRUCT_MISSION_PED sCelebPed
STRUCT_MISSION_PED sCelebsDriverPed
STRUCT_MISSION_PED sRivalPapPed
STRUCT_MISSION_PED sRivalPapDriverPed
STRUCT_MISSION_VEHICLE sBeverlyVehicle
STRUCT_MISSION_VEHICLE sCelebVehicle
STRUCT_MISSION_VEHICLE sRivalPapVehicle
STRUCT_OBJECT sObjCameraBeverly
STRUCT_OBJECT sObjFlashUnitBeverly
STRUCT_OBJECT sObjCameraRivalPap
STRUCT_OBJECT sObjFlashUnitRivalPap
STRUCT_OBJECT sObjSyringe
TEXT_LABEL_63 tlAnimName_BeverlyPoint = "point_45_left_pap"
TEXT_LABEL_23 tlPausedDialogue_DropOffBeverly = ""
TEXT_LABEL_23 tlDialogueRoot_MissionFailed = ""
TEXT_LABEL_23 tlAnimDict_BeverlyShoutComeOn = "rcmpaparazzo1ig_1_waive"
TEXT_LABEL_23 tlCurrentAnim_BeverlyShoutComeOn = "waive_comeback_f"
TEXT_LABEL_23 tlDialogueRoot_CameraDestroyed = ""
TEXT_LABEL_23 tlOutroMocapName = "PAP_1_MCS_1"
TEXT_LABEL_23 tlRivalPapInjuredAnims = "rcmpaparazzo1"
VECTOR vBevCameraLensOffset
VECTOR vCameraFlashFX_Offset
VECTOR vCelebPosInCarOffset
VECTOR vClearZone_DropOffPoint_Max
VECTOR vClearZone_DropOffPoint_Min
VECTOR vDropOffPoint
VECTOR vKnockOff_Biker_HitExecutionArea_PosFar
VECTOR vKnockOff_Biker_HitExecutionArea_PosNear
VECTOR vKnockOff_Biker_HitExecutionArea_OffsetFar[TOTAL_KNOCK_OFF_BIKER_ZONES]
VECTOR vKnockOff_Biker_HitExecutionArea_OffsetNear[TOTAL_KNOCK_OFF_BIKER_ZONES]
VECTOR vRivalPapCam_CutsceneTriggerPos
VECTOR vRivalPapDriver_InitialFleeDrivePoint
VECTOR vTakePhotoZones_CentrePos[TOTAL_BEVERLY_TAKE_PHOTOS_AREAS]
VECTOR vTakePhotoZones_CentrePosOffset[TOTAL_BEVERLY_TAKE_PHOTOS_AREAS]
VECTOR vPathNodeRequestMin
VECTOR vPathNodeRequestMax
VECTOR vPathNodeRequestStartPosition

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:DEBUG FUNCS / PROCS / WIDGETS
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_PrintMissionInfoToTTY					= FALSE								//NEEDS TO BE FALSE!!
	BOOL bDebug_PrintMissionRubberbandInfoToTTY			= FALSE								//NEEDS TO BE FALSE!!
	BOOL bDebug_SetupMission_DebugStage					= FALSE
	BOOL bDebug_ResetHintCam							= FALSE
	BOOL bDebug_UpdateCelebWanderSpeed 					= FALSE
	FLOAT fDebug_CelebWanderSpeed						= 30.0
	BOOL bDebug_DisplayPhotoArea						= FALSE
	BOOL bDEbug_DisplayFranklinSpecialAbilityCharge		= FALSE	
	BOOL bDebug_DontKnockThePedsOffTheBike				= FALSE									//NEEDS TO BE FALSE!!
	BOOL bDebug_OverrideKnockOffAnimsForSetup			= FALSE
	BOOL bDebug_KnockOffAnims_ForcePunch				= FALSE
	BOOL bDebug_KnockOffAnims_OverrideAnimSpeed			= FALSE
	BOOL bDebug_KnockOffAnims_OverrideHitTimes			= FALSE	
	BOOL bDebug_KnockOffAnims_OverrideSwipeSFXTime		= FALSE	
	FLOAT fDebug_KnockOff_Biker_AnimSpeed				= 1.0
	FLOAT fDebug_KnockOff_Biker_Anim_HitStartPoint		= 0.4
	FLOAT fDebug_KnockOff_Biker_Anim_HitDonePoint		= 0.8
	FLOAT fDebug_KnockOff_Biker_Anim_SwipeSFXstartPoint	= 0.2	
	BOOL bDebug_DoArmIntersectionTest					= FALSE									//NEEDS TO BE FALSE!!
	//FLOAT fDebug_PelvisRadius							= 0.45	
	VECTOR vDebug_PelvisPosOffsetDriver					= << 0.0, 0.15, 0.6 >>
	VECTOR vDebug_PelvisPosOffsetPassenger				= << 0.0, -0.75, 0.65 >>	
	//FLOAT fDebug_HandRadius								= 0.075
	//FLOAT fDebug_ArmRadius								= 0.075	
	BOOL bDebug_ResetKObools							= FALSE
	BOOL bDebug_TogglePapDriveWander					= FALSE
	BOOL bDebug_ToggleKnockoffBikerDebugDraws			= FALSE									//NEEDS TO BE FALSE!!
	BOOL bDebug_RivalPapSpawnArea						= FALSE
	VECTOR vDebug_PositionPlayerFromCar
	VECTOR vDebug_PositionOffsetPlayerFromCar			= << -1.5, 0.50, 0.00 >>
	BOOL bDebug_RespotPlayerPos							= FALSE	
	VECTOR vDebug_CameraOffsetOutro						= << -1.0, 0.5, 0.75 >>
	VECTOR vDebug_CameraPointAtOffset					= << 2.0, 1.0, 0.75 >>	
	BOOL bDebug_ResetCameraOffsetOutro					= FALSE
	CAMERA_INDEX camDebugPositioning
	BOOL bDebug_Attach_Bev_Camera_To_Beverly			= FALSE
	BOOL bDebug_Attach_Bev_Flash_Unit_To_Camera			= FALSE
	BOOL bDebug_Attach_Bev_Camera_To_Player				= FALSE
	BOOL bDebug_Attach_Pap_Camera_To_Player				= FALSE
	VECTOR vRivalPapSpawnOffset							= << 3.0, -15.0, 0.0 >>
	FLOAT fDebug_TakePhotoZone_Radius					= 3.5
	BOOL bDebug_Test_Put_Ped_On_Back_Bike				= FALSE
	BOOL bDebug_TestCameraFlash							= FALSE
	VECTOR vDebug_FlashOffset							= << 0.00, 0.00, 0.00 >>
	VECTOR vDebug_FlashRotationOffset					= << 0.0, 0.0, 0.0 >>	
	BOOL bDebug_CreateRandomPed							= FALSE
	BOOL bDebug_UpdatePedHealth							= FALSE
	INT iDebug_PedHealth								= 1000
	BOOL bDebug_PrintPedHealth							= FALSE
	PED_INDEX pedDebug
	BOOL bDebug_DisplayPlayerVehicleSpeed				= FALSE	
	BOOL bDebug_CheckLengthOgVehicleRecordings			= FALSE
	BOOL bDebug_TeleportBeverlyOffBikeSafe				= FALSE
	BOOL bDebug_ReapplyCameraAnimOnBev					= FALSE	
	FLOAT fDebug_TimeScale								= 1.0
	BOOL bDebug_SetTimeScale							= FALSE	
	BOOL bDebugPrint_CelebOffset_Y_ToPlayer				= FALSE	
	BOOL bDebug_Test_AreHeadingsSimilar					= FALSE
	FLOAT fDebug_Test_AreHeadingsSimilar_LowerThreshold = 0.9
	FLOAT fDebug_Test_AreHeadingsSimilar_UpperThreshold = 1.1
	BOOL bDisplayHeadingCheckTTY						= FALSE	
	BOOL bDebug_BevDoAimCameraAtCelebTask				= FALSE									//NEEDS TO BE FALSE!!
	BOOL bDebug_ApplyBevWeaponMovementClipSet			= FALSE
	BOOL bDebug_ResetBevWeaponMovementClipSet			= FALSE
	BOOL bDebug_ForcePlayKOAnimOnBev					= FALSE
	INT iDebug_KOAnimToForcePlayOnBev					= 0
	FLOAT fDebug_KOAnimSpeedForcePlayOnBev				= 1.0
	BOOL bDebug_OverrideBevCameraLensOffset				= FALSE
	VECTOR vDebug_BevCameraLensOffsetOverride			= vBevCameraLensOffset
	BOOL bDebug_OverrideProbeHitDist					= FALSE
	FLOAT fDebug_ProbeHitDistOverride					= SHAPE_TEST_PAP1_CAMERA_AIM_PROBE_HIT_DIST			
	BOOL bDebug_OverrideBevAimCameraViewingCone			= FALSE
	FLOAT fDebug_BevAimCameraViewingCone				= SHAPE_TEST_PAP1_CAMERA_AIM_VIEW_CONE
	BOOL bDebug_DisplayShapeTestPap1CameraAim			= FALSE									//NEEDS TO BE FALSE!!	
	BOOL bDebug_OverrideCelebPosInCarOffset				= FALSE
	VECTOR vDebug_vCelebPosInCarOffsetOverride			= vCelebPosInCarOffset
	FLOAT fDebug_TaskSweepAim_TurnRate					= 3.0
	BOOL bDebug_OverrideTaskSweepAim_TurnRate			= FALSE
	VECTOR vDebug_TempPlayerVehicleSetPosition
	BOOL bDebug_ApplyPositionValueToPlayerVehicle		= FALSE	
	FLOAT fDebug_TempPlayerVehicleQuatX
	FLOAT fDebug_TempPlayerVehicleQuatY
	FLOAT fDebug_TempPlayerVehicleQuatZ
	FLOAT fDebug_TempPlayerVehicleQuatW
	BOOL bDebug_ApplyQuaternionValuesToPlayerVehicle	= FALSE	
	BOOL bDebug_SnapPlayerVehicleToGround				= FALSE
	BOOL bDebug_PrintPlayerVehicleInfo					= FALSE
	BOOL bDebug_DisplayInfo_GetSideCoordsIsToEntity		= FALSE
	BOOL bDebug_BevTempAimLeft							= FALSE
	BOOL bDebug_GetCurrentGameplayHeadingAndPitch		= FALSE
	BOOL bDebug_TakeOffHelmet							= FALSE
	BOOL bDebug_GiveHelmet								= FALSE
	/// PURPOSE:
	/// My debug mission widget groups, which get created in RAG->SCRIPT
	PROC SETUP_MISSION_WIDGET()
		widgetGroup = START_WIDGET_GROUP("PAPARAZZO 1 WIDGETS")
			START_WIDGET_GROUP("GENERAL MISSION STUFF")
				ADD_WIDGET_BOOL("TTY Toggle - Print Mission Debug Info", bDebug_PrintMissionInfoToTTY)	
				ADD_WIDGET_BOOL("TTY Toggle - Print Mission Rubberband only Debug Info", bDebug_PrintMissionRubberbandInfoToTTY)					
				ADD_WIDGET_BOOL("DEBUG - set mission in debug stage", bDebug_SetupMission_DebugStage)
				ADD_WIDGET_BOOL("Print Current Gameplay Cam Heading and Pitch", bDebug_GetCurrentGameplayHeadingAndPitch)
				ADD_WIDGET_BOOL("bDEbug_DisplayFranklinSpecialAbilityCharge", bDEbug_DisplayFranklinSpecialAbilityCharge)
				ADD_WIDGET_BOOL("bUseNewCelebChaseRubberbanding", bUseNewCelebChaseRubberbanding)				
				ADD_WIDGET_BOOL("bDebug_DisplayPlayerVehicleSpeed", bDebug_DisplayPlayerVehicleSpeed)
				ADD_WIDGET_BOOL("bDebug_Test_Put_Ped_On_Back_Bike", bDebug_Test_Put_Ped_On_Back_Bike)
				ADD_WIDGET_BOOL("bDebug_TakeOffHelmet", bDebug_TakeOffHelmet)	
				ADD_WIDGET_BOOL("bDebug_GiveHelmet", bDebug_GiveHelmet)	
				ADD_WIDGET_FLOAT_SLIDER("fDebug_CelebWanderSpeed", fDebug_CelebWanderSpeed, 0.0, 100.0, 0.1)
				ADD_WIDGET_BOOL("Reset hint camera", bDebug_ResetHintCam)
				ADD_WIDGET_BOOL("Display Rival Pap Spawn Area", bDebug_RivalPapSpawnArea)
				ADD_WIDGET_VECTOR_SLIDER("vRivalPapSpawnOffset", vRivalPapSpawnOffset, -7000.0, 7000.0, 0.1)				
				ADD_WIDGET_BOOL("Create debug ped", bDebug_CreateRandomPed)
				ADD_WIDGET_BOOL("Update debug peds health", bDebug_UpdatePedHealth)
				ADD_WIDGET_INT_SLIDER("set debug peds health", iDebug_PedHealth, 0, 1000, 1)
				ADD_WIDGET_BOOL("Print PedHealth", bDebug_PrintPedHealth)								
				ADD_WIDGET_BOOL("bDebug_CheckLengthOFVehicleRecordings", bDebug_CheckLengthOgVehicleRecordings)
				ADD_WIDGET_BOOL("bDebug_TeleportBeverlyOffBikeSafe", bDebug_TeleportBeverlyOffBikeSafe)				
				ADD_WIDGET_BOOL("reapply camera anim on beverly for test", bDebug_ReapplyCameraAnimOnBev)				
				ADD_WIDGET_FLOAT_SLIDER("Time Scale adjustment (1.0) is normal speed - ", fDebug_TimeScale, 0.0, 2.0, 0.01)
				ADD_WIDGET_BOOL("Apply Time Scale", bDebug_SetTimeScale)				
				ADD_WIDGET_BOOL("print to TTY celeb offset Y to player", bDebugPrint_CelebOffset_Y_ToPlayer)				
				ADD_WIDGET_BOOL("bDebug_Test_AreHeadingsSimilar - bev bike and rival bike", bDebug_Test_AreHeadingsSimilar)
				ADD_WIDGET_FLOAT_SLIDER("fDebug_Test_AreHeadingsSimilar_LowerThreshold - ", fDebug_Test_AreHeadingsSimilar_LowerThreshold, -360.0, 360.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fDebug_Test_AreHeadingsSimilar_UpperThreshold - ", fDebug_Test_AreHeadingsSimilar_UpperThreshold, -360.0, 360.0, 0.01)
				ADD_WIDGET_BOOL("bDisplayHeadingCheckTTY", bDisplayHeadingCheckTTY)			
				ADD_WIDGET_BOOL("Debug Display Info Get Side Coords Is To Entity", bDebug_DisplayInfo_GetSideCoordsIsToEntity)	
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("PHOTO AREA SETUP note: right side only, mirrored in script")
				ADD_WIDGET_BOOL("Display Photo Area", bDebug_DisplayPhotoArea)
				ADD_WIDGET_VECTOR_SLIDER("OFFSET CENTRE POINT", vTakePhotoZones_CentrePosOffset[0], -7000.0, 7000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("RADIUS", fDebug_TakePhotoZone_Radius, 0.0, 100.0, 0.1)				
			STOP_WIDGET_GROUP()			
			/*START_WIDGET_GROUP("KNOCK OFF BIKER AREA SETUP")
				START_WIDGET_GROUP("GENERAL")
					ADD_WIDGET_BOOL("Toggle pap drive wander", bDebug_TogglePapDriveWander)
					ADD_WIDGET_BOOL("Toggle knock off biker debug draws", bDebug_ToggleKnockoffBikerDebugDraws)
					ADD_WIDGET_BOOL("Reset KO'd bools ", bDebug_ResetKObools)
					ADD_WIDGET_BOOL("Don't knock the bikers off on successful hit", bDebug_DontKnockThePedsOffTheBike)	
					ADD_WIDGET_BOOL("Do Arm Intersection Test For Knockoff Bikers", bDebug_DoArmIntersectionTest)	
					ADD_WIDGET_FLOAT_SLIDER("Pelvis Range Override - ", fDebug_PelvisRadius, 0.0, 4.0, 0.0001)
					ADD_WIDGET_VECTOR_SLIDER("vDebug_PelvisPosOffsetDriver", vDebug_PelvisPosOffsetDriver, -7000.0, 7000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vDebug_PelvisPosOffsetPassenger", vDebug_PelvisPosOffsetPassenger, -7000.0, 7000.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Hand Range Override - ", fDebug_HandRadius, 0.0, 4.0, 0.0001)
					ADD_WIDGET_FLOAT_SLIDER("Arm Range Override - ", fDebug_ArmRadius, 0.0, 4.0, 0.0001)
					ADD_WIDGET_BOOL("Set Beverly to Play the a KO anim", bDebug_ForcePlayKOAnimOnBev)
					ADD_WIDGET_INT_SLIDER("Which KO anim for force on Bev", iDebug_KOAnimToForcePlayOnBev, 0, 3, 1)
					ADD_WIDGET_FLOAT_SLIDER("Anim speed of forced KO anim on Bev", fDebug_KOAnimSpeedForcePlayOnBev, 0.0, 10.0, 0.1)	
					ADD_WIDGET_FLOAT_SLIDER("KNOCK OFF BIKER HIT EXECUTION AREA - LENGTH", fKnockOff_Biker_HitExecutionArea_Length, 0.0, 100.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("KNOCK OFF BIKER HIT EXECUTION AREA - WidthModifier", fKnockOff_Biker_StartAnimWidthModifier, 0.0, 1000.0, 0.1)
				STOP_WIDGET_GROUP()				
				START_WIDGET_GROUP("LEFT SIDE")
					ADD_WIDGET_VECTOR_SLIDER("LEFT KNOCK OFF BIKER HIT EXECUTION AREA NEAR OFFSET", vKnockOff_Biker_HitExecutionArea_OffsetNear[KNOCKOFF_BIKER_ATTACK_SIDE_LEFT], -7000.0, 7000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("LEFT KNOCK OFF BIKER HIT EXECUTION AREA FAR OFFSET", vKnockOff_Biker_HitExecutionArea_OffsetFar[KNOCKOFF_BIKER_ATTACK_SIDE_LEFT], -7000.0, 7000.0, 0.1)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("RIGHT SIDE")
					ADD_WIDGET_VECTOR_SLIDER("RIGHT KNOCK OFF BIKER HIT EXECUTION AREA NEAR OFFSET", vKnockOff_Biker_HitExecutionArea_OffsetNear[KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT], -7000.0, 7000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("RIGHT KNOCK OFF BIKER HIT EXECUTION AREA FAR OFFSET", vKnockOff_Biker_HitExecutionArea_OffsetFar[KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT], -7000.0, 7000.0, 0.1)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("ANIM SETUP")
					ADD_WIDGET_BOOL("Override Anims For Setup", bDebug_OverrideKnockOffAnimsForSetup)
					ADD_WIDGET_BOOL("Force Punch (false = Force Swipe", bDebug_KnockOffAnims_ForcePunch)
					ADD_WIDGET_BOOL("Use following anim speed as override", bDebug_KnockOffAnims_OverrideAnimSpeed)
					ADD_WIDGET_FLOAT_SLIDER("Set KO anim speed - ", fDebug_KnockOff_Biker_AnimSpeed, -10.0, 10.0, 0.01)
					ADD_WIDGET_BOOL("Use following hit times as override", bDebug_KnockOffAnims_OverrideHitTimes)
					ADD_WIDGET_FLOAT_SLIDER("Set KO anim hit start point - ", fDebug_KnockOff_Biker_Anim_HitStartPoint, 0.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Set KO anim hit done point - ", fDebug_KnockOff_Biker_Anim_HitDonePoint, 0.0, 1.0, 0.001)
					ADD_WIDGET_BOOL("Use following attack sfx trigger point as override", bDebug_KnockOffAnims_OverrideSwipeSFXTime)
					ADD_WIDGET_FLOAT_SLIDER("Set KO anim attack sound fx trigger point - ", fDebug_KnockOff_Biker_Anim_SwipeSFXstartPoint, 0.0, 1.0, 0.001)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()*/		
			START_WIDGET_GROUP("OUTRO SETUP")
				ADD_WIDGET_VECTOR_SLIDER("Position Offset of Player from Beverly car", vDebug_PositionOffsetPlayerFromCar, -4000.0, 4000.0, 0.1) 
				ADD_WIDGET_BOOL("bDebug_RespotPlayerPos", bDebug_RespotPlayerPos)
				ADD_WIDGET_VECTOR_SLIDER("vDebug_CameraOffsetOutro", vDebug_CameraOffsetOutro, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vDebug_CameraPointAtOffset", vDebug_CameraPointAtOffset, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_BOOL("bDebug_ResetCameraOffsetOutro", bDebug_ResetCameraOffsetOutro)			
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("CAMERA ATTACHMENT")
				ADD_WIDGET_VECTOR_SLIDER("Attach offset of Bev camera", sObjCameraBeverly.vOffset, -4000.0, 4000.0, 0.1) 
				ADD_WIDGET_VECTOR_SLIDER("Attach Rotation of Bev camera", sObjCameraBeverly.vRotation, -4000.0, 4000.0, 0.1)
			 	ADD_WIDGET_BOOL("Re-attach Bev camera to player", bDebug_Attach_Bev_Camera_To_Player)
				ADD_WIDGET_BOOL("Re-attach Bev camera to Bev", bDebug_Attach_Bev_Camera_To_Beverly)				
				ADD_WIDGET_VECTOR_SLIDER("Attach offset of Pap Cam Check Photo Pose", sObjCameraRivalPap.vOffset, -4000.0, 4000.0, 0.1) 
				ADD_WIDGET_VECTOR_SLIDER("Attach Rotation of Pap cam Check Photo Pose", sObjCameraRivalPap.vRotation, -4000.0, 4000.0, 0.1)
			 	ADD_WIDGET_BOOL("Re-attach Pap camera to player", bDebug_Attach_Pap_Camera_To_Player)				
				ADD_WIDGET_VECTOR_SLIDER("Additional offset for flash", vDebug_FlashOffset, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Additional offset for flash Rotation", vDebug_FlashRotationOffset, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_BOOL("Test Camera Flash Position", bDebug_TestCameraFlash)
				ADD_WIDGET_VECTOR_SLIDER("Attach offset of Bev Flash Unit", sObjFlashUnitBeverly.vOffset, -4000.0, 4000.0, 0.1) 
				ADD_WIDGET_VECTOR_SLIDER("Attach Rotation of Bev flash unit", sObjFlashUnitBeverly.vRotation, -4000.0, 4000.0, 0.1)
			 	ADD_WIDGET_BOOL("Re-attach Bev Flash unit to camera", bDebug_Attach_Bev_Flash_Unit_To_Camera)								
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("NEW ANIM TESTS")					
				ADD_WIDGET_BOOL("bDebug_ApplyBevWeaponMovementClipSet - only works in debug state", bDebug_ApplyBevWeaponMovementClipSet)	
				ADD_WIDGET_BOOL("bDebug_ResetBevWeaponMovementClipSet - only works in debug state", bDebug_ResetBevWeaponMovementClipSet)	
			STOP_WIDGET_GROUP()			
			START_WIDGET_GROUP("BEV CAMERA AIM TESTS")		
				ADD_WIDGET_BOOL("Toggle - Set Beverly to do aim camera at celeb task - only works in debug state", bDebug_BevDoAimCameraAtCelebTask)
				ADD_WIDGET_BOOL("Toggle - Override Task Sweep Aim Turn Rate", bDebug_OverrideTaskSweepAim_TurnRate)
				ADD_WIDGET_FLOAT_SLIDER("fDebug_TaskSweepAim_TurnRate - ", fDebug_TaskSweepAim_TurnRate, 0.0, 100.0, 0.001)
				ADD_WIDGET_BOOL("Toggle - Display Debug lines shape test pap 1 camera aim", bDebug_DisplayShapeTestPap1CameraAim)	
				ADD_WIDGET_BOOL("Toggle - Override Bev Camera Lens Offset", bDebug_OverrideBevCameraLensOffset)	
				ADD_WIDGET_VECTOR_SLIDER("vDebug_BevCameraLensOffsetOverride - ", vDebug_BevCameraLensOffsetOverride, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_BOOL("Toggle - Override Probe Hit Dist", bDebug_OverrideProbeHitDist)	
				ADD_WIDGET_FLOAT_SLIDER("fDebug_ProbeHitDistOverride - ", fDebug_ProbeHitDistOverride, 0.0, 100.0, 0.001)
				ADD_WIDGET_BOOL("Toggle - Override Bev Aim Camera Viewing Cone", bDebug_OverrideBevAimCameraViewingCone)	
				ADD_WIDGET_FLOAT_SLIDER("fDebug_BevAimCameraViewingCone - ", fDebug_BevAimCameraViewingCone, 0.0, 180.0, 0.001)
				ADD_WIDGET_BOOL("Toggle - Override Override Celeb Pos In Car Offset", bDebug_OverrideCelebPosInCarOffset)	
				ADD_WIDGET_VECTOR_SLIDER("vDebug_vCelebPosInCarOffsetOverride - ", vDebug_vCelebPosInCarOffsetOverride, -4000.0, 4000.0, 0.1)
				ADD_WIDGET_BOOL("Debug Bev Temp Aim Left", bDebug_BevTempAimLeft)
			STOP_WIDGET_GROUP()	
			START_WIDGET_GROUP("Player vehicle positioning stuff")
				ADD_WIDGET_VECTOR_SLIDER("Set player vehicle position vector - ", vDebug_TempPlayerVehicleSetPosition, -10000, 10000, 0.0001)
				ADD_WIDGET_BOOL("Apply above position to player's vehicle", bDebug_ApplyPositionValueToPlayerVehicle)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion X value -", fDebug_TempPlayerVehicleQuatX, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion Y value -", fDebug_TempPlayerVehicleQuatY, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion Z value -", fDebug_TempPlayerVehicleQuatZ, -3.0, 3.0, 0.0001)
				ADD_WIDGET_FLOAT_SLIDER("Set player vehicle quaternion W value -", fDebug_TempPlayerVehicleQuatW, -3.0, 3.0, 0.0001)
				ADD_WIDGET_BOOL("Apply above quaternion to player's vehicle", bDebug_ApplyQuaternionValuesToPlayerVehicle)
				ADD_WIDGET_BOOL("Snap the player's current vehicle to the ground", bDebug_SnapPlayerVehicleToGround)
				ADD_WIDGET_BOOL("Print player's current vehicle info, pos, quat and heading", bDebug_PrintPlayerVehicleInfo)
			STOP_WIDGET_GROUP()	
			SET_UBER_PARENT_WIDGET_GROUP(widgetGroup)			
		STOP_WIDGET_GROUP()
		//set the initial values now that the values they are grabbing off have been set
		vDebug_BevCameraLensOffsetOverride			= vBevCameraLensOffset
		vDebug_vCelebPosInCarOffsetOverride			= vCelebPosInCarOffset
	ENDPROC
		
	/// PURPOSE:
	///    updates my mission widgets, based off RAG input
	PROC MAINTAIN_MISSION_WIDGETS()		
		TEXT_LABEL_63 tlTemp
		/* // debug fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_0))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRESSED KEY 0") ENDIF #ENDIF
	    	IF IS_PED_UNINJURED(sBeverlyPed.ped)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
					KNOCK_PED_OFF_VEHICLE(sBeverlyPed.ped)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - sBeverlyPed PED KNOCKED OFF THE BIKE") ENDIF #ENDIF
				ENDIF				
				SET_ENTITY_HEALTH(sBeverlyPed.ped, 0)
			ENDIF
	    ENDIF	 */	
		IF bDebug_GetCurrentGameplayHeadingAndPitch
			FLOAT fTemp_Heading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
			FLOAT fTemp_Pitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
			OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "Current Gameplay Cam Relative Heading : "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_FLOAT_TO_DEBUG_FILE(fTemp_Heading)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "Current Gameplay Cam Relative Pitch : "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_FLOAT_TO_DEBUG_FILE(fTemp_Pitch)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()		
			bDebug_GetCurrentGameplayHeadingAndPitch = FALSE
		ENDIF	
		IF bDEbug_DisplayFranklinSpecialAbilityCharge
			INT iTempStatValue
			STAT_GET_INT(SP1_SPECIAL_ABILITY, iTempStatValue)	// Franklin check Franklin has some charge in his bar
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "FRANKLIN SPEC ABILITY CHARGE IS  - ", iTempStatValue) ENDIF #ENDIF
		ENDIF			
		IF bDebug_DisplayPlayerVehicleSpeed
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehTempDebug_ForSpeedCheck = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehTempDebug_ForSpeedCheck)
				AND NOT IS_ENTITY_DEAD(vehTempDebug_ForSpeedCheck)
					FLOAT fTempDebug_SpeedCheck = GET_ENTITY_SPEED(vehTempDebug_ForSpeedCheck)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "%%%%%%	vehTempDebug_ForSpeedCheck	=	", fTempDebug_SpeedCheck) ENDIF #ENDIF
					IF fTempDebug_SpeedCheck < 34.0
						SET_TEXT_COLOUR(255, 216, 0, 255)	//yellow - speed slow						
					ELIF fTempDebug_SpeedCheck > 43.0
						SET_TEXT_COLOUR(255, 0, 0, 255)	//red - speeding
					ELSE						
						SET_TEXT_COLOUR(0, 255, 0, 255) //green - speed ok	
					ENDIF
					SET_TEXT_SCALE(0.5, 0.5)
					DISPLAY_TEXT_WITH_FLOAT(0.45, 0.83, "SPEED: ", fTempDebug_SpeedCheck, 2)
				ENDIF
			ENDIF
		ENDIF		
		IF NOT IS_ENTITY_DEAD(sCelebVehicle.vehicle)
		AND NOT IS_ENTITY_DEAD(sCelebPed.ped)		
			IF bDebug_Test_Put_Ped_On_Back_Bike
				SAFE_DELETE_PED(sRivalPapPed.ped)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX vehIndexTempPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(vehIndexTempPlayer)
					AND NOT DOES_ENTITY_EXIST(sRivalPapPed.ped)
						sRivalPapPed.ped = 	CREATE_PED_INSIDE_VEHICLE(vehIndexTempPlayer, PEDTYPE_MISSION, sRivalPapPed.model, VS_FRONT_RIGHT)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapPed.ped, TRUE)
						SET_PED_CAN_BE_TARGETTED(sRivalPapPed.ped, FALSE)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_NEVER)
						bDebug_Test_Put_Ped_On_Back_Bike = FALSE
					ENDIF
				ENDIF
			ENDIF			
			IF bDebug_UpdateCelebWanderSpeed
				IF NOT IS_ENTITY_DEAD(sCelebsDriverPed.ped)
					TASK_VEHICLE_DRIVE_WANDER(sCelebsDriverPed.ped, sCelebVehicle.vehicle, fDebug_CelebWanderSpeed, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS)
					bDebug_UpdateCelebWanderSpeed = FALSE
				ENDIF
			ENDIF			
			IF bDebug_DisplayPhotoArea
				SET_DEBUG_ACTIVE(TRUE)
    			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				vTakePhotoZones_CentrePos[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vTakePhotoZones_CentrePosOffset[0])
				DRAW_DEBUG_SPHERE(vTakePhotoZones_CentrePos[0], fDebug_TakePhotoZone_Radius, 0, 0, 255, 100)
				vTakePhotoZones_CentrePos[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vTakePhotoZones_CentrePosOffset[1])
				DRAW_DEBUG_SPHERE(vTakePhotoZones_CentrePos[1], fDebug_TakePhotoZone_Radius, 0, 0, 255, 100)
			ENDIF			
			IF NOT IS_ENTITY_DEAD(sBeverlyVehicle.vehicle)
			AND NOT IS_ENTITY_DEAD(sBeverlyPed.ped)
			AND NOT IS_ENTITY_DEAD(sRivalPapVehicle.vehicle)
			AND NOT IS_ENTITY_DEAD(sRivalPapPed.ped)
			AND NOT IS_ENTITY_DEAD(sRivalPapDriverPed.ped)
				IF bDebug_TogglePapDriveWander
					IF NOT (WAITING_TO_START_TASK = GET_SCRIPT_TASK_STATUS(sRivalPapDriverPed.ped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER))
					AND NOT (PERFORMING_TASK = GET_SCRIPT_TASK_STATUS(sRivalPapDriverPed.ped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER))
						FREEZE_ENTITY_POSITION(sRivalPapVehicle.vehicle, FALSE)
						TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, 26.0, DRIVINGMODE_AVOIDCARS)

						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "WANDER APPLIED TO RIVAL PAP") ENDIF #ENDIF
					ELSE
						CLEAR_PED_TASKS(sRivalPapDriverPed.ped)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "WANDER CLEARED FROM RIVAL PAP") ENDIF #ENDIF
					ENDIF
					bDebug_TogglePapDriveWander = FALSE
				ENDIF
				IF bDebug_ResetKObools					
					bDebug_ResetKObools = FALSE
				ENDIF	
				IF bDebug_Test_AreHeadingsSimilar
					VECTOR vTempBeverlyVehicle, vTempRivalPapVehicle
					FLOAT fDotProductVehicles
					BOOL bDebug_AreEntitesCompletelyOppositeWay
					vTempBeverlyVehicle = GET_ENTITY_FORWARD_VECTOR(sBeverlyVehicle.vehicle)
					vTempRivalPapVehicle = GET_ENTITY_FORWARD_VECTOR(sRivalPapVehicle.vehicle)
					fDotProductVehicles = DOT_PRODUCT(vTempBeverlyVehicle, vTempRivalPapVehicle)					
					IF fDotProductVehicles > 0	//facing same 180 heading
						bDebug_AreEntitesCompletelyOppositeWay = FALSE
					ELSE
						bDebug_AreEntitesCompletelyOppositeWay = TRUE
						fDotProductVehicles = ABSF(fDotProductVehicles)	//turn into a positive value
					ENDIF					
					IF (fDotProductVehicles > fDebug_Test_AreHeadingsSimilar_LowerThreshold
					AND fDotProductVehicles < fDebug_Test_AreHeadingsSimilar_UpperThreshold)
						IF NOT bDebug_AreEntitesCompletelyOppositeWay
							#IF IS_DEBUG_BUILD IF bDisplayHeadingCheckTTY CPRINTLN(DEBUG_MISSION, "DEBUG^^^  - COMPARE HEADINGS ARE SIMILAR BOTH FORWARDS >>>>>>>>>>>>>>>>  = ", fDotProductVehicles) ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDisplayHeadingCheckTTY  CPRINTLN(DEBUG_MISSION, "DEBUG^^^  - COMPARE HEADINGS ARE SIMILAR BOTH BACKWARDS <<<<<<<<<<<<<<<  = ", fDotProductVehicles) ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDisplayHeadingCheckTTY  CPRINTLN(DEBUG_MISSION, "DEBUG^^^  - COMPARE HEADINGS ARE NOT SIMILAR	-------  = ", fDotProductVehicles) ENDIF #ENDIF
					ENDIF
				ENDIF
				IF bDebug_ForcePlayKOAnimOnBev
					SWITCH iDebug_KOAnimToForcePlayOnBev
						CASE 0
							sAnimNameBeverlyKnockOff = "atk_punch_right_pap"
						BREAK
						CASE 1
							sAnimNameBeverlyKnockOff = "atk_swipe_right_pap"
						BREAK
						CASE 2
							sAnimNameBeverlyKnockOff = "atk_punch_left_pap"
						BREAK
						CASE 3
							sAnimNameBeverlyKnockOff = "atk_swipe_left_pap"
						BREAK
					ENDSWITCH
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
						CLEAR_PED_TASKS(sBeverlyPed.ped)
						TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_HOLD_LAST_FRAME)					
					ELSE	
						FLOAT fCurrentAnimPhase
						FLOAT fAnimTotalTime
						IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
							fAnimTotalTime = GET_ENTITY_ANIM_TOTAL_TIME(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	DEBUG BEV KNOCKOFF_BIKER_ATTACK fAnimTotalTime at finished = ", fAnimTotalTime) ENDIF #ENDIF	
							CLEAR_PED_TASKS(sBeverlyPed.ped)
							bDebug_ForcePlayKOAnimOnBev = FALSE							
						ELSE							
							fCurrentAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)							
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	DEBUG BEV KNOCKOFF_BIKER_ATTACK fCurrentAnimPhase = ", fCurrentAnimPhase) ENDIF #ENDIF							
							fAnimTotalTime = GET_ENTITY_ANIM_TOTAL_TIME(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	DEBUG BEV KNOCKOFF_BIKER_ATTACK fAnimTotalTime = ", fAnimTotalTime) ENDIF #ENDIF							
							SET_ENTITY_ANIM_SPEED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, fDebug_KOAnimSpeedForcePlayOnBev)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	DEBUG BEV KNOCKOFF_BIKER_ATTACK	- FORCED knock off anim speed set  +++") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bDebug_RivalPapSpawnArea
				VECTOR vTempRivalPapSpawnArea
				vTempRivalPapSpawnArea = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vRivalPapSpawnOffset)
				DRAW_DEBUG_SPHERE(vTempRivalPapSpawnArea, 2.0, 0, 255, 0, 100)
			ENDIF			
			IF bDebug_RespotPlayerPos
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					vDebug_PositionPlayerFromCar = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), GET_ENTITY_HEADING(sBeverlyVehicle.vehicle), vDebug_PositionOffsetPlayerFromCar)
					GET_GROUND_Z_FOR_3D_COORD(vDebug_PositionPlayerFromCar, vDebug_PositionPlayerFromCar.Z)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vDebug_PositionPlayerFromCar)
					IF NOT IS_PED_INJURED(sBeverlyPed.ped)
						TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped)
					ENDIF				
					bDebug_RespotPlayerPos = FALSE
				ENDIF
			ENDIF			
			IF bDebug_ResetCameraOffsetOutro
				VECTOR vDebug_CameraPosition = << 0.0, 0.0, 0.0 >>
				IF DOES_ENTITY_EXIST(sBeverlyVehicle.vehicle)
					//vDebug_CameraPosition = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), GET_ENTITY_HEADING(sBeverlyVehicle.vehicle), vDebug_CameraOffsetOutro)			
					IF NOT DOES_CAM_EXIST(camDebugPositioning)
						camDebugPositioning = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)						
						vDebug_CameraPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, vDebug_CameraOffsetOutro)
						SET_CAM_COORD(camDebugPositioning, vDebug_CameraPosition)						
						vDebug_CameraPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, vDebug_CameraPointAtOffset)
						POINT_CAM_AT_COORD(camDebugPositioning, vDebug_CameraPosition)						
						//ATTACH_CAM_TO_ENTITY(camDebugPositioning, sBeverlyPed.ped, vDebug_CameraOffsetOutro)
						//POINT_CAM_AT_ENTITY(camDebugPositioning, sBeverlyPed.ped, vDebug_CameraPointAtOffset)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SET_CAM_ACTIVE(camDebugPositioning, TRUE)
					ELSE
						//ATTACH_CAM_TO_ENTITY(camDebugPositioning, sBeverlyVehicle.vehicle, vDebug_CameraOffsetOutro)
						//POINT_CAM_AT_ENTITY(camDebugPositioning, sBeverlyPed.ped, vDebug_CameraPointAtOffset)
						vDebug_CameraPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, vDebug_CameraOffsetOutro)
						SET_CAM_COORD(camDebugPositioning, vDebug_CameraPosition)						
						vDebug_CameraPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, vDebug_CameraPointAtOffset)
						POINT_CAM_AT_COORD(camDebugPositioning, vDebug_CameraPosition)
					ENDIF
					bDebug_ResetCameraOffsetOutro = FALSE
				ENDIF
			ENDIF	
			IF bDebug_Attach_Bev_Flash_Unit_To_Camera
				IF IS_ENTITY_ALIVE(sObjCameraBeverly.object)
					IF NOT DOES_ENTITY_EXIST(sObjFlashUnitBeverly.object)
						sObjFlashUnitBeverly.object = CREATE_OBJECT(sObjFlashUnitBeverly.model, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sObjCameraBeverly.object, << 0.0, 0.0, 0.5 >>))
					ENDIF
					IF NOT IS_ENTITY_DEAD(sObjFlashUnitBeverly.object)
						IF IS_ENTITY_ATTACHED(sObjFlashUnitBeverly.object)
							DETACH_ENTITY(sObjFlashUnitBeverly.object)
						ENDIF
						ATTACH_ENTITY_TO_ENTITY(sObjFlashUnitBeverly.object, sObjCameraBeverly.object, 0, sObjFlashUnitBeverly.vOffset, sObjFlashUnitBeverly.vRotation)
						bDebug_Attach_Bev_Flash_Unit_To_Camera = FALSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_Attach_Bev_Flash_Unit_To_Camera - DONE") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bDebug_Attach_Bev_Camera_To_Beverly
				IF DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
				AND NOT IS_ENTITY_DEAD(sObjCameraRivalPap.object)
					IF IS_ENTITY_ATTACHED(sObjCameraRivalPap.object)
						DETACH_ENTITY(sObjCameraRivalPap.object)
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					sObjCameraBeverly.object = CREATE_OBJECT(sObjCameraBeverly.model, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sBeverlyPed.ped, << 0.0, 0.0, 2.5 >>))
				ENDIF
				IF NOT IS_ENTITY_DEAD(sObjCameraBeverly.object)
					IF IS_ENTITY_ATTACHED(sObjCameraBeverly.object)
						DETACH_ENTITY(sObjCameraBeverly.object)
					ENDIF
				ENDIF
				ATTACH_ENTITY_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped, GET_PED_BONE_INDEX(sBeverlyPed.ped, BONETAG_PH_R_HAND), sObjCameraBeverly.vOffset, sObjCameraBeverly.vRotation)
				bDebug_Attach_Bev_Camera_To_Beverly = FALSE
			ELIF bDebug_Attach_Bev_Camera_To_Player
				IF DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
				AND NOT IS_ENTITY_DEAD(sObjCameraRivalPap.object)
					IF IS_ENTITY_ATTACHED(sObjCameraRivalPap.object)
						DETACH_ENTITY(sObjCameraRivalPap.object)
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					sObjCameraBeverly.object = CREATE_OBJECT(sObjCameraBeverly.model, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 0.0, 2.5 >>))
				ENDIF
				IF NOT IS_ENTITY_DEAD(sObjCameraBeverly.object)
					IF IS_ENTITY_ATTACHED(sObjCameraBeverly.object)
						DETACH_ENTITY(sObjCameraBeverly.object)
					ENDIF
				ENDIF
				ATTACH_ENTITY_TO_ENTITY(sObjCameraBeverly.object, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), sObjCameraBeverly.vOffset, sObjCameraBeverly.vRotation)
				bDebug_Attach_Bev_Camera_To_Player = FALSE				
			ELIF bDebug_Attach_Pap_Camera_To_Player
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
				AND NOT IS_ENTITY_DEAD(sObjCameraBeverly.object)
					IF IS_ENTITY_ATTACHED(sObjCameraBeverly.object)
						DETACH_ENTITY(sObjCameraBeverly.object)
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
					sObjCameraRivalPap.object = CREATE_OBJECT(sObjCameraRivalPap.model, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 0.0, 2.5 >>))
				ENDIF
				IF NOT IS_ENTITY_DEAD(sObjCameraRivalPap.object)
					IF IS_ENTITY_ATTACHED(sObjCameraRivalPap.object)
						DETACH_ENTITY(sObjCameraRivalPap.object)
					ENDIF
				ENDIF
				ATTACH_ENTITY_TO_ENTITY(sObjCameraRivalPap.object, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), sObjCameraRivalPap.vOffset, sObjCameraRivalPap.vRotation)			
				bDebug_Attach_Pap_Camera_To_Player = FALSE
			ENDIF			
			IF bDebug_TestCameraFlash		
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					//START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_rcpap1_camera", GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_R_HAND, vDebug_FlashOffset), (GET_ENTITY_ROTATION(sObjCameraBeverly.object) + vDebug_FlashRotationOffset))
					IF IS_ENTITY_ALIVE(sObjFlashUnitBeverly.object)
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", sObjFlashUnitBeverly.object, vDebug_FlashOffset, vDebug_FlashRotationOffset)
					ENDIF
					IF NOT HAS_SOUND_FINISHED(iSoundId_CameraFlash)
						STOP_SOUND(iSoundId_CameraFlash)
					ENDIF 
					IF REQUEST_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)
						PLAY_SOUND_FROM_COORD(iSoundId_CameraFlash, "CAMERA", GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_R_HAND, vDebug_FlashOffset), sPap1_SoundSetName)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- KBAS_RUNNING_ATTACK -  SWIPE sfx triggered  +++") ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- KBAS_RUNNING_ATTACK -  SWIPE sfx not triggered, audio bank not loaded!?  +++") ENDIF #ENDIF
					ENDIF
				ENDIF
				bDebug_TestCameraFlash = FALSE
			ENDIF			
			IF bDebug_CreateRandomPed
				SAFE_DELETE_PED(pedDebug)
				PED_INDEX pedIndexPlayer = PLAYER_PED_ID()
				REQUEST_MODEL(sRivalPapPed.model)
				WHILE NOT HAS_MODEL_LOADED(sRivalPapPed.model)
					WAIT(0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOADING DEBUG PED MODEL ASSET") ENDIF #ENDIF
				ENDWHILE
				iDebug_PedHealth = 1000
				pedDebug = CREATE_PED(PEDTYPE_MISSION, sRivalPapPed.model, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedIndexPlayer, << 3.0, 1.0, 0.0 >>))
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDebug, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapPed.model)
				bDebug_CreateRandomPed = FALSE
			ENDIF
			IF bDebug_UpdatePedHealth
				IF DOES_ENTITY_EXIST(pedDebug)
					SET_ENTITY_HEALTH(pedDebug, iDebug_PedHealth)
				ENDIF
				bDebug_UpdatePedHealth = FALSE
			ENDIF		
			
			IF bDebug_PrintPedHealth
				IF DOES_ENTITY_EXIST(pedDebug)
					INT iTempPedHealth = GET_ENTITY_HEALTH(pedDebug)
					OPEN_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Debug Ped's Health is - "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_INT_TO_DEBUG_FILE(iTempPedHealth)
						SAVE_NEWLINE_TO_DEBUG_FILE()						
						IF IS_PED_INJURED(pedDebug)
							tlTemp = "Debug Ped is classed as INJURED"
						ELSE
							tlTemp = "Debug Ped is NOT classed as injured"
						ENDIF
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_NEWLINE_TO_DEBUG_FILE()
					CLOSE_DEBUG_FILE()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Debug peds health is - ", iTempPedHealth) ENDIF #ENDIF
					bDebug_PrintPedHealth = FALSE
				ENDIF
			ENDIF
		ENDIF
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehDebug = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(vehDebug)
				IF bDebug_ApplyPositionValueToPlayerVehicle
					SET_ENTITY_COORDS_NO_OFFSET(vehDebug, vDebug_TempPlayerVehicleSetPosition)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - UPDATED PLAYER VEHICLE POS") ENDIF #ENDIF
					bDebug_ApplyPositionValueToPlayerVehicle = FALSE
				ENDIF				
				IF bDebug_ApplyQuaternionValuesToPlayerVehicle
					SET_ENTITY_QUATERNION(vehDebug, fDebug_TempPlayerVehicleQuatX, fDebug_TempPlayerVehicleQuatY, fDebug_TempPlayerVehicleQuatZ, fDebug_TempPlayerVehicleQuatW)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - UPDATED PLAYER VEHICLE QUATERNION") ENDIF #ENDIF
					bDebug_ApplyQuaternionValuesToPlayerVehicle = FALSE
				ENDIF				
				IF bDebug_SnapPlayerVehicleToGround
					SET_VEHICLE_ON_GROUND_PROPERLY(vehDebug)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - PLAYER VEHICLE SET ON GROUND PROPERLY") ENDIF #ENDIF
					bDebug_SnapPlayerVehicleToGround = FALSE
				ENDIF			
				IF bDebug_PrintPlayerVehicleInfo
					OPEN_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "#Vehicle Position = "
						VECTOR vDebug_VehiclePos = GET_ENTITY_COORDS(vehDebug)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_VECTOR_TO_DEBUG_FILE(vDebug_VehiclePos)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						FLOAT fDebug_VehicleQuaternion_X, fDebug_VehicleQuaternion_Y, fDebug_VehicleQuaternion_Z, fDebug_VehicleQuaternion_W
						GET_ENTITY_QUATERNION(vehDebug, fDebug_VehicleQuaternion_X, fDebug_VehicleQuaternion_Y, fDebug_VehicleQuaternion_Z, fDebug_VehicleQuaternion_W)						
						tlTemp = "Vehicle Quaternion X = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_X)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion Y = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_Y)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion Z = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_Z)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						tlTemp = "Vehicle Quaternion W = "
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleQuaternion_W)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						
						tlTemp = "Vehicle Heading = "
						FLOAT fDebug_VehicleHeading = GET_ENTITY_HEADING(vehDebug)
						SAVE_STRING_TO_DEBUG_FILE(tlTemp)
						SAVE_FLOAT_TO_DEBUG_FILE(fDebug_VehicleHeading)
						SAVE_NEWLINE_TO_DEBUG_FILE()						
					CLOSE_DEBUG_FILE()					
					bDebug_PrintPlayerVehicleInfo = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG AZ WIDGETS - bDebug_PrintPlayerVehicleInfo done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF			
		ENDIF	
		IF bDebug_CheckLengthOgVehicleRecordings
			REQUEST_VEHICLE_RECORDING(36, sPap1_UberRecordingName)
			REQUEST_VEHICLE_RECORDING(37, sPap1_UberRecordingName)
			REQUEST_VEHICLE_RECORDING(38, sPap1_UberRecordingName)
			REQUEST_VEHICLE_RECORDING(42, sPap1_UberRecordingName)
			REQUEST_VEHICLE_RECORDING(43, sPap1_UberRecordingName)
			REQUEST_VEHICLE_RECORDING(46, sPap1_UberRecordingName)			
			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(36, sPap1_UberRecordingName)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(37, sPap1_UberRecordingName)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(38, sPap1_UberRecordingName)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(42, sPap1_UberRecordingName)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(43, sPap1_UberRecordingName)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(46, sPap1_UberRecordingName)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOADING RECORDING FOR DEBUG CHECK") ENDIF #ENDIF
				WAIT(0)
			ENDWHILE			
			FLOAT fTime
			OPEN_DEBUG_FILE()				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				tlTemp = "debug info : total duration of vehicle recordings - "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[34] recording 36 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(36, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[35] recording 37 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(37, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[36] recording 38 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(38, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[40] recording 42 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(42, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[41] recording 43 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(43, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlTemp = "[44] recording 46 = "
				SAVE_STRING_TO_DEBUG_FILE(tlTemp)
				fTime = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(46, sPap1_UberRecordingName)
				SAVE_FLOAT_TO_DEBUG_FILE(fTime)
				SAVE_NEWLINE_TO_DEBUG_FILE()				
			CLOSE_DEBUG_FILE()			
			bDebug_CheckLengthOgVehicleRecordings = FALSE
		ENDIF		
		IF bDebug_TeleportBeverlyOffBikeSafe
			IF NOT IS_ENTITY_DEAD(sBeverlyPed.ped)
				VECTOR vSafePos
				WHILE NOT GET_SAFE_COORD_FOR_PED(GET_ENTITY_COORDS(sBeverlyPed.ped), TRUE, vSafePos)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "finding safe location for respot bev") ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
				SET_ENTITY_COORDS(sBeverlyPed.ped, vSafePos, FALSE)
				bDebug_TeleportBeverlyOffBikeSafe = FALSE
			ENDIF
		ENDIF				
		IF bDebug_SetTimeScale
			SET_TIME_SCALE(fDebug_TimeScale)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG RAG - APPLIED TIME SCALE - ", fDebug_TimeScale) ENDIF #ENDIF
			bDebug_SetTimeScale = FALSE
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    ovetride the knock off biker anim setup
	PROC DEBUG_OVERRIDE_KNOCK_OFF_BIKER_ANIM_SETUP()
		IF bDebug_OverrideKnockOffAnimsForSetup
			IF bDebug_KnockOffAnims_ForcePunch
				IF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
					sAnimNameBeverlyKnockOff = "atk_punch_left_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa
				ELIF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_LEFT
					sAnimNameBeverlyKnockOff = "atk_punch_right_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa	
				ENDIF
			ELSE
				IF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
					sAnimNameBeverlyKnockOff = "atk_swipe_left_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa	
				ELIF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_LEFT
					sAnimNameBeverlyKnockOff = "atk_swipe_right_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa
				ENDIF												
			ENDIF
		ENDIF
		IF bDebug_KnockOffAnims_OverrideAnimSpeed
			fKnockOff_Biker_UpdatedAnimSpeed = fDebug_KnockOff_Biker_AnimSpeed
		ENDIF
		IF bDebug_KnockOffAnims_OverrideHitTimes
			fKnockOff_Biker_Anim_HitStartPoint = fDebug_KnockOff_Biker_Anim_HitStartPoint
			fKnockOff_Biker_Anim_HitDonePoint = fDebug_KnockOff_Biker_Anim_HitDonePoint
		ENDIF
		IF bDebug_KnockOffAnims_OverrideSwipeSFXTime
			fKnockOff_Biker_Anim_SwipeSFXstartPoint = fDebug_KnockOff_Biker_Anim_SwipeSFXstartPoint	
		ENDIF					
	ENDPROC
	
	/// PURPOSE:
	/// 	removes my debug mission widget group
	PROC CLEANUP_MISSION_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	ENDPROC	
#ENDIF

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:FUNCTIONS AND PROCEEDURES
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    contains all the data needed for the uber recording
PROC LOAD_UBER_DATA()

// ****  UBER RECORDED TRAFFIC  **** 

TrafficCarPos[0] = <<-269.2346, 272.0872, 90.3265>>
TrafficCarQuatX[0] = -0.0287
TrafficCarQuatY[0] = -0.0274
TrafficCarQuatZ[0] = 0.7214
TrafficCarQuatW[0] = 0.6914
TrafficCarRecording[0] = 3
TrafficCarStartime[0] = 0.0000
TrafficCarModel[0] = Baller

TrafficCarPos[1] = <<-226.7559, 292.0566, 92.0173>>
TrafficCarQuatX[1] = 0.0000
TrafficCarQuatY[1] = 0.0000
TrafficCarQuatZ[1] = 0.9999
TrafficCarQuatW[1] = 0.0119
TrafficCarRecording[1] = 2
TrafficCarStartime[1] = 500.0000
TrafficCarModel[1] = Baller

TrafficCarPos[2] = <<-318.0732, 258.0473, 86.6976>>
TrafficCarQuatX[2] = -0.0322
TrafficCarQuatY[2] = -0.0153
TrafficCarQuatZ[2] = 0.8131
TrafficCarQuatW[2] = 0.5810
TrafficCarRecording[2] = 4
TrafficCarStartime[2] = 500.0000
TrafficCarModel[2] = ninef2

TrafficCarPos[3] = <<-382.6178, 231.0619, 83.6473>>
TrafficCarQuatX[3] = 0.0066
TrafficCarQuatY[3] = -0.0080
TrafficCarQuatZ[3] = -0.6558
TrafficCarQuatW[3] = 0.7548
TrafficCarRecording[3] = 6
TrafficCarStartime[3] = 6072.0000
TrafficCarModel[3] = jackal

//!!! Pos and quaternion will need update as vehicle model had to change, BUFFALO removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
TrafficCarPos[4] = <<-472.6657, 252.0544, 82.8362>>
TrafficCarQuatX[4] = 0.0018
TrafficCarQuatY[4] = 0.0015
TrafficCarQuatZ[4] = 0.6633
TrafficCarQuatW[4] = 0.7483
TrafficCarRecording[4] = 7
TrafficCarStartime[4] = 9108.0000
TrafficCarModel[4] = buffalo

TrafficCarPos[5] = <<-538.8723, 199.7826, 75.3031>>
TrafficCarQuatX[5] = 0.1322
TrafficCarQuatY[5] = 0.0002
TrafficCarQuatZ[5] = -0.0192
TrafficCarQuatW[5] = 0.9910
TrafficCarRecording[5] = 10
TrafficCarStartime[5] = 11088.0000
TrafficCarModel[5] = tornado

TrafficCarPos[6] = <<-539.8965, 255.4054, 82.4098>>
TrafficCarQuatX[6] = -0.0007
TrafficCarQuatY[6] = -0.0014
TrafficCarQuatZ[6] = 0.9581
TrafficCarQuatW[6] = 0.2865
TrafficCarRecording[6] = 9
TrafficCarStartime[6] = 12750.0000
TrafficCarModel[6] = sentinel

TrafficCarPos[7] = <<-539.5698, 280.9452, 82.5083>>
TrafficCarQuatX[7] = -0.0001
TrafficCarQuatY[7] = -0.0001
TrafficCarQuatZ[7] = 0.9992
TrafficCarQuatW[7] = 0.0409
TrafficCarRecording[7] = 11
TrafficCarStartime[7] = 12850.0000
TrafficCarModel[7] = serrano

TrafficCarPos[8] = <<-633.0772, 271.1177, 81.5183>>
TrafficCarQuatX[8] = -0.0052
TrafficCarQuatY[8] = -0.0044
TrafficCarQuatZ[8] = 0.6509
TrafficCarQuatW[8] = 0.7591
TrafficCarRecording[8] = 13
TrafficCarStartime[8] = 13900.0000
TrafficCarModel[8] = Baller

//edit this one
TrafficCarPos[9] = <<-613.1815, 267.2222, 81.5335>>
TrafficCarQuatX[9] = -0.0084
TrafficCarQuatY[9] = -0.0079
TrafficCarQuatZ[9] = 0.5968
TrafficCarQuatW[9] = 0.8023
TrafficCarRecording[9] = 12
TrafficCarStartime[9] = 14000.0000
TrafficCarModel[9] = tornado

TrafficCarPos[10] = <<-667.2612, 252.6343, 81.1762>>
TrafficCarQuatX[10] = 0.0074
TrafficCarQuatY[10] = -0.0049
TrafficCarQuatZ[10] = -0.5581
TrafficCarQuatW[10] = 0.8298
TrafficCarRecording[10] = 14
TrafficCarStartime[10] = 15048.0000
TrafficCarModel[10] = tornado

TrafficCarPos[11] = <<-775.2990, 210.8092, 75.4151>>
TrafficCarQuatX[11] = -0.0047
TrafficCarQuatY[11] = 0.0035
TrafficCarQuatZ[11] = -0.6623
TrafficCarQuatW[11] = 0.7492
TrafficCarRecording[11] = 16
TrafficCarStartime[11] = 18810.0000
TrafficCarModel[11] = serrano

//!!! Pos and quaternion will need update as vehicle model had to change, BUFFALO removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
TrafficCarPos[12] = <<-774.7975, 206.4167, 75.4047>>
TrafficCarQuatX[12] = -0.0066
TrafficCarQuatY[12] = 0.0056
TrafficCarQuatZ[12] = -0.6593
TrafficCarQuatW[12] = 0.7518
TrafficCarRecording[12] = 17
TrafficCarStartime[12] = 18876.0000
TrafficCarModel[12] = buffalo

TrafficCarPos[13] = <<-745.6793, 217.5777, 75.4614>>
TrafficCarQuatX[13] = -0.0008
TrafficCarQuatY[13] = 0.0004
TrafficCarQuatZ[13] = -0.5800
TrafficCarQuatW[13] = 0.8146
TrafficCarRecording[13] = 18
TrafficCarStartime[13] = 19074.0000
TrafficCarModel[13] = tornado

TrafficCarPos[14] = <<-805.5899, 219.7039, 75.2721>>
TrafficCarQuatX[14] = -0.0093
TrafficCarQuatY[14] = -0.0083
TrafficCarQuatZ[14] = 0.6901
TrafficCarQuatW[14] = 0.7236
TrafficCarRecording[14] = 19
TrafficCarStartime[14] = 20000.0000
TrafficCarModel[14] = serrano

TrafficCarPos[15] = <<-839.7690, 218.3051, 73.4582>>
TrafficCarQuatX[15] = -0.0132
TrafficCarQuatY[15] = -0.0118
TrafficCarQuatZ[15] = 0.6663
TrafficCarQuatW[15] = 0.7455
TrafficCarRecording[15] = 20
TrafficCarStartime[15] = 20460.0000
TrafficCarModel[15] = emperor

TrafficCarPos[16] = <<-856.8807, 196.2786, 72.6080>>
TrafficCarQuatX[16] = 0.1009
TrafficCarQuatY[16] = -0.0020
TrafficCarQuatZ[16] = -0.0453
TrafficCarQuatW[16] = 0.9939
TrafficCarRecording[16] = 21
TrafficCarStartime[16] = 21400.0000
TrafficCarModel[16] = tornado

TrafficCarPos[17] = <<-858.5552, 173.0333, 67.9306>>
TrafficCarQuatX[17] = 0.1077
TrafficCarQuatY[17] = 0.0013
TrafficCarQuatZ[17] = -0.0262
TrafficCarQuatW[17] = 0.9938
TrafficCarRecording[17] = 22
TrafficCarStartime[17] = 21648.0000
TrafficCarModel[17] = tornado

// will need quat values correcting!!!
TrafficCarPos[18] = <<-867.5400, 233.6300, 73.3800>>
TrafficCarQuatX[18] = 0.0029
TrafficCarQuatY[18] = 0.0017
TrafficCarQuatZ[18] = 0.7365
TrafficCarQuatW[18] = 0.6764
TrafficCarRecording[18] = 15
TrafficCarStartime[18] = 22000.0000
TrafficCarModel[18] = jackal

TrafficCarPos[19] = <<-885.0156, 216.7224, 72.9075>>
TrafficCarQuatX[19] = -0.0254
TrafficCarQuatY[19] = 0.0404
TrafficCarQuatZ[19] = 0.8137
TrafficCarQuatW[19] = -0.5793
TrafficCarRecording[19] = 24
TrafficCarStartime[19] = 22242.0000
TrafficCarModel[19] = tornado

TrafficCarPos[20] = <<-935.3688, 242.8499, 69.7386>>
TrafficCarQuatX[20] = -0.0029
TrafficCarQuatY[20] = 0.0039
TrafficCarQuatZ[20] = 0.8121
TrafficCarQuatW[20] = -0.5835
TrafficCarRecording[20] = 25
TrafficCarStartime[20] = 23562.0000
TrafficCarModel[20] = serrano

TrafficCarPos[21] = <<-1106.0233, 267.2069, 63.9306>>
TrafficCarQuatX[21] = 0.0322
TrafficCarQuatY[21] = -0.0271
TrafficCarQuatZ[21] = 0.7282
TrafficCarQuatW[21] = -0.6841
TrafficCarRecording[21] = 26
TrafficCarStartime[21] = 29367.0000
TrafficCarModel[21] = f620

TrafficCarPos[22] = <<-1146.9456, 244.4222, 66.2596>>
TrafficCarQuatX[22] = -0.0129
TrafficCarQuatY[22] = 0.0093
TrafficCarQuatZ[22] = -0.5717
TrafficCarQuatW[22] = 0.8203
TrafficCarRecording[22] = 27
TrafficCarStartime[22] = 31149.0000
TrafficCarModel[22] = ninef2

//comes out of junction on widing section
TrafficCarPos[23] = <<-1146.5225, 280.7030, 66.1987>>
TrafficCarQuatX[23] = 0.0065
TrafficCarQuatY[23] = -0.0253
TrafficCarQuatZ[23] = 0.9996
TrafficCarQuatW[23] = -0.0069
TrafficCarRecording[23] = 5
TrafficCarStartime[23] = 31553.0000
TrafficCarModel[23] = tornado

TrafficCarPos[24] = <<-1184.5457, 240.2789, 67.3019>>
TrafficCarQuatX[24] = 0.0027
TrafficCarQuatY[24] = -0.0023
TrafficCarQuatZ[24] = -0.6313
TrafficCarQuatW[24] = 0.7755
TrafficCarRecording[24] = 28
TrafficCarStartime[24] = 33000.0000
TrafficCarModel[24] = RapidGT2

TrafficCarPos[25] = <<-1233.5752, 227.3869, 64.6991>>
TrafficCarQuatX[25] = 0.0322
TrafficCarQuatY[25] = -0.0193
TrafficCarQuatZ[25] = -0.6721
TrafficCarQuatW[25] = 0.7395
TrafficCarRecording[25] = 29
TrafficCarStartime[25] = 34317.0000
TrafficCarModel[25] = RapidGT2

// replacement on quiet widing road
TrafficCarPos[26] = <<-1384.7358, 225.2400, 58.3821>>
TrafficCarQuatX[26] = 0.0071
TrafficCarQuatY[26] = -0.0076
TrafficCarQuatZ[26] = 0.9563
TrafficCarQuatW[26] = 0.2922
TrafficCarRecording[26] = 8
TrafficCarStartime[26] = 40977.0000
TrafficCarModel[26] = RapidGT2

TrafficCarPos[27] = <<-1428.7577, 129.0681, 52.6718>>
TrafficCarQuatX[27] = 0.0301
TrafficCarQuatY[27] = -0.0057
TrafficCarQuatZ[27] = -0.1124
TrafficCarQuatW[27] = 0.9932
TrafficCarRecording[27] = 37
TrafficCarStartime[27] = 44235.0000
TrafficCarModel[27] = jackal

TrafficCarPos[28] = <<-1415.9232, -38.1466, 52.6835>>
TrafficCarQuatX[28] = -0.0344
TrafficCarQuatY[28] = -0.0023
TrafficCarQuatZ[28] = 0.1209
TrafficCarQuatW[28] = 0.9921
TrafficCarRecording[28] = 38
TrafficCarStartime[28] = 46875.0000
TrafficCarModel[28] = jackal

TrafficCarPos[29] = <<-1461.8724, -1.2539, 53.1150>>
TrafficCarQuatX[29] = -0.0285
TrafficCarQuatY[29] = 0.0263
TrafficCarQuatZ[29] = -0.6578
TrafficCarQuatW[29] = 0.7522
TrafficCarRecording[29] = 30
TrafficCarStartime[29] = 48436.0000
TrafficCarModel[29] = f620

TrafficCarPos[30] = <<-1483.0806, -3.5968, 54.8590>>
TrafficCarQuatX[30] = -0.0293
TrafficCarQuatY[30] = 0.0225
TrafficCarQuatZ[30] = -0.6524
TrafficCarQuatW[30] = 0.7570
TrafficCarRecording[30] = 32
TrafficCarStartime[30] = 48700.0000
TrafficCarModel[30] = f620

TrafficCarPos[31] = <<-1414.1544, -55.0295, 52.6736>>
TrafficCarQuatX[31] = -0.0002
TrafficCarQuatY[31] = 0.0041
TrafficCarQuatZ[31] = 0.9726
TrafficCarQuatW[31] = -0.2324
TrafficCarRecording[31] = 34
TrafficCarStartime[31] = 50284.0000
TrafficCarModel[31] = RapidGT2

TrafficCarPos[32] = <<-1396.2426, -76.9218, 52.4577>>
TrafficCarQuatX[32] = 0.0020
TrafficCarQuatY[32] = 0.0013
TrafficCarQuatZ[32] = -0.3998
TrafficCarQuatW[32] = 0.9166
TrafficCarRecording[32] = 35
TrafficCarStartime[32] = 51274.0000
TrafficCarModel[32] = RapidGT2

TrafficCarPos[33] = <<-1493.3682, -117.2529, 50.9595>>
TrafficCarQuatX[33] = 0.0127
TrafficCarQuatY[33] = 0.0200
TrafficCarQuatZ[33] = 0.9109
TrafficCarQuatW[33] = 0.4120
TrafficCarRecording[33] = 23
TrafficCarStartime[33] = 55000.0000
TrafficCarModel[33] = RapidGT2

TrafficCarPos[34] = <<-1540.3041, -198.9243, 54.7733>>
TrafficCarQuatX[34] = 0.0290
TrafficCarQuatY[34] = 0.0106
TrafficCarQuatZ[34] = 0.3732
TrafficCarQuatW[34] = 0.9272
TrafficCarRecording[34] = 39
TrafficCarStartime[34] = 56554.0000
TrafficCarModel[34] = f620

TrafficCarPos[35] = <<-1598.0374, -225.2090, 54.4810>>
TrafficCarQuatX[35] = 0.0130
TrafficCarQuatY[35] = -0.0017
TrafficCarQuatZ[35] = -0.2635
TrafficCarQuatW[35] = 0.9646
TrafficCarRecording[35] = 41
TrafficCarStartime[35] = 59722.0000
TrafficCarModel[35] = RapidGT2

TrafficCarPos[36] = <<-1659.9841, -349.6841, 49.2819>>
TrafficCarQuatX[36] = 0.0142
TrafficCarQuatY[36] = -0.0097
TrafficCarQuatZ[36] = -0.4908
TrafficCarQuatW[36] = 0.8711
TrafficCarRecording[36] = 44
TrafficCarStartime[36] = 63220.0000
TrafficCarModel[36] = rocoto

// added on section where rival pap is
TrafficCarPos[37] = <<-1597.3040, -303.3082, 50.0390>>
TrafficCarQuatX[37] = 0.0527
TrafficCarQuatY[37] = 0.0303
TrafficCarQuatZ[37] = 0.3903
TrafficCarQuatW[37] = 0.9187
TrafficCarRecording[37] = 31
TrafficCarStartime[37] = 63577.0000
TrafficCarModel[37] = RapidGT2

TrafficCarPos[38] = <<-1690.8268, -361.6930, 47.9320>>
TrafficCarQuatX[38] = 0.0122
TrafficCarQuatY[38] = -0.0118
TrafficCarQuatZ[38] = -0.6151
TrafficCarQuatW[38] = 0.7883
TrafficCarRecording[38] = 47
TrafficCarStartime[38] = 64012.0000
TrafficCarModel[38] = RapidGT2

TrafficCarPos[39] = <<-1679.7949, -344.0163, 48.5221>>
TrafficCarQuatX[39] = -0.0063
TrafficCarQuatY[39] = -0.0182
TrafficCarQuatZ[39] = 0.7411
TrafficCarQuatW[39] = 0.6711
TrafficCarRecording[39] = 45
TrafficCarStartime[39] = 64150.0000
TrafficCarModel[39] = f620

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
TrafficCarPos[40] = <<-1731.0199, -428.0725, 43.3807>>
TrafficCarQuatX[40] = 0.0134
TrafficCarQuatY[40] = 0.0047
TrafficCarQuatZ[40] = -0.3262
TrafficCarQuatW[40] = 0.9452
TrafficCarRecording[40] = 55
TrafficCarStartime[40] = 66572.0000
TrafficCarModel[40] = rocoto

//comes on to sweeping bend where rival pap takes photos
TrafficCarPos[41] = <<-1798.4171, -458.2988, 41.0861>>
TrafficCarQuatX[41] = -0.0036
TrafficCarQuatY[41] = -0.0442
TrafficCarQuatZ[41] = 0.8837
TrafficCarQuatW[41] = -0.4660
TrafficCarRecording[41] = 49
TrafficCarStartime[41] = 68266.0000
TrafficCarModel[41] = f620

//vehicle climbing the bend where rival takes shots
TrafficCarPos[42] = <<-1718.5543, -516.9016, 37.0122>>
TrafficCarQuatX[42] = 0.0053
TrafficCarQuatY[42] = 0.0289
TrafficCarQuatZ[42] = 0.3998
TrafficCarQuatW[42] = 0.9161
TrafficCarRecording[42] = 40
TrafficCarStartime[42] = 69540.0000
TrafficCarModel[42] = f620

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
/// !!! ALSO may need re-record if wheels dont sit on the ground now
TrafficCarPos[43] = <<-1667.6849, -555.5110, 34.1023>>
TrafficCarQuatX[43] = 0.0205
TrafficCarQuatY[43] = 0.0384
TrafficCarQuatZ[43] = 0.4254
TrafficCarQuatW[43] = 0.9040
TrafficCarRecording[43] = 56
TrafficCarStartime[43] = 73040.0000
TrafficCarModel[43] = rocoto

TrafficCarPos[44] = <<-1647.0862, -565.3936, 32.9925>>
TrafficCarQuatX[44] = -0.0032
TrafficCarQuatY[44] = -0.0033
TrafficCarQuatZ[44] = 0.9523
TrafficCarQuatW[44] = 0.3050
TrafficCarRecording[44] = 50
TrafficCarStartime[44] = 74836.0000
TrafficCarModel[44] = RapidGT2

TrafficCarPos[45] = <<-1606.4623, -607.8560, 31.5569>>
TrafficCarQuatX[45] = 0.0220
TrafficCarQuatY[45] = 0.0106
TrafficCarQuatZ[45] = 0.4308
TrafficCarQuatW[45] = 0.9021
TrafficCarRecording[45] = 51
TrafficCarStartime[45] = 75166.0000
TrafficCarModel[45] = RapidGT2

//on road the pap guy turns onto
TrafficCarPos[46] = <<-1594.0869, -521.4292, 34.9854>>
TrafficCarQuatX[46] = -0.0077
TrafficCarQuatY[46] = -0.0061
TrafficCarQuatZ[46] = 0.8805
TrafficCarQuatW[46] = 0.4739
TrafficCarRecording[46] = 33
TrafficCarStartime[46] = 75476.0000
TrafficCarModel[46] = RapidGT2

TrafficCarPos[47] = <<-1546.9274, -671.7556, 28.4704>>
TrafficCarQuatX[47] = 0.0020
TrafficCarQuatY[47] = -0.0044
TrafficCarQuatZ[47] = 0.9056
TrafficCarQuatW[47] = -0.4241
TrafficCarRecording[47] = 54
TrafficCarStartime[47] = 78004.0000
TrafficCarModel[47] = RapidGT2



// ****  UBER RECORDED PARKED CARS  **** 

ParkedCarPos[0] = <<-283.4568, 251.5497, 88.9099>>
ParkedCarQuatX[0] = 0.0365
ParkedCarQuatY[0] = -0.0067
ParkedCarQuatZ[0] = -0.6704
ParkedCarQuatW[0] = 0.7411
ParkedCarModel[0] = ninef2

ParkedCarPos[1] = <<-339.4699, 293.5367, 85.4549>>
ParkedCarQuatX[1] = 0.0214
ParkedCarQuatY[1] = -0.0196
ParkedCarQuatZ[1] = -0.6993
ParkedCarQuatW[1] = 0.7142
ParkedCarModel[1] = Baller

ParkedCarPos[2] = <<-349.3771, 286.2561, 84.4616>>
ParkedCarQuatX[2] = -0.0255
ParkedCarQuatY[2] = -0.0264
ParkedCarQuatZ[2] = 0.6980
ParkedCarQuatW[2] = 0.7152
ParkedCarModel[2] = emperor

ParkedCarPos[3] = <<-365.4896, 231.2178, 84.2379>>
ParkedCarQuatX[3] = 0.0266
ParkedCarQuatY[3] = 0.0114
ParkedCarQuatZ[3] = -0.6726
ParkedCarQuatW[3] = 0.7394
ParkedCarModel[3] = jackal

ParkedCarPos[4] = <<-438.5490, 254.1521, 82.2881>>
ParkedCarQuatX[4] = -0.0259
ParkedCarQuatY[4] = 0.0162
ParkedCarQuatZ[4] = 0.6577
ParkedCarQuatW[4] = 0.7527
ParkedCarModel[4] = sentinel

ParkedCarPos[5] = <<-1501.3237, -686.0034, 27.1490>>
ParkedCarQuatX[5] = 0.0131
ParkedCarQuatY[5] = 0.0418
ParkedCarQuatZ[5] = 0.4600
ParkedCarQuatW[5] = 0.8869
ParkedCarModel[5] = Mesa

ParkedCarPos[6] = <<-501.7928, 241.8177, 82.6462>>
ParkedCarQuatX[6] = -0.0166
ParkedCarQuatY[6] = -0.0207
ParkedCarQuatZ[6] = 0.7367
ParkedCarQuatW[6] = -0.6758
ParkedCarModel[6] = jackal

ParkedCarPos[7] = <<-488.9706, 270.0654, 82.7035>>
ParkedCarQuatX[7] = -0.0005
ParkedCarQuatY[7] = -0.0128
ParkedCarQuatZ[7] = 0.7263
ParkedCarQuatW[7] = -0.6873
ParkedCarModel[7] = emperor

ParkedCarPos[8] = <<-573.4771, 269.1175, 82.3261>>
ParkedCarQuatX[8] = -0.0337
ParkedCarQuatY[8] = 0.0059
ParkedCarQuatZ[8] = 0.6577
ParkedCarQuatW[8] = 0.7525
ParkedCarModel[8] = RapidGT2

ParkedCarPos[9] = <<-608.2574, 252.1780, 81.3708>>
ParkedCarQuatX[9] = -0.0217
ParkedCarQuatY[9] = -0.0091
ParkedCarQuatZ[9] = 0.7397
ParkedCarQuatW[9] = -0.6725
ParkedCarModel[9] = RapidGT2

ParkedCarPos[10] = <<-684.0115, 237.1335, 80.5793>>
ParkedCarQuatX[10] = 0.0148
ParkedCarQuatY[10] = 0.0140
ParkedCarQuatZ[10] = -0.5278
ParkedCarQuatW[10] = 0.8491
ParkedCarModel[10] = jackal

ParkedCarPos[11] = <<-687.9144, 293.1930, 82.1187>>
ParkedCarQuatX[11] = 0.0060
ParkedCarQuatY[11] = 0.0308
ParkedCarQuatZ[11] = 0.6746
ParkedCarQuatW[11] = 0.7375
ParkedCarModel[11] = ninef2

ParkedCarPos[12] = <<-699.9624, 258.1157, 80.4591>>
ParkedCarQuatX[12] = -0.0385
ParkedCarQuatY[12] = -0.0023
ParkedCarQuatZ[12] = 0.8429
ParkedCarQuatW[12] = 0.5367
ParkedCarModel[12] = jackal

ParkedCarPos[13] = <<-707.4021, 254.6637, 80.0296>>
ParkedCarQuatX[13] = -0.0446
ParkedCarQuatY[13] = 0.0053
ParkedCarQuatZ[13] = 0.8525
ParkedCarQuatW[13] = 0.5207
ParkedCarModel[13] = ninef2

ParkedCarPos[14] = <<-726.7914, 295.4570, 84.4710>>
ParkedCarQuatX[14] = -0.0084
ParkedCarQuatY[14] = 0.0234
ParkedCarQuatZ[14] = 0.7017
ParkedCarQuatW[14] = 0.7120
ParkedCarModel[14] = jackal

ParkedCarPos[15] = <<-727.1002, 245.5221, 77.8995>>
ParkedCarQuatX[15] = -0.0710
ParkedCarQuatY[15] = -0.0493
ParkedCarQuatZ[15] = 0.8491
ParkedCarQuatW[15] = 0.5210
ParkedCarModel[15] = RapidGT2

ParkedCarPos[16] = <<-1248.6432, 253.0192, 63.5542>>
ParkedCarQuatX[16] = -0.0102
ParkedCarQuatY[16] = -0.0354
ParkedCarQuatZ[16] = 0.2844
ParkedCarQuatW[16] = 0.9580
ParkedCarModel[16] = RapidGT2

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[17] = <<-1480.4155, -610.0646, 30.5239>>
ParkedCarQuatX[17] = -0.0260
ParkedCarQuatY[17] = 0.0011
ParkedCarQuatZ[17] = 0.8868
ParkedCarQuatW[17] = 0.4614
ParkedCarModel[17] = rocoto

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[18] = <<-1590.7861, -644.4308, 29.8759>>
ParkedCarQuatX[18] = -0.0146
ParkedCarQuatY[18] = -0.0321
ParkedCarQuatZ[18] = 0.8915
ParkedCarQuatW[18] = -0.4517
ParkedCarModel[18] = rocoto

ParkedCarPos[19] = <<-1509.8171, -679.0406, 27.7404>>
ParkedCarQuatX[19] = 0.0124
ParkedCarQuatY[19] = 0.0412
ParkedCarQuatZ[19] = 0.4361
ParkedCarQuatW[19] = 0.8989
ParkedCarModel[19] = Mesa

ParkedCarPos[20] = <<-1288.8661, 266.9813, 63.7359>>
ParkedCarQuatX[20] = 0.0373
ParkedCarQuatY[20] = -0.0101
ParkedCarQuatZ[20] = -0.2742
ParkedCarQuatW[20] = 0.9609
ParkedCarModel[20] = RapidGT2

ParkedCarPos[21] = <<-1608.5123, -630.4704, 30.6462>>
ParkedCarQuatX[21] = -0.0159
ParkedCarQuatY[21] = -0.0320
ParkedCarQuatZ[21] = 0.8810
ParkedCarQuatW[21] = -0.4718
ParkedCarModel[21] = Mesa

ParkedCarPos[22] = <<-1351.0996, 244.0114, 59.8691>>
ParkedCarQuatX[22] = -0.0220
ParkedCarQuatY[22] = -0.0512
ParkedCarQuatZ[22] = 0.9952
ParkedCarQuatW[22] = -0.0810
ParkedCarModel[22] = RapidGT2

ParkedCarPos[23] = <<-1580.1294, -214.9907, 54.3721>>
ParkedCarQuatX[23] = 0.0235
ParkedCarQuatY[23] = 0.0162
ParkedCarQuatZ[23] = -0.3399
ParkedCarQuatW[23] = 0.9400
ParkedCarModel[23] = RapidGT2

ParkedCarPos[24] = <<-1584.4117, -220.3301, 54.2076>>
ParkedCarQuatX[24] = 0.0181
ParkedCarQuatY[24] = 0.0231
ParkedCarQuatZ[24] = -0.3403
ParkedCarQuatW[24] = 0.9399
ParkedCarModel[24] = RapidGT2

ParkedCarPos[25] = <<-1644.8414, -347.8500, 49.4095>>
ParkedCarQuatX[25] = 0.0212
ParkedCarQuatY[25] = 0.0147
ParkedCarQuatZ[25] = -0.4400
ParkedCarQuatW[25] = 0.8976
ParkedCarModel[25] = RapidGT2

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[26] = <<-1684.9635, -570.7172, 34.4679>>
ParkedCarQuatX[26] = -0.0007
ParkedCarQuatY[26] = -0.0608
ParkedCarQuatZ[26] = 0.8986
ParkedCarQuatW[26] = -0.4345
ParkedCarModel[26] = rocoto

ParkedCarPos[27] = <<-1706.1578, -408.2234, 44.9785>>
ParkedCarQuatX[27] = 0.0492
ParkedCarQuatY[27] = 0.0021
ParkedCarQuatZ[27] = -0.3248
ParkedCarQuatW[27] = 0.9445
ParkedCarModel[27] = RapidGT2

ParkedCarPos[28] = <<-1785.5399, -505.2336, 38.3513>>
ParkedCarQuatX[28] = -0.0003
ParkedCarQuatY[28] = -0.0001
ParkedCarQuatZ[28] = -0.4654
ParkedCarQuatW[28] = 0.8851
ParkedCarModel[28] = RapidGT2

ParkedCarPos[29] = <<-1775.0933, -516.8105, 38.3838>>
ParkedCarQuatX[29] = -0.0029
ParkedCarQuatY[29] = -0.0028
ParkedCarQuatZ[29] = 0.8619
ParkedCarQuatW[29] = 0.5070
ParkedCarModel[29] = RapidGT2

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[30] = <<-1770.0879, -522.8300, 38.5967>>
ParkedCarQuatX[30] = -0.0029
ParkedCarQuatY[30] = 0.0006
ParkedCarQuatZ[30] = 0.8626
ParkedCarQuatW[30] = 0.5059
ParkedCarModel[30] = rocoto

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[31] = <<-1700.4822, -560.2477, 35.9312>>
ParkedCarQuatX[31] = -0.0021
ParkedCarQuatY[31] = -0.0624
ParkedCarQuatZ[31] = 0.8919
ParkedCarQuatW[31] = -0.4478
ParkedCarModel[31] = rocoto

//!!! Pos and quaternion will need update as vehicle model had to change, matador removed from the game !!!
ParkedCarPos[32] = <<-1690.1038, -567.1520, 34.9940>>
ParkedCarQuatX[32] = -0.0034
ParkedCarQuatY[32] = -0.0581
ParkedCarQuatZ[32] = 0.8833
ParkedCarQuatW[32] = -0.4651
ParkedCarModel[32] = rocoto

// ****  UBER RECORDED SET PIECE CARS  **** 

// set behind the main car and the player's view to come down the road
SetPieceCarPos[0] = <<-143.9162, 258.2889, 94.5251>>
SetPieceCarQuatX[0] = -0.0303
SetPieceCarQuatY[0] = -0.0223
SetPieceCarQuatZ[0] = 0.6939
SetPieceCarQuatW[0] = 0.7191
SetPieceCarRecording[0] = 57
SetPieceCarStartime[0] = 0.0000
SetPieceCarRecordingSpeed[0] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[0] = buffalo

// drives up the hill and turns left onto road opposite start point
SetPieceCarPos[1] = <<-219.1163, 226.1757, 88.5679>>
SetPieceCarQuatX[1] = 0.1090
SetPieceCarQuatY[1] = 0.0087
SetPieceCarQuatZ[1] = 0.0041
SetPieceCarQuatW[1] = 0.9940
SetPieceCarRecording[1] = 59
SetPieceCarStartime[1] = 250.0000
SetPieceCarRecordingSpeed[1] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[1] = jackal

// start, drives up over the hill
SetPieceCarPos[2] = <<-277.7864, 257.1765, 89.5156>>
SetPieceCarQuatX[2] = 0.0246
SetPieceCarQuatY[2] = -0.0264
SetPieceCarQuatZ[2] = -0.6748
SetPieceCarQuatW[2] = 0.7371
SetPieceCarRecording[2] = 58
SetPieceCarStartime[2] = 400.0000
SetPieceCarRecordingSpeed[2] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[2] = jackal

//moved from traffic car [34] 
SetPieceCarPos[3] = <<-1338.0682, 205.2171, 58.2626>>
SetPieceCarQuatX[3] = 0.0094
SetPieceCarQuatY[3] = 0.0224
SetPieceCarQuatZ[3] = -0.6916
SetPieceCarQuatW[3] = 0.7219
SetPieceCarRecording[3] = 36
SetPieceCarStartime[3] = 37978.0000
SetPieceCarRecordingSpeed[3] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[3] = jackal

//moved from traffic car [40]
SetPieceCarPos[4] = <<-524.0712, 250.7650, 82.5395>>
SetPieceCarQuatX[4] = -0.0022
SetPieceCarQuatY[4] = 0.0037
SetPieceCarQuatZ[4] = 0.7477
SetPieceCarQuatW[4] = -0.6641
SetPieceCarRecording[4] = 42
SetPieceCarStartime[4] = 10151.0000
SetPieceCarRecordingSpeed[4] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[4] = buffalo

//moved from traffic car [41]
SetPieceCarPos[5] = <<-640.2011, 258.0730, 80.8710>>
SetPieceCarQuatX[5] = -0.0265
SetPieceCarQuatY[5] = -0.0020
SetPieceCarQuatZ[5] = 0.7299
SetPieceCarQuatW[5] = -0.6830
SetPieceCarRecording[5] = 43
SetPieceCarStartime[5] = 13517.0000
SetPieceCarRecordingSpeed[5] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[5] = buffalo

//moved from traffic car [44]
SetPieceCarPos[6] = <<-1062.9277, 258.7392, 63.7718>>
SetPieceCarQuatX[6] = -0.0022
SetPieceCarQuatY[6] = 0.0060
SetPieceCarQuatZ[6] = 0.7219
SetPieceCarQuatW[6] = -0.6920
SetPieceCarRecording[6] = 46
SetPieceCarStartime[6] = 26981.0000
SetPieceCarRecordingSpeed[6] = PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK
SetPieceCarModel[6] = jackal

ENDPROC

/// PURPOSE:
///    deals with requesting and removing the vehicle recordings
///    used by ther set piece and traffic vehicles in the uber recording
PROC PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
	IF GET_GAME_TIMER() - iPreLoadRecordingsTimer > 3
		INT i = 0
		REPEAT COUNT_OF(SetPieceCarID) i
			IF SetPieceCarRecording[i] > 0
				IF (fCurrentPlaybackTime > (SetPieceCarStartime[i] - 5000.0))
					REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[i], sPap1_UberRecordingName)
					IF NOT DOES_ENTITY_EXIST(SetPieceCarID[i])
						REQUEST_MODEL(SetPieceCarModel[i])
					ENDIF
				ELIF (fCurrentPlaybackTime > (SetPieceCarStartime[i] + 3000.0))
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[i])
						REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], sPap1_UberRecordingName)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
		REPEAT COUNT_OF(TrafficCarID) i 
			IF TrafficCarRecording[i] > 0
				IF (fCurrentPlaybackTime > (TrafficCarStartime[i] - 5000.0))
					REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], sPap1_UberRecordingName)
					/*IF NOT DOES_ENTITY_EXIST(TrafficCarID[i])	// not sure if i should do this
						REQUEST_MODEL(TrafficCarModel[i])
					ENDIF*/
				ELIF (fCurrentPlaybackTime > (TrafficCarStartime[i] + 3000.0))
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TrafficCarID[i])
						REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], sPap1_UberRecordingName)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT		
		iPreLoadRecordingsTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes all of the vehicle recordings needed for the uber recorder
PROC REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
	INT iNumTrafficCars = COUNT_OF(TrafficCarID)
	INT iNumSetPieceCars = COUNT_OF(SetPieceCarID)
	INT i = 0	
	//celeb vehicle's recording
	REMOVE_VEHICLE_RECORDING(1, sPap1_UberRecordingName)	
	REPEAT iNumTrafficCars i
		IF TrafficCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(TrafficCarRecording[i], sPap1_UberRecordingName)
		ENDIF
	ENDREPEAT
	i = 0
	REPEAT iNumSetPieceCars i
		IF SetPieceCarRecording[i] > 0
			REMOVE_VEHICLE_RECORDING(SetPieceCarRecording[i], sPap1_UberRecordingName)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    taken from traffic.sch for skipping time in recording which isn't part of the uber recording
/// PARAMS:
///    InCar - 
///    iRec - 
///    fTime - 
///    bPlay - 
///    bUseAI - 
///    bForceUpdate - 
///    bSwitchToAIOnPlayerImpact - 
///    bSwitchToAIOnAnyImpact - 
/// RETURNS:
///    
FUNC BOOL PAP1_SET_CAR_AT_PLAYBACK_POSITION(VEHICLE_INDEX &InCar, INT iRec, STRING sRecName, FLOAT fTime, BOOL bPlay, BOOL bUseAI, BOOL bForceUpdate = FALSE, 
									   BOOL bSwitchToAIOnPlayerImpact = FALSE, BOOL bSwitchToAIOnAnyImpact = FALSE)
	FLOAT fTemp
	IF (iRec > 0)
		REQUEST_VEHICLE_RECORDING(iRec, sRecName)
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iRec, sRecName)
			IF IS_VEHICLE_DRIVEABLE(InCar)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(InCar)
					FREEZE_ENTITY_POSITION(InCar, FALSE)
					IF (bUseAI)
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(InCar, iRec, sRecName)
					ELSE
						
						IF iRec != iDontSwitchThisSetpieceRecordingToAI
						AND iRec != iDontSwitchThisSetpieceRecordingToAI2
							IF bSwitchToAIOnAnyImpact
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(InCar, iRec, sRecName, ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT) | ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
							ELIF bSwitchToAIOnPlayerImpact
								START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(InCar, iRec, sRecName, ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT) | ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
							ELSE
								START_PLAYBACK_RECORDED_VEHICLE(InCar, iRec, sRecName)
							ENDIF
						ELSE
							START_PLAYBACK_RECORDED_VEHICLE(InCar, iRec, sRecName)
						ENDIF
						
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(InCar, fTime)
						
						IF bForceUpdate
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(InCar)
						ENDIF
						
						RETURN(TRUE)	
					ENDIF
				ELSE
					IF (GET_CURRENT_PLAYBACK_FOR_VEHICLE(InCar) = GET_VEHICLE_RECORDING_ID(iRec, sRecName))
						fTemp = GET_TIME_POSITION_IN_RECORDING(InCar)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(InCar, (fTime - fTemp))
						IF NOT (bPlay)
							STOP_PLAYBACK_RECORDED_VEHICLE(InCar)	
							REQUEST_VEHICLE_RECORDING(iRec, sRecName)
						ENDIF
						
						IF bForceUpdate
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(InCar)
						ENDIF
						
						RETURN(TRUE)
					ELSE
						STOP_PLAYBACK_RECORDED_VEHICLE(InCar)
						FREEZE_ENTITY_POSITION(InCar, FALSE)
						IF (bUseAI)
							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(InCar, iRec, sRecName)
						ELSE
							IF iRec != iDontSwitchThisSetpieceRecordingToAI
							AND iRec != iDontSwitchThisSetpieceRecordingToAI2
								IF bSwitchToAIOnAnyImpact
									START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(InCar, iRec, sRecName, ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT) | ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))
								ELIF bSwitchToAIOnPlayerImpact
									START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(InCar, iRec, sRecName, ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT) | ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY))	
								ELSE
									START_PLAYBACK_RECORDED_VEHICLE(InCar, iRec, sRecName)	
								ENDIF 
							ELSE
								START_PLAYBACK_RECORDED_VEHICLE(InCar, iRec, sRecName)
							ENDIF
						ENDIF
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(InCar, fTime)
						
						IF bForceUpdate
							FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(InCar)
						ENDIF
						
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

/// PURPOSE:
///    detach an entity, checking for IS_ENTITY_ALIVE
/// PARAMS:
///    entityIndex - entity to detach
PROC SAFE_DETACH_ENTITY(ENTITY_INDEX &entityIndex)
	IF IS_ENTITY_ALIVE(entityIndex)
		IF IS_ENTITY_ATTACHED(entityIndex)
			DETACH_ENTITY(entityIndex)
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    remove object from the game
/// PARAMS:
///    objectIndex - object to remove
///    bDelete - if true delete it, else set as no longer needed
PROC SAFE_REMOVE_OBJECT(OBJECT_INDEX &objectIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_OBJECT(objectIndex)
	ELSE
		SAFE_RELEASE_OBJECT(objectIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes vehicle from the world
/// PARAMS:
///    vehIndex - vehicle to be removed
///    bDelete - if true AND player isn't inside it delete it, else set as no longer needed
PROC SAFE_REMOVE_VEHICLE(VEHICLE_INDEX &vehIndex, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_VEHICLE(vehIndex)
	ELSE
		SAFE_RELEASE_VEHICLE(vehIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    removes ped from the game
/// PARAMS:
///    ped - ped to be removed
///    bDelete - if true delete it, else set as no longer needed
///    bDetach - if true ped will be detached if attached, and no in a vehicle or getting into a vehicle
PROC SAFE_REMOVE_PED(PED_INDEX &ped, BOOL bDelete = FALSE)
	IF bDelete
		SAFE_DELETE_PED(ped)
	ELSE
		SAFE_RELEASE_PED(ped)
	ENDIF
ENDPROC

/// PURPOSE:
///    teleport a ped to a new location
/// PARAMS:
///    pedIndex - ped to teleport
///    vPosition - position to teleport to
///    fHeading - heading to give the ped at the new location
///    bSnapToGround - if true ped will be positioned at ground level, else drops in a meter above
///    bForcePedAiAndAnimUpdate - if true FORCE_PED_AI_AND_ANIMATION_UPDATE is applied on the ped
PROC SAFE_TELEPORT_PED(PED_INDEX pedIndex, VECTOR vPosition, FLOAT fHeading = 0.0, BOOL bSnapToGround = FALSE, BOOL bForcePedAiAndAnimUpdate = FALSE)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF bSnapToGround
			// getting odd results with this where peds are 1m+ above ground level
			/*FLOAT fTempGroundZ
			IF GET_GROUND_Z_FOR_3D_COORD(vPosition, fTempGroundZ)
				vPosition.Z = fTempGroundZ
			ENDIF*/
			vPosition.Z = INVALID_WORLD_Z
		ENDIF
		SET_ENTITY_COORDS(pedIndex, vPosition)
		SET_ENTITY_HEADING(pedIndex, fHeading)
		IF bForcePedAiAndAnimUpdate
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedIndex)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    teleports a ped out of their vehicle.
///    NOTE: proc waits until GET_SAFE_COORD_FOR_PED returns true
/// PARAMS:
///    pedIndex - ped to Teleport
PROC SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PED_INDEX pedIndex)
	VECTOR vRespotCoords = << 0.0, 0.0, 0.0 >>
	FLOAT fRespotHeading = 0.0	
	IF IS_ENTITY_ALIVE(pedIndex)
		IF IS_PED_SITTING_IN_ANY_VEHICLE(pedIndex)		
			vRespotCoords = GET_ENTITY_COORDS(pedIndex)			
			WHILE NOT GET_SAFE_COORD_FOR_PED(vRespotCoords, FALSE, vRespotCoords)
				WAIT(0)
				vRespotCoords.x += 2.0
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE - looking for safe coords") ENDIF #ENDIF
			ENDWHILE			
			fRespotHeading = GET_ENTITY_HEADING(pedIndex)			
			SAFE_TELEPORT_PED(pedIndex, vRespotCoords, fRespotHeading, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks ped and vehicle are ok before putting the ped in the vehicle
///    If the seat is already occupied by a ped, it gets deleted.
/// PARAMS:
///    pedIndex - ped to put in the vehicle
///    vehIndex - vehicle ped is going into
///    seat - seat ped is to sit in
PROC SAFE_PUT_PED_INTO_VEHICLE(PED_INDEX pedIndex, VEHICLE_INDEX &vehIndex, VEHICLE_SEAT seat = VS_DRIVER)
	IF IS_ENTITY_ALIVE(pedIndex)
	AND IS_VEHICLE_OK(vehIndex)
		IF IS_VEHICLE_SEAT_FREE(vehIndex, seat)
			SAFE_SET_PED_INTO_VEHICLE(pedIndex, vehIndex, seat)
		ELSE			
			PED_INDEX pedTemp_AlreadyInSeat = GET_PED_IN_VEHICLE_SEAT(vehIndex, seat)	//check the ped we want to put in the vehicle isn't already in the vehicle				
			IF pedTemp_AlreadyInSeat != pedIndex
				IF IS_ENTITY_ALIVE(pedTemp_AlreadyInSeat)
					SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(pedTemp_AlreadyInSeat)
					SAFE_PUT_PED_INTO_VEHICLE(pedIndex, vehIndex, seat)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Test for if two coords are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN VDIST2(v1, v2) <= (fRange*fRange)
ENDFUNC

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL PAP1_IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    Test if Beverly has been consecutively aiming the camera at the celeb for at least the specified time
/// PARAMS:
///    iTimeAmount - the time to have been aiming for
/// RETURNS:
///    TRUE if he has been successfully aiming for iTimeAmount or more
FUNC BOOL HAS_TIME_PASSED_BEV_CONSECUTIVELY_AIMED_CAMERA_AT_CELEB(INT iTimeAmount)
	IF iConsecutiveAimingTimer = -1	// if the value hasn't be initial set don't check the timer
		RETURN FALSE
	ENDIF
	RETURN (GET_GAME_TIMER() - iConsecutiveAimingTimer) > iTimeAmount
ENDFUNC

/// PURPOSE:
///    check if iTimeAmount has passed since iTimer was set
/// PARAMS:
///    iTimer - the timer
///    iTimeAmount - the amount of time to check has passed
/// RETURNS:
///    True is the specified amount of time has passed for the specified timer.
FUNC BOOL HAS_TIME_PASSED(INT iTimer, INT iTimeAmount)
	RETURN (GET_GAME_TIMER() - iTimer) > iTimeAmount
ENDFUNC

/// PURPOSE:
///    Performs the CAN_PED_SEE_PED check and sets the specified FrameCounter if it returned TRUE
///    counter is used as a grace period for checking the ped has seen the player, as CAN_PED_SEE_PED takes several frames to return
/// PARAMS:
///    pedIndex - the ped to test against the player
///    iFrameCountLastSeenPlayer - the ped's last seen player counter
///    fOverride_PedViewCone - the ped's viewing cone - 170 is the default value
PROC UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(PED_INDEX pedIndex, INT &iFrameCountLastSeenPlayer, FLOAT fOverride_PedViewCone = 170.0)
	IF IS_ENTITY_ALIVE(pedIndex)
		IF CAN_PED_SEE_PED(pedIndex, PLAYER_PED_ID(), fOverride_PedViewCone)
			iFrameCountLastSeenPlayer = GET_FRAME_COUNT()
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER - CAN_PED_SEE_PED returned TRUE so set frame count to : ", iFrameCountLastSeenPlayer) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check is the specified frame count has passed using specified timer.
/// PARAMS:
///    iFrameCounterTimer - Frame Count to check.
///    iFrameAmount - Frame Count to check against.
/// RETURNS:
///    True is the specified amount of iFrameAmount has passed for the specified iFrameCounterTimer.
FUNC BOOL HAS_FRAME_COUNTER_PASSED(INT iFrameCounterTimer, INT iFrameAmount)
	RETURN (GET_FRAME_COUNT() - iFrameCounterTimer) > iFrameAmount
ENDFUNC

/// PURPOSE:
///    gets which side vCoordsToCheck is to entityIndex
///    Used to decide which attack anim to play, based on which side of the victim the attacking ped is approaching from
///    NOTE: doesn't check entity is alive
/// PARAMS:
///    entityIndex - the entity to check against
///    vEntityCoords - the entities coords
///    vCoordsToCheck - the coords we want to check
/// RETURNS:
///    Int depicting the side of entityIndex the vCoordsToCheck is.  0 = Left side, 1 = Right side, -1 = directly in line
FUNC INT GET_SIDE_COORDS_IS_TO_ENTITY(ENTITY_INDEX entityIndex, VECTOR vEntityCoords, VECTOR vCoordsToCheck)
	FLOAT fSide
	VECTOR vPlaneNorm
	vPlaneNorm = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, << 3.0, 0.0, 0.0 >>) - vEntityCoords)	
	fSide = DOT_PRODUCT(vCoordsToCheck - vEntityCoords, vPlaneNorm)
	#IF IS_DEBUG_BUILD
		IF bDebug_DisplayInfo_GetSideCoordsIsToEntity
			VECTOR vTemp
			vTemp = GET_ENTITY_FORWARD_VECTOR(entityIndex) * 60.0
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(entityIndex) + vTemp, GET_ENTITY_COORDS(entityIndex) - vTemp, 255, 0, 0, 150)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_SIDE_COORDS_IS_TO_ENTITY : vEntityCoords = ", vEntityCoords) ENDIF #ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_SIDE_COORDS_IS_TO_ENTITY : vCoordsToCheck = ", vCoordsToCheck) ENDIF #ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_SIDE_COORDS_IS_TO_ENTITY : vPlaneNorm = ", vPlaneNorm) ENDIF #ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_SIDE_COORDS_IS_TO_ENTITY dot returning : ", fSide) ENDIF #ENDIF
		ENDIF
	#ENDIF
	IF (fSide < 0)
		#IF IS_DEBUG_BUILD IF bDebug_DisplayInfo_GetSideCoordsIsToEntity DRAW_RECT(0.3, 0.3, 0.2, 0.2, 0, 255, 0, 150) ENDIF #ENDIF // green box
    	RETURN 0	//left side
	ELIF (fSide > 0)
		#IF IS_DEBUG_BUILD IF bDebug_DisplayInfo_GetSideCoordsIsToEntity DRAW_RECT(0.3, 0.3, 0.2, 0.2, 0, 0, 255, 150) ENDIF #ENDIF	// blue box
		RETURN 1	//right side
	ENDIF
	RETURN -1	// directly in line
ENDFUNC

/// PURPOSE:
///    Checks if two vectors are similair with in the specified thresholds
/// PARAMS:
///    v1 - Vector 1
///    v2 - Vector 2
///    fTolerance - Tolerance factor.
/// RETURNS:
///    TRUE if v1 is within fTolerance of v2.
FUNC BOOL ARE_VECTORS_SIMILIAR_WITHIN_THRESHOLDS(VECTOR v1, VECTOR v2, FLOAT fX_threshold = 0.5, FLOAT fY_threshold = 0.5, FLOAT fZ_threshold = 0.5)
	#IF IS_DEBUG_BUILD
		VECTOR vTempMin
		VECTOR vTempMax
		vTempMin = v1 - << fX_threshold, fY_threshold, fZ_threshold >>
		vTempMax = v1 + << fX_threshold, fY_threshold, fZ_threshold >>
		DRAW_DEBUG_BOX(vTempMin, vTempMax, 0, 255, 0, 150)
		vTempMin = v2 - << fX_threshold, fY_threshold, fZ_threshold >>
		vTempMax = v2 + << fX_threshold, fY_threshold, fZ_threshold >>
		DRAW_DEBUG_BOX(vTempMin, vTempMax, 0, 0, 255, 150)
	#ENDIF
	IF ABSF(v1.x - v2.x) <= fX_threshold
		IF ABSF(v1.y - v2.y) <= fY_threshold
			IF ABSF(v1.z - v2.z) <= fZ_threshold
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, DEBUG_MISSION, "ARE_VECTORS_SIMILIAR_WITHIN_THRESHOLDS -> returning true ")  ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF		
	ENDIF	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    clear's the specified print if it is displaying
/// PARAMS:
///    sStringToClear - string to test
PROC CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING(STRING sStringToClear)
	IF IS_THIS_PRINT_BEING_DISPLAYED(sStringToClear)
		CLEAR_THIS_PRINT(sStringToClear)
	ENDIF
ENDPROC

/// PURPOSE:
///    clear's the specified help text if it is displaying
/// PARAMS:
///    sStringToClear - string to test
PROC CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING(STRING sStringToClear, BOOL bClearNow = TRUE)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sStringToClear)
		CLEAR_HELP(bClearNow)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
///    also checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT
///    can return "NULL" the first few frames after CREATE_CONVERSATION has returned true
/// PARAMS:
///    sConversationRoot - the conversation root to test
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a root matching the passed in string or "NULL"
FUNC BOOL IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(STRING sConversationRoot)
	TEXT_LABEL_23 tlTempRoot		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()		
		//if current root matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempRoot, sConversationRoot)
			RETURN TRUE
		ENDIF		
		//if current root matches null, return true
		//because if the correct root isn't returned the first frame or so after CREATE_CONVERSATION has return true.
		IF ARE_STRINGS_EQUAL(tlTempRoot, "NULL")
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a specific conversation root is currently playing
///    also checks against the issue where GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL
///    can return "NULL" the first few frames after CREATE_CONVERSATION has returned true
/// PARAMS:
///    sConversationLabel - the conversation label to test
/// RETURNS:
///    TRUE - if there is an ongoing or queued conversation with a label matching the passed in string or "NULL"
FUNC BOOL IS_SPECIFIC_CONVERSATION_LABEL_CURRENTLY_PLAYING(STRING sConversationLabel)
	TEXT_LABEL_23 tlTempLabel		
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		tlTempLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()		
		//if current label matches passed in string return true
		IF ARE_STRINGS_EQUAL(tlTempLabel, sConversationLabel)
			RETURN TRUE
		ENDIF		
		//if current label matches null, return true
		//because if the correct root isn't returned the first frame or so after CREATE_CONVERSATION has return true.
		IF ARE_STRINGS_EQUAL(tlTempLabel, "NULL")
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
///    cloneConversation - ?
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using CREATE_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - Which line of dialogue to start from
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attempts to start dialogue using PLAY_SINGLE_LINE_FROM_CONVERSATION, but first checks that no message is being displayed or that the subtitles are off in the profile settings
/// PARAMS:
///    YourPedStruct - the mission's conversation struct
///    WhichBlockOfTextToLoad - Thr text block the convo lives in 
///    WhichRootLabel - The text root the convo lives in
///    WhichSpecificLabel - the specific line we want to play
///    PassedConversationPriority - the priority level of the convo
///    ShouldDisplaySubtitles - if subtitles should be displayed  NOTE: if set to FALSE we don't check for a message being displayed
///    ShouldAddToBriefScreen - if the convo should print to the brief screen 
/// RETURNS:
///    TRUE if the convo was created successfully
FUNC BOOL PAP1_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)
	IF ShouldDisplaySubtitles = DISPLAY_SUBTITLES
		IF IS_MESSAGE_BEING_DISPLAYED()
			IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) <> 0
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(YourPedStruct, WhichBlockOfTextToLoad, WhichRootLabel, WhichSpecificLabel, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    will return the enumSubtitlesState needed to allow a dialogue line to play
///    without subtitles if a message is being displayed
/// RETURNS:
///    enumSubtitlesState
FUNC enumSubtitlesState GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES()

	IF IS_MESSAGE_BEING_DISPLAYED()
		RETURN DO_NOT_DISPLAY_SUBTITLES
	ENDIF
	
	RETURN DISPLAY_SUBTITLES
ENDFUNC

/// PURPOSE:
///    trigger ambient speech using PLAY_PED_AMBIENT_SPEECH_WITH_VOICE
///    handles toggling STOP_PED_SPEAKING
/// PARAMS:
///    pedIndex - 
///    Context - 
///    VoiceName - 
///    Params - 
PROC PAP1_PLAY_TRIGGER_AMBIENT_SPEECH(PED_INDEX &pedIndex ,STRING Context, STRING VoiceName, SPEECH_PARAMS Params = SPEECH_PARAMS_ADD_BLIP)
	IF IS_PED_UNINJURED(pedIndex)
		BOOL bTemp = IS_AMBIENT_SPEECH_DISABLED(pedIndex)
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, FALSE)
		ENDIF
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedIndex, Context, VoiceName, Params)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PAP1_PLAY_TRIGGER_AMBIENT_SPEECH - Context : ", Context, " VoiceName : ", VoiceName) ENDIF #ENDIF
		IF bTemp
			STOP_PED_SPEAKING(pedIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    test the player controls to see if they are trying to interupt the rolling start
/// RETURNS:
///    TRUE if player is using controls
FUNC BOOL CHECK_FOR_PLAYER_INTERUPT_ROLLING_START()
		
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LEFT)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_RIGHT)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_PLAYER_INTERUPT_ROLLING_START - returning TRUE, control pressed ") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	INT iLx, iLy, iRx, iRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLx, iLy, iRx, iRy)

	// check for movement controls
	IF ABSI(iLx) > 28
	OR ABSI(iLy) > 28
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_FOR_PLAYER_INTERUPT_ROLLING_START - returning TRUE, analogue sticks values ") ENDIF #ENDIF
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    gets the string of the override clipset name Beverly needs to use with TASK_ENTER_VEHICLE 
/// PARAMS:
///    bUsingLeftEntry - if entering from the left side
/// RETURNS:
///    STRING value
FUNC STRING GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(BOOL bUsingLeftEntry)

	// left side
	IF bUsingLeftEntry
		RETURN "clipset@rcmpaparazzo1ig_1_ds"
	ENDIF
	
	// right side
	RETURN "clipset@rcmpaparazzo1ig_1_ps"
ENDFUNC

/// PURPOSE:
///    trigger a music event, whilst first checking it has been prepared (if required)
/// PARAMS:
///    sMusicEvent - the music event name
///    bRequiresPrepare - if TRUE PREPARE_MUSIC_EVENT is setup before the event can be trigger.  Check the doc to see if an event needs preloading
/// RETURNS:
///    TRUE if the event is trigger successfully
FUNC BOOL SAFE_TRIGGER_MISSION_MUSIC_EVENT(STRING sMusicEvent, BOOL bRequiresPrepare = FALSE)
	IF bRequiresPrepare
		IF NOT PREPARE_MUSIC_EVENT(sMusicEvent)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning FALSE, not yet prepared for = ", sMusicEvent) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	IF TRIGGER_MUSIC_EVENT(sMusicEvent)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SAFE_TRIGGER_MISSION_MUSIC_EVENT - returning TRUE for = ", sMusicEvent) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles triggering the requested mission music events
///    Since these cues need to be sequental, request event will only trigger if the previous event has triggered
/// PARAMS:
///    iMusicEvent - the event we want to trigger
PROC MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()

	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
		SWITCH iMissionMusicEventTriggerCounter
			CASE PAP1_MUSIC_EVENT_START
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_START")
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_START") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE PAP1_MUSIC_EVENT_BIKE
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_BIKE")				
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_BIKE") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE PAP1_MUSIC_EVENT_RIVAL
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_RIVAL")		
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_RIVAL - minus prepare") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE PAP1_MUSIC_EVENT_STOP_TO_RADIO
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_STOP_TO_RADIO", TRUE)
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_RESET_AUDIO_FLAG
					iTimer_DelayResetAudioFlag_AllowScoreAndRadio = GET_GAME_TIMER()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_STOP_TO_RADIO") ENDIF #ENDIF
				ENDIF
			BREAK
			CASE PAP1_MUSIC_EVENT_RESET_AUDIO_FLAG
				// reset the audio flag after the delay
				IF HAS_TIME_PASSED(iTimer_DelayResetAudioFlag_AllowScoreAndRadio, 5000)	// time delay suggested by Rebecca J				
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_RESET_AUDIO_FLAG") ENDIF #ENDIF					
				ENDIF
			BREAK		
			CASE PAP1_MUSIC_EVENT_FAIL
				IF SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_FAIL")
					iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION : ", "PAP1_MUSIC_EVENT_FAIL") ENDIF #ENDIF
				ENDIF
			BREAK
			DEFAULT			
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the music event to be triggered by MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
/// PARAMS:
///    iMusicEvent - the event we want to trigger
PROC SET_MUSIC_EVENT_FOR_TRIGGER(INT iMusicEvent)
	iMissionMusicEventTriggerCounter = iMusicEvent
	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENT_FOR_TRIGGER : ", "iMusicEvent = ", iMusicEvent) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    handles trigger correct music events after a skip has occured.  Replay system sets it's own music scene which 
///    we can't interupr
/// PARAMS:
///    eCurrentStage - which mission stage we are returning to
PROC SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS(MISSION_STAGE eCurrentStage)		
	iMissionMusicEventTriggerCounter = PAP1_MUSIC_EVENT_MAX
	SWITCH eCurrentStage
		CASE MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
			FALLTHRU
		CASE MISSION_STAGE_STOP_RIVAL_PAP
			WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_START")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_RIVAL_PAP_JOIN_CHASE / MISSION_STAGE_STOP_RIVAL_PAP ", "triggering PAP1_START", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
			WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_BIKE")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_RIVAL_PAP_JOIN_CHASE / MISSION_STAGE_STOP_RIVAL_PAP", "triggering PAP1_BIKE", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
			IF eCurrentStage = MISSION_STAGE_STOP_RIVAL_PAP
				WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_RIVAL")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_STOP_RIVAL_PAP", "triggering PAP1_RIVAL", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					WAIT(0)
				ENDWHILE
			ENDIF
			IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_CHASE")
				START_AUDIO_SCENE("PAPARAZZO_01_CHASE")								
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_RIVAL_PAP_JOIN_CHASE / MISSION_STAGE_STOP_RIVAL_PAP - set audio scene active - ", "PAPARAZZO_01_CHASE") ENDIF #ENDIF
			ENDIF
		BREAK
		CASE MISSION_STAGE_DROP_BEVERLY_OFF
			SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_STOP_TO_RADIO)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_DROP_BEVERLY_OFF - sET_MUSIC_EVENT_FOR_TRIGGER - ", "PAP1_MUSIC_EVENT_STOP_TO_RADIO") ENDIF #ENDIF
		BREAK
		// ensure no music cues will trigger during outro
		CASE MISSION_STAGE_END_CUTSCENE
			WHILE NOT SAFE_TRIGGER_MISSION_MUSIC_EVENT("PAP1_FAIL")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS : MISSION_STAGE_END_CUTSCENE", "triggering PAP1_FAIL", " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				WAIT(0)
			ENDWHILE
		BREAK
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    handles setting up the scenario blocking area during the chase
///    prevent target driving through peds without collision
PROC UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE(FLOAT fLocal_CurrentPlaybackTime)
	
	INT i
	SWITCH iScenarioBlockingAreaStage
		CASE 0
			// set these up straight away to prevent cars coming in near the start
			
			// while hotel building on left as you go down the hill
			scenarioBlockingChaseRoute[0] = ADD_SCENARIO_BLOCKING_AREA(<< -332.58029, 207.25702, 80.0 >>, << -281.58356, 257.80222, 95.0 >>)
			// right hand side small pull outs on the left past the white hotel
			scenarioBlockingChaseRoute[1] = ADD_SCENARIO_BLOCKING_AREA(<< -501.97012, 184.44948, 75.0 >>, << -390.58398, 241.06360, 90.0 >>)
			
			iScenarioBlockingAreaStage++
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		BREAK	
		CASE 1
			// cleanup the first two areas once chase gets past it
			IF fLocal_CurrentPlaybackTime > 15000.0	
				FOR i = 0 TO (PAP1_MAX_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
					REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
				ENDFOR
				iScenarioBlockingAreaStage++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE - iScenarioBlockingAreaStage++ =", iScenarioBlockingAreaStage, " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    request path nodes using REQUEST_PATH_NODES_IN_AREA_THIS_FRAME
/// PARAMS:
///    vPickupPosition - position pathing from
///    vDestinationPosition - position trying to path to
///    vPathNodeMin - the calculated minimum position we are loading paths
///    vPathNodeMax - the calculated maximum position we are loading paths
///    iExtraDist - used to add leeyway area around the position before the request
PROC PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(VECTOR vPickupPosition, VECTOR vDestinationPosition, VECTOR &vPathNodeMin, VECTOR &vPathNodeMax, INT iExtraDist = 1000)
	
	VECTOR vTempPathNodeMin, vTempPathNodeMax
	
	// determine if this is a new request 
	
	// set the area's X values
	IF vDestinationPosition.X <= vPickupPosition.X
		vTempPathNodeMin.X = vDestinationPosition.X
		vTempPathNodeMax.X = vPickupPosition.X
	ELSE
		vTempPathNodeMin.X = vPickupPosition.X
		vTempPathNodeMax.X = vDestinationPosition.X
	ENDIF
	// set the area's X values
	IF vDestinationPosition.Y <= vPickupPosition.Y
		vTempPathNodeMin.Y = vDestinationPosition.Y
		vTempPathNodeMax.Y = vPickupPosition.Y
	ELSE
		vTempPathNodeMin.Y = vPickupPosition.Y
		vTempPathNodeMax.Y = vDestinationPosition.Y
	ENDIF
	// extra leeway around the positions
	vTempPathNodeMin -= << iExtraDist, iExtraDist, iExtraDist >>
	vTempPathNodeMax += << iExtraDist, iExtraDist, iExtraDist >>
	//CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME -locally generated vector = vMin : ", vTempPathNodeMin, " vMax : ", vTempPathNodeMax,
	//													" using vPickupPosition : ", vPickupPosition, " : vDestinationPosition : ", vDestinationPosition)
	
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vTempPathNodeMin, vPathNodeMin)
	OR NOT ARE_VECTORS_ALMOST_EQUAL(vTempPathNodeMin, vPathNodeMin)
		vPathNodeMin = vTempPathNodeMin
		vPathNodeMax = vTempPathNodeMax
		CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME - detected new request for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	ELSE
		//CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " : PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME - detected existing request for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)
	ENDIF
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeMin.X, vPathNodeMin.Y, vPathNodeMax.X, vPathNodeMax.Y)
ENDPROC

/// PURPOSE:
///    check if the path nodes are loaded for the area
/// PARAMS:
///    vPathNodeMin - minimum positon
///    vPathNodeMax - maximum position
/// RETURNS:
///    TRUE if ARE_NODES_LOADED_FOR_AREA are TRUE
FUNC BOOL PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA(VECTOR vPathNodeMin, VECTOR vPathNodeMax)
	IF ARE_NODES_LOADED_FOR_AREA(vPathNodeMin.X, vPathNodeMin.Y, vPathNodeMax.X, vPathNodeMax.Y)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA - returning TRUE for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)  ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA - returning FALSE for - vMin : ", vPathNodeMin, " vMax : ", vPathNodeMax)  ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    checks if the player's current weapon is lethal
/// PARAMS:
///    bIncludeProjectilesCheck - if true, checks for grenades,stickybombs etc
///    bIncludeLeathalMeleeWeapons - if true check includes knife, hammer, crowbar etc
///    bIncludeStunGunAsLethal - added parameter as we now want pool peds to surrender to stun gun attack
/// RETURNS:
///    true - if lethal
FUNC BOOL IS_PLAYER_CURRENT_WEAPON_LETHAL(BOOL bIncludeProjectilesCheck = TRUE, BOOL bIncludeLeathalMeleeWeapons = TRUE, BOOL bIncludeStunGunAsLethal = TRUE)	
	//different dialogue based on player weapon
	WEAPON_TYPE currentPlayerWeapon
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentPlayerWeapon)
	SWITCH currentPlayerWeapon
		CASE WEAPONTYPE_PISTOL
			FALLTHRU
		CASE WEAPONTYPE_COMBATPISTOL
			FALLTHRU
		CASE WEAPONTYPE_APPISTOL
			FALLTHRU
		CASE WEAPONTYPE_DLC_SNSPISTOL
			FALLTHRU
		CASE WEAPONTYPE_MICROSMG
			FALLTHRU
		CASE WEAPONTYPE_SMG
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTRIFLE
			FALLTHRU
		CASE WEAPONTYPE_CARBINERIFLE
			FALLTHRU
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			FALLTHRU
		CASE WEAPONTYPE_ADVANCEDRIFLE
			FALLTHRU
		CASE WEAPONTYPE_MG
			FALLTHRU
		CASE WEAPONTYPE_COMBATMG
			FALLTHRU
		CASE WEAPONTYPE_PUMPSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_ASSAULTSHOTGUN
			FALLTHRU
		CASE WEAPONTYPE_SNIPERRIFLE
			FALLTHRU
		CASE WEAPONTYPE_HEAVYSNIPER
			FALLTHRU
		CASE WEAPONTYPE_REMOTESNIPER
			FALLTHRU
		CASE WEAPONTYPE_GRENADELAUNCHER
			FALLTHRU
		CASE WEAPONTYPE_RPG
			FALLTHRU
		CASE WEAPONTYPE_MINIGUN
			RETURN TRUE
	ENDSWITCH	
	IF bIncludeProjectilesCheck
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_GRENADE
				FALLTHRU
			CASE WEAPONTYPE_STICKYBOMB
				FALLTHRU
			CASE WEAPONTYPE_MOLOTOV
				FALLTHRU
			CASE WEAPONTYPE_BZGAS
				RETURN TRUE
		ENDSWITCH
	ENDIF
	IF bIncludeLeathalMeleeWeapons
		SWITCH currentPlayerWeapon
			CASE WEAPONTYPE_KNIFE
				FALLTHRU
			CASE WEAPONTYPE_HAMMER
				FALLTHRU
			CASE WEAPONTYPE_CROWBAR
				FALLTHRU
			CASE WEAPONTYPE_PETROLCAN
				FALLTHRU
			CASE WEAPONTYPE_DLC_BOTTLE
				RETURN TRUE
		ENDSWITCH
	ENDIF
	IF bIncludeStunGunAsLethal
		IF currentPlayerWeapon = WEAPONTYPE_STUNGUN
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is intimidating a ped enough that they should react
/// PARAMS:
///    pedIndex - Ped who the player is intimidating
///    iFrameCounter_PedLastSeePlayer - the frame count for the last time the pedIndex could see the player
///    vPlayerCoords - the player's position
///    bReturnForDamage - if true and player has damaged the ped the func will return TRUE
///    bReturnForMelee - if player is seen in melee in close proximity
///    fPlayerDetectionDist - Player is automatically classed as intimidating when shooting or targetting within this range
///    bAimingCloseToPedCounts - if true player aiming close to the ped counts
///    fBulletRadiusCheck - Used for IS_BULLET_IN_AREA check around pedIndex
///    fProjectileRadiusCheck - Used for IS_PROJECTILE_IN_AREA check around pedIndex
///    bVisibleWeaponIsIntimidating - if true and player is seen with weapon drawn func will return true
/// RETURNS:
///    True if bullet is in area around ped, if player is shooting, if player is targetting or free aiming at ped in view or if damaged.
FUNC BOOL IS_PLAYER_INTIMIDATE_SPECIFIC_PED(PED_INDEX pedIndex, INT iFrameCounter_PedLastSeePlayer, VECTOR vPlayerCoords, BOOL bReturnForDamage = TRUE, BOOL bReturnForMelee = TRUE, FLOAT fPlayerDetectionDist = 20.0,
									BOOL bAimingCloseToPedCounts = FALSE, FLOAT fBulletRadiusCheck = 8.0, FLOAT fProjectileRadiusCheck = 15.0, BOOL bVisibleWeaponIsIntimidating = FALSE)
	IF IS_PED_UNINJURED(pedIndex)	
		VECTOR vTemp_PedPos = GET_ENTITY_COORDS(pedIndex)	
		IF IS_BULLET_IN_AREA(vTemp_PedPos, fBulletRadiusCheck, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - bullet in area around ped") ENDIF #ENDIF
			RETURN TRUE
		ENDIF			
		IF NOT HAS_FRAME_COUNTER_PASSED(iFrameCounter_PedLastSeePlayer, CAN_PED_SEE_PED_FRAME_COUNT_GRACE_PERIOD)	// test ped has seen the player in the last 10 frames
			IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerCoords, fPlayerDetectionDist)	//and is with in the range
				IF IS_PLAYER_CURRENT_WEAPON_LETHAL()
					IF bVisibleWeaponIsIntimidating
						// B*1511108 - try to ensure weapon is visible
						IF IS_PED_WEAPON_READY_TO_SHOOT(PLAYER_PED_ID())
						OR IS_PED_RELOADING(PLAYER_PED_ID())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing has leathal weapon in view") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedIndex)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing targeting ped") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
					IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedIndex)	
							//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing free aiming at ped") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
						IF bAimingCloseToPedCounts	
							IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedIndex, 45.0)
								//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - playing free aiming around ped") ENDIF #ENDIF
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					//IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedIndex, 45.0)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player shooting towards Beverly with weapon") ENDIF #ENDIF
						RETURN TRUE
					//ENDIF
				ENDIF
				IF bReturnForMelee
					IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
					OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
						IF IS_ENTITY_IN_RANGE_COORDS(pedIndex, vPlayerCoords, 1.0)	//and is with in the range
							//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player seen in combat ") ENDIF #ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - player seen performing stealth kill") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF bReturnForDamage
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedIndex)
				//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - ped damaged by player with weapon") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, vTemp_PedPos, fProjectileRadiusCheck)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - explosion in area around ped") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
		VECTOR vMin, vMax // is one of the player's projectiles nearby?
		vMin = vTemp_PedPos
		vMax = vMin
		vMin.x= vMin.x - fProjectileRadiusCheck
		vMin.y = vMin.y -fProjectileRadiusCheck
		vMin.z = vMin.z - fProjectileRadiusCheck
		vMax.x = vMax.x + fProjectileRadiusCheck
		vMax.y = vMax.y + fProjectileRadiusCheck
		vMax.z = vMax.z + fProjectileRadiusCheck
		IF IS_PROJECTILE_IN_AREA(vMin, vMax, TRUE)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintBailBondInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PLAYER_INTIMIDATE_SPECIFIC_PED - projectile in area around ped : ", "return TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Draw the sight cone from a ped
/// PARAMS:
///    vStartPos - positio t o draw from
///    fViewAngle - directiono to draw in
///    angle - the angle of the cone so 180.0 is can see in a half circle
PROC DRAW_VIEW_CONE(VECTOR vStartPos, FLOAT fViewAngle, FLOAT angle = 120.0)
	angle /= 2.0
	DRAW_DEBUG_LINE(vStartPos, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fViewAngle + angle, <<0, 5, 0>>))
	DRAW_DEBUG_LINE(vStartPos, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStartPos, fViewAngle - angle, <<0, 5, 0>>))
ENDPROC

/// PURPOSE:
///    Setup a shape test for Beverly aiming his camera at the celeb
/// PARAMS:
///    iShapeTestIndex - the shapetest index we will be setting up
///    vStartPos - the position at the front of the camera
///    vStartForward - the cameras forward vector 
///    vCheckPos - the celeb's position we want to aim for
///    fViewCone - the cone the celeb pos has to be to allow the test to be setup
/// RETURNS:
///    TRUE if the shapetest is successfully set up
FUNC BOOL SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME(INT iShapeTestIndex, VECTOR vStartPos, VECTOR vStartForward, VECTOR vCheckPos, FLOAT fViewCone = 170.0)
	//check the shapetest Index is valid and isn't already active
	IF iShapeTestIndex < 0
	OR iShapeTestIndex > NUM_PAP1_SHAPE_TESTS
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "[ERROR] - iShapeTestIndex is out of range RETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ELIF mShapeTestCameraAim[iShapeTestIndex].bActive
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "[ERROR] - iShapeTestIndex is already active RETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	// check to see if vXCheckPos is behind the vStartPos 's forward vector - if so bail out as the target is celeb is behind the camera
	FLOAT fViewLimit = fViewCone / 2.0
	IF (fViewCone > 0.0)	
		VECTOR vNormalisedVec = NORMALISE_VECTOR(vCheckPos - vStartPos)
		FLOAT fViewAngle = DOT_PRODUCT(vStartForward, vNormalisedVec)	
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "fViewAngle = ", fViewAngle)  ENDIF #ENDIF
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "COS(fViewLimit) = ", COS(fViewLimit))  ENDIF #ENDIF
		IF fViewAngle <= COS(fViewLimit)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "RETURN FALSE - vXCheckPos is behind the vStartPos 's forward vector : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
			DRAW_VIEW_CONE(vStartPos, fViewAngle, fViewCone)
			#IF IS_DEBUG_BUILD IF bDebug_DisplayShapeTestPap1CameraAim DRAW_RECT(0.2, 0.5, 0.6, 0.2, 255, 255, 255, 150) ENDIF #ENDIF
			RETURN FALSE // player is behind the ped and can't be seen
		ENDIF
	ENDIF	
	VECTOR vTemp = vCheckPos		
	// extend the line - so we can be sure goes through the point we are interested in
	vTemp = (vCheckPos - vStartPos) * 1.25		
	mShapeTestCameraAim[iShapeTestIndex].mShapeTest = START_SHAPE_TEST_LOS_PROBE(vStartPos, vStartPos + vTemp, SCRIPT_INCLUDE_ALL)	// , sCelebVehicle.vehicle) can't do this as it doesn't seem to get through to the celeb in the vehicle
	IF mShapeTestCameraAim[iShapeTestIndex].mShapeTest = NULL
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "[ERROR]: Failed to create shape test - too many shape tests already : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		mShapeTestCameraAim[iShapeTestIndex].mShapeTest = NULL // reset the shapetest
		mShapeTestCameraAim[iShapeTestIndex].bActive = FALSE
	ELSE			
		// shapetest has been succesfully created, store the ped
		mShapeTestCameraAim[iShapeTestIndex].bActive = TRUE	
		mShapeTestCameraAim[iShapeTestIndex].vCheckPos = vCheckPos
		#IF IS_DEBUG_BUILD
			mShapeTestCameraAim[iShapeTestIndex].vStartPosDebug = vStartPos
			mShapeTestCameraAim[iShapeTestIndex].vEndPosDebug = vStartPos + vTemp
		#ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "New shape test setup  : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks the specified shape test result for Beverly aiming his camera at the celeb
/// PARAMS:
///    iShapeTestIndex - the shape test we want to check
///    iSeeTime - the return time the shape test returned true
/// RETURNS:
///    TRUE if the shape test satisfied the criteria
FUNC BOOL DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME(INT iShapeTestIndex, INT &iSeeTime)
	//check the shapetest Index is valid and is active
	IF iShapeTestIndex < 0
	OR iShapeTestIndex > NUM_PAP1_SHAPE_TESTS
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "[ERROR] - iShapeTestIndex is out of range RETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ELIF NOT mShapeTestCameraAim[iShapeTestIndex].bActive
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "[ERROR] - iShapeTestIndex is NOT active RETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ENDIF	
	INT iTempTime
	INT iHits
	VECTOR vHitPos
	VECTOR vNormal
	ENTITY_INDEX hitEntity
	SHAPETEST_STATUS shapeStatus = GET_SHAPE_TEST_RESULT(mShapeTestCameraAim[iShapeTestIndex].mShapeTest, iHits, vHitPos, vNormal, hitEntity)		
	IF (shapeStatus = SHAPETEST_STATUS_NONEXISTENT)
		// shape test doesn't exist for some reason....reset
		mShapeTestCameraAim[iShapeTestIndex].mShapeTest = NULL
		mShapeTestCameraAim[iShapeTestIndex].bActive = FALSE			
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "Exit - [SHAPE TEST]: Non Existant ETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ELIF (shapeStatus = SHAPETEST_STATUS_RESULTS_NOTREADY)
		// shape test isn't ready - try again next frame
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "Exit - [SHAPE TEST]: Isn't Ready ETURN FALSE : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
		RETURN FALSE
	ENDIF		
	BOOL bOk = FALSE
	FLOAT fProbeHitDist = SHAPE_TEST_PAP1_CAMERA_AIM_PROBE_HIT_DIST
	#IF IS_DEBUG_BUILD 
		IF bDebug_OverrideProbeHitDist
			fProbeHitDist = fDebug_ProbeHitDistOverride
		ENDIF
	#ENDIF
	// if we have hits registered check the hit is close enough
	// we use vdist2 here instead of get_distance_xx() as it's faster
	IF (iHits > 0)
		FLOAT fDist = VDIST2(mShapeTestCameraAim[iShapeTestIndex].vCheckPos, vHitPos)
		bOk = (fDist <= (fProbeHitDist * fProbeHitDist))			
		#IF IS_DEBUG_BUILD
			IF bDebug_DisplayShapeTestPap1CameraAim
				IF (bOk)
					DRAW_DEBUG_SPHERE(vHitPos, 0.25, 0, 255, 0, 150)
					DRAW_DEBUG_LINE(mShapeTestCameraAim[iShapeTestIndex].vStartPosDebug, vHitPos, 0, 255, 0, 150)
					//DRAW_DEBUG_LINE(vHitPos, mShapeTestCameraAim[iShapeTestIndex].vEndPosDebug, 0, 0, 255, 150)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "hit registed in range >>>>>>>>>> : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
				ELSE
					DRAW_DEBUG_SPHERE(vHitPos, 0.25, 255, 0, 0, 150)
					DRAW_DEBUG_LINE(mShapeTestCameraAim[iShapeTestIndex].vStartPosDebug, vHitPos, 255, 0, 0, 150)
					//DRAW_DEBUG_LINE(vHitPos, mShapeTestCameraAim[iShapeTestIndex].vEndPosDebug, 0, 0, 255, 150)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "hit registed out of range <<<<<<<<<<<<<<< : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
				ENDIF
			ENDIF
		#ENDIF			
		// we return the time that we hit
		iTempTime = GET_GAME_TIMER()		
	ELSE
		bOk = TRUE
		// we return the time that we hit
		iTempTime = GET_GAME_TIMER()	
		#IF IS_DEBUG_BUILD	
			IF bDebug_DisplayShapeTestPap1CameraAim
				DRAW_DEBUG_LINE(mShapeTestCameraAim[iShapeTestIndex].vStartPosDebug, mShapeTestCameraAim[iShapeTestIndex].vEndPosDebug, 255, 255, 255, 255)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME : ", "hit registed direct >>>>>>> : iShapeTestIndex = ", iShapeTestIndex) ENDIF #ENDIF
			ENDIF
		#ENDIF		
	ENDIF	
	// we reset the shape test for the last frame
	mShapeTestCameraAim[iShapeTestIndex].mShapeTest = NULL
	mShapeTestCameraAim[iShapeTestIndex].bActive = FALSE	
	IF (bOk)
		iSeeTime = iTempTime
	ENDIF		
	RETURN bOk			
ENDFUNC

/// PURPOSE:
///    Sets all shape test variables to be null
PROC RESET_IS_BEVERLY_AIMING_AT_CELEB_TEST()
	INT i
	FOR i=0 TO NUM_PAP1_SHAPE_TESTS -1
		mShapeTestCameraAim[i].mShapeTest = NULL
		mShapeTestCameraAim[i].bActive = FALSE
	ENDFOR
	iFrameCountSinceLastSuccessfulShapeTestCameraAim = 0
	iShapeTestCamerAimIndex_SetupThisFrame = 0
	iConsecutiveAimingTimer = -1
ENDPROC

/// PURPOSE:
///    Sets up the shapetests for checking Beverly aiming the camera at the celeb
///    uses multiple shapetests setup on different frames, since a shapetest returns the results on the following frame and can't setup the test again in the return frame
///    Also has a small grace period, so if a shapetest returns false it can keep returning true for a couple of frames, this is needed since I want to check for continuously aiming.
/// PARAMS:
///    vDesiredAimPos - celeb's position
/// RETURNS:
///    TRUE if Beverly is successfully aiming the camera at the celeb
FUNC BOOL IS_BEVERLY_AIMING_CAMERA_AT_CELEB(VECTOR vDesiredAimPos)
	IF NOT IS_ENTITY_ALIVE(sObjCameraBeverly.object)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "[ERROR] : sObjCameraBeverly.object isn't alive RETURN FALSE")  ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	#IF IS_DEBUG_BUILD 
		IF bDebug_OverrideBevCameraLensOffset
			vBevCameraLensOffset = vDebug_BevCameraLensOffsetOverride
		ENDIF
	#ENDIF	
	VECTOR vCameraLensPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sObjCameraBeverly.object, vBevCameraLensOffset)
	VECTOR vCameraForward = GET_ENTITY_FORWARD_VECTOR(sObjCameraBeverly.object)
	//for some reason the camera has been setup backwards, so we flip the forward vector results.
	vCameraForward.X = vCameraForward.X * -1.0
	vCameraForward.Y = vCameraForward.Y * -1.0
	vCameraForward.Z = vCameraForward.Z * -1.0
	INT iSeeTime = 0
	FLOAT fViewingCone = SHAPE_TEST_PAP1_CAMERA_AIM_VIEW_CONE
	#IF IS_DEBUG_BUILD 
		IF bDebug_OverrideBevAimCameraViewingCone
			fViewingCone = fDebug_BevAimCameraViewingCone
		ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
		IF bDebug_DisplayShapeTestPap1CameraAim
			DRAW_DEBUG_LINE(vCameraLensPos, vCameraLensPos + vCameraForward, 81, 0, 119, 200)
			DRAW_DEBUG_SPHERE(vCameraLensPos, 0.05, 178, 0, 255, 100)
			DRAW_DEBUG_SPHERE(vCameraLensPos + vCameraForward, 0.05, 178, 0, 255, 200)
			DRAW_DEBUG_SPHERE(vDesiredAimPos, 0.25, 178, 0, 255, 100)
		ENDIF
	#ENDIF								
	// ensure only one shapetest can be setup per frame
	BOOl bSetupOneProbeTestThisFrame = SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME(iShapeTestCamerAimIndex_SetupThisFrame, vCameraLensPos, vCameraForward, vDesiredAimPos, fViewingCone)	
	IF bSetupOneProbeTestThisFrame
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "pre check setup probe ID : ", iShapeTestCamerAimIndex_SetupThisFrame, " Frame Count : ", GET_FRAME_COUNT())  ENDIF #ENDIF
		IF (iShapeTestCamerAimIndex_SetupThisFrame >= (NUM_PAP1_SHAPE_TESTS - 1))
			iShapeTestCamerAimIndex_SetupThisFrame = 0
		ELSE
			iShapeTestCamerAimIndex_SetupThisFrame++
		ENDIF
	ENDIF
	BOOL bBevIsAimingAtCeleb = FALSE
	INT iNumSuccessfulTests = 0
	INT i = 0
	FOR i = 0 TO (NUM_PAP1_SHAPE_TESTS - 1)
		IF DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME(i, iSeeTime)			
			iNumSuccessfulTests++
		ENDIF
	ENDFOR
	// second attempt to setup a probe test, if one has become null in the test process
	IF NOT bSetupOneProbeTestThisFrame
		bSetupOneProbeTestThisFrame = SETUP_TEST_DOES_POS_HAVE_CLEAR_LINE_OF_SIGHT_TO_POS_RETURN_TIME(iShapeTestCamerAimIndex_SetupThisFrame, vCameraLensPos, vCameraForward, vDesiredAimPos, fViewingCone)	
		IF bSetupOneProbeTestThisFrame
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "post check setup probe ID : ", iShapeTestCamerAimIndex_SetupThisFrame, " Frame Count : ", GET_FRAME_COUNT())  ENDIF #ENDIF
			IF (iShapeTestCamerAimIndex_SetupThisFrame >= (NUM_PAP1_SHAPE_TESTS - 1))
				iShapeTestCamerAimIndex_SetupThisFrame = 0
			ELSE
				iShapeTestCamerAimIndex_SetupThisFrame++
			ENDIF
		ENDIF
	ENDIF	
	IF iNumSuccessfulTests > 0
		bBevIsAimingAtCeleb = TRUE
		IF iConsecutiveAimingTimer = -1	//if initial timer hasn't been set then set start it here
			iConsecutiveAimingTimer = GET_GAME_TIMER()
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "iConsecutiveAimingTimer initial time set to = ", iConsecutiveAimingTimer)  ENDIF #ENDIF
		ENDIF
		iFrameCountSinceLastSuccessfulShapeTestCameraAim = 0 // reset this as we had a successful hit
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "RETURN TRUE, iNumSuccessfulTests = ", iNumSuccessfulTests)  ENDIF #ENDIF
	ELSE
		IF iConsecutiveAimingTimer = -1 //don't set as aiming if we haven't successfully aimed the initial time
			bBevIsAimingAtCeleb = FALSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "RETURN FALSE not done initial aim yet")  ENDIF #ENDIF		
		ELSE
			iFrameCountSinceLastSuccessfulShapeTestCameraAim++
			IF iFrameCountSinceLastSuccessfulShapeTestCameraAim >= GRACE_PERIOD_FRAMES_LAST_SUCCESSFUL_SHAPE_TEST_CAMERA_AIM
				iConsecutiveAimingTimer = -1 // reset the aim timer
				bBevIsAimingAtCeleb = FALSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "RETURN FALSE, iConsecutiveAimingTimer reset to  = ", iConsecutiveAimingTimer)  ENDIF #ENDIF
			ELSE
				bBevIsAimingAtCeleb = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB : ", "RETURN TRUE, not aiming however still in the grace period : iFrameCountSinceLastSuccessfulShapeTestCameraAim = ", iFrameCountSinceLastSuccessfulShapeTestCameraAim)  ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bBevIsAimingAtCeleb
ENDFUNC

/// PURPOSE:
///    Set Beverly to Play hurry up anim
/// PARAMS:
///    entityTarget - entity he wants to get to
PROC SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(ENTITY_INDEX entityTarget)
	IF IS_PED_UNINJURED(sBeverlyPed.ped)
		IF IS_ENTITY_ALIVE(entityTarget)
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")
			AND NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")	
				IF IS_ENTITY_IN_ARC_2D(sBeverlyPed.ped, entityTarget, 85.0)	// only play the anim if the target is infront of Beverly
					INT iSide = GET_SIDE_COORDS_IS_TO_ENTITY(sBeverlyVehicle.vehicle, GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), GET_ENTITY_COORDS(entityTarget))
					STRING sHurryUpAnim
					IF iSide = 1 // RIGHT SIDE
						sHurryUpAnim = "hurry_up_right_pap"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_HURRY_UP_ANIM - hurry_up_right_pap set") ENDIF #ENDIF
					ELSE
						sHurryUpAnim = "hurry_up_left_pap"
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_HURRY_UP_ANIM - hurry_up_left_pap set") ENDIF #ENDIF
					ENDIF					
					//CLEAR_PED_TASKS(sBeverlyPed.ped)
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sHurryUpAnim, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_HOLD_LAST_FRAME)	// AF_HOLD_LAST_FRAME added to fix bug 1095914, AF_UPPERBODY added to fix B*1148188
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_HURRY_UP_ANIM - anim applied") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Beverly playing the point anims
/// PARAMS:
///    entityTarget - what to point at
/// RETURNS:
///    TRUE if the anim was tasked successfully
FUNC BOOL SET_BEVERLY_PLAY_POINT_ANIM(ENTITY_INDEX entityTarget)
	IF IS_PED_UNINJURED(sBeverlyPed.ped)
	AND IS_ENTITY_ALIVE(entityTarget)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM -check for playing current anim : ", tlAnimName_BeverlyPoint) ENDIF #ENDIF
		IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
			VECTOR vBeverly_Forward, vTargetEntity_ToBeverly
			vBeverly_Forward = GET_ENTITY_FORWARD_VECTOR(sBeverlyPed.ped)
			vBeverly_Forward = NORMALISE_VECTOR(vBeverly_Forward)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM : vBeverly_Forward = ", vBeverly_Forward) ENDIF #ENDIF			
			vTargetEntity_ToBeverly = GET_ENTITY_COORDS(entityTarget) - GET_ENTITY_COORDS(sBeverlyPed.ped)
			vTargetEntity_ToBeverly = NORMALISE_VECTOR(vTargetEntity_ToBeverly)			
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM : vTargetEntity_ToBeverly = ", vTargetEntity_ToBeverly) ENDIF #ENDIF
			FLOAT fAngle
			fAngle = DOT_PRODUCT(vBeverly_Forward, vTargetEntity_ToBeverly)
			//fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vBev_Forward.X, vBev_Forward.Y, vRivalPap_ToPlayer.X, vRivalPap_ToPlayer.Y)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM : fAngle = ", fAngle) ENDIF #ENDIF
			INT iSide = GET_SIDE_COORDS_IS_TO_ENTITY(sBeverlyVehicle.vehicle, GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), GET_ENTITY_COORDS(entityTarget))
			TEXT_LABEL_63 tlTempSide
			TEXT_LABEL_63 tlAnimName
			IF iSide = 1 // Right side
				tlTempSide = "right_pap"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - right anim") ENDIF #ENDIF
			ELSE		// Left side or inline
				tlTempSide = "left_pap"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - left anim") ENDIF #ENDIF
			ENDIF
			IF fAngle > 0.85	// forward
				tlAnimName = "point_fwd_"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - forward anim") ENDIF #ENDIF
			ELIF fAngle > 0.2	// in front 45 degree area
				tlAnimName = "point_45_"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - 45 degree anim") ENDIF #ENDIF
			ELIF fAngle > -0.6	// to the side
				tlAnimName = "point_side_"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - side anim") ENDIF #ENDIF
			ELSE	// behind
				tlAnimName = "point_bwd_"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - behind anim") ENDIF #ENDIF
			ENDIF			
			tlAnimName += tlTempSide	// add the side to the anim name	
			tlAnimName_BeverlyPoint = tlAnimName
			//CLEAR_PED_TASKS(sBeverlyPed.ped)
			TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_HOLD_LAST_FRAME) // AF_HOLD_LAST_FRAME added to fix bug 1095914
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - anim applied - RETURN TRUE") ENDIF #ENDIF
			RETURN TRUE
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - anim already playing - RETURN TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_POINT_ANIM - RETURN FALSE") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles Beverly playing come on anims
/// PARAMS:
///    sSpecificAnim - specify an anim to play, otherwise pass NULL for random selected
///    fBlendInDelta - speed to blend the anim in
///    fBlendOutDelta - speed to blend the anim out
///    nTimeToPlay - the time the animation has to play
///    AnimFlags
///    startPhase = which phase in the anim to start from
///    phaseControlled
///    ikFlags
/// RETURNS:
///    TRUE if the anim was tasked successfully
FUNC BOOL SET_BEVERLY_PLAY_COME_ON_ANIM(STRING sSpecificAnim = NULL, FLOAT fBlendInDelta = NORMAL_BLEND_IN, FLOAT fBlendOutDelta = SLOW_BLEND_OUT, INT nTimeToPlay =-1, ANIMATION_FLAGS AnimFlags = AF_DEFAULT, FLOAT startPhase = 0.0, BOOL phaseControlled = FALSE, IK_CONTROL_FLAGS ikFlags = AIK_NONE)
	IF IS_PED_UNINJURED(sBeverlyPed.ped)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM -check for playing current anim : ", tlCurrentAnim_BeverlyShoutComeOn) ENDIF #ENDIF
		IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
			IF IS_STRING_NULL_OR_EMPTY(sSpecificAnim)
				// pick a random anim to play
				INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
				IF iRand = 0
					sSpecificAnim = "waive_comeback_b"
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - set random anim : ", sSpecificAnim) ENDIF #ENDIF
				ELIF iRand = 1
					sSpecificAnim = "waive_comeback_f"
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - set random anim : ", sSpecificAnim) ENDIF #ENDIF
				ELIF iRand = 2
					sSpecificAnim = "waive_help_a"
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - set random anim : ", sSpecificAnim) ENDIF #ENDIF
				ELIF iRand = 3
					sSpecificAnim = "waive_help_b"
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - set random anim : ", sSpecificAnim) ENDIF #ENDIF
				ENDIF
			ENDIF
			IF GET_LENGTH_OF_LITERAL_STRING(sSpecificAnim) > 23
				SCRIPT_ASSERT("RC - Pap 1 : SET_BEVERLY_PLAY_COME_ON_ANIM() sSpecificAnim length more than 23 chars")
			ENDIF
			tlCurrentAnim_BeverlyShoutComeOn = sSpecificAnim
			//CLEAR_PED_TASKS(sBeverlyPed.ped)
			TASK_PLAY_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn, fBlendInDelta, fBlendOutDelta, nTimeToPlay, AnimFlags, startPhase, phaseControlled, ikFlags)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - anim applied : ", tlCurrentAnim_BeverlyShoutComeOn, " : RETURN TRUE") ENDIF #ENDIF
			RETURN TRUE
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - anim already playing - RETURN TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_PLAY_COME_HERE_ANIM - RETURN FALSE") ENDIF #ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    set the rival pap playing the gesture anim
/// RETURNS:
///    true if anim was applied successfully, or if anim was already playing
FUNC BOOL SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
	IF IS_PED_UNINJURED(sRivalPapPed.ped)
		IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
		AND NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")	
			IF NOT IS_ENTITY_IN_ARC_2D(sRivalPapPed.ped, PLAYER_PED_ID(), 85.0)	// only play the anim if the player is behind the rival pap
				INT iSide = GET_SIDE_COORDS_IS_TO_ENTITY(sRivalPapVehicle.vehicle, GET_ENTITY_COORDS(sRivalPapVehicle.vehicle), GET_ENTITY_COORDS(PLAYER_PED_ID()))
				STRING sGestureAnim
				IF iSide = 1 // RIGHT SIDE
					sGestureAnim = "gesture_behind_right_pap"
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_RIVAL_PAP_PLAY_GESTURE_ANIM - gesture_behind_right_pap set") ENDIF #ENDIF
				ELSE
					sGestureAnim = "gesture_behind_left_pap"
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_RIVAL_PAP_PLAY_GESTURE_ANIM - gesture_behind_left_pap set") ENDIF #ENDIF
				ENDIF					
				//CLEAR_PED_TASKS(sRivalPapPed.ped)
				CLEAR_PED_SECONDARY_TASK(sRivalPapPed.ped)
				TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, sGestureAnim, NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)	// , AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_RIVAL_PAP_PLAY_GESTURE_ANIM - anim applied , return TRUE") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ELSE	
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_RIVAL_PAP_PLAY_GESTURE_ANIM - anim already playing , return TRUE") ENDIF #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    trigger an ambient line of dialogue from the rival pap
/// RETURNS:
///    TRUE if PLAY_PED_AMBIENT_SPEECH_WITH_VOICE was called, still no guaranteed to play
FUNC BOOL PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE()
	IF IS_PED_UNINJURED(sRivalPapPed.ped)
		IF NOT IS_SCRIPTED_SPEECH_PLAYING(sRivalPapPed.ped)
		AND NOT IS_AMBIENT_SPEECH_PLAYING(sRivalPapPed.ped)			
			// Fuck you, Beverly!
			// Get away from me!  
			// Are you crazy?
			// You're a loser!  
			// Look at yourself, it's tragic!
			// Are you this desperate?  
			// You're not cut out for this, Beverly!
			// Stop trying to play with the big boys!  
			// Touch me and you're dead!
			// What is wrong with you?  
			//code generate the variation when using PLAY_PED_AMBIENT_SPEECH_WITH_VOICE so just pass in the context - "PAP1_RBIKE"
			PAP1_PLAY_TRIGGER_AMBIENT_SPEECH(sRivalPapPed.ped, "PAP1_DIAA", "Paparazzo1RivalPap", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)	// B*1380212 - request to switch speech param from SPEECH_PARAMS_SHOUTED
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE - PAP1_RBIKE aka PAP1_DIAA  return TRUE this frame :", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE - scripted or ambient speech in progress return FALSE this frame :", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    sets the initial values for all the variables used on the mission
///    also sets up relationship groups
PROC SET_MISSION_VARIABLES()
	INT i
	bDoneDialogue_InitialGetOnBike						= FALSE
	bDoDelayForPhotoTaken								= FALSE
	bDoneRivalPapFlee							= FALSE
	bDoneRivalPapSurrendered							= FALSE
	bDoneDialogue_InitialChaseDialogue					= FALSE
	FOR i = 0 TO (TOTAL_TOOK_PHOTO_DIALOGUE - 1)
		bDoneDialogue_BeverlyTookPhoto[i] = FALSE
	ENDFOR
	FOR i = 0 TO (TOTAL_CHASE_RIVAL_PAP_DIALOGUE - 1)
		bDoneDialogue_RivalPapTookPhoto[i] = FALSE
	ENDFOR
	bDoneObjective_GetCloseToCeleb						= FALSE
	bDoneObjective_StopTheRivalPaparazzi				= FALSE
	bDoneObjective_DropBeverlyOff						= FALSE
	bDoneObjective_ReturnToBeverly						= FALSE
	bDoneObjective_GetBackOnBike						= FALSE
	bDoneObjective_GetOnBike							= FALSE
	bDoneObjective_LoseTheCops							= FALSE
	bDoneRivalPapRemovedFromDialogue					= FALSE
	bDoneDialogue_BeverlySayKeepBike					= FALSE
	bDoneDialogue_RivalPapCameraDestroyed				 = FALSE
	bDoneDialogue_DropOffBeverly						= FALSE
	bDoneDialogue_DropOffBeverlyAdditionalChat			= FALSE
	bDoneDialogue_LoseTheCops_InitialChat				= FALSE
	bDoneWaitForCameraDestroyedDialogue					= FALSE
	bDoneSetupCelebForCleanup							= FALSE
	bDoneHintCamForRival								= FALSE
	bDoneDialogue_FailWarningAttackMiranda				= FALSE
	bDoneDialogue_BeverlyMissedKnockOffAttempt 			= TRUE	//needs to be true initially so it can be set to false when it needs to be triggered
	bDoneDialogue_BeverlyThrowsPunch					= TRUE	//needs to be true initially so it can be set to false when it needs to be triggered
	bDoneDialogue_PlayerPullsWheelie					= FALSE
	bDoneDialogue_MissionFailed							= TRUE	//initialise to TRUE and only set to false when we have dialogue which will need to play
	bDoneDialogue_FranklinRespondToBeverlyBerating		= TRUE	// initialised to TRUE and only set to false when we want him to say a reply
	bHasBeverlyBeenRemovedFromPlayerGroup				= FALSE
	bPausedDialogue_DropOffBeverly						= FALSE
	bStatTracker_HasBeverlyMissedWithSwing				= FALSE
	bHasPlayerShotRivalPapOffBike						= FALSE
	bHadToFadeOutToLoadMissionAssets					= FALSE
	bDoneSetupRivalPapDriverToActInjured				= FALSE
	bDone_KnockOff_Biker_SwipeSoundFX					= FALSE
	bAreEntitiesHeadingsCompleteOpposite_KO_Biker 		= FALSE
	bDone_ShowSpecialAbilityHelp						= FALSE
	bHasPlayerGotOnBike_FirstTime						= FALSE
	bBeverly_SpottedRivalPap_Anim						= FALSE
	bBeverly_InitialPointAtCeleb_Anim					= FALSE
	bHasSetExitStateFromIntroMocapForGameplayCam		= FALSE
	bPlayerNeedsHelmentRestoreAfterOutroMocap			= FALSE
	bRequestedOutroMocap								= FALSE
	bCheckForPlayerHelmetChange							= FALSE
	eP1_MissionFailedReason								= FAILED_DEFAULT		//used in Script_Failed() to see if a fail reason needs displaying
	ePlayerTransportState								= PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE
	fCurrentChaseDistanceCeleb							= 0.0
	fRivalBikeSpeed										= 45.0
	fCurrentPlaybackTime								= 0.0
	fMainPlaybackSpeed									= 1.0  //0.85  //trial to slow if down *
	fKnockOff_Biker_UpdatedAnimSpeed 					= 1.0
	fKnockOff_Biker_InitialAnimSpeed					= 1.0
	fKnockOff_Biker_Anim_TotalTime						= 0.0
	fTimeInRivalPapRecording 							= 0.0	
	fDistance_PlayerToBeverly							= 0.0
	fKnockOff_Biker_HitExecutionArea_Length				= KNOCKOFF_BIKER_ATTACK_HIT_EXECUTION_ZONE_WIDTH
	fKnockOff_Biker_StartAnimWidthModifier				= 0.8
	iCheckForCelebtVehicleHornTimer						= 0
	iCelebVehicleHornTime								= 0
	iGoodPhotosTakenCounter								= -1
	iCheckChaseSpeedTimer								= 0
	iTimerIncreaseCelebSpeedForPhotoTaken				= -1
	iNumFlashesForRivalCamera							= 0
	iRivalTakesPhotosBurst								= 0
	iRivalPapCameraFlashTimer							= 0
	iKnockOff_Biker_Attack_Side							= KNOCKOFF_BIKER_ATTACK_SIDE_NULL
	iKnockOff_Biker_Attack_AnimSide						= KNOCKOFF_BIKER_ATTACK_SIDE_NULL
	iPreLoadRecordingsTimer								= 0
	iSoundId_BikeAttack_MeleeHit						= GET_SOUND_ID()
	iSoundId_BikeAttack_MeleeSwipe						= GET_SOUND_ID()
	iSoundId_CameraFlash								= GET_SOUND_ID()
	iDialogueTimer_PlayerOffTheBike						= 0
	iDialogueTimer_BevLeftBehind						= 0
	iDialogueTimer_TakingLongTimeToReachDropOffPoint	= 0
	iDialogueTimer_RivalPapAmbientLines					= 0
	iDialogueTimer_ChasingRivalPaparazzi				= 0
	iDialogueTimer_ChasingCeleb							= 0
	iDialogueTimer_LoseTheCopsAdditionalChat			= 0
	iTimer_DelayStopPlayerVehicleRecording_ForMissionStageSkip = 0
	iMissionMusicEventTriggerCounter					= PAP1_MUSIC_EVENT_MAX
	iTimer_DelayResetAudioFlag_AllowScoreAndRadio		= 0
	iTimerTillRepeatBevFrustratedAnim					= 0
	iConsecutiveAimingTimer								= -1
	iShapeTestCamerAimIndex_SetupThisFrame				= 0
	iFrameCountSinceLastSuccessfulShapeTestCameraAim	= 0
	iTimerDelay_Progress_BevTakingPhotos				= 0
	iTimerDelay_RivalPapGestureAnim						= 0
	iTimer_DelayBeverlyInitialTurnToFaceFranklin		= 0
	iTimer_BeverlyBikeNotOnAllWheelsTimer				= GET_GAME_TIMER()
	iCounter_BeverlyPhotoBurst							= 0
	iDelay_BeverlyPhotoBurst							= 500
	iMax_BeverlyPhotoBurst								= GET_RANDOM_INT_IN_RANGE(4, 7)
	iFrameCountBeverlyLastSeenPlayer					= 0
	iStoredPlayerHelmetPropTextureIndex					= 0
	iStoredPlayerHelmetPropIndex						= 0
	iDialogueLines_RivalPapCameraWrecked				= 0
	iScenarioBlockingAreaStage							= 0
	iIntroFocusPushStage 								= 0
	iIntroFocusPushTimer 								= -1
	iDelayInitialChaseComment							= 0
	eKnockOffBikerAttackState							= KBAS_SETUP_ATTACK
	eBeverlyTakePhotoState								= PAP1_BTPS_SETUP
	ePedShootingUpProgress								= PAP1_PED_SHOOTING_UP_SETUP
	eRivalPapOnBikeProgress								= PAP1_RIVAL_PAP_ON_BIKE_TRIGGER_JOIN_CHASE
	tlPausedDialogue_DropOffBeverly 					= ""
	tlDialogueRoot_CameraDestroyed 						= ""
	tlDialogueRoot_MissionFailed						= ""
	vBevCameraLensOffset								= << 0.070, -0.4, 0.0 >>
	vCelebPosInCarOffset								= << 0.0, -2.4, 0.3 >> // central to car to allow probe test radius to match car width // << 0.35, -2.4, 0.3 >> // correct for the actual position of the celeb in the rear passenger side seat
	//used to create areas either side of the celeb vehicle where photos can be taken
	vTakePhotoZones_CentrePosOffset[0]					= << 3.0, -0.5, 0.0 >>
	vTakePhotoZones_CentrePosOffset[1] 					= << -3.0, -0.5, 0.0 >>
	vTakePhotoZones_CentrePos[0] 						= << 0.0, 0.0, 0.0 >>
	vTakePhotoZones_CentrePos[1] 						= << 0.0, 0.0, 0.0 >>
	vPathNodeRequestMin									= << 0.0, 0.0, 0.0 >>
	vPathNodeRequestMax									= << 0.0, 0.0, 0.0 >>
	vPathNodeRequestStartPosition						= << 0.0, 0.0, 0.0 >>	// script checks the << 0.0, 0.0, 0.0 >> status when setting the first time 
	//new ko position stuff ***	
	vKnockOff_Biker_HitExecutionArea_OffsetFar[KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT]	= << -1.2, 0.4, 1.5 >> 
	vKnockOff_Biker_HitExecutionArea_OffsetFar[KNOCKOFF_BIKER_ATTACK_SIDE_LEFT]		= << 1.2, 0.4, 1.5 >>
	vKnockOff_Biker_HitExecutionArea_OffsetNear[KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT]	= << 0.0, 0.4, -1.0 >>
	vKnockOff_Biker_HitExecutionArea_OffsetNear[KNOCKOFF_BIKER_ATTACK_SIDE_LEFT]	= << 0.0, 0.4, -1.0 >>		
	vKnockOff_Biker_HitExecutionArea_PosFar											= << 0.0, 0.0, 0.0 >>
	vKnockOff_Biker_HitExecutionArea_PosNear										= << 0.0, 0.0, 0.0 >>
	vRivalPapCam_CutsceneTriggerPos 					= << -1467.11, -103.44, 49.87 >>
	vRivalPapDriver_InitialFleeDrivePoint 				= << -924.0120, -142.9705, 36.7612 >>	// pos further away - <<-603.5562, -11.5857, 42.4863>>
	vDropOffPoint										= << -1493.37524, -196.88408, 49.39753 >> // << -1491.551270,-199.071838,49.397408 >>
	vCameraFlashFX_Offset								= << -0.04, -0.06, 0.0 >>
	//NOTE: for use with check photo anim only!
	vClearZone_DropOffPoint_Min							= << -1525.48, -223.51, 45.0 >>
	vClearZone_DropOffPoint_Max							= << -1482.88, -184.36, 59.0 >>		
	tlAnimDict_BeverlyShoutComeOn 						= "rcmpaparazzo1ig_1_waive"
	tlCurrentAnim_BeverlyShoutComeOn 					= "waive_comeback_f"
	sBeverlyVehicle.model = PCJ   //  GET_NPC_VEH_MODEL(CHAR_BEVERLY) //  FAGGIO2 is too slow
	sBeverlyVehicle.vSpawn = << -159.56, 275.41, 93.14 >> // pos change to use cutscene << -164.26, 275.12, 92.95 >>
	sBeverlyVehicle.fSpawnHeading = 102.75	// pos change to use cutscene 92.32	
	sBeverlyPed.model = GET_NPC_PED_MODEL(CHAR_BEVERLY)
	sBeverlyPed.vSpawn = << -159.49, 276.85, 93.66 >> // pos change to use cutscene
	sBeverlyPed.fSpawnHeading = -74.01 // pos change to use cutscene
	sCelebVehicle.model = STRETCH              
	sCelebVehicle.vSpawn = << -177.404144,266.726929,92.407616 >>
	sCelebVehicle.fSpawnHeading = 85.4595
	sCelebPed.model = U_F_M_MIRANDA
	sCelebPed.vSpawn = << 0.0, -170.05, 284.88 >>
	sCelebPed.fSpawnHeading = -172.46	
	sCelebsDriverPed.model = S_M_M_HighSec_01	
	sRivalPapVehicle.model = PCJ	//BATI
	sRivalPapVehicle.vSpawn = << -1585.9142, -133.9717, 55.1026 >>
	sRivalPapVehicle.fSpawnHeading = 0.0	//unknown	
	sRivalPapPed.model = U_M_Y_PAPARAZZI
//	sRivalPapPed.vSpawn = << -152.6825, 286.6073, 92.7637 >>
//	sRivalPapPed.fSpawnHeading = 205.9674
	sRivalPapDriverPed.model = U_M_M_RIVALPAP
	sObjCameraBeverly.model = PROP_PAP_CAMERA_01	
	sObjCameraBeverly.vOffset = << 0.0, 0.0, 0.0 >>
	sObjCameraBeverly.vRotation = << 0.0, 0.0, 0.0 >>
	sObjCameraBeverly.vPosition = << 0.0, 0.0, 0.0 >>	
	sObjFlashUnitBeverly.model = PROP_FLASH_UNIT
	sObjFlashUnitBeverly.vOffset = << 0.107, 0.025, 0.177 >>
	sObjFlashUnitBeverly.vRotation = << 0.0, 0.0, 0.0 >>
	sObjFlashUnitBeverly.vPosition = << 0.0, 0.0, 0.0 >>
	sObjCameraRivalPap.model = PROP_PAP_CAMERA_01
	sObjCameraRivalPap.vOffset = << 0.0, 0.0, 0.0 >>	
	sObjCameraRivalPap.vRotation = << 0.0, 0.0, 0.0 >>
	sObjFlashUnitRivalPap.model = PROP_FLASH_UNIT
	sObjFlashUnitRivalPap.vOffset = << 0.107, 0.025, 0.177 >>
	sObjFlashUnitRivalPap.vRotation = << 0.0, 0.0, 0.0 >>
	sObjFlashUnitRivalPap.vPosition = << 0.0, 0.0, 0.0 >>
	sObjSyringe.model = PROP_SYRINGE_01
	sObjSyringe.vOffset = << 0.0, 0.0, 0.0 >>
	sObjSyringe.vRotation = << 0.0, 0.0, 0.0 >>	
	ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemy)
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		relGroupFriendly= GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID()) //ADD_RELATIONSHIP_GROUP("FRIENDLIES", relGroupFriendly)
	ENDIF
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemy, relGroupFriendly)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupFriendly, relGroupEnemy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupFriendly, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupFriendly)		
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sBeverlyVehicle.model, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sCelebVehicle.model, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sRivalPapVehicle.model, TRUE)
ENDPROC

/// PURPOSE:
///    Initialization of the mission
PROC INIT_MISSION()
	SET_MISSION_VARIABLES()	
	#IF IS_DEBUG_BUILD		
		SETUP_MISSION_WIDGET()	//this needs setting up first to ensure debug print can be displayed when loading assets
		// stage skipping
	    mSkipMenu[Z_SKIP_INTRO].sTxtLabel = "Mocap: PAP_1_RCM"    
	    mSkipMenu[Z_SKIP_CHASE_CELEB_IN_VEHICLE].sTxtLabel = "Chase Miranda"     
		mSkipMenu[Z_SKIP_ENTER_RIVAL_PAP].sTxtLabel = "Rival pap joins the chase" 
		mSkipMenu[Z_SKIP_STOP_RIVAL_PAP].sTxtLabel = "Stop the rival paparazzi" 
	    mSkipMenu[Z_SKIP_DROP_BEVERLY_OFF].sTxtLabel = "Drop Beverly off" 
		mSkipMenu[Z_SKIP_OUTRO].sTxtLabel = "Mocap: PAP_1_MCS_1" 
		mSkipMenu[Z_SKIP_MISSION_PASSED].sTxtLabel = "Mission Passed"
	#ENDIF
	SET_WANTED_LEVEL_MULTIPLIER(0.2)	
	//ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)	
	REGISTER_SCRIPT_WITH_AUDIO()	
	SET_AGGRESSIVE_HORNS(TRUE)	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUS, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(COACH, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(MULE, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(MULE2, TRUE)
	DISABLE_TAXI_HAILING(TRUE)		// don't allow taxis on mission
	//remove stuff we don't want around the carpark drop off point - vehicles, peds, objects, blood decals
	SET_SCENARIO_GROUP_ENABLED("ATTRACT_PAP", FALSE)	// vehicle park up chain at the drop off point
	SET_ROADS_IN_ANGLED_AREA(<<-1514.624146,-211.543411,46.786194>>, <<-1487.885254,-190.754852,59.494499>>, 25.000000, FALSE, FALSE)	// drop off area
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)
	CLEAR_AREA(vDropOffPoint, 15.0, TRUE)
	CLEAR_AREA_OF_PEDS(vDropOffPoint, 15.0)	
	CLEAR_AREA_OF_VEHICLES(vDropOffPoint, 15.0)	
ENDPROC

/// PURPOSE:
///    handles the assets for the rival pap's arrival
/// PARAMS:
///    bWaitForAssetsToLoad - if true game waits for all requests to be loaded
PROC LOAD_ASSETS_FOR_RIVAL_PAP_ARRIVING(BOOL bWaitForAssetsToLoad = TRUE)	
	REQUEST_MODEL(sObjCameraRivalPap.model)
	REQUEST_MODEL(sObjFlashUnitRivalPap.model)
	REQUEST_MODEL(sRivalPapPed.model)
	REQUEST_MODEL(sRivalPapDriverPed.model)
	REQUEST_MODEL(sRivalPapVehicle.model)	
	REQUEST_VEHICLE_RECORDING(1, "Pap1RP")	//load rival pap's vehicle recording
	REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_01_A")	//road rash attack sfx
	
	IF bWaitForAssetsToLoad	
		WHILE NOT HAS_MODEL_LOADED(sObjCameraRivalPap.model)	
		OR NOT HAS_MODEL_LOADED(sObjFlashUnitRivalPap.model)	
		OR NOT HAS_MODEL_LOADED(sRivalPapPed.model)
		OR NOT HAS_MODEL_LOADED(sRivalPapDriverPed.model)
		OR NOT HAS_MODEL_LOADED(sRivalPapVehicle.model)		
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Pap1RP")		
		OR NOT REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_01_A")		
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "waiting for asset to load - rival pap   ") ENDIF #ENDIF
			WAIT(0)			
		ENDWHILE
	ENDIF
ENDPROC

/// PURPOSE:
///    handles all the mission asset requests
/// PARAMS:
///    bWaitForAssetsToLoad - if true game waits for all requests to be loaded
PROC LOAD_MISSION_ASSETS(BOOL bWaitForAssetsToLoad = FALSE)	
	REQUEST_ADDITIONAL_TEXT("PAP1", MISSION_TEXT_SLOT)	
	REQUEST_VEHICLE_RECORDING(1, sPap1_UberRecordingName)		//main uber recording
	REQUEST_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)	//flash sound fx	
	REQUEST_MODEL(sBeverlyPed.model)		// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now
	REQUEST_MODEL(sBeverlyVehicle.model)	// now handled in CREATE_BEVERLYS_BIKE_FOR_MISSION_START()
	REQUEST_MODEL(sCelebPed.model)
	REQUEST_MODEL(sCelebsDriverPed.model)
	REQUEST_MODEL(sCelebVehicle.model)
	REQUEST_MODEL(sObjCameraBeverly.model)	// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now
	REQUEST_MODEL(sObjFlashUnitBeverly.model)	// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now	
	REQUEST_PTFX_ASSET()	//needed for the camera flash fx	
	REQUEST_VEHICLE_ASSET(sBeverlyVehicle.model)	//entry / exit anims for Beverly's bike...needed otherwise Beverly will warp on/off it	
	REQUEST_ANIM_DICT(sAnimDictName_Pap1Specific)	//contains new pap attack anims, new camera script aim gun task anims, rival pap hit reactions, dialogue on bike anims
	REQUEST_ANIM_DICT(sAnimDictName_Pap1HandOverride)	// anim dict containing camera in hand override
	REQUEST_CLIP_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(TRUE))		// anim clipset containing Bev get on bike left side override
	REQUEST_CLIP_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(FALSE))	// anim clipset containing Bev get on bike right side override
	REQUEST_ANIM_DICT(tlAnimDict_BeverlyShoutComeOn) // contains Beverly encourage player to hurry up, come on, come back anims
	REQUEST_ANIM_SET(sAnimSet_BeverlyCameraWeaponMovementOnFoot)	// pap holding camera movement	
	//wait until loaded
	IF bWaitForAssetsToLoad		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)		
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, sPap1_UberRecordingName)
		OR NOT REQUEST_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)		
		OR NOT HAS_MODEL_LOADED(sBeverlyPed.model)	// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now
		OR NOT HAS_MODEL_LOADED(sBeverlyVehicle.model)
		OR NOT HAS_MODEL_LOADED(sCelebPed.model)
		OR NOT HAS_MODEL_LOADED(sCelebsDriverPed.model)
		OR NOT HAS_MODEL_LOADED(sCelebVehicle.model)
		OR NOT HAS_MODEL_LOADED(sObjCameraBeverly.model)	// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now	
		OR NOT HAS_MODEL_LOADED(sObjFlashUnitBeverly.model)	// NOW CREATED IN MOCAP		however not getting registered for some reason so keep request for now		
		OR NOT HAS_PTFX_ASSET_LOADED()			
		OR NOT HAS_VEHICLE_ASSET_LOADED(sBeverlyVehicle.model)
		OR NOT HAS_ANIM_DICT_LOADED(sAnimDictName_Pap1Specific)	//contains new pap attack anims, new camera script aim gun task anims, rival pap hit reactions, dialogue on bike anims
		OR NOT HAS_ANIM_DICT_LOADED(sAnimDictName_Pap1HandOverride)	// anim dict containing camera in hand override	
		OR NOT HAS_ANIM_SET_LOADED(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(TRUE))		// anim clipset containing Bev get on bike left side override
		OR NOT HAS_ANIM_SET_LOADED(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(FALSE))	// anim clipset containing Bev get on bike right side override
		OR NOT HAS_ANIM_DICT_LOADED(tlAnimDict_BeverlyShoutComeOn) // contains Beverly encourage player to hurry up, come on, come back anims
		OR NOT HAS_ANIM_SET_LOADED(sAnimSet_BeverlyCameraWeaponMovementOnFoot)	// pap holding camera movement
			IF bFinishedStageSkipping	//don't fade if we are stage skipping
				IF NOT IS_SCREEN_FADED_OUT()
					SAFE_FADE_SCREEN_OUT_TO_BLACK(0, TRUE)
					bHadToFadeOutToLoadMissionAssets = TRUE	//stored so I only fade back up if, I faded down
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SCREEN FADED BECAUSE ASSETS WEREN'T LOADED IN TIME") ENDIF #ENDIF
				ENDIF
			ENDIF
			WAIT(0)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "waiting for asset to load   ") ENDIF #ENDIF
		ENDWHILE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION ASSETS LOADED - GO GO GO   ") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up Beverly's bike for the start of the mission.
///    uses the scene setup entity if it's available, else creates a new entity.
///    This has to be done seperately since you can see the bike jump into position during the mocap otherwise.
PROC CREATE_BEVERLYS_BIKE_FOR_MISSION_START()
	sBeverlyVehicle.model = BEVERLY_BIKE	// get the model handle used in the initial scene, which keeps the model in memory for me
	IF NOT DOES_ENTITY_EXIST(sBeverlyVehicle.vehicle)
		CLEAR_AREA(sBeverlyVehicle.vSpawn, 2.0, TRUE)
		REQUEST_MODEL(sBeverlyVehicle.model)	// should already be in memory from the launcher
		IF HAS_MODEL_LOADED(sBeverlyVehicle.model)
			sBeverlyVehicle.vehicle = CREATE_VEHICLE(sBeverlyVehicle.model, sBeverlyVehicle.vSpawn, sBeverlyVehicle.fSpawnHeading)
		ELSE
			SCRIPT_ASSERT("PAPARAZZO 1 - CREATE_BEVERLYS_BIKE_FOR_MISSION_START - sBeverlyVehicle.model isnt loaded")
		ENDIF
	ENDIF
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[0])
	IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
		SET_VEHICLE_FIXED(sBeverlyVehicle.vehicle)
		SET_ENTITY_COORDS(sBeverlyVehicle.vehicle, sBeverlyVehicle.vSpawn)
		SET_ENTITY_HEADING(sBeverlyVehicle.vehicle, sBeverlyVehicle.fSpawnHeading)
		SET_VEHICLE_DOORS_LOCKED(sBeverlyVehicle.vehicle, VEHICLELOCK_UNLOCKED)
		SET_VEHICLE_ON_GROUND_PROPERLY(sBeverlyVehicle.vehicle)
		SET_VEHICLE_COLOURS(sBeverlyVehicle.vehicle, 62, 62) //BLUE
		SET_VEHICLE_CAN_LEAK_OIL(sBeverlyVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sBeverlyVehicle.vehicle, FALSE)	
		//SET_VEHICLE_TYRES_CAN_BURST(sBeverlyVehicle.vehicle, FALSE)
		SET_VEHICLE_STRONG(sBeverlyVehicle.vehicle, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(sBeverlyVehicle.vehicle, TRUE)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(sBeverlyVehicle.model)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_BEVERLYS_BIKE_FOR_MISSION_START done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    sets up the celeb vehicle for the start of in game play
PROC SETUP_CELEB_VEHICLE_FOR_IN_GAME_START()
	IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
		SET_VEHICLE_FIXED(sCelebVehicle.vehicle)		
		SET_VEHICLE_DOORS_LOCKED(sCelebVehicle.vehicle, VEHICLELOCK_UNLOCKED)					
		SET_VEHICLE_ENGINE_ON(sCelebVehicle.vehicle, TRUE, TRUE)
		SET_VEHICLE_COLOURS(sCelebVehicle.vehicle, 0, 0) //black
		SET_VEHICLE_TYRES_CAN_BURST(sCelebVehicle.vehicle, FALSE)
		SET_VEHICLE_STRONG(sCelebVehicle.vehicle, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(sCelebVehicle.vehicle, TRUE)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sCelebVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(sCelebVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sCelebVehicle.vehicle, FALSE)	
		SET_VEHICLE_DOOR_SHUT(sCelebVehicle.vehicle, SC_DOOR_FRONT_LEFT, TRUE)
		SET_VEHICLE_DOOR_SHUT(sCelebVehicle.vehicle, SC_DOOR_REAR_RIGHT, TRUE)			// Fix B*1134065 - door swinging open (think cutscene doesn't shut it again) 
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sCelebVehicle.vehicle, SC_DOOR_REAR_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(sCelebVehicle.vehicle, SC_DOOR_REAR_RIGHT, FALSE)
		SET_ENTITY_COORDS(sCelebVehicle.vehicle, sCelebVehicle.vSpawn)
		SET_ENTITY_QUATERNION(sCelebVehicle.vehicle, -0.0362, 0.0042, 0.6776, 0.7345)	// ensure update intro mocap where set cutscene exit state
		FREEZE_ENTITY_POSITION(sCelebVehicle.vehicle, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_CELEB_VEHICLE_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up Beverly for the start of in game play
PROC SETUP_BEVERLY_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sBeverlyPed.ped)
	
		SET_PED_COMPONENT_VARIATION(sBeverlyPed.ped, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0)	// set Beverly to not have his extra camera around his neck (as per bug 1034768) 
		
		// B*1409359 - ensure Beverly won't try to put a helmet on before his baseball cap gets set
		SET_PED_HELMET(sBeverlyPed.ped, FALSE)	
		SET_ENTITY_VISIBLE(sBeverlyPed.ped, TRUE)
		SET_PED_CAN_BE_TARGETTED(sBeverlyPed.ped, FALSE)
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sBeverlyPed.ped, KNOCKOFFVEHICLE_DEFAULT)
		ADD_ARMOUR_TO_PED(sBeverlyPed.ped, 100)
		SET_ENTITY_LOAD_COLLISION_FLAG(sBeverlyPed.ped, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sBeverlyPed.ped ,FALSE)
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_DontInfluenceWantedLevel, TRUE)	
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_RunFromFiresAndExplosions, TRUE)	
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_DontEnterVehiclesInPlayersGroup, TRUE)	//used to control which vehicles the ped will get into
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_UseKinematicModeWhenStationary, TRUE)	// B*1421480 - anims don't support avoid / bumped anims
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sBeverlyPed.ped, FALSE)
		SET_PED_DIES_IN_WATER(sBeverlyPed.ped, TRUE)
		SET_PED_DIES_IN_SINKING_VEHICLE(sBeverlyPed.ped, TRUE)
		SET_PED_MAX_TIME_IN_WATER(sBeverlyPed.ped, 2.0)
		SET_PED_MAX_TIME_UNDERWATER(sBeverlyPed.ped, 2.0)
		SET_PED_PATH_PREFER_TO_AVOID_WATER(sBeverlyPed.ped, TRUE)
		IF IS_PED_IN_GROUP(sBeverlyPed.ped)
			REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "*PED GROUP* BEV removed from any group at create ped stage") ENDIF #ENDIF
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sBeverlyPed.ped, relGroupFriendly)		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sBeverlyPed.ped, TRUE)
		ADD_PED_FOR_DIALOGUE(sDialogue, 3, sBeverlyPed.ped, "BEVERLY")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(sBeverlyPed.ped, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up celeb for the start of in game play
PROC SETUP_CELEB_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sCelebPed.ped)
		SET_PED_CAN_BE_TARGETTED(sCelebPed.ped, FALSE)
		ADD_ARMOUR_TO_PED(sCelebPed.ped, 100)
		SET_ENTITY_LOAD_COLLISION_FLAG(sCelebPed.ped, TRUE)
		SET_PED_SUFFERS_CRITICAL_HITS(sCelebPed.ped, FALSE)
		SET_PED_CAN_BE_SHOT_IN_VEHICLE(sCelebPed.ped, FALSE)
		SET_PED_CAN_BE_DRAGGED_OUT(sCelebPed.ped, FALSE)
		SET_PED_CONFIG_FLAG(sCelebPed.ped, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(sCelebPed.ped, PCF_DontInfluenceWantedLevel, TRUE)			
		IF IS_PED_IN_GROUP(sCelebPed.ped)
			REMOVE_PED_FROM_GROUP(sCelebPed.ped)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sCelebPed.ped, relGroupEnemy)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCelebPed.ped, TRUE)
		SET_PED_CONFIG_FLAG(sCelebPed.ped, PCF_GetOutUndriveableVehicle, FALSE)		// //stop ped leaving undriveable vehicle
		SET_PED_CONFIG_FLAG(sCelebPed.ped, PCF_GetOutBurningVehicle, FALSE)		// //stop ped leaving undriveable vehicle	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_CELEB_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    sets up celeb driver for the start of in game play
PROC SETUP_CELEB_DRIVER_FOR_IN_GAME_START()
	IF IS_PED_UNINJURED(sCelebsDriverPed.ped)
		SET_PED_CAN_BE_TARGETTED(sCelebsDriverPed.ped, FALSE)
		ADD_ARMOUR_TO_PED(sCelebsDriverPed.ped, 100)
		SET_PED_SUFFERS_CRITICAL_HITS(sCelebsDriverPed.ped, FALSE)
		SET_PED_CAN_BE_SHOT_IN_VEHICLE(sCelebsDriverPed.ped, FALSE)
		SET_PED_CAN_BE_DRAGGED_OUT(sCelebsDriverPed.ped, FALSE)
		SET_PED_CONFIG_FLAG(sCelebsDriverPed.ped, PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(sCelebsDriverPed.ped, PCF_DontInfluenceWantedLevel, TRUE)	
		IF IS_PED_IN_GROUP(sCelebsDriverPed.ped)
			REMOVE_PED_FROM_GROUP(sCelebsDriverPed.ped)
		ENDIF
		SET_PED_RELATIONSHIP_GROUP_HASH(sCelebsDriverPed.ped, relGroupEnemy)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCelebsDriverPed.ped, TRUE)
		SET_PED_CONFIG_FLAG(sCelebsDriverPed.ped, PCF_GetOutUndriveableVehicle, FALSE)		// //stop ped leaving undriveable vehicle
		SET_PED_CONFIG_FLAG(sCelebsDriverPed.ped, PCF_GetOutBurningVehicle, FALSE)		// //stop ped leaving undriveable vehicle	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_CELEB_DRIVER_FOR_IN_GAME_START done") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    deals with creating the peds, vehicles, objects etc
PROC CREATE_ENTITIES_FOR_MISSION_START()
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		ADD_PED_FOR_DIALOGUE(sDialogue, 1, PLAYER_PED_ID(), "FRANKLIN")
		SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	//Beverly	
	SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])	// Remove temporary RC character BEVERLY	
	IF NOT DOES_ENTITY_EXIST(sBeverlyPed.ped)
		sBeverlyPed.ped = CREATE_PED(PEDTYPE_MISSION, sBeverlyPed.model, sBeverlyPed.vSpawn, sBeverlyPed.fSpawnHeading)
		CPRINTLN(DEBUG_MISSION, "created beverly in script, not mocap ~~~~~	FC = ", GET_FRAME_COUNT())
		SETUP_BEVERLY_FOR_IN_GAME_START()
	ENDIF
	// Setup Beverly's camera and flash unit
	IF IS_PED_UNINJURED(sBeverlyPed.ped)		
		SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[0])	// remove camera from initial scene
		IF NOT DOES_ENTITY_EXIST(sObjCameraBeverly.object)
			sObjCameraBeverly.vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, << 0.0, 0.0, 2.5 >>)
			sObjCameraBeverly.object = CREATE_OBJECT(sObjCameraBeverly.model, sObjCameraBeverly.vPosition)
			CPRINTLN(DEBUG_MISSION, "CREATE_ENTITIES_FOR_MISSION_START - ObjCameraBeverly.object - created in script not mocap   FC = ", GET_FRAME_COUNT())
		ENDIF
		IF NOT DOES_ENTITY_EXIST(sObjFlashUnitBeverly.object)
			sObjFlashUnitBeverly.vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sBeverlyPed.ped, << 0.0, 0.0, 2.5 >>)
			sObjFlashUnitBeverly.object = CREATE_OBJECT(sObjFlashUnitBeverly.model, sObjFlashUnitBeverly.vPosition)
			CPRINTLN(DEBUG_MISSION, "CREATE_ENTITIES_FOR_MISSION_START - sObjFlashUnitBeverly.object - created in script not mocap   FC = ", GET_FRAME_COUNT())
		ENDIF
		IF IS_ENTITY_ALIVE(sObjCameraBeverly.object)
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(sObjFlashUnitBeverly.object, sObjCameraBeverly.object)
				ATTACH_ENTITY_TO_ENTITY(sObjFlashUnitBeverly.object, sObjCameraBeverly.object, 0, sObjFlashUnitBeverly.vOffset, sObjFlashUnitBeverly.vRotation)
				CPRINTLN(DEBUG_MISSION, "CREATE_ENTITIES_FOR_MISSION_START - sObjFlashUnitBeverly.object - ATTACHED to sObjCameraBeverly.object in script not mocap   FC = ", GET_FRAME_COUNT())
			ENDIF
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)
				ATTACH_ENTITY_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped, GET_PED_BONE_INDEX(sBeverlyPed.ped, BONETAG_PH_R_HAND), sObjCameraBeverly.vOffset, sObjCameraBeverly.vRotation)
				CPRINTLN(DEBUG_MISSION, "CREATE_ENTITIES_FOR_MISSION_START - sObjCameraBeverly.object - ATTACHED to sBeverlyPed.ped in script not mocap   FC = ", GET_FRAME_COUNT())
			ENDIF
			SET_ENTITY_VISIBLE(sObjCameraBeverly.object, TRUE)	//ADDED TO STOP VISIBLE SPAWNING IN ...GETS TURNED BACK ON IN MOCAP INTRO
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjCameraBeverly.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjFlashUnitBeverly.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sBeverlyPed.model)
	
	// Setup Celeb's Vehicle
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])	// celeb vehicle from initial setup
	IF NOT DOES_ENTITY_EXIST(sCelebVehicle.vehicle)
		CLEAR_AREA(sCelebVehicle.vSpawn, 2.0, TRUE)
		sCelebVehicle.vehicle = CREATE_VEHICLE(sCelebVehicle.model, sCelebVehicle.vSpawn, sCelebVehicle.fSpawnHeading)
		CPRINTLN(DEBUG_MISSION, "created Miranda's vehicle in script, not mocap	FC = ", GET_FRAME_COUNT())
		SETUP_CELEB_VEHICLE_FOR_IN_GAME_START()
	ENDIF
	
	// Setup Celeb ped and Celeb's driver
	IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
		IF NOT DOES_ENTITY_EXIST(sCelebPed.ped)
			sCelebPed.ped = CREATE_PED_INSIDE_VEHICLE(sCelebVehicle.vehicle, PEDTYPE_MISSION, sCelebPed.model, VS_BACK_RIGHT)
			CPRINTLN(DEBUG_MISSION, "created Miranda's in script, not mocap	FC = ", GET_FRAME_COUNT())
			SETUP_CELEB_FOR_IN_GAME_START()
		ENDIF
		IF NOT DOES_ENTITY_EXIST(sCelebsDriverPed.ped)
			sCelebsDriverPed.ped = CREATE_PED_INSIDE_VEHICLE(sCelebVehicle.vehicle, PEDTYPE_MISSION, sCelebsDriverPed.model)
			CPRINTLN(DEBUG_MISSION, "created Miranda's driver in script, not mocap	FC = ", GET_FRAME_COUNT())
			SETUP_CELEB_DRIVER_FOR_IN_GAME_START()
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebPed.model)	
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebsDriverPed.model)
	
	//back up fade in, if we had to fade out during loading of assets
	IF bFinishedStageSkipping	//however, don't fade if we are stage skipping
		IF bHadToFadeOutToLoadMissionAssets
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SCREEN FADED IN BY MY SCRIPT RATHER THAN MOCAP END		^^^^^^^^^^^") ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    make Miranda's vehicle sound it's horn when the player is close or hits it
/// PARAMS:
///    fChaseDistance - dist player from Miranda's vehicle
PROC MANAGE_CELEB_VEHICLE_USING_HORN(FLOAT fChaseDistance)
		
	IF HAS_TIME_PASSED(iCheckForCelebtVehicleHornTimer, iCelebVehicleHornTime)
		IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
			IF fChaseDistance < 4.0	// area close around Miranda's vehicle
			OR IS_ENTITY_TOUCHING_ENTITY(sCelebVehicle.vehicle, sBeverlyVehicle.vehicle)
				iCelebVehicleHornTime = GET_RANDOM_INT_IN_RANGE(1500, 3500)
				START_VEHICLE_HORN(sCelebVehicle.vehicle, iCelebVehicleHornTime, GET_HASH_KEY("HELDDOWN"))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CELEB_VEHICLE_USING_HORN - triggered, length = ", iCelebVehicleHornTime) ENDIF #ENDIF
				iCelebVehicleHornTime += 4000	// long wait before horn can be triggered again
				iCheckForCelebtVehicleHornTimer = GET_GAME_TIMER()				
				EXIT
			ENDIF
		ENDIF
		//don't do this check every frame
		iCelebVehicleHornTime = 250
		iCheckForCelebtVehicleHornTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles Miranda shooting up in the back of the limo
/// PARAMS:
///    pedIndex - the ped who will be playing the anims (miranda)
PROC MANAGE_PED_SHOOTING_UP(PED_INDEX pedIndex)
	IF IS_PED_UNINJURED(pedIndex)
		SWITCH ePedShootingUpProgress
			CASE PAP1_PED_SHOOTING_UP_SETUP
				REQUEST_MODEL(sObjSyringe.model)			
				IF HAS_MODEL_LOADED(sObjSyringe.model)
					IF NOT DOES_ENTITY_EXIST(sObjSyringe.object)
						sObjSyringe.vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedIndex, << 0.0, 0.0, 2.5 >>)
						sObjSyringe.object = CREATE_OBJECT(sObjSyringe.model, sObjSyringe.vPosition)
						ATTACH_ENTITY_TO_ENTITY(sObjSyringe.object, pedIndex, GET_PED_BONE_INDEX(pedIndex, BONETAG_PH_R_HAND), sObjSyringe.vOffset, sObjSyringe.vRotation)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_SHOOTING_UP - syringe attached") ENDIF #ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(sObjSyringe.object)
						SET_ENTITY_VISIBLE(sObjSyringe.object, TRUE)
						SET_ENTITY_INVINCIBLE(sObjSyringe.object, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(sObjSyringe.model)						
						REQUEST_ANIM_DICT("rcmpaparazzo1ig_4")	//ped shooting up in vehicle anim						
						ePedShootingUpProgress = PAP1_PED_SHOOTING_UP_UPDATE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_SHOOTING_UP - PAP1_PED_SHOOTING_UP_SETUP - done") ENDIF #ENDIF
					ENDIF					
				ENDIF
			BREAK
			CASE PAP1_PED_SHOOTING_UP_UPDATE
				IF NOT IS_ENTITY_PLAYING_ANIM(pedIndex, "rcmpaparazzo1ig_4", "miranda_shooting_up")
					REQUEST_ANIM_DICT("rcmpaparazzo1ig_4")	//ped shooting up in vehicle anim				
					IF HAS_ANIM_DICT_LOADED("rcmpaparazzo1ig_4")
						TASK_PLAY_ANIM(pedIndex, "rcmpaparazzo1ig_4", "miranda_shooting_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						REMOVE_ANIM_DICT("rcmpaparazzo1ig_4")	//ped shooting up in vehicle anim
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_SHOOTING_UP - PAP1_PED_SHOOTING_UP_UPDATE - anim applied") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PAP1_PED_SHOOTING_UP_CLEANUP
				IF NOT IS_ENTITY_PLAYING_ANIM(pedIndex, "rcmpaparazzo1ig_4", "miranda_shooting_up")
					STOP_ANIM_TASK(pedIndex, "rcmpaparazzo1ig_4", "miranda_shooting_up")
				ENDIF
				REMOVE_ANIM_DICT("rcmpaparazzo1ig_4")	//ped shooting up in vehicle anim
				SAFE_REMOVE_OBJECT(sObjSyringe.object, FALSE)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player uses the special ability
///    prior to the point I want to display the help text for it then turn off the bool for displaying the help text   
PROC CHECK_PLAYER_USED_SPECIAL_ABILITY()
	IF NOT bDone_ShowSpecialAbilityHelp
		IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())	
			CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("PAP1_H2")	// ~s~Hold ~INPUT_VEH_SPECIAL_ABILITY_FRANKLIN~ to activate overdrive mode, this will focus Franklin and improve his reflexes.~s~
			IF IS_PC_VERSION()
				CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("PAP1_H2_KM")	// ~s~Press ~INPUT_SPECIAL_ABILITY~ to activate Franklin's special ability.~s~
			ENDIF
			bDone_ShowSpecialAbilityHelp = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHECK_PLAYER_USED_SPECIAL_ABILITY - help text bool set TRUE") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC	

/// PURPOSE:
///    NEW uses UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING and CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR
///    Rubberbands the chase speed of Franklin to Miranda's limo
///    NOTE: having to override the standard uber recording behaviour a bit as the player needs to be able to get along side Miranda comfortably
/// PARAMS:
///    fCurrentChaseSpeed - the main chase speed which is to be updated
///    fChaseDistance - the current dist between Franklin and Miranda's limo
PROC NEW_UPDATE_CELEB_CHASE_SPEED(FLOAT &fCurrentChaseSpeed, FLOAT fChaseDistance)		
	//uber recording
	FLOAT fDesiredPlaybackSpeed = fCurrentChaseSpeed	
	FLOAT fLosingTriggerDist = (CELEB_ESCAPED_DIST / 100) * (PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE * 100)	// point where the flashing blip kicks in for escaping
	FLOAT fAccel = 0.1
	//IF bHasPlayerGotOnBike_FirstTime = FALSE		
	//	fDesiredPlaybackSpeed = 0.55
	//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player not on the bike first time <, fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
	
	//don't slow down when the rival pap peals off as we need to maintain speed here
	IF fCurrentPlaybackTime > 78000.0
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															9.5, 18.0, 125.0, (fLosingTriggerDist + 50), 50.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.85, 0.7, 1.25, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 15.0, TRUE)
		fAccel = 0.07
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fCurrentPlaybackTime > 78000.0 >, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
		
	//speed up towards the end of the chase to get back in front before the rival pap turns up
	ELIF fCurrentPlaybackTime > 54000.0
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															9.5, 17.0, 32.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.7, 0.5, 1.25, TRUE, (ABSF(BIKE_TOP_SPEED_MODIFIER) + 5), 20.0, TRUE)
		fAccel = 0.045
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fCurrentPlaybackTime > 54000.0 >, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF

	//begin speed up towards the end of the chase to get back in front before the rival pap turns up
	ELIF fCurrentPlaybackTime > 52500.0
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															6.5, 12.0, 32.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.7, 0.5, 1.15, TRUE, (ABSF(BIKE_TOP_SPEED_MODIFIER) + 2.5), 20.0, TRUE)
		fAccel = 0.045
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fCurrentPlaybackTime > 52500.0 >, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
	
	//begin speeding up towards the end of the chase to get back in front before the rival pap turns up
	ELIF fCurrentPlaybackTime > 51000.0
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															3.5, 6.0, 32.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.7, 0.5, 1.05, TRUE, (ABSF(BIKE_TOP_SPEED_MODIFIER)), 18.0, TRUE)
		fAccel = 0.040
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fCurrentPlaybackTime > 51000.0, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
		
	// take it really slow at the start of the chase to allow player time to get on the bike and catch up
	ELIF fCurrentPlaybackTime < 7000.0
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															3.0, 8.0, 22.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.6, 0.5, 1.25, TRUE, 0, 10.0, TRUE)
		fAccel = 0.075
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fCurrentPlaybackTime < 7000.0 >, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
	
	//if however the player trying to do the chase in a different vehicle dont slow down	
	ELIF ePlayerTransportState <> PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
	OR fChaseDistance > ((CELEB_ESCAPED_DIST / 100) * (PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE * 100))	// same time the chase blip will kick in		
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															10.0, 20.0, 150.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.7, 0.5, 1.2, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 20.0, TRUE)
			fAccel = 0.45
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player not on bev bike or dist from miranda is escape warning , BUT player in veh not bev's, so not slowed <, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
		ELSE
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															0.0, 0.2, 5.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.6, 0.5, 1.2, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 20.0, TRUE)
			fAccel = 0.45
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player not on bev bike or dist from miranda is escape warning, so slowed, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
		ENDIF		
	
	// speed boost directly after Beverly has taken a photo (during his check photo state)
	ELIF bDoDelayForPhotoTaken
	AND (GET_GAME_TIMER() - iTimerIncreaseCelebSpeedForPhotoTaken > 2500)	// 3500)	
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															5.0, 10.0, 18.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.6, 0.4, 1.2, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 15.0, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - Bev took a photo boost >, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
		
	//just maintain the speed if really close together (attempt to allow the player to get 
	// alongside the celeb's vehicle so Beverly can take a photo) normal uber calculating wants the player behind the main car)
	ELIF fChaseDistance < 4.0		
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															0.0, 0.5, 6.0, fLosingTriggerDist, 50.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.6, 0.4, 1.1, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 15.0, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - Player really close so just maintain current speed <>, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
	ELSE
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(fDesiredPlaybackSpeed, sBeverlyVehicle.vehicle, sCelebVehicle.vehicle, 
															0.0, 0.5, 6.0, fLosingTriggerDist, 30.0, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK,
															0.7, 0.5, 1.1, TRUE, ABSF(BIKE_TOP_SPEED_MODIFIER), 15.0, TRUE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - default values <>, cal fDesiredPlaybackSpeed = ", fDesiredPlaybackSpeed) ENDIF #ENDIF
	ENDIF

	UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentChaseSpeed, fDesiredPlaybackSpeed, fAccel)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionRubberbandInfoToTTY CPRINTLN(DEBUG_MISSION, "NEW_UPDATE_CELEB_CHASE_SPEED - fAccel = ", fAccel, " playback speed is = ", fCurrentChaseSpeed, " frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Rubberbands the chase speed of Franklin to Miranda's limo
///    NOTE: having to override the standard uber recording behaviour a bit as the player needs to be able to get along side Miranda comfortably
/// PARAMS:
///    fCurrentChaseSpeed - the main chase speed which is to be updated
///    fChaseDistance - the current dist between Franklin and Miranda's limo
PROC UPDATE_CELEB_CHASE_SPEED(FLOAT &fCurrentChaseSpeed, FLOAT fChaseDistance)		
	//uber recording
	FLOAT fDesiredPlaybackSpeed = fCurrentChaseSpeed
	FLOAT fMinDist = 0.0
	FLOAT fIdealDist = 0.2
	FLOAT fSlowDownDist = 12.0	//3.0
	FLOAT fMaxPlaybackSpeedMultiplier = 1.0
	FLOAT fAirResistanceMultiplier = 1.0
	FLOAT fCatchupSpeedMultiplier = 0.7
	BOOL bDoAirResistance = TRUE	
	
	IF bHasPlayerGotOnBike_FirstTime = FALSE		
		fDesiredPlaybackSpeed = 0.55
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player hasn't got on the bike first, so needing to slow the whole thing down loads") ENDIF #ENDIF	
	
	//speed up towards the end of the chase to get back in front before the rival pap turns up
	ELIF fCurrentPlaybackTime > 53000.0
		fMinDist = 9.5
		fIdealDist = 18.0	//15.0
		fSlowDownDist = 32.0	//30.0
		fMaxPlaybackSpeedMultiplier = 1.3	//1.2
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist, 
									fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - fast for catch up to rival pap") ENDIF #ENDIF
		
	//if however the player trying to do the chase in a different vehicle dont slow down	
	ELIF ePlayerTransportState <> PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
	OR fChaseDistance > ((CELEB_ESCAPED_DIST / 100) * (PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE * 100))	// same time the chase blip will kick in
		
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
			fMinDist = 10.0
			fIdealDist = 20.0
			fSlowDownDist = 150.0
			fMaxPlaybackSpeedMultiplier = 1.2
			CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
												fMinDist, fIdealDist, fSlowDownDist,
												fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player not on bev bike or dist from miranda is escape warning, BUT player in veh not bev's, so not slowed") ENDIF #ENDIF
		ELSE
			CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
												fMinDist, fIdealDist, fSlowDownDist,
												fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - player not on bev bike or dist from miranda is escape warning, so slowed") ENDIF #ENDIF
			IF fChaseDistance > 100.0
				fDesiredPlaybackSpeed = 0.25
			ENDIF
			IF fChaseDistance > 40.0
				IF fDesiredPlaybackSpeed > 0.5
					fDesiredPlaybackSpeed = 0.5
				ENDIF
			ENDIF
		ENDIF
		
	// speed boost directly after Beverly has taken a photo (during his check photo state)
	ELIF bDoDelayForPhotoTaken
	AND (GET_GAME_TIMER() - iTimerIncreaseCelebSpeedForPhotoTaken > 2500)	// 3500)	
		fMaxPlaybackSpeedMultiplier = 1.0
		fIdealDist = 0.6 //3.0
		fSlowDownDist = 18.0 //12.0
		fAirResistanceMultiplier = 5.0
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist,
									fMaxPlaybackSpeedMultiplier,  fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - Bev took a photo boost") ENDIF #ENDIF
		
		
	//just maintain the speed if really close together (attempt to allow the player to get 
	// alongside the celeb's vehicle so Beverly can take a photo) normal uber calculating wants the player behind the main car)
	ELIF fChaseDistance < 4.0		
		fDesiredPlaybackSpeed = fDesiredPlaybackSpeed
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - Player really close so just maintain current speed") ENDIF #ENDIF		
		
	//Start of the chase...slowed to accommidate player putting the helmet on.
	ELIF fCurrentPlaybackTime < 11000.0
		//change the catch up multipler
		fAirResistanceMultiplier = 2.0
		fCatchupSpeedMultiplier = 0.6
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist,
									fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - slow section for start of the chase to compensate player putting helmet on") ENDIF #ENDIF
		
	// TEMP gradule change back to defaults
	ELIF fCurrentPlaybackTime < 15000.0
		//change the catch up multipler
		fAirResistanceMultiplier = 1.66
		fCatchupSpeedMultiplier = 0.633
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist,
									fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - slow section for start of the chase gradual return ") ENDIF #ENDIF
		
	// TEMP gradule change back to defaults
	ELIF fCurrentPlaybackTime < 20000.0
		//change the catch up multipler
		fAirResistanceMultiplier = 1.33
		fCatchupSpeedMultiplier = 0.666
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist,
									fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - slow section for start of the chase gradual return 2") ENDIF #ENDIF
	
	// default values
	ELSE
		CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fDesiredPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 
									fMinDist, fIdealDist, fSlowDownDist,
									fMaxPlaybackSpeedMultiplier, fAirResistanceMultiplier, fCatchupSpeedMultiplier, bDoAirResistance)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - calculated default") ENDIF #ENDIF
	ENDIF
	
	fCurrentChaseSpeed = fDesiredPlaybackSpeed
	//UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fCurrentChaseSpeed, fDesiredPlaybackSpeed)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_CELEB_CHASE_SPEED - playback speed is - ", fCurrentChaseSpeed) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Calculates the speed the rival pap's vehicle needs to be driving at
/// RETURNS:
///    fReturnedSpeed
FUNC FLOAT GET_UPDATED_CHASE_SPEED_FOR_RIVAL_PAP_VEHICLE()
	FLOAT fReturnedSpeed
	FLOAT fIncreaseSpeedRange = 8.0
	FLOAT fDecreaseSpeedRange = 90.0	
	//VECTOR vBeverlyPos = GET_ENTITY_COORDS(sBeverlyPed.ped)
	//VECTOR vBeverlyOffsetFromRivalPap = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sRivalPapPed.ped, vBeverlyPos)	
	//INFO FROM PHIL - don't go below 25.0 speed for bikes as they will slow too much.
	//if however the player trying to do the chase in a different vehicle dont slow down
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "not slowing down as player is in alternative vehicle") ENDIF #ENDIF
		fReturnedSpeed = 45.0
		RETURN fReturnedSpeed
	//ELIF (vBeverlyOffsetFromRivalPap.Y > CLOSE_ENOUGH_FOR_BEVERLY_KNOCK_OFF_DISTANCE)
	ELIF eKnockOffBikerAttackState = KBAS_RUNNING_ATTACK		
		fReturnedSpeed = GET_ENTITY_SPEED(sRivalPapVehicle.vehicle) // GET_ENTITY_SPEED(sBeverlyVehicle.vehicle)		
		fReturnedSpeed += 0.2		
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_UPDATED_CHASE_SPEED_FOR_RIVAL_PAP_VEHICLE - > KBAS_RUNNING_ATTACK so maintain rival pap speed = ", fReturnedSpeed, "   >>>>>>>>>>>>>")  ENDIF #ENDIF
	ELIF fDistance_PlayerToRivalPap < fIncreaseSpeedRange
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPEED STABLISED		==================") ENDIF #ENDIF
		IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
			fReturnedSpeed = GET_ENTITY_SPEED(sBeverlyVehicle.vehicle)
			IF fReturnedSpeed < 26.0
				fReturnedSpeed = 26.0
			ENDIF
		ENDIF
		//fReturnedSpeed = 26.5   
	ELIF fDistance_PlayerToRivalPap > ((RIVAL_PAP_ESCAPED_DIST / 100) * (PAP1_RIVAL_PAP_CHASE_BLIP_FLASH_PERCENTAGE * 100))	// same time the chase blip will kick in
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPEED DECREASE DRAMATICALLY	<<<<<<<<<<<<<<") ENDIF #ENDIF
		fReturnedSpeed = 10.0  
	ELIF fDistance_PlayerToRivalPap > fDecreaseSpeedRange
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPEED DECREASE	<<<<<<<<<<<<<<") ENDIF #ENDIF
		fReturnedSpeed = 18.0   
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SPEED NORMALISED	<<<<<<<<<<<<<<") ENDIF #ENDIF
		fReturnedSpeed = 26.0 
	ENDIF	
	RETURN fReturnedSpeed
ENDFUNC

/// PURPOSE:
///    rubberbanding whilst chasing rival pap on the bike
PROC MONITOR_RIVAL_PAP_CHASE_SPEED()
	//don't do this every frame
	IF (GET_GAME_TIMER() - iCheckChaseSpeedTimer > 0)
		fRivalBikeSpeed = GET_UPDATED_CHASE_SPEED_FOR_RIVAL_PAP_VEHICLE()	
		SET_DRIVE_TASK_CRUISE_SPEED(sRivalPapDriverPed.ped, fRivalBikeSpeed)		
		iCheckChaseSpeedTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if Beverly is in position to start taking a photo
/// PARAMS:
///    iReturningSideForTakingPhoto - side of the target Beverly is to take the photo from
/// RETURNS:
///    TRUE if Bev is inside either the left or right zone
FUNC BOOL IS_BEVERLY_IN_A_START_TAKE_PHOTO_AREA()
	FLOAT fDistFromCelebVehiclesSideForPhoto[TOTAL_BEVERLY_TAKE_PHOTOS_AREAS]
	vTakePhotoZones_CentrePos[0] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vTakePhotoZones_CentrePosOffset[0])
	fDistFromCelebVehiclesSideForPhoto[0] = VDIST(GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), vTakePhotoZones_CentrePos[0])
	vTakePhotoZones_CentrePos[1] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vTakePhotoZones_CentrePosOffset[1])
	fDistFromCelebVehiclesSideForPhoto[1] = VDIST(GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), vTakePhotoZones_CentrePos[1])
	//DRAW_DEBUG_SPHERE(vTakePhotoZones_CentrePos[0], CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE, 0, 0, 255, 120)
	//DRAW_DEBUG_SPHERE(vTakePhotoZones_CentrePos[1], CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE, 0, 0, 255, 120)
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist from left is = ", fDistFromCelebVehiclesSideForPhoto[0]) ENDIF #ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist from right is = ", fDistFromCelebVehiclesSideForPhoto[1]) ENDIF #ENDIF	
	IF (fDistFromCelebVehiclesSideForPhoto[0] < CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE)
		//left side
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Bev inside Start Photo Anim Left Zone") ENDIF #ENDIF
		RETURN TRUE
	ELIF (fDistFromCelebVehiclesSideForPhoto[1] < CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE)
		//right side
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Bev inside Start Photo Anim Right Zone") ENDIF #ENDIF
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if Beverly is in in the specified taking photo zone
/// PARAMS:
///    iSideTakingPhoto - which zone to test Bev is in
/// RETURNS:
///    TRUE if Bev is inside the specified zone
FUNC BOOL IS_BEVERLY_IN_TAKE_PHOTO_AREA(INT iSideTakingPhoto)
	FLOAT fDistFromCelebVehiclesSideForPhoto	
	vTakePhotoZones_CentrePos[iSideTakingPhoto] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vTakePhotoZones_CentrePosOffset[iSideTakingPhoto])
	fDistFromCelebVehiclesSideForPhoto = VDIST(GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), vTakePhotoZones_CentrePos[iSideTakingPhoto])	
	IF (fDistFromCelebVehiclesSideForPhoto < CLOSE_ENOUGH_TO_TAKE_PHOTO_DISTANCE)
		RETURN TRUE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Bev inside take Photo area") ENDIF #ENDIF
	ENDIF	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Check if Beverly is is uninjured and sat on any bike
/// RETURNS:
///    TRUE if Bev is uninjured, on any bike and sitting in any vehicle
FUNC BOOL IS_BEVERLY_OK_AND_SAT_ON_ANY_BIKE()
	IF IS_PED_UNINJURED(sBeverlyPed.ped)
		IF (IS_PED_ON_ANY_BIKE(sBeverlyPed.ped) AND IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    common setup which needs to be done when switching over to check the photos state
///    moved here since this is called in multiple locations and needs to be kept in sync
PROC SETUP_CHECK_SUCCESSFUL_PHOTO()
	//if we reach the last slot there isn't any dialogue, so just allow photos to carry on being taken
	IF iGoodPhotosTakenCounter < (TOTAL_TOOK_PHOTO_DIALOGUE - 1)
		iGoodPhotosTakenCounter++
	ENDIF
	INFORM_MISSION_STATS_OF_INCREMENT(PAP1_PICTURES_SNAPPED)
	KILL_ANY_CONVERSATION()
	iTimerIncreaseCelebSpeedForPhotoTaken = GET_GAME_TIMER()	//used for rubberbanding	
	iTimerDelay_Progress_BevTakingPhotos = GET_GAME_TIMER()
	iCounter_BeverlyPhotoBurst = 0									// reset these ready for next attempt
	iMax_BeverlyPhotoBurst = GET_RANDOM_INT_IN_RANGE(6, 11)			// reset these ready for next attempt
	iDelay_BeverlyPhotoBurst = GET_RANDOM_INT_IN_RANGE(250, 750)	// reset these ready for next attempt - amount of time Bev has to be consecutively aiming at the celeb to take the a photo
ENDPROC

/// PURPOSE:
///    Handles Beverly taking photos of the celeb during the car chase
PROC MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE()
	IF NOT IS_BEVERLY_OK_AND_SAT_ON_ANY_BIKE()
		//Reset if Bev isn't ok to take a photo
		eBeverlyTakePhotoState = PAP1_BTPS_SETUP
		iCounter_BeverlyPhotoBurst = 0									// reset these ready for next attempt
		iMax_BeverlyPhotoBurst = GET_RANDOM_INT_IN_RANGE(6, 11)			// reset these ready for next attempt
		iDelay_BeverlyPhotoBurst = GET_RANDOM_INT_IN_RANGE(250, 750)	// reset these ready for next attempt - amount of time Bev has to be consecutively aiming at the celeb to take the a photo
	ELSE
		IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
			#IF IS_DEBUG_BUILD 
				IF bDebug_OverrideCelebPosInCarOffset
					vCelebPosInCarOffset = vDebug_vCelebPosInCarOffsetOverride
				ENDIF
			#ENDIF	
			VECTOR vCelebPos
			vCelebPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vCelebPosInCarOffset)		
			SWITCH eBeverlyTakePhotoState				
				CASE PAP1_BTPS_SETUP
					BOOL bSafeToStartTakingPhotos
					bSafeToStartTakingPhotos = TRUE
					// don't start taking a photo if we are playing a hurry up anim already
					IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")
						// make sure the anim has completed (has the hold last frame flag
						IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")
							bSafeToStartTakingPhotos = FALSE
						ENDIF
					ENDIF
					IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
						// make sure the anim has completed (has the hold last frame flag
						IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
							bSafeToStartTakingPhotos = FALSE
						ENDIF
					ENDIF
					IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
						// make sure the anim has completed (has the hold last frame flag
						IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
							bSafeToStartTakingPhotos = FALSE
						ENDIF
					ENDIF
					/* ^^^ bev alt anim during Franklin helmet anim change 
					// don't take photos is Beverly is currently set back for Franklin putting on his helmet
					IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")
						bSafeToStartTakingPhotos = FALSE
					ENDIF*/
		
					IF bSafeToStartTakingPhotos	
						IF IS_BEVERLY_IN_A_START_TAKE_PHOTO_AREA()										
							//IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)	// SCRIPT_TASK_AIM_GUN_SCRIPTED)	// SCRIPT_TASK_DRIVE_BY)
								//CLEAR_PED_TASKS(sBeverlyPed.ped)
								FLOAT fTaskSweepAim_TurnRate
								fTaskSweepAim_TurnRate = 3.0 	// default is 1.0, 2.0 would be twice as fast, etc
								#IF IS_DEBUG_BUILD IF bDebug_OverrideTaskSweepAim_TurnRate fTaskSweepAim_TurnRate = fDebug_TaskSweepAim_TurnRate ENDIF #ENDIF
								TASK_SWEEP_AIM_POSITION(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vCelebPos, fTaskSweepAim_TurnRate)													
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_SETUP - task sweep set") ENDIF #ENDIF			
								RESET_IS_BEVERLY_AIMING_AT_CELEB_TEST()
								// stop the get me alongside the car dialogue if we are switching to taking photos
								IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CHASE06")
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								ENDIF
								iTimerDelay_Progress_BevTakingPhotos = GET_GAME_TIMER()	
								eBeverlyTakePhotoState = PAP1_BTPS_TAKING_PHOTO
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_SETUP going to PAP1_BTPS_TAKING_PHOTO") ENDIF #ENDIF				
							//ENDIF
						ELSE
							/* ^^^ bev alt anim during Franklin helmet anim change - original to swap out*/
							IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")							
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - reset Bev to idle pose") ENDIF #ENDIF
							ENDIF
							/* ^^^ swap out end */							
							/* ^^^ bev alt anim during Franklin helmet anim change 
							IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@sport@front@base", "put_on_helmet")		// B*1422622 - Franklin's put on helmet animation cause him to clip through Beverly
								IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
									TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - eset Bev to idle pose") ENDIF #ENDIF
								ENDIF
							ELSE
								IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")
									REQUEST_ANIM_DICT("veh@bike@sport@rear@base")
									IF HAS_ANIM_DICT_LOADED("veh@bike@sport@rear@base")
										TASK_PLAY_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - applied still on bike anim whilst player puts on helmet") ENDIF #ENDIF
									ELSE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - requestin anim dict for still idle -  veh@bike@sport@rear@base for player puts on helmet") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF*/
						ENDIF
					ENDIF
					IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, sCelebPed.ped)
						TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, sCelebPed.ped, -1, SLF_FAST_TURN_RATE)	
					ENDIF
				BREAK			
				CASE PAP1_BTPS_TAKING_PHOTO
					IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
						//tasking isn't being preformed so revert back to setup state
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - reset Bev to idle pose") ENDIF #ENDIF
						ENDIF
						eBeverlyTakePhotoState = PAP1_BTPS_SETUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO going to PAP1_BTPS_SETUP sweep task wasn't being performed") ENDIF #ENDIF
					ELSE
						UPDATE_TASK_SWEEP_AIM_POSITION(sBeverlyPed.ped, vCelebPos)			
						INT iTimeDelay_ForRaisingCameraIntoPosition
						iTimeDelay_ForRaisingCameraIntoPosition = 750
						IF HAS_TIME_PASSED(iTimerDelay_Progress_BevTakingPhotos, iTimeDelay_ForRaisingCameraIntoPosition)	//delay for raising camera up ...where intro anim should be
							IF NOT IS_BEVERLY_IN_A_START_TAKE_PHOTO_AREA()
								// bit of leeyway for being outside the area
								INT iTimeDelay_ForCancelPhotoAttemptIfOutsideArea
								iTimeDelay_ForCancelPhotoAttemptIfOutsideArea = 1250
								IF HAS_TIME_PASSED(iTimerDelay_Progress_BevTakingPhotos, iTimeDelay_ForCancelPhotoAttemptIfOutsideArea)	//delay for cancel attempt if outside the photo area
									// if he successfully took a few photos then switch to checking the camera, else reset
									IF iCounter_BeverlyPhotoBurst >= iMax_BeverlyPhotoBurst
										SETUP_CHECK_SUCCESSFUL_PHOTO()									
										eBeverlyTakePhotoState = PAP1_BTPS_SUCCESSFUL_PHOTO
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO going to PAP1_BTPS_SUCCESSFUL_PHOTO - Bev dropped out photo area but has already took some photos") ENDIF #ENDIF
									ELSE
										//camera raised but bev no longer in photo area, so cancel
										IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")							
											TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - reset Bev to idle pose") ENDIF #ENDIF
										ENDIF
										eBeverlyTakePhotoState = PAP1_BTPS_SETUP
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO going to PAP1_BTPS_SETUP left photo area") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELSE IS_BEVERLY_AIMING_CAMERA_AT_CELEB(vCelebPos)
								IF HAS_TIME_PASSED_BEV_CONSECUTIVELY_AIMED_CAMERA_AT_CELEB(iDelay_BeverlyPhotoBurst)
									#IF IS_DEBUG_BUILD IF bDebug_DisplayShapeTestPap1CameraAim DRAW_RECT(0.3, 0.3, 0.2, 0.2, 0, 255, 0, 150) ENDIF #ENDIF	
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - bev has been consecutively aiming for at least this period of time = ", iDelay_BeverlyPhotoBurst) ENDIF #ENDIF
									IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
										IF IS_ENTITY_ALIVE(sObjFlashUnitBeverly.object)
											START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", sObjFlashUnitBeverly.object, vCameraFlashFX_Offset, << 0.0, 0.0, 0.0 >>)	// B*1706190 - no roration offset, uses parent
										ENDIF
										IF NOT HAS_SOUND_FINISHED(iSoundId_CameraFlash)
											STOP_SOUND(iSoundId_CameraFlash)
										ENDIF 
										IF REQUEST_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)
											PLAY_SOUND_FROM_COORD(iSoundId_CameraFlash, "CAMERA", GET_PED_BONE_COORDS(sBeverlyPed.ped, BONETAG_PH_R_HAND, vCameraFlashFX_Offset), sPap1_SoundSetName)
										ENDIF
									ENDIF
									iConsecutiveAimingTimer = -1	// reset the timer used to detect how long Bev has been continuously aiming at the celeb successfully.  used with iDelay_BeverlyPhotoBurst
									iFrameCountSinceLastSuccessfulShapeTestCameraAim = 0 // reset the grace period frame counter
									iDelay_BeverlyPhotoBurst = GET_RANDOM_INT_IN_RANGE(250, 750)	// setup the delay time for the next photo to be trigger
									iCounter_BeverlyPhotoBurst++
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO iCounter_BeverlyPhotoBurst incremented to : ", iCounter_BeverlyPhotoBurst, "frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
									
									// only switch to check photos state if he's successfully taken a burst of photos
									IF iCounter_BeverlyPhotoBurst >= iMax_BeverlyPhotoBurst
										
										REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_LOW)
										
										SETUP_CHECK_SUCCESSFUL_PHOTO()									
										eBeverlyTakePhotoState = PAP1_BTPS_SUCCESSFUL_PHOTO
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO going to PAP1_BTPS_SUCCESSFUL_PHOTO") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE PAP1_BTPS_SUCCESSFUL_PHOTO
					IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
						UPDATE_TASK_SWEEP_AIM_POSITION(sBeverlyPed.ped, vCelebPos)	
					ENDIF
					INT iTimeDelay_ForLoweringCameraIntoPosition
					iTimeDelay_ForLoweringCameraIntoPosition = 500					
					IF HAS_TIME_PASSED(iTimerDelay_Progress_BevTakingPhotos, iTimeDelay_ForLoweringCameraIntoPosition)
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
							//CLEAR_PED_TASKS(sBeverlyPed.ped)
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_TAKING_PHOTO resetting to idle anim for successful photo") ENDIF #ENDIF
						ELSE
							INT iTimeDelay_StartCheckingPhotoAnim
							iTimeDelay_StartCheckingPhotoAnim = 1000	// 1000 after reset idle anim
							IF HAS_TIME_PASSED(iTimerDelay_Progress_BevTakingPhotos, iTimeDelay_StartCheckingPhotoAnim)
								//check photos anim sequence - intro loop and outro
								TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_intro_pap", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_HOLD_LAST_FRAME)
								eBeverlyTakePhotoState = PAP1_BTPS_SETUP_CHECKING_PHOTO
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_SUCCESSFUL_PHOTO going to PAP1_BTPS_SETUP_CHECKING_PHOTO") ENDIF #ENDIF
							ENDIF	
						ENDIF
					ENDIF
				BREAK
				CASE PAP1_BTPS_SETUP_CHECKING_PHOTO
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_intro_pap")
						//animation isn't playing so revert back to setup state
						eBeverlyTakePhotoState = PAP1_BTPS_SETUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_SETUP_CHECKING_PHOTO going to PAP1_BTPS_SETUP anim was not playing") ENDIF #ENDIF
					ELSE
						SET_ENTITY_ANIM_SPEED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_intro_pap", 1.4)		
						IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_intro_pap")
							TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_loop_pap", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_LOOPING)
							eBeverlyTakePhotoState = PAP1_BTPS_CHECKING_PHOTO							
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_SUCCESSFUL_PHOTO going to PAP1_BTPS_SETUP_CHECKING_PHOTO") ENDIF #ENDIF
						ENDIF
					ENDIF	
				BREAK
				CASE PAP1_BTPS_CHECKING_PHOTO
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_loop_pap")
						//animation isn't playing so revert back to setup state
						eBeverlyTakePhotoState = PAP1_BTPS_SETUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_CHECKING_PHOTO going to PAP1_BTPS_SETUP anim was not playing") ENDIF #ENDIF
					ELSE					
						IF bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter]	// loop the checking photo anim until Bev has delivery his checking photo dialogue
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "delay next photo taking finished!") ENDIF #ENDIF
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_outro_pap", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY | AF_HOLD_LAST_FRAME)
								eBeverlyTakePhotoState = PAP1_BTPS_FINISHED_CHECKING_PHOTO
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_CHECKING_PHOTO going to PAP1_BTPS_FINISHED_CHECKING_PHOTO") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF					
				BREAK
				CASE PAP1_BTPS_FINISHED_CHECKING_PHOTO
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_outro_pap")
						//animation isn't playing so revert back to setup state
						eBeverlyTakePhotoState = PAP1_BTPS_SETUP
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_FINISHED_CHECKING_PHOTO going to PAP1_BTPS_SETUP anim was not playing") ENDIF #ENDIF
					ELSE
						SET_ENTITY_ANIM_SPEED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_outro_pap", 1.4)	
						IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "lookatcam_outro_pap")
							//setup the idle task once the check camera anims are done
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							eBeverlyTakePhotoState = PAP1_BTPS_SETUP
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE - PAP1_BTPS_FINISHED_CHECKING_PHOTO going to PAP1_BTPS_SETUP") ENDIF #ENDIF
						ENDIF
					ENDIF	
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF	
ENDPROC

//Handles removal of celeb when away from the player
//used when the buer recording has finished
//and the player should be chasing the rival instead
PROC CLEANUP_CELEB_WHEN_AWAY_FROM_THE_PLAYER()
	IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sCelebVehicle.vehicle, 120.0)
		IF IS_ENTITY_OCCLUDED(sCelebVehicle.vehicle)
			IF IS_PED_UNINJURED(sCelebPed.ped)
				SET_ENTITY_LOAD_COLLISION_FLAG(sCelebPed.ped, FALSE)
			ENDIF
			SAFE_REMOVE_PED(sCelebPed.ped, TRUE)
			SAFE_REMOVE_PED(sCelebsDriverPed.ped, TRUE)
			SAFE_REMOVE_VEHICLE(sCelebVehicle.vehicle, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Give the celeb driver a driving sequence to perform after the uber recording
PROC SETUP_CELEB_DRIVERS_BEHAVIOUR_WHEN_UBER_RECORDING_FINISHES()			
	IF IS_PED_UNINJURED(sCelebsDriverPed.ped)
		SEQUENCE_INDEX seqIndex_CelebtDriverForCleanup
		OPEN_SEQUENCE_TASK(seqIndex_CelebtDriverForCleanup)					
			TASK_VEHICLE_MISSION_PED_TARGET(NULL, sCelebVehicle.vehicle, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DRIVINGMODE_PLOUGHTHROUGH, 500.0, -1.0)
			//TASK_VEHICLE_MISSION_COORS_TARGET(NULL, sCelebVehicle.vehicle, << -1259.2776, -1048.7726, 7.4445 >>, MISSION_GOTO, 65.00, DRIVINGMODE_PLOUGHTHROUGH, -1, -1) 	these trigger assert since the goto coord is outside of the loaded nodes
			//TASK_VEHICLE_MISSION_COORS_TARGET(NULL, sCelebVehicle.vehicle, << 412.9179, 267.1376, 102.0397 >>, MISSION_GOTO, 65.00, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
			//TASK_VEHICLE_DRIVE_WANDER(NULL, sCelebVehicle.vehicle, 75.0, DRIVINGMODE_AVOIDCARS)
		SET_SEQUENCE_TO_REPEAT(seqIndex_CelebtDriverForCleanup, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(seqIndex_CelebtDriverForCleanup)
		TASK_PERFORM_SEQUENCE(sCelebsDriverPed.ped, seqIndex_CelebtDriverForCleanup)
		CLEAR_SEQUENCE_TASK(seqIndex_CelebtDriverForCleanup)
		SET_PED_KEEP_TASK(sCelebsDriverPed.ped, TRUE)	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Handles the dialogue when chasing the celeb whilst player and bev are on the bike
PROC MANAGE_DIALOGUE_DURING_CHASE_CELEB() 
	IF eBeverlyTakePhotoState = PAP1_BTPS_CHECKING_PHOTO
		IF NOT bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter]
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION()	// _DO_NOT_FINISH_LAST_LINE() B*1573394 - don't cut off ongoing dialogue just finish the current line - photo check lowe waits on no convo and bDoneDialogue_BeverlyTookPhoto set so should be fine
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "aTTEMPTED TO KILL THE CONVO FOR THE PHOTO TAKEN DIALOGUE") ENDIF #ENDIF
			ELSE
				IF NOT bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter]
					IF iGoodPhotosTakenCounter < (TOTAL_TOOK_PHOTO_DIALOGUE - 1)	// the additional index is used to keep photos checking (without dialogue).
						TEXT_LABEL_15 tl15_CheckPhotoRoot
						tl15_CheckPhotoRoot = "PAP1_SNAP"
						SWITCH iGoodPhotosTakenCounter	//only trigger once this value has been set.
							CASE 0
								tl15_CheckPhotoRoot += "01"
								// I don't believe it! She's slamming a speedball in there, man! This is journalistic gold!  
							BREAK
							CASE 1
								tl15_CheckPhotoRoot += "02"
								// Man, they don't call her the Silicone Slattern for nothing. She looks like a blood hound in a wind tunnel. I love her
								// No wonder this whole town's in therapy.   
							BREAK
							CASE 2
								tl15_CheckPhotoRoot += "05"	// rejigged after speaking with Mike (highly unlikely following two convos will trigger in time)
								// We done yet?
								// No, she still looks too good. I want the prisoner of war shot. That's where the big zeros are.  
							BREAK
							CASE 3
								tl15_CheckPhotoRoot += "03"
								// Oh yes, we have sideboob. We're historians, crusaders of truth, my friend!
								// We're two stalkers on a motorbike.
							BREAK
							CASE 4
								tl15_CheckPhotoRoot += "04"
								// She's still shooting up in there! Amazing. Wait, is that...?  
								// No, thought I'd had muff for a second. Open the curtains to the fun tunnel, Miranda!  
							BREAK
						ENDSWITCH						
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tl15_CheckPhotoRoot, CONV_PRIORITY_MEDIUM)
							bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter] = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter] = ", iGoodPhotosTakenCounter, " tl15_CheckPhotoRoo= ", tl15_CheckPhotoRoot) ENDIF #ENDIF
						ENDIF
					ELSE
						bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter] = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneDialogue_BeverlyTookPhoto[iGoodPhotosTakenCounter] = ", iGoodPhotosTakenCounter, " no dialogue so skipped") ENDIF #ENDIF
					ENDIF
					bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this so Franklin doesn't give a response in the wrong place
				ENDIF				
			ENDIF
		ENDIF		
		iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()	//reset the dialogue counter for additional dialogue, so he does say it straight after successful photo dialogue		
	ELIF eBeverlyTakePhotoState != PAP1_BTPS_SUCCESSFUL_PHOTO	//don't do any dialogue in the gap where a successful photo was taken and we are waiting for the check photo anim		
		//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_01") // Drive alongside Miranda's ~b~car.~s~	//*commented out for new display message check with subtitle profile settings	
			IF NOT bDoneDialogue_PlayerPullsWheelie
				// currently the only thing remotely close to a is_bike_pulling_wheelie check, potentially use with IS_ENTITY_IN_AIR()
				// but in this instance i think the dialogue will work with airbourne as well
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
					IF HAS_TIME_PASSED(iTimer_BeverlyBikeNotOnAllWheelsTimer, 500) //make sure player has been pulling wheelie for a decent chunk of time 
						// random dialogue, so only one line will be selected
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PPW1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// Christ, are you trying to kill me.
							// Less of the crazy driving, man.
							// You'll give me a heart attack!
							bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this so Franklin can't respond to this one
							bDoneDialogue_PlayerPullsWheelie = TRUE
						ENDIF
					ENDIF
				ELSE
					iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				// reset the dialogue so it can trigger agains
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PPW1")
				AND IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
					iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
					bDoneDialogue_PlayerPullsWheelie = FALSE
				ENDIF
			ENDIF
			// Franklin crashes the bike		
			IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CRASH")
			AND NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CHASE02")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sBeverlyVehicle.vehicle)
						//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//ENDIF
						IF IS_ENTITY_TOUCHING_ENTITY(sCelebVehicle.vehicle, sBeverlyVehicle.vehicle)
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE02", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Don't ram her off the road, man!
								// Oh yeah, real subtle!  
								// Not that close!  
								bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond	
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE_CELEB - crash bike into Miranda dialogue triggered - PAP1_CHASE02") ENDIF #ENDIF
							ENDIF
						ELSE
							// random selected lines
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CRASH", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// You idiot! 
								// For Christ's sake!
								// Are you blind?    
								// Can't you drive!?
								// Is it that hard? 
								// No! You moron!  
								// You have gotta be kidding me!
								bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE_CELEB - crash bike dialogue triggered - PAP1_CRASH") ENDIF #ENDIF
							ENDIF						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()			
			ELSE
				//only do this dialogue if we aren't so close to the limo that we are attempting a photo		
				IF eBeverlyTakePhotoState = PAP1_BTPS_SETUP	
					IF NOT bDoneDialogue_FranklinRespondToBeverlyBerating	// reset this to ensure Franklin doesn't say something straight away
						IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RESP", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())								
								// Yeah, yeah, I got it. 
								// Maybe if you shut up for a second.  
								// What do you think I'm doin'?
								// Fuck, I know, I'm tryin'!
								// Are you always this annoying?  
								// I get the idea, homie.  
								// You really ain't helpin'.
								// We might do better if you shut the fuck up.
								// I heard you the first time. 
								iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
								bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LCHASING RIVAL PAP - Franklin Response dialogue triggered - PAP1_RESP") ENDIF #ENDIF
							ENDIF
						ELSE
							bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// bypass response if we are out range of each other
						ENDIF
					ELSE
						// Franklin drives into a ped
						IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
						AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_HITPED", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
									// It's okay, they're fine.  
									// That's it, no prisoners!  
									// Collateral damage, don't worry about it.    
									// Good, if they get in the way, take them down!
									// Move next time!
									iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING CELEB - drove into ped dialogue - PAP1_HITPED") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE	
							IF HAS_TIME_PASSED(iDialogueTimer_ChasingCeleb, TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
								//different dialogue depending on how far away from the celeb we are						
								//Check if the player is behind or infront of the celeb
								VECTOR vPlayerPosOffsetFromCeleb = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sCelebVehicle.vehicle, GET_ENTITY_COORDS(PLAYER_PED_ID()))						
								#IF IS_DEBUG_BUILD
									IF bDebugPrint_CelebOffset_Y_ToPlayer
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dialogue checker - y offset = ", vPlayerPosOffsetFromCeleb.y) ENDIF #ENDIF
									ENDIF
								#ENDIF						
								IF vPlayerPosOffsetFromCeleb.y < 2.0//Player behind celeb						
									IF (fCurrentChaseDistanceCeleb < DIST_LIMIT_PLAYER_CLOSE_TO_CELEB_IN_CHASE_FOR_DIALOGUE)	
										// alterative dialogue if he's already took one successful photo - B*1050501
										IF iGoodPhotosTakenCounter = -1
											IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE06", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
												//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING CELEB GET CLOSE DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
												iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
												// Get closer, man.
												// Get me right next to her car door.
												// You got to get me alongside her car.
												// Get up alongside her, so I can take the shot.
												// Get me real close so I can zoom in on her new tits.
												// Can you get me at the side of her?
												// Come on man, get me in close.
												// I can't take the shot from here, go closer.
												// Come on, come on! Closer, closer!
												IF (GET_RANDOM_INT_IN_RANGE(0, 6) = 0)	// 20% chance of playing the anim
													SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(sCelebPed.ped)
												ENDIF
												IF (GET_RANDOM_INT_IN_RANGE(0, 6) <= 1)	// 40% chance of Franklin responding
													bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond
												ENDIF											
											ENDIF
										ELSE
											IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE08", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
												//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING CELEB GET CLOSE DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
												iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
												// Go again.
												// I need some more.
												// More, more!
												// I need more shots.
												IF (GET_RANDOM_INT_IN_RANGE(0, 6) = 0)	// 20% chance of playing the anim
													SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(sCelebPed.ped)
												ENDIF
											ENDIF
										ENDIF
									ELSE							
										IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_HURRY", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())	// used to be PAP1_CHASE04
											//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING CELEB FAR DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
											iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
											// Faster! Go! Go!
											// We need the shot! Hurry!
											// Can't this go any faster?
											// Do not let her go!
											IF (GET_RANDOM_INT_IN_RANGE(0, 3) < 2)	// 66% chance of playing the anim
												SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(sCelebPed.ped)
											ENDIF
											IF (GET_RANDOM_INT_IN_RANGE(0, 6) <= 4)	// 80% chance of Franklin responding
												bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond
											ENDIF
										ENDIF									
									ENDIF								
								ELSE //Player ahead of celeb						
									IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE07", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "$$$	Player ahead of celeb in chase") ENDIF #ENDIF
										iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
										// Hey, slow down she's behind us.
										// Man, hit the brakes, we are too far ahead.
										// Let Miranda catch up.
										// Go easy on the gas, we're too far in front.
										// I can't take the shot from here.
										IF (GET_RANDOM_INT_IN_RANGE(0, 6) <= 3)	// 60% chance of Franklin responding
											bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond
										ENDIF
									ENDIF							
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				// if Beverly is in the process of taking photos
				ELIF eBeverlyTakePhotoState = PAP1_BTPS_TAKING_PHOTO
					IF HAS_TIME_PASSED(iDialogueTimer_ChasingCeleb, GET_RANDOM_INT_IN_RANGE(250, 750))	// increase chance of this dialogue triggering
						// we are two dialogue roots for this, so randomly pick dialogue from either
						INT iRandomDialogueRoot = GET_RANDOM_INT_IN_RANGE(0, 3)
						TEXT_LABEL_23 tlRoot
						IF iRandomDialogueRoot = 0
							tlRoot = "PAP1_PHOTO"
							// The fans have a right to know! Everything! 
							// It's in the public interest! You homewrecker! 
							// Aroooo! Show us your marriage busting muff, Miranda! You bitch!
							// You are an angel! An angel!
							// Homewrecker! Harlot! 
							// Hey, here! Here! Here! Here, you bitch!
							// You ruined his life! Now make my day!
						ELIF iRandomDialogueRoot = 1
							tlRoot = "PAP1_PHOTO2"
							// Yeah. Yeah. Yeah. Oh Yeah! Yeah! Oh yeah, gimme more! Gimme more!
							// Oh, you're human, you bitch!
							// Nothing special about you.
							// She's a goddess, she's a whore!
							// I love her, I hate her!
						ELSE
							tlRoot = "PAP1_FIDGET"
							// C'mon. C'mon. C'mon. You ruined Mark Fostenburg's wife's life! 
							// Homewrecker! 
							// Open it up, you cow. 
							// Give me something, anything. Stop pretending to cry! 
							// I love you, Miranda!
							// You know you want it, Cowan!  
							// You're a fucking mess! Look at you!
							// Well done on the rehab! Not!
							// You don't a look a day over forty five!
						ENDIF
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlRoot, CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_DIALOGUE_DURING_CHASE_CELEB - bev taking photos triggered") ENDIF #ENDIF
							bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this so Franklin doesn't give a response in the wrong place
							iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()
						ENDIF		
					ENDIF
				ENDIF
			ENDIF
		//ENDIF		
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the dialogue during drop Beverly off at next stakeout
PROC MANAGE_DIALOGUE_DURING_DROP_BEVERLY_OFF()
	//	IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_05")		//*commented out for new display message check with subtitle profile settings	
		IF NOT bDoneDialogue_BeverlySayKeepBike
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF bPausedDialogue_DropOffBeverly
					IF PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DROP", tlPausedDialogue_DropOffBeverly, CONV_PRIORITY_MEDIUM)
						bPausedDialogue_DropOffBeverly = FALSE // reset flag
						bDoneDialogue_BeverlySayKeepBike = TRUE 
					ENDIF												
				ELSE
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DROP", CONV_PRIORITY_MEDIUM)
						// There's a small parking lot by the Café Redemption courtyard in Morningwood.  
						// Drop me there, and you can keep the bike, because I look after my people.  
						// I know when a ride's hot. If this is your bike, I'm the fuckin' pope, homie.  
						// Okay, okay, don't ruin the vibe.    
						bDoneDialogue_BeverlySayKeepBike = TRUE 
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT bDoneDialogue_DropOffBeverly											
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF bPausedDialogue_DropOffBeverly
					//alternative dialogue based for no photos taken during chase
					IF iGoodPhotosTakenCounter = -1														
						//failed dialogue
						IF PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF05", tlPausedDialogue_DropOffBeverly, CONV_PRIORITY_MEDIUM)
							bPausedDialogue_DropOffBeverly = FALSE // reset flag
							bDoneDialogue_DropOffBeverly = TRUE 
						ENDIF
					ELSE
						//successful dialogue
						IF PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF04", tlPausedDialogue_DropOffBeverly, CONV_PRIORITY_MEDIUM)														
							bPausedDialogue_DropOffBeverly = FALSE // reset flag
							bDoneDialogue_DropOffBeverly = TRUE
						ENDIF
					ENDIF													
				ELSE
					//alternative dialogue based for no photos taken during chase
					IF iGoodPhotosTakenCounter = -1														
						//failed dialogue
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF05", CONV_PRIORITY_MEDIUM)
							// You've probably cost me this year's Showbiz Photography Award.
							// Not one damn photo.
							// Fingers crossed this next stakeout goes better.
							bDoneDialogue_DropOffBeverly = TRUE
						ENDIF
					ELSE
						//successful dialogue
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF04", CONV_PRIORITY_MEDIUM)
							// I think I got a decent shot of Miranda back there.
							// So we're splitting the cash you get for the photo fifty-fifty, right?
							// Err, yeah sure.
							bDoneDialogue_DropOffBeverly = TRUE 
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT bDoneDialogue_DropOffBeverlyAdditionalChat
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF bPausedDialogue_DropOffBeverly
					IF PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF02", tlPausedDialogue_DropOffBeverly, CONV_PRIORITY_MEDIUM)
						bPausedDialogue_DropOffBeverly = FALSE // reset flag
						bDoneDialogue_DropOffBeverlyAdditionalChat = TRUE 
					ENDIF												
				ELSE
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF02", CONV_PRIORITY_MEDIUM)
						// I probably shouldn't talk, but this is one messed up line of work you're in.    
						// It's about staying true to yourself.  
						// With muff shots.  
						// Miranda Cowan's thirty nine years old - we just made her day, man. 
						// They want it, the little whores, they court it. It's a beautiful, abusive relationship.
						// Whose day you making next?  
						// Samantha Muldoon, the singer. I got a tip-off she's buying a North Korean baby on the black market. 
						// The holy grail of adoption contraband. This could define my career.  
						bDoneDialogue_DropOffBeverlyAdditionalChat = TRUE 
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Play bev comment about taking along time to reach drop off - setup as random dialogue in DialogueStar
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDialogueTimer_TakingLongTimeToReachDropOffPoint = GET_GAME_TIMER()
			ELSE
				IF NOT bDoneDialogue_PlayerPullsWheelie
					// currently the only thing remotely close to a is_bike_pulling_wheelie check, potentially use with IS_ENTITY_IN_AIR()
					// but in this instance i think the dialogue will work with airbourne as well
					IF NOT IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
						IF HAS_TIME_PASSED(iTimer_BeverlyBikeNotOnAllWheelsTimer, 500) //make sure player has been pulling wheelie for a decent chunk of time 
							// random dialogue, so only one line will be selected
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PPW1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Christ, you're gonna kill us.  
								// You're a fucking lunatic.  
								// Are you trying to give me a heart attack?  
								// Is that supposed to impress me? 
								bDoneDialogue_PlayerPullsWheelie = TRUE
								bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this so Franklin can't respond to this one
								iDialogueTimer_TakingLongTimeToReachDropOffPoint = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
					ENDIF
				ELSE
					// reset the dialogue bool once the current conversation has finished
					IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PPW1")
					AND IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
						bDoneDialogue_PlayerPullsWheelie = FALSE
						iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
				// Franklin drives into a ped
				IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
				AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_HITPED", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// It's okay, they're fine.  
							// That's it, no prisoners!  
							// Collateral damage, don't worry about it.    
							// Good, if they get in the way, take them down!
							// Move next time!
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DROP OFF BEV - drove into ped dialogue - PAP1_HITPED") ENDIF #ENDIF
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() - iDialogueTimer_TakingLongTimeToReachDropOffPoint > (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE_LONG + GET_RANDOM_INT_IN_RANGE(1, 2500)))
					AND (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_DOFF03", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())				
							// We better get a move on. I should have been there twenty minutes ago.
							// How is this taking so long? I'm gonna miss my chance. 
							// If we don't get there soon, Samantha will be long gone.  
							iDialogueTimer_TakingLongTimeToReachDropOffPoint = GET_GAME_TIMER()
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TAKING A LONG TIME TO REACH DROP OFF DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				// Franklin crashes the bike		
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CRASH")
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sBeverlyVehicle.vehicle)
						//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//ENDIF
						// random selected lines
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CRASH", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// You idiot! 
							// For Christ's sake!
							// Are you blind?    
							// Can't you drive!?
							// Is it that hard? 
							// No! You moron!  
							// You have gotta be kidding me!
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DROP OFF BEV - crash biek dialogue triggered - PAP1_CRASH") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF																				
		ENDIF
	//ENDIF
ENDPROC

/// PURPOSE:
///    Updates the common functions used in the uber chase
/// PARAMS:
///    bControlChaseHintCam - set the hint cam to focus on the uber chase vehicle
///    bOverrideRubberbanding - when true the normal rubberbanding tests are ignored
///    fRubberbandingOverrideSpeed - the speed to apply on all vehicles in the uber recording, only works if bOverrideRubberbanding is true
PROC UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME(BOOL bControlChaseHintCam = TRUE, BOOL bOverrideRubberbanding = FALSE, FLOAT fRubberbandingOverrideSpeed = 1.0)
	IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
	AND IS_VEHICLE_OK(sBeverlyVehicle.vehicle)	// added to fix bug 1071498 - caused by CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR using player vehicle instead of player
		fCurrentChaseDistanceCeleb = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sCelebVehicle.vehicle))
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)		
			fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sCelebVehicle.vehicle)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CURRENT PLAYBACK TIME = ", fCurrentPlaybackTime) ENDIF #ENDIF			
			IF bOverrideRubberbanding
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase speed - bOverrideRubberbanding true. Value = ", fRubberbandingOverrideSpeed) ENDIF #ENDIF
				fMainPlaybackSpeed = fRubberbandingOverrideSpeed
			ELSE
				IF bUseNewCelebChaseRubberbanding
					NEW_UPDATE_CELEB_CHASE_SPEED(fMainPlaybackSpeed, fCurrentChaseDistanceCeleb)
				ELSE
					UPDATE_CELEB_CHASE_SPEED(fMainPlaybackSpeed, fCurrentChaseDistanceCeleb)
				ENDIF
			ENDIF
			SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
			PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
			bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
			IF fCurrentPlaybackTime < 1500.0
				CREATE_ALL_WAITING_UBER_CARS()	// ensure cars spawn in at the start
			ENDIF
			UPDATE_UBER_PLAYBACK(sCelebVehicle.vehicle, fMainPlaybackSpeed)
			IF bControlChaseHintCam
				CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sCelebVehicle.vehicle)
			ENDIF
			MANAGE_CELEB_VEHICLE_USING_HORN(fCurrentChaseDistanceCeleb)
			UPDATE_SCENARIO_BLOCKING_ON_CHASE_ROUTE(fCurrentPlaybackTime)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    creates the rival pap, his camera, his driver and his bike
PROC CREATE_RIVAL_PAP_FOR_ENTRANCE()
	IF NOT DOES_ENTITY_EXIST(sRivalPapVehicle.vehicle)
		CLEAR_AREA(sRivalPapVehicle.vSpawn, 2.0, TRUE)
		sRivalPapVehicle.vehicle = CREATE_VEHICLE(sRivalPapVehicle.model, sRivalPapVehicle.vSpawn)
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "rival pap bike created") ENDIF #ENDIF
	ENDIF
	IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
		SET_ENTITY_QUATERNION(sRivalPapVehicle.vehicle, 0.0329, -0.0200, 0.9542, -0.2968)
		SET_VEHICLE_ENGINE_ON(sRivalPapVehicle.vehicle, TRUE, TRUE)
		SET_VEHICLE_COLOURS(sRivalPapVehicle.vehicle, 0, 0)
		SET_VEHICLE_TYRES_CAN_BURST(sRivalPapVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(sRivalPapVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_PETROL(sRivalPapVehicle.vehicle, FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapVehicle.model)
		IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_CHASE")
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(sRivalPapVehicle.vehicle, "PAPARAZZO_1_PAP_BIKE_GROUP")
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_RIVAL_PAP_FOR_ENTRANCE added rivap pap vehicle to mix group - ", "PAPARAZZO_1_PAP_BIKE_GROUP") ENDIF #ENDIF
		ENDIF	
	ENDIF
	IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
		IF NOT DOES_ENTITY_EXIST(sRivalPapDriverPed.ped)
			sRivalPapDriverPed.ped = CREATE_PED_INSIDE_VEHICLE(sRivalPapVehicle.vehicle, PEDTYPE_MISSION, sRivalPapDriverPed.model, VS_DRIVER)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "rival pap driver created") ENDIF #ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapDriverPed.ped, TRUE)
			SET_PED_CAN_BE_TARGETTED(sRivalPapDriverPed.ped, FALSE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapDriverPed.ped, KNOCKOFFVEHICLE_NEVER)
			SET_PED_HELMET(sRivalPapDriverPed.ped, TRUE)
			GIVE_PED_HELMET(sRivalPapDriverPed.ped, TRUE)
			SET_PED_CONFIG_FLAG(sRivalPapDriverPed.ped, PCF_DontInfluenceWantedLevel, TRUE)	
			/*
			IF IS_PED_IN_GROUP(sRivalPapDriverPed.ped)
				REMOVE_PED_FROM_GROUP(sRivalPapDriverPed.ped)
			ENDIF
			SET_PED_RELATIONSHIP_GROUP_HASH(sRivalPapDriverPed.ped, relGroupEnemy)
			*/
			SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapDriverPed.model)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(sRivalPapPed.ped)
			sRivalPapPed.ped = 	CREATE_PED_INSIDE_VEHICLE(sRivalPapVehicle.vehicle, PEDTYPE_MISSION, sRivalPapPed.model, VS_FRONT_RIGHT)
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "rival pap create") ENDIF #ENDIF
		ENDIF
		IF IS_PED_UNINJURED(sRivalPapPed.ped)
			SET_PED_HELMET(sRivalPapPed.ped, FALSE)	// B*1296026
			//setup variation which doesn't have camera prop around the neck
			SET_PED_COMPONENT_VARIATION(sRivalPapPed.ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(sRivalPapPed.ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			SET_PED_COMPONENT_VARIATION(sRivalPapPed.ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(sRivalPapPed.ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(sRivalPapPed.ped, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)	//for camera prop swap to 1, 0, 0) //(task)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapPed.ped, TRUE)
			SET_PED_CAN_BE_TARGETTED(sRivalPapPed.ped, FALSE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_NEVER)
			SET_ENTITY_LOAD_COLLISION_FLAG(sRivalPapPed.ped, TRUE)
			SET_PED_CONFIG_FLAG(sRivalPapPed.ped, PCF_DontInfluenceWantedLevel, TRUE)
			ADD_PED_FOR_DIALOGUE(sDialogue, 4, sRivalPapPed.ped, "Paparazzo1RivalPap")
			/*
			IF IS_PED_IN_GROUP(sRivalPapPed.ped)
				REMOVE_PED_FROM_GROUP(sRivalPapPed.ped)
			ENDIF
			SET_PED_RELATIONSHIP_GROUP_HASH(sRivalPapPed.ped, relGroupEnemy)
			*/					
			IF NOT DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
				sObjCameraRivalPap.object = CREATE_OBJECT(sObjCameraRivalPap.model, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRivalPapPed.ped, << 0.0, 0.0, 2.5 >>))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_RIVAL_PAP_FOR_ENTRANCE - sObjCameraRivalPap.object - created in script not mocap   FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(sObjFlashUnitRivalPap.object)
				sObjFlashUnitRivalPap.vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRivalPapPed.ped, << 0.0, 0.0, 2.5 >>)
				sObjFlashUnitRivalPap.object = CREATE_OBJECT(sObjFlashUnitRivalPap.model, sObjFlashUnitRivalPap.vPosition)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_RIVAL_PAP_FOR_ENTRANCE - sObjFlashUnitRivalPap.object - created in script not mocap   FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(sObjCameraRivalPap.object)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(sObjFlashUnitRivalPap.object, sObjCameraRivalPap.object)
					ATTACH_ENTITY_TO_ENTITY(sObjFlashUnitRivalPap.object, sObjCameraRivalPap.object, 0, sObjFlashUnitRivalPap.vOffset, sObjFlashUnitRivalPap.vRotation)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_RIVAL_PAP_FOR_ENTRANCE - sObjFlashUnitRivalPap.object - ATTACHED to sObjCameraRivalPap.object in script not mocap   FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraRivalPap.object, sRivalPapPed.ped)
					ATTACH_ENTITY_TO_ENTITY(sObjCameraRivalPap.object, sRivalPapPed.ped, GET_PED_BONE_INDEX(sRivalPapPed.ped, BONETAG_PH_R_HAND), sObjCameraRivalPap.vOffset, sObjCameraRivalPap.vRotation)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CREATE_RIVAL_PAP_FOR_ENTRANCE - sObjCameraBeverly.object - ATTACHED to sBeverlyPed.ped in script not mocap   FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		
			TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)	// | AF_UPPERBODY)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapPed.model)
			SET_MODEL_AS_NO_LONGER_NEEDED(sObjCameraRivalPap.model)
			SET_MODEL_AS_NO_LONGER_NEEDED(sObjFlashUnitRivalPap.model)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    set the particle fx to start and the sound fx for rival taking a photo
PROC SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME()
	IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
		IF DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
			IF IS_ENTITY_ALIVE(sObjFlashUnitRivalPap.object)
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", sObjFlashUnitRivalPap.object, vCameraFlashFX_Offset, << 0.0, 0.0, 0.0 >>)	// B*1706190 - no roration offset, uses parent
			ENDIF
			IF NOT HAS_SOUND_FINISHED(iSoundId_CameraFlash)
				STOP_SOUND(iSoundId_CameraFlash)
			ENDIF 
			IF REQUEST_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)												
				PLAY_SOUND_FROM_COORD(iSoundId_CameraFlash, "CAMERA", GET_PED_BONE_COORDS(sRivalPapPed.ped, BONETAG_PH_R_HAND, vCameraFlashFX_Offset), sPap1_SoundSetName)
			ENDIF
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME : done this frame : ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles the Rival pap taking photos of Miranda
PROC MANAGE_RIVAL_TAKING_PHOTOS(FLOAT fRivalPapPlaybackTime)
	
	FLOAT fRivalPapTaskSweepAim_TurnRate = 3.0 	// default is 1.0, 2.0 would be twice as fast, etc		
	VECTOR vAimCameraPos
	IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
		vAimCameraPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vCelebPosInCarOffset)
	ENDIF
	
	SWITCH iRivalTakesPhotosBurst
		CASE 0
			IF fRivalPapPlaybackTime >= 0.0 //1200.0
				IF NOT IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
					TASK_LOOK_AT_COORD(sRivalPapPed.ped, vAimCameraPos, -1)
					iRivalPapCameraFlashTimer = GET_GAME_TIMER()
					TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vAimCameraPos, fRivalPapTaskSweepAim_TurnRate)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "**MANAGE_RIVAL_TAKING_PHOTOS : ", " RIVAL SNAP =+= CASE ", iRivalTakesPhotosBurst,
																	"sweep task applied at fRivalPapPlaybackTime : ", fRivalPapPlaybackTime) ENDIF #ENDIF
				ELSE
					UPDATE_TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, vAimCameraPos)					
					IF (GET_GAME_TIMER() - iRivalPapCameraFlashTimer > 1000)
						IF iNumFlashesForRivalCamera < 3
							IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
								SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "FLASH NUMBER - ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								iRivalPapCameraFlashTimer = GET_GAME_TIMER() - 550	// make repeat flashes quicker
								iNumFlashesForRivalCamera++
								IF iNumFlashesForRivalCamera >= 3
									iNumFlashesForRivalCamera = 0
									iRivalPapCameraFlashTimer = GET_GAME_TIMER()
									iRivalTakesPhotosBurst++
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took round of snaps [][][] : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iRivalPapCameraFlashTimer = GET_GAME_TIMER()
				IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF				
			ENDIF
		BREAK
		CASE 1			
			IF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")	// wait for the take photo anim to finish first
				IF HAS_ENTITY_ANIM_FINISHED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
					iRivalTakesPhotosBurst++
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "left gesture anim finished playing : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ELSE
					SET_ENTITY_ANIM_SPEED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap", 1.25)
				ENDIF
			ELIF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")	// wait for the take photo anim to finish first
				IF HAS_ENTITY_ANIM_FINISHED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")
					iRivalTakesPhotosBurst++
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "right gesture anim finished playing : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ELSE
					SET_ENTITY_ANIM_SPEED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap", 1.25)
				ENDIF
			ELSE
				// set the gesture anim to play
				IF (GET_GAME_TIMER() - iRivalPapCameraFlashTimer > 1000)
					SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", " set gesture anim playing : ") ENDIF #ENDIF
				ELSE
					IF NOT IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
						TASK_LOOK_AT_COORD(sRivalPapPed.ped, vAimCameraPos, -1)
						iRivalPapCameraFlashTimer = GET_GAME_TIMER()
						TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vAimCameraPos, fRivalPapTaskSweepAim_TurnRate)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "**MANAGE_RIVAL_TAKING_PHOTOS : ", " RIVAL SNAP CASE ", iRivalTakesPhotosBurst,
																		"sweep task applied at fRivalPapPlaybackTime : ", fRivalPapPlaybackTime) ENDIF #ENDIF
					ELSE
						UPDATE_TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, vAimCameraPos)	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF fRivalPapPlaybackTime >= 4500.0 //6100.0
				IF NOT IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
					TASK_LOOK_AT_COORD(sRivalPapPed.ped, vAimCameraPos, -1)
					iRivalPapCameraFlashTimer = GET_GAME_TIMER()
					TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vAimCameraPos, fRivalPapTaskSweepAim_TurnRate)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", " RIVAL SNAP =+= CASE ", iRivalTakesPhotosBurst,
																	"sweep task applied at fRivalPapPlaybackTime : ", fRivalPapPlaybackTime) ENDIF #ENDIF
				ELSE
					UPDATE_TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, vAimCameraPos)					
					IF (GET_GAME_TIMER() - iRivalPapCameraFlashTimer > 1200)
						IF iNumFlashesForRivalCamera < 4
							IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
								SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "FLASH NUMBER - ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								iRivalPapCameraFlashTimer = GET_GAME_TIMER() - 500	// make repeat flashes quicker
								iNumFlashesForRivalCamera++
								IF iNumFlashesForRivalCamera >= 4
									iNumFlashesForRivalCamera = 0
									iRivalPapCameraFlashTimer = GET_GAME_TIMER()
									iRivalTakesPhotosBurst++
									SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took round of snaps [][][][] : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iRivalPapCameraFlashTimer = GET_GAME_TIMER()
				
				IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF				
			ENDIF
		BREAK
		CASE 3
			IF fRivalPapPlaybackTime >= 9000.0 //10100.0
				IF NOT IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
					TASK_LOOK_AT_COORD(sRivalPapPed.ped, vAimCameraPos, -1)
					iRivalPapCameraFlashTimer = GET_GAME_TIMER()
					TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vAimCameraPos, fRivalPapTaskSweepAim_TurnRate)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", " RIVAL SNAP =+= CASE ", iRivalTakesPhotosBurst,
																	"sweep task applied at fRivalPapPlaybackTime : ", fRivalPapPlaybackTime) ENDIF #ENDIF
				ELSE
					UPDATE_TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, vAimCameraPos)					
					IF (GET_GAME_TIMER() - iRivalPapCameraFlashTimer > 1200)
						IF iNumFlashesForRivalCamera < 3
							IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
								SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "FLASH NUMBER - ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								iRivalPapCameraFlashTimer = GET_GAME_TIMER() - 500 // make repeat flashes quicker
								iNumFlashesForRivalCamera++
								IF iNumFlashesForRivalCamera >= 3
									iNumFlashesForRivalCamera = 0
									iRivalPapCameraFlashTimer = GET_GAME_TIMER()
									iRivalTakesPhotosBurst++
									SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took round of snaps [][][][] : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iRivalPapCameraFlashTimer = GET_GAME_TIMER()
				
				IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF				
			ENDIF
		BREAK
		CASE 4
			// don't take photos after this point
			IF fRivalPapPlaybackTime >= 17000.0 //13500.0
				IF IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
					CLEAR_PED_TASKS(sRivalPapPed.ped)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "cleared ped sweep task : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF
				IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF
				iRivalTakesPhotosBurst++
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap photo taking timed out : case++ ", iRivalTakesPhotosBurst) ENDIF #ENDIF
			ELIF fRivalPapPlaybackTime >= 13000.0 //13500.0
				IF NOT IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
					TASK_LOOK_AT_COORD(sRivalPapPed.ped, vAimCameraPos, -1)
					iRivalPapCameraFlashTimer = GET_GAME_TIMER()
					TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vAimCameraPos, fRivalPapTaskSweepAim_TurnRate)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", " RIVAL SNAP =+= CASE ", iRivalTakesPhotosBurst,
																	"sweep task applied at fRivalPapPlaybackTime : ", fRivalPapPlaybackTime) ENDIF #ENDIF
				ELSE
					UPDATE_TASK_SWEEP_AIM_POSITION(sRivalPapPed.ped, vAimCameraPos)					
					IF (GET_GAME_TIMER() - iRivalPapCameraFlashTimer > 1200)
						IF iNumFlashesForRivalCamera < 3
							IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
								SET_RIVAL_PAP_TAKE_PHOTO_THIS_FRAME()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "FLASH NUMBER - ", iRivalTakesPhotosBurst) ENDIF #ENDIF
								iRivalPapCameraFlashTimer = GET_GAME_TIMER() - 500
								iNumFlashesForRivalCamera++
								IF iNumFlashesForRivalCamera >= 3
									iNumFlashesForRivalCamera = 0
									iRivalPapCameraFlashTimer = GET_GAME_TIMER()
									iRivalTakesPhotosBurst++
									SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took round of snaps [][][][] : case++ ",
																										iRivalTakesPhotosBurst, " fRivalPapPlaybackTime = ", fRivalPapPlaybackTime) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iRivalPapCameraFlashTimer = GET_GAME_TIMER()
				
				IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
					TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap took idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
				ENDIF				
			ENDIF
		BREAK
		DEFAULT
			IF IsPedPerformingTask(sRivalPapPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
				CLEAR_PED_TASKS(sRivalPapPed.ped)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "cleared ped sweep task : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
			ENDIF
			IF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")	//revert to idle anims		
				TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_RIVAL_TAKING_PHOTOS : ", "rival pap idle anim applied : case = ", iRivalTakesPhotosBurst) ENDIF #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Play the sfx for successful hit during knock off bike attack
PROC PLAY_KNOCK_OFF_BIKER_ATTACK_HIT_SOUND()
	//hit sound sfx
	IF NOT HAS_SOUND_FINISHED(iSoundId_BikeAttack_MeleeSwipe)
		STOP_SOUND(iSoundId_BikeAttack_MeleeSwipe)
	ENDIF
	//swipe sound sfx
	IF REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_01_A")
		PLAY_SOUND_FROM_ENTITY(iSoundId_BikeAttack_MeleeHit, "HIT", sBeverlyPed.ped, sPap1_SoundSetName)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HIT sfx triggered") ENDIF #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "HIT sfx not triggered, audio bank not loaded!? ") ENDIF #ENDIF
	ENDIF
ENDPROC 

/// PURPOSE:
///    Used to decide if the attacker has moved ahead or behind of the est victims position.
///    This is used to determine if we should speed up or slow down the attack anim to compensate.
/// PARAMS:
///    vVictimForwardVector - 
///    vVictimCoords - 
///    vAttackerCoords - 
///    bFlipResultsForEntitiesHeadingInOppositeDirections - if TRUE returned results will be flipped
/// RETURNS:
///    Int depicting the forward side of the victim the attack is.  < 0 = behind, > 0 = infront, 0 = in line
FUNC FLOAT GET_FORWARD_SIDE_ATTACKER_IS_TO_VICTIM(VECTOR vVictimForwardPos, VECTOR vVictimCoords, VECTOR vAttackerCoords, BOOL bFlipResultsForEntitiesHeadingInOppositeDirections = TRUE)	
	FLOAT fSide = DOT_PRODUCT(vAttackerCoords - vVictimCoords, vVictimForwardPos)
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_FORWARD_SIDE_ATTACKER_IS_TO_VICTIM returning - ", fSide) ENDIF #ENDIF
	IF (fSide < 0)
		IF bFlipResultsForEntitiesHeadingInOppositeDirections
			RETURN ABSF(fSide)		//forward side = infront (convert to positive value)
		ELSE
    		RETURN fSide		//forward side = behind
		ENDIF
	ELIF (fSide > 0)
		IF bFlipResultsForEntitiesHeadingInOppositeDirections
			RETURN fSide * -1	//forward side = behind	(convert to negative value)
		ELSE
			RETURN fSide		//forward side = infront
		ENDIF
	ENDIF
	RETURN 0.0			//forward side - in line
ENDFUNC

/// PURPOSE:
///    Check if the pedIndexAttacker is estimated to reach the range needed to start the knock off bike attack anim
/// PARAMS:
///    pedIndexAttacker - ped attacking
///    vAttackerCoords - attacking ped's coords
///    vehIndexVictim - victim's vehicle
///    vVictimCoords - victim's vehicle's coords
///    fTimeTillHitStartPoint - the selected knock off biker anim's time to reach the successful attack start point
/// RETURNS:
///    TRUE if the pedIndexAttacker can reach the start attack range.
FUNC BOOL IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK(PED_INDEX pedIndexAttacker, VECTOR vAttackerCoords, VEHICLE_INDEX vehIndexVictim, VECTOR vVictimCoords, FLOAT fTimeTillHitStartPoint)
    VECTOR vNewVictimCoords = vVictimCoords + (GET_ENTITY_VELOCITY(vehIndexVictim) * fTimeTillHitStartPoint)
	VECTOR vNewAttackerCoords = vAttackerCoords + (GET_ENTITY_VELOCITY(pedIndexAttacker) * fTimeTillHitStartPoint)
	FLOAT fVictimHeading = GET_ENTITY_HEADING(vehIndexVictim)
    #IF IS_DEBUG_BUILD
		IF bDebug_ToggleKnockoffBikerDebugDraws
			DRAW_DEBUG_SPHERE(vNewVictimCoords, 0.2,  0, 200, 0, 50)
			DRAW_DEBUG_SPHERE(vNewAttackerCoords, 0.2,  0, 0, 200, 50)
		ENDIF
	#ENDIF
	//modify the X value of the far offset based off distance, this stop attacker swinging if in line but out to the side
	//but also allows attacker to wind up if their predicted isn't really close whilst far away
	VECTOR vKnockOff_Biker_ExecuteArea_OffsetFar = vKnockOff_Biker_HitExecutionArea_OffsetFar[iKnockOff_Biker_Attack_Side]
	VECTOR vKnockOff_Biker_ExecuteArea_OffsetNear = vKnockOff_Biker_HitExecutionArea_OffsetNear[iKnockOff_Biker_Attack_Side]
	FLOAT fX = vKnockOff_Biker_HitExecutionArea_OffsetFar[iKnockOff_Biker_Attack_Side].X / 2	//in the middle of the Hit Execution area
	FLOAT fY = vKnockOff_Biker_HitExecutionArea_OffsetFar[iKnockOff_Biker_Attack_Side].Y		// This value is the middle point of the execution area in the Y value
	VECTOR vDesiredOffset_FromVictim = << fX, fY, 0.0 >>	//in the middle of the Hit Execution area
	VECTOR vDesiredPos_AttackHitPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVictimCoords, fVictimHeading, vDesiredOffset_FromVictim)
	#IF IS_DEBUG_BUILD IF bDebug_ToggleKnockoffBikerDebugDraws DRAW_DEBUG_SPHERE(vDesiredPos_AttackHitPoint, 0.25,  200, 100, 200, 70) ENDIF #ENDIF
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK - vDesiredPos_AttackHitPoint for start attack = ", vDesiredPos_AttackHitPoint) ENDIF #ENDIF	
	VECTOR vVictimForwardPos = GET_ENTITY_FORWARD_VECTOR(vehIndexVictim)
	FLOAT fFowardDistanceFromDesiredHitPos = GET_FORWARD_SIDE_ATTACKER_IS_TO_VICTIM(vVictimForwardPos, vDesiredPos_AttackHitPoint, vAttackerCoords, bAreEntitiesHeadingsCompleteOpposite_KO_Biker)	
	IF fFowardDistanceFromDesiredHitPos < ((fKnockOff_Biker_HitExecutionArea_Length / 2) * -1)	//if attacker is behind victim, further than execute hit area
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK -  vKnockOff_Biker_ExecuteArea_OffsetFar.X pre modify = ",  vKnockOff_Biker_ExecuteArea_OffsetFar.X) ENDIF #ENDIF
		FLOAT fNewX = vKnockOff_Biker_ExecuteArea_OffsetFar.X
		IF iKnockOff_Biker_Attack_Side = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT	// need to convert to positive value, as right side will be negative
			fNewX = ABSF(fNewX)
		ENDIF
		fFowardDistanceFromDesiredHitPos = ABSF(fFowardDistanceFromDesiredHitPos)
		fNewX += ((fFowardDistanceFromDesiredHitPos / fKnockOff_Biker_StartAnimWidthModifier) * TIMESTEP())	
		IF fNewX > 7.0	//upper cap
			fNewX = 7.0
		ENDIF
		IF iKnockOff_Biker_Attack_Side = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT	// need to convert to negative value, as right side will be negative
			fNewX *= -1
		ENDIF
		vKnockOff_Biker_ExecuteArea_OffsetFar.X = fNewX
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK - vKnockOff_Biker_ExecuteArea_OffsetFar.X after modifier = ", vKnockOff_Biker_ExecuteArea_OffsetFar.X) ENDIF #ENDIF
	ENDIF
	vKnockOff_Biker_HitExecutionArea_PosNear = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vNewVictimCoords, fVictimHeading, vKnockOff_Biker_ExecuteArea_OffsetNear)
	vKnockOff_Biker_HitExecutionArea_PosFar = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vNewVictimCoords, fVictimHeading, vKnockOff_Biker_ExecuteArea_OffsetFar)
	//test if the attacker's predicted point is inside the area for executing the hit
	 #IF IS_DEBUG_BUILD IF bDebug_ToggleKnockoffBikerDebugDraws IS_POINT_IN_ANGLED_AREA(vNewAttackerCoords, vKnockOff_Biker_HitExecutionArea_PosNear, vKnockOff_Biker_HitExecutionArea_PosFar, fKnockOff_Biker_HitExecutionArea_Length, TRUE, TRUE) ENDIF #ENDIF //just to display area in debug
	IF IS_POINT_IN_ANGLED_AREA(vNewAttackerCoords, vKnockOff_Biker_HitExecutionArea_PosNear, vKnockOff_Biker_HitExecutionArea_PosFar, fKnockOff_Biker_HitExecutionArea_Length, FALSE, FALSE)
		#IF IS_DEBUG_BUILD IF bDebug_ToggleKnockoffBikerDebugDraws DRAW_RECT(0.85, 0.7, 0.05, 0.05, 0, 255, 0, 150) ENDIF #ENDIF
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK - returning - TRUE") ENDIF #ENDIF	
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_ToggleKnockoffBikerDebugDraws DRAW_RECT(0.85, 0.7, 0.05, 0.05, 255, 0, 0, 150) ENDIF #ENDIF
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK - returning - FALSE") ENDIF #ENDIF	
	ENDIF
	RETURN FALSE				
ENDFUNC

/// PURPOSE:
///    track if the attack has moved further ahead or behind the projected positin to land a successful attack
/// PARAMS:
///    pedIndexAttacker - ped attacking
///    vAttackerCoords - attacking ped's coords
///    vehIndexVictim - victim's vehicle
///    vVictimCoords - victim's vehicle's coords 
///    fTimeTillDesiredAnimHitPoint - amount of time till we reach the desired anim hit point
/// RETURNS:
///    Int depicting the forward side of the victim the attack is.  < 0 = behind, > 0 = infront, 0 = in line	
FUNC FLOAT GET_PED_UPDATED_FORWARD_SIDE_TO_KNOCKOFF_BIKER_ATTACK_HIT_POINT(PED_INDEX pedIndexAttacker, VECTOR vAttackerCoords, VEHICLE_INDEX vehIndexVictim, VECTOR vVictimCoords, FLOAT fTimeTillDesiredAnimHitPoint)
    VECTOR vNewVictimCoords = vVictimCoords + (GET_ENTITY_VELOCITY(vehIndexVictim) * fTimeTillDesiredAnimHitPoint)
	VECTOR vNewAttackerCoords = vAttackerCoords + (GET_ENTITY_VELOCITY(pedIndexAttacker) * fTimeTillDesiredAnimHitPoint)
    #IF IS_DEBUG_BUILD
		IF bDebug_ToggleKnockoffBikerDebugDraws
			//DRAW_DEBUG_SPHERE(vNewVictimCoords, 0.2,  0, 200, 0, 50)
			//DRAW_DEBUG_SPHERE(vNewAttackerCoords, 0.2,  0, 0, 200, 50)
		ENDIF
	#ENDIF
	#IF IS_DEBUG_BUILD
	FLOAT fVictimHeading = GET_ENTITY_HEADING(vehIndexVictim)
	FLOAT fX = vKnockOff_Biker_HitExecutionArea_OffsetFar[iKnockOff_Biker_Attack_Side].X / 2	//in the middle of the Hit Execution area
	FLOAT fY = vKnockOff_Biker_HitExecutionArea_OffsetFar[iKnockOff_Biker_Attack_Side].Y		// This value is the middle point of the execution area in the Y value
	VECTOR vDesiredOffset_FromVictim = << fX, fY, 0.0 >>	//in the middle of the Hit Execution area
	VECTOR vDesiredPos_AttackHitPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vNewVictimCoords, fVictimHeading, vDesiredOffset_FromVictim)
	#ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_ToggleKnockoffBikerDebugDraws DRAW_DEBUG_SPHERE(vDesiredPos_AttackHitPoint, 0.25,  200, 100, 200, 70) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_PED_UPDATED_RANGE_TO_KNOCKOFF_BIKER_ATTACK_HIT_POINT - vDesiredPos_AttackHitPoint = ", vDesiredPos_AttackHitPoint) ENDIF #ENDIF	
	VECTOR vVictimForwardPos = GET_ENTITY_FORWARD_VECTOR(vehIndexVictim)
	//RETURN GET_FORWARD_SIDE_ATTACKER_IS_TO_VICTIM(vVictimForwardPos, vDesiredPos_AttackHitPoint, vNewAttackerCoords, bAreEntitiesHeadingsCompleteOpposite_KO_Biker) // vNewVictimCoords, vNewAttackerCoords, bAreEntitiesHeadingsCompleteOpposite_KO_Biker)	
	RETURN GET_FORWARD_SIDE_ATTACKER_IS_TO_VICTIM(vVictimForwardPos, vNewVictimCoords, vNewAttackerCoords, bAreEntitiesHeadingsCompleteOpposite_KO_Biker)	
ENDFUNC

/// PURPOSE:
///    Used in Knock off biker attack.  The total anim time appears to be constant, regardless of the anim speed
///    so this recalculates the anim phase's time taking into account the anim speed
/// PARAMS:
///    fAnimPhase - phase of the anim, must be between 0.0 and 1.0 to be valid
///    fTotalAnimTime - 
///    fAnimSpeed - 
/// RETURNS:
///    Time in milliseconds
FUNC FLOAT GET_ANIM_TIME_FROM_ANIM_PHASE(FLOAT fAnimPhase, FLOAT fTotalAnimTime, FLOAT fAnimSpeed)
	IF fAnimPhase < 0.0
	OR fAnimPhase > 1.0
		SCRIPT_ASSERT("Paparazzo 1 - GET_ANIM_TIME_FROM_ANIM_PHASE - invalid anim phase")
	ENDIF
	IF fAnimSpeed < 0.0
		SCRIPT_ASSERT("Paprazzo 1 - GET_ANIM_TIME_FROM_ANIM_PHASE - invalid anim speed")
	ENDIF
	RETURN ((fAnimPhase * fTotalAnimTime) * fAnimSpeed)
ENDFUNC

/// PURPOSE:
///    Test to see if the given ped's arm or hand is intersecting the given victim ped's body
///    uses ped bone positions with simple VDIST2 test followed by Z diff test
/// PARAMS:
///    vehIndexVictim - vehicle to base checks off
///    vehIndexAttacker - vehicle to base updated position checks off
///    bHasPassenger - test for ped passenger on bike aswell
/// RETURNS:
///    TRUE if attacking peds hand or arm is within range to be classed as intersecting the peds on the bike
FUNC BOOL IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK(PED_INDEX pedIndexAttacker, VEHICLE_INDEX vehIndexAttacker, VEHICLE_INDEX vehIndexVictim, BOOL bHasPassenger = FALSE, OBJECT_INDEX objIndexAttackerWeapon = NULL)		
	IF NOT IS_ENTITY_ALIVE(pedIndexAttacker)
		RETURN FALSE
	ENDIF
	IF NOT IS_ENTITY_ALIVE(vehIndexAttacker)
		RETURN FALSE
	ENDIF	
	IF NOT IS_ENTITY_ALIVE(vehIndexVictim)
		RETURN FALSE
	ENDIF
	VECTOR vAttackerHandPos
	VECTOR vAttackerArmPos	
	VECTOR vAttackerWeaponPos
	IF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_LEFT	
		vAttackerHandPos = GET_PED_BONE_COORDS(pedIndexAttacker, BONETAG_PH_L_HAND, << 0.0, 0.0, 0.0 >>)		// potentially need to be using 	BONETAG_PH_L_HAND 
		vAttackerArmPos = GET_PED_BONE_COORDS(pedIndexAttacker, BONETAG_L_FOREARM, << 0.0, 0.0, 0.0 >>)		
	ELIF iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
		vAttackerHandPos = GET_PED_BONE_COORDS(pedIndexAttacker, BONETAG_PH_R_HAND, << 0.0, 0.0, 0.0 >>) 		// potentially need to be using 	NETAG_PH_R_HAND
		vAttackerArmPos = GET_PED_BONE_COORDS(pedIndexAttacker, BONETAG_R_FOREARM, << 0.0, 0.0, 0.0 >>)	
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN FALSE - invalid attacking arm") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	// ped bone coords are behind on update, when travelling at speed, so having to calculate expected position based off velocity staying roughly since last frame
	VECTOR vVelocity = GET_ENTITY_VELOCITY(vehIndexAttacker)
	FLOAT fTime = GET_FRAME_TIME()
	VECTOR vPredictedPos
	//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "Velocity = ", vVelocity, "  Time = ", fTime) ENDIF #ENDIF
	vPredictedPos = vVelocity * fTime
	vAttackerHandPos += vPredictedPos
	vAttackerArmPos += vPredictedPos	
	VECTOR vPelvisPosOffset
	VECTOR vEstimatedVictimPelvisPos
	FLOAT fHeightMaxCap
	FLOAT fHeightMinCap
	FLOAT fHeightMaxCapDiff
	FLOAT fPelvisRadius = 0.425
	FLOAT fHandRadius = 0.08 // 0.075 good representation, although increased to try and make hit chance better 
	FLOAT fArmRadius = 0.08 // 0.075 good representation, although increased to try and make hit chance better 	
	FLOAT fWeaponRadius = 0.8 // 0.075 good representation, although increased to try and make hit chance better 
	//Driver 	
	vPelvisPosOffset = << 0.0, 0.15, 0.4 >>		// << 0.0, 0.15, 0.6 >> lowered Z as can look like it hit when motorbike is pitched
	#IF IS_DEBUG_BUILD
		IF bDebug_DoArmIntersectionTest
			vPelvisPosOffset = vDebug_PelvisPosOffsetDriver
		ENDIF
	#ENDIF
	vEstimatedVictimPelvisPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIndexVictim, vPelvisPosOffset)
	fHeightMaxCapDiff = 0.6
	fHeightMaxCap = vEstimatedVictimPelvisPos.z + fHeightMaxCapDiff	// additional height grabbed in game
	fHeightMinCap = vEstimatedVictimPelvisPos.z	
	IF IS_ENTITY_ALIVE(objIndexAttackerWeapon)
		vAttackerWeaponPos = GET_ENTITY_COORDS(objIndexAttackerWeapon)
		vAttackerWeaponPos += vPredictedPos	
	ENDIF
	#IF IS_DEBUG_BUILD
		IF bDebug_DoArmIntersectionTest
			//fPelvisRadius = fDebug_PelvisRadius
			DRAW_DEBUG_SPHERE(vEstimatedVictimPelvisPos, fPelvisRadius, 50, 255, 0, 120)	
			DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMinCap >>, 0, 255, 0, 200)
			DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMaxCap >>, 1.0, 0, 255, 0, 200)
			//fHandRadius = fDebug_HandRadius
			DRAW_DEBUG_SPHERE(vAttackerHandPos, fHandRadius, 0, 0, 255, 120)
			//fArmRadius = fDebug_ArmRadius
			DRAW_DEBUG_SPHERE(vAttackerArmPos, fArmRadius, 0, 0, 255, 120)	
			IF IS_ENTITY_ALIVE(objIndexAttackerWeapon)
				DRAW_DEBUG_SPHERE(vAttackerWeaponPos, fWeaponRadius, 0, 255, 100, 100)
			ENDIF
		ENDIF 
	#ENDIF	
	IF GET_DISTANCE_BETWEEN_COORDS(vAttackerHandPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fHandRadius)
		IF vAttackerHandPos.z <= fHeightMaxCap	// test hand height is below est victim head height
			IF vAttackerHandPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - hand hits driver") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF GET_DISTANCE_BETWEEN_COORDS(vAttackerArmPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fArmRadius)	
		IF vAttackerArmPos.z <= fHeightMaxCap	// test hand height is below est victim head height
			IF vAttackerArmPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - arm hits driver") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(objIndexAttackerWeapon)
		IF IS_ENTITY_IN_RANGE_COORDS(objIndexAttackerWeapon, vEstimatedVictimPelvisPos, (fPelvisRadius + fWeaponRadius))
			IF vAttackerWeaponPos.z <= fHeightMaxCap	// test hand height is below est victim head height
				IF vAttackerWeaponPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - weapon hits driver") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	// Passenger	
	IF bHasPassenger
		vPelvisPosOffset = << 0.0, -0.55, 0.45 >>		// << 0.0, -0.55, 0.65 >> lowered Z as can look like it hit when motorbike is pitched
		#IF IS_DEBUG_BUILD
			IF bDebug_DoArmIntersectionTest
				vPelvisPosOffset = vDebug_PelvisPosOffsetPassenger
			ENDIF
		#ENDIF
		vEstimatedVictimPelvisPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIndexVictim, vPelvisPosOffset)
		fHeightMaxCapDiff = 0.85
		fHeightMaxCap = vEstimatedVictimPelvisPos.z + fHeightMaxCapDiff	// additional height grabbed in game
		fHeightMinCap = vEstimatedVictimPelvisPos.z		
		#IF IS_DEBUG_BUILD
			IF bDebug_DoArmIntersectionTest
				//fPelvisRadius = fDebug_PelvisRadius
				DRAW_DEBUG_SPHERE(vEstimatedVictimPelvisPos, fPelvisRadius, 50, 255, 0, 120)
				DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMinCap >>, 1.0, 0, 255, 0, 200)
				DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMaxCap >>, 1.0, 0, 255, 0, 200)
			ENDIF
		#ENDIF		
		IF GET_DISTANCE_BETWEEN_COORDS(vAttackerHandPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fHandRadius)
			IF vAttackerHandPos.z <= fHeightMaxCap	// test hand height is below est victim head height
				IF vAttackerHandPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - hand hits passenger") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF		
		IF GET_DISTANCE_BETWEEN_COORDS(vAttackerArmPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fArmRadius)
			IF vAttackerArmPos.z <= fHeightMaxCap	// test hand height is below est victim head height
				IF vAttackerArmPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - arm hits passenger") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_ENTITY_ALIVE(objIndexAttackerWeapon)
			IF IS_ENTITY_IN_RANGE_COORDS(objIndexAttackerWeapon, vEstimatedVictimPelvisPos, (fPelvisRadius + fWeaponRadius))
				IF vAttackerWeaponPos.z <= fHeightMaxCap	// test hand height is below est victim head height
					IF vAttackerWeaponPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - weapon hits passenger") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		// additional check area between the driver and passenger where detection would slip through
		vPelvisPosOffset = << 0.0, -0.35, 0.42 >>		// pos, mid between driver and passenger
		#IF IS_DEBUG_BUILD
			IF bDebug_DoArmIntersectionTest
				vPelvisPosOffset = vDebug_PelvisPosOffsetPassenger
			ENDIF
		#ENDIF
		vEstimatedVictimPelvisPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehIndexVictim, vPelvisPosOffset)
		fHeightMaxCapDiff = 0.85
		fHeightMaxCap = vEstimatedVictimPelvisPos.z + fHeightMaxCapDiff	// additional height grabbed in game
		fHeightMinCap = vEstimatedVictimPelvisPos.z		
		#IF IS_DEBUG_BUILD
			IF bDebug_DoArmIntersectionTest
				//fPelvisRadius = fDebug_PelvisRadius
				DRAW_DEBUG_SPHERE(vEstimatedVictimPelvisPos, fPelvisRadius, 50, 150, 0, 100)
				DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMinCap >>, 1.0, 0, 150, 0, 200)
				DRAW_DEBUG_CROSS(<< vEstimatedVictimPelvisPos.X, vEstimatedVictimPelvisPos.Y, fHeightMaxCap >>, 1.0, 0, 150, 0, 200)
			ENDIF
		#ENDIF		
		IF GET_DISTANCE_BETWEEN_COORDS(vAttackerHandPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fHandRadius)
			IF vAttackerHandPos.z <= fHeightMaxCap	// test hand height is below est victim head height
				IF vAttackerHandPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - hand hits section between driver and passenger") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF		
		IF GET_DISTANCE_BETWEEN_COORDS(vAttackerArmPos, vEstimatedVictimPelvisPos, FALSE) < (fPelvisRadius + fArmRadius)
			IF vAttackerArmPos.z <= fHeightMaxCap	// test hand height is below est victim head height
				IF vAttackerArmPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - arm hits section between driver and passenger") ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_ENTITY_ALIVE(objIndexAttackerWeapon)
			IF IS_ENTITY_IN_RANGE_COORDS(objIndexAttackerWeapon, vEstimatedVictimPelvisPos, (fPelvisRadius + fWeaponRadius))
				IF vAttackerWeaponPos.z <= fHeightMaxCap	// test hand height is below est victim head height
					IF vAttackerWeaponPos.z >= fHeightMinCap //test hand height is heigher est victim pelvis height
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK : ", "RETURN TRUE - weapon hits hits section between driver and passenger") ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Check if the ped is ok to perform the attack///
/// PARAMS:
///    pedIndex - to test
/// RETURNS:
///    TRUE - if the ped exists, is not injured and
///    If player ped, checks for phone being on screen and
///    Checks the ped is sitting on a vehicle and on a bike
FUNC BOOL IS_PED_OK_TO_PERFORM_KNOCKOFF_BIKER_ATTACK(PED_INDEX pedIndex)
	IF IS_PED_UNINJURED(sBeverlyPed.ped)		
		IF pedIndex = PLAYER_PED_ID()
			IF IS_PHONE_ONSCREEN()
				RETURN FALSE
			ENDIF
		ENDIF	
		IF (IS_PED_ON_ANY_BIKE(pedIndex) AND IS_PED_SITTING_IN_ANY_VEHICLE(pedIndex))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Tests if two given entitie's (entityIndex01, entityIndex02) headings are either similiar or completely opposite
///    NOTE: doesn't check entities exist or if they are alive
/// PARAMS:
///    entityIndex01 - First entity
///    entityIndex02 - Second entity to test against
///    fThreshold - 1.0 represents both entities facing the exact same way.  0.0 is at 90degree angle
///    bHeadingsAreCompleteOpposite - returning bool to know if the entities are completely opposite
/// RETURNS:
///    TRUE - if entities headings are similar or completely opposite within the threshold
FUNC BOOL ARE_ENTITIES_HEADINGS_SIMILAR_OR_COMPLETE_OPPOSITE_WITHIN_THRESHOLD(ENTITY_INDEX entityIndex01, ENTITY_INDEX entityIndex02, FLOAT fThreshold, BOOL &bHeadingsAreCompleteOpposite)	
	VECTOR v1, v2
	FLOAT f1
	FLOAT fUpperThreshold
	fUpperThreshold = 1.0	//upper threshold is always 1.0
	v1 = GET_ENTITY_FORWARD_VECTOR(entityIndex01)
	v2 = GET_ENTITY_FORWARD_VECTOR(entityIndex02)
	f1 = DOT_PRODUCT(v1, v2)	
	IF f1 > 0	//facing same 180 heading
		bHeadingsAreCompleteOpposite = FALSE
	ELSE
		bHeadingsAreCompleteOpposite = TRUE
		f1 = ABSF(f1)	//turn into a positive value
	ENDIF	
	IF (f1 > fThreshold
	AND f1 < fUpperThreshold)
		IF NOT bHeadingsAreCompleteOpposite
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "^^^  - COMPARE HEADINGS ARE SIMILAR BOTH FORWARDS >>>>>>>>>>>>>>>>  = ", f1) ENDIF #ENDIF
		ELSE
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "^^^  - COMPARE HEADINGS ARE SIMILAR BOTH BACKWARDS >>>>>>>>>>>>>>>>  = ", f1) ENDIF #ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "^^^  - COMPARE HEADINGS  ARE NOT SIMILAR	-------  = ", f1) ENDIF #ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles ped performing knock off biker attack
/// PARAMS:
///    pedIndexAttacker - ped to perform attack
///    vehIndexVictim - vehicle to be attacked
///    objIndexAttackerWeapon - weapon to specify for attack
PROC MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK(PED_INDEX pedIndexAttacker, VEHICLE_INDEX vehIndexVictim, OBJECT_INDEX objIndexAttackerWeapon = NULL)				
	IF NOT IS_PED_OK_TO_PERFORM_KNOCKOFF_BIKER_ATTACK(pedIndexAttacker)
		//reset if ped is not ok for the attack
		eKnockOffBikerAttackState = KBAS_SETUP_ATTACK
	ELSE	
		IF IS_VEHICLE_OK(vehIndexVictim)		
			VECTOR vAttackerCoords = GET_ENTITY_COORDS(pedIndexAttacker)
			VECTOR vVictimCoords = GET_ENTITY_COORDS(vehIndexVictim)	
			//FLOAT fTimeAttackReachVictim
			FLOAT fDesiredAnimHitTime
			SWITCH eKnockOffBikerAttackState				
				CASE KBAS_SETUP_ATTACK
					IF NOT IS_ENTITY_PLAYING_ANIM(pedIndexAttacker, sAnimDictName_Pap1Specific, "idle")
						// reset Beverly's idle anim
						BOOL bSafeToResetIdleAnim
						bSafeToResetIdleAnim = TRUE
						// don't start taking a photo if we are playing a hurry up anim already
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")
							// make sure the anim has completed (has the hold last frame flag
							IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")
								bSafeToResetIdleAnim = FALSE
							ENDIF
						ENDIF
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
							// make sure the anim has completed (has the hold last frame flag
							IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
								bSafeToResetIdleAnim = FALSE
							ENDIF
						ENDIF
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
							// make sure the anim has completed (has the hold last frame flag
							IF NOT HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
								bSafeToResetIdleAnim = FALSE
							ENDIF
						ENDIF
						IF bSafeToResetIdleAnim
							TASK_PLAY_ANIM(pedIndexAttacker, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	// !! needs to stay normal blends otherwise throughs timings out for hit
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK - RESET BEVERLY TO IDLE POSE") ENDIF #ENDIF
						ENDIF
					ENDIF
					//decide which side the attack is to the victim
					iKnockOff_Biker_Attack_Side = GET_SIDE_COORDS_IS_TO_ENTITY(vehIndexVictim, vVictimCoords, vAttackerCoords)
					//reject attack if angle between entities is too big
					IF ARE_ENTITIES_HEADINGS_SIMILAR_OR_COMPLETE_OPPOSITE_WITHIN_THRESHOLD(pedIndexAttacker, vehIndexVictim, 
														KO_BIKER_START_ATTACK_HEADING_THRESOLD, bAreEntitiesHeadingsCompleteOpposite_KO_Biker)	
						//flip the attack side if entities are facing opposite directions
						IF bAreEntitiesHeadingsCompleteOpposite_KO_Biker						
							IF iKnockOff_Biker_Attack_Side = KNOCKOFF_BIKER_ATTACK_SIDE_LEFT
								iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "flipped iKnockOff_Biker_Attack_AnimSide to KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT") ENDIF #ENDIF		
							ELIF iKnockOff_Biker_Attack_Side = KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
								iKnockOff_Biker_Attack_AnimSide = KNOCKOFF_BIKER_ATTACK_SIDE_LEFT
								//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "flipped iKnockOff_Biker_Attack_AnimSide to KNOCKOFF_BIKER_ATTACK_SIDE_LEFT") ENDIF #ENDIF		
							ENDIF
						ELSE
							iKnockOff_Biker_Attack_AnimSide = iKnockOff_Biker_Attack_Side
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "iKnockOff_Biker_Attack_AnimSide set same as iKnockOff_Biker_Attack_Side") ENDIF #ENDIF	
						ENDIF
						// attacker is directly inslide with the victim
						IF iKnockOff_Biker_Attack_Side = KNOCKOFF_BIKER_ATTACK_SIDE_NULL									
							EXIT //setup a check to see which side of the victim the target ped is heading towards? Just abort for now
						ENDIF
						// setup the anim info
						SWITCH iKnockOff_Biker_Attack_AnimSide									
							CASE KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
								//33.3 % chance of punch anim, else swipe.  Swipe is a better anim so more likely to play
								IF (GET_RANDOM_INT_IN_RANGE(0, 3) = 0)						
									sAnimNameBeverlyKnockOff = "atk_punch_left_pap"		// anims have been tagged up with wrong direction! So Left - Right and visa versa						
									fKnockOff_Biker_InitialAnimSpeed = 1.0
									fKnockOff_Biker_Anim_HitStartPoint = 0.21	// 0.25
									fKnockOff_Biker_Anim_HitDonePoint = 0.4		// 0.33
									fKnockOff_Biker_Anim_SwipeSFXstartPoint = 0.2
									fKnockOff_Biker_Anim_TotalTime = 1599.999878
								ELSE	// 66.6 % chance of swipe anim
									sAnimNameBeverlyKnockOff = "atk_swipe_left_pap" 	// anims have been tagged up with wrong direction! So Left - Right and visa versa								
									fKnockOff_Biker_InitialAnimSpeed = 1.0
									fKnockOff_Biker_Anim_HitStartPoint = 0.235	// 0.28
									fKnockOff_Biker_Anim_HitDonePoint = 0.38	// 0.33
									fKnockOff_Biker_Anim_SwipeSFXstartPoint = 0.25
									fKnockOff_Biker_Anim_TotalTime = 1933.333130
								ENDIF																			
							BREAK
							CASE KNOCKOFF_BIKER_ATTACK_SIDE_LEFT	
								//33.3 % chance of punch anim, else swipe.  Swipe is a better anim so more likely to play
								IF (GET_RANDOM_INT_IN_RANGE(0, 3) = 0)
									sAnimNameBeverlyKnockOff = "atk_punch_right_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa						
									fKnockOff_Biker_InitialAnimSpeed = 1.0
									fKnockOff_Biker_Anim_HitStartPoint = 0.27	// 0.3
									fKnockOff_Biker_Anim_HitDonePoint = 0.45	// 0.36
									fKnockOff_Biker_Anim_SwipeSFXstartPoint = 0.25
									fKnockOff_Biker_Anim_TotalTime = 1366.666748
								ELSE	// 66.6 % chance of swipe anim
									sAnimNameBeverlyKnockOff = "atk_swipe_right_pap"	// anims have been tagged up with wrong direction! So Left - Right and visa versa							
									fKnockOff_Biker_InitialAnimSpeed = 1.0
									fKnockOff_Biker_Anim_HitStartPoint = 0.24	// 0.28
									fKnockOff_Biker_Anim_HitDonePoint = 0.38	// 0.33
									fKnockOff_Biker_Anim_SwipeSFXstartPoint = 0.25
									fKnockOff_Biker_Anim_TotalTime = 1799.999878
								ENDIF										
							BREAK								
							DEFAULT
								SCRIPT_ASSERT("RC - PAP 1 - invalid side for KO - iKnockOff_Biker_Attack_AnimSide")
							BREAK
						ENDSWITCH
						#IF IS_DEBUG_BUILD
							DEBUG_OVERRIDE_KNOCK_OFF_BIKER_ANIM_SETUP()
						#ENDIF
						//calculate the desired time it should take to land the hit
						fDesiredAnimHitTime = GET_ANIM_TIME_FROM_ANIM_PHASE(fKnockOff_Biker_Anim_HitStartPoint, fKnockOff_Biker_Anim_TotalTime, fKnockOff_Biker_InitialAnimSpeed)
						fDesiredAnimHitTime = fDesiredAnimHitTime / 1000 // convert from milliseconds
						//check if the attacker is close enough to begin the attack given the time it will take to land the hit
						IF IS_PED_IN_RANGE_FOR_START_KNOCKOFF_BIKER_ATTACK(pedIndexAttacker, vAttackerCoords, vehIndexVictim, vVictimCoords, fDesiredAnimHitTime)
							fKnockOff_Biker_UpdatedAnimSpeed = fKnockOff_Biker_InitialAnimSpeed
							TASK_PLAY_ANIM(pedIndexAttacker, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, FAST_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_HOLD_LAST_FRAME)					
							bDoneDialogue_BeverlyThrowsPunch = FALSE	// setup dialogue for punching to play
							bDone_KnockOff_Biker_SwipeSoundFX = FALSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_SETUP_ATTACK going to KBAS_RUNNING_ATTACK with ", sAnimNameBeverlyKnockOff, "  +++") ENDIF #ENDIF						
							eKnockOffBikerAttackState = KBAS_RUNNING_ATTACK
						ELSE
							bDoneDialogue_BeverlyThrowsPunch = TRUE	// reset the dialogue for throwing punch (true by default)
						ENDIF
					ENDIF										
				BREAK
				CASE KBAS_RUNNING_ATTACK
					// swing ended unsuccessfully (something went wrong since he was tasked to hold the last frame of the anim
					IF NOT IS_ENTITY_PLAYING_ANIM(pedIndexAttacker, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
						bStatTracker_HasBeverlyMissedWithSwing = TRUE
						bDoneDialogue_BeverlyMissedKnockOffAttempt = FALSE	//reset the failed knock off dialogue so it can trigger
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  Ped not playing attack anim so - KBAS_SETUP_ATTACK  +++") ENDIF #ENDIF
						eKnockOffBikerAttackState = KBAS_SETUP_ATTACK
					ELSE
						// swing ended unsuccessfully
						IF HAS_ENTITY_ANIM_FINISHED(pedIndexAttacker, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
							bDoneDialogue_BeverlyMissedKnockOffAttempt = FALSE	//reset the failed knock off dialogue so it can trigger
							bStatTracker_HasBeverlyMissedWithSwing = TRUE
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  Ped attack anim finished so - KBAS_SETUP_ATTACK  +++") ENDIF #ENDIF
							eKnockOffBikerAttackState = KBAS_SETUP_ATTACK							
						ELSE
							// swing anim is playing
							FLOAT fCurrentAnimPhase
							fCurrentAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME(pedIndexAttacker, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)	
							//constain to correct anim phase time since it can initial throw -1
							IF fCurrentAnimPhase >= 0.0 AND fCurrentAnimPhase <= 1.0	
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  attack anim current phase = ", fCurrentAnimPhase) ENDIF #ENDIF
								FLOAT fDesiredAnimHitPhase
								FLOAT fAnimPhaseDiff	// Tweak 1 added 23/01/2013 so I can test fDesiredAnimHitPhase again at the end of this block for 
								fDesiredAnimHitPhase = ((fKnockOff_Biker_Anim_HitStartPoint + fKnockOff_Biker_Anim_HitDonePoint) / 2)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  desired hit phase =", fDesiredAnimHitPhase) ENDIF #ENDIF		
								//only change the speed if we haven't reached end of the hit point phase
								IF fCurrentAnimPhase < fKnockOff_Biker_Anim_HitDonePoint	// Tweak 1 fDesiredAnimHitPhase	changed 23/01/2013 - changed because it could still land past fDesiredAnimHitPhase, so we should still try to land it  Problems with below convertions then
									fAnimPhaseDiff = fDesiredAnimHitPhase - fCurrentAnimPhase
									// is the animation past the desired anim hit point or not? if so value will need converting from a negative
									IF fCurrentAnimPhase > fDesiredAnimHitPhase
										fAnimPhaseDiff = ABSF(fAnimPhaseDiff)
									ENDIF
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  desired hit phase minus the current phase = ", fDesiredAnimHitPhase) ENDIF #ENDIF
									fDesiredAnimHitTime = GET_ANIM_TIME_FROM_ANIM_PHASE(fAnimPhaseDiff, fKnockOff_Biker_Anim_TotalTime, fKnockOff_Biker_UpdatedAnimSpeed)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  desired hit time =", fDesiredAnimHitTime) ENDIF #ENDIF
									// track if the attack has moved ahead of the projected hit point
									FLOAT fUpdatedForwardSide
									fUpdatedForwardSide = GET_PED_UPDATED_FORWARD_SIDE_TO_KNOCKOFF_BIKER_ATTACK_HIT_POINT(pedIndexAttacker, vAttackerCoords, vehIndexVictim, vVictimCoords, fDesiredAnimHitTime)
									// flip the results if the current anim phase is past the desired anim hit point	Tweak 1 added 23/01/2013 added so i can check if the anim was past the desired hit zone
									IF fCurrentAnimPhase > fDesiredAnimHitPhase
										IF fUpdatedForwardSide < 0.0			// attack is behind of projected hit point
											fUpdatedForwardSide = 1.0
										ELIF fUpdatedForwardSide > 0.0			// attack is now ahead of projected hit point
											fUpdatedForwardSide = -1.0
										ENDIF
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  fCurrentAnimPhase > fDesiredAnimHitPhase so fUpdatedForwardSide flipped to ", fUpdatedForwardSide, " frame_count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
									ENDIF									
									FLOAT fNewAnimSpeed	
									IF fUpdatedForwardSide < 0.0							// attack is behind of projected hit point so slow down anim
										fNewAnimSpeed = fKnockOff_Biker_UpdatedAnimSpeed * 0.90
									ELIF fUpdatedForwardSide > 0.0							// attack is now ahead of projected hit point so speed up anim
										fNewAnimSpeed = fKnockOff_Biker_UpdatedAnimSpeed * 1.10
									ELSE
										fNewAnimSpeed = fKnockOff_Biker_UpdatedAnimSpeed	// attacker is in line with projected hit point
									ENDIF
									IF fNewAnimSpeed < (fKnockOff_Biker_InitialAnimSpeed + 0.4)		//only change the speed if it's with in the thresholds
									AND fNewAnimSpeed > (fKnockOff_Biker_InitialAnimSpeed - 0.4)
										fKnockOff_Biker_UpdatedAnimSpeed = fNewAnimSpeed
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  anim speed updated to = ", fKnockOff_Biker_UpdatedAnimSpeed) ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
							// set the updated anim speed
							SET_ENTITY_ANIM_SPEED(pedIndexAttacker, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, fKnockOff_Biker_UpdatedAnimSpeed)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  attack anim speed set = ", fKnockOff_Biker_UpdatedAnimSpeed) ENDIF #ENDIF							
							//swipe sound sfx
							IF NOT bDone_KnockOff_Biker_SwipeSoundFX
								IF (fCurrentAnimPhase >= fKnockOff_Biker_Anim_SwipeSFXstartPoint)									
									IF REQUEST_SCRIPT_AUDIO_BANK("PAPARAZZO_01_A")
										PLAY_SOUND_FROM_ENTITY(iSoundId_BikeAttack_MeleeSwipe, "SWING", sBeverlyPed.ped, sPap1_SoundSetName)
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK -  SWIPE sfx triggered  +++") ENDIF #ENDIF
									ELSE
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK - SWIPE sfx not triggered, audio bank not loaded!?  +++") ENDIF #ENDIF
									ENDIF
									bDone_KnockOff_Biker_SwipeSoundFX = TRUE
								ENDIF
							ENDIF								
							//can only successfully land the attack if the anim phase is whilst the arm is swinging out
							IF (fCurrentAnimPhase >= fKnockOff_Biker_Anim_HitStartPoint)
							AND (fCurrentAnimPhase <= fKnockOff_Biker_Anim_HitDonePoint)							
								//IF IS_PED_IN_KNOCKOFF_BIKER_ATTACK_HIT_AREA(pedIndexAttacker, vehIndexVictim)	
								IF IS_PED_ARM_INTERSECTING_FOR_KNOCKOFF_BIKER_ATTACK(pedIndexAttacker, sBeverlyVehicle.vehicle, vehIndexVictim, TRUE, objIndexAttackerWeapon)
									BOOL bLocal_AreEntitiesHeadingCompleteOpposite
									IF ARE_ENTITIES_HEADINGS_SIMILAR_OR_COMPLETE_OPPOSITE_WITHIN_THRESHOLD(pedIndexAttacker, vehIndexVictim, 
															KO_BIKER_EXECUTE_ATTACK_HEADING_THRESOLD, bLocal_AreEntitiesHeadingCompleteOpposite)
										IF bAreEntitiesHeadingsCompleteOpposite_KO_Biker = bLocal_AreEntitiesHeadingCompleteOpposite
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	-  KBAS_RUNNING_ATTACK - going to KBAS_SUCCESSFUL_ATTACK  +++") ENDIF #ENDIF
											eKnockOffBikerAttackState = KBAS_SUCCESSFUL_ATTACK											
											#IF IS_DEBUG_BUILD
												IF bDebug_DontKnockThePedsOffTheBike
													PLAY_KNOCK_OFF_BIKER_ATTACK_HIT_SOUND()
													DRAW_RECT(0.2, 0.5, 0.2, 0.4, 250, 250, 250, 100)						
													#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK : ", "successful hit DEBUG don't knock off, anim phase is = ", fCurrentAnimPhase, " frame_count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
													eKnockOffBikerAttackState = KBAS_RUNNING_ATTACK		
													EXIT
												ENDIF
											#ENDIF
										ELSE
											//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- KBAS_RUNNING_ATTACK -  Attack failed as headings have completely changed  +++") ENDIF #ENDIF
										ENDIF
									ELSE
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- KBAS_RUNNING_ATTACK -  Attack failed as headings comparison is not withing threshold  +++") ENDIF #ENDIF
									ENDIF
								ENDIF								
							ENDIF
						ENDIF
					ENDIF					
				BREAK
				CASE KBAS_SUCCESSFUL_ATTACK				
					VECTOR vKnockoffForce
					VECTOR vKnockoffForceOffset
					vKnockoffForceOffset = <<0.0,0.21,0.0>>					
					SWITCH iKnockOff_Biker_Attack_Side
						CASE KNOCKOFF_BIKER_ATTACK_SIDE_RIGHT
							IF bAreEntitiesHeadingsCompleteOpposite_KO_Biker
								vKnockoffForce = << 2.0, 0.0, -4.0 >>
							ELSE
								vKnockoffForce = << -2.0, 0.0, -4.0 >>
							ENDIF	
						BREAK
						CASE KNOCKOFF_BIKER_ATTACK_SIDE_LEFT
							IF bAreEntitiesHeadingsCompleteOpposite_KO_Biker
								vKnockoffForce = << 2.0, 0.0, -4.0 >>
							ELSE
								vKnockoffForce = << -2.0, 0.0, -4.0 >>
							ENDIF							
						BREAK						
						DEFAULT
							SCRIPT_ASSERT("RC - PAP 1 - invalid side for KO - iKnockOff_Biker_Attack_Side inside KBAS_SUCCESSFUL_ATTACK")
						BREAK
					ENDSWITCH					
					IF IS_PED_ON_ANY_BIKE(sRivalPapPed.ped)
						IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
							KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
							APPLY_FORCE_TO_ENTITY(sRivalPapPed.ped, APPLY_TYPE_IMPULSE, vKnockoffForce, vKnockoffForceOffset, 
														GET_PED_RAGDOLL_BONE_INDEX(sRivalPapPed.ped, RAGDOLL_HEAD), TRUE, TRUE, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- Rival pap knocked off bike  +++") ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- Rival pap NOT knocked off bike  CAN_KNOCK_PED_OFF_VEHICLE was FALSE +++") ENDIF #ENDIF
						ENDIF
					ENDIF
					IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)	
						IF IS_PED_ON_ANY_BIKE(sRivalPapDriverPed.ped)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapDriverPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapDriverPed.ped)																							
								APPLY_FORCE_TO_ENTITY(sRivalPapDriverPed.ped, APPLY_TYPE_IMPULSE, vKnockoffForce, vKnockoffForceOffset,
														GET_PED_RAGDOLL_BONE_INDEX(sRivalPapDriverPed.ped, RAGDOLL_HEAD), TRUE, TRUE, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- Rival paps Driver knocked off bike  +++") ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- Rival pap Driver NOT knocked off bike  CAN_KNOCK_PED_OFF_VEHICLE was FALSE +++") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT bStatTracker_HasBeverlyMissedWithSwing
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(PAP1_TAKEN_OUT_IN_ONE_SWING)
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "+++	MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK	- Rival paps knocked off bike, going to - KBAS_CLEANUP_ATTACK  +++") ENDIF #ENDIF
					eKnockOffBikerAttackState = KBAS_CLEANUP_ATTACK						
				BREAK
				CASE KBAS_CLEANUP_ATTACK					
				BREAK			
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles updating rival pap's anims after taking photos
PROC UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS()
	IF IS_PED_UNINJURED(sRivalPapPed.ped)
	// make sure idle anim is reapplyed
		IF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
			IF HAS_ENTITY_ANIM_FINISHED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
				TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS : applied idle from gesture_behind_left_pap") ENDIF #ENDIF
			ELSE
				SET_ENTITY_ANIM_SPEED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap", 1.25)
			ENDIF
		ELIF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")
			IF HAS_ENTITY_ANIM_FINISHED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")
				TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS : applied idle from gesture_behind_right_pap") ENDIF #ENDIF
			ELSE
				SET_ENTITY_ANIM_SPEED(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap", 1.25)
			ENDIF
		ELIF NOT IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle")
			TASK_PLAY_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING) // | AF_UPPERBODY)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS : applied idle as it wasn't playing!") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the rival pap joining the uber rocording and driving behaviour when the recording ends
PROC UPDATE_RIVAL_PAP_ON_BIKE()
	IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
	AND IS_PED_UNINJURED(sRivalPapDriverPed.ped)
	AND IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)	
	AND IS_PED_UNINJURED(sRivalPapPed.ped)
	AND IS_PED_SITTING_IN_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle)	
		SWITCH eRivalPapOnBikeProgress
			CASE PAP1_RIVAL_PAP_ON_BIKE_TRIGGER_JOIN_CHASE
				IF fCurrentPlaybackTime >= UBER_PLAYBACK_TIME_FOR_TRIGGER_RIVAL_PAP_RECORDING
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, 1, "Pap1RP")
						SET_PLAYBACK_SPEED(sRivalPapVehicle.vehicle, fMainPlaybackSpeed)						
						MANAGE_RIVAL_TAKING_PHOTOS(fTimeInRivalPapRecording)
						vPathNodeRequestStartPosition = << 0.0, 0.0, 0.0 >>	// reset so the new request can be setup in PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA
						eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_TRIGGER_JOIN_CHASE done") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
					SET_PLAYBACK_SPEED(sRivalPapVehicle.vehicle, fMainPlaybackSpeed)
					fTimeInRivalPapRecording = GET_TIME_POSITION_IN_RECORDING(sRivalPapVehicle.vehicle)	
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RIVAL PAP playback time = ", fTimeInRivalPapRecording) ENDIF #ENDIF
					MANAGE_RIVAL_TAKING_PHOTOS(fTimeInRivalPapRecording)
					IF fTimeInRivalPapRecording > PAP1_RIVAL_PAP_END_RECORDING_POINT	//just before end
						PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vRivalPapDriver_InitialFleeDrivePoint, vPathNodeRequestMin, vPathNodeRequestMax)
						IF PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA(vPathNodeRequestMin, vPathNodeRequestMax)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
								STOP_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
							ENDIF	
							REMOVE_VEHICLE_RECORDING(1, "pap1RP")	
							MODIFY_VEHICLE_TOP_SPEED(sRivalPapVehicle.vehicle, BIKE_TOP_SPEED_MODIFIER) //match beverly's vehicle top speed
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ON_BIKE - RIVAL PAP BIKE TOP SPEED MODIFY SET") ENDIF #ENDIF
							IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
								MODIFY_VEHICLE_TOP_SPEED(sBeverlyVehicle.vehicle, 0.0)	//RESET ped'S BIKE TOP SPEED MODIFIER
							ENDIF	
							//remove camera flash assets at this point
							REMOVE_PTFX_ASSET()
							RELEASE_NAMED_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)												
							TASK_VEHICLE_MISSION_COORS_TARGET(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, vRivalPapDriver_InitialFleeDrivePoint, MISSION_GOTO, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions, -1, -1) // B*1533294 - DF_UseStringPullingAtJunctions
							SET_PED_KEEP_TASK(sRivalPapDriverPed.ped, TRUE)	
							iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
							iTimerDelay_RivalPapGestureAnim = GET_GAME_TIMER()
							IF NOT bDoneRivalPapRemovedFromDialogue
								REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	// take rival pap out of dialogue struct so he can use ambient trigger dialogue during knock off stage
								bDoneRivalPapRemovedFromDialogue = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA - bDoneRivalPapRemovedFromDialogue = TRUE") ENDIF #ENDIF
							ENDIF							
							eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA done - recording stopped early by me") ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA - waiting on path nodes to load for early exit") ENDIF #ENDIF
						ENDIF
					// begin requesting the path nodes for the pap drive task
					ELIF (fTimeInRivalPapRecording > (PAP1_RIVAL_PAP_END_RECORDING_POINT - 6000.0))
						IF ARE_VECTORS_ALMOST_EQUAL(vPathNodeRequestStartPosition, << 0.0, 0.0, 0.0 >>)
							vPathNodeRequestStartPosition = GET_ENTITY_COORDS(sRivalPapVehicle.vehicle)
							CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - set vPathNodeRequestStartPosition for path node loading ", vPathNodeRequestStartPosition)
						ENDIF
						PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vRivalPapDriver_InitialFleeDrivePoint, vPathNodeRequestMin, vPathNodeRequestMax)
					ENDIF					
				ELSE
					IF ARE_VECTORS_ALMOST_EQUAL(vPathNodeRequestStartPosition, << 0.0, 0.0, 0.0 >>)
						vPathNodeRequestStartPosition = GET_ENTITY_COORDS(sRivalPapVehicle.vehicle)
					ENDIF
					PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vRivalPapDriver_InitialFleeDrivePoint, vPathNodeRequestMin, vPathNodeRequestMax)
					IF PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA(vPathNodeRequestMin, vPathNodeRequestMax)
						REMOVE_VEHICLE_RECORDING(1, "pap1RP")	
						MODIFY_VEHICLE_TOP_SPEED(sRivalPapVehicle.vehicle, BIKE_TOP_SPEED_MODIFIER) //match beverly's vehicle top speed
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ON_BIKE - RIVAL PAP BIKE TOP SPEED MODIFY SET") ENDIF #ENDIF
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
							MODIFY_VEHICLE_TOP_SPEED(sBeverlyVehicle.vehicle, 0.0)	//RESET ped'S BIKE TOP SPEED MODIFIER
						ENDIF	
						//remove camera flash assets at this point
						REMOVE_PTFX_ASSET()
						RELEASE_NAMED_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)												
						TASK_VEHICLE_MISSION_COORS_TARGET(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, vRivalPapDriver_InitialFleeDrivePoint, MISSION_GOTO, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions, -1, -1) // B*1533294 - DF_UseStringPullingAtJunctions
						SET_PED_KEEP_TASK(sRivalPapDriverPed.ped, TRUE)	
						iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
						iTimerDelay_RivalPapGestureAnim = GET_GAME_TIMER()
						IF NOT bDoneRivalPapRemovedFromDialogue
							REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)	// take rival pap out of dialogue struct so he can use ambient trigger dialogue during knock off stage
							bDoneRivalPapRemovedFromDialogue = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA - bDoneRivalPapRemovedFromDialogue = TRUE") ENDIF #ENDIF
						ENDIF
						eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA done - recording ended unexpectedly (probably by uber recording ending") ENDIF #ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA - waiting on path nodes to load for recording ended (robably by uber recording ending") ENDIF #ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION
				IF IsPedPerformingTask(sRivalPapDriverPed.ped, SCRIPT_TASK_VEHICLE_MISSION)
					// need to request the path nodes each frame
					//PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vRivalPapDriver_InitialFleeDrivePoint, vPathNodeRequestMin, vPathNodeRequestMax)
					//IF NOT PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA(vPathNodeRequestMin, vPathNodeRequestMax)
					//	TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions) // B*1533294 - DF_UseStringPullingAtJunctions
					//	eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR
					//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION path node not loaded so quit early") ENDIF #ENDIF
					IF IS_ENTITY_IN_RANGE_COORDS(sRivalPapDriverPed.ped, vRivalPapDriver_InitialFleeDrivePoint, 65.0)
						TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions) // B*1533294 - DF_UseStringPullingAtJunctions
						eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION done") ENDIF #ENDIF
					ENDIF
				ELSE
					TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions) // B*1533294 - DF_UseStringPullingAtJunctions
					eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_GO_TO_INITIAL_FLEE_POSITION done - TASK_VEHICLE_MISSION_COORS_TARGET task ended unexpectedly") ENDIF #ENDIF
				ENDIF
				MONITOR_RIVAL_PAP_CHASE_SPEED()	
				IF fDistance_PlayerToRivalPap < 22.0					
					IF HAS_TIME_PASSED(iTimerDelay_RivalPapGestureAnim, GET_RANDOM_INT_IN_RANGE(6000, 10000))
						IF fDistance_PlayerToRivalPap > 4.0	// don't do the gesture if the player is closing in for the knock off
							IF SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
								iTimerDelay_RivalPapGestureAnim = GET_GAME_TIMER()
								IF HAS_TIME_PASSED(iDialogueTimer_RivalPapAmbientLines, 500)
									IF PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE()
										iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF HAS_TIME_PASSED(iDialogueTimer_RivalPapAmbientLines, GET_RANDOM_INT_IN_RANGE(3000, 5000))
						IF PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE()
							iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
				UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS()
			BREAK
			CASE PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR
				IF NOT IsPedPerformingTask(sRivalPapDriverPed.ped, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
					TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, fRivalBikeSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions) // B*1533294 - DF_UseStringPullingAtJunctions
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - PAP1_RIVAL_PAP_ON_BIKE_WANDER_BEHAVIOUR reapplied - TASK_VEHICLE_DRIVE_WANDER") ENDIF #ENDIF
				ENDIF
				MONITOR_RIVAL_PAP_CHASE_SPEED()	
				IF fDistance_PlayerToRivalPap < 22.0					
					IF HAS_TIME_PASSED(iTimerDelay_RivalPapGestureAnim, GET_RANDOM_INT_IN_RANGE(6000, 10000))
						IF fDistance_PlayerToRivalPap > 4.0	// don't do the gesture if the player is closing in for the knock off
							IF SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
								iTimerDelay_RivalPapGestureAnim = GET_GAME_TIMER()
								IF HAS_TIME_PASSED(iDialogueTimer_RivalPapAmbientLines, 500)
									IF PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE()
										iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF HAS_TIME_PASSED(iDialogueTimer_RivalPapAmbientLines, GET_RANDOM_INT_IN_RANGE(3000, 5000))
						IF PLAY_RIVAL_PAP_AMBIENT_DIALOGUE_LINE()
							iDialogueTimer_RivalPapAmbientLines = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
				UPDATE_RIVAL_PAP_ANIMS_AFTER_PHOTOS()
			BREAK
		ENDSWITCH
	ELSE
		IF eRivalPapOnBikeProgress <> PAP1_RIVAL_PAP_ON_BIKE_CLEANUP				
			IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
					STOP_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
				ENDIF
				MODIFY_VEHICLE_TOP_SPEED(sRivalPapVehicle.vehicle, BIKE_TOP_SPEED_MODIFIER) //match beverly's vehicle top speed
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RIVAL PAP BIKE TOP SPEED MODIFY SET") ENDIF #ENDIF
			ENDIF
			REMOVE_VEHICLE_RECORDING(1, "pap1RP")			
			IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
				MODIFY_VEHICLE_TOP_SPEED(sBeverlyVehicle.vehicle, 0.0)	//RESET ped'S BIKE TOP SPEED MODIFIER
			ENDIF
			IF IS_PED_UNINJURED(sRivalPapPed.ped)
				IF IS_AMBIENT_SPEECH_PLAYING(sRivalPapPed.ped)
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(sRivalPapPed.ped)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_ON_BIKE - cleanup set stopped rival pap ambient speech") ENDIF #ENDIF
				ENDIF
			ENDIF
			//remove camera flash assets at this point
			REMOVE_PTFX_ASSET()
			RELEASE_NAMED_SCRIPT_AUDIO_BANK(sPap1_AudioBank_CameraFlashSoundFX)
			IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
			AND IS_VEHICLE_OK(sRivalPapVehicle.vehicle)			
			AND IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)									
				TASK_VEHICLE_DRIVE_WANDER(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle, 55.0, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_UseStringPullingAtJunctions) // B*1533294 - DF_UseStringPullingAtJunctions
				SET_PED_KEEP_TASK(sRivalPapDriverPed.ped, TRUE)							
			ENDIF	
			eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_CLEANUP	
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "eRivalPapOnBikeProgress - set PAP1_RIVAL_PAP_ON_BIKE_CLEANUP") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Make the rival pap's driver behaviour when he's been knocked off the bike
PROC SET_RIVAL_PAP_DRIVER_INJURED_WHEN_OFF_THE_BIKE()
	IF NOT bDoneSetupRivalPapDriverToActInjured
		IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapDriverPed.ped, FALSE)
				IF (GET_ENTITY_HEALTH(sRivalPapDriverPed.ped) > 50)
					SET_ENTITY_HEALTH(sRivalPapDriverPed.ped, 50)	
				ENDIF
				SET_PED_INJURED_ON_GROUND_BEHAVIOUR(sRivalPapDriverPed.ped, 30000)	
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "** Rival pap driver set to injured behaviour on the ground **") ENDIF #ENDIF
				bDoneSetupRivalPapDriverToActInjured = TRUE
			ENDIF
		ELSE
			bDoneSetupRivalPapDriverToActInjured = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: 
///    Handle the rival pap's behaviour when he's been knocked off the bike
PROC UPDATE_RIVAL_PAP_BEHAVIOUR_AFTER_CRASH()
	
	IF IS_PED_UNINJURED(sRivalPapPed.ped)
		IF NOT bDoneRivalPapSurrendered			
			//Setup rival pap's flee behaviour when he falls off the bike
			IF NOT bDoneRivalPapFlee
				IF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
					STOP_ANIM_TASK(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_left_pap")
				ELIF IS_ENTITY_PLAYING_ANIM(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")
					STOP_ANIM_TASK(sRivalPapPed.ped, sAnimDictName_Pap1Specific, "gesture_behind_right_pap")
				ENDIF
				IF NOT IS_PED_RAGDOLL(sRivalPapPed.ped)	//added to stop is ped touching ped firing straight away when the rival pap is off his bike
					CLEAR_PED_TASKS(sRivalPapPed.ped)
					
					SET_PED_COMBAT_MOVEMENT(sRivalPapPed.ped, CM_WILLRETREAT)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapPed.ped, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapPed.ped, FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapPed.ped, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sRivalPapPed.ped, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(sRivalPapPed.ped, CA_ALWAYS_FLEE, TRUE)
					SET_PED_CAN_BE_TARGETTED(sRivalPapPed.ped, TRUE)
					SET_PED_AS_ENEMY(sRivalPapPed.ped, TRUE)		// B*1535622 - makes sure HUD elements are set for enemy ped
					
					IF (GET_ENTITY_HEALTH(sRivalPapPed.ped) >= 137)	// fatigued threshold (now 137.5) set lower to get different flee anims
						SET_ENTITY_HEALTH(sRivalPapPed.ped, 136)
					ENDIF
					
					// health lowering doesn't work on it's own - having to use clipset
					REQUEST_ANIM_DICT(tlRivalPapInjuredAnims)
					IF HAS_ANIM_DICT_LOADED(tlRivalPapInjuredAnims)						
						SET_PED_ALTERNATE_MOVEMENT_ANIM(sRivalPapPed.ped, AAT_IDLE, tlRivalPapInjuredAnims, "idle")
						SET_PED_ALTERNATE_MOVEMENT_ANIM(sRivalPapPed.ped, AAT_WALK, tlRivalPapInjuredAnims, "walk")
						SET_PED_ALTERNATE_MOVEMENT_ANIM(sRivalPapPed.ped, AAT_RUN, tlRivalPapInjuredAnims, "run")
						CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_BEHAVIOUR_AFTER_CRASH - set rival pap alt move anims for injured : FC = ", GET_FRAME_COUNT())
					ENDIF
					
					SET_PED_MAX_MOVE_BLEND_RATIO(sRivalPapPed.ped, PEDMOVEBLENDRATIO_RUN)
					//TASK_SMART_FLEE_PED(sRivalPapPed.ped, PLAYER_PED_ID(), 75.0, -1, TRUE)
					TASK_REACT_AND_FLEE_PED(sRivalPapPed.ped, PLAYER_PED_ID())	// B*1508246 - having to set this because TASK_SMART_FLEE_PED can't calculate his route quickly unless really low, but once he reaches the dist he stops which looks crap
					SET_PED_KEEP_TASK(sRivalPapPed.ped, TRUE)

					iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()	// set for use with random lines "PAP1_RIVLINE
					bDoneRivalPapFlee = TRUE
				ENDIF
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(sRivalPapPed.ped, PEDMOVEBLENDRATIO_RUN)	// see B*1071719 - need to be called each frame otherwise it resets.
				
				//Extra dialogue if the player is still around as rival pap flees
				IF iDialogueLines_RivalPapCameraWrecked < 6
					IF IS_ENTITY_IN_RANGE_COORDS(sRivalPapPed.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 25.0)
						IF NOT IS_AMBIENT_SPEECH_PLAYING(sRivalPapPed.ped) 
							IF NOT IS_PED_RAGDOLL(sRivalPapPed.ped)
								INT iTimeDelay = 750
								IF iDialogueLines_RivalPapCameraWrecked > 0
									GET_RANDOM_INT_IN_RANGE(2500, 4000)
								ENDIF
								IF HAS_TIME_PASSED(iDialogueTimer_ChasingRivalPaparazzi, iTimeDelay)
									//make sure removed for dialogue struct for ambient dialogue
									IF NOT bDoneRivalPapRemovedFromDialogue
										REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "RIVALPAP"
										bDoneRivalPapRemovedFromDialogue = TRUE
									ENDIF
									//code generate the variation when using PLAY_PED_AMBIENT_SPEECH_WITH_VOICE so just pass in the context - "PAP1_RIVLINE"
									PAP1_PLAY_TRIGGER_AMBIENT_SPEECH(sRivalPapPed.ped, "PAP1_DQAA", "Paparazzo1RivalPap", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)	// B*1380212 - request to switch speech param from SPEECH_PARAMS_SHOUTED
									// You're a disgrace to the industry!  
									// I'll make sure you never work in this town again!
									// Sabotage, that's all you've got! You're pathetic!
									// Everyone in Vinewood knows you're a joke!
									// Get the fuck away from me, I'm done with you.
									// You're a failure.  I made six figures last year! 
									CPRINTLN(DEBUG_MISSION, "UPDATE_RIVAL_PAP_BEHAVIOUR_AFTER_CRASH - triggered rival pap ambient flee speech :  PAP1_RIVLINE aka PAP1_DQAA : FC = ", GET_FRAME_COUNT())
									IF NOT IS_PED_HEADTRACKING_PED(sRivalPapPed.ped, PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(sRivalPapPed.ped, PLAYER_PED_ID(), 3000)
									ENDIF
									iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()	// set for use with random lines "PAP1_RIVLINE
									iDialogueLines_RivalPapCameraWrecked++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Handles Beverly dialogue during rival pap chase player, setup in D* as random dialogue
PROC MANAGE_ADDITIONAL_DIALOGUE_FOR_CHASING_RIVAL_PAP()
	// Beverly throws a punch
	IF NOT bDoneDialogue_BeverlyThrowsPunch		
		// extra check to make sure we don't cancel the dialogue for this if it's already on going 
		IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PUNCH")
		OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_RPAP05")	// cancel this if the missed attempt dialogue has already kicked in
			bDoneDialogue_BeverlyThrowsPunch = TRUE
		ELSE
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PUNCH", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
				// Hack! Hack! Hack! You hack!
				// You filthy panty sniffer!
				// Die you sick bastard!
				// You're scum! Scum!
				bDoneDialogue_BeverlyThrowsPunch = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT bDoneDialogue_BeverlyMissedKnockOffAttempt	// bool set in  
		IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PUNCH")	// wait for the punch dialogue to playout first
			// extra check to make sure we don't cancel the dialogue for this if it's already on going 
			IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_RPAP05")
				bDoneDialogue_BeverlyMissedKnockOffAttempt = TRUE
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				IF NOT bDoneDialogue_BeverlyMissedKnockOffAttempt
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RPAP05", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
						// Damn, missed him.  
						// Nearly!  
						// Next time, Madison, I swear!
						// So close!
						// Slippery son of a bitch.  
						// Come on!  
						// Are you kidding me?
						// You backstabbing shit!  
						bDoneDialogue_BeverlyMissedKnockOffAttempt = TRUE
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	IF NOT bDoneDialogue_PlayerPullsWheelie
		// currently the only thing remotely close to a is_bike_pulling_wheelie check, potentially use with IS_ENTITY_IN_AIR()
		// but in this instance i think the dialogue will work with airbourne as well
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
			IF HAS_TIME_PASSED(iTimer_BeverlyBikeNotOnAllWheelsTimer, 500) //make sure player has been pulling wheelie for a decent chunk of time 
				// random dialogue, so only one line will be selected
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PPW1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
					// Christ, you're gonna kill us.  
					// You're a fucking lunatic.  
					// Are you trying to give me a heart attack?  
					// Is that supposed to impress me? 
					bDoneDialogue_PlayerPullsWheelie = TRUE
					bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this so Franklin can't respond to this one
					iDialogueTimer_TakingLongTimeToReachDropOffPoint = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ELSE
			iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		// reset the dialogue bool once the current conversation has finished
		IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PPW1")
		AND IS_VEHICLE_ON_ALL_WHEELS(sBeverlyVehicle.vehicle)
			bDoneDialogue_PlayerPullsWheelie = FALSE
			iTimer_BeverlyBikeNotOnAllWheelsTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
	// Franklin crashes the bike		
	IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CRASH")
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sBeverlyVehicle.vehicle)
			IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
			AND NOT IS_ENTITY_TOUCHING_ENTITY(sRivalPapVehicle.vehicle, sBeverlyVehicle.vehicle)
				//IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//ENDIF
				// random selected lines
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CRASH", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
					// You idiot! 
					// For Christ's sake!
					// Are you blind?    
					// Can't you drive!?
					// Is it that hard? 
					// No! You moron!  
					// You have gotta be kidding me!
					bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// reset this so Franklin can respond
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING RIVAL PAP - crash biek dialogue triggered - PAP1_CRASH") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()			
	ELSE
		IF NOT bDoneDialogue_FranklinRespondToBeverlyBerating	// reset this to ensure Franklin doesn't say something straight away
			IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RESP", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())								
					// Yeah, yeah, I got it. 
					// Maybe if you shut up for a second.  
					// What do you think I'm doin'?
					// Fuck, I know, I'm tryin'!
					// Are you always this annoying?  
					// I get the idea, homie.  
					// You really ain't helpin'.
					// We might do better if you shut the fuck up.
					// I heard you the first time. 
					iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()
					bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LCHASING RIVAL PAP - Franklin Response dialogue triggered - PAP1_RESP") ENDIF #ENDIF
				ENDIF
			ELSE
				bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// bypass response if we are out range of each other
			ENDIF
		ELSE
			// Franklin drives into a ped
			IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
			AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_HITPED", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
						// It's okay, they're fine.  
						// That's it, no prisoners!  
						// Collateral damage, don't worry about it.    
						// Good, if they get in the way, take them down!
						// Move next time!
						iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING RIVAL PAP - drove into ped dialogue - PAP1_HITPED") ENDIF #ENDIF
					ENDIF
				ENDIF
			ELSE			
				IF (GET_GAME_TIMER() - iDialogueTimer_ChasingRivalPaparazzi > (TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE + GET_RANDOM_INT_IN_RANGE(1, 2500)))			
					//different dialogue depending on how far away from the rival pap we are				
					IF (fDistance_PlayerToRivalPap < DIST_LIMIT_PLAYER_CLOSE_DIST_TO_RIVAL_PAP_IN_CHASE)
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RIVAL", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING RIVAL PAP CLOSE DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
							iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()
							// I saw her first!
							// This crusty old boot is mine!
							// Have some professional courtesy!
							// Vultures! All of you! She's an angel!
							// I'll slit your throat, Madison!
							// Have some self-respect!
							// How do you sleep at night!?
						ENDIF
					ELIF (fDistance_PlayerToRivalPap < DIST_LIMIT_PLAYER_MEDIUM_DIST_TO_RIVAL_PAP_IN_CHASE)
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RPAP08", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING RIVAL PAP MEDIUM DIST DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
							iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()
							// Get me in close and I'll drag him off!
							// Get alongside them!
							// Go in closer so I can reach across.
							// Let's get right up in his face!  
							// How can I bitchslap him from here?  
							// Come on, I wanna look that bastard in the eye!
							IF (GET_RANDOM_INT_IN_RANGE(0,2) = 0)
								bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// make Franklin respond every now and then
							ENDIF
						ENDIF
					ELSE
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RPAP07", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CHASING RIVAL PAP FAR DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
							iDialogueTimer_ChasingRivalPaparazzi = GET_GAME_TIMER()
							// He's not taking my glory, not again!  
							// He's getting away, come on!
							// Come on, can't you go any faster!?
							// Whatever you do, don't lose them.
							// Crank the gas, come on!  
							IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
								IF (GET_RANDOM_INT_IN_RANGE(0, 3) < 2)	// 66% chance of playing the anim
									SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(sRivalPapPed.ped)
								ENDIF
							ENDIF
							IF (GET_RANDOM_INT_IN_RANGE(0,2) = 0)
								bDoneDialogue_FranklinRespondToBeverlyBerating = FALSE	// make Franklin respond every now and then
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF									
ENDPROC

/// PURPOSE:
///    handles triggering the relevant convo for rival pap being knocked off his bike
/// RETURNS:
///    TRUE if convo was triggered 
FUNC BOOL TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION()

	// retrigger from specific line if it was paused
	IF bPausedDialogue_DropOffBeverly
		IF PAP1_CREATE_CONVERSATION_FROM_SPECIFIC_LINE_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_CameraDestroyed, tlPausedDialogue_DropOffBeverly, CONV_PRIORITY_MEDIUM)
			bPausedDialogue_DropOffBeverly = FALSE // reset flag
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : restarted convo from specific line : ", tlPausedDialogue_DropOffBeverly) ENDIF #ENDIF
			RETURN TRUE
		ENDIF												
	ELSE
		IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PUNCH")	// wait for the punch dialogue to playout first
		
			//ALTERNATIVE CONVO  - if Beverly failed to take any photos, or if he Bev didn't knock him off, or if the player shoot him off
			// Bev didn't perform the attack which knocked him off
			IF eKnockOffBikerAttackState <> KBAS_CLEANUP_ATTACK			
				IF IS_PED_UNINJURED(sRivalPapPed.ped)	// B* 1349592 - ensure pap is dead since new dialogue references him as killed
					bHasPlayerShotRivalPapOffBike = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : bHasPlayerShotRivalPapOffBike = FALSE rival pap still alive") ENDIF #ENDIF
				ENDIF
				// player killed him / shot him off
				IF bHasPlayerShotRivalPapOffBike
					tlDialogueRoot_CameraDestroyed = "PAP1_URP03"
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_CameraDestroyed, CONV_PRIORITY_MEDIUM)
						// Jesus, you didn't have to kill the guy!
						// This is from the fool tryin' to punch him off a moving motorbike. 
						// Well, it's done now. A terrible accident, but life must go on. 
						// And I'm already late for my next stake-out. Let's get the hell out of here.
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : return TRUE player kill convo : ", tlDialogueRoot_CameraDestroyed) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					// he fell off due to crash
					tlDialogueRoot_CameraDestroyed = "PAP1_URP02"
					IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_CameraDestroyed, CONV_PRIORITY_MEDIUM)
						// He fell off! What an amateur.
						// Uh-oh! Looks like you wrecked your camera, Madison! 
						// Okay, buddy, let's get out of here. I'm already late for another stakeout.
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : return TRUE fell off in crash convo : ", tlDialogueRoot_CameraDestroyed) ENDIF #ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ELIF iGoodPhotosTakenCounter = -1
				//failed to take any photos dialogue
				tlDialogueRoot_CameraDestroyed = "PAP1_URP01"
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_CameraDestroyed, CONV_PRIORITY_MEDIUM)
					// Yeah, I downloaded your photos to the fucking road, Madison! Now grow up and get a boy's name!  
					// This way neither of us got any photos. It's only fair.  
					// Okay, buddy, let's get out of here. I'm already late for another stake-out.  
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : return TRUE no photos taken convo : ", tlDialogueRoot_CameraDestroyed) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				//successful took some photos dialogue
				tlDialogueRoot_CameraDestroyed = "PAP1_RPAP04"
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_CameraDestroyed, CONV_PRIORITY_MEDIUM)
					// Yeah, I downloaded your photos to the fucking road, Madison! Now grow up and get a boy's name!  
					// Okay, buddy, let's get out of here. I'm already late for another stake-out.  
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, " TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION : return TRUE successful photos taken convo : ", tlDialogueRoot_CameraDestroyed) ENDIF #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles cleanup of Beverly's animations whilst on foot
PROC MANAGE_CLEANUP_OF_BEVERLY_ON_FOOT_ANIMATIONS()
	IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
	OR IS_PED_RAGDOLL(sBeverlyPed.ped)
	OR IS_ENTITY_IN_WATER(sBeverlyPed.ped)
	OR IS_PED_GETTING_UP(sBeverlyPed.ped)
		IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
			STOP_ANIM_TASK(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLEANUP_OF_BEVERLY_ON_FOOT_ANIMATIONS  - stopped anim due to ragdoll/water/ get in vehicle") ENDIF #ENDIF
		ENDIF
		IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	//default on bike anim
			STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_CLEANUP_OF_BEVERLY_ON_FOOT_ANIMATIONS - stopped idle anim due to ragdoll/water/ get in vehicle") ENDIF #ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    cleanup beverly on bike anims
///    NOTE: doesn't check Beverly is off the bike or that the entities are alive
PROC CLEANUP_BEVERLY_ON_BIKE_ANIMS(FLOAT fBlendOutTime = FAST_BLEND_OUT)	
	IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	
		STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", fBlendOutTime)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_BEVERLY_ON_BIKE_ANIMS : anim stopped BEVERLY idle ", fBlendOutTime, " not sat on bike-  FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//remove chat on bike anim when convo is finished
		STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", fBlendOutTime)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_BEVERLY_ON_BIKE_ANIMS : anim stopped BEVERLY dialog_pap ", fBlendOutTime, " - not sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")	// don't clear the catch up anims
		STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap", fBlendOutTime)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_BEVERLY_ON_BIKE_ANIMS : anim stopped BEVERLY hurry_up_left_pap ", fBlendOutTime, " - not sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
		STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap", fBlendOutTime)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_BEVERLY_ON_BIKE_ANIMS : anim stopped BEVERLY hurry_up_right_pap ", fBlendOutTime, " - not sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)	
		STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint, fBlendOutTime)	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_BEVERLY_ON_BIKE_ANIMS : anim stopped BEVERLY tlAnimName_BeverlyPoint", fBlendOutTime, " not sat on bike-  FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    set Beverly to be part of player's group
///    NOTE: doesn't check Bev exists or is uninjured
PROC SET_BEVERLY_FOLLOW_PLAYER()
	IF NOT IS_PED_GROUP_MEMBER(sBeverlyPed.ped, GET_PLAYER_GROUP(PLAYER_ID()))
		SET_PED_AS_GROUP_MEMBER(sBeverlyPed.ped, GET_PLAYER_GROUP(PLAYER_ID()))
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_FOLLOW_BEVERLY : set as group memeber") ENDIF #ENDIF
	ENDIF
	IF IS_PED_IN_GROUP(sBeverlyPed.ped)
		SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_DontEnterVehiclesInPlayersGroup, TRUE)	//used to control which vehicles the ped will get into
		SET_GROUP_FORMATION(GET_PLAYER_GROUP(PLAYER_ID()), FORMATION_LOOSE)
		SET_GROUP_FORMATION_SPACING(GET_PLAYER_GROUP(PLAYER_ID()), 1.5)
		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(sBeverlyPed.ped, VS_FRONT_RIGHT)	
		CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_FOLLOW_BEVERLY : done : formation setting 1.5 *")
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET_BEVERLY_FOLLOW_BEVERLY : failed Bev not in a group!") ENDIF #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles Beverly being tasked to get on his bike
///    uses specific anim overrides
PROC TASK_BEVERLY_TO_GET_ON_HIS_BIKE()
	
	BOOL bUseLeftEntry = TRUE
	ENTER_EXIT_VEHICLE_FLAGS iEnterFlags = ECF_USE_LEFT_ENTRY
	
	IF (GET_SIDE_COORDS_IS_TO_ENTITY(sBeverlyVehicle.vehicle, GET_ENTITY_COORDS(sBeverlyVehicle.vehicle, FALSE), GET_ENTITY_COORDS(sBeverlyPed.ped, FALSE)) = 1)
		bUseLeftEntry = FALSE
		iEnterFlags = ECF_USE_RIGHT_ENTRY
	ENDIF

	// attempt to enter the prefered side	
	IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT, TRUE, bUseLeftEntry)
	
		REQUEST_CLIP_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
		IF HAS_ANIM_SET_LOADED(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
		
			TASK_ENTER_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, iEnterFlags, GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TASK_BEVERLY_TO_GET_ON_HIS_BIKE set 1st choice bUseLeftEntry = ", bUseLeftEntry, "  : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ELSE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TASK_BEVERLY_TO_GET_ON_HIS_BIKE failed clipset not loaded 1st choice bUseLeftEntry = ", bUseLeftEntry, "  : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ELSE	
		// back up attempt to enter from opposite side	
		IF bUseLeftEntry
			bUseLeftEntry = FALSE
			iEnterFlags = ECF_USE_RIGHT_ENTRY
		ELSE
			bUseLeftEntry = TRUE
			iEnterFlags = ECF_USE_LEFT_ENTRY
		ENDIF
		
		IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT, TRUE, bUseLeftEntry)
			
			REQUEST_CLIP_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
			IF HAS_ANIM_SET_LOADED(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
		
				TASK_ENTER_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, iEnterFlags, GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(bUseLeftEntry))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TASK_BEVERLY_TO_GET_ON_HIS_BIKE set 2nd choice bUseLeftEntry = ", bUseLeftEntry, "  : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "TASK_BEVERLY_TO_GET_ON_HIS_BIKE failed clipset not loaded 2nd choice bUseLeftEntry = ", bUseLeftEntry, "  : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF
	ENDIF					
ENDPROC

/// PURPOSE:
///    handles Bevrly turning to face the player and playing a frustrated anim
///    NOTE: doesn't check Bev exists or is uninjured
PROC UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM(BOOL bForceThisFrame = FALSE)
	IF NOT IS_ENTITY_IN_WATER(sBeverlyPed.ped)
	AND NOT IS_PED_RAGDOLL(sBeverlyPed.ped)
	AND NOT IS_PED_GETTING_UP(sBeverlyPed.ped)
	AND IS_PED_ON_FOOT(sBeverlyPed.ped)
		//play a frustrated anim that the player has left him
		IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
			IF NOT IS_PED_FACING_PED(sBeverlyPed.ped, PLAYER_PED_ID(), 45.0)
				IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					TASK_TURN_PED_TO_FACE_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
				ENDIF
			ELSE
				IF HAS_TIME_PASSED(iTimerTillRepeatBevFrustratedAnim, TIME_DELAY_FOR_REPEATED_BEVERLY_FRUSTRATED_ANIM)
				OR bForceThisFrame
					IF SET_BEVERLY_PLAY_COME_ON_ANIM()
						iTimerTillRepeatBevFrustratedAnim = GET_GAME_TIMER()
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM - anim applied") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iTimerTillRepeatBevFrustratedAnim = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    checks to see if Bev should be removed from the player's group
///    NOTE: doesn't check entities exist or are alive
/// RETURNS:
///    TRUE if reason to remove Bev from player group is detected
FUNC BOOL SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP()

	// player got in the wrong vehicle
	IF (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle))
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP : return TRUE player in wrong vehicle") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	// player left Beverly behind 
	IF (fDistance_PlayerToBeverly >= BEVERLY_LEAVE_PLAYER_GROUP_DISTANCE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP : return TRUE player left Beverly behind ") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	// player in water
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP : return TRUE player in water ") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    checks to see if Bev should join the player's group
///    NOTE: doesn't check entities exist or are alive
/// RETURNS:
///    TRUE if reasons to join player group are fulfilled
FUNC BOOL SHOULD_BEVERLY_JOIN_PLAYER_GROUP()

	// player got in the wrong vehicle
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_JOIN_PLAYER_GROUP : return FALSE player not sitting on Bev bike ") ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	// player in water
	IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_JOIN_PLAYER_GROUP : return FALSE player in water ") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	// player out of rejoin rangge 
	IF (fDistance_PlayerToBeverly > BEVERLY_REJOIN_PLAYER_GROUP_DISTANCE)
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SHOULD_BEVERLY_JOIN_PLAYER_GROUP : return FALSE player not in range to rejoin") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    perform cleanup needed for leaving the specified player transport state
/// PARAMS:
///    eStateToCleanup - the state to cleanup
PROC CLEANUP_PLAYER_TRANSPORT_STATE(PAP1_PLAYER_TRANSPORT_STATE_ENUM eStateToCleanup)
	SWITCH eStateToCleanup		
		CASE PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
			SWITCH eMissionStage
				CASE MISSION_STAGE_DROP_BEVERLY_OFF
					CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_05")	// Take Beverly to his next ~y~stakeout.~s~	
					SAFE_REMOVE_BLIP(blipIndexDropOffPoint)
					//used to restart the convo, once the player gets back on the bike and Beverly is also on the bike.
					IF bDoneDialogue_DropOffBeverlyAdditionalChat
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_DOFF02")
							bPausedDialogue_DropOffBeverly = TRUE
							bDoneDialogue_DropOffBeverlyAdditionalChat = FALSE
							tlPausedDialogue_DropOffBeverly = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						ENDIF
					ELIF bDoneDialogue_DropOffBeverly			
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_DOFF05")
						OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_DOFF04")
							bPausedDialogue_DropOffBeverly = TRUE
							bDoneDialogue_DropOffBeverly = FALSE
							tlPausedDialogue_DropOffBeverly = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						ENDIF
					ELIF bDoneDialogue_BeverlySayKeepBike
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_DROP")
							bPausedDialogue_DropOffBeverly = TRUE
							bDoneDialogue_BeverlySayKeepBike = FALSE
							tlPausedDialogue_DropOffBeverly = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						ENDIF
					ELIF bDoneDialogue_RivalPapCameraDestroyed
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING(tlDialogueRoot_CameraDestroyed)	// don't kill Beverly camera destroyed dialogue
							bPausedDialogue_DropOffBeverly = TRUE
							bDoneDialogue_RivalPapCameraDestroyed = FALSE
							tlPausedDialogue_DropOffBeverly = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - MISSION_STAGE_DROP_BEVERLY_OFF stored ongoing camera destroyed convo FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
					IF IS_PED_RAGDOLL(PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(sBeverlyPed.ped)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	// B*1521607 - stop instantly if crashing the bike
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF
				BREAK
				CASE MISSION_STAGE_LOSE_THE_COPS
					CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_09")	// Lose the cops.
					IF IS_PED_RAGDOLL(PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(sBeverlyPed.ped)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	// B*1521607 - stop instantly if crashing the bike
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF
				BREAK
				DEFAULT				
					eBeverlyTakePhotoState = PAP1_BTPS_SETUP //reset Beverly taking photos state
					IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
						CLEAR_PED_TASKS(sBeverlyPed.ped)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - cleared BEVERLY task SCRIPT_TASK_GENERAL_SWEEP FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_01")	// Drive alongside Miranda's ~b~car.~s~
					SAFE_REMOVE_BLIP(sCelebPed.blip)
					IF IS_PED_RAGDOLL(PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(sBeverlyPed.ped)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	// B*1521607 - stop instantly if crashing the bike
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF	
					bDoneDialogue_BeverlyMissedKnockOffAttempt = TRUE	//reset this dialogue to done so it doesn't try to trigger as soon as we get back on the bike
					bDoneDialogue_BeverlyThrowsPunch = TRUE
					//Extra stuff to cleanup if its in the destroy rival pap cam stage
					IF eMissionStage = MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
					OR eMissionStage = MISSION_STAGE_STOP_RIVAL_PAP						
						SAFE_REMOVE_BLIP(sCelebPed.blip)		
						SAFE_REMOVE_BLIP(sRivalPapPed.blip)
						CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_03")	//Chase after ~r~Tobias.~w~
						CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("PAP1_H1")
					ENDIF
				BREAK					
			ENDSWITCH
			IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	//setup the idle on bike pose
				
					IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//remove chat on bike anim when convo is finished
						STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", REALLY_SLOW_BLEND_OUT)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - anim stopped BEVERLY dialog_pap REALLY_SLOW_BLEND_OUT- still sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap")	// don't clear the catch up anims
						STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_left_pap", REALLY_SLOW_BLEND_OUT)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - anim stopped BEVERLY hurry_up_left_pap REALLY_SLOW_BLEND_OUT- still sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap")
						STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "hurry_up_right_pap", REALLY_SLOW_BLEND_OUT)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV -  anim stopped BEVERLY hurry_up_right_pap REALLY_SLOW_BLEND_OUT- still sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)	
						STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint, REALLY_SLOW_BLEND_OUT)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV -  anim stopped BEVERLY tlAnimName_BeverlyPoint REALLY_SLOW_BLEND_OUT- still sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
						RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
						STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - anim stopped BEVERLY cam_pos_hand_override INSTANT_BLEND_OUT - still sat on bike with WEAPON CLIPSET RESET FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					/* ^^^ bev alt anim during Franklin helmet anim change 
					ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")
						STOP_ANIM_TASK(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still", INSTANT_BLEND_OUT)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - anim stopped BEVERLY veh@bike@sport@rear@base still INSTANT_BLEND_OUT - still sat on bike with WEAPON CLIPSET RESET FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF */
					ENDIF
					
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV -  anim set BEVERLY idle SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT- still sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_DisableInVehicleActions, TRUE)	// B*1480880 - stop bev clipping during reverse anims
			ELSE
				CLEANUP_BEVERLY_ON_BIKE_ANIMS()
				IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped,sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
						RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
						//TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV - anim NOT set BEVERLY cam_pos_hand_override  bev GETTING ON BIKE  - cam_pos_hand_override INSTANT_BLEND_IN getting on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ELSE
					SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
				ENDIF
				/* fail attempt to use the cam override anim on Bev prior to him starting the custom get_in anims
					but this failed on anim playing check maybe because they are from a clipset?
				IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
						AND NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
							RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - bev GETTING ON BIKE  - RESET_PED_MOVEMENT_CLIPSET & applied cam_pos_hand_override FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
						OR IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
							RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
							STOP_ENTITY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - bev GETTING ON BIKE  - stopped cam_pos_hand_override anim for get_in anims FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ELSE
							CPRINTLN(DEBUG_MISSION, "here")	// ok so this isn't working because checking if Bev is playing one of the get_in anims isn't returning when it should do.. maybe because its actually a clipset?
						ENDIF
					ENDIF
				ELSE
					SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
				ENDIF*/
				TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
			ENDIF
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
		BREAK
		CASE PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE	
			TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_06")	// Get back on Beverly's ~b~motorbike.~s~
			CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_10")	// Get on Beverly's ~b~motorbike.~s~
			SAFE_REMOVE_BLIP(sBeverlyVehicle.blip)
			IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_IG5")	// stop get back on the bike dialogue instantly
			OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_FALL")
			OR IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_CRASH")			
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			KILL_ANY_CONVERSATION()
			IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")							
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE -  anim set BEVERLY idle SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT- sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY
			TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			SAFE_REMOVE_BLIP(sBeverlyPed.blip)
			CLEAR_SPECIFIC_PRINT_FROM_DISPLAYING("PAP1_08")	//Return to ~b~Beverly.~s~
			KILL_ANY_CONVERSATION()
			IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")							
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY -  anim set BEVERLY idle SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT- sat on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CLEANUP_PLAYER_TRANSPORT_STATE - done for eStateToCleanup : ", eStateToCleanup) ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Get updated the players Transport state
/// RETURNS:
///    returns the current player transport state enum
FUNC PAP1_PLAYER_TRANSPORT_STATE_ENUM GET_UPDATED_PLAYER_TRANSPORT_STATE()
	//IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
	IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)	// changed to use this to fix bug 938762 - Strange animation when getting off the bike.
		RETURN PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_TRANSPORT_STATE - ePlayerTransportState = PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE") ENDIF #ENDIF
	//ELIF NOT IS_PED_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
	ELIF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
		RETURN PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_TRANSPORT_STATE - ePlayerTransportState = PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY") ENDIF #ENDIF
	ELSE
		RETURN PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
		//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UPDATE_PLAYER_TRANSPORT_STATE - ePlayerTransportState = PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV") ENDIF #ENDIF
	ENDIF
ENDFUNC

/// PURPOSE:
///    Handle the mission flow for when the player and Beverly are on the bike.
///    moved this code here to improve readability as MANAGE_PLAYER_AND_BEVERLY() had became pretty big
PROC MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE()	

	SWITCH eMissionStage
	
		CASE MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
		
			IF NOT DOES_BLIP_EXIST(sCelebPed.blip)
				sCelebPed.blip = CREATE_PED_BLIP(sCelebPed.ped, TRUE, TRUE)
				SET_BLIP_SCALE(sCelebPed.blip, 1.0)	//TEMP: cars need bigger blips but the new rc_helpers_function - CREATE_PED_BLIP doesn't handle it.
			ELSE
				IF IS_PED_SITTING_IN_ANY_VEHICLE(sCelebPed.ped)
					SET_BLIP_SCALE(sCelebPed.blip, 1.0)	
				ENDIF
				UPDATE_CHASE_BLIP(sCelebPed.blip, sCelebPed.ped, CELEB_ESCAPED_DIST, PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE)
			ENDIF
			IF NOT bDoneDialogue_InitialChaseDialogue
				/* ^^^ bev alt anim during Franklin helmet anim change - original to swap out */
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")		
					iDelayInitialChaseComment = GET_GAME_TIMER()	// allow this anim to kick in before the point anim
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose - whilst waiting for initial chase dialogue to trigger") ENDIF #ENDIF
				ELSE
					IF HAS_TIME_PASSED(iDelayInitialChaseComment, 500)
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE01", CONV_PRIORITY_MEDIUM)
							// Go, go! Get us right up alongside that limo.
							bBeverly_InitialPointAtCeleb_Anim = SET_BEVERLY_PLAY_POINT_ANIM(sCelebPed.ped)
							bDoneDialogue_InitialChaseDialogue = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE - bDoneDialogue_InitialChaseDialogue done") ENDIF #ENDIF
						ENDIF		
					ENDIF
				ENDIF
				/* ^^^ swap out end */
				/* ^^^ bev alt anim during Franklin helmet anim change 
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	
				AND NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")	
					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@sport@front@base", "put_on_helmet")
						TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLEset Bev to idle pose - whilst waiting for initial chase dialogue to trigger") ENDIF #ENDIF
					ELSE
						REQUEST_ANIM_DICT("veh@bike@sport@rear@base")
						IF HAS_ANIM_DICT_LOADED("veh@bike@sport@rear@base")
							TASK_PLAY_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE - applied still on bike anim whilst player puts on helmet") ENDIF #ENDIF
						ELSE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE - requestin anim dict for still idle -  veh@bike@sport@rear@base for player puts on helmet") ENDIF #ENDIF
						ENDIF
					ENDIF
				ELSE
					// delay for player putting on helmet
					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@sport@front@base", "put_on_helmet")
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CHASE01", CONV_PRIORITY_MEDIUM)
							// Go, go! Get us right up alongside that limo.
							bBeverly_InitialPointAtCeleb_Anim = SET_BEVERLY_PLAY_POINT_ANIM(sCelebPed.ped)
							bDoneDialogue_InitialChaseDialogue = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE - bDoneDialogue_InitialChaseDialogue done") ENDIF #ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE - bDoneDialogue_InitialChaseDialogue delayed for player putting on helmet") ENDIF #ENDIF
					ENDIF
				ENDIF */
			ELIF NOT bDoneObjective_GetCloseToCeleb
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
					IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sCelebPed.ped)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sCelebPed.ped, -1, SLF_FAST_TURN_RATE)
					ENDIF
					IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, sCelebPed.ped)
						TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, sCelebPed.ped, -1, SLF_FAST_TURN_RATE)	
					ENDIF
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")			
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)	// wait for point anim to finish
							// make sure the anim has completed (has the hold last frame flag
							IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose - whilst setting initial objective") ENDIF #ENDIF
							ENDIF
						ELSE
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose - whilst setting initial objective") ENDIF #ENDIF
						ENDIF
					ENDIF
					PRINT_NOW("PAP1_01", DEFAULT_GOD_TEXT_TIME, 1)	//Drive close to ~r~Miranda~w~ so Beverly can get take some photos.	
					iDialogueTimer_ChasingCeleb = GET_GAME_TIMER()	// delay first comment from Beverly
					bDoneObjective_GetCloseToCeleb = TRUE
				ELSE
					// make Bev point at the celeb in the limo
					IF NOT bBeverly_InitialPointAtCeleb_Anim
						IF SET_BEVERLY_PLAY_POINT_ANIM(sCelebPed.ped)
							bBeverly_InitialPointAtCeleb_Anim = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bBeverly_InitialPointAtCeleb_Anim done") ENDIF #ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")			
							IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)	// wait for point anim to finish
								// make sure the anim has completed (has the hold last frame flag
								IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
									TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose - whilst waiting for initial chase dialogue to finish") ENDIF #ENDIF
								ENDIF
							ELSE
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose - whilst waiting for initial chase dialogue to finish") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
			ELSE
				MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE()
				MANAGE_DIALOGUE_DURING_CHASE_CELEB()
				IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sCelebPed.ped)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sCelebPed.ped, -1, SLF_FAST_TURN_RATE)
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
		
			IF NOT DOES_BLIP_EXIST(sCelebPed.blip)
				sCelebPed.blip = CREATE_PED_BLIP(sCelebPed.ped, TRUE, TRUE)
				SET_BLIP_SCALE(sCelebPed.blip, 1.0)	//TEMP: cars need bigger blips but the new rc_helpers_function - CREATE_PED_BLIP doesn't handle it.
			ELSE
				IF IS_PED_SITTING_IN_ANY_VEHICLE(sCelebPed.ped)
					SET_BLIP_SCALE(sCelebPed.blip, 1.0)	
				ENDIF
				UPDATE_CHASE_BLIP(sCelebPed.blip, sCelebPed.ped, CELEB_ESCAPED_DIST, PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE)
			ENDIF
			// new rival pap dialogue first 
			IF NOT bDoneDialogue_RivalPapTookPhoto[0]
				IF fTimeInRivalPapRecording > 1000.0	//moment rival appears
					IF fDistance_PlayerToRivalPap < 120.0 //only play if the player is close enough to see the rival arrive
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RP1", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							// Fuck you Beverly, this one's mine!
							bDoneDialogue_RivalPapTookPhoto[0] = TRUE
						ENDIF
					ELSE
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DELAY RIVAL PAP ARRIVE DIALOGUE, BEVERLY NOT CLOSE ENOUGH") ENDIF #ENDIF
					ENDIF
				ENDIF			
			ELIF NOT bDoneDialogue_RivalPapTookPhoto[1]	//This gets set to true in the Stage_Skip proc, when skipping the stage
				IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_RP1")	// Fuck you Beverly, this is my exclusive!
					IF fTimeInRivalPapRecording > 1000.0	//moment rival begins taking photos
						IF fDistance_PlayerToRivalPap < 120.0 //only play if the player is close enough to see the rival arrive
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RP2", CONV_PRIORITY_MEDIUM)
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
								// No, no, it's that bottom-feeder Madison!   
								// Shit, he's getting the money shots! Where's your dignity!?
								// Forget Miranda, we can't let him steal my exclusive.  
								// Get alongside him and I'll knock the weasel off.
								bDoneDialogue_RivalPapTookPhoto[1] = TRUE
							ENDIF
						ELSE
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DELAY bEVERL RIVAL PAP ARRIVE DIALOGUE, BEVERLY NOT CLOSE ENOUGH") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				TEXT_LABEL_31 tlTempRoot
				tlTempRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				IF NOT bBeverly_SpottedRivalPap_Anim
				AND IS_SCRIPTED_CONVERSATION_ONGOING()	// B*1555893 - make sure convo is playing out before triggering (not being built)
				AND ARE_STRINGS_EQUAL(tlTempRoot, "PAP1_RP2")
					IF SET_BEVERLY_PLAY_POINT_ANIM(sRivalPapPed.ped)
						bBeverly_SpottedRivalPap_Anim = TRUE
					ENDIF
				ELSE
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")			
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)	// wait for point anim to finish
							// make sure the anim has completed (has the hold last frame flag
							IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, tlAnimName_BeverlyPoint)
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	// keep these slow bend out for hit timings
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose") ENDIF #ENDIF
							ENDIF
						ELSE
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	// keep these slow bend out for hit timings
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "set Bev to idle pose") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "BEV FINISHED SPOTTED RIVAL PAP DIALOGUE SO GO TO SS_CLEANUP") ENDIF #ENDIF
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
				IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sRivalPapPed.ped)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRivalPapPed.ped, -1, SLF_FAST_TURN_RATE)
				ENDIF
				IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, sRivalPapPed.ped)
					TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, sRivalPapPed.ped, -1, SLF_FAST_TURN_RATE)
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_STAGE_STOP_RIVAL_PAP
		
			IF NOT bDoneObjective_StopTheRivalPaparazzi
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
						SET_VEHICLE_TYRES_CAN_BURST(sRivalPapVehicle.vehicle, TRUE)
						
						IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
						AND IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapDriverPed.ped, TRUE)
							SET_PED_CAN_BE_TARGETTED(sRivalPapDriverPed.ped, TRUE)
							SET_PED_AS_ENEMY(sRivalPapDriverPed.ped, TRUE)		// B*1535622 - makes sure HUD elements are set for enemy ped
							SET_ENTITY_IS_TARGET_PRIORITY(sRivalPapDriverPed.ped, TRUE)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapDriverPed.ped, KNOCKOFFVEHICLE_HARD) // swapped to hard as trial for B*1050458 
							SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_CAN_SCREAM | FA_WANDER_AT_END | FA_LOOK_FOR_CROWDS | FA_USE_COVER | FA_PREFER_PAVEMENTS, FALSE)
							SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_USE_VEHICLE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sRivalPapDriverPed.ped, CA_ALWAYS_FLEE, TRUE)					
							SET_PED_COMBAT_ATTRIBUTES(sRivalPapDriverPed.ped, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sRivalPapDriverPed.ped, CA_AGGRESSIVE, FALSE)
						ENDIF
						IF IS_PED_UNINJURED(sRivalPapPed.ped)
						AND IS_PED_SITTING_IN_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle)							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRivalPapPed.ped, TRUE)
							SET_PED_CAN_BE_TARGETTED(sRivalPapPed.ped, TRUE)
							SET_PED_AS_ENEMY(sRivalPapPed.ped, TRUE)		// B*1535622 - makes sure HUD elements are set for enemy ped
							SET_ENTITY_IS_TARGET_PRIORITY(sRivalPapPed.ped, TRUE)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_HARD) // swapped to hard as trial for B*1050458 
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RIVAL PAP PED HAS BEEN SETUP FOR IDLE ANIMS ON BIKE") ENDIF #ENDIF
						ENDIF						
					ENDIF
					IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
					AND bFinishedStageSkipping
						SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_RIVAL)
					ENDIF
					PRINT_NOW("PAP1_03", DEFAULT_GOD_TEXT_TIME, 1)	//Stop the ~r~rival paparazzi.~s~
					PRINT_HELP("PAP1_H1")	// Drive next to the rival paparazzi so Beverly can knock him off.~s~								
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_PAP1_STOP_RIVAL_PAP, "Stop the rival paparazzi") // 2nd Mission replay checkpoint TEMP POINT NEEDS OWN STAGE SETTING UP
					g_replay.iReplayInt[REPLAY_PHOTOS_TAKEN] = iGoodPhotosTakenCounter		// store the global variale used to setup the state on a replay					
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REPLAY CHECKPOINT SET - stop rival pap ^^^") ENDIF #ENDIF								
					bDoneObjective_StopTheRivalPaparazzi = TRUE
				ENDIF
			ELSE	
				MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK(sBeverlyPed.ped, sRivalPapVehicle.vehicle) //, FALSE, sObjCameraBeverly.object)
				IF NOT bDoneDialogue_RivalPapTookPhoto[2]
					// change this initial dialogue to remind player to chase rival pap if he's still going after celeb
					IF fCurrentChaseDistanceCeleb < fDistance_PlayerToRivalPap
					AND IS_PED_UNINJURED(sRivalPapPed.ped)
					AND NOT IS_PED_FACING_PED(PLAYER_PED_ID(), sRivalPapPed.ped, 90.0)
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_03")	//*commented out for new display message check with subtitle profile settings
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CONT", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// I said leave Miranda, she can wait!
								// No, no, no, get after that motorbike!
								// What are you doing? We can't let that hack get away!
								bDoneDialogue_RivalPapTookPhoto[2] = TRUE							
							ENDIF
						//ENDIF
					ELSE
						//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_03")	//*commented out for new display message check with subtitle profile settings
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RPAP02", CONV_PRIORITY_MEDIUM)
								//Stay on that bastard. 
								bDoneDialogue_RivalPapTookPhoto[2] = TRUE							
							ENDIF
						//ENDIF
					ENDIF
				ELSE
					//special ability help text
					IF NOT IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())									
						IF NOT bDone_ShowSpecialAbilityHelp									
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP1_H1")  // Drive next to the rival paparazzi so Beverly can knock him off.~s~
								INT iTempStatValue
								STAT_GET_INT(SP1_SPECIAL_ABILITY, iTempStatValue)	// Franklin check Franklin has some charge in his bar
								IF iTempStatValue > 0	// 30 is full for some reason
									IF fDistance_PlayerToRivalPap < 60.0
										IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
											PRINT_HELP("PAP1_H2_KM") // ~s~Press ~INPUT_SPECIAL_ABILITY~ to activate Franklin's special ability.~s~
										ELSE
											PRINT_HELP("PAP1_H2") // ~s~Hold ~INPUT_VEH_SPECIAL_ABILITY_FRANKLIN~ to activate overdrive mode, this will focus Franklin and improve his reflexes.~s~
										ENDIF
										bDone_ShowSpecialAbilityHelp = TRUE
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP - special ability help set to print PAP1_H2") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("PAP1_H2")	// ~s~Hold ~INPUT_VEH_SPECIAL_ABILITY_FRANKLIN~ to activate overdrive mode, this will focus Franklin and improve his reflexes.~s~
						IF IS_PC_VERSION()
							CLEAR_SPECIFIC_HELP_TEXT_FROM_DISPLAYING("PAP1_H2_KM")	// ~s~Hold ~INPUT_VEH_SPECIAL_ABILITY_FRANKLIN~ to activate overdrive mode, this will focus Franklin and improve his reflexes.~s~
						ENDIF
						bDone_ShowSpecialAbilityHelp = TRUE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP - special ability help cleared / toggled don't print") ENDIF #ENDIF
					ENDIF								
					MANAGE_ADDITIONAL_DIALOGUE_FOR_CHASING_RIVAL_PAP()
				ENDIF				
			ENDIF			
			IF bDoneObjective_StopTheRivalPaparazzi
				IF NOT DOES_BLIP_EXIST(sRivalPapPed.blip)
					sRivalPapPed.blip = CREATE_PED_BLIP(sRivalPapPed.ped, TRUE, FALSE)
					IF DOES_BLIP_EXIST(sRivalPapPed.blip)
						SET_BLIP_SCALE(sRivalPapPed.blip, 1.0)	//TEMP: cars need bigger blips but the new rc_helpers_function - CREATE_PED_BLIP doesn't handle it.
					ENDIF
				ELSE
					IF IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapPed.ped)
						SET_BLIP_SCALE(sRivalPapPed.blip, 1.0)	
					ENDIF
					UPDATE_CHASE_BLIP(sRivalPapPed.blip, sRivalPapPed.ped, RIVAL_PAP_ESCAPED_DIST, PAP1_RIVAL_PAP_CHASE_BLIP_FLASH_PERCENTAGE)
				ENDIF
				SAFE_REMOVE_BLIP(sCelebPed.blip)	
			ELSE
				IF NOT DOES_BLIP_EXIST(sCelebPed.blip)
					sCelebPed.blip = CREATE_PED_BLIP(sCelebPed.ped, TRUE, TRUE)
					IF DOES_BLIP_EXIST(sCelebPed.blip)
						SET_BLIP_SCALE(sCelebPed.blip, 1.0)	//TEMP: cars need bigger blips but the new rc_helpers_function - CREATE_PED_BLIP doesn't handle it.
					ENDIF
				ELSE
					IF IS_PED_SITTING_IN_ANY_VEHICLE(sCelebPed.ped)
						SET_BLIP_SCALE(sCelebPed.blip, 1.0)	
					ENDIF
					UPDATE_CHASE_BLIP(sCelebPed.blip, sCelebPed.ped, CELEB_ESCAPED_DIST, PAP1_CELEB_CHASE_BLIP_FLASH_PERCENTAGE)
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(sRivalPapPed.ped)
				IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sRivalPapPed.ped)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRivalPapPed.ped, -1, SLF_FAST_TURN_RATE)
				ENDIF
				IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, sRivalPapPed.ped)
					TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, sRivalPapPed.ped, -1, SLF_FAST_TURN_RATE)	
				ENDIF
			ENDIF
		BREAK	
		
		CASE MISSION_STAGE_DROP_BEVERLY_OFF	
		
			IF NOT bDoneDialogue_RivalPapCameraDestroyed
				IF TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION()
					bDoneDialogue_RivalPapCameraDestroyed = TRUE
				ENDIF
			ELSE
				IF NOT DOES_BLIP_EXIST(blipIndexDropOffPoint)
					blipIndexDropOffPoint = CREATE_COORD_BLIP(vDropOffPoint)
					vPathNodeRequestStartPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vDropOffPoint, vPathNodeRequestMin, vPathNodeRequestMax)
				ELSE				
					// don't keep requesting once they loaded ...since this is only used to make the gps route appear faster
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vPathNodeRequestStartPosition, << 0.0, 0.0, 0.0 >>)
						PAP1_REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPathNodeRequestStartPosition, vDropOffPoint, vPathNodeRequestMin, vPathNodeRequestMax)
						IF PAP1_ARE_REQUEST_PATH_NODES_LOADED_IN_AREA(vPathNodeRequestMin, vPathNodeRequestMax)
							vPathNodeRequestStartPosition = << 0.0, 0.0, 0.0 >>
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_DROP_BEVERLY_OFF - nodes loaded so reset vPathNodeRequestStartPosition") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF			
				IF NOT bDoneWaitForCameraDestroyedDialogue
					// have to use this to load in the blip GPS for the drop off point
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF bDoneWaitForCameraDestroyedDialogue TRUE ")
						bDoneWaitForCameraDestroyedDialogue = TRUE
					ENDIF
				ELIF NOT bDoneObjective_DropBeverlyOff
					PRINT_NOW("PAP1_05", DEFAULT_GOD_TEXT_TIME, 1)	// Take Beverly to his next ~y~stakeout.~s~						
					bDoneObjective_DropBeverlyOff = TRUE
				ELSE
					MANAGE_DIALOGUE_DURING_DROP_BEVERLY_OFF()
				ENDIF
			ENDIF
			
			//cleanup knock off anim if it's finished
			IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
				IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
					STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "cleared Beverly KO anim from Drop Off Beverly - appled on bike idle") ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//if player and bev are chatting play chat on bike anims
					FLOAT fAnimStartPhase
					fAnimStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//setup the idle on bike pose
						TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY, fAnimStartPhase)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "%anim set BEVERLY dialog_pap at phase - ", fAnimStartPhase) ENDIF #ENDIF
					ENDIF
				ELSE				
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	//setup the idle on bike pose
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//remove chat on bike anim when convo is finished
							//STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", REALLY_SLOW_BLEND_OUT)	
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "%%%%% anim stopped BEVERLY dialog_pap - REALLY_SLOW_BLEND_IN  %%%%%%") ENDIF #ENDIF
						ELSE
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET BEVERLY TO IDLE POSE") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MISSION_STAGE_LOSE_THE_COPS
			IF NOT bDoneDialogue_RivalPapCameraDestroyed
				IF TRIGGER_KNOCKED_RIVAL_OFF_BIKE_CONVERSATION()
					bDoneDialogue_RivalPapCameraDestroyed = TRUE
				ENDIF
			ELIF NOT bDoneWaitForCameraDestroyedDialogue
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF bDoneWaitForCameraDestroyedDialogue TRUE ")
					bDoneWaitForCameraDestroyedDialogue = TRUE
				ENDIF		
			ELIF NOT bDoneDialogue_LoseTheCops_InitialChat
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_LTC01", CONV_PRIORITY_MEDIUM)
					// Oh shit, it's the cops. Get rid of them. 
					// 5-0, man! They're on to me! Lose them!  
					bDoneDialogue_LoseTheCops_InitialChat = TRUE
				ENDIF
			//ELIF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_09")	// don't do if Lose the cops objective is currently being displayed		//*commented out for new display message check with subtitle profile settings	
			ELIF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_LTC01")	//don't play if initial cops dialogue is playing
				IF NOT bDoneObjective_LoseTheCops
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "lose cops objective displayed ^^^^^^ +++++ ^^^^^") ENDIF #ENDIF
					PRINT_NOW("PAP1_09", DEFAULT_GOD_TEXT_TIME, 1)	// Lose the cops.
					iDialogueTimer_LoseTheCopsAdditionalChat = GET_GAME_TIMER()	//reset the timer for additional dialogue										
					bDoneObjective_LoseTheCops = TRUE
				ELSE		
					//Play bev comment about losing the cops, setup in D* as random dialogue
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						iDialogueTimer_LoseTheCopsAdditionalChat = GET_GAME_TIMER()
					ELSE
						IF NOT bDoneDialogue_FranklinRespondToBeverlyBerating	// reset this to ensure Franklin doesn't say something straight away
							IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
								IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_RESP", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())							
									// Yeah, yeah, I got it. 
									// Maybe if you shut up for a second.  
									// What do you think I'm doin'?
									// Fuck, I know, I'm tryin'!
									// Are you always this annoying?  
									// I get the idea, homie.  
									// You really ain't helpin'.
									// We might do better if you shut the fuck up.
									// I heard you the first time. 
									iDialogueTimer_LoseTheCopsAdditionalChat = GET_GAME_TIMER()
									bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// bypass response if we are out range of each other
									//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOSE THE COPS ADDITONAL DIALOGUE TRIGGERED - Franklin Response - PAP1_RESP") ENDIF #ENDIF
								ENDIF
							ELSE
								bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// bypass response if we are out range of each other
							ENDIF
						ELSE
							// Franklin drives into a ped
							IF (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) >= 0)	// looks to be initially set to -1 so need to check from 0
							AND (GET_TIME_SINCE_PLAYER_HIT_PED(PLAYER_ID()) < 500)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_HITPED", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
										// It's okay, they're fine.  
										// That's it, no prisoners!  
										// Collateral damage, don't worry about it.    
										// Good, if they get in the way, take them down!
										// Move next time!
										iDialogueTimer_LoseTheCopsAdditionalChat = GET_GAME_TIMER()
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOSE THE COPS - drove into ped dialogue - PAP1_HITPED") ENDIF #ENDIF
									ENDIF
								ENDIF
							ELSE	
								IF (GET_GAME_TIMER() - iDialogueTimer_LoseTheCopsAdditionalChat > TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE_LONG)
								AND (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
									IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_LTC02", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())							
										// Go, go! I'm not going down for this!  
										// So, I should probably tell you, this isn't one hundred percent legal.  
										// Come on, lose them already!  
										// Hit the gas, man! They're still on us.  
										// You gotta get rid of them. I'm in breach of my restraining order!  
										// If we get caught, I'm saying you kidnapped me.
										iDialogueTimer_LoseTheCopsAdditionalChat = GET_GAME_TIMER()
										bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// B*1573363 - don't allow Franklin to respond
										//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOSE THE COPS ADDITONAL DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//cleanup knock off anim if it's finished
			IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
				IF HAS_ENTITY_ANIM_FINISHED(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff)
					STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, sAnimNameBeverlyKnockOff, SLOW_BLEND_OUT)
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "cleared Beverly KO anim from Drop Off Beverly - appled on bike idle") ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//if player and bev are chatting play chat on bike anims
					FLOAT fAnimStartPhase
					fAnimStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0, 1)
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//setup the idle on bike pose
						TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY, fAnimStartPhase)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "%anim set BEVERLY dialog_pap at phase - ", fAnimStartPhase) ENDIF #ENDIF
					ENDIF
				ELSE				
					IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")	//setup the idle on bike pose
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap")	//remove chat on bike anim when convo is finished
							//STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "dialog_pap", REALLY_SLOW_BLEND_OUT)	
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "%%%%% anim stopped BEVERLY dialog_pap - REALLY_SLOW_BLEND_IN  %%%%%%") ENDIF #ENDIF
						ELSE
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	// NORMAL_BLEND_IN
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET BEVERLY TO IDLE POSE") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
		/* ^^^ bev alt anim during Franklin helmet anim change 
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@sport@front@base", "put_on_helmet")		// B*1422622 - Franklin's put on helmet animation cause him to clip through Beverly
			IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")				
				TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE - eset Bev to idle pose") ENDIF #ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")
				REQUEST_ANIM_DICT("veh@bike@sport@rear@base")
				IF HAS_ANIM_DICT_LOADED("veh@bike@sport@rear@base")
					TASK_PLAY_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE - applied still on bike anim whilst player puts on helmet") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE - requestin anim dict for still idle -  veh@bike@sport@rear@base for player puts on helmet") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF	*/

		IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
			IF NOT IS_ENTITY_VISIBLE(sObjCameraBeverly.object)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)						
					SET_ENTITY_VISIBLE(sObjCameraBeverly.object, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE - reset Bev camera Visible FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
		SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_DisableInVehicleActions, TRUE)	// B*1480880 - stop bev clipping during reverse anims
		IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
			STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE ON BIKE - stopped  cam override anim as Bev on the bike") ENDIF #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the mission flow for when the player is off Beverly's bike
///    moved this code here to improve readability as MANAGE_PLAYER_AND_BEVERLY() had became pretty big
PROC MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE()

	IF NOT DOES_BLIP_EXIST(sBeverlyVehicle.blip)
		sBeverlyVehicle.blip = CREATE_VEHICLE_BLIP(sBeverlyVehicle.vehicle)
	ENDIF
	
	IF NOT bDoneObjective_GetBackOnBike
		IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, TRUE)
			// don't print the objective if the player is in the process of getting onto the bike
		ELSE
			PRINT_NOW("PAP1_06", DEFAULT_GOD_TEXT_TIME, 1)	// Get back on Beverly's ~b~motorbike.~s~
			bDoneObjective_GetBackOnBike = TRUE
		ENDIF
	ELSE	//IF NOT IS_THIS_PRINT_BEING_DISPLAYED("PAP1_06") // Get back on Beverly's ~b~motorbike.~s~	//*commented out for new display message check with subtitle profile settings	
		
		//Play bev comment about getting back on the bike, setup in D* as random dialogue
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
		ELSE
			IF (GET_GAME_TIMER() - iDialogueTimer_PlayerOffTheBike > TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
				IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
					IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
						// can only play specifc lines after this point since some don't fit when you aren't chasing miranda
						IF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
							IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
								IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_IG5", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())							
									// Come on! Come on!
									// This is the opportunity of a lifetime!
									// Miranda Cowan, man! This is immense!
									// What are you waiting for? This is important!  
									// Oh my God, if she gets away, I will lose my shit!!!
									iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - GET BACK ON THE BIKE DIALOGUE TRIGGERED - PAP1_IG5") ENDIF #ENDIF
								ENDIF
							ELSE
								IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_FALL", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())								
									// Back on the bike, man. We're gonna lose her!
									// You're fine, get back on. We've gotta get after them! 
									// Come on, let's go again! We can still catch them.
									iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - GET BACK ON THE BIKE DIALOGUE TRIGGERED - PAP1_FALL") ENDIF #ENDIF
								ENDIF
							ENDIF
						ELSE
							// having to use alternative for rest of mission stages since PAP1_IG5 and PAP1_FALL references Miranda
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_CRASH", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// You idiot!
								// For Christ's sake!
								// Are you blind?
								// Is it that hard?
								// No! You moron!
								// You have gotta be kidding me!
								iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - GET BACK ON THE BIKE DIALOGUE alternative dialogue - PAP1_CRASH") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bHasBeverlyBeenRemovedFromPlayerGroup
		IF SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP()
			IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
				IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
					REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - *PED GROUP* BEV removed from group Left Behind - PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV Unable to remove on first call  PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
				CLEAR_PED_TASKS(sBeverlyPed.ped)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - BEV - cleared tasks as getting in vehicle when removed from player group") ENDIF #ENDIF
			ENDIF
			UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM(TRUE)
			bHasBeverlyBeenRemovedFromPlayerGroup = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE bHasBeverlyBeenRemovedFromPlayerGroup = TRUE") ENDIF #ENDIF
		ELSE
			IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
				IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, TRUE)
					// B*1521660 - cancel enter task if player is being a dick
					FLOAT fBikeSpeed = GET_ENTITY_SPEED(sBeverlyVehicle.vehicle) 
					IF fBikeSpeed < 1.75
					AND IS_ENTITY_IN_RANGE_ENTITY(sBeverlyPed.ped, sBeverlyVehicle.vehicle, 8.0)
						IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)
							TASK_BEVERLY_TO_GET_ON_HIS_BIKE()
						ENDIF
					ELSE
						IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)					
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
								CLEAR_PED_TASKS(sBeverlyPed.ped)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY cleared enter vehicle task : speed was ", fBikeSpeed) ENDIF #ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)
						CLEAR_PED_TASKS(sBeverlyPed.ped)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY bev TASK_ENTER_VEHICLE cleared : FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				CLEANUP_BEVERLY_ON_BIKE_ANIMS()
				
				// B*1338882 - player not given enough time to get back on the bike to rejoin chase
				IF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
				OR eMissionStage = MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
				OR eMissionStage = MISSION_STAGE_STOP_RIVAL_PAP
					IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)
						IF NOT IS_ENTITY_IN_RANGE_COORDS(sBeverlyPed.ped, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 2.0)	// B*1898752 - don't override speed if Beverly is right next to player
							SET_PED_MIN_MOVE_BLEND_RATIO(sBeverlyPed.ped, 1.5)	// group behaviour automatically makes ped walk slowing to get in a vehicle, i need it to be faster
						ENDIF
					ENDIF
					SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_UseFastEnterExitVehicleRates, TRUE)	// to improve slowness getting in vehicles
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		IF SHOULD_BEVERLY_JOIN_PLAYER_GROUP()
			CLEAR_PED_TASKS(sBeverlyPed.ped)
			SET_BEVERLY_FOLLOW_PLAYER()
			//play Bev comment about player leaving him behind
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PRTB01", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - Bev comment about player leaving him triggered     ]]]") ENDIF #ENDIF
					// Where the hell did you go?  
					// I thought you'd bailed on me, man!  
					// Get your head in the game!
					// There you are, Christ, stay with me!  
					// I thought we were a team here. 
					// We don't have time to mess around.
				ENDIF
			ENDIF						
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE bHasBeverlyBeenRemovedFromPlayerGroup = FALSE") ENDIF #ENDIF
			bHasBeverlyBeenRemovedFromPlayerGroup = FALSE
		ELSE
			// having to keep checking this here now because of // B*1441191
			IF IS_PED_IN_GROUP(sBeverlyPed.ped)
				IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
					REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - *PED GROUP* BEV removed from group *on further attempts after ragdoll initial check* - PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE") ENDIF #ENDIF
				ENDIF
			ENDIF
			
			UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM()
			//Play bev comment about leaving him behind, setup in D* as random dialogue
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDialogueTimer_BevLeftBehind = GET_GAME_TIMER()
			ELSE
				IF (GET_GAME_TIMER() - iDialogueTimer_BevLeftBehind > TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
					IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE + 10)	// larger range to give this the chance to play
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_IG6", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "BEV LEFT BEHIND DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
							iDialogueTimer_BevLeftBehind = GET_GAME_TIMER()
							// Don't leave me here!
							// Do you know who I am?
							// You're walking away from fame, man!  
							// I make reputations! I break reputations!
							// Wait, this is your chance to make a difference!
							// Do you want to be a nobody your whole life?  
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, PLAYER_PED_ID())
		TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
	ENDIF
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()	// technically covered by above look at task but added to show usage.
	IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sBeverlyPed.ped)
		TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped, -1)
	ENDIF
	MANAGE_CLEANUP_OF_BEVERLY_ON_FOOT_ANIMATIONS()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
		SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_DisableInVehicleActions, TRUE)	// B*1480880 - stop bev clipping during reverse anims
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
		IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
			STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - bEV ON BIKE - stopped  cam override anim as Bev on the bike") ENDIF #ENDIF
		ENDIF
		IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
			TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - bEV ON BIKE - applied idle on bike anim FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		ENDIF
	ELSE
		IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped,sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
				RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
				//TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE -  anim NOT set BEVERLY cam_pos_hand_override  bev GETTING ON BIKE  - cam_pos_hand_override INSTANT_BLEND_IN getting on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ELSE
			IF IS_PED_GETTING_UP(sBeverlyPed.ped)
			OR IS_PED_RAGDOLL(sBeverlyPed.ped)
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped,sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE -  applied cam_pos_hand_override INSTANT_BLEND_IN getting UP FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - playing cam_pos_hand_override anim apparently FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					IF IS_ENTITY_VISIBLE(sObjCameraBeverly.object)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)						
							SET_ENTITY_VISIBLE(sObjCameraBeverly.object, FALSE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - hidden Bev camera for ragdoll / getting up FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
					STOP_ENTITY_ANIM(sBeverlyPed.ped, "cam_pos_hand_override", sAnimDictName_Pap1HandOverride, NORMAL_BLEND_OUT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - stopped cam_pos_hand_override NORMAL_BLEND_OUT not getting UP bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					IF NOT IS_ENTITY_VISIBLE(sObjCameraBeverly.object)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)						
							SET_ENTITY_VISIBLE(sObjCameraBeverly.object, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - reset Bev camera Visible FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
			ENDIF
		ENDIF
		/* fail attempt to use the cam override anim on Bev prior to him starting the custom get_in anims
			but this failed on anim playing check maybe because they are from a clipset?
		IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
				AND NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
					RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - bev GETTING ON BIKE  - RESET_PED_MOVEMENT_CLIPSET & applied cam_pos_hand_override FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
				OR IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
					RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
					STOP_ENTITY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - bev GETTING ON BIKE  - stopped cam_pos_hand_override anim for get_in anims FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "here")	// ok so this isn't working because checking if Bev is playing one of the get_in anims isn't returning when it should do.. maybe because its actually a clipset?
				ENDIF
			ENDIF
		ELSE
			SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
		ENDIF*/
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the mission flow for when the player and Beverly are on the bike.
///    moved this code here to improve readability as MANAGE_PLAYER_AND_BEVERLY() had became pretty big
PROC MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY()

	// Blip Beverly up when he's off the bike
	IF NOT DOES_BLIP_EXIST(sBeverlyPed.blip)
		sBeverlyPed.blip = CREATE_PED_BLIP(sBeverlyPed.ped, TRUE, TRUE)
	ENDIF
	
	IF NOT bHasBeverlyBeenRemovedFromPlayerGroup
		IF SHOULD_BEVERLY_BE_REMOVED_FROM_PLAYER_GROUP()			
			IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
				IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
					REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV removed from group Left Behind - PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV Unable to remove on first call  PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
				CLEAR_PED_TASKS(sBeverlyPed.ped)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bev cleared tasks as getting in vehicle when removed from player group") ENDIF #ENDIF
			ENDIF
			UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM(TRUE)
			bHasBeverlyBeenRemovedFromPlayerGroup = TRUE
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY bHasBeverlyBeenRemovedFromPlayerGroup = TRUE") ENDIF #ENDIF						
		ELSE
			IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)	
				// B*1521660 - cancel enter task if player is being a dick
				FLOAT fBikeSpeed = GET_ENTITY_SPEED(sBeverlyVehicle.vehicle) 
				IF fBikeSpeed < 1.75
				AND IS_ENTITY_IN_RANGE_ENTITY(sBeverlyPed.ped, sBeverlyVehicle.vehicle, 8.0)
					IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)					
						TASK_BEVERLY_TO_GET_ON_HIS_BIKE()
					ENDIF
				ELSE
					IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)		
						IF NOT IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
							CLEAR_PED_TASKS(sBeverlyPed.ped)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY cleared enter vehicle task : speed was ", fBikeSpeed) ENDIF #ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
				CLEANUP_BEVERLY_ON_BIKE_ANIMS()
				
				// B*1338882 - player not given enough time to get back on the bike to rejoin chase
				IF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
				OR eMissionStage = MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
				OR eMissionStage = MISSION_STAGE_STOP_RIVAL_PAP
					IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_ENTER_VEHICLE)
						SET_PED_MIN_MOVE_BLEND_RATIO(sBeverlyPed.ped, 1.5)	// group behaviour automatically makes ped walk slowing to get in a vehicle, i need it to be faster
					ENDIF
					SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_UseFastEnterExitVehicleRates, TRUE)	// to improve slowness getting in vehicles
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF SHOULD_BEVERLY_JOIN_PLAYER_GROUP()
			CLEAR_PED_TASKS(sBeverlyPed.ped)
			SET_BEVERLY_FOLLOW_PLAYER()			
			//play Bev comment about player leaving him behind
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PRTB01", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - Bev comment about player leaving him triggered     ]]]") ENDIF #ENDIF
					// Where the hell did you go?  
					// I thought you'd bailed on me, man!  
					// Get your head in the game!
					// There you are, Christ, stay with me!  
					// I thought we were a team here. 
					// We don't have time to mess around.
				ENDIF
			ENDIF						
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY bHasBeverlyBeenRemovedFromPlayerGroup = FALSE") ENDIF #ENDIF
			bHasBeverlyBeenRemovedFromPlayerGroup = FALSE
		ELSE
			// having to keep checking this here now because of // B*1441191
			IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
				IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
					REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - *PED GROUP* BEV removed from group *on further attempts after ragdoll initial check* - PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF NOT bDoneObjective_ReturnToBeverly
				PRINT_NOW("PAP1_08", DEFAULT_GOD_TEXT_TIME, 1)	//Return to ~b~Beverly.~s~
				bDoneObjective_ReturnToBeverly = TRUE
			ENDIF
			UPDATE_BEVERLY_PLAY_FRUSTRATED_ON_FOOT_ANIM()
			//Play bev comment about leaving him behind, setup in D* as random dialogue
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iDialogueTimer_BevLeftBehind = GET_GAME_TIMER()
			ELSE
				IF (GET_GAME_TIMER() - iDialogueTimer_BevLeftBehind > TIME_DELAY_FOR_NEXT_RANDOM_DIALOGUE_LINE)
					IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE + 15)	// larger range to give this the chance to play
						IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_IG6", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "BEV LEFT BEHIND DIALOGUE TRIGGERED	]]]") ENDIF #ENDIF
							iDialogueTimer_BevLeftBehind = GET_GAME_TIMER()
							// Don't leave me here!
							// Do you know who I am?
							// You're walking away from fame, man!  
							// I make reputations! I break reputations!
							// Wait, this is your chance to make a difference!
							// Do you want to be a nobody your whole life?  
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, PLAYER_PED_ID())
		TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
	ENDIF
	IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), sBeverlyPed.ped)
		TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped, -1)
	ENDIF
	MANAGE_CLEANUP_OF_BEVERLY_ON_FOOT_ANIMATIONS()
	IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
		SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_DisableInVehicleActions, TRUE)	// B*1480880 - stop bev clipping during reverse anims
		IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
			STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bEV ON BIKE - stopped  cam override anim as Bev on the bike") ENDIF #ENDIF
		ENDIF
		/* ^^^ bev alt anim during Franklin helmet anim change - change out */
		IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
			TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - bEV ON BIKE - applied idle on bike anim") ENDIF #ENDIF
		ENDIF
		/* ^^^ change out end */
		/* ^^^ bev alt anim during Franklin helmet anim change 
		IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@sport@front@base", "put_on_helmet")		// B*1422622 - Franklin's put on helmet animation cause him to clip through Beverly
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
				TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bEV ON BIKE - applied idle on bike anim") ENDIF #ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still")
				REQUEST_ANIM_DICT("veh@bike@sport@rear@base")
				IF HAS_ANIM_DICT_LOADED("veh@bike@sport@rear@base")
					TASK_PLAY_ANIM(sBeverlyPed.ped, "veh@bike@sport@rear@base", "still", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bEV ON BIKE - applied still on bike anim whilst player puts on helmet") ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bEV ON BIKE - requestin anim dict for still idle -  veh@bike@sport@rear@base for player puts on helmet") ENDIF #ENDIF
				ENDIF
			ENDIF
		ENDIF	*/
	ELSE		
		IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped,sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
				RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
				//TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - anim NOT set BEVERLY cam_pos_hand_override  bev GETTING ON BIKE  - cam_pos_hand_override INSTANT_BLEND_IN getting on bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ELSE
			IF IS_PED_GETTING_UP(sBeverlyPed.ped)
			OR IS_PED_RAGDOLL(sBeverlyPed.ped)
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped,sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE -  applied cam_pos_hand_override INSTANT_BLEND_IN getting UP FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - playing cam_pos_hand_override anim apparently FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					IF IS_ENTITY_VISIBLE(sObjCameraBeverly.object)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)						
							SET_ENTITY_VISIBLE(sObjCameraBeverly.object, FALSE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - hidden Bev camera for ragdoll / getting up FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
					STOP_ENTITY_ANIM(sBeverlyPed.ped, "cam_pos_hand_override", sAnimDictName_Pap1HandOverride, NORMAL_BLEND_OUT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - stopped cam_pos_hand_override NORMAL_BLEND_OUT not getting UP bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					IF NOT IS_ENTITY_VISIBLE(sObjCameraBeverly.object)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)						
							SET_ENTITY_VISIBLE(sObjCameraBeverly.object, TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE - reset Bev camera Visible FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF
				SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
			ENDIF
		ENDIF
		/* fail attempt to use the cam override anim on Bev prior to him starting the custom get_in anims
			but this failed on anim playing check maybe because they are from a clipset?
		IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
			IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
				AND NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
					RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bev GETTING ON BIKE  - RESET_PED_MOVEMENT_CLIPSET & applied cam_pos_hand_override FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ds", "get_in")
				OR IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_1_ps", "get_in")
					RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
					STOP_ENTITY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - bev GETTING ON BIKE  - stopped cam_pos_hand_override anim for get_in anims FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "here")	// ok so this isn't working because checking if Bev is playing one of the get_in anims isn't returning when it should do.. maybe because its actually a clipset?
				ENDIF
			ENDIF
		ELSE
			SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
		ENDIF*/		
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles player and Beverly
PROC MANAGE_PLAYER_AND_BEVERLY()	
	PAP1_PLAYER_TRANSPORT_STATE_ENUM eNewPlayerTransportState
	eNewPlayerTransportState = GET_UPDATED_PLAYER_TRANSPORT_STATE()
	IF eNewPlayerTransportState <> ePlayerTransportState		// if state has changed
		CLEANUP_PLAYER_TRANSPORT_STATE(ePlayerTransportState)	// cleanup the current state	
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MANAGE_PLAYER_AND_BEVERLY - state change - ", ePlayerTransportState, " -> ", eNewPlayerTransportState) ENDIF #ENDIF
		ePlayerTransportState = eNewPlayerTransportState		// set the new state			
	ENDIF				
	SWITCH ePlayerTransportState		
		CASE PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV	
			MANAGE_FLOW_FOR_PLAYER_AND_BEVERLY_ON_BIKE()
		BREAK
		CASE PAP1_PLAYER_TRANSPORT_STATE_OFF_THE_BIKE	
			MANAGE_FLOW_FOR_PLAYER_OFF_THE_BIKE()
		BREAK
		CASE PAP1_PLAYER_TRANSPORT_STATE_WAIT_FOR_BEVERLY		
			MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY()
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    requests the assets for the outro mocap cutscene entities so SET_CUTSCENE_PED_COMPONENT_VARIATION_... can be called
PROC SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			// B*1521921 - manually set player's components to ensure helmet appears if player is wearing it
			
			// if variation was set when player had no helmet, need to check for helmet being put on after this point inoder to reload cutscene for updated variations
			IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				bCheckForPlayerHelmetChange = TRUE
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP -  player wasn't wearing helmet - Frame Count : ", GET_FRAME_COUNT())
			ELSE
				bCheckForPlayerHelmetChange = FALSE
				CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - player wearing helmet - Frame Count : ", GET_FRAME_COUNT())
			ENDIF
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle_Franklin, PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()), CBF_MASKS|CBF_HEADSETS|CBF_PARACHUTES) 
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED player - Frame Count : ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
	IF IS_ENTITY_ALIVE(sBeverlyPed.ped)
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_Beverly, sBeverlyPed.ped)
			CPRINTLN(DEBUG_MISSION, "SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED bev - Frame Count : ", GET_FRAME_COUNT())
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    handles requesting mocap on player approach and releasing outro mocap if player moves away
///    Also sets the player variation by calling SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
PROC MANAGE_OUTRO_MOCAP_LOADING()

	IF IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDropOffPoint, DEFAULT_CUTSCENE_LOAD_DIST)
		IF NOT IS_PLAYER_CHANGING_CLOTHES()
			REQUEST_CUTSCENE(tlOutroMocapName)
			SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			bRequestedOutroMocap = TRUE
		ENDIF
	ELSE	
		IF bRequestedOutroMocap
			IF NOT IS_COORD_IN_RANGE_OF_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDropOffPoint, DEFAULT_CUTSCENE_UNLOAD_DIST)
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
					bRequestedOutroMocap = FALSE
					bCheckForPlayerHelmetChange = FALSE
					CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " Frame Count : ", GET_FRAME_COUNT())
				ENDIF
			ELSE
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
			ENDIF
		ENDIF
	ENDIF
	// B*1521921 - have to bin off the mocap if requested to start over
	IF bRequestedOutroMocap
		IF bCheckForPlayerHelmetChange	// this is only set to true when the cutscene variations are set so we know if can be binned off and start again
			IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				REMOVE_CUTSCENE()
				bRequestedOutroMocap = FALSE
				bCheckForPlayerHelmetChange = FALSE
				CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " helmet change!!! Frame Count : ", GET_FRAME_COUNT())
			ENDIF
		ENDIF
		IF IS_PLAYER_CHANGING_CLOTHES()
			REMOVE_CUTSCENE()
			bRequestedOutroMocap = FALSE
			CPRINTLN(DEBUG_MISSION, "MANAGE_OUTRO_MOCAP_LOADING - unloaded ", tlOutroMocapName, " player changing clothes! Frame Count : ", GET_FRAME_COUNT())		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE msStage)
	eMissionStage = msStage
	eSubStage = SS_SETUP
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		Script Terminated Functions
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Removes all the assets loaded by the mission, used in mission cleanup and reset mission functions.
/// PARAMS:
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC UNLOAD_ALL_MISSION_ASSETS(BOOL bClearTextSlots = TRUE)	
	REMOVE_ANIM_DICT(sAnimDictName_Pap1Specific)
	REMOVE_ANIM_DICT(tlAnimDict_BeverlyShoutComeOn)
	REMOVE_ANIM_DICT(sAnimDictName_Pap1HandOverride)	// anim dict containing camera in hand override
	REMOVE_ANIM_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(TRUE))		// anim clipset containing Bev get on bike left side override
	REMOVE_ANIM_SET(GET_BEVERLY_ENTER_VEHICLE_CLIPSET_NAME(FALSE))	// anim clipset containing Bev get on bike right side override
	REMOVE_ANIM_DICT(tlRivalPapInjuredAnims)
	REMOVE_ANIM_DICT("rcmpaparazzo1ig_4")	// ped shooting up in vehicle anim
	REMOVE_ANIM_SET(sAnimSet_BeverlyCameraWeaponMovementOnFoot)	// pap holding camera movement	
	REMOVE_PTFX_ASSET()
	RELEASE_SOUND_ID(iSoundId_BikeAttack_MeleeHit)
	RELEASE_SOUND_ID(iSoundId_BikeAttack_MeleeSwipe)
	RELEASE_SOUND_ID(iSoundId_CameraFlash)
	RELEASE_SCRIPT_AUDIO_BANK()
	UNREGISTER_SCRIPT_WITH_AUDIO()	
	//uber recording stuff
	REMOVE_VEHICLE_RECORDING(1, "Pap1RP")
	REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
	CLEANUP_UBER_PLAYBACK()
	REMOVE_VEHICLE_RECORDING(1, "Pap1PV")	//Special case recording for player vehicle, requested and used when skipping to stop rival pap stage	
	SET_MODEL_AS_NO_LONGER_NEEDED(sBeverlyPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sBeverlyVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebsDriverPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sCelebVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapPed.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapDriverPed.model)	
	SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapVehicle.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjCameraBeverly.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjFlashUnitBeverly.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjCameraRivalPap.model)
	SET_MODEL_AS_NO_LONGER_NEEDED(sObjFlashUnitRivalPap.model)
	REMOVE_VEHICLE_ASSET(sBeverlyVehicle.model)	//entry / exit animations for Beverly's bike		
	IF bClearTextSlots
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Deletes all of the blips used in the mission, checking if they exist.
PROC DELETE_ALL_MISSION_BLIPS()
	SAFE_REMOVE_BLIP(sCelebPed.blip)
	SAFE_REMOVE_BLIP(sCelebVehicle.blip)
	SAFE_REMOVE_BLIP(sRivalPapPed.blip)
	SAFE_REMOVE_BLIP(sRivalPapVehicle.blip)
	SAFE_REMOVE_BLIP(blipIndexDropOffPoint)
ENDPROC

/// PURPOSE:
///    Deletes all of the mission entities checking if they exist
///    Used in Mission Failed, when faded out.
/// PARAMS:
///    bDelete - if true all entities will be deleted, else released
PROC CLEANUP_ALL_MISSION_ENTITIES(BOOL bDelete = FALSE)

	CPRINTLN(DEBUG_MISSION, "CLEANUP_ALL_MISSION_ENTITIES() ~~~~~ called bDelete = ", bDelete)
	
	SAFE_REMOVE_OBJECT(sObjSyringe.object, bDelete)
	
	// B*1493509 - don't detach on release
	IF DOES_ENTITY_EXIST(sObjFlashUnitBeverly.object)		
		ONLY_CLEAN_UP_OBJECT_WHEN_OUT_OF_RANGE(sObjFlashUnitBeverly.object)
	ENDIF
	IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)		
		ONLY_CLEAN_UP_OBJECT_WHEN_OUT_OF_RANGE(sObjCameraBeverly.object)
	ENDIF
	IF DOES_ENTITY_EXIST(sObjCameraRivalPap.object)		
		ONLY_CLEAN_UP_OBJECT_WHEN_OUT_OF_RANGE(sObjCameraRivalPap.object)
	ENDIF
	IF DOES_ENTITY_EXIST(sObjFlashUnitRivalPap.object)		
		ONLY_CLEAN_UP_OBJECT_WHEN_OUT_OF_RANGE(sObjFlashUnitRivalPap.object)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objTrafficLightWestBound)
		SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightWestBound, TLO_NONE)	// release control of the traffic lights
	ENDIF
	SAFE_REMOVE_OBJECT(objTrafficLightWestBound, FALSE)	// don't delete the traffic lights grabbed from the world
	IF DOES_ENTITY_EXIST(objTrafficLightEastBound)
		SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightEastBound, TLO_NONE)	// release control of the traffic lights
	ENDIF				
	SAFE_REMOVE_OBJECT(objTrafficLightEastBound, FALSE)	// don't delete the traffic lights grabbed from the world
	
	IF IS_PED_UNINJURED(sBeverlyPed.ped)	
		IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
			// B*1580046 - to help
			SET_PED_CAN_TELEPORT_TO_GROUP_LEADER(sBeverlyPed.ped, PLAYER_GROUP_ID(), FALSE)
			SET_PED_NEVER_LEAVES_GROUP(sBeverlyPed.ped, FALSE)
			
			IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
				REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)
				CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV removed from group Left Behind - CLEANUP_ALL_MISSION_ENTITIES")
			ELSE
				SET_ENTITY_HEALTH(sBeverlyPed.ped, 0)
				CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV Unable to remove ragdoll so set his health to 0! CLEANUP_ALL_MISSION_ENTITIES")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "MANAGE_FLOW_FOR_WAIT_FOR_BEVERLY - *PED GROUP* BEV not in player group  CLEANUP_ALL_MISSION_ENTITIES")
		ENDIF
		RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
		SET_ENTITY_LOAD_COLLISION_FLAG(sBeverlyPed.ped, FALSE)
		SET_PED_KEEP_TASK(sBeverlyPed.ped, TRUE)
	ENDIF	
	SAFE_REMOVE_PED(sBeverlyPed.ped, bDelete)
	
	IF IS_PED_UNINJURED(sCelebPed.ped)
		SET_ENTITY_LOAD_COLLISION_FLAG(sCelebPed.ped, FALSE)
		SET_PED_KEEP_TASK(sCelebPed.ped, TRUE)
	ENDIF
	SAFE_REMOVE_PED(sCelebPed.ped, bDelete)	
	
	IF IS_PED_UNINJURED(sCelebsDriverPed.ped)
		SET_PED_KEEP_TASK(sCelebsDriverPed.ped, TRUE)
	ENDIF
	SAFE_REMOVE_PED(sCelebsDriverPed.ped, bDelete)	
	
	IF IS_PED_UNINJURED(sRivalPapPed.ped)
		SET_ENTITY_LOAD_COLLISION_FLAG(sRivalPapPed.ped, FALSE)
		SET_PED_KEEP_TASK(sRivalPapPed.ped, TRUE)
	ENDIF
	SAFE_REMOVE_PED(sRivalPapPed.ped, bDelete)	
	
	IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
		SET_PED_KEEP_TASK(sRivalPapDriverPed.ped, TRUE)
	ENDIF
	SAFE_REMOVE_PED(sRivalPapDriverPed.ped, bDelete)	
	
	IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
		SET_VEHICLE_CAN_LEAK_OIL(sBeverlyVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(sBeverlyVehicle.vehicle, TRUE)
		MODIFY_VEHICLE_TOP_SPEED(sBeverlyVehicle.vehicle, 0.0)
		SET_VEHICLE_STRONG(sBeverlyVehicle.vehicle, FALSE)
	ENDIF
	SAFE_REMOVE_VEHICLE(sBeverlyVehicle.vehicle, bDelete)	
	IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
		FREEZE_ENTITY_POSITION(sCelebVehicle.vehicle, FALSE)
		SET_VEHICLE_CAN_LEAK_OIL(sCelebVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(sCelebVehicle.vehicle, TRUE)
		SET_DISABLE_PRETEND_OCCUPANTS(sCelebVehicle.vehicle, TRUE)
	ENDIF
	SAFE_REMOVE_VEHICLE(sCelebVehicle.vehicle, bDelete)	
	IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
		SET_VEHICLE_CAN_LEAK_OIL(sRivalPapVehicle.vehicle, TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(sRivalPapVehicle.vehicle, TRUE)
		SET_DISABLE_PRETEND_OCCUPANTS(sRivalPapVehicle.vehicle, TRUE)
		MODIFY_VEHICLE_TOP_SPEED(sRivalPapVehicle.vehicle, 0.0)
	ENDIF
	SAFE_REMOVE_VEHICLE(sRivalPapVehicle.vehicle, bDelete)	
ENDPROC

/// PURPOSE:
///    Mission cleanup
/// PARAMS:
///    bDeleteAll - if TRUE all entities are deleted, else released
///    bClearTextSlots - if TRUE CLEAR_ADDITIONAL_TEXT on MISSION_TEXT_SLOT
PROC MISSION_CLEANUP(BOOL bDeleteAll = FALSE, BOOL bClearTextSlots = TRUE)
	INT i
	CLEAR_PRINTS()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP(TRUE)
	ENDIF
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	KILL_ANY_CONVERSATION()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	DISABLE_CELLPHONE(FALSE)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_TIME_SCALE(1)
	RC_END_CUTSCENE_MODE()	
	SET_AGGRESSIVE_HORNS(FALSE)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUS, FALSE) 
	SET_VEHICLE_MODEL_IS_SUPPRESSED(COACH, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(MULE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(MULE2, FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	//intro area
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingIntroArea)
	FOR i = 0 TO (PAP1_MAX_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
		REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
	ENDFOR
	SET_SCENARIO_GROUP_ENABLED("ATTRACT_PAP", TRUE)	// vehicle park up chain at the drop off point
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-1514.624146,-211.543411,46.786194>>, <<-1487.885254,-190.754852,59.494499>>, 25.000000)	// drop off area
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, TRUE)	
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<-116.820381,261.006592,101.028191>>, <<-206.292038,266.383789,86.079262>>, 15.000000)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>, TRUE)
	//SET_ROADS_BACK_TO_ORIGINAL(<< -227.98399, 209.28452, 75.96078 >>, << -210.06294, 244.48195, 99.00089 >>)	//across road on hill junction (done since i have an uber car coming up straight after mocap
	SET_ROADS_BACK_TO_ORIGINAL(<< -272.71304, 209.47295, 75.91507 >>, << -166.00862, 270.97842, 99.37162 >>)	// major area covering the junction where I want the cars to move from
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PED_UNINJURED(PLAYER_PED_ID())							
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
		ENDIF		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF DOES_CAM_EXIST(camCutsceneStart)
		DESTROY_CAM(camCutsceneStart)
	ENDIF
	IF DOES_CAM_EXIST(camCutsceneFinish)
		DESTROY_CAM(camCutsceneFinish)
	ENDIF
	IF IS_ENTITY_ALIVE(sCelebVehicle.vehicle)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sCelebVehicle.vehicle)
	ENDIF
	IF IS_ENTITY_ALIVE(sRivalPapVehicle.vehicle)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sRivalPapVehicle.vehicle)
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_INTRO")
		STOP_AUDIO_SCENE("PAPARAZZO_01_INTRO")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_CHASE")		
		STOP_AUDIO_SCENE("PAPARAZZO_01_CHASE")
	ENDIF
	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
		SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_FAIL)
	ENDIF
	SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)	// reset to default value
	WAIT_FOR_CUTSCENE_TO_STOP()
	SET_PLAYER_PED_DATA_IN_CUTSCENES()	// B*1521921 - reset to default
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 1)		// "FRANKLIN"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3)		// "BEVERLY"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "RIVALPAP"	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)	
	DELETE_ALL_MISSION_BLIPS()	
	REMOVE_RELATIONSHIP_GROUP(relGroupEnemy)
	// REMOVE_RELATIONSHIP_GROUP(relGroupFriendly)	no longer do this inorder to fix B*511728	
	CLEANUP_ALL_MISSION_ENTITIES(bDeleteAll)	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sBeverlyVehicle.model, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sCelebVehicle.model, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(sRivalPapVehicle.model, FALSE)	
	UNLOAD_ALL_MISSION_ASSETS(bClearTextSlots)	
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("act_cinema")	// B* 1546752 - Restart cinema world brain.
	 
	#IF IS_DEBUG_BUILD
		CLEANUP_MISSION_WIDGETS()
		SET_DEBUG_ACTIVE(FALSE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF	
	CPRINTLN(DEBUG_MISSION, "MISSION_CLEANUP - done")
ENDPROC

/// PURPOSE:
///    Handles call to MISSION_CLEANUP and terminates the thread
PROC Script_Cleanup()
	
	// Ensure launcher is terminated
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Script was triggered so additional cleanup required") PRINTNL()
		MISSION_CLEANUP()
	ENDIF

	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal)	
	TERMINATE_THIS_THREAD()	
ENDPROC

/// PURPOSE:
///    Mission Passed
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Script_Passed()")
	SAFE_FADE_SCREEN_IN_FROM_BLACK()
	ADD_CONTACT_TO_PHONEBOOK(CHAR_BEVERLY, FRANKLIN_BOOK)
//	CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_UNLOGGED_SMALL_ACTION, 500, FALSE, TRUE) // Initially requested on B*1237043 - Removed on request as B*1317037
	Random_Character_Passed(CP_RAND_C_PAP1)
	Script_Cleanup()	
ENDPROC

/// PURPOSE:
///    updates the reason for mission failed
///    in order of most important fail reason so if multiple fails conditions have been set, we use the most important
PROC UPDATE_FAIL_REASON()
	IF DOES_ENTITY_EXIST(sBeverlyPed.ped)
		IF IS_ENTITY_DEAD(sBeverlyPed.ped)
		OR IS_PED_INJURED(sBeverlyPed.ped)
			CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_BEVERLY_DIED")
			eP1_MissionFailedReason = FAILED_BEVERLY_DIED
			EXIT
		ELSE			
			//if we have already failed for spooking Beverly, don't do the check again
			IF eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
				EXIT	
			ENDIF
			IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())	// unfortunately have to do this otherwise falling off the bike clases as damaging Beverly
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped)
					CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_BEVERLY_SPOOKED - PLAYER DAMAGED BEV")
					eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
					EXIT
				ENDIF
			ENDIF
			// don't allow the mission to fail if the player is shooting whilst Beverly is on the bike with him
			IF ePlayerTransportState != PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
				UPDATE_PEDS_CAN_SEE_PLAYER_FRAME_COUNTER(sBeverlyPed.ped, iFrameCountBeverlyLastSeenPlayer)
				IF IS_PLAYER_INTIMIDATE_SPECIFIC_PED(sBeverlyPed.ped, iFrameCountBeverlyLastSeenPlayer, GET_ENTITY_COORDS(PLAYER_PED_ID()), FALSE, FALSE)
					CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_BEVERLY_SPOOKED")
					eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	//if we have already failed for killing the celeb, don't do the check again
	IF eP1_MissionFailedReason = FAILED_CELEB_DIED
		EXIT	
	ENDIF	
	//if the player has been damaging Miranda's vehicle, when it becomes undriveable, blow it up to speed up the mission failed.
	//if the player is shooting at it, fail for attacking miranda
	IF DOES_ENTITY_EXIST(sCelebVehicle.vehicle)
		IF NOT IS_VEHICLE_DRIVEABLE(sCelebVehicle.vehicle)			
			EXPLODE_VEHICLE(sCelebVehicle.vehicle, TRUE)
			IF IS_PED_UNINJURED(sCelebPed.ped)
				EXPLODE_PED_HEAD(sCelebPed.ped)
			ENDIF
			IF IS_PED_UNINJURED(sCelebsDriverPed.ped)
				EXPLODE_PED_HEAD(sCelebsDriverPed.ped)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_CELEB_DIED")
			eP1_MissionFailedReason = FAILED_CELEB_DIED
			EXIT
		ELSE
			// Suggestion from Dave not to fail whilst rival pap is close by
			IF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
			or eMissionStage = MISSION_STAGE_DROP_BEVERLY_OFF
				BOOL bAttackedCeleb = FALSE
				BOOL bBlockShootLines = FALSE
				// test for damaging celeb vehicle
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sCelebVehicle.vehicle, PLAYER_PED_ID(), FALSE)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sCelebVehicle.vehicle, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
						CLEAR_ENTITY_LAST_WEAPON_DAMAGE(sCelebVehicle.vehicle)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sCelebVehicle.vehicle) // reset the damage check
						bAttackedCeleb = TRUE
						CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - Franklin damaged celeb vehicle this frame FC :", GET_FRAME_COUNT())
					ENDIF
				ENDIF
				//B* 1355878 - test for projectile being thrown by player
				IF IS_PED_SHOOTING(PLAYER_PED_ID())	// less expensive check
					VECTOR vLocal_CelebCoords = GET_ENTITY_COORDS(sCelebVehicle.vehicle)
					IF IS_PROJECTILE_IN_AREA((vLocal_CelebCoords - << 5.0, 5.0, 5.0 >>), (vLocal_CelebCoords + << 5.0, 5.0, 5.0 >>), TRUE)
						bAttackedCeleb = TRUE
						bBlockShootLines = TRUE
						CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - Franklin player fired projectile at celeb vehicle this frame FC :", GET_FRAME_COUNT())
					ENDIF
				ENDIF
				IF bAttackedCeleb
					IF NOT bDoneDialogue_FailWarningAttackMiranda
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						// B*1312880 - don't display subtitles if objective is on screen
						// B*1509531 - block shoot reference lines for projectiles
						IF bBlockShootLines
							IF PAP1_PLAY_SINGLE_LINE_FROM_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PSW", "PAP1_PSW_01", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Hey, what the fuck are you doing?
								bDoneDialogue_FailWarningAttackMiranda = TRUE
								bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	//reset this so he doesn't respond after this comment
								eBeverlyTakePhotoState = PAP1_BTPS_SETUP	// reset Beverly taking photos state
							ENDIF
						ELSE
							// random line
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_PSW", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								// Hey, what the fuck are you doing?
								// That's not what I meant by shooting!  
								// Are you trying to get the cops on us?
								// Who the hell are you shooting at?  
								bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	//reset this so he doesn't respond after this comment
								bDoneDialogue_FailWarningAttackMiranda = TRUE
								eBeverlyTakePhotoState = PAP1_BTPS_SETUP	// reset Beverly taking photos state
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_PSW")
							CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_PLAYER_ATTACKED_CELEB VEHICLE")
							eP1_MissionFailedReason = FAILED_PLAYER_ATTACKED_CELEB
							EXIT
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	//check for player leaving Beverly behind
	IF bHasBeverlyBeenRemovedFromPlayerGroup
		IF IS_PED_UNINJURED(sBeverlyPed.ped)				
			IF fDistance_PlayerToBeverly > PLAYER_LEFT_BEVERLY_DISTANCE
				CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_LEFT_BEVERLY_BEHIND")
				eP1_MissionFailedReason = FAILED_LEFT_BEVERLY_BEHIND
				EXIT
			ENDIF
		ENDIF
	ENDIF	
	IF (eMissionStage > MISSION_STAGE_INTRO_MOCAP_SCENE)
		IF (eMissionStage <> MISSION_STAGE_END_CUTSCENE)	
			IF DOES_ENTITY_EXIST(sBeverlyVehicle.vehicle)
				IF IS_ENTITY_DEAD(sBeverlyVehicle.vehicle)
				OR IS_ENTITY_IN_WATER(sBeverlyVehicle.vehicle)
				OR NOT IS_VEHICLE_DRIVEABLE(sBeverlyVehicle.vehicle)
					CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_BEVERLY_VEHICLE_WRECKED")
					eP1_MissionFailedReason = FAILED_BEVERLY_VEHICLE_WRECKED
					EXIT
				ENDIF
				
				IF IS_VEHICLE_TYRE_BURST(sBeverlyVehicle.vehicle, SC_WHEEL_BIKE_FRONT)
					IF IS_VEHICLE_TYRE_BURST(sBeverlyVehicle.vehicle, SC_WHEEL_BIKE_REAR)
						CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_BEVERLY_VEHICLE_WRECKED - wheels burst")
						eP1_MissionFailedReason = FAILED_BEVERLY_VEHICLE_WRECKED
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	//if one of these fails has already been set, don't worry about retesting it
	IF eP1_MissionFailedReason = FAILED_CELEB_ESCAPED
	OR eP1_MissionFailedReason = FAILED_PAP_ESCAPED
		EXIT	
	ENDIF	
	IF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
	OR eMissionStage = MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
	OR eMissionStage = MISSION_STAGE_STOP_RIVAL_PAP	
		IF NOT bDoneObjective_StopTheRivalPaparazzi	// fail for losing celeb, up to "stop rival pap objective, then switch to losing rival pap fail			
			IF IS_PED_UNINJURED(sCelebPed.ped)
				IF fCurrentChaseDistanceCeleb > CELEB_ESCAPED_DIST
				//AND NOT IS_ENTITY_ON_SCREEN(sCelebPed.ped)
					CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_CELEB_ESCAPED")
					eP1_MissionFailedReason = FAILED_CELEB_ESCAPED
					EXIT
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_UNINJURED(sRivalPapPed.ped)
				IF IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapPed.ped)
					IF fDistance_PlayerToRivalPap > RIVAL_PAP_ESCAPED_DIST
					//AND NOT IS_ENTITY_OCCLUDED(sRivalPapPed.ped)
						CPRINTLN(DEBUG_MISSION, "Mission Failed Checks - FAILED_PAP_ESCAPED")
						eP1_MissionFailedReason = FAILED_PAP_ESCAPED
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    cycles through the conditions to see if the player has failed
PROC MISSION_FAILED_CHECKS()
	// don't allow mission failed checks during stage skips
	IF bFinishedStageSkipping
		IF eMissionStage != MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE		// don't do the checks if we are already in the waiting for fade during fail stage
		AND eMissionStage != MISSION_STAGE_INTRO_MOCAP_SCENE
		AND eMissionStage != MISSION_STAGE_END_CUTSCENE
		AND eMissionStage != MISSION_STAGE_MISSION_PASSED
			UPDATE_FAIL_REASON()		
			IF eP1_MissionFailedReason <> FAILED_DEFAULT
				SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
			ENDIF		
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    handles everything which needs to happen after END_REPLAY_SETUP() has been called
/// PARAMS:
///    eCurrentStage - which mission stage we are returning to
PROC REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(MISSION_STAGE eReturnToStage)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH eReturnToStage
			CASE MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
				// B*1399624 - When failing the car chase in Paparazzo 1 Beverly spawns on the street instead of standing next to the motorbike
				IF IS_PED_UNINJURED(sBeverlyPed.ped)
					SET_ENTITY_COORDS_NO_OFFSET(sBeverlyPed.ped, sBeverlyPed.vSpawn)
					SET_ENTITY_HEADING(sBeverlyPed.ped, sBeverlyPed.fSpawnHeading)
					//CLEAR_PED_TASKS_IMMEDIATELY(sBeverlyPed.ped)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)	
					SET_BEVERLY_PLAY_COME_ON_ANIM("waive_comeback_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.589, TRUE)	// exact frame the mocap exit state ends with - real frame is 0.593							
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sBeverlyPed.ped)	
					CPRINTLN(DEBUG_MISSION, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : Bev respotted for skip FC = ", GET_FRAME_COUNT())
				ENDIF				
			BREAK
			CASE MISSION_STAGE_STOP_RIVAL_PAP
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, << -1736.5334, -508.0815, 37.9800 >>, 192.10)	//Correct pos for vehicle recording
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_STOP_RIVAL_PAP ", "set player on bike for replay ****** ", " FC : ", GET_FRAME_COUNT())
					ENDIF
					// make sure Bev is playing anim ready
					IF IS_PED_UNINJURED(sBeverlyPed.ped)
						IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
							SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
						ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	// keep these slow bend out for hit timings
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sBeverlyPed.ped)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_STOP_RIVAL_PAP ", "set Bev to idle on bike pose") ENDIF #ENDIF
						ENDIF
					ENDIF
					// Added because info from Matt, uber recording isn't updated each frame in the skip because of waits so ambient traffic could appear.
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	
					WAIT(0)	// to help with Bev bike snap on fade up makes sure player is on the bike before playback kicks in
				ENDIF
				
				fMainPlaybackSpeed = 1.0 	// Set the main play back speed to 1.0 for the skip
				fCurrentChaseDistanceCeleb = 10 //reset to stop failed for escape triggering
				fDistance_PlayerToRivalPap = 10
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)		
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)
						SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
						//FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sCelebVehicle.vehicle)
					ELSE
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_STOP_RIVAL_PAP : unpaused celeb vehicle playback for skip FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, UBER_PLAYBACK_TIME_STOP_RIVAL_PAP_STAGE)			
					SET_UBER_PLAYBACK_TO_TIME_NOW(sCelebVehicle.vehicle, UBER_PLAYBACK_TIME_STOP_RIVAL_PAP_STAGE)				
					bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
					PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()						
					CREATE_ALL_WAITING_UBER_CARS()  //might help with some traffic failing to spawn?					
					UPDATE_UBER_PLAYBACK(sCelebVehicle.vehicle, fMainPlaybackSpeed)
				ENDIF				
				IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					PAP1_SET_CAR_AT_PLAYBACK_POSITION(sRivalPapVehicle.vehicle, 1, "Pap1RP", (UBER_PLAYBACK_TIME_STOP_RIVAL_PAP_STAGE - UBER_PLAYBACK_TIME_FOR_TRIGGER_RIVAL_PAP_RECORDING),
																				TRUE, FALSE, TRUE)						
					SET_PLAYBACK_SPEED(sRivalPapVehicle.vehicle, fMainPlaybackSpeed)
					eRivalPapOnBikeProgress = PAP1_RIVAL_PAP_ON_BIKE_CHASING_AFTER_MIRANDA
					iRivalTakesPhotosBurst = 4	// skip along the rival pap taking photos progress
				ENDIF
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					SET_ENTITY_QUATERNION(sBeverlyVehicle.vehicle, 0.2702, 0.0882, 0.8814, -0.3772)					//Correct quaternion for vehicle recording
					PAP1_SET_CAR_AT_PLAYBACK_POSITION(sBeverlyVehicle.vehicle, 1, "Pap1PV", 250, TRUE, FALSE, TRUE)						
					SET_PLAYBACK_SPEED(sRivalPapVehicle.vehicle, fMainPlaybackSpeed)
					// Added because info from Matt, uber recording isn't updated each frame in the skip because of waits so ambient traffic could appear.
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)					
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200, FALSE)
					
					// Added because info from Matt, uber recording isn't updated each frame in the skip because of waits so ambient traffic could appear.
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)		
				ENDIF
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_STOP_RIVAL_PAP : done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			BREAK
			CASE MISSION_STAGE_DROP_BEVERLY_OFF
				SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, <<-1586.6477, -533.9604, 34.4623>>, 307.1855)
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					SET_VEHICLE_ON_GROUND_PROPERLY(sBeverlyVehicle.vehicle)
				ENDIF
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_DROP_BEVERLY_OFF ", "set player on bike for replay ****** ", " FC : ", GET_FRAME_COUNT())
					ENDIF
					// make sure Bev is playing anim ready
					IF IS_PED_UNINJURED(sBeverlyPed.ped)
						IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
							SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
						ENDIF
						IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")
							TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sBeverlyPed.ped)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : MISSION_STAGE_STOP_RIVAL_PAP ", "set Bev to idle on bike pose") ENDIF #ENDIF
						ENDIF
					ENDIF
				ENDIF				
				CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 35.0, TRUE)	// stop cars crashing straight into player when fade up
				WAIT(0)	// to help with Bev bike snap on fade up makes sure player is on the bike before playback kicks in
				fCurrentChaseDistanceCeleb = 10 //reset to stop failed for escape triggering
				fDistance_PlayerToRivalPap = 10	
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY : MISSION_STAGE_DROP_BEVERLY_OFF : done FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			BREAK
			CASE MISSION_STAGE_END_CUTSCENE
				SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, vDropOffPoint, 221.0758)
				RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : ", "RC_REQUEST_MID_MISSION_CUTSCENE : PAP_1_MCS_1 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
				
				WHILE NOT RC_IS_CUTSCENE_OK_TO_START(TRUE)
					RC_REQUEST_MID_MISSION_CUTSCENE(tlOutroMocapName)		// can't use RC_REQUEST_CUTSCENE in this instance, as it causes bug 1000899
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : ", "waiting on HAS_CUTSCENE_LOADED : PAP_1_MCS_1 ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					
					SET_MISSION_ENTITIES_COMP_VARIATIONS_FOR_OUTRO_MOCAP()
					WAIT(0)
				ENDWHILE
			BREAK
		ENDSWITCH
		IF eReturnToStage != MISSION_STAGE_INTRO_MOCAP_SCENE
		AND eReturnToStage != MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				SET_BEVERLY_FOLLOW_PLAYER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    skip the current stage in the mission flow
PROC SKIP_STAGE()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		KILL_ANY_CONVERSATION()
		CLEAR_PRINTS()
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF	
		SWITCH eMissionStage
			CASE MISSION_STAGE_INTRO_MOCAP_SCENE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
					ENDIF
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_INTRO_MOCAP_SCENE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF	
			BREAK
			CASE MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)						
						// if he hasn't got his helmet yet force it here, then config flags handles him not removing it till mission passed
						IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())						
							GIVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
							CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : ", "player helmet set ", " FC : ", GET_FRAME_COUNT())
						ENDIF
						IF IS_PED_UNINJURED(sBeverlyPed.ped)
							IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						SET_VEHICLE_ENGINE_ON(sBeverlyVehicle.vehicle, TRUE, TRUE)	// make sure the engine is running
					ENDIF
					 bHasPlayerGotOnBike_FirstTime = TRUE	// set the flag as it's used by the chase rubberbanding
					fCurrentChaseDistanceCeleb = 10 //reset to stop failed for escape triggering
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)	
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : ", "set player on bike NONE replay ****** ", " FC : ", GET_FRAME_COUNT())
							ENDIF
						ENDIF
						SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, << -1434.5149, -67.2538, 51.6780 >>, 130.5866)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						LOAD_ASSETS_FOR_RIVAL_PAP_ARRIVING()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_RIVAL_PAP_JOIN_CHASE	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					// do this after the wait for world to load
					IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
							START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)					
						ENDIF
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, (UBER_PLAYBACK_TIME_RIVAL_PAP_JOIN_CHASE_STAGE))
						SET_UBER_PLAYBACK_TO_TIME_NOW(sCelebVehicle.vehicle, UBER_PLAYBACK_TIME_RIVAL_PAP_JOIN_CHASE_STAGE)
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
						PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()						
						CREATE_ALL_WAITING_UBER_CARS()  //might help with some traffic failing to spawn?					
						UPDATE_UBER_PLAYBACK(sCelebVehicle.vehicle, fMainPlaybackSpeed)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : ", "celeb vehicle paused", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					
					// Added because info from Matt, uber recording isn't updated each frame in the skip because of waits so ambient traffic could appear.
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)					
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200, FALSE)
					
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_CHASE_CELEB_IN_VEHICLE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
				IF eSubStage = SS_UPDATE								
					IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)						
						IF IS_PED_UNINJURED(sBeverlyPed.ped)
							IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
							ENDIF
							IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
							ENDIF
						ENDIF						
					ENDIF
					IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
							PAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", "celeb vehicle pause playback for skip", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF			
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
							SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", "rival vehicle skip to end and stopped playback for skip", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF					
					//Special recording for the player vehicle and load scene just for skipping to stop rival pap stage
					IF eMissionSkipTargetStage = MISSION_STAGE_STOP_RIVAL_PAP				
						REQUEST_VEHICLE_RECORDING(1, "Pap1PV")	
						WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Pap1PV")
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", "loading vehicle rec for player to start stop rival pap stage", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							WAIT(0)
						ENDWHILE
					ENDIF
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", "set player on bike NONE replay ****** ", " FC : ", GET_FRAME_COUNT())
							ENDIF							
						ENDIF
						// moves player on bike to correct spot for load scene
						SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, << -1736.5334, -508.0815, 37.9800 >>, 192.10)	//Correct pos for vehicle recording
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_STOP_RIVAL_PAP	
								WAIT_FOR_WORLD_TO_LOAD(<<-1727.6035, -524.8994, 36.6081>>, 75.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
								bLoadedWorldForStageSkipping = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF
					bDoneDialogue_RivalPapTookPhoto[0] = TRUE	//skip the rival pap spotted dialogue
					bDoneDialogue_RivalPapTookPhoto[1] = TRUE	//skip the rival pap spotted dialogue
					iRivalTakesPhotosBurst = 4	//set to stop all flashing
					iNumFlashesForRivalCamera = 0	//set to stop all flashing
					fMainPlaybackSpeed = 1.0 	// Set the main play back speed to 1.0 for the skip
					fCurrentChaseDistanceCeleb = 10 //reset to stop failed for escape triggering
					fDistance_PlayerToRivalPap = 10
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_RIVAL_PAP_JOIN_CHASE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_STOP_RIVAL_PAP
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run
					IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
						IF IS_PED_UNINJURED(sBeverlyPed.ped)
							IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sBeverlyVehicle.vehicle)
							STOP_PLAYBACK_RECORDED_VEHICLE(sBeverlyVehicle.vehicle)
							REMOVE_VEHICLE_RECORDING(1, "Pap1PV")
						ENDIF
						WAIT(0)	//allow chance for vehicle recording to stop on player's bike
					ENDIF
						
					// stop the celebs recording (so it kills the uber recording)
					IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
							STOP_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
						ENDIF
					ENDIF	
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
							STOP_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
						ENDIF
					ENDIF	
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STOP_RIVAL_PAP : ", "set player on bike NONE replay ****** ", " FC : ", GET_FRAME_COUNT())
							ENDIF							
						ENDIF
						// moves player on bike to correct spot for load scene
						SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, <<-1586.6477, -533.9604, 34.4623>>, 307.1855)
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
							SET_VEHICLE_ON_GROUND_PROPERLY(sBeverlyVehicle.vehicle)
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_DROP_BEVERLY_OFF	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STOP_RIVAL_PAP : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
					SAFE_TELEPORT_ENTITY(sRivalPapVehicle.vehicle, << -1594.5171, -537.8604, 34.2018 >>, 307.7577)	
					IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
					AND IS_PED_UNINJURED(sRivalPapPed.ped)
						CLEAR_PED_TASKS(sRivalPapPed.ped)
						CLEAR_PED_TASKS(sRivalPapDriverPed.ped)	
						IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
							SET_VEHICLE_ON_GROUND_PROPERLY(sRivalPapVehicle.vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
							ENDIF
							IF NOT IS_PED_SITTING_IN_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle, VS_FRONT_RIGHT)
							ENDIF
							WHILE NOT IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
							OR NOT IS_PED_SITTING_IN_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STOP_RIVAL_PAP : ", "waiting for rival and driver to be on the bike", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								WAIT(0)
							ENDWHILE
						ENDIF
					ENDIF
					IF IS_PED_UNINJURED(sRivalPapPed.ped)
						STOP_PED_SPEAKING(sRivalPapPed.ped, TRUE)			// B*1493561 - hide grunts
						DISABLE_PED_PAIN_AUDIO(sRivalPapPed.ped, TRUE)	// B*1493561 - hide grunts
						IF IS_PED_IN_ANY_VEHICLE(sRivalPapPed.ped)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)																							
								APPLY_FORCE_TO_ENTITY(sRivalPapPed.ped, APPLY_TYPE_IMPULSE, << 2.0, 0.0, -4.0 >>, <<0.0,0.21,0.0>>,
														GET_PED_RAGDOLL_BONE_INDEX(sRivalPapPed.ped, RAGDOLL_HEAD), TRUE, TRUE, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP : MISSION_STAGE_DROP_BEVERLY_OFF : Rival paps  knocked off bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP : MISSION_STAGE_DROP_BEVERLY_OFF : Rival pap  NOT knocked off bike  CAN_KNOCK_PED_OFF_VEHICLE was FALSE FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
							SET_ENTITY_HEALTH(sRivalPapPed.ped, 0)
						ENDIF						
					ENDIF
					IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
						STOP_PED_SPEAKING(sRivalPapDriverPed.ped, TRUE)			// B*1493561 - hide grunts
						DISABLE_PED_PAIN_AUDIO(sRivalPapDriverPed.ped, TRUE)	// B*1493561 - hide grunts
						IF IS_PED_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapDriverPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapDriverPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapDriverPed.ped)																							
								APPLY_FORCE_TO_ENTITY(sRivalPapDriverPed.ped, APPLY_TYPE_IMPULSE, << 2.0, 0.0, -4.0 >>, <<0.0,0.21,0.0>>,
														GET_PED_RAGDOLL_BONE_INDEX(sRivalPapDriverPed.ped, RAGDOLL_HEAD), TRUE, TRUE, TRUE)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP : MISSION_STAGE_DROP_BEVERLY_OFF : Rival pap Driver knocked off bike FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP : MISSION_STAGE_DROP_BEVERLY_OFF : Rival pap Driver NOT knocked off bike  CAN_KNOCK_PED_OFF_VEHICLE was FALSE FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
							ENDIF
						ENDIF
						SET_ENTITY_HEALTH(sRivalPapDriverPed.ped, 0)
					ENDIF
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
						APPLY_FORCE_TO_ENTITY(sRivalPapVehicle.vehicle, APPLY_TYPE_IMPULSE, << 2.0, 1.0, -4.0 >>, <<0.0,0.21,0.0>>, 0, TRUE, TRUE, TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_STOP_RIVAL_PAP : MISSION_STAGE_DROP_BEVERLY_OFF : Rival pap Vehicle impulse force applied FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					WAIT(750)	// Needed for B*1493561  - allow some time for the peds to fall off the bike		
					fCurrentChaseDistanceCeleb = 10 //reset to stop failed for escape triggering
					fDistance_PlayerToRivalPap = 10
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_STOP_RIVAL_PAP : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK		
			CASE MISSION_STAGE_DROP_BEVERLY_OFF
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
						IF IS_PED_UNINJURED(sBeverlyPed.ped)
							IF NOT IS_PED_SITTING_IN_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
					ENDIF					
					IF NOT IS_REPLAY_BEING_SET_UP()
						IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
							IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
								SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)				
								CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : ", "set player on bike NONE replay ****** ", " FC : ", GET_FRAME_COUNT())
							ENDIF
							SAFE_TELEPORT_ENTITY(sBeverlyVehicle.vehicle, vDropOffPoint, 221.0758)
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						// load world if target stage is the next one				
						IF bLoadedWorldForStageSkipping = FALSE
							IF eMissionSkipTargetStage = MISSION_STAGE_END_CUTSCENE	
								IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
									WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
									bLoadedWorldForStageSkipping = TRUE
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : ", "LOADED WORLD", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					bDoneDialogue_RivalPapCameraDestroyed = TRUE
					bDoneWaitForCameraDestroyedDialogue = TRUE
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_DROP_BEVERLY_OFF : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK
			CASE MISSION_STAGE_END_CUTSCENE
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					IF IS_CUTSCENE_PLAYING()
						STOP_CUTSCENE()
					ENDIF	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_END_CUTSCENE : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK	
			CASE MISSION_STAGE_LOSE_THE_COPS
				IF eSubStage = SS_UPDATE	//in script skips where we change the eSubStage, need to safe gaurd that the SS_SETUP has already had chance to run.
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					eSubStage = SS_CLEANUP
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SKIP_STAGE : ", "MISSION_STAGE_LOSE_THE_COPS : ", " FC : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			BREAK			
			DEFAULT
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Jumps to the stage selected
/// PARAMS:
///    eNewStage - stage to jump to
PROC JUMP_TO_STAGE(MISSION_STAGE eNewStage)    
    IF eMissionStage = eNewStage	// skip current stage
		IF IS_REPLAY_BEING_SET_UP()
			END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)	// player set onto bike handled by REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY			
		ENDIF
		REPLAY_SKIP_SETUP_UPON_RETURN_TO_GAMEPLAY(eMissionStage)	// specific setup which must come after END_REPLAY_SETUP
		SET_MUSIC_EVENTS_FOR_MISSION_STAGE_SKIPS(eMissionStage)
		// B*1481353 - don't fade back in here when skipping to mocap (let the mocap stage handle it)
		IF eMissionStage = MISSION_STAGE_END_CUTSCENE
			RC_END_Z_SKIP(DEFAULT, FALSE)
		// don't reset the gameplay cams if we are coming out of the mocap
		ELIF eMissionStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
			RC_END_Z_SKIP(FALSE)
		ELSE
			RC_END_Z_SKIP()
		ENDIF
    	bFinishedStageSkipping = TRUE
		bLoadedWorldForStageSkipping = FALSE
		// ensure we are fully faded in if we have skipped to the mission passed stage, since the mission passed GUI doesn't display if not (seems to need a frame wait too)
		IF eMissionStage = MISSION_STAGE_MISSION_PASSED
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		ENDIF
    ELSE
        SKIP_STAGE() 
    ENDIF	
ENDPROC

/// PURPOSE: 
///     Reset the mission, cleanups the current state and set's the mission up again
///     //USED by the mission replay checkpoint setup and Debug skips
PROC RESET_MISSION()
	IF NOT IS_REPLAY_BEING_SET_UP()
		SAFE_TELEPORT_PED_OUT_OF_THEIR_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
	AND bFinishedStageSkipping
		SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_FAIL)
	ENDIF
	MISSION_CLEANUP(TRUE, FALSE)
	
	#IF IS_DEBUG_BUILD	// should really only get called in debug build	
		//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_1(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " RESET_MISSION - waiting on SetupScene_PAPARAZZO_1   ~~~~~")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_PAPARAZZO_1, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
	#ENDIF
	//re do mission initialization
	INIT_MISSION()	
	SET_STAGE(MISSION_STAGE_INTRO_MOCAP_SCENE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RESET_MISSION - done") ENDIF #ENDIF
ENDPROC

/// PURPOSE:
///    Perform a Z skip.  Used by the mission checkpoints and the debug Z skip function
/// PARAMS:
///    iNewStage - Mission stage we want to skip to
///    bResetMission - used when we go backwards in mission flow.  If false we also don't stop the active cutscene in RC_START_Z_SKIP, instead handled in SKIP_STAGE to fix bug 1006740
PROC DO_Z_SKIP(INT iNewStage, BOOL bResetMission = FALSE)
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP with parameters  - iNewStage = ", iNewStage, " bResetMission = ", bResetMission) ENDIF #ENDIF
	RC_START_Z_SKIP(bResetMission, FALSE)	// ensure z skip doesn't remove player's helmet!
	IF bResetMission
		RESET_MISSION()
	ENDIF
	eMissionSkipTargetStage = INT_TO_ENUM(MISSION_STAGE, iNewStage)
	bFinishedStageSkipping = FALSE
	IF IS_REPLAY_BEING_SET_UP()
		bLoadedWorldForStageSkipping = TRUE
	ELSE
		bLoadedWorldForStageSkipping = FALSE
	ENDIF
	// load world for the mission start area if we are resetting the mission to the intro mocap.  Moved here from script skip stage to fix bug 1006740 - mocap exit states not getting set as game is waiting on world to load before getting to check				
	// basically if you press CROSS to confirm which stage in the z menu, it skipped the mocap but couldn't sent exit states as it was waiting for world to load first.		
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF eMissionSkipTargetStage = MISSION_STAGE_INTRO_MOCAP_SCENE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-152.36, 279.07, 93.8134>>, -30.0)	// moved back to start area
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)		
				WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
				bLoadedWorldForStageSkipping = TRUE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP -  LOADED WORLD ready at mission start area framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
		ENDIF	
	ENDIF
	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DO_Z_SKIP") ENDIF #ENDIF
	JUMP_TO_STAGE(eMissionSkipTargetStage)
ENDPROC
	
//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG - J,P and Z skip stuff
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD	
	/// PURPOSE:
	///    check for debug S, F, P and J skips
	PROC DEBUG_Check_Debug_Keys()
	    INT iNewStage
	    // Check for Pass
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))			
			WAIT_FOR_CUTSCENE_TO_STOP()	
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				CLEAR_PED_TASKS(sBeverlyPed.ped)
			ENDIF
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_FAIL)
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "S SKIP") ENDIF #ENDIF
	        Script_Passed()
	    ENDIF
	    // Check for Fail
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))	
			WAIT_FOR_CUTSCENE_TO_STOP()		
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "F SKIP") ENDIF #ENDIF
		    SET_STAGE(MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE)
	    ENDIF	    
	    // Check for Skip forward
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_DROP_BEVERLY_OFF)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int now = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage) + 1
				BREAK
			ENDSWITCH	
			#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "J SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
			DO_Z_SKIP(iNewStage, FALSE)
	    ENDIF	    
	    // Check for Skip backwards
	    IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
	        SWITCH eMissionStage
				CASE MISSION_STAGE_LOSE_THE_COPS
					iNewStage = ENUM_TO_INT(MISSION_STAGE_DROP_BEVERLY_OFF)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PRE STAGE, FROM LOSE COPS int now = ", iNewStage) ENDIF #ENDIF
				BREAK
				DEFAULT 
					iNewStage = ENUM_TO_INT(eMissionStage)-1
				BREAK
			ENDSWITCH		
		    IF iNewStage > -1
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "P SKIP : ", "new stage = ", iNewStage) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
		    ENDIF
	    ENDIF	    
	    // Z skip menu
	    IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, iNewStage)
	        // if we are skipping forward in the mission stages, just J skip rather than a full mission reset
			IF (eMissionStage = MISSION_STAGE_LOSE_THE_COPS)	// additional stages dealt seperately since they sit at the end of the MISSION_STAGE enum
				IF iNewStage = ENUM_TO_INT(MISSION_STAGE_END_CUTSCENE)	// trying to z skip past lose cops so don't reset mission
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, FALSE)
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
					DO_Z_SKIP(iNewStage, TRUE)
				ENDIF
			ELIF (iNewStage <= ENUM_TO_INT(eMissionStage))
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", TRUE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Z SKIP : new stage = ", iNewStage, " ResetMission = ", FALSE) ENDIF #ENDIF
				DO_Z_SKIP(iNewStage, FALSE)
			ENDIF
	    ENDIF 
	ENDPROC
#ENDIF

/// PURPOSE:
///    setups up the scenario blocking area, and clear areas needed when the mocap starts
///    Also performs the player vehicle resolves for the intro
PROC SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()

	// set the road changes up before the resolve vehicle checks
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-190.01993, 260.10822, 85.0>>, <<-131.57199, 285.80911, 102.0>>)
	
	SET_ROADS_IN_ANGLED_AREA(<<-116.820381,261.006592,101.028191>>, <<-206.292038,266.383789,86.079262>>, 15.000000, TRUE, FALSE)	// disable road nodes in front of the chase start area (near side of the road)
	//SET_ROADS_IN_AREA(<< -227.98399, 209.28452, 75.96078 >>, << -210.06294, 244.48195, 99.00089 >>)	//across road on hill junction (done since i have an uber car coming up straight after mocap
	SET_ROADS_IN_AREA(<< -272.71304, 209.47295, 75.91507 >>, << -166.00862, 270.97842, 99.37162 >>, FALSE)	// major area covering the junction where I want the cars to move from
	
	CPRINTLN(DEBUG_MISSION, "DOES SNAPSHOT VEHICLE EXIST = ", DOES_ENTITY_EXIST(g_startSnapshot.mVehicleIndex))
	VECTOR vPlayerLastVehicleRespotPosition = << -139.92, 276.95, 94.10 >>
	FLOAT fPlayerLastVehicleRespotHeading = 178.24
	VECTOR vRespotPosSizeLimit = << 4.5, 11.5, 20.0 >>	// GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR()
	// Beverly position
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-153.917435,283.676300,91.763824>>, <<-147.120819,283.598938,96.901680>>, 7.0,
														vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, vRespotPosSizeLimit, TRUE, FALSE)	// setVehicleAsVehGen parameter should no longer be used - SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN to handle it
	// car park outside the resturant
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-180.003601,277.143250,91.162712>>, <<-147.960114,276.252960,96.102898>>, 9.0,
														vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, vRespotPosSizeLimit, TRUE, FALSE)	// setVehicleAsVehGen parameter should no longer be used - SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN to handle it
	// stretch position leading to first set of lights
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-204.188965,271.303528,89.886497>>, <<-170.883987,267.556366,95.117683>>, 12.0,
														vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, vRespotPosSizeLimit, TRUE, FALSE)	// setVehicleAsVehGen parameter should no longer be used - SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN to handle it
	
	VEHICLE_INDEX vehPlayerLast = GET_PLAYERS_LAST_VEHICLE()
	IF IS_VEHICLE_OK(vehPlayerLast)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerLast)	// B*1397345 - veh seen dropping into position during mocap
		CPRINTLN(DEBUG_MISSION, "SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - set player last veh on ground properly framecount : ", GET_FRAME_COUNT())
	ENDIF
	// set the vehicle the player triggered the mission in to regenerate
	IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(vRespotPosSizeLimit, FALSE)
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(vPlayerLastVehicleRespotPosition, fPlayerLastVehicleRespotHeading, FALSE)
		CPRINTLN(DEBUG_MISSION, "SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - vehicle gen setup framecount : ", GET_FRAME_COUNT())
	ELSE
		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP - vehicle to big to setup vehicle gen! framecount : ", GET_FRAME_COUNT()) ENDIF #ENDIF
	ENDIF
	// clear intro cutscene
	SET_PED_NON_CREATION_AREA(<< -191.42, 269.87, 91.10 >>, << -135.91, 296.20, 100.42 >>)
	scenarioBlockingIntroArea = ADD_SCENARIO_BLOCKING_AREA(<< -191.42, 269.87, 91.10 >>, << -135.91, 296.20, 100.42 >>)	
	CLEAR_AREA_OF_PEDS(<< -151.66, 285.51, 92.76 >>, 20.0)

	CLEAR_ANGLED_AREA_OF_VEHICLES(<<-116.820381,261.006592,101.028191>>, <<-206.292038,266.383789,86.079262>>, 15.000000, FALSE, FALSE, FALSE, TRUE, TRUE)	// near side of the main road
	CLEAR_AREA_OF_VEHICLES(<< -162.42995, 279.91364, 92.71819 >>, 26.0)	//carpark
	CLEAR_AREA_OF_VEHICLES(<< -221.89508, 225.77153, 87.77625 >>, 20.0, TRUE)	//across road on hill junction (done since i have an uber car coming up straight after mocap
	
	//larger area for skipping since we are faded down anyway
	IF NOT bFinishedStageSkipping
		CLEAR_AREA_OF_VEHICLES(<< -162.42995, 279.91364, 92.71819 >>, 200, TRUE)	
	ENDIF
	// try to make any lingering traffic flow out of the area
	objTrafficLightWestBound = GET_CLOSEST_OBJECT_OF_TYPE(<< -237.65, 274.52, 91.00 >>, 10.0, prop_traffic_01a)
	objTrafficLightEastBound = GET_CLOSEST_OBJECT_OF_TYPE(<< -205.82, 248.14, 91.01 >>, 10.0, prop_traffic_01a) 
	IF DOES_ENTITY_EXIST(objTrafficLightWestBound)
		SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightWestBound, TLO_GREEN)	// make the remaining traffic flow out the area
	ENDIF
	IF DOES_ENTITY_EXIST(objTrafficLightEastBound)
		SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightEastBound, TLO_GREEN)	// make the remaining traffic flow out the area
	ENDIF
ENDPROC

/// PURPOSE:
///    wait until Beverly's animation match the start pose for the intro mocap
/// RETURNS:
///    TRUE when anim phase matches or player gets really close proximity
FUNC BOOL IS_BEVERLY_READY_TO_START_INTRO_MOCAP()

	STRING sAnimDict_BeverlyIdles = "rcmpaparazzo1beckon"
	FLOAT fCurrentPhase
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0])
		IF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_01")
		
			//fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_01")
			//IF fCurrentPhase < 0.05
			//OR fCurrentPhase > 0.98
				CPRINTLN(DEBUG_MISSION, "IS_EVERLY_READY_TO_START_INTRO_MOCAP : return TRUE : Beverly reach suitable time in anim pap_idle_01 : framecount : ", GET_FRAME_COUNT())
				RETURN TRUE
			//ENDIF
			
		ELIF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_action_01")
		
			fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_action_01")
			IF fCurrentPhase < 0.198
			OR fCurrentPhase > 0.957
				CPRINTLN(DEBUG_MISSION, "IS_EVERLY_READY_TO_START_INTRO_MOCAP : return TRUE : Beverly reach suitable time in anim pap_idle_action_01 : framecount : ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
			
		ELIF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_02")
		
			//fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_02")
			//IF fCurrentPhase < 0.008
			//OR fCurrentPhase > 0.99
				CPRINTLN(DEBUG_MISSION, "IS_EVERLY_READY_TO_START_INTRO_MOCAP : return TRUE : Beverly reach suitable time in anim pap_idle_02 : framecount : ", GET_FRAME_COUNT())
				RETURN TRUE
			//ENDIF
		
		ELIF IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_action_02")
		
			fCurrentPhase = GET_ENTITY_ANIM_CURRENT_TIME(sRCLauncherDataLocal.pedID[0], sAnimDict_BeverlyIdles, "pap_idle_action_02")
			IF fCurrentPhase < 0.027
			OR fCurrentPhase > 0.942
				CPRINTLN(DEBUG_MISSION, "IS_EVERLY_READY_TO_START_INTRO_MOCAP : return TRUE : Beverly reach suitable time in anim pap_idle_action_02 : framecount : ", GET_FRAME_COUNT())
				RETURN TRUE
			ENDIF
			
		ELSE
			CPRINTLN(DEBUG_MISSION, "IS_EVERLY_READY_TO_START_INTRO_MOCAP : return TRUE : Beverly not playing any of the idle anims! : framecount : ", GET_FRAME_COUNT())
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    handles the focus push when arriving to trigger the cutscene
///    basically a gives player cam a hint to focus on Beverly and gets player walking over there
/// RETURNS:
///    TRUE if it's done
FUNC BOOL HAS_INTRO_FOCUS_PUSH_FINSHED()

	// setup the hint camera or just turn to face if close already
	IF iIntroFocusPushStage = 0	
		IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0], 6.5)	// don't trigger if we are close
				SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], (<<0, 0, 0>>), TRUE, -1, DEFAULT_INTERP_IN_TIME)															
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.37)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.7)	// over left shoulder	// -0.01) over right shoulder
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.1)	//-0.05)
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
	            SET_GAMEPLAY_HINT_FOV(35.0)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)				
				iIntroFocusPushTimer = GET_GAME_TIMER()
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0])
					CPRINTLN(DEBUG_MISSION, " HAS_INTRO_FOCUS_PUSH_FINSHED() : turn to face task only")
				ENDIF
				iIntroFocusPushTimer = -1
			ENDIF
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0], 4000)
		ENDIF
		iIntroFocusPushStage++
		CPRINTLN(DEBUG_MISSION, " HAS_INTRO_FOCUS_PUSH_FINSHED() : iIntroFocusPushStage++ =", iIntroFocusPushStage)
		
	// wait for the timer to finsh
	ELIF iIntroFocusPushStage = 1
		IF iIntroFocusPushTimer = -1
		OR (GET_GAME_TIMER() - iIntroFocusPushTimer) > (DEFAULT_INTERP_IN_TIME + 1000)	// hint needs to remain for a second (takes 3 seconds to interpolate)
			RETURN TRUE
		ENDIF		
	ENDIF
	
	SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)	// wants him to walk at this point
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if push in and wait for bev anims should be skipped
///    and cutscene should just trigger asap
/// RETURNS:
///    TRUE if Player gets too close to Bev or if replay /repeat play is being setup
FUNC BOOL SHOULD_INTRO_CUTSCENE_TRIGGER_IMMEDIATELY()

	// allow this to go through regardless if a repeat play is being setup
	IF IS_REPLAY_IN_PROGRESS()
	OR IS_REPEAT_PLAY_ACTIVE()
	OR IS_REPLAY_BEING_SET_UP()
	OR NOT bFinishedStageSkipping	
		CPRINTLN(DEBUG_MISSION, "HAS_INTRO_FOCUS_PUSH_FINSHED : return TRUE : repeat play / replay active : framecount : ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	// B*1426354 - area around Beverly where who'd expect the cutscene to kick in
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-150.362885,287.136047,91.135529>>, <<-150.419830,282.028625,97.263824>>, 6.500000)
		CPRINTLN(DEBUG_MISSION, "HAS_INTRO_FOCUS_PUSH_FINSHED : return TRUE : player reached cutscene trigger area : framecount : ", GET_FRAME_COUNT())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		Mission Stages
//-------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Franklin meets Beverly.  Beverly spots a celebrity Miranda leaving a resturant
///    Beverly ask's Franklin to help him get some shots of Miranda
PROC STAGE_INTRO_MOCAP_SCENE()

	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)		//to stop vehicles blocking parking space during mocap	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)	// reduce the vehicle density during the mocap
	
	SWITCH eSubStage	
		//	-------------------------------------------------------------------------------------
		CASE SS_SETUP			
			// don't request the mocap if we are skipping past this stage
			IF NOT bFinishedStageSkipping
				IF NOT IS_REPLAY_BEING_SET_UP()
					SAFE_TELEPORT_PED(PLAYER_PED_ID(), <<-152.3902, 279.5714, 92.8134>>, 106.6662 )	// match mocap finished position and heading
					// load world if target stage is the next stage				
					IF bLoadedWorldForStageSkipping = FALSE
						IF eMissionSkipTargetStage = MISSION_STAGE_CHASE_CELEB_IN_VEHICLE	
							IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
								WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0, FLAG_COLLISIONS_MOVER | FLAG_MAPDATA)	
								bLoadedWorldForStageSkipping = TRUE
								CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE : SS_SETUP - bFinishedStageSkipping - LOADED WORLD ready for next stage after mocap framecount : ", GET_FRAME_COUNT())
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				// Remove temporary RC character BEVERLY and his camera
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
				SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
				
				SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_INTRO")
					START_AUDIO_SCENE("PAPARAZZO_01_INTRO")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP set audio scene active - ", "PAPARAZZO_01_INTRO") ENDIF #ENDIF
				ENDIF
				LOAD_MISSION_ASSETS(TRUE)
				CREATE_BEVERLYS_BIKE_FOR_MISSION_START()	// have to do this here, since it can be seen during the mocap cutscene otherwise	
				CREATE_ENTITIES_FOR_MISSION_START()
				IF NOT IS_REPLAY_BEING_SET_UP()
					IF IS_PED_UNINJURED(sBeverlyPed.ped)	
						//CLEAR_PED_TASKS_IMMEDIATELY(sBeverlyPed.ped)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)	
						SET_BEVERLY_PLAY_COME_ON_ANIM("waive_comeback_f", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.589, TRUE)	// exact frame the mocap exit state ends with - real frame is 0.593							
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sBeverlyPed.ped)	
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE FOR SKIP - sBeverlyPed.ped - set skipped mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				// match cutscene exit state
				IF NOT IS_REPLAY_BEING_SET_UP()
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 3000)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE FOR SKIP simulate player gait NON replay *** FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped, -1)
				
				IF NOT IS_REPLAY_BEING_SET_UP()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(11.7337)	// match end of mocap
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7.2753)	
				ENDIF
				CPRINTLN(DEBUG_MISSION, "INTRO MOCAP SETUP DONE FOR SKIP - GOING TO SS_CLEANUP")
				eSubStage = SS_CLEANUP
			ELSE	
				// handled by player_ped_public.sch
				/*IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_Franklin, PLAYER_PED_ID())						
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED player set this frame : ", GET_FRAME_COUNT())
					ELSE
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED player failed this frame : ", GET_FRAME_COUNT())
					ENDIF
				ENDIF*/
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])	// beverly launcher ped
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandle_Beverly, sRCLauncherDataLocal.pedID[0])
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED bev") ENDIF #ENDIF
					ENDIF
				ENDIF
				
				RC_PLAYER_TRIGGER_SCENE_LOCK_IN()	// need to prevent player interfering with Beverly whilst he's getting ready for the mocap to trigger
				
				IF HAS_INTRO_FOCUS_PUSH_FINSHED()
				AND IS_BEVERLY_READY_TO_START_INTRO_MOCAP()
				OR SHOULD_INTRO_CUTSCENE_TRIGGER_IMMEDIATELY()				

					RC_REQUEST_CUTSCENE("PAP_1_RCM")	// unable to do this prior to IS_BEVERLY_READY_TO_START_INTRO_MOCAP because it takes away player control	
					
					IF RC_IS_CUTSCENE_OK_TO_START(TRUE)
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Franklin, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(sBeverlyPed.ped, sSceneHandle_Beverly, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sBeverlyPed.model)
						REGISTER_ENTITY_FOR_CUTSCENE(sObjCameraBeverly.object, sSceneHandle_BeverlysCamera, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sObjCameraBeverly.model)	
						REGISTER_ENTITY_FOR_CUTSCENE(sObjFlashUnitBeverly.object, sSceneHandle_BeverlysCameraFlash, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sObjFlashUnitBeverly.model)	
						REGISTER_ENTITY_FOR_CUTSCENE(sCelebVehicle.vehicle, sSceneHandle_MirandasVehicle, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sCelebVehicle.model)		
						REGISTER_ENTITY_FOR_CUTSCENE(sCelebPed.ped, sSceneHandle_Miranda, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sCelebPed.model)		
						REGISTER_ENTITY_FOR_CUTSCENE(sCelebsDriverPed.ped, sSceneHandle_MirandasDriver, CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, sCelebsDriverPed.model)		
						SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// backup fade in, mainly for repeat play purposes.
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOW)
						
						START_CUTSCENE()				
						WAIT(0)	//needed because cutscene doesn't start straight away, causing player to see vehicles getting removed.
						
						IF IS_GAMEPLAY_HINT_ACTIVE()
							STOP_GAMEPLAY_HINT()
						ENDIF
					
						// Remove temporary RC character BEVERLY and his camera
						SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[0])
						SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[0])
						
						RC_CLEANUP_LAUNCHER()	// ensure called same frame as mission blocking area are setup
						
						RC_START_CUTSCENE_MODE(<< -149.75, 285.81, 93.67 >>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE) //has to be after start_cutscene and wait(0)
						SETUP_CLEAR_AREAS_AND_VEHICLE_RESOLVE_FOR_INTRO_MOCAP()
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_INTRO")
							IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
							AND bFinishedStageSkipping
								START_AUDIO_SCENE("PAPARAZZO_01_INTRO")
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP set audio scene active - ", "PAPARAZZO_01_INTRO") ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP skipped for replay audio scene - ", "PAPARAZZO_01_INTRO") ENDIF #ENDIF
							ENDIF
						ENDIF
						CREATE_BEVERLYS_BIKE_FOR_MISSION_START()	// have to do this here, since it can be seen during the mocap cutscene otherwise
						LOAD_MISSION_ASSETS(FALSE)	// call to start requesting assets need for mocap end							
						CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP done")
						eSubStage = SS_UPDATE
					ENDIF
				ELSE
					REQUEST_CUTSCENE("PAP_1_RCM")	// unable to use RC_REQUEST_CUTSCENE prior to IS_BEVERLY_READY_TO_START_INTRO_MOCAP because it takes away player control	
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
				ENDIF
			ENDIF
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			// Get handle to Beverly's camera in the mocap
			IF NOT DOES_ENTITY_EXIST(sObjCameraBeverly.object)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_BeverlysCamera))
					sObjCameraBeverly.object = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_BeverlysCamera))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ObjCameraBeverly.object - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			// Get handle to Beverly's flash unit in the mocap
			IF NOT DOES_ENTITY_EXIST(sObjFlashUnitBeverly.object)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_BeverlysCameraFlash))
					sObjFlashUnitBeverly.object = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_BeverlysCameraFlash))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sObjFlashUnitBeverly.object - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			// Get handle to celeb vehicle in the mocap
			IF NOT DOES_ENTITY_EXIST(sCelebVehicle.vehicle)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MirandasVehicle))
					sCelebVehicle.vehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MirandasVehicle))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sCelebVehicle.vehicle - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			// Get handle to Celeb ped in the mocap
			IF NOT DOES_ENTITY_EXIST(sCelebPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Miranda))
					sCelebPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Miranda))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sCelebPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			// Get handle to Celeb Driver ped in the mocap
			IF NOT DOES_ENTITY_EXIST(sCelebsDriverPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MirandasDriver))
					sCelebsDriverPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_MirandasDriver))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sCelebsDriverPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF			
			// Get handle to Beverly in the mocap
			IF NOT DOES_ENTITY_EXIST(sBeverlyPed.ped)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Beverly))
					sBeverlyPed.ped = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY(sSceneHandle_Beverly))
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sBeverlyPed.ped - got handle to mocap registered entity FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF	
			ENDIF
					
			// Set exit state for Beverly, his camera and flash unit
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Beverly)
				IF IS_PED_UNINJURED(sBeverlyPed.ped)	
					// set Beverly's correct position if the cutscene was skipped
					IF WAS_CUTSCENE_SKIPPED()
					AND IS_SCREEN_FADED_OUT()
						SET_ENTITY_COORDS_NO_OFFSET(sBeverlyPed.ped, sBeverlyPed.vSpawn)
						SET_ENTITY_HEADING(sBeverlyPed.ped, sBeverlyPed.fSpawnHeading)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - cutscene skipped, set Bev pos and heading FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF	
					IF IS_ENTITY_ALIVE(sObjFlashUnitBeverly.object)
					AND IS_ENTITY_ALIVE(sObjCameraBeverly.object)
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_BeverlysCameraFlash)
							ATTACH_ENTITY_TO_ENTITY(sObjFlashUnitBeverly.object, sObjCameraBeverly.object, 0, sObjFlashUnitBeverly.vOffset, sObjFlashUnitBeverly.vRotation)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sObjFlashUnitBeverly.object - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF			
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_BeverlysCamera)
							ATTACH_ENTITY_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped, GET_PED_BONE_INDEX(sBeverlyPed.ped, BONETAG_PH_R_HAND), sObjCameraBeverly.vOffset, sObjCameraBeverly.vRotation)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - ObjCameraBeverly.object - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
					ENDIF
					SETUP_BEVERLY_FOR_IN_GAME_START()	
					//CLEAR_PED_TASKS_IMMEDIATELY(sBeverlyPed.ped)							
					SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)	
					//TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1) moved the setup of next state
					SET_BEVERLY_PLAY_COME_ON_ANIM("waive_comeback_f", INSTANT_BLEND_IN)	// exact frame the mocap exit state ends with - real frame is 0.593						
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sBeverlyPed.ped)
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sBeverlyPed.ped - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			
			// Set exit state for Celeb, Driver and Celeb vehicle
			IF IS_VEHICLE_OK(sCelebVehicle.vehicle)	
				IF IS_PED_UNINJURED(sCelebPed.ped)
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Miranda)
						IF NOT IS_PED_SITTING_IN_VEHICLE(sCelebPed.ped, sCelebVehicle.vehicle)
							CLEAR_PED_TASKS_IMMEDIATELY(sCelebPed.ped)
							SET_PED_INTO_VEHICLE(sCelebPed.ped, sCelebVehicle.vehicle, VS_BACK_RIGHT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE -  sCelebPed.ped set into vehicle on exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
						SETUP_CELEB_FOR_IN_GAME_START() // done after are tasks get cleared above
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE -  sCelebPed.ped - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sCelebsDriverPed.ped)
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_MirandasDriver)
						IF NOT IS_PED_SITTING_IN_VEHICLE(sCelebsDriverPed.ped, sCelebVehicle.vehicle)
							CLEAR_PED_TASKS_IMMEDIATELY(sCelebsDriverPed.ped)
							SET_PED_INTO_VEHICLE(sCelebsDriverPed.ped, sCelebVehicle.vehicle, VS_DRIVER)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sCelebsDriverPed.ped set into vehicle on exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
						ENDIF
						SETUP_CELEB_DRIVER_FOR_IN_GAME_START() // done after are tasks get cleared above
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE -  sCelebsDriverPed.ped - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_MirandasVehicle)
					SETUP_CELEB_VEHICLE_FOR_IN_GAME_START()
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - sCelebVehicle.vehicle - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF				
			ENDIF
			
			// Set exit state for Franklin
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Franklin)
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF WAS_CUTSCENE_SKIPPED()
					AND IS_SCREEN_FADED_OUT()
						//SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-152.4410, 279.5606, 92.8229>>)
						//SET_ENTITY_HEADING(PLAYER_PED_ID(), 89.9099)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)	// 5.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)	//-16.0)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - cutscene skipped, set Player pos and heading FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF					
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 3000)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sBeverlyPed.ped, -1)		
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Franklin - set mocap exit state FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				bHasSetExitStateFromIntroMocapForGameplayCam = TRUE	// stop setting the gameplay cam now. 
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - bHasSetExitStateFromIntroMocapForGameplayCam set TRUE by can set exit state for Franklin FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			
			// unfortunately the only way to ensure the mocap camera blends out to gameplay cam at a sensible position is to spam this until the player's character exit state has fired
			IF NOT bHasSetExitStateFromIntroMocapForGameplayCam
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)	// 5.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)	//-16.0)	
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
			AND IS_SCREEN_FADED_OUT()
				REPLAY_STOP_EVENT()
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Camera - set mocap skipped values FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF				
				CLEAR_AREA_OF_VEHICLES(<< -162.42995, 279.91364, 92.71819 >>, 200)	//larger area for skipping since we are faded down anyway
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Clear cars  - skipped over the mocap FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()				
				CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - mocap cutscene finished FC = ", GET_FRAME_COUNT())
				LOAD_MISSION_ASSETS(TRUE)						
				CREATE_ENTITIES_FOR_MISSION_START()		
				//backup screen fade in, but not if we are skipping
				IF bFinishedStageSkipping
					SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
				ENDIF						
				eSubStage = SS_CLEANUP	
			ELSE
				// B*1385066 - prevent cutscene skip during blend out
				IF GET_CUTSCENE_TIME() >= 102000
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
				IF DOES_ENTITY_EXIST(objTrafficLightWestBound)
					SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightWestBound, TLO_GREEN)	// make the remaining traffic flow out the area
				ENDIF
				IF DOES_ENTITY_EXIST(objTrafficLightEastBound)
					SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightEastBound, TLO_GREEN)	// make the remaining traffic flow out the area
				ENDIF
				LOAD_MISSION_ASSETS()
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - mocap progress Time : ", GET_CUTSCENE_TIME(), " FC = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			ENDIF				
		BREAK
		CASE SS_CLEANUP
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ELSE
				REPLAY_STOP_EVENT()
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ENDIF	
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				IF bFinishedStageSkipping	//only do this if we aren't stage skipping
					RC_END_CUTSCENE_MODE()
				ENDIF
				IF DOES_ENTITY_EXIST(objTrafficLightWestBound)
					SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightWestBound, TLO_NONE)	// release control of the traffic lights
				ENDIF
				SAFE_REMOVE_OBJECT(objTrafficLightWestBound, FALSE)	// don't delete the traffic lights grabbed from the world
				IF DOES_ENTITY_EXIST(objTrafficLightEastBound)
					SET_ENTITY_TRAFFICLIGHT_OVERRIDE(objTrafficLightEastBound, TLO_NONE)	// release control of the traffic lights
				ENDIF				
				SAFE_REMOVE_OBJECT(objTrafficLightEastBound, FALSE)	// don't delete the traffic lights grabbed from the world
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				SET_STAGE(MISSION_STAGE_CHASE_CELEB_IN_VEHICLE) 
				CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_CLEANUP done")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Beverly takes photos of Miranda from the back of a motorbike
///    which Franklin is driving.
PROC STAGE_CHASE_CELEB_IN_VEHICLE()

	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	
	SWITCH eSubStage		
		//	-------------------------------------------------------------------------------------
	 	CASE SS_SETUP
			IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
			AND IS_PED_UNINJURED(sCelebsDriverPed.ped)
			AND IS_PED_UNINJURED(sCelebPed.ped)
			
				// B*1490105 - make sure ambient cars don't pull out on the player here (area isn't visible when we delete down the main road)
				CLEAR_ANGLED_AREA_OF_VEHICLES(<<-416.352783,222.045761,77.410965>>, <<-284.300873,233.325439,89.963669>>, 80.000000)
				
				//Setup Uber Playback
				INITIALISE_UBER_PLAYBACK(sPap1_UberRecordingName, 1, FALSE)	
				LOAD_UBER_DATA()
				SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
				SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
				switch_SetPieceCar_to_ai_on_collision = TRUE
				allow_veh_to_stop_on_any_veh_impact = TRUE
				fUberPlaybackDensitySwitchOffRange = 285.0
				//fPlaybackCarStreamingDistance = 160.0	// 120.0 // is default
				traffic_block_vehicle_colour(true, traffic_black)
				SET_RELEASED_DRIVERS_TO_EXIT_VEHICLES_ON_FLEE(TRUE) // B* 1490105 - stop peds driving the uber cars interfering with uber chase because of reaction to player shooting
				PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "UBER RECORDING KICKED OFF") ENDIF #ENDIF
					FREEZE_ENTITY_POSITION(sCelebVehicle.vehicle, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)
					//PAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
					//FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sCelebVehicle.vehicle)
						
					//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 0.1)
					//SET_UBER_PLAYBACK_TO_TIME_NOW(sCelebVehicle.vehicle, 0.1)	
					
					//UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(sCelebVehicle.vehicle)
					//#IF IS_DEBUG_BUILD	DISPLAY_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, RDM_WHOLELINE)	#ENDIF
					CREATE_ALL_WAITING_UBER_CARS()
					fMainPlaybackSpeed = 0.5
					SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
					UPDATE_UBER_PLAYBACK(sCelebVehicle.vehicle, fMainPlaybackSpeed)
				ENDIF				
			ENDIF
			IF IS_PED_UNINJURED(sCelebPed.ped)
				SET_PED_COMBAT_ATTRIBUTES(sCelebPed.ped, CA_LEAVE_VEHICLES, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(sCelebPed.ped, CA_CAN_TAUNT_IN_VEHICLE, TRUE)
			ENDIF
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE) // fixes multiple helmet issues plus lets player drive off quicker when falling off
			IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
				MODIFY_VEHICLE_TOP_SPEED(sBeverlyVehicle.vehicle, BIKE_TOP_SPEED_MODIFIER)
			ENDIF
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(sCelebVehicle.vehicle, "PAPARAZZO_1_LIMO_GROUP")
			iTimer_DelayBeverlyInitialTurnToFaceFranklin = GET_GAME_TIMER()
			
			//B*1520472 - give player 20% boost if he has 15% or less
			INT iTempStatValue
			STAT_GET_INT(SP1_SPECIAL_ABILITY, iTempStatValue)	// Franklin check Franklin has some charge in his bar (returns seconds, 30 being full)
			IF iTempStatValue < 4.5	// 15 %					
				SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.2, TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - special ability boost needed current value = ", iTempStatValue) ENDIF #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - special ability boost NOT needed current value = ", iTempStatValue) ENDIF #ENDIF
			ENDIF
			
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - SS_SETUP : tasked Bev to look at player ^^^") ENDIF #ENDIF
				ENDIF
			ENDIF
			IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
			AND bFinishedStageSkipping
				SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_START)
			ENDIF
			
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)	// B*1521921 - manually set the player's cutscene variation for the outro to ensure helmet appears if needed
 
 			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_LOWEST)
 
			eSubStage = SS_UPDATE	
			CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - SS_SETUP done")
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
			AND IS_VEHICLE_OK(sCelebVehicle.vehicle)
			AND IS_PED_UNINJURED(sCelebPed.ped)
			AND IS_PED_UNINJURED(sCelebsDriverPed.ped)
			AND IS_PED_UNINJURED(sBeverlyPed.ped)
			
				// only request the rival pap assets when the chase gets close  to the spawn point
				IF IS_ENTITY_IN_RANGE_COORDS(sCelebVehicle.vehicle, sRivalPapVehicle.vSpawn, 250.0)
					LOAD_ASSETS_FOR_RIVAL_PAP_ARRIVING(FALSE) //request assets for rival pap section early
				ENDIF
			
				fDistance_PlayerToBeverly = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sBeverlyPed.ped))	// used in several functions	
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist between player and beverly = ", fDistance_PlayerToBeverly) ENDIF #ENDIF	
				UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME()
				
				IF NOT bHasPlayerGotOnBike_FirstTime	//slower speed whilst player hasn't got on the bike for the first time
					IF NOT DOES_BLIP_EXIST(sBeverlyVehicle.blip)
						sBeverlyVehicle.blip = CREATE_VEHICLE_BLIP(sBeverlyVehicle.vehicle)
					ENDIF
					IF NOT bDoneDialogue_InitialGetOnBike
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF HAS_TIME_PASSED(iTimer_DelayBeverlyInitialTurnToFaceFranklin, 500)	// note also used to delay initial AI for Bev
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_BBF1", CONV_PRIORITY_MEDIUM)
								// Let's go! You drive, I'll get the shot.  
								TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)	
								bDoneDialogue_InitialGetOnBike = TRUE	
							ENDIF
						ENDIF
					ELIF NOT bDoneObjective_GetOnBike
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							VEHICLE_INDEX vehTemp
							IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								vehTemp = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, TRUE)	// bug fix 1042233
							AND (sBeverlyVehicle.vehicle != vehTemp)
								PRINT_NOW("PAP1_10", DEFAULT_GOD_TEXT_TIME, 1)	// Get on Beverly's ~b~motorbike.~s~
							ENDIF
							iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()	// set this here for his random lines
							bDoneObjective_GetOnBike = TRUE
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - done initial get on bike objective") ENDIF #ENDIF
						ENDIF
					ENDIF
					//Play bev comment about getting on the bike, setup in D* as random dialogue  taken from MANAGE_PLAYER_AND_BEVERLY()
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
					ELSE
						IF bDoneDialogue_InitialGetOnBike	// wait for the initial dialogue
							IF (GET_GAME_TIMER() - iDialogueTimer_PlayerOffTheBike > 1000)	// really short delay
								IF (fDistance_PlayerToBeverly < DIST_LIMIT_PLAYER_TO_BEVERLY_OK_FOR_DIALOGUE)
									IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
									AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),  sBeverlyVehicle.vehicle)
										// allow comment without subtitle if 
										IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, "PAP1_IG5", CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())								
											// Come on! Come on!
											// This is the opportunity of a lifetime!
											// Miranda Cowan, man! This is immense!
											// What are you waiting for? This is important!  
											// Oh my God, if she gets away, I will lose my shit!!!
											iDialogueTimer_PlayerOffTheBike = GET_GAME_TIMER()
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - GET ON THE BIKE DIALOGUE TRIGGERED - PAP1_IG5") ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
					OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, TRUE)
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						SAFE_REMOVE_BLIP(sBeverlyVehicle.blip)
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
							STOP_ANIM_TASK(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn, REALLY_SLOW_BLEND_OUT)
						ENDIF
						TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
						SET_BEVERLY_FOLLOW_PLAYER()
						// stop the dialogue about getting on the bike if it's playing
						IF IS_SPECIFIC_CONVERSATION_ROOT_CURRENTLY_PLAYING("PAP1_IG5")
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
						AND bFinishedStageSkipping
							SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_BIKE)
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_INTRO")
							STOP_AUDIO_SCENE("PAPARAZZO_01_INTRO")
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - SS_UPDATE stopped active audio scene ", "PAPARAZZO_01_INTRO") ENDIF #ENDIF
						ENDIF
						IF NOT IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_CHASE")
							IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
							AND bFinishedStageSkipping
								START_AUDIO_SCENE("PAPARAZZO_01_CHASE")								
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP set audio scene active - ", "PAPARAZZO_01_CHASE") ENDIF #ENDIF
							ELSE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_INTRO_MOCAP_SCENE - SS_SETUP skiped for replay audio scene active - ", "PAPARAZZO_01_CHASE") ENDIF #ENDIF
							ENDIF
						ENDIF					
						bHasPlayerGotOnBike_FirstTime = TRUE
					ELSE
						IF NOT IS_PED_HEADTRACKING_PED(sBeverlyPed.ped, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
						ENDIF
						IF IS_ENTITY_IN_RANGE_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), 4.5)
							IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
							//	STOP_ENTITY_ANIM(sBeverlyPed.ped, tlCurrentAnim_BeverlyShoutComeOn, tlAnimDict_BeverlyShoutComeOn, SLOW_BLEND_OUT)
							//	#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE Bev shout anim stopped for player proximity") ENDIF #ENDIF
							ELSE
								IF NOT IS_PED_FACING_PED(sBeverlyPed.ped, PLAYER_PED_ID(), 45.0)
									IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
										TASK_TURN_PED_TO_FACE_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
										#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE Bev set to turn to Franklin") ENDIF #ENDIF
									ENDIF
								ENDIF
								SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
							ENDIF
						ELSE
							IF HAS_TIME_PASSED(iTimer_DelayBeverlyInitialTurnToFaceFranklin, 250)	// note also used to delay initial dialogue from Bev
								IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, tlAnimDict_BeverlyShoutComeOn, tlCurrentAnim_BeverlyShoutComeOn)
									IF NOT IS_PED_FACING_PED(sBeverlyPed.ped, PLAYER_PED_ID(), 45.0)
										IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
											TASK_TURN_PED_TO_FACE_ENTITY(sBeverlyPed.ped, PLAYER_PED_ID(), -1)
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE Bev set to turn to Franklin") ENDIF #ENDIF
										ENDIF
									ELSE
										IF HAS_TIME_PASSED(iTimerTillRepeatBevFrustratedAnim, TIME_DELAY_FOR_REPEATED_BEVERLY_FRUSTRATED_ANIM)
											IF SET_BEVERLY_PLAY_COME_ON_ANIM("", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT)
												iTimerTillRepeatBevFrustratedAnim = GET_GAME_TIMER()
												#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE Bev set to play come here anim") ENDIF #ENDIF
											ENDIF
										ENDIF
									ENDIF
									SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
								ELSE
									iTimerTillRepeatBevFrustratedAnim = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MANAGE_PLAYER_AND_BEVERLY()	
				ENDIF				
				MANAGE_PED_SHOOTING_UP(sCelebPed.ped)
				//celeb reached trigger point in uber recording for rival pap to join in
				IF fCurrentPlaybackTime >= UBER_PLAYBACK_TIME_RIVAL_PAP_JOIN_CHASE_STAGE
					CPRINTLN(DEBUG_MISSION, "celeb REACHED TRIGGER POINT IN REOCORDING FOR RIVAL PAP CAM")
					eSubStage = SS_CLEANUP
					EXIT
				ENDIF
				//check to see if the player or has reached the rival pap area before the celeb
				//DRAW_DEBUG_SPHERE(vRivalPapCam_CutsceneTriggerPos, CLOSE_TO_RIVAL_PAP_CAM_CUTSCENE_TRIGGER_DIST, 0, 0, 255, 120)
				IF(GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRivalPapCam_CutsceneTriggerPos) < CLOSE_TO_RIVAL_PAP_CAM_CUTSCENE_TRIGGER_DIST)
					CPRINTLN(DEBUG_MISSION, "PLAYER CLOSE TO RIVAL PAP CAM TRIGGER AREA")
					eSubStage = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "chase celeb cleanup")	 #ENDIF			
			UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME()			
			//get this requested early
			REQUEST_MODEL(sRivalPapVehicle.model)		
			SET_STAGE(MISSION_STAGE_RIVAL_PAP_JOIN_CHASE)
			CPRINTLN(DEBUG_MISSION, "STAGE_CHASE_CELEB_IN_VEHICLE - SS_CLEANUP done")
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Point where the rival pap joins in the chase
///    //Rival pap gets some good photos of Miranda
PROC STAGE_RIVAL_PAP_JOIN_CHASE()

	VECTOR vPlayerPos	
	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	IF NOT bDoneHintCamForRival
		UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME()
	ELSE
		UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME(FALSE)	//don't do chase hint cam for CELEB if we have switch to following the rival pap
	ENDIF	
	
 	SWITCH eSubStage		
		//	-------------------------------------------------------------------------------------
 		CASE SS_SETUP	
			bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this to ensure Franklin doesn't say something straight away
			LOAD_ASSETS_FOR_RIVAL_PAP_ARRIVING()
			CREATE_RIVAL_PAP_FOR_ENTRANCE()	
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
			AND IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
				//revert pap to idle anim
				IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")
					TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
				ENDIF
			ENDIF
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - SS_SETUP done")
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				fDistance_PlayerToBeverly = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sBeverlyPed.ped))	// used in several functions	
				
				IF NOT IS_PED_UNINJURED(sRivalPapPed.ped)
					//Rival is a dead to advance to next stage
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapPed.ped, PLAYER_PED_ID(), FALSE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							bHasPlayerShotRivalPapOffBike = TRUE
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapPed.ped) // reset the damage check
							CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival pap dead - bHasPlayerShotRivalPapOffBike = TRUE")
						ELSE
							CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival pap dead - damaged by weapon check returned FALSE")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival pap dead - entity damaged by player check returned FALSE")
					ENDIF
					eSubStage = SS_CLEANUP
				ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapPed.ped)
					//rival pap has been knocked off the bike, so advance to next stage
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapPed.ped, PLAYER_PED_ID(), FALSE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							bHasPlayerShotRivalPapOffBike = TRUE
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapPed.ped) // reset the damage check
							SET_ENTITY_HEALTH(sRivalPapPed.ped, 0)	// if the player shot him off the bike - make sure he dies from getting knocked off
							CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival pap knocked off - bHasPlayerShotRivalPapOffBike = TRUE")
						ELSE
							CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival pap knocked off - damaged by weapon check returned FALSE")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rrival pap knocked off - entity damaged by player check returned FALSE")
					ENDIF
					// extra check to see if the player shot the driver off
					IF NOT bHasPlayerShotRivalPapOffBike
						IF NOT IS_PED_UNINJURED(sRivalPapDriverPed.ped)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapDriverPed.ped, PLAYER_PED_ID(), FALSE)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapDriverPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
									bHasPlayerShotRivalPapOffBike = TRUE
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapDriverPed.ped) // reset the damage check
									CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival dead or off bike - but detected DRIVER dead by - bHasPlayerShotRivalPapOffBike = TRUE")
								ENDIF
							ENDIF
						ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapDriverPed.ped, PLAYER_PED_ID(), FALSE)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapDriverPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
									bHasPlayerShotRivalPapOffBike = TRUE
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapDriverPed.ped) // reset the damage check
									CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - rival dead or off bike - but detected DRIVER of bike by - bHasPlayerShotRivalPapOffBike = TRUE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
				ELSE
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist between player and beverly = ", fDistance_PlayerToBeverly) ENDIF #ENDIF	
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())						
					fDistance_PlayerToRivalPap = VDIST(vPlayerPos, GET_ENTITY_COORDS(sRivalPapPed.ped))					
					UPDATE_RIVAL_PAP_ON_BIKE()							
									
					//celeb reached trigger point in uber recording where rival pap begins to peal off
					//can also switch state if the spotted dialogue has finished early
					IF fCurrentPlaybackTime >= UBER_PLAYBACK_TIME_STOP_RIVAL_PAP_STAGE
						CPRINTLN(DEBUG_MISSION, " STAGE_RIVAL_PAP_JOIN_CHASE : celeb REACHED TRIGGER POINT IN REOCORDING FOR RIVAL PAP TO PEAL OFF, switch to STOP RIVAL PAP")
						eSubStage = SS_CLEANUP
						EXIT
					ENDIF
					
					// force rival off the bike if his driver has come off when he;s still on
					IF NOT IS_PED_UNINJURED(sRivalPapDriverPed.ped)						
						IF IS_PED_ON_ANY_BIKE(sRivalPapPed.ped)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								CPRINTLN(DEBUG_MISSION, " STAGE_RIVAL_PAP_JOIN_CHASE : Rival pap knocked off bike in script - driver was injured")
							ENDIF
						ENDIF
					ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
						SET_ENTITY_HEALTH(sRivalPapDriverPed.ped, 0)	// kill him off if he fell off
						
						IF IS_PED_ON_ANY_BIKE(sRivalPapPed.ped)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								CPRINTLN(DEBUG_MISSION, " STAGE_RIVAL_PAP_JOIN_CHASE : Rival pap knocked off bike in script - driver wasn't on bike")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//maintain chase hint cam switch over CELEB focus cam is handled in UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME()
				IF NOT bDoneHintCamForRival
					IF fTimeInRivalPapRecording > 1000.0	//fTimeInRivalPapRecording is set in UPDATE_RIVAL_PAP_ON_BIKE()	
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						bDoneHintCamForRival = TRUE
					ENDIF
				ELSE
					CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, sRivalPapPed.ped)
				ENDIF	
				MANAGE_PLAYER_AND_BEVERLY()					
			ENDIF
		BREAK
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP		
			IF bFinishedStageSkipping	//don't do this if we are skipping
				UPDATE_RIVAL_PAP_ON_BIKE()	//this will handle rival pap driver continuing to drive on his own if he's alive on the bike and also stop the bike recording if no driver
			ENDIF			
			IF IS_PED_UNINJURED(sRivalPapPed.ped)
				CONTROL_PED_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, sRivalPapPed.ped)
			ENDIF			
			SET_STAGE(MISSION_STAGE_STOP_RIVAL_PAP)	
			CPRINTLN(DEBUG_MISSION, "STAGE_RIVAL_PAP_JOIN_CHASE - SS_CLEANUP done")
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///   Player given objective to stop the rival paparazzi
///    Beverly tells player to drive along side the rival pap so he can knock him off in road rash style attack
PROC STAGE_STOP_RIVAL_PAP()

	VECTOR vPlayerPos	
	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	MANAGE_OUTRO_MOCAP_LOADING()
	
 	SWITCH eSubStage		
		//	-------------------------------------------------------------------------------------
 		CASE SS_SETUP			
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - SS_SETUP	^^^^^^^^") ENDIF #ENDIF
			UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME(FALSE)		
			bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this to ensure Franklin doesn't say something straight away
			
			//check if he was rival was already killed or knocked off the bike in previous stage
			IF NOT IS_PED_UNINJURED(sRivalPapPed.ped)
				//Rival is a dead to advance to next stage
				eSubStage = SS_CLEANUP
			ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapPed.ped)
				//rival pap has been knocked off the bike, so advance to next stage
				eSubStage = SS_CLEANUP
			ELSE
				UPDATE_RIVAL_PAP_ON_BIKE()
				CONTROL_PED_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, sRivalPapPed.ped)				
				iTimer_DelayStopPlayerVehicleRecording_ForMissionStageSkip = GET_GAME_TIMER()	//used to stop the player vehicle recording which is set when skipping to stage				
				eSubStage = SS_UPDATE
				CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - SS_SETUP done")			
			ENDIF
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			//cleanup Player vehicle recording - only requested and used when skipping to stop rival pap stage
			IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sBeverlyVehicle.vehicle)
					IF CHECK_FOR_PLAYER_INTERUPT_ROLLING_START()
					OR HAS_TIME_PASSED(iTimer_DelayStopPlayerVehicleRecording_ForMissionStageSkip, TIME_DELAY_STOP_PLAYER_VEHICLE_RECORDING_FOR_STAGE_SKIP)
						STOP_PLAYBACK_RECORDED_VEHICLE(sBeverlyVehicle.vehicle)
						REMOVE_VEHICLE_RECORDING(1, "Pap1PV")	//Special case recording for player vehicle, requested and used when skipping to stop rival pap stage
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYER VEHICLE RECORDING STOPPED!!!") ENDIF #ENDIF
					ENDIF
				ENDIF
			ENDIF			
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
			
				fDistance_PlayerToBeverly = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sBeverlyPed.ped))	// used in several functions	
				
				//uber recording
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						UPDATE_PAP1_UBER_CHASE_FUNCS_THIS_FRAME(FALSE)						
						IF NOT bDoneSetupCelebForCleanup
							IF fCurrentPlaybackTime >= FINISH_UBER_RECORDING_EARLY_TIME
								//cleanup uber recording
								STOP_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)								
								CLEANUP_UBER_PLAYBACK()
								REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
								REMOVE_VEHICLE_RECORDING(1 , sPap1_UberRecordingName)	
								SETUP_CELEB_DRIVERS_BEHAVIOUR_WHEN_UBER_RECORDING_FINISHES()
								//added in attempt to populate roads better
								SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
								SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
								SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
								SET_VEHICLE_POPULATION_BUDGET(3)								
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneSetupCelebForCleanup - done early - STAGE_STOP_RIVAL_PAP update") ENDIF #ENDIF
								bDoneSetupCelebForCleanup = TRUE	
							ENDIF
						ENDIF
					ELSE	
						// keep updating the dist check as we need it for dialogue - still chasing celeb instead of rival pap
						IF IS_ENTITY_ALIVE(sCelebVehicle.vehicle)
							fCurrentChaseDistanceCeleb = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sCelebVehicle.vehicle))
						ENDIF
						IF NOT bDoneSetupCelebForCleanup
							//cleanup uber recording
							CLEANUP_UBER_PLAYBACK()
							REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
							REMOVE_VEHICLE_RECORDING(1 , sPap1_UberRecordingName)
							//added in attempt to populate roads better
							SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
							SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
							SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
							SET_VEHICLE_POPULATION_BUDGET(3)
							SETUP_CELEB_DRIVERS_BEHAVIOUR_WHEN_UBER_RECORDING_FINISHES()
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneSetupCelebForCleanup - done - STAGE_STOP_RIVAL_PAP update") ENDIF #ENDIF
							bDoneSetupCelebForCleanup = TRUE					
						ELSE
							CLEANUP_CELEB_WHEN_AWAY_FROM_THE_PLAYER()
						ENDIF
					ENDIF
				ENDIF	
				// maintain the overrides each frame
				IF bDoneSetupCelebForCleanup
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
					SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.3)
					SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.6)
				ENDIF
				IF NOT IS_PED_UNINJURED(sRivalPapPed.ped)
					//Rival is a dead to advance to next stage
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapPed.ped, PLAYER_PED_ID(), FALSE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							bHasPlayerShotRivalPapOffBike = TRUE
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapPed.ped) // reset the damage check
							CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - rival pap dead - bHasPlayerShotRivalPapOffBike = TRUE")
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
				ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapPed.ped)
					//rival pap has been knocked off the bike, so advance to next stage
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapPed.ped, PLAYER_PED_ID(), FALSE)
						IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
							bHasPlayerShotRivalPapOffBike = TRUE
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapPed.ped) // reset the damage check
							SET_ENTITY_HEALTH(sRivalPapPed.ped, 0)	// if the player shot him off the bike - make sure he dies from getting knocked off
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - rival pap off bike - bHasPlayerShotRivalPapOffBike = TRUE") ENDIF #ENDIF
						ENDIF
					ENDIF
					// extra check to see if the player shot the driver off
					IF NOT bHasPlayerShotRivalPapOffBike
						IF NOT IS_PED_UNINJURED(sRivalPapDriverPed.ped)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapDriverPed.ped, PLAYER_PED_ID(), FALSE)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapDriverPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
									bHasPlayerShotRivalPapOffBike = TRUE
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapDriverPed.ped) // reset the damage check
									CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - rival dead or off bike - but detected DRIVER dead by - bHasPlayerShotRivalPapOffBike = TRUE")
								ENDIF
							ENDIF
						ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRivalPapDriverPed.ped, PLAYER_PED_ID(), FALSE)
								IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sRivalPapDriverPed.ped, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)
									bHasPlayerShotRivalPapOffBike = TRUE
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRivalPapDriverPed.ped) // reset the damage check
									CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - rival dead or off bike - but detected DRIVER of bike by - bHasPlayerShotRivalPapOffBike = TRUE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					eSubStage = SS_CLEANUP
				ELSE					
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist between player and beverly = ", fDistance_PlayerToBeverly) ENDIF #ENDIF	
					CONTROL_PED_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, sRivalPapPed.ped)
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())						
					fDistance_PlayerToRivalPap = VDIST(vPlayerPos, GET_ENTITY_COORDS(sRivalPapPed.ped))			
					UPDATE_RIVAL_PAP_ON_BIKE()
					
					// force rival off the bike if his driver has come off when he;s still on
					IF NOT IS_PED_UNINJURED(sRivalPapDriverPed.ped)						
						IF IS_PED_ON_ANY_BIKE(sRivalPapPed.ped)							
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								CPRINTLN(DEBUG_MISSION, " STAGE_STOP_RIVAL_PAP : Rival pap knocked off bike in script - driver was injured")
							ENDIF
						ENDIF
					ELIF NOT IS_PED_SITTING_IN_ANY_VEHICLE(sRivalPapDriverPed.ped)
						SET_ENTITY_HEALTH(sRivalPapDriverPed.ped, 0)	// kill him off if he fell off
						
						IF IS_PED_ON_ANY_BIKE(sRivalPapPed.ped)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_EASY)
							IF CAN_KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								KNOCK_PED_OFF_VEHICLE(sRivalPapPed.ped)
								CPRINTLN(DEBUG_MISSION, " STAGE_STOP_RIVAL_PAP : Rival pap knocked off bike in script - driver wasn't on bike")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				MANAGE_PLAYER_AND_BEVERLY()
			ENDIF
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP
			//cleanup Player vehicle recording - only requested and used when skipping to stop rival pap stage
			IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sBeverlyVehicle.vehicle)
				STOP_PLAYBACK_RECORDED_VEHICLE(sBeverlyVehicle.vehicle)
				REMOVE_VEHICLE_RECORDING(1, "Pap1PV")	//Special case recording for player vehicle, requested and used when skipping to stop rival pap stage	
			ENDIF				
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			IF DOES_ENTITY_EXIST(sObjFlashUnitRivalPap.object)
				IF IS_ENTITY_ATTACHED(sObjFlashUnitRivalPap.object)
					DETACH_ENTITY(sObjFlashUnitRivalPap.object)
					APPLY_FORCE_TO_ENTITY(sObjFlashUnitRivalPap.object, APPLY_TYPE_EXTERNAL_IMPULSE, << 0.0, 0.0, -1.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, TRUE)
				ENDIF
				//SAFE_REMOVE_OBJECT(sObjFlashUnitRivalPap.object)	//they get removed too quick with this in
			ENDIF
			IF DOES_ENTITY_EXIST(sObjCameraRivalPap.object)
				IF IS_ENTITY_ATTACHED(sObjCameraRivalPap.object)
					DETACH_ENTITY(sObjCameraRivalPap.object)
					APPLY_FORCE_TO_ENTITY(sObjCameraRivalPap.object, APPLY_TYPE_EXTERNAL_IMPULSE, << 0.0, 0.0, -1.0 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, TRUE)
				ENDIF
				//SAFE_REMOVE_OBJECT(sObjCameraRivalPap.object)	//they get removed too quick with this in
			ENDIF		
			//this needs to match section in SS_UPDATE
			IF NOT bDoneSetupCelebForCleanup
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
					ENDIF
					SETUP_CELEB_DRIVERS_BEHAVIOUR_WHEN_UBER_RECORDING_FINISHES()
					CLEANUP_UBER_PLAYBACK()
					REMOVE_ALL_CAR_RECORDINGS_FOR_UBER_CHASE()
					//main car recording
					REMOVE_VEHICLE_RECORDING(1 , sPap1_UberRecordingName)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDoneSetupCelebForCleanup - done - STAGE_STOP_RIVAL_PAP cleanup") ENDIF #ENDIF
					bDoneSetupCelebForCleanup = TRUE
				ENDIF
			ENDIF
			// clean up the blocking area from earlier in the chase (stops global limit being pushed up)
			INT i
			FOR i = 0 TO (PAP1_MAX_SCENARIO_BLOCKING_AREAS_CHASE_ROUTE - 1)
				REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockingChaseRoute[i])
			ENDFOR
			SET_VEHICLE_POPULATION_BUDGET(3)	//reset
			UPDATE_RIVAL_PAP_ON_BIKE()	//this will handle rival pap driver continuing to drive on his own if he's alive on the bike and also stop the bike recording if no driver			
			SAFE_REMOVE_BLIP(sCelebPed.blip)
			SAFE_REMOVE_BLIP(sRivalPapPed.blip)
			SAFE_REMOVE_BLIP(sBeverlyPed.blip)
			SAFE_REMOVE_BLIP(sBeverlyVehicle.blip)
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			//SAFE_REMOVE_PED(sRivalPapDriverPed.ped)		//they get removed too quick with this in
			//SAFE_REMOVE_VEHICLE(sRivalPapVehicle.vehicle)	//they get removed too quick with this in
			IF NOT IS_PED_UNINJURED(sRivalPapPed.ped)
				REMOVE_PED_FOR_DIALOGUE(sDialogue, 4)		// "RIVALPAP"
			ENDIF
			IF IS_THIS_PRINT_BEING_DISPLAYED("PAP1_03")
				CLEAR_THIS_PRINT("PAP1_03")	//Chase after ~r~Tobias.~w~
			ENDIF
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PAP1_H1")
				CLEAR_HELP()
			ENDIF
			IF NOT IS_REPLAY_BEING_SET_UP() // don't allow music events to trigger during replay setup / stage skips
			AND bFinishedStageSkipping
				SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_STOP_TO_RADIO)
			ENDIF
			IF IS_ENTITY_ALIVE(sCelebVehicle.vehicle)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sCelebVehicle.vehicle)
			ENDIF
			IF IS_ENTITY_ALIVE(sRivalPapVehicle.vehicle)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sRivalPapVehicle.vehicle)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("PAPARAZZO_01_CHASE")
				STOP_AUDIO_SCENE("PAPARAZZO_01_CHASE")
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - SS_CLEANUP - stop active audio scene - ", "PAPARAZZO_01_CHASE") ENDIF #ENDIF
			ENDIF
			SET_STAGE(MISSION_STAGE_DROP_BEVERLY_OFF)
			CPRINTLN(DEBUG_MISSION, "STAGE_STOP_RIVAL_PAP - SS_CLEANUP done - heading to MISSION_STAGE_DROP_BEVERLY_OFF")
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Franklin has to take Beverly to his next stakeout
PROC STAGE_DROP_BEVERLY_OFF()	

	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	MANAGE_OUTRO_MOCAP_LOADING()
	
 	SWITCH eSubStage	
		//	-------------------------------------------------------------------------------------
 		CASE SS_SETUP
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_SETUP	^^^^^^^^") ENDIF #ENDIF		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_PAP1_DROP_BEVERLY_OFF, "Drop Beverley off", TRUE) // 3rd Mission replay checkpoint			
			IF IS_PED_UNINJURED(sRivalPapPed.ped)
				SET_ENTITY_LOAD_COLLISION_FLAG(sRivalPapPed.ped, FALSE)
			ENDIF
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
			ENDIF		
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_LOWEST)
			
			bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this to ensure Franklin doesn't say something straight away
			SET_WANTED_LEVEL_MULTIPLIER(0.8)	//raise this back up after the chase section
			vPathNodeRequestStartPosition = << 0.0, 0.0, 0.0 >> // reset ready for next request for drop off gps route
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 0, REPLAY_IMPORTANCE_LOWEST)	// B*1841676 - record taking out the rival paparazzo 
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_SETUP done")
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				fDistance_PlayerToBeverly = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sBeverlyPed.ped))	// used in several functions	
				//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "dist between player and beverly = ", fDistance_PlayerToBeverly) ENDIF #ENDIF								
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					//Don't allow the wanted level to drop this frame, as we need it to test in the cleanup
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_UPDATE - player got wanted rating")
					eSubStage = SS_CLEANUP
				ELSE
					MANAGE_PLAYER_AND_BEVERLY()	
					IF ePlayerTransportState = PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
						// make sure Beverly has said drop me off before advancing
						IF bDoneDialogue_RivalPapCameraDestroyed
						AND bDoneWaitForCameraDestroyedDialogue
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffPoint, << 4.0, 4.0, LOCATE_SIZE_HEIGHT >>, TRUE, TRUE, TM_IN_VEHICLE)	// used to use LOCATE_SIZE_ANY_MEANS for x and y but Les Bug * 1078339
							AND IS_ENTITY_AT_COORD(sBeverlyPed.ped, vDropOffPoint, << 4.0, 4.0, LOCATE_SIZE_HEIGHT >>)
								CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_UPDATE - player and bev at drop off location")
								eSubStage = SS_CLEANUP
							ENDIF
						ENDIF
					ENDIF						
				ENDIF
			ENDIF
			IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
				CLEANUP_CELEB_WHEN_AWAY_FROM_THE_PLAYER()
			ENDIF
			UPDATE_RIVAL_PAP_BEHAVIOUR_AFTER_CRASH()
			SET_RIVAL_PAP_DRIVER_INJURED_WHEN_OFF_THE_BIKE()
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP
			SAFE_REMOVE_BLIP(blipIndexDropOffPoint)
			SAFE_REMOVE_BLIP(sBeverlyPed.blip)
			SAFE_REMOVE_BLIP(sBeverlyVehicle.blip)	
			// clear drop off objective 
			IF IS_THIS_PRINT_BEING_DISPLAYED("PAP1_05")	// Take Beverly to his next ~y~stakeout.~s~	
				CLEAR_THIS_PRINT("PAP1_05")
			ENDIF
			//check which stage to go to next
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)				
				KILL_ANY_CONVERSATION()				
				SET_STAGE(MISSION_STAGE_LOSE_THE_COPS)
				CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_CLEANUP done - heading to MISSION_STAGE_LOSE_THE_COPS")
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)	// B*1521637 - dive off bike for cutscene
				SET_STAGE(MISSION_STAGE_END_CUTSCENE)
				CPRINTLN(DEBUG_MISSION, "STAGE_DROP_BEVERLY_OFF - SS_CLEANUP done - heading to MISSION_STAGE_END_CUTSCENE")
			ENDIF
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    player gets a wanted level during drop of Beverly stage
///    Wanted level must be cleared before dropping Beverly off
PROC STAGE_LOSE_THE_COPS()

	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	MANAGE_OUTRO_MOCAP_LOADING()
	
	SWITCH eSubStage	
		//	-------------------------------------------------------------------------------------
 		CASE SS_SETUP		
			//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_SETUP	^^^^^^^^") ENDIF #ENDIF			
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			IF IS_PED_UNINJURED(sBeverlyPed.ped)
				TASK_CLEAR_LOOK_AT(sBeverlyPed.ped)
			ENDIF		
			bDoneObjective_LoseTheCops = FALSE //reset lose the cops objective
			bDoneDialogue_FranklinRespondToBeverlyBerating = TRUE	// reset this to ensure Franklin doesn't say something straight away
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_SETUP")
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			IF IS_PED_UNINJURED(sBeverlyPed.ped)				
				fDistance_PlayerToBeverly = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sBeverlyPed.ped))	// used in several functions				
				MANAGE_PLAYER_AND_BEVERLY()	
				IF ePlayerTransportState = PAP1_PLAYER_TRANSPORT_STATE_ON_THE_BIKE_WITH_BEV
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)					
						CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_UPDATE - Player lost his wanted rating so going to cleanup")
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF
			ENDIF			
			IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
				CLEANUP_CELEB_WHEN_AWAY_FROM_THE_PLAYER()
			ENDIF
			UPDATE_RIVAL_PAP_BEHAVIOUR_AFTER_CRASH()
			SET_RIVAL_PAP_DRIVER_INJURED_WHEN_OFF_THE_BIKE()
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP			
			IF IS_THIS_PRINT_BEING_DISPLAYED("PAP1_09") 
				CLEAR_THIS_PRINT("PAP1_09")	// Lose the cops.
			ENDIF			
			KILL_ANY_CONVERSATION()	// B*1526224 - allow ongoing dialogue line to finish	
			SET_STAGE(MISSION_STAGE_DROP_BEVERLY_OFF)	
			CPRINTLN(DEBUG_MISSION, "STAGE_LOSE_THE_COPS - SS_CLEANUP")
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Franklin drops Beverly off at his next stakeout MOCAP
PROC STAGE_END_CUTSCENE_MOCAP()

	SWITCH eSubStage	
		//	-------------------------------------------------------------------------------------
		CASE SS_SETUP		
			// don't request the mocap if we are skipping past this stage
			IF NOT bFinishedStageSkipping
				IF IS_REPLAY_BEING_SET_UP()
					IF IS_ENTITY_ALIVE(sBeverlyVehicle.vehicle)
						SET_ENTITY_VISIBLE(sBeverlyVehicle.vehicle, TRUE)				
						IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
							SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
							CPRINTLN(DEBUG_MISSION, "INTRO MOCAP SETUP DONE FOR SKIP - set player into veh for NON replay ****** ")
						ENDIF
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)		
				ENDIF
				CPRINTLN(DEBUG_MISSION, "INTRO MOCAP SETUP DONE FOR SKIP - GOING TO SS_CLEANUP")
				eSubStage = SS_CLEANUP
			ELSE
				MANAGE_OUTRO_MOCAP_LOADING()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)	// B*1521637 - dive off bike for cutscene				
				
				IF RC_IS_CUTSCENE_OK_TO_START(TRUE)
					// Store the player's helmet status at this point to restore after the mocap
					IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())           				
						iStoredPlayerHelmetPropIndex = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
						iStoredPlayerHelmetPropTextureIndex = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
						bPlayerNeedsHelmentRestoreAfterOutroMocap = TRUE
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - stored player helmet details for restore after mocap ",
												" prop ID = ", iStoredPlayerHelmetPropIndex, " prop tex ID = ", iStoredPlayerHelmetPropTextureIndex, " :  Frame Count : ", GET_FRAME_COUNT())
					ELSE
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - player not wearing helmet - Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), sSceneHandle_Franklin, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					IF IS_ENTITY_ALIVE(sObjCameraBeverly.object)	// Think this has to be detached bofore i register Beverly for the cutscene
						IF IS_ENTITY_ATTACHED(sObjCameraBeverly.object)
							DETACH_ENTITY(sObjCameraBeverly.object)		// having to do this otherwise the camera is visibly offset in the wrong place...possilby where beverly ig is still
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - sObjCameraBeverly.object detached") ENDIF #ENDIF
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(sObjCameraBeverly.object, sSceneHandle_BeverlysCamera, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)		
					ENDIF	
					IF IS_ENTITY_ALIVE(sBeverlyPed.ped)
						REGISTER_ENTITY_FOR_CUTSCENE(sBeverlyPed.ped, sSceneHandle_Beverly, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					IF IS_ENTITY_ALIVE(sBeverlyVehicle.vehicle)
						REGISTER_ENTITY_FOR_CUTSCENE(sBeverlyVehicle.vehicle, sSceneHandle_BeverlysVehicle, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_LOWEST)
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_LOWEST)
					
					START_CUTSCENE()				
					WAIT(0)	//needed because cutscene doesn't start straight away, causing player to see vehicles getting removed.
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK(DEFAULT_FADE_TIME, FALSE)		// //B*1481353 - when skipping to mocap (let the mocap stage handle fade in) tried just before START_CUTSCENE but issue still occured
					
					RC_START_CUTSCENE_MODE(vDropOffPoint, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, FALSE)	// don't need to worry about player vehicle resolve since he'll be on bev's bike		
					//cutscene area	
					CLEAR_ANGLED_AREA_OF_VEHICLES(<<-1493.301025,-198.539536,47.397526>>, <<-1492.846191,-185.493973,52.481934>>, 6.000000, TRUE)	// tighter area check for B*1159709
					CLEAR_AREA_OF_PEDS(vDropOffPoint, 4.0)
					CLEAR_AREA(vDropOffPoint, 4.0, TRUE, TRUE)	// leave car gens as per B*1159709
					SET_RADIO_TO_STATION_NAME("OFF")	// B*1530475 - turn radio off
					SET_VEHICLE_MODEL_PLAYER_WILL_EXIT_SCENE(sBeverlyVehicle.model) // B*2057708 - new command from first person changes
					eSubStage = SS_UPDATE
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SS_SETUP *	 - Frame Count : ", GET_FRAME_COUNT())
				ENDIF
			ENDIF
		BREAK
		//	-------------------------------------------------------------------------------------
		CASE SS_UPDATE
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND IS_ENTITY_ALIVE(sBeverlyVehicle.vehicle)
				
				// setup Franklin's exit state
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_Franklin)
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, VS_DRIVER)
						CPRINTLN(DEBUG_MISSION, "****** STAGE_END_CUTSCENE_MOCAP - ", " set exit state for Franklin put on the bike in ss_update Frame Count : ", GET_FRAME_COUNT())
					ENDIF
					//SET_RADIO_TO_STATION_NAME("OFF")	// B*1530475 - turn radio off
					// Restore the player's helmet status
					IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
						IF bPlayerNeedsHelmentRestoreAfterOutroMocap
							SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), iStoredPlayerHelmetPropIndex)
							SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), iStoredPlayerHelmetPropTextureIndex)
							GIVE_PED_HELMET(PLAYER_PED_ID(), FALSE, DEFAULT, iStoredPlayerHelmetPropTextureIndex)
							CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - set exit state for Franklin : restored helmet - ",
												" prop ID = ", iStoredPlayerHelmetPropIndex, " prop tex ID = ", iStoredPlayerHelmetPropTextureIndex, " :  Frame Count : ", GET_FRAME_COUNT())
						ELSE
							CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - set exit state for Franklin : unable to restore helmet, bPlayerNeedsHelmentRestoreAfterOutroMocap was false :  Frame Count : ", GET_FRAME_COUNT())
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - !! player wearing helmet on exit state! so unable to restore - Frame Count : ", GET_FRAME_COUNT())
					ENDIF			
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_DO_NOTHING, FALSE, FAUS_CUTSCENE_EXIT)
					CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - bHasSetExitStateFromOutroMocapForGameplayCam set +++++ TRUE by can set exit state for Franklin FC = ", GET_FRAME_COUNT())
				ELSE					
					//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " set exit state for Franklin FALSE this frame Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				
				// setup Beverly's bike exit state
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(sSceneHandle_BeverlysVehicle)	
					SET_VEHICLE_ON_GROUND_PROPERLY(sBeverlyVehicle.vehicle)
					SET_VEHICLE_BRAKE_LIGHTS(sBeverlyVehicle.vehicle, FALSE)	// B*1554464 - brake light flashes for a few frames for some reason
					//FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sBeverlyVehicle.vehicle)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " set exit state for Beverly's * bike done Frame Count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
			ENDIF
			// unfortunately the only way to ensure the mocap camera blends out to gameplay cam at a sensible position is to spam this until the player's character exit state has fired
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				IF IS_ENTITY_ALIVE(sBeverlyVehicle.vehicle)
					SET_VEHICLE_BRAKE_LIGHTS(sBeverlyVehicle.vehicle, FALSE)	// B*1554464 - brake light flashes for a few frames for some reason
				ENDIF
				eSubStage = SS_CLEANUP
				CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - ", " SS_UPDATE * done Frame Count : ", GET_FRAME_COUNT())
			ELSE
				// B*1385066 - prevent cutscene skip during blend out
				IF GET_CUTSCENE_TIME() >= 26500
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				ENDIF
			ENDIF	
		BREAK		
		//	-------------------------------------------------------------------------------------
		CASE SS_CLEANUP	
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE()
			ELSE
				REPLAY_STOP_EVENT()
				IF HAS_CUTSCENE_LOADED()
					REMOVE_CUTSCENE()
				ENDIF
				IF bFinishedStageSkipping	//only do this if we aren't stage skipping
					RC_END_CUTSCENE_MODE()
				ENDIF
				IF IS_ENTITY_ALIVE(sBeverlyPed.ped)
					SET_ENTITY_COORDS_NO_OFFSET(sBeverlyPed.ped, << -1478.2562, -183.1669, 47.8236 >>)
					SAFE_REMOVE_OBJECT(sObjFlashUnitBeverly.object, TRUE)
					SAFE_REMOVE_OBJECT(sObjCameraBeverly.object, TRUE)					
					SAFE_REMOVE_PED(sBeverlyPed.ped, TRUE)
				ENDIF
				IF IS_ENTITY_ALIVE(sBeverlyVehicle.vehicle)			
					IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, VS_DRIVER)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "****** STAGE_END_CUTSCENE_MOCAP - ", "SS_CLEANUP frank put on bike as wasn't already! frame count : ", GET_FRAME_COUNT()) ENDIF #ENDIF
					ENDIF
					SET_VEHICLE_BRAKE_LIGHTS(sBeverlyVehicle.vehicle, FALSE)	// B*1554464 - brake light flashes for a few frames for some reason
					//SET_RADIO_TO_STATION_NAME("OFF")	// B*1530475 - turn radio off
				ENDIF
				SET_PLAYER_PED_DATA_IN_CUTSCENES()	// B*1521921 - reset to default
				SET_STAGE(MISSION_STAGE_MISSION_PASSED)
				CPRINTLN(DEBUG_MISSION, "STAGE_END_CUTSCENE_MOCAP - SS_CLEANUP")
			ENDIF	
		BREAK		
	ENDSWITCH	
ENDPROC

/// PURPOSE:
///    Mission has already failed at this point, but this handles
///    having to update the fail reason if the player does something more
///    serious during the fade.
PROC STAGE_MISSION_FAILED_WAIT_FOR_FADE()
	STRING sFailReason = NULL
	MANAGE_TRIGGER_MUSIC_EVENTS_DURING_MISSION()
	SWITCH eSubStage	
		//	------------------------------------------
		CASE SS_SETUP
			CLEAR_PRINTS()
			CLEAR_HELP()			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
			ENDIF			
			DELETE_ALL_MISSION_BLIPS()
			//setup mission failed dialogue if needed			
			IF eP1_MissionFailedReason = FAILED_PLAYER_ATTACKED_CELEB
				bDoneDialogue_MissionFailed = FALSE
				// Leave Miranda alone, you bastard!
				tlDialogueRoot_MissionFailed = "PAP1_FAIL2"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - set fail dialogue for attacked celeb") ENDIF #ENDIF
			ELIF eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
				bDoneDialogue_MissionFailed = FALSE
				// Leave Miranda alone, you bastard!
				tlDialogueRoot_MissionFailed = "PAP1_FAIL2"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - set fail dialogue for Beverly spooked") ENDIF #ENDIF
			ELIF eP1_MissionFailedReason = FAILED_CELEB_ESCAPED
			OR eP1_MissionFailedReason = FAILED_PAP_ESCAPED
				bDoneDialogue_MissionFailed = FALSE
				// For fuck sake, we lost them. 
				tlDialogueRoot_MissionFailed = "PAP1_FAIL1"
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - set fail dialogue for losing celeb / pap") ENDIF #ENDIF
			ENDIF
			
			//implemented to ensure Beverly gets off the bike to prevent bug 212968	
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())				
				SEQUENCE_INDEX seqIndex				
				//Setup Beverly for mission failed
				IF IS_PED_UNINJURED(sBeverlyPed.ped)									
					//Make Beverly flee the player if fail reason 
					IF eP1_MissionFailedReason = FAILED_LEFT_BEVERLY_BEHIND
					OR eP1_MissionFailedReason = FAILED_PLAYER_ATTACKED_CELEB
					//OR eP1_MissionFailedReason = FAILED_PLAYER_ATTACKED_BEVERLY
					OR eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
					OR eP1_MissionFailedReason = FAILED_CELEB_DIED
						// removal of ped group moved with in here as believe to be cause of bug 1178569
						// having to keep checking this here now because of // B*1441191
						IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
							IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
								REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)	
								CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP - *PED GROUP* BEV removed from group - STAGE_MISSION_FAILED_WAIT_FOR_FADE")
							ELSE
								CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP - *PED GROUP* BEV Uanble to remove due to ragdoll - STAGE_MISSION_FAILED_WAIT_FOR_FADE")
							ENDIF
						ENDIF
						// B*1441207 - make Bev drop his camera if he's on the bike when he beings to flee
						IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
						OR IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
							IF IS_ENTITY_ALIVE(sObjCameraBeverly.object)
								IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjCameraBeverly.object, sBeverlyPed.ped)
									DETACH_ENTITY(sObjCameraBeverly.object)
									CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP - BEV camera detached because he's on / getting on the bike")
									// B*1521559 - clear his holding camera anim if it's getting detached
									CLEANUP_BEVERLY_ON_BIKE_ANIMS(NORMAL_BLEND_OUT)
								ENDIF
							ENDIF
						ENDIF
						SET_PED_FLEE_ATTRIBUTES(sBeverlyPed.ped, FA_USE_VEHICLE, FALSE)							
						/*OPEN_SEQUENCE_TASK(seqIndex)
							IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
							OR IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)								
							ENDIF
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 100.0, -1)
						CLOSE_SEQUENCE_TASK(seqIndex)						
						TASK_PERFORM_SEQUENCE(sBeverlyPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)*/
						TASK_SMART_FLEE_PED(sBeverlyPed.ped, PLAYER_PED_ID(), 50.0, -1)	// lower from 100m to help with latency - shouldn't stop at 50 as game will have faded out by then
						SET_PED_KEEP_TASK(sBeverlyPed.ped, TRUE)	
						CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP - SET BEVERLY UP TO FLEE PLAYER IN FAILED SETUP	$$$")
					ENDIF
				ENDIF				
				//Setup Miranda, and her driver for mission failed
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sCelebPed.ped)
					IF NOT IS_PED_IN_ANY_VEHICLE(sCelebPed.ped)
						OPEN_SEQUENCE_TASK(seqIndex)								
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 50.0, -1)	// lower from 100m to help with latency - shouldn't stop at 50 as game will have faded out by then
						CLOSE_SEQUENCE_TASK(seqIndex)
						TASK_PERFORM_SEQUENCE(sCelebPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
					ENDIF
					SET_PED_KEEP_TASK(sCelebPed.ped, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET celeb UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
				ENDIF
				IF IS_PED_UNINJURED(sCelebsDriverPed.ped)			
					SET_PED_FLEE_ATTRIBUTES(sCelebsDriverPed.ped, FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(sCelebsDriverPed.ped, FA_PREFER_PAVEMENTS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sCelebsDriverPed.ped, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sCelebsDriverPed.ped, FA_LOOK_FOR_CROWDS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sCelebsDriverPed.ped, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)					
					IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					AND IS_PED_SITTING_IN_VEHICLE(sCelebsDriverPed.ped, sCelebVehicle.vehicle)
						OPEN_SEQUENCE_TASK(seqIndex)								
							TASK_VEHICLE_MISSION_PED_TARGET(NULL, sCelebVehicle.vehicle, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 500.0, -1.0)
						CLOSE_SEQUENCE_TASK(seqIndex)
						TASK_PERFORM_SEQUENCE(sCelebsDriverPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET celeb DRIVER UP TO FLEE PLAYER whilst in car IN FAILED SETUP	$$$") ENDIF #ENDIF
					ELSE
						OPEN_SEQUENCE_TASK(seqIndex)								
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 50.0, -1)	// lower from 100m to help with latency - shouldn't stop at 50 as game will have faded out by then
						CLOSE_SEQUENCE_TASK(seqIndex)
						TASK_PERFORM_SEQUENCE(sCelebsDriverPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET celeb DRIVER UP TO FLEE PLAYER whilst on foot IN FAILED SETUP	$$$") ENDIF #ENDIF
					ENDIF
					SET_PED_KEEP_TASK(sCelebsDriverPed.ped, TRUE)			
				ENDIF				
				//Setup rival pap and driver behaviour for mission failed
				BOOL bDoRivalPapNeedSettingUpToFlee
				bDoRivalPapNeedSettingUpToFlee = FALSE
				IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
						STOP_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
						bDoRivalPapNeedSettingUpToFlee = TRUE
					ENDIF
				ENDIF				
				IF IS_PED_UNINJURED(sRivalPapPed.ped)
					IF bDoRivalPapNeedSettingUpToFlee
						IF NOT IS_PED_IN_ANY_VEHICLE(sRivalPapPed.ped)
							OPEN_SEQUENCE_TASK(seqIndex)								
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 50.0, -1)	// lower from 100m to help with latency - shouldn't stop at 50 as game will have faded out by then
							CLOSE_SEQUENCE_TASK(seqIndex)
							TASK_PERFORM_SEQUENCE(sRivalPapPed.ped, seqIndex)
							CLEAR_SEQUENCE_TASK(seqIndex)
						ENDIF
					ENDIF
					SET_PED_KEEP_TASK(sRivalPapPed.ped, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET RIVAL PAP UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
				ENDIF				
				IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)			
					SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_PREFER_PAVEMENTS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_USE_COVER, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_LOOK_FOR_CROWDS, FALSE)
					SET_PED_FLEE_ATTRIBUTES(sRivalPapDriverPed.ped, FA_RETURN_TO_ORIGNAL_POSITION_AFTER_FLEE, FALSE)					
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					AND IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
						OPEN_SEQUENCE_TASK(seqIndex)								
							TASK_VEHICLE_MISSION_PED_TARGET(NULL, sRivalPapVehicle.vehicle, PLAYER_PED_ID(), MISSION_FLEE, 80.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 500.0, -1.0)
						CLOSE_SEQUENCE_TASK(seqIndex)
						TASK_PERFORM_SEQUENCE(sRivalPapDriverPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
					ELSE
						OPEN_SEQUENCE_TASK(seqIndex)								
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 50.0, -1)	// lower from 100m to help with latency - shouldn't stop at 50 as game will have faded out by then
						CLOSE_SEQUENCE_TASK(seqIndex)
						TASK_PERFORM_SEQUENCE(sRivalPapDriverPed.ped, seqIndex)
						CLEAR_SEQUENCE_TASK(seqIndex)
					ENDIF
					SET_PED_KEEP_TASK(sRivalPapDriverPed.ped, TRUE)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "SET RIVAL DRIVER UP TO FLEE PLAYER IN FAILED SETUP	$$$") ENDIF #ENDIF
				ENDIF	
			ENDIF	
			SET_MUSIC_EVENT_FOR_TRIGGER(PAP1_MUSIC_EVENT_FAIL)
			// set the fail reason
			SWITCH eP1_MissionFailedReason
				CASE FAILED_DEFAULT
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Default") ENDIF #ENDIF
				BREAK
				CASE FAILED_CELEB_DIED		
					sFailReason = "PAP1_F1"		//~r~Miranda Died.~w~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Miranda DIED") ENDIF #ENDIF
				BREAK
				CASE FAILED_CELEB_ESCAPED
					sFailReason = "PAP1_F2"		//~r~Miranda Escaped.~w~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Miranda ESCAPED") ENDIF #ENDIF
				BREAK
				CASE FAILED_PLAYER_ATTACKED_CELEB
					sFailReason = "PAP1_F7"		//~w~Franklin attacked Miranda.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: You attacked Miranda") ENDIF #ENDIF
				BREAK
				/*CASE FAILED_PLAYER_ATTACKED_BEVERLY
					sFailReason = "PAP1_F8"		//~w~Beverly was injured.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: You attacked Miranda") ENDIF #ENDIF
				BREAK*/
				CASE FAILED_BEVERLY_VEHICLE_WRECKED		
					sFailReason = "PAP1_F3"		//~r~Beverly's Vehicle was Wrecked.~w~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: BEVERLY VEHICLE WAS WRECKED") ENDIF #ENDIF
				BREAK
				CASE FAILED_BEVERLY_DIED	
					sFailReason = "PAP1_F4"		//~r~Beverly Died.~w~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: BEVERLY DIED") ENDIF #ENDIF
				BREAK
				CASE FAILED_PAP_ESCAPED		
					sFailReason = "PAP1_F5"		//~w~The rival paparazzo escaped.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Marco ESCAPED") ENDIF #ENDIF
				BREAK
				CASE FAILED_LEFT_BEVERLY_BEHIND		
					sFailReason = "PAP1_F6"		//~w~Beverly was abandoned.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Beverly was left behind.") ENDIF #ENDIF
				BREAK
				CASE FAILED_BEVERLY_SPOOKED	
					sFailReason = "PAP1_F9"		//~w~Beverly was spooked.~s~
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Mission Failed: Beverly was spooked.") ENDIF #ENDIF
				BREAK
			ENDSWITCH	
			//Check if fail reason needs to be displayed
			IF eP1_MissionFailedReason = FAILED_DEFAULT
				RANDOM_CHARACTER_FAILED()
			ELSE
				RANDOM_CHARACTER_FAILED_WITH_REASON(sFailReason)
			ENDIF			
			eSubStage = SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP done")		
		BREAK		
		//	------------------------------------------
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//---- Do any specific cleanup here----
				CLEANUP_ALL_MISSION_ENTITIES(TRUE)	
				CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_CLEANUP done")
				Script_Cleanup()	
			ELSE
				// not finished fading out
				IF IS_PED_UNINJURED(sBeverlyPed.ped)
					// mission failed dialogue (if setup)
					IF NOT bDoneDialogue_MissionFailed
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						// pap 1 has no recorded flee dialogue and ambient speech isn't working  so using pap 2 dialogue
						IF eP1_MissionFailedReason = FAILED_BEVERLY_SPOOKED
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, "PAP2AUD", "PAP2_FLEE", CONV_PRIORITY_AMBIENT_HIGH, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())	// B*1521584 - needs to be this priorty as it's not from PAP1AUD text
								bDoneDialogue_MissionFailed = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - bDoneDialogue_MissionFailed - true") ENDIF #ENDIF
							ENDIF
							//STOP_PED_SPEAKING(sBeverlyPed.ped, FALSE)
							//PLAY_PED_AMBIENT_SPEECH(sBeverlyPed.ped, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_FORCE_NORMAL)
						ELSE
							IF PAP1_CREATE_CONVERSATION_WITH_MESSAGE_DISPLAYED_CHECK(sDialogue, sPap1_Dialogue_TextBlockName, tlDialogueRoot_MissionFailed, CONV_PRIORITY_MEDIUM, GET_SUBTITLES_STATE_FOR_NON_CLASH_WITH_MESSAGES())
								bDoneDialogue_MissionFailed = TRUE
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - bDoneDialogue_MissionFailed - true") ENDIF #ENDIF
							ENDIF
						ENDIF
					ENDIF	
					// having to keep checking this here now because of // B*1441191
					IF IS_PED_GROUP_MEMBER(sBeverlyPed.ped, PLAYER_GROUP_ID())
						IF NOT IS_PED_RAGDOLL(sBeverlyPed.ped)	// B*1441191 - apparently calls the REMOVE_PED_FROM_GROUP need to delay for ragdoll since it will clear immediately causing ped to snap up
							REMOVE_PED_FROM_GROUP(sBeverlyPed.ped)	
							CPRINTLN(DEBUG_MISSION, "STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_SETUP - *PED GROUP* BEV removed from group - STAGE_MISSION_FAILED_WAIT_FOR_FADE - SS_UPDATE")
						ENDIF
					ENDIF
					IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)	// need to keep clipset's updating otherwise you get an assert - B*846177
						SET_PED_RESET_FLAG(sBeverlyPed.ped, PRF_DisableInVehicleActions, TRUE)	// B*1480880 - stop bev clipping during reverse anims
						RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE BEV ON BIKE -  AnimSet_BeverlyCameraWeaponMovementOnBike- done") ENDIF #ENDIF	
					ELSE
						SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
						//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "MISSION_FAILED_WAIT_FOR_FADE BEV ON BIKE -  AnimSet_BeverlyCameraWeaponMovementOnBike- done") ENDIF #ENDIF	
					ENDIF
				ENDIF
			ENDIF
		BREAK		
	ENDSWITCH
ENDPROC

//-------------------------------------------------------------------------------------------------------------------------------------------------
//		DEBUG STAGES
//-------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	PROC DEBUG_STAGE_RECORD_CHASE_ROUTE()
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SWITCH eSubStage		
			//	-------------------------------------------------------------------------------------
	 		CASE SS_SETUP
				LOAD_MISSION_ASSETS(TRUE)
				CREATE_ENTITIES_FOR_MISSION_START()
				SET_CLOCK_TIME(10, 0, 0)				
				SAFE_REMOVE_PED(sCelebsDriverPed.ped, TRUE)
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					SET_VEHICLE_DOORS_LOCKED(sCelebVehicle.vehicle, VEHICLELOCK_UNLOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sCelebVehicle.vehicle, TRUE)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sCelebVehicle.vehicle)
					SET_VEHICLE_ENGINE_ON(sCelebVehicle.vehicle, TRUE, TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)					
					REQUEST_VEHICLE_RECORDING(1, sPap1_UberRecordingName)					
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, sPap1_UberRecordingName)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOADED THE VEHICLE RECORDING 1 PAP1U") ENDIF #ENDIF
						WAIT(0)
					ENDWHILE					
					INIT_UBER_RECORDING(sPap1_UberRecordingName)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					eSubStage = SS_UPDATE
				ENDIF
			BREAK			
			//	-------------------------------------------------------------------------------------
			CASE SS_UPDATE
				// for uber recording
				UPDATE_UBER_RECORDING()					
				//this is for when you have already made the recording without traffic...play back the recording and press start recording in the uber playback.
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
							START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)
							//SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, 0.85) was used to re-record the main car slower
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK OF RECORDING STARTED") ENDIF #ENDIF
						ENDIF
					ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)	// setup stop and reset
					ENDIF
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)	
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, RDM_WHOLELINE)
					ENDIF
				ENDIF				
			BREAK			
			//	-------------------------------------------------------------------------------------
			CASE SS_CLEANUP
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "RECORD CHASE ROUTE CLEANUP") ENDIF #ENDIF
			BREAK			
		ENDSWITCH		
	ENDPROC
	
	PROC DEBUG_STAGE_PLAYBACK_CHASE_ROUTE()		
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		INT iMainCarrecID = 1
		VECTOR vPos[4]
		FLOAT fTime
		SWITCH eSubStage		
			//	-------------------------------------------------------------------------------------
	 		CASE SS_SETUP			
				LOAD_MISSION_ASSETS(TRUE)						
				CREATE_ENTITIES_FOR_MISSION_START()				
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					//SAFE_REMOVE_PED(sCelebsDriverPed.ped, TRUE)
					IF IS_PED_UNINJURED(sCelebPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sCelebPed.ped, sCelebVehicle.vehicle, VS_FRONT_RIGHT)
						//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sCelebVehicle.vehicle, VS_DRIVER)
					ENDIF
					SET_ENTITY_COLLISION(sCelebVehicle.vehicle, FALSE)
				ENDIF
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, VS_DRIVER)
					IF IS_PED_UNINJURED(sBeverlyPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
					ENDIF
				ENDIF									
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()				
				//for uber playback
				INITIALISE_UBER_PLAYBACK(sPap1_UberRecordingName, iMainCarrecID)
				LOAD_UBER_DATA()
				SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
				SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(A_M_M_BEVHILLS_02)
				PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
				//switch_SetPieceCar_to_ai_on_collision = true
				traffic_block_vehicle_colour(true, traffic_black)				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
					START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, iMainCarrecID, sPap1_UberRecordingName)
					DISPLAY_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, RDM_WHOLELINE)					
					//444
					//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1000.0)
					//PAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
				ENDIF							
				/*
				// for single recording			
				REQUEST_MODEL(sRivalPapVehicle.model)				
				//the rival rec
				REQUEST_VEHICLE_RECORDING(1, "Pap1RP")				
				WHILE NOT HAS_MODEL_LOADED(sRivalPapVehicle.model)
				OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Pap1RP")
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "LOADED BIKE MODEL") ENDIF #ENDIF
					WAIT(0)
				ENDWHILE				
				IF NOT DOES_ENTITY_EXIST(sRivalPapVehicle.vehicle)
					sRivalPapVehicle.vehicle = CREATE_VEHICLE(sRivalPapVehicle.model, <<-1565.0560, -172.2702, 54.9314>>)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(sRivalPapVehicle.model)
				IF DOES_ENTITY_EXIST(sRivalPapVehicle.vehicle)
				AND NOT IS_ENTITY_DEAD(sRivalPapVehicle.vehicle)
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sRivalPapVehicle.vehicle)
				ENDIF				
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, iMainCarrecID, sPap1_UberRecordingName)
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, RDM_WHOLELINE)					
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 60550.0)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
					ENDIF
				ENDIF				
				IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, iMainCarrecID, "Pap1RP")
						DISPLAY_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, RDM_WHOLELINE)					
						//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, 60500.0)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
					ENDIF
				ENDIF
				*/
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE SETUP DONE") ENDIF #ENDIF
				eSubStage = SS_UPDATE
			BREAK			
			//	-------------------------------------------------------------------------------------
			CASE SS_UPDATE		
				SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)
				SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(0.0)
				//for uber playback
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						fTime = GET_TIME_POSITION_IN_RECORDING(sCelebVehicle.vehicle)
					
						bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = TRUE	//might help with some traffic failing to spawn?
						CREATE_ALL_WAITING_UBER_CARS()  //might help with some traffic failing to spawn?							
						PRELOAD_UBER_RECORDINGS_DURING_PLAYBACK()
						//CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(sCelebVehicle.vehicle, PLAYER_PED_ID(), fMainPlaybackSpeed, PAP1_DEFAULT_CHASE_SPEED_FOR_UBER_PLAYBACK, 5, 20, 100, 2, 1)
						SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, 1.0)
						UPDATE_UBER_PLAYBACK(sCelebVehicle.vehicle, 1.0)
						
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
						vPos[0] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iMainCarrecID, fTime + 500.0, sPap1_UberRecordingName)
						vPos[1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iMainCarrecID, fTime + 1000.0, sPap1_UberRecordingName)
						vPos[2] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, <<-1.7329, 6.4243, 0.1649>>)
						vPos[3] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, <<-1.7329, 12.4243, 0.1649>>)
						
						DRAW_DEBUG_SPHERE(vPos[0], 0.5, 0, 255, 0, 128)
						DRAW_DEBUG_SPHERE(vPos[1], 0.5, 0, 255, 0, 128)
						DRAW_DEBUG_SPHERE(vPos[2], 1.0, 0, 0, 255, 128)
						DRAW_DEBUG_SPHERE(vPos[3], 1.0, 0, 0, 255, 128)
						
						// quit early
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)	
							CLEANUP_UBER_PLAYBACK(TRUE)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE STOPPED so cleaned up and reset") ENDIF #ENDIF
							eSubStage = SS_SETUP
						ENDIF
					ELSE
						//for uber playback
						CLEANUP_UBER_PLAYBACK(TRUE)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE ENDED so c;eaned up") ENDIF #ENDIF
						eSubStage = SS_CLEANUP
					ENDIF
				ENDIF					
				/*
				// for single recording
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
				AND IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
						SET_TIME_SCALE(0.5)						
					ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
					ELIF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
							START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)
						ENDIF
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 60550.0)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle)
						
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
							START_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, 1, "Pap1RP")
						ENDIF
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle, 0.1)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(sRivalPapVehicle.vehicle)
					ENDIF					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sRivalPapVehicle.vehicle)
						fTempTime = GET_TIME_POSITION_IN_RECORDING(sRivalPapVehicle.vehicle)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "current rec time rival pap - ", fTempTime) ENDIF #ENDIF
					ENDIF
				ENDIF
				*/
			BREAK			
			//	-------------------------------------------------------------------------------------
			CASE SS_CLEANUP
				#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE - CLEANUP use d pad left to reset") ENDIF #ENDIF
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)	// setup stop and reset
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "PLAYBACK CHASE ROUTE CLEANUP reset recording") ENDIF #ENDIF
					eSubStage = SS_SETUP
				ENDIF
			BREAK			
		ENDSWITCH		
	ENDPROC	

	PROC DEBUG_STAGE_DEBUG_STATE()		
	 	SWITCH eSubStage			
			//	-------------------------------------------------------------------------------------
	 		CASE SS_SETUP		
				bDebug_PrintMissionInfoToTTY					= TRUE
				SET_DEBUG_ACTIVE(TRUE)
    			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)					
				LOAD_MISSION_ASSETS(TRUE)	
				CREATE_BEVERLYS_BIKE_FOR_MISSION_START()
				//// celeb vehicle from initial setup
				IF (DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[1]))
					DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
				ENDIF		
				CREATE_ENTITIES_FOR_MISSION_START()												
				//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sCelebVehicle.vehicle)				
				REQUEST_ANIM_DICT(sAnimDictName_Pap1HandOverride)
				WHILE NOT HAS_ANIM_DICT_LOADED(sAnimDictName_Pap1HandOverride)
					WAIT(0)
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "WAITING FOR ANIM DIC TO LOAD - rcmpaparazzo1IG_1") ENDIF #ENDIF
				ENDWHILE	
				/*
				IF DOES_ENTITY_EXIST(sObjCameraBeverly.object)
					SET_ENTITY_VISIBLE(sObjCameraBeverly.object, TRUE)
				ENDIF
				IF IS_ENTITY_ALIVE(sBeverlyPed.ped)
					SET_ENTITY_VISIBLE(sBeverlyPed.ped, TRUE)
				ENDIF
				SAFE_REMOVE_PED(sCelebsDriverPed.ped, TRUE)
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					IF IS_PED_UNINJURED(sCelebPed.ped)
						SAFE_PUT_PED_INTO_VEHICLE(sCelebPed.ped, sCelebVehicle.vehicle, VS_BACK_RIGHT)
					ENDIF
					//SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sCelebVehicle.vehicle,  VS_DRIVER)					
				ENDIF
				//SAFE_TELEPORT_ENTITY(sCelebVehicle.vehicle, << -156.6, 260.1, 93.0 >>, 263.0)
				*/					
				//SAFE_TELEPORT_PED(PLAYER_PED_ID(), << -155.8644, 277.6098, 92.7736 >>, 103.3885, TRUE)					
				//SAFE_TELEPORT_PED(sBeverlyPed.ped, << -166.5576, 275.3646, 92.3437 >>, 273.0, TRUE)			
				IF IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
				AND IS_PED_UNINJURED(sBeverlyPed.ped)
					CLEAR_PED_TASKS(sBeverlyPed.ped)
					SET_BEVERLY_FOLLOW_PLAYER()
					SAFE_PUT_PED_INTO_VEHICLE(PLAYER_PED_ID(), sBeverlyVehicle.vehicle, VS_DRIVER) 
					SAFE_PUT_PED_INTO_VEHICLE(sBeverlyPed.ped, sBeverlyVehicle.vehicle, VS_FRONT_RIGHT)
					//IF IS_PED_UNINJURED(sBeverlyPed.ped)
					//	SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
					//ENDIF
					SET_VEHICLE_ENGINE_ON(sBeverlyVehicle.vehicle, TRUE, TRUE)
					//GIVE_WEAPON_TO_PED(sBeverlyPed.ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)	//giving him this instead as camera weapon doesn't work!
					//SET_PED_CURRENT_WEAPON_VISIBLE(sBeverlyPed.ped, TRUE)	
					//SET_PED_CAN_SWITCH_WEAPON(sBeverlyPed.ped, FALSE)
					//SET_PED_TARGET_LOSS_RESPONSE(sBeverlyPed.ped, TLR_NEVER_LOSE_TARGET)
					//SET_PED_COMBAT_ATTRIBUTES(sBeverlyPed.ped, CA_DO_DRIVEBYS, FALSE)	// need to be false otherwise they clash!
					//SET_PED_SHOOT_RATE(sBeverlyPed.ped, 100)
					//SET_PED_FIRING_PATTERN(sBeverlyPed.ped, FIRING_PATTERN_FULL_AUTO)
					//SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, "RCM_Paparazzo")
					//SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_ForcedAim, TRUE)
					//SET_PED_CONFIG_FLAG(sBeverlyPed.ped, PCF_IsAimingGun, TRUE)						
					//TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)	
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet- done") ENDIF #ENDIF							
				ENDIF					
				/*
				IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
					SET_VEHICLE_DOORS_LOCKED(sCelebVehicle.vehicle, VEHICLELOCK_UNLOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sCelebVehicle.vehicle, TRUE)
				ENDIF
				*/								
				LOAD_ASSETS_FOR_RIVAL_PAP_ARRIVING(TRUE)
				CREATE_RIVAL_PAP_FOR_ENTRANCE()
				IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					FREEZE_ENTITY_POSITION(sRivalPapVehicle.vehicle, TRUE)
				ENDIF
				IF IS_PED_UNINJURED(sRivalPapPed.ped)
				AND NOT DOES_BLIP_EXIST(sRivalPapPed.blip)
					sRivalPapPed.blip = CREATE_PED_BLIP(sRivalPapPed.ped, TRUE, FALSE)
					SET_BLIP_SCALE(sRivalPapPed.blip, 1.0)	//TEMP: cars need bigger blips but the new rc_helpers_function - CREATE_PED_BLIP doesn't handle it.
				ENDIF
				IF IS_PED_UNINJURED(sRivalPapPed.ped)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapPed.ped, KNOCKOFFVEHICLE_DEFAULT)
				ENDIF
				IF IS_PED_UNINJURED(sRivalPapDriverPed.ped)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sRivalPapDriverPed.ped, KNOCKOFFVEHICLE_DEFAULT)
				ENDIF
				SAFE_TELEPORT_ENTITY(sRivalPapVehicle.vehicle, << -156.6, 260.1, 93.0 >>, 263.0)						
				/*IF IS_VEHICLE_OK(sCelebVehicle.vehicle)
				AND IS_PED_UNINJURED(sCelebsDriverPed.ped)
				AND IS_PED_UNINJURED(sCelebPed.ped)			
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)
						START_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, 1, sPap1_UberRecordingName)
						#IF IS_DEBUG_BUILD	DISPLAY_PLAYBACK_RECORDED_VEHICLE(sCelebVehicle.vehicle, RDM_WHOLELINE)	#ENDIF												
						fMainPlaybackSpeed = 0.5
						SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
					ENDIF				
				ENDIF*/				
				SAFE_FADE_SCREEN_IN_FROM_BLACK(0, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				eSubStage = SS_UPDATE					
			BREAK			
			//	-------------------------------------------------------------------------------------
			CASE SS_UPDATE
				IF bDebug_TakeOffHelmet
					REMOVE_PLAYER_HELMET(PLAYER_ID(), FALSE)
					SET_PED_HELMET(PLAYER_PED_ID(), FALSE)
					bDebug_TakeOffHelmet = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Frank bDebug_TakeOffHelmet  frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				IF bDebug_GiveHelmet
					GIVE_PED_HELMET(PLAYER_PED_ID())
					SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
					bDebug_GiveHelmet = FALSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Frank bDebug_GiveHelmet  frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				/*
				IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_CRASH_HELMET)	
				AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P1_BIKE_HELMET)	// P1 = franklin
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Frank doesn't have helmet on  frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ELSE
					#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "Frank HAS helmet on  frame count = ", GET_FRAME_COUNT()) ENDIF #ENDIF
				ENDIF
				*/	
				//MANAGE_PED_SHOOTING_UP(sCelebPed.ped)				
				IF IS_PED_UNINJURED(sBeverlyPed.ped)
				AND IS_VEHICLE_OK(sBeverlyVehicle.vehicle)
					//MANAGE_BEVERLY_TAKING_PHOTOS_DURING_CHASE()
					//MANAGE_DIALOGUE_DURING_CHASE_CELEB()
					//MANAGE_PED_DOING_KNOCKOFF_BIKER_ATTACK(sBeverlyPed.ped, sRivalPapVehicle.vehicle, sObjCameraBeverly.object)		
					
					IF IS_VEHICLE_OK(sRivalPapVehicle.vehicle)
					AND IS_PED_UNINJURED(sRivalPapDriverPed.ped)
					AND IS_PED_SITTING_IN_VEHICLE(sRivalPapDriverPed.ped, sRivalPapVehicle.vehicle)
					AND IS_PED_UNINJURED(sRivalPapPed.ped)
					AND IS_PED_SITTING_IN_VEHICLE(sRivalPapPed.ped, sRivalPapVehicle.vehicle)
						IF bDebug_TogglePapDriveWander
							MONITOR_RIVAL_PAP_CHASE_SPEED()
						ENDIF
						/*VECTOR vBev_Forward, vRivalPap_ToBeverly
						vBev_Forward = GET_ENTITY_FORWARD_VECTOR(sBeverlyPed.ped)
						vBev_Forward = NORMALISE_VECTOR(vBev_Forward)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "vBev_Forward = ", vBev_Forward) ENDIF #ENDIF
						
						vRivalPap_ToBeverly = GET_ENTITY_COORDS(sRivalPapPed.ped) - GET_ENTITY_COORDS(sBeverlyPed.ped)
						vRivalPap_ToBeverly = NORMALISE_VECTOR(vRivalPap_ToBeverly)
						
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "vRivalPap_ToBeverly = ", vRivalPap_ToBeverly) ENDIF #ENDIF
						FLOAT fAngle
						fAngle = DOT_PRODUCT(vBev_Forward, vRivalPap_ToBeverly)
						//fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vBev_Forward.X, vBev_Forward.Y, vRivalPap_ToPlayer.X, vRivalPap_ToPlayer.Y)
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "GET_ANGLE_BETWEEN_2D_VECTORS bev and rival pa : fAngle = ", fAngle) ENDIF #ENDIF*/
						
						//IF HAS_TIME_PASSED(iTimerDelay_RivalPapGestureAnim, GET_RANDOM_INT_IN_RANGE(6000, 10000))
						//	fDistance_PlayerToRivalPap = 10.0
						//	IF fDistance_PlayerToRivalPap > 5.0
						//	AND fDistance_PlayerToRivalPap < 25.0
								//IF SET_RIVAL_PAP_PLAY_GESTURE_ANIM()
									//iTimerDelay_RivalPapGestureAnim = GET_GAME_TIMER()
								//ENDIF
						//	ENDIF
						//ENDIF	
						//GET_SIDE_COORDS_IS_TO_ENTITY(sBeverlyVehicle.vehicle, GET_ENTITY_COORDS(sBeverlyVehicle.vehicle), GET_ENTITY_COORDS(sRivalPapPed.ped))
						//SET_BEVERLY_PLAY_POINT_ANIM(sRivalPapPed.ped)
						//SET_BEVERLY_PLAY_HURRY_UP_ANIM_TOWARDS_ENTITY(sRivalPapPed.ped)
					ENDIF
					IF IS_PED_ON_ANY_BIKE(sBeverlyPed.ped)		
						//SET_CURRENT_PED_WEAPON(sBeverlyPed.ped, WEAPONTYPE_PISTOL, TRUE)
						//RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
					ENDIF					
					IF IS_PED_UNINJURED(sCelebPed.ped)
					AND IS_VEHICLE_OK(sCelebVehicle.vehicle)
					AND IS_PED_UNINJURED(sCelebsDriverPed.ped)						
						IF bDebug_BevDoAimCameraAtCelebTask
							#IF IS_DEBUG_BUILD 
								IF bDebug_OverrideCelebPosInCarOffset
									vCelebPosInCarOffset = vDebug_vCelebPosInCarOffsetOverride
								ENDIF
							#ENDIF
							VECTOR vCelebPos
							vCelebPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, vCelebPosInCarOffset)								
							#IF IS_DEBUG_BUILD 
								IF bDebug_BevTempAimLeft
									vCelebPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, << -8.0, 0.0, 0.0>>)	
								ELSE
									//vCelebPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sCelebVehicle.vehicle, << 8.0, 0.0, 0.0>>)	
								ENDIF
							#ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_DisplayShapeTestPap1CameraAim DRAW_DEBUG_SPHERE(vCelebPos, fDebug_ProbeHitDistOverride, 0, 225, 225, 150) ENDIF #ENDIF
							IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)	// SCRIPT_TASK_AIM_GUN_SCRIPTED)	// SCRIPT_TASK_DRIVE_BY)
								//IF Playing anim, stop anim task
								//TASK_AIM_GUN_SCRIPTED_WITH_TARGET(sBeverlyPed.ped, sCelebPed.ped, << 0.0, 0.0, 0.0 >>, SCRIPTED_GUN_TASK_BIKE)
								//CLEAR_PED_TASKS(sBeverlyPed.ped)
								FLOAT fTaskSweepAim_TurnRate
								fTaskSweepAim_TurnRate = 3.0 	// default is 1.0, 2.0 would be twice as fast, etc
								#IF IS_DEBUG_BUILD IF bDebug_OverrideTaskSweepAim_TurnRate fTaskSweepAim_TurnRate = fDebug_TaskSweepAim_TurnRate ENDIF #ENDIF
								TASK_SWEEP_AIM_POSITION(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "sweep_low", "sweep_med", "sweep_high", -1, vCelebPos, fTaskSweepAim_TurnRate)
								RESET_IS_BEVERLY_AIMING_AT_CELEB_TEST()
								iTimerDelay_Progress_BevTakingPhotos = GET_GAME_TIMER()
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_BevDoAimCameraAtCelebTask - TASK_SWEEP_AIM_POSITION : fTaskSweepAim_TurnRate = ", fTaskSweepAim_TurnRate) ENDIF #ENDIF
							ELSE
								UPDATE_TASK_SWEEP_AIM_POSITION(sBeverlyPed.ped, vCelebPos)
								
								IF HAS_TIME_PASSED(iTimerDelay_Progress_BevTakingPhotos, 1000)
									IF IS_BEVERLY_AIMING_CAMERA_AT_CELEB(vCelebPos)
										INT iAmountTimeSteadyCameraForShot
										iAmountTimeSteadyCameraForShot = 500
										IF HAS_TIME_PASSED_BEV_CONSECUTIVELY_AIMED_CAMERA_AT_CELEB(iAmountTimeSteadyCameraForShot)
											#IF IS_DEBUG_BUILD IF bDebug_DisplayShapeTestPap1CameraAim DRAW_RECT(0.3, 0.5, 0.2, 0.4, 0, 255, 0, 150) ENDIF #ENDIF	
											#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "IS_BEVERLY_AIMING_CAMERA_AT_CELEB - bev has been consecutively aiming for at least this period of time = ", iAmountTimeSteadyCameraForShot) ENDIF #ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_GENERAL_SWEEP)
								CLEAR_PED_TASKS(sBeverlyPed.ped)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_BevDoAimCameraAtCelebTask - TASK_SWEEP_AIM_ENTITY CLEARED") ENDIF #ENDIF
							ENDIF
						//	IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_2", "idle")
						//		CLEAR_PED_TASKS(sBeverlyPed.ped)
						//		TASK_PLAY_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_2", "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
						//	ENDIF
						//	IF NOT IsPedPerformingTask(sBeverlyPed.ped, SCRIPT_TASK_PLAY_ANIM)	// SCRIPT_TASK_AIM_GUN_SCRIPTED)	 //  SCRIPT_TASK_DRIVE_BY)
						//		//CLEAR_PED_TASKS(sBeverlyPed.ped)
						//		TASK_PLAY_ANIM(sBeverlyPed.ped, "rcmpaparazzo1ig_2", "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
						//		#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_BevDoAimCameraAtCelebTask - on bike idle anim applied") ENDIF #ENDIF
						//	ENDIF
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(sCelebVehicle.vehicle)		
							fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(sCelebVehicle.vehicle)
							//#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "CURRENT PLAYBACK TIME = ", fCurrentPlaybackTime) ENDIF #ENDIF
							fCurrentChaseDistanceCeleb = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sCelebVehicle.vehicle))			
							fMainPlaybackSpeed = 0.55
							//UPDATE_CELEB_CHASE_SPEED(fMainPlaybackSpeed)										
							SET_PLAYBACK_SPEED(sCelebVehicle.vehicle, fMainPlaybackSpeed)
						ENDIF
					ENDIF					
					IF bDebug_ApplyBevWeaponMovementClipSet
						IF IS_PED_SITTING_IN_ANY_VEHICLE(sBeverlyPed.ped)
							RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
							IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
								STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_OUT)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet ON BIKE - stopped  cam override anim") ENDIF #ENDIF
							ENDIF
							IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
								TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet ON BIKE - applied idle on bike anim") ENDIF #ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet ON BIKE -  AnimSet_BeverlyCameraWeaponMovementOnBike- done") ENDIF #ENDIF	
						ELSE
							IF IS_PED_GETTING_INTO_A_VEHICLE(sBeverlyPed.ped)
								IF NOT IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
									RESET_PED_MOVEMENT_CLIPSET(sBeverlyPed.ped)
									//TASK_PLAY_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY | AF_LOOPING)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet GETTING ON BIKE  - cam_pos_hand_override wasn't playing so NOT applied") ENDIF #ENDIF
								ENDIF
							ELSE
								SET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped, sAnimSet_BeverlyCameraWeaponMovementOnFoot)
								#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet OFF BIKE  - sAnimSet_BeverlyCameraWeaponMovementOnFoot- done") ENDIF #ENDIF	
								IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
									STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", REALLY_SLOW_BLEND_OUT)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet OFF BIKE  - stopped idle on bike anim") ENDIF #ENDIF
								ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
									STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", REALLY_SLOW_BLEND_OUT)
									#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ApplyBevWeaponMovementClipSet OFF BIKE - stopped  cam override anim") ENDIF #ENDIF
								ENDIF								
							ENDIF
						ENDIF
						//bDebug_ApplyBevWeaponMovementClipSet = FALSE
					ENDIF
					IF bDebug_ResetBevWeaponMovementClipSet	
						RESET_PED_WEAPON_MOVEMENT_CLIPSET(sBeverlyPed.ped)
						IF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle")					
							STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1Specific, "idle", REALLY_SLOW_BLEND_OUT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ResetBevWeaponMovementClipSet  - stopped idle on bike anim") ENDIF #ENDIF
						ELIF IS_ENTITY_PLAYING_ANIM(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override")
							STOP_ANIM_TASK(sBeverlyPed.ped, sAnimDictName_Pap1HandOverride, "cam_pos_hand_override", REALLY_SLOW_BLEND_OUT)
							#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ResetBevWeaponMovementClipSet - stopped  cam override anim") ENDIF #ENDIF
						ENDIF
						bDebug_ResetBevWeaponMovementClipSet = FALSE
						#IF IS_DEBUG_BUILD IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "bDebug_ResetBevWeaponMovementClipSet - done") ENDIF #ENDIF				
					ENDIF
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				ENDIF	
			BREAK
			//	-------------------------------------------------------------------------------------
			CASE SS_CLEANUP		
			BREAK			
		ENDSWITCH		
	ENDPROC
#ENDIF

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)	

	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF	
	
	/*IF IS_REPLAY_IN_PROGRESS() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		//set the initial scene back up
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_PAPARAZZO_1(sRCLauncherDataLocal)	
			CPRINTLN(DEBUG_MISSION, " IS_REPLAY_IN_PROGRESS - waiting on SetupScene_PAPARAZZO_1")
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		RC_TakeEntityOwnership(sRCLauncherDataLocal)
		SETUP_AREA_FOR_MISSION(RC_PAPARAZZO_1, FALSE)	// need to turn this off in this instance since launcher cleanup won't get called to do it
		g_bSceneAutoTrigger = FALSE
	ENDIF*/

	INIT_MISSION()
	
	IF IS_REPLAY_IN_PROGRESS()	// handle replay checkpoints		
		INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			iReplayStage++ // player is skipping this stage
		ENDIF		
		SWITCH iReplayStage
			CASE CP_PAP1_CHASE_CELEB_IN_VEHICLE
				START_REPLAY_SETUP(<<-152.3902, 279.5714, 92.8134>>, 106.6662, FALSE)	// match mocap finished position and heading
				bDoneDialogue_InitialGetOnBike = TRUE	//don't play this dialogue on a replay as we want to start with the objective displayed
				DO_Z_SKIP(Z_SKIP_CHASE_CELEB_IN_VEHICLE)	 // skip the mocap intro
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "start chase in progress")
			BREAK
			CASE CP_PAP1_STOP_RIVAL_PAP
				START_REPLAY_SETUP(<< -1736.5334, -508.0815, 37.9800 >>, 192.10, FALSE)
				DO_Z_SKIP(Z_SKIP_STOP_RIVAL_PAP)
				iGoodPhotosTakenCounter = g_replay.iReplayInt[REPLAY_PHOTOS_TAKEN] //set from the global value from the previous play through
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "stop rival pap in progress")
			BREAK
			CASE CP_PAP1_DROP_BEVERLY_OFF
				START_REPLAY_SETUP(<<-1586.6477, -533.9604, 34.4623>>, 307.1855, FALSE)
				DO_Z_SKIP(Z_SKIP_DROP_BEVERLY_OFF)
				bDoneDialogue_RivalPapCameraDestroyed = TRUE	//set this dialogue to have already played, because they want the skip to be at the objective instead
				iGoodPhotosTakenCounter = g_replay.iReplayInt[REPLAY_PHOTOS_TAKEN] //set from the global value from the previous play through
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "drop bev off in progress")
			BREAK	
			CASE CP_PAP1_MISSION_PASSED
				START_REPLAY_SETUP(vDropOffPoint, 221.0758, FALSE)
				DO_Z_SKIP(Z_SKIP_OUTRO)	 
				iGoodPhotosTakenCounter = g_replay.iReplayInt[REPLAY_PHOTOS_TAKEN] //set from the global value from the previous play through
				CPRINTLN(DEBUG_MISSION, "* Replay checkpoint * - ", "mission passed in progress (show outro mocap first)")
			BREAK
			DEFAULT
				 SCRIPT_ASSERT("RC - PAP 1 :  Replay checkpoint * - starting mission from invalid checkpoint")
			BREAK
		ENDSWITCH
	ENDIF	
	
	//#IF IS_DEBUG_BUILD	SET_PROFILING_OF_THIS_SCRIPT(TRUE)	#ENDIF
	WHILE(TRUE)	// Loop within here until the mission passes or fails
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_P")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
		
			MISSION_FAILED_CHECKS()
			CHECK_PLAYER_USED_SPECIAL_ABILITY()
			
			// Fix B*1121745 - swap over to call everyframe commands
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			SWITCH eMissionStage				
				CASE MISSION_STAGE_INTRO_MOCAP_SCENE
					STAGE_INTRO_MOCAP_SCENE()
				BREAK					
				CASE MISSION_STAGE_CHASE_CELEB_IN_VEHICLE
					STAGE_CHASE_CELEB_IN_VEHICLE()
				BREAK				
				CASE MISSION_STAGE_RIVAL_PAP_JOIN_CHASE
					STAGE_RIVAL_PAP_JOIN_CHASE()
				BREAK				
				CASE MISSION_STAGE_STOP_RIVAL_PAP
					STAGE_STOP_RIVAL_PAP()
				BREAK				
				CASE MISSION_STAGE_DROP_BEVERLY_OFF
					STAGE_DROP_BEVERLY_OFF()
				BREAK				
				CASE MISSION_STAGE_LOSE_THE_COPS
					STAGE_LOSE_THE_COPS()
				BREAK				
				CASE MISSION_STAGE_END_CUTSCENE
					STAGE_END_CUTSCENE_MOCAP()
				BREAK	
				CASE MISSION_STAGE_MISSION_PASSED
					Script_Passed()
				BREAK
				CASE MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
					STAGE_MISSION_FAILED_WAIT_FOR_FADE()
				BREAK						
					CASE MISSION_STAGE_DEBUG_RECORD_CHASE_ROUTE		// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_STAGE_RECORD_CHASE_ROUTE()
						#ENDIF
					BREAK				
					CASE MISSION_STAGE_DEBUG_PLAYBACK_CHASE_ROUTE		// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_STAGE_PLAYBACK_CHASE_ROUTE()
						#ENDIF
					BREAK											
					CASE MISSION_STAGE_DEBUG							// debug stage
						#IF IS_DEBUG_BUILD
							DEBUG_STAGE_DEBUG_STATE()
						#ENDIF
					BREAK				
			ENDSWITCH
		ENDIF	
		// if we are skipping through the mission stages, for a checkpoint / debug skip	
		IF bFinishedStageSkipping = FALSE			
			JUMP_TO_STAGE(eMissionSkipTargetStage)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MAINTAIN_MISSION_WIDGETS()	
			//This is here so it can use the debug skipping functions
			IF bDebug_SetupMission_DebugStage
				RC_START_Z_SKIP(TRUE, FALSE)
				RESET_MISSION()
				SET_STAGE(MISSION_STAGE_DEBUG)
				RC_END_Z_SKIP()
				IF bDebug_PrintMissionInfoToTTY CPRINTLN(DEBUG_MISSION, "DEBUG - mission setup in debug stage.") ENDIF
				bDebug_SetupMission_DebugStage = FALSE
			ENDIF
			
			// not skipping stages, check for debug keys as long as we aren't in fail state		
		  	IF bFinishedStageSkipping = TRUE				
				IF eMissionStage <> MISSION_STAGE_MISSION_FAILED_WAIT_FOR_FADE
		        	DEBUG_Check_Debug_Keys()
				ENDIF
			ENDIF
        #ENDIF	
		WAIT(0)
	ENDWHILE
ENDSCRIPT
