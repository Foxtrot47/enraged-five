// ====================================================
// This section not needed in a Random Character script

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
// End of section that is not needed
// ====================================================


// ****************************
// TEMPLATE SCRIPT STARTS HERE
// ****************************
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	RC_Template.sc
//		AUTHOR			:	Keith
//		DESCRIPTION		:	A Random Character template script (active in debug only).
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// ====================================================
// This section not needed in a Random Character script
CONST_FLOAT TERMINATION_RANGE	200.0 //(It should use brain range)

VECTOR m_characterLocation = << 332.8624, 6603.7827, 28.1753 >>
PED_INDEX m_theRandomChar = NULL
// End of section that is not needed
// ====================================================




// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Script_Cleanup()

	PRINTSTRING("...Random Character Template Script Cleanup") PRINTNL()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		PRINTSTRING("...Random Character Template Script was triggered so additional cleanup required") PRINTNL()
	ENDIF
	
	// Delete the Character if it has been created
	IF (DOES_ENTITY_EXIST(m_theRandomChar))
		DELETE_PED(m_theRandomChar)
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

PROC Script_Passed()

	PRINTSTRING("...Random Character Template Script Passed") PRINTNL()
	
	Random_Character_Passed()
	Script_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

PROC Script_Failed()

	PRINTSTRING("...Random Character Template Script Failed") PRINTNL()
	
	Random_Character_Failed()
	Script_Cleanup()
	
ENDPROC




// ===========================================================================================================
//		DEBUG
// ===========================================================================================================

// ====================================================
// This section not needed in a Random Character script
PROC DEBUG_Display_Screen_Text()

	// Display 'Random Character Template Script'
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_SCALE(0.50, 0.6)
	SET_TEXT_WRAP(0.0, 1.0)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.65, "STRING", "Random Character Template")
	
	// Display Pass and Fail instructions
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_SCALE(0.4, 0.45)
	SET_TEXT_WRAP(0.0, 1.0)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.7, "STRING", "Press 'S' to Pass - Press 'F' to Fail")
	
ENDPROC
// End of section that is not needed
// ====================================================

// PURPOSE:	Check for Forced Pass or Fail
PROC DEBUG_Check_Debug_Keys()

	// Check for Pass
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		Script_Passed()
	ENDIF

	// Check for Fail
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		Script_Failed()
	ENDIF
		
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Check for Script Specific Launch Restrictions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there are any script-specific reasons that would prevent this script from running now
//
// RETURN VALUE:	BOOL		TRUE if there is a restriction, FALSE if the script is okay to continue
FUNC BOOL Is_There_A_Script_Specific_Restriction()

	// EXAMPLES OF SCRIPT-SPECIFIC RESTRICTIONS
	//		Can the script only launch between specific hours of the day?
	//		Can only specific player characters launch the script?
	
	// There are no restrictions
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Random Character Template Script Launched") PRINTNL()

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Random Character Template Script Force Cleanup") PRINTNL()
		Script_Cleanup()
	ENDIF

	// Random Character scripts will be attached to World Points so will always get launched by code.
	// STEP ONE: Check if there are any script-specific reasons that should prevent the script running now.
	IF (Is_There_A_Script_Specific_Restriction())
		// ...there is a script-specific restriction, so the script should terminate
		PRINTSTRING("...Random Character Template Script-Specific Restriction, so Terminating") PRINTNL()
		Script_Cleanup()
	ENDIF
	
	// STEP TWO: Check with the Random Character Controller to see if this script is allowed to launch
	// NOTE: There are many reasons why permission will be denied. This Random Character may not yet
	//			be active in the game. Or this may be the second mission for this character but the
	//			first mission may not yet be passed. Or perhaps the first mission has been passed but
	//			there is a delay before the second mission is allowed. Or perhaps the player is already
	//			performing another mission or activity. Etc.
	IF NOT (Random_Character_Is_Allowed_To_Launch())
		// ...the random character controller has denied permission for this script to run, so terminate
		PRINTSTRING("...Random Character Template Script denied by Random Character Controller, so Terminating") PRINTNL()
		Script_Cleanup()
	ENDIF
	
	// STEP THREE: The script is allowed to launch, so request and display the Random Character model
	// NOTE: I'll use Lazlow
	WHILE NOT (CREATE_NPC_PED_ON_FOOT(m_theRandomChar, CHAR_LAZLOW, m_characterLocation, 209.1805))
		WAIT(0)
	ENDWHILE

	// ====================================================
	// This section not needed in a Random Character script
	// Warp the player near the template location if far away
	IF (IS_PLAYER_PLAYING(PLAYER_ID()))
		IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
			IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), m_characterLocation) > TERMINATION_RANGE)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 398.3480, 6596.1011, 27.4711 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 81.3965)
			ENDIF
		ENDIF
	ENDIF
	// End of section that is not needed
	// ====================================================
		
	// STEP FOUR: Loop to check for the player getting close enough to the character to play the mission.
	//				Also need to make sure the game conditions still allow the random character to be available.
	//				Also need to terminate if the player goes out of range of the random character.
	WHILE (TRUE)
		WAIT(0)
		
		// STEP FIVE: Check if the random character is still alive
		IF (IS_PED_INJURED(m_theRandomChar))
			// ...random character is injured, so cleanup
			PRINTSTRING("...Random Character Template Script terminating - random char is injured") PRINTNL()
			Script_Cleanup()
		ENDIF
		
		// STEP SIX: Check if the player is still in range of the random character
		IF (IS_PLAYER_PLAYING(PLAYER_ID()))
			IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), m_characterLocation) > TERMINATION_RANGE)
// from commands_brains.sch				IF NOT (IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE())				
					// ...player is out of range of random character, so cleanup
					PRINTSTRING("...Random Character Template Script terminating - player moved out of range") PRINTNL()
					Script_Cleanup()
				ENDIF
			ENDIF
		ENDIF
		
		// STEP SEVEN: Check with the flow controller to make sure the Random Character script is still allowed to
		//				check for activation. This will return FALSE if, for example, the player starts a mission.
		IF (Random_Character_Should_Terminate())
			// ...Random Character Controller has requested termination, so cleanup
			PRINTSTRING("...Random Character Template Script terminating - Random Character Controller requested termination") PRINTNL()
			Script_Cleanup()
		ENDIF
		
		// STEP EIGHT: Check if the player is within activation range of the Random Character mission
		// NOTE:		Once inside activation range the mission will stay within a mini mission loop
		//					until it terminates.
		IF (IS_PLAYER_PLAYING(PLAYER_ID()))
			IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), m_characterLocation) < RANDOMCHAR_ACTIVATE_MISSION_RANGE)
					// ...player is in activation range of the mission
					
					// STEP NINE: Need to stay in this loop until the mission is given permission to run
					// NOTE:		After first asking permission to run, a decision is usually made in a few frames time
					WHILE NOT (Random_Character_Request_Permission_To_Run())
						// Keep checking if the script should terminate
						IF (Random_Character_Should_Terminate())
							// ...Random Character Controller has requested termination, so cleanup
							PRINTSTRING("...Random Character Template Script terminating - Random Character Controller denied request to run") PRINTNL()
							Script_Cleanup()
						ENDIF
						
						WAIT(0)
					ENDWHILE
					
					// STEP TEN: The Random Character has been successfully met and the mission can run
					// NOTE:		It must run now until pass or fail.
					SET_MISSION_FLAG(TRUE)
					
					WHILE(TRUE)
						WAIT(0)
						
						// At this stage the mission should be running
						
						// ====================================================
						// This section not needed in a Random Character script
						DEBUG_Display_Screen_Text()
						// End of section that is not needed
						// ====================================================
						
						DEBUG_Check_Debug_Keys()
					ENDWHILE
				ENDIF
			ENDIF
		ENDIF		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


// ====================================================
// This section not needed in a Random Character script
#ENDIF	// IS_DEBUG_BUILD
// End of section that is not needed
// ====================================================
