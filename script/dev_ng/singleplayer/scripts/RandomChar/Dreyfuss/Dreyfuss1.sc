
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"

USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "randomChar_public.sch"
USING "RC_helper_functions.sch"
USING "script_player.sch"
USING "chase_hint_cam.sch"                            
USING "initial_scenes_Dreyfuss.sch"
USING "script_blips.sch"
USING "RC_Threat_public.sch"
USING "achievement_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch" 
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Dreyfuss1.sc
//		AUTHOR			:	Joanna Wright
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:ENUMS:
//-------------------------------------------------------------------------------------------------------------------------------------------------

ENUM MISSION_STAGE
	MS_INTRO,
	MS_CONFRONT_DREY,
	MS_DREY_DRIVES_OFF,
	MS_DREY_ABANDONED_CAR,
	MS_PASSED,
	MS_FAILED
ENDENUM

/// each mission stage has uses these substages
ENUM SUB_STAGE
	SS_SETUP,
	SS_UPDATE,
	SS_CLEANUP
ENDENUM

//-------------------------------------------------------------------------------------------------------------------------------------------------
//	:STRUCTS:
//-------------------------------------------------------------------------------------------------------------------------------------------------

STRUCT ANGLED_AREA
	VECTOR vec1
	VECTOR vec2
	FLOAT areaWidth
ENDSTRUCT

//****************************************************************************************************************************
//													:MISSION FLOW VARIABLES:
//****************************************************************************************************************************

CONST_INT CP_KILL_DREYFUSS 0

ANGLED_AREA areaNearDreyfussHouse
MISSION_STAGE eMissionStage = MS_INTRO  //track what MISSION stage we are at
SUB_STAGE eState = SS_SETUP
CONST_FLOAT fDreyfussEscapeDist 250.0
CONST_FLOAT normalDreyfussVehSpeed 15.0
CONST_FLOAT fastDreyfussVehSpeed 90.0
CONST_INT STUN_TIME 500

CONST_INT DREYFUSS	0

PED_INDEX pedDreyfuss

BLIP_INDEX biGOTO
INT iBlendRatioTimer = 0

CONST_INT CP_AFTER_MOCAP 0	//checkpoint

SEQUENCE_INDEX seqEscapePlanA //get into vehicle, drive away
SEQUENCE_INDEX seqAbandonCar //get out of vehicle, flee on foot

VEHICLE_INDEX playerVeh //an optional Player's vehicle
MODEL_NAMES playerVehModel = DOMINATOR
MODEL_NAMES dreyfussVehModel = LANDSTALKER
STRING sSceneHandleDreyfuss = "Dreyfuss"

structPedsForConversation sDialogue
INT speedZone

//INT DreyVehHealth = 400
//FLOAT DreyVehEngineHealth  = 400
//FLOAT DreyVehPetrolTankHealth = 500

BOOL rolling_start = TRUE
BOOL bAggravatedDrefuss = FALSE
BOOL bEscapedDriveway = FALSE

FLOAT fYellRange = 25 //how close the Player and Dreyfuss have to be to yell at each other during foot chase

CONST_INT MAX_LINES_CONV00 5
CONST_INT MAX_CAR_CHASE_LINES 31
CONST_INT MAX_RAM_LINES 7
CONST_INT MAX_LINES_CONV01  32
CONST_INT MAX_DREY_SHOT_AT_LINES 3
CONST_INT MAX_DREY_HURT_LINES 4
STRING specLbl00[MAX_LINES_CONV00]
STRING carChaseLines[MAX_CAR_CHASE_LINES]
STRING ramLines[MAX_RAM_LINES]
STRING specLbl01[MAX_LINES_CONV01]
STRING shotAtLines[MAX_DREY_SHOT_AT_LINES]
STRING hurtLines[MAX_DREY_HURT_LINES]
STRING abandonCarRootLabel = "DREY1_WRECK"

//whether conversations have played
BOOL bConv00Played[MAX_LINES_CONV00]
BOOL bCarChaseConv[MAX_CAR_CHASE_LINES]
BOOL bRamConv[MAX_RAM_LINES] //when ramming Dreyfuss
BOOL bConv01Played[MAX_LINES_CONV01]
BOOL bShotAtConvPlayed[MAX_DREY_SHOT_AT_LINES]
BOOL bCarWreckConv = FALSE
BOOL bEndConv = FALSE
BOOL bHurtConvPlayed[MAX_DREY_HURT_LINES]
INT currentLineNeededConv00 = 0
INT currentLineNeededConv01 = 0
INT iRamLineNeeded = 0
INT iCarChaseLineNeeded = 0
INT iShotAtLineNeeded = 0
INT iHurtLineNeeded = 0

//if all lines from conversations have been played
BOOL bConv01AllPlayed = FALSE
BOOL bHurtConvAllPlayed = FALSE
BOOL bShotConvAllPlayed = FALSE
BOOL bRamConvAllPlayed = FALSE
BOOL bCarConvAllPlayed = FALSE
BOOL bConv00AllPlayed = FALSE
	
//checks if Dreyfuss' vehicle is stuck
CONST_INT MAX_DREY_POS 3
VECTOR vecDreyfussPositionTrack[MAX_DREY_POS]
INT iDreyfussPositionTimer
BOOL bStopDreyfussLines = FALSE //whether Dreyfuss needs to play his conversation lines

INT iControlTimer = 0
INT iTextTimer = 0

VECTOR mPlayerVehPos	= <<-1473.0630, 524.9622, 117.0789>>
FLOAT fPlayerVehHeading = 105.5732
VECTOR mPlayerVehPosAlt	= <<-1433.9927, 528.9166, 118.1719>> // B*1337872
FLOAT fPlayerVehHeadingAlt = 179.8441

//for working out if Drey is shot at, recently hurt or none
CONST_INT NONE 0
CONST_INT HURT 1
CONST_INT SHOT_AT 2
INT iImportantLinePlaying = NONE //e.g if line is in response to Dreyfuss being shot or hurt
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

SCENARIO_BLOCKING_INDEX mScenarioBlocker
			
INT iCutsceneStage	

BOOL bHelpDisplayed

BOOL bPlayerCloseToDreyfuss

OBJECT_INDEX oYogaMat
			
#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 2
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------


/// PURPOSE:
///    Cleans up the mission (for use if mission has been triggered)
PROC CLEANUP()
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
	SORT_CARS_NEAR_DREYFUSS(TRUE)
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 1) // "FRANKLIN"
	REMOVE_PED_FOR_DIALOGUE(sDialogue, 3) // "DREYFUSS"
	SAFE_REMOVE_BLIP(biGOTO)
	IF IS_PED_UNINJURED(pedDreyfuss)
		SET_PED_KEEP_TASK(pedDreyfuss, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedDreyfuss, FALSE)
	ENDIF
	SAFE_RELEASE_PED(pedDreyfuss)
	SAFE_RELEASE_VEHICLE(playerVeh)
	KILL_FACE_TO_FACE_CONVERSATION()
	SET_ROADS_IN_AREA(areaNearDreyfussHouse.vec1, areaNearDreyfussHouse.vec2, TRUE)
	REMOVE_ROAD_NODE_SPEED_ZONE(speedZone)
ENDPROC

/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CLEANUP()
	ENDIF
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT, TRUE)
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(dreyCarModel, FALSE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_RCM_DREYFUSS_MAT, BUILDINGSTATE_NORMAL, TRUE) // mat not there (default)
	SAFE_RELEASE_OBJECT(oYogaMat)
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()

	CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Script_Passed") 
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

	AWARD_ACHIEVEMENT_FOR_MISSION(ACH16) 		// A Mystery Solved
	Random_Character_Passed(CP_RAND_C_DREY1)
	Script_Cleanup()
ENDPROC

/// PURPOSE:
///    Plays end dialogue before passing script
PROC SCRIPT_PASSING()
	
	SWITCH eState
		
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Play end dialogue...") 
			SAFE_REMOVE_BLIP(biGOTO)
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			eState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
		
			//play end speech
			IF NOT bEndConv
				IF IS_ENTITY_ALIVE(pedDreyfuss)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bEndConv = PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "DREY1AU", "DREY1_END", "DREY1_END_2", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
					ENDIF
				ELSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bEndConv = PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "DREY1AU", "DREY1_END", "DREY1_END_1", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			Script_Passed()
		BREAK
	ENDSWITCH
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Fail the mission
PROC STAGE_FAILED()
	SWITCH eState
		CASE SS_SETUP
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Starting state MST_FAILED") #ENDIF
			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			SAFE_REMOVE_BLIP(biGOTO)
			Random_Character_Failed()
			eState = SS_UPDATE
		BREAK
		CASE SS_UPDATE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: GET_MISSION_FLOW_SAFE_TO_CLEANUP returned true") #ENDIF
				SAFE_DELETE_PED(pedDreyfuss)
				SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[DREYFUSS])				
				Script_Cleanup()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: 
///    Opens the gates at the opposite house.
/// RETURNS: 
///    TRUE if the gates exist and were opened, FALSE otherwise
FUNC BOOL OPEN_GATES()
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-1482.991577, 523.455627, 117.998055 >>, 15.0, PROP_LRGGATE_01_L)
	AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1477.864624, 525.636292, 118.04558 >>, 15.0, PROP_LRGGATE_01_R)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01_L, <<-1482.991577, 523.455627, 117.998055 >>, TRUE, 0.0)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01_R, << -1477.864624, 525.636292, 118.04558 >>, TRUE, 0.0)
       	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01_L, <<-1482.991577, 523.455627, 117.998055 >>, TRUE, 1.0)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_LRGGATE_01_R, << -1477.864624, 525.636292, 118.04558 >>, TRUE, -1.0)
	
		RETURN TRUE
  	ENDIF
	
	RETURN FALSE
ENDFUNC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

/// PURPOSE:
///    Changes the mission's current stage
/// PARAMS:
///    msStage - Mission stage to switch to
PROC SET_STAGE(MISSION_STAGE msStage)
	eMissionStage = msStage
	eState = SS_SETUP
ENDPROC

/// PURPOSE:
///    Sets all bool arrays to false
PROC INIT_BOOL_ARRAYS()

	ramLines[0] = "DREY1_BUMP_1"
	ramLines[1] = "DREY1_BUMP_2"
	ramLines[2] = "DREY1_BUMP_3"
	ramLines[3] = "DREY1_BUMP_4"
	ramLines[4] = "DREY1_BUMP_5"
	ramLines[5] = "DREY1_BUMP_6"
	ramLines[6] = "DREY1_BUMP_7"
	
	specLbl00[0] = "DREY1_CHASE0_1"
	specLbl00[1] = "DREY1_CHASE0_2"
	specLbl00[2] = "DREY1_CHASE0_3"
	specLbl00[3] = "DREY1_CHASE0_4"
	specLbl00[4] = "DREY1_CHASE0_5"
	
	carChaseLines[0] = "DREY1_CHASE1_1"
	carChaseLines[1] = "DREY1_CHASE1_2"
	carChaseLines[2] = "DREY1_CHASE1_3"
	carChaseLines[3] = "DREY1_CHASE1_4"
	carChaseLines[4] = "DREY1_CHASE1_5"
	carChaseLines[5] = "DREY1_CHASE1_6"
	carChaseLines[6] = "DREY1_CHASE1_7"
	carChaseLines[7] = "DREY1_CHASE1_8"
	carChaseLines[8] = "DREY1_CHASE1_9"
	carChaseLines[9] = "DREY1_CHASE1_10"
	carChaseLines[10] = "DREY1_CHASE1_11"
	carChaseLines[11] = "DREY1_CHASE1_12"
	carChaseLines[12] = "DREY1_CHASE1_13"
	carChaseLines[13] = "DREY1_CHASE1_14"
	carChaseLines[14] = "DREY1_CHASE1_15"
	carChaseLines[15] = "DREY1_CHASE1_16"
	carChaseLines[16] = "DREY1_CHASE1_17"
	carChaseLines[17] = "DREY1_CHASE1_18"
	carChaseLines[18] = "DREY1_CHASE1_19"
	carChaseLines[19] = "DREY1_CHASE1_20"
	carChaseLines[20] = "DREY1_CHASE1_21"
	carChaseLines[21] = "DREY1_CHASE1_22"
	carChaseLines[22] = "DREY1_CHASE1_23"
	carChaseLines[23] = "DREY1_CHASE1_24"
	carChaseLines[24] = "DREY1_CHASE1_25"
	carChaseLines[25] = "DREY1_CHASE1_26"
	carChaseLines[26] = "DREY1_CHASE1_27"
	carChaseLines[27] = "DREY1_CHASE1_28"
	carChaseLines[28] = "DREY1_CHASE1_29"
	carChaseLines[29] = "DREY1_CHASE1_30"
	carChaseLines[30] = "DREY1_CHASE1_31"
	
	specLbl01[0] = "DREY1_CHASE2_1"
	specLbl01[1] = "DREY1_CHASE2_2"
	specLbl01[2] = "DREY1_CHASE2_3"
	specLbl01[3] = "DREY1_CHASE2_4"
	specLbl01[4] = "DREY1_CHASE2_5"
	specLbl01[5] = "DREY1_CHASE2_6"
	specLbl01[6] = "DREY1_CHASE2_7"
	specLbl01[7] = "DREY1_CHASE2_8"
	specLbl01[8] = "DREY1_CHASE2_9"
	specLbl01[9] = "DREY1_CHASE2_10"
	specLbl01[10] = "DREY1_CHASE2_11"
	specLbl01[11] = "DREY1_CHASE2_12"
	specLbl01[12] = "DREY1_CHASE2_13"
	specLbl01[13] = "DREY1_CHASE2_14"
	specLbl01[14] = "DREY1_CHASE2_15"
	specLbl01[15] = "DREY1_CHASE2_16"
	specLbl01[16] = "DREY1_CHASE2_17"
	specLbl01[17] = "DREY1_CHASE2_18"
	specLbl01[18] = "DREY1_CHASE2_19"
	specLbl01[19] = "DREY1_CHASE2_20"
	specLbl01[20] = "DREY1_CHASE2_21"
	specLbl01[21] = "DREY1_CHASE2_22"
	specLbl01[22] = "DREY1_CHASE2_23"
	specLbl01[23] = "DREY1_CHASE2_24"
	specLbl01[24] = "DREY1_CHASE2_25"
	specLbl01[25] = "DREY1_CHASE2_26"
	specLbl01[26] = "DREY1_CHASE2_27"
	specLbl01[27] = "DREY1_CHASE2_28"
	specLbl01[28] = "DREY1_CHASE2_29"
	specLbl01[29] = "DREY1_CHASE2_30"
	specLbl01[30] = "DREY1_CHASE2_31"
	specLbl01[31] = "DREY1_CHASE2_32"
	
	shotAtLines[0] = "DREY1_SHOOT_1"
	shotAtLines[1] = "DREY1_SHOOT_2"
	shotAtLines[2] = "DREY1_SHOOT_3"
	
	hurtLines[0] = "DREY1_HURT_1"
	hurtLines[1] = "DREY1_HURT_2"
	hurtLines[2] = "DREY1_HURT_3"
	hurtLines[3] = "DREY1_HURT_4"

	INT i = 0
	REPEAT MAX_DREY_HURT_LINES i
		bHurtConvPlayed[i] = FALSE
	ENDREPEAT
	i=0
	REPEAT MAX_DREY_SHOT_AT_LINES i
		bShotAtConvPlayed[i] = FALSE
	ENDREPEAT
	i=0
	REPEAT MAX_RAM_LINES i
		bRamConv[i] = FALSE
	ENDREPEAT
	i=0
	REPEAT MAX_CAR_CHASE_LINES i
		bCarChaseConv[i] = FALSE
	ENDREPEAT
	i=0
	REPEAT MAX_LINES_CONV00 i
		bConv00Played[i] = FALSE
	ENDREPEAT
	i=0
	REPEAT MAX_LINES_CONV01 i
		bConv01Played[i] = FALSE
	ENDREPEAT
	bCarWreckConv = FALSE
	bAggravatedDrefuss = FALSE
	bEndConv = FALSE
	bStopDreyfussLines = FALSE
	bConv01AllPlayed = FALSE
	bHurtConvAllPlayed = FALSE
	bShotConvAllPlayed = FALSE
	bRamConvAllPlayed = FALSE
	bCarConvAllPlayed = FALSE
	bConv00AllPlayed = FALSE
	iRamLineNeeded = 0
	iCarChaseLineNeeded = 0
	currentLineNeededConv00 = 0
	currentLineNeededConv01  = 0
	iBlendRatioTimer = 0
	iShotAtLineNeeded = 0
	iHurtLineNeeded = 0
	iImportantLinePlaying = NONE
	abandonCarRootLabel = "DREY1_WRECK"
ENDPROC

PROC SETUP_DREYFUSS_PED()
	IF NOT IS_ENTITY_ALIVE(pedDreyfuss)
		WHILE NOT CREATE_NPC_PED_ON_FOOT(pedDreyfuss, CHAR_DREYFUSS, <<-1472.9, 483.3, 115.2>>, 75)
			WAIT(0)
		ENDWHILE
	ENDIF
	IF IS_ENTITY_ALIVE(pedDreyfuss)
		ADD_PED_FOR_DIALOGUE(sDialogue, 3, pedDreyfuss, "DREYFUSS")
		SET_PED_CONFIG_FLAG(pedDreyfuss, PCF_PedsJackingMeDontGetIn, TRUE)
		SET_PED_CAN_BE_TARGETTED(pedDreyfuss, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(pedDreyfuss, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedDreyfuss, TRUE)
		SET_PED_PATH_MAY_ENTER_WATER(pedDreyfuss, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDreyfuss, TRUE)
		SET_PED_CONFIG_FLAG(pedDreyfuss, PCF_DisableGoToWritheWhenInjured, TRUE)
		SET_PED_CONFIG_FLAG(pedDreyfuss, PCF_DontInfluenceWantedLevel, TRUE)
	ENDIF
ENDPROC

PROC SETUP_DREYFUSS_VEHICLE()
	IF NOT IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
		REQUEST_MODEL(dreyfussVehModel)
		WHILE NOT HAS_MODEL_LOADED(dreyfussVehModel)
			WAIT(0)
		ENDWHILE 
		sRCLauncherDataLocal.vehID[DREYFUSS] = CREATE_VEHICLE(dreyfussVehModel, dreyfussCarPos, DreyfussCarHeading)
		SET_MODEL_AS_NO_LONGER_NEEDED(dreyfussVehModel)
	ENDIF
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
		//SET_ENTITY_HEALTH(sRCLauncherDataLocal.vehID[DREYFUSS], DreyVehHealth)
		//SET_VEHICLE_ENGINE_HEALTH(sRCLauncherDataLocal.vehID[DREYFUSS], DreyVehEngineHealth)
		//SET_VEHICLE_PETROL_TANK_HEALTH(sRCLauncherDataLocal.vehID[DREYFUSS], DreyVehPetrolTankHealth)
		SET_VEHICLE_INFLUENCES_WANTED_LEVEL(sRCLauncherDataLocal.vehID[DREYFUSS], FALSE)
		SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherDataLocal.vehID[DREYFUSS], 1)
		SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherDataLocal.vehID[DREYFUSS],"DR3YFU55")
	ENDIF
ENDPROC

/// PURPOSE:
///    Skips to the correct stage
/// PARAMS:
///    MISSION_STAGE eMissionStageToSkipTo: The stage you want to enter
PROC DREY1_DEBUG_SKIP_STATE(MISSION_STAGE eMissionStageToSkipTo)

	RC_START_Z_SKIP()
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	INIT_BOOL_ARRAYS()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	IF IS_CUTSCENE_ACTIVE()
		 WAIT_FOR_CUTSCENE_TO_STOP()
	ENDIF
	
	CLEAR_AREA(<<-1462.1774, 485.6856, 115.2016>>, 200, TRUE)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(), <<-1462.1774, 485.6856, 115.2016>>, 97.1913)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: hjgxfhj") #ENDIF
	ENDIF
	SAFE_DELETE_VEHICLE(playerVeh)
	SAFE_DELETE_PED(pedDreyfuss)
	SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[DREYFUSS])
	
	OPEN_GATES()
		
	SETUP_DREYFUSS_PED()
	SETUP_DREYFUSS_VEHICLE()
			
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
	ENDIF
	END_REPLAY_SETUP()
	
	IF eMissionStageToSkipTo = MS_CONFRONT_DREY
		IF IS_ENTITY_ALIVE(pedDreyfuss)
			TASK_FOLLOW_NAV_MESH_TO_COORD(pedDreyfuss, <<-1475.3658, 500.3797, 116.6013>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
		ENDIF
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 3000)
		ENDIF
		WAIT(500)
	ENDIF
	
	RC_END_Z_SKIP(TRUE, FALSE)
	SET_STAGE(eMissionStageToSkipTo)
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Check debug skip functionality
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: S key pressed so passing mission") #ENDIF
			WAIT_FOR_CUTSCENE_TO_STOP()
			Script_Passed()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: J key pressed so doing z skip forwards")
			IF eMissionStage = MS_INTRO
				DREY1_DEBUG_SKIP_STATE(MS_CONFRONT_DREY)
			ELSE
				Script_Passed()
			ENDIF	
		ENDIF	
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)	
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: P key pressed so doing z skip backwards")
			DREY1_DEBUG_SKIP_STATE(MS_INTRO)
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: F key pressed so failing mission") #ENDIF
			WAIT_FOR_CUTSCENE_TO_STOP()
			SET_STAGE(MS_FAILED)
		ENDIF
		
		INT i_new_state
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Z skip menu used so doing z skip")
			DREY1_DEBUG_SKIP_STATE(INT_TO_ENUM(MISSION_STAGE, i_new_state))
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		MISSION FUNCTIONS & PROCEDURES
// ===========================================================================================================

/// PURPOSE:
///    Changes blip if Dreyfuss in car, and advances the mission
PROC CheckIfDreyfussInCar()
	IF IS_PED_UNINJURED(pedDreyfuss) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[DREYFUSS])
		IF IS_PED_SITTING_IN_VEHICLE(pedDreyfuss, sRCLauncherDataLocal.vehID[DREYFUSS])
			IF DOES_BLIP_EXIST(biGOTO)
				SET_BLIP_SCALE(biGOTO, BLIP_SIZE_VEHICLE)
			ENDIF
			eState = SS_CLEANUP
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays given line in an array if needed. Stops lines  if conversation has been started and Dreyfuss is out of range.
/// PARAMS:
///    bEndOfLines = if all the lines in the current conversation have been played
///    INT& convLineNum = the line num to play, starting from 0
///    CONST_INT maxLines = the total number of lines in the conversation
///    BOOL& bPlayedLine = if Dreyfuss has played the line or not
///    STRING rootLabel
///    STRING specLabel
///    bWaitUntilEndToIncreaseNum - if TRUE it waits until there are no conversations playing or queued to increase the conv line num
PROC PLAY_DREYFUSS_LINES(BOOL& bEndOfLines, INT& convLineNum, INT maxLines, BOOL& bPlayedLine, STRING rootLabel, STRING specLabel, BOOL bWaitUntilEndToIncreaseNum = FALSE)
	//IF convLineNum > 0
	IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDreyfuss, fYellRange)
		bStopDreyfussLines = TRUE
	ELSE
		bStopDreyfussLines = FALSE
	ENDIF
	//ENDIF

	IF NOT bPlayedLine	
		IF NOT bStopDreyfussLines
			IF NOT bEndOfLines
				IF IS_PED_UNINJURED(pedDreyfuss)
				AND NOT IS_PED_RAGDOLL(pedDreyfuss)
				AND NOT IS_PED_IN_WRITHE(pedDreyfuss)
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDreyfuss, fYellRange)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF (bWaitUntilEndToIncreaseNum)
							OR ((NOT bWaitUntilEndToIncreaseNum) AND (iImportantLinePlaying = NONE))	
								CPRINTLN(DEBUG_MISSION, "DREYFUSS1: TRY TO PLAY A LINE")
								bPlayedLine = PLAY_SINGLE_LINE_FROM_CONVERSATION(sDialogue, "DREY1AU", rootLabel, specLabel, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
							ENDIF
							IF bPlayedLine
							AND NOT bWaitUntilEndToIncreaseNum
								IF convLineNum < (maxLines-1)
									convLineNum++
								ELSE
									bEndOfLines = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bWaitUntilEndToIncreaseNum
		AND (NOT (iImportantLinePlaying = NONE))
		AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			IF convLineNum < (maxLines-1)
				convLineNum++
			ELSE
				bEndOfLines = TRUE
			ENDIF
			
			IF (iImportantLinePlaying = HURT)
			AND DOES_ENTITY_EXIST(pedDreyfuss)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedDreyfuss)
			ENDIF
			iImportantLinePlaying = NONE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If Dreyfuss is shot at or hurt, stop and block other dialogue, play correct hurt/shot at dialogue, and allow other dialogue to play after
///    PARAMS:
///    bCheckDamagerVehicle - if TRUE, checks for if Player's vehicle damaged Dreyfuss
FUNC INT checkIfDreyfussHurtOrShotAt(BOOL bCheckDamagerVehicle = FALSE)
	//set hurt/ shot-at line type if needed			
	IF iImportantLinePlaying = NONE
		IF IS_PLAYER_SHOOTING_NEAR_PED(pedDreyfuss)
		AND (iShotAtLineNeeded < MAX_DREY_SHOT_AT_LINES) //no more shot-at lines to play if this is false
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			iImportantLinePlaying = SHOT_AT
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDreyfuss, PLAYER_PED_ID(), bCheckDamagerVehicle)
		AND (iHurtLineNeeded < MAX_DREY_HURT_LINES)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			iImportantLinePlaying = HURT
		ENDIF
	ENDIF
	
	//attempt to play correct conversation if needed, set line-playing back to none when finished
	IF iImportantLinePlaying = HURT
		PLAY_DREYFUSS_LINES(bHurtConvAllPlayed, iHurtLineNeeded, MAX_DREY_HURT_LINES, bHurtConvPlayed[iHurtLineNeeded], "DREY1_HURT", hurtLines[iHurtLineNeeded], TRUE)
	ELIF iImportantLinePlaying = SHOT_AT
		PLAY_DREYFUSS_LINES(bShotConvAllPlayed, iShotAtLineNeeded, MAX_DREY_SHOT_AT_LINES, bShotAtConvPlayed[iShotAtLineNeeded], "DREY1_SHOOT", shotAtLines[iShotAtLineNeeded], TRUE)
	ENDIF
	
	RETURN iImportantLinePlaying
ENDFUNC

/// PURPOSE:
///    Plays correct lines depending on if the car is wrecked, towed, and plays Dreyfuss' other fleeing lines after abandoning his car
PROC PLAY_CORRECT_ABANDONED_CAR_LINES() 
	IF NOT bCarWreckConv
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDreyfuss, fYellRange)
			IF IS_PED_BEING_JACKED(pedDreyfuss) // B*1433872
				bCarWreckConv = CREATE_CONVERSATION(sDialogue, "DREY1AU", "DREY1_HURT", CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			ELSE
				bCarWreckConv = CREATE_CONVERSATION(sDialogue, "DREY1AU", abandonCarRootLabel, CONV_PRIORITY_MEDIUM, DISPLAY_SUBTITLES)
			ENDIF
		ENDIF
	ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()//make sure car wrecked conversation is played first
		IF (checkIfDreyfussHurtOrShotAt(TRUE) = NONE)
			PLAY_DREYFUSS_LINES(bConv01AllPlayed, currentLineNeededConv01, MAX_LINES_CONV01, bConv01Played[currentLineNeededConv01], "DREY1_CHASE2", specLbl01[currentLineNeededConv01])
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Keep a set of points for tracking Dreyfuss being stuck in the chase section
PROC UPDATE_DREYFUSS_SAVED_POSITIONS()
	iDreyfussPositionTimer = GET_GAME_TIMER() // Save timer for delaying next check

	// Shuffle stored positions
	vecDreyfussPositionTrack[0] = vecDreyfussPositionTrack[1]
	vecDreyfussPositionTrack[1] = vecDreyfussPositionTrack[2]
	vecDreyfussPositionTrack[2] = GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[DREYFUSS])
ENDPROC

/// PURPOSE:
///    Checks the distance between two locations is less than a given amount
/// PARAMS:
///    VECTOR v1 	= location 1
///    VECTOR v2 	= location 2
///    FLOAT fRange = the range between the two locations
/// RETURNS:
///    TRUE if in range
FUNC BOOL ARE_PROGRESS_COORDS_IN_RANGE(VECTOR v1, VECTOR v2, FLOAT fRange)
	RETURN ( VDIST2(v1, v2) <= fRange*fRange )
ENDFUNC

/// PURPOSE:
///    Checks Dreyfuss is moving at a sensible rate and has not stopped for too long
/// RETURNS:
///    TRUE if he's moving sensibly, else false
FUNC BOOL DREYFUSS_DRIVE_CHECK()
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
		IF IS_PED_IN_VEHICLE((pedDreyfuss), sRCLauncherDataLocal.vehID[DREYFUSS])
			// See if it's time to check positions
			IF GET_GAME_TIMER() - iDreyfussPositionTimer > 3000
				UPDATE_DREYFUSS_SAVED_POSITIONS()
				IF ARE_PROGRESS_COORDS_IN_RANGE(vecDreyfussPositionTrack[0], vecDreyfussPositionTrack[2], 2)
					CPRINTLN(DEBUG_MISSION, "DREYFUSS1: DREYFUSS_DRIVE_CHECK::: Dreyfuss has moved less than 2m in last 6 seconds")
					RETURN FALSE
				ELSE
					CPRINTLN(DEBUG_MISSION, "DREYFUSS1: DREYFUSS_DRIVE_CHECK::: Dreyfuss OK, moved over 2m during last 6 seconds")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			iDreyfussPositionTimer = GET_GAME_TIMER() //reset timer
		ENDIF
		
		RETURN TRUE
	ENDIF
  
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if Dreyfuss is performing a certain task or not
/// PARAMS:
///    theScrTaskName - the SCRIPT_TASK_NAME you are looking for
/// RETURNS:
///    TRUE if performing or waiting for the task, else FALSE
FUNC BOOL IS_DREYFUSS_PERFORMING_TASK(SCRIPT_TASK_NAME theScrTaskName)
	IF (GET_SCRIPT_TASK_STATUS(pedDreyfuss,theScrTaskName) = PERFORMING_TASK) 
	OR (GET_SCRIPT_TASK_STATUS(pedDreyfuss,theScrTaskName) = WAITING_TO_START_TASK)
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_DREYFUSS_CAR_TYRES_BURST()
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
		INT iTyresBurst = 0
		IF IS_VEHICLE_TYRE_BURST(sRCLauncherDataLocal.vehID[DREYFUSS], SC_WHEEL_CAR_FRONT_LEFT)
			iTyresBurst++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(sRCLauncherDataLocal.vehID[DREYFUSS], SC_WHEEL_CAR_FRONT_RIGHT)
			iTyresBurst++
		ENDIF	
		IF IS_VEHICLE_TYRE_BURST(sRCLauncherDataLocal.vehID[DREYFUSS], SC_WHEEL_CAR_REAR_LEFT)
			iTyresBurst++
		ENDIF	
		IF IS_VEHICLE_TYRE_BURST(sRCLauncherDataLocal.vehID[DREYFUSS], SC_WHEEL_CAR_REAR_RIGHT)
			iTyresBurst++
		ENDIF
		IF (iTyresBurst > 1)
			abandonCarRootLabel = "DREY1_TYRE"
			bAggravatedDrefuss = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    advance stage if jacked or car ruined
PROC CheckIfDreyfussAbandonCar()
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
		IF ARE_DREYFUSS_CAR_TYRES_BURST()
			SET_STAGE(MS_DREY_ABANDONED_CAR)
		ELIF NOT DREYFUSS_DRIVE_CHECK()
			abandonCarRootLabel = "DREY1_RUN"
			bAggravatedDrefuss = TRUE
			SET_STAGE(MS_DREY_ABANDONED_CAR)
		ELSE
			IF IS_PED_BEING_JACKED(pedDreyfuss) 
			OR NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[DREYFUSS])	
				CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Abandon car") 
				bAggravatedDrefuss = TRUE
				SET_STAGE(MS_DREY_ABANDONED_CAR)
			ENDIF
		ENDIF
	ELSE
		bAggravatedDrefuss = TRUE
		abandonCarRootLabel = "DREY1_CGONE"
		SET_STAGE(MS_DREY_ABANDONED_CAR)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set variable and stage and return true if Dreyfuss' car is towed
///    /// RETURNS:
///    FALSE if not been towed, else TRUE
FUNC BOOL carTowCheck(MODEL_NAMES towName)
	VEHICLE_INDEX tow = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[DREYFUSS]), 20.0, towName, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
	IF DOES_ENTITY_EXIST(tow)
		CPRINTLN(DEBUG_MISSION, "DREYFUSS1: tow exists")
		IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tow, sRCLauncherDataLocal.vehID[DREYFUSS])
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss car is towed")
			abandonCarRootLabel = "DREY1_TOW"
			SET_STAGE(MS_DREY_ABANDONED_CAR)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Tells us if a entity is within the viewcone of a ped
/// PARAMS:
///    vEntityLookFor - the entity we are looking for 
///    vPed - the ped who is doing the seeing
///    viewcone - viewcone of the seeing ped
///    useHead - if this is true we take into account which way the seeing ped is looking or we use his forward vector
/// RETURNS:
///    True or False
FUNC BOOL IS_ENTITY_IN_PED_VIEW_CONE(PED_INDEX vPed, ENTITY_INDEX vEntityLookFor, FLOAT viewcone = 170.0, BOOL useHead = FALSE)
	VECTOR vNormalisedVec = NORMALISE_VECTOR(GET_ENTITY_COORDS(vEntityLookFor) - GET_ENTITY_COORDS(vPed))
	VECTOR fwd 
	FLOAT fViewAngle 
	
	IF viewcone < 0.1 OR viewcone > 360
		RETURN TRUE
	ENDIF
	
	IF (useHead = FALSE)
		fwd = GET_ENTITY_FORWARD_VECTOR(vPed)
	ELSE
		fwd = NORMALISE_VECTOR(GET_PED_BONE_COORDS(vPed, BONETAG_HEAD, <<0, 5, 0>>) - GET_PED_BONE_COORDS(vPed, BONETAG_HEAD, <<0, 0, 0>>))
	ENDIF
	
	fViewAngle = DOT_PRODUCT(fwd, vNormalisedVec)
	IF fViewAngle <= COS(viewcone / 2.0)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    makes Dreyfuss react if the player has done something odd to his car (e.g towed it)
/// RETURNS:
///    TRUE if moved away from original position and not seen, else FALSE
FUNC BOOL CheckIfCarMoved()
	IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[DREYFUSS])
		IF IS_DREYFUSS_PERFORMING_TASK(SCRIPT_TASK_PERFORM_SEQUENCE)
			IF (GET_SEQUENCE_PROGRESS(pedDreyfuss) > 0) //he's ready to get to his car

				//CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss sequence > 0") 

				//vehicle not where it's supposed to be, and not in sight
				IF NOT IS_ENTITY_AT_COORD (sRCLauncherDataLocal.vehID[DREYFUSS], dreyfussCarPos, << 20.0, 20.0, 100.0 >>)

					CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss car not at coord 20m") 				
					abandonCarRootLabel = "DREY1_CGONE"
					bAggravatedDrefuss = TRUE
					SET_STAGE(MS_DREY_ABANDONED_CAR)
					RETURN TRUE
					
				ELIF NOT IS_ENTITY_AT_COORD(sRCLauncherDataLocal.vehID[DREYFUSS], dreyfussCarPos, << 5.0, 5.0, 100.0 >>)

					CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss car not at coord") 
					
					IF NOT IS_ENTITY_IN_PED_VIEW_CONE(pedDreyfuss, sRCLauncherDataLocal.vehID[DREYFUSS])
						CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss car not in view") 
						abandonCarRootLabel = "DREY1_CGONE"
						bAggravatedDrefuss = TRUE
						SET_STAGE(MS_DREY_ABANDONED_CAR)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    makes Dreyfuss react if the player has done something odd to his car (e.g towed it)
/// RETURNS:
///    FALSE if not been fiddled with, else TRUE
FUNC BOOL CheckIfCarFiddledWith()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.vehID[DREYFUSS])
			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
				IF IS_ENTITY_IN_RANGE_ENTITY(pedDreyfuss, sRCLauncherDataLocal.vehID[DREYFUSS], 5.0)
					IF carTowCheck(TOWTRUCK)
						RETURN TRUE
					ELSE
						RETURN carTowCheck(TOWTRUCK2)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Dreyfuss car does not exist") 
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
///    Initialises everything
PROC INIT_MISSION()
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(dreyCarModel, TRUE)
	INIT_BOOL_ARRAYS() 
	
	CLEAR_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))	// ensure this bit is initially clear when starting the mission
	
	REQUEST_ADDITIONAL_TEXT("DREY1", MISSION_TEXT_SLOT)	
			
	ADD_PED_FOR_DIALOGUE(sDialogue, 1, PLAYER_PED_ID(), "FRANKLIN")
			
	SETUP_DREYFUSS_PED()
	SETUP_DREYFUSS_VEHICLE()
			
	#IF IS_DEBUG_BUILD
		mSkipMenu[0].sTxtLabel = "Intro"
		mSkipMenu[1].sTxtLabel = "Confront Dreyfuss" 
	#ENDIF
			
ENDPROC

/// PURPOSE: 
///    Halts the player and removes control if too close to Dreyfuss
PROC HALT_PLAYER_IF_CLOSE_TO_DREYFUSS()
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
	IF bPlayerCloseToDreyfuss = FALSE
		IF IS_ENTITY_ALIVE(pedDreyfuss)
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedDreyfuss) < 3.0
				bPlayerCloseToDreyfuss = TRUE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), pedDreyfuss)
				CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Franklin close to Dreyfuss so halting him")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the intro cutscene
PROC STAGE_INTRO()
	
	SWITCH eState
		
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Init MS_INTRO") 
			RC_REQUEST_CUTSCENE("paper_1_RCM_alt1", TRUE)
			IF IS_SCREEN_FADED_IN() // Don't do gameplay hint if skipping to intro
			AND NOT IS_REPLAY_IN_PROGRESS() // Or if replaying mission
			AND IS_ENTITY_ALIVE(pedDreyfuss)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sRCLauncherDataLocal.pedID[0]) > 4.0
				SET_GAMEPLAY_ENTITY_HINT(pedDreyfuss, (<<0, 0, -0.5>>), TRUE, -1, 3000)															
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.3)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.01)
                SET_GAMEPLAY_HINT_FOV(35.00)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				KILL_ANY_CONVERSATION()
				iCutsceneStage = 0
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Doing leadin focus camera") #ENDIF
			ELSE // Must be z skipping or replaying
				iCutsceneStage = 1
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Not doing leadin focus camera") #ENDIF
			ENDIF
			SAFE_RELEASE_OBJECT(oYogaMat)
			oYogaMat = GET_CLOSEST_OBJECT_OF_TYPE(<<-1459.57, 485.16, 115.20>>, 10.0, prop_yoga_mat_02)
			IF DOES_ENTITY_EXIST(oYogaMat)
				FREEZE_ENTITY_POSITION(oYogaMat, TRUE)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: grabbed and froze yoga mat") #ENDIF
			ELSE
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "DREYFUSS1: couldn't find yoga mat") #ENDIF
			ENDIF
			eState = SS_UPDATE
			bPlayerCloseToDreyfuss = FALSE
		BREAK
		
		CASE SS_UPDATE
		
			SWITCH iCutsceneStage
				CASE 0
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						HALT_PLAYER_IF_CLOSE_TO_DREYFUSS()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND IS_ENTITY_ALIVE(pedDreyfuss)
						AND CREATE_CONVERSATION(sDialogue, "DREY1AU", "DREY1_RCM_LI", CONV_PRIORITY_HIGH)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedDreyfuss, -1)
							iCutsceneStage++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						HALT_PLAYER_IF_CLOSE_TO_DREYFUSS()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND RC_IS_CUTSCENE_OK_TO_START()
							IF IS_ENTITY_ALIVE(pedDreyfuss)
								REGISTER_ENTITY_FOR_CUTSCENE(pedDreyfuss, sSceneHandleDreyfuss, CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							ENDIF
							RC_CLEANUP_LAUNCHER()
							START_CUTSCENE()
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
							SET_BUILDING_STATE(BUILDINGNAME_IPL_RCM_DREYFUSS_MAT, BUILDINGSTATE_DESTROYED) // mission version - the cutscene version is rotated 90 degrees so no idea what it's supposed to be used for
							iCutsceneStage++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF IS_CUTSCENE_PLAYING()
						IF IS_GAMEPLAY_HINT_ACTIVE()
							STOP_GAMEPLAY_HINT()
						ENDIF
						IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE)
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1457.842407,485.819519,114.201614>>, <<-1474.226929,482.811035,117.201561>>, 3.000000, mPlayerVehPos, fPlayerVehHeading) // Cutscene area
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1473.983643,510.045166,114.603439>>, <<-1474.226929,482.811035,118.201561>>, 4.000000, mPlayerVehPos, fPlayerVehHeading) // Drefuss running along path to gate
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1472.341187,508.362091,114.615784>>, <<-1475.958130,520.158936,120.044846>>, 10.000000, mPlayerVehPos, fPlayerVehHeading) // Dreyfuss' car in road
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(mPlayerVehPos, fPlayerVehHeading)
						ELSE
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1457.842407,485.819519,114.201614>>, <<-1474.226929,482.811035,117.201561>>, 3.000000, mPlayerVehPosAlt, fPlayerVehHeadingAlt) // Cutscene area
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1473.983643,510.045166,114.603439>>, <<-1474.226929,482.811035,118.201561>>, 4.000000, mPlayerVehPosAlt, fPlayerVehHeadingAlt) // Drefuss running along path to gate
							RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1472.341187,508.362091,114.615784>>, <<-1475.958130,520.158936,120.044846>>, 10.000000, mPlayerVehPosAlt, fPlayerVehHeadingAlt) // Dreyfuss' car in road
							SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(mPlayerVehPosAlt, fPlayerVehHeadingAlt)
						ENDIF
						RC_START_CUTSCENE_MODE(<< -1477.0, 496.8, 117.4 >>)
						STOP_FIRE_IN_RANGE(<< -1477.0, 496.8, 117.4 >>, 30.0)
						CLEAR_AREA_OF_VEHICLES(<<-1473.3762, 517.8441, 116.8854>>, 30)
						SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
						iCutsceneStage++
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_CUTSCENE_PLAYING()
						eState = SS_CLEANUP
					ELSE
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 3000)
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Dreyfuss")
						AND IS_ENTITY_ALIVE(pedDreyfuss)
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedDreyfuss, <<-1475.3658, 500.3797, 116.6013>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH		
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_STOP_EVENT()
			RC_END_CUTSCENE_MODE()
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			SET_STAGE(MS_CONFRONT_DREY)
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Cleaned up MS_INTRO") 
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Unknown state in stage intro")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Kill Dreyfuss if tazered
PROC STUNGUN_CHECK()
	IF IS_ENTITY_ALIVE(pedDreyfuss)
		IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(pedDreyfuss, WEAPONTYPE_STUNGUN)
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: DREYFUSS damaged by stungun")
			IF IS_ENTITY_ALIVE(pedDreyfuss)
				SET_ENTITY_HEALTH(pedDreyfuss, 99)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Kill Dreyfuss or let him go. He will attempt to get in his car
PROC STAGE_CONFRONT_DREYFUSS()
	
	SWITCH eState
		
		CASE SS_SETUP
			
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Init MS_CONFRONT_DREY") 
			
			rolling_start = TRUE
			bStopDreyfussLines = FALSE
			OPEN_GATES()
			
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[DREYFUSS])
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[DREYFUSS], VEHICLELOCK_UNLOCKED)
				SET_VEHICLE_ENGINE_ON(sRCLauncherDataLocal.vehID[DREYFUSS], TRUE, TRUE)
				SET_VEHICLE_DOOR_OPEN(sRCLauncherDataLocal.vehID[DREYFUSS], SC_DOOR_FRONT_LEFT)
			ENDIF
			
			IF IS_ENTITY_ALIVE(pedDreyfuss)
				SET_PED_MIN_MOVE_BLEND_RATIO(pedDreyfuss, PEDMOVEBLENDRATIO_SPRINT)
				IF NOT DOES_BLIP_EXIST(biGOTO)
					biGOTO = CREATE_PED_BLIP(pedDreyfuss)
				ENDIF
				INIT_FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, TRUE, FALSE)
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[DREYFUSS])
					OPEN_SEQUENCE_TASK(seqEscapePlanA)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1472.2698, 513.9628, 116.8980>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
						TASK_ENTER_VEHICLE(NULL, sRCLauncherDataLocal.vehID[DREYFUSS], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_SPRINT) 
						TASK_VEHICLE_DRIVE_WANDER(NULL, sRCLauncherDataLocal.vehID[DREYFUSS], normalDreyfussVehSpeed, DRIVINGMODE_PLOUGHTHROUGH)
					CLOSE_SEQUENCE_TASK(seqEscapePlanA)
					TASK_PERFORM_SEQUENCE(pedDreyfuss, seqEscapePlanA)
					speedZone = ADD_ROAD_NODE_SPEED_ZONE(GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[DREYFUSS]), 100, 10)
				ELSE
					TASK_SMART_FLEE_PED(pedDreyfuss, PLAYER_PED_ID(), 500, -1)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(playerVeh)
				REQUEST_MODEL(playerVehModel)
				WHILE NOT HAS_MODEL_LOADED(playerVehModel)
					WAIT(0)  
				ENDWHILE 
				playerVeh = CREATE_VEHICLE(playerVehModel, << -1482.3665, 529.0344, 117.2725 >>, 201.5764)
				SET_MODEL_AS_NO_LONGER_NEEDED(playerVehModel)
			ENDIF
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE) // Just in case screen is faded out
			
			iBlendRatioTimer = GET_GAME_TIMER()
			bHelpDisplayed = FALSE
				
			eState = SS_UPDATE
		BREAK

		CASE SS_UPDATE
			
			IF bHelpDisplayed = FALSE
			AND IS_SCREEN_FADED_IN()
				PRINT_HELP("DREY1_HELP")
				bHelpDisplayed = TRUE
			ENDIF
			
			STUNGUN_CHECK()
		
			IF NOT IS_ENTITY_ALIVE(pedDreyfuss) 
				REPLAY_RECORD_BACK_FOR_TIME(3.0)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DR1_DREYFUSS_KILLED)
				IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY) set")	
				ENDIF
				SET_STAGE(MS_PASSED)
			ELIF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedDreyfuss) > fDreyfussEscapeDist
				SET_STAGE(MS_PASSED)
			ELSE
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedDreyfuss)
				CheckIfDreyfussInCar()
				IF (checkIfDreyfussHurtOrShotAt(TRUE) = NONE)
					PLAY_DREYFUSS_LINES(bConv00AllPlayed, currentLineNeededConv00, MAX_LINES_CONV00, bConv00Played[currentLineNeededConv00], "DREY1_CHASE0", specLbl00[currentLineNeededConv00])
				ENDIF
				
				IF rolling_start
					if IS_ENTITY_ALIVE(pedDreyfuss)
						IF (GET_GAME_TIMER() - iBlendRatioTimer) > 3000
							SET_PED_MIN_MOVE_BLEND_RATIO(pedDreyfuss, PEDMOVEBLENDRATIO_SPRINT)
						ELSE
							rolling_start = FALSE
						ENDIF
					ENDIF
				ENDIF
			
				FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, FALSE)
				
				IF NOT CheckIfCarMoved()
					IF NOT CheckIfCarFiddledWith()
					AND NOT IS_DREYFUSS_PERFORMING_TASK(SCRIPT_TASK_PERFORM_SEQUENCE) //if Dreyfuss has broken out of the sequence
						CheckIfDreyfussAbandonCar()
					ENDIF
				ENDIF
				
				IF IS_DREYFUSS_PERFORMING_TASK(SCRIPT_TASK_PERFORM_SEQUENCE)
				AND (GET_SEQUENCE_PROGRESS(pedDreyfuss) > 0) //he's ready to get to his car
				AND ARE_DREYFUSS_CAR_TYRES_BURST() // B*1521829 Just make Dreyfuss flee if his tyres have been shot out
					SET_STAGE(MS_DREY_ABANDONED_CAR)
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			SET_STAGE(MS_DREY_DRIVES_OFF)
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Cleaned up MS_CONFRONT_DREY") 
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Unknown state in Confront Dreyfuss")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Kill Dreyfuss or let him go. He will attempt to flee in his car
PROC STAGE_DREY_DRIVES_OFF
	INT i = 0
	SWITCH eState
		
		CASE SS_SETUP
			
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Init MS_DREY_DRIVES_OFF") 
			SET_ROADS_IN_AREA(areaNearDreyfussHouse.vec1, areaNearDreyfussHouse.vec2, TRUE)
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)

			IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
				REPEAT MAX_DREY_POS i
					vecDreyfussPositionTrack[i] = GET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[DREYFUSS])
				ENDREPEAT
			ENDIF
			
			iDreyfussPositionTimer  = GET_GAME_TIMER()
			bEscapedDriveway   		= FALSE
			bStopDreyfussLines 		= FALSE
			eState 			   		= SS_UPDATE
		BREAK

		CASE SS_UPDATE
			
			STUNGUN_CHECK()
			
			IF NOT IS_ENTITY_ALIVE(pedDreyfuss) 
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 4, REPLAY_IMPORTANCE_HIGHEST)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DR1_DREYFUSS_KILLED)
				IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY) set")	
				ENDIF
				SET_STAGE(MS_PASSED)
			ELIF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedDreyfuss) > fDreyfussEscapeDist
				REPLAY_RECORD_BACK_FOR_TIME(6.0, 2, REPLAY_IMPORTANCE_HIGHEST)
				SET_STAGE(MS_PASSED)
			ELSE
				//sort out Dreyfuss's dialogue when being rammed and normal lines
				IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.vehID[DREYFUSS])
					//ram lines
	            	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND IS_ENTITY_TOUCHING_ENTITY(sRCLauncherDataLocal.vehID[DREYFUSS], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						PLAY_DREYFUSS_LINES(bRamConvAllPlayed, iRamLineNeeded, MAX_RAM_LINES, bRamConv[iRamLineNeeded], "DREY1_BUMP", ramLines[iRamLineNeeded])
	            	ENDIF
					//normal lines
					PLAY_DREYFUSS_LINES(bCarConvAllPlayed, iCarChaseLineNeeded, MAX_CAR_CHASE_LINES, bCarChaseConv[iCarChaseLineNeeded], "DREY1_CHASE1", carChaseLines[iCarChaseLineNeeded])
					IF NOT bAggravatedDrefuss
					AND NOT bEscapedDriveway
					AND IS_PED_SITTING_IN_ANY_VEHICLE(pedDreyfuss)
					AND NOT IS_ENTITY_AT_COORD(sRCLauncherDataLocal.vehID[DREYFUSS], <<-1474.4446, 519.4703, 117.0069>>, <<15.0, 15.0, 15.0>>)
						TASK_VEHICLE_MISSION_PED_TARGET(pedDreyfuss, sRCLauncherDataLocal.vehID[DREYFUSS], PLAYER_PED_ID(), MISSION_FLEE, fastDreyfussVehSpeed, DRIVINGMODE_AVOIDCARS, 100.0, 1.0)
						bEscapedDriveway = TRUE
					ENDIF
	            ENDIF
				checkIfDreyfussHurtOrShotAt()
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedDreyfuss)
				IF NOT bAggravatedDrefuss
				AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), pedDreyfuss, 10.0)
				AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_PED_SITTING_IN_ANY_VEHICLE(pedDreyfuss)
					TASK_VEHICLE_MISSION_PED_TARGET(pedDreyfuss, sRCLauncherDataLocal.vehID[DREYFUSS], PLAYER_PED_ID(), MISSION_FLEE, fastDreyfussVehSpeed, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0, 1.0)
					bAggravatedDrefuss = TRUE	
				ENDIF
				IF NOT CheckIfCarFiddledWith()
					CheckIfDreyfussAbandonCar()
				ENDIF
			ENDIF
			
			FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, FALSE)
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Cleaned up MS_DREY_DRIVES_OFF") 
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Unknown state in Dreyfuss Drives Off")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Kill Dreyfuss or let him go. His car is ruined or he has been jacked out. He will attempt to flee on foot.
PROC STAGE_DREY_ABANDONED_CAR()
	
	SWITCH eState
		
		CASE SS_SETUP

			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Init MS_DREY_ABANDONED_CAR")
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

			IF DOES_BLIP_EXIST(biGOTO)
				SET_BLIP_SCALE(biGOTO, BLIP_SIZE_PED)
			ENDIF
			
			IF IS_ENTITY_ALIVE(pedDreyfuss)
				OPEN_SEQUENCE_TASK(seqAbandonCar)
					IF IS_PED_IN_ANY_VEHICLE(pedDreyfuss)
						TASK_LEAVE_ANY_VEHICLE(NULL)
					ENDIF
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
				CLOSE_SEQUENCE_TASK(seqAbandonCar)
				TASK_PERFORM_SEQUENCE(pedDreyfuss, seqAbandonCar)
				CLEAR_SEQUENCE_TASK(seqAbandonCar)
				CPRINTLN(DEBUG_MISSION, "DREYFUSS1: DREY FLEE ABANDONED_CAR") 
			ENDIF

			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			bStopDreyfussLines = FALSE
			eState 			   = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			STUNGUN_CHECK()
			
			IF NOT IS_ENTITY_ALIVE(pedDreyfuss)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DR1_DREYFUSS_KILLED)
				IF NOT IS_BIT_SET(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					SET_BIT(g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY))
					CPRINTLN(DEBUG_INTERNET, GET_THIS_SCRIPT_NAME(), " g_savedGlobals.sRandomChars.g_iWebsiteQueryBit, ENUM_TO_INT(RC_NWS_DREYFUSS_KILLED_DREY) set")	
				ENDIF
				SET_STAGE(MS_PASSED)
			ELIF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedDreyfuss) > fDreyfussEscapeDist
				REPLAY_RECORD_BACK_FOR_TIME(6.0, 2, REPLAY_IMPORTANCE_HIGHEST)
				SET_STAGE(MS_PASSED)
			ELSE
				CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedDreyfuss)
				FLASH_BLIP_AND_TEXT(biGOTO, "", "", iControlTimer, iTextTimer, FALSE)
				PLAY_CORRECT_ABANDONED_CAR_LINES()
				SET_PED_DESIRED_MOVE_BLEND_RATIO(pedDreyfuss, 1.8)
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Cleaned up MS_DREY_ABANDONED_CAR") 
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("Unknown state in Dreyfuss Abandoned Car")
		BREAK
	ENDSWITCH
ENDPROC
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	// Take ownership of initial scene entities
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)

	// Now on mission
	SET_MISSION_FLAG(TRUE)

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	IF Is_Replay_In_Progress() // Set up the initial scene for replays
      	g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_DREYFUSS_1(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		g_bSceneAutoTrigger = FALSE
	ENDIF
	
	mScenarioBlocker = Dreyfuss1_Scenario_Blocker()
	SORT_CARS_NEAR_DREYFUSS(FALSE)
	
	IF IS_ENTITY_ALIVE(sRCLauncherDataLocal.pedID[0])
		pedDreyfuss = sRCLauncherDataLocal.pedID[0]
		SET_ENTITY_AS_MISSION_ENTITY(pedDreyfuss, TRUE, TRUE)
		CPRINTLN(DEBUG_MISSION, "DREYFUSS1: Grabbed pedDreyfuss from sRCLauncherDataLocal.pedID[0]")
	ENDIF
	
	INIT_MISSION()
	
	IF IS_REPLAY_IN_PROGRESS()
		INT istage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			istage++
		ENDIF
		SWITCH istage
			CASE CP_KILL_DREYFUSS
				START_REPLAY_SETUP(<<-1462.1774, 485.6856, 115.2016>>, 97.1913)
				DREY1_DEBUG_SKIP_STATE(MS_CONFRONT_DREY)
			BREAK
		ENDSWITCH
	ENDIF
	
	WHILE (TRUE)
		
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_Dreyfuss")
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SWITCH eMissionStage				
				CASE MS_INTRO
					STAGE_INTRO()
				BREAK
				
				CASE MS_CONFRONT_DREY
					STAGE_CONFRONT_DREYFUSS()
				BREAK
				
				CASE MS_DREY_DRIVES_OFF
					STAGE_DREY_DRIVES_OFF()
				BREAK
				
				CASE MS_DREY_ABANDONED_CAR
					STAGE_DREY_ABANDONED_CAR()
				BREAK		
				
				CASE MS_PASSED
					SCRIPT_PASSING()
				BREAK
				
				CASE MS_FAILED
					STAGE_FAILED()
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Unknown stage in Dreyfuss script loop")
				BREAK
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD	
			DEBUG_Check_Debug_Keys()
		#ENDIF
	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
