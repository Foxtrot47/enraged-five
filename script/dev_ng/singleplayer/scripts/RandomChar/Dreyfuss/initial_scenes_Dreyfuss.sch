USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

MODEL_NAMES mContactModel      = GET_NPC_PED_MODEL(CHAR_DREYFUSS)
MODEL_NAMES dreyCarModel       = LANDSTALKER
VECTOR      dreyfussCarPos 	   = <<-1473.3762, 517.8441, 116.8854>>
FLOAT       dreyfussCarHeading = 97.3011

/// PURPOSE:
///    Blocks the pool ped scenarios at Dreyfuss1's start
FUNC SCENARIO_BLOCKING_INDEX Dreyfuss1_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<-1495.49585, 519.46063, 100.8889>>, <<-1429.1115, 468.1972, 120.9696>>)
ENDFUNC

/// PURPOSE: sets road areas etc so cars aren't in the way
/// PARAMS:
///    bSet - whether to enable or disable the settings
PROC SORT_CARS_NEAR_DREYFUSS(BOOL bSet)
	SET_ROADS_IN_AREA(<< -1594.8999, 457.8715, -100 >>, << -1366.7844, 635.5790, 900 >>, bSet)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1473.5175, 508.0, 115.601>>, <<-1471.0171, 514.8290, 120.0>>, bSet)
ENDPROC

/// PURPOSE: Creates the scene for Dreyfuss 1
/// NOTE:    
FUNC BOOL SetupScene_DREYFUSS_1(g_structRCScriptArgs& sRCLauncherData)
	
	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_DREYFUSS  	0
	CONST_INT MODEL_CAR  		1
	
	// Variables
	MODEL_NAMES mModel[2]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_DREYFUSS] = mContactModel
	mModel[MODEL_CAR] = dreyCarModel
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE
	
			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-1457.251221,475.825836,114.201454>>
			sRCLauncherData.triggerLocate[1] = <<-1460.402344,490.470490,118.601013>>
			sRCLauncherData.triggerWidth = 35.000000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.sIntroCutscene 			= "paper_1_RCM_alt1"
			sRCLauncherData.bPedsCritical 			= TRUE
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
			
			// Request anims
			SETUP_LAUNCHER_ANIMS(sRCLauncherData.sAnims, "rcmcollect_paperleadinout@", "meditiate_idle")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE
		
			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAVE_LAUNCHER_ANIMS_LOADED(sRCLauncherData.sAnims)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
	
			// Create Dreyfuss
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_DREYFUSS, <<-1459.68, 485.26, 116.22>>, -131.30, "DREYFUSS LAUNCHER RC")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT) // B*1525035
			ENDIF
			
			// Create car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], dreyfussCarPos, DreyfussCarHeading)
				IF IS_ENTITY_ALIVE(sRCLauncherData.vehID[0])
					//SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0],VEHICLELOCK_LOCKOUT_PLAYER_ONLY) // B*1520728
					SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[0], 1)
					SET_VEHICLE_NUMBER_PLATE_TEXT(sRCLauncherData.vehID[0],"DR3YFU55")
					SET_VEHICLE_MODEL_IS_SUPPRESSED(dreyCarModel, TRUE)
				ENDIF
			ENDIF
			
			// Ready to roll
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
			
			// Mission version
			SET_BUILDING_STATE(BUILDINGNAME_IPL_RCM_DREYFUSS_MAT, BUILDINGSTATE_DESTROYED) 
	
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
	
			// Clear area
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(sRCLauncherData.pedID[0]), 50.0)
			ENDIF
			
			// Remove vehicles
			CLEAR_AREA_OF_VEHICLES(<<-1471.1663, 510.6191, 115.8419>>, 4.0)
			SORT_CARS_NEAR_DREYFUSS(FALSE)
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	// Scene not ready
	RETURN FALSE
ENDFUNC
