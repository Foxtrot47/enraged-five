USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE     eInitialSceneStage = IS_REQUEST_SCENE
SCENARIO_BLOCKING_INDEX RCMScenarioBlockArea

/// PURPOSE:
///    toggle setup of the mission trigger area, such as clear areas, scenario blocking and turning off road nodes.
/// PARAMS:
///    eMissionID - the mission ID
///    bEnable - if we are enabling the setup or turning it all back off
///    bCallClearAreas = added for B*1544717 - reduce num of clear area calls when cutscene starts (not convinced this is a good idea)
PROC SETUP_AREA_FOR_MISSION(g_eRC_MissionIDs eMissionID, BOOL bEnable, BOOL bCallClearAreas = TRUE)
	
	VECTOR vClearZone_DropOffPoint_Min = << 2711.19775, 4134.42529, 32.90168 >>
	VECTOR vClearZone_DropOffPoint_Max = << 2739.98145, 4155.22070, 50.28859 >>
	
	SWITCH eMissionID
	
		CASE RC_MAUDE_1		// ensure any values changes are updated in the mission script to keep in sync
			IF bEnable
				
				// Scenarios
				RCMScenarioBlockArea = ADD_SCENARIO_BLOCKING_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)
				
				// Peds
				SET_PED_NON_CREATION_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)

				// vehicles	
				SET_ROADS_IN_AREA(<< 2697.22241, 4119.89355, 42.79107 >>, << 2746.03955, 4162.97363, 43.62732 >>, FALSE)	// bigger area for turning off the road nodes
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, FALSE)
				
				IF bCallClearAreas
					REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max)
					CLEAR_AREA(<< 2728.33276, 4144.77783, 43.29292 >>, 7.5, TRUE)
				ENDIF
			ELSE
				// Restore everything to normal
				REMOVE_SCENARIO_BLOCKING_AREA(RCMScenarioBlockArea)
				
				CLEAR_PED_NON_CREATION_AREA()
				
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearZone_DropOffPoint_Min, vClearZone_DropOffPoint_Max, TRUE)
				SET_ROADS_BACK_TO_ORIGINAL(<< 2697.22241, 4119.89355, 42.79107 >>, << 2746.03955, 4162.97363, 43.62732 >>)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    ensure's the initial scene's sync scene is cleaned up prior to peds being released
///    Fix for B*1377647 - sync scene trying to start on ped without anims loaded
/// PARAMS:
///    sRCLauncherData -Random char mission data needed to get reference to entities 
PROC CLEANUP_MAUDE_SYNC_SCENE(g_structRCScriptArgs& sRCLauncherData)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(sRCLauncherData.iSyncSceneIndex)
		IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
			IF IsPedPerformingTask(sRCLauncherData.pedID[0], SCRIPT_TASK_SYNCHRONIZED_SCENE)
				CLEAR_PED_TASKS(sRCLauncherData.pedID[0])
			ENDIF
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.pedID[0], INSTANT_BLEND_OUT, TRUE)
		ENDIF
		IF IS_ENTITY_ALIVE(sRCLauncherData.ObjID[0])
			STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.ObjID[0], INSTANT_BLEND_OUT, TRUE)
		ENDIF
		CPRINTLN(DEBUG_RANDOM_CHAR, GET_THIS_SCRIPT_NAME(), ": CLEANUP_MAUDE_SYNC_SCENE : sync scene cleaned up sRCLauncherData.iSyncSceneIndex = ", sRCLauncherData.iSyncSceneIndex)
	ENDIF	
ENDPROC

/// PURPOSE: 
///    Creates initial scene for Maude encounter
FUNC BOOL SetupScene_MAUDE_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_MAUDE 	0
	CONST_INT MODEL_CHAIR 	1
	CONST_INT MODEL_LAPTOP 	2
	
	// Variables
	MODEL_NAMES mModel[3]
	INT         iCount
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_MAUDE]  = GET_NPC_PED_MODEL(CHAR_MAUDE)
	mModel[MODEL_CHAIR]  = prop_table_03_chr
	mModel[MODEL_LAPTOP] = Prop_Laptop_01a
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<2729.091797,4143.169922,41.363319>>
			sRCLauncherData.triggerLocate[1] = <<2714.735107,4145.382324,45.307323>>
			sRCLauncherData.triggerWidth = 12.800000
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical = TRUE
			sRCLauncherData.sIntroCutscene = "MAUDE_MCS_1"
	
			// Setup scene
			SETUP_AREA_FOR_MISSION(RC_MAUDE_1, TRUE)
	
			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT("special_ped@maude@base")
			
			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED("special_ped@maude@base")
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
		
		CASE IS_CREATE_SCENE
		
			// Has scene been created?
			bCreatedScene = TRUE
			
			// Create Maude's chair
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[0])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[0], mModel[MODEL_CHAIR], << 2728.35, 4145.59, 43.30 >>, -91.28)
			ENDIF
			
			// Maude's laptop
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.ObjID[1])
				CREATE_SCENE_PROP(sRCLauncherData.ObjID[1], mModel[MODEL_LAPTOP], <<2727.686035,4145.714844,44.080002>>, 71.0)
			ENDIF
			
			// Create Maude
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_MAUDE, << 2728.33, 4145.60, 43.89 >> , 89.19, "RC MAUDE 1")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Ready to apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK		

		CASE IS_COMPLETE_SCENE
			
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0])
			AND IS_ENTITY_ALIVE(sRCLauncherData.ObjID[0])
				sRCLauncherData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<< 2727.40, 4145.56, 43.68 >>, << 0.0, 0.0, -92.17 >>)
				SET_SYNCHRONIZED_SCENE_LOOPED(sRCLauncherData.iSyncSceneIndex, TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sRCLauncherData.iSyncSceneIndex, FALSE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], sRCLauncherData.iSyncSceneIndex, "special_ped@maude@base", "base", INSTANT_BLEND_IN, SLOW_BLEND_OUT,
												SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sRCLauncherData.pedID[0], TRUE)
				
				INT iEntitySyncedSceneFlags
				iEntitySyncedSceneFlags = 0
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_LOOP_WITHIN_SCENE)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
				iEntitySyncedSceneFlags += ENUM_TO_INT(SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT)		
				PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.ObjID[0], sRCLauncherData.iSyncSceneIndex, "base_chair", "special_ped@maude@base", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iEntitySyncedSceneFlags)
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH

	// Scene not ready
	RETURN FALSE
ENDFUNC
