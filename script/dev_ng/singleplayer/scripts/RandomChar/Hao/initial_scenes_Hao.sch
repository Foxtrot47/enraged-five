USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"

ENUM INITIAL_SCENE_STAGE
	IS_REQUEST_SCENE,
	IS_WAIT_FOR_SCENE,
	IS_CREATE_SCENE,
	IS_COMPLETE_SCENE
ENDENUM
INITIAL_SCENE_STAGE eInitialSceneStage = IS_REQUEST_SCENE

// RCM character
MODEL_NAMES mContactModel = GET_NPC_PED_MODEL(CHAR_HAO)

/// PURPOSE: Creates initial scene for Hao encounter
FUNC BOOL SetupScene_HAO_1(g_structRCScriptArgs& sRCLauncherData)

	// TODO: Look at best way of not having to define these variables every update
	// Constants
	CONST_INT MODEL_HAO   0
	CONST_INT MODEL_CAR   1
	CONST_INT MODEL_PHONE 2
	CONST_INT MODEL_CAR2  3	
	
	// Variables
	MODEL_NAMES mModel[4]
	INT         iCount
	INT 		iSceneID
	STRING      sAnimDict = "special_ped@hao@base"
	BOOL        bCreatedScene

	// Assign model names
	mModel[MODEL_HAO]   = mContactModel
	mModel[MODEL_CAR]   = PENUMBRA
	mModel[MODEL_PHONE] = PROP_NPC_PHONE
	mModel[MODEL_CAR2]  = RUINER
	
	// Handle loading assets
	SWITCH eInitialSceneStage
	
		CASE IS_REQUEST_SCENE

			// Setup launcher data
			sRCLauncherData.triggerType      = RC_TRIG_LOCATE_NONAXIS
			sRCLauncherData.triggerLocate[0] = <<-72.134911,-1256.712158,27.683605>>
			sRCLauncherData.triggerLocate[1] = <<-72.180153,-1267.586792,29.248524>>
			sRCLauncherData.triggerWidth     = 25.0
			sRCLauncherData.bAllowVehicleActivation = FALSE
			sRCLauncherData.bPedsCritical  = TRUE
			sRCLauncherData.bVehsCritical  = FALSE
			sRCLauncherData.sIntroCutscene = "HAO_MCS_1"

			// Request models
			FOR iCount = 0 TO COUNT_OF(mModel) -1
				REQUEST_MODEL(mModel[iCount])
			ENDFOR
	
			// Request anims
			REQUEST_ANIM_DICT(sAnimDict)

			// Wait for assets to load...
			eInitialSceneStage = IS_WAIT_FOR_SCENE
		BREAK
		
		CASE IS_WAIT_FOR_SCENE

			IF NOT HAVE_ALL_MODELS_IN_ARRAY_LOADED(mModel)
			OR NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				RETURN FALSE
			ENDIF
			
			// Assets are loaded - now create scene
			eInitialSceneStage = IS_CREATE_SCENE
		BREAK
	
		CASE IS_CREATE_SCENE	

			// Has scene been created?
			bCreatedScene = TRUE

			// Create Hao
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.pedID[0])
				IF NOT RC_CREATE_NPC_PED(sRCLauncherData.pedID[0], CHAR_HAO, <<-71.936844,-1259.7,28.193594>>, -177.55, "HAO LAUNCHER RC - HAO")
					bCreatedScene = FALSE
				ENDIF
			ENDIF
			
			// Create Hao's car
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[0])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[0], mModel[MODEL_CAR], <<-72.3226, -1258.6433, 28.1915>>, 90.8326 )
				SET_VEHICLE_COLOURS(sRCLauncherData.vehID[0], 38, 0)
				SET_VEHICLE_EXTRA_COLOURS(sRCLauncherData.vehID[0], 91, 0)
				SET_VEHICLE_MOD_KIT(sRCLauncherData.vehID[0], 0)
				
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_SPOILER, 2)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BUMPER_F, 1)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BUMPER_R, 1)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_SKIRT, 1)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_EXHAUST, 1)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_GRILL, 0)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BONNET, 2)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_ROOF, 0)
				PRELOAD_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_WHEELS, 20)

				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_SPOILER, 2)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BUMPER_F, 1)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BUMPER_R, 1)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_SKIRT, 1)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_EXHAUST, 1)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_GRILL, 0)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_BONNET, 2)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_ROOF, 0)
				SET_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_WHEELS, 20)
				
				TOGGLE_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_TOGGLE_TURBO, TRUE)
				TOGGLE_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_TOGGLE_NITROUS, TRUE)
				TOGGLE_VEHICLE_MOD(sRCLauncherData.vehID[0], MOD_TOGGLE_XENON_LIGHTS, TRUE)
				
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherData.vehID[0], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				ROLL_DOWN_WINDOW(sRCLauncherData.vehID[0], SC_WINDOW_FRONT_LEFT)
				SET_VEHICLE_LIGHTS(sRCLauncherData.vehID[0], FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(mModel[MODEL_CAR], TRUE)
				
				SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherData.vehID[0])
				WAIT(0)
			ENDIF
	
			// Create car that can be used by the player
			IF NOT DOES_ENTITY_EXIST(sRCLauncherData.vehID[1])
				CREATE_SCENE_VEHICLE(sRCLauncherData.vehID[1], mModel[MODEL_CAR2], <<-91.07, -1273.45, 28.86>>, 0.57 )
				SET_VEHICLE_COLOUR_COMBINATION(sRCLauncherData.vehID[1], 10)
			ENDIF
			
			// Attach phone to Hao
			IF IS_ENTITY_ALIVE(sRCLauncherData.pedID[0])
				CREATE_SCENE_PROP(sRCLauncherData.objID[0], mModel[MODEL_PHONE], <<-71.936844,-1259.077515,28.193594>>, -177.55)
				ATTACH_ENTITY_TO_ENTITY(sRCLauncherData.ObjID[0], sRCLauncherData.pedID[0], GET_PED_BONE_INDEX(sRCLauncherData.pedID[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE)
			ENDIF
			
			// Ready to apply anims
			IF bCreatedScene
				eInitialSceneStage = IS_COMPLETE_SCENE
			ENDIF
		BREAK
		
		CASE IS_COMPLETE_SCENE
	
			IF IS_PED_UNINJURED(sRCLauncherData.pedID[0]) AND IS_VEHICLE_OK(sRCLauncherData.vehID[0])
				
				INT    iBone 
				VECTOR vChassisPos
				
				// Apply synched scene
				iBone       = GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherData.vehID[0], "chassis_dummy")
				vChassisPos = GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherData.vehID[0], iBone)
				iSceneID    = CREATE_SYNCHRONIZED_SCENE(vChassisPos, GET_ENTITY_ROTATION(sRCLauncherData.vehID[0]))
	
				SET_SYNCHRONIZED_SCENE_LOOPED(iSceneID,TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID,FALSE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherData.pedID[0], iSceneID, sAnimDict, "hao_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
				//PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherData.vehID[0], iSceneID, "hao_base_penumbra", sAnimDict, NORMAL_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				TASK_LOOK_AT_ENTITY(sRCLauncherData.pedID[0], PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			
			// Release models
			FOR iCount = 0 TO COUNT_OF(mModel) - 1
				SET_MODEL_AS_NO_LONGER_NEEDED(mModel[iCount])
			ENDFOR
			
			// Scene is good to go!
			RETURN TRUE
		BREAK
	ENDSWITCH

	// Scene not ready
	RETURN FALSE
ENDFUNC
