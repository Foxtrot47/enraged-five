

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "initial_scenes_Hao.sch"

// *****************************************************************************************
//		SCRIPT NAME	:	launcher_Hao.sc
//
//		AUTHOR		:	David Roberts / Andrew Minghella
//
//		DESCRIPTION	:	Launcher script that determines which RC mission scene to setup.
//						This launcher script should be attached to appropriate world point.
// *****************************************************************************************
CONST_FLOAT 			WORLD_POINT_COORD_TOLERANCE 1.0
INT 					iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
SCENARIO_BLOCKING_INDEX mScenarioBlocker

/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
/// PARAMS:
///    sData - launcher data struct
///    bCleanupEntities - do we want to cleanup the entities in the launcher struct
PROC Script_Cleanup(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)

	IF bCleanupEntities
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Cleaning up entities in Launcher")
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF
	
	// Remove scenario blocking areas
	REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker) 
	
	//B*1574385 - Force update to blip in RandChar Controller if mission wasn't launched
	IF bCleanupEntities
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Creates the initial scene
FUNC BOOL LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)
	
	// Setup Hao
	IF NOT SetupScene_HAO_1(sData)
		RETURN FALSE
	ENDIF

	// Scene creation successful
	PRINT_LAUNCHER_DEBUG("Created initial scene")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Custom version of function that ignores height    
FUNC BOOL ARE_VECTORS_ALMOST_EQUAL_2D(VECTOR v1, VECTOR v2, FLOAT fTolerance = 0.5)
	
	IF fTolerance < 0
		fTolerance = 0
	ENDIF
	
	IF ABSF(v1.x - v2.x) <= fTolerance
		IF ABSF(v1.y - v2.y) <= fTolerance
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main script loops
/// PARAMS:
///    in_coords - world point co-ords
SCRIPT(coords_struct in_coords)
	
	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs 	sRCLauncherData			// Scene information to pass to mission script
	VECTOR				 	vInCoords = <<0,0,0>>	// Stores world point location

	//Reset all basic values of the data, so each scene has to set them up correctly
	RC_Reset_LauncherData(sRCLauncherData)

	// Update world point
	vInCoords = in_coords.vec_coord[0]

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		
		// Ensure candidate id is released in the event that the player has died
		// or been arrested prior to mission launch
		IF sRCLauncherData.eMissionID <> NO_RC_MISSION
			IF (g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID <> NO_CANDIDATE_ID)
				PRINT_LAUNCHER_DEBUG("Relinquishing candidate id...")
				Mission_Over(g_RandomChars[sRCLauncherData.eMissionID].rcMissionCandidateID)
			ENDIF
		ENDIF
		
		// Standard cleanup
		Script_Cleanup(sRCLauncherData)
	ENDIF
	
	// Pick which mission activated us
	g_eRC_MissionIDs eRCMissions[1]
	eRCMissions[0] = RC_HAO_1
	IF NOT (DETERMINE_RC_TO_LAUNCH(eRCMissions, sRCLauncherData, vInCoords, WORLD_POINT_COORD_TOLERANCE))
		RC_LAUNCHER_END()
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
		 
	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
		
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sRCLauncherData.eMissionID)
		RC_LAUNCHER_END()
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
		TERMINATE_THIS_THREAD()					// B* 1510945 - don't call cleanup if nothing has been setup yet 
	ENDIF
	
	//Halt launcher if Franklin1 is available B*-2167022
	IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
		PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 is available... [TERMINATING]")
		TERMINATE_THIS_THREAD()
	ELSE
		PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 is not available...")
	ENDIF
	
	// The script is allowed to launch so set up the initial scene
	WHILE NOT LOAD_INITIAL_SCENE(sRCLauncherData)
		WAIT(0)
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("Player out of range [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ENDIF
	ENDWHILE
	
	// Create a scenario blocking area
	mScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA(<<-103.0011, -1281.6357, 26.3686>>, <<-23.2460, -1241.3610, 30.3350>>)
	
	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vInCoords, sRCLauncherData.activationRange, TRUE)
  
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		
		WAIT(0)
		
		// Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sRCLauncherData)
			Script_Cleanup(sRCLauncherData)
		ENDIF
		
		//Cleanup if Franklin1 is available B*-2167022
		IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
			PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 became available... [TERMINATING]")
			Script_Cleanup(sRCLauncherData)
		ELSE
			PRINT_LAUNCHER_DEBUG("SP_MISSION_FRANKLIN_1 is not available...")
		ENDIF
		
		// Check for player moving HAO's vehicle
		IF IS_VEHICLE_OK(sRCLauncherData.vehID[0])
			IF NOT ARE_VECTORS_ALMOST_EQUAL_2D(<<-72.323, -1258.643, 28.648>>, GET_ENTITY_COORDS(sRCLauncherData.vehID[0]))
				PRINT_LAUNCHER_DEBUG("Player has moved HAO's vehicle [TERMINATING]")
				MAKE_ALL_INITIAL_SCENE_PEDS_FLEE(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF
		ENDIF

		// Update launcher blip + preload intro
		SET_RC_AWAITING_TRIGGER(sRCLauncherData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sRCLauncherData.sIntroCutscene, vInCoords)
		
		PLAY_LAUNCHER_AMBIENT_DIALOGUE(sRCLauncherData, vInCoords, "HAO1AU", "HAO1_HCO", 3, "HAO", 5000)
		
		//Is the player close enough to start the mission off
		IF ARE_RC_TRIGGER_CONDITIONS_MET(sRCLauncherData)
			
			//Launch the mission script
			IF NOT LAUNCH_RC_MISSION(sRCLauncherData)
				Script_Cleanup(sRCLauncherData)
			ENDIF

			//Waits in a loop until we can terminate the launcher - flagged from mission at the moment
			IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sRCLauncherData.eMissionID)
				// No need to clean up entities, as we should have passed them over to the mission script
				Script_Cleanup(sRCLauncherData, FALSE)			
			ENDIF
		ENDIF
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
