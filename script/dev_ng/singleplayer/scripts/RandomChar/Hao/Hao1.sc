
//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1

USING "rage_builtins.sch"
USING "globals.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "commands_entity.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "randomChar_public.sch"
USING "replay_public.sch"
USING "RC_Helper_Functions.sch"
USING "RC_Threat_Public.sch"
USING "initial_scenes_Hao.sch"
//USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "Race_Control.sch"
USING "commands_recording.sch"
USING "taxi_functions.sch"

#IF IS_DEBUG_BUILD
      USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Hao1.sc
//		AUTHOR			:	
//		DESCRIPTION		:	Franklin meets Hao which unlocks Street Race oddjob
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
g_structRCScriptArgs sRCLauncherDataLocal

ENUM MISSION_STAGE
	MS_INIT,
	MS_PRE_INTRO,
	MS_INTRO,
	MS_GET_CAR,
	//MS_RETURN_TO_CAR,
	MS_DRIVE_TO_RACE,
	MS_LOSE_COPS,
	MS_LINEUP,
	MS_COUNTDOWN,
	MS_RACE,
	MS_PASS,
	MS_FAIL
ENDENUM

ENUM SUB_STATE
	SS_SETUP,
	SS_UPDATE,
	SS_UPDATE_2,
	SS_CLEANUP
ENDENUM

ENUM MISSION_FAILED_REASON
	MFR_NONE,
	MFR_THREATENED_HAO,
	MFR_KILLED_HAO,
	MFR_DIDNT_WIN
ENDENUM

CONST_INT HAO 0

//----------------------
//	CHECKPOINTS
//----------------------
CONST_INT CP_MISSION_START	0
CONST_INT CP_RACE_START 1
CONST_INT CP_MISSION_PASSED 2

MISSION_STAGE missionStage = MS_INIT
SUB_STATE missionSubState = SS_SETUP
MISSION_FAILED_REASON missionFailReason = MFR_NONE

STRING sHaoDestRecording = "hao1_Hao"

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[2]
#ENDIF

BLIP_INDEX biDestination
VECTOR vRaceStart = <<-144.969666,-1550.709106,33.487854>>//<<-53.350788,-1813.927979,25.723551>>//<<-56.54, -1804.86, 26.28>>

//VECTOR vHaoDestStart = <<-98.958611,-1318.066406,28.357740>>//<<-92.201019,-1318.854858,28.180756>>
VECTOR vHaoDest = <<-59.64, -1808.68, 26.76>>

VECTOR vClearAreaMin = <<-66.364021,-1812.869507,23.765099>>
VECTOR vClearAreaMax = <<-46.690018,-1790.171021,29.893478>>

VECTOR vVehicleIntroPos = <<-79.29, -1264.51, 28.74>>//<<-73.3166, -1265.9296, 28.1742>>//<<-82.50, -1320.32, 28.82>>
FLOAT  fVehicleIntroHeading =  270.3906//90.3906//88.46
VECTOR vPlayerIntroPos = <<-72.4774, -1262.7596, 27.9890>>//<<-72.9071, -1317.2295, 28.0414>>
FLOAT  fPlayerIntroHeading = 349.5845//281.2032
/*VECTOR vHaoIntroPos = <<-72.2937, -1260.6362, 28.1462>>
FLOAT fHaoIntroHeading = 169.58*/
VECTOR vIntroAreaPos1 = <<-80.264381,-1262.474854,27.014877>>//<<-85.718742,-1258.504028,27.290422>>//<<-65.888504,-1259.101196,27.195118>>//<<-75.258995,-1317.123535,27.055149>>
VECTOR vIntroAreaPos2 = <<-64.890450,-1262.256104,30.031475>>//<<-74.818626,-1258.565308,30.190575>>//<<-100.941582,-1258.543823,30.358128>>//<<-66.016724,-1316.947876,30.052485>> 
FLOAT fIntroAreaWidth = 11.0//5.0
VECTOR vIntroAreaBPos1 = <<-115.662796,-1261.453247,27.156199>>
VECTOR vIntroAreaBPos2 = <<-85.775139,-1260.507568,30.297710>> 
FLOAT fIntroAreaBWidth = 8.0
VECTOR vIntroAreaCPos1 = <<-91.509140,-1267.899902,27.148733>>
VECTOR vIntroAreaCPos2 = <<-91.271820,-1278.836304,29.151850>> 
FLOAT fIntroAreaCWidth = 9.0

// Keeping the road in front of the intro clear of vehicles
VECTOR vClearAmbientVehiclesAreaPos1 = <<-99.922195,-1101.087891,23.907503>>
VECTOR vClearAmbientVehiclesAreaPos2 = <<-109.291039,-1385.994629,30.313572>> 
FLOAT fClearAmbientVehiclesAreaWidth = 40.0

// variables for dealing with the number of times the player has rammed Hao
INT iHaoRammedCount = 0
BOOL bHaoRammedLastFrame = FALSE
FLOAT fPlayerVsHaoClosingSpeedLastFrame = 0
INT iRamTimer = 0

// Variables for checking if the player has blocked Hao's start point
VECTOR vBlockingHaoOne = <<-137.7, -1551.0, 35.3>>
VECTOR vBlockingHaoTwo = <<-144.1, -1558.7, 33.6>>
FLOAT fBlockingHao = 6.0

INT iLeadIn
STRING sLeadInDict = "special_ped@hao@base"

//VEHICLE_INDEX viPlayerLast
VEHICLE_INDEX viPlayerLastUnsuitable

/*VEHICLE_INDEX viNearbyCar
MODEL_NAMES mnNearbyCar = RUINER //BANSHEE
VECTOR vPosNearbyCar = <<-91.07, -1273.45, 28.86>>
FLOAT fHeadingNearbyCar = 0.57*/

CAMERA_INDEX  camCutscene

BOOL bGetVehicle = TRUE
BOOL bGoToRace = TRUE
BOOL bReturnToVehicle = TRUE
//BOOL bGetNewVehicle = TRUE
//BOOL bWrongVehicle = TRUE
BOOL bHeadingIsOK
BOOL bHaoArrivesLate = TRUE
BOOL bClearVehicles = TRUE
BOOL bSlipStreamHelpReady = TRUE
BOOL bLookBackHelpReady = TRUE
BOOL bIntroSkipped = FALSE
BOOL bPlaceHao = TRUE

INT iCallDelayTimer = 0
INT iAtStartTimer = 0
INT iRamInRaceTimer = 0
INT iCloseInRaceTimer = 0

VECTOR vRoadOffOne = <<-189.571381,-1613.517090,31.753590>>//<<-41.457458,-1821.230591,24.364006>>
VECTOR vRoadOffTwo = <<-131.694153,-1538.062134,34.259357>>//<<-117.984245,-1757.774414,29.840506>>
FLOAT fRoadOffWidth = 20.0//15.0
VECTOR vDisableRoadsBigPos1 = <<-254.968719,-1422.748657,20.337524>>
VECTOR vDisableRoadsBigPos2 = <<75.873474,-1692.709229,38.141510>> 
FLOAT fDisableRoadsBigWidth = 500.0

//Area to disable one of the lanes approaching the race start
VECTOR vApproachRoadPos1 = <<-134.442383,-1532.813843,34.241035>>
VECTOR vApproachRoadPos2 = <<-225.420334,-1456.148560,29.321934>> 
FLOAT fApproachRoadWidth = 5.0

/*VECTOR vDisableRoadPos1 = <<-115.442245,-1756.196167,29.832426>>
VECTOR vDisableRoadPos2 = <<113.464401,-1947.301636,18.621464>> 
FLOAT fDisableRoadWidth = 15.0*/

// Variables for tracking stats
INT iCollisionTimer = 0
/*CONST_INT TARGET_RACE_TIME 999999999
CONST_INT TARGET_LAP_TIME 999999999
CONST_INT TARGET_COLLISIONS 10*/
//INT iPreviousCollisions = 0

structPedsForConversation convStruct

// ===========================================================================================================
//		CLEANUP FUNCTIONS
// ===========================================================================================================
/// PURPOSE:
///    Cleans up mission things
PROC CleanupMission(BOOL bDeleteAll = FALSE)
	SAFE_REMOVE_BLIP(biDestination)
	
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sRCLauncherDataLocal.vehID[HAO], TRUE)
	ENDIF
	
	IF bDeleteAll
		SAFE_DELETE_PED(sRCLauncherDataLocal.pedID[HAO])
		SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[HAO])
		//SAFE_DELETE_VEHICLE(viNearbyCar)
		SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
	ELSE
		SAFE_RELEASE_PED(sRCLauncherDataLocal.pedID[HAO])
		SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[HAO])
		SAFE_RELEASE_VEHICLE(sRCLauncherDataLocal.vehID[1])
		//SAFE_RELEASE_VEHICLE(viNearbyCar)
	ENDIF
	
	RACE_CLEANUP(bDeleteAll)
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, FALSE)
	SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_SHOP_CARMOD_01_AP, TRUE)
	SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SAVEHOUSE, TRUE)
	SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, TRUE)
	
	SUPPRESS_LARGE_VEHICLES(FALSE)
	
	IF DOES_CAM_EXIST(camCutscene)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CAM_ACTIVE(camCutscene, FALSE)
		DESTROY_CAM(camCutscene)
	ENDIF
	
	REMOVE_WAYPOINT_RECORDING(sHaoDestRecording)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearAreaMin, vClearAreaMax, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vRaceStart-<<300,300,300>>, vRaceStart+<<300,300,300>>, TRUE)
	SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, TRUE)
	SET_ROADS_IN_ANGLED_AREA(vApproachRoadPos1, vApproachRoadPos2, fApproachRoadWidth, FALSE, TRUE)
	SET_ROADS_IN_ANGLED_AREA(vDisableRoadsBigPos1, vDisableRoadsBigPos2, fDisableRoadsBigWidth, FALSE, TRUE)
	//SET_ROADS_IN_ANGLED_AREA(vDisableRoadPos1, vDisableRoadPos2, fDisableRoadWidth, FALSE, TRUE)
	
	CLEAR_PRINTS()
ENDPROC
/// PURPOSE:
///    Safely cleans up the script
PROC Script_Cleanup()

	// Clear the mission title
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	CPRINTLN(DEBUG_MISSION, "Hao 1: Script Cleanup")
	
	// Ensure launcher is cleaned up
	RC_CLEANUP_LAUNCHER()
	
	// If the mission was triggered then additional mission cleanup will be required.
	IF (Random_Character_Cleanup_If_Triggered())
		CPRINTLN(DEBUG_MISSION, "Hao 1: Script triggered, additional cleanup required...")
		CleanupMission()
	ENDIF
	
	DISABLE_TAXI_HAILING(FALSE)
	
	//Cleanup the scene created by the launcher
	RC_CleanupSceneEntities(sRCLauncherDataLocal, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Adds needed contacts, completion %, cleans up and passes script.
PROC Script_Passed()
	CPRINTLN(DEBUG_MISSION, "Hao 1: Passed")
	
	// Update mission stats
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_SRLS)						// Technically have completed the first street race
	INFORM_MISSION_STATS_OF_TIME_TOTAL_CHANGE(HAO1_FASTEST_LAP, sRaceHUD.iBestTime)
	
	// B*1577763
	// Flag flow bitset for the minigame and ensure first two races are unlocked
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STREET_RACES), TRUE)
	SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_LOS_SANTOS))
	SET_BIT(g_savedGlobals.sStreetRaceData.iRaceUnlocked, ENUM_TO_INT(STREET_RACE_CITY_CIRCUIT))
	
	// Pass mission and autosave
	Random_Character_Passed(CP_RAND_C_HAO1)
	Script_Cleanup()
ENDPROC

// ===========================================================================================================
//		HELPER FUNCTIONS
// ===========================================================================================================

/*PROC CreateNearbyCar()
	IF NOT DOES_ENTITY_EXIST(viNearbyCar)
		REQUEST_MODEL(mnNearbyCar)
		IF HAS_MODEL_LOADED(mnNearbyCar)
			CLEAR_AREA_OF_VEHICLES(vPosNearbyCar, 5.0)
			viNearbyCar = CREATE_VEHICLE(mnNearbyCar, vPosNearbyCar, fHeadingNearbyCar)
			SET_VEHICLE_COLOUR_COMBINATION(viNearbyCar, 10)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnNearbyCar)
		ENDIF
	ENDIF
ENDPROC*/

/// PURPOSE:
///    Checks if the player has killed or threatened Hao, fails if so
PROC CheckHao()
	IF missionStage <> MS_FAIL
	AND missionStage <> MS_INIT
		IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[HAO])
			IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[HAO])
				missionStage = MS_FAIL
				missionSubState = SS_SETUP
				MissionFailReason = MFR_KILLED_HAO
			ELSE
				IF HAS_PLAYER_THREATENED_PED(sRCLauncherDataLocal.pedID[HAO]) OR (IS_PED_BEING_JACKED(sRCLauncherDataLocal.pedID[HAO]) AND IS_PED_JACKING(PLAYER_PED_ID()))
					missionStage = MS_FAIL
					missionSubState = SS_SETUP
					MissionFailReason = MFR_THREATENED_HAO
				ENDIF
			ENDIF
		ENDIF
		
		IF (missionStage = MS_GET_CAR OR missionStage = MS_DRIVE_TO_RACE OR missionStage = MS_LOSE_COPS ) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
			IF HAS_PLAYER_RAMMED_ENEMY_ENOUGH(sRCLauncherDataLocal.vehID[HAO], bHaoRammedLastFrame, iRamTimer, iHaoRammedCount, fPlayerVsHaoClosingSpeedLastFrame, 5)
			OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[HAO], TRUE)
				missionStage = MS_FAIL
				missionSubState = SS_SETUP
				MissionFailReason = MFR_THREATENED_HAO
			ENDIF
		ELIF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRCLauncherDataLocal.vehID[HAO], PLAYER_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sRCLauncherDataLocal.vehID[HAO])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CheckRacers()
	IF missionStage = MS_GET_CAR OR missionStage = MS_DRIVE_TO_RACE OR missionStage = MS_LOSE_COPS
		INT i_index = 0
		REPEAT sTrackData.iNumAIRacers i_index
			IF DOES_ENTITY_EXIST(sRacer[i_index].piDriver) AND DOES_ENTITY_EXIST(sRacer[i_index].viCar)
				IF IS_PED_INJURED(sRacer[i_index].piDriver) OR NOT IS_VEHICLE_OK(sRacer[i_index].viCar)
					missionStage = MS_FAIL
					missionSubState = SS_SETUP
					eRaceState = RACE_STATE_FAIL
					failReason = FAIL_RIVAL_ATTACKED
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRacer[i_index].piDriver, PLAYER_PED_ID()) OR IS_PLAYER_SHOOTING_NEAR_PED(sRacer[i_index].piDriver)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sRacer[i_index].viCar, PLAYER_PED_ID())
					OR (IS_PED_BEING_JACKED(sRacer[i_index].piDriver) AND IS_PED_JACKING(PLAYER_PED_ID()))
					OR IS_PLAYER_TOWING_VEHICLE(sRacer[i_index].viCar)
					OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND (GET_ENTITY_SPEED(PLAYER_PED_ID())>5) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sRacer[i_index].viCar))
						missionStage = MS_FAIL
						missionSubState = SS_SETUP
						eRaceState = RACE_STATE_FAIL
						failReason = FAIL_RIVAL_ATTACKED
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls failing for a race specific reason
PROC CONTROL_RACE_FAIL()
	IF eRaceState = RACE_STATE_FAIL
		missionStage = MS_FAIL
		missionSubState = SS_SETUP
	ENDIF
ENDPROC

FUNC BOOL AI_RACERS_DONT_EXIST()
	INT i_index
	
	FOR i_index = 0 TO sTrackData.iNumAIRacers - 1
		IF NOT IS_PED_UNINJURED(sRacer[i_index].piDriver)
			RETURN TRUE
		ENDIF
		IF NOT IS_VEHICLE_OK(sRacer[i_index].viCar)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays dialogue if the player hangs around the race start without triggering the race
PROC PLAY_WAITING_CONV()
	IF GET_GAME_TIMER() > iAtStartTimer
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		AND IS_ENTITY_IN_RANGE_COORDS(sRCLauncherDataLocal.pedID[HAO], sTrackData.vStartGrid[sTrackData.iNumAIRacers-1], 10.0)
		AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 25.0)
			IF CREATE_CONVERSATION(convStruct, "HAO1AU", "HAO1_HWAR", CONV_PRIORITY_HIGH)
				iAtStartTimer = GET_GAME_TIMER() + 10000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue if the player rams Hao
PROC PLAY_RAM_CONV()
	IF GET_GAME_TIMER() > iRamInRaceTimer
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		AND IS_ENTITY_TOUCHING_ENTITY(sRCLauncherDataLocal.pedID[HAO], PLAYER_PED_ID())
			IF CREATE_CONVERSATION(convStruct, "HAO1AU", "HAO1_HRAM", CONV_PRIORITY_HIGH)
				iRamInRaceTimer = GET_GAME_TIMER() + 10000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue if the player is close to Hao
PROC PLAY_CLOSE_CONV()
	IF GET_GAME_TIMER() > iCloseInRaceTimer
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		AND IS_ENTITY_IN_RANGE_ENTITY(sRCLauncherDataLocal.pedID[HAO], PLAYER_PED_ID(), 15.0)
			IF CREATE_CONVERSATION(convStruct, "HAO1AU", "HAO1_HSPC", CONV_PRIORITY_HIGH)
				iCloseInRaceTimer = GET_GAME_TIMER() + 10000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_RACE_LINEUP()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 200)
		SETUP_ALT_RIVAL_MODELS()
		SETUP_AI_RACERS(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO])
	ENDIF
ENDPROC

/// PURPOSE:
///     Put Hao into the lineup - 7th place as it's easier for him to get there...
PROC PLACE_HAO()
	IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		CLEAR_PED_TASKS_IMMEDIATELY(sRCLauncherDataLocal.pedID[HAO])
		BRING_VEHICLE_TO_HALT(sRCLauncherDataLocal.vehID[HAO], 0.1, 1)
		IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[HAO])
			SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO])
		ENDIF
		SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[HAO], sTrackData.vStartGrid[sTrackData.iNumAIRacers-1])
		SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[HAO], sTrackData.fStartGrid[sTrackData.iNumAIRacers-1])
		CPRINTLN(DEBUG_MISSION, "Placed Hao")
	ENDIF
ENDPROC

PROC SkipToRaceStart()
	RC_START_Z_SKIP()
	
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		CLEAR_PED_TASKS_IMMEDIATELY(sRCLauncherDataLocal.pedID[HAO])
	ENDIF
	
	INT i_index 
	FOR i_index = 0 TO 1
		IF DOES_CAM_EXIST(gridCamera[i_index])
			IF IS_CAM_ACTIVE(gridCamera[i_index])
				SET_CAM_ACTIVE(gridCamera[i_index], FALSE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PLAYER_IN_SUITABLE_RACE_CAR()
			BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), 0.1, 1)
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vHaoDest)
		ELSE
			VEHICLE_INDEX viTemp
			CREATE_VEHICLE_FOR_REPLAY(viTemp, vHaoDest, 1.31, TRUE, TRUE, FALSE, FALSE, TRUE, BUFFALO)
			IF IS_VEHICLE_OK(viTemp)
				SET_VEHICLE_ENGINE_ON(viTemp, TRUE, TRUE)
			ENDIF
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	
	sRaceData.eRaceTrack = STREET_RACE_01
	sRaceData.eRaceType = RACETYPE_CAR
	RACE_INIT(TRUE)
	SETUP_PLAYER_PRECUTSCENE()
	
	WHILE AI_RACERS_DONT_EXIST()
		SPAWN_RACE_LINEUP()
	ENDWHILE
	PLACE_HAO()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	CLEAR_AREA_OF_VEHICLES(GET_PLAYER_COORDS(PLAYER_ID()), 500.0)	// use a big area since ambient vehicles get turned off anyway
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, FALSE)
	ENDIF
	
	missionSubState = SS_CLEANUP
	missionStage = MS_LINEUP
ENDPROC

PROC RestartMission()
	RC_START_Z_SKIP()
	
	CleanupMission(TRUE)
	
	eInitialSceneStage = IS_REQUEST_SCENE
	WHILE NOT SetupScene_HAO_1(sRCLauncherDataLocal)
		WAIT(0)
	ENDWHILE

	ADD_PED_FOR_DIALOGUE(convStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		ADD_PED_FOR_DIALOGUE(convStruct, 3, sRCLauncherDataLocal.pedID[HAO], "HAO")
	ENDIF
	
	SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vPlayerIntroPos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerIntroHeading)
	VEHICLE_INDEX viTemp
	CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-90.91, -1311.97, 28.73>>, 1.31, FALSE, TRUE, FALSE, FALSE, TRUE, BUFFALO)
	
	// Cleanup cameras
	INT i_index 
	FOR i_index = 0 TO 1
		IF DOES_CAM_EXIST(gridCamera[i_index])
			IF IS_CAM_ACTIVE(gridCamera[i_index])
				SET_CAM_ACTIVE(gridCamera[i_index], FALSE)
			ENDIF
		ENDIF
	ENDFOR

	// Set camera behind player
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	// Release countdown timer
	RELEASE_MINIGAME_COUNTDOWN_UI(sRaceHUD.uiCountdown)

	// Enable controls and go to race!
	END_RACE_CUTSCENE()
	
	WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	bGetVehicle = TRUE
	bGoToRace = TRUE
	bReturnToVehicle = TRUE
	//bGetNewVehicle = TRUE
	//bWrongVehicle = TRUE
	
	missionSubState = SS_SETUP
	missionStage = MS_INTRO
	
	RC_END_Z_SKIP()
ENDPROC

// ===========================================================================================================
//		DEBUG FUNCTIONS
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	
	PROC DEBUG_Check_Debug_Keys()
		IF missionStage <> MS_PASS AND missionStage <> MS_FAIL
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				WAIT_FOR_CUTSCENE_TO_STOP()
				Script_Passed()
			ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
				WAIT_FOR_CUTSCENE_TO_STOP()
				missionSubState = SS_SETUP
				missionStage = MS_FAIL
			ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
				SWITCH missionStage 
					CASE MS_INTRO
						// Ending the conversation ends the intro stage
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					BREAK
					CASE MS_GET_CAR
					CASE MS_DRIVE_TO_RACE
					//CASE MS_RETURN_TO_CAR
						SkipToRaceStart()
					BREAK
					CASE MS_LINEUP
						missionSubState = SS_CLEANUP
					BREAK
				ENDSWITCH
			ELIF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
				IF missionStage = MS_RACE
					RC_START_Z_SKIP()
					RACE_CLEANUP(TRUE)
					eInitialSceneStage = IS_REQUEST_SCENE
					WHILE NOT SetupScene_HAO_1(sRCLauncherDataLocal)
						WAIT(0)
					ENDWHILE
					ADD_PED_FOR_DIALOGUE(convStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
						ADD_PED_FOR_DIALOGUE(convStruct, 3, sRCLauncherDataLocal.pedID[HAO], "HAO")
					ENDIF
					SkipToRaceStart()
				ELSE
					RestartMission()
				ENDIF
			ENDIF
			
			INT debugJumpStage = -1
			IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, debugJumpStage)
				IF debugJumpStage = 0
					// restart mission
					RestartMission()
				ELIF debugJumpStage = 1
					// Skip to the race stage
					RC_START_Z_SKIP()
					RACE_CLEANUP(TRUE)
					eInitialSceneStage = IS_REQUEST_SCENE
					WHILE NOT SetupScene_HAO_1(sRCLauncherDataLocal)
						WAIT(0)
					ENDWHILE
					ADD_PED_FOR_DIALOGUE(convStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
						ADD_PED_FOR_DIALOGUE(convStruct, 3, sRCLauncherDataLocal.pedID[HAO], "HAO")
					ENDIF
					SkipToRaceStart()
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF	

// ===========================================================================================================
//		MISSION FUNCTIONS
// ===========================================================================================================
PROC INIT()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup INIT")
			REQUEST_ADDITIONAL_TEXT("HAO1", MISSION_TEXT_SLOT)
			sRaceWaypointRecording = "CityRace0_route1"
			ADD_PED_FOR_DIALOGUE(convStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
				ADD_PED_FOR_DIALOGUE(convStruct, 3, sRCLauncherDataLocal.pedID[HAO], "HAO")
				TASK_CLEAR_LOOK_AT(sRCLauncherDataLocal.pedID[HAO])
			ENDIF
			IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sRCLauncherDataLocal.vehID[HAO], FALSE)
				SET_DISABLE_PRETEND_OCCUPANTS(sRCLauncherDataLocal.vehID[HAO], TRUE)
				SET_VEHICLE_DOORS_LOCKED(sRCLauncherDataLocal.vehID[0], VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			ENDIF
			SET_PED_POPULATION_BUDGET(1)
			SET_REDUCE_PED_MODEL_BUDGET(TRUE)
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
			SUPPRESS_LARGE_VEHICLES(TRUE)
			REQUEST_WAYPOINT_RECORDING(sHaoDestRecording)
			sRaceData.eRaceTrack = STREET_RACE_01
			sRaceData.eRaceType = RACETYPE_CAR
			INIT_TRACK_DATA(sRaceData, sTrackData)
			SETUP_ALT_RIVAL_MODELS()
			//vRaceStart = sTrackData.vStartGrid[sTrackData.iNumAIRacers]
			
			DISABLE_TAXI_HAILING()
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				IF Is_Replay_In_Progress()
					VEHICLE_INDEX viTemp
					INT iReplayStage
					iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
					
					IF g_bShitskipAccepted = TRUE
						iReplayStage++ // player is skipping this stage
					ENDIF
					
					SWITCH iReplayStage
					
						CASE CP_MISSION_START
							//SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-72.4774, -1262.7596, 27.9891>>)
							/*SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<-72.4774, -1262.7596, 27.9891>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 349.5845)*/
							START_REPLAY_SETUP(<<-72.4774, -1262.7596, 27.9891>>, 119.6309)
							/*REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-121.1904, -1302.6024, 26.3543>>, <<-41.3738, -1222.2810, 30.3348>>)
							SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-121.1904, -1302.6024, 26.3543>>, <<-41.3738, -1222.2810, 30.3348>>, FALSE)
							CLEAR_AREA_OF_VEHICLES(vPosNearbyCar, 25.0)
							DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(vPosNearbyCar, 25.0)*/
							
							VEHICLE_INDEX viStartVehicle
							VECTOR vSafehouse
							FLOAT fSafehouse
							viStartVehicle = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH)
							IF IS_VEHICLE_OK(viStartVehicle)
								IF GET_CAR_RESPOT_POS_FOR_NEAREST_SAVEHOUSE(GET_ENTITY_COORDS(viStartVehicle), CHAR_FRANKLIN, vSafehouse, fSafehouse)
									SET_ENTITY_COORDS(viStartVehicle, vSafehouse)
									SET_ENTITY_HEADING(viStartVehicle, fSafehouse)
								ENDIF
							ENDIF
							
							//SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-121.1904, -1302.6024, 26.3543>>, <<-41.3738, -1222.2810, 30.3348>>, TRUE)
							
							CREATE_VEHICLE_FOR_REPLAY(viTemp, vVehicleIntroPos, fVehicleIntroHeading, FALSE, TRUE, FALSE, FALSE, TRUE, BUFFALO)
							//WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
								STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.vehID[HAO], INSTANT_BLEND_OUT, TRUE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[HAO], INSTANT_BLEND_OUT, TRUE)
								WAIT(0)
							ENDIF
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
							AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[HAO])
								SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[HAO],sRCLauncherDataLocal.vehID[HAO])
								SET_ENTITY_COORDS_GROUNDED(sRCLauncherDataLocal.vehID[HAO], <<-89.0261, -1260.9956, 28.2992>>)
								SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[HAO], 112.5636)
								SET_VEHICLE_ENGINE_ON(sRCLauncherDataLocal.vehID[HAO], TRUE, TRUE)
								SET_VEHICLE_FORWARD_SPEED(sRCLauncherDataLocal.vehID[HAO], 5.0)
							ENDIF
							/*CreateNearbyCar()
							WHILE NOT DOES_ENTITY_EXIST(viNearbyCar)
								CreateNearbyCar()
								WAIT(0)
							ENDWHILE*/
							END_REPLAY_SETUP()
							RC_END_Z_SKIP()
							missionSubState = SS_CLEANUP
							missionStage = MS_INTRO
						BREAK
						
						CASE CP_RACE_START
							START_REPLAY_SETUP(vHaoDest, 1.31)
							//SkipToRaceStart()
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_PLAYER_IN_SUITABLE_RACE_CAR()
								viTemp = GET_PLAYERS_LAST_VEHICLE()
								BRING_VEHICLE_TO_HALT(viTemp, 0.1, 1)
							ELSE
								CREATE_VEHICLE_FOR_REPLAY(viTemp, vHaoDest, 1.31, FALSE, TRUE, FALSE, FALSE, TRUE, BUFFALO)
								IF IS_VEHICLE_OK(viTemp)
									SET_VEHICLE_ENGINE_ON(viTemp, TRUE, TRUE)
								ENDIF
							ENDIF
							END_REPLAY_SETUP(viTemp)
							IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.pedID[HAO], "special_ped@hao@base", "hao_base", ANIM_SYNCED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.pedID[HAO], INSTANT_BLEND_OUT, TRUE)
							ENDIF
							IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO]) AND IS_ENTITY_PLAYING_ANIM(sRCLauncherDataLocal.vehID[HAO], "special_ped@hao@base", "hao_base_penumbra", ANIM_SYNCED_SCENE)
								STOP_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.vehID[HAO], INSTANT_BLEND_OUT, TRUE)
							ENDIF
							CLEAR_AREA_OF_VEHICLES(GET_PLAYER_COORDS(PLAYER_ID()), 500.0)	// use a big area since ambient vehicles get turned off anyway
							sRaceData.eRaceTrack = STREET_RACE_01
							sRaceData.eRaceType = RACETYPE_CAR
							RACE_INIT(TRUE)
							SETUP_PLAYER_PRECUTSCENE()
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							START_RACE_CUTSCENE()
							WHILE AI_RACERS_DONT_EXIST()
								SPAWN_RACE_LINEUP()
							ENDWHILE
							PLACE_HAO()
							PLACE_RACERS_FOR_RESTART()
							IF NOT IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
								CPRINTLN(DEBUG_MISSION, "Hao is not available")
							ENDIF
							IF NOT IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
								CPRINTLN(DEBUG_MISSION, "Hao's car is not available")
							ENDIF
							
							missionSubState = SS_CLEANUP
							missionStage = MS_LINEUP
						BREAK
						
						CASE CP_MISSION_PASSED
							START_REPLAY_SETUP(<<-29.86, -1833.06, 25.37>>, 234.16)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vHaoDest)
								viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF IS_VEHICLE_OK(viTemp)
									/*SET_ENTITY_COORDS(viTemp, <<-29.86, -1833.06, 25.37>>)
									SET_ENTITY_HEADING(viTemp, 234.16)*/
								ELSE
									CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-29.86, -1833.06, 25.37>>, 234.16, FALSE, FALSE, FALSE, FALSE, TRUE, BUFFALO)
								ENDIF
							ELSE
								CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-29.86, -1833.06, 25.37>>, 234.16, FALSE, FALSE, FALSE, FALSE, TRUE, BUFFALO)
							ENDIF
							END_REPLAY_SETUP(viTemp)
							//WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
							RC_END_Z_SKIP()
							missionSubState = SS_SETUP
							missionStage = MS_PASS
						BREAK
						
						DEFAULT
							SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
			            BREAK
					ENDSWITCH
						
				ELSE
					missionSubState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup INIT")
			missionStage = MS_PRE_INTRO
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC

PROC PRE_INTRO()
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	RC_REQUEST_CUTSCENE("hao_mcs_1")
	
	SWITCH missionSubState
		CASE SS_SETUP
			REQUEST_ANIM_DICT(sLeadInDict)
			
			IF HAS_ANIM_DICT_LOADED(sLeadInDict) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[0]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[0])
				INT iBone
				iBone       = GET_ENTITY_BONE_INDEX_BY_NAME(sRCLauncherDataLocal.vehID[0], "chassis_dummy")
				VECTOR vChassisPos
				vChassisPos = GET_WORLD_POSITION_OF_ENTITY_BONE(sRCLauncherDataLocal.vehID[0], iBone)
				iLeadIn = CREATE_SYNCHRONIZED_SCENE(vChassisPos, GET_ENTITY_ROTATION(sRCLauncherDataLocal.vehID[0]))
				SET_SYNCHRONIZED_SCENE_PHASE(iLeadIn, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iLeadIn,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iLeadIn,TRUE)
				TASK_SYNCHRONIZED_SCENE(sRCLauncherDataLocal.pedID[0], iLeadIn, sLeadInDict, "hao_leadin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
				//PLAY_SYNCHRONIZED_ENTITY_ANIM(sRCLauncherDataLocal.vehID[0], iLeadIn, "hao_leadin_penumbra", sLeadInDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				TASK_LOOK_AT_ENTITY(sRCLauncherDataLocal.pedID[0], PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				SET_GAMEPLAY_ENTITY_HINT(sRCLauncherDataLocal.pedID[0], <<0,0,0>>)
				
				missionSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iLeadIn)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLeadIn) >= 0.9
					missionSubState = SS_CLEANUP
				ENDIF
			ELSE
				missionSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup Pre intro")
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			missionStage = MS_INTRO
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC

PROC INTRO()
	//CreateNearbyCar()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup INTRO")
			RC_REQUEST_CUTSCENE("hao_mcs_1")
			IF RC_IS_CUTSCENE_OK_TO_START()
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.pedID[HAO], "Hao", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
					REGISTER_ENTITY_FOR_CUTSCENE(sRCLauncherDataLocal.vehID[HAO], "hao_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				SAFE_FADE_SCREEN_IN_FROM_BLACK(500, FALSE)
				RC_CLEANUP_LAUNCHER()
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				WAIT(0)
				SAFE_DELETE_OBJECT(sRCLauncherDataLocal.objID[HAO])
				FLOAT fCurrentHeading
				VEHICLE_INDEX viPlayer
				viPlayer = GET_PLAYERS_LAST_VEHICLE()
				fCurrentHeading = 0
				IF IS_VEHICLE_OK(viPlayer)
					fCurrentHeading = GET_ENTITY_HEADING(viPlayer)
				ENDIF
				IF fCurrentHeading > 0 AND fCurrentHeading < 180
					fVehicleIntroHeading =  90.3906
				ELSE
					fVehicleIntroHeading =  270.3906
				ENDIF
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaPos1, vIntroAreaPos2, fIntroAreaWidth, vVehicleIntroPos, fVehicleIntroHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaBPos1, vIntroAreaBPos2, fIntroAreaBWidth, vVehicleIntroPos, fVehicleIntroHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vIntroAreaCPos1, vIntroAreaCPos2, fIntroAreaCWidth, vVehicleIntroPos, fVehicleIntroHeading, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
				IF viPlayer <> sRCLauncherDataLocal.vehID[1]
					IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[1])
						SET_ENTITY_COORDS_GROUNDED(sRCLauncherDataLocal.vehID[1], <<-91.07, -1273.45, 28.86>>)
						SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[1], 0.57)
					ENDIF
				ENDIF
				//CLEAR_AREA_OF_VEHICLES(<<-91.07, -1273.45, 28.86>>, 300)
				//CLEAR_ANGLED_AREA_OF_VEHICLES(vClearAmbientVehiclesAreaPos1, vClearAmbientVehiclesAreaPos2, fClearAmbientVehiclesAreaWidth)
				SET_ROADS_IN_ANGLED_AREA(vClearAmbientVehiclesAreaPos1, vClearAmbientVehiclesAreaPos2, fClearAmbientVehiclesAreaWidth, FALSE, FALSE)
				RC_START_CUTSCENE_MODE(vPlayerIntroPos)
				
				missionSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Hao") AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("hao_car")
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[HAO],sRCLauncherDataLocal.vehID[HAO])
					SET_VEHICLE_DOOR_SHUT(sRCLauncherDataLocal.vehID[HAO], SC_DOOR_FRONT_LEFT)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-72.4639, -1261.2433, 28.0930>>)
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				RC_END_CUTSCENE_MODE()
				missionSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			/*IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 7 AND NOT IsPedPerformingTask(sRCLauncherDataLocal.pedID[HAO], SCRIPT_TASK_ENTER_VEHICLE)
			AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
			AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[HAO])
				TASK_ENTER_VEHICLE(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_WALK)
			ENDIF
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				SAFE_FADE_SCREEN_OUT_TO_BLACK()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
				AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[HAO])
					SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[HAO],sRCLauncherDataLocal.vehID[HAO]) 
				ENDIF
				WAIT(1000)
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				CPRINTLN(DEBUG_MISSION, "Cleanup INTRO")
				
				IF DOES_CAM_EXIST(camCutscene)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_CAM_ACTIVE(camCutscene, FALSE)
					DESTROY_CAM(camCutscene)
				ENDIF
				
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				RC_END_CUTSCENE_MODE()
				RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				bGetVehicle = TRUE
				bGoToRace = TRUE
				
				SAFE_FADE_SCREEN_IN_FROM_BLACK()
				missionStage = MS_GET_CAR
				missionSubState = SS_SETUP
			ENDIF*/
			
			REPLAY_STOP_EVENT()
			
			RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vClearAmbientVehiclesAreaPos1, vClearAmbientVehiclesAreaPos2, fClearAmbientVehiclesAreaWidth)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillTakeDamageWhenVehicleCrashes, FALSE)
			ENDIF
			
			bGetVehicle = TRUE
			bGoToRace = TRUE
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			missionStage = MS_GET_CAR
			missionSubState = SS_SETUP
			
			SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_SHOP_CARMOD_01_AP, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_UNSUITABLE_VEHICLE()
	IF NOT IS_PLAYER_IN_SUITABLE_RACE_CAR()
		//IF viPlayerLast <> viPlayerMain
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> viPlayerLastUnsuitable //bWrongVehicle
				PRINT_HELP("HAO1_4")
				//viPlayerLast = viPlayerMain
				//bWrongVehicle = FALSE
				viPlayerLastUnsuitable = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAO1_4")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks the player's wanted level, sends them to the LOSE_COPS state if it's greater than zero
PROC CHECK_FOR_COPS()
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		missionStage = MS_LOSE_COPS
		missionSubState = SS_SETUP
	ENDIF
ENDPROC

/// PURPOSE:
///    Emergency stop for Hao in case AI fails to stop him at the end of his waypoint recording
PROC HAO_EMERGENCY_STOP()
	IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[HAO])
			AND GET_IS_WAYPOINT_RECORDING_LOADED(sHaoDestRecording)
				INT wayP
				IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sHaoDestRecording, GET_ENTITY_COORDS(sRCLauncherDataLocal.pedID[HAO]), wayP)
					IF wayP >= 54
						STOP_PLAYBACK_RECORDED_VEHICLE(sRCLauncherDataLocal.vehID[HAO])
						TASK_VEHICLE_TEMP_ACTION(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO], TEMPACT_BRAKE, 500)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_CAR()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 350.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	ENDIF
	SPAWN_RACE_LINEUP()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup GET_CAR")
			REQUEST_WAYPOINT_RECORDING(sHaoDestRecording)
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sHaoDestRecording)
				IF bGetVehicle
					RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
					IF IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO]) AND IS_PED_UNINJURED(PLAYER_PED_ID())
					AND CREATE_CONVERSATION(convStruct, "HAO1AU", "HAO1_MCS1LO", CONV_PRIORITY_HIGH)
						IF IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
							/*SET_ENTITY_LOAD_COLLISION_FLAG(sRCLauncherDataLocal.pedID[HAO], TRUE)
							SEQUENCE_INDEX siHao
							OPEN_SEQUENCE_TASK(siHao)
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(NULL, sRCLauncherDataLocal.vehID[HAO], sHaoDestRecording, DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
								TASK_PAUSE(NULL, -1)
							CLOSE_SEQUENCE_TASK(siHao)
							TASK_PERFORM_SEQUENCE(sRCLauncherDataLocal.pedID[HAO], siHao)
							CLEAR_SEQUENCE_TASK(siHao)*/
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO], sHaoDestRecording, DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
						ENDIF
						REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vClearAreaMin, vClearAreaMax)
						REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vRaceStart-<<300,300,300>>, vRaceStart+<<300,300,300>>)
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vClearAreaMin, vClearAreaMax, FALSE)
						SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vRaceStart-<<300,300,300>>, vRaceStart+<<300,300,300>>, FALSE)
						SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, FALSE)
						SET_ROADS_IN_ANGLED_AREA(vDisableRoadsBigPos1, vDisableRoadsBigPos2, fDisableRoadsBigWidth, FALSE, FALSE)
						//SET_ROADS_IN_ANGLED_AREA(vDisableRoadPos1, vDisableRoadPos2, fDisableRoadWidth, FALSE, FALSE)
						
						SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>, 0.0, TRUE, CHAR_FRANKLIN)
						
						SET_ROADS_IN_ANGLED_AREA(vApproachRoadPos1, vApproachRoadPos2, fApproachRoadWidth, FALSE, FALSE)
						
						SAFE_REMOVE_BLIP(biDestination)
						
						CLEAR_PRINTS()
						missionSubState = SS_UPDATE
					ENDIF
				ELSE
					missionSubState = SS_UPDATE_2
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF bGetVehicle
					PRINT_NOW("HAO1_1", DEFAULT_GOD_TEXT_TIME, 1)	// Get a car.
					bGetVehicle = FALSE
				/*ELIF bGetNewVehicle
					PRINT_NOW("HAO1_5", DEFAULT_GOD_TEXT_TIME, 1)	// Get a new car.
					bGetNewVehicle = FALSE*/
				ENDIF
				missionSubState = SS_UPDATE_2
			ENDIF
		BREAK
		
		CASE SS_UPDATE_2
			IF NOT (IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO]) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[HAO], TRUE))
				IF IS_PLAYER_IN_SUITABLE_RACE_CAR()
					CLEAR_HELP()
					//viPlayerLast = viPlayerMain
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0)
					missionSubState = SS_CLEANUP
				ELSE
					PRINT_UNSUITABLE_VEHICLE()
				ENDIF
			ENDIF
			PLAY_WAITING_CONV()
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup GET_CAR")
			missionStage = MS_DRIVE_TO_RACE
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
	
	CHECK_FOR_COPS()
	HAO_EMERGENCY_STOP()
ENDPROC

/// PURPOSE:
///    Checks if the player has arrived at a weird angle or if they've blocked the race start. If they've done either of these things we need to play the cutscene to tidy things up
/// RETURNS:
///    TRUE if it's safe to continue without using the cutscene
FUNC BOOL DONT_NEED_CUTSCENE()
	IF IS_ANY_VEHICLE_NEAR_POINT(<<-168.426910,-1582.630981,33.966496>>, 20.0)
		CPRINTLN(DEBUG_MISSION, "Vehicle is blocking the start grid")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBlockingHaoOne, vBlockingHaoTwo, fBlockingHao)
		CPRINTLN(DEBUG_MISSION, "Player is in Hao's spot")
		RETURN FALSE
	ENDIF
	
	FLOAT fTargetHeading = sTrackData.fStartGrid[sTrackData.iNumAIRacers]
	FLOAT fCurrentHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	FLOAT fHeadingDiff
	
	WHILE fTargetHeading < 0
		fTargetHeading += 360
	ENDWHILE
	WHILE fCurrentHeading < 0
		fCurrentHeading += 360
	ENDWHILE
	
	fHeadingDiff = fTargetHeading - fCurrentHeading
	IF fHeadingDiff < 20 AND fHeadingDiff > -20
		CPRINTLN(DEBUG_MISSION, "Player approached at a reasonable angle")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "Player approached at a crazy angle")
	RETURN FALSE
ENDFUNC

PROC DRIVE_TO_RACE()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 350.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	ENDIF
	IF bClearVehicles AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 300)
		CLEAR_AREA_OF_VEHICLES(vRaceStart, 150.0)	// use a big area since ambient vehicles get turned off anyway
		bClearVehicles = FALSE
	ENDIF
	SPAWN_RACE_LINEUP()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup DRIVE_TO_RACE")
			SAFE_REMOVE_BLIP(biDestination)
			
			biDestination = CREATE_COORD_BLIP(vRaceStart)
			
			//viPlayerLast = viPlayerMain
			
			bReturnToVehicle = TRUE
			bClearVehicles = TRUE
			bPlaceHao = TRUE
			
			iAtStartTimer = 0
			
			//viPlayerMain = GET_PLAYERS_LAST_VEHICLE()
			
			CLEAR_PRINTS()
			IF bGoToRace
				PRINT_NOW("HAO1_2", DEFAULT_GOD_TEXT_TIME, 1)	// Go to the ~y~race.
				bGoToRace = FALSE
			ENDIF
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRaceStart, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
			//OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 20.0)
			OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 4.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vBlockingHaoOne, vBlockingHaoTwo, fBlockingHao)
				IF IS_PLAYER_IN_SUITABLE_RACE_CAR()
					//IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viPlayerMain)
						missionSubState = SS_CLEANUP
					//ENDIF
				ELSE
					IF bReturnToVehicle
						PRINT_NOW("HAO1_3", DEFAULT_GOD_TEXT_TIME, 1)
						bReturnToVehicle = FALSE
					ENDIF
					PLAY_WAITING_CONV()
				ENDIF
			ELSE
				PLAY_WAITING_CONV()
			ENDIF
			PRINT_UNSUITABLE_VEHICLE()
			
			IF bPlaceHao AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO])
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(sRCLauncherDataLocal.vehID[HAO])
					IF IS_ENTITY_OCCLUDED(sRCLauncherDataLocal.vehID[HAO])
						SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[HAO], sTrackData.vStartGrid[sTrackData.iNumAIRacers-1])
						SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[HAO], sTrackData.fStartGrid[sTrackData.iNumAIRacers-1])
						bPlaceHao = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup DRIVE_TO_RACE")
			IF IS_VEHICLE_OK(viPlayerMain)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viPlayerMain, 3.5)	// There's a vehicle in front of where the player stops so using a shorter distance than the default
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
					
					sRaceData.eRaceTrack = STREET_RACE_01
					sRaceData.eRaceType = RACETYPE_CAR
					RACE_INIT(TRUE, FALSE)
					missionSubState = SS_SETUP
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					IF DONT_NEED_CUTSCENE()
						bHeadingIsOK = TRUE
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						//SET_GAMEPLAY_COORD_HINT(sTrackData.vCheckPoint[0])
						SET_VEHICLE_IS_RACING(viPlayerMain, TRUE)
						missionStage = MS_COUNTDOWN
					ELSE
						bHeadingIsOK = FALSE
						missionStage = MS_LINEUP
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	CHECK_FOR_COPS()
	HAO_EMERGENCY_STOP()
ENDPROC

PROC LOSE_COPS()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vRaceStart, 350.0)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
	ENDIF
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup LOSE_COPS")
			SAFE_REMOVE_BLIP(biDestination)
			CLEAR_PRINTS()
			PRINT_NOW("HAO1_6", DEFAULT_GOD_TEXT_TIME, 1)	// Lose the cops.
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				missionSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup LOSE_COPS")
			IF bGoToRace
				// If bGoToRace is true, the player hadn't got a suitable vehicle before getting a wanted level, check their car
				IF IS_PLAYER_IN_SUITABLE_RACE_CAR()
					missionStage = MS_DRIVE_TO_RACE
					missionSubState = SS_SETUP
				ELSE
					missionStage = MS_GET_CAR
					missionSubState = SS_SETUP
				ENDIF
			ELSE
				// If bGoToRace is false, the player had already got a suitable vehicle so send them to the race
				missionStage = MS_DRIVE_TO_RACE
				missionSubState = SS_SETUP
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/*PROC RETURN_TO_CAR()
	
	SPAWN_RACE_LINEUP()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup RETURN_TO_CAR")
			SAFE_REMOVE_BLIP(biDestination)
			
			CLEAR_PRINTS()
			IF IS_VEHICLE_OK(viPlayerMain)
				biDestination = CREATE_VEHICLE_BLIP(viPlayerLast)
				IF bReturnToVehicle
					PRINT_NOW("HAO1_3", DEFAULT_GOD_TEXT_TIME, 1)	// Get back in your ~b~car.
					bReturnToVehicle = FALSE
				ENDIF
				missionSubState = SS_UPDATE
			ELSE
				missionStage = MS_GET_CAR
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			IF NOT IS_VEHICLE_OK(viPlayerLast)
				missionSubState = SS_SETUP
				missionStage = MS_GET_CAR
			ENDIF
			
			IF NOT (IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO]) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sRCLauncherDataLocal.vehID[HAO], TRUE))
				IF IS_PLAYER_IN_SUITABLE_RACE_CAR()
					missionSubState = SS_CLEANUP
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> viPlayerLastUnsuitable //bWrongVehicle
						PRINT_HELP("HAO1_4")
						//bWrongVehicle = FALSE
						viPlayerLastUnsuitable = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup RETURN_TO_CAR")
			sRaceData.eRaceTrack = STREET_RACE_01
			sRaceData.eRaceType = RACETYPE_CAR
			RACE_INIT(TRUE)
			missionStage = MS_DRIVE_TO_RACE
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
ENDPROC*/

PROC RACE_LINEUP()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup RACE_LINEUP")
			SAFE_REMOVE_BLIP(biDestination)
			
			// Start cutscene
			START_RACE_CUTSCENE()
			
			// Remove the initial scene car if the player isn't using it
			IF GET_PLAYERS_LAST_VEHICLE() <> sRCLauncherDataLocal.vehID[1]
				SAFE_DELETE_VEHICLE(sRCLauncherDataLocal.vehID[1])
			ENDIF
			
			// Setup vehicles
			SETUP_PLAYER_PRECUTSCENE()
			PLACE_HAO()
			
			// Setup initial camera
			CREATE_RACE_CAMERAS()
			
			//SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			
			SETUP_GRID_GIRLS()
			
			bPlayedIntroSound = FALSE
			bIntroSkipped = FALSE
			
			//RC_END_Z_SKIP()
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_RACE_START, "Race start", TRUE)
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			
			IF CONTROL_RACE_CAMERAS()
				SET_GRID_GIRLS_FOR_COUNTDOWN(0.32)
				missionStage = MS_COUNTDOWN
				missionSubState = SS_SETUP
			ENDIF
				
			// Camera running?
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				SAFE_FADE_SCREEN_OUT_TO_BLACK()
				bIntroSkipped = TRUE
				SETUP_PLAYER_COUNTDOWN()
				INT i_index
				FOR i_index = 0 TO sTrackData.iNumAIRacers-1
					IF IS_PED_UNINJURED(sRacer[i_index].piDriver)
						CLEAR_PED_TASKS(sRacer[i_index].piDriver)
					ENDIF
					IF IS_VEHICLE_OK(sRacer[i_index].viCar)
						BRING_VEHICLE_TO_HALT(sRacer[i_index].viCar, 0.1, 1)
						SET_ENTITY_COORDS_GROUNDED(sRacer[i_index].viCar, sTrackData.vStartGrid[i_index])
						SET_ENTITY_HEADING(sRacer[i_index].viCar, sTrackData.fStartGrid[i_index])
					ENDIF
				ENDFOR
				SAFE_DELETE_PED(piGridGirlTwo)
				SAFE_DELETE_PED(piGridGirlThree)
				//WAIT(1000)
				missionSubState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup RACE_LINEUP")
			
			SETUP_PLAYER_COUNTDOWN()
			SET_GRID_GIRLS_FOR_COUNTDOWN(0.27)
	
			// Set camera behind player
			IF bIntroSkipped
				SETUP_PLAYER_COUNTDOWN()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				WAIT(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			/*ELSE
				//RENDER_SCRIPT_CAMS(FALSE, TRUE)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()*/
			ENDIF
			
			// Cleanup cameras
			INT i_index
			i_index = 0
			REPEAT COUNT_OF(gridCamera) i_index
				IF DOES_CAM_EXIST(gridCamera[i_index])
					DESTROY_CAM(gridCamera[i_index])
				ENDIF
			ENDREPEAT
			
			// Got to race countdown
			missionStage = MS_COUNTDOWN
			missionSubState = SS_SETUP
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		BREAK
	ENDSWITCH
ENDPROC

PROC HAO_ARRIVES_LATE()
	IF bHaoArrivesLate AND IS_VEHICLE_OK(sRCLauncherDataLocal.vehID[HAO]) AND IS_PED_UNINJURED(sRCLauncherDataLocal.pedID[HAO])
		IF NOT IS_ENTITY_IN_RANGE_COORDS(sRCLauncherDataLocal.vehID[HAO], sTrackData.vStartGrid[sTrackData.iNumAIRacers-1], 10.0)
			CLEAR_PED_TASKS_IMMEDIATELY(sRCLauncherDataLocal.pedID[HAO])
			BRING_VEHICLE_TO_HALT(sRCLauncherDataLocal.vehID[HAO], 0.1, 1)
			IF NOT IS_PED_IN_VEHICLE(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO]) AND IS_VEHICLE_SEAT_FREE(sRCLauncherDataLocal.vehID[HAO])
				SET_PED_INTO_VEHICLE(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO])
			ENDIF
			VECTOR behind_pos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sTrackData.vStartGrid[sTrackData.iNumAIRacers-1], sTrackData.fStartGrid[sTrackData.iNumAIRacers-1], <<0.0, -5.0, 0.0>>)
			VECTOR targetPos
			targetPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sTrackData.vStartGrid[sTrackData.iNumAIRacers-1], sTrackData.fStartGrid[sTrackData.iNumAIRacers-1], <<0.0, 2.3, 0.0>>)
			SET_ENTITY_COORDS(sRCLauncherDataLocal.vehID[HAO], behind_pos)
			SET_ENTITY_HEADING(sRCLauncherDataLocal.vehID[HAO], sTrackData.fStartGrid[sTrackData.iNumAIRacers-1])
			SET_VEHICLE_ON_GROUND_PROPERLY(sRCLauncherDataLocal.vehID[HAO])
			SET_VEHICLE_FORWARD_SPEED(sRCLauncherDataLocal.vehID[HAO], 5.0)
			TASK_VEHICLE_DRIVE_TO_COORD(sRCLauncherDataLocal.pedID[HAO], sRCLauncherDataLocal.vehID[HAO], targetPos, 5.0, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(sRCLauncherDataLocal.vehID[HAO]), DF_ForceStraightLine|DRIVINGMODE_PLOUGHTHROUGH, 0.5, 15.0)
			bHaoArrivesLate = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC RACE_COUNTDOWN()
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	CONTROL_RACE_CAMERAS()
	DISABLE_SELECTOR_THIS_FRAME()
	
	SWITCH missionSubState
		CASE SS_SETUP
			SAFE_REMOVE_BLIP(biDestination)
			CPRINTLN(DEBUG_MISSION, "Setup RACE_COUNTDOWN")
			REQUEST_MINIGAME_COUNTDOWN_UI(sRaceHUD.uiCountdown)
			REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO", TRUE)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_RACE_START, "Race start", TRUE)
			SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SAVEHOUSE, FALSE)
			SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, FALSE)
			bRacersReleased = FALSE
			bHaoArrivesLate = TRUE
			iBoostTimer = -1
			eBoostState = BS_READY
			CLEAR_BITMASK_AS_ENUM(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_Go)
			CLEAR_BITMASK_AS_ENUM(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_1)
			CLEAR_BITMASK_AS_ENUM(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_2)
			CLEAR_BITMASK_AS_ENUM(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_3)
			CANCEL_TIMER(sRaceHUD.uiCountdown.CountdownTimer)
			
			IF IS_VEHICLE_OK(sPlayerVehicle.vehPlayerVehicle)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_VEHICLE_HANDBRAKE(sPlayerVehicle.vehPlayerVehicle, TRUE)
			ENDIF
			
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "RACE_REACH_START", "FRANKLIN_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
			ENDIF
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			REQUEST_SCRIPT_AUDIO_BANK("HUD_321_GO", TRUE)
					
			IF HAS_MINIGAME_COUNTDOWN_UI_LOADED(sRaceHUD.uiCountdown)
				
				UPDATE_RACE_CHECKPOINTS(FALSE)	
			
				IF UPDATE_MINIGAME_COUNTDOWN_UI(sRaceHUD.uiCountdown, TRUE, FALSE, FALSE, 3, TRUE)
					missionSubState = SS_CLEANUP
				ENDIF
				
				IF bHeadingIsOK
					IF IS_BITMASK_AS_ENUM_SET(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_2)
						HAO_ARRIVES_LATE()
					ENDIF
				ENDIF
				
				// See B*929855 - allow racers to move as soon as "GO" appears
				IF IS_BITMASK_AS_ENUM_SET(sRaceHUD.uiCountdown.iBitFlags, CNTDWN_UI_Played_Go)
					IF iBoostTimer < 0
						iBoostTimer = GET_GAME_TIMER() + BOOST_START_WINDOW
					ENDIF
					IF bRacersReleased = FALSE
						bRacersReleased = TRUE
						INIT_RACE_HUD()
						PLAYER_INDEX pId
						pId = GET_PLAYER_INDEX()
						SET_PLAYER_CONTROL(pId, TRUE)
						IF IS_VEHICLE_OK(sPlayerVehicle.vehPlayerVehicle)
							SET_VEHICLE_HANDBRAKE(sPlayerVehicle.vehPlayerVehicle, FALSE)
						ENDIF
						SET_FRONTEND_RADIO_ACTIVE(true)
						IF sRaceData.eRaceType = RACETYPE_SEA
							TRIGGER_MUSIC_EVENT("MGSR_START")
						ENDIF
						START_RACE_MIXER()
						CPRINTLN(DEBUG_MISSION, "Street Race: Starting race at game time ", GET_GAME_TIMER())
					ENDIF
					UPDATE_RACE_AI(FALSE)
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup RACE_COUNTDOWN")
			
			// Release countdown timer
			RELEASE_MINIGAME_COUNTDOWN_UI(sRaceHUD.uiCountdown)
			
			UPDATE_RACE_CHECKPOINTS(FALSE)
			
			/*IF bHeadingIsOK
				STOP_GAMEPLAY_HINT()
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			ENDIF*/
	
			// Enable controls and go to race!
			END_RACE_CUTSCENE()
			
			missionStage = MS_RACE
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
	
	HANDLE_START_BOOST()
ENDPROC

FUNC BOOL RIVAL_FINISHED()
	INT i_index
	FOR i_index = 0 TO sTrackData.iNumAIRacers-1
		IF sRacer[i_index].iCurrentLap > sTrackData.iNumLaps
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC CHECK_COLLISION_STAT()
	IF GET_GAME_TIMER() > iCollisionTimer
		IF HAS_PLAYER_COLLIDED_WITH_RACER()
			INFORM_MISSION_STATS_OF_INCREMENT(HAO1_COLLISIONS)
			iCollisionTimer = GET_GAME_TIMER() + 1000
		ENDIF
	ENDIF
ENDPROC

PROC SHOW_LOOK_BACK_HELP()
	IF bLookBackHelpReady AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		// Show the How to Look Back help text the first time the player is third or higher
		IF sRaceHUD.iPlayerPosition <= 3
			PRINT_HELP("HAO1_LOOK_HELP")
			bLookBackHelpReady = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC SHOW_SLIPSTREAM_HELP()
	IF bSlipStreamHelpReady AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND g_savedGlobals.sStreetRaceData.iSlipstreamHelpCount < 3
		PRINT_HELP("FM_IHELP_SLP") // You can slipstream by driving close behind another racer. This will give you a speed boost.
		bSlipStreamHelpReady = FALSE
		g_savedGlobals.sStreetRaceData.iSlipstreamHelpCount++
	ENDIF
ENDPROC

PROC SHOW_ABILITY_HELP()
	IF g_savedGlobals.sStreetRaceData.bSpecialAbilityHelp = FALSE
		IF sRaceHUD.iCurrentCheckPoint >= 4 AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED() AND IS_REPLAY_IN_PROGRESS()
			IF GET_PLAYER_SPECIAL_ABILITY_VALUE() > 0
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("HAO1_ABILITY_HELP_KM")
				ELSE
					PRINT_HELP("HAO1_ABILITY_HELP")
				ENDIF
			ENDIF
			g_savedGlobals.sStreetRaceData.bSpecialAbilityHelp = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC RACE_ACTIVE()
	IF sRaceHUD.iCurrentLap = 1 AND sRaceHUD.iCurrentCheckPoint < 3
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ELSE
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
	ENDIF
	
	HIDE_STREET_AND_CAR_NAMES_THIS_FRAME()
	
	DISABLE_SELECTOR_THIS_FRAME()
	
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup RACE_ACTIVE")
			
			STOP_SOUND(iIntroSound)
			
			// Used for respawning the player's car if it become stuck
			racesRespawnState = RACES_RESPAWN_SET
			iPlayerWantsToRecoverVehicleTimer = -1
			
			PRINT_HELP("HAO1_LAP_HELP")
			
			iPreviousPosition = sRaceHUD.iPlayerPosition
			iPositionConvTimer = GET_GAME_TIMER() + POSITION_CONV_TIME
			
			UPDATE_RACE_CHECKPOINTS()
			
			SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SAVEHOUSE, FALSE)
			SET_STATIC_BLIP_CATEGORY_VISIBILITY(STATIC_BLIP_CATEGORY_SHOP, FALSE)
			
			bLookBackHelpReady = TRUE
			bSlipStreamHelpReady = TRUE
			
			INIT_STOPPED_SOUND()
			
			iRamInRaceTimer = 0
			iCloseInRaceTimer = GET_GAME_TIMER() + 10000
			
			// Start Stats tracking
			iCollisionTimer = 0
			/*iNumCollisions = 0
			iPreviousCollisions = iNumCollisions*/
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(HAO1_RACE_TIME)
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			
			UPDATE_RACE_AI(TRUE, TRUE)
			UPDATE_PLAYER_POS()
			//CAM_HINT_FOR_NEXT_CAR()
			UPDATE_RACE_HUD()
			CHECK_RACE_CONVS()
			SHOW_LOOK_BACK_HELP()
			SHOW_SLIPSTREAM_HELP()
			SHOW_ABILITY_HELP()
			
			IF iReplayRecordWarpTimer > GET_GAME_TIMER()
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2226208
			ENDIF
			
			IF RIVAL_FINISHED() AND sRaceHUD.iPlayerPosition > 1
				missionFailReason = MFR_DIDNT_WIN
				missionStage = MS_FAIL
				missionSubState = SS_SETUP
				EXIT
			ENDIF
			
			/*IF iNumCollisions < iPreviousCollisions
				INFORM_MISSION_STATS_OF_INCREMENT(HAO1_COLLISIONS)
			ENDIF*/
			CHECK_COLLISION_STAT()
			
			PLAY_RAM_CONV()
			PLAY_CLOSE_CONV()

			IF IS_PLAYER_IN_VEHICLE()
			AND UPDATE_RACE_CHECKPOINTS()
				missionSubState = SS_CLEANUP
				STOP_AUDIO_SCENE("STREET_RACE_DURING_RACE")
				START_AUDIO_SCENE("STREET_RACE_OUTRO")
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 2, REPLAY_IMPORTANCE_HIGHEST) 
			CPRINTLN(DEBUG_MISSION, "Cleanup RACE_ACTIVE")
			IF sRaceHUD.iPlayerPosition = 1
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, HAO1_RACE_TIME)
				missionStage = MS_PASS
			ELSE
				missionFailReason = MFR_DIDNT_WIN
				missionStage = MS_FAIL
			ENDIF
			missionSubState = SS_SETUP
		BREAK
	ENDSWITCH
	
	HANDLE_START_BOOST()
	CONTROL_RACE_FAIL()
ENDPROC

PROC WAIT_FOR_PASS()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup WAIT_FOR_PASS")
			CLEANUP_RACERS(FALSE)
			CLEANUP_RACER_BLIPS()
			CLEANUP_CHECKPOINTS()
			IF IS_REPEAT_PLAY_ACTIVE()
				Script_Passed()
			ELSE
				missionSubState = SS_UPDATE
			ENDIF
		BREAK
		
		CASE SS_UPDATE
			iCallDelayTimer = GET_GAME_TIMER() + 2000
			missionSubState = SS_CLEANUP
		BREAK
		
		CASE SS_CLEANUP
			CPRINTLN(DEBUG_MISSION, "Cleanup WAIT_FOR_PASS")
			IF GET_GAME_TIMER() > iCallDelayTimer
				IF REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_HAO1, CT_FRIEND, BIT_FRANKLIN, CHAR_HAO, 1000, 5000, VID_BLANK, CID_DISPLAY_STREET_RACE_HELP)
					CREDIT_BANK_ACCOUNT(CHAR_FRANKLIN, BAAC_UNLOGGED_SMALL_ACTION, 200,TRUE,TRUE)//1412118
					Script_Passed()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC WAIT_FOR_FAIL()
	SWITCH missionSubState
		CASE SS_SETUP
			CPRINTLN(DEBUG_MISSION, "Setup WAIT_FOR_FAIL")
			
			CLEANUP_CHECKPOINTS()
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, HAO1_RACE_TIME)
			
			STRING sFailReason
			
			STOP_AUDIO_SCENE("STREET_RACE_DURING_RACE")
			
			IF DOES_ENTITY_EXIST(sRCLauncherDataLocal.pedID[HAO])
				IF IS_PED_INJURED(sRCLauncherDataLocal.pedID[HAO])
					MissionFailReason = MFR_KILLED_HAO
				ENDIF
			ENDIF

			// set the fail reason
			IF eRaceState = RACE_STATE_FAIL
				// Race specific fail reason
				SWITCH failReason
					CASE FAIL_RIVAL_ATTACKED
						sFailReason = "RACES_R_INJ"
					BREAK
					
					CASE FAIL_RIVAL_DEAD
						sFailReason = "RACES_R_DEAD"
					BREAK
					
					CASE FAIL_WRECKED
						sFailReason = "RACES_WRECK"
					BREAK
					
					CASE FAIL_ABANDONED_RACE
						sFailReason = "RACES_ABAND"
					BREAK
					
					CASE FAIL_MISSED_CHECKPOINT
						sFailReason = "RACES_MISS"
					BREAK
					
					CASE FAIL_ALERTED_COPS
						sFailReason = "RACES_COPS"
					BREAK
				ENDSWITCH
			ELSE
				SWITCH missionFailReason
					CASE MFR_NONE
						// no fail reason
					BREAK
					
					CASE MFR_KILLED_HAO
						sFailReason = "HAO1_FAIL1"
					BREAK
					
					CASE MFR_THREATENED_HAO
						sFailReason = "HAO1_FAIL2"
					BREAK
					
					CASE MFR_DIDNT_WIN
						sFailReason = "HAO1_FAIL3"
					BREAK
				ENDSWITCH
			ENDIF
			
			IF MissionFailReason = MFR_NONE AND failReason = FAIL_DEFAULT
				Random_Character_Failed()
			ELSE
				Random_Character_Failed_With_Reason(sFailReason)
			ENDIF
			
			missionSubState = SS_UPDATE
		BREAK
		
		CASE SS_UPDATE
			CPRINTLN(DEBUG_MISSION, "Cleanup WAIT_FOR_FAIL")
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT(g_structRCScriptArgs sRCLauncherDataIn)
	
	sRCLauncherDataLocal = sRCLauncherDataIn
	RC_TakeEntityOwnership(sRCLauncherDataLocal)
	
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINT_LAUNCHER_DEBUG("Force cleanup [TERMINATING]")
		Random_Character_Failed()
		Script_Cleanup()
	ENDIF
	
	RANDOM_CHAR_DISPLAY_MISSION_TITLE(RC_HAO_1)

	IF Is_Replay_In_Progress() // Set up the initial scene for replays
	    g_bSceneAutoTrigger = TRUE
		eInitialSceneStage = IS_REQUEST_SCENE
		WHILE NOT SetupScene_HAO_1(sRCLauncherDataLocal)
			WAIT(0)
		ENDWHILE
		RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sRCLauncherDataLocal, FALSE)
		g_bSceneAutoTrigger = FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
		sSkipMenu[0].sTxtLabel = "Restart"
		sSkipMenu[1].sTxtLabel = "Race"
	#ENDIF

	IF (g_savedGlobals.sRandomChars.g_bTriggeredHao1 = FALSE)
		g_savedGlobals.sRandomChars.g_bTriggeredHao1 = TRUE
	ENDIF

	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_SW")
		
		UPDATE_MISSION_NAME_DISPLAYING(sRCLauncherDataLocal.sIntroCutscene)
		
		WAIT(0)
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			SWITCH missionStage
				CASE MS_INIT
					INIT()
				BREAK
				
				CASE MS_PRE_INTRO
					PRE_INTRO()
				BREAK
				
				CASE MS_INTRO
					INTRO()
				BREAK
				
				CASE MS_GET_CAR
					GET_CAR()
				BREAK
				
				CASE MS_DRIVE_TO_RACE
					DRIVE_TO_RACE()
				BREAK
				
				/*CASE MS_RETURN_TO_CAR
					RETURN_TO_CAR()
				BREAK*/
				
				CASE MS_LOSE_COPS
					LOSE_COPS()
				BREAK
				
				CASE MS_LINEUP
					RACE_LINEUP()
				BREAK
				
				CASE MS_COUNTDOWN
					RACE_COUNTDOWN()
				BREAK
				
				CASE MS_RACE
					RACE_ACTIVE()
				BREAK
				
				CASE MS_PASS
					WAIT_FOR_PASS()
				BREAK
				
				CASE MS_FAIL
					WAIT_FOR_FAIL()
				BREAK
			ENDSWITCH
		ENDIF
		
		CheckHao()
		CheckRacers()
	
		// Check debug completion/failure
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF

	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
