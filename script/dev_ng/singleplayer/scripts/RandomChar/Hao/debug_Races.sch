#IF IS_DEBUG_BUILD

// Includes
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "shared_debug.sch"
USING "common_Races.sch"
USING "finance_control_public.sch"
USING "fmmc_cloud_loader.sch"

/// DEBUG WIDGET VARIABLES
WIDGET_GROUP_ID  widgetGroup
// Commented out for now this needs to be replaced with the new UGC system
//STRUCT_DATA_FILE cloudData
BOOL 			 bSetAtNight
BOOL 			 bGiveCash
BOOL 			 bGiveVeh
BOOL 			 bOutputData
GET_UGC_CONTENT_STRUCT ugcContentStruct

/// PURPOSE: Required to shift checkpoints from original cloud data
PROC ADJUST_CHECKPOINTS_FOR_RACE()
	
	INT i
	INT iReverse
	
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfCheckPoints)
		iReverse = (g_FMMC_STRUCT.iNumberOfCheckPoints - i)
		IF iReverse > 0
		AND iReverse < GET_FMMC_MAX_NUM_CHECKPOINTS()
			g_FMMC_STRUCT.sPlacedCheckpoint[iReverse].vCheckPoint = g_FMMC_STRUCT.sPlacedCheckpoint[iReverse -1].vCheckPoint
		ENDIF
	ENDFOR	
	
	IF g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_BOAT_P2P
	AND g_FMMC_STRUCT.iRaceType != FMMC_RACE_TYPE_AIR_P2P
		g_FMMC_STRUCT.vStartingGrid = g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint
		g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint = g_FMMC_STRUCT.sPlacedCheckpoint[(g_FMMC_STRUCT.iNumberOfCheckPoints)].vCheckPoint
	ELSE
		g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint = g_FMMC_STRUCT.vStartingGrid
	ENDIF
	g_FMMC_STRUCT.sPlacedCheckpoint[(g_FMMC_STRUCT.iNumberOfCheckPoints)].vCheckPoint = <<0.0, 0.0, 0.0>>
ENDPROC

/// PURPOSE: Setup Race Widget
PROC SETUP_RACE_WIDGET(RACE_DATA raceData)

	// Create widget
	IF raceData.eRaceType = RACETYPE_SEA
		widgetGroup = START_WIDGET_GROUP("Sea Races")
	ELSE
		widgetGroup = START_WIDGET_GROUP("Street Races")
	ENDIF
	
	// Track name
	SWITCH raceData.eRaceTrack

		// Street Races
		CASE STREET_RACE_01 ADD_WIDGET_STRING("Race Track: SOUTH LOS SANTOS") BREAK
		CASE STREET_RACE_02 ADD_WIDGET_STRING("Race Track: CITY CIRCUIT") 	  BREAK
		CASE STREET_RACE_04 ADD_WIDGET_STRING("Race Track: AIRPORT") 		  BREAK
		CASE STREET_RACE_05 ADD_WIDGET_STRING("Race Track: FREEWAY") 		  BREAK
		CASE STREET_RACE_06 ADD_WIDGET_STRING("Race Track: VESPUCCI CANALS")  BREAK

		// Sea Races
		CASE SEA_RACE_01 	ADD_WIDGET_STRING("Race Track: NORTH COAST") 	  BREAK
		CASE SEA_RACE_02	ADD_WIDGET_STRING("Race Track: SOUTH COAST") 	  BREAK
		CASE SEA_RACE_03 	ADD_WIDGET_STRING("Race Track: RATON CANYON") 	  BREAK
		CASE SEA_RACE_04 	ADD_WIDGET_STRING("Race Track: LOS SANTOS") 	  BREAK
	ENDSWITCH
	
	// Vehicle type
	SWITCH raceData.eRaceType
		CASE RACETYPE_BIKE	ADD_WIDGET_STRING("Race Type: BIKES") BREAK
		CASE RACETYPE_CAR	ADD_WIDGET_STRING("Race Type: CARS")  BREAK
		CASE RACETYPE_SEA	ADD_WIDGET_STRING("Race Type: SEA")   BREAK
	ENDSWITCH
		
	// Entry fee
	SWITCH raceData.iRaceFee
		CASE 1000	ADD_WIDGET_STRING("Entry Fee: $1000") BREAK
		CASE 5000	ADD_WIDGET_STRING("Entry Fee: $5000") BREAK
		DEFAULT 	ADD_WIDGET_STRING("Entry Fee: FREE")  BREAK
	ENDSWITCH

	ADD_WIDGET_BOOL("Give Entry Fee", bGiveCash)
	ADD_WIDGET_BOOL("Set At Night", bSetAtNight)
	ADD_WIDGET_BOOL("Warp Into Race Vehicle", bGiveVeh)
	ADD_WIDGET_BOOL("Output Race Data", bOutputData)
	STOP_WIDGET_GROUP()
ENDPROC


PROC OUTPUT_MP_GLOBALS_TO_TTY()	
	CPRINTLN(DEBUG_MISSION, "-----------------------------------------")
	CPRINTLN(DEBUG_MISSION, "Race Launcher: Outputting MP global data:")
	CPRINTLN(DEBUG_MISSION, "g_FMSP_Race_Data.bActive = ", g_FMSP_Race_Data.bActive)
	CPRINTLN(DEBUG_MISSION, "g_FMSP_Race_Data.sUserID = ", g_FMSP_Race_Data.sUserID)
	CPRINTLN(DEBUG_MISSION, "g_FMSP_Race_Data.sFilename = ", g_FMSP_Race_Data.sFilename)
	CPRINTLN(DEBUG_MISSION, "g_FMSP_Race_Data.vLocation = ", g_FMSP_Race_Data.vLocation)
	CPRINTLN(DEBUG_MISSION, "-----------------------------------------")
ENDPROC

PROC OUTPUT_RACE_DATA_TO_TTY()	
	
	INT i

	CPRINTLN(DEBUG_MISSION, "-------------------------------------------------------")
	CPRINTLN(DEBUG_MISSION, "--                     RACE DATA                     --")	
	CPRINTLN(DEBUG_MISSION, "-------------------------------------------------------")
	CPRINTLN(DEBUG_MISSION, "------------STARTING GRID-------------")
	CPRINTLN(DEBUG_MISSION, "trackData.iNumAIRacers = ", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1)
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		IF i < FMMC_MAX_NUM_RACERS
			CPRINTLN(DEBUG_MISSION, "trackData.vStartGrid[",i,"] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
			CPRINTLN(DEBUG_MISSION, "trackData.fStartGrid[",i,"] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead)
			WAIT(0)
		ENDIF
	ENDFOR

	CPRINTLN(DEBUG_MISSION, "-------------CHECKPOINTS--------------")
	CPRINTLN(DEBUG_MISSION, "trackData.iNumCheckpoints = ", g_FMMC_STRUCT.iNumberOfCheckPoints)
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfCheckPoints - 1
		IF i < GET_FMMC_MAX_NUM_CHECKPOINTS()
			CPRINTLN(DEBUG_MISSION, "trackData.vCheckPoint[",i,"] = ",g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint)
			WAIT(0)
		ENDIF
	ENDFOR	
	CPRINTLN(DEBUG_MISSION, "--------------------------------------")
ENDPROC

PROC OUTPUT_RACE_DATA_TO_DEBUG_FILE()	
	
	TEXT_LABEL_63 tTemp
	INT 		  i
	
	OPEN_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("------------STARTING GRID-------------")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("trackData.iNumAIRacers = ")
	SAVE_INT_TO_DEBUG_FILE(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1)
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		IF i < FMMC_MAX_NUM_RACERS
		
			// Start Grid Vector
			tTemp = "trackData.vStartGrid["
			tTemp += i
			tTemp += "] = "
			SAVE_STRING_TO_DEBUG_FILE(tTemp)
			SAVE_VECTOR_TO_DEBUG_FILE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Start Grid Heading
			tTemp = "trackData.fStartGrid["
			tTemp += i
			tTemp += "] = "
			SAVE_STRING_TO_DEBUG_FILE(tTemp)
			SAVE_FLOAT_TO_DEBUG_FILE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			WAIT(0)
		ENDIF
	ENDFOR
	
	SAVE_STRING_TO_DEBUG_FILE("-------------CHECKPOINTS--------------")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("trackData.iNumCheckpoints = ")
	SAVE_INT_TO_DEBUG_FILE(g_FMMC_STRUCT.iNumberOfCheckPoints)
	SAVE_NEWLINE_TO_DEBUG_FILE()

	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfCheckPoints - 1
		IF i < GET_FMMC_MAX_NUM_CHECKPOINTS()
			
			// Checkpoint
			tTemp = "trackData.vCheckPoint["
			tTemp += i
			tTemp += "] = "
			SAVE_STRING_TO_DEBUG_FILE(tTemp)
			SAVE_VECTOR_TO_DEBUG_FILE(g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			WAIT(0)
		ENDIF
	ENDFOR
	
	CLOSE_DEBUG_FILE()
ENDPROC

/// PURPOSE: Check for user input
PROC MAINTAIN_RACE_WIDGET(RACE_DATA raceData)
	
	// Force time of day to night
	IF bSetAtNight
		SET_CLOCK_TIME(0,0,0)
		bSetAtNight = FALSE
	ENDIF
	
	// Give entry fee for this race
	IF bGiveCash
		CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, raceData.iRaceFee)
		bGiveCash = FALSE
	ENDIF
	
	// Give suitable race vehicle
	IF bGiveVeh
		
		// Request either car or bike model
		MODEL_NAMES vehModel
		SWITCH raceData.eRaceType
			CASE RACETYPE_BIKE	vehModel = BATI  BREAK
			CASE RACETYPE_CAR	vehModel = BUFFALO  	 BREAK
			CASE RACETYPE_SEA	vehModel = SEASHARK  BREAK	
		ENDSWITCH
	
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		
			// Take out of existing vehicle
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				VEHICLE_INDEX oldCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

				IF DOES_ENTITY_EXIST(oldCar)
					IF NOT IS_ENTITY_A_MISSION_ENTITY(oldCar)
						SET_ENTITY_AS_MISSION_ENTITY(oldCar)
					ENDIF
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
					DELETE_VEHICLE(oldCar)
				ENDIF
			ENDIF
			
			// Request new car
			REQUEST_MODEL(vehModel)
			WHILE NOT HAS_MODEL_LOADED(vehModel)
				WAIT(0)
			ENDWHILE
			
			// Place player into new vehicle
			VEHICLE_INDEX newCar = CREATE_VEHICLE(vehModel, GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()))
			IF DOES_ENTITY_EXIST(newCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), newCar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(newCar)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
		ENDIF
		
		bGiveVeh = FALSE
	ENDIF
	
	// Output race information to debug channels
	IF bOutputData
		IF IS_PLAYER_ONLINE()
			IF NOT IS_PC_VERSION()
				// Commented out for now this needs to be replaced with the new UGC system
				/*WHILE NOT LOAD_DATA_OLD(cloudData, PLAYER_ID(), raceData.iSaveSlot, GET_CLOUD_ID())
					CPRINTLN(DEBUG_MISSION, "Race Launcher: Getting cloud data...")
					WAIT(0)
				ENDWHILE*/
				
				WHILE NOT GET_UGC_BY_CONTENT_ID(ugcContentStruct, GET_RACE_FILENAME(raceData.eRaceTrack))
					CPRINTLN(DEBUG_MISSION, "Race Launcher: Getting cloud data...")
					WAIT(0)
				ENDWHILE
				
				// Adjust for P2P races
				IF raceData.bP2P
					CPRINTLN(DEBUG_MISSION, "Race Launcher: Adjusting checkpoints for race...")
					ADJUST_CHECKPOINTS_FOR_RACE()
				ENDIF
				
				// Output race data
				OUTPUT_RACE_DATA_TO_TTY()
				OUTPUT_RACE_DATA_TO_DEBUG_FILE()
			ELSE
				CPRINTLN(DEBUG_MISSION, "Race Launcher: Unable to obtain data for PC version.")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "Race Launcher: Unable to obtain data when offline.")
		ENDIF
		
		bOutputData = FALSE
	ENDIF
ENDPROC

/// PURPOSE: Destroy widget
PROC CLEANUP_RACE_WIDGET()
	IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
		DELETE_WIDGET_GROUP(widgetGroup)
	ENDIF
ENDPROC

#ENDIF
