// Includes
USING "common_Races.sch"

// Functions
PROC INIT_TRACK_DATA(RACE_DATA raceData, TRACK_DATA &trackData)

	SWITCH raceData.eRaceTrack
	
		// South Los Santos
		CASE STREET_RACE_01
		
			// Laps
			trackData.iNumLaps = 2
			
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 30
			trackData.drivingSpeedMedium = 38
			trackData.drivingSpeedFast = 46
			
			// the spawn location for the vehicle the player gets if they win the race
			// Commenting out for now in case this is needed again
			/*trackData.vRewardVehicleSpawnPoint = <<-64.79, -1804.30, 26.92>>
			trackData.fRewardVehicleHeading = 229.98*/

			// Racers
			trackData.iNumAIRacers  = 5
			trackData.vStartGrid[0] = <<-151.94, -1566.87, 34.48>>//<<-67.8471, -1790.9946, 26.8489>>
			trackData.fStartGrid[0] = 139.80//-128.0000
			trackData.vStartGrid[1] = <<-155.98, -1563.47, 34.60>>//<<-71.0244, -1795.0614, 26.9190>>
			trackData.fStartGrid[1] = 139.80//-128.0000
			trackData.vStartGrid[2] = <<-146.40, -1560.51, 34.28>>//<<-75.7883, -1784.7903, 27.2680>>
			trackData.fStartGrid[2] = 139.80//-128.0000
			trackData.vStartGrid[3] = <<-150.53, -1556.87, 34.40>>//<<-78.9656, -1788.8571, 27.3164>>
			trackData.fStartGrid[3] = 139.80//-128.0000
			trackData.vStartGrid[4] = <<-141.15, -1553.81, 33.98>>//<<-83.7295, -1778.5861, 27.6870>>
			trackData.fStartGrid[4] = 139.80//-128.0000
			trackData.vStartGrid[5] = <<-144.97, -1550.67, 34.07>>//<<-86.9068, -1782.6528, 27.6880>>
			trackData.fStartGrid[5] = 139.80//-128.0000
			/*trackData.vStartGrid[6] = <<-91.6707, -1772.3817, 28.1402>>
			trackData.fStartGrid[6] = -128.0000
			trackData.vStartGrid[7] = <<-94.8480, -1776.4485, 28.1230>>
			trackData.fStartGrid[7] = -128.0000*/

			// Checkpoints
			trackData.iNumCheckpoints = 18
			trackData.vCheckPoint[0] = <<-177.4019, -1594.6443, 33.4994>>
			trackData.vCheckPoint[1] = <<-181.9884, -1688.1002, 32.2323>>
			trackData.vCheckPoint[2] = <<-68.2538, -1793.9515, 26.8799>>
			trackData.vCheckPoint[3] = <<56.0836, -1894.4215, 20.5566>>
			trackData.vCheckPoint[4] = <<102.9353, -1868.2292, 23.1889>>
			trackData.vCheckPoint[5] = <<236.268280,-1924.711548,23.296431>>//<<207.8150, -1909.6934, 22.7343>>
			trackData.vCheckPoint[6] = <<287.2185, -1884.0795, 25.9619>>
			trackData.vCheckPoint[7] = <<397.2169, -1948.5200, 23.3175>>
			trackData.vCheckPoint[8] = <<552.7326, -1879.7982, 24.4769>>
			trackData.vCheckPoint[9] = <<599.2975, -1699.4381, 21.9581>>
			trackData.vCheckPoint[10] = <<603.7488, -1595.4388, 25.5479>>
			trackData.vCheckPoint[11] = <<514.6337, -1443.9539, 28.3433>>
			trackData.vCheckPoint[12] = <<471.434174,-1433.924683,28.342113>>//<<450.1671, -1429.5634, 28.3572>>
			trackData.vCheckPoint[13] = <<293.2, -1521.2, 28.3415>>
			trackData.vCheckPoint[14] = <<182.5014, -1585.2716, 28.3099>>
			trackData.vCheckPoint[15] = <<16.9636, -1465.6825, 29.5632>>
			trackData.vCheckPoint[16] = <<-73.9895, -1476.3289, 31.1385>>
			trackData.vCheckPoint[17] = <<-155.847610,-1566.979980,34.000111>>
		BREAK
	
		// City Circuit
		CASE STREET_RACE_02
		
			// Laps
			trackData.iNumLaps = 1
			
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 30
			trackData.drivingSpeedMedium = 38
			trackData.drivingSpeedFast = 46
			
			// the spawn location for the vehicle the player gets if they win the race
			// Commenting out for now in case this is needed again
			/*trackData.vRewardVehicleSpawnPoint = <<-522.79, 264.52, 82.47>>
			trackData.fRewardVehicleHeading = 84.59*/
			
			// Racers
			trackData.iNumAIRacers = 5
			trackData.vStartGrid[0] = <<368.64, 309.52, 103.16>>
			trackData.fStartGrid[0] = 74.34
			trackData.vStartGrid[1] = <<370.33, 314.66, 103.07>>
			trackData.fStartGrid[1] = 74.34
			trackData.vStartGrid[2] = <<376.31, 307.58, 102.90>>
			trackData.fStartGrid[2] = 74.34
			trackData.vStartGrid[3] = <<377.50, 313.00, 103.04>>
			trackData.fStartGrid[3] = 74.34
			trackData.vStartGrid[4] = <<383.49, 305.81, 102.86>>
			trackData.fStartGrid[4] = 74.34
			trackData.vStartGrid[5] = <<385.23, 311.22, 102.77>>
			trackData.fStartGrid[5] = 74.34
			/*trackData.vStartGrid[6] = <<-492.88, 254.87, 82.49>>
			trackData.fStartGrid[6] = 74.34
			trackData.vStartGrid[7] = <<-492.31, 260.00, 82.40>>
			trackData.fStartGrid[7] = 74.34*/
			
			// Checkpoints
			trackData.iNumCheckpoints = 60
			trackData.vCheckPoint[0] = <<176.5163, 368.2185, 107.7932>>
			trackData.vCheckPoint[1] = <<74.6353, 326.5884, 111.1838>>
			trackData.vCheckPoint[2] = <<40.982517,278.166382,108.628006>>//<<36.6091, 280.9706, 108.5532>>
			trackData.vCheckPoint[3] = <<-6.5086, 269.1777, 107.8002>>
			trackData.vCheckPoint[4] = <<-157.2158, 260.5422, 92.9641>>
			trackData.vCheckPoint[5] = <<-306.6567, 264.1079, 86.8654>>
			trackData.vCheckPoint[6] = <<-510.8915, 258.9640, 82.0697>>
			trackData.vCheckPoint[7] = <<-677.1512, 285.4961, 81.0311>>
			trackData.vCheckPoint[8] = <<-829.2744, 291.7629, 85.4059>>
			trackData.vCheckPoint[9] = <<-854.597778,467.941254,86.396263>>//<<-854.1977, 465.2954, 86.3498>>
			trackData.vCheckPoint[10] = <<-877.540222,535.083862,90.457161>>
			trackData.vCheckPoint[11] = <<-1006.2338, 596.4271, 101.8474>>
			trackData.vCheckPoint[12] = <<-1211.739502,545.427917,96.671173>>//<<-1159.7075, 553.4713, 99.9593>>
			trackData.vCheckPoint[13] = <<-1247.7463, 481.3893, 92.6580>>
			trackData.vCheckPoint[14] = <<-1209.5111, 468.7086, 88.8673>>
			trackData.vCheckPoint[15] = <<-1096.5419, 456.5062, 76.7926>>
			trackData.vCheckPoint[16] = <<-1077.0458, 385.2891, 68.0071>>
			trackData.vCheckPoint[17] = <<-1081.6644, 292.3875, 63.0137>>
			trackData.vCheckPoint[18] = <<-1128.9888, 267.3287, 64.9992>>
			trackData.vCheckPoint[19] = <<-1274.6458, 228.7755, 59.8090>>
			trackData.vCheckPoint[20] = <<-1408.2948, 193.1421, 57.5883>>
			trackData.vCheckPoint[21] = <<-1429.7986, -18.8745, 51.4889>>
			trackData.vCheckPoint[22] = <<-1466.3798, -91.9526, 49.9463>>
			trackData.vCheckPoint[23] = <<-1582.2281, -187.6987, 54.6562>>
			trackData.vCheckPoint[24] = <<-1633.1866, -302.6338, 50.4393>>
			trackData.vCheckPoint[25] = <<-1755.9268, -441.1943, 41.2927>>
			trackData.vCheckPoint[26] = <<-1675.6215, -568.3997, 33.2031>>
			trackData.vCheckPoint[27] = <<-1556.6298, -662.1553, 27.9556>>
			trackData.vCheckPoint[28] = <<-1441.3038, -761.7834, 22.5293>>
			trackData.vCheckPoint[29] = <<-1327.7562, -861.1824, 15.5558>>
			trackData.vCheckPoint[30] = <<-1254.9, -1062.5, 7.4798>>
			trackData.vCheckPoint[31] = <<-1306.2, -1091.1, 6.0>>
			trackData.vCheckPoint[32] = <<-1266.1, -1265.2, 3.0>>
			trackData.vCheckPoint[33] = <<-1206.7, -1251.5, 6.0>>
			trackData.vCheckPoint[34] = <<-1149.226685,-1305.180054,4.164155>>//<<-1166.615723,-1295.253418,4.194942>>//<<-1065.0, -1290.6, 4.8732>>
			trackData.vCheckPoint[35] = <<-933.4516, -1221.1724, 4.1712>>
			trackData.vCheckPoint[36] = <<-804.6487, -1141.6809, 8.1794>>
			trackData.vCheckPoint[37] = <<-653.9498, -1049.0057, 16.1054>>
			trackData.vCheckPoint[38] = <<-635.1398, -891.9207, 23.9038>>
			trackData.vCheckPoint[39] = <<-633.6005, -739.1533, 26.3700>>
			trackData.vCheckPoint[40] = <<-627.4288, -582.9346, 33.6256>>
			trackData.vCheckPoint[41] = <<-625.0278, -397.2912, 33.7955>>
			trackData.vCheckPoint[42] = <<-586.7, -378.4, 33.9005>>
			trackData.vCheckPoint[43] = <<-437.4909, -388.7413, 32.1535>>
			trackData.vCheckPoint[44] = <<-231.9090, -410.7465, 29.6085>>
			trackData.vCheckPoint[45] = <<-147.8854, -373.2199, 32.7875>>
			trackData.vCheckPoint[46] = <<-115.0438, -285.8302, 40.5101>>
			trackData.vCheckPoint[47] = <<-51.4044, -257.3596, 44.7924>>
			trackData.vCheckPoint[48] = <<85.9933, -309.2740, 45.4642>>
			trackData.vCheckPoint[49] = <<225.0007, -356.0629, 43.2898>>
			trackData.vCheckPoint[50] = <<370.0895, -402.5069, 44.9239>>
			trackData.vCheckPoint[51] = <<481.0543, -305.9488, 45.6763>>
			trackData.vCheckPoint[52] = <<527.0013, -220.0025, 49.7887>>
			trackData.vCheckPoint[53] = <<546.504150,-101.520279,64.260414>>//<<514.2216, -142.4008, 57.3956>>
			trackData.vCheckPoint[54] = <<636.0579, -57.7903, 75.5052>>
			trackData.vCheckPoint[55] = <<703.7552, 18.0953, 83.1893>>
			trackData.vCheckPoint[56] = <<778.6013, 128.2524, 78.3677>>
			trackData.vCheckPoint[57] = <<751.6663, 181.9566, 81.9156>>
			trackData.vCheckPoint[58] = <<609.8065, 232.9736, 101.0741>>
			trackData.vCheckPoint[59] = <<369.29, 312.41, 103.24>>
		BREAK

		// Airport
		CASE STREET_RACE_04
		
			// Laps
			trackData.iNumLaps = 2
			
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 34
			trackData.drivingSpeedMedium = 40
			trackData.drivingSpeedFast = 46
			
			// the spawn location for the vehicle the player gets if they win the race
			// Commenting out for now in case this is needed again
			/*trackData.vRewardVehicleSpawnPoint = <<-1000.62, -2448.44, 19.62>>
			trackData.fRewardVehicleHeading = 153.76*/
			
			// Racers
			trackData.iNumAIRacers  = 5
			trackData.vStartGrid[0] = <<-816.46, -2547.30, 13.50>>//<<-987.8090, -2440.3245, 19.1756>>
			trackData.fStartGrid[0] = 341.07//-208.0000
			trackData.vStartGrid[1] = <<-812.31, -2549.09, 13.33>>//<<-992.3657, -2437.9016, 19.1459>>
			trackData.fStartGrid[1] = 341.07//-208.0000
			trackData.vStartGrid[2] = <<-818.74, -2553.95, 13.38>>//<<-983.0779, -2431.4265, 19.1820>>
			trackData.fStartGrid[2] = 341.07//-208.0000
			trackData.vStartGrid[3] = <<-815.07, -2555.46, 13.41>>//<<-987.6346, -2429.0037, 19.1280>>
			trackData.fStartGrid[3] = 341.07//-208.0000
			trackData.vStartGrid[4] = <<-822.05, -2559.68, 13.49>>
			trackData.fStartGrid[4] = 341.07//-208.0000
			trackData.vStartGrid[5] = <<-817.13, -2562.07, 13.34>>
			trackData.fStartGrid[5] = 341.07//-208.0000
			/*trackData.vStartGrid[6] = <<-973.6157, -2413.6309, 19.1948>>
			trackData.fStartGrid[6] = -208.0000
			trackData.vStartGrid[7] = <<-978.1724, -2411.2080, 19.0923>>
			trackData.fStartGrid[7] = -208.0000*/
			
			// Checkpoints
			trackData.iNumCheckpoints = 30
			trackData.vCheckPoint[0] = <<-807.6469, -2469.8474, 12.7351>>
			trackData.vCheckPoint[1] = <<-846.8392, -2324.6980, 16.9914>>
			trackData.vCheckPoint[2] = <<-762.6910, -2198.8743, 14.9078>>
			trackData.vCheckPoint[3] = <<-656.6849, -2105.3574, 14.5170>>
			trackData.vCheckPoint[4] = <<-562.5864, -2083.1013, 26.3678>>
			trackData.vCheckPoint[5] = <<-336.5846, -2111.5476, 22.7184>>
			trackData.vCheckPoint[6] = <<-238.7528, -2134.7715, 21.7367>>
			trackData.vCheckPoint[7] = <<-18.5043, -2050.7546, 18.0635>>
			trackData.vCheckPoint[8] = <<109.6071, -2047.1046, 17.3701>>
			trackData.vCheckPoint[9] = <<162.2125, -2017.6981, 17.2636>>
			trackData.vCheckPoint[10] = <<215.2578, -1946.2358, 20.9418>>
			trackData.vCheckPoint[11] = <<200.6974, -1907.0240, 22.7627>>
			trackData.vCheckPoint[12] = <<113.3993, -1865.5662, 23.4617>>
			trackData.vCheckPoint[13] = <<68.2166, -1891.0560, 20.7378>>
			trackData.vCheckPoint[14] = <<-111.4028, -1759.6040, 28.8334>>
			trackData.vCheckPoint[15] = <<-242.8844, -1813.0552, 28.7495>>
			trackData.vCheckPoint[16] = <<-364.1700, -1821.9215, 21.5683>>
			trackData.vCheckPoint[17] = <<-478.9611, -1885.9553, 16.6741>>
			trackData.vCheckPoint[18] = <<-598.0135, -1999.6506, 16.4316>>
			trackData.vCheckPoint[19] = <<-720.7257, -2133.1033, 12.3082>>
			trackData.vCheckPoint[20] = <<-777.2783, -2181.1165, 14.8988>>
			trackData.vCheckPoint[21] = <<-863.5436, -2251.4424, 17.3272>>
			trackData.vCheckPoint[22] = <<-948.9650, -2368.0957, 19.2050>>
			trackData.vCheckPoint[23] = <<-990.7916, -2440.4375, 19.2041>>
			trackData.vCheckPoint[24] = <<-1089.3838, -2613.6155, 19.2104>>
			trackData.vCheckPoint[25] = <<-1084.4546, -2680.6423, 19.2111>>
			trackData.vCheckPoint[26] = <<-996.7110, -2742.9341, 19.2060>>
			trackData.vCheckPoint[27] = <<-926.4546, -2730.2039, 19.2052>>
			trackData.vCheckPoint[28] = <<-874.8831, -2665.6255, 18.6777>>
			trackData.vCheckPoint[29] = <<-813.0546, -2546.7380, 12.7888>>
		BREAK

		// Freeway
		CASE STREET_RACE_05
			
			// Laps
			trackData.iNumLaps = 2
			
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 34
			trackData.drivingSpeedMedium = 40
			trackData.drivingSpeedFast = 46
			
			// the spawn location for the vehicle the player gets if they win the race
			// Commenting out for now in case this is needed again
			/*trackData.vRewardVehicleSpawnPoint = <<398.58, -1171.30, 39.53>>
			trackData.fRewardVehicleHeading = 92.05*/

			// Racers
			trackData.iNumAIRacers  = 5
			//trackData.vStartGrid[0] = <<423.2805, -1191.8965, 39.7758>>
			//trackData.fStartGrid[0] = 92.0000
			trackData.vStartGrid[0] = <<780.54, -1150.96, 28.65>>//<<423.1004, -1186.7389, 39.7662>>
			trackData.fStartGrid[0] = 93.71//92.0000
			trackData.vStartGrid[1] = <<780.24, -1145.74, 28.56>>//<<422.9203, -1181.5812, 39.7249>>
			trackData.fStartGrid[1] = 93.71//92.0000
			trackData.vStartGrid[2] = <<787.47, -1150.53, 28.50>>//<<422.7402, -1176.4236, 39.6901>>
			trackData.fStartGrid[2] = 93.71//92.0000
			//trackData.vStartGrid[3] = <<433.3519, -1191.5448, 39.9888>>
			//trackData.fStartGrid[3] = 92.0000
			trackData.vStartGrid[3] = <<787.19, -1145.74, 28.66>>//<<433.1718, -1186.3872, 39.9774>>
			trackData.fStartGrid[3] = 93.71//92.0000
			trackData.vStartGrid[4] = <<794.10, -1149.95, 28.66>>//<<432.9917, -1181.2295, 39.9366>>
			trackData.fStartGrid[4] = 93.71//92.0000
			trackData.vStartGrid[5] = <<793.58, -1144.73, 28.59>>//<<432.8116, -1176.0719, 39.9027>>
			trackData.fStartGrid[5] = 93.71//92.0000
			
			// Checkpoints
			trackData.iNumCheckpoints = 24
			trackData.vCheckPoint[0] = <<660.2601, -1154.4073, 40.6410>>
			trackData.vCheckPoint[1] = <<571.5685, -1178.6960, 41.2734>>
			trackData.vCheckPoint[2] = <<421.5113, -1184.2124, 39.7127>>
			trackData.vCheckPoint[3] = <<268.5206, -1176.5718, 37.1933>>
			trackData.vCheckPoint[4] = <<186.8177, -1162.0254, 37.1079>>
			trackData.vCheckPoint[5] = <<71.0,-1163.3,28.7>>
			trackData.vCheckPoint[6] = <<74.952896,-1267.338257,28.193726>>
			trackData.vCheckPoint[7] = <<244.1904, -1226.6989, 37.3049>>
			trackData.vCheckPoint[8] = <<394.5264, -1224.6189, 39.1224>>
			trackData.vCheckPoint[9] = <<519.6835, -1228.2539, 40.9569>>
			trackData.vCheckPoint[10] = <<643.6398, -1244.2620, 40.9174>>
			trackData.vCheckPoint[11] = <<765.5765, -1242.7665, 25.9457>>
			trackData.vCheckPoint[12] = <<851.6, -1254.2, 26.9066>>
			trackData.vCheckPoint[13] = <<859.1, -1326.3, 37.0286>>
			trackData.vCheckPoint[14] = <<739.5747, -1347.0132, 38.9030>>
			trackData.vCheckPoint[15] = <<695.9282, -1292.1365, 41.1269>>
			trackData.vCheckPoint[16] = <<729.7197, -1235.6235, 43.9479>>
			trackData.vCheckPoint[17] = <<755.8483, -1195.9965, 44.0202>>
			trackData.vCheckPoint[18] = <<724.6743, -1156.9841, 43.7047>>
			trackData.vCheckPoint[19] = <<688.6, -1101.7, 40.8716>>
			trackData.vCheckPoint[20] = <<732.4061, -1045.3109, 39.4973>>
			trackData.vCheckPoint[21] = <<815.3089, -1042.0963, 41.2541>>
			trackData.vCheckPoint[22] = <<865.2093, -1093.3431, 35.9246>>
			trackData.vCheckPoint[23] = <<777.423828,-1148.405273,28.078772>>//<<807.3, -1147.2, 27.8609>>
		BREAK
		
		// Vespucci Canals
		CASE STREET_RACE_06
		
			// Laps
			trackData.iNumLaps = 2
		
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 34
			trackData.drivingSpeedMedium = 40
			trackData.drivingSpeedFast = 46
			
			// the spawn location for the vehicle the player gets if they win the race
			// Commenting out for now in case this is needed again
			/*trackData.vRewardVehicleSpawnPoint = <<-1244.58, -1093.35, 7.45>>
			trackData.fRewardVehicleHeading = 201.95*/
		
			// Racers
			trackData.iNumAIRacers  = 5
			trackData.vStartGrid[0] = <<-1063.95, -1153.21, 1.52>>//<<-1065.318848,-1152.727539,1.691581>>//<<-1019.96, -1126.44, 1.65>>//<<-1255.1002, -1045.7418, 7.5358>>
			trackData.fStartGrid[0] = 299.67//303.47//-160.0000
			trackData.vStartGrid[1] = <<-1062.53, -1155.37, 1.46>>//<<-1063.993164,-1154.779541,1.620106>>//<<-1018.63, -1128.54, 1.50>>//<<-1259.9498, -1047.5070, 7.4633>>
			trackData.fStartGrid[1] = 299.67//303.47//-160.0000
			trackData.vStartGrid[2] = <<-1068.60, -1155.54, 1.61>>//<<-1069.609863,-1155.317627,1.658711>>//<<-1023.67, -1128.56, 1.53>>//<<-1258.5469, -1036.2721, 7.7785>>
			trackData.fStartGrid[2] = 299.67//303.47//-160.0000
			trackData.vStartGrid[3] = <<-1066.89, -1157.64, 1.56>>//<<-1068.627808,-1156.919556,1.575979>>//<<-1022.48, -1130.68, 1.70>>//<<-1263.3965, -1038.0372, 7.7087>>
			trackData.fStartGrid[3] = 299.67//303.47//-160.0000
			trackData.vStartGrid[4] = <<-1072.76, -1158.02, 1.53>>//<<-1073.714722,-1157.970459,1.717325>>//<<-1028.31, -1131.11, 1.71>>//<<-1261.9937, -1026.8024, 8.0213>>
			trackData.fStartGrid[4] = 299.67//303.47//-160.0000
			trackData.vStartGrid[5] = <<-1072.01, -1160.27, 1.48>>//<<-1072.504517,-1159.566650,1.652843>>//<<-1026.47, -1133.86, 1.59>>//<<-1266.8431, -1028.5675, 7.9143>>
			trackData.fStartGrid[5] = 299.67//303.47//-160.0000
			/*trackData.vStartGrid[6] = <<-1265.4403, -1017.3326, 8.2815>>
			trackData.fStartGrid[6] = -160.0000
			trackData.vStartGrid[7] = <<-1270.2899, -1019.0977, 8.1237>>
			trackData.fStartGrid[7] = -160.0000*/
			
			// Checkpoints
			trackData.iNumCheckpoints = 25
			trackData.vCheckPoint[0] = <<-925.4882, -1072.8036, 1.1502>>
			trackData.vCheckPoint[1] = <<-838.9020, -1020.6968, 12.2795>>
			trackData.vCheckPoint[2] = <<-752.4019, -968.1661, 15.5933>>
			trackData.vCheckPoint[3] = <<-660.0514, -959.4323, 20.3359>>
			trackData.vCheckPoint[4] = <<-643.1226, -996.0269, 19.6655>>
			trackData.vCheckPoint[5] = <<-755.0204, -1100.5715, 9.7347>>
			trackData.vCheckPoint[6] = <<-844.4481, -1152.1702, 5.5481>>
			trackData.vCheckPoint[7] = <<-934.6541, -1204.1019, 4.1492>>
			trackData.vCheckPoint[8] = <<-968.2922, -1181.9056, 2.9090>>
			trackData.vCheckPoint[9] = <<-1023.3292, -1087.9523, 1.0397>>
			trackData.vCheckPoint[10] = <<-1078.6326, -994.3664, 1.2191>>
			trackData.vCheckPoint[11] = <<-1170.7820, -837.8701, 13.2119>>
			trackData.vCheckPoint[12] = <<-1312.0331, -658.3402, 25.5365>>
			trackData.vCheckPoint[13] = <<-1375.9105, -560.5161, 29.2340>>
			trackData.vCheckPoint[14] = <<-1446.2238, -460.4871, 34.1264>>
			trackData.vCheckPoint[15] = <<-1487.2517, -447.6395, 34.5940>>
			trackData.vCheckPoint[16] = <<-1638.2157, -562.0701, 32.4537>>
			trackData.vCheckPoint[17] = <<-1625.8223, -610.9568, 31.6803>>
			trackData.vCheckPoint[18] = <<-1531.3903, -684.8500, 27.8725>>
			trackData.vCheckPoint[19] = <<-1424.3597, -771.3326, 21.8328>>
			trackData.vCheckPoint[20] = <<-1300.4484, -901.4435, 10.3951>>
			trackData.vCheckPoint[21] = <<-1257.0120, -1048.0339, 7.5077>>
			trackData.vCheckPoint[22] = <<-1211.111938,-1198.487183,6.755771>>//<<-1214.0095, -1181.1825, 6.7738>>
			//trackData.vCheckPoint[23] = <<-1181.9849, -1205.2546, 5.5401>>
			trackData.vCheckPoint[23] = <<-1104.3783, -1177.5079, 1.2615>>
			trackData.vCheckPoint[24] = <<-1062.985596,-1153.364502,1.129525>>//<<-1014.8540, -1126.1027, 1.0671>>
		BREAK

		// East Coast
		CASE SEA_RACE_01
		
			// Laps
			trackData.iNumLaps = 1

			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 52
			trackData.drivingSpeedMedium = 56
			trackData.drivingSpeedFast = 60

			// Racers
			trackData.iNumAIRacers = 7
			trackData.vStartGrid[0] = <<3058.7708, 637.4882, 0.0000>>
			trackData.fStartGrid[0] = 24.0000
			trackData.vStartGrid[1] = <<3061.6038, 638.7496, 0.0000>>
			trackData.fStartGrid[1] = 24.0000
			trackData.vStartGrid[2] = <<3064.4368, 640.0109, 0.0000>>
			trackData.fStartGrid[2] = 24.0000
			trackData.vStartGrid[3] = <<3067.2698, 641.2722, 0.0000>>
			trackData.fStartGrid[3] = 24.0000
			trackData.vStartGrid[4] = <<3061.6296, 631.0670, 0.0000>>
			trackData.fStartGrid[4] = 24.0000
			trackData.vStartGrid[5] = <<3064.4626, 632.3283, 0.0000>>
			trackData.fStartGrid[5] = 24.0000
			trackData.vStartGrid[6] = <<3067.2957, 633.5896, 0.0000>>
			trackData.fStartGrid[6] = 24.0000
			trackData.vStartGrid[7] = <<3070.1287, 634.8510, 0.0000>>
			trackData.fStartGrid[7] = 24.0000
			/*trackData.vStartGrid[8] = <<3064.4885, 624.6456, 0.0000>>
			trackData.fStartGrid[8] = 24.0000
			trackData.vStartGrid[9] = <<3067.3215, 625.9070, 0.0000>>
			trackData.fStartGrid[9] = 24.0000
			trackData.vStartGrid[10] = <<3070.1545, 627.1683, 0.0000>>
			trackData.fStartGrid[10] = 24.0000
			trackData.vStartGrid[11] = <<3072.9878, 628.4297, 0.0000>>
			trackData.fStartGrid[11] = 24.0000
			trackData.vStartGrid[12] = <<3067.3477, 618.2244, 0.0000>>
			trackData.fStartGrid[12] = 24.0000
			trackData.vStartGrid[13] = <<3070.1807, 619.4857, 0.0000>>
			trackData.fStartGrid[13] = 24.0000
			trackData.vStartGrid[14] = <<3073.0137, 620.7471, 0.0000>>
			trackData.fStartGrid[14] = 24.0000
			trackData.vStartGrid[15] = <<3075.8467, 622.0084, 0.0000>>
			trackData.fStartGrid[15] = 24.0000*/
			
			// Checkpoints
			trackData.iNumCheckpoints = 19
			trackData.vCheckPoint[0] = <<2978.9292, 778.0858, 0.0000>>
			trackData.vCheckPoint[1] = <<2925.5859, 840.1877, 0.0000>>
			trackData.vCheckPoint[2] = <<2913.8955, 1011.4951, 0.0000>>
			trackData.vCheckPoint[3] = <<2903.3430, 1120.0763, 0.0000>>
			trackData.vCheckPoint[4] = <<2944.4775, 1197.3416, 0.0000>>
			trackData.vCheckPoint[5] = <<3020.1887, 1217.4270, 0.0000>>
			trackData.vCheckPoint[6] = <<3107.7939, 1212.5355, 0.0000>>
			trackData.vCheckPoint[7] = <<3164.7542, 1246.8118, 0.0000>>
			trackData.vCheckPoint[8] = <<3185.5317, 1293.3823, 0.0000>>
			trackData.vCheckPoint[9] = <<3122.8853, 1432.1433, 0.0000>>
			trackData.vCheckPoint[10] = <<3021.7000, 1553.2744, 0.0000>>
			trackData.vCheckPoint[11] = <<3014.5593, 1634.4451, 0.0000>>
			trackData.vCheckPoint[12] = <<3045.4812, 1681.1735, 0.0000>>
			trackData.vCheckPoint[13] = <<3104.4434, 1687.2107, 0.0000>>
			trackData.vCheckPoint[14] = <<3147.7139, 1629.8402, 0.0000>>
			trackData.vCheckPoint[15] = <<3119.6807, 1571.5549, 0.0000>>
			trackData.vCheckPoint[16] = <<3005.0056, 1482.6628, 0.0000>>
			trackData.vCheckPoint[17] = <<2973.8721, 1355.7660, 0.0000>>
			trackData.vCheckPoint[18] = <<3014.1487, 1187.6003, 0.0000>>
			/*trackData.vCheckPoint[19] = <<3039.4414, 1039.0543, 0.0000>>
			trackData.vCheckPoint[20] = <<3060.6997, 901.0823, 0.0000>>
			trackData.vCheckPoint[21] = <<3102.7781, 755.8021, 0.0000>>
			trackData.vCheckPoint[22] = <<3154.9485, 650.1642, 0.0000>>
			trackData.vCheckPoint[23] = <<3170.7732, 591.6734, 0.0000>>
			trackData.vCheckPoint[24] = <<3138.2229, 538.3148, 0.0000>>
			trackData.vCheckPoint[25] = <<3092.9917, 534.7061, 0.0000>>
			trackData.vCheckPoint[26] = <<3067.7280, 566.4412, 0.0000>>
			trackData.vCheckPoint[27] = <<3062.4102, 640.7505, 0.0000>>*/
		BREAK
		
		// North Coast
		CASE SEA_RACE_02
			
			// Laps
			trackData.iNumLaps = 1
	
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 62
			trackData.drivingSpeedMedium = 66
			trackData.drivingSpeedFast = 70
	
			// Racers
			trackData.iNumAIRacers = 7
			trackData.vStartGrid[0] = <<3477.9264, 5203.8653, 0.0000>>
			trackData.fStartGrid[0] = 227.2083
			trackData.vStartGrid[1] = <<3473.7131, 5199.3135, 0.0000>>
			trackData.fStartGrid[1] = 227.2083
			/*trackData.vStartGrid[1] = <<3471.6064, 5197.0376, 0.0000>>
			trackData.fStartGrid[1] = 227.2083*/
			trackData.vStartGrid[2] = <<3469.4998, 5194.7617, 0.0000>>
			trackData.fStartGrid[2] = 227.2083
			/*trackData.vStartGrid[3] = <<3467.3928, 5192.4863, 0.0000>>
			trackData.fStartGrid[3] = 227.2083*/
			trackData.vStartGrid[3] = <<3465.2865, 5190.2099, 0.0000>>
			trackData.fStartGrid[3] = 227.2083
			
			trackData.vStartGrid[4] = <<3472.7682, 5208.6398, 0.0000>>
			trackData.fStartGrid[4] = 227.2083
			trackData.vStartGrid[5] = <<3468.5549, 5204.0884, 0.0000>>
			trackData.fStartGrid[5] = 227.2083
			/*trackData.vStartGrid[5] = <<3466.4482, 5201.8125, 0.0000>>
			trackData.fStartGrid[5] = 227.2083*/
			trackData.vStartGrid[6] = <<3464.3416, 5199.5371, 0.0000>>
			trackData.fStartGrid[6] = 227.2083
			/*trackData.vStartGrid[7] = <<3462.2349, 5197.2612, 0.0000>>
			trackData.fStartGrid[7] = 227.2083*/
			trackData.vStartGrid[7] = <<3460.1283, 5194.9853, 0.0000>>
			trackData.fStartGrid[7] = 227.2083
			
			// Checkpoints
			trackData.iNumCheckpoints = 29
			trackData.vCheckPoint[0] = <<3533.9011, 5140.1958, 0.0000>>
			trackData.vCheckPoint[1] = <<3561.5569, 5013.9424, 0.0000>>
			trackData.vCheckPoint[2] = <<3602.2319, 4928.8960, 0.0000>>
			trackData.vCheckPoint[3] = <<3682.4077, 4853.4238, 0.0000>>
			trackData.vCheckPoint[4] = <<3776.3032, 4862.3999, 0.0000>>
			trackData.vCheckPoint[5] = <<3838.7886, 4851.9604, 0.0000>>
			trackData.vCheckPoint[6] = <<3879.3362, 4777.7324, 0.0000>>
			trackData.vCheckPoint[7] = <<3875.3787, 4684.5742, 0.0000>>
			trackData.vCheckPoint[8] = <<4004.3552, 4513.1582, 0.0000>>
			trackData.vCheckPoint[9] = <<4028.4202, 4457.3506, 0.0000>>
			trackData.vCheckPoint[10] = <<4011.7520, 4363.8379, 0.0000>>
			trackData.vCheckPoint[11] = <<3965.2295, 4321.5239, 0.0000>>
			trackData.vCheckPoint[12] = <<3958.3074, 4269.1636, 0.0000>>
			trackData.vCheckPoint[13] = <<4007.0706, 4201.3105, 0.0000>>
			trackData.vCheckPoint[14] = <<4067.8120, 4155.9502, 0.0000>>
			trackData.vCheckPoint[15] = <<4124.1660, 4158.2925, 0.0000>>
			trackData.vCheckPoint[16] = <<4173.6597, 4207.4946, 0.0000>>
			trackData.vCheckPoint[17] = <<4200.3247, 4325.8677, 0.0000>>
			trackData.vCheckPoint[18] = <<4232.3613, 4458.9678, 0.0000>>
			trackData.vCheckPoint[19] = <<4212.9502, 4519.7437, 0.0000>>
			trackData.vCheckPoint[20] = <<4137.3862, 4562.0757, 0.0000>>
			trackData.vCheckPoint[21] = <<3952.5613, 4561.6904, 0.0000>>
			trackData.vCheckPoint[22] = <<3850.7271, 4674.4795, 0.0000>>
			trackData.vCheckPoint[23] = <<3840.6772, 4729.6973, 0.0000>>
			trackData.vCheckPoint[24] = <<3868.6235, 4810.9731, 0.0000>>
			trackData.vCheckPoint[25] = <<3861.7407, 4847.7783, 0.0000>>
			trackData.vCheckPoint[26] = <<3819.8706, 4912.8467, 0.0000>>
			trackData.vCheckPoint[27] = <<3707.3240, 5047.7964, 0.0000>>
			trackData.vCheckPoint[28] = <<3603.6787, 5125.1240, 0.0000>>
		BREAK

		// Raton Canyon
		CASE SEA_RACE_03
			
			// Laps
			trackData.iNumLaps = 1

			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player			
			trackData.drivingSpeedSlow = 20
			trackData.drivingSpeedMedium = 30
			trackData.drivingSpeedFast = 40

			// Racers
			trackData.iNumAIRacers = 3
			trackData.vStartGrid[0] = <<207.0846, 3615.2004, 30.1000>>
			trackData.fStartGrid[0] = -188.0000
//			trackData.vStartGrid[1] = <<204.0137, 3615.6321, 30.1000>>
//			trackData.fStartGrid[1] = -188.0000
			trackData.vStartGrid[1] = <<200.9427, 3616.0635, 30.1000>>
			trackData.fStartGrid[1] = -188.0000
//			trackData.vStartGrid[3] = <<197.8718, 3616.4951, 30.1000>>
//			trackData.fStartGrid[3] = -188.0000
			trackData.vStartGrid[2] = <<194.8008, 3616.9268, 30.1000>>
			trackData.fStartGrid[2] = -188.0000
//			trackData.vStartGrid[5] = <<191.7299, 3617.3584, 30.1000>>
//			trackData.fStartGrid[5] = -188.0000
			trackData.vStartGrid[3] = <<188.6589, 3617.7900, 30.1000>>
			trackData.fStartGrid[3] = -188.0000
//			trackData.vStartGrid[7] = <<185.5880, 3618.2217, 30.1000>>
//			trackData.fStartGrid[7] = -188.0000
/*			trackData.vStartGrid[8] = <<208.0629, 3622.1609, 30.1000>>
			trackData.fStartGrid[8] = -188.0000
			trackData.vStartGrid[9] = <<204.9919, 3622.5925, 30.1000>>
			trackData.fStartGrid[9] = -188.0000
			trackData.vStartGrid[10] = <<201.9210, 3623.0242, 30.1000>>
			trackData.fStartGrid[10] = -188.0000
			trackData.vStartGrid[11] = <<198.8500, 3623.4558, 30.1000>>
			trackData.fStartGrid[11] = -188.0000
			trackData.vStartGrid[12] = <<195.7791, 3623.8875, 30.1000>>
			trackData.fStartGrid[12] = -188.0000
			trackData.vStartGrid[13] = <<192.7081, 3624.3188, 30.1000>>
			trackData.fStartGrid[13] = -188.0000
			trackData.vStartGrid[14] = <<189.6372, 3624.7505, 30.1000>>
			trackData.fStartGrid[14] = -188.0000
			trackData.vStartGrid[15] = <<186.5662, 3625.1821, 30.1000>>
			trackData.fStartGrid[15] = -188.0000
*/
			// Checkpoints
			trackData.iNumCheckpoints = 14//22
			trackData.vCheckPoint[0] = <<156.2047, 3459.9316, 30.0000>>
			trackData.vCheckPoint[1] = <<113.8073, 3309.7798, 27.9960>>
			trackData.vCheckPoint[2] = <<36.4057, 3136.9475, 25.0106>>
			trackData.vCheckPoint[3] = <<-128.4259, 3113.2278, 20.7191>>
			trackData.vCheckPoint[4] = <<-232.1300, 3004.5320, 16.8594>>//<<-233.6271, 2995.9304, 16.4821>>
			trackData.vCheckPoint[5] = <<-371.5406, 3013.6919, 11.5494>>
			trackData.vCheckPoint[6] = <<-479.2344, 2908.7031, 12.1299>>
			trackData.vCheckPoint[7] = <<-611.1117, 2960.9141, 12.2048>>
			trackData.vCheckPoint[8] = <<-740.9820, 2867.5647, 12.1505>>
			trackData.vCheckPoint[9] = <<-863.8170, 2804.0242, 8.5818>>//<<-866.2031, 2798.3904, 8.2937>>
			trackData.vCheckPoint[10] = <<-1023.5380, 2833.8887, 2.9270>>
			trackData.vCheckPoint[11] = <<-1160.6161, 2766.0032, -1.4915>>
			trackData.vCheckPoint[12] = <<-1278.0231, 2669.2307, -0.6163>>
			trackData.vCheckPoint[13] = <<-1450.4285, 2625.4727, 0.0000>>
			/*trackData.vCheckPoint[14] = <<-1576.7704, 2641.2407, 0.0000>>
			trackData.vCheckPoint[15] = <<-1717.0342, 2592.6255, 0.0000>>
			trackData.vCheckPoint[16] = <<-1868.8031, 2586.3413, 0.0000>>
			trackData.vCheckPoint[17] = <<-2018.0389, 2547.2488, 0.0000>>
			trackData.vCheckPoint[18] = <<-2162.3650, 2592.0564, 0.0000>>
			trackData.vCheckPoint[19] = <<-2312.0320, 2601.3264, 0.0000>>
			trackData.vCheckPoint[20] = <<-2463.6404, 2580.8298, 0.0000>>
			trackData.vCheckPoint[21] = <<-2638.4182, 2597.1497, 0.0000>>*/
		BREAK
		
		// Los Santos
		CASE SEA_RACE_04
			
			// Laps & Racers
			trackData.iNumLaps = 1
			
			// Base AI speeds for each skill level, ensure there is not much spread to remain competitive
			// Mission_race.sc has rubber banding to try to keep them near to the player
			trackData.drivingSpeedSlow = 72
			trackData.drivingSpeedMedium = 76
			trackData.drivingSpeedFast = 80
			
			// Racers
			trackData.iNumAIRacers = 7
			trackData.vStartGrid[0] = <<626.4072, -2135.9280, 0.0000>>
			trackData.fStartGrid[0] = 176.0000
			trackData.vStartGrid[1] = <<623.3137, -2135.7117, 0.0000>>
			trackData.fStartGrid[1] = 176.0000
			trackData.vStartGrid[2] = <<620.2201, -2135.4954, 0.0000>>
			trackData.fStartGrid[2] = 176.0000
			trackData.vStartGrid[3] = <<617.1265, -2135.2791, 0.0000>>
			trackData.fStartGrid[3] = 176.0000
			trackData.vStartGrid[4] = <<626.8975, -2128.9163, 0.0000>>
			trackData.fStartGrid[4] = 176.0000
			trackData.vStartGrid[5] = <<623.8040, -2128.6997, 0.0000>>
			trackData.fStartGrid[5] = 176.0000
			trackData.vStartGrid[6] = <<620.7104, -2128.4834, 0.0000>>
			trackData.fStartGrid[6] = 176.0000
			trackData.vStartGrid[7] = <<617.6168, -2128.2671, 0.0000>>
			trackData.fStartGrid[7] = 176.0000
			/*trackData.vStartGrid[8] = <<627.3879, -2121.9043, 0.0000>>
			trackData.fStartGrid[8] = 176.0000
			trackData.vStartGrid[9] = <<624.2943, -2121.6880, 0.0000>>
			trackData.fStartGrid[9] = 176.0000
			trackData.vStartGrid[10] = <<621.2007, -2121.4717, 0.0000>>
			trackData.fStartGrid[10] = 176.0000
			trackData.vStartGrid[11] = <<618.1071, -2121.2554, 0.0000>>
			trackData.fStartGrid[11] = 176.0000
			trackData.vStartGrid[12] = <<627.8782, -2114.8926, 0.0000>>
			trackData.fStartGrid[12] = 176.0000
			trackData.vStartGrid[13] = <<624.7846, -2114.6760, 0.0000>>
			trackData.fStartGrid[13] = 176.0000
			trackData.vStartGrid[14] = <<621.6910, -2114.4597, 0.0000>>
			trackData.fStartGrid[14] = 176.0000
			trackData.vStartGrid[15] = <<618.5975, -2114.2434, 0.0000>>
			trackData.fStartGrid[15] = 176.0000*/
			
			// Checkpoints
			trackData.iNumCheckpoints = 25
			//trackData.vCheckPoint[0] = <<621.6622, -2137.0999, 0.0000>>
			//trackData.vCheckPoint[0] = <<621.6622, -2137.0999, 0.0000>>
			trackData.vCheckPoint[0] = <<628.2693, -2361.5728, 0.0000>>
			trackData.vCheckPoint[1] = <<624.4983, -2472.2712, 0.0000>>
			trackData.vCheckPoint[2] = <<656.1755, -2551.0635, 0.0000>>
			trackData.vCheckPoint[3] = <<707.1786, -2577.5090, 0.0000>>
			trackData.vCheckPoint[4] = <<821.6584, -2613.4531, 0.0000>>
			trackData.vCheckPoint[5] = <<913.3214, -2662.6289, 0.0000>>
			trackData.vCheckPoint[6] = <<992.7930, -2725.7344, 0.0000>>
			trackData.vCheckPoint[7] = <<1010.9907, -2799.7744, 0.0000>>
			trackData.vCheckPoint[8] = <<955.9268, -2841.9895, 0.0000>>
			trackData.vCheckPoint[9] = <<838.4080, -2820.5564, 0.0000>>
			trackData.vCheckPoint[10] = <<788.3865, -2739.5254, 0.0000>>
			trackData.vCheckPoint[11] = <<781.0709, -2635.6768, 0.0000>>
			trackData.vCheckPoint[12] = <<707.1389, -2600.3418, 0.0000>>
			trackData.vCheckPoint[13] = <<581.3910, -2527.3098, -1.0000>>
			trackData.vCheckPoint[14] = <<523.5403, -2445.4587, 0.0000>>
			trackData.vCheckPoint[15] = <<465.7080, -2385.9646, 0.0000>>
			trackData.vCheckPoint[16] = <<409.5882, -2270.4673, 0.0000>>
			trackData.vCheckPoint[17] = <<323.1916, -2259.3044, 0.0000>>
			trackData.vCheckPoint[18] = <<271.2700, -2286.1335, 0.0000>>
			trackData.vCheckPoint[19] = <<261.3279, -2344.9370, 0.0000>>
			trackData.vCheckPoint[20] = <<295.7343, -2372.1904, 0.0000>>
			trackData.vCheckPoint[21] = <<372.9001, -2373.0869, 0.0000>>
			trackData.vCheckPoint[22] = <<410.8412, -2413.0657, 0.0000>>
			trackData.vCheckPoint[23] = <<412.1277, -2539.3833, 0.0000>>
			trackData.vCheckPoint[24] = <<413.6328, -2712.8701, 0.0000>>
			/*trackData.iNumCheckpoints = 15
			//trackData.vCheckPoint[0] = <<0.0000, 0.0000, 0.0000>>
			//trackData.vCheckPoint[1] = <<621.6622, -2137.0999, 0.0000>>
			trackData.vCheckPoint[0] = <<628.2693, -2361.5728, 0.0000>>
			trackData.vCheckPoint[1] = <<624.4983, -2472.2712, 0.0000>>
			trackData.vCheckPoint[2] = <<610.3095, -2524.4880, 0.0000>>
			trackData.vCheckPoint[3] = <<581.3910, -2527.3098, 0.0000>>
			trackData.vCheckPoint[4] = <<523.5403, -2445.4587, 0.0000>>
			trackData.vCheckPoint[5] = <<465.7080, -2385.9646, 0.0000>>
			trackData.vCheckPoint[6] = <<409.5882, -2270.4673, 0.0000>>
			trackData.vCheckPoint[7] = <<323.1916, -2259.3044, 0.0000>>
			trackData.vCheckPoint[8] = <<271.2700, -2286.1335, 0.0000>>
			trackData.vCheckPoint[9] = <<261.3279, -2344.9370, 0.0000>>
			trackData.vCheckPoint[10] = <<295.7343, -2372.1904, 0.0000>>
			trackData.vCheckPoint[11] = <<372.9001, -2373.0869, 0.0000>>
			trackData.vCheckPoint[12] = <<410.8412, -2413.0657, 0.0000>>
			trackData.vCheckPoint[13] = <<412.1277, -2539.3833, 0.0000>>
			trackData.vCheckPoint[14] = <<413.6328, -2712.8701, 0.0000>>*/
			/*trackData.iNumCheckpoints = 28
			//trackData.vCheckPoint[1] = <<621.6622, -2137.0999, 0.0000>>
			trackData.vCheckPoint[0] = <<598.3651, -2408.5032, 0.0000>>
			trackData.vCheckPoint[1] = <<523.5403, -2445.4587, 0.0000>>
			trackData.vCheckPoint[2] = <<465.7080, -2385.9646, 0.0000>>
			trackData.vCheckPoint[3] = <<409.5882, -2270.4673, 0.0000>>
			trackData.vCheckPoint[4] = <<323.1916, -2259.3044, 0.0000>>
			trackData.vCheckPoint[5] = <<271.2700, -2286.1335, 0.0000>>
			trackData.vCheckPoint[6] = <<261.3279, -2344.9370, 0.0000>>
			trackData.vCheckPoint[7] = <<331.5211, -2370.2644, 0.0000>>
			trackData.vCheckPoint[8] = <<426.1933, -2377.4397, 0.0000>>
			trackData.vCheckPoint[9] = <<499.9052, -2439.6741, 0.0000>>
			trackData.vCheckPoint[10] = <<591.1573, -2535.2144, 0.0000>>
			trackData.vCheckPoint[11] = <<699.8745, -2600.2773, 0.0000>>
			trackData.vCheckPoint[12] = <<762.7516, -2617.9734, 0.0000>>
			trackData.vCheckPoint[13] = <<808.4507, -2684.6836, 0.0000>>
			trackData.vCheckPoint[14] = <<841.6678, -2800.7661, 0.0000>>
			trackData.vCheckPoint[15] = <<916.5242, -2828.1726, 0.0000>>
			trackData.vCheckPoint[16] = <<969.0256, -2795.9204, 0.0000>>
			trackData.vCheckPoint[17] = <<983.9186, -2723.2905, 0.0000>>
			trackData.vCheckPoint[18] = <<914.7256, -2678.0764, 0.0000>>
			trackData.vCheckPoint[19] = <<844.6096, -2628.6675, 0.0000>>
			trackData.vCheckPoint[20] = <<763.8650, -2584.3762, 0.0000>>
			trackData.vCheckPoint[21] = <<694.4482, -2576.1707, 0.0000>>
			trackData.vCheckPoint[22] = <<599.6021, -2523.5247, 0.0000>>
			trackData.vCheckPoint[23] = <<532.1508, -2457.5498, 0.0000>>
			trackData.vCheckPoint[24] = <<455.2576, -2390.0425, 0.0000>>
			trackData.vCheckPoint[25] = <<403.9789, -2422.3257, 0.0000>>
			trackData.vCheckPoint[26] = <<412.1277, -2539.3833, 0.0000>>
			trackData.vCheckPoint[27] = <<414.6720, -2728.1218, 0.0000>>*/
			/*trackData.iNumCheckpoints = 13
			trackData.vCheckPoint[0] = <<621.6622, -2137.0999, 0.0000>>
			trackData.vCheckPoint[1] = <<598.3651, -2408.5032, 0.0000>>
			trackData.vCheckPoint[2] = <<523.5403, -2445.4587, 0.0000>>
			trackData.vCheckPoint[3] = <<465.7080, -2385.9646, 0.0000>>
			trackData.vCheckPoint[4] = <<319.5621, -2357.7468, 0.0000>>
			trackData.vCheckPoint[5] = <<199.1218, -2318.0977, 0.0000>>
			trackData.vCheckPoint[6] = <<65.7813, -2265.3711, 0.0000>>
			trackData.vCheckPoint[7] = <<61.1874, -2058.6035, 0.0000>>
			trackData.vCheckPoint[8] = <<-192.9928, -1811.0072, 0.0000>>
			trackData.vCheckPoint[9] = <<-351.7690, -1691.6627, 0.0000>>
			trackData.vCheckPoint[10] = <<-473.5175, -1573.0287, 0.0000>>
			trackData.vCheckPoint[11] = <<-616.0802, -1512.7153, 0.0000>>
			trackData.vCheckPoint[12] = <<-866.3201, -1654.8147, 0.0000>>*/
		BREAK
	ENDSWITCH
ENDPROC
