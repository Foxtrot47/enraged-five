// Includes
USING "commands_hud.sch"
USING "script_drawing.sch"

// Constants
CONST_INT MAX_AI_RACERS 	15
CONST_INT MAX_GRID 			16
CONST_INT MAX_CHECKPOINTS	60

// Enums
ENUM RACE_TRACK_ID
	STREET_RACE_01,			// South Los Santos
	STREET_RACE_02,			// City Circuit
	STREET_RACE_04,			// Airport
	STREET_RACE_05,			// Freeway
	STREET_RACE_06,			// Vespucci Canals
	SEA_RACE_01,			// North Coast
	SEA_RACE_02,			// South Coast
	SEA_RACE_03,			// Raton Canyon
	SEA_RACE_04,			// Los Santos
	RACE_COUNT,
	RACE_INVALID
ENDENUM

ENUM RACE_TYPE
	RACETYPE_UNDEFINED,
	RACETYPE_BIKE,
	RACETYPE_CAR,
	RACETYPE_SEA
ENDENUM

// Structs
STRUCT TRACK_DATA
	VECTOR vCheckpoint[MAX_CHECKPOINTS]
	VECTOR vStartGrid[MAX_GRID]
	FLOAT fStartGrid[MAX_GRID]
	INT iNumLaps = 0
	INT iNumCheckpoints = 0
	INT	iNumAIRacers = 0
	FLOAT drivingSpeedSlow 	  // Base vehicle speeds - the race AI can alter these up/down depending on if the AI is behind/ahead of the player
	FLOAT drivingSpeedMedium
	FLOAT drivingSpeedFast
ENDSTRUCT

STRUCT RACE_DATA
	RACE_TRACK_ID eRaceTrack   = RACE_INVALID
	RACE_TYPE	  eRaceType    = RACETYPE_UNDEFINED
	INT  		  iCandidateID = NO_CANDIDATE_ID
	INT  		  iRaceFee     = 1000
	INT  		  iSaveSlot    = -1
	BOOL		  bP2P		   = FALSE
ENDSTRUCT

STRUCT PLAYER_VEHICLE
	VEHICLE_INDEX vehPlayerVehicle
	BLIP_INDEX biPlayerVehicle // Used if player exits his vehicle during the race
	BOOL bDisplayedGetBackInVehicleText // Only display the 'get back in the vehicle' text once
ENDSTRUCT
PLAYER_VEHICLE sPlayerVehicle

/// PURPOSE: Display race help message
PROC DISPLAY_RACE_HELP(STRING text)
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(text)
		PRINT_HELP_FOREVER(text)
	ENDIF
ENDPROC

/// PURPOSE: Display race help message with number
PROC DISPLAY_RACE_HELP_WITH_NUMBER(STRING text, INT num)

	IF NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(text, num)
		PRINT_HELP_FOREVER_WITH_NUMBER(text, num)
	ENDIF
ENDPROC

/// PURPOSE: Display race help message with two numbers
PROC DISPLAY_RACE_HELP_WITH_TWO_NUMBERS(STRING text, INT num1, INT num2)

	IF NOT IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED(text, num1, num2)
		BEGIN_TEXT_COMMAND_DISPLAY_HELP(text)
			ADD_TEXT_COMPONENT_INTEGER(num1)
			ADD_TEXT_COMPONENT_INTEGER(num2)
		END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the model name from the race type
FUNC MODEL_NAMES GET_MODEL_NAME_BASED_OFF_RACE_TYPE(RACE_TYPE rtype)
	MODEL_NAMES vehModel
	SWITCH rtype
		CASE RACETYPE_BIKE vehModel = BATI BREAK
		CASE RACETYPE_CAR  vehModel = NINEF    BREAK
		CASE RACETYPE_SEA  vehModel = SEASHARK BREAK
	ENDSWITCH
	return vehModel
ENDFUNC

/// PURPOSE:
///    Returns the correct cloud user name based on which platform we are running
/// RETURNS:
///    String of the user name
FUNC STRING GET_CLOUD_ID()
	
	STRING sReturnString

	IF IS_PS3_VERSION()
		sReturnString = "SPRACE"
	ELIF IS_XBOX360_VERSION()
		sReturnString = "2535285330962926"
	ELIF IS_PLAYSTATION_PLATFORM()
		sReturnString = "SPRACE"
	ELIF IS_XBOX_PLATFORM()
		sReturnString = "2535285330962926"
	ELIF IS_PC_VERSION()
		// Need to setup cloud data for PC version
		sReturnString = "SPRACE"
    ELSE
		sReturnString = "SPRACE"
	ENDIF
	
	RETURN sReturnString
ENDFUNC

/// PURPOSE:
///    String of the user name
FUNC STRING GET_RACE_FILENAME(RACE_TRACK_ID raceTrack)

	STRING sFilename
	
	IF IS_PS3_VERSION()
		SWITCH raceTrack
			CASE SEA_RACE_04 	sFilename 	= "qLp8OsaeTkCjzhK0SZoRVA" BREAK	//			SeaRace4
			CASE SEA_RACE_02 	sFilename 	= "JhV_7Ir4ekSQLafj22vFkg" BREAK	//			SeaRace2
			CASE SEA_RACE_03 	sFilename 	= "Fuc2Yl2sukOrORoMo1YJ1A" BREAK	//			SeaRace3
			CASE SEA_RACE_01 	sFilename 	= "aNlcpqEkhUytgK-8IMbTYQ" BREAK	//			SeaRace1
			CASE STREET_RACE_01 sFilename 	= "9aLp9VEnME25Mp_6XZaw0A" BREAK	//			CityRace1
			CASE STREET_RACE_04 sFilename 	= "lT9gI2mfrkGDhiW1lSlhbw" BREAK	// OR POSSIBLY 8bvbHCAJykmlKzY5PeLDyw	//			CityRace4
			CASE STREET_RACE_05 sFilename 	= "YxiNucGMGEu4lCKqizI2lA" BREAK	//			CityRace5
			CASE STREET_RACE_02 sFilename 	= "szYNFSberECI5goiWsh1bw" BREAK	//			CityRace2
			CASE STREET_RACE_06 sFilename 	= "nZ4p_4_F0EOFyZa2yKEHGA" BREAK	//			CityRace6			
		ENDSWITCH
	ELIF IS_XBOX360_VERSION()
		SWITCH raceTrack
			// Street Races
			CASE STREET_RACE_01 sFilename = "dm_test_20" BREAK	// South Los Santos
			CASE STREET_RACE_02 sFilename = "dm_test_18" BREAK	// City Circuit
			CASE STREET_RACE_04 sFilename = "dm_test_2"  BREAK	// Airport
			CASE STREET_RACE_05 sFilename = "dm_test_4"  BREAK  // Freeway
			CASE STREET_RACE_06 sFilename = "dm_test_5"  BREAK  // Vespucci Canals
			
			// Sea Races
			CASE SEA_RACE_01 sFilename = "dm_test_16" BREAK		// North Coast
			CASE SEA_RACE_02 sFilename = "dm_test_13" BREAK     // South Coast
			CASE SEA_RACE_03 sFilename = "dm_test_15" BREAK     // Raton Canyon
			CASE SEA_RACE_04 sFilename = "dm_test_24" BREAK     // Los Santos		
		ENDSWITCH
	ELIF IS_PLAYSTATION_PLATFORM()
		SWITCH raceTrack
			CASE SEA_RACE_04 	sFilename 	= "qLp8OsaeTkCjzhK0SZoRVA" BREAK	//			SeaRace4
			CASE SEA_RACE_02 	sFilename 	= "JhV_7Ir4ekSQLafj22vFkg" BREAK	//			SeaRace2
			CASE SEA_RACE_03 	sFilename 	= "Fuc2Yl2sukOrORoMo1YJ1A" BREAK	//			SeaRace3
			CASE SEA_RACE_01 	sFilename 	= "aNlcpqEkhUytgK-8IMbTYQ" BREAK	//			SeaRace1
			CASE STREET_RACE_01 sFilename 	= "9aLp9VEnME25Mp_6XZaw0A" BREAK	//			CityRace1
			CASE STREET_RACE_04 sFilename 	= "lT9gI2mfrkGDhiW1lSlhbw" BREAK	// OR POSSIBLY 8bvbHCAJykmlKzY5PeLDyw	//			CityRace4
			CASE STREET_RACE_05 sFilename 	= "YxiNucGMGEu4lCKqizI2lA" BREAK	//			CityRace5
			CASE STREET_RACE_02 sFilename 	= "szYNFSberECI5goiWsh1bw" BREAK	//			CityRace2
			CASE STREET_RACE_06 sFilename 	= "nZ4p_4_F0EOFyZa2yKEHGA" BREAK	//			CityRace6			
		ENDSWITCH
	ELIF IS_XBOX_PLATFORM()
		SWITCH raceTrack
			// Street Races
			CASE STREET_RACE_01 sFilename = "dm_test_20" BREAK	// South Los Santos
			CASE STREET_RACE_02 sFilename = "dm_test_18" BREAK	// City Circuit
			CASE STREET_RACE_04 sFilename = "dm_test_2"  BREAK	// Airport
			CASE STREET_RACE_05 sFilename = "dm_test_4"  BREAK  // Freeway
			CASE STREET_RACE_06 sFilename = "dm_test_5"  BREAK  // Vespucci Canals
			
			// Sea Races
			CASE SEA_RACE_01 sFilename = "dm_test_16" BREAK		// North Coast
			CASE SEA_RACE_02 sFilename = "dm_test_13" BREAK     // South Coast
			CASE SEA_RACE_03 sFilename = "dm_test_15" BREAK     // Raton Canyon
			CASE SEA_RACE_04 sFilename = "dm_test_24" BREAK     // Los Santos		
		ENDSWITCH
	ELIF IS_PC_VERSION()
		SWITCH raceTrack
			// Street Races
			CASE STREET_RACE_01 sFilename = "dm_test_20" BREAK	// South Los Santos
			CASE STREET_RACE_02 sFilename = "dm_test_18" BREAK	// City Circuit
			CASE STREET_RACE_04 sFilename = "dm_test_2"  BREAK	// Airport
			CASE STREET_RACE_05 sFilename = "dm_test_4"  BREAK  // Freeway
			CASE STREET_RACE_06 sFilename = "dm_test_5"  BREAK  // Vespucci Canals
			
			// Sea Races
			CASE SEA_RACE_01 sFilename = "dm_test_16" BREAK		// North Coast
			CASE SEA_RACE_02 sFilename = "dm_test_13" BREAK     // South Coast
			CASE SEA_RACE_03 sFilename = "dm_test_15" BREAK     // Raton Canyon
			CASE SEA_RACE_04 sFilename = "dm_test_24" BREAK     // Los Santos		
		ENDSWITCH
	ENDIF
	RETURN sFilename
ENDFUNC

PROC SETUP_ROAD_BLOCKING(RACE_TRACK_ID raceTrack, VECTOR & vRaceRoadPos1, VECTOR & vRaceRoadPos2, FLOAT & fRaceRoadWidth)
	SWITCH raceTrack

		// South Los Santos
		CASE STREET_RACE_01
			vRaceRoadPos1 = <<311.126221,-1896.261353,20.683289>>//<<-189.618484,-1616.818237,33.681194>>
			vRaceRoadPos2 = <<-295.065338,-1405.529663,35.314598>>//<<-131.111008,-1538.352417,32.255104>>
			fRaceRoadWidth = 300.0//30.0
		BREAK
		
		// City Circuit
		CASE STREET_RACE_02
			vRaceRoadPos1 = <<273.184967,335.296875,105.570442>>
			vRaceRoadPos2 = <<412.287659,299.230408,101.072403>>
			fRaceRoadWidth = 20.0
		BREAK
		
		// Airport
		CASE STREET_RACE_04
			vRaceRoadPos1 = <<-823.715698,-2588.000488,13.769104>>
			vRaceRoadPos2 = <<-801.335632,-2464.006592,12.429126>>
			fRaceRoadWidth = 30.0
		BREAK
		
		// Freeway		
		CASE STREET_RACE_05
			vRaceRoadPos1 = <<791.825745,-1423.862671,24.179544>>//<<800.515930,-1149.139771,27.544914>>
			vRaceRoadPos2 = <<783.166992,-1015.648010,50.241344>>//<<731.347717,-1149.962402,32.013943>>
			fRaceRoadWidth = 200.0//20.0
		BREAK
		
		// Vespucci Canals		
		CASE STREET_RACE_06
			vRaceRoadPos1 = <<-1084.030396,-1165.723511,0.150211>>
			vRaceRoadPos2 = <<-1029.212891,-1134.580933,2.174534>>
			fRaceRoadWidth = 10.0
		BREAK
	ENDSWITCH
ENDPROC
