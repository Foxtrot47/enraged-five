USING "minigame_uiinputs.sch"
USING "hud_drawing.sch"
USING "screens_header.sch"
USING "UIUtil.sch"
USING "script_oddjob_funcs.sch"
USING "script_usecontext.sch"
USING "common_races.sch"
USING "RC_Helper_Functions.sch"
USING "screen_placements.sch"
USING "screen_placements_export.sch"
USING "socialclub_leaderboard.sch"
USING "net_leaderboards.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		SCRIPT NAME		:	Race_Results.sch
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Deals with the end of race results
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

ENUM CR_FINISH_CUT_STATE
	CR_FINISH_INIT = 0,
	CR_FINISH_CREATE_CAM,
	CR_FINISH_WAIT_FOR_RESULTS,
	CR_FINISH_IDLE
ENDENUM

/*ENUM OFFROAD_SCREEN_TEXT
	OFFROAD_SC_MENU_TITLE,
	OFFROAD_SC_STATS_TITLE,
	OFFROAD_SC_STAT1_TXT,
	OFFROAD_SC_STAT2_TXT,
	OFFROAD_SC_STAT3_TXT,
	OFFROAD_SC_STAT4_TXT,
	OFFROAD_SC_STAT5_TXT,
	OFFROAD_SC_STAT6_TXT,
	OFFROAD_SC_STAT7_TXT,
	OFFROAD_SC_STAT8_TXT,
	OFFROAD_SC_STAT1VAL_TXT,
	OFFROAD_SC_STAT2VAL_TXT,
	OFFROAD_SC_STAT3VAL_TXT,
	OFFROAD_SC_STAT4VAL_TXT,
	OFFROAD_SC_STAT5VAL_TXT,
	OFFROAD_SC_STAT6VAL_TXT,
	OFFROAD_SC_STAT7VAL_TXT,
	OFFROAD_SC_BRONZEGOAL_TXT,
	OFFROAD_SC_SILVERGOAL_TXT,
	OFFROAD_SC_GOLDGOAL_TXT
ENDENUM

ENUM OFFROAD_SCREEN_SPRITE
	OFFROAD_SC_BACKGROUND,
	OFFROAD_SC_RACE_IMG,
	OFFROAD_SC_MEDAL_AWARD_IMG,
	OFFROAD_SC_BRONZE_IMG,
	OFFROAD_SC_SILVER_IMG,
	OFFROAD_SC_GOLD_IMG
ENDENUM

ENUM OFFROAD_SCREEN_RECT
	OFFROAD_SC_STATS_HEADER_BG,
	OFFROAD_SC_STATS_EDGE,
	OFFROAD_SC_RACE_IMG_BG,
	OFFROAD_SC_STAT1_BG,
	OFFROAD_SC_STAT2_BG,
	OFFROAD_SC_STAT3_BG,
	OFFROAD_SC_STAT4_BG,
	OFFROAD_SC_STAT5_BG,
	OFFROAD_SC_STAT6_BG,
	OFFROAD_SC_BRONZE_BG,
	OFFROAD_SC_SILVER_BG,
	OFFROAD_SC_GOLD_BG,
	OFFROAD_SC_BRONZE_OVERLAY,
	OFFROAD_SC_SILVER_OVERLAY,
	OFFROAD_SC_GOLD_OVERLAY
ENDENUM*/

STRUCT RACE_RESULTS_STRUCT
	CR_FINISH_CUT_STATE eCRCutFinish = CR_FINISH_INIT
	INT CAM_INTERP_TIME = 4200//4500
	INT PULSE_TIME = 3600//3000
	INT iDisplayResultsTimer = 0
	INT iPulseTimer = 0
	CAMERA_INDEX ciCityRaceFinishCam[8]
	//CAMERA_INDEX ciCityRaceFinishCamTwo[2]
	SIMPLE_USE_CONTEXT ucInstructions
	VECTOR vCamPos[8]
	VECTOR vCamRot[8]
	FLOAT fFOV
ENDSTRUCT

/*CR_FINISH_CUT_STATE eCRCutFinish = CR_FINISH_INIT

CONST_INT CAM_INTERP_TIME 3000
INT iDisplayResultsTimer = 0

CAMERA_INDEX ciCityRaceFinishCam

SIMPLE_USE_CONTEXT ucInstructions*/

SC_LEADERBOARD_CONTROL_STRUCT sclbControl

// Variables for getting best times from the leaderboards
INT iReadStage
INT iLoadStage
BOOL bSuccessful
INT iRaceOnlineID
INT iGlobalBest = -1
INT iPersonalBest = -1
BOOL bBestTimesReady = FALSE
BOOL bAllowLeaderboard = TRUE

// ===========================
//  FUNCTIONS
// ===========================

SPRITE_PLACEMENT TempScoreCard
PROC TEMP_SCORE_CARD_INIT ()
	PIXEL_POSITION_AND_SIZE_SPRITE(TempScoreCard, 512, 69, 512, 520, TRUE)
	SPRITE_COLOR(TempScoreCard, 255,255,255,64)
ENDPROC

FUNC FLOAT HT_HUD_FORMAT_FLOAT_SCORE(FLOAT thisFloat)
	IF thisFloat < 0
		RETURN 0.0
	ENDIF
	//if the formatting is fine we'll get here
	RETURN thisFloat
ENDFUNC

PROC SET_FINISH_CAM_POSITIONS(RACE_RESULTS_STRUCT& sRaceResults, RACE_TRACK_ID trackID)
	MODEL_NAMES mnCar
	IF IS_VEHICLE_OK(sPlayerVehicle.vehPlayerVehicle)
		mnCar = GET_ENTITY_MODEL(sPlayerVehicle.vehPlayerVehicle)
	ENDIF
	
	SWITCH trackID
		CASE STREET_RACE_01
			sRaceResults.vCamPos[0] = <<-165.999557,-1593.640259,36.050823>>
			sRaceResults.vCamRot[0] = <<-1.140186,-0.000000,-18.934132>>
			sRaceResults.vCamPos[1] = <<-166.138535,-1593.817627,36.055077>>
			sRaceResults.vCamRot[1] = <<-1.140186,0.000000,-18.478167>>
			
			sRaceResults.vCamPos[2] = <<-166.138535,-1593.817627,46.055077>>//<<-165.209152,-1595.171509,46.560944>>
			sRaceResults.vCamRot[2] = <<52.6710, 0.0000, -18.5600>>//<<30.3861, -0.0000, -18.5600>>//<<-1.140186,0.000000,-18.478167>>//<<9.106841,-0.000000,-13.721704>>
			
			sRaceResults.vCamPos[3] = <<-167.659088,-1578.869995,45.294144>>
			sRaceResults.vCamRot[3] = <<52.6710, -0.1338, 10.8626>>//<<26.4972, -0.1338, 10.8626>>//<<-2.670335,-0.133780,10.862582>>
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("DUBSTA3"))
				sRaceResults.vCamPos[4] = <<-167.5634, -1578.0896, 36.1500>>
				sRaceResults.vCamRot[4] = <<-5.9218, -0.0947, 21.8453>>
				sRaceResults.vCamPos[5] = <<-166.1911, -1577.5270, 36.0991>>
				sRaceResults.vCamRot[5] = <<-7.2671, -0.0947, 44.6906>>
			ELIF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[4] = <<-172.7921, -1581.9796, 36.2618>>
				sRaceResults.vCamRot[4] = <<-4.0569, -0.1415, -22.1047>>
				sRaceResults.vCamPos[5] = <<-172.3110, -1582.1637, 36.2613>>
				sRaceResults.vCamRot[5] = <<-4.0569, -0.1415, -19.9102>>
			ELSE
				sRaceResults.vCamPos[4] = <<-167.659088,-1578.869995,35.294144>>
				sRaceResults.vCamRot[4] = <<-2.670335,-0.133780,10.862582>>
				sRaceResults.vCamPos[5] = <<-167.488312,-1578.681396,35.287971>>
				sRaceResults.vCamRot[5] = <<-2.670335,-0.133781,12.423575>>
			ENDIF
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[6] = <<-166.3731, -1571.6453, 37.2137>>
				sRaceResults.vCamRot[6] = <<-10.5428, -0.1337, 132.3746>>
				sRaceResults.vCamPos[7] = <<-164.2483, -1569.8260, 37.7305>>
				sRaceResults.vCamRot[7] = <<-10.5428, -0.1337, 142.4265>>
			ELSE
				sRaceResults.vCamPos[6] = <<-165.813171,-1572.277588,35.576958>>
				sRaceResults.vCamRot[6] = <<-6.502827,-0.133783,134.851166>>
				sRaceResults.vCamPos[7] = <<-165.657867,-1571.710205,35.635654>>
				sRaceResults.vCamRot[7] = <<-6.502827,-0.133782,138.748703>>
			ENDIF
			
			sRaceResults.fFOV = 25
		BREAK
		CASE STREET_RACE_02
			sRaceResults.vCamPos[0] = <<345.9444, 326.0113, 104.2977>>//<<357.2692, 322.7362, 103.9649>>
			sRaceResults.vCamRot[0] = <<-2.2338, 0.0066, -123.2325>>//<<-3.6091, 0.0066, -125.5736>>
			sRaceResults.vCamPos[1] = <<345.6188, 325.8368, 104.3046>>//<<355.0306, 323.6436, 104.1131>>
			sRaceResults.vCamRot[1] = <<-2.2338, 0.0066, -125.4908>>//<<-2.7873, 0.0066, -136.0524>>
			
			sRaceResults.vCamPos[2] = <<345.6188, 325.8368, 114.3046>>//<<351.9950, 316.2025, 115.5018>>//<<355.5760, 321.7519, 111.3383>>
			sRaceResults.vCamRot[2] = <<61.4186, 0.0066, -125.2516>>//<<26.8785, 0.0066, -125.2516>>//<<-2.2338, 0.0066, -125.4908>>//<<11.0052, 0.0066, -115.5175>>//<<6.2304, 0.0066, -124.6168>>
			
			sRaceResults.vCamPos[3] = <<349.7675, 318.5139, 114.5701>>
			sRaceResults.vCamRot[3] = <<61.4186, 0.0683, -51.2922>>//<<22.4462, 0.0683, -51.2922>>//<<-6.4077, 0.0685, -51.2659>>
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("DUBSTA3"))
				sRaceResults.vCamPos[4] = <<350.6226, 319.2393, 105.5075>>
				sRaceResults.vCamRot[4] = <<-14.1152, 0.0341, -34.8046>>
				sRaceResults.vCamPos[5] = <<352.4439, 318.4305, 105.5037>>
				sRaceResults.vCamRot[5] = <<-14.1153, 0.0341, -13.9017>>
			ELIF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[4] = <<345.9572, 322.6875, 105.8543>>
				sRaceResults.vCamRot[4] = <<-10.7212, 0.0790, -88.9707>>
				sRaceResults.vCamPos[5] = <<346.2430, 322.2848, 105.8016>>
				sRaceResults.vCamRot[5] = <<-9.6145, 0.0790, -87.2469>>
			ELSE
				sRaceResults.vCamPos[4] = <<349.7675, 318.5139, 104.5701>>
				sRaceResults.vCamRot[4] = <<-6.4077, 0.0685, -51.2659>>
				sRaceResults.vCamPos[5] = <<349.9963, 318.3574, 104.5610>>
				sRaceResults.vCamRot[5] = <<-6.4077, 0.0685, -46.9079>>
			ENDIF
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[6] = <<357.8919, 321.2549, 105.4607>>
				sRaceResults.vCamRot[6] = <<-3.2542, 0.0014, 80.7604>>
				sRaceResults.vCamPos[7] = <<359.4669, 319.8764, 105.6448>>
				sRaceResults.vCamRot[7] = <<-3.2542, 0.0014, 80.7604>>
			ELSE
				sRaceResults.vCamPos[6] = <<356.9244, 319.9118, 104.1889>>
				sRaceResults.vCamRot[6] = <<-3.2411, 0.0035, 62.3779>>
				sRaceResults.vCamPos[7] = <<357.7713, 320.1022, 104.2291>>
				sRaceResults.vCamRot[7] = <<-3.2411, 0.0035, 74.7132>>
			ENDIF
			
			sRaceResults.fFOV = 25
		BREAK
		CASE STREET_RACE_04
			sRaceResults.vCamPos[0] = <<-796.1253, -2532.3179, 13.7076>>
			sRaceResults.vCamRot[0] = <<3.1635, -0.0349, 129.4171>>
			sRaceResults.vCamPos[1] = <<-796.1734, -2532.2598, 13.7076>>
			sRaceResults.vCamRot[1] = <<3.1635, -0.0349, 123.6241>>
			
			sRaceResults.vCamPos[2] = <<-796.1734, -2532.2598, 23.7076>>//<<-801.9807, -2532.1855, 25.3552>>
			sRaceResults.vCamRot[2] = <<53.9221, -0.0349, 122.1631>>//<<28.4593, -0.0349, 123.6241>>//<<3.1635, -0.0349, 123.6241>>//<<12.7229, -0.0346, 132.9350>>
			
			sRaceResults.vCamPos[3] = <<-796.4111, -2523.5613, 23.9621>>
			sRaceResults.vCamRot[3] = <<53.9221, -0.0707, -153.6631>>//<<19.8757, -0.0707, -153.6631>>//<<-2.3601, -0.0691, -153.5230>>
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("DUBSTA3"))
				sRaceResults.vCamPos[4] = <<-796.4529, -2524.6165, 15.0410>>
				sRaceResults.vCamRot[4] = <<-7.1159, 0.0458, -134.7064>>
				sRaceResults.vCamPos[5] = <<-797.3875, -2525.8794, 15.0393>>
				sRaceResults.vCamRot[5] = <<-7.1159, 0.0458, -115.5518>>
			ELIF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[4] = <<-791.3419, -2519.9180, 15.0172>>
				sRaceResults.vCamRot[4] = <<-2.2670, 0.0182, 170.5412>>
				sRaceResults.vCamPos[5] = <<-791.8328, -2519.8376, 15.0171>>
				sRaceResults.vCamRot[5] = <<-2.2670, 0.0182, 171.6637>>
			ELSE
				sRaceResults.vCamPos[4] = <<-796.4111, -2523.5613, 13.9621>>//<<-796.3872, -2523.5405, 13.9626>>
				sRaceResults.vCamRot[4] = <<-2.3601, -0.0691, -153.5230>>//<<-2.5866, -0.0094, -156.8727>>
				sRaceResults.vCamPos[5] = <<-796.5952, -2523.7085, 13.9597>>
				sRaceResults.vCamRot[5] = <<-2.5866, -0.0094, -153.6361>>
			ENDIF
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[6] = <<-795.2563, -2531.8118, 15.4606>>
				sRaceResults.vCamRot[6] = <<-7.3224, -0.0241, -23.3218>>
				sRaceResults.vCamPos[7] = <<-797.5802, -2532.9832, 15.7165>>
				sRaceResults.vCamRot[7] = <<-7.3224, -0.0241, -24.3511>>
			ELSE
				sRaceResults.vCamPos[6] = <<-797.7468, -2530.2664, 14.2931>>
				sRaceResults.vCamRot[6] = <<-7.1993, -0.0094, -44.5229>>
				sRaceResults.vCamPos[7] = <<-796.9835, -2531.1924, 14.3335>>
				sRaceResults.vCamRot[7] = <<-8.9660, -0.0094, -29.9138>>
			ENDIF
			
			sRaceResults.fFOV = 25
		BREAK
		CASE STREET_RACE_05
			sRaceResults.vCamPos[0] = <<750.5599, -1140.9828, 30.1426>>
			sRaceResults.vCamRot[0] = <<0.0483, 0.0013, -107.7410>>
			sRaceResults.vCamPos[1] = <<750.0837, -1141.1554, 30.1422>>
			sRaceResults.vCamRot[1] = <<0.0483, 0.0013, -117.1716>>
			
			sRaceResults.vCamPos[2] = <<750.0837, -1141.1554, 40.1422>>//<<748.6282, -1142.9174, 54.0660>>
			sRaceResults.vCamRot[2] = <<46.0912, 0.0013, -117.2112>>//<<19.0066, 0.0013, -117.1716>>//<<0.0483, 0.0013, -117.1716>>//<<8.5452, 0.0013, -112.2555>>
			
			sRaceResults.vCamPos[3] = <<813.1603, -1161.7493, 38.7039>>
			sRaceResults.vCamRot[3] = <<11.9990, 0.0201, -83.4509>>//<<-2.9712, 0.0059, -83.4627>>
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("DUBSTA3"))
				sRaceResults.vCamPos[4] = <<815.0908, -1162.6770, 29.8120>>
				sRaceResults.vCamRot[4] = <<-13.5952, 0.0560, -61.7062>>
				sRaceResults.vCamPos[5] = <<817.5979, -1163.7069, 29.4702>>
				sRaceResults.vCamRot[5] = <<-13.1626, 0.0560, -11.7796>>
			ELIF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[4] = <<810.1620, -1160.6328, 30.3700>>
				sRaceResults.vCamRot[4] = <<-12.1619, 0.0081, -85.4750>>
				sRaceResults.vCamPos[5] = <<810.3062, -1160.8761, 30.3431>>
				sRaceResults.vCamRot[5] = <<-11.7112, 0.0081, -83.0783>>
			ELSE
				sRaceResults.vCamPos[4] = <<813.1603, -1161.7493, 28.7039>>
				sRaceResults.vCamRot[4] = <<-2.9712, 0.0059, -83.4627>>
				sRaceResults.vCamPos[5] = <<813.6791, -1162.0240, 28.6743>>
				sRaceResults.vCamRot[5] = <<-3.9024, 0.0059, -79.3115>>
			ENDIF
			
			IF mnCar = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("MONSTER"))
				sRaceResults.vCamPos[6] = <<822.7552, -1160.9124, 29.7837>>
				sRaceResults.vCamRot[6] = <<-11.3320, 0.0040, 86.0030>>
				sRaceResults.vCamPos[7] = <<824.2561, -1162.3478, 30.0892>>
				sRaceResults.vCamRot[7] = <<-6.1371, 0.0040, 85.1825>>
			ELSE
				sRaceResults.vCamPos[6] = <<821.5195, -1163.6158, 28.5451>>
				sRaceResults.vCamRot[6] = <<-4.4063, -0.0244, 60.9237>>
				sRaceResults.vCamPos[7] = <<822.8655, -1162.8406, 28.6227>>
				sRaceResults.vCamRot[7] = <<-0.9147, -0.0244, 75.4051>>
			ENDIF
			
			sRaceResults.fFOV = 25
		BREAK
		CASE STREET_RACE_06
			sRaceResults.vCamPos[0] = <<-1051.9481, -1152.6044, 1.7317>>
			sRaceResults.vCamRot[0] = <<4.3072, -0.0727, 97.2603>>
			sRaceResults.vCamPos[1] = <<-1051.8080, -1152.2676, 1.7187>>
			sRaceResults.vCamRot[1] = <<4.3072, -0.0727, 90.1290>>
			
			sRaceResults.vCamPos[2] = <<-1051.8080, -1152.2676, 11.7187>>//<<-1056.8759, -1153.1646, 22.8423>>
			sRaceResults.vCamRot[2] = <<52.1620, -0.0727, 90.1290>>//<<35.3628, -0.0727, 90.1290>>//<<4.3072, -0.0727, 90.1290>>//<<2.7708, -0.0727, 88.7767>>
			
			sRaceResults.vCamPos[3] = <<-1023.0024, -1126.4069, 12.5596>>
			sRaceResults.vCamRot[3] = <<52.1620, -0.0315, 151.2765>>//<<15.9597, -0.0315, 151.2765>>//<<-2.9801, -0.0213, 151.2480>>
			
			sRaceResults.vCamPos[4] = <<-1023.0024, -1126.4069, 2.5596>>
			sRaceResults.vCamRot[4] = <<-2.9801, -0.0213, 151.2480>>
			sRaceResults.vCamPos[5] = <<-1023.4984, -1126.5459, 2.5418>>
			sRaceResults.vCamRot[5] = <<-2.9801, -0.0213, 155.1694>>
			
			sRaceResults.vCamPos[6] = <<-1028.4272, -1130.7130, 2.3506>>
			sRaceResults.vCamRot[6] = <<4.2629, -0.0213, -73.0231>>
			sRaceResults.vCamPos[7] = <<-1028.6172, -1131.7168, 2.6039>>
			sRaceResults.vCamRot[7] = <<-1.0784, -0.0213, -61.3829>>
			
			sRaceResults.fFOV = 25
		BREAK
		CASE SEA_RACE_01
			sRaceResults.vCamPos[0] = <<3006.734131,1170.371826,2.515141>>//<<3008.1960, 1168.5701, 2.4071>>
			sRaceResults.vCamRot[0] = <<4.303476,0.000000,-20.753296>>//<<-8.3052, 0.0000, -20.7533>>
			sRaceResults.vCamPos[1] = <<3006.734131,1170.371826,2.515141>>//<<3008.1960, 1168.5701, 2.4071>>
			sRaceResults.vCamRot[1] = <<4.303476,0.000000,-20.753296>>//<<-8.3052, 0.0000, -20.7533>>
			
			sRaceResults.vCamPos[2] = <<3006.734131,1170.371826,22.515141>>//<<3008.1960, 1168.5701, 12.4071>>//<<3008.2788, 1168.7892, 12.4169>>
			sRaceResults.vCamRot[2] = <<54.303476,0.000000,-20.753296>>//<<36.2222, 0.0000, -20.7533>>//-21.1617>>
			
			sRaceResults.fFOV = 35
		BREAK
		CASE SEA_RACE_02
			sRaceResults.vCamPos[0] = <<3589.2041, 5155.5752, 11.8528>>//<<3595.855957,5136.727539,4.004726>>//<<3593.260742,5138.154785,3.823712>>
			sRaceResults.vCamRot[0] = <<-15.7070, -0.0486, -159.6172>>//<<-0.188566,0.000000,-162.038422>>//<<-12.9053, -0.0000, -164.9235>>
			sRaceResults.vCamPos[1] = <<3589.2041, 5155.5752, 11.8528>>//<<3595.855957,5136.727539,4.004726>>//<<3593.260742,5138.154785,3.823712>>
			sRaceResults.vCamRot[1] = <<-15.7070, -0.0486, -159.6172>>//<<-0.188566,0.000000,-162.038422>>//<<-12.9053, -0.0000, -164.9235>>
			
			sRaceResults.vCamPos[2] = <<3595.855957,5136.727539,24.004726>>//<<3593.260742,5138.154785,13.823712>>
			sRaceResults.vCamRot[2] = <<50.188566,0.000000,-162.038422>>//<<54.1960, 0.0000, -164.9235>>//-167.1748>>
			
			sRaceResults.fFOV = 35
		BREAK
		CASE SEA_RACE_03
			sRaceResults.vCamPos[0] = <<-1466.864136,2631.413574,2.130511>>//<<-1472.567749,2629.427002,1.811960>>
			sRaceResults.vCamRot[0] = <<3.075093,-0.039810,-114.141556>>//<<1.564253,-0.039810,-89.297714>>
			sRaceResults.vCamPos[1] = <<-1466.864136,2631.413574,2.130511>>//<<-1472.567749,2629.427002,1.811960>>
			sRaceResults.vCamRot[1] = <<3.075093,-0.039810,-114.141556>>//<<1.564253,-0.039810,-89.297714>>
			
			sRaceResults.vCamPos[2] = <<-1466.864136,2631.413574,22.130511>>//<<-1472.567749,2629.427002,11.811960>>
			sRaceResults.vCamRot[2] = <<53.075093,-0.039810,-114.141556>>//<<45.9465, -0.039810,-89.297714>>//-0.0371, -81.2768>>
			
			sRaceResults.fFOV = 35
		BREAK
		CASE SEA_RACE_04
			sRaceResults.vCamPos[0] = <<406.017120,-2727.060547,1.925007>>//<<405.807617,-2730.208252,1.762025>>
			sRaceResults.vCamRot[0] = <<2.283322,0.014000,-41.709625>>//<<0.782910,0.014000,-15.434910>>
			sRaceResults.vCamPos[1] = <<406.017120,-2727.060547,1.925007>>//<<405.807617,-2730.208252,1.762025>>
			sRaceResults.vCamRot[1] = <<2.283322,0.014000,-41.709625>>//<<0.782910,0.014000,-15.434910>>
			
			sRaceResults.vCamPos[2] = <<406.017120,-2727.060547,21.925007>>//<<405.807617,-2730.208252,11.762025>>
			sRaceResults.vCamRot[2] = <<52.283322,0.014000,-41.709625>>//<<57.7838, 0.014000,-15.434910>>//0.0126, -39.0680>>
			
			sRaceResults.fFOV = 35
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Creates the camera that moves up to the sky
/// PARAMS:
///    vPlayerVehicle - The player's vehicle
PROC Make_Finish_Cameras(RACE_RESULTS_STRUCT & sRaceResults)
	INT i_index = 0
	REPEAT 7 i_index
		//IF NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[i_index])
			sRaceResults.ciCityRaceFinishCam[i_index] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[i_index], sRaceResults.vCamRot[i_index], 25.374201)
		//ENDIF
	ENDREPEAT
	
	/*IF NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[0]) AND NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[1])
		sRaceResults.ciCityRaceFinishCam[0] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPosOne, sRaceResults.vCamRotOne, 40.0)
		sRaceResults.ciCityRaceFinishCam[1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPosTwo, sRaceResults.vCamRotTwo, 40.0)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCamTwo[0]) AND NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCamTwo[1])
		sRaceResults.ciCityRaceFinishCamTwo[0] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPosTwo, sRaceResults.vCamRotTwo, 40.0)
		sRaceResults.ciCityRaceFinishCamTwo[1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPosThree, sRaceResults.vCamRotThree, 40.0)
	ENDIF*/
ENDPROC

/// PURPOSE:
///    Makes the camera rise up to the sky for the end of race results
/// PARAMS:
///    vPlayerVehicle - the player's vehicle
FUNC BOOL City_Race_Manage_Finish_Camera(VEHICLE_INDEX& vPlayerVehicle, RACE_RESULTS_STRUCT& sRaceResults, RACE_TRACK_ID trackID)
	IF NOT DOES_ENTITY_EXIST(vPlayerVehicle)
		CPRINTLN(DEBUG_MISSION, "City_Race_Manage_Finish_Camera: vPlayerVehicle doesnt exist, exiting")
		RETURN FALSE
	ENDIF 

	SWITCH sRaceResults.eCRCutFinish
		CASE CR_FINISH_INIT
			CPRINTLN(DEBUG_MISSION, "City_Race_Manage_Finish_Camera: inside CR_FINISH_INIT")
			IF (NOT DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[0]))
				CPRINTLN(DEBUG_MISSION, "City_Race_Manage_Finish_Camera: running Make_Finish_Cam")
				SET_FINISH_CAM_POSITIONS(sRaceResults, trackID)
				//Make_Finish_Cameras(sRaceResults)
				sRaceResults.ciCityRaceFinishCam[0] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[0], sRaceResults.vCamRot[0], sRaceResults.fFOV)
				sRaceResults.ciCityRaceFinishCam[1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[1], sRaceResults.vCamRot[1], sRaceResults.fFOV)
			ELIF NOT IS_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[0])
				CPRINTLN(DEBUG_MISSION, "City_Race_Manage_Finish_Camera: Trying to set finish cam active.")
				SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[0], TRUE)
				SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[1],sRaceResults.ciCityRaceFinishCam[0],2000,GRAPH_TYPE_LINEAR)
				ANIMPOSTFX_PLAY("MinigameEndFranklin", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1,"Hit_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[1],"HAND_SHAKE",0.3)
			ELSE
				/*BRING_VEHICLE_TO_HALT(vPlayerVehicle, 100.0, 1)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)*/
				//BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vPlayerVehicle, 70.0)
				SET_ENTITY_CAN_BE_DAMAGED(vPlayerVehicle, FALSE)
				CPRINTLN(DEBUG_MISSION, "City_Race_Manage_Finish_Camera: moving to state: CR_FINISH_CAMERA_PAN")
				sRaceResults.iDisplayResultsTimer = GET_GAME_TIMER() + sRaceResults.CAM_INTERP_TIME
				sRaceResults.iPulseTimer = GET_GAME_TIMER() + sRaceResults.PULSE_TIME
				sRaceResults.eCRCutFinish = CR_FINISH_WAIT_FOR_RESULTS
			ENDIF
		BREAK
		
		CASE CR_FINISH_WAIT_FOR_RESULTS
			//BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vPlayerVehicle, 70.0)
			IF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[1])
				IF NOT IS_CAM_INTERPOLATING(sRaceResults.ciCityRaceFinishCam[1])
					sRaceResults.ciCityRaceFinishCam[2] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[2], sRaceResults.vCamRot[2], sRaceResults.fFOV)
					SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[2],sRaceResults.ciCityRaceFinishCam[1],600,GRAPH_TYPE_SIN_ACCEL_DECEL)
					SET_CAM_MOTION_BLUR_STRENGTH(sRaceResults.ciCityRaceFinishCam[2],1.0)
					//SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[2],"HAND_SHAKE",1.0)
					ANIMPOSTFX_STOP("MinigameEndFranklin")
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
					PLAY_SOUND_FRONTEND(-1,"Short_Transition_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
					DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[0])
					DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[1])
				ENDIF
			//ELIF GET_GAME_TIMER() > sRaceResults.iDisplayResultsTimer
			ELIF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[2])
				IF NOT IS_CAM_INTERPOLATING(sRaceResults.ciCityRaceFinishCam[2])
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					sRaceResults.eCRCutFinish = CR_FINISH_IDLE
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CR_FINISH_IDLE
			// return TRUE so the race script can start displaying the results screen
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC Stop_Finish_Cam(RACE_RESULTS_STRUCT& sRaceResults, BOOL bStartNextCam = TRUE, BOOL bInterp = FALSE)
	IF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[2]) //AND DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[1])
		//IF IS_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[0]) OR IS_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[1])
			CPRINTLN(DEBUG_MISSION, "Cleared away the finish camera")
			sRaceResults.eCRCutFinish = CR_FINISH_INIT
			//RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ANIMPOSTFX_STOP("MinigameTransitionIn")
			PLAY_SOUND_FRONTEND(-1,"Short_Transition_Out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			//ANIMPOSTFX_PLAY("MinigameEndFranklin", 0, FALSE)
			/*SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[0], FALSE)
			SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[1], FALSE)*/
			IF bStartNextCam
				sRaceResults.ciCityRaceFinishCam[3] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[3], sRaceResults.vCamRot[3], 25.374201)
				SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[3], TRUE)
				sRaceResults.ciCityRaceFinishCam[4] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[4], sRaceResults.vCamRot[4], 25.374201)
				SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[4],sRaceResults.ciCityRaceFinishCam[3],600,GRAPH_TYPE_SIN_ACCEL_DECEL)
				SET_CAM_MOTION_BLUR_STRENGTH(sRaceResults.ciCityRaceFinishCam[3],1.0)
				SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[3],"HAND_SHAKE",0.3)
				SET_CAM_MOTION_BLUR_STRENGTH(sRaceResults.ciCityRaceFinishCam[4],1.0)
				SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[4],"HAND_SHAKE",0.3)
			ELSE
				VECTOR vPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,-5,1>>) //GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vRotation = GET_ENTITY_ROTATION(PLAYER_PED_ID())
				sRaceResults.ciCityRaceFinishCam[3] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vPosition + <<0,0,20>>, vRotation + <<55,0,0>>, 50.0)
				SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[3], TRUE)
				//WAIT(0)
				sRaceResults.ciCityRaceFinishCam[4] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vPosition, vRotation, 50.0)
				SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[4],sRaceResults.ciCityRaceFinishCam[3],600,GRAPH_TYPE_SIN_ACCEL_DECEL)
				SET_CAM_MOTION_BLUR_STRENGTH(sRaceResults.ciCityRaceFinishCam[3],1.0)
				SET_CAM_MOTION_BLUR_STRENGTH(sRaceResults.ciCityRaceFinishCam[4],1.0)
				DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[2])
				//WAIT(600)
				//RENDER_SCRIPT_CAMS(FALSE, bInterp, 600)
				//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				bInterp=bInterp
				//RENDER_SCRIPT_CAMS(FALSE, FALSE, 600)
			ENDIF
			ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			//DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[0])
			//DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[1])
			CPRINTLN(DEBUG_MISSION, "Finish camera not active")
		//ENDIF
	ENDIF
ENDPROC

FUNC BOOL City_Race_Manage_Reward_Camera(RACE_RESULTS_STRUCT& sRaceResults)
	SWITCH sRaceResults.eCRCutFinish
		CASE CR_FINISH_INIT
			IF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[4])
				IF NOT IS_CAM_INTERPOLATING(sRaceResults.ciCityRaceFinishCam[4])
					sRaceResults.ciCityRaceFinishCam[5] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[5], sRaceResults.vCamRot[5], 25.374201)
					SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[5],sRaceResults.ciCityRaceFinishCam[4],4500,GRAPH_TYPE_LINEAR)
					SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[5],"HAND_SHAKE",0.3)
					DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[4])
					PLAY_SOUND_FRONTEND(-1,"Short_Transition_Out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
				ENDIF
			ELIF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[5])
				IF NOT IS_CAM_INTERPOLATING(sRaceResults.ciCityRaceFinishCam[5])
					sRaceResults.ciCityRaceFinishCam[6] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[6], sRaceResults.vCamRot[6], 50.0)
					sRaceResults.ciCityRaceFinishCam[7] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sRaceResults.vCamPos[7], sRaceResults.vCamRot[7], 50.0)
					SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[6], TRUE)
					SET_CAM_ACTIVE_WITH_INTERP(sRaceResults.ciCityRaceFinishCam[7],sRaceResults.ciCityRaceFinishCam[6],2000,GRAPH_TYPE_DECEL)
					SHAKE_CAM(sRaceResults.ciCityRaceFinishCam[7],"HAND_SHAKE",0.3)
					DESTROY_CAM(sRaceResults.ciCityRaceFinishCam[5])
				ENDIF
			ELIF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[7])
				IF NOT IS_CAM_INTERPOLATING(sRaceResults.ciCityRaceFinishCam[7])
					RETURN TRUE
				ENDIF
			/*ELSE
				RETURN TRUE*/
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC Stop_Reward_Cam(RACE_RESULTS_STRUCT& sRaceResults)
	IF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[2])
	//IF DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[0]) AND DOES_CAM_EXIST(sRaceResults.ciCityRaceFinishCam[1])
		//IF IS_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[0]) OR IS_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCam[1])
			CPRINTLN(DEBUG_MISSION, "Cleared away finish camera two")
			sRaceResults.eCRCutFinish = CR_FINISH_INIT
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			//RENDER_SCRIPT_CAMS(FALSE, FALSE)
			//SET_CAM_ACTIVE(sRaceResults.ciCityRaceFinishCamTwo, FALSE)
			//DESTROY_CAM(sRaceResults.ciCityRaceFinishCamTwo[0])
			//DESTROY_CAM(sRaceResults.ciCityRaceFinishCamTwo[1])
			CPRINTLN(DEBUG_MISSION, "Finish camera two not active")
		//ENDIF
	ENDIF
ENDPROC

PROC GET_RACE_BEST_TIMES(RACE_TRACK_ID trackID)
	IF NOT bBestTimesReady AND IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		SWITCH trackID
			CASE STREET_RACE_01
				iRaceOnlineID = 0
			BREAK
			CASE STREET_RACE_02
				iRaceOnlineID = 1
			BREAK
			CASE STREET_RACE_04
				iRaceOnlineID = 2
			BREAK
			CASE STREET_RACE_05
				iRaceOnlineID = 3
			BREAK
			CASE STREET_RACE_06
				iRaceOnlineID = 4
			BREAK
			CASE SEA_RACE_01
				iRaceOnlineID = 5
			BREAK
			CASE SEA_RACE_02
				iRaceOnlineID = 6
			BREAK
			CASE SEA_RACE_03
				iRaceOnlineID = 7
			BREAK
			CASE SEA_RACE_04
				iRaceOnlineID = 8
			BREAK
		ENDSWITCH
		
		bBestTimesReady = GET_SP_RACE_PERSONAL_GLOBAL_BEST(iReadStage, iLoadStage, bSuccessful, iRaceOnlineID, iGlobalBest, iPersonalBest)
	ENDIF
ENDPROC

// Sets up the screen layout for the end-of-mission results screen.
PROC INIT_RACE_RESULTS(END_SCREEN_DATASET & esdEndScreen, RACE_TRACK_ID trackID, INT iPlayerPosition, INT iNumRacers, INT iPlayerTime)//, INT iPlayerLapTime)

	PRINTLN("Setting up Race Results screen")
	
	RESET_ENDSCREEN(esdEndScreen)
	
	END_SCREEN_MEDAL_STATUS eMedalStatus = ESMS_NO_MEDAL
	STRING sPositionLabel = ""
	SWITCH iPlayerPosition
		CASE 1
			eMedalStatus = ESMS_GOLD
			sPositionLabel = "SPR_1stpl"
		BREAK
		CASE 2
			eMedalStatus = ESMS_SILVER
			sPositionLabel = "SPR_2ndpl"
		BREAK
		CASE 3
			eMedalStatus = ESMS_BRONZE
			sPositionLabel = "SPR_3rdpl"
		BREAK
		CASE 4
			eMedalStatus = ESMS_NO_MEDAL
			sPositionLabel = "SPR_4thpl"
		BREAK
		CASE 5
			eMedalStatus = ESMS_NO_MEDAL
			sPositionLabel = "SPR_5thpl"
		BREAK
		CASE 6
			eMedalStatus = ESMS_NO_MEDAL
			sPositionLabel = "SPR_6thpl"
		BREAK
		CASE 7
			eMedalStatus = ESMS_NO_MEDAL
			sPositionLabel = "SPR_7thpl"
		BREAK
		CASE 8
			eMedalStatus = ESMS_NO_MEDAL
			sPositionLabel = "SPR_8thpl"
		BREAK
	ENDSWITCH
	
	STRING sRaceName
	SWITCH trackID
		CASE STREET_RACE_01
			sRaceName = "SLOSSANTOS"
		BREAK
		CASE STREET_RACE_02
			sRaceName = "CITYCIRCUIT"
		BREAK
		CASE STREET_RACE_04
			sRaceName = "AIRPORTR"
		BREAK
		CASE STREET_RACE_05
			sRaceName = "FREEWAY"
		BREAK
		CASE STREET_RACE_06
			sRaceName = "VERSPUCCI"
		BREAK
		CASE SEA_RACE_01
			sRaceName = "NORTHCOAST"
		BREAK
		CASE SEA_RACE_02
			sRaceName = "SOUTHCOAST"
		BREAK
		CASE SEA_RACE_03
			sRaceName = "CANYON"
		BREAK
		CASE SEA_RACE_04
			sRaceName = "LOSSANTOS"
		BREAK
	ENDSWITCH
	
	SET_ENDSCREEN_DATASET_HEADER(esdEndScreen, sPositionLabel, sRaceName)
	
	ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdEndScreen, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_TIME", "", iPlayerTime, 0, ESCM_NO_MARK)
	
	esdEndScreen.bHoldOnEnd  = TRUE
	
	IF bBestTimesReady AND IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		/*WHILE NOT GET_SP_RACE_PERSONAL_GLOBAL_BEST(iReadStage, iLoadStage, bSuccessful, iRaceOnlineID, iGlobalBest, iPersonalBest)
			WAIT(0)
		ENDWHILE*/
		
		IF iPersonalBest > iPlayerTime OR iPersonalBest <= 0
			iPersonalBest = iPlayerTime
		ENDIF
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdEndScreen, ESEF_TIME_M_S_MS_WITH_PERIOD, "SPR_BESTTIME", "", iPersonalBest, 0, ESCM_NO_MARK)
		
		IF iGlobalBest > iPlayerTime OR iGlobalBest <= 0
			iGlobalBest = iPlayerTime
		ENDIF
		ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esdEndScreen, ESEF_TIME_M_S_MS_WITH_PERIOD, "LOB_SPLIT_0", "", iGlobalBest, 0, ESCM_NO_MARK)
	ENDIF
		
	IF iPlayerPosition <= 3
		SET_ENDSCREEN_COMPLETION_LINE_STATE(esdEndScreen, TRUE, "SPR_RESULT", iPlayerPosition, iNumRacers, ESC_FRACTION_COMPLETION, eMedalStatus)
	ELSE
		SET_ENDSCREEN_COMPLETION_LINE_STATE(esdEndScreen, FALSE, "SPR_RESULT", iPlayerPosition, iNumRacers, ESC_FRACTION_COMPLETION, eMedalStatus)
	ENDIF
	//ENDSCREEN_PREPARE(esdEndScreen)
	
ENDPROC


// Show the results screen, and monitor the player's input.
PROC DRAW_RACE_RESULTS(END_SCREEN_DATASET & esdEndScreen)
	RENDER_ENDSCREEN(esdEndScreen)
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_UP()
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_DOWN()
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_SELECT()
	PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC HT_MENU_SFX_PLAY_NAV_BACK()
	PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_MINI_GAME_SOUNDSET")
ENDPROC

PROC INIT_RACE_LEADERBOARD(RACE_TRACK_ID trackID, INT iPlayerPosition, INT iPlayerTime, INT iPlayerLapTime, VEHICLE_INDEX viPlayer)
	IF IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		TEXT_LABEL_31 categoryNames[3]
		categoryNames[0] = "GameType"
		categoryNames[1] = "Location"
		categoryNames[2] = "Type"
		TEXT_LABEL_23 uniqueIdentifiers[3]
		uniqueIdentifiers[0] = "SP"
		SWITCH trackID
			CASE STREET_RACE_01
				uniqueIdentifiers[1] = "MGCR_1"
				uniqueIdentifiers[2] = "StreetRace"
			BREAK
			CASE STREET_RACE_02
				uniqueIdentifiers[1] = "MGCR_2"
				uniqueIdentifiers[2] = "StreetRace"
			BREAK
			CASE STREET_RACE_04
				uniqueIdentifiers[1] = "MGCR_4"
				uniqueIdentifiers[2] = "StreetRace"
			BREAK
			CASE STREET_RACE_05
				uniqueIdentifiers[1] = "MGCR_5"
				uniqueIdentifiers[2] = "StreetRace"
			BREAK
			CASE STREET_RACE_06
				uniqueIdentifiers[1] = "MGCR_6"
				uniqueIdentifiers[2] = "StreetRace"
			BREAK
			CASE SEA_RACE_01
				uniqueIdentifiers[1] = "MGSR_1"
				uniqueIdentifiers[2] = "SeaRace"
			BREAK
			CASE SEA_RACE_02
				uniqueIdentifiers[1] = "MGSR_2"
				uniqueIdentifiers[2] = "SeaRace"
			BREAK
			CASE SEA_RACE_03
				uniqueIdentifiers[1] = "MGSR_3"
				uniqueIdentifiers[2] = "SeaRace"
			BREAK
			CASE SEA_RACE_04
				uniqueIdentifiers[1] = "MGSR_4"
				uniqueIdentifiers[2] = "SeaRace"
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Track ID was invalid")
			BREAK
		ENDSWITCH
		
		SC_LEADERBOARD_CACHE_CLEAR_ALL()
		
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_RACES, uniqueIdentifiers, categoryNames, 3, -1, TRUE)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, -iPlayerTime, 0)				//(long)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_SCORE, -iPlayerTime, 0)				//(long)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME, iPlayerLapTime, 0)		//(long)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_BEST_TIME, iPlayerLapTime, 0)		//(long)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME, iPlayerTime, 0)			//(long)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_TOTAL_TIME, iPlayerTime, 0)			//(long)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID, ENUM_TO_INT(GET_ENTITY_MODEL(viPlayer)), 0)					//(int)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_VEHICLE_ID, ENUM_TO_INT(GET_ENTITY_MODEL(viPlayer)), 0)					//(int)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_COLOR, 0, 0) 				//(int)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_VEHICLE_COLOR, 0, 0) 				//(int)
			//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES, 1, 0)					//(int)
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(ENUM_TO_INT(LEADERBOARD_MINI_GAMES_RACES), LB_INPUT_COL_NUM_MATCHES, 1, 0)					//(int)
			INT iGold = 0
			INT iSilver = 0
			INT iBronze = 0
			INT iMedal = 0
			IF iPlayerPosition = 1
				iGold = 1
				iMedal = 3
			ELIF iPlayerPosition = 2
				iSilver = 1
				iMedal = 2
			ELIF iPlayerPosition = 3
				iBronze = 1
				iMedal = 1
			ENDIF
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_GOLD_MEDALS, iGold, 0)				//(int)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SILVER_MEDALS, iSilver, 0)			//(int)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BRONZE_MEDALS, iBronze, 0)			//(int)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_MEDAL, iMedal, 0)					//(int)
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_RACE_LEADERBOARD(RACE_TRACK_ID trackID)
	IF IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES() AND bAllowLeaderboard
		INT iSubType
		INT iLaps
		SWITCH trackID
			CASE STREET_RACE_01
				iSubType = 0
				iLaps = 2
			BREAK
			CASE STREET_RACE_02
				iSubType = 1
				iLaps = 1
			BREAK
			CASE STREET_RACE_04
				iSubType = 2
				iLaps = 2
			BREAK
			CASE STREET_RACE_05
				iSubType = 3
				iLaps = 2
			BREAK
			CASE STREET_RACE_06
				iSubType = 4
				iLaps = 2
			BREAK
			CASE SEA_RACE_01
				iSubType = 5
				iLaps = 0
			BREAK
			CASE SEA_RACE_02
				iSubType = 6
				iLaps = 0
			BREAK
			CASE SEA_RACE_03
				iSubType = 7
				iLaps = 0
			BREAK
			CASE SEA_RACE_04
				iSubType = 8
				iLaps = 0
			BREAK
			DEFAULT
				SCRIPT_ASSERT("Track ID was invalid")
			BREAK
		ENDSWITCH
		
		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(sclbControl, FMMC_TYPE_SP_STREET_RACE, "", "", iSubType, iLaps)
		//LOAD_SOCIAL_CLUB_LEADERBOARD_DATA(sclbControl)
		//SET_SC_LEADERBOARD_DISPLAY_TYPE(uiLeaderboard)
		//SET_SC_LEADERBOARD_TITLE(uiLeaderboard, sTitle)
		
		bAllowLeaderboard = FALSE
	ENDIF
ENDPROC

PROC DRAW_RACE_LEADERBOARD(SCALEFORM_INDEX& uiLeaderboard[])//, SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	IF IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		//DISPLAY_SC_LEADERBOARD_UI(uiLeaderboard)
		DRAW_SOCIAL_CLUB_LEADERBOARD(sclbControl, uiLeaderboard)
	ENDIF
ENDPROC

PROC CLEANUP_RACE_LEADERBOARD(END_SCREEN_DATASET & esdEndScreen, SCALEFORM_INDEX &uiLeaderboard)//, SC_LEADERBOARD_CONTROL_STRUCT& scLB_control)
	IF IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		CLEANUP_SC_LEADERBOARD_UI(uiLeaderboard)
		CLEANUP_SOCIAL_CLUB_LEADERBOARD(sclbControl)
	ENDIF
	//RESET_ENDSCREEN(esdEndScreen)
	ENDSCREEN_SHUTDOWN(esdEndScreen)
ENDPROC

PROC INIT_RESULT_SCREEN_BUTTONS(RACE_RESULTS_STRUCT& sRaceResults)
	CPRINTLN(DEBUG_MISSION, "INIT_RESULT_SCREEN_BUTTONS ")
	/*INIT_SIMPLE_USE_CONTEXT(sRaceResults.ucInstructions, FALSE, FALSE, TRUE, TRUE)
	ADD_SIMPLE_USE_CONTEXT_INPUT(sRaceResults.ucInstructions, "CMRC_CONT",			FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(sRaceResults.ucInstructions)*/
	
	INIT_SIMPLE_USE_CONTEXT(sRaceResults.ucInstructions, FALSE, FALSE, TRUE, TRUE)
	ADD_SIMPLE_USE_CONTEXT_INPUT(sRaceResults.ucInstructions, "SPR_CONT2",			FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	ADD_SIMPLE_USE_CONTEXT_INPUT(sRaceResults.ucInstructions, "SPR_UI_RETRY",		FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
	IF IS_PLAYER_ONLINE() AND NETWORK_HAVE_ONLINE_PRIVILEGES()
		ADD_SIMPLE_USE_CONTEXT_INPUT(sRaceResults.ucInstructions, "HUD_INPUT68",	FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
	ENDIF
	SET_SIMPLE_USE_CONTEXT_FULLSCREEN(sRaceResults.ucInstructions)
	SET_SIMPLE_USE_CONTEXT_MINIGAME_ATTACHED(sRaceResults.ucInstructions)
ENDPROC

PROC DRAW_RESULT_SCREEN_INSTRUCTIONS(RACE_RESULTS_STRUCT& sRaceResults)
	UPDATE_SIMPLE_USE_CONTEXT(sRaceResults.ucInstructions)
ENDPROC
