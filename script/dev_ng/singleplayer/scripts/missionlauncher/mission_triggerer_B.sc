

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "mission_triggerer_core.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Mission_triggerer.sch
//		AUTHOR			:	Ak, BenR
//		DESCRIPTION		:	Parses the g_TriggerableMissions list and checks for activated
//							triggers, on finding an active trigger an attempt will be made to
//							obtain mission candidate permissions for the trigger, mission 
//							script launching should be handled by the relevant scripts.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
      

// For all registered triggers check if they can trigger this frame.
// If they can try and secure a candidate ID for them.
// If a candidate ID is secured then set the mission to trigger.
SCRIPT
	CPRINTLN(DEBUG_TRIGGER, "Mission Triggerer B started.")

	MISSION_TRIGGERER_LOCAL_VARS sLocalVars
	sLocalVars.iNoFramesMissionRunning = 0
	sLocalVars.iLockInTimer = -1
	sLocalVars.fpBindTriggerScene = &SETUP_SET_B_TRIGGER_SCENE
	
	MISSION_TRIGGERER_INIT()
	
	WHILE TRUE
		IF NOT sLocalVars.bCallbackSetup
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND NOT IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
				CPRINTLN(DEBUG_TRIGGER, "Setting up new FORCE_CLEANUP for mission trigger B.")
				sLocalVars.bCallbackSetup = TRUE
				IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
					IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED
						CPRINTLN(DEBUG_TRIGGER, "mission_triggerer_B running cleanup for death/arrest.")
						CLEANUP_ACTIVE_TRIGGER_SCENE(sLocalVars)
						sLocalVars.bCallbackSetup = FALSE
					ELSE
						CPRINTLN(DEBUG_TRIGGER, "mission_triggerer_B has been forced to cleanup.")
						MISSION_TRIGGERER_FULL_CLEANUP(sLocalVars)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//This version of the mission triggerer shouldn't be running before Trevor 1 or past FBI4
		IF (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1))
		OR GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4)
			CPRINTLN(DEBUG_TRIGGER, "mission_triggerer_B cleaning up as it is running at the wrong time in the flow.")
			MISSION_TRIGGERER_FULL_CLEANUP(sLocalVars)
		ENDIF
		
		MISSION_TRIGGERER_MAIN_UPDATE(sLocalVars)
		WAIT(0)
	ENDWHILE
ENDSCRIPT
