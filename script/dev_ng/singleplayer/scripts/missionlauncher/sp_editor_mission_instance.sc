//
//
//// *****************************************************************************************
//// *****************************************************************************************
//// *****************************************************************************************
////
////		REPLACED WITH SPMC_PRELOADER AND SPMC_INSTANCER PENDING REMOVAL
////
//// *****************************************************************************************
//// *****************************************************************************************
//// ****************************************************************************************
//
//USING "rage_builtins.sch"
//USING "globals.sch"
//USING "fmmc_header.sch"   
//USING "fmmc_cloud_loader.sch"
//
//PROC CLEANUP(BLIP_INDEX &bp[],BLIP_INDEX &bv[],BLIP_INDEX &bo[])//,
//			 //OBJECT_INDEX &oi[],VEHICLE_INDEX &vi[],PED_INDEX &pi[])
//	PRINTSTRING("sp_editor_mission_instance: CLEANUP triggered\n")
//	SET_FMMC_MODELS_AS_NOT_NEEDED()
//
//	//TODO go through the arrays and delete anything that exsists
//	INT i = 0
//	REPEAT FMMC_MAX_PEDS i
//		IF DOES_BLIP_EXIST(bp[i])
//			REMOVE_BLIP(bp[i])
//		ENDIF
//	ENDREPEAT
//	REPEAT FMMC_MAX_VEHICLES i
//		IF DOES_BLIP_EXIST(bv[i])
//			REMOVE_BLIP(bv[i])
//		ENDIF
//	ENDREPEAT
//	REPEAT FMMC_MAX_NUM_OBJECTS i
//		IF DOES_BLIP_EXIST(bo[i])
//			REMOVE_BLIP(bo[i])
//		ENDIF
//	ENDREPEAT
//	
//	
//	IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
//		WHILE NOT IS_SCREEN_FADED_OUT()
//			WAIT(0)
//		ENDWHILE
//		DO_SCREEN_FADE_IN(1000)
//		WHILE NOT IS_SCREEN_FADED_IN()
//			WAIT(0)
//		ENDWHILE
//	ENDIF
//	
//	TERMINATE_THIS_THREAD()
//  
//ENDPROC
//	
//	
//
//
//STRUCT STRUCT_STAGE_RULE_TABLE
//
//	//overall rules
//	//start point
//	
//	//end point
//
//	
//	//specific conditions
//		//entity kill list
//		INT iKillRemaining
//		INT iToKill
//		ENTITY_INDEX killList[FMMC_MAX_PEDS + FMMC_MAX_VEHICLES]
//		//entity collect list
//		INT iCollectRemaining
//		INT iToCollect
//		
//		OBJECT_INDEX collectList[FMMC_MAX_NUM_OBJECTS]
//		
//		
//	
//	//ordered checkpoint list
//	
//
//ENDSTRUCT
//
//
//	
//PROC REBUILD_STAGE_TABLE(STRUCT_STAGE_RULE_TABLE &rbld,INT stage,OBJECT_INDEX &objs[], VEHICLE_INDEX &vehs[],PED_INDEX &peds[])
//	PRINTSTRING("REBUILD_STAGE_TABLE: Starting for stage ")
//	PRINTINT(stage)
//	PRINTNL()
//	
//	//uses current data in g_FMMC_STRUCT
//	//clear
//	rbld.iToKill = 0
//	rbld.iToCollect = 0
//	
//	INT i = 0
//	//count the kills in peds and vehicles
//	REPEAT g_FMMC_STRUCT.iNumberOfPeds i
//		PRINTSTRING("Checking ped ")
//		PRINTINT(i)
//		//PRINTSTRING(" stage ")
//		//PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage)
//		PRINTSTRING(" ruletype ")
//		PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[0])
//		PRINTNL()
//		//IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = stage
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[0] = ciRULE_KILL_ENTITY
//				rbld.killList[rbld.iToKill] = GET_ENTITY_FROM_PED_OR_VEHICLE(peds[i])
//				rbld.iToKill++
//			ENDIF
//		//ENDIF
//	ENDREPEAT
//	i = 0
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
//		PRINTSTRING("Checking vehicle ")
//		PRINTINT(i)
//		//PRINTSTRING(" stage ")
//		//PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage)
//		PRINTSTRING(" ruletype ")
//		PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[0])
//		PRINTNL()
//		//IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage = stage
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[0] = ciRULE_KILL_ENTITY
//				rbld.killList[rbld.iToKill] = GET_ENTITY_FROM_PED_OR_VEHICLE(vehs[i])
//				rbld.iToKill++
//			ENDIF
//		//ENDIF
//	ENDREPEAT
//
//	rbld.iKillRemaining = rbld.iToKill
//	PRINTSTRING("Found ")
//	PRINTINT(rbld.iKillRemaining)
//	PRINTSTRING(" kill required peds/vehicles in this stage, out of total veh/peds/obj : ")
//	PRINTINT(g_FMMC_STRUCT.iNumberOfPeds + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
//	PRINTNL()
//	
//	//count the collects in objects
//	REPEAT g_FMMC_STRUCT.iNumberOfObjects i
//		PRINTSTRING("Checking object ")
//		PRINTINT(i)
//		//PRINTSTRING(" stage ")
//		//PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage)
//		PRINTSTRING(" ruletype ")
//		PRINTINT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[0])
//		PRINTNL()
//		//IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = stage
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[0] = ciRULE_COLLECT_ENTITY
//				rbld.collectList[rbld.iToCollect] = objs[i]
//				rbld.iToCollect++
//			ENDIF
//		//ENDIF
//	ENDREPEAT
//	
//	rbld.iCollectRemaining = rbld.iToCollect
//	PRINTSTRING("Found ")
//	PRINTINT(rbld.iCollectRemaining)
//	PRINTSTRING(" collect required objects in this stage, out of total objects:")
//	PRINTINT(g_FMMC_STRUCT.iNumberOfObjects)
//	PRINTNL()
//	
//	PRINTSTRING("REBUILD_STAGE_TABLE: Complete\n")
//ENDPROC 
//	
//	
////SP fmmc instance
//SCRIPT 
//	BLIP_INDEX biPedBlip[FMMC_MAX_PEDS]
//	BLIP_INDEX biVehicleBlip[FMMC_MAX_VEHICLES]
//	BLIP_INDEX biObject[FMMC_MAX_NUM_OBJECTS]
//	
//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sp_editor_mission_instance")) > 1
//		SCRIPT_ASSERT("sp_editor_mission_instance: Limited to one script running at a time, bailing")
//		CLEANUP(biPedBlip,biVehicleBlip,biObject)//,oiObject,viVehcile,piPed)
//	ENDIF
//
//	PRINTSTRING("sp_editor_mission_instance: started\n")
//	//check for multiplayer, bail if so
//	IF NETWORK_IS_IN_SESSION() OR IS_CURRENTLY_ON_MISSION_TO_TYPE()
//		TERMINATE_THIS_THREAD()
//	ENDIF
//	//instanced values
//
//	
//	
//	OBJECT_INDEX oiObject[FMMC_MAX_NUM_OBJECTS]
//	VEHICLE_INDEX viVehicle[FMMC_MAX_VEHICLES]
//	PED_INDEX piPed[FMMC_MAX_PEDS]
//
//	
//	STRUCT_DATA_FILE df
//	STRUCT_REL_GROUP_HASH sRGH
//	
//	//Note, these missions can support respawning and have thier own fail conditions
//	//Thus deaths and arrests don't count as force cleanup
//	 //removed since load caused force cleanup before, bug bobby about this
//	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
//		SCRIPT_ASSERT("sp_editor_mission_instance: HAS_FORCE_CLEANUP_OCCURRED triggered")
//		CLEANUP(biPedBlip,biVehicleBlip,biObject)//,oiObject,viVehcile,piPed)
//	ENDIF
//	
//
//	
//	
//	
//	//fade for now
//	DO_SCREEN_FADE_OUT(1000)
//	WHILE NOT IS_SCREEN_FADED_OUT()
//		PRINTSTRING("sp_editor_mission_instance: Fading out\n")
//		WAIT(100)
//	ENDWHILE
//	
//	//TODO attempt to load the data
//	WHILE NOT LOAD_DATA(df,GET_PLAYER_INDEX())
//		PRINTSTRING("sp_editor_mission_instance: LOAD_DATA\n")
//		WAIT(0)
//	ENDWHILE
//	
//	IF NOT df.bSuccess
//		//datafile load failed
//		PRINTSTRING("sp_editor_mission_instance: Datafile load failed\n")
//		CLEANUP(biPedBlip,biVehicleBlip,biObject)//,oiObject,viVehcile,piPed)
//	ENDIF
//	
//	IF g_FMMC_STRUCT.iNumParticipants != 1
//		/*
//		PRINTSTRING("sp_editor_mission_instance: Loaded mission is not singleplayer, it is for ")
//		PRINTINT(g_FMMC_STRUCT.iNumParticipants)
//		PRINTSTRING(" players.\n")
//		SCRIPT_ASSERT("sp_editor_mission_instance: Loaded mission is not singleplayer. Terminating")
//		CLEANUP(biPedBlip,biVehicleBlip,biObject)
//		*/
//		
//		SCRIPT_ASSERT("sp_editor_mission_instance: Warning! Loaded mission is not singleplayer. Strange things may happen.")
//	ENDIF
//	
//	
//	WHILE NOT REQUEST_LOAD_FMMC_MODELS()
//	PRINTSTRING("sp_editor_mission_instance: REQUEST_LOAD_FMMC_MODELS()\n")
//		WAIT(0)
//	ENDWHILE
//		
//			
//
//
//	//TODO attempt to fill the arrays
//
//	
//	//INT iMissionStage = 0
//	
//	/*
//	WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
//		PRINTSTRING("sp_editor_mission_instance: Streaming area in\n")
//		WAIT(100)
//	ENDWHILE
//	*/
//	CREATE_FMMC_OBJECTS_SP(oiObject)
//	CREATE_FMMC_VEHICLES_SP(viVehicle)
//	CREATE_FMMC_PEDS_SP(piPed, sRGH)
//	
//		
//	
//	//BLIP_FMMC_PED(ENTITY_INDEX eiPassed, BLIP_INDEX &biPassed, INT iPedNumber, INT iStage= 0)
//	//BLIP_FMMC_VEH(ENTITY_INDEX eiPassed, BLIP_INDEX &biPassed, INT iVehNumber, INT iStage = 0)
//	//BLIP_FMMC_OBJ(ENTITY_INDEX eiPassed, BLIP_INDEX &biPassed, INT iObjNumber, INT iStage = 0)
//		
//	//move player to start location
//	
//	
//	LOAD_SCENE(g_FMMC_STRUCT.vStartPos)
//	REQUEST_COLLISION_AT_COORD(g_FMMC_STRUCT.vStartPos)
//	WAIT(100)
//	
//	PRINTLN("sp_editor_mission_instance: Moving player to ",g_FMMC_STRUCT.vStartPos.x , " ", g_FMMC_STRUCT.vStartPos.y, " ", g_FMMC_STRUCT.vStartPos.z , "\n")
//	
//	
//	IS_PLAYER_DEAD(GET_PLAYER_INDEX())//screw you death check
//	IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
//	SET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()),g_FMMC_STRUCT.vStartPos)
//	
//	
//	DO_SCREEN_FADE_IN(1000)
//	WHILE NOT IS_SCREEN_FADED_IN()
//		PRINTSTRING("sp_editor_mission_instance: Fading in\n")
//		WAIT(100)
//	ENDWHILE
//		
//	//tracked mission loop
//	PRINTSTRING("sp_editor_mission_instance: Entering tracking loop\n")
//	BOOL done = FALSE
//	INT stage = 0
//	INT lastStage = -1
//	INT i = 0
//	
//	//Note end vector in vCheckPoint[0] //uses fStartRadius
//	
//	//Make stage rule table if stage changes
//	
//	STRUCT_STAGE_RULE_TABLE srt
//	
//	WHILE NOT done
//		
//		IF stage != lastStage
//			//rebuild stage rule table
//			lastStage = stage
//			
//			REBUILD_STAGE_TABLE(srt,stage,oiObject,viVehicle,piPed)
//			
//			
//		ENDIF
//		
//
// 		i = 0
//		REPEAT  srt.iToKill i
//		
//		
//		ENDREPEAT
//		
//		
// 		i = 0
//		REPEAT  srt.iToCollect i
//		
//		
//		ENDREPEAT
//
//		
//		//check srt for contents being met
//		IF srt.iCollectRemaining = 0
//			
//		ENDIF
//		
//		IF srt.iKillRemaining = 0
//			
//		ENDIF
//		
//		
//		//check overall rules
//		//SWITCH g_FMMC_STRUCT.iMissionType
//		//ENDSWITCH
//		
//		//TODO check mission stage conditions
//		//g_FMMC_STRUCT
//		/*
//		i = 0
//		REPEAT g_FMMC_STRUCT.iNumberOfPeds i
//			//check ped rules for current stage
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iStage = stage
//
//				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule	
//					CASE ciRULE_KILL_ENTITY
//						
//					BREAK
//					CASE ciRULE_COLLECT_ENTITY
//						
//					BREAK
//				ENDSWITCH
//	
//			ENDIF
//			
//			//check object rules for current stage
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iStage = stage
//
//				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule	
//					CASE ciRULE_KILL_ENTITY
//						
//					BREAK
//					CASE ciRULE_COLLECT_ENTITY
//						
//					BREAK
//				ENDSWITCH
//	
//			ENDIF
//			
//			
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iStage = stage
//			
//				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule	
//					CASE ciRULE_KILL_ENTITY
//						
//					BREAK
//					CASE ciRULE_COLLECT_ENTITY
//						
//					BREAK
//				ENDSWITCH
//				
//			ENDIF
//			*/
//			/*//Weapons have no rules apparently
//			IF g_FMMC_STRUCT.sPlacedWeapon[i].iStage
//			
//				SWITCH g_FMMC_STRUCT.sPlacedWeapon[i].iRule	
//					CASE ciRULE_KILL_ENTITY
//						
//					BREAK
//					CASE ciRULE_COLLECT_ENTITY
//						
//					BREAK
//				ENDSWITCH
//				
//			ENDIF
//			
//		ENDREPEAT
//		*/
//		//check other state conditions
//		
//	
//		WAIT(0)
//
//	ENDWHILE
//	PRINTSTRING("sp_editor_mission_instance: Leaving tracking loop\n")
//
//	
//	CLEANUP(biPedBlip,biVehicleBlip,biObject)//,oiObject,viVehcile,piPed)
//	
//ENDSCRIPT
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

SCRIPT
ENDSCRIPT



