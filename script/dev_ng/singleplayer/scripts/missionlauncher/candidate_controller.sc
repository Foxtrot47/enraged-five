

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "mission_control_public.sch"
USING "candidate_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	candidate_controller.sc
//		AUTHOR			:	Ak
//		DESCRIPTION		:	
//
//
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

BOOL bNetworkJoinQueuesBlocked = FALSE

PROC Script_Cleanup()

	CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Cleaning up current running candidate for switch to MP.")
	Mission_Over(g_iCurrentlyRunningCandidateID)
	
	g_bCandidateSystemMidProcessing = FALSE
	TERMINATE_THIS_THREAD()
	
ENDPROC


SCRIPT
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Candidate_controller.sc has been forced to cleanup (SP to MP)")

		Script_Cleanup()
	ENDIF
	/////////////////////////////////
	//are there any candidates?
	//	//are there more candidates this frame than last?
	//		//if so then check for overflow and keep waiting in case more turn up
	//		//otherwise do something about it
	//			//consider triggering type
	//			//mark heighest priority candidate in list as chosen, and reset the list
	//			//set mission flag to true! - let mission launcher do it's thing and go back to waiting for legit candidates
	//no candidates wait
	/////////////////////////////////
	//
	/*
		BOOL g_PrivateCandidateSystemMissionIndicator
		INT g_nextMissionCandidateID // needs saving
		INT g_iToLaunchMissionId //don't save

		m_structMissionCandidate g_listCandidates[(MAX_MISSION_CANDIDATES+1)]

		MISSION_CANDIDATE_MISSION_TYPE_ENUM g_OnMissionState = MISSION_TYPE_OFF_MISSION
		
		
		STRUCT m_structMissionCandidate

			m_enumMissionCandidateTypeID type
			MISSION_CANDIDATE_MISSION_TYPE_ENUM missionType

		ENDSTRUCT
	*/
	
	INT iCandidatesLastFrame = g_iCurrentCandidatesInList
	MISSION_CANDIDATE_MISSION_TYPE_ENUM MissionStateLastTick = MISSION_TYPE_OFF_MISSION
	BOOL invalidations[MAX_MISSION_CANDIDATES]
	
	WHILE TRUE
	
		IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
			
			//check for current candidate termination
			//INT g_iCurrentlyRunningCandidateID = -2
			//THREADID g_CurrentlyRunningCandidateThread = NULL	
			IF g_CurrentlyRunningCandidateThread != NULL
				IF g_iCurrentlyRunningCandidateID != NO_CANDIDATE_ID
					IF NOT IS_THREAD_ACTIVE(g_CurrentlyRunningCandidateThread)
						CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Bound thread detected as dead, terminating current mission candidate...")
						Mission_Over(g_iCurrentlyRunningCandidateID)
					ENDIF 
				ENDIF
			ENDIF
		ENDIF
	

		IF g_iCurrentCandidatesInList > 0
			CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Candidate controller processing...")
			g_bCandidateSystemMidProcessing = TRUE
			//processing needs to happen
			IF g_iCurrentCandidatesInList = iCandidatesLastFrame
				//candidate entry has settled down, do adjudication

				
				INT arrayIndexToLaunch = 0
				
				IF g_iCurrentCandidatesInList > 1
					//first purge those that could not launch due to category
					//then from those resolve the priority
					
					
					INT i = 0
					REPEAT (g_iCurrentCandidatesInList) i
						 IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE( g_listCandidates[i].missionType )
							invalidations[i] = FALSE
						 ELSE
						 	invalidations[i] = TRUE
						 ENDIF
						 
						 IF g_listCandidates[i].type = MCTID_RETRACTED
						 	invalidations[i] = TRUE
						 ENDIF
					ENDREPEAT
					
					/*
					INT j = 0
					REPEAT (g_iCurrentCandidatesInList) i
						invalidations[i] = FALSE
						REPEAT (g_iCurrentCandidatesInList) j	
							IF g_listCandidates[i].missionType != g_listCandidates[j].missionType
							IF NOT CAN_MISSION_TYPE_START_AGAINST_TYPE(g_listCandidates[i].missionType, g_listCandidates[j].missionType)
								//i cannot start against j
								
							ENDIF
							ENDIF
						ENDREPEAT	
					ENDREPEAT
					*/

					INT considered = 0
					i = 0
					REPEAT (g_iCurrentCandidatesInList-1) i
						//so if the type enum is smaller then the current considered make it the new considered
						IF(!invalidations[i+1])
							IF ENUM_TO_INT(g_listCandidates[(i+1)].type) < ENUM_TO_INT(g_listCandidates[considered].type) 
								//new to consider
								considered = (i+1)
							ENDIF
						ENDIF
					ENDREPEAT
					
					arrayIndexToLaunch = considered
					
					//finally see if arrayIndexToLaunch can even launch
					IF g_listCandidates[arrayIndexToLaunch].missionType = MISSION_TYPE_OFF_MISSION
						arrayIndexToLaunch = -1
					ENDIF
					
					IF arrayIndexToLaunch != -1
						IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(g_listCandidates[arrayIndexToLaunch].missionType)
							arrayIndexToLaunch = -1
						ENDIF
					ENDIF
					
				ENDIF
				

				
				//TODO, check for state overwriting
				IF arrayIndexToLaunch > -1
					g_iCurrentlyRunningCandidateID = g_listCandidates[arrayIndexToLaunch].mcid
					g_CurrentlyRunningCandidateThread = g_listCandidates[arrayIndexToLaunch].terminationThread
					g_OnMissionState = g_listCandidates[arrayIndexToLaunch].missionType
					
					
					
					//candidate resolution complete, purge the list
					g_iCurrentCandidatesInList = 0
					CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Candidate controller set mission flag for candidate ID ", g_listCandidates[arrayIndexToLaunch].mcid)
					g_bCandidateSystemMidProcessing = FALSE
				ELSE 
					g_iCurrentCandidatesInList = 0
					CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Candidate controller launch aborted due to no missions able to launch in current state")
					g_bCandidateSystemMidProcessing = FALSE
				ENDIF
			ENDIF

		ENDIF

		IF MissionStateLastTick != g_OnMissionState
		
			IF MissionStateLastTick = MISSION_TYPE_OFF_MISSION
				PRINTLN(" <stockstate>Detected going on mission, lodging finance values")
				COPY_BANK_TRANSLOGS(FALSE) // set start of mission checkpoint
			ENDIF
			IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
				PRINTLN(" <stockstate>Detected coming off mission, lodging finance values")
				COPY_BANK_TRANSLOGS()
			ENDIF
		
			g_bCandidateSystemMidProcessing = FALSE //it changed state so it must have finished processing
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Candidate controller changed state from ", GET_MISSION_TYPE_DEBUG_STRING(MissionStateLastTick), " to ", GET_MISSION_TYPE_DEBUG_STRING(g_OnMissionState), ".")
			#ENDIF
			
			g_bBlipSystemMissionStateChange = TRUE
			
			//if going off random event
			/*//re enable when random events are using the system correctly
			IF MissionStateLastTick = MISSION_TYPE_RANDOM_EVENT
				FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
			ENDIF
			*/
			
			IF g_OnMissionState != MISSION_TYPE_RANDOM_EVENT
			AND g_OnMissionState != MISSION_TYPE_OFF_MISSION
				CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Force cleaning up Random Events as we go on mission.")
				FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
			ENDIF
			
//			IF (MissionStateLastTick = MISSION_TYPE_MINIGAME OR MissionStateLastTick = MISSION_TYPE_MINIGAME_FRIENDS)
//			AND CAN_MISSION_TYPE_START_AGAINST_TYPE(MISSION_TYPE_STORY, g_OnMissionState)
//				CPRINTLN(DEBUG_TRIGGER, "<CANDIDATE> Setting leave area flags for all blipped story missions as Story Missions come back online.")
//				Set_Leave_Area_Flag_For_All_Blipped_Missions()
//			ENDIF
			
			IF g_OnMissionState = MISSION_TYPE_OFF_MISSION 
			OR g_OnMissionState = MISSION_TYPE_FRIEND_ACTIVITY
				PAUSE_FLOW_HELP_QUEUE(FALSE)
			ELSE
				PAUSE_FLOW_HELP_QUEUE(TRUE)
			ENDIF
			
			//Toggle on and off allowing of network session queues to pull the player 
			//into MP games while on various mission types. #2044395
			IF Should_Network_Queues_Be_Blocked_For_Mission_Type(g_OnMissionState)
				IF NOT bNetworkJoinQueuesBlocked
					CPRINTLN(DEBUG_FLOW, "Candidate controller is blocking joining network games from queues as we go on mission.")
					NETWORK_BLOCK_JOIN_QUEUE_INVITES(TRUE)
					bNetworkJoinQueuesBlocked = TRUE
				ENDIF
			ELSE
				IF bNetworkJoinQueuesBlocked
					CPRINTLN(DEBUG_FLOW, "Candidate controller is allowing joining network games from queues as we go off mission.")
					NETWORK_BLOCK_JOIN_QUEUE_INVITES(FALSE)
					bNetworkJoinQueuesBlocked = FALSE
				ENDIF
			ENDIF
			
		ENDIF
		
		MissionStateLastTick = g_OnMissionState
		iCandidatesLastFrame = g_iCurrentCandidatesInList
		
		//For beast secret. Time of day query moved
		//to separate thread.
		g_todCurrent = GET_CURRENT_TIMEOFDAY()
		g_iDayOfWeek = ENUM_TO_INT(GET_CLOCK_DAY_OF_WEEK())
		
		WAIT(0)
	
	ENDWHILE
	
	
	g_bCandidateSystemMidProcessing = FALSE
ENDSCRIPT
