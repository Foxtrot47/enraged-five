

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_datafile.sch"
USING "commands_script.sch"                                
USING "stack_sizes.sch"

USING "CompletionPercentage_public.sch"
USING "CompletionPercentage_private.sch"

USING "cellphone_public.sch"
USING "flow_public_GAME.sch"
USING "Flow_Mission_Data_Public.sch"
USING "flow_public_core.sch"
USING "achievement_public.sch"


#if IS_DEBUG_BUILD
USING "commands_debug.sch"
USING "dialogue_public.sch"
#endif



                                                                  






// ******************************************************************************************
// ******************************************************************************************
// ******************************************************************************************
//
//      SCRIPT NAME     :   CompletionPercentage_controller.sc
//      AUTHOR          :   Steve T
//      DESCRIPTION     :   Tracks changes to completion states of missions and miscellaneous
//                      :   tasks and oddjobs to provide overall percentage complete of main
//                      :   game.
//      DATE            :   25.07.11                  
//
//
// ******************************************************************************************
// ******************************************************************************************
// ******************************************************************************************





 //Debug widget declarations.
#if IS_DEBUG_BUILD
            
    BOOL b_DisplayDebugCompletionPercentage = FALSE
    BOOL b_RecalculateDebugComponents = FALSE
    BOOL b_DumpComponentList = FALSE
    BOOL b_PrintDataLookupTest = FALSE
    BOOL b_TotalProgressDump = FALSE

    BOOL b_MarkAllElements = FALSE

    BOOL b_BypassCloudUploadTimeout = FALSE

    FLOAT f_tempTotalProgressStatGet
     

    SP_MISSIONS eMissionsID 

    SP_MINIGAMES eMinigamesID
    g_eRC_MissionIDs eRandomCharsID

    //structPedsForConversation MyLocalPedStruct2


#endif




//Release vars.

DATAFILE_DICT MissionsDicts[70]
DATAFILE_DICT MinigamesDicts[50]

DATAFILE_DICT OddjobsDicts[65]

DATAFILE_DICT RandomEventsDicts[85]
DATAFILE_DICT RandomCharsDicts[60]

DATAFILE_DICT MiscellaneousDicts[32]
//DATAFILE_DICT FriendsDicts[1] //Friends now rolled in with Miscellaneous section in json file. See #1196544



INT MissionsCount = 0
INT MinigamesCount = 0

INT OddjobsCount = 0

INT RandomCharsCount = 0
INT RandomEventsCount = 0

INT MiscellaneousCount = 0

//INT FriendsCount = 0  //Friends now rolled in with Miscellaneous section in json file. See #1196544



VECTOR FlowReturnVector




//Game Events throttle
INT CheckGameEventsThrottle_CurrentTime, CheckGameEventsThrottle_StartTime
INT i_ThrottleTimeDifference

INT LastCloudUploadStartTime


BOOL b_WantedLevelEscapeInProgress = FALSE





//Cloud manipulation

BOOL b_CP_Data_For_Cloud_Has_Been_Constructed = FALSE
BOOL b_Uploading_CP_Data_To_Cloud = FALSE

BOOL b_Successful_Upload_To_Cloud = FALSE

BOOL b_OkayToCommenceCloudUpload = FALSE


//DATAFILE_DICT CP_DataFileDict
DATAFILE_DICT CP_GeneralInfoDict

DATAFILE_DICT SectionsDict

DATAFILE_ARRAY DFA_Minigames
DATAFILE_ARRAY DFA_Missions

DATAFILE_ARRAY DFA_Oddjobs

DATAFILE_ARRAY DFA_RandomChars
DATAFILE_ARRAY DFA_RandomEvents

DATAFILE_ARRAY DFA_Miscellaneous

//DATAFILE_ARRAY DFA_Friends //Friends now rolled in with Miscellaneous section in json file. See #1196544



//DATAFILE_DICT MissionsDicts[g_Group_Num_of_Missions]
//DATAFILE_DICT MinigamesDicts[g_Group_Num_of_Minigames]

                                   
//DATAFILE_ARRAY DFA_ScriptRegisteredin100pc
//
//#if IS_DEBUG_BUILD
//    DATAFILE_ARRAY DFA_DebugRegisteredin100pc
//#endif
//
//DATAFILE_ARRAY DFA_TextKey
//
//


//STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct
//SP_MISSIONS eMission 



//TEXT_LABEL txtMissionName
//INT iLocalPlayerScore

INT tempIndex


#if IS_DEBUG_BUILD

PROC Create_CompletionPercentage_Widget_Group()




    PRINTNL()
    PRINTSTRING("COMPLETIONPERCENTAGE_CONTROLLER - Creating Debug Widget Group.")
    PRINTNL()


    START_WIDGET_GROUP  ("100 Percent Completion Debug")
        
        ADD_WIDGET_BOOL ("Bypass Upload Timeout for Checklist Debug", b_BypassCloudUploadTimeout)

        ADD_WIDGET_BOOL ("Display Current Completion Percentage", b_DisplayDebugCompletionPercentage)

        ADD_WIDGET_BOOL ("Recalculate percentage based on Debug Individual components list.", b_RecalculateDebugComponents)

        ADD_WIDGET_BOOL ("Dump List of Components assigned by script to console...", b_DumpComponentList)

        ADD_WIDGET_BOOL ("Mark all gameplay elements as debug complete.", b_MarkAllElements)
        
        ADD_WIDGET_BOOL ("Print component data lookup test results...", b_PrintDataLookupTest)

        ADD_WIDGET_BOOL ("Dump TOTAL PROGRESS MADE stat to TTY...", b_TotalProgressDump)

        ADD_WIDGET_BOOL ("Output Comp and Uncomp elements to JSON", g_OutputCompletedandUncompletedElements)



        START_WIDGET_GROUP ("Debug Individual components")



            START_WIDGET_GROUP ("Missions Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISSIONS

                        ADD_WIDGET_STRING ("____________________________________________________________________________")
                        ADD_WIDGET_STRING (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)



                        IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_ChoiceMission = CHOICE_MISSION

                            STRING tempString

                            SWITCH INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex)

                                
                           

                                CASE CP_BH3_CHOICE
                                    tempString = "The Big Score 2 CHOICE"
                                BREAK
                            
                                /*
                                CASE CP_FH2_CHOICE
                                    tempString = "The Agency Heist 2 CHOICE"
                                BREAK
                                */

                                CASE CP_FH3_CHOICE
                                    tempString = "The Agency Heist 3 CHOICE"
                                BREAK


                                /*
                                CASE CP_JH2_CHOICE
                                    tempstring = "The Jewel Store 2 finale CHOICE"
                                BREAK
                                */

                           
                                CASE CP_DH2_CHOICE
                                    tempstring = "The Port of Los Santos 2 finale CHOICE"
                                BREAK

                   

                                CASE CP_FINALE_PREP_CHOICE
                                    tempstring = "The Big Score Stage 1 CHOICE"
                                BREAK


                                CASE CP_FINALE_PREP_CHOICE2
                                    tempstring = "The Big Score Stage 2 CHOICE"
                                BREAK


                                CASE CP_FINALE_CHOICE
                                    tempstring = "The Big Decision CHOICE"
                                BREAK

                                
                                CASE CP_JH_PREP_CHOICE
                                    tempstring = "Jewel Store Prep Choice"
                                BREAK


                                DEFAULT
                                    tempstring = "WARNING! THIS MISSION MARKED AS CHOICE!"
                                BREAK
                                

                            ENDSWITCH


                            ADD_WIDGET_BOOL (tempString, g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)    


                        ELSE

                            //ADD_WIDGET_STRING ("_____________________________")
                            //ADD_WIDGET_STRING (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                                //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)
                            
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                            

                        ENDIF

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()
             







            //Random Characters.

            START_WIDGET_GROUP ("Random Characters Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMCHARS


                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                      
                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()



            START_WIDGET_GROUP ("Random Characters Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()

            







             //Minigames.

            START_WIDGET_GROUP ("Minigames Group - 100 percent.")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MINIGAMES

           
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                       
                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()


            
            
            START_WIDGET_GROUP ("Minigames Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()







            
            //Oddjobs.

            START_WIDGET_GROUP ("Oddjobs Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_ODDJOBS

           
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                       
                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()



            START_WIDGET_GROUP ("Oddjobs Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()

            














            //Random Events.

            START_WIDGET_GROUP ("Random Events Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMEVENTS

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()




            START_WIDGET_GROUP ("Random Events Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()

















            //Miscellaneous

            START_WIDGET_GROUP ("Miscellaneous Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISCELLANEOUS

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()



            START_WIDGET_GROUP ("Miscellaneous Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()








            //Friends

            START_WIDGET_GROUP ("Friends Group - 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()


            
            START_WIDGET_GROUP ("Friends Group - Ingame but NOT 100 percent")
                                                               
            
                tempIndex = 0 

                WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                    
                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT

                   
                            ADD_WIDGET_BOOL ((GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)),
                                            g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                        

                    ENDIF

                    tempIndex ++

                ENDWHILE  
            
            
            STOP_WIDGET_GROUP ()










        STOP_WIDGET_GROUP ()

    STOP_WIDGET_GROUP ()

   
   

ENDPROC
#endif






#if IS_DEBUG_BUILD


PROC Maintain_CompletionPercentage_Debug()


    IF b_MarkAllElements = TRUE
        SetAllItemsAsDebugComplete()
        PRINTNL()
        PRINTSTRING ("CP_CONTROLLER - WARNING! Marked all elements as complete from debug widget! Also reset completion flag.")
        PRINTNL()

        b_MarkAllElements = FALSE
    ENDIF





    IF b_DisplayDebugCompletionPercentage


       



        DISPLAY_TEXT_WITH_LITERAL_STRING (0.6, 0.60, "STRING", "Completion")
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.6, 0.70, "STRING", "Percentage")

        DISPLAY_TEXT_WITH_FLOAT (0.85, 0.7, "NUMBER", g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage, 3)




       

        //Data to display...
        //FLOAT g_c_Missions_CombinedWeighting //When all missions are complete, max reached.
        //FLOAT g_f_Expected_Number_in_Missions_Group

        //INT g_Group_Num_of_Missions   //actual number

        //INT temp_Group_Num_of_Missions //completed number


        DRAW_RECT (0.605, 0.317, 0.741, 0.375, 0, 0, 0, 185)
        DRAW_RECT (0.605, 0.722, 0.741, 0.375, 0, 0, 0, 185)



        //Missions
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.15, "STRING", "Missions CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.20, "STRING", "Missions Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.25,0.23, "NUMBER", temp_Group_Num_of_Missions)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.28, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.25, 0.31, "NUMBER", g_Group_Num_of_Missions)


        IF g_f_Expected_Number_in_Missions_Group =  g_Group_Num_of_Missions
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.36, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.38, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.25, 0.41, "NUMBER", g_f_Expected_Number_in_Missions_Group, 0)

        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.45, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.25, 0.47, "NUMBER", (g_c_Missions_CombinedWeighting / g_f_Expected_Number_in_Missions_Group), 4)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.32, 0.47, "NUMBER", g_c_Missions_CombinedWeighting, 1)




        //RandomChars
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.15, "STRING", "RandChar CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.20, "STRING", "RandChar Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.4, 0.23, "NUMBER", temp_Group_Num_of_RandomChars)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.28, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.4, 0.31, "NUMBER", g_Group_Num_of_RandomChars)


        IF g_f_Expected_Number_in_RandomChars_Group =   g_Group_Num_of_RandomChars
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.36, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.38, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.4, 0.41, "NUMBER", g_f_Expected_Number_in_RandomChars_Group, 0)

        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.45, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.4, 0.47, "NUMBER", (g_c_RandomChars_CombinedWeighting / g_f_Expected_Number_in_RandomChars_Group), 4)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.47, 0.47, "NUMBER", g_c_RandomChars_CombinedWeighting, 1)





        //Minigames
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.15, "STRING", "Minigames CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.20, "STRING", "Minigames Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.55, 0.23, "NUMBER", temp_Group_Num_of_Minigames)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.28, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.55, 0.31, "NUMBER", g_Group_Num_of_Minigames)


        IF g_f_Expected_Number_in_Minigames_Group = g_Group_Num_of_Minigames
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF

        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.36, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.38, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.55, 0.41, "NUMBER", g_f_Expected_Number_in_Minigames_Group, 0)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.55, 0.45, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.55, 0.47, "NUMBER", (g_c_Minigames_CombinedWeighting / g_f_Expected_Number_in_Minigames_Group), 3)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.62, 0.47, "NUMBER", g_c_Minigames_CombinedWeighting, 1)






        //Oddjobs
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.15, "STRING", "Oddjobs CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.20, "STRING", "Oddjobs Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.7, 0.23, "NUMBER", temp_Group_Num_of_Oddjobs)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.28, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.7, 0.31, "NUMBER", g_Group_Num_of_Oddjobs)


        IF g_f_Expected_Number_in_Oddjobs_Group =   g_Group_Num_of_Oddjobs
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.36, "STRING", "Weighting Divisor")
        
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.38, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.7, 0.41, "NUMBER", g_f_Expected_Number_in_Oddjobs_Group, 0)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.7, 0.45, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.7, 0.47, "NUMBER", (g_c_Oddjobs_CombinedWeighting / g_f_Expected_Number_in_Oddjobs_Group), 3)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.77, 0.47, "NUMBER", g_c_Oddjobs_CombinedWeighting, 1)





        //Random Events
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.15, "STRING", "RandEvents CP Details")
        format_medium_ostext(255, 255, 76, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.18, "STRING", "Need only finish 1/4")
         format_medium_ostext(255, 255, 76, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.20, "STRING", "of events to get full %")

        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.23, "STRING", "RandEvents Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.85, 0.26, "NUMBER", temp_Group_Num_of_RandomEvents)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.29, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.85, 0.32, "NUMBER", g_Group_Num_of_RandomEvents)


        IF g_f_Expected_Number_in_RandomEvents_Group = g_Group_Num_of_RandomEvents
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.36, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.38, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.85, 0.41, "NUMBER", g_f_Expected_Number_in_RandomEvents_Group, 0)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.85, 0.45, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.85, 0.47, "NUMBER", ((g_c_RandomEvents_CombinedWeighting / g_f_Expected_Number_in_RandomEvents_Group)* 4), 3)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.92, 0.47, "NUMBER", g_c_RandomEvents_CombinedWeighting, 1)






        //Miscellaneous
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.55, "STRING", "Miscellan. CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.60, "STRING", "Miscellan. Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.25,0.63, "NUMBER", temp_Group_Num_of_Miscellaneous)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.68, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.25, 0.71, "NUMBER", g_Group_Num_of_Miscellaneous)


        IF g_f_Expected_Number_in_Miscellaneous_Group = g_Group_Num_of_Miscellaneous
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.76, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.78, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.25, 0.81, "NUMBER", g_f_Expected_Number_in_Miscellaneous_Group, 0)

        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.25, 0.85, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.25, 0.87, "NUMBER", (g_c_Miscellaneous_CombinedWeighting / g_f_Expected_Number_in_Miscellaneous_Group), 3)


        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.32, 0.87, "NUMBER", g_c_Miscellaneous_CombinedWeighting, 1)





        //Friends
        format_medium_ostext(255, 255, 0, 255)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.55, "STRING", "Friends CP Details")


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.60, "STRING", "Friends Registered")
         
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.4, 0.63, "NUMBER", temp_Group_Num_of_Friends)


        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.68, "STRING", "Available from start")
        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_NUMBER (0.4, 0.71, "NUMBER", g_Group_Num_of_Friends)


        IF g_f_Expected_Number_in_Friends_Group = g_Group_Num_of_Friends
            format_medium_ostext(255, 255, 255, 205)
        ELSE
            format_medium_ostext(255, 10, 10, 205)
        ENDIF
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.76, "STRING", "Weighting Divisor")
        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.78, "STRING", "(should match avail.)")

        
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.4, 0.81, "NUMBER", g_f_Expected_Number_in_Friends_Group, 0)

        format_medium_ostext(255, 255, 255, 205)
        DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.85, "STRING", "% per Item / Total group %")
        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.4, 0.87, "NUMBER", (g_c_Friends_CombinedWeighting / g_f_Expected_Number_in_Friends_Group), 3)

        format_medium_ostext(255, 255, 255, 255)
        DISPLAY_TEXT_WITH_FLOAT (0.47, 0.87, "NUMBER", g_c_Friends_CombinedWeighting, 1)



    ENDIF



    IF b_RecalculateDebugComponents

        RecalculateResultantPercentageTotal()

        PRINTNL()
        PRINTSTRING ("CP_CONTROLLER - WARNING! Recalculated Resultant Percentage Total from debug widget!")
        PRINTNL()

        b_RecalculateDebugComponents = FALSE

    ENDIF


    
    IF b_DumpComponentList 

        INT debugIndex = 0 

            WHILE debugIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[debugIndex].CP_Marked_As_Complete = TRUE


                    PRINTSTRING ("CP_CONTROLLER - This component was assigned by script to be included in Resultant Percentage: ")
                    PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[debugIndex].CP_Label))
                    PRINTNL()

                ELSE
                                 
    
                    PRINTSTRING ("CP_CONTROLLER - Not yet assigned by script: ")
                    PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[debugIndex].CP_Label))
                    PRINTNL()

                ENDIF

                debugIndex ++

                PRINTNL()
                PRINTINT(debugIndex)

            ENDWHILE

            PRINTNL()
            PRINTNL()


        b_DumpComponentList = FALSE

     ENDIF
    
    //Added by BenR.
    IF b_PrintDataLookupTest
        PRINT_100_PERCENT_COMPLETION_DATA_LOOKUP_TEST()
        b_PrintDataLookupTest = FALSE
    ENDIF


    IF b_TotalProgressDump

        STAT_GET_FLOAT (TOTAL_PROGRESS_MADE, f_tempTotalProgressStatGet)

        PRINTSTRING("Native Stat TOTAL PROGRESS MADE is ")
        PRINTFLOAT (f_tempTotalProgressStatGet)
        PRINTNL()

    ENDIF


ENDPROC

#endif







PROC CheckForCombinedTimesBusted()


    INT i_CombinedTimesBusted, iTempValue

    i_CombinedTimesBusted = 0

    
    STAT_GET_INT(SP0_BUSTED, iTempValue)
    i_CombinedTimesBusted = i_CombinedTimesBusted + iTempValue

    STAT_GET_INT(SP1_BUSTED, iTempValue)
    i_CombinedTimesBusted = i_CombinedTimesBusted + iTempValue

    STAT_GET_INT(SP2_BUSTED, iTempValue)
    i_CombinedTimesBusted = i_CombinedTimesBusted + iTempValue


    IF i_CombinedTimesBusted  >  249 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_250)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_BUSTED, 250)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_100)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_250)
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been busted 250 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF


    
    IF i_CombinedTimesBusted  >  99 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_100)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_BUSTED, 100)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_100)
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been busted 100 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF



    IF i_CombinedTimesBusted  >  49 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_50)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_BUSTED, 50)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_50)
   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been busted 50 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF

    
    IF i_CombinedTimesBusted  >  24 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_25)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_BUSTED, 25)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_25)

   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been busted 25 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF




    IF i_CombinedTimesBusted  >  9


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_BUSTED, 10)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_BUSTED_10) 
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been busted 10 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF






ENDPROC
                                                                                                          







PROC CheckForCombinedTimesWasted()




    INT i_CombinedTimesWasted, iTempValue

    i_CombinedTimesWasted = 0

    
    STAT_GET_INT(SP0_DEATHS, iTempValue)
    i_CombinedTimesWasted = i_CombinedTimesWasted + iTempValue

    STAT_GET_INT(SP1_DEATHS, iTempValue)
    i_CombinedTimesWasted = i_CombinedTimesWasted + iTempValue

    STAT_GET_INT(SP2_DEATHS, iTempValue)
    i_CombinedTimesWasted = i_CombinedTimesWasted + iTempValue


    IF i_CombinedTimesWasted  >  249 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_250)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_DEATHS, 250)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_100)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_250)
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been Wasted 250 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF


    
    IF i_CombinedTimesWasted  >  99 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_100)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_DEATHS, 100)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_100)
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been Wasted 100 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF



    IF i_CombinedTimesWasted  >  49 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_50)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_DEATHS, 50)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_25)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_50)
   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been Wasted 50 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF

    
    IF i_CombinedTimesWasted  >  24 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_25)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_DEATHS, 25)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_25)

   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been Wasted 25 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF




    IF i_CombinedTimesWasted  >  9


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_DEATHS, 10)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_TIMES_COMBINED_Wasted_10) 
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has been Wasted 10 times")
                PRINTNL()

            #endif

        ENDIF


    ENDIF






ENDPROC








PROC CheckForCombinedShotsFired()




    INT i_CombinedShotsFired, iTempValue

    i_CombinedShotsFired = 0

    
    STAT_GET_INT(SP0_SHOTS, iTempValue)
    i_CombinedShotsFired = i_CombinedShotsFired + iTempValue

    STAT_GET_INT(SP1_SHOTS, iTempValue)
    i_CombinedShotsFired = i_CombinedShotsFired + iTempValue

    STAT_GET_INT(SP2_SHOTS, iTempValue)
    i_CombinedShotsFired = i_CombinedShotsFired + iTempValue


    IF i_CombinedShotsFired  >  4999999


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_5)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 5000000)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_2)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_3)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_4)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_5)

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has fired 5 million shots")
                PRINTNL()

            #endif

        ENDIF


    ENDIF


    
    IF i_CombinedShotsFired  >  3999999 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_4)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 4000000)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_2)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_3)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_4)
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has fired 4 million shots")
                PRINTNL()

            #endif

        ENDIF


    ENDIF



    IF i_CombinedShotsFired  >  2999999 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_3)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 3000000)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_2)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_3)
   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has fired 3 million shots")
                PRINTNL()

            #endif

        ENDIF


    ENDIF

    
    IF i_CombinedShotsFired  >  1999999 


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_2)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 2000000)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1) 
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_2)

   
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has fired 2 million shots")
                PRINTNL()

            #endif

        ENDIF


    ENDIF




    IF i_CombinedShotsFired  >  999999


        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1)

            PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 1000000)   
            
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_COMBINED_SHOTS_MILLION_1) 
         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has fired 1 million shots")
                PRINTNL()

            #endif

        ENDIF


    ENDIF






ENDPROC









PROC CheckForCombinedDistanceDriven()


    FLOAT f_CombinedDistanceDriven, fTempValue   //Think we want to use a combination figure of the player driven as Michael, Franklin and Trevor.

    f_CombinedDistanceDriven = 0

    STAT_GET_FLOAT(SP0_DIST_DRIVING_CAR, fTempValue)  //Michael
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue   
    
    STAT_GET_FLOAT(SP0_DIST_DRIVING_QUADBIKE, fTempValue)  //Michael
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP0_DIST_DRIVING_BIKE, fTempValue)  //Michael
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP0_DIST_DRIVING_BICYCLE, fTempValue)  //Michael
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

   // STAT_GET_FLOAT(SP0_DIST_DRIVING_TRAILER, fTempValue)  //Michael
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue


     
    
    STAT_GET_FLOAT(SP1_DIST_DRIVING_CAR, fTempValue)  //Franklin
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue 
    
    STAT_GET_FLOAT(SP1_DIST_DRIVING_QUADBIKE, fTempValue)  //Franklin
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP1_DIST_DRIVING_BIKE, fTempValue)  //Franklin
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP1_DIST_DRIVING_BICYCLE, fTempValue)  //Franklin
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

//    STAT_GET_FLOAT(SP1_DIST_DRIVING_TRAILER, fTempValue)  //Franklin
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue


    
       

    STAT_GET_FLOAT(SP2_DIST_DRIVING_CAR, fTempValue) //Trevor
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP2_DIST_DRIVING_QUADBIKE, fTempValue)  //Trevor
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP2_DIST_DRIVING_BIKE, fTempValue)  //Trevor
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

    STAT_GET_FLOAT(SP2_DIST_DRIVING_BICYCLE, fTempValue)  //Trevor
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue

 //   STAT_GET_FLOAT(SP2_DIST_DRIVING_TRAILER, fTempValue)  //Trevor
    f_CombinedDistanceDriven = f_CombinedDistanceDriven + fTempValue





    /*
    INT iTempValue
    //Test to make sure presence event stat update value passed doesn't impact local stat.
    PRINTSTRING("Check presence update does not impact local stat ")
    STAT_GET_INT(SP0_SHOTS, iTempValue)
    PRINTINT(iTempValue)
    PRINTNL()
    PRESENCE_EVENT_UPDATESTAT_INT (SP0_SHOTS, 5000)
    */
    
        



    //IF f_CombinedDistanceDriven > 5000.0    //Need to convert this stuff into miles for the check. Just testing out with the metres, presumably, that the driving stats chuck back.
    IF f_CombinedDistanceDriven > (1609.344 * 50000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_50000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_CAR, 50000.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres
                                                                             //Process_SP_Stat_Update perhaps needs updated with new stat also.
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_500)  
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_5000)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_50000)

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has driven 50000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


    //IF f_CombinedDistanceDriven > 3000.0    //Need to convert this stuff into miles for the check. Just testing out with the metres, presumably, that the driving stats chuck back.
    IF f_CombinedDistanceDriven > (1609.344 * 5000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_5000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_CAR, 5000.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_500)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_5000)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has driven 5000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


    //IF f_CombinedDistanceDriven > 1000.0    //Need to convert this stuff into miles for the check. Just testing out with the metres, presumably, that the driving stats chuck back.
   
    IF f_CombinedDistanceDriven > (1609.344 * 500.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_500)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_CAR, 500.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_ROADVEH_500)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has driven 500 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF

            

ENDPROC







PROC CheckForCombinedDistanceFlown()


    FLOAT f_CombinedDistanceFlown, fTempValue   //Think we want to use a combination figure of the player Flown as Michael, Franklin and Trevor.

    f_CombinedDistanceFlown = 0

    STAT_GET_FLOAT(SP0_DIST_DRIVING_PLANE, fTempValue)  //Michael
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue  
    
    STAT_GET_FLOAT(SP0_DIST_DRIVING_HELI, fTempValue)  //Michael
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue 
      
  //  STAT_GET_FLOAT(SP0_DIST_DRIVING_AUTOGYRO, fTempValue)  //Michael
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue     



    STAT_GET_FLOAT(SP1_DIST_DRIVING_PLANE, fTempValue)  //Franklin
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue  
    
    STAT_GET_FLOAT(SP1_DIST_DRIVING_HELI, fTempValue)  //Franklin
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue 
      
  //  STAT_GET_FLOAT(SP1_DIST_DRIVING_AUTOGYRO, fTempValue)  //Franklin
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue 
    
      

    STAT_GET_FLOAT(SP2_DIST_DRIVING_PLANE, fTempValue) //Trevor
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue

    STAT_GET_FLOAT(SP2_DIST_DRIVING_HELI, fTempValue)  //Trevor
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue 
      
   // STAT_GET_FLOAT(SP2_DIST_DRIVING_AUTOGYRO, fTempValue)  //Trevor
    f_CombinedDistanceFlown = f_CombinedDistanceFlown + fTempValue 


        



    IF f_CombinedDistanceFlown > (1609.344 * 50000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_50000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_PLANE, 50000.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres
                                                                             //Process_SP_Stat_Update perhaps needs updated with new stat also.
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_500)  
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_5000)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_50000)

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Flown 50000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


    IF f_CombinedDistanceFlown > (1609.344 * 5000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_5000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_PLANE, 5000.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_500)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_5000)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Flown 5000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


   
    IF f_CombinedDistanceFlown > (1609.344 * 500.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_500)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_DRIVING_PLANE, 500.0)   //! NEED new combined stat for driving here! //Test value! Needs to be the 500 miles converted to metres

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_DRIVING_FLYINGVEHICLE_500)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Flown 500 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF

            

ENDPROC













PROC CheckForCombinedDistanceRan()


    FLOAT f_CombinedDistanceRan, fTempValue   //Think we want to use a combination figure of the player Ran as Michael, Franklin and Trevor.

    f_CombinedDistanceRan = 0

    STAT_GET_FLOAT(SP0_DIST_RUNNING, fTempValue)  //Michael
    f_CombinedDistanceRan = f_CombinedDistanceRan + fTempValue  
    
    STAT_GET_FLOAT(SP1_DIST_RUNNING, fTempValue)  //Franklin
    f_CombinedDistanceRan = f_CombinedDistanceRan + fTempValue  
    
    STAT_GET_FLOAT(SP2_DIST_RUNNING, fTempValue)  //Trevor
    f_CombinedDistanceRan = f_CombinedDistanceRan + fTempValue 

        



    IF f_CombinedDistanceRan > (1609.344 * 1000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_1000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_RUNNING, 1000.0)   

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_50)  
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_100)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_1000)

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Ran 1000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


    IF f_CombinedDistanceRan > (1609.344 * 100.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_100)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_RUNNING, 100.0)   

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_100)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has ran 100 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


   
    IF f_CombinedDistanceRan > (1609.344 * 50.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_50)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_RUNNING, 50.0)  

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_RAN_50)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Ran 50 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF

            

ENDPROC










PROC CheckForCombinedDistanceSwam()


    FLOAT f_CombinedDistanceSwam, fTempValue   //Think we want to use a combination figure of the player Swam as Michael, Franklin and Trevor.

    f_CombinedDistanceSwam = 0

    STAT_GET_FLOAT(SP0_DIST_SWIMMING, fTempValue)  //Michael
    f_CombinedDistanceSwam = f_CombinedDistanceSwam + fTempValue  
    
    STAT_GET_FLOAT(SP1_DIST_SWIMMING, fTempValue)  //Franklin
    f_CombinedDistanceSwam = f_CombinedDistanceSwam + fTempValue  
    
    STAT_GET_FLOAT(SP2_DIST_SWIMMING, fTempValue)  //Trevor
    f_CombinedDistanceSwam = f_CombinedDistanceSwam + fTempValue 

        



    IF f_CombinedDistanceSwam > (1609.344 * 1000.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_1000)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_SWIMMING, 1000.0)   

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_50)  
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_100)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_1000)

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Swam 1000 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


    IF f_CombinedDistanceSwam > (1609.344 * 100.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_100)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_SWIMMING, 100.0)   

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_50)
            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_100)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Swam 100 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF


   
    IF f_CombinedDistanceSwam > (1609.344 * 50.0)
        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_50)       
            
            PRESENCE_EVENT_UPDATESTAT_FLOAT (SP0_DIST_SWIMMING, 50.0)  

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_DIST_Swam_50)  

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has Swam 50 miles")
                PRINTNL()

            #endif

        ENDIF

    ENDIF

            

ENDPROC









PROC CheckForFiveStarWantedLevelEvasion()
    
    

    IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist2A")) = 0)
	AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("rural_bank_heist")) = 0) //We can't let this check run during these missions as they apply a 5 star wanted level that can be cleared by failing.

            IF IS_PLAYER_PLAYING (PLAYER_ID()) //This will return false on death or arrest.


                IF b_WantedLevelEscapeInProgress

                        IF NOT (IS_ENTITY_DEAD (PLAYER_PED_ID()))

                                IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0

                                    WAIT (0) //Double check for safety. See bug 1413511

                                        IF NOT (IS_ENTITY_DEAD (PLAYER_PED_ID()))

                                            IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_EVADED_5STAR_WANTED_LEVEL)

                                                PRESENCE_EVENT_UPDATESTAT_INT (SP0_STARS_EVADED, 5)   
                                        
                                                SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker, g_BS_SP_Event_EVADED_5STAR_WANTED_LEVEL) 
                                     
                                                #if IS_DEBUG_BUILD

                                                    PRINTNL()
                                                    PRINTSTRING ("PRESENCE EVENT SENT - Player has evaded a 5 star wanted level.")
                                                    PRINTNL()

                                                #endif

                                            ENDIF   
                                                         
                                            b_WantedLevelEscapeInProgress = FALSE

                                        ELSE

                                            b_WantedLevelEscapeInProgress = FALSE

                                            #if IS_DEBUG_BUILD

                                                PRINTNL()
                                                PRINTSTRING ("Player died during 5 star Wanted Level evasion procedure - 1.")
                                                PRINTNL()

                                             #endif

                                        ENDIF

                                    
                                ENDIF

                
                        ELSE
                                
                                b_WantedLevelEscapeInProgress = FALSE

                                #if IS_DEBUG_BUILD

                                    PRINTNL()
                                    PRINTSTRING ("Player died during 5 star Wanted Level evasion procedure - 2 .")
                                    PRINTNL()

                                #endif
                
                        ENDIF
                        

                ELSE

                    IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 4
                    
                        b_WantedLevelEscapeInProgress = TRUE

                    ENDIF

                ENDIF

            
            ELSE
            
                b_WantedLevelEscapeInProgress = FALSE //Reset on death or arrest.   
                
            ENDIF 


    ELSE

        b_WantedLevelEscapeInProgress = FALSE 

    ENDIF


ENDPROC






PROC CheckForVehiclesDriven() 

    IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_DRIVEN_ALL_CARS)

        IF GET_PLAYER_HAS_DRIVEN_ALL_VEHICLES()
                         
            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("PRESENCE EVENT SENT - Player has driven all SP vehicles.")
                PRINTNL()

            #endif

            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_DRIVEN_ALL_CARS)
    
            PRESENCE_EVENT_UPDATESTAT_INT (SP_VEHICLE_MODELS_DRIVEN, 200)

        ENDIF

    ENDIF

ENDPROC







PROC CheckGameConditionsForPresenceEvents()

    

    CheckGameEventsThrottle_CurrentTime = GET_GAME_TIMER()
    i_ThrottleTimeDifference =  CheckGameEventsThrottle_CurrentTime - CheckGameEventsThrottle_StartTime


    IF i_ThrottleTimeDifference > 5000

        /*
        #if IS_DEBUG_BUILD

            cdPrintstring("Checking Game events throttle is working okay.")
            cdPrintnl()
            cdPrintnl()
    
        #endif
        */

        CheckForVehiclesDriven() //- returns true instantly?

        CheckForCombinedDistanceDriven()
        CheckForCombinedDistanceFlown()

        CheckForCombinedDistanceRan()
        CheckForCombinedDistanceSwam()


        CheckForCombinedTimesBusted()
        CheckForCombinedTimesWasted()
        CheckForCombinedShotsFired()


        CheckGameEventsThrottle_StartTime = GET_GAME_TIMER() //Reset throttle start time.

        
        INT temp_i_return = 0
        INT temp_i_ShutTimer = 0
        
        //Check for setting change for feed events.
        temp_i_Return = GET_PROFILE_SETTING(PROFILE_FEED_DELAY)

        
        //Update global used in Social_Feed_Controller.sch - see url:bugstar:1354142

        SWITCH  temp_i_Return

            CASE 0
                temp_i_ShutTimer = 0  //Instantaneous
            BREAK

            CASE 1
                temp_i_ShutTimer = 60000   //1 minute.
            BREAK

            CASE 2
                temp_i_ShutTimer = 120000  //2 minutes.
            BREAK

            CASE 3
                temp_i_ShutTimer = 180000  //3 minutes.
            BREAK

            CASE 4
                temp_i_ShutTimer = 240000  //4 minute.
            BREAK

            CASE 5
                temp_i_ShutTimer = 300000  //5 minutes.
            BREAK

            CASE 6
                temp_i_ShutTimer = 600000  //10 minutes.
            BREAK

            CASE 7
                temp_i_ShutTimer = 900000  //15 minutes.
            BREAK

            CASE 8
                temp_i_ShutTimer = 1800000     //30 minutes.
            BREAK

            CASE 9
                temp_i_ShutTimer = 3600000     //60 minutes.
            BREAK



            DEFAULT 

                #if IS_DEBUG_BUILD

                    PRINTNL()
                    PRINTSTRING ("CP_CONTROLLER - Delay Notification has returned a weird value - Defaulting to 5 minutes!") 
                    PRINTNL()

                #endif

                temp_i_ShutTimer = 300000  //5 minutes.

            BREAK


        ENDSWITCH


        IF temp_i_ShutTimer <> i_SP_QueueWindow_ShutTime //Check if the time specified by the return is different - if so we need ti update the master global.

            #if IS_DEBUG_BUILD
                
                PRINTNL()
                PRINTSTRING ("CP_CONTROLLER - Delay Notification has changed in frontend - Updating i_SP_QueueWindow_ShutTime to ")
                PRINTINT ((temp_i_ShutTimer / 60000))
                PRINTSTRING (" minutes.")
                PRINTNL()

            #endif

            i_SP_QueueWindow_ShutTime = temp_i_ShutTimer  //Critical! Update the master global used in social_feed_controller.sch since it has changed!

        ENDIF


    ENDIF
        
    
    //Five Star wanted level needs to run outwith the throttle...
    CheckForFiveStarWantedLevelEvasion()

    
       

ENDPROC








SCRIPT


    // This script needs to cleanup only when the game runs the magdemo and trial removal from MP.
    IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO)

        PRINTSTRING("...CompletionPercentage_controller.sc has been forced to cleanup (MAGDEMO / MP)")
        PRINTNL()
        
        TERMINATE_THIS_THREAD()

    ENDIF
    
    //Trial of removal from MP.
    //NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()


  
    #if IS_DEBUG_BUILD
        Create_CompletionPercentage_Widget_Group()
    #endif
 

    RecalculateResultantPercentageTotal() //Make sure stat is recalculated one time only on launch of new game.



    CheckGameEventsThrottle_StartTime = GET_GAME_TIMER()


    //LastCloudUpLoadStartTime = GET_GAME_TIMER() - Setting this will prevent the game from performing recalculation and cloud upload on loading a new game.

    LastCloudUpLoadStartTime = 0 //An initial set to zero however allows the authorisation routine to know that this is the launch of a new or saved game.

      
    WHILE TRUE

        WAIT(0)



        /*
        #if IS_DEBUG_BUILD

            IF g_DumpDisableEveryFrameCaller
            //IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER (MyLocalPedStruct2, CHAR_LESTER, "FM_1AU", "FM_LMG", CONV_PRIORITY_VERY_HIGH)
            //IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct2, CHAR_TREVOR, "MCH1AUD", "MCH1_CP01M", CONV_PRIORITY_VERY_HIGH)

                //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
                //DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (TRUE)
                //g_DumpDisableEveryFrameCaller = FALSE

            //ENDIF

            //DISABLE_CELLPHONE_THIS_FRAME_ONLY()

            ENDIF



            IF IS_KEYBOARD_KEY_JUST_PRESSED (KEY_0)

                Would use this... g_Trigger_CloudUpload_of_CP_Elements = TRUE 

                PRINTSTRING("[CompletionPercentage_Controller] Test firing cloud upload of CP elements.") 
                PRINTNL()

            ENDIF
        #endif
        */



//This is now done in MAINTAIN_RECEIVE_PRESENCE_EVENTS() in social_feed_controller.sch
//        Process_SP_Net_Events()

//File X:\gta5\script\dev\multiplayer\include\public\social_feed_controller.sch:
//446        PROC MAINTAIN_SOCIAL_FEED(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars)
//File X:\gta5\script\dev\singleplayer\scripts\socialcontrol\social_controller.sc:
//1565                      MAINTAIN_SOCIAL_FEED(socialFeedVars)







        CheckGameConditionsForPresenceEvents() //CheckGameEventsThrottle_CurrentTime is grabbed in here, we can use that for the cloud check also.


        IF g_Trigger_CloudUpload_of_CP_Elements

            IF (CheckGameEventsThrottle_CurrentTime - LastCloudUpLoadStartTime) > 300000 //Five minutes must have elapsed between this script's initialisation / last cloud upload and next upload attempt.
                                                                                         //# 1424520
                //Five Minute check okay...
                b_OkayToCommenceCloudUpload = TRUE

                LastCloudUpLoadStartTime = GET_GAME_TIMER()

                g_Trigger_CloudUpload_of_CP_Elements = FALSE

        
            ELSE        


                
                IF LastCloudUploadStartTime = 0 //This means that the first cloud upload on a new game load has not been performed, so safe to do so. #1485695

                
                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTSTRING("[CompletionPercentage_Controller] CLOUD UPLOAD - New or save game loaded. Authorising .json upload. #1485695") 
                        PRINTNL()
                    #endif


                    b_OkayToCommenceCloudUpload = TRUE

                    LastCloudUpLoadStartTime = GET_GAME_TIMER()

                    g_Trigger_CloudUpload_of_CP_Elements = FALSE


                ELSE

                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTSTRING("[CompletionPercentage_Controller] CLOUD UPLOAD - Five minutes have not elapsed since last .json cloud upload.") 
                        PRINTNL()
                    #endif

                    b_OkayToCommenceCloudUpload = FALSE

                    g_Trigger_CloudUpload_of_CP_Elements = FALSE

                ENDIF


                //Put debug widget check here for 1488238

                #if IS_DEBUG_BUILD
                
                    IF b_BypassCloudUploadTimeout = TRUE  //If ticked as TRUE, then this will force an upload regardless of five minute check above.

                        PRINTNL()
                        PRINTSTRING("[CompletionPercentage_Controller] CLOUD UPLOAD - Debug widget has been set BYPASSING five minute check and performing cloud upload of .json.") 
                        PRINTNL()


                        b_OkayToCommenceCloudUpload = TRUE

                        LastCloudUpLoadStartTime = GET_GAME_TIMER()

                        g_Trigger_CloudUpload_of_CP_Elements = FALSE

                    ENDIF
                
                #endif


            ENDIF

        ENDIF


         IF b_OkayToCommenceCloudUpload = TRUE
             IF NOT b_CP_Data_For_Cloud_Has_Been_Constructed
                IF NOT b_Uploading_CP_Data_To_Cloud



                    IF DATAFILE_GET_FILE_DICT() != NULL

                        DATAFILE_DELETE()

                        #if IS_DEBUG_BUILD

                            PRINTSTRING("[CompletionPercentage_Controller] Pre-cloud upload - Deleting rogue data file via DATAFILE_DELETE") 
                            PRINTNL()
                            
                        #endif


                        //This error will be received if the datafile is not removed and another upload is attempted.
                        //Error: m_file == NULL: A data file already exists (from Updating script completionpercentage_controller)
                    ENDIF





                    /* Having these here can cause a compiler warning regarding reference before set in loop...it doesn't seem to be a nasty one though. See Graeme.
                    DATAFILE_DICT MissionsDicts[70]
                    DATAFILE_DICT MinigamesDicts[25]

                    DATAFILE_DICT OddjobsDicts[65]
                
                    DATAFILE_DICT RandomEventsDicts[85]
                    DATAFILE_DICT RandomCharsDicts[50]

                    DATAFILE_DICT MiscellaneousDicts[5]
                    DATAFILE_DICT FriendsDicts[1]

                    */

                    MissionsCount = 0
                    MinigamesCount = 0

                    OddjobsCount = 0

                    RandomCharsCount = 0
                    RandomEventsCount = 0
                    
                    MiscellaneousCount = 0
                    
                    //FriendsCount = 0 //Friends now rolled in with Miscellaneous section in json file. See #1196544

                    

                    
                    DATAFILE_CREATE()

                    CP_GeneralInfoDict = DATAFILE_GET_FILE_DICT()
               


                
                          
                    
                    DATADICT_SET_FLOAT (CP_GeneralInfoDict, "tot_pc", g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)
                    
                    /*                                                                        
                    DATADICT_SET_INT (CP_GeneralInfoDict, "msnc", temp_Group_Num_of_Missions)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "mgc", temp_Group_Num_of_Minigames)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "ojc", temp_Group_Num_of_Oddjobs)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "rcc", temp_Group_Num_of_RandomChars)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "revc", temp_Group_Num_of_RandomEvents)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "misc", temp_Group_Num_of_Miscellaneous)
                    DATADICT_SET_INT (CP_GeneralInfoDict, "fdc", temp_Group_Num_of_Friends)
                    */


                    
                    SectionsDict = DATADICT_CREATE_DICT (CP_GeneralInfoDict, "Sect")

                 

                    

                    DFA_Minigames = DATADICT_CREATE_ARRAY (SectionsDict, "mgc")
                    DFA_Missions = DATADICT_CREATE_ARRAY (SectionsDict, "msnc")

                    DFA_Oddjobs = DATADICT_CREATE_ARRAY (SectionsDict, "ojc")
                    DFA_RandomChars = DATADICT_CREATE_ARRAY (SectionsDict, "rcc")

                    DFA_RandomEvents = DATADICT_CREATE_ARRAY (SectionsDict, "revc")
                    DFA_Miscellaneous = DATADICT_CREATE_ARRAY (SectionsDict, "misc")
                   
                    //DFA_Friends = DATADICT_CREATE_ARRAY (SectionsDict, "fdc")  //Friends now rolled in with Miscellaneous section in json file. See #1196544



                    tempIndex = 0

                    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

                        IF NOT (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_NO_GROUP)

                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISSIONS
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISSIONS_INGAME_BUT_NOT_100PERCENT




                                                                   
                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE
                         
                                    MissionsDicts[MissionsCount] = DATAARRAY_ADD_DICT (DFA_Missions)
                                    
                                    
                                    DATADICT_SET_STRING ( MissionsDicts[MissionsCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)                                
                                    DATADICT_SET_BOOL ( MissionsDicts[MissionsCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)
                                    
                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISSIONS
                                        DATADICT_SET_BOOL ( MissionsDicts[MissionsCount], "100", TRUE) //This is part of 100 percent, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( MissionsDicts[MissionsCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF

                                    #if IS_DEBUG_BUILD
                                   
                                        //DATADICT_SET_BOOL ( MissionsDicts[MissionsCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                   

                                        //Get the mission vector. The function will return this by default if nothing is available - RETURN <<0,0,0>>
                                        //New media should check for an x,y of 0, 0 and not display any co-ords wherever they are referenced online. Essentially 0,0 means non-applicable or available.
                                    
                       
                                        
                                        eMissionsID = GET_100_PERCENT_COMPLETION_STORY_MISSION(INT_TO_ENUM(enumCompletionPercentageEntries, tempIndex))

                                        IF eMissionsID = SP_MISSION_NONE //An assert will fire from Ben's functions if this is the case. Need to identify the culprit entry.

                                            PRINTNL()
                                            PRINTSTRING ("CompletionPercentageController: The suspect story mission entry is at position  ")
                                            PRINTINT (tempIndex)
                                            PRINTSTRING (" which is ")
                                            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label))
                                            PRINTNL()


                                        ENDIF

                                    #endif


                                    FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                    //The above can return a assert which highlights something wrong at Ben's side. Need to put in some debug to indicate which mission attempt is at fault.
                                    //SCRIPT: Script Name = completionpercentage_controller : Program Counter = 10199 GET_100_PERCENT_COMPLETION_STORY_MISSION: Tried to get a story mission enum from a 100 percent completion item with NULL enum data.

           


                                    DATADICT_SET_INT ( MissionsDicts[MissionsCount], "x", ROUND(FlowReturnVector.X) )  //Need to convert vector float component into int. ".5" will be rounded to positive infinity.
                                    
                                    DATADICT_SET_INT ( MissionsDicts[MissionsCount], "y", ROUND(FlowReturnVector.Y) )

                                    //Best score seems to return zero only for now.
                                    //DATADICT_SET_INT ( MissionsDicts[MissionsCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( MissionsDicts[MissionsCount], "prg", 0 )


                        

                                    MissionsCount ++

                                ENDIF
                            

                            ENDIF







                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MINIGAMES
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT



                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE


                                    MinigamesDicts[MinigamesCount] = DATAARRAY_ADD_DICT (DFA_Minigames)
                                    
                                    DATADICT_SET_STRING ( MinigamesDicts[MinigamesCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                    DATADICT_SET_BOOL ( MinigamesDicts[MinigamesCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                                    
                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MINIGAMES
                                        DATADICT_SET_BOOL ( MinigamesDicts[MinigamesCount], "100", TRUE) //This is part of 100 percent, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( MinigamesDicts[MinigamesCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF


                                    #if IS_DEBUG_BUILD

                                        //DATADICT_SET_BOOL ( MinigamesDicts[MinigamesCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                    
                                        eMinigamesID = GET_100_PERCENT_COMPLETION_MINIGAME(INT_TO_ENUM(enumCompletionPercentageEntries, tempIndex))

                                        IF eMinigamesID = MINIGAME_NONE //An assert will fire from Ben's functions if this is the case. Need to identify the culprit entry.

                                            PRINTNL()
                                            PRINTSTRING ("CompletionPercentageController: The suspect minigame entry is at position  ")
                                            PRINTINT (tempIndex)
                                            PRINTSTRING (" which is ")
                                            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label))
                                            PRINTNL()


                                        ENDIF
             
                                    #endif


                                    FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                    DATADICT_SET_INT ( MinigamesDicts[MinigamesCount], "x", ROUND(FlowReturnVector.X) )
                                    
                                    DATADICT_SET_INT ( MinigamesDicts[MinigamesCount], "y", ROUND(FlowReturnVector.Y) )

                                    //DATADICT_SET_INT ( MinigamesDicts[MinigamesCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( MinigamesDicts[MinigamesCount], "prg", 0 )


                                    MinigamesCount ++

                                ENDIF

                            ENDIF







                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_ODDJOBS
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT

                                
                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE

                                
                                
                                    OddjobsDicts[OddjobsCount] = DATAARRAY_ADD_DICT (DFA_Oddjobs)
                                    
                                    DATADICT_SET_STRING ( OddjobsDicts[OddjobsCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                    DATADICT_SET_BOOL ( OddjobsDicts[OddjobsCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_ODDJOBS
                                        DATADICT_SET_BOOL ( OddjobsDicts[OddjobsCount], "100", TRUE) //This is part of 100 percent, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( OddjobsDicts[OddjobsCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF




                                    #if IS_DEBUG_BUILD
                                    
                                        //DATADICT_SET_BOOL ( OddjobsDicts[OddjobsCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                    
                                        //Ben's treating minigames and oddjobs under the all encompassing "minigames" banner.
                                        eMinigamesID = GET_100_PERCENT_COMPLETION_MINIGAME(INT_TO_ENUM(enumCompletionPercentageEntries, tempIndex))

                                        IF eMinigamesID = MINIGAME_NONE //An assert will fire from Ben's functions if this is the case. Need to identify the culprit entry.

                                            PRINTNL()
                                            PRINTSTRING ("CompletionPercentageController: The suspect oddjob entry is at position  ")
                                            PRINTINT (tempIndex)
                                            PRINTSTRING (" which is ")
                                            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label))
                                            PRINTNL()


                                        ENDIF

                                    #endif

                                    FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                    DATADICT_SET_INT ( OddjobsDicts[OddjobsCount], "x", ROUND(FlowReturnVector.X) )
                                    
                                    DATADICT_SET_INT ( OddjobsDicts[OddjobsCount], "y", ROUND(FlowReturnVector.Y) )

                                    //DATADICT_SET_INT ( OddjobsDicts[OddjobsCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( OddjobsDicts[OddjobsCount], "prg", 0 )


                                    OddjobsCount ++

                                ENDIF

                            ENDIF








                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMCHARS
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT


                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE


                                    RandomCharsDicts[RandomCharsCount] = DATAARRAY_ADD_DICT (DFA_RandomChars)
                                    
                                    DATADICT_SET_STRING ( RandomCharsDicts[RandomCharsCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                    DATADICT_SET_BOOL ( RandomCharsDicts[RandomCharsCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMCHARS
                                        DATADICT_SET_BOOL ( RandomCharsDicts[RandomCharsCount], "100", TRUE) //This is part of 100 percent, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( RandomCharsDicts[RandomCharsCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF

                                    
                                    
                                    
                                    #if IS_DEBUG_BUILD
                                    
                                        //DATADICT_SET_BOOL ( RandomCharsDicts[RandomCharsCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                    
                                        eRandomCharsID = GET_100_PERCENT_COMPLETION_RANDOMCHAR_MISSION (INT_TO_ENUM(enumCompletionPercentageEntries, tempIndex))

                                        IF eRandomCharsID = NO_RC_MISSION //An assert will fire from Ben's functions if this is the case. Need to identify the culprit entry.

                                            PRINTNL()
                                            PRINTSTRING ("CompletionPercentageController: The suspect randomchar entry is at position  ")
                                            PRINTINT (tempIndex)
                                            PRINTSTRING (" which is ")
                                            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label))
                                            PRINTNL()


                                        ENDIF

                                    
                                    #endif

                                    FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))
                                    
                                    DATADICT_SET_INT ( RandomCharsDicts[RandomCharsCount], "x", ROUND(FlowReturnVector.X) )
                                    
                                    DATADICT_SET_INT ( RandomCharsDicts[RandomCharsCount], "y", ROUND(FlowReturnVector.Y) )

                                    //DATADICT_SET_INT ( RandomCharsDicts[RandomCharsCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( RandomCharsDicts[RandomCharsCount], "prg", 0 )


                                    RandomCharsCount ++


                                ENDIF

                            ENDIF







                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMEVENTS
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT


                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE


                                    RandomEventsDicts[RandomEventsCount] = DATAARRAY_ADD_DICT (DFA_RandomEvents)
                                    
                                    DATADICT_SET_STRING ( RandomEventsDicts[RandomEventsCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                    DATADICT_SET_BOOL ( RandomEventsDicts[RandomEventsCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                                    
                                    //ATTENTION! As random events are, to the player, optional ( internally they're not treated as such for calculating towards percentage,

                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_RANDOMEVENTS
                                        DATADICT_SET_BOOL ( RandomEventsDicts[RandomEventsCount], "100", FALSE) //ATTENTION! Not part of 100 percent as far as player is concerned, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( RandomEventsDicts[RandomEventsCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF
                                    

                                    #if IS_DEBUG_BUILD
                                        //DATADICT_SET_BOOL ( RandomEventsDicts[RandomEventsCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                    #endif

                                



                                    //FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                    //DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "x", ROUND(FlowReturnVector.X) )
                                    
                                    //DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "y", ROUND(FlowReturnVector.Y) )


                                    //Random Events use a script defined location passed in when the script is registered as complete.
                                    DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "x", ROUND(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_DefinedLocationX))
                                    DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "y", ROUND(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_DefinedLocationY))


                                    //DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( RandomEventsDicts[RandomEventsCount], "prg", 0 )


                                    RandomEventsCount ++

                                ENDIF

                            ENDIF






                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISCELLANEOUS
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS //Friends now rolled in with Miscellaneous section in json file. See #1196544
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT
                            OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT


                            
                                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete
                                OR g_OutputCompletedandUncompletedElements = TRUE


                                    MiscellaneousDicts[MiscellaneousCount] = DATAARRAY_ADD_DICT (DFA_Miscellaneous)
                                    
                                    DATADICT_SET_STRING ( MiscellaneousDicts[MiscellaneousCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                    DATADICT_SET_BOOL ( MiscellaneousDicts[MiscellaneousCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)



                                    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_MISCELLANEOUS
                                    OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS
                                        DATADICT_SET_BOOL ( MiscellaneousDicts[MiscellaneousCount], "100", TRUE) //This is part of 100 percent, mark json element as such.
                                    ELSE
                                        DATADICT_SET_BOOL ( MiscellaneousDicts[MiscellaneousCount], "100", FALSE) //Not part of 100 percent, mark json element as such.
                                    ENDIF




                                    #if IS_DEBUG_BUILD
                                        //DATADICT_SET_BOOL ( MiscellaneousDicts[MiscellaneousCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                    #endif

                                    FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                    DATADICT_SET_INT ( MiscellaneousDicts[MiscellaneousCount], "x", ROUND(FlowReturnVector.X) )
                                    
                                    DATADICT_SET_INT ( MiscellaneousDicts[MiscellaneousCount], "y", ROUND(FlowReturnVector.Y) )

                                    //DATADICT_SET_INT ( MiscellaneousDicts[MiscellaneousCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                    //DATADICT_SET_INT ( MiscellaneousDicts[MiscellaneousCount], "prg", 0 )


                                    MiscellaneousCount ++

                                ENDIF

                            ENDIF


                            /*
                            IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_FRIENDS

                                
                                FriendsDicts[FriendsCount] = DATAARRAY_ADD_DICT (DFA_Friends)
                                
                                DATADICT_SET_STRING ( FriendsDicts[FriendsCount], "nm", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Label)
                                DATADICT_SET_BOOL ( FriendsDicts[FriendsCount], "cmp", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete)

                                #if IS_DEBUG_BUILD
                                DATADICT_SET_BOOL ( FriendsDicts[FriendsCount], "dbg", g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Assigned_as_included_by_Script)
                                #endif

                                FlowReturnVector = GET_100_PERCENT_COMPLETION_WORLD_POSITION (INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))

                                DATADICT_SET_INT ( FriendsDicts[FriendsCount], "x", ROUND(FlowReturnVector.X) )
                                
                                DATADICT_SET_INT ( FriendsDicts[FriendsCount], "y", ROUND(FlowReturnVector.Y) )

                                DATADICT_SET_INT ( FriendsDicts[FriendsCount], "scr", (GET_100_PERCENT_COMPLETION_BEST_SCORE(INT_TO_ENUM (enumCompletionPercentageEntries, tempIndex))) )

                                DATADICT_SET_INT ( FriendsDicts[FriendsCount], "prg", 0 )


                                FriendsCount ++
                                

                            ENDIF
                            */


                        ENDIF

                        tempIndex ++

                    ENDWHILE







                    //Wrap these two calls with sign in / mp checks to be ultra-safe.

#IF USE_TU_CHANGES
                    IF SCRIPT_IS_CLOUD_AVAILABLE()
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
                    IF NETWORK_IS_CLOUD_AVAILABLE()
#ENDIF	//	NOT USE_TU_CHANGES

						b_CP_Data_For_Cloud_Has_Been_Constructed = TRUE
                        b_Uploading_CP_Data_To_Cloud = FALSE

                        g_Trigger_CloudUpload_of_CP_Elements = FALSE
                        b_OkayToCommenceCloudUpload = FALSE

                        #if IS_DEBUG_BUILD
                        
                            PRINTSTRING("[CompletionPercentage_Controller] Secondary SCRIPT_IS_CLOUD_AVAILABLE returns TRUE. Will attempt upload.") 
                            PRINTNL()
                        
                        #endif

                    ELSE

                        //Reset bools for another attempt next time percentage is recalculated if cloud routine cannot be launched this time round.
                        b_CP_Data_For_Cloud_Has_Been_Constructed = FALSE
                        b_Uploading_CP_Data_To_Cloud = FALSE

                        g_Trigger_CloudUpload_of_CP_Elements = FALSE
                        b_OkayToCommenceCloudUpload = FALSE



                        #if IS_DEBUG_BUILD
                        
                            PRINTSTRING("[CompletionPercentage_Controller] Secondary SCRIPT_IS_CLOUD_AVAILABLE returns FALSE. Resetting key bools.") 
                            PRINTNL()
                        
                        #endif

                    ENDIF

                ENDIF

             ENDIF
         ENDIF



    


                IF b_CP_Data_For_Cloud_Has_Been_Constructed

                    IF NOT b_Uploading_CP_Data_To_Cloud 

                        IF DATAFILE_START_SAVE_TO_CLOUD("GTA5/checklist/index.json") //Dave R. reported that this may have had an issue outwith my control.
                                                                                     //Perhaps timeout if it takes too long?    
                            b_Uploading_CP_Data_To_Cloud = TRUE

                            #if IS_DEBUG_BUILD

                                PRINTSTRING("[CompletionPercentage_Controller] Started to upload to cloud") 
                                PRINTNL()

                            #endif


                        ELSE

                            #if IS_DEBUG_BUILD

                                PRINTSTRING("[CompletionPercentage_Controller] Starting to upload to cloud...")
                                PRINTNL()
                            
                            #endif
                               
                        ENDIF
        
                    ELSE

                        /*
                        #if IS_DEBUG_BUILD

                            PRINTSTRING("[CompletionPercentage_Controller] Cloud upload in progress...")
                            PRINTNL()

                        #endif
                        */

                        IF NOT DATAFILE_UPDATE_SAVE_TO_CLOUD (b_Successful_Upload_To_Cloud) //The DATAFILE_UPDATE will only return true if the process has finished
                                                                                            //and the b_Successful_Upload_to_Cloud holds whether the process succeeded or failed.

                            IF (b_Successful_Upload_To_Cloud)

                                #if IS_DEBUG_BUILD
                                
                                    PRINTSTRING("[CompletionPercentage_Controller] Upload to cloud successful!") 
                                    PRINTNL()

                                #endif
                    
                            ELSE

                                #if IS_DEBUG_BUILD

                                    PRINTSTRING("[CompletionPercentage_Controller] Upload to cloud failed.") 
                                    PRINTNL()
                                    
                                #endif
   
                            ENDIF
                                                                                                                                        
                            
                            IF DATAFILE_GET_FILE_DICT() != NULL

                                DATAFILE_DELETE()

                                #if IS_DEBUG_BUILD

                                    PRINTSTRING("[CompletionPercentage_Controller] Deleting used file via DATAFILE_DELETE") 
                                    PRINTNL()
                                    
                                #endif


                                //This error will be received if the datafile is not removed and another upload is attempted.
                                //Error: m_file == NULL: A data file already exists (from Updating script completionpercentage_controller)
                            ENDIF
                            

                            b_CP_Data_For_Cloud_Has_Been_Constructed = FALSE
                            b_Uploading_CP_Data_To_Cloud = FALSE

                        ENDIF
                                                
                    ENDIF
                ENDIF
            
      














  

        //Fire off event when stat reaches 100 percent and bool it so it doesn't reiterate every frame.
        IF g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached = FALSE

            IF g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage = 100
            OR g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage > 100


#IF USE_TU_CHANGES
                IF SCRIPT_IS_CLOUD_AVAILABLE() //If this returns true then the user must be online and signed in. This may mean if the user isn't signed in then
                                                //no friends will be notified, as this is a one time only per game progress item.
#ENDIF	//	USE_TU_CHANGES
    
#IF NOT USE_TU_CHANGES
                IF NETWORK_IS_CLOUD_AVAILABLE() //If this returns true then the user must be online and signed in. This may mean if the user isn't signed in then
                                                //no friends will be notified, as this is a one time only per game progress item.
#ENDIF	//	NOT USE_TU_CHANGES
                    PRESENCE_EVENT_UPDATESTAT_FLOAT (TOTAL_PROGRESS_MADE, g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage)

                    #if IS_DEBUG_BUILD

                        PRINTSTRING("CompletionPercentage_Private is sending a 100 percent complete stat presence event.")
                        PRINTNL()

                    #endif



                    IF FACEBOOK_CAN_POST_TO_FACEBOOK()
                        
                        FACEBOOK_POST_COMPLETED_MILESTONE (FACEBOOK_MILESTONE_CHECKLIST)

                        SHOW_FACEBOOK_MILESTONE_FEED_MESSAGE (FACEBOOK_MILESTONE_CHECKLIST)
                    
                    ELSE

                        #if IS_DEBUG_BUILD

                            PRINTSTRING("CP_CONTROLLER - No facebook post allowed.")
                            PRINTNL()

                        #endif

                 
                    ENDIF


                ENDIF


                g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached = TRUE

                SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_GAME_100_PERCENT_COMPLETE, TRUE) //Set mission flow flag so that certain missions are unlocked. #966698
                
                SET_BUILDING_STATE( BUILDINGNAME_ES_FRANKLINS_HILLS_100_PERCENT_SHIRT, BUILDINGSTATE_DESTROYED) // 100% completion T-Shirt in Franklin's Hills apartment. - SR LDS.
                
                SET_BUILDING_STATE( BUILDINGNAME_IPL_UFO, BUILDINGSTATE_DESTROYED)                  // 100% completion UFO - SR LDS
                
                // add 100% tshirt to wardrobe
                SET_PED_COMP_ITEM_ACQUIRED_SP(PLAYER_ONE, COMP_TYPE_TORSO, TORSO_P1_100_PERCENT_TSHIRT, TRUE)
                CPRINTLN(DEBUG_PED_COMP, "100% tshirt acquired.")
                
                // activate the sasquatch RC
                RANDOM_CHARACTER_ACTIVATE_MISSION(RC_THELASTONE) 
                SET_BIT(g_savedGlobals.sRandomChars.savedRC[RC_THELASTONE].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED_IN_FLOW))
               
                AWARD_ACHIEVEMENT(ACH11) // Career Criminal - 100% on game stats
                #if IS_DEBUG_BUILD
               
                    PRINTSTRING("100 percent complete reached!")
                    PRINTNL()

                #endif

            ENDIF

        ENDIF
        
        
        
        
         
        
        // Stunt Jump tracker to maintain 100% & boost driving stats.
        
        IF GET_NUM_SUCCESSFUL_STUNT_JUMPS() > g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted
        
            IF (GET_NUM_SUCCESSFUL_STUNT_JUMPS() >= ACH19_STUNTJUMPS)
                AWARD_ACHIEVEMENT(ACH19) // Show Off - Get All 50 Stunt Jumps
            ENDIF
            
            INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_DRIVING_ABILITY, (GET_NUM_SUCCESSFUL_STUNT_JUMPS()-g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted)*2)
            g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted = GET_NUM_SUCCESSFUL_STUNT_JUMPS()
            CPRINTLN(DEBUG_AMBIENT, "iStuntJumpsCompleted is now ", g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted)
            IF g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted >= 50
                PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_KILLS, 100)
            ELIF g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted >= 38
                PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_KILLS, 75)
            ELIF g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted >= 25
                CPRINTLN(DEBUG_AMBIENT, "iStuntJumpsCompleted is greater than 25, completion registered on 100%.")
                REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_STNJMP)
                PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_KILLS, 50)
            ELIF g_savedGlobals.sAmbient.stuntJumps.iStuntJumpsCompleted >= 13
                PRESENCE_EVENT_UPDATESTAT_INT (SP0_WATER_CANNON_KILLS, 25)
            ENDIF
        ENDIF


        #if IS_DEBUG_BUILD

           
            Maintain_CompletionPercentage_Debug()


        #endif

    
            
    ENDWHILE
    
    
ENDSCRIPT


