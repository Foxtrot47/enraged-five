//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	tuneables_processing.sc 									//
//		AUTHOR			:	William Kennedy/Kenneth Ross								//
//		DESCRIPTION		:	Refreshes the global tuneable values.					 	//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "net_script_tunables.sch"

SCRIPT(TUNEABLES_LANCHER_STRUCT sTuneablesLauncherData)

	PRINTLN("STARTING TUNEABLES PROCESSING SCRIPT")
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tl63Print = GET_g_eTunablesContexts_DEBUG_PRINT(sTuneablesLauncherData.eContext)
	PRINTLN("[BWTUN] - sTuneablesLauncherData.eContext         = ", tl63Print)
	tl63Print = GET_g_eTunablesContexts_DEBUG_PRINT(sTuneablesLauncherData.eContextSubType)
	PRINTLN("[BWTUN] - sTuneablesLauncherData.eContextSubType  = ", tl63Print)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_Force_CONTENT_MODIFIER") 
		sTuneablesLauncherData.eContextSubType = INT_TO_ENUM(g_eTunablesContexts, GET_COMMANDLINE_PARAM_INT("sc_Force_CONTENT_MODIFIER") + ENUM_TO_INT(TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_INT))
		tl63Print = GET_g_eTunablesContexts_DEBUG_PRINT(sTuneablesLauncherData.eContextSubType)
		PRINTLN("[BWTUN] - sTuneablesLauncherData.eContextSubType  = ", tl63Print)
	ENDIF
	#ENDIF	
	
    // Before we do any processing, lets clear out our previously populated detection table
    NETWORK_ACCESS_TUNABLE_MODIFICATION_DETECTION_CLEAR()
    
	// Main loop
	WHILE (TRUE)
	
		WAIT(0)
		
		IF Refresh_MP_Script_Tunables(sTuneablesLauncherData.eContext, sTuneablesLauncherData.eContextSubType, sTuneablesLauncherData.bFillRankTuneables)
			PRINTLN("TERMINATING TUNEABLES PROCESSING SCRIPT")
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDWHILE
	
ENDSCRIPT
