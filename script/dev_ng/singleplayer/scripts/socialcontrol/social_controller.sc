//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   social_controller.sc                                        //
//      AUTHOR          :   Kenneth Ross                                                //
//      DESCRIPTION     :   Monitors the social integration events.                     //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_apps.sch"
USING "commands_vehicle.sch"
USING "comms_control_public.sch"
USING "vehicle_gen_public.sch"
USING "reward_unlocks.sch"
USING "net_scoring_common.sch"
USING "social_public.sch"
USING "shop_public.sch"
USING "finance_modifiers_public.sch"
USING "Transition_Controller.sch"
USING "carmod_shop_private.sch"
USING "respawn_location_private.sch"
USING "social_feed_controller.sch"
USING "transition_saving.sch"
USING "commands_socialclub.sch"
USING "code_control_data_gta5.sch"

USING "net_tuneables_controller.sch"
USING "net_realty_new.sch"
USING "FM_Community_Playlists.sch"

USING "static_car_app_data.sch"

ENUM SOCIAL_STAGE_ENUM
    SOCIAL_STAGE_INIT = 0,
    SOCIAL_STAGE_PROCESS,
    SOCIAL_STAGE_CLEANUP
ENDENUM
SOCIAL_STAGE_ENUM eStage = SOCIAL_STAGE_INIT

ENUM SOCIAL_INIT_ENUM
    INITIALISED_IN_SP,
    INITIALISED_IN_MP_FM,
    
    NOT_INITIALISED
ENDENUM
SOCIAL_INIT_ENUM eInit = NOT_INITIALISED

INT iCurrentAppFrame

INT iMPVehicleBlock
INT iMPCarAppSlot
INT iMPSavedVehicleSlot
BOOL bPriorityUpdate

BOOL bCheckPlayerNameDiffSP = TRUE
BOOL bCheckPlayerNameDiffMP = TRUE
BOOL bResendAppDataForAccountLinkSP = FALSE
BOOL bResendAppDataForAccountLinkMP = FALSE
BOOL bWasLinkedLastFrame = TRUE
BOOL bResendAppDataForCharacter0 = FALSE
BOOL bResendAppDataForCharacter1 = FALSE

BOOL bDeletingCarData
INT iDeleteCarDataControl

BOOL bBlockFrameUpdate

BOOL bProcessModUnlocks
INT iModUnlockStage
MODEL_NAMES eModUnlockModel
TEXT_LABEL_23 sModUnlockBlockName
TEXT_LABEL_23 sModUnlockVehicleBlockName

BOOL bChopDeathProcessed

BOOL bRefreshAppDataOnInit

BOOL bSaveCarData

INT iSCProfanityToken
INT iSCLicensePlateToken
INT iSCTextCheckStage

INT iInitialOrderCheckTimer
BOOL bProcessOrderNow[3]
BOOL bProcessOrderOnPlayerVehicle[3]
BOOL bInitialOrderCheckComplete[3]
BOOL bProcessMPOrderNow
BOOL bPreProcessOrderDone
BOOL bCheckOrderCost

TEXT_LABEL_15 tlProfanityPlate
INT iAmbProfanityToken
INT iProfanityPlateBack
BOOL bProcessedProfanityChecks

BOOL bInitialPassComplete

INT iMPLockCheck

INT iModUnlockCheck
INT iModColourUnlockCheck
INT iCached_UnlockBitset[3]
INT iCarColoursProcessed = 0
INT iCarColoursUnlocked[6]
INT iCached_CarColoursUnlocked[6]
INT iWheelVariation
TIME_DATATYPE tdFetchTimer
BOOL bFetchTimerSet

STRUCT_MP_PRESENCE_EVENT_VARS socialFeedVars


SCRIPT_TIMER QualPlaylistTimer

#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
	BOOL bRebuildPlatesList
#ENDIF

/// PURPOSE: Returns TRUE if the vehicle is of the correct type to use and in a suitable state
FUNC BOOL IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(MODEL_NAMES eVehicleModel)

	IF eVehicleModel = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF

	// Service vehicles
	SWITCH eVehicleModel
		CASE POLICE
		CASE POLICEOLD1
		CASE POLICEOLD2
		CASE POLICE2
		CASE POLICE3
		CASE POLICE4
		CASE FBI
		CASE FBI2
		CASE POLMAV
		CASE POLICEB
		CASE POLICET
		CASE RIOT
		CASE SHERIFF
		CASE PRANGER
		CASE  AMBULANCE
		CASE FIRETRUK
		CASE TAXI
		CASE LGUARD
		CASE RIPLEY
		CASE DILETTANTE2
		CASE AIRBUS
		CASE AIRTUG
		// Vans
		CASE BURRITO
		CASE RUMPO2
		CASE SPEEDO
		CASE SPEEDO2
		CASE SCORCHER
		CASE BMX
		CASE CRUISER
		CASE FIXTER
		CASE CADDY
		CASE FORKLIFT
		CASE CADDY2
		CASE CRUSADER
		CASE TRIBIKE
		CASE TRIBIKE2
		CASE TRIBIKE3
		CASE TRACTOR
		CASE TRACTOR2
		CASE MOWER
		CASE TORNADO4
		CASE DOCKTUG
		CASE STRETCH
		CASE BISON2
		CASE BISON3
		CASE BENSON
		CASE POUNDER
		CASE SUBMERSIBLE
		CASE MONSTER
		CASE TECHNICAL
		CASE PHANTOM2
		CASE WASTELANDER
		CASE BOXVILLE5
		CASE TERBYTE
		CASE SPEEDO4
		CASE MULE4
		CASE POUNDER2
		CASE KOSATKA
		CASE DELUXO
			RETURN FALSE
		BREAK
		DEFAULT
			IF IS_VEHICLE_AN_ARENA_CONTENDER_VEHICLE(eVehicleModel)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	// Planes/Helis
	IF IS_THIS_MODEL_A_HELI(eVehicleModel)
	OR IS_THIS_MODEL_A_PLANE(eVehicleModel)
		RETURN FALSE
	ENDIF
	
	// Nothing failed so must be safe
	RETURN TRUE
ENDFUNC


#IF IS_DEBUG_BUILD
    
    BOOL bBlockInit_Debug, bResetController_Debug

    BOOL bDebug_FailForLinkedAcccount
    BOOL bDebug_FailForGamemode
    
    BOOL bClearSPData
    BOOL bClearMPData
    
    BOOL bDeleteDataDebug
    INT iDeleteDataSlotDebug
    
	BOOL bSetProfanitPlate
    
	BOOL bSetFakePlate
    
    BOOL bDoProfanityCheck
	
	BOOL bAlwaysAllowAutoProcess = FALSE
    
	BOOL bObtain_all_coupon, bRedeem_all_coupon, bCouponObtained[MAX_COUPONS]
	BOOL bRefresh_coupon_status
	
	BOOL bRunPlateChecks
	
	BOOL bForceMPWipe
	BOOL bForceSPWipe
	
	BOOL bGenerateStaticData
	BOOL bDebugOpen
	INT iVehicleModelIndex
	
	BOOL bSendCurrentVehDataToApp
	
//	BOOL bFetchingData
	
	
	TEXT_WIDGET_ID  widgetFolderName
	TEXT_WIDGET_ID  widgetShotName
	STRING			sFolderName  = "X:/GTA5/"
	STRING			sShotName    = "testShot"
	STRUCT SCREENSHOT_STRUCT
		INT	iScreenShot  = 0
		
		VECTOR vCamCoord, vCamRot		
		FLOAT fCamFOV
		
		FLOAT fCamNearOutOfFocusPlane, fCamNearInFocusPlane, fCamFarInFocusPlane, fCamFarOutOfFocusPlane
		FLOAT fCamNearClip, fCamFarClip
		
		BOOL bTakeScreenshot
		BOOL bTestScreenshot
		INT iRenderingCam = 0
	ENDSTRUCT
	SCREENSHOT_STRUCT sScreenshot
	
	PROC SETUP_SOCIAL_CONTROL_WIDGETS()
        START_WIDGET_GROUP("Social Controller")
		
			START_WIDGET_GROUP("Processng vars")
				ADD_WIDGET_BOOL("g_bPopulateEmptyAppSlots", g_bPopulateEmptyAppSlots)
				ADD_WIDGET_INT_SLIDER("iStaggeredMaxFramesAppVehicles", g_sMPTunables.iStaggeredMaxFramesAppVehicles, 0, 1000, 1)
				ADD_WIDGET_INT_SLIDER("iStaggeredMaxFramesAppOrders", g_sMPTunables.iStaggeredMaxFramesAppOrders, 0, 1000, 1)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Generate static app data", bGenerateStaticData)
			ADD_WIDGET_INT_READ_ONLY("Vehicle model index", iVehicleModelIndex)
		
			ADD_WIDGET_BOOL("Run plate checks", bRunPlateChecks)
			
			ADD_WIDGET_BOOL("Rebuild plate list", bRebuildPlatesList)
			
			ADD_WIDGET_BOOL("Alwys do auto process", bAlwaysAllowAutoProcess)
			
            ADD_WIDGET_BOOL("MP 5 min window open", g_b5MinWindowOpen)
            ADD_WIDGET_BOOL("Block init", bBlockInit_Debug)
            ADD_WIDGET_BOOL("Reset controller", bResetController_Debug)
            
            ADD_WIDGET_BOOL("Clear SP data", bClearSPData)
            ADD_WIDGET_BOOL("Clear MP data", bClearMPData)
			
			ADD_WIDGET_BOOL("Force MP wipe", bForceMPWipe)
			ADD_WIDGET_BOOL("Force SP wipe", bForceSPWipe)
			
			ADD_WIDGET_INT_READ_ONLY("Inbox Processing Stage", socialFeedVars.iInboxProcessStage)
            
            ADD_WIDGET_INT_READ_ONLY("Delete car data stage", iDeleteCarDataControl)
            START_WIDGET_GROUP("Car App")
                ADD_WIDGET_BOOL("Run profanity check", bDoProfanityCheck)
                ADD_WIDGET_BOOL("Set Fake Plate", bSetFakePlate)
				ADD_WIDGET_BOOL("Set Profanit Plate", bSetProfanitPlate)
                ADD_WIDGET_INT_SLIDER("Vehicle slot", iDeleteDataSlotDebug, 0, MAX_MP_SAVED_VEHICLES-1, 1)
				ADD_WIDGET_BOOL("Send data (current vehicle)", bSendCurrentVehDataToApp)
				ADD_WIDGET_BOOL("Delete data", bDeleteDataDebug)
			STOP_WIDGET_GROUP()
            START_WIDGET_GROUP("Dog App")
                ADD_WIDGET_FLOAT_READ_ONLY("Happiness", g_savedGlobals.sSocialData.sDogAppData.fHappiness)
                ADD_WIDGET_FLOAT_READ_ONLY("Cleanliness", g_savedGlobals.sSocialData.sDogAppData.fCleanliness)
                ADD_WIDGET_FLOAT_READ_ONLY("Hunger", g_savedGlobals.sSocialData.sDogAppData.fHunger)
                ADD_WIDGET_INT_SLIDER("Training Level", g_savedGlobals.sSocialData.sDogAppData.iTrainingLevel, 0, 3, 1)
                ADD_WIDGET_INT_SLIDER("Collar", g_savedGlobals.sSocialData.sDogAppData.iCollar, 0, 5, 1)
            STOP_WIDGET_GROUP()
			
	        START_WIDGET_GROUP("Coupons")
				ADD_WIDGET_BOOL("bObtain_all_coupon", bObtain_all_coupon)
				ADD_WIDGET_BOOL("bRedeem_all_coupon", bRedeem_all_coupon)
				ADD_WIDGET_BOOL("bRefresh_coupon_status", bRefresh_coupon_status)
				
				COUPON_TYPE eC
				REPEAT MAX_COUPONS eC
					ADD_WIDGET_BOOL(GET_COUPON_STRING(eC), bCouponObtained[eC])
				ENDREPEAT
				
				ADD_WIDGET_INT_READ_ONLY("SP iSaveCoupons", g_savedGlobals.sFinanceData.iSaveCoupons)
				ADD_WIDGET_INT_READ_ONLY("MP iSaveCoupons", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons)
	        STOP_WIDGET_GROUP()
			
	        START_WIDGET_GROUP("Take Screenshot")
				widgetFolderName = ADD_TEXT_WIDGET("Folder Name")
				widgetShotName   = ADD_TEXT_WIDGET("Screenshot Name")
				SET_CONTENTS_OF_TEXT_WIDGET(widgetFolderName, sFolderName)
				SET_CONTENTS_OF_TEXT_WIDGET(widgetShotName, sShotName)
				ADD_WIDGET_BOOL("bTakeScreenshot", sScreenshot.bTakeScreenshot)
				ADD_WIDGET_BOOL("bTestScreenshot", sScreenshot.bTestScreenshot)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("GAMEPLAY RENDERING")
					ADD_TO_WIDGET_COMBO("DEBUG RENDERING")
					ADD_TO_WIDGET_COMBO("SCRIPT CAM RENDERING")
					ADD_TO_WIDGET_COMBO("XXX_3")
					ADD_TO_WIDGET_COMBO("XXX_4")
					ADD_TO_WIDGET_COMBO("XXX_5")
					ADD_TO_WIDGET_COMBO("XXX_6")
					ADD_TO_WIDGET_COMBO("XXX_7")
				STOP_WIDGET_COMBO("iRenderingCam", sScreenshot.iRenderingCam)
				
				ADD_WIDGET_STRING("Cam Params")
				ADD_WIDGET_VECTOR_SLIDER("vCamCoord",	sScreenshot.vCamCoord,	-9999, 9999, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vCamRot",		sScreenshot.vCamRot,	-9999, 9999, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fCamFOV",		sScreenshot.fCamFOV,	-9999, 9999, 0.1)
				
				ADD_WIDGET_STRING("Near Out Of Focus Plane")
				ADD_WIDGET_FLOAT_SLIDER("fCamNearOutOfFocusPlane",	sScreenshot.fCamNearOutOfFocusPlane,	-9999, 9999, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fCamNearInFocusPlane",		sScreenshot.fCamNearInFocusPlane,		-9999, 9999, 0.1)
				
				ADD_WIDGET_STRING("Far Out Of Focus Plane")
				ADD_WIDGET_FLOAT_SLIDER("fCamFarInFocusPlane",		sScreenshot.fCamFarInFocusPlane,		-9999, 9999, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fCamFarOutOfFocusPlane",	sScreenshot.fCamFarOutOfFocusPlane,		-9999, 9999, 0.1)
				
				ADD_WIDGET_STRING("Near/Far Clip")
				ADD_WIDGET_FLOAT_SLIDER("fCamNearClip",	sScreenshot.fCamNearClip,	-9999, 9999, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("fCamFarClip",	sScreenshot.fCamFarClip,	-9999, 9999, 0.1)
	        STOP_WIDGET_GROUP()
			
			
			
        STOP_WIDGET_GROUP()
    ENDPROC
	
	PROC MAINTAIN_SOCIAL_CONTROL_WIDGETS()
	
//		IF NETWORK_IS_GAME_IN_PROGRESS()
//			IF bFetchingData
//				DRAW_RECT(0.5, 0.1, 0.1, 0.05, 255, 255, 0, 255)
//			ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdFetchTimer)) < 1500
//				FLOAT fCooldownPerc = TO_FLOAT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdFetchTimer))) / 1500.0
//				DRAW_RECT(0.5, 0.1, 0.1-(0.1*fCooldownPerc), 0.05, 0, 128, 255, 255)
//			ENDIF
//		ENDIF
	
		IF bSendCurrentVehDataToApp
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
				IF DOES_ENTITY_EXIST(vehID)
				AND IS_VEHICLE_DRIVEABLE(vehID)
					IF NETWORK_IS_GAME_IN_PROGRESS()
						MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(vehID, iDeleteDataSlotDebug, TRUE, FALSE, TRUE, TRUE, TRUE)
					ELSE
						UPDATE_PLAYER_PED_SAVED_VEHICLE(GET_CURRENT_PLAYER_PED_ENUM(), vehID, 0, TRUE)
					ENDIF
				ENDIF
			ENDIF
			bSendCurrentVehDataToApp = FALSE
		ENDIF
		
		IF bGenerateStaticData
			
			OPEN_DEBUG_FILE()
			bDebugOpen = TRUE
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("PROC GET_STATIC_CAR_APP_DATA_FOR_VEHICLE(MODEL_NAMES eModel, STATIC_CAR_APP_DATA &sData)")SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	INT iModel = ENUM_TO_INT(eModel)")SAVE_NEWLINE_TO_DEBUG_FILE()
				
				INT iSwitchReset = -1
				MODEL_NAMES eReturnVehicleModelHashKey
				VEHICLE_INDEX tempVeh
				
				iVehicleModelIndex = 0
				
				WHILE GET_VEHICLE_MODEL_FROM_INDEX(iVehicleModelIndex, eReturnVehicleModelHashKey)
				
					IF IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(eReturnVehicleModelHashKey)
					
						REQUEST_MODEL(eReturnVehicleModelHashKey)
						WHILE NOT HAS_MODEL_LOADED(eReturnVehicleModelHashKey)
							REQUEST_MODEL(eReturnVehicleModelHashKey)
							WAIT(0)
						ENDWHILE
						
						tempVeh = CREATE_VEHICLE(eReturnVehicleModelHashKey, <<0, 0, 500>>, 0.0, FALSE, FALSE)
						
						IF DOES_ENTITY_EXIST(tempVeh)
						AND IS_VEHICLE_DRIVEABLE(tempVeh)
						AND GET_NUM_MOD_KITS(tempVeh) > 0
						
						
							IF iSwitchReset = 0
								SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("	SWITCH iModel")SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
							IF iSwitchReset = -1
								SAVE_STRING_TO_DEBUG_FILE("	SWITCH iModel")SAVE_NEWLINE_TO_DEBUG_FILE()
								iSwitchReset++
							ENDIF
							iSwitchReset++
							IF iSwitchReset > 25
								WAIT(0)
								iSwitchReset = 0
								DOES_ENTITY_EXIST(tempVeh)
								IS_VEHICLE_DRIVEABLE(tempVeh)
							ENDIF
							
							SET_VEHICLE_MOD_KIT(tempVeh, 0)
							
							SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eReturnVehicleModelHashKey))SAVE_STRING_TO_DEBUG_FILE(" // ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(eReturnVehicleModelHashKey))SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountEngine = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_ENGINE)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountBrakes = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_BRAKES)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountExhaust = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_EXHAUST)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountArmour = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_ARMOUR)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountHorn = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_HORN)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModCountSuspension = ")SAVE_INT_TO_DEBUG_FILE(GET_NUM_VEHICLE_MODS(tempVeh, MOD_SUSPENSION)+1)SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.fModPriceModifier = ")SAVE_FLOAT_TO_DEBUG_FILE(GET_VEHICLE_MOD_PRICE_MODIFIER(tempVeh))SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModColoursThatCanBeSet = ")SAVE_INT_TO_DEBUG_FILE(GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(tempVeh))SAVE_NEWLINE_TO_DEBUG_FILE()
								SAVE_STRING_TO_DEBUG_FILE("			sData.iModKitType = ")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_VEHICLE_MOD_KIT_TYPE(tempVeh)))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
						DELETE_VEHICLE(tempVeh)
						SET_MODEL_AS_NO_LONGER_NEEDED(eReturnVehicleModelHashKey)
					ENDIF
					
					iVehicleModelIndex++
					
					IF NOT bGenerateStaticData
						iVehicleModelIndex = 9999
					ENDIF
				ENDWHILE
				
				IF iSwitchReset != 0
					SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("ENDPROC")SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			bDebugOpen = FALSE
			
			bGenerateStaticData = FALSE
		ELSE
			IF bDebugOpen
				CLOSE_DEBUG_FILE()
				bDebugOpen = FALSE
			ENDIF
		ENDIF
        IF bDoProfanityCheck
            bProcessedProfanityChecks = FALSE
            g_savedGlobals.sSocialData.tlCarAppPlateText = "CUMFACE"
            g_savedGlobals.sSocialData.bCarAppPlateSet = TRUE
            bDoProfanityCheck = FALSE
        ENDIF
        IF bSetFakePlate
            g_savedGlobals.sSocialData.bCarAppPlateSet = TRUE
            g_savedGlobals.sSocialData.tlCarAppPlateText = "KEK SP"
            g_savedGlobals.sSocialData.iCarAppPlateBack = 1
            
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bCarAppPlateSet = TRUE
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.tlCarAppPlateText = "KEK MP"
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iCarAppPlateBack = 1
            
            bSetFakePlate = FALSE
        ENDIF
		IF bSetProfanitPlate
			
			SET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_0, 5)
			SET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_1, 20)
			SET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_2, 2)
			SET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_3, 10)
			
			g_tlPlateTextForSCAccount[0] = "fuck"
			
			g_bAddPlateTextToSC = TRUE
			bRebuildPlatesList = FALSE
	
			bSetProfanitPlate = FALSE
		ENDIF	
        IF bDeleteDataDebug
            IF NETWORK_IS_GAME_IN_PROGRESS()
                SET_BIT(g_MpSavedVehicles[iDeleteDataSlotDebug].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
            ELSE
                IF iDeleteDataSlotDebug >= COUNT_OF(g_savedGlobals.sSocialData.sCarAppData)
                    iDeleteDataSlotDebug = COUNT_OF(g_savedGlobals.sSocialData.sCarAppData)-1
                ENDIF
                g_savedGlobals.sSocialData.sCarAppData[iDeleteDataSlotDebug].bDeleteData = TRUE
            ENDIF
            bDeleteDataDebug = FALSE
        ENDIF
        IF bClearSPData
            g_savedGlobals.sSocialData.bSingleplayeDataWiped = FALSE
            bClearSPData = FALSE
        ENDIF
        IF bClearMPData
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bMultiplayerDataWiped = FALSE
            bClearMPData = FALSE
        ENDIF
		
		COUPON_TYPE eC
		IF bObtain_all_coupon
		OR IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_CTRL, "Coupon command line script debug trigger - obtain all (keyboard)")
			IF bObtain_all_coupon
				CPRINTLN(DEBUG_SOCIAL,"Coupon command line script debug trigger - obtain all (widget)")
			ELSE
				CPRINTLN(DEBUG_SOCIAL,"Coupon command line script debug trigger - obtain all (keyboard)")
			ENDIF
			
			REPEAT MAX_COUPONS eC
				OBTAIN_COUPON(eC) 
			ENDREPEAT
			
			bObtain_all_coupon = FALSE
		ENDIF
		IF bRedeem_all_coupon
			CPRINTLN(DEBUG_SOCIAL,"Coupon command line script debug trigger - redeem all")
			REPEAT MAX_COUPONS eC
				REDEEM_COUPON(eC) 
			ENDREPEAT
			
			bRedeem_all_coupon = FALSE
		ENDIF
		IF bRefresh_coupon_status
			REPEAT MAX_COUPONS eC
				IF NOT HAS_IMPORTANT_STATS_LOADED()
				AND (eC = COUPON_CASINO_PLANE_SITE
					OR eC = COUPON_CASINO_BOAT_SITE
					OR eC = COUPON_CASINO_CAR_SITE
					OR eC = COUPON_CASINO_CAR_SITE2
					OR eC = COUPON_CASINO_MIL_SITE
					OR eC = COUPON_CASINO_BIKE_SITE
					#IF FEATURE_GEN9_EXCLUSIVE
					OR IS_THIS_HSW_MOD_COUPON(eC)
					OR IS_THIS_HSW_UPGRADE_COUPON(eC)
					#ENDIF	)
					//
				ELSE
					bCouponObtained[eC] = DOES_SAVE_HAVE_COUPON(eC) 
				ENDIF
			ENDREPEAT
			bRefresh_coupon_status = FALSE
		ENDIF
		
		IF sScreenshot.bTakeScreenshot
		OR sScreenshot.bTestScreenshot
			
			IF IS_VECTOR_ZERO(sScreenshot.vCamCoord)
			AND IS_VECTOR_ZERO(sScreenshot.vCamRot)
			AND (sScreenshot.fCamFOV	= 0)
				sScreenshot.vCamCoord	= GET_CAM_COORD(GET_RENDERING_CAM())
				sScreenshot.vCamRot		= GET_CAM_ROT(GET_RENDERING_CAM())
				sScreenshot.fCamFOV		= GET_CAM_FOV(GET_RENDERING_CAM())
			ENDIF
			
		//	FLOAT fCamDOFStrength = GET_CAM_DOF_STRENGTH(GET_RENDERING_CAM())
			
			IF (sScreenshot.fCamNearOutOfFocusPlane	= 0)
			AND (sScreenshot.fCamNearInFocusPlane	= 0)
			AND (sScreenshot.fCamFarInFocusPlane	= 0)
			AND (sScreenshot.fCamFarOutOfFocusPlane	= 0)
				GET_CAM_DOF_PLANES(GET_RENDERING_CAM(), sScreenshot.fCamNearOutOfFocusPlane, sScreenshot.fCamNearInFocusPlane, sScreenshot.fCamFarInFocusPlane, sScreenshot.fCamFarOutOfFocusPlane)
			ENDIF
			
			IF sScreenshot.fCamNearClip = 0
				sScreenshot.fCamNearClip = GET_CAM_NEAR_CLIP(GET_RENDERING_CAM())
			ENDIF
			IF sScreenshot.fCamFarClip = 0
				sScreenshot.fCamFarClip = GET_CAM_FAR_CLIP(GET_RENDERING_CAM())
			ENDIF
			
			CAMERA_INDEX camClone = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sScreenshot.vCamCoord, sScreenshot.vCamRot, sScreenshot.fCamFOV)
			SET_CAM_DOF_PLANES(camClone, sScreenshot.fCamNearOutOfFocusPlane, sScreenshot.fCamNearInFocusPlane, sScreenshot.fCamFarInFocusPlane, sScreenshot.fCamFarOutOfFocusPlane)
			
			SET_CAM_NEAR_CLIP(camClone, sScreenshot.fCamNearClip)
			SET_CAM_FAR_CLIP(camClone, sScreenshot.fCamFarClip)
			
			INTERIOR_INSTANCE_INDEX InteriorInstanceIndex
			INT RoomKey
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				InteriorInstanceIndex = GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(PLAYER_ID()))
				RoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
			ENDIF
			
			SET_CAM_ACTIVE(camClone, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE, 0)
			WAIT(0)
		
			OBJECT_INDEX objID
			WHILE sScreenshot.bTestScreenshot
				SET_CAM_COORD(GET_RENDERING_CAM(), sScreenshot.vCamCoord)
				SET_CAM_ROT(GET_RENDERING_CAM(), sScreenshot.vCamRot)
				SET_CAM_FOV(GET_RENDERING_CAM(), sScreenshot.fCamFOV)
			
				SET_CAM_DOF_PLANES(camClone, sScreenshot.fCamNearOutOfFocusPlane, sScreenshot.fCamNearInFocusPlane, sScreenshot.fCamFarInFocusPlane, sScreenshot.fCamFarOutOfFocusPlane)
				
				SET_CAM_NEAR_CLIP(camClone, sScreenshot.fCamNearClip)
				SET_CAM_FAR_CLIP(camClone, sScreenshot.fCamFarClip)
				
				IF NOT DOES_ENTITY_EXIST(objID)
					MODEL_NAMES modelBag = PROP_GOLF_BALL
					REQUEST_MODEL(modelBag)
					IF HAS_MODEL_LOADED(modelBag)
						objID = CREATE_OBJECT(modelBag, sScreenshot.vCamCoord)
						SET_ENTITY_ROTATION(objID, sScreenshot.vCamRot)
						FREEZE_ENTITY_POSITION(objID, TRUE)
						SET_ENTITY_VISIBLE(objID, FALSE)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(modelBag)
					ENDIF
				ELSE
					VECTOR vSphereG = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objID, <<0,(sScreenshot.fCamNearClip+0.75),0>>)
					DRAW_DEBUG_SPHERE(vSphereG, 1.0, 000,255,000,255)
				ENDIF
				
				FORCE_ROOM_FOR_GAME_VIEWPORT(InteriorInstanceIndex, RoomKey)
				
				IF IS_GAMEPLAY_CAM_RENDERING()
					sScreenshot.iRenderingCam = 0	//	ADD_TO_WIDGET_COMBO("GAMEPLAY RENDERING")
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
						sScreenshot.vCamCoord	= GET_GAMEPLAY_CAM_COORD()
						sScreenshot.vCamRot		= GET_GAMEPLAY_CAM_ROT()
						sScreenshot.fCamFOV		= GET_GAMEPLAY_CAM_FOV()
					ENDIF
				ENDIF
				IF IS_CAM_RENDERING(GET_DEBUG_CAM())
					STRING sLiteral = "DEBUG RENDERING"
					HUD_COLOURS eColour = HUD_COLOUR_PURPLE
					
					INT red = 0, green = 0, blue = 0, alpha_value = 0
					GET_HUD_COLOUR(eColour, red, green, blue, alpha_value)
					
				//	SET_TEXT_SCALE(0.45, 0.45)
					SET_TEXT_COLOUR(red, green, blue, alpha_value)
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.5, "STRING", sLiteral)
					sScreenshot.iRenderingCam = 1	//	ADD_TO_WIDGET_COMBO("DEBUG RENDERING")
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
						sScreenshot.vCamCoord	= GET_CAM_COORD(GET_RENDERING_CAM())
						sScreenshot.vCamRot		= GET_CAM_ROT(GET_RENDERING_CAM())
						sScreenshot.fCamFOV		= GET_CAM_FOV(GET_RENDERING_CAM())
					ENDIF
				ENDIF
				IF IS_CAM_RENDERING(camClone)
					sScreenshot.iRenderingCam = 2	//	ADD_TO_WIDGET_COMBO("SCRIPT CAM RENDERING")
				ENDIF
				
				IF sScreenshot.bTakeScreenshot
					sScreenshot.bTestScreenshot = FALSE
				ENDIF
				
				WAIT(0)
			ENDWHILE
			IF DOES_ENTITY_EXIST(objID)
				DELETE_OBJECT(objID)
			ENDIF
		
			// Check widget for screenshot name
			sFolderName = GET_CONTENTS_OF_TEXT_WIDGET(widgetFolderName)
			sShotName   = GET_CONTENTS_OF_TEXT_WIDGET(widgetShotName)
			
			// Build screenshot name and directory
			TEXT_LABEL_63 sTemp = sFolderName
			sTemp += sShotName
			IF sScreenshot.iScreenShot < 10
				sTemp += "_00"
			ELIF sScreenshot.iScreenShot < 100
				sTemp += "_0"
			ELSE
				sTemp += "_"
			ENDIF
			sTemp += sScreenshot.iScreenShot
			
			// Take screenshot
			SAVE_SCREENSHOT(sTemp)
			sScreenshot.iScreenShot++
			
			IF DOES_CAM_EXIST(camClone)
				IF IS_CAM_ACTIVE(camClone)
					SET_CAM_ACTIVE(camClone, FALSE)
				ENDIF
				DESTROY_CAM(camClone)
			ENDIF
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			sScreenshot.bTakeScreenshot = FALSE
			sScreenshot.bTestScreenshot = FALSE
		ENDIF
    ENDPROC
#ENDIF

// ===========================================================================================================
//      Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()
    // CURRENTLY NOTHING TO CLEANUP
    TERMINATE_THIS_THREAD()
ENDPROC



FUNC STRING GET_APP_ATTRIBUTE_INT(STRING sAttribute)
    
    INT iAttributeHash =  GET_HASH_KEY(sAttribute)
    
    IF GET_HASH_KEY("uid") = iAttributeHash                                 RETURN "0" ENDIF
    IF GET_HASH_KEY("carModel") = iAttributeHash                            RETURN "1" ENDIF
    IF GET_HASH_KEY("playerSlot") = iAttributeHash                          RETURN "2" ENDIF
    IF GET_HASH_KEY("playerRank") = iAttributeHash                          RETURN "3" ENDIF
    IF GET_HASH_KEY("playerGangR") = iAttributeHash                         RETURN "4" ENDIF
    IF GET_HASH_KEY("playerGangG") = iAttributeHash                         RETURN "5" ENDIF
    IF GET_HASH_KEY("playerGangB") = iAttributeHash                         RETURN "6" ENDIF
    IF GET_HASH_KEY("carColour1") = iAttributeHash                          RETURN "7" ENDIF
    IF GET_HASH_KEY("carColour2") = iAttributeHash                          RETURN "8" ENDIF
    IF GET_HASH_KEY("carPlateBack") = iAttributeHash                        RETURN "9" ENDIF
    IF GET_HASH_KEY("windowTint") = iAttributeHash                          RETURN "10" ENDIF
    IF GET_HASH_KEY("bulletProofTyres") = iAttributeHash                    RETURN "11" ENDIF
    IF GET_HASH_KEY("carEngine") = iAttributeHash                           RETURN "12" ENDIF
    IF GET_HASH_KEY("carBrakes") = iAttributeHash                           RETURN "13" ENDIF
    IF GET_HASH_KEY("carExhaust") = iAttributeHash                          RETURN "14" ENDIF
    IF GET_HASH_KEY("carWheel") = iAttributeHash                            RETURN "15" ENDIF
    IF GET_HASH_KEY("carHorn1") = iAttributeHash                            RETURN "16" ENDIF
    IF GET_HASH_KEY("carHorn2") = iAttributeHash                            RETURN "17" ENDIF
    IF GET_HASH_KEY("carHorn3") = iAttributeHash                            RETURN "18" ENDIF
    IF GET_HASH_KEY("carHorn4") = iAttributeHash                            RETURN "19" ENDIF
    IF GET_HASH_KEY("carHorn5") = iAttributeHash                            RETURN "20" ENDIF
    IF GET_HASH_KEY("carHorn6") = iAttributeHash                            RETURN "21" ENDIF
    IF GET_HASH_KEY("carHorn7") = iAttributeHash                            RETURN "22" ENDIF
    IF GET_HASH_KEY("carHorn8") = iAttributeHash                            RETURN "23" ENDIF
    IF GET_HASH_KEY("carHorn9") = iAttributeHash                            RETURN "24" ENDIF
    IF GET_HASH_KEY("tyreSmoke") = iAttributeHash                           RETURN "25" ENDIF
    IF GET_HASH_KEY("tyreSmokeColourRed") = iAttributeHash                  RETURN "26" ENDIF
    IF GET_HASH_KEY("tyreSmokeColourGreen") = iAttributeHash                RETURN "27" ENDIF
    IF GET_HASH_KEY("tyreSmokeColourBlue") = iAttributeHash                 RETURN "28" ENDIF
    IF GET_HASH_KEY("carHorn") = iAttributeHash                             RETURN "29" ENDIF
    IF GET_HASH_KEY("carArmour") = iAttributeHash                           RETURN "30" ENDIF
    IF GET_HASH_KEY("carTurbo") = iAttributeHash                            RETURN "31" ENDIF
    IF GET_HASH_KEY("carSuspension") = iAttributeHash                       RETURN "32" ENDIF
    IF GET_HASH_KEY("carXenonLights") = iAttributeHash                      RETURN "33" ENDIF
    IF GET_HASH_KEY("carWheelType") = iAttributeHash                        RETURN "34" ENDIF
    IF GET_HASH_KEY("carUnlocked") = iAttributeHash                         RETURN "35" ENDIF
    IF GET_HASH_KEY("carColour1Unlocked") = iAttributeHash                  RETURN "36" ENDIF
    IF GET_HASH_KEY("carColour2Unlocked") = iAttributeHash                  RETURN "37" ENDIF
    IF GET_HASH_KEY("tyreSmokeColourEnabled") = iAttributeHash              RETURN "38" ENDIF
    IF GET_HASH_KEY("carEngineCount") = iAttributeHash                      RETURN "39" ENDIF
    IF GET_HASH_KEY("carBrakesCount") = iAttributeHash                      RETURN "40" ENDIF
    IF GET_HASH_KEY("carExhaustCount") = iAttributeHash                     RETURN "41" ENDIF
    IF GET_HASH_KEY("carWheelCount") = iAttributeHash                       RETURN "42" ENDIF
    IF GET_HASH_KEY("carHornCount") = iAttributeHash                        RETURN "43" ENDIF
    IF GET_HASH_KEY("carArmourCount") = iAttributeHash                      RETURN "44" ENDIF
    IF GET_HASH_KEY("carSuspensionCount") = iAttributeHash                  RETURN "45" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked0") = iAttributeHash                 RETURN "46" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked1") = iAttributeHash                 RETURN "47" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked2") = iAttributeHash                 RETURN "48" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked3") = iAttributeHash                 RETURN "49" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked4") = iAttributeHash                 RETURN "50" ENDIF
    IF GET_HASH_KEY("carColoursUnlocked5") = iAttributeHash                 RETURN "51" ENDIF
    IF GET_HASH_KEY("carPriceModifier") = iAttributeHash                    RETURN "52" ENDIF
    IF GET_HASH_KEY("carType") = iAttributeHash                             RETURN "53" ENDIF
    IF GET_HASH_KEY("playerGang") = iAttributeHash                          RETURN "54" ENDIF
    IF GET_HASH_KEY("unlockBitset1") = iAttributeHash                       RETURN "55" ENDIF
    IF GET_HASH_KEY("unlockBitset2") = iAttributeHash                       RETURN "56" ENDIF
    
    IF GET_HASH_KEY("orderCount_sp0") = iAttributeHash                      RETURN "57" ENDIF
    IF GET_HASH_KEY("orderCount_sp1") = iAttributeHash                      RETURN "58" ENDIF
    IF GET_HASH_KEY("orderCount_sp2") = iAttributeHash                      RETURN "59" ENDIF
    IF GET_HASH_KEY("orderCount_mp0") = iAttributeHash                      RETURN "60" ENDIF
    IF GET_HASH_KEY("orderCount_mp1") = iAttributeHash                      RETURN "61" ENDIF
    IF GET_HASH_KEY("orderCount_mp2") = iAttributeHash                      RETURN "62" ENDIF
    IF GET_HASH_KEY("orderCount_mp3") = iAttributeHash                      RETURN "63" ENDIF
    IF GET_HASH_KEY("orderCount_mp4") = iAttributeHash                      RETURN "64" ENDIF
    
    IF GET_HASH_KEY("spDiscount") = iAttributeHash                          RETURN "65" ENDIF
    IF GET_HASH_KEY("mpDiscount") = iAttributeHash                          RETURN "66" ENDIF
    
    IF GET_HASH_KEY("carColour1Group") = iAttributeHash                     RETURN "67" ENDIF
    IF GET_HASH_KEY("carColour2Group") = iAttributeHash                     RETURN "68" ENDIF
    
    IF GET_HASH_KEY("unlockBitset3") = iAttributeHash                       RETURN "69" ENDIF

	IF GET_HASH_KEY("playerName") = iAttributeHash                       	RETURN "70" ENDIF
	
	IF GET_HASH_KEY("newItemUnlocks") = iAttributeHash                      RETURN "72" ENDIF
	
	IF GET_HASH_KEY("playerNameSP") = iAttributeHash                       	RETURN "73" ENDIF
	IF GET_HASH_KEY("playerNameMP") = iAttributeHash                       	RETURN "74" ENDIF
    
    RETURN sAttribute
ENDFUNC

PROC APP_SET_INT_DATA(STRING sAttribute, INT iValue)
    APP_SET_INT(GET_APP_ATTRIBUTE_INT(sAttribute), iValue)
ENDPROC

PROC APP_SET_FLOAT_DATA(STRING sAttribute, FLOAT fValue)
    APP_SET_FLOAT(GET_APP_ATTRIBUTE_INT(sAttribute), fValue)
ENDPROC

PROC APP_SET_STRING_DATA(STRING sAttribute, STRING sValue)
    APP_SET_STRING(GET_APP_ATTRIBUTE_INT(sAttribute), sValue)
ENDPROC

FUNC INT APP_GET_INT_DATA(STRING sAttribute)
    RETURN APP_GET_INT(GET_APP_ATTRIBUTE_INT(sAttribute))
ENDFUNC

FUNC STRING APP_GET_STRING_DATA(STRING sAttribute)
    RETURN APP_GET_STRING(GET_APP_ATTRIBUTE_INT(sAttribute))
ENDFUNC

FUNC INT GENERATE_RANDOM_UNIQUE_ID()
    INT iUID
    
    INT i
    REPEAT 32 i
        IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
            SET_BIT(iUID, i)
        ENDIF
    ENDREPEAT
    
    CPRINTLN(DEBUG_SOCIAL,"GENERATE_RANDOM_UNIQUE_ID = ", iUID)
    
    RETURN iUID
ENDFUNC


PROC DO_HIDE_VEHICLE_SLOT_IN_APP(INT iSlot)
	IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bCarHiddenInApp[iSlot]
        
		CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - hiding multiplayer vehicle data in slot ", iSlot)
        
        APP_SET_APP("car")
        
        TEXT_LABEL_15 tlBlockName = "multiplayer"
        tlBlockName += GET_ACTIVE_CHARACTER_SLOT()
        
        APP_SET_BLOCK(tlBlockName)
            tlBlockName = "vehicle"
            tlBlockName += iSlot
            APP_SET_BLOCK(tlBlockName)
                APP_SET_INT_DATA("carUnlocked", 0)
            APP_CLOSE_BLOCK()
            bSaveCarData = TRUE SETTIMERA(0)
        APP_CLOSE_BLOCK()
        
        APP_CLOSE_APP()
        
        g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bCarHiddenInApp[iSlot] = TRUE
    ENDIF
ENDPROC


/// PURPOSE: Clears all app data when we restart singleplayer and clears the car app data when an order has been processed
PROC DO_PROCESS_DELETE_DATA()

    INT iSaveGameArraySlot = GET_SAVE_GAME_ARRAY_SLOT()

    TEXT_LABEL_15 tlBlockName, sOrderBlockName

    IF NETWORK_IS_GAME_IN_PROGRESS()
    
        // New game so wipe all the data
        IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.bMultiplayerDataWiped
        
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - wiping multiplayer data")
            
            APP_SET_APP("car")
            
            tlBlockName = "multiplayer"
            tlBlockName += GET_ACTIVE_CHARACTER_SLOT()
            
            APP_SET_BLOCK(tlBlockName)
            
                INT iSlot
                REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iSlot
                    tlBlockName = "vehicle"
                    tlBlockName += iSlot
                    APP_SET_BLOCK(tlBlockName)
                        APP_SET_INT_DATA("carUnlocked", 0)
                    APP_CLOSE_BLOCK()
                ENDREPEAT
            APP_CLOSE_BLOCK()
            
            //APP_SET_BLOCK("plate")
                //APP_SET_INT_DATA("carPlateBack", 0)
                //APP_SET_STRING_DATA("carPlateText", "")
            //APP_CLOSE_BLOCK()
            
            APP_SET_BLOCK("appdata")
            TEXT_LABEL_23 tlAttribute
            tlAttribute = "orderCount_mp"
            tlAttribute += GET_ACTIVE_CHARACTER_SLOT()
            APP_SET_INT_DATA(tlAttribute, 0)
            APP_CLOSE_BLOCK()
            
            bSaveCarData = TRUE SETTIMERA(0)
            APP_CLOSE_APP()
            
            
            g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.bMultiplayerDataWiped = TRUE
        ENDIF
        
        // Asked to delete the current vehicle block data
		IF iMPSavedVehicleSlot >= 0
		AND IS_BIT_SET(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
		
			// Determine which vehicle block this is stored in.
			INT iBlock
			REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iBlock
				IF (g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.iProcessSlot[iBlock]-1) = iMPSavedVehicleSlot
				
					CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - deleting multiplayer vehicle ", iMPSavedVehicleSlot, " in block ", iBlock)
        			
					APP_SET_APP("car")
					
					tlBlockName = "multiplayer"
					tlBlockName += GET_ACTIVE_CHARACTER_SLOT()

					APP_SET_BLOCK(tlBlockName)
						tlBlockName = "vehicle"
						tlBlockName += iBlock
						APP_SET_BLOCK(tlBlockName)
							APP_SET_INT_DATA("carUnlocked", 0)
						APP_CLOSE_BLOCK()
						bSaveCarData = TRUE SETTIMERA(0)
					APP_CLOSE_BLOCK()

					APP_CLOSE_APP()

					CLEAR_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
					CLEAR_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
				ENDIF
			ENDREPEAT
			
		ENDIF
        
        // Order processed so delete car data
        SWITCH iDeleteCarDataControl
            CASE 0
                IF g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.bDeleteCarData
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - deleting car data from MP")
                    APP_DELETE_APP_DATA("car")
                    bDeletingCarData = TRUE
                    iDeleteCarDataControl++
                ELSE
                    bDeletingCarData = FALSE
                ENDIF
            BREAK
            CASE 1
                CPRINTLN(DEBUG_SOCIAL,"APP_GET_DELETED_FILE_STATUS() = ", ENUM_TO_INT(APP_GET_DELETED_FILE_STATUS()))
                IF APP_GET_DELETED_FILE_STATUS() != APP_FILE_STATUS_PENDING
                    // Done
                    IF APP_GET_DELETED_FILE_STATUS() != APP_FILE_STATUS_FAILED
                        g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.bDeleteCarData = FALSE
                    ENDIF
                    
                    APP_SET_APP("car")
                    
                        // Delete the order block.
                        sOrderBlockName = "mp"
                        sOrderBlockName += GET_ACTIVE_CHARACTER_SLOT()
                        sOrderBlockName += "_order"
                        APP_SET_BLOCK(sOrderBlockName)
                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - clearing app block: ", sOrderBlockName)
                            APP_CLEAR_BLOCK()   
                        APP_CLOSE_BLOCK()
                        
                        APP_SET_BLOCK("appdata")
                            TEXT_LABEL_23 tlAttribute
                            tlAttribute = "orderCount_mp"
                            tlAttribute += GET_ACTIVE_CHARACTER_SLOT()
                            APP_SET_INT_DATA(tlAttribute, g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.iOrderCount)
                        APP_CLOSE_BLOCK()
                    APP_CLOSE_APP()
                    
                    iDeleteCarDataControl = 0
                ENDIF
            BREAK
        ENDSWITCH
    ELSE
    
        // New game so wipe all the data
        IF NOT g_savedGlobals.sSocialData.bSingleplayeDataWiped
        
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - wiping singleplayer data")
            
            APP_SET_APP("car")
            
            APP_SET_BLOCK("singleplayer0")
            APP_SET_INT_DATA("carUnlocked", 0)
            APP_CLOSE_BLOCK()
            
//          APP_SET_BLOCK("singleplayer1")
//          APP_SET_INT_DATA("carUnlocked", 0)
//          APP_CLOSE_BLOCK()
            
            APP_SET_BLOCK("singleplayer2")
            APP_SET_INT_DATA("carUnlocked", 0)
            APP_CLOSE_BLOCK()
            
            //APP_SET_BLOCK("plate")
            //APP_SET_INT_DATA("carPlateBack", 0)
            //APP_SET_STRING_DATA("carPlateText", "")
            //APP_CLOSE_BLOCK()
            
            APP_SET_BLOCK("appdata")
            APP_SET_INT_DATA("orderCount_sp0", 0)
            APP_SET_INT_DATA("orderCount_sp1", 0)
            APP_SET_INT_DATA("orderCount_sp2", 0)
            APP_CLOSE_BLOCK()
            
            bSaveCarData = TRUE SETTIMERA(0)
            APP_CLOSE_APP()
            
            APP_SET_APP("dog")
            APP_SET_BLOCK("saveData")
            APP_SET_INT_DATA("chopSafeHouse", 0)
            APP_SAVE_DATA()
            APP_CLOSE_BLOCK()
            APP_CLOSE_APP()
            
            
            g_savedGlobals.sSocialData.bPlayerUnlockedInApp[0] = FALSE
            g_savedGlobals.sSocialData.bPlayerUnlockedInApp[1] = FALSE
            g_savedGlobals.sSocialData.bPlayerUnlockedInApp[2] = FALSE
            
            g_savedGlobals.sSocialData.bSingleplayeDataWiped = TRUE
        ENDIF
        
        // Asked to delete SP characters vehicle data
        INT iPed
        REPEAT NUM_OF_PLAYABLE_PEDS iPed
            IF g_savedGlobals.sSocialData.sCarAppData[iPed].bDeleteData
            
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - deleting singleplayer vehicle data in slot ", ENUM_TO_INT(iPed))
                
                APP_SET_APP("car")
                
                tlBlockName = "singleplayer"
                tlBlockName += ENUM_TO_INT(iPed)
                
                APP_SET_BLOCK(tlBlockName)
                APP_SET_INT_DATA("carUnlocked", 0)
                bSaveCarData = TRUE SETTIMERA(0)
                APP_CLOSE_BLOCK()
                
                APP_CLOSE_APP()
                
                g_savedGlobals.sSocialData.bPlayerUnlockedInApp[iPed] = FALSE
                g_savedGlobals.sSocialData.sCarAppData[iPed].bDeleteData = FALSE
                g_savedGlobals.sSocialData.sCarAppData[iPed].bSendDataToCloud = FALSE
                g_savedGlobals.sSocialData.sCarAppData[iPed].eModel = DUMMY_MODEL_FOR_SCRIPT
                g_savedGlobals.sSocialData.bPlayerUnlockedInApp[iPed] = FALSE
            ENDIF
        ENDREPEAT
        
        // Order processed so delete car data
        SWITCH iDeleteCarDataControl
            CASE 0
                IF g_savedGlobals.sSocialData.bDeleteCarData
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_DATA - deleting car data from SP")
                    APP_DELETE_APP_DATA("car")
                    bDeletingCarData = TRUE
                    iDeleteCarDataControl++
                ELSE
                    bDeletingCarData = FALSE
                ENDIF
            BREAK
            CASE 1
                CPRINTLN(DEBUG_SOCIAL,"APP_GET_DELETED_FILE_STATUS() = ", ENUM_TO_INT(APP_GET_DELETED_FILE_STATUS()))
                IF APP_GET_DELETED_FILE_STATUS() != APP_FILE_STATUS_PENDING
                    // Done
                    IF APP_GET_DELETED_FILE_STATUS() != APP_FILE_STATUS_FAILED
                        g_savedGlobals.sSocialData.bDeleteCarData = FALSE
                    ENDIF
                    
                    APP_SET_APP("car")
                    
                        // Delete the order block.
                        sOrderBlockName = "sp"
                        sOrderBlockName += g_savedGlobals.sSocialData.iOrderToDelete
                        sOrderBlockName += "_order"
                        APP_SET_BLOCK(sOrderBlockName)
                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - clearing app block: ", sOrderBlockName)
                            APP_CLEAR_BLOCK()   
                        APP_CLOSE_BLOCK()
                        
                        APP_SET_BLOCK("appdata")
                        APP_SET_INT_DATA("orderCount_sp0", g_savedGlobals.sSocialData.iOrderCount[0])
                        APP_SET_INT_DATA("orderCount_sp1", g_savedGlobals.sSocialData.iOrderCount[1])
                        APP_SET_INT_DATA("orderCount_sp2", g_savedGlobals.sSocialData.iOrderCount[2])
                        APP_CLOSE_BLOCK()
                    APP_CLOSE_APP()
                    
                    iDeleteCarDataControl = 0
                ENDIF
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

PROC RESEND_APP_DATA_FOR_CHARACTER(INT iSlot, BOOL bResendData)
	SWITCH iSlot
		CASE 0
			bResendAppDataForCharacter0 = bResendData
			SET_PACKED_STAT_BOOL(PACKED_MP_RESEND_APP_DATA_FOR_CHARACTER_0, bResendData)
		BREAK
		CASE 1
			bResendAppDataForCharacter1 = bResendData
			SET_PACKED_STAT_BOOL(PACKED_MP_RESEND_APP_DATA_FOR_CHARACTER_1, bResendData)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_RESEND_APP_DATA_FOR_CHARACTER(INT iSlot)
	SWITCH iSlot
		CASE 0
			RETURN bResendAppDataForCharacter0
		BREAK
		CASE 1
			RETURN bResendAppDataForCharacter1
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: Clears all multiplayer app data and resends any info
PROC DO_PROCESS_DELETE_ALL_DATA_FOR_NEW_ACCOUNT()

    IF NETWORK_IS_SIGNED_ONLINE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// Check if the stored playerName matches - if it doesn't, wipe all the data.
			
			BOOL bDeleteAllData = FALSE
			
			// Only check name difference once per boot or when we change SC account.
			BOOL bResendAppDataForNameChange = FALSE
			IF bCheckPlayerNameDiffMP
				APP_SET_APP("car")
					APP_SET_BLOCK("appdata")
						INT iStoredPlayerName = APP_GET_INT_DATA("playerNameMP")
						IF (iStoredPlayerName != 0 AND iStoredPlayerName != GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
							bResendAppDataForNameChange = TRUE
						ENDIF
					APP_CLOSE_BLOCK()
				APP_CLOSE_APP()
				bCheckPlayerNameDiffMP = FALSE
			ENDIF
			
			IF bResendAppDataForNameChange
			OR bResendAppDataForAccountLinkMP
			#IF IS_DEBUG_BUILD
			OR bForceMPWipe
				bForceMPWipe = FALSE
			#ENDIF
			
				APP_SET_APP("car")
					APP_SET_BLOCK("appdata")
						APP_SET_INT_DATA("playerNameMP", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
						
						bDeleteAllData = TRUE
						RESEND_APP_DATA_FOR_CHARACTER(0, TRUE)
						RESEND_APP_DATA_FOR_CHARACTER(1, TRUE)
						
						bResendAppDataForAccountLinkMP = FALSE
					APP_CLOSE_BLOCK()
					
					IF bDeleteAllData
					
						CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_ALL_DATA_FOR_NEW_ACCOUNT - wiping all multiplayer data")
						
						INT iMPSlot
						INT iVehSlot
						TEXT_LABEL_15 tlBlockName
						
						REPEAT 5 iMPSlot
							tlBlockName = "multiplayer"
							tlBlockName += iMPSlot
							APP_SET_BLOCK(tlBlockName)
								REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iVehSlot
				                    tlBlockName = "vehicle"
				                    tlBlockName += iVehSlot
				                    APP_SET_BLOCK(tlBlockName)
				                        APP_SET_INT_DATA("carUnlocked", 0)
				                    APP_CLOSE_BLOCK()
				                ENDREPEAT
				            APP_CLOSE_BLOCK()
						ENDREPEAT
					ENDIF
					
				APP_CLOSE_APP()
			ENDIF
		ELSE
			// Check if the stored playerName matches - if it doesn't, wipe all the data.
			
			BOOL bWipeMPData = FALSE
			
			// Only check name difference once per boot or when we change SC account.
			BOOL bResendAppDataForNameChange = FALSE
			IF bCheckPlayerNameDiffSP
				APP_SET_APP("car")
					APP_SET_BLOCK("appdata")
						INT iStoredPlayerName = APP_GET_INT_DATA("playerNameSP")
						IF (iStoredPlayerName != 0 AND iStoredPlayerName != GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
							bResendAppDataForNameChange = TRUE
						ENDIF
					APP_CLOSE_BLOCK()
				APP_CLOSE_APP()
				bCheckPlayerNameDiffSP = FALSE
			ENDIF
			
			IF bResendAppDataForNameChange
			OR bResendAppDataForAccountLinkSP
			#IF IS_DEBUG_BUILD
			OR bForceSPWipe
				bForceSPWipe = FALSE
			#ENDIF
			
				APP_SET_APP("car")
					APP_SET_BLOCK("appdata")
					
						CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_ALL_DATA_FOR_NEW_ACCOUNT - resend SP data")
						
						APP_SET_INT_DATA("playerNameSP", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
						bRefreshAppDataOnInit = TRUE
						bResendAppDataForAccountLinkSP = FALSE
						
						bWipeMPData = TRUE
						
					APP_CLOSE_BLOCK()
					
					IF bWipeMPData
						// Clear all MP data when we are in SP
						CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DELETE_ALL_DATA_FOR_NEW_ACCOUNT - wiping all multiplayer data from SP")
						
						INT iMPSlot
						INT iVehSlot
						TEXT_LABEL_15 tlBlockName
						
						REPEAT 5 iMPSlot
							tlBlockName = "multiplayer"
							tlBlockName += iMPSlot
							APP_SET_BLOCK(tlBlockName)
								REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iVehSlot
				                    tlBlockName = "vehicle"
				                    tlBlockName += iVehSlot
				                    APP_SET_BLOCK(tlBlockName)
				                        APP_SET_INT_DATA("carUnlocked", 0)
				                    APP_CLOSE_BLOCK()
				                ENDREPEAT
				            APP_CLOSE_BLOCK()
						ENDREPEAT
					ENDIF
					
				APP_CLOSE_APP()
			ENDIF
	    ENDIF
	ENDIF
ENDPROC

PROC GET_COLOUR_UNLOCKS_STAGE_1(INT &iUnlockBitset)
    
    iUnlockBitset = 0
    
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 0) ENDIF //tyreSmokeColourEnabled
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 1) ENDIF //windowTint0Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_LIGHT_SMOKE_WINDOWS)   SET_BIT(iUnlockBitset, 2) ENDIF //windowTint1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_DARK_SMOKE_WINDOWS)    SET_BIT(iUnlockBitset, 3) ENDIF //windowTint2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_LIMO_WINDOWS)          SET_BIT(iUnlockBitset, 4) ENDIF //windowTint3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_WHITE)      SET_BIT(iUnlockBitset, 5) ENDIF //tyreSmokeColourWhiteUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_ORANGE)     SET_BIT(iUnlockBitset, 6) ENDIF //tyreSmokeColourOrangeUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_RED)        SET_BIT(iUnlockBitset, 7) ENDIF //tyreSmokeColourRedUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_YELLOW)     SET_BIT(iUnlockBitset, 8) ENDIF //tyreSmokeColourYellowUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_BLUE)       SET_BIT(iUnlockBitset, 9) ENDIF //tyreSmokeColourBlueUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TYRE_SMOKE_BLACK)      SET_BIT(iUnlockBitset, 10) ENDIF //tyreSmokeColourBlackUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 11) ENDIF //carEngine1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L2)             SET_BIT(iUnlockBitset, 12) ENDIF //carEngine2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L3)             SET_BIT(iUnlockBitset, 13) ENDIF //carEngine3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L4)             SET_BIT(iUnlockBitset, 14) ENDIF //carEngine4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L5)             SET_BIT(iUnlockBitset, 15) ENDIF //carEngine5Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 16) ENDIF //carBrakes1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L2)             SET_BIT(iUnlockBitset, 17) ENDIF //carBrakes2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L3)             SET_BIT(iUnlockBitset, 18) ENDIF //carBrakes3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L4)             SET_BIT(iUnlockBitset, 19) ENDIF //carBrakes4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L5)             SET_BIT(iUnlockBitset, 20) ENDIF //carBrakes5Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 21) ENDIF //carExhaust1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_EXHAUST_L2)            SET_BIT(iUnlockBitset, 22) ENDIF //carExhaust2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 23) ENDIF //carWheel1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 24) ENDIF //carWheel2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 25) ENDIF //carWheel3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 26) ENDIF //carWheel4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 27) ENDIF //carWheel5Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 28) ENDIF //carWheel6Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BULLET_PROOF_TYRES)    SET_BIT(iUnlockBitset, 29) ENDIF //bulletProofTyresUnlocked
ENDPROC

PROC GET_COLOUR_UNLOCKS_STAGE_2(INT &iUnlockBitset)
    
    iUnlockBitset = 0
    
    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 0) ENDIF //carHorn1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_HORN_L2)               SET_BIT(iUnlockBitset, 1) ENDIF //carHorn2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_HORN_L3)               SET_BIT(iUnlockBitset, 2) ENDIF //carHorn3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_HORN_L4)               SET_BIT(iUnlockBitset, 3) ENDIF //carHorn4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_HORN_L5)               SET_BIT(iUnlockBitset, 4) ENDIF //carHorn5Unlocked

    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BODY_ARMOUR_20)        SET_BIT(iUnlockBitset, 5) ENDIF //carArmour1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BODY_ARMOUR_40)        SET_BIT(iUnlockBitset, 6) ENDIF //carArmour2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BODY_ARMOUR_60)        SET_BIT(iUnlockBitset, 7) ENDIF //carArmour3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BODY_ARMOUR_80)        SET_BIT(iUnlockBitset, 8) ENDIF //carArmour4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BODY_ARMOUR_100)       SET_BIT(iUnlockBitset, 9) ENDIF //carArmour5Unlocked

    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 10) ENDIF //carSuspension1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_SUSPENSION_L1)         SET_BIT(iUnlockBitset, 11) ENDIF //carSuspension2Unlocked

    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_XENON_LIGHTS)          SET_BIT(iUnlockBitset, 12) ENDIF //carXenonLightsUnlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TURBO)                 SET_BIT(iUnlockBitset, 13) ENDIF //carTurboUnlocked

    IF IS_CARMOD_ITEM_UNLOCKED(DUMMY_CARMOD_UNLOCK_ITEM)            SET_BIT(iUnlockBitset, 14) ENDIF //plates1Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_PLATES_L2)             SET_BIT(iUnlockBitset, 15) ENDIF //plates2Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_PLATES_L3)             SET_BIT(iUnlockBitset, 16) ENDIF //plates3Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_PLATES_L4)             SET_BIT(iUnlockBitset, 17) ENDIF //plates4Unlocked
    IF IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_PLATES_L5)             SET_BIT(iUnlockBitset, 18) ENDIF //plates5Unlocked
	
	IF IS_MP_HIPSTER_PACK_PRESENT()
		SET_BIT(iUnlockBitset, 19) // windowTint4Unlocked (Pure Black Windows) - Hipster pack addition...
	ENDIF
	
ENDPROC

FUNC INT GET_COLOUR_UNLOCKS_STAGE_3(INT &iCarColUnlocked[6], INT iColourCheck = -1)
    
    INT iColourCount = 0
    
    IF iColourCheck != -1
        iColourCount = iColourCheck
    ENDIF
    
    BOOL bGotData = FALSE
    INT iGroup
    TEXT_LABEL tlLabel
    INT iAppCol1, iAppCol2
    VEHICLE_MOD_COLOUR_ENUM eModCol
    
    WHILE (iColourCheck = -1 OR iColourCheck = iColourCount) AND GET_CAR_APP_COLOUR_DATA(iColourCount, tlLabel, iGroup, iAppCol1, iAppCol2)
        
        bGotData = TRUE
        eModCol = GET_VEHICLE_MOD_COLOUR_FROM_LABEL(tlLabel)
        
        IF (iGroup = 0)
            IF IS_VEHICLE_MOD_COLOUR_UNLOCKED(MCT_METALLIC, eModCol)        SET_BIT(iCarColUnlocked[iColourCount/32], iColourCount%32) ENDIF
        ELIF (iGroup = 1)
            IF IS_VEHICLE_MOD_COLOUR_UNLOCKED(MCT_CLASSIC, eModCol)         SET_BIT(iCarColUnlocked[iColourCount/32], iColourCount%32) ENDIF
        ELIF (iGroup = 2)
            IF IS_VEHICLE_MOD_COLOUR_UNLOCKED(MCT_MATTE, eModCol)           SET_BIT(iCarColUnlocked[iColourCount/32], iColourCount%32) ENDIF
        ELIF (iGroup = 3)
            IF IS_VEHICLE_MOD_COLOUR_UNLOCKED(MCT_METALS, eModCol)          SET_BIT(iCarColUnlocked[iColourCount/32], iColourCount%32) ENDIF
        ELIF (iGroup = 4)
            IF IS_VEHICLE_MOD_COLOUR_UNLOCKED(MCT_CHROME, eModCol)          SET_BIT(iCarColUnlocked[iColourCount/32], iColourCount%32) ENDIF
        ENDIF
        
        iColourCount++
    ENDWHILE
    
    IF NOT bGotData
        RETURN -1
    ELSE
        RETURN iColourCheck
    ENDIF
ENDFUNC

FUNC BOOL IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_TYPE eMod, INT iIndex)
    CARMOD_UNLOCK_PERFORMANCE_RACES ePerformanceMod
    IF GET_CARMOD_UNLOCK_PERFORMANCE_RACES_FROM_MOD_TYPE(eModUnlockModel, eMod, iIndex, ePerformanceMod)
        RETURN IS_PERFORMANACE_CAR_MOD_UNLOCKED(ePerformanceMod)
    ENDIF
    RETURN TRUE
ENDFUNC

PROC GET_COLOUR_UNLOCKS_STAGE_4(INT &iUnlockBitset)
    // Performance mods.
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_TOGGLE_TURBO, 1) OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_TURBO) 		SET_BIT(iUnlockBitset, 0) ENDIF //carTurboUnlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_ENGINE, 0) 																SET_BIT(iUnlockBitset, 1) ENDIF //carEngine1Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_ENGINE, 1) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L2)     SET_BIT(iUnlockBitset, 2) ENDIF //carEngine2Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_ENGINE, 2) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L3)     SET_BIT(iUnlockBitset, 3) ENDIF //carEngine3Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_ENGINE, 3) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L4)     SET_BIT(iUnlockBitset, 4) ENDIF //carEngine4Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_ENGINE, 4) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_ENGINE_L5)     SET_BIT(iUnlockBitset, 5) ENDIF //carEngine5Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_BRAKES, 0) 																SET_BIT(iUnlockBitset, 6) ENDIF //carBrakes1Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_BRAKES, 1) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L2)     SET_BIT(iUnlockBitset, 7) ENDIF //carBrakes2Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_BRAKES, 2) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L3)     SET_BIT(iUnlockBitset, 8) ENDIF //carBrakes3Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_BRAKES, 3) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L4)     SET_BIT(iUnlockBitset, 9) ENDIF //carBrakes4Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_BRAKES, 4) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_BRAKES_L5)     SET_BIT(iUnlockBitset, 10) ENDIF //carBrakes5Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_EXHAUST, 0) 																SET_BIT(iUnlockBitset, 11) ENDIF //carExhaust1Unlocked
    IF IS_PERFORMANCE_MOD_UNLOCKED_FOR_APP(MOD_EXHAUST, 1) 		OR IS_CARMOD_ITEM_UNLOCKED(CARMOD_UNLOCK_EXHAUST_L2)    SET_BIT(iUnlockBitset, 12) ENDIF //carExhaust2Unlocked
ENDPROC

PROC DO_PROCESS_MOD_UNLOCKS()

	CONST_INT NO_COLOURS_TO_PROCESS_PER_FRAME 25

    INT iUnlockBitset
    
    SWITCH iModUnlockStage
        CASE 0
            IF bProcessModUnlocks
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - Start processing mod unlocks, sModUnlockBlockName = ", sModUnlockBlockName, ", sModUnlockVehicleBlockName = ", sModUnlockVehicleBlockName)
                
                IF NOT NETWORK_IS_GAME_IN_PROGRESS()
                    Execute_Activate_Shop_Carmod(0, FALSE)
                ENDIF
                
                GET_COLOUR_UNLOCKS_STAGE_1(iUnlockBitset)
                
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - unlockBitset1 = ", iUnlockBitset)
                
                APP_SET_APP("car")
                IF NETWORK_IS_GAME_IN_PROGRESS()
                    APP_SET_BLOCK(sModUnlockBlockName)
                    APP_SET_BLOCK("mpUnlocks")
                ELSE
                    APP_SET_BLOCK("spUnlocks")
                ENDIF
                
                APP_SET_INT_DATA("unlockBitset1", iUnlockBitset) iCached_UnlockBitset[0] = iUnlockBitset
                
                // Done
                IF NETWORK_IS_GAME_IN_PROGRESS()
                    APP_CLOSE_BLOCK()
                    APP_CLOSE_BLOCK()
                ELSE
                    APP_CLOSE_BLOCK()
                ENDIF
                APP_CLOSE_APP()
                
                iModUnlockStage++
            ENDIF
        BREAK
        
        CASE 1
        
            GET_COLOUR_UNLOCKS_STAGE_2(iUnlockBitset)
            
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - unlockBitset2 = ", iUnlockBitset)
            
            APP_SET_APP("car")
            IF NETWORK_IS_GAME_IN_PROGRESS()
                APP_SET_BLOCK(sModUnlockBlockName)
                APP_SET_BLOCK("mpUnlocks")
            ELSE
                APP_SET_BLOCK("spUnlocks")
            ENDIF
            
                APP_SET_INT_DATA("unlockBitset2", iUnlockBitset)  iCached_UnlockBitset[1] = iUnlockBitset
                
            // Done
            IF NETWORK_IS_GAME_IN_PROGRESS()
                APP_CLOSE_BLOCK()
                APP_CLOSE_BLOCK()
            ELSE
                APP_CLOSE_BLOCK()
            ENDIF
            APP_CLOSE_APP()
			
			//Clear colour unlock data. #1715985
			INT iColourUnlockIndex
			REPEAT 6 iColourUnlockIndex
				iCarColoursUnlocked[iColourUnlockIndex] = 0
			ENDREPEAT
			iCarColoursProcessed = 0
            
            iModUnlockStage++
        BREAK
		
		CASE 2
			IF NETWORK_IS_GAME_IN_PROGRESS()
				//Scheduled MP colour data lookup across multiple frames to ensure we don't run over our instruction count per frame. #1715985
				INT iColoursProcessedThisFrame
				BOOL bProcessedAllColours
				
				//Look up a quota of colours per frame defined by NO_COLOURS_TO_PROCESS_PER_FRAME.
				WHILE iColoursProcessedThisFrame < NO_COLOURS_TO_PROCESS_PER_FRAME AND NOT bProcessedAllColours
					//Look up data for 1 colour each time through the loop.
					IF GET_COLOUR_UNLOCKS_STAGE_3(iCarColoursUnlocked, iCarColoursProcessed) = -1 
						//All colours processed.
						bProcessedAllColours = TRUE
						iModUnlockStage++
					ENDIF
					iCarColoursProcessed++
					iColoursProcessedThisFrame++
				ENDWHILE
			ELSE
				iModUnlockStage++
			ENDIF
		BREAK
        
        CASE 3
            // Colour and performance unlocks are vehicle specific in MP.
            IF NETWORK_IS_GAME_IN_PROGRESS()
                APP_SET_APP("car")
                    APP_SET_BLOCK(sModUnlockBlockName)
                        APP_SET_BLOCK(sModUnlockVehicleBlockName)
							//Colour data used to be unlocked here but is now scheduled across multiple frames in CASE 2. #1715985
                            APP_SET_INT_DATA("carColoursUnlocked0", iCarColoursUnlocked[0]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked0 = ", iCarColoursUnlocked[0])  iCached_CarColoursUnlocked[0] = iCarColoursUnlocked[0]
                            APP_SET_INT_DATA("carColoursUnlocked1", iCarColoursUnlocked[1]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked1 = ", iCarColoursUnlocked[1])  iCached_CarColoursUnlocked[1] = iCarColoursUnlocked[1]
                            APP_SET_INT_DATA("carColoursUnlocked2", iCarColoursUnlocked[2]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked2 = ", iCarColoursUnlocked[2])  iCached_CarColoursUnlocked[2] = iCarColoursUnlocked[2]
                            APP_SET_INT_DATA("carColoursUnlocked3", iCarColoursUnlocked[3]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked3 = ", iCarColoursUnlocked[3])  iCached_CarColoursUnlocked[3] = iCarColoursUnlocked[3]
                            APP_SET_INT_DATA("carColoursUnlocked4", iCarColoursUnlocked[4]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked4 = ", iCarColoursUnlocked[4])  iCached_CarColoursUnlocked[4] = iCarColoursUnlocked[4]
                            APP_SET_INT_DATA("carColoursUnlocked5", iCarColoursUnlocked[5]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked5 = ", iCarColoursUnlocked[5])  iCached_CarColoursUnlocked[5] = iCarColoursUnlocked[5]
                            
                            GET_COLOUR_UNLOCKS_STAGE_4(iUnlockBitset)
                            APP_SET_INT_DATA("unlockBitset3", iUnlockBitset) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - unlockBitset3 = ", iUnlockBitset)  iCached_UnlockBitset[2] = iUnlockBitset
                            
                        APP_CLOSE_BLOCK()
                    APP_CLOSE_BLOCK()
                APP_CLOSE_APP()
            ELSE
                APP_SET_APP("car")
                    APP_SET_BLOCK("spUnlocks")
                        GET_COLOUR_UNLOCKS_STAGE_3(iCarColoursUnlocked)
                        APP_SET_INT_DATA("carColoursUnlocked0", iCarColoursUnlocked[0]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked0 = ", iCarColoursUnlocked[0])  iCached_CarColoursUnlocked[0] = iCarColoursUnlocked[0]
                        APP_SET_INT_DATA("carColoursUnlocked1", iCarColoursUnlocked[1]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked1 = ", iCarColoursUnlocked[1])  iCached_CarColoursUnlocked[1] = iCarColoursUnlocked[1]
                        APP_SET_INT_DATA("carColoursUnlocked2", iCarColoursUnlocked[2]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked2 = ", iCarColoursUnlocked[2])  iCached_CarColoursUnlocked[2] = iCarColoursUnlocked[2]
                        APP_SET_INT_DATA("carColoursUnlocked3", iCarColoursUnlocked[3]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked3 = ", iCarColoursUnlocked[3])  iCached_CarColoursUnlocked[3] = iCarColoursUnlocked[3]
                        APP_SET_INT_DATA("carColoursUnlocked4", iCarColoursUnlocked[4]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked4 = ", iCarColoursUnlocked[4])  iCached_CarColoursUnlocked[4] = iCarColoursUnlocked[4]
                        APP_SET_INT_DATA("carColoursUnlocked5", iCarColoursUnlocked[5]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColoursUnlocked5 = ", iCarColoursUnlocked[5])  iCached_CarColoursUnlocked[5] = iCarColoursUnlocked[5]
                    APP_CLOSE_BLOCK()
                APP_CLOSE_APP()
            ENDIF
            iModUnlockStage++
        BREAK
        
        CASE 4
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - Stop processing mod unlocks")
            bProcessModUnlocks = FALSE
            iModUnlockStage = 0
        BREAK
    ENDSWITCH
ENDPROC

FUNC BOOL IS_MP_ORDER_VALID(SOCIAL_CAR_APP_ORDER_DATA &sCarAppOrder, INT iVehicleSlot)
	
	INT iBaseCost = 0
	INT iMod
	
	INT iCol1 = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iColour1
	INT iCol2 = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iColour2
	INT iCol3 = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iColourExtra1
	INT iTint = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iWindowTintColour
	IF iTint = -1
		iTint = 0
	ENDIF
	IF sCarAppOrder.iWindowTint = -1
		sCarAppOrder.iWindowTint = 0
	ENDIF
	
	CPRINTLN(DEBUG_SOCIAL,"social_controller : Car app order breakdown")
	CPRINTLN(DEBUG_SOCIAL,"Order cost 	  = ", sCarAppOrder.iCost)
	CPRINTLN(DEBUG_SOCIAL,"Colour1		  = ", sCarAppOrder.iColourID1)
	CPRINTLN(DEBUG_SOCIAL,"Colour2		  = ", sCarAppOrder.iColourID2)
	CPRINTLN(DEBUG_SOCIAL,"Order engine     = ", sCarAppOrder.iEngine, 		"	: Current engine     = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ENGINE])
	CPRINTLN(DEBUG_SOCIAL,"Order brakes     = ", sCarAppOrder.iBrakes, 		"	: Current brakes     = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_BRAKES])
	CPRINTLN(DEBUG_SOCIAL,"Order exhaust    = ", sCarAppOrder.iExhaust, 		"	: Current exhaust    = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_EXHAUST])
	CPRINTLN(DEBUG_SOCIAL,"Order wheels     = ", sCarAppOrder.iWheels, 		"	: Current wheels     = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS])
	CPRINTLN(DEBUG_SOCIAL,"Order armour     = ", sCarAppOrder.iArmour, 		"	: Current armour     = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ARMOUR])
	CPRINTLN(DEBUG_SOCIAL,"Order suspension = ", sCarAppOrder.iSuspension, 	"	: Current suspension = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION])
	CPRINTLN(DEBUG_SOCIAL,"Order horn       = ", sCarAppOrder.iHorn, 			"	: Current horn       = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_HORN])
	CPRINTLN(DEBUG_SOCIAL,"Order lights     = ", sCarAppOrder.iLights, 		"	: Current lights     = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS])
	CPRINTLN(DEBUG_SOCIAL,"Order turbo      = ", sCarAppOrder.iTurbo, 		"	: Current turbo      = ", g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO])
	CPRINTLN(DEBUG_SOCIAL,"Order tint       = ", sCarAppOrder.iWindowTint, 	"	: Current tint       = ", iTint)
	
	// Things to check...
	// - Primary colour
	// - Secondary colour
	INT iPrim1, iPrim2, iSec1, iDummy
	INT iAppColGroup
	TEXT_LABEL_15 tlAppColLabel
	MOD_COLOR_TYPE eModColType
	
	INT iColCost = 0
	IF GET_VEHICLE_COLOUR_FROM_CAR_APP_COLOUR_INDEX(sCarAppOrder.iColourID1, iPrim1, iPrim2)
		IF iPrim1 != -1 AND iPrim2 != -1 AND (iPrim1 != iCol1 OR iPrim2 != iCol3)
			
			GET_CAR_APP_COLOUR_DATA(sCarAppOrder.iColourID1, tlAppColLabel, iAppColGroup, iPrim1, iPrim2)
			
			IF (iAppColGroup = 0)	eModColType = MCT_METALLIC //metallic
			ELIF (iAppColGroup = 1)	eModColType = MCT_CLASSIC //classic
			ELIF (iAppColGroup = 2)	eModColType = MCT_MATTE //matte
			ELIF (iAppColGroup = 3)	eModColType = MCT_METALS //metals
			ELIF (iAppColGroup = 4)	eModColType = MCT_CHROME //chrome
			ENDIF
			
			iColCost = GET_BASE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(tlAppColLabel, eModColType, TRUE, TRUE, FALSE, FALSE, FALSE)
			iBaseCost += iColCost
			CPRINTLN(DEBUG_SOCIAL,"  Primary Colour......$", iColCost)
		ENDIF
	ENDIF
	
	IF GET_VEHICLE_COLOUR_FROM_CAR_APP_COLOUR_INDEX(sCarAppOrder.iColourID2, iSec1, iDummy)
		IF iSec1 != -1 AND iSec1 != iCol2
			GET_CAR_APP_COLOUR_DATA(sCarAppOrder.iColourID2, tlAppColLabel, iAppColGroup, iPrim1, iPrim2)
			
			IF (iAppColGroup = 0)	eModColType = MCT_METALLIC //metallic
			ELIF (iAppColGroup = 1)	eModColType = MCT_CLASSIC //classic
			ELIF (iAppColGroup = 2)	eModColType = MCT_MATTE //matte
			ELIF (iAppColGroup = 3)	eModColType = MCT_METALS //metals
			ELIF (iAppColGroup = 4)	eModColType = MCT_CHROME //chrome
			ENDIF
			
			iColCost = GET_BASE_CARMOD_COLOUR_PRICE_FROM_TEXT_LABEL(tlAppColLabel, eModColType, FALSE, TRUE, FALSE, FALSE, FALSE)
			
			iBaseCost += iColCost
			CPRINTLN(DEBUG_SOCIAL,"  Secondary Colour......$", iColCost)
		ENDIF
	ENDIF
	
	// - Plates
	IF sCarAppOrder.iPlateBack_pending != g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iPlateIndex
		IF sCarAppOrder.iPlateBack_pending = 3 // Blue on White 1
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Plates, Blue on White 1......$", 200)
		ELIF sCarAppOrder.iPlateBack_pending = 0 // Blue on White 2
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Plates, Blue on White 2......$", 200)
		ELIF sCarAppOrder.iPlateBack_pending= 4 // Blue on White 3
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Plates, Blue on White 3......$", 200)
		ELIF sCarAppOrder.iPlateBack_pending = 2 // Orange on Blue
			iBaseCost += 300
			CPRINTLN(DEBUG_SOCIAL,"  Plates, Orange on Blue......$", 300)
		ELIF sCarAppOrder.iPlateBack_pending = 1 // Yellow on Black
			iBaseCost += 600
			CPRINTLN(DEBUG_SOCIAL,"  Plates, Yellow on Black......$", 600)
		ENDIF
	ENDIF
	
	// - Bullet proof tyres
	IF NOT IS_BIT_SET(g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		IF sCarAppOrder.bBulletProofTyres
			iBaseCost += 25000
			CPRINTLN(DEBUG_SOCIAL,"  Bullet proof tyres......$", 25000)
		ENDIF
	ENDIF
	
	// - Tyre smoke
	INT iRed, iGreen, iBlue
	iRed = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iTyreR
	iGreen = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iTyreG
	iBlue = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iTyreB
	
	IF sCarAppOrder.iTyreSmokeR != iRed
	OR sCarAppOrder.iTyreSmokeG != iGreen
	OR sCarAppOrder.iTyreSmokeB != iBlue
		SWITCH GET_TYRE_SMOKE_COLOUR_ENUM_FROM_RGB(sCarAppOrder.iTyreSmokeR, sCarAppOrder.iTyreSmokeG, sCarAppOrder.iTyreSmokeB)
			CASE TYRE_SMOKE_WHITE
				iBaseCost += 1500
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke white......$", 1500)
			BREAK
			CASE TYRE_SMOKE_BLACK
				iBaseCost += 5000
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke black......$", 5000)
			BREAK
			CASE TYRE_SMOKE_BLUE
				iBaseCost += 10000
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke blue......$", 10000)
			BREAK
			CASE TYRE_SMOKE_YELLOW
				iBaseCost += 12500
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke yellow......$", 12500)
			BREAK
			CASE TYRE_SMOKE_ORANGE
				iBaseCost += 15000
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke orange......$", 15000)
			BREAK
			CASE TYRE_SMOKE_RED
				iBaseCost += 20000
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke red......$", 20000)
			BREAK
			CASE TYRE_SMOKE_BUSINESS_PURPLE
				iBaseCost += 14000
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke red......$", 14000)
			BREAK
			CASE TYRE_SMOKE_BUSINESS_GREEN
				iBaseCost += 17500
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke purple......$", 17500)
			BREAK
			CASE TYRE_SMOKE_HIPSTER_PINK
				iBaseCost += g_sMPTunables.iDLC_hipster_modifier_Pink_Tire_Smoke
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke pink......$", g_sMPTunables.iDLC_hipster_modifier_Pink_Tire_Smoke)
			BREAK
			CASE TYRE_SMOKE_HIPSTER_BROWN
				iBaseCost += g_sMPTunables.iDLC_hipster_modifier_Brown_Tire_Smoke
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke brown......$", g_sMPTunables.iDLC_hipster_modifier_Brown_Tire_Smoke)
			BREAK
			CASE TYRE_SMOKE_INDI_PAT
				iBaseCost += g_sMPTunables.iVehiclekit_IndependenceDay_Patriot_Tire_Smoke
				CPRINTLN(DEBUG_SOCIAL,"  Tyre smoke red/white/blue......$", g_sMPTunables.iVehiclekit_IndependenceDay_Patriot_Tire_Smoke)
			BREAK
		ENDSWITCH
	ENDIF
	
	// - Window tint
	IF sCarAppOrder.iWindowTint != iTint
		IF sCarAppOrder.iWindowTint = 0
			iBaseCost += 500
			CPRINTLN(DEBUG_SOCIAL,"  Window tint none......$", 500)
		ELIF sCarAppOrder.iWindowTint = 1
			iBaseCost += 5000
			CPRINTLN(DEBUG_SOCIAL,"  Window tint limo......$", 5000)
		ELIF sCarAppOrder.iWindowTint = 2
			iBaseCost += 3500
			CPRINTLN(DEBUG_SOCIAL,"  Window tint dark smoke......$", 3500)
		ELIF sCarAppOrder.iWindowTint = 3
			iBaseCost += 1500
			CPRINTLN(DEBUG_SOCIAL,"  Window tint light smoke......$", 1500)
		ELIF sCarAppOrder.iWindowTint = 5
			iBaseCost += g_sMPTunables.iDLC_hipster_modifier_Pure_Black
			CPRINTLN(DEBUG_SOCIAL,"  Window tint pure black ......$", g_sMPTunables.iDLC_hipster_modifier_Pure_Black)
		ENDIF
	ENDIF
	
	// - Engine
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ENGINE]
	IF sCarAppOrder.iEngine != iMod
	AND sCarAppOrder.iEngine != -1
		IF sCarAppOrder.iEngine = 0
			iBaseCost += 500
			CPRINTLN(DEBUG_SOCIAL,"  Engine......$", 500)
		ELIF sCarAppOrder.iEngine = 1
			iBaseCost += 1800
			CPRINTLN(DEBUG_SOCIAL,"  Engine......$", 1800)
		ELIF sCarAppOrder.iEngine = 2
			iBaseCost += 2500
			CPRINTLN(DEBUG_SOCIAL,"  Engine......$", 2500)
		ELIF sCarAppOrder.iEngine = 3
			iBaseCost += 3600
			CPRINTLN(DEBUG_SOCIAL,"  Engine......$", 3600)
		ELIF sCarAppOrder.iEngine = 4
			iBaseCost += 6700
			CPRINTLN(DEBUG_SOCIAL,"  Engine......$", 6700)
		ENDIF
	ENDIF
	
	// - Brakes
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_BRAKES]
	IF sCarAppOrder.iBrakes != iMod
	AND sCarAppOrder.iBrakes != -1
		IF sCarAppOrder.iBrakes = 0
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Brakes......$", 200)
		ELIF sCarAppOrder.iBrakes = 1
			iBaseCost += 4000
			CPRINTLN(DEBUG_SOCIAL,"  Brakes......$", 4000)
		ELIF sCarAppOrder.iBrakes = 2
			iBaseCost += 5400
			CPRINTLN(DEBUG_SOCIAL,"  Brakes......$", 5400)
		ELIF sCarAppOrder.iBrakes = 3
			iBaseCost += 7000
			CPRINTLN(DEBUG_SOCIAL,"  Brakes......$", 7000)
		ENDIF
	ENDIF
	
	// - Exhaust
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_EXHAUST]
	IF sCarAppOrder.iExhaust != iMod
	AND sCarAppOrder.iExhaust != -1
		IF sCarAppOrder.iExhaust = 0
			iBaseCost += 260
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 260)
		ELIF sCarAppOrder.iExhaust = 1
			iBaseCost += 750
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 750)
		ELIF sCarAppOrder.iExhaust = 2
			iBaseCost += 1800
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 1800)
		ELIF sCarAppOrder.iExhaust = 3
			iBaseCost += 3000
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 3000)
		ELIF sCarAppOrder.iExhaust = 4
			iBaseCost += 9550
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 9550)
		ELIF sCarAppOrder.iExhaust = 5
			iBaseCost += 9750
			CPRINTLN(DEBUG_SOCIAL,"  Exhaust......$", 9750)
		ENDIF
	ENDIF
	
	// - Wheels
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS]
	IF sCarAppOrder.iWheels != iMod
	AND sCarAppOrder.iWheels != -1
		// wheels cost
	ENDIF
	
	// - Horn
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_HORN]
	IF sCarAppOrder.iHorn != iMod
	AND sCarAppOrder.iHorn != -1
		IF sCarAppOrder.iHorn = 0
			iBaseCost += 500
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 500)
		ELIF sCarAppOrder.iHorn = 1
			iBaseCost += 2000
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 2000)
		ELIF sCarAppOrder.iHorn = 2
			iBaseCost += 3000
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 3000)
		ELIF sCarAppOrder.iHorn = 3
			iBaseCost += 5000
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 5000)
		ELIF sCarAppOrder.iHorn = 4
			iBaseCost += 10000
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 10000)
		ELIF sCarAppOrder.iHorn = 5
			iBaseCost += 12500
			CPRINTLN(DEBUG_SOCIAL,"  Horn......$", 12500)
		ENDIF
	ENDIF
	
	// - Armour
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ARMOUR]
	IF sCarAppOrder.iArmour != iMod
	AND sCarAppOrder.iArmour != -1
		IF sCarAppOrder.iArmour = 0
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 200)
		ELIF sCarAppOrder.iArmour = 1
			iBaseCost += 3600
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 3600)
		ELIF sCarAppOrder.iArmour = 2
			iBaseCost += 6000
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 6000)
		ELIF sCarAppOrder.iArmour = 3
			iBaseCost += 9600
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 9600)
		ELIF sCarAppOrder.iArmour = 4
			iBaseCost += 16800
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 16800)
		ELIF sCarAppOrder.iArmour = 5
			iBaseCost += 24000
			CPRINTLN(DEBUG_SOCIAL,"  Armour......$", 24000)
		ENDIF
	ENDIF
	
	// - Suspension
	iMod = g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION]
	IF sCarAppOrder.iSuspension != iMod
	AND sCarAppOrder.iSuspension != -1
		IF sCarAppOrder.iSuspension = 0
			iBaseCost += 200
			CPRINTLN(DEBUG_SOCIAL,"  Suspension......$", 200)
		ELIF sCarAppOrder.iSuspension = 1
			iBaseCost += 1000
			CPRINTLN(DEBUG_SOCIAL,"  Suspension......$", 1000)
		ELIF sCarAppOrder.iSuspension = 2
			iBaseCost += 2000
			CPRINTLN(DEBUG_SOCIAL,"  Suspension......$", 2000)
		ELIF sCarAppOrder.iSuspension = 3
			iBaseCost += 3400
			CPRINTLN(DEBUG_SOCIAL,"  Suspension......$", 3400)
		ELIF sCarAppOrder.iSuspension = 4
			iBaseCost += 4400
			CPRINTLN(DEBUG_SOCIAL,"  Suspension......$", 4400)
		ENDIF
	ENDIF
	
	// - Lights
	IF sCarAppOrder.iLights != -1
		IF (sCarAppOrder.iLights != 1 AND g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] > 0)
			iBaseCost += 100
			CPRINTLN(DEBUG_SOCIAL,"  No lights......$", 100)
		ELIF (sCarAppOrder.iLights = 1 AND g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] <= 0)
			iBaseCost += 2000
			CPRINTLN(DEBUG_SOCIAL,"  Lights......$", 2000)
		ENDIF
	ENDIF
	
	// - Turbo
	IF sCarAppOrder.iTurbo != -1
		IF (sCarAppOrder.iTurbo != 1 AND g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] > 0)
			iBaseCost += 1000
			CPRINTLN(DEBUG_SOCIAL,"  No turbo......$", 1000)
		ELIF (sCarAppOrder.iTurbo = 1 AND g_MpSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] <= 0)
			iBaseCost += 24000
			CPRINTLN(DEBUG_SOCIAL,"  Turbo......$", 24000)
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_SOCIAL,"IS_MP_ORDER_VALID")
	CPRINTLN(DEBUG_SOCIAL,"...Base cost = ", iBaseCost)
	CPRINTLN(DEBUG_SOCIAL,"...Order cost = ", sCarAppOrder.iCost)
	
	RETURN (sCarAppOrder.iCost >= iBaseCost)
ENDFUNC
    

PROC FILL_CAR_APP_DATA(SOCIAL_CAR_APP_DATA &sData, INT iSelectedVehSlot)
	// Saved vehicle data
	sData.eModel = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.eModel
	sData.iWindowTint = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iWindowTintColour
	sData.iTyreSmokeR = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iTyreR
	sData.iTyreSmokeG = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iTyreG
	sData.iTyreSmokeB = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iTyreB
	sData.iWheelType = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iWheelType
	sData.bBulletProofTyres = (IS_BIT_SET(g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES))
	sData.tlPlateText = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.tlPlateText
	sData.iPlateBack = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iPlateIndex
	sData.iEngine = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ENGINE]
	sData.iBrakes = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_BRAKES]
	sData.iWheels = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_WHEELS]
	sData.iExhaust = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_EXHAUST]
	sData.iSuspension = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_SUSPENSION]
	sData.iArmour = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_ARMOUR]
	sData.iHorn = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_HORN]
	sData.iLights = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS]
	sData.iTyreSmoke = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE]
	sData.iTurbo = g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO]
	sData.bSendDataToCloud = (IS_BIT_SET(g_MpSavedVehicles[iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND))
	sData.bDeleteData = (IS_BIT_SET(g_MpSavedVehicles[iSelectedVehSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE))
	
	GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iColour1, g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iColourExtra1, g_MpSavedVehicles[iSelectedVehSlot].iPrimaryColourGroup, TRUE, sData.iColourID1)
	GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iColour2, -1, g_MpSavedVehicles[iSelectedVehSlot].iSecondaryColourGroup, FALSE, sData.iColourID2)
	
	// Clear the colourID's if we have a crew colour.
	IF (IS_BIT_SET(g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_PRIMARY))
		sData.iColourID1 = -1
	ENDIF
	IF (IS_BIT_SET(g_MpSavedVehicles[iSelectedVehSlot].vehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_SECONDARY))
		sData.iColourID2 = -1
	ENDIF
	
	// Common data for all vehicles in MP
	sData.bUpdateMods = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bUpdateMods
ENDPROC

FUNC BOOL FILL_STATIC_CAR_APP_DATA(SOCIAL_CAR_APP_DATA &sData)

	// The purpose of this function is to replace the following data lookup to ensure the data we have is 100% up to date.
	// STATIC_CAR_APP_DATA sStaticData
	// GET_STATIC_CAR_APP_DATA_FOR_VEHICLE(sData.eModel, sStaticData)

	
	IF NOT IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(sData.eModel)
		RETURN TRUE
	ENDIF
	
	IF bFetchTimerSet
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdFetchTimer)) < 1500
			RETURN FALSE
		ENDIF
	ENDIF
	bFetchTimerSet = FALSE
	
//	#IF IS_DEBUG_BUILD
//		bFetchingData = TRUE		
//	#ENDIF
	REQUEST_MODEL(sData.eModel)
	IF NOT HAS_MODEL_LOADED(sData.eModel)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX vehID
	IF NETWORK_IS_GAME_IN_PROGRESS()
		vehID = CREATE_VEHICLE(sData.eModel, <<660.3092+(NATIVE_TO_INT(PLAYER_ID())*10), 4977.6084, 1222.0242>>, 0, FALSE, FALSE)
	ELSE
		vehID = CREATE_VEHICLE(sData.eModel, <<660.3092, 4977.6084, 1222.0242>>, 0, FALSE, FALSE)
	ENDIF
	
	IF GET_NUM_MOD_KITS(vehID) > 0
		SET_VEHICLE_MOD_KIT(vehID, 0)
	ENDIF
	
	// Apply stored vehicle mods?
	SET_VEHICLE_WHEEL_TYPE(vehID, INT_TO_ENUM(MOD_WHEEL_TYPE, g_MpSavedVehicles[iMPSavedVehicleSlot].vehicleSetupMP.VehicleSetup.iWheelType))
	
	// No longer saving vehicle data in the MP savegame so just grab from the vehicle we created.
	sData.iHornHash[0] = GET_VEHICLE_DEFAULT_HORN_IGNORE_MODS(vehID)
	sData.iHornHash[1] = GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 0)
	sData.iHornHash[2] = GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 1)
	sData.iHornHash[3] = GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 2)
	sData.iHornHash[4] = GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 3)
	sData.iModCountEngine = GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
	sData.iModCountBrakes = GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
	sData.iModCountExhaust = GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
	sData.iModCountHorn = GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
	sData.iModCountArmour = GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
	sData.iModCountSuspension = GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
	sData.fModPriceModifier = GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
	sData.iModColoursThatCanBeSet = GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
	sData.eModKitType = GET_VEHICLE_MOD_KIT_TYPE(vehID)	
	sData.iModCountWheels = GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
	iWheelVariation = GET_VEHICLE_MOD_VARIATION(vehID, MOD_WHEELS)
	
	IF !NETWORK_IS_GAME_IN_PROGRESS()
		IF sData.eModel = PATRIOT
			sData.iModCountExhaust = 0
			PRINTLN("FILL_STATIC_CAR_APP_DATA iExhaust setting to = 0")
		ENDIF
		IF sData.eModel = PRAIRIE 
			IF sData.iModCountExhaust > 1
				PRINTLN("FILL_STATIC_CAR_APP_DATA iExhaust setting to = 1 it was: ", sData.iModCountExhaust)
				sData.iModCountExhaust = 1
			ENDIF 
		ENDIF
	ENDIF
	
	DELETE_VEHICLE(vehID)
	SET_MODEL_AS_NO_LONGER_NEEDED(sData.eModel)
	
	bFetchTimerSet = TRUE
	tdFetchTimer = GET_NETWORK_TIME()
	
	// Prevent save from kicking in.
	SETTIMERA(0)
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_SAVED_MP_CAR_APP_DATA(SOCIAL_CAR_APP_DATA &sData)
	
	IF sData.bSendDataToCloud
		SET_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
	ELSE
		CLEAR_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
	ENDIF
	
	IF sData.bDeleteData
		SET_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
	ELSE
		CLEAR_BIT(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
	ENDIF
ENDPROC


CONST_INT NUM_SUPERMOD_WHEELS_BENNY_ORIGINAL 	31
CONST_INT NUM_SUPERMOD_WHEELS_BENNY_BESPOKE 	31
CONST_INT NUM_SUPERMOD_WHEELS_RACE				20
CONST_INT NUM_SUPERMOD_WHEELS_STREET			30
CONST_INT NUM_SUPERMOD_WHEELS_TRACK				30

/// PURPOSE: Gets the unique wheel mod count for a vehicle. Excludes mods with tyre variants.
FUNC INT GET_VEHICLE_MOD_WHEEL_COUNT(SOCIAL_CAR_APP_DATA &sData)

	SWITCH INT_TO_ENUM(MOD_WHEEL_TYPE, sData.iWheelType)
		CASE MWT_SUPERMOD1
			RETURN NUM_SUPERMOD_WHEELS_BENNY_ORIGINAL
		BREAK
		CASE MWT_SUPERMOD2
			RETURN NUM_SUPERMOD_WHEELS_BENNY_BESPOKE
		BREAK
		CASE MWT_SUPERMOD3
			IF IS_VEHICLE_AN_OPEN_WHEEL_VEHICLE(sData.eModel)
				RETURN NUM_SUPERMOD_WHEELS_RACE
			ENDIF
		BREAK
		CASE MWT_SUPERMOD4
			RETURN NUM_SUPERMOD_WHEELS_STREET
		BREAK	
		CASE MWT_SUPERMOD5
			RETURN NUM_SUPERMOD_WHEELS_TRACK
		BREAK	
	ENDSWITCH
	
	RETURN sData.iModCountWheels - 1
ENDFUNC

/// PURPOSE: Gets the global mod wheel index for a given mod index and set.
FUNC INT GET_VEHICLE_MOD_WHEEL_INDEX_FOR_VARIATION(SOCIAL_CAR_APP_DATA &sData)
	INT iWheelCount = GET_VEHICLE_MOD_WHEEL_COUNT(sData)
	PRINTLN("GET_VEHICLE_MOD_WHEEL_INDEX_FOR_VARIATION iWheelCount: ", iWheelCount, " sData.iWheels: ", sData.iWheels, " sData.iWheelVariation: ", iWheelVariation)
	RETURN ((sData.iWheels % iWheelCount) + (iWheelVariation * iWheelCount))
ENDFUNC

/// PURPOSE: Attempts to fetch the car app data and sends txt if successful.
PROC DO_PROCESS_CAR_APP(enumCharacterList ePed)

    BOOL bGetPlate
    INT iPass
    INT iSlotID
    INT iMPCharSlot
    INT iR, iG, iB, iA
    TEXT_LABEL_23 sBlockName
    TEXT_LABEL_23 sOrderBlockName
    TEXT_LABEL_23 sVehicleBlockName
    SOCIAL_CAR_APP_DATA sData
    SOCIAL_CAR_APP_ORDER_DATA sOrder
    
    IF ePed = CHAR_MICHAEL
        iSlotID = 0
        sBlockName = "singleplayer0"
        sOrderBlockName = "sp0_order"
        sData = g_savedGlobals.sSocialData.sCarAppData[CHAR_MICHAEL]
        sOrder = g_savedGlobals.sSocialData.sCarAppOrder[CHAR_MICHAEL]
    ELIF ePed = CHAR_FRANKLIN
        iSlotID = 1
        sBlockName = "singleplayer1"
        sOrderBlockName = "sp1_order"
        sData = g_savedGlobals.sSocialData.sCarAppData[CHAR_FRANKLIN]
        sOrder = g_savedGlobals.sSocialData.sCarAppOrder[CHAR_FRANKLIN]
    ELIF ePed = CHAR_TREVOR
        iSlotID = 2
        sBlockName = "singleplayer2"
        sOrderBlockName = "sp2_order"
        sData = g_savedGlobals.sSocialData.sCarAppData[CHAR_TREVOR]
        sOrder = g_savedGlobals.sSocialData.sCarAppOrder[CHAR_TREVOR]
    ELIF ePed = CHAR_MULTIPLAYER
        iMPCharSlot = GET_ACTIVE_CHARACTER_SLOT()
        iSlotID = 3+iMPCharSlot
        sBlockName = "multiplayer"
        sBlockName += iMPCharSlot
        sOrderBlockName = "mp"
        sOrderBlockName += iMPCharSlot
        sOrderBlockName += "_order"
		
		INT iSavedVehicleSlot
		IF GET_SAVE_VEHICLE_SLOT_FROM_CAR_APP_SLOT(iMPCarAppSlot, iSavedVehicleSlot)
			FILL_CAR_APP_DATA(sData, iSavedVehicleSlot)
		ELSE
			FILL_CAR_APP_DATA(sData, 0)
		ENDIF
        sOrder = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder // we need to determine what the last used vehicle slot was..
        sVehicleBlockName = "vehicle"
        sVehicleBlockName += iMPVehicleBlock
    ELSE
        EXIT
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     REFERSH APP DATA
    ///    
    IF bRefreshAppDataOnInit
	OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND bResendAppDataForAccountLinkSP)
        IF NETWORK_IS_GAME_IN_PROGRESS()
            // dont bother with mp for now - not going to be changing profiles much.
            bRefreshAppDataOnInit = FALSE
        ELSE
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - refreshing contents of app.")
            
            APP_SET_APP("car")
            
            APP_SET_BLOCK("singleplayer0")
            APP_SET_INT_DATA("carUnlocked", 0)
            APP_CLOSE_BLOCK()
            
//          APP_SET_BLOCK("singleplayer1")
//          APP_SET_INT_DATA("carUnlocked", 0)
//          APP_CLOSE_BLOCK()
            
            APP_SET_BLOCK("singleplayer2")
            APP_SET_INT_DATA("carUnlocked", 0)
            APP_CLOSE_BLOCK()
            
            APP_CLOSE_APP()
			
			IF NETWORK_IS_SIGNED_ONLINE()
				APP_SET_APP("car")
					APP_SET_BLOCK("appdata")
						CPRINTLN(DEBUG_SOCIAL,"playerName = ", GET_PLAYER_NAME(PLAYER_ID()), " with hash ", GET_HASH_KEY((GET_PLAYER_NAME(PLAYER_ID()))), ", mp=", NETWORK_IS_GAME_IN_PROGRESS())
						APP_SET_INT_DATA("playerName", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
					APP_CLOSE_BLOCK()
				APP_CLOSE_APP()
			ENDIF
			
            INT iSlot
            REPEAT COUNT_OF(g_savedGlobals.sSocialData.sCarAppData) iSlot
                IF NOT g_savedGlobals.sSocialData.sCarAppData[iSlot].bDeleteData
                    g_savedGlobals.sSocialData.sCarAppData[iSlot].bSendDataToCloud = FALSE
                    IF g_savedGlobals.sSocialData.sCarAppData[iSlot].eModel != DUMMY_MODEL_FOR_SCRIPT
                    AND g_savedGlobals.sSocialData.sCarAppData[iSlot].iModCountArmour != 0 // we have to set model so check if mod count is set.
                        g_savedGlobals.sSocialData.sCarAppData[iSlot].bSendDataToCloud = TRUE
                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - resending vehicle in slot ", iSlot)
                    ELSE
                        g_savedGlobals.sSocialData.bPlayerUnlockedInApp[iSlot] = FALSE
                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - resending vehicle in slot ", iSlot, " (defaults)")
                    ENDIF
                ENDIF
            ENDREPEAT
            bRefreshAppDataOnInit = FALSE
        ENDIF
        
        IF ENUM_TO_INT(ePed) >= NUM_OF_PLAYABLE_PEDS-1 // On last ped?
            bRefreshAppDataOnInit = FALSE
			bResendAppDataForAccountLinkSP = FALSE
        ENDIF
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     UNLOCK SP CHARACTER
    ///    
    IF NOT NETWORK_IS_GAME_IN_PROGRESS()
        IF NOT g_savedGlobals.sSocialData.bPlayerUnlockedInApp[ePed]
            IF ((NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY) AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS) AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)) OR (NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)))
                IF (ePed = CHAR_FRANKLIN)
                OR (ePed = CHAR_MICHAEL AND GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) AND IS_PLAYER_PED_AVAILABLE(ePed) AND NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_KILLED])
                OR (ePed = CHAR_TREVOR AND GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) AND IS_PLAYER_PED_AVAILABLE(ePed) AND NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_TREVOR_KILLED])
                
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - sending initial vehicle data to cloud: ", sBlockName)
                    
                    APP_SET_APP("car")
                        APP_SET_BLOCK(sBlockName)
                        
                            APP_CLEAR_BLOCK()
                        
                            PED_VEH_DATA_STRUCT sVehData
                            GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
                            
                            INT iColID1, iColID2
                            GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(sVehData.iColour1, sVehData.iColourExtra1, -1, TRUE, iColID1)
                            GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(sVehData.iColour2, -1, -1, FALSE, iColID2)
                            
                            
                            APP_SET_INT_DATA("carUnlocked", 1)
                            
                            APP_SET_INT_DATA("carModel", ENUM_TO_INT(sVehData.model))
                            
                            APP_SET_INT_DATA("tyreSmokeColourEnabled", 1)
                            
                            APP_SET_INT_DATA("tyreSmokeColourRed", 255)
                            APP_SET_INT_DATA("tyreSmokeColourGreen", 255)
                            APP_SET_INT_DATA("tyreSmokeColourBlue", 255)
                            
                            IF sVehData.model = BUFFALO2
                            
                                APP_SET_STRING_DATA("carType", "sport")
                            
                                APP_SET_INT_DATA("carEngineCount", 5)
                                APP_SET_INT_DATA("carBrakesCount", 4)
                                APP_SET_INT_DATA("carExhaustCount", 6)
                                APP_SET_INT_DATA("carWheelCount", 26)
                                APP_SET_INT_DATA("carHornCount", 5)
                                APP_SET_INT_DATA("carArmourCount", 6)
                                APP_SET_INT_DATA("carSuspensionCount", 5)
                                
                                APP_SET_INT_DATA("carHorn1", 1748384362)
                                APP_SET_INT_DATA("carHorn2", 1766676233)
                                APP_SET_INT_DATA("carHorn3", -1390777827)
                                APP_SET_INT_DATA("carHorn4", -1751761149)
                                APP_SET_INT_DATA("carHorn5", 1732399718)
                                
                                APP_SET_FLOAT_DATA("carPriceModifier", 3.25)
                                
                                APP_SET_INT_DATA("carColour1", iColID1)
                                APP_SET_INT_DATA("carColour2", iColID2)
                                APP_SET_INT_DATA("carColour1Unlocked", 1)
                                APP_SET_INT_DATA("carColour2Unlocked", 1)
                                
                            ELIF sVehData.model = BODHI2
                                
                                APP_SET_STRING_DATA("carType", "car")
                                
                                APP_SET_INT_DATA("carEngineCount", 5)
                                APP_SET_INT_DATA("carBrakesCount", 4)
                                APP_SET_INT_DATA("carExhaustCount", 1)
                                APP_SET_INT_DATA("carWheelCount", 26)
                                APP_SET_INT_DATA("carHornCount", 5)
                                APP_SET_INT_DATA("carArmourCount", 6)
                                APP_SET_INT_DATA("carSuspensionCount", 1)
                                
                                APP_SET_INT_DATA("carHorn1", -1512308941)
                                APP_SET_INT_DATA("carHorn2", 1766676233)
                                APP_SET_INT_DATA("carHorn3", -1390777827)
                                APP_SET_INT_DATA("carHorn4", -1751761149)
                                APP_SET_INT_DATA("carHorn5", 1732399718)
                                
                                APP_SET_FLOAT_DATA("carPriceModifier", 1.0)
                                
                                APP_SET_INT_DATA("carColour1", iColID1)
                                APP_SET_INT_DATA("carColour1Unlocked", 1)
                                
                            ELIF sVehData.model = TAILGATER
                            
                                APP_SET_STRING_DATA("carType", "car")
                                
                                APP_SET_INT_DATA("carEngineCount", 5)
                                APP_SET_INT_DATA("carBrakesCount", 5)
                                APP_SET_INT_DATA("carExhaustCount", 5)
                                APP_SET_INT_DATA("carWheelCount", 26)
                                APP_SET_INT_DATA("carHornCount", 5)
                                APP_SET_INT_DATA("carArmourCount", 6)
                                APP_SET_INT_DATA("carSuspensionCount", 5)
                                
                                APP_SET_INT_DATA("carHorn1", 36213993)
                                APP_SET_INT_DATA("carHorn2", 1766676233)
                                APP_SET_INT_DATA("carHorn3", -1390777827)
                                APP_SET_INT_DATA("carHorn4", -1751761149)
                                APP_SET_INT_DATA("carHorn5", 1732399718)
                                
                                APP_SET_FLOAT_DATA("carPriceModifier", 1.0)
                                
                                APP_SET_INT_DATA("carColour1", iColID1)
                                APP_SET_INT_DATA("carColour2", iColID2)
                                APP_SET_INT_DATA("carColour1Unlocked", 1)
                                APP_SET_INT_DATA("carColour2Unlocked", 1)
                                
                            ENDIF
                        // Done
                        APP_CLOSE_BLOCK()
                        
                        sModUnlockBlockName = sBlockName
                        sModUnlockVehicleBlockName = sVehicleBlockName
                        eModUnlockModel = sVehData.model
                        bProcessModUnlocks = TRUE
                        bSaveCarData = TRUE SETTIMERA(0)
                    APP_CLOSE_APP()
                    
                    g_savedGlobals.sSocialData.bPlayerUnlockedInApp[ePed] = TRUE
                ENDIF
            ENDIF
        ELSE
            // Lock when ped is killed!
            IF (ePed = CHAR_MICHAEL AND g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_KILLED])
            OR (ePed = CHAR_TREVOR AND g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_TREVOR_KILLED])
                g_savedGlobals.sSocialData.sCarAppData[ePed].bDeleteData = TRUE
                g_savedGlobals.sSocialData.bPlayerUnlockedInApp[ePed] = FALSE
            ENDIF
        ENDIF
    ENDIF
    
    
    ///////////////////////////////////////////////////////
    ///     
    ///     LOCK MP CHARACTERS
    ///    
    IF NETWORK_IS_GAME_IN_PROGRESS()
    
        BOOL bDataHasBeenSent = FALSE
    
        IF (iMPLockCheck = 0)
            bDataHasBeenSent = GET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_0)
        ELIF (iMPLockCheck = 1)
            bDataHasBeenSent = GET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_1)
        ELIF (iMPLockCheck = 2)
            bDataHasBeenSent = GET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_2)
        ELIF (iMPLockCheck = 3)
            bDataHasBeenSent = GET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_3)
        ELIF (iMPLockCheck = 4)
            bDataHasBeenSent = GET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_4)
        ENDIF
        
        IF bDataHasBeenSent
        AND NOT IS_STAT_CHARACTER_ACTIVE(iMPLockCheck)
        
            // Remove the character
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - removing MP character data from cloud, slot = ", iMPLockCheck)
                    
            APP_SET_APP("car")
                
                TEXT_LABEL_15 tlBlock
                tlBlock = "multiplayer"
                tlBlock += iMPLockCheck
				
				APP_SET_BLOCK(tlBlock)
					INT iSlot
	                REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE iSlot
	                    tlBlock = "vehicle"
	                    tlBlock += iSlot
	                    APP_SET_BLOCK(tlBlock)
	                        APP_SET_INT_DATA("carUnlocked", 0)
	                    APP_CLOSE_BLOCK()
	                ENDREPEAT
				APP_CLOSE_BLOCK()
				
                bSaveCarData = TRUE SETTIMERA(0)
            APP_CLOSE_APP()
            
            IF (iMPLockCheck = 0)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_0, FALSE)
            ELIF (iMPLockCheck = 1)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_1, FALSE)
            ELIF (iMPLockCheck = 2)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_2, FALSE)
            ELIF (iMPLockCheck = 3)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_3, FALSE)
            ELIF (iMPLockCheck = 4)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_4, FALSE)
            ENDIF
        ENDIF
        
        iMPLockCheck++
        IF iMPLockCheck > 4
            iMPLockCheck = 0
        ENDIF
        
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     UPDATE THE UNLOCKS
    ///    
    IF NOT bProcessModUnlocks
        IF sData.bUpdateMods
        
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - updating unlocked mod states")
            
            sModUnlockBlockName = sBlockName
            sModUnlockVehicleBlockName = sVehicleBlockName
            eModUnlockModel = sData.eModel
            bProcessModUnlocks = TRUE
            bSaveCarData = TRUE SETTIMERA(0)
            
            sData.bUpdateMods = FALSE
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bUpdateMods = FALSE
			ENDIF
        ELSE
            // Periodically check if the mod unlocks have changed.
            IF NETWORK_IS_GAME_IN_PROGRESS()
            AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
            
                IF TIMERB() > 5000
                    BOOL bForceUpdate = FALSE
                    IF iModUnlockCheck = 0
                        INT iModUnlockBitset
                        GET_COLOUR_UNLOCKS_STAGE_1(iModUnlockBitset)
                        bForceUpdate = (iModUnlockBitset != iCached_UnlockBitset[0])
                        iModUnlockCheck++
                        SETTIMERB(0)
                    ELIF iModUnlockCheck = 1
                        INT iModUnlockBitset
                        GET_COLOUR_UNLOCKS_STAGE_2(iModUnlockBitset)
                        bForceUpdate = (iModUnlockBitset != iCached_UnlockBitset[1])
                        iModUnlockCheck++
                        SETTIMERB(0)
                    ELIF iModUnlockCheck = 2
                        INT iModUnlockBitsets[6]
                        IF GET_COLOUR_UNLOCKS_STAGE_3(iModUnlockBitsets, iModColourUnlockCheck) = -1
                            iModColourUnlockCheck = 0
                            iModUnlockCheck++
                            SETTIMERB(0)
                        ELSE
                            bForceUpdate = ((IS_BIT_SET(iModUnlockBitsets[iModColourUnlockCheck/32], iModColourUnlockCheck%32)) != (IS_BIT_SET(iCached_CarColoursUnlocked[iModColourUnlockCheck/32], iModColourUnlockCheck%32)))
                            iModColourUnlockCheck++
                            SETTIMERB(4900) // slight delay for next check
                        ENDIF
                    ENDIF
                    
                    IF bForceUpdate
                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - updating unlocked mod states (FORCED) ", iModUnlockCheck)
                        sModUnlockBlockName = sBlockName
                        sModUnlockVehicleBlockName = sVehicleBlockName
                        eModUnlockModel = sData.eModel
                        bProcessModUnlocks = TRUE
                        bSaveCarData = TRUE SETTIMERA(0)    
                    ENDIF
                    
                    IF iModUnlockCheck > 2
                        iModUnlockCheck = 0
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     PROCESS ORDER
    ///     
    IF sOrder.bProcessOrder
        IF NETWORK_IS_GAME_IN_PROGRESS()
            IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
            
                INT iOrderSlot = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iOrderVehicle
                
                IF g_MpSavedVehicles[iOrderSlot].vehicleSetupMP.VehicleSetup.eModel = sOrder.eModel
                    IF sOrder.bCheckPlateProfanity
                    
                        sOrder.bSCProfanityFailed = FALSE
						
						IF iSCTextCheckStage = 0
	                        IF SC_PROFANITY_GET_CHECK_IS_VALID(iSCProfanityToken)
	                            IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iSCProfanityToken)
	                                IF SC_PROFANITY_GET_STRING_PASSED(iSCProfanityToken)
	                                    iSCProfanityToken = 0
	                                    sOrder.bCheckPlateProfanity = FALSE
										
										#IF IS_DEBUG_BUILD
											IF bRunPlateChecks
												sOrder.bCheckPlateProfanity = TRUE
												iSCTextCheckStage++
											ENDIF
										#ENDIF
	                                ELSE
	                                    sOrder.bSCProfanityFailed = TRUE
										iSCProfanityToken = 0
	                                    sOrder.bCheckPlateProfanity = FALSE
	                                ENDIF
	                            ENDIF
	                        ELSE
	                            SC_PROFANITY_CHECK_STRING(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder.tlPlateText_pending, iSCProfanityToken)
	                        ENDIF
						ELIF iSCTextCheckStage = 1
						
							#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
								IF iSCLicensePlateToken = 0
									SC_LICENSEPLATE_ISVALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder.tlPlateText_pending, iSCLicensePlateToken)
								ELIF NOT SC_LICENSEPLATE_GET_ISVALID_IS_PENDING(iSCLicensePlateToken)
									CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - plate valid check finished in state ", SC_LICENSEPLATE_GET_ISVALID_STATUS(iSCLicensePlateToken))
									SWITCH SC_LICENSEPLATE_GET_ISVALID_STATUS(iSCLicensePlateToken)
										CASE LICENSEPLATE_ISVALID_OK
											iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
										BREAK
										DEFAULT
											sOrder.bSCProfanityFailed = TRUE
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
										BREAK
									ENDSWITCH
								ENDIF
							#ENDIF
							
							#IF NOT USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
								IF SC_LICENSEPLATE_GET_CHECK_IS_VALID(iSCLicensePlateToken)
								    IF NOT SC_LICENSEPLATE_GET_CHECK_IS_PENDING(iSCLicensePlateToken)
								        IF SC_LICENSEPLATE_GET_STRING_PASSED(iSCLicensePlateToken)
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
								        ELSE
								            sOrder.bSCProfanityFailed = TRUE
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
								        ENDIF
								    ENDIF
								ELSE
								    SC_LICENSEPLATE_CHECK_STRING(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder.tlPlateText_pending, iSCLicensePlateToken)
								ENDIF
							#ENDIF
						ENDIF
						
                    ELIF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
                        // wait for control
                    ELIF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
                        // wait for mission to end
						
					ELIF NOT HAS_IMPORTANT_STATS_LOADED()
						// wait for player to be in a game
						
                    ELIF sOrder.bSCProfanityFailed
                    	CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - send MP profanity text.")
						IF Request_MP_Comms_Txtmsg(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP2")
                        	sOrder.bProcessOrder = FALSE
                        	sOrder.bOrderPending = FALSE
                        	sOrder.bOrderReceivedOnBoot = FALSE
                        	sOrder.bOrderForPlayerVehicle = FALSE
						ENDIF
                        
                    ELIF bProcessMPOrderNow
                        sOrder.bOrderFailedFunds = FALSE
						
						// Fix for bug # 1687509 - Players are able to mod ifruit data so need to determine if order cost is valid or not.
						IF bCheckOrderCost
							IF NOT IS_MP_ORDER_VALID(sOrder, iOrderSlot)
								CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - MP order is not valid, someone has been messing with the prices so make them visit the mod shop, slot=", iOrderSlot)
								bProcessMPOrderNow = FALSE
							ENDIF
							bCheckOrderCost = FALSE
                        
						//ELIF (NETWORK_GET_VC_BANK_BALANCE()+NETWORK_GET_VC_WALLET_BALANCE() < sOrder.iCost)
						ELIF (sOrder.iCost > 0 AND NOT NETWORK_CAN_SPEND_MONEY(sOrder.iCost, FALSE, TRUE, FALSE))
						
							CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received on startup but player can't afford!")
							CPRINTLN(DEBUG_SOCIAL,"...order cost = ", sOrder.iCost)
							CPRINTLN(DEBUG_SOCIAL,"...player cash = ", NETWORK_GET_VC_WALLET_BALANCE())
							CPRINTLN(DEBUG_SOCIAL,"...player bank = ", NETWORK_GET_VC_BANK_BALANCE())
							
                            sOrder.bOrderFailedFunds = TRUE
                            bProcessMPOrderNow = FALSE
                            // this proc will fall through and send text message.
                        ELSE
                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received on startup so apply immediately!")
							
							IF NOT bPreProcessOrderDone
								COPY_CARAPP_ORDER_INTO_MP_SAVED_DATA(sOrder, FALSE)
								// Make sure this gets applied!
								SET_BIT(g_MpSavedVehicles[iOrderSlot].iVehicleBS,MP_SAVED_VEHICLE_REAPPLY_MODS)
								bPreProcessOrderDone = TRUE
								MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iOrderSlot,g_MpSavedVehicles[iOrderSlot],TRUE)
								REQUEST_SAVE(SSR_REASON_CAR_APP_UPDATE, STAT_SAVETYPE_IMMEDIATE_FLUSH)
							ENDIF
							
                            IF Request_MP_Comms_Txtmsg_With_Components(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP1", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sOrder.eModel))
                                BOOL fromBank = ((NETWORK_GET_VC_BANK_BALANCE()  >= sOrder.iCost))
								BOOL fromBankAndWallet = (NOT fromBank AND NETWORK_GET_VC_BANK_BALANCE() > 0)
								IF sOrder.iCost > 0
                                	NETWORK_BUY_ITEM(sOrder.iCost, ENUM_TO_INT(sOrder.eModel), PURCHASE_CARMODS, 1, fromBank, "CMOD_MAIN_0", GET_HASH_KEY(GET_SHOP_NAME(CARMOD_SHOP_01_AP)), GET_HASH_KEY("CMOD_MAIN_0"), 0, fromBankAndWallet)
								ENDIF
                                sOrder.bProcessOrder = FALSE
                                sOrder.bOrderPending = TRUE
                                sOrder.bOrderReceivedOnBoot = TRUE
                                bProcessMPOrderNow = FALSE
								COPY_CARAPP_ORDER_INTO_MP_SAVED_DATA(sOrder, TRUE)
                                SET_CAR_APP_ORDER_HAS_BEEN_PROCESSED(sOrder, CHAR_MULTIPLAYER)
								
								// Make sure this gets applied!
								SET_BIT(g_MpSavedVehicles[iOrderSlot].iVehicleBS,MP_SAVED_VEHICLE_REAPPLY_MODS)
								MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iOrderSlot,g_MpSavedVehicles[iOrderSlot],TRUE)
								REQUEST_SAVE(SSR_REASON_CAR_APP_UPDATE, STAT_SAVETYPE_IMMEDIATE_FLUSH)
                            ENDIF
                        ENDIF
                    ELSE
                        IF sOrder.bOrderFailedFunds
                            IF Request_MP_Comms_Txtmsg_With_Components(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP3", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sOrder.eModel))
                                sOrder.bProcessOrder = FALSE
                                sOrder.bOrderPending = TRUE
                                sOrder.bOrderReceivedOnBoot = FALSE
                                sOrder.bOrderForPlayerVehicle = FALSE
                            ENDIF
                        ELSE
                            IF Request_MP_Comms_Txtmsg_With_Components(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sOrder.eModel))
                                sOrder.bProcessOrder = FALSE
                                sOrder.bOrderPending = TRUE
                                sOrder.bOrderReceivedOnBoot = FALSE
                            ENDIF
                        ENDIF
                    ENDIF
                    bBlockFrameUpdate = TRUE
                ELSE
                    // Order doesn't match the vehicle in the slot so just ignore...
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - data doesn't match player vehicle data so cancel, slot=", iOrderSlot)
                    sOrder.bProcessOrder = FALSE
                    sOrder.bOrderPending = FALSE    
                    sOrder.bOrderReceivedOnBoot = FALSE 
                    sOrder.bOrderForPlayerVehicle = FALSE
                ENDIF
            ENDIF
        ELSE
            IF ePed = GET_CURRENT_PLAYER_PED_ENUM()
                IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sOrder.eModel
                OR g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sOrder.eModel
                OR g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_GARAGE][ePed].model = sOrder.eModel
                OR g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_MODDED][ePed].model = sOrder.eModel
                
                // Allow the default orders too...
                OR (ePed = CHAR_FRANKLIN AND sOrder.eModel = BUFFALO2 AND NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed])
                OR (ePed = CHAR_MICHAEL AND sOrder.eModel = TAILGATER AND NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed])
                OR (ePed = CHAR_TREVOR AND sOrder.eModel = BODHI2 AND NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed])
                    IF sOrder.bCheckPlateProfanity
                    
                        sOrder.bSCProfanityFailed = FALSE
						
						IF iSCTextCheckStage = 0
	                        IF SC_PROFANITY_GET_CHECK_IS_VALID(iSCProfanityToken)
	                            IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iSCProfanityToken)
	                                IF SC_PROFANITY_GET_STRING_PASSED(iSCProfanityToken)
	                                    iSCProfanityToken = 0
	                                    sOrder.bCheckPlateProfanity = FALSE
										
										#IF IS_DEBUG_BUILD
											IF bRunPlateChecks
												sOrder.bCheckPlateProfanity = TRUE
												iSCTextCheckStage++
											ENDIF
										#ENDIF
	                                ELSE
	                                    sOrder.bSCProfanityFailed = TRUE
	                                    iSCProfanityToken = 0
	                                    sOrder.bCheckPlateProfanity = FALSE
	                                ENDIF
	                            ENDIF
	                        ELSE
	                            SC_PROFANITY_CHECK_STRING(g_savedGlobals.sSocialData.sCarAppOrder[ePed].tlPlateText_pending, iSCProfanityToken)
	                        ENDIF
						ELIF iSCTextCheckStage = 1
						
							#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
								IF iSCLicensePlateToken = 0
									SC_LICENSEPLATE_ISVALID(g_savedGlobals.sSocialData.sCarAppOrder[ePed].tlPlateText_pending, iSCLicensePlateToken)
								ELIF NOT SC_LICENSEPLATE_GET_ISVALID_IS_PENDING(iSCLicensePlateToken)
									CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - plate valid check finished in state ", SC_LICENSEPLATE_GET_ISVALID_STATUS(iSCLicensePlateToken))
									SWITCH SC_LICENSEPLATE_GET_ISVALID_STATUS(iSCLicensePlateToken)
										CASE LICENSEPLATE_ISVALID_OK
											iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
										BREAK
										DEFAULT
											sOrder.bSCProfanityFailed = TRUE
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
										BREAK
									ENDSWITCH
								ENDIF
							#ENDIF
							
							#IF NOT USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
								IF SC_LICENSEPLATE_GET_CHECK_IS_VALID(iSCLicensePlateToken)
								    IF NOT SC_LICENSEPLATE_GET_CHECK_IS_PENDING(iSCLicensePlateToken)
								        IF SC_LICENSEPLATE_GET_STRING_PASSED(iSCLicensePlateToken)
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
								        ELSE
								            sOrder.bSCProfanityFailed = TRUE
								            iSCLicensePlateToken = 0
								            sOrder.bCheckPlateProfanity = FALSE
								        ENDIF
								    ENDIF
								ELSE
								    SC_LICENSEPLATE_CHECK_STRING(g_savedGlobals.sSocialData.sCarAppOrder[ePed].tlPlateText_pending, iSCLicensePlateToken)
								ENDIF
							#ENDIF
						ENDIF
						
                    ELIF sOrder.bSCProfanityFailed
                        // Do not send text on mission
                        IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
                        AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
                        AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
                            IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP2", TXTMSG_LOCKED)
                                sOrder.bProcessOrder = FALSE
                                sOrder.bOrderPending = FALSE
                                sOrder.bOrderReceivedOnBoot = FALSE
                                sOrder.bOrderForPlayerVehicle = FALSE
                            ENDIF
                        ENDIF
                        
                    ELIF bProcessOrderNow[ePed]
                        bProcessOrderNow[ePed] = FALSE
                        sOrder.bOrderFailedFunds = FALSE
                        sOrder.bOrderForPlayerVehicle = FALSE
                        
                        IF NOT CAN_PLAYER_AFFORD_ITEM_COST(sOrder.iCost)
                        AND (ePed != CHAR_FRANKLIN OR g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed])
                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received on startup but player can't afford!")
                            sOrder.bOrderFailedFunds = TRUE
                            // this proc will fall through and send text message.
                        ELSE
                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received on startup so apply immediately!")
                            sOrder.bProcessOrder = FALSE
                            sOrder.bOrderPending = TRUE
                            sOrder.bOrderReceivedOnBoot = TRUE
                            sOrder.bOrderForPlayerVehicle = bProcessOrderOnPlayerVehicle[ePed]
                            // player controller will drop this on the players vehicle when it spawns in...
                        ENDIF
                        
                        bProcessOrderOnPlayerVehicle[ePed] = FALSE
                    ELSE
                        IF sOrder.bOrderFailedFunds
                            // Do not send text on mission
                            IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
                            AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
                            AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
                                IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_LS_CUSTOMS, "SOCIAL_CARAPP3", TXTMSG_LOCKED, GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel)), -99, "NULL", STRING_COMPONENT)
                                    sOrder.bProcessOrder = FALSE
                                    sOrder.bOrderPending = TRUE
                                    sOrder.bOrderReceivedOnBoot = FALSE
                                    sOrder.bOrderForPlayerVehicle = FALSE
                                ENDIF
                            ENDIF
                        ELSE
                            // Do not send text on mission
                            IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
                            AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
                            AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
                                
                                TEXT_LABEL_15 tlLabel = "SOCIAL_CARAPP"
                                IF (ePed = CHAR_FRANKLIN AND NOT g_savedGlobals.sSocialData.bFirstOrderProcessed[ePed])
                                    tlLabel = "SOCIAL_FREE"
                                ENDIF
                                
                                IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_LS_CUSTOMS, tlLabel, TXTMSG_LOCKED, GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_savedGlobals.sSocialData.sCarAppOrder[ePed].eModel)), -99, "NULL", STRING_COMPONENT)
                                    sOrder.bProcessOrder = FALSE
                                    sOrder.bOrderPending = TRUE
                                    sOrder.bOrderReceivedOnBoot = FALSE
                                    sOrder.bOrderForPlayerVehicle = FALSE
                                    
                                    // Save the game as orders get wiped from cloud.
                                    MAKE_AUTOSAVE_REQUEST()
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                    bBlockFrameUpdate = TRUE
                ELSE
                    // Order doesn't match any of the stored vehicles so just ignore...
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - data doesn't match player vehicle data so cancel, slot=", ePed)
                    sOrder.bProcessOrder = FALSE
                    sOrder.bOrderPending = FALSE
                    sOrder.bOrderReceivedOnBoot = FALSE
                    sOrder.bOrderForPlayerVehicle = FALSE
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     GET DATA
    ///     
    BOOL bSafeToProcessOrder = TRUE
    // Only accept orders when the mod shop is available.
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF ePed != CHAR_MULTIPLAYER
        OR NOT IS_SHOP_AVAILABLE(CARMOD_SHOP_01_AP)
        OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
        OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
            bSafeToProcessOrder = FALSE
        ENDIF
    ELSE
        // IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY) // Allow orders to process on mission, just send text later.
        IF (NOT IS_PLAYER_PED_PLAYABLE(ePed))
        OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
            bSafeToProcessOrder = FALSE
        ENDIF
    ENDIF
    
    IF bSafeToProcessOrder
    AND NOT sOrder.bProcessOrder
    
//      INT i, j, k
//      VEHICLE_INDEX nearbyVehs[5]
//      TEXT_LABEL_31 tlGarageName
    
        REPEAT 2 iPass
            IF (iPass = 0)
            
                APP_SET_APP("car")
                    APP_SET_BLOCK(sOrderBlockName)
                    
                        // Fetch the data we require
                        IF APP_DATA_VALID()
                        
                            INT iUID = APP_GET_INT_DATA("uid")
                            IF (iUID != 0)
                            AND (iUID <> sOrder.iUID)
                            AND (APP_GET_INT_DATA("character") = iSlotID) // Make sure the order is for this character
                            AND (NOT NETWORK_IS_GAME_IN_PROGRESS() OR APP_GET_INT_DATA("vehicle") = iMPVehicleBlock) // Make sure the order is for this vehicle
                            
                                // We are now safe to access the fetched members
                                IF NETWORK_IS_GAME_IN_PROGRESS()
                                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - process UID: ", iUID, " for character ", iSlotID, " and vehicle ", iMPSavedVehicleSlot)
                                ELSE
                                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - process UID: ", iUID, " for character ", iSlotID)
                                ENDIF
                                
                                sOrder.iUID = iUID
                                sOrder.eModel = INT_TO_ENUM(MODEL_NAMES, APP_GET_INT_DATA("carModel"))
                                
                                IF NOT IS_MODEL_A_VEHICLE(sOrder.eModel)
                                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - model not valid, grabbing default")
                                    // The user must have used the app before the game so we should use the default player vehicle models.
                                    IF ePed = CHAR_MICHAEL
                                        sOrder.eModel = GET_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
                                    ELIF ePed = CHAR_FRANKLIN
                                        sOrder.eModel = GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR)
                                    ELIF ePed = CHAR_TREVOR
                                        sOrder.eModel = GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR)
                                    ELSE
                                        sOrder.eModel = BUFFALO2 // Default
                                    ENDIF
                                ENDIF
                                #IF IS_DEBUG_BUILD
								IF IS_MODEL_IN_CDIMAGE(sOrder.eModel)
		                        	CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carName = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sData.eModel), ", model = ", sData.eModel)
								ELSE
									CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - model = ", sData.eModel)
								ENDIF
								#ENDIF
                                sOrder.iCost = APP_GET_INT_DATA("cost")
                                sOrder.iColourID1 = APP_GET_INT_DATA("carColour1")
                                sOrder.iColourID2 = APP_GET_INT_DATA("carColour2")
                                sOrder.iColour1Group = APP_GET_INT_DATA("carColour1Group")
                                sOrder.iColour2Group = APP_GET_INT_DATA("carColour2Group")
                                sOrder.iWindowTint = APP_GET_INT_DATA("windowTint")
                                sOrder.bBulletProofTyres = INT_TO_BOOL(APP_GET_INT_DATA("bulletProofTyres"))
                                sOrder.iEngine = APP_GET_INT_DATA("carEngine")
                                sOrder.iBrakes = APP_GET_INT_DATA("carBrakes")
                                sOrder.iExhaust = APP_GET_INT_DATA("carExhaust")
                                sOrder.iWheels = APP_GET_INT_DATA("carWheel")
                                sOrder.iTyreSmokeR = APP_GET_INT_DATA("tyreSmokeColourRed")
                                sOrder.iTyreSmokeG = APP_GET_INT_DATA("tyreSmokeColourGreen")
                                sOrder.iTyreSmokeB = APP_GET_INT_DATA("tyreSmokeColourBlue")
                                sOrder.iHorn = APP_GET_INT_DATA("carHorn")
                                sOrder.iArmour = APP_GET_INT_DATA("carArmour")
                                sOrder.iTurbo = APP_GET_INT_DATA("carTurbo")
                                sOrder.iSuspension = APP_GET_INT_DATA("carSuspension")
                                sOrder.iLights = APP_GET_INT_DATA("carXenonLights")
                                sOrder.iTyreSmoke = APP_GET_INT_DATA("tyreSmoke")
                                
								sOrder.iWheelType = sData.iWheelType
								
								CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - iWheelType = ", sData.iWheelType)
								
                                bGetPlate = TRUE
                                
                                // Process order immediately checks
                                IF NOT NETWORK_IS_GAME_IN_PROGRESS()
                                    
                                    // Received order on startup
                                    IF (NOT bInitialOrderCheckComplete[ePed] AND (GET_GAME_TIMER() - iInitialOrderCheckTimer) < 20000)
                                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received within first 20 seconds - process now!")
                                        bProcessOrderNow[ePed] = TRUE
                                        
                                    // Received during armenian1
                                    ELIF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
                                    AND GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_FRANKLIN) = 0
                                    AND (g_savedGlobals.sFlow.isGameflowActive OR IS_REPEAT_PLAY_ACTIVE())
                                    AND (IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED) OR IS_REPEAT_PLAY_ACTIVE())
                                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received before armenian 1 - process now!")
                                        bProcessOrderNow[ePed] = TRUE
                                        bProcessOrderOnPlayerVehicle[ePed] = TRUE
                                        
                                    // For player vehicle that hasn't been created
                                    ELIF (ePed = CHAR_MICHAEL AND sOrder.eModel = TAILGATER)
                                    OR (ePed = CHAR_FRANKLIN AND sOrder.eModel = BUFFALO2)
                                    OR (ePed = CHAR_FRANKLIN AND sOrder.eModel = BAGGER)
                                    OR (ePed = CHAR_TREVOR AND sOrder.eModel = BODHI2)
                                        IF GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(ePed, sOrder.eModel) = 0
                                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received and player vehicle doesnt exist - process now!")
                                            bProcessOrderNow[ePed] = TRUE
                                            bProcessOrderOnPlayerVehicle[ePed] = TRUE
                                        ENDIF
                                    
                                    // For garage vehicle and the vehicle doesnt exist
                                    /*ELIF g_savedGlobals.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed] != NUMBER_OF_SAVEHOUSE_LOCATIONS
                                        
                                        BOOL bVehicleInGarage = FALSE
                                        WHILE GET_PLAYER_GARAGE_DATA(ePed, i, tlGarageName, eSavehouse)
                                            IF eSavehouse = g_savedGlobals.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed]
                                                j = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
                                                REPEAT j k
                                                    IF DOES_ENTITY_EXIST(nearbyVehs[k])
                                                    AND IS_VEHICLE_DRIVEABLE(nearbyVehs[k])
                                                    AND IS_VEHICLE_IN_GARAGE_AREA(tlGarageName, nearbyVehs[k])
                                                    AND sOrder.eModel = GET_ENTITY_MODEL(nearbyVehs[k])
                                                        bVehicleInGarage = TRUE
                                                        k=100 //Bail
                                                    ENDIF
                                                ENDREPEAT
                                                i=100 //Bail
                                            ENDIF
                                            i++
                                        ENDWHILE
                                        
                                        IF NOT bVehicleInGarage
                                            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received and vehicle doesnt exist in garage - process now!")
                                            bProcessOrderNow[ePed] = TRUE
                                        ENDIF*/
                                    ENDIF
                                    
                                    IF NOT bProcessOrderNow[ePed]
                                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received, order time = ", (iInitialOrderCheckTimer - GET_GAME_TIMER()))
                                    ENDIF
                                    
                                ELSE
									// Plates now cost $100k in GTA Online so we should force the player to go to the mod shop to confirm their purchase.
									#IF NOT USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
	                                    IF iMPSavedVehicleSlot >= 0
										AND (g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != iMPSavedVehicleSlot OR NOT IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED) OR (IS_PLAYER_SWITCH_IN_PROGRESS() AND GET_PLAYER_SWITCH_STATE() <= SWITCH_STATE_JUMPCUT_DESCENT))
										AND NOT (IS_BIT_SET(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE) OR (IS_PLAYER_SWITCH_IN_PROGRESS() AND GET_PLAYER_SWITCH_STATE() <= SWITCH_STATE_JUMPCUT_DESCENT))
										AND NOT MPGlobals.VehicleData.bAssignToMainScript
										AND NOT IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), TRUE)
		                                AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), TRUE)
											CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - order received and mp player vehicle is not created - process now!")
											CPRINTLN(DEBUG_SOCIAL,"...iMPSavedVehicleSlot = ", iMPSavedVehicleSlot)
											CPRINTLN(DEBUG_SOCIAL,"...g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed)
											CPRINTLN(DEBUG_SOCIAL,"...IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED = ", IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED))
		                                    bProcessMPOrderNow = TRUE
											bCheckOrderCost = TRUE
											bPreProcessOrderDone = FALSE
										ELSE
											CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - unable to process mp order automatically")
											CPRINTLN(DEBUG_SOCIAL,"...iMPSavedVehicleSlot = ", iMPSavedVehicleSlot)
											CPRINTLN(DEBUG_SOCIAL,"...g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed)
											CPRINTLN(DEBUG_SOCIAL,"...IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED) = ", IS_SAVED_VEHICLE_FLAG_SET( MP_SAVED_VEH_FLAG_CREATED))
											CPRINTLN(DEBUG_SOCIAL,"...MPGlobals.VehicleData.bAssignToMainScript = ", MPGlobals.VehicleData.bAssignToMainScript)
											CPRINTLN(DEBUG_SOCIAL,"...IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), TRUE) = ", IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), TRUE))
											CPRINTLN(DEBUG_SOCIAL,"...IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), TRUE) = ", IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), TRUE))
											bProcessMPOrderNow = FALSE
										ENDIF
										
										#IF IS_DEBUG_BUILD
											IF bAlwaysAllowAutoProcess
												bProcessMPOrderNow = TRUE
												bCheckOrderCost = TRUE
												bPreProcessOrderDone = FALSE
											ENDIF
										#ENDIF
									#ENDIF
                                ENDIF
                            ENDIF
                            
                            // App data valid so clear initial order check
                            IF NOT NETWORK_IS_GAME_IN_PROGRESS()
                                bInitialOrderCheckComplete[ePed] = TRUE
                            ENDIF
                        ENDIF
                    
                    APP_CLOSE_BLOCK() // order
                APP_CLOSE_APP() // car
                
            ELIF (iPass = 1 AND bGetPlate)
            
                APP_SET_APP("car")
                APP_SET_BLOCK("plate")
                
                IF APP_DATA_VALID()
                    sOrder.iPlateBack_pending = APP_GET_INT_DATA("carPlateBack") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP_PLATE - cloud plate back: ", sOrder.iPlateBack_pending)
					
					// Validate plate background
					IF sOrder.iPlateBack_pending > 4
						sOrder.iPlateBack_pending = 0
						sOrder.iPlateBack_pending = APP_GET_INT_DATA("carPlateBack") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP_PLATE - cloud plate back overriden: ", sOrder.iPlateBack_pending)
					ENDIF
					
					sOrder.tlPlateText_pending = APP_GET_STRING_DATA("carPlateText") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP_PLATE - cloud plate text: ", sOrder.tlPlateText_pending)
                ENDIF
                APP_CLOSE_BLOCK()
                APP_CLOSE_APP()
                
                // Ready to send text and process in LSC
                sOrder.bProcessOrder = TRUE
                sOrder.bCheckPlateProfanity = TRUE
                sOrder.bOrderFailedFunds = FALSE
                sOrder.bOrderReceivedOnBoot = FALSE
                sOrder.bOrderPaidFor = FALSE
                
                IF NOT NETWORK_IS_GAME_IN_PROGRESS()
                    STAT_SET_BOOL(CAR_MOD_APP_USED, TRUE)
                    BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_CARAPP)       //#1514495 - stockmarket
                    g_savedGlobals.sSocialData.bCarAppUsed = TRUE
                ELSE
                    g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iOrderVehicle = iMPSavedVehicleSlot
                    SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_MOD_CAR_USING_APP, TRUE)
                ENDIF
				
				#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
					bRebuildPlatesList = TRUE
				#ENDIF
            ENDIF
        ENDREPEAT
    ENDIF
    
    ///////////////////////////////////////////////////////
    ///     
    ///     SET DATA
    ///     
    IF NOT sData.bDeleteData
	AND bSafeToProcessOrder
    AND sData.bSendDataToCloud
    
		// Fetch static data for MP
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NOT FILL_STATIC_CAR_APP_DATA(sData)
				// Need to wait until we have the data.
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - fetching static")
				ENDIF
				#ENDIF
				bBlockFrameUpdate = TRUE
				EXIT
			ENDIF
		ENDIF
		
        IF NETWORK_IS_GAME_IN_PROGRESS()
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - sending vehicle data to cloud: ", sBlockName, " for vehicle ", iMPSavedVehicleSlot, " in vehicle block ", iMPVehicleBlock)
        ELSE
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - sending vehicle data to cloud: ", sBlockName)
        ENDIF
        
        APP_SET_APP("car")
            APP_SET_BLOCK("appdata")
                INT iGameUID = GENERATE_RANDOM_UNIQUE_ID()
                IF iGameUID = 0
                    iGameUID = GENERATE_RANDOM_UNIQUE_ID()
                ENDIF
                APP_SET_INT_DATA("appUID", iGameUID)
                g_savedGlobals.sSocialData.iGameUID = iGameUID
            
                // DISCOUNTS
                INT iSPDiscount = 0
                INT iMPDiscount = 0
                
                // - 5% for 95% positive reports
                IF HAS_IMPORTANT_STATS_LOADED()
                    IF SHOULD_GIVE_SHOP_DISCOUNT()
                        iMPDiscount += g_sMPTunables.iShopDiscountPercentValue
                    ENDIF
                ENDIF
                
//                // 20% discount for SE
//                IF IS_SPECIAL_EDITION_GAME() 
//                OR IS_COLLECTORS_EDITION_GAME() 
//                    iSPDiscount += 20
//                ENDIF
                
                APP_SET_FLOAT_DATA("spDiscount", (1.0 - (TO_FLOAT(iSPDiscount) / 100)))
                APP_SET_FLOAT_DATA("mpDiscount", (1.0 - (TO_FLOAT(iMPDiscount) / 100)))
				
				IF NETWORK_IS_SIGNED_ONLINE()
					CPRINTLN(DEBUG_SOCIAL,"playerName = ", GET_PLAYER_NAME(PLAYER_ID()), " with hash ", GET_HASH_KEY((GET_PLAYER_NAME(PLAYER_ID()))), ", mp=", NETWORK_IS_GAME_IN_PROGRESS())
					APP_SET_INT_DATA("playerName", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
				
					IF NETWORK_IS_GAME_IN_PROGRESS()
						APP_SET_INT_DATA("playerNameMP", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
					ELSE
						APP_SET_INT_DATA("playerNameSP", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
					ENDIF
				ENDIF
                
            APP_CLOSE_BLOCK()
        
            APP_SET_BLOCK(sBlockName)
            
                IF NETWORK_IS_GAME_IN_PROGRESS()
                    APP_SET_BLOCK(sVehicleBlockName)
                ENDIF
				
					IF NOT IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(sData.eModel)
						APP_SET_INT_DATA("carUnlocked", 0)
						CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - invalid vehicle model (", GET_MODEL_NAME_FOR_DEBUG(sData.eModel), ") - locking!")
					ELSE
	                    APP_SET_INT_DATA("carUnlocked", 1)
	                    
	                    
	                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       Special cases that we dont want to send to the app
	                    IF (sData.eModel = SANCHEZ)
	                    OR (sData.eModel = FAGGIO2)
	                    OR (sData.eModel = DUNE)
	                    OR (sData.eModel = BFINJECTION)
						OR NOT IS_THIS_WHEEL_TYPE_ALLOWED_FOR_CARAPP_ORDER(INT_TO_ENUM(MOD_WHEEL_TYPE, sData.iWheelType))
	                        sData.iModCountWheels = 0
	                    ENDIF
						
						IF IS_VEHICLE_A_SUPERMOD_MODEL(sData.eModel, SUPERMOD_FLAG_HAS_HYDRAULICS)
							sData.iModCountSuspension = 0
						ENDIF
						
						IF (sData.eModel = LECTRO)
	                    OR (sData.eModel = VINDICATOR)
							sData.iModCountHorn = 0
						ENDIF
	                    
						IF !NETWORK_IS_GAME_IN_PROGRESS()
							IF sData.eModel = PATRIOT
								sData.iModCountExhaust = 0
								CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_AP iExhaust setting to = 0")
							ENDIF
							IF sData.eModel = PRAIRIE 
								IF sData.iModCountExhaust > 1
									 CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_AP iExhaust setting to = 1 it was: ", sData.iModCountExhaust)
									sData.iModCountExhaust = 1
								ENDIF 
							ENDIF
						ENDIF
						
						IF sData.iLights > 1
							CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_AP iLights setting to = 0 it was: ", sData.iLights)
							sData.iLights = 0
						ENDIF
						
						// limitation on the iFruit side doesn't handle Tire Designs 
						INT iWheelIndexForVariation = GET_VEHICLE_MOD_WHEEL_INDEX_FOR_VARIATION(sData)
						PRINTLN("social_controller: DO_PROCESS_CAR_AP sData.iWheels ", sData.iWheels, " wheel index for variation: ", iWheelIndexForVariation)
						sData.iWheels = iWheelIndexForVariation
						
						///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       Player info, vehicle name, number plate background
	                    IF NETWORK_IS_GAME_IN_PROGRESS()
	                        APP_SET_INT_DATA("playerSlot", iMPCharSlot)
	                        APP_SET_INT_DATA("playerRank", GET_PLAYER_FM_RANK(PLAYER_ID()))
	                        
	                        SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
	                            CASE MP_M_FREEMODE_01
	                            CASE MP_F_FREEMODE_01
	                                APP_SET_STRING_DATA("playerGang", "None")   CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - playerGang = None")
	                                GET_HUD_COLOUR(HUD_COLOUR_FREEMODE, iR, iG, iB, iA)
	                            BREAK
	                        ENDSWITCH
	                        APP_SET_INT_DATA("playerGangR", iR)
	                        APP_SET_INT_DATA("playerGangG", iG)
	                        APP_SET_INT_DATA("playerGangB", iB)
	                    ENDIF
	                    
	                    IF NOT IS_MODEL_A_VEHICLE(sData.eModel)
	                        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carName = DEFAULT, model = ", sData.eModel)
	                        APP_SET_INT_DATA("carModel", -1)
	                        APP_SET_INT_DATA("carUnlocked", 0)
	                    ELSE
							#IF IS_DEBUG_BUILD
							IF IS_MODEL_IN_CDIMAGE(sData.eModel)
	                        	CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carName = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sData.eModel), ", model = ", sData.eModel)
							ELSE
								CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - model = ", sData.eModel)
							ENDIF
							#ENDIF
	                        APP_SET_INT_DATA("carModel", ENUM_TO_INT(sData.eModel))
	                    ENDIF
						
						
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
							SWITCH sData.eModKitType
		                        CASE MKT_STANDARD
		                            IF IS_THIS_MODEL_A_CAR(sData.eModel)
		                                APP_SET_STRING_DATA("carType", "car") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = car")
		                            ELSE
		                                APP_SET_STRING_DATA("carType", "bike") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = bike")
		                            ENDIF
		                        BREAK
		                        CASE MKT_SPORT      APP_SET_STRING_DATA("carType", "sport") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = sport")  BREAK
		                        CASE MKT_SUV        APP_SET_STRING_DATA("carType", "suv") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = suv")  BREAK
		                        CASE MKT_SPECIAL    APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special")  BREAK
		                    ENDSWITCH
						ELSE
							INT iPriceVariation = GET_VEHICLE_MOD_PRICE_VARIATION_FOR_CATALOGUE(sData.eModel)
		                    SWITCH INT_TO_ENUM(MOD_PRICE_VARIATION_ENUM, iPriceVariation)
								CASE MPV_STANDARD 	APP_SET_STRING_DATA("carType", "car") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = car ***") BREAK
								CASE MPV_SPORT 		APP_SET_STRING_DATA("carType", "sport") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = sport ***") BREAK
								CASE MPV_SUV 		APP_SET_STRING_DATA("carType", "suv") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = suv ***") BREAK
								CASE MPV_SPECIAL 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_BIKE 		APP_SET_STRING_DATA("carType", "bike") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = bike ***") BREAK
								CASE MPV_BTYPE3 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_SULTANRS 	APP_SET_STRING_DATA("carType", "car") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = car ***") BREAK
								CASE MPV_BANSHEE2 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_VIRGO2 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_SLAMVAN3 	APP_SET_STRING_DATA("carType", "car") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = car ***") BREAK
								CASE MPV_IE_BIKE 	APP_SET_STRING_DATA("carType", "car") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = car ***") BREAK
								CASE MPV_IE_RETRO 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_IE_HIGH 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_GR_HEAVY 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_GR_LIGHT 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_GR_BIKE 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								CASE MPV_GR_TRAILERLARGE 	APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special ***") BREAK
								DEFAULT
									//SCRIPT_ASSERT("DO_PROCESS_CAR_APP - Missing mod price variation lookup")
									APP_SET_STRING_DATA("carType", "special") CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carType = special (DEFAULT) ***")
								BREAK
							ENDSWITCH
						ENDIF
						
						///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       All the mods
	                    APP_SET_INT_DATA("carColour1", sData.iColourID1) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColour1 = ",  sData.iColourID1)
	                    APP_SET_INT_DATA("carColour2", sData.iColourID2) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carColour2 = ",  sData.iColourID2)
	                    APP_SET_INT_DATA("windowTint", sData.iWindowTint) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - windowTint = ",  sData.iWindowTint)
	                    APP_SET_INT_DATA("bulletProofTyres", BOOL_TO_INT(sData.bBulletProofTyres))
	                    APP_SET_INT_DATA("carEngine", sData.iEngine) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carEngine = ",  sData.iEngine)
	                    APP_SET_INT_DATA("carBrakes", sData.iBrakes) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carBrakes = ",  sData.iBrakes)
	                    APP_SET_INT_DATA("carExhaust", sData.iExhaust) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carExhaust = ",  sData.iExhaust)
	                    APP_SET_INT_DATA("carWheel", sData.iWheels) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carWheel = ",  sData.iWheels)
	                    APP_SET_INT_DATA("tyreSmokeColourRed", sData.iTyreSmokeR) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - tyreSmokeColourRed = ",  sData.iTyreSmokeR)
	                    APP_SET_INT_DATA("tyreSmokeColourGreen", sData.iTyreSmokeG) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - tyreSmokeColourGreen = ",  sData.iTyreSmokeG)
	                    APP_SET_INT_DATA("tyreSmokeColourBlue", sData.iTyreSmokeB) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - tyreSmokeColourBlue = ",  sData.iTyreSmokeB)
	                    APP_SET_INT_DATA("carHorn", sData.iHorn) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn = ",  sData.iHorn)
	                    APP_SET_INT_DATA("carArmour", sData.iArmour) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carArmour = ",  sData.iArmour)
	                    APP_SET_INT_DATA("carTurbo", sData.iTurbo) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carTurbo = ",  sData.iTurbo)
	                    APP_SET_INT_DATA("carSuspension", sData.iSuspension) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carSuspension = ",  sData.iSuspension)
	                    APP_SET_INT_DATA("carXenonLights", sData.iLights) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carXenonLights = ",  sData.iLights)
	                    APP_SET_INT_DATA("tyreSmoke", sData.iTyreSmoke) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - tyreSmoke = ",  sData.iTyreSmoke)
	                    APP_SET_INT_DATA("tyreSmoke", sData.iTyreSmoke) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - tyreSmoke = ",  sData.iTyreSmoke)
	                    APP_SET_INT_DATA("carWheelType", sData.iWheelType) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carWheelType = ",  sData.iWheelType)
	                    
	                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       Mod counts
	                    APP_SET_INT_DATA("carEngineCount", sData.iModCountEngine) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carEngineCount = ",  sData.iModCountEngine)
	                    APP_SET_INT_DATA("carBrakesCount", sData.iModCountBrakes) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carBrakesCount = ",  sData.iModCountBrakes)
	                    APP_SET_INT_DATA("carExhaustCount", sData.iModCountExhaust) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carExhaustCount = ",  sData.iModCountExhaust)
	                    APP_SET_INT_DATA("carWheelCount", sData.iModCountWheels) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carWheelCount = ",  sData.iModCountWheels)
	                    APP_SET_INT_DATA("carHornCount", sData.iModCountHorn) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHornCount = ",  sData.iModCountHorn)
	                    APP_SET_INT_DATA("carArmourCount", sData.iModCountArmour) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carArmourCount = ",  sData.iModCountArmour)
	                    APP_SET_INT_DATA("carSuspensionCount", sData.iModCountSuspension) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carSuspensionCount = ",  sData.iModCountSuspension)
	                    
	                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       Horn hash
	                    APP_SET_INT_DATA("carHorn1", sData.iHornHash[0]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn1 = ", sData.iHornHash[0])
	                    APP_SET_INT_DATA("carHorn2", sData.iHornHash[1]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn2 = ", sData.iHornHash[1])
	                    APP_SET_INT_DATA("carHorn3", sData.iHornHash[2]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn3 = ", sData.iHornHash[2])
	                    APP_SET_INT_DATA("carHorn4", sData.iHornHash[3]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn4 = ", sData.iHornHash[3])
	                    APP_SET_INT_DATA("carHorn5", sData.iHornHash[4]) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carHorn5 = ", sData.iHornHash[4])
	                    
	                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       Price modifier
	                    APP_SET_FLOAT_DATA("carPriceModifier", sData.fModPriceModifier) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carPriceModifier = ",  sData.fModPriceModifier)
	                    
	                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       All the unlocks
	                    IF NOT IS_BIT_SET(sData.iModColoursThatCanBeSet, 0) // Primary Colour
						OR sData.eModel = SOVEREIGN
	                        APP_SET_INT_DATA("carColour1Unlocked", 0)
	                    ELSE
	                        APP_SET_INT_DATA("carColour1Unlocked", 1)
	                    ENDIF
	                    
	                    IF NOT IS_BIT_SET(sData.iModColoursThatCanBeSet, 1) // Secondary Colour
						OR sData.eModel = SOVEREIGN
	                        APP_SET_INT_DATA("carColour2Unlocked", 0)
	                    ELSE
	                        APP_SET_INT_DATA("carColour2Unlocked", 1)
	                    ENDIF
						
						///////////////////////////////////////////////////////////////////////////////////////////////////////////
	                    ///       New item unlocks - patriot smoke, indi horns
						INT iNewUnlockFlags = 0
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						OR (IS_BIT_SET(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_BOUGHT_INDI_SMOKE))
							SET_BIT(iNewUnlockFlags, 0)
						ENDIF
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						OR (IS_BIT_SET(g_MpSavedVehicles[iMPSavedVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_BOUGHT_INDI_HORN))
							SET_BIT(iNewUnlockFlags, 1)
						ENDIF
						APP_SET_INT_DATA("newItemUnlocks", iNewUnlockFlags)
					ENDIF
                    
                IF NETWORK_IS_GAME_IN_PROGRESS()
                    APP_CLOSE_BLOCK()
                ENDIF
                
            // Done
            APP_CLOSE_BLOCK()
            
            sModUnlockBlockName = sBlockName
            sModUnlockVehicleBlockName = sVehicleBlockName
            eModUnlockModel = sData.eModel
            bProcessModUnlocks = TRUE
            
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - sending vehicle plate to cloud: ", sBlockName)
            APP_SET_BLOCK("plate")
                APP_SET_INT_DATA("carPlateBack", sData.iPlateBack) CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - carPlateBack = ", sData.iPlateBack)
            APP_CLOSE_BLOCK()
        
        APP_CLOSE_APP()
        
        // Done updating
        sData.bSendDataToCloud = FALSE
        
        bSaveCarData = TRUE SETTIMERA(0)
        
        IF NOT NETWORK_IS_GAME_IN_PROGRESS()
            g_savedGlobals.sSocialData.bFirstVehicleSentToCloud[ePed] = TRUE
        ELSE
            IF (iMPCharSlot = 0)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_0, TRUE)
            ELIF (iMPCharSlot = 1)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_1, TRUE)
            ELIF (iMPCharSlot = 2)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_2, TRUE)
            ELIF (iMPCharSlot = 3)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_3, TRUE)
            ELIF (iMPCharSlot = 4)
                SET_MP_BOOL_PLAYER_STAT(MPPLY_CARAPP_DATA_SENT_4, TRUE)
            ENDIF
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bCarHiddenInApp[iMPVehicleBlock] = FALSE
        ENDIF
    ENDIF
    
    // Update saved globals
    SWITCH ePed
        CASE CHAR_MICHAEL
        CASE CHAR_FRANKLIN
        CASE CHAR_TREVOR
            g_savedGlobals.sSocialData.sCarAppData[ePed] = sData
            g_savedGlobals.sSocialData.sCarAppOrder[ePed] = sOrder
        BREAK
        CASE CHAR_MULTIPLAYER
			UPDATE_SAVED_MP_CAR_APP_DATA(sData)
            g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder = sOrder
        BREAK
    ENDSWITCH
ENDPROC

/// PURPOSE: Attempts to fetch the dog app data and sends txt if successful.
PROC DO_PROCESS_DOG_APP()

    // Only process in singleplayer
    IF NETWORK_IS_GAME_IN_PROGRESS()
        EXIT
    ENDIF
    
    // Only process off mission
    IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
        EXIT
    ENDIF
    
    
    ///////////////////////////////////////////////////////
    ///     
    ///     SET DATA
    ///     
    IF g_bPlayerHasKilledChop
        IF NOT bChopDeathProcessed
        
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DOG_APP - processing Chop death")
            
            APP_SET_APP("dog")
            APP_SET_BLOCK("saveData")
            APP_SET_INT_DATA("chopKilled", GET_RANDOM_INT_IN_RANGE())
            APP_SAVE_DATA()
            APP_CLOSE_BLOCK()
            APP_CLOSE_APP()
            
            bChopDeathProcessed = TRUE
        ENDIF
    ELSE
        bChopDeathProcessed = FALSE
    ENDIF
    
    
    ///////////////////////////////////////////////////////
    ///     
    ///     GET DATA
    ///     
    APP_SET_APP("dog")
    APP_SET_BLOCK("saveData")
    
    IF APP_DATA_VALID()
        // Only progress if the fetched block is valid
        //IF APP_BLOCK_VALID()
        
            // We are now safe to access the fetched members
            //CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DOG_APP - processing stats")
            
            INT happinessTimer = APP_GET_INT("happinessTimer")          
            IF happinessTimer = 0
                //Default happiness timer if this value hasn't been set yet
                happinessTimer = 450        
            ENDIF
            
            INT happinessDecrement = APP_GET_INT("happinessDecrement")          
            IF happinessDecrement = 0
                //Default happiness decrement if this value hasn't been set yet
                happinessDecrement = 1
            ENDIF
            
            INT appTime = APP_GET_INT("time_stamp")
            INT cloudTime = GET_CLOUD_TIME_AS_INT()
            
            IF cloudTime = 0
                //Default cloud time to app time incase its zero
                cloudTime = appTime
            ENDIF
            
            INT currentHappiness = APP_GET_INT("happiness")
            INT time_stamp = cloudTime - appTime
            INT happinessDecrease = ( time_stamp / happinessTimer ) * happinessDecrement
            INT happiness = CLAMP_INT(currentHappiness - happinessDecrease, 0, 100)
            
            g_savedGlobals.sSocialData.sDogAppData.fHappiness = TO_FLOAT(happiness)
            g_savedGlobals.sSocialData.sDogAppData.fCleanliness = APP_GET_FLOAT("cleanliness")
            g_savedGlobals.sSocialData.sDogAppData.fHunger = APP_GET_FLOAT("hunger")
            g_savedGlobals.sSocialData.sDogAppData.iTrainingLevel = APP_GET_INT_DATA("trainingLevel")
            g_savedGlobals.sSocialData.sDogAppData.iCollar = APP_GET_INT_DATA("collar")
            
            g_savedGlobals.sSocialData.sDogAppData.bAppDataReceived = TRUE
            
            // Update stock market modifier
            IF g_savedGlobals.sSocialData.sDogAppData.fHappiness > 50
                ////BAWSAQ_INCREMENT_MODIFIER(BSMF_HAPPYDOGTIME, 1)
            ELSE
                ////BAWSAQ_INCREMENT_MODIFIER(BSMF_UNHAPPYDOGTIME, 1)
            ENDIF
            
            IF g_savedGlobals.sSocialData.sDogAppData.fHunger > 50
                ////BAWSAQ_INCREMENT_MODIFIER(BSMF_DOGFED, 0)
            ELSE
                ////BAWSAQ_INCREMENT_MODIFIER(BSMF_DOGFED, 1)
            ENDIF
            
            // Flag that we have used the app
            g_savedGlobals.sSocialData.bDogAppUsed = (APP_GET_INT_DATA("usedApp") != 0)
            
            BOOL bStatState
           	IF STAT_GET_BOOL(CHOP_APP_USED, bStatState)
				IF NOT bStatState
		            IF g_savedGlobals.sSocialData.bDogAppUsed
		                STAT_SET_BOOL(CHOP_APP_USED, TRUE)
		            ENDIF
				ENDIF
			ENDIF
        //ENDIF
    ENDIF
    APP_CLOSE_BLOCK()
    APP_CLOSE_APP()
    
    
    IF g_savedGlobals.sSocialData.bUpdateDogLocation
        APP_SET_APP("dog")
        APP_SET_BLOCK("saveData")
            IF Is_Savehouse_Respawn_Available(SAVEHOUSE_FRANKLIN_VH)
                APP_SET_INT_DATA("chopSafeHouse", 1)
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DOG_APP - chopSafeHouse = 1")
            ELSE
                APP_SET_INT_DATA("chopSafeHouse", 0)
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_DOG_APP - chopSafeHouse = 0")
            ENDIF
        APP_SAVE_DATA()
        APP_CLOSE_BLOCK()
        APP_CLOSE_APP()
        g_savedGlobals.sSocialData.bUpdateDogLocation = FALSE
    ENDIF
ENDPROC

/// PURPOSE: Checks to see if the social controller should be re-initialised
PROC CHECK_CONTROLLER_RESET()
    
    IF eStage != SOCIAL_STAGE_INIT
    AND eInit != NOT_INITIALISED
        // Reset if we have changed game modes
        IF eInit = INITIALISED_IN_SP
            IF NETWORK_IS_GAME_IN_PROGRESS()
                eInit = NOT_INITIALISED
            ENDIF
        ELIF eInit = INITIALISED_IN_MP_FM
            IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND NOT IS_TRANSITION_SESSION_LAUNCHING() AND NOT IS_TRANSITION_SESSION_KILLING_SESSION())
                eInit = NOT_INITIALISED
            ENDIF
            IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM 
                eInit = NOT_INITIALISED
            ENDIF
        ENDIF
        
        IF NOT APP_HAS_LINKED_SOCIAL_CLUB_ACCOUNT()
            eInit = NOT_INITIALISED
        ENDIF
        
        IF g_bResetSocialController #IF IS_DEBUG_BUILD OR bResetController_Debug #ENDIF
            eInit = NOT_INITIALISED
        ENDIF
        
        
        
        // Jump back to the initialisation stage if we have switched game modes
        IF eInit = NOT_INITIALISED
            eStage = SOCIAL_STAGE_INIT
            PRINTSTRING("\n social_controller: RESET")PRINTNL()
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Wait for the game to be in the correct state before proceeding
PROC DO_INITIALISE()

    #IF IS_DEBUG_BUILD
        IF bBlockInit_Debug
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - blocked by debug widget")
            EXIT
        ENDIF
    #ENDIF

    // Wait for account to be linked with social club
    IF NOT APP_HAS_LINKED_SOCIAL_CLUB_ACCOUNT() OR NOT APP_HAS_SYNCED_DATA("car")
        
        #IF IS_DEBUG_BUILD
            IF NOT bDebug_FailForLinkedAcccount
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - not linked to Social Club account")
                bDebug_FailForLinkedAcccount = TRUE
            ENDIF
        #ENDIF
        bWasLinkedLastFrame = FALSE
        EXIT
	ELSE
		//If the player is linked but wasn't during the last check , do full cleanup
		IF NOT bWasLinkedLastFrame
			bResendAppDataForAccountLinkSP = TRUE
			bResendAppDataForAccountLinkMP = TRUE
			bCheckPlayerNameDiffSP = TRUE
			bCheckPlayerNameDiffMP = TRUE
			
			// Re-grab SC plates
			PRINTLN("[PURCHASED PLATES] Changed account, re-grab plates.")
			g_bRebuildSCPlateList = TRUE
			
			#IF IS_DEBUG_BUILD
				 CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - player has linked a different Social Club account")
			#ENDIF
    	ENDIF
		bWasLinkedLastFrame = TRUE
    ENDIF
    
    #IF IS_DEBUG_BUILD
        bDebug_FailForLinkedAcccount = FALSE
    #ENDIF

    // Wait for MP player to be in main game
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
        
            #IF IS_DEBUG_BUILD
                IF NOT bDebug_FailForGamemode
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - not in FM")
                    bDebug_FailForGamemode = TRUE
                ENDIF
            #ENDIF
        
            EXIT
        ENDIF
		
		IF NOT HAS_IMPORTANT_STATS_LOADED()
		
			#IF IS_DEBUG_BUILD
                IF NOT bDebug_FailForGamemode
                    CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - waiting on important stats loading")
                    bDebug_FailForGamemode = TRUE
                ENDIF
            #ENDIF
			
            EXIT
		ENDIF
    ENDIF
    
    #IF IS_DEBUG_BUILD
        bDebug_FailForGamemode = FALSE
    #ENDIF
    
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
            CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - initialised in FM")
            eInit = INITIALISED_IN_MP_FM
        ENDIF
		
		// Initialise the new app update
		IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bNewSetupInitialised
			INT i
			FOR i = MAX_MP_VEHICLE_APP_CAN_HANDLE-1 TO 0 STEP -1
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = i+1
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] = i+1
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bCarHiddenInApp[i] = FALSE
			ENDFOR
			
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iLastSavedVehUsed = -1
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.bNewSetupInitialised = TRUE
		ENDIF
		
		bResendAppDataForCharacter0 = GET_PACKED_STAT_BOOL(PACKED_MP_RESEND_APP_DATA_FOR_CHARACTER_0)
		bResendAppDataForCharacter1 = GET_PACKED_STAT_BOOL(PACKED_MP_RESEND_APP_DATA_FOR_CHARACTER_1)
		
		IF IS_LAST_GEN_PLAYER()
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_RESEND_APP_DATA_FOR_PLAYER)
				RESEND_APP_DATA_FOR_CHARACTER(0, TRUE)
				RESEND_APP_DATA_FOR_CHARACTER(1, TRUE)
				
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_RESEND_APP_DATA_FOR_PLAYER, TRUE)
				
				CPRINTLN(DEBUG_SOCIAL,"DO_INITIALISE - Player is last gen player so resend all app data")
			ENDIF
		ELSE
			IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_RESEND_APP_DATA_FOR_PLAYER)
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_RESEND_APP_DATA_FOR_PLAYER, FALSE)
			ENDIF
		ENDIF
    ELSE
        CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - initialised in SP")
        eInit = INITIALISED_IN_SP
    ENDIF
	
	#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
		// If we have a pending order, make sure we grab the plate list from players SC account.
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.sCarAppOrder.bOrderPending
				PRINTLN("[PURCHASED PLATES] DO_INITIALISE - Order pending so rebuild list of plates (MP)")
				bRebuildPlatesList = TRUE
			ENDIF
			
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_REBUILDSCPLATELIST)
				bRebuildPlatesList = TRUE
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_REBUILDSCPLATELIST, FALSE)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] DO_INITIALISE - Plates edited in SC admin so rebuilding")
				#ENDIF
			ENDIF
			
			// Plates are now part of the vehicle dupe protection system so we need to grab.
			IF NOT g_sMPTunables.bDisable_SC_Number_Plate_Initial_Get
			AND NOT g_bInitialSCPlateListGrabbedMP
				PRINTLN("[PURCHASED PLATES] DO_INITIALISE - Entering MP for first time so build list of plates")
				bRebuildPlatesList = TRUE
			ENDIF
		ELSE
			IF g_savedGlobals.sSocialData.sCarAppOrder[CHAR_MICHAEL].bOrderPending
			OR g_savedGlobals.sSocialData.sCarAppOrder[CHAR_FRANKLIN].bOrderPending
			OR g_savedGlobals.sSocialData.sCarAppOrder[CHAR_TREVOR].bOrderPending
				PRINTLN("[PURCHASED PLATES] DO_INITIALISE - Order pending so rebuild list of plates (SP)")
				bRebuildPlatesList = TRUE
			ENDIF
		ENDIF
	#ENDIF
    
    iDeleteCarDataControl = 0
	iMPVehicleBlock = 0
	iMPCarAppSlot = 0
	iMPSavedVehicleSlot = 0
    
    bProcessedProfanityChecks = FALSE
    
    bProcessModUnlocks = FALSE
    iModUnlockStage = 0
    
    IF NOT bInitialPassComplete
    OR g_bResetSocialController
        // Always refresh chop data on init
        g_savedGlobals.sSocialData.bUpdateDogLocation = TRUE
        
        // Refresh car data if we have stored vehicles or used the cloud
        bRefreshAppDataOnInit = FALSE
        APP_SET_APP("car")
            APP_SET_BLOCK("appdata")
            INT iCloudUID = APP_GET_INT_DATA("appUID")
            IF iCloudUID != g_savedGlobals.sSocialData.iGameUID
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_INITIALISE - game and cloud data differ, refresh! gameUID=", g_savedGlobals.sSocialData.iGameUID, ", cloudUID=", iCloudUID)
                bRefreshAppDataOnInit = TRUE
            ENDIF
            APP_CLOSE_BLOCK()
        APP_CLOSE_APP()
        
        INT iSlot
        REPEAT COUNT_OF(g_savedGlobals.sSocialData.sCarAppOrder) iSlot
            bProcessOrderNow[iSlot] = FALSE
            bProcessOrderOnPlayerVehicle[iSlot] = FALSE
            bInitialOrderCheckComplete[iSlot] = FALSE
        ENDREPEAT
        bProcessMPOrderNow = FALSE
    ENDIF
    
    g_bResetSocialController = FALSE
    bInitialPassComplete = TRUE
    
    eStage = SOCIAL_STAGE_PROCESS
ENDPROC

INT iLastUsedVehiclesStage
PROC DO_MAINTAIN_LAST_USED_SAVED_VEHICLES()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT i, j, iVehUpdate, iFreeSlots, iSavedVehicles, iFirstFreeSlot
	BOOL bUpdatePriorities
	INT iSlotPriorityCopy[MAX_MP_VEHICLE_APP_CAN_HANDLE]
	
	// Make a copy of the priority update array
	REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
		iSlotPriorityCopy[i] = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i]
	ENDREPEAT
	
	SWITCH iLastUsedVehiclesStage
		
		// Remove old vehicles
		CASE 0
			bUpdatePriorities = FALSE
			IF NOT IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), TRUE)
				REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
					iVehUpdate = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i]-1
					IF iVehUpdate >= 0
					AND (g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
					OR (IS_BIT_SET(g_MpSavedVehicles[iVehUpdate].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) AND NOT IS_BIT_SET(g_MpSavedVehicles[iVehUpdate].iVehicleBS,MP_SAVED_VEHICLE_INSURED)))
					
						CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - Vehicle ", iVehUpdate, " no longer valid -- remove")
						
						// remove from priority array + copy array
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] = 0
						iSlotPriorityCopy[i] = 0
						
						// remove from process slot array
						REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE j
							IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[j] = iVehUpdate+1
								g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[j] = 0
							ENDIF
						ENDREPEAT
						
						bUpdatePriorities = TRUE
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bUpdatePriorities
				j = 0
				REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
					IF iSlotPriorityCopy[i] != 0
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[j] = iSlotPriorityCopy[i]
						CPRINTLN(DEBUG_SOCIAL,"...slot[", j, "] Vehicle = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[j]-1)
						j++
					ENDIF
				ENDREPEAT
				FOR i = j TO MAX_MP_VEHICLE_APP_CAN_HANDLE-1
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] = 0
					CPRINTLN(DEBUG_SOCIAL,"...slot[", i, "] Vehicle = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i]-1)
				ENDFOR
			ENDIF
			
			iLastUsedVehiclesStage++
		BREAK
		
		// Update last used vehicle
		CASE 1
			iVehUpdate = -1
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iLastSavedVehUsed
			AND g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != -1
			AND g_MpSavedVehicles[g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			AND (NOT IS_BIT_SET(g_MpSavedVehicles[g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) OR IS_BIT_SET(g_MpSavedVehicles[g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
				iVehUpdate = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iLastSavedVehUsed = iVehUpdate
				CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - Last used vehicle updated")
			ELIF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iNewSavedVehToProcess != 0
				iVehUpdate = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iNewSavedVehToProcess-1
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iNewSavedVehToProcess = 0
				CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - New vehicle added, save slot = ", iVehUpdate)
			ENDIF
			
			IF iVehUpdate != -1
				CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - Vehicle ", iVehUpdate, " is now the top priority")
				
				BOOL bSlotDefined
				bSlotDefined = FALSE
				
				// Check to see if we already have a slot defined for this vehicle
				REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = (iVehUpdate+1)
						CPRINTLN(DEBUG_SOCIAL,"...Vehicle already stored in app slot ", i)
						bSlotDefined = TRUE
					ENDIF
				ENDREPEAT
				
				// If not defined, replace the lowest priority vehicle
				IF NOT bSlotDefined
					INT iLowestPrioritySlot
					iLowestPrioritySlot = -1
					REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
						IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] > 0
							iLowestPrioritySlot = i
						ENDIF
					ENDREPEAT
					// If we're not at fully capacity pick the next free slot
					IF iLowestPrioritySlot < MAX_MP_VEHICLE_APP_CAN_HANDLE-1
						iLowestPrioritySlot++
					ENDIF
					
					// Find the vehicle slot we used with this priority
					REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
						IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[iLowestPrioritySlot]
							g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = (iVehUpdate+1)
							CPRINTLN(DEBUG_SOCIAL,"...Vehicle now stored in app slot ", i)
							i = MAX_MP_VEHICLE_APP_CAN_HANDLE+1// Bail
						ENDIF
					ENDREPEAT
				ENDIF
				
				// Update the priorities
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[0] = (iVehUpdate+1)
				#IF IS_DEBUG_BUILD
				IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel)
					CPRINTLN(DEBUG_SOCIAL,"...priority slot[0] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[0]-1, " = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel))
				ELSE
					CPRINTLN(DEBUG_SOCIAL,"...priority slot[0] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[0]-1, " = ", g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel)
				ENDIF
				#ENDIF 
				
				
				j = 1
				REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
					IF iSlotPriorityCopy[i] != 0
					AND iSlotPriorityCopy[i] != (iVehUpdate+1)
					AND j < MAX_MP_VEHICLE_APP_CAN_HANDLE
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[j] = iSlotPriorityCopy[i]
						#IF IS_DEBUG_BUILD
						IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iSlotPriorityCopy[i]-1].vehicleSetupMP.VehicleSetup.eModel)
							CPRINTLN(DEBUG_SOCIAL,"...priority slot[", j, "] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[j]-1, " = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSlotPriorityCopy[i]-1].vehicleSetupMP.VehicleSetup.eModel))
						ELSE
							CPRINTLN(DEBUG_SOCIAL,"...priority slot[", j, "] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[j]-1, " = ", g_MpSavedVehicles[iSlotPriorityCopy[i]-1].vehicleSetupMP.VehicleSetup.eModel)
						ENDIF
						#ENDIF
						
						j++
					ENDIF
				ENDREPEAT
				// Clear out remaining slots
				FOR i = j TO MAX_MP_VEHICLE_APP_CAN_HANDLE-1
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] = 0
					CPRINTLN(DEBUG_SOCIAL,"...priority slot[", i, "] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i]-1)
				ENDFOR
				
			
			// No updates took place so check to see if we can fill the empty app slots
			ELIF g_bPopulateEmptyAppSlots
				iFreeSlots = 0
				iFirstFreeSlot = -1
				FOR i = 0 TO MAX_MP_VEHICLE_APP_CAN_HANDLE-1
					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[i] = 0
						IF iFirstFreeSlot = -1
							iFirstFreeSlot = i
						ENDIF
						iFreeSlots++
					ENDIF
				ENDFOR
				//CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - iFreeSlots = ", iFreeSlots)
				
				IF iFreeSlots > 0
					
					iSavedVehicles = 0
					FOR i = 0 TO MAX_MP_SAVED_VEHICLES-1
						IF GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(i, j)
						AND g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						AND (NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) OR IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
						AND IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
							iSavedVehicles++
						ENDIF
						IF iSavedVehicles > (MAX_MP_VEHICLE_APP_CAN_HANDLE-iFreeSlots)
							BREAKLOOP
						ENDIF
					ENDFOR
					//CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - iSavedVehicles = ", iSavedVehicles)
					
					IF iSavedVehicles > (MAX_MP_VEHICLE_APP_CAN_HANDLE-iFreeSlots)
						CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_LAST_USED_SAVED_VEHICLES - We have a space to store a vehicle! iFreeSlots=", iFreeSlots, ", iSavedVehicles=", iSavedVehicles)
						
						FOR i = 0 TO MAX_MP_SAVED_VEHICLES-1
							IF GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(i, j)
							AND g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
							AND (NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) OR IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
							AND IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
							
								BOOL bSlotDefined
								bSlotDefined = FALSE
								
								// Check to see if we already have a slot defined for this vehicle
								REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE j
									IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[j] = (i+1)
										bSlotDefined = TRUE
										j = MAX_MP_VEHICLE_APP_CAN_HANDLE+1 // Bail
									ENDIF
								ENDREPEAT
								
								IF NOT bSlotDefined
									iVehUpdate = i // Use this vehicle
									i = MAX_MP_SAVED_VEHICLES+1 // Bail
								ENDIF
							ENDIF
						ENDFOR
						
						IF iVehUpdate != -1
							// Find the vehicle slot we used with this priority
							REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
								IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[iFirstFreeSlot]
									g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[i] = (iVehUpdate+1)
									CPRINTLN(DEBUG_SOCIAL,"...Vehicle ", iVehUpdate, " now stored in app slot ", i)
									i = MAX_MP_VEHICLE_APP_CAN_HANDLE+1// Bail
								ENDIF
							ENDREPEAT
							
							// Update the priorities
							g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[iFirstFreeSlot] = (iVehUpdate+1)
							#IF IS_DEBUG_BUILD
							IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel)
								CPRINTLN(DEBUG_SOCIAL,"...priority slot[", iFirstFreeSlot, "] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[iFirstFreeSlot]-1, " = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel))
							ELSE
								CPRINTLN(DEBUG_SOCIAL,"...priority slot[", iFirstFreeSlot, "] Vehicle ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iSlotPriority[iFirstFreeSlot]-1, " = ", g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF			
			iLastUsedVehiclesStage++
		BREAK
		
		DEFAULT
			iLastUsedVehiclesStage++
		BREAK
	ENDSWITCH
	
	IF iLastUsedVehiclesStage > g_sMPTunables.iStaggeredMaxFramesAppVehicles
		iLastUsedVehiclesStage = 0
	ENDIF
ENDPROC

PROC DO_MAINTAIN_APP_DATA_RESEND()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
		
		INT iSaveGameArraySlot = GET_SAVE_GAME_ARRAY_SLOT()
		
		IF SHOULD_RESEND_APP_DATA_FOR_CHARACTER(iSaveGameArraySlot)
		
			CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_APP_DATA_RESEND - Resending app data for slot ", iSaveGameArraySlot)
				
			INT i
			INT iVehUpdate
			INT iAppSlot
			REPEAT MAX_MP_VEHICLE_APP_CAN_HANDLE i
				iVehUpdate = g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.iSlotPriority[i]-1
				
				CPRINTLN(DEBUG_SOCIAL,"...iVehUpdate[", i, "] = ", iVehUpdate)
				
				IF iVehUpdate >= 0
				AND g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
				AND (NOT IS_BIT_SET(g_MpSavedVehicles[iVehUpdate].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) OR IS_BIT_SET(g_MpSavedVehicles[iVehUpdate].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
				AND IS_VEHICLE_MODEL_SAFE_FOR_CARAPP(g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel)
				
					IF GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(iVehUpdate, iAppSlot)
						IF NOT IS_BIT_SET(g_MpSavedVehicles[iVehUpdate].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_DELETE)
//						AND g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.sCarAppData[iAppSlot].eModel = g_MpSavedVehicles[iVehUpdate].vehicleSetupMP.VehicleSetup.eModel
							SET_BIT(g_MpSavedVehicles[iVehUpdate].iVehicleBS, MP_SAVED_VEHICLE_CARAPP_SEND)
							CPRINTLN(DEBUG_SOCIAL,"DO_MAINTAIN_APP_DATA_RESEND - resending vehicle, saved_slot=", iVehUpdate, ", app_slot=", iAppSlot)
						ENDIF
	                ENDIF
				ENDIF
			ENDREPEAT
			
			g_savedMPGlobalsNew.g_savedMPGlobals[iSaveGameArraySlot].MpSavedCarApp.bMultiplayerDataWiped = FALSE
			
			RESEND_APP_DATA_FOR_CHARACTER(iSaveGameArraySlot, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Do the main processing here
PROC DO_APP_PROCESSING()

	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		EXIT
	ENDIF

//	#IF IS_DEBUG_BUILD
//		bFetchingData = FALSE
//	#ENDIF

    // Do not try to delete data if we in the process of sending mod unlocks.
    IF iModUnlockStage = 0
        
        DO_PROCESS_DELETE_DATA()
        
        // Dont do any processing if we are deleting the car data
        IF bDeletingCarData
            EXIT
        ENDIF
		
		DO_PROCESS_DELETE_ALL_DATA_FOR_NEW_ACCOUNT()
    ENDIF
    
    DO_PROCESS_MOD_UNLOCKS()
    
    // Dont do any processing if we are sending new mod data
    IF bProcessModUnlocks
        EXIT
    ENDIF
	
	DO_MAINTAIN_LAST_USED_SAVED_VEHICLES()
	DO_MAINTAIN_APP_DATA_RESEND()
    
    // Update the frame that we are processing
    BOOL bBlocked = bBlockFrameUpdate
    BOOL bAllowSave = (iCurrentAppFrame = 0 AND NOT bProcessModUnlocks AND NOT bDeletingCarData)
    IF bBlockFrameUpdate
        bBlockFrameUpdate = FALSE
    ELSE
        iCurrentAppFrame = (iCurrentAppFrame+1) % g_sMPTunables.iStaggeredMaxFramesAppOrders
    ENDIF
    
    IF NETWORK_IS_GAME_IN_PROGRESS()
	
		IF iCurrentAppFrame < (MAX_MP_VEHICLE_APP_CAN_HANDLE*2) // 2 pass: 0=priority, 1=cleanup
            IF NOT bBlocked
                iMPVehicleBlock++
                IF iMPVehicleBlock >= MAX_MP_VEHICLE_APP_CAN_HANDLE
                    iMPVehicleBlock = 0
					bPriorityUpdate = !bPriorityUpdate
                ENDIF
            ENDIF
			
			iMPSavedVehicleSlot = (g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[iMPVehicleBlock]-1)
			
			IF bPriorityUpdate
				IF GET_CAR_APP_SLOT_FOR_SAVE_VEHICLE(iMPSavedVehicleSlot, iMPCarAppSlot)
					DO_PROCESS_CAR_APP(CHAR_MULTIPLAYER)
				ENDIF
			ELSE
				// If this slot is not used as one of the priority vehicles, hide
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.iProcessSlot[iMPVehicleBlock] = 0
					DO_HIDE_VEHICLE_SLOT_IN_APP(iMPVehicleBlock)
				ENDIF
			ENDIF
        ENDIF
    ELSE
        SWITCH iCurrentAppFrame
            CASE 0  DO_PROCESS_CAR_APP(CHAR_MICHAEL)    BREAK
            CASE 1  DO_PROCESS_CAR_APP(CHAR_FRANKLIN)   BREAK
            CASE 2  DO_PROCESS_CAR_APP(CHAR_TREVOR)     BREAK
            CASE 3  DO_PROCESS_DOG_APP()                BREAK
        ENDSWITCH
    ENDIF
    
    IF bAllowSave
        IF bSaveCarData
            IF TIMERA() > 5000
                CPRINTLN(DEBUG_SOCIAL,"social_controller: DO_PROCESS_CAR_APP - saving car data")
                APP_SET_APP("car")
                APP_SAVE_DATA()
                APP_CLOSE_APP()
                bSaveCarData = FALSE
            ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Check to see if the players last vehicle has a profance license plate.
PROC DO_PROFANITY_CHECKS()
    IF NOT bProcessedProfanityChecks
        IF SC_PROFANITY_GET_CHECK_IS_VALID(iAmbProfanityToken)
            IF NOT SC_PROFANITY_GET_CHECK_IS_PENDING(iAmbProfanityToken)
                IF SC_PROFANITY_GET_STRING_PASSED(iAmbProfanityToken)
                    iAmbProfanityToken = 0
                    bProcessedProfanityChecks = TRUE
                    CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - End: passed")
                ELSE
                    CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - End: failed")
                    IF NETWORK_IS_GAME_IN_PROGRESS()
                        g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.tlCarAppPlateText = GENERATE_RANDOM_NUMBER_PLATE()
                        CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - Change: plate = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedCarApp.tlCarAppPlateText)
                    ELSE
                        g_savedGlobals.sSocialData.tlCarAppPlateText = GENERATE_RANDOM_NUMBER_PLATE()
                        CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - Change: plate = ", g_savedGlobals.sSocialData.tlCarAppPlateText)
                    ENDIF
                    iAmbProfanityToken = 0
                    bProcessedProfanityChecks = TRUE
                ENDIF
            ENDIF
        ELSE
            IF GET_CAR_APP_NUMBER_PLATE(tlProfanityPlate, iProfanityPlateBack)
                SC_PROFANITY_CHECK_STRING(tlProfanityPlate, iAmbProfanityToken)
                CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - Start: token = ", iAmbProfanityToken)
            ELSE
                CPRINTLN(DEBUG_SOCIAL,"DO_PROFANITY_CHECKS - End: no stored plate")
                iAmbProfanityToken = 0
                bProcessedProfanityChecks = TRUE
            ENDIF
        ENDIF
    ENDIF
ENDPROC







PROC Maintain_QualPlaylistReminder()  //Can't edit header file. Needs to be here.  For TODO 1562725

    IF NOT HAS_NET_TIMER_STARTED(QualPlaylistTimer)
        START_NET_TIMER(QualPlaylistTimer)
    ELIF HAS_NET_TIMER_EXPIRED(QualPlaylistTimer, (120 * 60000)) //Change this to two hours after testing...

        IF IS_EVENT_PLAYLIST_ACTIVE_BY_TYPE(ciQUALIFING_TOURNAMENT_PLAYLIST)


            //SCRIPT_TIMER iLocalGarageCheck

            //Two hour timer has expired. Make checks for free-roam and playlist
            IF HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
			
				IF NOT IS_TRANSITION_ACTIVE() // B*1663140 - stop messages appearing on the transition screen.
					
	                //IF CAN_DO_QUICK_MATCH(iLocalGarageCheck) = 0 //Zero is "Okay" result. 


	                RESET_NET_TIMER(QualPlaylistTimer)
	                
	                  //Steve T. 1562725
	                BEGIN_TEXT_COMMAND_THEFEED_POST ("SC_PLAY_REM")

	                #if IS_DEBUG_BUILD

	                    cdPrintnl()
	                    cdPrintstring("SOCIAL_CONTROLLER - Displayed qualifying playlist reminder SC_PLAY_REM")
	                    cdPrintnl()

	                #endif
	                    
	                    /* Alternatively
	                    TEXT_LABEL_63 s_MPGamerTagTempHolder

	                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
	               
	                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
	                    s_MPGamerTagTempHolder += "Social Club"
	                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"

	                    ND_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

	                    */


	                //END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "CONTENT_TICK")
	               


	                END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE, TRUE)

	        
	                //ENDIF
				
				ENDIF
				
            ENDIF
        
        ENDIF


    ENDIF


ENDPROC

#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP

INT iPurchasedPlatesStage
INT iPurchasedPlateIndex
INT iPurchasedPlateToken

PROC GET_ARRAY_OF_PLATE_DATA_FLAGS(STRING sPlateData, TEXT_LABEL_31 &tlPlateFlags[10])
	INT iStrLen = 0
	IF NOT IS_STRING_NULL_OR_EMPTY(sPlateData)
		iStrLen = GET_LENGTH_OF_LITERAL_STRING(sPlateData)
	ENDIF
	
	INT iCurrentChar
	TEXT_LABEL_7 tlCurrentChar
	TEXT_LABEL_31 tlCurrentFlag = ""
	BOOL bProcessingFlags = (iStrLen != 0)
	
	INT iFlagIndex
	
	INT i
	REPEAT COUNT_OF(tlPlateFlags) i
		tlPlateFlags[i] = ""
	ENDREPEAT
	
	// Query each character to build up the flag bitset
	WHILE bProcessingFlags
		tlCurrentChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sPlateData, iCurrentChar, iCurrentChar+1)
		
		iCurrentChar++
		
		// Reached end of flag
		IF GET_HASH_KEY(tlCurrentChar) = HASH(",")
		OR iCurrentChar >= iStrLen
			
			// If we have hit the end, make sure we add the last character
			IF GET_HASH_KEY(tlCurrentChar) != HASH(",")
				tlCurrentFlag += tlCurrentChar
			ENDIF
			
			IF iFlagIndex < COUNT_OF(tlPlateFlags)
				tlPlateFlags[iFlagIndex] = tlCurrentFlag
				iFlagIndex++
			ENDIF
			
			tlCurrentFlag = ""
		ELSE
			tlCurrentFlag += tlCurrentChar
		ENDIF
		
		// Reached end of string
		IF iCurrentChar >= iStrLen
			bProcessingFlags = FALSE
		ENDIF
	ENDWHILE
ENDPROC

FUNC BOOL HAS_PLATE_DATA_GOT_FLAG_SET(STRING sPlateData, STRING sFlag)
	// Grab list of current flags
	TEXT_LABEL_31 tlPlateFlags[10]
	GET_ARRAY_OF_PLATE_DATA_FLAGS(sPlateData, tlPlateFlags)
	
	// Cycle through list and see if any match the flag passed in
	INT iPlate
	INT iFlagHash = GET_HASH_KEY(sFlag)
	REPEAT COUNT_OF(tlPlateFlags) iPlate
		IF NOT IS_STRING_NULL_OR_EMPTY(tlPlateFlags[iPlate])
			IF GET_HASH_KEY(tlPlateFlags[iPlate]) = iFlagHash
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(TEXT_LABEL_63 &tlPlateData, STRING sFlag, BOOL bSet)
	
	//NATIVE FUNC STRING SC_LICENSEPLATE_GET_PLATE_DATA(INT token, INT index)
	
	//PLATE_DATA_GIFTED_PLATE = 1 // == �NoDelete� string
	//PLATE_DATA_SPPLATE     = 2 // == �SPPlate� string
	
	//NATIVE FUNC BOOL  SC_LICENSEPLATE_SET_PLATE_DATA(STRING oldPlateText, STRING newPlateText, INT bitfield)
	
	PRINTLN("MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG")
	PRINTLN("...tlPlateData = '", tlPlateData, "'")
	IF bSet
		PRINTLN("...add '", sFlag, "'")
	ELSE
		PRINTLN("...remove '", sFlag, "'")
	ENDIF
	
	// Grab list of current flags
	TEXT_LABEL_31 tlPlateFlags[10]
	GET_ARRAY_OF_PLATE_DATA_FLAGS(tlPlateData, tlPlateFlags)
	
	// Cycle through list and add all flags except the one passed in to this proc
	tlPlateData = ""
	
	INT iPlate
	INT iFlagHash = GET_HASH_KEY(sFlag)
	INT iPlateFlagHash
	BOOL bAddComma
	REPEAT COUNT_OF(tlPlateFlags) iPlate
		IF NOT IS_STRING_NULL_OR_EMPTY(tlPlateFlags[iPlate])
			iPlateFlagHash = GET_HASH_KEY(tlPlateFlags[iPlate])
			IF iPlateFlagHash != iFlagHash
				IF bAddComma
					tlPlateData += ","
					bAddComma = FALSE
				ENDIF
				tlPlateData += tlPlateFlags[iPlate]
				bAddComma = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Now add the flag passed in if required
	IF bSet
		IF bAddComma
			tlPlateData += ","
			bAddComma = FALSE
		ENDIF
		tlPlateData += sFlag
		bAddComma = TRUE
	ENDIF
	
	PRINTLN("...Return string = '", tlPlateData, "'")
ENDPROC

PROC MAINTAIN_PURCHASED_PLATES_LIST()
	IF g_bRebuildSCPlateList
	AND NOT bRebuildPlatesList
		bRebuildPlatesList = TRUE
		g_bRebuildSCPlateList = FALSE
	ENDIF
	
	IF bRebuildPlatesList
	AND iPurchasedPlatesStage = 0
	AND NOT bProcessModUnlocks
	AND NOT g_sMPTunables.bDisable_SC_Number_Plate_Get
	AND eStage != SOCIAL_STAGE_INIT
	
		// Check if we have ever ordered a plate.
		INT iOrderedPlateCount = 0
		
		APP_SET_APP("car")
            APP_SET_BLOCK("appdata")
                iOrderedPlateCount += APP_GET_INT_DATA("orderCount_sp0")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_sp1")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_sp2")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_mp0")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_mp1")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_mp2")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_mp3")
				iOrderedPlateCount += APP_GET_INT_DATA("orderCount_mp4")
            APP_CLOSE_BLOCK()
        APP_CLOSE_APP()
		
		iOrderedPlateCount = iOrderedPlateCount
		
		IF SC_LICENSEPLATE_GET_CHECK_IS_VALID(iPurchasedPlateToken)
		    IF NOT SC_LICENSEPLATE_GET_CHECK_IS_PENDING(iPurchasedPlateToken)
				
				CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] MAINTAIN_PURCHASED_PLATES_LIST - Building list of plates")
			
				INT iPlate
				INT iPlateCount = COUNT_OF(g_tlPlateTextFromSCAccount)
				REPEAT iPlateCount iPlate
					g_tlPlateTextFromSCAccount[iPlate] = ""
					g_bPlateTextFromSCAccountInMPList[iPlate] = FALSE
				ENDREPEAT
				
				iPlateCount = SC_LICENSEPLATE_GET_COUNT(iPurchasedPlateToken)
				
				PRINTLN("...iPlateCount = ", iPlateCount)
				PRINTLN("...iOrderedPlateCount = ", iOrderedPlateCount)
				
				g_bPlateCountOnSCAccountFull = (iPlateCount >= COUNT_OF(g_tlPlateTextFromSCAccount))
				PRINTLN("...g_bPlateCountOnSCAccountFull = ", g_bPlateCountOnSCAccountFull)
				
				TEXT_LABEL_63 tlPlateData
				
//				IF iPlateCount > 1 OR iOrderedPlateCount > 0
					REPEAT iPlateCount iPlate
						IF iPlate < COUNT_OF(g_tlPlateTextFromSCAccount)
							g_tlPlateTextFromSCAccount[iPlate] = SC_LICENSEPLATE_GET_PLATE(iPurchasedPlateToken, iPlate)
							
							tlPlateData = SC_LICENSEPLATE_GET_PLATE_DATA(iPurchasedPlateToken, iPlate)
							IF HAS_PLATE_DATA_GOT_FLAG_SET(tlPlateData, "MPPlate")
							OR HAS_PLATE_DATA_GOT_FLAG_SET(tlPlateData, "NoDelete")
							OR iPlate = 0
								g_bPlateTextFromSCAccountInMPList[iPlate] = TRUE
							ENDIF
							
							PRINTLN("...Added '", g_tlPlateTextFromSCAccount[iPlate], "' plateData = ", tlPlateData)
						ELSE
							PRINTLN("...Skipped '", SC_LICENSEPLATE_GET_PLATE(iPurchasedPlateToken, iPlate), "'")
						ENDIF
					ENDREPEAT
//				ENDIF
				
				// Done processing so clear token and check
				iPurchasedPlateToken = 0
				bRebuildPlatesList = FALSE
				
				g_bInitialSCPlateListGrabbedMP = TRUE
		    ENDIF
		ELSE
			SC_LICENSEPLATE_CHECK_STRING("TEST", iPurchasedPlateToken)
		ENDIF
	ENDIF
ENDPROC

BOOL bPlateQueueTimerSet
TIME_DATATYPE tdPlateQueueTimer
INT iArrayOfPlateDigits[8]

PROC MAINTAIN_PURCHASED_PLATES_ADD()

	// The purpose of this proc is to take the queued plates and add them to the players social club account.
	// If the player buys this in MP then we need to set a flag on the plateData field so that the iFruit app
	// knows that the player has paid for it.
	// We store the MP plate characters in packed stats so that we can retry failed attempts. Once we have 
	// successfully added a plate and the MPPlate flag to the plateData we then clear the packed stats.

	SWITCH iPurchasedPlatesStage
		CASE 0
			IF g_bAddPlateTextToSC
			AND NOT bRebuildPlatesList
			AND NOT g_sMPTunables.bDisable_SC_Number_Plate_Add
			
				INT iPlate
				iPurchasedPlateIndex = -1
				REPEAT COUNT_OF(g_tlPlateTextForSCAccount) iPlate
					IF NOT IS_STRING_NULL_OR_EMPTY(g_tlPlateTextForSCAccount[iPlate])
						iPurchasedPlateIndex = iPlate
					ENDIF
				ENDREPEAT
				
				IF iPurchasedPlateIndex != -1
					CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Attempting to add plate '", g_tlPlateTextForSCAccount[iPurchasedPlateIndex], "' with hash ", GET_HASH_KEY(g_tlPlateTextForSCAccount[iPurchasedPlateIndex]))
					iPurchasedPlatesStage++
				ELSE
					CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] No plates in queue, end.")
					g_bAddPlateTextToSC = FALSE
				ENDIF
			ENDIF
			
			// If we are not adding any plates do a check to see if we have one that was stored and never added to SC.
			IF NOT g_bAddPlateTextToSC
			AND NETWORK_IS_GAME_IN_PROGRESS()
			AND GET_PACKED_STAT_BOOL(PACKED_MP_STAT_SC_PLATE_STORED)
			AND (NOT bPlateQueueTimerSet OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdPlateQueueTimer)) > 25000)
				
				TEXT_LABEL_15 tlNumberPlate
				iArrayOfPlateDigits[0] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_0)
				iArrayOfPlateDigits[1] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_1)
				iArrayOfPlateDigits[2] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_2)
				iArrayOfPlateDigits[3] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_3)
				iArrayOfPlateDigits[4] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_4)
				iArrayOfPlateDigits[5] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_5)
				iArrayOfPlateDigits[6] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_6)
				iArrayOfPlateDigits[7] = GET_PACKED_STAT_INT(PACKED_MP_SC_PLATE_7)
				
				GET_VEHICLE_NUMBER_PLATE_FROM_INTS(tlNumberPlate, iArrayOfPlateDigits)
				PRINTLN("[PURCHASED PLATES] Plate stored in MP stats '", tlNumberPlate, "'")
				
				ADD_NUMBER_PLATE_TEXT_TO_PLAYERS_SC_ACCOUNT(tlNumberPlate)
				
				bPlateQueueTimerSet = FALSE
			ENDIF
		BREAK
		CASE 1
			IF iPurchasedPlateToken = 0
				SC_LICENSEPLATE_ISVALID(g_tlPlateTextForSCAccount[iPurchasedPlateIndex], iPurchasedPlateToken)
			ELIF NOT SC_LICENSEPLATE_GET_ISVALID_IS_PENDING(iPurchasedPlateToken)
				SWITCH SC_LICENSEPLATE_GET_ISVALID_STATUS(iPurchasedPlateToken)
					CASE LICENSEPLATE_ISVALID_OK
						CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate passed valid checks")
						
						// Check if we already have the plate
						INT iPlateCount
						INT iPlate
						BOOL bAlreadyExists
						
						bAlreadyExists = FALSE
						iPlateCount = COUNT_OF(g_tlPlateTextFromSCAccount)
						
						CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Checking if plate already exists")
						
						REPEAT iPlateCount iPlate
							IF NOT IS_STRING_NULL_OR_EMPTY(g_tlPlateTextFromSCAccount[iPlate])
								CPRINTLN(DEBUG_SOCIAL,"...[", iPlate, "] = '", g_tlPlateTextFromSCAccount[iPlate], "' with hash ", GET_HASH_KEY(g_tlPlateTextFromSCAccount[iPlate]))
								IF GET_HASH_KEY(g_tlPlateTextFromSCAccount[iPlate]) = GET_HASH_KEY(g_tlPlateTextForSCAccount[iPurchasedPlateIndex])
									bAlreadyExists = TRUE
									iPlate = iPlateCount+1// Bail
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SOCIAL,"...[", iPlate, "] = empty")
							ENDIF
						ENDREPEAT
						
						IF bAlreadyExists
							// We now need to add the SP or MP flag so we know that this plate has been authorised by script
							PRINTLN("[PURCHASED PLATES] Plate already exists, proceed to modify plate data")
							iPurchasedPlatesStage = 3
						ELSE
							// We now need to add the plate
							PRINTLN("[PURCHASED PLATES] Plate doesn't exist, proceed to add plate")
							iPurchasedPlatesStage = 2
						ENDIF
					BREAK
					DEFAULT
						CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate failed valid check, status = ", SC_LICENSEPLATE_GET_ISVALID_STATUS(iPurchasedPlateToken))

						g_tlPlateTextForSCAccount[iPurchasedPlateIndex] = ""
						
						// url:bugstar:6564951 - Anomalous spike in calls to licenseplates.asmx/IsValid
						IF (SC_LICENSEPLATE_GET_ISVALID_STATUS(iPurchasedPlateToken) = LICENSEPLATE_ISVALID_PROFANE)
							CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate failed profane. clear stat and notify player")
							IF NETWORK_IS_GAME_IN_PROGRESS()
								SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SC_PLATE_STORED, FALSE)
								CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate failed profane. SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SC_PLATE_STORED, FALSE)")
							ENDIF	
						ENDIF
						
						iPurchasedPlatesStage = 0 // Jump back to start and check for next plate
					BREAK
				ENDSWITCH
				
				iPurchasedPlateToken = 0 // Clear the token
			ENDIF
		BREAK
		//////////////////////////////////////////////////////////////////////////////////////
		///      ADD PLATE
		///      
		CASE 2
			IF iPurchasedPlateToken = 0
			
				TEXT_LABEL_63 tlPlateData
				tlPlateData = ""
				
				IF g_bAddSCPlateToMPList[iPurchasedPlateIndex]
					// Clear SPPlate flag and set MPPlate flag
					MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "SPPlate", FALSE)
					MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "MPPlate", TRUE)
				ELSE
					// Set SPPlate flag
					MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "SPPlate", TRUE)
				ENDIF
				
				SC_LICENSEPLATE_ADD(g_tlPlateTextForSCAccount[iPurchasedPlateIndex], tlPlateData, iPurchasedPlateToken)
				
			ELIF NOT SC_LICENSEPLATE_GET_ADD_IS_PENDING(iPurchasedPlateToken)
				SWITCH SC_LICENSEPLATE_GET_ADD_STATUS(iPurchasedPlateToken)
					CASE LICENSEPLATE_ADD_OK
						CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate successfully added")
						
						// Done processsing
						bRebuildPlatesList = TRUE
						iPurchasedPlatesStage = 0 // Jump back to start and check for next plate
						g_tlPlateTextForSCAccount[iPurchasedPlateIndex] = ""
						
						iPurchasedPlateToken = 0 // Clear the token
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
							SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SC_PLATE_STORED, FALSE)
						ENDIF
					BREAK
					DEFAULT
						CPRINTLN(DEBUG_SOCIAL,"[PURCHASED PLATES] Plate failed to add, status = ", SC_LICENSEPLATE_GET_ADD_STATUS(iPurchasedPlateToken))
						iPurchasedPlateToken = 0 // Clear the token
						iPurchasedPlatesStage = 0 // Jump back to start and check for next plate
						g_tlPlateTextForSCAccount[iPurchasedPlateIndex] = ""
						
						// We can queue this back up later.
						IF NETWORK_IS_GAME_IN_PROGRESS()
							PRINTLN("[PURCHASED PLATES] Will rety MP plate in 25 seconds...")
							bPlateQueueTimerSet = TRUE
							tdPlateQueueTimer = GET_NETWORK_TIME()
						ENDIF
					BREAK
				ENDSWITCH
				
				iPurchasedPlateToken = 0 // Clear the token
			ENDIF
		BREAK
		//////////////////////////////////////////////////////////////////////////////////////
		///      MODIFY PLATE
		///      
		CASE 3
			// Re-grab the data since we just added a plate but use the CHECK_STRING method to get the token.
			IF SC_LICENSEPLATE_GET_CHECK_IS_VALID(iPurchasedPlateToken)
		  	  IF NOT SC_LICENSEPLATE_GET_CHECK_IS_PENDING(iPurchasedPlateToken)
				// Grab the plate index so we can modify the data
					INT iPlateIndex
					INT iPlateCount
					INT iPlate
					STRING sPlateText
					TEXT_LABEL_63 tlPlateData
					iPlateIndex = -1
					iPlateCount = SC_LICENSEPLATE_GET_COUNT(iPurchasedPlateToken)
					
					PRINTLN("[PURCHASED PLATES] Grabbing index for plate '", g_tlPlateTextForSCAccount[iPurchasedPlateIndex], "' with hash ", GET_HASH_KEY(g_tlPlateTextForSCAccount[iPurchasedPlateIndex]))
					
					REPEAT iPlateCount iPlate
						sPlateText = SC_LICENSEPLATE_GET_PLATE(iPurchasedPlateToken, iPlate)
						PRINTLN("...Plate[", iPlate, "] = '", sPlateText, "'")
						IF NOT IS_STRING_NULL_OR_EMPTY(sPlateText)
						AND GET_HASH_KEY(sPlateText) = GET_HASH_KEY(g_tlPlateTextForSCAccount[iPurchasedPlateIndex])
							iPlateIndex = iPlate
							iPlate = iPlateCount+1//Bail
						ENDIF
					ENDREPEAT
					
					IF iPlateIndex != -1
						
						// Grab current plate data string
						tlPlateData = SC_LICENSEPLATE_GET_PLATE_DATA(iPurchasedPlateToken, iPlateIndex)
						
						PRINTLN("[PURCHASED PLATES] Found index ", iPlateIndex, ", data = '", tlPlateData, "'")
						
						IF g_bAddSCPlateToMPList[iPurchasedPlateIndex]
							// Clear SPPlate flag and set MPPlate flag
							MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "SPPlate", FALSE)
							MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "MPPlate", TRUE)
						
						ELIF NOT HAS_PLATE_DATA_GOT_FLAG_SET(tlPlateData, "MPPlate")
							// Set SPPlate flag
							MAINTAIN_PURCHASED_PLATES_UPDATE_FLAG(tlPlateData, "SPPlate", TRUE)
						ENDIF
						
						// Now push to backend.
						SC_LICENSEPLATE_SET_PLATE_DATA(sPlateText, sPlateText, tlPlateData)
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
							SET_PACKED_STAT_BOOL(PACKED_MP_STAT_SC_PLATE_STORED, FALSE)
						ENDIF
					ELSE
						PRINTLN("[PURCHASED PLATES] Failed to find plate index")
						
						// We can queue this back up later.
						IF NETWORK_IS_GAME_IN_PROGRESS()
							PRINTLN("[PURCHASED PLATES] Will rety MP plate in 25 seconds...")
							bPlateQueueTimerSet = TRUE
							tdPlateQueueTimer = GET_NETWORK_TIME()
						ENDIF
					ENDIF
					
					bRebuildPlatesList = TRUE
					iPurchasedPlatesStage = 0 // Jump back to start and check for next plate
					g_tlPlateTextForSCAccount[iPurchasedPlateIndex] = ""
					
					iPurchasedPlateToken = 0 // Clear the token
				ENDIF
			ELSE
				SC_LICENSEPLATE_CHECK_STRING("TEST", iPurchasedPlateToken)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#ENDIF

PROC MAINTAIN_INVALID_TRANSACTION_CHECK()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND g_bKickPlayerForInvalidTransaction
	#IF IS_DEBUG_BUILD
	AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SkipTransactionValidation")
	#ENDIF
		SCRIPT_ASSERT("MAINTAIN_INVALID_TRANSACTION_CHECK - Invalid transaction data - kicking player")
		PRINTLN("MAINTAIN_INVALID_TRANSACTION_CHECK - Invalid transaction data - kicking player")
		NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_INVALID_TRANSACTION_DETECTED))
	ENDIF
ENDPROC

SCRIPT

    PRINTSTRING("\nStarting social controller")PRINTNL()
    
    // This script needs to cleanup only when the game runs the magdemo
    IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
        PRINTSTRING("...social_controller.sc has been forced to cleanup (MAGDEMO)")
        PRINTNL()
        
        TERMINATE_THIS_THREAD()
    ENDIF
    
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
    
    // Setup some debug widgets
    #IF IS_DEBUG_BUILD
        SETUP_SOCIAL_CONTROL_WIDGETS()
    #ENDIF
    
    iInitialOrderCheckTimer = GET_GAME_TIMER()
    
    SETTIMERA(0)
    SETTIMERB(0)
    
    // Main loop
    WHILE (TRUE)
    
        WAIT(0)
        
		IF IS_PLAYING_DLC_INTRO_BINK()
			PRINTSTRING("social controller suspended, bink video playing")
			
		ELSE
		
	        #IF IS_DEBUG_BUILD
	            MAINTAIN_SOCIAL_CONTROL_WIDGETS()
	        #ENDIF
	        
	        CHECK_CONTROLLER_RESET()
	        
	        MAINTAIN_TUNEABLES()
			MAINTAIN_INVALID_TRANSACTION_CHECK()
	        
	        SWITCH eStage
	            CASE SOCIAL_STAGE_INIT
	                DO_INITIALISE()
	            BREAK
	            CASE SOCIAL_STAGE_PROCESS
	                DO_APP_PROCESSING()
	                DO_PROFANITY_CHECKS()
	                MAINTAIN_SOCIAL_FEED(socialFeedVars)
					#IF USE_MULTI_NUMBER_PLATE_SETUP_IN_MP
						MAINTAIN_PURCHASED_PLATES_LIST()
						MAINTAIN_PURCHASED_PLATES_ADD()
					#ENDIF

	                IF NETWORK_IS_GAME_IN_PROGRESS()
	                    Maintain_QualPlaylistReminder()
	                ENDIF

	            BREAK
	            CASE SOCIAL_STAGE_CLEANUP
	                Script_Cleanup()
	            BREAK
	        ENDSWITCH
			
		ENDIF
    ENDWHILE
    
ENDSCRIPT
