USING "rage_builtins.sch"
USING "globals.sch"

STRUCT STATIC_CAR_APP_DATA
	INT iModCountEngine
	INT iModCountBrakes
	INT iModCountExhaust
	INT iModCountArmour
	INT iModCountHorn
	INT iModCountSuspension
	FLOAT fModPriceModifier
	INT iModColoursThatCanBeSet
	INT iModKitType
ENDSTRUCT

// Use [rag/Script/Social Controller/Generate static app data] to generate.


PROC GET_STATIC_CAR_APP_DATA_FOR_VEHICLE(MODEL_NAMES eModel, STATIC_CAR_APP_DATA &sData)
	INT iModel = ENUM_TO_INT(eModel)
	SWITCH iModel
		CASE -349601129 // Bifta
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 92612664 // Kalahari
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1488164764 // paradise
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 117401876 // btype
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1297672541 // Jester
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 408192225 // TurismoR
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 767087018 // Alpha
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1403128555 // zentorno
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -142942670 // Massacro
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 486987393 // Huntley
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1836027715 // thrust
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 841808271 // RHAPSODY
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 23
			sData.iModKitType = 0
		BREAK
		CASE 1373123368 // Warrener
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -1205801634 // Blade
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 75131841 // glendale
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 7
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -431692672 // panto
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1237253773 // dubsta3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE 1078682497 // pigalle
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 743478836 // Sovereign
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1011753235 // coquette2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -339587598 // SWIFT
			sData.iModCountEngine = 1
			sData.iModCountBrakes = 1
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 1
			sData.iModCountHorn = 1
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -159126838 // innovation
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1265391242 // hakuchou
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1089039904 // furoregt
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1106353882 // jester2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -631760477 // massacro2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE -589178377 // ratloader2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 2
		BREAK
		CASE 729783779 // slamvan
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 941800958 // casco
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1860900134 // INSURGENT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 2071877360 // INSURGENT2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 296357396 // GBURRITO2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1753414259 // ENDURO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE -2107990196 // guardian
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 640818791 // LECTRO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1372848492 // kuruma
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 410882957 // KURUMA2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 833469436 // slamvan2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1075432268 // SWIFT2
			sData.iModCountEngine = 1
			sData.iModCountBrakes = 1
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 1
			sData.iModCountHorn = 1
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1566741232 // feltzer3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 1987142870 // osiris
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -498054846 // virgo
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1581459400 // windsor
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 784565758 // coquette3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1353081087 // vindicator
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1663218586 // t20
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1479664699 // BRAWLER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 1070967343 // toro
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 349605904 // chino
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -2119578145 // faction
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1790546981 // faction2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 525509695 // moonbeam
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 1896491931 // moonbeam2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -2040426790 // primo2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1361687965 // chino2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1013450936 // BUCCANEER2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 2006667053 // voodoo
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 2068293287 // lurcher
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 3
		BREAK
		CASE -831834716 // btype2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 1102544804 // verlierer2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1943285540 // nightshade
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1660945322 // MAMBA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -114627507 // LIMO2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1485523546 // schafter3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1489967196 // schafter4
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -888242983 // schafter5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1922255844 // schafter6
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 906642318 // cog55
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 704435172 // cog552
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -2030171296 // cognoscenti
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -604842630 // cognoscenti2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1878062887 // baller3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 634118882 // baller4
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 470404958 // baller5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 666166960 // baller6
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 908897389 // toro2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 972671128 // tampa
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -295689028 // sultanrs
			sData.iModCountEngine = 6
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 633712403 // banshee2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -602287871 // btype3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -2039755226 // faction3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -1126264336 // minivan2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 223258115 // sabregt2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1119641113 // slamvan3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1797613329 // tornado5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -899509638 // virgo2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 16646064 // virgo3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1203490606 // xls
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -432008408 // xls2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1757836725 // seven70
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1426219628 // fmj
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 1274868363 // bestiagts
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -1829802492 // pfister811
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -305727417 // brickade
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1475773103 // rumpo3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 2123327359 // prototipo
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 234062309 // reaper
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1930048799 // windsor2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 482197771 // lynx
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 741090084 // gargoyle
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 2067820283 // tyrus
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 3
		BREAK
		CASE 819197656 // sheava
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -777172681 // omnis
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -1232836011 // le7b
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 683047626 // contender
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 101905590 // trophytruck
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -663299102 // trophytruck2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -2103821244 // rallytruck
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE 390201602 // cliffhanger
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 86520421 // bf400
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1887331236 // tropos
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 1549126457 // brioso
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1071380347 // tampa2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -1558399629 // tornado6
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE -1289178744 // faggio3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1842748181 // FAGGIO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -674927303 // raptor
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -609625092 // vortex
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -2115793025 // avarus
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1491277511 // sanctus
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1026149675 // youga2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -255678177 // hakuchou2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1606187161 // nightblade
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 6774487 // chimera
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 7
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 2035069708 // esskey
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -618617997 // wolfsbane
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 8
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1009268949 // zombiea
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 9
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -570033273 // zombieb
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 9
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 822018448 // defiler
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1404136503 // daemon2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 8
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 1873600305 // ratbike
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 8
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -405626514 // shotaro
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1523428744 // manchez
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -440768424 // blazer4
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 196747873 // elegy
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 10
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 272929391 // tempesta
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -2048333973 // italigtb
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 8
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -482719877 // italigtb2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1034187331 // nero
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 1093792632 // nero2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 1886268224 // specter
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1074745671 // specter2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 7
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE -239841468 // diablous
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1790834270 // diablous2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 7
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -1590337689 // blazer5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 941494461 // ruiner2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -827162039 // dune4
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 3
			sData.iModKitType = 0
		BREAK
		CASE -312295511 // dune5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 3
			sData.iModKitType = 0
		BREAK
		CASE -1758137366 // penetrator
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1180875963 // technical2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 627535535 // fcr
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -757735410 // fcr2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 7
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -2022483795 // comet3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 3
		BREAK
		CASE 777714999 // ruiner3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 723973206 // dukes
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -326143852 // dukes2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 237764926 // buffalo3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE -915704871 // dominator2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE 1233534620 // marshall
			sData.iModCountEngine = 1
			sData.iModCountBrakes = 1
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 1
			sData.iModCountHorn = 1
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 349315417 // gauntlet2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1923400478 // STALION
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -401643538 // STALION2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 1039032026 // blista2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -591651781 // blista3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 1032823388 // NINEF
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 23
			sData.iModKitType = 1
		BREAK
		CASE -1461482751 // ninef2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -344943009 // BLISTA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1809822327 // ASEA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1903012613 // ASTEROPE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -808831384 // BALLER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE 142944341 // BALLER2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE 850565707 // BJXL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -1041692462 // BANSHEE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1126868326 // BfInjection
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -2128233223 // BLAZER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -48031959 // BLAZER2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1269889662 // blazer3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -16948145 // BISON
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE 1069929536 // BOBCATXL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -1435919434 // BODHI2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -682211828 // buccaneer
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -304802106 // BUFFALO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 736902334 // BUFFALO2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 6
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -1696146015 // BULLET
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -907477130 // burrito2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1743316013 // burrito3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 893081117 // burrito4
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1132262048 // burrito5
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 2006918058 // CAVALCADE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -789894171 // cavalcade2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE -1745203402 // GBURRITO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 2072687711 // carbonizzare
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE -1311154784 // CHEETAH
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE -1045541610 // Comet2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 330661258 // COGCABRIO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 108773431 // COQUETTE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -1543762099 // GRESLEY
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE -1130810103 // DILETTANTE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1661854193 // DUNE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 1
			sData.iModKitType = 0
		BREAK
		CASE 37348240 // HOTKNIFE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1770332643 // DLOADER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1177543287 // DUBSTA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 23
			sData.iModKitType = 2
		BREAK
		CASE -394074634 // Dubsta2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 21
			sData.iModKitType = 2
		BREAK
		CASE 80636076 // DOMINATOR
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -685276541 // EMPEROR
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1883002148 // Emperor2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1241712818 // emperor3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1291952903 // ENTITYXF
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE -5153954 // EXEMPLAR
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -566387422 // ELEGY2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE -591610296 // F620
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -391594584 // FELON
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -89291282 // FELON2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1995326987 // Feltzer2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 1
		BREAK
		CASE -1137532101 // FQ2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 499169875 // FUSILADE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1909141499 // FUGITIVE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 2016857647 // FUTO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE -1775728740 // GRANGER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 2
		BREAK
		CASE -1800170043 // GAUNTLET
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 884422927 // HABANERO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 418536135 // INFERNUS
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1289722222 // INGOT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 886934177 // INTRUDER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1177863319 // issi2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -624529134 // JACKAL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1051415893 // JB700
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE 544021352 // khamelion
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1269098716 // LANDSTALKER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -2124201592 // MANANA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 914654722 // MESA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE -2064372143 // MESA3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 2
		BREAK
		CASE -310465116 // MINIVAN
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -433375717 // monroe
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE -1050465301 // mule2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1348744438 // ORACLE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -511601230 // ORACLE2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -808457413 // PATRIOT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE -377465520 // PENUMBRA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 1830407356 // PEYOTE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -2095439403 // PHOENIX
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1507916787 // PICADOR
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -119658072 // PONY
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 943752001 // pony2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1450650718 // PRAIRIE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1883869285 // PREMIER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1150599089 // PRIMO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1645267888 // RANCHERXL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE 1933662059 // rancherxl2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1934452204 // RAPIDGT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1737773231 // RapidGT2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -1651067813 // RADI
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -667151410 // ratloader
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -1207771834 // REBEL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE -14495224 // REGINA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -2045594037 // rebel2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -227741703 // RUINER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1162065741 // RUMPO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 2136773105 // ROCOTO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE 627094268 // ROMERO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE -1685021548 // SABREGT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -599568815 // SADLER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 2
		BREAK
		CASE 734217681 // sadler2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1189015600 // SANDKING
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE 989381445 // sandking2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 6
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE -1255452397 // SCHAFTER2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -746882698 // schwarzer
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1221512915 // SEMINOLE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 2
		BREAK
		CASE 1349725314 // SENTINEL
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE 873639469 // sentinel2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE -1122289213 // ZION
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE -1193103848 // zion2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 1337041428 // SERRANO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE 1922257928 // sheriff2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1477580979 // STANIER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 1545842587 // STINGER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE -2098947590 // STINGERGT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE 1723137093 // STRATUM
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 970598228 // SULTAN
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 1
		BREAK
		CASE 1123216662 // SUPERD
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
		CASE 384071873 // SURANO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 699456151 // SURFER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1311240698 // Surfer2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1894894188 // SURGE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1008861746 // tailgater
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 5
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 464687292 // TORNADO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE 1531094468 // tornado2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1762279763 // tornado3
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -442313018 // towtruck2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 523724515 // VOODOO2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 1777363799 // WASHINGTON
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE 65402552 // YOUGA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 2.5000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 2
		BREAK
		CASE 758895617 // ztype
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 788045382 // sanchez
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 16
			sData.iModKitType = 0
		BREAK
		CASE -1453280962 // sanchez2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 1672195559 // AKUMA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE 11251904 // carbonrs
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -2140431165 // BAGGER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 4
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -114291515 // BATI
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -891462355 // bati2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 24
			sData.iModKitType = 0
		BREAK
		CASE -893578776 // RUFFIAN
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE 2006142190 // DAEMON
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -1670998136 // DOUBLE
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -909201658 // PCJ
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 3
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 27
			sData.iModKitType = 0
		BREAK
		CASE -140902153 // VADER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 25
			sData.iModKitType = 0
		BREAK
		CASE -825837129 // VIGERO
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 4
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 1
		BREAK
		CASE 55628203 // faggio2
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE 301427732 // HEXER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -634879114 // NEMESIS
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 0
		BREAK
		CASE -2122757008 // stunt
			sData.iModCountEngine = 1
			sData.iModCountBrakes = 1
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 1
			sData.iModCountHorn = 1
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1214505995 // shamal
			sData.iModCountEngine = 1
			sData.iModCountBrakes = 1
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 1
			sData.iModCountHorn = 1
			sData.iModCountSuspension = 1
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 0
		BREAK
		CASE -1352468814 // TRFLAT
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 1.0000
			sData.iModColoursThatCanBeSet = 16
			sData.iModKitType = 0
		BREAK
	ENDSWITCH
	SWITCH iModel
		CASE -1216765807 // ADDER
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 3
		BREAK
		CASE -1622444098 // VOLTIC
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 1
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 3.2500
			sData.iModColoursThatCanBeSet = 19
			sData.iModKitType = 1
		BREAK
		CASE 338562499 // VACCA
			sData.iModCountEngine = 5
			sData.iModCountBrakes = 4
			sData.iModCountExhaust = 2
			sData.iModCountArmour = 6
			sData.iModCountHorn = 53
			sData.iModCountSuspension = 5
			sData.fModPriceModifier = 5.0000
			sData.iModColoursThatCanBeSet = 17
			sData.iModKitType = 3
		BREAK
	ENDSWITCH
ENDPROC

