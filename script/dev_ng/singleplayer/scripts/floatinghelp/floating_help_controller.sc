USING "help_at_location.sch"
USING "commands_hud.sch"
USING "commands_script.sch"
USING "script_maths.sch"
USING "script_player.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
#ENDIF

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widget_debug
	INT i_debug_slot = -1
	INT i_debug_duration = DEFAULT_HELP_TEXT_TIME
	INT i_debug_arrow_dir = 0
	BOOL b_debug_print_help = FALSE
	BOOL b_debug_clear_help = FALSE
	BOOL b_debug_use_2d_offset = FALSE
	BOOL b_debug_use_screen_offset = FALSE
	BOOL b_debug_turn_on_prints = FALSE
	VECTOR v_debug_offset = <<0.0, 0.0, 1.0>>
	BOOL b_debug_reset_help[MAX_NUMBER_OF_FLOATING_HELP]
	INT i_new_time[MAX_NUMBER_OF_FLOATING_HELP]
	INT i_arrow_direction[MAX_NUMBER_OF_FLOATING_HELP]
	INT i_message_style[MAX_NUMBER_OF_FLOATING_HELP]
	PROC CREATE_WIDGETS()
		widget_debug = START_WIDGET_GROUP("Floating Help")
			ADD_WIDGET_BOOL("Enable debug prints", b_debug_turn_on_prints)
			ADD_WIDGET_BOOL("Print test help", b_debug_print_help)
			ADD_WIDGET_BOOL("Clear help", b_debug_clear_help)
			ADD_WIDGET_INT_SLIDER("Slot", i_debug_slot, -1, 2, 1)
			ADD_WIDGET_INT_SLIDER("Help duration", i_debug_duration, -1, 50000, 100)
			ADD_WIDGET_VECTOR_SLIDER("Offset", v_debug_offset, -2.0, 2.0, 0.01)
			ADD_WIDGET_INT_SLIDER("arrow direction", i_debug_arrow_dir, 0, 4, 1)
			ADD_WIDGET_BOOL("Use 2D offset from entity", b_debug_use_2d_offset)
			ADD_WIDGET_BOOL("Use screen coords", b_debug_use_screen_offset)
			
			TEXT_LABEL_31 txTlabel
			INT i
			REPEAT COUNT_OF(g_sFloatingHelpData) i
				txTlabel = "eHELP_TEXT_FLOATING_ID - "
				txTlabel += i
				START_WIDGET_GROUP(txTlabel)
					ADD_WIDGET_BOOL("Reset Help Message", b_debug_reset_help[i])
					ADD_WIDGET_INT_SLIDER("New Duration",i_new_time[i] , -1, 50000, 1)
					ADD_WIDGET_INT_SLIDER("Current timer", g_sFloatingHelpData[i].i_timer , -1, 50000, 1)
					ADD_WIDGET_INT_SLIDER("arrow direction", i_arrow_direction[i], 0, 4, 1)
					ADD_WIDGET_INT_SLIDER("message style", i_message_style[i], 0, 1, 1)
					ADD_WIDGET_FLOAT_SLIDER("X Coord", g_sFloatingHelpData[i].v_pos.x, -10000.0, 10000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Y Coord", g_sFloatingHelpData[i].v_pos.y, -10000.0, 10000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Z Coord", g_sFloatingHelpData[i].v_pos.z, -10000.0, 10000.0, 0.001)
			STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC DESTROY_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widget_debug)
			DELETE_WIDGET_GROUP(widget_debug)
		ENDIF
	ENDPROC
	
	PROC UPDATE_WIDGETS()
		IF b_debug_print_help
			FLOATING_HELP_SLOT_TYPE e_slot = FLOATING_HELP_FIND_NEXT_FREE_SLOT
		
			IF i_debug_slot > -1
				e_slot = INT_TO_ENUM(FLOATING_HELP_SLOT_TYPE, i_debug_slot)
			ENDIF
		
			IF b_debug_use_screen_offset
				HELP_AT_SCREEN_LOCATION("CMN_MLEAVE", v_debug_offset.x, v_debug_offset.y, INT_TO_ENUM(eARROW_DIRECTION, i_debug_arrow_dir), i_debug_duration, e_slot)
			ELIF b_debug_use_2d_offset
				HELP_AT_ENTITY_2D_OFFSET("CMN_MLEAVE", PLAYER_PED_ID(), v_debug_offset.x, v_debug_offset.y, INT_TO_ENUM(eARROW_DIRECTION, i_debug_arrow_dir), i_debug_duration, e_slot)
			ELSE
				HELP_AT_ENTITY_OFFSET("CMN_MLEAVE", PLAYER_PED_ID(), v_debug_offset, INT_TO_ENUM(eARROW_DIRECTION, i_debug_arrow_dir), i_debug_duration, e_slot)
			ENDIF
		
			b_debug_print_help = FALSE
		ENDIF
		
		IF b_debug_clear_help
			IF i_debug_slot = -1
				CLEAR_ALL_FLOATING_HELP()
			ELSE	
				CLEAR_FLOATING_HELP(INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i_debug_slot))
			ENDIF
			
			b_debug_clear_help = FALSE
		ENDIF
		
		/*IF IS_THIS_FLOATING_HELP_TEXT_BEING_DISPLAYED(FLOATING_HELP_TEXT_ID_1, "CMN_MLEAVE")
			PRINTLN("TEST")
		ENDIF
		
		IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED("CMN_MLEAVE")
			PRINTLN("TEST2")
		ENDIF*/
	ENDPROC
#ENDIF


/// PURPOSE:
///    Updates all floating help: repositions, manages timers and clears if necessary.
PROC UPDATE_FLOATING_HELP()
	INT i = 0
	
	REPEAT COUNT_OF(g_sFloatingHelpData) i
		IF g_sFloatingHelpData[i].i_timer != 0
			eHELP_TEXT_FLOATING_ID eSlotID = INT_TO_ENUM(eHELP_TEXT_FLOATING_ID, i)
		
			IF GET_GAME_TIMER() > g_sFloatingHelpData[i].i_timer
			AND g_sFloatingHelpData[i].i_timer != -1
				#IF IS_DEBUG_BUILD
					IF b_debug_turn_on_prints
						PRINTLN("floating_help_controller: timer finished for help slot ", i)
					ENDIF
				#ENDIF
			
				//Help text has displayed for the full amount: clear it.
				IF private_IS_THIS_FLOATING_HELP_BEING_DISPLAYED(i)
					CLEAR_FLOATING_HELP(eSlotID, FALSE)
				ENDIF
				private_RESET_FLOATING_HELP_ELEMENT(i)
			ELSE
				IF private_IS_THIS_FLOATING_HELP_BEING_DISPLAYED(i)
					//If the help text is still off screen, increment the timer so it doesn't count down until the player has viewed the text.
					IF g_sFloatingHelpData[i].i_timer != -1
						IF NOT IS_BIT_SET(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_HAS_BEEN_LOOKED_AT)
							g_sFloatingHelpData[i].i_timer += ROUND(GET_FRAME_TIME() * 1000.0)
							IF IS_FLOATING_HELP_TEXT_ON_SCREEN(eSlotID)
								SET_BIT(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_HAS_BEEN_LOOKED_AT)
							ENDIF
						ENDIF
					ENDIF
					
					//Set the help position: use different commands depending on data stored.
					IF g_sFloatingHelpData[i].v_pos.z != FLOATING_HELP_2D_Z_VALUE
						IF g_sFloatingHelpData[i].entity != NULL
							IF NOT IS_ENTITY_DEAD(g_sFloatingHelpData[i].entity)
								IF NOT IS_BIT_SET(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_USE_2D_OFFSET_FROM_ENTITY)
									SET_FLOATING_HELP_TEXT_WORLD_POSITION(eSlotID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sFloatingHelpData[i].entity, g_sFloatingHelpData[i].v_pos))
								ELSE
									SET_FLOATING_HELP_TEXT_TO_ENTITY(eSlotID, g_sFloatingHelpData[i].entity, g_sFloatingHelpData[i].v_pos.x, g_sFloatingHelpData[i].v_pos.y)
								ENDIF
							ENDIF
						ELIF (g_sFloatingHelpData[i].v_pos.x != 0.0 OR g_sFloatingHelpData[i].v_pos.y != 0.0 OR g_sFloatingHelpData[i].v_pos.z != 0.0)
							SET_FLOATING_HELP_TEXT_WORLD_POSITION(eSlotID, g_sFloatingHelpData[i].v_pos)
						ENDIF
					ELSE
						SET_FLOATING_HELP_TEXT_SCREEN_POSITION(eSlotID, g_sFloatingHelpData[i].v_pos.x, g_sFloatingHelpData[i].v_pos.y)
					ENDIF
				ELSE
					//If the help text isn't displayed but the data is still active then reset it after some time.
					IF GET_GAME_TIMER() - g_sFloatingHelpData[i].i_start_time > 1000
						private_RESET_FLOATING_HELP_ELEMENT(i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Re-print the help messages if the bool has been set by the widget. 
		#IF IS_DEBUG_BUILD
			IF b_debug_reset_help[i]
				IF IS_BIT_SET(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_STRING)
					IF IS_BIT_SET(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
						//IF it is with a literal string and a number
						private_PRINT_FLOATING_HELP(	g_sFloatingHelpData[i].s_current_label,	g_sFloatingHelpData[i].s_literal_label, g_sFloatingHelpData[i].i_help_number, i_new_time[i],
														INT_TO_ENUM(HELP_MESSAGE_STYLE, i_message_style[i]), g_sFloatingHelpData[i].v_pos, g_sFloatingHelpData[i].entity, INT_TO_ENUM(eARROW_DIRECTION, i_arrow_direction[i]),
														TRUE, TRUE, INT_TO_ENUM(FLOATING_HELP_SLOT_TYPE, i))			 
					ELSE
						//IF it is with a literal string and no number
						private_PRINT_FLOATING_HELP(	g_sFloatingHelpData[i].s_current_label,	g_sFloatingHelpData[i].s_literal_label, g_sFloatingHelpData[i].i_help_number,i_new_time[i],
														INT_TO_ENUM(HELP_MESSAGE_STYLE, i_message_style[i]), g_sFloatingHelpData[i].v_pos, g_sFloatingHelpData[i].entity, INT_TO_ENUM(eARROW_DIRECTION, i_arrow_direction[i]),
														TRUE, FALSE, INT_TO_ENUM(FLOATING_HELP_SLOT_TYPE, i))	
					ENDIF
				ELIF IS_BIT_SET(g_sFloatingHelpData[i].i_flags, FLOATING_HELP_FLAGS_PRINT_WITH_NUMBER)
					//IF it is with a no literal string and a number
					private_PRINT_FLOATING_HELP(	g_sFloatingHelpData[i].s_current_label,	g_sFloatingHelpData[i].s_literal_label, g_sFloatingHelpData[i].i_help_number,i_new_time[i],
													INT_TO_ENUM(HELP_MESSAGE_STYLE, i_message_style[i]), g_sFloatingHelpData[i].v_pos, g_sFloatingHelpData[i].entity, INT_TO_ENUM(eARROW_DIRECTION, i_arrow_direction[i]),
													FALSE, TRUE, INT_TO_ENUM(FLOATING_HELP_SLOT_TYPE, i))	
				ELSE
					//IF it is with a no literal string and no number
					private_PRINT_FLOATING_HELP(	g_sFloatingHelpData[i].s_current_label,	g_sFloatingHelpData[i].s_literal_label, g_sFloatingHelpData[i].i_help_number,i_new_time[i],
													INT_TO_ENUM(HELP_MESSAGE_STYLE, i_message_style[i]), g_sFloatingHelpData[i].v_pos, g_sFloatingHelpData[i].entity, INT_TO_ENUM(eARROW_DIRECTION, i_arrow_direction[i]),
													FALSE, FALSE, INT_TO_ENUM(FLOATING_HELP_SLOT_TYPE, i))		
				ENDIF
				b_debug_reset_help[i] = FALSE
			ENDIF
		#ENDIF
	ENDREPEAT
ENDPROC

SCRIPT 
	//This script needs to be usable by multiplayer
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() 
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		WAIT(0)
		
		UPDATE_FLOATING_HELP()
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
		#ENDIF
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		DESTROY_WIDGETS()
	#ENDIF
ENDSCRIPT