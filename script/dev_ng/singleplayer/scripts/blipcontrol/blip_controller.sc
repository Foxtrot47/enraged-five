// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	blip_controller.sch
//		AUTHOR			:	Ak
//		DESCRIPTION		:	The manager script for the global blip data.
//							Changes to the data are made with the procs in
//							blip_control_public.sch
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "selector_public.sch"
USING "blip_control_public.sch"
USING "blip_ambient.sch"
USING "candidate_public.sch"

 
BOOL bInExile = FALSE
BOOL bInExileLastTick = FALSE
BOOL bFlashAlt


#IF IS_DEBUG_BUILD
	BOOL d_BlipCtrl_bWidgetCreate = FALSE
	BOOL d_BlipCtrl_bWidgetsCreated = FALSE
	BOOL d_BlipCtrl_bWidgetModeActive = FALSE
	
	// Kenneth R.
	// Widget values have been moved to blip_globals.sch to stop thread stack overflow.
#ENDIF
		

PROC Script_Cleanup()
	TERMINATE_THIS_THREAD()
ENDPROC


#IF IS_DEBUG_BUILD
	PROC DEBUG_CREATE_WIDGETS()
		IF d_BlipCtrl_bWidgetsCreated
			EXIT
		ENDIF
		
		
		// Initialise widgets.
		START_WIDGET_GROUP("Static blips")

			ADD_WIDGET_BOOL("ACTIVATE DEBUG WIDGETS",d_BlipCtrl_bWidgetModeActive)

			START_WIDGET_GROUP("Teleports")
				INT i
				REPEAT STATIC_BLIP_NAME_DUMMY_FINAL i
					ADD_WIDGET_BOOL(DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i),d_BlipCtrl_bWidgetBools[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Status")
				// Add widget group for static blip status information.
				REPEAT STATIC_BLIP_NAME_DUMMY_FINAL i  
					START_WIDGET_GROUP(DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i))
						// STATIC_BLIP_SETTING_ACTIVE
						ADD_WIDGET_BOOL("Blip Active",d_BlipCtrl_bWidgetBoolActive[i])
						
						// STATIC_BLIP_SETTING_HIDE_INSIDE
						ADD_WIDGET_BOOL("Hidden indoors",d_BlipCtrl_bWidgetBoolHiddenInside[i])
						
						// STATIC_BLIP_SETTING_HIDE_OUTSIDE
						ADD_WIDGET_BOOL("Hidden outside",d_BlipCtrl_bWidgetBoolHiddenOutside[i])
						
						// STATIC_BLIP_SETTING_FLASH_ON_ACTIVE
						ADD_WIDGET_BOOL("Flash when activated",d_BlipCtrl_bWidgetBoolFlashOnActive[i])
						
						// STATIC_BLIP_SETTING_HAS_FLASHED
						ADD_WIDGET_BOOL("Has flashed",d_BlipCtrl_bWidgetBoolHasFlashed[i])
						
						// STATIC_BLIP_SETTING_APPEAR_FRONT
						ADD_WIDGET_BOOL("Appear on front end map",d_BlipCtrl_bWidgetBoolAppearFront[i])
					
						//  STATIC_BLIP_SETTING_RADAR_SHORT
						ADD_WIDGET_BOOL("Appear on radar short range",d_BlipCtrl_bWidgetBoolRadarShort[i])
						
						// STATIC_BLIP_SETTING_RADAR_LONG
						ADD_WIDGET_BOOL("Appear on radar long range",d_BlipCtrl_bWidgetBoolRadarLong[i])
						
						// STATIC_BLIP_SETTING_HIDE_ON_MISSION
						ADD_WIDGET_BOOL("Hide during mission",d_BlipCtrl_bWidgetBoolHideOnMission[i])
						
						// STATIC_BLIP_SETTING_ZONE_SPECIFIC
						ADD_WIDGET_BOOL("Zone specific",d_BlipCtrl_bWidgetBoolZoneSpecific[i])
						
						// STATIC_BLIP_SETTING_DISCOVERABLE
						ADD_WIDGET_BOOL("Discoverable blip",d_BlipCtrl_bWidgetBoolDiscoverable[i])
						
						// STATIC_BLIP_SETTING_DISCOVERED
						ADD_WIDGET_BOOL("Static blip has been discovered",d_BlipCtrl_bWidgetBoolDiscovered[i])
						
						// STATIC_BLIP_ACTIVATION_STATE_PREPOST_MISSION
						ADD_WIDGET_BOOL("Active supressed due to mission",d_BlipCtrl_bWidgetBoolSupressedDueToMission[i])
						
						// STATIC_BLIP_SETTING_CHARACTER_SPECIFIC
						ADD_WIDGET_BOOL("Char specific",d_BlipCtrl_bWidgetBoolCharSpecific[i])
						
						// STATIC_BLIP_SETTING_OTHER_CHARACTER_SPECIFIC
						ADD_WIDGET_BOOL("Other char specific",d_BlipCtrl_bWidgetBoolSecondaryCharSpecific[i])
						
						// STATIC_BLIP_IGNORES_CHAR_SPECIFIC_MODE_BOOL
						ADD_WIDGET_BOOL("Ignores char specific mode",d_BlipCtrl_bWidgetBoolUniversal[i])
						
						// STATIC_BLIP_SETTING_NO_LONG_RANGE_ON_MISSION
						ADD_WIDGET_BOOL("Not long range during mission",d_BlipCtrl_bWidgetBoolNotLongRangeDuringMission[i])
						
						// STATIC_BLIP_SETTING_TIME_SPECIFIC
						ADD_WIDGET_BOOL("Time specific",d_BlipCtrl_bWidgetBoolTimeSpecific[i])
						
						//STATIC_BLIP_SETTING_ON_WANTED_HIDE
						ADD_WIDGET_BOOL("Hide On Wanted",d_BlipCtrl_bWidgetBoolOnWantedHide[i])
						
						//STATIC_BLIP_SETTING_HIDE_ON_RANDOM_EVENT
						ADD_WIDGET_BOOL("Hide On Random Event",d_BlipCtrl_bWidgetBoolHideOnRandomEvent[i])
					STOP_WIDGET_GROUP()
				ENDREPEAT
			
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		d_BlipCtrl_bWidgetsCreated = TRUE
	ENDPROC
#ENDIF


PROC CONFIGURE_STATIC_BLIP(BLIP_INDEX bi,INT i)
	enumCharacterList cchar = GET_CURRENT_PLAYER_PED_ENUM()

	SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(bi,TRUE)
	SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOW_LOWEST)
	
	SET_BLIP_COLOUR(bi, GET_STATIC_BLIP_COLOUR(INT_TO_ENUM(STATIC_BLIP_NAME_ENUM,i)))
	
	IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_MISSION
	OR g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_RANDOMCHAR
		SWITCH cchar
			CASE CHAR_MICHAEL
				SET_BLIP_COLOUR(bi, BLIP_COLOUR_MICHAEL)
			BREAK
			CASE CHAR_FRANKLIN
				SET_BLIP_COLOUR(bi, BLIP_COLOUR_FRANKLIN)
			BREAK
			CASE CHAR_TREVOR
				SET_BLIP_COLOUR(bi, BLIP_COLOUR_TREVOR)
			BREAK
		ENDSWITCH
	ENDIF

	// This causes area blips to disappear
	IF NOT IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_AREA_BLIP)
		SET_BLIP_SCALE(bi, 1.0)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(g_GameBlips[i].txtBlipName)
		IF DOES_TEXT_LABEL_EXIST(g_GameBlips[i].txtBlipName)
			#IF IS_DEBUG_BUILD
				IF !g_bSuppressBlipDebug 
					CDEBUG1LN(DEBUG_BLIP, "Set name of blip: ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " to ", g_GameBlips[i].txtBlipName)
					CPRINTLN(DEBUG_BLIP," // ")
				ENDIF
			#ENDIF
			SET_BLIP_NAME_FROM_TEXT_FILE(bi, g_GameBlips[i].txtBlipName)
		ENDIF
	ENDIF
	
	// Does the blip appear on the map screen?
	BOOL bOnMap = IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_APPEAR_FRONT)
	
	// Does the blip appear on the radar when its in range?
	BOOL bShortRadar = IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_RADAR_SHORT)
	
	// Does the blip appear on the radar edges when at long range?
	BOOL bLongRadar = IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_RADAR_LONG)
	
	// All true = DISPLAY_BOTH
	// Finally if the override flag is set then go with short if on mission
	BOOL bLongOverrideOnMission = FALSE 
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
		bLongOverrideOnMission = IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_NO_LONG_RANGE_ON_MISSION)
		#IF IS_DEBUG_BUILD
			IF !g_bSuppressBlipDebug 
				CDEBUG3LN(DEBUG_SYSTEM, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " no long range state on mission.")
		 	ENDIF
		#ENDIF
	ENDIF

	IF bLongRadar AND (NOT bLongOverrideOnMission)
		SET_BLIP_AS_SHORT_RANGE(bi,FALSE)
	ELSE
		SET_BLIP_AS_SHORT_RANGE(bi,TRUE)
	ENDIF

	IF bShortRadar AND bOnMap
		//show both
		SET_BLIP_DISPLAY(bi,DISPLAY_BOTH)
	ELSE
		IF bShortRadar
			//shortradar but not map
			SET_BLIP_DISPLAY(bi,DISPLAY_RADAR_ONLY)
		ENDIF
		IF bOnMap
			//on map but not short radar
			SET_BLIP_DISPLAY(bi,DISPLAY_MAP)
		ENDIF
	ENDIF

	SWITCH g_GameBlips[i].eCategory
		CASE STATIC_BLIP_CATEGORY_SHOP // note cars and ammunation will get changed below as well due to 1458983
			SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOW_LOWEST)
			SET_BLIP_HIGH_DETAIL(bi,FALSE) 
		BREAK
		
		CASE STATIC_BLIP_CATEGORY_MINIGAME
		CASE STATIC_BLIP_CATEGORY_AMBIENT
			SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOW_LOWEST)//This enum name sucks. Thanks Dp
			SET_BLIP_HIGH_DETAIL(bi,FALSE)
		BREAK
		
		CASE STATIC_BLIP_CATEGORY_SAVEHOUSE
			SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOW)	
			SET_BLIP_HIGH_DETAIL(bi,TRUE)
		BREAK
		
		CASE STATIC_BLIP_CATEGORY_PROPERTY
			SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOWEST)			
			SET_BLIP_CATEGORY(bi,BLIP_CATEGORY_PROPERTY)
			SET_BLIP_HIGH_DETAIL(bi,FALSE)
		BREAK	
		
		CASE STATIC_BLIP_CATEGORY_MISSION
		CASE STATIC_BLIP_CATEGORY_RANDOMCHAR
			SET_BLIP_HIGH_DETAIL(bi,TRUE)
			
			// Ensure story missions and RCM's are given the correct priority
			IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_MISSION
				SET_BLIP_PRIORITY(bi,BLIPPRIORITY_HIGH)
			ELSE
				SET_BLIP_PRIORITY(bi,BLIPPRIORITY_MED)
			ENDIF
			
			// If is char specific and owner not active then make 50% scale
			// otherwise normal scale
			IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_CHARACTER_SPECIFIC)
			
				IF NOT IS_STATIC_BLIP_OWNERS_ACTIVE(INT_TO_ENUM(STATIC_BLIP_NAME_ENUM,i))
				
					IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_AREA_BLIP)
						// Don't display area blips when an invalid character
						SET_BLIP_ALPHA(bi, 0)
					ELSE
						
						//SET_BLIP_COLOUR(bi ,BLIP_COLOUR_INACTIVE_MISSION)
						IF g_GameBlips[i].eVisibleOnlyToChar = CHAR_MICHAEL
							SET_BLIP_COLOUR(bi, BLIP_COLOUR_MICHAEL)
						endif
						IF g_GameBlips[i].eVisibleOnlyToChar = CHAR_FRANKLIN
							SET_BLIP_COLOUR(bi, BLIP_COLOUR_FRANKLIN)
						endif
						IF g_GameBlips[i].eVisibleOnlyToChar = CHAR_TREVOR
							SET_BLIP_COLOUR(bi, BLIP_COLOUR_TREVOR)
						endif
						
						//SET_BLIP_ALPHA(bi, 122)
						
						// RCM blips should appear behind and be short range.
						IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_RANDOMCHAR
							SET_BLIP_AS_SHORT_RANGE(bi, IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_RADAR_SHORT))  //P.D. fix for bug 1454314
							SET_BLIP_PRIORITY(bi,BLIPPRIORITY_LOW)
							SET_BLIP_AS_SHORT_RANGE(bi, TRUE)						
							SET_BLIP_HIDDEN_ON_LEGEND(bi, TRUE)
							SET_BLIP_SCALE(bi, 0.77)
						Else
							SET_BLIP_SCALE(bi, 0.72)
						endif
					ENDIF
				ELSE
					// Restore area blip - we set alpha to zero when invalid character
					IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_AREA_BLIP)
						SET_BLIP_ALPHA(bi, 128)
					ENDIF
					
					// Ensure RCM blip isn't hidden on legend when not greyed out
					IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_RANDOMCHAR
						SET_BLIP_HIDDEN_ON_LEGEND(bi, FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		DEFAULT
			SET_BLIP_PRIORITY(bi,BLIPPRIORITY_MED)
		BREAK
	ENDSWITCH

	//AMMUNATION AND MOD SHOP BLIP SPECIFIC SETTINGS // 1458983
	SWITCH INT_TO_ENUM(STATIC_BLIP_NAME_ENUM,i)
		CASE STATIC_BLIP_SHOP_GUN_01_DT	 			// Weapons - Downtown
		CASE STATIC_BLIP_SHOP_GUN_02_SS	 			// Weapons - Sandy Shores
		CASE STATIC_BLIP_SHOP_GUN_03_HW 			// Weapons - Vinewood
		CASE STATIC_BLIP_SHOP_GUN_04_ELS			// Weapons - East Los Santos
		CASE STATIC_BLIP_SHOP_GUN_05_PB				// Weapons - Paleto Bay
		CASE STATIC_BLIP_SHOP_GUN_06_LS				// Weapons - Little Seoul
		CASE STATIC_BLIP_SHOP_GUN_07_MW	 			// Weapons - Morningwood
		CASE STATIC_BLIP_SHOP_GUN_08_CS	 			// Weapons - Countryside
		CASE STATIC_BLIP_SHOP_GUN_09_GOH 			// Weapons - Great Ocean Highway
		CASE STATIC_BLIP_SHOP_GUN_10_VWH 			// Weapons - Vinewood Hills
		CASE STATIC_BLIP_SHOP_GUN_11_ID1			// Weapons - Cypress Flats
			SET_BLIP_HIGH_DETAIL(bi,TRUE) 
			BREAK
	ENDSWITCH

	IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[i].bMultiCoordAndSprite
		SWITCH cchar
			CASE CHAR_FRANKLIN
			CASE CHAR_MICHAEL
			CASE CHAR_TREVOR
				SET_BLIP_COORDS(g_GameBlips[i].biBlip,g_GameBlips[i].vCoords[cchar])
			BREAK
		ENDSWITCH
	ENDIF

	// Should the blip show hover info. Can't set this on all blips by default as having this flagged on
	// causes the scale and location name to disappear which looks strange on blips that have no info
	// to display. #1146770
	IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_SHOW_MAP_HOVER_INFO) 
		SET_BLIP_AS_MISSION_CREATOR_BLIP(bi, TRUE)
	ELSE
		SET_BLIP_AS_MISSION_CREATOR_BLIP(bi, FALSE)
	ENDIF
	
	//blip tick effect
	IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_TICKED) 
		SHOW_TICK_ON_BLIP(bi, TRUE)
	ELSE
		SHOW_TICK_ON_BLIP(bi, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_DISPLAY_AS_INACTIVE)
		SET_BLIP_COLOUR(bi ,BLIP_COLOUR_INACTIVE_MISSION)
	ENDIF
	
ENDPROC


INT htime = 0
FUNC BOOL TIME_CHANGED_SINCE_LAST_CALL()//only interested in hours
	
	IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
		RETURN FALSE // no time of day blips on mission
	ENDIF
	
	IF GET_CLOCK_HOURS() != htime
		htime = GET_CLOCK_HOURS()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


INT iWantedLev = 0
FUNC BOOL IS_PLAYER_WANTED_CHANGED_SINCE_LAST_CALL()
	IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) != iWantedLev
		iWantedLev = GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


BOOL bOutside = TRUE
INTERIOR_INSTANCE_INDEX last = NULL
FUNC BOOL DID_INTERIOR_CHANGE_SINCE_LAST_CALL()
	
	IF IS_ENTITY_DEAD(GET_PLAYER_PED(GET_PLAYER_INDEX()))
		RETURN FALSE
	ENDIF
	
	INTERIOR_INSTANCE_INDEX current = GET_INTERIOR_FROM_ENTITY(GET_PLAYER_PED(GET_PLAYER_INDEX()))
	
	IF last != current
		last = current
		CDEBUG2LN(debug_blip,"DID_INTERIOR_CHANGE_SINCE_LAST_CALL: Interior changed to outside? ", current = NULL)
		
		IF current = NULL
		OR IS_INTERIOR_A_TUNNEL(current)	//Don't consider tunnels as "inside" interiors
			bOutside = TRUE
		ELSE
		
			bOutside = FALSE
		
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL DID_MISSION_STATE_CHANGE_SINCE_LAST_CALL()
	BOOL ret = g_bBlipSystemMissionStateChange
	g_bBlipSystemMissionStateChange = FALSE
	
	IF ret 
		CDEBUG1LN(DEBUG_SYSTEM, "Blip system detected a mission state change.")
	ENDIF
	
	RETURN ret
ENDFUNC


BOOL bAmbState = FALSE
FUNC BOOL DID_AMBIENT_STATE_CHANGE_SINCE_LAST_CALL()
	IF bAmbState != IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
		bAmbState = IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL BLIP_WANTED_CHECK()
	IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) != 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC TURN_ON_CARWASH_FAIRGROUND_CABLE_CAR_BLIPS()
	IF	GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) = TRUE
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CARWASH_LONG, TRUE)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CARWASH_SHORT, TRUE)
		
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CABLECAR_BOTTOM, TRUE)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CABLECAR_TOP, TRUE)	

		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_FGROUND_BIGWHEEL, TRUE)
		SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_AMBIENT_FGROUND_BIGWHEEL)
		
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_FGROUND_RCOASTER, TRUE)
		SET_STATIC_BLIP_SHORT_RANGE(STATIC_BLIP_AMBIENT_FGROUND_RCOASTER)
	ENDIF
ENDPROC


PROC TURN_ON_CINEMA_BLIPS()
	IF	GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) = TRUE	
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_ACTIVITY_CINEMA_VINEWOOD, TRUE)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_ACTIVITY_CINEMA_DOWNTOWN, TRUE)
		SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_ACTIVITY_CINEMA_MORNINGWOOD, TRUE)
	ENDIF
ENDPROC


INT iClosestSaveHouse = -1
INT iLoggedActiveSaveHouseBlips = 0
INT WatchedSaveHouses[MAX_WATCHED_SAVEHOUSES]


PROC SAVEHOUSE_BLIP_CREATED(INT id)
	IF iLoggedActiveSaveHouseBlips = MAX_WATCHED_SAVEHOUSES	
		#IF IS_DEBUG_BUILD	
		
			CDEBUG1LN(DEBUG_SYSTEM, "The following safehouse blips were being watched at the time of the assert")
		
			INT l = 0
			REPEAT MAX_WATCHED_SAVEHOUSES l
				CDEBUG1LN(DEBUG_SYSTEM, "Blip number ", l ," : ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(WatchedSaveHouses[l]), ".")
			ENDREPEAT
			
			SCRIPT_ASSERT("blip_controller watching too many savehouses at once!")
		#ENDIF
		EXIT
	ENDIF
	
	INT i = 0
	
	INT freeIndex = -1
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP,"passed id is ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(id))
	#ENDIF
	
	REPEAT iLoggedActiveSaveHouseBlips i
		IF WatchedSaveHouses[i] = id
			CPRINTLN(DEBUG_BLIP,"Savehouse already being watched.")
			EXIT 
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP,"starting loop through WatchedSaveHouses")
	#ENDIF
	
	REPEAT MAX_WATCHED_SAVEHOUSES i
		#IF IS_DEBUG_BUILD
			IF WatchedSaveHouses[i] = -1
				CDEBUG1LN(DEBUG_BLIP,"WatchedSaveHouses[", i,"] is empty")
			ELSE
				CDEBUG1LN(DEBUG_BLIP,"WatchedSaveHouses[", i,"] is ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(WatchedSaveHouses[i]))
			ENDIF
		#ENDIF
		IF WatchedSaveHouses[i] = -1
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_BLIP,"first free space found at ", i, " setting to ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(id))
			#ENDIF
			freeIndex = i
			i = MAX_WATCHED_SAVEHOUSES
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP,"end of loop through WatchedSaveHouses")
	#ENDIF
	
	WatchedSaveHouses[freeIndex] = id
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_BLIP,DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(WatchedSaveHouses[freeIndex]), " added to watched savehouses")
	#ENDIF
	
	++iLoggedActiveSaveHouseBlips
ENDPROC


PROC SAVEHOUSE_BLIP_REMOVED(INT id)
	IF iLoggedActiveSaveHouseBlips < 1
		SCRIPT_ASSERT("blip_controller trying to remove a savehouse watch when list is empty!!")
		EXIT
	ENDIF
	
	INT i = 0
	REPEAT MAX_WATCHED_SAVEHOUSES i
		IF(WatchedSaveHouses[i] = id)
			--iLoggedActiveSaveHouseBlips
			WatchedSaveHouses[i] = -1
			
			IF id = iClosestSaveHouse
				iClosestSaveHouse = -1
			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC


// Keep the nearest safehouse blip long range and the others short range.
PROC DO_SAVEHOUSE_BLIP_PROXIMITY_CHECK()
	IF IS_PLAYER_DEAD(GET_PLAYER_INDEX())
		EXIT
	ENDIF

	VECTOR vp = GET_PLAYER_COORDS(GET_PLAYER_INDEX())
	
	INT iClosest = -1
	FLOAT fCloseDist = 1000000.0
	INT i = 0

	REPEAT MAX_WATCHED_SAVEHOUSES i
		IF WatchedSaveHouses[i] != -1
			IF DOES_BLIP_EXIST(g_GameBlips[WatchedSaveHouses[i]].biBlip)
				FLOAT dist = GET_DISTANCE_BETWEEN_COORDS(vp, GET_BLIP_COORDS(g_GameBlips[WatchedSaveHouses[i]].biBlip))
				IF(dist < fCloseDist)
					fCloseDist = dist
					iClosest = i	
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iClosestSaveHouse = iClosest 
		EXIT	
	ENDIF
	
	iClosestSaveHouse = iClosest
	
	IF iClosest = -1
		EXIT
	ENDIF
		
	REPEAT MAX_WATCHED_SAVEHOUSES i
		IF WatchedSaveHouses[i] != -1
			IF i = iClosest
				IF DOES_BLIP_EXIST(g_GameBlips[WatchedSaveHouses[i]].biBlip)
					CDEBUG2LN(DEBUG_BLIP,"Savehouse blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(WatchedSaveHouses[i]), " being set long range as it is the closest.")
					SET_STATIC_BLIP_LONG_RANGE(INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, WatchedSaveHouses[i]))
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(g_GameBlips[WatchedSaveHouses[i]].biBlip)
					CDEBUG2LN(DEBUG_BLIP,"Savehouse blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(WatchedSaveHouses[i]), " being set short range as it is not the closest.")
					SET_STATIC_BLIP_SHORT_RANGE(INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, WatchedSaveHouses[i]))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


//Manage the blip active state of one safehouse blip per frame.
PROC UPDATE_SAFEHOUSE_BLIPS_DISPLAYING()
	
	INT iSafeHouseIndex
	REPEAT NUMBER_OF_ACTIVE_SAVEHOUSE_LOCATIONS iSafeHouseIndex
		Update_Savehouse_Respawn_Blip(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSafeHouseIndex))
	ENDREPEAT

ENDPROC


/// PURPOSE:
///    Checks whether to activate/deactivate the world blip for Chop
PROC DO_CHOP_SAFEHOUSE_BLIP_CHECK()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN

		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_CHOP_THE_DOG_UNLOCKED)
		
			// Assert guard
			IF IS_PLAYER_PLAYING(PLAYER_ID()) // B*1406239
					
				// Franklin is at Vinewood Hills
				IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		
					// Ensure Franklin's aunt house is clear
					IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_AMBIENT_CHOP_SC)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CHOP_SC, FALSE)
					ENDIF

					// Enable Vinewood Hills blip
					IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_AMBIENT_CHOP_VH) 
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("chop")) = 0
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<19.30, 528.24, 169.63>>) > 50
								SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CHOP_VH, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// Clear Vinewood Hills - although in theory this shouldn't be possible unless debug warping back in flow
					IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_AMBIENT_CHOP_VH)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CHOP_VH, FALSE)
					ENDIF
					
					// Enable aunt's house Hills blip
					IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_AMBIENT_CHOP_SC) 
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("chop")) = 0
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-11.15, -1425.56, 29.67>>) > 50
								SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_AMBIENT_CHOP_SC, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


CONST_INT MINIMAP_MAX_BLIPS 150

enumCharacterList lastchar
enumCharacterList currchar 

BOOL bCharChanged = FALSE	
BOOL bPrologueModeChanged = FALSE
BOOL bTimeChanged = FALSE
BOOL bWantedChanged = FALSE
BOOL bMissionStateChanged = FALSE
BOOL bAmbientStateChange = FALSE
BOOL bOnPrologueMode = FALSE
BOOL bInExileChanged = FALSE
BOOL bInteriorChanged = FALSE


FUNC BOOL CHECK_FOR_CHANGE()

	BOOL somethingNewChanged = FALSE
	
	IF TIME_CHANGED_SINCE_LAST_CALL()
		IF !bTimeChanged
			bTimeChanged = TRUE 
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: time changed.")
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_CHANGED_SINCE_LAST_CALL()
		IF !bWantedChanged
			bWantedChanged = TRUE
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: wanted changed.")
		ENDIF
	ENDIF
	
	IF DID_MISSION_STATE_CHANGE_SINCE_LAST_CALL()
		IF !bMissionStateChanged
			bMissionStateChanged = TRUE
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: mission state changed.")
		ENDIF
	ENDIF
	
	IF DID_AMBIENT_STATE_CHANGE_SINCE_LAST_CALL()
		IF !bAmbientStateChange
			bAmbientStateChange = TRUE
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: ambient state changed.")
		ENDIF
	ENDIF

	bPrologueModeChanged = FALSE
	IF g_BlipSystemPrologueMode != bOnPrologueMode
		bOnPrologueMode = g_BlipSystemPrologueMode
		IF !bPrologueModeChanged
			bPrologueModeChanged = TRUE
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: prologue mode changed.")
		ENDIF
	ENDIF
	
	IF DID_INTERIOR_CHANGE_SINCE_LAST_CALL()
		IF !bInteriorChanged
			bInteriorChanged = TRUE
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: interior changed.")
		ENDIF
	ENDIF

	currchar = GET_CURRENT_PLAYER_PED_ENUM()
	IF currchar != lastchar
		CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: char changed.")
		lastchar = currchar
		
		bCharChanged = TRUE
		somethingNewChanged = TRUE
	ENDIF
	
	bInExileLastTick = bInExile
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		bInExile = TRUE
	ENDIF
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
		bInExile = FALSE
	ENDIF
	
	IF bInExile != bInExileLastTick
		IF !bInExileChanged
			somethingNewChanged = TRUE
			CPRINTLN(DEBUG_BLIP,"CHECK_FOR_CHANGE: exile mode changed.")
		ENDIF
		bInExileChanged = TRUE
	ENDIF
	
	RETURN somethingNewChanged
ENDFUNC


PROC RESET_CHANGE()
	 bCharChanged = FALSE	
	 bPrologueModeChanged = FALSE
	 bTimeChanged = FALSE
	 bWantedChanged = FALSE
	 bMissionStateChanged = FALSE
	 bAmbientStateChange = FALSE
	 bOnPrologueMode = FALSE
	 bInExileChanged = FALSE
	 bInteriorChanged = FALSE
ENDPROC


FUNC BOOL CHECK_BLIP_CHANGE_SETTINGS(INT i)

	// Check for things that could wake up this blip
	IF bWantedChanged AND IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ON_WANTED_HIDE)
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from WANTED LEVEL")
		RETURN TRUE
	ENDIF
	
	IF bMissionStateChanged 
	AND (IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_ON_MISSION) 
	OR IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_NO_LONG_RANGE_ON_MISSION) 
	OR IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MISSION_LEVEL_EXCLUSIVE))
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from MISSION STATE CHANGED")
		RETURN TRUE
	ENDIF
	
	IF bAmbientStateChange AND IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_ON_RANDOM_EVENT)
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from HIDE ON RANDOM EVENT")
		RETURN TRUE
	ENDIF
	
	IF bCharChanged 
	AND (IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_OTHER_CHARACTER_SPECIFIC) 
	OR IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_CHARACTER_SPECIFIC)
	OR IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE))
		 
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ",DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from CHAR CHANGED")
		RETURN TRUE
	ENDIF
	
	IF bPrologueModeChanged
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from PROLOGUE MODE CHANGED")
		RETURN TRUE
	ENDIF
	
	IF bInExileChanged
	AND (IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_BLIP_SETTING_HIDE_IN_EXILE))
		SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from HIDE IN EXILE")
		RETURN TRUE
	ENDIF
	
	IF bInteriorChanged
	AND (IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_HIDE_INSIDE) 
	OR IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_HIDE_OUTSIDE))
	OR g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_SAVEHOUSE
		SET_BIT(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_STATUS_CHANGED)
		CDEBUG3LN(DEBUG_BLIP,"Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i)," changed status from HIDE IN/OUTSIDE")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 


SCRIPT 

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		CPRINTLN(DEBUG_BLIP,"...blip_controller.sc has been forced to cleanup (SP to MP)")
		Script_Cleanup()
	ENDIF
	
	
	//1459074
	INT ij
	STATIC_BLIP_NAME_ENUM eBlip
	FOR ij = 0 TO NUMBER_OF_SHOPS-1
		IF IS_SHOP_AVAILABLE_IN_SINGLEPLAYER((INT_TO_ENUM(SHOP_NAME_ENUM, ij)))
			eBlip = GET_SHOP_BLIP_ENUM((INT_TO_ENUM(SHOP_NAME_ENUM, ij)))
			SET_STATIC_BLIP_NAME(eBlip, GET_SHOP_BLIP_NAME((INT_TO_ENUM(SHOP_NAME_ENUM, ij))))
		ENDIF
	ENDFOR
	
	//#1415245
	SET_STATIC_BLIP_HIDE_IN_EXTERIOR(STATIC_BLIP_MINIGAME_SHOOTING_RANGE1, TRUE)
	SET_STATIC_BLIP_HIDE_IN_EXTERIOR(STATIC_BLIP_MINIGAME_SHOOTING_RANGE2, TRUE)

	g_bBlipSystemRefreshDetector = TRUE
	WHILE g_bBlipSystemStartupLeash // Wait until startup positioning is done before processing.
		IF (GET_GAME_TIMER() % 250) = 0
			CPRINTLN(DEBUG_BLIP,"blip controller leashed. Waiting to be unleashed.")
		ENDIF
		#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow 
			g_bBlipSystemStartupLeash = FALSE
			CPRINTLN(DEBUG_BLIP,"blip controller leash skipped due to g_flowUnsaved.bUpdatingGameflow ")
		ENDIF
		#ENDIF
		WAIT(0)
	ENDWHILE

	INT i = 0
	REPEAT MAX_WATCHED_SAVEHOUSES i
		WatchedSaveHouses[i] = -1
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		// Initialise widgets.
		START_WIDGET_GROUP("Static blips switch")
		ADD_WIDGET_BOOL("CREATE DEBUG WIDGETS", d_BlipCtrl_bWidgetCreate)
		STOP_WIDGET_GROUP()
	#ENDIF
	
	SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
	
	// Prime all the blips for processing.
	i = 0
	REPEAT g_iTotalStaticBlips i
		SET_BIT(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_STATUS_CHANGED)
	ENDREPEAT
	
	bOnPrologueMode = g_BlipSystemPrologueMode
 
	BOOL ChangeLastPass = FALSE
	
	TURN_ON_CARWASH_FAIRGROUND_CABLE_CAR_BLIPS()
	TURN_ON_CINEMA_BLIPS()
	
	WHILE TRUE
	
		IF !ChangeLastPass
			RESET_CHANGE()
		ENDIF
		
		ChangeLastPass = CHECK_FOR_CHANGE()
		IF ChangeLastPass
			g_bBlipSystemRefreshDetector = TRUE
		ENDIF
		
		// Wake up and update any blips that need updating.
		INT Updated = 0
		INT Changed = 0
		i = 0
		
		IF g_bBlipSystemRefreshDetector
		
			TURN_ON_CARWASH_FAIRGROUND_CABLE_CAR_BLIPS()
			TURN_ON_CINEMA_BLIPS()
			
			INT iUpdateLimiter = 0
			
			REPEAT g_iTotalStaticBlips i
				BOOL bChanged = IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_STATUS_CHANGED)
				IF !bChanged
					IF CHECK_BLIP_CHANGE_SETTINGS(i)
						// Increment changed to trigger the wait check.
						++Changed
						bChanged = TRUE
					ENDIF
				ENDIF
				++Updated
				IF bChanged
					CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " being updated.")
					
					// Do status update for this blip.
					INT iSuppressionVotes = 0
					INT iSuppressionChecks = 0
					
					// Auto suppress if we're in Prologue mode.
					IF g_BlipSystemPrologueMode
						CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " prologue suppress.")
						++iSuppressionVotes
						++iSuppressionChecks
					// Otherwise do normal suppression checks.
					ELSE 
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ON_WANTED_HIDE)
							IF BLIP_WANTED_CHECK()
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " wanted suppress.")
								ENDIF
								#ENDIF
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_INSIDE)
							IF !bOutside
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " inside suppress.")
								ENDIF
								#ENDIF
														
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_OUTSIDE)
							IF bOutside
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " outside suppress.")
								ENDIF
								#ENDIF
														
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_ON_MISSION)
							IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " hide on mission suppress.")
								ENDIF
								#ENDIF
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MISSION_LEVEL_EXCLUSIVE) 
							IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(g_GameBlips[i].launchLevel)
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " hide on mission - NEW LAUNCH LEVEL VERSION - suppress.")
								ENDIF
								#ENDIF
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_HIDE_ON_RANDOM_EVENT)
							IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
								++iSuppressionVotes
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " random event suppress.")
								ENDIF
								#ENDIF
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_CHARACTER_SPECIFIC)
							IF g_GameBlips[i].eCategory != STATIC_BLIP_CATEGORY_MISSION // Mission and RCM blips have special fade rules
							AND g_GameBlips[i].eCategory != STATIC_BLIP_CATEGORY_RANDOMCHAR
								IF NOT IS_STATIC_BLIP_OWNERS_ACTIVE(INT_TO_ENUM(STATIC_BLIP_NAME_ENUM,i))
									++iSuppressionVotes
									#IF IS_DEBUG_BUILD
										IF !g_bSuppressBlipDebug
											CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " owner not active suppress.")
										ENDIF
									#ENDIF	
								ENDIF
								++iSuppressionChecks 
							ENDIF
						ENDIF

						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								SWITCH currchar
									CASE CHAR_FRANKLIN
									CASE CHAR_MICHAEL
									CASE CHAR_TREVOR
										// Valid chars, don't suppress.
									BREAK
									DEFAULT
										#IF IS_DEBUG_BUILD
										IF !g_bSuppressBlipDebug
										CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " invalid multichar suppress.")
										ENDIF
										#ENDIF
										++iSuppressionVotes // This blip cannot exist if the player is invalid.
									BREAK
								ENDSWITCH
							++iSuppressionChecks 
						ENDIF
						
						IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_BLIP_SETTING_HIDE_IN_EXILE) 
							IF bInExile
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " invalid multichar suppress.")
								ENDIF
								#ENDIF
								++iSuppressionVotes
							ENDIF
							++iSuppressionChecks 
						ENDIF
						
						IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_SAVEHOUSE
							UPDATE_SAFEHOUSE_BLIPS_DISPLAYING()
						ENDIF
					ENDIF
					
					IF (iSuppressionChecks > 0)
						IF iSuppressionVotes > 0  
							CLEAR_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVE)
						ELSE
							// Check its not only now becoming unsuppressed.
							IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVATED_EXTERNALLY)
								#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " no longer suppressed.")
								ENDIF
								#ENDIF
								SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVE)
							ENDIF
						ENDIF
					ELSE
						// Nothing wants to suppress this blip at all.
						#IF IS_DEBUG_BUILD
							IF !g_bSuppressBlipDebug
							CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " has nothing suppressing it this update.")
							ENDIF
						#ENDIF
						IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVATED_EXTERNALLY)
							SET_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVE)
						ENDIF
					ENDIF

					IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_ACTIVE)
					AND IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_VISIBILITY)
					
						BOOL doSaveHouseCheck = FALSE
						IF NOT DOES_BLIP_EXIST(g_GameBlips[i].biBlip)
							
							WHILE NOT (GET_NUMBER_OF_ACTIVE_BLIPS() < MINIMAP_MAX_BLIPS) // due to 584048
								#IF IS_DEBUG_BUILD
									IF !g_bSuppressBlipDebug
										CPRINTLN(DEBUG_BLIP,"!!!!!!!!!!!!!\n\nBlip controller is stalled while NOT(GET_NUMBER_OF_ACTIVE_BLIPS() < MINIMAP_MAX_BLIPS), checking again in 2 seconds\n\n!!!!!!!!!!!!!!!")
									ENDIF
								#ENDIF
								WAIT(2000)
							ENDWHILE
								
							IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								
								SWITCH currchar
									CASE CHAR_FRANKLIN
									CASE CHAR_MICHAEL
									CASE CHAR_TREVOR
										
										#IF IS_DEBUG_BUILD
											IF !g_bSuppressBlipDebug
												CDEBUG1LN(DEBUG_BLIP, "Blip ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " created for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(currchar), " at ", g_GameBlips[i].vCoords[currchar],".")
											ENDIF
										#ENDIF
										
										IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_AREA_BLIP)
											g_GameBlips[i].biBlip = ADD_BLIP_FOR_RADIUS(g_GameBlips[i].vCoords[0], g_GameBlips[i].fRadius)
											SET_BLIP_ALPHA(g_GameBlips[i].biBlip , 128)
											SHOW_HEIGHT_ON_BLIP(g_GameBlips[i].biBlip , FALSE)
										ELSE
											g_GameBlips[i].biBlip = ADD_BLIP_FOR_COORD(g_GameBlips[i].vCoords[currchar])
											SET_BLIP_SPRITE(g_GameBlips[i].biBlip, g_GameBlips[i].eSprite[currchar])
										ENDIF
									BREAK
									
									DEFAULT
										SCRIPT_ASSERT("Multichar blip didn't find a legal player char!")
										g_GameBlips[i].biBlip = ADD_BLIP_FOR_COORD(<<1,2,3>>)
									BREAK
								ENDSWITCH
								
							ELSE
								IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_AREA_BLIP)
									g_GameBlips[i].biBlip = ADD_BLIP_FOR_RADIUS(g_GameBlips[i].vCoords[0], g_GameBlips[i].fRadius)
									SET_BLIP_ALPHA(g_GameBlips[i].biBlip, 128)
									SHOW_HEIGHT_ON_BLIP(g_GameBlips[i].biBlip , FALSE)
								ELSE
									g_GameBlips[i].biBlip = ADD_BLIP_FOR_COORD(g_GameBlips[i].vCoords[0])
									SET_BLIP_SPRITE(g_GameBlips[i].biBlip, g_GameBlips[i].eSprite[0])
									
									// Rob - 2118989 - display dollar sign indicator on business properties for sale
									/*
									IF g_GameBlips[i].eSprite[0] = RADAR_TRACE_BUSINESS
										SHOW_FOR_SALE_ICON_ON_BLIP(g_GameBlips[i].biBlip, FALSE)
									ELIF g_GameBlips[i].eSprite[0] = RADAR_TRACE_BUSINESS_FOR_SALE
										SHOW_FOR_SALE_ICON_ON_BLIP(g_GameBlips[i].biBlip, TRUE)
									ENDIF
									*/
								ENDIF
							ENDIF
							
							IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_SAVEHOUSE
								doSaveHouseCheck = TRUE
							ENDIF
							
						// Do blip settings post creation.
						ELSE 
							// Otherwise make sure the position is correct anyway
							IF IS_BIT_SET(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								SWITCH currchar
									CASE CHAR_FRANKLIN
									CASE CHAR_MICHAEL
									CASE CHAR_TREVOR
										SET_BLIP_COORDS(g_GameBlips[i].biBlip, g_GameBlips[i].vCoords[currchar])
										CPRINTLN(DEBUG_BLIP,"blip_controller: Setting multicoord position ", currchar, "for blip ",DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i))
									BREAK
									DEFAULT
										SCRIPT_ASSERT("Multichar blip position set didn't find a legal player char!")
									BREAK
								ENDSWITCH
							ELSE
								SET_BLIP_COORDS(g_GameBlips[i].biBlip, g_GameBlips[i].vCoords[0])
							ENDIF
							
							IF IS_BIT_SET(g_GameBlips[i].iSetting, STATIC_BLIP_SETTING_WILL_FLASH_ON_NEXT_ACTIVE) 
								IF bFlashAlt
									SET_BLIP_FLASHES(g_GameBlips[i].biBlip,TRUE )
									bFlashAlt = FALSE
								ELSE
									SET_BLIP_FLASHES_ALTERNATE(g_GameBlips[i].biBlip,TRUE)
									bFlashAlt = TRUE
								ENDIF
								SET_BLIP_FLASH_TIMER(g_GameBlips[i].biBlip,10000)
								CLEAR_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_WILL_FLASH_ON_NEXT_ACTIVE)
							ELSE
								SET_BLIP_FLASHES(g_GameBlips[i].biBlip,FALSE )
							ENDIF
							
						ENDIF
						
						CONFIGURE_STATIC_BLIP(g_GameBlips[i].biBlip,i)
						
						IF doSaveHouseCheck 
							SAVEHOUSE_BLIP_CREATED(i)
						ENDIF
						
						++Changed
					ELSE
						#IF IS_DEBUG_BUILD
							IF !g_bSuppressBlipDebug
							CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " attempting remove.")
							ENDIF
						#ENDIF
						
						IF DOES_BLIP_EXIST(g_GameBlips[i].biBlip)			
							
							// Remove it
							#IF IS_DEBUG_BUILD
								IF !g_bSuppressBlipDebug
								CDEBUG3LN(DEBUG_BLIP, "Blip status ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(i), " removed.")
								ENDIF
							#ENDIF
							
							REMOVE_BLIP(g_GameBlips[i].biBlip)
							++Changed
							
							IF g_GameBlips[i].eCategory = STATIC_BLIP_CATEGORY_SAVEHOUSE
								SAVEHOUSE_BLIP_REMOVED(i)
							ENDIF
						ENDIF
						
						g_GameBlips[i].biBlip = NULL
					ENDIF
				ENDIF

				CLEAR_BIT(g_GameBlips[i].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
				
				INT doUpdate = 30
				IF IS_SCREEN_FADED_OUT()
					doUpdate = 250
				ENDIF
				IF Changed > doUpdate // increased to 33 for 1400251
					WAIT(0)
					Changed = 0
				ENDIF
				
				IF Updated / 20 > iUpdateLimiter
					iUpdateLimiter = Updated / 20
					CDEBUG2LN(debug_blip,"Delaying due to updates ", Updated, " limiter ",iUpdateLimiter)
					WAIT(0)
				ENDIF
			ENDREPEAT
		ENDIF

		// If there was an update mid update, then do another run through to make sure the list is settled
		#IF IS_DEBUG_BUILD
			IF g_bBlipChangeDuringUpdate
				CPRINTLN(DEBUG_BLIP,"Blip controller: change detected during update. Continuing to update blips")
			ENDIF
		#ENDIF
		
		g_bBlipSystemRefreshDetector = g_bBlipChangeDuringUpdate
		g_bBlipChangeDuringUpdate = FALSE
		
		IF Updated = 0
			DO_SAVEHOUSE_BLIP_PROXIMITY_CHECK()
			
			// Handle turning Chop's blip on and off.
			DO_CHOP_SAFEHOUSE_BLIP_CHECK()
			
			WAIT(500)
			
			#IF IS_DEBUG_BUILD
				WHILE g_flowUnsaved.bUpdatingGameflow
					CDEBUG1LN(DEBUG_BLIP, "blip_controller hibernating for 250 ms while g_flowUnsaved.bUpdatingGameflow = true")
					WAIT(250)
				ENDWHILE
			#ENDIF

			currchar = GET_CURRENT_PLAYER_PED_ENUM() // Make sure current character is still correct.
		ENDIF
		
	ENDWHILE
ENDSCRIPT
