USING "global_block_defines.sch"

GLOBALS GLOBALS_BLOCK_STANDARD TRUE

USING "rage_builtins.sch"
USING "globals.sch"
USING "mission_flow_initialiser.sch"
using "decorator_initialiser.sch"

// All the registration headers required to specify which global variables need to be saved
USING "flow_globals_reg.sch"
USING "player_ped_globals_reg.sch"
USING "charsheet_globals_reg.sch"
USING "respawn_location_globals_reg.sch"
USING "comms_control_globals_reg.sch"
USING "code_control_globals_reg.sch"
USING "building_globals_reg.sch"
USING "vehicle_gen_globals_reg.sch"
USING "flow_help_globals_reg.sch"
USING "CompletionPercentage_globals_reg.sch"
USING "Cellphone_Settings_globals_reg.sch"
USING "finance_globals_reg.sch"
USING "email_globals_reg.sch"
USING "snapshots_globals_reg.sch"
using "respawn_location_data.sch"
using "player_scene_globals_reg.sch"
using "flow_mission_data_gta5.sch"
using "flow_heist_data_gta5.sch"
using "stripclub_public.sch"
using "vector_id_data_gta5.sch"
using "blip_ambient.sch"
using "streamed_scripts.sch"


/// PURPOSE:
///    Gets the initial player spawn coords for the current level.
#IF IS_DEBUG_BUILD   
PROC SET_INITIAL_DEBUG_SPAWN_COORDS()
            
    //when adding new entries to this list remember to update iTOTAL_DEBUG_START_LOCATIONS in debug globals
    //with the new total
    
    //Michael's house.
    g_DebugStartPositionStrings[0] = "michaels house"
    g_vDebugStartPositions[0] = << -841.2872, 158.0778, 66.1773 >>
    g_vDebugStartOrientations[0] = 294.6890
    
    //Franklin's house.
    g_DebugStartPositionStrings[1] = "franklins house"
    g_vDebugStartPositions[1] = << -16.7612, -1453.1525, 30.5500 >>
    g_vDebugStartOrientations[1] = 131.3492
    
    //Trevor's shack.
    g_DebugStartPositionStrings[2] = "trevors shack"
    g_vDebugStartPositions[2] = << 1981.2532, 3815.8582, 31.3914 >> 
    g_vDebugStartOrientations[2] = 253.9870
    
    //The strip club planning location in south central.
    g_DebugStartPositionStrings[3] = "strip club planning location"
    g_vDebugStartPositions[3] = << 128.5380, -1307.6937, 28.1732 >>
    g_vDebugStartOrientations[3] = 118.0145
    
    //The sweatshop.
    g_DebugStartPositionStrings[4] = "sweatshop"
    g_vDebugStartPositions[4] = << 713.8944, -1086.5442, 21.3346 >>
    g_vDebugStartOrientations[4] = 118.0145
    
    //The garage.
    g_DebugStartPositionStrings[5] = "garage"
    g_vDebugStartPositions[5] = << 189.6962, -1255.6549, 28.3109 >>
    g_vDebugStartOrientations[5] = 331.5037
    
    //The car showroom.
    g_DebugStartPositionStrings[6] = "showroom"
    g_vDebugStartPositions[6] = << -59.7362, -1110.6184, 25.4353 >> 
    g_vDebugStartOrientations[6] = 120.2055
    
    //The FBI building.
    g_DebugStartPositionStrings[7] = "fbi building"
    g_vDebugStartPositions[7] = << 94.9453, -742.9583, 44.7549 >> 
    g_vDebugStartOrientations[7] = 120.2055
    
    //The rural bank.
    g_DebugStartPositionStrings[8] = "rural bank"
    g_vDebugStartPositions[8] = << -116.0866, 6456.1396, 30.4904 >>
    g_vDebugStartOrientations[8] = 206.6007
    
    //The original start location.
    g_DebugStartPositionStrings[9] = "original start location"
    g_vDebugStartPositions[9] = << -1234.70, -1135.56, 6.81 >>
    g_vDebugStartOrientations[9] = 135.0000
    
ENDPROC
#endif

SCRIPT
	
	// Contained within streamed_scripts.sch (replaces gta4's initial_objects.sch)
    // ChrisM - Moved this above Create_The_Player() due to an issue where the player
    // is placed at a script brain prior to it being registered with a script.
	
	#IF IS_DEBUG_BUILD
		SET_INITIAL_DEBUG_SPAWN_COORDS()
	#endif		
	
    Setup_Streamed_Scripts_Associated_With_Objects_Peds_WorldPoints_Etc() 
	CPRINTLN(DEBUG_INIT, "Clearing state of SP globals.")
		
	INTIIALISE_GTA5_DECORATORS()
	INITIALISE_STATIC_BLIP_SETTINGS()
    Initialise_GTA5_Mission_Data_On_Startup()
    
    
    Reset_All_Drunk_Variables_And_Terminate_All_Unneeded_Scripts()
    Reset_All_Replay_Variables()
    Initialise_Global_Interior_Instances() // added by Neil F. 30/4/2012 
	INITIALISE_TEAMS_DATA()
	Initialise_GTA5_Heist_Data_On_Startup()
	Store_All_Vector_ID_Data()
	
    // NOTE: This must come after Initialise_CharSheet_Global_Variables_On_StartupNRM() as it references global data.
    Register_Police_Respawn_Locations()
    Register_Hospital_Respawn_Locations()
    Clear_Savehouse_Respawn_Locations()
		
    CPRINTLN(DEBUG_INIT, "SP globals initialised")	

ENDSCRIPT




