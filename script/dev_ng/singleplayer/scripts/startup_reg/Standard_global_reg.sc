USING "global_block_defines.sch"

USING "rage_builtins.sch"
USING "globals.sch"
USING "mission_flow_initialiser.sch"

// All the registration headers required to specify which global variables need to be saved
USING "flow_globals_reg.sch"
USING "heist_globals_reg.sch"
USING "stripclub_globals_reg.sch"
USING "pilotschool_globals_reg.sch"
USING "shop_globals_reg.sch"
USING "ambient_globals_reg.sch"
USING "player_ped_globals_reg.sch"
USING "charsheet_globals_reg.sch"
USING "respawn_location_globals_reg.sch"
USING "family_globals_reg.sch"
USING "player_scene_globals_reg.sch"
USING "friends_globals_reg.sch"
USING "comms_control_globals_reg.sch"
USING "code_control_globals_reg.sch"
USING "building_globals_reg.sch"
USING "vehicle_gen_globals_reg.sch"
USING "flow_help_globals_reg.sch"
USING "CompletionPercentage_globals_reg.sch"
USING "Cellphone_Settings_globals_reg.sch"
USING "social_globals_reg.sch"
USING "finance_globals_reg.sch"
USING "random_char_globals_reg.sch"
USING "towing_globals_reg.sch"
USING "assassin_globals_reg.sch"
USING "basejump_globals_reg.sch"
USING "trafficking_globals_reg.sch"
USING "taxi_globals_reg.sch"
USING "offroad_globals_reg.sch"
USING "golf_globals_reg.sch"
USING "range_globals_reg.sch"
USING "tennis_globals_reg.sch"
USING "sptt_globals_reg.sch"
USING "Triathlon_globals_reg.sch"
USING "shoprobberies_globals_reg.sch"
USING "shrink_globals_reg.sch"
USING "specialPed_globals_reg.sch"
USING "email_globals_reg.sch"
USING "darts_globals_reg.sch"
USING "sea_race_globals_reg.sch"
USING "street_race_globals_reg.sch"
USING "rampage_globals_reg.sch"
USING "debug_channels_core.sch"
USING "feed_globals_reg.sch"
USING "snapshots_globals_reg.sch"
USING "BailBond_globals_reg.sch"
USING "properties_globals_reg.sch"
USING "random_event_globals_reg.sch"
USING "ScriptSaves_Procs.sch"
USING "flyUnderBridges.sch"
USING "savegame_patching.sch"
USING "screens_header.sch"
USING "streamed_Scripts.sch"
USING "stripclub_public.sch"
USING "properties_private.sch"
USING "country_race_globals_reg.sch"
USING "flow_heist_data_gta5.sch"
USING "director_mode_globals_reg.sch"


CONST_INT __ASSERT_IF_SAVEGAME_SIZES_DIFFER 1

#IF IS_DEBUG_BUILD

INT iRandomStartPosIndex = -1  
INT RunningTotalOfStructures = 0

PROC VerifySizeOfSavedStructure(INT SizeOfMostRecentStructure, STRING NameOfMostRecentStructure, BOOL bSP = TRUE)
    CPRINTLN(DEBUG_INIT, "SIZE_OF(", NameOfMostRecentStructure, ") = ", SizeOfMostRecentStructure)	
    RunningTotalOfStructures += SizeOfMostRecentStructure	
    INT SizeOfRegisteredVariables = GET_SIZE_OF_SAVE_DATA(bSP)
    
    IF RunningTotalOfStructures <> SizeOfRegisteredVariables
        CPRINTLN(DEBUG_INIT, "Size of registered save variables differs from actual size of structure")
        CPRINTLN(DEBUG_INIT, "Size of all structures so far = ", RunningTotalOfStructures)
        CPRINTLN(DEBUG_INIT, "SizeOfRegisteredVariables = ", SizeOfRegisteredVariables)
        CPRINTLN(DEBUG_INIT, "Most Recent Structure = ", NameOfMostRecentStructure)
		#IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
        	SCRIPT_ASSERT("Size of save struct differs from size of registered save variables - check TTY")
		#ENDIF
    ENDIF
ENDPROC

#ENDIF  //  IS_DEBUG_BUILD

/// PURPOSE:
///    Requests a script, waits for it to stream in, then launches it
PROC Request_And_Launch_Script_With_Wait(STRING strScriptName, INT iStackSize)
    REQUEST_SCRIPT(strScriptName)
    WHILE NOT HAS_SCRIPT_LOADED(strScriptName)
        WAIT(0)
        REQUEST_SCRIPT(strScriptName)
    ENDWHILE
    START_NEW_SCRIPT(strScriptName, iStackSize)
ENDPROC

SCRIPT
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	Clear_Mission_Flow_Saved_Globals()
	Initialise_Strip_Club_Global_Variables_On_Startup()
	Initialise_FriendSheet_Global_Variables_On_Startup()	
	Initialise_Properties_Global_Variables_On_Startup()  
	Initialise_PlayerSceneData_Global_Variables_On_Startup()
	Initialise_CompletionPercentage_Tracking_Before_Savegame_Restoration()
	Initialise_CharSheet_Global_Variables_On_Startup()
	Register_Savehouse_Respawn_Locations()	
		
	#IF IS_DEBUG_BUILD
		RunningTotalOfStructures = GET_SIZE_OF_SAVE_DATA(TRUE)
	#ENDIF
	
		
	START_SAVE_DATA(g_savedGlobals, SIZE_OF(g_savedGlobals), TRUE)
			REGISTER_FLOAT_TO_SAVE(g_savedGlobals.fSaveVersion , "fSaveVersion")
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.fSaveVersion), "g_savedGlobals.fSaveVersion")
	#ENDIF  //  IS_DEBUG_BUILD
		
	        Register_Flow_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFlow), "g_savedGlobals.sFlow")
	#ENDIF  //  IS_DEBUG_BUILD

			Register_Flow_Custom_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFlowCustom), "g_savedGlobals.sFlowCustom")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Heist_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sHeistData), "g_savedGlobals.sHeistData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_StripClub_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sStripClubData), "g_savedGlobals.sStripClubData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_PilotSchool_Saved_Globals()
	#IF IS_DEBUG_BUILD      
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFlightSchoolData), "g_savedGlobals.sFlightSchoolData")
	#ENDIF  //  IS_DEBUG_BUILD

		Register_Rampage_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRampageData), "g_savedGlobals.sRampageData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Shop_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sShopData), "g_savedGlobals.sShopData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Social_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sSocialData), "g_savedGlobals.sSocialData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_ambient_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sAmbient), "g_savedGlobals.sAmbient")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Player_Ped_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sPlayerData), "g_savedGlobals.sPlayerData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Respawn_Location_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRespawnData), "g_savedGlobals.sRespawnData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_CharSheet_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCharSheetData), "g_savedGlobals.sCharSheetData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Friends_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFriendsData), "g_savedGlobals.sFriendsData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Family_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFamilyData), "g_savedGlobals.sFamilyData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Player_Scene_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sPlayerSceneData), "g_savedGlobals.sPlayerSceneData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Random_Char_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRandomChars), "g_savedGlobals.sRandomChars")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Comms_Control_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCommsControlData), "g_savedGlobals.sCommsControlData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Code_Control_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCodeControlData), "g_savedGlobals.sCodeControlData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Building_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sBuildingData), "g_savedGlobals.sBuildingData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Vehicle_Gen_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sVehicleGenData), "g_savedGlobals.sVehicleGenData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_FlowHelp_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFlowHelp), "g_savedGlobals.sFlowHelp")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_CompletionPercentage_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCompletionPercentageData), "g_savedGlobals.sCompletionPercentageData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_CellphoneSettings_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCellphoneSettingsData) + SIZE_OF(g_savedGlobals.sTextMessageSavedData) + SIZE_OF(g_savedGlobals.sGalleryImageSavedData),
	         "g_savedGlobals.sCellphoneSettingsData + g_savedGlobals.sTextMessageSavedData + g_savedGlobals.sGalleryImageSavedData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Finance_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sFinanceData), "g_savedGlobals.sFinanceData")
	#ENDIF  //  IS_DEBUG_BUILD

	        // Minigame and Oddjob globals
	        Register_Towing_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sTowingData), "g_savedGlobals.sTowingData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Assassin_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sAssassinData), "g_savedGlobals.sAssassinData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Basejump_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sBasejumpData), "g_savedGlobals.sBasejumpData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Darts_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sDartsData), "g_savedGlobals.sDartsData")
	#ENDIF  //  IS_DEBUG_BUILD

	        
	        Register_Trafficking_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sTraffickingData), "g_savedGlobals.sTraffickingData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Taxi_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sTaxiData), "g_savedGlobals.sTaxiData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Shrink_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sShrinkData), "g_savedGlobals.sShrinkData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Offroad_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sOffroadData), "g_savedGlobals.sOffroadData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Golf_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sGolfData), "g_savedGlobals.sGolfData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_ShootingRange_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRangeData), "g_savedGlobals.sRangeData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Tennis_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sTennisData), "g_savedGlobals.sTennisData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_Triathlon_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sTriathlonData), "g_savedGlobals.sTriathlonData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_SPTT_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sSPTTData), "g_savedGlobals.sSPTTData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_ShopRobberies_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sShopRobberiesData), "g_savedGlobals.sShopRobberiesData")
	#ENDIF  //  IS_DEBUG_BUILD
	        
	        Register_SpecialPed_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sSpecialPedData), "g_savedGlobals.sSpecialPedData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Email_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sEmailData), "g_savedGlobals.sEmailData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Street_Race_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sStreetRaceData), "g_savedGlobals.sStreetRaceData")
	#ENDIF  //  IS_DEBUG_BUILD

	        Register_Snapshot_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRepeatPlayData), "g_savedGlobals.sRepeatPlayData")
	#ENDIF

	     	Register_SpBuyableVehicle_Saved_Globals()
	#IF IS_DEBUG_BUILD
			VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sBuyableVehicleSavedData), "g_savedGlobals.sBuyableVehicleSavedData")
	#ENDIF
		
	        Register_Bailbond_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sBailBondData), "g_savedGlobals.sBailBondData")
	#ENDIF

	        Register_Properties_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sPropertyData), "g_savedGlobals.sPropertyData")
	#ENDIF

	        Register_Random_Events_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sRandomEventData), "g_savedGlobals.sRandomEventData")
	#ENDIF

	        Register_Sea_Race_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sSeaRaceData), "g_savedGlobals.sSeaRaceData")
	#ENDIF  //  IS_DEBUG_BUILD
	
			Register_Country_Race_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sCountryRaceData), "g_savedGlobals.sCountryRaceData")
	#ENDIF  //  IS_DEBUG_BUILD
	
#IF FEATURE_SP_DLC_DIRECTOR_MODE
			Register_Director_Mode_Saved_Globals()
	#IF IS_DEBUG_BUILD
	        VerifySizeOfSavedStructure(SIZE_OF(g_savedGlobals.sDirectorModeData), "g_savedGlobals.sDirectorModeData")
	#ENDIF  //  IS_DEBUG_BUILD
#ENDIF
	
	STOP_SAVE_DATA()

	CPRINTLN(DEBUG_INIT, "Standard_global_reg.sc - about to set STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED")
	SET_BIT(iBitFieldOfRegisteredSaveData, STANDARD_GLOBAL_SAVE_DATA_HAS_BEEN_REGISTERED)
			
	    INT SizeOfStruct = SIZE_OF(g_savedGlobals)
	    INT SizeOfRegisteredVariables = GET_SIZE_OF_SAVE_DATA(TRUE)
	    CPRINTLN(DEBUG_INIT, "SizeOfStruct = ", SizeOfStruct)
	    CPRINTLN(DEBUG_INIT, "SizeOfRegisteredVariables = ", SizeOfRegisteredVariables)
	   
	    #IF __ASSERT_IF_SAVEGAME_SIZES_DIFFER
	        IF SizeOfStruct <> SizeOfRegisteredVariables
	            SCRIPT_ASSERT("Size of save struct differs from size of registered save variables - check TTY")
	        ENDIF
	    #ENDIF

	CPRINTLN(DEBUG_INIT, "Saved globals registered and restored.")
// -----------------------------------------------------------------------------------------------------------
// SAVE GLOBALS REGISTRATION and RELOADING - END
// -----------------------------------------------------------------------------------------------------------	
	
	/// url:bugstar:6144130 - need to copy saved contacts into overflow array
	/// so we can keep adding characters without blowing the save
	GLOBAL_CHARACTER_SHEET_COPY_SAVED_CONTACTS_TO_OVERFLOW_ARRAY()
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("Under the bridge stats initialising")PRINTNL()
		INT fKnifeStat
		STAT_GET_INT(SP_KNIFE_FLIGHTS_COUNT,fKnifeStat)
		PRINTSTRING("Current saved knife stat:")PRINTINT(fKnifeStat)PRINTNL()
		PRINTSTRING("Current saved knife script value:")PRINTINT(GET_NUMBER_OF_COMPLETED_KNIFE_FLIGHTS())PRINTNL()
		INT fBridgeStat
		STAT_GET_INT(SP_KNIFE_FLIGHTS_COUNT,fBridgeStat)
		PRINTSTRING("Current saved under the bridge stat:")PRINTINT(fBridgeStat)PRINTNL()
		PRINTSTRING("Current saved under the bridge script value:")PRINTINT(GET_NUMBER_OF_COMPLETED_KNIFE_FLIGHTS())PRINTNL()
	#ENDIF
	
	//Ensure underbridge and knife flight stats are in sync with script globals.
	STAT_SET_INT(SP_KNIFE_FLIGHTS_COUNT, GET_NUMBER_OF_COMPLETED_KNIFE_FLIGHTS()) 
    STAT_SET_INT(SP_UNDER_THE_BRIDGE_COUNT, GET_NUMBER_OF_COMPLETED_UNDER_BRIDGES())
	
	if IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		g_bRestoredSaveThisSession = true
	else
		g_bRestoredSaveThisSession = false
	endif
	
	//Do we need to jump flow strand pointers based on requested pointer overrides.
	APPLY_STRAND_POINTER_OVERRIDES()
	
	// Reset default clothes if the SE/CE content has been uninstalled.
	IF NOT IS_SPECIAL_EDITION_GAME()
	AND NOT IS_COLLECTORS_EDITION_GAME()
	AND NOT IS_JAPANESE_SPECIAL_EDITION_GAME()
		IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
		OR IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION)
			#IF IS_DEBUG_BUILD
				PRINTLN("DLC CHECK: Setting default outfit as dlc content checks have changed")
			#ENDIF
			
			SETUP_DEFAULT_PLAYER_VARIATIONS(CHAR_MICHAEL)
			SETUP_DEFAULT_PLAYER_VARIATIONS(CHAR_FRANKLIN)
			SETUP_DEFAULT_PLAYER_VARIATIONS(CHAR_TREVOR)
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			ENDIF
			// no longer clearing these bits as the player_controller script will pick it up and remove the clothes.
			//CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_SPECIAL_EDITION)
			//CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_COLLECTORS_EDITION)
		ENDIF
	ENDIF
	
	// Remove blimp from phone contacts list if DLC has been uninstalled
	IF NOT IS_PREORDER_GAME() 
    AND NOT IS_SPECIAL_EDITION_GAME() 
    AND NOT IS_COLLECTORS_EDITION_GAME()
	AND NOT IS_JAPANESE_SPECIAL_EDITION_GAME()
	AND NOT IS_LAST_GEN_PLAYER()
		IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, NCU_BLIMP)
			CPRINTLN(DEBUG_REPEAT, "Removing blimp contact from phone as DLC has been removed.")
			REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(CHAR_BLIMP)
			SET_SCENARIO_GROUP_ENABLED("BLIMP", FALSE)
			CLEAR_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_BLIMP_UNLOCK)
			CLEAR_BIT(g_savedGlobals.sShopData.iContentChecks_Game, NCU_BLIMP)
		ENDIF
	ENDIF
	
	// Disable stunt plane time trials if DLC has been uninstalled.
	IF NOT IS_SPECIAL_EDITION_GAME() 
    AND NOT IS_COLLECTORS_EDITION_GAME()
	AND NOT IS_JAPANESE_SPECIAL_EDITION_GAME()
		IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES))
			CPRINTLN(DEBUG_REPEAT, "Removing stunt plane time trials minigame as DLC has been removed.")
			SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES), FALSE)
			SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STUNT_PLANES,	FALSE)
		ENDIF
	ENDIF
	
	// Patch weapon tints into new array.
	IF NOT IS_BIT_SET(g_savedGlobals.sPlayerData.iDLCPatchBitset, 0)
		
		INT iCharIndex
		INT iWeapon
		INT iModBit
		INT iTint
		
		REPEAT 3 iCharIndex
			REPEAT NUM_PLAYER_PED_WEAPON_SLOTS iWeapon
				iTint = 0
				FOR iModBit = 20 TO 31
					IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeapon].iModsAsBitfield, iModBit)
						PRINTLN("PATCHING WEAPON TINT INDEX ", iTint, " FOR WEAPON ", iWeapon)
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeapon].iTint = iTint
						CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sWeaponInfo[iWeapon].iModsAsBitfield, iModBit)
					ENDIF
					iTint++
				ENDFOR
			ENDREPEAT
			
			REPEAT NUMBER_OF_DLC_WEAPONS iWeapon
				iTint = 0
				FOR iModBit = 20 TO 31
					IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iModsAsBitfield, iModBit)
						PRINTLN("PATCHING WEAPON TINT INDEX ", iTint, " FOR DLC WEAPON ", iWeapon)
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iTint = iTint
						CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iModsAsBitfield, iModBit)
					ENDIF
					iTint++
				ENDFOR
			ENDREPEAT
		ENDREPEAT
		
		SET_BIT(g_savedGlobals.sPlayerData.iDLCPatchBitset, 0)
	ENDIF
	
	PED_WEAPONS_STRUCT sTempPedWeapons
	scrShopWeaponData weaponData
	PATCH_DLC_WEAPON_LAYOUT(sTempPedWeapons, weaponData)
	
	// Player positioning logic.
	IF (g_bRestoredSaveThisSession
	OR NOT ARE_VECTORS_EQUAL(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos, <<0.0,0.0,0.0>>))
	#IF IS_DEBUG_BUILD
	AND g_eCachedLevel = LEVEL_GTA5
	#ENDIF

		IF g_bRunMultiplayerOnStartup
			//We are loading directly into MP from a save.
			//Restore player variations and weapons and unleash the blip controller now.
			//This is usually done by startup positioning which we don't need to run in this
			//case. -BenR
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
				RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_ARMOUR(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(PLAYER_PED_ID(), FALSE)
			ENDIF
			g_bBlipSystemStartupLeash = FALSE //unleash the blip system
		ELSE
			// A save has been loaded. Position the player with startup_positioning routines.
			CPRINTLN(DEBUG_INIT, "Loading into level GTA5. Running startup positioning.")
			Request_And_Launch_Script_With_Wait("startup_positioning", SHOP_STACK_SIZE)
		ENDIF

	ELSE
	
		// Don't load the scene in Release mode on a new game start.
		// Speeds up loading because the player will be re-positioned
		// by the first mission script anyway.
		#IF IS_DEBUG_BUILD

			IF g_eCachedLevel != LEVEL_GTA5 
			OR (NOT HAVE_PLAYER_COORDS_BEEN_OVERRIDDEN()
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("debugLocationstart")
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("debugstart")
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("DoReleaseStartup")
			AND NOT g_bRunMultiplayerOnStartup)
			
				// New game debug start into SP. Pick a random debug location, move the player there,
				// and stream it in as effciently as possible.
				CPRINTLN(DEBUG_INIT, "Starting into SP with debug scripts. Setting up player in a debug spawn location.")
				
		        IF NOT (g_eCachedLevel = LEVEL_NET_TEST)
		        AND NOT (g_eCachedLevel = LEVEL_TESTBED)
		        AND NOT (g_eCachedLevel = LEVEL_ANIM_TEST)
					iRandomStartPosIndex = GET_RANDOM_INT_IN_RANGE(0,(iTOTAL_DEBUG_START_LOCATIONS-1))
		            IF (iRandomStartPosIndex > -1)PRINTNL()
		                CPRINTLN(DEBUG_INIT,    "============================================================")
		                CPRINTLN(DEBUG_INIT,    "PLAYER START LOCATION: ", 
		                                        g_DebugStartPositionStrings[iRandomStartPosIndex], 
		                                        " (", 
		                                        g_vDebugStartPositions[iRandomStartPosIndex],
		                                        ")")
		                CPRINTLN(DEBUG_INIT,    "============================================================")
		            ENDIF
					
		        ENDIF
				 
				// Unpause so we can move the camera.
				SET_GAME_PAUSED(FALSE)
				
				VECTOR vSpawnCoords 
				IF g_eCachedLevel = LEVEL_NET_TEST
					vSpawnCoords = <<-54.6355, -62.9232, 59.0000>>		
			    ELIF g_eCachedLevel = LEVEL_TESTBED
					vSpawnCoords = << -1.38, 75.30, 7.6>>
			    ELIF g_eCachedLevel = LEVEL_ANIM_TEST
					vSpawnCoords = << -2.38, -1.04, -2.0>>
			    ENDIF	
				
				//Set random position if not in gameflow mode
			    IF g_savedGlobals.sFlow.isGameflowActive = FALSE
				and iRandomStartPosIndex != -1
			         vSpawnCoords = g_vDebugStartPositions[iRandomStartPosIndex]         
			    ENDIF

				// Move the player and the camera.
				CLEAR_AREA(vSpawnCoords, 5.0, TRUE)
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vSpawnCoords)
						if iRandomStartPosIndex != -1
							SET_ENTITY_HEADING(PLAYER_PED_ID(), g_vDebugStartOrientations[iRandomStartPosIndex])
						ELSE
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)	
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF

				// Wait a frame for the camera to catch up before pausing.
				WAIT(0) 
				SET_GAME_PAUSED(TRUE)

				//Net Testbed does it's own load scene from \multiplayer\scripts\modes\MPTestbed.sc
				IF g_eCachedLevel != LEVEL_NET_TEST
					// Load the stream vol
					NEW_LOAD_SCENE_START_SPHERE(vSpawnCoords, 600)
				
					INT iNewLoadSceneTimeout = 0
				
					CPRINTLN(DEBUG_INIT, "<startup> wait for new load scene.")
					WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
					AND (iNewLoadSceneTimeout < 350)
						iNewLoadSceneTimeout++
					
						WAIT(0)
					ENDWHILE		
					CPRINTLN(DEBUG_INIT, "<startup> new load scene loaded [", iNewLoadSceneTimeout, "].")
				
					// Get rid of the new load scene
					NEW_LOAD_SCENE_STOP()
				ENDIF

				CLEAR_AREA(vSpawnCoords, 5.0, TRUE)
				
				VECTOR vDebugStartPositions = vSpawnCoords
				FLOAT vStartPos_ground_z
				IF GET_GROUND_Z_FOR_3D_COORD(vDebugStartPositions, vStartPos_ground_z)
					vDebugStartPositions.z = vStartPos_ground_z
				ENDIF
				
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vDebugStartPositions)
						if iRandomStartPosIndex != -1
							SET_ENTITY_HEADING(PLAYER_PED_ID(), g_vDebugStartOrientations[iRandomStartPosIndex])
						ELSE
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)	
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF

				// Unpause when all is done
				SET_GAME_PAUSED(FALSE)
				
				//Ignore auto MP boot checks for test levels.
				IF g_eCachedLevel = LEVEL_GTA5
					RUN_SECOND_MULTIPLAYER_ON_STARTUP_CHECK()

					// Fade the screen in at the end of the debug positioning.
					// DO NOT MOVE THIS FADE IN back outwith the check. Speak to me if you really need this. It causes a crash. - BRENDA
					IF NOT g_bRunMultiplayerOnStartup
						SHUTDOWN_LOADING_SCREEN()
						IF NOT IS_SCREEN_FADING_IN()
						AND NOT IS_SCREEN_FADED_IN()
							CPRINTLN(DEBUG_INIT, "Fading in for debug build startup.")
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF

		#ENDIF		
	ENDIF
	
	// We always want to start the game in gameflow mode now.	
	g_savedGlobals.sFlow.isGameflowActive = TRUE
	
	// New game startup configuration.
    IF NOT g_bRestoredSaveThisSession
	
		//If the player hasn't already chosen to boot into MP check one last time before
		//allowing SP to start.
		RUN_SECOND_MULTIPLAYER_ON_STARTUP_CHECK()

	    #IF IS_FINAL_BUILD
			//Release script running release startup.
			IF g_bRunMultiplayerOnStartup
			OR g_bMagDemoActive
	            CLEAR_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			ELSE
				SET_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			ENDIF
	    #ENDIF

	    #IF IS_DEBUG_BUILD
			//Debug script simulating Release startup from command line.
	      	IF GET_COMMANDLINE_PARAM_EXISTS("DoReleaseStartup")
				CPRINTLN(DEBUG_INIT, "Simulating a release startup with command line \"-DoReleaseStartup\".")
				IF g_bRunMultiplayerOnStartup
				OR g_bMagDemoActive
					CPRINTLN(DEBUG_INIT, "Simulated release startup. Prologue was delayed due to MP or magdemo override.")
		            CLEAR_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
				ELSE
					CPRINTLN(DEBUG_INIT, "Simulated release startup. Prologue was given the OK to run.")
					SET_BIT(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
				ENDIF
				
			//Debug script running debug startup.
	        ELSE
				
				//DO NOT MOVE THIS FADE IN back outwith the check. Speak to me if you really need this. It causes a crash. - BRENDA
				CPRINTLN(DEBUG_INIT, "Running debug startup for new game. Prologue not being triggered.")

				IF NOT g_bRunMultiplayerOnStartup
					SHUTDOWN_LOADING_SCREEN()
					IF NOT IS_SCREEN_FADED_IN()
	        			IF NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
	    #ENDIF
	ENDIF

    // Startup complete so set the flag to let us know that we shouldn't fade in again.
    g_savedGlobals.sRespawnData.bNewGameStarted = TRUE
		
	// Remove all mission doors from the system to prevent it hitting max registered door limit - bug# 1578613
	REMOVE_ALL_MISSION_SPECIFIC_DOORS_FROM_SYSTEM_ON_STARTUP()
    
    // Initialise Stunt Jumps relies on save data being loaded to avoid re-adding
    Initialise_Stunt_Jumps()    			
	Initialise_Savehouse_Respawn_Locations()
    Initialise_Police_Respawn_Locations()
    Initialise_Hospital_Respawn_Locations()	
		
    // Set the stored building states.
    INITIALISE_STORED_BUILDING_STATES_ON_STARTUP()

    // Store the initial player ped index.
    STORE_TEMP_PLAYER_PED_ID(PLAYER_PED_ID())
	
	// DLC Blimp configuartion (B*1409618).
	#IF IS_DEBUG_BUILD
    IF g_eCachedLevel != LEVEL_NET_TEST
    #ENDIF
	
		IF IS_BIT_SET(g_savedGlobals.sShopData.iContentChecks_Game, 3)
		    IF NOT IS_SCENARIO_GROUP_ENABLED("BLIMP")
				CPRINTLN(DEBUG_INIT, "startup.sc - preorder, CE or SE game - enable Scenario Group \"BLIMP\".")
		    	SET_SCENARIO_GROUP_ENABLED("BLIMP", TRUE)
			ENDIF
		ELSE
		    IF IS_SCENARIO_GROUP_ENABLED("BLIMP")
				CPRINTLN(DEBUG_INIT, "startup.sc - not preorder, CE or SE game - disable Scenario Group \"BLIMP\".")
		    	SET_SCENARIO_GROUP_ENABLED("BLIMP", FALSE)
			ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	RecalculateResultantPercentageTotal()
	
	CPRINTLN(DEBUG_INIT, "Give tunables_registration one frame to run before requesting and launching main_persistent.")	
	WAIT(0)	

	IF NOT ( IS_COMMANDLINE_END_USER_BENCHMARK() OR LANDING_SCREEN_STARTED_END_USER_BENCHMARK())
		Request_And_Launch_Script_With_Wait("main_persistent", DEFAULT_STACK_SIZE)    	
		ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)  // NeilF: Important! Only enable world point scripts AFTER all the global blocks have been registered. 

	
#IF IS_DEBUG_OR_PROFILE_BUILD
		// MAGDEMO STARTUP
		// For the magdemo, GET_NAME_OF_SCRIPT_TO_AUTOMATICALLY_START() was temporarily available in Release scripts
		STRING script_to_launch = GET_NAME_OF_SCRIPT_TO_AUTOMATICALLY_START()
		IF NOT ARE_STRINGS_EQUAL(script_to_launch, "NULL")
			IF g_bMagDemoActive = TRUE		
				IF ARE_STRINGS_EQUAL(script_to_launch, "magdemo2a")
				OR ARE_STRINGS_EQUAL(script_to_launch, "magdemo2b")
					script_to_launch = "magdemo2"
				ENDIF		
				Request_And_Launch_Script_With_Wait(script_to_launch, SHOP_STACK_SIZE)
			ELSE
				Request_And_Launch_Script_With_Wait(script_to_launch, DEFAULT_STACK_SIZE)
			ENDIF
		ENDIF
#ENDIF // IS_DEBUG_OR_PROFILE_BUILD

#IF IS_DEBUG_BUILD
		//Command line startup modifiers.
		IF GET_COMMANDLINE_PARAM_EXISTS("noambient")
			CPRINTLN(DEBUG_INIT, "WARNING! Ambient script blocking was enabled from the command line.")
			g_bDebugBlockAmbientScripts = TRUE
		ENDIF
		IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
			CPRINTLN(DEBUG_INIT, "WARNING! Flow phonecall silencing was enabled from the command line.")
			g_bDebugFlowSilencePhonecalls = TRUE
		ENDIF	
#ENDIF // IS_DEBUG_BUILD

	ELSE
		CPRINTLN(DEBUG_INIT, "Skipping main_persistent launch due to benchmark being run from command line.")
	ENDIF

ENDSCRIPT