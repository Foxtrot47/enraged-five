
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"

#IF IS_DEBUG_BUILD

USING "friends_debug.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	friends_debug_controller.sc
//		AUTHOR			:	Sam H
//		DESCRIPTION		:	Friends debug widgets, enabled by script_launch_control widget.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************



// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

//-- Rag ---
WIDGET_GROUP_ID hWidget
WIDGET_GROUP_ID hSubWidget_FLoc, hSubWidget_ALoc, hSubWidget_AScene, hSubWidget_Luke
//WIDGET_GROUP_ID hSubWidget_dropoff

BOOL		rag_bFLocEdit
BOOL		rag_bALocEdit

//INT			rag_iDropoffLoc
//BOOL		rag_bDropoffEdit
//BOOL		rag_bDropoffSave = TRUE

INT			rag_iASceneLoc
BOOL		rag_bASceneEdit
BOOL		rag_bASceneSave = TRUE

INT			rag_iLukeLoc
BOOL		rag_bLukeEdit

BOOL		rag_bTrackFriendPlayerInstances
BOOL		rag_bTestAdhocFriendLoc
BOOL		rag_bExportAllFLOCDropoffs
BOOL		rag_bExportAllALOCDropoffs
BOOL		rag_bPrintInvolvedFriends
BOOL		rag_bPrintMissionsCloseToLocations

BOOL		rag_bKnockoutRunScene[3]
FLOAT		rag_fKnockoutRagdollTime[3]

VECTOR		rag_vKnockoutBoxOffsetA[3]
VECTOR		rag_vKnockoutBoxOffsetB[3]
VECTOR		rag_vKnockoutBoxOffsetC[3]

VECTOR		rag_vKnockoutBoxSizeA[3]
VECTOR		rag_vKnockoutBoxSizeB[3]
VECTOR		rag_vKnockoutBoxSizeC[3]

INT			rag_iForcePickupLoc = ENUM_TO_INT(NO_FRIEND_LOCATION)

//BOOL		bCheckForMissingFriendDialogue
//BOOL		bTestFriendPhonecalls, bTestFriendContexts, bTestFriendFacetofaces, bTestFriendTextMessages
//BOOL		bAddFriendsForPlayerChar//, bTestAllLamarDialogue
//


//-- General ---
//BOOL						bDrawDebugDrunkInfo
structPedsForConversation	convPeds
BLIP_INDEX					hTrackFriendPlayerBlips[3]


// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC FriendsDebugController_Cleanup()
	
	IF DOES_WIDGET_GROUP_EXIST(hWidget)
		DELETE_WIDGET_GROUP(hWidget)
	ENDIF

	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC FriendsDebugController_CreateWidgets()

//	SET_CURRENT_WIDGET_GROUP(friends_controller_widget)
	hWidget = START_WIDGET_GROUP("Friends Debug Controller")
	
		START_WIDGET_GROUP("General")
			ADD_WIDGET_BOOL("Track player instances",			rag_bTrackFriendPlayerInstances)
			ADD_WIDGET_BOOL("Test adhoc FriendLocation",		rag_bTestAdhocFriendLoc)
			ADD_WIDGET_BOOL("bExportAllFLOCDropoffs",			rag_bExportAllFLOCDropoffs)
			ADD_WIDGET_BOOL("bExportAllALOCDropoffs",			rag_bExportAllALOCDropoffs)
			ADD_WIDGET_BOOL("bPrintInvolvedFriends",			rag_bPrintInvolvedFriends)
			ADD_WIDGET_BOOL("bPrintMissionsCloseToLocations",	rag_bPrintMissionsCloseToLocations)

			ADD_WIDGET_BOOL("Supress ambient players",			g_bSupressAmbientPlayersForFriendActivity)
			START_NEW_WIDGET_COMBO()
				enumFriendLocation eLoopFLoc
				REPEAT NO_FRIEND_LOCATION eLoopFLoc
					ADD_TO_WIDGET_COMBO(GetLabel_enumFriendLocation(eLoopFLoc))
				ENDREPEAT
			STOP_WIDGET_COMBO("Force pickup loc", rag_iForcePickupLoc)
			
			START_WIDGET_GROUP("Knockout scenes")
				enumCharacterList eCharLoop
				REPEAT NUM_OF_PLAYABLE_PEDS eCharLoop
					START_WIDGET_GROUP(GetLabel_enumFriend(GET_FRIEND_FROM_CHAR(eCharLoop)))
						ADD_WIDGET_BOOL("Test scene",						rag_bKnockoutRunScene[eCharLoop])
						ADD_WIDGET_FLOAT_SLIDER("Ragdoll time",				rag_fKnockoutRagdollTime[eCharLoop], 0.0, 1.0, 0.1)
						ADD_WIDGET_STRING(".")
						ADD_WIDGET_VECTOR_SLIDER("BoxOffsetA",				rag_vKnockoutBoxOffsetA[eCharLoop], 	-20.0, 20.0, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("BoxSizeA",				rag_vKnockoutBoxSizeA[eCharLoop], 		0.0, 10.0, 0.1)
						ADD_WIDGET_STRING(".")
						ADD_WIDGET_VECTOR_SLIDER("BoxOffsetB",				rag_vKnockoutBoxOffsetB[eCharLoop], 	-20.0, 20.0, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("BoxSizeB",				rag_vKnockoutBoxSizeB[eCharLoop], 		0.0, 10.0, 0.1)
						ADD_WIDGET_STRING(".")
						ADD_WIDGET_VECTOR_SLIDER("BoxOffsetC",				rag_vKnockoutBoxOffsetC[eCharLoop], 	-20.0, 20.0, 0.1)
						ADD_WIDGET_VECTOR_SLIDER("BoxSizeC",				rag_vKnockoutBoxSizeC[eCharLoop], 		0.0, 10.0, 0.1)
					STOP_WIDGET_GROUP()
				ENDREPEAT
			STOP_WIDGET_GROUP()

//			ADD_WIDGET_BOOL("bCheckForMissingFriendDialogue", bCheckForMissingFriendDialogue)
//			ADD_WIDGET_BOOL("bTestFriendPhonecalls", bTestFriendPhonecalls)
//			ADD_WIDGET_BOOL("bTestFriendContexts", bTestFriendContexts)
//			ADD_WIDGET_BOOL("bTestFriendFacetofaces", bTestFriendFacetofaces)
//			ADD_WIDGET_BOOL("bTestFriendTextMessages", bTestFriendTextMessages)
//			ADD_WIDGET_BOOL("bAddFriendsForPlayerChar", bAddFriendsForPlayerChar)
//			ADD_WIDGET_BOOL("g_bForceFriendActivityWithAnyone", g_bForceFriendActivityWithAnyone)
		STOP_WIDGET_GROUP()

		hSubWidget_FLoc =
		START_WIDGET_GROUP("Friend locations")
			ADD_WIDGET_BOOL("Edit", rag_bFLocEdit)
		STOP_WIDGET_GROUP()
		
		hSubWidget_ALoc =
		START_WIDGET_GROUP("Activity locations")
			ADD_WIDGET_BOOL("Edit", rag_bALocEdit)
		STOP_WIDGET_GROUP()

//		hSubWidget_Dropoff =
//		START_WIDGET_GROUP("Dropoff Scenes")
//			START_NEW_WIDGET_COMBO()
//				ADD_TO_WIDGET_COMBO("Select a dropoff scene to edit...")
//				enumFriendLocation eFriendLoc
//				REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
//					IF eFriendLoc <> FLOC_adhoc
//						ADD_TO_WIDGET_COMBO(GetLabel_enumFriendLocation(eFriendLoc))
//					ENDIF
//				ENDREPEAT
//				enumActivityLocation eActivityLoc
//				REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
//					ADD_TO_WIDGET_COMBO(GetLabel_enumActivityLocation(eActivityLoc))
//				ENDREPEAT
//			STOP_WIDGET_COMBO("Scene to edit", rag_iDropoffLoc)
//			ADD_WIDGET_BOOL("Edit", rag_bDropoffEdit)
//			ADD_WIDGET_BOOL("Save on exit", rag_bDropoffSave)
//		STOP_WIDGET_GROUP()

		hSubWidget_AScene =
		START_WIDGET_GROUP("Activity Scenes")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Select an activity scene to edit...")
				enumActivityLocation eActivityLoc
				REPEAT MAX_ACTIVITY_LOCATIONS eActivityLoc
					ADD_TO_WIDGET_COMBO(GetLabel_enumActivityLocation(eActivityLoc))
				ENDREPEAT
			STOP_WIDGET_COMBO("Scene to edit", rag_iASceneLoc)
			ADD_WIDGET_BOOL("Edit", rag_bASceneEdit)
			ADD_WIDGET_BOOL("Save on exit", rag_bASceneSave)
		STOP_WIDGET_GROUP()

		hSubWidget_Luke =
		START_WIDGET_GROUP("Luke Dropoff Cam Editor")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Select a dropoff scene to edit...")
				enumFriendLocation eFriendLoc
				REPEAT MAX_FRIEND_LOCATIONS eFriendLoc
					IF eFriendLoc <> FLOC_adhoc
						ADD_TO_WIDGET_COMBO(GetLabel_enumFriendLocation(eFriendLoc))
					ENDIF
				ENDREPEAT
				ADD_TO_WIDGET_COMBO(GetLabel_enumActivityLocation(ALOC_cinema_downtown))
				ADD_TO_WIDGET_COMBO(GetLabel_enumActivityLocation(ALOC_cinema_morningwood))
				ADD_TO_WIDGET_COMBO(GetLabel_enumActivityLocation(ALOC_cinema_vinewood))
			STOP_WIDGET_COMBO("Scene to edit", rag_iLukeLoc)
			ADD_WIDGET_BOOL("Edit", rag_bLukeEdit)
		STOP_WIDGET_GROUP()

	STOP_WIDGET_GROUP()
//	CLEAR_CURRENT_WIDGET_GROUP(friends_controller_widget)

	// Init data
	rag_fKnockoutRagdollTime[CHAR_MICHAEL]	= 0.287
	rag_fKnockoutRagdollTime[CHAR_FRANKLIN]	= 0.255
	rag_fKnockoutRagdollTime[CHAR_TREVOR]	= 0.4
	
	// Michael
	Private_GetAmbFriendOutroAreas(CHAR_MICHAEL, rag_vKnockoutBoxOffsetA[CHAR_MICHAEL], rag_vKnockoutBoxSizeA[CHAR_MICHAEL],
												rag_vKnockoutBoxOffsetB[CHAR_MICHAEL], rag_vKnockoutBoxSizeB[CHAR_MICHAEL],
												rag_vKnockoutBoxOffsetC[CHAR_MICHAEL], rag_vKnockoutBoxSizeC[CHAR_MICHAEL])

	Private_GetAmbFriendOutroAreas(CHAR_FRANKLIN, rag_vKnockoutBoxOffsetA[CHAR_FRANKLIN], rag_vKnockoutBoxSizeA[CHAR_FRANKLIN],
												rag_vKnockoutBoxOffsetB[CHAR_FRANKLIN], rag_vKnockoutBoxSizeB[CHAR_FRANKLIN],
												rag_vKnockoutBoxOffsetC[CHAR_FRANKLIN], rag_vKnockoutBoxSizeC[CHAR_FRANKLIN])

	Private_GetAmbFriendOutroAreas(CHAR_TREVOR, rag_vKnockoutBoxOffsetA[CHAR_TREVOR], rag_vKnockoutBoxSizeA[CHAR_TREVOR],
												rag_vKnockoutBoxOffsetB[CHAR_TREVOR], rag_vKnockoutBoxSizeB[CHAR_TREVOR],
												rag_vKnockoutBoxOffsetC[CHAR_TREVOR], rag_vKnockoutBoxSizeC[CHAR_TREVOR])

ENDPROC


PROC FriendsDebugController_UpdateWidgets()

	//-- Editors

	// Display friend locations
	IF rag_bFLocEdit
		PRIVATE_DEBUG_DisplayFriendLocations(hSubWidget_FLoc, rag_bFLocEdit)				
	ENDIF

	// Display activity locations
	IF rag_bALocEdit
		PRIVATE_DEBUG_DisplayActivityLocations(hSubWidget_ALoc, rag_bALocEdit)
	ENDIF
	
	
	g_ForceFriendLocation = INT_TO_ENUM(enumFriendLocation, rag_iForcePickupLoc)
	
	
	// Edit dropoff scenes
//	IF rag_bDropoffEdit
//		TEXT_LABEL_63			tLocName
//		VECTOR					vLocAnchor
//		structFDropoffScene		dropoffData
//		
//		//rag_bDropoffSave = TRUE
//		
//		IF rag_iDropoffLoc = 0
//			rag_bDropoffEdit = FALSE
//			
//		ELIF rag_iDropoffLoc < ENUM_TO_INT(MAX_FRIEND_LOCATIONS)						// Edit FriendLoc dropoff scene
//			enumFriendLocation eFriendLoc = INT_TO_ENUM(enumFriendLocation, rag_iDropoffLoc)
//			
//			tLocName	= GetLabel_enumFriendLocation(eFriendLoc)
//			vLocAnchor	= g_FriendLocations[eFriendLoc].vPickupCoord
//			Private_FLOC_GetDropoffScene(eFriendLoc, dropoffData)
//			
//			PRIVATE_DEBUG_EditDropoffScene(hSubWidget_dropoff, convPeds, rag_bDropoffEdit, dropoffData, vLocAnchor)
//				
//			IF rag_bDropoffSave
//				PRIVATE_DEBUG_SaveDropoffScene(tLocName, dropoffData, TRUE)
//			ENDIF
//		
//		ELSE																			// Edit ActivityLoc dropoff scene
//			enumActivityLocation eActivityLoc = INT_TO_ENUM(enumActivityLocation, rag_iDropoffLoc - ENUM_TO_INT(MAX_FRIEND_LOCATIONS))
//			
//			tLocName	= GetLabel_enumActivityLocation(eActivityLoc)
//			vLocAnchor	= GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
//			Private_ALOC_GetDropoffScene(eActivityLoc, dropoffData)
//			
//			PRIVATE_DEBUG_EditDropoffScene(hSubWidget_dropoff, convPeds, rag_bDropoffEdit, dropoffData, vLocAnchor)
//				
//			IF rag_bDropoffSave
//				PRIVATE_DEBUG_SaveDropoffScene(tLocName, dropoffData, FALSE)
//			ENDIF
//
//		ENDIF
//	ENDIF
	
	// Edit activity scenes
	IF rag_bASceneEdit
		TEXT_LABEL_63			tLocName
		VECTOR					vLocAnchor
		structFActivityScene	sceneData
		
		enumActivityLocation eActivityLoc = INT_TO_ENUM(enumActivityLocation, rag_iASceneLoc - 1)
		
		IF eActivityLoc < MAX_ACTIVITY_LOCATIONS
			tLocName	= GetLabel_enumActivityLocation(eActivityLoc)
			vLocAnchor	= GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
			Private_ALOC_GetActivityScene(eActivityLoc, sceneData)
			
			PRIVATE_DEBUG_EditActivityScene(hSubWidget_AScene, convPeds, rag_bASceneEdit, sceneData, vLocAnchor, eActivityLoc)
				
			IF rag_bASceneSave
				PRIVATE_DEBUG_SaveActivityScene(tLocName, sceneData)
			ENDIF
		ENDIF
	ENDIF

	// Edit luke scenes
	IF rag_bLukeEdit
		TEXT_LABEL_63			tLocName
		VECTOR					vLocAnchor
		structFDropoffScene		dropoffData
		
		IF rag_iLukeLoc = 0
			rag_bLukeEdit = FALSE
			
		ELIF rag_iLukeLoc < ENUM_TO_INT(MAX_FRIEND_LOCATIONS)						// Edit FriendLoc luke scene
			enumFriendLocation eFriendLoc = INT_TO_ENUM(enumFriendLocation, rag_iLukeLoc)
			
			tLocName	= GetLabel_enumFriendLocation(eFriendLoc)
			vLocAnchor	= g_FriendLocations[eFriendLoc].vPickupCoord
			Private_FLOC_GetDropoffScene(eFriendLoc, dropoffData)
			
			PRIVATE_DEBUG_EditDropoffScene_Luke(hSubWidget_Luke, convPeds, rag_bLukeEdit, dropoffData, vLocAnchor)
			PRIVATE_DEBUG_SaveDropoffScene(tLocName, dropoffData, TRUE)
		
		ELSE																			// Edit ActivityLoc luke scene
			enumActivityLocation eActivityLoc = INT_TO_ENUM(enumActivityLocation, rag_iLukeLoc - ENUM_TO_INT(MAX_FRIEND_LOCATIONS)) + ALOC_cinema_downtown
			
			tLocName	= GetLabel_enumActivityLocation(eActivityLoc)
			vLocAnchor	= GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
			Private_ALOC_GetDropoffScene(eActivityLoc, dropoffData)
			
			PRIVATE_DEBUG_EditDropoffScene_Luke(hSubWidget_Luke, convPeds, rag_bLukeEdit, dropoffData, vLocAnchor)
			PRIVATE_DEBUG_SaveDropoffScene(tLocName, dropoffData, FALSE)

		ENDIF
	ENDIF


	//-- General

	PRIVATE_DEBUG_TrackFriendPlayerInstances(rag_bTrackFriendPlayerInstances, hTrackFriendPlayerBlips)

	IF rag_bTestAdhocFriendLoc
		PRIVATE_DEBUG_TestAdhocFriendLocation(rag_bTestAdhocFriendLoc)
	ENDIF
	
	enumCharacterList eCharLoop
	REPEAT NUM_OF_PLAYABLE_PEDS eCharLoop
		IF rag_bKnockoutRunScene[eCharLoop]
			PRIVATE_DEBUG_TestKnockoutScene(eCharLoop, rag_fKnockoutRagdollTime[eCharLoop],
											rag_vKnockoutBoxOffsetA[eCharLoop], rag_vKnockoutBoxSizeA[eCharLoop],
											rag_vKnockoutBoxOffsetB[eCharLoop], rag_vKnockoutBoxSizeB[eCharLoop],
											rag_vKnockoutBoxOffsetC[eCharLoop], rag_vKnockoutBoxSizeC[eCharLoop])
			rag_bKnockoutRunScene[eCharLoop] = FALSE
		ENDIF
	ENDREPEAT
	
	IF rag_bExportAllFLOCDropoffs
		PRIVATE_DEBUG_ExportAllFLOCDropoffs()
		rag_bExportAllFLOCDropoffs = FALSE
	ENDIF

	IF rag_bExportAllALOCDropoffs
		PRIVATE_DEBUG_ExportAllALOCDropoffs()
		rag_bExportAllALOCDropoffs = FALSE
	ENDIF

	IF rag_bPrintInvolvedFriends
		PRIVATE_DEBUG_PrintInvolvedFriendsList()
		rag_bPrintInvolvedFriends = FALSE
	ENDIF

	IF rag_bPrintMissionsCloseToLocations
		PRIVATE_DEBUG_PrintMissionsCloseToLocations()
		rag_bPrintMissionsCloseToLocations = FALSE
	ENDIF



	//-- Obsolete

//	IF bCheckForMissingFriendDialogue
//		PRIVATE_DEBUG_CheckForMissingFriendDialogue()
//		bCheckForMissingFriendDialogue = FALSE
//	ENDIF
//	
//	IF bTestFriendPhonecalls
//		PRIVATE_DEBUG_TestFriendPhoneDialogue(friends_controller_widget, bTestFriendPhonecalls)
//	ENDIF
//
//	IF bTestFriendContexts
//		PRIVATE_DEBUG_TestFriendActivityDialogue(friends_controller_widget, bTestFriendContexts)
//	ENDIF
//
//	IF bTestFriendTextMessages
//		PRIVATE_DEBUG_TestFriendTextMessages(friends_controller_widget, bTestFriendTextMessages)
//	ENDIF
			
ENDPROC

#ENDIF


SCRIPT
	#IF IS_DEBUG_BUILD
		PRINTLN("Starting friends_debug_controller.sc")
		
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		
		// This script needs to cleanup only when the game runs the magdemo
		IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
			PRINTLN("...friends_debug_controller.sc has been forced to cleanup (MAGDEMO)")
			FriendsDebugController_Cleanup()
		ENDIF
		
		// Additional debug items
		FriendsDebugController_CreateWidgets()

		WHILE (TRUE)
			WAIT(0)
			
			FriendsDebugController_UpdateWidgets()
				
			IF NOT g_bDebug_KeepFriendsDebugControllerRunning
				PRINTLN("...friends_debug_controller.sc has been forced to cleanup (not running)")
				FriendsDebugController_Cleanup()
			ENDIF
		ENDWHILE		
	#ENDIF
	
	TERMINATE_THIS_THREAD()

ENDSCRIPT

