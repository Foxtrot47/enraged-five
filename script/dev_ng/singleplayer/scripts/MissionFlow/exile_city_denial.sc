
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "flow_public_core.sch"
USING "friendActivity_public.sch"

INT iCandidateID = NO_CANDIDATE_ID
 
BOOL bInAreaThisFrame = FALSE
BOOL bInAreaLastFrame = FALSE
BOOL bCallbackSetup = FALSE
BOOL bCityMode = TRUE

INCIDENT_INDEX theIncident

//bit set for warning calls from martin 
CONST_INT EXILE_WARNING_M_1 0 
CONST_INT EXILE_WARNING_M_2 1 
CONST_INT EXILE_WARNING_M_3 2 
CONST_INT EXILE_WARNING_T_1 3 
CONST_INT EXILE_WARNING_T_2 4 
CONST_INT EXILE_WARNING_T_3 5 


PROC ONSTART_HOSPITAL_DISABLED()
	CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Disabling exile zone hospital spawns.")
	
	Set_Hospital_Respawn_Available(HOSPITAL_RH, FALSE)
	Set_Hospital_Respawn_Available(HOSPITAL_DT, FALSE)
	Set_Hospital_Respawn_Available(HOSPITAL_SC, FALSE)

	Set_Police_Respawn_Available(POLICE_STATION_VB, FALSE)
	Set_Police_Respawn_Available(POLICE_STATION_SC, FALSE)
	Set_Police_Respawn_Available(POLICE_STATION_RH, FALSE)
	Set_Police_Respawn_Available(POLICE_STATION_DT, FALSE)
ENDPROC


PROC ONEND_HOSPITAL_ENABLED()
	CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Enabling exile zone hospital spawns.")
	
	Set_Hospital_Respawn_Available(HOSPITAL_RH, TRUE)
	Set_Hospital_Respawn_Available(HOSPITAL_DT, TRUE)
	Set_Hospital_Respawn_Available(HOSPITAL_SC, TRUE)

	Set_Police_Respawn_Available(POLICE_STATION_VB, TRUE)
	Set_Police_Respawn_Available(POLICE_STATION_SC, TRUE)
	Set_Police_Respawn_Available(POLICE_STATION_RH, TRUE)
	Set_Police_Respawn_Available(POLICE_STATION_DT, TRUE)
ENDPROC


PROC CLEANUP_EXILE_CITY_DENIAL()
	CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Cleaning up exile city denial.")
	
	IF iCandidateID != NO_CANDIDATE_ID
		CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Releasing candidate ID.")
		Mission_Over(iCandidateID)
	ENDIF
	
	CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN1)
	CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN2)
	CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN3)
	CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN1)
	CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN2)
	CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN3)
	
	bInAreaThisFrame = FALSE
	bInAreaLastFrame = FALSE
	g_bExileMidTriggering = FALSE
	
	DELETE_INCIDENT(theIncident)
	CDEBUG1LN(DEBUG_FLOW, "<CITY-EXILE> Cleanup called DELETE_INCIDENT().")
ENDPROC


PROC TERMINATE_EXILE_CITY_DENIAL()
	CLEANUP_EXILE_CITY_DENIAL()
	
	CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Exile city denial terminated.")
	ONEND_HOSPITAL_ENABLED()
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT 
	CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> City denial script launched.")
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("exile_city_denial")) > 1
		SCRIPT_ASSERT("Exile city denial script is attempting to launch while an instance is already active. Killing this instance.")
		g_bExileMidTriggering = FALSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	REGISTER_SCRIPT_TO_RELAUNCH_LIST(LAUNCH_BIT_EXILE_CITY_DENIAL)
	ONSTART_HOSPITAL_DISABLED()
	
	SETTIMERA(0)
	SETTIMERB(0)
	
	INT iResistanceLevel = 2
	INT iHelpCooldown = 0
	
	WHILE NOT bCallbackSetup
		CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Setting up a new FORCE_CLEANUP callback.")
		bCallbackSetup = TRUE
		
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED|FORCE_CLEANUP_FLAG_REPEAT_PLAY)	
			g_bExileMidTriggering = FALSE

			IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
				CPRINTLN(DEBUG_FLOW,"<CITY-EXILE> Exile script exiting as FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED is FALSE.")
				REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_EXILE_CITY_DENIAL)
				TERMINATE_EXILE_CITY_DENIAL()
			ELIF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
				CPRINTLN(DEBUG_FLOW,"<CITY-EXILE> Exile script exiting as FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED is TRUE.")
				REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_EXILE_CITY_DENIAL)
				TERMINATE_EXILE_CITY_DENIAL()
			ENDIF
		
			SWITCH GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP()
				CASE FORCE_CLEANUP_FLAG_DEBUG_MENU
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> A force cleanup occured of type 'debug menu'. Terminating.")
					REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_EXILE_CITY_DENIAL)
					TERMINATE_EXILE_CITY_DENIAL()
				BREAK
				CASE FORCE_CLEANUP_FLAG_SP_TO_MP
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> A force cleanup occured of type 'SP->MP'. Terminating.")
					TERMINATE_EXILE_CITY_DENIAL()
				BREAK
				
				CASE FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED
					CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN1)
					CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN2)
					CANCEL_COMMUNICATION(CALL_EXILE_M_MARTIN3)
					CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN1)
					CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN2)
					CANCEL_COMMUNICATION(CALL_EXILE_T_MARTIN3)
					
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Noted the player was killed or arrested.")
					bInAreaThisFrame = FALSE
					
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Waiting till player respawns...")
					WHILE NOT IS_PLAYER_PLAYING(PLAYER_ID())
						WAIT(1000)
					ENDWHILE
					bCallbackSetup = FALSE
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> ...player respawned.")
					
					//NB. We purposefully don't terminate on this force cleanup.
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Falling through and setting up new FORCE_CLEANUP this frame.")
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDWHILE
	
	
	//[MF] Fix for B* 1707857
	
	
	WHILE TRUE
		IF iHelpCooldown > 0
			iHelpCooldown -= TIMERA()
			SETTIMERA(0)
			IF iHelpCooldown < 0
				iHelpCooldown = 0
			ENDIF
		ENDIF
		
		VECTOR v = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		bInAreaThisFrame = CITY_EXILE_AREA_CHECK(v, bCityMode)

		//Don't check exile status until flow knows for sure what the status is
		IF (g_flowUnsaved.bUpdatingGameflow 
		OR g_flowUnsaved.bFlowControllerBusy) 
			bInAreaThisFrame = FALSE
		ENDIF
		
		BOOL flowBail = FALSE
		BOOL charBail = FALSE
	
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
			flowBail = TRUE
		ENDIF
		IF (NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED))
			CDEBUG3LN(DEBUG_FLOW,"<CITY-EXILE> script mid run without FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED being true. Bailing out")
			flowBail = TRUE
		ENDIF
		
		IF flowBail
			CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> The Mike/Trevor exile ended in flow. Terminating.")
			g_bExileMidTriggering = FALSE
			REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_EXILE_CITY_DENIAL)
			TERMINATE_EXILE_CITY_DENIAL()
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			charBail = TRUE
		ENDIF
		
		//set the flow global that the candidate system uses
		g_bExileMidTriggering = bInAreaThisFrame
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			 	IF IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					CDEBUG3LN(DEBUG_FLOW, "<CITY-EXILE> City exile blocked this frame as the player is flying an aircraft.")
					bInAreaThisFrame = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF (bInAreaLastFrame != bInAreaThisFrame) 
		
			IF NOT bInAreaThisFrame
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Player left denial zone. Cleaning up.")
				CLEANUP_EXILE_CITY_DENIAL()
			ENDIF
		
			IF bInAreaThisFrame AND NOT charBail
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Michael or Trevor entered the exile denial zone.")
				
				//try to initialise a MISSION_TYPE_EXILE
				BOOL gotInit = FALSE
				BOOL seekingCandidate = TRUE
				
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Exile denial script attempting to go on mission.")
				WHILE seekingCandidate// = MCRET_PROCESSING
					SWITCH Request_Mission_Launch(iCandidateID, MCTID_CONTACT_POINT, MISSION_TYPE_EXILE)
						CASE MCRET_DENIED
							CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Exile denial script was denied from going on mission.")
							seekingCandidate = FALSE
							gotInit = FALSE
							BREAK
						CASE MCRET_ACCEPTED
							CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Exile denial script sucessfully went on mission.")
							seekingCandidate = FALSE
							gotInit = TRUE
							BREAK
						DEFAULT 
							WAIT(0)
							BREAK
					ENDSWITCH
						
					IF g_bInMultiplayer
						CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Exile bailed out due to MP during request")
						TERMINATE_THIS_THREAD()
					ENDIF
				ENDWHILE
				
				//Entering the area initialise the pursuit.
				IF gotInit
					
					//If the player has warped into an interior hold the pursuit
					IS_PED_INJURED(PLAYER_PED_ID()) //We want to be able to run this check while dead.
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						INTERIOR_INSTANCE_INDEX indindx = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						WHILE IS_VALID_INTERIOR(indindx)
							CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Delaying exile response for 5 seconds while the player is indoors.")
							WAIT(5000)
							IS_PED_INJURED(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								indindx = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
							ENDIF
						ENDWHILE
					ENDIF
					
					// Warning calls from Martin for Michael and Trevor.
					IF GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
						IF NOT IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_M_1 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_M_MARTIN1,CT_ON_MISSION,BIT_MICHAEL,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_M_WARN,FLOW_CHECK_NONE, COMM_FLAG_CALL_REQUEUE_ON_MISS)
												
						ELIF NOT IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_M_2 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_M_MARTIN2,CT_ON_MISSION,BIT_MICHAEL,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_M_WARN,FLOW_CHECK_NONE, COMM_FLAG_CALL_REQUEUE_ON_MISS)
												
						ELIF NOT IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_M_3 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_M_MARTIN3,CT_ON_MISSION,BIT_MICHAEL,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_M_WARN,FLOW_CHECK_NONE, COMM_FLAG_CALL_REQUEUE_ON_MISS)					
						ENDIF
					ELIF GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
						IF not IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_T_1 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_T_MARTIN1,CT_ON_MISSION,BIT_TREVOR,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_T_WARN,FLOW_CHECK_NONE,COMM_FLAG_CALL_REQUEUE_ON_MISS)
												
						ELIF not IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_T_2 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_T_MARTIN2,CT_ON_MISSION,BIT_TREVOR,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_T_WARN,FLOW_CHECK_NONE, COMM_FLAG_CALL_REQUEUE_ON_MISS)
												
						ELIF not IS_BIT_SET(g_savedGlobals.sCommsControlData.iExileWarningBitset,EXILE_WARNING_T_2 )
						
							REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CALL_EXILE_T_MARTIN3,CT_ON_MISSION,BIT_TREVOR,CHAR_MARTIN,
							3,10000,20000,COMM_NONE,VID_BLANK,CID_EXILE_INCREASE_T_WARN,FLOW_CHECK_NONE, COMM_FLAG_CALL_REQUEUE_ON_MISS)
						ENDIF	
					ENDIF
										
					IF iHelpCooldown = 0 AND (NOT charBail)
						IF (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY))
						AND (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR))
						AND (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS))
						
							IF GET_NUMBER_RESOURCES_ALLOCATED_TO_WANTED_LEVEL( DT_GANGS ) < 256
								CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Creating incident with resistance level ", iResistanceLevel, ".")
								
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										CREATE_INCIDENT_WITH_ENTITY(DT_GANGS, PLAYER_PED_ID(), iResistanceLevel, 0.0, theIncident)
									ENDIF
								ENDIF
							ENDIF

							#IF IS_DEBUG_BUILD //to fix 1267647
								WAIT(10000)
								IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
							#ENDIF
							
							
							IF IS_SCREEN_FADED_IN()
								IF bCityMode
									PRINT_HELP("CITDENAL")//710952
								ELSE
									PRINT_HELP("CITDENAL_R")//1170122
								ENDIF
								iHelpCooldown = 120000
							ENDIF
							
							#IF IS_DEBUG_BUILD
								ENDIF
							#ENDIF
							
							
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Failed to go on mission. Pretending we aren't in area this frame.")
					bInAreaThisFrame = FALSE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Player left denial zone. Cleaning up.")
				CLEANUP_EXILE_CITY_DENIAL()
			ENDIF
		 
		ENDIF
		
		//new check for event continuing to exist
		IF bInAreaThisFrame
			IF NOT IS_INCIDENT_VALID(theIncident)
				IF GET_NUMBER_RESOURCES_ALLOCATED_TO_WANTED_LEVEL( DT_GANGS ) < 256
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Creating incident with resistance level ", iResistanceLevel, ".")
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CREATE_INCIDENT_WITH_ENTITY(DT_GANGS, PLAYER_PED_ID(), iResistanceLevel, 0.0, theIncident)
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
		
		//check if resistance should be scaled up
		IF bInAreaThisFrame AND (TIMERB() > 60000)
			SETTIMERB(0)
			iResistanceLevel += GET_RANDOM_INT_IN_RANGE(1,2)
			IF iResistanceLevel < 8
				IF IS_INCIDENT_VALID(theIncident)
					SET_INCIDENT_REQUESTED_UNITS(theIncident,DT_GANGS,iResistanceLevel)
					CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Resistance increased to ", iResistanceLevel, ".")
				ELSE
					IF GET_NUMBER_RESOURCES_ALLOCATED_TO_WANTED_LEVEL( DT_GANGS ) < 256
						CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> Creating incident with resistance level ", iResistanceLevel, ".")
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CREATE_INCIDENT_WITH_ENTITY(DT_GANGS, PLAYER_PED_ID(), iResistanceLevel, 0.0, theIncident)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				iResistanceLevel = 8
			ENDIF
		ENDIF

		bInAreaLastFrame = bInAreaThisFrame
		
		IF charBail
			g_bExileMidTriggering = FALSE
			IF bInAreaThisFrame
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> The player switched to Franklin. Cleaning up.")
				CLEANUP_EXILE_CITY_DENIAL()
			ENDIF
			ONEND_HOSPITAL_ENABLED()
			WHILE GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				WAIT(2000)
			ENDWHILE
			ONSTART_HOSPITAL_DISABLED()
			charBail = FALSE
		ENDIF
		
		IF bInAreaThisFrame
			WAIT(0)
		ELSE
			WAIT(10000)
		ENDIF
		
		IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_EXILE)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_EXILE)
			IF bInAreaThisFrame
				CPRINTLN(DEBUG_FLOW, "<CITY-EXILE> A mission started. Cleaning up.")
				CLEANUP_EXILE_CITY_DENIAL()
			ENDIF
			WHILE IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
				WAIT(8000)
			ENDWHILE
		ENDIF

	ENDWHILE
	ONEND_HOSPITAL_ENABLED()
ENDSCRIPT
