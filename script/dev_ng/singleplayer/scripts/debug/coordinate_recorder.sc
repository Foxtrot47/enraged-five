

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// +---------------------------------------------------------------------------------+
// +--------------------------¦	   Speech queue System  ¦----------------------------+    
// +--------------------------¦	   	by BObby Wright 	¦----------------------------+    
// +--------------------------¦	   						¦----------------------------+    
// +--------------------------¦	    Private Functions	¦----------------------------+    
// +--------------------------¦	   						¦----------------------------+    
// +---------------------------------------------------------------------------------+
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_ped.sch"
USING "commands_hud.sch"
USING "commands_script.sch"
USING "dialogue_public.sch"
USING "script_maths.sch"
#IF IS_DEBUG_BUILD

	BOOL bStartRecordingCoordinates
	BOOL bDoTExt
	INT iGETPlayerCoordinateTime = 5000
	INT iPlayerCoordinateTimer
	INT iTextTimer
	BOOL bSetUp
	VECTOR vLastCoords
	TEXT_LABEL_31 tlMissionNameOLD
	
	STRING file = "coordinate_recorder.xml"
	STRING path = "X:/gta5/build/dev/"
	//Makes the text queue widgets
	PROC SET_UP_WIDGETS()
		START_WIDGET_GROUP("coordinate_recorder")
			ADD_WIDGET_BOOL("Start Recording Coordinates", bStartRecordingCoordinates)
			ADD_WIDGET_INT_SLIDER("Time to get coordinates", iPlayerCoordinateTimer, 0, 30000, 1000)
			ADD_WIDGET_VECTOR_SLIDER("Players Last Coordinates", vLastCoords, -4000.0, 4000.0, 0.001)
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC temp_SAVE_VECTOR_TO_NAMED_DEBUG_FILE(VECTOR vVector_to_save,STRING pathPassed, STRING filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("<coord x=\"",pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.x,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" y=\"",pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.y,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" z=\"",pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.z,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\"\\>",pathPassed, filePassed)
	ENDPROC
#ENDIF

// +--------------------------¦	 	Control the Speech Queue	¦----------------------------+   
SCRIPT 
	//So it can be used in a multiplayer script
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() 
	
	#IF IS_DEBUG_BUILD
		SET_UP_WIDGETS()
	#ENDIF
		
	WHILE TRUE
		WAIT(0)
		#IF IS_DEBUG_BUILD
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_R, KEYBOARD_MODIFIER_CTRL, "Coordinate Recorder")
				iTextTimer = GET_GAME_TIMER() + iGETPlayerCoordinateTime
				bDoTExt = TRUE
				IF bStartRecordingCoordinates = TRUE
					bStartRecordingCoordinates = FALSE
				ELSE
					bStartRecordingCoordinates = TRUE
				ENDIF
			ENDIF
		
			IF bDoTExt
				IF GET_GAME_TIMER() < iTextTimer
					TEXT_LABEL_63 tlRrecordStatus
					SET_TEXT_SCALE(0.4600, 0.4600)
					SET_TEXT_CENTRE(FALSE)
					SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
					SET_TEXT_EDGE(0, 0, 0, 0, 255)
					SET_TEXT_COLOUR(255, 0, 0, 255)
					IF bStartRecordingCoordinates = FALSE
						tlRrecordStatus = "Stopping Recording"
						SET_TEXT_COLOUR(255, 0, 0, 255)
					ELSE
						tlRrecordStatus = "Starting Recording"
						SET_TEXT_COLOUR(0, 255, 0, 255)
					ENDIF
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING",tlRrecordStatus)
				ELSE
					bDoTExt = FALSE
				ENDIF 
			ENDIF 
			
			IF bStartRecordingCoordinates
				IF GET_GAME_TIMER() >iPlayerCoordinateTimer
					iPlayerCoordinateTimer = GET_GAME_TIMER() + iGETPlayerCoordinateTime
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT ARE_STRINGS_EQUAL(tlMissionNameOLD, g_txtFlowAutoplayRunningMission)
							IF bSetUp = TRUE
								SAVE_STRING_TO_NAMED_DEBUG_FILE("<\\",path, file)
								SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionNameOLD,path, file)
								SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path, file)
								tlMissionNameOLD = g_txtFlowAutoplayRunningMission			
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
							ENDIF
							bSetUp = TRUE
							SAVE_STRING_TO_NAMED_DEBUG_FILE("<",path, file)
							SAVE_STRING_TO_NAMED_DEBUG_FILE(g_txtFlowAutoplayRunningMission, path, file)
							SAVE_STRING_TO_NAMED_DEBUG_FILE(">",path, file)
							SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
						ENDIF
						vLastCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())	
						temp_SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vLastCoords,path, file)
						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT


