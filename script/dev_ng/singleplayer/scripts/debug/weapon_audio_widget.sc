

#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Fraser Morgan				Date: 01/07/11				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Weapon Audio Tuning								│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


// HEADERS  
USING "rage_builtins.sch"
USING "globals.sch" 
USING "brains.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_physics.sch"
USING "commands_misc.sch"
USING "flow_public_core_override.sch"
USING "weapon_enums.sch"

// VARIABLES
WEAPON_TYPE WeaponOne = WEAPONTYPE_ASSAULTRIFLE
WEAPON_TYPE WeaponTwo = WEAPONTYPE_ASSAULTRIFLE



VECTOR vPedOne, vPedControl = <<0, 0, 0>>
VECTOR vFront, vSide, vUp, vPos, vTransformedOnePos, vTransformedTwoPos
VECTOR vPedFemalePos = <<0, 0, 0>>

BOOL bPedTwoIsFiring = FALSE
BOOL bCreateControlPed = FALSE
BOOL bDeleteControl = FALSE
BOOL bCreatePedOne = FALSE
BOOL bDeletePedOne = FALSE
BOOL bAlternateGunFire = FALSE
BOOL bControlPedFiring = FALSE

FLOAT fPedOnePos = 40
FLOAT fDeltaPos = 1
FLOAT fControlPos = 1
FLOAT fPedMaleMinPos = 1
FLOAT fPedFemalePos = 1
FLOAT fDistPlayer = 0

INT iweaponTypeControl = 0
INT iweaponTypeTwo = 0
INT iGunFireTime = 1000
INT iBetweenFire = 0
INT iNumberSteps = 1
	
// INDEXES

// MALES
PED_INDEX Ped_One, Ped_Control





//	#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID createWeaponAudioWidgets
//	#ENDIF

// cleanup the mission and widget
PROC MISSION_CLEANUP()
//	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(createWeaponAudioWidgets)
			DELETE_WIDGET_GROUP(createWeaponAudioWidgets)
		ENDIF
//    #ENDIF
	IF DOES_ENTITY_EXIST(Ped_One)
		OR DOES_ENTITY_EXIST(Ped_Control)
		DELETE_PED(Ped_One)
		DELETE_PED(Ped_Control)
	ENDIF
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

// Alternates fire between the two peds.
PROC ALTERNATE_GUN_FIRE()
	
		IF DOES_ENTITY_EXIST(Ped_One)
			AND DOES_ENTITY_EXIST(Ped_Control)
			SET_PED_ACCURACY(Ped_One, 100)
			SET_PED_ACCURACY(Ped_Control, 100)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Ped_Control, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Ped_One, TRUE)
		ENDIF
		
		IF iNumberSteps = 1
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
		ENDIF
		
		IF iNumberSteps = 2
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 3
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (SQRT(fPedOnePos/fPedMaleMinPos)))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (SQRT(fPedOnePos/fPedMaleMinPos)))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 4
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			
			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 5
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.25))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.25))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.75))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.75))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 6
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.20))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.20))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.40))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.40))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.60))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.60))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.80))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.80))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 7
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.16))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.16))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.83))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.83))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 8
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.14))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.14))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.29))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.29))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.43))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.43))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.57))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.57))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.71))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.71))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.86))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.86))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		IF iNumberSteps = 9
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.13))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.13))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.25))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.25))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.38))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.38))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.50))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.63))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.63))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.75))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.75))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.88))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.88))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF

		IF iNumberSteps = 10
			vPedOne = vTransformedOnePos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedMaleMinPos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.11))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.11))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.22))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.22))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.33))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.44))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.44))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.55))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.55))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.66))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.77))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.77))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.88))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * (fPedMaleMinPos * (POW((fPedOnePos/fPedMaleMinPos), (0.88))))
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)
			
			vPedOne = vTransformedOnePos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_One, vPedOne, TRUE)
			vPedOne = vTransformedTwoPos + vFront * fPedOnePos
			SET_ENTITY_COORDS_NO_OFFSET(Ped_Control, vPedOne, TRUE)
			fDistPlayer = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), Ped_Control)
			TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_One)
			WAIT(iBetweenFire)
			

			TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
			WAIT(iGunFireTime)
			CLEAR_PED_TASKS_IMMEDIATELY(Ped_Control)
			WAIT(iBetweenFire)	
		ENDIF
		
		
ENDPROC

PROC CREATE_WEAPON_WIDGETS()

//	#IF IS_DEBUG_BUILD
		createWeaponAudioWidgets = START_WIDGET_GROUP("Weapon Audio Tuning")
			START_WIDGET_GROUP("Ped Creation")
				START_WIDGET_GROUP("Female")
					ADD_WIDGET_FLOAT_SLIDER("Female Distance:", fPedFemalePos, 0, 150, 1)
					ADD_WIDGET_BOOL("Create Female", bCreatePedOne)
					ADD_WIDGET_BOOL("Delete Female", bDeletePedOne)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Male")
					ADD_WIDGET_FLOAT_SLIDER("Male Distance:", fControlPos, 0, 150, 1)
					ADD_WIDGET_BOOL("Create Male", bCreateControlPed)
					ADD_WIDGET_BOOL("Delete Male", bDeleteControl)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
				
				
				START_WIDGET_GROUP("Weapon Selection")
					START_NEW_WIDGET_COMBO()
						ADD_TO_WIDGET_COMBO("PISTOL")
						ADD_TO_WIDGET_COMBO("PISTOL_COMBAT")
						ADD_TO_WIDGET_COMBO("PISTOL_AP")
						ADD_TO_WIDGET_COMBO("SMG")
						ADD_TO_WIDGET_COMBO("SMG_MICRO")
						ADD_TO_WIDGET_COMBO("RIFLE_ASSAULT")
						ADD_TO_WIDGET_COMBO("RIFLE_CARBINE")
						ADD_TO_WIDGET_COMBO("RIFLE_ADV")
						ADD_TO_WIDGET_COMBO("LMG_MG")
						ADD_TO_WIDGET_COMBO("LMG_COMBAT")
						ADD_TO_WIDGET_COMBO("PUMP")
						ADD_TO_WIDGET_COMBO("SHOTGUN_SAWNOF")
						ADD_TO_WIDGET_COMBO("SHOTGUN_ASSAULT")
						ADD_TO_WIDGET_COMBO("SNIPER")
						ADD_TO_WIDGET_COMBO("SNIPER_REMOTE")
						ADD_TO_WIDGET_COMBO("SNIPER_RIFLE")
						ADD_TO_WIDGET_COMBO("GRENADE_LAUNCHER")
						ADD_TO_WIDGET_COMBO("RPG")
						ADD_TO_WIDGET_COMBO("MINIGUN")
						ADD_TO_WIDGET_COMBO("THROWN_GRENADE")
						ADD_TO_WIDGET_COMBO("THROWN_SMOKE_GRENADE")
						ADD_TO_WIDGET_COMBO("THROWN_STICKY_BOMB")
						ADD_TO_WIDGET_COMBO("STUNGUN")
						ADD_TO_WIDGET_COMBO("RUBBERGUN")
						ADD_TO_WIDGET_COMBO("PROG")
						ADD_TO_WIDGET_COMBO("FIRE_EXTINGUISHER")
						ADD_TO_WIDGET_COMBO("PETROL_CAN")
						//ADD_TO_WIDGET_COMBO("LOUDHAILER")
					STOP_WIDGET_COMBO("Female Weapon:", iweaponTypeTwo)
						ADD_WIDGET_BOOL("Make Female Fire", bPedTwoIsFiring)
							
							START_NEW_WIDGET_COMBO()
								ADD_TO_WIDGET_COMBO("PISTOL")
								ADD_TO_WIDGET_COMBO("PISTOL_COMBAT")
								ADD_TO_WIDGET_COMBO("PISTOL_AP")
								ADD_TO_WIDGET_COMBO("SMG")
								ADD_TO_WIDGET_COMBO("SMG_MICRO")
								ADD_TO_WIDGET_COMBO("RIFLE_ASSAULT")
								ADD_TO_WIDGET_COMBO("RIFLE_CARBINE")
								ADD_TO_WIDGET_COMBO("RIFLE_ADV")
								ADD_TO_WIDGET_COMBO("LMG_MG")
								ADD_TO_WIDGET_COMBO("LMG_COMBAT")
								ADD_TO_WIDGET_COMBO("LMG_ASSAULT")
								ADD_TO_WIDGET_COMBO("PUMP")
								ADD_TO_WIDGET_COMBO("SHOTGUN_SAWNOF")
								ADD_TO_WIDGET_COMBO("SHOTGUN_ASSAULT")
								ADD_TO_WIDGET_COMBO("SNIPER")
								ADD_TO_WIDGET_COMBO("SNIPER_REMOTE")
								ADD_TO_WIDGET_COMBO("SNIPER_RIFLE")
								ADD_TO_WIDGET_COMBO("GRENADE_LAUNCHER")
								ADD_TO_WIDGET_COMBO("RPG")
								ADD_TO_WIDGET_COMBO("MINIGUN")
								ADD_TO_WIDGET_COMBO("THROWN_GRENADE")
								ADD_TO_WIDGET_COMBO("THROWN_SMOKE_GRENADE")
								ADD_TO_WIDGET_COMBO("THROWN_STICKY_BOMB")
								ADD_TO_WIDGET_COMBO("STUNGUN")
								ADD_TO_WIDGET_COMBO("RUBBERGUN")
								ADD_TO_WIDGET_COMBO("PROG")
								ADD_TO_WIDGET_COMBO("FIRE_EXTINGUISHER")
								ADD_TO_WIDGET_COMBO("PETROL_CAN")
								//ADD_TO_WIDGET_COMBO("LOUDHAILER")
							STOP_WIDGET_COMBO("Make Male Weapon:", iweaponTypeControl)
						ADD_WIDGET_BOOL("Make Male Fire", bControlPedFiring)
				STOP_WIDGET_GROUP()
						
						
						START_WIDGET_GROUP("Alternating Fire")
							ADD_WIDGET_INT_SLIDER("Number Of Steps", iNumberSteps, 1, 10, 1)
							ADD_WIDGET_INT_SLIDER("Length of Gunfire(ms):", iGunFireTime, 0, 20000, 500)
							ADD_WIDGET_INT_SLIDER("Time Between Shots(ms):", iBetweenFire, 0, 10000, 500)
							ADD_WIDGET_BOOL("Alternate gunfire", bAlternateGunFire)
							ADD_WIDGET_FLOAT_SLIDER("Set Ped Max Dist", fPedOnePos, 1, 150, 1)
							ADD_WIDGET_FLOAT_SLIDER("Set Ped Min Dist", fPedMaleMinPos, 1, 150, 1)
							ADD_WIDGET_FLOAT_READ_ONLY("Current Dist from Player:", fDistPlayer)
						STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
//	#ENDIF
ENDPROC

// WEAPON TYPES ONE
PROC WEAPON_UPDATER_CONTROL()
	SWITCH iweaponTypeControl
		CASE 0
			WeaponOne = WEAPONTYPE_PISTOL
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 1
			WeaponOne = WEAPONTYPE_COMBATPISTOL
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 2
			WeaponOne = WEAPONTYPE_APPISTOL
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 3
			WeaponOne = WEAPONTYPE_SMG
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 4
			WeaponOne = WEAPONTYPE_MICROSMG
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 5
		    WeaponOne = WEAPONTYPE_ASSAULTRIFLE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 6
	    	WeaponOne = WEAPONTYPE_CARBINERIFLE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 7
			WeaponOne = WEAPONTYPE_ADVANCEDRIFLE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 8
			WeaponOne = WEAPONTYPE_MG
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 9
			WeaponOne = WEAPONTYPE_COMBATMG
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 10
			WeaponOne = WEAPONTYPE_PUMPSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 11
	 		WeaponOne = WEAPONTYPE_SAWNOFFSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 12
	    	WeaponOne = WEAPONTYPE_ASSAULTSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 13
			WeaponOne = WEAPONTYPE_HEAVYSNIPER
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 14
	    	WeaponOne = WEAPONTYPE_REMOTESNIPER
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 15
			WeaponOne = WEAPONTYPE_SNIPERRIFLE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 16
			WeaponOne = WEAPONTYPE_GRENADELAUNCHER
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 17
			WeaponOne = WEAPONTYPE_RPG
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 18
			WeaponOne = WEAPONTYPE_MINIGUN 
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 19
			WeaponOne = WEAPONTYPE_GRENADE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 20
			WeaponOne = WEAPONTYPE_SMOKEGRENADE
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 21
			WeaponOne = WEAPONTYPE_STICKYBOMB
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
	    BREAK
		CASE 22
			WeaponOne = WEAPONTYPE_STUNGUN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 24
			WeaponOne = WEAPONTYPE_PETROLCAN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
		CASE 25
			WeaponOne = WEAPONTYPE_PETROLCAN
			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
		BREAK
//		CASE 26
//			WeaponOne = WEAPONTYPE_DLC_LOUDHAILER
//			GIVE_WEAPON_TO_PED(Ped_Control, WeaponOne, 5000, TRUE)
//		BREAK
	ENDSWITCH
ENDPROC


// WEAPON TYPES TWO
PROC WEAPON_UPDATER_PEDS()
	SWITCH iweaponTypeTwo
		CASE 0
			WeaponTwo = WEAPONTYPE_PISTOL
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 1
			WeaponTwo = WEAPONTYPE_COMBATPISTOL
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 2
			WeaponTwo = WEAPONTYPE_APPISTOL
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 3
			WeaponTwo = WEAPONTYPE_SMG
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 4
			WeaponTwo = WEAPONTYPE_MICROSMG
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 5
		    WeaponTwo = WEAPONTYPE_ASSAULTRIFLE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 6
	    	WeaponTwo = WEAPONTYPE_CARBINERIFLE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 7
			WeaponTwo = WEAPONTYPE_ADVANCEDRIFLE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 8
			WeaponTwo = WEAPONTYPE_MG
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 9
			WeaponTwo = WEAPONTYPE_COMBATMG
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 10
			WeaponTwo = WEAPONTYPE_PUMPSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 11
	 		WeaponTwo = WEAPONTYPE_SAWNOFFSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 12
	    	WeaponTwo = WEAPONTYPE_ASSAULTSHOTGUN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 13
			WeaponTwo = WEAPONTYPE_HEAVYSNIPER
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 14
	    	WeaponTwo = WEAPONTYPE_REMOTESNIPER
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 15
			WeaponTwo = WEAPONTYPE_SNIPERRIFLE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 16
			WeaponTwo = WEAPONTYPE_GRENADELAUNCHER
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 17
			WeaponTwo = WEAPONTYPE_RPG
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 18
			WeaponTwo = WEAPONTYPE_MINIGUN 
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 19
			WeaponTwo = WEAPONTYPE_GRENADE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 20
			WeaponTwo = WEAPONTYPE_SMOKEGRENADE
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 21
			WeaponTwo = WEAPONTYPE_STICKYBOMB
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
	    BREAK
		CASE 22
			WeaponTwo = WEAPONTYPE_STUNGUN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 24
			WeaponTwo = WEAPONTYPE_PETROLCAN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
		CASE 25
			WeaponTwo = WEAPONTYPE_PETROLCAN
			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
		BREAK
//		CASE 26
//			WeaponTwo = WEAPONTYPE_DLC_LOUDHAILER
//			GIVE_WEAPON_TO_PED(Ped_One, WeaponTwo, 5000, TRUE)
//		BREAK
	ENDSWITCH
ENDPROC
PROC PED_BEHAVIOURS()

			IF DOES_ENTITY_EXIST(Ped_One)
			AND NOT IS_ENTITY_DEAD(Ped_One)
				SET_PED_CAN_SWITCH_WEAPON(Ped_One, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Ped_One, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Ped_One, FALSE)
				SET_PED_AS_ENEMY(Ped_One, TRUE)
				SET_ENTITY_INVINCIBLE(Ped_One, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(Ped_Control)
			AND NOT IS_ENTITY_DEAD(Ped_Control)
				SET_PED_CAN_SWITCH_WEAPON(Ped_Control, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Ped_Control, TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(Ped_Control, FALSE)
				SET_PED_AS_ENEMY(Ped_Control, TRUE)
				SET_ENTITY_INVINCIBLE(Ped_Control, TRUE)
			ENDIF	
ENDPROC
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF

	CREATE_WEAPON_WIDGETS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	
	REQUEST_MODEL(A_F_Y_BEACH_01)
	REQUEST_MODEL(A_M_Y_BEACH_01)
	
	WHILE NOT HAS_MODEL_LOADED(A_F_Y_BEACH_01)
		AND NOT HAS_MODEL_LOADED(A_M_Y_BEACH_01)
		WAIT(0)
	ENDWHILE
	
	
	WHILE TRUE
		// Main Loop
		WAIT(0)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		SET_CLOCK_TIME(12, 00, 00)
		ENDIF
		
		// Updating Ped Weapons
		WEAPON_UPDATER_CONTROL()
		WEAPON_UPDATER_PEDS()
		
		// Spawning peds at player's location.
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
		vTransformedOnePos = vPos + fDeltaPos * vSide
		vTransformedTwoPos = vPos - fDeltaPos * vSide
		vPedControl = vTransformedOnePos + vFront * fControlPos
		vPedFemalePos = vTransformedTwoPos + vFront * fPedFemalePos
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())

			IF  bAlternateGunFire = TRUE
				ALTERNATE_GUN_FIRE()
			ENDIF
			
// 	=======================||PED ONE||=======================

			IF bCreateControlPed
			bControlPedFiring = FALSE
				IF DOES_ENTITY_EXIST(Ped_Control)
					DELETE_PED(Ped_Control)
				ENDIF
				Ped_Control = CREATE_PED(PEDTYPE_GANG1, A_M_Y_BEACH_01, vPedControl)
				bCreateControlPed = FALSE
			ENDIF
			
			IF bControlPedFiring
				IF DOES_ENTITY_EXIST(Ped_Control)
					SET_PED_INFINITE_AMMO(Ped_Control, TRUE, WEAPONTYPE_INVALID)
					SET_PED_ACCURACY(Ped_Control, 100)
					TASK_SHOOT_AT_COORD(Ped_Control, (GET_ENTITY_COORDS(Ped_Control, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
				ENDIF
				bControlPedFiring = TRUE
				ELSE IF DOES_ENTITY_EXIST(Ped_Control)
						CLEAR_PED_TASKS(Ped_Control)
					ENDIF
			ENDIF				

			IF bCreatePedOne
			bPedTwoIsFiring = FALSE
				IF DOES_ENTITY_EXIST(Ped_One)
					DELETE_PED(Ped_One)
				ENDIF
				Ped_One = CREATE_PED(PEDTYPE_GANG1, A_F_Y_BEACH_01, vPedFemalePos)
				bCreatePedOne = FALSE
			ENDIF

			PED_BEHAVIOURS()
		
			IF bPedTwoIsFiring
				IF DOES_ENTITY_EXIST(Ped_One)
					SET_PED_INFINITE_AMMO(Ped_One, TRUE, WEAPONTYPE_INVALID)
					SET_PED_ACCURACY(Ped_One, 100)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Ped_One, TRUE)
					TASK_SHOOT_AT_COORD(Ped_One, (GET_ENTITY_COORDS(Ped_One, TRUE) + << 1, 1, 1 >>), -1, FIRING_TYPE_CONTINUOUS)
				ENDIF
				bPedTwoIsFiring = TRUE
				ELSE IF DOES_ENTITY_EXIST(Ped_One)
					CLEAR_PED_TASKS(Ped_One)
					ENDIF
			ENDIF
			
// ---------------------------------------------------------------------DELETION			
			IF bDeleteControl
				IF DOES_ENTITY_EXIST(Ped_Control)
					DELETE_PED(Ped_Control)
				ENDIF
				bDeleteControl = FALSE
			ENDIF

			IF bDeletePedOne
				IF DOES_ENTITY_EXIST(Ped_One)
					DELETE_PED(Ped_One)
				ENDIF
				bDeletePedOne = FALSE
			ENDIF

			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				MISSION_CLEANUP()
			ENDIF
		ENDIF
	ENDWHILE
ENDSCRIPT

#ENDIF	//	 IS_DEBUG_BUILD

