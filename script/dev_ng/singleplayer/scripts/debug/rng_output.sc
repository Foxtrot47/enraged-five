//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	rng_output.sc										//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Used to output datasets that use the random number  //
//		                    generator.                                          //
//																				//
//////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

USING "casino_slots_rng.sch"
USING "three_card_poker_helpers.sch"
USING "blackjack_helpers.sch"
USING "net_lucky_reward.sch"

USING "RouletteConstants.sch"

USING "net_casino_inside_track_minigame.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"

ENUM RNG_OUTPUT_STAGE
	STAGE_INIT = 0,
	STAGE_PROCESSING,
	STAGE_CLEANUP
ENDENUM
RNG_OUTPUT_STAGE eStage = STAGE_INIT

CONST_INT RNG_OUTPUT_SLOTS 					0
CONST_INT RNG_OUTPUT_SLOTS_PRIZE			1
CONST_INT RNG_OUTPUT_ROULETTE 				2
CONST_INT RNG_OUTPUT_3_CARD_POKER			3
CONST_INT RNG_OUTPUT_BLACKJACK				4
CONST_INT RNG_OUTPUT_LUCKY_WHEEL			5
CONST_INT RNG_OUTPUT_RAW					6
CONST_INT RNG_OUTPUT_INSIDE_TRACK			7
CONST_INT RNG_OUTPUT_INSIDE_TRACK_DISTRO	8
CONST_INT RNG_OUTPUT_TYPE_COUNT 			9

CONST_INT RNG_OUTPUT_INSIDE_TRACK_SELECTION_SIZE 20

TWEAK_INT RNG_OUTPUT_SAMPLE_SIZE 			50000000
TWEAK_INT RNG_OUTPUT_SAMPLE_SIZE_PER_FILE 	5000000
TWEAK_INT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME 	1000
TWEAK_INT RNG_OUTPUT_FILE_WRITE_FRAME_DELAY	0

STRUCT RNG_OUTPUT_DATA
	INT iFrameCountSinceWrite
	INT iItemStep
	INT iItemTotal
	INT iItemCount
	INT iItemSelection
	BOOL bRun
	BOOL bPause
	BOOL bInit
	INT iType
	BOOL bGenerateDataset
	BOOL bInitDataset
	FLOAT fPercentageComplete
	TEXT_LABEL_63 tlFilename
	INT iTimer
	TEXT_WIDGET_ID twRunningTime
ENDSTRUCT
RNG_OUTPUT_DATA sRNGData

//SLOTS
LOCAL_SLOT_MACHINE_SETUP slotSetup
LOCAL_SLOT_MACHINE localSlots[54]
RESULT_STRUCT results
INT iOverrideReelResult[NUM_REELS]
INT iOverrideWheelResult = -1

//3 CARD POKER
TCP_DECK sTCPDeck

//BLACKJACK
BJACK_DECK sBJDeck

//INSIDE TRACK
INSIDE_TRACK_RACE_DATA sRaceData

FUNC STRING GET_RNG_OUTPUT_TYPE_NAME(INT iType)
	SWITCH iType
		CASE RNG_OUTPUT_SLOTS				RETURN "slots" BREAK
		CASE RNG_OUTPUT_SLOTS_PRIZE			RETURN "slots_prize" BREAK
		CASE RNG_OUTPUT_ROULETTE			RETURN "roulette" BREAK
		CASE RNG_OUTPUT_3_CARD_POKER		RETURN "poker" BREAK
		CASE RNG_OUTPUT_BLACKJACK			RETURN "blackjack" BREAK
		CASE RNG_OUTPUT_LUCKY_WHEEL 		RETURN "lucky_wheel" BREAK
		CASE RNG_OUTPUT_RAW					RETURN "raw" BREAK
		CASE RNG_OUTPUT_INSIDE_TRACK		RETURN "insidetrack_selection" BREAK
		CASE RNG_OUTPUT_INSIDE_TRACK_DISTRO	RETURN "insidetrack_winner" BREAK
	ENDSWITCH
	RETURN "NA"
ENDFUNC

FUNC INT GET_RNG_OUTPUT_SELECTION_SIZE(INT iType)
	SWITCH iType
		CASE RNG_OUTPUT_SLOTS			RETURN NUM_REELS BREAK
		CASE RNG_OUTPUT_3_CARD_POKER	RETURN TCP_DECK_MAX_CARDS BREAK
		CASE RNG_OUTPUT_BLACKJACK		RETURN (BJACK_MAX_DECKS * BJACK_DECK_MAX_CARDS) BREAK
	ENDSWITCH
	RETURN 1
ENDFUNC

PROC SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(INT iOverride0, INT iOverride1, INT iOverride2, INT iOverride3, INT iOverride4, INT iOverride5)
	g_iCachedRNGValues_InsideTrackOverride[0] = iOverride0
	g_iCachedRNGValues_InsideTrackOverride[1] = iOverride1
	g_iCachedRNGValues_InsideTrackOverride[2] = iOverride2
	g_iCachedRNGValues_InsideTrackOverride[3] = iOverride3
	g_iCachedRNGValues_InsideTrackOverride[4] = iOverride4
	g_iCachedRNGValues_InsideTrackOverride[5] = iOverride5
ENDPROC

PROC UPDATE_RNG_OUTPUT_INSIDE_TRACK_SELECTION(INT iSelection)
	SWITCH iSelection
		
		CASE -1
			SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(-1,-1,-1,-1,-1,-1)
		BREAK
		
		// Max/Min Distribution
		CASE 0	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(28,46,23,72,3,63) BREAK
		CASE 1	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(62,99,64,82,76,83) BREAK
		CASE 2	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(28,99,23,82,3,83) BREAK
		CASE 3	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(28,46,64,82,76,83) BREAK
		CASE 4	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(62,99,23,72,76,83) BREAK
		CASE 5	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(62,99,64,82,3,63) BREAK
		CASE 6	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(28,99,64,82,76,83) BREAK
		CASE 7	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(62,99,23,82,76,83) BREAK
		CASE 8	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(62,99,64,82,3,83) BREAK
		
		// Random
		CASE 9	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(97,27,23,18,14,63) BREAK
		CASE 10	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(99,85,29,34,83,59) BREAK
		CASE 11	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(19,13,30,52,55,91) BREAK
		CASE 12	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(48,28,16,8,1,90) BREAK
		CASE 13	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(27,36,37,18,54,43) BREAK
		CASE 14	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(44,13,29,18,3,5) BREAK
		CASE 15	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(13,74,78,88,1,54) BREAK
		CASE 16	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(85,41,12,71,75,11) BREAK
		CASE 17	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(48,28,30,34,63,3) BREAK
		CASE 18	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(19,39,16,64,5,14) BREAK
		CASE 19	SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(68,84,88,25,1,32) BREAK
		
		// Sink
		DEFAULT
			SET_RNG_OUTPUT_INSIDE_TRACK_SELECTION(0,0,0,0,0,0)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(INT iHorseEnumID)
	SWITCH iHorseEnumID
		// Favourite
		CASE 7	RETURN 1
		CASE 9	RETURN 2
		CASE 13	RETURN 3
		CASE 15	RETURN 4
		CASE 19	RETURN 5
		CASE 26	RETURN 6
		CASE 27	RETURN 7
		CASE 28	RETURN 8
		CASE 36	RETURN 9
		CASE 39	RETURN 10
		CASE 41	RETURN 11
		CASE 42	RETURN 12
		CASE 44	RETURN 13
		CASE 46	RETURN 14
		CASE 48	RETURN 15
		CASE 49	RETURN 16
		CASE 50	RETURN 17
		CASE 51	RETURN 18
		CASE 60	RETURN 19
		CASE 61	RETURN 20
		CASE 62	RETURN 21
		CASE 68	RETURN 22
		CASE 70	RETURN 23
		CASE 73	RETURN 24
		CASE 74	RETURN 25
		CASE 79	RETURN 26
		CASE 84	RETURN 27
		CASE 85	RETURN 28
		CASE 89	RETURN 29
		CASE 92	RETURN 30
		CASE 97	RETURN 31
		CASE 98	RETURN 32
		CASE 99	RETURN 33
		
		// Middle
		CASE 4	RETURN 1
		CASE 6	RETURN 2
		CASE 8	RETURN 3
		CASE 12	RETURN 4
		CASE 16	RETURN 5
		CASE 17	RETURN 6
		CASE 18	RETURN 7
		CASE 23	RETURN 8
		CASE 24	RETURN 9
		CASE 25	RETURN 10
		CASE 29	RETURN 11
		CASE 30	RETURN 12
		CASE 34	RETURN 13
		CASE 35	RETURN 14
		CASE 37	RETURN 15
		CASE 45	RETURN 16
		CASE 47	RETURN 17
		CASE 52	RETURN 18
		CASE 53	RETURN 19
		CASE 58	RETURN 20
		CASE 64	RETURN 21
		CASE 65	RETURN 22
		CASE 71	RETURN 23
		CASE 72	RETURN 24
		CASE 78	RETURN 25
		CASE 81	RETURN 26
		CASE 82	RETURN 27
		CASE 86	RETURN 28
		CASE 88	RETURN 29
		CASE 93	RETURN 30
		CASE 94	RETURN 31
		CASE 96	RETURN 32
		
		// Outlier
		CASE 1	RETURN 1
		CASE 2	RETURN 2
		CASE 3	RETURN 3
		CASE 5	RETURN 4
		CASE 10	RETURN 5
		CASE 11	RETURN 6
		CASE 14	RETURN 7
		CASE 20	RETURN 8
		CASE 21	RETURN 9
		CASE 22	RETURN 10
		CASE 31	RETURN 11
		CASE 32	RETURN 12
		CASE 33	RETURN 13
		CASE 38	RETURN 14
		CASE 40	RETURN 15
		CASE 43	RETURN 16
		CASE 54	RETURN 17
		CASE 55	RETURN 18
		CASE 56	RETURN 19
		CASE 57	RETURN 20
		CASE 59	RETURN 21
		CASE 63	RETURN 22
		CASE 66	RETURN 23
		CASE 67	RETURN 24
		CASE 69	RETURN 25
		CASE 75	RETURN 26
		CASE 76	RETURN 27
		CASE 77	RETURN 28
		CASE 80	RETURN 29
		CASE 83	RETURN 30
		CASE 87	RETURN 31
		CASE 90	RETURN 32
		CASE 91	RETURN 33
		CASE 95	RETURN 34
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC GET_RNG_OUTPUT_FILENAME(INT iTypeOverride = -1)

	INT iType = sRNGData.iType
	IF iTypeOverride != -1
		iType = iTypeOverride
	ENDIF
	
	sRNGData.tlFilename = "RNGnums_("
	sRNGData.tlFilename += GET_RNG_OUTPUT_TYPE_NAME(iType)
	sRNGData.tlFilename += ")"
	
	IF (iType = RNG_OUTPUT_INSIDE_TRACK_DISTRO)
		sRNGData.tlFilename += "_selections("
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionFavourite[0])
		sRNGData.tlFilename += ","
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionFavourite[1])
		sRNGData.tlFilename += ","
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionMiddle[0])
		sRNGData.tlFilename += ","
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionMiddle[1])
		sRNGData.tlFilename += ","
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionOutlier[0])
		sRNGData.tlFilename += ","
		sRNGData.tlFilename += GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionOutlier[1])
		sRNGData.tlFilename += ")"
	ENDIF
	
	sRNGData.tlFilename += "1"
	INT i
	REPEAT (sRNGData.iItemCount / RNG_OUTPUT_SAMPLE_SIZE_PER_FILE) i
		sRNGData.tlFilename += "1"
	ENDREPEAT
	
	sRNGData.tlFilename += ".txt"
ENDPROC

PROC DO_INITIALISE()
	START_WIDGET_GROUP("RNG Output")
	
		START_WIDGET_GROUP("Dataset")
			START_NEW_WIDGET_COMBO()
				INT iType
				REPEAT RNG_OUTPUT_TYPE_COUNT iType
					ADD_TO_WIDGET_COMBO(GET_RNG_OUTPUT_TYPE_NAME(iType))
				ENDREPEAT
			STOP_WIDGET_COMBO("Type", sRNGData.iType)
			
			ADD_WIDGET_INT_SLIDER("Sample size", RNG_OUTPUT_SAMPLE_SIZE, 0, 100000000, 1000000)
			ADD_WIDGET_INT_SLIDER("Sample size per file", RNG_OUTPUT_SAMPLE_SIZE_PER_FILE, 0, 100000000, 1000000)
			ADD_WIDGET_INT_SLIDER("Sample size per frame", RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME, 0, 100000000, 1000000)
			ADD_WIDGET_INT_SLIDER("File write frame delay", RNG_OUTPUT_FILE_WRITE_FRAME_DELAY, 0, 10000, 1000)
			
			ADD_WIDGET_INT_READ_ONLY("Current sample", sRNGData.iItemCount)
			ADD_WIDGET_FLOAT_READ_ONLY("Percentage complete", sRNGData.fPercentageComplete)
			
			ADD_WIDGET_INT_READ_ONLY("Current selection", sRNGData.iItemSelection)
			
			sRNGData.twRunningTime = ADD_TEXT_WIDGET("Running time")
			
			ADD_WIDGET_BOOL("Generate", sRNGData.bRun)
			ADD_WIDGET_BOOL("Pause", sRNGData.bPause)
			ADD_WIDGET_BOOL("Generate Full Dataset", sRNGData.bGenerateDataset)
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	eStage = STAGE_PROCESSING
ENDPROC

PROC DO_PROCESSING()

	INT iSelection
	INT iSample
	
	IF sRNGData.bPause
		EXIT
	ENDIF
	
	IF sRNGData.bGenerateDataset
		IF NOT sRNGData.bInitDataset
			
			sRNGData.bInitDataset = TRUE
			sRNGData.bInit = TRUE
			sRNGData.iItemCount = 0
			sRNGData.iItemSelection = 0
			sRNGData.iFrameCountSinceWrite = 0
			sRNGData.iType = 0//-1
			sRNGData.fPercentageComplete = 0
			
			SETUP_SLOT_MACHINES(slotSetup, localSlots)
			
			TCP_INIT_AND_SHUFFLE_DECK(sTCPDeck)
			
			BJACK_INIT_AND_SHUFFLE_DECK(sBJDeck)
		ENDIF
		
		IF NOT sRNGData.bRun
		
			sRNGData.iType++
			sRNGData.bRun = TRUE
			sRNGData.bInit = TRUE
			sRNGData.iItemCount = 0
			sRNGData.iItemSelection = 0
			sRNGData.iFrameCountSinceWrite = 0
			
			IF (sRNGData.iType = RNG_OUTPUT_SLOTS)
				RNG_OUTPUT_SAMPLE_SIZE = 50000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 50000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 5000
			ELIF (sRNGData.iType = RNG_OUTPUT_SLOTS_PRIZE)
				RNG_OUTPUT_SAMPLE_SIZE = 10000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 10000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 5000
			ELIF (sRNGData.iType = RNG_OUTPUT_ROULETTE)
				RNG_OUTPUT_SAMPLE_SIZE = 50000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 50000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 10000
			ELIF (sRNGData.iType = RNG_OUTPUT_3_CARD_POKER)
				RNG_OUTPUT_SAMPLE_SIZE = 50000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 5000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 5000
			ELIF (sRNGData.iType = RNG_OUTPUT_BLACKJACK)
				RNG_OUTPUT_SAMPLE_SIZE = 10000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 2000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 5000
			ELIF (sRNGData.iType = RNG_OUTPUT_LUCKY_WHEEL)
				RNG_OUTPUT_SAMPLE_SIZE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 10000
			ELIF (sRNGData.iType = RNG_OUTPUT_INSIDE_TRACK)
				RNG_OUTPUT_SAMPLE_SIZE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 10000
			ELIF (sRNGData.iType = RNG_OUTPUT_INSIDE_TRACK_DISTRO)
				RNG_OUTPUT_SAMPLE_SIZE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FILE = 100000000
				RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME = 10000
			ELSE
				sRNGData.bGenerateDataset = FALSE
			ENDIF
		ENDIF
	ELSE
		sRNGData.bInitDataset = FALSE
	ENDIF

	IF sRNGData.bRun
		
		IF NOT sRNGData.bInit
			sRNGData.bInit = TRUE
			sRNGData.iItemCount = 0
			sRNGData.iFrameCountSinceWrite = 0
			sRNGData.iItemSelection = 1
			
			SETUP_SLOT_MACHINES(slotSetup, localSlots)
			
			TCP_INIT_AND_SHUFFLE_DECK(sTCPDeck)
			
			BJACK_INIT_AND_SHUFFLE_DECK(sBJDeck)
			
			UPDATE_RNG_OUTPUT_INSIDE_TRACK_SELECTION(-1)
			
			
			sRNGData.iTimer = GET_CLOUD_TIME_AS_INT()
		ENDIF
		
		IF sRNGData.iFrameCountSinceWrite < RNG_OUTPUT_FILE_WRITE_FRAME_DELAY
			sRNGData.iFrameCountSinceWrite++
			EXIT
		ENDIF
		
		sRNGData.iFrameCountSinceWrite = 0
		
		IF (sRNGData.iType = RNG_OUTPUT_SLOTS)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
			
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
					
					DETERMINE_SLOT_MACHINE_RESULT(0, results, slotSetup, localSlots, iOverrideReelResult, iOverrideWheelResult)
					
					REPEAT GET_RNG_OUTPUT_SELECTION_SIZE(sRNGData.iType) iSelection
						IF iSelection > 0
							SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						ENDIF
						SAVE_INT_TO_NAMED_DEBUG_FILE(results.iVirtualReelPos[iSelection], "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					ENDREPEAT
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
		ELIF (sRNGData.iType = RNG_OUTPUT_SLOTS_PRIZE)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
			
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					iOverrideReelResult[0] = SRS_WILD
					iOverrideReelResult[1] = SRS_WILD
					iOverrideReelResult[2] = SRS_WILD
					
					DETERMINE_SLOT_MACHINE_RESULT(0, results, slotSetup, localSlots, iOverrideReelResult, iOverrideWheelResult)
					
					SAVE_INT_TO_NAMED_DEBUG_FILE(results.iWheelPos, "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
		ELIF (sRNGData.iType = RNG_OUTPUT_ROULETTE)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					SAVE_INT_TO_NAMED_DEBUG_FILE(ROULETTE_GENERATE_WINNING_NUMBER(), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
		
		ELIF (sRNGData.iType = RNG_OUTPUT_3_CARD_POKER)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					TCP_SHUFFLE_DECK(sTCPDeck)
					
					REPEAT GET_RNG_OUTPUT_SELECTION_SIZE(sRNGData.iType) iSelection
						IF iSelection > 0
							SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						ENDIF
						//SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sTCPDeck.eCards[iSelection]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(g_iCachedRNGValues_Poker[iSelection], "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					ENDREPEAT
					
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
		
		ELIF (sRNGData.iType = RNG_OUTPUT_BLACKJACK)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
			
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					BJACK_SHUFFLE_DECK(sBJDeck)
					
					REPEAT GET_RNG_OUTPUT_SELECTION_SIZE(sRNGData.iType) iSelection
						IF iSelection > 0
							SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						ENDIF
						//SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sBJDeck.eCards[iSelection]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(g_iCachedRNGValues_Blackjack[iSelection], "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					ENDREPEAT
					
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
		ELIF (sRNGData.iType = RNG_OUTPUT_LUCKY_WHEEL)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
					//SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(PICK_RANDOM_LUCK_REWARD_TYPE()), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					PICK_RANDOM_LUCK_REWARD_TYPE()
					SAVE_INT_TO_NAMED_DEBUG_FILE(g_iCachedRNGValues_LuckyWheel, "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
		ELIF (sRNGData.iType = RNG_OUTPUT_RAW)
		
			GET_RNG_OUTPUT_FILENAME()
			OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
						
					SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RANDOM_MWC_INT_IN_RANGE(0, 2147483647), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
		ELIF (sRNGData.iType = RNG_OUTPUT_INSIDE_TRACK)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					SET_BIT(sRaceData.iBS, BS_RACE_INSIDE_TRACK_MINIGAME_SINGLE_EVENT)
					MAINTAIN_INSIDE_TRACK_MINIGAME_SETUP_NEW_GAME(sRaceData, TRUE)
					
					GET_RNG_OUTPUT_FILENAME()
					OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionFavourite[0]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionFavourite[1]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionMiddle[0]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionMiddle[1]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionOutlier[0]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackSelectionOutlier[1]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					CLOSE_DEBUG_FILE()
					
					RANDOMISE_INSIDE_TRACK_MINIGAME_REWARD(sRaceData)
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
		ELIF (sRNGData.iType = RNG_OUTPUT_INSIDE_TRACK_DISTRO)
		
			UPDATE_RNG_OUTPUT_INSIDE_TRACK_SELECTION(sRNGData.iItemSelection)
		
			REPEAT RNG_OUTPUT_SAMPLE_SIZE_PER_FRAME iSample
				IF sRNGData.iItemCount < RNG_OUTPUT_SAMPLE_SIZE
				
					SET_BIT(sRaceData.iBS, BS_RACE_INSIDE_TRACK_MINIGAME_SINGLE_EVENT)
					
					MAINTAIN_INSIDE_TRACK_MINIGAME_SETUP_NEW_GAME(sRaceData, TRUE)
					RANDOMISE_INSIDE_TRACK_MINIGAME_REWARD(sRaceData)
					
					GET_RNG_OUTPUT_FILENAME()
					OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(GET_RNG_OUTPUT_INSIDE_TRACK_LOCAL_HORSE_INDEX(g_iCachedRNGValues_InsideTrackFinish[0]), "X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", sRNGData.tlFilename)
					CLOSE_DEBUG_FILE()
					
					sRNGData.iItemCount++
					
				ENDIF
			ENDREPEAT
			
			UPDATE_RNG_OUTPUT_INSIDE_TRACK_SELECTION(-1)
			
		ENDIF
		
		sRNGData.fPercentageComplete = (TO_FLOAT(sRNGData.iItemCount) / TO_FLOAT(RNG_OUTPUT_SAMPLE_SIZE))*100.0
		
		IF sRNGData.iItemCount >= RNG_OUTPUT_SAMPLE_SIZE
			sRNGData.bRun = FALSE
			
			IF (sRNGData.iType = RNG_OUTPUT_INSIDE_TRACK_DISTRO)
				sRNGData.iItemSelection++
				IF sRNGData.iItemSelection < RNG_OUTPUT_INSIDE_TRACK_SELECTION_SIZE
					sRNGData.bRun = TRUE
					sRNGData.iItemCount = 0
				ENDIF
			ENDIF
		ENDIF
		
		INT iRunningTime = (GET_CLOUD_TIME_AS_INT() - sRNGData.iTimer)
		
		INT iHours = (iRunningTime/3600)
		INT iMinutes = ((iRunningTime%3600)/60)
		INT iSeconds = ((iRunningTime%3600)%60)
		
		TEXT_LABEL_63 tlRunningTime = ""
		
		IF iHours < 10
			tlRunningTime += "0"
		ENDIF
		tlRunningTime += iHours
		tlRunningTime += ":"
		IF iMinutes < 10
			tlRunningTime += "0"
		ENDIF
		tlRunningTime += iMinutes
		tlRunningTime += ":"
		IF iSeconds < 10
			tlRunningTime += "0"
		ENDIF
		tlRunningTime += iSeconds
		
		SET_CONTENTS_OF_TEXT_WIDGET(sRNGData.twRunningTime, tlRunningTime)
		
	ELIF sRNGData.bInit
		sRNGData.bInit = FALSE
	ENDIF
ENDPROC

PROC DO_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	WHILE TRUE
	
		WAIT(0)
		
		SWITCH eStage
			CASE STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE STAGE_PROCESSING
				DO_PROCESSING()
			BREAK
			
			CASE STAGE_CLEANUP
				DO_CLEANUP()
			BREAK
		ENDSWITCH
		
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
//			
//			INT iHorseIDTemp, iThisOdds
//			FOR iHorseIDTemp = ENUM_TO_INT(INSIDE_TRACK_HORSE_ATetheredEnd) TO ENUM_TO_INT(INSIDE_TRACK_HORSE_TYPE_COUNT)-1
//				iThisOdds = GET_INSIDE_TRACK_MINIGAME_SCALEFORM_HORSE_ODDS(INT_TO_ENUM(INSIDE_TRACK_HORSE_TYPE, iHorseIDTemp))
//				
//				CLEAR_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", "horse_info.txt")
//				
//				OPEN_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", "horse_info.txt")
//				
//					SAVE_INT_TO_NAMED_DEBUG_FILE(iHorseIDTemp, "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					SAVE_INT_TO_NAMED_DEBUG_FILE(iThisOdds, "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					SAVE_STRING_TO_NAMED_DEBUG_FILE("/1", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					SAVE_STRING_TO_NAMED_DEBUG_FILE(",", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					
//					IF (iThisOdds <= 5)
//						SAVE_STRING_TO_NAMED_DEBUG_FILE("Favourite", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					ELIF (iThisOdds <= 15)
//						SAVE_STRING_TO_NAMED_DEBUG_FILE("Middle", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					ELSE
//						SAVE_STRING_TO_NAMED_DEBUG_FILE("Outlier", "X:/gta5/titleupdate/dev_ng/RNG", "horse_info.xls")
//					ENDIF
//					
//					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE("X:/gta5/titleupdate/dev_ng/RNG", "horse_info.txt")
//					
//					
//				CLOSE_DEBUG_FILE()
//				
//				
//			ENDFOR
//			
//		ENDIF
	ENDWHILE
	
ENDSCRIPT

#ENDIF
