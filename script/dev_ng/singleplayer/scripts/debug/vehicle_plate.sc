//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	FacebookWeb.sc												//
//		AUTHOR			:	Bobby												//
//		DESCRIPTION		:	A carmod shop that gives the player the chance to customise	//
//							their current vehicle.										//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"

USING "commands_path.sch"

#IF IS_DEBUG_BUILD
			
	TEXT_WIDGET_ID	twid_NumberPlate
	STRING st_Car_numberPlate
	INT iCurentHash
	
	BOOL bMakeAcar = TRUE
	INT iPlateNumber = 0
	INT iPlateNumberOld
	VECTOR vMakeCarPos
	VEHICLE_INDEX vi_player_car
	
	PROC MISSION_CLEANUP()
		// Mark the peds as no longer needed.
		DISPLAY_RADAR(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE,0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE) 
		DESTROY_ALL_CAMS()
	    SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_TIME_SCALE(1.0)
		TERMINATE_THIS_THREAD()
	ENDPROC
#ENDIF	

SCRIPT
	#IF IS_DEBUG_BUILD
		
	    SET_MISSION_FLAG (TRUE)
	    // death arrest check
	    IF HAS_FORCE_CLEANUP_OCCURRED() 
	        MISSION_CLEANUP()
	    ENDIF
		
		START_WIDGET_GROUP(" Custom Vehicle Number Plate")
			twid_NumberPlate = ADD_TEXT_WIDGET("Number Plate")
			SET_CONTENTS_OF_TEXT_WIDGET(twid_NumberPlate, "Type in new plate")
			INT iMaxNumberOfPlates = GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
			TEXT_LABEL_63 txtlbl
			txtlbl = "Total Custom Plate Backgrounds = "
			txtlbl += iMaxNumberOfPlates
			ADD_WIDGET_STRING(txtlbl)
			//ADD_WIDGET_INT_READ_ONLY("Total Plate BGS", iMaxNumberOfPlates)
			ADD_WIDGET_INT_SLIDER("Change to Plate BG", iPlateNumber, 0, iMaxNumberOfPlates-1, 1)
			ADD_WIDGET_BOOL("Make a Vehicle", bMakeAcar)
		STOP_WIDGET_GROUP()
		
		iCurentHash = GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_NumberPlate))
		iPlateNumberOld = iPlateNumber
		
		// Main loop
		WHILE TRUE
			WAIT(0)
			
			IF bMakeAcar
				IF NOT IS_VEHICLE_DRIVEABLE(vi_player_car)	
					REQUEST_MODEL(TAILGATER)
					IF HAS_MODEL_LOADED(TAILGATER)				
						GET_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0,5.0, 0.0>>),vMakeCarPos)
						vi_player_car = CREATE_VEHICLE(TAILGATER,vMakeCarPos, 0.0)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vi_player_car)
						bMakeAcar = FALSE
					ENDIF
				ELSE 
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vi_player_car)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					ENDIF
					DELETE_VEHICLE(vi_player_car)
				ENDIF
			ENDIF
			
			//Change the plate number
			IF  GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_NumberPlate)) <> iCurentHash
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						st_Car_numberPlate = GET_CONTENTS_OF_TEXT_WIDGET(twid_NumberPlate)
						iCurentHash = GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_NumberPlate))
						SET_VEHICLE_NUMBER_PLATE_TEXT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),st_Car_numberPlate)
					ENDIF					
				ENDIF
			ENDIF
			
			IF iPlateNumber <> iPlateNumberOld
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),iPlateNumber )
						iPlateNumberOld = iPlateNumber
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				MISSION_CLEANUP()
			ENDIF

		ENDWHILE
	#ENDIF	
	
ENDSCRIPT
