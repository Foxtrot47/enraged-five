#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   Train Tester                                              	//
//      AUTHOR          :   Fraser Morgan                                             	//
//      DESCRIPTION     :   Tool for creating trains moving, stopping etc. for Audio    // 														//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "script_buttons.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_physics.sch"
USING "shared_debug.sch"


VEHICLE_INDEX missionTrain
CAMERA_INDEX trainCam

INT iTrainCam = -30
INT iCarriage
VECTOR vCreate = <<613, 6438, 31>>
VECTOR vCamPoint
VECTOR vTrainPOSITION
VECTOR vTrainCam
VECTOR vStoppingArea
VECTOR vStopZone
VECTOR vFront, vSide, vUp, vPos
VECTOR vTrainFront, vTrainSide, vTrainUp, vTrainPos
VECTOR vPointForward
INT iCreate = 0
INT iDistance = 60
FLOAT fStop
FLOAT fSpeed = 0.0
FLOAT fPadSpeed
FLOAT fTrainDist
BOOL bDirection = TRUE
BOOL bDoCreate = FALSE
BOOL bTerminate = FALSE
BOOL bAllowStops = FALSE
BOOL bPadControl = FALSE
BOOL bStopAtPlayer = FALSE
BOOL bReturningToPlayer = FALSE
BOOL bSkipToStop = FALSE
BOOL bTrainCam
BOOL bTurnTrain
BOOL bLookForward
BOOL bPlayerControl = TRUE
BOOL bWarpToCity
BOOL bWarpToCountry
BOOL bInCity

WIDGET_GROUP_ID createTrainWidgets



// cleanup the mission
PROC MISSION_CLEANUP()
	SET_RANDOM_TRAINS(TRUE)

		IF DOES_WIDGET_GROUP_EXIST(createTrainWidgets)
			DELETE_WIDGET_GROUP(createTrainWidgets)
		ENDIF
	SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
	CLEAR_FOCUS()
	DESTROY_CAM(trainCam, TRUE)
	TERMINATE_THIS_THREAD() 
ENDPROC
PROC TRAIN_CAMERA()

	IF bTrainCam = TRUE
		IF DOES_ENTITY_EXIST(missionTrain)
			SET_FOCUS_POS_AND_VEL((GET_ENTITY_COORDS(missionTrain, FALSE)), <<0,0,0>>)
			RENDER_SCRIPT_CAMS(TRUE, TRUE)
		ENDIF
	ELSE RENDER_SCRIPT_CAMS(FALSE, TRUE)
		CLEAR_FOCUS()
	ENDIF
ENDPROC
PROC CREATE_TRAIN_WIDGETS()
		createTrainWidgets = START_WIDGET_GROUP("Train Creation")

			ADD_WIDGET_INT_SLIDER("Carriage Configuration", iCreate, 0, 23, 1)
			ADD_WIDGET_FLOAT_SLIDER("Speed", fSpeed, 0, 50, 1)
			ADD_WIDGET_BOOL("Direction", bDirection)
			ADD_WIDGET_BOOL("Create Train", bDoCreate)
			ADD_WIDGET_BOOL("Warp Player to City", bWarpToCity)
			ADD_WIDGET_BOOL("Warp Player to Country", bWarpToCountry)
			ADD_WIDGET_STRING("Pad: RT for speed, Y for direction change")
			ADD_WIDGET_BOOL("Pad Enabled", bPadControl)
			ADD_WIDGET_FLOAT_READ_ONLY("pad value", fPadSpeed)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("Camera Options")
			ADD_WIDGET_INT_SLIDER("Camera Distance", iTrainCam, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("Carriage to View", iCarriage, 0, 100, 1)
			ADD_WIDGET_BOOL("Camera on Train", bTrainCam)
			ADD_WIDGET_BOOL("Point Camera Forward", bLookForward)
			ADD_WIDGET_BOOL("Stops At Stations", bAllowStops)
			ADD_WIDGET_BOOL("Jump To Next Station", bSkipToStop)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("'Train Fly-By' makes the train constantly drive past player's position.")
			ADD_WIDGET_STRING("The train will turn around at your set distance and return.")
			ADD_WIDGET_BOOL("Train Fly-By", bTurnTrain)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("'Stop Train at Player' will make the train travel out to your set distance,")
			ADD_WIDGET_STRING("turn around, then travel back and stop at your location.")
			ADD_WIDGET_STRING("NOTES: Player needs to be facing tracks. Train may stop short if it hasn't")
			ADD_WIDGET_STRING("been given long enough to reach full speed.")
			ADD_WIDGET_BOOL("Stop Train at Player", bStopAtPlayer)
			ADD_WIDGET_INT_SLIDER("Distance to go", iDistance, 60,  1000, 1)
			ADD_WIDGET_FLOAT_READ_ONLY("Train's current dist.", fTrainDist)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Lock player control", bPlayerControl)
			ADD_WIDGET_BOOL("End Script", bTerminate)
		STOP_WIDGET_GROUP()

ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF
	
	CREATE_TRAIN_WIDGETS()
	
	SET_RANDOM_TRAINS(FALSE)
	DELETE_ALL_TRAINS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<626.68, 6442.31, 30.88>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), -177)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	
	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTCAR)
	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
	REQUEST_MODEL(TANKERCAR)
	REQUEST_MODEL(METROTRAIN)
	
	
	WHILE NOT HAS_MODEL_LOADED(FREIGHT)
	OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
	OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
	OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
	OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
	OR NOT HAS_MODEL_LOADED(TANKERCAR)
	OR NOT HAS_MODEL_LOADED(METROTRAIN)
		WAIT(0)
	ENDWHILE
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
	
		TRAIN_CAMERA()
				
		INVALIDATE_IDLE_CAM()

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF bDoCreate
				IF DOES_ENTITY_EXIST(missionTrain)
					DELETE_MISSION_TRAIN(missionTrain)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(trainCam)
				ENDIF
				IF bInCity = TRUE
					missionTrain = CREATE_MISSION_TRAIN(iCreate, <<519.2, -1127.4, 29.6>>, bDirection)
				ELSE missionTrain = CREATE_MISSION_TRAIN(iCreate, vCreate, bDirection)
				ENDIF
					
				trainCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				CLEAR_FOCUS()
				bDoCreate = FALSE
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(missionTrain)
				vCamPoint = <<0, 0, 2>>
				IF GET_TRAIN_DIRECTION(missionTrain) = TRUE
					vTrainCam = <<iTrainCam, 0, 5>>
				ELSE vTrainCam = <<-iTrainCam, 0, 5>>
				ENDIF
				IF bLookForward = TRUE
					GET_ENTITY_MATRIX(missionTrain, vTrainFront, vTrainSide, vTrainUp, vTrainPos)
					vPointForward = vTrainPos + vTrainFront + vTrainUp * 1.880
					ATTACH_CAM_TO_ENTITY(trainCam, missionTrain, <<0, 0, 4>>)
					POINT_CAM_AT_COORD(trainCam, vPointForward)
				ELSE POINT_CAM_AT_ENTITY(trainCam, (GET_TRAIN_CARRIAGE(missionTrain, iCarriage)), vCamPoint)
					ATTACH_CAM_TO_ENTITY(trainCam, (GET_TRAIN_CARRIAGE(missionTrain, iCarriage)), vTrainCam)
				ENDIF
				SET_CAM_FOV(trainCam, 30)
				fTrainDist = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(missionTrain, TRUE)), (GET_ENTITY_COORDS(PLAYER_PED_ID())))
			ENDIF
			
			IF IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE)
				IF DOES_ENTITY_EXIST(missionTrain)
					IF bDirection = TRUE
						vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
						DELETE_MISSION_TRAIN(missionTrain)
						missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, FALSE)
						bDirection = FALSE
					ELSE
						vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
						DELETE_MISSION_TRAIN(missionTrain)
						missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, TRUE)
						bDirection = TRUE
					ENDIF
				ENDIF
			ENDIF

			IF IS_VEHICLE_DRIVEABLE(missionTrain)
				IF bPadControl = TRUE
					fPadSpeed = TO_FLOAT(GET_BUTTON_VALUE(PAD1, RIGHTSHOULDER2)) / 5.1
					SET_TRAIN_CRUISE_SPEED(missionTrain, fPadSpeed)
				ENDIF
			ENDIF
			
			IF bWarpToCity
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<514.2, -1127.1, 29.3>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), -95)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					bInCity = TRUE
					bWarpToCity = FALSE
				ENDIF
			ENDIF
			IF bWarpToCountry
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<626.68, 6442.31, 30.88>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), -177)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					bInCity = FALSE
					bWarpToCountry = FALSE
				ENDIF
			ENDIF

			
			IF bTurnTrain = TRUE
				bStopAtPlayer = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)), (GET_ENTITY_COORDS(missionTrain, TRUE)), FALSE) > iDistance
					
					IF bDirection = TRUE
						vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
						DELETE_MISSION_TRAIN(missionTrain)
						missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, FALSE)
						WAIT(500)
						bDirection = FALSE
					ELSE
						vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
						DELETE_MISSION_TRAIN(missionTrain)
						missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, TRUE)
						WAIT(500)
						bDirection = TRUE
					ENDIF
				ENDIF
			ENDIF
					
			IF bStopAtPlayer = TRUE
				bPadControl = FALSE
				bTurnTrain = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID(), TRUE)), (GET_ENTITY_COORDS(missionTrain, TRUE)), FALSE) > iDistance
					IF bReturningToPlayer = FALSE
						IF bDirection = TRUE
							vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
							DELETE_MISSION_TRAIN(missionTrain)
							missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, FALSE)
							SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)
							bDirection = FALSE
							bReturningToPlayer = TRUE
							WAIT(500)
						ELSE
							vTrainPOSITION = GET_ENTITY_COORDS(missionTrain, TRUE)
							DELETE_MISSION_TRAIN(missionTrain)
							missionTrain = CREATE_MISSION_TRAIN(iCreate, vTrainPOSITION, TRUE)
							SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)
							bDirection = TRUE
							bReturningToPlayer = TRUE
							WAIT(500)
						ENDIF
					ENDIF
				ELSE IF bReturningToPlayer = FALSE 
						SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)	
					ENDIF
				ENDIF

				IF bReturningToPlayer = TRUE
					GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
					vStopZone = vPos + vFront * 5.6
					fStop = (fSpeed*fSpeed / 16.66) - 10
					IF bDirection = TRUE
						vStoppingArea = vStopZone + fStop * vSide
					ELSE vStoppingArea = vStopZone - fStop * vSide
					ENDIF
					IF IS_ENTITY_AT_COORD(missionTrain, vStoppingArea, <<10, 10, 10>>)
						SET_TRAIN_CRUISE_SPEED(missionTrain, 0)
						SET_TRAIN_IS_STOPPED_AT_STATION(missionTrain)
					ENDIF
				ENDIF
			ELSE bReturningToPlayer = FALSE
				
			ENDIF

			IF DOES_ENTITY_EXIST(missionTrain)
				IF bAllowStops
					SET_TRAIN_STOPS_FOR_STATIONS(missionTrain, TRUE)
					SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)
				ELSE IF bPadControl = FALSE
					AND bReturningToPlayer = FALSE
						SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)
						SET_TRAIN_STOPS_FOR_STATIONS(missionTrain, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bSkipToStop
				SKIP_TO_NEXT_ALLOWED_STATION(missionTrain)
				bSkipToStop = FALSE
			ENDIF
			
			IF bPlayerControl
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
			ELSE
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
			ENDIF
			
			
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				OR bTerminate
					MISSION_CLEANUP()
				ENDIF
			IF bTerminate
				MISSION_CLEANUP()
			ENDIF
		ENDIF
    ENDWHILE	
ENDSCRIPT

#ENDIF