/*
	Author: 		Orlando Cazalet-Hyams
	Team: 			Online Technical
	Description:	Update the global event cache (g_eventCache) with certain events (only error events right now)
					and display info on these through a debug-only UI.
					main_persistent.sc manages the launching of this script.
					
*/

USING "script_maths.sch"
USING "commands_hud.sch"
USING "error_listener_public.sch"
USING "fm_in_corona_header.sch"
USING "transition_common.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
BOOL 		m_bTerminateScript
BOOL		m_bClearEventCache
BOOL 		m_bExtraDebug 
BOOL 		m_bClearNetFatalError
BOOL 		m_bCanCloseNetFatalError
BOOL 		m_bAllowDebugDisplay = TRUE

// Trigger an error through error thrower
BOOL 		m_bArrayOverrun
BOOL 		m_bInsLimit
BOOL		m_bStackOverflow

PROC UPDATE_DEBUG_STRINGS_WITH_NEW_SCRIPTS()
	SCRIPT_THREAD_ITERATOR_RESET()
	THREADID 	tempID = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
	INT			iScriptHash 
	STRING		sScriptName

	WHILE tempID <> NULL
		sScriptName = GET_NAME_OF_SCRIPT_WITH_THIS_ID(tempID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sScriptName)
			iScriptHash = GET_HASH_KEY(sScriptName)
			TEXT_LABEL_31 sScriptHash =  iScriptHash
			REMOVE_DEBUG_STRING_WITH_THIS_KEY(sScriptHash)
			ADD_DEBUG_STRING(sScriptHash, sScriptName)
		ENDIF
		tempID = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
	ENDWHILE
ENDPROC

/* ----------------- Drawing ------------------------ */

//Debug build has a simple display.
ENUM EVENT_DISPLAY_STATE
	EDS_HIDDEN,
	EDS_FLOAT_IN,
	EDS_FLOAT_OUT,
	EDS_SHOWING
ENDENUM

EVENT_DISPLAY_STATE m_eDisplayState = EDS_HIDDEN
INT 		m_iFullCacheWriteAttemps
INT 		m_iDisplayTimer	 		= -1
INT 		m_iHighlightIndex		= -1
FLOAT 		m_fCurrentY 			= 1
CONST_FLOAT cfELEMENT_HEIGHT	 	0.04	
CONST_FLOAT cfELEMENT_WIDTH 		0.82	
CONST_FLOAT cfPADDING_Y 			0.01	
CONST_FLOAT cfPADDING_X				0.05	
CONST_FLOAT cfHEADER_Y		 		0.1		//Title + some extra space
CONST_INT 	ciDISPLAY_DURATION		10000 	//10 seconds
CONST_FLOAT	cfFLOAT_SPEED 			0.8		//screen heights per second	
//Stuff for net fatal error display
EVENT_DISPLAY_STATE m_eNetFatalErrorState = EDS_HIDDEN
FLOAT 		m_fNetFatalErrorCurrentY = 0.0

PROC FORMAT_TEXT(FLOAT fXMin, FLOAT fXMax, eTextJustification eJustification = FONT_CENTRE)
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(0, 0, 0, 255) 
	SET_TEXT_JUSTIFICATION(eJustification)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_WRAP(fXMin,  fXMax)
ENDPROC

PROC UPDATE_DISPLAY_STATE(EVENT_DISPLAY_STATE eNewState)
	m_eDisplayState = eNewState
	SWITCH eNewState
	CASE EDS_HIDDEN
		m_fCurrentY = 1.0
	BREAK
	ENDSWITCH
ENDPROC
PROC UPDATE_NET_FATAL_ERROR_STATE(EVENT_DISPLAY_STATE eNewState)
	m_eNetFatalErrorState = eNewState
	SWITCH eNewState
	CASE EDS_HIDDEN
		m_fNetFatalErrorCurrentY = 0.0
	BREAK
	ENDSWITCH
ENDPROC

//Max size = player name length OR 19
PROC AMMEND_PLAYER_NAME_TO_LABEL(TEXT_LABEL_63& txt, PLAYER_INDEX player)
	IF player = PLAYER_ID()
		txt += "local player"		
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(g_netFatalError.data.txt23FatalPlayer)
			txt += g_netFatalError.data.txt23FatalPlayer
		ELSE
			txt += "unknown player (" 
			txt += NATIVE_TO_INT(player) 
			txt += ")"
		ENDIF
	ENDIF
ENDPROC

//SEE ERROR_INFO in text files
PROC DISPLAY_ERROR_INFO(FLOAT DisplayAtX, FLOAT DisplayAtY, EVENT_CACHE_ITEM& error)
	TEXT_LABEL_63 data	//= error.iFrame
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ERROR_INFO_0")
		data = error.iFrame
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(data)
		
		data = GET_EVENT_NAME(INT_TO_ENUM(EVENT_NAMES, error.iEventName))
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(data)
		
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL_HASH_KEY(error.iScriptNameHash)
		
		data = error.iDebugCount
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(data)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC


//Not a particularly elegant solution, most was c&p from existing draw proc.
PROC UPDATE_NET_FATAL_ERROR_DISPLAY()
	//Nothing to display if there isn't an event
	IF NOT g_bReceivedNetFatalError 
		UPDATE_NET_FATAL_ERROR_STATE(EDS_HIDDEN)	
		EXIT
	ELIF m_eNetFatalErrorState = EDS_HIDDEN
		UPDATE_NET_FATAL_ERROR_STATE(EDS_FLOAT_IN)
	ELIF m_bCanCloseNetFatalError
	AND IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_CTRL, "CTRL+L : Clear net fatal error")
		g_bReceivedNetFatalError = FALSE
		EXIT	
	ENDIF

	FLOAT fPosX	= (1 - cfELEMENT_WIDTH - (cfPADDING_X * 2))/2
	FLOAT fWidth = cfELEMENT_WIDTH + (cfPADDING_X * 2)
	FLOAT fHeight = cfHEADER_Y + ((cfELEMENT_HEIGHT + cfPADDING_Y) * 2)
	FLOAT fTargetPosY = fHeight
	FLOAT fHalfHeight = fHeight/2
	FLOAT fElementY
	
	SWITCH m_eNetFatalErrorState
		CASE EDS_FLOAT_IN
			m_fNetFatalErrorCurrentY += cfFLOAT_SPEED * GET_FRAME_TIME()
			IF m_fNetFatalErrorCurrentY >= fTargetPosY
				m_fNetFatalErrorCurrentY = fTargetPosY
				UPDATE_NET_FATAL_ERROR_STATE(EDS_SHOWING)
			ENDIF
		BREAK
		CASE EDS_HIDDEN EXIT
	ENDSWITCH

	/* Draw background */
	DRAW_RECT(fPosX + (fWidth / 2) , m_fNetFatalErrorCurrentY - fHalfHeight, fWidth, fHeight, 250, 110, 110, 210)
	
	/* Draw title */
	TEXT_LABEL_63 txtOutput = "<<<<< NET FATAL SCRIPT ERROR " 
	IF m_bCanCloseNetFatalError
		txtOutput += "| CTRL+L TO CLEAR "
	ENDIF
	txtOutput += ">>>>>"
	FORMAT_TEXT(fPosX + cfPADDING_X, fPosX + cfPADDING_X + cfELEMENT_WIDTH)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + cfPADDING_X + (cfELEMENT_WIDTH /2), m_fNetFatalErrorCurrentY - fHeight + cfPADDING_Y, "STRING", txtOutput)
	
		
	txtOutput = "["
	txtOutput += g_netFatalError.iFrameReceived
	txtOutput += "] "
	txtOutput += GET_EVENT_NAME(g_netFatalError.data.eEvent)
	txtOutput += " in "
	txtOutput += g_netFatalError.data.txtScriptName
	txtOutput += " for"
	
	fElementY = m_fNetFatalErrorCurrentY - fHeight + cfHEADER_Y
	FLOAT fBGMin = 20
	FLOAT fBGMax = 140
	INT iBGHighlight = ROUND(LERP_FLOAT(fBGMin, fBGMax, (SIN(TO_FLOAT(GET_GAME_TIMER())* 0.25) /2.0) + 1.0))
	DRAW_RECT(fPosX + (fWidth / 2.0), fElementY + (cfPADDING_Y*2), cfELEMENT_WIDTH, fHeight - cfHEADER_Y, 255, iBGHighlight, iBGHighlight, 200)
	
	FORMAT_TEXT(fPosX + cfPADDING_X, fPosX + cfPADDING_X + cfELEMENT_WIDTH)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + (fWidth / 2.0), fElementY - cfELEMENT_HEIGHT /2, "STRING", txtOutput)
	
	txtOutput = ""
	AMMEND_PLAYER_NAME_TO_LABEL(txtOutput, g_netFatalError.iPlayerIndex)
	txtOutput += " on frame "
	txtOutput += g_netFatalError.data.iFrame
	txtOutput += ". Please restart your game."
	fElementY += cfELEMENT_HEIGHT + cfPADDING_Y
	FORMAT_TEXT(fPosX + cfPADDING_X, fPosX + cfPADDING_X + cfELEMENT_WIDTH)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + (fWidth / 2.0), fElementY - cfELEMENT_HEIGHT /2, "STRING", txtOutput)
	
ENDPROC

PROC HIDE_POPUP()
	m_fCurrentY = 1
	UPDATE_DISPLAY_STATE(EDS_HIDDEN)
ENDPROC

//Draw the debug display
PROC UPDATE_DEBUG_DISPLAY()		
	//Close if cache is cleared
	IF g_eventCache.iLength <= 0
		m_iFullCacheWriteAttemps = 0
		m_fCurrentY = 1
		m_iDisplayTimer = -1
		UPDATE_DISPLAY_STATE(EDS_HIDDEN)
		EXIT
	ENDIF
	
	INT iCurrentTime = GET_GAME_TIMER()
	FLOAT fPosX	= (1 - cfELEMENT_WIDTH - (cfPADDING_X * 2))/2
	FLOAT fWidth = cfELEMENT_WIDTH + (cfPADDING_X * 2)
	FLOAT fHeight = cfHEADER_Y + ((cfELEMENT_HEIGHT + cfPADDING_Y) * g_eventCache.iLength) + PICK_FLOAT(m_iFullCacheWriteAttemps > 0, (cfELEMENT_HEIGHT + cfPADDING_Y), 0.0)
	FLOAT fTargetPosY = 1 - fHeight
	FLOAT fElementY

	
	SWITCH m_eDisplayState
		CASE EDS_FLOAT_IN
			m_fCurrentY -= cfFLOAT_SPEED * GET_FRAME_TIME()
			IF m_fCurrentY <= fTargetPosY
				m_fCurrentY = fTargetPosY
				m_iDisplayTimer = GET_GAME_TIMER()
				UPDATE_DISPLAY_STATE(EDS_SHOWING)
			ENDIF
		BREAK
		CASE EDS_SHOWING
			IF iCurrentTime - m_iDisplayTimer >= ciDISPLAY_DURATION
				m_fCurrentY = fTargetPosY //Incase we jumped to this state
				UPDATE_DISPLAY_STATE(EDS_FLOAT_OUT)
			ENDIF
		BREAK
		CASE EDS_FLOAT_OUT
			m_fCurrentY += cfFLOAT_SPEED * GET_FRAME_TIME()
			IF m_fCurrentY >= 1 
				m_fCurrentY = 1
				UPDATE_DISPLAY_STATE(EDS_HIDDEN)
				EXIT
			ENDIF
		BREAK
		CASE EDS_HIDDEN EXIT
	ENDSWITCH
			
	/* Draw background */
	DRAW_RECT(fPosX + (fWidth / 2) , m_fCurrentY + (fHeight /2), fWidth, fHeight, 250, 110, 110, 210)

	/* Draw title */
	FORMAT_TEXT(fPosX + cfPADDING_X, fPosX + cfPADDING_X + cfELEMENT_WIDTH)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + cfPADDING_X + (cfELEMENT_WIDTH /2), m_fCurrentY + cfPADDING_Y, "STRING", "<<<<< FATAL SCRIPT ERROR >>>>>")
	
	/* Draw timer */
	TEXT_LABEL_63 sOutputString = PICK_INT(m_eDisplayState != EDS_SHOWING, 0, CEIL((ciDISPLAY_DURATION - (iCurrentTime - m_iDisplayTimer))/ 1000.0))
	TEXT_LABEL_31 txtName
	FORMAT_TEXT(fPosX, m_fCurrentY + cfPADDING_X * 1.5)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + cfPADDING_X * 1.5, m_fCurrentY + cfPADDING_Y, "STRING", sOutputString)
	
	/* Draw array elements */
	FLOAT fBGMin = 20
	FLOAT fBGMax = 140
	INT iBGHighlight = ROUND(LERP_FLOAT(fBGMin, fBGMax, (SIN(TO_FLOAT(GET_GAME_TIMER())* 0.25) /2.0) + 1.0))
	INT iCacheIndex	
	REPEAT g_eventCache.iLength iCacheIndex
		sOutputString = "["
		sOutputString += g_eventCache.events[iCacheIndex].iFrame
		sOutputString += "] "
		sOutputString += GET_EVENT_NAME(INT_TO_ENUM(EVENT_NAMES, g_eventCache.events[iCacheIndex].iEventName))
		sOutputString += " in "
		txtName = g_eventCache.events[iCacheIndex].iScriptNameHash
		txtName = GET_STRING_FROM_TEXT_FILE(txtName)
		sOutputString += txtName 
		sOutputString += " ("
		sOutputString += g_eventCache.events[iCacheIndex].iDebugCount
		sOutputString += ")"
	
		fElementY = m_fCurrentY + cfHEADER_Y + ((cfELEMENT_HEIGHT + cfPADDING_Y) * iCacheIndex)
		FORMAT_TEXT(fPosX - cfELEMENT_WIDTH /2, fPosX  + cfELEMENT_WIDTH/2)
		IF iCacheIndex = m_iHighlightIndex
			DRAW_RECT(fPosX + (fWidth / 2.0), fElementY, cfELEMENT_WIDTH, cfELEMENT_HEIGHT, 255, iBGHighlight, iBGHighlight, 200)
		ELSE
			DRAW_RECT(fPosX + (fWidth / 2.0), fElementY, cfELEMENT_WIDTH, cfELEMENT_HEIGHT, 255, 60, 60, 200)
		ENDIF
		DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + (fWidth / 2.0), fElementY - cfELEMENT_HEIGHT /2, "STRING", sOutputString)
	ENDREPEAT
	IF m_iFullCacheWriteAttemps > 0
		fElementY = m_fCurrentY + cfHEADER_Y + ((cfELEMENT_HEIGHT + cfPADDING_Y) * g_eventCache.iLength)
		sOutputString = "Full cache. Attepts to write to full cache: "
		sOutputString += m_iFullCacheWriteAttemps
		FORMAT_TEXT(fPosX - cfELEMENT_WIDTH /2, fPosX  + cfELEMENT_WIDTH/2)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fPosX + (fWidth / 2.0), fElementY, "STRING", sOutputString)
	ENDIF
ENDPROC
#ENDIF

PROC INTIALISE_EVENT_CACHE_ITEM(EVENT_CACHE_ITEM& cacheItem)
	cacheItem.iEventName 		= -1
	cacheItem.iFrame 			= -1
	cacheItem.iScriptNameHash 	= -1
ENDPROC

/// PURPOSE:
///    Get the name hash of the script that has caused the event.
PROC GET_AND_STORE_SCRIPT_ID(SCRIPT_EVENT_QUEUES eQueue, INT iQueueIndex, EVENT_NAMES eEvent, EVENT_CACHE_ITEM& item)
	IF eEvent = EVENT_ERRORS_UNKNOWN_ERRORS
	OR eEvent = EVENT_ERRORS_ARRAY_OVERFLOW
	OR eEvent = EVENT_ERRORS_INSTRUCTION_LIMIT
	OR eEvent = EVENT_ERRORS_STACK_OVERFLOW
		STRUCT_SCRIPT_ERROR sErrorData
		IF GET_EVENT_DATA(eQueue, iQueueIndex, sErrorData, SIZE_OF(sErrorData))
			item.iScriptNameHash = sErrorData.m_scriptID
			PRINTLN("[error_listener] item.iScriptNameHash = sErrorData.m_scriptID = ", sErrorData.m_scriptID)
		ENDIF
	ENDIF
	/* Add the ability to get the script ID from other events here...*/
ENDPROC

#IF IS_DEBUG_BUILD
PROC TRIGGER_BROADCAST_NET_FATAL_ERROR(INT iScripHash, EVENT_NAMES eEvent)
	IF g_bReceivedNetFatalError
		EXIT //Don't bother broadcasting if we already have one
	ENDIF

	text_label_31 txtHash = iScripHash
	g_netFatalError.data.txtScriptName = GET_STRING_FROM_TEXT_FILE(txtHash)
	g_netFatalError.data.eEvent 	= eEvent
	g_netFatalError.data.iFrame 	= GET_FRAME_COUNT() - 1
	g_netFatalError.iPlayerIndex 	= PLAYER_ID()
	g_netFatalError.iFrameReceived 	= GET_FRAME_COUNT() - 1
	g_bBroadcastNetFatalError 		= TRUE //Trigger broadcast
	g_bReceivedNetFatalError 		= TRUE //Won't send to self and now we have stuff stored in global already
	PRINTLN("[error_listener] Broadcasting net fatal error ", GET_EVENT_NAME_NOCAPS(eEvent) ," in " ,g_netFatalError.data.txtScriptName, " from player ", GET_PLAYER_NAME(PLAYER_ID()), " (", NATIVE_TO_INT(PLAYER_ID()), ")")
ENDPROC

FUNC BOOL THROW_ERROR(ERROR_THROWER_ERROR eType)
	INT iScriptHash = HASH("error_thrower")
	REQUEST_SCRIPT_WITH_NAME_HASH(iScriptHash)
	IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(iScriptHash)
		ERROR_THROWER_ARGS args
		args.eError = eType
		START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(iScriptHash, args, SIZE_OF(args), DEFAULT_STACK_SIZE)
		SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(iScriptHash)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_WIDGETS()
	IF m_bClearNetFatalError
		m_bClearNetFatalError = FALSE
		g_bReceivedNetFatalError = FALSE
		UPDATE_NET_FATAL_ERROR_STATE(EDS_HIDDEN)
	ENDIF
	IF m_bClearEventCache
		m_bClearEventCache = FALSE
		CLEAR_EVENT_CACHE()
	ENDIF
	
	IF m_bArrayOverrun
	AND THROW_ERROR(ETE_ARRAY_OVERRUN)
		m_bArrayOverrun = FALSE
	ENDIF
	
	IF m_bInsLimit
	AND THROW_ERROR(ETE_INS_LIMIT)
		m_bInsLimit = FALSE
	ENDIF
	
	IF m_bStackOverflow
	AND THROW_ERROR(ETE_STACK_OVERFLOW)
		m_bStackOverflow = FALSE
	ENDIF
ENDPROC

#ENDIF //IS_DEBUG_BUILD

PROC HANDLE_EVENT(EVENT_CACHE_ITEM& event)
	EVENT_NAMES eEventName = INT_TO_ENUM(EVENT_NAMES, event.iEventName)
	IF IS_PLAYER_IN_CORONA()
	AND IS_EVENT_A_FATAL_ERROR(eEventName)
	AND eEventName <> EVENT_ERRORS_UNKNOWN_ERRORS // Native trace can trigger this so we should risk ignoring it
	AND NETWORK_CAN_BAIL()
		PRINTLN("[error_listener] HANDLE_EVENT - NETWORK_BAIL : Event name = ", event.iEventName, " Script name hash = ", event.iScriptNameHash," IS_PLAYER_IN_CORONA() = TRUE ")
		NETWORK_BAIL(ENUM_TO_INT(BAIL_FROM_SCRIPT), BAIL_FROM_SCRIPT_PARAM1_FATAL_ERROR)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cache any events that pass SHOULD_CACHE_THIS_EVENT() from 
///    SCRIPT_EVENT_QUEUE_ERRORS that have been broadcast this frame
///    into g_eventCache.
PROC UPDATE_EVENT_CACHE()
	INT iEventIndexInQueue
	EVENT_NAMES eEvent
	EVENT_CACHE_ITEM eventData
	INT iDuplicateIndex
 	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_ERRORS) iEventIndexInQueue
		eEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_ERRORS, iEventIndexInQueue)

		IF SHOULD_CACHE_THIS_EVENT(eEvent)
			INTIALISE_EVENT_CACHE_ITEM(eventData)
			GET_AND_STORE_SCRIPT_ID(SCRIPT_EVENT_QUEUE_ERRORS,iEventIndexInQueue, eEvent, eventData)
			eventData.iFrame 		= GET_FRAME_COUNT() - 1 //We will have caught it on the frame after
			eventData.iEventName 	= ENUM_TO_INT(eEvent)
			
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[error_listener] Event.iEventName      = ", eventData.iEventName)
			PRINTLN_FINAL("[error_listener] Event.iScriptNameHash = ", eventData.iScriptNameHash)
			PRINTLN_FINAL("[error_listener] Event.iFrame          = ", eventData.iFrame)
			#ENDIF //USE_FINAL_PRINTS
			PRINTLN("[error_listener] Event.iEventName      = ", eventData.iEventName)
			PRINTLN("[error_listener] Event.iScriptNameHash = ", eventData.iScriptNameHash)
			PRINTLN("[error_listener] Event.iFrame          = ", eventData.iFrame)
			
			IF NOT IS_EVENT_ITEM_DUPLICATE(eventData, iDuplicateIndex)
				#IF IS_DEBUG_BUILD 	
				IF
				#ENDIF //IS DEBUG BUILD
				
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[error_listener] Event not a duplicate. Attempting to add to cache...")
				#ENDIF //USE_FINAL_PRINTS
				
				PUSH_EVENT_INTO_CACHE(eventData)
						
			#IF IS_DEBUG_BUILD				
					PRINTLN("[error_listener] Event successfully added to cache")
					m_iHighlightIndex = g_eventCache.iLength - 1
					g_eventCache.events[g_eventCache.iLength - 1].iDebugCount = 1
				ELSE
					PRINTLN("[error_listener] Cache is full. Full write attempts = ", m_iFullCacheWriteAttemps)
					m_iFullCacheWriteAttemps++
				ENDIF
				
				//Consider broadcasting event to other players
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND IS_EVENT_A_FATAL_ERROR(eEvent)
				AND IS_GAME_RESET_REQUIRED_ON_FATAL_ERROR_FOR_SCRIPT(eventData.iScriptNameHash)
				AND NOT g_bReceivedNetFatalError
					TRIGGER_BROADCAST_NET_FATAL_ERROR(eventData.iScriptNameHash, eEvent)
					UPDATE_NET_FATAL_ERROR_STATE(EDS_FLOAT_IN)
				ENDIF
				
				UPDATE_DISPLAY_STATE(EDS_FLOAT_IN)
				PRINTLN("[error_listener] Adding event to cache. Event: ", GET_EVENT_NAME(INT_TO_ENUM(EVENT_NAMES, eventData.iEventName)), " ScriptNameHash: ", eventData.iScriptNameHash)
			ELSE
				UPDATE_DISPLAY_STATE(EDS_FLOAT_IN)
				g_eventCache.events[iDuplicateIndex].iDebugCount++
				m_iHighlightIndex = iDuplicateIndex
				PRINTLN("[error_listener] Event duplicate. Event: ", GET_EVENT_NAME(INT_TO_ENUM(EVENT_NAMES, eventData.iEventName)), " ScriptNameHash: ", eventData.iScriptNameHash, " Count: ", g_eventCache.events[iDuplicateIndex].iDebugCount)
			#ENDIF //IS_DEBUG_BUILD
			ENDIF

			HANDLE_EVENT(eventData)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SCRIPT_UPDATE()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
 	ADD_SCRIPT_PROFILE_MARKER("Update start")
	#ENDIF //SCRIPT_PROFILER_ACTIVE
	
	UPDATE_DEBUG_STRINGS_WITH_NEW_SCRIPTS()
	#IF SCRIPT_PROFILER_ACTIVE 
    ADD_SCRIPT_PROFILE_MARKER("UPDATE_DEBUG_STRINGS_WITH_NEW_SCRIPTS")
	#ENDIF //SCRIPT_PROFILER_ACTIVE
	
	MAINTAIN_WIDGETS()
	#IF SCRIPT_PROFILER_ACTIVE 
    ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_WIDGETS")
    #ENDIF //SCRIPT_PROFILER_ACTIVE
	#ENDIF //IS_DEBUG_BUILD
	
	UPDATE_EVENT_CACHE()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
    ADD_SCRIPT_PROFILE_MARKER("UPDATE_EVENT_CACHE - non debug")
    #ENDIF //SCRIPT_PROFILER_ACTIVE
	
	IF m_bAllowDebugDisplay
		UPDATE_NET_FATAL_ERROR_DISPLAY()
		UPDATE_DEBUG_DISPLAY()
	ENDIF
	
	#IF SCRIPT_PROFILER_ACTIVE 
    ADD_SCRIPT_PROFILE_MARKER("Update end")
    #ENDIF //SCRIPT_PROFILER_ACTIVE
	#ENDIF //IS_DEBUG_BUILD
ENDPROC

FUNC BOOL SHOULD_SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
	IF m_bTerminateScript
		PRINTLN("[error_listener] SHOULD_SCRIPT_CLEANUP : (RAG) m_bTerminateScript : TRUE")
		RETURN TRUE
	ENDIF
	#ENDIF //IS_DEBUG_BUILD
	RETURN FALSE
ENDFUNC

PROC SCRIPT_INIT()
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	#IF IS_DEBUG_BUILD
	m_bCanCloseNetFatalError = GET_COMMANDLINE_PARAM_EXISTS("sc_DevErrorListener")
	m_bAllowDebugDisplay = NOT GET_COMMANDLINE_PARAM_EXISTS("sc_HideErrorListener")
	START_WIDGET_GROUP("Error listener")
		#IF SCRIPT_PROFILER_ACTIVE 
		CREATE_SCRIPT_PROFILER_WIDGET() 
		#ENDIF //SCRIPT_PROFILER_ACTIVE
		ADD_WIDGET_BOOL("Full logging", m_bExtraDebug)
		ADD_WIDGET_BOOL("Terminate (relaunch)", m_bTerminateScript)
		ADD_WIDGET_BOOL("Clear event cache", m_bClearEventCache)
		ADD_WIDGET_BOOL("Clear net fatal error", m_bClearNetFatalError)
		ADD_WIDGET_BOOL("CTRL+L clears net fatal error", m_bCanCloseNetFatalError)
		ADD_WIDGET_BOOL("Allow all warnings", m_bAllowDebugDisplay)
		
		ADD_WIDGET_BOOL("Force error: Array overrun", m_bArrayOverrun)
		ADD_WIDGET_BOOL("Force error: Instruction limit", m_bInsLimit)
		ADD_WIDGET_BOOL("Force error: Stack overflow", m_bStackOverflow)
	STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC

SCRIPT
	SCRIPT_INIT()
	
	WHILE TRUE
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF //SCRIPT_PROFILER_ACTIVE
		#ENDIF //IS_DEBUG_BUILD
		
		IF SHOULD_SCRIPT_CLEANUP()
			EXIT
		ENDIF
		SCRIPT_UPDATE()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF //SCRIPT_PROFILER_ACTIVE
		#ENDIF //IS_DEBUG_BUILD
	ENDWHILE
ENDSCRIPT
