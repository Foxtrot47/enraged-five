

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
using "cellphone_public.sch"

#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

	USING "sceneBuilder.sch"		
	
	SCRIPT
		DISABLE_CELLPHONE(true)
		initWidgets()
	
		WHILE (TRUE	)
			sceneBuilder()
			WAIT(0)
		ENDWHILE
	ENDSCRIPT

#ENDIF

