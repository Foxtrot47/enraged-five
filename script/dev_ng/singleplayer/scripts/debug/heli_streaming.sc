USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
SCRIPT 
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_misc.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_clock.sch"
USING "selector_public.sch"

CONST_INT numlocation 1
//CONST_INT stat_count 5000
INT index, loaded_low, loaded_med, loaded_hi, required_low, required_med, required_hi, count
INT total_loaded_low, total_loaded_med, total_loaded_hi, total_required_low, total_required_med, total_required_hi
//INT last_loaded_low, last_loaded_med, last_loaded_hi, last_required_low, last_required_med, last_required_hi
INT average_loaded_low, average_loaded_med, average_loaded_hi, average_required_low, average_required_med, average_required_hi
INT peak_loaded_low, peak_loaded_med, peak_loaded_hi, peak_required_low, peak_required_med, peak_required_hi
INT not_loaded_low, not_loaded_med, not_loaded_hi

VECTOR FPSlocation[1]
//STRING FPSlocationName[1]
//VECTOR v_car_destination[4]
FLOAT car_heading[numlocation]
//FLOAT camera_pan_mod
//FLOAT camera_pitch_mod
VEHICLE_INDEX veh_car
MODEL_NAMES model_car
BOOL SWITCH_VEHS, have_stats_started
BOOL ANOTHER_ITERATION
VECTOR coords

ENUM FPS_TEST_STAGE
	SETUP,
	WARP_TO_LOCATION,
	CREATE_CAR,
	CAR_DRIVE,
	CLEANUP_ZONE,
	STREAMING_RESULTS
ENDENUM
FPS_TEST_STAGE FPSlocationStage = SETUP

PROC CLEANUP_SCRIPT()
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (FALSE)
	PRINTLN("HELI STREAMING TERMINATING")
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF
	
	PRINTLN("HELI STREAMING STARTING")
	
	index = 0
	
	WHILE TRUE
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH FPSlocationStage
					
					CASE SETUP
						
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						FPSlocationStage = WARP_TO_LOCATION

						SHUTDOWN_LOADING_SCREEN()
						// Fade the screen in
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF

						AUTOMATED_TEST_BEGIN()
						
					BREAK
					
					CASE WARP_TO_LOCATION
						index = 0
						count = 0
						total_loaded_low = 0
						total_loaded_med = 0
						total_loaded_hi = 0
						total_required_low = 0
						total_required_med = 0
						total_required_hi = 0
						peak_loaded_low = 0
						peak_loaded_med = 0
						peak_loaded_hi = 0
						peak_required_low = 0
						peak_required_med = 0
						peak_required_hi = 0
						
//						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "heli_streaming")
//							REMOVE_VEHICLE_RECORDING(1, "heli_streaming")
//						ENDIF
//						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "jet_streaming")
//							REMOVE_VEHICLE_RECORDING(1, "jet_streaming")
//						ENDIF
						
						FPSlocation[0] = <<1367.1207, -2075.1047, 300.0>>	// HELI

//						SET_MAX_WANTED_LEVEL(0)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), FPSlocation[index])
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						

						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
						SETTIMERA(0)
						FPSlocationStage = CREATE_CAR
						
					BREAK				

					CASE CREATE_CAR
					
						PRINTSTRING("...Create car")
						PRINTNL()
						
						IF NOT DOES_ENTITY_EXIST(veh_car)
							
							IF NOT SWITCH_VEHS
								model_car = FROGGER
								REQUEST_MODEL(model_car)
							ELSE
								model_car = LAZER
								REQUEST_MODEL(model_car)						
							ENDIF
							
							IF HAS_MODEL_LOADED(model_car)
								CLEAR_AREA(FPSlocation[index], 20, TRUE)
								veh_car = CREATE_VEHICLE(model_car, FPSlocation[index], car_heading[index])
							ENDIF
							
						ELSE
							IF IS_VEHICLE_DRIVEABLE(veh_car)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
								SET_ENTITY_COORDS(veh_car, FPSlocation[index])
								SET_ENTITY_HEADING(veh_car, car_heading[index])
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								SET_ENTITY_INVINCIBLE(veh_car, TRUE)
								SET_VEHICLE_ENGINE_ON(veh_car, TRUE, TRUE)

								WAIT(0)
								
								coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
								CLEAR_AREA(coords, 2000, TRUE)

								PRINTLN("Waiting for scene to load...")

								NEW_LOAD_SCENE_START_SPHERE(coords, 4500.0)
								WAIT(0)

								WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
									WAIT(0)
								ENDWHILE
								
								NEW_LOAD_SCENE_STOP()

								PRINTLN("Scene loaded, let's wait for the rest...")
								
								LOAD_ALL_OBJECTS_NOW()
								
								INSTANTLY_FILL_PED_POPULATION()
								INSTANTLY_FILL_VEHICLE_POPULATION()

								PRINTLN("Waiting for peds and vehicles...")
								
								SETTIMERA(0)
								WHILE TIMERA() < 3000
									WAIT(0)
								ENDWHILE

								IF NOT SWITCH_VEHS
									REQUEST_VEHICLE_RECORDING(1, "heli_streaming")
									WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "heli_streaming")
										WAIT(0)
									ENDWHILE
									IF IS_VEHICLE_DRIVEABLE(veh_car)
										START_PLAYBACK_RECORDED_VEHICLE(veh_car, 1, "heli_streaming")
									ENDIF
									
								ELSE
									REQUEST_VEHICLE_RECORDING(1, "jet_streaming")
									WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "jet_streaming")
										WAIT(0)
									ENDWHILE
									IF IS_VEHICLE_DRIVEABLE(veh_car)
										START_PLAYBACK_RECORDED_VEHICLE(veh_car, 1, "jet_streaming")
									ENDIF
								ENDIF
								
								WAIT(0)
								

								PRINTLN("Let's go!")
								
								SET_CLOCK_TIME(17, 0, 0)
						
								SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
							
								IF NOT have_stats_started
									DBG_SET_MAPDATA_STATS(TRUE)
									PRINTLN("DBG_SET_MAPDATA_STATS(TRUE)")
									have_stats_started = TRUE
								ENDIF
								FPSlocationStage = CAR_DRIVE
								ANOTHER_ITERATION = TRUE
								AUTOMATED_TEST_START_ITERATION()
							ENDIF
						ENDIF
//					REQUEST_VEHICLE_RECORDING(INT FileNumber, STRING pRecordingName)
//					HAS_VEHICLE_RECORDING_BEEN_LOADED(INT FileNumber, STRING pRecordingName)
//					START_PLAYBACK_RECORDED_VEHICLE
//					IS_PLAYBACK_GOING_ON_FOR_VEHICLE(VEHICLE_INDEX VehicleIndex)
					
					BREAK
			
					CASE CAR_DRIVE
					
						IF IS_VEHICLE_DRIVEABLE(veh_car) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_car)
							SETTIMERA(0)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
							ANOTHER_ITERATION = AUTOMATED_TEST_NEXT_ITERATION()
							SETTIMERB(0)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
						//IMAPS LOADED
						loaded_low = DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_LOW)
						total_loaded_low = total_loaded_low + loaded_low //sum of all
						IF loaded_low > peak_loaded_low //Get peak
							peak_loaded_low = loaded_low
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_LOW) = ", loaded_low, " PEAK LOW = ", peak_loaded_low, " Sum of all(DEBUG) = ", total_loaded_low)

						loaded_med = DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_MED)
						total_loaded_med = total_loaded_med + loaded_med //sum of all
						IF loaded_med > peak_loaded_med //Get peak
							peak_loaded_med = loaded_med
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_MED) = ", loaded_med, " PEAK MED = ", peak_loaded_med, " Sum of all(DEBUG) = ", total_loaded_med)
	
						loaded_hi = DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_HI)
						total_loaded_hi = total_loaded_hi + loaded_hi //sum of all
						IF loaded_hi > peak_loaded_hi //Get peak
							peak_loaded_hi = loaded_hi
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_LOADED(MAPDATA_HI) = ", loaded_hi, " PEAK HI = ", peak_loaded_hi, " Sum of all(DEBUG) = ", total_loaded_hi)			
			
			
						//IMAPS REQUIRED
						required_low = DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_LOW)
						total_required_low = total_required_low + required_low //sum of all
						IF required_low > peak_required_low //Get peak
							peak_required_low = required_low
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_LOW) = ", required_low, " PEAK LOW = ", peak_required_low, " Sum of all(DEBUG) = ", total_required_low)

						required_med = DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_MED)
						total_required_med = total_required_med + required_med //sum of all
						IF required_med > peak_required_med //Get peak
							peak_required_med = required_med
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_MED) = ", required_med, " PEAK MED = ", peak_required_med, " Sum of all(DEBUG) = ", total_required_med)
						
						required_hi = DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_HI)
						total_required_hi = total_required_hi + required_hi //sum of all
						IF required_hi > peak_required_hi //Get peak
							peak_required_hi = required_hi
						ENDIF
//						PRINTLN("DBG_GET_NUM_MAPDATA_REQUIRED(MAPDATA_HI) = ", required_hi, " PEAK HI = ", peak_required_hi, " Sum of all(DEBUG) = ", total_required_hi)
			
						count ++
//						PRINTLN("number of times stats run = ", count)
											
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) //Debug skip camera rotate
							SETTIMERA(5000)
							FPSlocationStage = WARP_TO_LOCATION
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) //Debug skip camera rotate
							SETTIMERA(5000)
							IF NOT SWITCH_VEHS
								SWITCH_VEHS = TRUE
							ELSE
								SWITCH_VEHS = FALSE
							ENDIF
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
							FPSlocationStage = WARP_TO_LOCATION
						ENDIF
						
					BREAK
					
					CASE CLEANUP_ZONE

						IF have_stats_started
							average_loaded_low = total_loaded_low / count 
							PRINTLN("AVERAGE LOADED LOW = ", average_loaded_low, " PEAK LOADED LOW = ", peak_loaded_low)
							average_loaded_med = total_loaded_med / count 
							PRINTLN("AVERAGE LOADED MED = ", average_loaded_med, " PEAK LOADED MED = ", peak_loaded_med)
							average_loaded_hi = total_loaded_hi / count 
							PRINTLN("AVERAGE LOADED HI = ", average_loaded_hi, " PEAK LOADED HI = ", peak_loaded_hi)
							
							average_required_low = total_required_low / count 
							PRINTLN("AVERAGE REQUIRED LOW = ", average_required_low, " PEAK REQUIRED LOW = ", peak_required_low)
							average_required_med = total_required_med / count 
							PRINTLN("AVERAGE REQUIRED MED = ", average_required_med, " PEAK REQUIRED MED = ", peak_required_med)
							average_required_hi = total_required_hi / count 
							PRINTLN("AVERAGE REQUIRED HI = ", average_required_hi, " PEAK REQUIRED HI = ", peak_required_hi)

							not_loaded_low = average_required_low - average_loaded_low 
							PRINTLN("NOT LOADED LOW = ", not_loaded_low)
							not_loaded_med = average_required_med - average_loaded_med 
							PRINTLN("NOT LOADED MED = ", not_loaded_med)
							not_loaded_hi = average_required_hi - average_loaded_hi 
							PRINTLN("NOT LOADED HI = ", not_loaded_hi)
							SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)

							DBG_SET_MAPDATA_STATS(FALSE)
							PRINTLN("DBG_SET_MAPDATA_STATS(FALSE)")
							have_stats_started = FALSE
						ENDIF

//						DISPLAY_TEXT_WITH_LITERAL_STRING(0.2, 0.2, "STRING", "AVERAGE LOADED LOW")
						DRAW_DEBUG_TEXT_2D("AVERAGE LOADED LOW", <<0.1, 0.1, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_loaded_low), <<0.25, 0.1, 0>> )
						DRAW_DEBUG_TEXT_2D("PEAK LOADED LOW", <<0.1, 0.12, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_loaded_low), <<0.25, 0.12, 0>> )
							
						DRAW_DEBUG_TEXT_2D("AVERAGE LOADED MED", <<0.1, 0.14, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_loaded_med), <<0.25, 0.14, 0>> )
						DRAW_DEBUG_TEXT_2D("PEAK LOADED MED", <<0.1, 0.16, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_loaded_med), <<0.25, 0.16, 0>> )

						DRAW_DEBUG_TEXT_2D("AVERAGE LOADED HI", <<0.1, 0.18, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_loaded_hi), <<0.25, 0.18, 0>> )
						DRAW_DEBUG_TEXT_2D("PEAK LOADED HI", <<0.1, 0.2, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_loaded_hi), <<0.25, 0.2, 0>> )


						DRAW_DEBUG_TEXT_2D("AVERAGE REQUIRED LOW", <<0.1, 0.3, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_required_low), <<0.25, 0.3, 0>> )						
						DRAW_DEBUG_TEXT_2D("PEAK REQUIRED LOW", <<0.1, 0.32, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_required_low), <<0.25, 0.32, 0>> )

						DRAW_DEBUG_TEXT_2D("AVERAGE REQUIRED MED", <<0.1, 0.34, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_required_med), <<0.25, 0.34, 0>> )						
						DRAW_DEBUG_TEXT_2D("PEAK REQUIRED MED", <<0.1, 0.36, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_required_med), <<0.25, 0.36, 0>> )
						
						DRAW_DEBUG_TEXT_2D("AVERAGE REQUIRED HI", <<0.1, 0.38, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(average_required_hi), <<0.25, 0.38, 0>> )						
						DRAW_DEBUG_TEXT_2D("PEAK REQUIRED HI", <<0.1, 0.4, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(peak_required_hi), <<0.25, 0.4, 0>> )


						DRAW_DEBUG_TEXT_2D("NOT LOADED LOW", <<0.1, 0.5, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(not_loaded_low), <<0.25, 0.5, 0>> )						
						DRAW_DEBUG_TEXT_2D("NOT LOADED MED", <<0.1, 0.52, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(not_loaded_med), <<0.25, 0.52, 0>> )						
						DRAW_DEBUG_TEXT_2D("NOT LOADED HI", <<0.1, 0.54, 0>> )
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(not_loaded_hi), <<0.25, 0.54, 0>> )						
						
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
							index = 0
							FPSlocationStage = WARP_TO_LOCATION
						ENDIF
						
						IF TIMERB() > 3000
							IF ANOTHER_ITERATION
								// Time for another one.
								index = 0
								FPSlocationStage = WARP_TO_LOCATION
							ENDIF
						ENDIF
								
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) //Debug skip camera rotate
							SETTIMERA(5000)
							IF NOT SWITCH_VEHS
								SWITCH_VEHS = TRUE
							ELSE
								SWITCH_VEHS = FALSE
							ENDIF
							SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
							FPSlocationStage = WARP_TO_LOCATION
						ENDIF
						
					BREAK
					
					CASE STREAMING_RESULTS
						
						CLEANUP_SCRIPT()
						
					BREAK
					
				ENDSWITCH

				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Press S to switch tests. Press P to restart test. Press F to fail", 1, 1)
				
			ENDIF
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			CLEANUP_SCRIPT()
		ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF

