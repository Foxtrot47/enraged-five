USING "buildtype.sch"
USING "rage_builtins.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF





// Only include in debug mode
#IF IS_DEBUG_BUILD



// 
USING "stack_sizes.sch"

// game commands
USING "commands_camera.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_xml.sch"
USING "script_PLAYER.sch"
USING "commands_script.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "net_events.sch"

// global files
USING "globals.sch"
USING "script_debug.sch"
USING "script_XML.sch"


CONST_INT VEHICLE_LOD_TEST_CAM_SPEED 4500


BOOl bKillXMLMenusScript

// ped and vehicle used on menus, done as global to script so it can be deleted
VEHICLE_INDEX xmlVehicle
PED_INDEX xmlPed
OBJECT_INDEX xmlObject
NETWORK_INDEX netVehicle
BOOL bNetScript

BOOL bSearchVehicle
INT iVehicleSearchStage

BOOL bVehicleLodTest
INT iVehicleLodTestStage
INT iVehicleLodTestCam
//INT iVehicleLodTestTimer
FLOAT fVehicleLodTestSpeedMultiplier

CAMERA_INDEX camID_LodTest
TIME_DATATYPE tdDisableInputTimer

PROC SETUP_MENUS()
	#IF NOT DEFINE_NM_BUILD_MODE
	
		PRINTSTRING("running SETUP_MENUS ")
		PRINTNL()
		
		xmlMenuInfo[XML_MENU_LIST_SHARED_WARPS].xml   = "debug/dbg_SharedGta5Warps.xml"
        xmlMenuInfo[XML_MENU_LIST_PERSONAL_WARPS].xml = "debug/dbg_MyGta5Warps.xml"
        xmlMenuInfo[XML_MENU_LIST_CODE_ACTIONS].xml   =  "debug/dbg_Tests.xml"
	
        xmlMenuInfo[XML_MENU_LIST_SHARED_WARPS].active   = TRUE
        xmlMenuInfo[XML_MENU_LIST_PERSONAL_WARPS].active = TRUE
        xmlMenuInfo[XML_MENU_LIST_CODE_ACTIONS].active   = TRUE
    

    // set shared xml files
		xmlMenuInfo[XML_MENU_LIST_VEHICLES].xml = "debug/dbg_vehicles.xml"
		xmlMenuInfo[XML_MENU_LIST_VEHICLES].active = TRUE
		
		xmlMenuInfo[XML_MENU_LIST_PEDS].xml = "debug/dbg_peds.xml"  
		xmlMenuInfo[XML_MENU_LIST_PEDS].active = TRUE

		xmlMenuInfo[XML_MENU_LIST_OBJECTS].xml = "debug/dbg_objects.xml"    
		xmlMenuInfo[XML_MENU_LIST_OBJECTS].active = TRUE
		
		//add the xml menu for triggering MoVE minigame scripts
		xmlMenuInfo[XML_MENU_LIST_MOVE].xml = "debug/dbg_move_minigames.xml"    
		xmlMenuInfo[XML_MENU_LIST_MOVE].active = TRUE
		
		// reset mission one
		xmlMenuInfo[XML_MENU_LIST_MISSION].xml  = ""
		xmlMenuInfo[XML_MENU_LIST_MISSION].active = FALSE

		// keys to activate menus
		xmlMenuInfo[XML_MENU_LIST_SHARED_WARPS].button   = ENUM_TO_INT(KEY_2)
		xmlMenuInfo[XML_MENU_LIST_PERSONAL_WARPS].button = ENUM_TO_INT(KEY_3)
		xmlMenuInfo[XML_MENU_LIST_CODE_ACTIONS].button   = ENUM_TO_INT(KEY_4)
		xmlMenuInfo[XML_MENU_LIST_VEHICLES].button       = ENUM_TO_INT(KEY_5)
		xmlMenuInfo[XML_MENU_LIST_PEDS].button           = ENUM_TO_INT(KEY_6)
		xmlMenuInfo[XML_MENU_LIST_MOVE].button           = ENUM_TO_INT(KEY_7)
		//xmlMenuInfo[XML_MENU_LIST_OBJECTS].button        = ENUM_TO_INT(KEY_R)
		//xmlMenuInfo[XML_MENU_LIST_MISSION].button        = ENUM_TO_INT(KEY_R)
	#ENDIF
ENDPROC

// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()

	// CURRENTLY NOTHING TO CLEANUP
	IF bVehicleLodTest
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		IF DOES_CAM_EXIST(camID_LodTest)
			DESTROY_CAM(camID_LodTest)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(xmlVehicle)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(xmlVehicle)
		ENDIF
		
	ENDIF
	
	IF bNetScript
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC


//
PROC loadScriptForXMLMenu(STRING scriptName, INT args = -1)

    IF DOES_SCRIPT_EXIST(scriptName)
        
        REQUEST_SCRIPT(scriptName)
        WHILE NOT HAS_SCRIPT_LOADED(scriptName)
            WAIT(0)
            REQUEST_SCRIPT(scriptName)
        ENDWHILE
        
        IF args <> -1
            START_NEW_SCRIPT_WITH_ARGS(scriptName, args, SIZE_OF(args), DEFAULT_STACK_SIZE) 
        ELSE
            START_NEW_SCRIPT(scriptName, DEFAULT_STACK_SIZE)    
        ENDIF
        
    ENDIF

ENDPROC

PROC createDebugVehicle(INT vehicleEnumValue, BOOL warpPlayerInside = FALSE, BOOL keepInVehicle = TRUE, BOOL freezePosition = FALSE, BOOL cleanupVehicle = TRUE, BOOL defaultColours = FALSE)

    VECTOR vehiclePosition
    FLOAT vehicleHeading
    BOOL playerWasInCar = FALSE
    
    IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
        REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
        WHILE NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
            HIDE_HUD_AND_RADAR_THIS_FRAME()
			WAIT(0)
            REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
        ENDWHILE
        
        IF IS_PLAYER_PLAYING(PLAYER_ID())   
        
			IF NETWORK_IS_GAME_IN_PROGRESS()
			
				VEHICLE_INDEX tempVeh
			
				IF NETWORK_DOES_NETWORK_ID_EXIST(netVehicle)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(netVehicle)
						IF NOT IS_ENTITY_DEAD(NET_TO_VEH(netVehicle))
							IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(netVehicle))
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									playerWasInCar = TRUE
								ENDIF
							ENDIF
						
							tempVeh = NET_TO_VEH(netVehicle)
							DELETE_VEHICLE(tempVeh)
						ENDIF
					ENDIF
				ENDIF
				
				vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.5, 0.0 >>)
	            vehicleHeading  = (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 90.0)
		        
				CREATE_NET_VEHICLE(netVehicle, INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue), vehiclePosition, vehicleHeading, FALSE)
				
				SCRIPT_EVENT_DATA_TICKER_MESSAGE cheatTickerEventData
				IF IS_THIS_MODEL_A_PLANE(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
					cheatTickerEventData.dataInt = ciCHEAT_TICKER_PLANE
				ELIF IS_THIS_MODEL_A_HELI(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
					cheatTickerEventData.dataInt = ciCHEAT_TICKER_HELI
				ELIF IS_THIS_MODEL_A_BOAT(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
					cheatTickerEventData.dataInt = ciCHEAT_TICKER_BOAT
				ELIF IS_THIS_MODEL_A_BIKE(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
					cheatTickerEventData.dataInt = ciCHEAT_TICKER_BIKE
				ELIF IS_THIS_MODEL_A_BICYCLE(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
					cheatTickerEventData.dataInt = ciCHEAT_TICKER_BICYCLE
				ENDIF
				cheatTickerEventData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_U
				cheatTickerEventData.playerID = PLAYER_ID()
				BROADCAST_TICKER_EVENT(cheatTickerEventData, ALL_PLAYERS())
				
				IF defaultColours
					SET_VEHICLE_COLOUR_COMBINATION(NET_TO_VEH(netVehicle), 0)
				ENDIF
				
				IF (playerWasInCar AND keepInVehicle)
				OR (warpPlayerInside)
					TASK_ENTER_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(netVehicle), -1, VS_DRIVER, PEDMOVE_SPRINT, ECF_WARP_PED)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
				
				IF cleanupVehicle
					tempVeh = NET_TO_VEH(netVehicle)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(tempVeh)
				ELSE
					xmlVehicle = NET_TO_VEH(netVehicle)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(xmlVehicle)
		            IF NOT IS_ENTITY_DEAD(xmlVehicle)
		                IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), xmlVehicle)
		                    vehiclePosition = GET_ENTITY_COORDS(xmlVehicle)
		                    vehicleHeading  = GET_ENTITY_HEADING(xmlVehicle)
		                    SET_ENTITY_COORDS(PLAYER_PED_ID(), <<vehiclePosition.x, vehiclePosition.y, vehiclePosition.z+4.0>>)
		                    playerWasInCar = TRUE
		                ENDIF
		            ENDIF
		                
		            DELETE_VEHICLE(xmlVehicle)
		        ENDIF
		        
		        IF NOT playerWasInCar
					vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.5, 0.0 >>)
		            vehicleHeading  = (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 90.0)
		        ENDIF
				
				xmlVehicle = CREATE_VEHICLE(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue), vehiclePosition, vehicleHeading, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(xmlVehicle)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_VEHICLE_IS_SPRAYED(xmlVehicle)
				ENDIF
				SET_VEHICLE_IS_STOLEN(xmlVehicle, FALSE)
				
				IF defaultColours
					SET_VEHICLE_COLOUR_COMBINATION(xmlVehicle, 0)
				ENDIF
				
				IF (playerWasInCar AND keepInVehicle)
				OR (warpPlayerInside)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), xmlVehicle, VS_DRIVER)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
					SET_CINEMATIC_MODE_ACTIVE(FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING (0.0)
	            ENDIF
				
				IF freezePosition
					FREEZE_ENTITY_POSITION(xmlVehicle, TRUE)
				ENDIF
	            
	            SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, vehicleEnumValue))
				
				IF cleanupVehicle
					SET_VEHICLE_AS_NO_LONGER_NEEDED(xmlVehicle)
				ENDIF
			ENDIF
            
        ENDIF   
    ENDIF

ENDPROC

PROC createDebugPed(INT pedEnumValue)

    VECTOR pedPosition
    FLOAT pedHeading
    
    IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, pedEnumValue))
        REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, pedEnumValue))
        WHILE NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, pedEnumValue))
            HIDE_HUD_AND_RADAR_THIS_FRAME()
			WAIT(0)
            REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, pedEnumValue))
        ENDWHILE
    
        IF IS_PLAYER_PLAYING(PLAYER_ID())
                
            IF DOES_ENTITY_EXIST(xmlPed)
                DELETE_PED(xmlPed)
            ENDIF
                
			pedPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.0, 0.0 >>)
            pedHeading  = (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 180.0)
            GET_GROUND_Z_FOR_3D_COORD(pedPosition, pedPosition.z)
            xmlPed = CREATE_PED(PEDTYPE_MISSION, INT_TO_ENUM(MODEL_NAMES, pedEnumValue), pedPosition, pedHeading)
        
            SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, pedEnumValue))

        ENDIF
    ENDIF

ENDPROC

PROC createDebugObject(INT objectEnumValue)

    VECTOR objectPosition
//    FLOAT objectHeading
    
    IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, objectEnumValue))
        REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, objectEnumValue))
        WHILE NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, objectEnumValue))
            HIDE_HUD_AND_RADAR_THIS_FRAME()
			WAIT(0)
            REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, objectEnumValue))
        ENDWHILE
    
        IF IS_PLAYER_PLAYING(PLAYER_ID())
                
            IF DOES_ENTITY_EXIST(xmlObject)
                DELETE_OBJECT(xmlObject)
            ENDIF
                
			objectPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.0, 0.0 >>)
    //        objectHeading  = (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 180.0)
            GET_GROUND_Z_FOR_3D_COORD(objectPosition, objectPosition.z)
            xmlObject = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, objectEnumValue), objectPosition)
        
            SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, objectEnumValue))

        ENDIF
    ENDIF

ENDPROC

FUNC BOOL DOES_STRING_CONTAIN_STRING(TEXT_LABEL_15 &tlLabel, TEXT_LABEL_15 &tlKey, BOOL bGrabTextFromLabel = FALSE)
	IF NOT IS_STRING_NULL_OR_EMPTY(tlLabel)
	AND DOES_TEXT_LABEL_EXIST(tlLabel)
	
		TEXT_LABEL_63 tlMain = tlLabel
		IF bGrabTextFromLabel
			tlMain = GET_STRING_FROM_TEXT_FILE(tlLabel)
		ENDIF
		
		INT iKeyHash = GET_HASH_KEY(tlKey)
		
		INT iLengthMain = GET_LENGTH_OF_LITERAL_STRING(tlMain)
		INT iLengthKey = GET_LENGTH_OF_LITERAL_STRING(tlKey)
		
		INT iChar
		
		FOR iChar = 0 TO iLengthMain-1
			IF iChar+iLengthKey > iLengthMain
				RETURN FALSE
			ELIF GET_HASH_KEY(GET_STRING_FROM_STRING(tlMain, iChar, iChar+iLengthKey)) = iKeyHash
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC



// MAIN SCRIPT
SCRIPT

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		PRINTSTRING("...xml_menus.sc has been forced to cleanup (SP to MP)")
		PRINTNL()
		
		Script_Cleanup()
	ENDIF
	
	// PROCESS_PRE_GAME
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		RESERVE_NETWORK_MISSION_VEHICLES(1)
		
		// This makes sure the net script is active, waits untull it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		bNetScript = TRUE
	ENDIF
	
	
	BOOL bCommandLineBlock = FALSE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableVehicleSpawnMenu")
		bCommandLineBlock = TRUE
	ENDIF
	
	WHILE bCommandLineBlock
		WAIT(0)
		// dont allow it to work.
	ENDWHILE
	
	
	
    // RESET ALL THE GLOBAL XML MENU DATA
    resetAllXMLmenuData()
	
	SETUP_MENUS()

    // widget
    xmlMenuData.size = 0.725
    START_WIDGET_GROUP("XML Menus")
        ADD_WIDGET_FLOAT_SLIDER("Menu Size", xmlMenuData.size, 0.2, 1.0, 0.05)
		ADD_WIDGET_BOOL("Kill Script", bKillXMLMenusScript)
    STOP_WIDGET_GROUP()

    // MAIN SCRIPT LOOP
    WHILE TRUE

        WAIT(0)
		
		// If we have a match end event, bail.
		IF bNetScript
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				Script_Cleanup()
			ENDIF
		ENDIF
		
		IF bKillXMLMenusScript
			Script_Cleanup()
		ENDIF
		
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdDisableInputTimer)) < 1000)
			PRINTLN("INPUT_VEH_CIN_CAM Blocked by script ", GET_THIS_SCRIPT_NAME())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		ENDIF

        IF IS_XML_MENU_ON_SCREEN()
		
			BOOL bRemoveXMLMenu = TRUE
			
			IF bSearchVehicle
			
				INT iItem
				TEXT_LABEL_15 tlLabel, tlSearch
				INT iTempCurrent, iTempTop
				BOOL bItemFound = FALSE
			
				SWITCH iVehicleSearchStage
					CASE 0
						SHOW_ONSCREEN_KEYBOARD("WEB_SEARCH", "", 15)
						iVehicleSearchStage++
					BREAK
					CASE 1
						SWITCH UPDATE_ONSCREEN_KEYBOARD()
							CASE OSK_SUCCESS
								IF NOT IS_STRING_NULL_OR_EMPTY(GET_ONSCREEN_KEYBOARD_RESULT())
									
									tlSearch = GET_ONSCREEN_KEYBOARD_RESULT()
									
									iTempCurrent = xmlMenuData.current
									iTempTop = xmlMenuData.top
									
									// To save on memory the vehicle menu only holds data for small portion so we need to process each block.
									iItem = 0
									xmlMenuData.current = 0
									
									WHILE xmlMenuData.current < xmlMenuData.total AND NOT bItemFound
									
										xmlMenuData.top = xmlMenuData.current
										loadXmlIntoGlobalStruct(xmlMenuInfo[xmlMenuData.choosenMenu].xml, xmlMenuData.choosenMenu)
										
										REPEAT TOTAL_XML_ITEMS_ON_SCREEN iItem
											IF xmlMenuData.current < xmlMenuData.total
												
												IF IS_MODEL_VALID((INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[iItem].value)))
													tlLabel = GET_MODEL_NAME_FOR_DEBUG(INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[iItem].value))
													IF DOES_STRING_CONTAIN_STRING(tlLabel, tlSearch, TRUE)
													OR DOES_STRING_CONTAIN_STRING(tlLabel, tlSearch, FALSE)
														bItemFound = TRUE
														BREAKLOOP
													ENDIF
												ELSE
													tlLabel = xmlMenuData.items[iItem].modelName
													IF DOES_STRING_CONTAIN_STRING(tlLabel, tlSearch, FALSE)
														bItemFound = TRUE
														BREAKLOOP
													ENDIF
												ENDIF
											ENDIF
											xmlMenuData.current++
										ENDREPEAT
									ENDWHILE
									
									IF NOT bItemFound
										xmlMenuData.current = iTempCurrent
										xmlMenuData.top = iTempTop
										loadXmlIntoGlobalStruct(xmlMenuInfo[xmlMenuData.choosenMenu].xml, xmlMenuData.choosenMenu)
									ENDIF
									
								ENDIF
								
								SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
								
								bSearchVehicle = FALSE
							BREAK
							CASE OSK_CANCELLED
							CASE OSK_FAILED
								#IF IS_DEBUG_BUILD
									SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
								#ENDIF
								bSearchVehicle = FALSE
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELSE
				SWITCH getXmlMenuInput()

	                CASE XML_MENU_INPUT_UP
	                    moveUpXMLMenu()
	                BREAK

	                CASE XML_MENU_INPUT_DOWN
	                    moveDownXMLMenu()
	                BREAK
					
					CASE XML_MENU_INPUT_LEFT
						IF xmlMenuData.choosenMenu = XML_MENU_LIST_VEHICLES
							INT i
							REPEAT TOTAL_XML_ITEMS_ON_SCREEN i
	                    		moveUpXMLMenu(FALSE)
							ENDREPEAT
						ENDIF
	                BREAK

	                CASE XML_MENU_INPUT_RIGHT
						IF xmlMenuData.choosenMenu = XML_MENU_LIST_VEHICLES
							INT i
							REPEAT TOTAL_XML_ITEMS_ON_SCREEN i
								moveDownXMLMenu(FALSE)
							ENDREPEAT
						ENDIF
	                BREAK
					
					CASE XML_MENU_INPUT_SEARCH
						SWITCH xmlMenuData.choosenMenu 
	                    	CASE XML_MENU_LIST_VEHICLES
	                            IF NOT bSearchVehicle
									bSearchVehicle = TRUE
									iVehicleSearchStage = 0
								ENDIF
								
	                        BREAK
	                    ENDSWITCH
					BREAK

	                CASE XML_MENU_INPUT_ACCEPT
					
						IF IS_PLAYER_PLAYING(PLAYER_ID())

	                        SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							
	                        SWITCH xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].type
							
	                            CASE XML_MENU_OPTION_WARP
	                                SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].warp)
	                                SET_ENTITY_HEADING(PLAYER_PED_ID(),     xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].heading)
	                                SET_GAMEPLAY_CAM_RELATIVE_HEADING (0.0)
	                            BREAK
	    
	                            CASE XML_MENU_OPTION_SCRIPT
	                                loadScriptForXMLMenu(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].scriptName)
	                            BREAK
	    
	                            CASE XML_MENU_OPTION_VALUE
	                            
	                                SWITCH xmlMenuData.choosenMenu 
	                                
	                                    CASE XML_MENU_LIST_VEHICLES
	                                        // spawn vehicle
											IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value))
	                                        	createDebugVehicle(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value)
											ELSE
												bRemoveXMLMenu = FALSE
											ENDIF
	                                    BREAK
	                                    
	                                    CASE XML_MENU_LIST_PEDS
	                                        // spawn ped
	                                        createDebugPed(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value)
	                                    BREAK
	                                    
	                                    CASE XML_MENU_LIST_OBJECTS
	                                        // spawn object
	                                        createDebugObject(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value)
	                                    BREAK
	                                    
	                                    CASE XML_MENU_LIST_MISSION
	                                        xmlMenuData.missionValue = xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value
	                                    BREAK
	                                    
										CASE XML_MENU_LIST_CODE_ACTIONS
											loadScriptForXMLMenu("debug_launcher", xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value)
										BREAK
										
	                                ENDSWITCH
	                                
	                            BREAK
	    
	                        ENDSWITCH
	                    
	                    ENDIF
						
						IF bRemoveXMLMenu
							REMOVE_XML_MENU()
						ENDIF

	                BREAK

	                CASE XML_MENU_INPUT_ACCEPT2
					
						IF IS_PLAYER_PLAYING(PLAYER_ID())

	                        SWITCH xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].type
							
	                            CASE XML_MENU_OPTION_VALUE
	                            
	                                SWITCH xmlMenuData.choosenMenu 
	                                
	                                    CASE XML_MENU_LIST_VEHICLES
										
											IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value))
												SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
												IF NETWORK_IS_GAME_IN_PROGRESS()
													tdDisableInputTimer = GET_NETWORK_TIME()
												ENDIF
												
												// spawn vehicle with player inside
												createDebugVehicle(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value, TRUE)
											ELSE
												bRemoveXMLMenu = FALSE
											ENDIF
	                                    BREAK
										
	                                ENDSWITCH
	                                
	                            BREAK
	    
	                        ENDSWITCH
	                    
	                    ENDIF
						
						IF bRemoveXMLMenu
							REMOVE_XML_MENU()
						ENDIF

	                BREAK
					
					CASE XML_MENU_INPUT_ACCEPT3

	                    IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF IS_MODEL_IN_CDIMAGE(INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value))
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								bVehicleLodTest = TRUE
								iVehicleLodTestStage = 0
								iVehicleLodTestCam = 0
								//iVehicleLodTestTimer = -1
								fVehicleLodTestSpeedMultiplier = 1.0
		                    ELSE
								bRemoveXMLMenu = FALSE
	                    	ENDIF
						ENDIF
						
						IF bRemoveXMLMenu
							REMOVE_XML_MENU()
						ENDIF

	                BREAK

	                CASE XML_MENU_INPUT_CANCEL

	                    REMOVE_XML_MENU()
	                    IF IS_PLAYER_PLAYING(PLAYER_ID())
	                        SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	                    ENDIF

	                BREAK
	                
	                CASE XML_MENU_INPUT_NEXT_MENU
	                    GRAB_NEXT_XML_MENU()
	                    RESET_AND_RELOAD_XML_MENU()
	                BREAK
	                
	                CASE XML_MENU_INPUT_PREVIOUS_MENU
	                    GRAB_PREVIOUS_XML_MENU()
	                    RESET_AND_RELOAD_XML_MENU()
	                BREAK
	                
	                CASE XML_MENU_INPUT_NEW_MENU
	                    RESET_AND_RELOAD_XML_MENU()
	                BREAK
					
					CASE XML_MENU_INPUT_KEYPRESS
						//jumpToXMLMenuKey()
					BREAK

	            ENDSWITCH
			ENDIF
            
            // ------------------------------------------------------
            // DRAW MENU
            // ------------------------------------------------------
            
            IF IS_XML_MENU_ON_SCREEN()
                drawXMLMenu()
            ENDIF
			
		ELIF bVehicleLodTest
		
			IF DOES_ENTITY_EXIST(xmlVehicle)
			AND IS_VEHICLE_DRIVEABLE(xmlVehicle)
				IF GET_CLOCK_HOURS() >= 8 AND GET_CLOCK_HOURS() < 18
					SET_VEHICLE_LIGHTS(xmlVehicle, FORCE_VEHICLE_LIGHTS_OFF)
				ELSE
					SET_VEHICLE_LIGHTS(xmlVehicle, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF
			ENDIF
		
		
			VECTOR vOffset1, vOffset2, vMinDim, vMaxDim
			FLOAT fFarOffset
			
			IF IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
				moveDownXMLMenu()
				iVehicleLodTestStage = 0
			ELIF IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE)
				iVehicleLodTestCam--
				IF iVehicleLodTestCam < 0
					iVehicleLodTestCam = 3
				ENDIF
				iVehicleLodTestStage = 0
			ELIF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
				iVehicleLodTestCam++
				IF iVehicleLodTestCam > 3
					iVehicleLodTestCam = 0
				ENDIF
				iVehicleLodTestStage = 0
			ELIF IS_BUTTON_JUST_PRESSED(PAD1, TRIANGLE)
				iVehicleLodTestStage = 99
			ELIF IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)
				fVehicleLodTestSpeedMultiplier -= 0.5
			ELIF IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER1)
				fVehicleLodTestSpeedMultiplier += 0.5
			ENDIF
			
			// Cap the speeds
			IF fVehicleLodTestSpeedMultiplier < 0.5
				fVehicleLodTestSpeedMultiplier = 0.5
			ENDIF	
			IF fVehicleLodTestSpeedMultiplier > 2.0
				fVehicleLodTestSpeedMultiplier = 2.0
			ENDIF
			
			INT iFinalCamSpeed = FLOOR((VEHICLE_LOD_TEST_CAM_SPEED)/fVehicleLodTestSpeedMultiplier)
			
			SWITCH iVehicleLodTestStage
				// Warp the player to the airport, create vehicle, start camera
				CASE 0
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << 1466.5885, 3187.7419, 39.4116 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 105.0)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					IF DOES_CAM_EXIST(camID_LodTest)
						DESTROY_CAM(camID_LodTest)
					ENDIF
					
					CLEAR_AREA(<< 1466.5885, 3187.7419, 39.4116 >>, 500.0, TRUE)
					
					createDebugVehicle(xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value, FALSE, FALSE, TRUE, FALSE, TRUE)
					
					IF DOES_ENTITY_EXIST(xmlVehicle)
					AND IS_VEHICLE_DRIVEABLE(xmlVehicle)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					
						IF iVehicleLodTestCam = 0 // Front
							SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID()))
						ELIF iVehicleLodTestCam = 1 // Right
							SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+90.0)
						ELIF iVehicleLodTestCam = 2 // Rear
							SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+180.0)
						ELIF iVehicleLodTestCam = 3 // Left
							SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())-90.0)
						ENDIF
						
						camID_LodTest = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
						POINT_CAM_AT_ENTITY(camID_LodTest, xmlVehicle, <<0,0,0.5>>, TRUE)
						SET_CAM_FOV(camID_LodTest, 50)
						
						vOffset1 = <<0,0,0>>
						vOffset2 = <<0,0,0>>
						vMinDim = <<0,0,0>>
						vMaxDim = <<0,0,0>>
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(xmlVehicle), vMinDim, vMaxDim)
						
						fFarOffset = 50.0
						
						IF iVehicleLodTestCam = 0 // Front
							vOffset1 = << 0.0, 5.0, 1.0 >>
							vOffset2 = vOffset1 + << 0, fFarOffset, 0 >>
						ELIF iVehicleLodTestCam = 1 // Right
							vOffset1 = << 5.0, 0.0, 1.0 >>
							vOffset2 = vOffset1 + << fFarOffset, 0, 0 >>
						ELIF iVehicleLodTestCam = 2 // Rear
							vOffset1 = << 0.0, -5.0, 1.0 >>
							vOffset2 = vOffset1 + << 0, -fFarOffset, 0 >>
						ELIF iVehicleLodTestCam = 3 // Left
							vOffset1 = << -5.0, 0.0, 1.0 >>
							vOffset2 = vOffset1 + << -fFarOffset, 0, 0 >>
						ENDIF
						
						SET_CAM_PARAMS(camID_LodTest, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(xmlVehicle, vOffset1), <<0,0,0>>, 50.0, 0)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						//iVehicleLodTestTimer = -1
						iVehicleLodTestStage++
					ELSE
						iVehicleLodTestStage = 99
					ENDIF
				BREAK
				
				// Pause for a seconds then interp to 100m
				CASE 1
					IF DOES_CAM_EXIST(camID_LodTest)
						IF NOT IS_CAM_INTERPOLATING(camID_LodTest)
						
//							IF iVehicleLodTestTimer = -1
//								iVehicleLodTestTimer = GET_GAME_TIMER()
//							ELIF (GET_GAME_TIMER() - iVehicleLodTestTimer) > 1000
								
								IF DOES_ENTITY_EXIST(xmlVehicle)
								AND IS_VEHICLE_DRIVEABLE(xmlVehicle)
								AND NOT IS_PED_INJURED(PLAYER_PED_ID())
								
									IF iVehicleLodTestCam = 0 // Front
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID()))
									ELIF iVehicleLodTestCam = 1 // Right
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+90.0)
									ELIF iVehicleLodTestCam = 2 // Rear
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+180.0)
									ELIF iVehicleLodTestCam = 3 // Left
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())-90.0)
									ENDIF
									
									vOffset1 = <<0,0,0>>
									vOffset2 = <<0,0,0>>
									vMinDim = <<0,0,0>>
									vMaxDim = <<0,0,0>>
									GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(xmlVehicle), vMinDim, vMaxDim)
									
									fFarOffset = 100.0
									
									IF iVehicleLodTestCam = 0 // Front
										vOffset1 = << 0.0, 5.0, 1.0 >>
										vOffset2 = vOffset1 + << 0, fFarOffset, 0 >>
									ELIF iVehicleLodTestCam = 1 // Right
										vOffset1 = << 5.0, 0.0, 1.0 >>
										vOffset2 = vOffset1 + << fFarOffset, 0, 0 >>
									ELIF iVehicleLodTestCam = 2 // Rear
										vOffset1 = << 0.0, -5.0, 1.0 >>
										vOffset2 = vOffset1 + << 0, -fFarOffset, 0 >>
									ELIF iVehicleLodTestCam = 3 // Left
										vOffset1 = << -5.0, 0.0, 1.0 >>
										vOffset2 = vOffset1 + << -fFarOffset, 0, 0 >>
									ENDIF
									
									SET_CAM_PARAMS(camID_LodTest,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(xmlVehicle, vOffset2), <<0,0,0>>, 50.0, iFinalCamSpeed)
									
									//iVehicleLodTestTimer = -1
									iVehicleLodTestStage++
								ELSE
									iVehicleLodTestStage = 99
								ENDIF
//							ENDIF
						ENDIF
					ELSE
						iVehicleLodTestStage = 99
					ENDIF
				BREAK
				
				// Interp back to start
				CASE 2
					IF DOES_CAM_EXIST(camID_LodTest)
						IF NOT IS_CAM_INTERPOLATING(camID_LodTest)
						
//							IF iVehicleLodTestTimer = -1
//								iVehicleLodTestTimer = GET_GAME_TIMER()
//							ELIF (GET_GAME_TIMER() - iVehicleLodTestTimer) > 1000
								
								IF DOES_ENTITY_EXIST(xmlVehicle)
								AND IS_VEHICLE_DRIVEABLE(xmlVehicle)
								AND NOT IS_PED_INJURED(PLAYER_PED_ID())
								
									IF iVehicleLodTestCam = 0 // Front
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID()))
									ELIF iVehicleLodTestCam = 1 // Right
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+90.0)
									ELIF iVehicleLodTestCam = 2 // Rear
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())+180.0)
									ELIF iVehicleLodTestCam = 3 // Left
										SET_ENTITY_HEADING(xmlVehicle, GET_ENTITY_HEADING(PLAYER_PED_ID())-90.0)
									ENDIF
									
									vOffset1 = <<0,0,0>>
									vOffset2 = <<0,0,0>>
									vMinDim = <<0,0,0>>
									vMaxDim = <<0,0,0>>
									GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(xmlVehicle), vMinDim, vMaxDim)
									
									fFarOffset = 100.0
									
									IF iVehicleLodTestCam = 0 // Front
										vOffset1 = << 0.0, 5.0, 1.0 >>
										vOffset2 = vOffset1 + << 0, fFarOffset, 0 >>
									ELIF iVehicleLodTestCam = 1 // Right
										vOffset1 = << 5.0, 0.0, 1.0 >>
										vOffset2 = vOffset1 + << fFarOffset, 0, 0 >>
									ELIF iVehicleLodTestCam = 2 // Rear
										vOffset1 = << 0.0, -5.0, 1.0 >>
										vOffset2 = vOffset1 + << 0, -fFarOffset, 0 >>
									ELIF iVehicleLodTestCam = 3 // Left
										vOffset1 = << -5.0, 0.0, 1.0 >>
										vOffset2 = vOffset1 + << -fFarOffset, 0, 0 >>
									ENDIF
									
									SET_CAM_PARAMS(camID_LodTest, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(xmlVehicle, vOffset1), <<0,0,0>>, 50.0, iFinalCamSpeed)
									
									//iVehicleLodTestTimer = -1
									iVehicleLodTestStage = 1
								ELSE
									iVehicleLodTestStage = 99
								ENDIF
//							ENDIF
						ENDIF
					ELSE
						iVehicleLodTestStage = 99
					ENDIF
				BREAK
				
				// Cleanup and return to menu
				CASE 99
					IF DOES_ENTITY_EXIST(xmlVehicle)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(xmlVehicle)
					ENDIF
					
					IF DOES_CAM_EXIST(camID_LodTest)
						DESTROY_CAM(camID_LodTest)
					ENDIF
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					bVehicleLodTest = FALSE
					
					CLEAR_HELP()
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
				
					RESET_AND_RELOAD_XML_MENU()
				BREAK
			ENDSWITCH
			
			
			SET_TEXT_SCALE(0.0000, 0.35)
			SET_TEXT_WRAP(0.0, 1.0)
			SET_TEXT_CENTRE(FALSE)
		    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		    SET_TEXT_EDGE(0, 0, 0, 0, 0)
			
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("XML_VEH_LOD")
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MODEL_NAME_FOR_DEBUG(INT_TO_ENUM(MODEL_NAMES, xmlMenuData.items[GET_CURRENT_XML_MENU_ARRAY_POS()].value)))
				ADD_TEXT_COMPONENT_FLOAT(fVehicleLodTestSpeedMultiplier, 1)
			END_TEXT_COMMAND_DISPLAY_TEXT(0.05, 0.05)
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
		ELSE
		

            IF IS_PLAYER_PLAYING(PLAYER_ID())
                
                INT i
                
                // check if any of the menu buttons are being pressed
                FOR i = 0 TO (ENUM_TO_INT(TOTAL_XML_MENUS)-1)

                    IF xmlMenuInfo[i].active

						// [SP] Hack for Oculus
						IF INT_TO_ENUM(XML_MENU_LIST, i) = XML_MENU_LIST_VEHICLES
						AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
						AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RS)
						AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT)
						
							//PRINTSTRING("Testing 1,2,3")
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

                            IF loadXmlIntoGlobalStruct(xmlMenuInfo[i].xml, XML_MENU_LIST_VEHICLES)
                                xmlMenuData.choosenMenu = XML_MENU_LIST_VEHICLES
                                xmlMenuData.menuIsBeingDrawn = TRUE
                            ENDIF
						ENDIF
						
                        IF xmlMenuInfo[i].button != 0
						AND IS_KEYBOARD_KEY_JUST_PRESSED(INT_TO_ENUM(KEY_NUMBER, xmlMenuInfo[i].button))
						
							// For now, only allow the vehicle menu in multiplayer
							IF (g_bInMultiplayer AND INT_TO_ENUM(XML_MENU_LIST, i) = XML_MENU_LIST_VEHICLES)
							OR (NOT g_bInMultiplayer)
							
	                            SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

	                            IF loadXmlIntoGlobalStruct(xmlMenuInfo[i].xml, INT_TO_ENUM(XML_MENU_LIST, i))
	                                xmlMenuData.choosenMenu = INT_TO_ENUM(XML_MENU_LIST, i)
	                                i = ENUM_TO_INT(TOTAL_XML_MENUS)
	                                xmlMenuData.menuIsBeingDrawn = TRUE
	                            ENDIF
							ENDIF

                        ENDIF

                    ENDIF

                ENDFOR
				
            ENDIF

        ENDIF

    ENDWHILE

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD


