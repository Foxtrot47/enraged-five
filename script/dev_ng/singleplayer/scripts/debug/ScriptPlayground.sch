// ScriptPlayground.sch

// ************************************ DEFINE STRUCTS, ENUMS, CONSTANTS ***************************************
ENUM SCRIPTPLAYGROUND_STATE
	PLAYGROUNDSTATE_Init = 0,		// Set any variables needed. (happens once)
	PLAYGROUNDSTATE_Request,		// Request assets needed for this script to run.  (happens once)
	PLAYGROUNDSTATE_Stream,			// Check that all assets have been fully loaded.  (happens until done streaming)
	PLAYGROUNDSTATE_Spawn,			// Spawn anything, or begin anything. 			  (happens once)
	PLAYGROUNDSTATE_Update			// General update								  (runs until script ends)
ENDENUM

// This is 
//STRUCT SomeDataStruct
//	INT 			iVal1
//	FLOAT 			fVal2
//	STRING			szVal3
//	TEXT_LABEL_15	szVal4
//ENDSTRUCT

// We'll define constants here. These are values that can never be changed.
// Good to have these as opposed to hard-coding, because if you need to change a hard-coded value, you have to change it everywhere.
// These, however, you change once, and it changes it everywhere.
//CONST_INT		SOME_INT_NAME		1
//CONST_FLOAT	SOME_FLOAT_NAME		1.0



// This is typically where you're going to define PROCs, FUNCs, etc. This is in an effort
// to keep the main script clean
//PROC DO_SOMETHING_HERE()
// 		// Do other things in here, generally to passed in params.
//ENDPROC

//FUNC BOOL DO_SOMETHING_HERE_WITH_A_RETURN_VALUE()
// 		// Do other things in here, generally to passed in params.
//		RETURN FALSE
//ENDFUNC

