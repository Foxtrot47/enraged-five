





/////////////////////////////////////////////////////////////////////////////////////////



USING "buildtype.sch"
USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
	USING "commands_debug.sch"
#ENDIF

USING "commands_hud.sch"
USING "commands_script.sch"
USING "commands_ped.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_player.sch"
USING "commands_entity.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "selector_public.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"

USING "shared_debug.sch"
USING "script_debug.sch"



///////////////////////////////////////////
///    VARIABLES FOR CALLING SCRIPT
INT iGameServerTestStage

#IF IS_DEBUG_BUILD
BOOL bKillScript = FALSE
#ENDIF
///    END OF VARIABLES FOR CALLING SCRIPT
///////////////////////////////////////////



///////////////////////////////////////////
///    DEBUG PROCS
#IF IS_DEBUG_BUILD
PROC ADD_GAME_SERVER_TEST_WIDGETS()
	START_WIDGET_GROUP("Game Server Test")
		ADD_WIDGET_BOOL("Kill script", bKillScript)
	STOP_WIDGET_GROUP()
ENDPROC
PROC ADD_GAME_SERVER_TEST_TEXT()
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(0.0000, 0.350)
	SET_TEXT_COLOUR(iR, iG, iB, iA)
	SET_TEXT_CENTRE(TRUE)

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING("GAME SERVER TEST RUNNING")
	END_TEXT_COMMAND_DISPLAY_TEXT(0.0, 0.1) // range [0.0-1.0]
ENDPROC
#ENDIF
///    END OF DEBUG PROCS
///////////////////////////////////////////


/// PURPOSE: Terminates the script thread
PROC CLEANUP_GAME_SERVER_TEST_SCRIPT()
	PRINTLN("Terminating ", GET_THIS_SCRIPT_NAME())
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE: Main entry point for the game_server_test.sc script.
PROC PROCESS_GAME_SERVER_TEST()

	#IF IS_DEBUG_BUILD
		IF bKillScript
			CLEANUP_GAME_SERVER_TEST_SCRIPT()
		ENDIF
	#ENDIF
	
	// Display GAME SERVER TEST RUNNING at the top of the screen
	#IF IS_DEBUG_BUILD
		//ADD_GAME_SERVER_TEST_TEXT()
	#ENDIF
	
	
	VEHICLE_INDEX veh_car
	MODEL_NAMES model_car = ADDER
	//VECTOR v_car_pos = <<188.6595, -948.7460, 29.0921>>
	//enumCharacterList ePed
	PICKUP_INDEX wep_pick
	VECTOR ped_coord
	INT iPlacementFlags = 0
	
	// Add all your other stuff here.
	SWITCH iGameServerTestStage
		CASE 0
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_K) // check keypress
				
				ADD_GAME_SERVER_TEST_TEXT()
				IF NOT DOES_ENTITY_EXIST(veh_car)
					REQUEST_MODEL(model_car)
		
					IF HAS_MODEL_LOADED(model_car)
						GET_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0,5.0, 0.0>>),ped_coord)
						veh_car = CREATE_VEHICLE(model_car, ped_coord, 153.1829)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
						
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
						SET_VEHICLE_TYRE_SMOKE_COLOR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 226, 6, 6)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_L)
				ped_coord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0,5.0, 0.0>>)
				
				//ePed = GET_CURRENT_PLAYER_PED_ENUM()
				//IF IS_PLAYER_PED_PLAYABLE(ePed)
					//CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 5000)
				//ENDIF
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				
				CREATE_PICKUP_ROTATE(PICKUP_WEAPON_ADVANCEDRIFLE,ped_coord,<<0.0,2.0,0.0>>,iPlacementFlags)
				wep_pick = CREATE_PICKUP(PICKUP_AMMO_RIFLE,ped_coord,iPlacementFlags)
				
				SET_PICKUP_REGENERATION_TIME(wep_pick, 60)
				
				//SET_VEHICLE_TYRE_SMOKE_COLOR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 226, 6, 6)
				//GIVE_LOCAL_PLAYER_CASH(1000)
				//TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEBUG,1000)
				//NETWORK_EARN_FROM_DEBUG(1000)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				
				//ped_coord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<5.0,0.0, 0.0>>)
				//GIVE_LOCAL_PLAYER_CASH(1000)
				//CREATE_PICKUP(PICKUP_MONEY_CASE,ped_coord)
				//TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEBUG,1000)
				INT iTransactionID
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ARMORED_TRUCKS, 1000, iTransactionID)
				//GIVE_LOCAL_PLAYER_FM_CASH(5000, 1, TRUE, 0)
				//NETWORK_EARN_FROM_DEBUG(1000)
				
				//enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
				//IF IS_PLAYER_PED_PLAYABLE(ePed)
				//	CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 5000)
				//ENDIF
			ENDIF
			//	create vehicle with mods
			//	try and store in inventory
		BREAK
	ENDSWITCH
ENDPROC

