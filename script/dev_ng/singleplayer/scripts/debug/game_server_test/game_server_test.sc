USING "buildtype.sch"
USING "rage_builtins.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "game_server_test.sch"

// MAIN SCRIPT
SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	#IF IS_DEBUG_BUILD
		ADD_GAME_SERVER_TEST_WIDGETS()
	#ENDIF
	
    // MAIN SCRIPT LOOP
    WHILE TRUE

        WAIT(0)
		
		PROCESS_GAME_SERVER_TEST()

    ENDWHILE

ENDSCRIPT

#ENDIF	// IS_DEBUG_BUILD


