USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_OR_PROFILE_BUILD

USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_clock.sch"
USING "selector_public.sch"
USING "select_mission_stage.sch"

CONST_INT 		Z_SKIP_TEST_00				0
CONST_INT 		Z_SKIP_TEST_01				1
CONST_INT 		Z_SKIP_TEST_02				2
CONST_INT 		Z_SKIP_TEST_03				3
CONST_INT 		Z_SKIP_TEST_MAX				4

CONST_INT		iCONST_NUM_PED_MODELS		4
CONST_INT 		iCONST_NUM_LOCATIONS		12
CONST_INT		iCONST_NUM_AMBIENTS			8
CONST_INT		iCONST_SETTLE_TIME			5000
CONST_FLOAT		flCONST_CAMERA_PITCH_MOD	1.0
CONST_FLOAT		flCONST_CAMERA_PAN_MOD 		0.3 

STRUCT	FPS_LOCATION_DATA
	VECTOR 					vPos								//	Teleport Location
	FLOAT					flHeading							//	Teleport Heading
	//STRING 					strLocationName						// 	Location name
	TEXT_LABEL_63			txtLabelName
	VECTOR					vCarDestination
	FLOAT					flCarHeading
	MODEL_NAMES				mnPedModel[iCONST_NUM_PED_MODELS]	//	Ped Models
	MODEL_NAMES				mnVehModel							//	Vehicle Model
ENDSTRUCT
FPS_LOCATION_DATA	m_locationData[iCONST_NUM_LOCATIONS]

ENUM FPS_TEST_STAGE
	CHOOSE_SETUP,
	SETUP,
	WARP_TO_LOCATION,
	WAIT_TO_SETTLE,
	CAMERA_PITCH,
	CAMERA_ROTATE,
	CREATE_CAR,
	CAR_DRIVE,
	CLEANUP_ZONE,
	FPS_DONE
ENDENUM
FPS_TEST_STAGE eFPSlocationStage = CHOOSE_SETUP

ENUM FPS_RESULTS_SCREEN
	RS_START,
	RS_WAIT_FOR_SMOKETEST_SHOW,
	RS_WAIT_FOR_GRAPH,
	RS_DONE
ENDENUM
FPS_RESULTS_SCREEN	eResultsScreen = RS_START

INT 	iCurrentLocationIndex, iAverageRepeat
FLOAT 	flNewHeading
BOOL	bProceed
VECTOR  vehicles_coords

//AI test vars
CONST_INT numgangpeds 5
VECTOR GangStartPos[numgangpeds], CopStartPos[numgangpeds]
FLOAT GangStartHead[numgangpeds], CopStartHead[numgangpeds]
PED_INDEX pedCop[numgangpeds], pedCriminal[numgangpeds]
REL_GROUP_HASH relGroupCop, relGroupCrim
BOOL AI_SETUP_DONE
INT GANGPED

VEHICLE_INDEX veh_car
MODEL_NAMES model_car = NINEF2

//TIMER	tmrResultsScreen

PROC RESET_ALL_LOCATION_MODELS()
	INT i, j
	FOR i = 0 TO iCONST_NUM_LOCATIONS - 1
		FOR j = 0 TO iCONST_NUM_PED_MODELS - 1
			m_locationData[i].mnPedModel[j] = DUMMY_MODEL_FOR_SCRIPT
		ENDFOR
		m_locationData[i].mnVehModel = DUMMY_MODEL_FOR_SCRIPT
	ENDFOR
ENDPROC

PROC	INIT_ALL_LOCATION_DATA()
	FLOAT  fHeading
	RESET_ALL_LOCATION_MODELS()
	
	// Area #0 - DOCKS
	m_locationData[0].txtLabelName = "DOCKSTERMINAL"
	m_locationData[0].vPos = << 814.5111, -3050.1453, 4.7421 >>		
	m_locationData[0].flHeading = fHeading
	
	// Area #1 - PERSHING
	m_locationData[1].txtLabelName = "PERSHINGSQUARE"
	m_locationData[1].vPos = << 132.5859, -985.7192, 28.3605 >>		
	m_locationData[1].flHeading = fHeading
	
	// Area #2 - VINEWOOD HILLS
	m_locationData[2].txtLabelName = "VINEWOODHILLS"
	m_locationData[2].vPos = << -307.9470, 383.8104, 109.3216 >>	
	m_locationData[2].flHeading = fHeading

	// Area #3 - SANDY SHORE
	m_locationData[3].txtLabelName = "SANDYSHORE"
	m_locationData[3].vPos = << 1933.5807, 3780.2559, 31.3052 >>	
	m_locationData[3].flHeading = fHeading

	// Area #4 - PALETO BAY
	m_locationData[4].txtLabelName = "PALETOBAY"
	m_locationData[4].vPos = << 1933.5807, 3780.2559, 31.3052 >>	
	m_locationData[4].flHeading = fHeading
	
	// Area #5 - Downton Day
	m_locationData[5].txtLabelName = "DOWNTOWNDAY"
	m_locationData[5].vPos = << -68.898770,-1100.900024,26.0 >>	
	m_locationData[5].flHeading = fHeading
	
	// Area #6 - Downtown Night
	m_locationData[6].txtLabelName = "DOWNTOWNNIGHT"
	m_locationData[6].vPos = << -68.898770,-1100.900024,26.0 >>
	m_locationData[6].flHeading = fHeading
	
	// Area #7 - Life invaders Interior
	m_locationData[7].txtLabelName = "INTERIOR"
	m_locationData[7].vPos = <<-1063.7321, -243.4981, 43.5213>>
	m_locationData[7].flHeading = fHeading
	
	// Area #8 - Streaming test
	m_locationData[8].txtLabelName = "STREAMING"
	m_locationData[8].vPos = << -217.45, -685.04, 33.31 >>
	m_locationData[8].flHeading = fHeading
	m_locationData[8].vCarDestination = << 542.70, -853.72, 40.36 >>
	m_locationData[8].flCarHeading = -162.7
	
	// Area #9 - Streaming test with wanted level
	m_locationData[9].txtLabelName = "STREAMING5STAR"
	m_locationData[9].vPos = << -217.45, -685.04, 33.31 >>
	m_locationData[9].flHeading = fHeading
	m_locationData[9].vCarDestination = << 542.70, -853.72, 40.36 >>
	m_locationData[9].flCarHeading = -162.7
	
	// Area #10 - Physics test
	m_locationData[10].txtLabelName = "PHYSICS"
	m_locationData[10].vPos = << -592.5580, -1652.5548, 23.4580 >>
	m_locationData[10].flHeading = fHeading
	
	// Area #11 - Physics test with wanted level
	m_locationData[11].txtLabelName = "PHYSICS5STAR"
	m_locationData[11].vPos = << -592.5580, -1652.5548, 23.4580 >>
	m_locationData[11].flHeading = fHeading	
	
	IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
		INT i
		FOR i = 0 TO iCONST_NUM_LOCATIONS - 1
			m_locationData[i].txtLabelName += "_maponly"
		ENDFOR
	ENDIF
	
ENDPROC

//PROC CREATE_AND_TASK_VEHICLE_TO_COORD(VEHICLE_INDEX &veh, PED_INDEX &pedDriver, VECTOR vPos, FLOAT flHeading, VECTOR vDestPos)
//	pedDriver = CREATE_PED_WITH_DEFAULTS(PEDTYPE_MISSION, m_locationData[iCurrentLocationIndex].mnPedModel[0], (<<0,0,0>>), 0.0)
//	veh = CREATE_VEHICLE(m_locationData[iCurrentLocationIndex].mnVehModel, vPos, flHeading)
//	IF NOT IS_VEHICLE_FUCKED(veh)
//	AND IS_PED_ACTIVE(pedDriver)
//		SET_PED_INTO_VEHICLE(pedDriver, veh)
//		TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, veh, vDestPos, 1,  DRIVINGSTYLE_NORMAL,DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 1, 1)
//		//TASK_VEHICLE_DRIVE_WANDER(pedDriver, veh, 3, DRIVINGMODE_PLOUGHTHROUGH)
//	ENDIF
//ENDPROC
//
//PROC CREATE_AND_TASK_PED_TO_COORD(INT index, VECTOR vPos, VECTOR vDest, MODEL_NAMES mnModel)
//	m_pedAmbients[index] = CREATE_PED_WITH_DEFAULTS(PEDTYPE_MISSION, mnModel, vPos, 0.0)
//	IF IS_PED_ACTIVE(m_pedAmbients[index])
//		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedAmbients[index], TRUE)
//		TASK_FOLLOW_NAV_MESH_TO_COORD(m_pedAmbients[index], vDest, PEDMOVE_WALK, -1)
//	ENDIF
//ENDPROC

//PROC POPULATE_CURRENT_LOCATION()
//	SWITCH m_locationData[iCurrentLocationIndex].regLocation
//		CASE REGION_GRZ_ADLERRANCH
//			// Vehicles
//			// Nothing here
//				
//			// Peds
//			CREATE_AND_TASK_PED_TO_COORD(0, (<<-945.41516, 1518.37170, 200.43372>>), (<<-910.72601, 1522.98840, 201.51256>>), m_locationData[0].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(1, (<<-945.58911, 1516.54956, 200.44449>>), (<<-914.44611, 1514.11523, 200.33000>>), m_locationData[0].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(2, (<<-941.24860, 1512.61914, 200.05653>>), (<<-909.05963, 1475.11292, 198.93240>>), m_locationData[0].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(3, (<<-941.35022, 1507.84741, 199.76065>>), (<<-914.58392, 1492.19983, 200.63724>>), m_locationData[0].mnPedModel[3])
//			CREATE_AND_TASK_PED_TO_COORD(4, (<<-943.24561, 1509.28345, 200.13329>>), (<<-968.51776, 1562.39075, 205.91476>>), m_locationData[0].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(5, (<<-955.24506, 1521.39795, 200.53381>>), (<<-1004.80542, 1518.51685, 200.04126>>), m_locationData[0].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(6, (<<-957.16968, 1517.73914, 200.26569>>), (<<-1020.19604, 1540.85400, 204.52209>>), m_locationData[0].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(7, (<<-959.58856, 1512.04370, 199.99704>>), (<<-1008.03790, 1548.15198, 206.20193>>), m_locationData[0].mnPedModel[3])
//		BREAK
//		
//		CASE REGION_ROA_ANNESBURG
//			// Vehicle
//			CREATE_AND_TASK_VEHICLE_TO_COORD(m_vehWagon1, m_pedDriver1, (<<2948.17822, 1395.11938, 43.24228>>), 156.10, (<<2913.4, 1256.6, 47.6>>))
//			
//			// Peds
//			CREATE_AND_TASK_PED_TO_COORD(0, (<<2923.28833, 1371.12878, 43.46996>>), (<<2865.73853, 1370.62451, 63.94314>>), m_locationData[1].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(1, (<<2923.57007, 1373.89490, 43.46996>>), (<<2876.66016, 1385.03333, 65.95869>>), m_locationData[1].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(2, (<<2941.05933, 1358.62305, 43.47213>>), (<<2924.64966, 1280.06946, 43.56168>>), m_locationData[1].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(3, (<<2942.38770, 1360.42944, 43.50419>>), (<<2924.64966, 1280.06946, 43.56168>>), m_locationData[1].mnPedModel[3])
//			CREATE_AND_TASK_PED_TO_COORD(4, (<<2944.68433, 1368.12598, 43.58495>>), (<<2924.64966, 1280.06946, 43.56168>>), m_locationData[1].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(5, (<<2928.64697, 1353.89282, 43.49571>>), (<<2944.10278, 1425.77319, 45.02636>>), m_locationData[1].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(6, (<<2928.52783, 1343.93799, 43.53968>>), (<<2944.10278, 1425.77319, 45.02636>>), m_locationData[1].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(7, (<<2919.33960, 1372.75903, 43.59244>>), (<<2944.10278, 1425.77319, 45.02636>>), m_locationData[1].mnPedModel[3])
//		BREAK
//		
//		CASE REGION_BAY_NEWBORDEAUX
//			// Vehicle
//			CREATE_AND_TASK_VEHICLE_TO_COORD(m_vehWagon1, m_pedDriver1, (<<2610.93530, -1206.73608, 52.13085>>), 150.31, (<<2638.4, -1320.8, 52.8>>))
//			
//			// Peds
//			CREATE_AND_TASK_PED_TO_COORD(0, (<<2546.05908, -1219.45850, 52.25940>>), (<<2544.73389, -1114.34436, 43.44470>>), m_locationData[2].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(1, (<<2558.38086, -1196.30945, 52.26160>>), (<<2463.70654, -1243.48572, 51.67789>>), m_locationData[2].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(2, (<<2558.21729, -1203.56189, 52.17490>>), (<<2686.86328, -1210.76208, 51.23523>>), m_locationData[2].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(3, (<<2614.71802, -1194.78577, 52.12616>>), (<<2634.43115, -1316.03638, 47.43451>>), m_locationData[2].mnPedModel[3])
//			CREATE_AND_TASK_PED_TO_COORD(4, (<<2610.94702, -1282.26807, 51.30997>>), (<<2656.96216, -1142.20410, 52.09549>>), m_locationData[2].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(5, (<<2596.39014, -1236.24707, 52.24875>>), (<<2474.08105, -1196.18640, 50.95575>>), m_locationData[2].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(6, (<<2636.37329, -1183.99426, 52.30649>>), (<<2567.89014, -1313.15955, 49.69562>>), m_locationData[2].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(7, (<<2601.21045, -1198.88635, 52.24863>>), (<<2546.24219, -1204.64380, 52.69946>>), m_locationData[2].mnPedModel[3])
//		BREAK
//		
//		CASE REGION_HRT_VALENTINE
//			// Vehicle
//			CREATE_AND_TASK_VEHICLE_TO_COORD(m_vehWagon1, m_pedDriver1, (<<-290.22406, 790.63641, 117.46346>>), 98.86, (<<-368.9, 771, 117.5>>))
//			
//			// Peds
//			CREATE_AND_TASK_PED_TO_COORD(0, (<<-283.65869, 796.69177, 117.60806>>), (<<-378.18396, 781.88116, 115.22844>>), m_locationData[3].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(1, (<<-294.41443, 794.19464, 117.67975>>), (<<-378.18396, 781.88116, 115.22844>>), m_locationData[3].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(2, (<<-279.20795, 786.62927, 117.64545>>), (<<-378.14630, 768.52130, 115.22289>>), m_locationData[3].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(3, (<<-282.54456, 785.91278, 118.13098>>), (<<-378.14630, 768.52130, 115.22289>>), m_locationData[3].mnPedModel[3])
//			CREATE_AND_TASK_PED_TO_COORD(4, (<<-272.18863, 801.80414, 118.27293>>), (<<-280.76630, 857.42059, 119.32507>>), m_locationData[3].mnPedModel[0])
//			CREATE_AND_TASK_PED_TO_COORD(5, (<<-258.73929, 803.40765, 118.87163>>), (<<-270.48480, 856.29504, 120.12206>>), m_locationData[3].mnPedModel[1])
//			CREATE_AND_TASK_PED_TO_COORD(6, (<<-269.23090, 784.29565, 117.61450>>), (<<-226.23019, 691.12341, 113.04720>>), m_locationData[3].mnPedModel[2])
//			CREATE_AND_TASK_PED_TO_COORD(7, (<<-258.09464, 793.60693, 118.10144>>), (<<-206.42035, 693.08063, 113.23892>>), m_locationData[3].mnPedModel[3])
//		BREAK
//	
//	ENDSWITCH
//ENDPROC

FUNC BOOL IS_RESULTS_SCREEN_FINISHED()
	SWITCH eResultsScreen
		CASE RS_START
			METRICS_SMOKETEST_STOP()
			PRINTLN("SMOKETEST METRICS_SMOKETEST_STOP")
			
			METRICS_SMOKETEST_SAVE_TO_FILE(m_locationData[iCurrentLocationIndex].txtLabelName)
			//RESTART_TIMER_NOW(tmrResultsScreen)
			eResultsScreen = RS_WAIT_FOR_SMOKETEST_SHOW
		BREAK
		
		CASE RS_WAIT_FOR_SMOKETEST_SHOW
			//IF GET_TIMER_IN_SECONDS(tmrResultsScreen) >= 2.0
				METRICS_SMOKETEST_CLEAR()
				//RESTART_TIMER_NOW(tmrResultsScreen)
				eResultsScreen = RS_WAIT_FOR_GRAPH
			//ENDIF
		BREAK
		
		CASE RS_WAIT_FOR_GRAPH
			//IF GET_TIMER_IN_SECONDS(tmrResultsScreen) >= 2.0
				eResultsScreen = RS_DONE
			//ENDIF
		BREAK
		
		CASE RS_DONE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_PEDS_AND_VEHICLES()
//	CLEANUP_PED(m_pedDriver1)
//	CLEANUP_PED(m_pedDriver2)
//	CLEANUP_PED_ARRAY(m_pedAmbients)
	//DELETE_VEHICLE(m_vehWagon1)
	//DELETE_VEHICLE(m_vehWagon2)
ENDPROC

PROC CLEANUP_SCRIPT()

	SET_STREAMING(TRUE)
	
	//INT i
//	FOR i = 0 TO iCONST_NUM_LOCATIONS - 1
//		RESET_CLEARED_AREA_AND_ROADS_BACK_TO_ORIGINAL( m_locationData[i].sbiBlocker, m_locationData[i].vScenarioBlockMin, m_locationData[i].vScenarioBlockMax, 0.0, TRUE )
//	ENDFOR
	
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	METRICS_SMOKETEST_STOP()
	PRINTLN("SMOKETEST METRICS_SMOKETEST_SAVE")
	METRICS_SMOKETEST_CLEAR()
	METRICS_SMOKETEST_HIDE()
	PRINTLN("SMOKETEST SCRIPT TERMINATING")
	CLEANUP_PEDS_AND_VEHICLES()
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL ARE_LOCATION_MODELS_LOADED()
	INT i
	FOR i = 0 TO iCONST_NUM_PED_MODELS - 1
		IF m_locationData[iCurrentLocationIndex].mnPedModel[i] != DUMMY_MODEL_FOR_SCRIPT
			IF NOT HAS_MODEL_LOADED(m_locationData[iCurrentLocationIndex].mnPedModel[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF m_locationData[iCurrentLocationIndex].mnVehModel != DUMMY_MODEL_FOR_SCRIPT
		IF NOT HAS_MODEL_LOADED(m_locationData[iCurrentLocationIndex].mnVehModel)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RELEASE_LOCATION_MODELS()
	INT i
	FOR i = 0 TO iCONST_NUM_PED_MODELS - 1
		IF m_locationData[iCurrentLocationIndex].mnPedModel[i] != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(m_locationData[iCurrentLocationIndex].mnPedModel[i])
		ENDIF
	ENDFOR
	
	IF m_locationData[iCurrentLocationIndex].mnVehModel != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(m_locationData[iCurrentLocationIndex].mnVehModel)
	ENDIF

ENDPROC

PROC REQUEST_LOCATION_MODELS()
	INT i
	FOR i = 0 TO iCONST_NUM_PED_MODELS - 1
		IF m_locationData[iCurrentLocationIndex].mnPedModel[i] != DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(m_locationData[iCurrentLocationIndex].mnPedModel[i])
		ENDIF
	ENDFOR
	
	IF m_locationData[iCurrentLocationIndex].mnVehModel != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(m_locationData[iCurrentLocationIndex].mnVehModel)
	ENDIF
ENDPROC

PROC SETUP_GUNFIGHT()

	REQUEST_MODEL(S_M_Y_COP_01)
	REQUEST_MODEL(A_M_Y_MEXTHUG_01)
	LOAD_ALL_OBJECTS_NOW()

	CopStartPos[0] = << -579.2419, -1656.8274, 18.7912 >>
	CopStartHead[0] = 118
	CopStartPos[1] = << -572.1108, -1662.7108, 18.2502 >>
	CopStartHead[1] = 94
	CopStartPos[2] = << -581.9711, -1671.4646, 18.2560 >>	
	CopStartHead[2] = 94
	CopStartPos[3] = << -566.4862, -1671.4352, 18.2296 >>	
	CopStartHead[3] = 87
	CopStartPos[4] = << -571.9099, -1675.8474, 18.7016 >>
	CopStartHead[4] = 48
	
	REPEAT numgangpeds GANGPED
		pedCop[GANGPED] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, CopStartPos[GANGPED])
		SET_PED_HEADING_AND_PITCH(pedCop[GANGPED], CopStartHead[GANGPED], 0)
		SET_PED_COMBAT_MOVEMENT(pedCop[GANGPED], CM_DEFENSIVE )
		SET_PED_FLEE_ATTRIBUTES(pedCop[GANGPED], FA_USE_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedCop[GANGPED], CA_CAN_CAPTURE_ENEMY_PEDS, FALSE)
		SET_ENTITY_PROOFS(pedCop[GANGPED], TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[GANGPED], relGroupCop)
		GIVE_WEAPON_TO_PED(pedCop[GANGPED], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		SET_CURRENT_PED_WEAPON(pedCop[GANGPED], WEAPONTYPE_PISTOL, TRUE)
	ENDREPEAT

	GangStartPos[0] = << -601.7824, -1674.6423, 18.5608 >> 	
	GangStartHead[0] = 285
	GangStartPos[1] = << -600.7036, -1669.3536, 18.5807 >>	
	GangStartHead[1] = 285
	GangStartPos[2] = << -606.2323, -1669.1875, 18.8904 >>	
	GangStartHead[2] = 274
	GangStartPos[3] = << -605.7510, -1678.3025, 18.6469 >>	
	GangStartHead[3] = 289
	GangStartPos[4] = << -596.6554, -1674.1642, 18.4122 >>
	GangStartHead[4] = 286
	
	REPEAT numgangpeds GANGPED
		pedCriminal[GANGPED] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MEXTHUG_01, GangStartPos[GANGPED])
		SET_PED_HEADING_AND_PITCH(pedCriminal[GANGPED], GangStartHead[GANGPED], 0)
		SET_PED_COMBAT_MOVEMENT(pedCriminal[GANGPED], CM_DEFENSIVE )
		SET_PED_FLEE_ATTRIBUTES(pedCriminal[GANGPED], FA_USE_COVER, TRUE)
		SET_ENTITY_PROOFS(pedCriminal[GANGPED], TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCriminal[GANGPED], relGroupCrim)
		GIVE_WEAPON_TO_PED(pedCriminal[GANGPED], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		SET_CURRENT_PED_WEAPON(pedCriminal[GANGPED], WEAPONTYPE_PISTOL, TRUE)
	ENDREPEAT
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCrim, relGroupCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCop, relGroupCrim)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCrim, RELGROUPHASH_PLAYER)

	REPEAT numgangpeds GANGPED
		REGISTER_HATED_TARGETS_IN_AREA(pedCop[GANGPED], CopStartPos[GANGPED], 200)
		TASK_TURN_PED_TO_FACE_ENTITY(pedCop[GANGPED], pedCriminal[GANGPED])
	ENDREPEAT
	REPEAT numgangpeds GANGPED
		REGISTER_HATED_TARGETS_IN_AREA(pedCriminal[GANGPED], GangStartPos[GANGPED], 200)
		TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[GANGPED], pedCop[GANGPED])
	ENDREPEAT

	AI_SETUP_DONE = TRUE

ENDPROC
#ENDIF // IS_DEBUG_OR_PROFILE_BUILD

SCRIPT
#IF IS_DEBUG_OR_PROFILE_BUILD
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF
	
	g_savedGlobals.sFlow.isGameflowActive = false
	
	WAIT(20000)
	
	g_savedGlobals.sFlow.isGameflowActive = false
	
	IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
		PRINTLN("SMOKETEST SCRIPT MAPONLY STARTING")
	ELSE
		PRINTLN("SMOKETEST SCRIPT STARTING")
	ENDIF

	iCurrentLocationIndex = 0
	iAverageRepeat = 0
	eResultsScreen = RS_START
	eFPSlocationStage = CHOOSE_SETUP
	
	ADD_RELATIONSHIP_GROUP("RE_ARREST_COP", relGroupCop)
	ADD_RELATIONSHIP_GROUP("RE_ARREST_CRIM", relGroupCrim)
	
	WHILE TRUE

		WAIT(0)

		IF IS_PLAYER_PLAYING(PLAYER_ID())
			
			SWITCH eFPSlocationStage
			
				CASE CHOOSE_SETUP
					SET_MAX_WANTED_LEVEL(0)
					//FLUSH_ENTIRE_SCENE()
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					eFPSlocationStage = SETUP
				BREAK
				
				CASE SETUP
					INIT_ALL_LOCATION_DATA()
					bProceed = FALSE
					METRICS_SMOKETEST_CLEAR()
					PRINTLN("SMOKETEST METRICS_SMOKETEST_CLEAR")
					SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), TRUE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					eFPSlocationStage = WARP_TO_LOCATION
					
					// Fade the screen in
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
						PRINTLN("FPS TEST DO_SCREEN_FADE_IN")
					ENDIF

				BREAK
				
				CASE WARP_TO_LOCATION
					PRINTLN("WARP_TO_LOCATION")
					
					SET_MAX_WANTED_LEVEL(0)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					
					IF iCurrentLocationIndex = 9
						SET_MAX_WANTED_LEVEL(5)
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 5)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK,FALSE)
					ENDIF
							
					IF iCurrentLocationIndex = 11
						SET_MAX_WANTED_LEVEL(5)
						SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 5)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK,TRUE)
					ENDIF
						
					REPEAT numgangpeds GANGPED
						IF DOES_ENTITY_EXIST(pedCop[GANGPED])
							SET_PED_AS_NO_LONGER_NEEDED(pedCop[GANGPED])
						ENDIF
					ENDREPEAT
					REPEAT numgangpeds GANGPED
						IF DOES_ENTITY_EXIST(pedCriminal[GANGPED])
							SET_PED_AS_NO_LONGER_NEEDED(pedCriminal[GANGPED])
						ENDIF
					ENDREPEAT
					SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
					SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MEXTHUG_01)
					AI_SETUP_DONE = FALSE
					
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), m_locationData[iCurrentLocationIndex].vPos)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), m_locationData[iCurrentLocationIndex].flHeading)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					flNewHeading = -90
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(flNewHeading)
					
					CLEAR_AREA(m_locationData[iCurrentLocationIndex].vPos, 2000, TRUE)
					LOAD_SCENE(m_locationData[iCurrentLocationIndex].vPos)
					LOAD_ALL_OBJECTS_NOW()
					INSTANTLY_FILL_PED_POPULATION()
					INSTANTLY_FILL_VEHICLE_POPULATION()
					//m_locationData[iCurrentLocationIndex].sbiBlocker = CLEAR_AREA_OF_ALL_OBJECTS_AND_DISABLE_ROADS( m_locationData[iCurrentLocationIndex].vScenarioBlockMin, m_locationData[iCurrentLocationIndex].vScenarioBlockMax, m_locationData[iCurrentLocationIndex].vPos, 1000.0, 0.0, TRUE )
					// Only request ped models if we're not running with -maponly
					
					IF iCurrentLocationIndex = 6
						SET_CLOCK_TIME(0, 0, 0)
						NETWORK_OVERRIDE_CLOCK_TIME(0, 0, 0)
					ELSE
						SET_CLOCK_TIME(17, 0, 0)
						NETWORK_OVERRIDE_CLOCK_TIME(17, 0, 0)
					ENDIF
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
						
					IF NOT GET_COMMANDLINE_PARAM_EXISTS("maponly")
						REQUEST_LOCATION_MODELS()
					ENDIF
					
					SETTIMERA(0)
					bProceed = FALSE
					eFPSlocationStage = WAIT_TO_SETTLE
				BREAK
			
				CASE WAIT_TO_SETTLE
					
					IF iCurrentLocationIndex = 7
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
					
					// If we're running with -maponly, we just wanna wait for the timer, otherwise, also wait for the models to load in
					IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
						IF TIMERA() > iCONST_SETTLE_TIME
							bProceed = TRUE
						ENDIF
					ELSE
						IF TIMERA() > iCONST_SETTLE_TIME
						AND ARE_LOCATION_MODELS_LOADED()
							bProceed = TRUE
						ELSE
							CPRINTLN(DEBUG_MISSION, "WAIT_TO_SETTLE - Waiting on ped models to load")
						ENDIF
					ENDIF
					
					IF bProceed
						IF iAverageRepeat = 0
							METRICS_SMOKETEST_START(m_locationData[iCurrentLocationIndex].txtLabelName)
							PRINTLN("SMOKETEST METRICS_SMOKETEST_START")
						ENDIF
						SETTIMERA(0)
						IF iCurrentLocationIndex = 7
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						ENDIF
						
//						//Only populate the world if we're not running with -maponly
//						IF NOT GET_COMMANDLINE_PARAM_EXISTS("maponly")
//							POPULATE_CURRENT_LOCATION()
//						ENDIF
						eFPSlocationStage = CAMERA_PITCH
						
						IF iCurrentLocationIndex = 8 OR iCurrentLocationIndex = 9
							eFPSlocationStage = CREATE_CAR
						ENDIF
					ENDIF

#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
						SETTIMERA(5000)
						eFPSlocationStage = CLEANUP_ZONE
					ENDIF
#ENDIF // IS_DEBUG_BUILD					
				BREAK

				CASE CAMERA_PITCH
				
					IF iCurrentLocationIndex = 10 or iCurrentLocationIndex = 11
						IF NOT AI_SETUP_DONE
							SETUP_GUNFIGHT()
						ENDIF
					ENDIF
				
					IF TIMERA() > 30
						SETTIMERA(0)
						flNewHeading += flCONST_CAMERA_PITCH_MOD
						IF flNewHeading < 0
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(flNewHeading)
						ELSE
							flNewHeading = 0
							eFPSlocationStage = CAMERA_ROTATE
						ENDIF
					ENDIF

#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
						SETTIMERA(5000)
						eFPSlocationStage = CLEANUP_ZONE
					ENDIF
#ENDIF // IS_DEBUG_BUILD			
				BREAK
				
				CASE CAMERA_ROTATE
				
					IF TIMERA() > 30
						SETTIMERA(0)
						flNewHeading += flCONST_CAMERA_PAN_MOD
						IF flNewHeading < 360
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(flNewHeading)
						ELSE
							flNewHeading = 0
							IF iAverageRepeat < 2
								iAverageRepeat ++
								SETTIMERA(iCONST_SETTLE_TIME)
								//eFPSlocationStage = WAIT_TO_SETTLE
							ELSE
								eFPSlocationStage = CLEANUP_ZONE
								SET_STREAMING(FALSE)
							ENDIF
						ENDIF
					ENDIF

#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
						SETTIMERA(5000)
						eFPSlocationStage = CLEANUP_ZONE
					ENDIF
#ENDIF // IS_DEBUG_BUILD					
				BREAK
				
				CASE CREATE_CAR
					
					PRINTSTRING("...Create car")
					PRINTNL()
						
					IF NOT DOES_ENTITY_EXIST(veh_car)

						model_car = NINEF2
						REQUEST_MODEL(model_car)
							
						IF HAS_MODEL_LOADED(model_car)
							CLEAR_AREA(m_locationData[iCurrentLocationIndex].vPos, 20, TRUE)
							veh_car = CREATE_VEHICLE(model_car, m_locationData[iCurrentLocationIndex].vPos,
											m_locationData[iCurrentLocationIndex].flCarHeading, FALSE)
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(veh_car)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
							SET_ENTITY_COORDS(veh_car, m_locationData[iCurrentLocationIndex].vPos)
							SET_ENTITY_HEADING(veh_car, m_locationData[iCurrentLocationIndex].flCarHeading)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_ENTITY_INVINCIBLE(veh_car, TRUE)
							TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(),veh_car, m_locationData[iCurrentLocationIndex].vCarDestination,20.0,DRIVINGSTYLE_ACCURATE,model_car,DRIVINGMODE_PLOUGHTHROUGH,7.0,1.0)
							SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
						ENDIF
						eFPSlocationStage = CAR_DRIVE
					ENDIF
						
				BREAK
					
				CASE CAR_DRIVE
				
					IF IS_VEHICLE_DRIVEABLE(veh_car) AND FINISHED_TASK = GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD )
						SETTIMERA(0)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
//							IF average_repeat < 2
//								average_repeat ++
//								METRICS_ZONE_STOP()
//								METRICS_ZONES_SHOW()
//								FPSlocationStage = WAIT_TO_SETTLE
//							ELSE
						eFPSlocationStage = CLEANUP_ZONE
//							ENDIF
					ENDIF
					
					IF TIMERA() > 250
						vehicles_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						CLEAR_AREA(vehicles_coords, 12, TRUE)
						SETTIMERA(0)
					ENDIF
					
#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
						SETTIMERA(5000)
						eFPSlocationStage = CLEANUP_ZONE
					ENDIF
#ENDIF // IS_DEBUG_BUILD					
		
				BREAK

				CASE CLEANUP_ZONE
					IF TIMERA() > iCONST_SETTLE_TIME
						IF IS_RESULTS_SCREEN_FINISHED()
							SET_STREAMING(TRUE)
							IF NOT GET_COMMANDLINE_PARAM_EXISTS("maponly")
								CLEANUP_PEDS_AND_VEHICLES()
								RELEASE_LOCATION_MODELS()
							ENDIF
							
							flNewHeading = 0
							iAverageRepeat = 0
							//RESET_CLEARED_AREA_AND_ROADS_BACK_TO_ORIGINAL( m_locationData[iCurrentLocationIndex].sbiBlocker, m_locationData[iCurrentLocationIndex].vScenarioBlockMin, m_locationData[iCurrentLocationIndex].vScenarioBlockMax, 0.0, TRUE )
							iCurrentLocationIndex ++
							
							//FLUSH_ENTIRE_SCENE()
							eResultsScreen = RS_START
							IF iCurrentLocationIndex < iCONST_NUM_LOCATIONS 
								eFPSlocationStage = WARP_TO_LOCATION
							ELSE
								eFPSlocationStage = FPS_DONE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE FPS_DONE
					CLEANUP_SCRIPT()
				BREAK
				
			ENDSWITCH
		
#IF IS_DEBUG_BUILD		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				CLEANUP_SCRIPT()
			ENDIF
#ENDIF // IS_DEBUG_BUILD			
			
		ENDIF
		
	ENDWHILE
#ENDIF // IS_DEBUG_OR_PROFILE_BUILD
ENDSCRIPT
