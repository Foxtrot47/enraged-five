//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	item_ownership_output.sc							//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Used to output list of items player can own and 	//
//		                    their current ownership status.                     //
//																				//
//////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "ped_component_public.sch"
USING "gunclub_shop_private.sch"
USING "net_gang_boss_drops.sch"
USING "net_realty_vehicle_garage.sch"
USING "net_rank_unlocks.sch"

ENUM SCRIPT_STAGE
	SCRIPT_STAGE_INIT = 0,
	SCRIPT_STAGE_PROCESSING,
	SCRIPT_STAGE_CLEANUP
ENDENUM
SCRIPT_STAGE eScriptStage = SCRIPT_STAGE_INIT
BOOL bTerminateScript


CONST_INT ITEM_OUTPUT_CLOTHING 		0
CONST_INT ITEM_OUTPUT_WEAPONS		1
CONST_INT ITEM_OUTPUT_VEHICLES 		2
CONST_INT ITEM_OUTPUT_TATTOOS 		3
CONST_INT ITEM_OUTPUT_PROPERTIES 	4
CONST_INT ITEM_OUTPUT_TYPE_COUNT 	5

ENUM ITEM_OUTPUT_STAGE
	OUTPUT_STAGE_INIT = 0,
	OUTPUT_STAGE_OPEN_FILE,
	OUTPUT_STAGE_PROCESSING,
	OUTPUT_STAGE_CLOSE_FILE
ENDENUM

ENUM ITEM_PROCESSING_STAGE
	PROCESSING_STAGE_1 = 0,
	PROCESSING_STAGE_2,
	PROCESSING_STAGE_3,
	PROCESSING_STAGE_4,
	PROCESSING_STAGE_5,
	PROCESSING_STAGE_END
ENDENUM

STRUCT ITEM_OUTPUT_DATA
	
	INT iWidget_Type
	BOOL bWidget_OutputOwnedItems
	BOOL bWidget_OutputAllItems
	BOOL bWidget_Processing
	
	BOOL bSelection_All
	INT iSelection_Type
	BOOL bSelection_OutputOwnedItems
	BOOL bSelection_OutputAllItems
	
	TEXT_LABEL_63 tlPath
	TEXT_LABEL_63 tlFilename
	
	INT iClothingComp
	INT iClothingProp
	INT iClothingOutfit
	INT iClothingDraw
	BOOL bCountsInitialised
	
	ITEM_OUTPUT_STAGE eOutputStage = OUTPUT_STAGE_INIT
	ITEM_PROCESSING_STAGE eProcessingStage = PROCESSING_STAGE_1
ENDSTRUCT
ITEM_OUTPUT_DATA sOutputData


FUNC STRING GET_ITEM_OUTPUT_TYPE_NAME(INT iType)
	SWITCH iType
		CASE ITEM_OUTPUT_CLOTHING		RETURN "Clothing" BREAK
		CASE ITEM_OUTPUT_WEAPONS		RETURN "Weapons" BREAK
		CASE ITEM_OUTPUT_VEHICLES		RETURN "Vehicles" BREAK
		CASE ITEM_OUTPUT_TATTOOS		RETURN "Tattoos" BREAK
		CASE ITEM_OUTPUT_PROPERTIES 	RETURN "Properties" BREAK
	ENDSWITCH
	RETURN "NA"
ENDFUNC

FUNC STRING GET_ITEM_OUTPUT_HEADER(INT iType)
	SWITCH iType
		CASE ITEM_OUTPUT_CLOTHING		RETURN "Owned,Type,TextLabel,Name" BREAK
		CASE ITEM_OUTPUT_WEAPONS		RETURN "Owned,Type,TextLabel,Name" BREAK
		CASE ITEM_OUTPUT_VEHICLES		RETURN "Owned,Type,TextLabel,Name" BREAK
		CASE ITEM_OUTPUT_TATTOOS		RETURN "Owned,Type,TextLabel,Name" BREAK
		CASE ITEM_OUTPUT_PROPERTIES 	RETURN "Owned,Type,TextLabel,Name" BREAK
	ENDSWITCH
	RETURN "NA"
ENDFUNC

PROC OUTPUT_DEFAULT_ITEM_DATA(STRING sSubType, STRING sTextLabel, BOOL bItemOwned)
	IF sOutputData.bSelection_OutputAllItems
	OR (sOutputData.bSelection_OutputOwnedItems AND bItemOwned)
		// Owned
		SAVE_STRING_TO_DEBUG_FILE(BOOL_TO_STRING(bItemOwned))
		SAVE_STRING_TO_DEBUG_FILE(",")
		// Type
		SAVE_STRING_TO_DEBUG_FILE(sSubType)
		SAVE_STRING_TO_DEBUG_FILE(",")
		// TextLabel
		SAVE_STRING_TO_DEBUG_FILE(sTextLabel)
		SAVE_STRING_TO_DEBUG_FILE(",")
		// Name
		IF DOES_TEXT_LABEL_EXIST(sTextLabel)
			SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sTextLabel))
		ELSE
			SAVE_STRING_TO_DEBUG_FILE(sTextLabel)
		ENDIF
		SAVE_STRING_TO_DEBUG_FILE(",")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
ENDPROC

PROC OUTPUT_WEAPON_MOD_ITEM_DATA(STRING sSubType, STRING sWeaponTextLabel, STRING sComponentTextLabel, BOOL bItemOwned)
	IF sOutputData.bSelection_OutputAllItems
	OR (sOutputData.bSelection_OutputOwnedItems AND bItemOwned)
		// Owned
		SAVE_STRING_TO_DEBUG_FILE(BOOL_TO_STRING(bItemOwned))
		SAVE_STRING_TO_DEBUG_FILE(",")
		// Type
		SAVE_STRING_TO_DEBUG_FILE(sSubType)
		SAVE_STRING_TO_DEBUG_FILE(",")
		// TextLabel
		SAVE_STRING_TO_DEBUG_FILE(sComponentTextLabel)
		SAVE_STRING_TO_DEBUG_FILE(",")
		// Name
		IF DOES_TEXT_LABEL_EXIST(sComponentTextLabel)
			IF DOES_TEXT_LABEL_EXIST(sWeaponTextLabel)
				SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sWeaponTextLabel))
				SAVE_STRING_TO_DEBUG_FILE(": ")
			ENDIF
			SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sComponentTextLabel))
		ELSE
			SAVE_STRING_TO_DEBUG_FILE(sComponentTextLabel)
		ENDIF
		SAVE_STRING_TO_DEBUG_FILE(",")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
ENDPROC

FUNC BOOL PROCESS_CLOTHING_OUTPUT()
	
	BOOL bItemOwned
	INT iTexCount, iTex
	INT iDLCItemCount, iDLCItem
	INT iCurrentPed
	TATTOO_FACTION_ENUM eFaction
	PED_COMP_NAME_ENUM eItem
	PED_COMP_NAME_ENUM eDLCOutfits
	PED_INDEX pedID = PLAYER_PED_ID()
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID)
	sTattooShopItemValues sDLCTattooData
	INT iDrawablesPerFrame
	
	IF ePedModel = MP_M_FREEMODE_01
		iCurrentPed = 3
		eDLCOutfits = OUTFIT_FMM_DLC
		eFaction = TATTOO_MP_FM
	ELIF ePedModel = MP_F_FREEMODE_01
		iCurrentPed = 4
		eDLCOutfits = OUTFIT_FMF_DLC
		eFaction = TATTOO_MP_FM_F
	ENDIF
	
	SWITCH sOutputData.eProcessingStage
		CASE PROCESSING_STAGE_1
			sOutputData.bCountsInitialised = FALSE
			sOutputData.eProcessingStage = PROCESSING_STAGE_2
		BREAK
		CASE PROCESSING_STAGE_2
			/////////////////////////////////////////
			///     Ped Components
			///     
			IF NOT sOutputData.bCountsInitialised
				sOutputData.iClothingComp = 0
				sOutputData.iClothingDraw = 0
				sOutputData.bCountsInitialised = TRUE
			ENDIF
			
			REPEAT 100 iDrawablesPerFrame
				iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, sOutputData.iClothingComp), sOutputData.iClothingDraw)
				REPEAT iTexCount iTex
				
					bItemOwned = FALSE
					eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sOutputData.iClothingDraw, iTex, INT_TO_ENUM(PED_COMP_TYPE_ENUM, sOutputData.iClothingComp))
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, sOutputData.iClothingComp)), eItem)
					
					IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_AVAILABLE_BIT)
						IF DOES_TEXT_LABEL_EXIST(g_sTempCompData[1].sLabel)
							IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_ACQUIRED_BIT))
							OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND GIVE_DLC_CLOTHING_ITEM_FOR_FREE(g_iLastDLCItemNameHash))
								IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
								AND IS_CONTENT_ITEM_LOCKED_BY_SCRIPT(g_iLastDLCItemLockHash, g_iLastDLCItemNameHash, SHOP_PED_COMPONENT)
									// Skip
								ELSE
									bItemOwned = TRUE
								ENDIF
							ENDIF
							
							OUTPUT_DEFAULT_ITEM_DATA(GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, sOutputData.iClothingComp)), g_sTempCompData[1].sLabel, bItemOwned)
						ENDIF
					ENDIF
				ENDREPEAT
				
				sOutputData.iClothingDraw++
				IF sOutputData.iClothingDraw >= GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, sOutputData.iClothingComp))
					sOutputData.iClothingDraw = 0
					sOutputData.iClothingComp++
					IF sOutputData.iClothingComp >= NUM_PED_COMPONENTS
						sOutputData.bCountsInitialised = FALSE
						sOutputData.eProcessingStage = PROCESSING_STAGE_3
						BREAKLOOP
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		CASE PROCESSING_STAGE_3
			/////////////////////////////////////////
			///     Overlays
			///     
			iDLCItemCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
			REPEAT iDLCItemCount iDLCItem
				IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCItem, sDLCTattooData)
					IF sDLCTattooData.UpdateGroup = HASH("torsoDecal")
						IF DOES_TEXT_LABEL_EXIST(sDLCTattooData.Label)
						
							bItemOwned = FALSE
						
							IF IS_MP_TATTOO_UNLOCKED(INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCItem))
							OR GIVE_DLC_CLOTHING_ITEM_FOR_FREE(DEFAULT, sDLCTattooData.Collection, sDLCTattooData.Preset)
								IF NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT(sDLCTattooData.Collection, sDLCTattooData.Preset, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCItem))
									bItemOwned = TRUE
								ENDIF
							ENDIF
							
							OUTPUT_DEFAULT_ITEM_DATA("GRAPHIC", sDLCTattooData.Label, bItemOwned)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			sOutputData.bCountsInitialised = FALSE
			sOutputData.eProcessingStage = PROCESSING_STAGE_4
		BREAK
		CASE PROCESSING_STAGE_4
			/////////////////////////////////////////
			///     Ped Props
			///     
			IF NOT sOutputData.bCountsInitialised
				sOutputData.iClothingProp = 0
				sOutputData.iClothingDraw = 0
				sOutputData.bCountsInitialised = TRUE
			ENDIF
			
			REPEAT 100 iDrawablesPerFrame
				iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, sOutputData.iClothingProp), sOutputData.iClothingDraw)
				REPEAT iTexCount iTex
				
					bItemOwned = FALSE
					eItem = GET_PROP_ITEM_FROM_VARIATIONS(pedID, sOutputData.iClothingDraw, iTex, INT_TO_ENUM(PED_PROP_POSITION, sOutputData.iClothingProp))
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, COMP_TYPE_PROPS, eItem)
					
					IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_AVAILABLE_BIT)
						IF DOES_TEXT_LABEL_EXIST(g_sTempCompData[1].sLabel)
						
							IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_ACQUIRED_BIT))
							OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND GIVE_DLC_CLOTHING_ITEM_FOR_FREE(g_iLastDLCItemNameHash))
								IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
								AND IS_CONTENT_ITEM_LOCKED_BY_SCRIPT(g_iLastDLCItemLockHash, g_iLastDLCItemNameHash, SHOP_PED_PROP)
									// Skip
								ELSE
									bItemOwned = TRUE
								ENDIF
							ENDIF
							
							OUTPUT_DEFAULT_ITEM_DATA(GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, sOutputData.iClothingProp)), g_sTempCompData[1].sLabel, bItemOwned)
						ENDIF
					ENDIF
				ENDREPEAT
				
				sOutputData.iClothingDraw++
				IF sOutputData.iClothingDraw >= GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, sOutputData.iClothingProp))
					sOutputData.iClothingDraw = 0
					sOutputData.iClothingProp++
					IF sOutputData.iClothingProp >= NUMBER_OF_PED_PROP_TYPES
						sOutputData.bCountsInitialised = FALSE
						sOutputData.eProcessingStage = PROCESSING_STAGE_5
						BREAKLOOP
					ENDIF
				ENDIF
			ENDREPEAT
		BREAK
		CASE PROCESSING_STAGE_5
			/////////////////////////////////////////
			///     Outfits
			///     
			iDLCItemCount = SETUP_SHOP_PED_OUTFIT_QUERY(iCurrentPed, ENUM_TO_INT(CLO_SHOP_LOW))
			REPEAT ENUM_TO_INT(eDLCOutfits)+iDLCItemCount iDLCItem
				
				eItem = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eDLCOutfits)+iDLCItem)
				g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, COMP_TYPE_OUTFIT, eItem)
				
				IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_AVAILABLE_BIT)
					IF DOES_TEXT_LABEL_EXIST(g_sTempCompData[1].sLabel)
					
						IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_ACQUIRED_BIT))
						OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND GIVE_DLC_CLOTHING_ITEM_FOR_FREE(g_iLastDLCItemNameHash))
							bItemOwned = TRUE
						ELSE
							bItemOwned = FALSE
						ENDIF
						
						OUTPUT_DEFAULT_ITEM_DATA("OUTFIT", g_sTempCompData[1].sLabel, bItemOwned)
					ENDIF
				ENDIF
			ENDREPEAT
			sOutputData.eProcessingStage = PROCESSING_STAGE_END
		BREAK
	ENDSWITCH
	
	RETURN (sOutputData.eProcessingStage != PROCESSING_STAGE_END)
ENDFUNC

FUNC BOOL PROCESS_WEAPONS_OUTPUT()
		
	INT iDLCIndex
	INT iComponent
	INT iTint
	INT iWeapon
	scrShopWeaponData weaponData
	TEXT_LABEL_15 tlWeaponLabel, tlCompLabel
	WEAPONCOMPONENT_TYPE eComponent
	PLAYERKIT ePlayerKit
	BOOL bItemOwned
	
	WEAPON_TYPE eWeapon = GET_WEAPONTYPE_FROM_ITERATED_INDEX(iWeapon)
    WHILE eWeapon != WEAPONTYPE_INVALID
	
		tlWeaponLabel = GET_WEAPON_NAME(eWeapon)
		
		IF eWeapon != WEAPONTYPE_UNARMED
		AND eWeapon != WEAPONTYPE_ANIMAL
		AND eWeapon != WEAPONTYPE_COUGAR
		AND GET_HASH_KEY(tlWeaponLabel) != GET_HASH_KEY("WT_INVALID")
			
			bItemOwned = FALSE
			
			IF NOT IS_DLC_WEAPON_LOCKED_BY_SCRIPT(eWeapon)
			AND IS_MP_WEAPON_PURCHASED(eWeapon)
			
				bItemOwned = TRUE
				
				iDLCIndex = GET_DLC_WEAPON_DATA_FOR_WEAPON_TYPE(eWeapon, weaponData)
				IF iDLCIndex != -1
				AND GET_DLC_WEAPON_DATA(iDLCIndex, weaponData)
				AND IS_CONTENT_ITEM_LOCKED(weaponData.m_lockHash)
					bItemOwned = FALSE
				ENDIF
			ENDIF
			
			OUTPUT_DEFAULT_ITEM_DATA("WEAPON", tlWeaponLabel, bItemOwned)
			
			iComponent = 0
			WHILE LOOKUP_WEAPON_ADDONS(eComponent, eWeapon, iComponent)
				
				bItemOwned = FALSE
				IF IS_MP_WEAPON_ADDON_PURCHASED(eComponent, eWeapon)
					bItemOwned = TRUE
				ENDIF
				
				IF sOutputData.bSelection_OutputAllItems
				OR (sOutputData.bSelection_OutputOwnedItems AND bItemOwned)
				
					tlCompLabel = GET_WEAPON_COMPONENT_NAME(eComponent, eWeapon)
					
					OUTPUT_WEAPON_MOD_ITEM_DATA("WEAPON_MOD", tlWeaponLabel, tlCompLabel, bItemOwned)
				ENDIF
				
				iComponent++
			ENDWHILE
			
			iTint = 0
			ePlayerKit = LOOKUP_WEAPON_TINT(eWeapon, iTint)
			WHILE ePlayerKit != PLAYERKIT_NONE
				
				bItemOwned = FALSE
				IF IS_MP_KIT_UNLOCKED(ePlayerKit)
					bItemOwned = TRUE
				ENDIF
				
				IF sOutputData.bSelection_OutputAllItems
				OR (sOutputData.bSelection_OutputOwnedItems AND bItemOwned)
				
					tlCompLabel = "TINT_"
					tlCompLabel += iTint
					
					OUTPUT_WEAPON_MOD_ITEM_DATA("WEAPON_TINT", tlWeaponLabel, tlCompLabel, bItemOwned)
				ENDIF
				
				iTint++
				ePlayerKit = LOOKUP_WEAPON_TINT(eWeapon, iTint)
			ENDWHILE
		ENDIF
		
		iWeapon++
		eWeapon = GET_WEAPONTYPE_FROM_ITERATED_INDEX(iWeapon)
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_VEHICLES_OUTPUT()
	BOOl bItemOwned
	INT iVehToCheck
	TEXT_LABEL tlLabel
	
	REPEAT MAX_MP_SAVED_VEHICLES iVehToCheck
		bItemOwned = FALSE
		tlLabel = "NA"
		IF g_MpSavedVehicles[iVehToCheck].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			IF NOT IS_BIT_SET(g_MpSavedVehicles[iVehToCheck].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
			OR IS_BIT_SET(g_MpSavedVehicles[iVehToCheck].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
				bItemOwned = TRUE
				tlLabel = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iVehToCheck].VehicleSetupMP.VehicleSetup.eModel)
			ENDIF
		ENDIF
		OUTPUT_DEFAULT_ITEM_DATA("VEHICLE", tlLabel, bItemOwned)
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_TATTOOS_OUTPUT()

	REQUEST_ADDITIONAL_TEXT("TAT_MNU", MENU_TEXT_SLOT)
	IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("TAT_MNU", MENU_TEXT_SLOT)	
		RETURN TRUE
	ENDIF
	
	BOOL bItemOwned
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	INT i
	REPEAT MAX_NUMBER_OF_TATTOOS i
		bItemOwned = FALSE
		IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, i), eFaction, PLAYER_PED_ID())
			IF sTattooData.iUpgradeGroup != HASH("torsoDecal")
			AND sTattooData.iUpgradeGroup != HASH("hairOverlay")
			AND sTattooData.iUpgradeGroup != HASH("crewLogo")
			AND sTattooData.iUpgradeGroup != HASH("rank")
			AND sTattooData.iPreset != HASH("000_A")
			AND sTattooData.iPreset != HASH("000_B")
			AND sTattooData.iPreset != HASH("000_C")
			AND sTattooData.iPreset != HASH("000_D")
			AND sTattooData.iPreset != HASH("000_E")
			AND sTattooData.iPreset != HASH("000_F")
				IF IS_MP_TATTOO_PURCHASED(sTattooData.eEnum)
					bItemOwned = TRUE
				ENDIF
				OUTPUT_DEFAULT_ITEM_DATA("TATTOO", sTattooData.sLabel, bItemOwned)
			ENDIF
		ENDIF
	ENDREPEAT
	
	sTattooShopItemValues 	sDLCTattooData
	TATTOO_NAME_ENUM 		eDLCTattoo
	
	INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)		
	INT iDLCIndex
	REPEAT iDLCCount iDLCIndex
		bItemOwned = FALSE
		IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
		AND NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
			IF sDLCTattooData.UpdateGroup != HASH("torsoDecal")
			AND sDLCTattooData.UpdateGroup != HASH("hairOverlay")
			AND sDLCTattooData.UpdateGroup != HASH("crewLogo")
			AND sDLCTattooData.UpdateGroup != HASH("rank")
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
				IF IS_MP_TATTOO_PURCHASED(eDLCTattoo)
				AND NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT(sDLCTattooData.Collection, sDLCTattooData.Preset, eDLCTattoo)
					bItemOwned = TRUE
				ENDIF
				OUTPUT_DEFAULT_ITEM_DATA("TATTOO", sDLCTattooData.Label, bItemOwned)
			ENDIF
		ENDIF
	ENDREPEAT
	
	CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, TRUE)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PROPERTIES_OUTPUT()
	
	SIMPLE_INTERIOR_TYPE eSimpleInteriorType
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iSimpleInterior
	
	FACTORY_TYPE eFactoryType
	FACTORY_ID eFactory
	INT iFactory
	INT iFactoryType
	
	INT iPropertyID
	INT iPropertySlot
	
	INT iMod, iCurrentMod
	
	TEXT_LABEL_31 tlCategory, tlName
	
	BOOL bItemOwned
	
	/////////////////////////////////////////
	///     Warehouses
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_WAREHOUSE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_WAREHOUSE_NAME(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Import/Export Garage
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_IE_GARAGE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_IE_GARAGE_NAME_LABEL(GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_IE_GARAGE(PLAYER_ID()) = GET_IMPORT_EXPORT_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Hangar
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_HANGAR
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_HANGAR_NAME_FROM_ID(GET_HANGAR_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_HANGAR(PLAYER_ID()) = GET_HANGAR_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Defunct Base
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_DEFUNCT_BASE_NAME_FROM_ID(GET_DEFUNCT_BASE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID()) = GET_DEFUNCT_BASE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Nightclub / Business HUB
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_BUSINESS_HUB_NAME_FROM_ID(GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_BUSINESS_HUB(PLAYER_ID()) = GET_BUSINESS_HUB_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Arena Wars Garage
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_ARENA_GARAGE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_ARENA_GARAGE_NAME_FROM_ID(GET_ARENA_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_ARENA_GARAGE(PLAYER_ID()) = GET_ARENA_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Casino Apartment
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_CASINO_APT_NAME_FROM_ID(GET_CASINO_APT_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_CASINO_APARTMENT(PLAYER_ID()) = GET_CASINO_APT_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Arcade
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_ARCADE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_ARCADE_NAME_FROM_ID(GET_ARCADE_PROPERTY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID()) = GET_ARCADE_PROPERTY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Auto Shop
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_AUTO_SHOP
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_AUTO_SHOP_NAME_FROM_ID(GET_AUTO_SHOP_PROPERTY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID()) = GET_AUTO_SHOP_PROPERTY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Fixer HQ
	///     
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_FIXER_HQ
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = GET_FIXER_HQ_NAME_FROM_ID(GET_FIXER_HQ_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID()) = GET_FIXER_HQ_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Multistory Garage
	///     
	#IF FEATURE_DLC_2_2022
	eSimpleInteriorType = SIMPLE_INTERIOR_TYPE_MULTISTOREY_GARAGE
	REPEAT ENUM_TO_INT(SIMPLE_INTERIOR_END) iSimpleInterior
		eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, iSimpleInterior)
		IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = eSimpleInteriorType
			tlName = "MULTISTOREY_"
			tlName += ENUM_TO_INT(GET_MULTISTOREY_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			bItemOwned = (GET_PLAYERS_OWNED_MULTISTOREY_GARAGE(PLAYER_ID()) = GET_MULTISTOREY_GARAGE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			OUTPUT_DEFAULT_ITEM_DATA(GET_SIMPLE_INTERIOR_TYPE_DEBUG_NAME(eSimpleInteriorType), tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	#ENDIF
	
	/////////////////////////////////////////
	///     Factories / Businesses
	///     
	FOR iFactoryType = ENUM_TO_INT(FACTORY_TYPE_FAKE_IDS) TO ENUM_TO_INT(FACTORY_TYPE_WEAPONS)
		eFactoryType = INT_TO_ENUM(FACTORY_TYPE, iFactoryType)
		REPEAT ENUM_TO_INT(FACTORY_ID_MAX) iFactory
			eFactory = INT_TO_ENUM(FACTORY_ID, iFactory)
			IF GET_FACTORY_TYPE_FROM_FACTORY_ID(eFactory) = eFactoryType
				tlName = GET_FACTORY_NAME_FROM_ID(eFactory)
				bItemOwned = DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactory)
				
				SWITCH eFactoryType
					CASE FACTORY_TYPE_CRACK			OUTPUT_DEFAULT_ITEM_DATA("FACTORY_CRACK", tlName, bItemOwned) BREAK
					CASE FACTORY_TYPE_METH			OUTPUT_DEFAULT_ITEM_DATA("FACTORY_METH", tlName, bItemOwned) BREAK
					CASE FACTORY_TYPE_FAKE_IDS		OUTPUT_DEFAULT_ITEM_DATA("FACTORY_FAKE_ID", tlName, bItemOwned) BREAK
					CASE FACTORY_TYPE_FAKE_MONEY	OUTPUT_DEFAULT_ITEM_DATA("FACTORY_FAKE_CASH", tlName, bItemOwned) BREAK
					CASE FACTORY_TYPE_WEED			OUTPUT_DEFAULT_ITEM_DATA("FACTORY_WEED", tlName, bItemOwned) BREAK
					CASE FACTORY_TYPE_WEAPONS		OUTPUT_DEFAULT_ITEM_DATA("FACTORY_WEAPONS", tlName, bItemOwned) BREAK
				ENDSWITCH
			ENDIF
		ENDREPEAT
	ENDFOR
	
	/////////////////////////////////////////
	///     Yacht
	///     
	OUTPUT_DEFAULT_ITEM_DATA("YACHT", "YACHT", DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID()))
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_MOD)
	REPEAT 16 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "MOD_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("YACHT", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FIXTURE)
	REPEAT 2 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "FIXTURE_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("YACHT", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_LIGHTING)
	REPEAT 8 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "LIGHTING_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("YACHT", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FLAG)
	REPEAT 46 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "FLAG_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("YACHT", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_COLOR)
	REPEAT 16 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "COLOR_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("YACHT", tlName, bItemOwned)
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Subarmarine - Kosatka
	///     
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "KOSATKA", DOES_PLAYER_OWN_A_SUBMARINE(PLAYER_ID()))
	
	iCurrentMod = GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_COLOUR))
	REPEAT 16 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "COLOR_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_FLAG))
	REPEAT 46 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "FLAG_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", tlName, bItemOwned)
	ENDREPEAT
	
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "MISSILES", GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_MISSILES)) != 0)
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "SONAR", GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_SONAR)) != 0)
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "WEAPON_STATION", GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_WEAPON_STATION)) != 0)
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "SEASPARROW2", GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_SEASPARROW2)) != 0)
	OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", "AVISA", GET_PACKED_STAT_INT(GET_STATS_PACKED_FOR_KOSATKA_UPGRADE(eKOSATKA_MOD_AVISA)) != 0)
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_C)
	REPEAT 30 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "AVISA_COLOR_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_W)
	REPEAT 3 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "AVISA_WEAPON_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_INT_SUB_HELI_CM)
	REPEAT 3 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "AVISA_COUNTER_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("KOSATKA", tlName, bItemOwned)
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Hacker Truck - Terrorbyte
	///     
	OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", "TERRORBYTE", IS_HACKER_TRUCK_PURCHASED())
	OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", "DRONE", IS_PLAYER_HACKER_TRUCK_DRONE_STATION_PURCHASED(PLAYER_ID()))
	OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", "MISSILE", IS_PLAYER_HACKER_TRUCK_MISSILE_LAUNCHER_PURCHASED(PLAYER_ID()))
	OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", "BIKE_WORKSHOP", IS_PLAYER_HACKER_TRUCK_MOTORCYCLE_WORKSHOP_PURCHASED(PLAYER_ID()))
	OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", "WEAPON_WORKSHOP", IS_PLAYER_HACKER_TRUCK_WEAPON_STATION_PURCHASED(PLAYER_ID()))
	
	iCurrentMod = GET_PLAYER_HACKER_TRUCK_VEHICLE_INTERIOR_DECAL_PURCHASED(PLAYER_ID())
	REPEAT 25 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "INTERIOR_DECAL_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", tlName, bItemOwned)
	ENDREPEAT
	
	iCurrentMod = GET_PLAYER_HACKER_TRUCK_VEHICLE_INTERIOR_TINT_PURCHASED(PLAYER_ID())
	REPEAT 9 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "INTERIOR_TINT_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("TERRORBYTE", tlName, bItemOwned)
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Armory Truck - MOC
	///     
	OUTPUT_DEFAULT_ITEM_DATA("MOC", "MOC", IS_GUNRUNNING_TRUCK_PURCHASED())
	OUTPUT_DEFAULT_ITEM_DATA("MOC", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(PHANTOM3), GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0) = ENUM_TO_INT(PHANTOM3))
	OUTPUT_DEFAULT_ITEM_DATA("MOC", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(HAULER2), GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0) = ENUM_TO_INT(HAULER2))
	
	iCurrentMod = GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_1_NUM)
	FOR iMod = AT_STI_WHITE_LIGTH_GREY TO AT_STI_BLACK_BLUE
		bItemOwned = (iMod = iCurrentMod)
		tlName = GET_ARMORY_TRUCK_SECTION_TINT_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, iMod))
		OUTPUT_DEFAULT_ITEM_DATA("MOC", tlName, bItemOwned)
	ENDFOR
	
	REPEAT AT_ST_MAX_SECTION_TYPES iMod
		bItemOwned = FALSE
		IF  iMod != ENUM_TO_INT(AT_ST_EMPTY_SINGLE)
		AND iMod != ENUM_TO_INT(AT_ST_EMPTY_DOUBLE)
		AND iMod != ENUM_TO_INT(AT_ST_EMPTY_SINGLE_DOOR)
		AND iMod != ENUM_TO_INT(AT_ST_UNDEFINED)
			IF GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_1_TYPE) = iMod
			OR GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_2_TYPE) = iMod
			OR GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_3_TYPE) = iMod
				bItemOwned = TRUE
			ENDIF
			tlName = GET_ARMORY_TRUCK_SECTION_TYPE_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, iMod))
			OUTPUT_DEFAULT_ITEM_DATA("MOC", tlName, bItemOwned)
		ENDIF
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Armory Aircraft - Avenger
	///     
	OUTPUT_DEFAULT_ITEM_DATA("AVENGER", "AVENGER", IS_ARMORY_AIRCRAFT_PURCHASED())
	OUTPUT_DEFAULT_ITEM_DATA("AVENGER", "TURRET", IS_PLAYER_ARMORY_AIRCRAFT_GUN_TURRET_PURCHASED(PLAYER_ID()))
	OUTPUT_DEFAULT_ITEM_DATA("AVENGER", "VEHICLE_WORKSHOP", IS_PLAYER_ARMORY_AIRCRAFT_VEHICLE_WORKSHOP_PURCHASED(PLAYER_ID()))
	OUTPUT_DEFAULT_ITEM_DATA("AVENGER", "WEAPON_WORKSHOP", IS_PLAYER_ARMORY_AIRCRAFT_WEAPON_WORKSHOP_PURCHASED(PLAYER_ID()))
	iCurrentMod = GET_PLAYER_ARMORY_AIRCRAFT_VEHICLE_INTERIOR_PURCHASED(PLAYER_ID())
	REPEAT 9 iMod
		bItemOwned = (iMod = iCurrentMod)
		tlName = "INTERIOR_"
		tlName += iMod
		OUTPUT_DEFAULT_ITEM_DATA("AVENGER", tlName, bItemOwned)
	ENDREPEAT
	
	/////////////////////////////////////////
	///     Acid Lab
	///     
	OUTPUT_DEFAULT_ITEM_DATA("ACID_LAB", "ACID_LAB", IS_ACID_LAB_PURCHASED())
	
	/////////////////////////////////////////
	///     Properties
	///     
	FOR iPropertyID = 1 TO MAX_MP_PROPERTIES
	
		bItemOwned = FALSE
		
		REPEAT MAX_OWNED_PROPERTIES iPropertySlot
			IF GET_OWNED_PROPERTY(iPropertySlot) = iPropertyID
				bItemOwned = TRUE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF IS_PROPERTY_YACHT_APARTMENT(iPropertyID)
			tlCategory = "YACHT"
		ELIF IS_PROPERTY_CUSTOM_APARTMENT(iPropertyID)
			tlCategory = "CUSTOM_APT"
		ELIF IS_PROPERTY_STILT_APARTMENT(iPropertyID)
			tlCategory = "STILT_APT"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_LARGE_APT
			tlCategory = "APT_LARGE"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_MED_APT
			tlCategory = "APT_MEDIUM"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_SMALL_APT
			tlCategory = "APT_SMALL"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_OFFICE_GARAGE
			tlCategory = "OFFICE_GARAGE"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_10_GAR
			tlCategory = "10_CAR_GARAGE"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_6_GAR
			tlCategory = "6_CAR_GARAGE"
		ELIF GET_PROPERTY_SIZE_TYPE(iPropertyID) = PROP_SIZE_TYPE_2_GAR
			tlCategory = "2_CAR_GARAGE"
		ELIF IS_PROPERTY_OFFICE(iPropertyID)
			tlCategory = "OFFICE"
		ELIF IS_PROPERTY_CLUBHOUSE(iPropertyID)
			tlCategory = "CLUBHOUSE"
		ELSE
			RELOOP
		ENDIF
		
		tlName = GET_PROPERTY_NAME(iPropertyID)
		
		OUTPUT_DEFAULT_ITEM_DATA(tlCategory, tlName, bItemOwned)
	ENDFOR
	
	// CLUBHOUSE MODS
	//<Item>MP_STAT_PROP_CLUBHOUSE_VAR_v0</Item>
	//<Item>MP_STAT_CLBHOS_WALL_v0</Item>			IS_CLUBHOUSE_WALL_B_PURCHASED()
	//<Item>MP_STAT_CLBHOS_HANGING_v0</Item>		IS_CLUBHOUSE_HANGING_B_PURCHASED()
	//<Item>MP_STAT_CLBHOS_FURNITURE_v0</Item>		IS_CLUBHOUSE_FURNISHINGS_B_PURCHASED()
	//<Item>MP_STAT_CLBHOS_FONT_v0</Item>
	//<Item>MP_STAT_CLBHOS_COLOUR_v0</Item>
	//<Item>MP_STAT_CLBHOS_EMBLEM_v0</Item>
	//<Item>MP_STAT_CLBHOS_SINAGEHIDE_v0</Item>		IS_CLUBHOUSE_SINAGEHIDE_PURCHASED()
	//<Item>MP_STAT_CLBHOS_GUNLOCKER_v0</Item>		IS_CLUBHOUSE_GUN_LOCKER_PURCHASED()
	//<Item>MP_STAT_CLBHOS_GARAGE_v0</Item>			IS_CLUBHOUSE_GARAGE_PURCHASED()
	//<Item>MP_STAT_PROP_CLBHOSE_NAME_ID_v0</Item>
	//<Item>MP_STAT_BIKER_CLIENT_VEHICLE_v0</Item>
	//<Item>MP_STAT_MISSION_BIKER_CLIENT_v0</Item>

	// OFFICE GARAGE MODS
	//<Item>MP_STAT_PROP_OFFICE_GAR1_VAR_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR1_LIGHTING_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR1_NUMBERING_v0</Item>

	// OFFICE GARAGE MODS
	//<Item>MP_STAT_PROP_OFFICE_GAR2_VAR_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR2_LIGHTING_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR2_NUMBERING_v0</Item>

	// OFFICE GARAGE MODS
	//<Item>MP_STAT_PROP_OFFICE_GAR3_VAR_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR3_LIGHTING_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_GAR3_NUMBERING_v0</Item>

	// OFFICE MODS
	//<Item>MP_STAT_PROP_OFFICE_VAR_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_ACCOMMODATION_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_LOCKER_CASH_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_LOCKER_GUN_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_NAME_ID_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_PERSONNEL_v0</Item>
	//<Item>MP_STAT_FONT_PLAYER_OFFICE_v0</Item>
	//<Item>MP_STAT_COLOUR_PLAYER_OFFICE_v0</Item>
	//<Item>MP_STAT_PROP_OFFICE_MODSHOP_v0</Item>
	
	RETURN FALSE
ENDFUNC

PROC DO_INITIALISE()
	START_WIDGET_GROUP("Item Ownership Output")
	
		START_NEW_WIDGET_COMBO()
			INT iType
			REPEAT ITEM_OUTPUT_TYPE_COUNT iType
				ADD_TO_WIDGET_COMBO(GET_ITEM_OUTPUT_TYPE_NAME(iType))
			ENDREPEAT
			ADD_TO_WIDGET_COMBO("All")
		STOP_WIDGET_COMBO("Item Type", sOutputData.iWidget_Type)
		ADD_WIDGET_BOOL("Output owned items", sOutputData.bWidget_OutputOwnedItems)
		ADD_WIDGET_BOOL("Output all items", sOutputData.bWidget_OutputAllItems)
		ADD_WIDGET_BOOL("Processing", sOutputData.bWidget_Processing)
		ADD_WIDGET_BOOL("Terminate script", bTerminateScript)
	STOP_WIDGET_GROUP()
	
	eScriptStage = SCRIPT_STAGE_PROCESSING
ENDPROC

PROC DO_PROCESSING()

	IF bTerminateScript
		eScriptStage = SCRIPT_STAGE_CLEANUP
		EXIT
	ENDIF
	
	sOutputData.bWidget_Processing = FALSE
	
	SWITCH sOutputData.eOutputStage
		CASE OUTPUT_STAGE_INIT
			IF sOutputData.bWidget_OutputOwnedItems
			OR sOutputData.bWidget_OutputAllItems
				sOutputData.iSelection_Type = sOutputData.iWidget_Type
				sOutputData.bSelection_OutputOwnedItems = sOutputData.bWidget_OutputOwnedItems
				sOutputData.bSelection_OutputAllItems = sOutputData.bWidget_OutputAllItems
				
				IF sOutputData.iSelection_Type = ITEM_OUTPUT_TYPE_COUNT
					sOutputData.iSelection_Type = 0
					sOutputData.bSelection_All = TRUE
				ELSE
					sOutputData.bSelection_All = FALSE
				ENDIF
				
				sOutputData.bWidget_OutputAllItems = FALSE
				sOutputData.bWidget_OutputOwnedItems = FALSE
				sOutputData.bWidget_Processing = TRUE
				
				sOutputData.eOutputStage = OUTPUT_STAGE_OPEN_FILE
			ENDIF
		BREAK
		CASE OUTPUT_STAGE_OPEN_FILE
			
			sOutputData.tlPath = "X:/ItemOwnership"
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
				sOutputData.tlFilename = "M_ItemOwnership_"
			ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				sOutputData.tlFilename = "F_ItemOwnership_"
			ELSE
				sOutputData.tlFilename = "ItemOwnership_"
			ENDIF
			sOutputData.tlFilename += GET_ITEM_OUTPUT_TYPE_NAME(sOutputData.iSelection_Type)
			sOutputData.tlFilename += ".csv"
			
			CLEAR_NAMED_DEBUG_FILE(sOutputData.tlPath, sOutputData.tlFilename)
			OPEN_NAMED_DEBUG_FILE(sOutputData.tlPath, sOutputData.tlFilename)
			SAVE_STRING_TO_DEBUG_FILE(GET_ITEM_OUTPUT_HEADER(sOutputData.iSelection_Type))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			sOutputData.eOutputStage = OUTPUT_STAGE_PROCESSING
			sOutputData.eProcessingStage = PROCESSING_STAGE_1
		BREAK
		CASE OUTPUT_STAGE_PROCESSING
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SWITCH sOutputData.iSelection_Type
					CASE ITEM_OUTPUT_CLOTHING		sOutputData.bWidget_Processing = PROCESS_CLOTHING_OUTPUT() 		BREAK
					CASE ITEM_OUTPUT_WEAPONS		sOutputData.bWidget_Processing = PROCESS_WEAPONS_OUTPUT() 		BREAK
					CASE ITEM_OUTPUT_VEHICLES		sOutputData.bWidget_Processing = PROCESS_VEHICLES_OUTPUT() 		BREAK
					CASE ITEM_OUTPUT_TATTOOS		sOutputData.bWidget_Processing = PROCESS_TATTOOS_OUTPUT() 		BREAK
					CASE ITEM_OUTPUT_PROPERTIES 	sOutputData.bWidget_Processing = PROCESS_PROPERTIES_OUTPUT() 	BREAK
				ENDSWITCH
				
				IF NOT sOutputData.bWidget_Processing
					sOutputData.eOutputStage = OUTPUT_STAGE_CLOSE_FILE
				ENDIF
			ENDIF
		BREAK
		CASE OUTPUT_STAGE_CLOSE_FILE
			CLOSE_DEBUG_FILE()
			
			IF sOutputData.bSelection_All AND sOutputData.iSelection_Type < ITEM_OUTPUT_TYPE_COUNT-1
				sOutputData.iSelection_Type++
				sOutputData.eOutputStage = OUTPUT_STAGE_OPEN_FILE
			ELSE
				sOutputData.eOutputStage = OUTPUT_STAGE_INIT
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC DO_CLEANUP()

	IF sOutputData.eOutputStage = OUTPUT_STAGE_PROCESSING
		CLOSE_DEBUG_FILE()
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	WHILE TRUE
	
		WAIT(0)
		
		SWITCH eScriptStage
			CASE SCRIPT_STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE SCRIPT_STAGE_PROCESSING
				DO_PROCESSING()
			BREAK
			
			CASE SCRIPT_STAGE_CLEANUP
				DO_CLEANUP()
			BREAK
		ENDSWITCH
	ENDWHILE
	
ENDSCRIPT

#ENDIF
