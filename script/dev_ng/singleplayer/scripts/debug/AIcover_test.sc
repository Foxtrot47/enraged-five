USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
SCRIPT 
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_clock.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "commands_misc.sch"

CONST_INT numlocation 5
INT index
VECTOR GangStartPos[numlocation], CopStartPos[numlocation], player_start_pos
FLOAT GangStartHead[numlocation], CopStartHead[numlocation]
PED_INDEX pedCop[numlocation], pedCriminal[numlocation]
BOOL density, MetricsEnd

//FLOAT new_heading
REL_GROUP_HASH relGroupCop, relGroupCrim

ENUM FPS_TEST_STAGE
	SETUP,
	WAIT_FOR_CLEANUP
ENDENUM
FPS_TEST_STAGE GangStartPosStage = SETUP

PROC CLEANUP_SCRIPT()
	METRICS_ZONES_CLEAR()
	METRICS_ZONES_HIDE()
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		CLEANUP_SCRIPT()
	ENDIF

	PRINTLN("AIcover_test SCRIPT STARTING")
	
	WHILE TRUE
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH GangStartPosStage
				
					CASE SETUP
						
						MetricsEnd = FALSE
						
						SET_VEHICLE_POPULATION_BUDGET(0)
						SET_PED_POPULATION_BUDGET(0)
						
						REQUEST_MODEL(S_M_Y_COP_01)
						REQUEST_MODEL(A_M_Y_MEXTHUG_01)
						LOAD_ALL_OBJECTS_NOW()
						
						player_start_pos = << -592.5580, -1652.5548, 23.4580 >>
						CLEAR_AREA(player_start_pos, 5000, TRUE, TRUE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), player_start_pos)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 195)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_CLOCK_TIME(17, 0, 0)
						LOAD_SCENE(player_start_pos)

						ADD_RELATIONSHIP_GROUP("RE_ARREST_COP", relGroupCop)
						ADD_RELATIONSHIP_GROUP("RE_ARREST_CRIM", relGroupCrim)

						CopStartPos[0] = << -579.2419, -1656.8274, 18.7912 >>
						CopStartHead[0] = 118
						CopStartPos[1] = << -572.1108, -1662.7108, 18.2502 >>
						CopStartHead[1] = 94
						CopStartPos[2] = << -581.9711, -1671.4646, 18.2560 >>	
						CopStartHead[2] = 94
						CopStartPos[3] = << -566.4862, -1671.4352, 18.2296 >>	
						CopStartHead[3] = 87
						CopStartPos[4] = << -571.9099, -1675.8474, 18.7016 >>
						CopStartHead[4] = 48
						
						REPEAT numlocation index
							pedCop[index] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, CopStartPos[index])
							SET_PED_HEADING_AND_PITCH(pedCop[index], CopStartHead[index], 0)
							SET_PED_COMBAT_MOVEMENT(pedCop[index], CM_DEFENSIVE )
							SET_PED_FLEE_ATTRIBUTES(pedCop[index], FA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedCop[index], CA_CAN_CAPTURE_ENEMY_PEDS, FALSE)
							SET_ENTITY_PROOFS(pedCop[index], TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[index], relGroupCop)
							GIVE_WEAPON_TO_PED(pedCop[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
							SET_CURRENT_PED_WEAPON(pedCop[index], WEAPONTYPE_PISTOL, TRUE)
						ENDREPEAT

						GangStartPos[0] = << -601.7824, -1674.6423, 18.5608 >> 	
						GangStartHead[0] = 285
						GangStartPos[1] = << -600.7036, -1669.3536, 18.5807 >>	
						GangStartHead[1] = 285
						GangStartPos[2] = << -606.2323, -1669.1875, 18.8904 >>	
						GangStartHead[2] = 274
						GangStartPos[3] = << -605.7510, -1678.3025, 18.6469 >>	
						GangStartHead[3] = 289
						GangStartPos[4] = << -596.6554, -1674.1642, 18.4122 >>
						GangStartHead[4] = 286
						
						REPEAT numlocation index
							pedCriminal[index] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MEXTHUG_01, GangStartPos[index])
							SET_PED_HEADING_AND_PITCH(pedCriminal[index], GangStartHead[index], 0)
							SET_PED_COMBAT_MOVEMENT(pedCriminal[index], CM_DEFENSIVE )
							SET_PED_FLEE_ATTRIBUTES(pedCriminal[index], FA_USE_COVER, TRUE)
							SET_ENTITY_PROOFS(pedCriminal[index], TRUE, TRUE, TRUE, TRUE, TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedCriminal[index], relGroupCrim)
							GIVE_WEAPON_TO_PED(pedCriminal[index], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
							SET_CURRENT_PED_WEAPON(pedCriminal[index], WEAPONTYPE_PISTOL, TRUE)
						ENDREPEAT
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCrim, relGroupCop)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCop, relGroupCrim)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCop, RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCrim, RELGROUPHASH_PLAYER)
	
						REPEAT numlocation index
							REGISTER_HATED_TARGETS_IN_AREA(pedCop[index], CopStartPos[index], 200)
							TASK_TURN_PED_TO_FACE_ENTITY(pedCop[index], pedCriminal[index])
						ENDREPEAT
						REPEAT numlocation index
							REGISTER_HATED_TARGETS_IN_AREA(pedCriminal[index], GangStartPos[index], 200)
							TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[index], pedCop[index])
						ENDREPEAT						
						METRICS_ZONE_START("Physics")
						METRICS_ZONES_SHOW()
						SETTIMERA(0)
						GangStartPosStage = WAIT_FOR_CLEANUP
						
					BREAK
				
					CASE WAIT_FOR_CLEANUP
						
						IF NOT MetricsEnd
							IF TIMERA() > 30000

								METRICS_ZONE_STOP()
								METRICS_ZONE_SAVE_TO_FILE("PhysicsTest")
								MetricsEnd = TRUE
							ENDIF
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
							REPEAT numlocation index
								IF NOT IS_PED_INJURED(pedCop[index])
									SET_PED_AS_NO_LONGER_NEEDED(pedCop[index])
								ENDIF
							ENDREPEAT
							SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
							
							REPEAT numlocation index
								IF NOT IS_PED_INJURED(pedCriminal[index])
									SET_PED_AS_NO_LONGER_NEEDED(pedCriminal[index])
								ENDIF
							ENDREPEAT				
							SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MEXTHUG_01)
							
							REMOVE_RELATIONSHIP_GROUP(relGroupCop)
							REMOVE_RELATIONSHIP_GROUP(relGroupCrim)	
							METRICS_ZONE_STOP()
							METRICS_ZONES_CLEAR()
							CLEAR_AREA(player_start_pos, 5000, TRUE, TRUE)
							
							GangStartPosStage = SETUP
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
						OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
							METRICS_ZONE_STOP()
							CLEANUP_SCRIPT()
						ENDIF
					
						IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T))
							IF NOT density
								SET_VEHICLE_POPULATION_BUDGET(3)
								SET_PED_POPULATION_BUDGET(3)
								PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Traffic ON", 1000, 1)
								WAIT(500)
								density = TRUE
							ELSE
								SET_VEHICLE_POPULATION_BUDGET(0)
								SET_PED_POPULATION_BUDGET(0)
								CLEAR_AREA(player_start_pos, 5000, TRUE, TRUE)
								PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Traffic OFF", 1000, 1)
								WAIT(500)
								density = FALSE
							ENDIF
						ENDIF
					
						IF NOT density
							SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
							SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
							SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
							SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
						ELSE
							SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
							SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
							SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(1)
							SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(1, 1)
						ENDIF		
								
						PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Press P to start test. F to fail, T to toggle traffic", 1000, 1)
									
					BREAK
								
				ENDSWITCH

			ENDIF
			

		
	ENDWHILE
ENDSCRIPT

#ENDIF

