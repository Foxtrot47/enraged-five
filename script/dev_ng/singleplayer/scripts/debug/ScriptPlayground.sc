// ScriptPlayground.sc

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// For now, ignore these preprocessor directives. Just know that this script only runs in debug.
#IF IS_DEBUG_BUILD
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"

USING "ScriptPlayground.sch"

// ************************************ DEFINE GLOBAL VARIABLES ***************************************

// ****************************************************************************************************


/// PURPOSE:
///    Kills this script, and cleans all of it's assets.
PROC SCRIPT_CLEANUP()
	PRINTLN("************** ScriptPlayground.sc TERMINATING **************")
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC


// Main script body.
SCRIPT
	PRINTLN("************** ScriptPlayground.sc STARTING **************")
	
	// Initial, one time, major script setup/variable initiations.
	SCRIPTPLAYGROUND_STATE 	eState = PLAYGROUNDSTATE_Init
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// This will run this script forever, until it's been forced to cleanup.
	WHILE TRUE
		DRAW_DEBUG_TEXT_2D("Script Playground Running!", <<0.8, 0.92, 0>>)
		
		// Force a quit if F is pressed.
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Quit Script Playground")
			SCRIPT_CLEANUP()
		ENDIF
		
		// Other quit conditions could go here.
		
		// General state-based update
		SWITCH (eState)
			CASE PLAYGROUNDSTATE_Init
				// Run init items.
				
				// Go to the next state.
				eState = PLAYGROUNDSTATE_Request
			BREAK
			
			CASE PLAYGROUNDSTATE_Request
				// Things you could request.
				//REQUEST_MODEL()
				//REQUEST_SCRIPT_AUDIO_BANK()
				//REQUEST_SCRIPT()
				//REQUEST_SCALEFORM_MOVIE()
				//REQUEST_STREAMED_TEXTURE_DICT()
				
				// Go to the next state.
				eState = PLAYGROUNDSTATE_Stream
			BREAK
			
			CASE PLAYGROUNDSTATE_Stream
				// Things you could check for streaming.
				// These will all go below, in that "IF //Done Streaming" check.
				//HAS_MODEL_LOADED()
				//REQUEST_SCRIPT_AUDIO_BANK()		// This function is unique. It's both a request and a "has this requests loaded".
				//HAS_SCRIPT_LOADED()
				//HAS_SCALEFORM_MOVIE_LOADED()
				//HAS_STREAMED_TEXTURE_DICT_LOADED() 
				
				
				//IF //Done Streaming
					// Go to the next state.
					eState = PLAYGROUNDSTATE_Spawn
				//ENDIF
			BREAK
			
			CASE PLAYGROUNDSTATE_Spawn
				// Spawn anything needed here.
				
				// Go to the next state.
				eState = PLAYGROUNDSTATE_Update
			BREAK
			
			CASE PLAYGROUNDSTATE_Update
				// Will return here forever, and process anything in here, every frame.
			BREAK
		ENDSWITCH
		
		// Wait a wait(0), this script will run every frame. Should be the only wait in here.
		WAIT(0)
	ENDWHILE
ENDSCRIPT
#ENDIF


