// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "ped_component_public.sch"
USING "freemode_header.sch"

ENUM DEBUG_STAGE
	STAGE_INIT = 0,
	STAGE_PROCESSING,
	STAGE_CLEANUP
ENDENUM
DEBUG_STAGE eStage = STAGE_INIT

CONST_INT NUM_DEBUG_CLONES	79

BOOL bKillScript
BOOL bWarpToLSIA
BOOL bCreateAllClones
BOOL bDeleteAllClones

// Clones
PED_INDEX piClone[NUM_DEBUG_CLONES]

BOOL bAutomateOutfitChangingOutfit
BOOL bAutomateOutfitChangingClone
BOOL bAutomateOutfitChangingCloneOneByOne

INT iInterval = 3000
INT iNumPedsToCreate = 31
INT iCloneStagger = 0

SCRIPT_TIMER AutomationTimer

// Functions

PROC DO_INITIALISE()

	START_WIDGET_GROUP("#Debug Clone Outfit Testing")
	
		ADD_WIDGET_BOOL("Kill Script", bKillScript)
		
		ADD_WIDGET_BOOL("Warp to LSIA", bWarpToLSIA)
	
		START_WIDGET_GROUP("Clone Creation & Deletion")
			ADD_WIDGET_STRING("---- All Clones ----")
			ADD_WIDGET_INT_SLIDER("Num Clones to Create", iNumPedsToCreate, 1, (NUM_DEBUG_CLONES-1), 1)
			ADD_WIDGET_BOOL("Create All Peds", bCreateAllClones)
			ADD_WIDGET_BOOL("Delete All Peds", bDeleteAllClones)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Clone outfits")
			ADD_WIDGET_STRING("-- ONLY SELECT ONE OF THE BELOW AT A TIME, BOTH CANNOT BE RUNNING --")
			ADD_WIDGET_BOOL("Automate Outfit changing after interval (set outfits on each ped)", bAutomateOutfitChangingOutfit)
			ADD_WIDGET_BOOL("Automate Outfit changing after interval (clone the local player, do all peds every x seconds)", bAutomateOutfitChangingClone)
			ADD_WIDGET_BOOL("Automate Outfit changing after interval (clone the local player, do each ped one by one every x seconds)", bAutomateOutfitChangingCloneOneByOne)
			
			ADD_WIDGET_STRING("----------------------------------------------------")
			ADD_WIDGET_INT_SLIDER("Interval", iInterval, 1000, 20000, 500)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	
	eStage = STAGE_PROCESSING
ENDPROC

FUNC VECTOR GET_CLONE_CREATION_OFFSET(INT iLoop)

	VECTOR vOffset
	
	SWITCH iLoop
		CASE 0			vOffset = <<1, 0, 0>>		BREAK
		CASE 1			vOffset = <<2, 0, 0>>		BREAK
		CASE 2			vOffset = <<3, 0, 0>>		BREAK
		CASE 3			vOffset = <<4, 0, 0>>		BREAK
		CASE 4			vOffset = <<5, 0, 0>>		BREAK
		CASE 5			vOffset = <<6, 0, 0>>		BREAK
		CASE 6			vOffset = <<7, 0, 0>>		BREAK
		CASE 7			vOffset = <<0, -1, 0>>		BREAK
		CASE 8			vOffset = <<1, -1, 0>>		BREAK
		CASE 9			vOffset = <<2, -1, 0>>		BREAK
		CASE 10			vOffset = <<3, -1, 0>>		BREAK
		CASE 11			vOffset = <<4, -1, 0>>		BREAK
		CASE 12			vOffset = <<5, -1, 0>>		BREAK
		CASE 13			vOffset = <<6, -1, 0>>		BREAK
		CASE 14			vOffset = <<7, -1, 0>>		BREAK
		CASE 15			vOffset = <<0, -2, 0>>		BREAK
		CASE 16			vOffset = <<1, -2, 0>>		BREAK
		CASE 17			vOffset = <<2, -2, 0>>		BREAK
		CASE 18			vOffset = <<3, -2, 0>>		BREAK
		CASE 19			vOffset = <<4, -2, 0>>		BREAK
		CASE 20			vOffset = <<5, -2, 0>>		BREAK
		CASE 21			vOffset = <<6, -2, 0>>		BREAK
		CASE 22			vOffset = <<7, -2, 0>>		BREAK
		CASE 23			vOffset = <<0, -3, 0>>		BREAK
		CASE 24			vOffset = <<1, -3, 0>>		BREAK
		CASE 25			vOffset = <<2, -3, 0>>		BREAK
		CASE 26			vOffset = <<3, -3, 0>>		BREAK
		CASE 27			vOffset = <<4, -3, 0>>		BREAK
		CASE 28			vOffset = <<5, -3, 0>>		BREAK
		CASE 29			vOffset = <<6, -3, 0>>		BREAK
		CASE 30			vOffset = <<7, -3, 0>>		BREAK
		CASE 31			vOffset = <<0, -4, 0>>		BREAK
		CASE 32			vOffset = <<1, -4, 0>>		BREAK
		CASE 33			vOffset = <<2, -4, 0>>		BREAK
		CASE 34			vOffset = <<3, -4, 0>>		BREAK
		CASE 35			vOffset = <<4, -4, 0>>		BREAK
		CASE 36			vOffset = <<5, -4, 0>>		BREAK
		CASE 37			vOffset = <<6, -4, 0>>		BREAK
		CASE 38			vOffset = <<7, -4, 0>>		BREAK
		CASE 39			vOffset = <<0, -5, 0>>		BREAK
		CASE 40			vOffset = <<1, -5, 0>>		BREAK
		CASE 41			vOffset = <<2, -5, 0>>		BREAK
		CASE 42			vOffset = <<3, -5, 0>>		BREAK
		CASE 43			vOffset = <<4, -5, 0>>		BREAK
		CASE 44			vOffset = <<5, -5, 0>>		BREAK
		CASE 45			vOffset = <<6, -5, 0>>		BREAK
		CASE 46			vOffset = <<7, -5, 0>>		BREAK
		CASE 47			vOffset = <<0, -6, 0>>		BREAK
		CASE 48			vOffset = <<1, -6, 0>>		BREAK
		CASE 49			vOffset = <<2, -6, 0>>		BREAK
		CASE 50			vOffset = <<3, -6, 0>>		BREAK
		CASE 51			vOffset = <<4, -6, 0>>		BREAK
		CASE 52			vOffset = <<5, -6, 0>>		BREAK
		CASE 53			vOffset = <<6, -6, 0>>		BREAK
		CASE 54			vOffset = <<7, -6, 0>>		BREAK
		CASE 55			vOffset = <<0, -7, 0>>		BREAK
		CASE 56			vOffset = <<1, -7, 0>>		BREAK
		CASE 57			vOffset = <<2, -7, 0>>		BREAK
		CASE 58			vOffset = <<3, -7, 0>>		BREAK
		CASE 59			vOffset = <<4, -7, 0>>		BREAK
		CASE 60			vOffset = <<5, -7, 0>>		BREAK
		CASE 61			vOffset = <<6, -7, 0>>		BREAK
		CASE 62			vOffset = <<7, -7, 0>>		BREAK
		CASE 63			vOffset = <<0, -8, 0>>		BREAK
		CASE 64			vOffset = <<1, -8, 0>>		BREAK
		CASE 65			vOffset = <<2, -8, 0>>		BREAK
		CASE 66			vOffset = <<3, -8, 0>>		BREAK
		CASE 67			vOffset = <<4, -8, 0>>		BREAK
		CASE 68			vOffset = <<5, -8, 0>>		BREAK
		CASE 69			vOffset = <<6, -8, 0>>		BREAK
		CASE 70			vOffset = <<7, -8, 0>>		BREAK
		CASE 71			vOffset = <<0, -9, 0>>		BREAK
		CASE 72			vOffset = <<1, -9, 0>>		BREAK
		CASE 73			vOffset = <<2, -9, 0>>		BREAK
		CASE 74			vOffset = <<3, -9, 0>>		BREAK
		CASE 75			vOffset = <<4, -9, 0>>		BREAK
		CASE 76			vOffset = <<5, -9, 0>>		BREAK
		CASE 77			vOffset = <<6, -9, 0>>		BREAK
		CASE 78			vOffset = <<7, -9, 0>>		BREAK
	ENDSWITCH

	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), vOffset)
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_RANDOM_OUTFIT()
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 14)

	SWITCH iRand
		CASE 0 RETURN OUTFIT_SMUGGLER_RACE_SUIT_FLOW
		CASE 1 RETURN OUTFIT_SMUGGLER_RACE_SUIT_CNT
		CASE 2 RETURN OUTFIT_SMUGGLER_RACE_SUIT_METV
		CASE 3 RETURN OUTFIT_SMUGGLER_RACE_SUIT_ECOLA
		CASE 4 RETURN OUTFIT_SMUGGLER_RACE_SUIT_SPRUNK
		CASE 5 RETURN OUTFIT_SMUGGLER_RACE_SUIT_TINKLE
		CASE 6 RETURN OUTFIT_STUNT_RACE_BIKER_0
		CASE 7 RETURN OUTFIT_STUNT_RACE_BIKER_1
		CASE 8 RETURN OUTFIT_STUNT_RACE_STUNT_0
		CASE 9 RETURN OUTFIT_STUNT_RACE_STUNT_1
		CASE 10 RETURN OUTFIT_STUNT_RACE_RACER_0
		CASE 11 RETURN OUTFIT_STUNT_RACE_RACER_1
		CASE 12 RETURN OUTFIT_STUNT_RACE_MOTO_0
		CASE 13 RETURN OUTFIT_STUNT_RACE_MOTO_1
	ENDSWITCH
	
	RETURN OUTFIT_MP_FREEMODE
ENDFUNC

PROC SET_NEW_OUTFIT(PED_INDEX pedID)
	MP_OUTFITS_APPLY_DATA 	sApplyData
	sApplyData.pedID 		= pedID
	sApplyData.eOutfit 		= GET_RANDOM_OUTFIT()
	sApplyData.eApplyStage 	= AOS_SET
	SET_PED_MP_OUTFIT(sApplyData)
ENDPROC

PROC DO_PROCESSING()

	IF bKillScript
		eStage = STAGE_CLEANUP
	ENDIF
	
	IF bWarpToLSIA
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1484.1968, -2863.6448, 12.9500>>, FALSE)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 146.4111)
			bWarpToLSIA = FALSE
		ENDIF
	ENDIF
	
	INT iLoop
	
	IF bCreateAllClones
		IF NOT DOES_ENTITY_EXIST(piClone[iCloneStagger])
			piClone[iCloneStagger] = CREATE_PED(PEDTYPE_MISSION, GET_PLAYER_MODEL(), GET_CLONE_CREATION_OFFSET(iCloneStagger), GET_ENTITY_HEADING(PLAYER_PED_ID()), FALSE, FALSE)
			CLONE_PED_TO_TARGET(PLAYER_PED_ID(), piClone[iCloneStagger])
		ELSE
			iCloneStagger++
		ENDIF
		
		IF iCloneStagger > iNumPedsToCreate
			iCloneStagger = 0
			bCreateAllClones = FALSE
		ENDIF
	ENDIF
	
	IF bDeleteAllClones
		REPEAT iNumPedsToCreate iLoop
			IF DOES_ENTITY_EXIST(piClone[iLoop])
				DELETE_PED(piClone[iLoop])
			ENDIF
		ENDREPEAT
	
		bDeleteAllClones = FALSE
	ENDIF

	IF bAutomateOutfitChangingOutfit
		IF HAS_NET_TIMER_EXPIRED(AutomationTimer, iInterval, TRUE)
			IF iCloneStagger = 0
				SET_NEW_OUTFIT(PLAYER_PED_ID())
			ENDIF
			
			IF DOES_ENTITY_EXIST(piClone[iCloneStagger])
				SET_NEW_OUTFIT(piClone[iCloneStagger])
			ENDIF
			
			iCloneStagger++
			IF iCloneStagger > iNumPedsToCreate
				iCloneStagger = 0
				RESET_NET_TIMER(AutomationTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF bAutomateOutfitChangingClone
		IF HAS_NET_TIMER_EXPIRED(AutomationTimer, iInterval, TRUE)
			IF iCloneStagger = 0
				SET_NEW_OUTFIT(PLAYER_PED_ID())
			ENDIF
			
			IF DOES_ENTITY_EXIST(piClone[iCloneStagger])
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), piClone[iCloneStagger])
			ENDIF
			
			iCloneStagger++
			IF iCloneStagger > iNumPedsToCreate
				iCloneStagger = 0
				RESET_NET_TIMER(AutomationTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF bAutomateOutfitChangingCloneOneByOne
		IF HAS_NET_TIMER_EXPIRED(AutomationTimer, iInterval, TRUE)
			IF iCloneStagger = 0
				SET_NEW_OUTFIT(PLAYER_PED_ID())
			ENDIF
			
			IF DOES_ENTITY_EXIST(piClone[iCloneStagger])
				CLONE_PED_TO_TARGET(PLAYER_PED_ID(), piClone[iCloneStagger])
			ENDIF
			
			iCloneStagger++
			IF iCloneStagger > iNumPedsToCreate
				iCloneStagger = 0
			ENDIF
			RESET_NET_TIMER(AutomationTimer)
		ENDIF
	ENDIF
		
	IF !bAutomateOutfitChangingClone
	AND !bAutomateOutfitChangingClone
	AND !bAutomateOutfitChangingCloneOneByOne
		IF HAS_NET_TIMER_STARTED(AutomationTimer)
			RESET_NET_TIMER(AutomationTimer)
		ENDIF
	ENDIF

ENDPROC

PROC DO_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	WHILE TRUE
	
		WAIT(0)
		
		SWITCH eStage
			CASE STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE STAGE_PROCESSING
				DO_PROCESSING()
			BREAK
			
			CASE STAGE_CLEANUP
				DO_CLEANUP()
			BREAK
		ENDSWITCH
	ENDWHILE
	
ENDSCRIPT


#ENDIF






