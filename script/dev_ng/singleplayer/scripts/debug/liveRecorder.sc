USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

	USING "liveRecorder.sch"
	
	SCRIPT
		IF HAS_FORCE_CLEANUP_OCCURRED(ENUM_TO_INT(FORCE_CLEANUP_FLAG_SP_TO_MP))
			STOP_RECORDING_ALL_VEHICLES()
			TERMINATE_THIS_THREAD()
		ENDIF
		WHILE (TRUE	)
			liveRecorder()
			WAIT(0)
		ENDWHILE
	ENDSCRIPT

#ENDIF

