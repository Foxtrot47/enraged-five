using "rage_builtins.sch"
using "globals.sch"

// Do nothing in release mode

#IF IS_FINAL_BUILD

	SCRIPT

	ENDSCRIPT

#ENDIF



#IF IS_DEBUG_BUILD

using "rerecord_recording.sch"

SCRIPT

	WHILE (TRUE)

		DO_RE_RECORDING_WIDGET()		

		WAIT (0)		
	ENDWHILE
ENDSCRIPT

#endif 

