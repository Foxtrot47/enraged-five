
USING "rage_builtins.sch"
USING "globals.sch"



SCRIPT
	TERMINATE_THIS_THREAD()
ENDSCRIPT


/*

USING "rage_builtins.sch"
USING "globals.sch"
USING "hud_drawing.sch"
USING "UIUtil.sch"


// Variables
#IF IS_DEBUG_BUILD
	INT iSelectedIndex = 0
	INT iMaxIndex = 3
	INT iSelections[3]
	//BOOL bButtonCooldown = FALSE
#ENDIF


// Main script
SCRIPT 


	IF HAS_FORCE_CLEANUP_OCCURRED()	
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: Unpausing flow - due to cleanup")
		g_flowUnsaved.bUpdatingGameflow = TRUE
		TERMINATE_THIS_THREAD()
	ENDIF

#IF IS_DEBUG_BUILD
	
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("debug_app_select_screen")) > 1
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: Attempting to launch with an instance already active.")
		TERMINATE_THIS_THREAD()
	ENDIF

	CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: Making sure flow paused")
	g_flowUnsaved.bUpdatingGameflow = FALSE
	
	BOOL bDone = FALSE
	INT  red = 255
	INT  green = 0
	
	SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),FALSE)
	SET_GAME_PAUSED(TRUE)
	
	WHILE NOT bDone
	
		DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)

		// DEBUG: Social Club Integration/Apps SelectionScreen
		SET_TEXT_SCALE(0.9, 0.9)
		DISPLAY_TEXT(0.15, 0.1, "DBGAPPHDR")
		
		// (to simulate which apps the player has downloaded and integrated into V via the social club)
		SET_TEXT_SCALE(0.6, 0.6)
		DISPLAY_TEXT(0.10,0.2,"DBGAPPHDRSB")
		
		// ~PAD_DPAD_UP~ ~PAD_DPAD_DOWN~ ~r~to pick a setting.  ~PAD_DPAD_LEFT~ ~PAD_DPAD_RIGHT~ ~r~To change settings.
		SET_TEXT_SCALE(0.6, 0.6)
		DISPLAY_TEXT(0.10, 0.3, "DBGAPPCONT")

		// CHOP THE DOG app
		SET_TEXT_SCALE(0.5,0.5)
		DISPLAY_TEXT(0.35, 0.4, "DBGCHPAPP")

		// Played Chop App - Yes/No
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT(0.6, 0.4, "DBGYSNW")

		red = 255
		green = 0
		
		IF iSelectedIndex = 0
			red = 0
			green = 255
		ENDIF

		IF iSelections[0] = 1
			DRAW_RECT(0.618, 0.4156, 0.035, 0.04, red, green, 0, 255)
		ELSE
			DRAW_RECT(0.672, 0.4156, 0.035, 0.04, red, green, 0, 255)
		ENDIF

		// Chop Behaviour - Good/Bad
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT(0.6, 0.45, "DBGGBSNW")

		red = 255
		green = 0
		
		IF iSelectedIndex = 1
			red = 0
			green = 255
		ENDIF
		
		IF iSelections[1] = 1
			DRAW_RECT(0.625, 0.4656, 0.047, 0.04, red, green, 0, 255)
		ELSE
			DRAW_RECT(0.691, 0.4656, 0.035, 0.04, red, green, 0, 255)		
		ENDIF
		
		// LOS SANTOS CUSTOMS app
		SET_TEXT_SCALE(0.5,0.5)
		DISPLAY_TEXT(0.35,0.5,"DBGLSCUSAPP")
				
		// Played Car App - Yes/No
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT(0.6, 0.5, "DBGYSNW")
		
		red   = 255
		green = 0
		
		IF iSelectedIndex = 2
			red = 0
			green = 255
		ENDIF
		
		IF iSelections[2] = 1
			DRAW_RECT(0.618, 0.5156, 0.035, 0.04, red, green, 0, 255)
		ELSE
			DRAW_RECT(0.672, 0.5156, 0.035, 0.04, red, green, 0, 255)
		ENDIF
		
		// PRESS ~g~START~w~ TO CONTINUE
		SET_TEXT_SCALE(0.6,0.6)
		DISPLAY_TEXT(0.10,0.8,"DBGSTRTCNT")
		
		// ~r~WARNING: THESE CHANGES WILL ONLY TAKE EFFECT AT MISSION STARTS
		SET_TEXT_SCALE(0.5,0.5)
		DISPLAY_TEXT(0.10,0.85,"DBGSTRTWRNG")
		
		WAIT(0)

		// User has pressed start
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			bDone = TRUE
			SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), FALSE)
		ENDIF
		
		// Handle menu cycling (up/down)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			iSelectedIndex--
		ENDIF
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			iSelectedIndex++
		ENDIF
		
		// Cap values
		IF NOT (iSelectedIndex < iMaxIndex)
			iSelectedIndex = iMaxIndex -1
		ENDIF
		
		IF NOT (iSelectedIndex > -1)
			iSelectedIndex = 0
		ENDIF
		
		// Handle option cycling (left/right)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			iSelections[iSelectedIndex] = 1
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			iSelections[iSelectedIndex] = 0
		ENDIF
	ENDWHILE
	
	// APPLY SETTINGS
	CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugAppScreenOverride = TRUE")
	g_bDebugAppScreenOverride = TRUE

	// CHOP THE DOG
	// Played app
	IF iSelections[0] = 1
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugChopPlayedStatusOverride = TRUE")
		g_bDebugChopPlayedStatusOverride = TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugChopPlayedStatusOverride = FALSE")
		g_bDebugChopPlayedStatusOverride = FALSE
	ENDIF
	
	// Good/Bad behaviour
	IF iSelections[1] = 1
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugChopGoodStatusOverride = TRUE")
		g_bDebugChopGoodStatusOverride = TRUE
		g_bDebugChopPlayedStatusOverride = TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugChopGoodStatusOverride = FALSE")
		g_bDebugChopGoodStatusOverride = FALSE
	ENDIF
	
	// LOS SANTOS CUSTOMS
	IF iSelections[2] = 1
	 	CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugLosSantosCustomsPlayedStatusOverride = TRUE")
		g_bDebugLosSantosCustomsPlayedStatusOverride = TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: g_bDebugLosSantosCustomsPlayedStatusOverride = FALSE")
		g_bDebugLosSantosCustomsPlayedStatusOverride = FALSE
	ENDIF
	
	// Unpause game
	SET_GAME_PAUSED(FALSE)
	
	// Unpause the flow
	CPRINTLN(DEBUG_MISSION, "DEBUG APP SELECT: Unpausing flow")
	g_flowUnsaved.bUpdatingGameflow = TRUE
	
#ENDIF
	
	// Restore control
	SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
	SET_PAUSE_MENU_ACTIVE(FALSE)
 	TERMINATE_THIS_THREAD()
	ENDSCRIPT
*/








