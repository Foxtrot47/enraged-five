USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
SCRIPT 
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_clock.sch"
USING "selector_public.sch"

CONST_INT numlocation 7
INT index, settle_time, average_repeat
VECTOR FPSlocation[numlocation], vehicles_coords
STRING FPSlocationName[numlocation]
VECTOR v_car_destination[numlocation]
FLOAT car_heading[numlocation]
FLOAT new_heading
FLOAT camera_pan_mod
FLOAT camera_pitch_mod
VEHICLE_INDEX veh_car
MODEL_NAMES model_car = NINEF2
 

ENUM FPS_TEST_STAGE
	CHOOSE_SETUP,
	SETUP,
	WARP_TO_LOCATION,
	WAIT_TO_SETTLE,
	CAMERA_PITCH,
	CAMERA_ROTATE,
	CREATE_CAR,
	CAR_DRIVE,
	CLEANUP_ZONE,
	FPS_RESULTS
ENDENUM
FPS_TEST_STAGE FPSlocationStage = CHOOSE_SETUP

PROC CLEANUP_SCRIPT()
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	METRICS_ZONE_STOP()
//	METRICS_ZONE_SAVE()
	PRINTLN("FPS TEST METRICS_ZONE_SAVE")
	METRICS_ZONES_CLEAR()
	METRICS_ZONES_HIDE()
	SET_STREAMING(TRUE)
	PRINTLN("FPS TEST SCRIPT TERMINATING")
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("maponly")	
		PRINTLN("FPS TEST MAGDEMO MAPONLY SCRIPT STARTING")
	ELSE
		PRINTLN("FPS TEST MAGDEMO SCRIPT STARTING")
	ENDIF
	
	index = 0
	average_repeat = 0
	
	WHILE TRUE
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH FPSlocationStage
				
					CASE CHOOSE_SETUP
						
						camera_pitch_mod = 1.0
						camera_pan_mod = 0.6
						settle_time = 3000
						FPSlocationStage = SETUP
						
//						PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Press Numpad 1 for location test. Press Numpad 2 for magdemo locations ", 1000, 1)
						
					BREAK
					
					CASE SETUP
						
						FPSlocation[0] = <<-782.2075, 185.3187, 71.8352>>		// Mansion Pool at sunlounger
						FPSlocation[1] = <<-864.6050, 162.3987, 64.9456>>		// Drive from mansion
						v_car_destination[1] = <<-831.1718, -86.7136, 36.8818>>
						car_heading[1] = 180
						FPSlocation[2] = <<2006.6549, 3825.3169, 31.2671>>	// Trailer Park Drive								
						v_car_destination[2] = <<1022.9534, 3628.9602, 31.7661>>
						car_heading[2] = 28.9
						FPSlocation[3] = <<-1182.1354, -1572.9576, 3.3047>>	// Franklin at mussel beach
						FPSlocation[4] = <<-1187.4091, -1530.4419, 3.3809>>	// Franklin driving from mussel beach
						v_car_destination[4] = <<-67.4970, -572.2403, 36.5082>>
						car_heading[4] = 301
						FPSlocation[5] = <<1367.1207, -2075.1047, 350.9982>>	// FBI2 Heli fly
						v_car_destination[5] = <<153.6122, -703.1117, 350.0777>>
						car_heading[5] = 344
						FPSlocation[6] = <<109.6936, -616.7566, 206.0467>>	// FBI2 Exterior of scyscraper
						
						IF GET_COMMANDLINE_PARAM_EXISTS("maponly")					
							FPSlocationName[0] = "mc_MansionPool_maponly"
							FPSlocationName[1] = "mc_MansionDrive_maponly"
							FPSlocationName[2] = "mc_TrailerP_maponly"
							FPSlocationName[3] = "mc_MBeach_maponly"
							FPSlocationName[4] = "mc_MBeachDrive_maponly"
							FPSlocationName[5] = "mc_FBI2HeliA_maponly"
							FPSlocationName[6] = "mc_FBI2Office_maponly"
						ELSE
							FPSlocationName[0] = "mc_MansionPool"
							FPSlocationName[1] = "mc_MansionDrive"
							FPSlocationName[2] = "mc_TrailerP"
							FPSlocationName[3] = "mc_MBeach"
							FPSlocationName[4] = "mc_MBeachDrive"
							FPSlocationName[5] = "mc_FBI2HeliA"
							FPSlocationName[6] = "mc_FBI2Office"
						ENDIF

						METRICS_ZONES_CLEAR()
						PRINTLN("FPS TEST METRICS_ZONES_CLEAR")
//						METRICS_ZONES_SHOW()
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						FPSlocationStage = WARP_TO_LOCATION

						// Fade the screen in
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						
					BREAK
					
					CASE WARP_TO_LOCATION
					
						SET_MAX_WANTED_LEVEL(0)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						
						SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), FPSlocation[index])
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						new_heading = -90
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(new_heading)						
						CLEAR_AREA(FPSlocation[index], 2000, TRUE)
						LOAD_SCENE(FPSlocation[index])
						LOAD_ALL_OBJECTS_NOW()
						INSTANTLY_FILL_PED_POPULATION()
						INSTANTLY_FILL_VEHICLE_POPULATION()
						SET_CLOCK_TIME(17, 0, 0)
						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
						SETTIMERA(0)
						FPSlocationStage = WAIT_TO_SETTLE
						
//						average_repeat = 3
					BREAK
				
					CASE WAIT_TO_SETTLE
			
						IF TIMERA() > settle_time
							//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
//							METRICS_ZONES_HIDE()
							SETTIMERA(0)
							FPSlocationStage = CAMERA_PITCH
							IF index = 1 OR index = 2 OR index = 4 OR index = 5 OR index = 8
								IF average_repeat = 0
									METRICS_ZONE_START(FPSlocationName[index])
									PRINTLN("FPS TEST METRICS_ZONE_START - STREAMING OFF")
								ENDIF
								SET_STREAMING(TRUE)
								FPSlocationStage = CREATE_CAR
							ENDIF
						
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotateF
							SETTIMERA(5000)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
						IF index = 6
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF
						
					BREAK

					CASE CAMERA_PITCH
						
						IF TIMERA() > 30
							SETTIMERA(0)
							new_heading = new_heading + camera_pitch_mod
							IF new_heading < 0
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(new_heading)
							ELSE
								new_heading = 0
								FPSlocationStage = CAMERA_ROTATE
							ENDIF
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(5000)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
						IF index = 6
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF
						
					BREAK
					
					CASE CAMERA_ROTATE
								
						IF TIMERA() > 30
							SETTIMERA(0)
							new_heading = new_heading + camera_pan_mod
							IF new_heading < 360
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(new_heading)
							ELSE
								new_heading = 0
								IF average_repeat < 4
									
									average_repeat ++
									
									IF average_repeat = 1
										METRICS_ZONE_START(FPSlocationName[index])
										PRINTLN("FPS TEST METRICS_ZONE_START - STREAMING OFF")
										average_repeat ++
									ENDIF
									
									
//									METRICS_ZONE_STOP()
//									METRICS_ZONES_SHOW()
									SETTIMERA(settle_time)
									FPSlocationStage = WAIT_TO_SETTLE
								ELSE
									FPSlocationStage = CLEANUP_ZONE
								ENDIF
								
							ENDIF
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(5000)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
						IF index = 6
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF
						
					BREAK

					CASE CREATE_CAR
					
						PRINTSTRING("...Create car")
						PRINTNL()
						
						IF NOT DOES_ENTITY_EXIST(veh_car)
							IF index = 5
								model_car = FROGGER
								REQUEST_MODEL(model_car)
							ELSE
								model_car = NINEF2
								REQUEST_MODEL(model_car)
							ENDIF
							
							IF HAS_MODEL_LOADED(model_car)
								CLEAR_AREA(FPSlocation[index], 20, TRUE)
								veh_car = CREATE_VEHICLE(model_car, FPSlocation[index], car_heading[index])
							ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(veh_car)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
								SET_ENTITY_COORDS(veh_car, FPSlocation[index])
								SET_ENTITY_HEADING(veh_car, car_heading[index])
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								SET_ENTITY_INVINCIBLE(veh_car, TRUE)
								IF index = 5
									TASK_HELI_MISSION(PLAYER_PED_ID(), veh_car, NULL, NULL, v_car_destination[index], MISSION_GOTO, 30, 50, 0, 340, 100)
								ELSE
									TASK_VEHICLE_DRIVE_TO_COORD(PLAYER_PED_ID(),veh_car, v_car_destination[index],20.0,DRIVINGSTYLE_ACCURATE,model_car,DRIVINGMODE_PLOUGHTHROUGH,7.0,1.0)
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
						ENDIF
							FPSlocationStage = CAR_DRIVE
						ENDIF
						
					BREAK
			
					CASE CAR_DRIVE
					
						IF index = 5
							IF IS_VEHICLE_DRIVEABLE(veh_car) AND IS_ENTITY_AT_COORD(veh_car, v_car_destination[index], <<60, 60, 200>> )
								SETTIMERA(0)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
//								IF average_repeat < 2
//									average_repeat ++
//									METRICS_ZONE_STOP()
//									METRICS_ZONES_SHOW()
//									FPSlocationStage = WAIT_TO_SETTLE
//								ELSE
									FPSlocationStage = CLEANUP_ZONE
//								ENDIF
							ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(veh_car) AND FINISHED_TASK = GET_SCRIPT_TASK_STATUS( PLAYER_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD )
								SETTIMERA(0)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
//								IF average_repeat < 2
//									average_repeat ++
//									METRICS_ZONE_STOP()
//									METRICS_ZONES_SHOW()
//									FPSlocationStage = WAIT_TO_SETTLE
//								ELSE
									FPSlocationStage = CLEANUP_ZONE
//								ENDIF
							ENDIF
						ENDIF
						
						IF TIMERA() > 250
							vehicles_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							CLEAR_AREA(vehicles_coords, 12, TRUE)
							SETTIMERA(0)
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(5000)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
		
					BREAK
					
					CASE CLEANUP_ZONE

						IF TIMERA() > settle_time
							METRICS_ZONE_STOP()
							PRINTLN("FPS TEST METRICS_ZONE_STOP")
							WAIT(100)
							METRICS_ZONES_SHOW()
							METRICS_ZONE_SAVE_TO_FILE(FPSlocationName[index])
							METRICS_ZONE_SAVE_TELEMETRY()
							WAIT(2000)
							METRICS_ZONES_CLEAR()
							WAIT(0)							
							METRICS_ZONES_HIDE()

//							IF average_repeat = 0
//								METRICS_ZONES_HIDE()
								SET_DISPLAY_STREAM_GRAPH(TRUE)
								WAIT(2000)
								SAVE_SCREENSHOT(FPSlocationName[index])
								WAIT(500)
								SET_DISPLAY_STREAM_GRAPH(FALSE)
								WAIT(0)
//							ENDIF
							
//							METRICS_ZONES_SHOW()
							new_heading = 0
							average_repeat = 0
							index ++
							IF index < numlocation 
								IF DOES_ENTITY_EXIST(veh_car)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
								ENDIF
								FPSlocationStage = WARP_TO_LOCATION
							ELSE
								SETTIMERA(0)
								FPSlocationStage = FPS_RESULTS
							ENDIF
							
						ENDIF
					
					BREAK
					
					CASE FPS_RESULTS
						
						//Gives time to view results before cleaning up
						IF TIMERA() > 2000
							CLEANUP_SCRIPT()
						ENDIF
						
					BREAK
					
				ENDSWITCH

			ENDIF
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			CLEANUP_SCRIPT()
		ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF

