USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
SCRIPT 
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "commands_clock.sch"
USING "selector_public.sch"
USING "select_mission_stage.sch"
USING "commands_interiors.sch"
USING "flow_reset_GAME.sch"

CONST_INT numlocation 15
CONST_INT camera_rotate_speed 20
INT index, settle_time, average_repeat, ii
VECTOR FPSlocation[numlocation], vehicles_coords, vMin, vMax
STRING FPSlocationName[numlocation]
//VECTOR v_car_destination[numlocation]
FLOAT car_heading[numlocation]
FLOAT new_heading
FLOAT camera_pan_mod
FLOAT camera_pitch_mod
VEHICLE_INDEX veh_car
MODEL_NAMES model_car = ZENTORNO
	
CONST_INT TEST_Docks_Terminal				0
CONST_INT TEST_Pershing_Square				1
CONST_INT TEST_Vinewood_Hills				2
CONST_INT TEST_Sandy_Shore					3
CONST_INT TEST_Paleto_Bay					4
CONST_INT TEST_Downtown_Day					5
CONST_INT TEST_Downtown_Night				6
CONST_INT TEST_Bean_Machine					7
CONST_INT TEST_Pine_Forest					8
CONST_INT TEST_Interior_Life_Invaders		9
CONST_INT TEST_Streaming_Vinewood			10
CONST_INT TEST_Streaming_freeway			11
CONST_INT TEST_Physics_gunfight				12
CONST_INT TEST_Physics_gunfight_5Star		13
CONST_INT TEST_Physics_Vehicles				14
CONST_INT TEST_MAX							15
MissionStageMenuTextStruct sSkipMenu[TEST_MAX]
INT iSkipToThisStage = -1

//AI test vars
CONST_INT numgangpeds 5
VECTOR GangStartPos[numgangpeds], CopStartPos[numgangpeds]
FLOAT GangStartHead[numgangpeds], CopStartHead[numgangpeds]
PED_INDEX pedCop[numgangpeds], pedCriminal[numgangpeds]
REL_GROUP_HASH relGroupCop, relGroupCrim
INT i
BOOL FIveStarAlwayOn, explosion_triggered
CONST_INT num_vehs 5
FLOAT offsetVehicleDist
VEHICLE_INDEX veh[num_vehs][num_vehs]
FLOAT fMiddle = TO_FLOAT(num_vehs)/2

ENUM FPS_TEST_STAGE
	CHOOSE_SETUP,
	SETUP,
	WARP_TO_LOCATION,
	WAIT_TO_SETTLE,
	CAMERA_PITCH,
	CAMERA_ROTATE,
	CAR_DRIVE,
	CAR_SMASH,
	CLEANUP_ZONE,
	FPS_RESULTS
ENDENUM
FPS_TEST_STAGE FPSlocationStage = CHOOSE_SETUP

PROC CLEANUP_SCRIPT()
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
//	METRICS_ZONE_STOP()
//	METRICS_ZONE_SAVE()
//	PRINTLN("FPS TEST METRICS_ZONE_SAVE")
//	METRICS_ZONES_CLEAR()
//	METRICS_ZONES_HIDE()
	PRINTLN("FPS TEST SCRIPT TERMINATING")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SETUP_GUNFIGHT()

	REQUEST_MODEL(S_M_Y_COP_01)
	REQUEST_MODEL(A_M_Y_MEXTHUG_01)
	LOAD_ALL_OBJECTS_NOW()

	CopStartPos[0] = << -579.2419, -1656.8274, 18.7912 >>
	CopStartHead[0] = 118
	CopStartPos[1] = << -572.1108, -1662.7108, 18.2502 >>
	CopStartHead[1] = 94
	CopStartPos[2] = << -581.9711, -1671.4646, 18.2560 >>	
	CopStartHead[2] = 94
	CopStartPos[3] = << -566.4862, -1671.4352, 18.2296 >>	
	CopStartHead[3] = 87
	CopStartPos[4] = << -571.9099, -1675.8474, 18.7016 >>
	CopStartHead[4] = 48
	
	REPEAT numgangpeds i
		pedCop[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01, CopStartPos[i])
		SET_PED_HEADING_AND_PITCH(pedCop[i], CopStartHead[i], 0)
		SET_PED_COMBAT_MOVEMENT(pedCop[i], CM_DEFENSIVE )
		SET_PED_FLEE_ATTRIBUTES(pedCop[i], FA_USE_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedCop[i], CA_CAN_CAPTURE_ENEMY_PEDS, FALSE)
		SET_ENTITY_PROOFS(pedCop[i], TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCop[i], relGroupCop)
		GIVE_WEAPON_TO_PED(pedCop[i], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		SET_CURRENT_PED_WEAPON(pedCop[i], WEAPONTYPE_PISTOL, TRUE)
	ENDREPEAT

	GangStartPos[0] = << -601.7824, -1674.6423, 18.5608 >> 	
	GangStartHead[0] = 285
	GangStartPos[1] = << -600.7036, -1669.3536, 18.5807 >>	
	GangStartHead[1] = 285
	GangStartPos[2] = << -606.2323, -1669.1875, 18.8904 >>	
	GangStartHead[2] = 274
	GangStartPos[3] = << -605.7510, -1678.3025, 18.6469 >>	
	GangStartHead[3] = 289
	GangStartPos[4] = << -596.6554, -1674.1642, 18.4122 >>
	GangStartHead[4] = 286
	
	REPEAT numgangpeds i
		pedCriminal[i] = CREATE_PED(PEDTYPE_MISSION, A_M_Y_MEXTHUG_01, GangStartPos[i])
		SET_PED_HEADING_AND_PITCH(pedCriminal[i], GangStartHead[i], 0)
		SET_PED_COMBAT_MOVEMENT(pedCriminal[i], CM_DEFENSIVE )
		SET_PED_FLEE_ATTRIBUTES(pedCriminal[i], FA_USE_COVER, TRUE)
		SET_ENTITY_PROOFS(pedCriminal[i], TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedCriminal[i], relGroupCrim)
		GIVE_WEAPON_TO_PED(pedCriminal[i], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		SET_CURRENT_PED_WEAPON(pedCriminal[i], WEAPONTYPE_PISTOL, TRUE)
	ENDREPEAT
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCrim, relGroupCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCop, relGroupCrim)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroupCrim, RELGROUPHASH_PLAYER)

	REPEAT numgangpeds i
		REGISTER_HATED_TARGETS_IN_AREA(pedCop[i], CopStartPos[i], 200)
		TASK_TURN_PED_TO_FACE_ENTITY(pedCop[i], pedCriminal[i])
	ENDREPEAT
	REPEAT numgangpeds i
		REGISTER_HATED_TARGETS_IN_AREA(pedCriminal[i], GangStartPos[i], 200)
		TASK_TURN_PED_TO_FACE_ENTITY(pedCriminal[i], pedCop[i])
	ENDREPEAT

ENDPROC

PROC CREATE_CAR_FOR_REC()
				
	PRINTSTRING("...Create car")
	PRINTNL()
	
	IF NOT DOES_ENTITY_EXIST(veh_car)
		REQUEST_MODEL(model_car)
		
		WHILE NOT HAS_MODEL_LOADED(model_car)
			WAIT(0)
			PRINTSTRING("...Car loading")
		ENDWHILE
		CLEAR_AREA(FPSlocation[index], 20, TRUE)
		veh_car = CREATE_VEHICLE(model_car, FPSlocation[index], car_heading[index])
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(veh_car)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
			SET_ENTITY_COORDS(veh_car, FPSlocation[index])
			SET_ENTITY_HEADING(veh_car, car_heading[index])
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_ENTITY_INVINCIBLE(veh_car, TRUE)
				
			IF index = TEST_Streaming_Vinewood
				REQUEST_VEHICLE_RECORDING(1, "zentorno_streamingV")
				WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "zentorno_streamingV")
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(veh_car)
					START_PLAYBACK_RECORDED_VEHICLE(veh_car, 1, "zentorno_streamingV")
				ENDIF
			ELIF index = TEST_Streaming_freeway
				REQUEST_VEHICLE_RECORDING(1, "zentorno_streamingF")
				WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "zentorno_streamingF")
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(veh_car)
					START_PLAYBACK_RECORDED_VEHICLE(veh_car, 1, "zentorno_streamingF")
					SET_PLAYBACK_SPEED(veh_car, 1.08)
				ENDIF								
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
		ENDIF
	ENDIF
					
ENDPROC

PROC SETUP_VEHICLE_TEST()

	offsetVehicleDist = 25
	
	REQUEST_MODEL(A_M_Y_MEXTHUG_01)
	REQUEST_MODEL(model_car)
	WHILE NOT HAS_MODEL_LOADED(A_M_Y_MEXTHUG_01)
	OR NOT HAS_MODEL_LOADED(model_car)
		WAIT(0)
	ENDWHILE
	
	GET_MODEL_DIMENSIONS(model_car, vMin, vMax)
	REPEAT num_vehs i
		REPEAT num_vehs ii
			veh[i][ii] = CREATE_VEHICLE(model_car, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(FPSlocation[TEST_Physics_Vehicles], 0.0, <<(3*vMax.x)*i, (3*vMax.y)*ii, 0>>), 0.0)
			SET_VEHICLE_FORWARD_SPEED(veh[i][ii], 20)
		ENDREPEAT
	ENDREPEAT
	
	veh_car = CREATE_VEHICLE(model_car, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(FPSlocation[TEST_Physics_Vehicles], 0.0, <<(2*vMax.x)*(fMiddle), -offsetVehicleDist, 0>>), 0.0)
	SET_VEHICLE_FORWARD_SPEED(veh_car, 20)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MEXTHUG_01)
								
ENDPROC


PROC CLEANUP_ASSETS()

	IF DOES_ENTITY_EXIST(veh_car)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
	ENDIF
	REPEAT num_vehs i
		REPEAT num_vehs ii
			IF DOES_ENTITY_EXIST(veh[i][ii])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(veh[i][ii])
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

SCRIPT
		
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS)
	ENDIF

	RUN_GENERAL_SCRIPT_CLEANUP()
	RESET_GAMEFLOW()
	PRINTLN("FLOW SHUTDOWN")
	
	sSkipMenu[TEST_Docks_Terminal		 ].sTxtLabel = "Docks Terminal"
	sSkipMenu[TEST_Pershing_Square		 ].sTxtLabel = "Pershing Square"
	sSkipMenu[TEST_Vinewood_Hills		 ].sTxtLabel = "Vinewood Hills"
	sSkipMenu[TEST_Sandy_Shore			 ].sTxtLabel = "Sandy Shore"
	sSkipMenu[TEST_Paleto_Bay			 ].sTxtLabel = "Paleto Bay"
	sSkipMenu[TEST_Downtown_Day			 ].sTxtLabel = "Downtown Day"
	sSkipMenu[TEST_Downtown_Night		 ].sTxtLabel = "Downtown Night"
	sSkipMenu[TEST_Bean_Machine			 ].sTxtLabel = "Bean Machine"
	sSkipMenu[TEST_Pine_Forest			 ].sTxtLabel = "Pine Forest"
	sSkipMenu[TEST_Interior_Life_Invaders].sTxtLabel = "Interior - Life Invaders"
	sSkipMenu[TEST_Streaming_Vinewood	 ].sTxtLabel = "Streaming - Vinewood"
	sSkipMenu[TEST_Streaming_freeway	 ].sTxtLabel = "Streaming - country freeway"
	sSkipMenu[TEST_Physics_gunfight		 ].sTxtLabel = "Physics - gunfight"
	sSkipMenu[TEST_Physics_gunfight_5Star].sTxtLabel = "Physics - gunfight 5Star"
	sSkipMenu[TEST_Physics_Vehicles		 ].sTxtLabel = "Physics - Vehicles"						


	IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
		PRINTLN("FPS TEST SCRIPT MAPONLY STARTING")
	ELSE
		PRINTLN("FPS TEST SCRIPT STARTING")
	ENDIF
	
	index = 0
	average_repeat = 0
	
	ADD_RELATIONSHIP_GROUP("RE_ARREST_COP", relGroupCop)
	ADD_RELATIONSHIP_GROUP("RE_ARREST_CRIM", relGroupCrim)
	
	SET_RANDOM_SEED(1)
	SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
	SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)	
	
	WHILE TRUE
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH FPSlocationStage
				
					CASE CHOOSE_SETUP
						
						camera_pitch_mod = 1.0
						camera_pan_mod = 0.3
						settle_time = 4000
						FPSlocationStage = SETUP
						
//						PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Press Numpad 1 for location test. Press Numpad 2 for magdemo locations ", 1000, 1)
						
					BREAK
					
					CASE SETUP
						
						FPSlocation[TEST_Docks_Terminal		   ] = << 814.5111, -3050.1453, 4.7421 >>		// DOCKS
						FPSlocation[TEST_Pershing_Square	   ] = << 132.5859, -985.7192, 28.3605 >>		// PERSHING
						FPSlocation[TEST_Vinewood_Hills		   ] = << -307.9470, 383.8104, 109.3216 >>	// VINEWOOD HILLS
						FPSlocation[TEST_Sandy_Shore		   ] = << 1933.5807, 3780.2559, 31.3052 >>	// SANDY SHORE
						FPSlocation[TEST_Paleto_Bay			   ] = << -148.8289, 6245.2271, 30.1799 >>	// PALETO BAY
						FPSlocation[TEST_Downtown_Day		   ] = << -68.898770,-1100.900024,26.0 >> // Downton Day
						FPSlocation[TEST_Downtown_Night		   ] = << -68.898770,-1100.900024,26.0 >> // Downtown Night
						FPSlocation[TEST_Bean_Machine		   ] = << -664.31,240.41,82.41 >>	// Bean Machine
						FPSlocation[TEST_Pine_Forest		   ] = << -663.69, 5560.35, 39.42 >>	// Pine Forest
						FPSlocation[TEST_Interior_Life_Invaders] = <<-1057.0321, -244.981, 44.9 >> // Life invaders Interior
						FPSlocation[TEST_Streaming_Vinewood	   ] = << -1426.3285, -11.6231, 52.0029 >>	// Streaming test Vinewood blvd
						FPSlocation[TEST_Streaming_freeway	   ] = << 1818.4769, 2010.4287, 75.8026 >>	// Streaming test country freeway
						FPSlocation[TEST_Physics_gunfight	   ] = << -592.5580, -1652.5548, 23.4580 >>	// Physics test
						FPSlocation[TEST_Physics_gunfight_5Star] = <<-592.5580, -1652.5548, 23.4580 >>	// Physics test with wanted level
						FPSlocation[TEST_Physics_Vehicles	   ] = << 1148.8738, 125.4214, 81.0134 >>	// Physics test with vehicles

						IF NOT IS_XBOX360_VERSION()
							IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
								FPSlocationName[TEST_Docks_Terminal		   ] = "mc_Docks_Terminal_maponly"
								FPSlocationName[TEST_Pershing_Square	   ] = "mc_Pershing_Square_maponly"
								FPSlocationName[TEST_Vinewood_Hills		   ] = "mc_Vinewood_Hills_maponly"
								FPSlocationName[TEST_Sandy_Shore		   ] = "mc_Sandy_Shore_maponly"
								FPSlocationName[TEST_Paleto_Bay			   ] = "mc_Paleto_Bay_maponly"
								FPSlocationName[TEST_Downtown_Day		   ] = "mc_Downtown_day_maponly"
								FPSlocationName[TEST_Downtown_Night		   ] = "mc_Downtown_night_maponly"
								FPSlocationName[TEST_Bean_Machine		   ] = "mc_Bean_machine_mapony"
								FPSlocationName[TEST_Pine_Forest		   ] = "mc_Pine_forest_maponly"	
								FPSlocationName[TEST_Interior_Life_Invaders] = "mc_interior_maponly"
								FPSlocationName[TEST_Streaming_Vinewood	   ] = "mc_StreamingVine_maponly"
								FPSlocationName[TEST_Streaming_freeway	   ] = "mc_StreamingFreeway_maponly"
								FPSlocationName[TEST_Physics_gunfight	   ] = "mc_Physics_maponly"
								FPSlocationName[TEST_Physics_gunfight_5Star] = "mc_Physics5Star_maponly"	
								FPSlocationName[TEST_Physics_Vehicles	   ] = "mc_PhysicsVehicle_maponly"	
							ELSE
								FPSlocationName[TEST_Docks_Terminal		   ] = "mc_Docks_Terminal"
								FPSlocationName[TEST_Pershing_Square	   ] = "mc_Pershing_Square"
								FPSlocationName[TEST_Vinewood_Hills		   ] = "mc_Vinewood_Hills"
								FPSlocationName[TEST_Sandy_Shore		   ] = "mc_Sandy_Shore"
								FPSlocationName[TEST_Paleto_Bay			   ] = "mc_Paleto_Bay"
								FPSlocationName[TEST_Downtown_Day		   ] = "mc_Downtown_day"
								FPSlocationName[TEST_Downtown_Night		   ] = "mc_Downtown_night"
								FPSlocationName[TEST_Bean_Machine		   ] = "mc_Bean_machine"
								FPSlocationName[TEST_Pine_Forest		   ] = "mc_Pine_forest"
								FPSlocationName[TEST_Interior_Life_Invaders] = "mc_interior"					
								FPSlocationName[TEST_Streaming_Vinewood	   ] = "mc_StreamingVine"
								FPSlocationName[TEST_Streaming_freeway	   ] = "mc_StreamingFreeway"
								FPSlocationName[TEST_Physics_gunfight	   ] = "mc_Physics"
								FPSlocationName[TEST_Physics_gunfight_5Star] = "mc_Physics5Star"
								FPSlocationName[TEST_Physics_Vehicles	   ] = "mc_PhysicsVehicle"	

							ENDIF
						ELSE
							IF GET_COMMANDLINE_PARAM_EXISTS("maponly")
								FPSlocationName[TEST_Docks_Terminal		   ] = "mc_Docks_Terminal_maponly_XBOX"
								FPSlocationName[TEST_Pershing_Square	   ] = "mc_Pershing_Square_maponly_XBOX"
								FPSlocationName[TEST_Vinewood_Hills		   ] = "mc_Vinewood_Hills_maponly_XBOX"
								FPSlocationName[TEST_Sandy_Shore		   ] = "mc_Sandy_Shore_maponly_XBOX"
								FPSlocationName[TEST_Paleto_Bay			   ] = "mc_Paleto_Bay_maponly_XBOX"
								FPSlocationName[TEST_Downtown_Day		   ] = "mc_Downtown_day_maponly_XBOX"
								FPSlocationName[TEST_Downtown_Night		   ] = "mc_Downtown_night_maponly_XBOX"
								FPSlocationName[TEST_Bean_Machine		   ] = "mc_Bean_machine_mapony_XBOX"
								FPSlocationName[TEST_Pine_Forest		   ] = "mc_Pine_forest_maponly_XBOX"
								FPSlocationName[TEST_Interior_Life_Invaders] = "mc_interior_maponly_XBOX"
								FPSlocationName[TEST_Streaming_Vinewood	   ] = "mc_StreamingVine_maponly_XBOX"
								FPSlocationName[TEST_Streaming_freeway	   ] = "mc_StreamingFreeway_maponly_XBOX"
								FPSlocationName[TEST_Physics_gunfight	   ] = "mc_Physics_maponly_XBOX"
								FPSlocationName[TEST_Physics_gunfight_5Star] = "mc_Physics5Star_maponly_XBOX"						
								FPSlocationName[TEST_Physics_Vehicles	   ] = "mc_PhysicsVehicle_maponly_XBOX"	

							ELSE
								FPSlocationName[TEST_Docks_Terminal		   ] = "mc_Docks_Terminal_XBOX"
								FPSlocationName[TEST_Pershing_Square	   ] = "mc_Pershing_Square_XBOX"
								FPSlocationName[TEST_Vinewood_Hills		   ] = "mc_Vinewood_Hills_XBOX"
								FPSlocationName[TEST_Sandy_Shore		   ] = "mc_Sandy_Shore_XBOX"
								FPSlocationName[TEST_Paleto_Bay			   ] = "mc_Paleto_Bay_XBOX"
								FPSlocationName[TEST_Downtown_Day		   ] = "mc_Downtown_day_XBOX"
								FPSlocationName[TEST_Downtown_Night		   ] = "mc_Downtown_night_XBOX"
								FPSlocationName[TEST_Bean_Machine		   ] = "mc_Bean_machine_XBOX"
								FPSlocationName[TEST_Pine_Forest		   ] = "mc_Pine_forest_XBOX"
								FPSlocationName[TEST_Interior_Life_Invaders] = "mc_interior_XBOX"					
								FPSlocationName[TEST_Streaming_Vinewood	   ] = "mc_StreamingVine_XBOX"
								FPSlocationName[TEST_Streaming_freeway	   ] = "mc_StreamingFreeway_XBOX"
								FPSlocationName[TEST_Physics_gunfight	   ] = "mc_Physics_XBOX"
								FPSlocationName[TEST_Physics_gunfight_5Star] = "mc_Physics5Star_XBOX"
								FPSlocationName[TEST_Physics_Vehicles	   ] = "mc_PhysicsVehicle_XBOX"	

							ENDIF
						ENDIF
						
						METRICS_ZONES_CLEAR()
						PRINTLN("FPS TEST METRICS_ZONES_CLEAR")
//						METRICS_ZONES_SHOW()
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
						SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						DISABLE_AIM_CAM_THIS_UPDATE()
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, 0, TRUE)
						WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
							WAIT(0)
							PRINTLN("SETTING PED TO DEFAULT")
						ENDWHILE
					    SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						FPSlocationStage = WARP_TO_LOCATION
	
						// Fade the screen in
						IF NOT IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							DO_SCREEN_FADE_IN(500)
						ENDIF
	
						IF GET_COMMANDLINE_PARAM_EXISTS("fps_SkipToStreaming")
						OR GET_COMMANDLINE_PARAM_EXISTS("SkipLocationTests")
							index = TEST_Streaming_Vinewood
						ENDIF
						
						IF GET_COMMANDLINE_PARAM_EXISTS("fps_SkipToPhysics")
							index = TEST_Physics_gunfight
						ENDIF

						IF GET_COMMANDLINE_PARAM_EXISTS("fps_FiveStar")
							FIveStarAlwayOn = TRUE
						ENDIF
						
					BREAK
					
					CASE WARP_TO_LOCATION
					
						SET_MAX_WANTED_LEVEL(0)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							
						IF index = TEST_Physics_gunfight_5Star
						OR FIveStarAlwayOn = TRUE
							SET_MAX_WANTED_LEVEL(5)
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 5)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK,TRUE)
						ENDIF
						
						REPEAT numgangpeds i
							IF DOES_ENTITY_EXIST(pedCop[i])
								SET_PED_AS_NO_LONGER_NEEDED(pedCop[i])
							ENDIF
						ENDREPEAT
						REPEAT numgangpeds i
							IF DOES_ENTITY_EXIST(pedCriminal[i])
								SET_PED_AS_NO_LONGER_NEEDED(pedCriminal[i])
							ENDIF
						ENDREPEAT
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
						SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_MEXTHUG_01)
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), FPSlocation[index])
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						new_heading = -90
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(new_heading)						
						CLEAR_AREA(FPSlocation[index], 2000, TRUE)
						
						IF index = TEST_Interior_Life_Invaders
							SET_BUILDING_STATE(BUILDINGNAME_IPL_INVADER_OFFICE_INTERIOR, BUILDINGSTATE_DESTROYED)
						ENDIF
						
						LOAD_SCENE(FPSlocation[index])
						LOAD_ALL_OBJECTS_NOW()
						INSTANTLY_FILL_PED_POPULATION()
						INSTANTLY_FILL_VEHICLE_POPULATION()
						IF index = TEST_Downtown_Night
							SET_CLOCK_TIME(0, 0, 0)
						ELSE
							SET_CLOCK_TIME(17, 0, 0)
						ENDIF
						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
						SETTIMERA(0)
						
						IF index = TEST_Physics_Vehicles
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1177.5040, 138.1767, 79.8866>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 61)
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						ENDIF
						
						FPSlocationStage = WAIT_TO_SETTLE

					BREAK
				
					CASE WAIT_TO_SETTLE
			
						IF index = TEST_Interior_Life_Invaders
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						ENDIF
						
						IF TIMERA() > settle_time
							IF average_repeat = 0
								METRICS_ZONE_START(FPSlocationName[index])
								PRINTLN("FPS TEST METRICS_ZONE_START")
							ENDIF
							SETTIMERA(0)
							IF index = TEST_Interior_Life_Invaders
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							ENDIF
							
							IF index = TEST_Streaming_Vinewood OR index = TEST_Streaming_freeway
								CREATE_CAR_FOR_REC()
								FPSlocationStage = CAR_DRIVE
							ELIF index = TEST_Physics_gunfight or index = TEST_Physics_gunfight_5Star
								SETUP_GUNFIGHT()
								FPSlocationStage = CAMERA_PITCH
							ELIF index = TEST_Physics_Vehicles
								SETUP_VEHICLE_TEST()
								FPSlocationStage = CAR_SMASH
							ELSE
								FPSlocationStage = CAMERA_PITCH
							ENDIF
							
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)
							ENDIF
							
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(settle_time)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
					BREAK

					CASE CAMERA_PITCH
							
						IF TIMERA() > camera_rotate_speed
							SETTIMERA(0)
							new_heading = new_heading + camera_pitch_mod
							IF new_heading < 0
								SET_GAMEPLAY_CAM_RELATIVE_PITCH(new_heading)
							ELSE
								new_heading = 0
								FPSlocationStage = CAMERA_ROTATE
							ENDIF
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(settle_time)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
					BREAK
					
					CASE CAMERA_ROTATE
								
						IF TIMERA() > camera_rotate_speed
							SETTIMERA(0)
							new_heading = new_heading + camera_pan_mod
							IF new_heading < 360
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(new_heading)
							ELSE
								IF average_repeat < 2
									new_heading = 0
									average_repeat ++
								ELSE
									FPSlocationStage = CLEANUP_ZONE
								ENDIF
								
							ENDIF
						ENDIF

						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(settle_time)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
					BREAK

					CASE CAR_DRIVE
					
						IF IS_VEHICLE_DRIVEABLE(veh_car) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_car)
							SETTIMERA(0)
							CLEANUP_ASSETS()
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
						IF NOT GET_COMMANDLINE_PARAM_EXISTS("fps_DontEatCars")
							IF TIMERA() > 180
								vehicles_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
								CLEAR_AREA(vehicles_coords, 16, TRUE)
								SETTIMERA(0)
							ENDIF
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(settle_time)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
		
					BREAK

					CASE CAR_SMASH
						
						IF NOT explosion_triggered
							IF TIMERA() > 6000
								ADD_EXPLOSION(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<1159, 151, 82>>, 0, <<0,0,0>>),  EXP_TAG_PETROL_PUMP, 1)
								explosion_triggered = TRUE
							ENDIF
						ELSE
							IF TIMERA() > 18000
								CLEANUP_ASSETS()
								SETTIMERA(settle_time)
								FPSlocationStage = CLEANUP_ZONE
							ENDIF
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							SETTIMERA(settle_time)
							FPSlocationStage = CLEANUP_ZONE
						ENDIF
						
					BREAK

					CASE CLEANUP_ZONE

						IF TIMERA() > settle_time
							METRICS_ZONE_STOP()
							PRINTLN("FPS TEST METRICS_ZONE_STOP")
							WAIT(100)
							METRICS_ZONES_SHOW()
							METRICS_ZONE_SAVE_TO_FILE(FPSlocationName[index])
							METRICS_ZONE_SAVE_TELEMETRY()
							WAIT(2000)
							METRICS_ZONES_CLEAR()
							WAIT(0)							
							METRICS_ZONES_HIDE()

							IF NOT IS_PC_VERSION()
								SET_DISPLAY_STREAM_GRAPH(TRUE)
								WAIT(2000)
								SAVE_SCREENSHOT(FPSlocationName[index])
								WAIT(500)
								SET_DISPLAY_STREAM_GRAPH(FALSE)
								WAIT(0)
							ENDIF

							new_heading = 0
							average_repeat = 0
							index ++
							IF index < numlocation 
								CLEANUP_ASSETS()
								FPSlocationStage = WARP_TO_LOCATION
							ELSE
								SETTIMERA(0)
								FPSlocationStage = FPS_RESULTS
							ENDIF
							
						ENDIF
					
					BREAK
					
					CASE FPS_RESULTS
						
						//Gives time to view results before cleaning up
						IF TIMERA() > 2000
							CLEANUP_SCRIPT()
						ENDIF
						
					BREAK
					
				ENDSWITCH
			
				INVALIDATE_IDLE_CAM()
			
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					CLEANUP_SCRIPT()
				ENDIF
				
				IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, iSkipToThisStage)
					index = iSkipToThisStage  
					new_heading = 0
					average_repeat = 0
					CLEANUP_ASSETS()
					FPSlocationStage = WARP_TO_LOCATION
			    ENDIF
		
			ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF

