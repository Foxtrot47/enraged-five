//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	debug_ped_data.sc									//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Used to output shop meta for outfits.		 		//
//																				//
//////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "ped_component_public.sch"
USING "tattoo_shop_private.sch"
USING "freemode_header.sch"
USING "clothes_shop_private.sch"
USING "mphud_charactercreatormenu.sch"
USING "mphud_charactercontroller.sch"

ENUM DEBUG_PED_STAGE
	STAGE_INIT = 0,
	STAGE_PROCESSING,
	STAGE_CLEANUP
ENDENUM
DEBUG_PED_STAGE eStage = STAGE_INIT

CONST_INT CLOTHES_SELECT_MENU_MAIN			0
CONST_INT CLOTHES_SELECT_MENU_COMPS			1
CONST_INT CLOTHES_SELECT_MENU_PROPS			2
CONST_INT CLOTHES_SELECT_MENU_DECORATIONS	3
CONST_INT CLOTHES_SELECT_MENU_OUTFITS		4
CONST_INT CLOTHES_SELECT_MENU_MODEL			5
CONST_INT CLOTHES_SELECT_MENU_SHOP_LOCATE	6
CONST_INT CLOTHES_SELECT_MENU_CAMERA 		7

CONST_INT SHOP_LOCATE_WARP_LOCATION_TOPS 		0
CONST_INT SHOP_LOCATE_WARP_LOCATION_PANTS 		1
CONST_INT SHOP_LOCATE_WARP_LOCATION_SHOES 		2
CONST_INT SHOP_LOCATE_WARP_LOCATION_ACCESSORIES 3
CONST_INT SHOP_LOCATE_WARP_LOCATION_GLASSES 	4
CONST_INT SHOP_LOCATE_WARP_LOCATION_HATS 		5
CONST_INT SHOP_LOCATE_WARP_LOCATION_MASKS 		6
CONST_INT SHOP_LOCATE_WARP_LOCATION_OUTFITS 	7
CONST_INT SHOP_LOCATE_WARP_LOCATION_WARDROBE 	8
CONST_INT MAX_SHOP_LOCATE_WARP_LOCATIONS 		9

CONST_INT CAMERA_SELECT_OFF				0
CONST_INT CAMERA_SELECT_HEAD 			1
CONST_INT CAMERA_SELECT_TORSO 			2	
CONST_INT CAMERA_SELECT_LEGS			3
CONST_INT CAMERA_SELECT_FEET 			4
CONST_INT CAMERA_SELECT_HEAD_REAR 		5	
CONST_INT CAMERA_SELECT_TORSO_REAR		6	
CONST_INT CAMERA_SELECT_LEGS_REAR 		7	
CONST_INT CAMERA_SELECT_FEET_REAR 		8	
CONST_INT MAX_CAMERA_SELECT_OPTIONS 	9

STRUCT CLOTHES_SELECTOR_DATA
	BOOL bDisplay
	BOOL bMenuAssetsRequested
	BOOL bMenuInitialised
	BOOL bGrabClothingData
	BOOL bUseCachedClothingData
	BOOL bRebuildMenu
	BOOL bValidateClothes
	BOOL bEnterKeyWord
	BOOL bKeyWordState
	INT iValidateClothesCount
	INT iCurrentItem[2]
	INT iCurrentTopItem[2]
	INT iCurrentItem_copy[2]
	INT iCurrentTopItem_copy[2]
	INT iCurrentMenu
	INT iMenuDepth
	INT iDrawIndex
	SHOP_INPUT_DATA_STRUCT sInputData
	INT iIndex
	INT iTexture
	INT iLastIndex
	INT iLastTexture
	INT iLocalIndex
	INT iOutfitIndex
	INT iOutfitCount
	MODEL_NAMES eModelSelect
	BOOL bSetPlayerModel
	INT iLocateSelect
	BOOL bWarpToLocate
	INT iCameraSelect
	TEXT_LABEL_15 tlItemLabel
	TEXT_LABEL_63 tlDLCKey
	TEXT_LABEL_63 tlDLCPack
	TEXT_LABEL_15 tlKeyWord
	INT iDecorationCollection
	INT iDecorationPreset
	BOOL bDecorationCompatible
	INT iDecorationIndex
	INT iDecorationCount
	BOOL bCompatibleOnly
	BOOL bReleaseUD
	BOOL bReleaseLR
	TEXT_LABEL_63 tlShopMenu
ENDSTRUCT
CLOTHES_SELECTOR_DATA sClothesSelect

BOOL bKillScript

CAMERA_INDEX camPlayerOffset
VECTOR vCamOffsetPos, vCamOffsetPoint
BOOL bUseOffsetCam
BOOl bSetOffsetCamPreset[8]

BOOL bSetPlayerModel_Male
BOOL bSetPlayerModel_Female
INT iSelectedDLCPack
INT iSelectedPedComponent

BOOL bOutputUniqueHashNames
BOOL bOutputShopLocateNames

BOOL bGenerate_ShopMeta
BOOL bGenerate_MissingShopMeta
BOOL bGenerate_MissingSaveItems
BOOL bGenerate_GET_DLC_NAME_HASH_STRING
BOOL bGenerate_GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP
BOOL bGenerate_GET_TATTOO_ENUM_FROM_DLC_HASH
BOOL bGenerate_ClothingInfo
BOOL bGenerate_ClothingScreenshotsPrimary
BOOL bGenerate_ClothingScreenshots
BOOL bDisplay_ShopPedInfo
BOOL bGenerate_ExclsuiveClothingInfo

BOOL bGenerate_PedRenders
BOOL bRunning_PedRenders
INT iTotalPedRenders
INT iCurrentPedRender
INT iCurrentPedRenderState
INT iPedRenderDLCPack

BOOL b_Apply_Freemode_outfit

BOOL b_debug_htb

BOOL b_debug_ped_save[6]
BOOL b_debug_ped_set[6]
BOOL b_debug_ped_create[6]
INT i_debug_process_ped_set
INT i_debug_process_ped_create
BOOL b_debug_output_ped_save_data
TEXT_WIDGET_ID twDebugPedNamed
PED_INDEX pedDebugPedCreate[6]
BOOL b_debug_ped_created[6]
BOOL b_debug_ped_randomise_appearance
BOOL b_debug_ped_create_clone

BOOL b_debug_set_player_and_cam[4]

BOOL b_debug_outfit_output_1
BOOL b_debug_outfit_output_2

INT i_debug_heist_outfit

INT i_debug_heist_outfit_last_set
BOOL b_debug_set_heist_outfit
BOOL b_debug_set_heist_outfit_auto
TEXT_WIDGET_ID t_debug_heist_outfit_name
MP_OUTFITS_APPLY_DATA sApplyOutfitData

INT i_debug_heist_mask
INT i_debug_heist_mask_last_set
BOOL b_debug_set_heist_mask
BOOL b_debug_set_heist_mask_auto
TEXT_WIDGET_ID t_debug_heist_mask_name
MP_OUTFITS_APPLY_DATA sApplyMaskData

INT i_debug_heist_gear
BOOL b_debug_set_heist_gear
BOOL b_debug_remove_heist_gear
BOOL b_debug_output_heist_DLC_hash

INT i_debug_Vs_outfit = ENUM_TO_INT(OUTFIT_VERSUS_CLASSIC_JUNGLE_0)
BOOL b_debug_set_Vs_outfit
INT i_debug_VsTheme_outfit = ENUM_TO_INT(OUTFIT_VERSUS_THEMED_SLASHERS_0) 
BOOL b_debug_set_VsTheme_outfit
INT i_debug_ng_outfit = ENUM_TO_INT(OUTFIT_NG_CASUAL_0) 
BOOL b_debug_set_ng_outfit


INT i_debug_lowr_outfit = ENUM_TO_INT(OUTFIT_VERSUS_LOWR_BLUE_SMART_0) 
BOOL b_debug_set_lowr_outfit

INT i_debug_entourage_outfit = ENUM_TO_INT(OUTFIT_VERSUS_ENTOURAGE_VIP_0) 
BOOL b_debug_set_entourage_outfit

INT i_debug_hal_outfit = ENUM_TO_INT(OUTFIT_VERSUS_HAL_MANIACS_0) 
BOOL b_debug_set_hal_outfit

INT i_debug_solo_outfit = ENUM_TO_INT(OUTFIT_LOW_FLOW_WOLVES_0)
INT i_debug_solo_outfit2 = ENUM_TO_INT(OUTFIT_SOLO_SENIORS_0) 
BOOL b_debug_set_solo_outfit
BOOL b_debug_set_solo_outfit2

INT i_debug_low_flow_outfit = ENUM_TO_INT(OUTFIT_LOW_FLOW_LOWRIDER_0) 
BOOL b_debug_set_low_flow_outfit

INT i_debug_beast_outfit = ENUM_TO_INT(OUTFIT_ADVERSARY_B_OVERRIDE_0) 
BOOL b_debug_set_beast_outfit
INT i_debug_extraction_outfit = ENUM_TO_INT(OUTFIT_VERSUS_EXTRACTION_VIP_0) 
BOOL b_debug_set_extraction_outfit

INT i_debug_gang_boss_vip_outfits = ENUM_TO_INT(OUTFIT_MAGNATE_BOSS_BARON_0) 
BOOL b_debug_set_gang_boss_vip_outfits

INT i_debug_gang_boss_bodyguard_outfits = ENUM_TO_INT(OUTFIT_MAGNATE_GOON_NARCO_0) 
BOOL b_debug_set_gang_boss_bodyguard_outfits

INT i_debug_hidden_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_VINTAGE_GREEN_0) 
TEXT_WIDGET_ID tw_debug_hidden_outfit_name
BOOL b_debug_set_hidden_outfits

INT i_debug_hidden_lowrider_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_LOWRIDER_GREEN_0) 
BOOL b_debug_set_hidden_lowrider_outfits

INT i_debug_classic_competitors_outfits = ENUM_TO_INT(OUTFIT_VERSUS_LOWR2_BROWN_SPORT_0) 
BOOL b_debug_set_classic_competitors_outfits

INT i_debug_trading_places_winner_outfits = ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_WINNER_0)
BOOL b_debug_set_trading_places_winner_outfits

INT i_debug_trading_places_loser_outfits = ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_LOSER_0)
BOOL b_debug_set_trading_places_loser_outfits

INT i_debug_power_play_team_sport_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_PURPLE_0)
BOOL b_debug_set_power_play_team_sport_outfits

INT i_debug_executive_ceo_outfits = ENUM_TO_INT(OUTFIT_EXEC_CEO_FOUNDER_0)
BOOL b_debug_set_executive_ceo_outfits

INT i_debug_executive_associate_outfits = ENUM_TO_INT(OUTFIT_EXEC_ASSOCIATE_OPERATORS_0)
BOOL b_debug_set_executive_associate_outfits

INT i_debug_executive_securoserv_outfits = ENUM_TO_INT(OUTFIT_MAGNATE_GOON_SECUROSERV_0)
BOOL b_debug_set_executive_securoserv_outfits

INT i_debug_power_play_themed_outfits = ENUM_TO_INT(OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_0)
BOOL b_debug_set_power_play_themed_outfits

INT i_debug_stunt_race_outfits = ENUM_TO_INT(OUTFIT_STUNT_RACE_BIKER_0)
BOOL b_debug_set_stunt_race_outfits

INT i_debug_biker_club_outfits_0to3 = ENUM_TO_INT(OUTFIT_BIKER_DIRT_0)
BOOL b_debug_set_biker_club_outfits_0to3

INT i_debug_biker_club_outfits_4to7 = ENUM_TO_INT(OUTFIT_BIKER_DIRT_4)
BOOL b_debug_set_biker_club_outfits_4to7

INT i_debug_biker_deadline_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_DEADLINE_PURPLE_0)
BOOL b_debug_set_biker_deadline_outfits

INT i_debug_biker_slipstream_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_TEAM_STUNT_BIKER_PURPLE_0)
BOOL b_debug_set_biker_slipstream_outfits

INT i_debug_biker_lost_damned_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_LOST_DEVILS_0)
BOOL b_debug_set_biker_lost_damned_outfits

INT i_debug_biker_club_rank_outfits = ENUM_TO_INT(OUTFIT_BIKER_RANK_0)
BOOL b_debug_set_biker_club_rank_outfits

INT i_debug_impexp_ceo_outfits = ENUM_TO_INT(OUTFIT_IE_CEO_LONGLINE_0)
BOOL b_debug_set_impexp_ceo_outfits

INT i_debug_impexp_associate_outfits = ENUM_TO_INT(OUTFIT_IE_LONGLINE_0)
BOOL b_debug_set_impexp_associate_outfits

INT i_debug_impexp_juggernaut_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0)
BOOL b_debug_set_impexp_juggernaut_outfits

INT i_debug_impexp_juggernaut_orange_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0)
BOOL b_debug_set_impexp_juggernaut_orange_outfits

INT i_debug_impexp_juggernaut_purple_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0)
BOOL b_debug_set_impexp_juggernaut_purple_outfits

INT i_debug_specraces_general_combat_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_GENCOMBAT_FOREST_0)
BOOL b_debug_set_specraces_general_combat_outfits

INT i_debug_specraces_land_grab_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_SR_LG_ORANGE_0)
BOOL b_debug_set_specraces_land_grab_outfits

INT i_debug_gunrun_lions_den_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_LIONSDEN_ATTACKER_0)
BOOL b_debug_set_gunrun_lions_den_outfits

INT i_debug_gunrun_dawn_raid_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_DAWNRAID_ORANGE_0)
BOOL b_debug_set_gunrun_dawn_raid_outfits

INT i_debug_gunrun_biker_zombie_outfits = ENUM_TO_INT(OUTFIT_BIKER_ZOMBIE_0)
BOOL b_debug_set_gunrun_biker_zombie_outfits

INT i_debug_gunrun_biker_raider_outfits = ENUM_TO_INT(OUTFIT_BIKER_RAIDER_0)
BOOL b_debug_set_gunrun_biker_raider_outfits

INT i_debug_gunrun_biker_puffer_outfits = ENUM_TO_INT(OUTFIT_BIKER_PUFFER_0)
BOOL b_debug_set_gunrun_biker_puffer_outfits

INT i_debug_gunrun_biker_hillbilly_outfits = ENUM_TO_INT(OUTFIT_BIKER_HILLBILLY_0)
BOOL b_debug_set_gunrun_biker_hillbilly_outfits

INT i_debug_gunrun_ceo_outfits = ENUM_TO_INT(OUTFIT_GUNR_CEO_SURVIVALIST_0)
BOOL b_debug_set_gunrun_ceo_outfits

INT i_debug_gunrun_associate_outfits = ENUM_TO_INT(OUTFIT_GUNR_ASSOCIATE_SURVIVALIST_0)
BOOL b_debug_set_gunrun_associate_outfits

INT i_debug_gunrun_team_general_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_GENERAL_ORANGE_0)
BOOL b_debug_set_gunrun_team_general_outfits

INT i_debug_gunrun_power_mad_juggernaut_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PINK_TARGET_0)
BOOL b_debug_set_gunrun_power_mad_juggernaut_outfits

INT i_debug_gunrun_power_mad_team_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GR_POWERMAD_ORANGE_0)
BOOL b_debug_set_gunrun_power_mad_team_outfits

INT i_debug_gunrun_wvm_oppressor_outfits = ENUM_TO_INT(OUTFIT_WVM_OPPRESSOR_0)
BOOL b_debug_set_gunrun_wvm_oppressor_outfits

INT i_debug_smuggler_hostile_takeover_outfits = ENUM_TO_INT(OUTFIT_HIDDEN_HOSTILE_TAKEOVER_GREEN_0)
BOOL b_debug_set_smuggler_hostile_takeover_outfits

INT i_debug_smuggler_condemned_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_CND_CONDEMNED)
BOOL b_debug_set_smuggler_condemned_outfits

INT i_debug_smuggler_vehicle_warfare_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_VEHICLE_WARFARE_PURPLE_0)
BOOL b_debug_set_smuggler_vehicle_warfare_outfits

INT i_debug_smuggler_air_shootout_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_AIR_SHOOTOUT_PURPLE_0)
BOOL b_debug_set_smuggler_air_shootout_outfits

INT i_debug_smuggler_bombushka_run_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_BOMBUSHKA_RUN_BLACK_0)
BOOL b_debug_set_smuggler_bombushka_run_outfits

INT i_debug_smuggler_stockpile_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_STOCKPILE_PURPLE_0)
BOOL b_debug_set_smuggler_stockpile_outfits

INT i_debug_smuggler_race_outfits = ENUM_TO_INT(OUTFIT_SMUGGLER_RACE_SUIT_FLOW)
BOOL b_debug_set_smuggler_race_outfits

// Organisation outfits
INT i_debug_smuggler_ceo_outfits = ENUM_TO_INT(OUTFIT_SMUGGLER_CEO_FORMAL_PILOT_0)
BOOL b_debug_set_smuggler_ceo_outfits

INT i_debug_smuggler_associate_outfits = ENUM_TO_INT(OUTFIT_SMUGGLER_ASSOCIATE_FORMAL_PILOT_0)
BOOL b_debug_set_smuggler_associate_outfits

INT i_debug_smuggler_biker_casual_pilot_outfits = ENUM_TO_INT(OUTFIT_BIKER_CASUAL_PILOT_0)
BOOl b_debug_set_smuggler_biker_casual_pilot_outfits

INT i_debug_smuggler_biker_pirate_outfits = ENUM_TO_INT(OUTFIT_BIKER_PIRATE_0)
BOOl b_debug_set_smuggler_biker_pirate_outfits

INT i_debug_gangops_under_control_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_UNDER_CONTROL)
BOOL b_debug_set_gangops_under_control_outfit

INT i_debug_gangops_heist_scuba_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA)
BOOL b_debug_set_gangops_heist_scuba_outfit

INT i_debug_gangops_heist_paramedic_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_PARAMEDIC_BLUE_0)
BOOL b_debug_set_gangops_heist_paramedic_outfit

INT i_debug_gangops_heist_medtech1_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT1_0)
BOOL b_debug_set_gangops_heist_medtech1_outfit

INT i_debug_gangops_heist_medtech2_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT2_0)
BOOL b_debug_set_gangops_heist_medtech2_outfit

INT i_debug_gangops_heist_modern_stealth_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MODERN_STEALTH_0)
BOOL b_debug_set_gangops_heist_modern_stealth_outfit

INT i_debug_gangops_heist_casual_pilot_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CASUAL_PILOT_0)
BOOL b_debug_set_gangops_heist_casual_pilot_outfit

INT i_debug_gangops_heist_fighter_pilot_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_FIGHTER_PILOT_0)
BOOL b_debug_set_gangops_heist_fighter_pilot_outfit

INT i_debug_gangops_heist_high_tech_riot_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_RIOT_0)
BOOL b_debug_set_gangops_heist_high_tech_riot_outfit

INT i_debug_gangops_heist_high_tech_impact_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_IMPACT_0)
BOOL b_debug_set_gangops_heist_high_tech_impact_outfit

INT i_debug_gangops_heist_med_tech_masked_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_MASKED_0)
BOOL b_debug_set_gangops_heist_med_tech_masked_outfit

INT i_debug_gangops_heist_med_tech_rebellion_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_REBELLION_0)
BOOL b_debug_set_gangops_heist_med_tech_rebellion_outfit

INT i_debug_gangops_heist_med_tech_havoc_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_HAVOC_0)
BOOL b_debug_set_gangops_heist_med_tech_havoc_outfit

INT i_debug_gangops_heist_med_tech_adaptable_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_ADAPTABLE_0)
BOOL b_debug_set_gangops_heist_med_tech_adaptable_outfit

INT i_debug_gangops_heist_sub_driver_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SUB_DRIVER_0)
BOOL b_debug_set_gangops_heist_sub_driver_outfit

INT i_debug_gangops_heist_heavy_combat_gear_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HEAVY_COMBAT_GEAR_0)
BOOL b_debug_set_gangops_heist_heavy_combat_gear_outfit

INT i_debug_gangops_heist_low_tech_tactical_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_TACTICAL_0)
BOOL b_debug_set_gangops_heist_low_tech_tactical_outfit

INT i_debug_gangops_heist_low_tech_combat_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_COMBAT_0)
BOOL b_debug_set_gangops_heist_low_tech_combat_outfit

INT i_debug_gangops_heist_classic_stealth_gear_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CLASSIC_STEALTH_GEAR_0)
BOOL b_debug_set_gangops_heist_classic_stealth_gear_outfit

INT i_debug_gangops_heist_military_camo_gear_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MILITARY_CAMO_GEAR_0)
BOOL b_debug_set_gangops_heist_military_camo_gear_outfit

INT i_debug_gangops_heist_gorka_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_GORKA_0)
BOOL b_debug_set_gangops_heist_gorka_outfit

INT i_debug_gangops_slashers_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_SL_CLOWN_0)
BOOL b_debug_set_gangops_slashers_outfit

INT i_debug_gangops_hard_target_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_HT_BLACK_TARGET_0)
BOOL b_debug_set_gangops_hard_target_outfit

INT i_debug_gangops_air_quota_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_AIR_QUOTA_PURPLE_0)
BOOL b_debug_set_gangops_air_quota_outfit

INT i_debug_gangops_heist_scuba2_outfits = ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA_NOTANK)
BOOL b_debug_set_gangops_heist_scuba2_outfit

INT i_debug_target_races_showdown_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SHOWDOWN_PURPLE_0)
BOOL b_debug_set_target_races_showdown_outfit

INT i_debug_target_races_trapdoor_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRAPDOOR_PURPLE_0)
BOOL b_debug_set_target_races_trapdoor_outfit

INT i_debug_target_races_venetian_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_COP)
BOOL b_debug_set_target_races_venetian_outfit

INT i_debug_target_races_venetian_firesuits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_FIRESUIT_CLASSIC_BLUE)
BOOL b_debug_set_target_races_venetian_firesuits

INT i_debug_business_battles_drop_the_bomb_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_DROPBOMB_PURPLE_0)
BOOL b_debug_set_business_battles_drop_the_bomb_outfit

INT i_debug_business_battles_sumo_run_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUMORUN_PURPLE_0)
BOOL b_debug_set_business_battles_sumo_run_outfit

INT i_debug_business_battles_offense_defense_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_OFFDEF_PURPLE_0)
BOOL b_debug_set_business_battles_offense_defense_outfit

INT i_debug_business_battles_hunting_pack_remix_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_HUNTBACK_REMIX_HOONS_0)
BOOL b_debug_set_business_battles_hunting_pack_remix_outfit

INT i_debug_business_battles_trading_places_remix_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_0)
BOOL b_debug_set_business_battles_trading_places_remix_outfit

INT i_debug_business_battles_come_out_to_play_remix_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_COMEPLAYREMIX_HUNTER_0)
BOOL b_debug_set_business_battles_come_out_to_play_remix_outfit

INT i_debug_business_battles_running_back_remix_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_BLACK)
BOOL b_debug_set_business_battles_running_back_remix_outfit

INT i_debug_business_battles_nightclub_chiliad_outfits = ENUM_TO_INT(OUTFIT_NIGHTCLUB_CHILIAD_0)
BOOL b_debug_set_business_battles_nightclub_chiliad_outfit

INT i_debug_business_battles_nightclub_kifflom_outfits = ENUM_TO_INT(OUTFIT_NIGHTCLUB_KIFFLOM_0)
BOOL b_debug_set_business_battles_nightclub_kifflom_outfit

INT i_debug_business_battles_running_back_remix_2_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_GREEN)
BOOL b_debug_set_business_battles_running_back_remix_2_outfit

INT i_debug_arena_wars_contender_outfits = ENUM_TO_INT(OUTFIT_ARENA_WARS_CONTENDER_GENERAL_0)
BOOL b_debug_set_arena_wars_contender_outfit

BOOL b_debug_set_casino_impotent_rage_outfit
BOOL b_debug_set_casino_highroller_outfit

INT i_debug_casino_spaceling_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SPACELING_0)
BOOL b_debug_set_casino_spaceling_outfit

#IF FEATURE_CASINO_HEIST
BOOL b_debug_set_casino_heist_undertaker_outfit

INT i_debug_casino_heist_direct_light_i_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_0)
BOOL b_debug_set_casino_heist_direct_light_i_outfit

INT i_debug_casino_heist_direct_light_ii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_0)
BOOL b_debug_set_casino_heist_direct_light_ii_outfit

INT i_debug_casino_heist_direct_light_iii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_0)
BOOL b_debug_set_casino_heist_direct_light_iii_outfit

INT i_debug_casino_heist_direct_heavy_i_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_0)
BOOL b_debug_set_casino_heist_direct_heavy_i_outfit

INT i_debug_casino_heist_direct_heavy_ii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_0)
BOOL b_debug_set_casino_heist_direct_heavy_ii_outfit

INT i_debug_casino_heist_direct_heavy_iii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_0)
BOOL b_debug_set_casino_heist_direct_heavy_iii_outfit

INT i_debug_casino_heist_fib_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIB_0)
BOOL b_debug_set_casino_heist_fib_outfit

INT i_debug_casino_heist_stealth_i_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_I_0)
BOOL b_debug_set_casino_heist_stealth_i_outfit

INT i_debug_casino_heist_stealth_ii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_II_0)
BOOL b_debug_set_casino_heist_stealth_ii_outfit

INT i_debug_casino_heist_stealth_iii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_III_0)
BOOL b_debug_set_casino_heist_stealth_iii_outfit

INT i_debug_casino_heist_covert_stealth_i_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_0)
BOOL b_debug_set_casino_heist_covert_stealth_i_outfit

INT i_debug_casino_heist_covert_stealth_ii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_0)
BOOL b_debug_set_casino_heist_covert_stealth_ii_outfit

INT i_debug_casino_heist_covert_stealth_iii_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_0)
BOOL b_debug_set_casino_heist_covert_stealth_iii_outfit

BOOL b_debug_set_casino_heist_valet_outfit

INT i_debug_casino_heist_firefighter_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_0)
BOOL b_debug_set_casino_heist_firefighter_outfit

INT i_debug_casino_heist_noose_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_0)
BOOL b_debug_set_casino_heist_noose_outfit

INT i_debug_casino_heist_gruppe_sechs_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0)
BOOL b_debug_set_casino_heist_gruppe_sechs_outfit

INT i_debug_casino_heist_bugstars_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_0)
BOOL b_debug_set_casino_heist_bugstars_outfit

INT i_debug_casino_heist_celeb_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_0)
BOOL b_debug_set_casino_heist_celeb_outfit

INT i_debug_casino_heist_maintenance_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
BOOL b_debug_set_casino_heist_maintenance_outfit

INT i_debug_casino_heist_prison_guard_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_PRISON_GUARD_0)
BOOL b_debug_set_casino_heist_prison_guard_outfit

INT i_debug_casino_heist_high_roller_outfits = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_0)
BOOL b_debug_set_casino_heist_high_roller_outfit

INT i_debug_sum20_outfit = ENUM_TO_INT(OUTFIT_SUM20_ALIEN_AWARD)
BOOL b_debug_set_sum20_outfit

#ENDIF

#IF FEATURE_HEIST_ISLAND

INT i_debug_island_heist_guard_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_0)
BOOL b_debug_set_island_heist_guard_outfit

INT i_debug_island_heist_smuggler_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_0)
BOOL b_debug_set_island_heist_smuggler_outfit

INT i_debug_island_heist_heavy_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_HEAVY_1_0)
BOOL b_debug_set_island_heist_heavy_outfit

INT i_debug_island_heist_light_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_LIGHT_1_0)
BOOL b_debug_set_island_heist_light_outfit

INT i_debug_island_heist_stealth_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_STEALTH_1_0)
BOOL b_debug_set_island_heist_stealth_outfit

INT i_debug_island_heist_beach_party_outfits = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_BEACH_PARTY_0)
BOOL b_debug_set_island_heist_beach_party_outfit

#ENDIF

#IF FEATURE_TUNER
INT i_debug_tuner_robber_outfits = ENUM_TO_INT(OUTFIT_TUNER_ROBBER_BRAVADO)
BOOL b_debug_set_tuner_robber_outfit

INT i_debug_tuner_security_outfits = ENUM_TO_INT(OUTFIT_TUNER_SECURITY_0)
BOOL b_debug_set_tuner_security_outfit

INT i_debug_tuner_lost_mc_outfits = ENUM_TO_INT(OUTFIT_TUNER_LOST_MC_0)
BOOL b_debug_set_tuner_lost_mc_outfit

INT i_debug_tuner_dock_worker_outfits = ENUM_TO_INT(OUTFIT_TUNER_DOCK_WORKER_0)
BOOL b_debug_set_tuner_dock_worker_outfit
#ENDIF

#IF FEATURE_FIXER
INT i_debug_fixer_setup_outfits = ENUM_TO_INT(OUTFIT_FIXER_SETUP_0)
BOOL b_debug_set_fixer_setup_outfit

INT i_debug_fixer_party_promoter_outfits = ENUM_TO_INT(OUTFIT_FIXER_PARTY_PROMOTER_0)
BOOL b_debug_set_fixer_party_promoter_outfit

INT i_debug_fixer_billionaire_games_outfits = ENUM_TO_INT(OUTFIT_FIXER_BILLIONAIRE_GAMES_0)
BOOL b_debug_set_fixer_billionaire_games_outfit

INT i_debug_fixer_golf_outfits = ENUM_TO_INT(OUTFIT_FIXER_GOLF_0)
BOOL b_debug_set_fixer_golf_outfit

BOOL b_debug_set_heist_navy_coveralls
#ENDIF

#IF FEATURE_DLC_1_2022
BOOL b_debug_set_ld_organics_award

INT i_debug_sum22_iaa_agent_outfits = ENUM_TO_INT(OUTFIT_SUM22_IAA_AGENT_0)
BOOL b_debug_set_sum22_iaa_agent_outfit

INT i_debug_sum22_halloween_riders_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_0)
BOOL b_debug_set_sum22_halloween_riders_outfit

INT i_debug_sum22_halloween_hunted_outfits = ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_0)
BOOL b_debug_set_sum22_halloween_hunted_outfit
#ENDIF

ENUM DLC_PACK_ENUM
	DLC_PACK_MP_BEACH,
	DLC_PACK_MP_XMAS,
	DLC_PACK_MP_VALENTINES,
	DLC_PACK_MP_BUSINESS,
	DLC_PACK_MP_BUSINESS2,
	DLC_PACK_MP_HIPSTER,
	DLC_PACK_MP_INDEPENDENCE,
	DLC_PACK_MP_PILOT,
	DLC_PACK_MP_LTS,
	DLC_PACK_MP_XMAS2,
	DLC_PACK_MP_HEIST,
	DLC_PACK_MP_LUXE,
	DLC_PACK_MP_LUXE2,
	DLC_PACK_MP_REPLAY,
	DLC_PACK_MP_LOWRIDER,
	DLC_PACK_MP_HALLOWEEN,
	DLC_PACK_MP_APARTMENT,
	DLC_PACK_MP_XMAS3,
	DLC_PACK_MP_JANUARY2016,
	DLC_PACK_MP_VALENTINES2,
	DLC_PACK_MP_LOWRIDER2,
	DLC_PACK_MP_EXECUTIVE,
	DLC_PACK_MP_STUNT,
	DLC_PACK_MP_BIKER,
	DLC_PACK_MP_IMPORTEXPORT,
	DLC_PACK_MP_GUNRUNNING,
	DLC_PACK_MP_AIRRACES,
	DLC_PACK_MP_SMUGGLER,
	DLC_PACK_MP_CHRISTMAS2017,
	DLC_PACK_MP_ASSAULT,
	DLC_PACK_MP_BATTLE,
	DLC_PACK_MP_CHRISTMAS2018,
	DLC_PACK_MP_VINEWOOD,
	DLC_PACK_MP_HEIST3,
	DLC_PACK_MP_SUM,
	DLC_PACK_MP_HEIST4,
	DLC_PACK_MP_TUNER,
	DLC_PACK_MP_FIXER,
	DLC_PACK_MP_G9EC,
	DLC_PACK_MP_SUM2,
	DLC_PACK_MP_SUM2_G9EC,
	
	DLC_PACK_MP_MAX
ENDENUM

FUNC STRING GET_DLC_PACK_NAME(DLC_PACK_ENUM eDLCPack)

	SWITCH eDLCPack
		CASE DLC_PACK_MP_BEACH			RETURN "BEACH" 				BREAK
		CASE DLC_PACK_MP_XMAS			RETURN "XMAS" 				BREAK
		CASE DLC_PACK_MP_VALENTINES		RETURN "VALENTINES" 		BREAK
		CASE DLC_PACK_MP_BUSINESS		RETURN "BUSINESS" 			BREAK
		CASE DLC_PACK_MP_BUSINESS2		RETURN "BUSINESS2" 			BREAK
		CASE DLC_PACK_MP_HIPSTER		RETURN "HIPSTER" 			BREAK
		CASE DLC_PACK_MP_INDEPENDENCE	RETURN "INDEPENDENE" 		BREAK
		CASE DLC_PACK_MP_PILOT			RETURN "PILOT" 				BREAK
		CASE DLC_PACK_MP_LTS			RETURN "LTS" 				BREAK
		CASE DLC_PACK_MP_XMAS2			RETURN "XMAS2" 				BREAK
		CASE DLC_PACK_MP_HEIST			RETURN "HEIST" 				BREAK
		CASE DLC_PACK_MP_LUXE			RETURN "LUXE" 				BREAK
		CASE DLC_PACK_MP_LUXE2			RETURN "LUXE2" 				BREAK
		CASE DLC_PACK_MP_REPLAY			RETURN "REPLAY" 			BREAK
		CASE DLC_PACK_MP_LOWRIDER		RETURN "LOWRIDER" 			BREAK
		CASE DLC_PACK_MP_HALLOWEEN		RETURN "HALLOWEEN" 			BREAK
		CASE DLC_PACK_MP_APARTMENT		RETURN "APARTMENT" 			BREAK
		CASE DLC_PACK_MP_XMAS3			RETURN "XMAS3" 				BREAK
		CASE DLC_PACK_MP_JANUARY2016	RETURN "JANUARY2016" 		BREAK
		CASE DLC_PACK_MP_VALENTINES2	RETURN "VALENTINES2"		BREAK
		CASE DLC_PACK_MP_LOWRIDER2		RETURN "LOWRIDER2" 			BREAK
		CASE DLC_PACK_MP_EXECUTIVE		RETURN "EXECUTIVE" 			BREAK
		CASE DLC_PACK_MP_STUNT			RETURN "STUNT" 				BREAK
		CASE DLC_PACK_MP_BIKER 			RETURN "BIKER" 				BREAK
		CASE DLC_PACK_MP_IMPORTEXPORT	RETURN "IMPORTEXPORT" 		BREAK
		CASE DLC_PACK_MP_GUNRUNNING		RETURN "GUNRUNNING" 		BREAK
		CASE DLC_PACK_MP_AIRRACES		RETURN "AIRRACES" 			BREAK
		CASE DLC_PACK_MP_SMUGGLER		RETURN "SMUGGLER" 			BREAK
		CASE DLC_PACK_MP_CHRISTMAS2017	RETURN "CHRISTMAS2017" 		BREAK
		CASE DLC_PACK_MP_ASSAULT		RETURN "ASSAULT" 			BREAK
		CASE DLC_PACK_MP_BATTLE			RETURN "BATTLE" 			BREAK
		CASE DLC_PACK_MP_CHRISTMAS2018	RETURN "CHRISTMAS2018" 		BREAK
		CASE DLC_PACK_MP_VINEWOOD		RETURN "VINEWOOD" 			BREAK
		CASE DLC_PACK_MP_HEIST3			RETURN "HEIST3" 			BREAK
		CASE DLC_PACK_MP_SUM			RETURN "SUMMER2020"			BREAK
		CASE DLC_PACK_MP_HEIST4			RETURN "HEIST4" 			BREAK
		CASE DLC_PACK_MP_TUNER			RETURN "TUNER"				BREAK
		CASE DLC_PACK_MP_FIXER			RETURN "FIXER"				BREAK
		CASE DLC_PACK_MP_G9EC			RETURN "GEN9EC"				BREAK
		CASE DLC_PACK_MP_SUM2			RETURN "SUMMER2022"			BREAK
		CASE DLC_PACK_MP_SUM2_G9EC		RETURN "SUMMER2022_G9EC"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE: Grab the dlcName attribute string found in shop meta:
///          //gta5_dlc/mpPacks/mpBeach/build/dev_ng/common/data/mp_m_freemode_01_beach_shop.meta
FUNC STRING GET_SHOP_PED_APPAREL_DLC_NAME(DLC_PACK_ENUM eDLCPack, MODEL_NAMES eModel)

	IF eModel = MP_M_FREEMODE_01
		SWITCH eDLCPack
			CASE DLC_PACK_MP_BEACH			RETURN "male_freemode_beach" 			BREAK
			CASE DLC_PACK_MP_XMAS			RETURN "male_xmas" 						BREAK
			CASE DLC_PACK_MP_VALENTINES		RETURN "male_freemode_valentines" 		BREAK
			CASE DLC_PACK_MP_BUSINESS		RETURN "male_freemode_business" 		BREAK
			CASE DLC_PACK_MP_BUSINESS2		RETURN "male_freemode_business2" 		BREAK
			CASE DLC_PACK_MP_HIPSTER		RETURN "male_freemode_hipster" 			BREAK
			CASE DLC_PACK_MP_INDEPENDENCE	RETURN "male_freemode_independence" 	BREAK
			CASE DLC_PACK_MP_PILOT			RETURN "male_freemode_pilot" 			BREAK
			CASE DLC_PACK_MP_LTS			RETURN "male_freemode_mplts" 			BREAK
			CASE DLC_PACK_MP_XMAS2			RETURN "male_xmas2" 					BREAK
			CASE DLC_PACK_MP_HEIST			RETURN "male_heist" 					BREAK
			CASE DLC_PACK_MP_LUXE			RETURN "mp_m_luxe_01" 					BREAK
			CASE DLC_PACK_MP_LUXE2			RETURN "mp_m_luxe_02" 					BREAK
			CASE DLC_PACK_MP_REPLAY			RETURN "mp_m_htb_01" 					BREAK
			CASE DLC_PACK_MP_LOWRIDER		RETURN "mp_m_lowrider_01" 				BREAK
			CASE DLC_PACK_MP_HALLOWEEN		RETURN "male_freemode_halloween" 		BREAK
			CASE DLC_PACK_MP_APARTMENT		RETURN "male_apt01" 					BREAK
			CASE DLC_PACK_MP_XMAS3			RETURN "mp_m_xmas_03" 					BREAK
			CASE DLC_PACK_MP_JANUARY2016	RETURN "mp_m_january2016" 				BREAK
			CASE DLC_PACK_MP_VALENTINES2	RETURN "mp_m_valentines_02" 			BREAK
			CASE DLC_PACK_MP_LOWRIDER2		RETURN "mp_m_lowrider_02" 				BREAK
			CASE DLC_PACK_MP_EXECUTIVE		RETURN "mp_m_executive_01" 				BREAK
			CASE DLC_PACK_MP_STUNT			RETURN "mp_m_stunt_01" 					BREAK
			CASE DLC_PACK_MP_BIKER 			RETURN "mp_m_bikerdlc_01" 				BREAK
			CASE DLC_PACK_MP_IMPORTEXPORT	RETURN "mp_m_importexport_01" 			BREAK
			CASE DLC_PACK_MP_GUNRUNNING		RETURN "mp_m_gunrunning_01" 			BREAK
			CASE DLC_PACK_MP_AIRRACES		RETURN "mp_m_airraces_01" 				BREAK
			CASE DLC_PACK_MP_SMUGGLER		RETURN "mp_m_smuggler_01" 				BREAK
			CASE DLC_PACK_MP_CHRISTMAS2017	RETURN "mp_m_christmas2017" 			BREAK
			CASE DLC_PACK_MP_ASSAULT		RETURN "mp_m_assault" 					BREAK
			CASE DLC_PACK_MP_BATTLE			RETURN "mp_m_battle" 					BREAK
			CASE DLC_PACK_MP_CHRISTMAS2018	RETURN "mp_m_christmas2018" 			BREAK
			CASE DLC_PACK_MP_VINEWOOD		RETURN "mp_m_vinewood" 					BREAK
			CASE DLC_PACK_MP_HEIST3			RETURN "mp_m_heist3" 					BREAK
			CASE DLC_PACK_MP_SUM			RETURN "mp_m_sum"						BREAK
			CASE DLC_PACK_MP_HEIST4			RETURN "mp_m_heist4"					BREAK
			CASE DLC_PACK_MP_TUNER			RETURN "mp_m_tuner"						BREAK
			CASE DLC_PACK_MP_FIXER			RETURN "mp_m_security"					BREAK
			CASE DLC_PACK_MP_G9EC			RETURN "mp_m_g9ec"						BREAK
			CASE DLC_PACK_MP_SUM2			RETURN "mp_m_sum2"						BREAK
			CASE DLC_PACK_MP_SUM2_G9EC		RETURN "mp_m_sum2_g9ec"					BREAK
		ENDSWITCH
	ELIF eModel = MP_F_FREEMODE_01
		SWITCH eDLCPack
			CASE DLC_PACK_MP_BEACH			RETURN "female_freemode_beach" 			BREAK
			CASE DLC_PACK_MP_XMAS			RETURN "female_xmas" 					BREAK
			CASE DLC_PACK_MP_VALENTINES		RETURN "female_freemode_valentines" 	BREAK
			CASE DLC_PACK_MP_BUSINESS		RETURN "female_freemode_business" 		BREAK
			CASE DLC_PACK_MP_BUSINESS2		RETURN "female_freemode_business2" 		BREAK
			CASE DLC_PACK_MP_HIPSTER		RETURN "female_freemode_hipster" 		BREAK
			CASE DLC_PACK_MP_INDEPENDENCE	RETURN "female_freemode_independence" 	BREAK
			CASE DLC_PACK_MP_PILOT			RETURN "female_freemode_pilot" 			BREAK
			CASE DLC_PACK_MP_LTS			RETURN "female_freemode_mplts" 			BREAK
			CASE DLC_PACK_MP_XMAS2			RETURN "female_xmas2" 					BREAK
			CASE DLC_PACK_MP_HEIST			RETURN "female_heist" 					BREAK
			CASE DLC_PACK_MP_LUXE			RETURN "mp_f_luxe_01" 					BREAK
			CASE DLC_PACK_MP_LUXE2			RETURN "mp_f_luxe_02" 					BREAK
			CASE DLC_PACK_MP_REPLAY			RETURN "mp_f_htb_01" 					BREAK
			CASE DLC_PACK_MP_LOWRIDER		RETURN "mp_f_lowrider_01" 				BREAK
			CASE DLC_PACK_MP_HALLOWEEN		RETURN "female_freemode_halloween" 		BREAK
			CASE DLC_PACK_MP_APARTMENT		RETURN "female_apt01" 					BREAK
			CASE DLC_PACK_MP_XMAS3			RETURN "mp_f_xmas_03" 					BREAK
			CASE DLC_PACK_MP_JANUARY2016	RETURN "mp_f_january2016" 				BREAK
			CASE DLC_PACK_MP_VALENTINES2	RETURN "mp_f_valentines_02" 			BREAK
			CASE DLC_PACK_MP_LOWRIDER2		RETURN "mp_f_lowrider_02" 				BREAK
			CASE DLC_PACK_MP_EXECUTIVE		RETURN "mp_f_executive_01" 				BREAK
			CASE DLC_PACK_MP_STUNT			RETURN "mp_f_stunt_01" 					BREAK
			CASE DLC_PACK_MP_BIKER 			RETURN "mp_f_bikerdlc_01" 				BREAK
			CASE DLC_PACK_MP_IMPORTEXPORT	RETURN "mp_f_importexport_01" 			BREAK
			CASE DLC_PACK_MP_GUNRUNNING		RETURN "mp_f_gunrunning_01" 			BREAK
			CASE DLC_PACK_MP_AIRRACES		RETURN "mp_f_airraces_01" 				BREAK
			CASE DLC_PACK_MP_SMUGGLER		RETURN "mp_f_smuggler_01" 				BREAK
			CASE DLC_PACK_MP_CHRISTMAS2017	RETURN "mp_f_christmas2017" 			BREAK
			CASE DLC_PACK_MP_ASSAULT		RETURN "mp_f_assault" 					BREAK
			CASE DLC_PACK_MP_BATTLE			RETURN "mp_f_battle" 					BREAK
			CASE DLC_PACK_MP_CHRISTMAS2018	RETURN "mp_f_christmas2018" 			BREAK
			CASE DLC_PACK_MP_VINEWOOD		RETURN "mp_f_vinewood" 					BREAK
			CASE DLC_PACK_MP_HEIST3			RETURN "mp_f_heist3" 					BREAK
			CASE DLC_PACK_MP_SUM			RETURN "mp_f_sum"						BREAK
			CASE DLC_PACK_MP_HEIST4			RETURN "mp_f_heist4"					BREAK
			CASE DLC_PACK_MP_TUNER			RETURN "mp_f_tuner"						BREAK
			CASE DLC_PACK_MP_FIXER			RETURN "mp_f_security"					BREAK
			CASE DLC_PACK_MP_G9EC			RETURN "mp_f_g9ec"						BREAK
			CASE DLC_PACK_MP_SUM2			RETURN "mp_f_sum2"						BREAK
			CASE DLC_PACK_MP_SUM2_G9EC		RETURN "mp_f_sum2_g9ec"					BREAK
		ENDSWITCH
	ELSE
		SWITCH eDLCPack
			CASE DLC_PACK_MP_BEACH			RETURN "mpBeach" 						BREAK
			CASE DLC_PACK_MP_XMAS			RETURN "mpChristmas" 					BREAK
			CASE DLC_PACK_MP_VALENTINES		RETURN "mpValentines" 					BREAK
			CASE DLC_PACK_MP_BUSINESS		RETURN "mpBusiness" 					BREAK
			CASE DLC_PACK_MP_BUSINESS2		RETURN "mpBusiness2" 					BREAK
			CASE DLC_PACK_MP_HIPSTER		RETURN "mpHipster" 						BREAK
			CASE DLC_PACK_MP_INDEPENDENCE	RETURN "mpIndependence" 				BREAK
			CASE DLC_PACK_MP_PILOT			RETURN "mpPilot" 						BREAK
			CASE DLC_PACK_MP_LTS			RETURN "mpLTS" 							BREAK
			CASE DLC_PACK_MP_XMAS2			RETURN "mpChristmas2" 					BREAK
			CASE DLC_PACK_MP_HEIST			RETURN "mpHeist" 						BREAK
			CASE DLC_PACK_MP_LUXE			RETURN "mpLuxe" 						BREAK
			CASE DLC_PACK_MP_LUXE2			RETURN "mpLuxe2" 						BREAK
			CASE DLC_PACK_MP_REPLAY			RETURN "mpReplay" 						BREAK
			CASE DLC_PACK_MP_LOWRIDER		RETURN "mpLowrider" 					BREAK
			CASE DLC_PACK_MP_HALLOWEEN		RETURN "mpHalloween" 					BREAK
			CASE DLC_PACK_MP_APARTMENT		RETURN "mpApartment" 					BREAK
			CASE DLC_PACK_MP_XMAS3			RETURN "mpxmas_604490" 					BREAK
			CASE DLC_PACK_MP_JANUARY2016	RETURN "mpJanuary2016" 					BREAK
			CASE DLC_PACK_MP_VALENTINES2	RETURN "mpValentines" 					BREAK
			CASE DLC_PACK_MP_LOWRIDER2		RETURN "mpLowrider2" 					BREAK
			CASE DLC_PACK_MP_EXECUTIVE		RETURN "mpExecutive" 					BREAK
			CASE DLC_PACK_MP_STUNT			RETURN "mpStunt" 						BREAK
			CASE DLC_PACK_MP_BIKER 			RETURN "mpBiker" 						BREAK
			CASE DLC_PACK_MP_IMPORTEXPORT	RETURN "mpImportExport" 				BREAK
			CASE DLC_PACK_MP_GUNRUNNING		RETURN "mpGunrunning" 					BREAK
			CASE DLC_PACK_MP_AIRRACES		RETURN "mpAirraces" 					BREAK
			CASE DLC_PACK_MP_SMUGGLER		RETURN "mpSmuggler" 					BREAK
			CASE DLC_PACK_MP_CHRISTMAS2017	RETURN "mpChristmas2017" 				BREAK
			CASE DLC_PACK_MP_ASSAULT		RETURN "mpAssault" 						BREAK
			CASE DLC_PACK_MP_BATTLE			RETURN "mpBattle" 						BREAK
			CASE DLC_PACK_MP_CHRISTMAS2018	RETURN "mpChristmas2018" 				BREAK
			CASE DLC_PACK_MP_VINEWOOD		RETURN "mpVinewood" 					BREAK
			CASE DLC_PACK_MP_HEIST3			RETURN "mpHeist3" 						BREAK
			CASE DLC_PACK_MP_SUM			RETURN "mpSum"							BREAK
			CASE DLC_PACK_MP_HEIST4			RETURN "mpHeist4"						BREAK
			CASE DLC_PACK_MP_TUNER			RETURN "mpTuner"						BREAK
			CASE DLC_PACK_MP_FIXER			RETURN "mpSecurity"						BREAK
			CASE DLC_PACK_MP_G9EC			RETURN "mpG9EC"							BREAK
			CASE DLC_PACK_MP_SUM2			RETURN "mpSum2"							BREAK
			CASE DLC_PACK_MP_SUM2_G9EC		RETURN "mpSum2_g9ec"					BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_SHOP_PED_APPAREL_PREFIX(DLC_PACK_ENUM eDLCPack)

	SWITCH eDLCPack
		CASE DLC_PACK_MP_BEACH			RETURN "DLC_MP_BEACH" 		BREAK
		CASE DLC_PACK_MP_XMAS			RETURN "DLC_MP_XMAS" 		BREAK
		CASE DLC_PACK_MP_VALENTINES		RETURN "DLC_MP_VAL"			BREAK
		CASE DLC_PACK_MP_BUSINESS		RETURN "DLC_MP_BUSI" 		BREAK
		CASE DLC_PACK_MP_BUSINESS2		RETURN "DLC_MP_BUS2" 		BREAK
		CASE DLC_PACK_MP_HIPSTER		RETURN "DLC_MP_HIPS" 		BREAK
		CASE DLC_PACK_MP_INDEPENDENCE	RETURN "DLC_MP_IND" 		BREAK
		CASE DLC_PACK_MP_PILOT			RETURN "DLC_MP_PILOT" 		BREAK
		CASE DLC_PACK_MP_LTS			RETURN "DLC_MP_LTS" 		BREAK
		CASE DLC_PACK_MP_XMAS2			RETURN "DLC_MP_XMAS2" 		BREAK
		CASE DLC_PACK_MP_HEIST			RETURN "DLC_MP_HEIST" 		BREAK
		CASE DLC_PACK_MP_LUXE			RETURN "DLC_MP_LUXE" 		BREAK
		CASE DLC_PACK_MP_LUXE2			RETURN "DLC_MP_LUXE2" 		BREAK
		CASE DLC_PACK_MP_REPLAY			RETURN "DLC_MP_REPLAY" 		BREAK
		CASE DLC_PACK_MP_LOWRIDER		RETURN "DLC_MP_LOW" 		BREAK
		CASE DLC_PACK_MP_HALLOWEEN		RETURN "DLC_MP_HALLOWEEN" 	BREAK
		CASE DLC_PACK_MP_APARTMENT		RETURN "DLC_MP_APA" 		BREAK
		CASE DLC_PACK_MP_XMAS3			RETURN "DLC_MP_XMAS3" 		BREAK
		CASE DLC_PACK_MP_JANUARY2016	RETURN "DLC_MP_JAN" 		BREAK
		CASE DLC_PACK_MP_VALENTINES2	RETURN "DLC_MP_VAL2" 		BREAK
		CASE DLC_PACK_MP_LOWRIDER2		RETURN "DLC_MP_LOW2" 		BREAK
		CASE DLC_PACK_MP_EXECUTIVE		RETURN "DLC_MP_EXEC" 		BREAK
		CASE DLC_PACK_MP_STUNT			RETURN "DLC_MP_STUNT" 		BREAK
		CASE DLC_PACK_MP_BIKER 			RETURN "DLC_MP_BIKER" 		BREAK
		CASE DLC_PACK_MP_IMPORTEXPORT	RETURN "DLC_MP_IE" 			BREAK
		CASE DLC_PACK_MP_GUNRUNNING		RETURN "DLC_MP_GR" 			BREAK
		CASE DLC_PACK_MP_AIRRACES		RETURN "DLC_MP_AR" 			BREAK
		CASE DLC_PACK_MP_SMUGGLER		RETURN "DLC_MP_SMUG" 		BREAK
		CASE DLC_PACK_MP_CHRISTMAS2017	RETURN "DLC_MP_X17" 		BREAK
		CASE DLC_PACK_MP_ASSAULT		RETURN "DLC_MP_ASS" 		BREAK
		CASE DLC_PACK_MP_BATTLE			RETURN "DLC_MP_BH" 			BREAK
		CASE DLC_PACK_MP_CHRISTMAS2018	RETURN "DLC_MP_ARENA" 		BREAK
		CASE DLC_PACK_MP_VINEWOOD		RETURN "DLC_MP_VWD" 		BREAK
		CASE DLC_PACK_MP_HEIST3			RETURN "DLC_MP_H3" 			BREAK
		CASE DLC_PACK_MP_SUM			RETURN "DLC_MP_SUM"			BREAK
		CASE DLC_PACK_MP_HEIST4			RETURN "DLC_MP_H4"			BREAK
		CASE DLC_PACK_MP_TUNER			RETURN "DLC_MP_TUNER"		BREAK
		CASE DLC_PACK_MP_FIXER			RETURN "DLC_MP_FIXER"		BREAK
		CASE DLC_PACK_MP_G9EC			RETURN "DLC_MP_G9EC"		BREAK
		CASE DLC_PACK_MP_SUM2			RETURN "DLC_MP_SUM2"		BREAK
		CASE DLC_PACK_MP_SUM2_G9EC		RETURN "DLC_MP_SUM2_G9EC"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL GET_DLC_PACK_NAME_FROM_HASH(INT iPackHash, TEXT_LABEL_63 &t, BOOL bDisplayName = TRUE)
	t = ""
	
	IF NOT bDisplayName
		SWITCH iPackHash
			CASE HASH("Female_freemode_beach") t = "Female_freemode_beach" BREAK
			CASE HASH("Male_freemode_beach") t = "Male_freemode_beach" BREAK
			CASE HASH("female_xmas") t = "female_xmas" BREAK
			CASE HASH("male_xmas") t = "male_xmas" BREAK
			CASE HASH("Male_freemode_valentines") t = "Male_freemode_valentines" BREAK
			CASE HASH("Female_freemode_valentines") t = "Female_freemode_valentines" BREAK
			CASE HASH("Female_freemode_business") t = "Female_freemode_business" BREAK
			CASE HASH("Male_freemode_business") t = "Male_freemode_business" BREAK
			CASE HASH("Female_freemode_business2") t = "Female_freemode_business2" BREAK
			CASE HASH("Male_freemode_business2") t = "Male_freemode_business2" BREAK
			CASE HASH("Female_freemode_hipster") t = "Female_freemode_hipster" BREAK
			CASE HASH("Male_freemode_hipster") t = "Male_freemode_hipster" BREAK
			CASE HASH("Female_freemode_independence") t = "Female_freemode_independence" BREAK
			CASE HASH("Male_freemode_independence") t = "Male_freemode_independence" BREAK
			CASE HASH("Female_freemode_pilot") t = "Female_freemode_pilot" BREAK
			CASE HASH("Male_freemode_pilot") t = "Male_freemode_pilot" BREAK
			CASE HASH("Female_freemode_mpLTS") t = "Female_freemode_mpLTS" BREAK
			CASE HASH("Male_freemode_mpLTS") t = "Male_freemode_mpLTS" BREAK
			CASE HASH("female_xmas2") t = "female_xmas2" BREAK
			CASE HASH("male_xmas2") t = "male_xmas2" BREAK
			CASE HASH("Male_heist") t = "Male_heist" BREAK
			CASE HASH("Female_heist") t = "Female_heist" BREAK
			CASE HASH("mp_f_luxe_01") t = "mp_f_luxe_01" BREAK
			CASE HASH("mp_m_luxe_01") t = "mp_m_luxe_01" BREAK
			CASE HASH("mp_f_luxe_02") t = "mp_f_luxe_02" BREAK
			CASE HASH("mp_m_luxe_02") t = "mp_m_luxe_02" BREAK
			CASE HASH("mp_m_htb_01") t = "mp_m_htb_01" BREAK
			CASE HASH("mp_f_htb_01") t = "mp_f_htb_01" BREAK
			CASE HASH("male_freemode_halloween") t = "male_freemode_halloween" BREAK
			CASE HASH("Female_freemode_halloween") t = "Female_freemode_halloween" BREAK
			CASE HASH("mp_m_lowrider_01") t = "mp_m_lowrider_01" BREAK
			CASE HASH("mp_f_lowrider_01") t = "mp_f_lowrider_01" BREAK
			CASE HASH("male_apt01") t = "male_apt01" BREAK
			CASE HASH("female_apt01") t = "female_apt01" BREAK
			CASE HASH("mp_m_xmas_03") t = "mp_m_xmas_03" BREAK
			CASE HASH("mp_f_xmas_03") t = "mp_f_xmas_03" BREAK
			CASE HASH("mp_f_January2016") t = "mp_f_January2016" BREAK
			CASE HASH("mp_m_January2016") t = "mp_m_January2016" BREAK
			CASE HASH("mp_m_Valentines_02") t = "mp_m_Valentines_02" BREAK
			CASE HASH("mp_f_Valentines_02") t = "mp_f_Valentines_02" BREAK
			CASE HASH("mp_f_lowrider_02") t = "mp_f_lowrider_02" BREAK
			CASE HASH("mp_m_lowrider_02") t = "mp_m_lowrider_02" BREAK
			CASE HASH("mp_f_executive_01") t = "mp_f_executive_01" BREAK
			CASE HASH("mp_m_executive_01") t = "mp_m_executive_01" BREAK
			CASE HASH("mp_f_stunt_01") t = "mp_f_stunt_01" BREAK
			CASE HASH("mp_m_stunt_01") t = "mp_m_stunt_01" BREAK
			CASE HASH("mp_f_bikerdlc_01") t = "mp_f_bikerdlc_01" BREAK
			CASE HASH("mp_m_bikerdlc_01") t = "mp_m_bikerdlc_01" BREAK
			CASE HASH("mp_f_importexport_01") t = "mp_f_importexport_01" BREAK
			CASE HASH("mp_m_importexport_01") t = "mp_m_importexport_01" BREAK
			CASE HASH("mp_f_gunrunning_01") t = "mp_f_gunrunning_01" BREAK
			CASE HASH("mp_m_gunrunning_01") t = "mp_m_gunrunning_01" BREAK
			CASE HASH("mp_f_airraces_01") t = "mp_f_airraces_01" BREAK
			CASE HASH("mp_m_airraces_01") t = "mp_m_airraces_01" BREAK
			CASE HASH("mp_f_smuggler_01") t = "mp_f_smuggler_01" BREAK
			CASE HASH("mp_m_smuggler_01") t = "mp_m_smuggler_01" BREAK
			CASE HASH("mp_m_christmas2017") t = "mp_m_christmas2017" BREAK
			CASE HASH("mp_f_christmas2017") t = "mp_f_christmas2017" BREAK
			CASE HASH("mp_m_assault") t = "mp_m_assault" BREAK
			CASE HASH("mp_f_assault") t = "mp_f_assault" BREAK
			CASE HASH("mp_m_battle") t = "mp_m_battle" BREAK
			CASE HASH("mp_f_battle") t = "mp_f_battle" BREAK
			CASE HASH("mp_m_christmas2018") t = "mp_m_christmas2018" BREAK
			CASE HASH("mp_f_christmas2018") t = "mp_f_christmas2018" BREAK
			CASE HASH("mp_m_vinewood") t = "mp_m_vinewood" BREAK
			CASE HASH("mp_f_vinewood") t = "mp_f_vinewood" BREAK
			CASE HASH("mp_m_Heist3") t = "mp_m_Heist3" BREAK
			CASE HASH("mp_f_Heist3") t = "mp_f_Heist3" BREAK
			CASE HASH("mp_m_sum") t = "mp_m_sum" BREAK
			CASE HASH("mp_f_sum") t = "mp_f_sum" BREAK
			CASE HASH("mp_m_heist4") t = "mp_m_heist4" BREAK
			CASE HASH("mp_f_heist4") t = "mp_f_heist4" BREAK
			CASE HASH("mp_m_tuner") t = "mp_m_tuner" BREAK
			CASE HASH("mp_f_tuner") t = "mp_f_tuner" BREAK
			CASE HASH("mp_m_security") t = "mp_m_security" BREAK
			CASE HASH("mp_f_security") t = "mp_f_security" BREAK
			CASE HASH("mp_m_g9ec") t = "mp_m_g9ec" BREAK
			CASE HASH("mp_f_g9ec") t = "mp_f_g9ec" BREAK
			CASE HASH("mp_m_sum2") t = "mp_m_sum2" BREAK
			CASE HASH("mp_f_sum2") t = "mp_f_sum2" BREAK
			CASE HASH("mp_m_sum2_g9ec") t = "mp_m_sum2_g9ec" BREAK
			CASE HASH("mp_f_sum2_g9ec") t = "mp_f_sum2_g9ec" BREAK
		ENDSWITCH
	ELSE
		SWITCH iPackHash
			CASE HASH("Female_freemode_beach") t = "Beach Bum" BREAK
			CASE HASH("Male_freemode_beach") t = "Beach Bum" BREAK
			CASE HASH("female_xmas") t = "Christmas" BREAK
			CASE HASH("male_xmas") t = "Christmas" BREAK
			CASE HASH("Male_freemode_valentines") t = "Valentines" BREAK
			CASE HASH("Female_freemode_valentines") t = "Valentines" BREAK
			CASE HASH("Female_freemode_business") t = "Business" BREAK
			CASE HASH("Male_freemode_business") t = "Business" BREAK
			CASE HASH("Female_freemode_business2") t = "High-Life" BREAK
			CASE HASH("Male_freemode_business2") t = "High-Life" BREAK
			CASE HASH("Female_freemode_hipster") t = "Hipster" BREAK
			CASE HASH("Male_freemode_hipster") t = "Hipster" BREAK
			CASE HASH("Female_freemode_independence") t = "Independence" BREAK
			CASE HASH("Male_freemode_independence") t = "Independence" BREAK
			CASE HASH("Female_freemode_pilot") t = "Flight School" BREAK
			CASE HASH("Male_freemode_pilot") t = "Flight School" BREAK
			CASE HASH("Female_freemode_mpLTS") t = "LTS Creator" BREAK
			CASE HASH("Male_freemode_mpLTS") t = "LTS Creator" BREAK
			CASE HASH("female_xmas2") t = "Christmas 2014" BREAK
			CASE HASH("male_xmas2") t = "Christmas 2014" BREAK
			CASE HASH("Male_heist") t = "Heists" BREAK
			CASE HASH("Female_heist") t = "Heists" BREAK
			CASE HASH("mp_f_luxe_01") t = "Luxe 1" BREAK
			CASE HASH("mp_m_luxe_01") t = "Luxe 1" BREAK
			CASE HASH("mp_f_luxe_02") t = "Luxe 2" BREAK
			CASE HASH("mp_m_luxe_02") t = "Luxe 2" BREAK
			CASE HASH("mp_m_htb_01") t = "Replay Editor" BREAK
			CASE HASH("mp_f_htb_01") t = "Replay Editor" BREAK
			CASE HASH("male_freemode_halloween") t = "Halloween" BREAK
			CASE HASH("Female_freemode_halloween") t = "Halloween" BREAK
			CASE HASH("mp_m_lowrider_01") t = "Lowriders" BREAK
			CASE HASH("mp_f_lowrider_01") t = "Lowriders" BREAK
			CASE HASH("male_apt01") t = "Apartment" BREAK
			CASE HASH("female_apt01") t = "Apartment" BREAK
			CASE HASH("mp_m_xmas_03") t = "Christmas 2015" BREAK
			CASE HASH("mp_f_xmas_03") t = "Christmas 2015" BREAK
			CASE HASH("mp_f_January2016") t = "January" BREAK
			CASE HASH("mp_m_January2016") t = "January" BREAK
			CASE HASH("mp_m_Valentines_02") t = "Valentines 2016" BREAK
			CASE HASH("mp_f_Valentines_02") t = "Valentines 2016" BREAK
			CASE HASH("mp_f_lowrider_02") t = "Lowriders 2" BREAK
			CASE HASH("mp_m_lowrider_02") t = "Lowriders 2" BREAK
			CASE HASH("mp_f_executive_01") t = "Executive" BREAK
			CASE HASH("mp_m_executive_01") t = "Executive" BREAK
			CASE HASH("mp_f_stunt_01") t = "Stunt" BREAK
			CASE HASH("mp_m_stunt_01") t = "Stunt" BREAK
			CASE HASH("mp_f_bikerdlc_01") t = "Bikers" BREAK
			CASE HASH("mp_m_bikerdlc_01") t = "Bikers" BREAK
			CASE HASH("mp_f_importexport_01") t = "Import/Export" BREAK
			CASE HASH("mp_m_importexport_01") t = "Import/Export" BREAK
			CASE HASH("mp_f_gunrunning_01") t = "Gunrunning" BREAK
			CASE HASH("mp_m_gunrunning_01") t = "Gunrunning" BREAK
			CASE HASH("mp_f_airraces_01") t = "Air Races" BREAK
			CASE HASH("mp_m_airraces_01") t = "Air Races" BREAK
			CASE HASH("mp_f_smuggler_01") t = "Smuggler" BREAK
			CASE HASH("mp_m_smuggler_01") t = "Smuggler" BREAK
			CASE HASH("mp_m_christmas2017") t = "Christmas 2017" BREAK
			CASE HASH("mp_f_christmas2017") t = "Christmas 2017" BREAK
			CASE HASH("mp_m_assault") t = "SSASSS" BREAK
			CASE HASH("mp_f_assault") t = "SSASSS" BREAK
			CASE HASH("mp_m_battle") t = "Business Battles" BREAK
			CASE HASH("mp_f_battle") t = "Business Battles" BREAK
			CASE HASH("mp_m_christmas2018") t = "Christmas 2018" BREAK
			CASE HASH("mp_f_christmas2018") t = "Christmas 2018" BREAK
			CASE HASH("mp_m_vinewood") t = "Vinewood" BREAK
			CASE HASH("mp_f_vinewood") t = "Vinewood" BREAK
			CASE HASH("mp_m_Heist3") t = "Casino Heist" BREAK
			CASE HASH("mp_f_Heist3") t = "Casino Heist" BREAK
			CASE HASH("mp_m_sum") t = "Summer 2020" BREAK
			CASE HASH("mp_f_sum") t = "Summer 2020" BREAK
			CASE HASH("mp_m_heist4") t = "Island Heist" BREAK
			CASE HASH("mp_f_heist4") t = "Island Heist" BREAK
			CASE HASH("mp_m_tuner") t = "Tuner" BREAK
			CASE HASH("mp_f_tuner") t = "Tuner" BREAK
			CASE HASH("mp_m_security") t = "Fixer" BREAK
			CASE HASH("mp_f_security") t = "Fixer" BREAK
			CASE HASH("mp_m_g9ec") t = "Gen9EC" BREAK
			CASE HASH("mp_f_g9ec") t = "Gen9EC" BREAK
			CASE HASH("mp_m_sum2") t = "Summer 2022" BREAK
			CASE HASH("mp_f_sum2") t = "Summer 2022" BREAK
			CASE HASH("mp_m_sum2_g9ec") t = "Summer 2022 G9EC" BREAK
			CASE HASH("mp_f_sum2_g9ec") t = "Summer 2022 G9EC" BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH iPackHash
		CASE HASH("mpAirraces_overlays") 	t = "mpAirraces_overlays" BREAK
		CASE HASH("mpArmy_overlays") 	t = "mpArmy_overlays" BREAK
		CASE HASH("mpBattle_overlays") 	t = "mpBattle_overlays" BREAK
		CASE HASH("mpBeach_overlays") 	t = "mpBeach_overlays" BREAK
		CASE HASH("mpBiker_overlays") 	t = "mpBiker_overlays" BREAK
		CASE HASH("mpBusiness_overlays") 	t = "mpBusiness_overlays" BREAK
		CASE HASH("mpChristmas2_overlays") 	t = "mpChristmas2_overlays" BREAK
		CASE HASH("mpChristmas2017_overlays") 	t = "mpChristmas2017_overlays" BREAK
		CASE HASH("mpChristmas2018_overlays") 	t = "mpChristmas2018_overlays" BREAK
		CASE HASH("mpExecutive_overlays") 	t = "mpExecutive_overlays" BREAK
		CASE HASH("mpGunrunning_overlays") 	t = "mpGunrunning_overlays" BREAK
		CASE HASH("mpHeist_overlays") 	t = "mpHeist_overlays" BREAK
		CASE HASH("mpHeist3_overlays") 	t = "mpHeist3_overlays" BREAK
		CASE HASH("mpHeist4_overlays") 	t = "mpHeist4_overlays" BREAK
		CASE HASH("mpHipster_overlays") 	t = "mpHipster_overlays" BREAK
		CASE HASH("mpImportExport_overlays") 	t = "mpImportExport_overlays" BREAK
		CASE HASH("mpIndependance_overlays") 	t = "mpIndependance_overlays" BREAK
		CASE HASH("mpLowrider_overlays") 	t = "mpLowrider_overlays" BREAK
		CASE HASH("mpLowrider2_overlays") 	t = "mpLowrider2_overlays" BREAK
		CASE HASH("mpLTS_overlays") 	t = "mpLTS_overlays" BREAK
		CASE HASH("mpLuxe_overlays") 	t = "mpLuxe_overlays" BREAK
		CASE HASH("mpLuxe2_overlays") 	t = "mpLuxe2_overlays" BREAK
		CASE HASH("mpPilot_overlays") 	t = "mpPilot_overlays" BREAK
		CASE HASH("mpSecurity_overlays") 	t = "mpSecurity_overlays" BREAK
		CASE HASH("mpSmuggler_overlays") 	t = "mpSmuggler_overlays" BREAK
		CASE HASH("mpSports_overlays") 	t = "mpSports_overlays" BREAK
		CASE HASH("mpStunt_overlays") 	t = "mpStunt_overlays" BREAK
		CASE HASH("mpSum_overlays") 	t = "mpSum_overlays" BREAK
		CASE HASH("mpTuner_overlays") 	t = "mpTuner_overlays" BREAK
		CASE HASH("mpValentines_overlays") 	t = "mpValentines_overlays" BREAK
		CASE HASH("mpVinewood_overlays") 	t = "mpVinewood_overlays" BREAK
		CASE HASH("mpxmas_604490_overlays") 	t = "mpxmas_604490_overlays" BREAK
		CASE HASH("mpSum2_overlays") 	t = "mpSum2_overlays" BREAK
		CASE HASH("mpDummy") 	t = "mpDummy" BREAK
	ENDSWITCH
	
	RETURN (NOT IS_STRING_NULL_OR_EMPTY(t))
ENDFUNC

FUNC STRING GET_DLC_CONTENT_LOCK_NAME_FROM_HASH(INT iLockHash)
	SWITCH iLockHash
		CASE HASH("CU_AIRRACE_CLOTHES") 	RETURN "CU_AIRRACE_CLOTHES" BREAK
		CASE HASH("CU_CNC_CLOTHES") 		RETURN "CU_CNC_CLOTHES" BREAK
		CASE HASH("CU_ARC1_CLOTHES") 		RETURN "CU_ARC1_CLOTHES" BREAK
		CASE HASH("CU_ARC1_MASKS") 			RETURN "CU_ARC1_MASKS" BREAK
		CASE HASH("CU_BIKER_CLOTHES") 		RETURN "CU_BIKER_CLOTHES" BREAK
		CASE HASH("CU_GANGOPS_CLOTHES") 	RETURN "CU_GANGOPS_CLOTHES" BREAK
		CASE HASH("CU_XMAS5_CLOTHES") 		RETURN "CU_XMAS5_CLOTHES" BREAK
		CASE HASH("CU_XMAS5_MASKS") 		RETURN "CU_XMAS5_MASKS" BREAK
		CASE HASH("CU_XMAS6_CLOTHES") 		RETURN "CU_XMAS6_CLOTHES" BREAK
		CASE HASH("CU_ARENA_CLOTHES") 		RETURN "CU_ARENA_CLOTHES" BREAK
		CASE HASH("CU_EXE_CLOTHES") 		RETURN "CU_EXE_CLOTHES" BREAK
		CASE HASH("CU_EXE_TATTOOS") 		RETURN "CU_EXE_TATTOOS" BREAK
		CASE HASH("CU_GEN9EC_CLOTHES") 		RETURN "CU_GEN9EC_CLOTHES" BREAK
		CASE HASH("CU_GUNRUN_CLOTHES") 		RETURN "CU_GUNRUN_CLOTHES" BREAK
		CASE HASH("CU_HEIST3_CLOTHES") 		RETURN "CU_HEIST3_CLOTHES" BREAK
		CASE HASH("CU_HEIST3_MASKS") 		RETURN "CU_HEIST3_MASKS" BREAK
		CASE HASH("CU_HEIST4_CLOTHES") 		RETURN "CU_HEIST4_CLOTHES" BREAK
		CASE HASH("CU_HEIST4_MASKS") 		RETURN "CU_HEIST4_MASKS" BREAK
		CASE HASH("CU_IMPEXP_CLOTHES") 		RETURN "CU_IMPEXP_CLOTHES" BREAK
		CASE HASH("CU_XMAS4_CLOTHES") 		RETURN "CU_XMAS4_CLOTHES" BREAK
		CASE HASH("CU_XMAS4_MASKS") 		RETURN "CU_XMAS4_MASKS" BREAK
		CASE HASH("CU_FIXER_CLOTHES") 		RETURN "CU_FIXER_CLOTHES" BREAK
		CASE HASH("CU_FIXER_MASKS") 		RETURN "CU_FIXER_MASKS" BREAK
		CASE HASH("CU_SMUG_CLOTHES") 		RETURN "CU_SMUG_CLOTHES" BREAK
		CASE HASH("CU_STUNT_CLOTHES") 		RETURN "CU_STUNT_CLOTHES" BREAK
		CASE HASH("CU_SUM_CLOTHES") 		RETURN "CU_SUM_CLOTHES" BREAK
		CASE HASH("CU_SUM_MASKS") 			RETURN "CU_SUM_MASKS" BREAK
		CASE HASH("CU_SUM2_CLOTHES") 		RETURN "CU_SUM2_CLOTHES" BREAK
		CASE HASH("CU_SUM2_MASKS") 			RETURN "CU_SUM2_MASKS" BREAK
		CASE HASH("CU_TUNER_CLOTHES") 		RETURN "CU_TUNER_CLOTHES" BREAK
		CASE HASH("CU_TUNER_MASKS") 		RETURN "CU_TUNER_MASKS" BREAK
		CASE HASH("CU_VINEWOOD_CLOTHES") 	RETURN "CU_VINEWOOD_CLOTHES" BREAK
		CASE HASH("CU_VINEWOOD_MASKS") 		RETURN "CU_VINEWOOD_MASKS" BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC GET_PED_SAVE_DATA_FROM_ATTRIBUTE_1(INT iAttributeHash, INT iAttribute)
	SWITCH iAttributeHash
		CASE HASH("model") g_DebugPedSaveData.ePedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute))) BREAK
		
		CASE HASH("CompDraw0") g_DebugPedSaveData.iCompDraw[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw1") g_DebugPedSaveData.iCompDraw[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw2") g_DebugPedSaveData.iCompDraw[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw3") g_DebugPedSaveData.iCompDraw[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw4") g_DebugPedSaveData.iCompDraw[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw5") g_DebugPedSaveData.iCompDraw[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw6") g_DebugPedSaveData.iCompDraw[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw7") g_DebugPedSaveData.iCompDraw[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw8") g_DebugPedSaveData.iCompDraw[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw9") g_DebugPedSaveData.iCompDraw[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw10") g_DebugPedSaveData.iCompDraw[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompDraw11") g_DebugPedSaveData.iCompDraw[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("CompTex0") g_DebugPedSaveData.iCompTex[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex1") g_DebugPedSaveData.iCompTex[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex2") g_DebugPedSaveData.iCompTex[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex3") g_DebugPedSaveData.iCompTex[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex4") g_DebugPedSaveData.iCompTex[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex5") g_DebugPedSaveData.iCompTex[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex6") g_DebugPedSaveData.iCompTex[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex7") g_DebugPedSaveData.iCompTex[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex8") g_DebugPedSaveData.iCompTex[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex9") g_DebugPedSaveData.iCompTex[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex10") g_DebugPedSaveData.iCompTex[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("CompTex11") g_DebugPedSaveData.iCompTex[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("PropDraw0") g_DebugPedSaveData.iPropDraw[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw1") g_DebugPedSaveData.iPropDraw[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw2") g_DebugPedSaveData.iPropDraw[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw3") g_DebugPedSaveData.iPropDraw[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw4") g_DebugPedSaveData.iPropDraw[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw5") g_DebugPedSaveData.iPropDraw[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw6") g_DebugPedSaveData.iPropDraw[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw7") g_DebugPedSaveData.iPropDraw[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropDraw8") g_DebugPedSaveData.iPropDraw[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("PropTex0") g_DebugPedSaveData.iPropTex[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex1") g_DebugPedSaveData.iPropTex[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex2") g_DebugPedSaveData.iPropTex[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex3") g_DebugPedSaveData.iPropTex[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex4") g_DebugPedSaveData.iPropTex[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex5") g_DebugPedSaveData.iPropTex[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex6") g_DebugPedSaveData.iPropTex[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex7") g_DebugPedSaveData.iPropTex[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("PropTex8") g_DebugPedSaveData.iPropTex[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("m_head0") 				g_DebugPedSaveData.m_head0 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_head1") 				g_DebugPedSaveData.m_head1 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_head2") 				g_DebugPedSaveData.m_head2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_tex0") 				g_DebugPedSaveData.m_tex0 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_tex1") 				g_DebugPedSaveData.m_tex1 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_tex2") 				g_DebugPedSaveData.m_tex2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_headBlend") 			g_DebugPedSaveData.m_headBlend = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_texBlend") 			g_DebugPedSaveData.m_texBlend = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_varBlend") 			g_DebugPedSaveData.m_varBlend = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("m_isParent") 			g_DebugPedSaveData.m_isParent = GET_BOOL_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("iOverlayValue0") 		g_DebugPedSaveData.iOverlayValue[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend0") 		g_DebugPedSaveData.fOverlayBlend[0] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved0") 			g_DebugPedSaveData.iColourSaved[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved20") 		g_DebugPedSaveData.iColourSaved2[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour0") 				g_DebugPedSaveData.iColour[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType0") 				g_DebugPedSaveData.rtType[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue1") 		g_DebugPedSaveData.iOverlayValue[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend1") 		g_DebugPedSaveData.fOverlayBlend[1] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved1") 			g_DebugPedSaveData.iColourSaved[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved21") 		g_DebugPedSaveData.iColourSaved2[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour1") 				g_DebugPedSaveData.iColour[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType1") 				g_DebugPedSaveData.rtType[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue2") 		g_DebugPedSaveData.iOverlayValue[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend2") 		g_DebugPedSaveData.fOverlayBlend[2] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved2") 			g_DebugPedSaveData.iColourSaved[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved22") 		g_DebugPedSaveData.iColourSaved2[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour2") 				g_DebugPedSaveData.iColour[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType2") 				g_DebugPedSaveData.rtType[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue3") 		g_DebugPedSaveData.iOverlayValue[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend3") 		g_DebugPedSaveData.fOverlayBlend[3] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved3") 			g_DebugPedSaveData.iColourSaved[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved23") 		g_DebugPedSaveData.iColourSaved2[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour3") 				g_DebugPedSaveData.iColour[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType3") 				g_DebugPedSaveData.rtType[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue4") 		g_DebugPedSaveData.iOverlayValue[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend4") 		g_DebugPedSaveData.fOverlayBlend[4] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved4") 			g_DebugPedSaveData.iColourSaved[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved24") 		g_DebugPedSaveData.iColourSaved2[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour4") 				g_DebugPedSaveData.iColour[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType4") 				g_DebugPedSaveData.rtType[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue5") 		g_DebugPedSaveData.iOverlayValue[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend5") 		g_DebugPedSaveData.fOverlayBlend[5] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved5") 			g_DebugPedSaveData.iColourSaved[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved25") 		g_DebugPedSaveData.iColourSaved2[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour5") 				g_DebugPedSaveData.iColour[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType5") 				g_DebugPedSaveData.rtType[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue6") 		g_DebugPedSaveData.iOverlayValue[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend6") 		g_DebugPedSaveData.fOverlayBlend[6] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved6") 			g_DebugPedSaveData.iColourSaved[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved26") 		g_DebugPedSaveData.iColourSaved2[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour6") 				g_DebugPedSaveData.iColour[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType6") 				g_DebugPedSaveData.rtType[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue7") 		g_DebugPedSaveData.iOverlayValue[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend7") 		g_DebugPedSaveData.fOverlayBlend[7] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved7") 			g_DebugPedSaveData.iColourSaved[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved27") 		g_DebugPedSaveData.iColourSaved2[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour7") 				g_DebugPedSaveData.iColour[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType7") 				g_DebugPedSaveData.rtType[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue8") 		g_DebugPedSaveData.iOverlayValue[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend8") 		g_DebugPedSaveData.fOverlayBlend[8] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved8") 			g_DebugPedSaveData.iColourSaved[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved28") 		g_DebugPedSaveData.iColourSaved2[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour8") 				g_DebugPedSaveData.iColour[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType8") 				g_DebugPedSaveData.rtType[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue9") 		g_DebugPedSaveData.iOverlayValue[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend9") 		g_DebugPedSaveData.fOverlayBlend[9] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved9") 			g_DebugPedSaveData.iColourSaved[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved29") 		g_DebugPedSaveData.iColourSaved2[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour9") 				g_DebugPedSaveData.iColour[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType9") 				g_DebugPedSaveData.rtType[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue10") 		g_DebugPedSaveData.iOverlayValue[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend10") 		g_DebugPedSaveData.fOverlayBlend[10] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved10") 		g_DebugPedSaveData.iColourSaved[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved210") 		g_DebugPedSaveData.iColourSaved2[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour10") 				g_DebugPedSaveData.iColour[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType10") 				g_DebugPedSaveData.rtType[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue11") 		g_DebugPedSaveData.iOverlayValue[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend11") 		g_DebugPedSaveData.fOverlayBlend[11] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved11") 		g_DebugPedSaveData.iColourSaved[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved211") 		g_DebugPedSaveData.iColourSaved2[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour11") 				g_DebugPedSaveData.iColour[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType11") 				g_DebugPedSaveData.rtType[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iOverlayValue12") 		g_DebugPedSaveData.iOverlayValue[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fOverlayBlend12") 		g_DebugPedSaveData.fOverlayBlend[12] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved12") 		g_DebugPedSaveData.iColourSaved[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColourSaved212") 		g_DebugPedSaveData.iColourSaved2[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iColour12") 				g_DebugPedSaveData.iColour[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("rtType12") 				g_DebugPedSaveData.rtType[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
	ENDSWITCH
	SWITCH iAttributeHash
		CASE HASH("fMorphBlend0") 			g_DebugPedSaveData.fMorphBlend[0] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend1") 			g_DebugPedSaveData.fMorphBlend[1] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend2") 			g_DebugPedSaveData.fMorphBlend[2] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend3") 			g_DebugPedSaveData.fMorphBlend[3] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend4") 			g_DebugPedSaveData.fMorphBlend[4] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend5") 			g_DebugPedSaveData.fMorphBlend[5] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend6") 			g_DebugPedSaveData.fMorphBlend[6] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend7") 			g_DebugPedSaveData.fMorphBlend[7] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend8") 			g_DebugPedSaveData.fMorphBlend[8] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend9") 			g_DebugPedSaveData.fMorphBlend[9] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend10") 			g_DebugPedSaveData.fMorphBlend[10] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend11") 			g_DebugPedSaveData.fMorphBlend[11] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend12") 			g_DebugPedSaveData.fMorphBlend[12] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend13") 			g_DebugPedSaveData.fMorphBlend[13] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend14") 			g_DebugPedSaveData.fMorphBlend[14] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend15") 			g_DebugPedSaveData.fMorphBlend[15] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend16") 			g_DebugPedSaveData.fMorphBlend[16] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend17") 			g_DebugPedSaveData.fMorphBlend[17] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend18") 			g_DebugPedSaveData.fMorphBlend[18] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("fMorphBlend19") 			g_DebugPedSaveData.fMorphBlend[19] = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("fEyes") 					g_DebugPedSaveData.fEyes = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		
		CASE HASH("iHairTint1") 			g_DebugPedSaveData.iHairTint1 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
		CASE HASH("iHairTint2") 			g_DebugPedSaveData.iHairTint2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
	ENDSWITCH
ENDPROC
PROC GET_PED_SAVE_DATA_FROM_ATTRIBUTE_2(INT iAttributeHash, INT iAttribute)
	INT iValue = -1
	SWITCH iAttributeHash
		CASE HASH("tattoo0")
		CASE HASH("tattoo1")
		CASE HASH("tattoo2")
		CASE HASH("tattoo3")
		CASE HASH("tattoo4")
		CASE HASH("tattoo5")
		CASE HASH("tattoo6")
		CASE HASH("tattoo7")
		CASE HASH("tattoo8")
		CASE HASH("tattoo9")
		CASE HASH("tattoo10")
		CASE HASH("tattoo11")
		CASE HASH("tattoo12")
		CASE HASH("tattoo13")
		CASE HASH("tattoo14")
		CASE HASH("tattoo15")
		CASE HASH("tattoo16")
		CASE HASH("tattoo17")
		CASE HASH("tattoo18")
		CASE HASH("tattoo19")
		CASE HASH("tattoo20")
		CASE HASH("tattoo21")
		CASE HASH("tattoo22")
		CASE HASH("tattoo23")
		CASE HASH("tattoo24")
		CASE HASH("tattoo25")
		CASE HASH("tattoo26")
		CASE HASH("tattoo27")
		CASE HASH("tattoo28")
		CASE HASH("tattoo29")
		CASE HASH("tattoo30")
		CASE HASH("tattoo31")
		CASE HASH("tattoo32")
		CASE HASH("tattoo33")
		CASE HASH("tattoo34")
		CASE HASH("tattoo35")
		CASE HASH("tattoo36")
		CASE HASH("tattoo37")
		CASE HASH("tattoo38")
		CASE HASH("tattoo39")
		CASE HASH("tattoo40")
		CASE HASH("tattoo41")
		CASE HASH("tattoo42")
		CASE HASH("tattoo43")
		CASE HASH("tattoo44")
		CASE HASH("tattoo45")
		CASE HASH("tattoo46")
		CASE HASH("tattoo47")
		CASE HASH("tattoo48")
		CASE HASH("tattoo49")
			iValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
	ENDSWITCH
	
	IF iValue != -1
		SET_BIT(g_DebugPedSaveData.iTattoos[iValue/32], iValue%32)
	ENDIF
ENDPROC
PROC GET_PED_SAVE_DATA_FROM_ATTRIBUTE_3(INT iAttributeHash, INT iAttribute)
	INT iValue = -1
	SWITCH iAttributeHash
		CASE HASH("dlcTattoo0")
		CASE HASH("dlcTattoo1")
		CASE HASH("dlcTattoo2")
		CASE HASH("dlcTattoo3")
		CASE HASH("dlcTattoo4")
		CASE HASH("dlcTattoo5")
		CASE HASH("dlcTattoo6")
		CASE HASH("dlcTattoo7")
		CASE HASH("dlcTattoo8")
		CASE HASH("dlcTattoo9")
		CASE HASH("dlcTattoo10")
		CASE HASH("dlcTattoo11")
		CASE HASH("dlcTattoo12")
		CASE HASH("dlcTattoo13")
		CASE HASH("dlcTattoo14")
		CASE HASH("dlcTattoo15")
		CASE HASH("dlcTattoo16")
		CASE HASH("dlcTattoo17")
		CASE HASH("dlcTattoo18")
		CASE HASH("dlcTattoo19")
		CASE HASH("dlcTattoo20")
		CASE HASH("dlcTattoo21")
		CASE HASH("dlcTattoo22")
		CASE HASH("dlcTattoo23")
		CASE HASH("dlcTattoo24")
		CASE HASH("dlcTattoo25")
		CASE HASH("dlcTattoo26")
		CASE HASH("dlcTattoo27")
		CASE HASH("dlcTattoo28")
		CASE HASH("dlcTattoo29")
		CASE HASH("dlcTattoo30")
		CASE HASH("dlcTattoo31")
		CASE HASH("dlcTattoo32")
		CASE HASH("dlcTattoo33")
		CASE HASH("dlcTattoo34")
		CASE HASH("dlcTattoo35")
		CASE HASH("dlcTattoo36")
		CASE HASH("dlcTattoo37")
		CASE HASH("dlcTattoo38")
		CASE HASH("dlcTattoo39")
		CASE HASH("dlcTattoo40")
		CASE HASH("dlcTattoo41")
		CASE HASH("dlcTattoo42")
		CASE HASH("dlcTattoo43")
		CASE HASH("dlcTattoo44")
		CASE HASH("dlcTattoo45")
		CASE HASH("dlcTattoo46")
		CASE HASH("dlcTattoo47")
		CASE HASH("dlcTattoo48")
		CASE HASH("dlcTattoo49")
			iValue = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
	ENDSWITCH
	
	IF iValue != -1
		SET_BIT(g_DebugPedSaveData.iDLCTattoos[iValue/32], iValue%32)
	ENDIF
ENDPROC



PROC PROCESS_PED_SAVE_DEBUG(INT iPedSave)
			
	TEXT_LABEL_31 tlFilename
	TEXT_LABEL_63 tlPathname = "X:/gta5/titleupdate/dev_ng/DebugPedData/"
	
	INT iComp
	INT iProp
	scrPedHeadBlendData targetBlendData
	INT iOverlay
	HEAD_OVERLAY_SLOT overlaySlot
	STATS_PACKED overlayStat
	MP_FLOAT_STATS overlayStatBlend
	MP_INT_STATS overlayColourVal
	MP_INT_STATS overlaySecColourVal
	INT iOverlayValue
	FLOAT fOverlayBlend
	INT iColour
	INT iColourSaved
	INT iColourSaved2
	RAMP_TYPE rtType
	INT i
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	
	INT iDLCIndex
	INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
	sTattooShopItemValues sDLCTattooData
	TATTOO_NAME_ENUM eDLCTattoo
	
	INT iTatCount
	
	INT iMicroMorph
	MICRO_MORPH_TYPE microMorph
	MP_FLOAT_STATS microMorphStat
	FLOAT fMorphBlend

	tlFilename = "PedSave_"
	tlFilename += (iPedSave+1)
	tlFilename += ".xml"
	
	IF iPedSave = COUNT_OF(b_debug_ped_save)-1
		tlFilename = GET_CONTENTS_OF_TEXT_WIDGET(twDebugPedNamed)
		tlFilename += ".xml"
	ENDIF
	
	CLEAR_NAMED_DEBUG_FILE(tlPathname, tlFilename)
	
	OPEN_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<DebugPedData ", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		// PED MODEL
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- PLAYER MODEL -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	model=\"MP_M_FREEMODE_01\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	model=\"MP_F_FREEMODE_01\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ELSE
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	model=\"0\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDIF
		
		// PED COMPONENTS - DRAWABLE
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- PED COMPONENTS - DRAWABLE -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT NUM_PED_COMPONENTS iComp
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	CompDraw", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(iComp, tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp)), tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		// PED COMPONENTS - TEXTURE
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- PED COMPONENTS - TEXTURE -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT NUM_PED_COMPONENTS iComp
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	CompTex", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(iComp, tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp)), tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		// PED PROPS - INDEX
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- PED PROPS - INDEX -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT 9 iProp
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	PropDraw", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(iProp, tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp)), tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		// PED PROPS - TEXTURE
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- PED PROPS - TEXTURE -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT 9 iProp
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	PropTex", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(iProp, tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
			SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp)), tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		
		// HEAD BELND
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- HEAD BLEND -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		GET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), targetBlendData)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_head0=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_head0, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_head1=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_head1, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_head2=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_head2, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_tex0=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_tex0, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_tex1=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_tex1, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_tex2=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(targetBlendData.m_tex2, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_headBlend=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(targetBlendData.m_headBlend, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_texBlend=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(targetBlendData.m_texBlend, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_varBlend=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(targetBlendData.m_varBlend, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	m_isParent=\"", tlPathname, tlFilename)SAVE_BOOL_TO_NAMED_DEBUG_FILE(targetBlendData.m_isParent, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		
		// MICRO MORPHS
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- MICRO MORPHS -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH iMicroMorph
			microMorph = INT_TO_ENUM(MICRO_MORPH_TYPE, iMicroMorph)
			microMorphStat = GET_CHARACTER_FEATURE_BLEND_STAT(microMorph)
			
			fMorphBlend = GET_MP_FLOAT_CHARACTER_STAT(microMorphStat)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fMorphBlend", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iMicroMorph, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fMorphBlend, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		
		
		// OVERLAYS
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- OVERLAYS -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlay
			overlaySlot = GET_CHARACTER_CREATOR_OVERLAY_FOR_INDEX(iOverlay)
			overlayStat = GET_CHARACTER_OVERLAY_STAT(overlaySlot)
			overlayStatBlend = GET_CHARACTER_OVERLAY_BLEND_STAT(overlaySlot)
			
			// Overlay
			IF overlayStat != INT_TO_ENUM(STATS_PACKED, -1)
			AND overlayStatBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1)
				iOverlayValue = GET_PACKED_STAT_INT(overlayStat)
				fOverlayBlend = GET_MP_FLOAT_CHARACTER_STAT(overlayStatBlend)
			ELSE
				iOverlayValue = -1
				fOverlayBlend = -1
			ENDIF
			
			// Hair
			overlayColourVal = GET_CHARACTER_OVERLAY_COLOUR_STAT(overlaySlot)
			overlaySecColourVal = GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(overlaySlot)
			
			IF overlayColourVal != INT_TO_ENUM(MP_INT_STATS, -1)
				iColourSaved = GET_MP_INT_CHARACTER_STAT(overlayColourVal)
				iColourSaved2 = GET_MP_INT_CHARACTER_STAT(overlaySecColourVal)
				
				UNPACK_CHARACTER_OVERLAY_TINT_VALUES(iColourSaved, iColour, rtType)
			ELSE
				iColour = -1
				iColourSaved = -1
				iColourSaved2 = -1
				rtType = INT_TO_ENUM(RAMP_TYPE, -1)
			ENDIF
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	iOverlayValue", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlayValue, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	fOverlayBlend", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fOverlayBlend, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	iColourSaved", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iColourSaved, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	iColourSaved2", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iColourSaved2, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	iColour", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iColour, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	rtType", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(iOverlay, tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(rtType), tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		ENDREPEAT
		
		// EYES
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- EYES -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	fEyes=\"", tlPathname, tlFilename)SAVE_FLOAT_TO_NAMED_DEBUG_FILE(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20), tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		
		// HAIR TINT
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- HAIR TINT -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	iHairTint1=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT), tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("	iHairTint2=\"", tlPathname, tlFilename)SAVE_INT_TO_NAMED_DEBUG_FILE(GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT), tlPathname, tlFilename)SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		// TATTOOS
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		//SAVE_STRING_TO_NAMED_DEBUG_FILE("	<!-- TATTOOS -->", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
			eFaction = TATTOO_MP_FM
		ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
			eFaction = TATTOO_MP_FM_F
		ENDIF
		
		iTatCount = 0
		REPEAT MAX_NUMBER_OF_TATTOOS i
			IF IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i))
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	tattoo", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iTatCount, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(i, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				iTatCount++
			ENDIF
		ENDREPEAT
		
		// Apply the DLC tats.
		iTatCount = 0
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
					eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
					IF IS_MP_TATTOO_CURRENT(eDLCTattoo)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("	dlcTattoo", tlPathname, tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(iTatCount, tlPathname, tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
						SAVE_INT_TO_NAMED_DEBUG_FILE(iDLCIndex, tlPathname, tlFilename)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
						iTatCount++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		SAVE_STRING_TO_NAMED_DEBUG_FILE(" />", tlPathname, tlFilename)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
	CLOSE_DEBUG_FILE()
	
	b_debug_ped_save[iPedSave] = FALSE
ENDPROC

FUNC BOOL PROCESS_PED_LOAD_DEBUG(INT iPedSave, STRING sFilenameOverride = NULL)

	BOOL bRetSuccess = FALSE

	TEXT_LABEL_31 tlFilename
	TEXT_LABEL_63 tlPathname = "X:/gta5/titleupdate/dev_ng/DebugPedData/"
	
	INT i
	
	tlFilename = "PedSave_"
	tlFilename += (iPedSave+1)
	tlFilename += ".xml"
	
	IF iPedSave = COUNT_OF(b_debug_ped_set)-1
		tlFilename = GET_CONTENTS_OF_TEXT_WIDGET(twDebugPedNamed)
		tlFilename += ".xml"
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sFilenameOverride)
		tlFilename = sFilenameOverride
	ENDIF

	// Flush out the temp struct
	g_DebugPedSaveData.ePedModel = DUMMY_MODEL_FOR_SCRIPT
	
	REPEAT NUM_PED_COMPONENTS i
		g_DebugPedSaveData.iCompDraw[i] = 0
		g_DebugPedSaveData.iCompTex[i] = 0
	ENDREPEAT
	
	REPEAT 9 i
		g_DebugPedSaveData.iPropDraw[i] = -1
		g_DebugPedSaveData.iPropTex[i] = -1
	ENDREPEAT
	
	g_DebugPedSaveData.m_head0 = 0
	g_DebugPedSaveData.m_head1 = 0
	g_DebugPedSaveData.m_head2 = 0
	g_DebugPedSaveData.m_tex0 = 0
	g_DebugPedSaveData.m_tex1 = 0
	g_DebugPedSaveData.m_tex2 = 0
	g_DebugPedSaveData.m_headBlend = 0.0
	g_DebugPedSaveData.m_texBlend = 0.0
	g_DebugPedSaveData.m_varBlend = 0.0
	g_DebugPedSaveData.m_isParent = FALSE
	g_DebugPedSaveData.fEyes = 0.0
	g_DebugPedSaveData.iHairTint1 = 0
	g_DebugPedSaveData.iHairTint2 = 0
	
	REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS i
		g_DebugPedSaveData.iOverlayValue[i] = 0
		g_DebugPedSaveData.fOverlayBlend[i] = 0.0
		g_DebugPedSaveData.iColourSaved[i] = 0
		g_DebugPedSaveData.iColourSaved[i] = 0
		g_DebugPedSaveData.iColour[i] = 0
		g_DebugPedSaveData.rtType[i] = 0
	ENDREPEAT
	
	REPEAT MAX_NUMBER_OF_TATTOO_BITSETS i
		g_DebugPedSaveData.iTattoos[i] = 0
	ENDREPEAT
	
	REPEAT MAX_NUMBER_OF_DLC_TATTOO_BITSETS i
		g_DebugPedSaveData.iDLCTattoos[i] = 0
	ENDREPEAT
	
	REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH i
		g_DebugPedSaveData.fMorphBlend[i] = 0.0
	ENDREPEAT
	
	TEXT_LABEL_63 tlXMLFile
	tlXMLFile = tlPathname
	tlXMLFile += tlFilename
	
	// Check that the xml exists and load
	IF NOT LOAD_XML_FILE(tlXMLFile)
	OR GET_NUMBER_OF_XML_NODES() = 0
		// bad file
		DELETE_XML_FILE()
	ELSE
	
		INT iNumNodes = GET_NUMBER_OF_XML_NODES()
		INT iNumAttributes
		INT iAttribute
		TEXT_LABEL_63 txtNodeName
		INT iAttributeHash
		REPEAT iNumNodes i
			txtNodeName = GET_XML_NODE_NAME()
			SWITCH GET_HASH_KEY(txtNodeName)
				CASE HASH("DebugPedData")
					iNumAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
					REPEAT iNumAttributes iAttribute
						iAttributeHash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttribute))
						GET_PED_SAVE_DATA_FROM_ATTRIBUTE_1(iAttributeHash, iAttribute)
						GET_PED_SAVE_DATA_FROM_ATTRIBUTE_2(iAttributeHash, iAttribute)
						GET_PED_SAVE_DATA_FROM_ATTRIBUTE_3(iAttributeHash, iAttribute)
					ENDREPEAT
				BREAK
			ENDSWITCH
			
			GET_NEXT_XML_NODE()
		ENDREPEAT
		
		DELETE_XML_FILE()
		
		bRetSuccess = TRUE
	ENDIF
	
	RETURN bRetSuccess
ENDFUNC

PROC PROCESS_PED_SET_DEBUG(INT iPedSave)
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	INT i
	INT iOverlay
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	INT iDLCIndex
	INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
	TATTOO_NAME_ENUM eDLCTattoo
	HEAD_OVERLAY_SLOT overlaySlot
	
	IF g_DebugPedSaveData.ePedModel != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(g_DebugPedSaveData.ePedModel)
		IF HAS_MODEL_LOADED(g_DebugPedSaveData.ePedModel)
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) != g_DebugPedSaveData.ePedModel
				SET_PLAYER_MODEL(PLAYER_ID(), g_DebugPedSaveData.ePedModel)
				// Update stored model
				IF g_DebugPedSaveData.ePedModel = MP_M_FREEMODE_01
					SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0)
				ELIF g_DebugPedSaveData.ePedModel = MP_F_FREEMODE_01
					SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1)
				ENDIF
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(g_DebugPedSaveData.ePedModel)
				SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
				CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
				
				REPEAT NUM_PED_COMPONENTS i
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, i), g_DebugPedSaveData.iCompDraw[i], g_DebugPedSaveData.iCompTex[i])
				ENDREPEAT
				
				REPEAT 9 i
					IF g_DebugPedSaveData.iPropDraw[i] != -1
						SET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i), g_DebugPedSaveData.iPropDraw[i], g_DebugPedSaveData.iPropTex[i])
					ENDIF
				ENDREPEAT
				
				SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), g_DebugPedSaveData.m_head0, g_DebugPedSaveData.m_head1, g_DebugPedSaveData.m_head2, g_DebugPedSaveData.m_tex0, g_DebugPedSaveData.m_tex1, g_DebugPedSaveData.m_tex2, g_DebugPedSaveData.m_headBlend, g_DebugPedSaveData.m_texBlend, g_DebugPedSaveData.m_varBlend, g_DebugPedSaveData.m_isParent)
				
				SET_PED_HAIR_TINT(PLAYER_PED_ID(), g_DebugPedSaveData.iHairTint1, g_DebugPedSaveData.iHairTint2)
				
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH i
					SET_PED_MICRO_MORPH(PLAYER_PED_ID(), INT_TO_ENUM(MICRO_MORPH_TYPE, i), g_DebugPedSaveData.fMorphBlend[i])
				ENDREPEAT
				
				REPEAT MAX_NUMBER_OF_TATTOOS i
					SET_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i), IS_BIT_SET(g_DebugPedSaveData.iTattoos[i/32], i%32))
				ENDREPEAT
				
				REPEAT iDLCCount iDLCIndex
					eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
					IF (iDLCIndex/32) < COUNT_OF(g_DebugPedSaveData.iDLCTattoos)
						SET_MP_TATTOO_CURRENT(eDLCTattoo, IS_BIT_SET(g_DebugPedSaveData.iDLCTattoos[iDLCIndex/32], iDLCIndex%32))
					ELSE
						PRINTLN("PROCESS_PED_SET_DEBUG - Need to increase tattoo array to ", (iDLCIndex/32))
						SCRIPT_ASSERT("PROCESS_PED_SET_DEBUG - Need to increase tattoo array ")
					ENDIF
				ENDREPEAT
				
				UPDATE_TATOOS_MP(PLAYER_PED_ID())
				
				REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlay
					overlaySlot = GET_CHARACTER_CREATOR_OVERLAY_FOR_INDEX(iOverlay)
					IF g_DebugPedSaveData.iOverlayValue[iOverlay] != -1
						SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), overlaySlot, g_DebugPedSaveData.iOverlayValue[iOverlay], g_DebugPedSaveData.fOverlayBlend[iOverlay])
					ENDIF
					IF g_DebugPedSaveData.iColour[iOverlay] != -1
						SET_PED_HEAD_OVERLAY_TINT(PLAYER_PED_ID(), overlaySlot, INT_TO_ENUM(RAMP_TYPE, g_DebugPedSaveData.rtType[iOverlay]), g_DebugPedSaveData.iColour[iOverlay], g_DebugPedSaveData.iColourSaved2[iOverlay])
					ENDIF
				ENDREPEAT
				
				SET_HEAD_BLEND_EYE_COLOR(PLAYER_PED_ID(), ROUND(g_DebugPedSaveData.fEyes))
				
				// Update stored model
				IF g_DebugPedSaveData.ePedModel = MP_M_FREEMODE_01
					SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0)
				ELIF g_DebugPedSaveData.ePedModel = MP_F_FREEMODE_01
					SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1)
				ENDIF
				
				// Complete
				CLEAR_BIT(i_debug_process_ped_set, iPedSave)
				b_debug_ped_set[iPedSave] = FALSE
			ENDIF
			
		ENDIF
	ELSE
		SCRIPT_ASSERT("Unable to set outfit - Invalid model")
		CLEAR_BIT(i_debug_process_ped_set, iPedSave)
		b_debug_ped_set[iPedSave] = FALSE
	ENDIF
ENDPROC

PROC PROCESS_PED_CREATE_DEBUG(INT iPedSave)
	
	INT i
	INT iOverlay
	TATTOO_FACTION_ENUM eFaction
	INT iDLCIndex
	INT iDLCCount
	TATTOO_NAME_ENUM eDLCTattoo
	HEAD_OVERLAY_SLOT overlaySlot
	VECTOR vRot
	
	REQUEST_MODEL(g_DebugPedSaveData.ePedModel)
	IF HAS_MODEL_LOADED(g_DebugPedSaveData.ePedModel)
		IF NOT b_debug_ped_created[iPedSave]
			IF DOES_ENTITY_EXIST(pedDebugPedCreate[iPedSave])
				DELETE_PED(pedDebugPedCreate[iPedSave])
			ENDIF
			
			vRot = GET_FINAL_RENDERED_CAM_ROT()
			pedDebugPedCreate[iPedSave] = CREATE_PED(PEDTYPE_CIVFEMALE, g_DebugPedSaveData.ePedModel, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FINAL_RENDERED_CAM_COORD(), vRot.z, <<0.0, 2.0, 0.0>>), 0.0, FALSE, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDebugPedCreate[iPedSave], TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(g_DebugPedSaveData.ePedModel)
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedDebugPedCreate[iPedSave])
			CLEAR_ALL_PED_PROPS(pedDebugPedCreate[iPedSave])
			
			REPEAT NUM_PED_COMPONENTS i
				SET_PED_COMPONENT_VARIATION(pedDebugPedCreate[iPedSave], INT_TO_ENUM(PED_COMPONENT, i), g_DebugPedSaveData.iCompDraw[i], g_DebugPedSaveData.iCompTex[i])
			ENDREPEAT
			
			REPEAT 9 i
				IF g_DebugPedSaveData.iPropDraw[i] != -1
					SET_PED_PROP_INDEX(pedDebugPedCreate[iPedSave], INT_TO_ENUM(PED_PROP_POSITION, i), g_DebugPedSaveData.iPropDraw[i], g_DebugPedSaveData.iPropTex[i])
				ENDIF
			ENDREPEAT
			
			SET_PED_HEAD_BLEND_DATA(pedDebugPedCreate[iPedSave], g_DebugPedSaveData.m_head0, g_DebugPedSaveData.m_head1, g_DebugPedSaveData.m_head2, g_DebugPedSaveData.m_tex0, g_DebugPedSaveData.m_tex1, g_DebugPedSaveData.m_tex2, g_DebugPedSaveData.m_headBlend, g_DebugPedSaveData.m_texBlend, g_DebugPedSaveData.m_varBlend, g_DebugPedSaveData.m_isParent)
			
			SET_PED_HAIR_TINT(pedDebugPedCreate[iPedSave], g_DebugPedSaveData.iHairTint1, g_DebugPedSaveData.iHairTint2)
			
			REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH i
				SET_PED_MICRO_MORPH(pedDebugPedCreate[iPedSave], INT_TO_ENUM(MICRO_MORPH_TYPE, i), g_DebugPedSaveData.fMorphBlend[i])
			ENDREPEAT
			
			eFaction = GET_TATTOO_FACTION_FOR_PED(pedDebugPedCreate[iPedSave])
			iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
			
			REPEAT MAX_NUMBER_OF_TATTOOS i
				SET_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i), IS_BIT_SET(g_DebugPedSaveData.iTattoos[i/32], i%32))
			ENDREPEAT
			
			REPEAT iDLCCount iDLCIndex
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
				IF (iDLCIndex/32) < COUNT_OF(g_DebugPedSaveData.iDLCTattoos)
					SET_MP_TATTOO_CURRENT(eDLCTattoo, IS_BIT_SET(g_DebugPedSaveData.iDLCTattoos[iDLCIndex/32], iDLCIndex%32))
				ELSE
					PRINTLN("PROCESS_PED_CREATE_DEBUG - Need to increase tattoo array to ", (iDLCIndex/32))
					SCRIPT_ASSERT("PROCESS_PED_CREATE_DEBUG - Need to increase tattoo array ")
				ENDIF
			ENDREPEAT
			
			UPDATE_TATOOS_MP(pedDebugPedCreate[iPedSave])
			
			REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlay
				overlaySlot = GET_CHARACTER_CREATOR_OVERLAY_FOR_INDEX(iOverlay)
				IF g_DebugPedSaveData.iOverlayValue[iOverlay] != -1
					SET_PED_HEAD_OVERLAY(pedDebugPedCreate[iPedSave], overlaySlot, g_DebugPedSaveData.iOverlayValue[iOverlay], g_DebugPedSaveData.fOverlayBlend[iOverlay])
				ENDIF
				IF g_DebugPedSaveData.iColour[iOverlay] != -1
					SET_PED_HEAD_OVERLAY_TINT(pedDebugPedCreate[iPedSave], overlaySlot, INT_TO_ENUM(RAMP_TYPE, g_DebugPedSaveData.rtType[iOverlay]), g_DebugPedSaveData.iColour[iOverlay], g_DebugPedSaveData.iColourSaved2[iOverlay])
				ENDIF
			ENDREPEAT
			
			SET_HEAD_BLEND_EYE_COLOR(pedDebugPedCreate[iPedSave], ROUND(g_DebugPedSaveData.fEyes))
			
			b_debug_ped_created[iPedSave] = TRUE
			
		ELSE
			IF HAS_PED_HEAD_BLEND_FINISHED(pedDebugPedCreate[iPedSave])
				IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedDebugPedCreate[iPedSave])
					FINALIZE_HEAD_BLEND(pedDebugPedCreate[iPedSave])
					
					// Update stored model
					IF g_DebugPedSaveData.ePedModel = MP_M_FREEMODE_01
						SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0)
					ELIF g_DebugPedSaveData.ePedModel = MP_F_FREEMODE_01
						SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1)
					ENDIF
					
					// Complete
					CLEAR_BIT(i_debug_process_ped_create, iPedSave)
					b_debug_ped_create[iPedSave] = FALSE
					b_debug_ped_created[iPedSave] = FALSE
				ENDIf
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("Unable to set outfit - Invalid model")
		CLEAR_BIT(i_debug_process_ped_create, iPedSave)
		b_debug_ped_create[iPedSave] = FALSE
	ENDIF
ENDPROC

PROC PROCESS_PED_RANDOMISE_DEBUG()

	PED_VARIATION_STRUCT sPedClothes
	GET_PED_VARIATIONS(PLAYER_PED_ID(), sPedClothes)
	
	// WARNING: We don't have a nice way of randomising head blends and micro morphs
	// so decided to use the character creator data and functions.
	
	MPHUD_PLACEMENT_TOOLS sPlacementData
	MPHUD_GTA_ONLINE_CHARACTER_DATA sCharacterData
	
	INITIALISE_CHARACTER_CONTROLLER(sCharacterData, sPlacementData)
	GENERATE_INITIAL_CHARACTER_CREATOR_DATA(sCharacterData, sPlacementData)
	
	// Override some states so the randomise functions plays ball
	sCharacterData.iCharacterCreatorCurrentScreen = ciCHARACTER_CREATOR_COLUMN_MAIN_MENU
	g_Private_EnteredCharCreationFromAlterationPrompt = FALSE
	sPlacementData.ScreenPlace.iSelectedCharacter = 0
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_ISACTIVE, 1)
		sPlacementData.ScreenPlace.iSelectedCharacter = 1
	ENDIF
	IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
		sCharacterData.eActiveChild = CHARACTER_CREATOR_SON
	ELSE
		sCharacterData.eActiveChild = CHARACTER_CREATOR_DAUGHTER
	ENDIF
	
	WHILE g_sCharacterControllerData.eCharacterControllerPedCreationState != CHARACTER_CREATOR_PED_STATE_COMPLETE
		GENERATE_CHARACTER_CREATOR_PEDS(sCharacterData, sPlacementData)
		WAIT(0)
	ENDWHILE
	
	PROCESS_CHARACTER_CREATOR_RANDOMIZE(sCharacterData)
	
	CLONE_PED_TO_TARGET(sCharacterData.characterCreatorPeds[ENUM_TO_INT(sCharacterData.eActiveChild)], PLAYER_PED_ID())
	
	WAIT(0)
	
	DESTROY_ALL_CHARACTER_CREATOR_PEDS(sCharacterData)
	RESET_CHARACTER_CONTROLLER_DATA(sCharacterData)
	
	// Keep the clothes we had
	INT i
	REPEAT NUM_PED_COMPONENTS i
		IF i != ENUM_TO_INT(PED_COMP_HEAD)
		AND i != ENUM_TO_INT(PED_COMP_HAIR)
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, i), sPedClothes.iDrawableVariation[i], sPedClothes.iTextureVariation[i])
		ENDIF
	ENDREPEAT
	
	REPEAT 9 i
		IF sPedClothes.iPropIndex[i] = -1
			CLEAR_PED_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i))
		ELSE
			SET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i), sPedClothes.iPropIndex[i], sPedClothes.iPropTexture[i])
		ENDIF
	ENDREPEAT
	
	// Pick a random hair style, but keep creator tints
	PED_COMP_NAME_ENUM eHair
	IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
		eHair = GET_RANDOM_MALE_HAIR()
	ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
		eHair = GET_RANDOM_FEMALE_HAIR()
	ENDIF
	// Keep the tint that was generated using creator
	INT iTint1, iTint2
	GET_PED_HAIR_TINT(PLAYER_PED_ID(), iTint1, iTint2)
	// Apply then update stats
	SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, eHair, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iTint1, iTint2)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO,ENUM_TO_INT(eHair))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,ENUM_TO_INT(eHair))
	SET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT, iTint1)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, iTint2)
	g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_HAIR, eHair)
	SET_PACKED_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HAIR, g_sTempCompData[1].iTexture)
	SET_PACKED_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HAIR, g_sTempCompData[1].iDrawable)
	
	WAIT(0)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
		WAIT(0)
	ENDWHILE
	
	WHILE NOT HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
		WAIT(0)
	ENDWHILE
	
	FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
	
	b_debug_ped_randomise_appearance = FALSE
ENDPROC

PROC PROCESS_PED_CREATE_CLONE_DEBUG()
	
	REQUEST_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
	IF NOT HAS_MODEL_LOADED(GET_ENTITY_MODEL(PLAYER_PED_ID()))
		EXIT
	ENDIF
	
	VECTOR vRot = GET_FINAL_RENDERED_CAM_ROT()
	PED_INDEX pedTempPed = CLONE_PED(PLAYER_PED_ID(), FALSE, FALSE, FALSE)
	SET_ENTITY_COORDS(pedTempPed, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FINAL_RENDERED_CAM_COORD(), vRot.z, <<0.0, 2.0, 0.0>>))
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTempPed, TRUE)
	
	WAIT(0)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedTempPed)
		WAIT(0)
	ENDWHILE
	
	WHILE NOT HAS_PED_HEAD_BLEND_FINISHED(pedTempPed)
		WAIT(0)
	ENDWHILE
	
	FINALIZE_HEAD_BLEND(pedTempPed)
	
	b_debug_ped_create_clone = FALSE
ENDPROC

PROC ADD_PED_COMP_DATA_TO_FILE(PED_COMP_TYPE_ENUM eCompType, INT iType)

	PED_COMP_NAME_ENUM ePedComp = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), eCompType)
	INT iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(GET_PED_COMPONENT_FROM_TYPE(eCompType)), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
	
	IF iDLCNameHash = 0
		iDLCNameHash = -1
	ENDIF
	
	TEXT_LABEL_63 tlNameHash
	
	IF iType = 0
		IF iDLCNameHash = -1
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ELSE
			IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_COMPONENT(ENUM_TO_INT(")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("), componentItem)")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_COMPONENT(")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE(", componentItem)   // MISSING DLC NAME")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = componentItem.m_drawableIndex		")SAVE_STRING_TO_DEBUG_FILE("sOutfitsData.iComponentTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = componentItem.m_textureIndex")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ELIF iType = 1
	
		IF iDLCNameHash != -1
			ePedComp = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
		ELSE
			iDLCNameHash = 0
		ENDIF
		
		IF ENUM_TO_INT(ePedComp) = 0
		AND iDLCNameHash = 0
			// skip
		ELSE	
			SAVE_STRING_TO_DEBUG_FILE("			<Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF iDLCNameHash = 0
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>   <!-- MISSING DLC NAME -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<enumValue value=\"") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(ePedComp))SAVE_STRING_TO_DEBUG_FILE("\" />")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(eCompType))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			</Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDIF
ENDPROC

PROC ADD_PED_PROP_DATA_TO_FILE(PED_PROP_POSITION eAnchor, INT iType)

	IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor) = -1
		EXIT
	ENDIF
	
	PED_COMP_NAME_ENUM ePedProp = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor), eAnchor)
	INT iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(eAnchor), GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor))
	
	IF iDLCNameHash = 0
		iDLCNameHash = -1
	ENDIF
	
	TEXT_LABEL_63 tlNameHash
	
	IF iType = 0
		IF iDLCNameHash = -1
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = ") SAVE_INT_TO_DEBUG_FILE(GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor))
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = ") SAVE_INT_TO_DEBUG_FILE(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor))
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ELSE
			IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_PROP(ENUM_TO_INT(")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("), propItem)")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_PROP(")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE(", propItem) // MISSING DLC NAME")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = propItem.m_propIndex")
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = propItem.m_textureIndex")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ELIF iType = 1
		IF iDLCNameHash != -1
			ePedProp = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
		ELSE
			iDLCNameHash = 0
		ENDIF
		
		IF ENUM_TO_INT(ePedProp) = 0
		AND iDLCNameHash = 0
			// skip
		ELSE
			SAVE_STRING_TO_DEBUG_FILE("			<Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF iDLCNameHash = 0
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>   <!-- MISSING DLC NAME -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<enumValue value=\"") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(ePedProp))SAVE_STRING_TO_DEBUG_FILE("\" />")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<eAnchorPoint>")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("</eAnchorPoint>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			</Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDIF
ENDPROC
PROC OUTPUT_OUTFIT_SETUP_META(STRING sFilename = NULL)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF IS_STRING_NULL_OR_EMPTY(sFilename)
				SAVE_STRING_TO_DEBUG_FILE("	<!-- OUTFIT -->")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	<!-- OUTFIT ")
				SAVE_STRING_TO_DEBUG_FILE(sFilename)
				SAVE_STRING_TO_DEBUG_FILE(" -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<cost value=\"99\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<textLabel>CLO_OUTFIT_LABEL</textLabel>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<uniqueNameHash>DLC_MP_OUTFIT_HASH</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<locate value=\"999\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("		<includedPedComponents>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_BERD, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TORSO, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_LEGS, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_FEET, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TEETH, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL2, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_DECL, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_JBIB, 1)
			
			SAVE_STRING_TO_DEBUG_FILE("		</includedPedComponents>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<includedPedProps>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HEAD, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EYES, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EARS, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_MOUTH, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_HAND, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_HAND, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_WRIST, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_WRIST, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HIP, 1)
			
			SAVE_STRING_TO_DEBUG_FILE("		</includedPedProps>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	</Item>")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC OUTPUT_OUTFIT_SETUP_HEIST(STRING sFilename = NULL)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			IF IS_STRING_NULL_OR_EMPTY(sFilename)
				SAVE_STRING_TO_DEBUG_FILE("CASE MP_OUTFIT_ENUM")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("CASE ")
				SAVE_STRING_TO_DEBUG_FILE(sFilename)
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_BERD, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TORSO, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_LEGS, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_FEET, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TEETH, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL2, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_DECL, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_JBIB, 0)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HEAD, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EYES, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EARS, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_MOUTH, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_HAND, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_HAND, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_WRIST, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_WRIST, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HIP, 0)
			
			SAVE_STRING_TO_DEBUG_FILE("BREAK")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC OUTPUT_PED_DEBUG_OUTPUT()
	// load xml and store file names.
	TEXT_LABEL_63 tlFilenames[12]
	
	IF NOT LOAD_XML_FILE("X:/gta5/titleupdate/dev_ng/DebugPedData/batch_output.xml")
		// bad file
		DELETE_XML_FILE()
		
		// create one
		tlFilenames[0] = "X:/gta5/titleupdate/dev_ng/DebugPedData"
		tlFilenames[1] = "batch_output.xml"
		
		CLEAR_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
		OPEN_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<DebugPedData ", tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_STRING_TO_NAMED_DEBUG_FILE("	File1=\"\"", tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" />", tlFilenames[0], tlFilenames[1])
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlFilenames[0], tlFilenames[1])
		CLOSE_DEBUG_FILE()
		
	ELIF GET_NUMBER_OF_XML_NODES() = 0
		// bad file
		DELETE_XML_FILE()
	ELSE
	
		INT iNumNodes = GET_NUMBER_OF_XML_NODES()
		INT iNumAttributes
		INT iAttribute
		TEXT_LABEL_63 txtNodeName
		INT iAttributeHash
		INT i
		REPEAT iNumNodes i
			txtNodeName = GET_XML_NODE_NAME()
			SWITCH GET_HASH_KEY(txtNodeName)
				CASE HASH("DebugPedData")
					iNumAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
					REPEAT iNumAttributes iAttribute
						iAttributeHash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttribute))
						SWITCH iAttributeHash
							CASE HASH("File1") tlFilenames[0] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File2") tlFilenames[1] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File3") tlFilenames[2] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File4") tlFilenames[3] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File5") tlFilenames[4] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File6") tlFilenames[5] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File7") tlFilenames[6] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File8") tlFilenames[7] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File9") tlFilenames[8] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File10") tlFilenames[9] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File11") tlFilenames[10] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
							CASE HASH("File12") tlFilenames[11] = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute) BREAK
						ENDSWITCH
					ENDREPEAT
				BREAK
			ENDSWITCH
			
			GET_NEXT_XML_NODE()
		ENDREPEAT
		
		DELETE_XML_FILE()
		
		WAIT(0)
		
		REPEAT COUNT_OF(tlFilenames) i
			IF NOT IS_STRING_NULL_OR_EMPTY(tlFilenames[i])
				IF PROCESS_PED_LOAD_DEBUG(0, tlFilenames[i])
					SET_BIT(i_debug_process_ped_set, 0)
					WHILE IS_BIT_SET(i_debug_process_ped_set, 0)
						PROCESS_PED_SET_DEBUG(0)
						WAIT(0)
						IF NOT IS_BIT_SET(i_debug_process_ped_set, 0)
							OUTPUT_OUTFIT_SETUP_META(tlFilenames[i])
						ENDIF
					ENDWHILE
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(tlFilenames) i
			IF NOT IS_STRING_NULL_OR_EMPTY(tlFilenames[i])
				IF PROCESS_PED_LOAD_DEBUG(0, tlFilenames[i])
					SET_BIT(i_debug_process_ped_set, 0)
					WHILE IS_BIT_SET(i_debug_process_ped_set, 0)
						PROCESS_PED_SET_DEBUG(0)
						WAIT(0)
						IF NOT IS_BIT_SET(i_debug_process_ped_set, 0)
							OUTPUT_OUTFIT_SETUP_HEIST(tlFilenames[i])
						ENDIF
					ENDWHILE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	b_debug_output_ped_save_data = FALSE
ENDPROC

FUNC INT GET_CLOTHING_SELECT_MENU_COUNT(INT iMenu)
	SWITCH iMenu
		CASE CLOTHES_SELECT_MENU_MAIN	RETURN 7 BREAK
		CASE CLOTHES_SELECT_MENU_COMPS	RETURN 12 BREAK
		CASE CLOTHES_SELECT_MENU_PROPS	RETURN 9 BREAK
		CASE CLOTHES_SELECT_MENU_DECORATIONS RETURN 2 BREAK
		CASE CLOTHES_SELECT_MENU_OUTFITS RETURN 1 BREAK
		CASE CLOTHES_SELECT_MENU_MODEL RETURN 1 BREAK
		CASE CLOTHES_SELECT_MENU_SHOP_LOCATE RETURN 1 BREAK
		CASE CLOTHES_SELECT_MENU_CAMERA RETURN 1 BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC BOOL IS_HASH_VALUE_IN_HASH_ARRAY(INT iHashValue, INT &iHashArray[])

	IF iHashValue = 0
		RETURN FALSE
	ENDIF
	
	INT iCount
	REPEAT COUNT_OF(iHashArray) iCount
		IF iHashArray[iCount] = iHashValue
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ITEM_LABEL_CONTAIN_STRING(TEXT_LABEL_15 &tlLabel, TEXT_LABEL_15 &tlKey)
	IF NOT IS_STRING_NULL_OR_EMPTY(tlLabel)
	AND DOES_TEXT_LABEL_EXIST(tlLabel)
	
		IF GET_HASH_KEY(tlLabel) = GET_HASH_KEY(tlKey)
			RETURN TRUE
		ENDIF
	
		TEXT_LABEL_63 tlMain = GET_STRING_FROM_TEXT_FILE(tlLabel)
		
		INT iKeyHash = GET_HASH_KEY(tlKey)
		
		INT iLengthMain = GET_LENGTH_OF_LITERAL_STRING(tlMain)
		INT iLengthKey = GET_LENGTH_OF_LITERAL_STRING(tlKey)
		
		INT iChar
		
		FOR iChar = 0 TO iLengthMain-1
			IF iChar+iLengthKey > iLengthMain
				RETURN FALSE
			ELIF GET_HASH_KEY(GET_STRING_FROM_STRING(tlMain, iChar, iChar+iLengthKey)) = iKeyHash
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_DEBUG_CLOTHING_MENU(BOOL bProcessingItem = FALSE)
	
	SET_MENU_SELECTION_BAR_COLOUR(HUD_COLOUR_GREY, bProcessingItem)
	
	DRAW_MENU()
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	
	// Draw component/prop info.
	IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
	OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
		IF IS_CUSTOM_MENU_ON_SCREEN()
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
			
			FLOAT fFinalPanelY = GET_CUSTOM_MENU_FINAL_Y_COORD()
			FLOAT fStartY = GET_CUSTOM_MENU_FINAL_Y_COORD()+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 KEYWORD
			IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
				SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGS")
					IF sClothesSelect.bKeyWordState
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("Filter: ~b~")
					ELSE
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("Filter: ~r~")
					ENDIF
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlKeyWord)
				END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				fFinalPanelY += CUSTOM_MENU_HEADER_H
				
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
				fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			ENDIF
			
			////////////////////////////////////////
			///    	 LOCKED STATUS
			BOOL bAddLockedStatus = FALSE
			IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])) != sClothesSelect.iLastIndex
				OR GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])) != sClothesSelect.iLastTexture
					bAddLockedStatus = TRUE
				ENDIF
			ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
				IF sClothesSelect.iLastIndex != -1
					IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])) != sClothesSelect.iLastIndex
					OR (sClothesSelect.iLastIndex != -1 AND GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])) != sClothesSelect.iLastTexture)
						bAddLockedStatus = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF bAddLockedStatus
				SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
					// Locked
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING("~r~***ITEM LOCKED***")
				END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
				fFinalPanelY += CUSTOM_MENU_HEADER_H
				
				DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
				fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			ENDIF
			
			////////////////////////////////////////
			///    	 VARIATION INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF1")
				// Global ID: ~1~, ~1~ ~n~
				ADD_TEXT_COMPONENT_INTEGER(sClothesSelect.iIndex)
				ADD_TEXT_COMPONENT_INTEGER(sClothesSelect.iTexture)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF2")
				// Local ID: ~1~ , ~1~ ~n~
				ADD_TEXT_COMPONENT_INTEGER(sClothesSelect.iLocalIndex)
				ADD_TEXT_COMPONENT_INTEGER(sClothesSelect.iTexture)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("GEN_BIGM_NUM")
				// Local ID: ~1~ , ~1~ ~n~
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING("Enum: ")
				//ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP(GET_ENTITY_MODEL(PLAYER_PED_ID()), GET_HASH_KEY(sClothesSelect.tlDLCKey), INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])))
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
					ADD_TEXT_COMPONENT_INTEGER(ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), sClothesSelect.iIndex, sClothesSelect.iTexture, INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]))))
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					ADD_TEXT_COMPONENT_INTEGER(ENUM_TO_INT(GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), sClothesSelect.iIndex, sClothesSelect.iTexture, INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]))))
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(-99)
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF11")
				// Menu:~g~ ~a~
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(sClothesSelect.tlShopMenu)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 DLC PACK INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF7")
				// DLC Pack: ~a~
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlDLCPack)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF6")
				// DLC Key: ~a~
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlDLCKey)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 TEXT LABEL INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF5")
				// Label Hash: ~1~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_INTEGER(0)
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(GET_HASH_KEY(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF4")
				// Label: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlItemLabel)
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF3")
				// Name: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
				OR NOT DOES_TEXT_LABEL_EXIST(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_STRING_FROM_TEXT_FILE(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			RESET_SCRIPT_GFX_ALIGN()
			
		ENDIF
		
	ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
		IF IS_CUSTOM_MENU_ON_SCREEN()
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
			
			FLOAT fFinalPanelY = GET_CUSTOM_MENU_FINAL_Y_COORD()
			FLOAT fStartY = GET_CUSTOM_MENU_FINAL_Y_COORD()+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 DECORATION INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF8")
				// Collection: ~a~
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlDLCKey)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF9")
				// Preset: ~1~
				ADD_TEXT_COMPONENT_INTEGER(sClothesSelect.iDecorationPreset)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF10")
				// Compatible: ~a~
				IF sClothesSelect.bDecorationCompatible
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("Yes")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~No")
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 TEXT LABEL INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF5")
				// Label Hash: ~1~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_INTEGER(0)
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(GET_HASH_KEY(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF4")
				// Label: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlItemLabel)
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF3")
				// Name: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
				OR NOT DOES_TEXT_LABEL_EXIST(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_STRING_FROM_TEXT_FILE(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			RESET_SCRIPT_GFX_ALIGN()
		ENDIF
		
	ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
		IF IS_CUSTOM_MENU_ON_SCREEN()
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
			
			FLOAT fFinalPanelY = GET_CUSTOM_MENU_FINAL_Y_COORD()
			FLOAT fStartY = GET_CUSTOM_MENU_FINAL_Y_COORD()+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 DLC PACK INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF7")
				// DLC Pack: ~a~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlDLCPack)
				OR NOT DOES_TEXT_LABEL_EXIST(sClothesSelect.tlDLCPack)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_STRING_FROM_TEXT_FILE(sClothesSelect.tlDLCPack))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF6")
				// DLC Key: ~a~
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlDLCKey)
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			////////////////////////////////////////
			///    	 TEXT LABEL INFO
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF5")
				// Label Hash: ~1~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_INTEGER(0)
				ELSE
					ADD_TEXT_COMPONENT_INTEGER(GET_HASH_KEY(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF4")
				// Label: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sClothesSelect.tlItemLabel)
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			SETUP_MENU_ITEM_MESSAGE_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("CLO_SEL_INF3")
				// Name: ~a~ ~n~
				IF IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlItemLabel)
				OR NOT DOES_TEXT_LABEL_EXIST(sClothesSelect.tlItemLabel)
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME("~r~NA")
				ELSE
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_STRING_FROM_TEXT_FILE(sClothesSelect.tlItemLabel))
				ENDIF
			END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_X+CUSTOM_MENU_TEXT_INDENT_X, fFinalPanelY+CUSTOM_MENU_TEXT_INDENT_Y)
			fFinalPanelY += CUSTOM_MENU_HEADER_H
			
			DRAW_RECT_FROM_CORNER(CUSTOM_MENU_X, fStartY, CUSTOM_MENU_W, fFinalPanelY-fStartY, 0, 0, 0, tiCOMMON_MENU_BG_ALPHA)
			fStartY = fFinalPanelY+CUSTOM_MENU_SPACER_H
			
			RESET_SCRIPT_GFX_ALIGN()
		ENDIF
		
	ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
	
	ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
	
	ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
		
	ENDIF
ENDPROC

PROC PROCESS_CLOTHING_SELECT_MENU()

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_SHIFT, "Clothes Debug")
		sClothesSelect.bDisplay = !sClothesSelect.bDisplay
		IF sClothesSelect.bDisplay
			g_bValidatePlayersTorsoComponent = FALSE
			g_bSafeToProcessMPChecks = FALSE
			PRINTLN("PROCESS_CLOTHING_SELECT_MENU - Activating debug menu (Shift+C)")
		ELSE
			IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SkipPedCompValidation")
				g_bValidatePlayersTorsoComponent = TRUE
			ENDIF
			g_bSafeToProcessMPChecks = TRUE
			PRINTLN("PROCESS_CLOTHING_SELECT_MENU - De-activating debug menu (Shift+C)")
		ENDIF
	ENDIF
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		sClothesSelect.bDisplay = FALSE
	ENDIF
	
	IF sClothesSelect.bDisplay
	
		sClothesSelect.bMenuAssetsRequested = TRUE
		REQUEST_ADDITIONAL_TEXT("CLO_MNU", MENU_TEXT_SLOT)
		IF LOAD_MENU_ASSETS()
		AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
		AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
		AND NOT IS_WARNING_MESSAGE_ACTIVE()
		AND NOT g_sShopSettings.bProcessStoreAlert
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND HAS_THIS_ADDITIONAL_TEXT_LOADED("CLO_MNU", MENU_TEXT_SLOT)
		
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			
			IF NOT sClothesSelect.bMenuInitialised
				INT iCount
				REPEAT COUNT_OF(sClothesSelect.iCurrentItem) iCount
					sClothesSelect.iCurrentItem[iCount] = 0
					sClothesSelect.iCurrentTopItem[iCount] = 0
					sClothesSelect.iCurrentItem_copy[iCount] = 0
					sClothesSelect.iCurrentTopItem_copy[iCount] = 0
				ENDREPEAT
				sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MAIN
				sClothesSelect.iMenuDepth = 0
				sClothesSelect.bMenuInitialised = TRUE
				sClothesSelect.bRebuildMenu = TRUE
				sClothesSelect.tlKeyWord = ""
			ENDIF
			
			IF sClothesSelect.bGrabClothingData
			
				INT iDraw
				INT iDLCPackName
				INT iDLCNameHash
				INT iSlot = sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]
				scrShopPedComponent componentItem
				scrShopPedProp propItem
				PED_COMP_NAME_ENUM ePedComp
				CLOTHES_MENU_ENUM eMenu
				TEXT_LABEL_15 tlMenuLabel
				INT iSubMenu
				
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				
					IF sClothesSelect.bUseCachedClothingData
						sClothesSelect.iIndex = sClothesSelect.iLastIndex
						sClothesSelect.iTexture = sClothesSelect.iLastTexture
					ELSE
						sClothesSelect.iIndex = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iSlot))
						sClothesSelect.iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iSlot))
						sClothesSelect.iLastIndex = sClothesSelect.iIndex
						sClothesSelect.iLastTexture = sClothesSelect.iTexture
					ENDIF
					sClothesSelect.iLocalIndex = 0
					
					FOR iDraw = 0 TO sClothesSelect.iIndex
						IF iDLCPackName != GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iSlot, iDraw)
							iDLCPackName = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iSlot, iDraw)
							sClothesSelect.iLocalIndex = 0
						ELSE
							sClothesSelect.iLocalIndex++
						ENDIF
					ENDFOR
					
					iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iSlot, sClothesSelect.iIndex, sClothesSelect.iTexture)
					
					IF iDLCNameHash != 0
						IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, sClothesSelect.tlDLCKey)
							sClothesSelect.tlDLCKey = ""
							sClothesSelect.tlDLCKey += iDLCNameHash
						ENDIF
						IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackName, sClothesSelect.tlDLCPack)
							sClothesSelect.tlDLCPack = ""
							sClothesSelect.tlDLCPack += iDLCPackName
						ENDIF
						GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
						sClothesSelect.tlItemLabel = componentItem.m_textLabel
						
						IF componentItem.m_locate = 0
							eMenu = CLO_MENU_MASKS
						ELSE
							eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, componentItem.m_locate)
						ENDIF
						tlMenuLabel = GET_CLOTHES_MENU_NAME(eMenu, FALSE)
						
						IF DOES_TEXT_LABEL_EXIST(tlMenuLabel)
						
							sClothesSelect.tlShopMenu = GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
						
							IF eMenu = CLO_MENU_MASKS
							AND GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_MASK_TYPE_INVALID
								iSubMenu = GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ELIF eMenu = CLO_MENU_EARRINGS
							AND GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_EARRING_TYPE_INVALID
								iSubMenu = GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ELIF eMenu = CLO_MENU_MP_AWARD_SHIRTS
							AND GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_AWARD_TYPE_INVALID
								iSubMenu = GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ELIF eMenu = CLO_MENU_MP_XMAS_TOPS
							AND GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_FESTIVE_TYPE_INVALID
								iSubMenu = GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ELIF eMenu = CLO_MENU_MP_ARENA_WAR_TOPS
							AND GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_ARENA_WAR_TOP_TYPE_INVALID
								iSubMenu = GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ENDIF
						ELSE
							sClothesSelect.tlShopMenu = "~r~NA"
						ENDIF	
					ELSE
						sClothesSelect.tlShopMenu = "~r~NA"
						sClothesSelect.tlDLCKey = "~r~NA"
						
						IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackName, sClothesSelect.tlDLCPack)
							sClothesSelect.tlDLCPack = ""
							sClothesSelect.tlDLCPack += iDLCPackName
						ENDIF
						
						ePedComp = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iSlot))
						IF ePedComp != DUMMY_PED_COMP
							g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iSlot), ePedComp)
							sClothesSelect.tlItemLabel = g_sTempCompData[1].sLabel
						ELSE
							sClothesSelect.tlItemLabel = ""
						ENDIF
					ENDIF
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					
					IF sClothesSelect.bUseCachedClothingData
						sClothesSelect.iIndex = sClothesSelect.iLastIndex
						sClothesSelect.iTexture = sClothesSelect.iLastTexture
					ELSE
						sClothesSelect.iIndex = GET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iSlot))
						sClothesSelect.iTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iSlot))
						sClothesSelect.iLastIndex = sClothesSelect.iIndex
						sClothesSelect.iLastTexture = sClothesSelect.iTexture
					ENDIF
					sClothesSelect.iLocalIndex = -1
					
					FOR iDraw = 0 TO sClothesSelect.iIndex
						IF iDLCPackName != GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iSlot), iDraw)
							iDLCPackName = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iSlot), iDraw)
							sClothesSelect.iLocalIndex = 0
						ELSE
							sClothesSelect.iLocalIndex++
						ENDIF
					ENDFOR
					
					iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iSlot, sClothesSelect.iIndex, sClothesSelect.iTexture)
					
					IF iDLCNameHash != 0
						IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, sClothesSelect.tlDLCKey)
							sClothesSelect.tlDLCKey = ""
							sClothesSelect.tlDLCKey += iDLCNameHash
						ENDIF
						IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackName, sClothesSelect.tlDLCPack)
							sClothesSelect.tlDLCPack = ""
							sClothesSelect.tlDLCPack += iDLCPackName
						ENDIF
						GET_SHOP_PED_PROP(iDLCNameHash, propItem)
						sClothesSelect.tlItemLabel = propItem.m_textLabel
						
						IF propItem.m_locate = 0
							eMenu = CLO_MENU_MASKS
						ELSE
							eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, propItem.m_locate)
						ENDIF
						tlMenuLabel = GET_CLOTHES_MENU_NAME(eMenu, FALSE)
						
						IF DOES_TEXT_LABEL_EXIST(tlMenuLabel)
						
							sClothesSelect.tlShopMenu = GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
						
							IF eMenu = CLO_MENU_MASKS
							AND GET_MASK_MENU_FROM_LABEL(propItem.m_textLabel) != iSHOP_MASK_TYPE_INVALID
								iSubMenu = GET_MASK_MENU_FROM_LABEL(propItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ELIF eMenu = CLO_MENU_EARRINGS
							AND GET_EARRING_MENU_FROM_LABEL(propItem.m_textLabel) != iSHOP_EARRING_TYPE_INVALID
								iSubMenu = GET_EARRING_MENU_FROM_LABEL(propItem.m_textLabel)
								GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
								sClothesSelect.tlShopMenu += " - "
								sClothesSelect.tlShopMenu += GET_STRING_FROM_TEXT_FILE(tlMenuLabel)
								
							ENDIF
						ELSE
							sClothesSelect.tlShopMenu = "~r~NA"
						ENDIF
					ELSE
						sClothesSelect.tlShopMenu = "~r~NA"
						sClothesSelect.tlDLCKey = "~r~NA"
						
						IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackName, sClothesSelect.tlDLCPack)
							sClothesSelect.tlDLCPack = ""
							sClothesSelect.tlDLCPack += iDLCPackName
						ENDIF
						
						ePedComp = GET_PED_PROP_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iSlot))
						IF ePedComp != DUMMY_PED_COMP
							g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePedComp)
							sClothesSelect.tlItemLabel = g_sTempCompData[1].sLabel
						ELSE
							sClothesSelect.tlItemLabel = ""
						ENDIF
					ENDIF
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
					
					sClothesSelect.iDecorationCount = 0
					sClothesSelect.tlDLCKey = "~r~NA"
					sClothesSelect.iDecorationCollection = -1
					sClothesSelect.iDecorationPreset = -1
					sClothesSelect.tlItemLabel = "~r~NA"
					sClothesSelect.bDecorationCompatible = FALSE
					
					TATTOO_FACTION_ENUM eFaction = TATTOO_MP_FM
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
						eFaction = TATTOO_MP_FM_F
					ENDIF
					
					INT iDLCIndex
					INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
					sTattooShopItemValues sDLCTattooData
					
					// Grab top info so we can check compatibility
					INT iDLCItemHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
					// If we're wearing a jacket try and get the jbib version of the t-shirt instead
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCItemHash, DLC_RESTRICTION_TAG_JACKET, ENUM_TO_INT(SHOP_PED_COMPONENT))
						PED_COMP_NAME_ENUM eCurrentAccs = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
						PED_COMP_NAME_ENUM eJBIBFromAccs = GET_JBIB_FROM_SPECIAL(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentAccs, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
						IF eJBIBFromAccs != DUMMY_PED_COMP
							iDLCItemHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
						ENDIF
					ENDIF
					
					INT iPresetCount
					INT iCompatiblePresets[256]
					
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCItemHash, DLC_RESTRICTION_TAG_TAT_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCItemHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
						// Need to lookup the variant components defined in the shop meta for this dlc jbib
						INT iAltComps = GET_SHOP_PED_APPAREL_VARIANT_COMPONENT_COUNT(iDLCItemHash)
						INT iAltComp
						INT iRetNameHash, iRetCompEnum, iRetType
						REPEAT iAltComps iAltComp
							GET_VARIANT_COMPONENT(iDLCItemHash, iAltComp, iRetNameHash, iRetCompEnum, iRetType)
							IF (iRetType = ENUM_TO_INT(PED_COMP_DECL))
								iCompatiblePresets[iPresetCount] = iRetNameHash
								iPresetCount++
							ENDIF
						ENDREPEAT
					ENDIF
					
					REPEAT iDLCCount iDLCIndex
						IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
							IF sDLCTattooData.UpdateGroup = HASH("torsoDecal")
							AND (NOT sClothesSelect.bCompatibleOnly OR IS_HASH_VALUE_IN_HASH_ARRAY(sDLCTattooData.Preset, iCompatiblePresets))
							
								IF sClothesSelect.iDecorationIndex = sClothesSelect.iDecorationCount
									IF NOT GET_DLC_PACK_NAME_FROM_HASH(sDLCTattooData.Collection, sClothesSelect.tlDLCKey)
										sClothesSelect.tlDLCKey = ""
										sClothesSelect.tlDLCKey += sDLCTattooData.Collection
									ENDIF
									sClothesSelect.iDecorationCollection = sDLCTattooData.Collection
									sClothesSelect.iDecorationPreset = sDLCTattooData.Preset
									sClothesSelect.tlItemLabel = sDLCTattooData.Label
									sClothesSelect.bDecorationCompatible = IS_HASH_VALUE_IN_HASH_ARRAY(sDLCTattooData.Preset, iCompatiblePresets)
								ENDIF
								sClothesSelect.iDecorationCount++
							ENDIF
						ENDIF
					ENDREPEAT
					
					// Preview
					IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] != 0
						UPDATE_TATOOS_MP(PLAYER_PED_ID(), TRUE)
						IF sClothesSelect.iDecorationCollection != -1
						AND sClothesSelect.iDecorationPreset != -1
							ADD_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sClothesSelect.iDecorationCollection, sClothesSelect.iDecorationPreset)
							ADD_SECONDARY_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sClothesSelect.iDecorationCollection, sClothesSelect.iDecorationPreset)
						ENDIF
					ENDIF
						
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
				
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
						sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(3, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
					ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
						sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(4, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
					ELSE
						sClothesSelect.iOutfitCount = 0
					ENDIF
					
					scrShopPedOutfit outfitItem
					GET_SHOP_PED_QUERY_OUTFIT(sClothesSelect.iOutfitIndex, outfitItem)
					
					IF NOT GET_DLC_NAME_HASH_STRING(outfitItem.m_nameHash, sClothesSelect.tlDLCKey)
						sClothesSelect.tlDLCKey = ""
						sClothesSelect.tlDLCKey += outfitItem.m_nameHash
					ENDIF
					sClothesSelect.tlDLCPack = GET_SHOP_CONTENT_FOR_MENU(outfitItem.m_textLabel)
					sClothesSelect.tlItemLabel = outfitItem.m_textLabel
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
				
					sClothesSelect.eModelSelect = GET_ENTITY_MODEL(PLAYER_PED_ID())
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
				
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
					
				ENDIF
				sClothesSelect.bUseCachedClothingData = TRUE
				sClothesSelect.bGrabClothingData = FALSE
			ENDIF
			
			IF sClothesSelect.bRebuildMenu
			
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MAIN
				
					CLEAR_MENU_DATA()
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("CLOTHING DEBUG (Shift+C)")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Components")
					ADD_MENU_ITEM_TEXT(1, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Props")
					ADD_MENU_ITEM_TEXT(2, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Overlays")
					ADD_MENU_ITEM_TEXT(3, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Outfits")
					ADD_MENU_ITEM_TEXT(4, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Model")
					ADD_MENU_ITEM_TEXT(5, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Locate")
					ADD_MENU_ITEM_TEXT(6, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Camera")
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
					
					CLEAR_MENU_DATA()
					SET_MAX_MENU_ROWS_TO_DISPLAY(12)
					
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT COMPONENT")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					INT iItem
					REPEAT 12 iItem
						ADD_MENU_ITEM_TEXT(iItem, "STRING", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_PED_COMP_NAME_STRING(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItem)))
						
						ADD_MENU_ITEM_TEXT(iItem, "CLO_SEL_RNG", 2)
						
						IF sClothesSelect.bUseCachedClothingData
						AND sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = iItem
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iLastIndex)
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iLastTexture)
						ELSE
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iItem)))
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iItem)))
						ENDIF
					ENDREPEAT
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_VAL")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, "CLO_SEL_SRCH")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_REM")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RT, "ITEM_SKIP")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RB, "ITEM_SKIP2")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT PROP")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					INT iItem
					REPEAT 9 iItem
						ADD_MENU_ITEM_TEXT(iItem, "STRING", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iItem)))
						
						ADD_MENU_ITEM_TEXT(iItem, "CLO_SEL_RNG", 2)
						IF sClothesSelect.bUseCachedClothingData
						AND sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = iItem
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iLastIndex)
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iLastTexture)
						ELSE
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iItem)))
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iItem)))
						ENDIF
					ENDREPEAT
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT, "CLO_SEL_SRCH")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_REM")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RT, "ITEM_SKIP")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RB, "ITEM_SKIP2")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
					
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT OVERLAY")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Compatible Only")
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					IF sClothesSelect.bCompatibleOnly
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Yes")
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("No")
					ENDIF
					
					ADD_MENU_ITEM_TEXT(1, "STRING", 1, (sClothesSelect.iDecorationCount > 0))
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Overlay")
					
					ADD_MENU_ITEM_TEXT(1, "CM_ITEM_COUNT", 2, (sClothesSelect.iDecorationCount > 0))
					
					IF sClothesSelect.iDecorationIndex = 0
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(0)
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iDecorationIndex+1)
					ENDIF
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iDecorationCount)
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_REM")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RT, "ITEM_SKIP2")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
					
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT OUTFIT")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Outfit")
					
					ADD_MENU_ITEM_TEXT(0, "CM_ITEM_COUNT", 2)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iOutfitIndex+1)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(sClothesSelect.iOutfitCount)
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_RB, "ITEM_SKIP2")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
					
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT MODEL")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Model")
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					IF sClothesSelect.eModelSelect = MP_M_FREEMODE_01
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("MP_M_FREEMODE_01")
					ELIF sClothesSelect.eModelSelect = MP_F_FREEMODE_01
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("MP_F_FREEMODE_01")
					ENDIF
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
				
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT LOCATE")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Locate")
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					SWITCH sClothesSelect.iLocateSelect
						CASE SHOP_LOCATE_WARP_LOCATION_TOPS 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Tops") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_PANTS 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Pants") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_SHOES 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Shoes") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_ACCESSORIES 	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Accessories") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_GLASSES		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Glasses") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_HATS 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Hats") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_MASKS 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Masks") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_OUTFITS 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Outfits") BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_WARDROBE 	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Wardrobe") BREAK
					ENDSWITCH
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
				
					CLEAR_MENU_DATA()
					CUSTOM_MENU_W = 0.320
					SET_MENU_TITLE("STRING")
					ADD_LITERAL_TO_MENU_TITLE("SELECT CAMERA")
					
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Camera")
					
					ADD_MENU_ITEM_TEXT(0, "STRING", 1)
					SWITCH sClothesSelect.iCameraSelect
						CASE CAMERA_SELECT_OFF			ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Off") BREAK
						CASE CAMERA_SELECT_HEAD 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Head") BREAK
						CASE CAMERA_SELECT_TORSO 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Torso") BREAK
						CASE CAMERA_SELECT_LEGS			ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Legs") BREAK
						CASE CAMERA_SELECT_FEET 		ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Feet") BREAK
						CASE CAMERA_SELECT_HEAD_REAR 	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Head Rear") BREAK
						CASE CAMERA_SELECT_TORSO_REAR	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Torso Rear") BREAK
						CASE CAMERA_SELECT_LEGS_REAR 	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Legs Rear") BREAK
						CASE CAMERA_SELECT_FEET_REAR 	ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL("Feet Rear") BREAK
					ENDSWITCH
					
					SET_TOP_MENU_ITEM(sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth])
					SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_Y, "ITEM_EXIT")
					
				ENDIF
				
				sClothesSelect.bRebuildMenu = FALSE
			ENDIF
			
			DRAW_DEBUG_CLOTHING_MENU()
			
			IF sClothesSelect.bValidateClothes
				IF sClothesSelect.iValidateClothesCount = 0
					PED_COMP_NAME_ENUM eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
					PED_COMP_NAME_ENUM eCurrentAccs = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
					
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, eCurrentJbib)	
					IF IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_OUTFIT_ONLY_BIT)
						// Skip
						sClothesSelect.bValidateClothes = FALSE
					ELSE
						// Re-apply jbib to correct some components.
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eCurrentJbib, FALSE)
						
						// Grab correct accs for current jacket
						IF IS_JBIB_COMPONENT_A_JACKET(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentJbib)
							PED_COMP_NAME_ENUM eJBIBFromAccs = GET_JBIB_FROM_SPECIAL(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentAccs, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
							IF eJBIBFromAccs != DUMMY_PED_COMP
								PED_COMP_NAME_ENUM eTempSpecial = GET_SPECIAL_COMP_FOR_JBIB_JACKET_VERSION(GET_ENTITY_MODEL(PLAYER_PED_ID()), eJBIBFromAccs, eCurrentJbib, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
								IF eTempSpecial != DUMMY_PED_COMP
								AND eTempSpecial != eCurrentAccs
									SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, eTempSpecial, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF sClothesSelect.iValidateClothesCount = 1
					FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), COMP_TYPE_BERD), FALSE)
				ELIF sClothesSelect.iValidateClothesCount = 2
					FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), COMP_TYPE_JBIB), FALSE)
				ELIF sClothesSelect.iValidateClothesCount = 3
					FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP(PLAYER_PED_ID(), COMP_TYPE_LEGS, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG), COMP_TYPE_LEGS), FALSE)
				ELIF sClothesSelect.iValidateClothesCount = 4
					
					//VALIDATE_INVISIBLE_PED_COMPONENTS()
					
					sClothesSelect.bValidateClothes = FALSE
					sClothesSelect.iValidateClothesCount = 0
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					sClothesSelect.bUseCachedClothingData = FALSE
				ENDIF
				sClothesSelect.iValidateClothesCount++
				EXIT
			ENDIF
			
			IF sClothesSelect.bEnterKeyWord
			
				SWITCH UPDATE_ONSCREEN_KEYBOARD()
					CASE OSK_SUCCESS
						sClothesSelect.tlKeyWord = GET_ONSCREEN_KEYBOARD_RESULT()
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
						sClothesSelect.bEnterKeyWord = FALSE
						sClothesSelect.bKeyWordState = TRUE
						
						IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
						AND GET_LENGTH_OF_LITERAL_STRING(sClothesSelect.tlKeyWord) > 1
							INT iLabelHash
							iLabelHash = GET_HASH_KEY(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sClothesSelect.tlKeyWord, 0, 1))
							IF iLabelHash = HASH("!")
								sClothesSelect.tlKeyWord = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sClothesSelect.tlKeyWord, 1, GET_LENGTH_OF_LITERAL_STRING(sClothesSelect.tlKeyWord))
								sClothesSelect.bKeyWordState = FALSE
							ENDIF
						ENDIF
					BREAK
					CASE OSK_CANCELLED
					CASE OSK_FAILED
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
						sClothesSelect.bEnterKeyWord = FALSE
						sClothesSelect.bKeyWordState = TRUE
					BREAK
				ENDSWITCH
				EXIT
			ENDIF
				
			UPDATE_SHOP_INPUT_DATA(sClothesSelect.sInputData, 300)
			
			BOOL bUp = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP) OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sClothesSelect.sInputData.bDPADUPReset)
			BOOL bDown = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)  OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sClothesSelect.sInputData.bDPADDOWNReset)
			BOOL bLeft = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT) OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT) AND sClothesSelect.sInputData.bDPADLEFTReset)
			BOOL bRight = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT) AND sClothesSelect.sInputData.bDPADRIGHTReset)
			BOOL bAccept = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN))
			BOOL bCancel = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_BACK))
			BOOL bOptions = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DELETE))
			BOOL bExit = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_ESCAPE))
			BOOL bClear = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME))
			BOOL bDrawSkip = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT))
			BOOL bBlockSkip = (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB))
			BOOL bOutputToDebug = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB))
			
			// Force accept
			IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
			AND sClothesSelect.bSetPlayerModel
				bAccept = TRUE
			ENDIF
			IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
			AND sClothesSelect.bWarpToLocate
				bAccept = TRUE
			ENDIF
			
			// Priorities
			IF sClothesSelect.bReleaseUD
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					sClothesSelect.bReleaseUD = FALSE
				ELSE
					bLeft = FALSE
					bRight = FALSE
				ENDIF
			ELIF sClothesSelect.bReleaseLR
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
					sClothesSelect.bReleaseLR = FALSE
				ELSE
					bUp = FALSE
					bDown = FALSE
				ENDIF
			ENDIF
			IF bUp OR bDown
				sClothesSelect.bReleaseUD = TRUE
			ENDIF
			IF bLeft OR bRight
				sClothesSelect.bReleaseLR = TRUE
			ENDIF
			
			IF bOutputToDebug
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					OPEN_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("[")
						SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_TYPE_STRING(INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])))
						SAVE_STRING_TO_DEBUG_FILE("][")
						SAVE_STRING_TO_DEBUG_FILE(sClothesSelect.tlDLCPack)
						SAVE_STRING_TO_DEBUG_FILE("][global=")
						SAVE_INT_TO_DEBUG_FILE(sClothesSelect.iIndex)
						SAVE_STRING_TO_DEBUG_FILE(",")
						SAVE_INT_TO_DEBUG_FILE(sClothesSelect.iTexture)
						SAVE_STRING_TO_DEBUG_FILE("][local=")
						SAVE_INT_TO_DEBUG_FILE(sClothesSelect.iLocalIndex)
						SAVE_STRING_TO_DEBUG_FILE(",")
						SAVE_INT_TO_DEBUG_FILE(sClothesSelect.iTexture)
						SAVE_STRING_TO_DEBUG_FILE("][")
						SAVE_STRING_TO_DEBUG_FILE("enum=")
						SAVE_INT_TO_DEBUG_FILE((ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), sClothesSelect.iIndex, sClothesSelect.iTexture, INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])))))
						SAVE_STRING_TO_DEBUG_FILE("][")
						SAVE_STRING_TO_DEBUG_FILE(sClothesSelect.tlDLCKey)
						SAVE_STRING_TO_DEBUG_FILE("]")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					CLOSE_DEBUG_FILE()
				ENDIF
			ENDIF
			
			// Timers
			IF bUp OR bDown
				sClothesSelect.sInputData.bDPADUPReset = FALSE
				sClothesSelect.sInputData.bDPADDownReset = FALSE
				sClothesSelect.sInputData.bLeftStickUDReset = FALSE
				sClothesSelect.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sClothesSelect.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
			ELIF bLeft OR bRight
				sClothesSelect.sInputData.bDPADLEFTReset = FALSE
				sClothesSelect.sInputData.bDPADRIGHTReset = FALSE
				sClothesSelect.sInputData.bLeftStickLRReset = FALSE
				sClothesSelect.sInputData.iLeftStickLRTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sClothesSelect.sInputData.iMP_LeftStickLRTimer = GET_NETWORK_TIME()
				ENDIF
			ENDIF
			
			// Navigation
			IF bAccept
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MAIN
					sClothesSelect.iCurrentItem_copy[sClothesSelect.iMenuDepth] = sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]
					sClothesSelect.iCurrentTopItem_copy[sClothesSelect.iMenuDepth] = sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth]
					
					IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 0
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
						sClothesSelect.bGrabClothingData = TRUE
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 1
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
						sClothesSelect.bGrabClothingData = TRUE
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 2
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
						sClothesSelect.bGrabClothingData = TRUE
						sClothesSelect.iDecorationIndex = 0
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 3
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
						sClothesSelect.bGrabClothingData = TRUE
						sClothesSelect.iOutfitIndex = 0
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 4
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
						sClothesSelect.bGrabClothingData = TRUE
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 5
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
						sClothesSelect.bGrabClothingData = TRUE
					ELIF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 6
						sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
						sClothesSelect.bGrabClothingData = TRUE
					ENDIF
					sClothesSelect.iMenuDepth++
					
					sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 0
					sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth] = 0
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					sClothesSelect.bValidateClothes = TRUE
					sClothesSelect.iValidateClothesCount = 0
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
					// [TODO]
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
				
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(OUTFIT_FMM_DLC)+sClothesSelect.iOutfitIndex), FALSE)
					ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(OUTFIT_FMF_DLC)+sClothesSelect.iOutfitIndex), FALSE)
					ENDIF
					
					UPDATE_TATOOS_MP(PLAYER_PED_ID())
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
				
					sClothesSelect.bSetPlayerModel = TRUE
					
					REQUEST_MODEL(sClothesSelect.eModelSelect)
					IF HAS_MODEL_LOADED(sClothesSelect.eModelSelect)
					
						FLOAT fGameplayCamHeading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
						FLOAT fGameplayCamPitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
						
						SET_PLAYER_MODEL(PLAYER_ID(), sClothesSelect.eModelSelect)
						SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						SET_MODEL_AS_NO_LONGER_NEEDED(sClothesSelect.eModelSelect)
						
						// Update stored model
						IF sClothesSelect.eModelSelect = MP_M_FREEMODE_01
							SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0)
						ELIF sClothesSelect.eModelSelect = MP_F_FREEMODE_01
							SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1)
						ENDIF
						
						// Clear some save data.
						INT i
						TATTOO_FACTION_ENUM eFaction = TATTOO_MP_FM
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
							eFaction = TATTOO_MP_FM_F
						ENDIF
						INT iDLCIndex
						INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
						REPEAT MAX_NUMBER_OF_TATTOOS i
							SET_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i), FALSE)
						ENDREPEAT
						REPEAT iDLCCount iDLCIndex
							SET_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex), FALSE)
						ENDREPEAT
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeading)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitch)
						
						sClothesSelect.bSetPlayerModel = FALSE
					ENDIF					
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
				
					BOOL bLoadingScene = FALSE
					
					VECTOR vLocateCoords
					FLOAT fLocateHeading
					SWITCH sClothesSelect.iLocateSelect
						CASE SHOP_LOCATE_WARP_LOCATION_TOPS 		vLocateCoords = <<76.0612, -1396.2948, 28.3761>> fLocateHeading = 281.1407 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_PANTS 		vLocateCoords = <<80.3144, -1399.1885, 28.3761>> fLocateHeading = 307.7607 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_SHOES 		vLocateCoords = <<81.4530, -1396.6451, 28.3761>> fLocateHeading = 294.1775 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_ACCESSORIES 	vLocateCoords = <<77.7338, -1389.3140, 28.3761>> fLocateHeading = 29.8214 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_GLASSES 		vLocateCoords = <<75.1655, -1390.8882, 28.3761>> fLocateHeading = 120.8661 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_HATS 		vLocateCoords = <<73.2574, -1400.3412, 28.3761>> fLocateHeading = 215.6490 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_MASKS 		vLocateCoords = <<-1336.5450, -1278.8964, 3.8578>> fLocateHeading = 292.4067 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_OUTFITS 		vLocateCoords = <<75.4393, -1392.4756, 28.3761>> fLocateHeading = 103.7945 BREAK
						CASE SHOP_LOCATE_WARP_LOCATION_WARDROBE		vLocateCoords = <<71.2583, -1387.7257, 28.3761>> fLocateHeading = 5.6863 BREAK
					ENDSWITCH
					
					// Keep processing until we load the scene.
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_START_SPHERE(vLocateCoords, 50)
					ENDIF
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						IF NOT IS_NEW_LOAD_SCENE_LOADED()
							bLoadingScene = TRUE
						ELSE
							NEW_LOAD_SCENE_STOP()
						ENDIF
					ENDIF
					
					IF bLoadingScene
						sClothesSelect.bWarpToLocate = TRUE
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vLocateCoords)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), fLocateHeading)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						sClothesSelect.bWarpToLocate = FALSE
					ENDIF
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
				
					SWITCH sClothesSelect.iCameraSelect
						CASE CAMERA_SELECT_OFF			bUseOffsetCam = FALSE	BREAK
						CASE CAMERA_SELECT_HEAD 		bUseOffsetCam = TRUE	bSetOffsetCamPreset[0] = TRUE	BREAK
						CASE CAMERA_SELECT_TORSO 		bUseOffsetCam = TRUE	bSetOffsetCamPreset[2] = TRUE	BREAK
						CASE CAMERA_SELECT_LEGS			bUseOffsetCam = TRUE	bSetOffsetCamPreset[4] = TRUE	BREAK
						CASE CAMERA_SELECT_FEET 		bUseOffsetCam = TRUE	bSetOffsetCamPreset[6] = TRUE	BREAK
						CASE CAMERA_SELECT_HEAD_REAR 	bUseOffsetCam = TRUE	bSetOffsetCamPreset[1] = TRUE	BREAK
						CASE CAMERA_SELECT_TORSO_REAR	bUseOffsetCam = TRUE	bSetOffsetCamPreset[3] = TRUE	BREAK
						CASE CAMERA_SELECT_LEGS_REAR 	bUseOffsetCam = TRUE	bSetOffsetCamPreset[5] = TRUE	BREAK
						CASE CAMERA_SELECT_FEET_REAR 	bUseOffsetCam = TRUE	bSetOffsetCamPreset[7] = TRUE	BREAK
					ENDSWITCH
				ENDIF
				
			ELIF bClear
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
					PED_COMPONENT ePedComp = INT_TO_ENUM(PED_COMPONENT, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					// Berd?
					IF ePedComp = PED_COMP_BERD
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ePedComp, 0, 0)
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					sClothesSelect.bUseCachedClothingData = FALSE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					CLEAR_PED_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]))
					
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					sClothesSelect.bUseCachedClothingData = FALSE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
					UPDATE_TATOOS_MP(PLAYER_PED_ID())
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELIF bCancel
				IF sClothesSelect.iCurrentMenu > 0
					sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MAIN
					sClothesSelect.iMenuDepth--
					sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = sClothesSelect.iCurrentItem_copy[sClothesSelect.iMenuDepth]
					sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth] = sClothesSelect.iCurrentTopItem_copy[sClothesSelect.iMenuDepth]
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELIF bOptions
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					sClothesSelect.bEnterKeyWord = TRUE
					SHOW_ONSCREEN_KEYBOARD("CLO_SEL_SRCHT", "", 15)
					PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELIF bExit
				sClothesSelect.bDisplay = FALSE
				sClothesSelect.bMenuInitialised = FALSE
				IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_SkipPedCompValidation")
					g_bValidatePlayersTorsoComponent = TRUE
				ENDIF
				bUseOffsetCam = FALSE
				g_bSafeToProcessMPChecks = TRUE
				PRINTLN("PROCESS_CLOTHING_SELECT_MENU - De-activating debug menu (Exit)")
				PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
			ELIF bUp
				sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]--
				IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] < 0
					sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = (GET_CLOTHING_SELECT_MENU_COUNT(sClothesSelect.iCurrentMenu)-1)
				ENDIF
				SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
				sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth] = GET_TOP_MENU_ITEM()
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					sClothesSelect.bUseCachedClothingData = FALSE
				ENDIF
				
			ELIF bDown
				sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth]++
				IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] > (GET_CLOTHING_SELECT_MENU_COUNT(sClothesSelect.iCurrentMenu)-1)
					sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 0
				ENDIF
				SET_CURRENT_MENU_ITEM(sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
				sClothesSelect.iCurrentTopItem[sClothesSelect.iMenuDepth] = GET_TOP_MENU_ITEM()
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				OR sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					sClothesSelect.bUseCachedClothingData = FALSE
				ENDIF
			
			ELIF bLeft
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
					
					PED_COMPONENT ePedComp = INT_TO_ENUM(PED_COMPONENT, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					PED_COMP_TYPE_ENUM eCompType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					INT iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ePedComp)
					INT iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ePedComp)
					
					IF sClothesSelect.bUseCachedClothingData
						iDrawable = sClothesSelect.iLastIndex
						iTexture = sClothesSelect.iLastTexture
					ENDIF
					
					// Use keyword search
					IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
					
						INT iItemCount
						INT iStartDraw = iDrawable
						INT iStartTexture = iTexture-1
						BOOL bItemFound = FALSE
						
						FOR iDrawable = iStartDraw TO 0 STEP -1
						
							IF iDrawable != iStartDraw
								iStartTexture = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
							ENDIF
							
							FOR iTexture = iStartTexture TO 0 STEP -1
								g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, eCompType))
								IF DOES_ITEM_LABEL_CONTAIN_STRING(g_sTempCompData[1].sLabel, sClothesSelect.tlKeyWord)
								OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND sClothesSelect.bKeyWordState = DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, GET_HASH_KEY(sClothesSelect.tlKeyWord), ENUM_TO_INT(SHOP_PED_COMPONENT)))
									IF NOT bDrawSkip OR iDrawable != iStartDraw
										bItemFound = TRUE
										BREAKLOOP
									ENDIF
								ENDIF
								iItemCount++
								
								IF (iItemCount % 50) = 0
									WAIT(0)
									IS_PED_INJURED(PLAYER_PED_ID())
									DRAW_DEBUG_CLOTHING_MENU(TRUE)
								ENDIF
							ENDFOR
							
							IF bItemFound
								BREAKLOOP
							ENDIF
						ENDFOR
						
						IF NOT bItemFound
							iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ePedComp)
							iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ePedComp)
							
							IF sClothesSelect.bUseCachedClothingData
								iDrawable = sClothesSelect.iLastIndex
								iTexture = sClothesSelect.iLastTexture
							ENDIF
						ENDIF
					ELSE
						IF iTexture > 0
						AND NOT bDrawSkip
						AND NOT bBlockSkip
							iTexture--
						ELIF iDrawable > 0
						OR bDrawSkip
						OR bBlockSkip
							IF bBlockSkip
								INT iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), ENUM_TO_INT(ePedComp), iDrawable)
								INT iDLCPackHash2
								BOOL bFoundItem = FALSE
								
								iDrawable--
								WHILE iDrawable > 0 AND NOT bFoundItem
									iDLCPackHash2 = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), ENUM_TO_INT(ePedComp), iDrawable)
									IF iDLCPackHash != iDLCPackHash2
										bFoundItem = TRUE
									ELSE
										iDrawable--
									ENDIF
								ENDWHILE
							ELSE
								iDrawable--
							ENDIF
							IF iDrawable < 0
								iDrawable = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
							ENDIF
							iTexture = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
							IF bDrawSkip
							OR bBlockSkip
								iTexture = 0
							ENDIF
						ELSE
							iDrawable = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
							iTexture = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
						ENDIF
					ENDIF
					
					PED_COMP_NAME_ENUM eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, eCompType)
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, eItem)
					IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND IS_CONTENT_ITEM_LOCKED(g_iLastDLCItemLockHash))
						// Skip.
					ELIF ePedComp = PED_COMP_BERD
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), eCompType, eItem, FALSE)
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ePedComp, iDrawable, iTexture)
					ENDIF
					
					sClothesSelect.iLastIndex = iDrawable
					sClothesSelect.iLastTexture = iTexture
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
					PED_PROP_POSITION ePropPos = INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					INT iDrawable = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos)
					INT iTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ePropPos)
					
					IF sClothesSelect.bUseCachedClothingData
						iDrawable = sClothesSelect.iLastIndex
						iTexture = sClothesSelect.iLastTexture
					ENDIF
					
					// Use keyword search
					IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
					
						INT iItemCount
						INT iStartDraw = iDrawable
						INT iStartTexture = iTexture-1
						BOOL bItemFound = FALSE
						
						FOR iDrawable = iStartDraw TO 0 STEP -1
						
							IF iDrawable != iStartDraw
								iStartTexture = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
							ENDIF
							
							FOR iTexture = iStartTexture TO 0 STEP -1
								g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, ePropPos))
								IF DOES_ITEM_LABEL_CONTAIN_STRING(g_sTempCompData[1].sLabel, sClothesSelect.tlKeyWord)
								OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND sClothesSelect.bKeyWordState = DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, GET_HASH_KEY(sClothesSelect.tlKeyWord), ENUM_TO_INT(SHOP_PED_PROP)))
									IF NOT bDrawSkip OR iDrawable != iStartDraw
										bItemFound = TRUE
										BREAKLOOP
									ENDIF
								ENDIF
								iItemCount++
								
								IF (iItemCount % 50) = 0
									WAIT(0)
									IS_PED_INJURED(PLAYER_PED_ID())
									DRAW_DEBUG_CLOTHING_MENU(TRUE)
								ENDIF
							ENDFOR
							
							IF bItemFound
								BREAKLOOP
							ENDIF
						ENDFOR
						
						IF NOT bItemFound
							iDrawable = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos)
							iTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ePropPos)
							
							IF sClothesSelect.bUseCachedClothingData
								iDrawable = sClothesSelect.iLastIndex
								iTexture = sClothesSelect.iLastTexture
							ENDIF
						ENDIF
					ELSE
						IF iTexture > 0
						AND NOT bDrawSkip
						AND NOT bBlockSkip
							iTexture--
						ELIF iDrawable > -1
							IF bBlockSkip
								INT iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), ePropPos, iDrawable)
								INT iDLCPackHash2
								BOOL bFoundItem = FALSE
								
								iDrawable--
								WHILE iDrawable > 0 AND NOT bFoundItem
									iDLCPackHash2 = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), ePropPos, iDrawable)
									IF iDLCPackHash != iDLCPackHash2
										bFoundItem = TRUE
									ELSE
										iDrawable--
									ENDIF
								ENDWHILE
							ELSE
								iDrawable--
							ENDIF
							IF iDrawable <= -1
								iDrawable = -1
								iTexture = -1
							ENDIF
							IF iDrawable >= 0
								iTexture = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
								IF bDrawSkip
								OR bBlockSkip
									iTexture = 0
								ENDIF
							ENDIF
						ELSE
							iDrawable = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos)-1
							iTexture = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
						ENDIF
					ENDIF
					IF iDrawable = -1
						CLEAR_PED_PROP(PLAYER_PED_ID(), ePropPos)
					ELSE
						PED_COMP_NAME_ENUM eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, COMP_TYPE_PROPS)
						g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, eItem)
						IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND IS_CONTENT_ITEM_LOCKED(g_iLastDLCItemLockHash))
							// Skip.
						ELSE
							SET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos, iDrawable, iTexture)
						ENDIF
					ENDIF
					
					sClothesSelect.iLastIndex = iDrawable
					sClothesSelect.iLastTexture = iTexture
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
				
					IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 0 // Compatible?
						sClothesSelect.bCompatibleOnly = !sClothesSelect.bCompatibleOnly
						sClothesSelect.iDecorationIndex = 0
						sClothesSelect.bRebuildMenu = TRUE
						sClothesSelect.bGrabClothingData = TRUE
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ELSE
						IF bDrawSkip
							// Get the previous collection
							
							TATTOO_FACTION_ENUM eFaction = TATTOO_MP_FM
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
								eFaction = TATTOO_MP_FM_F
							ENDIF
							
							INT iDLCIndex
							INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
							sTattooShopItemValues sDLCTattooData
							INT iTempDecIndex = sClothesSelect.iDecorationIndex
							INT iPreviousCollection = 0
							INT iDecCount = 0
							
							REPEAT iDLCCount iDLCIndex
								IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
									IF sDLCTattooData.UpdateGroup = HASH("torsoDecal")
										IF iPreviousCollection != sDLCTattooData.Collection
										AND sClothesSelect.iDecorationCollection != sDLCTattooData.Collection
										AND iDecCount < sClothesSelect.iDecorationIndex
											iTempDecIndex = iDecCount
											iPreviousCollection = sDLCTattooData.Collection
										ENDIF
										iDecCount++
									ENDIF
								ENDIF
							ENDREPEAT
							sClothesSelect.iDecorationIndex = iTempDecIndex
							
						ELSE
							sClothesSelect.iDecorationIndex--
							IF sClothesSelect.iDecorationIndex < 0
								sClothesSelect.iDecorationIndex = sClothesSelect.iDecorationCount-1
							ENDIF
						ENDIF
						
						sClothesSelect.bRebuildMenu = TRUE
						sClothesSelect.bGrabClothingData = TRUE
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
				
					IF bBlockSkip
						INT iDLCOutfitPack
						STRING strPack
						scrShopPedOutfit outfitItem
						BOOL bItemFound = FALSE
						
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
							sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(3, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
						ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
							sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(4, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
						ENDIF
						
						GET_SHOP_PED_QUERY_OUTFIT(sClothesSelect.iOutfitIndex, outfitItem)
						strPack = GET_SHOP_CONTENT_FOR_MENU(outfitItem.m_textLabel)
						IF NOT IS_STRING_NULL_OR_EMPTY(strPack)
							iDLCOutfitPack = GET_HASH_KEY(strPack)
						ENDIF
						
						WHILE sClothesSelect.iOutfitIndex > 0 AND NOT bItemFound
							sClothesSelect.iOutfitIndex--
							GET_SHOP_PED_QUERY_OUTFIT(sClothesSelect.iOutfitIndex, outfitItem)
							strPack = GET_SHOP_CONTENT_FOR_MENU(outfitItem.m_textLabel)
							IF NOT IS_STRING_NULL_OR_EMPTY(strPack)
								IF iDLCOutfitPack != GET_HASH_KEY(strPack)
									bItemFound = TRUE
								ENDIF
							ENDIF
						ENDWHILE
					ELSE
						sClothesSelect.iOutfitIndex--
					ENDIF
					
					sClothesSelect.iOutfitIndex--
					IF sClothesSelect.iOutfitIndex < 0
						sClothesSelect.iOutfitIndex = sClothesSelect.iOutfitCount-1
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
				
					IF sClothesSelect.eModelSelect = MP_M_FREEMODE_01
						sClothesSelect.eModelSelect = MP_F_FREEMODE_01
					ELSE
						sClothesSelect.eModelSelect = MP_M_FREEMODE_01
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
					
					sClothesSelect.iLocateSelect--
					IF sClothesSelect.iLocateSelect < 0
						sClothesSelect.iLocateSelect = MAX_SHOP_LOCATE_WARP_LOCATIONS-1
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
					
					sClothesSelect.iCameraSelect--
					IF sClothesSelect.iCameraSelect < 0
						sClothesSelect.iCameraSelect = MAX_CAMERA_SELECT_OPTIONS-1
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ENDIF
				
			ELIF bRight
				IF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_COMPS
				
					PED_COMPONENT ePedComp = INT_TO_ENUM(PED_COMPONENT, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					PED_COMP_TYPE_ENUM eCompType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					
					INT iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ePedComp)
					INT iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ePedComp)
					
					IF sClothesSelect.bUseCachedClothingData
						iDrawable = sClothesSelect.iLastIndex
						iTexture = sClothesSelect.iLastTexture
					ENDIF
					
					// Use keyword search
					IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
					
						INT iItemCount
						INT iStartDraw = iDrawable
						INT iStartTexture = iTexture+1
						BOOL bItemFound = FALSE
						
						FOR iDrawable = iStartDraw TO GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
						
							IF iDrawable != iStartDraw
								iStartTexture = 0
							ENDIF
							
							FOR iTexture = iStartTexture TO GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
								g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, eCompType))
								IF DOES_ITEM_LABEL_CONTAIN_STRING(g_sTempCompData[1].sLabel, sClothesSelect.tlKeyWord)
								OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND sClothesSelect.bKeyWordState = DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, GET_HASH_KEY(sClothesSelect.tlKeyWord), ENUM_TO_INT(SHOP_PED_COMPONENT)))
									IF NOT bDrawSkip OR iDrawable != iStartDraw
										bItemFound = TRUE
										BREAKLOOP
									ENDIF
								ENDIF
								iItemCount++
								
								IF (iItemCount % 50) = 0
									WAIT(0)
									IS_PED_INJURED(PLAYER_PED_ID())
									DRAW_DEBUG_CLOTHING_MENU(TRUE)
								ENDIF
							ENDFOR
							
							IF bItemFound
								BREAKLOOP
							ENDIF
						ENDFOR
						
						IF NOT bItemFound
							iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), ePedComp)
							iTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), ePedComp)
							
							IF sClothesSelect.bUseCachedClothingData
								iDrawable = sClothesSelect.iLastIndex
								iTexture = sClothesSelect.iLastTexture
							ENDIF
						ENDIF
					ELSE
						IF iTexture < GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
						AND NOT bDrawSkip
						AND NOT bBlockSkip
							iTexture++
						ELIF iDrawable < GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
						OR bDrawSkip
						OR bBlockSkip
							iTexture = 0
							IF bBlockSkip
								INT iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), ENUM_TO_INT(eCompType), iDrawable)
								INT iDLCPackHash2
								BOOL bFoundItem = FALSE
								
								iDrawable++
								WHILE iDrawable < GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp) AND NOT bFoundItem
									iDLCPackHash2 = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), ENUM_TO_INT(eCompType), iDrawable)
									IF iDLCPackHash != iDLCPackHash2
										bFoundItem = TRUE
									ELSE
										iDrawable++
									ENDIF
								ENDWHILE
							ELSE
								iDrawable++
							ENDIF
							IF iDrawable > GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
								iDrawable = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePedComp)-1
								iTexture = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePedComp, iDrawable)-1
							ENDIF
						ENDIF
					ENDIF
					
					PED_COMP_NAME_ENUM eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, eCompType)
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCompType, eItem)
					IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND IS_CONTENT_ITEM_LOCKED(g_iLastDLCItemLockHash))
						// Skip.
					ELIF ePedComp = PED_COMP_BERD
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), eCompType, eItem, FALSE)
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ePedComp, iDrawable, iTexture)
					ENDIF
					
					sClothesSelect.iLastIndex = iDrawable
					sClothesSelect.iLastTexture = iTexture
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_PROPS
				
					PED_PROP_POSITION ePropPos = INT_TO_ENUM(PED_PROP_POSITION, sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth])
					INT iDrawable = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos)
					INT iTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ePropPos)
					
					IF sClothesSelect.bUseCachedClothingData
						iDrawable = sClothesSelect.iLastIndex
						iTexture = sClothesSelect.iLastTexture
					ENDIF
					
					// Use keyword search
					IF NOT IS_STRING_NULL_OR_EMPTY(sClothesSelect.tlKeyWord)
					
						INT iItemCount
						INT iStartDraw = iDrawable
						INT iStartTexture = iTexture+1
						BOOL bItemFound = FALSE
						
						FOR iDrawable = iStartDraw TO GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos)-1
						
							IF iDrawable != iStartDraw
								iStartTexture = 0
							ENDIF
							
							FOR iTexture = iStartTexture TO GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
								g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, ePropPos))
								IF DOES_ITEM_LABEL_CONTAIN_STRING(g_sTempCompData[1].sLabel, sClothesSelect.tlKeyWord)
								OR (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND sClothesSelect.bKeyWordState = DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(g_iLastDLCItemNameHash, GET_HASH_KEY(sClothesSelect.tlKeyWord), ENUM_TO_INT(SHOP_PED_PROP)))
									IF NOT bDrawSkip OR iDrawable != iStartDraw
										bItemFound = TRUE
										BREAKLOOP
									ENDIF
								ENDIF
								iItemCount++
								
								IF (iItemCount % 50) = 0
									WAIT(0)
									IS_PED_INJURED(PLAYER_PED_ID())
									DRAW_DEBUG_CLOTHING_MENU(TRUE)
								ENDIF
							ENDFOR
							
							IF bItemFound
								BREAKLOOP
							ENDIF
						ENDFOR
						
						IF NOT bItemFound
							iDrawable = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos)
							iTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ePropPos)
							
							IF sClothesSelect.bUseCachedClothingData
								iDrawable = sClothesSelect.iLastIndex
								iTexture = sClothesSelect.iLastTexture
							ENDIF
						ENDIF
					ELSE
						IF iDrawable = -1
							IF GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos) > 0
								iDrawable = 0
								iTexture = 0
							ENDIF
						ELIF iTexture < GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
						AND NOT bDrawSkip
						AND NOT bBlockSkip
							iTexture++
						ELIF iDrawable < GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos)-1
						OR bDrawSkip
						OR bBlockSkip
							iTexture = 0
							IF bBlockSkip
								INT iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), ePropPos, iDrawable)
								INT iDLCPackHash2
								BOOL bFoundItem = FALSE
								
								iDrawable++
								WHILE iDrawable < GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos) AND NOT bFoundItem
									iDLCPackHash2 = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), ePropPos, iDrawable)
									IF iDLCPackHash != iDLCPackHash2
										bFoundItem = TRUE
									ELSE
										iDrawable++
									ENDIF
								ENDWHILE
							ELSE
								iDrawable++
							ENDIF
							IF iDrawable > GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos)-1
								iDrawable = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), ePropPos)-1
								iTexture = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ePropPos, iDrawable)-1
							ENDIF
						ENDIF
					ENDIF
					
					IF iDrawable = -1
						CLEAR_PED_PROP(PLAYER_PED_ID(), ePropPos)
					ELSE
						PED_COMP_NAME_ENUM eItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDrawable, iTexture, COMP_TYPE_PROPS)
						g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, eItem)
						IF (IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT) AND IS_CONTENT_ITEM_LOCKED(g_iLastDLCItemLockHash))
							// Skip.
						ELSE
							SET_PED_PROP_INDEX(PLAYER_PED_ID(), ePropPos, iDrawable, iTexture)
						ENDIF
					ENDIF
					
					sClothesSelect.iLastIndex = iDrawable
					sClothesSelect.iLastTexture = iTexture
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_DECORATIONS
					
					IF sClothesSelect.iCurrentItem[sClothesSelect.iMenuDepth] = 0 // Compatible?
						sClothesSelect.bCompatibleOnly = !sClothesSelect.bCompatibleOnly
						sClothesSelect.iDecorationIndex = 0
						sClothesSelect.bRebuildMenu = TRUE
						sClothesSelect.bGrabClothingData = TRUE
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ELSE
						IF bDrawSkip
							// Get the next collection
							
							TATTOO_FACTION_ENUM eFaction = TATTOO_MP_FM
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
								eFaction = TATTOO_MP_FM_F
							ENDIF
							
							INT iDLCIndex
							INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
							sTattooShopItemValues sDLCTattooData
							
							INT iPreviousCollection = sClothesSelect.iDecorationCollection
							INT iDecCount = 0
							BOOL bFoundCurrentCollection = FALSE
							
							REPEAT iDLCCount iDLCIndex
								IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
									IF sDLCTattooData.UpdateGroup = HASH("torsoDecal")
										IF sDLCTattooData.Collection = sClothesSelect.iDecorationCollection
											bFoundCurrentCollection = TRUE
										ENDIF
										IF iPreviousCollection != sDLCTattooData.Collection
										AND bFoundCurrentCollection
											sClothesSelect.iDecorationIndex = iDecCount
											iPreviousCollection = sDLCTattooData.Collection
											bFoundCurrentCollection = FALSE
										ENDIF
										iDecCount++
									ENDIF
								ENDIF
							ENDREPEAT
							
							IF sClothesSelect.iDecorationIndex >= sClothesSelect.iDecorationCount
								sClothesSelect.iDecorationIndex = sClothesSelect.iDecorationCount-1
							ENDIF
						ELSE
							sClothesSelect.iDecorationIndex++
							IF sClothesSelect.iDecorationIndex >= sClothesSelect.iDecorationCount
								sClothesSelect.iDecorationIndex = 0
							ENDIF
						ENDIF
						
						sClothesSelect.bRebuildMenu = TRUE
						sClothesSelect.bGrabClothingData = TRUE
						PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					ENDIF
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_OUTFITS
					
					IF bBlockSkip
						INT iDLCOutfitPack
						STRING strPack
						scrShopPedOutfit outfitItem
						BOOL bItemFound = FALSE
						
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
							sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(3, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
						ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
							sClothesSelect.iOutfitCount = SETUP_SHOP_PED_OUTFIT_QUERY(4, 0)//ENUM_TO_INT(CLO_SHOP_LOW))
						ENDIF
						
						GET_SHOP_PED_QUERY_OUTFIT(sClothesSelect.iOutfitIndex, outfitItem)
						strPack = GET_SHOP_CONTENT_FOR_MENU(outfitItem.m_textLabel)
						IF NOT IS_STRING_NULL_OR_EMPTY(strPack)
							iDLCOutfitPack = GET_HASH_KEY(strPack)
						ENDIF
						
						WHILE sClothesSelect.iOutfitIndex < sClothesSelect.iOutfitCount AND NOT bItemFound
							sClothesSelect.iOutfitIndex++
							GET_SHOP_PED_QUERY_OUTFIT(sClothesSelect.iOutfitIndex, outfitItem)
							strPack = GET_SHOP_CONTENT_FOR_MENU(outfitItem.m_textLabel)
							IF NOT IS_STRING_NULL_OR_EMPTY(strPack)
								IF iDLCOutfitPack != GET_HASH_KEY(strPack)
									bItemFound = TRUE
								ENDIF
							ENDIF
						ENDWHILE
					ELSE
						sClothesSelect.iOutfitIndex++
					ENDIF
					
					IF sClothesSelect.iOutfitIndex >= sClothesSelect.iOutfitCount
						sClothesSelect.iOutfitIndex = 0
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					sClothesSelect.bGrabClothingData = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_MODEL
				
					IF sClothesSelect.eModelSelect = MP_M_FREEMODE_01
						sClothesSelect.eModelSelect = MP_F_FREEMODE_01
					ELSE
						sClothesSelect.eModelSelect = MP_M_FREEMODE_01
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_SHOP_LOCATE
					
					sClothesSelect.iLocateSelect++
					IF sClothesSelect.iLocateSelect >= MAX_SHOP_LOCATE_WARP_LOCATIONS
						sClothesSelect.iLocateSelect = 0
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ELIF sClothesSelect.iCurrentMenu = CLOTHES_SELECT_MENU_CAMERA
					
					sClothesSelect.iCameraSelect++
					IF sClothesSelect.iCameraSelect >= MAX_CAMERA_SELECT_OPTIONS
						sClothesSelect.iCameraSelect = 0
					ENDIF
					
					sClothesSelect.bRebuildMenu = TRUE
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF sClothesSelect.bMenuAssetsRequested
			IF LOAD_MENU_ASSETS()
				CLEANUP_MENU_ASSETS()
				sClothesSelect.bMenuAssetsRequested = FALSE
			ENDIF
			CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, FALSE)
		ENDIF
		sClothesSelect.bMenuInitialised = FALSE
	ENDIF
ENDPROC

PROC DO_INITIALISE()
	START_WIDGET_GROUP("Clothing Debug")
	
		ADD_WIDGET_BOOL("Kill Script", bKillScript)
		ADD_WIDGET_BOOL("Display shop ped info", bDisplay_ShopPedInfo)
		
		START_WIDGET_GROUP("Ped Renders")
			ADD_WIDGET_BOOL("Generate Ped Renders", bGenerate_PedRenders)
			ADD_WIDGET_INT_READ_ONLY("Total Models", iTotalPedRenders)
			ADD_WIDGET_INT_READ_ONLY("Current Model", iCurrentPedRender)
			
			START_NEW_WIDGET_COMBO()
				INT iPack
				REPEAT DLC_PACK_MP_MAX iPack
					ADD_TO_WIDGET_COMBO(GET_DLC_PACK_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iPack)))
				ENDREPEAT
				ADD_TO_WIDGET_COMBO("ALL")
			STOP_WIDGET_COMBO("DLC Pack", iPedRenderDLCPack)
			iPedRenderDLCPack = ENUM_TO_INT(DLC_PACK_MP_MAX)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Camera Focus")
			ADD_WIDGET_BOOL("Activate", bUseOffsetCam)
			ADD_WIDGET_VECTOR_SLIDER("Position Offset", vCamOffsetPos, -10, 10, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Point Offset", vCamOffsetPoint, -10, 10, 0.01)
			
			ADD_WIDGET_BOOL("Preset: Head", bSetOffsetCamPreset[0])
			ADD_WIDGET_BOOL("Preset: Head Rear", bSetOffsetCamPreset[1])
			ADD_WIDGET_BOOL("Preset: Torso", bSetOffsetCamPreset[2])
			ADD_WIDGET_BOOL("Preset: Torso Rear", bSetOffsetCamPreset[3])
			ADD_WIDGET_BOOL("Preset: Legs", bSetOffsetCamPreset[4])
			ADD_WIDGET_BOOL("Preset: Legs Rear", bSetOffsetCamPreset[5])
			ADD_WIDGET_BOOL("Preset: Feet", bSetOffsetCamPreset[6])
			ADD_WIDGET_BOOL("Preset: Feet Rear", bSetOffsetCamPreset[7])
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Player")
			ADD_WIDGET_BOOL("MP_M_FREEMODE_01", bSetPlayerModel_Male)
			ADD_WIDGET_BOOL("MP_F_FREEMODE_01", bSetPlayerModel_Female)
			ADD_WIDGET_BOOL("Randomise appearance", b_debug_ped_randomise_appearance)
			ADD_WIDGET_BOOL("Create clone", b_debug_ped_create_clone)
		STOP_WIDGET_GROUP()
		
		//////////////////////////////////////////////////////////////////////////////
		///      
		///      [SHOP META]
		///      
		//////////////////////////////////////////////////////////////////////////////
		START_WIDGET_GROUP("Shop Meta")
			
			START_NEW_WIDGET_COMBO()
				REPEAT DLC_PACK_MP_MAX iPack
					ADD_TO_WIDGET_COMBO(GET_DLC_PACK_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iPack)))
				ENDREPEAT
				ADD_TO_WIDGET_COMBO("ALL")
			STOP_WIDGET_COMBO("DLC Pack", iSelectedDLCPack)
			iSelectedDLCPack = ENUM_TO_INT(DLC_PACK_MP_MAX)-1
			
			ADD_WIDGET_BOOL("Output uniqueNameHash", bOutputUniqueHashNames)
			ADD_WIDGET_BOOL("Output locate/menu names", bOutputShopLocateNames)
			ADD_WIDGET_BOOL("Generate shop meta", bGenerate_ShopMeta)
			ADD_WIDGET_BOOL("Generate missing shop meta", bGenerate_MissingShopMeta)
			ADD_WIDGET_BOOL("Generate missing save data items", bGenerate_MissingSaveItems)
			
		STOP_WIDGET_GROUP()
		
		//////////////////////////////////////////////////////////////////////////////
		///      
		///      [SCRIPT LOOKUPS]
		///      
		//////////////////////////////////////////////////////////////////////////////
		START_WIDGET_GROUP("Script Lookups")
		
			START_NEW_WIDGET_COMBO()
				REPEAT DLC_PACK_MP_MAX iPack
					ADD_TO_WIDGET_COMBO(GET_DLC_PACK_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iPack)))
				ENDREPEAT
				ADD_TO_WIDGET_COMBO("ALL")
			STOP_WIDGET_COMBO("DLC Pack", iSelectedDLCPack)
			
			//			-- dlc_ped_component_names.sch :: GET_DLC_NAME_HASH_STRING
			ADD_WIDGET_BOOL("GET_DLC_NAME_HASH_STRING", bGenerate_GET_DLC_NAME_HASH_STRING)
			
			//			-- dlc_ped_component_enum_lookup.sch :: GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP
			ADD_WIDGET_BOOL("GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP", bGenerate_GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP)
			
			//			-- tattoo_private.sch :: GET_TATTOO_ENUM_FROM_DLC_HASH
			ADD_WIDGET_BOOL("GET_TATTOO_ENUM_FROM_DLC_HASH", bGenerate_GET_TATTOO_ENUM_FROM_DLC_HASH)
			
			//			-- scriptMetaData.meta
			// TODO
		STOP_WIDGET_GROUP()
		
		//////////////////////////////////////////////////////////////////////////////
		///      
		///      [MENU AND ITEM INFO]
		///      
		//////////////////////////////////////////////////////////////////////////////
		START_WIDGET_GROUP("Item Info")
			
			START_NEW_WIDGET_COMBO()
				REPEAT DLC_PACK_MP_MAX iPack
					ADD_TO_WIDGET_COMBO(GET_DLC_PACK_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iPack)))
				ENDREPEAT
				ADD_TO_WIDGET_COMBO("ALL")
			STOP_WIDGET_COMBO("DLC Pack", iSelectedDLCPack)
			
			START_NEW_WIDGET_COMBO()
				INT iComp
				REPEAT NUMBER_OF_PED_COMP_TYPES iComp
					IF iComp = ENUM_TO_INT(COMP_TYPE_OUTFIT)
					OR iComp = ENUM_TO_INT(COMP_TYPE_PROPGROUP)
						ADD_TO_WIDGET_COMBO("NA")
					ELSE
						ADD_TO_WIDGET_COMBO(GET_PED_COMP_TYPE_STRING(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp)))
					ENDIF
				ENDREPEAT
				ADD_TO_WIDGET_COMBO("ALL")
			STOP_WIDGET_COMBO("Comp Type", iSelectedPedComponent)
			iSelectedPedComponent = NUMBER_OF_PED_COMP_TYPES
			
			//			-- Item data + variants + overlays
			ADD_WIDGET_BOOL("Output item info", bGenerate_ClothingInfo)
			
			//			-- Screenshots
			ADD_WIDGET_BOOL("Generate item screenshot", bGenerate_ClothingScreenshots)
			ADD_WIDGET_BOOL("Generate item screenshot (tex=0)", bGenerate_ClothingScreenshotsPrimary)
			
			//			-- Exclusive Items
			ADD_WIDGET_BOOL("Output Exclsuive Clothes", bGenerate_ExclsuiveClothingInfo)
		STOP_WIDGET_GROUP()
		
		//////////////////////////////////////////////////////////////////////////////
		///      
		///      [OUTFITS]
		///      
		//////////////////////////////////////////////////////////////////////////////
		START_WIDGET_GROUP("XML Outfits")
			twDebugPedNamed = ADD_TEXT_WIDGET("Named slot") SET_CONTENTS_OF_TEXT_WIDGET(twDebugPedNamed, "")
			ADD_WIDGET_BOOL("Output script and meta", b_debug_output_ped_save_data)
			
			INT i
			TEXT_LABEL_31 tlName
			
			START_WIDGET_GROUP("Save")
				REPEAT COUNT_OF(b_debug_ped_save) i
					tlName = "Save ped to slot "
					tlName += (i+1)
					
					IF i = COUNT_OF(b_debug_ped_save)-1
						tlName += " (named)"
					ENDIF
					
					ADD_WIDGET_BOOL(tlName, b_debug_ped_save[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Apply")
				REPEAT COUNT_OF(b_debug_ped_set) i
					tlName = "Apply ped in slot "
					tlName += (i+1)
					
					IF i = COUNT_OF(b_debug_ped_set)-1
						tlName += " (named)"
					ENDIF
					
					ADD_WIDGET_BOOL(tlName, b_debug_ped_set[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Create")
				REPEAT COUNT_OF(b_debug_ped_create) i
					tlName = "Create ped in slot "
					tlName += (i+1)
					
					IF i = COUNT_OF(b_debug_ped_create)-1
						tlName += " (named)"
					ENDIF
					
					ADD_WIDGET_BOOL(tlName, b_debug_ped_create[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Script Outfits")
			
			ADD_WIDGET_BOOL("Position (base)", b_debug_set_player_and_cam[0])
			ADD_WIDGET_BOOL("Position (heels)", b_debug_set_player_and_cam[1])
			ADD_WIDGET_BOOL("Position (hat+heels)", b_debug_set_player_and_cam[2])
			ADD_WIDGET_BOOL("Position (mask)", b_debug_set_player_and_cam[3])
			
			ADD_WIDGET_BOOL("Output outfit setup (heist)", b_debug_outfit_output_1)
			ADD_WIDGET_BOOL("Output outfit setup (meta)", b_debug_outfit_output_2)
			
			ADD_WIDGET_BOOL("Validate Clothes", g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK)
			
			ADD_WIDGET_BOOL("Apply Freemode outfit", b_Apply_Freemode_outfit)
			
			START_WIDGET_GROUP("Heist Clothing")
			
				ADD_WIDGET_INT_SLIDER("Heist outfit", i_debug_heist_outfit, 0, ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1, 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_heist_outfit)
				ADD_WIDGET_BOOL("Set outfit automatically", b_debug_set_heist_outfit_auto)
				t_debug_heist_outfit_name = ADD_TEXT_WIDGET("Outfit")
				SET_CONTENTS_OF_TEXT_WIDGET(t_debug_heist_outfit_name, "null")
				
				ADD_WIDGET_INT_SLIDER("Heist mask", i_debug_heist_mask, 0, ENUM_TO_INT(OUTFIT_MAX_MASKS)-1, 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_heist_mask)
				ADD_WIDGET_BOOL("Set outfit automatically", b_debug_set_heist_mask_auto)
				t_debug_heist_mask_name = ADD_TEXT_WIDGET("Mask")
				SET_CONTENTS_OF_TEXT_WIDGET(t_debug_heist_mask_name, "null")
				
				ADD_WIDGET_INT_SLIDER("Heist gear", i_debug_heist_gear, 0, ENUM_TO_INT(GEAR_MAX_AMOUNT)-1, 1)
				ADD_WIDGET_BOOL("Set gear", b_debug_set_heist_gear)
				ADD_WIDGET_BOOL("Remove gear", b_debug_remove_heist_gear)
				ADD_WIDGET_BOOL("Output hash data", b_debug_output_heist_DLC_hash)
				ADD_WIDGET_BOOL("Allow Heist content", g_bDebugAllowHeistItems)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Vs classics Clothing")
				ADD_WIDGET_INT_SLIDER("Vs outfit(starts at OUTFIT_VERSUS_CLASSIC_JUNGLE_0):", i_debug_Vs_outfit,ENUM_TO_INT(OUTFIT_VERSUS_CLASSIC_JUNGLE_0),ENUM_TO_INT(OUTFIT_VERSUS_CLASSIC_WHITE_TUX_0) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_Vs_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Vs Themed Clothing")
				ADD_WIDGET_INT_SLIDER("Vs outfit(starts at OUTFIT_VERSUS_THEMED_SLASHERS_0):", i_debug_VsTheme_outfit,ENUM_TO_INT(OUTFIT_VERSUS_THEMED_SLASHERS_0),ENUM_TO_INT(OUTFIT_VERSUS_CLASSIC_COWBOYS_5) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_VsTheme_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("NG Clothing")
				ADD_WIDGET_INT_SLIDER("ng outfit(starts at OUTFIT_NG_CASUAL_0):", i_debug_ng_outfit,ENUM_TO_INT(OUTFIT_NG_CASUAL_0),ENUM_TO_INT(OUTFIT_NG_ECCENTRIC_7) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_ng_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Lowrider Vs Clothing")
				ADD_WIDGET_INT_SLIDER("Lowrider Vs outfit:", i_debug_lowr_outfit,ENUM_TO_INT(OUTFIT_VERSUS_LOWR_BLUE_SMART_0),ENUM_TO_INT(OUTFIT_VERSUS_LOWR_RED_STREET_5) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_lowr_outfit)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Entourage Vs Clothing")
				ADD_WIDGET_INT_SLIDER("Entourage Vs outfit:", i_debug_entourage_outfit,ENUM_TO_INT(OUTFIT_VERSUS_ENTOURAGE_VIP_0),ENUM_TO_INT(OUTFIT_VERSUS_ENTOURAGE_ATTACKER_3) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_entourage_outfit)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Extraction Vs Clothing")
				ADD_WIDGET_INT_SLIDER("Extraction Vs outfit:", i_debug_extraction_outfit,ENUM_TO_INT(OUTFIT_VERSUS_EXTRACTION_VIP_0),ENUM_TO_INT(OUTFIT_VERSUS_EXTRACTION_VIP_0) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_extraction_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Halloween Vs Clothing")
				ADD_WIDGET_INT_SLIDER("Halloween Vs outfit:", i_debug_hal_outfit,ENUM_TO_INT(OUTFIT_VERSUS_HAL_MANIACS_0),ENUM_TO_INT(OUTFIT_VERSUS_HAL_PIMP_WITCHES_5) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_hal_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Solo Vs Clothing")
				ADD_WIDGET_INT_SLIDER("Solo Vs outfit:", i_debug_solo_outfit,ENUM_TO_INT(OUTFIT_LOW_FLOW_WOLVES_0),ENUM_TO_INT(OUTFIT_LOW_FLOW_STRIPES_0) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_solo_outfit)
				ADD_WIDGET_INT_SLIDER("Solo Vs outfit 2:", i_debug_solo_outfit2,ENUM_TO_INT(OUTFIT_SOLO_SENIORS_0),ENUM_TO_INT(OUTFIT_SOLO_MONSTER_MILITIA_0) , 1)
				ADD_WIDGET_BOOL("Set outfit2", b_debug_set_solo_outfit2)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Lowrider Flow Clothing")
				ADD_WIDGET_INT_SLIDER("Lowrider Flow outfit:", i_debug_low_flow_outfit,ENUM_TO_INT(OUTFIT_LOW_FLOW_LOWRIDER_0),ENUM_TO_INT(OUTFIT_LOW_FLOW_FUNERAL_3) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_low_flow_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Beast Clothing")
				ADD_WIDGET_INT_SLIDER("Beast outfit:", i_debug_beast_outfit,ENUM_TO_INT(OUTFIT_ADVERSARY_B_OVERRIDE_0),ENUM_TO_INT(OUTFIT_VERSUS_CLASSIC_METAL_5) , 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_beast_outfit)
			STOP_WIDGET_GROUP()
						
			START_WIDGET_GROUP("Hunt the Beast")
				ADD_WIDGET_BOOL("Set outfit", b_debug_htb)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Magnate Gang Clothing")
				ADD_WIDGET_INT_SLIDER("Gang/Boss outfit:", i_debug_gang_boss_vip_outfits, ENUM_TO_INT(OUTFIT_MAGNATE_BOSS_BARON_0), ENUM_TO_INT(OUTFIT_MAGNATE_BOSS_SYNDICATE_0), 1)
				ADD_WIDGET_BOOL("Set VIP outfit", b_debug_set_gang_boss_vip_outfits)
				
				ADD_WIDGET_INT_SLIDER("Bodyguard outfit:", i_debug_gang_boss_bodyguard_outfits, ENUM_TO_INT(OUTFIT_MAGNATE_GOON_NARCO_0), ENUM_TO_INT(OUTFIT_MAGNATE_GOON_HITMEN_3), 1)
				ADD_WIDGET_BOOL("Set Bodyguard outfit", b_debug_set_gang_boss_bodyguard_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Hidden Clothing (Jan16)")
				ADD_WIDGET_INT_SLIDER("Hidden outfit:", i_debug_hidden_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_VINTAGE_GREEN_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_DROP_PURPLE_0), 1)
				tw_debug_hidden_outfit_name = ADD_TEXT_WIDGET("Outfit Name")
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_hidden_outfit_name, GET_MP_OUTFIT_NAME_FROM_ENUM(INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_hidden_outfits)))
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_hidden_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Hidden Lowrider2 Clothing (Lowrider2)")
				ADD_WIDGET_INT_SLIDER("Hidden Lowrider2 outfit:", i_debug_hidden_lowrider_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_LOWRIDER_GREEN_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_LOWRIDER_PINK_3), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_hidden_lowrider_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("VS Classic - Competitors Clothing (Lowrider2)")
				ADD_WIDGET_INT_SLIDER("VS Classic - Competitors outfit:", i_debug_classic_competitors_outfits, ENUM_TO_INT(OUTFIT_VERSUS_LOWR2_BROWN_SPORT_0), ENUM_TO_INT(OUTFIT_VERSUS_LOWR2_WHITE_SPORT_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_classic_competitors_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Trading Places Winner Clothing")
				ADD_WIDGET_INT_SLIDER("Winner outfit:", i_debug_trading_places_winner_outfits, ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_WINNER_0), ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_WINNER_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_trading_places_winner_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Trading Places Loser Clothing")
				ADD_WIDGET_INT_SLIDER("Loser outfit:", i_debug_trading_places_loser_outfits, ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_LOSER_0), ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_LOSER_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_trading_places_loser_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Power Play Team Sport Clothing")
				ADD_WIDGET_INT_SLIDER("Team Sport outfit: ", i_debug_power_play_team_sport_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_SPORT_GREEN_0), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_power_play_team_sport_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Executive CEO/Associate Clothing")
				ADD_WIDGET_INT_SLIDER("Executive CEO outfit:", i_debug_executive_ceo_outfits, ENUM_TO_INT(OUTFIT_EXEC_CEO_FOUNDER_0), ENUM_TO_INT(OUTFIT_EXEC_CEO_WARLORD_0), 1)
				ADD_WIDGET_BOOL("Set CEO outfit", b_debug_set_executive_ceo_outfits)
				
				ADD_WIDGET_INT_SLIDER("Executive Associate outfit:", i_debug_executive_associate_outfits, ENUM_TO_INT(OUTFIT_EXEC_ASSOCIATE_OPERATORS_0), ENUM_TO_INT(OUTFIT_EXEC_ASSOCIATE_RUNNERS_2), 1)
				ADD_WIDGET_BOOL("Set Associate outfit", b_debug_set_executive_associate_outfits)
				
				ADD_WIDGET_INT_SLIDER("Executive Securoserv outfit:", i_debug_executive_securoserv_outfits, ENUM_TO_INT(OUTFIT_MAGNATE_GOON_SECUROSERV_0), ENUM_TO_INT(OUTFIT_MAGNATE_GOON_SECUROSERV_3), 1)
				ADD_WIDGET_BOOL("Set Securoserv outfit", b_debug_set_executive_securoserv_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Power Play Themed Clothing")
				ADD_WIDGET_INT_SLIDER("Power Play Themed outfit:", i_debug_power_play_themed_outfits, ENUM_TO_INT(OUTFIT_VERSUS_THEMED_EXEC_PURPLE_BUGS_0), ENUM_TO_INT(OUTFIT_VERSUS_THEMED_EXEC_COOKIES_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_power_play_themed_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Stunt Pack Race Clothing")
				ADD_WIDGET_INT_SLIDER("Stunt Race Outfit:", i_debug_stunt_race_outfits, ENUM_TO_INT(OUTFIT_STUNT_RACE_BIKER_0), ENUM_TO_INT(OUTFIT_STUNT_RACE_MOTO_1), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_stunt_race_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Biker Club Clothing")
				ADD_WIDGET_INT_SLIDER("Biker club outfits (0-3): ", i_debug_biker_club_outfits_0to3, ENUM_TO_INT(OUTFIT_BIKER_DIRT_0), ENUM_TO_INT(OUTFIT_BIKER_RAZOR_3), 1)
				ADD_WIDGET_BOOL("Set outfit (0-3)", b_debug_set_biker_club_outfits_0to3)
				
				ADD_WIDGET_INT_SLIDER("Biker club outfits (4-7): ", i_debug_biker_club_outfits_4to7, ENUM_TO_INT(OUTFIT_BIKER_DIRT_4), ENUM_TO_INT(OUTFIT_BIKER_RAZOR_7), 1)
				ADD_WIDGET_BOOL("Set outfit (4-7)", b_debug_set_biker_club_outfits_4to7)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Deadline Hidden Clothing")
				ADD_WIDGET_INT_SLIDER("Deadline hiddne outfits): ", i_debug_biker_deadline_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_DEADLINE_PURPLE_0), ENUM_TO_INT(OUTFIT_HIDDEN_DEADLINE_GREEN_0), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_biker_deadline_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Slipstream Hidden Clothing")
				ADD_WIDGET_INT_SLIDER("Slipstream hiddne outfits): ", i_debug_biker_slipstream_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_TEAM_STUNT_BIKER_PURPLE_0), ENUM_TO_INT(OUTFIT_HIDDEN_TEAM_STUNT_BIKER_GREEN_0), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_biker_slipstream_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Lost and Damned Hidden Clothing")
				ADD_WIDGET_INT_SLIDER("Lost and Damned hiddne outfits): ", i_debug_biker_lost_damned_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_LOST_DEVILS_0), ENUM_TO_INT(OUTFIT_HIDDEN_LOST_ANGELS_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_biker_lost_damned_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Biker Club Rank Clothing")
				ADD_WIDGET_INT_SLIDER("Biker club rank outfits): ", i_debug_biker_club_rank_outfits, ENUM_TO_INT(OUTFIT_BIKER_RANK_0), ENUM_TO_INT(OUTFIT_BIKER_RANK_7), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_biker_club_rank_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Imp/Exp CEO Clothing")
				ADD_WIDGET_INT_SLIDER("Imp/Exp CEO outfits: ", i_debug_impexp_ceo_outfits, ENUM_TO_INT(OUTFIT_IE_CEO_LONGLINE_0), ENUM_TO_INT(OUTFIT_IE_CEO_DEMON_0), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_impexp_ceo_outfits)
				
				ADD_WIDGET_INT_SLIDER("Imp/Exp Associate outfits: ", i_debug_impexp_associate_outfits, ENUM_TO_INT(OUTFIT_IE_LONGLINE_0), ENUM_TO_INT(OUTFIT_IE_DEMON_2), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_impexp_associate_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Juggernaut Outfits")
				ADD_WIDGET_INT_SLIDER("Imp/Exp Juggernaut Standard outfits: ", i_debug_impexp_juggernaut_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0), ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_ATTACKER_5), 1)
				ADD_WIDGET_BOOL("Set Standard outfit", b_debug_set_impexp_juggernaut_outfits)
				
				ADD_WIDGET_INT_SLIDER("Imp/Exp Juggernaut Orange outfits: ", i_debug_impexp_juggernaut_orange_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0), ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_ORANGE_ATTACKER_5), 1)
				ADD_WIDGET_BOOL("Set Orange outfit", b_debug_set_impexp_juggernaut_orange_outfits)
				
				ADD_WIDGET_INT_SLIDER("Imp/Exp Juggernaut Purple outfits: ", i_debug_impexp_juggernaut_purple_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0), ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PURPLE_ATTACKER_5), 1)
				ADD_WIDGET_BOOL("Set Purple outfit", b_debug_set_impexp_juggernaut_purple_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Special Races - General Combat Outfits")
				ADD_WIDGET_INT_SLIDER("Spec races General Combat outfits: ", i_debug_specraces_general_combat_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_GENCOMBAT_FOREST_0), ENUM_TO_INT(OUTFIT_HIDDEN_GENCOMBAT_URBAN_5), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_specraces_general_combat_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Special Races - Land Grab Outfits")
				ADD_WIDGET_INT_SLIDER("Spec races Land Grab outfits: ", i_debug_specraces_land_grab_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_SR_LG_ORANGE_0), ENUM_TO_INT(OUTFIT_HIDDEN_SR_LG_GREEN_0), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_specraces_land_grab_outfits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gunrunning Outfits")
				START_WIDGET_GROUP("Gunrunning - Lions Den Outfits")
					ADD_WIDGET_INT_SLIDER("Lions den outfit: ", i_debug_gunrun_lions_den_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_LIONSDEN_ATTACKER_0), ENUM_TO_INT(OUTFIT_HIDDEN_LIONSDEN_DEFENDER_5), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_lions_den_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - Dawn Raid Outfits")
					ADD_WIDGET_INT_SLIDER("Dawn raid outfit: ", i_debug_gunrun_dawn_raid_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_DAWNRAID_ORANGE_0), ENUM_TO_INT(OUTFIT_HIDDEN_DAWNRAID_PURPLE_5), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_dawn_raid_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - Team General Outfits")
					ADD_WIDGET_INT_SLIDER("Dawn raid outfit: ", i_debug_gunrun_team_general_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_GENERAL_ORANGE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TEAM_GENERAL_GREEN_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_team_general_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - Power Mad Outfits")
					ADD_WIDGET_INT_SLIDER("Juggernaut outfit: ", i_debug_gunrun_power_mad_juggernaut_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_PINK_TARGET_0), ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN4_GREEN_TARGET_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_power_mad_juggernaut_outfits)
					
					ADD_WIDGET_INT_SLIDER("Team colour outfit: ", i_debug_gunrun_power_mad_team_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GR_POWERMAD_ORANGE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GR_POWERMAD_GREEN_1), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_power_mad_team_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - MC Outfits")
					ADD_WIDGET_INT_SLIDER("Zombie outfit: ", i_debug_gunrun_biker_zombie_outfits, ENUM_TO_INT(OUTFIT_BIKER_ZOMBIE_0), ENUM_TO_INT(OUTFIT_BIKER_ZOMBIE_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_biker_zombie_outfits)
					
					ADD_WIDGET_INT_SLIDER("Raider outfit: ", i_debug_gunrun_biker_raider_outfits, ENUM_TO_INT(OUTFIT_BIKER_RAIDER_0), ENUM_TO_INT(OUTFIT_BIKER_RAIDER_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_biker_raider_outfits)
					
					ADD_WIDGET_INT_SLIDER("Puffer outfit: ", i_debug_gunrun_biker_puffer_outfits, ENUM_TO_INT(OUTFIT_BIKER_PUFFER_0), ENUM_TO_INT(OUTFIT_BIKER_PUFFER_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_biker_puffer_outfits)
					
					ADD_WIDGET_INT_SLIDER("Hillbilly outfit: ", i_debug_gunrun_biker_hillbilly_outfits, ENUM_TO_INT(OUTFIT_BIKER_HILLBILLY_0), ENUM_TO_INT(OUTFIT_BIKER_HILLBILLY_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_biker_hillbilly_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - CEO/Associate Outfits")
					ADD_WIDGET_INT_SLIDER("CEO outfit: ", i_debug_gunrun_ceo_outfits, ENUM_TO_INT(OUTFIT_GUNR_CEO_SURVIVALIST_0), ENUM_TO_INT(OUTFIT_GUNR_CEO_GANGMASTER_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_ceo_outfits)
					
					ADD_WIDGET_INT_SLIDER("Associate outfit: ", i_debug_gunrun_associate_outfits, ENUM_TO_INT(OUTFIT_GUNR_ASSOCIATE_SURVIVALIST_0), ENUM_TO_INT(OUTFIT_GUNR_ASSOCIATE_GANGMASTER_2), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_associate_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Gunrunning - WVM Oppressor Outfits")
					ADD_WIDGET_INT_SLIDER("Oppressor outfit: ", i_debug_gunrun_wvm_oppressor_outfits, ENUM_TO_INT(OUTFIT_WVM_OPPRESSOR_0), ENUM_TO_INT(OUTFIT_WVM_OPPRESSOR_3), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gunrun_wvm_oppressor_outfits)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Smuggler Outfits")
				START_WIDGET_GROUP("Smuggler - Hostile Takeover Outfits")
					ADD_WIDGET_INT_SLIDER("Hostile takeover outfit: ", i_debug_smuggler_hostile_takeover_outfits, ENUM_TO_INT(OUTFIT_HIDDEN_HOSTILE_TAKEOVER_GREEN_0), ENUM_TO_INT(OUTFIT_HIDDEN_HOSTILE_TAKEOVER_PINK_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_hostile_takeover_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Condemned Outfits")
					ADD_WIDGET_INT_SLIDER("Condemned outfit: ", i_debug_smuggler_condemned_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_CND_CONDEMNED), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_CND_3), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_condemned_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Vehicle Warfare Outfits")
					ADD_WIDGET_INT_SLIDER("Vehicle Warfare outfit: ", i_debug_smuggler_vehicle_warfare_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_VEHICLE_WARFARE_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_VEHICLE_WARFARE_GREEN_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_vehicle_warfare_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Air Shootout Outfits")
					ADD_WIDGET_INT_SLIDER("Air Shootout outfit: ", i_debug_smuggler_air_shootout_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_AIR_SHOOTOUT_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_AIR_SHOOTOUT_GREEN_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_air_shootout_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Bombushka Run Outfits")
					ADD_WIDGET_INT_SLIDER("Bombushka Run outfit: ", i_debug_smuggler_bombushka_run_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_BOMBUSHKA_RUN_BLACK_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_BOMBUSHKA_RUN_WHITE_3), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_bombushka_run_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Stockpile Outfits")
					ADD_WIDGET_INT_SLIDER("Stockpile outfit: ", i_debug_smuggler_stockpile_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_STOCKPILE_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SM_STOCKPILE_GREEN_0), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_stockpile_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Smuggler - Race Outfits")
					ADD_WIDGET_INT_SLIDER("Race outfit: ", i_debug_smuggler_race_outfits, ENUM_TO_INT(OUTFIT_SMUGGLER_RACE_SUIT_FLOW), ENUM_TO_INT(OUTFIT_SMUGGLER_RACE_SUIT_TINKLE), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_smuggler_race_outfits)
				STOP_WIDGET_GROUP()
				
				// Gang outfits
				
				START_WIDGET_GROUP("Organisation Outfits")
					ADD_WIDGET_INT_SLIDER("CEO outfit: ", i_debug_smuggler_ceo_outfits, ENUM_TO_INT(OUTFIT_SMUGGLER_CEO_FORMAL_PILOT_0), ENUM_TO_INT(OUTFIT_SMUGGLER_CEO_TACTICAL_STEALTH_0), 1)
					ADD_WIDGET_BOOL("Set CEO outfit", b_debug_set_smuggler_ceo_outfits)

					ADD_WIDGET_INT_SLIDER("Associate outfit: ", i_debug_smuggler_associate_outfits, ENUM_TO_INT(OUTFIT_SMUGGLER_ASSOCIATE_FORMAL_PILOT_0), ENUM_TO_INT(OUTFIT_SMUGGLER_ASSOCIATE_TACTICAL_STEALTH_2), 1)
					ADD_WIDGET_BOOL("Set Associate outfit", b_debug_set_smuggler_associate_outfits)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MC Outfits")
					ADD_WIDGET_INT_SLIDER("Casual Pilot outfit: ", i_debug_smuggler_biker_casual_pilot_outfits, ENUM_TO_INT(OUTFIT_BIKER_CASUAL_PILOT_0), ENUM_TO_INT(OUTFIT_BIKER_CASUAL_PILOT_7), 1)
					ADD_WIDGET_BOOL("Set casual pilot outfit", b_debug_set_smuggler_biker_casual_pilot_outfits)
					
					ADD_WIDGET_INT_SLIDER("Pirate outfit: ", i_debug_smuggler_biker_pirate_outfits, ENUM_TO_INT(OUTFIT_BIKER_PIRATE_0), ENUM_TO_INT(OUTFIT_BIKER_PIRATE_7), 1)
					ADD_WIDGET_BOOL("Set casual pilot outfit", b_debug_set_smuggler_biker_pirate_outfits)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Gang Ops Outfits")
				START_WIDGET_GROUP("Under Control Outfit")
					ADD_WIDGET_INT_SLIDER("Outfit: ", i_debug_gangops_under_control_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_UNDER_CONTROL), ENUM_TO_INT(OUTFIT_GANGOPS_UNDER_CONTROL), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gangops_under_control_outfit)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Slashers Outfit")
					ADD_WIDGET_INT_SLIDER("Outfit: ", i_debug_gangops_slashers_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_SL_CLOWN_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_SL_GRNBRWN_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gangops_slashers_outfit)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Hard Target Outfit")
					ADD_WIDGET_INT_SLIDER("Outfit: ", i_debug_gangops_hard_target_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_HT_BLACK_TARGET_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_GO_HT_GREEN_7), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gangops_hard_target_outfit)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Air Quota Outfit")
					ADD_WIDGET_INT_SLIDER("Outfit: ", i_debug_gangops_air_quota_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_AIR_QUOTA_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_AIR_QUOTA_GREEN_3), 1)
					ADD_WIDGET_BOOL("Set outfit", b_debug_set_gangops_air_quota_outfit)
				STOP_WIDGET_GROUP()
				
				// Heist outfits
				
				START_WIDGET_GROUP("Gang Ops Heist Outfits")
					ADD_WIDGET_INT_SLIDER("Scuba Outfit: ", i_debug_gangops_heist_scuba_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA), 1)
					ADD_WIDGET_BOOL("Set scuba outfit", b_debug_set_gangops_heist_scuba_outfit)

					ADD_WIDGET_INT_SLIDER("Scuba (no tank) Outfit: ", i_debug_gangops_heist_scuba2_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA_NOTANK), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SCUBA_NOTANK), 1)
					ADD_WIDGET_BOOL("Set scuba (no tank) outfit", b_debug_set_gangops_heist_scuba2_outfit)

					ADD_WIDGET_INT_SLIDER("Paramedic Outfits: ", i_debug_gangops_heist_paramedic_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_PARAMEDIC_BLUE_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_PARAMEDIC_GREEN_1), 1)
					ADD_WIDGET_BOOL("Set paramedic outfit", b_debug_set_gangops_heist_paramedic_outfit)
					
					ADD_WIDGET_INT_SLIDER("Medium Tech Combat 1 Outfits: ", i_debug_gangops_heist_medtech1_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT1_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT1_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Combat 1 outfit", b_debug_set_gangops_heist_medtech1_outfit)
					
					ADD_WIDGET_INT_SLIDER("Medium Tech Combat 2 Outfits: ", i_debug_gangops_heist_medtech2_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT2_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MEDTECH_COMBAT2_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Combat 2 outfit", b_debug_set_gangops_heist_medtech2_outfit)
					
					ADD_WIDGET_INT_SLIDER("Modern Stealth Outfits: ", i_debug_gangops_heist_modern_stealth_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MODERN_STEALTH_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MODERN_STEALTH_3), 1)
					ADD_WIDGET_BOOL("Set Modern Stealth outfit", b_debug_set_gangops_heist_modern_stealth_outfit)
					
					ADD_WIDGET_INT_SLIDER("Casual Pilot Outfits: ", i_debug_gangops_heist_casual_pilot_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CASUAL_PILOT_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CASUAL_PILOT_3), 1)
					ADD_WIDGET_BOOL("Set Casual Pilot outfit", b_debug_set_gangops_heist_casual_pilot_outfit)
					
					ADD_WIDGET_INT_SLIDER("Fighter Pilot Outfits: ", i_debug_gangops_heist_fighter_pilot_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_FIGHTER_PILOT_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_FIGHTER_PILOT_3), 1)
					ADD_WIDGET_BOOL("Set Fighter Pilot outfit", b_debug_set_gangops_heist_fighter_pilot_outfit)
					
					ADD_WIDGET_INT_SLIDER("High Tech Riot Outfits: ", i_debug_gangops_heist_high_tech_riot_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_RIOT_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_RIOT_3), 1)
					ADD_WIDGET_BOOL("Set High Tech Riot outfit", b_debug_set_gangops_heist_high_tech_riot_outfit)
					
					ADD_WIDGET_INT_SLIDER("High Tech Impact Outfits: ", i_debug_gangops_heist_high_tech_impact_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_IMPACT_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HIGH_TECH_IMPACT_3), 1)
					ADD_WIDGET_BOOL("Set High Tech Impact outfit", b_debug_set_gangops_heist_high_tech_impact_outfit)

					ADD_WIDGET_INT_SLIDER("Medium Tech Masked Outfits: ", i_debug_gangops_heist_med_tech_masked_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_MASKED_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_MASKED_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Masked outfit", b_debug_set_gangops_heist_med_tech_masked_outfit)

					ADD_WIDGET_INT_SLIDER("Medium Tech Rebellion Outfits: ", i_debug_gangops_heist_med_tech_rebellion_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_REBELLION_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_REBELLION_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Rebellion outfit", b_debug_set_gangops_heist_med_tech_rebellion_outfit)

					ADD_WIDGET_INT_SLIDER("Medium Tech Havoc Outfits: ", i_debug_gangops_heist_med_tech_havoc_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_HAVOC_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_HAVOC_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Havoc outfit", b_debug_set_gangops_heist_med_tech_havoc_outfit)

					ADD_WIDGET_INT_SLIDER("Medium Tech Adaptable Outfits: ", i_debug_gangops_heist_med_tech_adaptable_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_ADAPTABLE_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MED_TECH_ADAPTABLE_3), 1)
					ADD_WIDGET_BOOL("Set Medium Tech Adaptable outfit", b_debug_set_gangops_heist_med_tech_adaptable_outfit)

					ADD_WIDGET_INT_SLIDER("Sub Driver Outfits: ", i_debug_gangops_heist_sub_driver_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SUB_DRIVER_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_SUB_DRIVER_3), 1)
					ADD_WIDGET_BOOL("Set Sub Driver outfit", b_debug_set_gangops_heist_sub_driver_outfit)

					ADD_WIDGET_INT_SLIDER("Heavy Combat Gear Outfits: ", i_debug_gangops_heist_heavy_combat_gear_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HEAVY_COMBAT_GEAR_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_HEAVY_COMBAT_GEAR_3), 1)
					ADD_WIDGET_BOOL("Set Heavy Combat Gear outfit", b_debug_set_gangops_heist_heavy_combat_gear_outfit)

					ADD_WIDGET_INT_SLIDER("Low Tech Tactical Outfits: ", i_debug_gangops_heist_low_tech_tactical_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_TACTICAL_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_TACTICAL_3), 1)
					ADD_WIDGET_BOOL("Set Low Tech Tactical outfit", b_debug_set_gangops_heist_low_tech_tactical_outfit)

					ADD_WIDGET_INT_SLIDER("Low Tech Combat Outfits: ", i_debug_gangops_heist_low_tech_combat_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_COMBAT_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_LOW_TECH_COMBAT_3), 1)
					ADD_WIDGET_BOOL("Set Low Tech Combat outfit", b_debug_set_gangops_heist_low_tech_combat_outfit)

					ADD_WIDGET_INT_SLIDER("Classic Stealth Outfits: ", i_debug_gangops_heist_classic_stealth_gear_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CLASSIC_STEALTH_GEAR_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_CLASSIC_STEALTH_GEAR_3), 1)
					ADD_WIDGET_BOOL("Set Classic Stealth outfit", b_debug_set_gangops_heist_classic_stealth_gear_outfit)

					ADD_WIDGET_INT_SLIDER("Military Camo Gear Outfits: ", i_debug_gangops_heist_military_camo_gear_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MILITARY_CAMO_GEAR_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_MILITARY_CAMO_GEAR_3), 1)
					ADD_WIDGET_BOOL("Set Military Camo Gear outfit", b_debug_set_gangops_heist_military_camo_gear_outfit)
					
					ADD_WIDGET_INT_SLIDER("Gorka Suits: ", i_debug_gangops_heist_gorka_outfits, ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_GORKA_0), ENUM_TO_INT(OUTFIT_GANGOPS_HEIST_GORKA_3), 1)
					ADD_WIDGET_BOOL("Set Gorka suit", b_debug_set_gangops_heist_gorka_outfit)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Assault")
				ADD_WIDGET_INT_SLIDER("Showdown Outfits", i_debug_target_races_showdown_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SHOWDOWN_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SHOWDOWN_GREEN_3), 1)
				ADD_WIDGET_BOOL("Set Showdown outfit", b_debug_set_target_races_showdown_outfit)
				
				ADD_WIDGET_INT_SLIDER("Trapdoor Outfits", i_debug_target_races_trapdoor_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRAPDOOR_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRAPDOOR_GREEN_3), 1)
				ADD_WIDGET_BOOL("Set Trapdoor outfit", b_debug_set_target_races_trapdoor_outfit)
				
				ADD_WIDGET_INT_SLIDER("Venetian Job Outfits", i_debug_target_races_venetian_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_COP), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_ROBBER_GRAY), 1)
				ADD_WIDGET_BOOL("Set Venetian Job outfit", b_debug_set_target_races_venetian_outfit)
				
				ADD_WIDGET_INT_SLIDER("Venetian Job Firesuits", i_debug_target_races_venetian_firesuits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_FIRESUIT_CLASSIC_BLUE), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_VENETIAN_FIRESUIT_CLASSIC_PINK), 1)
				ADD_WIDGET_BOOL("Set Venetian Job Firesuit", b_debug_set_target_races_venetian_firesuits)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Business Battles")
				ADD_WIDGET_INT_SLIDER("Drop the Bomb Outfits", i_debug_business_battles_drop_the_bomb_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_DROPBOMB_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_DROPBOMB_GREEN_3), 1)
				ADD_WIDGET_BOOL("Set Drop the Bomb outfit", b_debug_set_business_battles_drop_the_bomb_outfit)
				
				ADD_WIDGET_INT_SLIDER("Sumo Run Outfits", i_debug_business_battles_sumo_run_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUMORUN_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUMORUN_GREEN_0), 1)
				ADD_WIDGET_BOOL("Set Sumo Run outfit", b_debug_set_business_battles_sumo_run_outfit)
				
				ADD_WIDGET_INT_SLIDER("Stunt Offense Defense Outfits", i_debug_business_battles_offense_defense_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_OFFDEF_PURPLE_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_OFFDEF_GREEN_3), 1)
				ADD_WIDGET_BOOL("Set Stunt Offense Defense outfit", b_debug_set_business_battles_offense_defense_outfit)
				
				ADD_WIDGET_INT_SLIDER("Hunting Pack Remix Outfits", i_debug_business_battles_hunting_pack_remix_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_HUNTBACK_REMIX_HOONS_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_HUNTBACK_REMIX_RABBLE_RUNNER_0), 1)
				ADD_WIDGET_BOOL("Set Hunting Pack Remix outfit", b_debug_set_business_battles_hunting_pack_remix_outfit)
				
				ADD_WIDGET_INT_SLIDER("Trading Places Remix Outfits", i_debug_business_battles_trading_places_remix_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_7), 1)
				ADD_WIDGET_BOOL("Set Trading Places Remix outfit", b_debug_set_business_battles_trading_places_remix_outfit)
				
				ADD_WIDGET_INT_SLIDER("Come Out To Play Remix Outfits", i_debug_business_battles_come_out_to_play_remix_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_COMEPLAYREMIX_HUNTER_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_COMEPLAYREMIX_RUNNER_1), 1)
				ADD_WIDGET_BOOL("Set Come Out To Play Remix outfit", b_debug_set_business_battles_come_out_to_play_remix_outfit)
				
				ADD_WIDGET_INT_SLIDER("Running Back Remix Outfits", i_debug_business_battles_running_back_remix_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_BLACK), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_BLUE), 1)
				ADD_WIDGET_BOOL("Set Running Back Remix outfit", b_debug_set_business_battles_running_back_remix_outfit)
				
				ADD_WIDGET_INT_SLIDER("Running Back Remix (Green + Yellow) Outfits", i_debug_business_battles_running_back_remix_2_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_GREEN), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_RUNBACKREMIX_YELLOWGLOW), 1)
				ADD_WIDGET_BOOL("Set Running Back Remix (Green + Yellow) outfit", b_debug_set_business_battles_running_back_remix_2_outfit)
				
				ADD_WIDGET_INT_SLIDER("Nightclub Chiliad Outfits", i_debug_business_battles_nightclub_chiliad_outfits, ENUM_TO_INT(OUTFIT_NIGHTCLUB_CHILIAD_0), ENUM_TO_INT(OUTFIT_NIGHTCLUB_CHILIAD_2), 1)
				ADD_WIDGET_BOOL("Set Nightclub Chiliad outfit", b_debug_set_business_battles_nightclub_chiliad_outfit)
				
				ADD_WIDGET_INT_SLIDER("Nightclub Kifflom Outfits", i_debug_business_battles_nightclub_kifflom_outfits, ENUM_TO_INT(OUTFIT_NIGHTCLUB_KIFFLOM_0), ENUM_TO_INT(OUTFIT_NIGHTCLUB_KIFFLOM_0), 1)
				ADD_WIDGET_BOOL("Set Nightclub Kifflom outfit", b_debug_set_business_battles_nightclub_kifflom_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Arena Wars")
				ADD_WIDGET_INT_SLIDER("Arena Contender Outfits", i_debug_arena_wars_contender_outfits, ENUM_TO_INT(OUTFIT_ARENA_WARS_CONTENDER_GENERAL_0), ENUM_TO_INT(OUTFIT_ARENA_WARS_PETE), 1)
				ADD_WIDGET_BOOL("Set Arena Contender outfit", b_debug_set_arena_wars_contender_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Casino")
				ADD_WIDGET_BOOL("Set Impotent Rage outfit", b_debug_set_casino_impotent_rage_outfit)
				ADD_WIDGET_BOOL("Set Highroller outfit", b_debug_set_casino_highroller_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Spaceling Outfits", i_debug_casino_spaceling_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SPACELING_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SPACELING_3), 1)
				ADD_WIDGET_BOOL("Set Casino Spaceling outfit", b_debug_set_casino_spaceling_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Casino Heist")
				ADD_WIDGET_BOOL("Set Casino Heist undertaker outfit", b_debug_set_casino_heist_undertaker_outfit)
			
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Light I Outfits", i_debug_casino_heist_direct_light_i_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_I_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Light I outfit", b_debug_set_casino_heist_direct_light_i_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Light II Outfits", i_debug_casino_heist_direct_light_ii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_II_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Light II outfit", b_debug_set_casino_heist_direct_light_ii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Light III Outfits", i_debug_casino_heist_direct_light_iii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_LIGHT_III_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Light III outfit", b_debug_set_casino_heist_direct_light_iii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Heavy I Outfits", i_debug_casino_heist_direct_heavy_i_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_I_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Heavy I outfit", b_debug_set_casino_heist_direct_heavy_i_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Heavy II Outfits", i_debug_casino_heist_direct_heavy_ii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_II_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Heavy II outfit", b_debug_set_casino_heist_direct_heavy_ii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Direct Heavy III Outfits", i_debug_casino_heist_direct_heavy_iii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_DIRECT_HEAVY_III_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Direct Heavy III outfit", b_debug_set_casino_heist_direct_heavy_iii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist FIB Outfits", i_debug_casino_heist_fib_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIB_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIB_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist FIB outfit", b_debug_set_casino_heist_fib_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Stealth I Outfits", i_debug_casino_heist_stealth_i_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_I_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_I_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Stealth I outfit", b_debug_set_casino_heist_stealth_i_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Stealth II Outfits", i_debug_casino_heist_stealth_ii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_II_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_II_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Stealth II outfit", b_debug_set_casino_heist_stealth_ii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Stealth III Outfits", i_debug_casino_heist_stealth_iii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_III_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_STEALTH_III_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Stealth III outfit", b_debug_set_casino_heist_stealth_iii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Covert Stealth I Outfits", i_debug_casino_heist_covert_stealth_i_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_I_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Covert Stealth I outfit", b_debug_set_casino_heist_covert_stealth_i_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Covert Stealth II Outfits", i_debug_casino_heist_covert_stealth_ii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_II_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Covert Stealth II outfit", b_debug_set_casino_heist_covert_stealth_ii_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Covert Stealth III Outfits", i_debug_casino_heist_covert_stealth_iii_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_COVERT_STEALTH_III_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Covert Stealth III outfit", b_debug_set_casino_heist_covert_stealth_iii_outfit)
				
				ADD_WIDGET_BOOL("Set Casino Heist Valet outfit", b_debug_set_casino_heist_valet_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Firefighter Outfits", i_debug_casino_heist_Firefighter_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_7), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Firefighter outfit", b_debug_set_casino_heist_Firefighter_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist NOOSE Outfits", i_debug_casino_heist_NOOSE_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist NOOSE outfit", b_debug_set_casino_heist_NOOSE_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Gruppe Sechs Outfits", i_debug_casino_heist_gruppe_sechs_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Gruppe Sechs outfit", b_debug_set_casino_heist_gruppe_sechs_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Bugstars Outfits", i_debug_casino_heist_Bugstars_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_5), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Bugstars outfit", b_debug_set_casino_heist_Bugstars_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Celeb Outfits", i_debug_casino_heist_celeb_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Celeb outfit", b_debug_set_casino_heist_celeb_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Maintenance Outfits", i_debug_casino_heist_maintenance_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_5), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Maintenance outfit", b_debug_set_casino_heist_maintenance_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist Prison Guard Outfits", i_debug_casino_heist_prison_guard_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_PRISON_GUARD_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_PRISON_GUARD_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist Prison Guard outfit", b_debug_set_casino_heist_prison_guard_outfit)
				
				ADD_WIDGET_INT_SLIDER("Casino Heist High Roller Outfits", i_debug_casino_heist_high_roller_outfits, ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_0), ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_3), 1)
				ADD_WIDGET_BOOL("Set Casino Heist High Roller outfit", b_debug_set_casino_heist_high_roller_outfit)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("DLC Summer 2020")
				ADD_WIDGET_INT_SLIDER("Summer 2020 Outfit", i_debug_sum20_outfit, ENUM_TO_INT(OUTFIT_SUM20_ALIEN_AWARD), ENUM_TO_INT(OUTFIT_SUM20_ALIEN_AWARD), 1)
				ADD_WIDGET_BOOL("Set outfit", b_debug_set_sum20_outfit)
			STOP_WIDGET_GROUP()
			
			#IF FEATURE_HEIST_ISLAND
			
			START_WIDGET_GROUP("Island Heist")
				ADD_WIDGET_INT_SLIDER("Island Heist Guard Outfits", i_debug_island_heist_guard_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_3), 1)
				ADD_WIDGET_BOOL("Set Island Heist Guard Outfits", b_debug_set_island_heist_guard_outfit)
				
				ADD_WIDGET_INT_SLIDER("Island Heist Smuggler Outfits", i_debug_island_heist_smuggler_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_3), 1)
				ADD_WIDGET_BOOL("Set Island Heist Smuggler Outfits", b_debug_set_island_heist_smuggler_outfit)
				
				ADD_WIDGET_INT_SLIDER("Island Heist Heavy Outfits", i_debug_island_heist_heavy_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_HEAVY_1_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_HEAVY_2_3), 1)
				ADD_WIDGET_BOOL("Set Island Heist Heavy Outfits", b_debug_set_island_heist_heavy_outfit)
				
				ADD_WIDGET_INT_SLIDER("Island Heist Light Outfits", i_debug_island_heist_light_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_LIGHT_1_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_LIGHT_2_3), 1)
				ADD_WIDGET_BOOL("Set Island Heist Light Outfits", b_debug_set_island_heist_light_outfit)
				
				ADD_WIDGET_INT_SLIDER("Island Heist Stealth Outfits", i_debug_island_heist_stealth_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_STEALTH_1_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_STEALTH_2_3), 1)
				ADD_WIDGET_BOOL("Set Island Heist Stealth Outfits", b_debug_set_island_heist_stealth_outfit)
				
				ADD_WIDGET_INT_SLIDER("Island Heist Beach Party Outfits", i_debug_island_heist_beach_party_outfits, ENUM_TO_INT(OUTFIT_ISLAND_HEIST_BEACH_PARTY_0), ENUM_TO_INT(OUTFIT_ISLAND_HEIST_BEACH_PARTY_0), 1)
				ADD_WIDGET_BOOL("Set Island Heist Beach Party Outfits", b_debug_set_island_heist_beach_party_outfit)
			STOP_WIDGET_GROUP()
			
			#ENDIF
			
			#IF FEATURE_TUNER
			START_WIDGET_GROUP("Tuner")
			
				ADD_WIDGET_INT_SLIDER("Tuner Robber Outfits", i_debug_tuner_robber_outfits, ENUM_TO_INT(OUTFIT_TUNER_ROBBER_BRAVADO), ENUM_TO_INT(OUTFIT_TUNER_ROBBER_ROBBER), 1)
				ADD_WIDGET_BOOL("Set Tuner Robber Outfit", b_debug_set_tuner_robber_outfit)
				
				ADD_WIDGET_INT_SLIDER("Tuner Security Outfits", i_debug_tuner_security_outfits, ENUM_TO_INT(OUTFIT_TUNER_SECURITY_0), ENUM_TO_INT(OUTFIT_TUNER_SECURITY_3), 1)
				ADD_WIDGET_BOOL("Set Tuner Security Outfit", b_debug_set_tuner_security_outfit)
				
				ADD_WIDGET_INT_SLIDER("Tuner Lost MC Outfits", i_debug_tuner_lost_mc_outfits, ENUM_TO_INT(OUTFIT_TUNER_LOST_MC_0), ENUM_TO_INT(OUTFIT_TUNER_LOST_MC_3), 1)
				ADD_WIDGET_BOOL("Set Tuner Lost MC Outfit", b_debug_set_tuner_lost_mc_outfit)
				
				ADD_WIDGET_INT_SLIDER("Tuner Dock Worker Outfits", i_debug_tuner_dock_worker_outfits, ENUM_TO_INT(OUTFIT_TUNER_DOCK_WORKER_0), ENUM_TO_INT(OUTFIT_TUNER_DOCK_WORKER_3), 1)
				ADD_WIDGET_BOOL("Set Tuner Dock Worker Outfit", b_debug_set_tuner_dock_worker_outfit)
			
			STOP_WIDGET_GROUP()
			#ENDIF
			
			#IF FEATURE_TUNER
			START_WIDGET_GROUP("Fixer")
			
				ADD_WIDGET_INT_SLIDER("Fixer Setup Outfits", i_debug_fixer_setup_outfits, ENUM_TO_INT(OUTFIT_FIXER_SETUP_0), ENUM_TO_INT(OUTFIT_FIXER_SETUP_2), 1)
				ADD_WIDGET_BOOL("Set Fixer Setup Outfit", b_debug_set_fixer_setup_outfit)
				
				ADD_WIDGET_INT_SLIDER("Fixer Party Promoter Outfits", i_debug_fixer_party_promoter_outfits, ENUM_TO_INT(OUTFIT_FIXER_PARTY_PROMOTER_0), ENUM_TO_INT(OUTFIT_FIXER_PARTY_PROMOTER_3), 1)
				ADD_WIDGET_BOOL("Set Fixer Party Promoter Outfit", b_debug_set_fixer_party_promoter_outfit)
				
				ADD_WIDGET_INT_SLIDER("Fixer Billionaire Games Outfits", i_debug_fixer_billionaire_games_outfits, ENUM_TO_INT(OUTFIT_FIXER_BILLIONAIRE_GAMES_0), ENUM_TO_INT(OUTFIT_FIXER_BILLIONAIRE_GAMES_3), 1)
				ADD_WIDGET_BOOL("Set Fixer Billionaire Games Outfit", b_debug_set_fixer_billionaire_games_outfit)
				
				ADD_WIDGET_INT_SLIDER("Fixer Golf Outfits", i_debug_fixer_golf_outfits, ENUM_TO_INT(OUTFIT_FIXER_GOLF_0), ENUM_TO_INT(OUTFIT_FIXER_GOLF_7), 1)
				ADD_WIDGET_BOOL("Set Fixer Golf Outfit", b_debug_set_fixer_golf_outfit)
			
				ADD_WIDGET_BOOL("Set Heist Navy Coveralls", b_debug_set_heist_navy_coveralls)
			
			STOP_WIDGET_GROUP()
			#ENDIF
			
			#IF FEATURE_DLC_1_2022
			START_WIDGET_GROUP("Summer 2022")
				ADD_WIDGET_BOOL("Set LD Organics Award outfit", b_debug_set_ld_organics_award)
				
				ADD_WIDGET_INT_SLIDER("Sum22 IAA Agent Outfits", i_debug_sum22_iaa_agent_outfits, ENUM_TO_INT(OUTFIT_SUM22_IAA_AGENT_0), ENUM_TO_INT(OUTFIT_SUM22_IAA_AGENT_3), 1)
				ADD_WIDGET_BOOL("Set Sum22 IAA AGent Outfit", b_debug_set_sum22_iaa_agent_outfit)
				
				ADD_WIDGET_INT_SLIDER("Sum22 Halloween Riders Outfits", i_debug_sum22_halloween_riders_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_RIDERS_7), 1)
				ADD_WIDGET_BOOL("Set Sum22 Halloween Riders Outfit", b_debug_set_sum22_halloween_riders_outfit)
				
				ADD_WIDGET_INT_SLIDER("Sum22 Halloween Hunted Outfits", i_debug_sum22_halloween_hunted_outfits, ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_0), ENUM_TO_INT(OUTFIT_VERSUS_HIDDEN_SUM22_HALLOWEEN_HUNTED_7), 1)
				ADD_WIDGET_BOOL("Set Sum22 Halloween Hunted Outfit", b_debug_set_sum22_halloween_hunted_outfit)
			
			STOP_WIDGET_GROUP()
			
			#ENDIF
			
		STOP_WIDGET_GROUP()
		
		//////////////////////////////////////////////////////////////////////////////
		///      
		///      [MISC FLAGS]
		///      
		//////////////////////////////////////////////////////////////////////////////
		START_WIDGET_GROUP("Global Flags")
			ADD_WIDGET_BOOL("g_bValidatePlayersTorsoComponent", g_bValidatePlayersTorsoComponent)
			ADD_WIDGET_BOOL("g_bValidatePlayersHairStats", g_bValidatePlayersHairStats)
			ADD_WIDGET_BOOL("g_bValidatePlayersHelmetBerd", g_bValidatePlayersHelmetBerd)
			ADD_WIDGET_BOOL("g_bValidatePlayersScubaGear", g_bValidatePlayersScubaGear)
			ADD_WIDGET_BOOL("g_bForcePlayersHelmetOn", g_bForcePlayersHelmetOn)
			ADD_WIDGET_BOOL("g_bUnoptimiseValidatePlayersHairStats", g_bUnoptimiseValidatePlayersHairStats)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	
	eStage = STAGE_PROCESSING
ENDPROC

PROC PROCESS_CLOTHING_DEBUG_OUTPUT()

	IF bSetPlayerModel_Male
		MODEL_NAMES eModel = MP_M_FREEMODE_01
		REQUEST_MODEL(eModel)
		IF HAS_MODEL_LOADED(eModel)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_MODEL(PLAYER_ID(), eModel)
				SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
			bSetPlayerModel_Male = FALSE
		ENDIF
		
		// Update stored model
		SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 0)
	ENDIF
	IF bSetPlayerModel_Female
		MODEL_NAMES eModel = MP_F_FREEMODE_01
		REQUEST_MODEL(eModel)
		IF HAS_MODEL_LOADED(eModel)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_MODEL(PLAYER_ID(), eModel)
				SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
			bSetPlayerModel_Female = FALSE
		ENDIF
		// Update stored model
		SET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, 1)
	ENDIF
	
	IF bOutputUniqueHashNames
	
		INT iComp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		PED_COMP_NAME_ENUM eItem
		INT iDLCNameHash
		TEXT_LABEL_63 tlNameHash
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		INT iDLCPackHash = -1
		INT iLocalDrawOffset
		scrShopPedComponent componentItem
		PED_COMP_NAME_ENUM ePedComp
		INT iMissingItemsCount = 0
		
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		IF ePedModel = MP_M_FREEMODE_01		SAVE_STRING_TO_DEBUG_FILE("DLC NAME HASHES - MP_M_FREEMODE_01")
		ELIF ePedModel = MP_F_FREEMODE_01	SAVE_STRING_TO_DEBUG_FILE("DLC NAME HASHES - MP_F_FREEMODE_01")
		ENDIF
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT NUM_PED_COMPONENTS iComp
			
			iDLCPackHash = -1
			iLocalDrawOffset = 0
			
			SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, iComp)))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF iComp != ENUM_TO_INT(PED_COMP_HEAD)
			//AND iComp != ENUM_TO_INT(PED_COMP_DECL)
				eItem = GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(ePedModel, INT_TO_ENUM(PED_COMPONENT, iComp))
				IF eItem != DUMMY_PED_COMP
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)), eItem)
					iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
					iDLCPackHash = -1
					REPEAT iDrawCount iDraw
					
						IF GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw) != iDLCPackHash
							iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
							iLocalDrawOffset = iDraw
						ENDIF
						
						IF iDraw >= g_sTempCompData[1].iDrawable
						
							IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
							OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
								iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
								REPEAT iTexCount iTex
									iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDraw, iTex)
									
									SAVE_STRING_TO_DEBUG_FILE("...")
									SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, iComp)))
									
									SAVE_STRING_TO_DEBUG_FILE(", Local ")
									SAVE_INT_TO_DEBUG_FILE(iDraw-iLocalDrawOffset)
									SAVE_STRING_TO_DEBUG_FILE(", Global ")
									SAVE_INT_TO_DEBUG_FILE(iDraw)
									SAVE_STRING_TO_DEBUG_FILE(", Texture ")
									SAVE_INT_TO_DEBUG_FILE(iTex)
									
									
									IF iDLCNameHash != 0
										GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
									ELSE
										ePedComp = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp))
										IF ePedComp != DUMMY_PED_COMP
											g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp), ePedComp)
										ENDIF
									ENDIF
									
									IF iDLCNameHash = 0
										SAVE_STRING_TO_DEBUG_FILE(", ***missing***")
										iMissingItemsCount++
									ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
										SAVE_STRING_TO_DEBUG_FILE(", ")
										SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
									ELSE
										SAVE_STRING_TO_DEBUG_FILE(", ")
										SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)
									ENDIF
									
									SAVE_NEWLINE_TO_DEBUG_FILE()
								ENDREPEAT
							ENDIF
						ENDIF
					ENDREPEAT
					
					WAIT(0)
				ENDIF
			ENDIF
		ENDREPEAT
		
		WAIT(0)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iProp
		REPEAT NUMBER_OF_PED_PROP_TYPES iProp
		
			SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
			iDLCPackHash = -1
			iLocalDrawOffset = 0
			
			REPEAT iDrawCount iDraw
			
				IF GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw) != iDLCPackHash
					iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					iLocalDrawOffset = iDraw
				ENDIF
			
				IF iDraw >= GET_DEFAULT_DLC_DRAW_FOR_PROP(ePedModel, INT_TO_ENUM(PED_PROP_POSITION, iProp))
				
					IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
					OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
				
						iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
						REPEAT iTexCount iTex
							iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDraw, iTex)
							
							SAVE_STRING_TO_DEBUG_FILE("...")
							SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))
							
							SAVE_STRING_TO_DEBUG_FILE(", Local ")
							SAVE_INT_TO_DEBUG_FILE(iDraw-iLocalDrawOffset)
							SAVE_STRING_TO_DEBUG_FILE(", Global ")
							SAVE_INT_TO_DEBUG_FILE(iDraw)
							SAVE_STRING_TO_DEBUG_FILE(", Texture ")
							SAVE_INT_TO_DEBUG_FILE(iTex)
							
							IF iDLCNameHash = 0
								SAVE_STRING_TO_DEBUG_FILE(", ***missing***")
								iMissingItemsCount++
							ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
								SAVE_STRING_TO_DEBUG_FILE(", ")
								SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
							ELSE
								SAVE_STRING_TO_DEBUG_FILE(", ")
								SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)
							ENDIF
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDREPEAT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		WAIT(0)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("MISSING ITEMS COUNT ")SAVE_INT_TO_DEBUG_FILE(iMissingItemsCount)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("INVALID ENTRIES")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		
		INT iDLCItemCount
		INT iDLCItem
		INT iCurrentPed = GET_SHOP_PED_ID_FOR_MODEL(GET_ENTITY_MODEL(PLAYER_PED_ID()))
		//scrShopPedComponent componentItem
		scrShopPedProp propItem
		
		REPEAT NUM_PED_COMPONENTS iComp
			iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), -1, iComp)
			REPEAT iDLCItemCount iDLCItem
				GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
				
				IF componentItem.m_drawableIndex < 0
				OR componentItem.m_drawableIndex >= GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
				OR componentItem.m_textureIndex < 0
				OR componentItem.m_textureIndex >= GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), componentItem.m_drawableIndex)
					
					SAVE_STRING_TO_DEBUG_FILE("...")
					SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, iComp)))
					SAVE_STRING_TO_DEBUG_FILE(", Global Drawable ")
					SAVE_INT_TO_DEBUG_FILE(componentItem.m_drawableIndex)
					SAVE_STRING_TO_DEBUG_FILE(", Texture ")
					SAVE_INT_TO_DEBUG_FILE(componentItem.m_textureIndex)
					
					IF GET_DLC_NAME_HASH_STRING(componentItem.m_nameHash, tlNameHash)
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
					ELSE
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_INT_TO_DEBUG_FILE(componentItem.m_nameHash)
					ENDIF
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			ENDREPEAT
			
			WAIT(0)
		ENDREPEAT
		
		iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_PROP), -1, -1)
		REPEAT iDLCItemCount iDLCItem
			GET_SHOP_PED_QUERY_PROP(iDLCItem, propItem)
			
			IF propItem.m_propIndex < 0
			OR propItem.m_propIndex >= GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint))
			OR propItem.m_textureIndex < 0
			OR propItem.m_textureIndex >= GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint), propItem.m_propIndex)
				
				SAVE_STRING_TO_DEBUG_FILE("...")
				SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint)))
				SAVE_STRING_TO_DEBUG_FILE(", Global Drawable ")
				SAVE_INT_TO_DEBUG_FILE(propItem.m_propIndex)
				SAVE_STRING_TO_DEBUG_FILE(", Texture ")
				SAVE_INT_TO_DEBUG_FILE(propItem.m_textureIndex)
				
				IF GET_DLC_NAME_HASH_STRING(propItem.m_nameHash, tlNameHash)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
				ELSE
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_INT_TO_DEBUG_FILE(propItem.m_nameHash)
				ENDIF
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		CLOSE_DEBUG_FILE()
		
		bOutputUniqueHashNames = FALSE
	ENDIF
	
	IF bOutputShopLocateNames
		
		REQUEST_ADDITIONAL_TEXT("CLO_MNU", MENU_TEXT_SLOT)
		WHILE NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("CLO_MNU", MENU_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		OPEN_DEBUG_FILE()
		
		INT iMenuIndex
		REPEAT ENUM_TO_INT(CLO_MENU_LAST_SHOP_MENU) iMenuIndex
			SAVE_INT_TO_DEBUG_FILE(iMenuIndex)
			SAVE_STRING_TO_DEBUG_FILE("	")
			IF DOES_TEXT_LABEL_EXIST(GET_CLOTHES_MENU_NAME(INT_TO_ENUM(CLOTHES_MENU_ENUM, iMenuIndex), FALSE))
				SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(GET_CLOTHES_MENU_NAME(INT_TO_ENUM(CLOTHES_MENU_ENUM, iMenuIndex), FALSE)))
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		
		CLOSE_DEBUG_FILE()
		
		CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, FALSE)
		
		bOutputShopLocateNames = FALSE
	ENDIF
	
	IF (bGenerate_ShopMeta OR bGenerate_MissingShopMeta)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("<!-- ")
		SAVE_STRING_TO_DEBUG_FILE(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), GET_ENTITY_MODEL(PLAYER_PED_ID())))
		SAVE_STRING_TO_DEBUG_FILE(" -->")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	<pedComponents>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iComp, iProp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		INT iDLCPackHash = GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), GET_ENTITY_MODEL(PLAYER_PED_ID())))
		TEXT_LABEL_63 tlPackID = GET_SHOP_PED_APPAREL_PREFIX(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack))
		INT iLocalDraw
		INT iLastDLCHash
		INT iDLCNameHash
		REPEAT NUM_PED_COMPONENTS iComp
		
			iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
			iLocalDraw = 0
			iLastDLCHash = 0
			
			REPEAT iDrawCount iDraw
			
				IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					iLastDLCHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					iLocalDraw = 0
				ENDIF
				
				IF (iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
				OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw))
				AND GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw) != 0
				
					SAVE_STRING_TO_DEBUG_FILE("<!-- ")
					SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))
					SAVE_STRING_TO_DEBUG_FILE(": -->")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				
					iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
					REPEAT iTexCount iTex
					
						iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDraw, iTex)
						IF (NOT bGenerate_MissingShopMeta OR iDLCNameHash = 0)
						
							SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<cost value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<textLabel />")SAVE_NEWLINE_TO_DEBUG_FILE()
							
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
								SAVE_STRING_TO_DEBUG_FILE("			<uniqueNameHash>")SAVE_STRING_TO_DEBUG_FILE(tlPackID)SAVE_STRING_TO_DEBUG_FILE("_M_")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("			<uniqueNameHash>")SAVE_STRING_TO_DEBUG_FILE(tlPackID)SAVE_STRING_TO_DEBUG_FILE("_F_")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
							SAVE_STRING_TO_DEBUG_FILE("			<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<locate value=\"-99\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<scriptSaveData value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<restrictionTags />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<forcedComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<variantComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<variantProps />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<drawableIndex value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<localDrawableIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<textureIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<isInOutfit value=\"false\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
					ENDREPEAT
				ENDIF
				iLocalDraw++
			ENDREPEAT
		ENDREPEAT
		
		SAVE_STRING_TO_DEBUG_FILE("	</pedComponents>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	<pedProps>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		WAIT(0)
		
		REPEAT 9 iProp
		
			iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
			iLocalDraw = 0
			iLastDLCHash = 0
			
			REPEAT iDrawCount iDraw
			
				IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					iLastDLCHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					iLocalDraw = 0
				ENDIF
				
				IF (iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
				OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw))
				AND GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw) != 0
				
					SAVE_STRING_TO_DEBUG_FILE("<!-- ")
					SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))
					SAVE_STRING_TO_DEBUG_FILE(": -->")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					REPEAT iTexCount iTex
					
						iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDraw, iTex)
						IF (NOT bGenerate_MissingShopMeta OR iDLCNameHash = 0)
						
							SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<cost value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<textLabel />")SAVE_NEWLINE_TO_DEBUG_FILE()
							
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
								SAVE_STRING_TO_DEBUG_FILE("			<uniqueNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_SHOP_PED_APPAREL_PREFIX(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack)))SAVE_STRING_TO_DEBUG_FILE("_M_")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("			<uniqueNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_SHOP_PED_APPAREL_PREFIX(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack)))SAVE_STRING_TO_DEBUG_FILE("_F_")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
							SAVE_STRING_TO_DEBUG_FILE("			<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<locate value=\"-99\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<scriptSaveData value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<restrictionTags />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<forcedComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<variantComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<variantProps />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<propIndex value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<localPropIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iLocalDraw)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<eAnchorPoint>")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))SAVE_STRING_TO_DEBUG_FILE("</eAnchorPoint>")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<textureIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			<isInOutfit value=\"false\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
					ENDREPEAT
				ENDIF
				iLocalDraw++
			ENDREPEAT
		ENDREPEAT
		
		SAVE_STRING_TO_DEBUG_FILE("	</pedProps>")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		CLOSE_DEBUG_FILE()
		
		bGenerate_ShopMeta = FALSE
		bGenerate_MissingShopMeta = FALSE
	ENDIF
	
	IF bGenerate_MissingSaveItems
	
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iComp, iProp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		INT iDLCPackHash = GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), GET_ENTITY_MODEL(PLAYER_PED_ID())))
		INT iLocalDraw
		INT iLastDLCHash
		INT iDLCNameHash
		TEXT_LABEL_63 tlDLCName
		scrShopPedComponent componentItem
		scrShopPedProp propItem
		
		REPEAT NUM_PED_COMPONENTS iComp
		
			iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
			iLocalDraw = 0
			iLastDLCHash = 0
			
			REPEAT iDrawCount iDraw
			
				IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					iLastDLCHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					iLocalDraw = 0
				ENDIF
				
				IF (iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
				OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw))
				AND GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw) != 0
				
					iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
					REPEAT iTexCount iTex
					
						iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDraw, iTex)
						GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
						
						IF NOT DOES_SHOP_PED_APPAREL_HAVE_ACQUIRED_SAVE_DATA(iDLCNameHash)
						AND NOT IS_STRING_NULL_OR_EMPTY(componentItem.m_textLabel)
							
							SAVE_STRING_TO_DEBUG_FILE("<Item key=\"")
							IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlDLCName)
								SAVE_STRING_TO_DEBUG_FILE(tlDLCName)
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("MISSING_")
								SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))
								SAVE_STRING_TO_DEBUG_FILE("_")
								SAVE_INT_TO_DEBUG_FILE(iLocalDraw)
								SAVE_STRING_TO_DEBUG_FILE("_")
								SAVE_INT_TO_DEBUG_FILE(iTex)
							ENDIF
							SAVE_STRING_TO_DEBUG_FILE("\" value=\"\"/>")
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
					ENDREPEAT
				ENDIF
				iLocalDraw++
			ENDREPEAT
		ENDREPEAT
		
		WAIT(0)
		
		REPEAT 9 iProp
		
			iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
			iLocalDraw = 0
			iLastDLCHash = 0
			
			REPEAT iDrawCount iDraw
			
				IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					iLastDLCHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					iLocalDraw = 0
				ENDIF
				
				IF (iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
				OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw))
				AND GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw) != 0
				
					iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					REPEAT iTexCount iTex
					
						iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDraw, iTex)
						
						GET_SHOP_PED_PROP(iDLCNameHash, propItem)
						
						IF NOT DOES_SHOP_PED_APPAREL_HAVE_ACQUIRED_SAVE_DATA(iDLCNameHash)
						AND NOT IS_STRING_NULL_OR_EMPTY(propItem.m_textLabel)
						AND GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlDLCName)
						
							SAVE_STRING_TO_DEBUG_FILE("<Item key=\"")
							IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlDLCName)
								SAVE_STRING_TO_DEBUG_FILE(tlDLCName)
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("MISSING_")
								SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))
								SAVE_STRING_TO_DEBUG_FILE("_")
								SAVE_INT_TO_DEBUG_FILE(iLocalDraw)
								SAVE_STRING_TO_DEBUG_FILE("_")
								SAVE_INT_TO_DEBUG_FILE(iTex)
							ENDIF
							SAVE_STRING_TO_DEBUG_FILE("\" value=\"\"/>")
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
					ENDREPEAT
				ENDIF
				iLocalDraw++
			ENDREPEAT
		ENDREPEAT
		
		CLOSE_DEBUG_FILE()
	
		bGenerate_MissingSaveItems = FALSE
	ENDIF
	
	IF bGenerate_GET_DLC_NAME_HASH_STRING
	
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("GET_DLC_NAME_HASH_STRING")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iComp, iProp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		INT iDLCPackHash = GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), GET_ENTITY_MODEL(PLAYER_PED_ID())))
		TEXT_LABEL_63 tlPackID = GET_SHOP_PED_APPAREL_PREFIX(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack))
		TEXT_LABEL_63 tlItemKey
		INT iLocalDraw
		INT iLastDLCHash
		INT iCount
		
		INT iPass
		REPEAT 2 iPass
			iCount = 0
			REPEAT NUM_PED_COMPONENTS iComp
			
				iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
				iLocalDraw = 0
				iLastDLCHash = 0
				
				REPEAT iDrawCount iDraw
				
					IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
						iLastDLCHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
						iLocalDraw = 0
					ENDIF
					
					IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
					OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					
						iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
						REPEAT iTexCount iTex
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							tlItemKey = tlPackID
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
								tlItemKey += "_M_"
							ELSE
								tlItemKey += "_F_"
							ENDIF
							tlItemKey += GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
							tlItemKey += "_"
							tlItemKey += iLocalDraw
							tlItemKey += "_"
							tlItemKey += iTex
							
							IF iPass = 0
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE(", // ")SAVE_INT_TO_DEBUG_FILE(GET_HASH_KEY(tlItemKey))
							ELSE
								IF iCount = 0
									SAVE_STRING_TO_DEBUG_FILE("		SWITCH eDLCComp")SAVE_NEWLINE_TO_DEBUG_FILE()
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("			CASE ")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE(" 	t = \"")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE("\" BREAK")
								iCount++
								
								IF iCount >= 20
									SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_STRING_TO_DEBUG_FILE("		ENDSWITCH")
									iCount = 0
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					iLocalDraw++
				ENDREPEAT
			ENDREPEAT
			
			WAIT(0)
			
			REPEAT 9 iProp
			
				iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
				iLocalDraw = 0
				iLastDLCHash = 0
				
				REPEAT iDrawCount iDraw
				
					IF iLastDLCHash != GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
						iLastDLCHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
						iLocalDraw = 0
					ENDIF
					
					IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
					OR iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
					
						iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
						REPEAT iTexCount iTex
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							tlItemKey = tlPackID
							IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
								tlItemKey += "_M_"
							ELSE
								tlItemKey += "_F_"
							ENDIF
							tlItemKey += GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp))
							tlItemKey += "_"
							tlItemKey += iLocalDraw
							tlItemKey += "_"
							tlItemKey += iTex
							
							IF iPass = 0
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE(", // ")SAVE_INT_TO_DEBUG_FILE(GET_HASH_KEY(tlItemKey))
							ELSE
								IF iCount = 0
									SAVE_STRING_TO_DEBUG_FILE("		SWITCH eDLCComp")SAVE_NEWLINE_TO_DEBUG_FILE()
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("			CASE ")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE(" 	t = \"")SAVE_STRING_TO_DEBUG_FILE(tlItemKey)SAVE_STRING_TO_DEBUG_FILE("\" BREAK")
								iCount++
								
								IF iCount >= 20
									SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_STRING_TO_DEBUG_FILE("		ENDSWITCH")
									iCount = 0
								ENDIF
							ENDIF
							
						ENDREPEAT
					ENDIF
					iLocalDraw++
				ENDREPEAT
			ENDREPEAT
			
			IF iCount != 0
				SAVE_NEWLINE_TO_DEBUG_FILE()SAVE_STRING_TO_DEBUG_FILE("		ENDSWITCH")
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		
		CLOSE_DEBUG_FILE()
	
		bGenerate_GET_DLC_NAME_HASH_STRING = FALSE
	ENDIF
	
	IF bGenerate_GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP
	AND NETWORK_IS_GAME_IN_PROGRESS()
	
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iPed
		MODEL_NAMES ePlayerModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		IF ePlayerModel = MP_M_FREEMODE_01
			iPed = 3
		ELIF ePlayerModel = MP_F_FREEMODE_01
			iPed = 4
		ENDIF
		
		PED_COMP_NAME_ENUM eDefaultDLCProp = GET_DEFAULT_DLC_PED_COMPONENT_FOR_PROP(ePlayerModel)
		
		scrShopPedProp propItem
		INIT_SHOP_PED_PROP(propItem)
		
		IF ePlayerModel = MP_M_FREEMODE_01
			SAVE_STRING_TO_DEBUG_FILE("FUNC INT __MALE_PROPS_LOOKUP(INT iNameHash)")
		ELIF ePlayerModel = MP_F_FREEMODE_01
			SAVE_STRING_TO_DEBUG_FILE("FUNC INT __FEMALE_PROPS_LOOKUP(INT iNameHash)")
		ENDIF
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	SWITCH iNameHash")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iLookupEnum
		INT iDLCItem
		INT iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_PROP), -1, -1) // shop=CLO_SHOP_NONE
		REPEAT iDLCItemCount iDLCItem
			GET_SHOP_PED_QUERY_PROP(iDLCItem, propItem)
			SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_INT_TO_DEBUG_FILE(propItem.m_nameHash)SAVE_STRING_TO_DEBUG_FILE("		RETURN ")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eDefaultDLCProp)+iDLCItem)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Validate what we already have.
			iLookupEnum = ENUM_TO_INT(GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), propItem.m_propIndex, propItem.m_textureIndex, INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint)))
			IF iLookupEnum != (ENUM_TO_INT(eDefaultDLCProp)+iDLCItem)
				ASSERTLN("Invalid meta: [", GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint)), "] ", propItem.m_nameHash, ", lookup enum = ", iLookupEnum, ", expected enum = ", (ENUM_TO_INT(eDefaultDLCProp)+iDLCItem), ", draw = ", propItem.m_propIndex, ", tex = ", propItem.m_textureIndex)
			ENDIF
			
			IF (iDLCItem % 100) = 0
				SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	SWITCH iNameHash")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDREPEAT
		
		SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	RETURN -99")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("ENDFUNC")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		WAIT(0)
		
		scrShopPedComponent componentItem
		INIT_SHOP_PED_COMPONENT(componentItem)
		
		PED_COMP_NAME_ENUM eDefaultDLCComp
		
		INT iCompType
		FOR iCompType = 1 TO 11
		
			WAIT(0)
		
			IF ePlayerModel = MP_M_FREEMODE_01
				SAVE_STRING_TO_DEBUG_FILE("FUNC INT __MALE_")
			ELIF ePlayerModel = MP_F_FREEMODE_01
				SAVE_STRING_TO_DEBUG_FILE("FUNC INT __FEMALE_")
			ENDIF
			
			SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, iCompType)))
			SAVE_STRING_TO_DEBUG_FILE("_LOOKUP(INT iNameHash)")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	SWITCH iNameHash")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
		
			eDefaultDLCComp = GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(ePlayerModel, INT_TO_ENUM(PED_COMPONENT, iCompType))
			iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), -1, iCompType) // shop=CLO_SHOP_NONE
			
			REPEAT iDLCItemCount iDLCItem
				GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
				SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_INT_TO_DEBUG_FILE(componentItem.m_nameHash)SAVE_STRING_TO_DEBUG_FILE("		RETURN ")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eDefaultDLCComp)+iDLCItem)SAVE_STRING_TO_DEBUG_FILE(" BREAK")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				// Validate what we already have.
				iLookupEnum = ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), componentItem.m_drawableIndex, componentItem.m_textureIndex, INT_TO_ENUM(PED_COMP_TYPE_ENUM, componentItem.m_eCompType)))
				IF iLookupEnum != (ENUM_TO_INT(eDefaultDLCComp)+iDLCItem)
					ASSERTLN("Invalid meta: [", GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, iCompType)), "] ", componentItem.m_nameHash, ", lookup enum = ", iLookupEnum, ", expected enum = ", (ENUM_TO_INT(eDefaultDLCComp)+iDLCItem), ", draw = ", componentItem.m_drawableIndex, ", tex = ", componentItem.m_textureIndex)
				ENDIF
				
				IF (iDLCItem % 100) = 0
					SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("	SWITCH iNameHash")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				
				IF (iDLCItem % 500) = 0
					WAIT(0)
				ENDIF
			ENDREPEAT
			
			WAIT(0)
			
			SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	RETURN -99")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("ENDFUNC")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDFOR
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		CLOSE_DEBUG_FILE()
		
		bGenerate_GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP = FALSE
	ENDIF
	
	IF bGenerate_GET_TATTOO_ENUM_FROM_DLC_HASH
	
		INT iDLCIndex
        INT iDLCCount
        sTattooShopItemValues sDLCTattooData
        TATTOO_FACTION_ENUM eFaction
		TEXT_LABEL_63 tlPackName
        
        eFaction = TATTOO_MP_FM
        iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("FUNC BOOL LOOKUP_MALE_TATTOO_ENUM_FOR_DLC_HASH(INT iDLCTattooNameHash, TATTOO_NAME_ENUM &eTattoo)")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	INT iDLCIndex = -1")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iCount = 0
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF iCount = 0
					SAVE_STRING_TO_DEBUG_FILE("	SWITCH iDLCTattooNameHash")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			
				SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Preset)SAVE_STRING_TO_DEBUG_FILE(" iDLCIndex = ")SAVE_INT_TO_DEBUG_FILE(iDLCIndex)SAVE_STRING_TO_DEBUG_FILE(" BREAK // ")
				IF GET_DLC_PACK_NAME_FROM_HASH(sDLCTattooData.Collection, tlPackName)
					SAVE_STRING_TO_DEBUG_FILE(tlPackName)
				ELSE
					SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Collection)
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(sDLCTattooData.label)
					SAVE_STRING_TO_DEBUG_FILE(" [")SAVE_STRING_TO_DEBUG_FILE(sDLCTattooData.label)SAVE_STRING_TO_DEBUG_FILE("]")
					IF DOES_TEXT_LABEL_EXIST(sDLCTattooData.label)
						SAVE_STRING_TO_DEBUG_FILE(" ")SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sDLCTattooData.label))
					ENDIF
				ENDIF
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				iCount++
				IF iCount >= 100
					SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					iCount = 0
				ENDIF
			ENDIF
		ENDREPEAT
		IF iCount != 0
			SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE("	")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	IF iDLCIndex != -1")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		eTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, (ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex))")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		RETURN TRUE")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	ENDIF")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	RETURN FALSE")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("ENDFUNC")
		SAVE_NEWLINE_TO_DEBUG_FILE()
        
        eFaction = TATTOO_MP_FM_F
        iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("FUNC BOOL LOOKUP_FEMALE_TATTOO_ENUM_FOR_DLC_HASH(INT iDLCTattooNameHash, TATTOO_NAME_ENUM &eTattoo)")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	INT iDLCIndex = -1")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		iCount = 0
        REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF iCount = 0
					SAVE_STRING_TO_DEBUG_FILE("	SWITCH iDLCTattooNameHash")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF iDLCIndex >= 300 AND iDLCIndex <= 302
					SAVE_STRING_TO_DEBUG_FILE("		//CASE -963164512 iDLCIndex = ")
					SAVE_INT_TO_DEBUG_FILE(iDLCIndex)
					SAVE_STRING_TO_DEBUG_FILE(" BREAK // MP_LUXE_VDG_006_F")
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Preset)SAVE_STRING_TO_DEBUG_FILE(" iDLCIndex = ")SAVE_INT_TO_DEBUG_FILE(iDLCIndex)SAVE_STRING_TO_DEBUG_FILE(" BREAK // ")
					IF GET_DLC_PACK_NAME_FROM_HASH(sDLCTattooData.Collection, tlPackName)
						SAVE_STRING_TO_DEBUG_FILE(tlPackName)
					ELSE
						SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Collection)
					ENDIF
					IF NOT IS_STRING_NULL_OR_EMPTY(sDLCTattooData.label)
						SAVE_STRING_TO_DEBUG_FILE(" [")SAVE_STRING_TO_DEBUG_FILE(sDLCTattooData.label)SAVE_STRING_TO_DEBUG_FILE("]")
						IF DOES_TEXT_LABEL_EXIST(sDLCTattooData.label)
							SAVE_STRING_TO_DEBUG_FILE(" ")SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sDLCTattooData.label))
						ENDIF
					ENDIF
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				iCount++
				IF iCount >= 100
					SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					iCount = 0
				ENDIF
			ENDIF
		ENDREPEAT
		IF iCount != 0
			SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE("	")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	IF iDLCIndex != -1")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		eTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, (ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex))")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		RETURN TRUE")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	ENDIF")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("	RETURN FALSE")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("ENDFUNC")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		CLOSE_DEBUG_FILE()
		
		bGenerate_GET_TATTOO_ENUM_FROM_DLC_HASH = FALSE
	ENDIF

	IF bDisplay_ShopPedInfo
	AND NETWORK_IS_GAME_IN_PROGRESS()
		INT iComp, iProp
		INT iDLCNameHash
		TEXT_LABEL_63 tlDisplayText1, tlDisplayText2
		PED_COMP_NAME_ENUM ePedComp
		PED_INDEX pedID = PLAYER_PED_ID()
		
		IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		AND DOES_ENTITY_EXIST(g_pShopClonePed)
		AND NOT IS_PED_INJURED(g_pShopClonePed)
			pedID = g_pShopClonePed
		ENDIF
		
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID)
		scrShopPedComponent componentItem
		scrShopPedProp propItem
		BOOL bNameHashFound
		FLOAT fScreenX, fScreenY, fYOffset
		//GET_SCREEN_COORD_FROM_WORLD_COORD(GET_PED_BONE_COORDS(pedID, BONETAG_HEAD, <<0,0,0>>), fScreenX, fScreenY)
		fScreenX = 0.6
		fScreenY = 0.3
		
		TEXT_LABEL tlSplit = "  "
		
		REPEAT NUMBER_OF_PED_PROP_TYPES iProp
		
			bNameHashFound = FALSE
			tlDisplayText1 = ""
			tlDisplayText2 = ""
			
			iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(pedID), iProp, GET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp)), GET_PED_PROP_TEXTURE_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp)))
			IF iDLCNameHash != 0
				GET_SHOP_PED_PROP(iDLCNameHash, propItem)
				bNameHashFound = GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlDisplayText1)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(componentItem.m_textLabel)
					IF bNameHashFound
						tlDisplayText2 += tlSplit
					ENDIF
					tlDisplayText2 += "~g~"
					tlDisplayText2 += componentItem.m_textLabel
					IF DOES_TEXT_LABEL_EXIST(componentItem.m_textLabel)
						tlDisplayText2 += tlSplit
						tlDisplayText2 += "~r~"
						tlDisplayText2 += GET_STRING_FROM_TEXT_FILE(componentItem.m_textLabel)
					ENDIF
				ENDIF
			ELSE
				ePedComp = GET_PED_PROP_CURRENT_FROM_LOOKUP(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp))
				IF ePedComp != DUMMY_PED_COMP
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, COMP_TYPE_PROPS, ePedComp)
					
					IF GET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp)) != -1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_sTempCompData[1].sLabel)
						AND NOT ARE_STRINGS_EQUAL(g_sTempCompData[1].sLabel, "NO_LABEL")
						
							tlDisplayText1 = "DISK_"
							tlDisplayText1 += GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp))
							tlDisplayText1 += "_"
							tlDisplayText1 += GET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp))
							tlDisplayText1 += "_"
							tlDisplayText1 += GET_PED_PROP_TEXTURE_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, iProp))
							
							tlDisplayText2 += tlSplit
							tlDisplayText2 += "~g~"
							tlDisplayText2 += g_sTempCompData[1].sLabel
							IF DOES_TEXT_LABEL_EXIST(g_sTempCompData[1].sLabel)
								tlDisplayText2 += tlSplit
								tlDisplayText2 += "~r~"
								tlDisplayText2 += GET_STRING_FROM_TEXT_FILE(g_sTempCompData[1].sLabel)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tlDisplayText1)
			OR NOT IS_STRING_NULL_OR_EMPTY(tlDisplayText2)
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(0.0000, 0.360)
				SET_TEXT_COLOUR(255, 165, 0, 255)
				SET_TEXT_WRAP(0.0, 5.0)
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("TWOSTRINGS")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
				FLOAT fWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
				DRAW_RECT_FROM_CORNER(fScreenX, fScreenY+fYOffset, fWidth, 0.030, 0, 0, 0, 100)
				
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(0.0000, 0.360)
				SET_TEXT_COLOUR(255, 165, 0, 255)
				SET_TEXT_WRAP(0.0, 5.0)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGS")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
				END_TEXT_COMMAND_DISPLAY_TEXT(fScreenX, fScreenY+fYOffset)
				
				fYOffset += 0.030
			ENDIF
		ENDREPEAT
		
		fYOffset += (0.030)
		
		INT iLoopCounter
		REPEAT NUM_PED_COMPONENTS iLoopCounter
			
			SWITCH iLoopCounter
				CASE 0 iComp = ENUM_TO_INT(PED_COMP_HAIR) BREAK
				CASE 1 iComp = ENUM_TO_INT(PED_COMP_BERD) BREAK
				CASE 2 iComp = ENUM_TO_INT(PED_COMP_JBIB) BREAK
				CASE 3 iComp = ENUM_TO_INT(PED_COMP_SPECIAL) BREAK
				CASE 4 iComp = ENUM_TO_INT(PED_COMP_LEG) BREAK
				CASE 5 iComp = ENUM_TO_INT(PED_COMP_FEET) BREAK
				CASE 6 iComp = ENUM_TO_INT(PED_COMP_TEETH) BREAK
				CASE 7 iComp = ENUM_TO_INT(PED_COMP_TORSO) BREAK
				CASE 8 iComp = ENUM_TO_INT(PED_COMP_SPECIAL2) BREAK
				CASE 9 iComp = ENUM_TO_INT(PED_COMP_DECL) BREAK
				CASE 10 iComp = ENUM_TO_INT(PED_COMP_HAND) BREAK
				CASE 11 iComp = ENUM_TO_INT(PED_COMP_HEAD) BREAK
			ENDSWITCH
			
			bNameHashFound = FALSE
			tlDisplayText1 = ""
			tlDisplayText2 = ""
			
			iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(pedID), iComp, GET_PED_DRAWABLE_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, iComp)), GET_PED_TEXTURE_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, iComp)))
			IF iDLCNameHash != 0
				GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
				bNameHashFound = GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlDisplayText1)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(componentItem.m_textLabel)
					IF bNameHashFound
						tlDisplayText2 += tlSplit
					ENDIF
					tlDisplayText2 += "~g~"
					tlDisplayText2 += componentItem.m_textLabel
					IF DOES_TEXT_LABEL_EXIST(componentItem.m_textLabel)
						tlDisplayText2 += tlSplit
						tlDisplayText2 += "~r~"
						tlDisplayText2 += GET_STRING_FROM_TEXT_FILE(componentItem.m_textLabel)
					ENDIF
				ENDIF
			ELSE
				ePedComp = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(pedID, INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp))
				IF ePedComp != DUMMY_PED_COMP
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp), ePedComp)
					
					IF (NOT IS_STRING_NULL_OR_EMPTY(g_sTempCompData[1].sLabel)
					AND NOT ARE_STRINGS_EQUAL(g_sTempCompData[1].sLabel, "NO_LABEL"))
					OR iComp = ENUM_TO_INT(PED_COMP_TORSO)
					
						tlDisplayText1 = "DISK_"
						tlDisplayText1 += GET_PED_COMP_NAME_STRING(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iComp))
						tlDisplayText1 += "_"
						tlDisplayText1 += GET_PED_DRAWABLE_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, iComp))
						tlDisplayText1 += "_"
						tlDisplayText1 += GET_PED_TEXTURE_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, iComp))
						
						tlDisplayText2 += tlSplit
						tlDisplayText2 += "~g~"
						tlDisplayText2 += g_sTempCompData[1].sLabel
						IF DOES_TEXT_LABEL_EXIST(g_sTempCompData[1].sLabel)
							tlDisplayText2 += tlSplit
							tlDisplayText2 += "~r~"
							tlDisplayText2 += GET_STRING_FROM_TEXT_FILE(g_sTempCompData[1].sLabel)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tlDisplayText1)
			OR NOT IS_STRING_NULL_OR_EMPTY(tlDisplayText2)
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(0.0000, 0.360)
				SET_TEXT_COLOUR(255, 165, 0, 255)
				SET_TEXT_WRAP(0.0, 5.0)
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("TWOSTRINGS")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
				FLOAT fWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
				DRAW_RECT_FROM_CORNER(fScreenX, fScreenY+fYOffset, fWidth, 0.030, 0, 0, 0, 100)
				
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(0.0000, 0.360)
				SET_TEXT_COLOUR(255, 165, 0, 255)
				SET_TEXT_WRAP(0.0, 5.0)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGS")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
				END_TEXT_COMMAND_DISPLAY_TEXT(fScreenX, fScreenY+fYOffset)
				
				fYOffset += 0.030
			ENDIF
		ENDREPEAT
		
		// List all crew tattoos that are current.
		TATTOO_DATA_STRUCT sTattooData
		INT iCrewTattoo
		TATTOO_NAME_ENUM eCrewTattoo
		REPEAT 6 iCrewTattoo
			SWITCH iCrewTattoo
				CASE 0 eCrewTattoo = TATTOO_MP_FM_CREW_A tlDisplayText1 = "TATTOO_MP_FM_CREW_A" BREAK
				CASE 1 eCrewTattoo = TATTOO_MP_FM_CREW_B tlDisplayText1 = "TATTOO_MP_FM_CREW_B" BREAK
				CASE 2 eCrewTattoo = TATTOO_MP_FM_CREW_C tlDisplayText1 = "TATTOO_MP_FM_CREW_C" BREAK
				CASE 3 eCrewTattoo = TATTOO_MP_FM_CREW_D tlDisplayText1 = "TATTOO_MP_FM_CREW_D" BREAK
				CASE 4 eCrewTattoo = TATTOO_MP_FM_CREW_E tlDisplayText1 = "TATTOO_MP_FM_CREW_E" BREAK
				CASE 5 eCrewTattoo = TATTOO_MP_FM_CREW_F tlDisplayText1 = "TATTOO_MP_FM_CREW_F" BREAK
			ENDSWITCH
			
			IF IS_MP_TATTOO_CURRENT(eCrewTattoo)
				IF GET_TATTOO_DATA(sTattooData, eCrewTattoo, GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()), PLAYER_PED_ID())
					tlDisplayText2 += tlSplit
					tlDisplayText2 += "~g~"
					tlDisplayText2 += sTattooData.sLabel
					IF DOES_TEXT_LABEL_EXIST(sTattooData.sLabel)
						tlDisplayText2 += tlSplit
						tlDisplayText2 += "~r~"
						tlDisplayText2 += GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel)
					ENDIF
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, 0.360)
					SET_TEXT_COLOUR(255, 165, 0, 255)
					SET_TEXT_WRAP(0.0, 5.0)
					BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("TWOSTRINGS")
						ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
						ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
					FLOAT fWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
					DRAW_RECT_FROM_CORNER(fScreenX, fScreenY+fYOffset, fWidth, 0.030, 0, 0, 0, 100)
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, 0.360)
					SET_TEXT_COLOUR(255, 165, 0, 255)
					SET_TEXT_WRAP(0.0, 5.0)
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGS")
						ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText1)
						ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlDisplayText2)
					END_TEXT_COMMAND_DISPLAY_TEXT(fScreenX, fScreenY+fYOffset)
					
					fYOffset += 0.030
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bGenerate_ClothingScreenshots
	OR bGenerate_ClothingScreenshotsPrimary
	
		INT iComp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		PED_COMP_NAME_ENUM eItem
		INT iDLCNameHash
		TEXT_LABEL_63 tlNameHash
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		INT iDLCPackHash = -1
		scrShopPedComponent componentItem
		scrShopPedProp propItem
		INT iAltComps
		INT iAltComp
		INT iRetNameHash, iRetCompEnum, iRetType
		INT iDLCTatIndex
		TATTOO_NAME_ENUM eTattooLookup
		TATTOO_FACTION_ENUM eFaction
		sTattooShopItemValues sDLCTattooData
		scrPedHeadBlendData playerBlendData
		
		IF ePedModel = MP_M_FREEMODE_01
			eFaction = TATTOO_MP_FM
		ELIF ePedModel = MP_F_FREEMODE_01
			eFaction = TATTOO_MP_FM_F
		ENDIF
		
		REQUEST_ANIM_DICT("clothingshirt")
		WHILE NOT HAS_ANIM_DICT_LOADED("clothingshirt")
			WAIT(0)
		ENDWHILE
		
		g_bValidatePlayersTorsoComponent = FALSE
		BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)
		DISABLE_CELLPHONE(TRUE)
		HANG_UP_AND_PUT_AWAY_PHONE()
		
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEL, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)
		
		INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<228.6058, -992.0537, -99.9999>>, "hei_dlc_garage_high_new")
		PIN_INTERIOR_IN_MEMORY(interiorID)
		WHILE NOT IS_INTERIOR_READY(interiorID)
			WAIT(0)
		ENDWHILE
		
		// Spawn at location, play fixed anims.
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<234.7408, -995.2711, -99.9999>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 268.9845)
		FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), interiorID, HASH("GtaMloRoom001"))
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		
		CAMERA_INDEX camClothingFocus = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0,0.0,0.0>>)
		POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0,0.0,0.0>>)
		SET_CAM_ACTIVE(camClothingFocus, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		REPEAT NUM_PED_COMPONENTS iComp
		
			IF iComp = ENUM_TO_INT(PED_COMP_HEAD)
				RELOOP
			ENDIF
			
			WAIT(0)
		
			IF iComp = ENUM_TO_INT(PED_COMP_BERD)
			OR iComp = ENUM_TO_INT(PED_COMP_HAIR)
				DETACH_CAM(camPlayerOffset)
				ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.163, -0.448, 0.753>>)
				POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0, 0.0, 0.655>>)
			ELIF iComp = ENUM_TO_INT(PED_COMP_LEG)
				DETACH_CAM(camPlayerOffset)
				ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.163, -0.96, -0.12>>)
				POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0, 0.0, -0.42>>)
			ELIF iComp = ENUM_TO_INT(PED_COMP_FEET)
				DETACH_CAM(camPlayerOffset)
				ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.163, -0.96, -0.45>>)
				POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0, 0.0, -0.87>>)
			ELSE
				DETACH_CAM(camPlayerOffset)
				ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.163, -0.950, 0.545>>)
				POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0, 0.0, 0.265>>)
			ENDIF
			WAIT(0)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			IF iComp = ENUM_TO_INT(PED_COMP_BERD)
				GET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), playerBlendData)
				IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
					SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), 0,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0,  playerBlendData.m_texBlend, 0.0)
				ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
					SET_PED_HEAD_BLEND_DATA(PLAYER_PED_ID(), 21,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0, playerBlendData.m_texBlend, 0.0)
				ENDIF
				MICRO_MORPH_TYPE microMorph
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH microMorph
					SET_PED_MICRO_MORPH(PLAYER_PED_ID(), microMorph, 0.0)
				ENDREPEAT
			ELSE
				RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
			ENDIF
			
			IF iSelectedPedComponent = NUMBER_OF_PED_COMP_TYPES
			OR GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)) = INT_TO_ENUM(PED_COMP_TYPE_ENUM, iSelectedPedComponent)
			
				eItem = GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(ePedModel, INT_TO_ENUM(PED_COMPONENT, iComp))
				IF eItem != DUMMY_PED_COMP
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)), eItem)
					iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
					
					REPEAT iDrawCount iDraw
					
						CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
						CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
						SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						IF ePedModel = MP_M_FREEMODE_01
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 3, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 11, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 13, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 15, 0)
						ELIF ePedModel = MP_F_FREEMODE_01
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 8, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 13, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 12, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 2, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
							SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 82, 0)
						ENDIF
					
						iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
					
						IF iDraw >= g_sTempCompData[1].iDrawable
						
							IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
							OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
								iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
								
								IF bGenerate_ClothingScreenshotsPrimary
									iTexCount = 1
								ENDIF
								
								REPEAT iTexCount iTex
									iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDraw, iTex)
									GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
									IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
										IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
											tlNameHash = ""
											tlNameHash += iDLCNameHash
										ENDIF
										
										SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), componentItem.m_drawableIndex, componentItem.m_textureIndex)
										WAIT(0)
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
											WAIT(0)
											CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										ENDWHILE
										
										
										//SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMP_TYPE_ENUM, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))), GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP(ePedModel, iDLCNameHash, INT_TO_ENUM(PED_COMP_TYPE_ENUM, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))), FALSE)
										//FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), COMP_TYPE_JBIB), FALSE)
										SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), componentItem.m_drawableIndex, componentItem.m_textureIndex)
										WAIT(0)
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										
										WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
											WAIT(0)
											CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										ENDWHILE
										
										WAIT(0)
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
										
										IF NOT IS_STRING_NULL_OR_EMPTY(tlNameHash)
											SAVE_SCREENSHOT(tlNameHash)
										ENDIF
										
										// OVERLAY ITEMS
										IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_TAT_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
										OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
											
											iAltComps = GET_SHOP_PED_APPAREL_VARIANT_COMPONENT_COUNT(iDLCNameHash)
											REPEAT iAltComps iAltComp											
												GET_VARIANT_COMPONENT(iDLCNameHash, iAltComp, iRetNameHash, iRetCompEnum, iRetType)
												IF iRetType = ENUM_TO_INT(PED_COMP_DECL)
												AND iRetNameHash != 0 AND iRetNameHash != 1849449579 // 1849449579 = "0"
												
													eTattooLookup = GET_TATTOO_ENUM_FROM_DLC_HASH(iRetNameHash, eFaction)
													iDLCTatIndex =  ENUM_TO_INT(eTattooLookup) - ENUM_TO_INT(TATTOO_MP_FM_DLC)
													
													IF iDLCTatIndex >= 0
													AND GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCTatIndex, sDLCTattooData)
													AND iRetNameHash = sDLCTattooData.Preset
													
														//Apply overlay.
														CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
														ADD_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sDLCTattooData.Collection, sDLCTattooData.Preset)
														//Wait
														WAIT(0)
														CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
														
														IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
															tlNameHash = ""
															tlNameHash += iDLCNameHash
														ENDIF
														tlNameHash += "_overlay"
														tlNameHash += iAltComp
														
														IF NOT IS_STRING_NULL_OR_EMPTY(tlNameHash)
															SAVE_SCREENSHOT(tlNameHash)
														ENDIF
													ENDIF
												ENDIF
											ENDREPEAT
										ENDIF	
										
										RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
									ENDIF
								ENDREPEAT
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDREPEAT
		
		WAIT(0)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		
		DETACH_CAM(camPlayerOffset)
		ATTACH_CAM_TO_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.163, -0.448, 0.753>>)
		POINT_CAM_AT_ENTITY(camClothingFocus, PLAYER_PED_ID(), <<0.0, 0.0, 0.655>>)
		
		INT iProp
		REPEAT NUMBER_OF_PED_PROP_TYPES iProp
		
			IF iSelectedPedComponent = NUMBER_OF_PED_COMP_TYPES
			OR iSelectedPedComponent = ENUM_TO_INT(COMP_TYPE_PROPS)
			
				iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
				
				REPEAT iDrawCount iDraw
				
					CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
					CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
					SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
					IF ePedModel = MP_M_FREEMODE_01
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 3, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 11, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 13, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 15, 0)
					ELIF ePedModel = MP_F_FREEMODE_01
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO, 8, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG, 13, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 12, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 2, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB, 82, 0)
					ENDIF
					
					iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
				
					IF iDraw >= GET_DEFAULT_DLC_DRAW_FOR_PROP(ePedModel, INT_TO_ENUM(PED_PROP_POSITION, iProp))
					
						IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
						OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
					
							iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
							
							IF bGenerate_ClothingScreenshotsPrimary
								iTexCount = 1
							ENDIF
							
							REPEAT iTexCount iTex
								
								iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDraw, iTex)
								GET_SHOP_PED_PROP(iDLCNameHash, propItem)
								IF NOT IS_CONTENT_ITEM_LOCKED(propItem.m_lockHash)
									IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
										tlNameHash = ""
										tlNameHash += iDLCNameHash
									ENDIF
									
									SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), propItem.m_propIndex, propItem.m_textureIndex)
									WAIT(0)
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									WHILE NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID())
										WAIT(0)
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									ENDWHILE
									
									//SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_FROM_NAME_HASH_LOOKUP(ePedModel, iDLCNameHash, COMP_TYPE_PROPS), FALSE)
									//FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), iDraw, iTex, INT_TO_ENUM(PED_PROP_POSITION, iProp)), FALSE)
									SET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), propItem.m_propIndex, propItem.m_textureIndex)
									WAIT(0)
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									
									WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
										WAIT(0)
										CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									ENDWHILE
									
									WAIT(0)
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									
									IF NOT IS_STRING_NULL_OR_EMPTY(tlNameHash)
										SAVE_SCREENSHOT(tlNameHash)
									ENDIF
									
									RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
									
								ENDIF
							ENDREPEAT
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
		
		WAIT(0)
		
		SET_CAM_ACTIVE(camClothingFocus, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(camClothingFocus)
		
		REMOVE_ANIM_DICT("clothingshirt")
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
		
		g_bValidatePlayersTorsoComponent = TRUE
		DISABLE_CELLPHONE(FALSE)
		
		CLOSE_DEBUG_FILE()
	
		bGenerate_ClothingScreenshots = FALSE
		bGenerate_ClothingScreenshotsPrimary = FALSE
	ENDIF
	
	IF bGenerate_ExclsuiveClothingInfo
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			INT iComp
			//INT iProp
			INT iDrawable, iDrawableCount
			INT iTexture, iTextureCount
			INT iDLCNameHash
			INT iDLCPackHash
			scrShopPedComponent componentItem
			//scrShopPedProp propItem
			TEXT_LABEL_63 tlPack
			BOOL bLockStatus[2]
			
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(PLAYER_PED_ID())))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	Pack	Type	Drawable	Texture	ContentLock	ExclusiveLock")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			FOR iComp = 0 TO NUM_PED_COMPONENTS-1
				iDrawableCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT,iComp))
				REPEAT iDrawableCount iDrawable
				
					iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDrawable)
					IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackHash, tlPack, FALSE)
						tlPack = "NA"
					ENDIF
					
					iTextureCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDrawable)
					REPEAT iTextureCount iTexture
					
						iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDrawable, iTexture)
						IF iDLCNameHash != 0
							GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
							
							bLockStatus[0] = IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
							bLockStatus[1] = IS_PED_DRAWABLE_GEN9_EXCLUSIVE(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDrawable)
							
							IF bLockStatus[0]
							OR bLockStatus[1]
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(tlPack)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING((INT_TO_ENUM(PED_COMPONENT, iComp))))
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(iDrawable)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(iTexture)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_BOOL_TO_DEBUG_FILE(bLockStatus[0])
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_BOOL_TO_DEBUG_FILE(bLockStatus[1])
								SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
						ENDIF
					ENDREPEAT
				ENDREPEAT
			ENDFOR
			
			/*SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	Pack	Type	Drawable	Texture	ContentLock	ExclusiveLock")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			FOR iProp = 0 TO NUM_PED_PROPS-1
				iDrawableCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION,iProp))
				REPEAT iDrawableCount iDrawable
				
					iDLCPackHash = GET_DLC_PACK_HASH_FOR_CPROP(PLAYER_PED_ID(), iProp, iDrawable)
					IF NOT GET_DLC_PACK_NAME_FROM_HASH(iDLCPackHash, tlPack, FALSE)
						tlPack = "NA"
					ENDIF
				
					iTextureCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDrawable)
					REPEAT iTextureCount iTexture
					
						iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDrawble, iTexture)
						IF iDLCNameHash != 0
							GET_SHOP_PED_PROP(iDLCNameHash, propItem)
							
							bLockStatus[0] = IS_CONTENT_ITEM_LOCKED(propItem.m_lockHash)
							bLockStatus[1] = IS_PED_PROP_GEN9_EXCLUSIVE(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDrawable)
							
							IF bLockStatus[0]
							OR bLockStatus[1]
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(tlPack)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING((INT_TO_ENUM(PED_PROP_POSITION, iProp))))
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(iDrawable)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(iTexture)
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_BOOL_TO_DEBUG_FILE(bLockStatus[0])
								SAVE_STRING_TO_DEBUG_FILE("	")SAVE_BOOL_TO_DEBUG_FILE(bLockStatus[1])
								SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
						ENDIF
					ENDREPEAT
				ENDREPEAT
			ENDFOR*/
			
			CLOSE_DEBUG_FILE()
		ENDIF
		bGenerate_ExclsuiveClothingInfo = FALSE
	ENDIF
	
	IF bGenerate_ClothingInfo
		INT iComp
		INT iDrawCount, iDraw
		INT iTexCount, iTex
		PED_COMP_NAME_ENUM eItem
		INT iDLCNameHash
		TEXT_LABEL_63 tlNameHash, tlVariantNameHash
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		INT iDLCPackHash = -1
		INT iLocalDrawOffset
		scrShopPedComponent componentItem
		scrShopPedProp propItem
		INT iAltComps
		INT iAltComp
		INT iRetNameHash, iRetCompEnum, iRetType
		TEXT_LABEL_15 tlMenuLabel
		CLOTHES_MENU_ENUM eMenu, eMenuOverride
		INT iSubMenu
		INT iDLCTatIndex
		TATTOO_NAME_ENUM eTattooLookup
		TATTOO_FACTION_ENUM eFaction
		sTattooShopItemValues sDLCTattooData
		TEXT_LABEL_63 tlPackName
		INT iOverlayCount
		
		IF ePedModel = MP_M_FREEMODE_01
			eFaction = TATTOO_MP_FM
		ELIF ePedModel = MP_F_FREEMODE_01
			eFaction = TATTOO_MP_FM_F
		ENDIF
		
		REQUEST_ADDITIONAL_TEXT("CLO_MNU", MENU_TEXT_SLOT)
		WHILE NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("CLO_MNU", MENU_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		OPEN_DEBUG_FILE()
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("COMPONENT	DLC_PACK	DLC_KEY	DLC_LOCK	GLOBAL_ID	LOCAL_ID	TEXTURE_ID	ENUM	SHOP_MENU	TEXT_LABEL	NAME	AWARD_ITEM	OVERLAY_ITEM	OVERLAY_COUNT	VARIANT_1	VARIANT_2	VARIANT_3	VARIANT_4	VARIANT_5	VARIANT_6")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		REPEAT NUM_PED_COMPONENTS iComp
			
			IF iComp = ENUM_TO_INT(PED_COMP_HEAD)
				RELOOP
			ENDIF
			
			IF iSelectedPedComponent = NUMBER_OF_PED_COMP_TYPES
			OR GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)) = INT_TO_ENUM(PED_COMP_TYPE_ENUM, iSelectedPedComponent)
			
				eItem = GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(ePedModel, INT_TO_ENUM(PED_COMPONENT, iComp))
				IF eItem != DUMMY_PED_COMP
					g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)), eItem)
					iDrawCount = GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp))
					iDLCPackHash = -1
					REPEAT iDrawCount iDraw
					
						IF GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw) != iDLCPackHash
							iDLCPackHash = GET_DLC_PACK_HASH_FOR_COMPONENT(PLAYER_PED_ID(), iComp, iDraw)
							iLocalDrawOffset = iDraw
						ENDIF
						
						IF iDraw >= g_sTempCompData[1].iDrawable
						
							IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
							OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
								iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, iComp), iDraw)
								REPEAT iTexCount iTex
									iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), iComp, iDraw, iTex)
									GET_SHOP_PED_COMPONENT(iDLCNameHash, componentItem)
									IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
										tlNameHash = ""
										tlNameHash += iDLCNameHash
									ENDIF
									
									// COMPONENT
									SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))
									SAVE_STRING_TO_DEBUG_FILE("	")
									// DLC_PACK
									IF GET_DLC_PACK_NAME_FROM_HASH(iDLCPackHash, tlPackName)
										SAVE_STRING_TO_DEBUG_FILE(tlPackName)
									ELSE
										SAVE_INT_TO_DEBUG_FILE(iDLCPackHash)
									ENDIF
									SAVE_STRING_TO_DEBUG_FILE("	")
									// DLC_KEY
									//SAVE_STRING_TO_DEBUG_FILE("=HYPERLINK(\"X:\\gta5\\titleupdate\\dev_ng\\")
									SAVE_STRING_TO_DEBUG_FILE("=HYPERLINK(\"Screenshots\\")
									SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
									SAVE_STRING_TO_DEBUG_FILE(".png\", \"")
									SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
									SAVE_STRING_TO_DEBUG_FILE("\")")
									SAVE_STRING_TO_DEBUG_FILE("	")
									// DLC_LOCK
									SAVE_STRING_TO_DEBUG_FILE(GET_DLC_CONTENT_LOCK_NAME_FROM_HASH(componentItem.m_lockHash))
									SAVE_STRING_TO_DEBUG_FILE("	")
									// GLOBAL_ID
									SAVE_INT_TO_DEBUG_FILE(iDraw)
									SAVE_STRING_TO_DEBUG_FILE("	")
									// LOCAL_ID
									SAVE_INT_TO_DEBUG_FILE(iDraw-iLocalDrawOffset)
									SAVE_STRING_TO_DEBUG_FILE("	")
									// TEXTURE_ID
									SAVE_INT_TO_DEBUG_FILE(iTex)
									SAVE_STRING_TO_DEBUG_FILE("	")
									// ENUM
									IF ePedModel = MP_M_FREEMODE_01
										SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, iDLCNameHash, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)), 3)))
									ELSE
										SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, iDLCNameHash, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)), 4)))
									ENDIF
									SAVE_STRING_TO_DEBUG_FILE("	")
									// SHOP_MENU
									IF componentItem.m_locate = 0
										eMenu = CLO_MENU_MASKS
									ELSE
										eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, componentItem.m_locate)
									ENDIF
									tlMenuLabel = GET_CLOTHES_MENU_NAME(eMenu, FALSE)
									
									IF DOES_TEXT_LABEL_EXIST(tlMenuLabel)
									
										IF eMenu = CLO_MENU_MASKS
										AND GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_MASK_TYPE_INVALID
											iSubMenu = GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel)
											GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
											
										ELIF eMenu = CLO_MENU_EARRINGS
										AND GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_EARRING_TYPE_INVALID
											iSubMenu = GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel)
											GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
											
										ELIF eMenu = CLO_MENU_MP_AWARD_SHIRTS
										AND GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_AWARD_TYPE_INVALID
											iSubMenu = GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel)
											GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
											
										ELIF eMenu = CLO_MENU_MP_XMAS_TOPS
										AND GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_FESTIVE_TYPE_INVALID
											iSubMenu = GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel)
											GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
											
										ELIF eMenu = CLO_MENU_MP_ARENA_WAR_TOPS
										AND GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_ARENA_WAR_TOP_TYPE_INVALID
											iSubMenu = GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel)
											GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
											
										ENDIF
										
										SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
										SAVE_STRING_TO_DEBUG_FILE(" - ")
										SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlMenuLabel))
									ELSE
										SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
									ENDIF
									SAVE_STRING_TO_DEBUG_FILE("	")
									
									IF DOES_TEXT_LABEL_EXIST(componentItem.m_textLabel)
										// TEXT_LABEL
										SAVE_STRING_TO_DEBUG_FILE(componentItem.m_textLabel)
										SAVE_STRING_TO_DEBUG_FILE("	")
										// NAME
										SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(componentItem.m_textLabel))
										SAVE_STRING_TO_DEBUG_FILE("	")
									ELIF NOT IS_STRING_NULL_OR_EMPTY(componentItem.m_textLabel)
										// TEXT_LABEL
										SAVE_STRING_TO_DEBUG_FILE(componentItem.m_textLabel)
										SAVE_STRING_TO_DEBUG_FILE("	")
										// NAME
										SAVE_STRING_TO_DEBUG_FILE("	")
									ELSE
										SAVE_STRING_TO_DEBUG_FILE("		")
									ENDIF
									
									// AWARD_ITEM
									IF GIVE_DLC_CLOTHING_ITEM_FOR_FREE(iDLCNameHash)
										SAVE_STRING_TO_DEBUG_FILE("TRUE")
									ELSE
										SAVE_STRING_TO_DEBUG_FILE("FALSE")
									ENDIF
									SAVE_STRING_TO_DEBUG_FILE("	")
									
									// OVERLAY_ITEM
									IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_TAT_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
									OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
										SAVE_STRING_TO_DEBUG_FILE("TRUE")
									ELSE
										SAVE_STRING_TO_DEBUG_FILE("FALSE")
									ENDIF
									SAVE_STRING_TO_DEBUG_FILE("	")
									
									// OVERLAY_COUNT
									iAltComps = GET_SHOP_PED_APPAREL_VARIANT_COMPONENT_COUNT(iDLCNameHash)
									iOverlayCount = 0
									REPEAT iAltComps iAltComp
										IF iRetType = ENUM_TO_INT(PED_COMP_DECL)
											iOverlayCount++
										ENDIF
									ENDREPEAT
									SAVE_INT_TO_DEBUG_FILE(iOverlayCount)
									SAVE_STRING_TO_DEBUG_FILE("	")
									
									// VARIANTS
									iAltComps = GET_SHOP_PED_APPAREL_VARIANT_COMPONENT_COUNT(iDLCNameHash)
									REPEAT iAltComps iAltComp
									
										IF (iAltComp % 50) = 0
											WAIT(0)
										ENDIF
									
										GET_VARIANT_COMPONENT(iDLCNameHash, iAltComp, iRetNameHash, iRetCompEnum, iRetType)
										IF iRetType != ENUM_TO_INT(PED_COMP_SPECIAL2)
										AND iRetType != ENUM_TO_INT(PED_COMP_DECL)
											IF iRetNameHash != 0 AND iRetNameHash != -1
												IF GET_DLC_NAME_HASH_STRING(iRetNameHash, tlVariantNameHash)
													SAVE_STRING_TO_DEBUG_FILE(tlVariantNameHash)
												ELSE
													SAVE_INT_TO_DEBUG_FILE(iRetNameHash)
												ENDIF
											ELSE
												g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iRetType)), INT_TO_ENUM(PED_COMP_NAME_ENUM, iRetCompEnum))
												SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iRetType))))
												SAVE_STRING_TO_DEBUG_FILE("_")
												SAVE_INT_TO_DEBUG_FILE(g_sTempCompData[1].iDrawable)
												SAVE_STRING_TO_DEBUG_FILE("_")
												SAVE_INT_TO_DEBUG_FILE(g_sTempCompData[1].iTexture)
											ENDIF
											
											SAVE_STRING_TO_DEBUG_FILE("	")
										ENDIF
									ENDREPEAT
									
									SAVE_NEWLINE_TO_DEBUG_FILE()
									
									// OVERLAY ITEMS
									IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_TAT_DECL, ENUM_TO_INT(SHOP_PED_COMPONENT))
									OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_MULTI_DECAL, ENUM_TO_INT(SHOP_PED_COMPONENT))
										
										iAltComps = GET_SHOP_PED_APPAREL_VARIANT_COMPONENT_COUNT(iDLCNameHash)
										REPEAT iAltComps iAltComp
										
											IF (iAltComp % 50) = 0
												WAIT(0)
											ENDIF
											
											GET_VARIANT_COMPONENT(iDLCNameHash, iAltComp, iRetNameHash, iRetCompEnum, iRetType)
											IF iRetType = ENUM_TO_INT(PED_COMP_DECL)
											AND iRetNameHash != 0 AND iRetNameHash != 1849449579 // 1849449579 = "0"
											
												// COMPONENT
												SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp))))
												SAVE_STRING_TO_DEBUG_FILE("-OVERLAY")
												SAVE_STRING_TO_DEBUG_FILE("	")
												// DLC_PACK
												IF GET_DLC_PACK_NAME_FROM_HASH(sDLCTattooData.Collection, tlPackName)
													SAVE_STRING_TO_DEBUG_FILE(tlPackName)
												ELSE
													SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Collection)
												ENDIF
												SAVE_STRING_TO_DEBUG_FILE("	")
												// DLC_KEY
												SAVE_STRING_TO_DEBUG_FILE("=HYPERLINK(\"Screenshots\\")
												SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
												SAVE_STRING_TO_DEBUG_FILE("_overlay")
												SAVE_INT_TO_DEBUG_FILE(iAltComp)
												SAVE_STRING_TO_DEBUG_FILE(".png\", \"")
												SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
												SAVE_STRING_TO_DEBUG_FILE("\")")
												SAVE_STRING_TO_DEBUG_FILE("	")
												
												eTattooLookup = GET_TATTOO_ENUM_FROM_DLC_HASH(iRetNameHash, eFaction)
												iDLCTatIndex =  ENUM_TO_INT(eTattooLookup) - ENUM_TO_INT(TATTOO_MP_FM_DLC)
												
												IF iDLCTatIndex >= 0
												AND GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCTatIndex, sDLCTattooData)
												AND iRetNameHash = sDLCTattooData.Preset
												
													// DLC_LOCK
													SAVE_STRING_TO_DEBUG_FILE(GET_DLC_CONTENT_LOCK_NAME_FROM_HASH(sDLCTattooData.m_lockHash))
													SAVE_STRING_TO_DEBUG_FILE("	")
													// GLOBAL_ID
													SAVE_INT_TO_DEBUG_FILE(iDLCTatIndex)
													SAVE_STRING_TO_DEBUG_FILE("	")
													// LOCAL_ID
													SAVE_INT_TO_DEBUG_FILE(sDLCTattooData.Preset)
													SAVE_STRING_TO_DEBUG_FILE("	")
													// TEXTURE_ID
													SAVE_STRING_TO_DEBUG_FILE("	")
													// ENUM
													SAVE_STRING_TO_DEBUG_FILE("	")
													// SHOP_MENU
													IF componentItem.m_locate = 0
														eMenu = CLO_MENU_MASKS
													ELSE
														eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, componentItem.m_locate)
													ENDIF
													IF GET_MENU_OVERRIDE_FOR_MULTI_DECAL_ITEM(iDLCNameHash, sDLCTattooData.Collection, sDLCTattooData.Preset, eMenuOverride)
														eMenu = eMenuOverride
													ENDIF
													tlMenuLabel = GET_CLOTHES_MENU_NAME(eMenu, FALSE)
													IF DOES_TEXT_LABEL_EXIST(tlMenuLabel)
													
														IF eMenu = CLO_MENU_MASKS
														AND GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_MASK_TYPE_INVALID
															iSubMenu = GET_MASK_MENU_FROM_LABEL(componentItem.m_textLabel)
															GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
															
														ELIF eMenu = CLO_MENU_EARRINGS
														AND GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_EARRING_TYPE_INVALID
															iSubMenu = GET_EARRING_MENU_FROM_LABEL(componentItem.m_textLabel)
															GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
															
														ELIF eMenu = CLO_MENU_MP_AWARD_SHIRTS
														AND GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_AWARD_TYPE_INVALID
															iSubMenu = GET_AWARDS_MENU_FROM_LABEL(componentItem.m_textLabel)
															GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
															
														ELIF eMenu = CLO_MENU_MP_XMAS_TOPS
														AND GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_FESTIVE_TYPE_INVALID
															iSubMenu = GET_FESTIVE_MENU_FROM_LABEL(componentItem.m_textLabel)
															GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
															
														ELIF eMenu = CLO_MENU_MP_ARENA_WAR_TOPS
														AND GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel) != iSHOP_ARENA_WAR_TOP_TYPE_INVALID
															iSubMenu = GET_ARENA_WAR_TOP_MENU_FROM_LABEL(componentItem.m_textLabel)
															GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
															
														ENDIF
														
														SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
														SAVE_STRING_TO_DEBUG_FILE(" - ")
														SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlMenuLabel))
													ELSE
														SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
													ENDIF
													SAVE_STRING_TO_DEBUG_FILE("	")
													IF DOES_TEXT_LABEL_EXIST(sDLCTattooData.Label)
														// TEXT_LABEL
														SAVE_STRING_TO_DEBUG_FILE(sDLCTattooData.Label)
														SAVE_STRING_TO_DEBUG_FILE("	")
														// NAME
														SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(sDLCTattooData.Label))
														SAVE_STRING_TO_DEBUG_FILE("	")
													ELIF NOT IS_STRING_NULL_OR_EMPTY(sDLCTattooData.Label)
														// TEXT_LABEL
														SAVE_STRING_TO_DEBUG_FILE(sDLCTattooData.Label)
														SAVE_STRING_TO_DEBUG_FILE("	")
														// NAME
														SAVE_STRING_TO_DEBUG_FILE("	")
													ELSE
														SAVE_STRING_TO_DEBUG_FILE("		")
													ENDIF
													
													// AWARD_ITEM
													IF GIVE_DLC_CLOTHING_ITEM_FOR_FREE(DEFAULT, sDLCTattooData.Collection, sDLCTattooData.Preset)
														SAVE_STRING_TO_DEBUG_FILE("TRUE")
													ELSE
														SAVE_STRING_TO_DEBUG_FILE("FALSE")
													ENDIF
													SAVE_STRING_TO_DEBUG_FILE("	")
													
													// OVERLAY_ITEM
													SAVE_STRING_TO_DEBUG_FILE("TRUE")
													SAVE_STRING_TO_DEBUG_FILE("	")
													// OVERLAY_COUNT
													SAVE_INT_TO_DEBUG_FILE(0)
													SAVE_STRING_TO_DEBUG_FILE("	")
												ELSE
													// DLC_LOCK
													SAVE_STRING_TO_DEBUG_FILE("")
													// GLOBAL_ID
													SAVE_INT_TO_DEBUG_FILE(0)
													SAVE_STRING_TO_DEBUG_FILE("	")
													// LOCAL_ID
													SAVE_INT_TO_DEBUG_FILE(iRetNameHash)
													SAVE_STRING_TO_DEBUG_FILE("	")
													// TEXTURE_ID
													SAVE_STRING_TO_DEBUG_FILE("	")
													// ENUM
													SAVE_STRING_TO_DEBUG_FILE("	")
													// TEXT_LABEL
													SAVE_STRING_TO_DEBUG_FILE("	")
													// NAME
													SAVE_STRING_TO_DEBUG_FILE("	")
													// SHOP_MENU
													SAVE_STRING_TO_DEBUG_FILE("	")
													// AWARD_ITEM
													SAVE_STRING_TO_DEBUG_FILE("FALSE")
													SAVE_STRING_TO_DEBUG_FILE("	")
													// OVERLAY_ITEM
													SAVE_STRING_TO_DEBUG_FILE("TRUE")
													SAVE_STRING_TO_DEBUG_FILE("	")
													// OVERLAY_COUNT
													SAVE_INT_TO_DEBUG_FILE(0)
													SAVE_STRING_TO_DEBUG_FILE("	")
												ENDIF
												
												SAVE_NEWLINE_TO_DEBUG_FILE()
												
											ENDIF
										ENDREPEAT
									ENDIF	
								ENDREPEAT
							ENDIF
						ENDIF
					ENDREPEAT
					
					WAIT(0)
				ENDIF
			ENDIF
			
			WAIT(0)
		ENDREPEAT
		
		WAIT(0)
		
		INT iProp
		REPEAT NUMBER_OF_PED_PROP_TYPES iProp
		
			IF iSelectedPedComponent = NUMBER_OF_PED_COMP_TYPES
			OR iSelectedPedComponent = ENUM_TO_INT(COMP_TYPE_PROPS)
			
				iDrawCount = GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp))
				iDLCPackHash = -1
				
				REPEAT iDrawCount iDraw
				
					IF GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw) != iDLCPackHash
						iDLCPackHash = GET_DLC_PACK_HASH_FOR_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
						iLocalDrawOffset = iDraw
					ENDIF
				
					IF iDraw >= GET_DEFAULT_DLC_DRAW_FOR_PROP(ePedModel, INT_TO_ENUM(PED_PROP_POSITION, iProp))
					
						IF iSelectedDLCPack >= ENUM_TO_INT(DLC_PACK_MP_MAX)
						OR GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iSelectedDLCPack), ePedModel)) = iDLCPackHash
					
							iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, iProp), iDraw)
							REPEAT iTexCount iTex
								iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), iProp, iDraw, iTex)
								GET_SHOP_PED_PROP(iDLCNameHash, propItem)
								IF NOT GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
									tlNameHash = ""
									tlNameHash += iDLCNameHash
								ENDIF
								
								// COMPONENT
								SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, iProp)))
								SAVE_STRING_TO_DEBUG_FILE("	")
								// DLC_PACK
								IF GET_DLC_PACK_NAME_FROM_HASH(iDLCPackHash, tlPackName)
									SAVE_STRING_TO_DEBUG_FILE(tlPackName)
								ELSE
									SAVE_INT_TO_DEBUG_FILE(iDLCPackHash)
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("	")
								// DLC_KEY
								SAVE_STRING_TO_DEBUG_FILE("=HYPERLINK(\"Screenshots\\")
								SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
								SAVE_STRING_TO_DEBUG_FILE(".png\", \"")
								SAVE_STRING_TO_DEBUG_FILE(tlNameHash)
								SAVE_STRING_TO_DEBUG_FILE("\")")
								SAVE_STRING_TO_DEBUG_FILE("	")
								// DLC_LOCK
								SAVE_STRING_TO_DEBUG_FILE(GET_DLC_CONTENT_LOCK_NAME_FROM_HASH(propItem.m_lockHash))
								SAVE_STRING_TO_DEBUG_FILE("	")
								// GLOBAL_ID
								SAVE_INT_TO_DEBUG_FILE(iDraw)
								SAVE_STRING_TO_DEBUG_FILE("	")
								// LOCAL_ID
								SAVE_INT_TO_DEBUG_FILE(iDraw-iLocalDrawOffset)
								SAVE_STRING_TO_DEBUG_FILE("	")
								// TEXTURE_ID
								SAVE_INT_TO_DEBUG_FILE(iTex)
								SAVE_STRING_TO_DEBUG_FILE("	")
								// ENUM
								IF ePedModel = MP_M_FREEMODE_01
									SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, iDLCNameHash, COMP_TYPE_PROPS, 3)))
								ELSE
									SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, iDLCNameHash, COMP_TYPE_PROPS, 4)))
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("	")
								// SHOP_MENU
								IF propItem.m_locate = 0
									eMenu = CLO_MENU_MASKS
								ELSE
									eMenu = INT_TO_ENUM(CLOTHES_MENU_ENUM, propItem.m_locate)
								ENDIF
								tlMenuLabel = GET_CLOTHES_MENU_NAME(eMenu, FALSE)
								IF DOES_TEXT_LABEL_EXIST(tlMenuLabel)
								
									IF eMenu = CLO_MENU_MASKS
									AND GET_MASK_MENU_FROM_LABEL(propItem.m_textLabel) != iSHOP_MASK_TYPE_INVALID
										iSubMenu = GET_MASK_MENU_FROM_LABEL(propItem.m_textLabel)
										GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)
										
									ELIF eMenu = CLO_MENU_EARRINGS
									AND GET_EARRING_MENU_FROM_LABEL(propItem.m_textLabel) != iSHOP_EARRING_TYPE_INVALID
										iSubMenu = GET_EARRING_MENU_FROM_LABEL(propItem.m_textLabel)
										GET_SHOP_SUB_GROUP_LABEL(eMenu, iSubMenu, tlMenuLabel)	
									ENDIF
									
									SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
									SAVE_STRING_TO_DEBUG_FILE(" - ")
									SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlMenuLabel))
								ELSE
									SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eMenu))
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("	")
								IF DOES_TEXT_LABEL_EXIST(propItem.m_textLabel)
									// TEXT_LABEL
									SAVE_STRING_TO_DEBUG_FILE(propItem.m_textLabel)
									SAVE_STRING_TO_DEBUG_FILE("	")
									// NAME
									SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(propItem.m_textLabel))
									SAVE_STRING_TO_DEBUG_FILE("	")
								ELIF NOT IS_STRING_NULL_OR_EMPTY(propItem.m_textLabel)
									// TEXT_LABEL
									SAVE_STRING_TO_DEBUG_FILE(propItem.m_textLabel)
									SAVE_STRING_TO_DEBUG_FILE("	")
									// NAME
									SAVE_STRING_TO_DEBUG_FILE("	")
								ELSE
									SAVE_STRING_TO_DEBUG_FILE("		")
								ENDIF
								
								// AWARD
								IF GIVE_DLC_CLOTHING_ITEM_FOR_FREE(iDLCNameHash)
									SAVE_STRING_TO_DEBUG_FILE("TRUE")
								ELSE
									SAVE_STRING_TO_DEBUG_FILE("FALSE")
								ENDIF
								SAVE_STRING_TO_DEBUG_FILE("	")
								
								// OVERLAYS
								SAVE_STRING_TO_DEBUG_FILE("FALSE")
								SAVE_STRING_TO_DEBUG_FILE("	")
								
								// OVERLAY_COUNT
								SAVE_INT_TO_DEBUG_FILE(0)
								SAVE_STRING_TO_DEBUG_FILE("	")
								
								SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDREPEAT
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			WAIT(0)
		ENDREPEAT
		
		CLOSE_DEBUG_FILE()
		
		CLEAR_ADDITIONAL_TEXT(MENU_TEXT_SLOT, FALSE)
		
		bGenerate_ClothingInfo = FALSE
	ENDIF
	
ENDPROC

PED_INDEX tempPedID[5]
CAMERA_INDEX tempCameraID
MODEL_NAMES returnPedModel
INTERIOR_INSTANCE_INDEX tempInteriorID
TEXT_LABEL_63 tlPackName
TEXT_LABEL_63 tlModelName
TIME_DATATYPE tdTimer
INT iTimer

PROC PROCESS_PED_RENDERS()

	IF bGenerate_PedRenders
		
		IF NOT bRunning_PedRenders
		
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
			OR IS_NEW_LOAD_SCENE_ACTIVE()
			OR (NETWORK_IS_GAME_IN_PROGRESS() AND NOT IS_NET_PLAYER_OK(PLAYER_ID()))
				EXIT
			ENDIF
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			g_bReducedApartmentCreationTurnedOn = TRUE
			BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(TRUE)
			DISABLE_CELLPHONE(TRUE)
			HANG_UP_AND_PUT_AWAY_PHONE()
			
			SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEL, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)
			
			EXECUTE_CONTENT_CHANGESET_GROUP(HASH("mpHeist"), HASH("GROUP_MAP"))
			
			WHILE BUSYSPINNER_IS_ON()
				WAIT(0)
			ENDWHILE
			
			WAIT(0)
			
			tempInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<228.6058, -992.0537, -99.9999>>, "hei_dlc_garage_high_new")
			PIN_INTERIOR_IN_MEMORY(tempInteriorID)
			WHILE NOT IS_INTERIOR_READY(tempInteriorID) AND bGenerate_PedRenders
				WAIT(0)
			ENDWHILE
			CLEAR_AREA(<<234.7408, -995.2711, -99.9999>>, 100.0, TRUE)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<234.7408, -995.2711, -99.9999>>)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), tempInteriorID, HASH("GtaMloRoom001"))
			
			WAIT(0)
			
			IF NOT LOAD_XML_FILE("X:/gta5/titleupdate/dev_ng/common/data/script/xml/PedRenders/ped_pack_list.xml")
			OR GET_NUMBER_OF_XML_NODES() = 0
				DELETE_XML_FILE()
				bGenerate_PedRenders = FALSE
			ELSE
				iTotalPedRenders = GET_NUMBER_OF_XML_NODES()
				iCurrentPedRender = 0
				iCurrentPedRenderState = 0
				bRunning_PedRenders = TRUE
			ENDIF
		ELSE
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HELP_TEXT_THIS_FRAME()
			THEFEED_HIDE_THIS_FRAME()
			IF (iCurrentPedRenderState = 0)
			
				INT iNumAttributes
				INT iAttribute
				INT iAttributeHash
				
				IF iCurrentPedRender < iTotalPedRenders
					SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
						CASE HASH("pedItem")
							iNumAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
							REPEAT iNumAttributes iAttribute
								iAttributeHash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttribute))
								SWITCH iAttributeHash
									CASE HASH("model")
										tlModelName = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute)
										returnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tlModelName))
									BREAK
									CASE HASH("mpPack")
										tlPackName = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute)
									BREAK
								ENDSWITCH
							ENDREPEAT
						BREAK
					ENDSWITCH
					iCurrentPedRenderState++
				ELSE
					bGenerate_PedRenders = FALSE
				ENDIF
			ELIF (iCurrentPedRenderState = 1)
				IF ENUM_TO_INT(returnPedModel) = 0
				OR NOT IS_MODEL_VALID(returnPedModel)
					PRINTLN("[PED_RENDER] Model is not valid (", returnPedModel, ") ", tlModelName)
					iCurrentPedRenderState = -1
				ELIF iPedRenderDLCPack < ENUM_TO_INT(DLC_PACK_MP_MAX)
				AND GET_HASH_KEY(GET_SHOP_PED_APPAREL_DLC_NAME(INT_TO_ENUM(DLC_PACK_ENUM, iPedRenderDLCPack), returnPedModel)) != GET_HASH_KEY(tlPackName)
					PRINTLN("[PED_RENDER] Model not in selected pack (", returnPedModel, ") ", tlModelName)
					iCurrentPedRenderState = -1
				ELSE
					PRINTLN("[PED_RENDER] Loading model (", returnPedModel, ") ", tlModelName)
					REQUEST_MODEL(returnPedModel)
					IF HAS_MODEL_LOADED(returnPedModel)
						INT iPed
						VECTOR vCoords = <<234.7406, -995.2711, -98.9950>>
						BOOL bLeftSpawn
						REPEAT COUNT_OF(tempPedID) iPed
							IF bLeftSpawn
								vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, 90.0, <<0.8*iPed, 0.0, 0.0>>)
							ELSE
								vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, 90.0, <<-0.8*iPed, 0.0, 0.0>>)
							ENDIF
							bLeftSpawn = !bLeftSpawn
							tempPedID[iPed] = CREATE_PED(PEDTYPE_CIVMALE, returnPedModel, vCoords, 90.0, FALSE, FALSE)
							SET_ENTITY_COORDS_NO_OFFSET(tempPedID[iPed], vCoords)
							//SET_ENTITY_COLLISION(tempPedID[iPed], FALSE)
							FORCE_ROOM_FOR_ENTITY(tempPedID[iPed], tempInteriorID, HASH("GtaMloRoom001"))
							
							IF iPed != 0
								SET_PED_RANDOM_COMPONENT_VARIATION(tempPedID[iPed])
							ENDIF
						ENDREPEAT
						
						SET_MODEL_AS_NO_LONGER_NEEDED(returnPedModel)
						
						iTimer = GET_GAME_TIMER()
						IF NETWORK_IS_GAME_IN_PROGRESS()
							tdTimer = GET_NETWORK_TIME()
						ENDIF
						
						tempCameraID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
						SET_CAM_FOV(tempCameraID, 21.2161)
						SET_CAM_COORD(tempCameraID, <<228.7997, -995.3265, -97.7907>>)
						SET_CAM_ROT(tempCameraID, <<-12.8226, -0.0000, -89.1094>>)
						SET_CAM_ACTIVE(tempCameraID, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						iCurrentPedRenderState++
					ENDIF
				ENDIF
			ELIF (iCurrentPedRenderState = 2)
			
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(0.0, 0.6)
				SET_TEXT_LEADING(2)
				SET_TEXT_COLOUR(iR, iG, iB, iA)
				SET_TEXT_CENTRE(FALSE)
			    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			    SET_TEXT_EDGE(0, 0, 0, 0, 0)
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("TWOSTRINGSNL")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlPackName)
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(tlModelName)
				END_TEXT_COMMAND_DISPLAY_TEXT(0.05, 0.9)
			
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdTimer)) > 200)
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_GAME_TIMER()-iTimer) > 200)
					
					INT iPed
					BOOL bStreamingComplete = TRUE
					REPEAT COUNT_OF(tempPedID) iPed
						IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(tempPedID[iPed])
							bStreamingComplete = FALSE
						ENDIF
					ENDREPEAT
					
					IF bStreamingComplete
						TEXT_LABEL_63 tlScreenshot = "Renders/"
						tlScreenshot += tlPackName
						tlScreenshot += "_"
						tlScreenshot += tlModelName
						SAVE_SCREENSHOT(tlScreenshot)
						iCurrentPedRenderState++
					ENDIF
				ENDIF
			ELSE
				DESTROY_CAM(tempCameraID)
				INT iPed
				REPEAT COUNT_OF(tempPedID) iPed
					IF DOES_ENTITY_EXIST(tempPedID[iPed])
						DELETE_PED(tempPedID[iPed])
					ENDIF
				ENDREPEAT
				iCurrentPedRenderState = 0
				
				iCurrentPedRender++
				IF iCurrentPedRender < iTotalPedRenders
					GET_NEXT_XML_NODE()
				ENDIF
			ENDIF
		ENDIF
		
	ELIF bRunning_PedRenders
		
		IF DOES_CAM_EXIST(tempCameraID)
			DESTROY_CAM(tempCameraID)
		ENDIF
		INT iPed
		REPEAT COUNT_OF(tempPedID) iPed
			IF DOES_ENTITY_EXIST(tempPedID[iPed])
				DELETE_PED(tempPedID[iPed])
			ENDIF
		ENDREPEAT
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DISABLE_CELLPHONE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		
		DELETE_XML_FILE()
		
		iCurrentPedRender = 0
		iCurrentPedRenderState = 0
		bRunning_PedRenders = FALSE
	ENDIF
ENDPROC

PROC DO_PROCESSING()

	PROCESS_CLOTHING_SELECT_MENU()
	
	PROCESS_CLOTHING_DEBUG_OUTPUT()
	
	PROCESS_PED_RENDERS()
	
	IF bUseOffsetCam
		IF NOT DOES_CAM_EXIST(camPlayerOffset)
			camPlayerOffset = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			ATTACH_CAM_TO_ENTITY(camPlayerOffset, PLAYER_PED_ID(), vCamOffsetPos)
			POINT_CAM_AT_ENTITY(camPlayerOffset, PLAYER_PED_ID(), vCamOffsetPoint)
			SET_CAM_ACTIVE(camPlayerOffset, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ELSE
		
			IF bSetOffsetCamPreset[0]
				vCamOffsetPos = <<-0.163, 0.448, 0.753>>
				vCamOffsetPoint = <<0.0, 0.0, 0.6>>
				bSetOffsetCamPreset[0] = FALSE
			ENDIF
			IF bSetOffsetCamPreset[1]
				vCamOffsetPos = <<0.163, -0.448, 0.753>>
				vCamOffsetPoint = <<0.0, 0.0, 0.6>>
				bSetOffsetCamPreset[1] = FALSE
			ENDIF
			
			IF bSetOffsetCamPreset[2]
				vCamOffsetPos = <<0.163, -0.950, 0.545>>
				vCamOffsetPoint = <<0.0, 0.0, 0.265>>
				bSetOffsetCamPreset[2] = FALSE
			ENDIF
			IF bSetOffsetCamPreset[3]
				vCamOffsetPos = <<-0.163, 0.950, 0.545>>
				vCamOffsetPoint = <<0.0, 0.0, 0.265>>
				bSetOffsetCamPreset[3] = FALSE
			ENDIF
			
			IF bSetOffsetCamPreset[4]
				vCamOffsetPos = <<-0.163, 0.96, -0.12>>
				vCamOffsetPoint = <<0.0, 0.0, -0.42>>
				bSetOffsetCamPreset[4] = FALSE
			ENDIF
			IF bSetOffsetCamPreset[5]
				vCamOffsetPos = <<0.163, -0.96, -0.12>>
				vCamOffsetPoint = <<0.0, 0.0, -0.42>>
				bSetOffsetCamPreset[5] = FALSE
			ENDIF
			
			IF bSetOffsetCamPreset[6]
				vCamOffsetPos = <<-0.163, 0.96, -0.45>>
				vCamOffsetPoint = <<0.0, 0.0, -0.87>>
				bSetOffsetCamPreset[6] = FALSE
			ENDIF
			IF bSetOffsetCamPreset[7]
				vCamOffsetPos = <<0.163, -0.96, -0.45>>
				vCamOffsetPoint = <<0.0, 0.0, -0.87>>
				bSetOffsetCamPreset[7] = FALSE
			ENDIF
			
			DETACH_CAM(camPlayerOffset)
			ATTACH_CAM_TO_ENTITY(camPlayerOffset, PLAYER_PED_ID(), vCamOffsetPos)
			POINT_CAM_AT_ENTITY(camPlayerOffset, PLAYER_PED_ID(), vCamOffsetPoint)
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(camPlayerOffset)
			DESTROY_CAM(camPlayerOffset)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
	ENDIF
	
	IF bKillScript
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF b_Apply_Freemode_outfit
		SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
		b_Apply_Freemode_outfit = FALSE
	ENDIF

	IF b_debug_output_ped_save_data
		OUTPUT_PED_DEBUG_OUTPUT()
	ENDIF
	
	INT iPedSave
	REPEAT COUNT_OF(b_debug_ped_save) iPedSave
		IF b_debug_ped_save[iPedSave]
			PROCESS_PED_SAVE_DEBUG(iPedSave)
		ELIF b_debug_ped_set[iPedSave]
			IF NOT IS_BIT_SET(i_debug_process_ped_set, iPedSave)
				IF PROCESS_PED_LOAD_DEBUG(iPedSave)
					SET_BIT(i_debug_process_ped_set, iPedSave)
				ELSE
					b_debug_ped_set[iPedSave] = FALSE
				ENDIF
			ELSE
				PROCESS_PED_SET_DEBUG(iPedSave)
			ENDIF
		ELIF b_debug_ped_create[iPedSave]
			IF NOT IS_BIT_SET(i_debug_process_ped_create, iPedSave)
				IF PROCESS_PED_LOAD_DEBUG(iPedSave)
					SET_BIT(i_debug_process_ped_create, iPedSave)
				ELSE
					b_debug_ped_create[iPedSave] = FALSE
				ENDIF
			ELSE
				PROCESS_PED_CREATE_DEBUG(iPedSave)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF b_debug_ped_randomise_appearance
		PROCESS_PED_RANDOMISE_DEBUG()
	ENDIF
	
	IF b_debug_ped_create_clone
		PROCESS_PED_CREATE_CLONE_DEBUG()
	ENDIF
	
	
	IF b_debug_outfit_output_1
		OUTPUT_OUTFIT_SETUP_HEIST()
		b_debug_outfit_output_1 = FALSE
	ENDIF
	
	IF b_debug_htb
		SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
			CASE MP_M_FREEMODE_01
				APPLY_MALE_BIGFOOT_CLOTHES()
			BREAK
			CASE MP_F_FREEMODE_01
				APPLY_FEMALE_BIGFOOT_CLOTHES()
			BREAK
		ENDSWITCH
		b_debug_htb = FALSE
	ENDIF
	
	
	IF g_DebugGenerateItemData.b_output_item_data
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("<!-- ")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemDesc))SAVE_STRING_TO_DEBUG_FILE(" -->")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iDraw = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_item_draw_component))
		INT iTexCount, iTex
			
		iTexCount = GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_item_draw_component), iDraw)
		REPEAT iTexCount iTex
			SAVE_STRING_TO_DEBUG_FILE("<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<cost value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_cost)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<textLabel>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemLabel))
				SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_draw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)
				SAVE_STRING_TO_DEBUG_FILE("</textLabel>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<uniqueNameHash>")
				SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemHash))
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					SAVE_STRING_TO_DEBUG_FILE("_M_")
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("_F_")
				ENDIF
				SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_NAME_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_item_draw_component))))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_draw)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)
			SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<locate value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_locate)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<scriptSaveData value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag1))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag2))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag3))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag4))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag5))
				SAVE_STRING_TO_DEBUG_FILE("	<restrictionTags>")SAVE_NEWLINE_TO_DEBUG_FILE()
				
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag1))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag1))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag2))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag2))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag3))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag3))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag4))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag4))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag5))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemTag5))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("	</restrictionTags>")SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	<restrictionTags />")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			IF g_DebugGenerateItemData.i_forced_component1 < 12
			OR g_DebugGenerateItemData.i_forced_component2 < 12
			OR g_DebugGenerateItemData.i_forced_component3 < 12
				SAVE_STRING_TO_DEBUG_FILE("	<forcedComponents>")SAVE_NEWLINE_TO_DEBUG_FILE()
				
				IF g_DebugGenerateItemData.i_forced_component1 < 12
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced1))
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced1))SAVE_STRING_TO_DEBUG_FILE("</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ELSE
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>0</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
					SAVE_STRING_TO_DEBUG_FILE("			<enumValue value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_forced_comp_enum1)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_forced_component1))))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF g_DebugGenerateItemData.i_forced_component2 < 12
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced2))
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced2))SAVE_STRING_TO_DEBUG_FILE("</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ELSE
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>0</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
					SAVE_STRING_TO_DEBUG_FILE("			<enumValue value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_forced_comp_enum2)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_forced_component2))))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF g_DebugGenerateItemData.i_forced_component3 < 12
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced3))
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemForced3))SAVE_STRING_TO_DEBUG_FILE("</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ELSE
						SAVE_STRING_TO_DEBUG_FILE("			<nameHash>0</nameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
					SAVE_STRING_TO_DEBUG_FILE("			<enumValue value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_forced_comp_enum3)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_forced_component3))))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("	</forcedComponents>")SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	<forcedComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			SAVE_STRING_TO_DEBUG_FILE("	<variantComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<variantProps />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<drawableIndex value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<localDrawableIndex value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_draw)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, g_DebugGenerateItemData.i_item_draw_component))))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<textureIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<isInOutfit value=\"false\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
	
		g_DebugGenerateItemData.b_output_item_data = FALSE
	ENDIF
	
	IF g_DebugGenerateItemData.b_output_prop_item_data
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("<!-- ")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemDescProp))SAVE_STRING_TO_DEBUG_FILE(" -->")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		INT iDraw = GET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, g_DebugGenerateItemData.i_item_draw_prop))
		INT iTexCount, iTex
			
		iTexCount = GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, g_DebugGenerateItemData.i_item_draw_prop), iDraw)
		REPEAT iTexCount iTex
			SAVE_STRING_TO_DEBUG_FILE("<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<cost value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_prop_cost)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<textLabel>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropLabel))
				SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_index)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)
				SAVE_STRING_TO_DEBUG_FILE("</textLabel>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<uniqueNameHash>")
				SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropHash))
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					SAVE_STRING_TO_DEBUG_FILE("_M_")
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("_F_")
				ENDIF
				SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_NAME_STRING(INT_TO_ENUM(PED_PROP_POSITION, g_DebugGenerateItemData.i_item_draw_prop)))SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_index)SAVE_STRING_TO_DEBUG_FILE("_")SAVE_INT_TO_DEBUG_FILE(iTex)
			SAVE_STRING_TO_DEBUG_FILE("</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<locate value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_prop_locate)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<scriptSaveData value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag1))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag2))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag3))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag4))
			OR NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag5))
				SAVE_STRING_TO_DEBUG_FILE("	<restrictionTags>")SAVE_NEWLINE_TO_DEBUG_FILE()
				
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag1))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag1))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag2))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag2))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag3))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag3))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag4))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag4))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag5))
					SAVE_STRING_TO_DEBUG_FILE("		<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("			<tagNameHash>")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(g_DebugGenerateItemData.twItemPropTag5))SAVE_STRING_TO_DEBUG_FILE("</tagNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("		</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("	</restrictionTags>")SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	<restrictionTags />")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			SAVE_STRING_TO_DEBUG_FILE("	<forcedComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<variantComponents />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<variantProps />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<drawableIndex value=\"0\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<localPropIndex value=\"")SAVE_INT_TO_DEBUG_FILE(g_DebugGenerateItemData.i_item_index)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<eAnchorPoint>")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, g_DebugGenerateItemData.i_item_draw_prop)))SAVE_STRING_TO_DEBUG_FILE("</eAnchorPoint>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<textureIndex value=\"")SAVE_INT_TO_DEBUG_FILE(iTex)SAVE_STRING_TO_DEBUG_FILE("\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	<isInOutfit value=\"false\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		
		g_DebugGenerateItemData.b_output_prop_item_data = FALSE
	ENDIF
	
	IF b_debug_outfit_output_2
		OUTPUT_OUTFIT_SETUP_META()
		b_debug_outfit_output_2 = FALSE
	ENDIF
	
	sApplyOutfitData.pedID   = PLAYER_PED_ID()
	sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_heist_outfit)
	
	sApplyMaskData.pedID   	= PLAYER_PED_ID()	
	sApplyMaskData.eMask	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, i_debug_heist_mask)
	
	IF b_debug_set_Vs_outfit		
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_Vs_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
		ENDIF	
	ENDIF
	IF b_debug_set_VsTheme_outfit		
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_VsTheme_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
		ENDIF	
	ENDIF	
	IF b_debug_set_ng_outfit		
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_NG_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
		ENDIF	
	ENDIF
	IF b_debug_set_lowr_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_lowr_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_entourage_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_entourage_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_entourage_outfit = FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_hal_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_hal_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_hal_outfit		= FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_solo_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_solo_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_solo_outfit	= FALSE
			b_debug_set_solo_outfit2	= FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_solo_outfit2
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_solo_outfit2)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_solo_outfit	= FALSE
			b_debug_set_solo_outfit2	= FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_low_flow_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_low_flow_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_beast_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_beast_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_beast_outfit	= FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_extraction_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_extraction_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 		= FALSE
			b_debug_set_heist_outfit 		= FALSE
			b_debug_set_heist_outfit_auto 	= FALSE
			b_debug_set_Vs_outfit 			= FALSE
			b_debug_set_ng_outfit 			= FALSE
			b_debug_set_lowr_outfit			= FALSE
			b_debug_set_low_flow_outfit 	= FALSE
			b_debug_set_extraction_outfit	= FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gang_boss_vip_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gang_boss_vip_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_gang_boss_vip_outfits = FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_gang_boss_bodyguard_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gang_boss_bodyguard_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_gang_boss_bodyguard_outfits = FALSE
		ENDIF		
	ENDIF
	IF b_debug_set_hidden_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_hidden_outfits)
		SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_hidden_outfit_name, GET_MP_OUTFIT_NAME_FROM_ENUM(INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_hidden_outfits)))
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_hidden_lowrider_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_hidden_lowrider_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_classic_competitors_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_classic_competitors_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_trading_places_winner_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_trading_places_winner_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_trading_places_loser_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_trading_places_loser_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_power_play_team_sport_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_power_play_team_sport_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_executive_ceo_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_executive_ceo_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_executive_associate_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_executive_associate_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_executive_securoserv_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_executive_securoserv_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_power_play_themed_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_power_play_themed_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_stunt_race_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_stunt_race_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_club_outfits_0to3
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_club_outfits_0to3)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_club_outfits_4to7
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_club_outfits_4to7)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_deadline_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_deadline_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_slipstream_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_slipstream_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_lost_damned_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_lost_damned_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_biker_club_rank_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_biker_club_rank_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_impexp_ceo_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_impexp_ceo_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_impexp_associate_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_impexp_associate_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_impexp_juggernaut_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_impexp_juggernaut_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_impexp_juggernaut_orange_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_impexp_juggernaut_orange_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_impexp_juggernaut_purple_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_impexp_juggernaut_purple_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_specraces_general_combat_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_specraces_general_combat_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_specraces_land_grab_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_specraces_land_grab_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_lions_den_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_lions_den_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_dawn_raid_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_dawn_raid_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_team_general_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_team_general_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_power_mad_juggernaut_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_power_mad_juggernaut_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_power_mad_juggernaut_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_power_mad_team_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_power_mad_team_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_power_mad_juggernaut_outfits = FALSE
			b_debug_set_gunrun_power_mad_team_outfits = FALSE
		ENDIF
	ENDIF
	
	IF b_debug_set_gunrun_biker_zombie_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_biker_zombie_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
			b_debug_set_gunrun_biker_zombie_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_biker_raider_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_biker_raider_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_biker_puffer_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_biker_puffer_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
			b_debug_set_gunrun_biker_zombie_outfits = FALSE
			b_debug_set_gunrun_biker_puffer_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_biker_hillbilly_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_biker_hillbilly_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
			b_debug_set_gunrun_biker_zombie_outfits = FALSE
			b_debug_set_gunrun_biker_hillbilly_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_ceo_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_ceo_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
			b_debug_set_gunrun_ceo_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_associate_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_associate_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_heist_outfit_auto = FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_lowr_outfit		= FALSE
			b_debug_set_low_flow_outfit = FALSE
			b_debug_set_hidden_outfits 	= FALSE
			b_debug_set_hidden_lowrider_outfits = FALSE
			b_debug_set_classic_competitors_outfits = FALSE
			b_debug_set_trading_places_winner_outfits = FALSE
			b_debug_set_trading_places_loser_outfits = FALSE
			b_debug_set_power_play_team_sport_outfits = FALSE
			b_debug_set_executive_ceo_outfits = FALSE
			b_debug_set_executive_associate_outfits = FALSE
			b_debug_set_executive_securoserv_outfits = FALSE
			b_debug_set_power_play_themed_outfits = FALSE
			b_debug_set_stunt_race_outfits = FALSE
			b_debug_set_biker_club_outfits_0to3 = FALSE
			b_debug_set_biker_club_outfits_4to7 = FALSE
			b_debug_set_biker_deadline_outfits = FALSE
			b_debug_set_biker_slipstream_outfits = FALSE
			b_debug_set_biker_lost_damned_outfits = FALSE
			b_debug_set_biker_club_rank_outfits = FALSE
			b_debug_set_impexp_ceo_outfits = FALSE
			b_debug_set_impexp_associate_outfits = FALSE
			b_debug_set_impexp_juggernaut_outfits = FALSE
			b_debug_set_impexp_juggernaut_orange_outfits = FALSE
			b_debug_set_impexp_juggernaut_purple_outfits = FALSE
			b_debug_set_specraces_general_combat_outfits = FALSE
			b_debug_set_specraces_land_grab_outfits = FALSE
			b_debug_set_gunrun_lions_den_outfits = FALSE
			b_debug_set_gunrun_dawn_raid_outfits = FALSE
			b_debug_set_gunrun_team_general_outfits = FALSE
			b_debug_set_gunrun_biker_raider_outfits = FALSE
			b_debug_set_gunrun_ceo_outfits = FALSE
			b_debug_set_gunrun_associate_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gunrun_wvm_oppressor_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gunrun_wvm_oppressor_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gunrun_wvm_oppressor_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_hostile_takeover_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_hostile_takeover_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_hostile_takeover_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_condemned_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_condemned_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_condemned_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_vehicle_warfare_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_vehicle_warfare_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_vehicle_warfare_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_air_shootout_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_air_shootout_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_air_shootout_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_bombushka_run_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_bombushka_run_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_bombushka_run_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_stockpile_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_stockpile_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_stockpile_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_race_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_race_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_race_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_ceo_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_ceo_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_ceo_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_associate_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_associate_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_associate_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_biker_casual_pilot_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_biker_casual_pilot_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_biker_casual_pilot_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_smuggler_biker_pirate_outfits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_smuggler_biker_pirate_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_smuggler_biker_pirate_outfits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_under_control_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_under_control_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_under_control_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_scuba_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_scuba_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_scuba_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_scuba2_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_scuba2_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_scuba2_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_paramedic_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_paramedic_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_paramedic_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_medtech1_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_medtech1_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_medtech1_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_medtech2_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_medtech2_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_medtech2_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_modern_stealth_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_modern_stealth_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_modern_stealth_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_casual_pilot_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_casual_pilot_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_casual_pilot_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_fighter_pilot_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_fighter_pilot_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_fighter_pilot_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_high_tech_riot_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_high_tech_riot_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_high_tech_riot_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_high_tech_impact_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_high_tech_impact_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_high_tech_impact_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_med_tech_masked_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_med_tech_masked_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_med_tech_masked_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_med_tech_rebellion_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_med_tech_rebellion_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_med_tech_rebellion_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_med_tech_havoc_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_med_tech_havoc_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_med_tech_havoc_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_med_tech_adaptable_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_med_tech_adaptable_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_med_tech_adaptable_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_sub_driver_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_sub_driver_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_sub_driver_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_heavy_combat_gear_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_heavy_combat_gear_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_heavy_combat_gear_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_low_tech_tactical_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_low_tech_tactical_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_low_tech_tactical_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_low_tech_combat_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_low_tech_combat_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_low_tech_combat_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_classic_stealth_gear_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_classic_stealth_gear_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_classic_stealth_gear_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_military_camo_gear_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_military_camo_gear_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_military_camo_gear_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_heist_gorka_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_heist_gorka_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_heist_gorka_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_slashers_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_slashers_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_slashers_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_hard_target_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_hard_target_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_hard_target_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_gangops_air_quota_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_gangops_air_quota_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_gangops_air_quota_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_target_races_showdown_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_target_races_showdown_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_target_races_showdown_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_target_races_trapdoor_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_target_races_trapdoor_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_target_races_trapdoor_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_target_races_venetian_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_target_races_venetian_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_target_races_venetian_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_target_races_venetian_firesuits
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_target_races_venetian_firesuits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_target_races_venetian_firesuits = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_drop_the_bomb_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_drop_the_bomb_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_drop_the_bomb_outfit = FALSE
		ENDIF		
	ENDIF

	IF b_debug_set_business_battles_sumo_run_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_sumo_run_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_sumo_run_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_offense_defense_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_offense_defense_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_offense_defense_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_hunting_pack_remix_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_hunting_pack_remix_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_hunting_pack_remix_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_trading_places_remix_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_trading_places_remix_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_trading_places_remix_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_come_out_to_play_remix_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_come_out_to_play_remix_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_come_out_to_play_remix_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_running_back_remix_2_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_running_back_remix_2_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_running_back_remix_2_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_nightclub_chiliad_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_nightclub_chiliad_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_nightclub_chiliad_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_business_battles_nightclub_kifflom_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_business_battles_nightclub_kifflom_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_business_battles_nightclub_kifflom_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_arena_wars_contender_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_arena_wars_contender_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_arena_wars_contender_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_impotent_rage_outfit
		sApplyOutfitData.eOutfit = OUTFIT_CASINO_IMPOTENT_RAGE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_impotent_rage_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_highroller_outfit
		sApplyOutfitData.eOutfit = OUTFIT_CASINO_HIGHROLLER
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_highroller_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_spaceling_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_spaceling_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_spaceling_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_undertaker_outfit
		sApplyOutfitData.eOutfit = OUTFIT_CASINO_HEIST_UNDERTAKER
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_undertaker_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_light_i_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_light_i_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_light_i_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_light_ii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_light_ii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_light_ii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_light_iii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_light_iii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_light_iii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_heavy_i_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_heavy_i_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_heavy_i_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_heavy_ii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_heavy_ii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_heavy_ii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_direct_heavy_iii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_direct_heavy_iii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_direct_heavy_iii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_fib_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_fib_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_fib_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_stealth_i_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_stealth_i_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_stealth_i_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_stealth_ii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_stealth_ii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_stealth_ii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_stealth_iii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_stealth_iii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_stealth_iii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_covert_stealth_i_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_covert_stealth_i_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_covert_stealth_i_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_covert_stealth_ii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_covert_stealth_ii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_covert_stealth_ii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_covert_stealth_iii_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_covert_stealth_iii_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_covert_stealth_iii_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_valet_outfit
		sApplyOutfitData.eOutfit = OUTFIT_CASINO_HEIST_VALET
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_valet_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_firefighter_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_firefighter_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_firefighter_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_noose_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_noose_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_noose_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_gruppe_sechs_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_gruppe_sechs_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_gruppe_sechs_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_bugstars_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_bugstars_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_bugstars_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_celeb_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_celeb_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_celeb_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_maintenance_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_maintenance_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_maintenance_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_prison_guard_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_prison_guard_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_prison_guard_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_casino_heist_high_roller_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_casino_heist_high_roller_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_casino_heist_high_roller_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_sum20_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_sum20_outfit)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_sum20_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_guard_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_guard_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_guard_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_smuggler_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_smuggler_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_smuggler_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_heavy_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_heavy_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_heavy_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_light_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_light_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_light_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_stealth_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_stealth_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_stealth_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_island_heist_beach_party_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_island_heist_beach_party_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_island_heist_beach_party_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_tuner_robber_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_tuner_robber_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_tuner_robber_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_tuner_security_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_tuner_security_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_tuner_security_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_tuner_lost_mc_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_tuner_lost_mc_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_tuner_lost_mc_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_tuner_dock_worker_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_tuner_dock_worker_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_tuner_dock_worker_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_fixer_setup_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_fixer_setup_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_fixer_setup_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_fixer_party_promoter_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_fixer_party_promoter_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_fixer_party_promoter_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_fixer_billionaire_games_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_fixer_billionaire_games_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_fixer_billionaire_games_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_fixer_golf_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_fixer_golf_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_fixer_golf_outfit = FALSE
		ENDIF		
	ENDIF
	
	IF b_debug_set_heist_navy_coveralls
		sApplyOutfitData.eOutfit = OUTFIT_HEIST_COVERALLS_NAVY
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_heist_navy_coveralls = FALSE
		ENDIF		
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF b_debug_set_ld_organics_award
		sApplyOutfitData.eOutfit = OUTFIT_LD_ORGANICS_AWARD_0
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_ld_organics_award = FALSE
		ENDIF
	ENDIF
	
	IF b_debug_set_sum22_iaa_agent_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_sum22_iaa_agent_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_sum22_iaa_agent_outfit = FALSE
		ENDIF
	ENDIF
	
	IF b_debug_set_sum22_halloween_riders_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_sum22_halloween_riders_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_sum22_halloween_riders_outfit = FALSE
		ENDIF
	ENDIF
	
	IF b_debug_set_sum22_halloween_hunted_outfit
		sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, i_debug_sum22_halloween_hunted_outfits)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			b_debug_set_sum22_halloween_hunted_outfit = FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF b_debug_set_heist_outfit					
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyOutfitData)
			SET_CONTENTS_OF_TEXT_WIDGET(t_debug_heist_outfit_name, GET_MP_OUTFIT_NAME_FROM_ENUM(sApplyOutfitData.eOutfit))
			
			i_debug_heist_outfit_last_set = i_debug_heist_outfit
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
		ENDIF	
	ENDIF		
	IF b_debug_set_heist_outfit_auto
		IF i_debug_heist_outfit != i_debug_heist_outfit_last_set
			b_debug_set_heist_outfit = TRUE
		ENDIF
	ENDIF
	
	IF b_debug_set_heist_mask				
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		and	SET_PED_MP_OUTFIT(sApplyMaskData,true,true)
			
			SET_CONTENTS_OF_TEXT_WIDGET(t_debug_heist_mask_name, GET_MP_OUTFIT_MASK_NAME(sApplyOutfitData.eMask))				
			i_debug_heist_mask_last_set = i_debug_heist_mask
			b_debug_set_heist_mask 		= FALSE
			b_debug_set_VsTheme_outfit 	= FALSE
			b_debug_set_heist_outfit 	= FALSE
			b_debug_set_Vs_outfit 		= FALSE
			b_debug_set_ng_outfit 		= FALSE
			b_debug_set_heist_outfit 	= FALSE
		ENDIF	
	ENDIF		
	IF b_debug_set_heist_mask_auto
		IF i_debug_heist_mask != i_debug_heist_mask_last_set
			b_debug_set_heist_mask = TRUE
		ENDIF
	ENDIF
	
	IF b_debug_set_heist_gear
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_MP_HEIST_GEAR(PLAYER_PED_ID(), INT_TO_ENUM(MP_HEIST_GEAR_ENUM, i_debug_heist_gear))
		ENDIF
		b_debug_set_heist_gear = FALSE
	ENDIF
	IF b_debug_remove_heist_gear
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			REMOVE_MP_HEIST_GEAR(PLAYER_PED_ID(), INT_TO_ENUM(MP_HEIST_GEAR_ENUM, i_debug_heist_gear))
		ENDIF
		b_debug_remove_heist_gear = FALSE
	ENDIF
	
	IF b_debug_output_heist_DLC_hash
		INT i
		INT iNameHash
		MP_OUTFITS_DATA sOutfitsData			
		IF GET_MP_OUTFIT_DATA(sOutfitsData,sApplyOutfitData)
			PRINTLN("*****|HESIT OUTFIT HASH DATA - ", i_debug_heist_outfit)
			
			REPEAT NUM_PED_COMPONENTS i
				IF sOutfitsData.iComponentDrawableID[i] != -1
					iNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), i, sOutfitsData.iComponentDrawableID[i], sOutfitsData.iComponentTextureID[i])
					IF iNameHash != 0
						PRINTLN("*****|GET_SHOP_PED_COMPONENT(", iNameHash, ", componentItem)")
						PRINTLN("*****|sOutfitsData.iComponentDrawableID[", GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "]		= componentItem.m_drawableIndex 			sOutfitsData.iComponentTextureID[", GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "]			= componentItem.m_textureIndex")
					ELSE
						PRINTLN("*****|sOutfitsData.iComponentDrawableID[", GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "]		= ", sOutfitsData.iComponentDrawableID[i], " 			sOutfitsData.iComponentTextureID[", GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "]			= ", sOutfitsData.iComponentTextureID[i])
					ENDIF
				ENDIF
			ENDREPEAT
			
			REPEAT COUNT_OF(sOutfitsData.iPropDrawableID) i
				IF sOutfitsData.iPropDrawableID[i] != -1
					iNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), i, sOutfitsData.iPropDrawableID[i], sOutfitsData.iPropTextureID[i])
					IF iNameHash != 0
						PRINTLN("*****|GET_SHOP_PED_PROP(", iNameHash, ", propItem)")
						PRINTLN("*****|sOutfitsData.iPropDrawableID[", GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, i)), "]		= propItem.m_propIndex 			sOutfitsData.iPropTextureID[", GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, i)), "]			= propItem.m_textureIndex")
					ELSE
						PRINTLN("*****|sOutfitsData.iPropDrawableID[", GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, i)), "]		= ", sOutfitsData.iPropDrawableID[i], " 			sOutfitsData.iPropTextureID[", GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, i)), "]			= ", sOutfitsData.iPropTextureID[i])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		b_debug_output_heist_DLC_hash = FALSE
	ENDIF
	
	IF b_debug_set_player_and_cam[0]
	OR b_debug_set_player_and_cam[1]
	OR b_debug_set_player_and_cam[2]
	OR b_debug_set_player_and_cam[3]
		CAMERA_INDEX camID = GET_DEBUG_CAM()
		IF DOES_CAM_EXIST(camID)
		AND IS_CAM_ACTIVE(camID)
		
			VECTOR vPlayerCoords
			FLOAT fPlayerHeading
			
			VECTOR vCamCoords
			VECTOR vCamRot
			FLOAT fCamFOV
			
			
			IF b_debug_set_player_and_cam[0]
				vPlayerCoords = << 235.289825, -994.445435, -98.999924 >>
				fPlayerHeading = 50.449604
				vCamCoords = << 233.612976, -993.258911, -98.426796 >>
				vCamRot = << -16.813923, -0.000001, -129.191162 >>
				fCamFOV = 50.000000
			ELIF b_debug_set_player_and_cam[1]
				vPlayerCoords = << 235.289825, -994.445435, -98.999924 >>
				fPlayerHeading = 50.449604
				vCamCoords = << 233.592545, -993.242249, -98.357346 >>
				vCamRot = << -16.471943, -0.000000, -129.191162 >>
				fCamFOV = 50.000000
			ELIF b_debug_set_player_and_cam[2]
				vPlayerCoords = << 235.289825, -994.445435, -98.999924 >>
				fPlayerHeading = 50.449604
				vCamCoords = << 233.500809, -993.167358, -98.320786 >>
				vCamRot = << -15.333229, -0.000000, -129.191162 >>
				fCamFOV = 50.000000
			ELIF b_debug_set_player_and_cam[3]
				vPlayerCoords = << 235.289825, -994.445435, -98.999924 >>
				fPlayerHeading = 50.449600
				vCamCoords = << 234.969223, -994.042786, -98.275002 >>
				vCamRot = << -8.781961, -0.000001, -142.976669 >>
				fCamFOV = 50.000000
			ENDIF
			
			SET_CAM_COORD(camID, vCamCoords)
			SET_CAM_ROT(camID, vCamRot)
			SET_CAM_FOV(camID, fCamFOV)
			
			SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), vPlayerCoords)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerHeading)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		ENDIF
		b_debug_set_player_and_cam[0] = FALSE
		b_debug_set_player_and_cam[1] = FALSE
		b_debug_set_player_and_cam[2] = FALSE
		b_debug_set_player_and_cam[3] = FALSE
	ENDIF
	
ENDPROC

PROC DO_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	WHILE TRUE
	
		WAIT(0)
		
		SWITCH eStage
			CASE STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE STAGE_PROCESSING
				DO_PROCESSING()
			BREAK
			
			CASE STAGE_CLEANUP
				DO_CLEANUP()
			BREAK
		ENDSWITCH
		
	ENDWHILE
	
ENDSCRIPT

#ENDIF
