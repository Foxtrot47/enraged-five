
#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Fraser Morgan				Date: 30/07/2012			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Ped Walking Script									│
//│																				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

CONST_INT NUMBER_OF_PEDS 200

USING "rage_builtins.sch"
USING "globals.sch" 
USING "brains.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_task.sch"
USING "commands_object.sch"
USING "commands_physics.sch"
USING "commands_player.sch"
USING "commands_misc.sch"
USING "flow_public_core_override.sch"
USING "weapon_enums.sch"


VECTOR vFront, vSide, vUp, vPos
VECTOR vPedPos, vTransformedPedPos
VECTOR vFrontPed, vSidePed, vUpPed, vPosPed
VECTOR vLeftSide
VECTOR vPedOrigin
VECTOR vCircumference
VECTOR vHeadHere
VECTOR vClickPos

FLOAT fDeltaPos = 1
FLOAT fPedPos = 3
FLOAT fPedDistance = 10
FLOAT fDist
FLOAT fHead
FLOAT fRadius = 1

INT iWalk
INT timeCheck
INT iLODPed
INT iCircle
INT iShoes
INT randomTime[NUMBER_OF_PEDS]
BOOL bCreatePed
BOOL bDeletePed
BOOL bWalkInLine 
BOOL bSniper
BOOL bLODPed
BOOL bWalkInCircle
BOOL bControlPed
BOOL bTerminate
BOOL bNotFrank
BOOL bSinglePed
BOOL bTimer
BOOL bAnimals




PED_INDEX WALKING_PED[NUMBER_OF_PEDS]
INT iCurrentPedIndex 

WIDGET_GROUP_ID createPedWalkingWidgets
TEXT_WIDGET_ID twid_animalselection

MODEL_NAMES ped_selection
MODEL_NAMES animal_selection

PROC MISSION_CLEANUP()
	REQUEST_MODEL(PLAYER_ONE)
	IF DOES_WIDGET_GROUP_EXIST(createPedWalkingWidgets)
		DELETE_WIDGET_GROUP(createPedWalkingWidgets)
	ENDIF

	INT i = 0
	REPEAT iCurrentPedIndex i
		IF DOES_ENTITY_EXIST(WALKING_PED[i])
			DELETE_PED(WALKING_PED[i])
		ENDIF
	ENDREPEAT
	
	IF bNotFrank = TRUE
		REQUEST_MODEL(PLAYER_ONE)
			IF NOT HAS_MODEL_LOADED(PLAYER_ONE)
				WAIT(2500)
			ENDIF
		SET_PLAYER_MODEL(GET_PLAYER_INDEX(), PLAYER_ONE)
	ENDIF

	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC
PROC CREATE_PED_WIDGETS()
	createPedWalkingWidgets = START_WIDGET_GROUP("Walking Ped")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("BARE")
				ADD_TO_WIDGET_COMBO("FLIPFLOPS ")
				ADD_TO_WIDGET_COMBO("SHOES")
				ADD_TO_WIDGET_COMBO("TRAINERS")
				ADD_TO_WIDGET_COMBO("HIGH HEELS")
				ADD_TO_WIDGET_COMBO("BOOTS")
				ADD_TO_WIDGET_COMBO("GOLF")
				ADD_TO_WIDGET_COMBO("CLOWN")
				ADD_TO_WIDGET_COMBO("FLIPPERS")
				ADD_TO_WIDGET_COMBO("CHOP")
				ADD_TO_WIDGET_COMBO("HEN")
				ADD_TO_WIDGET_COMBO("COYOTE")
				ADD_TO_WIDGET_COMBO("BOAR")
				ADD_TO_WIDGET_COMBO("RAT")
				ADD_TO_WIDGET_COMBO("COW")
				ADD_TO_WIDGET_COMBO("CHIMP")
				ADD_TO_WIDGET_COMBO("DEER")
			STOP_WIDGET_COMBO("Ped Shoe Type:", iShoes)
			ADD_WIDGET_BOOL("Enabled - only one ped at time", bSinglePed)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Enable Model Name Spawning", bAnimals)
			twid_animalselection = ADD_TEXT_WIDGET("Animal Selection")
			SET_CONTENTS_OF_TEXT_WIDGET(twid_animalselection, "A_C_CHOP")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Become the ped", bControlPed)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Create Ped at set loaction", bCreatePed)
			ADD_WIDGET_FLOAT_SLIDER("Ped dist. from player", fPedPos, 0, 2000, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Walk in line", bWalkInLine)
			ADD_WIDGET_FLOAT_SLIDER("Distance to walk", fPedDistance, 1, 200, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Walk in a circle", bWalkInCircle)
			ADD_WIDGET_FLOAT_SLIDER("Radius of circle", fRadius, 1, 20, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Enable ped LOD", bLODPed)
			ADD_WIDGET_INT_SLIDER("LOD level:", iLODPed, 0, 50, 1)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Delete All Peds", bDeletePed)
			ADD_WIDGET_BOOL("Give player a sniper", bSniper)
			ADD_WIDGET_BOOL("Terminate Script", bTerminate)
	STOP_WIDGET_GROUP()
ENDPROC

PROC SHOE_UPDATER()
	SWITCH iShoes
		CASE 0
		ped_selection = A_F_Y_BEACH_01
		BREAK
		CASE 1
		ped_selection = A_M_Y_STWHI_01
		BREAK
		CASE 2
		ped_selection = A_M_Y_BUSINESS_01
		BREAK
		CASE 3
		ped_selection = A_M_Y_POLYNESIAN_01
		BREAK
		CASE 4
		ped_selection = A_F_M_BEVHILLS_01
		BREAK
		CASE 5 
		ped_selection = A_M_M_PROLHOST_01
		BREAK
		CASE 6
		ped_selection = A_F_Y_GOLFER_01
		BREAK
		CASE 7
		ped_selection = S_M_Y_CLOWN_01 
		BREAK
		CASE 8
		ped_selection = PLAYER_ONE
		BREAK
		CASE 9
		ped_selection = A_C_CHOP
		BREAK
		CASE 10
		ped_selection = A_C_HEN
		BREAK
		CASE 11
		ped_selection = A_C_COYOTE
		BREAK
		CASE 12
		ped_selection = A_C_BOAR
		BREAK
		CASE 13
		ped_selection = A_C_RAT
		BREAK
		CASE 14
		ped_selection = A_C_COW
		BREAK
		CASE 15
		ped_selection = A_C_CHIMP
		BREAK
		CASE 16
		ped_selection = A_C_DEER
		BREAK
	ENDSWITCH
ENDPROC
PROC CLICK_UPDATER()
INT i = 0
	IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
	
		vClickPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
		
		IF bSinglePed
			REPEAT iCurrentPedIndex i
				IF DOES_ENTITY_EXIST(WALKING_PED[i])
					DELETE_PED(WALKING_PED[i])
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF bAnimals
		
			//ANIMAL selection stuff
			animal_selection = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_animalselection)))
			REQUEST_MODEL(animal_selection)
			IF NOT HAS_MODEL_LOADED(animal_selection)
				WAIT(2500)
			ENDIF
		
			IF (iCurrentPedIndex < NUMBER_OF_PEDS)
				IF iShoes = 8
					WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, animal_selection, vClickPos)
					randomTime[iCurrentPedIndex] = GET_RANDOM_INT_IN_RANGE(0, 5000)
					//SET_PED_COMPONENT_VARIATION(WALKING_PED[iCurrentPedIndex], PED_COMP_FEET, 2, 0)
					SET_ENTITY_HEADING(WALKING_PED[iCurrentPedIndex], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex ++
				ELSE WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, animal_selection, vClickPos)
					randomTime[iCurrentPedIndex] = GET_RANDOM_INT_IN_RANGE(0, 5000)
					SET_ENTITY_HEADING(WALKING_PED[iCurrentPedIndex], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex ++
				ENDIF
			ENDIF
		
			
			
			
			
		//Ped Selection Stuff
		ELSE REQUEST_MODEL(ped_selection)
			
			IF NOT HAS_MODEL_LOADED(ped_selection)
				WAIT(2500)
			ENDIF
		
			IF (iCurrentPedIndex < NUMBER_OF_PEDS)
				IF iShoes = 8
					WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, ped_selection, vClickPos)
					randomTime[iCurrentPedIndex] = GET_RANDOM_INT_IN_RANGE(0, 5000)
					SET_PED_COMPONENT_VARIATION(WALKING_PED[iCurrentPedIndex], PED_COMP_FEET, 2, 0)
					SET_ENTITY_HEADING(WALKING_PED[iCurrentPedIndex], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex ++
				ELSE WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, ped_selection, vClickPos)
					randomTime[iCurrentPedIndex] = GET_RANDOM_INT_IN_RANGE(0, 5000)
					SET_ENTITY_HEADING(WALKING_PED[iCurrentPedIndex], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex ++
				ENDIF
			ENDIF
		ENDIF
		
		iWalk = 0
		iCircle = 0
	ENDIF
	
	IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
		AND DOES_ENTITY_EXIST(WALKING_PED[iCurrentPedIndex])
			vClickPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			SET_ENTITY_COORDS(WALKING_PED[iCurrentPedIndex], vClickPos, TRUE)
	ENDIF	
ENDPROC

PROC PED_BEHAVIOURS()
INT i = 0
	REPEAT iCurrentPedIndex i
		IF DOES_ENTITY_EXIST(WALKING_PED[i])
		AND NOT IS_ENTITY_DEAD(WALKING_PED[i])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(WALKING_PED[i], TRUE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(WALKING_PED[i], FALSE)
			SET_ENTITY_INVINCIBLE(WALKING_PED[i], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC
PROC WALKING_IN_LINE()
IF DOES_ENTITY_EXIST(WALKING_PED[0])
INT i = 0
	REPEAT iCurrentPedIndex i
			SWITCH iWalk
				CASE 0
					GET_ENTITY_MATRIX(WALKING_PED[i], vFrontPed, vSidePed, vUpPed, vPosPed)
					vLeftSide = vPosPed - fPedDistance * vSidePed
					TASK_TURN_PED_TO_FACE_COORD(WALKING_PED[i], vLeftSide, -1)
					TASK_GO_STRAIGHT_TO_COORD(WALKING_PED[i], vLeftSide, 1.0)
					vPedOrigin = GET_ENTITY_COORDS(WALKING_PED[i], TRUE)
					iWalk = 1
				BREAK
				CASE 1
					IF IS_ENTITY_AT_COORD(WALKING_PED[i], vLeftSide, <<0.5, 0.5, 0.5>>, TRUE)
						TASK_GO_STRAIGHT_TO_COORD(WALKING_PED[i], vPedOrigin, 1.0)
						iWalk = 2
					ENDIF
				BREAK
				CASE 2
					IF IS_ENTITY_AT_COORD(WALKING_PED[i], vPedOrigin, <<0.5, 0.5, 0.5>>, TRUE)
						TASK_GO_STRAIGHT_TO_COORD(WALKING_PED[i], vLeftSide, 1.0)
						iWalk = 1
					ENDIF
				BREAK
			ENDSWITCH
	ENDREPEAT
	ENDIF
ENDPROC
PROC WALKING_IN_CIRCLE()
INT i = 0
	IF DOES_ENTITY_EXIST(WALKING_PED[0])
		REPEAT iCurrentPedIndex i 
			IF  (GET_GAME_TIMER() - timeCheck) > randomTime[i]
				SWITCH iCircle
					CASE 0
						GET_ENTITY_MATRIX(WALKING_PED[i], vFrontPed, vSidePed, vUpPed, vPosPed)
						vLeftSide = vPosPed - fRadius * vSidePed
						iCircle++
					BREAK
					CASE 1
						GET_ENTITY_MATRIX(WALKING_PED[i], vFrontPed, vSidePed, vUpPed, vPosPed)
						vCircumference = vPosPed + vFrontPed * 1.0 // - 
						fDist = GET_DISTANCE_BETWEEN_COORDS(vLeftSide, vCircumference)
						fHead = fRadius - fDist
						vHeadHere = vCircumference + vSidePed * fHead
						TASK_GO_STRAIGHT_TO_COORD(WALKING_PED[i], vHeadHere, 1.0)
					BREAK
				ENDSWITCH
			ENDIF 
		ENDREPEAT
	ENDIF
ENDPROC
SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF

	CREATE_PED_WIDGETS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	iCurrentPedIndex = 0
	WHILE TRUE
		// Main Loop
		WAIT(0)
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		ENDIF
		
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
		vTransformedPedPos = vPos + fDeltaPos * vSide
		vPedPos = vTransformedPedPos + vFront * fPedPos
		
		PED_BEHAVIOURS()
		
		SHOE_UPDATER()
		
		CLICK_UPDATER()
		
		INVALIDATE_IDLE_CAM()
		
		IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
		ENDIF

		IF bCreatePed
		INT i = 0
		REPEAT iCurrentPedIndex i
			IF DOES_ENTITY_EXIST(WALKING_PED[i])
				DELETE_PED(WALKING_PED[i])
			ENDIF
		ENDREPEAT
		
			IF bAnimals 
				animal_selection = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_animalselection)))
				REQUEST_MODEL(animal_selection)
				IF NOT HAS_MODEL_LOADED(animal_selection)
					WAIT(2500)
				ENDIF
				IF (iCurrentPedIndex < NUMBER_OF_PEDS)
					WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, animal_selection, vPedPos)
					SET_ENTITY_HEADING(WALKING_PED[0], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex++
				ENDIF
				iWalk = 0
				iCircle = 0
				bCreatePed = FALSE
			
			ELSE REQUEST_MODEL(ped_selection)
				IF NOT HAS_MODEL_LOADED(ped_selection)
					WAIT(2500)
				ENDIF
				IF (iCurrentPedIndex < NUMBER_OF_PEDS)
					WALKING_PED[iCurrentPedIndex] = CREATE_PED(PEDTYPE_GANG1, ped_selection, vPedPos)
					SET_ENTITY_HEADING(WALKING_PED[0], (GET_ENTITY_HEADING(PLAYER_PED_ID())))
					iCurrentPedIndex++
				ENDIF
				iWalk = 0
				iCircle = 0
				bCreatePed = FALSE
			ENDIF
		ENDIF
		
		IF bWalkInLine = TRUE
			WALKING_IN_LINE()
		ELSE iWalk = 0
		ENDIF
		
		IF bWalkInCircle = TRUE
			IF bTimer = FALSE
				timeCheck = GET_GAME_TIMER()
				bTimer = TRUE
			ENDIF
			WALKING_IN_CIRCLE()
		ELSE iCircle = 0
			bTimer = FALSE
		ENDIF
		
		IF bSniper
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 5000, TRUE, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, 5000, FALSE, FALSE)
			bSniper = FALSE
		ENDIF
		
		IF bLODPed
			SET_ENTITY_LOD_DIST(WALKING_PED[0], iLODPed)
			bLODPed = FALSE
		ENDIF
			
		IF bControlPed
			IF bAnimals
				animal_selection = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twid_animalselection)))
				REQUEST_MODEL(animal_selection)
				
				IF NOT HAS_MODEL_LOADED(animal_selection)
					WAIT(2500)
				ENDIF
				
				SET_PLAYER_MODEL(GET_PLAYER_INDEX(), animal_selection)
				bControlPed = FALSE
				bNotFrank = TRUE
				
			ELSE REQUEST_MODEL(ped_selection)
			
				IF NOT HAS_MODEL_LOADED(ped_selection)
					WAIT(2500)
				ENDIF
				
				SET_PLAYER_MODEL(GET_PLAYER_INDEX(), ped_selection)
				bControlPed = FALSE
				bNotFrank = TRUE
				
			ENDIF
		ENDIF

		IF bDeletePed
		INT i = 0
			REPEAT iCurrentPedIndex i
				IF DOES_ENTITY_EXIST(WALKING_PED[i])
					DELETE_PED(WALKING_PED[i])
				ENDIF
			ENDREPEAT
			iCurrentPedIndex = 0
			bDeletePed = FALSE
		ENDIF
		
		IF bTerminate
			MISSION_CLEANUP()
		ENDIF
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_CLEANUP()
		ENDIF
	ENDWHILE
ENDSCRIPT

#ENDIF	//	 IS_DEBUG_BUILD

