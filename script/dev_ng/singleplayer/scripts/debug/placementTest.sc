

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// placementTest.sc
USING "hud_drawing.sch"
USING "UIUtil.sch"
USING "minigame_uiinputs.sch"

INT iState = 0
INT iStartTime

SCRIPT
	SCRIPT_SCALEFORM_SPLASH siMovie
	siMovie.siMovie = REQUEST_SCALEFORM_SPLASH_UI()
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	WHILE TRUE
		IF HAS_SCALEFORM_MOVIE_LOADED(siMovie.siMovie)
			SWITCH iState
				CASE 0
					PRINTLN("Case 0!")
					SET_SCALEFORM_SPLASH_TEXT_LABEL(siMovie, "OFFR_BLIP_R5", 255, 0, 255, 255)
					SPLASH_TEXT_TRANSITION_IN(siMovie)	
					iStartTime = GET_GAME_TIMER()
					
					iState = 1
				BREAK
				
				CASE 1
					PRINTLN("Case 1!")
					IF (GET_GAME_TIMER() - iStartTime > 3000)
						PRINTLN("TRANSITION OUT!")
						SPLASH_TEXT_TRANSITION_OUT(siMovie)
						iState = 2
					//ELSE
					//	UPDATE_SCALEFORM_SPLASH(siMovie)
					ENDIF
				BREAK
			ENDSWITCH
			
			IF UPDATE_SCALEFORM_SPLASH(siMovie)
				//PRINTLN("TERMINATING!!!")
				//TERMINATE_THIS_THREAD()
			ENDIF
		ENDIF

		WAIT(0)
	ENDWHILE
ENDSCRIPT
