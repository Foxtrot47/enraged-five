USING "rage_builtins.sch"
USING "globals.sch"
USING "wardrobe_private.sch"
USING "socialclub_leaderboard.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF


// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_misc.sch"
USING "commands_debug.sch"
USING "shared_debug.sch"

USING "debug_menu_core.sch"
USING "flow_info_core.sch"
USING "flow_diagram_core.sch"
USING "SP_Mission_Flow_OffSkip.sch"

USING "SP_Family_Control_F8_Screen.sch"
USING "SP_Friends_Control_F10_Screen.sch"
USING "SP_Random_EVENT_Control_F7_Screen.sch"

USING "Mission_Control_Public.sch"
USING "flow_public_core_override.sch"
USING "script_heist.sch"
USING "flow_launcher_main_core.sch"
USING "batch_mission_launcher.sch"
USING "net_debug_F9.sch"

USING "vehicle_chase_debug.sch"

USING "debug_channels_structs.sch"

USING "script_launch_control.sch"

USING "photographyWildlife.sch"


DEBUG_MENU_DATA_STRUCT sDebugMenuData

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	debug.sc
//		AUTHOR			:	Keith
//		DESCRIPTION		:	Debug control system that runs alongside Main.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// Instance of Shared Debug struct
structSharedDebugVars	m_sharedDebugVars

// Instance of a visual flow diagram.
FLOW_DIAGRAM sDebugFlowDiagram
FLOW_LAUNCHER_VARS		m_flowLauncher

// Instance of the batch launcher.
BATCH_MISSION_LAUNCHER_STRUCT	m_batchLauncher

// Ambient Debug Variables
BOOL					m_createRandomPedAtNearestCarNode	= FALSE
BOOL					m_createProstituteAtNearestCarNode	= FALSE

// Metrics Zone system control variables.
CONST_INT				TIME_DISPLAY_METRICS_REPORT			15000
BOOL					m_bShowCurrentMetricsZoneResults = FALSE
BOOL					m_bClearCurrentMetricsZoneResults = FALSE
BOOL					m_bHideCurrentMetricsZoneResults = FALSE

// Q Key Character Swap variables.
FLOAT 					m_fQSkipClearTimeCycleStage = 0

// Vehicle Debug Menu
//TIME_DATATYPE tdRelaunchInMPTimer
INT tdRelaunchInMPTimer
BOOL bRelaunchTimerSet

//population debug variables
bool	bDebugPopulationToolsEnabled	= 	false
float 	veh_DensityMulti 				=  	1
float 	veh_randDensityMulti			= 	1
float 	veh_parkedDensityMulti			= 	1
int		veh_pop_budget					= 	1
float 	ped_scenarioMulti				=	1
float  	ped_densityMulti				= 	1
int 	ped_pop_budget					= 	1
int 	iPopTextTimer
int 	iPropPriorityLevel				= 	3

//debug start positions widget
//WIDGET_ID m_StartPositionDebugWidgets[iTOTAL_DEBUG_START_LOCATIONS]
BOOL m_bClickedStartPos[iTOTAL_DEBUG_START_LOCATIONS]

//Vehicle chase debug data
STRUCT_Vehicle_Chase_Debug m_VehicleChaseDebugData

PED_SCENE_STRUCT sPedScene
PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
SCRIPT_TIMER stF9SpaceTimer
WIDGET_GROUP_ID widgetDebug

//Money debug helpers
INT iF6InitialDelay = 1000
INT iF6Tick = 30
INT iF6NextTick = 0

DEBUG_MOVERS_STRUCT sDebugMoversStruct

//SC leaderboard debug
STRUCT SCLB_LEADERBOARD_DEBUG_TEST
	BOOL bRead
	INT iRaceID
	
	INT iReadProgress
	INT iOverallProgress
	BOOL bReadResult
	
	INT iGlobalBest
	INT iPersonalBest
ENDSTRUCT
SCLB_LEADERBOARD_DEBUG_TEST sclb_Debug

BOOL m_bKillScript
BOOL m_bRestartScript

PROC Intialise_Debug_Start_Positions_Widget()
	INT i = 0
	START_WIDGET_GROUP("Start positions")
			REPEAT iTOTAL_DEBUG_START_LOCATIONS i
		 		ADD_WIDGET_BOOL(g_DebugStartPositionStrings[i],m_bClickedStartPos[i])
			ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC


PROC Update_Start_Pos_Widget_Response()
	INT i = 0
	INT selected = -1
	REPEAT iTOTAL_DEBUG_START_LOCATIONS i
		IF m_bClickedStartPos[i] = TRUE
			selected = i
		ENDIF
	ENDREPEAT
	
	IF selected > -1
		REPEAT iTOTAL_DEBUG_START_LOCATIONS i
			m_bClickedStartPos[i] = FALSE
		ENDREPEAT
		
		//try to load the coordinate area
		
		LOAD_SCENE(g_vDebugStartPositions[selected])
		
		SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), g_vDebugStartPositions[selected])
		
		SET_ENTITY_HEADING(PLAYER_PED_ID(),g_vDebugStartOrientations[selected])
		
		// Set Gameplay Camera before and after a wait - sometimes it works without the WAIT
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		WAIT(0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	ENDIF
ENDPROC


/// PURPOSE:
///    To run the smoketest fullmap_test_flow in game flow mode but with all missions complete
PROC Maintain_end_of_game_skip()
	IF g_end_of_game_flow
		LAUNCHER_LAUNCH_FLOW_TO_LABEL(m_flowLauncher, LABEL_GAME_END)
		g_end_of_game_flow = FALSE
	ENDIF
ENDPROC


/// PURPOSE:
/// Draws a time of day script variable to screen as debug text.
PROC Draw_Time_Of_Day_To_Screen(TIMEOFDAY paramTimeOfDay, FLOAT paramPosX, FLOAT paramPosY, INT paramRed = 255, INT paramGreen = 255, INT paramBlue = 255)
	TEXT_LABEL_7 txtHour = ""
	TEXT_LABEL_7 txtMin = ""
	TEXT_LABEL_7 txtSec = ""
	INT iTempInt
		
	//Get hour as a text label.
	iTempInt = GET_TIMEOFDAY_HOUR(paramTimeOfDay)
	IF iTempInt < 10
		txtHour += 0
	ENDIF
	txtHour += iTempInt
		
	//Get minute as a text label.
	iTempInt = GET_TIMEOFDAY_MINUTE(paramTimeOfDay)
	IF iTempInt < 10
		txtMin += 0
	ENDIF
	txtMin += iTempInt
		
	//Get second as a text label.
	iTempInt = GET_TIMEOFDAY_SECOND(paramTimeOfDay)
	IF iTempInt < 10
		txtSec += 0
	ENDIF
	txtSec += iTempInt
		
	//Setup text params.
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_BOTTOM)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_JUSTIFICATION(FONT_LEFT)
	SET_TEXT_COLOUR(paramRed, paramGreen, paramBlue, 255)
	SET_TEXT_DROP_SHADOW()
	SET_SCRIPT_GFX_ALIGN_PARAMS(paramPosX, paramPosY, FLOW_MISSION_NAME_SIZE_X, FLOW_MISSION_NAME_SIZE_Y)
	SET_TEXT_SCALE(0.55, 0.55)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	
	//Draw the time/date.
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("DBG_TIMEDATE")
		ADD_TEXT_COMPONENT_INTEGER(GET_TIMEOFDAY_DAY(paramTimeOfDay))
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MONTH_STRING_FROM_MONTH_OF_YEAR(GET_TIMEOFDAY_MONTH(paramTimeOfDay)))
		ADD_TEXT_COMPONENT_INTEGER(GET_TIMEOFDAY_YEAR(paramTimeOfDay))
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(txtHour)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(txtMin)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(txtSec)
	END_TEXT_COMMAND_DISPLAY_TEXT(paramPosX, paramPosY)
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC


/// PURPOSE:
/// Draws script's interpretation of the current time/date and an additional custom time/date to the screen.
PROC Maintain_Game_Time_Date_Displaying()
	IF m_sharedDebugVars.bDrawGameTimeDate
		//Draw the current time of day.
		Draw_Time_Of_Day_To_Screen(	GET_CURRENT_TIMEOFDAY(),
									m_sharedDebugVars.fCurrentTimeDateX, 
									m_sharedDebugVars.fCurrentTimeDateY,
									255, 255, 255)
		
		//If a script has set a debug time of day draw it here as well.
		IF g_debugTOD != INVALID_TIMEOFDAY
			Draw_Time_Of_Day_To_Screen(	g_debugTOD,
										m_sharedDebugVars.fDebugTimeDateX, 
										m_sharedDebugVars.fDebugTimeDateY,
										255, 100, 100)
		ENDIF
	ENDIF
	
	//Set current TOD
	IF m_sharedDebugVars.bSetTOD
		m_sharedDebugVars.bSetTOD = FALSE
			
		SET_CLOCK_TIME(m_sharedDebugVars.fDebugDate1.Hour, m_sharedDebugVars.fDebugDate1.minute, m_sharedDebugVars.fDebugDate1.Seconds)
		SET_CLOCK_DATE(m_sharedDebugVars.fDebugDate1.Day,INT_TO_ENUM(MONTH_OF_YEAR, m_sharedDebugVars.fDebugDate1.Month), m_sharedDebugVars.fDebugDate1.Year)
		
	ENDIF
		
	
	//Perform TOD calculation
	IF m_sharedDebugVars.bAddTODs
		m_sharedDebugVars.bAddTODs = FALSE
		
		TIMEOFDAY v1 = CONVERT_STATDATE_TO_TIMEOFDAY(m_sharedDebugVars.fDebugDate1)
		
		ADD_TIME_TO_TIMEOFDAY(v1,m_sharedDebugVars.fDebugDate2.Seconds,m_sharedDebugVars.fDebugDate2.Minute,m_sharedDebugVars.fDebugDate2.Hour,
			m_sharedDebugVars.fDebugDate2.Day,m_sharedDebugVars.fDebugDate2.Month, m_sharedDebugVars.fDebugDate2.Year)
		m_sharedDebugVars.fDebugDateRes = CONVERT_TIMEOFDAY_TO_STATDATE(v1)
	ENDIF
ENDPROC


// *****************************************************************************************
//	Savehouse warp 
// *****************************************************************************************

/// PURPOSE:
///    KEY M: Toggles Mission menu on/off
PROC Maintain_KeyH_Warp_To_Savehouse()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_H, KEYBOARD_MODIFIER_SHIFT, "Warp To Savehouse")
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SWITCH GET_CURRENT_PLAYER_PED_ENUM()
				CASE CHAR_MICHAEL
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-821.5895, 176.8634, 70.5457>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 289.0280)
				BREAK
				CASE CHAR_FRANKLIN
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-13.9896, -1447.2451, 29.6045>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 108.5263)
				BREAK
				CASE CHAR_TREVOR
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1983.2773, 3817.2107, 31.3619>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 163.2128)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC


// *****************************************************************************************
//	Player cash
// *****************************************************************************************

/// PURPOSE:
///    KEY CTRL+F6: Gives the player a lump sum of cash
PROC Maintain_KeyF6_Cash()

	// Note, pressing F6 key on its own will give the player 100 bucks from code.


	
	//Increase / Decrease Cash
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F6, KEYBOARD_MODIFIER_SHIFT, "Decrease Cash")
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			DEBIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 1000)
			iF6NextTick = GET_GAME_TIMER() + iF6InitialDelay		
		ENDIF
	ENDIF
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F6)
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 5000)
			iF6NextTick = GET_GAME_TIMER() + iF6InitialDelay
		ENDIF
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F6, KEYBOARD_MODIFIER_CTRL, "Lump Cash Increase")
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 100000)
			iF6NextTick = GET_GAME_TIMER() + iF6InitialDelay
		ENDIF
	ENDIF
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F6, KEYBOARD_MODIFIER_CTRL_SHIFT, "MASSIVE Lump Cash Increase")
		enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePed)
			CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 100000000)
			iF6NextTick = GET_GAME_TIMER() + iF6InitialDelay
		ENDIF
	ENDIF
	
	//Quick increase/decrease cash
	IF IS_DEBUG_KEY_PRESSED(KEY_F6, KEYBOARD_MODIFIER_CTRL,"Quick lump cash increase")
		IF GET_GAME_TIMER() > iF6NextTick
			enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
			IF IS_PLAYER_PED_PLAYABLE(ePed)
				CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 100000)
				iF6NextTick = GET_GAME_TIMER() + iF6Tick
			ENDIF
		ENDIF
	ENDIF
	IF IS_DEBUG_KEY_PRESSED(KEY_F6, KEYBOARD_MODIFIER_NONE,"Quick cash increase")
		IF GET_GAME_TIMER() > iF6NextTick
			enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
			IF IS_PLAYER_PED_PLAYABLE(ePed)
				CREDIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 5000)
				iF6NextTick = GET_GAME_TIMER() + iF6Tick
			ENDIF
		ENDIF
	ENDIF
	IF IS_DEBUG_KEY_PRESSED(KEY_F6, KEYBOARD_MODIFIER_SHIFT,"Quick cash decrease")
		IF GET_GAME_TIMER() > iF6NextTick
			enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
			IF IS_PLAYER_PED_PLAYABLE(ePed)
				DEBIT_BANK_ACCOUNT(ePed, BAAC_DEFAULT_DEBUG, 1000)
				iF6NextTick = GET_GAME_TIMER() + iF6Tick
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


// *****************************************************************************************
//	Player swap 
// *****************************************************************************************

/// PURPOSE:
///    KEY Q: Swaps the player character and tests timecycle modifiers.
PROC Maintain_KeyQ_Swap_Player()
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Q)
	
		//C//PRINTLN(DEBUG_PED_COMP, "doing Q skip")
		
		INT iCurrentPlayerPed = GET_CURRENT_PLAYER_PED_INT()
		INT iNewPlayerPed = iCurrentPlayerPed
		
		WHILE TRUE
			//Step to the next playable ped index.
			iNewPlayerPed++
			IF iNewPlayerPed > 2
				iNewPlayerPed = 0
			ENDIF
			
			//Have we looped back to the current player ped and failed to find a new available ped to switch to?
			IF iNewPlayerPed = iCurrentPlayerPed
				EXIT
			ENDIF
			
			m_fQSkipClearTimeCycleStage++
			IF m_fQSkipClearTimeCycleStage > 3
				IF NOT g_bUseCharacterFilters
					g_bUseCharacterFilters = TRUE
					//PRINTLN("<QSKIP> Turning on character timecycle modifier.")
					UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
				ELSE
					//PRINTLN("<QSKIP> Clearing character timecycle modifier.")
					SET_NEXT_PLAYER_TCMODIFIER("")
				ENDIF
				m_fQSkipClearTimeCycleStage = 0
				EXIT
			ENDIF
			
			//Is this new playable ped index available to be swapped to?
			IF IS_PLAYER_PED_AVAILABLE(INT_TO_ENUM(enumCharacterList, iNewPlayerPed))
				//PRINTLN("<QSKIP> Performing swapping of player character.")
				
				STORE_DEFAULT_PLAYER_SWITCH_STATE(PLAYER_PED_ID())
				
				WHILE NOT SET_CURRENT_SELECTOR_PED(GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(INT_TO_ENUM(enumCharacterList, iNewPlayerPed)))
					WAIT(0)
				ENDWHILE
				
				// set a random combination of clothes on the ped
				SET_RANDOM_CLOTHES_COMBO(PLAYER_PED_ID(), TRUE)
				
				IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_W)
					//PRINTLN("<QSKIP> KEY_W NOT pressed - reset last known ped info.")
					ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_MISSION_NONE)
				ELSE
					//PRINTLN("<QSKIP> KEY_W pressed.")
				ENDIF
				
				EXIT
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC


/// PURPOSE:
///    KEY K: Kills the player character.
PROC Maintain_KeyK_Kill_PLayer()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_SHIFT, "Kill local player")
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
		ENDIF
	ENDIF
ENDPROC


// *****************************************************************************************
//	Mission Menu - toggle on/off
// *****************************************************************************************

SCRIPT_TIMER buttonHoldTimer

/// PURPOSE:
///    KEY M: Toggles Mission menu on/off
PROC Maintain_KeyM_Toggle_Mission_Menu()

	IF GET_COMMANDLINE_PARAM_EXISTS("mp_DisableDebugMenu") 
		EXIT
	ENDIF
	// Activate?
	BOOL bJoypadKeysPressed = FALSE
	IF (IS_BUTTON_PRESSED(PAD1, SELECT)
	AND IS_BUTTON_PRESSED(PAD1, RIGHTSHOCK)
	AND IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER1))
		IF NOT IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_KEYS_PRESSED)
			SET_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_KEYS_PRESSED)
		ENDIF
	ELSE
		IF IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_KEYS_PRESSED)
			CLEAR_DEBUG_MENU_PROCESS_FLAG(sDebugMenuData.iProcessFlags, DM_PROCESS_KEYS_PRESSED)
			bJoypadKeysPressed = TRUE
		ENDIF
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("mp_EnableDebugMenuButtonPress") 
		IF IS_BUTTON_PRESSED(PAD1, DPADLEFT)
			IF HAS_NET_TIMER_EXPIRED(buttonHoldTimer, 1500)
				bJoypadKeysPressed = TRUE
				RESET_NET_TIMER(buttonHoldTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(buttonHoldTimer)
		ENDIF
	ENDIF
		
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_M)
	OR (bJoypadKeysPressed)
	OR (IS_DEBUG_MENU_PROCESS_FLAG_SET(sDebugMenuData.iProcessFlags, DM_PROCESS_LAUNCH) AND IS_BUTTON_JUST_PRESSED(PAD1, CIRCLE) AND NOT sDebugMenuData.bDrawAltWindow)
		
		DM_Toggle_Menu_Launch_State(sDebugMenuData)
	ENDIF
	
ENDPROC


// *****************************************************************************************
//	Random Event Debug Screen - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY F11: Toggles Random Event debug screen on/off
PROC Maintain_KeyF11_Toggle_Random_Event_Debug_Screen()

	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F11))
		WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_F11))
			WAIT (0)
		ENDWHILE
		
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F11_SCREEN_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_F11_SCREEN_ON)
			WAIT(0)
			
			//PRINTSTRING("...F11 SCREEN OFF")
			//PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_F11_SCREEN_ON)

			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F8_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F8_SCREEN_ON)
				//PRINTSTRING("...F8 SCREEN OFF")//PRINTNL()
			ENDIF
			
			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
				//PRINTSTRING("...F9 SCREEN OFF")//PRINTNL()
			ENDIF

			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F10_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F10_SCREEN_ON)
				//PRINTSTRING("...F10 SCREEN OFF")//PRINTNL()
			ENDIF
			
			//PRINTSTRING("...F11 SCREEN ON")
			//PRINTNL()
		ENDIF
	ENDIF

	IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F11_SCREEN_ON))
		Display_Random_Event_F11_Screen()
	ELSE
		IF (IS_DEBUG_KEY_PRESSED(KEY_F11, KEYBOARD_MODIFIER_CTRL, "toggle F11 screen off"))
			g_bDrawLiteralSceneString = FALSE
		ENDIF
	ENDIF

ENDPROC



// *****************************************************************************************
//	Family Controller Debug Screen - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY F8: Toggles Family Controller debug screen on/off
PROC Maintain_KeyF8_Toggle_Family_Control_Debug_Screen()

	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F8))
		WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_F8))
			WAIT (0)
		ENDWHILE
		
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F8_SCREEN_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_F8_SCREEN_ON)
			WAIT(0)
			
			Cleanup_F8_Switch_Details()
			
			//PRINTSTRING("...F8 SCREEN OFF")
			//PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_F8_SCREEN_ON)
				
			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F11_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F11_SCREEN_ON)
				//PRINTSTRING("...F9 SCREEN OFF")//PRINTNL()
			ENDIF
			
			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
				//PRINTSTRING("...F9 SCREEN OFF")//PRINTNL()
			ENDIF

			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F10_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F10_SCREEN_ON)
				//PRINTSTRING("...F10 SCREEN OFF")//PRINTNL()
			ENDIF
			
			//PRINTSTRING("...F8 SCREEN ON")
			//PRINTNL()
		ENDIF
	ENDIF

	IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F8_SCREEN_ON))
		Display_Family_Controller_F8_Screen( sPedScene, sPassedScene)
	ELSE
		IF (IS_DEBUG_KEY_PRESSED(KEY_F8, KEYBOARD_MODIFIER_CTRL, "toggle F8 screen off"))
			g_bDrawLiteralSceneString = FALSE
		ENDIF
	ENDIF

ENDPROC


// *****************************************************************************************
//	Friends Controller Debug Screen - toggle on/off
// *****************************************************************************************

/// PURPOSE:
///    KEY F10: Toggles Friends Controller debug screen on/off
PROC Maintain_KeyF10_Toggle_Friends_Control_Debug_Screen()

	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F10))
		WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_F10))
			WAIT (0)
		ENDWHILE
		
		IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F10_SCREEN_ON))
			CLEAR_BIT(m_sharedDebugFlags, DBG_F10_SCREEN_ON)
			WAIT(0)
			
			Cleanup_F10_Switch_Details()
			
			//PRINTSTRING("...F10 SCREEN OFF")
			//PRINTNL()
		ELSE
			SET_BIT(m_sharedDebugFlags, DBG_F10_SCREEN_ON)
			
			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F8_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F8_SCREEN_ON)
				//PRINTSTRING("...F8 SCREEN OFF")//PRINTNL()
			ENDIF

			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
				//PRINTSTRING("...F9 SCREEN OFF")//PRINTNL()
			ENDIF

			IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F11_SCREEN_ON))
				CLEAR_BIT(m_sharedDebugFlags, DBG_F11_SCREEN_ON)
				//PRINTSTRING("...F11 SCREEN OFF")//PRINTNL()
			ENDIF

			//PRINTSTRING("...F10 SCREEN ON")
			//PRINTNL()
		ENDIF
	ENDIF

	IF (IS_BIT_SET(m_sharedDebugFlags, DBG_F10_SCREEN_ON))
		Display_Friends_Controller_F10_Screen()
	ENDIF

ENDPROC


proc MaintainPopulationDebug()
	//if population command line is used.
		if bDebugPopulationToolsEnabled 
			// vehicle population debug						
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD,KEYBOARD_MODIFIER_NONE,"veh_DensityMulti increase")			
				veh_DensityMulti += 0.1
				//PRINTLN("veh_DensityMulti increased to: ", veh_DensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA,KEYBOARD_MODIFIER_NONE,"veh_DensityMulti decrease")
				veh_DensityMulti -= 0.1
				if veh_DensityMulti <= 0
					veh_DensityMulti = 0
				endif
				//PRINTLN("veh_DensityMulti decreased to: ", veh_DensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD,KEYBOARD_MODIFIER_SHIFT,"veh_randDensityMulti increase")
				veh_randDensityMulti += 0.1
				//PRINTLN("veh_randDensityMulti increased to: ",veh_randDensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA,KEYBOARD_MODIFIER_SHIFT,"veh_randDensityMulti decrease")			
				veh_randDensityMulti -= 0.1
				if veh_randDensityMulti <= 0
					veh_randDensityMulti = 0
				endif
				//PRINTLN("veh_randDensityMulti decreased to: ",veh_randDensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD,KEYBOARD_MODIFIER_CTRL,"veh_parkedDensityMulti increase")
				veh_parkedDensityMulti += 0.1
				//PRINTLN("veh_parkedDensityMulti increased to: ",veh_parkedDensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA,KEYBOARD_MODIFIER_CTRL,"veh_ParkedDensityMulti decrease")
				veh_parkedDensityMulti -= 0.1
				if veh_parkedDensityMulti <= 0
					veh_parkedDensityMulti = 0
				endif
				//PRINTLN("veh_ParkedDensityMulti decreased to: ",veh_parkedDensityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD,KEYBOARD_MODIFIER_ALT,"VEHICLE_POPULATION_BUDGET increase")
				veh_pop_budget = CLAMP_INT(veh_pop_budget+1,0,3)			
				SET_VEHICLE_POPULATION_BUDGET(veh_pop_budget)
				//PRINTLN("VEHICLE_POPULATION_BUDGET: ",veh_pop_budget)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_COMMA,KEYBOARD_MODIFIER_ALT,"VEHICLE_POPULATION_BUDGET decrease")
				veh_pop_budget = CLAMP_INT(veh_pop_budget -1,0,3)
				SET_VEHICLE_POPULATION_BUDGET(veh_pop_budget)
				//PRINTLN("VEHICLE_POPULATION_BUDGET: ",veh_pop_budget)
				iPopTextTimer = GET_GAME_TIMER()
			endif									
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(veh_DensityMulti)
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(veh_randDensityMulti)
			SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(veh_parkedDensityMulti)
						
			//ped population debug		
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT,KEYBOARD_MODIFIER_CTRL,"ped_scenarioMulti increase")
				ped_scenarioMulti += 0.1
				//PRINTLN("ped_scenarioMulti increased to: ",ped_scenarioMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT,KEYBOARD_MODIFIER_CTRL,"ped_scenarioMulti decrease")
				ped_scenarioMulti -= 0.1
				if ped_scenarioMulti <= 0
					ped_scenarioMulti = 0
				endif
				//PRINTLN("ped_scenarioMulti decreased to: ",ped_scenarioMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_UP,KEYBOARD_MODIFIER_CTRL,"ped_densityMulti increase")
				ped_densityMulti += 0.1
				//PRINTLN("ped_densityMulti increased to: ",ped_densityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN,KEYBOARD_MODIFIER_CTRL,"ped_densityMulti decrease")
				ped_densityMulti -= 0.1
				if ped_densityMulti <= 0
					ped_densityMulti = 0
				endif
				//PRINTLN("ped_densityMulti decreased to: ",ped_densityMulti)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_UP,KEYBOARD_MODIFIER_SHIFT,"ped_pop increase")
				ped_pop_budget = CLAMP_INT(ped_pop_budget + 1,0,3)					
				SET_PED_POPULATION_BUDGET(ped_pop_budget)
				//PRINTLN("PED_POPULATION_BUDGET: ",ped_pop_budget)
				
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN,KEYBOARD_MODIFIER_SHIFT,"ped_pop decrease")
				ped_pop_budget = CLAMP_INT(ped_pop_budget - 1,0,3)						
				SET_PED_POPULATION_BUDGET(ped_pop_budget)
				//PRINTLN("PED_POPULATION_BUDGET: ",ped_pop_budget)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			
			SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(ped_scenarioMulti,ped_scenarioMulti)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(ped_densityMulti)			
			
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT,KEYBOARD_MODIFIER_SHIFT,"prop priority increase")
				iPropPriorityLevel = CLAMP_INT(iPropPriorityLevel - 1,0,3)
				SET_GLOBAL_INSTANCE_PRIORITY(iPropPriorityLevel)
				//PRINTLN("Prop priority level: ",iPropPriorityLevel)
				iPopTextTimer = GET_GAME_TIMER()
			endif
			if IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT,KEYBOARD_MODIFIER_SHIFT,"prop priority decrease")
				iPropPriorityLevel = CLAMP_INT(iPropPriorityLevel + 1,0,3)
				SET_GLOBAL_INSTANCE_PRIORITY(iPropPriorityLevel)
				//PRINTLN("Prop priority level: ",iPropPriorityLevel)
				iPopTextTimer = GET_GAME_TIMER()
			endif		
					
						
			if get_game_timer() - iPopTextTimer < 4000	
				
				float Xoffset	= 0.6	
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_FLOAT(Xoffset,0.1,"DPOP_VEHDENSMULT",veh_DensityMulti,2)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_FLOAT(Xoffset,0.15,"DPOP_VEHRANDMULT",veh_randDensityMulti,2)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_FLOAT(Xoffset,0.2,"DPOP_VEHPARKMULT",veh_parkedDensityMulti,2)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_NUMBER(Xoffset,0.25,"DPOP_VEHPOPBUDG",veh_pop_budget)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_FLOAT(Xoffset,0.3,"DPOP_PEDSCENMULT",ped_scenarioMulti,2)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_FLOAT(Xoffset,0.35,"DPOP_PEDDENSMULT",ped_densityMulti,2)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_NUMBER(Xoffset,0.4,"DPOP_PEDPOPBUDG",ped_pop_budget)
				Configure_On_Screen_Text()
				DISPLAY_TEXT_WITH_NUMBER(Xoffset,0.45,"DPOP_PROPLEVEL",iPropPriorityLevel)
			endif
		endif
endproc

// *****************************************************************************************
//	AMBIENT DEBUG
// *****************************************************************************************

/// PURPOSE: Creates the widget that enable a couple of Ambient debug functions
PROC Create_Ambient_Widgets()

	START_WIDGET_GROUP("Ambient Script Debug")
		ADD_WIDGET_BOOL("Tick to allow debug script launches to count towards 100%", g_bDontResetCompletionOnDebugLaunch)
		ADD_WIDGET_BOOL("Tick to hold Random Events", g_bHoldRandomEventForSelection)
		ADD_WIDGET_BOOL("Create random ped at nearest car node", m_createRandomPedAtNearestCarNode)
		ADD_WIDGET_BOOL("Create hooker at nearest car node", m_createProstituteAtNearestCarNode)
	STOP_WIDGET_GROUP()

ENDPROC

/// PURPOSE: Maintains the widget that allows a couple of Ambient debug functions
PROC Maintain_Ambient_Widgets()
	PED_INDEX randomPed
	PED_INDEX prostPed
	VECTOR creationPos
	IF m_createRandomPedAtNearestCarNode
		IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), creationPos)
					randomPed = CREATE_RANDOM_PED(creationPos)
					SET_PED_AS_NO_LONGER_NEEDED(randomPed)
					m_createRandomPedAtNearestCarNode = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF m_createProstituteAtNearestCarNode
		REQUEST_MODEL(S_F_Y_HOOKER_01)
		
		IF  HAS_MODEL_LOADED(S_F_Y_HOOKER_01)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), creationPos)
						//randomPed = CREATE_RANDOM_PED(creationPos)
					prostPed = CREATE_PED(PEDTYPE_PROSTITUTE,S_F_Y_HOOKER_01, creationPos, 0.0)
					WAIT(1000)
					SET_PED_AS_NO_LONGER_NEEDED(prostPed)
					
					m_createProstituteAtNearestCarNode = FALSE
				ENDIF
			ENDIF

			
		ENDIF
	ENDIF

ENDPROC

// *****************************************************************************************
//	Currently Playing Dialogue Information
//  Note: Had to be placed in the main script file for debug due to cylic dependencies with Dialogue_public.sch
// *****************************************************************************************

#IF IS_DEBUG_BUILD
PROC DEBUG_SHOW_DIALOGUE_ROOT_PLAYING()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SUBTRACT, KEYBOARD_MODIFIER_NONE, "ConversationDebug")
	OR g_bEnableConversationDebugFromWidget 
		g_bEnableConversationDebugFromWidget = FALSE
		
		IF g_bEnableConversationDebug
			g_bEnableConversationDebug = FALSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		ELSE
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bEnableConversationDebug = TRUE
		ENDIF
	ENDIF
	
	IF NOT g_bEnableConversationDebug
		EXIT
	ENDIF
	
	BOOL bLoadingBlock
	INT iPrint
	TEXT_LABEL_63 tl63 = ""
	TEXT_LABEL_23 tl23 = ""
	FLOAT fX, fY, fSpacing
	VECTOR vPos = <<0.0, 0.0, 0.0>>

	// Could use widget.
	fX = 0.55
	fY = 0.25
	fSpacing = 0.0275
		
	PROCESS_DEBUG_MOVER_FOR_TYPE(sDebugMoversStruct, ciDEBUG_ON_SCREEN_MOVERS_CONVERSATION, fX, fY, 0.25, 0.025, TRUE, FALSE, TRUE)
	
	vPos = <<fX, fY+(0.03*iPrint), fY+(0.015*iPrint)>>
	
	tl63 = "------- Conversation Debug -------"
	DRAW_DEBUG_TEXT_2D(tl63, vPos,  255, 255, 255, 255)
	iPrint++
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_DIVIDE, KEYBOARD_MODIFIER_NONE, "ConversationDebugKeyboard")
		g_bEnableConversationDebugCustomPlaying = TRUE
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_MULTIPLY, KEYBOARD_MODIFIER_NONE, "ConversationDebugKeyboard")
		g_bEnableConversationDebugCustomPlayingLine = TRUE
	ENDIF
	
	IF g_bEnableConversationDebugCustomPlaying
	OR g_bEnableConversationDebugCustomPlayingLine
	OR g_bEnableConversationDebugCustomPlayingAmbientLinePos
	
		PRINTLN("[ConversationDebug] - Processing Debug Conversation.")
		
		REQUEST_ADDITIONAL_TEXT_FOR_DLC(g_tl31_Block_DiagDebug, DLC_MISSION_DIALOGUE_TEXT_SLOT)	
		
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED(g_tl31_Block_DiagDebug, DLC_MISSION_DIALOGUE_TEXT_SLOT)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
			PRINTLN("[ConversationDebug] - g_tl31_Block_DiagDebug: ", g_tl31_Block_DiagDebug, " g_tl31_Root_DiagDebug: ", g_tl31_Root_DiagDebug, " g_tl31_Label_DiagDebug: ", g_tl31_Label_DiagDebug)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_ConversationDebugVoiceNamePed1)
			AND g_ConversationDebugVoiceNumberPed1 != -1
				PRINTLN("[ConversationDebug] - Adding ped 1, Name: ", g_ConversationDebugVoiceNamePed1, " Speaker: ", g_ConversationDebugVoiceNumberPed1)
				ADD_PED_FOR_DIALOGUE(g_ConversationDebugCustom_speechPedStruct, g_ConversationDebugVoiceNumberPed1, NULL, g_ConversationDebugVoiceNamePed1)
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(g_ConversationDebugVoiceNamePed2)
			AND g_ConversationDebugVoiceNumberPed2 != -1
				PRINTLN("[ConversationDebug] - Adding ped 2, Name: ", g_ConversationDebugVoiceNamePed2, " Speaker: ", g_ConversationDebugVoiceNumberPed2)
				ADD_PED_FOR_DIALOGUE(g_ConversationDebugCustom_speechPedStruct, g_ConversationDebugVoiceNumberPed2, NULL, g_ConversationDebugVoiceNamePed2)
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(g_ConversationDebugVoiceNamePed3)
			AND g_ConversationDebugVoiceNumberPed3 != -1
				PRINTLN("[ConversationDebug] - Adding ped 3, Name: ", g_ConversationDebugVoiceNumberPed3, " Speaker: ", g_ConversationDebugVoiceNumberPed3)
				ADD_PED_FOR_DIALOGUE(g_ConversationDebugCustom_speechPedStruct, g_ConversationDebugVoiceNumberPed3, NULL, g_ConversationDebugVoiceNamePed3)
			ENDIF
			
			IF g_bEnableConversationDebugCustomPlaying
				PRINTLN("[ConversationDebug] - Playing Conversation")
				IF CREATE_CONVERSATION(g_ConversationDebugCustom_speechPedStruct, g_tl31_Block_DiagDebug, g_tl31_Root_DiagDebug, CONV_PRIORITY_VERY_HIGH)
					g_bEnableConversationDebugCustomPlaying = FALSE
				ENDIF
			ELIF g_bEnableConversationDebugCustomPlayingLine
				PRINTLN("[ConversationDebug] - Playing Single Line using label.")
				PLAY_SINGLE_LINE_FROM_CONVERSATION(g_ConversationDebugCustom_speechPedStruct, g_tl31_Block_DiagDebug, g_tl31_Root_DiagDebug, g_tl31_Label_DiagDebug, CONV_PRIORITY_VERY_HIGH)
				g_bEnableConversationDebugCustomPlayingLine = FALSE
			ELIF g_bEnableConversationDebugCustomPlayingAmbientLinePos
				
				VECTOR vPosSpeech
				vPosSpeech = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				
				PRINTLN("[ConversationDebug] - Playing Single Context using Label as Context at position: ", vPosSpeech)
				
				IF NOT IS_VECTOR_ZERO(vPosSpeech)
					PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(g_tl31_Label_DiagDebug, g_ConversationDebugVoiceNamePed1, vPosSpeech, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_NORMAL_CRITICAL))
				ENDIF
				
				/*
				TEXT_LABEL_31 tl31 = ""
				tl31 = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_tl31_Root_DiagDebug)
				tl31 += "A"
				
				PRINTLN("[ConversationDebug] - Playing Single Context using Root at position.     g_tl31_Root_DiagDebug: ", g_tl31_Root_DiagDebug, "     tl31: ", tl31, "     Voice: ", g_ConversationDebugVoiceNamePed1)
								
				VECTOR vPosSpeech
				vPosSpeech = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				PLAY_AMBIENT_SPEECH_FROM_POSITION_NATIVE(tl31, g_ConversationDebugVoiceNamePed1, vPosSpeech, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_NORMAL_CRITICAL))*/
				
				g_bEnableConversationDebugCustomPlayingAmbientLinePos = FALSE
			ENDIF
				
		ELSE
			PRINTLN("[ConversationDebug] - Loading block: ", g_tl31_Block_DiagDebug)
			bLoadingBlock = TRUE
		ENDIF
	ENDIF
		
	IF g_bEnableConversationDebugCustomKillConvo	
		g_bEnableConversationDebugCustomKillConvo = FALSE
		g_bEnableConversationDebugCustomPlaying = FALSE
		g_bEnableConversationDebugCustomPlayingLine = FALSE
		g_bEnableConversationDebugCustomPlayingAmbientLinePos = FALSE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
	
	IF IS_SCRIPTED_CONVERSATION_ONGOING()
	OR NOT IS_CONVERSATION_STATUS_FREE()
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Conversation is Loading/Playing"
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Block: "
		tl63 += g_BlockToLoadHolder
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Root: "
		tl23 = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		tl63 += tl23
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Label/Context: "
		tl23 = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		tl63 += tl23
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Line: "
		tl63 += GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Speaker Int: "
		tl23 = GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE()
		tl63 += tl23
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		IF g_iDialogueProgressLastPlayed > -1
			vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
			tl63 = "Dialogue Trigger: "
			tl63 += g_iDialogueProgressLastPlayed
			DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
			iPrint++
		ENDIF
	ELSE
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Conversation is not Playing"
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Custom Block: "
		tl63 += g_tl31_Block_DiagDebug
		IF bLoadingBlock
			tl63 += "  IS LOADING ! "
		ENDIF
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Custom Root: "
		tl63 += g_tl31_Root_DiagDebug
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Custom Label: "
		tl63 += g_tl31_Label_DiagDebug
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++		
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Name 1: "
		tl63 += g_ConversationDebugVoiceNamePed1
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Number 1: "
		tl63 += g_ConversationDebugVoiceNumberPed1
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Name 2: "
		tl63 += g_ConversationDebugVoiceNamePed2
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Number 2: "
		tl63 += g_ConversationDebugVoiceNumberPed2
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
		
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Name 3: "
		tl63 += g_ConversationDebugVoiceNamePed3
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
		vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
		tl63 = "Ped Voice Number 3: "
		tl63 += g_ConversationDebugVoiceNumberPed3
		DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
		iPrint++	
	ENDIF
		
	vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
	tl63 = "---------------------------------"
	DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
	iPrint++
	
	vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
	tl63 = "To close press [Numpad Minus]"
	DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
	iPrint++	
	
	vPos = <<fX, fY+(fSpacing*iPrint), fY+(fSpacing*iPrint)>>
	tl63 = "RAG Path: Script/Debug/Conversation and Dialogue Testing"
	DRAW_DEBUG_TEXT_2D(tl63, vPos, 255, 255, 255, 255)
	iPrint++	
	
ENDPROC

PROC PROCESS_MUTING_MUSIC()
	IF g_bEnableMusicMute
		IF NOT IS_AUDIO_SCENE_ACTIVE("MUTES_MUSIC_SCENE")
			START_AUDIO_SCENE("MUTES_MUSIC_SCENE")
		ELIF IS_AUDIO_SCENE_ACTIVE("MUTES_MUSIC_SCENE")
			STOP_AUDIO_SCENE("MUTES_MUSIC_SCENE")
		ENDIF
		g_bEnableMusicMute = FALSE
	ENDIF		
ENDPROC

// 0 = Start
// 1 = End
FUNC VECTOR GET_LAST_MOUSE_TO_WORLD_COORD_SHAPETEST_RESULT(INT iResult)
	
	IF iResult < 0
	OR iResult > 1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF

	RETURN vMouseToWorldCoordResult[iResult]
ENDFUNC

PROC PROCESS_MOUSE_TO_WORLD_COORD_SHAPETEST()
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_7, KEYBOARD_MODIFIER_CTRL, "Display mouse to world coord.")
		IF NOT g_bEnableMouseToWorldDebug
			g_bEnableMouseToWorldDebug = TRUE
		ELSE
			g_bEnableMouseToWorldDebug = FALSE
		ENDIF
	ENDIF
	
	IF NOT g_bEnableMouseToWorldDebug
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
	ENTITY_INDEX eiEntityTemp
	INT iHitSomethingTemp
	VECTOR vTempA, vTempB
	TEXT_LABEL_63 tl63
	
	IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
	AND IS_KEYBOARD_KEY_PRESSED(KEY_7)
		IF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_RESULTS_READY
			stiMouseToWorldCoordTest = NULL
			vMouseToWorldCoordResult[1] = GET_ENTITY_COORDS(eiEntityTemp)
		ENDIF
		
		IF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_NONEXISTENT
			stiMouseToWorldCoordTest = START_SHAPE_TEST_MOUSE_CURSOR_LOS_PROBE(vTempA, vTempB, SCRIPT_INCLUDE_ALL, NULL, 0)
		ENDIF
		VECTOR vPosTemp
		GET_MOUSE_POSITION(vPosTemp.x, vPosTemp.y)
		DRAW_DEBUG_TEXT_2D("Grabbing Entity.", vPosTemp)
		EXIT
		
	ELIF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
		IF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_RESULTS_READY
			stiMouseToWorldCoordTest = NULL
			vMouseToWorldCoordResult[1] = vTempA
		ENDIF
		
		IF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_NONEXISTENT
			stiMouseToWorldCoordTest = START_SHAPE_TEST_MOUSE_CURSOR_LOS_PROBE(vTempA, vTempB, SCRIPT_INCLUDE_ALL, NULL, SCRIPT_SHAPETEST_OPTION_DEFAULT)
		ENDIF
		VECTOR vPosTemp
		GET_MOUSE_POSITION(vPosTemp.x, vPosTemp.y)
		DRAW_DEBUG_TEXT_2D("Grabbing Location.", vPosTemp)
		EXIT
	ELIF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
		IF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_RESULTS_READY
			stiMouseToWorldCoordTest = NULL
			IF DOES_ENTITY_EXIST(eiEntityTemp)
				eiMouseToWorldCoordCompareTarget = eiEntityTemp				
			ENDIF
		ELIF GET_SHAPE_TEST_RESULT(stiMouseToWorldCoordTest, iHitSomethingTemp, vTempA, vTempB, eiEntityTemp) = SHAPETEST_STATUS_NONEXISTENT
			stiMouseToWorldCoordTest = START_SHAPE_TEST_MOUSE_CURSOR_LOS_PROBE(vTempA, vTempB, SCRIPT_INCLUDE_ALL, NULL, 0)			
		ENDIF
		VECTOR vPosTemp
		GET_MOUSE_POSITION(vPosTemp.x, vPosTemp.y)
		DRAW_DEBUG_TEXT_2D("Grabbing Entity.", vPosTemp)
		EXIT	
	ENDIF
	
	stiMouseToWorldCoordTest = NULL
	
	VECTOR vPos	
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(eiMouseToWorldCoordCompareTarget), GET_LAST_MOUSE_TO_WORLD_COORD_SHAPETEST_RESULT(1))
	VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(eiMouseToWorldCoordCompareTarget, vMouseToWorldCoordResult[1])
	
	// --- Mouse
	GET_SCREEN_COORD_FROM_WORLD_COORD(vMouseToWorldCoordResult[1], vPos.x, vPos.y)	
	tl63 = "Mouse Pos In World: <<"
	tl63 += FLOAT_TO_STRING(vMouseToWorldCoordResult[1].x)
	tl63 += ", "
	
	tl63 += FLOAT_TO_STRING(vMouseToWorldCoordResult[1].y)
	tl63 += ", "
	tl63 += FLOAT_TO_STRING(vMouseToWorldCoordResult[1].z)
	tl63 += ">>"	
	DRAW_DEBUG_TEXT_2D(tl63, vPos)
	
	DRAW_MARKER(MARKER_SPHERE, vMouseToWorldCoordResult[1], (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.3, 0.3, 0.3>>))	
	
	IF DOES_ENTITY_EXIST(eiMouseToWorldCoordCompareTarget)
	AND NOT IS_VECTOR_ZERO(GET_ENTITY_COORDS(eiMouseToWorldCoordCompareTarget))
		
		tl63 = "Local Offset From Target: <<"
		tl63 += FLOAT_TO_STRING(vOffset.x)
		tl63 += ", "
		tl63 += FLOAT_TO_STRING(vOffset.y)
		tl63 += ", "
		tl63 += FLOAT_TO_STRING(vOffset.z)
		tl63 += ">>"
			
		vPos.y += 0.05
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
		
		tl63 = "Distance Target: "
		tl63 += FLOAT_TO_STRING(fDist)
		vPos.y += 0.05
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
		
		// --- Target
		
		VECTOR vTempPos = GET_ENTITY_COORDS(eiMouseToWorldCoordCompareTarget)
		GET_SCREEN_COORD_FROM_WORLD_COORD(vTempPos, vPos.x, vPos.y)
		tl63 = "Target Coord: <<"
		tl63 += FLOAT_TO_STRING(vTempPos.x)
		tl63 += ", "
		tl63 += FLOAT_TO_STRING(vTempPos.y)
		tl63 += ", "
		tl63 += FLOAT_TO_STRING(vTempPos.z)
		tl63 += ">>"
			
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
		
		vPos.y += 0.05
		
		tl63 = "Target Heading: "
		tl63 += FLOAT_TO_STRING(GET_ENTITY_HEADING(eiMouseToWorldCoordCompareTarget))
			
		DRAW_DEBUG_TEXT_2D(tl63, vPos)
		DRAW_MARKER(MARKER_SPHERE, GET_ENTITY_COORDS(eiMouseToWorldCoordCompareTarget), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.3, 0.3, 0.3>>), 100, 255, 100)
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
		PRINTLN("--- PROCESS_MOUSE_TO_WORLD_COORD_SHAPETEST ---")
		VECTOR vTargetCoord = GET_ENTITY_COORDS(eiMouseToWorldCoordCompareTarget)
		FLOAT fTargetHeading = GET_ENTITY_HEADING(eiMouseToWorldCoordCompareTarget)		
		PRINTLN("Mouse Pos In World: <<", vMouseToWorldCoordResult[1].x, ",", vMouseToWorldCoordResult[1].y, ",",vMouseToWorldCoordResult[1].z, ">>")		
		PRINTLN("Distance Target: ", fDist)
		PRINTLN("Local Offset From Target: <<", vOffset.x, ",", vOffset.y, ",",vOffset.z, ">>")
		
		PRINTLN("---")
		PRINTLN("Target Coord: <<", vTargetCoord.x, ",", vTargetCoord.y, ",",vTargetCoord.z, ">>")		
		PRINTLN("Target Heading: ", fTargetHeading)
		PRINTLN("---")
	ENDIF
ENDPROC

PROC CREATE_TUNER_INTRO_CAM_WIDGETS()
	START_WIDGET_GROUP("Temp Intro Cam - Tuner Overrides")
		
	ADD_WIDGET_VECTOR_SLIDER("Intro Cam Pos 1", g_vTempIntroCamTuner_IntroCamPos1, -20.0, 20.0, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Intro Cam Point 1", g_vTempIntroCamTuner_IntroCamPoint1, -20.0, 20.0, 0.01)
	
	ADD_WIDGET_VECTOR_SLIDER("Intro Cam Pos 2", g_vTempIntroCamTuner_IntroCamPos2, -20.0, 20.0, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Intro Cam Point 2", g_vTempIntroCamTuner_IntroCamPoint2, -20.0, 20.0, 0.01)
	
	ADD_WIDGET_VECTOR_SLIDER("Outro Cam Pos 1", g_vTempIntroCamTuner_OutroCamPos1, -20.0, 20.0, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("Outro Cam Point 1", g_vTempIntroCamTuner_OutroCamPoint1, -20.0, 20.0, 0.01)
	
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_CONVERSATION_AND_DIALOGUE_WIDGETS()
	START_WIDGET_GROUP("Conversation and Dialogue Testing")
		
		ADD_WIDGET_BOOL("Toggle On Screen conversation Debug", g_bEnableConversationDebugFromWidget)
		
		g_ConversationDebugCustomBlock = ADD_TEXT_WIDGET("Block")
		SET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomBlock, "null")
		
		g_ConversationDebugCustomRoot = ADD_TEXT_WIDGET("Root")
		SET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomRoot, "null")
		
		g_ConversationDebugCustomLabel = ADD_TEXT_WIDGET("Label")
		SET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomLabel, "null")
		
	
		g_widConversationDebugVoiceNamePed1 = ADD_TEXT_WIDGET("Ped Voice Name 1")
		SET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed1, "null")		
		ADD_WIDGET_INT_SLIDER("Ped Voice Number 1:", g_ConversationDebugVoiceNumberPed1, -1, 16, 1)
		
		g_widConversationDebugVoiceNamePed2 = ADD_TEXT_WIDGET("Ped Voice Name 2")
		SET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed2, "null")		
		ADD_WIDGET_INT_SLIDER("Ped Voice Number 2:", g_ConversationDebugVoiceNumberPed2, -1, 16, 1)
		
		g_widConversationDebugVoiceNamePed3 = ADD_TEXT_WIDGET("Ped Voice Name 3")
		SET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed3, "null")		
		ADD_WIDGET_INT_SLIDER("Ped Voice Number 3:", g_ConversationDebugVoiceNumberPed3, -1, 16, 1)
				
		ADD_WIDGET_BOOL("Play Conversation From Root", g_bEnableConversationDebugCustomPlaying)
		
		ADD_WIDGET_BOOL("Play Single Line (Label with with Ped Voice 1)", g_bEnableConversationDebugCustomPlayingLine)
		
		ADD_WIDGET_BOOL("Play Single Context Ambiently (Label with with Ped Voice 1)", g_bEnableConversationDebugCustomPlayingAmbientLinePos)
		
		ADD_WIDGET_BOOL("Immediately Kill Convo/Line", g_bEnableConversationDebugCustomKillConvo)
								
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_CONVERSATION_AND_DIALOGUE_WIDGETS()
	
	IF DOES_TEXT_WIDGET_EXIST(g_ConversationDebugCustomBlock)
		g_tl31_Block_DiagDebug = GET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomBlock)
	ENDIF
	IF DOES_TEXT_WIDGET_EXIST(g_ConversationDebugCustomRoot)
		g_tl31_Root_DiagDebug = GET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomRoot)
	ENDIF
	IF DOES_TEXT_WIDGET_EXIST(g_ConversationDebugCustomLabel)
		g_tl31_Label_DiagDebug = GET_CONTENTS_OF_TEXT_WIDGET(g_ConversationDebugCustomLabel)
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(g_widConversationDebugVoiceNamePed1)
		g_ConversationDebugVoiceNamePed1 = GET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed1)
	ENDIF
	IF DOES_TEXT_WIDGET_EXIST(g_widConversationDebugVoiceNamePed2)
		g_ConversationDebugVoiceNamePed2 = GET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed2)
	ENDIF
	IF DOES_TEXT_WIDGET_EXIST(g_widConversationDebugVoiceNamePed3)
		g_ConversationDebugVoiceNamePed3 = GET_CONTENTS_OF_TEXT_WIDGET(g_widConversationDebugVoiceNamePed3)
	ENDIF
	
ENDPROC

 #ENDIF
 
// *****************************************************************************************
//	SYSTEMS DEBUG
// *****************************************************************************************

/// PURPOSE: Creates the widget that enable a couple of systems to work in debug mode
PROC Create_Systems_Widgets()

	START_WIDGET_GROUP("Misc Systems Debug")
		ADD_WIDGET_BOOL("Allow shop systems in debug", g_bShopsAvailableInDebug)
		ADD_WIDGET_BOOL("Allow savegame bed in debug", g_bSavegameBedAvailableInDebug)
		ADD_WIDGET_BOOL("Allow vehicle gen in debug", g_bVehicleGenAvailableInDebug)
		ADD_WIDGET_BOOL("Allow random characters in debug", g_bRandomCharsAvailableInDebug)
		
		ADD_BIT_FIELD_WIDGET("Debug Timestamp Bitset", g_iDebugTimestampBitset)
	STOP_WIDGET_GROUP()

ENDPROC

PROC Create_transition_test_widget()
	START_WIDGET_GROUP("Automated Leave/Join Test")
		ADD_WIDGET_BOOL("Start Automated Join/Leave Sessions Test. PRESS: 'SHIFT + M' after click", g_binitiatejoinleavetest)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE: Creates the debug menu widgets
PROC Create_Debug_Menu_Widgets()

	START_WIDGET_GROUP("Debug Menu")
		ADD_WIDGET_BOOL("Use Bugstar Query", g_debugMenuControl.bUseBugstarQuery)
		ADD_WIDGET_BOOL("Bugstar Queries Disabled", g_bugstarQuery.bQueriesDisabled)
		
		ADD_WIDGET_BOOL("Bugstar Query Started", g_bugstarQuery.bQueryStarted)
		ADD_WIDGET_INT_READ_ONLY("Bugstar Query ID", g_bugstarQuery.iQueryID)
		//ADD_BIT_FIELD_WIDGET("Process Flags", sDebugMenuData.iProcessFlags)
		
		ADD_WIDGET_BOOL("Display Bugs", sDebugMenuData.sBugData.bDisplay)
		
		ADD_WIDGET_FLOAT_SLIDER("fTempPos[0]", g_debugMenuControl.fTempPos[0], -10000.0, 10000.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fTempPos[1]", g_debugMenuControl.fTempPos[1], -10000.0, 10000.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fTempPos[2]", g_debugMenuControl.fTempPos[2], -10000.0, 10000.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fTempPos[3]", g_debugMenuControl.fTempPos[3], -10000.0, 10000.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fTempPos[4]", g_debugMenuControl.fTempPos[4], -10000.0, 10000.0, 0.01)
	STOP_WIDGET_GROUP()

ENDPROC


/// PURPOSE: Creates the Metrics Zone system widget.
PROC Create_Metrics_Zone_Widgets()

	START_WIDGET_GROUP("Metrics Zones")
		ADD_WIDGET_BOOL("Enable Mission Metrics Zones", g_bFlowMetricZonesEnabled)
		ADD_WIDGET_BOOL("Show Current Metrics Zone Results", m_bShowCurrentMetricsZoneResults)
		ADD_WIDGET_BOOL("Hide Current Metrics Zone Results", m_bHideCurrentMetricsZoneResults)
		ADD_WIDGET_BOOL("Clear Current Metrics Zone Results", m_bClearCurrentMetricsZoneResults)
	STOP_WIDGET_GROUP()

ENDPROC

PROC Maintain_Metrics_Zone_Widgets()
	
	IF m_bShowCurrentMetricsZoneResults
		METRICS_ZONES_SHOW()
		// KGM 28/3/12: Modified to be used in MP also
		// NOTE: This will have to change again when we use the new wrap-safe network time functions
		g_iTimeDisplayedMetricsReport = GET_GAME_TIMER()
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			g_iTimeDisplayedMetricsReport_NETWORK = GET_NETWORK_TIME()
		ENDIF
		g_bDisplayingMetricsZoneResults = TRUE
		m_bShowCurrentMetricsZoneResults = FALSE
	ENDIF
	IF m_bHideCurrentMetricsZoneResults
		METRICS_ZONES_HIDE()
		g_bDisplayingMetricsZoneResults = FALSE
		m_bHideCurrentMetricsZoneResults = FALSE
	ENDIF
	IF m_bClearCurrentMetricsZoneResults
		METRICS_ZONES_CLEAR()
		m_bClearCurrentMetricsZoneResults = FALSE
	ENDIF
	
	IF g_bDisplayingMetricsZoneResults
		// KGM 28/3/12: Modified to be used in MP also
		// NOTE: This will have to change again when we use the new wrap-safe network time functions
		
		
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			TIME_DATATYPE netTimer = GET_NETWORK_TIME()
			IF IS_TIME_MORE_THAN(netTimer , GET_TIME_OFFSET(g_iTimeDisplayedMetricsReport_NETWORK , TIME_DISPLAY_METRICS_REPORT))
				//C//PRINTLN(DEBUG_METRICS, "Metric zone report cleaning up after displaying for ", TIME_DISPLAY_METRICS_REPORT, "ms.")
				METRICS_ZONES_HIDE()
				g_bDisplayingMetricsZoneResults = FALSE
			ENDIF
		ELSE
			INT theTimer = GET_GAME_TIMER()
			IF theTimer > g_iTimeDisplayedMetricsReport + TIME_DISPLAY_METRICS_REPORT
				//C//PRINTLN(DEBUG_METRICS, "Metric zone report cleaning up after displaying for ", TIME_DISPLAY_METRICS_REPORT, "ms.")
				METRICS_ZONES_HIDE()
				g_bDisplayingMetricsZoneResults = FALSE
			ENDIF		
		ENDIF
		

		
		
	ENDIF
ENDPROC

///Purpose: Gives player all weapons or removes them (Shift pressed)
PROC Maintain_KeyE_Weapon_Controls()
	if IS_DEBUG_KEY_JUST_PRESSED(KEY_E,KEYBOARD_MODIFIER_SHIFT,"Remove all player weapons")
		REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
	ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_E,KEYBOARD_MODIFIER_CTRL,"Halve player ammo")
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_GRENADE_LAUNCHER,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_GRENADE_LAUNCHER)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MG,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MG)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MINIGUN,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MINIGUN)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MOLOTOV,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_MOLOTOV)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_PETROL_CAN,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_PETROL_CAN)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_PISTOL,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_PISTOL)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_REMOTE,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_REMOTE)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_RIFLE,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_RIFLE)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_RPG,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_RPG)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SHOTGUN,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SHOTGUN)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SMG,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SMG)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SNIPER,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_SNIPER)/2)
		SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_STICKY_BOMB,GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(),AMMOTYPE_STICKY_BOMB)/2)
	ENDIF
ENDPROC

/// PURPOSE:
///    Prints the camera and player position to the console log when pressing shift-c
PROC Maintain_KeyC_Camera_Print()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C,KEYBOARD_MODIFIER_SHIFT,"Print camera/player positions")
		Vector p1,p2
		p1 = GET_FINAL_RENDERED_CAM_COORD()
		p2 = GET_ENTITY_COORDS(PLAYER_PED_ID())
		CPRINTLN(DEBUG_AMBIENT,"Cam: ",p1, " Player: ",p2)
	ENDIF
ENDPROC

/// PURPOSE: Start transition test. Automatically switches game modes repeatedly.


/*
/// PURPOSE: Create a prostitute ped brain for testing
PROC Maintain_Ambient_Widgets()
	PED_INDEX randomPed
	VECTOR creationPos
	
	REQUEST_MODEL(S_F_Y_HOOKER_01)
	
	IF m_createProstituteAtNearestCarNode
	AND HAS_MODEL_LOADED(S_F_Y_HOOKER_01)
		IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), creationPos)
					
					
					//randomPed = CREATE_RANDOM_PED(creationPos)
					randomPed = CREATE_PED(PEDTYPE_PROSTITUTE,S_F_Y_HOOKER_01, creationPos, 0.0)
					SET_PED_AS_NO_LONGER_NEEDED(randomPed)
					
					m_createRandomPedAtNearestCarNode = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

*/

// ===========================================================================================================

/// Little test of a Social Club leaderboard read for SP races.
PROC CREATE_SC_LB_READ_WIDGET()
	START_WIDGET_GROUP("SC Leaderboard Read")
		ADD_WIDGET_BOOL("Read SP race board", sclb_Debug.bRead)
		ADD_WIDGET_INT_SLIDER("SP race ID", sclb_Debug.iRaceID,0,8,1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_SC_LB_READ_WIDGET()
	IF sclb_Debug.bRead
		IF GET_SP_RACE_PERSONAL_GLOBAL_BEST(sclb_Debug.iReadProgress,sclb_Debug.iOverallProgress,sclb_Debug.bReadResult,sclb_Debug.iRaceID,sclb_Debug.iGlobalBest,sclb_Debug.iPersonalBest)
			//PRINTLN("MAINTAIN_SC_LB_READ_WIDGET: Global best for SP race ID:",sclb_Debug.iRaceID," = ",sclb_Debug.iGlobalBest)
			//PRINTLN("MAINTAIN_SC_LB_READ_WIDGET: Personal best for SP race ID:",sclb_Debug.iRaceID," = ",sclb_Debug.iPersonalBest)
			CLEANUP_SP_RACE_PERSONAL_GLOBAL_BEST(sclb_Debug.iReadProgress,sclb_Debug.iOverallProgress,sclb_Debug.bReadResult,sclb_Debug.iGlobalBest,sclb_Debug.iPersonalBest)
			sclb_Debug.bRead = FALSE
		ENDIF
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Persistent MP heist planning board debugging options.
PROC CREATE_HEIST_DEBUG_WIDGETS()
	START_WIDGET_GROUP("MP Heist Planning Board")
		START_WIDGET_GROUP("Help-text Sequence Timing")
			ADD_WIDGET_INT_SLIDER("OVERVIEW DUR(ms): ", ci_HEIST_HELP_TEXT_OVERVIEW_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("OVERVIEW 2 DUR(ms): ", ci_HEIST_HELP_TEXT_OVERVIEW_2_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("PIE CHART DURATION(ms): ", ci_HEIST_HELP_TEXT_PIE_CHART_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("CENTRE TABLE DURATION(ms): ", ci_HEIST_HELP_TEXT_TABLE_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("STATS CARD DURATION(ms): ", ci_HEIST_HELP_TEXT_PLAYER_CARD_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("TODO LIST DURATION(ms): ", ci_HEIST_HELP_TEXT_TODO_LIST_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("MAP DURATION(ms): ", ci_HEIST_HELP_TEXT_MAP_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("STOAGE LOCKER DUR(ms): ", ci_HEIST_HELP_TEXT_STORAGE_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("STORAGE LOCKER 2 DUR(ms): ", ci_HEIST_HELP_TEXT_STORAGE_2_DURATION,1000,60000,500)
			ADD_WIDGET_INT_SLIDER("FINAL DURATION(ms): ", ci_HEIST_HELP_TEXT_FINAL_DURATION,1000,60000,500)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF




// ===========================================================================================================

/// PURPOSE:
///    Checks for any keyboard-activated debug commands specific to singleplayer.
PROC Maintain_SP_Debug_Keypresses()

	Maintain_KeyF6_Cash()
	Maintain_KeyQ_Swap_Player()
	Maintain_KeyH_Warp_To_Savehouse()
	Maintain_KeyM_Toggle_Mission_Menu()
	Maintain_KeyJ_Skips()
	Maintain_KeyK_Kill_Player()
	Maintain_KeyE_Weapon_Controls()
	Maintain_KeyC_Camera_Print()
	Maintain_KeyF8_Toggle_Family_Control_Debug_Screen()
	Maintain_KeyF10_Toggle_Friends_Control_Debug_Screen()
	Maintain_KeyF11_Toggle_Random_Event_Debug_Screen()
	
ENDPROC

// ===========================================================================================================

/// PURPOSE:
///    Checks for any Widget-activated debug commands specific to singleplayer.
PROC Maintain_SP_Debug_Widgets()

	Maintain_Ambient_Widgets()

ENDPROC


// ===========================================================================================================

/// PURPOSE: Runs the update procedure for the visual flow diagram while the diagram is flagged as being active.
PROC Maintain_SP_Visual_Flow_Diagram(FLOW_LAUNCHER_VARS &flowLauncher)

	IF g_flowDiagram.bFlowDiagramActive
		UPDATE_FLOW_DIAGRAM(sDebugFlowDiagram, flowLauncher)
	ENDIF
	
ENDPROC


// ===========================================================================================================

/// PURPOSE:
///    Gets a debug timestamp using a bugstar query
PROC Matintain_Debug_Timestamp()

	////PRINTLN("Current time: ", GET_DEBUG_TIMESTAMP(), ".")
	
	IF NOT HAS_BUGSTAR_QUERY_STARTED()
		CLEAR_BIT(g_iDebugTimestampBitset, 1)
		CLEAR_BIT(g_iDebugTimestampBitset, 2)
	ENDIF
	
	IF IS_BIT_SET(g_iDebugTimestampBitset, 0)

		IF NOT IS_BIT_SET(g_iDebugTimestampBitset, 1)
			// Start the query if we havent done so already
			IF IS_BUGSTAR_QUERY_SAFE_TO_START(FALSE)
				TEXT_LABEL_63 sQuery = "MissionsV2/1/Bugs?fields=Id"
				START_BUGSTAR_QUERY(sQuery)
				//CDEBUG1LN(DEBUG_SYSTEM, "...starting new query: ", sQuery)
				SET_BUGSTAR_QUERY_STARTED()
				SET_BIT(g_iDebugTimestampBitset, 1)
			ENDIF
		ELSE
			// Wait for the query to finish
			IF NOT (IS_BUGSTAR_QUERY_PENDING())
				SET_BIT(g_iDebugTimestampBitset, 2)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_iDebugTimestampBitset, 2)
			// If the query was successful, create the xml and parse
			IF NOT IS_BUGSTAR_QUERY_SUCCESSFUL()
				//C//PRINTLN(DEBUG_SYSTEM, "Matintain_Debug_Timestamp: query not successful")
				// CLEAN UP QUERY
				END_BUGSTAR_QUERY()
				SET_BUGSTAR_QUERY_FINISHED()
				g_iDebugTimestampBitset = 0
			ELSE
				//CDEBUG1LN(DEBUG_SYSTEM, "...query successful")
				
				IF NOT CREATE_XML_FROM_BUGSTAR_QUERY()
					//C//PRINTLN(DEBUG_SYSTEM, "Matintain_Debug_Timestamp: could not create XML from query")
					// CLEAN UP QUERY
					END_BUGSTAR_QUERY()
					SET_BUGSTAR_QUERY_FINISHED()
					g_iDebugTimestampBitset = 0
				ELSE
				
					INT iNodeCount = GET_NUMBER_OF_XML_NODES()
					
					// Check that the xml contains some nodes
					IF iNodeCount = 0
						//C//PRINTLN(DEBUG_SYSTEM, "Matintain_Debug_Timestamp: no nodes in XML from query")
						DELETE_XML_FILE()
						// CLEAN UP QUERY
						END_BUGSTAR_QUERY()
						SET_BUGSTAR_QUERY_FINISHED()
						g_iDebugTimestampBitset = 0
					ELSE
						//CDEBUG1LN(DEBUG_SYSTEM, "Matintain_Debug_Timestamp: Loaded XML/query successful - grabbing data")
						
						// Integers used for the FOR loops going through all the nodes and attributes
						INT eachNode
						TEXT_LABEL_31 tlCurrentDate
						
						// Loop through all the nodes
						FOR eachNode = 0 TO (iNodeCount-1)
							SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
								// 'currentDate'
								CASE DM_HASH_CURRENT_DATE
									tlCurrentDate = GET_STRING_FROM_XML_NODE()
									// Compress, 2012-04-27T17:02:48.898+01:00 to 270412_170248
									
									g_tlDebugTimestamp = GET_STRING_FROM_STRING(tlCurrentDate, 8, 10)
									g_tlDebugTimestamp += GET_STRING_FROM_STRING(tlCurrentDate, 5, 7)
									g_tlDebugTimestamp += GET_STRING_FROM_STRING(tlCurrentDate, 2, 4)
									g_tlDebugTimestamp += "_"
									g_tlDebugTimestamp += GET_STRING_FROM_STRING(tlCurrentDate, 11, 13)
									g_tlDebugTimestamp += GET_STRING_FROM_STRING(tlCurrentDate, 14, 16)
									g_tlDebugTimestamp += GET_STRING_FROM_STRING(tlCurrentDate, 17, 19)
									
									//CDEBUG1LN(DEBUG_SYSTEM, "Matintain_Debug_Timestamp:  Current date is ", g_tlDebugTimestamp)
									eachNode = iNodeCount // Bail
								BREAK
							ENDSWITCH
							
							// Tell the script to goto the next node in the xml file
							GET_NEXT_XML_NODE()
						ENDFOR
						
						DELETE_XML_FILE()
						// CLEAN UP QUERY
						END_BUGSTAR_QUERY()
						SET_BUGSTAR_QUERY_FINISHED()
						g_iDebugTimestampBitset = 0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// *****************************************************************************************
//	Debug General Housekeeping
// *****************************************************************************************

// PURPOSE:	Maintains any regular debug housekeeping
// 
PROC Maintain_Launch_And_Forget_Debug_Display_Array()

	INT tempLoop = 0
	
	// Check all launch and forget missions to see if they are still active
	REPEAT DEBUG_MAX_LAUNCH_AND_FORGET_SCRIPTS tempLoop
		IF NOT (g_debugLaunchAndForgetScripts[tempLoop].theThreadID = NULL)
			IF NOT (IS_THREAD_ACTIVE(g_debugLaunchAndForgetScripts[tempLoop].theThreadID))
				g_debugLaunchAndForgetScripts[tempLoop].theThreadID = NULL
				g_debugLaunchAndForgetScripts[tempLoop].filename	= ""
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintains any regular debug housekeeping
// 
PROC SP_Debug_General_Housekeeping()

	Maintain_Launch_And_Forget_Debug_Display_Array()

ENDPROC

PROC SCRIPT_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC

// ===========================================================================================================

PROC CREATE_WILDLIFE_PHOTOGRAPHY_WIDGETS()

	START_WIDGET_GROUP("Photography Wildlife")
		ADD_WIDGET_BOOL("Launch photographyWildlife Script", g_bLanchPhotographyWildlifeScript)
		ADD_WIDGET_BOOL("Terminate photographyWildlife Script", g_bTerminatePhotographyWildlifeScript)
		ADD_WIDGET_BOOL("Block photographyWildlife Auto Restart", g_bBlockPhotographyWildlifeRelaunch)
		ADD_WIDGET_BOOL("Clear photographyWildlife Save Data", g_bClearAllPhotographyWildlifeSaveData)
	STOP_WIDGET_GROUP()

ENDPROC


PROC MAINTAIN_WILDLIFE_PHOTOGRAPHY_WIDGETS()

	IF g_bLanchPhotographyWildlifeScript
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("photographyWildlife")) = 0
			//CLEAR_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ALL_PHOTOGRAPHS_COLLECTED)
			//CLEAR_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED)
			REQUEST_AND_LAUNCH_SCRIPT_WITH_NAME_HASH( HASH("photographyWildlife") #IF IS_DEBUG_BUILD ,"photographyWildlife" #ENDIF )
		ENDIF
		
		g_bLanchPhotographyWildlifeScript = FALSE
	ENDIF

	IF g_bTerminatePhotographyWildlifeScript
		TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("photographyWildlife")
		
		CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_DATA()
		
		CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_TEXT_DATA()
		
		g_bTerminatePhotographyWildlifeScript = FALSE
	ENDIF
	
	IF g_bClearAllPhotographyWildlifeSaveData
		CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_DATA()
		g_bClearAllPhotographyWildlifeSaveData = FALSE
	ENDIF

ENDPROC



SCRIPT
	// Set the XML paths, since we can't do default values for text labels
	m_flowLauncher.txtLauncherXMLPath				= "X:/gta5/titleupdate/dev_ng/common/data/script/xml/"
	m_flowLauncher.txtLauncherDiagramXMLFile		= "debug/SP_flow_diagram.xml"
	m_flowLauncher.txtLauncherPlaythroughXMLFile	= "SPPlaythrough/SP_playthrough_order.xml"
	
	// Default time/date debug text screen positions.
	m_sharedDebugVars.fCurrentTimeDateX 	= 0.140
	m_sharedDebugVars.fCurrentTimeDateY 	= -0.430
	m_sharedDebugVars.fDebugTimeDateX 		= 0.140
	m_sharedDebugVars.fDebugTimeDateY 		= -0.410
	
	g_flowUnsaved.bShowMissionFlowDebugScreen = TRUE
	
	Initialise_Batch_Mission_Launcher(m_batchLauncher)

	#IF IS_FINAL_BUILD
		SCRIPT_ASSERT("debug.sc should not be active in RELEASE SCRIPT MODE")
		EXIT
	#ENDIF

	// We have one common debug script that runs in both sp and mp. 
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	//**************************
	//
	//   DEBUG LOOP
	//
	//**************************
	//PRINTSTRING("GTA5 - Starting Debug Loop")
	//PRINTNL()
	
	//Initialise the systems.
	Initialise_Vehicle_Chase_Debug(m_VehicleChaseDebugData)
	
	//create the start widgets
	widgetDebug = START_WIDGET_GROUP("Debug")
//		ADD_BIT_FIELD_WIDGET("Process Flags", sDebugMenuData.iProcessFlags)
		ADD_WIDGET_BOOL("Kill script", m_bKillScript)
		ADD_WIDGET_BOOL("Restart script", m_bRestartScript)
		Intialise_Debug_Start_Positions_Widget()
		Create_Shared_Debug_Widgets(m_sharedDebugVars)
		Create_Ambient_Widgets()
		Create_Systems_Widgets()
		CREATE_F9_WIDGET() // mp 
		Create_debug_Menu_Widgets()
		Create_Metrics_Zone_Widgets()
		Create_Vehicle_Chase_Debug_Widgets(m_VehicleChaseDebugData)
		Create_transition_test_widget()
		CREATE_SC_LB_READ_WIDGET()
		CREATE_HEIST_DEBUG_WIDGETS()
		CREATE_WILDLIFE_PHOTOGRAPHY_WIDGETS()		
		CREATE_CONVERSATION_AND_DIALOGUE_WIDGETS()
		CREATE_TUNER_INTRO_CAM_WIDGETS()
	STOP_WIDGET_GROUP()
	
	sDebugFlowDiagram.strFlowDiagramXML = "X:/gta5/titleupdate/dev_ng/common/data/script/xml/debug/SP_flow_diagram.xml"
	sDebugFlowDiagram.sBugstarData.sBugstarFile = "X:/gta5/build/dev_ng/common/data/script/xml/SPDebug/bugstar.xml"
	sDebugFlowDiagram.bDrawCursor = TRUE
	
	
	// Allow bugstar integration with the debug menu?
	g_bugstarQuery.bQueriesDisabled = TRUE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_integration")
		g_bugstarQuery.bQueriesDisabled = FALSE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("populationTools")
	    bDebugPopulationToolsEnabled = TRUE
	ENDIF
	
//	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UseCodeAIBlipSystem")
//		g_bUseCodeAIBlipSystem = TRUE
//		//PRINTSTRING("setting g_bUseCodeAIBlipSystem = TRUE")
//		//PRINTNL()
//	ELSE
//		//PRINTSTRING("no sc_UseCodeAIBlipSystem found")
//		//PRINTNL()
//	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("allInteriorsOn")//SPCID_TURN_ON_ALL_INTERIORS
        CPRINTLN(DEBUG_INIT, "WARNING! all mission ineriors were enabled from the command line.")		
		SET_INTERIOR_CAPPED(INTERIOR_V_COMEDY, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_PSYCHEOFFICE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_RANCH, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_RECYCLE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_JANITOR, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_TORTURE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_VINEWOOD, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_MORNINGWOOD, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_CINEMA_DOWNTOWN, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_EPSILONISM, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGES, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEL, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_SHERIFF, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_SHERIFF2, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_LAB, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_CHOPSHOP, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_FOUNDRY, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_ABATTOIR, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_58_SOL_OFFICE, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_1, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_2, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_3, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_4, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_5, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_6, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_7, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_8, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_9, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_10, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_11, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_12, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_13, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_14, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_15, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_16, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_17, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_18, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_19, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APARTMENT_HIGH_20, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_APART_MIDSPAZ, FALSE)
		SET_INTERIOR_CAPPED(INTERIOR_V_GARAGEM_SP, FALSE)
		
		SET_INTERIOR_DISABLED(INTERIOR_V_COMEDY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_PSYCHEOFFICE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_RANCH, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_RECYCLE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_ROCKCLUB, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_JANITOR, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_LESTERS, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_TORTURE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_VINEWOOD, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_MORNINGWOOD, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_DOWNTOWN, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_EPSILONISM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SHERIFF, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_SHERIFF2, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_LAB, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FARMHOUSE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_CHOPSHOP, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_FOUNDRY, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_ABATTOIR, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_1, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_2, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_3, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_4, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_5, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_6, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_7, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_8, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_9, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_10, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_11, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_12, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_13, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_14, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_15, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_16, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_17, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_18, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_19, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_20, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_APART_MIDSPAZ, FALSE)
		SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM_SP, FALSE)
    ENDIF
	
	WHILE (TRUE)
		
		WAIT(0)
		
		//PRINTLN(DEBUG_SYSTEM, "debug runnign")

		Maintain_end_of_game_skip()
		Maintain_Shared_Debug_Keypresses(stF9SpaceTimer)
		Maintain_Shared_Debug_Widgets(m_sharedDebugVars)
		Matintain_Debug_Timestamp()
		Maintain_Metrics_Zone_Widgets()	// Metrics functions are now SP and MP compatible
		Maintain_Vehicle_Chase_Debug(m_VehicleChaseDebugData)
		MaintainPopulationDebug() 
		Maintain_Game_Time_Date_Displaying()
		MAINTAIN_WILDLIFE_PHOTOGRAPHY_WIDGETS()
		MAINTAIN_CONVERSATION_AND_DIALOGUE_WIDGETS()
		
		MAINTAIN_SC_LB_READ_WIDGET()
		// To run in SP only once the main script has been initialised.
		IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
			IF (g_isSPMainInitialised)
				Maintain_SP_Debug_Keypresses()
				Maintain_SP_Debug_Widgets()
				Maintain_SP_Visual_Flow_Diagram(m_flowLauncher)
				SP_Debug_General_Housekeeping()
				Run_Batch_Mission_Launch_Routines(m_batchLauncher, m_flowLauncher)
				
				PROCESS_DEBUG_MENU(sDebugMenuData)
				PROCESS_DEBUG_MENU_PRELAUNCH_CODE_ID_EXECUTION(sDebugMenuData) 	// Must come before flow activation.
				PROCESS_DEBUG_MENU_FLOW_ACTIVATION(sDebugMenuData, sDebugFlowDiagram, m_flowLauncher)
				PROCESS_DEBUG_MENU_GAME_TIME_MANAGEMENT(sDebugMenuData) 		// Must come after flow activation.
				PROCESS_DEBUG_MENU_CODE_ID_EXECUTION(sDebugMenuData)			// Must come after flow activation.
				
				Update_Start_Pos_Widget_Response()
				
				IF NOT m_flowLauncher.bLauncherWidgetSetup
					SET_CURRENT_WIDGET_GROUP(widgetDebug)
						LAUNCHER_CREATE_LAUNCH_TO_MULTIPLE_LABELS_WIDGET(m_flowLauncher)
					CLEAR_CURRENT_WIDGET_GROUP(widgetDebug)
					m_flowLauncher.bLauncherWidgetSetup = TRUE
				ELSE
					LAUNCHER_UPDATE_ACTIVATE_TO_MULTIPLE_LABELS_WIDGET(m_flowLauncher)
				ENDIF
				
				IF IS_BIT_SET(m_sharedDebugFlags, DBG_F9_SCREEN_ON)
					DISPLAY_FLOW_INFO_SCREEN()
				ENDIF
			ENDIF
			
			
			
			bRelaunchTimerSet = FALSE
		ENDIF
		
		//Draw transition session debug
		DRAW_PLAYERS_IN_MY_TRANSITION_SESSION()
		
		
		
		// To run in MP only
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			Maintain_KeyM_Toggle_Mission_Menu()
			
			// Requires updated every frame
			PROCESS_DEBUG_MENU(sDebugMenuData)
			
			// f9 screen
			UPDATE_MP_F9_SCREEN()
			
			// Relaunch the xml_menus script when we jump to MP
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("xml_menus")) = 0
			AND NOT NETWORK_IS_SCRIPT_ACTIVE("xml_menus", DEFAULT_MISSION_INSTANCE, TRUE)
			AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			AND NOT IS_TRANSITION_SESSION_LAUNCHING()
				IF NOT bRelaunchTimerSet
					tdRelaunchInMPTimer = GET_GAME_TIMER() // used to be 'get_network_timer'. Changed to fix 1493461 KW
					bRelaunchTimerSet = TRUE
				ELIF GET_GAME_TIMER()> (tdRelaunchInMPTimer+ 1500)
					IF NETWORK_IS_IN_SESSION()
					AND IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
						// Do not launch when in the transition proecss.
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("MainTransition")) = 0
							REQUEST_SCRIPT("xml_menus")
							IF HAS_SCRIPT_LOADED("xml_menus")
								START_NEW_SCRIPT("xml_menus", DEFAULT_STACK_SIZE)
								SET_SCRIPT_AS_NO_LONGER_NEEDED("xml_menus")
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
			ELSE
				bRelaunchTimerSet = FALSE
			ENDIF
						
			DEBUG_SHOW_DIALOGUE_ROOT_PLAYING()
			
			PROCESS_MOUSE_TO_WORLD_COORD_SHAPETEST()
			
			PROCESS_MUTING_MUSIC()
	
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F1,KEYBOARD_MODIFIER_SHIFT,"terminate debug")
			OR m_bKillScript
				PRINT_HELP_WITH_PLAYER_NAME("STRING", "Debug.sc Terminated", HUD_COLOUR_WHITE)
				SCRIPT_CLEANUP()
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F3,KEYBOARD_MODIFIER_SHIFT,"restart debug")
			OR m_bRestartScript 
				PRINT_HELP_WITH_PLAYER_NAME("STRING", "Debug.sc Will Restart", HUD_COLOUR_WHITE)
				g_dRestartDebugSc = TRUE
				SCRIPT_CLEANUP()
			ENDIF
			
		ENDIF
		
		// If the SP Main script does not exist then clear the 'sp main initialised' flag
		// NOTE: This gets set to TRUE in 'main' just before the main loop starts
		IF (g_isSPMainInitialised)
			IF (!g_bLoadedClifford and !g_bLoadedNorman and (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main")) = 0))
			OR (g_bLoadedClifford and (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main_CLF")) = 0))
			OR (g_bLoadedNorman and (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("main_NRM")) = 0))
				g_isSPMainInitialised = FALSE
			ENDIF
		ENDIF		
		
		
	ENDWHILE
	
ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

