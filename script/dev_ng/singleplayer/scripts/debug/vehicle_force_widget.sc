//***********************************Vehicle Force Widget
//***********Author: Lawrence Kerr


using "rage_builtins.sch"
using "globals.sch"

#IF IS_FINAL_BUILD
	
	script
	
	endscript 
	
#endif 

#IF IS_DEBUG_BUILD

using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "commands_misc.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "model_enums.sch"
using "cellphone_public.sch"
using "selector_public.sch"

bool reset_vehicle_position = false
bool create_veh = false
bool apply_force_position = false
bool update_vehicle = false
bool original_creation = false
bool output_data = false

int force_status = 0

float force_multiplier = 1.0
float vehicle_rot

vector force_vec_a
vector force_vec_b 
vector force_vec_ba
vector force_offset = <<0.5, 0.5, 1.0>>

vehicle_index current_car

TEXT_WIDGET_ID widget_model_name

model_names vehicle_model

func bool mission_vehicle_injured(vehicle_index &this_vehicle)
	
	if DOES_ENTITY_EXIST(this_vehicle)
		if not is_vehicle_driveable(this_vehicle)
			return true
		endif 
	endif 
	
	return false
	
endfunc 

func float magnitude_of_vector(vector &vec)

	return sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z)

endfunc

//proc normalise_vector(vector &vec)
//
//	float inverted_magnitude = (1 / magnitude_of_vector(vec))
//	vec.x *= inverted_magnitude
//	vec.y *= inverted_magnitude
//	vec.z *= inverted_magnitude
//
//endproc
	
proc load_widget_data()
	
	start_widget_group("vehicle force widget")
		
		add_widget_bool("create_vehicle", create_veh)
		add_widget_bool("reset vehicle position", reset_vehicle_position)
		add_widget_bool("update vehicle model", update_vehicle)
		
		widget_model_name = ADD_TEXT_WIDGET("Vehicle Model")
		SET_CONTENTS_OF_TEXT_WIDGET(widget_model_name, "emperor")
		
		ADD_WIDGET_FLOAT_SLIDER("vehicle pos.x", force_vec_a.x, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle pos.y", force_vec_a.y, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle pos.z", force_vec_a.z, -3000.0, 3000.0, 0.1)
		
		ADD_WIDGET_FLOAT_SLIDER("vehicle rot", vehicle_rot, 0, 360, 0.1)
	
		ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.x", force_vec_b.x, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.y", force_vec_b.y, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle target pos.z", force_vec_b.z, -3000.0, 3000.0, 0.1)
		
		ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.x", force_offset.x, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.y", force_offset.y, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vehicle force offset.z", force_offset.z, -3000.0, 3000.0, 0.1)
		
		add_widget_float_slider("force multiplier", force_multiplier, 0.0, 200, 0.1)
		
		add_widget_bool("apply force to vehicle", apply_force_position)
		
		add_widget_bool("Output data to temp_debug.txt", output_data)
		
		add_widget_string("")
		add_widget_string("Data to input into function apply_force_to_car()")
		ADD_WIDGET_FLOAT_SLIDER("vforce.x", force_vec_ba.x, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vforce.y", force_vec_ba.y, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("vforce.z", force_vec_ba.z, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("voffset.x", force_offset.x, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("voffset.y", force_offset.y, -3000.0, 3000.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("voffset.z", force_offset.z, -3000.0, 3000.0, 0.1)
		
	stop_widget_group()

endproc
	
proc vehicle_force_system()

	if reset_vehicle_position
		force_status = 0
		create_veh = true
		update_vehicle = false
		reset_vehicle_position = false
	endif 
	
	if update_vehicle or mission_vehicle_injured(current_car)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(current_car)
		DELETE_VEHICLE(current_car)
		create_veh = true 
		force_status = 0
		update_vehicle = false
	endif 
	
	if output_data
		OPEN_DEBUG_FILE()
		save_string_to_debug_file("vForce = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_vec_ba.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		save_string_to_debug_file("vOffset = ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.x)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.y)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(force_offset.z)
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
			
		output_data = false
	endif 
	
	
	//check script / draw debug lines and sphers 
	DRAW_MARKER(MARKER_CYLINDER, force_vec_b, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.5, 0.5, 0.5>>, 255, 0, 128, 178)
	
	clear_area(force_vec_a, 100, true)
	

	switch force_status 
	
		case 0
			
			if create_veh
			
				vehicle_model = INT_TO_ENUM(model_names, GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(widget_model_name)))
				
				IF NOT IS_MODEL_IN_CDIMAGE(vehicle_model)
					SCRIPT_ASSERT("Vehicle model does not exist!") 
				else 
				
					if not has_model_loaded(vehicle_model)
						request_model(vehicle_model)
					else 
						
						if DOES_ENTITY_EXIST(current_car)
							if is_vehicle_driveable(current_car)
								SET_ENTITY_COORDS(current_car, force_vec_a)
								SET_ENTITY_HEADING(current_car, vehicle_rot)
								force_status++
							endif 
						else 
							if not original_creation
								if not is_ped_injured(player_ped_id())
									force_vec_a = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(player_ped_id(), <<0.0, 4.0, 2.5>>)  
									force_vec_b = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(player_ped_id(), <<-1.0, 4.0, 0.0>>)  
									original_creation = true 
								endif 
							endif 
								
							current_car = create_vehicle(vehicle_model, force_vec_a, vehicle_rot)
							set_model_as_no_longer_needed(vehicle_model)
							SET_ENTITY_PROOFS(current_car, true, true, true, true, true)
							force_status++
						endif 
						
					endif 
					
				endif 
				
			endif 

		break 
		
		case 1

			if apply_force_position
									
				//get_car_coordinates(current_car, force_vec_a.x, force_vec_a.y, force_vec_a.z)
				
				force_vec_ba = force_vec_b - force_vec_a
				
				normalise_vector(force_vec_ba)
				
				force_vec_ba.x *= force_multiplier
				force_vec_ba.y *= force_multiplier
				force_vec_ba.z *= force_multiplier
	
				APPLY_FORCE_TO_ENTITY(current_car, APPLY_TYPE_EXTERNAL_IMPULSE, force_vec_ba, force_offset, 0, false, true, true)  
			
				apply_force_position = false
			
				force_status++
			else 
			
				SET_ENTITY_HEADING(current_car, vehicle_rot)
				SET_ENTITY_COORDS_NO_OFFSET(current_car, force_vec_a)
				
			endif 
											
		break 
		
		case 3
		
		
		break 
		
	endswitch 

endproc 

SCRIPT
	load_widget_data()

	WHILE TRUE
		WAIT (0)
		
		vehicle_force_system()
		
	ENDWHILE
ENDSCRIPT

#endif 
