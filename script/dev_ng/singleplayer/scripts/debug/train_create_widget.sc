//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :                                                 				//
//      AUTHOR          :                                                      			//
//      DESCRIPTION     :         														//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_physics.sch"
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
#ENDIF

VEHICLE_INDEX missionTrain

VECTOR vCreate = <<613, 6438, 31>>
INT iCreate = 0
FLOAT fSpeed = 5.0
BOOL bDirection = TRUE
BOOL bDoCreate = FALSE
BOOL bTerminate = FALSE
BOOL bGrabCam = FALSE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID createTrainWidgets
#ENDIF


// cleanup the mission
PROC MISSION_CLEANUP()
	SET_RANDOM_TRAINS(TRUE)
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(createTrainWidgets)
			DELETE_WIDGET_GROUP(createTrainWidgets)
		ENDIF
	#ENDIF
	TERMINATE_THIS_THREAD() 
ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		createTrainWidgets = START_WIDGET_GROUP("Train Create Debug")
			ADD_WIDGET_VECTOR_SLIDER("Position to create", vCreate, -7000, 7000, 1) 
			ADD_WIDGET_BOOL("Grab cam coords as creation pos", bGrabCam)
			ADD_WIDGET_INT_SLIDER("Train Configuration", iCreate, 0, 24, 1)
			ADD_WIDGET_FLOAT_SLIDER("Speed", fSpeed, 0, 30, 1)
			ADD_WIDGET_BOOL("Direction", bDirection)
			ADD_WIDGET_BOOL("Do Create Train", bDoCreate)
			ADD_WIDGET_BOOL("Terminate Script", bTerminate)
		STOP_WIDGET_GROUP()
	#ENDIF
	
	SET_RANDOM_TRAINS(FALSE)
	DELETE_ALL_TRAINS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<626.68, 6442.31, 30.88>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), -177)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
	
	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTCAR)
	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
	REQUEST_MODEL(TANKERCAR)
	REQUEST_MODEL(METROTRAIN)
	
	WHILE NOT HAS_MODEL_LOADED(FREIGHT)
	OR NOT HAS_MODEL_LOADED(FREIGHTCAR)
	OR NOT HAS_MODEL_LOADED(FREIGHTGRAIN)
	OR NOT HAS_MODEL_LOADED(FREIGHTCONT1)
	OR NOT HAS_MODEL_LOADED(FREIGHTCONT2)
	OR NOT HAS_MODEL_LOADED(TANKERCAR)
	OR NOT HAS_MODEL_LOADED(METROTRAIN)
		WAIT(0)
	ENDWHILE
	
    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
			IF bDoCreate
				IF DOES_ENTITY_EXIST(missionTrain)
					DELETE_MISSION_TRAIN(missionTrain)
				ENDIF
				missionTrain = CREATE_MISSION_TRAIN(iCreate, vCreate, bDirection)
				bDoCreate = FALSE
			ENDIF
			
			IF bGrabCam
				vCreate = GET_FINAL_RENDERED_CAM_COORD()
			
				bGrabCam = FALSE
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(missionTrain)
			AND NOT IS_ENTITY_DEAD(missionTrain)
				SET_TRAIN_SPEED(missionTrain, fSpeed)
				SET_TRAIN_CRUISE_SPEED(missionTrain, fSpeed)
			ENDIF
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				OR bTerminate
					MISSION_CLEANUP()
				ENDIF
			#ENDIF
			IF bTerminate
				MISSION_CLEANUP()
			ENDIF
		ENDIF
    ENDWHILE	
ENDSCRIPT