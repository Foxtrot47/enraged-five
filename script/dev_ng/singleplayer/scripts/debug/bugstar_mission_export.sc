//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	bugstar_mission_export.sc							//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Exports all the mission data for use with the 		//
//							debug mission menu.									//
//																				//
//////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "script_bugstar.sch"

INT iStage
TEXT_LABEL_63 tlDebugText
TEXT_LABEL_15 tlMissionID[1000]
INT iMissionCount
INT iMissionCurrent


#IF NOT USE_TU_CHANGES
TEXT_LABEL_63 tlTextFilePath = "X:/gta5/assets_ng/GameText/dev_ng/American/"
TEXT_LABEL_31 tlTextFileName = "americanDebugBugstar.txt"

TEXT_LABEL_63 tlXMLFilePath = "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/"
TEXT_LABEL_31 tlXMLFileName = "bugstar.xml"
#ENDIF

#IF USE_TU_CHANGES
TEXT_LABEL_63 tlTextFilePath = "X:/gta5/assets_ng/GameText/dev_ng/American/"
TEXT_LABEL_31 tlTextFileName = "americanDebugBugstar.txt"

TEXT_LABEL_63 tlXMLFilePath = "X:/gta5/titleupdate/dev_ng/common/data/script/xml/SPDebug/"
TEXT_LABEL_31 tlXMLFileName = "bugstar.xml"
#ENDIF


PROC DISPLAY_STRING(FLOAT x, FLOAT y, STRING sText, BOOL bCentre, INT r = 255, INT g = 255, INT b = 255, INT a = 255)
	SET_TEXT_CENTRE(bCentre)
	SET_TEXT_SCALE(0.5000, 0.5000)
	SET_TEXT_COLOUR(r, g, b, a)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	DISPLAY_TEXT_WITH_LITERAL_STRING(x, y, "STRING", sText)
ENDPROC

FUNC BOOL CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(INT eachAttribute, STRING sNode)

	INT iNodeHash = GET_HASH_KEY(sNode)
	
	IF GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)) = iNodeHash
	
		// XML
		IF iNodeHash = GET_HASH_KEY("title")
		OR iNodeHash = GET_HASH_KEY("percentage")
			SAVE_STRING_TO_NAMED_DEBUG_FILE(sNode, tlXMLFilePath, tlXMLFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlXMLFilePath, tlXMLFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlXMLFilePath, tlXMLFileName)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", tlXMLFilePath, tlXMLFileName)
		ELSE
			// Add to XML if the value is not 0
			TEXT_LABEL_63 tlXMLString = GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
			INT iXMLInt
			IF STRING_TO_INT(tlXMLString, iXMLInt)
			AND iXMLInt = 0
				// dont bother adding
			ELSE
				SAVE_STRING_TO_NAMED_DEBUG_FILE(sNode, tlXMLFilePath, tlXMLFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlXMLFilePath, tlXMLFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(tlXMLString, tlXMLFilePath, tlXMLFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", tlXMLFilePath, tlXMLFileName)
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

SCRIPT

	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	// START TEXT FILE
	CLEAR_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
	
	// START XML FILE
	CLEAR_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
	SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
	SAVE_STRING_TO_NAMED_DEBUG_FILE("<DebugMenuItems>", tlXMLFilePath, tlXMLFileName)
	
	
	WHILE TRUE
	
		WAIT(0)
		
		// Export data to assets/GameText/American/americanDebugBugstar.txt
		//  - mission title
		//  - mission description
		//  - mission comments
		//  - story description
		// Export data to build/dev/common/data/script/xml/SPDebug/bugstar.xml
		// - 
		SWITCH iStage
			// https://rsgedibgs8.rockstar.t2.corp:8443/BugstarRestService-1.0/rest/Projects/1546/Missions/
			CASE 0
			
				IF g_bugstarQuery.bQueriesDisabled
				OR NOT g_debugMenuControl.bUseBugstarQuery
					tlDebugText = "Bugstar queries have been disabled..."
				ELSE
					tlDebugText = "Grabbing list of missions for export..."
				ENDIF
				
				DISPLAY_STRING(0.5, 0.5, tlDebugText, TRUE, 255, 255, 255, 255)
				
				IF NOT HAS_BUGSTAR_QUERY_STARTED()
					IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
						
						PRINTLN("MISSION DATA EXPORT - Grabbing mission list")
						
						// Start a new query
						TEXT_LABEL_63 tlQuery
						tlQuery = "Missions/"
						
						START_BUGSTAR_QUERY(tlQuery)
						SET_BUGSTAR_QUERY_STARTED()
						
						// Reset some data
						iMissionCurrent = 0
						iMissionCount = 0
					ELSE
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							iStage = 99
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BUGSTAR_QUERY_PENDING()
						IF (IS_BUGSTAR_QUERY_SUCCESSFUL())
							IF(CREATE_XML_FROM_BUGSTAR_QUERY())
								
								// Integers used for the FOR loops going through all the nodes and attributes
								INT eachNode
								TEXT_LABEL_63 tlNodeString
								INT iNodeCount
								
								// Loop through all the nodes
								iNodeCount = GET_NUMBER_OF_XML_NODES()
								FOR eachNode = 0 TO (iNodeCount-1)
								
									IF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("missionId")
										tlNodeString = GET_STRING_FROM_XML_NODE()
										IF NOT IS_STRING_NULL_OR_EMPTY(tlNodeString)
											tlMissionID[iMissionCount] = tlNodeString
											iMissionCount++
										ENDIF
									ENDIF
									
									// Tell the script to goto the next node in the xml file
									GET_NEXT_XML_NODE()
								ENDFOR
								
								DELETE_XML_FILE()
							ENDIF
						ENDIF
						
						// Query finished pending and we will have processed a successful query so end
						END_BUGSTAR_QUERY()
						SET_BUGSTAR_QUERY_FINISHED()
						
						IF iMissionCount = 0
							SCRIPT_ASSERT("Failed to obtain mission list")
							iStage = 99
						ELSE
							PRINTLN("MISSION DATA EXPORT - ", iMissionCount, " missions found")
							iStage++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// https://rsgedibgs8.rockstar.t2.corp:8443/BugstarRestService-1.0/rest/Projects/1546/Missions/Summary/missionID?filter=detailed
			CASE 1
			
				tlDebugText = "Processing "
				tlDebugText += tlMissionID[iMissionCurrent]
				tlDebugText += " for mission export ("
				tlDebugText += iMissionCurrent
				tlDebugText += "/"
				tlDebugText += iMissionCount
				tlDebugText += ")"
				DISPLAY_STRING(0.5, 0.5, tlDebugText, TRUE, 255, 255, 255, 255)
				
				tlDebugText = "["
				INT i
				FLOAT fPerc
				fPerc = ((TO_FLOAT(iMissionCurrent)/TO_FLOAT(iMissionCount)) * 100.0)
				REPEAT 50 i
					IF fPerc > ((TO_FLOAT(i)/TO_FLOAT(50)*100.0))
						tlDebugText += "|"
					ELSE
						tlDebugText += "-"
					ENDIF
				ENDREPEAT
				tlDebugText += "]"
								DISPLAY_STRING(0.5, 0.55, tlDebugText, TRUE, 255, 255, 255, 255)
				
				IF NOT HAS_BUGSTAR_QUERY_STARTED()
					IF IS_BUGSTAR_QUERY_SAFE_TO_START(FALSE)
					
						PRINTLN("MISSION DATA EXPORT - Grabbing mission data for ", tlMissionID[iMissionCurrent])
						
						// Start a new query
						TEXT_LABEL_63 tlQuery
						tlQuery = "Missions/Summary/"
						tlQuery += tlMissionID[iMissionCurrent]
						tlQuery += "?filter=detailed"
						START_BUGSTAR_QUERY(tlQuery)
						SET_BUGSTAR_QUERY_STARTED()
					ENDIF
				ELSE
					IF NOT IS_BUGSTAR_QUERY_PENDING()
						IF (IS_BUGSTAR_QUERY_SUCCESSFUL())
							IF(CREATE_XML_FROM_BUGSTAR_QUERY())
							
								// Integers used for the FOR loops going through all the nodes and attributes
								INT eachNode, eachAttribute
								INT iNodeCount, iAttributeCount, iPass
								
								BOOL bBugstarItemAdded
								bBugstarItemAdded = FALSE
								
								//REPEAT 2 iPass // Do 2 passes to make sure we get the bugstarItem node first
								REPEAT 1 iPass // Do 1 pass and ignore the team bugd and top 5 bug owners
								
									// Loop through all the nodes
									iNodeCount = GET_NUMBER_OF_XML_NODES()
									FOR eachNode = 0 TO (iNodeCount-1)
										IF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("bugstarItem") AND iPass = 0
										
											bBugstarItemAdded = TRUE
										
											// START XML MISSION
											SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
											SAVE_STRING_TO_NAMED_DEBUG_FILE("  <bugstarItem missionID=\"", tlXMLFilePath, tlXMLFileName)
											SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionID[iMissionCurrent], tlXMLFilePath, tlXMLFileName)
											SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", tlXMLFilePath, tlXMLFileName)
										
											eachAttribute = 0
											
											// Loop through all the attributes
											iAttributeCount = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
											FOR eachAttribute = 0 TO (iAttributeCount-1)
											
												IF GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)) = GET_HASH_KEY("missionName")
													// TEXT
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("[", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionID[iMissionCurrent], tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("]", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													
													// XML
													SAVE_STRING_TO_NAMED_DEBUG_FILE("title=\"", tlXMLFilePath, tlXMLFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlXMLFilePath, tlXMLFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("\" ", tlXMLFilePath, tlXMLFileName)
												
												ELIF GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)) = GET_HASH_KEY("comments")
													// TEXT
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("[", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionID[iMissionCurrent], tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("_MC", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("]", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
												
												ELIF GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)) = GET_HASH_KEY("story")
													// TEXT
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("[", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionID[iMissionCurrent], tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("_SD", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("]", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
												
												ELIF GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute)) = GET_HASH_KEY("description")
													// TEXT
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("[", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(tlMissionID[iMissionCurrent], tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("_MD", tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE("]", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), tlTextFilePath, tlTextFileName)
													SAVE_STRING_TO_NAMED_DEBUG_FILE(" ", tlTextFilePath, tlTextFileName)
													SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
													
												// XML ONLY
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "bugstarMissionId")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "file")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "author")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "percentage")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "highlight")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "wish")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "todos")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "tasks")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "dBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "cBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "bBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "aBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "overdueBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "oldBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "avgBugAge")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "fixedBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "addedBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "leslieTodos")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "lesBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "teamInfo")
												ENDIF
											ENDFOR
											
											SAVE_STRING_TO_NAMED_DEBUG_FILE(">", tlXMLFilePath, tlXMLFileName)
										
										ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("teamInfo") AND iPass = 1 AND bBugstarItemAdded
											// START XML MISSION
											SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
											SAVE_STRING_TO_NAMED_DEBUG_FILE("    <teamInfo ", tlXMLFilePath, tlXMLFileName)
											
											eachAttribute = 0
											
											// Loop through all the attributes
											iAttributeCount = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
											FOR eachAttribute = 0 TO (iAttributeCount-1)
												// XML ONLY
												IF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "owner")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "noBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "id")
												ENDIF
											ENDFOR
											
											SAVE_STRING_TO_NAMED_DEBUG_FILE("/>", tlXMLFilePath, tlXMLFileName)
											
										ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("topBugOwners") AND iPass = 1 AND bBugstarItemAdded
											// START XML MISSION
											SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
											SAVE_STRING_TO_NAMED_DEBUG_FILE("    <topBugOwners ", tlXMLFilePath, tlXMLFileName)
											
											eachAttribute = 0
											
											// Loop through all the attributes
											iAttributeCount = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
											FOR eachAttribute = 0 TO (iAttributeCount-1)
												// XML ONLY
												IF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "owner")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "noBugs")
												ELIF CHECK_BUGSTAR_NODE_AND_ADD_TO_XML_FILE(eachAttribute, "avgBugAge")
												ENDIF
											ENDFOR
											
											SAVE_STRING_TO_NAMED_DEBUG_FILE("/>", tlXMLFilePath, tlXMLFileName)
										ENDIF
										
										// Tell the script to goto the next node in the xml file
										GET_NEXT_XML_NODE()
									ENDFOR
								ENDREPEAT
								
								IF bBugstarItemAdded
									SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
									SAVE_STRING_TO_NAMED_DEBUG_FILE("  </bugstarItem>", tlXMLFilePath, tlXMLFileName)
								ENDIF
								
								DELETE_XML_FILE()
							ENDIF
						ENDIF
						
						// Query finished pending and we will have processed a successful query so end
						END_BUGSTAR_QUERY()
						SET_BUGSTAR_QUERY_FINISHED()
						
						iMissionCurrent++
						IF iMissionCurrent >= iMissionCount
						OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
							iStage = 99
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 99
				// END TEXT FILE
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("[DUMMY_BUGSTAR]", tlTextFilePath, tlTextFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("THIS LABEL NEEDS TO BE HERE !!!", tlTextFilePath, tlTextFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlTextFilePath, tlTextFileName)
				
				// END XML FILE
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("</DebugMenuItems>", tlXMLFilePath, tlXMLFileName)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlXMLFilePath, tlXMLFileName)
				
				PRINTLN("MISSION DATA EXPORT - Finished processing mission data")
				TERMINATE_THIS_THREAD()
			BREAK
		ENDSWITCH
		
	ENDWHILE
	
ENDSCRIPT

#ENDIF
