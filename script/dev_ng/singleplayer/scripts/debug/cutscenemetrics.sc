



USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_debug.sch"
USING "commands_cutscene.sch"


#IF IS_DEBUG_BUILD

ENUM CUT_LOAD_AND_PLAY_RESULT //not a boolean for future cases and clarity
	LOAD_AND_PLAY_GOOD,
	LOAD_TIMEOUT
ENDENUM

INT iClosedZones = 0
INT iFiles = 0
INT iFNth = 9

PROC OPEN_METRIC_ZONE(STRING cut)
	PRINTLN("<CUTMETRICS> Starting metric zone for: ",cut)
	TEXT_LABEL_63 tl = "zone_" 
	tl += cut
	PRINTLN("<CUTMETRICS> METRICS_ZONE_START: ",tl)
	METRICS_ZONE_START(tl)
	WAIT(100)
ENDPROC

PROC CLOSE_METRIC_ZONE(STRING cut, BOOL bFinal)
	WAIT(100)
	METRICS_ZONE_STOP()
	PRINTLN("<CUTMETRICS> Ended metric zone for: ",cut)

	++iClosedZones
	IF iClosedZones > iFNth OR bFinal
		iClosedZones = 0
		//offload
		TEXT_LABEL tl = "cuts"
		tl += iFiles
		
		METRICS_ZONE_SAVE_TELEMETRY()
		PRINTLN("<CUTMETRICS> Saved telemetry")
		METRICS_ZONE_SAVE_TO_FILE(tl)
		PRINTLN("<CUTMETRICS> Saved cut metric set to ",tl)
		iFiles++
		METRICS_ZONES_CLEAR()
		WAIT(0)
		
	ENDIF
ENDPROC

FUNC CUT_LOAD_AND_PLAY_RESULT LOAD_AND_PLAY_CUTSCENE(STRING cut, bool bFinal)
		PRINTLN("<CUTMETRICS> Requesting: ",cut)	
		REQUEST_CUTSCENE(cut)
		WAIT(50)
		

		INT timer = GET_GAME_TIMER() + 120000 //time plus 2 minutes
		
		WHILE NOT HAS_CUTSCENE_LOADED() //should wait for area to stream in also
			IF GET_GAME_TIMER() > timer
			 	//2 minute timeout
				REMOVE_CUTSCENE()
				WAIT(1000)
				RETURN LOAD_TIMEOUT
			ENDIF
			WAIT(10)
		ENDWHILE
		PRINTLN("<CUTMETRICS> Load complete...")
		
		
		OPEN_METRIC_ZONE(cut)
		START_CUTSCENE()
		WAIT(1000)
		WHILE IS_CUTSCENE_PLAYING()
			PRINTLN("<CUTMETRICS> Playing...")
			WAIT(2000)
		ENDWHILE
		CLOSE_METRIC_ZONE(cut,bFinal)
		
		
		PRINTLN("<CUTMETRICS> Play complete: ",cut)	
		
		WAIT(50)
		REMOVE_CUTSCENE()
		PRINTLN("<CUTMETRICS> Remove called on: ",cut)	
		WAIT(50)
		
		
		RETURN LOAD_AND_PLAY_GOOD
		
ENDFUNC


#ENDIF

SCRIPT 
	#IF IS_DEBUG_BUILD
	iClosedZones = 0
	iFiles = 0
	INT iTotalCutScenes = GET_LENGTH_OF_CUTSCENE_LIST()-1//GET_TOTAL_SCENE_INDICES()//get total from code
	INT iCurrentCut = 0
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	IF GET_COMMANDLINE_PARAM_EXISTS("cutmetricsStart")
		INT iStartat =  GET_COMMANDLINE_PARAM_INT("cutmetricsStart")
		IF iStartat > -1
		IF iStartat < iTotalCutScenes
		
			PRINTLN("<CUTMETRICS> Cutscene run startat set to: ", iStartat, " via cutmetricsStart command line arg")
			iCurrentCut = iStartat
		ENDIF
		ENDIF
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("cutmetricsEnd")
		INT iEndat =  GET_COMMANDLINE_PARAM_INT("cutmetricsEnd")
		IF iEndat > -1
		IF iEndat < iTotalCutScenes 
			PRINTLN("<CUTMETRICS> Cutscene run end value set to: ", iEndat, " via cutmetricsEnd command line arg")
			iTotalCutScenes = iEndat
		ENDIF
		ENDIF
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("cutmetricsSaveNth")
		iFNth =  GET_COMMANDLINE_PARAM_INT("cutmetricsSaveNth")-1
		IF iFNth < 0
			iFNth = 10
		ENDIF
	ENDIF
	
	
	
	
	IF NOT (iCurrentCut < iTotalCutScenes)
	
		PRINTLN("<CUTMETRICS> Spcified cutscene metric range is invalid, terminating ( Current = ", iCurrentCut , ", Total = " , iTotalCutScenes ," ) " )
	
		TERMINATE_THIS_THREAD()
	
	ENDIF
	
	
	STRING currentScene = GET_CUTSCENE_NAME_FROM_LIST(iCurrentCut)
	PRINTLN("<CUTMETRICS> Starting with cutscene: ", currentScene)
	
	BOOL done = FALSE
	
	WHILE NOT done
		
		BOOL bFinal = FALSE
		
		IF iCurrentCut = iTotalCutScenes
			bFinal = TRUE
		ENDIF
		
	
		SWITCH LOAD_AND_PLAY_CUTSCENE(currentScene, bFinal)
			CASE LOAD_AND_PLAY_GOOD
				PRINTLN("<CUTMETRICS> ", currentScene, " loaded and played correctly")
				BREAK
			CASE LOAD_TIMEOUT
				PRINTLN("<CUTMETRICS> ", currentScene, " triggered 2 minute time out during load phase")
				BREAK
		ENDSWITCH
		
		
	
		IF  iCurrentCut = iTotalCutScenes
			done = TRUE
		ELSE 
			//move to next cutscene
			iCurrentCut++
			currentScene = GET_CUTSCENE_NAME_FROM_LIST(iCurrentCut)
			PRINTLN("<CUTMETRICS> Moving on to: ", currentScene, " ")
			WAIT(1000)
		ENDIF
	
	
		WAIT(0)
	ENDWHILE
	
	
	PRINTLN("<CUTMETRICS> Complete ")
	
	#ENDIF
ENDSCRIPT
































