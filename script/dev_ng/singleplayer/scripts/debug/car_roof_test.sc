//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Matthew Booton					Date: 04/03/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						     Car Roof Test										│
//│																				│
//│			Warps the player to the Pershing Square test area and creates		│
//│ 		a car with a folding roof											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"

USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"

USING "shared_debug.sch"
USING "script_debug.sch"

VECTOR v_car_pos = <<188.6595, -948.7460, 29.0921>>

VEHICLE_INDEX veh_car
MODEL_NAMES model_car = NINEF2
STRING str_anim_dict = "VA_NINEF2"
STRING str_anim_name = "NINEF2_ROOF"

PROC MISSION_SETUP()	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	CLEAR_AREA(<<0.0, 0.0, 0.0>>, 5000.0, TRUE)
ENDPROC

PROC MISSION_CLEANUP()
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	REMOVE_ANIM_DICT(str_anim_dict)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)
	
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF

	MISSION_SETUP()

	WHILE (TRUE)
		WAIT(0)
		
		IF NOT DOES_ENTITY_EXIST(veh_car)
			REQUEST_MODEL(model_car)
		
			IF HAS_MODEL_LOADED(model_car)
				veh_car = CREATE_VEHICLE(model_car, v_car_pos, 153.1829)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
				
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
			ENDIF
		ELSE
			REQUEST_ANIM_DICT(str_anim_dict)
			
			IF HAS_ANIM_DICT_LOADED(str_anim_dict)
				IF IS_VEHICLE_DRIVEABLE(veh_car)
					IF IS_BUTTON_JUST_PRESSED(PAD1, CROSS)
						TASK_VEHICLE_PLAY_ANIM(veh_car, str_anim_dict, str_anim_name)
					ENDIF
					
					DISPLAY_TEXT_WITH_LITERAL_STRING(0.1, 0.1, "STRING", "Press X to play roof animation")
				ENDIF
			ENDIF
		ENDIF
			
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_CLEANUP()
		ENDIF
	ENDWHILE
	
	MISSION_CLEANUP()
ENDSCRIPT		


#ENDIF	// IS_DEBUG_BUILD