//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :                                                 				//
//      AUTHOR          :                                                      			//
//      DESCRIPTION     :         														//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

// including the most common script headers
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "commands_physics.sch"
#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
#ENDIF

BOOL bSetOverallVehicleDensity = TRUE
BOOL bSetOverallPedDensity = TRUE

FLOAT fVehicleDensity = 1.0
FLOAT fRandomVehicleDensity = 1.0
FLOAT fParkedVehicleDensity = 1.0
FLOAT fAmbientVehicleRangeMultiplier = 1.0
FLOAT fPedDensity = 1.0
FLOAT fScenarioPedDensityInt = 1.0
FLOAT fScenarioPedDensityExt = 1.0

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID densityWidgets
#ENDIF

// cleanup the mission
PROC MISSION_CLEANUP()
	SET_RANDOM_TRAINS(TRUE)
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(densityWidgets)
			DELETE_WIDGET_GROUP(densityWidgets)
		ENDIF
	#ENDIF
	TERMINATE_THIS_THREAD() 
ENDPROC

// main script
SCRIPT
	SET_MISSION_FLAG(TRUE)
	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		densityWidgets = START_WIDGET_GROUP("Population Density")
		
		ADD_WIDGET_BOOL("Set Overall Vehicle Density", bSetOverallVehicleDensity)
		ADD_WIDGET_FLOAT_SLIDER("Overall Vehicle Density", fVehicleDensity, 0, 1, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Random Vehicle Density", fRandomVehicleDensity, 0, 1, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Parked Vehicle Density", fParkedVehicleDensity, 0, 1, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Ambient Vehicle Range Mult", fAmbientVehicleRangeMultiplier, 0, 4, 0.1)
		ADD_WIDGET_BOOL("Set Overall Ped Density", bSetOverallPedDensity)
		ADD_WIDGET_FLOAT_SLIDER("Overall Ped Density", fPedDensity, 0, 1, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Scenario Ped Interior Density", fScenarioPedDensityInt, 0, 1, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Scenario Ped Exterior Density", fScenarioPedDensityExt, 0, 1, 0.1)
		STOP_WIDGET_GROUP()
	#ENDIF
	

    WHILE TRUE
		// main loop	
       	WAIT(0)
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
			IF bSetOverallVehicleDensity
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fVehicleDensity)
			ENDIF
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fRandomVehicleDensity)
			SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fParkedVehicleDensity)
			SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(fAmbientVehicleRangeMultiplier)
			IF bSetOverallPedDensity
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(fPedDensity)
			ENDIF
			SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(fScenarioPedDensityInt, fScenarioPedDensityExt)

			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
					MISSION_CLEANUP()
				ENDIF
			#ENDIF
		ENDIF
    ENDWHILE	
ENDSCRIPT