/*
	Author: 		Orlando Cazalet-Hyams
	Team: 			Online Technical
	Description:	Script specifically used to test error_listener.sc - launch this cause a fatal script error.
					Change the type of error with the args
*/

#IF IS_DEBUG_BUILD
USING "error_listener_public.sch"

FUNC INT ARRAY_OVERRRUN()
	PRINTLN("[ERROR_THROWER] ARRAY_OVERRRUN")
	INT iArray[1]
	INT iFive = 5
	RETURN iArray[iFive]
ENDFUNC

PROC INSTRUCTION_LIMIT()
	PRINTLN("[ERROR_THROWER] INSTRUCTION_LIMIT")
	WHILE TRUE
	ENDWHILE
ENDPROC

FUNC INT STACK_OVERFLOW()
	PRINTLN("[ERROR_THROWER] STACK_OVERFLOW")
	INT iArray[DEFAULT_STACK_SIZE * 3]
	RETURN iArray[0]
ENDFUNC
	
SCRIPT (ERROR_THROWER_ARGS args)
	PRINTLN("[ERROR_THROWER] ThreadID       : ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
	PRINTLN("[ERROR_THROWER] ScriptNameHash : ", HASH("error_thrower"))
	PRINTLN("[ERROR_THROWER] WAIT(0)")
	
	// We need to wait one frame otherwise the script name comes up as NULL - error_listner.sc needs a chance to grab this script name.
	WAIT(0)
	
	PRINTLN("[ERROR_THROWER] ThreadID       : ", NATIVE_TO_INT(GET_ID_OF_THIS_THREAD()))
	PRINTLN("[ERROR_THROWER] ScriptNameHash : ", HASH("error_thrower"))
	PRINTLN("[ERROR_THROWER] THROWING ERROR...")
	
	SWITCH args.eError
		CASE ETE_ARRAY_OVERRUN	ARRAY_OVERRRUN()	BREAK
		CASE ETE_INS_LIMIT		INSTRUCTION_LIMIT()	BREAK
		CASE ETE_STACK_OVERFLOW	STACK_OVERFLOW()	BREAK
	ENDSWITCH
	
	PRINTLN("[ERROR_THROWER] NO ERROR THROWN")
ENDSCRIPT
#ENDIF

#IF NOT IS_DEBUG_BUILD
SCRIPT
ENDSCRIPT
#ENDIF
