//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	celebration_editor.sc								//
//		AUTHOR			:	William Kennedy										//
//		DESCRIPTION		:	Allows editing of MP celebrations			 		//
//																				//
//////////////////////////////////////////////////////////////////////////////////

// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"
USING "ped_component_public.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"
USING "script_bugstar.sch"
USING "shared_debug.sch"
USING "net_include.sch"
USING "net_wait_zero.sch"

// Variables
CONST_INT MAX_NUM_CELEBRATION_PEDS	15
CONST_INT MAX_NUM_CELEBRATION_VEHICLES 4

CONST_INT CELEBRATION_VEHICLE_MONSTER 0
CONST_INT CELEBRATION_VEHICLE_ZENTORNO 1
CONST_INT CELEBRATION_VEHICLE_MOTORBIKE 2
CONST_INT CELEBRATION_VEHICLE_CYCLE 3

BOOL bPlayPairedAnim
VECTOR vPairedAnimWorldPosition
FLOAT fPairedAnimHeading

STRUCT STRUCT_PLAYER_NAME_DATA
	
	VECTOR vPos
	VECTOR vRotation
	VECTOR vScale
	VECTOR vWorldSize
	
ENDSTRUCT

STRUCT STRUCT_CELEBRATION_PED
	
	PED_INDEX pedId
	VECTOR vWorldPosition
	FLOAT fRotation
	MODEL_NAMES eModel
	BOOL bActive
	BOOL bPlayAnim
	INT iPlayAnimStage
	INT iAnimSceneId
	FLOAT fAnimLength
	SCRIPT_TIMER stTimer
	VECTOR vPairedAnimOffset
	STRUCT_PLAYER_NAME_DATA sPlayerNameData
	
ENDSTRUCT

STRUCT STRUCT_CELEBRATION_VEHICLE
	
	VEHICLE_INDEX vehicleId
	VECTOR vWorldPosition
	FLOAT fRotation
	MODEL_NAMES eModel
	BOOL bActive
	
ENDSTRUCT

STRUCT STRUCT_CELEBRATION_CAMERA
	
	CAMERA_INDEX camId
	VECTOR vWorldPosition
	VECTOR vWorldRotation
	FLOAT fFov
	BOOL bActive
	
ENDSTRUCT

STRUCT_CELEBRATION_PED sPed[MAX_NUM_CELEBRATION_PEDS]
STRUCT_CELEBRATION_VEHICLE sVehicle[MAX_NUM_CELEBRATION_VEHICLES]
STRUCT_CELEBRATION_CAMERA sCamera

BOOL bTerminateEditor
BOOL bEditingEnabled = TRUE
INT iEditingStage
BOOL bPrintData
INTERIOR_INSTANCE_INDEX interiorId
SCALEFORM_INDEX playerNameMovies[MAX_NUM_CELEBRATION_PEDS]
INT iDrawNamesStage
BOOL bEditNAmes

// Functions
PROC ADD_WIDGETS()
	
	INT i
	TEXT_LABEL_63 tl63Temp
	
	START_WIDGET_GROUP("Celebration Editor")
		
		ADD_WIDGET_BOOL("Terminate Editor", bTerminateEditor)
		ADD_WIDGET_BOOL("Enable Editing", bEditingEnabled)
		ADD_WIDGET_BOOL("Print Scene Data", bPrintData)
		ADD_WIDGET_BOOL("Edit Names", bEditNAmes)
		
		START_WIDGET_GROUP("Create Entities")
		
			REPEAT MAX_NUM_CELEBRATION_PEDS i
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Active"
				ADD_WIDGET_BOOL(tl63Temp, sPed[i].bActive)
			ENDREPEAT
			
			REPEAT MAX_NUM_CELEBRATION_VEHICLES i
				SWITCH i
					CASE CELEBRATION_VEHICLE_MONSTER	tl63Temp = "Monster Active" 	BREAK
					CASE CELEBRATION_VEHICLE_ZENTORNO 	tl63Temp = "Zentorno Active" 	BREAK
					CASE CELEBRATION_VEHICLE_MOTORBIKE	tl63Temp = "Motorbike Active" 	BREAK
					CASE CELEBRATION_VEHICLE_CYCLE 		tl63Temp = "Cycle Active" 		BREAK
				ENDSWITCH
				ADD_WIDGET_BOOL(tl63Temp, sVehicle[i].bActive)
			ENDREPEAT
			
			tl63Temp = ""
			tl63Temp = "Camera Active"
			ADD_WIDGET_BOOL(tl63Temp, sCamera.bActive)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Entity Properties")
			
			tl63Temp = "Play Paired Anims On Ped 0 and 1"
			ADD_WIDGET_BOOL(tl63Temp, bPlayPairedAnim)
			ADD_WIDGET_VECTOR_SLIDER("Paired Anim Pos", vPairedAnimWorldPosition, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Paired Anim Rot", fPairedAnimHeading, -10000.0, 10000.0, 0.01)
			
			REPEAT MAX_NUM_CELEBRATION_PEDS i
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Position"
				ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sPed[i].vWorldPosition, -10000.0, 10000.0, 0.01)
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Heading"
				ADD_WIDGET_FLOAT_SLIDER(tl63Temp, sPed[i].fRotation, -10000.0, 10000.0, 0.1)
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Play Celebration"
				ADD_WIDGET_BOOL(tl63Temp, sPed[i].bPlayAnim)
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Paired Offset"
				ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sPed[i].vPairedAnimOffset, -10000.0, 10000.0, 0.01)
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Name Scale"
				ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sPed[i].sPlayerNameData.vScale, -10000.0, 10000.0, 0.01)
				tl63Temp = "Ped "
				tl63Temp += i
				tl63Temp += " Name World Scale"
				ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sPed[i].sPlayerNameData.vWorldSize, -10000.0, 10000.0, 0.01)
			ENDREPEAT
			
			REPEAT MAX_NUM_CELEBRATION_VEHICLES i
				SWITCH i
					CASE CELEBRATION_VEHICLE_MONSTER	tl63Temp = "Monster Position" 	BREAK
					CASE CELEBRATION_VEHICLE_ZENTORNO 	tl63Temp = "ZENTORNO Position" 	BREAK
					CASE CELEBRATION_VEHICLE_MOTORBIKE	tl63Temp = "Motorbike Position" BREAK
					CASE CELEBRATION_VEHICLE_CYCLE 		tl63Temp = "Cycle Position" 	BREAK
				ENDSWITCH
				ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sVehicle[i].vWorldPosition, -10000.0, 10000.0, 0.01)
				SWITCH i
					CASE CELEBRATION_VEHICLE_MONSTER	tl63Temp = "Monster Rotation" 	BREAK
					CASE CELEBRATION_VEHICLE_ZENTORNO 	tl63Temp = "ZENTORNO Rotation" 	BREAK
					CASE CELEBRATION_VEHICLE_MOTORBIKE	tl63Temp = "Motorbike Rotation" BREAK
					CASE CELEBRATION_VEHICLE_CYCLE 		tl63Temp = "Cycle Rotation" 	BREAK
				ENDSWITCH
				ADD_WIDGET_FLOAT_SLIDER(tl63Temp, sVehicle[i].fRotation, -10000.0, 10000.0, 0.01)
			ENDREPEAT
			
			tl63Temp = "Camera "
			tl63Temp += i
			tl63Temp += " Position"
			ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sCamera.vWorldPosition, -10000.0, 10000.0, 0.01)
			tl63Temp = "Camera "
			tl63Temp += i
			tl63Temp += " Rotation"
			ADD_WIDGET_VECTOR_SLIDER(tl63Temp, sCamera.vWorldRotation, -10000.0, 10000.0, 0.01)
			tl63Temp = "Camera "
			tl63Temp += i
			tl63Temp += " FOV"
			ADD_WIDGET_FLOAT_SLIDER(tl63Temp, sCamera.fFov, -10000.0, 10000.0, 0.01)
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC INITIALISE_VEHICLE_DATA(INT iVehicle, BOOL bForceInitialiseAsInactive = FALSE)
	
	SWITCH iVehicle
		CASE CELEBRATION_VEHICLE_MONSTER
			sVehicle[iVehicle].vWorldPosition = <<413.179, -974.422, -100.004>>
			sVehicle[iVehicle].fRotation = 158.368
			sVehicle[iVehicle].eModel = MONSTER
			IF bForceInitialiseAsInactive
				sVehicle[iVehicle].bActive = FALSE
			ENDIF
		BREAK
		CASE CELEBRATION_VEHICLE_ZENTORNO
			sVehicle[iVehicle].vWorldPosition = <<413.8590, -974.6100, -100.0042>>
			sVehicle[iVehicle].fRotation = 149.8085
			sVehicle[iVehicle].eModel = ZENTORNO
			IF bForceInitialiseAsInactive
				sVehicle[iVehicle].bActive = FALSE
			ENDIF
		BREAK
		CASE CELEBRATION_VEHICLE_MOTORBIKE
			sVehicle[iVehicle].vWorldPosition = <<414.7016, -975.6291, -100.0042>>
			sVehicle[iVehicle].fRotation = 237.3050
			sVehicle[iVehicle].eModel = AKUMA
			IF bForceInitialiseAsInactive
				sVehicle[iVehicle].bActive = FALSE
			ENDIF
		BREAK
		CASE CELEBRATION_VEHICLE_CYCLE
			sVehicle[iVehicle].vWorldPosition = <<415.1816, -976.0641, -100.0042>>
			sVehicle[iVehicle].fRotation = 139.7340
			sVehicle[iVehicle].eModel = FIXTER
			IF bForceInitialiseAsInactive
				sVehicle[iVehicle].bActive = FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC INITIALISE_PED_DATA(INT iPed, BOOL bForceInitialiseAsInactive = FALSE)
	
	SWITCH iPed
		
		CASE 0
			sPed[0].vWorldPosition = <<414.4000, -977.6000, -99.0>>
			sPed[0].fRotation = 200.0000
			sPed[0].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[0].eModel = A_F_Y_HIPSTER_01
			sPed[0].bActive = TRUE
			sPed[0].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[0].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
			IF bForceInitialiseAsInactive
				sPed[0].bActive = FALSE
			ENDIF
		BREAK
		
		CASE 1
			sPed[1].vWorldPosition = <<413.0202, -976.6559, -99.0>>
			sPed[1].fRotation = 225.3750
			sPed[1].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[1].eModel = A_M_Y_VINEWOOD_01
			sPed[1].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[1].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 2
			sPed[2].vWorldPosition = <<414.9879, -975.4411, -99.0>>
			sPed[2].fRotation = 183.6250
			sPed[2].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[2].eModel = A_F_Y_HIPSTER_01
			sPed[2].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[2].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 3
			sPed[3].vWorldPosition = <<416.2123, -975.5722, -99.0>>
			sPed[3].fRotation = 155.3000
			sPed[3].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[3].eModel = A_M_Y_VINEWOOD_01
			sPed[3].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[3].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 4
			sPed[4].vWorldPosition = <<410.7824, -976.5933, -99.0>>
			sPed[4].fRotation = 240.9000
			sPed[4].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[4].eModel = A_F_Y_HIPSTER_01
			sPed[4].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[4].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 5
			sPed[5].vWorldPosition = <<411.1806, -975.8190, -99.0>>
			sPed[5].fRotation = 227.5000
			sPed[5].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[5].eModel = A_M_Y_VINEWOOD_01
			sPed[5].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[5].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 6
			sPed[6].vWorldPosition = <<414.0178, -974.6259, -99.0>>
			sPed[6].fRotation = 206.6500
			sPed[6].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[6].eModel = A_F_Y_HIPSTER_01
			sPed[6].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[6].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 7
			sPed[7].vWorldPosition = <<415.7338, -972.9619, -99.0>>
			sPed[7].fRotation = 170.7750
			sPed[7].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[7].eModel = A_M_Y_VINEWOOD_01
			sPed[7].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[7].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 8
			sPed[8].vWorldPosition = <<412.1275, -974.0275, -99.0>>
			sPed[8].fRotation = 200.0000
			sPed[8].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[8].eModel = A_F_Y_HIPSTER_01
			sPed[8].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[8].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 9
			sPed[9].vWorldPosition = <<417.3252, -970.7984, -99.0>>
			sPed[9].fRotation = 160.0250
			sPed[9].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[9].eModel = A_M_Y_VINEWOOD_01
			sPed[9].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[9].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 10
			sPed[10].vWorldPosition = <<409.8050, -975.0361, -99.0>>
			sPed[10].fRotation = 240.6500
			sPed[10].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[10].eModel = A_F_Y_HIPSTER_01
			sPed[10].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[10].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 11
			sPed[11].vWorldPosition = <<410.6598, -974.3722, -99.0>>
			sPed[11].fRotation = 222.1500
			sPed[11].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[11].eModel = A_M_Y_VINEWOOD_01
			sPed[11].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[11].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 12
			sPed[12].vWorldPosition = <<407.5374, -974.3933, -99.0>>
			sPed[12].fRotation = 258.1750
			sPed[12].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[12].eModel = A_F_Y_HIPSTER_01
			sPed[12].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[12].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 13
			sPed[13].vWorldPosition = <<413.1881, -969.0190, -99.0>>
			sPed[13].fRotation = 204.0250
			sPed[13].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[13].eModel = A_M_Y_VINEWOOD_01
			sPed[13].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[13].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
		CASE 14
			sPed[14].vWorldPosition = <<415.0103, -969.1959, -99.0>>
			sPed[14].fRotation = 182.3500
			sPed[14].vPairedAnimOffset = << 0.0, 0.0, 90.0 >>
			sPed[14].eModel = A_F_Y_HIPSTER_01
			sPed[14].sPlayerNameData.vScale = << 0.75, 0.5, 0.375 >>
			sPed[14].sPlayerNameData.vWorldSize = << 0.75, 0.5, 0.375 >>
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC INITIALISE_ALL_PEDS_DATA()
	
	INT i
	
	REPEAT MAX_NUM_CELEBRATION_PEDS i
		INITIALISE_PED_DATA(i)
	ENDREPEAT
	
ENDPROC

PROC INITIALISE_ALL_VEHICLES_DATA()
	
	INT i
	
	REPEAT MAX_NUM_CELEBRATION_VEHICLES i
		INITIALISE_VEHICLE_DATA(i)
	ENDREPEAT
	
ENDPROC

PROC INITIALISE_CAMERA_DATA(BOOL bResetAsInactive = FALSE)
	
	sCamera.vWorldPosition = <<415.6, -980.4, -99.4>>
	sCamera.vWorldRotation = <<10.1, -0.2, 21.0>>
	sCamera.fFov = 48.0
	sCamera.bActive = TRUE
	
	IF bResetAsInactive
		sCamera.bActive = FALSE
	ENDIF
	
ENDPROC

PROC INITIALISE_DATA()
	
	vPairedAnimWorldPosition = << 414.650, -978.460, -100.004 >>
	fPairedAnimHeading = 200.000
	
	INITIALISE_ALL_PEDS_DATA()
	INITIALISE_ALL_VEHICLES_DATA()
	INITIALISE_CAMERA_DATA()
	
ENDPROC

FUNC BOOL LOAD_CELEBRATION_INTERIOR()
	
	IF NOT IS_VALID_INTERIOR(interiorId)
	
		interiorId = GET_INTERIOR_AT_COORDS_WITH_TYPE(sPed[0].vWorldPosition,"v_winningroom")
	
	ELSE
	
		IF IS_INTERIOR_DISABLED(interiorId) 
			DISABLE_INTERIOR(interiorId, FALSE)
		ENDIF
		
		PIN_INTERIOR_IN_MEMORY(interiorId)
		
		RETURN IS_INTERIOR_READY(interiorId)
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CLEANUP_CELEBRATIONS_PEDS()

	INT i
	STRUCT_CELEBRATION_PED sPedTemp
	
	REPEAT MAX_NUM_CELEBRATION_PEDS i 
		
		IF DOES_ENTITY_EXIST(sPed[i].pedId)
			DELETE_PED(sPed[i].pedId)
		ENDIF
		
		sPed[i] = sPedTemp
		INITIALISE_PED_DATA(i)
		
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_CELEBRATION_VEHICLES()
	
	INT i
	STRUCT_CELEBRATION_VEHICLE sVehicleTemp
	
	REPEAT MAX_NUM_CELEBRATION_VEHICLES i
		
		IF DOES_ENTITY_EXIST(sVehicle[i].vehicleId)
			DELETE_VEHICLE(sVehicle[i].vehicleId)
		ENDIF
		
		sVehicle[i] = sVehicleTemp
		INITIALISE_VEHICLE_DATA(i)
		
	ENDREPEAT
	
ENDPROC

PROC CLEANUP_CELEBRATION_CAMERA()
	
	STRUCT_CELEBRATION_CAMERA sCameraTemp
	
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	
	sCamera = sCameraTemp
	INITIALISE_CAMERA_DATA()
		
ENDPROC

PROC CLEANUP_CELEBRATION_EDITOR()
	
	CLEANUP_CELEBRATIONS_PEDS()
	CLEANUP_CELEBRATION_VEHICLES()
	CLEANUP_CELEBRATION_CAMERA()
	
ENDPROC

PROC RESET_PLAY_ANIM_DATA(INT iPed, TEXT_LABEL_63 tl63_AnimDict)
	
	RESET_NET_TIMER(sPed[iPed].stTimer)
	CLEAR_PED_TASKS_IMMEDIATELY(sPed[iPed].pedId)
	sPed[iPed].iAnimSceneId = 0
	REMOVE_ANIM_DICT(tl63_AnimDict)
	sPed[iPed].bPlayAnim = FALSE
	sPed[iPed].iPlayAnimStage = 0
	sPed[iPed].fAnimLength = 0
	
ENDPROC

PROC MAINTAIN_PLAY_CELEBRATION_ANIM(INT iPed)
	
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName, tl63_PairedAnimDict, tl63_PairedAnimName[2]
	VECTOR vPedPairedPos, vPedPairedRot, vPairedScenePos, vPairedSceneRot
	
	IF IS_PED_MALE(sPed[iPed].pedId)
		tl63_AnimDict = "anim@mp_player_intcelebrationmale@finger"
		tl63_AnimName = "finger"
	ELSE
		tl63_AnimDict = "anim@mp_player_intcelebrationfemale@finger"
		tl63_AnimName = "finger"
	ENDIF
	
	tl63_PairedAnimDict = "anim@mp_player_intcelebrationpaired@f_m_manly_handshake"
	tl63_PairedAnimName[0] = "MANLY_HANDSHAKE_LEFT"
	tl63_PairedAnimName[1] = "MANLY_HANDSHAKE_RIGHT"
	
	SWITCH sPed[iPed].iPlayAnimStage
		
		CASE 0
			
			IF sPed[iPed].bPlayAnim
				
				REQUEST_ANIM_DICT(tl63_AnimDict)
				
				IF HAS_ANIM_DICT_LOADED(tl63_AnimDict)
				
					sPed[iPed].iAnimSceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(sPed[iPed].pedId), GET_ENTITY_ROTATION(sPed[iPed].pedId))
					
					IF sPed[iPed].fAnimLength = 0
						sPed[iPed].fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
					ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED(sPed[iPed].stTimer)
						START_NET_TIMER(sPed[iPed].stTimer, TRUE)
					ENDIF
					
					TASK_SYNCHRONIZED_SCENE(	sPed[iPed].pedId, 
												sPed[iPed].iAnimSceneId, 
												tl63_AnimDict, tl63_AnimName, 
												INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN,
												AIK_DISABLE_LEG_IK | 
												AIK_DISABLE_ARM_IK |
												AIK_DISABLE_HEAD_IK |
												AIK_DISABLE_TORSO_IK |
												AIK_DISABLE_TORSO_REACT_IK	)
					
					sPed[iPed].iPlayAnimStage++
					
				ENDIF
				
			ELIF bPlayPairedAnim
				
				IF iPed >= 0
				AND iPed <= 1
					
					REQUEST_ANIM_DICT(tl63_PairedAnimDict)
					
					IF HAS_ANIM_DICT_LOADED(tl63_PairedAnimDict)
						
						vPairedScenePos = vPairedAnimWorldPosition
						vPairedSceneRot = << 0.0, 0.0, fPairedAnimHeading >>
						
						vPairedSceneRot += sPed[iPed].vPairedAnimOffset
						
						sPed[iPed].iAnimSceneId = CREATE_SYNCHRONIZED_SCENE(vPairedScenePos, vPairedSceneRot)
						
						IF sPed[iPed].fAnimLength = 0
							sPed[iPed].fAnimLength = GET_ANIM_DURATION(tl63_PairedAnimDict, tl63_PairedAnimName[iPed])
						ENDIF
						
						IF NOT HAS_NET_TIMER_STARTED(sPed[iPed].stTimer)
							START_NET_TIMER(sPed[iPed].stTimer, TRUE)
						ENDIF
						
						vPedPairedPos = GET_ANIM_INITIAL_OFFSET_POSITION(tl63_PairedAnimDict, tl63_PairedAnimName[iPed], vPairedScenePos, vPairedSceneRot)
						vPedPairedRot = GET_ANIM_INITIAL_OFFSET_ROTATION(tl63_PairedAnimDict, tl63_PairedAnimName[iPed], vPairedScenePos, vPairedSceneRot)
						
						IF DOES_ENTITY_EXIST(sPed[iPed].pedId)
							IF NOT IS_ENTITY_DEAD(sPed[iPed].pedId)
								SET_ENTITY_COORDS_NO_OFFSET(sPed[iPed].pedId, vPedPairedPos)
								SET_ENTITY_ROTATION(sPed[iPed].pedId, vPedPairedRot)
							ENDIF
						ENDIF
						
						TASK_SYNCHRONIZED_SCENE(	sPed[iPed].pedId, 
													sPed[iPed].iAnimSceneId, 
													tl63_PairedAnimDict, tl63_PairedAnimName[iPed], 
													INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
													SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN,
													AIK_DISABLE_LEG_IK | 
													AIK_DISABLE_ARM_IK |
													AIK_DISABLE_HEAD_IK |
													AIK_DISABLE_TORSO_IK |
													AIK_DISABLE_TORSO_REACT_IK	)
						
						sPed[iPed].iPlayAnimStage++
						
					ENDIF
				
				ELSE
					
					RESET_PLAY_ANIM_DATA(iPed, tl63_AnimDict)
				
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sPed[iPed].iAnimSceneId)
				sPed[iPed].iPlayAnimStage++
			ENDIF
			
		BREAK
		
		CASE 2
			
			IF HAS_NET_TIMER_EXPIRED(sPed[iPed].stTimer, (CEIL(sPed[iPed].fAnimLength + 1)*1000), TRUE)
				RESET_PLAY_ANIM_DATA(iPed, tl63_AnimDict)
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC VECTOR GET_PLAYER_NAME_OFFSET_NG_DEBUG(PED_INDEX &peds, INT iIndex)
	
	FLOAT x, y, z
	
	x = 0.0
	y = 0.5
	
	IF iIndex = 5 // Put the fifth peds offset slightly higher to balance out scene.
		IF IS_PED_WEARING_HIGH_HEELS(peds)
			z = 0.10
		ELSE
			z = 0.05
		ENDIF
	ELSE
		IF IS_PED_WEARING_HIGH_HEELS(peds)
			z = 0.10
		ELSE
			z = 0.05
		ENDIF
	ENDIF
	
	RETURN << x, y, z >>
	
ENDFUNC

FUNC VECTOR GET_MISSION_LOCATE_ROTATION_VECTOR_DEBUG(VECTOR paramLocatePos, VECTOR paramPointAtPos)

	//If the vector is ZERO then Return Zero
	IF IS_VECTOR_ZERO(paramPointAtPos)		
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	//Get the heading and flip it by 180
	VECTOR returnRotation
	returnRotation.z = 180 - GET_HEADING_FROM_VECTOR_2D(paramPointAtPos.x - paramLocatePos.x, paramPointAtPos.y - paramLocatePos.y)
	//remove the x and y
	returnRotation.x = 0.0
	returnRotation.y = 0.0
	
	//return the correct rotational vector
	RETURN returnRotation
	
ENDFUNC

PROC MAINTAIN_NAMES()
	
	INT i
	TEXT_LABEL_63 tl63Temp
	BOOL bAllMoviesLoaded = TRUE
	VECTOR vRotation, vPedCoords
	
	SWITCH iDrawNamesStage
		
		CASE 0
			
			IF bEditNAmes
				
				REPEAT MAX_NUM_CELEBRATION_PEDS i 
					
					IF i <= 7 // Limited to 8 scalform movies at the moment. 
						
						tl63Temp = "PLAYER_NAME_0"
						tl63Temp += (i + 1)
						
						playerNameMovies[i] = REQUEST_SCALEFORM_MOVIE(tl63Temp)
						
						IF NOT HAS_SCALEFORM_MOVIE_LOADED(playerNameMovies[i])
							bAllMoviesLoaded = FALSE
						ENDIF
						
					ENDIF
					
				ENDREPEAT
				
				IF bAllMoviesLoaded
					iDrawNamesStage++
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
		
			REPEAT MAX_NUM_CELEBRATION_PEDS i 
				
				IF i <= 7 // Limited to 8 scalform movies at the moment. 
					
					BEGIN_SCALEFORM_MOVIE_METHOD(playerNameMovies[i],"SET_PLAYER_NAME")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("WWWWWWWWW")
					END_SCALEFORM_MOVIE_METHOD()
					
				ENDIF
				
			ENDREPEAT
			
			iDrawNamesStage++
			
		BREAK
		
		CASE 2
			
			REPEAT MAX_NUM_CELEBRATION_PEDS i
			
				IF i <= 7 // Limited to 8 scalform movies at the moment. 
					
					IF DOES_ENTITY_EXIST(sPed[i].pedId)
						IF NOT IS_ENTITY_DEAD(sPed[i].pedId)
							VECTOR vOffset
							vOffset = GET_PLAYER_NAME_OFFSET_NG_DEBUG(sPed[i].pedId, i)
							vPedCoords = GET_PED_BONE_COORDS(sPed[i].pedId, BONETAG_SPINE3, <<0,0,0>>)
							VECTOR vRelativeOffset
							vRelativeOffset = (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sPed[i].pedId, vOffset) - GET_ENTITY_COORDS(sPed[i].pedId))
							VECTOR vTextCoords
							vTextCoords = (vPedCoords + vRelativeOffset)
							vRotation = GET_MISSION_LOCATE_ROTATION_VECTOR_DEBUG(vPedCoords, GET_FINAL_RENDERED_CAM_COORD())
							DRAW_SCALEFORM_MOVIE_3D_SOLID(playerNameMovies[i], vTextCoords, vRotation, sPed[i].sPlayerNameData.vScale, sPed[i].sPlayerNameData.vWorldSize)
						ENDIF
					ENDIF
				
				ENDIF
				
			ENDREPEAT
			
			
		BREAK
		
	ENDSWITCH
	
	
	
ENDPROC

PROC MAINTAIN_PEDS()
	
	INT i
	FLOAT fZcoord
	
	REPEAT MAX_NUM_CELEBRATION_PEDS i 
		
		IF sPed[i].bActive
			
			IF NOT DOES_ENTITY_EXIST(sPed[i].pedId)
				
				IF REQUEST_LOAD_MODEL(sPed[i].eModel)
					sPed[i].pedId = CREATE_PED(PEDTYPE_MISSION, sPed[i].eModel, sPed[i].vWorldPosition, sPed[i].fRotation)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sPed[i].pedId, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(sPed[i].pedId, TRUE)
					SET_ENTITY_INVINCIBLE(sPed[i].pedId, TRUE)
					IF GET_GROUND_Z_FOR_3D_COORD(sPed[i].vWorldPosition, fZcoord)
						sPed[i].vWorldPosition.z = fZcoord
					ELSE
						sPed[i].vWorldPosition.z -= 1.0
					ENDIF
					SET_ENTITY_COORDS(sPed[i].pedId, sPed[i].vWorldPosition)
					SET_MODEL_AS_NO_LONGER_NEEDED(sPed[i].eModel)
				ENDIF
				
			ELSE
				
				IF DOES_ENTITY_EXIST(sPed[i].pedId)
					IF NOT IS_ENTITY_DEAD(sPed[i].pedId)
						IF NOT bPlayPairedAnim
							SET_ENTITY_COORDS(sPed[i].pedId, sPed[i].vWorldPosition)
							SET_ENTITY_HEADING(sPed[i].pedId, sPed[i].fRotation)
						ELSE
							
						ENDIF
					ENDIF
				ENDIF
				
				MAINTAIN_PLAY_CELEBRATION_ANIM(i)
				
			ENDIF
			
		ELSE
			
			IF DOES_ENTITY_EXIST(sPed[i].pedId)
				DELETE_PED(sPed[i].pedId)
				INITIALISE_PED_DATA(i, TRUE)
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_VEHICLES()
	
	INT i
	FLOAT fZcoord
	
	REPEAT MAX_NUM_CELEBRATION_VEHICLES i 
		
		IF sVehicle[i].bActive
			
			IF NOT DOES_ENTITY_EXIST(sVehicle[i].vehicleId)
				
				IF REQUEST_LOAD_MODEL(sVehicle[i].eModel)
					sVehicle[i].vehicleId = CREATE_VEHICLE(sVehicle[i].eModel, sVehicle[i].vWorldPosition, sVehicle[i].fRotation)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehicle[i].vehicleId, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(sVehicle[i].vehicleId, TRUE)
					SET_ENTITY_INVINCIBLE(sVehicle[i].vehicleId, TRUE)
					IF GET_GROUND_Z_FOR_3D_COORD(sVehicle[i].vWorldPosition, fZcoord)
						sVehicle[i].vWorldPosition.z = fZcoord
					ENDIF
					SET_ENTITY_COORDS(sVehicle[i].vehicleId, sVehicle[i].vWorldPosition)
					SET_MODEL_AS_NO_LONGER_NEEDED(sVehicle[i].eModel)
				ENDIF
				
			ELSE
				
				IF DOES_ENTITY_EXIST(sVehicle[i].vehicleId)
					IF IS_VEHICLE_DRIVEABLE(sVehicle[i].vehicleId)
						SET_ENTITY_COORDS(sVehicle[i].vehicleId, sVehicle[i].vWorldPosition)
						SET_ENTITY_HEADING(sVehicle[i].vehicleId, sVehicle[i].fRotation)
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
			
			IF DOES_ENTITY_EXIST(sVehicle[i].vehicleId)
				DELETE_VEHICLE(sVehicle[i].vehicleId)
				INITIALISE_VEHICLE_DATA(i, TRUE)
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
ENDPROC

PROC MAINTAIN_CAMERA()
	
	IF sCamera.bActive
		
		IF NOT DOES_CAM_EXIST(sCamera.camId)
			
			sCamera.camId = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			SET_CAM_ACTIVE(sCamera.camId, TRUE)
			SET_CAM_PARAMS(sCamera.camId, sCamera.vWorldPosition, sCamera.vWorldRotation, sCamera.fFov)
			SET_CAM_NEAR_CLIP(sCamera.camId, 0.01)
			SHAKE_CAM(sCamera.camId, "HAND_SHAKE", 0.3)
			SET_CAM_DOF_FNUMBER_OF_LENS(sCamera.camId, 250.0)
			SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(sCamera.camId, 0.0)
			SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(sCamera.camId, 1.0)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
		ELSE
		
			SET_CAM_PARAMS(sCamera.camId, sCamera.vWorldPosition, sCamera.vWorldRotation, sCamera.fFov)
		
		ENDIF
		
	ELSE
		
		IF DOES_CAM_EXIST(sCamera.camId)
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_CAM_ACTIVE(sCamera.camId, FALSE)
			DESTROY_CAM(sCamera.camId, TRUE)
			INITIALISE_CAMERA_DATA(TRUE)
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PRINT_CELEBRATION_DATA()
	
	INT i
		
	PRINTNL()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("--- CELEBRATION EDITOR SCENE DATA ---") SAVE_NEWLINE_TO_DEBUG_FILE()
	
	REPEAT MAX_NUM_CELEBRATION_PEDS i  
		IF sPed[i].bActive
			SAVE_STRING_TO_DEBUG_FILE("Ped ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" coords = ") SAVE_VECTOR_TO_DEBUG_FILE(sPed[i].vWorldPosition) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Ped ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" heading = ") SAVE_FLOAT_TO_DEBUG_FILE(sPed[i].fRotation) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Ped ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" paired rotation offset = ") SAVE_VECTOR_TO_DEBUG_FILE(sPed[i].vPairedAnimOffset) SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_CELEBRATION_VEHICLES i
		IF sVehicle[i].bActive
			SAVE_STRING_TO_DEBUG_FILE("Vehicle ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" coords = ") SAVE_VECTOR_TO_DEBUG_FILE(sVehicle[i].vWorldPosition) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Vehcile ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" heading = ") SAVE_FLOAT_TO_DEBUG_FILE(sVehicle[i].fRotation) SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
	
	IF sCamera.bActive
		SAVE_STRING_TO_DEBUG_FILE("Camera ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" coords = ") SAVE_VECTOR_TO_DEBUG_FILE(sCamera.vWorldPosition) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" rotation = ") SAVE_VECTOR_TO_DEBUG_FILE(sCamera.vWorldRotation) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Camera ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" FOV = ") SAVE_FLOAT_TO_DEBUG_FILE(sCamera.fFov) SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF
	
	SAVE_STRING_TO_DEBUG_FILE("Paired Anim Pos ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" coords = ") SAVE_VECTOR_TO_DEBUG_FILE(vPairedAnimWorldPosition) SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("Paired Anim Rot ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE(" heading = ") SAVE_FLOAT_TO_DEBUG_FILE(fPairedAnimHeading) SAVE_NEWLINE_TO_DEBUG_FILE()
							
ENDPROC

PROC SET_PLAYER_FOR_CELEBRATION_EDITING(BOOL bSetForEditing)
	
	VECTOR vWarpPos
	
	IF bSetForEditing
		vWarpPos = sPed[0].vWorldPosition
	ELSE
		vWarpPos = << 189.7, -1255.7, 29.2 >>
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpPos)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), bSetForEditing)
			SET_ENTITY_DYNAMIC(PLAYER_PED_ID(), (NOT bSetForEditing))
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), (NOT bSetForEditing))
		ENDIF
	ENDIF
	
ENDPROC

PROC RESET_CELEBRATION_EDITOR(BOOL bTerminateThread = FALSE)
	
	SET_PLAYER_FOR_CELEBRATION_EDITING(FALSE)
	CLEANUP_CELEBRATION_EDITOR()
	iEditingStage = 0
	PRINTLN("[Celebration Editor] - Going to stage ", iEditingStage)
	
	IF bTerminateThread
		PRINTLN("[Celebration Editor] - RESET_CELEBRATION_EDITOR - bTerminateThread = TRUE. Calling TERMINATE_THIS_THREAD().")
		TERMINATE_THIS_THREAD()
	ENDIF
	
ENDPROC

PROC MAINTAIN_CELEBRATION_EDITOR()
	
	IF bTerminateEditor
		RESET_CELEBRATION_EDITOR(TRUE)
	ENDIF
	
	IF bPrintData
		PRINT_CELEBRATION_DATA()
		bPrintData = FALSE
	ENDIF
	
	SWITCH iEditingStage
		
		CASE 0
//			IF bEditingEnabled
//				DO_SCREEN_FADE_OUT(500)
				iEditingStage++
				PRINTLN("[Celebration Editor] - Going to stage ", iEditingStage)
//			ENDIF
		BREAK
		
		CASE 1
//			IF IS_SCREEN_FADED_OUT()
				ANIMPOSTFX_STOP_ALL()
				SET_PLAYER_FOR_CELEBRATION_EDITING(TRUE)
				iEditingStage++
				PRINTLN("[Celebration Editor] - Going to stage ", iEditingStage)
//			ENDIF
		BREAK
		
		CASE 2
			IF LOAD_CELEBRATION_INTERIOR()
//				DO_SCREEN_FADE_IN(500)
				iEditingStage++
				PRINTLN("[Celebration Editor] - Going to stage ", iEditingStage)
			ELSE
				PRINTLN("[Celebration Editor] - LOAD_CELEBRATION_INTERIOR = FALSE.")
			ENDIF
		BREAK
		
		CASE 3 
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			MAINTAIN_PEDS()
			MAINTAIN_VEHICLES()
			MAINTAIN_CAMERA()
			MAINTAIN_NAMES()
			IF NOT bEditingEnabled
				iEditingStage++
				PRINTLN("[Celebration Editor] - Going to stage ", iEditingStage)
			ENDIF
		BREAK
		
		CASE 4
			RESET_CELEBRATION_EDITOR()
		BREAK
		
	ENDSWITCH
	
ENDPROC

// Main
SCRIPT
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	INITIALISE_DATA()
	ADD_WIDGETS()
	
	WHILE TRUE
		
		MP_LOOP_WAIT_ZERO()
		MAINTAIN_CELEBRATION_EDITOR()
		
	ENDWHILE
	
ENDSCRIPT

#ENDIF
