//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	line_activation_test.sc								//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Used to setup line activation locates. Displays		//
//							locate outline and spheres when player activates.	//
//																				//
//////////////////////////////////////////////////////////////////////////////////

USING "buildtype.sch"

#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

CONST_INT MAX_ACTIVATION_TESTS 5

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "lineactivation.sch"
USING "shared_debug.sch"

ENUM STAGE_CONTROL_ENUM
	STAGE_INIT = 0,
	STAGE_UPDATE,
	STAGE_CLEANUP
ENDENUM
STAGE_CONTROL_ENUM eStage = STAGE_INIT

STRUCT ACTIVATION_PARAM_STRUCT
	VECTOR vCoord
	FLOAT fCoordHeading
	FLOAT fLocateSize
	
	INT iDirection
	INT iActiveSide
	INT iEaseOfActivation
	
	BOOL bCheckStationary
	BOOL bActive
	BOOL bSnapToPlayer
ENDSTRUCT
ACTIVATION_PARAM_STRUCT sActivationParams[MAX_ACTIVATION_TESTS]

WIDGET_GROUP_ID widgetID
BOOL bOutputData

/// PURPOSE: Output all the data so we can copy/paste
PROC DO_OUTPUT_TO_SCRIPT()

	INT i
	TEXT_LABEL_31 sDirection, sActiveSide, sEaseOfActivation, sStationary
	
	OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("/* LINE ACTIVATION TEST */")
		SAVE_NEWLINE_TO_DEBUG_FILE()
	
		REPEAT MAX_ACTIVATION_TESTS i
			IF sActivationParams[i].bActive
				IF sActivationParams[i].iDirection = 0		sDirection = "ECOMPASS_NORTH"
				ELIF sActivationParams[i].iDirection = 0	sDirection = "ECOMPASS_EAST"
				ELIF sActivationParams[i].iDirection = 0	sDirection = "ECOMPASS_WEST"
				ELIF sActivationParams[i].iDirection = 0	sDirection = "ECOMPASS_SOUTH"
				ENDIF
				
				IF sActivationParams[i].iActiveSide = 0		sActiveSide = "EACTIVESIDE_ONEWAY"
				ELIF sActivationParams[i].iActiveSide = 0	sActiveSide = "EACTIVESIDE_OPPOSITES"
				ELIF sActivationParams[i].iActiveSide = 0	sActiveSide = "EACTIVESIDE_FOURCORNERS"
				ENDIF
				
				IF sActivationParams[i].iEaseOfActivation = 0	sEaseOfActivation = "EACTIVATIONEASE_HARD"
				ELIF sActivationParams[i].iEaseOfActivation = 0	sEaseOfActivation = "EACTIVATIONEASE_MEDIUM"
				ELIF sActivationParams[i].iEaseOfActivation = 0	sEaseOfActivation = "EACTIVATIONEASE_EASY"
				ENDIF
				
				IF sActivationParams[i].bCheckStationary		sStationary = "TRUE"
				ELIF NOT sActivationParams[i].bCheckStationary	sStationary = "FALSE"
				ENDIF
				
				SAVE_STRING_TO_DEBUG_FILE("Coord				= ")	SAVE_VECTOR_TO_DEBUG_FILE(sActivationParams[i].vCoord)			SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("CoordHeading		= ")		SAVE_FLOAT_TO_DEBUG_FILE(sActivationParams[i].fCoordHeading)	SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Direction			= ")	SAVE_STRING_TO_DEBUG_FILE(sDirection)							SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("LocateSize			= ")	SAVE_FLOAT_TO_DEBUG_FILE(sActivationParams[i].fLocateSize)		SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("activeSide			= ")	SAVE_STRING_TO_DEBUG_FILE(sActiveSide)							SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Ease_of_activation	= ")	SAVE_STRING_TO_DEBUG_FILE(sEaseOfActivation)					SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Check_stationary	= ")		SAVE_STRING_TO_DEBUG_FILE(sStationary)							SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("IF IS_PLAYER_ACTIVATING_COORD(Coord, CoordHeading, Direction, LocateSize, activeSide, Ease_of_activation, Check_stationary)")
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("IF IS_PLAYER_ACTIVATING_COORD(")
				SAVE_VECTOR_TO_DEBUG_FILE(sActivationParams[i].vCoord)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sActivationParams[i].fCoordHeading)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(sDirection)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(sActivationParams[i].fLocateSize)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(sActiveSide)	
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(sEaseOfActivation)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_STRING_TO_DEBUG_FILE(sStationary)
				SAVE_STRING_TO_DEBUG_FILE(")")
				
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		CLOSE_DEBUG_FILE()
	ENDREPEAT
ENDPROC

/// PURPOSE: Setup the widgets and default data
PROC DO_INITIALISE()
	
	INT i
	TEXT_LABEL_31 sTempLabel
	
	// Set up widgets
	START_WIDGET_GROUP("Line Activation Test")
	
		ADD_WIDGET_BOOL("Output data", bOutputData)
		
		REPEAT MAX_ACTIVATION_TESTS i
		
			// Set some default states
			sActivationParams[i].bSnapToPlayer = TRUE
			sActivationParams[i].fLocateSize = 0.4
			
			sTempLabel = "Test[" sTempLabel += i sTempLabel += "]"
			widgetID = START_WIDGET_GROUP(sTempLabel)
				ADD_WIDGET_BOOL("Active", sActivationParams[i].bActive)
				ADD_WIDGET_BOOL("Snap To Player", sActivationParams[i].bSnapToPlayer)
				ADD_WIDGET_VECTOR_SLIDER("Coord", sActivationParams[i].vCoord, -10000.0, 10000.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Heading", sActivationParams[i].fCoordHeading, 0.0, 360.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Locate Size", sActivationParams[i].fLocateSize, 0.0, 50.0, 0.2)
				ADD_WIDGET_INT_SLIDER("Direction", sActivationParams[i].iDirection, 0, 3, 1)
				ADD_WIDGET_INT_SLIDER("Side", sActivationParams[i].iActiveSide, 0, 2, 1)
				ADD_WIDGET_INT_SLIDER("Ease", sActivationParams[i].iEaseOfActivation, 0, 2, 1)
				ADD_WIDGET_BOOL("Check Stationary", sActivationParams[i].bCheckStationary)
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
	STOP_WIDGET_GROUP()
	SETTIMERA(0)
	eStage = STAGE_UPDATE
ENDPROC

/// PURPOSE: Update locate positions, play anims, set anim phase etc.
PROC DO_UPDATE()
	
	IF (TIMERA() < 8000)
		SET_TEXT_SCALE(0.35,0.35)
		SET_TEXT_CENTRE(TRUE)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.05, "STRING", "Turn on debug lines and spheres in RAG/Script/Draw Debug Lines And Spheres")
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF bOutputData
			DO_OUTPUT_TO_SCRIPT()
			bOutputData = FALSE
		ENDIF
		
		INT i
		REPEAT MAX_ACTIVATION_TESTS i
			IF sActivationParams[i].bSnapToPlayer
				sActivationParams[i].vCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				sActivationParams[i].bSnapToPlayer = FALSE
			ENDIF
			
			IF sActivationParams[i].bActive
				IF IS_PLAYER_ACTIVATING_COORD(sActivationParams[i].vCoord, sActivationParams[i].fCoordHeading, INT_TO_ENUM(ECOMPASS, sActivationParams[i].iDirection),sActivationParams[i].fLocateSize, INT_TO_ENUM(EACTIVESIDE, sActivationParams[i].iActiveSide), INT_TO_ENUM(EACTIVATIONEASE, sActivationParams[i].iEaseOfActivation), sActivationParams[i].bCheckStationary)
					DRAW_DEBUG_SPHERE(sActivationParams[i].vCoord, 0.1, 0, 255, 0, 255)
					DRAW_DEBUG_LINE(sActivationParams[i].vCoord, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sActivationParams[i].vCoord, sActivationParams[i].fCoordHeading, <<0.0, 0.5, 0.0>>), 0, 255, 0, 255)
				ELSE
					DRAW_DEBUG_SPHERE(sActivationParams[i].vCoord, 0.1, 255, 0, 0, 255)
					DRAW_DEBUG_LINE(sActivationParams[i].vCoord, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sActivationParams[i].vCoord, sActivationParams[i].fCoordHeading, <<0.0, 0.5, 0.0>>), 255, 0, 0, 255)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			eStage = STAGE_CLEANUP
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Remove any widgets and terminate
PROC DO_CLEANUP()
	
	// Remove widgets
	IF DOES_WIDGET_GROUP_EXIST(widgetID)
		DELETE_WIDGET_GROUP(widgetID)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	
	WHILE TRUE
	
		WAIT(0)
		
		SWITCH eStage
			CASE STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE STAGE_UPDATE
				DO_UPDATE()
			BREAK
			
			CASE STAGE_CLEANUP
				DO_CLEANUP()
			BREAK
		ENDSWITCH
	ENDWHILE
	
ENDSCRIPT
#ENDIF