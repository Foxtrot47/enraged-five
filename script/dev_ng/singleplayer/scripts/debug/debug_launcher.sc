// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD


USING "rage_builtins.sch"

// game commands
USING "commands_debug.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_script.sch"
USING"script_player.sch"
USING "script_XML.sch"
USING "commands_audio.sch"

ENUM XmlInput
	SWITCH_ON_SKIS = 0, 
	SWITCH_OFF_SKIS,
	TEST_NAVIGATION = 100, 		//reserved for launching scripts
	TEST_COMBAT = 101,
	TEST_GENERAL = 102,
	TEST_CAMERA = 103,
	TEST_WARPS = 104, 
	TEST_VEHICLE_AI = 105,
	TEST_CUTSCENE = 106,
//	TEST_RAYFIRE = 107,
	TEST_DYNAMIX = 108,
	TEST_PHYSICS = 109,
	TEST_PHYSICS_PERF = 110,
	TEST_HORSE = 111,
	TEST_VEHICLE = 112,
//	TEST_BINK = 113,
	TEST_SCRIPTED_RT = 114,
	TEST_PROP_DROP = 115,
	TEST_SCENARIO = 116,
	TEST_AINEWENGLAND = 117,
	TEST_WALKING_PED = 118,
	TEST_COVER = 119,
	TEST_TARGETTING = 120,
	TEST_SWITCH = 121,
	TEST_CUTSCENE_SAMPLES = 122
ENDENUM

PROC  SET_SCRIPT_CAN_START (string current_Script, int current_Script_Hash)
	int iCurrent_Script
		iCurrent_Script = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(current_Script_Hash)
		
		IF (iCurrent_Script = 0)
			REQUEST_SCRIPT(current_Script)
				WHILE NOT HAS_SCRIPT_LOADED(current_Script)
						WAIT(0)
				ENDWHILE
			IF ARE_STRINGS_EQUAL(current_Script, "physics_perf_test") 
			or ARE_STRINGS_EQUAL(current_Script, "physics_test") 
				START_NEW_SCRIPT(current_Script, MISSION_STACK_SIZE)
			ELSE
				START_NEW_SCRIPT(current_Script, DEFAULT_STACK_SIZE)
			ENDIF
			PRINTSTRING("Launching ")
			PRINTSTRING(current_Script)
			PRINTSTRING(" script")
			PRINTNL()
		ENDIF
	//ENDIF
ENDPROC


//please add any script here that you want to launch
PROC TERMINATE_TEST_SCRIPTS ( )
//	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("RayFire_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("combat_test" )
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("general_test" )
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("camera_test" )
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("navmeshtest" )			
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("vehicle_ai_test" )
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("physics_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("cutscene_test" )	
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("DynaMixTest")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("physics_perf_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("horse_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("vehicle_test")
//	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("bink_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("scriptedRTTest")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("prop_drop")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("scenarios_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("ainewengland_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("walking_ped")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("cover_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("targetting_test")
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME ("switch_test")
ENDPROC

SCRIPT(XmlInput xmlValue)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		TERMINATE_TEST_SCRIPTS()
		wait (5)
		TERMINATE_THIS_THREAD()
	ENDIF

//	STRING CurrentScript
	//IF ADDING A NEW SCRIPT PLEASE ADD TO THE TERMINATE_TEST_SCRIPTS function
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SWITCH xmlValue
			//Quick debug options
			CASE SWITCH_ON_SKIS      
				SET_PED_SKIING(PLAYER_PED_ID(), TRUE)  
			BREAK
		
			CASE SWITCH_OFF_SKIS    
				SET_PED_SKIING(PLAYER_PED_ID(), FALSE)  
			BREAK
		
		//Dealing with the launching of test scripts
			CASE TEST_NAVIGATION	
					TERMINATE_TEST_SCRIPTS()			//Terminate 
					wait (5)
					SET_SCRIPT_CAN_START ("navmeshtest", HASH("navmeshtest"))
			BREAK
			
			CASE TEST_COMBAT
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("combat_test", HASH("combat_test"))
			BREAK
			
			CASE TEST_GENERAL
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("general_test", HASH("general_test"))
			BREAK		
			
			CASE TEST_SCENARIO
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("scenarios_test", HASH("scenarios_test"))
			BREAK	

			CASE TEST_AINEWENGLAND
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("ainewengland_test", HASH("ainewengland_test"))
			BREAK		
	
			CASE TEST_CAMERA
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("camera_test", HASH("camera_test"))
			BREAK
		
			CASE TEST_WARPS
					TERMINATE_TEST_SCRIPTS()
			BREAK
			
			CASE TEST_VEHICLE_AI
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("vehicle_ai_test", HASH("vehicle_ai_test"))
			BREAK
			
			CASE TEST_PHYSICS
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("physics_test", HASH("physics_test"))
			BREAK
			
			CASE TEST_CUTSCENE
				TERMINATE_TEST_SCRIPTS()
				wait(5)
				SET_SCRIPT_CAN_START("cutscene_test", HASH("cutscene_test"))
			BREAK

			CASE TEST_DYNAMIX
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START("DynaMixTest", HASH("DynaMixTest"))
			BREAK
			
// 			CASE TEST_RAYFIRE
// 				TERMINATE_TEST_SCRIPTS()
// 				wait(5)
// 				SET_SCRIPT_CAN_START("RayFire_test")
// 			BREAK
			
			CASE TEST_PHYSICS_PERF
				TERMINATE_TEST_SCRIPTS()
				wait (5)
				SET_SCRIPT_CAN_START("physics_perf_test", HASH("physics_perf_test"))
			BREAK
			
			CASE TEST_HORSE
				TERMINATE_TEST_SCRIPTS()
				wait (5)
				SET_SCRIPT_CAN_START("horse_test", HASH("horse_test"))
			BREAK
			
			CASE TEST_VEHICLE
					TERMINATE_TEST_SCRIPTS()
					wait (5)
					SET_SCRIPT_CAN_START ("vehicle_test", HASH("vehicle_test"))
			BREAK
					
//			CASE TEST_BINK
//				TERMINATE_TEST_SCRIPTS()
//				wait (5)
//				SET_SCRIPT_CAN_START ("bink_test")
//			BREAK

			case TEST_SCRIPTED_RT
				TERMINATE_TEST_SCRIPTS()
				wait (5)
				SET_SCRIPT_CAN_START("scriptedRTTest", HASH("scriptedRTTest"))
			BREAK
			CASE TEST_PROP_DROP
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("prop_drop", HASH("prop_drop"))
			BREAK
			CASE TEST_WALKING_PED
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("walking_ped", HASH("walking_ped"))
			BREAK
			CASE TEST_COVER
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("cover_test", HASH("cover_test"))
			BREAK
			CASE TEST_TARGETTING
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("targetting_test", HASH("targetting_test"))
			BREAK
			CASE TEST_SWITCH
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("switch_test", HASH("switch_test"))
			BREAK
			CASE TEST_CUTSCENE_SAMPLES
				TERMINATE_TEST_SCRIPTS()
				WAIT(5)
				SET_SCRIPT_CAN_START("CutsceneSamples", HASH("CutsceneSamples"))			
			BREAK
		ENDSWITCH
		
	ENDIF

ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD

