//////////////////////////////////////////////////////////////////////////////////
//																				//
//		SCRIPT NAME		:	hud_creator.sc										//
//		AUTHOR			:	Kenneth Ross										//
//		DESCRIPTION		:	Used to launch the Hud Creator Tool created by		//
//							Andy Duthie. Calls the Maintain_Hud_Creator_Tool() 	//
//							proc each frame until the F key has been pressed.	//
//																				//
//////////////////////////////////////////////////////////////////////////////////



// Include the standard headers (which also ensures the correct #DEFINEs are recognised)
USING "rage_builtins.sch"
USING "globals.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD
USING "commands_script.sch"
USING "Hud_creator_tool.sch"

PROC CLEANUP_SCRIPT()
 	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()
		CLEANUP_SCRIPT()
	ENDIF
	
	WHILE TRUE
	
		WAIT(0)
		
		Maintain_Hud_Creator_Tool()
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			CLEANUP_SCRIPT()
		ENDIF
		
	ENDWHILE
	
	
ENDSCRIPT
#ENDIF