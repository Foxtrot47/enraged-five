#IF IS_FINAL_BUILD

SCRIPT 
ENDSCRIPT

#ENDIF

#IF IS_DEBUG_BUILD
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

USING "script_player.sch"
USING "script_misc.sch"

USING "shared_debug.sch"
USING "script_debug.sch"
USING "scripted_cam_editor_public.sch"


CONST_INT CUT_TYPE_STATIC		0
CONST_INT CUT_TYPE_INTERP		1
CONST_INT CUT_TYPE_SPLINE		2

CONST_INT SHAKE_TYPE_ROAD_VIBRATION		1
CONST_INT SHAKE_TYPE_HAND				2
CONST_INT SHAKE_TYPE_SKY_DIVING			3
CONST_INT SHAKE_TYPE_FIRST_PERSON		4
CONST_INT SHAKE_TYPE_WOBBLY				5
CONST_INT SHAKE_TYPE_VIBRATE			6

CONST_INT MAX_NODES_PER_CUT		16
CONST_INT MAX_LOOKAT_NODES		8
CONST_INT MAX_CUTS				4

STRUCT CAM_NODE_DATA
	VECTOR v_pos
	VECTOR v_rot
	FLOAT f_fov
	INT i_duration //Used for both spline and interp duration if relevant.
	
	//Interp-only variables
	INT i_pos_graph_type
	INT i_rot_graph_type
	
	//Spline-only variables
	INT i_spline_node_flags
ENDSTRUCT

STRUCT CAM_LOOKAT_NODE_DATA
	VECTOR v_pos
	INT i_duration
ENDSTRUCT

STRUCT CAMERA_CUT_DATA
	INT i_duration
	INT i_type //0 = static, 1 = interp, 2 = spline
	INT i_shake_type
	FLOAT f_shake_amplitude
	FLOAT f_blur_strength
	
	CAM_NODE_DATA s_nodes[MAX_NODES_PER_CUT]
	CAM_LOOKAT_NODE_DATA s_lookat_nodes[MAX_LOOKAT_NODES]

	//Spline-only variables
	INT i_spline_smooth_type
	BOOL b_use_lookat_spline
ENDSTRUCT

STRUCT SPLINE_EDITOR_DATA
	CAMERA_INDEX cam_main
	CAMERA_INDEX cam_lookat
	CAMERA_INDEX cam_nodes[MAX_NODES_PER_CUT]
	OBJECT_INDEX obj_helper
	WIDGET_GROUP_ID widget
	WIDGET_GROUP_ID widget_cut
	TEXT_WIDGET_ID widget_save_name
	CAMERA_CUT_DATA s_cuts[MAX_CUTS]
	
	//General functionality
	BOOL b_output_data
	BOOL b_save_to_xml
	BOOL b_load_from_xml
	BOOL b_add_node_at_current_cam_pos
	BOOL b_replace_node_at_current_cam_pos
	BOOL b_add_lookat_node_at_current_cam_pos
	BOOL b_replace_lookat_node_at_current_cam_pos
	BOOL b_try_to_replicate_cam_rotation
	BOOL b_delete_current_node
	BOOL b_delete_current_lookat_node
	BOOl b_view_node
	BOOL b_preview_spline
	BOOL b_display_spline
	BOOL b_pause_game
	BOOL b_game_paused
	BOOL b_terminate
	BOOL b_debug_lines_active
	INT i_interp_to_game_cam_duration
	INT i_playback_current_event
	
	//Vehicle recording syncing
	VEHICLE_INDEX veh_to_sync_with
	BOOL b_sync_recording
	FLOAT f_playback_sync_time
	
	//Variables keeping track of the currently selected cut
	INT i_current_cut
	INT i_previous_cut
	INT i_current_cut_duration
	INT i_current_cut_type
	INT i_previous_cut_type
	INT i_current_cut_smooth_style
	INT i_current_cut_shake_type
	FLOAT f_current_cut_shake_amplitude
	FLOAT f_current_cut_blur_strength
	
	//Variables keeping track of the currently selected node
	INT i_current_node
	INT i_previous_node
	VECTOR v_current_node_pos
	VECTOR v_current_node_rot
	FLOAT f_current_node_fov
	INT i_current_node_duration //Note: this is used for both interps and splines depending on context.
	INT i_current_node_interp_pos_graph
	INT i_current_node_interp_rot_graph
	INT i_current_node_spline_smooth
	
	//Variables keeping track of the currently selected lookat node
	INT i_current_lookat_node
	INT i_previous_lookat_node
	VECTOR v_current_lookat_node_pos
	INT i_current_lookat_duration
	BOOL b_current_lookat_spline_active
ENDSTRUCT

SPLINE_EDITOR_DATA s_cam_editor

PROC CREATE_CUT_WIDGETS(BOOL b_add_to_group = FALSE)
	IF b_add_to_group
		SET_CURRENT_WIDGET_GROUP(s_cam_editor.widget)
	ENDIF

	s_cam_editor.widget_cut = START_WIDGET_GROUP("Camera cut options")	
		ADD_WIDGET_STRING("General cut options")
		ADD_WIDGET_INT_SLIDER("Current cut", s_cam_editor.i_current_cut, 0, MAX_CUTS - 1, 1)
		ADD_WIDGET_INT_SLIDER("Cut duration", s_cam_editor.i_current_cut_duration, 0, 100000, 100)
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("None")
			ADD_TO_WIDGET_COMBO("ROAD_VIBRATION_SHAKE")
			ADD_TO_WIDGET_COMBO("HAND_SHAKE")
			ADD_TO_WIDGET_COMBO("SKY_DIVING_SHAKE")
			ADD_TO_WIDGET_COMBO("FIRST_PERSON_AIM_SHAKE")
			ADD_TO_WIDGET_COMBO("WOBBLY_SHAKE")
			ADD_TO_WIDGET_COMBO("VIBRATE_SHAKE")
		STOP_WIDGET_COMBO("Shake type", s_cam_editor.i_current_cut_shake_type)
		ADD_WIDGET_FLOAT_SLIDER("Shake amplitude", s_cam_editor.f_current_cut_shake_amplitude, 0.0, 1.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Motion blur strength", s_cam_editor.f_current_cut_blur_strength, 0.0, 1.0, 0.1)
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("Static cam")
			ADD_TO_WIDGET_COMBO("Interp")
			ADD_TO_WIDGET_COMBO("Spline")
		STOP_WIDGET_COMBO("Camera type", s_cam_editor.i_current_cut_type)
		ADD_WIDGET_STRING("")
		
		//Build different widgets depending on the cut type
		IF s_cam_editor.i_current_cut_type = CUT_TYPE_STATIC		
			ADD_WIDGET_STRING("Cam options") 
			ADD_WIDGET_VECTOR_SLIDER("Position", s_cam_editor.v_current_node_pos, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", s_cam_editor.v_current_node_rot, -180.0, 180.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("FOV", s_cam_editor.f_current_node_fov, 0.0, 80.0, 0.1)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_BOOL("View cam", s_cam_editor.b_view_node)
			ADD_WIDGET_BOOL("Delete cam", s_cam_editor.b_delete_current_node)
		ELIF s_cam_editor.i_current_cut_type = CUT_TYPE_INTERP
			ADD_WIDGET_STRING("Individual node options: an interp is made up of 2 nodes.") 
			ADD_WIDGET_INT_SLIDER("Current node", s_cam_editor.i_current_node, 0, 1, 1)
			ADD_WIDGET_BOOL("View current node", s_cam_editor.b_view_node)
			ADD_WIDGET_BOOL("Delete current node", s_cam_editor.b_delete_current_node)
			ADD_WIDGET_VECTOR_SLIDER("Position", s_cam_editor.v_current_node_pos, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", s_cam_editor.v_current_node_rot, -180.0, 180.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("FOV", s_cam_editor.f_current_node_fov, 0.0, 80.0, 0.1)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_INT_SLIDER("Interp duration", s_cam_editor.i_current_node_duration, 0, 100000, 100)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Default")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
			STOP_WIDGET_COMBO("Position graph type", s_cam_editor.i_current_node_interp_pos_graph)
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Default")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_SIN_ACCEL_DECEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_ACCEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_DECEL")
				ADD_TO_WIDGET_COMBO("GRAPH_TYPE_LINEAR")
			STOP_WIDGET_COMBO("Rotation graph type", s_cam_editor.i_current_node_interp_rot_graph)
		ELSE
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Default")
				ADD_TO_WIDGET_COMBO("CAM_SPLINE_NO_SMOOTHING")
				ADD_TO_WIDGET_COMBO("CAM_SPLINE_SLOW_IN")
				ADD_TO_WIDGET_COMBO("CAM_SPLINE_SLOW_OUT")
				ADD_TO_WIDGET_COMBO("CAM_SPLINE_SLOW_IN_OUT")
			STOP_WIDGET_COMBO("Spline smoothing style", s_cam_editor.i_current_cut_smooth_style)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_STRING("Individual spline node options") 
			ADD_WIDGET_INT_SLIDER("Current node", s_cam_editor.i_current_node, 0, MAX_NODES_PER_CUT - 1, 1)
			ADD_WIDGET_BOOL("View current node", s_cam_editor.b_view_node)
			ADD_WIDGET_BOOL("Delete current node", s_cam_editor.b_delete_current_node)
			ADD_WIDGET_VECTOR_SLIDER("Position", s_cam_editor.v_current_node_pos, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", s_cam_editor.v_current_node_rot, -180.0, 180.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("FOV", s_cam_editor.f_current_node_fov, 0.0, 80.0, 0.1)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_INT_SLIDER("Duration from previous node", s_cam_editor.i_current_node_duration, 0, 100000, 100)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("Default")
				ADD_TO_WIDGET_COMBO("NO_FLAGS")
				ADD_TO_WIDGET_COMBO("SMOOTH_LENS_PARAMS")
				ADD_TO_WIDGET_COMBO("SMOOTH_ROT")
			STOP_WIDGET_COMBO("Node smooth type", s_cam_editor.i_current_node_spline_smooth)
			ADD_WIDGET_STRING("")
			
			ADD_WIDGET_STRING("Lookat spline options") 
			ADD_WIDGET_BOOL("Point main spline at lookat spline (spline will ignore rotation settings)", s_cam_editor.b_current_lookat_spline_active)
			ADD_WIDGET_BOOL("Replicate debug cam rotation when adding nodes", s_cam_editor.b_try_to_replicate_cam_rotation)
			ADD_WIDGET_BOOL("Add lookat node at debug cam pos", s_cam_editor.b_add_lookat_node_at_current_cam_pos)
			ADD_WIDGET_BOOL("Replace current lookat node using debug cam pos", s_cam_editor.b_replace_lookat_node_at_current_cam_pos)
			ADD_WIDGET_BOOL("Delete current lookat node", s_cam_editor.b_delete_current_lookat_node)
			ADD_WIDGET_INT_SLIDER("Current lookat node", s_cam_editor.i_current_lookat_node, 0, MAX_LOOKAT_NODES - 1, 1)
			ADD_WIDGET_VECTOR_SLIDER("Lookat node pos", s_cam_editor.v_current_lookat_node_pos, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_INT_SLIDER("Duration from previous node", s_cam_editor.i_current_lookat_duration, 0, 100000, 100)
		ENDIF
	STOP_WIDGET_GROUP()
	
	IF b_add_to_group
		CLEAR_CURRENT_WIDGET_GROUP(s_cam_editor.widget)
	ENDIF
ENDPROC

PROC CREATE_WIDGETS(BOOL b_only_create_cut_widgets = FALSE)
	IF b_only_create_cut_widgets
		CREATE_CUT_WIDGETS(TRUE)
	ELSE
		s_cam_editor.widget = START_WIDGET_GROUP("Scripted Cam Editor")		
			ADD_WIDGET_BOOL("Terminate editor", s_cam_editor.b_terminate)
			ADD_WIDGET_BOOL("Pause/unpause game (except this script)", s_cam_editor.b_pause_game)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Add node at debug cam pos", s_cam_editor.b_add_node_at_current_cam_pos)
			ADD_WIDGET_BOOL("Replace current node using debug cam pos", s_cam_editor.b_replace_node_at_current_cam_pos)
			ADD_WIDGET_INT_SLIDER("Interp to game cam duration", s_cam_editor.i_interp_to_game_cam_duration, 0, 50000, 100)
			ADD_WIDGET_BOOL("Display sequence", s_cam_editor.b_display_spline)
			ADD_WIDGET_BOOL("Preview sequence", s_cam_editor.b_preview_spline)
			
			START_WIDGET_GROUP("Output Options")
				ADD_WIDGET_BOOL("Output script to temp_debug", s_cam_editor.b_output_data)
				s_cam_editor.widget_save_name = ADD_TEXT_WIDGET("File name")
				ADD_WIDGET_BOOL("Save sequence to XML file", s_cam_editor.b_save_to_xml)
				ADD_WIDGET_BOOL("Load sequence from XML file", s_cam_editor.b_load_from_xml)
			STOP_WIDGET_GROUP()
			
			/*START_WIDGET_GROUP("Uber recording integration")
				ADD_WIDGET_BOOL("Sync vehicle recording with cutscene", s_cam_editor.b_sync_recording)
				ADD_WIDGET_FLOAT_SLIDER("Playback sync time", s_cam_editor.f_playback_sync_time, 0.0, 500000.0, 0.01)
			STOP_WIDGET_GROUP()*/
			
			CREATE_CUT_WIDGETS()
			
		STOP_WIDGET_GROUP()
	ENDIF
ENDPROC

PROC DESTROY_WIDGETS(BOOL b_destroy_cut_widgets_only = FALSE)
	IF b_destroy_cut_widgets_only
		IF DOES_WIDGET_GROUP_EXIST(s_cam_editor.widget_cut)
			DELETE_WIDGET_GROUP(s_cam_editor.widget_cut)
		ENDIF
	ELSE
		IF DOES_TEXT_WIDGET_EXIST(s_cam_editor.widget_save_name)
			DELETE_TEXT_WIDGET(s_cam_editor.widget_save_name)
		ENDIF

		IF DOES_WIDGET_GROUP_EXIST(s_cam_editor.widget)
			DELETE_WIDGET_GROUP(s_cam_editor.widget)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_EDITOR()
	DESTROY_ALL_CAMS()
	DESTROY_WIDGETS()
	
	IF DOES_ENTITY_EXIST(s_cam_editor.obj_helper)
		DELETE_OBJECT(s_cam_editor.obj_helper)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

FUNC CAM_NODE_DATA GET_EMPTY_NODE()
	CAM_NODE_DATA s_empty_node
	
	RETURN s_empty_node
ENDFUNC

FUNC CAM_LOOKAT_NODE_DATA GET_EMPTY_LOOKAT_NODE()
	CAM_LOOKAT_NODE_DATA s_empty_node
	
	RETURN s_empty_node
ENDFUNC

//PROC SYNC_CUTSCENE_EDITOR_PLAYBACK_TO_THIS_FRAME()
//	IF HAS_SCRIPT_LOADED("locates_tester.sc")
//		SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 1234)
//	ENDIF
//ENDPROC

PROC SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(VEHICLE_INDEX &veh, FLOAT f_time)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, f_time - GET_TIME_POSITION_IN_RECORDING(veh))
		ENDIF
	ENDIF
ENDPROC

FUNC CAMERA_GRAPH_TYPE GET_CAMERA_GRAPH_TYPE_FROM_INT(INT i)
	IF i = 1
		RETURN GRAPH_TYPE_SIN_ACCEL_DECEL
	ELIF i = 2
		RETURN GRAPH_TYPE_ACCEL
	ELIF i = 3
		RETURN GRAPH_TYPE_DECEL
	ELIF i = 4
		RETURN GRAPH_TYPE_LINEAR
	ENDIF
	
	RETURN GRAPH_TYPE_SIN_ACCEL_DECEL //Default
ENDFUNC

FUNC CAM_SPLINE_NODE_FLAGS GET_SPLINE_NODE_FLAG_FROM_INT(INT i)
	IF i = 1
		RETURN CAM_SPLINE_NODE_NO_FLAGS
	ELIF i = 2
		RETURN CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS
	ELIF i = 3
		RETURN CAM_SPLINE_NODE_SMOOTH_ROT
	ENDIF
	
	RETURN CAM_SPLINE_NODE_NO_FLAGS
ENDFUNC

PROC DRAW_CAMERA_BOX(SPLINE_EDITOR_DATA &s_editor, VECTOR v_pos, VECTOR v_rot, INT r, INT g, INT b, INT a)
	CONST_FLOAT CAM_LENGTH	0.3
	CONST_FLOAT CAM_WIDTH	0.15
	CONST_FLOAT CAM_HEIGHT	0.15
	
	IF DOES_ENTITY_EXIST(s_editor.obj_helper)
		SET_ENTITY_COORDS_NO_OFFSET(s_editor.obj_helper, v_pos)
		SET_ENTITY_ROTATION(s_editor.obj_helper, v_rot)
		
		VECTOR v_front_top_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_front_top_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_front_bottom_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_front_bottom_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_back_top_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_back_top_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_back_bottom_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_back_bottom_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_cone_centre = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<0.0, CAM_LENGTH, 0.0>>)
		VECTOR v_cone_top_left = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_top_right = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_bottom_left = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_bottom_right = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_editor.obj_helper, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
		
		DRAW_DEBUG_LINE(v_front_top_left_corner, v_front_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_top_right_corner, v_front_bottom_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_right_corner, v_front_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_left_corner, v_front_top_left_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_back_top_left_corner, v_back_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_top_right_corner, v_back_bottom_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_bottom_right_corner, v_back_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_bottom_left_corner, v_back_top_left_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_front_top_left_corner, v_back_top_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_top_right_corner, v_back_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_left_corner, v_back_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_right_corner, v_back_bottom_right_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_top_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_top_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_bottom_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_bottom_right, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_cone_top_left, v_cone_top_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_top_right, v_cone_bottom_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_bottom_right, v_cone_bottom_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_bottom_left, v_cone_top_left, r, g, b, a)
	ENDIF
ENDPROC

PROC DRAW_CAM_DATA()
	IF NOT s_cam_editor.b_debug_lines_active
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		s_cam_editor.b_debug_lines_active = TRUE
	ENDIF
	
	VECTOR v_mid
	FLOAT f_time, f_speed, f_rot_speed, f_dist, f_rot_diff
	TEXT_LABEL_31 str_speed, str_rot_speed
	
	
	INT i = 0, j = 0
	INT r, g, b
	FOR i = 0 TO MAX_CUTS - 1 STEP 1
		//Draw the cam nodes for this cut
		FOR j = 0 TO MAX_NODES_PER_CUT - 1 STEP 1
			IF s_cam_editor.s_cuts[i].s_nodes[j].f_fov > 0.0
				IF s_cam_editor.i_current_node = j AND s_cam_editor.i_current_cut = i
					r = 255
					g = 0
					b = 0
				ELSE
					r = 0
					g = 0
					b = 255
				ENDIF
			
				TEXT_LABEL str_index = ""
				str_index += j
			
				DRAW_DEBUG_TEXT(str_index, s_cam_editor.s_cuts[i].s_nodes[j].v_pos + <<0.0, 0.0, 1.0>>, r, g, b, 255)	
				DRAW_CAMERA_BOX(s_cam_editor, s_cam_editor.s_cuts[i].s_nodes[j].v_pos, s_cam_editor.s_cuts[i].s_nodes[j].v_rot, r, g, b, 255)
				
				IF j > 0
					DRAW_DEBUG_LINE(s_cam_editor.s_cuts[i].s_nodes[j].v_pos, s_cam_editor.s_cuts[i].s_nodes[j-1].v_pos, 0, 128, 0, 255)
					
					//Mid-point (this is where the speed will be drawn)
					v_mid = s_cam_editor.s_cuts[i].s_nodes[j-1].v_pos + ((s_cam_editor.s_cuts[i].s_nodes[j].v_pos - s_cam_editor.s_cuts[i].s_nodes[j-1].v_pos) * 0.5)
					f_time = TO_FLOAT(s_cam_editor.s_cuts[i].s_nodes[j].i_duration) / 1000.0
					f_dist = VDIST(s_cam_editor.s_cuts[i].s_nodes[j-1].v_pos, s_cam_editor.s_cuts[i].s_nodes[j].v_pos)
					f_rot_diff = VDIST(s_cam_editor.s_cuts[i].s_nodes[j-1].v_rot, s_cam_editor.s_cuts[i].s_nodes[j].v_rot)
					str_speed = "pos speed = "
					str_rot_speed = "rot speed = "
					
					IF f_time = 0.0
						str_speed += "instant"
						str_rot_speed += "instant"
					ELSE
						f_speed = f_dist / f_time
						f_rot_speed = f_rot_diff / f_time
						str_speed += GET_STRING_FROM_FLOAT(f_speed)
						str_rot_speed += GET_STRING_FROM_FLOAT(f_rot_speed)
					ENDIF
					
					DRAW_DEBUG_TEXT(str_rot_speed, v_mid + <<0.0, 0.0, 1.0>>, r, g, b, 255)
					DRAW_DEBUG_TEXT(str_speed, v_mid + <<0.0, 0.0, 1.5>>, r, g, b, 255)	
				ENDIF
			ENDIF
		ENDFOR
		
		//Draw the lookat nodes for this cut.
		FOR j = 0 TO MAX_LOOKAT_NODES - 1 STEP 1
			IF s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.x != 0.0
			AND s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.y != 0.0
				IF s_cam_editor.i_current_lookat_node = j AND s_cam_editor.i_current_cut = i
					r = 255
					g = 0
					b = 0
				ELSE
					r = 0
					g = 0
					b = 255
				ENDIF
			
				TEXT_LABEL str_index = ""
				str_index += j
			
				DRAW_DEBUG_TEXT(str_index, s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos + <<0.0, 0.0, 1.0>>, r, g, b, 255)
				DRAW_DEBUG_SPHERE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos, 0.25, r, g, b, 200)
				
				IF j > 0
					DRAW_DEBUG_LINE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos, s_cam_editor.s_cuts[i].s_lookat_nodes[j-1].v_pos, 0, 128, 0, 255)
					
					//Mid-point (this is where the speed will be drawn)
					v_mid = s_cam_editor.s_cuts[i].s_lookat_nodes[j-1].v_pos + ((s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos - s_cam_editor.s_cuts[i].s_lookat_nodes[j-1].v_pos) * 0.5)
					f_time = TO_FLOAT(s_cam_editor.s_cuts[i].s_lookat_nodes[j].i_duration) / 1000.0
					f_dist = VDIST(s_cam_editor.s_cuts[i].s_lookat_nodes[j-1].v_pos, s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos)
					str_speed = "speed = "
					
					IF f_time = 0.0
						str_speed += "instant"
					ELSE
						f_speed = f_dist / f_time
						str_speed += GET_STRING_FROM_FLOAT(f_speed)
					ENDIF
					
					DRAW_DEBUG_TEXT(str_speed, v_mid + <<0.0, 0.0, 1.0>>, r, g, b, 255)	
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	
	IF DOES_CAM_EXIST(s_cam_editor.cam_main)
		DRAW_CAMERA_BOX(s_cam_editor, GET_CAM_COORD(s_cam_editor.cam_main), GET_CAM_ROT(s_cam_editor.cam_main), 0, 255, 0, 255)
	ENDIF
	
	IF DOES_CAM_EXIST(s_cam_editor.cam_lookat)
		DRAW_DEBUG_SPHERE(GET_CAM_COORD(s_cam_editor.cam_lookat), 0.25, 0, 255, 0, 255)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(s_cam_editor.veh_to_sync_with)
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(s_cam_editor.veh_to_sync_with), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_cam_editor.veh_to_sync_with, <<0.0, 0.0, 2.0>>), 255, 0, 0,255)
	ENDIF
ENDPROC

PROC UPDATE_CAM_PLAYBACK()
	IF s_cam_editor.i_playback_current_event <= MAX_CUTS
		BOOL b_trigger_next_cut = FALSE
		BOOL b_cutscene_has_ended = FALSE
		
		//Camera will cut if the following conditions are met:
		// - The current cut contains camera information.
		// - Thre previous cut has finished (or this is the first cut).
		//The cutscene will end if the following conditions are met:
		// - The current cut has no camera information, or the end of the array has been reached.
		// - The previous cut has finished (or this is the first cut).
		IF s_cam_editor.i_playback_current_event = 0
			IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].f_fov != 0
				b_trigger_next_cut = TRUE
			ELSE
				b_cutscene_has_ended = TRUE
			ENDIF
		ELIF s_cam_editor.i_playback_current_event < MAX_CUTS
			IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event - 1].i_duration < TIMERB()
				IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].f_fov != 0
					b_trigger_next_cut = TRUE
				ELSE
					b_cutscene_has_ended = TRUE
				ENDIF
			ENDIF
		ELIF s_cam_editor.i_playback_current_event = MAX_CUTS
			IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event - 1].i_duration < TIMERB()
				b_cutscene_has_ended = TRUE
			ENDIF
		ENDIF
	
		IF b_trigger_next_cut
			DESTROY_ALL_CAMS()
			
			//If vehicle recording syncing is enabled, set up the vehicle playback to the correct time.
			IF s_cam_editor.i_playback_current_event = 0
				IF s_cam_editor.b_sync_recording
					IF IS_VEHICLE_DRIVEABLE(s_cam_editor.veh_to_sync_with)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(s_cam_editor.veh_to_sync_with)
							SET_TIME_IN_PLAYBACK_RECORDED_VEHICLE(s_cam_editor.veh_to_sync_with, s_cam_editor.f_playback_sync_time)
							UNPAUSE_PLAYBACK_RECORDED_VEHICLE(s_cam_editor.veh_to_sync_with)
							
							WAIT(0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Create cam based on the given type for this cut
			IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_type = CUT_TYPE_STATIC
				s_cam_editor.cam_main = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].v_pos,
																						s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].v_rot,
																						s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].f_fov, TRUE)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_type = CUT_TYPE_INTERP
				s_cam_editor.cam_main = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].v_pos,
																						s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].v_rot,
																						s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[0].f_fov, TRUE)
				
				SET_CAM_PARAMS(s_cam_editor.cam_main, s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].v_pos,
													s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].v_rot,
													s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].f_fov,
													s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].i_duration,
													GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].i_pos_graph_type), 
													GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[1].i_rot_graph_type))
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_type = CUT_TYPE_SPLINE
				s_cam_editor.cam_main = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
		
				IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 1
					SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_main, CAM_SPLINE_NO_SMOOTH)
				ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 2
					SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_main, CAM_SPLINE_SLOW_IN_SMOOTH)
				ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 3
					SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_main, CAM_SPLINE_SLOW_OUT_SMOOTH)
				ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 4
					SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_main, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
				ENDIF
				
				INT i
				FOR i = 0 TO MAX_NODES_PER_CUT - 1 STEP 1
					IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].f_fov > 0.0
						s_cam_editor.cam_nodes[i] = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].v_pos,
																									s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].v_rot,
																									s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].f_fov, TRUE)
																									
						IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].i_spline_node_flags = 0 //Default flags, no need to set param
							ADD_CAM_SPLINE_NODE_USING_CAMERA(s_cam_editor.cam_main, s_cam_editor.cam_nodes[i], 
															 s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].i_duration)
						ELSE
							ADD_CAM_SPLINE_NODE_USING_CAMERA(s_cam_editor.cam_main, s_cam_editor.cam_nodes[i], 
															s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].i_duration,
															GET_SPLINE_NODE_FLAG_FROM_INT(s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_nodes[i].i_spline_node_flags))
						ENDIF
					ENDIF
				ENDFOR
				
				IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].b_use_lookat_spline
					s_cam_editor.cam_lookat = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
				
					IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 1
						SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_lookat, CAM_SPLINE_NO_SMOOTH)
					ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 2
						SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_lookat, CAM_SPLINE_SLOW_IN_SMOOTH)
					ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 3
						SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_lookat, CAM_SPLINE_SLOW_OUT_SMOOTH)
					ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_spline_smooth_type = 4
						SET_CAM_SPLINE_SMOOTHING_STYLE(s_cam_editor.cam_lookat, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
					ENDIF
				
					FOR i = 0 TO MAX_LOOKAT_NODES - 1 STEP 1
						IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_lookat_nodes[i].v_pos.x != 0.0
						AND s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_lookat_nodes[i].v_pos.y != 0.0
							ADD_CAM_SPLINE_NODE(s_cam_editor.cam_lookat, s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_lookat_nodes[i].v_pos,
																		 <<0.0, 0.0, 0.0>>,
																		 s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].s_lookat_nodes[i].i_duration)
						ENDIF
					ENDFOR
					
					POINT_CAM_AT_CAM(s_cam_editor.cam_main, s_cam_editor.cam_lookat)
					SET_CAM_ACTIVE(s_cam_editor.cam_lookat, TRUE)
				ENDIF
			ENDIF
			
			//Shake (if needed)
			IF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_ROAD_VIBRATION
				SHAKE_CAM(s_cam_editor.cam_main, "ROAD_VIBRATION_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_HAND
				SHAKE_CAM(s_cam_editor.cam_main, "HAND_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_SKY_DIVING
				SHAKE_CAM(s_cam_editor.cam_main, "SKY_DIVING_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_FIRST_PERSON
				SHAKE_CAM(s_cam_editor.cam_main, "FIRST_PERSON_AIM_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_WOBBLY
				SHAKE_CAM(s_cam_editor.cam_main, "WOBBLY_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ELIF s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].i_shake_type = SHAKE_TYPE_VIBRATE
				SHAKE_CAM(s_cam_editor.cam_main, "VIBRATE_SHAKE", s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_shake_amplitude)
			ENDIF
			
			//Motion blur
			SET_CAM_MOTION_BLUR_STRENGTH(s_cam_editor.cam_main, s_cam_editor.s_cuts[s_cam_editor.i_playback_current_event].f_blur_strength)
			
			SET_CAM_ACTIVE(s_cam_editor.cam_main, TRUE)
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			IF NOT IS_CAM_RENDERING(s_cam_editor.cam_main)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			SETTIMERB(0)
			s_cam_editor.i_playback_current_event++
		ENDIF
		
		IF b_cutscene_has_ended
			DESTROY_ALL_CAMS()
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			
			IF s_cam_editor.i_interp_to_game_cam_duration = 0
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ELSE
				RENDER_SCRIPT_CAMS(FALSE, TRUE, s_cam_editor.i_interp_to_game_cam_duration)
			ENDIF
			
			s_cam_editor.i_playback_current_event = 100
		ELSE
			//SET_USE_HI_DOF()
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_CAM_DATA()
	INT i = 0
	INT j = 0

	REPEAT MAX_CUTS i
		REPEAT MAX_NODES_PER_CUT j
			s_cam_editor.s_cuts[i].s_nodes[j].f_fov = 0.0
		ENDREPEAT
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Uses the current data stored in the given node (from the given cut) to overwrite the current node data stored by the widgets.
PROC REPLACE_WIDGET_NODE_DATA_WITH_CUT_NODE_DATA(INT i_cut_containing_node, INT i_node_to_replace)
	s_cam_editor.v_current_node_pos = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].v_pos
	s_cam_editor.v_current_node_rot = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].v_rot
	s_cam_editor.f_current_node_fov = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].f_fov
	s_cam_editor.i_current_node_duration = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_duration
	s_cam_editor.i_current_node_interp_pos_graph = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_pos_graph_type
	s_cam_editor.i_current_node_interp_rot_graph = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_rot_graph_type
	s_cam_editor.i_current_node_spline_smooth = s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_spline_node_flags	
ENDPROC

/// PURPOSE:
///    Uses the current node data stored by the widgets to replace the current data stored in the given node (from the given cut).
PROC REPLACE_CUT_NODE_DATA_WITH_WIDGET_NODE_DATA(INT i_cut_containing_node, INT i_node_to_replace)
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].v_pos = s_cam_editor.v_current_node_pos
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].v_rot = s_cam_editor.v_current_node_rot
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].f_fov = s_cam_editor.f_current_node_fov
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_duration = s_cam_editor.i_current_node_duration
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_pos_graph_type = s_cam_editor.i_current_node_interp_pos_graph
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_rot_graph_type = s_cam_editor.i_current_node_interp_rot_graph
	s_cam_editor.s_cuts[i_cut_containing_node].s_nodes[i_node_to_replace].i_spline_node_flags = s_cam_editor.i_current_node_spline_smooth
ENDPROC

PROC REPLACE_WIDGET_LOOKAT_NODE_DATA_WITH_CUT_LOOKAT_NODE_DATA(INT i_cut_containing_node, INT i_node_to_replace)
	s_cam_editor.v_current_lookat_node_pos = s_cam_editor.s_cuts[i_cut_containing_node].s_lookat_nodes[i_node_to_replace].v_pos
	s_cam_editor.i_current_lookat_duration = s_cam_editor.s_cuts[i_cut_containing_node].s_lookat_nodes[i_node_to_replace].i_duration
ENDPROC

PROC REPLACE_CUT_LOOKAT_NODE_DATA_WITH_WIDGET_LOOKAT_NODE_DATA(INT i_cut_containing_node, INT i_node_to_replace)
	s_cam_editor.s_cuts[i_cut_containing_node].s_lookat_nodes[i_node_to_replace].v_pos = s_cam_editor.v_current_lookat_node_pos
	s_cam_editor.s_cuts[i_cut_containing_node].s_lookat_nodes[i_node_to_replace].i_duration = s_cam_editor.i_current_lookat_duration
ENDPROC

/// PURPOSE:
///    Uses the current data stored in the widgets to overwrite the data for the given cut.
PROC REPLACE_CUT_DATA_WITH_WIDGET_DATA(INT i_cut_to_replace)
	s_cam_editor.s_cuts[i_cut_to_replace].i_duration = s_cam_editor.i_current_cut_duration
	s_cam_editor.s_cuts[i_cut_to_replace].i_type = s_cam_editor.i_current_cut_type
	s_cam_editor.s_cuts[i_cut_to_replace].i_shake_type = s_cam_editor.i_current_cut_shake_type
	s_cam_editor.s_cuts[i_cut_to_replace].f_shake_amplitude = s_cam_editor.f_current_cut_shake_amplitude
	s_cam_editor.s_cuts[i_cut_to_replace].f_blur_strength = s_cam_editor.f_current_cut_blur_strength
	s_cam_editor.s_cuts[i_cut_to_replace].i_spline_smooth_type = s_cam_editor.i_current_cut_smooth_style
	s_cam_editor.s_cuts[i_cut_to_replace].b_use_lookat_spline = s_cam_editor.b_current_lookat_spline_active
	
	REPLACE_CUT_NODE_DATA_WITH_WIDGET_NODE_DATA(i_cut_to_replace, s_cam_editor.i_current_node)
	REPLACE_CUT_LOOKAT_NODE_DATA_WITH_WIDGET_LOOKAT_NODE_DATA(i_cut_to_replace, s_cam_editor.i_current_lookat_node)
ENDPROC

/// PURPOSE:
///    Uses the current data stored in the given cut to overwrite the data stored in the widgets.
PROC REPLACE_WIDGET_DATA_WITH_CUT_DATA(INT i_cut_to_replace)
	s_cam_editor.i_current_cut_duration = s_cam_editor.s_cuts[i_cut_to_replace].i_duration
	s_cam_editor.i_current_cut_type = s_cam_editor.s_cuts[i_cut_to_replace].i_type
	s_cam_editor.i_current_cut_shake_type = s_cam_editor.s_cuts[i_cut_to_replace].i_shake_type
	s_cam_editor.f_current_cut_shake_amplitude = s_cam_editor.s_cuts[i_cut_to_replace].f_shake_amplitude
	s_cam_editor.f_current_cut_blur_strength = s_cam_editor.s_cuts[i_cut_to_replace].f_blur_strength
	s_cam_editor.i_current_cut_smooth_style = s_cam_editor.s_cuts[i_cut_to_replace].i_spline_smooth_type
	s_cam_editor.b_current_lookat_spline_active = s_cam_editor.s_cuts[i_cut_to_replace].b_use_lookat_spline
	
	REPLACE_WIDGET_NODE_DATA_WITH_CUT_NODE_DATA(i_cut_to_replace, s_cam_editor.i_current_node)
	REPLACE_WIDGET_LOOKAT_NODE_DATA_WITH_CUT_LOOKAT_NODE_DATA(i_cut_to_replace, s_cam_editor.i_current_lookat_node)
ENDPROC



PROC SAVE_CAM_DATA_TO_DEBUG()
	INT i = 0
	TEXT_LABEL str_indentation = ""

	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("--------------------------- CAM EDITOR OUTPUT -----------------------------")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("SWITCH i_current_event")
	str_indentation = "	" //NOTE: The string contains tabs, not spaces.
	
	WHILE i < MAX_CUTS
		IF s_cam_editor.s_cuts[i].s_nodes[0].f_fov != 0
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(str_indentation)
			SAVE_STRING_TO_DEBUG_FILE("CASE ")
			SAVE_INT_TO_DEBUG_FILE(i)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			str_indentation = "		"
			
			//Timer check: don't do for the first cut
			IF i > 0
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("IF TIMERB() > ")
				SAVE_INT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i-1].i_duration)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str_indentation = "			"
			ENDIF
			
			//Destroy existing cams
			SAVE_STRING_TO_DEBUG_FILE(str_indentation)
			SAVE_STRING_TO_DEBUG_FILE("DESTROY_ALL_CAMS()")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			//Create cam depending on type
			IF s_cam_editor.s_cuts[i].i_type = CUT_TYPE_STATIC
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("cam_main = CREATE_CAM_WITH_PARAMS(\"DEFAULT_SCRIPTED_CAMERA\", ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].v_pos)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].v_rot)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].f_fov)
				SAVE_STRING_TO_DEBUG_FILE(", TRUE)")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELIF s_cam_editor.s_cuts[i].i_type = CUT_TYPE_INTERP
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("cam_main = CREATE_CAM_WITH_PARAMS(\"DEFAULT_SCRIPTED_CAMERA\", ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].v_pos)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].v_rot)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[0].f_fov)
				SAVE_STRING_TO_DEBUG_FILE(", TRUE)")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SET_CAM_PARAMS(cam_main, ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[1].v_pos)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[1].v_rot)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[1].f_fov)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_INT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[1].i_duration)
				SAVE_STRING_TO_DEBUG_FILE(", ")			
				
				IF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_pos_graph_type) = GRAPH_TYPE_SIN_ACCEL_DECEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_SIN_ACCEL_DECEL, ")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_pos_graph_type) = GRAPH_TYPE_ACCEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_ACCEL, ")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_pos_graph_type) = GRAPH_TYPE_DECEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_DECEL, ")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_pos_graph_type) = GRAPH_TYPE_LINEAR
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_LINEAR, ")
				ENDIF
				
				IF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_rot_graph_type) = GRAPH_TYPE_SIN_ACCEL_DECEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_SIN_ACCEL_DECEL)")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_rot_graph_type) = GRAPH_TYPE_ACCEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_ACCEL)")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_rot_graph_type) = GRAPH_TYPE_DECEL
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_DECEL)")
				ELIF GET_CAMERA_GRAPH_TYPE_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[1].i_rot_graph_type) = GRAPH_TYPE_LINEAR
					SAVE_STRING_TO_DEBUG_FILE("GRAPH_TYPE_LINEAR)")
				ENDIF	
			
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELIF s_cam_editor.s_cuts[i].i_type = CUT_TYPE_SPLINE
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("cam_main = CREATE_CAM(\"DEFAULT_SPLINE_CAMERA\")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
	
				//Smooth style
				IF s_cam_editor.s_cuts[i].i_spline_smooth_type != 0
					SAVE_STRING_TO_DEBUG_FILE(str_indentation)
					
					IF s_cam_editor.s_cuts[i].i_spline_smooth_type = 1
						SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_main, CAM_SPLINE_NO_SMOOTH)")
					ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 2
						SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_main, CAM_SPLINE_SLOW_IN_SMOOTH)")
					ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 3
						SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_main, CAM_SPLINE_SLOW_OUT_SMOOTH)")
					ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 4
						SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_main, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)")
					ENDIF
					
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				//Spline nodes: cam node is created inside the parameters
				INT j = 0
				WHILE j < MAX_NODES_PER_CUT
					IF s_cam_editor.s_cuts[i].s_nodes[j].f_fov > 0.0
						SAVE_STRING_TO_DEBUG_FILE(str_indentation)
						SAVE_STRING_TO_DEBUG_FILE("ADD_CAM_SPLINE_NODE_USING_CAMERA(cam_main, CREATE_CAM_WITH_PARAMS(\"DEFAULT_SCRIPTED_CAMERA\", ")
						SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[j].v_pos)
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[j].v_rot)
						SAVE_STRING_TO_DEBUG_FILE(", ")
						SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[j].f_fov)
						SAVE_STRING_TO_DEBUG_FILE(", TRUE), ")
						SAVE_INT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_nodes[j].i_duration)

						IF s_cam_editor.s_cuts[i].s_nodes[j].i_spline_node_flags = 0
							SAVE_STRING_TO_DEBUG_FILE(")")
						ELIF GET_SPLINE_NODE_FLAG_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[j].i_spline_node_flags) = CAM_SPLINE_NODE_SMOOTH_ROT
							SAVE_STRING_TO_DEBUG_FILE(", CAM_SPLINE_NODE_SMOOTH_ROT)")
						ELIF GET_SPLINE_NODE_FLAG_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[j].i_spline_node_flags) = CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS
							SAVE_STRING_TO_DEBUG_FILE(", CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS)")
						ELIF GET_SPLINE_NODE_FLAG_FROM_INT(s_cam_editor.s_cuts[i].s_nodes[j].i_spline_node_flags) = CAM_SPLINE_NODE_NO_FLAGS
							SAVE_STRING_TO_DEBUG_FILE(", CAM_SPLINE_NODE_NO_FLAGS)")
						ENDIF
						
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				
					j++
				ENDWHILE
				
				//Add the lookat spline if necessary
				IF s_cam_editor.s_cuts[i].b_use_lookat_spline
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE(str_indentation)
					SAVE_STRING_TO_DEBUG_FILE("cam_lookat = CREATE_CAM(\"DEFAULT_SPLINE_CAMERA\")")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					//Smooth style (this just matches whatever the main smooth style is).
					IF s_cam_editor.s_cuts[i].i_spline_smooth_type != 0
						SAVE_STRING_TO_DEBUG_FILE(str_indentation)
						
						IF s_cam_editor.s_cuts[i].i_spline_smooth_type = 1
							SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_lookat, CAM_SPLINE_NO_SMOOTH)")
						ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 2
							SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_lookat, CAM_SPLINE_SLOW_IN_SMOOTH)")
						ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 3
							SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_lookat, CAM_SPLINE_SLOW_OUT_SMOOTH)")
						ELIF s_cam_editor.s_cuts[i].i_spline_smooth_type = 4
							SAVE_STRING_TO_DEBUG_FILE("SET_CAM_SPLINE_SMOOTHING_STYLE(cam_lookat, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)")
						ENDIF
						
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
					
					j = 0
					WHILE j < MAX_LOOKAT_NODES
						IF s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.x != 0.0
						AND s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.y != 0.0
							SAVE_STRING_TO_DEBUG_FILE(str_indentation)
							SAVE_STRING_TO_DEBUG_FILE("ADD_CAM_SPLINE_NODE(cam_lookat, ")
							SAVE_VECTOR_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos)
							SAVE_STRING_TO_DEBUG_FILE(", <<0.0, 0.0, 0.0>>, ")
							SAVE_INT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].i_duration)
							SAVE_STRING_TO_DEBUG_FILE(")")
							
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
						
						j++
					ENDWHILE

					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE(str_indentation)
					SAVE_STRING_TO_DEBUG_FILE("POINT_CAM_AT_CAM(cam_main, cam_lookat)")
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE(str_indentation)
					SAVE_STRING_TO_DEBUG_FILE("SET_CAM_ACTIVE(cam_lookat, TRUE)")
				ENDIF
				
				//Activate main cam at the end
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SET_CAM_ACTIVE(cam_main, TRUE)")
				SAVE_NEWLINE_TO_DEBUG_FILE()			
			ENDIF
			
			//Shake (if needed)
			IF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_ROAD_VIBRATION
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"ROAD_VIBRATION_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ELIF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_HAND
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"HAND_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ELIF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_SKY_DIVING
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"SKY_DIVING_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ELIF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_FIRST_PERSON
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"FIRST_PERSON_AIM_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ELIF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_WOBBLY
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"WOBBLY_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ELIF s_cam_editor.s_cuts[i].i_shake_type = SHAKE_TYPE_VIBRATE
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SHAKE_CAM(cam_main, \"VIBRATE_SHAKE\", ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ENDIF			
			
			//Motion blur (if needed)
			IF s_cam_editor.s_cuts[i].f_blur_strength > 0.0
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("SET_CAM_MOTION_BLUR_STRENGTH(cam_main, ")
				SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i].f_blur_strength)
				SAVE_STRING_TO_DEBUG_FILE(")")
			ENDIF
			
			//Render cam
			IF i = 0
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("RENDER_SCRIPT_CAMS(TRUE, FALSE)")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			//Reset timer and increment case
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(str_indentation)
			SAVE_STRING_TO_DEBUG_FILE("SETTIMERB(0)")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(str_indentation)
			SAVE_STRING_TO_DEBUG_FILE("i_current_event++")
			SAVE_NEWLINE_TO_DEBUG_FILE()

			//ENDIF for the timer check
			IF i > 0
				str_indentation = "		"
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("ENDIF")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			str_indentation = "	"
			SAVE_STRING_TO_DEBUG_FILE(str_indentation)
			SAVE_STRING_TO_DEBUG_FILE("BREAK")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ELIF i > 0
			//Cleanup case: once previous cut is done clean up all of the cameras
			IF s_cam_editor.s_cuts[i-1].s_nodes[0].f_fov != 0
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("CASE ")
				SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str_indentation = "		"
				
				//Timer check
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("IF TIMERB() > ")
				SAVE_INT_TO_DEBUG_FILE(s_cam_editor.s_cuts[i-1].i_duration)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				str_indentation = "			"
				
				//Destroy existing cams
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("DESTROY_ALL_CAMS()")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				//Go back to game cam
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				IF s_cam_editor.i_interp_to_game_cam_duration = 0
					SAVE_STRING_TO_DEBUG_FILE("RENDER_SCRIPT_CAMS(FALSE, FALSE)")
				ELSE
					SAVE_STRING_TO_DEBUG_FILE("RENDER_SCRIPT_CAMS(FALSE, TRUE, ")
					SAVE_INT_TO_DEBUG_FILE(s_cam_editor.i_interp_to_game_cam_duration)
					SAVE_STRING_TO_DEBUG_FILE(")")
				ENDIF
				SAVE_NEWLINE_TO_DEBUG_FILE()
			
				str_indentation = "		"
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("ENDIF")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			
				str_indentation = "	"
				SAVE_STRING_TO_DEBUG_FILE(str_indentation)
				SAVE_STRING_TO_DEBUG_FILE("BREAK")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
		ENDIF
		
		i++
	ENDWHILE

	SAVE_STRING_TO_DEBUG_FILE("ENDSWITCH")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("--------------------------------------------------------------------------------")
	SAVE_NEWLINE_TO_DEBUG_FILE()

	s_cam_editor.b_output_data = FALSE

	/*IF s_cam_editor.b_sync_recording
		SAVE_STRING_TO_DEBUG_FILE("Playback start time = ")
		SAVE_FLOAT_TO_DEBUG_FILE(s_cam_editor.f_playback_time)
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDIF

	s_cam_editor.b_output_data = FALSE*/
ENDPROC

PROC SAVE_CAM_DATA_TO_XML()
	//XML FORMAT:
	//<root>
	//	//For each cut:
	//	<cut cut_id = "x" i_type = "x" i_shake_type = "x" f_shake_amp = "x" i_duration = "x" i_smooth_style = "x">	
	//		//For each node:
	//		<node node_id = "x" pos_x = "x" pos_y = "x" pos_z = "x" rot_x = "x" rot_y = "x" rot_z = "x" f_fov = "x" i_duration = "x" pos_graph = "x" rot_graph = "x" node_smooth = "x" />
	//	</cut>
	//</root>
	INT i = 0
	INT j = 0
	
	TEXT_LABEL_63 str_path = "X:\\"	
	TEXT_LABEL_63 str_file = GET_CONTENTS_OF_TEXT_WIDGET(s_cam_editor.widget_save_name)
	str_file += ".xml"
	STRING str_indent = ""
	
	CLEAR_NAMED_DEBUG_FILE(str_path, str_file)
	OPEN_NAMED_DEBUG_FILE(str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<camera_sequence>", str_path, str_file)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
		str_indent = "	"
	
		SAVE_STRING_TO_NAMED_DEBUG_FILE(str_indent, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("<game_interp duration = \"", str_path, str_file)
		SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.i_interp_to_game_cam_duration, str_path, str_file)
		SAVE_STRING_TO_NAMED_DEBUG_FILE("\" />", str_path, str_file)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
	
		REPEAT MAX_CUTS i
			IF s_cam_editor.s_cuts[i].s_nodes[0].f_fov != 0
				SAVE_STRING_TO_NAMED_DEBUG_FILE(str_indent, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("<cut cut_id = \"", str_path, str_file)
				SAVE_INT_TO_NAMED_DEBUG_FILE(i, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_type = \"", str_path, str_file)
				SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].i_type, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_shake_type = \"", str_path, str_file)
				SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].i_shake_type, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" f_shake_amp = \"", str_path, str_file)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].f_shake_amplitude, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" f_blur_strength = \"", str_path, str_file)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].f_blur_strength, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_duration = \"", str_path, str_file)
				SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].i_duration, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_smooth_style = \"", str_path, str_file)
				SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].i_spline_smooth_type, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(" b_use_lookat_spline = \"", str_path, str_file)
				IF s_cam_editor.s_cuts[i].b_use_lookat_spline
					SAVE_STRING_TO_NAMED_DEBUG_FILE("true", str_path, str_file)
				ELSE
					SAVE_STRING_TO_NAMED_DEBUG_FILE("false", str_path, str_file)
				ENDIF
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\">", str_path, str_file)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
				
				str_indent = "		"
				
				REPEAT MAX_NODES_PER_CUT j
					IF s_cam_editor.s_cuts[i].s_nodes[j].f_fov != 0
						SAVE_STRING_TO_NAMED_DEBUG_FILE(str_indent, str_path, str_file)
						
						SAVE_CAM_EDITOR_NODE_DATA_TO_NAMED_DEBUG_FILE(j, 
								s_cam_editor.s_cuts[i].s_nodes[j].v_pos,
								s_cam_editor.s_cuts[i].s_nodes[j].v_rot,
								s_cam_editor.s_cuts[i].s_nodes[j].f_fov,
								s_cam_editor.s_cuts[i].s_nodes[j].i_duration,
								s_cam_editor.s_cuts[i].s_nodes[j].i_pos_graph_type,
								s_cam_editor.s_cuts[i].s_nodes[j].i_rot_graph_type,
								s_cam_editor.s_cuts[i].s_nodes[j].i_spline_node_flags,
								str_path, str_file)

						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
					ENDIF
				ENDREPEAT
				
				REPEAT MAX_LOOKAT_NODES j
					IF s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.x != 0.0
					AND s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.y != 0.0
						SAVE_STRING_TO_NAMED_DEBUG_FILE(str_indent, str_path, str_file)
						
						SAVE_STRING_TO_NAMED_DEBUG_FILE("<lookat_node lookat_node_id = \"", str_path, str_file)
						SAVE_INT_TO_NAMED_DEBUG_FILE(j, str_path, str_file)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
						
						SAVE_STRING_TO_NAMED_DEBUG_FILE(" lookat_pos_x = \"", str_path, str_file)
						SAVE_FLOAT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.x, str_path, str_file)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
						
						SAVE_STRING_TO_NAMED_DEBUG_FILE(" lookat_pos_y = \"", str_path, str_file)
						SAVE_FLOAT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.y, str_path, str_file)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
						
						SAVE_STRING_TO_NAMED_DEBUG_FILE(" lookat_pos_z = \"", str_path, str_file)
						SAVE_FLOAT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].v_pos.z, str_path, str_file)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", str_path, str_file)
						
						SAVE_STRING_TO_NAMED_DEBUG_FILE(" i_lookat_duration = \"", str_path, str_file)
						SAVE_INT_TO_NAMED_DEBUG_FILE(s_cam_editor.s_cuts[i].s_lookat_nodes[j].i_duration, str_path, str_file)
						SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>", str_path, str_file)

						SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
					ENDIF
				ENDREPEAT
				
				str_indent = "	"
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE(str_indent, str_path, str_file)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("</cut>", str_path, str_file)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
			ENDIF
		ENDREPEAT
		
		SAVE_STRING_TO_NAMED_DEBUG_FILE("</camera_sequence>", str_path, str_file)
		SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(str_path, str_file)
	CLOSE_DEBUG_FILE()
	
	s_cam_editor.b_save_to_xml = FALSE
ENDPROC

PROC LOAD_CAM_DATA_FROM_XML()
	TEXT_LABEL_63 str_path = "X:\\"
	str_path += GET_CONTENTS_OF_TEXT_WIDGET(s_cam_editor.widget_save_name)
	str_path += ".xml"
	
	INT i_hash_game_interp_id = GET_HASH_KEY("duration")
	INT i_hash_cut_id = GET_HASH_KEY("cut_id")
	INT i_hash_node_id = GET_HASH_KEY("node_id")
	INT i_hash_type = GET_HASH_KEY("i_type")
	INT i_hash_shake_type = GET_HASH_KEY("i_shake_type")
	INT i_hash_shake_amp = GET_HASH_KEY("f_shake_amp")
	INT i_hash_blur_strength = GET_HASH_KEY("f_blur_strength")
	INT i_hash_duration = GET_HASH_KEY("i_duration")
	INT i_hash_smooth_style = GET_HASH_KEY("i_smooth_style")
	INT i_hash_pos_x = GET_HASH_KEY("pos_x")
	INT i_hash_pos_y = GET_HASH_KEY("pos_y")
	INT i_hash_pos_z = GET_HASH_KEY("pos_z")
	INT i_hash_rot_x = GET_HASH_KEY("rot_x")
	INT i_hash_rot_y = GET_HASH_KEY("rot_y")
	INT i_hash_rot_z = GET_HASH_KEY("rot_z")
	INT i_hash_fov = GET_HASH_KEY("f_fov")
	INT i_hash_node_duration = GET_HASH_KEY("i_node_duration")
	INT i_hash_pos_graph = GET_HASH_KEY("i_pos_graph")
	INT i_hash_rot_graph = GET_HASH_KEY("i_rot_graph")
	INT i_hash_node_smooth = GET_HASH_KEY("i_node_smooth")
	INT i_hash_use_lookat = GET_HASH_KEY("b_use_lookat_spline")
	INT i_hash_lookat_node_id = GET_HASH_KEY("lookat_node_id")
	INT i_hash_lookat_pos_x = GET_HASH_KEY("lookat_pos_x")
	INT i_hash_lookat_pos_y = GET_HASH_KEY("lookat_pos_y")
	INT i_hash_lookat_pos_z = GET_HASH_KEY("lookat_pos_z")
	INT i_hash_lookat_node_duration = GET_HASH_KEY("i_lookat_duration")
	
	
	IF LOAD_XML_FILE(str_path)
		INT i_num_nodes = GET_NUMBER_OF_XML_NODES()
	
		IF i_num_nodes != 0
			INT i = 0
			INT i_current_cut = 0
			INT i_current_node = 0
			
			//First clear the current data
			CLEAR_CAM_DATA()
			
			REPEAT i_num_nodes i
				STRING str_name = GET_XML_NODE_NAME()
				INT i_num_attributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
				INT i_name_hash
				INT j = 0
			
				//Three types of tags in the XML file: cuts, nodes, and the game interp node. Cuts and nodes hold all of the attributes for a particular 
				//camera cut or a single node within a cut. The game interp node contains the duration for interping back to the game cam.
				IF ARE_STRINGS_EQUAL(str_name, "game_interp")
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_game_interp_id
							s_cam_editor.i_interp_to_game_cam_duration = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
				ELIF ARE_STRINGS_EQUAL(str_name, "cut")
					//Get the current cut first: the attributes seem to be read in alphabetical order, but we need to guarantee the cut index is up to date first.
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_cut_id
							i_current_cut = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
					
					//Read in the cut attributes
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_type
							s_cam_editor.s_cuts[i_current_cut].i_type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_shake_type
							s_cam_editor.s_cuts[i_current_cut].i_shake_type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_shake_amp
							s_cam_editor.s_cuts[i_current_cut].f_shake_amplitude = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_blur_strength
							s_cam_editor.s_cuts[i_current_cut].f_blur_strength = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_duration
							s_cam_editor.s_cuts[i_current_cut].i_duration = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_smooth_style
							s_cam_editor.s_cuts[i_current_cut].i_spline_smooth_type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_use_lookat
							IF ARE_STRINGS_EQUAL(GET_STRING_FROM_XML_NODE_ATTRIBUTE(j), "true")
								s_cam_editor.s_cuts[i_current_cut].b_use_lookat_spline = TRUE
							ELSE
								s_cam_editor.s_cuts[i_current_cut].b_use_lookat_spline = FALSE
							ENDIF
						ENDIF
					ENDREPEAT
				ELIF ARE_STRINGS_EQUAL(str_name, "node")
					//Get the current node: the attributes seem to be read in alphabetical order, but we need to guarantee the node index is up to date first.
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_node_id
							i_current_node = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
				
					//Read in the node attributes
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_pos_x
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_pos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_pos_y
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_pos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_pos_z
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_pos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_rot_x
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_rot.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_rot_y
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_rot.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_rot_z
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].v_rot.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_fov
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].f_fov = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_node_duration
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].i_duration = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_pos_graph
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].i_pos_graph_type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_rot_graph
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].i_rot_graph_type = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_node_smooth
							s_cam_editor.s_cuts[i_current_cut].s_nodes[i_current_node].i_spline_node_flags = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
				ELIF ARE_STRINGS_EQUAL(str_name, "lookat_node")
					//Get the current node: the attributes seem to be read in alphabetical order, but we need to guarantee the node index is up to date first.
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_lookat_node_id
							i_current_node = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
					
					//Read in the node attributes
					REPEAT i_num_attributes j	
						i_name_hash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(j))
						
						IF i_name_hash = i_hash_lookat_pos_x
							s_cam_editor.s_cuts[i_current_cut].s_lookat_nodes[i_current_node].v_pos.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_lookat_pos_y
							s_cam_editor.s_cuts[i_current_cut].s_lookat_nodes[i_current_node].v_pos.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_lookat_pos_z
							s_cam_editor.s_cuts[i_current_cut].s_lookat_nodes[i_current_node].v_pos.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(j)
						ELIF i_name_hash = i_hash_lookat_node_duration
							s_cam_editor.s_cuts[i_current_cut].s_lookat_nodes[i_current_node].i_duration = GET_INT_FROM_XML_NODE_ATTRIBUTE(j)
						ENDIF
					ENDREPEAT
				ENDIF

				GET_NEXT_XML_NODE()
			ENDREPEAT
			
			//Update the widget info: default back to the first cut and first node
			s_cam_editor.i_current_node = 0
			s_cam_editor.i_previous_node = 0
			
			s_cam_editor.i_current_cut = 0
			s_cam_editor.i_previous_cut = 0
			
			s_cam_editor.i_current_lookat_node = 0
			s_cam_editor.i_previous_lookat_node = 0
			
			REPLACE_WIDGET_DATA_WITH_CUT_DATA(s_cam_editor.i_current_cut)
		ELSE
			SCRIPT_ASSERT("Camera editor: XML file does not contain any camera information.")
		ENDIF
		
		DELETE_XML_FILE()
	ELSE
		SCRIPT_ASSERT("Camera editor: requested XML file could not be found.")
	ENDIF
	
	s_cam_editor.b_load_from_xml = FALSE
ENDPROC



PROC UPDATE_CAM_EDITOR()
	//If the cut type was changed, rebuild the widgets to remove any no longer needed options
	IF s_cam_editor.i_current_cut_type != s_cam_editor.i_previous_cut_type
		s_cam_editor.i_current_node = 0
	
		DESTROY_WIDGETS(TRUE)
		CREATE_WIDGETS(TRUE)
	ENDIF
	s_cam_editor.i_previous_cut_type = s_cam_editor.i_current_cut_type

	//helper object: makes it easier to draw debug lines for each node
	MODEL_NAMES model_helper = PROP_GOLF_BALL
	IF NOT DOES_ENTITY_EXIST(s_cam_editor.obj_helper)
		REQUEST_MODEL(model_helper)
		
		IF HAS_MODEL_LOADED(model_helper)
			s_cam_editor.obj_helper = CREATE_OBJECT_NO_OFFSET(model_helper, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			FREEZE_ENTITY_POSITION(s_cam_editor.obj_helper, TRUE)
			SET_ENTITY_COLLISION(s_cam_editor.obj_helper, FALSE)
			SET_ENTITY_VISIBLE(s_cam_editor.obj_helper, FALSE)
		ENDIF
	ENDIF		
	
	//Check if the user clicked on any vehicles with recordings playing
	ENTITY_INDEX entity_temp = GET_FOCUS_ENTITY_INDEX()
	IF entity_temp != NULL
		IF IS_ENTITY_A_VEHICLE(entity_temp)
			IF NOT IS_ENTITY_DEAD(entity_temp)
				INT i_search_flags = VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS
				i_search_flags |= VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING
				
				VEHICLE_INDEX veh_temp = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(entity_temp), 10.0, GET_ENTITY_MODEL(entity_temp), i_search_flags)
				 
				IF s_cam_editor.veh_to_sync_with != veh_temp
					s_cam_editor.veh_to_sync_with = veh_temp
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Update the array data of the current cut and node to match the widget values (only if the current node/cut wasn't changed this frame)
	IF s_cam_editor.i_previous_cut = s_cam_editor.i_current_cut
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_duration = s_cam_editor.i_current_cut_duration
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_type = s_cam_editor.i_current_cut_type
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_shake_type = s_cam_editor.i_current_cut_shake_type
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].f_shake_amplitude = s_cam_editor.f_current_cut_shake_amplitude
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].f_blur_strength = s_cam_editor.f_current_cut_blur_strength
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_spline_smooth_type = s_cam_editor.i_current_cut_smooth_style
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].b_use_lookat_spline = s_cam_editor.b_current_lookat_spline_active
		
		
		IF s_cam_editor.i_previous_node = s_cam_editor.i_current_node
			REPLACE_CUT_NODE_DATA_WITH_WIDGET_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_current_node)
		ENDIF
		
		IF s_cam_editor.i_previous_lookat_node = s_cam_editor.i_current_lookat_node
			REPLACE_CUT_LOOKAT_NODE_DATA_WITH_WIDGET_LOOKAT_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_current_lookat_node)
		ENDIF
	ENDIF

	//Add spline nodes when instructed by the user: nodes are added based on the current cam position
	IF s_cam_editor.b_add_node_at_current_cam_pos
		INT i_num_valid_nodes = 0
		INT i = 0
		BOOL b_added = FALSE
		
		//Work out how many nodes are valid: this varies depending on cut type (e.g. you shouldn't be allowed to add two cameras if the cut is for a single static shot)
		IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_type = CUT_TYPE_STATIC
			i_num_valid_nodes = 1
		ELIF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_type = CUT_TYPE_INTERP
			i_num_valid_nodes = 2
		ELIF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].i_type = CUT_TYPE_SPLINE
			i_num_valid_nodes = MAX_NODES_PER_CUT
		ENDIF
		
		WHILE i < i_num_valid_nodes AND NOT b_added
			IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].f_fov = 0.0
				CAMERA_INDEX cam_debug = GET_DEBUG_CAM()
				
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].v_pos = GET_CAM_COORD(cam_debug)
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].v_rot = GET_CAM_ROT(cam_debug)
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].f_fov = GET_CAM_FOV(cam_debug)
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].i_duration = 0
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].i_pos_graph_type = 0
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].i_rot_graph_type = 0 
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].i_spline_node_flags = 0 
				
				//Set the current selected node to be the one that was just added
				REPLACE_WIDGET_NODE_DATA_WITH_CUT_NODE_DATA(s_cam_editor.i_current_cut, i)
				
				s_cam_editor.i_current_node = i
				s_cam_editor.i_previous_node = i //This prevents data being overwritten later on
				
				b_added = TRUE
			ENDIF
		
			i++
		ENDWHILE
		
		IF NOT b_added
			SCRIPT_ASSERT("Not enough free nodes left for this camera cut.")
		ENDIF
		
		s_cam_editor.b_add_node_at_current_cam_pos = FALSE
	ENDIF
	
	//Replace the current node with the debug cam params when instructed (NOTE: the additional params remain unchanged)
	IF s_cam_editor.b_replace_node_at_current_cam_pos
		CAMERA_INDEX cam_debug = GET_DEBUG_CAM()
		
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_pos = GET_CAM_COORD(cam_debug)
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_rot = GET_CAM_ROT(cam_debug)
		s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].f_fov = GET_CAM_FOV(cam_debug)
		
		s_cam_editor.v_current_node_pos = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_pos
		s_cam_editor.v_current_node_rot = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_rot
		s_cam_editor.f_current_node_fov = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].f_fov
		
		s_cam_editor.b_replace_node_at_current_cam_pos = FALSE
	ENDIF
	
	//Remove the currently selected spline node if instructed by the user and shift any future nodes to fill the gap.
	IF s_cam_editor.b_delete_current_node
		INT i = s_cam_editor.i_current_node + 1
		WHILE i < MAX_NODES_PER_CUT
			IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i].f_fov != 0.0
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i-1] = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i]
			ELSE
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[i-1] = GET_EMPTY_NODE()
			ENDIF
			
			//Replace the widget info to point to the node that replaces the deleted one
			IF i-1 = s_cam_editor.i_current_node
				REPLACE_WIDGET_NODE_DATA_WITH_CUT_NODE_DATA(s_cam_editor.i_current_cut, i-1)
			ENDIF
			
			i++
		ENDWHILE
		
		s_cam_editor.b_delete_current_node = FALSE
	ENDIF
	
	//Add lookat nodes when instructed.
	IF s_cam_editor.b_add_lookat_node_at_current_cam_pos
		INT i = 0
		BOOL b_added = FALSE
		
		WHILE i < MAX_LOOKAT_NODES AND NOT b_added
			IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos.x = 0.0
			AND s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos.y = 0.0
				CAMERA_INDEX cam_debug = GET_DEBUG_CAM()
				VECTOR v_cam_pos = GET_CAM_COORD(cam_debug)
				
				IF s_cam_editor.b_try_to_replicate_cam_rotation
					VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_CAM_ROT(cam_debug))
					
					s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos = v_cam_pos + (v_dir * 20.0)
				ELSE
					s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos = v_cam_pos
				ENDIF
				
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].i_duration = 0
				
				//Set the current selected node to be the one that was just added
				REPLACE_WIDGET_LOOKAT_NODE_DATA_WITH_CUT_LOOKAT_NODE_DATA(s_cam_editor.i_current_cut, i)
				
				s_cam_editor.i_current_lookat_node = i
				s_cam_editor.i_previous_lookat_node = i //This prevents data being overwritten later on
				
				b_added = TRUE
			ENDIF
		
			i++
		ENDWHILE
		
		IF NOT b_added
			SCRIPT_ASSERT("Not enough free lookat nodes left for this camera cut.")
		ENDIF
	
		s_cam_editor.b_add_lookat_node_at_current_cam_pos = FALSE
	ENDIF
	
	//Replace the current lookat node when instructed.
	IF s_cam_editor.b_replace_lookat_node_at_current_cam_pos
		CAMERA_INDEX cam_debug = GET_DEBUG_CAM()
		VECTOR v_cam_pos = GET_CAM_COORD(cam_debug)
		
		IF s_cam_editor.b_try_to_replicate_cam_rotation
			VECTOR v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_CAM_ROT(cam_debug))
			
			s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[s_cam_editor.i_current_lookat_node].v_pos = v_cam_pos + (v_dir * 20.0)
		ELSE
			s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[s_cam_editor.i_current_lookat_node].v_pos  = v_cam_pos
		ENDIF
		
		s_cam_editor.v_current_lookat_node_pos = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[s_cam_editor.i_current_lookat_node].v_pos
		s_cam_editor.b_replace_lookat_node_at_current_cam_pos = FALSE
	ENDIF
	
	//Delete the current lookat node when instructed.
	IF s_cam_editor.b_delete_current_lookat_node
		INT i = s_cam_editor.i_current_lookat_node + 1
		WHILE i < MAX_LOOKAT_NODES
			IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos.x != 0.0
			AND s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i].v_pos.y != 0.0
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i-1] = s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i]
			ELSE
				s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_lookat_nodes[i-1] = GET_EMPTY_LOOKAT_NODE()
			ENDIF
			
			//Replace the widget info to point to the node that replaces the deleted one
			IF i-1 = s_cam_editor.i_current_lookat_node
				REPLACE_WIDGET_LOOKAT_NODE_DATA_WITH_CUT_LOOKAT_NODE_DATA(s_cam_editor.i_current_cut, i-1)
			ENDIF
			
			i++
		ENDWHILE
		
		s_cam_editor.b_delete_current_lookat_node = FALSE
	ENDIF
	
	//Set the debug cam to be the same as the current spline node if instructed
	IF s_cam_editor.b_view_node
		IF s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].f_fov > 0.0
			//Clean up any spline currently active
			DESTROY_ALL_CAMS()
			s_cam_editor.cam_main = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_pos,
																						s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].v_rot,
																						s_cam_editor.s_cuts[s_cam_editor.i_current_cut].s_nodes[s_cam_editor.i_current_node].f_fov,
																						TRUE)
				
			IF NOT IS_CAM_RENDERING(s_cam_editor.cam_main)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
			ENDIF
			
			s_cam_editor.i_playback_current_event = 100
		ENDIF
		
		s_cam_editor.b_view_node = FALSE
	ENDIF
	
	//Play back the spline if instructed
	IF s_cam_editor.b_preview_spline
	OR IS_BIT_SET(g_iScriptedCamEditorFlags, CAM_EDITOR_FLAGS_FORCE_PLAYBACK)
		s_cam_editor.i_playback_current_event = 0
		s_cam_editor.b_preview_spline = FALSE
		CLEAR_BIT(g_iScriptedCamEditorFlags, CAM_EDITOR_FLAGS_FORCE_PLAYBACK)
	ENDIF
	
	UPDATE_CAM_PLAYBACK()

	//Draw the nodes on screen, and lines connecting them
	IF s_cam_editor.b_display_spline
		DRAW_CAM_DATA()
	ENDIF
	
	//Output all of the spline data to temp_debug if instructed.
	IF s_cam_editor.b_output_data
		SAVE_CAM_DATA_TO_DEBUG()
	ENDIF
	
	//Output all of the spline data to a given XML file if instructed.
	IF s_cam_editor.b_save_to_xml
		SAVE_CAM_DATA_TO_XML()
	ENDIF
	
	//Load all of the spline data from a given XML file if instructed.
	IF s_cam_editor.b_load_from_xml
		LOAD_CAM_DATA_FROM_XML()
	ENDIF
	
	//Pause the game (except this script)
	IF s_cam_editor.b_pause_game
		s_cam_editor.b_game_paused = NOT s_cam_editor.b_game_paused
		SET_GAME_PAUSED(s_cam_editor.b_game_paused)
		
		s_cam_editor.b_pause_game = FALSE
	ENDIF
	
	//If the current cut is changed update both cut and node information.
	IF s_cam_editor.i_current_cut != s_cam_editor.i_previous_cut	
		//Save the current widget info to the previously selected node
		REPLACE_CUT_DATA_WITH_WIDGET_DATA(s_cam_editor.i_previous_cut)
		
		//Update the widget info to refer to the current selected node
		REPLACE_WIDGET_DATA_WITH_CUT_DATA(s_cam_editor.i_current_cut)
	ENDIF
	
	//If the current node was changed update node information only
	IF s_cam_editor.i_current_node != s_cam_editor.i_previous_node
		REPLACE_CUT_NODE_DATA_WITH_WIDGET_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_previous_node)
		REPLACE_WIDGET_NODE_DATA_WITH_CUT_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_current_node)
	ENDIF
	
	//Do the same for the lookat nodes
	IF s_cam_editor.i_current_lookat_node != s_cam_editor.i_previous_lookat_node
		REPLACE_CUT_LOOKAT_NODE_DATA_WITH_WIDGET_LOOKAT_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_previous_lookat_node)
		REPLACE_WIDGET_LOOKAT_NODE_DATA_WITH_CUT_LOOKAT_NODE_DATA(s_cam_editor.i_current_cut, s_cam_editor.i_current_lookat_node)
	ENDIF
	
	s_cam_editor.i_previous_node = s_cam_editor.i_current_node
	s_cam_editor.i_previous_lookat_node = s_cam_editor.i_current_lookat_node
	s_cam_editor.i_previous_cut = s_cam_editor.i_current_cut
	
	IF s_cam_editor.b_terminate
		CLEANUP_EDITOR()
	ENDIF	
ENDPROC				



SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		CLEANUP_EDITOR()
	ENDIF

	CREATE_WIDGETS()

	s_cam_editor.i_playback_current_event = 100 //Stops editor from trying to play cameras until the user presses play
	s_cam_editor.b_display_spline = TRUE
	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Cam editor is active.", 7000, 0)

	WHILE (TRUE)
		WAIT(0)

		UPDATE_CAM_EDITOR()
	ENDWHILE
ENDSCRIPT		
#ENDIF