
//╒═════════════════════════════════════════════════════════════════════════════╕
//│					  	Custom Startup Configuration Script						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		DESCRIPTION: 	Run on startup with the command line arg 				│
//│						-runscript=custom_config. Can be used to				│
//│						automatically configure global settings as the			│
//│						game initialises.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_game.sch"
#ENDIF

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		TERMINATE_THIS_THREAD()
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTNL()
		PRINTLN("****** RUNNING CUSTOM CONFIGURATION SCRIPT ******")
		
		g_savedGlobals.sFlow.isGameflowActive	= TRUE			// Start game in gameflow mode.
		
		g_bDebugBlockAutosaves 					= FALSE			// Block the autosave controller from starting save operations.
		
		g_bDebugFlowSilencePhonecalls 			= FALSE			// All off-mission phonecalls automatically complete silently.
		
		g_bDebugDisableFailScreen				= FALSE			// Disable fade to black replay screens when failing missions.
		
		g_b_CellDialDebugTextToggle				= TRUE			// Enables detailed console output from all cellphone and dialogue operations.
		
		g_debugMenuControl.bUseBugstarQuery		= TRUE			// Allow the debug menu to query bugstar for bug stats.
		
		g_eDebugForceStartingCharacter			= NO_CHARACTER	// Force the game to start with a certain character. 
																// NO_CHARACTER , CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR
		
		PRINTLN("->Start game in gameflow mode: ", 			g_savedGlobals.sFlow.isGameflowActive)
		PRINTLN("->Autosave requests blocked: ", 			g_bDebugBlockAutosaves)
		PRINTLN("->Flow phonecalls silenced: ", 			g_bDebugFlowSilencePhonecalls)
		PRINTLN("->Disable fail screens: ", 				g_bDebugDisableFailScreen)
		PRINTLN("->Enable cellphone and dialogue output: ",	g_b_CellDialDebugTextToggle)
		PRINTLN("->Enable debug menu Bugstar queries: ",	g_debugMenuControl.bUseBugstarQuery)
		PRINTLN("->Start game with character: ",			GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(g_eDebugForceStartingCharacter))
		PRINTLN("********** CUSTOM CONFIGURATION ENDED ***********\n")
	#ENDIF
	
	TERMINATE_THIS_THREAD()
ENDSCRIPT

