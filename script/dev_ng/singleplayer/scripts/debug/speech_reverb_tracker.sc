#IF IS_FINAL_BUILD
	SCRIPT
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//		..........................
//		: Fraser Morgan			 :
//		: Speech Reverb Tracker	 :
//		: 10/10/2012			 :
//		..........................

USING "rage_builtins.sch"
USING "globals.sch" 
USING "brains.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_task.sch"
USING "commands_object.sch"
USING "commands_physics.sch"
USING "commands_player.sch"
USING "commands_misc.sch"
USING "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "weapon_enums.sch"


PED_INDEX REVERB_PED

WIDGET_GROUP_ID reverbingPedWidgets

BOOL bCreatePed
BOOL bSpeech
BOOL bDeletePed
BOOL bTerminate
BOOL bSpeechWaitDelete

INT iTime = 1500

VECTOR vFront, vSide, vUp, vPos
VECTOR vPedPos, vTransformedPedPos
VECTOR vClickPos

FLOAT fDelta	= 0
FLOAT fPedPos	= 1

PROC MISSION_CLEANUP()

	IF DOES_WIDGET_GROUP_EXIST(reverbingPedWidgets)
 		DELETE_WIDGET_GROUP(reverbingPedWidgets)
	ENDIF
	
	IF DOES_ENTITY_EXIST(REVERB_PED)
		DELETE_PED(REVERB_PED)
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC
PROC CLICK_UPDATER()

	IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
	
		vClickPos = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
		
		IF DOES_ENTITY_EXIST(REVERB_PED)
			DELETE_PED(REVERB_PED)
		ENDIF
			
		REQUEST_MODEL(A_F_Y_BEVHILLS_01)
			
		IF NOT HAS_MODEL_LOADED(A_F_Y_BEVHILLS_01)
			WAIT(1500)
		ENDIF
			
		REVERB_PED = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BEVHILLS_01, vClickPos)
			
		SET_ENTITY_HEADING(REVERB_PED, (GET_ENTITY_HEADING(PLAYER_PED_ID())))
			
		bCreatePed = FALSE
		
	ENDIF
	
	IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_RIGHT_BTN)
		IF DOES_ENTITY_EXIST(REVERB_PED)
			bSpeechWaitDelete = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC PED_BEHAVIOURS()

	IF DOES_ENTITY_EXIST(REVERB_PED) AND NOT IS_ENTITY_DEAD(REVERB_PED)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(REVERB_PED, TRUE)
	ENDIF
	
ENDPROC

PROC CREATE_WIDGETS()

	reverbingPedWidgets = START_WIDGET_GROUP("Speech Reverb Tracker")
			ADD_WIDGET_BOOL("Create Ped", bCreatePed)
			ADD_WIDGET_BOOL("Trigger Ambient Speech", bSpeech)
			ADD_WIDGET_BOOL("Delete Ped", bDeletePed)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Time before deletion", iTime, 0, 5500, 100)
			ADD_WIDGET_BOOL("Speech, Wait, Delete", bSpeechWaitDelete)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("::TO USE MOUSE CLICK TO SPAWN A PED::")
			ADD_WIDGET_STRING("1. Highlight Script bank in RAG")
			ADD_WIDGET_STRING("2. Click [Toggle Script Bank] then navigate to Script -> Script Debug Tools")
			ADD_WIDGET_STRING("3. Click [Enable / Disable Debugging]")
			ADD_WIDGET_STRING("4. Click anywhere on screen to spawn the ped.")
			ADD_WIDGET_STRING("(you may need to restart this script)")
			ADD_WIDGET_BOOL("Terminate Script", bTerminate)
	STOP_WIDGET_GROUP()
	
ENDPROC

SCRIPT 

	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_CLEANUP()
	ENDIF

	CREATE_WIDGETS()
	
	WHILE TRUE
		WAIT(0)
	
		PED_BEHAVIOURS()
		
		CLICK_UPDATER()
		
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vFront, vSide, vUp, vPos)
		
		vTransformedPedPos = vPos + fDelta * vSide
		
		vPedPos = vTransformedPedPos + vFront * fPedPos
		
		IF bSpeechWaitDelete
			IF DOES_ENTITY_EXIST(REVERB_PED)
				TASK_TURN_PED_TO_FACE_ENTITY(REVERB_PED, PLAYER_PED_ID(), 0)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(REVERB_PED, "PHONE_CONV1_CHAT3", "A_F_M_BEVHILLS_02_WHITE_FULL_01", "SPEECH_PARAMS_FORCE_NORMAL")
				WAIT(iTime)
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(REVERB_PED)
				DELETE_PED(REVERB_PED)
			ENDIF
			bSpeechWaitDelete = FALSE
		ENDIF
		
		IF bDeletePed
				IF DOES_ENTITY_EXIST(REVERB_PED)
					WAIT(iTime)
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(REVERB_PED)
					DELETE_PED(REVERB_PED)
				ENDIF
			bDeletePed = FALSE
		ENDIF
		
		IF bSpeech
			IF DOES_ENTITY_EXIST(REVERB_PED)
				TASK_TURN_PED_TO_FACE_ENTITY(REVERB_PED, PLAYER_PED_ID(), 0)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(REVERB_PED, "PHONE_CONV1_CHAT3", "A_F_M_BEVHILLS_02_WHITE_FULL_01", "SPEECH_PARAMS_FORCE_NORMAL")
			ENDIF
			bSpeech = FALSE
		ENDIF
		
		IF bCreatePed
		
			IF DOES_ENTITY_EXIST(REVERB_PED)
				DELETE_PED(REVERB_PED)
			ENDIF
			
			REQUEST_MODEL(A_F_Y_BEVHILLS_01)
			
			IF NOT HAS_MODEL_LOADED(A_F_Y_BEVHILLS_01)
				WAIT(1500)
			ENDIF
			
			REVERB_PED = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BEVHILLS_01, vPedPos)
			
			SET_ENTITY_HEADING(REVERB_PED, (GET_ENTITY_HEADING(PLAYER_PED_ID())))
			
			bCreatePed = FALSE
			
		ENDIF
		
		IF bTerminate
			MISSION_CLEANUP()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_CLEANUP()
		ENDIF
	
	ENDWHILE
	
ENDSCRIPT


#ENDIF //IS_DEBUG_BUILD

