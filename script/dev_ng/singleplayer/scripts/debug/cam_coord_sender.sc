

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// +---------------------------------------------------------------------------------+
// +--------------------------¦	   Speech queue System  ¦----------------------------+    
// +--------------------------¦	   	by BObby Wright 	¦----------------------------+    
// +--------------------------¦	   						¦----------------------------+    
// +--------------------------¦	    Private Functions	¦----------------------------+    
// +--------------------------¦	   						¦----------------------------+    
// +---------------------------------------------------------------------------------+
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_ped.sch"
USING "commands_hud.sch"
USING "commands_script.sch"
USING "dialogue_public.sch"
USING "script_maths.sch"
USING "commands_xml.sch"
#IF IS_DEBUG_BUILD
//USING "hud_creator_tool.sch"
	BOOL bSetfilename
	BOOL bKill
	BOOL boobUseDebug
	BOOL bSendCOorderStatus = TRUE 
	FLOAT fCamFOV = 45.0
	INT iSendTimer
	INT iMainState
	INT iSendTimerDelay = 0//500
	INT iSwapStatus = -1
	INT iReadTimer
	INT iReCheckStatus
	INT iReadTimerDelay = iSendTimerDelay
	FLOAT fMousex
	FLOAT fMousey
	FLOAT fw[2]
	FLOAT fh[2]
	VECTOR vCam
	VECTOR vCamROT
	TEXT_WIDGET_ID	twid_CamCoords
	TEXT_LABEL_63 file = "coord_sender.xml"
	STRING path = "N:/RSGEDI/Temp/" //coord_sender/ //"X:/gta5/build/dev/"
//	WIDGET_GROUP_ID  wgGroup
	//Makes the text queue widgets
	PROC SET_UP_WIDGETS()
		/*wgGroup = */START_WIDGET_GROUP(" Camera Coord Sender")
			twid_CamCoords = ADD_TEXT_WIDGET("File Name")
			ADD_WIDGET_INT_SLIDER("iSwapStatus ", iSwapStatus,-1, 10000, 1)
			SET_CONTENTS_OF_TEXT_WIDGET(twid_CamCoords, "Type in new File Name")
			ADD_WIDGET_STRING("(remember the .xml in the name)")
			ADD_WIDGET_BOOL("Update the File Name", bSetfilename)
			ADD_WIDGET_BOOL("boobUseDebug", boobUseDebug)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_BOOL("Send Cam Coords", g_bSendCoords)
			ADD_WIDGET_BOOL("Read Cam Coords", g_bRead_coordinates)
			ADD_WIDGET_INT_SLIDER("Send Timer", iSendTimerDelay,-1, 10000, 10)
			ADD_WIDGET_INT_SLIDER("Read Timer", iReadTimerDelay,0-1, 10000, 10)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_STRING("")
			ADD_WIDGET_VECTOR_SLIDER("Cam Coords", vCam, -4000.0, 4000.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("Cam Rotation", vCamROT, -4000.0, 4000.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Cam FOV", fCamFOV, 0.0, 3600.0, 0.001)
			
			ADD_WIDGET_FLOAT_SLIDER("w", fw[0], 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("f", fh[0], 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("w", fw[1], 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("f", fh[1], 0.0, 1.0, 0.001)
			
			ADD_WIDGET_BOOL("Kill Script", bKill)
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC temp_SAVE_VECTOR_TO_NAMED_DEBUG_FILE(VECTOR vVector_to_save,STRING pathPassed, STRING filePassed, STRING sInitialString)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE(sInitialString,pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.x,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" y=\"",pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.y,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" z=\"",pathPassed, filePassed)
	    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(vVector_to_save.z,pathPassed, filePassed)
	    SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>",pathPassed, filePassed)
	ENDPROC
	TEXT_LABEL_63 sXMLFilePath = "N:/RSGEDI/Temp/"
	TEXT_LABEL_63 sXMLFile
	//STRING sXMLFile = "SPPlaythrough/coord_sender.xml"
		
	CONST_INT iFOV  		 1783694977
	CONST_INT iCoord  		-1036347508
	CONST_INT iROT  		-1342477066
	CONST_INT iSWAPCONTROL  -262404537
	CONST_INT ix  			-1828477467
	CONST_INT iy  			-2137718520
	CONST_INT iz  			-1235194722
	CONST_INT iff 		 	 1208324078
	CONST_INT iSwap 		 1133170572
	CONST_INT mouse			 1711906636
	CONST_INT mousex		 1315400787
	CONST_INT mousey		-735348775

	FUNC BOOL Do_XML_READ()
		sXMLFile = sXMLFilePath
		sXMLFile += file
		// Load in xml file if it exists
		IF LOAD_XML_FILE(sXMLFile)
		//PRINTSTRING("Do_XML_READ'")PRINTSTRING(sXMLFile)PRINTSTRING("'")PRINTNL()
			IF GET_NUMBER_OF_XML_NODES() <> 0
			//PRINTSTRING("GET_NUMBER_OF_XML_NODES = ")PRINTINT(GET_NUMBER_OF_XML_NODES()) PRINTNL()
	        	// Integers used for the FOR loops going through all the nodes and attributes
	        	INT eachNode, eachAttribute
			   
	        	// Loop through all the nodes
	        	FOR eachNode = 0 TO (GET_NUMBER_OF_XML_NODES()-1)
					// Convert the current node name to a hash key so it can be used in a switch against the const's created at the top of this file
					SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
					
						CASE iCoord
							//PRINTSTRING("iCoord")PRINTNL()
							
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
										CASE ix
											vCam.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
										
										CASE iy
											vCam.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
										
										CASE iz
											vCam.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									ENDSWITCH
								ENDFOR
							
							ENDIF
						BREAK
						
						CASE iFOV
							//PRINTSTRING("iFOV")PRINTNL()
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
										CASE iff
											fCamFOV = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									ENDSWITCH
								ENDFOR
							
							ENDIF
						BREAK
						CASE iROT
							//PRINTSTRING("iROT")PRINTNL()
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
										CASE ix
											vCamROT.x = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
										
										CASE iy
											vCamROT.y = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
										
										CASE iz
											vCamROT.z = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									ENDSWITCH
								ENDFOR
							
							ENDIF
						BREAK
						
						CASE iSWAPCONTROL
							IF iReCheckStatus < GET_GAME_TIMER() 
							//PRINTSTRING("iSWAPCONTROL")PRINTNL()
								IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
									FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
										SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
											CASE iSwap
												iSwapStatus = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
											BREAK
										ENDSWITCH
									ENDFOR
								ENDIF
							ENDIF
						BREAK
						
						CASE mouse
							//PRINTSTRING("iCoord")PRINTNL()
							
							IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
								FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
									SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))
										CASE mousex
											fMousex = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
										
										CASE mousey
											fMousey = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
										BREAK
									ENDSWITCH
								ENDFOR
							ENDIF
						BREAK
						DEFAULT
							PRINTINT(GET_HASH_KEY(GET_XML_NODE_NAME()))PRINTNL()
						//	PRINTSTRING("DEFAULT")PRINTNL()
						BREAK
					// End of the node switch statement
					ENDSWITCH
							
					// Tell the script to goto the next node in the xml file
					GET_NEXT_XML_NODE()
				
				// End of for loop going through the nodes
				ENDFOR
		
				// Now we have gone through all the nodes you need to unload the xml file
				DELETE_XML_FILE()
				RETURN TRUE
			ENDIF
		ENDIF
		RETURN FALSE
	ENDFUNC
#ENDIF
//PURPOSE: cleans up all resources used in the mission
PROC SCRIPT_CLEAN_UP()
	SET_DEBUG_CAM_ACTIVE(FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
	ENDIF				
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	VECTOR vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fTemp
	IF GET_GROUND_Z_FOR_3D_COORD(vTemp, ftemp)
		vTemp.Z = ftemp
	ENDIF
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vTemp)
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, TRUE, FALSE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	TERMINATE_THIS_THREAD()
ENDPROC




// +--------------------------¦	 	Control the Speech Queue	¦----------------------------+   
SCRIPT 
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	#IF IS_DEBUG_BUILD
		SET_UP_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		WAIT(0)
		#IF IS_DEBUG_BUILD
			
//			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_CTRL, "COORD_SENDER")
//				IF g_bSendCoords = TRUE
//					g_bSendCoords = FALSE
//				ELSE
//					g_bSendCoords = TRUE
//					bDoTExt = TRUE 
//				ENDIF
//			ENDIF
//			
//			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_R, KEYBOARD_MODIFIER_CTRL, "COORD_READER")
//				IF g_bRead_coordinates = TRUE
//					g_bRead_coordinates = FALSE
//				ELSE
//					g_bRead_coordinates = TRUE
//				ENDIF
//			ENDIF
//			
			
			//Maintain_Hud_Creator_Tool(wgGroup)
			
			IF iReCheckStatus < GET_GAME_TIMER() 
				IF iSwapStatus > 0
					IF g_bSendCoords = TRUE
						g_bRead_coordinates = TRUE
						g_bSendCoords = FALSE
						iSwapStatus = -1
						//SET_DEBUG_CAM_ACTIVE(FALSE)
						iReCheckStatus= GET_GAME_TIMER() + 2000
					ELIF g_bRead_coordinates = TRUE
						SET_DEBUG_CAM_ACTIVE(FALSE)
						g_bSendCoords = TRUE
						g_bRead_coordinates = FALSE
						//CLEAR_NAMED_DEBUG_FILE(path, file)
						iReCheckStatus= GET_GAME_TIMER() + 2000
						iSwapStatus = -1
						//SET_DEBUG_CAM_ACTIVE(FALSE)
					ENDIF				
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
				IF bSendCOorderStatus = TRUE
					bSendCOorderStatus = FALSE
				ELSE
					bSendCOorderStatus = TRUE
				ENDIF
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(key_s, KEYBOARD_MODIFIER_SHIFT, "SWAP CAM COORD CONTROLLer")
				iSwapStatus = 10
			ENDIF
			
			IF g_bSendCoords = TRUE
				iMainState = 0
			ENDIF
			IF g_bRead_coordinates = TRUE
				iMainState = 1
			ENDIF
			
			CAMERA_INDEX ciTemp	
			
			
			IF bSendCOorderStatus = TRUE
				SWITCH iMainState
					CASE 0 
						GET_MOUSE_POSITION(fMousex, fMousey)
						IF GET_GAME_TIMER() > iSendTimer
					
							iSendTimer = GET_GAME_TIMER() +iSendTimerDelay
						
							//Camera Coords
							//SET_DEBUG_CAM_ACTIVE(TRUE)
							ciTemp	= GET_DEBUG_CAM()
							vCam     	= GET_CAM_COORD(ciTemp)//GET_FINAL_RENDERED_CAM_COORD()
							vCamROT		= GET_CAM_ROT(ciTemp)//GET_FINAL_RENDERED_CAM_ROT()
							fCamFOV		= GET_CAM_FOV(ciTemp)//GET_FINAL_RENDERED_CAM_FOV()
							
							IF vCam.x != 0
							AND vCam.y != 0
								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									SET_ENTITY_COORDS(PLAYER_PED_ID(),<<vCam.X,vCam.Y, vCam.Z>> )
								ENDIF
							ENDIF
															
							CLEAR_NAMED_DEBUG_FILE(path, file)
							OPEN_NAMED_DEBUG_FILE(path, file)
								// Add the initial xml data to the file
								SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version='1.0'?>", path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								SAVE_STRING_TO_NAMED_DEBUG_FILE("<DebugMenuItems>", path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
							
								temp_SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vCam,path, file,  "<coord x=\"")
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								SAVE_STRING_TO_NAMED_DEBUG_FILE("<FOV f=\"",path, file)
								SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fCamFOV,path, file)
				    			SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>",path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								
								temp_SAVE_VECTOR_TO_NAMED_DEBUG_FILE(vCamROT,path, file, "<rot x=\"")
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								
								
								//FOr the mouse pointer
								SAVE_STRING_TO_NAMED_DEBUG_FILE("<mouse mousex=\"",path, file)
							    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fMousex ,path, file)
							    SAVE_STRING_TO_NAMED_DEBUG_FILE("\" mousey=\"",path, file)
							    SAVE_FLOAT_TO_NAMED_DEBUG_FILE(fMousey ,path, file)
							    SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>",path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
								
								
								IF iSwapStatus > 0
									SAVE_STRING_TO_NAMED_DEBUG_FILE("<SWAPCONTROL Swap=\"",path, file)
									SAVE_INT_TO_NAMED_DEBUG_FILE(iSwapStatus,path, file)
				    				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"/>",path, file)
									SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
									//iSwapStatus = -1
								ENDIF
								// Add the final xml data
								SAVE_STRING_TO_NAMED_DEBUG_FILE("</DebugMenuItems>", path, file)
								SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(path, file)
							CLOSE_DEBUG_FILE()
						ENDIF
						SET_TEXT_SCALE(0.4600, 0.4600)
						SET_TEXT_CENTRE(TRUE)
						SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
						SET_TEXT_EDGE(0, 0, 0, 0, 255)
						SET_TEXT_COLOUR(255, 255, 255, 255)					
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.8, "STRING","Sending Camera Position~n~Start + Circle to activate.~n~Shift + S to Receive~n~Z to deactivate")
					BREAK
					
					CASE 1
						IF GET_GAME_TIMER() > iReadTimer
							IF Do_XML_READ()
								iReadTimer = GET_GAME_TIMER() +iReadTimerDelay
								
								SET_DEBUG_CAM_ACTIVE(TRUE)
								ciTemp	= GET_DEBUG_CAM()
								SET_CAM_COORD(ciTemp	,vCam)
								SET_CAM_ROT(ciTemp	, vCamROT)
								SET_CAM_FOV(ciTemp	, fCamFOV)

								IF NOT IS_PED_INJURED(PLAYER_PED_ID())
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									SET_ENTITY_COORDS(PLAYER_PED_ID(),<<vCam.X,vCam.Y, vCam.Z>> )
								ENDIF
								//LOAD_SCENE(vCam)
							ENDIF
						ENDIF
						IF fMousex != 0 
						AND fMousey != 0
							DRAW_RECT(fMousex, fMousey, 0.026, 0.008,  0, 0, 0, 251)
							DRAW_RECT( fMousex, fMousey, 0.004, 0.046, 0, 0, 0, 251)
							DRAW_RECT(fMousex, fMousey, 0.025, 0.005,  255, 255, 255, 251)
							DRAW_RECT( fMousex, fMousey, 0.003, 0.045, 255, 255, 255, 251)
						ENDIF
						
						SET_TEXT_SCALE(0.4600, 0.4600)
						SET_TEXT_CENTRE(TRUE)
						SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
						SET_TEXT_EDGE(0, 0, 0, 0, 255)
						SET_TEXT_COLOUR(255, 255, 255, 255)
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.8, "STRING","Receiving Camera Position~n~Shift + S to Send~n~Z to deactivate")
					BREAK				
				ENDSWITCH
			ELSE
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_TEXT_SCALE(0.4600, 0.4600)
				SET_TEXT_CENTRE(TRUE)
				SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
				SET_TEXT_EDGE(0, 0, 0, 0, 255)
				SET_TEXT_COLOUR(255, 255, 255, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.8, "STRING","Press Z To activate")
			ENDIF
			
			IF bSetfilename = TRUE
				bSetfilename = FALSE
				file = GET_CONTENTS_OF_TEXT_WIDGET(twid_CamCoords)
			ENDIF
			IF bKill
			OR IS_KEYBOARD_KEY_PRESSED(KEY_F)
				SCRIPT_CLEAN_UP()
			ENDIF
		#ENDIF
	ENDWHILE
ENDSCRIPT





