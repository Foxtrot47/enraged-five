USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"

// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

// Only include in debug mode
#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch" 




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	streaming.sc
//		AUTHOR			:	Klaas Schilstra
//		DESCRIPTION		:	Streaming performance test script
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

NETWORK_INDEX netVeh

VECTOR v_car_pos 					= << -1649.5, -559.4, 33.1 >>
VECTOR vehicles_coords
FLOAT car_heading			=	309
//FLOAT veh_speed				=   60 //meters per second
BOOL density = FALSE

//SEQUENCE_INDEX SEQ
VEHICLE_INDEX veh_car
MODEL_NAMES model_car = ZENTORNO

ENUM eSCRIPT_STATE	
    SCRIPT_STATE_START = -1,
    SCRIPT_STATE_CREATE_CAR = 0,
    SCRIPT_STATE_WAIT_FOR_KEY,
    SCRIPT_STATE_DRIVE_CAR,
    SCRIPT_STATE_DRIVE_TO_POINT,
	SCRIPT_STATE_SHOW_RESULTS,
    SCRIPT_STATE_END
ENDENUM

eSCRIPT_STATE script_state = SCRIPT_STATE_START
BOOL automatic_mode = TRUE /*FALSE*/
INT automatic_flashy_timer = 0
INT automatic_countdown = 0
INT automatic_iteration = 0

STRUCT ServerBroadcastData 
INT iServerData 
ENDSTRUCT 
ServerBroadcastData serverBD 

STRUCT PlayerBroadcastData 
INT iPlayerData
ENDSTRUCT 
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	PRINTSTRING("...Placeholder Mission Cleanup")
	PRINTNL()
	
	CLEAR_PRINTS()
	CLEAR_HELP()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())	
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(veh_car)

	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	
	CLEAR_WEATHER_TYPE_PERSIST()
	METRICS_ZONES_CLEAR()
	METRICS_ZONES_HIDE()
		
	TERMINATE_THIS_THREAD()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
//	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()

	PRINTSTRING("...Placeholder Mission Failed")
	PRINTNL()
	
	//Mission_Flow_Mission_Failed()
	Mission_Cleanup()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Setup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Setup()	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_OVERRIDE_WEATHER("EXTRASUNNY")
	ELSE
		SET_CLOCK_TIME(12,0,0)
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
	ENDIF
	//LOAD_SCENE(v_car_pos)

	PRINTLN("Waiting for scene to load...")
	SET_ENTITY_COORDS(PLAYER_PED_ID(), v_car_pos)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 0)

	NEW_LOAD_SCENE_START_SPHERE(v_car_pos, 2500.0)
	WAIT(0)

	WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
		WAIT(0)
	ENDWHILE
	
	NEW_LOAD_SCENE_STOP()
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	CLEAR_AREA(v_car_pos, 1000, TRUE, TRUE)
	INSTANTLY_FILL_PED_POPULATION()
	INSTANTLY_FILL_VEHICLE_POPULATION()
	
	AUTOMATED_TEST_BEGIN()
	
	density = TRUE
		
ENDPROC

PROC CLEANUP_SCRIPT()

	TERMINATE_THIS_THREAD()
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	ENDIF
	
	PRINTSTRING("...Placeholder Mission Launched")
	PRINTNL()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// MP Setup
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": PROCESS_PRE_GAME started in MP for ", GET_THIS_SCRIPT_NAME())
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits untull it is.
		IF NOT HANDLE_NET_SCRIPT_INITIALISATION(FALSE, -1, TRUE)
			Mission_Cleanup()
		ENDIF
		
		NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD)) 
		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
		
		RESERVE_NETWORK_MISSION_VEHICLES(1)
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
		// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
		IF NOT Wait_For_First_Network_Broadcast()
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Failed to receive initial network broadcast for ", GET_THIS_SCRIPT_NAME(), ". Cleaning up.")
			Mission_Cleanup()
		ENDIF
	ENDIF
	
	script_state = SCRIPT_STATE_START
	
	Mission_Setup()
	
	WHILE (TRUE)

		SWITCH script_state

			CASE SCRIPT_STATE_START
				PRINTSTRING("...Going for a drive")
				PRINTNL()
				METRICS_ZONES_CLEAR()
				script_state = SCRIPT_STATE_CREATE_CAR
			BREAK
		
			CASE SCRIPT_STATE_CREATE_CAR
				PRINTSTRING("...Create car")
				PRINTNL()
				IF NOT DOES_ENTITY_EXIST(veh_car)
					REQUEST_MODEL(model_car)
					
					IF HAS_MODEL_LOADED(model_car)
					
						IF NETWORK_IS_GAME_IN_PROGRESS()
							IF CREATE_NET_VEHICLE(netVeh, model_car, v_car_pos, car_heading)
								veh_car = NET_TO_VEH(netVeh)
								SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
							ENDIF
						ELSE
							veh_car = CREATE_VEHICLE(model_car, v_car_pos, car_heading)
							SET_MODEL_AS_NO_LONGER_NEEDED(model_car)
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(veh_car)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_car)
						SET_ENTITY_COORDS(veh_car, v_car_pos)
						SET_ENTITY_HEADING(veh_car, car_heading)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					ENDIF
					
					PRINTLN("Waiting for scene to load...")

					NEW_LOAD_SCENE_START_SPHERE(v_car_pos, 2500.0)
					WAIT(0)

					WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
						WAIT(0)
					ENDWHILE
					
					NEW_LOAD_SCENE_STOP()
					
//					SETTIMERB(0)
					script_state = SCRIPT_STATE_WAIT_FOR_KEY
					automatic_countdown = 100
				ENDIF
			BREAK	
				
			CASE SCRIPT_STATE_WAIT_FOR_KEY
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					SET_CLOCK_TIME(17, 0, 0)
				ENDIF
				// Display Pass and Fail instructions
//				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
//				IF TIMERB() > 2000
//					PRINTSTRING("...Gogogogo")
//					PRINTNL()
//					script_state = SCRIPT_STATE_DRIVE_CAR
//				ENDIF
				
				IF (automatic_mode)
					IF (automatic_countdown > 0)
						automatic_countdown = automatic_countdown - 1
					ELSE
						script_state = SCRIPT_STATE_DRIVE_CAR
					ENDIF
				ENDIF
			BREAK
			
			CASE SCRIPT_STATE_DRIVE_CAR
				IF IS_VEHICLE_DRIVEABLE(veh_car)
					METRICS_ZONE_START("Streaming")
					//METRICS_ZONES_SHOW()
					
					REQUEST_VEHICLE_RECORDING(1, "zentorno_streaming")
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "zentorno_streaming")
						WAIT(0)
					ENDWHILE
					IF IS_VEHICLE_DRIVEABLE(veh_car)
						START_PLAYBACK_RECORDED_VEHICLE(veh_car, 1, "zentorno_streaming")
					ENDIF
														
					script_state = SCRIPT_STATE_DRIVE_TO_POINT
					
					// Test starts now
					AUTOMATED_TEST_START_ITERATION()
				ENDIF
			BREAK
			
			CASE SCRIPT_STATE_DRIVE_TO_POINT
				IF IS_VEHICLE_DRIVEABLE(veh_car) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_car)
					METRICS_ZONE_STOP()
					METRICS_ZONE_SAVE_TO_FILE("StreamingTest")
					AUTOMATED_TEST_NEXT_ITERATION()
					SETTIMERA(0)
					script_state = SCRIPT_STATE_SHOW_RESULTS
					automatic_countdown = 120
				ENDIF
				
				IF TIMERA() > 180
					vehicles_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					CLEAR_AREA(vehicles_coords, 16, TRUE)
					SETTIMERA(0)
				ENDIF
			BREAK
			
			CASE SCRIPT_STATE_SHOW_RESULTS
				METRICS_ZONES_SHOW()
//				PRINTSTRING("...PRESS P to exit or F to fail")
//				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
//				IF TIMERB() > 5000
	//				METRICS_ZONES_CLEAR()
//					CLEAR_AREA(v_car_pos, 5000, TRUE, TRUE)
//					CLEAR_AREA_OF_VEHICLES(v_car_pos, 5000.0, TRUE)
//					script_state = SCRIPT_STATE_CREATE_CAR
//					script_state = SCRIPT_STATE_END
	//			ENDIF
				
				IF TIMERA() > 180
					vehicles_coords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					CLEAR_AREA(vehicles_coords, 16, TRUE)
					SETTIMERA(0)
				ENDIF

				IF (automatic_mode)
					IF (automatic_countdown > 0)
						automatic_countdown = automatic_countdown - 1
					ELSE
						METRICS_ZONES_CLEAR()
						script_state = SCRIPT_STATE_CREATE_CAR
						automatic_iteration = automatic_iteration + 1
					ENDIF
				ENDIF
				
			BREAK
			
			CASE SCRIPT_STATE_END
				PRINTSTRING("...The end")
				PRINTNL()
				Mission_Passed()
			BREAK
			
		ENDSWITCH

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_A))
			IF automatic_mode
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Automatic Mode OFF", 1000, 1)
				automatic_mode = FALSE
			ELSE
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Automatic Mode ON", 1000, 1)
				automatic_mode = TRUE
			ENDIF
		ENDIF
			
		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			METRICS_ZONE_STOP()
			METRICS_ZONES_CLEAR()
			Mission_Failed()
		ENDIF

		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T))
			IF NOT density
				SET_VEHICLE_POPULATION_BUDGET(3)
				SET_PED_POPULATION_BUDGET(3)
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Traffic ON", 1000, 1)
				WAIT(500)
				density = TRUE
			ELSE
				SET_VEHICLE_POPULATION_BUDGET(0)
				SET_PED_POPULATION_BUDGET(0)
				CLEAR_AREA(v_car_pos, 5000, TRUE, TRUE)
				CLEAR_AREA_OF_VEHICLES(v_car_pos, 5000.0, TRUE)
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Traffic OFF", 1000, 1)
				WAIT(500)
				density = FALSE
			ENDIF
		ENDIF

		IF NOT density
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
			SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0, 0)
		ELSE
			SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
			SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(1)
			SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(1, 1)
		ENDIF
						
		IF (automatic_mode)
			IF (automatic_flashy_timer < 30)
				TEXT_LABEL_63 tlTemp = "AUTOMATIC MODE - press A to disable. Current lap: "
				tlTemp += CONVERT_INT_TO_STRING(automatic_iteration)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.8, "STRING", tlTemp)
			ELSE
				IF (automatic_flashy_timer > 60)
					automatic_flashy_timer = 0
				ENDIF
			ENDIF
			
			automatic_flashy_timer = automatic_flashy_timer + 1
		ELSE
			PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Press P to restart test. F to fail, T to toggle traffic, A for automatic mode", 1000, 1)
		ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


#ENDIF	// IS_DEBUG_BUILD
