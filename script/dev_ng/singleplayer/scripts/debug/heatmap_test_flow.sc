USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_FINAL_BUILD
SCRIPT 
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_player.sch"
USING "commands_clock.sch"
USING "commands_script.sch"
USING "script_player.sch"
USING "streamed_scripts.sch"
USING "respawn_location_data.sch"
USING "Mission_Flow_Initialiser.sch"
USING "flow_launcher_main_core.sch"
USING "area_checks.sch"

// All the registration headers required to specify which global variables need to be saved
USING "flow_globals_reg.sch"

// Allow the globals to be created
//GLOBALS TRUE

CONST_INT numlocation 5
FLOAT grid_start_x = -3350
FLOAT grid_start_y = -3700
FLOAT grid_end_x = 4100
FLOAT grid_end_y = 7250
INT grid_size = 100, i
INT nodenum = 0
FLOAT new_heading
BOOL REVERSE, SKIP_FIRST_TIME
VECTOR FPSlocation = <<grid_start_x, grid_start_y, INVALID_WORLD_Z>>
VECTOR debug_info_print

ENUM FPS_TEST_STAGE
	SETUP,
	WARP_TO_LOCATION,
	WATER_CHECK,
	WAIT_TO_SETTLE,
	CAMERA_ROTATE,
	CLEANUP_ZONE,
	FPS_RESULTS
ENDENUM
FPS_TEST_STAGE FPSlocationStage = SETUP

PROC CLEANUP_SCRIPT()
	SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	PRINTLN("FULLMAP FLOW TEST SCRIPT TERMINATING")
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
		TERMINATE_THIS_THREAD()
	ENDIF
	
	PRINTLN("FULLMAP FLOW TEST SCRIPT STARTING")

	g_end_of_game_flow = TRUE
	g_disable_for_smoketest = TRUE

	WHILE TRUE
		
		WAIT(0)

			IF IS_PLAYER_PLAYING(PLAYER_ID())
			
				SWITCH FPSlocationStage
				
					CASE SETUP
						
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
						SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")

						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE, TRUE)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE, TRUE)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_PRISON, TRUE)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_BIOTECH, TRUE)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS, TRUE)
						SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MOVIE_STUDIO, TRUE)
						
						IF GET_COMMANDLINE_PARAM_EXISTS("grid_start_x")
							grid_start_x = GET_COMMANDLINE_PARAM_FLOAT("grid_start_x")
							PRINTLN("grid_start_x = ", grid_start_x)
						ELSE
							PRINTLN("Default grid_start_x = ", grid_start_x)
						ENDIF
						IF GET_COMMANDLINE_PARAM_EXISTS("grid_start_y")
							grid_start_y = GET_COMMANDLINE_PARAM_FLOAT("grid_start_y")
							PRINTLN("grid_start_y = ", grid_start_y)
						ELSE
							PRINTLN("Default grid_start_y = ", grid_start_y)						
						ENDIF
						IF GET_COMMANDLINE_PARAM_EXISTS("grid_end_x")
							grid_end_x = GET_COMMANDLINE_PARAM_FLOAT("grid_end_x")
							PRINTLN("grid_end_x = ", grid_end_x)
						ELSE
							PRINTLN("Default grid_end_x = ", grid_end_x)						
						ENDIF
						IF GET_COMMANDLINE_PARAM_EXISTS("grid_end_y")
							grid_end_y = GET_COMMANDLINE_PARAM_FLOAT("grid_end_y")
							PRINTLN("grid_end_y = ", grid_end_y)
						ELSE
							PRINTLN("Default grid_end_y = ", grid_end_y)						
						ENDIF
						IF GET_COMMANDLINE_PARAM_EXISTS("grid_size")
							grid_size = GET_COMMANDLINE_PARAM_INT("grid_size")
							PRINTLN("grid_size = ", grid_size)
						ELSE
							PRINTLN("Default grid_size = ", grid_size)
						ENDIF

						FPSlocation = <<grid_start_x+grid_size/2, grid_start_y+grid_size/2, INVALID_WORLD_Z>>
						
						IF GET_COMMANDLINE_PARAM_EXISTS("skip_telemetry_samples")
							nodenum = GET_COMMANDLINE_PARAM_INT("skip_telemetry_samples")
							
							REPEAT nodenum i
								IF NOT REVERSE
									FPSlocation.x = FPSlocation.x + grid_size
//									PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ELSE
									FPSlocation.x = FPSlocation.x - grid_size
//									PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ENDIF
								
								IF FPSlocation.x > grid_end_x
									FPSlocation.y = FPSlocation.y + grid_size
//									PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = TRUE
								ENDIF

								IF FPSlocation.x < grid_start_x
									FPSlocation.y = FPSlocation.y + grid_size
//									PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = FALSE
								ENDIF
							ENDREPEAT
							
						ENDIF
						
						PRINTLN("Starting node = ", nodenum)
						
						//Load the initial start position since we don't have the main script doing it for us
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<FPSlocation.x, FPSlocation.y, INVALID_WORLD_Z>>)
						WAIT(0)					
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<FPSlocation.x, FPSlocation.y, INVALID_WORLD_Z>>)

						debug_info_print = GET_ENTITY_COORDS(PLAYER_PED_ID())
						PRINTLN("Player start coord = ", debug_info_print)
						
						LOAD_SCENE(FPSlocation)
						LOAD_ALL_OBJECTS_NOW()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(0)
						ENDIF
						WAIT(0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						
						IF NOT GET_COMMANDLINE_PARAM_EXISTS("reverse_fullmap_test")
							IF FPSlocation.y > grid_end_y
								PRINTLN("SAMPLE #", nodenum, " Out of range ", FPSlocation)
								FPSlocationStage = CLEANUP_ZONE
							ELSE
								FPSlocationStage = WARP_TO_LOCATION
							ENDIF
						ELSE
							IF FPSlocation.y < grid_end_y
								PRINTLN("SAMPLE #", nodenum, " Out of range ", FPSlocation)
								FPSlocationStage = CLEANUP_ZONE
							ELSE
								FPSlocationStage = WARP_TO_LOCATION
							ENDIF						
						ENDIF
					BREAK
					
					CASE WARP_TO_LOCATION
						
						IF NOT GET_COMMANDLINE_PARAM_EXISTS("reverse_fullmap_test")
							IF SKIP_FIRST_TIME //So the start coords are counted
								IF NOT REVERSE
									FPSlocation.x = FPSlocation.x + grid_size
	//								PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ELSE
									FPSlocation.x = FPSlocation.x - grid_size
	//								PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ENDIF
								
								IF FPSlocation.x > grid_end_x
									FPSlocation.y = FPSlocation.y + grid_size
	//								PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = TRUE
								ENDIF

								IF FPSlocation.x < grid_start_x
									FPSlocation.y = FPSlocation.y + grid_size
	//								PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = FALSE
								ENDIF
							ENDIF
						ELSE
							IF SKIP_FIRST_TIME //So the start coords are counted
								IF NOT REVERSE
									FPSlocation.x = FPSlocation.x - grid_size
	//								PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ELSE
									FPSlocation.x = FPSlocation.x + grid_size
	//								PRINTLN("New FPSlocation.x = ", FPSlocation.x)
								ENDIF
								
								IF FPSlocation.x < grid_end_x
									FPSlocation.y = FPSlocation.y - grid_size
	//								PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = TRUE
								ENDIF

								IF FPSlocation.x > grid_start_x
									FPSlocation.y = FPSlocation.y - grid_size
	//								PRINTLN("New FPSlocation.y = ", FPSlocation.y)
									REVERSE = FALSE
								ENDIF
							ENDIF
						ENDIF

						SKIP_FIRST_TIME = TRUE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						SET_PED_CAN_RAGDOLL(PLAYER_PED_ID(), FALSE)
						
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<FPSlocation.x, FPSlocation.y, INVALID_WORLD_Z>>)
						
						SET_CLOCK_TIME(17, 0, 0)
						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
						
						WAIT(0)
						SETTIMERA(0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						FPSlocationStage = WATER_CHECK
						
					BREAK

					CASE WATER_CHECK
						
						IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
							PRINTLN("No solid ground. Set Z to 0")
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<FPSlocation.x, FPSlocation.y, 0.5>>)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						ENDIF
						
						IF TIMERA() > 100
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
//							LOAD_SCENE(FPSlocation)
//							LOAD_ALL_OBJECTS_NOW()
							debug_info_print = GET_ENTITY_COORDS(PLAYER_PED_ID())
							PRINTLN("START TELEMETRY SAMPLE #", nodenum, "   Zone =", GET_NAME_OF_ZONE(debug_info_print), "   Info =",GET_NAME_OF_INFO_ZONE(debug_info_print), "   Location Coords =", debug_info_print)
							SETTIMERA(0)
							FPSlocationStage = WAIT_TO_SETTLE
						ENDIF
						
					BREAK

					CASE WAIT_TO_SETTLE
			
						IF TIMERA() > 1000
							SETTIMERA(0)
							new_heading = 0
							PRINTLN("Start Capturing node #", CONVERT_INT_TO_STRING(nodenum))
							METRICS_ZONE_START(GET_STRING_FROM_VECTOR(debug_info_print))
							FPSlocationStage = CAMERA_ROTATE
						ENDIF

						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF
					BREAK
					
					CASE CAMERA_ROTATE
								
						IF TIMERA() > 10
							SETTIMERA(0)
							new_heading = new_heading + 0.85
							IF new_heading < 360
								SET_GAMEPLAY_CAM_RELATIVE_HEADING(new_heading)
							ELSE
								new_heading = 0
								PRINTLN("Stop Capturing node #", CONVERT_INT_TO_STRING(nodenum))
								METRICS_ZONE_STOP()
								PRINTLN("Save Telemetry #", CONVERT_INT_TO_STRING(nodenum))
								METRICS_ZONE_SAVE_TELEMETRY()
//								METRICS_ZONE_SAVE_TO_FILE(GET_STRING_FROM_VECTOR(debug_info_print)) //TEST
								METRICS_ZONES_CLEAR()
								FPSlocationStage = CLEANUP_ZONE
							ENDIF
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) //Debug skip camera rotate
							new_heading = 0
							FPSlocationStage = CLEANUP_ZONE
						ENDIF

						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
						ENDIF						
					BREAK
					
					CASE CLEANUP_ZONE

						IF NOT GET_COMMANDLINE_PARAM_EXISTS("reverse_fullmap_test")
							IF FPSlocation.y <= grid_end_y
								PRINTLN("TELEMETRY SAMPLE #", nodenum, " was captured ok ", debug_info_print)
								nodenum ++
								FPSlocationStage = WARP_TO_LOCATION
							ENDIF	
						ELSE
							IF FPSlocation.y >= grid_end_y
								PRINTLN("TELEMETRY SAMPLE #", nodenum, " was captured ok ", debug_info_print)
								nodenum ++
								FPSlocationStage = WARP_TO_LOCATION
							ENDIF						
						ENDIF

						IF NOT GET_COMMANDLINE_PARAM_EXISTS("reverse_fullmap_test")
							IF FPSlocation.y > grid_end_y
								SETTIMERA(0)
								PRINTLN("LAST TELEMETRY SAMPLE #", nodenum, " was captured ok ", debug_info_print)
								FPSlocationStage = FPS_RESULTS
							ENDIF
						ELSE
							IF FPSlocation.y < grid_end_y
								SETTIMERA(0)
								PRINTLN("LAST TELEMETRY SAMPLE #", nodenum, " was captured ok ", debug_info_print)
								FPSlocationStage = FPS_RESULTS
							ENDIF						
						ENDIF

					BREAK
					
					CASE FPS_RESULTS
						
						IF TIMERA() > 2000
							CLEANUP_SCRIPT()
						ENDIF
						
					BREAK
					
				ENDSWITCH

			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				CLEANUP_SCRIPT()
			ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF

