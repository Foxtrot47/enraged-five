

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"

#IF IS_DEBUG_BUILD

CONST_INT	MAX_NUM_BOOTY_CALL_DEBUG_CAMS	4
CONST_INT	DEFAULT_DEBUG_CAM_PAN_TIME		3000
CONST_FLOAT	DEFAULT_DEBUG_CAM_FOV			50.0
CONST_FLOAT	DEFAULT_DEBUG_CAM_HANDSHAKE_AMP	0.3

STRUCT sBootyCallCamStruct
	
	CAMERA_INDEX	iD
	VECTOR 			vRot
	VECTOR 			vPos
	FLOAT			fFOV		=	DEFAULT_DEBUG_CAM_FOV
	INT				iPanTime	=	DEFAULT_DEBUG_CAM_PAN_TIME
	FLOAT			fHandShake	= 	DEFAULT_DEBUG_CAM_HANDSHAKE_AMP
	BOOL			bCamSet = FALSE
	
ENDSTRUCT
sBootyCallCamStruct tBCCamStruct[MAX_NUM_BOOTY_CALL_DEBUG_CAMS]


BOOTY_CALL_CONTACT_ENUM eBootyEnum = BC_STRIPPER_PEACH		//Init this to a chick that can't be taken home

WIDGET_GROUP_ID wg_BCD_Main
WIDGET_GROUP_ID wg_BCD_Scene
WIDGET_GROUP_ID wg_BCD_Camera
WIDGET_GROUP_ID wg_BCD_CameraDetails
WIDGET_GROUP_ID wg_BCD_Playback

INT iWhichBCsHouse = 0
INT iPrevBCHouse = 0

INT iWhichBCCam = 0
INT iPrevBCCam = -1
INT iNumCamsSet = 0

BOOL bLoadScript
BOOL bGoToHouse
BOOL bPlayCustomScene
BOOL bClearCam
BOOL bSceneRendering

INT iWhichSeq = 0
BOOL bDeleteDebugOutputFile
BOOL bTerminateScript
//BOOL bShouldSave
BOOL bSaveOnExit			= TRUE

BOOL bGrabCameraInfo
BOOL bLoadCameraInfo
BOOL bSaveCameraToText

structTimer timerSceneTime

/*
BOOL bKnockBoots
BOOL bHidePlayer
BOOL bHideBootyCall
*/

USING "bootycall_debug.sch"



PROC BootyCallDebugController_Cleanup()
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	SET_DEBUG_CAM_ACTIVE(FALSE,TRUE)
	
	DESTROY_BC_CAMS_SAFE()
	
	IF DOES_WIDGET_GROUP_EXIST(wg_BCD_Main)
		DELETE_WIDGET_GROUP(wg_BCD_Main)
	ENDIF
	
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("stripperhome")
	
	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC
#ENDIF


SCRIPT
	#IF IS_DEBUG_BUILD
		PRINTLN("Starting bootycall_debug_controller.sc")
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC_DEBUG CONTROLLER] Starting bootycall_debug_controller.sc")
		
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		
		// This script needs to cleanup only when the game runs the magdemo
		IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC_DEBUG CONTROLLER]...bootycall_debug_controller.sc has been forced to cleanup (MAGDEMO)")
			BootyCallDebugController_Cleanup()
		ENDIF
		
		// Additional debug items
		CREATE_BOOTY_CALL_FINAL_SCENE_MODE(wg_BCD_Main)

		WHILE (TRUE)
			WAIT(0)
			
			BootyCallDebugController_UpdateWidgets()
				
			IF NOT g_bTurnOnLukeHCameraDebug
				IF bSaveOnExit
					bSaveCameraToText = TRUE
					BootyCallDebug_WriteCamsToDebugFile()
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC DEBUG.SCH] Save on Exit :)   ")
			
				ENDIF
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[BC_DEBUG CONTROLLER]...bootycall_debug_controller.sc has been forced to cleanup (not running)")
				BootyCallDebugController_Cleanup()
			ENDIF
		ENDWHILE		
	#ENDIF
	
	TERMINATE_THIS_THREAD()

ENDSCRIPT
