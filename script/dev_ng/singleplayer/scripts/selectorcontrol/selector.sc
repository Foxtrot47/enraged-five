//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	selector.sc													//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Displays the hotswap HUD and handles the switching between  //
//							singleplayer and multiplayer.								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

//Modifies script compiled into IS_SELECTOR_PED_AVAILABLE_IN_FLOW. -BenR
CONST_INT USE_SELECTOR_CHANGES	1

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_graphics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "script_MATHS.sch"
USING "script_player.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "stats_private.sch"
//USING "Shared_hud_displays.sch"
USING "net_hud_activating.sch"
USING "MP_SkyCam.sch"
USING "Screens_header.sch"
USING "Transition_Common.sch"
USING "PM_MissionCreator_Public.sch"
USING "net_player_headshots.sch"
USING "flow_public_GAME.sch"
using "Transition_Joining.sch"
USING "beast_secret_unlock.sch"

ENUM SELECTOR_STAGE_ENUM
	SELECTOR_INITIALISE = 0,
	SELECTOR_CONTROLLER,
	SELECTOR_END
ENDENUM
SELECTOR_STAGE_ENUM eSelectorStage = SELECTOR_INITIALISE

SELECTOR_PED_STRUCT sSelectorPeds
CONST_FLOAT F_STAT_X_POSITION  0.056
FLOAT fPLAYER_STATS_X = F_STAT_X_POSITION	//0.032//0.0375//0.225
FLOAT fPLAYER_STATS_Y = 0.006//0.003//0.035
FLOAT fPLAYER_STATS_W = 0.140625//0.5
FLOAT fPLAYER_STATS_H = 0.3875//0.149
FLOAT fPLAYER_STATS_SCALE_W = 1.0
FLOAT fPLAYER_STATS_SCALE_H = 1.0

//FLOAT fPLAYER_SWITCH_X = 0.053
FLOAT fPLAYER_SWITCH_Y = 0.087
FLOAT fPLAYER_SWITCH_W = 0.098
FLOAT fPLAYER_SWITCH_H = 0.175
FLOAT fPLAYER_SWITCH_SCALE_W = 1.0
FLOAT fPLAYER_SWITCH_SCALE_H = 1.0

//FLOAT fPLAYER_SWITCH_WIDE_WIDE_POS_X = 0.75
FLOAT fPLAYER_SWITCH_WIDE_SCALE = 1.3333

//FLOAT fPLAYER_SWITCH_WIDE_WIDE_POS_X = 0.75
FLOAT fMISSION_NAME_X = -0.106
FLOAT fMISSION_NAME_Y = 0.635

//INT CodeExitMPCheck

// All the Scaleform related data
SCALEFORM_INDEX movieID, movieID2
BOOL bUsingPrologueMovie
BOOL bRequested
INT iState[4]
INT iEnum[4]
INT iCurrent[4]
INT iMissions[4]
BOOL bHinted[4]
BOOL bDamaged[4]
BOOL bFlashDamage[4]
INT iSelectedSlot
INT iSelectedSlotOnLoad
BOOL bSelection
BOOL bDisplay
SCRIPT_TIMER TransitionFailsafe_Timer
INT iHeadshotTextureID = -1

INT iAutoSwitchEnum[4]
INT iAutoSwitchSlot
INT iAutoSwitchDisplayTimer
BOOL bAutoSwitchDisplayTimerSet

//Purposefully "hidden" in amongst this script. We want it to be hard
//for hackers to spot these scripts in decompiled output. -BenR
#IF FEATURE_SP_DLC_BEAST_SECRET
#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastUnlockVars sBeastUnlockVars
#ENDIF
#ENDIF

BLIP_INDEX selectedPedBlip

BOOL bMPAutoBootFromStartup = FALSE

#IF USE_REPLAY_RECORDING_TRIGGERS

BOOL bAddActionReplayOn
BOOL bAddActionReplayOff
REPLAY_START_PARAM eFeedStartType
INT iRecPost[3]
INT iRecPostActionReplay = -1
INT iActionReplayTimer
TIME_DATATYPE tdActionReplayTimer
BOOL bReplayFeedDisabledLastFrame
INT iExcludeInputTimer
#ENDIF

BOOL bSwitchAvailableAudioPlayed

BOOL bSetSystemTimer

BOOL bDisplayCharacterInfo

FLOAT fLastTimeScaleSet
BOOL bSlowingDownTime, bReduceTimeScaleForSwitch
FLOAT fTargetScale
INT iTimeScaleTimer

BOOL bSetPedHead
BOOL bPedHeadSet
INT iLoadPedHeadStage
STRING sMPPedHeadshot
INT iPedHeadSlot

BOOL bSuppressUIForShortSwitch
INT iSuppressRadarTimer
INT iLastSwitchFrameCount

BOOL bUpdateCountdown
BOOL bForceUpdateCountdown
INT iCountdownTimer
TIME_DATATYPE tdCountdownTimer

BOOL bMapUpdated

INT iIndicatorSFX[2]
BOOL bHintIndicatorNotTriggered

BOOL bDisabledLastFrame

INT iSwitchActiveTimer
BOOL bSwitchActiveBlock

BOOL bCleanupMovies
INT iCleanupMoviesTimer
//TIME_DATATYPE tdCleanupMoviesTimer

INT iLoadMoviesTimer
TIME_DATATYPE tdLoadMoviesTimer

BOOL bMatchmakingCardOnscreen

INT iLastTimeSwitchWasOnscreen

BOOL bShownPSPlusSignIn = FALSE

#IF IS_DEBUG_BUILD
WIDGET_GROUP_ID widgetID
//SkySwoop widget 
INT iSkySwoopWidget

INT iCurrentState_Debug
INT iMissionType_Debug
BOOL bTimescale_Debug

BOOL bForceLoad_Debug
BOOL bHideThisFrame_Debug
BOOL bAllowHiddenSelection_Debug

BOOL bDisplayAutoSwitchHint0, bDisplayAutoSwitchHint1, bDisplayAutoSwitchHint2

BOOL bPauseMenuActiveEX_Debug, bPauseMenuActive_Debug

PROC PRINT_DEBUG_STAT_TEXT(FLOAT fX, FLOAT fY, STRING sText)
	SET_TEXT_SCALE(0.0000, 0.2900)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", sText)
ENDPROC

PROC MAINTAIN_SELECTOR_MULTIPLAYER_QUICKLAUNCH()

	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableShiftEntryToMP")
			EXIT
		ENDIF
	
		
		
	#ENDIF

	IF g_bDeleteAllCharsQuick = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()
	
	
		//Jump Straight into a game as a cop or a mexican
//		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Cop ")
//			
//			PRINTSTRING("SELECTOR - Selected a Cop with shift-C")
//			
//			g_qlWhichTeam = QUICKLAUNCH_CNC_COP
//			g_HaveIShiftedIntoTheGame = TRUE
//	//			SET_MP_HUD_ON_SCREEN(TRUE)
//			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
//				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_M, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Mexican ") 
//			PRINTSTRING("/n SELECTOR - Selected a Mexican with shift-M")
//			g_qlWhichTeam = QUICKLAUNCH_CNC_VAGOS
//			g_HaveIShiftedIntoTheGame = TRUE
//	//			SET_MP_HUD_ON_SCREEN(TRUE)
//			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
//				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Lost MC ") 
//			PRINTSTRING("/n SELECTOR - Selected a Biker with shift-N")
//			g_qlWhichTeam = QUICKLAUNCH_CNC_BIKER
//			g_HaveIShiftedIntoTheGame = TRUE
//	//			SET_MP_HUD_ON_SCREEN(TRUE)
//			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
//				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_R, KEYBOARD_MODIFIER_CTRL, " Debug Start as a Spectator ") 
//			PRINTSTRING("/n SELECTOR - Selected a spectator with ctrl-R")
//			g_qlWhichTeam = QUICKLAUNCH_CNC_SPECTATOR
//			g_HaveIShiftedIntoTheGame = TRUE
//	//			SET_MP_HUD_ON_SCREEN(TRUE)
//			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
//				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
//			ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_O, KEYBOARD_MODIFIER_CTRL, " Debug Start FREEMODE as a Spectator ") 
			PRINTSTRING("/n SELECTOR - Selected a spectator with alt-R")
			g_qlWhichTeam = QUICKLAUNCH_FREEMODE_SPECTATOR
			g_HaveIShiftedIntoTheGame = TRUE
	//			SET_MP_HUD_ON_SCREEN(TRUE)
			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
			ENDIF
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_SHIFT, " Debug Start in FREEMODE ") 
			PRINTSTRING("/n SELECTOR - Selected a Freemode with shift-F ")
			g_qlWhichTeam = QUICKLAUNCH_FREEMODE
			g_HaveIShiftedIntoTheGame = TRUE
	//			SET_MP_HUD_ON_SCREEN(TRUE)
			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
			ENDIF
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_Q, KEYBOARD_MODIFIER_SHIFT, " Debug RETURN TO SINGLEPLAYER ") 
			PRINTSTRING("/n SELECTOR - Selected a SinglePlayer with shift-Q ")
			g_iLeaveMultiplayerState = LEAVE_MP_STATE_YES
			g_HaveIShiftedIntoTheGame = TRUE
			g_TransitionCameraState = LEAVEMPHUD_MPTOSP
			STOP_PLAYER_SWITCH()
			SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
	//			SET_MP_HUD_ON_SCREEN(TRUE)
			IF NOT (IS_TRANSITION_MENU_TRIGGERED())
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC MAINTAIN_SELECTOR_MULTIPLAYER_QUICKLAUNCH_TRANSITION()
	
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableShiftEntryToMP")
			EXIT
		ENDIF
		
	#ENDIF
	
	IF g_bDeleteAllCharsQuick = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()

		//Jump Straight into a game as a cop or a mexican
		
//		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Cop ")	
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Cop with shift-C")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_COP)
//			ELSE
//				PRINTSTRING("SELECTOR - Selected a Cop with shift-C")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_COP)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_M, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Mexican ") 
//			PRINTSTRING("/n SELECTOR - Selected a Mexican with shift-M")
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Vagos with shift-M")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_VAGOS)
//			ELSE
//				PRINTSTRING("SELECTOR - Selected a Vagos with shift-M")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_VAGOS)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_SHIFT, " Debug Start as a Lost MC ") 
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Biker with shift-N")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_BIKER)
//			ELSE
//				PRINTSTRING("/n SELECTOR - Selected a Biker with shift-N")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_BIKER)
//			ENDIF
//		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_R, KEYBOARD_MODIFIER_CTRL, " Debug Start as a Spectator") 
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Spectator with shift-R")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_SPECTATOR)
//			ELSE
//				PRINTSTRING("/n SELECTOR - Selected a Biker with ctrl-R")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SPECTATOR)
//			ENDIF
//		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_O, KEYBOARD_MODIFIER_CTRL, " Debug Start as FREEMODE a Spectator") 
//			PRINTSTRING("/n SELECTOR - Selected a Freemode as spectator with alt-R ")
//			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_FREEMODE_SPECTATOR)
//			STOP_PLAYER_SWITCH()
//		
		IF ((IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_SHIFT, " Debug Start in FREEMODE ")) OR ((GET_COMMANDLINE_PARAM_EXISTS("sc_botperformancetest") AND (g_bAlreadySkippedIntoFreemode=FALSE))))
			PRINTSTRING("/n SELECTOR - Selected a Freemode with shift-F ")
			g_bAlreadySkippedIntoFreemode = TRUE
			STOP_PLAYER_SWITCH()
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_FREEMODE)
		ELIF (GET_COMMANDLINE_PARAM_EXISTS("sc_mpUnlockAllFreemode") AND NOT g_bAlreadySkippedIntoFreemode)
			PRINTSTRING("/n SELECTOR - launch Freemode with sc_mpUnlockAllFreemode command line ")
			g_bAlreadySkippedIntoFreemode = TRUE
			STOP_PLAYER_SWITCH()
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_FREEMODE)
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_Q, KEYBOARD_MODIFIER_SHIFT, " Debug RETURN TO SINGLEPLAYER ") 
			PRINTSTRING("/n SELECTOR - Selected a SinglePlayer with shift-Q ")
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_SP)
			STOP_PLAYER_SWITCH()
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_Q, KEYBOARD_MODIFIER_CTRL, " Debug RETURN TO SINGLEPLAYER - EVERYONE ") 
			PRINTSTRING("/n SELECTOR - Selected a SinglePlayer Kicker with Ctrl-Q ")
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_SP)
			STOP_PLAYER_SWITCH()
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bKickEveryoneOutSessionTrigger = TRUE
			g_iKickEveryoneOutSessionPlayerNumber = NATIVE_TO_INT(PLAYER_ID())
			
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_C, KEYBOARD_MODIFIER_ALT, " Debug Start as a Cop with tutorial ")	
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Cop with alt-C")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_COP_WTUTORIAL)
//			ELSE
//				PRINTSTRING("SELECTOR - Selected a Cop with alt-C")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_COP_WTUTORIAL)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_ALT, " Debug Start as a Vagos with tutorial ") 
//			PRINTSTRING("/n SELECTOR - Selected a Vagos with alt-V")
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Vagos with alt-V")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_VAGOS_WTUTORIAL)
//			ELSE
//				PRINTSTRING("SELECTOR - Selected a Vagos with alt-V")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_VAGOS_WTUTORIAL)
//			ENDIF
//		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_ALT, " Debug Start as a Lost MC with tutorial ") 
//			IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
//				PRINTSTRING("SELECTOR - Selected Swap Team to a Lost MC with alt-N")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_BIKER_WTUTORIAL)
//			ELSE
//				PRINTSTRING("/n SELECTOR - Selected a Lost MC with alt-N")
//				SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_BIKER_WTUTORIAL)
//			ENDIF
		ENDIF
	ENDIF
	
	/*
	IF g_binitiatejoinleavetest = TRUE
	
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		//AND IS_PLAYER_PLAYING(PLAYER_ID())
			PRINTSTRING("/n Switch to Freemode with automated test ")
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_FREEMODE)
		ENDIF
		
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		//AND IS_PLAYER_PLAYING(PLAYER_ID())
			PRINTSTRING("/n Switch to CNC Mexican gang automated test")
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_CNC_SWAPTO_VAGOS)
		ENDIF
		
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_CnC
		//AND IS_PLAYER_PLAYING(PLAYER_ID())
			PRINTSTRING("/n Switch to Single Player with automated test")
			SET_QUICKLAUNCH_STATE(QUICKLAUNCH_SP)
		ENDIF
	
	ENDIF
	
	*/
ENDPROC

PROC INITIALISE_SELECTOR_WIDGETS()
	widgetID = START_WIDGET_GROUP("Selector Debug")
		
		
		ADD_WIDGET_BOOL("g_sSelectorUI.bMustReleaseSelectorUIButton", g_sSelectorUI.bMustReleaseSelectorUIButton)
		ADD_WIDGET_BOOL("g_sSelectorUI.bMustReleaseCancelButton", g_sSelectorUI.bMustReleaseCancelButton)
		ADD_WIDGET_BOOL("g_sSelectorUI.bMustReleaseAcceptButton", g_sSelectorUI.bMustReleaseAcceptButton)
		ADD_WIDGET_BOOL("g_sSelectorUI.bMustReleaseSquareButton", g_sSelectorUI.bMustReleaseSquareButton)
		
		ADD_WIDGET_BOOL("PAUSE MENU ACTIVE EX", bPauseMenuActiveEX_Debug)
		ADD_WIDGET_BOOL("PAUSE MENU ACTIVE", bPauseMenuActive_Debug)
		
		ADD_WIDGET_BOOL("g_bGTAOnlineAvailable", g_bGTAOnlineAvailable)
		
		ADD_WIDGET_BOOL("bDisplayAutoSwitchHint0", bDisplayAutoSwitchHint0)
		ADD_WIDGET_BOOL("bDisplayAutoSwitchHint1", bDisplayAutoSwitchHint1)
		ADD_WIDGET_BOOL("bDisplayAutoSwitchHint2", bDisplayAutoSwitchHint2)
		
		//ADD_WIDGET_FLOAT_SLIDER("PLAYER SWITCH X", fPLAYER_SWITCH_X, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER SWITCH Y", fPLAYER_SWITCH_Y, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER SWITCH W", fPLAYER_SWITCH_W, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER SWITCH H", fPLAYER_SWITCH_H, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fPLAYER SWITCH SCALE W", fPLAYER_SWITCH_SCALE_W, 0.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fPLAYER SWITCH SCALE H", fPLAYER_SWITCH_SCALE_H, 0.0, 2.0, 0.001)
		//ADD_WIDGET_FLOAT_SLIDER("fPLAYER SWITCH WIDE POS X", fPLAYER_SWITCH_WIDE_WIDE_POS_X, 0.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fPLAYER SWITCH WIDE SCALE", fPLAYER_SWITCH_WIDE_SCALE, 0.0, 2.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS X", fPLAYER_STATS_X, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS Y", fPLAYER_STATS_Y, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS W", fPLAYER_STATS_W, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS H", fPLAYER_STATS_H, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS SCALE W", fPLAYER_STATS_SCALE_W, 0.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PLAYER STATS SCALE H", fPLAYER_STATS_SCALE_H, 0.0, 2.0, 0.001)
		
		ADD_WIDGET_FLOAT_SLIDER("fMISSION_NAME_X", fMISSION_NAME_X, -2.0, 2.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("fMISSION_NAME_Y", fMISSION_NAME_Y, -2.0, 2.0, 0.001)
		
		ADD_WIDGET_BOOL("Activate Debug Display", g_sSelectorUI.debug_bDisplay)
		ADD_WIDGET_BOOL("SWITCH cam active", g_bSelectorCamActive)
		
		ADD_WIDGET_STRING("Switch UI states")
		ADD_WIDGET_INT_READ_ONLY("eUIstateCurrent", iCurrentState_Debug)
		ADD_WIDGET_BOOL("bDisabled", g_sSelectorUI.bDisabled)
		ADD_WIDGET_BOOL("bDisabledThisFrame", g_sSelectorUI.bDisabledThisFrame)
		ADD_WIDGET_BOOL("bDisableForEndOfMission", g_sSelectorUI.bDisableForEndOfMission)
		ADD_WIDGET_BOOL("bHiddenThisFrame", g_sSelectorUI.bHiddenThisFrame)
		ADD_WIDGET_BOOL("bDisplay", g_sSelectorUI.bDisplay)
		ADD_WIDGET_BOOL("bRequested", bRequested)
		ADD_WIDGET_BOOL("bLoaded", g_sSelectorUI.bLoaded)
		ADD_WIDGET_BOOL("bSetup", g_sSelectorUI.bSetup)
		ADD_WIDGET_BOOL("bOnScreen", g_sSelectorUI.bOnScreen)
		ADD_WIDGET_BOOL("bMissionUpdate", g_sSelectorUI.bMissionUpdate)
		ADD_WIDGET_BOOL("bCharAvailable", g_sSelectorUI.bCharAvailable)
		ADD_WIDGET_BOOL("bFirstSelectionMade", g_sSelectorUI.bFirstSelectionMade)
		ADD_WIDGET_BOOL("bForceUpdate", g_sSelectorUI.bForceUpdate)
		ADD_WIDGET_BOOL("bSelection", bSelection)
		ADD_WIDGET_BOOL("bForceUpdateCountdown", bForceUpdateCountdown)
		ADD_WIDGET_BOOL("bForceLoad_Debug", bForceLoad_Debug)
		ADD_WIDGET_BOOL("bHideThisFrame_Debug", bHideThisFrame_Debug)
		ADD_WIDGET_BOOL("bAllowHiddenSelection_Debug", bAllowHiddenSelection_Debug)
		ADD_WIDGET_BOOL("bSuppressUIForShortSwitch", bSuppressUIForShortSwitch)
		
		ADD_WIDGET_INT_READ_ONLY("iSelectedSlot", g_sSelectorUI.iSelectedSlot)
		ADD_WIDGET_INT_READ_ONLY("iSelectedSlotOnLoad", iSelectedSlotOnLoad)
		
		ADD_WIDGET_INT_READ_ONLY("Current mission type", iMissionType_Debug)
		
		ADD_WIDGET_BOOL("bTimescale_Debug", bTimescale_Debug)
		
		
		
		ADD_WIDGET_BOOL("bSFX_BlockAudioCalls", g_sSelectorUI.bSFX_BlockAudioCalls)
		ADD_WIDGET_INT_READ_ONLY("SkySwoopStage", iSkySwoopWidget)
		
		//ADD_WIDGET_BOOL("g_bKillFMMCmenu", g_bKillFMMCmenu)
	//	ADD_WIDGET_BOOL("g_FMMC_STRUCT.bKillFMMC", g_FMMC_STRUCT.bKillFMMC)
		
		ADD_WIDGET_STRING("Code switch")
		ADD_WIDGET_BOOL("g_bUseTransitionCamera", g_sSelectorUI.debug_bUseTransitionCamera)
		ADD_WIDGET_FLOAT_SLIDER("g_fSwitchPedAfterPhase", g_sSelectorUI.debug_fSwitchPedAfterPhase, 0.0, 1.0, 0.1)
	STOP_WIDGET_GROUP()
	WidgetID = widgetID		//compile fix
	
	g_sSelectorUI.debug_fSwitchPedAfterPhase = 0.5
ENDPROC

PROC UPDATE_SELECTOR_WIDGETS()
	
	iCurrentState_Debug = ENUM_TO_INT(g_sSelectorUI.eUIStateCurrent)
	iMissionType_Debug = ENUM_TO_INT(g_OnMissionState)
	
	bPauseMenuActiveEX_Debug = IS_PAUSE_MENU_ACTIVE_EX()
	bPauseMenuActive_Debug = IS_PAUSE_MENU_ACTIVE()
	
	IF bForceLoad_Debug
		FORCE_LOAD_SELECTOR_THIS_FRAME()
	ENDIF
	IF bHideThisFrame_Debug
		HIDE_SELECTOR_THIS_FRAME(TRUE, bAllowHiddenSelection_Debug)
	ENDIF
	
	IF bTimescale_Debug
		IF NOT g_bDrawLiteralSceneString
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDrawLiteralSceneString = TRUE
		ENDIF
		
		TEXT_LABEL_63 str
		
		str  = "fTargetScale: "
		str += GET_STRING_FROM_FLOAT(fTargetScale)
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID()), -5.0, HUD_COLOUR_PURE_WHITE)
		
		str  = "fLastTimeScaleSet: "
		str += GET_STRING_FROM_FLOAT(fLastTimeScaleSet)
		DrawDebugSceneTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID()), -4.0, HUD_COLOUR_PURE_WHITE)
		
	ENDIF
	
	IF bDisplayAutoSwitchHint0
		DISPLAY_SELECTOR_PED_AUTOSWITCH_UI(SELECTOR_PED_MICHAEL)
		bDisplayAutoSwitchHint0 = FALSE
	ENDIF
	IF bDisplayAutoSwitchHint1
		DISPLAY_SELECTOR_PED_AUTOSWITCH_UI(SELECTOR_PED_FRANKLIN)
		bDisplayAutoSwitchHint1 = FALSE
	ENDIF
	IF bDisplayAutoSwitchHint2
		DISPLAY_SELECTOR_PED_AUTOSWITCH_UI(SELECTOR_PED_TREVOR)
		bDisplayAutoSwitchHint2 = FALSE
	ENDIF
	
	IF g_sSelectorUI.debug_bDisplay
		INT i
		TEXT_LABEL_63 sTempLabel
		REPEAT 4 i
			IF DOES_ENTITY_EXIST(g_sSelectorUI.debug_pedID[i])
				IF NOT DOES_BLIP_EXIST(g_sSelectorUI.debug_blipID[i])
					g_sSelectorUI.debug_blipID[i] = ADD_BLIP_FOR_ENTITY(g_sSelectorUI.debug_pedID[i])
					IF i = 0
						SET_BLIP_SPRITE(g_sSelectorUI.debug_blipID[i], RADAR_TRACE_MICHAEL_FAMILY)
					ELIF i = 1
						SET_BLIP_SPRITE(g_sSelectorUI.debug_blipID[i], RADAR_TRACE_TREVOR_FAMILY)
					ELIF i = 2
						SET_BLIP_SPRITE(g_sSelectorUI.debug_blipID[i], RADAR_TRACE_FRANKLIN_FAMILY)
					ENDIF
					SET_BLIP_FLASHES(g_sSelectorUI.debug_blipID[i], FALSE)
					g_sSelectorUI.debug_bBlipFlashing[i] = FALSE
				ENDIF
				IF NOT IS_PED_INJURED(g_sSelectorUI.debug_pedID[i])
					IF g_sSelectorUI.debug_bBlipFlashing[i]
						SET_BLIP_FLASHES(g_sSelectorUI.debug_blipID[i], FALSE)
						g_sSelectorUI.debug_bBlipFlashing[i] = FALSE
					ENDIF
				ELSE
					IF NOT g_sSelectorUI.debug_bBlipFlashing[i]
						SET_BLIP_FLASHES(g_sSelectorUI.debug_blipID[i], TRUE)
						g_sSelectorUI.debug_bBlipFlashing[i] = TRUE
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(g_sSelectorUI.debug_blipID[i])
					REMOVE_BLIP(g_sSelectorUI.debug_blipID[i])
				ENDIF
			ENDIF
			
			IF i = 0	sTempLabel = "pedID[SELECTOR_PED_MICHAEL] = "		ENDIF
			IF i = 1	sTempLabel = "pedID[SELECTOR_PED_FRANKLIN] = "		ENDIF
			IF i = 2	sTempLabel = "pedID[SELECTOR_PED_TREVOR] = "		ENDIF
			IF i = 3	sTempLabel = "pedID[SELECTOR_PED_MULTIPLAYER] = "	ENDIF
			
			IF NOT DOES_ENTITY_EXIST(g_sSelectorUI.debug_pedID[i])
			OR IS_PED_INJURED(g_sSelectorUI.debug_pedID[i])
				sTempLabel += "DEAD"
			ELSE
				sTempLabel += "ALIVE"
			ENDIF
			IF g_sSelectorUI.debug_bHintState[i]
				sTempLabel += "-HINTED"
			ENDIF
			IF g_sSelectorUI.debug_bBlockState[i]
				sTempLabel += "-BLOCKED"
			ENDIF
			
			IF g_sSelectorUI.debug_iState[i] =  0
				sTempLabel += "-NOTSET"
			ELIF g_sSelectorUI.debug_iState[i] = 1 // AVAILABLE = 1
				sTempLabel += "-AVAILABLE"
			ELIF g_sSelectorUI.debug_iState[i] = 2 // UNAVAIL = 2
				sTempLabel += "-UNAVAIL"
			ELIF g_sSelectorUI.debug_iState[i] = 3 // NOTMET= 3
				sTempLabel += "-NOTMET"
			ENDIF
			
			SET_TEXT_SCALE(0.0, 0.3)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_CENTRE(FALSE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.0500, 0.375+(i*0.03), "STRING", sTempLabel)	
		ENDREPEAT
	ELSE
		INT i
		REPEAT 3 i
			IF DOES_BLIP_EXIST(g_sSelectorUI.debug_blipID[i])
				REMOVE_BLIP(g_sSelectorUI.debug_blipID[i])
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC UPDATE_SKYSWOOP_WIDGETS()
	SKYSWOOP Swoop = GET_SKYSWOOP_STAGE()	
	SWITCH Swoop
		CASE SKYSWOOP_NONE
			iSkySwoopWidget = 0 
		BREAK
		CASE SKYSWOOP_GOINGUP
			iSkySwoopWidget = 1 
		BREAK
		CASE SKYSWOOP_INSKYSTATIC
			iSkySwoopWidget = 2
		BREAK
		CASE SKYSWOOP_INSKYMOVING
			iSkySwoopWidget = 3
		BREAK
		CASE SKYSWOOP_GOINGDOWN
			iSkySwoopWidget = 4 
		BREAK
	ENDSWITCH 

ENDPROC
	
#ENDIF

PROC GET_STAT_BOX_X_POS( FLOAT &fXPosition)
	
	INT fX, fY
	GET_ACTUAL_SCREEN_RESOLUTION( fX, fY )
	FLOAT fPhysAspect = ( TO_FLOAT( fX ) / TO_FLOAT( fY ) )
	FLOAT fFakeAspect = GET_ASPECT_RATIO( FALSE )
	
	//	Fix for B* 2264324 - Problems with selector position when in aspect over 3:2
	IF fPhysAspect > ( 3.0 / 2.0 )
	AND fPhysAspect < 3.5	//	Below triple head physical aspect ratio.
		IF fFakeAspect = ( 16.0 / 9.0 )
		OR fFakeAspect = ( 17.0 / 9.0 )
		OR ( fFakeAspect = ( 21.0 / 9.0 ) AND fPhysAspect >= ( 17.0 / 9.0 ) )
			fXPosition = 0.042
		ELIF fFakeAspect = ( 5.0 / 3.0 )
			fXPosition = 0.045						//	Few hard coded values depending on the aspect ratio
		ELIF fFakeAspect = ( 16.0 / 10.0 )
			fXPosition = 0.047
		ELIF fPhysAspect % fFakeAspect = 0.0
			fXPosition = 0.047						//	If the aspect ratio is the same as the physical aspect
		ELSE										//	and hasn't been caught by standard ratio above
			fXPosition = F_STAT_X_POSITION
		ENDIF
	ELSE
		fXPosition = F_STAT_X_POSITION
	ENDIF
	
ENDPROC


/// PURPOSE: Clears all the runtime states to match ambient setup
PROC MAINTAIN_SELECTOR_RUNTIME_STATES()
	g_sSelectorUI.bForceUpdate = FALSE
	g_sSelectorUI.bFakeStateUpdated = FALSE
	g_sSelectorUI.bCharAvailable = FALSE
	g_sSelectorUI.bSelection = FALSE
	g_sSelectorUI.bMissionUpdate = FALSE
	g_sSelectorUI.bCheckPlayerControl = TRUE
	g_sSelectorUI.bCheckActivationButton = TRUE
	g_sSelectorUI.bAllowWhenDead = FALSE
	g_sSelectorUI.bHiddenThisFrame = FALSE
	g_sSelectorUI.bAllowSelectionWhenHidden = FALSE
	g_sSelectorUI.bForceLoadThisFrame = FALSE
	g_sSelectorUI.bDisableMapOverrideThisFrame = FALSE
	g_sSelectorUI.bHideUiForSwitch = FALSE
	
	
	IF bDisabledLastFrame
	AND NOT g_sSelectorUI.bDisabledThisFrame
		PRINTLN("DISABLE_SELECTOR_THIS_FRAME() stopped getting called")
		#IF IS_DEBUG_BUILD
			g_sSelectorUI.tlDisableScript = ""
		#ENDIF
	ENDIF
	bDisabledLastFrame = g_sSelectorUI.bDisabledThisFrame
	g_sSelectorUI.bDisabledThisFrame = FALSE
	
	#IF USE_REPLAY_RECORDING_TRIGGERS
	IF bReplayFeedDisabledLastFrame
	AND NOT g_sSelectorUI.bReplayFeedDisabledThisFrame
		PRINTLN("DISABLE_REPLAY_RECORDING_UI_THIS_FRAME() stopped getting called")
		#IF IS_DEBUG_BUILD
			g_sSelectorUI.tlDisableReplayFeedScript = ""
		#ENDIF
	ENDIF
	bReplayFeedDisabledLastFrame = g_sSelectorUI.bReplayFeedDisabledThisFrame
	g_sSelectorUI.bReplayFeedDisabledThisFrame = FALSE
	#ENDIF
	
	IF g_sSelectorUI.bDisableForEndOfMission
		IF (GET_GAME_TIMER() - g_sSelectorUI.iEndOfMissionTimer) > 5000
		OR NETWORK_IS_GAME_IN_PROGRESS()
			g_sSelectorUI.bDisableForEndOfMission = FALSE
		ENDIF
	ENDIF
	
	bMatchmakingCardOnscreen = g_Show_Lap_Dpad // we cache this value so that it's state change can be handled a frame later.
	
	#IF USE_REPLAY_RECORDING_TRIGGERS
	IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
	AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
		g_sSelectorUI.bMustReleaseSelectorUIButton = FALSE
	ENDIF
	IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		g_sSelectorUI.bMustReleaseCancelButton = FALSE
	ENDIF
	IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
	AND NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
		g_sSelectorUI.bMustReleaseAcceptButton = FALSE
	ENDIF
	IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	AND NOT IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		g_sSelectorUI.bMustReleaseSquareButton = FALSE
	ENDIF
	#ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////
///    Scaleform related procedures
///    

/// PURPOSE: Requests the selector UI Scaleform movie
PROC LOAD_SELECTOR_UI()

	IF NOT g_sSelectorUI.bLoaded
	
		IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iLoadMoviesTimer) > 300)
		OR (NETWORK_IS_GAME_IN_PROGRESS() AND (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdLoadMoviesTimer, 300)) OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdLoadMoviesTimer)) > 1000))
		
			IF NOT NETWORK_IS_GAME_IN_PROGRESS() OR NOT bMatchmakingCardOnscreen
			
				IF NOT bRequested
					IF MISSION_FLOW_GET_RUNNING_MISSION() = SP_MISSION_PROLOGUE
						movieID = REQUEST_SCALEFORM_MOVIE("PLAYER_SWITCH_PROLOGUE")
						bUsingPrologueMovie = TRUE
					ELSE
						movieID = REQUEST_SCALEFORM_MOVIE("PLAYER_SWITCH")
						movieID2 = REQUEST_SCALEFORM_MOVIE("PLAYER_SWITCH_STATS_PANEL")
						bUsingPrologueMovie = FALSE
					ENDIF
					
					PRINTSTRING("\n LOAD_SELECTOR_UI - Requesting scaleform movies.")PRINTNL()
					bRequested = TRUE
					//g_sSelectorUI.iSFX_Display = -1
				ENDIF
				
				bSwitchAvailableAudioPlayed = FALSE
				g_sSelectorUI.bForceUpdate = TRUE
				
				g_sSelectorUI.bLoaded = FALSE
				IF HAS_SCALEFORM_MOVIE_LOADED(movieID)
				AND (bUsingPrologueMovie OR HAS_SCALEFORM_MOVIE_LOADED(movieID2))
				
					IF NOT bSetSystemTimer
						PRINTLN("LOAD_SELECTOR_UI - Setting scaleform movie to use system time")
						SET_SCALEFORM_MOVIE_TO_USE_SYSTEM_TIME(movieID, TRUE)
						bSetSystemTimer = TRUE
					ENDIF
				
					g_sSelectorUI.bLoaded = TRUE
				ENDIF
			ELSE
				PRINTSTRING("\n LOAD_SELECTOR_UI - Waiting for matchmaking card to be removed before requesting scaleform movies.")PRINTNL()
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF NETWORK_IS_GAME_IN_PROGRESS()
					PRINTSTRING("\n LOAD_SELECTOR_UI - Waiting for load timer to pass before requesting scaleform movies. Time diff=")PRINTINT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdLoadMoviesTimer)))PRINTNL()
				ELSE
					PRINTSTRING("\n LOAD_SELECTOR_UI - Waiting for load timer to pass before requesting scaleform movies.")PRINTNL()
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	iCleanupMoviesTimer = GET_GAME_TIMER()
//	IF NETWORK_IS_GAME_IN_PROGRESS()
//		tdCleanupMoviesTimer = GET_NETWORK_TIME()
//	ENDIF
ENDPROC

#IF USE_REPLAY_RECORDING_TRIGGERS

STRUCT REPLAY_DISPLAY_DATA
	INT iGameTime
	TIME_DATATYPE tdNetworkTime
	BOOL bRequestFeed
ENDSTRUCT
REPLAY_DISPLAY_DATA sReplayData

PROC UPDATE_REPLAY_RECORDING_FEED_REQUEST(BOOL &bDisplayReplayFeedIcons)
	IF bDisplayReplayFeedIcons
		// Set the time we first request the feed so we can add a small delay for displaying.
		// This allows us to ignore the tapping of dpad_down.
		IF NOT sReplayData.bRequestFeed
			sReplayData.iGameTime = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sReplayData.tdNetworkTime = GET_NETWORK_TIME()
			ENDIF
			sReplayData.bRequestFeed = TRUE
		ENDIF
		
		IF NOT g_sSelectorUI.bDisplay
			IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - sReplayData.iGameTime) < SELECTOR_UI_AMBIENT_SP_TIME_msec)
			OR (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sReplayData.tdNetworkTime)) < SELECTOR_UI_AMBIENT_MP_TIME_msec)
				bDisplayReplayFeedIcons = FALSE
				
				#IF IS_DEBUG_BUILD
					IF NETWORK_IS_GAME_IN_PROGRESS()
						PRINTLN("UPDATE_REPLAY_RECORDING_FEED_REQUEST - FALSE: Initial delay, will start in ", 300-ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sReplayData.tdNetworkTime)))
					ELSE
						PRINTLN("UPDATE_REPLAY_RECORDING_FEED_REQUEST - FALSE: Initial delay, will start in ", 300-(GET_GAME_TIMER() - sReplayData.iGameTime))
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		sReplayData.bRequestFeed = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_REPLAY_RECORDING_UI_SAFE_TO_USE()
	
	#IF IS_DEBUG_BUILD
		BOOL bOutputDebug
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
			bOutputDebug = TRUE
		ENDIF
	#ENDIF
	
	IF (g_sSelectorUI.bReplayFeedDisabledThisFrame)
	OR (IS_PED_INJURED(PLAYER_PED_ID()))
	OR (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
	OR (IS_PLAYER_SWITCH_IN_PROGRESS())
	OR (IS_PLAYER_PED_SWITCH_IN_PROGRESS())
	OR (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD))
	OR (IS_CUTSCENE_PLAYING())
	OR (IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS())
	OR (NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()) AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("golf")) = 0)
	OR (IS_PLAYER_BEING_ARRESTED(PLAYER_ID()))
	OR (IS_REPLAY_BEING_PROCESSED())
	OR (IS_TRANSITION_ACTIVE())
	OR (IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN())
	OR (IS_RESULT_SCREEN_DISPLAYING())
	OR (IS_TRANSITION_ACTIVE())
	OR (IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP())
	OR (IS_CUSTOM_MENU_ON_SCREEN())
	OR (NOT IS_SKYSWOOP_AT_GROUND())
	OR (IS_PHONE_ONSCREEN())
	OR (IS_PAUSE_MENU_ACTIVE())
	OR (IS_SYSTEM_UI_BEING_DISPLAYED())
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) > 0)
	OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("creator")) > 0)
	OR (IS_BROWSER_OPEN())
	OR (g_bHeistEndscreenDisplaying)
	OR (IS_PLAYER_CHANGING_CLOTHES())
	OR (NOT IS_REPLAY_INITIALIZED())
	OR (NOT IS_REPLAY_AVAILABLE())
	OR IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_GETTING_LAP_DANCE)
	OR (NETWORK_IS_GAME_IN_PROGRESS() AND IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID()))
	#IF FEATURE_GEN9_STANDALONE
	OR NOT HAS_STORY_ENTITLEMENT()	
	#ENDIF
		#IF IS_DEBUG_BUILD
			IF bOutputDebug
				IF (g_sSelectorUI.bReplayFeedDisabledThisFrame)											PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Disabled this frame")
				ELIF (IS_PED_INJURED(PLAYER_PED_ID()))													PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Ped injured")
				ELIF (IS_PLAYER_SWITCH_IN_PROGRESS())													PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Code switch in progress")
				ELIF (IS_PLAYER_PED_SWITCH_IN_PROGRESS())												PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Scripted switch in progress")
				ELIF (IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD))									PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: HUD transition active")
				ELIF (IS_CUTSCENE_PLAYING())															PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Cutscene playing")
				ELIF (IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS())										PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Phone active")
				ELIF (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))											PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Player control turned off")
				ELIF (NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID()))									PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Not ready for cutscene")
				ELIF (IS_REPLAY_BEING_PROCESSED())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Processing replay")
				ELIF (IS_RESULT_SCREEN_DISPLAYING())													PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Result screen displaying")
				ELIF (IS_TRANSITION_ACTIVE())															PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Transition active")
				ELIF (IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP())											PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Browsing items in shop")
				ELIF (IS_CUSTOM_MENU_ON_SCREEN())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Custom menu on screen")
				ELIF (NOT IS_SKYSWOOP_AT_GROUND())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Sky swoop not at ground")
				ELIF (IS_PHONE_ONSCREEN())																PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Phone on screen")
				ELIF (IS_PAUSE_MENU_ACTIVE())															PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Pause menu active")
				ELIF (IS_SYSTEM_UI_BEING_DISPLAYED())													PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: System UI on sreen")
				ELIF (IS_BROWSER_OPEN())																PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Browser visible")
				ELIF (g_bHeistEndscreenDisplaying)														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Heist end screen")
				ELIF (IS_PLAYER_CHANGING_CLOTHES())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Changing clothes")
				ELIF (NOT IS_REPLAY_INITIALIZED())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Replay not initialised")
				ELIF (NOT IS_REPLAY_AVAILABLE())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Replay not available")
				ELIF IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_GETTING_LAP_DANCE)	PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Player is getting a lapdance")
				ELIF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_PLAYER_DOING_A_YACHT_WARP(PLAYER_ID()))		PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: Player is moving yacht")
				#IF FEATURE_GEN9_STANDALONE
				ELIF (NOT HAS_STORY_ENTITLEMENT())														PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: No Story Entitlement")
				#ENDIF
				ELSE																					PRINTLN("IS_REPLAY_RECORDING_UI_SAFE_TO_USE - FALSE: missing")
				ENDIF
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CLEANUP_REPLAY_RECORD_FEED()
	
	IF g_sSelectorUI.bFeedAddedForRecording
		PRINTLN("SWITCH UI - [REPLAY-FEED] Cleaning up RECORDING items added for replay feed.")
		INT iPost
		REPEAT COUNT_OF(iRecPost) iPost
			IF iRecPost[iPost] != -1
				THEFEED_REMOVE_ITEM(iRecPost[iPost])
				iRecPost[iPost] = -1
			ENDIF
		ENDREPEAT
		
		// Clear the prvious temp feed message for Action Replay On/Off
		IF iRecPostActionReplay != -1
			THEFEED_REMOVE_ITEM(iRecPostActionReplay)
			iRecPostActionReplay  = -1
		ENDIF
		
		THEFEED_SET_SNAP_FEED_ITEM_POSITIONS(FALSE)
		
		THEFEED_RESUME()
		
		g_b_ReapplyStickySaveFailedFeed = FALSE
		
		g_sSelectorUI.bFeedAddedForRecording = FALSE
	ENDIF
	
	// Action Replay On
	IF bAddActionReplayOn
		PRINTLN("SWITCH UI - [REPLAY-FEED] Cleaning up ACTION REPLAY ON items added for replay feed.")
		// Clear the prvious temp feed message for Action Replay On/Off
		IF iRecPostActionReplay != -1
			THEFEED_REMOVE_ITEM(iRecPostActionReplay)
			iRecPostActionReplay  = -1
		ENDIF
	
		BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_6") 
		iRecPostActionReplay = END_TEXT_COMMAND_THEFEED_POST_REPLAY(ACTION_REPLAY, 0, "")
		bAddActionReplayOn = FALSE
		iActionReplayTimer = GET_GAME_TIMER()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tdActionReplayTimer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
	
	// Action Replay Off
	IF bAddActionReplayOff
		PRINTLN("SWITCH UI - [REPLAY-FEED] Cleaning up ACTION REPLAY OFF items added for replay feed.")
	
		// Clear the prvious temp feed message for Action Replay On/Off
		IF iRecPostActionReplay != -1
			THEFEED_REMOVE_ITEM(iRecPostActionReplay)
			iRecPostActionReplay  = -1
		ENDIF
		
		BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_7") 
		iRecPostActionReplay = END_TEXT_COMMAND_THEFEED_POST_REPLAY(ACTION_REPLAY, 0, "")
		bAddActionReplayOff = FALSE
		iActionReplayTimer = GET_GAME_TIMER()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tdActionReplayTimer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE: Marks the selector UI Scaleform movie as no longer needed and resets states
PROC CLEANUP_SELECTOR_UI()

	IF g_sSelectorUI.bLoaded
		DEBUG_PRINTCALLSTACK()
		PRINTSTRING("\n CLEANUP_SELECTOR_UI - Flagging scaleform movies to cleanup.")PRINTNL()
		bCleanupMovies = TRUE
		
		// SFX - UI on screen
		IF g_sSelectorUI.iSFX_Display != -1
		AND g_sSelectorUI.iSFX_Display_ScriptHash = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
			STOP_SOUND(g_sSelectorUI.iSFX_Display)
			PRINTLN("SWITCH UI - stopping sound: CHARACTER_CHANGE_DPAD_DOWN_MASTER")
			RELEASE_SOUND_ID(g_sSelectorUI.iSFX_Display)
			g_sSelectorUI.iSFX_Display = -1
			g_sSelectorUI.iSFX_Display_ScriptHash = 0	
			SET_AUDIO_FLAG("ActivateSwitchWheelAudio", FALSE)
		ENDIF
	ENDIF
	
	IF bMapUpdated
		IF DOES_BLIP_EXIST(selectedPedBlip)
			REMOVE_BLIP(selectedPedBlip)
		ENDIF
		SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)
		SET_ABILITY_BAR_VALUE(-1, -1)
		SET_MAX_HEALTH_HUD_DISPLAY(0)
		SET_MAX_ARMOUR_HUD_DISPLAY(0)
		UNLOCK_MINIMAP_POSITION()
	ENDIF
	bMapUpdated = FALSE
	bSelection = FALSE
	bDisplayCharacterInfo = FALSE
	iSelectedSlotOnLoad = -1
	iAutoSwitchSlot = -1
	bHintIndicatorNotTriggered = FALSE
	g_sSelectorUI.bSelection = FALSE
	g_sSelectorUI.iSelectedSlot = -1
	g_sSelectorUI.bSetup = FALSE
	g_sSelectorUI.bOnScreen = FALSE
	g_sSelectorUI.bCheckPlayerControl = TRUE
	g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HIDDEN
ENDPROC

PROC LOAD_MP_HEADSHOT()
	SWITCH iLoadPedHeadStage
		CASE 0
		
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			#IF IS_DEBUG_BUILD
			OR g_debugMenuControl.fTempPos[0] != 0.0
			#ENDIF
				bSetPedHead = TRUE
				EXIT
			ENDIF
		
		
			IF IS_PLAYER_ONLINE()
			AND NETWORK_IS_SIGNED_ONLINE()
			AND NETWORK_IS_CLOUD_AVAILABLE()
			AND IS_GTA_ONLINE_AVAILABLE()
			
			AND NOT g_bGenerateInitialTransparentHeadshot	//Don't even attempt to load a texture if we know a generate request
															//is in progress. This will save us from hammering the cloud. -BenR
				IF NOT HAS_NETWORK_TIME_STARTED()
				OR IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sExportPlayerHeadshotCountdown) 	//If time is before this timer we know
																						 	//we have a headshot request in progress.
				
					IF HAS_IMPORTANT_STATS_LOADED()
					AND IS_STAT_CHARACTER_ACTIVE(GET_ACTIVE_CHARACTER_SLOT())
						PEDHEADSHOT_SP_SWITCH_REFRESH_CHECK()
					ENDIF
					
					GAMER_HANDLE playerHandle
					playerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
					TEXT_LABEL_63 tlCloudPath
					iPedHeadSlot = GET_ACTIVE_CHARACTER_SLOT()
					tlCloudPath = Get_Headshot_dds_File(iPedHeadSlot)
					
					IF iPedHeadSlot != -1
						iHeadshotTextureID = TEXTURE_DOWNLOAD_REQUEST(playerHandle, tlCloudPath, "selector_ui_headshot", g_bUseCachedTransparentLocalPlayerHeadshot[iPedHeadSlot])
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SELECTOR === Cloud path set to ", tlCloudPath)
							CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SELECTOR === Kenneth's selector used g_bUseCachedTransparentLocalPlayerHeadshot = ", g_bUseCachedTransparentLocalPlayerHeadshot[iPedHeadSlot])
						#ENDIF
					ELSE
						iHeadshotTextureID = TEXTURE_DOWNLOAD_REQUEST(playerHandle, tlCloudPath, "selector_ui_headshot")
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SELECTOR === Cloud path set to ", tlCloudPath)
						#ENDIF
					ENDIF
					
					iLoadPedHeadStage++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF TEXTURE_DOWNLOAD_HAS_FAILED(iHeadshotTextureID)
				CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SELECTOR === Texture download failed. Flagging to generate a new headshot next time we're in MP.")
				g_bGenerateInitialTransparentHeadshot = TRUE
				iLoadPedHeadStage = 99
			ELSE
				sMPPedHeadshot = TEXTURE_DOWNLOAD_GET_NAME(iHeadshotTextureID)
				IF NOT IS_STRING_NULL_OR_EMPTY(sMPPedHeadshot)
					IF iPedHeadSlot != -1
						g_bUseCachedTransparentLocalPlayerHeadshot[iPedHeadSlot] = TRUE
					ENDIF
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SELECTOR === Kenneth's selector set sMPPedHeadshot = ", sMPPedHeadshot)
					#ENDIF
					
					bSetPedHead = TRUE
					iLoadPedHeadStage++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_MP_HEADSHOT()
	IF iHeadshotTextureID != -1
		TEXTURE_DOWNLOAD_RELEASE(iHeadshotTextureID)
	ENDIF
	iHeadshotTextureID = -1
	sMPPedHeadshot = ""
	bSetPedHead = FALSE
	bPedHeadSet = FALSE
	iLoadPedHeadStage = 0
ENDPROC



PROC REBUILD_STAT_UI()

	IF bUsingPrologueMovie
		EXIT
	ENDIF
	
	enumCharacterList eChar = NO_CHARACTER
	HUD_COLOURS eHudCol
	
	// Top
	IF g_sSelectorUI.iSelectedSlot = 0
		eChar = CHAR_FRANKLIN
		eHudCol = HUD_COLOUR_FRANKLIN
	// Right
	ELIF g_sSelectorUI.iSelectedSlot = 1
		eChar = CHAR_TREVOR
		eHudCol = HUD_COLOUR_TREVOR
	// Bottom
	ELIF g_sSelectorUI.iSelectedSlot = 2
		eChar = CHAR_MULTIPLAYER
		eHudCol = HUD_COLOUR_FREEMODE
	// Left
	ELIF g_sSelectorUI.iSelectedSlot = 3
		eChar = CHAR_MICHAEL
		eHudCol = HUD_COLOUR_MICHAEL
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(movieID2, "SET_STATS_LABELS")
		
		// ADD: Character colour
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eHudCol))
		
		// ADD: Split into 10
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL((eChar = CHAR_MULTIPLAYER))
		
		INT iStat
		REPEAT NUMBER_OF_PLAYER_STATS iStat
			IF iStat != ENUM_TO_INT(PS_SPECIAL_ABILITY)
			OR g_sSelectorUI.iSelectedSlot != 2
				// ADD: Stat value
				IF (eChar = NO_CHARACTER)
				OR (eChar = CHAR_MULTIPLAYER AND NOT HAS_IMPORTANT_STATS_LOADED())
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				ELIF (eChar = CHAR_MULTIPLAYER)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CALCULATE_PLAYER_STAT_VALUE(eChar, GET_PLAYER_STAT_FOR_DISPLAY_SLOT(iStat), FALSE, GET_ACTIVE_CHARACTER_SLOT()))
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CALCULATE_PLAYER_STAT_VALUE(eChar, GET_PLAYER_STAT_FOR_DISPLAY_SLOT(iStat), FALSE))
				ENDIF
				
				// ADD: Stat name
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_PLAYER_STAT_DISPLAY_NAME(GET_PLAYER_STAT_FOR_DISPLAY_SLOT(iStat)))
				
			ELIF iStat = ENUM_TO_INT(PS_SPECIAL_ABILITY) AND g_sSelectorUI.iSelectedSlot = 2
				
				IF HAS_IMPORTANT_STATS_LOADED()
					INT iCharCurrentXP = g_MP_STAT_CHAR_XP_FM[GET_SLOT_NUMBER(-1)]//GET_STAT_CHARACTER_XP()
					INT iRank = GET_FM_RANK_FROM_XP_VALUE(iCharCurrentXP, TRUE)
					INT iPlayersCurrent_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(iRank, FALSE)
	            	INT iPlayersNext_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(iRank+1, FALSE)
					INT iRankProgression = FLOOR((TO_FLOAT(iCharCurrentXP-iPlayersCurrent_Rank_Limit_XP)/TO_FLOAT(iPlayersNext_Rank_Limit_XP-iPlayersCurrent_Rank_Limit_XP))*100)
					
					IF g_sMPTunableArrays.iTopRankValues[MAX_FM_RANK] = iCharCurrentXP
					AND iRankProgression = 0
						iRankProgression = 100
						NET_NL()NET_PRINT("[BCSELECTOR] Change iRankProgression to 100 ")

					ENDIF

					// ADD: Stat value
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRankProgression)
					
					// ADD: Stat name
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("TR_RANKNUM")
						ADD_TEXT_COMPONENT_INTEGER(iRank)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("TR_RANK")
				ENDIF
				
				
			ENDIF
		ENDREPEAT
		
		IF (eChar = CHAR_MULTIPLAYER AND HAS_IMPORTANT_STATS_LOADED())
			// ADD: Stat value
			//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(FLOOR(GET_MP_FLOAT_PLAYER_STAT(MPPLY_PLAYER_FMENTAL_STATE)))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(FLOOR(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PLAYER_MENTAL_STATE)))
			
			// ADD: Stat name
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PCARD_MENTAL_STATE")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE: Requests the selector UI Scaleform movie and keeps states up to date
PROC MAINTAIN_SELECTOR_UI()

	BOOL bReduceTimeScale = FALSE
	BOOL bDrawStat = FALSE
	BOOL bCleanupMap = TRUE
	
	BOOL bUpdateCounter = FALSE
	BOOL bUpdateHints = FALSE
	
	BOOL bSwitchAvailableAudio = FALSE
	
	// Determine what state the UI should be in
	SELECTOR_UI_STATE_ENUM eUIStateIntended = SELECTOR_UI_HIDDEN
	
	IF IS_SELECTOR_UI_BUTTON_PRESSED(g_sSelectorUI.bCheckPlayerControl) OR IS_SELECTOR_UI_BUTTON_JUST_RELEASED(g_sSelectorUI.bCheckPlayerControl)
		g_sSelectorUI.bDisplay = IS_SELECTOR_UI_SAFE_TO_DISPLAY(TRUE)
		IF g_sSelectorUI.bDisplay
			IF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_FULL
				eUIStateIntended = SELECTOR_UI_FULL
			ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HIDDEN
				eUIStateIntended = SELECTOR_UI_HIDDEN_TO_FULL
			ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HIDDEN_TO_FULL
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(g_sSelectorUI.tdDisplayTimer, SELECTOR_UI_AMBIENT_MP_TIME_msec)) OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_sSelectorUI.tdDisplayTimer)) > SELECTOR_UI_AMBIENT_MP_TIME_msec+1000))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND NOT g_sSelectorUI.bMissionUpdate AND (GET_GAME_TIMER() - g_sSelectorUI.iDisplayTimer) > SELECTOR_UI_AMBIENT_SP_TIME_msec)
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND g_sSelectorUI.bMissionUpdate AND (GET_GAME_TIMER() - g_sSelectorUI.iDisplayTimer) > SELECTOR_UI_QUICK_SWITCH_TIME_msec)
					eUIStateIntended = SELECTOR_UI_FULL
				ELSE
					eUIStateIntended = SELECTOR_UI_HIDDEN_TO_FULL
				ENDIF
			ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT
				eUIStateIntended = SELECTOR_UI_HINT_TO_FULL
			ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT_TO_FULL
				IF (NETWORK_IS_GAME_IN_PROGRESS() AND (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(g_sSelectorUI.tdDisplayTimer, SELECTOR_UI_QUICK_SWITCH_TIME_msec)) OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_sSelectorUI.tdDisplayTimer)) > SELECTOR_UI_QUICK_SWITCH_TIME_msec+1000))
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - g_sSelectorUI.iDisplayTimer) > SELECTOR_UI_QUICK_SWITCH_TIME_msec)
					eUIStateIntended = SELECTOR_UI_FULL
				ELSE
					eUIStateIntended = SELECTOR_UI_HINT_TO_FULL
				ENDIF
			ENDIF
		ENDIF
		
	ELIF g_sSelectorUI.bMissionUpdate AND g_sSelectorUI.bCharAvailable
		g_sSelectorUI.bDisplay = IS_SELECTOR_UI_SAFE_TO_DISPLAY(FALSE)
		IF g_sSelectorUI.bDisplay
			eUIStateIntended = SELECTOR_UI_HINT
		ENDIF
		
	ELIF g_sSelectorUI.bShowAutoSwitch
		g_sSelectorUI.bDisplay = TRUE
		eUIStateIntended = SELECTOR_UI_AUTOSWITCH_HINT
		
		iAutoSwitchEnum[GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(SELECTOR_PED_MICHAEL)] = 0
		iAutoSwitchEnum[GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(SELECTOR_PED_TREVOR)] = 1
		iAutoSwitchEnum[GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(SELECTOR_PED_FRANKLIN)] = 2
		iAutoSwitchEnum[GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(SELECTOR_PED_MULTIPLAYER)] = 3
		
		IF iAutoSwitchSlot != g_sSelectorUI.iAutoSwitchSlot
			g_sSelectorUI.bForceUpdate = TRUE
			g_sSelectorUI.iAutoSwitchTimer = GET_GAME_TIMER()
			bAutoSwitchDisplayTimerSet = FALSE
		ENDIF
		
		IF g_sSelectorUI.bOnScreen
			IF NOT bAutoSwitchDisplayTimerSet
				iAutoSwitchDisplayTimer = GET_GAME_TIMER()
				bAutoSwitchDisplayTimerSet = TRUE
			ENDIF
			// Ony display this for half a second
			IF (GET_GAME_TIMER() - iAutoSwitchDisplayTimer) > 500
				g_sSelectorUI.bShowAutoSwitch = FALSE
			ENDIF
		ELSE
			// Bail out if we have not displayed it within 2 seconds.
			IF (GET_GAME_TIMER() - g_sSelectorUI.iAutoSwitchTimer) > 2000
				g_sSelectorUI.bShowAutoSwitch = FALSE
			ENDIF
			bAutoSwitchDisplayTimerSet = FALSE
		ENDIF
	ELSE
		g_sSelectorUI.bDisplay = FALSE
	ENDIF
	
	// Make sure we reset the cleanup flag if we now require the movie to be used.
	IF g_sSelectorUI.bDisplay
		bCleanupMovies = FALSE
	ENDIF
	
	// Jump to the delay state when we are force loading to prevent cleanup
	IF (eUIStateIntended = SELECTOR_UI_HIDDEN AND g_sSelectorUI.bForceLoadThisFrame)
	OR (g_sSelectorUI.bHiddenThisFrame)
		eUIStateIntended = SELECTOR_UI_HIDDEN_TO_FULL
	ENDIF
	
	IF g_sSelectorUI.eUIStateCurrent != eUIStateIntended
		// If we are cleaning up due to a switch, make sure we upadte the switch count
		IF bSelection != g_sSelectorUI.bSelection
		AND g_sSelectorUI.iSelectedSlot != 2
		AND g_sSelectorUI.bMissionUpdate
		AND NOT g_bInMultiplayer
			// Flag that we manually selected character from UI
			// Note: this flag is so we can update the mission switch count, we will rest it 
			// in take_control_of_selector_ped or when a forced switch occurs with set_current_selector_ped
			g_sSelectorUI.bManualSelection = TRUE
		ENDIF
		
		
		PRINTLN("SWITCH UI - changing to UI state ", ENUM_TO_INT(eUIStateIntended))
		
		// Set default params for new state.
		g_sSelectorUI.bFirstSelectionMade = FALSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			g_sSelectorUI.tdDisplayTimer = GET_NETWORK_TIME()
		ELSE
			g_sSelectorUI.iDisplayTimer = GET_GAME_TIMER()
		ENDIF
		
		IF eUIStateIntended != SELECTOR_UI_AUTOSWITCH_HINT
			g_sSelectorUI.bShowAutoSwitch = FALSE
		ENDIF
		
		g_sSelectorUI.bForceUpdate = TRUE
	ENDIF
	
	// Load scaleform if required or if we are already in the process of loading.
	IF g_sSelectorUI.bDisplay OR (bRequested AND NOT bCleanupMovies) OR g_sSelectorUI.bForceLoadThisFrame
		
		IF NOT g_sSelectorUI.bHiddenThisFrame
			// Prevent street name from displaying
			IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_AREA_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			ENDIF
			
			IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_VEHICLE_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			ENDIF
			IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			ENDIF
			IF NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_STREET_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			ENDIF
		ENDIF
		
		LOAD_SELECTOR_UI()
		LOAD_MP_HEADSHOT()
		TEXT_LABEL_31 tlMPLabel
		GET_MP_COUNTDOWN_STRING(tlMPLabel, FALSE)
	ENDIF
	
	// Update the scaleform movie if it has loaded
	IF g_sSelectorUI.bLoaded
	
		INT iSlot
		
		// Once only set up to the movie
		IF NOT g_sSelectorUI.bSetup
		AND eUIStateIntended != SELECTOR_UI_HIDDEN_TO_FULL
		AND eUIStateIntended != SELECTOR_UI_HIDDEN
			
			PRINTLN("MAINTAIN_SELECTOR_UI() - Setting initial states")
			
			REBUILD_STAT_UI()
			
			FOR iSlot = 0 TO 3
//				BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_SLOT")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) // NOTMET= 3
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // CHAR_MICHAEL = 0
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE) // Current?
//				END_SCALEFORM_MOVIE_METHOD()
//				
//				BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_DAMAGE")
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
//					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//				END_SCALEFORM_MOVIE_METHOD()
				
				// Update the local states so we can tell if there is an update
				iState[iSlot] = g_sSelectorUI.iState[iSlot]
				iEnum[iSlot] = g_sSelectorUI.iEnum[iSlot]
				iCurrent[iSlot] = g_sSelectorUI.iCurrent[iSlot]
				bDamaged[iSlot] = g_sSelectorUI.bDamaged[iSlot]
				bFlashDamage[iSlot] = g_sSelectorUI.bFlashDamage[iSlot]
			ENDFOR
			
//			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_MP_LABEL")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING("")
//			END_SCALEFORM_MOVIE_METHOD()
//				
//			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_COUNTER_ALL")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//			END_SCALEFORM_MOVIE_METHOD()
			iMissions[0] = g_sSelectorUI.iMissions[0]
			iMissions[1] = g_sSelectorUI.iMissions[1]
			iMissions[2] = g_sSelectorUI.iMissions[2]
			iMissions[3] = g_sSelectorUI.iMissions[3]
			
//			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_HINTED_ALL")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
//			END_SCALEFORM_MOVIE_METHOD()
			bHinted[0] = g_sSelectorUI.bHinted[0]
			bHinted[1] = g_sSelectorUI.bHinted[1]
			bHinted[2] = g_sSelectorUI.bHinted[2]
			bHinted[3] = g_sSelectorUI.bHinted[3]
			
//			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
//			END_SCALEFORM_MOVIE_METHOD()
			bDisplay = FALSE
			
			// Force selection update
//			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
//				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
//			END_SCALEFORM_MOVIE_METHOD()
			iSelectedSlot = -1
			
			bDisplayCharacterInfo = FALSE
			g_sSelectorUI.bSetup = TRUE
			g_sSelectorUI.bResetSelection = TRUE
			g_sSelectorUI.bForceUpdate = TRUE
		ENDIF
		
		IF g_sSelectorUI.bSetup
		OR eUIStateIntended = SELECTOR_UI_HIDDEN
			///////////////////////////////////////////////////////////////////////////////////////////////////
			///       
			///       HINT UI: available characters, hints, no selection
			///       
			IF eUIStateIntended = SELECTOR_UI_HINT
			OR eUIStateIntended = SELECTOR_UI_HINT_TO_FULL
				
				FOR iSlot = 0 TO 3
					
					// Update the slot data if the stored data differs
					IF iState[iSlot] != g_sSelectorUI.iState[iSlot]
					OR iEnum[iSlot] != g_sSelectorUI.iEnum[iSlot]
					OR iCurrent[iSlot] != g_sSelectorUI.iCurrent[iSlot]
					OR g_sSelectorUI.bForceUpdate
					
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
							
							IF g_sSelectorUI.iState[iSlot] = 1 // AVAILABLE = 1
							AND (g_sSelectorUI.iCurrent[iSlot] != 1) // Dont show current player
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // AVAILABLE = 1
							ELSE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) // NOTMET= 3
							ENDIF
							
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iEnum[iSlot])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						END_SCALEFORM_MOVIE_METHOD()
						
						// Update the local states so we can tell if there is an update
						iState[iSlot] = g_sSelectorUI.iState[iSlot]
						iEnum[iSlot] = g_sSelectorUI.iEnum[iSlot]
						iCurrent[iSlot] = g_sSelectorUI.iCurrent[iSlot]
						
						bUpdateCounter = TRUE
					ENDIF
					
					IF bDamaged[iSlot] != g_sSelectorUI.bDamaged[iSlot]
					OR bFlashDamage[iSlot] != g_sSelectorUI.bFlashDamage[iSlot]
					OR g_sSelectorUI.bForceUpdate
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_DAMAGE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(INT_TO_ENUM(SELECTOR_SLOTS_ENUM, iSlot)))
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bDamaged[iSlot])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bFlashDamage[iSlot])
						END_SCALEFORM_MOVIE_METHOD()
						bDamaged[iSlot] = g_sSelectorUI.bDamaged[iSlot]
						bFlashDamage[iSlot] = g_sSelectorUI.bFlashDamage[iSlot]
					ENDIF
					
					IF bHinted[iSlot] != g_sSelectorUI.bHinted[iSlot]
					OR g_sSelectorUI.bForceUpdate
						bUpdateHints = TRUE
					ENDIF
					
					IF iMissions[iSlot] != g_sSelectorUI.iMissions[iSlot]
					OR g_sSelectorUI.bForceUpdate
						bUpdateCounter = TRUE
					ENDIF
				ENDFOR
				
				IF bUpdateHints
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_HINTED_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(0)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(1)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(2)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(3)])
					END_SCALEFORM_MOVIE_METHOD()
					bHinted[0] = g_sSelectorUI.bHinted[0]
					bHinted[1] = g_sSelectorUI.bHinted[1]
					bHinted[2] = g_sSelectorUI.bHinted[2]
					bHinted[3] = g_sSelectorUI.bHinted[3]
					bUpdateHints = FALSE
				ENDIF
				
				IF bUpdateCounter
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_COUNTER_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(0)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(1)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(2)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(3)])
					END_SCALEFORM_MOVIE_METHOD()
					iMissions[0] = g_sSelectorUI.iMissions[0]
					iMissions[1] = g_sSelectorUI.iMissions[1]
					iMissions[2] = g_sSelectorUI.iMissions[2]
					iMissions[3] = g_sSelectorUI.iMissions[3]
					bUpdateCounter = FALSE
				ENDIF
				
				IF bDisplay != g_sSelectorUI.bDisplay
				OR g_sSelectorUI.bForceUpdate
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
					END_SCALEFORM_MOVIE_METHOD()
					iSelectedSlot = -1
					
					bDisplayCharacterInfo = FALSE
					
					bDisplay = g_sSelectorUI.bDisplay
				ENDIF
				
			
			///////////////////////////////////////////////////////////////////////////////////////////////////
			///       
			///       FULL UI: available characters, hints, selection
			///       
			ELIF eUIStateIntended = SELECTOR_UI_FULL
			
				FOR iSlot = 0 TO 3
					// Update the slot data if the stored data differs
					IF iState[iSlot] != g_sSelectorUI.iState[iSlot]
					OR iEnum[iSlot] != g_sSelectorUI.iEnum[iSlot]
					OR iCurrent[iSlot] != g_sSelectorUI.iCurrent[iSlot]
					OR g_sSelectorUI.bForceUpdate
					
						PRINTLN("MAINTAIN_SELECTOR_UI() - Updating slot data")
						
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iState[iSlot])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iEnum[iSlot])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL((g_sSelectorUI.iCurrent[iSlot] = 1))
						END_SCALEFORM_MOVIE_METHOD()
						
						// Update the local states so we can tell if there is an update
						iState[iSlot] = g_sSelectorUI.iState[iSlot]
						iEnum[iSlot] = g_sSelectorUI.iEnum[iSlot]
						iCurrent[iSlot] = g_sSelectorUI.iCurrent[iSlot]
					ENDIF
					
					IF bDamaged[iSlot] != g_sSelectorUI.bDamaged[iSlot]
					OR bFlashDamage[iSlot] != g_sSelectorUI.bFlashDamage[iSlot]
					OR g_sSelectorUI.bForceUpdate
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_DAMAGE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(INT_TO_ENUM(SELECTOR_SLOTS_ENUM, iSlot)))
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bDamaged[iSlot])
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bFlashDamage[iSlot])
						END_SCALEFORM_MOVIE_METHOD()
						bDamaged[iSlot] = g_sSelectorUI.bDamaged[iSlot]
						bFlashDamage[iSlot] = g_sSelectorUI.bFlashDamage[iSlot]
					ENDIF
					
					IF bHinted[iSlot] != g_sSelectorUI.bHinted[iSlot]
					OR g_sSelectorUI.bForceUpdate
						bUpdateHints = TRUE
					ENDIF
					
					IF iMissions[iSlot] != g_sSelectorUI.iMissions[iSlot]
					OR g_sSelectorUI.bForceUpdate
						bUpdateCounter = TRUE
					ENDIF
				ENDFOR
				
				IF bUpdateHints
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_HINTED_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(0)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(1)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(2)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(g_sSelectorUI.bHinted[GET_SELECTOR_PED_FROM_UI_SLOT(3)])
					END_SCALEFORM_MOVIE_METHOD()
					bHinted[0] = g_sSelectorUI.bHinted[0]
					bHinted[1] = g_sSelectorUI.bHinted[1]
					bHinted[2] = g_sSelectorUI.bHinted[2]
					bHinted[3] = g_sSelectorUI.bHinted[3]
					bUpdateHints = FALSE
				ENDIF
				
				IF bUpdateCounter
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_COUNTER_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(0)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(1)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(2)])
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iMissions[GET_SELECTOR_PED_FROM_UI_SLOT(3)])
					END_SCALEFORM_MOVIE_METHOD()
					iMissions[0] = g_sSelectorUI.iMissions[0]
					iMissions[1] = g_sSelectorUI.iMissions[1]
					iMissions[2] = g_sSelectorUI.iMissions[2]
					iMissions[3] = g_sSelectorUI.iMissions[3]
					bUpdateCounter = FALSE
				ENDIF
				
				IF bSetPedHead AND NOT bPedHeadSet
				
					IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
					#IF IS_DEBUG_BUILD
					OR g_debugMenuControl.fTempPos[0] != 0.0
					#ENDIF
					
						PRINTLN("MAINTAIN_SELECTOR_UI() - Setting player headshot: director")
						
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_MULTIPLAYER_HEAD")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("director")
						END_SCALEFORM_MOVIE_METHOD()
					
					ELIF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
					AND NOT IS_REPEAT_PLAY_ACTIVE()
					
						PRINTLN("MAINTAIN_SELECTOR_UI() - Setting player headshot: ", sMPPedHeadshot)
						
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_MULTIPLAYER_HEAD")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sMPPedHeadshot)
						END_SCALEFORM_MOVIE_METHOD()
					ENDIF
					bPedHeadSet = TRUE
				ENDIF
				
				IF bDisplay != g_sSelectorUI.bDisplay
				OR g_sSelectorUI.bForceUpdate
					
					PRINTLN("MAINTAIN_SELECTOR_UI() - Updating visibility state")
					
					IF g_sSelectorUI.bDisplay
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
						END_SCALEFORM_MOVIE_METHOD()
						
						IF iSelectedSlot = -1
							bDisplayCharacterInfo = FALSE
						ELSE
							bDisplayCharacterInfo = TRUE
						ENDIF
					ELSE
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						END_SCALEFORM_MOVIE_METHOD()
						
						// Force selection update
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
						END_SCALEFORM_MOVIE_METHOD()
						iSelectedSlot = -1
						
						bDisplayCharacterInfo = FALSE
					ENDIF
					bDisplay = g_sSelectorUI.bDisplay
				ENDIF
				
				
				IF bUpdateCountdown
				OR bForceUpdateCountdown
				OR g_sSelectorUI.bForceUpdate
					
					TEXT_LABEL_31 tlMPLabel
					GET_MP_COUNTDOWN_STRING(tlMPLabel, TRUE)
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_MP_LABEL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(tlMPLabel)
					END_SCALEFORM_MOVIE_METHOD()
					
					bUpdateCountdown = FALSE
					iCountdownTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						tdCountdownTimer = GET_NETWORK_TIME()
					ENDIF
				ELSE
					IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdCountdownTimer, 1000)))
					OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iCountdownTimer) > 1000)
						bUpdateCountdown = TRUE
					ENDIF
				ENDIF
				
				// Update the selected slot
				
				SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD) 
				SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				
				FLOAT fRightStickUD = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				FLOAT fRightStickLR = GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				
				
				BOOL bCheckSelection = TRUE
				INT iNewSelection = -1
				
				// PC Shortcut keys
				// Fake stick direction and slot depending on which shortcut key pressed
				IF IS_PC_VERSION()
					
					IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL )
						fRightStickUD = 0
						fRightStickLR = -1
						g_sSelectorUI.iSelectedSlot = 3
						g_sSelectorUI.bFirstSelectionMade = TRUE 

						PRINTLN("SWITCH UI - INPUT_SELECT_CHARACTER_MICHAEL")
					ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN )
						fRightStickUD = -1
						fRightStickLR = 0
						g_sSelectorUI.iSelectedSlot = 0
						g_sSelectorUI.bFirstSelectionMade = TRUE 
						PRINTLN("SWITCH UI - INPUT_SELECT_CHARACTER_FRANKLIN")
					ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR )
						fRightStickUD = 0
						fRightStickLR = 1
						g_sSelectorUI.iSelectedSlot = 1
						g_sSelectorUI.bFirstSelectionMade = TRUE 
						PRINTLN("SWITCH UI - INPUT_SELECT_CHARACTER_TREVOR")
					ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER )
						fRightStickUD = 1
						fRightStickLR = 0
						g_sSelectorUI.iSelectedSlot = 2
						g_sSelectorUI.bFirstSelectionMade = TRUE 
						PRINTLN("SWITCH UI - INPUT_SELECT_CHARACTER_MULTIPLAYER")
					ENDIF
			
				ENDIF
				
				// Clear stick reset when we hit dead zone.
				// Dont check for selection when in the dead zone.
				IF GET_DISTANCE_BETWEEN_COORDS(<<0,0,0>>, <<fRightStickLR, fRightStickUD, 0.0>>) <= 0.3
					bCheckSelection = FALSE
					g_sSelectorUI.bResetSelection = FALSE
				ENDIF
				
				// Grab the new selection to see if it differs from default
				IF bCheckSelection
					FLOAT fRange = 90
					VECTOR vDir = NORMALISE_VECTOR(<<fRightStickLR, fRightStickUD, 0.0>>)
					FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(0.0, -1.0, vDir.x, vDir.y)
					
					IF fRightStickLR < 0
						fAngle = 360-fAngle
					ENDIF
					
					// Top
					IF (fAngle >= (0.0) AND fAngle <= (0.0+(fRange/2.0)))
					OR (fAngle >= (360-(fRange/2.0)) AND fAngle <= (360))
						iNewSelection = 0
					// Right
					ELIF (fAngle >= (90.0-(fRange/2.0)) AND fAngle <= (90.0+(fRange/2.0)))
						iNewSelection = 1
					// Bottom
					ELIF (fAngle >= (180.0-(fRange/2.0)) AND fAngle <= (180.0+(fRange/2.0)))
						iNewSelection = 2
					// Left
					ELIF (fAngle >= (270.0-(fRange/2.0))  AND fAngle <= (270.0+(fRange/2.0)))
						iNewSelection = 3
					ELSE
						//PRINTLN("MAINTAIN_SELECTOR_UI() - Not accounting for selection angle ", fAngle)
					ENDIF
								
				ENDIF
				
				IF bCheckSelection
				AND g_sSelectorUI.bResetSelection
				AND iSelectedSlotOnLoad = -1
				AND g_sSelectorUI.iSelectedSlot != -1
					iSelectedSlotOnLoad = iNewSelection
				ENDIF
				
				// New selection has been made
				IF iSelectedSlotOnLoad != -1
				AND iNewSelection != iSelectedSlotOnLoad
					g_sSelectorUI.bResetSelection = FALSE
				ENDIF
				
				// Update to new selection
				IF bCheckSelection
				AND NOT g_sSelectorUI.bResetSelection
					g_sSelectorUI.iSelectedSlot = iNewSelection
					g_sSelectorUI.bFirstSelectionMade = TRUE
				ENDIF
				
				IF iSelectedSlot != g_sSelectorUI.iSelectedSlot
					IF NOT g_sSelectorUI.bSFX_BlockAudioCalls
						// Don't play initial MP selection
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						OR iSelectedSlot != -1
							PLAY_SOUND(-1, "CHARACTER_CHANGE_CHARACTER_01_MASTER", DEFAULT, DEFAULT, DEFAULT, FALSE) // Michael
							PRINTLN("SWITCH UI - playing sound: CHARACTER_CHANGE_CHARACTER_01_MASTER")
						ENDIF
					ENDIF
				ENDIF
				
				IF iSelectedSlot != g_sSelectorUI.iSelectedSlot
				OR g_sSelectorUI.bForceUpdate
					PRINTLN("MAINTAIN_SELECTOR_UI() - Updating selected player, ", g_sSelectorUI.iSelectedSlot)
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iSelectedSlot)
					END_SCALEFORM_MOVIE_METHOD()
					iSelectedSlot = g_sSelectorUI.iSelectedSlot
					
					// Stats
					REBUILD_STAT_UI()
					
					bDisplayCharacterInfo = FALSE
					
					IF iSelectedSlot != -1
					AND (g_sSelectorUI.iState[iSelectedSlot] != 3 OR iSelectedSlot = 2)// NOTMET= 3
					AND g_sSelectorUI.iEnum[iSelectedSlot] != 4 //CHAR_CHOP_RIGHT = 4    
					AND g_sSelectorUI.iEnum[iSelectedSlot] != 5 //CHAR_CHOP_UP = 5 
					AND g_sSelectorUI.iEnum[iSelectedSlot] != 6 //CHAR_CHOP_LEFT = 6 
						IF iSelectedSlot = 0 // Top
							bDisplayCharacterInfo = TRUE
						ELIF iSelectedSlot = 1 // Right
							bDisplayCharacterInfo = TRUE
						ELIF iSelectedSlot = 2 // Bottom
							IF NETWORK_IS_GAME_IN_PROGRESS()
							OR NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
								bDisplayCharacterInfo = TRUE
							ENDIF
						ELIF iSelectedSlot = 3 // Left
							bDisplayCharacterInfo = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bSelection != g_sSelectorUI.bSelection
					// Only process the selection on SP characters when in SP mode.
					bSelection = g_sSelectorUI.bSelection
					
					// SFX - UI on screen
					IF g_sSelectorUI.iSFX_Display != -1
					AND g_sSelectorUI.iSFX_Display_ScriptHash = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
						STOP_SOUND(g_sSelectorUI.iSFX_Display)
						PRINTLN("SWITCH UI - stopping sound: CHARACTER_CHANGE_DPAD_DOWN_MASTER")
						RELEASE_SOUND_ID(g_sSelectorUI.iSFX_Display)
						g_sSelectorUI.iSFX_Display = -1
						g_sSelectorUI.iSFX_Display_ScriptHash = 0
					ENDIF
					
					IF g_sSelectorUI.bFX_Fade
						PRINTLN("SWITCH UI - stopping postfx (selection = f_selection)")
						ANIMPOSTFX_STOP("SwitchHUDMichaelIn")
						ANIMPOSTFX_STOP("SwitchHUDFranklinIn")
						ANIMPOSTFX_STOP("SwitchHUDTrevorIn")
						ANIMPOSTFX_STOP("SwitchHUDIn")
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL	
								IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenMichaelIn")
								AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortMichaelIn")
									ANIMPOSTFX_PLAY("SwitchHUDMichaelOut", 0, FALSE)
								ELSE
									PRINTLN("SWITCH UI - don't trigger SwitchHUDMichaelOut [SwitchOpenMichaelIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenMichaelIn"), ", SwitchOpenMichaelIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortMichaelIn"), "]")
								ENDIF
							BREAK
							CASE CHAR_FRANKLIN
								IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenFranklinIn")
								AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortFranklinIn")
									ANIMPOSTFX_PLAY("SwitchHUDFranklinOut", 0, FALSE)	
								ELSE
									PRINTLN("SWITCH UI - don't trigger SwitchHUDFranklinOut [SwitchOpenFranklinIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenFranklinIn"), ", SwitchOpenFranklinIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortFranklinIn"), "]")
								ENDIF
							BREAK
							CASE CHAR_TREVOR
								IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenTrevorIn")
								AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortTrevorIn")
									ANIMPOSTFX_PLAY("SwitchHUDTrevorOut", 0, FALSE)
								ELSE
									PRINTLN("SWITCH UI - don't trigger SwitchHUDTrevorOut [SwitchOpenTrevorIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenTrevorIn"), ", SwitchOpenTrevorIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortTrevorIn"), "]")
								ENDIF
							BREAK
							DEFAULT
								ANIMPOSTFX_PLAY("SwitchHUDOut", 0, FALSE)	NET_NL()NET_PRINT("SwitchHUDOut 2 ")		BREAK
						ENDSWITCH
						SET_AUDIO_FLAG("ActivateSwitchWheelAudio", FALSE)
						g_sSelectorUI.bFX_Fade = FALSE
					ENDIF
					
					PRINTLN("CLEANING UP THE SWITCH UI")
					
				ENDIF
				
			///////////////////////////////////////////////////////////////////////////////////////////////////
			///       
			///       AUTOSWITCH UI: show who we are autoswitching too
			///       
			ELIF eUIStateIntended = SELECTOR_UI_AUTOSWITCH_HINT
				
				FOR iSlot = 0 TO 3
					
					// Update the slot data if the stored data differs
					IF g_sSelectorUI.bForceUpdate
					
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_SLOT")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)
							
							IF g_sSelectorUI.iAutoSwitchSlot = iSlot // AVAILABLE = 1
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // AVAILABLE = 1
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAutoSwitchEnum[iSlot])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
							ELSE
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3) // NOTMET= 3
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAutoSwitchEnum[iSlot])
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
							ENDIF
						END_SCALEFORM_MOVIE_METHOD()
						
						BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_DAMAGE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_SELECTOR_UI_SLOT_FOR_SELECTOR_PED(INT_TO_ENUM(SELECTOR_SLOTS_ENUM, iSlot)))
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						END_SCALEFORM_MOVIE_METHOD()
					ENDIF
				ENDFOR
				
				IF g_sSelectorUI.bForceUpdate
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_HINTED_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_COUNTER_ALL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
					END_SCALEFORM_MOVIE_METHOD()
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g_sSelectorUI.iAutoSwitchSlot)
					END_SCALEFORM_MOVIE_METHOD()
					iAutoSwitchSlot = g_sSelectorUI.iAutoSwitchSlot
					iSelectedSlot = -1
					bDisplayCharacterInfo = FALSE
					bDisplay = g_sSelectorUI.bDisplay
				ENDIF
				
				
				
			///////////////////////////////////////////////////////////////////////////////////////////////////
			///       
			///       HIDDEN UI: cleanup
			///       
			ELIF eUIStateIntended = SELECTOR_UI_HIDDEN
				
				BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_SWITCH_VISIBLE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
				END_SCALEFORM_MOVIE_METHOD()
				
				// Force selection update
				BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_PLAYER_SELECTED")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				END_SCALEFORM_MOVIE_METHOD()
				iSelectedSlot = -1
				
				bDisplayCharacterInfo = FALSE
				
//				// Get some UI to display again
				IF NOT IS_PC_VERSION()
				AND GET_CURRENT_GAMEMODE() != GAMEMODE_FM
				AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				AND NOT IS_SELECTOR_CAM_ACTIVE()
				AND NOT IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
				AND g_sSelectorUI.eUIStateCurrent != SELECTOR_UI_AUTOSWITCH_HINT
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				ENDIF
				
				// Cleanup movie so that it doesnt stay in memory through out the entire game
				CLEANUP_SELECTOR_UI()
				CLEANUP_MP_HEADSHOT()
				
			///////////////////////////////////////////////////////////////////////////////////////////////////
			///       
			///       HIDDEN TO FULL UI: prep
			///       
			ELIF eUIStateIntended = SELECTOR_UI_HIDDEN_TO_FULL
				//PRINTLN("SWITCH UI - initial delay...")
			ENDIF
		ENDIF
		g_sSelectorUI.eUIStateCurrent = eUIStateIntended
	ENDIF
	
	// Draw the movie if it is still loaded
	IF g_sSelectorUI.bLoaded
	AND g_sSelectorUI.bSetup
	
		// SFX - UI on screen
		IF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_FULL
			IF g_sSelectorUI.bOnScreen // 1 frame delay
				IF NOT g_sSelectorUI.bSFX_BlockAudioCalls
					IF g_sSelectorUI.iSFX_Display = -1
						g_sSelectorUI.iSFX_Display = GET_SOUND_ID()
						g_sSelectorUI.iSFX_Display_ScriptHash = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
						IF NETWORK_IS_GAME_IN_PROGRESS()
							PLAY_SOUND(g_sSelectorUI.iSFX_Display, "CHARACTER_CHANGE_DPAD_DOWN_MP_MASTER", DEFAULT, DEFAULT, DEFAULT, FALSE)
						ELSE
							PLAY_SOUND(g_sSelectorUI.iSFX_Display, "CHARACTER_CHANGE_DPAD_DOWN_MASTER", DEFAULT, DEFAULT, DEFAULT, FALSE)
						ENDIF
						PRINTLN("SWITCH UI - playing sound: CHARACTER_CHANGE_DPAD_DOWN_MASTER")
					ENDIF
				ENDIF
				
				IF NOT g_sSelectorUI.bFX_Fade
					PRINTLN("SWITCH UI - starting postfx")
					
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL	ANIMPOSTFX_PLAY("SwitchHUDMichaelIn", 0, FALSE)		BREAK
						CASE CHAR_FRANKLIN	ANIMPOSTFX_PLAY("SwitchHUDFranklinIn", 0, FALSE)	BREAK
						CASE CHAR_TREVOR	ANIMPOSTFX_PLAY("SwitchHUDTrevorIn", 0, FALSE)		BREAK
						DEFAULT 			ANIMPOSTFX_PLAY("SwitchHUDIn", 0, FALSE)			BREAK
					ENDSWITCH
					
					SET_AUDIO_FLAG("ActivateSwitchWheelAudio", TRUE)
					g_sSelectorUI.bFX_Fade = TRUE
				ENDIF
			ENDIF
		ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT
			// Play initial sound
			bSwitchAvailableAudio = TRUE
		ENDIF
		
		IF g_sSelectorUI.bDisplay
		
			IF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_FULL
				// Slow down time when UI is on screen
				bReduceTimeScaleForSwitch = TRUE
				bReduceTimeScale = TRUE
				
				// PLAYER STATS
				IF bDisplayCharacterInfo
				AND NOT bUsingPrologueMovie
					//IF g_sSelectorUI.bOnScreen // 1 frame delay
					
						IF NETWORK_IS_GAME_IN_PROGRESS()
							bDrawStat = TRUE
						ELIF g_sSelectorUI.iSelectedSlot = 0 // Top
							IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
							OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED) // allow stats to display when we boot into debug
								bDrawStat = TRUE
							ENDIF
						ELIF g_sSelectorUI.iSelectedSlot = 1 // Right
							IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
							OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED) // allow stats to display when we boot into debug
								bDrawStat = TRUE
							ENDIF
						ELIF g_sSelectorUI.iSelectedSlot = 2 // Bottom
							IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
							AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
							AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
							AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
							AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
							AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
							AND NOT IS_REPEAT_PLAY_ACTIVE()
							AND HAS_IMPORTANT_STATS_LOADED()
							AND IS_STAT_CHARACTER_ACTIVE(GET_ACTIVE_CHARACTER_SLOT())
								bDrawStat = TRUE
							ENDIF
						ELIF g_sSelectorUI.iSelectedSlot = 3 // Left
							IF IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
							OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED) // allow stats to display when we boot into debug
								bDrawStat = TRUE
							ENDIF
						ENDIF
						
						//bug:2415703
						if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
							bDrawStat = FALSE
						ENDIF
						
						IF bDrawStat
										
							IF GET_IS_WIDESCREEN()			
								SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_RIGHT)
							ENDIF
							
							SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
							
							//SET_SCRIPT_GFX_ALIGN_PARAMS(fPLAYER_STATS_X, fPLAYER_STATS_Y, fPLAYER_STATS_W*fPLAYER_STATS_SCALE_W, fPLAYER_STATS_H*fPLAYER_STATS_SCALE_H)
							
							SET_SCRIPT_GFX_ALIGN_PARAMS(0,0,0,0)
							GET_STAT_BOX_X_POS( fPLAYER_STATS_X )
							
							IF NOT bSuppressUIForShortSwitch
								//DRAW_SCALEFORM_MOVIE(movieID, (((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H), 255, 255, 255, 255)
								DRAW_SCALEFORM_MOVIE(movieID2, fPLAYER_STATS_X + (((fPLAYER_STATS_W*fPLAYER_STATS_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_STATS_Y, (fPLAYER_STATS_W*fPLAYER_STATS_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_STATS_H*fPLAYER_STATS_SCALE_H), 255, 255, 255, 255)
							ENDIF
							RESET_SCRIPT_GFX_ALIGN()
							
							INT iPed
							// Top
							IF g_sSelectorUI.iSelectedSlot = 0
								iPed = ENUM_TO_INT(SELECTOR_PED_FRANKLIN)
							// Right
							ELIF g_sSelectorUI.iSelectedSlot = 1
								iPed = ENUM_TO_INT(SELECTOR_PED_TREVOR)
							// Bottom
							ELIF g_sSelectorUI.iSelectedSlot = 2
								iPed = ENUM_TO_INT(SELECTOR_PED_MULTIPLAYER)
							// Left
							ELIF g_sSelectorUI.iSelectedSlot = 3
								iPed = ENUM_TO_INT(SELECTOR_PED_MICHAEL)
							ENDIF
							
							IF NOT ARE_VECTORS_EQUAL(g_sSelectorUI.vCoords[iPed], <<0,0,0>>)
							AND NOT g_sSelectorUI.bDisableMapOverrideThisFrame
								IF NOT DOES_BLIP_EXIST(selectedPedBlip)
									selectedPedBlip = ADD_BLIP_FOR_COORD(g_sSelectorUI.vCoords[iPed])
									SET_BLIP_COLOUR(selectedPedBlip, BLIP_COLOUR_BLUE)
								ENDIF
								
								SET_BLIP_COORDS(selectedPedBlip, g_sSelectorUI.vCoords[iPed])
								
								IF g_sSelectorUI.iHealth[iPed] = -1
									SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)
									SET_ABILITY_BAR_VALUE(-1, -1)
									SET_MAX_HEALTH_HUD_DISPLAY(0)
									SET_MAX_ARMOUR_HUD_DISPLAY(0)
									//PRINTLN("SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)")
								ELSE
									// The ui bases health on players max health so we need to scale accordingly.
									SET_MAX_HEALTH_HUD_DISPLAY(GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
									SET_MAX_ARMOUR_HUD_DISPLAY(100)
									SET_HEALTH_HUD_DISPLAY_VALUES(FLOOR(TO_FLOAT(GET_PED_MAX_HEALTH(PLAYER_PED_ID()))*(TO_FLOAT(g_sSelectorUI.iHealth[iPed]) / 100)), g_sSelectorUI.iArmour[iPed])
									SET_ABILITY_BAR_VALUE(TO_FLOAT(g_sSelectorUI.iSpecial[iPed]), TO_FLOAT(g_sSelectorUI.iSpecialMax[iPed]))
									//PRINTLN("SET_HEALTH_HUD_DISPLAY_VALUES(", FLOOR(TO_FLOAT(GET_PED_MAX_HEALTH(PLAYER_PED_ID()))*(TO_FLOAT(g_sSelectorUI.iHealth[iPed]) / 100)), ", ", g_sSelectorUI.iArmour[iPed], ")")
								ENDIF
								
								LOCK_MINIMAP_POSITION(g_sSelectorUI.vCoords[iPed].x, g_sSelectorUI.vCoords[iPed].y)
								bMapUpdated = TRUE
								bCleanupMap = FALSE
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
				
				// INPUT BLOCKS
				PROCESS_SELECTOR_INPUT_BLOCKS()
				
				// SPECIAL ABILITY BLOCK
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
							SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
						ENDIF
					ENDIF
				ENDIF
				
				
				// AMMO COUNT AND CASH
				IF g_sSelectorUI.bOnScreen // 1 frame delay
					IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
						IF MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_PROLOGUE
							SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
						ENDIF
						SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
					ENDIF
				ENDIF
				
				// CURRENT MISSION
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				AND NOT IS_STRING_NULL_OR_EMPTY(g_sMissionStatsName)
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
					OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
					OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
					OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
						IF g_sSelectorUI.bOnScreen // 1 frame delay
						
							SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
							SET_SCRIPT_GFX_ALIGN_PARAMS(fMISSION_NAME_X, 0, 0, 0)
							SET_TEXT_SCALE(FLOW_MISSION_NAME_SCALE_X, FLOW_MISSION_NAME_SCALE_Y)
							
							SET_TEXT_FONT(FONT_CURSIVE)
							SET_TEXT_JUSTIFICATION(FONT_RIGHT)
							
							INT red, green, blue, iAlpha
							
							// this is the default color - if we aren't a player character use this			
							SWITCH (GET_CURRENT_PLAYER_PED_ENUM())
								CASE CHAR_MICHAEL
									GET_HUD_COLOUR(HUD_COLOUR_MICHAEL, red, green, blue, iAlpha)
								BREAK
								CASE CHAR_FRANKLIN
									GET_HUD_COLOUR(HUD_COLOUR_FRANKLIN, red, green, blue, iAlpha)
								BREAK
								CASE CHAR_TREVOR
									GET_HUD_COLOUR(HUD_COLOUR_TREVOR, red, green, blue, iAlpha)
								BREAK
								DEFAULT
									red = 240
									green = 200
									blue = 80
								BREAK
							ENDSWITCH
							SET_TEXT_COLOUR(red, green, blue, iAlpha) // yellow text
							SET_TEXT_DROP_SHADOW()
							
							BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMissionStatsName)
							END_TEXT_COMMAND_DISPLAY_TEXT(fMISSION_NAME_X, fMISSION_NAME_Y)
							
							RESET_SCRIPT_GFX_ALIGN()
							
							
							/*SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
							SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
							//DRAW_RECT_FROM_CORNER(0.0, 0.0, CUSTOM_MENU_W, CUSTOM_MENU_HEADER_H, 0, 0, 0, 255)
							SETUP_MENU_HEADING_TEXT()
							BEGIN_TEXT_COMMAND_DISPLAY_TEXT(g_sMissionStatsName)
							END_TEXT_COMMAND_DISPLAY_TEXT(CUSTOM_MENU_TITLE_TEXT_INDENT_X, CUSTOM_MENU_TITLE_TEXT_INDENT_Y)
							RESET_SCRIPT_GFX_ALIGN()*/
						ENDIF
					ENDIF
				ENDIF
				
				// NAMES
				//IF g_sSelectorUI.bOnScreen // 1 frame delay
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				//ENDIF
				
				// SWITCH UI
				SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_RIGHT)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
				//SET_SCRIPT_GFX_ALIGN_PARAMS((((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H))
				SET_SCRIPT_GFX_ALIGN_PARAMS(0,0,0,0)
				IF NOT bSuppressUIForShortSwitch
					DRAW_SCALEFORM_MOVIE(movieID, (((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H), 255, 255, 255, 255)
				ENDIF
				RESET_SCRIPT_GFX_ALIGN()
				g_sSelectorUI.bOnScreen = TRUE
			
			ELIF g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT
			OR g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT_TO_FULL
			OR g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_AUTOSWITCH_HINT
				// NAMES
				//IF g_sSelectorUI.bOnScreen // 1 frame delay
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				//ENDIF
				
				// SWITCH UI
				SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_RIGHT)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
				//SET_SCRIPT_GFX_ALIGN_PARAMS((((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H))
				SET_SCRIPT_GFX_ALIGN_PARAMS(0,0,0,0)
				IF NOT bSuppressUIForShortSwitch
					DRAW_SCALEFORM_MOVIE(movieID, (((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H), 255, 255, 255, 255)
				ENDIF
				RESET_SCRIPT_GFX_ALIGN()
				g_sSelectorUI.bOnScreen = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// SFX - UI no longer screen
	IF g_sSelectorUI.eUIStateCurrent != SELECTOR_UI_FULL
		IF g_sSelectorUI.iSFX_Display != -1
		AND g_sSelectorUI.iSFX_Display_ScriptHash = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
		
			STOP_SOUND(g_sSelectorUI.iSFX_Display)
			PRINTLN("SWITCH UI - stopping sound: CHARACTER_CHANGE_DPAD_DOWN_MASTER")
			RELEASE_SOUND_ID(g_sSelectorUI.iSFX_Display)
			g_sSelectorUI.iSFX_Display = -1
			g_sSelectorUI.iSFX_Display_ScriptHash = 0
			
		ENDIF
	ENDIF
	
	// Let code know if we are displaying the switch UI
	IF (g_sSelectorUI.bOnScreen AND g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_FULL)
		HUD_SHOWING_CHARACTER_SWITCH_SELECTION(TRUE)
	ELSE
		HUD_SHOWING_CHARACTER_SWITCH_SELECTION(FALSE)
	ENDIF
	
	// Keep the timescale reduced while a switch is processing	#1332134
	IF NOT bReduceTimeScale
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			
			bReduceTimeScale = TRUE
			bReduceTimeScaleForSwitch = FALSE
			
			IF GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
				//short range switch, no timescale
				bReduceTimeScale = FALSE
			ELSE
				
				IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_JUMPCUT_ASCENT
					bReduceTimeScale = FALSE
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT bReduceTimeScale
		IF g_sSelectorUI.bFX_Fade
			ANIMPOSTFX_STOP("SwitchHUDMichaelIn")
			ANIMPOSTFX_STOP("SwitchHUDFranklinIn")
			ANIMPOSTFX_STOP("SwitchHUDTrevorIn")
			ANIMPOSTFX_STOP("SwitchHUDIn")
			SET_AUDIO_FLAG("ActivateSwitchWheelAudio", FALSE)
			IF g_sSelectorUI.bFX_SkipFadeOut
				PRINTLN("SWITCH UI - stopping postfx skipped playing fadeout")
			ELSE
				IF bReduceTimeScaleForSwitch
					PRINTLN("SWITCH UI - stopping postfx (not bReduceTimeScale - NOT before switch)")
					SWITCH GET_CURRENT_PLAYER_PED_ENUM()
						CASE CHAR_MICHAEL	
							IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenMichaelIn")
							AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortMichaelIn")
								ANIMPOSTFX_PLAY("SwitchHUDMichaelOut", 0, FALSE)
							ELSE
								PRINTLN("SWITCH UI - don't trigger SwitchHUDMichaelOut [SwitchOpenMichaelIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenMichaelIn"), ", SwitchOpenMichaelIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortMichaelIn"), "]")
							ENDIF
						BREAK
						CASE CHAR_FRANKLIN
							IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenFranklinIn")
							AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortFranklinIn")
								ANIMPOSTFX_PLAY("SwitchHUDFranklinOut", 0, FALSE)	
							ELSE
								PRINTLN("SWITCH UI - don't trigger SwitchHUDFranklinOut [SwitchOpenFranklinIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenFranklinIn"), ", SwitchOpenFranklinIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortFranklinIn"), "]")
							ENDIF
						BREAK
						CASE CHAR_TREVOR
							IF NOT ANIMPOSTFX_IS_RUNNING("SwitchOpenTrevorIn")
							AND NOT ANIMPOSTFX_IS_RUNNING("SwitchShortTrevorIn")
								ANIMPOSTFX_PLAY("SwitchHUDTrevorOut", 0, FALSE)
							ELSE
								PRINTLN("SWITCH UI - don't trigger SwitchHUDTrevorOut [SwitchOpenTrevorIn:", ANIMPOSTFX_IS_RUNNING("SwitchOpenTrevorIn"), ", SwitchOpenTrevorIn:", ANIMPOSTFX_IS_RUNNING("SwitchShortTrevorIn"), "]")
							ENDIF
						BREAK
						DEFAULT 
							IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
							  //removed for todo 1629006 - Brenda Double FX when entering GTAO, but play it if we're in MP and the player selects the MP character
								ANIMPOSTFX_PLAY("SwitchHUDOut", 0, FALSE)		
								NET_NL()NET_PRINT("SwitchHUDOut 1 ")	
							ENDIF
							
						BREAK 
					ENDSWITCH
				ELSE
					PRINTLN("SWITCH UI - stopping postfx (not bReduceTimeScale - before switch)")
				ENDIF
			ENDIF
			
			g_sSelectorUI.bFX_Fade = FALSE
		ENDIF
	ENDIF
	
	// Process the timescale
	IF bReduceTimeScale != bSlowingDownTime
		// Reset the timer
		IF fTargetScale = fLastTimeScaleSet
			iTimeScaleTimer = GET_GAME_TIMER()
		ELSE
			iTimeScaleTimer = GET_GAME_TIMER()-250+(GET_GAME_TIMER()-iTimeScaleTimer)
		ENDIF
		// Set the intended states
		IF bReduceTimeScale
			fTargetScale = 0.10
			bSlowingDownTime = TRUE
		ELSE
			fTargetScale = 1.0
			bSlowingDownTime = FALSE
		ENDIF
	ENDIF
	
	// Update to intended state
	IF fTargetScale != fLastTimeScaleSet
		FLOAT fUpdateScale, fCurrentPhase
		// Duration is 250ms but we are changing time scale so need to multiple by last scale set
		fCurrentPhase = (((TO_FLOAT(GET_GAME_TIMER()-iTimeScaleTimer))) / (250.0*fLastTimeScaleSet))
		
		IF bSlowingDownTime
			fUpdateScale = 1.0-(fCurrentPhase*0.9)
			
			IF fUpdateScale <= 0.111111
				fUpdateScale = 0.1
			ENDIF
		ELSE
			fUpdateScale = 0.1+(fCurrentPhase*0.9)
			
			IF fUpdateScale >= 0.999999
				fUpdateScale = 1.0
			ENDIF
		ENDIF
		
		BOOL bUpdate = FALSE
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			bUpdate = TRUE
		ELIF NETWORK_IS_GAME_IN_PROGRESS()
            // skip time scale
			fUpdateScale = 1.0
		ELIF IS_TRANSITION_ACTIVE()
			// skip time scale
			fUpdateScale = 1.0
        ELIF NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
        	// skip time scale
        ELIF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
        	// skip time scale
        ELSE
			bUpdate = TRUE
        ENDIF
        
        IF bUpdate
        OR fUpdateScale > fLastTimeScaleSet
			SET_TIME_SCALE(fUpdateScale)
			PRINTLN("SWITCH UI - set timescale to ", fUpdateScale)
        ENDIF
        
        fLastTimeScaleSet = fUpdateScale
	ENDIF
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////  ROCKSTAR EDITOR RECORDING SET UP //////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     
	
	#IF USE_REPLAY_RECORDING_TRIGGERS
	
	BOOL bAllowReplayRecording = FALSE
	BOOL bDisplayReplayFeedIcons = FALSE
	
	// Only allow replay recording when running PC version or when we are signed into a SC account on consoles
	IF (NETWORK_IS_SIGNED_IN() OR IS_PC_VERSION())
		
		IF  HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_INTRO1)
		and HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_INTRO2)
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_REC)					
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_EDIT_3")
					CASE FHS_EXPIRED
						IF IS_REPLAY_RECORDING() 
						and eFeedStartType = REPLAY_START_PARAM_DIRECTOR
							ADD_HELP_TO_FLOW_QUEUE("AM_H_EDIT_3", FHP_MEDIUM)
						ENDIF
					BREAK
					CASE FHS_DISPLAYED
						SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_ROCKSTAR_EDITOR_REC)
					BREAK
				ENDSWITCH				
			ENDIF
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_ACTION_MODE)					
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_EDIT_4")
					CASE FHS_EXPIRED
						IF IS_REPLAY_RECORDING() 
						and eFeedStartType = REPLAY_START_PARAM_HIGHLIGHT
							ADD_HELP_TO_FLOW_QUEUE("AM_H_EDIT_4", FHP_MEDIUM)
						ENDIF
					BREAK
					CASE FHS_DISPLAYED
						SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_ROCKSTAR_EDITOR_ACTION_MODE)
					BREAK
				ENDSWITCH				
			ENDIF
		ENDIF
		
		
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///      DISPLAY ICONS ON THE FEED
		///      
		
		IF IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
		OR (IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))	//Allow PC shortcuts - bug 2228146
		
			bAllowReplayRecording = IS_REPLAY_RECORDING_UI_SAFE_TO_USE()
			
			iExcludeInputTimer = GET_GAME_TIMER() + 250	//250 milliseconds delay for exclusive inputs
			PRINTLN("iExcludeInputTimer = ", GET_GAME_TIMER() + 250)
			IF bAllowReplayRecording
				IF g_sSelectorUI.bDisplay
					// Wait for switch UI to load.
					IF g_sSelectorUI.bOnScreen
					AND g_sSelectorUI.bLoaded
					AND g_sSelectorUI.bSetup
					AND g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_FULL
						bDisplayReplayFeedIcons = TRUE
					ELSE
						PRINTLN("SWITCH UI - [REPLAY-FEED] Trying to display replay recording icons but the selector hasn't loaded yet...")
					ENDIF
				ELIF IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
					// Switch is not safe to display so we do not need to wait for it.
					bDisplayReplayFeedIcons = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
			IF bAllowReplayRecording	//Fix for bug 2207278
				iDisplayingReplayRecordUI = GET_GAME_TIMER() + 500
			ENDIF
		ENDIF
		
		// Need to call this after we have checked the inputs so we can start/reset timer.
		// If the initial delay has not passed then we set bDisplayReplayFeedIcons to FALSE.
		UPDATE_REPLAY_RECORDING_FEED_REQUEST(bDisplayReplayFeedIcons)
		
		IF bDisplayReplayFeedIcons
			IF NOT g_sSelectorUI.bFeedAddedForRecording
				PRINTLN("SWITCH UI - [REPLAY-FEED] Adding feed icons for recording...")
				
				INT iPost
				REPEAT COUNT_OF(iRecPost) iPost
					iRecPost[iPost] = -1
				ENDREPEAT
				
				THEFEED_FLUSH_QUEUE()
				
				THEFEED_SET_SNAP_FEED_ITEM_POSITIONS(TRUE)
				
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
				AND NOT GET_IS_USING_HOOD_CAMERA()
					THEFEED_FREEZE_NEXT_POST()
					BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_WAR")
					iRecPost[0] = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)
				ENDIF
				
				IF IS_REPLAY_RECORDING()
					IF eFeedStartType = REPLAY_START_PARAM_DIRECTOR
						// Save Recording
						PRINTLN("SWITCH UI - [REPLAY-FEED] Adding SAVE RECORDING feed item.")
						THEFEED_FREEZE_NEXT_POST()
						BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_5")
						iRecPost[1] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON,"~INPUT_REPLAY_START_STOP_RECORDING~","")// F1 for PC
//							GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING) ,"") 

						
						// Cancel Recording
						PRINTLN("SWITCH UI - [REPLAY-FEED] Adding CANCEL RECORDING feed item.")
						THEFEED_FREEZE_NEXT_POST()
						BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_4") 
						
						iRecPost[2] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON,"~INPUT_SAVE_REPLAY_CLIP~","")  // F3 for PC

						
					ELIF eFeedStartType = REPLAY_START_PARAM_HIGHLIGHT
						// Save Action Replay
						PRINTLN("SWITCH UI - [REPLAY-FEED] Adding SAVE ACTION REPLAY feed item.")
						THEFEED_FREEZE_NEXT_POST()
						BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_3") 
						iRecPost[1] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON,  "~INPUT_REPLAY_START_STOP_RECORDING~","")  // F1 for PC
						
						// Turn Off Action Replay
						PRINTLN("SWITCH UI - [REPLAY-FEED] Adding TURN OFF ACTION REPLAY feed item.")
						THEFEED_FREEZE_NEXT_POST()
						BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_2") 
						iRecPost[2] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON,  "~INPUT_SAVE_REPLAY_CLIP~","")  // F3 for PC

					ENDIF
				ELSE
					// Start Recording
					PRINTLN("SWITCH UI - [REPLAY-FEED] Adding START RECORDING feed item.")
					THEFEED_FREEZE_NEXT_POST()
					BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_1")
					iRecPost[1] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON,  "~INPUT_REPLAY_START_STOP_RECORDING~","")  // F1 for PC

					// Turn On Action Replay
					PRINTLN("SWITCH UI - [REPLAY-FEED] Adding TURN ON ACTION REPLAY feed item.")
					THEFEED_FREEZE_NEXT_POST()
					BEGIN_TEXT_COMMAND_THEFEED_POST("REC_FEED_0") 
					iRecPost[2] = END_TEXT_COMMAND_THEFEED_POST_REPLAY_INPUT(REPLAY_BUTTON, "~INPUT_REPLAY_START_STOP_RECORDING_SECONDARY~","")   // F2 for PC

				ENDIF
				
				PRINTLN("SWITCH UI - [REPLAY-FEED] Pausing feed after adding recording items.")
				THEFEED_PAUSE()
				
				g_sSelectorUI.bFeedAddedForRecording = TRUE
			ENDIF
		ELSE
			CLEANUP_REPLAY_RECORD_FEED()
		ENDIF
		
		// Clear temp Action Replay On/Off feed message
		IF iRecPostActionReplay != -1
			IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iActionReplayTimer) > 4000)
			OR (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdActionReplayTimer)) > 4000)
				PRINTLN("SWITCH UI - [REPLAY-FEED] Cleaning up action replay on/off feed messages.")
				THEFEED_REMOVE_ITEM(iRecPostActionReplay)
				iRecPostActionReplay = -1
			ENDIF
		ENDIF
		
		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		///      MANAGE START/STOP RECORDING
		///      
		IF bAllowReplayRecording
			BOOL bAcceptPressed = FALSE
			BOOL bCancelPressed = FALSE
			BOOL bStartBufferPressed = FALSE
			BOOL bCloseSelectorUI = FALSE
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				// PC: Function keys
				bAcceptPressed = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING) // F1
				bCancelPressed = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SAVE_REPLAY_CLIP) // F3
				bStartBufferPressed = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING_SECONDARY) // F2
			ELSE
				// Consoles: DPAD_DOWN + ((A/B/X) OR (X/O/SQ))
				IF IS_SELECTOR_UI_BUTTON_PRESSED(FALSE)
//					bAcceptPressed 		= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
//					bCancelPressed 		= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
//					bStartBufferPressed = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X) 		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X))
					bAcceptPressed 		= (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING) 			OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING))
					bCancelPressed 		= (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SAVE_REPLAY_CLIP) 						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SAVE_REPLAY_CLIP))
					bStartBufferPressed = (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING_SECONDARY)	OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_REPLAY_START_STOP_RECORDING_SECONDARY)) 		
				ENDIF
			ENDIF
			
			IF IS_REPLAY_RECORDING()
				IF bAcceptPressed
					// Save Buffer/Recording
					IF eFeedStartType = REPLAY_START_PARAM_HIGHLIGHT
						SAVE_REPLAY_RECORDING()
					ELSE
						STOP_REPLAY_RECORDING()
					ENDIF
					bCloseSelectorUI = TRUE
				ELIF bCancelPressed
					// Cancel Buffer/Recording
					CANCEL_REPLAY_RECORDING()
					IF eFeedStartType = REPLAY_START_PARAM_HIGHLIGHT
						bAddActionReplayOff = TRUE
					ENDIF
					bCloseSelectorUI = TRUE
				ENDIF
			ELSE
				IF bAcceptPressed // Recording
					START_REPLAY_RECORDING(REPLAY_START_PARAM_DIRECTOR)
					IF IS_REPLAY_RECORD_SPACE_AVAILABLE()
						eFeedStartType = REPLAY_START_PARAM_DIRECTOR					
					ENDIF
					bCloseSelectorUI = TRUE
				ELIF bStartBufferPressed // Buffer
					START_REPLAY_RECORDING(REPLAY_START_PARAM_HIGHLIGHT)
					IF IS_REPLAY_RECORD_SPACE_AVAILABLE()
						eFeedStartType = REPLAY_START_PARAM_HIGHLIGHT
						bAddActionReplayOn = TRUE						
					ENDIF
					bCloseSelectorUI = TRUE
				ENDIF
			ENDIF
			
			// Force player to release gamepad controls before processing again.
			IF bCloseSelectorUI
				g_sSelectorUI.bMustReleaseSelectorUIButton = TRUE
				g_sSelectorUI.bMustReleaseCancelButton = bCancelPressed
				g_sSelectorUI.bMustReleaseAcceptButton = bAcceptPressed
				g_sSelectorUI.bMustReleaseSquareButton = bStartBufferPressed
			ENDIF
		ENDIF
		
	// Replay Recording not availables so cleanup.
	ELSE
		CLEANUP_REPLAY_RECORD_FEED()
	ENDIF
	
	// Input exclusive until player releases buttons
	// Only do this if the creators aren't running or this will block creator input
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("creator")) = 0)
			IF g_sSelectorUI.bMustReleaseCancelButton
			OR bAllowReplayRecording
			OR iExcludeInputTimer > GET_GAME_TIMER()
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PRINTLN("SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)")
				IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			ENDIF
			IF g_sSelectorUI.bMustReleaseAcceptButton
			OR bAllowReplayRecording
			OR iExcludeInputTimer > GET_GAME_TIMER()
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				PRINTLN("SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")
				IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			ENDIF
			IF g_sSelectorUI.bMustReleaseSquareButton
			OR bAllowReplayRecording
			OR iExcludeInputTimer > GET_GAME_TIMER()
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				PRINTLN("SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_X)")
				IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			ENDIF
		ENDIF
	ENDIF
	
	#ENDIF
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// Check for the flow requesting a ped hint on one of the three player characters.
	INT index
	REPEAT 3 index
		IF IS_BIT_SET(g_iPlayerFlowHintActive, index)
			IF NOT sSelectorPeds.bDisplayHint[index]
				PRINTLN("SWITCH UI - Setting flow requested selector hint on ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)))
				SET_SELECTOR_PED_HINT(sSelectorPeds, INT_TO_ENUM(SELECTOR_SLOTS_ENUM, index), TRUE)
			ENDIF
		ELSE
			IF sSelectorPeds.bDisplayHint[index]
				PRINTLN("SWITCH UI - Clearing flow requested selector hint on ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)))
				SET_SELECTOR_PED_HINT(sSelectorPeds, INT_TO_ENUM(SELECTOR_SLOTS_ENUM, index), FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bCleanupMap
		IF bMapUpdated
			IF DOES_BLIP_EXIST(selectedPedBlip)
				REMOVE_BLIP(selectedPedBlip)
			ENDIF
			SET_HEALTH_HUD_DISPLAY_VALUES(-1, -1)
			SET_ABILITY_BAR_VALUE(-1, -1)
			SET_MAX_HEALTH_HUD_DISPLAY(0)
			SET_MAX_ARMOUR_HUD_DISPLAY(0)
			UNLOCK_MINIMAP_POSITION()
			bMapUpdated = FALSE
		ENDIF
	ENDIF
	
	// Damaged/hint indicator audio.
	BOOL bDamagedIndicatorAudioRequired = FALSE
	BOOL bHintIndicatorAudioRequired = FALSE
	IF (g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT OR g_sSelectorUI.eUIStateCurrent = SELECTOR_UI_HINT_TO_FULL)
		IF ((g_sSelectorUI.bDamaged[0] AND g_sSelectorUI.bFlashDamage[0]) OR (g_sSelectorUI.bDamaged[1] AND g_sSelectorUI.bFlashDamage[1]) OR (g_sSelectorUI.bDamaged[2] AND g_sSelectorUI.bFlashDamage[2]) OR (g_sSelectorUI.bDamaged[3] AND g_sSelectorUI.bFlashDamage[3]))
			bDamagedIndicatorAudioRequired = TRUE
		ELIF (g_sSelectorUI.bHinted[0] OR g_sSelectorUI.bHinted[1] OR g_sSelectorUI.bHinted[2] OR g_sSelectorUI.bHinted[3])
			bHintIndicatorAudioRequired = TRUE
		ENDIF
	ENDIF
	IF (NOT g_sSelectorUI.bHinted[0] AND NOT g_sSelectorUI.bHinted[1] AND NOT g_sSelectorUI.bHinted[2] AND NOT g_sSelectorUI.bHinted[3])
		bHintIndicatorNotTriggered = FALSE
	ENDIF
	IF bDamagedIndicatorAudioRequired
		IF iIndicatorSFX[0] = -1
			iIndicatorSFX[0] = GET_SOUND_ID()
			START_AUDIO_SCENE("PLAYER_SWITCH_RED_ALERT_SCENE")
			PLAY_SOUND_FRONTEND(iIndicatorSFX[0], "SwitchRedWarning", "SPECIAL_ABILITY_SOUNDSET", FALSE)
			PRINTLN("SWITCH UI - playing sound: SwitchRedWarning, SPECIAL_ABILITY_SOUNDSET")
		ENDIF
	ELSE
		IF iIndicatorSFX[0] != -1
			STOP_SOUND(iIndicatorSFX[0])
			PRINTLN("SWITCH UI - stopping sound: SwitchRedWarning, SPECIAL_ABILITY_SOUNDSET")
			RELEASE_SOUND_ID(iIndicatorSFX[0])
			IF NOT bHintIndicatorAudioRequired
				STOP_AUDIO_SCENE("PLAYER_SWITCH_RED_ALERT_SCENE")
			ENDIF
			iIndicatorSFX[0] = -1
		ENDIF
	ENDIF
	IF bHintIndicatorAudioRequired
		IF iIndicatorSFX[1] = -1
		AND NOT bHintIndicatorNotTriggered
			iIndicatorSFX[1] = GET_SOUND_ID()
			bHintIndicatorNotTriggered = TRUE
			START_AUDIO_SCENE("PLAYER_SWITCH_WHITE_ALERT_SCENE")
			PLAY_SOUND_FRONTEND(iIndicatorSFX[1], "SwitchWhiteWarning", "SPECIAL_ABILITY_SOUNDSET", FALSE)
			PRINTLN("SWITCH UI - playing sound: SwitchWhiteWarning, SPECIAL_ABILITY_SOUNDSET")
		ENDIF
	ELSE
		IF iIndicatorSFX[1] != -1
			STOP_SOUND(iIndicatorSFX[1])
			PRINTLN("SWITCH UI - stopping sound: SwitchWhiteWarning, SPECIAL_ABILITY_SOUNDSET")
			RELEASE_SOUND_ID(iIndicatorSFX[1])
			IF NOT bDamagedIndicatorAudioRequired
				STOP_AUDIO_SCENE("PLAYER_SWITCH_WHITE_ALERT_SCENE")
			ENDIF
			iIndicatorSFX[1] = -1
		ENDIF
	ENDIF
	
	IF bSwitchAvailableAudio
	AND NOT bDamagedIndicatorAudioRequired
	AND NOT bHintIndicatorAudioRequired
		IF NOT bSwitchAvailableAudioPlayed
			// Fix for bugs 1583038 and 1583031 - dontr trigger the available audio if it has just played.
			IF GET_GAME_TIMER() - iLastTimeSwitchWasOnscreen > 12500
				PLAY_SOUND_FRONTEND(-1, "CHARACTER_SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				PRINTLN("SWITCH UI - playing sound: HUD_DEFAULT_INFO_MASTER")
			ENDIF
		ENDIF
		bSwitchAvailableAudioPlayed = TRUE
	ENDIF
	
	// Fix for bug 1500026 - Need to suppress vehicle info just after a switch.
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR IS_SELECTOR_CAM_ACTIVE()
	OR g_sSelectorUI.bHideUiForSwitch
		iSwitchActiveTimer = GET_GAME_TIMER()
	ENDIF
	IF (GET_GAME_TIMER() - iSwitchActiveTimer < 1000)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		bSwitchActiveBlock = TRUE
	ELIF bSwitchActiveBlock
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			ENDIF
			//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		ENDIF
		bSwitchActiveBlock = FALSE
	ENDIF
	
	// Fix for bug 1437330 - During short range switches, all HUD elements should disappear and reappear on the same frame.
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
	AND GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
		bSuppressUIForShortSwitch = TRUE
		iSuppressRadarTimer = GET_GAME_TIMER()
		PRINTLN(">>> SUPPRESS MAP")
	ENDIF
	IF bSuppressUIForShortSwitch
	
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			iLastSwitchFrameCount = 0
		ELSE
			iLastSwitchFrameCount++
		ENDIF
	
		IF (GET_GAME_TIMER() - iSuppressRadarTimer > 5000)
		OR g_sSelectorUI.bOnScreen
		OR (NOT IS_PLAYER_SWITCH_IN_PROGRESS() AND NOT g_sSelectorUI.bMissionUpdate AND iLastSwitchFrameCount > 5)
			bSuppressUIForShortSwitch = FALSE
		ENDIF
		
//		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	ENDIF
	
	IF bCleanupMovies
		IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iCleanupMoviesTimer) > 2000 AND NOT bSuppressUIForShortSwitch)
		OR (NETWORK_IS_GAME_IN_PROGRESS())// AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdCleanupMoviesTimer, 2000)))
			
			PRINTSTRING("\n CLEANUP_SELECTOR_UI - Cleaning up scaleform movie.")PRINTNL()
			
			IF bRequested
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(movieID)
				IF NOT bUsingPrologueMovie
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(movieID2)
				ENDIF
			ENDIF
			iLoadMoviesTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tdLoadMoviesTimer = GET_NETWORK_TIME()
			ENDIF
			
			NET_NL()NET_PRINT("SELECTOR: bCleanupMovies = TRUE so put up the save failed again ")
			g_b_ReapplyStickySaveFailedFeed = FALSE
			
			bSetSystemTimer = FALSE
			bRequested = FALSE
			movieID = NULL
			movieID2 = NULL
			bCleanupMovies = FALSE
			g_sSelectorUI.bLoaded = FALSE
		ELSE
			PRINTSTRING("\n CLEANUP_SELECTOR_UI - Waiting for cleanup timer to pass before unloadig scaleform movies.")PRINTNL()
		ENDIF
	ENDIF
	
	IF g_sSelectorUI.bOnScreen
		iLastTimeSwitchWasOnscreen = GET_GAME_TIMER()
	ENDIF
ENDPROC

PROC FAILSAFE_PLAYER_RESPAWN()
	NET_NL()NET_PRINT("SELECTOR: FAILSAFE_PLAYER_RESPAWN - AUTOMATIC RESPAWN")
	IF IS_NET_PLAYER_OK(PLAYER_ID()) = FALSE
		TRANSITION_BEGIN_RESPAWN()
	ENDIF
	
	TRIGGER_TRANSITION_MENU_ACTIVE(FALSE)
	SET_SOMETHING_QUITTING_MP(FALSE)
	DISPLAYING_SCRIPT_HUD(HUDPART_TRANSITIONHUD, FALSE)
	DISABLE_CELLPHONE(FALSE)
	SET_SCRIPT_AS_NO_LONGER_NEEDED("MainTransition")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CANCEL_PLAYER_PED_SWITCH_REQUEST()
	ENDIF
ENDPROC


PROC DRAW_FAKE_SWITCH()
//
//	//STATS
//SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
//SET_SCRIPT_GFX_ALIGN_PARAMS(fPLAYER_STATS_X, fPLAYER_STATS_Y, fPLAYER_STATS_W*fPLAYER_STATS_SCALE_W, fPLAYER_STATS_H*fPLAYER_STATS_SCALE_H)
//IF NOT bSuppressUIForShortSwitch
//      DRAW_SCALEFORM_MOVIE(movieID2, fPLAYER_STATS_X, fPLAYER_STATS_Y, fPLAYER_STATS_W*fPLAYER_STATS_SCALE_W, fPLAYER_STATS_H*fPLAYER_STATS_SCALE_H, 255, 255, 255, 255)
//ENDIF
//RESET_SCRIPT_GFX_ALIGN()


//SWITCH

// SWITCH UI
//IF HAS_SCALEFORM_MOVIE_LOADED(movieID)
//	PRINTSTRING("\n DRAW_FAKE_SWITCH - CALLING scaleform.")PRINTNL()
//	IF g_b_Private_IsSelectorActuallyDrawing = FALSE
//		SET_WIDESCREEN_FORMAT(WIDESCREEN_FORMAT_RIGHT)
//		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_BOTTOM)
//		//SET_SCRIPT_GFX_ALIGN_PARAMS((((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H))
//		SET_SCRIPT_GFX_ALIGN_PARAMS(0,0,0,0)
//		
//		
//		PRINTSTRING("\n DRAW_FAKE_SWITCH - DRAWING scaleform.")PRINTNL()
//		
//		DRAW_SCALEFORM_MOVIE(movieID, (((fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE)*0.5), fPLAYER_SWITCH_Y, (fPLAYER_SWITCH_W*fPLAYER_SWITCH_SCALE_W)*fPLAYER_SWITCH_WIDE_SCALE, (fPLAYER_SWITCH_H*fPLAYER_SWITCH_SCALE_H), 255, 255, 255, 255)
//		RESET_SCRIPT_GFX_ALIGN()
//	ENDIF
//
//	g_b_Private_IsSelectorActuallyDrawing = FALSE
//
//	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
//	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
//	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
//	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
//	
//ENDIF


ENDPROC

	

/// PURPOSE: Checks for the player bringing up the switch UI and making an ambient selection
PROC MAINTAIN_AMBIENT_SELECTOR()
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		AND NOT g_bMagDemoActive
			MAINTAIN_SELECTOR_MULTIPLAYER_QUICKLAUNCH_TRANSITION()
		ENDIF
	#ENDIF
	
	
	BOOL bLaunchFromEventReceived = FALSE
	BOOL bInMpIntroTaxi = FALSE
	BOOL bCheckPlayerControl = g_sSelectorUI.bCheckPlayerControl
	// Check for ambient character selection
	BOOL bUISelection = FALSE
	IF NOT g_sSelectorUI.bMissionUpdate
	AND NOT g_bMagdemoDoTakeOverSwitch
		IF (NOT IS_PLAYER_SWITCH_IN_PROGRESS() OR g_TransitionData.bCouldNotJoinNetworkGame)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_LOCAL_PLAYER_IN_TAXI_FOR_INTRO()
					bInMpIntroTaxi = TRUE
				ENDIF
			ENDIF
			
			IF bInMpIntroTaxi
				bCheckPlayerControl = FALSE
			ENDIF
			
			IF ((IS_SELECTOR_UI_BUTTON_PRESSED(bCheckPlayerControl ) OR IS_SELECTOR_UI_BUTTON_JUST_RELEASED(bCheckPlayerControl)) AND UPDATE_SELECTOR_HUD(sSelectorPeds, bCheckPlayerControl))	// Returns TRUE when the player has made a valid selection
				PRINTLN("BC: - SELECTOR REQUESTING A TRANSITION")
				SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_SELECTOR_WHEEL))
				
				IF (HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds))
				OR GET_CURRENT_GAMEMODE() = GAMEMODE_FM
					INT iPlayerNum = NATIVE_TO_INT(PLAYER_ID())
					IF iPlayerNum > -1
						SET_BIT(GlobalplayerBD_FM[iPlayerNum].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bIsOnWarningScreen)
						NET_NL()NET_PRINT("BC: GlobalPlayerBroadcastDataFM_BS_bIsOnWarningScreen = TRUE ")
						
					ENDIF
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						SET_AUDIO_FLAG("AllowRadioOverScreenFade", TRUE)
						SET_NO_LOADING_SCREEN(TRUE)
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					DRAW_FAKE_SWITCH()

				ENDIF

				
				g_Private_SwitchedSP_DuringActivity = TRUE //Always show screen. TODO 1334154

				IF NETWORK_IS_ACTIVITY_SESSION()
				OR (FM_EVENT_IS_PLAYER_LEAVING_EVENT_SEVERE() OR FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE())
					NET_NL()NET_PRINT("BC: NETWORK_IS_ACTIVITY_SESSION or FM EVENT and quit through the Selector - g_b_QuitActivityThroughSelector = TRUE ")
					g_b_QuitActivityThroughSelector = TRUE
				ENDIF

				SWITCH  GET_CURRENT_GAMEMODE()
					CASE GAMEMODE_FM
						IF IS_ON_BASEJUMP_GLOBAL_SET()
						OR IS_ON_DEATHMATCH_GLOBAL_SET()
						OR IS_ON_RACE_GLOBAL_SET()
						OR IS_ON_RALLY_RACE_GLOBAL_SET()
						OR g_bFM_ON_TEAM_MISSION
						OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT)
							PRINTLN("BC: - SELECTOR g_Private_SwitchedSP_DuringActivity = TRUE")
							g_Private_SwitchedSP_DuringActivity = TRUE
						ELSE
						ENDIF
					BREAK
					
					
					
				ENDSWITCH
				

				bUISelection = TRUE
			ENDIF
		ENDIF
		
		
	
	ELSE 
		NET_NL()NET_PRINT("g_sSelectorUI.bMissionUpdate = ")NET_PRINT_BOOL(g_sSelectorUI.bMissionUpdate)
	ENDIF	
	
	
	#IF IS_DEBUG_BUILD
		IF GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY
			PRINTLN("BC: - SELECTOR GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY REQUESTING A TRANSITION")
			bUISelection = TRUE
		ENDIF
	#ENDIF
	
	IF IS_TRANSITION_MENU_TRIGGERED()
		PRINTLN("BC: - SELECTOR IS_TRANSITION_MENU_TRIGGERED REQUESTING A TRANSITION")
		bUISelection = TRUE
	ENDIF
	
	IF HAVE_I_BEEN_KICKED()
		PRINTLN("BC: - SELECTOR HAVE_I_BEEN_KICKED REQUESTING A TRANSITION")
		bUISelection = TRUE
	ENDIF
	
	#IF FEATURE_FREEMODE_ARCADE
	IF (IS_FREEMODE_ARCADE() AND IS_RETURN_TO_LANDING_PAGE())
		PRINTLN("BC: - SELECTOR IS_FREEMODE_ARCADE IS_RETURN_TO_LANDING_PAGE REQUESTING A TRANSITION")
		bUISelection = TRUE
		
		SET_TRANSITION_LAUNCH_BLOCKING_MUSIC_STOP(TRUE)
		
		IF NOT IS_GAMEMODE_AN_ARCADEMODE(GET_CURRENT_GAMEMODE())
			SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		ENDIF
	ELIF (IS_FREEMODE_ARCADE() AND IS_RETURN_TO_GTA_ONLINE())
		PRINTLN("BC: - SELECTOR IS_FREEMODE_ARCADE IS_RETURN_TO_GTA_ONLINE REQUESTING A TRANSITION")
		bUISelection = TRUE
		
		IF NOT IS_GAMEMODE_AN_ARCADEMODE(GET_CURRENT_GAMEMODE())
			SET_CURRENT_GAMEMODE(GAMEMODE_EMPTY)
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_TRANSITION_ACTIVE() = FALSE
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			PRINTLN("BC: JAMES A - SELECTOR PAUSE MENU IS REQUESTING A TRANSITION")
			SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_PAUSE_MENU))
			bUISelection = TRUE
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			PRINTLN("BC: JAMES A - SELECTOR Trying to start maintransition IS_PAUSE_MENU_REQUESTING_TRANSITION but IS_TRANSITION_ACTIVE = TRUE ")
		ENDIF 
	#ENDIF
	ENDIF
	

	IF LOBBY_AUTO_MULTIPLAYER_RANDOM_JOB()
		PRINTLN("BC: JAMES A - SELECTOR LOBBY_AUTO_MULTIPLAYER_RANDOM_JOB() ")
		SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_RANDOM_JOB))
	ENDIF
	
	IF LOBBY_AUTO_MULTIPLAYER_EVENT()
		IF IS_FEATURED_PLAYLIST_ACTIVE()
			PRINTLN("BC: JAMES A - SELECTOR LOBBY_AUTO_MULTIPLAYER_EVENT() JT_FEATURE_PLAYLIST ")
			SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_FEATURE_PLAYLIST))
		ELSE
			PRINTLN("BC: JAMES A - SELECTOR LOBBY_AUTO_MULTIPLAYER_EVENT() JT_EVENT_PLAYLIST ")
			SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_EVENT_PLAYLIST))
		ENDIF
	ENDIF

	
	//IF (LOBBY_ENTER_MULTIPLAYER()) // Command has been removed from game, see bug #507451.
	IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
		FORCE_RESPAWN()
		sSelectorPeds.bInitialised = TRUE
		PRINTLN("BC: JAMES A - SELECTOR GET_CONFIRM_INVITE_INTO_GAME_STATE = TRUE")
		sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
		bUISelection = TRUE
		bLaunchFromEventReceived = TRUE
		SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_INVITED))
	ENDIF
	
	IF IS_PAUSE_MENU_MIGRATE_SAVES()
 	AND ((HAS_IMPORTANT_STATS_LOADED() AND IS_MIGRATE_CHECK_START_RUNNING() = FALSE) 
									OR HAS_IMPORTANT_STATS_LOADED() = FALSE)
	AND IS_NOT_MIGRATION_RUNNING()
	
		BOOL continueToMigrateCheck = TRUE

		//IMPORTANT: We cant do any migration bulshyte if stats are loaded!!!
		IF HAS_IMPORTANT_STATS_LOADED()
			PRINTLN("[SAVETRANS] HAS_IMPORTANT_STATS_LOADED - calling RELOAD_EVERYTHING_NOW_LIGHT_VERSION")
			//Clear all stats ligth version, we dont really need to reset local values now.
			continueToMigrateCheck = RELOAD_EVERYTHING_NOW_LIGHT_VERSION()
		ENDIF

		IF continueToMigrateCheck
		//If we good to go, or we're in FM
		OR GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			//If in FM set that we need to clean up the stats when we get to the loader
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
				SET_CLEAR_STATS_IN_TRANSFER_AFTER_BEING_IN_FM(TRUE)
			ENDIF
			STAT_MIGRATE_CLEAR_FOR_RESTART()
			PRINTLN("BC: JAMES A - SELECTOR IS_PAUSE_MENU_MIGRATE_SAVES = TRUE ")
			PRINTLN("[SAVETRANS] Selector: STAT_MIGRATE_CLEAR_FOR_RESTART() called STAT_MIGRATE_CLEAR_FOR_RESTART")
			FORCE_RESPAWN()
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
			OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
				SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
			ENDIF
			sSelectorPeds.bInitialised = TRUE
			sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
			bUISelection = TRUE
		ENDIF
	ENDIF

	IF DID_LAUNCHED_LIVETILE_WHILE_TRANSITIONING()
	AND NOT IS_TRANSITION_ACTIVE()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND g_sPlayerPedRequest.eState != PR_STATE_PROCESSING 
	
		NET_NL()NET_PRINT("BC: DID_LAUNCHED_LIVETILE_WHILE_TRANSITIONING() = TRUE ")

		//g_AppLaunchStructDetails.nFlags = LAUNCH_FROM_LIVE_AREA
		INT bitset = ENUM_TO_INT(g_AppLaunchStructDetails.nFlags)
		bitset = bitset|ENUM_TO_INT(LAUNCH_FROM_LIVE_AREA)
		g_AppLaunchStructDetails.nFlags = INT_TO_ENUM(APP_LAUNCH_FLAGS, bitset)
		NET_NL()NET_PRINT("BC: bitset = ")NET_PRINT_INT(bitset)
		//SET_BIT(ENUM_TO_INT(g_AppLaunchStructDetails.nFlags), ENUM_TO_INT(LAUNCH_FROM_LIVE_AREA))
		
	ENDIF
	
	IF HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND g_sPlayerPedRequest.eState != PR_STATE_PROCESSING 
	
		FORCE_RESPAWN()
		sSelectorPeds.bInitialised = TRUE
		PRINTLN("BC: JAMES A - SELECTOR HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED = TRUE")
		IF NOT GET_IS_LOADING_SCREEN_ACTIVE()
//			IF g_bScriptsSetSafeForCutscene = TRUE
//			AND IS_GAMEPLAY_CAM_RENDERING() = FALSE
//				SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
//			ELSE
//				SET_SP_SELECTOR_SKYCAM_UP(TRUE)
//			ENDIF
		ENDIF
		sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
		bUISelection = TRUE
		bLaunchFromEventReceived = TRUE
	ENDIF
	
	IF HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND g_sPlayerPedRequest.eState != PR_STATE_PROCESSING 
	
		FORCE_RESPAWN()
		sSelectorPeds.bInitialised = TRUE
		PRINTLN("BC: JAMES A - SELECTOR HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT = TRUE")
		IF NOT GET_IS_LOADING_SCREEN_ACTIVE()
//			IF g_bScriptsSetSafeForCutscene = TRUE
//			AND IS_GAMEPLAY_CAM_RENDERING() = FALSE
//				SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
//			ELSE
//				SET_SP_SELECTOR_SKYCAM_UP(TRUE)
//			ENDIF
		ENDIF
		sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
		bUISelection = TRUE
		bLaunchFromEventReceived = TRUE
	ENDIF
	
	IF HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED()
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND g_sPlayerPedRequest.eState != PR_STATE_PROCESSING 
	
		FORCE_RESPAWN()
		sSelectorPeds.bInitialised = TRUE
		PRINTLN("BC: JAMES A - SELECTOR HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED = TRUE")
		IF NOT GET_IS_LOADING_SCREEN_ACTIVE()
//			IF g_bScriptsSetSafeForCutscene = TRUE
//			AND IS_GAMEPLAY_CAM_RENDERING() = FALSE
//				SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
//			ELSE
//				SET_SP_SELECTOR_SKYCAM_UP(TRUE)
//			ENDIF
		ENDIF
		sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
		bUISelection = TRUE
		bLaunchFromEventReceived = TRUE
	ENDIF
	
	IF NOT bMPAutoBootFromStartup
		//This check has been moved to a cached version at the beginning of startup.sc.
		//Search project for LOAD_SCREEN_CHOICE_IS_MULTIPLAYER() and g_bRunMultiplayerOnStartup - BenR
		IF g_bRunMultiplayerOnStartup
		
// 		OR LOBBY_AUTO_MULTIPLAYER_FREEMODE()	// -StraightIntoFreemode 
//		IF LOBBY_AUTO_MULTIPLAYER_MENU() 		// -netAutoMultiplayerMenu
//		OR LOBBY_AUTO_MULTIPLAYER_CNC()			// -StraightIntoCNC
//		OR NETWORK_AUTO_MULTIPLAYER_LAUNCH() 	// -netAutoMultiplayerLaunch - Not used but keeping the text link here. 
//		OR LOBBY_AUTO_MULTIPLAYER_CREATOR()		// -StraightIntoCreator - Not used but keeping the text link here.

			CPRINTLN(DEBUG_INIT, "<LOAD-CHOICE> Selector is simulating a switch into MP through the selector HUD.")
			#IF USE_FINAL_PRINTS 
				PRINTLN_FINAL("<2140379> <LOAD-CHOICE> Selector is simulating a switch into MP through the selector HUD.")
			#ENDIF
			
			SET_BIT(g_b_EnteredMaintransitionSelectorBitset, ENUM_TO_INT(JT_ONLINE_BUTTON))
			g_b_isSelectorAbouttoBootMaintransition = TRUE
			sSelectorPeds.bInitialised = TRUE
			sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MULTIPLAYER
			bUISelection = TRUE
			bMPAutoBootFromStartup = TRUE
		ENDIF
	ENDIF
	
	
	// Process any selections
	IF bUISelection
	
		DRAW_FAKE_SWITCH()
		
	
		BOOL Run_warning_Screen = TRUE
	
		IF g_Private_SwitchedSP_DuringActivity = FALSE
			NET_NL()NET_PRINT("g_Private_SwitchedSP_DuringActivity = FALSE: Run_warning_Screen = FALSE ")
			Run_warning_Screen = FALSE
		ENDIF
		
		IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			NET_NL()NET_PRINT("IS_PAUSE_MENU_REQUESTING_TRANSITION() = FALSE: Run_warning_Screen = FALSE ")
			Run_warning_Screen = FALSE
			
		ENDIF
		
		//Global for BG script control for bug  
		//url:bugstar:3770047 - [PUBLIC][EXPLOIT] Off freemode script / "off the radar" exploit using recent GTAO activity, sp transition and friend in different aiming mode
		IF g_b_SwitchSelectorLiveTileInviteShowScreenProcess = TRUE
			IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
				NET_NL()NET_PRINT("GET_CONFIRM_INVITE_INTO_GAME_STATE() = TRUE: Run_warning_Screen = FALSE g_b_SwitchSelectorLiveTileInviteShowScreenProcess = TRUE")
				Run_warning_Screen = FALSE
			ENDIF
		ENDIF
		
		
		
		IF HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED()
			NET_NL()NET_PRINT("BC: HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED() = TRUE: ")
			IF GET_IS_LOADING_SCREEN_ACTIVE()
				Run_warning_Screen = FALSE
			ELSE
				Run_warning_Screen = TRUE
			ENDIF
			SET_LAUNCHED_LIVETILE_WHILE_TRANSITIONING(FALSE)

		ENDIF
		
		IF HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT()
			NET_NL()NET_PRINT("BC: HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT() = TRUE: ")
			IF GET_IS_LOADING_SCREEN_ACTIVE()
				Run_warning_Screen = FALSE
			ELSE
				Run_warning_Screen = TRUE
			ENDIF
			SET_LAUNCHED_LIVETILE_WHILE_TRANSITIONING(FALSE)
			//Clear that the last one was set up
			CLEAR_HAVE_DONE_INITIAL_PS4_LIVE_STREAM_SETUP()
			CLEAR_PS4_LIVE_STREAM_LAUNCH_ACTIVE()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				SET_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
			ENDIF
			
			SET_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
		ENDIF
		
		IF HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED()
			NET_NL()NET_PRINT("BC: HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED() = TRUE: ")
			IF GET_IS_LOADING_SCREEN_ACTIVE()
				Run_warning_Screen = FALSE
			ELSE
				Run_warning_Screen = TRUE
			ENDIF
			SET_LAUNCHED_LIVETILE_WHILE_TRANSITIONING(FALSE)
		ENDIF


		//Global for BG script control for bug  
		//url:bugstar:3770047 - [PUBLIC][EXPLOIT] Off freemode script / "off the radar" exploit using recent GTAO activity, sp transition and friend in different aiming mode
		IF g_b_SwitchSelectorLiveTileInviteShowScreenProcess = FALSE
			IF GET_CONFIRM_INVITE_INTO_GAME_STATE()
				NET_NL()NET_PRINT("GET_CONFIRM_INVITE_INTO_GAME_STATE() = TRUE: Run_warning_Screen = FALSE g_b_SwitchSelectorLiveTileInviteShowScreenProcess = FALSE")
				Run_warning_Screen = FALSE
			ENDIF
		ENDIF

		#IF IS_DEBUG_BUILD
			IF GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL(DEFAULT, FALSE)	
				Run_warning_Screen = FALSE
			ENDIF
		#ENDIF
	
		// [DIRECTOR_TODO]
		#IF FEATURE_SP_DLC_DIRECTOR_MODE
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			AND NOT bLaunchFromEventReceived		//B* 3765314: Don't trigger the Trailer switch if moving into MP
				SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
				IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
					CPRINTLN(debug_director,"Detected switch to Casting Trailer from the Selector script")
					CDEBUG1LN(DEBUG_DIRECTOR,"MAINTAIN_AMBIENT_SELECTOR: setting g_bDirectorSwitchToCastingTrailer to TRUE")
					g_bDirectorSwitchToCastingTrailer = TRUE
				ELIF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(g_savedGlobals.sPlayerData.sInfo.eLastKnownPed))
					g_bDirectorSwitchToLastPlayer = TRUE
				ENDIF
				
				// Clear the selection so multiplayer doesn't kick in.
				sSelectorPeds.eNewSelectorPed = NUMBER_OF_SELECTOR_PEDS
			ELIF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				CPRINTLN(DEBUG_DIRECTOR,"Attemtped switch to Casting Trailer from a received online event. Skipping switch.")
			ENDIF
		#ENDIF
		
		IF (HAS_ANY_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds))
			IF g_bMagdemoTakingOverSwitch
				g_bMagdemoDoTakeOverSwitch = TRUE
			ELSE
#IF FEATURE_GEN9_STANDALONE
				// Check player has an active character before performing switch request to MP, otherwise take them to Career Builder if applicable
				IF HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds)
				AND IS_STAT_CHARACTER_ACTIVE(0) = FALSE
				AND IS_STAT_CHARACTER_ACTIVE(1) = FALSE
				AND NETWORK_GET_MP_WINDFALL_AVAILABLE()
					PRINTLN("MAINTAIN_AMBIENT_SELECTOR - Multiplayer requested but no existing characters and Windfall is available. Redirecting to Windfall.")
					SET_LAUNCH_LANDING_PAGE_TO_WINDFALL()
				// Check player entitlement for switching to Story mode (i.e. not a Multiplayer selection)
				ELIF HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds) = FALSE AND GET_PLAYER_HAS_STORY_MODE_ENTITLEMENT() = FALSE
					PRINTLN("MAINTAIN_AMBIENT_SELECTOR - Singleplayer requested but player has no Story mode entitlement.")
					// Do nothing here, player cannot switch to SP characters without Story mode entitlement.
					// They will see the SP upsell prompt when this selection is processed below
				ELSE
#ENDIF // FEATURE_GEN9_STANDALONE
					MAKE_PLAYER_PED_SWITCH_REQUEST(GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(sSelectorPeds.eNewSelectorPed), (NOT Run_warning_Screen))	//, PR_TYPE_AMBIENT)
#IF FEATURE_GEN9_STANDALONE
				ENDIF
#ENDIF // FEATURE_GEN9_STANDALONE
			ENDIF
		ENDIF
		
		IF HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds)
		OR IS_TRANSITION_MENU_TRIGGERED()
		OR IS_PAUSE_MENU_REQUESTING_TRANSITION()
		OR NETWORK_IS_GAME_IN_PROGRESS()
		OR GET_CONFIRM_INVITE_INTO_GAME_STATE()
		OR HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED()
		OR HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT()
		OR HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED()
		OR HAVE_I_BEEN_KICKED() 
		#IF FEATURE_FREEMODE_ARCADE OR IS_RETURN_TO_GTA_ONLINE() #ENDIF
		#IF IS_DEBUG_BUILD OR GET_QUICKLAUNCH_STATE() != QUICKLAUNCH_EMPTY #ENDIF
		
			#IF USE_REPLAY_RECORDING_TRIGGERS
				CLEANUP_REPLAY_RECORD_FEED()
			#ENDIF
		
			
			
//			HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
	
			
			IF Run_warning_Screen 
			
				
				BOOL bBringUpSignInUI
			
				MULTIPLAYER_ACCESS_CODE nAccessCode
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
					SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
				ENDIF
				
				SET_AUDIO_FLAG("ActivateSwitchWheelAudio", FALSE)
				
				RUN_TRANSITION_SFX(TRUE)
				
				#IF FEATURE_GEN9_STANDALONE
				SCRIPT_ROUTER_CONTEXT_DATA srcData	// Used for switching to SP character
				#ENDIF // FEATURE_GEN9_STANDALONE
				
				WARNING_SCREEN_RETURN aResult = WARNING_SCREEN_RETURN_NONE
				WHILE Run_warning_Screen = TRUE 
					DRAW_FAKE_SWITCH()
					
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					
//					HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
					
					SET_ALL_PLAYER_OVERHEADS_VISIBILITY(FALSE)
					
					SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode)
					#IF IS_DEBUG_BUILD
								
						PRINTLN("\n***")
						PRINTLN("Multiplayer has been selected for SWITCH! BUT IS_PLAYER_ONLINE() = ", IS_PLAYER_ONLINE())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_SIGNED_ONLINE() = ", NETWORK_IS_SIGNED_ONLINE())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_SIGNED_IN() = ", NETWORK_IS_SIGNED_IN())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = ", NETWORK_HAS_VALID_ROS_CREDENTIALS())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_CAN_ENTER_MULTIPLAYER() = ", NETWORK_CAN_ENTER_MULTIPLAYER())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode) = ", SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode), " nAccessCode = ", nAccessCode)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed = ", g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT nAccessCode = ", nAccessCode)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT IS_SP_PROLOGUE_FINISHED = ", IS_SP_PROLOGUE_FINISHED())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT IS_PLAYER_LOGGING_IN_NP = ", IS_PLAYER_LOGGING_IN_NP())
						IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
							PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_GET_NP_UNAVAILABLE_REASON() = ", GET_UNAVAILABILITY_REASON_STRING(NETWORK_GET_NP_UNAVAILABLE_REASON()))
						ENDIF
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE = ", NETWORK_IS_CLOUD_AVAILABLE())
						IF NETWORK_IS_SIGNED_IN()
							PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_HAVE_PLATFORM_SUBSCRIPTION = ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION())
						ENDIF
						IF IS_PLAYSTATION_PLATFORM()
							PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_NP_AVAILABLE = ", NETWORK_IS_NP_AVAILABLE())
						ENDIF
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsSignedIn = ", g_SignInStructDetails.bIsSignedIn)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsDuplicateSignIn = ", g_SignInStructDetails.bIsDuplicateSignIn)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bWasOnline = ", g_SignInStructDetails.bWasOnline)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bWasSignedIn = ", g_SignInStructDetails.bWasSignedIn)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsOnline = ", g_SignInStructDetails.bIsOnline)
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_REFRESHING_ROS_CREDENTIALS() = ", NETWORK_IS_REFRESHING_ROS_CREDENTIALS())
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CABLE_CONNECTED() = ", NETWORK_IS_CABLE_CONNECTED())
						PRINTLN("***\n")
					#ENDIF
					
					
					#IF USE_FINAL_PRINTS	
					PRINTLN_FINAL("\n***")
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT IS_PLAYER_ONLINE() = ", IS_PLAYER_ONLINE())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_SIGNED_ONLINE() = ", NETWORK_IS_SIGNED_ONLINE())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_SIGNED_IN() = ", NETWORK_IS_SIGNED_IN())					
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = ", NETWORK_HAS_VALID_ROS_CREDENTIALS())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_CAN_ENTER_MULTIPLAYER() = ", NETWORK_CAN_ENTER_MULTIPLAYER())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode) = ", SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode), " nAccessCode = ", nAccessCode)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed = ", g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT nAccessCode = ", nAccessCode)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT IS_SP_PROLOGUE_FINISHED = ", IS_SP_PROLOGUE_FINISHED())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT IS_PLAYER_LOGGING_IN_NP = ", IS_PLAYER_LOGGING_IN_NP())
					IF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
						PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_GET_NP_UNAVAILABLE_REASON() = ", GET_UNAVAILABILITY_REASON_STRING(NETWORK_GET_NP_UNAVAILABLE_REASON()))
					ENDIF
					
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE = ", NETWORK_IS_CLOUD_AVAILABLE())
					IF NETWORK_IS_SIGNED_IN()
						PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_HAVE_PLATFORM_SUBSCRIPTION = ", NETWORK_HAVE_PLATFORM_SUBSCRIPTION())
					ENDIF
					IF IS_PLAYSTATION_PLATFORM()
						PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_NP_AVAILABLE = ", NETWORK_IS_NP_AVAILABLE())
					ENDIF
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsSignedIn = ", g_SignInStructDetails.bIsSignedIn)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsDuplicateSignIn = ", g_SignInStructDetails.bIsDuplicateSignIn)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bWasOnline = ", g_SignInStructDetails.bWasOnline)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bWasSignedIn = ", g_SignInStructDetails.bWasSignedIn)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT g_SignInStructDetails.bIsOnline = ", g_SignInStructDetails.bIsOnline)
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_REFRESHING_ROS_CREDENTIALS() = ", NETWORK_IS_REFRESHING_ROS_CREDENTIALS())
					PRINTLN_FINAL("Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CABLE_CONNECTED() = ", NETWORK_IS_CABLE_CONNECTED())

					#ENDIF	
					
					
					
		
					IF NOT IS_GTA_ONLINE_AVAILABLE()
					    IF NOT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(nAccessCode)
						OR SYSTEM_IS_MP_AVAILABLE() = FALSE
							
							
							IF nAccessCode = ACCESS_DENIED_NETWORK_LOCKED
							
								aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_NETWORK_LOCKED  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_NETWORK_LOCKED ")
								#ENDIF
							
					        ELIF nAccessCode = ACCESS_DENIED_NOT_SIGNED_IN
					            // display alert, show sign in ui
								
								IF bBringUpSignInUI = FALSE AND IS_SCARLETT_VERSION() = FALSE
									SET_TIME_SCALE(1.0)
									DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
									RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
									bBringUpSignInUI = TRUE
								ENDIF
								
								//Sign in 
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_IN ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_IN ")
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(TRUE)
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
					        ELIF nAccessCode = ACCESS_DENIED_NOT_SIGNED_ONLINE
					            // display alert, show sign in ui
								IF bBringUpSignInUI = FALSE
									SET_TIME_SCALE(1.0)
									DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
									RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
									
									PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN DISPLAY_SYSTEM_SIGNIN_UI(TRUE) - called ")
									PRINTLN("***\n")
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN DISPLAY_SYSTEM_SIGNIN_UI(TRUE) - called ")
									#ENDIF
									bBringUpSignInUI = TRUE
								ENDIF
								
								//Sign in 
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_ONLINE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_ONLINE  ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(TRUE)
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							
							
							ELIF NETWORK_IS_CABLE_CONNECTED() = FALSE
					
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_CABLE_CONNECTED = FALSE 1 ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_CABLE_CONNECTED = FALSE 1 ")								
								#ENDIF
								
								aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_NO_CABLE()
			
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF NETWORK_IS_REFRESHING_ROS_CREDENTIALS()
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_REFRESHING_ROS_CREDENTIALS = TRUE, show nothing and just wait a bit ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_REFRESHING_ROS_CREDENTIALS = TRUE, show nothing and just wait a bit  ")								
								#ENDIF
								
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING()
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_NP_PENDING = TRUE, show nothing and just wait a bit ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_NP_PENDING = TRUE, show nothing and just wait a bit  ")								
								#ENDIF
								
							
							ELIF IS_XBOX_PLATFORM() AND nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE	//Show new system UI
								//Sign in 
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT IS_XBOX_PLATFORM and ACCESS_DENIED_NO_ONLINE_PRIVILEGE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT IS_XBOX_PLATFORM and ACCESS_DENIED_NO_ONLINE_PRIVILEGE ")								
								#ENDIF
								
								IF bShownPSPlusSignIn = FALSE
									DOES_PLAYER_HAVE_XBOX_PERMISSIONS(TRUE)
									bShownPSPlusSignIn = TRUE
								ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF	
								
							
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
							AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_AGE
							
								// display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL]  Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN  ")								
								#ENDIF
								
								aResult = RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN()
	
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
							AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_GAME_UPDATE
							
								// display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE  ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE()
	
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
							AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_SYSTEM_UPDATE
							
								// display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE ")								
								#ENDIF
								
								aResult = RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE()
	
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
							AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_CONNECTION
							
								// display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with reason REASON_CONNECTION ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with reason REASON_CONNECTION ")								
								#ENDIF
								
								aResult = RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION()
	
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
							
//							ELIF IS_PC_VERSION()
							ELIF nAccessCode = ACCESS_DENIED_NO_SCS_PRIVILEGE
					            // display alert
								
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PC_VERSION RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PC_VERSION RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED  ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF

								
							ELIF NETWORK_IS_CLOUD_AVAILABLE() = FALSE
							
								IF HAS_NET_TIMER_EXPIRED(g_st_SigningOutSelectorTimer, 3000)
									PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE, - DO SCREEN  ")
									PRINTLN("***\n")
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE, - DO SCREEN  ")								
									#ENDIF
									
									aResult = RUN_WARNINGSCREEN_TRANSITION_CLOUD_DOWN()
									IF aResult = WARNING_SCREEN_RETURN_ACCEPT
										aResult = WARNING_SCREEN_RETURN_CANCEL
									ENDIF	
								ELSE
									PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE, - NO SCREEN  ")
									PRINTLN("***\n")
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE, - NO SCREEN  ")								
									#ENDIF
								ENDIF

							ELIF NETWORK_IS_PLATFORM_SUBSCRIPTION_CHECK_PENDING() = TRUE
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_PLATFORM_SUBSCRIPTION_CHECK_PENDING = TRUE, show nothing and just wait a bit ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_PLATFORM_SUBSCRIPTION_CHECK_PENDING = TRUE, show nothing and just wait a bit  ")								
								#ENDIF
								
							ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
								 // display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE ")								
								#ENDIF
								
								IF bShownPSPlusSignIn = FALSE
									NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
									bShownPSPlusSignIn = TRUE
								ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF IS_XBOX_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
								 // display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE ")								
								#ENDIF
								
								IF bShownPSPlusSignIn = FALSE
									DOES_PLAYER_HAVE_XBOX_GOLD_MEMBERSHIP(TRUE)
									bShownPSPlusSignIn = TRUE
								ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF IS_MOD_INSTALLED()
								 // display alert
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED IS_MOD_INSTALLED()  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED IS_MOD_INSTALLED() ")								
								#ENDIF
								
								aResult = RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
					        ELIF nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE
							

					           	 // display alert
								//PRINTLN("Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS ")
								//PRINTLN("***\n")
								//aResult = RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS()
	
								
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE  ")								
								#ENDIF
								
								IF IS_PLAYSTATION_PLATFORM()
									aResult = RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN()
								ELSE
									aResult = RUN_WARNINGSCREEN_TRANSITION_UNDERAGE()
								ENDIF
								
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
	
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							
							
								
					        ELIF nAccessCode = ACCESS_DENIED_NO_SCS_PRIVILEGE
					            // display alert
								
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED  ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF

							
							ELIF nAccessCode = ACCESS_DENIED_MULTIPLAYER_DISABLED
							OR NETWORK_IS_MULTIPLAYER_DISABLED() 
							
								aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_MAINTENANCE()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
									
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_MULTIPLAYER_DISABLED  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_MULTIPLAYER_DISABLED  ")								
								#ENDIF
								
							ELIF NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE, - NO SCREEN  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE, - NO SCREEN  ")								
								#ENDIF
								
								RESET_NET_TIMER(g_st_ROSInvalidSelectorTimer)	
								
							ELIF NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING()	
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING() = TRUE, - NO SCREEN  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING() = TRUE, - NO SCREEN  ")								
								#ENDIF
								
							ELIF nAccessCode = ACCESS_DENIED_NO_TUNABLES 
											
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_NO_TUNABLES in NOT IS_GTA_ONLINE_AVAILABLE ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_NO_TUNABLES in NOT IS_GTA_ONLINE_AVAILABLE  ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_TUNABLES()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
								
							ELIF NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING()
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING() = TRUE, - NO SCREEN  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING() = TRUE, - NO SCREEN  ")								
								#ENDIF
								
							ELIF nAccessCode =  ACCESS_DENIED_NO_BACKGROUND_SCRIPT //
							
							
								IF HAS_NET_TIMER_EXPIRED(g_st_ROSInvalidSelectorTimer, 3000)
									PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT ")
									PRINTLN("***\n")
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT  ")								
									#ENDIF
									
									aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_BACKGROUND()
									IF aResult = WARNING_SCREEN_RETURN_ACCEPT
										aResult = WARNING_SCREEN_RETURN_CANCEL
									ENDIF
								ELSE
									PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT - NO SCREEN - Wait a bit ")
									PRINTLN("***\n")
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT - NO SCREEN - Wait a bit  ")								
									#ENDIF
									
									IF BUSYSPINNER_IS_ON() = FALSE
										BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
										END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
									ENDIF
									
								ENDIF
						
							ELIF nAccessCode = ACCESS_DENIED_INVALID_PROFILE_SETTINGS
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_INVALID_PROFILE_SETTINGS in NOT IS_GTA_ONLINE_AVAILABLE - NO SCREEN ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_INVALID_PROFILE_SETTINGS in NOT IS_GTA_ONLINE_AVAILABLE - NO SCREEN  ")
								#ENDIF
							ELSE
							
							
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Prologue check 1 Run  ")
								PRINTLN("***\n")
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Prologue check 1 Run   ")								
								#ENDIF
								aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_SP_PROLOGUE_NOT_DONE()
								IF aResult = WARNING_SCREEN_RETURN_ACCEPT
									aResult = WARNING_SCREEN_RETURN_CANCEL
								ENDIF
						
							
					        ENDIF
							
						ELSE
							
							
							PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Prologue check 2 Run  ")
							PRINTLN("***\n")
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Prologue check 2 Run  ")								
							#ENDIF
							aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_SP_PROLOGUE_NOT_DONE()
							IF aResult = WARNING_SCREEN_RETURN_ACCEPT
								aResult = WARNING_SCREEN_RETURN_CANCEL
							ENDIF
							
					        
						ENDIF
					
				
				
					ELIF IS_MOD_INSTALLED() 
						 // display alert
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED IS_MOD_INSTALLED()  2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED IS_MOD_INSTALLED()  2 ")								
						#ENDIF
						
						
						aResult = RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF

					ELIF nAccessCode = ACCESS_DENIED_TUNABLE_NOT_FOUND
											
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_TUNABLE_NOT_FOUND ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_TUNABLE_NOT_FOUND   ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
					ELIF nAccessCode = ACCESS_DENIED_NETWORK_LOCKED
						
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_NETWORK_LOCKED - NO SCREEN ")
						PRINTLN("***\n")		
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_NETWORK_LOCKED - NO SCREEN ")								
						#ENDIF
						
						
					ELIF nAccessCode = ACCESS_DENIED_NOT_SIGNED_IN
			            // display alert, show sign in ui
						
						IF bBringUpSignInUI = FALSE AND IS_SCARLETT_VERSION() = FALSE
							SET_TIME_SCALE(1.0)
							DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
							RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
							bBringUpSignInUI = TRUE
						ENDIF
						
						//Sign in 
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_IN 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_IN 2 ")
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(TRUE)
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
			        ELIF nAccessCode = ACCESS_DENIED_NOT_SIGNED_ONLINE
			            // display alert, show sign in ui
						IF bBringUpSignInUI = FALSE
							SET_TIME_SCALE(1.0)
							DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
							RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
							
							PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN DISPLAY_SYSTEM_SIGNIN_UI(TRUE) - called 2 ")
							PRINTLN("***\n")
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN DISPLAY_SYSTEM_SIGNIN_UI(TRUE) - called 2 ")
							#ENDIF
							bBringUpSignInUI = TRUE
						ENDIF
						
						//Sign in 
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_ONLINE 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN ACCESS_DENIED_NOT_SIGNED_ONLINE 2 ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(TRUE)
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
								
					ELIF IS_XBOX_PLATFORM() AND nAccessCode = ACCESS_DENIED_NO_ONLINE_PRIVILEGE	//Show new system UI
						//Sign in 
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT IS_XBOX_PLATFORM and ACCESS_DENIED_NO_ONLINE_PRIVILEGE 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT IS_XBOX_PLATFORM and ACCESS_DENIED_NO_ONLINE_PRIVILEGE 2 ")								
						#ENDIF
						
						IF bShownPSPlusSignIn = FALSE
							DOES_PLAYER_HAVE_XBOX_PERMISSIONS(TRUE)
							bShownPSPlusSignIn = TRUE
						ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
						
					ELIF NETWORK_IS_CABLE_CONNECTED() = FALSE
					
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_CABLE_CONNECTED = FALSE ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_CABLE_CONNECTED = FALSE ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_NO_CABLE()
	
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
					ELIF NETWORK_IS_REFRESHING_ROS_CREDENTIALS()
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_REFRESHING_ROS_CREDENTIALS = TRUE, show nothing and just wait a bit 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_REFRESHING_ROS_CREDENTIALS = TRUE, show nothing and just wait a bit 2  ")								
						#ENDIF	
	
					ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_PENDING()
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_NP_PENDING = TRUE, show nothing and just wait a bit 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_NP_PENDING = TRUE, show nothing and just wait a bit 2 ")								
						#ENDIF
		
					//For bug 2008393
					ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
					AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_OTHER

						// display alert
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with REASON_OTHER ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with REASON_OTHER ")								
						#ENDIF
						
						aResult = RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION()

						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
							
					ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_IS_NP_AVAILABLE() = FALSE
					AND NETWORK_GET_NP_UNAVAILABLE_REASON() = REASON_CONNECTION
					
						// display alert
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with reason REASON_CONNECTION ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION with reason REASON_CONNECTION ")								
						#ENDIF
						
						aResult = RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION()

						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
	
					ELIF NETWORK_IS_SIGNED_ONLINE() = FALSE
					
						IF bBringUpSignInUI = FALSE
							SET_TIME_SCALE(1.0)
							DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
							RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
							bBringUpSignInUI = TRUE
						ENDIF
						
						//Sign in 
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN NETWORK_IS_SIGNED_ONLINE = FALSE ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN NETWORK_IS_SIGNED_ONLINE = FALSE  ")								
						#ENDIF
						
						aResult = RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(TRUE, FALSE)
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
//					ELIF NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE
//						aResult = RUN_WARNINGSCREEN_SELECTOR_TRANSITION_CLOUD_DOWN()
						
						
//					ELIF NETWORK_IS_MULTIPLAYER_DISABLED()
//						PRINTLN("Multiplayer has been selected for SWITCH! BUT Run NETWORK_IS_MULTIPLAYER_DISABLED() = TRUE ")
//						PRINTLN("***\n")
//						
//						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_HEAVY_LOAD()
//						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
//							aResult = WARNING_SCREEN_RETURN_CANCEL
//						ENDIF
					
					ELIF IS_PLAYSTATION_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
					AND HAS_SYSTEM_SERVICE_TRIGGERED() = FALSE					
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL]  Multiplayer has been selected for SWITCH! BUT Run NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE 2 ")								
						#ENDIF
						
						IF IS_PLAYSTATION_PLATFORM()
							
							IF IS_SYSTEM_UI_BEING_DISPLAYED() = FALSE
							AND bShownPSPlusSignIn = TRUE
								aResult = RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS()
							ENDIF
							IF bShownPSPlusSignIn = FALSE
								NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
								bShownPSPlusSignIn = TRUE
							ENDIF
							
						ELSE
							aResult = RUN_WARNINGSCREEN_TRANSITION_UNDERAGE()
						ENDIF
						
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
					ELIF IS_XBOX_PLATFORM() AND NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE
					AND HAS_SYSTEM_SERVICE_TRIGGERED() = FALSE		
						 // display alert
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = FALSE 2 ")								
						#ENDIF
						
						IF bShownPSPlusSignIn = FALSE
							DOES_PLAYER_HAVE_XBOX_GOLD_MEMBERSHIP(TRUE)
							bShownPSPlusSignIn = TRUE
						ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
//					
					ELIF  GET_ACCOUNT_PERMISSION_SETUP() = ACCOUNT_PERMS_BLOCKED
	
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run GET_ACCOUNT_PERMISSION_SETUP() = ACCOUNT_PERMS_BLOCKED ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL]  Multiplayer has been selected for SWITCH! BUT Run GET_ACCOUNT_PERMISSION_SETUP() = ACCOUNT_PERMS_BLOCKED  ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_UNDERAGE()
						
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
		
						
//					ELIF NETWORK_CAN_ENTER_MULTIPLAYER() = FALSE	
//					AND GET_CURRENT_GAMEMODE() != GAMEMODE_FM	
					
					ELIF IS_PLAYER_LOGGING_IN_NP() = TRUE
	
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PLAYER_LOGGING_IN_NP() = TRUE ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PLAYER_LOGGING_IN_NP() = TRUE ")								
						#ENDIF
						
//					ELIF IS_PC_VERSION()
					ELIF nAccessCode = ACCESS_DENIED_NO_SCS_PRIVILEGE
			            // display alert
						
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PC_VERSION RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_PC_VERSION RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED 2 ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF

									
						
					ELIF NETWORK_IS_CLOUD_AVAILABLE() = FALSE
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_AVAILABLE() = FALSE ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_CLOUD_DOWN()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF	
						
					ELIF NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE
						PRINTLN("Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE, - NO SCREEN  ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_HAS_VALID_ROS_CREDENTIALS() = FALSE, - NO SCREEN   ")								
						#ENDIF	
								
						RESET_NET_TIMER(g_st_ROSInvalidSelectorTimer)	
					
					ELIF NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING()	
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING() = TRUE, - NO SCREEN 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_TUNABLE_CLOUD_REQUEST_PENDING() = TRUE, - NO SCREEN 2 ")								
						#ENDIF
					
					ELIF nAccessCode = ACCESS_DENIED_NO_TUNABLES AND GET_CURRENT_GAMEMODE() != GAMEMODE_FM
											
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_NO_TUNABLES 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run ACCESS_DENIED_NO_TUNABLES 2 ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_TUNABLES()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
						
					ELIF NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING()
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING() = TRUE, - NO SCREEN 2 ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT NETWORK_IS_CLOUD_BACKGROUND_SCRIPT_REQUEST_PENDING() = TRUE, - NO SCREEN 2 ")								
						#ENDIF	
						
						IF BUSYSPINNER_IS_ON() = FALSE
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
						ENDIF
										
					ELIF nAccessCode =  ACCESS_DENIED_NO_BACKGROUND_SCRIPT //
					
					
						
						IF HAS_NET_TIMER_EXPIRED(g_st_ROSInvalidSelectorTimer, 3000)
							PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT ")
							PRINTLN("***\n")
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT  ")								
							#ENDIF
							aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_BACKGROUND()
							IF aResult = WARNING_SCREEN_RETURN_ACCEPT
								aResult = WARNING_SCREEN_RETURN_CANCEL
							ENDIF
						ELSE
							PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT - NO SCREEN - Wait a bit ")
							PRINTLN("***\n")
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run  ACCESS_DENIED_NO_BACKGROUND_SCRIPT - NO SCREEN - Wait a bit  ")								
							#ENDIF
							
						ENDIF
						
					
					ELIF nAccessCode = ACCESS_DENIED_MULTIPLAYER_DISABLED
					OR NETWORK_IS_MULTIPLAYER_DISABLED() 
					
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_MAINTENANCE()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
							
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_MULTIPLAYER_DISABLED - NO SCREEN  ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT ACCESS_DENIED_MULTIPLAYER_DISABLED - NO SCREEN   ")								
						#ENDIF
					#IF NOT FEATURE_GEN9_STANDALONE
					ELIF IS_SP_PROLOGUE_FINISHED() = FALSE
					
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_SP_PROLOGUE_FINISHED() = FALSE ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run IS_SP_PROLOGUE_FINISHED() = FALSE   ")								
						#ENDIF
						aResult = RUN_WARNINGSCREEN_TRANSITION_SELECTOR_SP_PROLOGUE_NOT_DONE()
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							aResult = WARNING_SCREEN_RETURN_CANCEL
						ENDIF
					#ENDIF // NOT FEATURE_GEN9_STANDALONE
					
					ELIF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run QUIT MP? ")
								PRINTLN("***\n")
						#IF USE_FINAL_PRINTS		
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run QUIT MP?   ")	
						#ENDIF
						
						// Show the SP upsell prompt if applicable
						#IF FEATURE_GEN9_STANDALONE
						IF NOT HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds)
						AND NOT HAS_STORY_ENTITLEMENT()
							aResult = RUN_WARNINGSCREEN_ENTER_SP_UPSELL()
							
							IF aResult = WARNING_SCREEN_RETURN_ACCEPT
								SET_LAUNCH_LANDING_PAGE_TO_SP_UPSELL(SPL_CHARACTER_SELECTION_WHEEL)
							ENDIF
						ELSE
						#ENDIF // FEATURE_GEN9_STANDALONE
							aResult = RUN_WARNINGSCREEN_QUIT_GTAONLINE()
							
							IF aResult = WARNING_SCREEN_RETURN_ACCEPT
								RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL(DEFAULT, FALSE)
								
								#IF FEATURE_GEN9_STANDALONE
								// Set script router link for return to story mode
								srcData.eSource = SRCS_SCRIPT
								srcData.eMode = SRCM_STORY
								srcData.eArgType = SRCA_NONE
								SET_SCRIPT_ROUTER_LINK(srcData)
								#ENDIF // FEATURE_GEN9_STANDALONE
							ENDIF
						#IF FEATURE_GEN9_STANDALONE
						ENDIF
						#ENDIF // FEATURE_GEN9_STANDALONE
						
					ELSE
						
						PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run QUIT SP? ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT Run QUIT SP?    ")	
						#ENDIF
						aResult = RUN_WARNINGSCREEN_QUIT_SINGLEPLAYER()
						
						IF aResult = WARNING_SCREEN_RETURN_ACCEPT
							RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL(DEFAULT, FALSE)
						ENDIF
					ENDIF
					
					
					IF IS_PLAYER_IN_CORONA()
					AND IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
					AND NOT(HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED()
					OR HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT()
					OR HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED())
						PRINTLN("[TS] [BCWHEEL]  Multiplayer has been selected for SWITCH! BUT Player is in a corona! - QUIT ")
						PRINTLN("***\n")
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[TS] [BCWHEEL] Multiplayer has been selected for SWITCH! BUT Player is in a corona! - QUIT   ")	
						#ENDIF
						
						aResult = WARNING_SCREEN_RETURN_CANCEL
					ENDIF

					
					
					IF aResult = WARNING_SCREEN_RETURN_ACCEPT
						Run_warning_Screen = FALSE
						bShownPSPlusSignIn = FALSE 

						IF ( HAS_EVENT_APP_LAUNCH_STRAIGHT_INTO_FREEMODE_RECIEVED()
						OR HAS_EVENT_APP_LAUNCH_DIRECT_TO_CONTENT()
						OR HAS_EVENT_APP_LAUNCH_LIVE_AREA_RECIEVED())
						AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						AND g_sPlayerPedRequest.eState != PR_STATE_PROCESSING 
						
							IF g_bScriptsSetSafeForCutscene = TRUE
							AND IS_GAMEPLAY_CAM_RENDERING() = FALSE
								SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
							ELSE
								SET_SP_SELECTOR_SKYCAM_UP(TRUE)
							ENDIF
						ENDIF
						
					ELIF aResult = WARNING_SCREEN_RETURN_CANCEL
						Run_warning_Screen = FALSE
						bShownPSPlusSignIn = FALSE 
						g_iExternalWarningScreenStages = 0
						REFRESH_ALL_OVERHEAD_DISPLAY()
						SET_ALL_PLAYER_OVERHEADS_VISIBILITY(TRUE)
						EXTERNAL_TRANSITION_CLEANUP(FALSE)
						CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING()
						CLEAR_TRANSITION_SESSION_LAUNCHED_LIVE_TILE_WHEN_GAME_RUNNING_MP()
						g_b_ReapplyStickySaveFailedFeed = FALSE
						RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()
						bUISelection = FALSE
					ENDIF
					
					IF HAS_QUIT_CURRENT_GAME()
						bShownPSPlusSignIn = FALSE 
						Run_warning_Screen = FALSE
						g_iExternalWarningScreenStages = 0
						REFRESH_ALL_OVERHEAD_DISPLAY()
						SET_ALL_PLAYER_OVERHEADS_VISIBILITY(TRUE)
					ENDIF
					
					
					WAIT (0) 
					IF (NETWORK_IS_SIGNED_ONLINE() = FALSE )
						SET_TIME_SCALE(1.0)
					ENDIF
					IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM
					OR (NETWORK_IS_SIGNED_ONLINE() = FALSE AND (IS_SYSTEM_UI_BEING_DISPLAYED() OR IS_WARNING_MESSAGE_ACTIVE())))
						IF IS_SCREEN_FADED_IN()
							IF IS_WARNING_MESSAGE_ACTIVE() OR IS_SYSTEM_UI_BEING_DISPLAYED()
								IF SHOULD_FADE_IN_SCREEN_WITH_MESSAGE()
									IF IS_SCREEN_FADED_IN()
									AND NOT IS_SCREEN_FADING_OUT()
										DO_SCREEN_FADE_IN(0)
										PRINTLN("[BCWHEEL] DO_SCREEN_FADE_IN(0) 1")
									ENDIF
								ENDIF	
								PRINTLN("[BCWHEEL] Multiplayer has been selected for SWITCH! BUT SET_GAME_PAUSED(TRUE) ")
								SET_GAME_PAUSED(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					
					
				ENDWHILE	
				
//				CLEANUP_SELECTOR_UI()
				SET_GAME_PAUSED(FALSE)
				bBringUpSignInUI = FALSE
				bShownPSPlusSignIn = FALSE 
				RUN_TRANSITION_SFX(FALSE)
			ENDIF
			
			

			IF bUISelection = FALSE
				
				IF IS_SCREEN_FADED_OUT()
					SET_NO_LOADING_SCREEN(FALSE)
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						DO_SCREEN_FADE_IN(0)
						PRINTLN("[BCWHEEL] DO_SCREEN_FADE_IN(0) 2")
					ELSE
						IF SHOULD_FADE_IN_SCREEN_WITH_MESSAGE()
							DO_SCREEN_FADE_IN_FOR_TRANSITION(0)
							PRINTLN("[BCWHEEL] DO_SCREEN_FADE_IN(0) 2")
						ENDIF	
					ENDIF		
				ENDIF

				INT iPlayerNum = NATIVE_TO_INT(PLAYER_ID())
				IF iPlayerNum > -1
					//GlobalplayerBD_FM[iPlayerNum].bIsOnWarningScreen = FALSE
					CLEAR_BIT(GlobalplayerBD_FM[iPlayerNum].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bIsOnWarningScreen)
					NET_NL()NET_PRINT("BC: GlobalPlayerBroadcastDataFM_BS_bIsOnWarningScreen) = FALSE ")
				ENDIF

				CLEANUP_SELECTOR_UI()
				CLEANUP_MP_HEADSHOT()
				
				SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
				bShownPSPlusSignIn = FALSE 
				g_Private_SwitchedSP_DuringActivity = FALSE
				SET_SKYFREEZE_CLEAR(TRUE)
				NET_NL()NET_PRINT("BC: bUISelection = FALSE: Run_warning_Screen = FALSE ")
				EXIT
			ENDIF
			
			IF IS_REPEAT_PLAY_ACTIVE()
				bShownPSPlusSignIn = FALSE 
				NET_NL()NET_PRINT("BR: Repeat play is active. Session about to restart. Exiting MP transition.")
				FORCE_CLEANUP(FORCE_CLEANUP_FLAG_SP_TO_MP)
				SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
				EXIT
			ENDIF
			
			NET_NL()NET_PRINT("BC: bUISelection = TRUE: Past Confirm Screen ")
			
			SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
			
//			DRAW_FAKE_SWITCH()
			
//			PLAY_MP_ENTRY_POSTFX()
			HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			ENDIF
			
		
			IF HAS_MULTIPLAYER_BEEN_SELECTED(sSelectorPeds)
			AND  IS_PAUSE_MENU_REQUESTING_TRANSITION() = FALSE
				g_Private_MP_Picked_from_selector = TRUE
			ELSE
				g_Private_MP_Picked_from_selector = FALSE
			ENDIF
			
			
			NET_NL()NET_PRINT("[SELECTOR_CAM] g_Private_MP_Picked_from_selector = ")NET_PRINT_BOOL(g_Private_MP_Picked_from_selector)	
			NET_NL()NET_PRINT("[SELECTOR_CAM] g_Private_SwitchedSP_DuringActivity = ")NET_PRINT_BOOL(g_Private_SwitchedSP_DuringActivity)	
			NET_NL()NET_PRINT("[SELECTOR_CAM] g_bRunMultiplayerOnStartup = ")NET_PRINT_BOOL(g_bRunMultiplayerOnStartup)	
			NET_NL()NET_PRINT("[SELECTOR_CAM] LOBBY_AUTO_MULTIPLAYER_FREEMODE() = ")NET_PRINT_BOOL(LOBBY_AUTO_MULTIPLAYER_FREEMODE())	
			NET_NL()NET_PRINT("[SELECTOR_CAM] bMPAutoBootFromStartup = ")NET_PRINT_BOOL(bMPAutoBootFromStartup)	

			
			IF (g_Private_MP_Picked_from_selector
			OR g_Private_SwitchedSP_DuringActivity)
			AND NOT g_bRunMultiplayerOnStartup
			AND NOT LOBBY_AUTO_MULTIPLAYER_FREEMODE()
			AND NOT bMPAutoBootFromStartup
						

				SET_SP_SELECTOR_SKYCAM_UP(TRUE)
			ENDIF
			
			
			IF bMPAutoBootFromStartup
				IF IS_SCREEN_FADED_IN()
					CPRINTLN(DEBUG_INIT, "<LOAD-CHOICE> Unflagging MPAutoBoot flag after screen has faded back in.")
					#IF USE_FINAL_PRINTS 
						PRINTLN_FINAL("<2140379> <LOAD-CHOICE> Unflagging MPAutoBoot flag after screen has faded back in.")
					#ENDIF
					
					bMPAutoBootFromStartup = FALSE
				ENDIF
			ENDIF
			
			
			IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			AND g_eRunningMission = SP_MISSION_NONE
			AND IS_SP_SELECTOR_SKYCAM_UP() = FALSE
			AND IS_MP_PAUSE_MENU_SKYCAM_UP() = FALSE
			AND IS_MP_SWAP_CHARACTERS_SKYCAM_UP() = FALSE
			
			
			
				//Don't do the quick skycam as the property splits the gamecamera and player position often, causing crashes Bug 1689967
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) = 0 
				AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
				AND IS_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP() = FALSE
				#IF FEATURE_FREEMODE_ARCADE
				#IF FEATURE_COPS_N_CROOKS
				AND (GET_CURRENT_TRANSITION_FM_MENU_CHOICE() != FM_MENU_CHOICE_GOTO_CNC
				OR GET_ARCADE_MODE_ENTRY_POINT() != ARCADE_ENTRY_POINT_PHONE)
				#ENDIF
				#ENDIF
					SET_SP_PAUSE_MENU_SKYCAM_UP(TRUE)
				ENDIF
				
				IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
					SET_SINGLEPLAYER_END_NOW()
				ENDIF

			ENDIF
			
			IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
			AND g_eRunningMission != SP_MISSION_NONE
			AND IS_SP_SELECTOR_SKYCAM_UP() = FALSE
			AND IS_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP() = FALSE
			
				NET_NL()NET_PRINT("SELECTOR: ENTERING MP FROM A MISSION - SET_SKYFREEZE_FROZEN")
				SET_SKYFREEZE_FROZEN()	
				SET_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP(TRUE)
				
				SET_SINGLEPLAYER_END_NOW()
			ENDIF
			
				
	
			g_Private_SwitchedSP_DuringActivity = FALSE
			
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				// REMOVE ANY WEAPONS THAT MISSION SCRIPTS HAVE GIVEN THE PLAYER #1619373
				INT iWeaponSlot
				WEAPON_SLOT eWeaponSlot
				WEAPON_TYPE eWeaponInSlot
				enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					FOR iWeaponSlot = 0 TO NUM_PLAYER_PED_WEAPON_SLOTS -1
						eWeaponSlot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot)
						IF eWeaponSlot != WEAPONSLOT_INVALID
						AND eWeaponSlot != WEAPONSLOT_UNARMED
							eWeaponInSlot = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), eWeaponSlot)
							IF eWeaponInSlot != WEAPONTYPE_INVALID
							AND eWeaponInSlot != WEAPONTYPE_UNARMED
								IF NOT IS_PLAYER_PED_WEAPON_UNLOCKED(eCurrentPed, eWeaponInSlot)
									// player has this weapon, remove it
									SET_PED_AMMO(PLAYER_PED_ID(), eWeaponInSlot, 0)
									REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), eWeaponInSlot)
									PRINTLN("Removing weapon from SP player that is locked -- ", GET_WEAPON_NAME(eWeaponInSlot))
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				STORE_PLAYER_PED_INFO(PLAYER_PED_ID())
				STORE_Clock_For_Multiplayer_Switch()	//#796096
				
				// Cleanup random events
				FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
			ENDIF

			IF HAS_ANY_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds)
				SET_SOMETHING_QUITTING_MP(TRUE)
			ENDIF
			
			// Prepare states
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
//			HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
			
			DISABLE_CELLPHONE(TRUE)
			SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
//			CLEANUP_SELECTOR_UI()
//			CLEANUP_MP_HEADSHOT()
			
			//SCRIPT_ASSERT("KILLING FACE TO FACE CONVERSATION")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			KILL_ANY_CONVERSATION()
//			SET_MP_HUD_ON_SCREEN(TRUE)
			DISPLAYING_SCRIPT_HUD(HUDPART_TRANSITIONHUD, TRUE)
			RESET_NET_TIMER(TransitionFailsafe_Timer)
			
			IF BUSYSPINNER_IS_ON()
				BUSYSPINNER_OFF()
			ENDIF

//			PRINTLN("About to request and launch MP_PROP_GLOBAL_BLOCK.sc ")
//			REQUEST_SCRIPT("MP_PROP_GLOBAL_BLOCK")
//		    WHILE NOT HAS_SCRIPT_LOADED("MP_PROP_GLOBAL_BLOCK")
//		        WAIT(0)
//		        REQUEST_SCRIPT("MP_PROP_GLOBAL_BLOCK")
//		    ENDWHILE
//		    START_NEW_SCRIPT("MP_PROP_GLOBAL_BLOCK",DEFAULT_STACK_SIZE)
//			
//			PRINTLN("Give tunables_registration one frame to run before requesting and launching main_persistent.")
//			
//			WAIT(0)
			
//			HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
			
		// Launch the multiplayer hud
			REQUEST_SCRIPT("MainTransition")
			WHILE NOT HAS_SCRIPT_LOADED("MainTransition")
			AND bUISelection
				PRINTSTRING("loading script MainTransition")
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
//				HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
//				DRAW_FAKE_SWITCH()
				PRINTNL()
				
//				IS_QUICK_CAMERA_QUIT_FINISHED()
				
//				IF (GET_THE_NETWORK_TIMER() - TransitionFailsafe_Timer) > 20000
				IF HAS_NET_TIMER_EXPIRED(TransitionFailsafe_Timer, 20000)
					FAILSAFE_PLAYER_RESPAWN()
					bUISelection = FALSE
					EXIT
				ENDIF
				
				#IF USE_REPLAY_RECORDING_TRIGGERS
					CLEANUP_REPLAY_RECORD_FEED()
				#ENDIF
				
				WAIT(0)
			ENDWHILE
			
			#IF USE_REPLAY_RECORDING_TRIGGERS
				CLEANUP_REPLAY_RECORD_FEED()
			#ENDIF
			CLEANUP_SELECTOR_UI()
			CLEANUP_MP_HEADSHOT()
			
			IF NOT IS_EMERGENCY_SKYCAM_UP_RUNNING()
				IF IS_SCREEN_FADED_OUT()
					SET_NO_LOADING_SCREEN(FALSE)
					SET_AUDIO_FLAG("AllowRadioOverScreenFade", FALSE)
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						DO_SCREEN_FADE_IN(0)
						PRINTLN("[BCWHEEL] DO_SCREEN_FADE_IN(0) 3")
					ELSE
						IF SHOULD_FADE_IN_SCREEN_WITH_MESSAGE()
							DO_SCREEN_FADE_IN_FOR_TRANSITION(0)
							PRINTLN("[BCWHEEL] DO_SCREEN_FADE_IN(0) 3")
						ENDIF	
					ENDIF
					
				ENDIF
			ENDIF
//			
			IF g_sMPTunables.bDisablePVDuplicateFix = FALSE
			AND HAS_DEFAULT_INFO_BEEN_SET() // Only process if we have initialised SP data.
				WHILE NOT IS_CHARACTER_MODEL_CHECK_DONE()

					PRINTLN("BC: Selector is waiting for g_bCharacterModelCheckDone to be TRUE. ")
					#IF USE_FINAL_PRINTS 
						PRINTLN_FINAL("<2140379> Selector is waiting for g_bCharacterModelCheckDone to be TRUE.")
					#ENDIF
					
					MAINTAIN_SP_CHARACTER_MODEL_CHECK()
					WAIT(0)
				ENDWHILE
			ELSE
				PRINTLN("MAINTAIN_SP_CHARACTER_MODEL_CHECK - Skipping char model checks. bDisablePVDuplicateFix = ", g_sMPTunables.bDisablePVDuplicateFix, ", HAS_DEFAULT_INFO_BEEN_SET = ", HAS_DEFAULT_INFO_BEEN_SET())
				#IF USE_FINAL_PRINTS 
					PRINTLN_FINAL("<2140379> Skipping char model checks. bDisablePVDuplicateFix = ", g_sMPTunables.bDisablePVDuplicateFix, ", HAS_DEFAULT_INFO_BEEN_SET = ", HAS_DEFAULT_INFO_BEEN_SET())
				#ENDIF
				
				SET_CHARACTER_MODEL_CHECK_DONE(TRUE)
			ENDIF
			
			PRINTLN("About to start the MainTransition script when a TRANSITION_STACK_SIZE stack is free")
			WHILE GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(TRANSITION_STACK_SIZE) = 0
				WAIT(0)
			ENDWHILE
			PRINTLN("MainTransition script ready to start")
			
			g_tiTransitionThread = START_NEW_SCRIPT("MainTransition", TRANSITION_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED("MainTransition")
			
			// disable prospero activity script routing
			#IF FEATURE_GEN9_STANDALONE
			SET_ACTIVITY_SCRIPT_ROUTING_ENABLED(FALSE)			
			#ENDIF
			
			g_b_isSelectorAbouttoBootMaintransition = FALSE
			bMPAutoBootFromStartup = FALSE
			
			// Wait for the player to make their decision on the multiplayer hud


			
			WHILE IS_THREAD_ACTIVE(g_tiTransitionThread) AND (GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
				
				#IF IS_DEBUG_BUILD
					UPDATE_SKYSWOOP_WIDGETS()
				#ENDIF
				
				WAIT(0)
			ENDWHILE
			
			
			// Wait for the transition to complete
			WHILE IS_SKYSWOOP_MOVING()
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
//				HIDE_HUD_FOR_TRANSITION_THIS_FRAME()
				
				#IF IS_DEBUG_BUILD
					UPDATE_SKYSWOOP_WIDGETS()
				#ENDIF
				
				WAIT(0)
			ENDWHILE
			
	
			// Done
			g_IsSomethingElseExittingMP = FALSE
			SET_SOMETHING_QUITTING_MP(FALSE)
			DISPLAYING_SCRIPT_HUD(HUDPART_TRANSITIONHUD, FALSE)
//			SET_MP_HUD_ON_SCREEN(FALSE)
			DISABLE_CELLPHONE(FALSE)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				PRINTLN("[SWITCH MP/SP] Still in MP so clearing ambient switch request...")
				CANCEL_PLAYER_PED_SWITCH_REQUEST()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////
///    Main procedures and functions
///    

// PURPOSE: Creates any assets required for the script
PROC DO_INITIALISE()

	g_sSelectorUI.iSFX_Display = -1
	g_sSelectorUI.iSFX_HeadDown = -1
	g_sSelectorUI.iSFX_Sky = -1
	
	iIndicatorSFX[0] = -1
	iIndicatorSFX[1] = -1
	
	fLastTimeScaleSet = 1.0
	fTargetScale = 1.0
	bSlowingDownTime = FALSE
	
	sSelectorPeds.bAmbient = TRUE
	
	MAINTAIN_SELECTOR_RUNTIME_STATES()
	
	// Progress to the next stage of the selector
	eSelectorStage = SELECTOR_CONTROLLER	
ENDPROC

// PURPOSE: Handles player input and updates system accordingly
PROC DO_CONTROLLER()

	// Update UI and selection states
	MAINTAIN_AMBIENT_SELECTOR()
	
	// Update Scaleform movie details
	MAINTAIN_SELECTOR_UI()
	
	// Reset all the flags back to ambient setup
	MAINTAIN_SELECTOR_RUNTIME_STATES()
ENDPROC

// PURPOSE: Cleans up any resources the script has created and then terminates the script
PROC SCRIPT_CLEANUP()
	CLEANUP_SELECTOR_UI()
	CLEANUP_MP_HEADSHOT()
	
	//Purposefully "hidden" in amongst this script. We want it to be hard
	//for hackers to spot these scripts in decompiled output. -BenR
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
		Cleanup_Beast_Unlock(sBeastUnlockVars)
	#ENDIF
	#ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	PROC Maintain_transition_test()
		IF g_binitiatejoinleavetest = TRUE
			PRINTSTRING("/n  automated test 1 ")
			// Transition to Freemode from Single Player
			//IF IS_CNC_RUNNING()
		//	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			//IF NOT IS_TRANSITION_MENU_TRIGGERED()
			//IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			AND NOT IS_PLAYER_DEAD(PLAYER_ID())
			AND NOT IS_TRANSITION_RUNNING()
			
				IF IS_PLAYER_PLAYING(PLAYER_ID())
				AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND CAN_PLAYER_START_MISSION(PLAYER_ID())
					PRINTSTRING("/n  automated test 2 ")
					MP_GAMEMODE gamemode = GET_CURRENT_GAMEMODE()
					//DO_INITIALISE()
					SWITCH gamemode
						
						CASE GAMEMODE_SP
						//AND IS_PLAYER_PLAYING(PLAYER_ID())
							PRINTSTRING("/n Switch to Freemode with automated test ")
							/*g_qlWhichTeam = QUICKLAUNCH_FREEMODE
							g_HaveIShiftedIntoTheGame = TRUE
							IF NOT (IS_TRANSITION_MENU_TRIGGERED())
								TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
							ENDIF*/
							SET_QUICKLAUNCH_STATE(QUICKLAUNCH_FREEMODE)
						BREAK
						
						CASE GAMEMODE_FM
						//AND IS_PLAYER_PLAYING(PLAYER_ID())
							PRINTSTRING("/n Switch to CNC Mexican gang automated test")
							/*g_qlWhichTeam = QUICKLAUNCH_CNC_VAGOS
							g_HaveIShiftedIntoTheGame = TRUE
				//			SET_MP_HUD_ON_SCREEN(TRUE)
							IF NOT (IS_TRANSITION_MENU_TRIGGERED())
								TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
							ENDIF*/
							SET_QUICKLAUNCH_STATE(QUICKLAUNCH_SP)
						BREAK
						
//						CASE GAMEMODE_CnC
//						//AND IS_PLAYER_PLAYING(PLAYER_ID())
//							PRINTSTRING("/n Switch to Single Player with automated test")
//							/*g_iLeaveMultiplayerState = LEAVE_MP_STATE_YES
//							g_HaveIShiftedIntoTheGame = TRUE
//							g_TransitionCameraState = LEAVEMPHUD_MPTOSP
//							GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
//				//			SET_MP_HUD_ON_SCREEN(TRUE)
//							IF NOT (IS_TRANSITION_MENU_TRIGGERED())
//								TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
//							ENDIF*/
//							SET_QUICKLAUNCH_STATE(QUICKLAUNCH_SP)
//						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
#ENDIF


SCRIPT

	#IF IS_DEBUG_BUILD
		INITIALISE_SELECTOR_WIDGETS()
	#ENDIF
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
		Reset_Beast_Unlock_Variables(sBeastUnlockVars)
		#IF IS_DEBUG_BUILD
			Create_Beast_Unlock_Widgets(sBeastUnlockVars, widgetID)
		#ENDIF
	#ENDIF
	#ENDIF
	
	//0 is valid value, so default to -1 before the Selector listeners process. BC: 31/07/2014
	RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()
	
	// The main mission loop 
	WHILE TRUE
	
		#IF FEATURE_SP_DLC_BEAST_SECRET
		#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
			Maintain_Beast_Unlock(sBeastUnlockVars)
		#ENDIF
		#ENDIF
	
		SWITCH eSelectorStage
			CASE SELECTOR_INITIALISE
				DO_INITIALISE()
			BREAK
			
			CASE SELECTOR_CONTROLLER
				DO_CONTROLLER()
			BREAK
			
			CASE SELECTOR_END
				SCRIPT_CLEANUP()
			BREAK	
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			UPDATE_SELECTOR_WIDGETS()
			UPDATE_SKYSWOOP_WIDGETS()
			
			#IF FEATURE_SP_DLC_BEAST_SECRET
			#IF NOT FEATURE_SP_DLC_BEAST_SECRET_DEBUG
				Update_Beast_Unlock_Widgets(sBeastUnlockVars)
			#ENDIF
			#ENDIF
			
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				Maintain_transition_test()
			ENDIF
		#ENDIF
		
		
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT
