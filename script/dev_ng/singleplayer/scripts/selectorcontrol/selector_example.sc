//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	selector_example.sc											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Test script so we can test play around with the various		//
//							hotswap features.											//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


// Do nothing in release mode
#IF IS_FINAL_BUILD
SCRIPT
ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_graphics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "script_MATHS.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"

ENUM SELECTOR_STAGE_ENUM
	SELECTOR_INITIALISE = 0,
	SELECTOR_CONTROLLER,
	SELECTOR_END
ENDENUM
SELECTOR_STAGE_ENUM eSelectorStage = SELECTOR_INITIALISE

SELECTOR_PED_STRUCT sSelectorPeds //Struct that will store all the peds for hot-swap
SELECTOR_CAM_STRUCT sCamDetails
VEHICLE_INDEX 		vehIndex[2]

REL_GROUP_HASH relGroup
BOOL bRelGroupSetup
INT iCamType
BOOL bKeepTasks = FALSE

//BLIP_INDEX blipPeds[NUMBER_OF_SELECTOR_PEDS]



	BOOL bPedBlocked[NUMBER_OF_SELECTOR_PEDS]
	BOOL bPedHinted[NUMBER_OF_SELECTOR_PEDS]
	BOOL bPedDamaged[NUMBER_OF_SELECTOR_PEDS]
	INT iFakeState[NUMBER_OF_SELECTOR_PEDS]
	BOOL bReGroup, bRunwayTest
	BOOL bGiveTrevorWeapon
	BOOL bSetPhaseOnce, bSetPhase
	FLOAT fPhase
	
	PROC SETUP_DEBUG_WIDGETS()
		START_WIDGET_GROUP("Selector Example Debug")
			ADD_WIDGET_BOOL("Peds keep tasks", bKeepTasks)
			
			ADD_WIDGET_BOOL("Set cam phase", bSetPhase)
			ADD_WIDGET_BOOL("Set cam phase once", bSetPhaseOnce)
			ADD_WIDGET_FLOAT_SLIDER("Cam phase", fPhase, 0.0, 1.0, 0.01)
		
			ADD_WIDGET_BOOL("Michael Blocked", bPedBlocked[SELECTOR_PED_MICHAEL])
			ADD_WIDGET_BOOL("Trevor Blocked", bPedBlocked[SELECTOR_PED_TREVOR])
			ADD_WIDGET_BOOL("Franklin Blocked", bPedBlocked[SELECTOR_PED_FRANKLIN])
			
			ADD_WIDGET_BOOL("Michael Hinted", bPedHinted[SELECTOR_PED_MICHAEL])
			ADD_WIDGET_BOOL("Trevor Hinted", bPedHinted[SELECTOR_PED_TREVOR])
			ADD_WIDGET_BOOL("Franklin Hinted", bPedHinted[SELECTOR_PED_FRANKLIN])
			
			ADD_WIDGET_BOOL("Michael Damaged", bPedDamaged[SELECTOR_PED_MICHAEL])
			ADD_WIDGET_BOOL("Trevor Damaged", bPedDamaged[SELECTOR_PED_TREVOR])
			ADD_WIDGET_BOOL("Franklin Damaged", bPedDamaged[SELECTOR_PED_FRANKLIN])
			
			ADD_WIDGET_INT_SLIDER("Michael state", iFakeState[SELECTOR_PED_MICHAEL], 0, 3, 1)
			ADD_WIDGET_INT_SLIDER("Trevor state", iFakeState[SELECTOR_PED_TREVOR], 0, 3, 1)
			ADD_WIDGET_INT_SLIDER("Franklin state", iFakeState[SELECTOR_PED_FRANKLIN], 0, 3, 1)
			
			ADD_WIDGET_INT_SLIDER("Cam type", iCamType, 0, 3, 1)
			
			ADD_WIDGET_BOOL("Regroup", bReGroup)
			ADD_WIDGET_BOOL("Runway Test", bRunwayTest)
			
			ADD_WIDGET_BOOL("Give weapon to Trevor", bGiveTrevorWeapon)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC MAINTAIN_DEBUG_WIDGETS()
	
		IF bGiveTrevorWeapon
			GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, -1, TRUE, TRUE)
			bGiveTrevorWeapon = FALSE
		ENDIF
	
		IF bSetPhase OR bSetPhaseOnce
			SET_SELECTOR_CAM_PHASE(sCamDetails, fPhase)
			bSetPhaseOnce = FALSE
		ENDIF
	
		IF sSelectorPeds.bBlockSelectorPed[SELECTOR_PED_MICHAEL] <> bPedBlocked[SELECTOR_PED_MICHAEL]
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, bPedBlocked[SELECTOR_PED_MICHAEL])
		ENDIF
		IF sSelectorPeds.bBlockSelectorPed[SELECTOR_PED_TREVOR] <> bPedBlocked[SELECTOR_PED_TREVOR]
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, bPedBlocked[SELECTOR_PED_TREVOR])
		ENDIF
		IF sSelectorPeds.bBlockSelectorPed[SELECTOR_PED_FRANKLIN] <> bPedBlocked[SELECTOR_PED_FRANKLIN]
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_FRANKLIN, bPedBlocked[SELECTOR_PED_FRANKLIN])
		ENDIF
		
		IF sSelectorPeds.bDisplayHint[SELECTOR_PED_MICHAEL] <> bPedHinted[SELECTOR_PED_MICHAEL]
			SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, bPedHinted[SELECTOR_PED_MICHAEL])
		ENDIF
		IF sSelectorPeds.bDisplayHint[SELECTOR_PED_TREVOR] <> bPedHinted[SELECTOR_PED_TREVOR]
			SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, bPedHinted[SELECTOR_PED_TREVOR])
		ENDIF
		IF sSelectorPeds.bDisplayHint[SELECTOR_PED_FRANKLIN] <> bPedHinted[SELECTOR_PED_FRANKLIN]
			SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, bPedHinted[SELECTOR_PED_FRANKLIN])
		ENDIF
		
		IF sSelectorPeds.bDisplayDamage[SELECTOR_PED_MICHAEL] <> bPedDamaged[SELECTOR_PED_MICHAEL]
			SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_MICHAEL, bPedDamaged[SELECTOR_PED_MICHAEL])
		ENDIF
		IF sSelectorPeds.bDisplayDamage[SELECTOR_PED_TREVOR] <> bPedDamaged[SELECTOR_PED_TREVOR]
			SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_TREVOR, bPedDamaged[SELECTOR_PED_TREVOR])
		ENDIF
		IF sSelectorPeds.bDisplayDamage[SELECTOR_PED_FRANKLIN] <> bPedDamaged[SELECTOR_PED_FRANKLIN]
			SET_SELECTOR_PED_DAMAGED(sSelectorPeds, SELECTOR_PED_FRANKLIN, bPedDamaged[SELECTOR_PED_FRANKLIN])
		ENDIF
		
		IF sSelectorPeds.eFakeState[SELECTOR_PED_MICHAEL] <> INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_MICHAEL])
			SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, SELECTOR_PED_MICHAEL, INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_MICHAEL]))
		ENDIF
		IF sSelectorPeds.eFakeState[SELECTOR_PED_TREVOR] <> INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_TREVOR])
			SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, SELECTOR_PED_TREVOR, INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_TREVOR]))
		ENDIF
		IF sSelectorPeds.eFakeState[SELECTOR_PED_FRANKLIN] <> INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_FRANKLIN])
			SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, SELECTOR_PED_FRANKLIN, INT_TO_ENUM(SELECTOR_STATE_ENUM, iFakeState[SELECTOR_PED_FRANKLIN]))
		ENDIF
		
		IF bReGroup
		
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], << -1127.3188, -1989.2003, 12.7621 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 180.6440)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], << -1131.0607, -1991.5133, 12.7556 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 10.3631)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], << -1136.2836, -1981.2650, 12.7434 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 288.4871)
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1133.4170, -1984.3705, 12.7448 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 152.3949)
			ENDIF
		
			bReGroup = FALSE
		ENDIF
		
		IF bRunwayTest
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], << -1349.2848, -2234.4182, 13.9499 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 139.5694)
			ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], << -1349.2848, -2234.4182, 13.9499 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 139.5694)
			ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], << -1349.2848, -2234.4182, 13.9499 >>)
				SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 139.5694)
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1770.0112, -2954.1919, 13.9494 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 327.4279)
			ENDIF
			bRunwayTest = FALSE
		ENDIF
	ENDPROC


//////////////////////////////////////////////////////////////////////////////////////////
///    Procedures and functions
///    


// PURPOSE: Creates any assets required for the script
PROC DO_INITIALISE()

	// Set the current player to Michael
	WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
		WAIT(0)
	ENDWHILE
	
	// Set up the relationships groups
	ADD_RELATIONSHIP_GROUP("SELECTOR", relGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroup, RELGROUPHASH_PLAYER)
	bRelGroupSetup = TRUE
	
	// Create Franklin
	WHILE NOT CREATE_PLAYER_VEHICLE(vehIndex[0], CHAR_FRANKLIN, << -1280.5129, -984.3002, 9.0555 >>, 193.7342, TRUE, VEHICLE_TYPE_CAR)
		WAIT(0)
	ENDWHILE
	WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, vehIndex[0])
		WAIT(0)
	ENDWHILE
	//blipPeds[SELECTOR_PED_FRANKLIN] = ADD_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	//SET_BLIP_COLOUR(blipPeds[SELECTOR_PED_FRANKLIN], BLIP_COLOUR_GREEN)
	//SET_BLIP_SCALE(blipPeds[SELECTOR_PED_FRANKLIN], BLIP_SIZE_PED)
	//SET_BLIP_AS_FRIENDLY(blipPeds[SELECTOR_PED_FRANKLIN], TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], relGroup)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
	
	SET_PED_MAX_HEALTH_WITH_SCALE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 2000)
	PRINTLN("Franklin health = ", GET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
	
	
	// Create Trevor
	WHILE NOT CREATE_PLAYER_VEHICLE(vehIndex[1], CHAR_TREVOR, << -1636.8440, -609.5120, 32.0636 >>, 232.3183, TRUE, VEHICLE_TYPE_CAR)
		WAIT(0)
	ENDWHILE
	WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vehIndex[1])
		WAIT(0)
	ENDWHILE
	//blipPeds[SELECTOR_PED_MICHAEL] = ADD_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	//SET_BLIP_COLOUR(blipPeds[SELECTOR_PED_TREVOR], BLIP_COLOUR_GREEN)
	//SET_BLIP_SCALE(blipPeds[SELECTOR_PED_TREVOR], BLIP_SIZE_PED)
	//SET_BLIP_AS_FRIENDLY(blipPeds[SELECTOR_PED_TREVOR], TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], relGroup)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
	
	SET_PED_MAX_HEALTH_WITH_SCALE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 1800)
	PRINTLN("Trevor health = ", GET_ENTITY_HEALTH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
	
	SET_SELECTOR_PED_ACTIVITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_ACTIVITY_SNIPER)
	
	// Progress to the next stage of the selector
	eSelectorStage = SELECTOR_CONTROLLER
	
	
		bReGroup = TRUE
	
ENDPROC

// PURPOSE: Handles player input and updates system accordingly
PROC DO_CONTROLLER()

	// Something along the lines of what should be used in mission scripts
	IF NOT sCamDetails.bRun
		IF UPDATE_SELECTOR_HUD(sSelectorPeds)     // Returns TRUE when the player has made a selection
			IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
				sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
				sCamDetails.bRun  = TRUE
			ENDIF
		ENDIF
	ELSE
		IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 0.0,0.0, INT_TO_ENUM(SELECTOR_CAM_INTERP_TYPE, iCamType))	// Returns FALSE when the camera spline is complete
			IF sCamDetails.bOKToSwitchPed
				IF NOT sCamDetails.bPedSwitched
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, bKeepTasks)
						// Update the relationship groups
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], relGroup)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)	
						ENDIF
						sCamDetails.bPedSwitched = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Update blips
	/*INT i
	FOR i = 0 TO ENUM_TO_INT(NUMBER_OF_SELECTOR_PEDS)-1
		SELECTOR_SLOTS_ENUM eSlot = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, i)
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[eSlot])
			IF NOT DOES_BLIP_EXIST(blipPeds[eSlot])
				blipPeds[eSlot] = ADD_BLIP_FOR_ENTITY(sSelectorPeds.pedID[eSlot])
				SET_BLIP_COLOUR(blipPeds[eSlot], BLIP_COLOUR_GREEN)
				SET_BLIP_SCALE(blipPeds[eSlot], BLIP_SIZE_PED)
				SET_BLIP_AS_FRIENDLY(blipPeds[eSlot], TRUE)
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipPeds[eSlot])
				REMOVE_BLIP(blipPeds[eSlot])
			ENDIF
		ENDIF
	ENDFOR*/

	// Progress to the end stage of the selector
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			eSelectorStage = SELECTOR_END
		ENDIF
	
	
ENDPROC

// PURPOSE: Cleans up any resources the script has created and then terminates the script
PROC MISSION_CLEANUP()
	
	// Cleanup the relationships
	IF bRelGroupSetup
		REMOVE_RELATIONSHIP_GROUP(relGroup)
	ENDIF
	
	// Cleanup the peds
	INT iCount
	FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SELECTOR_PEDS)-1
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[iCount])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[iCount])
				SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	// Cleanup the vehicles
	FOR iCount = 0 TO 1
		IF DOES_ENTITY_EXIST(vehIndex[iCount])
			IF IS_VEHICLE_DRIVEABLE(vehIndex[iCount])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehIndex[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	// Cleanup blips
	/*FOR iCount = 0 TO ENUM_TO_INT(NUMBER_OF_SELECTOR_PEDS)-1
		IF DOES_BLIP_EXIST(blipPeds[iCount])
			REMOVE_BLIP(blipPeds[iCount])
		ENDIF
	ENDFOR*/
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT
	
	IF HAS_FORCE_CLEANUP_OCCURRED()	
		MISSION_CLEANUP()   
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	
		SETUP_DEBUG_WIDGETS()
	

	// The main mission loop
	WHILE TRUE
	
		
			MAINTAIN_DEBUG_WIDGETS()
		
	
		SWITCH eSelectorStage
			CASE SELECTOR_INITIALISE
				DO_INITIALISE()
			BREAK
			
			CASE SELECTOR_CONTROLLER
				DO_CONTROLLER()
			BREAK
			
			CASE SELECTOR_END
				MISSION_CLEANUP()
			BREAK	
		ENDSWITCH	
		
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT

#ENDIF
