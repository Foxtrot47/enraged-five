// TaxiTimer Class
// Author : John R. Diaz
// Keeps track of all the different timers and their processing
USING "timer_public.sch"

FUNC STRING GET_TAXI_TIMER_NAME(TAXI_TIMERS aWhichTimer)
	STRING sTimerName
	
	SWITCH aWhichTimer
		
		CASE TT_CLEANUP
			sTimerName = "TT_CLEANUP"
		BREAK
		
		CASE TT_PASSENGER_ACTION
			sTimerName = "TT_PASSENGER_ACTION"
		BREAK
		
		CASE TT_DELAY
			sTimerName = "TT_DELAY"
		BREAK
		
		
		CASE TT_GENERIC
			sTimerName = "TT_GENERIC"
		BREAK
		
		CASE TT_IKW_DIRECTIONS
			sTimerName = "TT_IKW_DIRECTIONS"
		BREAK
		
		CASE TT_DROPOFF
			sTimerName = "TT_DROPOFF"
		BREAK
		
		CASE TT_STOP_STUCK
			sTimerName = "TT_STOP_STUCK"
		BREAK
		
		CASE TT_STOPPED
			sTimerName = "TT_STOPPED"
		BREAK
		
		CASE TT_POLICE
			sTimerName = "TT_POLICE"
		BREAK
		
		CASE TT_DQUEUE
			sTimerName = "TT_DQUEUE"
		BREAK
		
		CASE TT_RIDETODEST
			sTimerName = "TT_RIDETODEST"
		BREAK
		
		CASE TT_FAILDELAY
			sTimerName = "TT_FAILDELAY"
		BREAK
	
		CASE TT_FOLLOW
			sTimerName = "TT_FOLLOW"
		BREAK
		
		CASE TT_BORING
			sTimerName = "TT_BORING"
		BREAK
		
		CASE TT_BANTER
			sTimerName = "TT_BANTER"
		BREAK
		
		CASE TT_PENALTY
			sTimerName = "TT_PENALTY"
		BREAK
		
		CASE TT_COLLIDE
			sTimerName = "TT_COLLIDE"
		BREAK
		
		CASE TT_NOTINCAB
			sTimerName = "TT_NOTINCAB"
		BREAK
		
		CASE TT_DIALOGUE
			sTimerName = "TT_DIALOGUE"
		BREAK
		
		CASE TT_INTERUPT
			sTimerName = "TT_INTERUPT"
		BREAK
		
		CASE TT_CUTSCENE
			sTimerName = "TT_CUTSCENE"
		BREAK
		
		CASE TT_PUKING
			sTimerName = "TT_PUKING"
		BREAK
		
		CASE TT_RDSTADDTL
			sTimerName = "TT_RDSTADDTL"
		BREAK		
		
		CASE TT_TIP_CHECK
			sTimerName = "TT_TIP_CHECK"
		BREAK
		
		CASE TT_UNDER_WATER
			sTimerName = "TT_UNDER_WATER"
		BREAK
		
		CASE TT_CONTROL_CHECK
			sTimerName = "TT_CONTROL_CHECK"
		BREAK	
		
		CASE TT_FOLLOW_CAR_AI
			sTimerName = "TT_FOLLOW_CAR_AI"
		BREAK
		
		CASE TT_AGGRO
			sTimerName = "TT_AGGRO"
		BREAK
		
		CASE TT_REGION_FIND_POINT
			sTimerName = "TT_REGION_FIND_POINT"
		BREAK
		
		CASE TT_REGION_TIME_POINT
			sTimerName = "TT_REGION_TIME_POINT"
		BREAK
		
		CASE TT_REGION_FIND_FAIL
			sTimerName = "TT_REGION_FIND_FAIL"
		BREAK
		
		DEFAULT
			sTimerName = "INVALID TIMER"
		BREAK
	ENDSWITCH
	
	RETURN sTimerName
ENDFUNC

PROC TAXI_START_TIMER(TaxiStruct &myTaxiData, TAXI_TIMERS aWhichTimer)
	START_TIMER_NOW(myTaxiData.tTimers[aWhichTimer])
ENDPROC


FUNC BOOL IS_TAXI_TIMER_STARTED(TaxiStruct &myTaxiData, TAXI_TIMERS aWhichTimer) 
	RETURN IS_TIMER_STARTED(myTaxiData.tTimers[aWhichTimer])
ENDFUNC

PROC  TAXI_RESET_TIMERS(TaxiStruct &myTaxiData,TAXI_TIMERS aWhichTimer = TT_NUM_TIMERS, FLOAT fTimeToSkipTo = 0.0 ,BOOL bDebug = FALSE)
	INT i 
	IF aWhichTimer = TT_NUM_TIMERS
		REPEAT TT_NUM_TIMERS i 
			IF fTimeToSkipTo > 0
				RESTART_TIMER_AT(myTaxiData.tTimers[i],fTimeToSkipTo)
			ELSE
				RESTART_TIMER_NOW(myTaxiData.tTimers[i])
			ENDIF
		ENDREPEAT
	ELSE
		IF fTimeToSkipTo > 0
			RESTART_TIMER_AT(myTaxiData.tTimers[aWhichTimer],fTimeToSkipTo)
		ELSE
			RESTART_TIMER_NOW(myTaxiData.tTimers[aWhichTimer])
		ENDIF
	ENDIF
	
	IF bDebug
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_RESET_TIMERS reset ","",GET_TAXI_TIMER_NAME(aWhichTimer))
	ENDIF
ENDPROC

PROC TAXI_PAUSE_TIMER(TaxiStruct &myTaxiData, TAXI_TIMERS aWhichTimer = TT_NUM_TIMERS)
	INT i 
	IF aWhichTimer = TT_NUM_TIMERS
		REPEAT TT_NUM_TIMERS i 	
			IF NOT IS_TIMER_PAUSED(myTaxiData.tTimers[i])
				PAUSE_TIMER(myTaxiData.tTimers[i])
			ENDIF
		ENDREPEAT
	ELSE
		IF IS_TIMER_STARTED(myTaxiData.tTimers[aWhichTimer])
			IF NOT IS_TIMER_PAUSED(myTaxiData.tTimers[aWhichTimer])
				PAUSE_TIMER(myTaxiData.tTimers[aWhichTimer])
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC TAXI_UNPAUSE_TIMER(TaxiStruct &myTaxiData, TAXI_TIMERS aWhichTimer = TT_NUM_TIMERS)
	IF IS_TIMER_STARTED(myTaxiData.tTimers[aWhichTimer])
		IF IS_TIMER_PAUSED(myTaxiData.tTimers[aWhichTimer])
			UNPAUSE_TIMER(myTaxiData.tTimers[aWhichTimer])
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_TAXI_TIMER_IN_SECONDS(TaxiStruct &myTaxiData, TAXI_TIMERS whichTimer)

	IF NOT IS_TIMER_STARTED(myTaxiData.tTimers[whichTimer])
		START_TIMER_NOW(myTaxiData.tTimers[whichTimer])
	ENDIF

	RETURN GET_TIMER_IN_SECONDS(myTaxiData.tTimers[whichTimer])
	
ENDFUNC

//Wrapper for CANCEL_TIMER on a taxi timer.
PROC  TAXI_CANCEL_TIMERS(TaxiStruct &myTaxiData, TAXI_TIMERS aWhichTimer = TT_NUM_TIMERS , BOOL bDebug= FALSE)
	//This is valuable tuning debug info to see how long QA is hanging out outside of the cab causing mayhem.
	IF aWhichTimer = TT_NOTINCAB
		CDEBUG1LN(DEBUG_OJ_TAXI,"[ooooooo TIMERS ooooooooo] Player Was Out Of Cab for ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, aWhichTimer))
	ENDIF
	
	INT i 
	IF aWhichTimer = TT_NUM_TIMERS
		REPEAT TT_NUM_TIMERS i 
			CANCEL_TIMER(myTaxiData.tTimers[i])
		ENDREPEAT
	ELSE
		CANCEL_TIMER(myTaxiData.tTimers[aWhichTimer])
	ENDIF
	
	IF bDebug
		CDEBUG1LN(DEBUG_OJ_TAXI,"[ooooooo TIMERS ooooooooo] TAXI_CANCEL_TIMERS set ", GET_TAXI_TIMER_NAME(aWhichTimer))
	ENDIF
ENDPROC

FUNC BOOL IS_TAXI_EXPIRED(TaxiStruct &myTaxiData, FLOAT fTimeToWait, TAXI_TIMERS whichTimer = TT_DROPOFF)
	RETURN TIMER_DO_ONCE_WHEN_READY(myTaxiData.tTimers[whichTimer],fTimeToWait)
ENDFUNC


FUNC BOOL TAXI_TIMER_DO_WHEN_READY(TaxiStruct &myTaxiData, TAXI_TIMERS whichTimer, FLOAT fTimeToWait = 1.0)
	IF NOT IS_TIMER_STARTED(myTaxiData.tTimers[whichTimer])
		START_TIMER_NOW(myTaxiData.tTimers[whichTimer])
	ENDIF
	
	RETURN(TIMER_DO_WHEN_READY(myTaxiData.tTimers[whichTimer],fTimeToWait))
ENDFUNC

//EOF
