// launcher_Taxi_lib.sch
//USING "controller_Taxi.sch"
USING "minigames_helpers.sch"

/// PURPOSE: Adding this because there is supposed to be a new taxi model coming down the pipe and this will make it a one line update as opposed to multiple
///    
/// RETURNS: The MODEL_NAME of the taxi model we should be using for the oddjob
///    
FUNC MODEL_NAMES TAXI_GET_TAXI_MODEL_NAME()
	RETURN TAXI
	//old green prius taxi was called TAXI2, may it rest in peace :)
ENDFUNC
/// PURPOSE: Used to keep track of how many times an oddjob has been attempted. MOSTLY USED FOR DIALOGUE PURPOSES
///    
/// PARAMS:
///    txMissionType - The Enum for the which mission to increment
PROC TAXI_CONTROLLER_UpdateNumRuns(TAXI_MISSION_TYPES txMissionType)
	g_savedGlobals.sTaxiData.iTaxiOJ_NumRuns[txMissionType]++
	CDEBUG1LN(DEBUG_OJ_TAXI,"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% This taxi oddjob has been run #:			",g_savedGlobals.sTaxiData.iTaxiOJ_NumRuns[txMissionType] ," 			times.%%%%%%%%%%%%%%%%%%%%%")
ENDPROC

FUNC INT TAXI_CONTROLLER_GetNumRuns(TAXI_MISSION_TYPES txMissionType)
	RETURN	g_savedGlobals.sTaxiData.iTaxiOJ_NumRuns[txMissionType]
ENDFUNC


FUNC TAXI_MISSION_TYPES TAXI_CONTROLLER_GetRandomMissionType(TAXI_MISSION_TYPES lastMissionType)
	INT iRandType
	
	// Pick anything that's not a dupe.
	iRandType = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(TXM_00_INVALID) + 1, ENUM_TO_INT(TXM_NUM_TYPES))
	WHILE (iRandType = ENUM_TO_INT(lastMissionType))
		iRandType = GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(TXM_00_INVALID) + 1, ENUM_TO_INT(TXM_NUM_TYPES))
	ENDWHILE

	RETURN INT_TO_ENUM(TAXI_MISSION_TYPES, iRandType)
ENDFUNC


FUNC STRING TAXI_CONTROLLER_GET_OFFER_OR_RESPONSE_LABEL(TAXI_MISSION_TYPES eType, BOOL bGetOffer = TRUE)
	IF (eType = TXM_02_TAKEITEASY)
		RETURN PICK_STRING(bGetOffer, "OJTX_AAAA", "OJTX_AAAB")

	ELIF (eType = TXM_01_NEEDEXCITEMENT)
		RETURN PICK_STRING(bGetOffer, "OJTX_ABAA", "OJTX_ABAB")
		
	ELIF (eType = TXM_03_DEADLINE)
		RETURN PICK_STRING(bGetOffer, "OJTX_ACAA", "OJTX_ACAB")

	ELIF (eType = TXM_05_TAKETOBEST)
		RETURN PICK_STRING(bGetOffer, "OJTX_AFAA", "OJTX_AFAB")
	
	ELIF (eType = TXM_07_CUTYOUIN)
		RETURN PICK_STRING(bGetOffer, "OJTX_AHAA", "OJTX_AHAB")
		
	ELIF (eType = TXM_08_GOTYOUNOW) OR (eType = TXM_04_GOTYOURBACK)
		RETURN PICK_STRING(bGetOffer, "OJTX_AIAA", "OJTX_AIAB")
		
	ELIF (eType = TXM_09_CLOWNCAR)
		RETURN PICK_STRING(bGetOffer, "OJTX_AJAA", "OJTX_AJAB")
	
	ELIF (eType = TXM_10_FOLLOWTHATCAR)
		RETURN PICK_STRING(bGetOffer, "OJTX_ADAA", "OJTX_ADAB")
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING TAXI_CONTROLLER_GET_DIALOGUE_ROOT(TAXI_MISSION_TYPES eType)

		
	IF (eType = TXM_01_NEEDEXCITEMENT)
		RETURN "OJTX_EXC_OFF"
	
	ELIF (eType = TXM_02_TAKEITEASY)
		RETURN "OJTX_TIE_OFF"
		
	ELIF (eType = TXM_03_DEADLINE)
		RETURN "OJTX_DL_OFF"
		
	ELIF (eType = TXM_04_GOTYOURBACK)
		RETURN "OJTX_GB_OFF"
		
	ELIF (eType = TXM_05_TAKETOBEST)
		RETURN "OJTX_TB_OFF"
	
	ELIF (eType = TXM_07_CUTYOUIN)
		RETURN "OJTX_CI_OFF"
		
	ELIF (eType = TXM_08_GOTYOUNOW) 
		RETURN "OJTX_GN_OFF"
		
	ELIF (eType = TXM_09_CLOWNCAR)
		RETURN "OJTX_CC_OFF"
		
	ELIF (eType = TXM_10_FOLLOWTHATCAR)
		RETURN "OJTX_FC_OFF"
	
	ELIF (eType = TXM_PROCEDURAL)
		RETURN "OJTX_PRO_OFF"
	ENDIF
	
	RETURN ""
ENDFUNC
/// PURPOSE:
///    Assigns the correct dialogue data based on what player enum you currently are
/// PARAMS:
///    convoData - 
PROC TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(structPedsForConversation & convoData)
	enumCharacterList playerEnum
	playerEnum = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())

	IF (playerEnum = CHAR_MICHAEL)
		ADD_PED_FOR_DIALOGUE(convoData, 0, PLAYER_PED_ID(), "MICHAEL")
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE: MICHAEL")
	ELIF (playerEnum = CHAR_TREVOR)
		ADD_PED_FOR_DIALOGUE(convoData, 0, PLAYER_PED_ID(), "TREVOR")
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE: TREVOR - NOT bUnique")
	ELIF (playerEnum = CHAR_FRANKLIN)
		ADD_PED_FOR_DIALOGUE(convoData, 0, PLAYER_PED_ID(), "FRANKLIN")
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE: FRANKLIN - NOT bUnique")
	ELSE
		ADD_PED_FOR_DIALOGUE(convoData, 0, PLAYER_PED_ID(), "MICHAEL")
		TEXT_LABEL_63 txtError = "Invalid enum passed to Taxi dialogue is: "
		txtError += ENUM_TO_INT(playerEnum)
		SCRIPT_ASSERT("INVALID player has been passed to Taxi Dialogue - Please contact John Diaz with this assert.")
		SCRIPT_ASSERT(txtError)
	ENDIF	
	
	//Add Dispatch
	ADD_PED_FOR_DIALOGUE(convoData, 8, NULL, "TaxiDispatch")
ENDPROC

FUNC VECTOR TAXI_CONTROLLER_GET_TAXI_JOB_PickupPoint(TAXI_MISSION_TYPES typeToRequest)
	VECTOR vCurrentPassengerPt
	
	IF (typeToRequest = TXM_01_NEEDEXCITEMENT)
		vCurrentPassengerPt = << -496.0739, -336.6628, 33.5017 >>
	
	ELIF (typeToRequest = TXM_02_TAKEITEASY)
		vCurrentPassengerPt = << 58.8213, 293.8480, 109.6124 >>
	
	ELIF (typeToRequest = TXM_03_DEADLINE)
		vCurrentPassengerPt = << 1407.9248, 3601.9922, 33.9870 >>
	
	ELIF (typeToRequest = TXM_04_GOTYOURBACK)
		vCurrentPassengerPt = << 11.8607, -1123.4800, 27.6801 >>
		
	ELIF (typeToRequest = TXM_05_TAKETOBEST)
		vCurrentPassengerPt = << -412.0875, 1171.3588, 324.8176 >>
	
	ELIF (typeToRequest = TXM_07_CUTYOUIN)
		vCurrentPassengerPt = << -1042.9464, -2689.5498, 12.7572 >>
		
	ELIF (typeToRequest = TXM_08_GOTYOUNOW)
		vCurrentPassengerPt = << -1612.2349, 189.1934, 58.9435 >>
		
	ELIF (typeToRequest = TXM_09_CLOWNCAR)
		vCurrentPassengerPt = << -1285.1528, 294.9519, 63.8557 >>
		
	ELIF (typeToRequest = TXM_10_FOLLOWTHATCAR)
		vCurrentPassengerPt = << 1356.8243, -1581.1733, 52.1245 >>
		
	
	ELSE
		SCRIPT_ASSERT("Thread Name for Taxi Oddjob mission is invalid - Please bug John Diaz with repro steps")
	ENDIF
	
	RETURN vCurrentPassengerPt
	
ENDFUNC

FUNC TAXI_MISSION_TYPES TAXI_CONTROLLER_GetClosestAvailableMission()
	// Go through all missions that aren't complete.
	// Get the closest mission that doesn't have the failed flag set.
	
	FLOAT fCloseDist = TO_FLOAT(TAXI_MAX_PICKUP * TAXI_MAX_PICKUP) //999999999.9
	FLOAT fMinimDist = TO_FLOAT(TAXI_MIN_PICKUP * TAXI_MIN_PICKUP)	// must be greater than 100m2 away
	FLOAT fDist2
	TAXI_MISSION_TYPES eCloseType = TXM_00_INVALID
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	CDEBUG1LN(DEBUG_OJ_TAXI, "TAXI_MAX_PICKUP = ", TAXI_MAX_PICKUP, ", squared aka fCloseDist = ", fCloseDist)
	
	INT index
	REPEAT TXM_NUM_TYPES index
		IF NOT (g_savedGlobals.sTaxiData.missions[index].bIsComplete)
			// If the mission isn't done...
			IF NOT (g_savedGlobals.sTaxiData.missions[index].bMissionFailed)
				// ... and it's not failed.
				fDist2 = VDIST2(vPlayerPos, TAXI_CONTROLLER_GET_TAXI_JOB_PickupPoint(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				CDEBUG1LN(DEBUG_OJ_TAXI,"Distance to mission: ", fDist2, " / Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				IF (fDist2 < fCloseDist) AND fDist2 > fMinimDist
					fCloseDist = fDist2
					eCloseType = INT_TO_ENUM(TAXI_MISSION_TYPES, index)
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI, "Mission too far, Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"Mission failed, Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"Mission completed, Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
		ENDIF
	ENDREPEAT
	
	// If we didn't find anything, it's probably because there's only one non-complete mission left, but it's marked as failed...
	IF (eCloseType = TXM_00_INVALID)
		// Find the one that's not complete, but is marked as failed. Return it.
		REPEAT TXM_NUM_TYPES index
			IF NOT (g_savedGlobals.sTaxiData.missions[index].bIsComplete) AND
					(g_savedGlobals.sTaxiData.missions[index].bMissionFailed)
				
				// check the distance again
				fDist2 = VDIST2(vPlayerPos, TAXI_CONTROLLER_GET_TAXI_JOB_PickupPoint(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				CDEBUG1LN(DEBUG_OJ_TAXI,"Distance to fail mission: ", fDist2, " / Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				
				IF (fDist2 < fCloseDist) AND fDist2 > fMinimDist
					g_savedGlobals.sTaxiData.missions[index].bMissionFailed = FALSE
					
					eCloseType = INT_TO_ENUM(TAXI_MISSION_TYPES, index)
					//RETURN eCloseType
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI, "Fail Mission too far, Mission name: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(INT_TO_ENUM(TAXI_MISSION_TYPES, index)))
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// if still no mission, GO PROCEDURAL
	IF (eCloseType = TXM_00_INVALID)
		CDEBUG1LN(DEBUG_OJ_TAXI, "WE'RE GOING PROCEDURAL")
		
		eCloseType = TXM_PROCEDURAL
	ENDIF
	
	RETURN eCloseType
ENDFUNC

PROC TAXI_CONTROLLER_FillMissionThreadName(TAXI_MISSION_TYPES typeToRequest, TEXT_LABEL_23 & taxiThreadName, structPedsForConversation & convoStruct, BOOL bRadioPrint = TRUE)
	IF (typeToRequest = TXM_01_NEEDEXCITEMENT)
		taxiThreadName = "Taxi_NeedExcitement"
	
	ELIF (typeToRequest = TXM_02_TAKEITEASY)
		taxiThreadName = "Taxi_TakeItEasy"
	
	ELIF (typeToRequest = TXM_03_DEADLINE)
		taxiThreadName = "Taxi_Deadline"
	
	ELIF (typeToRequest = TXM_04_GOTYOURBACK)
		taxiThreadName = "Taxi_GotYourBack"
		
	ELIF (typeToRequest = TXM_05_TAKETOBEST)
		taxiThreadName = "Taxi_TakeToBest"
		
	ELIF (typeToRequest = TXM_07_CUTYOUIN)
		taxiThreadName = "Taxi_CutYouIn"
		
	ELIF (typeToRequest = TXM_08_GOTYOUNOW)
		taxiThreadName = "Taxi_GotYouNow"
		
	ELIF (typeToRequest = TXM_09_CLOWNCAR)
		taxiThreadName = "Taxi_ClownCar"
		
	ELIF (typeToRequest = TXM_10_FOLLOWTHATCAR)
		taxiThreadName = "Taxi_FollowCar"
	
	ELIF (typeToRequest = TXM_PROCEDURAL)
		taxiThreadName = "Taxi_Procedural"
		
	
	ELSE
		SCRIPT_ASSERT("Thread Name for Taxi Oddjob mission is invalid - Please bug John Diaz with repro steps")
	ENDIF
	
	IF (bRadioPrint)
		TEXT_LABEL_23 radioText = TAXI_CONTROLLER_GET_DIALOGUE_ROOT(typeToRequest)
		TEXT_LABEL_23 specLabel = radioText
		specLabel += "_1"//TAXI_CONTROLLER_GET_OFFER_OR_RESPONSE_LABEL(typeToRequest, TRUE)
		
		TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(convoStruct)
		
		PLAY_SINGLE_LINE_FROM_CONVERSATION(convoStruct, "OJTXAUD", radioText, specLabel, CONV_PRIORITY_HIGH)
	ENDIF
ENDPROC

/// PURPOSE: 
///    When the launcher first kicks off, it needs to set all the missions that we're going to play, and in what order.
PROC TAXI_CONTROLLER_InitMissionArray()
	CDEBUG1LN(DEBUG_OJ_TAXI,"#################################### INIT TAXI MISSION ORDER!!!! ####################################")
	// If we've never filled out the order before, and we haven't finished them all, fill the order.
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData,TAXI_BOOL_INIT_MISSIONS)
		INT index
		FOR index = 0 TO (ENUM_TO_INT(TXM_NUM_TYPES) - 1)
			g_savedGlobals.sTaxiData.missions[0].bIsComplete = FALSE
			g_savedGlobals.sTaxiData.missions[0].bMissionFailed = FALSE
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE: Spawns a taxi right in front of the player
#IF IS_DEBUG_BUILD
	PROC TAXI_CONTROLLER_Give_Taxi()
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_LOOK_BEHIND)
			INT iLoopCounter = 0
			REQUEST_MODEL(TAXI_GET_TAXI_MODEL_NAME())
			
			WHILE (NOT HAS_MODEL_LOADED(TAXI_GET_TAXI_MODEL_NAME())) AND (iLoopCounter < 500)
				iLoopCounter++
				WAIT(0)
			ENDWHILE
			
			IF (iLoopCounter < 500)
				PED_INDEX playerPed = PLAYER_PED_ID()
				IF NOT IS_ENTITY_DEAD(playerPed)
					// Create the taxi and then set it as no longer needed.
					VECTOR vTaxiPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(playerPed, <<1.5, 3.0, 0.75>>)
					CREATE_VEHICLE(TAXI_GET_TAXI_MODEL_NAME(), vTaxiPos, GET_ENTITY_HEADING(playerPed))
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(TAXI_GET_TAXI_MODEL_NAME())
		ENDIF
	ENDPROC

	PROC TAXI_CONTROLLER_PrintCurrentMissionArray()
		IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_COVER) AND IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			INT index
			REPEAT TXM_NUM_TYPES index
				CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Controller Mission Order Slot: ", index, ", IsPassed: ",g_savedGlobals.sTaxiData.missions[index].bIsComplete)
				CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Controller Mission Order Slot: ", index, ", IsFailed: ",g_savedGlobals.sTaxiData.missions[index].bMissionFailed)
			ENDREPEAT
		ENDIF
	ENDPROC
	
	PROC TAXI_CONTROLLER_ToggleProcedurals(BOOL & bDoProcedurals)
		IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_COVER) AND IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			IF NOT bDoProcedurals
				CDEBUG1LN(DEBUG_OJ_TAXI,"Setting procedurals on")
				bDoProcedurals = TRUE
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"Setting procedurals off")
				bDoProcedurals = FALSE
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

PROC TAXI_CONTROLLER_ResetFailedMissions()
	INT index
	REPEAT TXM_NUM_TYPES index
		g_savedGlobals.sTaxiData.missions[index].bMissionFailed = FALSE
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Fancy function name. 
///    If a mission was failed, it invalidates it for selection.
///    If a mission was passed, it removes it from the array.
PROC TAXI_CONTROLLER_ProcessMissionEnding(TAXI_MISSION_TYPES eTypePlayed, BOOL bPassed)
	// Reset all missions to be not failed.
	TAXI_CONTROLLER_ResetFailedMissions()
	
	// Handle the current mission.
	IF bPassed
		// We passed this one, remove it from the array of potentials.
		g_savedGlobals.sTaxiData.missions[eTypePlayed].bIsComplete = TRUE
		g_savedGlobals.sTaxiData.missions[eTypePlayed].bMissionFailed = FALSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"Setting bIsComplete for mission: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(eTypePlayed))
	ELSE
		// We failed this one, set is as non-selectable for now.
		// This gets reset when another mission is started...
		g_savedGlobals.sTaxiData.missions[eTypePlayed].bIsComplete = FALSE
		g_savedGlobals.sTaxiData.missions[eTypePlayed].bMissionFailed = TRUE
		CDEBUG1LN(DEBUG_OJ_TAXI,"Setting bMissionFailed for mission: ", TAXI_CONTROLLER_GET_DIALOGUE_ROOT(eTypePlayed))
	ENDIF
ENDPROC

/// PURPOSE:
///    Lets the controller how many more missions are available to play before taxi is done.
FUNC INT TAXI_CONTROLLER_GetMissionCountRemaining()
	INT index, iCnt = 0
	REPEAT TXM_NUM_TYPES index
		IF NOT g_savedGlobals.sTaxiData.missions[index].bIsComplete
			iCnt += 1
		ENDIF
	ENDREPEAT
	
	RETURN iCnt
ENDFUNC

/// PURPOSE:
///    Lets us know if the player is in any taxi.
/// RETURNS:
///    TRUE sometimes, FALSE at other junctures.
FUNC BOOL IS_PLAYER_IN_ANY_TAXI()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF (GET_PED_IN_VEHICLE_SEAT(tempVeh) = PLAYER_PED_ID())
					IF IS_VEHICLE_MODEL(tempVeh, TAXI_GET_TAXI_MODEL_NAME())
						//SET_HORN_ENABLED(tempVeh, FALSE)		// To Fix Bug # 551605 - DS		
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_TAXI_VEHICLE_DAMAGE_OK(VEHICLE_INDEX vehToCheck)
	// Make sure the car has at least 2/4 tires, and some doors.
	INT iTiresIntact = 0
	IF NOT IS_VEHICLE_TYRE_BURST(vehToCheck, SC_WHEEL_CAR_FRONT_LEFT)			iTiresIntact += 1				ENDIF
	IF NOT IS_VEHICLE_TYRE_BURST(vehToCheck, SC_WHEEL_CAR_FRONT_RIGHT)			iTiresIntact += 1				ENDIF
	IF NOT IS_VEHICLE_TYRE_BURST(vehToCheck, SC_WHEEL_CAR_REAR_LEFT)			iTiresIntact += 1				ENDIF
	IF NOT IS_VEHICLE_TYRE_BURST(vehToCheck, SC_WHEEL_CAR_REAR_RIGHT)			iTiresIntact += 1				ENDIF
	IF (iTiresIntact < 2)
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehToCheck))
		INT iDoorsIntact = 0
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehToCheck, SC_DOOR_FRONT_LEFT) 			iDoorsIntact += 1				ENDIF
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehToCheck, SC_DOOR_FRONT_RIGHT) 			iDoorsIntact += 1				ENDIF
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehToCheck, SC_DOOR_REAR_LEFT) 				iDoorsIntact += 1				ENDIF
		IF NOT IS_VEHICLE_DOOR_DAMAGED(vehToCheck, SC_DOOR_REAR_RIGHT) 			iDoorsIntact += 1				ENDIF
		
		IF (iDoorsIntact < 2)
			PRINTLN("Doors left: ", iDoorsIntact)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Checks to see if the player's even allowed to accept or ask for a mission.
/// RETURNS:
///    TRUE if so.
FUNC TAXI_NO_MISSION_REASON TAXI_CONTROLLER_GetMissionAbility()
	IF NOT IS_PLAYER_IN_ANY_TAXI()
		// Not even in a taxi?
		RETURN TAXINOMISSION_NOTAXI
	ELSE
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		// In a taxi... check health.
		INT iEngineHealth, iVehHealth
		iEngineHealth = ROUND(GET_VEHICLE_ENGINE_HEALTH(tempVeh))
		iVehHealth = GET_ENTITY_HEALTH(tempVeh)
		IF (iEngineHealth < 100) OR (iVehHealth < 100)
			RETURN TAXINOMISSION_HEALTHGONE
		ENDIF
		
		// Check doors and tires.
		IF NOT IS_TAXI_VEHICLE_DAMAGE_OK(tempVeh)
			RETURN TAXINOMISSION_HEALTHGONE
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			RETURN TAXINOMISSION_WANTED
		ENDIF
	ENDIF	
	
	RETURN TAXI_CAN_RUN_MISSION
ENDFUNC

/// PURPOSE: This appends the correct chars to the conversation line to be played depending on what player is being used
///    
/// PARAMS:
///    tlLine - the line to append to
///    bRequestMission - If the player is requesting a mission, the lines are looking for M,F,T , other he's being offered a mission and play_single_line is expecting an "_#"
PROC TAXI_ADD_DIALOGUE_OFFSET_FOR_DISPATCH(TEXT_LABEL_23 &tlLine,BOOL bRequestMission = FALSE)

	enumCharacterList playerEnum
	playerEnum = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())

	IF (playerEnum = CHAR_MICHAEL)
		IF bRequestMission
			tlLine += "M"
		ELSE
			tlLine += "_2"
		ENDIF
	ELIF (playerEnum = CHAR_TREVOR)
		IF bRequestMission
			tlLine += "T"
		ELSE
			tlLine += "_3"
		ENDIF
	ELIF (playerEnum = CHAR_FRANKLIN)
		IF bRequestMission
			tlLine += "F"
		ELSE
			tlLine += "_4"
		ENDIF
	ENDIF	

ENDPROC

PROC TOGGLE_TAXI_OJ_RADIO_SOUNDS_ON()
	
	//B*519180 - Turn on "Taxi Sounds" when player is in taxi.
	VEHICLE_INDEX vPlayerTaxi
	
	vPlayerTaxi = GET_PLAYERS_LAST_VEHICLE()
	
	IF IS_VEHICLE_DRIVEABLE(vPlayerTaxi)
		PLAY_SOUND_FROM_ENTITY(-1, "Radio_On", vPlayerTaxi, "TAXI_SOUNDS")
		CDEBUG1LN(DEBUG_OJ_TAXI," 			[TAXI_SOUNDS] Taxi Sounds Are Enabled Captain")
	ENDIF
	
ENDPROC

PROC TOGGLE_TAXI_OJ_RADIO_SOUNDS_OFF()
	//B*519180 - Turn off "Taxi Sounds" when player leaves taxi.
	VEHICLE_INDEX vPlayerTaxi
	
	vPlayerTaxi = GET_PLAYERS_LAST_VEHICLE()
	
	IF IS_VEHICLE_DRIVEABLE(vPlayerTaxi)
		PLAY_SOUND_FROM_ENTITY(-1, "Radio_Off", vPlayerTaxi, "TAXI_SOUNDS")		
		CDEBUG1LN(DEBUG_OJ_TAXI," 			[TAXI_SOUNDS] Taxi Sounds Are Disabled Captain")
	ENDIF
	
	
ENDPROC

PROC TOGGLE_TAXI_OJ_DISPATCH()
	
	IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PLAYER_IN_TAXI)
		IF IS_PLAYER_IN_ANY_TAXI()
			SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PLAYER_IN_TAXI)
			TOGGLE_TAXI_OJ_RADIO_SOUNDS_ON()
		ENDIF
	ELSE
		IF NOT IS_PLAYER_IN_ANY_TAXI()
			CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PLAYER_IN_TAXI)
			TOGGLE_TAXI_OJ_RADIO_SOUNDS_OFF()
		ENDIF
	ENDIF

ENDPROC

//EOF
