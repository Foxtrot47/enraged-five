///		Taxi_Tips.sch
///		Author: John R. Diaz
///		Starting a new header to keep track of all the tipping system information for taxi oddjobs
USING "script_maths.sch"
USING "dialogue_public.sch"

CONST_INT		TAXI_OJ_TIP_LEVEL_1	1
CONST_INT		TAXI_OJ_TIP_LEVEL_2	2
CONST_INT		TAXI_OJ_TIP_LEVEL_3	3

CONST_INT		TAXI_TDL_TIP_HIGH					30
CONST_INT		TAXI_TDL_TIP_MED					10


//Deadline Specific
CONST_FLOAT		CONST_TAXI_NUM_SECS_IN_INTERVAL		29.0		//Use this to divide up the deadline and see how often passenger is going to give you an update on your driving
CONST_INT		CONST_TAXI_TIP_FACTOR				10

CONST_FLOAT		TAXI_TIP_PERCENT_GREAT		0.5
CONST_FLOAT		TAXI_TIP_PERCENT_GOOD		0.25
CONST_FLOAT		TAXI_TIP_PERCENT_AVG		0.1
CONST_FLOAT		TAXI_TIP_PERCENT_BAD		0.25

CONST_FLOAT		CONST_TAXI_OJ_TIP_PERCENT_AMAZING	0.4
CONST_FLOAT		CONST_TAXI_OJ_TIP_PERCENT_AVERAGE	0.15
CONST_FLOAT		CONST_TAXI_OJ_TIP_PERCENT_ASS		0.0

CONST_INT		CONST_TAXI_OJ_TIP_AMAZING_CUTOFF	6
CONST_INT		CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF	3


STRUCT TAXI_OJ_TIP_TYPE
	FLOAT	fTipLevel
	BOOL	bTipEnabled
	BOOL	bPositive
	
ENDSTRUCT
STRUCT TAXI_OJ_TIP_DATA

	FLOAT 	fTimeToCheckAgain
	INT 	iTipBits
	INT 	iTipTracker
	
ENDSTRUCT

ENUM TAXI_TIP_BITS
	//General bits-------------------------
	TAXI_TIP_BIT_CONVERTED		 	= 	BIT0,
	TAXI_TIP_BIT_POS_DELIVERY_TIME 	= 	BIT1,
	TAXI_TIP_BIT_HIT_PED			=	BIT2,
	TAXI_TIP_BIT_HIT_PED_POS		=	BIT3,
	TAXI_TIP_BIT_TOOK_DAMAGE		=	BIT4,
	TAXI_TIP_BIT_TOOK_DAMAGE_POS	=	BIT5,
	TAXI_TIP_BIT_ROLL_CAR			=	BIT6,
	TAXI_TIP_BIT_ROLL_CAR_POS		=	BIT7,
	TAXI_TIP_BIT_LEFT_CAR			=	BIT8,
	TAXI_TIP_BIT_STOPPED			=	BIT9,
	TAXI_TIP_BIT_RADIO_STATION_LIKE	=	BIT10,
	TAXI_TIP_BIT_RADIO_STATION_DISLIKE=	BIT11,
	TAXI_TIP_BIT_FREEBIE			=	BIT12,
	
	TAXI_TIP_BIT_BORED				=	BIT13,
	TAXI_TIP_BIT_SPEEDING			=	BIT14,
	TAXI_TIP_BIT_AIR				=	BIT15,
	TAXI_TIP_BIT_QUICKSTOP			=	BIT16,
	TAXI_TIP_BIT_POWERSLIDING		=	BIT17,
	TAXI_TIP_BIT_ONCOMING			=	BIT18,
	TAXI_TIP_BIT_SIDEWALK			=	BIT19,
	TAXI_TIP_BIT_OFFROAD			=	BIT20,
	TAXI_TIP_BIT_NEG_EXCITEMENT		=	BIT21,
	TAXI_TIP_BIT_POS_EXCITEMENT		=	BIT22,
	TAXI_TIP_BIT_FOLLOW_TOO_FAR		=	BIT23,
	TAXI_TIP_BIT_FOLLOW_TOO_CLOSE	=	BIT24,
	TAXI_TIP_BIT_FOLLOW_JUST_RIGHT	=	BIT25,
	TAXI_TIP_BIT_NOPUKE				=	BIT26,
	TAXI_TIP_BIT_PUKE				=	BIT27,
	TAXI_TIP_BIT_PUKE_STOP			=	BIT28,
	TAXI_TIP_BIT_PUKE_NO_STOP		=	BIT29,
	TAXI_TIP_BIT_LOST_POLICE		=	BIT30
	
ENDENUM

ENUM TAXI_TIP_TYPES
	TAXI_TIP_TYPE_NULL			= -1,
	//General-----------------------------------------------------------
	TAXI_TIP_TYPE_DELIVERY_TIME = 0,
	TAXI_TIP_TYPE_HIT_PED,
	TAXI_TIP_TYPE_TOOK_DAMAGE,
	TAXI_TIP_TYPE_ROLL_CAR,
	TAXI_TIP_TYPE_LEFT_CAR,
	TAXI_TIP_TYPE_STOPPED,
	TAXI_TIP_TYPE_RADIO_STATION_LIKE,
	TAXI_TIP_TYPE_RADIO_STATION_DISLIKE,
	TAXI_TIP_TYPE_LOST_POLICE,
	
	//Needs Excitement & Take It Easy
	TAXI_TIP_TYPE_BORED,			
	TAXI_TIP_TYPE_SPEEDING,		
	TAXI_TIP_TYPE_AIR,			
	TAXI_TIP_TYPE_QUICKSTOP,		
	TAXI_TIP_TYPE_POWERSLIDING,	
	TAXI_TIP_TYPE_ONCOMING,		
	TAXI_TIP_TYPE_SIDEWALK,		
	TAXI_TIP_TYPE_OFFROAD,		
	TAXI_TIP_TYPE_NOPUKE,
	TAXI_TIP_TYPE_PUKE,
	TAXI_TIP_TYPE_PUKE_STOP,
	TAXI_TIP_TYPE_PUKE_NO_STOP,
	
	//Follow Car Specifics----------------------------------------------
	TAXI_TIP_TYPE_FOLLOW_CAR_NEG_LOOSING_CAR,
	TAXI_TIP_TYPE_FOLLOW_CAR_NEG_TOO_CLOSE_TO_CAR,
	TAXI_TIP_TYPE_FOLLOW_CAR_POS_SWEET_SPOT,
	
	//I Know Way--------------------------------------------------------
	TAXI_TIP_TYPE_IKNOWWAY_POS_DRIVING_STRAIGHT,
	TAXI_TIP_TYPE_IKNOWWAY_NEG_MISSED_TURN,
	
	
	TAXI_TIP_TYPE_TOTAL
ENDENUM
PROC ADD_TAXI_OJ_TIP(TAXI_TIP_TYPES eTipType,INT &iTipToAddOrSubtract,INT &bitField, TEXT_LABEL_23 &myTaxiLine, TEXT_LABEL_23 textLine, BOOL bGoodTip =FALSE)
	//Keep track of first time gate here.
	
	//What tip of tip
	IF eTipType = TAXI_TIP_TYPE_DELIVERY_TIME//---------------------------------
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_DELIVERY_TIME)
			
			//Negative Tip
			IF NOT bGoodTip
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
			//Positive Increment
			ELSE
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_3)
			ENDIF
			
			//If the bit is set than set the line, I wonder if there's a better way to do this? So I don't have to include it in every Bit Check
			myTaxiLine = textLine
		ENDIF
	ELIF eTipType =TAXI_TIP_TYPE_LOST_POLICE//----------------------------------------
	
			
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_LOST_POLICE)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine
		ENDIF

	
	ELIF eTipType = TAXI_TIP_TYPE_HIT_PED//----------------------------------------
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_HIT_PED)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FREEBIE)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
				myTaxiLine = textLine
			//Give the player a freebie
			ELSE
				CLEAR_BITMASK_AS_ENUM(bitField,TAXI_TIP_BIT_FREEBIE)
			ENDIF
		ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_HIT_PED_POS)	
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_3)
			myTaxiLine = textLine

		ENDIF
	
	ELIF eTipType = TAXI_TIP_TYPE_STOPPED//----------------------------------------
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_STOPPED)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FREEBIE)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
				myTaxiLine = textLine
			//Give the player a freebie
			ELSE
				CLEAR_BITMASK_AS_ENUM(bitField,TAXI_TIP_BIT_FREEBIE)
			ENDIF
		ENDIF

	ELIF eTipType = TAXI_TIP_TYPE_TOOK_DAMAGE//----------------------------------------
		//Is the tip turned on
		//Negative Tip
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_TOOK_DAMAGE)
			IF NOT IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FREEBIE)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
				myTaxiLine = textLine
				//Give the player a freebie
			ELSE
				CLEAR_BITMASK_AS_ENUM(bitField,TAXI_TIP_BIT_FREEBIE)
			ENDIF
		ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_TOOK_DAMAGE_POS)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_3)
			myTaxiLine = textLine
		ENDIF
	
	ELIF eTipType = TAXI_TIP_TYPE_ROLL_CAR//----------------------------------------

		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_ROLL_CAR)
			IF NOT IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FREEBIE)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				//Give the player a freebie
			ELSE
				CLEAR_BITMASK_AS_ENUM(bitField,TAXI_TIP_BIT_FREEBIE)
			ENDIF
		
		ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_ROLL_CAR_POS)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_3)
			myTaxiLine = textLine
		ENDIF
	
	ELIF eTipType = TAXI_TIP_TYPE_LEFT_CAR//----------------------------------------
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_LEFT_CAR)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
			myTaxiLine = textLine
		ENDIF
	
	ELIF eTipType = TAXI_TIP_TYPE_RADIO_STATION_LIKE//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_RADIO_STATION_LIKE)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_RADIO_STATION_DISLIKE//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
			myTaxiLine = textLine
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_BORED//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_BORED)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_3)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_SPEEDING//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_SPEEDING)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			
			ELSE
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			
				
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_AIR//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_AIR)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_QUICKSTOP//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_QUICKSTOP)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_POWERSLIDING//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POWERSLIDING)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_ONCOMING//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_ONCOMING)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_SIDEWALK//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_SIDEWALK)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
		
	ELIF eTipType = TAXI_TIP_TYPE_OFFROAD//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_OFFROAD)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NEG_EXCITEMENT)
				iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
			ELIF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_POS_EXCITEMENT)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine
				
			ENDIF
			
		ENDIF
	
	ELIF eTipType = TAXI_TIP_TYPE_FOLLOW_CAR_NEG_LOOSING_CAR//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FOLLOW_TOO_FAR)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine
		
		ENDIF	
	ELIF eTipType = TAXI_TIP_TYPE_FOLLOW_CAR_NEG_TOO_CLOSE_TO_CAR//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FOLLOW_TOO_CLOSE)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine
			
		ENDIF	
	ELIF eTipType = TAXI_TIP_TYPE_FOLLOW_CAR_POS_SWEET_SPOT//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_FOLLOW_JUST_RIGHT)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine
						
		ENDIF	
	ELIF eTipType = TAXI_TIP_TYPE_NOPUKE//----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NOPUKE)
			IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_NOPUKE)
				iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_2)
				myTaxiLine = textLine				
			ENDIF
			
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_PUKE //----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_PUKE)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
			myTaxiLine = textLine				
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_PUKE_NO_STOP //----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_PUKE_NO_STOP)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_1)
			myTaxiLine = textLine				
			
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_PUKE_STOP //----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_PUKE_STOP)
			iTipToAddOrSubtract += (1 * TAXI_OJ_TIP_LEVEL_1)
			myTaxiLine = textLine				
			
		ENDIF
	ELIF eTipType = TAXI_TIP_TYPE_IKNOWWAY_NEG_MISSED_TURN //----------------------------------------
		
		//Is the tip turned on
		IF IS_BITMASK_AS_ENUM_SET(bitField,TAXI_TIP_BIT_AIR)
			iTipToAddOrSubtract -= (1 * TAXI_OJ_TIP_LEVEL_2)
			myTaxiLine = textLine				
			
		ENDIF
	ENDIF
	
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Adding Taxi OJ Tip: ","",iTipToAddOrSubtract)
ENDPROC

PROC UPDATE_TAXI_OJ_TIP(TaxiStruct &myTaxiData, INT &iTipStateIndex, BOOL bDebug = FALSE)
	IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_TipsBitMask, TAXI_TIP_BIT_CONVERTED)
		SWITCH iTipStateIndex
			CASE 0
				IF NOT IS_STRING_NULL_OR_EMPTY(myTaxiData.sTaxiOJ_TipLine)
				AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					iTipStateIndex++
					CDEBUG1LN(DEBUG_OJ_TAXI,"Advancing UPDATE_TAXI_OJ_TIP to next state")
				ENDIF
			BREAK
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					myTaxiData.sTaxiOJ_TipLine = NULL_STRING()
					myTaxiData.iTaxiOJ_CashTip += myTaxiData.iTaxiOJ_CashTipToAdd
					
					IF myTaxiData.iTaxiOJ_CashTip < 0
						myTaxiData.iTaxiOJ_CashTip = 0
					ENDIF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ Tip has earned " ,"",myTaxiData.iTaxiOJ_CashTipToAdd, " current tip is ", "", myTaxiData.iTaxiOJ_CashTip )
					
					myTaxiData.iTaxiOJ_CashTipToAdd = 0
					iTipStateIndex = 0
						IF bDebug
								
							#IF IS_DEBUG_BUILD
								SCRIPT_ASSERT("Tip Updated")
							#ENDIF
						
						ENDIF
				ENDIF
			BREAK
			
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC SET_TAXI_TIP_CUTOFFS( TaxiStruct &myTaxiData, INT iCashAvgCutoff , INT iCashAmazeCutoff )
	
	myTaxiData.iTaxiOJ_CashTipAvg = iCashAvgCutoff
	myTaxiData.iTaxiOJ_CashTipAmazing = iCashAmazeCutoff
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXI_TIP_CUTOFFS - Average = ", myTaxiData.iTaxiOJ_CashTipAvg, " Amazing = ", myTaxiData.iTaxiOJ_CashTipAmazing)
ENDPROC

/// PURPOSE: Call this at the very end of all your tip calculation to convert the tip into monetary value. 
///    		NOTE : This overwrites your tip. If you need that value for something, save it off before calling this function
///    
/// PARAMS:
///    myTaxiData - global taxi data

PROC CONVERT_TAXI_TIP_TO_CASH(TaxiStruct &myTaxiData)
	
	//AMAZING TIP
	IF myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing
		myTaxiData.iTaxiOJ_CashTip = CEIL(myTaxiData.iTaxiOJ_CashFare * CONST_TAXI_OJ_TIP_PERCENT_AMAZING)
		
	
	//AVERAGE TIP
	ELIF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAmazing	
	AND myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAvg
		myTaxiData.iTaxiOJ_CashTip = CEIL(myTaxiData.iTaxiOJ_CashFare * CONST_TAXI_OJ_TIP_PERCENT_AVERAGE)
	
	//ASS TIP
	ELSE
		myTaxiData.iTaxiOJ_CashTip = 0
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_TipsBitMask, TAXI_TIP_BIT_CONVERTED)
		SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask, TAXI_TIP_BIT_CONVERTED)
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$		CONVERT_TAXI_TIP_TO_CASH: $", myTaxiData.iTaxiOJ_CashFare, " with a tip of $", myTaxiData.iTaxiOJ_CashTip )	
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE: used for dev only to set your tip level to amazing
///    
/// PARAMS:
///    myTaxiData - global taxi data
PROC SET_TAXI_TIP_TO_AMAZING(TaxiStruct &myTaxiData)
	myTaxiData.iTaxiOJ_CashTip+= myTaxiData.iTaxiOJ_CashTipAmazing + 100
	CDEBUG1LN(DEBUG_OJ_TAXI,"Cheated and set Taxi Tip to AMAZING")
ENDPROC
#ENDIF
//EOF
