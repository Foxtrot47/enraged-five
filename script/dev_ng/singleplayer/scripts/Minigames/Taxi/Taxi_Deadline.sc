//=======================================================================================================================================
// Taxi_Deadline.sc
// Dev : John R. Diaz
/*
	•	“I’m on a deadline.” The passenger gives you a set amount of time to get to the location. This is all or nothing. 
		If you exceed the time, the passenger will bail. 
*/



//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



//Includes------------------------------------------------------
USING "Taxi_Includes.sch"


//Consts-------------------------------------------------------
CONST_FLOAT						CONST_TAXI_GOOD_SPEED				23.0		//TAXI has a max speed of 49, so half that is a decent speed
CONST_FLOAT						CONST_TAXI_OJ_DISTANCE_PAD			20.0
CONST_FLOAT						CONST_TAXI_OJ_TIME_TO_LOSE_ALL_TIP	240.0
CONST_INT						CONST_iTaxi_DeadlineMinutes			30//45
//Local Variables-----------------------------------------------


//Custom Data
TaxiStruct 						myTaxiData

MODEL_NAMES 					mPassengerModel = G_M_M_CHIGOON_02

//Taxi Bonus Data-------------------------------------------------------------------
ENUM TAXIOJ_DL_BONUS
	TDL_BONUS_SPEEDDEMON = 0,
	TDL_BONUS_TOTAL				
ENDENUM

CONST_INT						TAXI_CONST_BONUS_CASH_SPEED_DEMON	200
CONST_FLOAT						TAXI_CONST_BONUS_TIME_SPEED_DEMON	160.0
BONUS_FIELD						bonusFieldDeadline[TDL_BONUS_TOTAL]

//--------------------------------------------------------------------------------

//Ints
INT 							iDebugThrottle = 1
INT								iTipIndex =0 
INT								iRideProgress = 0
//INT 							iCurrentGameTimeInMilliseconds
//INT 							iTimeLimitMin
//INT 							iTimeLimitHour

//Floats
FLOAT							fTimeLimit

SEQUENCE_INDEX					siTemp

VECTOR							vNewDLDropoff = <<344.74585, 452.18317, 145.993595>>

structTimer						DeadlineTimer
structTimer						localBanterDelay
structTimer						endBanterDelay

//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

TIMEOFDAY						eDeadline

TEXT_LABEL_7					sDeadlineTime

CONST_FLOAT						DEADLINE_FAIL_LEEWAY 10.0

//Aggro checks
AGGRO_ARGS						aggroArgs  

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID	 			taxiRideWidgets
	TEXT_LABEL_63				sDebugString[5]
	BOOL 						bDebugDrawDL_Stats
	BOOL 						bDebugTurnOnFreeRide = FALSE
	BOOL 						bDebugSucceed
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF

ENUM TAXI_OJ_DL_POINTOFINTEREST
	
	TXOJ_POI_CASINO = 0,
	TXOJ_POI_CASINO_WARPPT,
	
	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1,
	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_WARPPT_1,
	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_PICKUP_1,
	
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_2,
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_2,
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_PICKUP_2,
	
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_3,
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_3,
	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_PICKUP_3,
	
	TXOJ_POI_TOTAL_NUM
ENDENUM

TAXI_OJ_DL_POINTOFINTEREST eWarpPt 		
TAXI_OJ_DL_POINTOFINTEREST ePickupPt

//For B*328695 - to change startpt for this mission change these below------------------------------------------------
TAXI_OJ_DL_POINTOFINTEREST eSpawnPt 	= 		TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1// TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_2 *****OR****  TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1


ENUM TAXI_DEADLINE_GOAL_DISTS_AND_TIMES
	TXDL_OVERALL_GOAL_TIME	=	0,
	TXDL_TIME_GOOD_1,
	TXDL_DIST_GOOD_1,
	TXDL_TIME_GOOD_2,
	TXDL_DIST_GOOD_2,
	TXDL_TIME_GOOD_3,
	TXDL_DIST_GOOD_3,
	
	TXDL_TOTAL_GOALS
	
ENDENUM

FLOAT		fDeadlineGoals[TXDL_TOTAL_GOALS]

CONST_INT	NUM_TIME_DISTANCE_SAMPLES	22
CONST_INT	NUM_POS_LINES				8
CONST_INT	NUM_NEG_LINES				8

FLOAT		fTimeToCheckDistanceLeft[NUM_TIME_DISTANCE_SAMPLES]
FLOAT		fExpectedDistanceLeft[NUM_TIME_DISTANCE_SAMPLES]

//FUNCTIONS------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_TAXI_OJ_DL_VECTORS(TAXI_OJ_DL_POINTOFINTEREST eWhichPt )
	
	VECTOR vReturnPoint = NULL_VECTOR()
	
	SWITCH eWhichPt
	
		//Casino
		CASE TXOJ_POI_CASINO
			vReturnPoint = << 924.2684, 50.0780, 79.7647 >> 
		BREAK
		
		CASE TXOJ_POI_CASINO_WARPPT
			vReturnPoint = << 916.0502, 39.3504, 79.7647 >> 
		BREAK
		
		//Passenger Spawn Pt Original - NE CountrySide
		CASE TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1
			vReturnPoint = <<1971.2462, 3741.5171, 31.3268>> //<< 1407.9248, 3601.9922, 33.9870 >>
		BREAK
		
		//WarpPt for SpawnPt #1
		CASE TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_WARPPT_1
			vReturnPoint = <<2004.2780, 3752.3193, 31.4156>> //<< 1368.4233, 3570.0098, 33.9975 >>
		BREAK
		
		CASE TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_PICKUP_1
			vReturnPoint = << 1410.8911, 3596.0676, 33.8351 >>
		BREAK
		//Passenger Spawn Pt Alt - Near Del Perro Beach SW part of Map
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_2
			vReturnPoint = << -1383.0603, -972.8339, 8.0140 >>
		BREAK
		
		//WarpPt for SpawnPt #2
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_2
			vReturnPoint = << -1399.1967, -944.1848, 9.4306 >>
		BREAK
		
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_PICKUP_2
			vReturnPoint = << -1379.2621, -972.9459, 7.8097 >>
		BREAK
		//Alt Pickup Pt#3 by Bahama Mamas West
		//Passenger Spawn Pt Alt - Near Del Perro Beach SW part of Map
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_3
			vReturnPoint = << -1390.7650, -584.3324, 29.2306 >>
		BREAK
		
		//WarpPt for SpawnPt #2
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_3
			vReturnPoint = << -1410.2234, -590.6025, 29.3669 >>
		BREAK
		
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_PICKUP_3
			vReturnPoint = << -1379.2621, -972.9459, 7.8097 >>
		BREAK
		
	ENDSWITCH
	
	RETURN vReturnPoint
ENDFUNC
/// PURPOSE: the index in these should correspond to the index above in VECTOR variation
///    
/// PARAMS:
///    iWhichIndex - 
/// RETURNS:
///    
FUNC FLOAT GET_TAXI_OJ_DL_HEADINGS(TAXI_OJ_DL_POINTOFINTEREST eWhichPt)
	
	FLOAT fReturnHeading = 0
	
	SWITCH eWhichPt
	
		//Casino
		CASE TXOJ_POI_CASINO
			fReturnHeading = 328.48
		BREAK
		
		CASE TXOJ_POI_CASINO_WARPPT
			fReturnHeading = 328.48
		BREAK
		
		//Passenger Spawn Pt Original - NE CountrySide
		CASE TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1
			fReturnHeading = 239.1430 //237.0
		BREAK
		
		//Heading for WarpPt to SpawnPt #1
		CASE TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_WARPPT_1
			fReturnHeading = 121.2021 //287
		BREAK
		
		//Passenger Spawn Pt Alt - Near Del Perro Beach SW part of Map
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_2
			fReturnHeading = 268.6
		BREAK
		
		//Heading for WarpPt for SpawnPt #2
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_2
			fReturnHeading = 218.5
		BREAK
		
		//Passenger Spawn Pt Alt - Near Del Perro Beach SW part of Map
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_SPAWNPT_3
			fReturnHeading = 268.6
		BREAK
		
		//Heading for WarpPt for SpawnPt #2
		CASE TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_3
			fReturnHeading = 301.6025 
		BREAK
		
	ENDSWITCH
	
	RETURN fReturnHeading
ENDFUNC

PROC SETUP_TAXI_OJ_DEADLINE_GOALS()
	//Picked the Country start pt
	IF eSpawnPt 	= 	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_SPAWNPT_1
		
		eWarpPt 	= 	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_WARPPT_1
		ePickupPt	= 	TXOJ_POI_NE_COUNTRYSIDE_PASSENGER_PICKUP_1
		
		fDeadlineGoals[TXDL_OVERALL_GOAL_TIME] = 210 //165//180
		fDeadlineGoals[TXDL_TIME_GOOD_1] = 60//70
		fDeadlineGoals[TXDL_DIST_GOOD_1] = 3150
		fDeadlineGoals[TXDL_TIME_GOOD_2] = 105//110
		fDeadlineGoals[TXDL_DIST_GOOD_2] = 1800
		fDeadlineGoals[TXDL_TIME_GOOD_3] = 150 //160 //TAXI_CONST_BONUS_TIME_SPEED_DEMON
		fDeadlineGoals[TXDL_DIST_GOOD_3] = 200
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Goals defined for 1st dropoff")
	//Picked Del Perro Beach startpt
	ELSE
		eWarpPt 		= 	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_WARPPT_3
		ePickupPt	= 	TXOJ_POI_DEL_PERRO_BEACH_PASSENGER_PICKUP_3
		
		fDeadlineGoals[TXDL_OVERALL_GOAL_TIME] = 140
		fDeadlineGoals[TXDL_TIME_GOOD_1] = 60
		fDeadlineGoals[TXDL_DIST_GOOD_1] = 1780
		fDeadlineGoals[TXDL_TIME_GOOD_2] = 90
		fDeadlineGoals[TXDL_DIST_GOOD_2] = 900
		fDeadlineGoals[TXDL_TIME_GOOD_3] = 120 //TAXI_CONST_BONUS_TIME_SPEED_DEMON
		fDeadlineGoals[TXDL_DIST_GOOD_3] = 228
		CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Goals defined for 2nd dropoff")

	ENDIF
	
	
	fTimeToCheckDistanceLeft[0] = 70.050049
	fExpectedDistanceLeft[0]    = 3090.417480
	
	fTimeToCheckDistanceLeft[1] = 76.252930
	fExpectedDistanceLeft[1]    = 2929.083252
	
	fTimeToCheckDistanceLeft[2] = 82.693115
	fExpectedDistanceLeft[2]    = 2759.830322
	
	fTimeToCheckDistanceLeft[3] = 88.343994
	fExpectedDistanceLeft[3]    = 2576.061279
	
	fTimeToCheckDistanceLeft[4] = 94.978027
	fExpectedDistanceLeft[4]    = 2350.885986
	
	fTimeToCheckDistanceLeft[5] = 100.798096
	fExpectedDistanceLeft[5]    = 2177.442383
	
	fTimeToCheckDistanceLeft[6] = 106.467041	
	fExpectedDistanceLeft[6]    = 2016.681396	
	
	fTimeToCheckDistanceLeft[7] = 112.928955
	fExpectedDistanceLeft[7]    = 1839.417358
	
	fTimeToCheckDistanceLeft[8] = 118.420898
	fExpectedDistanceLeft[8]    = 1689.620483
	
	fTimeToCheckDistanceLeft[9] = 124.424072	
	fExpectedDistanceLeft[9]    = 1526.637939
		
	fTimeToCheckDistanceLeft[10] = 130.760010	
	fExpectedDistanceLeft[10]    = 1356.038818
	
	fTimeToCheckDistanceLeft[11] = 136.699951
	fExpectedDistanceLeft[11]    = 1184.012695
	
	fTimeToCheckDistanceLeft[12] = 142.572998
	fExpectedDistanceLeft[12]    = 1034.024048
	
	fTimeToCheckDistanceLeft[13] = 148.216064
	fExpectedDistanceLeft[13]    = 894.622498
	
	fTimeToCheckDistanceLeft[14] = 154.432129
	fExpectedDistanceLeft[14]    = 756.521912
		
	fTimeToCheckDistanceLeft[15] = 160.274902	
	fExpectedDistanceLeft[15]    = 669.363953
	
	fTimeToCheckDistanceLeft[16] = 166.250000	
	fExpectedDistanceLeft[16]    = 570.295349
		
	fTimeToCheckDistanceLeft[17] = 172.250977	
	fExpectedDistanceLeft[17]    = 521.976990
		
	fTimeToCheckDistanceLeft[18] = 178.605957	
	fExpectedDistanceLeft[18]    = 340.746582
		
	fTimeToCheckDistanceLeft[19] = 184.680908	
	fExpectedDistanceLeft[19]    = 224.309128
	
	fTimeToCheckDistanceLeft[20] = 190.708008
	fExpectedDistanceLeft[20]    = 126.953461
	
	fTimeToCheckDistanceLeft[21] = 193.06201
	fExpectedDistanceLeft[21]    = 81.804207
	
	myTaxiData.vTaxiOJ_WarpPtPickup = GET_TAXI_OJ_DL_VECTORS(eWarpPt)
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = GET_TAXI_OJ_DL_HEADINGS(eWarpPt) 
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = GET_TAXI_OJ_DL_VECTORS(TXOJ_POI_CASINO_WARPPT)
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = GET_TAXI_OJ_DL_HEADINGS(TXOJ_POI_CASINO_WARPPT)
	
	myTaxiData.vTaxiOJ_PassengerGoToPt = << 328.67575, 443.98456, 144.22983 >> //<< 926.2454, 44.1140, 79.8996 >>
ENDPROC

/// PURPOSE: Do all your taxi  string table, and anims and whatever we need for taxi oddjobs
///   
PROC REQUEST_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
	//Load text and UI
	REQUEST_MODEL(mPassengerModel)
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	TAXI_INIT_SHARED_STREAMS()
	CDEBUG1LN(DEBUG_OJ_TAXI,"Initial Taxi Oddjob Deadline Assets requested.")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
	//Load text and UI
	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Initial Taxi Oddjob Deadline Assets released.")
ENDPROC


//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    

FUNC BOOL HAVE_TAXI_OJ_DL_STAGE_01_ASSETS_LOADED()
	
	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading G_M_M_ChiGoon_02",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Deadline Stage 01 Assets Loaded")
	RETURN TRUE		
ENDFUNC



PROC LOCK_CASINO_DOORS_FOR_TAXI_ODDJOB()
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 929.6144, 46.3955, 80.0993 >>,TRUE,1.0, 50.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 928.6238, 44.9848, 80.0993 >>,TRUE,1.0, 50.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 928.1625, 43.7989, 79.8993 >>,TRUE,1.0, 50.0)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 927.4266, 42.1881, 80.0884 >>,TRUE,1.0, 50.0)
		
	CDEBUG1LN(DEBUG_OJ_TAXI,"[AIRLOCK]   --------      Casino Doors Have Been Stream Locked")	
ENDPROC


PROC SET_LOCK_STATE_OF_TAXI_ODDJOB_DEADLINE_CASINO_DOORS(BOOL bLockDoors = FALSE)
	IF bLockDoors 
		LOCK_CASINO_DOORS_FOR_TAXI_ODDJOB()
		
		//Left side casino doors
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 929.6144, 46.3955, 80.0993 >>,bLockDoors,0)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 928.6238, 44.9848, 80.0993 >>,bLockDoors,0)
		
		//Right side casino doors
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 928.1625, 43.7989, 79.8993 >>,bLockDoors,0)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 927.4266, 42.1881, 80.0884 >>,bLockDoors,0)
		CDEBUG1LN(DEBUG_OJ_TAXI,"[AIRLOCK]   --------      Casino Doors Have Been Locked")
		
	ELSE
		
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 929.6144, 46.3955, 80.0993 >>,bLockDoors,0.0, 50.0)
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 928.6238, 44.9848, 80.0993 >>,bLockDoors,0.0, 50.0)
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01L,<< 928.1625, 43.7989, 79.8993 >>,bLockDoors,0.0, 50.0)
		SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_CASINO_DOOR_01R,<< 927.4266, 42.1881, 80.0884 >>,bLockDoors,0.0, 50.0)
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"[AIRLOCK]   --------      Casino Doors Have Been UNLocked")
	ENDIF
	
ENDPROC

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
	#ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontAllowToBeDraggedOutOfVehicle, FALSE)
	ENDIF
	
	RELEASE_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
	
	LOCK_CASINO_DOORS_FOR_TAXI_ODDJOB()
	
	TERMINATE_THIS_THREAD()
ENDPROC



// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES()
	
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_03_DEADLINE)
	
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData, CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF, CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	myTaxiData.fTaxiEnterSpeed = PEDMOVE_RUN
	CDEBUG1LN(DEBUG_OJ_TAXI,"Current Dialogue line = ", tTaxiOJ_DQ_Data.iCurrentDQLine)
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Deadline")
		
		INIT_ODDJOB_TAXI_WIDGETS()
		
		ADD_WIDGET_BOOL("Show Deadline Stats", bDebugDrawDL_Stats)
	
		STOP_WIDGET_GROUP()
		
		INIT_TAXI_WIDGETS(taxiRideWidgets)
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Deadline  ~~~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()
	
	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugDrawDL_Stats
		//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		sDebugString[0] = "Compliments # "
		sDebugString[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_NumCompliments)
		DRAW_DEBUG_TEXT_2D(sDebugString[0],<<0.8,0.3,0.0>>)
		
		sDebugString[1] = "Disses # "
		sDebugString[1] +=TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_NumDisses)
		DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.80,0.32,0.0>>)
		
		sDebugString[2] = "Detla C - D=  "
		sDebugString[2] +=TAXI_UTILS_GET_STRING_FROM_INT_SP(myTaxiData.iTaxiOJ_NumCompliments - myTaxiData.iTaxiOJ_NumDisses)
		DRAW_DEBUG_TEXT_2D(sDebugString[2],<<0.80,0.34,0.0>>)			
	ENDIF
	
ENDPROC
#ENDIF


PROC TAXI_OJ_DL_SET_TIPS_AND_EXCITEMENT_TO_CHECK()

	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	
	//Exitement Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)
	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	//Start the mission timer here
	TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
ENDPROC

PROC ADD_TIME_TO_HOUR(TIMEOFDAY & eTimeToAddTo, INT iHour)
	IF ((GET_TIMEOFDAY_HOUR(eTimeToAddTo) + iHour) > 23)
		SET_TIMEOFDAY_HOUR(eTimeToAddTo, (GET_TIMEOFDAY_HOUR(eTimeToAddTo) + iHour) - 24)
	ELSE
		SET_TIMEOFDAY_HOUR(eTimeToAddTo, (GET_TIMEOFDAY_HOUR(eTimeToAddTo) + iHour))
	ENDIF
ENDPROC

PROC ADD_TIME_TO_MINUTE(TIMEOFDAY & eTimeToAddTo, INT iMinute)
	IF (GET_TIMEOFDAY_MINUTE(eTimeToAddTo) + iMinute) > 59
		SET_TIMEOFDAY_MINUTE(eTimeToAddTo, (GET_TIMEOFDAY_MINUTE(eTimeToAddTo) + iMinute) - 60)
		ADD_TIME_TO_HOUR(eTimeToAddTo, 1)
	ELSE
		SET_TIMEOFDAY_MINUTE(eTimeToAddTo, (GET_TIMEOFDAY_MINUTE(eTimeToAddTo) + iMinute))
	ENDIF
ENDPROC
//Tweak deadline time here.
//B*1311118 - adding 10 secs
FUNC TIMEOFDAY GET_TAXI_DEADLINE_TIMEOFDAY()
	
	TIMEOFDAY eCurrentTime
	eCurrentTime = GET_CURRENT_TIMEOFDAY()
	
	ADD_TIME_TO_HOUR(eCurrentTime, 3) //2)
	ADD_TIME_TO_MINUTE(eCurrentTime, CONST_iTaxi_DeadlineMinutes)
	
	RETURN eCurrentTime
ENDFUNC

FUNC TEXT_LABEL_7 GET_TAXI_HOURS_MINUTES_STRING(TIMEOFDAY eCurrentTime)
	
	TEXT_LABEL_7 sTimeToPrint
	TEXT_LABEL_3 sTimeHours
	TEXT_LABEL_3 sTimeMinutes
	
	sTimeHours = CONVERT_INT_TO_STRING(GET_TIMEOFDAY_HOUR(eCurrentTime))
	sTimeToPrint += sTimeHours
	
	sTimeToPrint += ":"
	
	IF GET_TIMEOFDAY_MINUTE(eCurrentTime) < 10
		sTimeToPrint += "0"
	ENDIF
	
	sTimeMinutes = CONVERT_INT_TO_STRING(GET_TIMEOFDAY_MINUTE(eCurrentTime))
	sTimeToPrint += sTimeMinutes
	
	RETURN sTimeToPrint
	
ENDFUNC

BOOL bTaxiTimeUp
BOOL bTaxi10SecWarning
BOOL bTaxiStopWarning
INT iTaxiBeepTimeStamp

//Displays the clock in the bottom right hand side of the screen and plays warning audio during last 10 seconds
PROC DRAW_DEADLINE_CLOCK()
	
//	iCurrentGameTimeInMilliseconds = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000
//	iCurrentGameTimeInMilliseconds += GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60
//
//	IF iCurrentGameTimeInMilliseconds > 719999 // IF the time is more than 11:59(am)
//		IF iCurrentGameTimeInMilliseconds > 779999 // IF the time is more than 12.59(pm)
//			DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds - 720000, "TIMER_TIME", 0, TIMER_STYLE_CLOCKPM) // show evrything from 13:00 to 23:59 - 12:00 = as 01:00(pm) to 11:59(pm)
//		ELSE // IF the time is more than 11:59(am) but less than 12.59(pm)
//			DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "TIMER_TIME", 0, TIMER_STYLE_CLOCKPM) // show as 24-hour clock (normal)
//		ENDIF
//	ELSE // ELSE the time is less than 12:00(pm)
//		IF iCurrentGameTimeInMilliseconds < 60000 // IF the time is less than 01:00(am)
//			DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds + 720000, "TIMER_TIME", 0, TIMER_STYLE_CLOCKAM) // show evrything from 00:00(am) to 00:59(am) + 12:00(am) = as 12:00(am) to 12.59
//		ELSE // IF the time is less than 12:00(pm) but more than 01.00(am)
//			DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "TIMER_TIME", 0, TIMER_STYLE_CLOCKAM) // show as 24-hour clock (normal)
//		ENDIF
//	ENDIF
//	DRAW_GENERIC_TIMER((iTimeLimitHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60)
//						+ (iTimeLimitMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000), "TIMER_TIME")
	
	INT iTimer = ROUND((fTimeLimit - GET_TIMER_IN_SECONDS_SAFE(DeadlineTimer))* 1000.0)
	
	IF iTimer < 0
		iTimer = 0
		bTaxiTimeUp = TRUE
		IF NOT bTaxiStopWarning
			PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
			bTaxiStopWarning = TRUE
		ENDIF
	ENDIF
	
	DRAW_GENERIC_TIMER(iTimer, "TIM_TIMER")
	
	IF NOT bTaxiTimeUp
		
		IF NOT bTaxi10SecWarning
			FLOAT fTimer = GET_TIMER_IN_SECONDS(DeadlineTimer)
			IF (fTimeLimit - fTimer) < 1.0
			OR (fTimeLimit - fTimer) < 1.5
			OR (fTimeLimit - fTimer) < 2.0
			OR (fTimeLimit - fTimer) < 2.5
			OR (fTimeLimit - fTimer) < 3.0
			OR (fTimeLimit - fTimer) < 3.5
			OR (fTimeLimit - fTimer) < 4.0
			OR (fTimeLimit - fTimer) < 4.5
			OR (fTimeLimit - fTimer) < 5.0 
			OR (fTimeLimit - fTimer) < 6.0
			OR (fTimeLimit - fTimer) < 7.0
			OR (fTimeLimit - fTimer) < 8.0
			OR (fTimeLimit - fTimer) < 9.0
			OR (fTimeLimit - fTimer) < 10.0
			OR (fTimeLimit - fTimer) < 11.0
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				CDEBUG1LN(DEBUG_OJ_TAXI,"playing beep now")
				bTaxi10SecWarning = TRUE
				iTaxiBeepTimeStamp = GET_GAME_TIMER()
			ENDIF
		ELSE
			FLOAT fTimer = GET_TIMER_IN_SECONDS(DeadlineTimer)
			IF ((fTimeLimit - fTimer) < 5.5 AND GET_GAME_TIMER() - iTaxiBeepTimeStamp > 500)
			OR ((fTimeLimit - fTimer) < 11.0 AND GET_GAME_TIMER() - iTaxiBeepTimeStamp > 1000)
				bTaxi10SecWarning = FALSE
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC


FUNC FLOAT GET_DYNAMIC_STOPPING_DISTANCE_FOR_TAXI()
	FLOAT fCurrentPlayerSpeed
	
	fCurrentPlayerSpeed = GET_ENTITY_SPEED(myTaxiData.viTaxi)
	
	IF fCurrentPlayerSpeed >= 10
		RETURN fCurrentPlayerSpeed * 0.5
	ENDIF
	
	RETURN 5.0
ENDFUNC

FUNC FLOAT EVALUATE_TAXI_OJ_DEADLINE_BEST_ETA()

	FLOAT fRetVal
	FLOAT fVehMaxSpeed 
	FLOAT fDistLeft
	
	fDistLeft = GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE)
	
	IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
		fVehMaxSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(myTaxiData.viTaxi)
		fVehMaxSpeed = fVehMaxSpeed * 0.65
	ENDIF	
	fRetVal = fDistLeft/fVehMaxSpeed
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_TAXI_OJ_DEADLINE_BEST_ETA: ETA = ", fRetVal)
	CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_TAXI_OJ_DEADLINE_BEST_ETA: fDistLeft = ", fDistLeft)
	CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_TAXI_OJ_DEADLINE_BEST_ETA: fVehMaxSpeed = ", fVehMaxSpeed)
	RETURN fRetVal
ENDFUNC

FUNC BOOL CAN_TAXI_OJ_DEADLINE_TAXI_GET_TO_DESTINATION_IN_TIME()
	
	FLOAT fTimeLeft
	
	fTimeLeft = fTimeLimit - GET_TIMER_IN_SECONDS_SAFE(DeadlineTimer)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"CAN_TAXI_OJ_DEADLINE_TAXI_GET_TO_DESTINATION_IN_TIME: Goal Time = ", fTimeLeft)
	RETURN (fTimeLeft > EVALUATE_TAXI_OJ_DEADLINE_BEST_ETA())
ENDFUNC

INT 	iFeedBackIterator
INT		iPosLines
INT		iNegLines
BOOL	bFirstFeedBackLineSaid
CONST_FLOAT	TAXI_OJ_DELTA_TIME			6.0
CONST_FLOAT	TAXI_OJ_FEEDBACK_WAIT_TIME	20.0

FUNC BOOL EVALUATE_NEW_TIME(TAXI_DIALOGUE_INDEX tdPositiveLine, TAXI_DIALOGUE_INDEX tdNegativeLine, FLOAT fGoodTime)
	IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_RIDETODEST)
//		CDEBUG1LN(DEBUG_OJ_TAXI,"YP fTimeToCheckDistanceLeft[X] = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
//		CDEBUG1LN(DEBUG_OJ_TAXI,"YP fExpectedDistanceLeft[X]    = ", GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE))
//		CDEBUG1LN(DEBUG_OJ_TAXI,"")
		IF iFeedBackIterator < NUM_TIME_DISTANCE_SAMPLES 
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) <= fTimeToCheckDistanceLeft[iFeedBackIterator] + TAXI_OJ_DELTA_TIME
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) >= fTimeToCheckDistanceLeft[iFeedBackIterator] - TAXI_OJ_DELTA_TIME
					CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: Current Time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST), " fTimeToCheckDistanceLeft[", iFeedBackIterator,"] = ", fTimeToCheckDistanceLeft[iFeedBackIterator])
					IF GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE) <= fExpectedDistanceLeft[iFeedBackIterator]
					AND iPosLines < NUM_POS_LINES
						CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: + Line Curr Dist = ",GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE), " fExpectedDistanceLeft[", iFeedBackIterator,"] = ", fExpectedDistanceLeft[iFeedBackIterator])
				        
						IF NOT bFirstFeedBackLineSaid
							CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: - + FIRST Line")
							iFeedBackIterator++						
							iPosLines++
							SET_NEXT_TAXI_SPEECH(myTaxiData,tdPositiveLine,TRUE)							
						
							RESTART_TIMER_NOW(localBanterDelay)
							bFirstFeedBackLineSaid = TRUE
						ENDIF
						
						IF IS_TIMER_STARTED(localBanterDelay)
							IF GET_TIMER_IN_SECONDS(localBanterDelay) > TAXI_OJ_FEEDBACK_WAIT_TIME
								CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: + Line")
								iFeedBackIterator++						
								iPosLines++
								SET_NEXT_TAXI_SPEECH(myTaxiData,tdPositiveLine,TRUE)
								
								CANCEL_TIMER(localBanterDelay)
								RESTART_TIMER_NOW(localBanterDelay)
							ENDIF	
						ENDIF
						RETURN TRUE
					ELIF GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE) > fExpectedDistanceLeft[iFeedBackIterator]
					AND iNegLines < NUM_NEG_LINES
						CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: - Line Curr Dist = ",GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE), " fExpectedDistanceLeft[", iFeedBackIterator,"] = ", fExpectedDistanceLeft[iFeedBackIterator])

						IF NOT bFirstFeedBackLineSaid
							CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: FIRST - Line")
							iFeedBackIterator++
							iNegLines++
							SET_NEXT_TAXI_SPEECH(myTaxiData,tdNegativeLine,TRUE)						
						
							RESTART_TIMER_NOW(localBanterDelay)
							bFirstFeedBackLineSaid = TRUE
						ENDIF
												
						IF IS_TIMER_STARTED(localBanterDelay)
							IF GET_TIMER_IN_SECONDS(localBanterDelay) > TAXI_OJ_FEEDBACK_WAIT_TIME
								CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: - Line")
								iFeedBackIterator++
								iNegLines++
								SET_NEXT_TAXI_SPEECH(myTaxiData,tdNegativeLine,TRUE)
								
								CANCEL_TIMER(localBanterDelay)
								RESTART_TIMER_NOW(localBanterDelay)
							ENDIF	
						ENDIF						
						RETURN TRUE
					ENDIF
				ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) > fTimeToCheckDistanceLeft[iFeedBackIterator] + TAXI_OJ_DELTA_TIME
					iFeedBackIterator++
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE // all distance iterations have been checked, player should be getting there or else just say bad lines
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) >= fGoodTime
			AND GET_PLAYER_DISTANCE_FROM_LOCATION(myTaxiData.vTaxiOJDropoff, FALSE) > 50
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_TIMER_STARTED(endBanterDelay)
						CDEBUG1LN(DEBUG_OJ_TAXI,"starting end banter delay")
						RESTART_TIMER_NOW(endBanterDelay)
					ELIF GET_TIMER_IN_SECONDS(endBanterDelay) > 4.0
						CDEBUG1LN(DEBUG_OJ_TAXI,"EVALUATE_NEW_TIME: - Line 3")
						SET_NEXT_TAXI_SPEECH(myTaxiData,tdNegativeLine,TRUE)
					ENDIF
				ELSE
					IF IS_TIMER_STARTED(endBanterDelay)
						CDEBUG1LN(DEBUG_OJ_TAXI,"canceling end banter delay")
						CANCEL_TIMER(endBanterDelay)
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

//TODO none of these values have been tweaked yet.
PROC UPDATE_PROGRESS()
	
	IF (ROUND(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)*1000) % 10000) < 50
		CDEBUG1LN(DEBUG_OJ_TAXI,"########################### DEADLINE CHECK")
		CDEBUG1LN(DEBUG_OJ_TAXI,"Time driving passenger    = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
		CDEBUG1LN(DEBUG_OJ_TAXI,"Current Mileage           = ", myTaxiData.fTaxiOJ_CurrentMileage)
		CDEBUG1LN(DEBUG_OJ_TAXI,"Distance from Destination = ", GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff))
		CDEBUG1LN(DEBUG_OJ_TAXI,"########################### DEADLINE CHECK")
	ENDIF
	
	IF NOT EVALUATE_OVERALL_TIME(myTaxiData, TAXI_DI_TIME_BAD, fDeadlineGoals[TXDL_OVERALL_GOAL_TIME])
		SWITCH iRideProgress
			//Init Good Time Vals for Debug Info
			CASE 0
				SETUP_TAXI_OJ_DEADLINE_GOALS()
				myTaxiData.fTaxiOJ_TimeGood = fDeadlineGoals[TXDL_TIME_GOOD_1]
				myTaxiData.fTaxiOJ_DistanceGood = fDeadlineGoals[TXDL_DIST_GOOD_1]
				iRideProgress++
				CDEBUG1LN(DEBUG_OJ_TAXI,"Progress Taxi State = 1")
				
			BREAK
			
			CASE 1
				EVALUATE_NEW_TIME(TAXI_DI_TIME_FAST, TAXI_DI_TIME_BAD, myTaxiData.fTaxiOJ_TimeGood)
			BREAK
		ENDSWITCH
		
	ENDIF
ENDPROC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_DL_LINES()
	//Trigger lines
	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine

			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_DL_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO CASINO has been assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO CASINO has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
//			CASE 1
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
//				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
//						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
//						IF g_bDebug
//							SCRIPT_ASSERT("Triggering Banter 1")
//						ENDIF
//					
//				ENDIF
//			BREAK
			
			CASE 1 //2
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter 2")
						ENDIF
					
				ENDIF
			BREAK
		
			
			CASE 2 //3
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(8.0,14.0)
					//Set Radio Station Check
					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
						tTaxiOJ_DQ_Data.iCurrentDQLine++
						IF g_bDebug
							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)
	
ENDPROC
/*

oooo     oooo      o      ooooo oooo   oooo 
 8888o   888      888      888   8888o  88  
 88 888o8 88     8  88     888   88 888o88  
 88  888  88    8oooo88    888   88   8888  
o88o  8  o88o o88o  o888o o888o o88o    88  
                                           
*/

PROC Main_Taxi_OJ_Deadline()
	
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			IF TAXI_HANDLE_FAIL(myTaxiData)
				Script_Failed()
			ENDIF
		ELSE
	
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
				SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
				Script_Cleanup()
			ENDIF
		
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)  
		
		UPDATE_PROGRESS()
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		
		
		//Track Driver progress
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			//TAXI_DL_MONITOR_PASSENGER_HAPPINESS_AND_TIPS()
			TAXI_ODDJOB_HANDLE_TAXI_BLIPPING(myTaxiData)
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_DL_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF				

		ENDIF
		
		//================================================================================================
	
		
		SWITCH myTaxiData.tTaxiOJ_RideState
		
/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
						
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o    
*/
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_DL_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, GET_TAXI_OJ_DL_VECTORS(eSpawnPt), GET_TAXI_OJ_DL_VECTORS(ePickupPt), "TaxiKwak",mPassengerModel,180.6)
					
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
				
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_DL_STAGE_01_ASSETS_LOADED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					//Init Bonus----------------------------------------
					TAXI_INITIALIZE_BONUS_FIELD(bonusFieldDeadline[TDL_BONUS_SPEEDDEMON], "TAXI_SC_BN_03", TAXI_CONST_BONUS_CASH_SPEED_DEMON)			//Completed quickly
					
					//Init Finalized
					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldDeadline)					
			
					//SET_LOCK_STATE_OF_TAXI_ODDJOB_DEADLINE_CASINO_DOORS(TRUE)
					
					//Setting Pickup Pt
					myTaxiData.vTaxiOJPickup = GET_TAXI_OJ_DL_VECTORS(eSpawnPt)
				
					//SET REACT BITS
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF
	
			BREAK
			
			//Creates the passenger and sets objective to "PICKUP PASSENGER"---------------------------------------
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, INT_TO_ENUM(PED_COMPONENT,8), 1, 1, 0) //(accs)
						
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 0, 0)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 0,2)
//						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HEAD, 0,0)
					ENDIF
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)

					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
			
				ENDIF
			BREAK
			
			//Waits for passenger to head to cab
			CASE TRS_MANAGE_PICKUP
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					
					myTaxiData.vTaxiOJDropoff =  vNewDLDropoff //GET_TAXI_OJ_DL_VECTORS(TXOJ_POI_CASINO)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"@@@@@@@@@@ TIME Picked up.  Time hour = ", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), " Timer minutes = ", GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
					CDEBUG1LN(DEBUG_OJ_TAXI,"@@@@@@@@@@ TIME Picked up.  Time = ", Get_String_From_TIMEOFDAY(GET_CURRENT_TIMEOFDAY()))
					
					eDeadline = GET_TAXI_DEADLINE_TIMEOFDAY()
					
					sDeadlineTime = GET_TAXI_HOURS_MINUTES_STRING(eDeadline)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"@@@@@@@@@@ Deadline = ", Get_String_From_TIMEOFDAY(eDeadline))
					CDEBUG1LN(DEBUG_OJ_TAXI,"@@@@@@@@@@ Deadline string = ", sDeadlineTime)
					
					myTaxiData.sFastTime = sDeadlineTime
					
					//iTimeLimitHour = GET_TIMEOFDAY_HOUR(eDeadline)
					//iTimeLimitMin = GET_TIMEOFDAY_MINUTE(eDeadline)
					
					fTimeLimit = 210//165.0
					START_TIMER_NOW(DeadlineTimer)
					
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_OBJ,TRUE)
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					//Set all tip checks on
					TAXI_OJ_DL_SET_TIPS_AND_EXCITEMENT_TO_CHECK()
				
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK
			
			//Handles the driving, waits for you to reach the destination, than selects one of 3 exit scenearios
			//1. Regular Pay & Thank
			//2. Forgot something in the backseat
			//3. Run out and try to stiff the player
			/* DRIVING PASSENGER
	
ooooooooo  oooooooooo  ooooo ooooo  oooo ooooo oooo   oooo  ooooooo8  
 888    88o 888    888  888   888    88   888   8888o  88 o888    88  
 888    888 888oooo88   888    888  88    888   88 888o88 888    oooo 
 888    888 888  88o    888     88888     888   88   8888 888o    88  
o888ooo88  o888o  88o8 o888o     888     o888o o88o    88  888ooo888  
*/
			CASE TRS_DRIVING_PASSENGER
				
				DRAW_DEADLINE_CLOCK()
				
				IF GET_TIMER_IN_SECONDS_SAFE(DeadlineTimer) > fTimeLimit + DEADLINE_FAIL_LEEWAY
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TAXI_SET_FAIL(myTaxiData, "Player did not hit the deadline", TFS_TAXI_STOPPED)
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF
				ENDIF
				
				IF TAXI_HANDLE_DRIVING(myTaxiData,/*tTaxiOJ_DQ_Data, */GET_DYNAMIC_STOPPING_DISTANCE_FOR_TAXI(), 7, 30) //12,60)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,CONST_TAXI_OJ_LINE_NUM_TO_CLOSE_DIALOGUE_Q,TRUE)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"dead line timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
						
						//Remove destination blip
						REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
						
						//Ludicrous Speed Bonus
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) < fDeadlineGoals[TXDL_TIME_GOOD_3]//TAXI_CONST_BONUS_TIME_SPEED_DEMON
						OR myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing
							TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TDL_BONUS_SPEEDDEMON))

						ENDIF
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"##### TT_RIDETODEST = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
						CDEBUG1LN(DEBUG_OJ_TAXI,"##### TIME dropped off.  Time hour = ", GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()), " Timer minutes = ", GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()))
						
						IF GET_TIMER_IN_SECONDS_SAFE(DeadlineTimer) > 210
							myTaxiData.iTaxiOJ_CashTip = 0
						ENDIF
						
						//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$CASH
						SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
						TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
						CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
						ENDIF
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
						
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF
				ENDIF
			BREAK
			
			//Regular Payment
			CASE TRS_REGULAR_PAYMENT
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData, TRUE)//FALSE)
						
					SET_LOCK_STATE_OF_TAXI_ODDJOB_DEADLINE_CASINO_DOORS(FALSE)
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				        IF ARE_VECTORS_EQUAL(NULL_VECTOR(),myTaxiData.vTaxiOJ_PassengerGoToPt)
				            SET_ENTITY_HEADING(myTaxiData.piTaxiPassenger, 84.9058)
				            SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
						ELSE
							CLEAR_SEQUENCE_TASK(siTemp)
							OPEN_SEQUENCE_TASK(siTemp)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_CLEAR_LOOK_AT(NULL)
				                TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,myTaxiData.vTaxiOJ_PassengerGoToPt,PEDMOVEBLENDRATIO_RUN)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 322.1072, 442.4288, 140.6772 >>,  PEDMOVEBLENDRATIO_RUN)//<< 935.9512, 36.1616, 79.8993 >>, PEDMOVEBLENDRATIO_RUN)
								TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE")
							CLOSE_SEQUENCE_TASK(siTemp)
							TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
							
							SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger,TRUE)
						ENDIF
					
					ENDIF
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
					
				ENDIF				

			BREAK
			
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
//					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, << 931.7672, 40.1704, 79.8993 >>) < 3.0 //<< 935.3044, 37.0121, 79.8993 >>) < 3.0
//					OR GET_SEQUENCE_PROGRESS(myTaxiData.piTaxiPassenger) > 3
//					OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_GENERIC) > 5
					#IF IS_DEBUG_BUILD
					OR bDebugSucceed
					#ENDIF
					
						//In the off chance the player might get himself locked inside, just don't lock the doors.
//						IF GET_PLAYER_DISTANCE_FROM_LOCATION(<< 931.7672, 40.1704, 79.8993 >>) > 5.0
//							//SET_LOCK_STATE_OF_TAXI_ODDJOB_DEADLINE_CASINO_DOORS(TRUE)
//						ENDIF
						
						TAXI_MISSION_END(TRUE,myTaxiData)	
						//Move on to the next stage
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
//					ENDIF
				ENDIF
			
			BREAK
			
			CASE TRS_CLEANUP
				Script_Cleanup()
			BREAK
			
		ENDSWITCH
	ENDIF

ENDPROC
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
		
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD	
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
				//If you J Skip, check the timer and add some tip
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) < fDeadlineGoals[TXDL_TIME_GOOD_3]// TAXI_CONST_BONUS_TIME_SPEED_DEMON
					SET_TAXI_TIP_TO_AMAZING(myTaxiData)
				ENDIF
			ENDIF
		ENDIF
		
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			bDebugSucceed = TRUE
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF	
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
			PROCESS_WIDGETS()
		ENDIF
	#ENDIF
	//END DEBUG----------------------------------------------------
		
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_Deadline()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

//Test stuff

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
