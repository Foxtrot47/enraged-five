///    Taxi_Scorecard.sch
///    Author : John R. Diaz
///    Written : May 23, 2011
///    This handles the rendering & input of the scorecard
///    

// -----------------------------------------------------------
// EDITS:
// 8/27/2011: Ryan Paradis
//		- Removed everything associated with the old scorecard method. This includes CONSTs and the HUD_2D_* items. We moved to the new
//			minigame stats tracker scorecard. Much better looking. Also cleaned up the timer on ending the mission so that we don't
//			have a WHILE loop internal to the SWITCH. Loads cleaner as a result.

USING "globals.sch"
USING "hud_text.sch"
USING "minigames_helpers.sch"

ENUM TAXI_SCORECARD_STATES
	TSC_INIT = 0,
	//TSC_GRADE_PICKUP,
	TSC_GRADE_DROPOFF,
	TSC_FARE,
	TSC_TIPS,
	TSC_BONUS,
	TSC_RANK,
	TSC_TALLY,
	TSC_FINISH,
	TSC_NUM_STATES
ENDENUM


STRUCT TAXI_SCORECARD
	TAXI_MISSION_TYPES	eMissionType
	
	FLOAT fFare
	FLOAT fTips
	FLOAT fSpecialCash
	BONUS_FIELD iCashBonusInfo[MAX_BONUS_INFO]
	FLOAT fTotalMoneys
	
ENDSTRUCT
//EOF
