
USING "commands_audio.sch"
USING "commands_misc.sch"
USING "Taxi_dialogue_lib.sch"
USING "Taxi_Radio_Control.sch"

PROC TAXI_RADIO_STATION_TURN_ON(TaxiStruct &myTaxiData,BOOL bDEbug = FALSE)
	myTaxiData.bTaxiOJ_CheckRadioStation = TRUE
	IF bDEbug
		SCRIPT_ASSERT("Taxi Radio Check turned on")
	ENDIF
ENDPROC

PROC TAXI_RADIO_STATION_TURN_OFF(TaxiStruct &myTaxiData)
	myTaxiData.bTaxiOJ_CheckRadioStation = FALSE
ENDPROC

FUNC BOOL DID_TAXI_RADIO_HIT_FAV_STATION(TaxiStruct &myTaxiData)
	RETURN myTaxiData.tTaxiOJ_Radio.bHitFav
ENDFUNC

FUNC BOOL GET_TAXI_RADIO_CHECK_FLAG(TaxiStruct &myTaxiData)
	RETURN myTaxiData.bTaxiOJ_CheckRadioStation
ENDFUNC

FUNC TAXI_RADIO_STATIONS_INDEX GET_TAXI_RADIOSTATION()
	CDEBUG1LN(DEBUG_OJ_TAXI,"Player is listening to Radio Station Index # ","",GET_PLAYER_RADIO_STATION_INDEX())
	RETURN INT_TO_ENUM(TAXI_RADIO_STATIONS_INDEX,GET_PLAYER_RADIO_STATION_INDEX())
ENDFUNC 

FUNC BOOL HAS_TAXIRADIO_HIT_LEAST_FAV_STATION(TaxiStruct &myTaxiData)
	RETURN myTaxiData.tTaxiOJ_Radio.bHitLeastFav
ENDFUNC

PROC SET_TAXIRADIO_FAV_STATION(TaxiStruct &myTaxiData, TAXI_RADIO_STATIONS_INDEX trsFavoriteStation = TRS_0_CLASS_ROCK)
	myTaxiData.tTaxiOJ_Radio.iLikedStation = ENUM_TO_INT(trsFavoriteStation)
	CDEBUG1LN(DEBUG_OJ_TAXI,"Favorite Taxi Oddjob station for passenger has been set to ","", myTaxiData.tTaxiOJ_Radio.iLikedStation  )
ENDPROC

PROC SET_TAXIRADIO_DISLIKED_STATION(TaxiStruct &myTaxiData, TAXI_RADIO_STATIONS_INDEX trsDislikedStation = TRS_15_SILVERLAKE)
	myTaxiData.tTaxiOJ_Radio.iDislikedStation = ENUM_TO_INT(trsDislikedStation)
	CDEBUG1LN(DEBUG_OJ_TAXI,"Disliked Taxi Oddjob station for passenger has been set to ","", myTaxiData.tTaxiOJ_Radio.iDislikedStation  )
ENDPROC

PROC SET_TAXIRADIO_STATION_CATEGORY(TaxiStruct &myTaxiData)
	SWITCH INT_TO_ENUM(TAXI_RADIO_STATIONS_INDEX, myTaxiData.tTaxiOJ_Radio.iLikedStation)
		CASE TRS_2_HIPHOP_NEW
		CASE TRS_8_HIPHOP_OLD
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_RAP) 
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_RAP")
		BREAK
		
		CASE TRS_0_CLASS_ROCK
		CASE TRS_3_PUNK
		CASE TRS_5_COUNTRY
		CASE TRS_9_SURF
		CASE TRS_15_SILVERLAKE
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_ROCK)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_ROCK")
		BREAK
		
		CASE TRS_4_TALK_01
		CASE TRS_10_TALK_02
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_TALK)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_TALK")
		BREAK
		
		CASE TRS_1_POP
		CASE TRS_6_DANCE_01
		CASE TRS_13_DANCE_02
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_DANCE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_DANCE")
		BREAK
		
		CASE TRS_11_REGGAE
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_REGGAE)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_REGGAE")
		BREAK
		
		CASE TRS_12_JAZZ
		CASE TRS_14_MOTOWN
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_SOUL)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_SOUL")
		BREAK
		
		CASE TRS_7_MEXICAN
			myTaxiData.tTaxiOJ_Radio.iCategory = ENUM_TO_INT(TRC_MEX)
			CDEBUG1LN(DEBUG_OJ_TAXI,"SET_TAXIRADIO_STATION_CATEGORY set to TRC_MEX")
		BREAK
	ENDSWITCH
ENDPROC

PROC INIT_TAXI_OJ_RADIO_STATION_PREFERENCES_PER_MISSION(TaxiStruct &myTaxiData)
	
	INT iTemp 
	
	SWITCH myTaxiData.tTaxiOJ_MissionType
		
		CASE TXM_01_NEEDEXCITEMENT
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_3_PUNK)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_14_MOTOWN)
		BREAK
		
		CASE TXM_02_TAKEITEASY
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_14_MOTOWN)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_8_HIPHOP_OLD)
		BREAK
		
		CASE TXM_03_DEADLINE
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_8_HIPHOP_OLD)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_7_MEXICAN)
		BREAK

		CASE TXM_04_GOTYOURBACK
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_15_SILVERLAKE)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_6_DANCE_01)
		BREAK
		
		CASE TXM_05_TAKETOBEST
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_0_CLASS_ROCK)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_3_PUNK)
		BREAK
			
		CASE TXM_07_CUTYOUIN
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_6_DANCE_01)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_7_MEXICAN)
		BREAK

		CASE TXM_08_GOTYOUNOW
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_8_HIPHOP_OLD)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_15_SILVERLAKE)
		BREAK
				
		CASE TXM_09_CLOWNCAR
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_8_HIPHOP_OLD)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_14_MOTOWN)
		BREAK

		CASE TXM_10_FOLLOWTHATCAR
			SET_TAXIRADIO_FAV_STATION(myTaxiData,TRS_7_MEXICAN)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData,TRS_15_SILVERLAKE)
		BREAK
		
		CASE TXM_PROCEDURAL
			SET_TAXIRADIO_FAV_STATION(myTaxiData, INT_TO_ENUM(TAXI_RADIO_STATIONS_INDEX, GET_RANDOM_INT_IN_RANGE(0, 17)))
			CDEBUG1LN(DEBUG_OJ_TAXI,"Fave Radio station for procedural is ", myTaxiData.tTaxiOJ_Radio.iLikedStation)
			iTemp = IWRAP((myTaxiData.tTaxiOJ_Radio.iLikedStation + GET_RANDOM_INT_IN_RANGE(1, 17)), 0, 16)
			SET_TAXIRADIO_DISLIKED_STATION(myTaxiData, INT_TO_ENUM(TAXI_RADIO_STATIONS_INDEX, iTemp))
			CDEBUG1LN(DEBUG_OJ_TAXI,"Hated Radio station for procedural is ", myTaxiData.tTaxiOJ_Radio.iDislikedStation)
			SET_TAXIRADIO_STATION_CATEGORY(myTaxiData)
			myTaxiData.tTaxiOJ_Radio.bReactNeutral = TRUE
			myTaxiData.tTaxiOJ_Radio.bGiveHint = TRUE
		BREAK
	ENDSWITCH

ENDPROC
PROC RUN_TAXIRADIO_UPDATE(TaxiStruct &myTaxiData)

	IF IS_RADIO_RETUNING()
	AND myTaxiData.tTaxiOJ_Radio.iSwitch != 0
		myTaxiData.tTaxiOJ_Radio.iSwitch = 0
	ENDIF
	
	SWITCH myTaxiData.tTaxiOJ_Radio.iSwitch
		
		//Reacquires radio station if ever retuned
		CASE 0
			myTaxiData.tTaxiOJ_Radio.iCurrStation = GET_PLAYER_RADIO_STATION_INDEX()
			TAXI_CANCEL_TIMERS(myTaxiData, TT_DELAY)
			CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi radio is ","",myTaxiData.tTaxiOJ_Radio.iCurrStation, "", " Which is called ","",GET_PLAYER_RADIO_STATION_NAME())
			myTaxiData.tTaxiOJ_Radio.iSwitch ++
		BREAK
		
		//Checks to see if the radio station gets set to one that is liked or disliked.
		CASE 1
			IF myTaxiData.tTaxiOJ_Radio.bGiveHint
			AND NOT (myTaxiData.tTaxiOJ_Radio.iCurrStation = myTaxiData.tTaxiOJ_Radio.iLikedStation)
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 3.0
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RADIO_HINT, TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Radio station hint given")
					myTaxiData.tTaxiOJ_Radio.bGiveHint = FALSE
					myTaxiData.tTaxiOJ_Radio.iSwitch ++
				ENDIF
			
			//Matches HATED
			ELIF myTaxiData.tTaxiOJ_Radio.iCurrStation = myTaxiData.tTaxiOJ_Radio.iDislikedStation
				IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE)
				OR NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE2)
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 5.0	
						myTaxiData.tTaxiOJ_Radio.bHitLeastFav = TRUE
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RADIO_CHANGE,TRUE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Radio stations matches the one of the ones that the passenger hates")
						myTaxiData.tTaxiOJ_Radio.iSwitch ++
					ENDIF
				ENDIF
			//Matches FAVORITE
			ELIF myTaxiData.tTaxiOJ_Radio.iCurrStation = myTaxiData.tTaxiOJ_Radio.iLikedStation
				IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RADIO_LIKE)
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 5.0	
						myTaxiData.tTaxiOJ_Radio.bHitFav = TRUE
						myTaxiData.tTaxiOJ_Radio.bGiveHint = FALSE
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RADIO_LIKE,TRUE)
						TAXI_CANCEL_TIMERS(myTaxiData, TT_RDSTADDTL)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Radio stations matches the one of the ones that the passenger loves")
						myTaxiData.tTaxiOJ_Radio.iSwitch ++
					ENDIF
				ENDIF
			//Neutral situation
			ELIF myTaxiData.tTaxiOJ_Radio.bReactNeutral
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 8.0
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_RADIO_CHANGE_NEUTRAL, TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Radio station is neutral, making a comment about it")
					myTaxiData.tTaxiOJ_Radio.iSwitch ++
				ENDIF
			ENDIF
			
		BREAK
		
		//Sits in here until player retunes
		//If it's NEUTRAL station CASE 2 will never be hit.
		CASE 2
			IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE) 
			OR IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RADIO_LIKE) 
				IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_RDSTADDTL)
					TAXI_START_TIMER(myTaxiData, TT_RDSTADDTL)
				ENDIF
			ENDIF
			
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_RDSTADDTL)
			AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RDSTADDTL) > 15.0
				IF myTaxiData.tTaxiOJ_Radio.iCurrStation = myTaxiData.tTaxiOJ_Radio.iDislikedStation
					//Will only trigger once
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_RADIO_CHANGE2)
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RADIO_CHANGE,TRUE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"RUN_TAXIRADIO_UPDATE: Passenger complaines that radio ch was not changed!")
						TAXI_CANCEL_TIMERS(myTaxiData, TT_RDSTADDTL)
					ENDIF
				ELIF myTaxiData.tTaxiOJ_Radio.iCurrStation = myTaxiData.tTaxiOJ_Radio.iLikedStation
					//Will only trigger once
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_RADIO_LIKE2)
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RADIO_LIKE,TRUE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"RUN_TAXIRADIO_UPDATE: Passenger compliments the radio station!")
						TAXI_CANCEL_TIMERS(myTaxiData, TT_RDSTADDTL)
					ENDIF
				ELSE
//					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RADIO_CHANGE_NEUTRAL,TRUE)
//					TAXI_CANCEL_TIMERS(myTaxiData, TT_RDSTADDTL)
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH

ENDPROC
PROC RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(TaxiStruct &myTaxiData)
	IF GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)
		RUN_TAXIRADIO_UPDATE(myTaxiData)
	ENDIF
ENDPROC
//EOF
