//Taxi_AI_Car.sch
//Author: John R. Diaz
//
USING "Commands_Task.sch"
//USING "Taxi_HUD.sch"

TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_FAR_DIST_WARN	 			120.0		//How far away the taxi has to be from the followee to consider being too far and out of sight
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_FAR_DIST					150.0		//How far away the taxi has to be from the followee to consider being too far and out of sight
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST 				22.5 //30.0	//How far away the taxi has to be from the followee to consider being too far and out of sight
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_ESCAPEE_STOPPED_TOO_CLOSE_DIST 13.0 //17.5
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_CLOSE_FAIL_NOW_DIST 		15.0		//How close the taxi has to be from the followee to consider being too close and spotted
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_FAR_FAIL_DIST 			300.0		//How far away the taxi has to be from the followee to consider being too far and out of sight
TWEAK_FLOAT		TWEAK_TAXI_FOLLOW_TOO_FAR_FAIL_NOW_DIST 		480.0		//How far away the taxi has to be from the followee to consider being too far and out of sight
CONST_FLOAT		CONST_TAXI_HIT_FOLLOW_CAR_DELAY					5.0
CONST_INT		TAXI_FOLLOW_CAR_HITS_BEFORE_FAIL				1
CONST_INT		CONST_TAXI_TIME_CHECK_HIT						100
CONST_FLOAT 	TAXI_CONST_TIME_BEFORE_CHECK_DIST				10.0
CONST_FLOAT		CONST_TAXI_OJ_HEALTH_STOP_CAR_THRESHOLD			350.0
CONST_INT		TAXI_CONST_STOP_CAR_START_HEALTH				800
CONST_FLOAT		CONST_TAXI_OJ_STOP_CAR_SPEED_STUCK				5.0
CONST_FLOAT		CONST_TAXI_OJ_STOP_CAR_STUCK_TIME				10.0

//FollowCar
CONST_FLOAT		TAXI_TOO_CLOSE_DIST								50.0		//how close you can be to your target before getting warned that you're going to give away your cover
CONST_FLOAT		TAXI_TOO_FAR_DIST								100.0		//how far you can be to your target before getting warned that you're losing them
CONST_FLOAT		TAXI_JUST_RIGHT_DIST							50.0
CONST_FLOAT		TAXI_MIN_INTERRUPT_TIME							15.0
CONST_FLOAT		TAXI_MAX_INTERRUPT_TIME							25.0
CONST_FLOAT		TAXI_FOLLOW_CRUISE_SPEED						25.0
CONST_FLOAT		TAXI_FOLLOW_TOOCLOSE_TIME						45.0
CONST_FLOAT		TAXI_FOLLOW_LOST_TIME							60.0
CONST_INT		TAXI_GOD_TEXT_DISPLAY							4000
CONST_INT		CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL			3
CONST_INT		CONST_TAXI_FOLLOW_NUM_TOO_FAR_FAIL				3
CONST_INT		CONST_TAXI_FOLLOW_NUM_TOO_FAR_FAIL_WARN			2
CONST_FLOAT		TAXI_CONST_TIME_SWEET_SPOT_DELAY				15.0 //30.0

ENUM CHASE_CAMERA_POSITION
	CCP_CAM_FRONT,
	CCP_CAM_BEHIND,
	CCP_CAM_LEFT,
	CCP_CAM_RIGHT
ENDENUM

ENUM TAXI_ESCAPEE_STATE
	TES_INVALID,
	TES_INIT,
	TES_DRIVING,
	TES_STOPPING,
	TES_EXITING,
	TES_ON_FOOT,
	TES_DEAD
ENDENUM

STRUCT escapeStruct

	MODEL_NAMES escapeeModel
	MODEL_NAMES escapeeCarModel

	PED_INDEX piPed
	
	VEHICLE_INDEX viCar
		
	BLIP_INDEX bBlip
	
	INT iStartHealth
	INT iNumTimesHitByPlayer = 0
	INT iNumTimesOutofSight	=	0
	INT iNumTimesTooClose	=	0
	
	INT iNumTimesWarned = 0
	
	FLOAT fCarHeading
	FLOAT fHeading
	FLOAT fHealthFactor
	FLOAT fBailFactor
	FLOAT fCruiseSpeed
	FLOAT fCurrentEngHealth
	FLOAT fPetrolHealth
	VECTOR vCarSpawnPt
	
	BOOL bIsFollowMode
	//BOOL bIsStopMode
	BOOL bIsOutofSight
	BOOL bIsTooClose
	BOOL bIsSweetSpot
	BOOL bIsSweetSpotBanter
	BOOL bReTaskToAVOIDCARS
	BOOL bReTaskToSTOPFORCARS
	structTimer closeTimer

	REL_GROUP_HASH relEscapee

	TAXI_ESCAPEE_STATE tesState
	SCRIPT_TASK_NAME stn_CurrentTask

ENDSTRUCT

//EOF
