//USING "globals.sch"

//Adding in for stats 4/25/11 - JDiaz
ENUM TAXI_STATS_INFO
	//Fares										//		
	TAXI_STAT_FARES_COMPLETED = 0,				//		iNumFaresCompleted
	TAXI_STAT_FARES_FAILED,						//		iNumFaresFailed
	TAXI_STAT_FARES_ACCEPTED,					//		iNumTotalFaresAccepted
	TAXI_STAT_FARES_EXPIRED,					//		iNumTotalFaresExpired
	//TAXI_STAT_FARES_FLAWLESS,					//		iNumFlawlessFares
												//	
	//Distance
	//TAXI_STAT_DIST_SHORTEST,					//		iShortestDistance
	TAXI_STAT_DIST_LONGEST,						//		iLongestDistance
	//TAXI_STAT_DIST_AVG,							//		fAvgFareDistance
	TAXI_STAT_DIST_TOTAL_W_PASSENGER,			//		fTotalDistanceTraveledWithPassenger
	//TAXI_STAT_DIST_TOTAL,						//		fTotalDistanceTraveled
												//
	//Wanted Levels
	TAXI_STAT_WANTED_GAINED,					//		iNumWantedLevelGained
	TAXI_STAT_WANTED_LOST,						//		iNumWantedLevelLost
												//	
	//Violence/Carelessness/Fun
	TAXI_STAT_FUN_NUM_WRECKED,					//		iNumTaxisWrecked
	//TAXI_STAT_FUN_NUM_PEDS_KILLED,				//		iNumPedsKilledInTaxi
	TAXI_STAT_NUM_HORN_HONKS,					//	
	//	//Money
	TAXI_STAT_MONEY_TOTAL,						//	 	fTotalMoneyEarned
	TAXI_STAT_MONEY_TIPS,
	TAXI_STAT_MONEY_HI_TIP,						//		iHighestTip
	//TAXI_STAT_MONEY_LO_TIP,						//		iLowestTip
												//	
	//	//Time
	//TAXI_STAT_TIME_TOTAL,
	//TAXI_STAT_TIME_PICKUP,
	//TAXI_STAT_TIME_AVG_FARE,					//		fAvgFareTime
	//TAXI_STAT_TIME_AVG_PICKUP,					//		fAvgPickupTime

	//Lost & Found Items
	//TAXI_STAT_ITEMS_KEPT,						//		iNumLostItemsKept
	//TAXI_STAT_ITEMS_RTURNED,					//		iNumLostItemsReturned
												//	
	//Get Paid the Hard Way
	TAXI_STAT_PASS_RUN,							//		iNumPassengersRun
	TAXI_STAT_PASS_PAY,							//		iNumPassengersMade2Pay
	
	TAXI_HELP_SHOWN,
	
	TAXI_STATS_NUM_STATS						//  15 total 
ENDENUM

//STRING sStatsMenu[TAXI_STATS_NUM_STATS]
//EOF

