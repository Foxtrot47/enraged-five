///		Taxi_Procedural.sc
///		Author: Lino A. Manansala
///		Procedurals! aka Endless Summer



//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.



USING "Taxi_Includes.sch"
USING "Taxi_Regions_Lib.sch"
USING "Taxi_Locations_Lib.sch"
USING "minigame_midsized_message.sch"

//#IF IS_DEBUG_BUILD
INT iDebugThrottle
//#ENDIF

ENUM TAXI_FAIL_STATE
	TAXIFAIL_INIT,
	TAXIFAIL_PRINT
ENDENUM

TAXI_FAIL_STATE eTaxiFailState = TAXIFAIL_INIT

//Custom Data
TaxiStruct 						myTaxiData

TAXI_PED_RUN_STATE 				myRunState = TPRS_INIT

TAXI_MONEY_STRUCT 				taxiMoney

TAXI_PROCEDURAL_DATA			myTaxiProceduralData

TAXI_REG_INFO					txRegionsArray[TR_NUM_REGIONS]

MODEL_NAMES 					mPassengerModel

//TAXI_PROCEDURAL_LOCATIONS 	eTaxiDestination

//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

AGGRO_ARGS						aggroArgs

INT								iTipIndex

FLOAT 							fTipRate

BOOL 							bPassengerRan = TRUE

SEQUENCE_INDEX					siTemp

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize

#IF IS_DEBUG_BUILD

BOOL bDebugPassRun

//	WIDGET_GROUP_ID	 			taxiRideWidgets
//	TEXT_LABEL_63				sDebugString[5]
//	BOOL 						bDebugDrawP_Stats
//	BOOL 						bDebugTurnOnFreeRide = FALSE
//	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
//	
#ENDIF

PROC Script_Cleanup()
//	#IF IS_DEBUG_BUILD
//		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
//			DELETE_WIDGET_GROUP(taxiRideWidgets)
//		ENDIF
//		
//		CLEANUP_TAXI_WIDGETS()
//	#ENDIF
	
	// release assets
	IF NETWORK_IS_SIGNED_ONLINE()
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "online, writing to social club leaderboard")
		WRITE_TAXI_SCLB_DATA(TXM_PROCEDURAL)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
	
//	IF NOT IS_TIMER_STARTED(timerFailDelay)
//		TAXI_SCRIPT_FAILED(myTaxiData)
//		START_TIMER_NOW(timerFailDelay)
//	ELIF GET_TIMER_IN_SECONDS(timerFailDelay) > 4.0
//		Script_Cleanup()
//	ENDIF
ENDPROC

PROC REQUEST_TAXI_PROCEDURAL_STREAMS()
	REQUEST_MODEL(mPassengerModel)
	//REQUEST_ANIM_DICT("move_m@quick")
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	TAXI_INIT_SHARED_STREAMS()
	CDEBUG1LN(DEBUG_OJ_TAXI,"Initial Taxi Oddjob Deadline Assets requested.")
ENDPROC

FUNC BOOL HAVE_TAXI_PROCEDURAL_STREAMS_LOADED()
	
	//Always good to make sure everything is safe. 
	IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
		IF NOT HAS_MODEL_LOADED(mPassengerModel)
			#IF IS_DEBUG_BUILD
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading G_M_M_ChiGoon_02",iDebugThrottle)
			#ENDIF
			RETURN FALSE
		ENDIF
		
//		IF NOT HAS_ANIM_DICT_LOADED("move_m@quick")
//			#IF IS_DEBUG_BUILD
//			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading move_m@quick",iDebugThrottle)
//			#ENDIF
//			RETURN FALSE
//		ENDIF
		
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
			#IF IS_DEBUG_BUILD
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
			#IF IS_DEBUG_BUILD
			CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Procedural Assets Loaded")
	RETURN TRUE		
ENDFUNC

/// PURPOSE:
///    Set big message with a number token
/// PARAMS:
///    scaleformStruct - 
///    label - splash text string
///    iNum - Number to insert into the string.
///    iDuration - duration in milliseconds
///    eMessageType - Tells us what scaleform method we want to call
PROC SET_SCALEFORM_SHARD_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(SCRIPT_SHARD_BIG_MESSAGE & scaleformStruct, STRING labeToDisp, INT iFare, INT iTips, INT iTotalCash,
																	STRING strapLine, INT iDuration = 4000, HUD_COLOURS eHudColor = HUD_COLOUR_WHITE)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformStruct.siMovie, GET_SCALEFORM_SHARD_METHOD())
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(eHudColor)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(labeToDisp)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(labeToDisp)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(strapline)
			//Fare
			ADD_TEXT_COMPONENT_INTEGER(iFare)
			//Tips
			ADD_TEXT_COMPONENT_INTEGER(iTips)
			//Total
			ADD_TEXT_COMPONENT_INTEGER(iTotalCash)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
	
	// Restart our timer for updating.
	RESTART_TIMER_NOW(scaleformStruct.movieTimer)
	
	// Store the duration.
	scaleformStruct.iDuration = iDuration
	scaleformStruct.eEndFlash = HUD_COLOUR_WHITE
ENDPROC

// All these peds have approved voices
FUNC MODEL_NAMES GET_RANDOM_TAXI_PASSENGER_MODEL()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
		CASE 0 	RETURN A_F_Y_VINEWOOD_04
		CASE 1 	RETURN A_M_M_SOUCENT_04
		CASE 2 	RETURN A_M_Y_SALTON_01
		CASE 3 	RETURN A_M_Y_SOUCENT_04
		CASE 4 	RETURN A_M_Y_VINEWOOD_01
	ENDSWITCH
	
	RETURN A_F_Y_VINEWOOD_04
ENDFUNC

// All these peds have approved voices
FUNC STRING GET_TAXI_PASSENGER_VOICE(MODEL_NAMES eTaxiPassengerModel)
	SWITCH eTaxiPassengerModel
		CASE A_F_Y_VINEWOOD_04	RETURN "A_F_Y_VINEWOOD_04_WHITE_MINI_02"
		CASE A_M_M_SOUCENT_04	RETURN "A_M_M_SOUCENT_04_BLACK_MINI_01"
		CASE A_M_Y_SALTON_01	RETURN "A_M_Y_SALTON_01_WHITE_MINI_02"
		CASE A_M_Y_SOUCENT_04	RETURN "A_M_Y_SOUCENT_04_BLACK_MINI_01"
		CASE A_M_Y_VINEWOOD_01	RETURN "A_M_Y_VINEWOOD_01_BLACK_MINI_01"
	ENDSWITCH
	
	RETURN "A_F_Y_VINEWOOD_04_WHITE_MINI_02"
ENDFUNC

PROC INIT_TAXI_PROCEDURAL_DATA()
	mPassengerModel = GET_RANDOM_TAXI_PASSENGER_MODEL()
	
	myTaxiData.sProceduralPassengerVoice = GET_TAXI_PASSENGER_VOICE(mPassengerModel)
	
	// this is mostly all copied from TAXI_ODDJOB_GLOBAL_SETUP
	
	DISABLE_TAXI_HAILING()
	TOGGLE_TAXI_OJ_RADIO_SOUNDS_ON()
	
	TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo)
	
	RESET_TAXI_STRUCT_TO_DEFAULTS(myTaxiData)
	myTaxiData.tTaxiOJ_MissionType = TXM_PROCEDURAL
	
	IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
        SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
        SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), TRUE)
    ENDIF
	
	INIT_TAXI_OJ_RADIO_STATION_PREFERENCES_PER_MISSION(myTaxiData)
	
	// passing in TXM_NUM_TYPES because procedural comes after it
	TAXI_CONTROLLER_UpdateNumRuns(TXM_PROCEDURAL)
	
	SET_TAXI_ODDJOB_SPECIFICS(myTaxiData)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
	ENDIF
	
	myTaxiData.sDrivingStat = GET_TAXI_DRIVING_STAT_FOR_PLAYER()
	
    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_ODDJOB_PROCEDURAL_SETUP - SUCCESS")
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData, CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF, CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	myTaxiData.fTaxiEnterSpeed = 1.45// PEDMOVEBLENDRATIO_WALK
	
//	#IF IS_DEBUG_BUILD
//		
//		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Procedural")
//		
//		INIT_ODDJOB_TAXI_WIDGETS()
//		
//		ADD_WIDGET_BOOL("Show Procedural Stats", bDebugDrawP_Stats)
//	
//		STOP_WIDGET_GROUP()
//		
//		INIT_TAXI_WIDGETS(taxiRideWidgets)
//	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Procedural ~~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
ENDPROC

/// main difference is there's no dialogue, just god text
PROC TAXI_OJ_MONITOR_WANTED_LEVEL_PROCEDURAL()
    
    IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits,TAXI_OJ_JB_DISABLE_WANTED)
        
        SWITCH myTaxiData.tWantedStateIndex
        
            CASE TWS_CHECK_IF_WANTED//--------------------------------------------------------------
        
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
                    IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > TAXI_TWEAKS_TIME_DX_DELAY_POLICE
                        
                        //Preserve the wanted_level for the stats
                        SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                        
                        
                        TAXI_RESET_TIMERS(myTaxiData, TT_POLICE)
                        
                        //Hide old gps & blips
                        IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)          
                            SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
                            SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
                        ENDIF
                        
						myTaxiData.bIsCurrentlyWanted = TRUE
						
                        myTaxiData.tWantedStateIndex = TWS_PRINT_OBJ_TO_LOSE_POLICE
                        CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_PRINT_OBJ_TO_LOSE_POLICE")
                    ENDIF
                    
                ENDIF
            BREAK
            
            CASE TWS_PRINT_OBJ_TO_LOSE_POLICE//--------------------------------------------------------------
                
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 4.0
                    
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)   
                    myTaxiData.tWantedStateIndex = TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED")
                ENDIF
            BREAK
            
            CASE TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED//--------------------------------------------------------------
                
                IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED")
				ELIF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED - objective not printed")
                ENDIF
            BREAK
            
            
            CASE TWS_CHECK_IF_PLAYER_LOST_WANTED//--------------------------------------------------------------
                
                //If it's grown since last time make sure we update stats and the tracker
                IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(),GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_GAINED)
                ENDIF
                
                //Toggle escape police dialogue every x seconds
//                IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)// NOT myTaxiData.bTaxiOJ_CanSpeak
//                    IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
//                        IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > TAXI_OJ_CONST_TIME_BETWEEN_POLICE_REACTIONS
//                            SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE,TRUE)
//                        ENDIF
//                    ENDIF
//                ENDIF
                
                //Wait til he's lost it
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
						CDEBUG1LN(DEBUG_OJ_TAXI,"cleared objective print")
						CLEAR_PRINTS()
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"objective print was not on screen")
					ENDIF
					
                    //Show Blip & GPS again
                    IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - putting the drop off blip back on")
						SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,255)
						SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, TRUE)
                    ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - blip does not exist, can't bring it back!")
					ENDIF
                    
					myTaxiData.bIsCurrentlyWanted = FALSE
					
                    myTaxiData.tWantedStateIndex = TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED")
                ENDIF
                    
            BREAK
            
            CASE TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                
                    //"Nice You Lost the Pigs "
//                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_POLICE_LOST,TRUE,FALSE,TRUE)
        
                    //Award Bonus
                    //TAXI_SET_BONUS(myTaxiData,TAXI_BONUS_LOST_POLICE, 100 )
                        
                    //Update stats
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_LOST,GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                        
                    //Set tracker back to zero
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,0)
                    
                    myTaxiData.tWantedStateIndex = TWS_CLEANUP
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_MONITOR_WANTED_LEVEL - Police lost")
                ENDIF
                
            BREAK
        
            CASE TWS_CLEANUP
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                    OPEN_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TDQ_ADD_DELAY_BEFORE_RESUME)
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_WANTED
                ENDIF
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

PROC TAXI_OJ_PRO_SET_TIPS_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	
	//Exitement Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)
	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	//Start the mission timer here
	TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_PRO_SET_TIPS_AND_EXCITEMENT_TO_CHECK set tips to check on")
ENDPROC

PROC TAXI_P_OJ_RATE_OVERALL_TIP_LEVEL(BOOL bThisPassengerRan = FALSE)
	
	KILL_FACE_TO_FACE_CONVERSATION()
	CLEAR_PRINTS()
	
	IF bThisPassengerRan
		SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_RUNOFF,TRUE)
	
	ELIF myTaxiData.iTaxiOJ_CashTip < myTaxiData.iTaxiOJ_CashTipAvg
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "GENERIC_INSULT_MED", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_ASS iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, " which is less than iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
	
	ELIF myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "TAXI_GOOD", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AMAZING iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, " which is greater than iTaxiOJ_CashTipAmazing = ", myTaxiData.iTaxiOJ_CashTipAmazing)
	
	ELSE
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "GENERIC_THANKS", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_DIALOGUE_LIB] TAXI_RATE_OVERALL_TIP_LEVEL: TAXI_DI_RATE_TIP_AVERAGE iTaxiOJ_CashTip = ",myTaxiData.iTaxiOJ_CashTip, "iTaxiOJ_CashTipAvg = ", myTaxiData.iTaxiOJ_CashTipAvg)
	ENDIF
	
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue, TAXI_DXF_THANKS)
	
	TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
	
	
ENDPROC

FUNC FLOAT GET_DYNAMIC_STOPPING_DISTANCE_FOR_TAXI_PROCEDURAL()
	FLOAT fCurrentPlayerSpeed
	
	fCurrentPlayerSpeed = GET_ENTITY_SPEED(myTaxiData.viTaxi)
	
	IF fCurrentPlayerSpeed >= 5
		RETURN fCurrentPlayerSpeed * 0.5
	ENDIF
	
	RETURN 6.0
ENDFUNC

PROC TRIGGER_TAXI_QUEUE_PRO_LINES()
	//Trigger lines
	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine

			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_PRO_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO LOCATION has been assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO LOCATION has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
					//SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(myTaxiData.piTaxiPassenger, "PED_RANT", myTaxiData.sProceduralPassengerVoice, SPEECH_PARAMS_FORCE_FRONTEND)
					ENDIF
					
					tTaxiOJ_DQ_Data.iCurrentDQLine++
					
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 1")
					ENDIF
				ENDIF
			BREAK
			
//			CASE 2
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
//					
//					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
//					IF g_bDebug
//						SCRIPT_ASSERT("Triggering Banter 2")
//					ENDIF
//				ENDIF
//			BREAK
			
//			CASE 2
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(8.0,14.0)
//					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						tTaxiOJ_DQ_Data.iCurrentDQLine++
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)
	
ENDPROC

PROC Main_Taxi_OJ_Procedural()
	
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			SWITCH eTaxiFailState
				CASE TAXIFAIL_INIT
					IF TAXI_HANDLE_FAIL(myTaxiData)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Fail State going to : TAXIFAIL_PRINT")
						eTaxiFailState = TAXIFAIL_PRINT
					ENDIF
				BREAK
				CASE TAXIFAIL_PRINT
					Script_Failed()
				BREAK
			ENDSWITCH
		ELSE
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
				Script_Cleanup()
			ENDIF
		ENDIF
	
	//Proceed
	ELSE
		
		IF myTaxiData.tTaxiOJ_RideState <= TRS_DRIVING_PASSENGER
			
			RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
			
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		ENDIF
		
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_PRO_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF				

		ENDIF
		
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TC_HOWTOSTART")
			CLEAR_HELP()
			CDEBUG1LN(DEBUG_OJ_TAXI,"Tutorial help was cleared")
		ENDIF
		
		SWITCH myTaxiData.tTaxiOJ_RideState
			CASE TRS_FINDING_LOCATION
				IF GET_TAXI_DROPOFF_LOCATION_FROM_DISTANCE(txRegionsArray, myTaxiData.vTaxiOJPickup, 1)
				
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRS_FINDING_LOCATION - Passenger spawned at these coords", myTaxiData.vTaxiOJPickup)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_INIT_STREAM)
				ENDIF
			BREAK
			
			CASE TRS_INIT_STREAM
				IF TAXI_INIT_READY(myTaxiData)
					//requests
					REQUEST_TAXI_PROCEDURAL_STREAMS()
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				ENDIF
			BREAK
			
			CASE TRS_STREAMING
				IF HAVE_TAXI_PROCEDURAL_STREAMS_LOADED()					
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF
			BREAK
			
			CASE TRS_SPAWNING
				//IF TAXI_SPAWN_PASSENGER(myTaxiData, myTaxiData.vTaxiOJPickup, myTaxiData.vTaxiOJPickup , "TaxiHan", mPassengerModel,180.6)
				IF NOT DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
			        
					ADD_TAXI_OJ_SPEED_REDUCTION_VOLUMES(myTaxiData,myTaxiData.vTaxiOJPickup)
					
					//Initialize spawn and pickup locations
					myTaxiData.vTaxiOJSpawn = myTaxiData.vTaxiOJPickup
					
					//Stops cars from spawning in the area around the passenger
					TAXI_PREP_TURN_OFF_VEHICLE_GENS(myTaxiData.vTaxiOJPickup)
					
					CLEAR_AREA_OF_PEDS(myTaxiData.vTaxiOJPickup,TAXI_OJ_RADIUS_CLEAR_PED)
					
					// 2119025 - remove switching off roads here as it's breaking highways.
					//TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(myTaxiData.vTaxiOJPickup, FALSE)
					
					//IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
						myTaxiData.piTaxiPassenger = CREATE_PED(PEDTYPE_MISSION, mPassengerModel, myTaxiData.vTaxiOJSpawn) //CREATE_RANDOM_PED(myTaxiData.vTaxiOJSpawn)
					
						SET_AMBIENT_VOICE_NAME(myTaxiData.piTaxiPassenger, myTaxiData.sProceduralPassengerVoice)
						
						//Dialogue Setup-----------------
	        			ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo, 3, myTaxiData.piTaxiPassenger, "TaxiGeneric")
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SPAWN_PASSENGER - Passenger spawned at these coords", "", myTaxiData.vTaxiOJSpawn)
						
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_DisableSeeThroughChecksWhenTargeting, TRUE)
						
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							//SET_PED_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger, "move_m@quick")
							
							SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_WillFlyThroughWindscreen,FALSE)
							SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_PedsJackingMeDontGetIn,TRUE)
							SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_CanSayFollowedByPlayerAudio,TRUE)
							
							//myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJSpawn,TRUE)
							myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiData.piTaxiPassenger)
							SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger, "TAXI_BLIP_PASS")
							SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
							SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger,TRUE)
							
							ADD_RELATIONSHIP_GROUP("TAXI_Passenger", myTaxiData.relPassenger)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, myTaxiData.relPassenger, RELGROUPHASH_COP)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiData.piTaxiPassenger,myTaxiData.relPassenger)
							TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SPAWN_PASSENGER - SUCCESS")
						ENDIF
						
						//Give player obj to go pickup Passenger
						ENABLE_TAXI_SPEECH(myTaxiData)
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					//ENDIF
				ENDIF
			BREAK
			
			//Waits for passenger to head to cab
			CASE TRS_MANAGE_PICKUP
				//Help: You can cancel a taxi job before picking up the passenger by pressing ~INPUT_SCRIPT_LS~.~s~
				IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CANCELHELP_SHOWN)
					IF TAXI_CONTROLLER_GetNumRuns(TXM_PROCEDURAL) >= 2 
						PRINT_HELP("TAXI_2CANCEL")
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CANCELHELP_SHOWN)
					ENDIF
				ENDIF
											
				//Player can cancel this before he picks up the passenger
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
				AND NOT (myTaxiData.iTaxiOJ_StatesPickup > 0)
					TAXI_SET_FAIL(myTaxiData,"Player cancelled on dispatch",TFS_TAXI_CANCELLED)
				ENDIF
				
				
				
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
//					eTaxiDestination = INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(TAXILOC_NUM)))
//					CDEBUG1LN(DEBUG_OJ_TAXI,"TRS_WAIT_PARK - Passenger location is ", GET_TAXI_LOCATION_STRING(eTaxiDestination))
					
//					RESET_PED_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger)
					
					TAXI_SET_PROC_DESTINATION(myTaxiProceduralData)
					
					// !!!!!!
					// COMMENT IN TO TEST SPECIFIC LOCATIONS!!!! JUST FOR DEBUG
					// myTaxiProceduralData.CurrentLocation = TAXILOC_ZANCUDO_RIVER_HILL_CHURCH //TAXILOC_ALAMO_SEA_HOOD //
					// !!!!!!
					
					// possible destination / dropoff combo
					// -224.54303, 3898.72266, 36.39012
					// -201.79492, 3934.59351, 33.56364
					
					myTaxiData.vTaxiOJDropoff = GET_TAXI_LOCATION_COORD(myTaxiProceduralData.CurrentLocation)
					myTaxiData.vTaxiOJ_PassengerGoToPt = GET_TAXI_LOCATION_WALK_TO_COORD(myTaxiProceduralData.CurrentLocation)
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					
					IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						myTaxiData.blipTaxiDropOff = CREATE_BLIP_FOR_COORD(myTaxiData.vTaxiOJDropoff,TRUE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SET_DROPOFF_BLIP_ON - Destination Blip set")
					ENDIF
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					//Set all tip checks on
					TAXI_OJ_PRO_SET_TIPS_AND_EXCITEMENT_TO_CHECK()
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK
			
			CASE TRS_DRIVING_PASSENGER
//				IF TAXI_HANDLE_DRIVING(myTaxiData,tTaxiOJ_DQ_Data,TRUE,12,60)
					
				IF IS_TAXI_RIDE_ALL_READY(myTaxiData)
			        
					UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
					
			        //TAXI_OJ_MONITOR_PLAYER_HORN_HONK(myTaxiData)
			        
					TAXI_OJ_MONITOR_WANTED_LEVEL_PROCEDURAL()
					
			        RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(myTaxiData)
					
			        TAXI_OJ_MONITOR_STOPPED(myTaxiData)
			        
					IF HAS_TAXI_OJ_REACHED_DROPOFF(myTaxiData, GET_DYNAMIC_STOPPING_DISTANCE_FOR_TAXI_PROCEDURAL())
			        	
						CDEBUG1LN(DEBUG_OJ_TAXI,"Time Driven = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
						CDEBUG1LN(DEBUG_OJ_TAXI,"GPS Distance = ", myTaxiProceduralData.fGPSDistance)
						
						fTipRate = (myTaxiProceduralData.fGPSDistance/GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST)) * 3600
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"fTipRate = ", fTipRate)
						
						IF fTipRate > 40.0
							myTaxiData.iTaxiOJ_CashTip = myTaxiData.iTaxiOJ_CashTipAmazing
							
						ELIF fTipRate > 30.0
							myTaxiData.iTaxiOJ_CashTip = myTaxiData.iTaxiOJ_CashTipAvg
							
						ELSE
							myTaxiData.iTaxiOJ_CashTip = 0
							
						ENDIF
						
						//CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,CONST_TAXI_OJ_LINE_NUM_TO_CLOSE_DIALOGUE_Q,TRUE)
						
						//Remove destination blip
						REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
						
						INT iRandom
						iRandom = GET_RANDOM_INT_IN_RANGE(0, 100)
						
						IF iRandom < 10
							bPassengerRan = TRUE
							CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger Ran set to TRUE")
						ELSE
							bPassengerRan = FALSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger Ran set to FALSE")
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF bDebugPassRun
							bPassengerRan = TRUE
						ENDIF
						#ENDIF
						
						//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$CASH
						SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
						TAXI_P_OJ_RATE_OVERALL_TIP_LEVEL(bPassengerRan)
						CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
						
						IF bPassengerRan
							TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPECIAL_ENDING)
						ELSE
							TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
						ENDIF
						
					ENDIF
			    ENDIF
					
//				ENDIF
			BREAK
			
			CASE TRS_REGULAR_PAYMENT
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData, TRUE)
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						CLEAR_SEQUENCE_TASK(siTemp)
						OPEN_SEQUENCE_TASK(siTemp)
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
							IF DOES_SCENARIO_EXIST_IN_AREA(GET_TAXI_LOCATION_WALK_TO_COORD(myTaxiProceduralData.CurrentLocation), 5.0, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, GET_TAXI_LOCATION_WALK_TO_COORD(myTaxiProceduralData.CurrentLocation), 5.0)
							ELSE
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_TAXI_LOCATION_WALK_TO_COORD(myTaxiProceduralData.CurrentLocation), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
								TASK_STAND_STILL(NULL, 500)
								TASK_START_SCENARIO_IN_PLACE(NULL, PICK_STRING(GET_RANDOM_BOOL(), "WORLD_HUMAN_STAND_MOBILE", "WORLD_HUMAN_AA_SMOKE") , 5000, TRUE)
								TASK_WANDER_STANDARD(NULL)
							ENDIF
						CLOSE_SEQUENCE_TASK(siTemp)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, siTemp)
						
						//scenari
						
						SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger,TRUE)
					ENDIF
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF
			BREAK
			
			CASE TRS_SPECIAL_ENDING
				IF TAXI_SET_PED_RUNNING(myRunState, myTaxiData, myTaxiData.piTaxiPassenger, taxiMoney, TRUE)	
					
					//State++
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF
			BREAK
			
			CASE TRS_SCORECARD_GRADE
				SWITCH tscIndex
					CASE TSC_INIT
						PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_UNDER_THE_BRIDGE", "HUD_MINI_GAME_SOUNDSET", FALSE)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"Setting the fare to: ", myTaxiData.iTaxiOJ_CashFare)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Setting the tip to: ", myTaxiData.iTaxiOJ_CashTip)	
						
						SET_SCALEFORM_SHARD_MESSAGE_WITH_THREE_NUMBERS_IN_STRAPLINE(TaxiMidSize, "TAXI_FARE_FIN", myTaxiData.iTaxiOJ_CashFare, myTaxiData.iTaxiOJ_CashTip, 
																						(myTaxiData.iTaxiOJ_CashFare+myTaxiData.iTaxiOJ_CashTip), "TAXI_FARE_DET")
						TaxiMidSize.iDuration = 4000
						
						SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
						tscIndex = TSC_TALLY
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CALC_SCORECARD - TSC_TALLY")
					BREAK
					
					//After pressing X to continue & close the menu
					CASE TSC_TALLY
						IF NOT UPDATE_SHARD_BIG_MESSAGE(TaxiMidSize, TRUE)
							TAXI_REWARD_DRIVER(myTaxiData)
							
							TAXI_RESET_TIMERS(myTaxiData, TT_GENERIC)
							SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
							
							tscIndex = TSC_FINISH
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CALC_SCORECARD - TSC_FINISH")
						ENDIF
					BREAK
					
					CASE TSC_FINISH
						CDEBUG1LN(DEBUG_OJ_TAXI,"** TAXI_CALC_SCORECARD - SCORECARD DONE! **")
						
						TAXI_MISSION_END(TRUE,myTaxiData)
						
						//Move on to the next stage
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
					BREAK
				ENDSWITCH
				
			BREAK
			
			CASE TRS_CLEANUP
				Script_Cleanup()
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

SCRIPT
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		Script_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// init init initialize
	INIT_TAXI_PROCEDURAL_DATA()
	INIT_TAXI_REGIONS(txRegionsArray)
	INIT_ALL_TAXI_EXCITEMENT_VALUES()
	INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
	
	// setting this to really far away in order to get a close pickup
	myTaxiData.vTaxiOJPickup = <<4186.7969, -3657.3494, -0.5762>>
	
	WHILE TRUE
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD	
//		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
//			IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
//				//If you J Skip, check the timer and add some tip
//				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) < fDeadlineGoals[TXDL_TIME_GOOD_3]// TAXI_CONST_BONUS_TIME_SPEED_DEMON
//					SET_TAXI_TIP_TO_AMAZING(myTaxiData)
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			bDebugPassRun = TRUE
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_R))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 15)
			ENDIF
		ENDIF	
		
//		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
//			PROCESS_WIDGETS()
//		ENDIF
	#ENDIF
	//END DEBUG----------------------------------------------------
		
		Main_Taxi_OJ_Procedural()
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT

// EOF
