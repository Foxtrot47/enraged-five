//Taxi_Point_Driving_Lib.sch
//Author : John R. Diaz
//Use this to monitor player's driving behavior


USING "Taxi_point_driving.sch"
USING "Taxi_Dialogue_Lib.sch"


//Excitement Tweaks----------------------------------------
TWEAK_FLOAT     TAXI_TWEAKS_THRESHOLD_DRIFT             3.0     // How fast in an X direction do we need to be going to be counted as "drifting"
TWEAK_FLOAT     TAXI_TWEAKS_THRESHOLD_SPEED             25.0    //How fast to go for it to be considered speeding
TWEAK_FLOAT     TAXI_TWEAKS_SPEED_CHANGE_DIFF           10.0        //the Speed delta to consider a quickstop - USED IN CONJUNCTION WITH TAXI_TWEAKS_SPEED_CHANGE_MIN
TWEAK_FLOAT     TAXI_TWEAKS_SPEED_CHANGE_MIN            12.0    //the min speed you have to reach to be considered a hard stop - USED IN CONJUNCTION WITH TAXI_TWEAKS_SPEED_CHANGE_DIFF

TWEAK_FLOAT     TAXI_TWEAKS_SWERVE_INIT_SPEED           2.5     //the min lateral speed that the taxi needs to move to initialize the swerve timer

TWEAK_FLOAT     TAXI_TWEAK_TIME_DRAMATIC_SPEED_CHANGE   0.9
TWEAK_FLOAT     TAXI_TWEAKS_TIME_AIR                    0.7     // How long do we need to be in the air before we counted as "in the air"
TWEAK_FLOAT     TAXI_TWEAKS_TIME_ONCOMING_TRAFFIC       3.5     //Min amount of time in seconds that you have to be driving into oncoming traffic for it to trigger
TWEAK_FLOAT     TAXI_TWEAKS_TIME_POWERSLIDING           1.2     //Min amount of time to hold a p slide for it to trigger
TWEAK_FLOAT     TAXI_TWEAKS_TIME_SIDEWALKIN             2.0     //Min amount of time to drive on the sidewalk for it to trigger
TWEAK_FLOAT     TAXI_TWEAKS_TIME_SPEEDIN                3.5     //Min amount of time to speed for it to trigger
TWEAK_FLOAT     TAXI_TWEAKS_TIME_BORING                 20.0    //how often to deduct points if nothing is done
TWEAK_FLOAT     TAXI_TWEAKS_TIME_RELAXING               30.0    //how often to deduct points if nothing is done

//Const Num for how long before checking the event again
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_AIR_CHECK		5.0//8.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_QSTOP_CHECK	5.0//10.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_DRIFT_CHECK	5.0//9.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_ROLL_CHECK		5.0//8.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_ONCOMING_CHECK	5.0//9.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_SIDEWALK_CHECK	4.0//8.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_SPEEDING_CHECK	6.0//4.0//8.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_SWERVING_CHECK	8.0//10.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_REVERSE_CHECK	5.0//10.0
TWEAK_FLOAT TAXI_TWEAKS_EX_DELAY_FOR_NEARMISS_CHECK	7.0//10.0

TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_TOOK_DAMAGE	7.0//9.0//11.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_HIT_PED		7.0//10.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_HIT_OBJECT		11.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_HIT_VEHICLE	11.0
TWEAK_FLOAT	TAXI_TWEAKS_EX_DELAY_FOR_HIT_BUILDING	11.0

//Time since last caps
TWEAK_FLOAT	TAXI_TWEAKS_EX_RAN_RED_CAP				100.0

TWEAK_FLOAT	TAXI_TWEAKS_SMALL_HIT					10.0
TWEAK_FLOAT	TAXI_TWEAKS_BIG_HIT						80.0

TWEAK_FLOAT TAXI_TWEAKS_ROLL_NEG					-145.0
TWEAK_FLOAT TAXI_TWEAKS_ROLL_POS					145.0


TAXI_EXCITEMENT_TYPE 	txExcitementArray[TXET_NUM_TYPES]
//BOOL 					bAllExcitementHit = FALSE

BOOL 					bRolledUpsideDown

FLOAT					fSwerveOriginal

#IF IS_DEBUG_BUILD
	TEXT_LABEL_31 sDebugTextPD[6]
#ENDIF
FUNC STRING GET_TAXI_EXCITEMENT_BY_NAME(TAXI_EXCITMENT_FLAG aWhichFlag)

	STRING sExFlagName
	
	SWITCH aWhichFlag
		
		CASE TXET_AIR
			sExFlagName = "TXET_AIR"
		BREAK
		
		CASE TXET_POWERSLIDE
			sExFlagName = "TXET_POWERSLIDE"
		BREAK
		
		CASE TXET_QUICKSTOP
			sExFlagName = "TXET_QUICKSTOP"
		BREAK
		
		CASE TXET_TOOKDAMAGE
			sExFlagName = "TXET_TOOKDAMAGE"
		BREAK
		
		CASE TXET_HIT_PED
			sExFlagName = "TXET_HIT_PED"
		BREAK
		
		CASE TXET_RANLIGHT
			sExFlagName = "TXET_RANLIGHT"
		BREAK
		
		CASE TXET_SIDEWALK
			sExFlagName = "TXET_SIDEWALK"
		BREAK

		CASE TXET_SPEEDING
			sExFlagName = "TXET_SPEEDING"
		BREAK
		
		CASE TXET_WRONG_LANE
			sExFlagName = "TXET_WRONG_LANE"
		BREAK
		
		CASE TXET_ROLL
			sExFlagName = "TXET_ROLL"
		BREAK
		
		CASE TXET_WANTED
			sExFlagName = "TXET_WANTED"
		BREAK
		
		CASE TXET_SWERVING
			sExFlagName = "TXET_SWERVING"
		BREAK
		
		CASE TXET_REVERSE
			sExFlagName = "TXET_REVERSE"
		BREAK
		
		CASE TXET_OFFROAD
			sExFlagName = "TXET_OFFROAD"
		BREAK
		
		CASE TXET_NEAR_MISS
			sExFlagName = "TXET_NEAR_MISS"
		BREAK
		
		DEFAULT
			sExFlagName = "INVALID EXCITEMENT FLAG"
			SCRIPT_ASSERT("Invalid Excitement Flag passed")
		BREAK
	ENDSWITCH
	
	RETURN sExFlagName
ENDFUNC

PROC INIT_ALL_TAXI_EXCITEMENT_VALUES()
	//SPEEDING----------------------------------------------------------------------------------

	txExcitementArray[TXET_SPEEDING].iAmountOfExcitement = ENUM_TO_INT(TEA_LO)
	txExcitementArray[TXET_SPEEDING].iTimesActivated = 0
	txExcitementArray[TXET_SPEEDING].sType = "TX_SPEED_N"
	txExcitementArray[TXET_SPEEDING].tDialogueIndex = TAXI_DI_SPEEDING
	
	//AIR----------------------------------------------------------------------------------

	txExcitementArray[TXET_AIR].iAmountOfExcitement = ENUM_TO_INT(TEA_LO)
	txExcitementArray[TXET_AIR].iTimesActivated = 0
	txExcitementArray[TXET_AIR].sType = "TX_AIR_N"
	txExcitementArray[TXET_AIR].tDialogueIndex = TAXI_DI_AIRBORN

	//QUICKSTOP----------------------------------------------------------------------------------
	txExcitementArray[TXET_QUICKSTOP].iAmountOfExcitement = ENUM_TO_INT(TEA_LO)
	txExcitementArray[TXET_QUICKSTOP].iTimesActivated = 0
	txExcitementArray[TXET_QUICKSTOP].sType = "TX_QSTOP_N"
	txExcitementArray[TXET_QUICKSTOP].tDialogueIndex = TAXI_DI_SICK_WHOA

	//POWERSLIDE----------------------------------------------------------------------------------
	txExcitementArray[TXET_POWERSLIDE].iAmountOfExcitement = ENUM_TO_INT(TEA_MID)
	txExcitementArray[TXET_POWERSLIDE].iTimesActivated = 0
	txExcitementArray[TXET_POWERSLIDE].sType = "TX_DRIFT_N"
	txExcitementArray[TXET_POWERSLIDE].tDialogueIndex = TAXI_DI_POWERSLIDING
	
	//SIDEWALK----------------------------------------------------------------------------------

	txExcitementArray[TXET_SIDEWALK].iAmountOfExcitement = ENUM_TO_INT(TEA_MID)
	txExcitementArray[TXET_SIDEWALK].iTimesActivated = 0
	txExcitementArray[TXET_SIDEWALK].sType = "TX_SIDEWALK_N"
	txExcitementArray[TXET_SIDEWALK].tDialogueIndex = TAXI_DI_SIDEWALKIN
	
	//WRONGLANE----------------------------------------------------------------------------------

	txExcitementArray[TXET_WRONG_LANE].iAmountOfExcitement = ENUM_TO_INT(TEA_MID)
	txExcitementArray[TXET_WRONG_LANE].iTimesActivated = 0
	txExcitementArray[TXET_WRONG_LANE].sType = "TX_ONCOMING_N"
	txExcitementArray[TXET_WRONG_LANE].tDialogueIndex = TAXI_DI_OPPLANE
	
	//HIT PERSON----------------------------------------------------------------------------------

	txExcitementArray[TXET_HIT_PED].iAmountOfExcitement = ENUM_TO_INT(TEA_HI)
	txExcitementArray[TXET_HIT_PED].iTimesActivated = 0
	txExcitementArray[TXET_HIT_PED].sType = "TX_HITRUN_N"
	txExcitementArray[TXET_HIT_PED].tDialogueIndex = TAXI_DI_HITNRUN
	
	//TOOK DAMAGE----------------------------------------------------------------------------------

	txExcitementArray[TXET_TOOKDAMAGE].iAmountOfExcitement = ENUM_TO_INT(TEA_HI)
	txExcitementArray[TXET_TOOKDAMAGE].iTimesActivated = 0
	txExcitementArray[TXET_TOOKDAMAGE].sType = "TX_RECKLESS_N"
	txExcitementArray[TXET_TOOKDAMAGE].tDialogueIndex = TAXI_DI_COLLISION
	
	//ROLLED TAXI----------------------------------------------------------------------------------

	txExcitementArray[TXET_ROLL].iAmountOfExcitement = ENUM_TO_INT(TEA_HI)
	txExcitementArray[TXET_ROLL].iTimesActivated = 0
	txExcitementArray[TXET_ROLL].sType = "TX_ROLL_N"
	txExcitementArray[TXET_ROLL].tDialogueIndex = TAXI_DI_ROLLED
	
	//SWERVING-------------------------------------------------------------------------------------
	
	txExcitementArray[TXET_SWERVING].iAmountOfExcitement = ENUM_TO_INT(TEA_LO)
	txExcitementArray[TXET_SWERVING].iTimesActivated = 0
	txExcitementArray[TXET_SWERVING].sType = "TX_SWERVE_N"
	txExcitementArray[TXET_SWERVING].tDialogueIndex = TAXI_DI_SWERVE
	
	//REVERSE-------------------------------------------------------------------------------------
	
	txExcitementArray[TXET_REVERSE].iAmountOfExcitement = ENUM_TO_INT(TEA_LO)
	txExcitementArray[TXET_REVERSE].iTimesActivated = 0
	txExcitementArray[TXET_REVERSE].sType = "TX_REVERSE_N"
	txExcitementArray[TXET_REVERSE].tDialogueIndex = TAXI_DI_REVERSE
	
	//OFFROAD-------------------------------------------------------------------------------------
	
	txExcitementArray[TXET_OFFROAD].iAmountOfExcitement = ENUM_TO_INT(TEA_MID)
	txExcitementArray[TXET_OFFROAD].iTimesActivated = 0
	txExcitementArray[TXET_OFFROAD].sType = "TX_OFFROAD_N"
	txExcitementArray[TXET_OFFROAD].tDialogueIndex = TAXI_DI_OFFROAD
	
	//NEAR MISS-----------------------------------------------------------------------------------
	
	txExcitementArray[TXET_NEAR_MISS].iAmountOfExcitement = ENUM_TO_INT(TEA_MID)
	txExcitementArray[TXET_NEAR_MISS].iTimesActivated = 0
	txExcitementArray[TXET_NEAR_MISS].sType = "TX_NEARMIS_N"
	txExcitementArray[TXET_NEAR_MISS].tDialogueIndex = TAXI_DI_NEARMISS
ENDPROC
PROC SET_ALL_TAXI_PASSENGER_REACT_BITS(TaxiStruct &myTaxiData)
	INT k  
	REPEAT ENUM_TO_INT(TXET_NUM_TYPES) k
		SET_BITMASK(myTaxiData.iTaxiOJ_PassengerReactBits, k)
	ENDREPEAT
ENDPROC
PROC CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(TaxiStruct &myTaxiData)
	INT k  
	REPEAT ENUM_TO_INT(TXET_NUM_TYPES) k
		CLEAR_BITMASK(myTaxiData.iTaxiOJ_PassengerReactBits, k)
	ENDREPEAT
ENDPROC
FUNC BOOL IS_TAXI_POWERSLIDING_FOR_TIME(TaxiStruct &myTaxiData)
	VECTOR tempTaxiVelocity
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		
		tempTaxiVelocity = GET_ENTITY_SPEED_VECTOR(myTaxiData.viTaxi, TRUE)
		
		//If I'm not in the air and past this threshold I'm drifting
		IF ABSF(tempTaxiVelocity.x) > TAXI_TWEAKS_THRESHOLD_DRIFT
		AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
			ELSE
				//Might want to port over to do once when ready 
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer) > TAXI_TWEAKS_TIME_POWERSLIDING
					CANCEL_TIMER(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
					
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						sDebugTextPD[2] = "Sliding = "
						sDebugTextPD[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer))
						DRAW_DEBUG_TEXT_2D(sDebugTextPD[2],<<0.8,0.36,0.0>>)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Car is sliding. ", "", tempTaxiVelocity, "for : ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer), "", "seconds") 
					#ENDIF
				ENDIF
			ENDIF
			
			
		ELSE
			//Vehicle is no longer in drifting.
			CANCEL_TIMER(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL POINT_BOX_SAT_INTERSECTION(VECTOR point, VECTOR cor0, VECTOR cor1, VECTOR cor2)

      point.z = 0 cor0.z = 0 cor1.z = 0 cor2.z = 0
      VECTOR axis0 = NORMALISE_VECTOR(cor1-cor0)
      VECTOR axis1 = NORMALISE_VECTOR(cor2-cor0)
      
      FLOAT pointProjected0 = DOT_PRODUCT(point, axis0)
      FLOAT cor0Projected   = DOT_PRODUCT(cor0, axis0)
      FLOAT cor1Projected   = DOT_PRODUCT(cor1, axis0)
      
      FLOAT pointProjected1 = DOT_PRODUCT(point, axis1)
      FLOAT cor2Projected   = DOT_PRODUCT(cor0, axis1)
      FLOAT cor3Projected   = DOT_PRODUCT(cor2, axis1)
      
      IF cor0Projected > cor1Projected
            FLOAT temp = cor0Projected
            cor0Projected = cor1Projected
            cor1Projected = temp
      ENDIF
      
      IF pointProjected0 < cor0Projected OR pointProjected0 > cor1Projected
            RETURN FALSE
      ENDIF
      
      IF cor2Projected > cor3Projected
            FLOAT temp = cor2Projected
            cor2Projected = cor3Projected
            cor3Projected = temp
      ENDIF
      
      IF pointProjected1 < cor2Projected OR pointProjected1 > cor3Projected
            RETURN FALSE
      ENDIF

      RETURN TRUE
ENDFUNC

FUNC BOOL IS_ENTITY_ON_ROAD(ENTITY_INDEX entity)

	VECTOR node0, node1, node2, dir0, dir1
	VECTOR cor0, cor1, cor2, cor3
	float width
	      
	GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(entity), 1, node0)
	GET_NTH_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(entity), 2, node1)
	GET_POSITION_BY_SIDE_OF_ROAD(node0, -1, node2)

	width = VMAG(node2-node0)
	dir0 = NORMALISE_VECTOR(<<node1.x-node0.x, node1.y-node0.y, 0>>)
	dir1 = ROTATE_VECTOR_ABOUT_Z_ORTHO(dir0, ROTSTEP_90)*width
	dir0 *= 2.0
	cor0 = node0-dir0 + dir1
	cor1 = node0-dir0 - dir1
	cor2 = node1+dir0 + dir1
	cor3 = node1+dir0 - dir1
	cor3 = cor3

	/* debug
	DRAW_MARKER(MARKER_CYLINDER, node0, <<0,0,0>>, <<0,0,0>>, <<1,1,1>>, 255, 0, 0)
	DRAW_MARKER(MARKER_CYLINDER, node1, <<0,0,0>>, <<0,0,0>>, <<1,1,1>>, 0, 255, 0)
	DRAW_MARKER(MARKER_CYLINDER, node2, <<0,0,0>>, <<0,0,0>>, <<1,1,1>>, 0, 0, 255)
	DRAW_MARKER(MARKER_CYLINDER, cor0, <<0,0,0>>, <<0,0,0>>, <<1,1,1>>, 255, 255, 255)
	DRAW_MARKER(MARKER_CYLINDER, cor1, <<0,0,0>>, <<0,0,0>>, <<1,1,1>>, 0, 0, 0)
	DRAW_LINE(cor0 +<<0,0,0.5>>,cor1 +<<0,0,0.5>>)
	DRAW_LINE(cor0 +<<0,0,0.5>>,cor2 +<<0,0,0.5>>)
	DRAW_LINE(cor2 +<<0,0,0.5>>,cor3 +<<0,0,0.5>>)
	DRAW_LINE(cor1 +<<0,0,0.5>>,cor3 +<<0,0,0.5>>)
	//*/
	
	RETURN POINT_BOX_SAT_INTERSECTION(GET_ENTITY_COORDS(entity), cor0, cor1, cor2) //IS_ENTITY_IN_AREA(entity, cor0, cor1, FALSE, FALSE)
ENDFUNC


FUNC BOOL IS_TAXI_OFFROAD(TaxiStruct & myTaxiData)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	//AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
	//AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
	//AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_HIT_PED].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_ROLL].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_SIDEWALK].tWakeupTimer)
	
		IF NOT IS_ENTITY_ON_ROAD(myTaxiData.viTaxi)
		AND GET_ENTITY_SPEED(myTaxiData.viTaxi) > 15.0
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_OFFROAD].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_OFFROAD].tWakeupTimer)
				CDEBUG1LN(DEBUG_OJ_TAXI,"[OFFROAD] Offroad started")
			ELSE
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_OFFROAD].tWakeupTimer) > 5.0
					CANCEL_TIMER(txExcitementArray[TXET_OFFROAD].tWakeupTimer)
					
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						sDebugTextPD[2] = "OFFROAD = "
						sDebugTextPD[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_OFFROAD].tWakeupTimer))
						DRAW_DEBUG_TEXT_2D(sDebugTextPD[2],<<0.8,0.28,0.0>>)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Car is offroad for : ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_OFFROAD].tWakeupTimer), "", "seconds") 
					#ENDIF
				ENDIF
			ENDIF
			
			
		ELSE
			//Vehicle is no longer in drifting.
			CANCEL_TIMER(txExcitementArray[TXET_OFFROAD].tWakeupTimer)
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_NEAR_MISS(TaxiStruct & myTaxiData)
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_TOOKDAMAGE].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_HIT_PED].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_ROLL].tWakeupTimer)
		
		IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer)
		
			myTaxiData.viNearMiss = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(myTaxiData.viTaxi), TAXI_CONST_NEAR_MISS_CAR_DISTANCE, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_CHECK_VEHICLE_OCCUPANTS_STATES)
			
			IF DOES_ENTITY_EXIST(myTaxiData.viNearMiss)
				IF GET_ENTITY_SPEED(myTaxiData.viTaxi) > TAXI_CONST_NEAR_MISS_MIN_SPEED
				AND GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viNearMiss, myTaxiData.viTaxi) < TAXI_TWEAK_NEAR_MISS_RADIUS
				AND (NOT IS_ENTITY_DEAD(myTaxiData.viNearMiss) AND NOT IS_VEHICLE_SEAT_FREE(myTaxiData.viNearMiss, VS_DRIVER))
					
					// start a timer
					START_TIMER_NOW(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[NEAR_MISS]  Timer Started")
				ENDIF
			ENDIF
		
		ELSE
			IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer) < 1.5
			AND GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viNearMiss, myTaxiData.viTaxi) > 4.5
			AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(myTaxiData.viNearMiss)
			
				CANCEL_TIMER(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer)
				CDEBUG1LN(DEBUG_OJ_TAXI,"[NEAR_MISS]  Completed!  Returning TRUE")
			
				RETURN TRUE
			ELIF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer) >= 1.5
			
				CANCEL_TIMER(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer)
				CDEBUG1LN(DEBUG_OJ_TAXI,"[NEAR_MISS]  No near miss if stayed close for too long")
				
				RETURN FALSE
			ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(myTaxiData.viNearMiss)
				
				CANCEL_TIMER(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer)
				CDEBUG1LN(DEBUG_OJ_TAXI,"[NEAR_MISS]  No near miss if player hits car")
				
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
					sDebugTextPD[2] = "Near Miss = "
					sDebugTextPD[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer))
					DRAW_DEBUG_TEXT_2D(sDebugTextPD[2],<<0.8,0.32,0.0>>)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Car is near miss for : ", GET_TIMER_IN_SECONDS(txExcitementArray[TXET_NEAR_MISS].tWakeupTimer), " seconds") 
				#ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		IF GET_ENTITY_SPEED(myTaxiData.viTaxi) > TAXI_CONST_NEAR_MISS_MIN_SPEED
			DRAW_DEBUG_TEXT_2D("Taxi is ready to near miss",<<0.8,0.30,0.0>>)
		ENDIF
		
		IF DOES_ENTITY_EXIST(myTaxiData.viNearMiss)
		AND GET_ENTITY_SPEED(myTaxiData.viTaxi) > TAXI_CONST_NEAR_MISS_MIN_SPEED
		AND (NOT IS_ENTITY_DEAD(myTaxiData.viNearMiss) AND NOT IS_VEHICLE_SEAT_FREE(myTaxiData.viNearMiss, VS_DRIVER))
			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(myTaxiData.viNearMiss), TAXI_TWEAK_NEAR_MISS_RADIUS, 0, 0, 255, 100)
			
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_SWERVING(TaxiStruct & myTaxiData)
	VECTOR tempTaxiVelocity
	
	// This fires too frequently if vehicle is at high speed
	// so we'll block this from firing if it's speeding, in the air, drifting, or rolling
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_HIT_PED].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_ROLL].tWakeupTimer)
	AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_TOOKDAMAGE].tWakeupTimer)
		
		tempTaxiVelocity = GET_ENTITY_SPEED_VECTOR(myTaxiData.viTaxi, TRUE)
		
		//CDEBUG1LN(DEBUG_OJ_TAXI,"********** FIND A PATTERN FOR SWERVING.  SPEED VECTOR IS CURRENTLY: ", tempTaxiVelocity)
		
		//If I'm not in the air and past this threshold, I may be trying to swerve
		IF ABSF(tempTaxiVelocity.x) > TAXI_TWEAKS_SWERVE_INIT_SPEED
		AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)
		
			CDEBUG1LN(DEBUG_OJ_TAXI,"[SWERVE] Started")
			
			//start a timer
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SWERVING].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_SWERVING].tWakeupTimer)
				fSwerveOriginal = tempTaxiVelocity.x
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"[SWERVE]  Timer Started")
			ELSE
				
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SWERVING].tWakeupTimer) < 1.5
				AND (ABSF(fSwerveOriginal) - ABSF(tempTaxiVelocity.x)) < 0.0
					CANCEL_TIMER(txExcitementArray[TXET_SWERVING].tWakeupTimer)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[SWERVE]  Completed.  Returning TRUE")
					
					RETURN TRUE
				
				ELIF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SWERVING].tWakeupTimer) >= 1.5
					//Vehicle is no longer swerving.
					CANCEL_TIMER(txExcitementArray[TXET_SWERVING].tWakeupTimer)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"[SWERVE]  Not Done.  Returning FALSe")
					
					RETURN FALSE
					
				ELSE
					#IF IS_DEBUG_BUILD
						sDebugTextPD[2] = "Swerving = "
						sDebugTextPD[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SWERVING].tWakeupTimer))
						DRAW_DEBUG_TEXT_2D(sDebugTextPD[2],<<0.8,0.40,0.0>>)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Car is swerving. ", "", tempTaxiVelocity, "for : ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SWERVING].tWakeupTimer), "", "seconds") 
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_TAXI_IN_REVERSE(TaxiStruct &myTaxiData)
	VECTOR tempTaxiVelocity
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		
		tempTaxiVelocity = GET_ENTITY_SPEED_VECTOR(myTaxiData.viTaxi, TRUE)
		
		//If I'm not in the air and past this threshold I'm going in reverse
		IF tempTaxiVelocity.y < -10.0
		AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_REVERSE].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_REVERSE].tWakeupTimer)
			ELSE
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_REVERSE].tWakeupTimer) > 5.0
					CANCEL_TIMER(txExcitementArray[TXET_REVERSE].tWakeupTimer)
					
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						sDebugTextPD[2] = "Reversing = "
						sDebugTextPD[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_REVERSE].tWakeupTimer))
						DRAW_DEBUG_TEXT_2D(sDebugTextPD[2],<<0.8,0.42,0.0>>)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Car is in reverse. ", "", tempTaxiVelocity, "for : ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_REVERSE].tWakeupTimer), "", "seconds") 
					#ENDIF
				ENDIF
			ENDIF
			
		ELSE
			//Vehicle is no longer in drifting.
			CANCEL_TIMER(txExcitementArray[TXET_REVERSE].tWakeupTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_AIRBORNE_FOR_TIME(TaxiStruct &myTaxiData)

	//Vehicle is still in air.
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(myTaxiData.viTaxi)
		AND IS_ENTITY_IN_AIR(myTaxiData.viTaxi)		//If we don't check for this , fire false positives when in WATER
		
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_AIR].tWakeupTimer)
			ELSE
	
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_AIR].tWakeupTimer) > TAXI_TWEAKS_TIME_AIR
					CANCEL_TIMER(txExcitementArray[TXET_AIR].tWakeupTimer)
					RESTART_TIMER_NOW(txExcitementArray[TXET_POWERSLIDE].tSleepTimer)
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD	
						sDebugTextPD[0] = "Air = "
						sDebugTextPD[0] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TIMER_IN_SECONDS(txExcitementArray[TXET_AIR].tWakeupTimer))
						DRAW_DEBUG_TEXT_2D(sDebugTextPD[0],<<0.8,0.38,0.0>>)
					#ENDIF
				
					CDEBUG1LN(DEBUG_OJ_TAXI,"Car is in the air for  ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_AIR].tWakeupTimer), "", "seconds.") 
				ENDIF
			
			ENDIF
	
		ELSE
			//Vehicle is no longer in air.
			CANCEL_TIMER(txExcitementArray[TXET_AIR].tWakeupTimer)
		ENDIF

	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: This is used to check for sudden stops in TAKE IT EASY. Basically compares your speed now and then TAXI_TWEAK_TIME_DRAMATIC_SPEED_CHANGE from now and checks if they 
///    are greater than TAXI_TWEAKS_SPEED_CHANGE_DIFF. I used to check for jumps positive & negative by grabbing the ABSF, but ended up only wanting sudden stops instead of sudden speed ups. 
///    
/// PARAMS:
///    myTaxiData -  Our global Taxi Data
/// RETURNS:
///    
FUNC BOOL IS_TAXI_QUICK_SPEED_CHANGE(TaxiStruct &myTaxiData)
	
	IF NOT IS_PED_INJURED(myTaxiData.piTaxiPlayer)
		//If I'm Stopped / NOT AIRBORNE / NOT SLIDING than check for speed change
		IF NOT IS_PED_STOPPED(myTaxiData.piTaxiPlayer)
		AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tWakeupTimer)	
		AND NOT IS_TIMER_STARTED(txExcitementArray[TXET_POWERSLIDE].tWakeupTimer)
		
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_QUICKSTOP].tWakeupTimer)
			
				myTaxiData.fTaxiOJ_SpeedCurr = GET_ENTITY_SPEED(myTaxiData.piTaxiPlayer)
				
				//LM Let's only start the timer if the player is moving at a decent speed.
				// This is so we don't punish the player for trying to legitimately stop at lights and stop signs
				IF myTaxiData.fTaxiOJ_SpeedCurr > 10.0
					START_TIMER_NOW(txExcitementArray[TXET_QUICKSTOP].tWakeupTimer)
					
				ENDIF
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Grabbed Speed = ", "", myTaxiData.fTaxiOJ_SpeedCurr )
			ELSE
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_QUICKSTOP].tWakeupTimer) > TAXI_TWEAK_TIME_DRAMATIC_SPEED_CHANGE
				
					myTaxiData.fTaxiOJ_SpeedPrev = myTaxiData.fTaxiOJ_SpeedCurr
					myTaxiData.fTaxiOJ_SpeedCurr = GET_ENTITY_SPEED(myTaxiData.piTaxiPlayer)
					
					FLOAT fSpeedDelta = (myTaxiData.fTaxiOJ_SpeedPrev - myTaxiData.fTaxiOJ_SpeedCurr)
					
					CANCEL_TIMER(txExcitementArray[TXET_QUICKSTOP].tWakeupTimer)
					
					//Check if your the difference in speeds # seconds later is larger than CONST_DRAMATIC_SPEED_CHANGE
				
					//	RETURN ABSF(myTaxiData.fTaxiOJ_SpeedPrev - myTaxiData.fTaxiOJ_SpeedCurr) > TAXI_TWEAKS_SPEED_CHANGE_DIFF
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Speed Delta= ", "", fSpeedDelta)
					IF fSpeedDelta > TAXI_TWEAKS_SPEED_CHANGE_DIFF
					AND myTaxiData.fTaxiOJ_SpeedCurr < TAXI_TWEAKS_SPEED_CHANGE_MIN
						RETURN TRUE
					ENDIF
				ENDIF

			ENDIF
		//Car is completely stopped 
		ELSE
			CANCEL_TIMER(txExcitementArray[TXET_QUICKSTOP].tWakeupTimer)
			
		ENDIF
	ENDIF
	
	RETURN FALSE
			
ENDFUNC

FUNC BOOL DID_TAXI_TAKE_DAMAGE(TaxiStruct &myTaxiData)
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		
		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(myTaxiData.viTaxi)
			INT iCurrentVehicleHealth = GET_ENTITY_HEALTH(myTaxiData.viTaxi)
			INT iSizeOfImpact = myTaxiData.iOldVehicleHealth - iCurrentVehicleHealth
			myTaxiData.iOldVehicleHealth = iCurrentVehicleHealth
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(myTaxiData.viTaxi)
			
			IF iSizeOfImpact > TAXI_TWEAKS_SMALL_HIT
				
				IF myTaxiData.bBoredFlag
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
					myTaxiData.bBoredFlag = FALSE
				ELSE
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_COLLISION,TRUE,FALSE,TRUE)
				ENDIF
				
				//TODO: GET BIG HITS WORKING
				/*
				IF iSizeOfImpact > TAXI_TWEAKS_BIG_HIT
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_COLLISION,TRUE)					
				ELSE
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_COLLISION,TRUE)
				ENDIF
				*/
				CANCEL_TIMER(txExcitementArray[TXET_QUICKSTOP].tSleepTimer)
				RETURN TRUE
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_TAXI_HIT_PED(TaxiStruct &myTaxiData)
		
	//Hit Peds
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_DRIVER) = PLAYER_PED_ID()
			IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())

				CDEBUG1LN(DEBUG_OJ_TAXI,"Somebody got hit!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_TAXI_RUN_RED_LIGHT(TaxiStruct &myTaxiData)
		
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		INT iTempTimeTaxiRanLight = GET_TIME_SINCE_PLAYER_RAN_LIGHT(PLAYER_ID())
		
	
		IF iTempTimeTaxiRanLight > 0 
		AND iTempTimeTaxiRanLight < TAXI_TWEAKS_EX_RAN_RED_CAP
		
			CANCEL_TIMER(txExcitementArray[TXET_QUICKSTOP].tSleepTimer)
			CANCEL_TIMER(txExcitementArray[TXET_POWERSLIDE].tSleepTimer)
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_IN_ONCOMING_LANE(TaxiStruct &myTaxiData)
	
	//Driving against traffic
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		INT iTempTimeDroveAgainstTraffic = GET_TIME_SINCE_PLAYER_DROVE_AGAINST_TRAFFIC(PLAYER_ID())
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"Hit Oncoming") 
		
		#IF IS_DEBUG_BUILD
			sDebugTextPD[1] = "Oncoming = "
			sDebugTextPD[1] += TAXI_UTILS_GET_STRING_FROM_INT_SP(iTempTimeDroveAgainstTraffic)
			DRAW_DEBUG_TEXT_2D(sDebugTextPD[1],<<0.8,0.4,0.0>>)
		#ENDIF
		
		IF iTempTimeDroveAgainstTraffic = 0 
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer)
			ELSE
			
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer) > TAXI_TWEAKS_TIME_ONCOMING_TRAFFIC
					CANCEL_TIMER(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer)
					RETURN TRUE
				ELSE
					
					//Reset timers for other checks that shouldn't fire if this one is firing, other until we make this exclusive.
					CDEBUG1LN(DEBUG_OJ_TAXI,"Car is in the wrong lane for  ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer), "", "seconds.") 
				ENDIF
			
			ENDIF
		
		ELSE
			//Vehicle is no longer in wrong lane.
			CANCEL_TIMER(txExcitementArray[TXET_WRONG_LANE].tWakeupTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_ON_SIDEWALK_FOR_TIME(TaxiStruct &myTaxiData)
	
	
	//Driving on sidewalk
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		INT iTempTimeDroveOnSidewalk = GET_TIME_SINCE_PLAYER_DROVE_ON_PAVEMENT(PLAYER_ID())
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"Hit sidewalk") 
		
		#IF IS_DEBUG_BUILD
			sDebugTextPD[3] = "Sidewalk = "
			sDebugTextPD[3] += TAXI_UTILS_GET_STRING_FROM_INT_SP(iTempTimeDroveOnSidewalk)
			DRAW_DEBUG_TEXT_2D(sDebugTextPD[3],<<0.8,0.42,0.0>>)
		#ENDIF
		
		IF iTempTimeDroveOnSidewalk = 0
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SIDEWALK].tWakeupTimer)
				START_TIMER_NOW(txExcitementArray[TXET_SIDEWALK].tWakeupTimer)
			ELSE
			
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SIDEWALK].tWakeupTimer) > TAXI_TWEAKS_TIME_SIDEWALKIN
					CANCEL_TIMER(txExcitementArray[TXET_SIDEWALK].tWakeupTimer)
					RETURN TRUE
				ELSE
					
					//Reset timers for other checks that shouldn't fire if this one is firing, other until we make this exclusive.
					CDEBUG1LN(DEBUG_OJ_TAXI,"Car is on the sidewalk  ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SIDEWALK].tWakeupTimer), "", "seconds.") 
				ENDIF
			
			ENDIF
		
		ELSE
			//Vehicle is no longer in wrong lane.
			CANCEL_TIMER(txExcitementArray[TXET_SIDEWALK].tWakeupTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TAXI_SPEEDING(TaxiStruct &myTaxiData)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		#IF IS_DEBUG_BUILD
			sDebugTextPD[4] = "Speed = "
			sDebugTextPD[4] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_ENTITY_SPEED(myTaxiData.viTaxi))
			DRAW_DEBUG_TEXT_2D(sDebugTextPD[4],<<0.8,0.34,0.0>>)
		
		#ENDIF
		
		IF (GET_ENTITY_SPEED(myTaxiData.viTaxi) > TAXI_TWEAKS_THRESHOLD_SPEED)
		
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
					START_TIMER_NOW(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
			ELSE
			
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SPEEDING].tWakeupTimer) > TAXI_TWEAKS_TIME_SPEEDIN
					CANCEL_TIMER(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
					RETURN TRUE
				ELSE
					
					//Reset timers for other checks that shouldn't fire if this one is firing, other until we make this exclusive.
					CDEBUG1LN(DEBUG_OJ_TAXI,"Car is speeding ", "" , GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SPEEDING].tWakeupTimer), "", "seconds.") 
				ENDIF
			
			ENDIF
			
		ELSE
			//Vehicle is no longer in wrong lane.
			CANCEL_TIMER(txExcitementArray[TXET_SPEEDING].tWakeupTimer)
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DID_TAXI_ROLL_RIDE(TaxiStruct &myTaxiData)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi,VS_DRIVER) = PLAYER_PED_ID()
			IF IS_ENTITY_UPSIDEDOWN(myTaxiData.viTaxi)
				IF NOT bRolledUpsideDown
					IF GET_ENTITY_ROLL(myTaxiData.viTaxi) <= TAXI_TWEAKS_ROLL_NEG
					OR GET_ENTITY_ROLL(myTaxiData.viTaxi) >= TAXI_TWEAKS_ROLL_POS
						CDEBUG1LN(DEBUG_OJ_TAXI,"1st half completed, roll is ", GET_ENTITY_ROLL(myTaxiData.viTaxi))
						bRolledUpsideDown = TRUE
					ENDIF
				ENDIF
			ELSE
				IF bRolledUpsideDown
					IF GET_ENTITY_ROLL(myTaxiData.viTaxi) <= 35
					AND GET_ENTITY_ROLL(myTaxiData.viTaxi) >= -35
						CDEBUG1LN(DEBUG_OJ_TAXI,"2nd half completed, roll is ", GET_ENTITY_ROLL(myTaxiData.viTaxi))
						bRolledUpsideDown = FALSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: If it's TAKE IT EASY 
///    
/// PARAMS:
///    myTaxiData - 
///    bWantsExcitement - TAKE IT EASY - FALSE
///    					  NEEDS EXCITEMENT - TRUE
PROC TAXI_UPDATE_EXCITEMENT_COMFORT(TaxiStruct &myTaxiData, TAXI_EXCITMENT_FLAG flagWhich)

	//IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
	myTaxiData.iTaxiOJ_PassengerExcitement += txExcitementArray[flagWhich].iAmountOfExcitement
	CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger Excitment is: ", "", myTaxiData.iTaxiOJ_PassengerExcitement)
	
ENDPROC

PROC TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TAXI_EXCITMENT_FLAG flagWhich, TaxiStruct &myTaxiData)

	txExcitementArray[flagWhich].iTimesActivated++
	
	TAXI_UPDATE_EXCITEMENT_COMFORT(myTaxiData,flagWhich)
	
	CANCEL_TIMER(txExcitementArray[flagWhich].tSleepTimer)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Triggering Taxi Excitement = ", "", GET_TAXI_EXCITEMENT_BY_NAME(flagWhich), " Times Activated = ", "",txExcitementArray[flagWhich].iTimesActivated )
	
	//IF myTaxiData.iTaxiOJ_PassengerExcitement > 10
	
		myTaxiData.bExcitedFlag = TRUE
	//ENDIF
	
ENDPROC
PROC TAXI_TIE_INCREMENT_NUM_WARNINGS(TaxiStruct &myTaxiData, INT iNum = 1)
	myTaxiData.iTaxiOJ_NumDisses += iNum
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_TIE_INCREMENT_NUM_WARNINGS: iWarnings = ", myTaxiData.iTaxiOJ_NumDisses)
ENDPROC
FUNC INT GET_TAXI_TIE_NUM_WARNINGS(TaxiStruct &myTaxiData)
	//CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_TIE_NUM_WARNINGS: iWarnings = ", myTaxiData.iTaxiOJ_NumDisses)
	RETURN myTaxiData.iTaxiOJ_NumDisses
ENDFUNC
PROC TAXI_RESET_EXITEMENT_TIMERS(TaxiStruct &myTaxiData)
	INT iLoopIterator
	
	REPEAT TXET_NUM_TYPES iLoopIterator
		IF IS_TIMER_STARTED(txExcitementArray[iLoopIterator].tSleepTimer)
			RESTART_TIMER_NOW(txExcitementArray[iLoopIterator].tSleepTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"Restarting Excitement Timer ", "", iLoopIterator)
		ENDIF
	ENDREPEAT
	
	//Also reset Boring Timer
	TAXI_RESET_TIMERS(myTaxiData, TT_BORING,0,TRUE)
	
	//Doesn't hurt to clear this here, so you don't get any false hits
	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
ENDPROC

PROC HANDLE_TAXI_EXCITEMENT(TaxiStruct &myTaxiData, BOOL bFreeRideOn, BOOL bNeedsExcitementLeeway = FALSE)
	//These are set in priority from the one's that are the most urgent to the least
	
	//IF myTaxiData.bObjPrinted
		//BORING-------------------------------------------
		//Track if the player is driving safe
		
		//NEEDS EXCITEMENT version of handling boring drivers
		
		IF myTaxiData.tTaxiOJ_MissionType = TXM_01_NEEDEXCITEMENT
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF bNeedsExcitementLeeway
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BORING) > (TAXI_TWEAKS_TIME_BORING + 10.0) 
						IF myTaxiData.bExcitedFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EXCITED_TO_BORED,TRUE)
							myTaxiData.bExcitedFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORING,TRUE)
						ENDIF
						
						myTaxiData.bBoredFlag = TRUE
						TAXI_RESET_TIMERS(myTaxiData, TT_BORING,0,TRUE)
					ENDIF
				ELSE
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BORING) > TAXI_TWEAKS_TIME_BORING
						IF myTaxiData.bExcitedFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EXCITED_TO_BORED,TRUE)
							myTaxiData.bExcitedFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORING,TRUE)
						ENDIF
						
						myTaxiData.bBoredFlag = TRUE
						TAXI_RESET_TIMERS(myTaxiData, TT_BORING,0,TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//TAKE IT EASY version of handling boring (read: good) drivers
		IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BORING) > TAXI_TWEAKS_TIME_RELAXING
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF myTaxiData.bExcitedFlag
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EXCITED_TO_BORED,TRUE)
						myTaxiData.bExcitedFlag = FALSE
					ELSE
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORING,TRUE)
					ENDIF
					
					myTaxiData.bBoredFlag = TRUE
					TAXI_RESET_TIMERS(myTaxiData, TT_BORING,0,TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		//Speeding------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_SPEEDING)
			
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SPEEDING].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_SPEEDING].tSleepTimer)
				
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SPEEDING].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_SPEEDING_CHECK
					
					IF IS_TAXI_SPEEDING(myTaxiData)

						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_SPEEDING].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)	
						
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_SPEEDING,myTaxiData)
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: SPEEDING")
						
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_SPEEDING].sType,txExcitementArray[TXET_SPEEDING].iTimesActivated)
						ENDIF
						#ENDIF//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		//AIR-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_AIR)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_AIR].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_AIR].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_AIR].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_AIR_CHECK
					
					IF IS_TAXI_AIRBORNE_FOR_TIME(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_AIR,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_AIR].tDialogueIndex,TRUE)
						ENDIF
					
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: AIR")
						
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_AIR].sType,txExcitementArray[TXET_AIR].iTimesActivated)
						ENDIF
						#ENDIF//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		
		//POWERSLIDING-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_POWERSLIDE)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_POWERSLIDE].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_POWERSLIDE].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_POWERSLIDE].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_DRIFT_CHECK
					
					IF IS_TAXI_POWERSLIDING_FOR_TIME(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_POWERSLIDE,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_POWERSLIDE].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: POWERSLIDE")
						
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_POWERSLIDE].sType,txExcitementArray[TXET_POWERSLIDE].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		//-----------------------------------------------------
		
		//-----------------------------------------------------
		//TOOK DAMAGE-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_TOOKDAMAGE].tSleepTimer)
				//Set Vehicle Health
				IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
					myTaxiData.iOldVehicleHealth =  GET_ENTITY_HEALTH(myTaxiData.viTaxi)
					START_TIMER_NOW(txExcitementArray[TXET_TOOKDAMAGE].tSleepTimer)
				ENDIF
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_TOOKDAMAGE].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_TOOK_DAMAGE
				OR txExcitementArray[TXET_TOOKDAMAGE].iTimesActivated = 0	
				
					IF DID_TAXI_TAKE_DAMAGE(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_TOOKDAMAGE,myTaxiData)
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: TOOK DAMAGE")	
					
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_TOOKDAMAGE].sType,txExcitementArray[TXET_TOOKDAMAGE].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
		
				ENDIF
			ENDIF
		ENDIF	
		
		
		//ONCOMING TRAFFIC-----------------------------------------
//		IF NOT (myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY)
			IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_WRONGLANE)
				IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_WRONG_LANE].tSleepTimer)
					START_TIMER_NOW(txExcitementArray[TXET_WRONG_LANE].tSleepTimer)
				
				ELSE
					//Timer has been started - Lets check if it' been dormant long enough before firing again		
					IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_WRONG_LANE].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_ONCOMING_CHECK
						
						IF IS_TAXI_IN_ONCOMING_LANE(myTaxiData)
							
							TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)	
							TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_WRONG_LANE,myTaxiData)
							IF myTaxiData.bBoredFlag
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
								myTaxiData.bBoredFlag = FALSE
							ELSE
								SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_WRONG_LANE].tDialogueIndex,TRUE)
							ENDIF
							
							TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: WRONG LANE")	
							
							#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
							IF bFreeRideOn
								CLEAR_HELP()
								PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_WRONG_LANE].sType,txExcitementArray[TXET_WRONG_LANE].iTimesActivated)
							ENDIF
							#ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
//		ENDIF
		
		//ON SIDEWALK-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_SIDEWALK)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SIDEWALK].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_SIDEWALK].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SIDEWALK].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_SIDEWALK_CHECK
					
					IF IS_TAXI_ON_SIDEWALK_FOR_TIME(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_SIDEWALK,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_SIDEWALK].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: SIDEWALK")	
						
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_SIDEWALK].sType,txExcitementArray[TXET_SIDEWALK].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		
		//ROLLED-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_ROLL].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_ROLL].tSleepTimer)
			ELSE
				
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_ROLL].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_ROLL_CHECK
				OR txExcitementArray[TXET_ROLL].iTimesActivated = 0	
				
					IF DID_TAXI_ROLL_RIDE(myTaxiData)
			
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_ROLL,myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_ROLL].tDialogueIndex,TRUE,FALSE, TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: ROLL")						
						
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_ROLL].sType,txExcitementArray[TXET_ROLL].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		//HITPED-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_HIT_PED].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_HIT_PED].tSleepTimer)
				
			ELIF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_HIT_PED].tSleepTimer) <= TAXI_TWEAKS_EX_DELAY_FOR_HIT_PED
				//Clear this here also otherwise this will fire as soon as the timer lets up again, regardless if the player has hit anybody.
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_HIT_PED].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_HIT_PED
				OR txExcitementArray[TXET_HIT_PED].iTimesActivated = 0	
				
					IF DID_TAXI_HIT_PED(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_HIT_PED,myTaxiData)
					
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
					
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_HIT_PED].tDialogueIndex,TRUE,FALSE, TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)	
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_HIT_PED].sType,txExcitementArray[TXET_HIT_PED].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//OFFROAD-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_OFFROAD)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_OFFROAD].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_OFFROAD].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_OFFROAD].tSleepTimer) > 10.0
		
					IF IS_TAXI_OFFROAD(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_OFFROAD,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_OFFROAD].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: OFFROAD")	
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_OFFROAD].sType,txExcitementArray[TXET_OFFROAD].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//NEAR MISS---------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_NEAR_MISS)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_NEAR_MISS].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_NEAR_MISS].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_NEAR_MISS].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_NEARMISS_CHECK
		
					IF IS_TAXI_NEAR_MISS(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_NEAR_MISS,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_NEAR_MISS].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: NEAR MISS")	
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_NEAR_MISS].sType,txExcitementArray[TXET_NEAR_MISS].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//SWERVING---------------------------------------------
		//IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_SWERVE)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_SWERVING].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_SWERVING].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_SWERVING].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_SWERVING_CHECK
					
					IF IS_TAXI_SWERVING(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_SWERVING,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_SWERVING].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: SWERVE")	
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_SWERVING].sType,txExcitementArray[TXET_SWERVING].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF	
		//ENDIF
		
		//-----------------------------------------------------
		
		
		//REVERSING--------------------------------------------
		//IF myTaxiData.tTaxiOJ_MissionType = TXM_02_TAKEITEASY
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_REVERSE)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_REVERSE].tSleepTimer)
				START_TIMER_NOW(txExcitementArray[TXET_REVERSE].tSleepTimer)
			
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_REVERSE].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_REVERSE_CHECK
					
					IF IS_TAXI_IN_REVERSE(myTaxiData)
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)	
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_REVERSE,myTaxiData)
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_REVERSE].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: REVERSE")
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_REVERSE].sType,txExcitementArray[TXET_REVERSE].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		//ENDIF
		
		//-----------------------------------------------------
		
		//SUDDEN STOPS-----------------------------------------
		IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_QUICKSTOP)
			IF NOT IS_TIMER_STARTED(txExcitementArray[TXET_QUICKSTOP].tSleepTimer)
				START_TIMER_AT(txExcitementArray[TXET_QUICKSTOP].tSleepTimer,0.0)
			ELSE
				//Timer has been started - Lets check if it' been dormant long enough before firing again		
				IF GET_TIMER_IN_SECONDS(txExcitementArray[TXET_QUICKSTOP].tSleepTimer) > TAXI_TWEAKS_EX_DELAY_FOR_QSTOP_CHECK
					
					IF IS_TAXI_QUICK_SPEED_CHANGE(myTaxiData)
						
						IF myTaxiData.bBoredFlag
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BORED_TO_EXCITED,TRUE)
							myTaxiData.bBoredFlag = FALSE
						ELSE
							SET_NEXT_TAXI_SPEECH(myTaxiData,txExcitementArray[TXET_QUICKSTOP].tDialogueIndex,TRUE)
						ENDIF
						
						TAXI_TIE_INCREMENT_NUM_WARNINGS(myTaxiData)
						TRIGGER_TAXI_EXCITEMENT_BY_TYPE(TXET_QUICKSTOP,myTaxiData)
						
						TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"HANDLE_TAXI_EXCITEMENT: QUICK STOP")					
	
						#IF IS_DEBUG_BUILD//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~					
						IF bFreeRideOn
							CLEAR_HELP()
							PRINT_HELP_WITH_NUMBER(txExcitementArray[TXET_QUICKSTOP].sType,txExcitementArray[TXET_QUICKSTOP].iTimesActivated)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		UNUSED_PARAMETER(bFreeRideOn)
		
		//-----------------------------------------------------
	//ENDIF
ENDPROC

//EOF
