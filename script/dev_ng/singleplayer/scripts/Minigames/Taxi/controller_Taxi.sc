// controller_Taxi.sc


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "taxi_support_lib.sch"

// ********************************** Script Globals *************************************
THREADID			threadCurTaxi = NULL
TEXT_LABEL_23		taxiThreadName
BLIP_INDEX			blipTempTaxi	
TAXI_OFFER_STATE	eTXOState = TXO_WAIT_PRINT_OFFER
TAXI_MISSION_TYPES	eCurrentType
INT 				iMissionCandID = NO_CANDIDATE_ID
INT					iSelectorTime //LM - time when the selector button was just released
INT					iOfferHelpTime 
FLOAT				fReofferAt = 0.0
BOOL				bOfferClear = FALSE
BOOL				bInCar = FALSE
BOOL				bWasPassed = FALSE
BOOL				bJobRequested = FALSE
BOOL				bOfferHelpShown = FALSE
INT					iDebugThrottle = 1
structTimer			taxiContTimer
structTimer			taxiResetTimer

#IF IS_DEBUG_BUILD
BOOL 		bDoProcedurals
#ENDIF

// THIS IS BAD
structPedsForConversation		controllerConvos
// ********************************** ************** *************************************

PROC THREAD_CLEANUP()
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
		CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), FALSE)
	ENDIF
					
	IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
		CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
		Mission_Over(iMissionCandID)
	ENDIF

	IF (threadCurTaxi <> NULL)
		IF IS_THREAD_ACTIVE(threadCurTaxi)
			TERMINATE_THREAD(threadCurTaxi)
			threadCurTaxi = NULL
		ENDIF
	ENDIF
	
	IF GET_LENGTH_OF_LITERAL_STRING(taxiThreadName) <> 0
		SET_SCRIPT_AS_NO_LONGER_NEEDED(taxiThreadName)
	ENDIF
	
	//Clean Up Blipped Taxi if exists
	IF DOES_BLIP_EXIST(blipTempTaxi)
		REMOVE_BLIP(blipTempTaxi)
	ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"******** controller_Taxi has been ordered to clean up ********")
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT
	CDEBUG1LN(DEBUG_OJ_TAXI,"******** LAUNCHING controller_Taxi ********")
	
	// Initial setup.
	BOOL bDisableTaxiMissions = FALSE
	TAXI_CONTROL_STATE txController = TXC_INIT_CONTROLLER
	
	CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
	CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
	
	// Do we need to cleanup?
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CDEBUG1LN(DEBUG_OJ_TAXI,"...controller_Taxi.sc has been asked to clean up")
		THREAD_CLEANUP()
	ENDIF
	
	// Set us to relaunch when a save is loaded.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_MG_CTRL_TAXI)
	
	WHILE (TRUE)
		//Check the flow bitset for this minigame to see if it is currently allowed to be active.
		bDisableTaxiMissions = NOT GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TAXI))
		
		//ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
		
		#IF IS_DEBUG_BUILD
			TAXI_CONTROLLER_PrintCurrentMissionArray()
			TAXI_CONTROLLER_ToggleProcedurals(bDoProcedurals)
		#ENDIF
		
		INT iWaitTime = 0

		// If we've been disabled, go back to the start.
		IF bDisableTaxiMissions AND (txController != TXC_INIT_TAXI_RANK)
			CDEBUG1LN(DEBUG_OJ_TAXI,"*** IF bDisableTaxiMissions AND (txController != TXC_INIT_TAXI_RANK) -> TXC_INIT_TAXI_RANK ***")
			txController = TXC_INIT_TAXI_RANK
		ENDIF
		
		// If a mission has been fired off while we're trying to get setup, send us back a bit.
		IF (txController = TXC_WAIT_FOR_ACCEPT)
			IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
				CANCEL_TIMER(taxiContTimer)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"*** IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME) -> TXC_INIT_TAXI_RANK ***")
				
				txController = TXC_INIT_TAXI_RANK
			ENDIF
		ENDIF
		
		// See if something is hijacking us in debug, and forcing a mission to start.
		#IF IS_DEBUG_BUILD
			IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_LAUNCHED_VIA_DEBUG)
				CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_LAUNCHED_VIA_DEBUG)
				
				// Okay, we're forcing a mission...
				KILL_ANY_CONVERSATION()

//				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NeedExcitement)
//					eCurrentType = TXM_01_NEEDEXCITEMENT
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_01_NEEDEXCITEMENT, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NeedExcitement)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeItEasy)
//					eCurrentType = TXM_02_TAKEITEASY
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_02_TAKEITEASY, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeItEasy)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Deadline)
//					eCurrentType = TXM_03_DEADLINE
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_03_DEADLINE, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Deadline)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYourBack)
//					eCurrentType = TXM_04_GOTYOURBACK
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_04_GOTYOURBACK, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYourBack)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeToBest)
//					eCurrentType = TXM_05_TAKETOBEST
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_05_TAKETOBEST, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TakeToBest)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_IKnowWay)
//					eCurrentType = TXM_06_IKNOWWAY
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_06_IKNOWWAY, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_IKnowWay)
					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CutYouIn)
//					eCurrentType = TXM_07_CUTYOUIN
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_07_CUTYOUIN, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CutYouIn)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYouNow)
//					eCurrentType = TXM_08_GOTYOUNOW
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_08_GOTYOUNOW, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_GotYouNow)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ClownCar)
//					eCurrentType = TXM_09_CLOWNCAR
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_09_CLOWNCAR, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ClownCar)
//					
//				ELIF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_FollowThatCar)
//					eCurrentType = TXM_10_FOLLOWTHATCAR
//					TAXI_CONTROLLER_FillMissionThreadName(TXM_10_FOLLOWTHATCAR, taxiThreadName, controllerConvos, FALSE)
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_FollowThatCar)
//				
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Procedural)
					eCurrentType = TXM_PROCEDURAL
					TAXI_CONTROLLER_FillMissionThreadName(TXM_PROCEDURAL, taxiThreadName, controllerConvos, FALSE)
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_Procedural)
					
				ENDIF
				
				// Request a script no matter what.
				REQUEST_SCRIPT(taxiThreadName)
				CANCEL_TIMER(taxiContTimer)
				txController = TXC_TAXI_RANK_WAIT
			ENDIF
		#ENDIF
		
		SWITCH (txController)
			CASE TXC_INIT_CONTROLLER
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					// We were running into an issue where returning from MP, the player doesn't have a valid enum. We need to 
					// check this before initting the conversation.
					enumCharacterList playerEnum
					playerEnum = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
					IF (playerEnum = CHAR_MICHAEL) OR (playerEnum = CHAR_TREVOR) OR (playerEnum = CHAR_FRANKLIN)
						CDEBUG1LN(DEBUG_OJ_TAXI,"*** TXC_INIT_CONTROLLER ***")
						
						// Setup the mission order for us.
						TAXI_CONTROLLER_InitMissionArray()
						
						// If we're out of missions, quit.
//						IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ALL_MISSIONS_DONE)
//							txController = TXC_CLEANUP_CONTROLLER
//						ELSE					
							//Set this to let the dev know that the controller is running
							IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CONTROLLER_RUNNING)
								SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_CONTROLLER_RUNNING)
							ENDIF
							
							// Create conversation peds.
							TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(controllerConvos)
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"*** TXC_INIT_CONTROLLER going to TXC_INIT_TAXI_RANK ***")
							
							txController = TXC_INIT_TAXI_RANK
							RESTART_TIMER_NOW(taxiContTimer)
//						ENDIF
					ENDIF
				ENDIF
				
				iWaitTime = 100
			BREAK
			
			CASE TXC_INIT_TAXI_RANK			
				// If we're out of missions, quit.
//				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ALL_MISSIONS_DONE)
//					txController = TXC_CLEANUP_CONTROLLER
//				ENDIF
				
//				PED_INDEX playerPed 
//				VEHICLE_INDEX tempVeh
				
				IF IS_SELECTOR_UI_BUTTON_JUST_RELEASED() // LM let's have 
					iSelectorTime = GET_GAME_TIMER()
				ENDIF
				
				// Need to check the following:
				// - No minigames in progress
				// - Not wanted
				// - Not disabled
				// - Not selecting another player
				// - In a car
				// -- That's a taxi
				// -- In the driver seat
				IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() //IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)
				AND NOT g_bTriggerSceneActive
				
					IF (txController <> TXC_WAIT_FOR_ACCEPT)
						IF NOT IS_MINIGAME_IN_PROGRESS() AND NOT bDisableTaxiMissions AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
						
							//B*519180 - Turn off dispatch if player leaves Taxi - JD 6/11/12
							TOGGLE_TAXI_OJ_DISPATCH()
							
							IF IS_PLAYER_IN_ANY_TAXI()
								
								// If he wasn't preciously in the car, and he now is, print a help if the car is too damaged to offer jobs.
								IF NOT bInCar
									IF (TAXI_CONTROLLER_GetMissionAbility() = TAXINOMISSION_HEALTHGONE)
										IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TAXIHEALTHHELP)
											// Set this help up to use the flow queue to ensure this help cleans up properly and is 
											// guarded against displaying at inappropriate moments. -BenR
											SWITCH GET_FLOW_HELP_MESSAGE_STATUS("TC_H_TOODAMAGED")
												CASE FHS_EXPIRED
													ADD_HELP_TO_FLOW_QUEUE("TC_H_TOODAMAGED", FHP_MEDIUM, 0, 1000)
													BREAK
												CASE FHS_DISPLAYED
													SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TAXIHEALTHHELP)
													BREAK
											ENDSWITCH
//											SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TAXIHEALTHHELP)
//											PRINT_HELP("TC_H_TOODAMAGED")
										ENDIF
									ELSE
										CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_TAXIHEALTHHELP)
									ENDIF
									bInCar = TRUE
								ENDIF
								
								IF (GET_PLAYER_WANTED_LEVEL(Player_ID()) != 0)
									eTXOState = TXO_WAIT_PRINT_OFFER
								ELSE
									IF bWasPassed
										IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ANOTHER_JOB)
											SWITCH GET_FLOW_HELP_MESSAGE_STATUS("TC_ANOTHERJOB")
											
												CASE FHS_EXPIRED
													CDEBUG1LN(DEBUG_OJ_TAXI,"help added to queue: TC_ANOTHERJOB")
													ADD_HELP_TO_FLOW_QUEUE("TC_ANOTHERJOB", FHP_MEDIUM, 0, 1000)
												BREAK
												CASE FHS_DISPLAYED
													CDEBUG1LN(DEBUG_OJ_TAXI,"Another job displayed, clearing out")
													SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ANOTHER_JOB)
												BREAK
											ENDSWITCH
										ENDIF
									ELSE
										// New help check
										IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_FIRSTTIMEINTAXI)
											IF NOT bOfferHelpShown
												SWITCH GET_FLOW_HELP_MESSAGE_STATUS("TC_HOWTOSTART")
												
													CASE FHS_EXPIRED
														CDEBUG1LN(DEBUG_OJ_TAXI,"help added to queue: TC_HOWTOSTART")
														ADD_HELP_TO_FLOW_QUEUE("TC_HOWTOSTART", FHP_MEDIUM, 0, 1000)
													BREAK
													CASE FHS_DISPLAYED
														iOfferHelpTime = GET_GAME_TIMER()
														bOfferHelpShown = TRUE
														CDEBUG1LN(DEBUG_OJ_TAXI,"help displayed in queue: TC_HOWTOSTART")
														IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TC_HOWTOSTART")
															g_savedGlobals.sTaxiData.iTaxiStats[TAXI_HELP_SHOWN]++
														ELSE
															CDEBUG1LN(DEBUG_OJ_TAXI,"taxi tut not being displayed")
														ENDIF
														CDEBUG1LN(DEBUG_OJ_TAXI,"help has now been shown this many times: ", g_savedGlobals.sTaxiData.iTaxiStats[TAXI_HELP_SHOWN])
														IF g_savedGlobals.sTaxiData.iTaxiStats[TAXI_HELP_SHOWN] >= 5
															SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_FIRSTTIMEINTAXI)
														ENDIF
													BREAK
												ENDSWITCH
											ENDIF
	//										IF (GET_GAME_TIMER() % 1000) < 50
	//											CDEBUG1LN(DEBUG_OJ_TAXI,"GET_FLOW_HELP_MESSAGE_STATUS(TC_HOWTOSTART) = ", GET_FLOW_HELP_MESSAGE_STATUS("TC_HOWTOSTART"))
	//										ENDIF
	//									ELSE
	//										IF (GET_GAME_TIMER() % 1000) < 50
	//											CDEBUG1LN(DEBUG_OJ_TAXI,"help already displayed")
	//										ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Clean Up Blipped Taxi if exists
								IF DOES_BLIP_EXIST(blipTempTaxi)
									REMOVE_BLIP(blipTempTaxi)
								ENDIF
											
								// Handle the job offer state.
								SWITCH (eTXOState)		
									CASE TXO_WAIT_PRINT_OFFER
										IF (TAXI_CONTROLLER_GetMissionAbility() = TAXI_CAN_RUN_MISSION)
											IF NOT IS_TIMER_STARTED(taxiContTimer)
												START_TIMER_NOW(taxiContTimer)
											ENDIF
											
											IF IS_TIMER_STARTED(taxiResetTimer)
												CANCEL_TIMER(taxiResetTimer)
											ENDIF
											
											IF GET_TIMER_IN_SECONDS(taxiContTimer) > 20.0
												IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
												AND IS_CONVERSATION_STATUS_FREE()
													
													// Select next mission. eCurrentType = ?
//													eCurrentType = TXM_PROCEDURAL//TAXI_CONTROLLER_GetClosestAvailableMission()
//													
//													TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(controllerConvos)
//													TAXI_CONTROLLER_FillMissionThreadName(eCurrentType, taxiThreadName, controllerConvos)
//													RESTART_TIMER_NOW(taxiContTimer)
													
													eCurrentType = TXM_PROCEDURAL
													taxiThreadName = "Taxi_Procedural"
													ADD_PED_FOR_DIALOGUE(controllerConvos, 8, NULL, "TaxiDispatch")
													CREATE_CONVERSATION(controllerConvos, "OJTXAUD", "OJTX_PRO_OFF", CONV_PRIORITY_NON_CRITICAL_CALL)
													RESTART_TIMER_NOW(taxiContTimer)
													
													CDEBUG1LN(DEBUG_OJ_TAXI,"eTXOState = TXO_WAIT_OFFER_DISP")
													eTXOState = TXO_WAIT_OFFER_DISP
												ELSE
													IF NOT IS_CONVERSATION_STATUS_FREE()
														IF (GET_GAME_TIMER() % 1000) < 50
															CDEBUG1LN(DEBUG_OJ_TAXI,"conversation status is not free, can't offer taxi job")
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									BREAK
			
									CASE TXO_WAIT_OFFER_DISP
										IF NOT IS_TIMER_STARTED(taxiContTimer)
											START_TIMER_NOW(taxiContTimer)
										ENDIF
											
										IF GET_TIMER_IN_SECONDS(taxiContTimer) >= 6.0
											// Set this help up to use the flow queue to ensure this help cleans up properly and is 
											// guarded against displaying at inappropriate moments. -BenR
											SWITCH GET_FLOW_HELP_MESSAGE_STATUS("TC_JOBOFFERED")
												CASE FHS_EXPIRED
													CDEBUG1LN(DEBUG_OJ_TAXI,"help added to queue: TC_JOBOFFERED")
													ADD_HELP_TO_FLOW_QUEUE("TC_JOBOFFERED", FHP_MEDIUM, 0, 1000)
												BREAK
												CASE FHS_DISPLAYED
													bOfferClear = TRUE
													RESTART_TIMER_NOW(taxiContTimer)
													CDEBUG1LN(DEBUG_OJ_TAXI,"help text displayed in queue: TC_JOBOFFERED")
													CDEBUG1LN(DEBUG_OJ_TAXI,"eTXOState = TXO_WAIT_ACCEPT")
													eTXOState = TXO_WAIT_ACCEPT
												BREAK
											ENDSWITCH
											IF (GET_GAME_TIMER() % 1000) < 50
												CDEBUG1LN(DEBUG_OJ_TAXI,"GET_FLOW_HELP_MESSAGE_STATUS(TC_JOBOFFERED) = ", GET_FLOW_HELP_MESSAGE_STATUS("TC_JOBOFFERED"))
											ENDIF
										ENDIF
									BREAK
									
									CASE TXO_WAIT_ACCEPT
										IF NOT IS_TIMER_STARTED(taxiContTimer)
											START_TIMER_NOW(taxiContTimer)
										ENDIF
										
										// If we wait too long to accept, terminate the offer.
										IF GET_TIMER_IN_SECONDS(taxiContTimer) > 15.0
											// We didn't accept the job fast enough, we lost it.
											IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSEDJOB)
												PRINT_HELP("TC_MISSEDJOB")
												SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSEDJOB)
											ENDIF
											
											//Stats Goodness...Figured this would best be kept in here -JD 4/25/11
											//TAXI_STATS_UPDATE(TAXI_STAT_FARES_EXPIRED) //this is done in support now
											
											// Random amount of time till next offer. We add the timer because above, we check for the
											// timer to be > 20.0. so we don't get re-offered immediately.
											fReofferAt = GET_RANDOM_FLOAT_IN_RANGE(10.0, 40.0) + GET_TIMER_IN_SECONDS(taxiContTimer)
											
											CDEBUG1LN(DEBUG_OJ_TAXI,"eTXOState = TXO_WAIT_REOFFER")
											eTXOState = TXO_WAIT_REOFFER
											
										ELIF bOfferClear
											IF GET_TIMER_IN_SECONDS(taxiContTimer) > 6.0
												bOfferClear = FALSE
												CLEAR_HELP()
											ENDIF
										ENDIF
									BREAK
									
									CASE TXO_WAIT_REOFFER
										IF NOT IS_TIMER_STARTED(taxiContTimer)
											START_TIMER_NOW(taxiContTimer)
										ENDIF
										
										// In this case, it's the blank spot between when one offer was missed, and the accepting of a new offer.
										IF GET_TIMER_IN_SECONDS(taxiContTimer) > fReofferAt
											CANCEL_TIMER(taxiContTimer)
											
											CDEBUG1LN(DEBUG_OJ_TAXI,"eTXOState = TXO_WAIT_PRINT_OFFER")							
											eTXOState = TXO_WAIT_PRINT_OFFER
										ENDIF
									BREAK
								ENDSWITCH

								// We've been told a job was taken. Continue.
								IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_HORN)
								AND NOT IS_ENTITY_UPSIDEDOWN(PLAYER_PED_ID())
								AND NOT g_bPlayerCanCallProstitute
								AND (GET_GAME_TIMER() - iSelectorTime) > 3000
								//AND NOT g_sSelectorUI.bSelection
									TAXI_NO_MISSION_REASON eReason
									eReason = TAXI_CONTROLLER_GetMissionAbility()
									
									IF (eReason = TAXI_CAN_RUN_MISSION)
										// Did the player jump the gun?
										IF (eTXOState = TXO_WAIT_PRINT_OFFER) OR (eTXOState = TXO_WAIT_REOFFER)
											// PLAYER IS DEMANDING A JOB BECAUSE NOTHING HAS BEEN OFFERED.
											// If so, just grab a mission, and he's going to do that.
											
											TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(controllerConvos)
											eCurrentType = TXM_PROCEDURAL //TAXI_CONTROLLER_GetClosestAvailableMission()
											TAXI_CONTROLLER_FillMissionThreadName(eCurrentType, 
													taxiThreadName, controllerConvos, FALSE)
											
											bJobRequested = TRUE
											
											// We need to send him to a state where he requests a job.
											txController = TXC_TAXI_RANK_WAIT //TXC_DISPATCH_CONFIRM
										ELSE
											CLEAR_PRINTS()
											CLEAR_HELP()
											
											//Set this to let the system know you're in a taxi mission
											IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSION_RUNNING)
												SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_MISSION_RUNNING)
												CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_UPDATE: Mission IS running")
											ENDIF
											
											txController = TXC_TAXI_RANK_WAIT
										ENDIF
										
										#IF IS_DEBUG_BUILD
										IF bDoProcedurals
											CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi mission override!, Procedural running")
											taxiThreadName = "Taxi_Procedural"
										ENDIF
										#ENDIF
										
										// Request a script no matter what.
										REQUEST_SCRIPT(taxiThreadName)
										CANCEL_TIMER(taxiContTimer)

										iWaitTime = 0
										
									ELIF (eReason = TAXINOMISSION_HEALTHGONE)
										// Can't run a mission because the cab is just about dead.
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											PRINT_HELP("TXC_HEALTH_GONE")
										ENDIF
									ELIF (eReason = TAXINOMISSION_WANTED)
										// Can't run a mission because the player is wanted
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TXC_WANTED_WARN")
												IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_WANTED_HELP)
													PRINT_HELP("TXC_WANTED_WARN")
													SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_WANTED_HELP)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_TIMER_STARTED(taxiResetTimer)
									CDEBUG1LN(DEBUG_OJ_TAXI,"starting reset timer")
									START_TIMER_NOW(taxiResetTimer)
								ELSE
									IF GET_TIMER_IN_SECONDS(taxiResetTimer) < 10.0
										IF (GET_GAME_TIMER() % 1000) < 50
											CDEBUG1LN(DEBUG_OJ_TAXI,"reset timer is at : ", GET_TIMER_IN_SECONDS(taxiResetTimer))
										ENDIF
									ENDIF
									
									IF GET_TIMER_IN_SECONDS(taxiResetTimer) > 10.0
									AND eTXOState != TXO_WAIT_PRINT_OFFER
										CDEBUG1LN(DEBUG_OJ_TAXI,"player out of cab too long, resetting")
										eTXOState = TXO_WAIT_PRINT_OFFER
									ENDIF
								ENDIF
								
								IF IS_FLOW_HELP_MESSAGE_QUEUED("TC_HOWTOSTART")
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed how to start from the help queue")
									REMOVE_HELP_FROM_FLOW_QUEUE("TC_HOWTOSTART")
								ENDIF
								
								IF IS_FLOW_HELP_MESSAGE_QUEUED("TC_JOBOFFERED")
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed job offered from the help queue")
									REMOVE_HELP_FROM_FLOW_QUEUE("TC_JOBOFFERED")
								ENDIF
								
								IF IS_FLOW_HELP_MESSAGE_QUEUED("TC_H_TOODAMAGED")
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed too damaged from the help queue")
									REMOVE_HELP_FROM_FLOW_QUEUE("TC_H_TOODAMAGED")
								ENDIF
								
								IF IS_FLOW_HELP_MESSAGE_QUEUED("TC_ANOTHERJOB")
									CDEBUG1LN(DEBUG_OJ_TAXI,"removed another job from the help queue")
									REMOVE_HELP_FROM_FLOW_QUEUE("TC_ANOTHERJOB")
								ENDIF
								
								IF (GET_GAME_TIMER() - iOfferHelpTime) > 60000
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TC_HOWTOSTART")
								AND bOfferHelpShown
									CDEBUG1LN(DEBUG_OJ_TAXI,"resetting taxi offer help")
									bOfferHelpShown = FALSE
								ENDIF
								
								IF bWasPassed
									CDEBUG1LN(DEBUG_OJ_TAXI,"Another job already shown, clearing passed flag")
									bWasPassed = FALSE
								ENDIF
								
								bInCar = FALSE
								CANCEL_TIMER(taxiContTimer)
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"minigame in progress = ", IS_MINIGAME_IN_PROGRESS())
							CDEBUG1LN(DEBUG_OJ_TAXI,"bDisableTaxiMissions = ", bDisableTaxiMissions)
							CDEBUG1LN(DEBUG_OJ_TAXI,"mission of type = ", IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT))
						
						ENDIF
					
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"txController = ", txController)
					ENDIF
				ELSE
					IF (GET_PLAYER_WANTED_LEVEL(Player_ID()) != 0)
						
						//CDEBUG1LN(DEBUG_OJ_TAXI,"txo state is wait print offer")
						
						eTXOState = TXO_WAIT_PRINT_OFFER
					ENDIF
				ENDIF
			BREAK
			
			
			CASE TXC_TAXI_RANK_WAIT	
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_HORN_ENABLED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)		// To Fix Bug # 551605 - DS	
					PRINTLN("TAXI CONTROLLER: TURNING BACK ON HORN")
				ENDIF
			
				IF HAS_SCRIPT_LOADED(taxiThreadName)
					m_enumMissionCandidateReturnValue eLaunchVal
					eLaunchVal = Request_Mission_Launch(iMissionCandID, MCTID_SELECTED_BY_PLAYER, MISSION_TYPE_MINIGAME)
					
					// Process state we've been handed.
					IF (eLaunchVal = MCRET_ACCEPTED)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_WAIT: Mission permissions accepted ")
						
						g_bTaxiProceduralRunning = TRUE
						CDEBUG1LN(DEBUG_OJ_TAXI,"setting global procedural taxi flag to true")
						
						IF bJobRequested
							txController = TXC_DISPATCH_ASK
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_WAIT: going to _dispatch_ask")
//							#IF IS_DEBUG_BUILD
//							IF bDoProcedurals
//							txController = TXC_ACTIVATE
//							ENDIF
//							#ENDIF
							
						ELSE
							txController = TXC_PLAYER_ACCEPT
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_WAIT: going to _dispatch_confirm")
						ENDIF
						iWaitTime = 0
					ELIF (eLaunchVal = MCRET_DENIED)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_WAIT: Mission permissions DENIED ")
						
						// Denied.
						eTXOState = TXO_WAIT_PRINT_OFFER
						txController = TXC_CLEANUP_TAXI_RANK
					ELSE
						CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("PROCESS_TAXI_DBG_SKIP = Scene is loading.",iDebugThrottle)
					ENDIF
				ENDIF
			BREAK
			
			CASE TXC_DISPATCH_ASK
				
				TEXT_LABEL_23 askJob
				askJob = "OJTX_PLRDE1"
				//askJob += GET_RANDOM_INT_IN_RANGE(1, 3)
				TAXI_ADD_DIALOGUE_OFFSET_FOR_DISPATCH(askJob,TRUE)
				CREATE_CONVERSATION(controllerConvos, "OJTXAUD", askJob, CONV_PRIORITY_HIGH)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_DISPATCH_ASK: job asked for, going to _confirm")
				
				txController = TXC_DISPATCH_CONFIRM
			BREAK
			
			//Dispatch plays her confirmation line
			CASE TXC_DISPATCH_CONFIRM
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					CREATE_CONVERSATION(controllerConvos, "OJTXAUD", "OJTX_DIS_JOB", CONV_PRIORITY_HIGH)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_DISPATCH_CONFIRM: confirmed, going to _activate")
					txController = TXC_PLAYER_ACCEPT
				
				#IF IS_DEBUG_BUILD
				ELSE
					IF (GET_GAME_TIMER() % 500) < 50
						TEXT_LABEL_23 tempCurrentLabel
						tempCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_DISPATCH_CONFIRM: waiting for conversation to stop")
						CDEBUG1LN(DEBUG_OJ_TAXI,tempCurrentLabel)
					ENDIF
				#ENDIF
				ENDIF
			BREAK
			
			CASE TXC_PLAYER_ACCEPT
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
					// Print the convo line.
					TEXT_LABEL_23 specLabel
					specLabel = "OJTX_ACCEPT"
					
					TAXI_ADD_DIALOGUE_OFFSET_FOR_DISPATCH(specLabel,TRUE)
					TAXI_ASSIGN_PLAYER_ENUM_FOR_DIALOGUE(controllerConvos)
					CREATE_CONVERSATION(controllerConvos, "OJTXAUD", specLabel, CONV_PRIORITY_HIGH)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_DISPATCH_CONFIRM: offer accepted, going to _activate")
					txController = TXC_ACTIVATE
				
				#IF IS_DEBUG_BUILD
				ELSE
					IF (GET_GAME_TIMER() % 500) < 50
						TEXT_LABEL_23 tempCurrentLabel
						tempCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_PLAYER_ACCEPT: waiting for conversation to stop")
						CDEBUG1LN(DEBUG_OJ_TAXI,tempCurrentLabel)
					ENDIF
				#ENDIF
				ENDIF
			BREAK
			
			CASE TXC_ACTIVATE
				IF IS_PLAYER_IN_ANY_TAXI()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						bWasPassed = FALSE
						
						IF IS_FLOW_HELP_MESSAGE_QUEUED("TC_HOWTOSTART")
							REMOVE_HELP_FROM_FLOW_QUEUE("TC_HOWTOSTART")
						ENDIF
						
						// Launch script. We're not caring about location, as the script defines where this is going to take place.
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_ACTIVATE: Launching new Taxi Script: ", taxiThreadName)
						threadCurTaxi = START_NEW_SCRIPT(taxiThreadName, MISSION_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED(taxiThreadName)
						
						IF NOT IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
							SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
							SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), TRUE)
						ENDIF
						
						SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
						taxiThreadName = ""
						
						bJobRequested = FALSE
						
						//TAXI_STATS_UPDATE(TAXI_STAT_FARES_ACCEPTED) // this is done in support lib
						
						txController = TXC_TAXI_RANK_UPDATE
						eTXOState = TXO_WAIT_PRINT_OFFER
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_ANY_CONVERSATION()
					ENDIF
					
					bWasPassed = FALSE
					bJobRequested = FALSE
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_ACTIVATE: player out of cab, setting this thread as no launger needed: ", taxiThreadName)
					SET_SCRIPT_AS_NO_LONGER_NEEDED(taxiThreadName)
					
					SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
					taxiThreadName = ""
					
//					CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_UPDATE: Taxi thread ", taxiThreadName, " no longer active.")
//	
//					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData,TAXI_BOOL_INIT_MISSIONS)
//					
//					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
//						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
//						SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), FALSE)
//					ENDIF
//								
//					// Reset the mission we're running, because we aren't running it anymore.
//					eCurrentType = TXM_00_INVALID
					
					eTXOState = TXO_WAIT_PRINT_OFFER
					txController = TXC_TAXI_RANK_UPDATE
					
				ENDIF
			BREAK
			
			CASE TXC_TAXI_RANK_UPDATE			
				IF NOT IS_THREAD_ACTIVE(threadCurTaxi)				
					// Thread is inexplicably dead...
					CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_TAXI_RANK_UPDATE: Taxi thread ", taxiThreadName, " no longer active.")
	
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData,TAXI_BOOL_INIT_MISSIONS)
					
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_SUPPRESSING_TAXI)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(TAXI_GET_TAXI_MODEL_NAME(), FALSE)
					ENDIF
					
					g_bTaxiProceduralRunning = FALSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"setting global procedural taxi flag to false")
					
					// The controller will process our mission array.
					//TAXI_CONTROLLER_ProcessMissionEnding(eCurrentType, bWasPassed)
					
					// Reset the mission we're running, because we aren't running it anymore.
					eCurrentType = TXM_00_INVALID
					
					txController = TXC_CLEANUP_TAXI_RANK
					iWaitTime = 0
				ELSE

					// Check for passed here... this is because there's some mission that continue on, even after flagged as passed,
					// but we want to auto-save earlier.
					IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PASSED)
						CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_PASSED)
						REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(GET_TAXI_OJ_COMPLETION_ENUM(eCurrentType))
						
						bWasPassed = TRUE
						// The controller will process our mission array.
						TAXI_CONTROLLER_ProcessMissionEnding(eCurrentType, bWasPassed)
						
						//B*436518 - Only AutoSave on Mission Pass - JD 4/24/12
						SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
						ODDJOB_AUTO_SAVE()
					ENDIF
				ENDIF
			BREAK
			
			CASE TXC_CLEANUP_TAXI_RANK
				IF IS_BITMASK_AS_ENUM_SET(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
					CLEAR_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_NEED_CLEAR_MISSION_CAND)
					Mission_Over(iMissionCandID)
				ENDIF
				iMissionCandID = NO_CANDIDATE_ID
				
//				IF (TAXI_CONTROLLER_GetMissionCountRemaining() = 0)
//					SET_BITMASK_AS_ENUM(g_savedGlobals.sTaxiData.iGenericTaxiData, TAXI_BOOL_ALL_MISSIONS_DONE)
//				ENDIF
				
				IF NOT IS_AUTOSAVE_REQUEST_IN_PROGRESS()
					SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
					
					//set rich presence back to default.
					SET_DEFAULT_RICH_PRESENCE_FOR_SP()
					
					// Mini-cleanup. Need to terminate the old thread in case we've been asked to force a cleanup due to widgets.
					taxiThreadName = ""
					
					IF (threadCurTaxi <> NULL)
						IF IS_THREAD_ACTIVE(threadCurTaxi)
							TERMINATE_THREAD(threadCurTaxi)
							threadCurTaxi = NULL
						ENDIF
					ENDIF
					
					CANCEL_TIMER(taxiContTimer)
					
					SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
					
					// Back to start!
					// commenting the following out with the inclusion of procedural.  There's always taxi missions now.
//					IF (TAXI_CONTROLLER_GetMissionCountRemaining() = 0)
//						
//						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_CLEANUP_TAXI_RANK: All taxi missions done")
//						txController = TXC_CLEANUP_CONTROLLER 
//					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXC_CLEANUP_TAXI_RANK:eTXOState = TXO_WAIT_PRINT_OFFER, txController = TXC_INIT_TAXI_RANK")
						eTXOState = TXO_WAIT_PRINT_OFFER
						txController = TXC_INIT_TAXI_RANK 
						iWaitTime = 0
//					ENDIF
				ENDIF
			BREAK
			
			CASE TXC_CLEANUP_CONTROLLER
				CDEBUG1LN(DEBUG_OJ_TAXI,"*** TXC_CLEANUP_CONTROLLER ***")
				THREAD_CLEANUP()
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("The controller_Taxi thread is in an undefined state. Terminating thread!")
				THREAD_CLEANUP()
			BREAK
		ENDSWITCH
		
		WAIT(iWaitTime)
	ENDWHILE
	
ENDSCRIPT

//EOF
