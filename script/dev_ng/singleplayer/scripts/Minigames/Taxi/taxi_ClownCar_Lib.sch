//Taxi_ClownCar_Lib.sch
//Author: John R. Diaz
//Put all the functions for group taxi driving in its own helper lib.

//Changelog-
//	7/5/11 - Added conditional player control giving & taking away in HAS_TAXI_GROUP_REACHED_DESTINATION. There was nasty bug where you wouldn't be given control back due to changes I made to IS_TAXI_FULLY_STOPPED
//
USING "Taxi_ClownCar.sch"

BOOL 		bHotBoxPFXEnabled, bBrokenWindowPFXEnabled, bOpenDoorPFXEnabled
BOOL 		bHotBoxReady
BOOL 		bHotBoxWindowReady
BOOL 		bHotBoxEarly
BOOL		bWeedEffect
BOOL 		bDidPedMakeWay = FALSE
BOOL 		bTaxiPedTasked = FALSE
BOOL 		bTaxiPassengerSpoke = FALSE

FLOAT 		fGoodPaulieDropOffTime 	= 98.0//88.0 //150.0
FLOAT 		fGoodFrankieDropOffTime = 55.0//70.0 //100.0
FLOAT 		fGoodRejinoDropOffTime 	= 60.0 //110.0

INT 		iTaxiWindows[4]
INT			iTaxiDoors[4]

CONST_INT 	k_bBroken 			1
CONST_INT 	iNumDoors	 		4
CONST_INT 	iNumWindows 		4


/// PURPOSE: Wrapper for SET_PED_RELATIONSHIP_GROUP_HASH that encompases a ped check
///    //PARAM NOTES:REL_GROUP_HASH is an enum list in generic.sch
/// PARAMS:
///    PedIndex - 
///    relGroup - 
PROC  SAFE_SET_PED_RELATIONSHIP_GROUP_HASH( PED_INDEX PedIndex, REL_GROUP_HASH relGroup )
	IF NOT IS_PED_INJURED(PedIndex)
		SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex,relGroup)
	ENDIF 
		
ENDPROC

FUNC BOOL TAXI_WINDOW_DOOR_INTEGRITY_MONITOR(TaxiStruct &myTaxiData)
	
	INT iter
	BOOL bAllDoorsAndWindowsClosed = TRUE
	
	REPEAT iNumDoors iter
		IF IS_VEHICLE_DOOR_FULLY_OPEN(myTaxiData.viTaxi, INT_TO_ENUM(SC_DOOR_LIST, iter))
			IF NOT IS_BIT_SET(iTaxiDoors[iter], k_bBroken)
				SET_BIT(iTaxiDoors[iter], k_bBroken)
				CDEBUG1LN(DEBUG_OJ_TAXI,"SET_BIT(iTaxiDoors[iter], k_bBroken) = ", iTaxiDoors[iter], ", iter = ", iter )
			ENDIF
			
			IF IS_VEHICLE_DOOR_DAMAGED(myTaxiData.viTaxi, INT_TO_ENUM(SC_DOOR_LIST, iter))
				bAllDoorsAndWindowsClosed = FALSE
				
				IF NOT bHotBoxEarly
					bHotBoxEarly = TRUE
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(iTaxiDoors[iter], k_bBroken)
				CLEAR_BIT(iTaxiDoors[iter], k_bBroken)
				CDEBUG1LN(DEBUG_OJ_TAXI,"CLEAR_BIT(iTaxiDoors[iter], k_bBroken) = ", iTaxiDoors[iter], ", iter = ", iter )
			ENDIF
		ENDIF
		
		IF NOT IS_VEHICLE_WINDOW_INTACT(myTaxiData.viTaxi, INT_TO_ENUM(SC_WINDOW_LIST, iter))
			bAllDoorsAndWindowsClosed = FALSE
			IF NOT IS_BIT_SET(iTaxiWindows[iter], k_bBroken)
				SET_BIT(iTaxiWindows[iter], k_bBroken)
				CDEBUG1LN(DEBUG_OJ_TAXI,"SET_BIT(iTaxiWindows[iter], k_bBroken) = ", iTaxiWindows[iter], ", iter = ", iter )
			ENDIF
			
			IF NOT bHotBoxEarly
				bHotBoxEarly = TRUE
			ENDIF
			
		ELSE
			IF IS_BIT_SET(iTaxiWindows[iter], k_bBroken)
				CLEAR_BIT(iTaxiWindows[iter], k_bBroken)
				CDEBUG1LN(DEBUG_OJ_TAXI,"CLEAR_BIT(iTaxiWindows[iter], k_bBroken) = ", iTaxiWindows[iter], ", iter = ", iter )
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bAllDoorsAndWindowsClosed
	
ENDFUNC

PROC TAXI_OJ_EXIT_CLOSEST_TO_STREET_GENERAL(VEHICLE_INDEX & viCab, PED_INDEX & piPassenger, VECTOR vGoToPt)

	VECTOR vTempTaxiDoorPosLeft
	VECTOR vTempTaxiDoorPosRight
	
	ENTER_EXIT_VEHICLE_FLAGS eExitFlag
	
	SEQUENCE_INDEX siTemp
	
	IF IS_VEHICLE_DRIVEABLE(viCab)
		IF Not IS_ENTITY_DEAD(piPassenger)
			vTempTaxiDoorPosLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCab, << -1.78774, -1.62399, -0.6206 >>)
			vTempTaxiDoorPosRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCab, << 1.78498, -1.24105, -0.6422>>)
			
			//See which door is closer to the dropoff point
			//Left Door is closer and not blocked
			IF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vGoToPt, FALSE) < GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vGoToPt, FALSE)
			AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger,viCab,VS_BACK_LEFT)

				eExitFlag = ECF_USE_LEFT_ENTRY
				CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of LEFT DOOR because it's closer ","", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vGoToPt, FALSE), "" , " than the right side door which is: ","",  GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vGoToPt, FALSE))
			
			//Right door is closer and not blocked
			ELIF GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vGoToPt, FALSE) >= GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vGoToPt, FALSE)
			AND IS_ENTRY_POINT_FOR_SEAT_CLEAR(piPassenger,viCab,VS_BACK_RIGHT)

				eExitFlag = ECF_USE_RIGHT_ENTRY
				CDEBUG1LN(DEBUG_OJ_TAXI,"Passenger is exiting out of RIGHT DOOR because it's closer than ","", GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosLeft, vGoToPt, FALSE), "" , "where as this distance is: ","",  GET_DISTANCE_BETWEEN_COORDS(vTempTaxiDoorPosRight, vGoToPt, FALSE))

			//All doors are shitty, just get out by any means.
			ELSE
				
				eExitFlag = ECF_WARP_PED
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_EXIT_CLOSEST_TO_STREET_GENERAL is EXITING Passenger out of ANY DOOR")	
			
			ENDIF
			
			VECTOR vScenarioPt = << 312.99612, -274.44791, 52.92882 >>
			
			IF eExitFlag = ECF_WARP_PED
				CDEBUG1LN(DEBUG_OJ_TAXI,"TASK_SEQUENCE to leave any vehicle")	
				CLEAR_SEQUENCE_TASK(siTemp)
				OPEN_SEQUENCE_TASK(siTemp)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					// go for a scenario first
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vScenarioPt, PEDMOVEBLENDRATIO_WALK)
					TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vScenarioPt, 5.0)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPt, PEDMOVEBLENDRATIO_WALK)
				CLOSE_SEQUENCE_TASK(siTemp)
				TASK_PERFORM_SEQUENCE(piPassenger, siTemp)
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"TASK_SEQUENCE to leave a specific door")	
				CLEAR_SEQUENCE_TASK(siTemp)
				OPEN_SEQUENCE_TASK(siTemp)
					TASK_LEAVE_VEHICLE(NULL, viCab, eExitFlag)
					// go for a scenario first
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vScenarioPt, PEDMOVEBLENDRATIO_WALK)
					TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, vScenarioPt, 5.0)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vGoToPt, PEDMOVEBLENDRATIO_WALK)
				CLOSE_SEQUENCE_TASK(siTemp)
				TASK_PERFORM_SEQUENCE(piPassenger, siTemp)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    enables all the hotbox fx for the taxi
/// PARAMS:
///    myTaxiData - 
///    myTaxiGroup - 
PROC ENABLE_TAXI_HOTBOX(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		// Base Case - all windows and doors are closed
		//Windows 1 - Driver Front
		myTaxiGroup.pFxTaxiWindowSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowDF, 
																			<<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
		
		//Window 2 - Passenger Front
		myTaxiGroup.pFxTaxiWindowSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, 
																			vSmokeWindowPF, 
																			<<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6))
	
		//Window 3 - Driver Rear
		myTaxiGroup.pFxTaxiWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, 
																			vSmokeWindowDR, 
																			<<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0))
		
		//Window 4 - Passenger Rear
		myTaxiGroup.pFxTaxiWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, 
																			vSmokeWindowPR, 
																			<<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_HOTBOX - Turned on")
//																			
//		//Broken Window PTFX -----------------
//		//Broken Windows 1 - Driver Front
//		myTaxiGroup.pFxTaxiBrokenWindowSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, 
//																					<<TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_FRONT_X,TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_FRONT_Y, TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_FRONT_Z >>, 
//																					<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
//		
//		//Broken Window 2 - Passenger Front
//		myTaxiGroup.pFxTaxiBrokenWindowSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, 
//																					<<TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_FRONT_X,TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_FRONT_Y, TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_FRONT_Z >>, 
//																					<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6) )
//		
//		//Broken Window 3 - Driver Rear
//		myTaxiGroup.pFxTaxiBrokenWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, 
//																					<<TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_REAR_X,TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_REAR_Y, TAXI_OJ_CONST_PFX_BROKEN_WINDOW_DRIVER_REAR_Z >>, 
//																					<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0) )
//		
//		//Broken Window 4 - Passenger Rear
//		myTaxiGroup.pFxTaxiBrokenWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, 
//																					<<TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_REAR_X,TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_REAR_Y, TAXI_OJ_CONST_PFX_BROKEN_WINDOW_PASSENGER_REAR_Z >>, 
//																					<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
//		
//		//Open Door PTFX ---------------------
//		//Open Door 1 - Driver Front
//		myTaxiGroup.pFxTaxiOpenDoorSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, 
//																				<<TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_Z >>, 
//																				<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
//		
//		//Open Door 2 - Passenger Front
//		myTaxiGroup.pFxTaxiOpenDoorSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, 
//																				<<TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_Z >>, 
//																				<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6) )
//		
//		//Open Door 3 - Driver Rear
//		myTaxiGroup.pFxTaxiOpenDoorSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, 
//																				<<TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_Z >>, 
//																				<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0) )
//		
//		//Open Door 4 - Passenger Rear
//		myTaxiGroup.pFxTaxiOpenDoorSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, 
//																				<<TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_Z >>, 
//																				<<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
//		
		// Set the initial smoke and speed evos to make the smoke not visible at first
		// initial smoke evo = 1.0, initial speed evo = 0.0
		INT iTemp
		REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", 1.0)
			//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "smoke", 1.0)
			//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "smoke", 1.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "speed", 0.0)
			//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "speed", 0.0)
			//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "speed", 0.0)
		ENDREPEAT
		
	ENDIF
ENDPROC

PROC ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup, BOOL bRearWindowsOnly = FALSE)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF bRearWindowsOnly
			//Window 3 - Driver Rear
			myTaxiGroup.pFxTaxiWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowDR, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.75) )
		
			//Window 4 - Passenger Rear
			myTaxiGroup.pFxTaxiWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowPR, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.75)) 
		
		ELSE
			
			//Windows 1 - Driver Front
			myTaxiGroup.pFxTaxiWindowSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowDF, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
			
			//Window 2 - Passenger Front
			myTaxiGroup.pFxTaxiWindowSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowPF, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6) )
		
			//Window 3 - Driver Rear
			myTaxiGroup.pFxTaxiWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowDR, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0) )
			
			//Window 4 - Passenger Rear
			myTaxiGroup.pFxTaxiWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vSmokeWindowPR, <<0,0,0>>,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
		
		ENDIF
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE - Turned on")
	ENDIF
	
	bHotBoxPFXEnabled = bHotBoxPFXEnabled
	bHotBoxPFXEnabled = TRUE
ENDPROC

PROC ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_BROKEN_WINDOW(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	
		//Broken Window PTFX -----------------
		
		//Broken Windows 1 - Driver Front
		myTaxiGroup.pFxTaxiBrokenWindowSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, vSmokeWindowBrokenDF, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
		
		//Broken Window 2 - Passenger Front
		myTaxiGroup.pFxTaxiBrokenWindowSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, vSmokeWindowBrokenPF, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6) )
		
		//Broken Window 3 - Driver Rear
		myTaxiGroup.pFxTaxiBrokenWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, vSmokeWindowBrokenDR, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0) )
		
		//Broken Window 4 - Passenger Rear
		myTaxiGroup.pFxTaxiBrokenWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, vSmokeWindowBrokenPR, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_BROKEN_WINDOW - Turned on")
	ENDIF
	
	bBrokenWindowPFXEnabled = bBrokenWindowPFXEnabled
	bBrokenWindowPFXEnabled = TRUE
ENDPROC

PROC ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_OPEN_DOOR(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	
		//Open Door PTFX ---------------------
		
		//Open Door 1 - Driver Front
		myTaxiGroup.pFxTaxiOpenDoorSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, <<TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_FRONT_Z >>, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6)) 
		
		//Open Door 2 - Passenger Front
		myTaxiGroup.pFxTaxiOpenDoorSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, <<TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_FRONT_Z >>, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.4,0.6) )
		
		//Open Door 3 - Driver Rear
		myTaxiGroup.pFxTaxiOpenDoorSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, <<TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_DRIVER_REAR_Z >>, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0) )
		
		//Open Door 4 - Passenger Rear
		myTaxiGroup.pFxTaxiOpenDoorSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, <<TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_X,TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_Y, TAXI_OJ_CONST_PFX_OPEN_DOOR_PASSENGER_REAR_Z >>, <<0,0,0>>) //,GET_RANDOM_FLOAT_IN_RANGE(0.8,1.0)) 
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_OPEN_DOOR - Turned on")
	ENDIF
	
	bOpenDoorPFXEnabled = bOpenDoorPFXEnabled
	bOpenDoorPFXEnabled = TRUE
ENDPROC

PROC ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_CLOSED_DOOR(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	
		//Closed Door PTFX ---------------------
		
		myTaxiGroup.pFxTaxiClosedDoorSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_door", myTaxiData.viTaxi, 
																				<< TAXI_OJ_CONST_PFX_CLOSED_DOOR_X,
																				TAXI_OJ_CONST_PFX_CLOSED_DOOR_Y, 
																				TAXI_OJ_CONST_PFX_CLOSED_DOOR_Z >>, 
																				<<0,0,0>>)
		
		myTaxiGroup.pFxTaxiClosedWindowSmoke = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_window", myTaxiData.viTaxi, 
																				<< TAXI_OJ_CONST_PFX_CLOSED_WINDOW_X,
																				TAXI_OJ_CONST_PFX_CLOSED_WINDOW_Y, 
																				TAXI_OJ_CONST_PFX_CLOSED_WINDOW_Z >>, 
																				<<0,0,0>>)
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_CLOSED_DOOR - Turned on")
	ENDIF
	
	//SET_PARTICLE_FX_CAM_INSIDE_VEHICLE(TRUE)
	SET_PARTICLE_FX_FORCE_VEHICLE_INTERIOR(TRUE)
//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "smoke", 1.0)
//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "speed", 0.0)
//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "smoke", 1.0)
//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "speed", 0.0)
	
ENDPROC

PROC ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup, VECTOR vWindowDF, VECTOR vWindowDR, VECTOR vWindowPF, VECTOR vWindowPR, FLOAT &fPFXScalar[])
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		
		//Windows 1 - Driver Front
		myTaxiGroup.pFxTaxiWindowSmoke[0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vWindowDF, <<0,0,0>>,fPFXScalar[0]) 
		
		//Window 2 - Passenger Front
		myTaxiGroup.pFxTaxiWindowSmoke[1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vWindowPF, <<0,0,0>>,fPFXScalar[1]) 
	
		//Window 3 - Driver Rear
		myTaxiGroup.pFxTaxiWindowSmoke[2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vWindowDR, <<0,0,0>>,fPFXScalar[2]) 
		
		//Window 4 - Passenger Rear
		myTaxiGroup.pFxTaxiWindowSmoke[3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ojtaxi_hotbox_trail", myTaxiData.viTaxi, vWindowPR, <<0,0,0>>,fPFXScalar[3] ) 
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX - Turned on")
	ENDIF
ENDPROC

PROC DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_CLOSED_DOOR( TAXI_PASSENGER_GROUP &myTaxiGroup)
		
	IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiClosedDoorSmoke)
		STOP_PARTICLE_FX_LOOPED(myTaxiGroup.pFxTaxiClosedDoorSmoke)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiClosedWindowSmoke)
		STOP_PARTICLE_FX_LOOPED(myTaxiGroup.pFxTaxiClosedWindowSmoke)
	ENDIF
	
ENDPROC

PROC DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX( TAXI_PASSENGER_GROUP &myTaxiGroup)
		
	INT iTempCount
	REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTempCount
		IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTempCount])
			STOP_PARTICLE_FX_LOOPED(myTaxiGroup.pFxTaxiWindowSmoke[iTempCount])
			CDEBUG1LN(DEBUG_OJ_TAXI,"DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX - ", iTempCount)
		ENDIF
	ENDREPEAT
	
	bHotBoxPFXEnabled = FALSE
ENDPROC

PROC DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_BROKEN_WINDOW( TAXI_PASSENGER_GROUP &myTaxiGroup)
		
	INT iTempCount
	REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTempCount
		IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTempCount])
			STOP_PARTICLE_FX_LOOPED(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTempCount])
			CDEBUG1LN(DEBUG_OJ_TAXI,"DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_BROKEN_WINDOW - ", iTempCount)
		ENDIF
	ENDREPEAT
	
	bBrokenWindowPFXEnabled = FALSE
ENDPROC

PROC DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_OPEN_DOOR( TAXI_PASSENGER_GROUP &myTaxiGroup)
		
	INT iTempCount
	REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTempCount
		IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTempCount])
			STOP_PARTICLE_FX_LOOPED(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTempCount])
			CDEBUG1LN(DEBUG_OJ_TAXI,"DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_OPEN_DOOR - ", iTempCount)
		ENDIF
	ENDREPEAT
	
	bOpenDoorPFXEnabled = FALSE
ENDPROC

PROC REMOVE_TAXI_OJ_PFX_GROUP_HOT_BOX( TAXI_PASSENGER_GROUP &myTaxiGroup)
		
	INT iTempCount
	REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTempCount
		IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTempCount])
			REMOVE_PARTICLE_FX(myTaxiGroup.pFxTaxiWindowSmoke[iTempCount])
			CDEBUG1LN(DEBUG_OJ_TAXI,"REMOVE_TAXI_OJ_PFX_GROUP_HOT_BOX - ", iTempCount)
		ENDIF
	ENDREPEAT

ENDPROC
PROC TAXI_SET_INTRO_AMB_PEDS_AS_NOLONGER_NEEDED(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup, FLOAT fPedNoLongerNeededDist = 200.0)
	INT i
	IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPlayer)
		REPEAT TAXI_AMB_PED_NUM i
			IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[i])
				IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.piTaxiPlayer, myTaxiGroup.piAmbPed[i]) > fPedNoLongerNeededDist
					SET_PED_AS_NO_LONGER_NEEDED(myTaxiGroup.piAmbPed[i])
				ENDIF
			ENDIF	
		ENDREPEAT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(myTaxiGroup.viAmbStretch)
		IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.piTaxiPlayer, myTaxiGroup.viAmbStretch) > fPedNoLongerNeededDist
			SET_VEHICLE_AS_NO_LONGER_NEEDED(myTaxiGroup.viAmbStretch)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(myTaxiGroup.viAmbSuperD)
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Creates a group of passengers for the taxi to pickup
///    
/// PARAMS:
///    myTaxiData - our Taxi Info
///    locatesData - usually only use one per script
/// RETURNS:
///    
FUNC BOOL TAXI_SPAWN_GROUP_PASSENGERS(TaxiStruct &myTaxiData, VECTOR passengerSpawnPt, VECTOR passengerPickupPt, TAXI_PASSENGER_GROUP & myTaxiGroup)
	
	myTaxiData.vTaxiOJSpawn = passengerSpawnPt
	myTaxiData.vTaxiOJPickup = passengerPickupPt

	TAXI_PREP_TURN_OFF_VEHICLE_GENS(myTaxiData.vTaxiOJPickup)
	CLEAR_AREA_OF_VEHICLES(myTaxiData.vTaxiOJPickup, TAXI_OJ_RADIUS_CLEAR_VEHICLE)
	
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1])
		myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO] 	= g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_1]
		myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE] 	= g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_2]
		myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE] 	= g_PropertySystemData.managementEventPed[PROPERTY_MANAGEMENT_PED_TAXI_CUSTOMER_3]
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], TRUE, TRUE)
	ELSE
		myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO] 	=  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mPassengerModel[0],myTaxiData.vTaxiOJSpawn, -178.76)
		myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE] 	=  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mPassengerModel[1],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiGroup.piPassenger[0],<<0.5,0.5,0>>), 21.77)
		myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE] 	=  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mPassengerModel[1],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiGroup.piPassenger[0],<<0.5,-0.5,0>>), -147.25)
	ENDIF
	
	SET_PED_AS_TAXI_OJ_PASSENGER(myTaxiData,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	myTaxiGroup.bDroppedOff[0] = FALSE
	myTaxiGroup.bDroppedOff[1] = FALSE
	myTaxiGroup.bDroppedOff[2] = FALSE
	
	//SEAT ASSIGNMENT
	myTaxiGroup.passengerSeat[0] = VS_FRONT_RIGHT
	myTaxiGroup.passengerSeat[1] = VS_BACK_LEFT
	myTaxiGroup.passengerSeat[2] = VS_BACK_RIGHT
	
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
		TASK_TURN_PED_TO_FACE_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		TASK_TURN_PED_TO_FACE_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		TASK_TURN_PED_TO_FACE_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	ENDIF
	
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
		ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,5,myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],"TaxiPaulie")
		SET_AMBIENT_VOICE_NAME(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], "TaxiPaulie")
		SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],PCF_CanSayFollowedByPlayerAudio,TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
		ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,4,myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],"TaxiClyde")
		SET_AMBIENT_VOICE_NAME(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], "TaxiClyde")
		SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],PCF_CanSayFollowedByPlayerAudio,TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		
		ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,3,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],"TaxiDarren")//"TaxiRejino")
		SET_AMBIENT_VOICE_NAME(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], "TaxiDarren")
		SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],PCF_CanSayFollowedByPlayerAudio,TRUE)
		
		ADD_RELATIONSHIP_GROUP("TAXI_Passenger", myTaxiData.relPassenger)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
		SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],myTaxiData.relPassenger)	
		SAFE_SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],myTaxiData.relPassenger)
		SAFE_SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],myTaxiData.relPassenger)
		RETURN TRUE

	ENDIF

	
	RETURN FALSE
ENDFUNC

PROC TAXI_INIT_GROUP_PASSENGER_BLIP(TaxiStruct &myTaxiData)
	IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
		//Blip
		myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiData.piTaxiPassenger)
		SET_BLIP_NAME_FROM_TEXT_FILE(myTaxiData.blipTaxiPassenger, "TAXI_BLIP_PASS") //GET_TAXI_OJ_BLIP_NAME(GET_TAXI_MISSION_TYPE(myTaxiData)))
		SET_GPS_FLAGS(GPS_FLAG_IGNORE_ONE_WAY)
		SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger,TRUE)
	ENDIF
ENDPROC

PROC TASK_GROUP_TAXI_WAIT_ANIMS(TAXI_PASSENGER_GROUP &myTaxiGroup,TAXI_PASSENGER_NAMES eName,INT animTime = -1)
	animTime = animTime
	SWITCH eName
		CASE TAXI_GROUP_P0_REJINO
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
			TASK_LOOK_AT_ENTITY(NULL,myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)
			TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "m_impatient_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
		BREAK
		CASE TAXI_GROUP_P1_FRANKIE
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
			TASK_LOOK_AT_ENTITY(NULL,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)			
			TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "m_impatient_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)			
		BREAK
		CASE TAXI_GROUP_P2_PAULIE
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
			TASK_LOOK_AT_ENTITY(NULL,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)			
			TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "m_impatient_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)			
		BREAK	
	ENDSWITCH
	
ENDPROC

PROC INIT_AMBIENT_CROWD_AT_CC_PICKUP(TAXI_PASSENGER_GROUP &myTaxiGroup)
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[3])
		myTaxiGroup.piAmbPed[0] = g_PropertySystemData.managementEventPed[3]
		myTaxiGroup.piAmbPed[1] = g_PropertySystemData.managementEventPed[4]
		myTaxiGroup.piAmbPed[2] = g_PropertySystemData.managementEventPed[5]
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piAmbPed[0], TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piAmbPed[1], TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piAmbPed[2], TRUE, TRUE)
	ELSE
		myTaxiGroup.piAmbPed[0] =  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mAmbPedModel[0],<< -1283.4502, 299.3781, 63.9305 >>,151.9095)
		myTaxiGroup.piAmbPed[1] =  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mAmbPedModel[1],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiGroup.piAmbPed[0],<<1.0,1.0,0>>),90.5877)
		myTaxiGroup.piAmbPed[2] =  CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mAmbPedModel[2],GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiGroup.piAmbPed[0],<<-1.0,1.0,0>>),163.5411)
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventPed[6])
		myTaxiGroup.piAmbPed[3] = g_PropertySystemData.managementEventPed[6]
		myTaxiGroup.piAmbPed[4] = g_PropertySystemData.managementEventPed[7]
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piAmbPed[3], TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.piAmbPed[4], TRUE, TRUE)
	ELSE
		myTaxiGroup.piAmbPed[3] = CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mAmbPedModel[3],<< -1282.8298, 306.2308, 63.9354 >>, 163.5411)
		myTaxiGroup.piAmbPed[4] = CREATE_PED(PEDTYPE_MISSION,myTaxiGroup.mAmbPedModel[4],<< -1280.9871, 305.4652, 63.9499 >>, 90.5877)		
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_PropertySystemData.managementEventVehicle[0])
		myTaxiGroup.viAmbStretch = g_PropertySystemData.managementEventVehicle[0]
		myTaxiGroup.viAmbSuperD = g_PropertySystemData.managementEventVehicle[1]
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.viAmbStretch, TRUE, TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(myTaxiGroup.viAmbSuperD, TRUE, TRUE)
	ELSE
		myTaxiGroup.viAmbStretch = CREATE_VEHICLE(STRETCH,<< -1290.7235, 284.8930, 63.7823 >>, 55.6864)
		myTaxiGroup.viAmbSuperD = CREATE_VEHICLE(SUPERD,<< -1288.9644, 296.9287, 63.8537 >>, 67.6169 )
	ENDIF
	
	SEQUENCE_INDEX tempIndex
	
	INT i
	
	VECTOR vWalkToPoint1 = << -1276.6930, 312.9434, 64.4910 >>
	VECTOR vWalkToPoint2 = << -1281.3488, 315.1646, 64.4805 >>
	
	REPEAT TAXI_AMB_PED_NUM i
		IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[i])
			CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piAmbPed[i])
			
			CLEAR_SEQUENCE_TASK(tempIndex)
			OPEN_SEQUENCE_TASK(tempIndex)
				SWITCH i
					CASE 0
						//TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1000,3000))						
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piAmbPed[1])
						TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "f_impatient_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)	
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 4000)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
					BREAK
					CASE 1
						//TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1000,3000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piAmbPed[0])
						TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "f_impatient_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)	
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 6000)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
					BREAK
					CASE 2
						//TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1000,3000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiGroup.piAmbPed[0])
						TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@intro", "f_impatient_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)	
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 8500)
						TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
						TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
					BREAK
					CASE 3
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,myTaxiGroup.piAmbPed[TAXI_AMB_P6_GUEST3])
						TASK_LOOK_AT_ENTITY(NULL,myTaxiGroup.piAmbPed[TAXI_AMB_P6_GUEST3],TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)
						TASK_CHAT_TO_PED(NULL, myTaxiGroup.piAmbPed[TAXI_AMB_P6_GUEST3], CF_AUTO_CHAT, <<0,0,0>>, 0, 0)
					BREAK
					CASE 4
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,myTaxiGroup.piAmbPed[TAXI_AMB_P4_GUEST1])
						TASK_LOOK_AT_ENTITY(NULL,myTaxiGroup.piAmbPed[TAXI_AMB_P4_GUEST1],TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)
						TASK_CHAT_TO_PED(NULL, myTaxiGroup.piAmbPed[TAXI_AMB_P4_GUEST1], CF_AUTO_CHAT, <<0,0,0>>, 0, 0)
					BREAK	
				ENDSWITCH
			CLOSE_SEQUENCE_TASK(tempIndex)
			TASK_PERFORM_SEQUENCE(myTaxiGroup.piAmbPed[i], tempIndex)
		ENDIF
	ENDREPEAT		
	
	i = 0
	REPEAT TAXI_GROUP_SIZE i
		IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[i])
//		AND (IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.vTaxiOJSpawn, <<3.0,3.0,2.0>>)
//			OR IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiData.vTaxiOJSpawn, <<3.0,3.0,2.0>>)
//			OR IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], myTaxiData.vTaxiOJSpawn, <<3.0,3.0,2.0>>))
		AND ( GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			OR GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK	)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"taxi handle group pickup: group tasked ", i)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiGroup.piPassenger[i], TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
			
			CLEAR_SEQUENCE_TASK(tempIndex)
			OPEN_SEQUENCE_TASK(tempIndex)
				TASK_GROUP_TAXI_WAIT_ANIMS(myTaxiGroup,INT_TO_ENUM(TAXI_PASSENGER_NAMES,i), GET_RANDOM_INT_IN_RANGE(1000,2000))
			CLOSE_SEQUENCE_TASK(tempIndex)
			TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
			
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_GROUP_PASSENGER_IN_TAXI( PED_INDEX ped, TaxiStruct &myTaxiData)
	//INT iThrottle = 2
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF NOT IS_PED_INJURED(ped)

			IF IS_PED_SITTING_IN_VEHICLE(ped,myTaxiData.viTaxi)
				RETURN TRUE
			ENDIF
			
		ELSE
			TAXI_SET_FAIL(myTaxiData, "Passenger was injured", TFS_PASSENGER_DIED)
			CDEBUG1LN(DEBUG_OJ_TAXI,"IS_PASSENGER_IN_TAXI - FAILED - PED injured")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_PASSENGER_IN_TAXI - FAILED - Taxi dead")
		TAXI_SET_FAIL(myTaxiData, "Taxi was destroyed", TFS_TAXI_DISABLED)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GROUP_IN_TAXI(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)
	
	RETURN	(IS_GROUP_PASSENGER_IN_TAXI(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],myTaxiData) 
			AND IS_GROUP_PASSENGER_IN_TAXI(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],myTaxiData) 
			AND IS_GROUP_PASSENGER_IN_TAXI(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],myTaxiData) )
ENDFUNC

FUNC BOOL SET_GROUP_TAXI_WALK_TO_CAB(TaxiStruct &myTaxiData,TAXI_PASSENGER_GROUP &myTaxiGroup, BOOL bWaitForTasksToFinish = TRUE)
	INT i, k 
	SEQUENCE_INDEX tempIndex
	VECTOR tempVector
	REPEAT TAXI_GROUP_SIZE i
		tempVector = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, GET_ENTITY_COORDS(myTaxiGroup.piPassenger[i]))
		IF (bWaitForTasksToFinish 
		AND (GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK	
		OR GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK))
		OR NOT bWaitForTasksToFinish
			
			CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
			CLEAR_SEQUENCE_TASK(tempIndex)
			OPEN_SEQUENCE_TASK(tempIndex)
			TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1,1500))
			//Passenger coming in from RIGHT
			IF tempVector.x > 0
				//TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<2.0,0.0,0.0>>), myTaxiData.fTaxiEnterSpeed, -1)
				TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,-1,myTaxiGroup.passengerSeat[i], myTaxiData.fTaxiEnterSpeed)
			//Passenger coming in from the left
			ELSE
				//TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<-2.0,0.0,0.0>>), myTaxiData.fTaxiEnterSpeed, -1)
				TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,-1,myTaxiGroup.passengerSeat[i], myTaxiData.fTaxiEnterSpeed)
			ENDIF
			CLOSE_SEQUENCE_TASK(tempIndex)
			TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
			
			k++
		ENDIF
	ENDREPEAT
	RETURN (k = ENUM_TO_INT(TAXI_GROUP_SIZE))
ENDFUNC
FUNC BOOL TAXI_SHOULD_PASSENGERS_WALK_BACK_TO_SPAWN_PT(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup) 
	INT i
	
	REPEAT TAXI_GROUP_SIZE i
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiGroup.piPassenger[i], myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST * 0.5
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST * 0.5
		OR GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData,myTaxiGroup.piPassenger[i]) > CONST_TAXI_PASSENGER_PICKUP_DIST * 0.5
			PRINTLN("TAXI_SHOULD_PASSENGERS_WALK_BACK_TO_SPAWN_PT: TAXI_PED_ENTER_CUT_STOP")
			RETURN TRUE
		ENDIF	
	ENDREPEAT
	RETURN FALSE
ENDFUNC
PROC TAXI_TASK_PASSENGERS_WALK_BACK_TO_SPAWN_PT(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup) 
	PRINTLN("TAXI_TASK_PASSENGERS_WALK_BACK_TO_SPAWN_PT")
	INT i
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
	AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
		REPEAT TAXI_GROUP_SIZE i
			CLEAR_PED_TASKS(myTaxiGroup.piPassenger[i])
			TASK_GO_STRAIGHT_TO_COORD(myTaxiGroup.piPassenger[i], myTaxiData.vTaxiOJSpawn, myTaxiData.fTaxiEnterSpeed)		
		ENDREPEAT
	ENDIF
ENDPROC
PROC SET_GROUP_TAXI_IDLE_TASKS(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)
	VECTOR vPlayerTemp, vPassengerTemp
	SEQUENCE_INDEX tempIndex
	INT i 
	INT iLOSProbeHit
	
	REPEAT TAXI_GROUP_SIZE i
		IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[i])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiGroup.piPassenger[i], TRUE)
		
			IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) < 20//35
				IF i = ENUM_TO_INT(TAXI_GROUP_P0_REJINO)
				AND NOT myTaxiGroup.bGroupTaskedToWalkToCab
					myTaxiGroup.bGroupTaskedToWalkToCab = SET_GROUP_TAXI_WALK_TO_CAB(myTaxiData,myTaxiGroup, FALSE)
				ENDIF
			ELIF myTaxiGroup.bGroupTaskedToWalkToCab
			AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
			AND TAXI_SHOULD_PASSENGERS_WALK_BACK_TO_SPAWN_PT(myTaxiData, myTaxiGroup)
				TAXI_TASK_PASSENGERS_WALK_BACK_TO_SPAWN_PT(myTaxiData, myTaxiGroup)
				myTaxiGroup.bGroupTaskedToWalkToCab = FALSE
			ELIF ( GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			OR GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[i], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK	)
			AND (NOT myTaxiGroup.bGroupTaskedToWalkToCab)
			AND (IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.vTaxiOJSpawn, <<5.0,5.0,2.0>>)
			OR IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiData.vTaxiOJSpawn, <<5.0,5.0,2.0>>)
			OR IS_ENTITY_AT_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], myTaxiData.vTaxiOJSpawn, <<5.0,5.0,2.0>>))
				
				vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0, 2, 1>>)
    			vPassengerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPassenger, << 0, 2, 1>>)
				
				iLOSProbeHit = IS_LOS_SHAPETEST_CLEAR_BETWEEN_COORDS(myTaxiData.tShapeTest, vPassengerTemp, vPlayerTemp)
				
				IF i = 0 AND iLOSProbeHit = 0 
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
					
					CLEAR_SEQUENCE_TASK(tempIndex)
					OPEN_SEQUENCE_TASK(tempIndex)
						TASK_GROUP_TAXI_WAIT_ANIMS(myTaxiData,myTaxiGroup,INT_TO_ENUM(TAXI_PASSENGER_NAMES,i), GET_RANDOM_INT_IN_RANGE(1000,2000))
					CLOSE_SEQUENCE_TASK(tempIndex)
					TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
					
				ELIF i = 0 AND iLOSProbeHit = 1
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
					
					CLEAR_SEQUENCE_TASK(tempIndex)
					OPEN_SEQUENCE_TASK(tempIndex)
						TASK_GROUP_TAXI_WAIT_ANIMS(myTaxiData,myTaxiGroup,INT_TO_ENUM(TAXI_PASSENGER_NAMES,i),GET_RANDOM_INT_IN_RANGE(1000,2000),FALSE)
					CLOSE_SEQUENCE_TASK(tempIndex)
					TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
				ELIF i != 0
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
					
					CLEAR_SEQUENCE_TASK(tempIndex)
					OPEN_SEQUENCE_TASK(tempIndex)
						TASK_GROUP_TAXI_WAIT_ANIMS(myTaxiData,myTaxiGroup,INT_TO_ENUM(TAXI_PASSENGER_NAMES,i),GET_RANDOM_INT_IN_RANGE(1000,2000),FALSE)
					CLOSE_SEQUENCE_TASK(tempIndex)
					TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
ENDPROC

PROC SET_GROUP_TAXI_ABANDON_BLIP_ON_OFF(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup, BOOL bOn = TRUE)
	INT iIter
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF bON
			//Picking up
			IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
				SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,0)
				SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, FALSE)
				
				//No passenger, just give god text
				SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_RETURN)
			
			//Dropping Off
			ELSE
				//Hide all dests
				REPEAT TAXI_GROUP_SIZE iIter
					IF DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[iIter])
						SET_BLIP_ALPHA(myTaxiGroup.blipTaxiDropOff[iIter],0)
						//SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
					ENDIF
				ENDREPEAT
				
				//OBJ : "Return to taxi" - GATED
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_LEFT_CAR,TRUE,TRUE)
				//SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_RETURN_TO_TAXI,TAXI_OBJ_LEFT_CAR)
			ENDIF
			
			//Blip Taxi--------
			TAXI_OJ_UPDATE_BLIP_TAXI(myTaxiData)
		ELSE
			//*Sigh* I have to turn off all the mission blips whatever they may be....
			//Leave this for a later day.
			IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
				REMOVE_BLIP(myTaxiData.blipTaxiVehicle)
				//Picking Up
				IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
					SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,255)
					SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, TRUE)
					
					//Give old obj - GATED
					SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,TAXI_OBJ_PICKUP)
				ELSE
				
					REPEAT TAXI_GROUP_SIZE iIter
						IF DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[iIter])
							SET_BLIP_ALPHA(myTaxiGroup.blipTaxiDropOff[iIter],255)
							//SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff, FALSE)
						ENDIF
					ENDREPEAT
					
					//This should give the player his last objective.- GATED
					//SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,TAXI_OBJ_GIVE_MAIN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL UPDATE_GROUP_ENTER_CUTSCENE(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup, BOOL bGroupTooCloseToTaxi = FALSE)
	
	STOP_PHONE_AND_APPS_FOR_TAXI_CUTSCENE()
	
	CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("UPDATE_PASSENGER_ENTER_CUTSCENE",myTaxiData.iTaxiOJ_DebugThrottle)
	
	INT iPassengerWarpTime = 4500
	VECTOR vOffset, vLookAt, vWarpPos, vCamInterpPos1, vCamInterpPos2//vCamInterpRot1, vCamInterpRot2, vGirlsLookAt
	VECTOR vCamCutPos1, vCamCutRot1, vCamCutPos2, vCamCutRot2, vCamCutPosL1, vCamCutRotL1, vCamCutPosL2, vCamCutRotL2
	
	VECTOR vWalkToPoint1 = << -1276.6930, 312.9434, 64.4910 >>
	VECTOR vWalkToPoint2 = << -1281.3488, 315.1646, 64.4805 >>
	
	VEHICLE_SEAT mySeat
	SEQUENCE_INDEX tempIndex
	
	vCamInterpPos1 = << -1282.7733, 290.7357, 66.9617 >>
	//vCamInterpRot1 = << -15.3280, 0.0000, 5.3098 >>
	vCamInterpPos2 = << -1289.3171, 295.6037, 66.8641 >>
	//vCamInterpRot2 = << -13.5525, -0.0000, -66.0536 >>
	
	vCamCutPos1 = << -1295.2147, 293.4460, 65.9716 >>
	vCamCutRot1 = << -6.1914, 0.9119, -84.4908 >>
	vCamCutPos2 = << -1285.0226, 301.5293, 65.5530 >> //<< -1284.2249, 303.5769, 66.1083 >>
	vCamCutRot2 = << 3.6139, 0.9119, -161.2593 >> //<< -3.1133, 0.9119, -172.1514 >>
	
	vCamCutPosL1 = << -1299.6752, 297.5572, 65.8142 >>
	vCamCutRotL1 = << -1.1155, -0.0000, -108.8907 >>
	vCamCutPosL2 = << -1281.0234, 299.8725, 65.7046 >> //<< -1280.0297, 300.2708, 65.8353 >>
	vCamCutRotL2 = << 0.7709, -0.0000, 113.5377 >> //<< -0.2141, 0.0000, 113.5377 >>
	
	INT i
	IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger) AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
		//OH NO!!
		//taxiEnterCut = TAXI_PED_ENTER_CUT_END
	ENDIF
	
	IF taxiEnterCut < TAXI_PED_ENTER_CUT_SKIP AND taxiEnterCut > TAXI_PED_ENTER_CUT_STOP
		IF HANDLE_SKIP_CUTSCENE(iAllowSkipCutsceneTime) 
			
			taxiEnterCut =  TAXI_PED_ENTER_CUT_SKIP
		ENDIF
	ENDIF
	
	IF bGroupTooCloseToTaxi
		iPassengerWarpTime = 0	
	ENDIF
	
	SWITCH taxiEnterCut
		CASE TAXI_PED_ENTER_CUT_STOP
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_STOP")
			
			IF IS_TAXI_FULLY_STOPPED(myTaxiData,FALSE) AND CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
				
				IF IS_TAXI_CLEAR_OF_DEAD_BODIES(myTaxiData)
					myTaxiGroup.bCutScenePlaying = TRUE
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPlayer,TRUE)
					iAllowSkipCutsceneTime = GET_GAME_TIMER()
					SETTIMERA(0)
					taxiEnterCut = TAXI_PED_ENTER_CUT_FIRST
					CDEBUG1LN(DEBUG_OJ_TAXI,"taxiEnterCut -> TAXI_PED_ENTER_CUT_FIRST")
				ELSE
					TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.",TFS_SPOOKED)
				ENDIF
			ENDIF
		BREAK
	
		CASE TAXI_PED_ENTER_CUT_FIRST
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_FIRST", << 0.5, 0.25, 0.0 >>)
			
			ODDJOB_ENTER_CUTSCENE()
			
			CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<0,0,0>>, <<0,0,0>>,60)
			
			REPEAT TAXI_GROUP_SIZE i
				
//				vOffset = <<1.7327, 6.4934, 12.8112>>
//				vLookAt = <<3.7387, 8.2009, 14.2465>> 
                vOffset = <<-3.5082, 2.9335, 0.8747>>
                vLookAt = <<-0.8785, 4.3486, 0.5883>>
				//ODDJOB_ENTER_CUTSCENE()
				
				IF i = ENUM_TO_INT(TAXI_GROUP_P2_PAULIE)
					SET_CAM_COORD(myTaxiData.camTaxi, << -1284.1151, 298.1077, 65.7673 >>) //<< -1276.5935, 287.3171, 67.3009 >>) //<< -1285.5311, 294.3935, 72.7399 >>)
					SET_CAM_ROT(myTaxiData.camTaxi,<< -1.4458, 0.0000, -28.9988 >>) //<< -10.7374, -0.0000, 6.5152 >>) //<< 35.4821, -0.0000, -11.9386 >>)					
					
					IF bGroupTooCloseToTaxi
			            CDEBUG1LN(DEBUG_OJ_TAXI,"taxi is too close, cam is attached to taxi")
						ATTACH_CAM_TO_ENTITY(myTaxiData.camTaxi, myTaxiData.viTaxi, vOffset)
			            POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myTaxiData.viTaxi, vLookAt)					
					ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(myTaxiData.viTaxi), vCamInterpPos1)
					< GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(myTaxiData.viTaxi), vCamInterpPos2)
						CDEBUG1LN(DEBUG_OJ_TAXI,"taxi approached from the right, cam is set to caminterp1")
						SET_CAM_COORD(myTaxiData.camTaxi, vCamCutPos1)
						SET_CAM_ROT(myTaxiData.camTaxi, vCamCutRot1)
						POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], <<1.0,1.0,1.0>>)
//						SET_CAM_COORD(myTaxiData.camTaxi, vCamInterpPos1)
//						SET_CAM_ROT(myTaxiData.camTaxi,vCamInterpRot1)
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"taxi approached from the left, cam is set to caminterp2")
						SET_CAM_COORD(myTaxiData.camTaxi, vCamCutPosL1)
						SET_CAM_ROT(myTaxiData.camTaxi, vCamCutRotL1)
						//POINT_CAM_AT_ENTITY(myTaxiData.camTaxi, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], <<1.0,1.0,1.0>>)
					ENDIF
					
//					SET_CAM_FOV(myTaxiData.camTaxi, 60)
//					SET_CAM_ACTIVE(myTaxiData.camTaxi,TRUE)
//					RENDER_SCRIPT_CAMS(TRUE,!bGroupTooCloseToTaxi, 4000)
					
					SET_CAM_FOV(myTaxiData.camTaxi, 35)
					SET_CAM_ACTIVE(myTaxiData.camTaxi, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					//
		            IF bGroupTooCloseToTaxi
		                vOffset = <<-2.6687, -0.8670, 0.8080>>
		                vLookAt = <<0.6614, -0.6920, 0.1872>>					
						myTaxiData.camInterp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1,1,1>>, <<0,0,0>>, 60.00, FALSE)
			            ATTACH_CAM_TO_ENTITY(myTaxiData.camInterp, myTaxiData.viTaxi, vOffset)
			            POINT_CAM_AT_ENTITY(myTaxiData.camInterp, myTaxiData.viTaxi, vLookAt)
			            SET_CAM_FOV(myTaxiData.camInterp, 60)
			            SETTIMERA(0)
			            SET_CAM_ACTIVE_WITH_INTERP(myTaxiData.camInterp, myTaxiData.camTaxi, 4000)	
					ENDIF
				ENDIF
			ENDREPEAT
			
			SETTIMERA(0)
			taxiEnterCut = TAXI_PED_ENTER_CUT_SECOND
			CDEBUG1LN(DEBUG_OJ_TAXI,"taxiEnterCut -> TAXI_PED_ENTER_CUT_SECOND")
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_SECOND
			IF TIMERA() > 2500
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(myTaxiData.viTaxi), vCamInterpPos1)
					< GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(myTaxiData.viTaxi), vCamInterpPos2)
					STOP_CAM_POINTING(myTaxiData.camTaxi)
					SET_CAM_COORD(myTaxiData.camTaxi, vCamCutPos2)
					SET_CAM_ROT(myTaxiData.camTaxi, vCamCutRot2)
				ELSE
					SET_CAM_COORD(myTaxiData.camTaxi, vCamCutPosL2)
					SET_CAM_ROT(myTaxiData.camTaxi, vCamCutRotL2)
				ENDIF
				
				//SETTIMERA(0)
				taxiEnterCut = TAXI_PED_ENTER_CUT_INTERP_WAIT
				CDEBUG1LN(DEBUG_OJ_TAXI,"taxiEnterCut -> TAXI_PED_ENTER_CUT_INTERP_WAIT")
			ENDIF
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_INTERP_WAIT
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_INTERP_WAIT", << 0.5, 0.25, 0.0 >>)
			
			VECTOR vTemp
			IF TIMERA() > 5000
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					IF NOT IS_CAM_INTERPOLATING(myTaxiData.camTaxi)
						
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
						taxiEnterCut = TAXI_PED_ENTER_CUT_INTERP
						CDEBUG1LN(DEBUG_OJ_TAXI,"taxiEnterCut -> TAXI_PED_ENTER_CUT_INTERP")
					ENDIF
				ELSE
					CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<1,1,1>>, <<0,0,0>>,60.0)
				ENDIF
			ELIF TIMERA() > iPassengerWarpTime
				IF NOT myTaxiGroup.bGroupTaskedGetInCar
//					REPEAT TAXI_GROUP_SIZE i
//						IF myTaxiGroup.passengerSeat[i] = VS_BACK_RIGHT
//							mySeat = VS_BACK_RIGHT
//							vWarpPos = << 1.78498, -1.94105, -0.6422>>//<< 1.78498, -1.24105, -0.6422>>
//						ELIF myTaxiGroup.passengerSeat[i] = VS_BACK_LEFT			
//							mySeat = VS_BACK_LEFT
//							vWarpPos = << -1.78774, -1.62399, -0.6206 >>
//						ELSE
//							mySeat = VS_FRONT_RIGHT
//							vWarpPos = << 1.78498, 0, -0.6422>>//<< 1.78498, 1.24105, -0.6422>>
//						ENDIF
//						vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vWarpPos)
//						SET_ENTITY_COORDS(myTaxiGroup.piPassenger[i], vTemp, FALSE)
//
//						CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
//						IF NOT bGroupTooCloseToTaxi
//							TASK_STAND_STILL(myTaxiGroup.piPassenger[i],GET_RANDOM_INT_IN_RANGE(1000,2000))
//						ENDIF	
//						TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[i],myTaxiData.viTaxi,DEFAULT_TIME_BEFORE_WARP,mySeat, PEDMOVEBLENDRATIO_SPRINT,ECF_WARP_ENTRY_POINT|ECF_RESUME_IF_INTERRUPTED)
//					ENDREPEAT

					myTaxiGroup.bGroupTaskedGetInCar = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_INTERP
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_INTERP", << 0.5, 0.25, 0.0 >>)
			
			REPEAT TAXI_GROUP_SIZE i
				IF myTaxiGroup.passengerSeat[i] = VS_BACK_RIGHT
					mySeat = VS_BACK_RIGHT
					vWarpPos = << 1.78498, -1.94105, -0.6422>>//<< 1.78498, -1.24105, -0.6422>>
				ELIF myTaxiGroup.passengerSeat[i] = VS_BACK_LEFT			
					mySeat = VS_BACK_LEFT
					vWarpPos = << -1.78774, -1.62399, -0.6206 >>
				ELSE
					mySeat = VS_FRONT_RIGHT
					vWarpPos = << 1.78498, 0, -0.6422>>//<< 1.78498, 1.24105, -0.6422>>
				ENDIF
				vTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vWarpPos)
				SET_ENTITY_COORDS(myTaxiGroup.piPassenger[i], vTemp, FALSE)
				
				CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[i])
				IF NOT bGroupTooCloseToTaxi
					TASK_STAND_STILL(myTaxiGroup.piPassenger[i],GET_RANDOM_INT_IN_RANGE(1000,2000))
				ENDIF	
				TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[i],myTaxiData.viTaxi,DEFAULT_TIME_BEFORE_WARP,mySeat, PEDMOVE_RUN,ECF_WARP_ENTRY_POINT|ECF_RESUME_IF_INTERRUPTED)
				
				IF myTaxiGroup.passengerSeat[i] = VS_BACK_RIGHT
					mySeat = VS_BACK_RIGHT
					vOffset = <<2.4426, -0.7154, 0.9986>>
					vLookAt = <<-0.3956, -0.6549, 0.0283>>
				ELIF myTaxiGroup.passengerSeat[i] = VS_BACK_LEFT
					mySeat = VS_BACK_LEFT
					vOffset = <<-2.6687, -0.8670, 0.8080>>
					vLookAt = <<0.6614, -0.6920, 0.1872>>				
				ELSE
					mySeat = VS_FRONT_RIGHT
					vOffset = <<2.4426, -0.7154, 0.9986>>
					vLookAt = <<-0.3956, -0.6549, 0.0283>>					
				ENDIF
				IF i = ENUM_TO_INT(TAXI_GROUP_P2_PAULIE)
				AND NOT bGroupTooCloseToTaxi			
					myTaxiData.camInterp = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1,1,1>>, <<0,0,0>>, 60.00, FALSE)
					ATTACH_CAM_TO_ENTITY(myTaxiData.camInterp, myTaxiData.viTaxi, vOffset)
					POINT_CAM_AT_ENTITY(myTaxiData.camInterp, myTaxiData.viTaxi, vLookAt)
					SET_CAM_FOV(myTaxiData.camInterp, 60)
					SETTIMERA(0)
					
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(myTaxiData.viTaxi), TAXI_OJ_RADIUS_CLEAR_VEHICLE)
					CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(myTaxiData.viTaxi), TAXI_OJ_RADIUS_CLEAR_VEHICLE)	
					
					//SET_CAM_ACTIVE_WITH_INTERP(myTaxiData.camInterp, myTaxiData.camTaxi, 4000)
					SET_CAM_ACTIVE(myTaxiData.camInterp, TRUE)
					//RENDER_SCRIPT_CAMS(TRUE,FALSE)
				ENDIF				
				CLEAR_HELP()
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION()
				ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
			ENDREPEAT
			CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_GROUP_ENTER_CUTSCENE: GOTO TAXI_PED_ENTER_CUT_WAIT")
			taxiEnterCut = TAXI_PED_ENTER_CUT_WAIT
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_WAIT
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_WAIT", << 0.5, 0.25, 0.0 >>)
			
			IF TIMERA() > 4500 //OR IS_PED_SITTING_IN_ANY_VEHICLE(myTaxiData.piTaxiPassenger)
				CDEBUG1LN(DEBUG_OJ_TAXI,"UPDATE_GROUP_ENTER_CUTSCENE: GOTO TAXI_PED_ENTER_CUT_END")
				taxiEnterCut =  TAXI_PED_ENTER_CUT_END
			ENDIF
		
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_ENTER
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_ENTER", << 0.5, 0.25, 0.0 >>)
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_SKIP
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_SKIP", << 0.5, 0.25, 0.0 >>)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_SKIP")
			REPEAT TAXI_GROUP_SIZE i
				IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[i]) AND NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
					IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
						SET_PED_INTO_VEHICLE(myTaxiGroup.piPassenger[i], myTaxiData.viTaxi, myTaxiGroup.passengerSeat[i])
					ENDIF				
				ENDIF				
			ENDREPEAT
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			DESTROY_CAM(myTaxiData.camTaxi)
			DESTROY_CAM(myTaxiData.camInterp)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			CLEAR_HELP()
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION()
			ENABLE_TAXI_SPEECH(myTaxiData,FALSE)
				
				
			taxiEnterCut = TAXI_PED_ENTER_CUT_FADEIN
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_FADEIN
		 	DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_FADEIN", << 0.5, 0.25, 0.0 >>)
			
			IF IS_SCREEN_FADED_IN()
            AND NOT IS_SCREEN_FADING_IN()
			
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_FADEIN")
				ODDJOB_EXIT_CUTSCENE()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE TAXI_PED_ENTER_CUT_END
			DRAW_DEBUG_TEXT_2D("UPDATE_GROUP_ENTER_CUTSCENE - TAXI_PED_ENTER_CUT_END", << 0.5, 0.25, 0.0 >>)
			
			REPEAT TAXI_AMB_NUM_PARTY_GIRLS i
				IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[i])	
					CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piAmbPed[i])
					CLEAR_SEQUENCE_TASK(tempIndex)
					OPEN_SEQUENCE_TASK(tempIndex)
						//TASK_TURN_PED_TO_FACE_COORD(NULL,vGirlsLookAt)
						//TASK_LOOK_AT_COORD(NULL,vGirlsLookAt,TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)						
						SWITCH i
							CASE 0
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
								TASK_WANDER_STANDARD(NULL)
							BREAK
							
							CASE 1
								TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(500, 1000))
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
								TASK_WANDER_STANDARD(NULL)
							BREAK
							
							CASE 2
								TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(2000, 3000))
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
								TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
								TASK_WANDER_STANDARD(NULL)
							BREAK								
						ENDSWITCH
					CLOSE_SEQUENCE_TASK(tempIndex)
					TASK_PERFORM_SEQUENCE(myTaxiGroup.piAmbPed[i], tempIndex)
				ENDIF
			ENDREPEAT		
		
		
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_PED_ENTER_CUT_SKIP")
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			DESTROY_CAM(myTaxiData.camTaxi)
			DESTROY_CAM(myTaxiData.camInterp)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ODDJOB_EXIT_CUTSCENE()
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPlayer,FALSE)
			//myTaxiGroup.bCutScenePlaying = FALSE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPlayer,FALSE)
	
	RETURN FALSE
ENDFUNC

PROC TAXI_HANDLE_PLAYER_DIVE_GROUP(TaxiStruct & myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
	//reset cam
	CLEANUP_TAXI_QUICK_CAM(myTaxiData, TAXI_CAM_PICKUP_INTERP_OUT)
	
	// cleaer the passengers 'get in cab' task
	IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	AND NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	AND NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
	
		CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
		CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"ALL PED TASKS CLEARED")
	ENDIF
	
	// conversation stopper
	KILL_ANY_CONVERSATION()
	
	// cancel the taxi not_in_cab timer so that it can be reset
	SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
ENDPROC

FUNC BOOL UPDATE_PED_GROUP_DRIVE_UP(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup, FLOAT fPedLookDistance)
    
   	VECTOR vPlayerTemp, vPassengerTemp, vPlayerOffset
	INT iLOSProbeHit
	
	INT iBoneWindscreen
	iBoneWindscreen = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "windscreen")
	
	vPlayerOffset = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneWindscreen)
	vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vPlayerOffset)
	vPlayerOffset.y += 1.0
	
	vPlayerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, vPlayerOffset)
	vPassengerTemp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPassenger, << 0, 0.25, 0.9>>)
    
	DRAW_DEBUG_LINE(vPlayerTemp, vPassengerTemp)
	
    SWITCH taxiDriveUp
        CASE TAXI_PED_DRIVE_UP_INIT
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], TRUE)
			taxiDriveUp = TAXI_PED_TRIGGER_SHAPE_TEST
			
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("taxiDriveUp = TAXI_PED_DRIVE_UP_INIT", << 0.7, 0.5, 0.0>>)
			#ENDIF
        BREAK
		CASE TAXI_PED_TRIGGER_SHAPE_TEST
			#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("taxiDriveUp = TAXI_PED_TRIGGER_SHAPE_TEST", << 0.7, 0.5, 0.0>>)
			#ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, myTaxiGroup.piPassenger[eCurrentPickupPed],FALSE)  <=  fPedLookDistance
			AND NOT myTaxiData.bIsTaxiDebugSkipping
            AND ABSF(vPlayerTemp.z - vPassengerTemp.z) < 5.0
            	#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("Within range, grabbing shape tests", << 0.7, 0.55, 0.0>>)
				#ENDIF
				
				iLOSProbeHit = IS_LOS_SHAPETEST_CLEAR_BETWEEN_COORDS(myTaxiData.tShapeTest, vPlayerTemp, vPassengerTemp)
				IF iLOSProbeHit = 0
					CDEBUG1LN(DEBUG_OJ_TAXI,"iLOSProbeHit = TRUE, going to TAXI_PED_DRIVE_UP_DIALOUGE")
					taxiDriveUp = TAXI_PED_DRIVE_UP_DIALOUGE
				ENDIF
			ENDIF
		BREAK
        CASE TAXI_PED_DRIVE_UP_DIALOUGE
			
			CLEAR_PRINTS()
			SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_HAIL,TRUE,FALSE,TRUE)  
            
			CLEAR_PED_TASKS(myTaxiGroup.piPassenger[eCurrentPickupPed])
            
			TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, myTaxiData.viTaxi, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			
			CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            OPEN_SEQUENCE_TASK(myTaxiData.siSeqIndex)
				TASK_PLAY_ANIM(NULL, "oddjobs@towingcome_here", "come_here_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY| AF_LOOPING)
                TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.viTaxi)
				//SET_SEQUENCE_TO_REPEAT(myTaxiData.siSeqIndex, REPEAT_FOREVER)
            CLOSE_SEQUENCE_TASK(myTaxiData.siSeqIndex)
            TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[eCurrentPickupPed], myTaxiData.siSeqIndex)
			
			RETURN TRUE
			
			//taxiDriveUp = TAXI_PED_DRIVE_UP_END
        BREAK
        CASE TAXI_PED_DRIVE_UP_END
            taxiDriveUp = TAXI_PED_DRIVE_UP_INIT
			
			RETURN TRUE
        BREAK
    ENDSWITCH
    
    RETURN FALSE
    
ENDFUNC 

FUNC BOOL SHOULD_TRIGGER_TAXI_GROUP_OPT(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup, INT iTimeTilAction = 5, FLOAT minDist = 35.0, FLOAT fOverRideDist = 8.0, FLOAT minSpeed = 4.0)
    IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
            
        IF NOT IS_ENTITY_OCCLUDED(myTaxiGroup.piPassenger[eCurrentPickupPed]) 
		AND GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiGroup.piPassenger[eCurrentPickupPed]) < minDist
            IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_PASSENGER_ACTION) 
                TAXI_RESET_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
                CDEBUG1LN(DEBUG_OJ_TAXI,"SHOULD_TRIGGER_TAXI_GROUP_OPT - Timer to walk over is started")
            ENDIF
        ELSE
            IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_PASSENGER_ACTION)
                TAXI_CANCEL_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
            ENDIF
        ENDIF
        
        IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PASSENGER_ACTION) > iTimeTilAction 
			AND GET_ENTITY_SPEED(myTaxiData.viTaxi) < minSpeed 
			AND NOT IS_ENTITY_OCCLUDED(myTaxiGroup.piPassenger[eCurrentPickupPed]))
        OR GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiGroup.piPassenger[eCurrentPickupPed]) <= fOverRideDist 
          
            RETURN TRUE
        ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

//Handles the passenger being tasked to go the player's taxi but the player drives away
PROC TAXI_GROUP_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
    IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
        IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[eCurrentPickupPed])
            IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiGroup.piPassenger[eCurrentPickupPed]) > CONST_TAXI_PASSENGER_PICKUP_DIST_FAIL
                IF GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[eCurrentPickupPed], SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
                OR GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[eCurrentPickupPed], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
                    
                    TAXI_SET_FAIL(myTaxiData,"Player abandoned passenger on pickup.",TFS_ABANDONED_PASSENGER)
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_GROUP_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES - Player abandoned the pickup.")
                    
                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL CHECK_GROUP_DISTANCE_FROM_SPAWN_POINT(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
	INT i
	REPEAT TAXI_GROUP_SIZE i
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[i])
	AND NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiGroup.piPassenger[i], myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST
			CDEBUG1LN(DEBUG_OJ_TAXI,"One of the passengers is not in the car and is too far from the spawn point")
			CDEBUG1LN(DEBUG_OJ_TAXI,"passenger was number ", i)
			RETURN TRUE
		ENDIF
	ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TAXI_DISTANCE_FROM_SPAWN_POINT(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
	INT i
	REPEAT TAXI_GROUP_SIZE i
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[i])
	AND NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST
			CDEBUG1LN(DEBUG_OJ_TAXI,"One of the passengers is not in the car and the cab is too far from the spawn point")
			CDEBUG1LN(DEBUG_OJ_TAXI,"passenger was number ", i)
			RETURN TRUE
		ENDIF
	ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TAXI_DISTANCE_FROM_PASSENGERS(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
	INT i
	REPEAT TAXI_GROUP_SIZE i
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[i])
	AND NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
		IF GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiGroup.piPassenger[i]) > CONST_TAXI_PASSENGER_PICKUP_DIST
			CDEBUG1LN(DEBUG_OJ_TAXI,"One of the passengers is not in the car and the cab is too far from that passenger")
			CDEBUG1LN(DEBUG_OJ_TAXI,"passenger was number ", i)
			RETURN TRUE
		ENDIF
	ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TAXI_GROUP_SEATS(TAXI_PASSENGER_GROUP & myTaxiGroup)
	
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
	
		IF IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		OR IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
		OR IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
			IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
			OR NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
			OR NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
				IF NOT IS_TIMER_STARTED(tSeatTimer)
					START_TIMER_NOW(tSeatTimer)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Some of the passengers are in their seat, some are not")
				ELIF GET_TIMER_IN_SECONDS(tSeatTimer) > 15
					CDEBUG1LN(DEBUG_OJ_TAXI,"player has been trolling for over 15 seconds, fail RETURN TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Functionally the same as TAXI_HANDLE_STANDARD_PICKUP, but this one handles multiple passengers being picked up at once. Can probably convert both into a smarter single function
///    
/// PARAMS:
///    myTaxiData - 
///    locatesData - 
///    bCreateBlip - 
/// RETURNS:
///    
FUNC BOOL TAXI_HANDLE_GROUP_PICKUP(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP & myTaxiGroup)
	//INT i, k 
	//Check if anyone is injured
	MONITOR_TAXI_PASSENGER_OK(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	MONITOR_TAXI_PASSENGER_OK(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	MONITOR_TAXI_PASSENGER_OK(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
	
	PED_INDEX piLeftOverPassenger
	VEHICLE_SEAT vsLeftOverPassengersSeat
	
	SEQUENCE_INDEX tempIndex
	
//	VECTOR vWalkToPoint1 = << -1276.6930, 312.9434, 64.4910 >>
//	VECTOR vWalkToPoint2 = << -1281.3488, 315.1646, 64.4805 >>
	
	INT i
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		//Player is in TAXI------------------------------------
		IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
			TAXI_YELL_PLAYER_TO_RETURN(myTaxiData)
			
			IF IS_OJ_TAXI_PLAYER_CONTROL_OFF() //LM if player dives just before a pickup, their controls might be off
				SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
				TAXI_HANDLE_PLAYER_DIVE_GROUP(myTaxiData, myTaxiGroup)
			ENDIF
			
			// need to re-task the passengers here maybe
			
			
			
		ELIF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0)
			TAXI_HANDLE_WANTED_PRE_PICKUP(myTaxiData)
			
		ELSE
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				CDEBUG1LN(DEBUG_OJ_TAXI,"SET_PLAYER_REENTERED_TAXI_OJ called in 1268")
				SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
				SET_GROUP_TAXI_ABANDON_BLIP_ON_OFF(myTaxiData,myTaxiGroup,FALSE)
			ENDIF
			
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_POLICE)
				TAXI_CANCEL_TIMERS(myTaxiData, TT_POLICE)
				CLEAR_PRINTS()
				IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
					SET_BLIP_ALPHA(myTaxiData.blipTaxiPassenger,255)
					SET_BLIP_ROUTE(myTaxiData.blipTaxiPassenger, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
				IF NOT myTaxiData.bIsTaxiDebugSkipping
					//BLIP_TAXI_OJ_PASSENGER(myTaxiData)
					FLOAT fDistaForPickup = (GET_ENTITY_SPEED(myTaxiData.viTaxi)/TAXI_TWEAKS_SPEED_FACTOR) + TAXI_TWEAKS_PICKUP_PAD
					
					IF myTaxiData.bPassengerObjPrinted
						IF GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiGroup.piPassenger[eCurrentPickupPed]) > 50
						OR GET_TIMER_IN_SECONDS_SAFE(tLeftTimer) > 5.0
							TAXI_SET_FAIL(myTaxiData, "Left Passenger", TFS_ABANDONED_PASSENGER)
						ENDIF
					ENDIF
					
					SWITCH myTaxiData.iTaxiOJ_StatesPickup
						
						 //Wait til player is near the passenger to stop
	                    CASE 0
	                        IF GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[eCurrentPickupPed], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), myTaxiGroup.piPassenger[eCurrentPickupPed]) < 20 //10
								AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiGroup.piPassenger[eCurrentPickupPed], myTaxiData.vTaxiOJSpawn) <= CONST_TAXI_PASSENGER_PICKUP_DIST
								AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) <= CONST_TAXI_PASSENGER_PICKUP_DIST)
								IF UPDATE_PED_GROUP_DRIVE_UP(myTaxiData, myTaxiGroup, TAXI_DIALOGUE_MAX_DISTANCE)
		                            CDEBUG1LN(DEBUG_OJ_TAXI,"taxi handle group pickup: UPDATE_PED_DRIVE_UP returned true, going to Case 1")
									
									myTaxiData.iTaxiOJ_StatesPickup++
		                        ENDIF
							ELSE
								
							ENDIF
	                    BREAK
						CASE 1
	                        //Sets the PED to stand and wave at the player until he pulls up close enough to him/her.
	                        IF SHOULD_TRIGGER_TAXI_GROUP_OPT(myTaxiData, myTaxiGroup, 0)
								
								myTaxiData.bPedEntering = TRUE
								
//								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
//								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
//								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
//								
//								TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],myTaxiData.viTaxi,DEFAULT_TIME_BEFORE_WARP,VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
//								TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],myTaxiData.viTaxi,DEFAULT_TIME_BEFORE_WARP,VS_BACK_LEFT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
//								TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],myTaxiData.viTaxi,DEFAULT_TIME_BEFORE_WARP,VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
								
								//Handles telling a left over passenger to GTFO B*513153
								IF bNoEnterCutscene
									piLeftOverPassenger = GET_LEFT_OVER_PASSENGER_IN_TAXI(myTaxiData,vsLeftOverPassengersSeat)
									
									IF NOT IS_ENTITY_DEAD(piLeftOverPassenger)
										TASK_LEAVE_ANY_VEHICLE(piLeftOverPassenger)
									ENDIF
								ENDIF
								
								REPEAT TAXI_GROUP_SIZE i
									
									CLEAR_PED_TASKS(myTaxiGroup.piPassenger[i])
									
									CLEAR_SEQUENCE_TASK(tempIndex)
									OPEN_SEQUENCE_TASK(tempIndex)
										//TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1,1500))
										SWITCH i
											CASE 0
												IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
													TASK_LOOK_AT_ENTITY(NULL, myTaxiData.viTaxi, -1)
													TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1750,2250))
													TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,40000,VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
												ENDIF
											BREAK
											CASE 1
												IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
													TASK_LOOK_AT_ENTITY(NULL, myTaxiData.viTaxi, -1)
													//TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1750,2250))
													TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,40000,VS_BACK_LEFT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
												ENDIF
											BREAK
											CASE 2
												IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[i])
													TASK_LOOK_AT_ENTITY(NULL, myTaxiData.viTaxi, -1)
													TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(750,1250))
													TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,40000,VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK,ECF_RESUME_IF_INTERRUPTED)
												ENDIF
											BREAK
										ENDSWITCH
									CLOSE_SEQUENCE_TASK(tempIndex)
									TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[i], tempIndex)
								ENDREPEAT
								
	                            myTaxiData.iTaxiOJ_StatesPickup++
	                        ELSE
								#IF IS_DEBUG_BUILD
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]),fDistaForPickup,0,0,255,125)
								#ENDIF
								
								IF NOT IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
									CDEBUG1LN(DEBUG_OJ_TAXI,"NOT IS_PLAYER_DRIVING_TAXI_OJ, clearing ped tasks")
									CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
									CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
									CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
								ELSE
									IF GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
									AND GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) > 8
										CDEBUG1LN(DEBUG_OJ_TAXI,"re-tasking player anim")
										
										CLEAR_SEQUENCE_TASK(myTaxiData.siSeqIndex)
							            OPEN_SEQUENCE_TASK(myTaxiData.siSeqIndex)
							                TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.viTaxi)
											//TASK_PLAY_ANIM(NULL, "oddjobs@towingcome_here", "come_here_idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)//, -1, AF_SECONDARY | AF_UPPERBODY )
											TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_nod_yes_hard",NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)
							            CLOSE_SEQUENCE_TASK(myTaxiData.siSeqIndex)
							            TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.siSeqIndex)
									ENDIF
//									IF NOT IS_ENTITY_PLAYING_ANIM(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], "oddjobs@towingcome_here", "come_here_idle_a")
//										CDEBUG1LN(DEBUG_OJ_TAXI,"re-tasking player anim")
//										TASK_PLAY_ANIM(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], "oddjobs@towingcome_here", "come_here_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, 
//														AF_SECONDARY | AF_UPPERBODY| AF_LOOPING)
//									ENDIF
								ENDIF
								
	                        ENDIF       
	                    BREAK
						CASE 2
							IF bNoEnterCutscene
								IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) < 3
									IF NOT IS_TAXI_CLEAR_OF_DEAD_BODIES(myTaxiData)
										CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
										CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
										CLEAR_PED_TASKS_IMMEDIATELY(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
										TAXI_SET_FAIL(myTaxiData,"You had a dead body in your back seat.",TFS_SPOOKED)
									ENDIF
					            ENDIF
							ENDIF
							
							TAXI_GROUP_ENTER_CAB_FAILSAFE_IF_PLAYER_LEAVES(myTaxiData, myTaxiGroup)
	                        
							//Once passenger is in range OR for some reason control was pulled away from player, TRIGGER ENTER CUTSCENE
	                        IF GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) < fDistaForPickup
	                        OR IS_OJ_TAXI_PLAYER_CONTROL_OFF()
	                        
	                            IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi) //LM extra check just in case player dives
	                                IF IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
									AND IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
									AND IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
										TAXI_OJ_REMOVE_PASSENGER_BLIP(myTaxiData)
										
										SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
										SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
										SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], PCF_DontAllowToBeDraggedOutOfVehicle, TRUE)
										
										SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], PCF_OpenDoorArmIK, TRUE)
										
										TAXI_CANCEL_TIMERS(myTaxiData, TT_PASSENGER_ACTION)
										
										CLEAR_GPS_FLAGS()
										
//										REPEAT TAXI_AMB_NUM_PARTY_GIRLS i
//											IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[i])	
//												//CLEAR_PED_TASKS(myTaxiGroup.piAmbPed[i])
//												CLEAR_SEQUENCE_TASK(tempIndex)
//												OPEN_SEQUENCE_TASK(tempIndex)
//													//TASK_TURN_PED_TO_FACE_COORD(NULL,vGirlsLookAt)
//													//TASK_LOOK_AT_COORD(NULL,vGirlsLookAt,TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)						
//													SWITCH i
//														CASE 0
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 500)
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
//															TASK_WANDER_STANDARD(NULL)
//														BREAK
//														
//														CASE 1
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 500)
//															TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(500, 1000))
//															//TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_IMPATIENT", 2000)
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//															//TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(500, 1000))
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
//															TASK_WANDER_STANDARD(NULL)
//														BREAK
//														
//														CASE 2
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@base", "base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 500)
//															TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(3000, 4000))
//															//TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_IMPATIENT", 4000)
//															//TASK_PLAY_ANIM(NULL, "amb@world_human_stand_impatient@female@no_sign@exit", "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
//															//TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(2000, 3000))
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint1, PEDMOVEBLENDRATIO_WALK)
//															TASK_GO_STRAIGHT_TO_COORD(NULL, vWalkToPoint2, PEDMOVEBLENDRATIO_WALK)
//															TASK_WANDER_STANDARD(NULL)
//														BREAK								
//													ENDSWITCH
//												CLOSE_SEQUENCE_TASK(tempIndex)
//												TASK_PERFORM_SEQUENCE(myTaxiGroup.piAmbPed[i], tempIndex)
//											ENDIF
//										ENDREPEAT	
										
	                                    RETURN TRUE
	                                ENDIF
	                            ENDIF
	                        ELSE
								IF (GET_GAME_TIMER() % 1000) < 50
									CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Speed is: ", "", GET_ENTITY_SPEED(myTaxiData.viTaxi),"", " Dist = ","", GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData,myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]), " waiting for distance: ","",fDistaForPickup)
								ENDIF
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]),fDistaForPickup,0,0,255,TAXI_TWEAKS_SPEED_ALPHA)
	                        ENDIF
							
							//if player drives away from the passenger while they try to walk up, reset the pickup states----------------------------------
//							IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST
//							OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJSpawn) > CONST_TAXI_PASSENGER_PICKUP_DIST
//							OR GET_TAXI_DISTANCE_FROM_ENTITY(myTaxiData, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) > CONST_TAXI_PASSENGER_PICKUP_DIST
							
							IF CHECK_TAXI_GROUP_SEATS(myTaxiGroup)
								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
								CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
								TAXI_SET_FAIL(myTaxiData, "Left Passenger", TFS_ABANDONED_PASSENGER)
							ENDIF
							
							IF CHECK_GROUP_DISTANCE_FROM_SPAWN_POINT(myTaxiData, myTaxiGroup)
							OR CHECK_TAXI_DISTANCE_FROM_SPAWN_POINT(myTaxiData, myTaxiGroup)
							OR CHECK_TAXI_DISTANCE_FROM_PASSENGERS(myTaxiData, myTaxiGroup)
							// seat check
								
								CDEBUG1LN(DEBUG_OJ_TAXI,"passenger groups tasked to walk back to spawn point")
								IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
									TASK_FOLLOW_NAV_MESH_TO_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], 
															GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(myTaxiData.vTaxiOJSpawn, 0, <<0.5,0.5,0>>), PEDMOVEBLENDRATIO_WALK)
									eCurrentPickupPed = TAXI_GROUP_P2_PAULIE
								ENDIF
								
								IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
									TASK_FOLLOW_NAV_MESH_TO_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], 
															GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(myTaxiData.vTaxiOJSpawn, 0, <<0.5,0.5,0>>), PEDMOVEBLENDRATIO_WALK)
									eCurrentPickupPed = TAXI_GROUP_P1_FRANKIE
								ENDIF
								
								IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
									TASK_FOLLOW_NAV_MESH_TO_COORD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], 
															myTaxiData.vTaxiOJSpawn, PEDMOVEBLENDRATIO_WALK)
									eCurrentPickupPed = TAXI_GROUP_P0_REJINO
								ENDIF
								
								REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
								myTaxiData.blipTaxiPassenger = CREATE_BLIP_FOR_ENTITY(myTaxiGroup.piPassenger[eCurrentPickupPed])
								
								RESTART_TIMER_NOW(tLeftTimer)
								
								myTaxiData.iTaxiOJ_StatesPickup = 0
								taxiDriveUp = TAXI_PED_DRIVE_UP_INIT
							ENDIF
							
	                    BREAK
					ENDSWITCH
					
				ENDIF
			ELSE
				TAXI_SET_FAIL(myTaxiData, "Passenger injured.")
			ENDIF
			
		ENDIF
	ELSE
		TAXI_SET_FAIL(myTaxiData, "Taxi not drivable.")
	ENDIF
	
	RETURN FALSE
ENDFUNC
    
FUNC BOOL IS_GROUP_INJURED_AND_IN_VEHICLE(TAXI_PASSENGER_GROUP &myTaxiGroup)
	IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	AND NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
		
		IF NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
		AND NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
		AND NOT IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(TAXI_PASSENGER_GROUP & myTaxiGroup)
	
	//Check if Paulie has been dropped off.
	IF NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P2_PAULIE]
		IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE])
			myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE] = CREATE_BLIP_FOR_COORD(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE],TRUE)
			myTaxiGroup.blipCurrentDropoff = myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE]
		ENDIF
		
		//LM  we have to check this just in case player leaves taxi
		IF NOT DOES_BLIP_HAVE_GPS_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE])
			SET_BLIP_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE] ,TRUE)
		ENDIF
		
	//Otherwise it's Frankie's turn to get dropped off	
	ELIF NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P1_FRANKIE]
		IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE])
			myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE] = CREATE_BLIP_FOR_COORD(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE],TRUE)
			myTaxiGroup.blipCurrentDropoff = myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE]
		ENDIF
		
		//LM  we have to check this just in case player leaves taxi
		IF NOT DOES_BLIP_HAVE_GPS_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE])
			SET_BLIP_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE] ,TRUE)
		ENDIF
	
	//Otherwise is Rejino's turn to get dropped off
	ELSE
		IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO])
			myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO] = CREATE_BLIP_FOR_COORD(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO],TRUE)
			myTaxiGroup.blipCurrentDropoff = myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO]
		ENDIF
		
		//LM  we have to check this just in case player leaves taxi
		IF NOT DOES_BLIP_HAVE_GPS_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO])
			SET_BLIP_ROUTE(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO] ,TRUE)
		ENDIF
	ENDIF
			
	
ENDPROC

PROC TAXI_HANDLE_HOTBOX (TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup, BOOL bSmokeFadeOut=FALSE, BOOL bSmokeInstantFadeIn=FALSE)
	
	BOOL bAllDoorsOrWindowsClosed
	
	//Start Hotbox PFX-----------------------------
	IF bHotBoxReady
		IF NOT myTaxiGroup.bIsTimeToTriggerHotBox
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				RESTART_COUNTDOWNTIMER_NOW(myTaxiGroup.tSmokeOnTimer)
				
				myTaxiGroup.bIsTimeToTriggerHotBox = TRUE
				
				ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_CLOSED_DOOR(myTaxiData, myTaxiGroup)
			//	ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE(myTaxiData, myTaxiGroup)
			//	ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_BROKEN_WINDOW(myTaxiData, myTaxiGroup)
			//	ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_OPEN_DOOR(myTaxiData, myTaxiGroup)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox closed door on")
				
				//LM a sneaky add on 12/2/11
				myTaxiGroup.iSmokeStartTime = GET_GAME_TIMER()
				CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox time set at ", myTaxiGroup.iSmokeStartTime)
				
			ENDIF
		ENDIF
	ENDIF
	
	//--------------------------------------------------
	
	IF bHotBoxWindowReady
		IF NOT myTaxiGroup.bIsTimeToTriggerHotBoxWindow
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				myTaxiGroup.bIsTimeToTriggerHotBoxWindow = TRUE
				
				ENABLE_TAXI_HOTBOX(myTaxiData, myTaxiGroup)
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox on")
				
				myTaxiGroup.iSmokeStartTime = GET_GAME_TIMER()
				CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox time set at ", myTaxiGroup.iSmokeStartTime)
				
				myTaxiGroup.iSmokeWindowStartTime = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	bAllDoorsOrWindowsClosed = TAXI_WINDOW_DOOR_INTEGRITY_MONITOR(myTaxiData)
	
	INT iTemp
	
	//Manage Hotbox speed evolution---------------------
	IF myTaxiGroup.bIsTimeToTriggerHotBox
	OR myTaxiGroup.bIsTimeToTriggerHotBoxWindow
		FLOAT fTaxiCurrentSpeed, fTaxiSmokeMaxSpeed, fSpeedEvo
		
		// current speed affects fSpeedEvo, which in turn affects the amount of smoke 
		fTaxiCurrentSpeed = GET_ENTITY_SPEED(myTaxiData.viTaxi)
		fTaxiSmokeMaxSpeed = 4.0
		
		// at 1.0, fSpeedEvo displays the smoke at full speed
		// else get a ratio of the current to max speed and let the smoke flow
		IF fTaxiCurrentSpeed >= fTaxiSmokeMaxSpeed
			fSpeedEvo = 1.0
		ELSE
			fSpeedEvo = fTaxiCurrentSpeed/fTaxiSmokeMaxSpeed
		ENDIF
		
		IF myTaxiGroup.bIsTimeToTriggerHotBoxWindow
			iTemp = 0
			REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp
				//we can set the speed evo for all pfx because it has no effect on visibility, only shape
				//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "speed", fSpeedEvo)
				//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "speed", fSpeedEvo)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
					SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "speed", fSpeedEvo)
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF myTaxiGroup.bIsTimeToTriggerHotBox
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "speed", fSpeedEvo)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "speed", fSpeedEvo)
		ENDIF
	ENDIF
	
	
	//--------------------------------------------------
	
	//Manage Hotbox smoke evolution---------------------
	INT iCurrentPlumeTime, iPlumeTimeMax, iPlumeDecrease
	FLOAT fSmokeEvo
	
	// handling the car going into  or out of speed blur
	// grab the time that it happens
	IF GET_ENTITY_SPEED(myTaxiData.viTaxi) >= 40.0 
	AND NOT myTaxiGroup.bIsCarInSpeedBlur
		myTaxiGroup.iSmokeStartTime = GET_GAME_TIMER()
		CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox time set at ", myTaxiGroup.iSmokeStartTime)
		myTaxiGroup.bIsCarInSpeedBlur = TRUE
		CDEBUG1LN(DEBUG_OJ_TAXI,"############## Speed is over 33.  Setting un plume flags.")
	ENDIF
	
	IF GET_ENTITY_SPEED(myTaxiData.viTaxi) < 40.0
	AND myTaxiGroup.bIsCarInSpeedBlur
	AND NOT bSmokeFadeOut
		myTaxiGroup.iSmokeStartTime = GET_GAME_TIMER()
		CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox time set at ", myTaxiGroup.iSmokeStartTime)
		myTaxiGroup.bIsCarInSpeedBlur = FALSE
		myTaxiGroup.bIsSmokeUnplumed = FALSE
		CDEBUG1LN(DEBUG_OJ_TAXI,"############## Speed is under 33.  Setting plume flags.")
	ENDIF
	
	
	IF bSmokeFadeOut
	AND NOT myTaxiGroup.bIsCarInSpeedBlur
		myTaxiGroup.iSmokeStartTime = GET_GAME_TIMER() 
		CDEBUG1LN(DEBUG_OJ_TAXI,"Hotbox time set at ", myTaxiGroup.iSmokeStartTime)
		myTaxiGroup.bIsCarInSpeedBlur = TRUE
		CDEBUG1LN(DEBUG_OJ_TAXI,"############## First Drop off reached.  Setting un plume flags.")
	ENDIF
	
	
	// plume the smoke
	IF (myTaxiGroup.bIsTimeToTriggerHotBox OR myTaxiGroup.bIsTimeToTriggerHotBoxWindow)
	AND NOT myTaxiGroup.bIsCarInSpeedBlur
	AND NOT bSmokeFadeOut
	
		IF bSmokeInstantFadeIn
			
			fSmokeEvo = 0.0
			myTaxiGroup.bIsSmokeFullyPlumed = TRUE
			myTaxiGroup.bIsCarInSpeedBlur = FALSE
			
			IF myTaxiGroup.bIsTimeToTriggerHotBoxWindow
				// setting the evo for all the windows
				iTemp = 0
				REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp
					IF IS_VEHICLE_DOOR_FULLY_OPEN(myTaxiData.viTaxi, INT_TO_ENUM(SC_DOOR_LIST, iTemp))
						
						//IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp])
						//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "smoke", fSmokeEvo)
						//ENDIF
						
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", 1.0)
						ENDIF
					ELIF NOT IS_VEHICLE_WINDOW_INTACT(myTaxiData.viTaxi, INT_TO_ENUM(SC_WINDOW_LIST, iTemp))
						IF (GET_GAME_TIMER() % 2000) < 50
							CDEBUG1LN(DEBUG_OJ_TAXI,"Window ", itemp, " is not intact")
						ENDIF
						//IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp])
						//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "smoke", fSmokeEvo)
						//ENDIF
						
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", 1.0)
						ENDIF
						
					ELSE
						IF iTemp > 1 // only the back windows
						AND bAllDoorsOrWindowsClosed
							IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
								SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", fSmokeEvo)
							ENDIF
							//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "smoke", 1.0)
							//SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "smoke", 1.0)
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
			
			IF myTaxiGroup.bIsTimeToTriggerHotBox
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "smoke", fSmokeEvo)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "smoke", fSmokeEvo)
			ENDIF
		
		ELSE
			iPlumeTimeMax = 6000
			iCurrentPlumeTime = GET_GAME_TIMER() - myTaxiGroup.iSmokeStartTime
			
			// full plume happens when fSmokeEvo is set to 0.0.
			// else, get a ratio of the time and fade in the smoke
			IF iCurrentPlumeTime >= iPlumeTimeMax
				fSmokeEvo = 0.0
				myTaxiGroup.bIsSmokeFullyPlumed = TRUE
				myTaxiGroup.bIsCarInSpeedBlur = FALSE
			ELSE
				fSmokeEvo = 1.0 - (TO_FLOAT(iCurrentPlumeTime)/TO_FLOAT(iPlumeTimeMax))
			ENDIF
			
			IF myTaxiGroup.bIsTimeToTriggerHotBoxWindow
				// setting the evo for all the windows
				iTemp = 0
				REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp
					IF IS_VEHICLE_DOOR_FULLY_OPEN(myTaxiData.viTaxi, INT_TO_ENUM(SC_DOOR_LIST, iTemp))
						//IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp])
						//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "smoke", fSmokeEvo)
						//ENDIF
						
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", 1.0)
						ENDIF
					ELIF NOT IS_VEHICLE_WINDOW_INTACT(myTaxiData.viTaxi, INT_TO_ENUM(SC_WINDOW_LIST, iTemp))
						IF (GET_GAME_TIMER() % 2000) < 50
							CDEBUG1LN(DEBUG_OJ_TAXI,"Window ", itemp, " is not intact")
						ENDIF
						//IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp])
						//	SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "smoke", fSmokeEvo)
						//ENDIF
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", 1.0)
						ENDIF
					ELSE
						IF iTemp > 1 // only the back windows
						AND bAllDoorsOrWindowsClosed
							IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
								SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", fSmokeEvo)
							ENDIF
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
			
			IF myTaxiGroup.bIsTimeToTriggerHotBox
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "smoke", fSmokeEvo)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "smoke", fSmokeEvo)
			ENDIF
			
		ENDIF
	ENDIF
	
	// un-plume the smoke
	IF (myTaxiGroup.bIsTimeToTriggerHotBox OR myTaxiGroup.bIsTimeToTriggerHotBoxWindow)
	AND ((myTaxiGroup.bIsSmokeFullyPlumed AND NOT myTaxiGroup.bIsSmokeUnplumed)
		OR bSmokeFadeOut)
		
		IF myTaxiGroup.bIsCarInSpeedBlur 
			
			iPlumeDecrease = 6000
			iCurrentPlumeTime = GET_GAME_TIMER() - myTaxiGroup.iSmokeStartTime
			CDEBUG1LN(DEBUG_OJ_TAXI,"############  iCurrentPlumeTime  = ", iCurrentPlumeTime)
			
			// full plume happens when fSmokeEvo is set to 0.0.
			// else, get a ratio of the time and fade in the smoke
			IF iCurrentPlumeTime >= iPlumeDecrease
				fSmokeEvo = 1.0
				myTaxiGroup.bIsSmokeUnplumed = TRUE
			ELSE
				fSmokeEvo = ((TO_FLOAT(iCurrentPlumeTime)/TO_FLOAT(iPlumeDecrease)))
				CDEBUG1LN(DEBUG_OJ_TAXI,"#########  fSmokeEvo  = ", fSmokeEvo)
			ENDIF
			
			IF myTaxiGroup.bIsTimeToTriggerHotBoxWindow
				// setting the evo for all the windows
				iTemp = 0
				REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp
					IF IS_VEHICLE_DOOR_FULLY_OPEN(myTaxiData.viTaxi, INT_TO_ENUM(SC_DOOR_LIST, iTemp))
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiOpenDoorSmoke[iTemp], "smoke", fSmokeEvo)
						ENDIF
					ELIF NOT IS_VEHICLE_WINDOW_INTACT(myTaxiData.viTaxi, INT_TO_ENUM(SC_WINDOW_LIST, iTemp))
						IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp])
							SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiBrokenWindowSmoke[iTemp], "smoke", fSmokeEvo)
						ENDIF
					ENDIF
					
					IF DOES_PARTICLE_FX_LOOPED_EXIST(myTaxiGroup.pFxTaxiWindowSmoke[iTemp])
						CDEBUG1LN(DEBUG_OJ_TAXI,"this is unpluming  = ", iTemp)
						SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp], "smoke", fSmokeEvo)
						CDEBUG1LN(DEBUG_OJ_TAXI,"this loop exists  = ", iTemp)
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"this loop does not exist  = ", iTemp)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF myTaxiGroup.bIsTimeToTriggerHotBox
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedDoorSmoke, "smoke", fSmokeEvo)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiClosedWindowSmoke, "smoke", fSmokeEvo)
			ENDIF
		ENDIF
	ENDIF
	//--------------------------------------------------
	
ENDPROC  

PROC TAXI_CLOWN_CAR_HANDLE_BLIPS(TaxiStruct &myTaxiData, BLIP_INDEX &blipToTurnOn)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		//Player NOT in Taxi
		IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
	
			//Fail Timer if player has been out of cab too long with warning.
			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				//Hide the current obj blip
				IF DOES_BLIP_EXIST(blipToTurnOn)
				AND GET_BLIP_ALPHA(blipToTurnOn) > 0
					SET_BLIP_ALPHA(blipToTurnOn,0)
				ENDIF
				
				TAXI_OJ_UPDATE_BLIP_TAXI(myTaxiData)
				
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_LEFT_CAR,TRUE,TRUE)
			ELSE
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB) > TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR
					TAXI_SET_FAIL(myTaxiData, "Player not in taxi.")
				ENDIF
			ENDIF
			
		//Player returned to the cab-------------------------------------------
		ELSE
			//Even though the fail check only runs when he's not in the cab, it's important to cancel
			//this timer cause it controls other important systems in the dialogue
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
			ENDIF
			
			//*Sigh* I have to turn off all the mission blips whatever they may be....
			//Leave this for a later day.
			IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
				REMOVE_BLIP(myTaxiData.blipTaxiVehicle)
			ENDIF	
				
			
			IF DOES_BLIP_EXIST(blipToTurnOn)
			AND GET_BLIP_ALPHA(blipToTurnOn) < 255
			AND GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1

				SET_BLIP_ALPHA(blipToTurnOn,255)
				SET_BLIP_ROUTE(blipToTurnOn,TRUE)
				//This should give the player his last objective, but ONLY ONCE
				//SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,myTaxiData.tTaxiOJ_ObjectiveCurrent)
			
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Monitors when the player goes wanted and taking care of everything that needs it like: dialogue/objectives
///
///    
/// PARAMS:
///    myTaxiData - our global taxi data
PROC TAXI_OJ_GROUP_MONITOR_WANTED_LEVEL(TaxiStruct &myTaxiData, TAXI_OJ_DIALOGUE_Q_DATA &tTaxiDQ_data, TAXI_PASSENGER_GROUP &myTaxiGroup)
    
    IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits,TAXI_OJ_JB_DISABLE_WANTED)
        
        SWITCH myTaxiData.tWantedStateIndex
        
            CASE TWS_CHECK_IF_WANTED//--------------------------------------------------------------
        
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) >= 1
                    IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > TAXI_TWEAKS_TIME_DX_DELAY_POLICE
                        
                        //Preserve the wanted_level for the stats
                        SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                        
                        //"Hey lose the cops"
                        SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_WENT_WANTED,TRUE)
                        
                        TAXI_RESET_TIMERS(myTaxiData, TT_POLICE)
                        
                        //Hide old gps & blips
						// get current blip, then hide it
                        IF DOES_BLIP_EXIST(myTaxiGroup.blipCurrentDropoff)          
                            SET_BLIP_ALPHA(myTaxiGroup.blipCurrentDropoff,0)
                            SET_BLIP_ROUTE(myTaxiGroup.blipCurrentDropoff, FALSE)
                        ENDIF
                        
						myTaxiData.bIsCurrentlyWanted = TRUE
						
                        myTaxiData.tWantedStateIndex = TWS_PRINT_OBJ_TO_LOSE_POLICE
                        CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_PRINT_OBJ_TO_LOSE_POLICE")
                    ENDIF
                    
                ENDIF
            BREAK
            
            CASE TWS_PRINT_OBJ_TO_LOSE_POLICE//--------------------------------------------------------------
                
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 4.0
                    
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_POLICE,TRUE)   
                    myTaxiData.tWantedStateIndex = TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED")
                ENDIF
            BREAK
            
            CASE TWS_CONFIRM_OBJ_LOSE_POLICE_PRINTED//--------------------------------------------------------------
                
                IF IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_POL")
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE)
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED")
				ELIF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					myTaxiData.tWantedStateIndex = TWS_CHECK_IF_PLAYER_LOST_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CHECK_IF_PLAYER_LOST_WANTED - objective not printed")
                ENDIF
            BREAK
            
            
            CASE TWS_CHECK_IF_PLAYER_LOST_WANTED//--------------------------------------------------------------
                
                //If it's grown since last time make sure we update stats and the tracker
                IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(),GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()))
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_GAINED)
                ENDIF
                
                //Toggle escape police dialogue every x seconds
                IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)// NOT myTaxiData.bTaxiOJ_CanSpeak
                    IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
                        IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > TAXI_OJ_CONST_TIME_BETWEEN_POLICE_REACTIONS
                            SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE,TRUE)
                        ENDIF
                    ENDIF
                ENDIF
                
                //Wait til he's lost it
                IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
                                
                    //Show Blip & GPS again
                    IF DOES_BLIP_EXIST(myTaxiGroup.blipCurrentDropoff)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - putting the drop off blip back on")
						SET_BLIP_ALPHA(myTaxiGroup.blipCurrentDropoff,255)
						SET_BLIP_ROUTE(myTaxiGroup.blipCurrentDropoff, TRUE)
                    ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - blip does not exist, can't bring it back!")
					ENDIF
                    
					myTaxiData.bIsCurrentlyWanted = FALSE
					
                    myTaxiData.tWantedStateIndex = TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TX_MONITOR_WANTED_LEVEL - TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED")
                ENDIF
                    
            BREAK
            
            CASE TWS_CONGRATULATE_PLAYER_ON_LOSING_WANTED
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                
                    //"Nice You Lost the Pigs "
                    SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_POLICE_LOST,TRUE,FALSE,TRUE)
        
                    //Award Bonus
                    //TAXI_SET_BONUS(myTaxiData,TAXI_BONUS_LOST_POLICE, 100 )
                        
                    //Update stats
                    TAXI_STATS_UPDATE(TAXI_STAT_WANTED_LOST,GET_TAXI_OJ_WANTED_LEVEL(myTaxiData))
                        
                    //Set tracker back to zero
                    SET_TAXI_OJ_WANTED_LEVEL(myTaxiData,0)
                    
                    myTaxiData.tWantedStateIndex = TWS_CLEANUP
                    CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_MONITOR_WANTED_LEVEL - Police lost")
                ENDIF
                
            BREAK
        
            CASE TWS_CLEANUP
                IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                    OPEN_DIALOGUE_QUEUE(tTaxiDQ_data,-1,TDQ_ADD_DELAY_BEFORE_RESUME)
                    myTaxiData.tWantedStateIndex = TWS_CHECK_IF_WANTED
                ENDIF
            BREAK
        ENDSWITCH
    ENDIF
ENDPROC

/// PURPOSE: This is the group version of IS_TAXI_RIDE_ALL_READY, spefically used by clown car
///    		Also handles reminding the player to get back in the taxi if he's left.
/// PARAMS:
///    myTaxiData - Our global Taxi data
/// RETURNS:
///    
FUNC BOOL IS_TAXI_RIDE_ALL_READY_FOR_GROUP(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup)
	
	INT iTemp
	BOOL bPassengerSafe = TRUE
	
	IF NOT IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_RIDE_ALL_READY_FOR_GROUP: NOT IS_VEHICLE_DRIVEABLE")
		TAXI_SET_FAIL(myTaxiData, "IS_TAXI_RIDE_ALL_READY - Taxi not drivable.",TFS_TAXI_DISABLED)
	ELSE
		TAXI_CLOWN_CAR_HANDLE_BLIPS(myTaxiData,myTaxiGroup.blipCurrentDropoff) 
		
		REPEAT TAXI_GROUP_SIZE iTemp
		
			IF IS_PED_INJURED(myTaxiGroup.piPassenger[iTemp])
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_RIDE_ALL_READY_FOR_GROUP: IS_PED_INJURED")
				TAXI_SET_FAIL(myTaxiData, "Passenger injured.", TFS_PASSENGER_DIED)
				bPassengerSafe = FALSE
				
			ENDIF	
		
		ENDREPEAT
		
		//Passengers are safe
		IF bPassengerSafe
			
			IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
				//CDEBUG1LN(DEBUG_OJ_TAXI,"IS_TAXI_RIDE_ALL_READY: IS_TAXI_DRIVEN_BY_PLAYER")
				RETURN TRUE
			ENDIF
	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handle hotbox anims
/// PARAMS:
///    piSmoker - the smoker 
///    eSmokeAnimStage - func handles this
///    iAnimCounter - func handles this
/// RETURNS:
///    Return TRUE if anim cycle has been completed
FUNC BOOL TASK_HOTBOX_ANIMS(PED_INDEX & piSmoker, SMOKE_ANIM_STAGE & eSmokeAnimStage)
	
	SEQUENCE_INDEX siTemp
	
	SWITCH eSmokeAnimStage
		CASE SMOKE_BASE_ANIM
			CLEAR_SEQUENCE_TASK(siTemp)
			OPEN_SEQUENCE_TASK(siTemp)
				
				TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@hotbox", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@hotbox", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@hotbox", "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@hotbox", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				TASK_PLAY_ANIM(NULL, "oddjobs@taxi@gyn@cc@hotbox", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				
			CLOSE_SEQUENCE_TASK(siTemp)
			TASK_PERFORM_SEQUENCE(piSmoker, siTemp)
			
			eSmokeAnimStage = SMOKE_PUFF_ANIM
		BREAK
		
		CASE SMOKE_PUFF_ANIM
			IF ( GET_SCRIPT_TASK_STATUS(piSmoker, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(piSmoker, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK	)
				
				eSmokeAnimStage = SMOKE_BASE_ANIM
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SWITCH_CURRENT_SMOKER(TAXI_PASSENGER_NAMES & eSmoker)
	
	INT iCurrentSmoker
	
	iCurrentSmoker = ENUM_TO_INT(eSmoker)
	iCurrentSmoker++
	
	IF iCurrentSmoker = ENUM_TO_INT(TAXI_GROUP_SIZE)
		iCurrentSmoker = 0
	ENDIF
//	
//	WHILE myTaxiGroup.bDroppedOff[iCurrentSmoker]
//		iCurrentSmoker++
//	
//		IF iCurrentSmoker = ENUM_TO_INT(TAXI_GROUP_SIZE)
//			iCurrentSmoker = 0
//		ENDIF
//	ENDWHILE
	
	eSmoker = INT_TO_ENUM(TAXI_PASSENGER_NAMES, iCurrentSmoker)
	
ENDPROC

FUNC BOOL HAS_TAXI_GROUP_BEEN_DROPPED_OFF_TIMELY(TAXI_PASSENGER_GROUP &myTaxiGroup)
	RETURN myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P0_REJINO] AND myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P1_FRANKIE] AND myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P2_PAULIE]
ENDFUNC

FUNC BOOL HAS_TAXI_GROUP_REACHED_HIT_ALL_DROPOFFS(TaxiStruct &myTaxiData, TAXI_PASSENGER_GROUP &myTaxiGroup,TAXI_OJ_DIALOGUE_Q_DATA &tTaxiOJ_DQ_Data, TAXI_OJ_DQ_CONVERSATION_LINE & tDialogueLine[])
	
	SEQUENCE_INDEX tempSeqIndex
	
	TAXI_OJ_GROUP_MONITOR_WANTED_LEVEL(myTaxiData, tTaxiOJ_DQ_Data, myTaxiGroup)
	
	IF GET_TAXI_SPEECH_INDEX(myTaxiData) >= TAXI_DI_VARIABLE_BANTER
		IF bHotBoxEarly
			
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > 20
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
			AND NOT bHotBoxReady
			
				bHotBoxReady = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// first leg of the mission: smoke should emit, anims handled
	IF myTaxiGroup.iWhichDropoff < 2
		IF myTaxiGroup.bSmokeInstantFadeIn
		AND NOT bHotBoxEarly
			TAXI_HANDLE_HOTBOX(myTaxiData, myTaxiGroup, FALSE, myTaxiGroup.bSmokeInstantFadeIn)
			myTaxiGroup.bSmokeInstantFadeIn = FALSE
		ELSE
			TAXI_HANDLE_HOTBOX(myTaxiData, myTaxiGroup)
		ENDIF
		
		IF myTaxiGroup.bSmokeAnimStart
		AND myTaxiGroup.iWhichDropoff < 1
			IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[myTaxiGroup.eCurrentSmoker])
			AND IS_PED_IN_ANY_VEHICLE(myTaxiGroup.piPassenger[myTaxiGroup.eCurrentSmoker])
				// task smoking anims until they get close to the first dropoff, at that point clear them out
				IF NOT bTaxiPassengerSpoke
					IF TASK_HOTBOX_ANIMS(myTaxiGroup.piPassenger[myTaxiGroup.eCurrentSmoker], myTaxiGroup.eSmokeAnimStage)
						SWITCH_CURRENT_SMOKER(myTaxiGroup.eCurrentSmoker)
					ENDIF
				ELIF NOT myTaxiGroup.bClearHotboxAnims
					
					CLEAR_PED_TASKS(myTaxiGroup.piPassenger[myTaxiGroup.eCurrentSmoker])
					TASK_STAND_STILL(myTaxiGroup.piPassenger[myTaxiGroup.eCurrentSmoker], 500)
					myTaxiGroup.bClearHotboxAnims = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ELIF NOT myTaxiGroup.bIsSmokeUnplumed
		TAXI_HANDLE_HOTBOX(myTaxiData, myTaxiGroup, TRUE)
	ENDIF
	
	//LM change 12/8/11
	// persistent check that all passengers aren't dead
	IF IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
	OR IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
	OR IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiData.viTaxi)
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], myTaxiData.viTaxi)
		myTaxiData.tTaxiOJ_FailType = TFS_PASSENGER_SHOT
		TAXI_SET_FAIL(myTaxiData, "Passenger injured.", TFS_PASSENGER_SHOT)
	ENDIF
	
	IF NOT IS_TAXI_RIDE_ALL_READY_FOR_GROUP(myTaxiData, myTaxiGroup)
		// if driver door is blocked, have rejino make way
		IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi, VS_DRIVER)
			
			TASK_LEAVE_ANY_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
			
			bDidPedMakeWay = TRUE
			bTaxiPedTasked = FALSE
		ENDIF
	
	ELSE
		IF bDidPedMakeWay
			// put rejino back in the cab when the seat is free
			IF IS_VEHICLE_SEAT_FREE(myTaxiData.viTaxi, VS_FRONT_RIGHT)
				
				IF NOT bTaxiPedTasked
					TASK_ENTER_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.viTaxi, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
					bTaxiPedTasked = TRUE
				ENDIF
				
				// if player tries to drive away without letting the passenger get back in
				IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]) > 40
					TAXI_SET_FAIL(myTaxiData, "Passenger left behind.", TFS_ABANDONED_PASSENGER)
					//LM TODO:  we need a proper fail case and dialogue for leaving the passenger behind
				ENDIF
			ENDIF
			
			IF GET_PED_IN_VEHICLE_SEAT(myTaxiData.viTaxi, VS_FRONT_RIGHT) = myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO]
				// make sure the dest blip is routed once you get back in the car
				TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
				bDidPedMakeWay = FALSE
			ENDIF
		ENDIF
		
		TAXI_OJ_MONITOR_STOPPED(myTaxiData)
		
		UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
		
		IF NOT bDidPedMakeWay
		AND NOT IS_PED_IN_VEHICLE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], myTaxiData.viTaxi)
			TAXI_SET_FAIL(myTaxiData, "Passenger left car.")
		ENDIF

		SWITCH myTaxiGroup.iWhichDropoff
			//Check for the taxi to make it to the 1st dropoff point - PAULIE'S POINT
			CASE 0
				IF NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P2_PAULIE]
					
					IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 150
					AND NOT bTaxiPassengerSpoke
					
						//important QUEUE LINE #
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_CC_CLOSE, TRUE)
						
						bTaxiPassengerSpoke = TRUE
					ENDIF
					
//					#IF IS_DEBUG_BUILD
//						DRAW_DEBUG_SPHERE(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE],TAXI_OJ_CONST_GATEWAY_SIZE_SMALL,0,0,255,125)
//					#ENDIF
					
					//IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE]) <= TAXI_OJ_CONST_GATEWAY_SIZE_SMALL
					IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE], << 5.0, 5.0, LOCATE_SIZE_HEIGHT >>, !(myTaxiData.bIsCurrentlyWanted))
						IF IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, 4)
						 
							IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
								//De-blip
								REMOVE_BLIP(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE])
								
								//CLEAR_DIALOGUE_QUEUE(tDialogueLine)
								//CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,1)
								KILL_FACE_TO_FACE_CONVERSATION()
								//Tell ped to leave the car
								
								IF myTaxiGroup.eCurrentSmoker = TAXI_GROUP_P2_PAULIE
									CLEAR_PED_TASKS(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
								ENDIF
								
								CLEAR_SEQUENCE_TASK(tempSeqIndex)
								OPEN_SEQUENCE_TASK(tempSeqIndex)
									
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -98.0071, -851.2611, 40.9833 >>, PEDMOVEBLENDRATIO_WALK, 30000)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -83.1601, -835.3889, 39.5744 >>, PEDMOVEBLENDRATIO_WALK)
									TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE")
									
								CLOSE_SEQUENCE_TASK(tempSeqIndex)
								TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], tempSeqIndex)
								CLEAR_SEQUENCE_TASK(tempSeqIndex)
								
								//Dropped off in good time
								IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) < fGoodPaulieDropOffTime
									myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P2_PAULIE] = TRUE
									
									SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF,TRUE)
									myTaxiData.iTaxiOJ_CashTip += 3
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"Bonus Rewarded for Paulie Dropoff: Target time = ",fGoodPaulieDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								ELSE
									myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P2_PAULIE] = FALSE
									SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF_ASS,TRUE)
									myTaxiData.iTaxiOJ_CashTip -= 3
									CDEBUG1LN(DEBUG_OJ_TAXI,"You were too slow: Target time = ",fGoodPaulieDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								ENDIF
								
								myTaxiGroup.iWhichDropoff++
								CDEBUG1LN(DEBUG_OJ_TAXI,"HTGRHAD - iWhichDropoff = 1")
								
								TAXI_PAUSE_TIMER(myTaxiData, TT_DROPOFF)
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi not Fully Stopped")
						ENDIF
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE] = TRUE")
					
					ENDIF
				ENDIF
			BREAK
			
			//Wait til PAULIE leaves vehicle
			CASE 1
			
				IF NOT IS_PED_IN_VEHICLE_SAFE(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE], myTaxiData.viTaxi)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 1.0
				AND NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P2_PAULIE]
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Paulie is out of the car, turning control back on")
					
					DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX(myTaxiGroup)
					
					myTaxiGroup.bDroppedOff[TAXI_GROUP_P2_PAULIE] = TRUE
					
					myTaxiData.vTaxiOJ_WarpPtDropoff = << 193.1020, -220.0569, 52.8647 >>
					myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 250.7
					
					//TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
					
					
				ELIF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
				AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
					
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"HTGRHAD - iWhichDropoff = 2")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DROPOFF)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF2, TRUE)
					
					IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE])
//						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC2")
							TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
//						ENDIF
					ENDIF
					
					bTaxiPassengerSpoke = FALSE
					
					myTaxiGroup.iWhichDropoff++
					
				ENDIF
				
			BREAK
			
			//Wait to reach the next destination - FRANKIE's Point
			CASE 2
				IF NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P1_FRANKIE]
					
//					IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE])
//						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC2")
//							TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
//						ENDIF
//					ENDIF
					
					IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE])) < 175
					AND NOT bTaxiPassengerSpoke
						
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_CC_CLOSE, TRUE)
						
						bTaxiPassengerSpoke = TRUE
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE],TAXI_OJ_CONST_GATEWAY_SIZE_SMALL,0,0,255,125)
					#ENDIF
					
//					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE]) <= TAXI_OJ_CONST_GATEWAY_SIZE_SMALL
					IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE], << 5.0, 5.0, LOCATE_SIZE_HEIGHT >>, !(myTaxiData.bIsCurrentlyWanted))
						IF IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, 4)
							IF NOT IS_PED_INJURED(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
								
								KILL_FACE_TO_FACE_CONVERSATION()															
								//De-blip
								REMOVE_BLIP(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P1_FRANKIE])
								
								//Tell ped to leave the car
								TAXI_OJ_EXIT_CLOSEST_TO_STREET_GENERAL(myTaxiData.viTaxi, myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P1_FRANKIE])
								
								IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) < fGoodFrankieDropOffTime
									myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P1_FRANKIE] = TRUE
									
									SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF2_GOOD,TRUE)
									myTaxiData.iTaxiOJ_CashTip += 2
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"Bonus Rewarded for Frankie Dropoff: Target time = ",fGoodFrankieDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								ELSE
									myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P1_FRANKIE] = FALSE
									
									SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF2_BAD,TRUE)
									myTaxiData.iTaxiOJ_CashTip -= 2
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"You were too slow: Target time = ",fGoodFrankieDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								ENDIF
								
								myTaxiGroup.iWhichDropoff++
								CDEBUG1LN(DEBUG_OJ_TAXI,"HTGRHAD - iWhichDropoff = 3")
								
								TAXI_PAUSE_TIMER(myTaxiData, TT_DROPOFF)
							ENDIF
							
						ENDIF
					ENDIF			
				ENDIF
			BREAK
			
			//Wait til Frankie leaves vehicle
			CASE 3
			
				IF NOT IS_PED_IN_VEHICLE_SAFE(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE], myTaxiData.viTaxi)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DELAY) > 1.0	
				AND NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P1_FRANKIE]
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"FRANKIE is out of the car, turning control back on")
					
					myTaxiGroup.bDroppedOff[TAXI_GROUP_P1_FRANKIE] = TRUE
					
					myTaxiData.vTaxiOJ_WarpPtDropoff = << 51.4688, -1302.0417, 28.1383 >>
					myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 216.5
						
					//TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
					
				ELIF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
				AND IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
					
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"HTGRHAD - iWhichDropoff =4")
					
					TAXI_RESET_TIMERS(myTaxiData, TT_DROPOFF)
					TAXI_RESET_TIMERS(myTaxiData, TT_BANTER)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF3, TRUE)
					
					bTaxiPassengerSpoke = FALSE
					
					IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO])
//						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC3")
							TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
//						ENDIF
					ENDIF
					
					myTaxiGroup.iWhichDropoff++
					
				ENDIF
		
			BREAK
			
			//Wait to reach the next destination - TAXI_GROUP_P0_REJINO's Point
			CASE 4
				IF NOT myTaxiGroup.bDroppedOff[TAXI_GROUP_P0_REJINO]
					
//					IF NOT DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO])
//						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC3")
//							TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
//						ENDIF
//					ENDIF
					
					IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO])) < 100
					AND NOT bTaxiPassengerSpoke
						
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_CC_CLOSE, TRUE)
						
						bTaxiPassengerSpoke = TRUE
					ENDIF
					
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO],TAXI_OJ_CONST_GATEWAY_SIZE_SMALL,0,0,255,125)
					#ENDIF
					
					//IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO]) <= TAXI_OJ_CONST_GATEWAY_SIZE_SMALL
					IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO], << 5.0, 5.0, LOCATE_SIZE_HEIGHT >>, !(myTaxiData.bIsCurrentlyWanted))
						IF IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE, 4)
							
							//De-blip
							REMOVE_BLIP(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P0_REJINO])
							myTaxiGroup.bDroppedOff[TAXI_GROUP_P0_REJINO] = TRUE
							
							KILL_FACE_TO_FACE_CONVERSATION()
							//Tell ped to leave the car
	//							CLEAR_SEQUENCE_TASK(tempSeqIndex)
	//							OPEN_SEQUENCE_TASK(tempSeqIndex)
	//								TASK_LEAVE_ANY_VEHICLE(NULL)
	//								TASK_WANDER_STANDARD(NULL)
	//								//TODO - SET A POI
	//							CLOSE_SEQUENCE_TASK(tempSeqIndex)
	//							TASK_PERFORM_SEQUENCE(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO], tempSeqIndex)
	//							CLEAR_SEQUENCE_TASK(tempSeqIndex)
							
							//Play line dropoff line
							//SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_GROUP_DROPOFF,TRUE)
						
							//myTaxiGroup.iWhichDropoff++
							CDEBUG1LN(DEBUG_OJ_TAXI,"HTGRHAD - iWhichDropoff = END")
							
							IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) < fGoodRejinoDropOffTime
								myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P0_REJINO] = TRUE
								CDEBUG1LN(DEBUG_OJ_TAXI,"Bonus Rewarded for Rejino Dropoff: Target time = ",fGoodRejinoDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								
								myTaxiData.iTaxiOJ_CashTip += 2
							ELSE
								myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P0_REJINO] = FALSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"You were too slow: Target time = ",fGoodRejinoDropOffTime, " actual time = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) )
								
								myTaxiData.iTaxiOJ_CashTip -= 2
							ENDIF
							
							RETURN TRUE
						ENDIF
							
					ENDIF			
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL TAXI_OJ_HANDLE_GROUP_AGGRO(TaxiStruct& myTaxiData,TAXI_PASSENGER_GROUP &myTaxiGroup,AGGRO_ARGS &aggroArgs)
    IF NOT myTaxiData.bTaxiOJ_Failed
        EAggro aggroReason
        INT k 
        IF DO_AGGRO_CHECK(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],myTaxiData.viTaxi,aggroArgs, aggroReason)
		OR DO_AGGRO_CHECK(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],myTaxiData.viTaxi,aggroArgs, aggroReason)
		OR DO_AGGRO_CHECK(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],myTaxiData.viTaxi,aggroArgs, aggroReason)
            TAXI_OJ_HANDLE_AGGRO(myTaxiData,aggroReason,aggroArgs)
            //TAXI_SET_FAIL(myTaxiData,"Player aggro'd the taxi.",TFS_ABANDONED_PASSENGER)
        ENDIF
		
		REPEAT ENUM_TO_INT(TAXI_AMB_PED_NUM) k
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
				IF DO_AGGRO_CHECK(myTaxiGroup.piAmbPed[INT_TO_ENUM(TAXI_AMB_PED_NAMES, k)],myTaxiData.viTaxi,aggroArgs, aggroReason)
					 //TAXI_OJ_HANDLE_AGGRO(myTaxiData,aggroReason,aggroArgs)
					TAXI_SET_FAIL(myTaxiData,"Taxi attacked the entourage")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT	 
                
        CHECK_TAXI_SWAP_CAR(myTaxiData)
    ENDIF
//    
//	
//	// player control failsafe
//	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//	AND (DOES_CAM_EXIST(myTaxiData.camTaxi) AND NOT IS_CAM_ACTIVE(myTaxiData.camTaxi))
//	AND (DOES_CAM_EXIST(myTaxiData.camInterp) AND NOT IS_CAM_ACTIVE(myTaxiData.camInterp))
//	AND NOT IS_MESSAGE_BEING_DISPLAYED()
//	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//	
//		IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CONTROL_CHECK) > 10
//			TAXI_CANCEL_TIMERS(myTaxiData, TT_CONTROL_CHECK)
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			CDEBUG1LN(DEBUG_OJ_TAXI,"TT_CONTROL_CHECK > 10, PLAYER CONTROL RESTORED!")
//		ENDIF
//	ELSE
//		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_CONTROL_CHECK)
//			TAXI_CANCEL_TIMERS(myTaxiData, TT_CONTROL_CHECK)
//		ENDIF
//	ENDIF
	
    RETURN FALSE
ENDFUNC


//EOF
