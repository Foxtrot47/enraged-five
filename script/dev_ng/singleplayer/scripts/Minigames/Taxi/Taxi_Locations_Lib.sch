///		Taxi_Locations_Lib.sch
///		Author: Lino A. Manansala
///		Starting a new header to keep track of the locations for the taxi procedural system

USING "Taxi_Locations.sch"

PROC TAXI_SET_PROC_DISTANCE(TAXI_PROCEDURAL_DATA & TaxiProcData)
	
	TAXI_PROCEDURAL_DISTANCE eTemp
	eTemp = INT_TO_ENUM(TAXI_PROCEDURAL_DISTANCE, ENUM_TO_INT(g_LastProceduralDistance) + 1)
	
	IF (eTemp = TAXIDIST_NUM)
		eTemp = INT_TO_ENUM(TAXI_PROCEDURAL_DISTANCE, 0)
	ENDIF
	
	TaxiProcData.CurrentDistance = eTemp
	CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_SET_PROC_DISTANCE] TaxiProcData.CurrentDistance set to " , GET_TAXI_DISTANCE_STRING(TaxiProcData.CurrentDistance))
	
	g_LastProceduralDistance = eTemp
ENDPROC

PROC TAXI_SET_PROC_DESTINATION(TAXI_PROCEDURAL_DATA & TaxiProcData)
	//VECTOR vPlayerPos
	VECTOR vDestPos
	INT iTemp, iDestCounter
	FLOAT fUpperLimit, fCurDist
	
	TAXI_SET_PROC_DISTANCE(TaxiProcData)
	
	// set upper limit
	SWITCH TaxiProcData.CurrentDistance
		CASE TAXIDIST_SHORT
			fUpperLimit = TAXI_MEDIUM_DISTANCE
		BREAK
		CASE TAXIDIST_MEDIUM
			fUpperLimit = TAXI_LONG_DISTANCE
		BREAK
		CASE TAXIDIST_LONG
			fUpperLimit = TAXI_MAX_DISTANCE
		BREAK
	ENDSWITCH
	
//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	ENDIF
	
	REPEAT ENUM_TO_INT(TAXILOC_NUM) iTemp
		IF iDestCounter < TAXI_POSSIBLE_DROPOFFS
			vDestPos = GET_TAXI_LOCATION_COORD(INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp))
			fCurDist = GET_GPS_DISTANCE(vDestPos)
			CDEBUG1LN(DEBUG_OJ_TAXI, "[TAXI_SET_PROC_DESTINATION] ", GET_TAXI_LOCATION_STRING(INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp)), " is this far: ", fCurDist)
			
			IF fCurDist < fUpperLimit 
			AND fCurDist >= GET_TAXI_PROCEDURAL_DISTANCE(TaxiProcData.CurrentDistance)
			AND (INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp) != g_LastFewDestinations[0])
			AND (INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp) != g_LastFewDestinations[1])
			AND (INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp) != g_LastFewDestinations[2])
			AND (INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp) != g_LastFewDestinations[3])
				TaxiProcData.ePossibleDropoffs[iDestCounter] = INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp)
				iDestCounter++
				
				CDEBUG1LN(DEBUG_OJ_TAXI, "[TAXI_SET_PROC_DESTINATION] ", GET_TAXI_LOCATION_STRING(INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, iTemp)), " added to possibles")
				CDEBUG1LN(DEBUG_OJ_TAXI, "[TAXI_SET_PROC_DESTINATION] iDestCounter = ", iDestCounter)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iDestCounter > 0
		iTemp = GET_RANDOM_INT_IN_RANGE() % iDestCounter
		TaxiProcData.CurrentLocation = TaxiProcData.ePossibleDropoffs[iTemp]
		TaxiProcData.fGPSDistance = GET_GPS_DISTANCE(GET_TAXI_LOCATION_COORD(TaxiProcData.CurrentLocation))
		CDEBUG1LN(DEBUG_OJ_TAXI, "[TAXI_SET_PROC_DESTINATION] random iTemp = ", iTemp, " TaxiProcData.CurrentLocation = ", GET_TAXI_LOCATION_STRING(TaxiProcData.CurrentLocation))
	ELSE
		CDEBUG1LN(DEBUG_OJ_TAXI, "[TAXI_SET_PROC_DESTINATION] iDestCounter = 0, picking a random location")
		TaxiProcData.CurrentLocation = INT_TO_ENUM(TAXI_PROCEDURAL_LOCATIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(TAXILOC_NUM)))
		TaxiProcData.fGPSDistance = GET_GPS_DISTANCE(GET_TAXI_LOCATION_COORD(TaxiProcData.CurrentLocation))
		CDEBUG1LN(DEBUG_OJ_TAXI,"[TAXI_SET_PROC_DESTINATION] TaxiProcData.CurrentLocation = ", GET_TAXI_LOCATION_STRING(TaxiProcData.CurrentLocation))
	ENDIF
	
	g_LastFewDestinations[3] = g_LastFewDestinations[2]
	g_LastFewDestinations[2] = g_LastFewDestinations[1]
	g_LastFewDestinations[1] = g_LastFewDestinations[0]
	g_LastFewDestinations[0] = TaxiProcData.CurrentLocation 
	
ENDPROC

//EOF
