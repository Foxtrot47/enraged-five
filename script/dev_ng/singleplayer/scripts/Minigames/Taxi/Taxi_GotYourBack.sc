//=======================================================================================================================================
// Taxi_GotYouNow.sc
// Dev : John R. Diaz
/*
	•	“We’ve got you this time, pal!” This is the only scenario that launches without immediately notifying the player. If the player 
	has angered a specific gang or has committed enough crimes to anger the police, this scenario will launch and mimic a normal passenger. 
	However, the drop off for this passenger will be someplace unusual. (Such as behind a warehouse or at an abandoned peer.) When you arrive, 
	the passenger runs from the car and takes cover. Baddies and cars pop out of hiding and ambush you. “You shouldn’t fuck with ___!” they 
	shout before opening fire.  The player can flee, but if they opt to fight the ambushers should have ammo and contraband such that the 
	player feels that they did not waste their time by picking up cab fares.
*/



//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//12/20/11 - End of mission cleaned up to allow for better sequence of events.  General script cleanup.  --johnsripan
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//Includes------------------------------------------------------
USING "Taxi_Includes.sch"
USING "Taxi_ambush_lib.sch"

//Local Variables-----------------------------------------------
TaxiStruct 						myTaxiData

TaxiAmbushStruct_v2 			TAS_Local
TaxiAmbushStruct_v2				TAS_Local2

REL_GROUP_HASH 					relPursuers

ENUM TAXIOJ_GYB_BONUS
	TGYB_BONUS_TERMINATOR = 0,
	TGYB_BONUS_TOTAL
				
ENDENUM

ENUM TAXI_DYNAMIC_REQUESTS
	TAXIDR_REQUEST,
	TAXIDR_WAIT_STREAM,
	TAXIDR_CREATE,
	TAXIDR_CLEANUP
ENDENUM

TAXI_DYNAMIC_REQUESTS eTaxiRequestState = TAXIDR_REQUEST

CONST_INT						TAXI_CONST_BONUS_CASH_TERMINATOR			200
CONST_INT						TAXI_CONST_NUM_TOTAL_ENEMIES				8
CONST_INT						k_bDropOffFound			0
CONST_INT						k_bPassengerTalking		1
CONST_INT						k_bPassengerVulnerable	2
CONST_INT						k_bSideTaskLoopB		3
CONST_INT						k_bSideTaskOutro		4
CONST_INT						k_bMainTaskOutro		5
CONST_INT						k_bGivePassengerGun		6
CONST_INT						k_bGiveBikerCGun		7
CONST_INT						k_bGiveBikerDGun		8
CONST_INT						k_bPauseConvo			9
CONST_INT						k_bSideConvoPlayed		10
CONST_INT						TAXI_CONST_GYB_DIST_TO_HAND_GUN				675 //1000 //550

CONST_FLOAT						TAXI_PASSENGER_GUN_TIME	0.838
CONST_FLOAT						TAXI_BIKER_C_GUN_TIME	0.853
CONST_FLOAT						TAXI_BIKER_D_GUN_TIME	0.218

BONUS_FIELD						bonusFieldGotYourBack[TGYB_BONUS_TOTAL]

MODEL_NAMES 					mPassengerModel	= A_M_M_FARMER_01
MODEL_NAMES 					mBoxPile 		= PROP_BOX_WOOD04A
MODEL_NAMES 					mRegularBox 	= PROP_CARDBORDBOX_03A
MODEL_NAMES 					mExpBarrel 		= PROP_BARREL_EXP_01A

//Ambush states for ambush state machine
ENUM AMBUSH_STATES
	AMB_INIT = 0,
	AMB_TALKING,
	AMB_TALKING2,
	AMB_STANDOFF,
	AMB_COMBAT,
	AMB_SECOND_WAVE,
	AMB_WAIT_FOR_DEATH,
	AMB_END
ENDENUM
AMBUSH_STATES					m_AmbushState = AMB_INIT

//Native Data
SEQUENCE_INDEX 					seqIndexTaxiAIAction

OBJECT_INDEX					oiBoxPiles[3]
OBJECT_INDEX					oiExpBarrel
OBJECT_INDEX					oiRegBox

PED_INDEX						piGangsterGirls[2]
PED_INDEX						piMedic

VEHICLE_INDEX					viExtraBike //one more bike in docks scene that doesn't fit in ambush struct
VEHICLE_INDEX					viAmbulance

SCENARIO_BLOCKING_INDEX			tempScenarioBlockingIndex

INCIDENT_INDEX					ambulanceIndex
INCIDENT_INDEX					policeIndex

//Ints
INT 							iDebugThrottle = 1
INT								iAmbushTimer = 0
INT								iGiveGunTimer = 0
INT								iSyncFailSafeTimer = 0
INT								iTaxiGYBCarRecFileNo = 112
INT								iStandoffMarker = 0
INT								iGangsterKilledCount = 0
INT								iPlayerKilledCount = 0
INT								iTipIndex = 0

INT								iLocalBitSet = 0
INT 							iSideSceneId
INT 							iSideSceneId2
INT 							iSideSceneId3
INT 							iSideSceneId4
INT 							iMainSceneId
INT 							iMainSceneId2

//Bools
BOOL 							bWeaponGivenToPlayer = FALSE
//BOOL 							bDisableCin = FALSE
BOOL 							bEnableBonusCam// = TRUE
//BOOL 							bCineCamSwitch = TRUE
BOOL 							bTaxiDelayFail
BOOL 							bTaxiFailOnce
BOOL 							bTaxiEarlyAggro
BOOL							bAmbushSetup
BOOL							bAmbushBlipTransition
BOOL							bDispatchSuccessful
BOOL							bAmbulanceFound
BOOL							bSayShout
BOOL							bCutToGamePlay
BOOL							bPulseTriggered

//Vectors
VECTOR 							vPassengerPt = << 11.8607, -1123.4800, 27.6801 >> //Ammunation  //<< -271.9266, 248.3547, 89.1946 >>	//Viper Room
VECTOR 							vPassengerPickupPt = << 11.8607, -1123.4800, 27.6801 >> //<< -275.2373, 252.9418, 88.8746 >>

VECTOR							vDocksMin = << 144.3291, -3352.7734, 3.6651 >>
VECTOR							vDocksMax = << 311.9527, -3305.6133, 10.6651 >>

VECTOR 							scenePositionSide = << 206.636, -3322.673, 4.6339 >> //4.715 >>
VECTOR 							sceneRotationSide = << 0.000, 0.000, 24.000 >>
VECTOR 							scenePositionMain = << 208.786, -3319.823, 4.6339 >> //4.715 >>
VECTOR 							sceneRotationMain = << 0.000, 0.000, 47.000 >>

VECTOR							vPassengerWalkPoint = << 199.51877, -3326.20117, 4.78716 >>

VECTOR							vIncidentPoint = << 232.99445, -3327.82495, 4.79877 >>

//Const
CONST_INT						kPedLowAccuracy  1
CONST_INT						kPedNotSoLowAccuracy  1

CONST_FLOAT 					fGoodFleeDistance 4000.0

STRING							sSyncSceneDict = "oddjobs@taxi@argument"

TEXT_LABEL_23 					sStandoffTrigger1 = "txm12_deal1_7"
TEXT_LABEL_23 					sStandoffTrigger2 = "txm12_deal1_7"
TEXT_LABEL_23 					sCurrentLine

structTimer						ambushBanterTimer
structTimer						pulseTimer

//Dialogue Queue Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

//Aggro checks
AGGRO_ARGS 						aggroArgs 

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			taxiRideWidgets
	TEXT_LABEL_63				sDebugString[3]
	BOOL 						bForceReset, bResetting, bPassengerInvincible
	BOOL 						bDebugTurnOnFreeRide = FALSE
	BOOL						bDebugBonusTermintator = FALSE	
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF

//FUNCTIONS------------------------------------------------------------------------------------------------------------

/// PURPOSE:	Used as an update routine that also gates progress in a conditional statement, also manages blips
///    
/// PARAMS:		
///    Fighters - Array of peds to be monitored
///    Blips - Array of blips in direct relation to peds
/// RETURNS:	returns true if all peds are dead
///    
FUNC BOOL UPDATE_AMBUSH_PEDS(PED_INDEX &Fighters[], BLIP_INDEX &Blips[], PED_INDEX &Fighters2[], BLIP_INDEX &Blips2[])
	INT i
	
	BOOL bAreAllEnemiesDead = TRUE
	iPlayerKilledCount = 0
	
	REPEAT 4 i //COUNT_OF(Fighters) i
		IF IS_ENTITY_DEAD( Fighters[i] )
			IF DOES_BLIP_EXIST( Blips[i] )
				REMOVE_BLIP( Blips[i] )
			ENDIF
			
			IF HAS_TAXI_DAMAGED_ENTITY(myTaxiData,Fighters[i])
				iPlayerKilledCount++
			ENDIF
			
			iGangsterKilledCount++
		ELSE
			iPlayerKilledCount = 0
			bAreAllEnemiesDead = FALSE
		ENDIF
		
		IF IS_ENTITY_DEAD( Fighters2[i] )
			IF DOES_BLIP_EXIST( Blips2[i] )
				REMOVE_BLIP( Blips2[i] )
				bSayShout = TRUE
			ENDIF
			
			IF HAS_TAXI_DAMAGED_ENTITY(myTaxiData,Fighters2[i])
				iPlayerKilledCount++
			ENDIF
			
//		ELIF NOT DOES_BLIP_EXIST( Blips2[i] )
//			// if blip doesn't exist and entity is not dead, that means ped isn't in combat
//			// so do nothing
		ELSE
			iPlayerKilledCount = 0
			bAreAllEnemiesDead = FALSE
		ENDIF
			
	ENDREPEAT
	
	RETURN bAreAllEnemiesDead
ENDFUNC
/// PURPOSE:A central location for all the vehicle recording names used in this mission.
///    
/// PARAMS:
///    iNum - Corresponds to which car recording we want
/// RETURNS:
///    
FUNC STRING GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(INT iNum)
	STRING sVehRecName
	SWITCH iNum
		CASE 0
			sVehRecName = "txm12_s01_a"
		BREAK
		
		CASE 1
			sVehRecName = "txm12_s01_b"
		BREAK
	ENDSWITCH
	
	RETURN sVehRecName
ENDFUNC

/// PURPOSE: Make all of our mission specific requests here
///    
PROC REQUEST_TAXI_ODDJOB_GYB_STREAMS_STAGE_01()
	//Load text and UI
	
	REQUEST_MODEL(mPassengerModel)
	
	//Load text and UI
	TAXI_INIT_SHARED_STREAMS()
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM - GYB Stage 01 Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_GYB_STREAMS_STAGE_01()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mPassengerModel)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi OJ GYB Stage01 Assets Released")
	
ENDPROC

PROC REQUEST_TAXI_ODDJOB_GYB_STREAMS_STAGE_AMBUSH()
	
	REQUEST_MODEL(mBoxPile)
	REQUEST_MODEL(mRegularBox)
	REQUEST_MODEL(mExpBarrel)
	
	REQUEST_MODEL(TAS_Local.gangmodel[0])
	REQUEST_MODEL(TAS_Local.gangmodel[1])
	REQUEST_MODEL(TAS_Local.modelName_GangCar[0])
	REQUEST_MODEL(TAS_Local.modelName_GangCar[1])
	REQUEST_MODEL(TAS_Local.modelName_GangCar[2])
	REQUEST_MODEL(TAS_Local.modelName_GangCar[3])
	REQUEST_MODEL(TAS_Local2.modelName_GangCar[1])
		
	REQUEST_VEHICLE_RECORDING(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(0))
	REQUEST_VEHICLE_RECORDING(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(1))
	
	REQUEST_ANIM_DICT("random@countryside_gang_fight")
	REQUEST_ANIM_DICT(sSyncSceneDict)

	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM - GYB Stage Ambush Assets Requested")

ENDPROC 

PROC RELEASE_TAXI_ODDJOB_GYB_STREAMS_STAGE_AMBUSH()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(mBoxPile)
	SET_MODEL_AS_NO_LONGER_NEEDED(mRegularBox)
	SET_MODEL_AS_NO_LONGER_NEEDED(mExpBarrel)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.gangmodel[0])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.gangmodel[1])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.modelName_GangCar[0])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.modelName_GangCar[1])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.modelName_GangCar[2])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local.modelName_GangCar[3])
	SET_MODEL_AS_NO_LONGER_NEEDED(TAS_Local2.modelName_GangCar[1])
		
	REMOVE_VEHICLE_RECORDING(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(0))
	REMOVE_VEHICLE_RECORDING(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(1))
	
	REMOVE_ANIM_DICT("random@countryside_gang_fight")
	REMOVE_ANIM_DICT(sSyncSceneDict)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM - GYB Stage Ambush Assets Released")
ENDPROC


FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_ABMUSH_ASSETS_LOADED()

	IF NOT HAS_MODEL_LOADED(mBoxPile)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading PROP_BOXPILE_02C",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(mRegularBox)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading PROP_CARDBORDBOX_03A",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.gangmodel[0])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading Baddie driver model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.gangmodel[1])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading Gangster girl model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.modelName_GangCar[0])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading modelName_Car1 model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.modelName_GangCar[1])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading modelName_Car2 model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.modelName_GangCar[2])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading modelName_Car3 model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(TAS_Local.modelName_GangCar[3])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading modelName_Car4 model",iDebugThrottle)
		RETURN FALSE
	ENDIF 
	
	IF NOT HAS_MODEL_LOADED(TAS_Local2.modelName_GangCar[1])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading modelName_Car5 model",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(0))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMING - Vehicle Recording Loading TXM12_S01_A...",iDebugThrottle)
		RETURN FALSE
	ENDIF


	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(1))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMING - Vehicle Recording Loading TXM12_S01_B...",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("random@countryside_gang_fight")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading random@countryside_gang_fight ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sSyncSceneDict)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading sSyncSceneDict ",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	//JD - 5/9/12 - For some reason mExpBarrel is loading commenting out for now since it's not a critical prop
	IF NOT HAS_MODEL_LOADED(mExpBarrel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading PROP_BARREL_EXP_01A",iDebugThrottle)
		//RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//// PURPOSE: Waits for our assets to load
 ///    
 /// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
 ///    
 ///     
FUNC BOOL HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()

	IF NOT HAS_MODEL_LOADED(mPassengerModel)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_M_Farmer_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF

	RETURN TRUE		
ENDFUNC


PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
	#ENDIF
	
	CLEANUP_TAXI_OJ_PASSENGER(myTaxiData)
	
	SET_WANTED_LEVEL_MULTIPLIER(1)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vDocksMin, vDocksMax, TRUE)
	CLEAR_PED_NON_CREATION_AREA()
	REMOVE_SCENARIO_BLOCKING_AREA(tempScenarioBlockingIndex)
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(viExtraBike)
		
	RELEASE_TAXI_ODDJOB_GYB_STREAMS_STAGE_01()
	RELEASE_TAXI_ODDJOB_GYB_STREAMS_STAGE_AMBUSH()
	
	IF DOES_CAM_EXIST(myTaxiData.camTaxi)
		DESTROY_CAM(myTaxiData.camTaxi)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	ENDIF
	
	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	IF NOT bTaxiFailOnce
		TAXI_SCRIPT_FAILED(myTaxiData)
		bTaxiFailOnce = TRUE
	ENDIF
	
	SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
	
	IF bTaxiDelayFail
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), myTaxiData.vTaxiOJDropoff) > 50
		OR UPDATE_AMBUSH_PEDS( TAS_Local.piAmbush, TAS_Local.blipPedAmbush, TAS_Local2.piAmbush, TAS_Local2.blipPedAmbush )
			Script_Cleanup()
		ENDIF
	ELSE
		Script_Cleanup()
	ENDIF

ENDPROC



PROC INITIALIZE_TAS_LOCAL()
	TAS_Local.gangmodel[0] = G_M_Y_LOST_01
	TAS_Local.gangmodel[1] = S_F_Y_Hooker_01
	TAS_Local.modelName_GangCar[0] = GBURRITO
	TAS_Local.modelName_GangCar[1] = DAEMON //GBURRITO
	TAS_Local.modelName_GangCar[2] = HEXER
	TAS_Local.modelName_GangCar[3] = EMPEROR
	
	TAS_Local2.gangmodel[0] = G_M_Y_LOST_01
	TAS_Local2.modelName_GangCar[0] = GBURRITO
	TAS_Local2.modelName_GangCar[1] = HEXER //EMPEROR2
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - The Latin Gang is chosen!")
ENDPROC

/// PURPOSE: Get ambush veh spawn position.  Range = 1-4
/// 
FUNC VECTOR GET_AMBUSH_VEHICLE_SPAWN_POSITION( INT idx )

	SWITCH idx
		CASE 1 //truck
			RETURN << 208.8206, -3319.2800, 4.7925 >> //<< 208.5512, -3318.6257, 4.7917 >> //<< 200.2243, -3318.6624, 4.7376 >>
		BREAK
		
		CASE 2 //bike
			RETURN << 202.0773, -3320.3933, 4.7657 >> //<< 209.8816, -3319.1345, 4.7924 >> 
		BREAK
		
		CASE 3 //car
			RETURN << 216.1051, -3318.8367, 4.7918 >>
		BREAK
		
		CASE 4 //car
			RETURN << 221.6868, -3324.7466, 5.3063 >>
		BREAK
		
		CASE 5 //bike
			RETURN << 200.1736, -3320.6655, 4.7361 >> //<< 210.6792, -3320.6277, 4.7932 >>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: Get ambush veh spawn heading.  Range = 1-4
/// 
FUNC FLOAT GET_AMBUSH_VEHICLE_SPAWN_HEADING( INT idx )

	SWITCH idx
		CASE 1
			RETURN 301.9600 //293.4266 //50.7207
		BREAK
		CASE 2
			RETURN 33.5561 //103.1631 
		BREAK
		CASE 3
			RETURN 87.9127
		BREAK
		CASE 4
			RETURN 195.38
		BREAK
		CASE 5
			RETURN 35.3307 //109.4646
		BREAK
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

/// PURPOSE: Get ambush ped spawn position.  Range = 1-7
///    
FUNC VECTOR GET_AMBUSH_PED_SPAWN_POSITION( INT idx )

	SWITCH idx
		CASE 1 // gangster left of boss
			RETURN << 202.35, -3321.76, 4.79 >>
		BREAK
		CASE 2 // boss guy
			RETURN << 204.27, -3321.77, 5.79 >>
		BREAK
		CASE 3 // gangster right of boss
			RETURn << 204.93, -3322.56, 4.79 >> //>><< 205.90, -3322.58, 5.80 >> //<< 205.48, -3319.82, 5.80 >>
		BREAK
		CASE 4 // gangster in the back
			RETURN << 218.19, -3323.58, 5.80 >> //<< 212.8885, -3314.0981, 5.80 >>
		BREAK
		CASE 5 // gangster girl to the side
			RETURN << 219.52, -3324.00, 5.80 >>
		BREAK
		CASE 6 // 2nd gangster girl to the side
			RETURN << 220.14, -3325.39, 5.80 >>
		BREAK
//		CASE 7
//			RETURN << 211.4085, -3316.9163, 4.7910 >>
//		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: Get ambush ped spawn heading.  Range = 1-7
///    
FUNC FLOAT GET_AMBUSH_PED_SPAWN_HEADING( INT idx )

	SWITCH idx
		CASE 1 // gangster left of boss
			RETURN 251.3766 
		BREAK
		CASE 2 // boss guy
			RETURn -129.32 //169.60
		BREAK
		CASE 3 // gangster right of boss
			RETURN 39.53 //9.74
		BREAK
		CASE 4 // gangster in the back
			RETURN -139.98 //-127.39
		BREAK
		CASE 5 // gangster girl to the side
			RETURN 147.25
		BREAK
		CASE 6 // 2nd gangster girl to the side
			RETURN 68.18
		BREAK
//		CASE 7
//			RETURN 195.4813
//		BREAK
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

/// PURPOSE: Get ambush veh spawn position.  Range = 1-4
/// 
FUNC VECTOR GET_AMBUSH_VEHICLE_SPAWN_POSITION2( INT idx )

	SWITCH idx
		CASE 1
			RETURN << 290.2584, -3236.6604, 4.8352 >>
		BREAK
		CASE 2
			RETURN << 299.2215, -3235.2480, 4.7762 >>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
	
ENDFUNC

/// PURPOSE: Get ambush veh spawn heading.  Range = 1-4
/// 
FUNC FLOAT GET_AMBUSH_VEHICLE_SPAWN_HEADING2( INT idx )

	SWITCH idx
		CASE 1
			RETURN 159.2139
		BREAK
		CASE 2
			RETURN 156.7733
		BREAK
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

FUNC VECTOR GET_GANGSTER_IDLE_WALK_POINTS( INT idx)
	SWITCH idx
		CASE 1
			RETURN << 204.43, -3327.59, 4.79 >>
		BREAK
		CASE 2
			RETURN << 207.65, -3328.01, 4.79 >>
		BREAK
		CASE 3
			RETURN << 212.60, -3327.76, 4.80 >>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_GANGSTER_WALK_POINTS( INT idx)
	SWITCH idx
		CASE 1
			RETURN << 202.24, -3322.48, 5.79 >>
		BREAK
		CASE 2
			RETURN << 203.40, -3323.22, 5.79 >>
		BREAK
		CASE 3
			RETURN << 205.66, -3322.41, 5.80 >>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Creates and inserts the ambush gang members' cars
PROC CREATE_AMBUSH_CARS()
	TAS_Local.viGangCar[0] = CREATE_VEHICLE( TAS_Local.modelName_GangCar[0], GET_AMBUSH_VEHICLE_SPAWN_POSITION(1), GET_AMBUSH_VEHICLE_SPAWN_HEADING(1) )
	TAS_Local.viGangCar[1] = CREATE_VEHICLE( TAS_Local.modelName_GangCar[1], GET_AMBUSH_VEHICLE_SPAWN_POSITION(2), GET_AMBUSH_VEHICLE_SPAWN_HEADING(2) )
	TAS_Local.viGangCar[2] = CREATE_VEHICLE( TAS_Local.modelName_GangCar[3], GET_AMBUSH_VEHICLE_SPAWN_POSITION(3), GET_AMBUSH_VEHICLE_SPAWN_HEADING(3) )
	TAS_Local.viGangCar[3] = CREATE_VEHICLE( TAS_Local.modelName_GangCar[3], GET_AMBUSH_VEHICLE_SPAWN_POSITION(4), GET_AMBUSH_VEHICLE_SPAWN_HEADING(4) )
	
	viExtraBike = CREATE_VEHICLE(TAS_Local.modelName_GangCar[2], GET_AMBUSH_VEHICLE_SPAWN_POSITION(5), GET_AMBUSH_VEHICLE_SPAWN_HEADING(5) )
	
	SET_VEHICLE_NUMBER_PLATE_TEXT(TAS_Local.viGangCar[2],"NikoB")
	SET_VEHICLE_NUMBER_PLATE_TEXT(TAS_Local.viGangCar[3],"RomanB")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"CREATE_AMBUSH_CARS completed")
ENDPROC

/// PURPOSE:
///    Creates and inserts the ambush gang members
PROC CREATE_AMBUSH_PEDS()
	TAS_Local.piAmbush[0] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[0], GET_AMBUSH_PED_SPAWN_POSITION(1), GET_AMBUSH_PED_SPAWN_HEADING(1)) // GET_GANGSTER_IDLE_WALK_POINTS(1))
	TAS_Local.piAmbush[1] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[0], GET_GANGSTER_WALK_POINTS(2), 0) 
	TAS_Local.piAmbush[2] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[0], GET_AMBUSH_PED_SPAWN_POSITION(3), GET_AMBUSH_PED_SPAWN_HEADING(3) )
	TAS_Local.piAmbush[3] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[0], GET_AMBUSH_PED_SPAWN_POSITION(4), GET_AMBUSH_PED_SPAWN_HEADING(4) )
	
	SET_PED_CONFIG_FLAG(TAS_Local.piAmbush[1], PCF_AllowMedicsToAttend, TRUE)
	
	piGangsterGirls[0] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[1], GET_AMBUSH_PED_SPAWN_POSITION(5), GET_AMBUSH_PED_SPAWN_HEADING(5) )
	piGangsterGirls[1] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[1], GET_AMBUSH_PED_SPAWN_POSITION(6), GET_AMBUSH_PED_SPAWN_HEADING(6) )
	
	SET_PED_CAN_BE_TARGETTED(piGangsterGirls[0] , FALSE)
	SET_PED_CAN_BE_TARGETTED(piGangsterGirls[1] , FALSE)

	IF NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 5, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[0], INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[1], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[1], INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)	
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[2], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[2], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[3])
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[3], INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[3], INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[3], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(TAS_Local.piAmbush[3], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
	ENDIF	
//	TAS_Local.piAmbush[4] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[1], GET_AMBUSH_PED_SPAWN_POSITION(5), GET_AMBUSH_PED_SPAWN_HEADING(5) )
//	TAS_Local.piAmbush[5] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[1], GET_AMBUSH_PED_SPAWN_POSITION(6), GET_AMBUSH_PED_SPAWN_HEADING(6) )
//	TAS_Local.piAmbush[6] = CREATE_PED( PEDTYPE_MISSION, TAS_Local.gangmodel[0], GET_AMBUSH_PED_SPAWN_POSITION(7), GET_AMBUSH_PED_SPAWN_HEADING(7) )
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"CREATE_AMBUSH_PEDS completed")
ENDPROC

/// PURPOSE:
///    Creates and inserts second wave of gang members
PROC CREATE_AMBUSH_SECOND_WAVE()
	TAS_Local2.viGangCar[0] = CREATE_VEHICLE( TAS_Local2.modelName_GangCar[0], GET_AMBUSH_VEHICLE_SPAWN_POSITION2(1), GET_AMBUSH_VEHICLE_SPAWN_HEADING2(1) )
	TAS_Local2.viGangCar[1] = CREATE_VEHICLE( TAS_Local2.modelName_GangCar[1], GET_AMBUSH_VEHICLE_SPAWN_POSITION2(2), GET_AMBUSH_VEHICLE_SPAWN_HEADING2(2) )
	
	TAS_Local2.piAmbush[0] = CREATE_PED_INSIDE_VEHICLE(TAS_Local2.viGangCar[0], PEDTYPE_MISSION, TAS_Local2.gangmodel[0], VS_DRIVER)
	TAS_Local2.piAmbush[1] = CREATE_PED_INSIDE_VEHICLE(TAS_Local2.viGangCar[0], PEDTYPE_MISSION, TAS_Local2.gangmodel[0], VS_FRONT_RIGHT)
	TAS_Local2.piAmbush[2] = CREATE_PED_INSIDE_VEHICLE(TAS_Local2.viGangCar[1], PEDTYPE_MISSION, TAS_Local2.gangmodel[0], VS_DRIVER)
	TAS_Local2.piAmbush[3] = CREATE_PED_INSIDE_VEHICLE(TAS_Local2.viGangCar[1], PEDTYPE_MISSION, TAS_Local2.gangmodel[0], VS_FRONT_RIGHT)
	
	SET_VEHICLE_NUMBER_PLATE_TEXT(TAS_Local2.viGangCar[1],"LuisLopz")
	
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo, 6, TAS_Local2.piAmbush[0], "TaxiBruce")
	SET_AMBIENT_VOICE_NAME(TAS_Local2.piAmbush[0], "TaxiBruce")
	
	GIVE_WEAPON_TO_PED( TAS_Local2.piAmbush[0], WEAPONTYPE_SMG, 100)
	GIVE_WEAPON_TO_PED( TAS_Local2.piAmbush[1], WEAPONTYPE_PISTOL, 100)
	GIVE_WEAPON_TO_PED( TAS_Local2.piAmbush[2], WEAPONTYPE_SMG, 100)
	GIVE_WEAPON_TO_PED( TAS_Local2.piAmbush[3], WEAPONTYPE_PISTOL, 100, TRUE )
	
	INT i
	FOR i = 0 TO 3
		
		SET_PED_MONEY(TAS_Local2.piAmbush[i],GET_RANDOM_INT_IN_RANGE(50,100))
		
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local2.piAmbush[i], CA_DO_DRIVEBYS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local2.piAmbush[i], CA_AGGRESSIVE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local2.piAmbush[i], CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local2.piAmbush[i], CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local2.piAmbush[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
				
		//Set peds to not influnece Wanted level
		SET_PED_CONFIG_FLAG(TAS_Local2.piAmbush[i],PCF_DontInfluenceWantedLevel, TRUE)
		
		SET_PED_SEEING_RANGE(TAS_Local2.piAmbush[i], 300.0)
		SET_PED_HEARING_RANGE(TAS_Local2.piAmbush[i], 300.0)
		SET_PED_ID_RANGE(TAS_Local2.piAmbush[i], 300.0)
		
	//	SET_PED_MAX_HEALTH( TAS_Local2.piAmbush[i], 25 )
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TAS_Local2.piAmbush[i], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local2.piAmbush[i], relPursuers )
		
		SET_PED_ACCURACY(TAS_Local2.piAmbush[i], 50)
						
	//	TASK_STAND_STILL( TAS_Local.piAmbush[i], -1 )
		
	ENDFOR
ENDPROC 

/// PURPOSE:
///    Initializes the ambush cars.  Currently just opens their trunks.
PROC INITIALIZE_AMBUSH_CARS()
	//SET_VEHICLE_DOOR_OPEN( TAS_Local.viCar1, SC_DOOR_BOOT )	//commenting out because vans don't have boots
	//SET_VEHICLE_DOOR_OPEN( TAS_Local.viCar2, SC_DOOR_BOOT )	//commenting out because vans don't have boots
	SET_VEHICLE_DOOR_OPEN( TAS_Local.viGangCar[2], SC_DOOR_BOOT )
	SET_VEHICLE_DOOR_OPEN( TAS_Local.viGangCar[3], SC_DOOR_BOOT )
	CDEBUG1LN(DEBUG_OJ_TAXI,"INITIALIZE_AMBUSH_CARS completed")
ENDPROC

/// PURPOSE:
///    Places various props and cover points for end scene
PROC CREATE_AMBUSH_OBJECTS()
//	oiBoxPiles[0] = CREATE_OBJECT (mBoxPile, << 208.3600, -3329.1101, 4.7800 >>)
//	SET_ENTITY_COORDS (oiBoxPiles[0], << 208.3600, -3329.1101, 4.7800 >>)
//	SET_ENTITY_ROTATION (oiBoxPiles[0], << 0.0700, 0.0600, -3.3200 >>)
	
	oiBoxPiles[0] = CREATE_OBJECT (mBoxPile, <<204.844513,-3333.998291,4.795367>>)
	SET_ENTITY_COORDS (oiBoxPiles[0], <<204.844513,-3333.998291,4.795367>>)
	SET_ENTITY_ROTATION (oiBoxPiles[0], <<-0.036243,-0.001240,91.560631>>)
	
	oiBoxPiles[1] = CREATE_OBJECT (mBoxPile, << 212.6700, -3328.7700, 4.7900 >>)
	SET_ENTITY_COORDS (oiBoxPiles[1], << 212.6700, -3328.7700, 4.7900 >>)
	SET_ENTITY_ROTATION (oiBoxPiles[1], << -0.0400, -0.0100, 86.3600 >>)
	
	oiBoxPiles[2] = CREATE_OBJECT (mBoxPile, << 204.8248, -3328.6311, 4.7915 >>)
	SET_ENTITY_COORDS (oiBoxPiles[2], << 204.8248, -3328.6311, 4.7915 >>)
	SET_ENTITY_ROTATION (oiBoxPiles[2], << -0.0027, -0.0374, 0.1414 >>)
	
	oiRegBox = CREATE_OBJECT (mRegularBox, << 214.2505, -3314.6729, 4.7883 >>)
	SET_ENTITY_COORDS (oiRegBox, << 214.2505, -3314.6729, 4.7883 >>)
	SET_ENTITY_ROTATION (oiRegBox, << 0.0, 0.0, -17.3990 >>)
	
	//JD - 5/9/12 - For some reason mExpBarrel is loading commenting out for now since it's not a critical prop
	IF HAS_MODEL_LOADED(mExpBarrel)
		oiExpBarrel = CREATE_OBJECT (mExpBarrel, << 220.7266, -3320.0015, 5.2749 >>)
		SET_ENTITY_COORDS (oiExpBarrel, << 220.7266, -3320.0015, 5.2749 >>)
		SET_ENTITY_ROTATION (oiExpBarrel, <<0, 0, 0>>)
	ENDIF
ENDPROC

/// PURPOSE:
///    Initializes the ambush gang member peds with prelim data and and tasks them to just stand there.
PROC INITIALIZE_AMBUSH_PEDS()
	INT i = 0

//	GIVE_WEAPON_TO_PED( TAS_Local.piAmbush[0], WEAPONTYPE_ASSAULTRIFLE, 100)
//	GIVE_WEAPON_TO_PED( TAS_Local.piAmbush[1], WEAPONTYPE_PISTOL, 100)
//	GIVE_WEAPON_TO_PED( TAS_Local.piAmbush[2], WEAPONTYPE_ASSAULTRIFLE, 100)
//	GIVE_WEAPON_TO_PED( TAS_Local.piAmbush[3], WEAPONTYPE_PISTOL, 100)
	
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,4,TAS_Local.piAmbush[1],"TaxiDom")
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,5,TAS_Local.piAmbush[3],"TaxiGangM")
	SET_AMBIENT_VOICE_NAME(TAS_Local.piAmbush[1], "TaxiDom")
	SET_AMBIENT_VOICE_NAME(TAS_Local.piAmbush[3], "TaxiGangM")

	// first wave of gangster dudes
	FOR i = 0 TO 3
		
		SET_PED_MONEY(TAS_Local.piAmbush[i],GET_RANDOM_INT_IN_RANGE(20,50))
		
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local.piAmbush[i], CA_DO_DRIVEBYS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local.piAmbush[i], CA_AGGRESSIVE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local.piAmbush[i], CA_USE_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local.piAmbush[i], CA_LEAVE_VEHICLES, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(TAS_Local.piAmbush[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
		
		//Set peds to not influnece Wanted level
		SET_PED_CONFIG_FLAG(TAS_Local.piAmbush[i],PCF_DontInfluenceWantedLevel, TRUE)
		
		SET_PED_SEEING_RANGE(TAS_Local.piAmbush[i], 300.0)
		SET_PED_HEARING_RANGE(TAS_Local.piAmbush[i], 300.0)
		SET_PED_ID_RANGE(TAS_Local.piAmbush[i], 300.0)
		
	//	SET_PED_MAX_HEALTH( TAS_Local.piAmbush[i], 25 )
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TAS_Local.piAmbush[i], FALSE)
		//SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[i], relPursuers )
		
		SET_PED_ACCURACY(TAS_Local.piAmbush[i], kPedLowAccuracy)
		
	ENDFOR
	
	SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[0], relPursuers )
	SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[1], relPursuers )
	SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[2], relPursuers )
	SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[3], relPursuers )
	
	// gangster girls
	FOR i = 0 TO 1
		
		SET_PED_CONFIG_FLAG(piGangsterGirls[i],PCF_DontInfluenceWantedLevel, TRUE)
		
		SET_PED_SEEING_RANGE(piGangsterGirls[i], 100.0)
		SET_PED_HEARING_RANGE(piGangsterGirls[i], 100.0)
		SET_PED_ID_RANGE(piGangsterGirls[i], 100.0)
		
	//	SET_PED_MAX_HEALTH( piGangsterGirls[i], 25 )
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piGangsterGirls[i], TRUE)
	//	SET_PED_RELATIONSHIP_GROUP_HASH( piGangsterGirls[i], relPursuers )
		
	ENDFOR
	
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,7,piGangsterGirls[1],"TaxiGangGirl2")
	ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,8,piGangsterGirls[0],"TaxiGangGirl1")
	SET_AMBIENT_VOICE_NAME(piGangsterGirls[1], "TaxiGangGirl2")
	SET_AMBIENT_VOICE_NAME(piGangsterGirls[0], "TaxiGangGirl1")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"INITIALIZE_AMBUSH_PEDS completed")

ENDPROC

PROC DEFINE_AMBUSH_STRUCT()
		
	INITIALIZE_TAS_LOCAL()
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Picked Shipyard ambush Location")
	
	//Drop off pos: ShipYard
	TAS_Local.DropOffPos = << 187.5257, -3320.1628, 4.6276 >>

ENDPROC

PROC DRAW_AMBUSH_LOCATIONS()
	DRAW_DEBUG_TEXT("Drop off Pos", TAS_Local.DropOffPos)
	DRAW_DEBUG_SPHERE(TAS_Local.DropOffPos, 1.0)
	
	//
	DRAW_DEBUG_TEXT("Big Car 1 Start", GET_AMBUSH_VEHICLE_SPAWN_POSITION( 1 ))
	DRAW_DEBUG_SPHERE(GET_AMBUSH_VEHICLE_SPAWN_POSITION( 1 ), 1.0)
	
	//
	DRAW_DEBUG_TEXT("Big Car 2 Start", GET_AMBUSH_VEHICLE_SPAWN_POSITION( 2 ))
	DRAW_DEBUG_SPHERE(GET_AMBUSH_VEHICLE_SPAWN_POSITION( 2 ), 1.0)
		
	//
	DRAW_DEBUG_TEXT("Small Car 1 Start", GET_AMBUSH_VEHICLE_SPAWN_POSITION( 3 ))
	DRAW_DEBUG_SPHERE(GET_AMBUSH_VEHICLE_SPAWN_POSITION( 3 ), 1.0)
	
	//
	DRAW_DEBUG_TEXT("Small Car 2 Start", GET_AMBUSH_VEHICLE_SPAWN_POSITION( 4 ))
	DRAW_DEBUG_SPHERE(GET_AMBUSH_VEHICLE_SPAWN_POSITION( 4 ), 1.0)

ENDPROC

PROC SET_RELATIONSHIP_BETWEEN_EVERYONE( RELATIONSHIP_TYPE relTyp )

	SET_RELATIONSHIP_BETWEEN_GROUPS(relTyp, relPursuers, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(relTyp, relPursuers, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(relTyp, RELGROUPHASH_COP, relPursuers)

	
ENDPROC

/// PURPOSE:
///    Initializes script variables
PROC INITIALIZE_SCRIPT_VARIABLES()
	
	myTaxiData.vTaxiOJ_WarpPtPickup = << 107.3107, -1124.8654, 28.1980 >> //<< -292.2656, 250.1203, 87.7068 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 88.9745 //279.13
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = << 195.7672, -2981.7319, 4.8916 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 173.6662
	
	ADD_RELATIONSHIP_GROUP("TAXI_Pursuers", relPursuers)
	
	SET_RELATIONSHIP_BETWEEN_EVERYONE( ACQUAINTANCE_TYPE_PED_LIKE )
		
	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_04_GOTYOURBACK)
		
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData,CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF,CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	DEFINE_AMBUSH_STRUCT()

	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Got your back")
			
			INIT_ODDJOB_TAXI_WIDGETS()
			
			ADD_WIDGET_STRING("Debug Taxi Bonus")
				ADD_WIDGET_BOOL("Terminator ",bDebugBonusTermintator)
				
			ADD_WIDGET_BOOL("Restart Taxi Mission", bForceReset)
			
			ADD_WIDGET_BOOL("Passenger Invincible", bPassengerInvincible)
		STOP_WIDGET_GROUP()
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Got Your Back ~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()
	
	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugBonusTermintator
		sDebugString[0] = "Num Baddies Player Killed : "
		sDebugString[0] += TAXI_UTILS_GET_STRING_FROM_INT_SP(iPlayerKilledCount)
		sDebugString[0] += " /8"
		DRAW_DEBUG_TEXT_2D(sDebugString[0],<<0.07,0.75,0.0>>)
		
		//Show if bonus has been unlocked or not
		IF iPlayerKilledCount < TAXI_CONST_NUM_TOTAL_ENEMIES
			sDebugString[1]= "Terminator Bonus LOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>,255,0,0)
			
		ELSE
			sDebugString[1]= "Terminator Bonus UnLOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>)
		ENDIF
	ENDIF
	
	IF bForceReset
		DEFINE_AMBUSH_STRUCT()
		REQUEST_TAXI_ODDJOB_GYB_STREAMS_STAGE_01()
		myTaxiData.vTaxiOJDropoff = TAS_Local.DropOffPos
		bForceReset = FALSE
		bResetting = TRUE
	ENDIF
	
	IF bResetting
		IF HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()
			bResetting = FALSE
		ENDIF
	ENDIF
	
	IF bPassengerInvincible
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			SET_ENTITY_INVINCIBLE(myTaxiData.piTaxiPassenger, TRUE)
		ENDIF
		bPassengerInvincible = FALSE
	ENDIF
ENDPROC
#ENDIF

PROC TASK_ALL_INITIAL_ANIMS()
	
	IF NOT IS_PED_INJURED(piGangsterGirls[0])
	AND NOT IS_PED_INJURED(piGangsterGirls[0])
	AND NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
		//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId)
			iSideSceneId = CREATE_SYNCHRONIZED_SCENE(scenePositionSide, sceneRotationSide)
			SET_SYNCHRONIZED_SCENE_LOOPED(iSideSceneId,TRUE)
			TASK_SYNCHRONIZED_SCENE(TAS_Local.piAmbush[3], iSideSceneId, sSyncSceneDict, "hooker_loop_a_biker_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
			TASK_SYNCHRONIZED_SCENE(piGangsterGirls[0], iSideSceneId, sSyncSceneDict, "hooker_loop_a_hooker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
			TASK_SYNCHRONIZED_SCENE(piGangsterGirls[1], iSideSceneId, sSyncSceneDict, "hooker_loop_a_hooker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE)
		//ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
	AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
	AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
		GIVE_WEAPON_TO_PED(TAS_Local.piAmbush[0], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
		//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId)
			iMainSceneId = CREATE_SYNCHRONIZED_SCENE(scenePositionMain, sceneRotationMain)
			SET_SYNCHRONIZED_SCENE_LOOPED(iMainSceneId,TRUE)
			TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[0], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[1], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[2], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"main scene id initialised, set to ", iMainSceneId)
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:  Tasks all hostile peds to walk away.
///    
/// PARAMS:
///    Leavers - Array of hostile peds.
PROC TASK_ALL_PEDS_TO_LEAVE(PED_INDEX &Leavers[])
	INT i
	
	REPEAT COUNT_OF(Leavers) i
		IF NOT IS_PED_INJURED(Leavers[i])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Leavers[i], FALSE)
			SET_PED_COMBAT_ATTRIBUTES(Leavers[i], CA_DO_DRIVEBYS , FALSE)
			SET_PED_COMBAT_ATTRIBUTES(Leavers[i], CA_USE_VEHICLE , FALSE)
			SET_PED_COMBAT_ATTRIBUTES(Leavers[i], CA_LEAVE_VEHICLES, TRUE)
			TASK_LEAVE_ANY_VEHICLE(Leavers[i])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:  Tasks an array of peds to attack hated targets.  Also re-aligns ped relationships.
///    
/// PARAMS:
///    Fighters - Array of soon to be hostile peds.
PROC TASK_ALL_PEDS_TO_FIGHT(PED_INDEX &Fighters[])
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, myTaxiData.relPassenger)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, relPursuers)
	
	INT i
	WEAPON_TYPE currentWeapon
	
	REPEAT COUNT_OF(Fighters) i
		IF NOT IS_PED_INJURED(Fighters[i])
			CLEAR_PED_TASKS( Fighters[i] )
			
			GET_CURRENT_PED_WEAPON(Fighters[i], currentWeapon)
			IF (currentWeapon = WEAPONTYPE_UNARMED)
				GIVE_WEAPON_TO_PED(Fighters[i], WEAPONTYPE_PISTOL, INFINITE_AMMO)	
			ENDIF
			
		//	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TAS_Local2.piAmbush[i], FALSE)
			
			SET_PED_ACCURACY(Fighters[i], 30)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(Fighters[i], 1000.0)
			
			SET_PED_KEEP_TASK(Fighters[i], TRUE)
		ENDIF
	ENDREPEAT
		
	
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC KILL_ALL_TAXI_OJ_ENEMIES(PED_INDEX &Fighters[])

	INT iIterator
	REPEAT COUNT_OF(Fighters) iIterator
		IF NOT IS_ENTITY_DEAD(Fighters[iIterator])
			SET_ENTITY_HEALTH(Fighters[iIterator],2)
		ENDIF
	ENDREPEAT
		
			
ENDPROC
#ENDIF

PROC TASK_PEDS_TO_FLEE (PED_INDEX &Runners[])
	INT i
	
	REPEAT COUNT_OF(Runners) i
		IF NOT IS_PED_INJURED(Runners[i])
			CLEAR_PED_TASKS_IMMEDIATELY( Runners[i] )
			
			SET_PED_FLEE_ATTRIBUTES(Runners[i], FA_CAN_SCREAM|FA_USE_COVER|FA_PREFER_PAVEMENTS, TRUE)
			
			CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
			OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
				
				TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(1, 1000))
				TASK_SMART_FLEE_PED(NULL, myTaxiData.piTaxiPassenger, fGoodFleeDistance, -1)
			
			CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
			TASK_PERFORM_SEQUENCE(Runners[i], seqIndexTaxiAIAction)
			
			SET_PED_KEEP_TASK(Runners[i], TRUE)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"stripper set to flee")
			
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:  Sets the accuracy of a bunch of peds a bit higher
///    
/// PARAMS:
///    Fighters - Array of hostile peds
PROC LET_PEDS_HIT_THINGS(PED_INDEX &Fighters[])
	INT i
	
	REPEAT COUNT_OF(Fighters) i
		IF NOT IS_PED_INJURED(Fighters[i])			
			SET_PED_ACCURACY(Fighters[i], kPedNotSoLowAccuracy)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_ANY_AMBUSH_PED_OUT_OF_VEHICLE(PED_INDEX &Fighters[])
	INT i
	
	REPEAT COUNT_OF(Fighters) i
		IF NOT IS_PED_INJURED(Fighters[i])
			IF NOT IS_PED_IN_ANY_VEHICLE(Fighters[i])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:  Blips all peds in an array hostile
///    
/// PARAMS:
///    Fighters - Array of hostile peds
///    Blips - Array of blips to be stored
PROC BLIP_ALL_PEDS(PED_INDEX &Fighters[], BLIP_INDEX &Blips[])
	INT i
	
	REPEAT COUNT_OF(Blips) i
		IF NOT IS_PED_INJURED(Fighters[i])
			IF NOT DOES_BLIP_EXIST(Blips[i])
				Blips[i] = CREATE_BLIP_ON_ENTITY(Fighters[i],FALSE)
				CDEBUG1LN(DEBUG_OJ_TAXI,"[AMBUSH] [BLIP] - Blipped baddie" )
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

// hard coded for the globals we have here
PROC BLIP_SECOND_WAVE_CARS()
	IF NOT DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[0])
	AND NOT DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[1])
		TAS_Local2.blipVehAmbush[0] = CREATE_BLIP_ON_ENTITY(TAS_Local2.viGangCar[0], FALSE)
		TAS_Local2.blipVehAmbush[1] = CREATE_BLIP_ON_ENTITY(TAS_Local2.viGangCar[1], FALSE)
	ENDIF
ENDPROC

// hard coded for the globals we have here
PROC REMOVE_SECOND_WAVE_CAR_BLIPS()
	
	IF DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[0])
		REMOVE_BLIP(TAS_Local2.blipVehAmbush[0])
	ENDIF
	IF DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[1])
		REMOVE_BLIP(TAS_Local2.blipVehAmbush[1])
	ENDIF
ENDPROC

PROC PRINT_CAR_UPDATE(STRING Carname, STRING UpdateString)
	CDEBUG1LN(DEBUG_OJ_TAXI,"Carname - ","",Carname,"", " - TAXI - ", updateString )
ENDPROC

/// PURPOSE:  Sets variables for the passenger to go aggressive.  Also tasks him into combat.
///    
PROC SET_PASSENGER_IN_COMBAT()
	IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			
			IF NOT DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
				myTaxiData.blipTaxiPassenger = CREATE_BLIP_ON_ENTITY(myTaxiData.piTaxiPassenger)
			ENDIF
			
			WEAPON_TYPE currentWeapon
			GET_CURRENT_PED_WEAPON(myTaxiData.piTaxiPassenger, currentWeapon)
			IF (currentWeapon = WEAPONTYPE_UNARMED)
				GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 100)
			ENDIF
			
			//SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_DO_DRIVEBYS, FALSE)
			//SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_LEAVE_VEHICLES, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_BLIND_FIRE_IN_COVER, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_USE_COVER, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_USE_PROXIMITY_FIRING_RATE, TRUE) 
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_CAN_USE_PEEKING_VARIATIONS, TRUE)
			//SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_JUST_SEEK_COVER, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
			
			SET_PED_SEEING_RANGE(myTaxiData.piTaxiPassenger, 300.0)
			SET_PED_HEARING_RANGE(myTaxiData.piTaxiPassenger, 300.0)
			SET_PED_ID_RANGE(myTaxiData.piTaxiPassenger, 300.0)
			
			SET_PED_COMBAT_MOVEMENT(myTaxiData.piTaxiPassenger, CM_DEFENSIVE)
			SET_PED_CAN_EVASIVE_DIVE (myTaxiData.piTaxiPassenger, TRUE)
			
			SET_PED_ACCURACY(myTaxiData.piTaxiPassenger, 80)
			
			SET_PED_COMBAT_RANGE(myTaxiData.piTaxiPassenger, CR_FAR)
			
			SET_PED_ANGLED_DEFENSIVE_AREA(myTaxiData.piTaxiPassenger, <<203.7838, -3326.7012, 4.7901>>, <<204.2880, -3334.8457,6.8830>>, 5.5)
		//	GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 200, FALSE)
							
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, myTaxiData.relPassenger)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, myTaxiData.relPassenger, relPursuers)
			CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
			OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
				TASK_SEEK_COVER_FROM_POS( NULL, <<201.9434, -3324.0291, 4.7888>>, 5000, TRUE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED( NULL,200.0)
			CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
			TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiAIAction)
			
			SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:  Tasks the player to walk to gang members and look like he's saying stuff
///    
PROC TASK_PASSENGER_TO_WALK()
	IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			
			SET_PED_RELATIONSHIP_GROUP_HASH( myTaxiData.piTaxiPassenger, myTaxiData.relPassenger )
			
			TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, TAS_Local.piAmbush[1], INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
			CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
			OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_USE_RIGHT_ENTRY)
				//TASK_GO_TO_COORD_ANY_MEANS( NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 0.0, 2.0, 0.0 >>), PEDMOVEBLENDRATIO_WALK, NULL )
				//TASK_FOLLOW_NAV_MESH_TO_COORD( NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, << 1.1, 2.0, 0.0 >>), PEDMOVEBLENDRATIO_WALK )
				TASK_FOLLOW_NAV_MESH_TO_COORD( NULL, vPassengerWalkPoint, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP + 10000 )
				//TASK_CLEAR_LOOK_AT(NULL)
				TASK_ACHIEVE_HEADING(NULL, -30.30)
				TASK_CLEAR_LOOK_AT(NULL)
				//TASK_TURN_PED_TO_FACE_ENTITY(NULL, TAS_Local.piAmbush[1])
			CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
			TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiAIAction)
		
		ENDIF
	ENDIF
		
ENDPROC

PROC TASK_GANGSTER_TO_WALK(PED_INDEX & piGangster, INT iDest, INT iDelayStart = 1, INT iDelayEnd = 2000)
	
	IF DOES_ENTITY_EXIST(piGangster)
		IF NOT IS_PED_INJURED(piGangster)
			
			CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
			OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
				TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(iDelayStart, iDelayEnd))
				TASK_GO_TO_COORD_ANY_MEANS( NULL, GET_GANGSTER_WALK_POINTS(iDest+1), 1.0, NULL )
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.piTaxiPassenger, -1)
			CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
			TASK_PERFORM_SEQUENCE(piGangster, seqIndexTaxiAIAction)
			
			SET_PED_KEEP_TASK(piGangster, TRUE)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC TASK_SECOND_WAVE_CAR_RECS()
	IF IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[0])
		START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(TAS_Local2.viGangCar[0], iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(0),ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY | SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
		SET_PLAYBACK_SPEED(TAS_Local2.viGangCar[0], 1)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[1])
		START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(TAS_Local2.viGangCar[1], iTaxiGYBCarRecFileNo, GET_TAXI_OJ_GYB_VEHICLE_RECORDING_NAME(1),ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY | SWITCH_ON_ANY_VEHICLE_IMPACT),1000)
		SET_PLAYBACK_SPEED(TAS_Local2.viGangCar[1], 1)
	ENDIF
ENDPROC

ENUM TAXI_GYB_CAMSTATUS
	TAXIGYB_CAM1,
	TAXIGYB_CAM2,
	TAXIGYB_CAM3,
	TAXIGYB_CAM4,
	TAXIGYB_CAM5,
	TAXIGYB_CAM6,
	TAXIGYB_INACTIVE
ENDENUM
TAXI_GYB_CAMSTATUS eTaxiGYBCamStatus = TAXIGYB_CAM1 //TAXIGYB_CAM2

INT iCamTimer
INT iCamEnableTimer

BOOL bRStickRight
BOOL bRStickLeft

FUNC BOOL TAXI_CAM_RIGHT()
	
	FLOAT fRightX
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	
	IF fRightX > 0.8
		IF NOT bRStickRight
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAXI_VIEW2")
				CLEAR_HELP()
			ENDIF
			
			bRStickRight = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		IF bRStickRight
			bRStickRight = FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TAXI_CAM_LEFT()
	
	FLOAT fRightX
	
	fRightX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	
	IF fRightX < -0.8
		IF NOT bRStickLeft
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAXI_VIEW2")
				CLEAR_HELP()
			ENDIF
			
			bRStickLeft = TRUE
			RETURN TRUE
		ENDIF
	ELSE
		IF bRStickLeft
			bRStickLeft = FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC ACTIVATE_GYB_CAM(VECTOR vCamPos, VECTOR vCamRot, FLOAT fFOV)
	
	IF NOT DOES_CAM_EXIST(myTaxiData.camTaxi)
		CREATE_TAXI_OJ_CAM(myTaxiData.camTaxi,<<0,0,0>>, <<0,0,0>>, fFOV)
	ENDIF
	
	SET_CAM_COORD(myTaxiData.camTaxi, vCamPos)
	SET_CAM_ROT(myTaxiData.camTaxi, vCamRot)
	SHAKE_CAM(myTaxiData.camTaxi, "HAND_SHAKE", 0.3)
	
ENDPROC

PROC TAXI_HANDLE_CINEMATIC_CAM()
	
	VECTOR vCamPos
	VECTOR vCamRot
	FLOAT fFOV
	
	TEXT_LABEL_23 sCamDialogueLine
	
	sCamDialogueLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	//			***** NEW CAM PLAN *****
	// first cam - first two lines
	// second cam - in front of the side scene, switch on this line:
	// 		"txm12_ig1c_3" oh he looks really pissed
	// third cam - delay a couple of seconds and then go behind bikers
	// fourth cam - diagonal view, switch on this line:
	// 		"txm12_deal1_3" i told you, don't worry about kelly
	// fifth cam - behind bikers again, switch on this line: 
	// 		"txm12_deal1_5"
	// sixth cam - side view, switch on this line
	// 		"txm12_deal1_7"
	
	
	IF m_AmbushState < AMB_COMBAT
	AND GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < 50
	AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT bCutToGamePlay
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			IF DOES_CAM_EXIST(myTaxiData.camTaxi)
				IF NOT IS_CAM_ACTIVE(myTaxiData.camTaxi)
					CDEBUG1LN(DEBUG_OJ_TAXI,"enabling cutscene cam")
					
					iCamEnableTimer = GET_GAME_TIMER()
					
					SET_CAM_ACTIVE(myTaxiData.camTaxi, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF m_AmbushState < AMB_COMBAT
	AND GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < 50
	AND myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT bCutToGamePlay
		IF bEnableBonusCam
			
			SWITCH eTaxiGYBCamStatus
				CASE TAXIGYB_CAM1
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam1")
					
					iCamTimer = GET_GAME_TIMER()
					
					vCamPos = <<223.3000, -3326.3000, 6.4000>> 	//<<188.9051, -3323.9082, 6.0142>>
					vCamRot = <<-4.1000, -0.0000, 78.0000>> 	//<<-0.3250, 0.0006, -92.2181>>
					fFOV = 24.3  //24.45
					
					ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
					
					eTaxiGYBCamStatus = TAXIGYB_CAM2
					
				BREAK
				
				CASE TAXIGYB_CAM2
					IF ARE_STRINGS_EQUAL("txm12_ig1c_3", sCamDialogueLine)
					
						CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam2")
						
						iCamTimer = GET_GAME_TIMER()
						
						vCamPos = <<211.1000, -3326.1001, 5.8000>> //<<225.2906, -3328.0032, 5.8274>> 	//<<226.5799, -3330.8562, 6.2288>>
						vCamRot = <<4.1000, 0.0000, -81.2000>> //<<0.8983, -0.0169, 73.7207>> 		//<<-1.2353, 0.0016, 64.0858>>
						fFOV = 24.3000 //19.0028 	//21.7
						
						ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
						
						eTaxiGYBCamStatus = TAXIGYB_CAM3
					ENDIF
				BREAK
				
				CASE TAXIGYB_CAM3
					IF (GET_GAME_TIMER() - iCamTimer) > 5000
						CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam3")
						
						iCamTimer = GET_GAME_TIMER()
						
						vCamPos = <<202.0472, -3314.8298, 5.9369>>
						vCamRot = <<-1.9358, -0.0169, 169.6817>>
						fFOV = 20.9113
						
						ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
						
						eTaxiGYBCamStatus = TAXIGYB_CAM4
					ENDIF
					
				BREAK
				
				CASE TAXIGYB_CAM4
					IF ARE_STRINGS_EQUAL("txm12_deal1_3", sCamDialogueLine)
					
						CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam4")
						
						iCamTimer = GET_GAME_TIMER()
						
						vCamPos = <<194.7488, -3328.1011, 5.9471>>
						vCamRot = <<-0.3369, -0.0169, -55.7257>>
						fFOV = 24.5237
						
						ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
						
						eTaxiGYBCamStatus = TAXIGYB_CAM5
					ENDIF
				BREAK
				
				CASE TAXIGYB_CAM5
					IF ARE_STRINGS_EQUAL("txm12_deal1_5", sCamDialogueLine)
					
						CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam5")
						
						iCamTimer = GET_GAME_TIMER()
						
						vCamPos = <<202.0472, -3314.8298, 5.9369>>
						vCamRot = <<-1.9358, -0.0169, 169.6817>>
						fFOV = 20.9113
						
						ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
						
						eTaxiGYBCamStatus = TAXIGYB_CAM6
					ENDIF
				BREAK
				
				CASE TAXIGYB_CAM6
					IF ARE_STRINGS_EQUAL("txm12_deal1_7", sCamDialogueLine)
					
						CDEBUG1LN(DEBUG_OJ_TAXI,"reached cutscene cam6")
						
						iCamTimer = GET_GAME_TIMER()
						
						vCamPos = <<194.1000, -3324.8999, 6.2000>> 	//<<211.1000, -3326.1001, 5.8000>>
						vCamRot = <<-4.6000, 0.0000, -83.1000>> 	//<<4.1000, 0.0000, -81.2000>>
						fFOV = 29.0	//24.3000
						
						ACTIVATE_GYB_CAM(vCamPos, vCamRot, fFOV)
						
						eTaxiGYBCamStatus = TAXIGYB_INACTIVE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			AND (GET_GAME_TIMER() - iCamEnableTimer) > 500
				CDEBUG1LN(DEBUG_OJ_TAXI,"disabling cutscene cam")
				
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					SET_CAM_ACTIVE(myTaxiData.camTaxi, FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF	
	ELIF bEnableBonusCam
	
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_CINEMATIC_CAM: Player got out of the car, disabling cutscene cam")
			
			IF DOES_CAM_EXIST(myTaxiData.camTaxi)
				IF IS_CAM_ACTIVE(myTaxiData.camTaxi)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					SET_CAM_ACTIVE(myTaxiData.camTaxi, FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF bCutToGamePlay
			IF NOT IS_TIMER_STARTED(pulseTimer)
				IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
				AND IS_CAM_ACTIVE(myTaxiData.camTaxi)
					IF NOT bPulseTriggered
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_CINEMATIC_CAM: triggering pulse")
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPulseTriggered = TRUE
					ENDIF
				ENDIF
				RESTART_TIMER_NOW(pulseTimer)
			ELIF GET_TIMER_IN_SECONDS(pulseTimer) > 0.3
				IF DOES_CAM_EXIST(myTaxiData.camTaxi)
					IF IS_CAM_ACTIVE(myTaxiData.camTaxi)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_HANDLE_CINEMATIC_CAM: cutting to gameplay")
						SET_CAM_ACTIVE(myTaxiData.camTaxi, FALSE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
	ENDIF
	
ENDPROC

/*                   __                        __         
                    /\ \                      /\ \        
   __       ___ ___ \ \ \____   __  __    ____\ \ \___    
 /'__`\   /' __` __`\\ \ '__`\ /\ \/\ \  /',__\\ \  _ `\  
/\ \L\.\_ /\ \/\ \/\ \\ \ \L\ \\ \ \_\ \/\__, `\\ \ \ \ \ 
\ \__/.\_\\ \_\ \_\ \_\\ \_,__/ \ \____/\/\____/ \ \_\ \_\
 \/__/\/_/ \/_/\/_/\/_/ \/___/   \/___/  \/___/   \/_/\/_/
*/
/// PURPOSE:  This is a state machine driven update function.
///    1)  Player drops off passenger.  Player sees a bunch of gang members milling about.
///    2)  Passenger goes to talk to gang members.
///    3)  Passenger's discussion with gang members gets heated.
///    4)  Passenger pulls a piece and wastes a gang member.
///    5)  All remaining gang memebers open fire.
///    6)  Player is awarded if all gang members are killed and passenger survives.
///    
/// RETURNS:  State machine returns TRUE once all states have been completed.  FALSE otherwise
///    
FUNC BOOL PROCESS_AMBUSH()
	
	IF m_AmbushState < AMB_COMBAT
	AND (  IS_PED_INJURED(TAS_Local.piAmbush[0]) 
		OR IS_PED_INJURED(TAS_Local.piAmbush[1]) 
		OR IS_PED_INJURED(TAS_Local.piAmbush[2])  
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[0]))
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[1])) 
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[2]))
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[0], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[1], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
		OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[2], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON)) 
		OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TAS_Local.piAmbush[1]) < 6.0)	
		OR ( IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<204.078323,-3331.305420,6.040432>>,<<7.500000,7.500000,1.250000>>)
			AND	(  ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[0]))
				OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[1]))
				OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[2]))
				OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[0]))
				OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[1]))
				OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),TAS_Local.piAmbush[2])) ) ) )
		
		
//		SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_PLAYER_RUNS, TRUE, TRUE)
		
		myTaxiData.bIsTaxiOJInCombat = TRUE
		m_AmbushState = AMB_COMBAT
		CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - early aggro moving on to state AMB_COMBAT")
	ENDIF
	
	
	SWITCH m_AmbushState
		CASE AMB_INIT
			
			SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_AMBUSHED_TALK,TRUE)
			CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TRUE)
			
			TASK_PASSENGER_TO_WALK()
			//GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 100, FALSE, TRUE)
			
			CLEAR_BIT( iLocalBitSet, k_bPassengerTalking )
			CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_TALKING")
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId)
				IF NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
				AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
				AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
					
					iMainSceneId = CREATE_SYNCHRONIZED_SCENE(scenePositionMain, sceneRotationMain)
					SET_SYNCHRONIZED_SCENE_LOOPED(iMainSceneId,TRUE)
					TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[0], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[1], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[2], iMainSceneId, "oddjobs@taxi@argument", "idle_a_biker_c", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"HAD TO RE-INITIALISE THE MAIN SCENE!  set to ", iMainSceneId)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_OJ_TAXI,"main scene is running, that's great")
			ENDIF
			
			// init cams here
			
			iAmbushTimer = GET_GAME_TIMER()
			m_AmbushState = AMB_TALKING
		BREAK
		
		CASE AMB_TALKING
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_BIT_SET(iLocalBitSet, k_bSideConvoPlayed)
				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_AMBUSH_EXTRA_BANTER, TRUE)
				SET_BIT(iLocalBitSet, k_bSideConvoPlayed)
				START_TIMER_NOW(ambushBanterTimer)
			ENDIF
			
			IF (GET_GAME_TIMER() - iAmbushTimer) > 4000
				IF NOT IS_PLAYER_CONTROL_ON(GET_PLAYER_INDEX())
					IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
						PRINT_HELP("TAXI_VIEW")
					ENDIF
					
					bEnableBonusCam = TRUE
					
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
					AND NOT IS_PED_INJURED(piGangsterGirls[0])
					AND NOT IS_PED_INJURED(piGangsterGirls[1])
						iSideSceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePositionSide, sceneRotationSide)
						TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[3], iSideSceneId2, "oddjobs@taxi@argument", "bridge_biker_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (piGangsterGirls[0], iSideSceneId2, "oddjobs@taxi@argument", "bridge_hooker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (piGangsterGirls[1], iSideSceneId2, "oddjobs@taxi@argument", "bridge_hooker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
					ENDIF
					
					SET_PLAYER_CONTROL(GET_PLAYER_INDEX(), TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_TALKING2")
					
				ENDIF
				
				// stick camera state machine here
				
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAXI_VIEW")
			AND (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
				CLEAR_HELP()
			ENDIF
			
			
//			IF IS_TIMER_STARTED(ambushBanterTimer)
//				CDEBUG1LN(DEBUG_OJ_TAXI,"talking has been going for ", GET_TIMER_IN_SECONDS(ambushBanterTimer), " seconds")
//			ENDIF
			
			IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			AND ((NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()) OR (GET_TIMER_IN_SECONDS_SAFE(ambushBanterTimer) > 9))
				IF IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, vPassengerWalkPoint, << 1.5, 1.5, 2.0>>)
				AND GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				AND (IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iMainSceneId) > 0.995)
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - Passenger has reached his mark, proceeding to talk some shit.")
					
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
						iMainSceneId2 = CREATE_SYNCHRONIZED_SCENE(scenePositionMain, sceneRotationMain)
						TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[0], iMainSceneId2, "oddjobs@taxi@argument", "stand_off_biker_a", INSTANT_BLEND_IN, SLOW_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[1], iMainSceneId2, "oddjobs@taxi@argument", "stand_off_biker_b", INSTANT_BLEND_IN, SLOW_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[2], iMainSceneId2, "oddjobs@taxi@argument", "stand_off_biker_c", INSTANT_BLEND_IN, SLOW_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME ( iMainSceneId2, TRUE)
					ENDIF
					TASK_PLAY_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@taxi@argument", "stand_off_passenger", SLOW_BLEND_IN)
					
					iSyncFailSafeTimer = GET_GAME_TIMER()
					iAmbushTimer = GET_GAME_TIMER()
					m_AmbushState = AMB_TALKING2
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId)
						SET_SYNCHRONIZED_SCENE_RATE(iMainSceneId, 1.45)
//						CDEBUG1LN(DEBUG_OJ_TAXI,"main scene id phase = ", GET_SYNCHRONIZED_SCENE_PHASE(iMainSceneId))
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"main scene id NOT running")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AMB_TALKING2
			IF GET_GAME_TIMER() - iAmbushTimer > 2500
			AND NOT IS_BIT_SET( iLocalBitSet, k_bPassengerTalking )
				SET_NEXT_TAXI_SPEECH( myTaxiData,TAXI_DI_AMBUSHED,TRUE, TRUE)
				SET_BIT( iLocalBitSet, k_bPassengerTalking )
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iMainSceneId2) >= TAXI_PASSENGER_GUN_TIME
			AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
			AND NOT IS_BIT_SET( iLocalBitSet, k_bGivePassengerGun )
				GIVE_WEAPON_TO_PED(TAS_Local.piAmbush[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				SET_BIT( iLocalBitSet, k_bGivePassengerGun )
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(iMainSceneId2) >= TAXI_BIKER_C_GUN_TIME
			AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
			AND NOT IS_BIT_SET( iLocalBitSet, k_bGiveBikerCGun )
				GIVE_WEAPON_TO_PED(TAS_Local.piAmbush[2], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
				SET_BIT( iLocalBitSet, k_bGiveBikerCGun )
			ENDIF
			
			IF NOT IS_BIT_SET( iLocalBitSet, k_bSideTaskLoopB )
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId2) AND GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId2) = 1.0)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId2)
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
					AND NOT IS_PED_INJURED(piGangsterGirls[0])
					AND NOT IS_PED_INJURED(piGangsterGirls[1])
						iSideSceneId3 = CREATE_SYNCHRONIZED_SCENE(scenePositionSide, sceneRotationSide)
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - for the loopB iSideSceneId3 = ", iSideSceneId3)
						TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[3], iSideSceneId3, "oddjobs@taxi@argument", "hooker_loop_b_biker_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (piGangsterGirls[0], iSideSceneId3, "oddjobs@taxi@argument", "hooker_loop_b_hooker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						TASK_SYNCHRONIZED_SCENE (piGangsterGirls[1], iSideSceneId3, "oddjobs@taxi@argument", "hooker_loop_b_hooker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
						SET_SYNCHRONIZED_SCENE_LOOPED(iSideSceneId3,TRUE)
						SET_BIT( iLocalBitSet, k_bSideTaskLoopB )
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( iLocalBitSet, k_bPassengerTalking)
				sCurrentLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				
				IF ARE_STRINGS_EQUAL(sStandoffTrigger1, sCurrentLine)
				OR ARE_STRINGS_EQUAL(sStandoffTrigger2, sCurrentLine)
					
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
					AND NOT IS_PED_INJURED(piGangsterGirls[0])
					AND NOT IS_PED_INJURED(piGangsterGirls[1])
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - for the outro iSideSceneId3 = ", iSideSceneId3)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId3)
							iSideSceneId4 = CREATE_SYNCHRONIZED_SCENE(scenePositionSide, sceneRotationSide)
							TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[3], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_biker_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (piGangsterGirls[0], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_hooker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (piGangsterGirls[1], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_hooker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSideSceneId4, TRUE)
							iGiveGunTimer = GET_GAME_TIMER()
							m_AmbushState = AMB_STANDOFF
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 0")
						ELSE
							iSideSceneId4 = CREATE_SYNCHRONIZED_SCENE(scenePositionSide, sceneRotationSide)
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - had to re-validate side scene")
							TASK_SYNCHRONIZED_SCENE (TAS_Local.piAmbush[3], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_biker_d", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (piGangsterGirls[0], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_hooker_a", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							TASK_SYNCHRONIZED_SCENE (piGangsterGirls[1], iSideSceneId4, "oddjobs@taxi@argument", "hooker_outro_hooker_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSideSceneId4, TRUE)
							iGiveGunTimer = GET_GAME_TIMER()
							m_AmbushState = AMB_STANDOFF
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 0")
						ENDIF
					ENDIF
					
					
				ENDIF
				
			ENDIF
		BREAK
		
		CASE AMB_STANDOFF
			
			SWITCH iStandoffMarker
				// first let's task the farmer dude to aim at the drug boss
				CASE 0
					
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) >= TAXI_BIKER_D_GUN_TIME
					IF (GET_GAME_TIMER() - iGiveGunTimer) > 850
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
					AND NOT IS_BIT_SET( iLocalBitSet, k_bGiveBikerDGun )
						GIVE_WEAPON_TO_PED(TAS_Local.piAmbush[3], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - gun given to biker d in case 0")
						SET_BIT( iLocalBitSet, k_bGiveBikerDGun )
//					ELSE
//						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId)
//							CDEBUG1LN(DEBUG_OJ_TAXI," iSideSceneId not running")
//						ELIF GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) < TAXI_BIKER_D_GUN_TIME
//							CDEBUG1LN(DEBUG_OJ_TAXI," iSideSceneId hasn't hit gun point")
//							CDEBUG1LN(DEBUG_OJ_TAXI," GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) = ", GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId))
//						ENDIF
					ENDIF
					
					
					
//					sCurrentLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
//					IF ARE_STRINGS_EQUAL("txm12_deal1_8", sCurrentLine)
//					AND NOT IS_BIT_SET( iLocalBitSet, k_bPauseConvo )
//						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
//						PRINT_NOW("TAXI_OBJ_GYB", 3000, 0)
//						iAmbushTimer = GET_GAME_TIMER()
//						SET_BIT( iLocalBitSet, k_bPauseConvo )
//					ENDIF
					
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
					AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						
						SET_PED_RESET_FLAG(TAS_Local.piAmbush[0], PRF_InstantBlendToAim, TRUE)
						SET_PED_RESET_FLAG(TAS_Local.piAmbush[1], PRF_InstantBlendToAim, TRUE)
						SET_PED_RESET_FLAG(TAS_Local.piAmbush[2], PRF_InstantBlendToAim, TRUE)
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_InstantBlendToAim, TRUE)
						
						IF (IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId2) AND GET_SYNCHRONIZED_SCENE_PHASE(iMainSceneId2) = 1.0)
						OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iMainSceneId2)
						OR (GET_GAME_TIMER() - iSyncFailSafeTimer) > 27000
							
							IF (GET_GAME_TIMER() - iSyncFailSafeTimer) > 27000
								CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - Sync scene hit the fail safe.  this is bad but it should manage.")
							ENDIF
							
							bCutToGamePlay = TRUE
							
							TASK_AIM_GUN_AT_ENTITY(TAS_Local.piAmbush[0], myTaxiData.piTaxiPassenger, -1)//, TRUE)
							TASK_AIM_GUN_AT_ENTITY(TAS_Local.piAmbush[1], myTaxiData.piTaxiPassenger, -1)//, TRUE)
							TASK_AIM_GUN_AT_ENTITY(TAS_Local.piAmbush[2], myTaxiData.piTaxiPassenger, -1)//, TRUE)
							TASK_AIM_GUN_AT_ENTITY(myTaxiData.piTaxiPassenger, TAS_Local.piAmbush[1], -1)//, TRUE)
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - aim task given")
							
							iAmbushTimer = 0
							
							iStandoffMarker++
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 1")
						ENDIF
					ENDIF
					
				BREAK
			
				// second let's task the two gangster dudes to aim at the farmer, shortly after
				CASE 1
					
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) >= TAXI_BIKER_D_GUN_TIME
					IF (GET_GAME_TIMER() - iGiveGunTimer) > 850
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
					AND NOT IS_BIT_SET( iLocalBitSet, k_bGiveBikerDGun )
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - gun given to biker d in case 1")
						GIVE_WEAPON_TO_PED(TAS_Local.piAmbush[3], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
						SET_BIT( iLocalBitSet, k_bGiveBikerDGun )
//					ELSE
//						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId)
//							CDEBUG1LN(DEBUG_OJ_TAXI," iSideSceneId not running")
//						ELIF GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) < TAXI_BIKER_D_GUN_TIME
//							CDEBUG1LN(DEBUG_OJ_TAXI," iSideSceneId hasn't hit gun point")
//							CDEBUG1LN(DEBUG_OJ_TAXI," GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId) = ", GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId))
//						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
						SET_PED_RESET_FLAG(TAS_Local.piAmbush[3], PRF_InstantBlendToAim, TRUE)
					ENDIF
					
					IF NOT IS_BIT_SET( iLocalBitSet, k_bSideTaskOutro ) 
						IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSideSceneId4)
						OR GET_SYNCHRONIZED_SCENE_PHASE(iSideSceneId4) = 1.0
							IF NOT IS_PED_INJURED(TAS_Local.piAmbush[3])
							AND NOT IS_PED_INJURED(piGangsterGirls[0])
							AND NOT IS_PED_INJURED(piGangsterGirls[1])
							AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
//								CLEAR_PED_TASKS (piGangsterGirls[0])
//								CLEAR_PED_TASKS (piGangsterGirls[1])
//								CLEAR_PED_TASKS (TAS_Local.piAmbush[3])
//								
								CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - Flee task given")
								TASK_FOLLOW_NAV_MESH_TO_COORD(piGangsterGirls[0], <<299.3585, -3204.9048, 4.7214>>, PEDMOVE_SPRINT)
								TASK_FOLLOW_NAV_MESH_TO_COORD(piGangsterGirls[1], <<299.3585, -3204.9048, 4.7214>>, PEDMOVE_SPRINT)
								FORCE_PED_MOTION_STATE(piGangsterGirls[0], MS_ON_FOOT_SPRINT)
								FORCE_PED_MOTION_STATE(piGangsterGirls[1], MS_ON_FOOT_SPRINT)
								TASK_AIM_GUN_AT_ENTITY(TAS_Local.piAmbush[3], myTaxiData.piTaxiPassenger, -1, TRUE)
								
								SET_BIT( iLocalBitSet, k_bSideTaskOutro )
							ENDIF
						ENDIF
					ENDIF
					
					sCurrentLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
					IF ARE_STRINGS_EQUAL("txm12_deal1_7", sCurrentLine)
					AND iAmbushTimer = 0
						
						iAmbushTimer = GET_GAME_TIMER()
						
					ELIF (iAmbushTimer > 0 AND (GET_GAME_TIMER() - iAmbushTimer) > 1500 )
					AND NOT IS_BIT_SET( iLocalBitSet, k_bPauseConvo )
						//PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						KILL_ANY_CONVERSATION()
						PRINT_NOW("TAXI_OBJ_GYB", 4000, 0)
						BLIP_ALL_PEDS( TAS_Local.piAmbush, TAS_Local.blipPedAmbush )
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - kill the cutscene cam for the objective prompt")
						bEnableBonusCam = FALSE
						
						DESTROY_CAM(myTaxiData.camTaxi)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						
						iAmbushTimer = GET_GAME_TIMER()
						iStandoffMarker = 2
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 2")
						
						SET_BIT( iLocalBitSet, k_bPauseConvo )
					ELIF (iAmbushTimer > 0 AND (GET_GAME_TIMER() - iAmbushTimer) > 1200 )
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						AND IS_CAM_ACTIVE(myTaxiData.camTaxi)
							IF NOT bPulseTriggered
								CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - triggered pulse")
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								bPulseTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				
				CASE 2
					
					IF GET_GAME_TIMER() - iAmbushTimer > 4000
					AND IS_BIT_SET( iLocalBitSet, k_bPauseConvo )
						//PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						
						PLAY_SINGLE_LINE_FROM_CONVERSATION(myTaxiData.tTaxiOJ_Convo, myTaxiData.sTaxiOJ_DXSubtitleGroupID, "txm12_deal1", "txm12_deal1_9",CONV_PRIORITY_VERY_HIGH)
						
						iAmbushTimer = GET_GAME_TIMER()
						CLEAR_BIT( iLocalBitSet, k_bPauseConvo )
					ENDIF
					
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
					AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
					//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_BIT_SET( iLocalBitSet, k_bPauseConvo )
					AND GET_GAME_TIMER() - iAmbushTimer > 2000
						//Setting their health here
						SET_ENTITY_HEALTH(TAS_Local.piAmbush[0] , 115)
						SET_ENTITY_HEALTH(TAS_Local.piAmbush[2] , 115)
						
						iAmbushTimer = GET_GAME_TIMER()
						iStandoffMarker = 8
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 8")
						
					ENDIF
					
				BREAK
			
				// fourth can we get a shaky gun effect or anim here?  maybe just aim shakingly
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GYB,TRUE)
						
						TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
						
						iAmbushTimer = GET_GAME_TIMER()
						iStandoffMarker++
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - countdown")
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_MESSAGE_BEING_DISPLAYED()//IS_ANY_TEXT_BEING_DISPLAYED(locatesData)
						
						TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ENEMY_EGG_ON_2, TRUE)
						iStandoffMarker++
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - 5")
					ENDIF
				BREAK
				CASE 5
				CASE 6
				CASE 7
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE) > 2.0
						
						TAXI_RESET_TIMERS(myTaxiData,  TT_CUTSCENE)
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ENEMY_EGG_ON_2,TRUE)
						iStandoffMarker++
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - ", iStandoffMarker)
					ENDIF
				BREAK
			
				// fifth boss warns the farmer to run along.  he'll count to three before shots are fired.  farmer will not shoot
				CASE 8
					// when the dialogue gets written, change this condition
//					IF (GET_TAXI_SPEECH_INDEX(myTaxiData) = TAXI_DI_ENEMY_EGG_ON_2)
//					AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_PED_INJURED(TAS_Local.piAmbush[0])
						AND NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
						AND NOT IS_PED_INJURED(TAS_Local.piAmbush[2])
							
//							SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[1], relPursuers )
							
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(TAS_Local.piAmbush[1], TRUE)
//							TASK_STAND_STILL(TAS_Local.piAmbush[1], 1500)
							
							CLEAR_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relPursuers, myTaxiData.relPassenger)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, myTaxiData.relPassenger)
							
							SET_PED_ACCURACY(TAS_Local.piAmbush[0], 100)
							SET_PED_ACCURACY(TAS_Local.piAmbush[2], 100)
							
							TASK_SHOOT_AT_ENTITY(TAS_Local.piAmbush[0], myTaxiData.piTaxiPassenger, 5000, FIRING_TYPE_CONTINUOUS)
							TASK_SHOOT_AT_ENTITY(TAS_Local.piAmbush[1], myTaxiData.piTaxiPassenger, 5000, FIRING_TYPE_RANDOM_BURSTS)
							TASK_SHOOT_AT_ENTITY(TAS_Local.piAmbush[2], myTaxiData.piTaxiPassenger, 5000, FIRING_TYPE_CONTINUOUS)
							
							//TASK_COMBAT_PED(TAS_Local.piAmbush[0], myTaxiData.piTaxiPassenger)
							//TASK_COMBAT_PED(TAS_Local.piAmbush[2], myTaxiData.piTaxiPassenger)
							
//							IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
//								//CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
//								SET_ENTITY_HEALTH(myTaxiData.piTaxiPassenger, 110)
//							ENDIF
							
							iAmbushTimer = GET_GAME_TIMER()
							iStandoffMarker++
							CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_STANDOFF - ", iStandoffMarker)
						ENDIF
//					ENDIF
				BREAK
				
				CASE 9
					IF (GET_GAME_TIMER() - iAmbushTimer) > 5000	//account for the difference in god text time
						m_AmbushState = AMB_COMBAT
						CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_COMBAT")
					ENDIF
				BREAK
			ENDSWITCH
			
			// look into the gangsters shooting at the player if he gets close
		BREAK
		
		CASE AMB_COMBAT
			
			IF NOT myTaxiData.bIsTaxiOJInCombat
				myTaxiData.bIsTaxiOJInCombat = TRUE
			ENDIF
			
			// setting ignore bits since they're in combat.  these will be reset when the fight is over.  
			// either way, the mission should fail if the passenger dies
			SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_Aiming))
			SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_Attacked))
			SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_MinorAttacked))
			
			KILL_ANY_CONVERSATION()
			
			CREATE_AMBUSH_SECOND_WAVE()
			
//			IF NOT IS_PED_INJURED(TAS_Local.piAmbush[1])
//				
//				SET_PED_RELATIONSHIP_GROUP_HASH( TAS_Local.piAmbush[1], relPursuers )
//			ENDIF
			
			SET_PASSENGER_IN_COMBAT()
			TASK_ALL_PEDS_TO_FIGHT( TAS_Local.piAmbush )
			BLIP_ALL_PEDS( TAS_Local.piAmbush, TAS_Local.blipPedAmbush )
			TASK_PEDS_TO_FLEE( piGangsterGirls )
			
			iAmbushTimer = GET_GAME_TIMER()
			m_AmbushState = AMB_SECOND_WAVE
			CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - moving on to state AMB_WAIT_FOR_DEATH")
		BREAK
		
		CASE AMB_SECOND_WAVE
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					KILL_ALL_TAXI_OJ_ENEMIES(TAS_Local.piAmbush)
				ENDIF
			#ENDIF
				
			IF iGangsterKilledCount >= 2
			AND NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_FOLLOW)
			
				TAXI_START_TIMER(myTaxiData, TT_FOLLOW)
				
			ELIF IS_TAXI_TIMER_STARTED(myTaxiData, TT_FOLLOW)
			AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) > 3.0
				
//				SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_ENEMY_FIRES, TRUE, TRUE)
				
				TASK_SECOND_WAVE_CAR_RECS()
				BLIP_SECOND_WAVE_CARS()
				
				iAmbushTimer = GET_GAME_TIMER()
				m_AmbushState = AMB_WAIT_FOR_DEATH
			ELSE
				iGangsterKilledCount = 0
			ENDIF
			
			UPDATE_AMBUSH_PEDS( TAS_Local.piAmbush, TAS_Local.blipPedAmbush, TAS_Local2.piAmbush, TAS_Local2.blipPedAmbush )
		BREAK
		
		CASE AMB_WAIT_FOR_DEATH
		
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					KILL_ALL_TAXI_OJ_ENEMIES(TAS_Local2.piAmbush)
					KILL_ALL_TAXI_OJ_ENEMIES(TAS_Local.piAmbush)
				ENDIF
			#ENDIF
			
			IF IS_PED_INJURED(TAS_Local2.piAmbush[0])
//			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(TAS_Local2.piAmbush[0], PLAYER_PED_ID())
//			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(TAS_Local2.piAmbush[0], myTaxiData.piTaxiPassenger))
			AND IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[0])
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TAS_Local2.viGangCar[0])
				CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - playback for first vehicle stopped")
				STOP_PLAYBACK_RECORDED_VEHICLE(TAS_Local2.viGangCar[0])
			ENDIF
			
			IF (IS_PED_INJURED(TAS_Local2.piAmbush[2])
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(TAS_Local2.piAmbush[2], PLAYER_PED_ID())
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(TAS_Local2.piAmbush[2], myTaxiData.piTaxiPassenger))
			AND IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[1])
			AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TAS_Local2.viGangCar[1])
				CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - playback for second vehicle stopped")
				STOP_PLAYBACK_RECORDED_VEHICLE(TAS_Local2.viGangCar[1])
			ENDIF
			
			IF DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[0])
			AND NOT IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[0])
				REMOVE_BLIP(TAS_Local2.blipVehAmbush[0])
			ENDIF
			
			IF DOES_BLIP_EXIST(TAS_Local2.blipVehAmbush[1])
			AND NOT IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[1])
				REMOVE_BLIP(TAS_Local2.blipVehAmbush[1])
			ENDIF
			
			IF NOT bAmbushBlipTransition
			AND ( (IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[0]) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TAS_Local2.viGangCar[0]))
				OR (IS_VEHICLE_DRIVEABLE(TAS_Local2.viGangCar[1]) AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(TAS_Local2.viGangCar[1])) )
				IF IS_ANY_AMBUSH_PED_OUT_OF_VEHICLE(TAS_Local2.piAmbush)
					BLIP_ALL_PEDS( TAS_Local2.piAmbush, TAS_Local2.blipPedAmbush )
					REMOVE_SECOND_WAVE_CAR_BLIPS()
					
					bAmbushBlipTransition = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET( iLocalBitSet, k_bPassengerVulnerable )
				IF GET_GAME_TIMER() - iAmbushTimer > 5000
					TASK_ALL_PEDS_TO_FIGHT( TAS_Local2.piAmbush )
					
					LET_PEDS_HIT_THINGS( TAS_Local.piAmbush )
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, myTaxiData.relPassenger)
					SET_BIT( iLocalBitSet, k_bPassengerVulnerable )
					CDEBUG1LN(DEBUG_OJ_TAXI,"PROCESS_AMBUSH - It's been 5 seconds, now setting remaining peds to target passenger")
				ENDIF
			
			ELSE
				// TAS_Local2.piAmbush[0] seems to be losing his combat task
				// it looks like he loses his task since he's saying a dialogue line.
				// TODO: find a more elegant way to do this
				INT i
				REPEAT COUNT_OF(TAS_Local2.piAmbush) i
					IF NOT IS_PED_INJURED(TAS_Local2.piAmbush[i])
						IF GET_SCRIPT_TASK_STATUS(TAS_Local2.piAmbush[i], SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(TAS_Local2.piAmbush[i], SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) <> WAITING_TO_START_TASK
							
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(TAS_Local2.piAmbush[i], 1000.0)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAS_Local2.piAmbush[0] NEEDED TO BE RE-TASKED")
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				IF bSayShout
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_SHOOT, TRUE, FALSE, TRUE)
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"not saying a shout because conversation is playing")
					ENDIF
					bSayShout = FALSE
				ENDIF
			ENDIF
			
			RETURN UPDATE_AMBUSH_PEDS( TAS_Local.piAmbush, TAS_Local.blipPedAmbush, TAS_Local2.piAmbush, TAS_Local2.blipPedAmbush )
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_DYNAMIC_TAXI_REQUESTS()
	SWITCH eTaxiRequestState
		CASE TAXIDR_REQUEST
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff) < 300
				REQUEST_TAXI_ODDJOB_GYB_STREAMS_STAGE_AMBUSH()
				eTaxiRequestState = TAXIDR_WAIT_STREAM
			ENDIF
		BREAK
		CASE TAXIDR_WAIT_STREAM
			IF HAVE_TAXI_OJ_FC_STAGE_ABMUSH_ASSETS_LOADED()
				eTaxiRequestState = TAXIDR_CREATE
			ENDIF
		BREAK
		CASE TAXIDR_CREATE
			IF NOT bAmbushSetup
				CREATE_AMBUSH_CARS()
				INITIALIZE_AMBUSH_CARS()
				
				CREATE_AMBUSH_PEDS()
				INITIALIZE_AMBUSH_PEDS()
				
				TASK_ALL_INITIAL_ANIMS()
				
				CREATE_AMBUSH_OBJECTS()
				
				eTaxiRequestState = TAXIDR_CLEANUP
				
				bAmbushSetup = TRUE
			ENDIF
		BREAK
		CASE TAXIDR_CLEANUP	
			IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff) > 350
				RELEASE_TAXI_ODDJOB_GYB_STREAMS_STAGE_AMBUSH()
				eTaxiRequestState = TAXIDR_REQUEST
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_GYB_LINES()

	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	
	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
		
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_GYB_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been  assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 1")
					ENDIF
				
					//Start the mission timer here
					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
				ENDIF
//				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
//				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_VARIABLE_BANTER
//				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
//					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_VARIABLE_BANTER,TRUE)
//					IF g_bDebug
//						SCRIPT_ASSERT("Triggering Banter 1")
//					ENDIF
//				
//					//Start the mission timer here
//					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
//				ENDIF
			BREAK
			
			CASE 2
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 2.0
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 2")
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(2.0,7.0)
					//Set Radio Station Check
					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
						tTaxiOJ_DQ_Data.iCurrentDQLine++
						IF g_bDebug
							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)

ENDPROC

PROC TAXI_OJ_GYB_SET_TIP_AND_EXCITEMENT_TO_CHECK()
	
	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	
	//EXCITEMENT BITS
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
	
ENDPROC

PROC Main_Taxi_OJ_GotYourBack()
	
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF TAXI_HANDLE_FAIL(myTaxiData)
			Script_Failed()
		ENDIF
	
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState >= TRS_DRIVING_PASSENGER
			
			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
			TAXI_HANDLE_CINEMATIC_CAM()
		ENDIF
		
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
		IF myTaxiData.tTaxiOJ_RideState < TRS_DROPPING_OFF
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		ENDIF
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		
		//Find Dropoff Point
		IF myTaxiData.tTaxiOJ_RideState > TRS_MANAGE_PICKUP
		AND NOT IS_BIT_SET( iLocalBitSet, k_bDropOffFound )
			myTaxiData.vTaxiOJDropoff = << 189.7825, -3325.6838, 4.6697 >>
			SET_BIT( iLocalBitSet, k_bDropOffFound )
		ENDIF
	
	
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_GYB_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF			

		ENDIF
		
			
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF			
		
		//FAIL CASES
		//1. If Walter dies fail mission
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
			IF IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)	
				IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_GENERIC)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi passenger died, starting generic timer")
					TAXI_START_TIMER(myTaxiData, TT_GENERIC)
				ENDIF
				
				IF myTaxiData.tTaxiOJ_RideState >= TRS_DROPPING_OFF
				OR m_AmbushState >= AMB_TALKING
					bTaxiDelayFail = TRUE
				ENDIF
				TASK_ALL_PEDS_TO_FIGHT( TAS_Local.piAmbush )
				TASK_ALL_PEDS_TO_FIGHT( TAS_Local2.piAmbush )
				TASK_PEDS_TO_FLEE( piGangsterGirls )
				
				TAXI_SET_FAIL(myTaxiData,"TX12_GYB - Walter has been killed", TFS_PASSENGER_DIED)
			ENDIF
		ENDIF
		//================================================================================================
	
	
		SWITCH myTaxiData.tTaxiOJ_RideState
			
			//PRESTREAM and Check that the player and taxi are good to go
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_GYB_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiWalter",mPassengerModel,202.0,35.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
				
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_FC_STAGE_01_ASSETS_LOADED()

					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					//Init Bonus----------------------------------------
					TAXI_INITIALIZE_BONUS_FIELD(bonusFieldGotYourBack[TGYB_BONUS_TERMINATOR], "TAXI_SC_BN_12", TAXI_CONST_BONUS_CASH_TERMINATOR)			//Escaped Bonus

					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldGotYourBack)
					
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					//SET REACT BITS
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_FINDING_LOCATION)	
				ENDIF
	
			BREAK
			
			CASE TRS_FINDING_LOCATION
				myTaxiData.vTaxiOJPickup  = vPassengerPt	
				
				myTaxiData.vTaxiOJDropoff = TAS_Local.DropOffPos
				
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)	
			BREAK
			
			CASE TRS_SPAWNING
//				IF TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiWalter",mPassengerModel,202.0,35.0)
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
				
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_LEG, 0,2)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_HEAD, 0,2)
					ENDIF
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					//Set relationships here
					SET_RELATIONSHIP_BETWEEN_GROUPS( ACQUAINTANCE_TYPE_PED_LIKE, relPursuers, myTaxiData.relPassenger)
					SET_RELATIONSHIP_BETWEEN_GROUPS( ACQUAINTANCE_TYPE_PED_LIKE, myTaxiData.relPassenger, relPursuers)
					
					SET_PED_CAN_BE_TARGETTED(myTaxiData.piTaxiPassenger, FALSE)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			CASE TRS_MANAGE_PICKUP

				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
				
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)	
				ENDIF
			BREAK
			
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_TIME)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK
			//This state simply waits for the obj to be given before drawing the countdown timer.
			CASE TRS_WAIT_FOR_TIME
				IF IS_BIT_SET( iLocalBitSet, k_bDropOffFound )
					
					TAXI_OJ_GYB_SET_TIP_AND_EXCITEMENT_TO_CHECK()
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)	
				ENDIF
			BREAK
							
			CASE TRS_DRIVING_PASSENGER
				UPDATE_DYNAMIC_TAXI_REQUESTS()
				
				IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff)) < TAXI_CONST_GYB_DIST_TO_HAND_GUN
				AND NOT myTaxiData.bIsCurrentlyWanted
				AND NOT bWeaponGivenToPlayer
					
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vDocksMin, vDocksMax, FALSE)
					SET_PED_NON_CREATION_AREA(vDocksMin, vDocksMax)
					tempScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vDocksMin, vDocksMax)
					CLEAR_AREA_OF_PEDS(<< 195.16, -3282.54, 4.79 >>, 25.0)
					
					
					
					//No need to comment on music if approaching destination
					TAXI_RADIO_STATION_TURN_OFF(myTaxiData)
					
					CLEAR_DIALOGUE_QUEUE(tDialogueLine)
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,3)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_CC_BANTER, TRUE)
					GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPlayer, WEAPONTYPE_COMBATPISTOL, 200, TRUE)
					
					bWeaponGivenToPlayer = TRUE
				ENDIF
				
				IF TAXI_HANDLE_DRIVING(myTaxiData,/*tTaxiOJ_DQ_Data,*/16)
				
					REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					
					CDEBUG1LN(DEBUG_OJ_TAXI," [ AGGRO ]	Turn off aggro checks once arrive at the dropoff")
					SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_ShotNear))
					SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_HeardShot))
					
					SET_WANTED_LEVEL_MULTIPLIER(0.1)
					SETTIMERA(0)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)	
					
				ENDIF
				
				IF (( GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiData.vTaxiOJDropoff) < 10 ) 
				AND ( GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi, TAS_Local.piAmbush[1]) < 10 ))
					IF IS_TAXI_FULLY_STOPPED(myTaxiData, TRUE)
						
						REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
						//Change exception bits
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_TAXI_NOT_DRIVEABLE),FALSE)
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_TAXI_STOPPED),FALSE)
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_TAXI_FLIPPED),FALSE)
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_TAXI_WATER),FALSE)
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_PLAYER_WANTED),FALSE)
						TAXI_EXCEPTION_SET_ENABLE_BIT(ENUM_TO_INT(TAXI_GENERIC_EXCEPTION_PASSENGER_NOT_OK),TRUE)
						
						SET_WANTED_LEVEL_MULTIPLIER(0.1)
						SETTIMERA(0)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)	
					ENDIF
				ENDIF
				
				IF NOT bTaxiEarlyAggro
					//TASK_ALL_INITIAL_ANIMS()
				ENDIF
				
				//checking for premature aggro
				IF NOT bTaxiEarlyAggro
					IF bAmbushSetup
						IF (IS_PED_INJURED(TAS_Local.piAmbush[0]) 
							OR IS_PED_INJURED(TAS_Local.piAmbush[1]) 
							OR IS_PED_INJURED(TAS_Local.piAmbush[2]) 
							OR IS_PED_INJURED(TAS_Local.piAmbush[3]) 
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[0]))
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[1])) 
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(TAS_Local.piAmbush[2]))
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[0])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[0], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[1])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[1], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
							OR ((NOT IS_ENTITY_DEAD(TAS_Local.piAmbush[2])) AND HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(TAS_Local.piAmbush[2], WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON))
							OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), TAS_Local.piAmbush[1]) < 6.0)	)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(TAS_Local.piAmbush[0], relPursuers)
							
							TASK_ALL_PEDS_TO_FIGHT( TAS_Local.piAmbush )
							TASK_PEDS_TO_FLEE( piGangsterGirls )
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"Early aggro triggered")
							
							TAXI_SET_FAIL(myTaxiData,"Aggro Heard Shot", TFS_ABANDONED_PASSENGER)
							
							bTaxiEarlyAggro = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE TRS_DROPPING_OFF
			
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < 250.0
					IF PROCESS_AMBUSH()
						
						CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_Aiming))
						CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_Attacked))
						CLEAR_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, (EAggro_MinorAttacked))
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_RETURN_TO_PED,TRUE)
						
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger) AND NOT IS_PED_INJURED(myTaxiData.piTaxiPlayer)
							CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
							TASK_GO_TO_ENTITY(myTaxiData.piTaxiPassenger,myTaxiData.piTaxiPlayer, DEFAULT_TIME_BEFORE_WARP, 6.0)
						ENDIF
						
						SETTIMERA(0)
						
						myTaxiData.bIsTaxiOJInCombat = FALSE
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_TIE_UP_LOSE_ENDS)
					
					ENDIF
				ELSE
					TAXI_SET_FAIL(myTaxiData, "Player abandoned passenger", TFS_ABANDONED_PASSENGER)
				ENDIF
			BREAK
			
			CASE TRS_TIE_UP_LOSE_ENDS//------------------------------------------------------------------------
				//IF IS_ENTITY_AT_ENTITY(myTaxiData.piTaxiPassenger,myTaxiData.piTaxiPlayer, <<7.0,7.0,20.0>>)
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPlayer) < 7.0
				AND TIMERA() > 3000	
					
					TASK_LOOK_AT_ENTITY(myTaxiData.piTaxiPassenger, myTaxiData.piTaxiPlayer, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					
					CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
					OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
						
						
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.piTaxiPlayer,-1)
					
						
					CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiAIAction)
					
					myTaxiData.iTaxiOJ_CashTip = 50
					
					//CASH$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
					CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"iPlayerKilledCount = ", iPlayerKilledCount)
				
					IF iPlayerKilledCount = TAXI_CONST_NUM_TOTAL_ENEMIES
						TAXI_SET_BONUS_AWARD(myTaxiData, ENUM_TO_INT(TGYB_BONUS_TERMINATOR))
					ENDIF
					
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
				
				ELIF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > 100.0
					TAXI_SET_FAIL(myTaxiData, "Player abandoned passenger", TFS_ABANDONED_PASSENGER)
				ENDIF
			BREAK
			
			
			CASE TRS_REGULAR_PAYMENT//------------------------------------------------------------------------
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CUTSCENE)  > 2.0
				
					TAXI_CANCEL_TIMERS(myTaxiData, TT_CUTSCENE)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
					
					IF CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, vIncidentPoint, 1, 0.0, ambulanceIndex)
						CDEBUG1LN(DEBUG_OJ_TAXI,"ambulance incident succesfully created")
						SET_IDEAL_SPAWN_DISTANCE_FOR_INCIDENT(ambulanceIndex, 200)
						bDispatchSuccessful = TRUE
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"ambulance incident NOT created")
					ENDIF
					
					IF CREATE_INCIDENT(DT_POLICE_VEHICLE_REQUEST, vIncidentPoint, 3, 0.0, policeIndex)
						CDEBUG1LN(DEBUG_OJ_TAXI,"police incident succesfully created")
						//SET_IDEAL_SPAWN_DISTANCE_FOR_INCIDENT(policeIndex, 200)
						bDispatchSuccessful = TRUE
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"police incident NOT created")
					ENDIF
				ENDIF
			
			BREAK
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					
					IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPassenger)
//						CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
//						OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)
//							
//							TASK_USE_MOBILE_PHONE_TIMED(NULL, 10000)
//							TASK_WANDER_IN_AREA(NULL, << 206.0538, -3326.4331, 4.7915 >>, 20.0)
//							
//						CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
//						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiAIAction)
						
						//TASK_WANDER_IN_AREA(myTaxiData.piTaxiPassenger, << 206.0538, -3326.4331, 4.7915 >>, 20.0)
						
						IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						
							
							CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
							CLEAR_SEQUENCE_TASK(seqIndexTaxiAIAction)
							OPEN_SEQUENCE_TASK(seqIndexTaxiAIAction)								
								TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 500.0, 30000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 261.09155, -3089.06152, 4.79249 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 304.63266, -2965.92236, 5.00012 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 309.89066, -2870.47290, 5.15704 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
								TASK_WANDER_STANDARD(NULL)
							CLOSE_SEQUENCE_TASK(seqIndexTaxiAIAction)
							TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiAIAction)							
							
							SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
						ENDIF
					ENDIF
					
					TAXI_MISSION_END(TRUE, myTaxiData, FALSE)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Starting TIMERA to give the player time leave before activating wanted level")
					SETTIMERA(0)
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF
			
			BREAK
			
			CASE TRS_CLEANUP
				// at this point the passenger tells the player to clear out because he's coming clean to the cops
				//  player then has 10 seconds to leave the area, else he gets a 1 star wanted level
				IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), << 213.4579, -3329.4707, 4.7971 >>) > 40)
					CDEBUG1LN(DEBUG_OJ_TAXI,"GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), myTaxiData.vTaxiOJDropoff) > 40, cleaning up")
					Script_Cleanup()
					
				ELSE
					
					IF bDispatchSuccessful
						viAmbulance = GET_RANDOM_VEHICLE_IN_SPHERE(vIncidentPoint, 40, AMBULANCE, 0)
						IF IS_VEHICLE_DRIVEABLE(viAmbulance)
							CDEBUG1LN(DEBUG_OJ_TAXI,"ambulance vehicle found")
							bAmbulanceFound = TRUE
						ENDIF
						
						viAmbulance = GET_RANDOM_VEHICLE_IN_SPHERE(vIncidentPoint, 40, POLICE, 0)
						IF IS_VEHICLE_DRIVEABLE(viAmbulance)
							CDEBUG1LN(DEBUG_OJ_TAXI,"police vehicle found")
							bAmbulanceFound = TRUE
						ENDIF
						
						piMedic = GET_RANDOM_PED_AT_COORD(vIncidentPoint, <<10,10,5>>)
						IF NOT IS_PED_INJURED(piMedic)
							CDEBUG1LN(DEBUG_OJ_TAXI,"random ped found")
							bAmbulanceFound = TRUE
						ENDIF
						
						IF bAmbulanceFound
						OR TIMERA() > 30000
							CDEBUG1LN(DEBUG_OJ_TAXI,"TIMERA() > 30000, or dispatch found, then cleaning up")
							
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							Script_Cleanup()
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"dispatch was never created, cleaning up")
						
						Script_Cleanup()
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	myTaxiData.iTaxiOJ_GetRunModCount = 1
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD
	
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF
		
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF		
		
		PROCESS_WIDGETS()

	#ENDIF
	//END DEBUG----------------------------------------------------

		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			Main_Taxi_OJ_GotYourBack()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
