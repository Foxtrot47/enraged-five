///    Taxi_AI_Car_Lib.sch
///    Author: John R. Diaz
///    Purpose - Keep all functions/procs that deal with following/chasing an AI car while on a taxi mission

USING "Taxi_AI_Car.sch"
USING "Taxi_Support_lib.sch"
USING "chase_hint_cam.sch"

FUNC MODEL_NAMES PICK_RANDOM_MODEL_FROM_ARRAY(MODEL_NAMES &array[])
	MODEL_NAMES returnModel
	INT tempInt
	
	tempInt = GET_RANDOM_INT_IN_RANGE(0, 65535) % COUNT_OF(array)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Chose model number: ","",tempInt)
	
	returnModel = array[tempInt]
	
	RETURN returnModel
ENDFUNC

FUNC MODEL_NAMES CHOOSE_RANDOM_MODEL_FROM_LIST(MODEL_NAMES name1, MODEL_NAMES name2, MODEL_NAMES name3, MODEL_NAMES name4, MODEL_NAMES name5)
	MODEL_NAMES chosenModel, randModel[5]
	INT randInt
	
	randModel[0] = name1
	randModel[1] = name2
	randModel[2] = name3
	randModel[3] = name4
	randModel[4] = name5
	
	randInt = GET_RANDOM_INT_IN_RANGE() % COUNT_OF(randModel)
	
	chosenModel = randModel[randInt]
	
	RETURN chosenModel
ENDFUNC 

//add function for choosing baddie and vehicle model
// add them to streaming.

PROC INITIALIZE_ESCAPEE_STRUCT(TaxiStruct &myTaxiData,escapeStruct &theEscapestruct)
	
	INT iNumTimesThisTaxiJobWasRun = g_savedGlobals.sTaxiData.iTaxiOJ_NumRuns[myTaxiData.tTaxiOJ_MissionType]
	
	//Set the type
	theEscapestruct.bIsFollowMode = TRUE
	theEscapeStruct.stn_CurrentTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
	
	//Healthfactor
	theEscapestruct.fHealthFactor = 0.7 - (0.6 * iNumTimesThisTaxiJobWasRun)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Set fire health to: ","", theEscapestruct.fHealthFactor)
	
	
	//This number determines what health percentage the driver will bail at.
	theEscapeStruct.fBailFactor = 0.8 - (0.6 * iNumTimesThisTaxiJobWasRun)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Set bail health to: ","", theEscapestruct.fBailFactor)

	//This is the base cruise speed for the fleeing car.
	theEscapeStruct.fCruiseSpeed = 24.0 + (10 * iNumTimesThisTaxiJobWasRun)
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI - Set cruise speed to: ","", theEscapestruct.fCruiseSpeed)
ENDPROC

FUNC BOOL ESCAPEE_SEARCH_FOR_NEARBY_UNSEEN_ROAD(Taxistruct &myTaxiData, VECTOR &PassedVector, FLOAT &PassedHeading, VECTOR vBaseOffset, FLOAT BaseDistance = 100.0, FLOAT VisibilitySphereSize = 10.0, BOOL bDrawSpheres = FALSE)
	VECTOR vOffset, vTestVector, vNodePosition
	
	//For the road test
	VECTOR vStartVector, vEndVector
	INT iLanesNorth, iLanesSouth
	FLOAT fMedianWidth
	
	//Ahead of the player
	vOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-1 * BaseDistance, 1 * BaseDistance), GET_RANDOM_FLOAT_IN_RANGE(1 * BaseDistance, -1 * BaseDistance), 0.0>>

	vTestVector = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.piTaxiPlayer, (vOffset + vBaseOffset))
	IF bDrawSpheres
//		DRAW_DEBUG_TEXT("Test offset", vTestVector)
//		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(Player), vTestVector)
//		DRAW_DEBUG_SPHERE(vTestVector, VisibilitySphereSize, 0, 0, 255, 255)
	ENDIF
	IF GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTestVector, vNodePosition, PassedHeading)
		
		IF GET_CLOSEST_ROAD(vNodePosition, 10.0, 2, vStartVector, vEndVector, iLanesNorth, iLanesSouth, fMedianWidth)
//			IF NOT (iLanesNorth > 2 OR  iLanesSouth > 2 OR fMedianWidth > 3.0)
//			AND NOT (iLanesNorth < 1 OR iLanesSouth < 1)
				Passedvector = vNodePosition
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPlayer, PassedVector) > 10.0
					IF bDrawSpheres
						DRAW_DEBUG_TEXT("Result Location", PassedVector)
						DRAW_DEBUG_LINE(PassedVector, vTestVector)
						DRAW_DEBUG_LINE(PassedVector, GET_ENTITY_COORDS(myTaxiData.piTaxiPlayer), 255, 0, 0, 255)
						DRAW_DEBUG_SPHERE(PassedVector, VisibilitySphereSize, 255, 0, 0, 255)
					ENDIF
					
					IF NOT IS_SPHERE_VISIBLE(PassedVector, VisibilitySphereSize)
						RETURN TRUE
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL TAXI_FIND_DRIVEBY_STRIP(TaxiStruct &myTaxiData, VECTOR &vPointToSpawn, VECTOR &vOptPointToSpawn, FLOAT fRadius, INT &iNodeAdress, FLOAT &fHeading, FLOAT fYOffset)
	
	//Pass in a vector that's offset behind the player
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		//Check if he's on a highway, this should handle the case where picking up a passenger underneath a hiway and the system returningn a node on the hiway.
		IF GET_IS_PLAYER_DRIVING_ON_HIGHWAY(GET_PLAYER_INDEX())
			IF GET_RANDOM_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, <<0.0,fYOffset,0.0>>),fRadius, 1,FALSE,FALSE,vPointToSpawn,iNodeAdress)
				GET_SPAWN_COORDS_FOR_VEHICLE_NODE(iNodeAdress, GET_ENTITY_COORDS(myTaxiData.viTaxi),vOptPointToSpawn,fHeading)
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_RANDOM_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, <<0.0,fYOffset,0.0>>),fRadius, 1,FALSE,TRUE,vPointToSpawn,iNodeAdress)
				GET_SPAWN_COORDS_FOR_VEHICLE_NODE(iNodeAdress, GET_ENTITY_COORDS(myTaxiData.viTaxi),vOptPointToSpawn,fHeading)
				RETURN TRUE
			ENDIF
		ENDIF
	
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL TAXI_FIND_SPAWN_NEAR_PLAYER(TaxiStruct &myTaxiData, VECTOR &vPointToSpawn,FLOAT fYOffset,FLOAT fXOffset)
	
	//Pass in a vector that's offset behind the player
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF GET_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi, <<0.0,fYOffset,0.0>>),vPointToSpawn)
			vPointToSpawn = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vPointToSpawn,GET_ENTITY_HEADING(myTaxiData.viTaxi),<<fXOffset,0.0,0.0>>)
			RETURN TRUE		
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    
/// PARAMS:
///    vTestVector - 
///    ReturnVector - 
///    ReturnHeading - 
///    bSideOfRoad - 
///    bDoVisibilityCheck - 
/// RETURNS:
///    
FUNC BOOL TAXI_GET_INTERSECTION(VECTOR vTestVector, VECTOR &ReturnVector, FLOAT &ReturnHeading, BOOL bSideOfRoad = FALSE, BOOL bDoVisibilityCheck = FALSE)
	VECTOR vStartVector, vEndVector
	INT iLanesNorth, iLanesSouth
	FLOAT fMedianWidth
	
	VECTOR vNodePosition
	
	FLOAT VisibilitySphereSize = 2.0
	
	IF GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vTestVector, vNodePosition, ReturnHeading)
		
		IF GET_CLOSEST_ROAD(vNodePosition, 5.0, 1, vStartVector, vEndVector, iLanesNorth, iLanesSouth, fMedianWidth)
		
			IF NOT (iLanesNorth > 2 OR  iLanesSouth > 2 OR fMedianWidth > 3.0)
			AND NOT (iLanesNorth < 1 OR iLanesSouth < 1)
			
				IF bSideOfRoad 
					IF GET_POSITION_BY_SIDE_OF_ROAD(vNodePosition, -1, ReturnVector)
//						IF GET_ENTITY_DISTANCE_FROM_LOCATION(Player, ReturnVector) > BaseDistance
							IF bDoVisibilityCheck
								IF NOT IS_SPHERE_VISIBLE(ReturnVector, VisibilitySphereSize)
									RETURN TRUE
								ENDIF
							ELSE
								RETURN TRUE
							ENDIF
//						ENDIF
					ENDIF
				ELSE
				
					ReturnVector = vNodePosition
//					IF GET_ENTITY_DISTANCE_FROM_LOCATION(Player, ReturnVector) > BaseDistance
						IF bDoVisibilityCheck
							IF NOT IS_SPHERE_VISIBLE(ReturnVector, VisibilitySphereSize)
								RETURN TRUE
							ENDIF
						ELSE
							RETURN TRUE
						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 


/// PURPOSE: Gives you a place to spawn a vehicle
///    
/// PARAMS:
///    Player - 
///    PassedVector - 
///    PassedHeading - 
///    vBaseOffset - 
///    bSideOfRoad - 
///    BaseDistance - 
///    bDoVisibilityCheck - 
///    bDrawSpheres - 
/// RETURNS: TRUE once a good road has been found. Sends it back into the passedVector & passedHeading
///    
FUNC BOOL TAXI_SEARCH_FOR_NEARBY_UNSEEN_ROAD(ENTITY_INDEX Player, VECTOR &PassedVector, FLOAT &PassedHeading, VECTOR vBaseOffset, BOOL bSideOfRoad = FALSE, BOOL bDoVisibilityCheck = FALSE, BOOL bDrawSpheres = FALSE)
	VECTOR vOffset, vTestVector//, vNodePosition

	bDrawSpheres = bDrawSpheres
	//Ahead of the player
	IF GET_RANDOM_INT_IN_RANGE(0, 60000) > 20000
		vOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-1 , 1 ), GET_RANDOM_FLOAT_IN_RANGE(0.5 , 1.5 ), 0.0>>
	ELSE
	//behind Player
		vOffset = <<GET_RANDOM_FLOAT_IN_RANGE(-1 , 1 ), GET_RANDOM_FLOAT_IN_RANGE(-0.5, -1.5), 0.0>>
	ENDIF
	vTestVector = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Player, (vOffset + vBaseOffset)) //GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Player,(vOffset + vBaseOffset))
					
	IF TAXI_GET_INTERSECTION(vTestVector, PassedVector, PassedHeading, bSideOfRoad, bDoVisibilityCheck)
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_SEARCH_FOR_NEARBY_UNSEEN_ROAD - point = ", "", PassedVector)
		Return TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_VEHICLE_TAXI_ENEMY(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct, FLOAT fCruiseSpeed, VEHICLE_MISSION Mission = MISSION_FLEE)

	theEscapestruct.viCar = CREATE_VEHICLE(theEscapestruct.escapeeCarModel, theEscapestruct.vCarSpawnPt, theEscapeStruct.fCarheading)
	
	//CLEAR_AREA_OF_VEHICLES(theEscapestruct.vCarSpawnPt,TAXI_OJ_RADIUS_CLEAR_VEHICLE)
	
	IF IS_VEHICLE_DRIVEABLE(theEscapestruct.viCar)
		theEscapeStruct.fCruiseSpeed = fCruiseSpeed

		SET_VEHICLE_FORWARD_SPEED(theEscapestruct.viCar, 15)
		
		//Create driver and set him up
		theEscapestruct.piPed = CREATE_PED_INSIDE_VEHICLE(theEscapestruct.viCar, PEDTYPE_MISSION, theEscapestruct.escapeeModel)
		
		//Add to dialogue struct.
		ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,4,theEscapestruct.piPed,"PED4")
		
		SET_PED_RELATIONSHIP_GROUP_HASH(theEscapestruct.piPed, theEscapestruct.relEscapee)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_USE_VEHICLE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_FLEE_WHILST_IN_VEHICLE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_ALWAYS_FLEE,TRUE)
		SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_SEEING_RANGE(theEscapestruct.piPed, 300.0)
		SET_PED_HEARING_RANGE(theEscapestruct.piPed, 300.0)
		SET_PED_ID_RANGE(theEscapestruct.piPed, 300.0)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(theEscapestruct.piPed, TRUE)		
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(theEscapeStruct.viCar,TRUE)
		TASK_VEHICLE_MISSION_PED_TARGET(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.piTaxiPlayer, Mission, theEscapeStruct.fCruiseSpeed, DRIVINGMODE_AVOIDCARS, 0, 0)
		
//		No need to blip for now as we want him inconspicuous
		IF DOES_BLIP_EXIST(myTaxiData.blipTaxiPassenger)
			REMOVE_BLIP(myTaxiData.blipTaxiPassenger)
		ENDIF
		
		theEscapestruct.bBlip =CREATE_BLIP_ON_ENTITY(theEscapestruct.viCar) 
		
		//Store current health
		SET_ENTITY_HEALTH(theEscapeStruct.viCar,TAXI_CONST_STOP_CAR_START_HEALTH)
		theEscapeStruct.fCurrentEngHealth = GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) 
			
		theEscapestruct.tesState = TES_INIT		
		
		//After we've successfully spawned the escapee set roads back to orig
		TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(myTaxiData.vTaxiOJPickup,TRUE)
						
		CDEBUG1LN(DEBUG_OJ_TAXI,"SPAWN_ESCAPEE - SUCCESS")
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
ENDFUNC
PROC TAXI_ODDJOB_SET_AI_SPAWN(escapeStruct &theEscapeStruct, VECTOR vSpawnPt, FLOAT fHeading)
	theEscapeStruct.vCarSpawnPt = vSpawnPt
	theEscapeStruct.fCarHeading = fHeading
	
ENDPROC

PROC TAXI_WATCH_FOLLOW_STATUS(TaxiStruct &myTaxiData, structTimer &spottedTimer, structTimer &lostTimer)
	//Pause Timers once player is back in sweet spot
	IF myTaxiData.tTaxiOJ_FollowPosition = TAXI_SWEET_SPOT
		IF NOT IS_TIMER_PAUSED(spottedTimer)
		AND IS_TIMER_STARTED(spottedTimer)
			PAUSE_TIMER(spottedTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS - Spotted Timer Paused")
		ENDIF
		
		IF NOT IS_TIMER_PAUSED(lostTimer)
		AND IS_TIMER_STARTED(lostTimer)
			PAUSE_TIMER(lostTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS - Lost Timer Paused")
		ENDIF
		
	ELIF myTaxiData.tTaxiOJ_FollowPosition = TAXI_TOO_CLOSE_TARGET
		//Use a timer to see if you've been in DANGER ZONE for too long, if so FAIL
		IF NOT IS_TIMER_STARTED(spottedTimer)
			START_TIMER_NOW(spottedTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS - timerSpotted - started")
		ELIF IS_TIMER_PAUSED(spottedTimer)
			UNPAUSE_TIMER(spottedTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS - spottedTimer - unpaused")
		ENDIF
		
	//Use a timer to see if you've been in DANGER ZONE for too long, if so FAIL
	ELIF myTaxiData.tTaxiOJ_FollowPosition = TAXI_LOSING_CHASE
		IF NOT IS_TIMER_STARTED(lostTimer)
			START_TIMER_NOW(lostTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS - timerLost - started")
		ELIF IS_TIMER_PAUSED(lostTimer)
			UNPAUSE_TIMER(lostTimer)
			CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_WATCH_FOLLOW_STATUS -lostTimer - unpaused")
		ENDIF
	ENDIF
	
	//Fail for Losing Site for too long
	IF TIMER_DO_ONCE_WHEN_READY(lostTimer, TAXI_FOLLOW_LOST_TIME)
		TAXI_SET_FAIL(myTaxiData,"TAXI_FTCFAIL1", TFS_LOST_CAR)
	ENDIF
	
	//Fail for Following too close for 2 long
	IF TIMER_DO_ONCE_WHEN_READY(spottedTimer, TAXI_FOLLOW_TOOCLOSE_TIME)
		TAXI_SET_FAIL(myTaxiData,"TAXI_FTCFAIL2", TFS_SPOTTED)
	ENDIF
	
ENDPROC
//Handle an edge case where the ped could leave the car and then it would just continue to drive itself.
PROC TAXI_OJ_CLEANUP_VEHICLE_RECORDING_IF_PED_EXITS(escapeStruct &theEscapeStruct)
	
	IF IS_PED_AND_VEHICLE_VALID(theEscapeStruct.piPed,theEscapeStruct.viCar )
		IF NOT IS_PED_IN_VEHICLE(theEscapeStruct.piPed,theEscapeStruct.viCar)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(theEscapeStruct.viCar)
				STOP_PLAYBACK_RECORDED_VEHICLE(theEscapeStruct.viCar)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
	//Monitors if the enemy is stuck hasn't moved for a while
FUNC BOOL TAXI_OJ_CHECK_ESCAPEE_STUCK(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct)
	
	If (GET_ENTITY_SPEED(theEscapeStruct.viCar) < CONST_TAXI_OJ_STOP_CAR_SPEED_STUCK)
		IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOP_STUCK) 
			TAXI_START_TIMER(myTaxiData, TT_STOP_STUCK)
			CDEBUG1LN(DEBUG_OJ_TAXI,"CES_Boring / Stuck Timer started")
		ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_STOP_STUCK) > CONST_TAXI_OJ_STOP_CAR_STUCK_TIME
			CDEBUG1LN(DEBUG_OJ_TAXI,"CES_Taxi Timer Boring / Stuck is returning true")
			RETURN TRUE
		ENDIF
		CDEBUG1LN(DEBUG_OJ_TAXI,"ENtity speed is below min reqs")
	ELSE
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_STOP_STUCK) 
			TAXI_CANCEL_TIMERS(myTaxiData, TT_STOP_STUCK)
			CDEBUG1LN(DEBUG_OJ_TAXI,"CES_Boring / Stuck Timer Cenceled")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: All the taxi AI behavior is handled here
///    
/// PARAMS:
///    myTaxiData - ref to the global taxi data
///    theEscapeStruct - ref to the AI struct
/// RETURNS:
///    
FUNC BOOL HANDLE_ESCAPEE(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct)
	
	BOOL bShouldBail
	
	FLOAT fDistToTarget
	//Update distance to target for fail checks and dialogue
	IF DOES_ENTITY_EXIST(theEscapeStruct.piPed)
		fDistToTarget = GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.piTaxiPlayer, theEscapeStruct.piPed)
	ENDIF
	
	//This check runs the entire state machine---------------------------------------------------------------
	IF theEscapeStruct.tesState = TES_DRIVING
	OR theEscapeStruct.tesState = TES_EXITING
	OR theEscapeStruct.tesState = TES_ON_FOOT

		
		IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
		
			TAXI_OJ_CLEANUP_VEHICLE_RECORDING_IF_PED_EXITS(theEscapeStruct)
			
			//Override the cinematic cam
			CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(theEscapeStruct.viCar)
			
			//Calculates how much damage vehicle has taken and after certain threshold do something
			IF GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) < CONST_TAXI_OJ_HEALTH_STOP_CAR_THRESHOLD
			OR GET_ENTITY_HEALTH(theEscapeStruct.viCar) < CONST_TAXI_OJ_HEALTH_STOP_CAR_THRESHOLD
			OR TAXI_OJ_CHECK_ESCAPEE_STUCK(myTaxiData, theEscapeStruct)
#IF IS_DEBUG_BUILD
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
#ENDIF				
				//Output all values for debugging
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(theEscapeStruct.viCar)
					STOP_PLAYBACK_RECORDED_VEHICLE(theEscapeStruct.viCar)
					
				ENDIF

				SET_VEHICLE_ENGINE_ON(theEscapeStruct.viCar,FALSE,FALSE)
				
				bShouldBail = TRUE
			ENDIF
		ENDIF
	ENDIF
	//==========================================================================================================
	
	//Goes on during the DRIVE STATE----------------------------------------------------------------------------
	//Checks when the driver should bail if the car has taken too much damage/is stuck/ or player has stayed close long enough.rere
	IF theEscapeStruct.tesState = TES_DRIVING
		IF NOT bShouldBail
			
			IF (IS_VEHICLE_STUCK_TIMER_UP(theEscapeStruct.viCar, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(theEscapeStruct.viCar, VEH_STUCK_JAMMED, 20000)
			OR IS_VEHICLE_STUCK_TIMER_UP(theEscapeStruct.viCar, VEH_STUCK_HUNG_UP, 20000)
			OR IS_VEHICLE_STUCK_TIMER_UP(theEscapeStruct.viCar, VEH_STUCK_ON_SIDE, 5000))
				bShouldBail = TRUE
				CDEBUG1LN(DEBUG_OJ_TAXI,"Escapee is bailing because car is stuck!")
			ENDIF			
		ENDIF
	ENDIF
	//===========================================================================================================
	SWITCH theEscapeStruct.tesState		
		CASE TES_INIT//------------------------------------
			IF NOT DOES_ENTITY_EXIST(theEscapeStruct.piPed)
			OR NOT DOES_ENTITY_EXIST(theEscapeStruct.viCar)
				theEscapeStruct.tesState = TES_DEAD
				CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DEAD")
			ELSE
				IF NOT IS_PED_INJURED(theEscapeStruct.piPed)
				AND IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
					
					theEscapeStruct.iStartHealth = GET_ENTITY_HEALTH(theEscapeStruct.viCar)
					
					theEscapeStruct.tesState = TES_DRIVING
					CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DRIVING")
			
				ELSE
					theEscapeStruct.tesState = TES_DEAD
					CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DEAD")

				ENDIF
			ENDIF
		BREAK
		
		CASE TES_DRIVING//--------------------------------
			IF NOT IS_PED_INJURED(theEscapeStruct.piPed)
				IF IS_PED_IN_VEHICLE_SAFE(theEscapeStruct.piPed, theEscapeStruct.viCar)
					IF (fDistToTarget < TAXI_OJ_DIST_SC_FAIL OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(theEscapeStruct.piPed), TAXI_OJ_RADIUS_VISIBLE_MULT))
						IF NOT bShouldBail	
							
							IF GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> WAITING_TO_START_TASK
							AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> DORMANT_TASK
							AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> VACANT_STAGE
							AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> GROUP_TASK_STAGE
							AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, theEscapeStruct.stn_CurrentTask) <> SECONDARY_TASK_STAGE
								CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Tasking escapee to flee in car")
								
								//If follow mode is on , set him to behave nicely
								IF theEscapeStruct.bIsFollowMode
									TASK_VEHICLE_DRIVE_TO_COORD(theEscapeStruct.piPed,theEscapeStruct.viCar,myTaxiData.vTaxiOJDropoff,TAXI_FOLLOW_CRUISE_SPEED,DRIVINGSTYLE_NORMAL,theEscapeStruct.escapeeCarModel,DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,5.0,-1)
								
								//Otherwise drive as frantic as possible
								ELSE						
									TASK_VEHICLE_MISSION_PED_TARGET(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.piTaxiPlayer, MISSION_FLEE, theEscapeStruct.fCruiseSpeed, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS, 0, 0)
									theEscapeStruct.fHeading = theEscapeStruct.fCruiseSpeed
								ENDIF
							ELSE
								IF fDistToTarget < 50.0
									If theEscapeStruct.fHeading <> theEscapeStruct.fCruiseSpeed
										theEscapeStruct.fHeading = theEscapeStruct.fCruiseSpeed
										SET_DRIVE_TASK_CRUISE_SPEED(theEscapeStruct.piPed, theEscapeStruct.fHeading)
										CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Setting cruise speed to max.")
									ENDIF
								ELIF fDistToTarget < 100.0
									If theEscapeStruct.fHeading <> (theEscapeStruct.fCruiseSpeed * 0.75)
										theEscapeStruct.fHeading = (theEscapeStruct.fCruiseSpeed * 0.75)
										
										SET_DRIVE_TASK_CRUISE_SPEED(theEscapeStruct.piPed, theEscapeStruct.fHeading)
										CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Setting cruise speed to 0.75 of max.")
									ENDIF
								ELIF fDistToTarget < 150.0
									If theEscapeStruct.fHeading <> (theEscapeStruct.fCruiseSpeed * 0.50)
										
										theEscapeStruct.fHeading = (theEscapeStruct.fCruiseSpeed * 0.50)
										SET_DRIVE_TASK_CRUISE_SPEED(theEscapeStruct.piPed, theEscapeStruct.fHeading)
										CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Setting cruise speed to 0.50 of max.")
									ENDIF
								ELSE
									If theEscapeStruct.fHeading <> theEscapeStruct.fCruiseSpeed
										theEscapeStruct.fHeading = theEscapeStruct.fCruiseSpeed
										SET_DRIVE_TASK_CRUISE_SPEED(theEscapeStruct.piPed, theEscapeStruct.fHeading)
										CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Setting cruise speed to max for escape.")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							
							theEscapeStruct.tesState = TES_EXITING
							CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_EXITING")
						ENDIF
					ELSE
						TAXI_SET_FAIL(myTaxiData, "Enemy escaped.", TFS_LOST_CAR)
					ENDIF
				ELSE
					theEscapeStruct.tesState = TES_EXITING
					CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_EXITING")

				ENDIF
			ELSE
				theEscapeStruct.tesState = TES_DEAD
				CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DEAD")
			ENDIF
		BREAK
		
		CASE TES_EXITING//----------------------------------------------------
			IF NOT IS_PED_INJURED(theEscapeStruct.piPed)
				IF (fDistToTarget < TAXI_OJ_DIST_SC_FAIL OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(theEscapeStruct.piPed), TAXI_OJ_RADIUS_VISIBLE_MULT))
					IF IS_VEHICLE_STOPPED(theEscapeStruct.viCar)
						IF IS_PED_IN_ANY_VEHICLE(theEscapeStruct.piPed)
							IF NOT IS_ENTITY_ON_FIRE(theEscapeStruct.viCar)
								IF GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> DORMANT_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> VACANT_STAGE
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> GROUP_TASK_STAGE
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> SECONDARY_TASK_STAGE
									
									//SET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar, -500.0)
									SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed,  CA_LEAVE_VEHICLES, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed,  CA_ALWAYS_FLEE, TRUE)
									CLEAR_PED_TASKS(theEscapeStruct.piPed)
									TASK_LEAVE_ANY_VEHICLE(theEscapeStruct.piPed)
									CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - escapee exiting from vehicle.")
							
								ENDIF
							ELSE	
								IF GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> DORMANT_TASK
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> VACANT_STAGE
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> GROUP_TASK_STAGE
								AND GET_SCRIPT_TASK_STATUS(theEscapeStruct.piPed, SCRIPT_TASK_LEAVE_VEHICLE) <> SECONDARY_TASK_STAGE
								
									
									SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed,  CA_LEAVE_VEHICLES, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(theEscapestruct.piPed, CA_ALWAYS_FLEE , TRUE)
									CLEAR_PED_TASKS(theEscapeStruct.piPed)
									TASK_LEAVE_VEHICLE(theEscapeStruct.piPed, theEscapeStruct.viCar, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT)
									CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - escapee leaping from vehicle.")
								ENDIF
							ENDIF
						//Ped has EXITED Cleanup everything	
						ELSE
							SEQUENCE_INDEX	tempIndex
							
							IF DOES_BLIP_EXIST(theEscapeStruct.bBlip)
								REMOVE_BLIP(theEscapeStruct.bBlip)
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(theEscapestruct.piPed)
				
							CLEAR_SEQUENCE_TASK(tempIndex)
							OPEN_SEQUENCE_TASK(tempIndex)
								
								TASK_LEAVE_ANY_VEHICLE(NULL,0, ECF_JUMP_OUT)
								TASK_SMART_FLEE_PED(NULL,myTaxiData.piTaxiPlayer,10000,-1)
								TASK_SMART_FLEE_COORD(NULL, myTaxiData.vTaxiOJDropoff, 20.0, 10000, TRUE)
							
							CLOSE_SEQUENCE_TASK(tempIndex)
							TASK_PERFORM_SEQUENCE(theEscapestruct.piPed, tempIndex)
							CLEAR_SEQUENCE_TASK(tempIndex)
							
							
							theEscapeStruct.tesState = TES_ON_FOOT
							CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_ON_FOOT")
						ENDIF
					ENDIF
				ELSE
					TAXI_SET_FAIL(myTaxiData, "Enemy escaped.", TFS_LOST_CAR)

				ENDIF
			ELSE
				theEscapeStruct.tesState = TES_DEAD
				CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DEAD")

			ENDIF
		BREAK
		
		CASE TES_ON_FOOT//-----------------------------------
			IF NOT IS_PED_INJURED(theEscapeStruct.piPed)
				IF (fDistToTarget < TAXI_OJ_DIST_SC_FAIL OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(theEscapeStruct.piPed), TAXI_OJ_RADIUS_VISIBLE_MULT))
					IF NOT IS_PED_IN_ANY_VEHICLE(theEscapeStruct.piPed)
						
				
						IF NOT IS_PED_FLEEING(theEscapeStruct.piPed)
							TASK_SMART_FLEE_COORD(theEscapeStruct.piPed, myTaxiData.vTaxiOJDropoff, 20.0, 10000, TRUE)
							CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi - Tasking escapee to flee on foot")
						ENDIF
						RETURN TRUE
						
					ELSE
						TASK_LEAVE_ANY_VEHICLE(theEscapeStruct.piPed)
						theEscapeStruct.tesState = TES_EXITING
						CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_EXITING")
					ENDIF
				ELSE
					TAXI_SET_FAIL(myTaxiData, "Enemy escaped.", TFS_LOST_CAR)
				ENDIF
			ELSE
				theEscapeStruct.tesState = TES_DEAD
				CDEBUG1LN(DEBUG_OJ_TAXI,"tesState = TES_DEAD")

			ENDIF
		BREAK
		
		CASE TES_DEAD//---------------------------------------
			IF DOES_BLIP_EXIST(theEscapeStruct.bBlip)
				REMOVE_BLIP(theEscapeStruct.bBlip)
			ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//USED
/// PURPOSE: Track how many times the player hits the chasee.
///    
/// PARAMS:
///    myTaxiData - 
///    theEscapeStruct - 
///    bShowDebug - 
PROC UPDATE_NUM_TIMES_PLAYER_HIT_NPC(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct, BOOL bShowDebug = TRUE)
	
	theEscapeStruct.iNumTimesHitByPlayer++
	theEscapeStruct.iNumTimesWarned++
	
	IF theEscapeStruct.iNumTimesHitByPlayer >= TAXI_FOLLOW_CAR_HITS_BEFORE_FAIL //OR theEscapeStruct.iNumTimesWarned > CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			ENABLE_TAXI_SPEECH(myTaxiData,FALSE,TRUE)
			IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
			AND NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
				TASK_VEHICLE_MISSION(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.viTaxi, MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 1000)
				SET_PED_KEEP_TASK(theEscapeStruct.piPed, TRUE)
			ENDIF			
			TAXI_SET_FAIL(myTaxiData,"Taxi was ID'd by the Chasee", TFS_SPOTTED)
		ELSE
			CDEBUG1LN(DEBUG_OJ_TAXI,"can't fail player yet for being hitting too many times, killing conversation first")
			KILL_ANY_CONVERSATION()
		ENDIF
	ELSE
		TAXI_RESET_TIMERS(myTaxiData, TT_COLLIDE)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(theEscapeStruct.viCar)
		TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_HIT_CAR)
		ENABLE_TAXI_SPEECH(myTaxiData)
	ENDIF
	
	IF bShowDebug
		CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI HIT - Num of time player hit NPC = ", "", theEscapeStruct.iNumTimesHitByPlayer)
	ENDIF
	
ENDPROC

//USED
PROC TAXI_AI_HANDLE_BLIPS(TaxiStruct &myTaxiData, BLIP_INDEX &blipToTurnOn)

	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		//Player NOT in Taxi
		IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
	
			//Fail Timer if player has been out of cab too long with warning.
			IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				//Hide the current obj blip
				IF DOES_BLIP_EXIST(blipToTurnOn)
				AND GET_BLIP_ALPHA(blipToTurnOn) > 0
					SET_BLIP_ALPHA(blipToTurnOn,0)
				ENDIF
				
				TAXI_OJ_UPDATE_BLIP_TAXI(myTaxiData)
				
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_LEFT_CAR,TRUE)
			ELSE
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_NOTINCAB) > TAXI_TWEAK_TIME_FAIL_ABANDONED_CAR
					TAXI_SET_FAIL(myTaxiData, "Player not in taxi - HANDLE BLIPS", TFS_ABANDONED_PASSENGER)
				ENDIF
			ENDIF
			
		//Player returned to the cab-------------------------------------------
		ELSE
			//Even though the fail check only runs when he's not in the cab, it's important to cancel
			//this timer cause it controls other important systems in the dialogue
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_NOTINCAB)
				TAXI_RESET_TIMERS(myTaxiData, TT_STOPPED)
				SET_PLAYER_REENTERED_TAXI_OJ(myTaxiData)
			ENDIF
			
			//*Sigh* I have to turn off all the mission blips whatever they may be....
			//Leave this for a later day.
			IF DOES_BLIP_EXIST(myTaxiData.blipTaxiVehicle)
				REMOVE_BLIP(myTaxiData.blipTaxiVehicle)
			ENDIF	
				
			
			IF DOES_BLIP_EXIST(blipToTurnOn)
			AND GET_BLIP_ALPHA(blipToTurnOn) < 255
			AND GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1

				SET_BLIP_ALPHA(blipToTurnOn,255)
				SET_BLIP_ROUTE(blipToTurnOn,TRUE)
				//This should give the player his last objective, but ONLY ONCE
				//SPEAK_IN_TAXI_OJ_IF_NOT_GATED(myTaxiData,myTaxiData.iTaxiOJ_Bits_Gates, TAXI_GATE_REGIVE_OBJ,myTaxiData.tTaxiOJ_ObjectiveCurrent)
			
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_GOING_REALLY_SLOW(VEHICLE_INDEX & viSlow)
	IF IS_VEHICLE_DRIVEABLE(viSlow)
		IF IS_VEHICLE_STOPPED(viSlow)
		OR GET_ENTITY_SPEED(viSlow) < 4.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_BETWEEN_SLOW_AND_GO(VEHICLE_INDEX & viSlow)
	IF IS_VEHICLE_DRIVEABLE(viSlow)
		IF (GET_ENTITY_SPEED(viSlow) >= 4.0 AND GET_ENTITY_SPEED(viSlow) < 15.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_AT_MOVING_SPEED(VEHICLE_INDEX & viSlow)
	IF IS_VEHICLE_DRIVEABLE(viSlow)
		IF GET_ENTITY_SPEED(viSlow) >= 15.0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DYNAMIC_DISTANCE_CHECK(VEHICLE_INDEX & viSlow)
	FLOAT fDistanceCheck
	FLOAT fCurrentSpeed, fTooclose, fTooCloseStopped
	
	fTooclose = TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST
	fTooCloseStopped = TWEAK_TAXI_FOLLOW_ESCAPEE_STOPPED_TOO_CLOSE_DIST
	
	IF IS_VEHICLE_DRIVEABLE(viSlow)
		fCurrentSpeed = GET_ENTITY_SPEED(viSlow)
	ENDIF
	
	fDistanceCheck = ((((fCurrentSpeed - 4.0) / 11) * (fTooclose - fTooCloseStopped)) + fTooCloseStopped)
	
	RETURN fDistanceCheck
ENDFUNC

FUNC INT TAXI_AI_FOLLOW_GET_NUM_LOST(escapeStruct &theEscapeStruct)
	RETURN theEscapeStruct.iNumTimesTooClose
ENDFUNC

PROC TAXI_CLEAR_DISTANCE_WARNING_DIALOGUE_IF_PLAYER_DISTANCE_IS_SAFE(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct)
	IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
		IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < TWEAK_TAXI_FOLLOW_TOO_FAR_DIST_WARN
			IF myTaxiData.tTaxiOJ_FollowPosition = TAXI_TOO_FAR_WARNING
			OR myTaxiData.tTaxiOJ_FollowPosition = TAXI_LOSING_CHASE
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) = TAXI_DI_FOLLOW_CAR_BARKS
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CLEAR_DISTANCE_WARNING_DIALOGUE_IF_PLAYER_DISTANCE_IS_SAFE: Clear too FAR warnning dlg since player got to safe dist")
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST
			IF myTaxiData.tTaxiOJ_FollowPosition = TAXI_TOO_CLOSE_WARNING
			OR myTaxiData.tTaxiOJ_FollowPosition = TAXI_TOO_CLOSE_TARGET
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) = TAXI_DI_FOLLOW_CAR_BARKS
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CLEAR_DISTANCE_WARNING_DIALOGUE_IF_PLAYER_DISTANCE_IS_SAFE: Clear too CLOSE warnning dlg since player got to safe dist")
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

//USED
/// PURPOSE: This keeps track of the player following too far, too close, or hitting the chasee.
///    
/// PARAMS:
///    myTaxiData - global taxi datat
///    theEscapeStruct - our escapee instance
PROC TAXI_AI_WATCH_FOLLOW_STATUS(TaxiStruct &myTaxiData, escapeStruct &theEscapeStruct, BOOL bMonitorTooClose = TRUE, BOOL bDrawDebug = FALSE)
	
	//Monitor the followee
	IF NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(theEscapeStruct.piPed,PLAYER_PED_ID(),FALSE)	
			TAXI_SET_FAIL(myTaxiData,"Taxi shot up the car", TFS_CHASEE_SHOT)
		ENDIF
	ELSE
		TAXI_SET_FAIL(myTaxiData,"Followee died", TFS_CHASEE_KILLED)
	ENDIF
	
	//Fail for going wanted
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
	AND IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
	AND NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
		TASK_VEHICLE_MISSION(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.viTaxi, MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 1000)
		SET_PED_KEEP_TASK(theEscapeStruct.piPed, TRUE)
		TAXI_SET_FAIL(myTaxiData,"Player went wanted", TFS_WANTED)
	ENDIF
	
	TAXI_CLEAR_DISTANCE_WARNING_DIALOGUE_IF_PLAYER_DISTANCE_IS_SAFE(myTaxiData, theEscapeStruct)
	
	IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
		
		//Debug sphere around safe follow position
		IF bDrawDebug
			IF IS_VEHICLE_AT_MOVING_SPEED(theEscapeStruct.viCar)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(theEscapeStruct.viCar),TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST,255,0,0,60)
			ELIF IS_VEHICLE_BETWEEN_SLOW_AND_GO(theEscapeStruct.viCar)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(theEscapeStruct.viCar),GET_DYNAMIC_DISTANCE_CHECK(theEscapeStruct.viCar),255,0,0,60)
			ELIF IS_VEHICLE_GOING_REALLY_SLOW(theEscapeStruct.viCar)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(theEscapeStruct.viCar),TWEAK_TAXI_FOLLOW_ESCAPEE_STOPPED_TOO_CLOSE_DIST,125,0,125,60)
			ENDIF
		
		ENDIF
					
		//Should detect whether player hit vehicle
		IF GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) < theEscapeStruct.fCurrentEngHealth
		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(theEscapeStruct.viCar,PLAYER_PED_ID())
		OR GET_VEHICLE_PETROL_TANK_HEALTH(theEscapeStruct.viCar) < 	theEscapeStruct.fPetrolHealth
			
			//Does this check gunshots????
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(theEscapeStruct.viCar,PLAYER_PED_ID(),FALSE)
				
				TAXI_SET_FAIL(myTaxiData,"Taxi shot up the car", TFS_CHASEE_SHOT)
			ENDIF
			
			
			//IF GET_TIME_SINCE_PLAYER_HIT_VEHICLE(GET_PLAYER_INDEX()) < CONST_TAXI_TIME_CHECK_HIT
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_COLLIDE) > CONST_TAXI_HIT_FOLLOW_CAR_DELAY
				UPDATE_NUM_TIMES_PLAYER_HIT_NPC(myTaxiData, theEscapeStruct,TRUE)

			ELSE
				//Escape health took a hit, but not by the player
				theEscapeStruct.fCurrentEngHealth = GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) 
				theEscapeStruct.iStartHealth = GET_ENTITY_HEALTH(theEscapeStruct.viCar)
				theEscapeStruct.fPetrolHealth = GET_VEHICLE_PETROL_TANK_HEALTH(theEscapeStruct.viCar)
			
				CDEBUG1LN(DEBUG_OJ_TAXI,"Engine hit, but not by player 2nd check")
				CDEBUG1LN(DEBUG_OJ_TAXI," ENGINE HEALTH = ", "", GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar), " Current Health = ", "", theEscapeStruct.fCurrentEngHealth)
				CDEBUG1LN(DEBUG_OJ_TAXI," ENTITY HEALTH = ", "", GET_ENTITY_HEALTH(theEscapeStruct.viCar), " Current Health = ", "", theEscapeStruct.iStartHealth)
				CDEBUG1LN(DEBUG_OJ_TAXI," PETROL HEALTH = ", "", GET_VEHICLE_PETROL_TANK_HEALTH(theEscapeStruct.viCar), " Current PEtrol = ", "", theEscapeStruct.fPetrolHealth)
				
				
			ENDIF
		ENDIF
		
		//See that the player has gotten a certain distance away before checking if he has LOS 
		IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_FAR_DIST_WARN
			
			IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_FAR_FAIL_NOW_DIST
				SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EMPTY,FALSE)
				
				TAXI_SET_FAIL(myTaxiData,"Taxi let the Chasee lose him", TFS_LOST_CAR)
				
			ELIF NOT theEscapeStruct.bIsOutofSight
				//If he can't see the car than set to letting get away.
//				IF (NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.viTaxi,theEscapeStruct.viCar) OR GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_FAR_DIST)
//				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) > TAXI_CONST_TIME_BEFORE_CHECK_DIST
				IF (IS_LOS_SHAPETEST_CLEAR_BETWEEN_COORDS(myTaxiData.tShapeTest, GET_ENTITY_COORDS(myTaxiData.viTaxi),GET_ENTITY_COORDS(theEscapeStruct.viCar)) = -1 AND GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_FAR_DIST)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) > TAXI_CONST_TIME_BEFORE_CHECK_DIST
				
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: theEscapeStruct.iNumTimesWarned++ = ", theEscapeStruct.iNumTimesWarned)
					theEscapeStruct.bIsOutofSight = TRUE
					theEscapeStruct.bIsSweetSpot = FALSE
					theEscapeStruct.iNumTimesWarned++
					//theEscapeStruct.iNumTimesOutofSight++
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
					
					IF theEscapeStruct.iNumTimesWarned > CONST_TAXI_FOLLOW_NUM_TOO_FAR_FAIL //Fail case if been warned more than 
						IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
						AND NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
							TASK_VEHICLE_MISSION(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.viTaxi, MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 1000)
							SET_PED_KEEP_TASK(theEscapeStruct.piPed, TRUE)
						ENDIF	
						TAXI_SET_FAIL(myTaxiData,"Taxi let the Chasee lose him", TFS_LOST_CAR)
					ELIF theEscapeStruct.iNumTimesWarned = CONST_TAXI_FOLLOW_NUM_TOO_FAR_FAIL //Warn player about to fail
						TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_TOO_FAR_WARNING)	
					ELSE
						TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_LOSING_CHASE) 	//Bitch to player he's too close
					ENDIF
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Follow Car - Num Times Out of Site: ", "", theEscapeStruct.iNumTimesOutofSight)
				ENDIF
			//Out of Site is true
			ELSE
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.viTaxi,theEscapeStruct.viCar)
					theEscapeStruct.bIsOutofSight = FALSE
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) >= TWEAK_TAXI_FOLLOW_TOO_FAR_FAIL_DIST
					//AND theEscapeStruct.iNumTimesOutofSight > CONST_TAXI_FOLLOW_NUM_TOO_FAR_FAIL_WARN
						
				
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EMPTY,FALSE)
						
						TAXI_SET_FAIL(myTaxiData,"Taxi let the Chasee lose him", TFS_LOST_CAR)
					ENDIF
				ENDIF
			ENDIF
						
		ELIF ( (GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST
			AND IS_VEHICLE_AT_MOVING_SPEED(theEscapeStruct.viCar))
		OR (GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < TWEAK_TAXI_FOLLOW_ESCAPEE_STOPPED_TOO_CLOSE_DIST
			AND IS_VEHICLE_GOING_REALLY_SLOW(theEscapeStruct.viCar)) 
		OR (GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < GET_DYNAMIC_DISTANCE_CHECK(theEscapeStruct.viCar)
			AND IS_VEHICLE_BETWEEN_SLOW_AND_GO(theEscapeStruct.viCar)) )
			//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: GET_DISTANCE_BETWEEN_ENTITIES < TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST")
			IF NOT theEscapeStruct.bIsTooClose
			OR IS_TAXI_TIMER_STARTED(myTaxiData, TT_PUKING)
				CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS:NOT theEscapeStruct.bIsTooClose")
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.viTaxi,theEscapeStruct.viCar)
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) > TAXI_CONST_TIME_BEFORE_CHECK_DIST
				AND bMonitorTooClose
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: theEscapeStruct.iNumTimesWarned++ = ", theEscapeStruct.iNumTimesWarned)
					theEscapeStruct.bIsTooClose = TRUE
					theEscapeStruct.bIsSweetSpot = FALSE
					theEscapeStruct.iNumTimesTooClose++
					theEscapeStruct.iNumTimesWarned++
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
					
					IF NOT IS_TAXI_TIMER_STARTED(myTaxiData, TT_PUKING)
						TAXI_RESET_TIMERS(myTaxiData, TT_PUKING)
					ENDIF
					//Fail case if been warned more than 
					IF theEscapeStruct.iNumTimesWarned > CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
							AND NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
								TASK_VEHICLE_MISSION(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.viTaxi, MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 1000)
								SET_PED_KEEP_TASK(theEscapeStruct.piPed, TRUE)
							ENDIF					
							TAXI_SET_FAIL(myTaxiData,"Taxi was ID'd by the Chasee", TFS_SPOTTED)
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"can't fail player yet for being too close too many times, killing conversation first")
							KILL_ANY_CONVERSATION()
						ENDIF
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS:theEscapeStruct.iNumTimesWarned > CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL, SET FAIL")
					//Warn player about to fail
					ELIF theEscapeStruct.iNumTimesWarned = CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL
						TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_TOO_CLOSE_WARNING)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: theEscapeStruct.iNumTimesWarned = CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL")		
					//Bitch to player he's too close
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_TOO_CLOSE_TARGET)")
						TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_TOO_CLOSE_TARGET)
					ENDIF
					IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < TWEAK_TAXI_FOLLOW_TOO_CLOSE_FAIL_NOW_DIST
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_EMPTY,FALSE)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF IS_VEHICLE_DRIVEABLE(theEscapeStruct.viCar)
							AND NOT IS_ENTITY_DEAD(theEscapeStruct.piPed)
								TASK_VEHICLE_MISSION(theEscapeStruct.piPed, theEscapeStruct.viCar, myTaxiData.viTaxi, MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 1000)
								SET_PED_KEEP_TASK(theEscapeStruct.piPed, TRUE)
							ENDIF							
							TAXI_SET_FAIL(myTaxiData,"Taxi was ID'd by the Chasee", TFS_SPOTTED)
						ELSE
							CDEBUG1LN(DEBUG_OJ_TAXI,"can't fail player yet for being too close, killing conversation first")
							KILL_ANY_CONVERSATION()
						ENDIF
						
					ELIF theEscapeStruct.iNumTimesWarned <= CONST_TAXI_FOLLOW_NUM_TOO_CLOSE_FAIL 
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS,TRUE)
					ENDIF					
										
					//Store current health
					theEscapeStruct.fCurrentEngHealth = GET_VEHICLE_ENGINE_HEALTH(theEscapeStruct.viCar) 
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"Follow Car - Num Times Too Close: ", "", theEscapeStruct.iNumTimesTooClose)
				ELSE
//					IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.viTaxi,theEscapeStruct.viCar)
//						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY")
//					ENDIF
//					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) <= TAXI_CONST_TIME_BEFORE_CHECK_DIST					
//						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: NOT TT_FOLLOW") 				
//					ENDIF
//					IF NOT bMonitorTooClose
//						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: NOT bMonitorTooClose")
//					ENDIF
				ENDIF
			ELSE
				//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: theEscapeStruct.bIsTooClose SET TO TRUE STILL!")
			ENDIF
		
		//If player is not TOO CLOSE OR NOT TOO FAR, THAN HE'S JUST RIGHT?
		ELSE
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_PUKING)
				TAXI_CANCEL_TIMERS(myTaxiData, TT_PUKING)
			ENDIF
			//CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_AI_WATCH_FOLLOW_STATUS: SWEET SPOT")
			IF theEscapeStruct.bIsTooClose
				IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) > TWEAK_TAXI_FOLLOW_TOO_CLOSE_DIST
					theEscapeStruct.bIsTooClose = FALSE
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Was Too Close, but now just right")
				ENDIF
			ENDIF
			
			IF theEscapeStruct.bIsOutofSight
				IF GET_DISTANCE_BETWEEN_ENTITIES(myTaxiData.viTaxi,theEscapeStruct.viCar) < TWEAK_TAXI_FOLLOW_TOO_FAR_DIST
				AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myTaxiData.viTaxi,theEscapeStruct.viCar)
					theEscapeStruct.bIsOutofSight = FALSE
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Was Too Far, but now just right")
				ENDIF
			ENDIF
			
			//Than in sweet spot
			IF NOT theEscapeStruct.bIsSweetSpot
				
				IF NOT theEscapeStruct.bIsOutofSight 
				AND NOT theEscapeStruct.bIsTooClose
					theEscapeStruct.bIsSweetSpot = TRUE
					
					TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_SWEET_SPOT)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS,TRUE)
					
					TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Sweet spot on")
				ENDIF
			ELSE
				IF myTaxiData.iTaxiOJ_BanterCount > myTaxiData.iTaxiOJ_NumDXVariations
					theEscapeStruct.bIsSweetSpotBanter = TRUE
				ENDIF

				IF NOT theEscapeStruct.bIsSweetSpotBanter
					AND NOT theEscapeStruct.bIsOutofSight 
					AND NOT theEscapeStruct.bIsTooClose
					
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_FOLLOW) > TAXI_CONST_TIME_SWEET_SPOT_DELAY
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
						TAXI_SET_CHASE_POSITION(myTaxiData,TAXI_SWEET_SPOT_BANTER)
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_FOLLOW_CAR_BARKS,TRUE)
						
						TAXI_RESET_TIMERS(myTaxiData, TT_FOLLOW)
						CDEBUG1LN(DEBUG_OJ_TAXI,"Sweet spot banter")
					ENDIF
				ENDIF
				
			ENDIF
				
			
		ENDIF//DISTANCE CHECK
	
	ENDIF	//VEHICLE_DRIVABLE
ENDPROC
				
//EOF
