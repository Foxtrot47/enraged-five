//=======================================================================================================================================
// Taxi_CutYouIn.sc
// Dev : John R. Diaz
/*
	•	
*/
//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//Includes------------------------------------------------------
USING "Taxi_Includes.sch"

ENUM TAXIOJ_GYN_BONUS
	TCYI_BONUS_LOST_COPS = 0,
	TCYI_BONUS_TOTAL
				
ENDENUM

BONUS_FIELD						bonusFieldCutYouIn[TCYI_BONUS_TOTAL]

//Local Variables-----------------------------------------------
CONST_FLOAT						TAXI_CONST_BONUS_TIME_TO_LOSE_COPS							180.0
CONST_FLOAT						TAXI_OJ_CONST_DIST_TO_SEE_AIRPORT							300.0 //350.0
CONST_FLOAT						TAXI_OJ_CONST_DIST_TO_SEE_JEWELRY							80.0
CONST_FLOAT						TAXI_OJ_CONST_DIST_FROM_PLAYER_TO_JEWELRY_STORE				10.0
CONST_FLOAT						TAXI_OJ_CONST_TIME_BEFORE_JEWELRY_STORE_REMINDER			5.0//20.0
CONST_FLOAT						TAXI_OJ_CONST_TIME_BEFORE_PASSENGER_SHOOTS_PLAYER			2.0
CONST_FLOAT						CONST_TAXI_TIME_TO_LOSE_POLICE_AVG							150.0 //181.0
CONST_FLOAT						CONST_TAXI_TIME_TO_LOSE_POLICE_AMAZING						75.0 //120.0 //150.0
CONST_FLOAT						TAXI_OJ_CONST_TIME_TO_RETURN_TO_CAB_AFTER_SHOOTING			8.0

CONST_FLOAT						TAXI_CYI_LOCAL_COP_REACT_DELAY								15.0
CONST_FLOAT						TAXI_CYI_LOCAL_COP_REACT_DELAY_EXT							25.0
CONST_FLOAT						TAXI_CYI_LOCAL_COP_REACT_DELAY_MAX							35.0

CONST_INT						CONST_TAXI_OJ_CYI_NUM_RESUME_INTERRUPT_LINES				5
CONST_INT						TAXI_CONST_BONUS_CASH_SMOOTH_ESCAPE							1000
CONST_INT						TAXI_OJ_CONST_WANTED_LEVEL_FOR_THIS_MISSION					2 //3
CONST_INT						TAXI_OJ_CONST_RETURN_REMINDER_SHOWN							1
CONST_INT						TAXI_OJ_CONST_WAIT_REMINDER_SHOWN							2
CONST_INT						TAXI_OJ_CONST_SHOW_ALLEY_CAM_HELP							3
CONST_INT						TAXI_OJ_CONST_CLEAR_ALLEY_CAM_HELP							4
CONST_INT						TAXI_OJ_CONST_TIME_BEFORE_WARP								40000

CONST_INT						TAXI_CONST_TIP_CASH_AMAZING									10000
CONST_INT						TAXI_CONST_TIP_CASH_AVERAGE									5000

//Custom Data
TaxiStruct 						myTaxiData

OBJECT_INDEX					oiBankNote

SEQUENCE_INDEX 					seqIndexTaxiTemp

//Vectors

VECTOR 							vTaxiJewelryNPCSpot = << 342.6987, -996.7599, 28.2438 >>  //LM was here, yet again 12/5/11
VECTOR 							vTaxiJewelreyInStore = <<321.10886, -996.42853, 28.20932>>//<< 328.4126, -995.5303, 28.2713 >>//<< 342.6987, -996.7599, 28.2438 >>
VECTOR 							vPassengerPt = << -1042.9464, -2689.5498, 12.7572 >>//<< -1032.4874, -2730.2161, 12.7572 >>
VECTOR 							vPassengerPickupPt = << -1044.8083, -2694.1379, 12.7280 >>

//VECTOR							vAlleyCamPos = <<349.45,-949.97,31.45>>
VECTOR							vAlleyCamLookAt = << 348.0136, -993.6165, 31.9655 >>//<<428.000,-2000.000,-177.81>>

//VECTOR							vTaxiJewelryStoreParkingSpot = << -600.6864, -310.3101, 33.8214 >>
//Bools
BOOL							bDropOffFound
BOOL							bTaxiShotsFired
BOOL							bBrokeLoyalty 	//bonus flag - auto as long as player never gets reminded to return to the player
BOOL							bTasked = FALSE 
BOOL							bPassengerIsGoingToBeArrested
BOOL							bIsAirportBlipped
BOOL							bIsHandoffDone
BOOL							bCreateCops
BOOL							bFinishedReminder
BOOL							bStartedReminder

BOOL							bClearCops

//Ints
INT 							iDebugThrottle = 1
INT								iReminderBitField
INT								iTipIndex = 0
INT								iArrestStateIndex = 0
INT								iSoundID_BurglarAlarm = -1
INT								iCopNum
INT								iCopLinesSaid

TEXT_LABEL_23 					sHandoffTrigger1 = "txm8_thank1M_2"
TEXT_LABEL_23 					sHandoffTrigger2 = "txm8_thank1T_2"
TEXT_LABEL_23 					sHandoffTrigger3 = "txm8_thank1F_2"
TEXT_LABEL_23 					sHandoffTrigger4 = "txm8_thank2M_2"
TEXT_LABEL_23 					sHandoffTrigger5 = "txm8_thank2T_2"
TEXT_LABEL_23 					sHandoffTrigger6 = "txm8_thank2F_2"
TEXT_LABEL_23 					sHandoffTrigger7 = "txm8_thank3M_2"
TEXT_LABEL_23 					sHandoffTrigger8 = "txm8_thank3T_2"
TEXT_LABEL_23 					sHandoffTrigger9 = "txm8_thank3F_2"
TEXT_LABEL_23 					sCurrentLine

AGGRO_ARGS 						aggroArgs 


SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize

ENUM TX_OJ_CYI_COP_PEDS
	TCP_COP_RIGHT =	0,
	TCP_COP_LEFT,
	
	TCP_NUM_COPS
ENDENUM
PED_INDEX						piMissionCops[TCP_NUM_COPS]
PED_INDEX						piDriverCop
PED_INDEX						piAmbientPedFlee[8]

SCENARIO_BLOCKING_INDEX			sbiAirportCopBlock

VEHICLE_INDEX					viCopCar
//VEHICLE_INDEX					viDummyCars[2]

REL_GROUP_HASH 					relPursuers

//Dialogue Queue----------------------------------------------------------------
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

ENUM TAXIOJ_STATES_PLAYER_NEAR_JEWELRY_STORE
	TAXIOJ_STATE_PNJS_REMINDER_WAIT = 0,
	TAXIOJ_STATE_PNJS_REMINDER_1,
	TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH,
	TAXIOJ_STATE_PNJS_CHECK_REMINDER_2,
	TAXIOJ_STATE_PNJS_REMINDER_2,
	TAXIOJ_STATE_PNJS_RESUME_1,
	TAXIOJ_STATE_PNJS_RESUME_2,
	TAXIOJ_STATE_PNJS_CHECK_REMINDER_3,
	TAXIOJ_STATE_PNJS_CHECK_FAIL,
	TAXIOJ_STATE_PNJS_FAIL,
	TAXIOJ_STATE_PNJS_CONCLUDE
ENDENUM
TAXIOJ_STATES_PLAYER_NEAR_JEWELRY_STORE	eJewelryStoreStates = TAXIOJ_STATE_PNJS_REMINDER_WAIT
TAXIOJ_STATES_PLAYER_NEAR_JEWELRY_STORE	eJewelryStoreLastState
//Floats

//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			taxiRideWidgets
	TEXT_LABEL_63				sDebugPoliceTimer
	BOOL 						bDebugTurnOnFreeRide = FALSE
	BOOL						bDebugShowPoliceTimer
	BOOL						bDebugActivatePoliceEnding
	BOOL						bDebugAddBag
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS	
	
#ENDIF

//FUNCTIONS------------------------------------------------------------------------------------------------------------

ENUM TAXI_OJ_CYI_CRITICAL_PTS
	TXCP_JSTORE = 0,
	TXCP_COP_SPAWNPT_1,
	TXCP_COP_SPAWNPT_2,
	TXCP_COP_WALKTOPT_1,
	TXCP_COP_WALKTOPT_2
	
ENDENUM
FUNC VECTOR GET_MISSION_CRITICAL_POINTS(INT iDropoffPt)
	SWITCH iDropoffPt
		CASE -1
			RETURN << 335.4227, -956.4223, 28.3588 >>
		BREAK
		//Jewelry Store
		CASE 0 
			RETURN << 349.9260, -958.3827, 28.3033 >>//<< 354.5381, -947.7332, 28.3590 >>
		BREAK
		
		//Cop SpwnPt #1
		CASE 1
			RETURN << -1020.5814, -2743.8420, 18.4049 >>
		BREAK
		
		//Cop SpwnPt #2 - CLOSEST TO BACK
		CASE 2
			RETURN << -1043.9473, -2748.6826, 20.3594 >> //<< -1051.2062, -2726.9028, 18.6043 >>
		BREAK
		
		//Cop WalkTo #1
		CASE 3
			RETURN << -1042.3907, -2731.6443, 19.1701 >>
		BREAK
		
		//Cop Walkto #2
		CASE 4
			RETURN << -1045.6769, -2730.1091, 19.1701 >>
		BREAK
		
		//Airport dropoff pt
		CASE 5
			RETURN << -1032.2947, -2731.4495, 19.0458 >>//<< -1040.0483, -2726.1892, 19.0676 >>//<< -1039.9452, -2728.2905, 19.0531 >>
		BREAK
		
		//Teleport passenger to side of cop car
		CASE 6
			RETURN << -1051.0447, -2722.9338, 19.1693 >>
		BREAK
		
		//Teleport Cop to just in front of cop car
		CASE 7
			RETURN << -1052.9054, -2723.3755, 19.1693 >>
		BREAK
		
		CASE 8
			RETURN << -1042.8121, -2743.0955, 20.3620 >>
		BREAK
		DEFAULT 
			RETURN << 354.5381, -947.7332, 28.3590 >>
		BREAK
	ENDSWITCH
	RETURN NULL_VECTOR()
ENDFUNC

/// PURPOSE: Call this to get the different camera positions for the csite cutscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_CYI_ALT_END_CS_CAM_POS(INT index)
	VECTOR vCamPos
	SWITCH index
		
		//By the entrance
		CASE 0
			 vCamPos = << -1040.4861, -2726.0576, 21.4491 >>
		BREAK
		
		//Shot of car coming
		CASE 4
			vCamPos = << -1044.8472, -2726.2390, 20.2197 >> //<< -1046.0826, -2724.3037, 20.2183 >>
		BREAK
		
		//Look away from car while door opens
		CASE 6
			vCamPos = << -1045.2518, -2733.1543, 23.9084 >>
		BREAK
		//Lookaway shot while peds are teleported
		CASE 5
			vCamPos = << -1053.6528, -2720.4998, 20.1615 >>
		BREAK
		
		//Int of cab
		CASE 1
			 vCamPos =  << -1042.0082, -2729.2322, 21.2022 >>
		BREAK
		
		//Right of Cab
		CASE 2
			 vCamPos =  << -1049.7991, -2725.0686, 20.7481 >>
		BREAK
		
		//Facing the cab
		CASE 3
			 vCamPos =  << -1043.8876, -2723.7827, 20.6874 >>
		BREAK
		
		//New shots
		//Facing passenger shot #1
		CASE 7
			vCamPos = << -1041.9169, -2746.2385, 22.2494 >>
		BREAK
		
		//Facing passenger from car
		CASE 8
			vCamPos = << -1041.8114, -2739.3528, 21.2173 >> //<< -1043.8667, -2738.1841, 20.3518 >>
			
		BREAK
		
		
	ENDSWITCH
	
	RETURN vCamPos
ENDFUNC
/// PURPOSE: Call this to get the different camera directions for the csite custscene
///    
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC VECTOR GET_TAXI_OJ_CYI_ALT_END_CS_CAM_ROT(INT index)
	VECTOR vCamDir
	SWITCH index
		
		//By the entrance
		CASE 0
			 vCamDir =  << -15.0777, 0.0000, 116.3223 >>
		BREAK
		
		Case 4
			 vCamDir =  << -0.0351, 0.0992, 32.6067 >> //<< -0.4138, 0.0000, 32.6908 >>
		BREAK
		
		CASE 5
			vCamDir = << 1.4039, -0.0000, -83.0298 >>
		BREAK
		
		//Look away from car while door opens
		CASE 6
			vCamDir = << -36.2279, -0.0000, -13.9491 >>
		BREAK
		//Int of cab
		CASE 1
			 vCamDir = << -7.6081, 0.0000, 58.9461 >>
		BREAK
		
		//Right of Cab
		CASE 2
			 vCamDir =  << -3.9964, -0.0000, -21.0031 >>
		BREAK
		
		//Facing the cab
		CASE 3
			 vCamDir = << -3.9964, 0.0000, 64.1173 >>
		BREAK
		
		//New shots
		//Facing passenger shot #1
		CASE 7
			vCamDir = << -9.2396, -0.0000, -4.2184 >>
		BREAK
		
		//Facing passenger from car
		CASE 8
			vCamDir = << -5.1729, 0.1144, -33.6602 >> //<< -1.2737, 0.0000, -13.2514 >>
			
		BREAK
		
	ENDSWITCH
	
	RETURN vCamDir
ENDFUNC

/// PURPOSE:A central location for all the vehicle recording names used in this mission.
///    
/// PARAMS:
///    iNum - Corresponds to which car recording we want
/// RETURNS:
///    
FUNC STRING GET_TAXI_OJ_CYI_VEHICLE_RECORDING_NAME(INT iNum)
	STRING sVehRecName
	SWITCH iNum
		
		CASE 1
			sVehRecName = "txm8_pol1_A"
		BREAK
	ENDSWITCH
	
	RETURN sVehRecName
ENDFUNC

ENUM TAXI_CYI_MODELS
	TCM_PASSENGER = 0,
	TCM_CAR_1,
	TCM_CAR_2,
	TCM_COP_MALE,
	TCM_COP_CAR
	
ENDENUM
FUNC MODEL_NAMES GET_TAXI_OJ_CYI_MODEL_NAMES(TAXI_CYI_MODELS index)

//Native Data
	MODEL_NAMES mnModel
	SWITCH index
		CASE TCM_PASSENGER
			mnModel = U_M_M_JewelThief
		BREAK
		
		CASE TCM_CAR_1
			mnModel = MANANA
		BREAK
		
		CASE TCM_CAR_2
			mnModel = ISSI2
		BREAK
		
		CASE TCM_COP_MALE
			mnModel = S_M_Y_Cop_01
		BREAK
		
		CASE TCM_COP_CAR
			mnModel = POLICE2
		BREAK
		DEFAULT
			mnModel = U_M_M_JewelThief
			SCRIPT_ASSERT("Passed invalid index to GET_TAXI_OJ_CYI_MODEL_NAMES()")
		BREAK
	ENDSWITCH
	
	RETURN mnModel
ENDFUNC

FUNC BOOL TAXICYI_IS_THIS_CONVERSATION_PLAYING(STRING sRoot)

	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Request all of our assets to be streamed, call this in prestreaming or init.
///    
PROC REQUEST_TAXI_ODDJOB_CYI_STREAMS_STAGE_01()
	//Load text and UI
	TAXI_INIT_SHARED_STREAMS()
	REQUEST_MODEL(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_PASSENGER))
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	REQUEST_SCRIPT_AUDIO_BANK("Alarms")
	
	REQUEST_ANIM_DICT("MOVE_P_M_ZERO_RUCKSACK")
	
//	REQUEST_MODEL(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_1))
//	REQUEST_MODEL(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_2))
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Requested Taxi ODDJOB CYI - Initial Assets")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_CYI_STREAMS_STAGE_01()
	
//	SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_1))
//	SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_2))
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Released Taxi ODDJOB CYI - Initial Assets")
ENDPROC

PROC REQUEST_TAXI_ODDJOB_CYI_STREAMS_END()
	REQUEST_ANIM_DICT("oddjobs@taxi@cyi")
	REQUEST_MODEL(P_BANKNOTE_S)
ENDPROC

PROC RELEASE_TAXI_ODDJOB_CYI_STREAMS_END()
	REMOVE_ANIM_DICT("oddjobs@taxi@cyi")
	SET_MODEL_AS_NO_LONGER_NEEDED(P_BANKNOTE_S)
ENDPROC

PROC REQUEST_TAXI_ODDJOB_CYI_STREAMS_STAGE_ALT_ENDING()

	//Only request these if player has taken too long
	REQUEST_MODEL(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE))
	REQUEST_MODEL(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_CAR))
	REQUEST_VEHICLE_RECORDING(000,GET_TAXI_OJ_CYI_VEHICLE_RECORDING_NAME(1))
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Requested Taxi ODDJOB CYI - Alt Ending Assets")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_CYI_STREAMS_STAGE_ALT_ENDING()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_CAR))
	REMOVE_VEHICLE_RECORDING(000,GET_TAXI_OJ_CYI_VEHICLE_RECORDING_NAME(1))
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"Released Taxi ODDJOB CYI - Alt Ending Assets")
	
ENDPROC

FUNC BOOL HAVE_TAXI_OJ_CYI_END_ASSETS_LOADED()
	
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@taxi@cyi")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading oddjobs@taxi@cyi",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(P_BANKNOTE_S)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading P_BANKNOTE_S",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

FUNC BOOL HAVE_TXM_CYI_ALT_ENDING_STREAMS_LOADED()

	IF NOT HAS_MODEL_LOADED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading S_M_Y_Cop_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_CAR))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading POLICE2",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	
	//All my vehicle recordings -------------------

	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(000,GET_TAXI_OJ_CYI_VEHICLE_RECORDING_NAME(1))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMING - Vehicle Recording Loading txm8_pol1_A000...",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

						
						

/// PURPOSE: Waits for our assets to load
///    
/// RETURNS: TRUE if everything has loaded correctly FALSE if there was a problem
///    
FUNC BOOL HAVE_TAXI_OJ_CYI_STAGE_01_ASSETS_LOADED()
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading shared assets",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF NOT HAS_MODEL_LOADED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_PASSENGER))
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading U_M_M_JewelThief",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("MOVE_P_M_ZERO_RUCKSACK")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading MOVE_P_M_ZERO_RUCKSACK",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE		
ENDFUNC

FUNC BOOL HAVE_TAXI_OJ_CYI_STAGE_02_ASSETS_LOADED()

//	IF NOT HAS_MODEL_LOADED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_1))
//		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading MANANA",iDebugThrottle)
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT HAS_MODEL_LOADED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_2))
//		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading ISSI2",iDebugThrottle)
//		RETURN FALSE
//	ENDIF
	

	RETURN TRUE		
ENDFUNC

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
		
	#ENDIF
	
	//Mission specific cleanup
	//Set roads disabled back to on
	TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(GET_MISSION_CRITICAL_POINTS(-1),TRUE)
	
	INT iCopIter
	REPEAT TCP_NUM_COPS iCopIter
		//Clean up cop 1
		IF NOT IS_ENTITY_DEAD(piMissionCops[iCopIter])
			SET_PED_AS_NO_LONGER_NEEDED(piMissionCops[iCopIter])
		ENDIF
	ENDREPEAT
	
//	//Misc cars to cleanup
//	IF NOT IS_ENTITY_DEAD(	viDummyCars[0])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(	viDummyCars[0])
//	ENDIF
//	
//	IF NOT IS_ENTITY_DEAD(	viDummyCars[1])
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(	viDummyCars[1])
//	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_PASSENGER))
	
	//Make sure soundID is valid
	ODDJOB_STOP_SOUND(iSoundID_BurglarAlarm)

	DISTANT_COP_CAR_SIRENS(FALSE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	
	STOP_AUDIO_SCENE("TAXI_CUT_U_IN")
	
	REMOVE_ANIM_DICT("MOVE_P_M_ZERO_RUCKSACK")
	
	RELEASE_TAXI_ODDJOB_CYI_STREAMS_STAGE_01()
	RELEASE_TAXI_ODDJOB_CYI_STREAMS_STAGE_ALT_ENDING()
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	//SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(AP1_02_DOOR_L, << -1042.22, -2747.98, 21.97 >>, TRUE, 0)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiAirportCopBlock)
	
	IF DOES_ENTITY_EXIST(oiBankNote)
		DELETE_OBJECT(oiBankNote)
	ENDIF
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES(TAXI_MISSION_TYPES thisMissionType = TXM_07_CUTYOUIN)

	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,thisMissionType)
	
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	SET_TAXI_TIP_CUTOFFS(myTaxiData,2,8)
	
	//Airport
	myTaxiData.vTaxiOJ_WarpPtPickup = << -1069.2772, -2666.1558, 12.6650 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 202.9
	
	//Jewelry Store
	myTaxiData.vTaxiOJ_WarpPtDropoff = << 323.9376, -956.7155, 28.3460 >>  //LM chizange for jewelry store downtown
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 262.6098
	
	myTaxiData.vTaxiOJ_PassengerGoToPt = << -1037.71118, -2748.46313, 20.36420 >>
		
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
	
			taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Cut You In")
			INIT_ODDJOB_TAXI_WIDGETS()
			
			ADD_WIDGET_STRING("Police Escape Debug")
				ADD_WIDGET_BOOL("Show Police escape timer",bDebugShowPoliceTimer) 
				ADD_WIDGET_BOOL("Activate Police Ending", bDebugActivatePoliceEnding)
			
			ADD_WIDGET_STRING("Passenger Debug")
				ADD_WIDGET_BOOL("Add bag",bDebugAddBag)
				
			ADD_WIDGET_STRING("Alley Cam Rot")	
				ADD_WIDGET_FLOAT_SLIDER("Alley Cam look X", vAlleyCamLookAt.x, -2000.0, 2000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Alley Cam look Y", vAlleyCamLookAt.y, -2000.0, 2000.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Alley Cam look Z", vAlleyCamLookAt.z, -2000.0, 2000.0, 0.1)		
				
		STOP_WIDGET_GROUP()
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Cut You In ~~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()
	
	UPDATE_ZVOLUME_WIDGETS()
	
	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugAddBag
		IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
			SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger,PED_COMP_SPECIAL,1,0)
			bDebugAddBag = FALSE
		ENDIF
	ENDIF
	
	IF bDebugShowPoliceTimer
		IF myTaxiData.tTaxiOJ_RideState = TRS_ESCAPE_POLICE
		OR myTaxiData.tTaxiOJ_RideState = TRS_POLICE_ESCAPED
			IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_POLICE)
			
				sDebugPoliceTimer= "Police Escape Time: "
				sDebugPoliceTimer += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE))
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > TAXI_CONST_BONUS_TIME_TO_LOSE_COPS
					DRAW_DEBUG_TEXT_2D(sDebugPoliceTimer,<<0.05,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,0.0>>,255,0,0)
				ELSE
					DRAW_DEBUG_TEXT_2D(sDebugPoliceTimer,<<0.05,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,0.0>>)
				ENDIF
			ENDIF
		ELSE
			//Show if bonus has been unlocked or not
			IF IS_BIT_SET(myTaxiData.iTaxiOJ_CashBonusInfo[ENUM_TO_INT(TCYI_BONUS_LOST_COPS)].status, BONUS_STATUS_AWARDED)
				sDebugPoliceTimer= "Private Eye Bonus UNLOCKED "
				DRAW_DEBUG_TEXT_2D(sDebugPoliceTimer,<<0.05,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,0.0>>)
			ELSE
				sDebugPoliceTimer= "Private Eye Bonus LOCKED "
				DRAW_DEBUG_TEXT_2D(sDebugPoliceTimer,<<0.05,TAXI_UI_LOWEST_DEBUG_Y_ABOVE_MINI_MAP,0.0>>,255,0,0)
			ENDIF
		ENDIF
	ENDIF
	
	IF bDebugActivatePoliceEnding
		bPassengerIsGoingToBeArrested = TRUE
		
		bDebugActivatePoliceEnding = FALSE
	ENDIF
	
ENDPROC
#ENDIF

FUNC BOOL IS_THIS_THE_HANDOFF_LINE()
	sCurrentLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	IF ARE_STRINGS_EQUAL(sHandoffTrigger1, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger2, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger3, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger4, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger5, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger6, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger7, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger8, sCurrentLine)
	OR ARE_STRINGS_EQUAL(sHandoffTrigger9, sCurrentLine)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//This handles if the player leaves the taxi and is too close to Vincent during the jewelry store sequence
PROC TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX()
	IF g_bDebug
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE",iDebugThrottle)
	ENDIF
	IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
		
		SWITCH eJewelryStoreStates
			CASE TAXIOJ_STATE_PNJS_REMINDER_WAIT
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, vTaxiJewelryNPCSpot) < 16
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE: TAXIOJ_STATE_PNJS_REMINDER_WAIT - ped is < 16m from his destination start caring about aggro = ",GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, vTaxiJewelryNPCSpot))
//					GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 50)
					eJewelryStoreStates = TAXIOJ_STATE_PNJS_REMINDER_1
				ENDIF
			BREAK
			CASE TAXIOJ_STATE_PNJS_REMINDER_1
				//Player is bothering Vince on his way to the heist
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) < TAXI_OJ_CONST_DIST_FROM_PLAYER_TO_JEWELRY_STORE
					
					GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 50, TRUE)
					CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
					OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)						
						//Stop 1
						//TASK_TURN_PED_TO_FACE_ENTITY(NULL, myTaxiData.piTaxiPlayer)
						//TASK_LOOK_AT_ENTITY(NULL,myTaxiData.piTaxiPlayer,TAXI_LOOK_AT_TIME,SLF_FAST_TURN_RATE)
						TASK_AIM_GUN_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
					CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
					CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
					TAXI_RESET_TIMERS(myTaxiData,  TT_PENALTY)
					eJewelryStoreStates = TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH
				ENDIF
			BREAK
			
			CASE TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PENALTY) > 1.5
					IF eJewelryStoreLastState <> TAXIOJ_STATE_PNJS_RESUME_1
						//Turn to face the player and stop in place til player leaves
						
						IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPlayer)
							IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
								IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
									CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH - Play TAXI_DI_CYI_DONT_FOLLOW_ME")
									SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_CYI_DONT_FOLLOW_ME,TRUE)		
								ELSE
									CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH - Play TAXI_DI_CYI_GET_BACK_TO_CAR")
									SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_CYI_GET_BACK_TO_CAR, TRUE, TRUE)		
								ENDIF
							ENDIF
						ENDIF
						
						bFinishedReminder= FALSE
						bBrokeLoyalty = TRUE
						TAXI_RESET_TIMERS(myTaxiData,  TT_PENALTY)
						TAXI_PAUSE_TIMER(myTaxiData, TT_DROPOFF)
						
						eJewelryStoreStates = TAXIOJ_STATE_PNJS_CHECK_REMINDER_2
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH - eJewelryStoreStates = TAXIOJ_STATE_PNJS_CHECK_REMINDER_2")
					ELSE
						TAXI_CANCEL_TIMERS(myTaxiData, TT_PENALTY)
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_REMINDER_1_SPEECH - eJewelryStoreStates = TAXIOJ_STATE_PNJS_FAIL")
						eJewelryStoreStates = TAXIOJ_STATE_PNJS_FAIL
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXIOJ_STATE_PNJS_CHECK_REMINDER_2
				//If player is cooperating
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) >= TAXI_OJ_CONST_DIST_FROM_PLAYER_TO_JEWELRY_STORE
					eJewelryStoreStates = TAXIOJ_STATE_PNJS_RESUME_1
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_CHECK_REMINDER_2 - eJewelryStoreStates = TAXIOJ_STATE_PNJS_RESUME_1")			
				//Otherwise remind him one last time
				ELIF bStartedReminder
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING() 
						IF NOT bFinishedReminder
							TAXI_RESET_TIMERS(myTaxiData,  TT_PENALTY)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_CHECK_REMINDER_2 starting penalty timer after speech is said")
							bFinishedReminder = TRUE
						ELSE
							IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PENALTY) > TAXI_OJ_CONST_TIME_BEFORE_JEWELRY_STORE_REMINDER
								TAXI_CANCEL_TIMERS(myTaxiData, TT_PENALTY)
								eJewelryStoreStates = TAXIOJ_STATE_PNJS_FAIL
								CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_CHECK_REMINDER_2 - eJewelryStoreStates = TAXIOJ_STATE_PNJS_FAIL")	
							ENDIF
						ENDIF
					ENDIF
				ELIF NOT bStartedReminder
					IF TAXICYI_IS_THIS_CONVERSATION_PLAYING("txm8_lvMe1")
					OR TAXICYI_IS_THIS_CONVERSATION_PLAYING("txm8_lvMe2")
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_CHECK_REMINDER_2 - warning dialog played, setting flag")
						bStartedReminder = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE TAXIOJ_STATE_PNJS_RESUME_1
				TAXI_UNPAUSE_TIMER(myTaxiData, TT_DROPOFF)
				
				IF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger,SCRIPT_TASK_PERFORM_SEQUENCE) <> FINISHED_TASK
					CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
					OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)
						//Send back to store
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTaxiJewelryNPCSpot,PEDMOVEBLENDRATIO_WALK, TAXI_OJ_CONST_TIME_BEFORE_WARP)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 337.3356, -997.7456, 28.1318 >>,PEDMOVEBLENDRATIO_SPRINT, TAXI_OJ_CONST_TIME_BEFORE_WARP)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vTaxiJewelreyInStore,PEDMOVEBLENDRATIO_SPRINT, TAXI_OJ_CONST_TIME_BEFORE_WARP)
						
					CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
					CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_RESUME_1 - eJewelryStoreStates = TAXIOJ_STATE_PNJS_RESUME_1")
					eJewelryStoreLastState = TAXIOJ_STATE_PNJS_RESUME_1
					eJewelryStoreStates = TAXIOJ_STATE_PNJS_REMINDER_WAIT
				ENDIF
			BREAK
						
			CASE TAXIOJ_STATE_PNJS_FAIL
				IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_PENALTY)
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_PENALTY) > TAXI_OJ_CONST_TIME_BEFORE_PASSENGER_SHOOTS_PLAYER
						TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)					
						
						//Fail. He should only shoot the player if he'd been hurt.
						CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
						OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)
//							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID())
//								TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(),-1,FIRING_TYPE_CONTINUOUS)					
//							ELSE
//								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300.0, -1)
//							ENDIF
							CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_FAIL - Shoot!")
							TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(),-1,FIRING_TYPE_CONTINUOUS)					
						CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
						CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
						
						SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger,TRUE)
						bTasked = TRUE
						
						IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						AND GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) > 0
							
							SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
							SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff,FALSE)
							
							CLEAR_THIS_PRINT("TAXI_OBJ_CYI_1B")
						ENDIF
						
						eJewelryStoreStates = TAXIOJ_STATE_PNJS_CONCLUDE
						CDEBUG1LN(DEBUG_OJ_TAXI,"eJewelryStoreStates = TAXIOJ_STATE_PNJS_CONCLUDE")
					ENDIF
				ELSE
					TAXI_RESET_TIMERS(myTaxiData,  TT_PENALTY)
					
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPlayer)
						IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
							IF IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
								CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_FAIL - Play TAXI_DI_CYI_DONT_FOLLOW_ME")
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_CYI_DONT_FOLLOW_ME,TRUE)
							ELSE
								CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX: TAXIOJ_STATE_PNJS_FAIL - Play TAXI_DI_CYI_GET_BACK_TO_CAR")
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_CYI_GET_BACK_TO_CAR,TRUE,TRUE)					
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			BREAK
			
			CASE TAXIOJ_STATE_PNJS_CONCLUDE
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 2.0
					TAXI_SET_FAIL(myTaxiData, "Passenger shoots player", TFS_TIME_EXPIRED)
				ENDIF
			BREAK
			
			
		ENDSWITCH
	ENDIF
ENDPROC

PROC TAXI_OJ_CYI_SET_TIP_AND_EXCITEMENT_TO_CHECK()

	//Tip Bits
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	
	
	//EXCITEMENT BITS
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)					
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)	
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
					
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
ENDPROC
//// PURPOSE: The tip for this mission is based primarily off of the time it took you to lose the police
 ///    
PROC EVALUATE_TIME_TAKEN_TO_LOSE_POLICE_IN_TAXI_CYI()
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"CYI Time taken to lose cops = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST))
	
	//Lost cops super fast
	IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) < CONST_TAXI_TIME_TO_LOSE_POLICE_AMAZING
	AND NOT bPassengerIsGoingToBeArrested
		
		myTaxiData.iTaxiOJ_CashTipToAdd += 12
		//myTaxiData.iTaxiOJ_CashTip += 10
		SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_TO_AIRPORT_AMAZING,TRUE,FALSE,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"CYI Taxi Tip for losing cops = Amazing")
	
	//Took mad long to lose the cops
	ELIF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_RIDETODEST) > CONST_TAXI_TIME_TO_LOSE_POLICE_AVG
	OR bPassengerIsGoingToBeArrested
		myTaxiData.iTaxiOJ_CashTipToAdd = -10
		SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_TO_AIRPORT_ASS,TRUE,FALSE,TRUE)
		
		bPassengerIsGoingToBeArrested = TRUE
		REQUEST_TAXI_ODDJOB_CYI_STREAMS_STAGE_ALT_ENDING()
		
		CDEBUG1LN(DEBUG_OJ_TAXI,"CYI Taxi Tip for losing cops = Ass")
	
	//Average
	ELSE
		myTaxiData.iTaxiOJ_CashTipToAdd += 5
		SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_TO_AIRPORT_AVG,TRUE,FALSE,TRUE)
		CDEBUG1LN(DEBUG_OJ_TAXI,"CYI Taxi Tip for losing cops = Average")

	ENDIF

ENDPROC
/// PURPOSE: Cut You In is the one mission that has a different tipping standard, as such it has it's own convert tip to cash func
///    
/// PARAMS:
///    myTaxiData - global taxi data
PROC CYI_CONVERT_TAXI_TIP_TO_CASH()

	IF myTaxiData.iTaxiOJ_CashTip >= myTaxiData.iTaxiOJ_CashTipAmazing 
		myTaxiData.iTaxiOJ_CashTip = TAXI_CONST_TIP_CASH_AMAZING
		
	ELIF myTaxiData.iTaxiOJ_CashTip <= 0
		myTaxiData.iTaxiOJ_CashTip = 0
	
	ELSE
		myTaxiData.iTaxiOJ_CashTip = TAXI_CONST_TIP_CASH_AVERAGE
	ENDIF
	
			
ENDPROC

PROC SAFE_TASK_ENTER_VEHICLE(PED_INDEX &eEntity, VEHICLE_INDEX &viVehicle, VEHICLE_SEAT vSeat = VS_DRIVER, FLOAT MoveBlendRatio = PEDMOVEBLENDRATIO_WALK, ENTER_EXIT_VEHICLE_FLAGS iFlags = ECF_RESUME_IF_INTERRUPTED )
    
    SEQUENCE_INDEX siTemp
    
    IF NOT IS_ENTITY_DEAD(eEntity)
        IF IS_VEHICLE_DRIVEABLE(viVehicle)
            CLEAR_SEQUENCE_TASK(siTemp)
            OPEN_SEQUENCE_TASK(siTemp)
        
                CLEAR_PED_TASKS_IMMEDIATELY(eEntity)
                TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(1000,2000))
                TASK_ENTER_VEHICLE(NULL,viVehicle,TAXI_OJ_CONST_TIME_BEFORE_WARP,vSeat,MoveBlendRatio,iFlags) 
                
            CLOSE_SEQUENCE_TASK(siTemp)
            TASK_PERFORM_SEQUENCE(eEntity, siTemp)
            CLEAR_SEQUENCE_TASK(siTemp)
        ENDIF
    ENDIF
ENDPROC

FUNC STRING GET_PLAYER_HAND_OVER_ANIM_STRING(VEHICLE_SEAT eSeat)
	
	SWITCH eSeat
		CASE VS_FRONT_RIGHT		RETURN "std_hand_off_ps_driver"
		CASE VS_BACK_RIGHT		RETURN "std_hand_off_rps_driver"
		CASE VS_BACK_LEFT		RETURN "std_hand_off_rds_driver"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PASSENGER_HAND_OVER_ANIM_STRING(VEHICLE_SEAT eSeat)
	
	SWITCH eSeat
		CASE VS_FRONT_RIGHT		RETURN "std_hand_off_ps_passenger"
		CASE VS_BACK_RIGHT		RETURN "std_hand_off_rps_passenger"
		CASE VS_BACK_LEFT		RETURN "std_hand_off_rds_passenger"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_MONEY_HAND_OVER_ANIM_STRING(VEHICLE_SEAT eSeat)
	
	SWITCH eSeat
		CASE VS_FRONT_RIGHT		RETURN "std_hand_off_ps_money"
		CASE VS_BACK_RIGHT		RETURN "std_hand_off_rps_money"
		CASE VS_BACK_LEFT		RETURN "std_hand_off_rds_money"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE: Handles the alternate dropoff scenario for the passenger
///    
FUNC BOOL UPDATE_TAXI_PASSENGER_ARREST_FOR_CYI()
	SEQUENCE_INDEX tempSequenceIndex
	VECTOR vTemp
	// TODO: REMOVE VTEMP AFTER THIS SCENE HAS BEEN POLISHED
	vTemp = vTemp
	
	IF iArrestStateIndex > 2
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	
	SWITCH iArrestStateIndex
		
		//Turn on sirens
		//Disable random cops from spawning
		//Tell passenger where to go
		CASE - 1
			SCRIPT_ASSERT("TEST ARREST")	
			iArrestStateIndex++
		BREAK
		
		CASE 0
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			SET_CREATE_RANDOM_COPS(FALSE)
			
			CLEAR_SEQUENCE_TASK(tempSequenceIndex)
			OPEN_SEQUENCE_TASK(tempSequenceIndex)
				TASK_LEAVE_ANY_VEHICLE(NULL,0,ECF_DONT_CLOSE_DOOR)
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-1041.8997, -2745.1133, 20.3644>>,1.25)
			CLOSE_SEQUENCE_TASK(tempSequenceIndex)
			TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
			CLEAR_SEQUENCE_TASK(tempSequenceIndex)
			
			viCopCar = CREATE_VEHICLE( GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_CAR), << -1067.1459, -2579.6316, 19.7762 >>, 150 )
			
			piDriverCop = CREATE_PED_INSIDE_VEHICLE(viCopCar,PEDTYPE_MISSION,GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE))
			
			TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
			
			CDEBUG1LN(DEBUG_OJ_TAXI,"going to arrest state 1")	
			iArrestStateIndex++
		BREAK
		
		//Spawn both cops have them run to their FREEZE points
		CASE 1
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 0.5
				
				IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
					IF NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_RIGHT])
						TASK_LOOK_AT_ENTITY(piMissionCops[TCP_COP_RIGHT], myTaxiData.piTaxiPassenger, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						ADD_PED_FOR_DIALOGUE(myTaxiData.tTaxiOJ_Convo,4,piMissionCops[TCP_COP_RIGHT],"TaxiOJCop1")
						SET_AMBIENT_VOICE_NAME(piMissionCops[TCP_COP_RIGHT], "TaxiOJCop1")
					ENDIF
					IF NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_LEFT])
						TASK_LOOK_AT_ENTITY(piMissionCops[TCP_COP_LEFT], myTaxiData.piTaxiPassenger, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					ENDIF
				ENDIF
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Police tasked to look at the guy")
			ENDIF
			
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_RIGHT])
			AND NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_LEFT])
			AND NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				IF IS_ENTITY_IN_ANGLED_AREA( myTaxiData.piTaxiPassenger,<<-1032.675171,-2738.775146,19.169693>>, <<-1041.510132,-2733.615234,21.545042>>, 2.0)
					
					// tasking the arrest
					
					OPEN_SEQUENCE_TASK(tempSequenceIndex)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,myTaxiData.piTaxiPassenger, myTaxiData.piTaxiPassenger, PEDMOVEBLENDRATIO_WALK, FALSE, 4.0)
						TASK_AIM_GUN_AT_ENTITY(NULL,myTaxiData.piTaxiPassenger,-1)
					CLOSE_SEQUENCE_TASK(tempSequenceIndex)
					TASK_PERFORM_SEQUENCE(piMissionCops[TCP_COP_RIGHT], tempSequenceIndex)
					CLEAR_SEQUENCE_TASK(tempSequenceIndex)
					
					SET_PED_KEEP_TASK(piMissionCops[TCP_COP_RIGHT],TRUE)
					
					OPEN_SEQUENCE_TASK(tempSequenceIndex)
						TASK_STAND_STILL(NULL, 1500)
						TASK_AIM_GUN_AT_ENTITY(NULL,myTaxiData.piTaxiPassenger,2000)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(NULL,myTaxiData.piTaxiPassenger, myTaxiData.piTaxiPassenger, PEDMOVEBLENDRATIO_WALK, FALSE, 4.0)
						TASK_AIM_GUN_AT_ENTITY(NULL,myTaxiData.piTaxiPassenger,-1)
					CLOSE_SEQUENCE_TASK(tempSequenceIndex)
					TASK_PERFORM_SEQUENCE(piMissionCops[TCP_COP_LEFT], tempSequenceIndex)
					CLEAR_SEQUENCE_TASK(tempSequenceIndex)
					
					SET_PED_KEEP_TASK(piMissionCops[TCP_COP_LEFT],TRUE)
					
					//Task passenger to freeze
					OPEN_SEQUENCE_TASK(tempSequenceIndex)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,piMissionCops[TCP_COP_RIGHT],3)
						TASK_LOOK_AT_ENTITY(NULL, piMissionCops[TCP_COP_RIGHT], -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						TASK_STAND_STILL(NULL, 1000)
						TASK_HANDS_UP(NULL,-1)
					CLOSE_SEQUENCE_TASK(tempSequenceIndex)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempSequenceIndex)
					CLEAR_SEQUENCE_TASK(tempSequenceIndex)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_POLICE_ARREST,TRUE)
					
					INT iIndex
					iCopNum = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), piAmbientPedFlee)
					REPEAT iCopNum iIndex
						IF piAmbientPedFlee[iIndex] <> PLAYER_PED_ID()
						AND piAmbientPedFlee[iIndex] <> myTaxiData.piTaxiPassenger
						AND piAmbientPedFlee[iIndex] <> piMissionCops[TCP_COP_LEFT]
						AND piAmbientPedFlee[iIndex] <> piMissionCops[TCP_COP_RIGHT]
						AND piAmbientPedFlee[iIndex] <> piDriverCop
						AND NOT IS_ENTITY_A_MISSION_ENTITY(piAmbientPedFlee[iIndex])
							SET_ENTITY_AS_MISSION_ENTITY( piAmbientPedFlee[iIndex], TRUE, TRUE )
							CDEBUG1LN(DEBUG_OJ_TAXI,"setting ped number ", iIndex, " to flee!")
							TASK_SMART_FLEE_PED( piAmbientPedFlee[iIndex], myTaxiData.piTaxiPassenger, 100.0, -1 )
						ENDIF
					ENDREPEAT
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
					
					iArrestStateIndex++
					CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - HANDS UP")
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 5.0
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relPursuers, RELGROUPHASH_COP)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, relPursuers)
				
				WEAPON_TYPE currentWeapon
				GET_CURRENT_PED_WEAPON(myTaxiData.piTaxiPassenger, currentWeapon)
				IF (currentWeapon = WEAPONTYPE_UNARMED)
					GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, INFINITE_AMMO)
				ENDIF
				
				SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_AGGRESSIVE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger, CA_USE_PROXIMITY_FIRING_RATE, TRUE) 
				
				SET_PED_COMBAT_MOVEMENT(myTaxiData.piTaxiPassenger, CM_WILLADVANCE)
				SET_PED_CAN_EVASIVE_DIVE (myTaxiData.piTaxiPassenger, TRUE)
				
				SET_PED_ACCURACY(myTaxiData.piTaxiPassenger, 80)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(myTaxiData.piTaxiPassenger, 50)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
					
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - next shot")
			ENDIF
			
			IF ( HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piMissionCops[TCP_COP_RIGHT], PLAYER_PED_ID()) 
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piMissionCops[TCP_COP_LEFT], PLAYER_PED_ID()) )
			AND NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				CDEBUG1LN(DEBUG_OJ_TAXI,"player intervened, passenger tries to flee")
				TASK_SMART_FLEE_PED( myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), 300.0, -1 )
				TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - next shot")
			ENDIF
			
		BREAK
		
		//Aim your gun and tell the passenger to freeze
		CASE 4
			// 
			IF IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
				
				IF NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_RIGHT])
					CLEAR_PED_TASKS(piMissionCops[TCP_COP_RIGHT])
					IF DOES_SCENARIO_EXIST_IN_AREA(<<-1036.501953,-2742.985596,20.169693>>, 5.0, TRUE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(piMissionCops[TCP_COP_RIGHT], <<-1036.501953,-2742.985596,20.169693>>, 1)
						CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - scenario exists, using it")
					ELSE
						TASK_START_SCENARIO_IN_PLACE(piMissionCops[TCP_COP_RIGHT], "WORLD_HUMAN_GUARD_STAND_ARMY")
						CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - scenario does not exist, forcing it")
					ENDIF
					SET_PED_KEEP_TASK(piMissionCops[TCP_COP_RIGHT], TRUE)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - right cop was tasked")
				ENDIF
				IF NOT IS_ENTITY_DEAD(piMissionCops[TCP_COP_LEFT])
					CLEAR_PED_TASKS(piMissionCops[TCP_COP_LEFT])
					
					vTemp = GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, FALSE)
					
					CLEAR_SEQUENCE_TASK(tempSequenceIndex)
					OPEN_SEQUENCE_TASK(tempSequenceIndex)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, vTemp, vTemp, PEDMOVE_WALK, FALSE, 3.0)
						TASK_AIM_GUN_AT_COORD(NULL, vTemp, 500)
						TASK_LOOK_AT_COORD(NULL, vTemp, -1, SLF_SLOW_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					CLOSE_SEQUENCE_TASK(tempSequenceIndex)
					TASK_PERFORM_SEQUENCE(piMissionCops[TCP_COP_LEFT], tempSequenceIndex)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - left cop was tasked")
					
					SET_PED_KEEP_TASK(piMissionCops[TCP_COP_RIGHT], TRUE)
				ENDIF
				
				RETURN TRUE
				
			ELSE
				
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 6.0
					SET_NEXT_TAXI_SPEECH(myTaxiData, TAXI_DI_SHOOT, TRUE, FALSE, TRUE)
					TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
				ENDIF
				
				IF IS_ENTITY_DEAD(piMissionCops[TCP_COP_RIGHT])
				AND IS_ENTITY_DEAD(piMissionCops[TCP_COP_LEFT])
					CDEBUG1LN(DEBUG_OJ_TAXI,"one of the cops died, passenger will bone out")
					CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
					TASK_SMART_FLEE_PED( myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), 300.0, -1 )
				ENDIF
				
				IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > 50
					CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Passenger has fled, returning true")
					RETURN TRUE
				ENDIF
			
			ENDIF
			TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
			iArrestStateIndex++
			CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Police Created = 5")
		BREAK
		
		//Look away while door opens
		CASE 5
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 2.0
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Police Created = 5")
			ENDIF
						
		BREAK
		
		//Passenger is being escorted to the back car viCopCar
		CASE 6
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 2.0
				
				vTemp = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viCopCar, GET_TAXI_OJ_CYI_ALT_END_CS_CAM_POS(5))
				PRINTLN("cam offset from vehicle = ", vTemp)
				
				TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Police Created = 5")
			ENDIF
						
		BREAK
		
		CASE 7
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) >= 1.0
				
				iArrestStateIndex++
				CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Teleporting everyone")
			ENDIF
		BREAK
		
		//Once cop is in the car, tell everyone else to get
		CASE 8
			
			iArrestStateIndex++
			CDEBUG1LN(DEBUG_OJ_TAXI,"ALT_END - Everybody should be in car")
			
		BREAK
		
		
		//Once last cop is in car, have everyone get out of there
		CASE 9
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_CYI_LINES()

	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)
	
	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
	
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			
			//Make sure if player interrupts the initial line, that the objective prints anyway.
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TX_OBJ_CYI_DO")
						OR DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been  assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO J Store has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			//Trigger Line #1 -------------------------------------------------
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					
					//Start the mission timer here
					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: CASE 0 - CALL TAXI_DI_BANTER")
				ENDIF
			BREAK
			
			//Trigger Line #2--------------------------------------------------
			CASE 2
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 2.0
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: CASE 1 - CALL TAXI_DI_BANTER_2")
				ENDIF
			BREAK
			
			//Trigger Line #3--------------------------------------------------
			CASE 3
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 2.0
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_3,TRUE)
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: CASE 2 - CALL TAXI_DI_BANTER_3")
				ENDIF
			BREAK
			
			//Trigger Line #5-------------------------------------------------
			CASE 5
				//Make sure player isn't wanted
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
					IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GO_AIRPORT
					AND NOT IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)
					AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					AND NOT bIsAirportBlipped
					AND myTaxiData.tTaxiOJ_RideState > TRS_DRIVING_PASSENGER
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > 2.0
							//Trigger Airport Obj
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GO_AIRPORT,TRUE,FALSE,TRUE)
							TAXI_RESET_TIMERS(myTaxiData,  TT_DIALOGUE,TAXI_DX_DELAY)
							
							IF g_bDebug
								SCRIPT_ASSERT("Setting airport objective on")
							ENDIF
						ENDIF
					ELSE
						IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GO_AIRPORT
							CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: airport obj is not speech index")
						ENDIF
						IF NOT IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: obj not being displayed")
						ENDIF
						IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
							CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: interruption line not playing")
						ENDIF
						IF NOT bIsAirportBlipped
							CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: airport not blipped")
						ENDIF
						IF myTaxiData.tTaxiOJ_RideState > TRS_DRIVING_PASSENGER
							CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: ride state > driving passenger")
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: wanted level not < 1")
				ENDIF
			BREAK	

			CASE 6
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > 4.0
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_AIRPORT_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
				AND NOT IS_VIP_TAXI_OBJ_BEING_DISPLAYED(myTaxiData)
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_AIRPORT_BANTER,TRUE)
					IF g_bDebug
						SCRIPT_ASSERT("Triggering airport banter")
					ENDIF
						
				ENDIF
			
			BREAK
			
			CASE 7
				//3/2/12 - JD- Turning this off because it triggered unexpectedly
				//TAXI_RADIO_STATION_TURN_ON(myTaxiData,g_bDebug)
				tTaxiOJ_DQ_Data.iCurrentDQLine++
			BREAK
		ENDSWITCH
	ENDIF
	//CDEBUG1LN(DEBUG_OJ_TAXI,"TRIGGER_TAXI_QUEUE_CYI_LINES: Call PROCESS_IMPORTANT_DIALOGUE_Q")
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)
ENDPROC

/*

oooo     oooo      o      ooooo oooo   oooo 
 8888o   888      888      888   8888o  88  
 88 888o8 88     8  88     888   88 888o88  
 88  888  88    8oooo88    888   88   8888  
o88o  8  o88o o88o  o888o o888o o88o    88  
                                           
*/
PROC MAIN_TAXI_OJ_CUTYOUIN()

	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		IF TAXI_HANDLE_FAIL(myTaxiData,bTasked)
			Script_Failed()
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		TAXI_OJ_MAINTAIN_GLOBAL_GATES(myTaxiData)
		
		//Gobal taxi updates that all taxi missions run every frame
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
		IF myTaxiData.tTaxiOJ_RideState != TRS_SPECIAL_ENDING
		AND myTaxiData.tTaxiOJ_RideState != TRS_CLEANUP
			PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		ENDIF
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION
			
			IF myTaxiData.tTaxiOJ_RideState = TRS_WAIT_FOR_PASSENGER
			OR myTaxiData.tTaxiOJ_RideState = TRS_ESCAPE_POLICE
			OR myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			OR myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
				TAXI_ODDJOB_HANDLE_TAXI_BLIPPING(myTaxiData)
			ENDIF
			
			//Trigger Lines
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				TRIGGER_TAXI_QUEUE_CYI_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF

		ENDIF
		//Track Driver progress
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
		OR myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF	
		IF myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP
		OR myTaxiData.tTaxiOJ_RideState = TRS_WAIT_FOR_PASSENGER	
			IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
			AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
				CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vAlleyCamLookAt)
			ELSE
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
			ENDIF
			//GOD TEXT
			IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
			AND SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
				IF NOT IS_BIT_SET(iReminderBitField, TAXI_OJ_CONST_SHOW_ALLEY_CAM_HELP)
					PRINT_HELP("TAXI_HNT_CAM")
					SET_BIT(iReminderBitField, TAXI_OJ_CONST_SHOW_ALLEY_CAM_HELP)
					IF IS_BIT_SET(iReminderBitField, TAXI_OJ_CONST_CLEAR_ALLEY_CAM_HELP)
						CLEAR_BIT(iReminderBitField, TAXI_OJ_CONST_CLEAR_ALLEY_CAM_HELP)
					ENDIF
				ENDIF
			ELIF NOT IS_BIT_SET(iReminderBitField, TAXI_OJ_CONST_CLEAR_ALLEY_CAM_HELP)
				CLEAR_HELP(TRUE)
				SET_BIT(iReminderBitField, TAXI_OJ_CONST_CLEAR_ALLEY_CAM_HELP)
			ENDIF
		ENDIF
					
		//================================================================================================
	
		SWITCH myTaxiData.tTaxiOJ_RideState
		
			/*PRESTREAM and Check that the player and taxi are good to go----------------------------------------
						
ooooo oooo   oooo ooooo ooooooooooo 
 888   8888o  88   888  88  888  88 
 888   88 888o88   888      888     
 888   88   8888   888      888     
o888o o88o    88  o888o    o888o    

			*/
			CASE TRS_INIT_STREAM
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_CYI_STREAMS_STAGE_01()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_PASSENGER(myTaxiData, vPassengerPt, vPassengerPickupPt, "TaxiDerrick",GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_PASSENGER),116.1366,10.0)
				TAXI_INIT_PASSENGER_BLIP(myTaxiData)
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TAXI_OJ_CYI_STAGE_01_ASSETS_LOADED()
					
					ADD_RELATIONSHIP_GROUP("TAXI_Pursuers", relPursuers)
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					//Init Bonus----------------------------------------
					TAXI_INITIALIZE_BONUS_FIELD(bonusFieldCutYouIn[TCYI_BONUS_LOST_COPS], "TAXI_SC_BN_07", TAXI_CONST_BONUS_CASH_SMOOTH_ESCAPE)			//Escaped Bonus

					TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldCutYouIn)
					
					
					//SET REACT BITS
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					myTaxiData.vTaxiOJPickup =  vPassengerPt	
					
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF

			BREAK
			/*SPAWNING---------------------------------------------------------------------------------------------------------
		
 oooooooo8 oooooooooo   o  oooo     oooo oooo   oooo ooooo oooo   oooo  
888         888    888 888  88   88  88   8888o  88   888   8888o  88  
 888oooooo  888oooo88 8  88  88 888 88    88 888o88   888   88 888o88  
        888 888      8oooo88  888 888     88   8888   888   88   8888  
o88oooo888 o888o   o88o  o888o 8   8     o88o    88  o888o o88o    88 
		*/
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData)
					IF NOT IS_ENTITY_DEAD(myTaxiData.piTaxiPassenger)
						SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_SPECIAL, 0, 0)
						
						SET_PED_RELATIONSHIP_GROUP_HASH(myTaxiData.piTaxiPassenger, relPursuers)
						
						REMOVE_ALL_PED_WEAPONS(myTaxiData.piTaxiPassenger)
					ENDIF
					
					SET_ROADS_IN_ANGLED_AREA(<< -1035.3260, -2703.3054, 12.8004 >>,<< -1056.0803, -2568.6753, 12.8181 >>, 125.0,TRUE,FALSE )
					
					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, RELGROUPHASH_AMBIENT_GANG_FAMILY,myTaxiData.relPassenger )
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, RELGROUPHASH_AMBIENT_GANG_LOST, myTaxiData.relPassenger)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, RELGROUPHASH_AMBIENT_GANG_MEXICAN, myTaxiData.relPassenger)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_PASSENGER))
					
					//SET_PED_WEAPON_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger, "MOVE_P_M_ZERO_RUCKSACK")
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
										
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
//Waits for passenger to head to cab
/*			
oooooooooo ooooo  oooooooo8 oooo   oooo ooooo  oooo oooooooooo  
 888    888 888 o888     88  888  o88    888    88   888    888 
 888oooo88  888 888          888888      888    88   888oooo88  
 888        888 888o     oo  888  88o    888    88   888        
o888o      o888o 888oooo88  o888o o888o   888oo88   o888o       
                                                                
																*/
			CASE TRS_MANAGE_PICKUP
				IF TAXI_HANDLE_IV_PICKUP_NO_METER(myTaxiData)
					
					myTaxiData.vTaxiOJDropoff =  GET_MISSION_CRITICAL_POINTS(0)
					bDropOffFound = TRUE//GET_TAXI_DROPOFF_LOCATION_FROM_DISTANCE(txRegionsArray, myTaxiData.vTaxiOJDropoff,myTaxiData.iTaxiOJ_StatesGenIndex)
			
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
			
			//Wait for the passenger to get into the taxi---------------------------------------------------------------------------------------------
			CASE TRS_WAIT_PARK
				IF IS_PASSENGER_ENTERING_TAXI(myTaxiData)
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_GREET,TRUE)
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)	
					
					TAXI_OJ_CYI_SET_TIP_AND_EXCITEMENT_TO_CHECK()
	
					//Set roads around first dropoff point
					TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(GET_MISSION_CRITICAL_POINTS(-1))
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE(myTaxiData)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK
			
/* DRIVING PASSENGER
	
ooooooooo  oooooooooo  ooooo ooooo  oooo ooooo oooo   oooo  ooooooo8  
 888    88o 888    888  888   888    88   888   8888o  88 o888    88  
 888    888 888oooo88   888    888  88    888   88 888o88 888    oooo 
 888    888 888  88o    888     88888     888   88   8888 888o    88  
o888ooo88  o888o  88o8 o888o     888     o888o o88o    88  888ooo888  
*/
			//Once you get to destination wait----------------------------------------------------------------------------------------------------------
			CASE TRS_DRIVING_PASSENGER

				//Jewelry Store arrival line
				IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST)
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_SEEN_DESTINATION
//				AND NOT IS_VEHICLE_DRIVEABLE(viDummyCars[0])
					IF IS_PASSENGER_IN_TAXI(myTaxiData)
						IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
							IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) <= TAXI_OJ_CONST_DIST_TO_SEE_JEWELRY
								

								//Trigger Line #4-------------------------------------------------
								CLEAR_DIALOGUE_QUEUE(tDialogueLine)
								CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,4)
				
								//Setup JStore Scene
//								viDummyCars[0] = CREATE_VEHICLE(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_1),<< 356.0680, -950.7437, 28.4065 >>,312.5482 )
//								viDummyCars[1] = CREATE_VEHICLE(GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_CAR_2),<< 345.7162, -950.8976, 28.4339 >>,313.2876 )
								
//								SET_VEHICLE_NUMBER_PLATE_TEXT(viDummyCars[0],"3MINS2W")
//								SET_VEHICLE_NUMBER_PLATE_TEXT(viDummyCars[1],"MOONLIVE")
								
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_SEEN_DESTINATION,TRUE,TRUE)
								
								SET_CREATE_RANDOM_COPS(FALSE)
//								CLEAR_AREA_OF_COPS(vTaxiJewelryNPCSpot, 20.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bClearCops
					IF IS_PASSENGER_IN_TAXI(myTaxiData)
						IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
							IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) <= 100
								CLEAR_AREA_OF_COPS(<<333.620789,-955.502747,28.422501>>, 60.0)
								bClearCops = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF TAXI_HANDLE_DRIVING(myTaxiData/*,tTaxiOJ_DQ_Data*/)
					bDropOffFound = FALSE
					
					//REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					
					SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
					
					//Update J Skip pAirport
					myTaxiData.vTaxiOJ_WarpPtDropoff = << -1047.5184, -2722.1135, 19.0823 >>
					myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 238.52
	
					TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
				
					//Close dialogue gates
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_BitsDialogue,TAXI_DXF_BANTER)		
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_1ST_STOP,TRUE,TRUE)
					
					//Set current objective in case we need to be reminded
					myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_WAIT_PASS
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SEND_TO_STORE)
				ENDIF
			BREAK
			
			CASE TRS_SEND_TO_STORE
				
				
				IF NOT  IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF IS_PASSENGER_IN_TAXI(myTaxiData)
						
						LEAVE_TAXI_OJ_FROM_CLOSEST_DOOR_TO_POINT(myTaxiData,vTaxiJewelryNPCSpot)

					ELIF GET_SCRIPT_TASK_STATUS(myTaxiData.piTaxiPassenger,SCRIPT_TASK_LEAVE_VEHICLE) = FINISHED_TASK
						CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
						OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)	
							
							//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 347.2881, -965.1024, 28.4274 >>, 1.5, TAXI_OJ_CONST_TIME_BEFORE_WARP)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTaxiJewelryNPCSpot, 1.5, TAXI_OJ_CONST_TIME_BEFORE_WARP)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 337.3356, -997.7456, 28.1318 >>,PEDMOVEBLENDRATIO_SPRINT, TAXI_OJ_CONST_TIME_BEFORE_WARP)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vTaxiJewelreyInStore,PEDMOVEBLENDRATIO_SPRINT, TAXI_OJ_CONST_TIME_BEFORE_WARP)
							
							//TASK_DUCK(NULL,-1)
							//TASK_STAND_STILL(NULL,-1)
							
						CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
						TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
						CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
						
						SET_PLAYER_CONTROL(GET_PLAYER_INDEX(),TRUE)
						
						TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
						//Move on to the next stage
						//GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 50, TRUE)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_1ST_STOP)
						
						bTaxiShotsFired = FALSE
						
					ENDIF
					
				ENDIF
			BREAK

			//Wait til he gets to first stop, and wait for the passenger to return
			CASE TRS_WAIT_1ST_STOP
				IF NOT  IS_PED_INJURED(myTaxiData.piTaxiPassenger)
				//	SET_PED_MIN_MOVE_BLEND_RATIO(myTaxiData.piTaxiPassenger, 1.5)
				ENDIF
				
				//Manage Vincent here if player gets close to Vincent
				IF NOT bTaxiShotsFired
					TAXI_CYI_HANDLE_PLAYER_NEAR_JEWELRY_STORE_DEUX()
				ENDIF	
				
				//Make sure player stays in area
				IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
					//Handle objective and blipping
					IF NOT IS_ENTITY_AT_COORD(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff,<<10,10,10>>)
						IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) > TAXI_OJ_DIST_SC_FAIL_CYI
							TAXI_SET_FAIL(myTaxiData, "Player abandoned his accomplice while he at the jewelry store",TFS_ABANDONED_PASSENGER)
						ENDIF
						
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > TAXI_OJ_CONST_DIST_FROM_PLAYER_TO_JEWELRY_STORE
							
							//BLIP LOCATION
							IF GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) = 0
							AND NOT bTasked
								TAXI_SET_DROPOFF_BLIP_ON(myTaxiData,TRUE)
								CLEAR_THIS_PRINT("TAXI_OBJ_CYI_01")
							ENDIF

							//OBJ - "Return to the rendevous point"
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_CYI_RETURN_WAIT,TRUE,FALSE,TRUE)
							
						ENDIF
						
					//Obj - ~s~Wait here for your passenger to return.~s~
					ELSE
						IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						AND GET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff) > 0
							
							SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
							SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff,FALSE)
							
							SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_WAIT_PASS,TRUE,FALSE,TRUE)
							CLEAR_THIS_PRINT("TAXI_OBJ_CYI_1B")
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"TXM_CYI - Blip is hidden cause taxi is on the rendevous point")
							
						ENDIF
						
					ENDIF
				//Player has left Taxi
				ELSE
					IF NOT bBrokeLoyalty
						bBrokeLoyalty = TRUE
					ENDIF
					TAXI_YELL_PLAYER_TO_RETURN(myTaxiData,TRUE)
				ENDIF
					
				//LM adding shots fired 12/7/11
				IF IS_ENTITY_AT_COORD(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff,<<10,10,10>>)
				AND IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, vTaxiJewelreyInStore, <<5,5,5>>)
				AND IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
				AND NOT bTaxiShotsFired
					
				
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						IF IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger)
							//Turn off heard shot aggro B*520359
							SET_BIT(aggroArgs.iBitFieldDontCheck, ENUM_TO_INT(EAggro_HeardShot))
							
							GIVE_WEAPON_TO_PED(myTaxiData.piTaxiPassenger, WEAPONTYPE_COMBATPISTOL, 50)
							
							//TASK_SHOOT_AT_COORD(myTaxiData.piTaxiPassenger, << 301.2225, -992.7658, 38.0946 >>/*<< 340.5714, -996.7450, 29.7600 >>*/, 10000, FIRING_TYPE_RANDOM_BURSTS)
							TASK_SHOOT_AT_COORD(myTaxiData.piTaxiPassenger, << 335.53137, -992.47852, 28.38245 >>, 10000, FIRING_TYPE_RANDOM_BURSTS)
							
							SET_ENTITY_VISIBLE(myTaxiData.piTaxiPassenger , FALSE)
							SET_ENTITY_CAN_BE_DAMAGED(myTaxiData.piTaxiPassenger,FALSE)
							SET_ENTITY_INVINCIBLE(myTaxiData.piTaxiPassenger,TRUE)
							//SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger,<<301.2225, -992.7658, 36.0946>>)
							SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, <<339.9392, -992.9357, 28.3731>>)
							
							bTaxiShotsFired = TRUE
							
							CDEBUG1LN(DEBUG_OJ_TAXI,"TXM_CYI - Ped is now shooting")
							
							TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
						ENDIF
					ENDIF
				ENDIF
				
				
				//Wait time
				
				IF bTaxiShotsFired
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > TAXI_OJ_CONST_TIME_TO_RETURN_TO_CAB_AFTER_SHOOTING
					AND IS_ENTITY_AT_COORD(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff,<<10,10,10>>)
					AND IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
						//Send to airport 
						IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
							IF IS_ENTITY_OCCLUDED(myTaxiData.piTaxiPassenger)
								//Clear it once he's visible again
								CLEAR_BIT(aggroArgs.iBitFieldDontCheck,ENUM_TO_INT(EAggro_HeardShot))
								
								SET_ENTITY_VISIBLE(myTaxiData.piTaxiPassenger, TRUE)
								SET_ENTITY_CAN_BE_DAMAGED(myTaxiData.piTaxiPassenger,TRUE)
								SET_ENTITY_INVINCIBLE(myTaxiData.piTaxiPassenger,FALSE)
								SET_ENTITY_COORDS(myTaxiData.piTaxiPassenger, vTaxiJewelreyInStore)//<< 339.9748, -999.8591, 28.3299 >>)
								
								SET_PED_COMPONENT_VARIATION(myTaxiData.piTaxiPassenger, PED_COMP_SPECIAL, 1, 0)
								
								IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
									
									//Set config flag so that he doesn't react to the explosion in the next state
									SET_PED_CONFIG_FLAG(myTaxiData.piTaxiPassenger,PCF_RunFromFiresAndExplosions,FALSE)
									
									CLEAR_PED_TASKS_IMMEDIATELY(myTaxiData.piTaxiPassenger)
									
									VEHICLE_SEAT eSeat
									eSeat = TAXI_GET_PROPER_SEAT(myTaxiData.viTaxi, myTaxiData.piTaxiPassenger)
									
									CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
									OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)	
										
										//TASK_GO_STRAIGHT_TO_COORD(NULL,vTaxiJewelryNPCSpot,PEDMOVEBLENDRATIO_WALK)
										//TASK_GO_TO_COORD_ANY_MEANS(NULL,vTaxiJewelryNPCSpot,PEDMOVEBLENDRATIO_WALK, NULL)
										TASK_GO_TO_COORD_ANY_MEANS(NULL,<< 343.9973, -998.3080, 28.2314 >>,PEDMOVEBLENDRATIO_SPRINT, NULL)
										TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
										TASK_SWAP_WEAPON(NULL,FALSE)
										TASK_ENTER_VEHICLE(NULL,myTaxiData.viTaxi,TAXI_OJ_CONST_TIME_BEFORE_WARP,eSeat)
										
									CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
									TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
									CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
									
									CDEBUG1LN(DEBUG_OJ_TAXI,"TXM_CYI - get back in taxi cab task given")
									TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
									
									IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
										
										SET_BLIP_ALPHA(myTaxiData.blipTaxiDropOff,0)
										SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff,FALSE)
										
										CDEBUG1LN(DEBUG_OJ_TAXI,"hiding blip and route while the passenger is running back to the cab")
										
									ENDIF
									
									SET_PED_WEAPON_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger, "MOVE_P_M_ZERO_RUCKSACK")
									//RESET_PED_WEAPON_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger)
									TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_PASSENGER)
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
				ENDIF
			BREAK
			//Sent to Taxi
			CASE TRS_WAIT_FOR_PASSENGER
				
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 23 //20
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
						CLEAR_PED_TASKS(myTaxiData.piTaxiPassenger)
						TASK_SHOOT_AT_ENTITY(myTaxiData.piTaxiPassenger, PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
					ENDIF
					
					TAXI_SET_FAIL(myTaxiData, "player didn't let teh Passenger in", TFS_ABANDONED_PASSENGER)// TFS_TIME_EXPIRED)
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"TXM_CYI - timer to let passenger in is at: ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF))
					ENDIF
				ENDIF
				
				//LM added an explosion while the guy is running back to the taxi
				IF  IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<347.862091,-983.611816,31.308886>>, <<4.0, 3.0, 3.0>>)
				AND IS_ENTITY_AT_COORD(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff,<<10,10,10>>)
				//AND IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
				AND bTaxiShotsFired
					ADD_EXPLOSION(<<344.59061, -996.40900, 28.30219>>, EXP_TAG_ROCKET)
					PLAY_SOUND_FROM_COORD(iSoundID_BurglarAlarm, "Burglar_Bell", <<340.8,-965.4,28.4>>,"Generic_Alarms")
					bTaxiShotsFired = FALSE
				ENDIF
				
				IF IS_PASSENGER_IN_TAXI(myTaxiData)
					
					CDEBUG1LN(DEBUG_OJ_TAXI,"TXM_CYI - passenger is now in taxi")
					
					SET_CREATE_RANDOM_COPS(TRUE)
					
					SET_PLAYER_WANTED_LEVEL_NO_DROP(GET_PLAYER_INDEX(),TAXI_OJ_CONST_WANTED_LEVEL_FOR_THIS_MISSION)
					SET_PLAYER_WANTED_LEVEL_NOW(GET_PLAYER_INDEX())
					
					START_AUDIO_SCENE("TAXI_CUT_U_IN")
					
					//Set road back to on
					TAXI_PREP_SET_ROADS_IN_AREA_FOR_PICKUP(GET_MISSION_CRITICAL_POINTS(0),TRUE)
					
					//Set the passenger v. cops
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, myTaxiData.relPassenger, RELGROUPHASH_COP)
//					SET_PED_COMBAT_ATTRIBUTES(myTaxiData.piTaxiPassenger,CA_LEAVE_VEHICLES,FALSE)
			
					TAXI_STATS_UPDATE(TAXI_STAT_WANTED_GAINED,TAXI_OJ_CONST_WANTED_LEVEL_FOR_THIS_MISSION)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_LOSE_JEWELRY_POL,TRUE)
				
					//Clean up an GPS Stuff
					IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
						SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff,FALSE)
						REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
					ENDIF
					
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					
					//SET_CINEMATIC_BUTTON_ACTIVE(TRUE)	
					//Move on to the next stage
					TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
					TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
					
					TAXI_RESET_TIMERS(myTaxiData,  TT_RIDETODEST)
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_ESCAPE_POLICE)
				
				ENDIF
			BREAK
			
			/*ESCAPE POLICE-------------------------------------------------------------------------------------------
	
oooooooooo    ooooooo  ooooo       ooooo  oooooooo8 ooooooooooo 
 888    888 o888   888o 888         888 o888     88  888    88  
 888oooo88  888     888 888         888 888          888ooo8    
 888        888o   o888 888      o  888 888o     oo  888    oo  
o888o         88ooo88  o888ooooo88 o888o 888oooo88  o888ooo8888 */

			CASE TRS_ESCAPE_POLICE
				UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
				
				IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
					CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
				ENDIF
				
				//Toggle escape police dialogue every x seconds
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) > 0
					IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)// NOT myTaxiData.bTaxiOJ_CanSpeak
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > GET_RANDOM_FLOAT_IN_RANGE(TAXI_CYI_LOCAL_COP_REACT_DELAY, TAXI_CYI_LOCAL_COP_REACT_DELAY_EXT) 
								AND iCopLinesSaid < 8)
							OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > GET_RANDOM_FLOAT_IN_RANGE(TAXI_CYI_LOCAL_COP_REACT_DELAY_EXT, TAXI_CYI_LOCAL_COP_REACT_DELAY_MAX) 
								
								CDEBUG1LN(DEBUG_OJ_TAXI,"Dialogue timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE))
								CDEBUG1LN(DEBUG_OJ_TAXI,"iCopLinesSaid = ", iCopLinesSaid)
								
								IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_ESCAPE_POLICE
									SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE,TRUE)
									iCopLinesSaid++
								ELSE
									ENABLE_TAXI_SPEECH(myTaxiData)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 10
					
					STOP_AUDIO_SCENE("TAXI_CUT_U_IN")
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_POLICE_ESCAPED)	
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_DISABLE_WANTED)
				ENDIF
				
			BREAK
			
			//Added this case to handle DEbug cheating, for some reason you would still keep a wanted level if you didn't reduce the wanted level twice.
			CASE TRS_POLICE_ESCAPED
				UPDATE_OJ_TAXI_MILEAGE(myTaxiData,myTaxiData.fTaxiOJ_TempFloat)
				
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) > 0
					IF NOT IS_TAXI_SPEECH_ENABLED(myTaxiData)// NOT myTaxiData.bTaxiOJ_CanSpeak
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > GET_RANDOM_FLOAT_IN_RANGE(TAXI_CYI_LOCAL_COP_REACT_DELAY, TAXI_CYI_LOCAL_COP_REACT_DELAY_EXT) 
								AND iCopLinesSaid < 8)
							OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE) > GET_RANDOM_FLOAT_IN_RANGE(TAXI_CYI_LOCAL_COP_REACT_DELAY_EXT, TAXI_CYI_LOCAL_COP_REACT_DELAY_MAX) 
								
								CDEBUG1LN(DEBUG_OJ_TAXI,"Dialogue timer = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DIALOGUE))
								CDEBUG1LN(DEBUG_OJ_TAXI,"iCopLinesSaid = ", iCopLinesSaid)
								
								IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_ESCAPE_POLICE
									SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_ESCAPE_POLICE,TRUE)
									iCopLinesSaid++
								ELSE
									ENABLE_TAXI_SPEECH(myTaxiData)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
				AND GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 2
					
					//DETERMINE BONUS HERE
					IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) <= TAXI_CONST_BONUS_TIME_TO_LOSE_COPS
						TAXI_SET_BONUS_AWARD(myTaxiData,ENUM_TO_INT(TCYI_BONUS_LOST_COPS))
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI BONUS UNLOCKED - Safe Getaway")
					ELSE
						CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI BONUS LOCKED - Safe Getaway")
					ENDIF
					//Set cop passenger relationship to neutral
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_RESPECT, myTaxiData.relPassenger, RELGROUPHASH_COP)
					
					//Set destination to the airport
					myTaxiData.vTaxiOJDropoff = GET_MISSION_CRITICAL_POINTS(5)
				
					TAXI_STATS_UPDATE(TAXI_STAT_WANTED_LOST,TAXI_OJ_CONST_WANTED_LEVEL_FOR_THIS_MISSION)
					
					//TAXI_RESET_TIMERS(myTaxiData,  TT_POLICE)
					TAXI_RESET_EXITEMENT_TIMERS(myTaxiData)
					TAXI_CANCEL_TIMERS(myTaxiData, TT_POLICE)
					TAXI_CANCEL_TIMERS(myTaxiData, TT_STOPPED)
					TAXI_CANCEL_TIMERS(myTaxiData, TT_FAILDELAY)
					
					IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
					ENDIF
					
					myTaxiData.bSecondLeg = TRUE
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REVEAL_DESTINATION)
					
				ENDIF
			BREAK
			
					/*
					
ooooooooooo ooooo oooooooooo     oooooooo8     o      ooooo         oooooooo8  ooooo ooooo ooooooooooo oooooooooo  ooooooooooo 
88  888  88  888   888    888  o888     88    888      888        o888     88   888   888   888    88   888    888  888    88  
    888      888   888oooo88   888           8  88     888        888           888ooo888   888ooo8     888oooo88   888ooo8    
    888      888   888         888o     oo  8oooo88    888      o 888o     oo   888   888   888    oo   888  88o    888    oo  
   o888o    o888o o888o         888oooo88 o88o  o888o o888ooooo88  888oooo88   o888o o888o o888ooo8888 o888o  88o8 o888ooo8888 
  
					*/
			CASE TRS_REVEAL_DESTINATION
				IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData) 
					EVALUATE_TIME_TAKEN_TO_LOSE_POLICE_IN_TAXI_CYI()
					
					// blipping sooner now
					TAXI_SET_DROPOFF_BLIP_ON(myTaxiData,TRUE)
						
					
					REQUEST_TAXI_ODDJOB_CYI_STREAMS_END()
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DROPPING_OFF)
				ENDIF
					
			BREAK
			
			CASE TRS_DROPPING_OFF
				//Blip the airport only once the bojective has been printed in the TAXI_DIALOGUE_OBJ_MANAGER
				IF NOT bIsAirportBlipped
					IF myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GO_AIRPORT
					AND GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) < 1
						CDEBUG1LN(DEBUG_OJ_TAXI,"MAIN_TAXI_OJ_CUTYOUIN: myTaxiData.tTaxiOJ_ObjectiveCurrent = TAXI_OBJ_GO_AIRPORT")
						
						bIsAirportBlipped = TRUE
						
						SET_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_GREETING_DONE)
						
						//Make sure soundID is valid
						ODDJOB_STOP_SOUND(iSoundID_BurglarAlarm)
						
						CDEBUG1LN(DEBUG_OJ_TAXI,"Taxi Alarm Sounds should stop")
						
						tTaxiOJ_DQ_Data.iCurrentDQLine++
						
					ENDIF
				ENDIF
				
				IF bIsAirportBlipped
					IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) > 0
						CDEBUG1LN(DEBUG_OJ_TAXI,"MAIN_TAXI_OJ_CUTYOUIN: Setting bIsAirportBlipped = FALSE since we got WANTED again")
						IF DOES_BLIP_EXIST(myTaxiData.blipTaxiDropOff)
							SET_BLIP_ROUTE(myTaxiData.blipTaxiDropOff,FALSE)
							REMOVE_BLIP(myTaxiData.blipTaxiDropOff)
						ENDIF						
					ELSE
						TAXI_SET_DROPOFF_BLIP_ON(myTaxiData,TRUE)
					ENDIF
				ENDIF
				
				IF IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_DISABLE_WANTED)
				AND bIsAirportBlipped
					//Reset this to handle cases where you debug disable 
					//IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_POLICE) > 3.0
						
						CLEAR_BITMASK_AS_ENUM(myTaxiData.iTaxiJobBits, TAXI_OJ_JB_DISABLE_WANTED)
						IF g_bDebug
							SCRIPT_ASSERT("Wanted Bitmask has been reenabled")
						ENDIF
					
					//ENDIF
				ENDIF
				//Airport arrival line
				IF NOT IS_BITMASK_AS_ENUM_SET(myTaxiData.iTaxiOJ_ExtraBitsDialogue,TAXI_DXF_SEEN_DEST_2)
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_SEEN_DESTINATION_2
					IF IS_PASSENGER_IN_TAXI(myTaxiData)
						IF IS_TAXI_DRIVEN_BY_PLAYER(myTaxiData)
							IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJDropoff) <= TAXI_OJ_CONST_DIST_TO_SEE_AIRPORT
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_SEEN_DESTINATION_2,TRUE,TRUE)
								ELSE
									KILL_ANY_CONVERSATION()
								ENDIF
								
								
								IF bPassengerIsGoingToBeArrested
								AND NOT bCreateCops	
									CLEAR_AREA(myTaxiData.vTaxiOJDropoff, 3.0, FALSE)
									
									sbiAirportCopBlock = ADD_SCENARIO_BLOCKING_AREA(<<-1033.18774, -2743.17969, 19.0>>, <<-1047.94824, -2739.17969, 20.20066>>)
									piMissionCops[0] = CREATE_PED(PEDTYPE_COP, GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE), <<-1036.501953,-2742.985596,20.169693>>)
									piMissionCops[1] = CREATE_PED(PEDTYPE_COP, GET_TAXI_OJ_CYI_MODEL_NAMES(TCM_COP_MALE),<<-1043.70569, -2739.23364, 19.16969>>)
									GIVE_WEAPON_TO_PED(piMissionCops[0], WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
									GIVE_WEAPON_TO_PED(piMissionCops[1], WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE)
									IF DOES_SCENARIO_EXIST_IN_AREA(<<-1036.501953,-2742.985596,20.169693>>, 5.0, TRUE)
										TASK_USE_NEAREST_SCENARIO_TO_COORD(piMissionCops[0], <<-1036.501953,-2742.985596,20.169693>>, 2.5)
										CDEBUG1LN(DEBUG_OJ_TAXI,"scenario exists, using it A")
									ELSE
										TASK_START_SCENARIO_IN_PLACE(piMissionCops[0], "WORLD_HUMAN_GUARD_STAND_ARMY")
										CDEBUG1LN(DEBUG_OJ_TAXI,"scenario does not exist, forcing it A")
									ENDIF
									IF DOES_SCENARIO_EXIST_IN_AREA(<<-1043.70569, -2739.23364, 19.16969>>, 5.0, TRUE)
										TASK_USE_NEAREST_SCENARIO_TO_COORD(piMissionCops[1], <<-1043.70569, -2739.23364, 19.16969>>, 2.5)
										CDEBUG1LN(DEBUG_OJ_TAXI,"scenario exists, using it B")
									ELSE
										TASK_START_SCENARIO_IN_PLACE(piMissionCops[1], "WORLD_HUMAN_GUARD_STAND_ARMY")
										CDEBUG1LN(DEBUG_OJ_TAXI,"scenario does not exist, forcing it B")
									ENDIF
									CDEBUG1LN(DEBUG_OJ_TAXI,"MAIN_TAXI_OJ_CUTYOUIN: ambush cops created")
									
									bCreateCops = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF TAXI_HANDLE_DRIVING(myTaxiData/*,tTaxiOJ_DQ_Data*/)
				
					tTaxiOJ_DQ_Data.tDialogueQStates = TDQ_CLOSED
					REMOVE_BLIP(myTaxiData.blipTaxiDropOff)

					
					//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$CASH
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)	
					CYI_CONVERT_TAXI_TIP_TO_CASH()
				
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)
				ENDIF
				
			BREAK
			
	
			/*	REGULAR PAYMENT------------------------------------------------------------------------------------------------
	
oooooooooo   o   ooooo  oooo oooo     oooo ooooooooooo oooo   oooo ooooooooooo 
 888    888 888    888  88    8888o   888   888    88   8888o  88  88  888  88 
 888oooo88 8  88     888      88 888o8 88   888ooo8     88 888o88      888     
 888      8oooo88    888      88  888  88   888    oo   88   8888      888     
o888o   o88o  o888o o888o    o88o  8  o88o o888ooo8888 o88o    88     o888o    
                                                                            */
			CASE TRS_REGULAR_PAYMENT
				
				IF IS_THIS_THE_HANDOFF_LINE()
				AND NOT bIsHandoffDone
					VEHICLE_SEAT eSeat
					GET_LEFT_OVER_PASSENGER_IN_TAXI(myTaxiData, eSeat)
					CDEBUG1LN(DEBUG_OJ_TAXI,"MAIN_TAXI_OJ_CUTYOUIN: eSeat = ", eSeat)
					
					oiBankNote = CREATE_OBJECT(P_BANKNOTE_S, GET_ENTITY_COORDS(myTaxiData.piTaxiPassenger))
					ATTACH_ENTITY_TO_ENTITY(oiBankNote, myTaxiData.piTaxiPassenger, ENUM_TO_INT(BONETAG_R_HAND), <<0,0,0>>, << 0,0,0>>)
					
					IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						TASK_PLAY_ANIM(myTaxiData.piTaxiPassenger, "oddjobs@taxi@cyi", GET_PASSENGER_HAND_OVER_ANIM_STRING(eSeat),REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "oddjobs@taxi@cyi", GET_PLAYER_HAND_OVER_ANIM_STRING(eSeat),REALLY_SLOW_BLEND_IN,REALLY_SLOW_BLEND_OUT)
						PLAY_ENTITY_ANIM(oiBankNote, GET_MONEY_HAND_OVER_ANIM_STRING(eSeat), "oddjobs@taxi@cyi", 1.0, FALSE, FALSE)
					ENDIF
					bIsHandoffDone = TRUE
				ENDIF
				
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData)
					
					//SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(AP1_02_DOOR_L, << -1042.22, -2747.98, 21.97 >>, FALSE, 0)
					
					CLEAR_SEQUENCE_TASK(seqIndexTaxiTemp)
					OPEN_SEQUENCE_TASK(seqIndexTaxiTemp)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_CLEAR_LOOK_AT(NULL)
		                TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,myTaxiData.vTaxiOJ_PassengerGoToPt,PEDMOVEBLENDRATIO_WALK)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
					CLOSE_SEQUENCE_TASK(seqIndexTaxiTemp)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, seqIndexTaxiTemp)
					
					SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger,TRUE)
					
					//TAXI_OJ_TASK_PASSENGER_DROPOFF(myTaxiData)
					SET_PED_WEAPON_MOVEMENT_CLIPSET(myTaxiData.piTaxiPassenger, "MOVE_P_M_ZERO_RUCKSACK")
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)
				ENDIF	
			BREAK
			

			/*Pop up the scorecard---------------------------------------------------------------------------------------

 oooooooo8    oooooooo8   ooooooo  oooooooooo  ooooooooooo  oooooooo8 
888         o888     88 o888   888o 888    888  888    88 o888     88 
 888oooooo  888         888     888 888oooo88   888ooo8   888         
        888 888o     oo 888o   o888 888  88o    888    oo 888o     oo 
o88oooo888   888oooo88    88ooo88  o888o  88o8 o888ooo8888 888oooo88  
                                                                      */
			CASE TRS_SCORECARD_GRADE
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					//Branching ending depending on how long it took you to lose the cops.
					IF bPassengerIsGoingToBeArrested OR iArrestStateIndex = -1
						
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPECIAL_ENDING)
					ELSE
						
						TAXI_MISSION_END(TRUE,myTaxiData)
						//Move on to the next stage
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
					ENDIF
					
				ENDIF
			
			BREAK
						//Handles the passenger getting arrested
			
			CASE TRS_SPECIAL_ENDING
				IF UPDATE_TAXI_PASSENGER_ARREST_FOR_CYI()
					TAXI_MISSION_END(TRUE, myTaxiData)	
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF
			BREAK
			
			CASE TRS_CLEANUP
				Script_Cleanup()
			BREAK
			
		ENDSWITCH
		

	ENDIF
ENDPROC
PROC TAXI_OJ_HANDLE_CYI_J_SKIPS()
	IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)

		IF iArrestStateIndex = -1
		AND myTaxiData.tTaxiOJ_RideState >= TRS_DRIVING_PASSENGER
			IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi,myTaxiData.vTaxiOJ_WarpPtDropoff) > 5.0//CONST_TAXI_OJ_DISTANCE_AWAY_TO_WARP
				
				REQUEST_TAXI_ODDJOB_CYI_STREAMS_STAGE_ALT_ENDING()
				
				REQUEST_TAXI_ODDJOB_CYI_STREAMS_END()
				
				myTaxiData.tTaxiOJ_RideState = TRS_DROPPING_OFF
				myTaxiData.vTaxiOJDropoff = GET_MISSION_CRITICAL_POINTS(5)
				
				//Update J Skip pAirport
				myTaxiData.vTaxiOJ_WarpPtDropoff = <<-1030.5925, -2503.5776, 19.1324>>
				myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 160.5881
				
				SET_ENTITY_COORDS(myTaxiData.viTaxi,myTaxiData.vTaxiOJ_WarpPtDropoff)
				SET_ENTITY_HEADING(myTaxiData.viTaxi,myTaxiData.fTaxiOJ_WarpPtHeadingDropoff)
			ENDIF
			
		ELIF myTaxiData.tTaxiOJ_RideState = TRS_WAIT_1ST_STOP
		
			IF NOT bDropOffFound
				IF NOT IS_PASSENGER_IN_TAXI(myTaxiData)
					SET_ENTITY_VISIBLE(myTaxiData.piTaxiPassenger, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(myTaxiData.piTaxiPassenger,TRUE)
					SET_ENTITY_INVINCIBLE(myTaxiData.piTaxiPassenger,FALSE)
					SET_PED_INTO_VEHICLE(myTaxiData.piTaxiPassenger,myTaxiData.viTaxi,VS_BACK_LEFT)
					SET_PLAYER_WANTED_LEVEL_NO_DROP(GET_PLAYER_INDEX(),3)
				ENDIF

				myTaxiData.vTaxiOJDropoff = GET_MISSION_CRITICAL_POINTS(5)		//Airport
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_FOR_PASSENGER)
				bDropOffFound = TRUE
			ENDIF
			
		ELIF myTaxiData.tTaxiOJ_RideState = TRS_ESCAPE_POLICE
			IF GET_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX()) > 1
				CLEAR_PLAYER_WANTED_LEVEL(GET_PLAYER_INDEX())
			ENDIF
			
		ENDIF	
	ENDIF
	
ENDPROC	
SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)

	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
		
		
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD	
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			TAXI_OJ_HANDLE_CYI_J_SKIPS()
		ENDIF
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			TAXI_DEBUG_FAIL_TRIGGERED(myTaxiData)
			Script_Failed()
		ENDIF

		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF	
		
		PROCESS_WIDGETS()
	#ENDIF
	//END DEBUG----------------------------------------------------

		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			MAIN_TAXI_OJ_CUTYOUIN()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF

		
		
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
