//=======================================================================================================================================
// Taxi_ClownCar.sc
// Dev : John R. Diaz
/*
		Triplets get in your taxi and all need to go to different dropoff locations. Get all of them to their destination before time runs out.
		Bonus time is awarded for each individual dropoff.
*/



//CHANGELOG==========================================================================================================
//9/27/11 - Reformatting/prettifying all taxi oj scripts
//===================================================================================================================


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//Includes------------------------------------------------------

USING "Taxi_Includes.sch"

USING "taxi_ClownCar_Lib.sch"


//Local Variables-----------------------------------------------

CONST_INT						TAXI_CONST_BONUS_CASH_PUNCTUAL	200

TaxiStruct 						myTaxiData

TAXI_PASSENGER_GROUP 			myTaxiGroup

BONUS_FIELD						bonusFieldClownCar[1]

//Ints
INT 							iDebugThrottle = 1
INT								iTipIndex = 0

//VECTOR 						vDropOff = << -1178.4397, -1577.0077, 3.3163 >> //KUSH SHOP
VECTOR 							vPassengerPt = <<-1284.36658, 295.74365, 63.83044>>// << -1285.1528, 294.9519, 63.8557 >>//<< -1284.1324, 296.2160, 63.9232 >>//<< -1278.4894, 292.7891, 63.8264 >>//<< -1277.6429, 294.3654, 63.9556 >>//<< -1281.1560, 300.9622, 63.9483 >> //RESORT
VECTOR 							vPassengerPickupPt = << -1286.2181, 292.5730, 63.7927 >>

//Bools
BOOL 							bDropOffFound
BOOL							bFirstDropoffBlipEnabled
BOOL							bAmbientIntroNoSetUp
BOOL							bInitWindowOffset
// Dialogue Q Info
BOOL							g_bDebug = FALSE
TAXI_OJ_DIALOGUE_Q_DATA			tTaxiOJ_DQ_Data								
TAXI_OJ_DQ_CONVERSATION_LINE	tDialogueLine[CONST_TAXIOJ_SIZE_Q]

AGGRO_ARGS 						aggroArgs

SCRIPT_SHARD_BIG_MESSAGE TaxiMidSize
SCENARIO_BLOCKING_INDEX			siHotel
//Debug---------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID	 			taxiRideWidgets,ojTaxiWidgets_Hotbox
	TEXT_LABEL_31				sDebugString[4]
	BOOL 						bDebugDrawDistance
	BOOL						bTaxiOJHotBoxPFXEnable, bTaxiOJHotBoxPFXDisable
	BOOL						bTaxiOJPFXBrokenWindowEnable, bTaxiOJPFXBrokenWindowDisable
	BOOL						bTaxiOJPFXOpenDoorEnable, bTaxiOJPFXOpenDoorDisable
	BOOL						bTaxiOJPFXClosedDoorEnable, bTaxiOJPFXClosedDoorDisable
	BOOL						bTaxiOJHotBoxSpeedEvo, bTaxiOJHotBoxSmokeEvo
	BOOL 						bDebugVelocity = FALSE
	BOOL						bDebugTaxiWindowTint, bSmashWindows
	HUD_2D_INT 					h2i_distance
	FLOAT						fPFXScale[4]
	FLOAT						fWindow1X,fWindow2X,fWindow3X,fWindow4X = 0
	FLOAT						fWindow1Y, fWindow2Y, fWindow3Y, fWindow4Y = 0
	FLOAT						fWindow1Z, fWindow2Z, fWindow3Z, fWindow4Z = 0
	VECTOR 						vWindowDF, vWindowDR, vWindowPF, vWindowPR, vWindowDF2, vWindowDR2, vWindowPF2, vWindowPR2
	FLOAT 						fWindowPtRange = 0.05
	FLOAT						fSpeedEvo, fSmokeEvo
	INT							iTaxiWindowColor, iTaxiWindowColorRead
	BOOL 						bDebugTurnOnFreeRide = FALSE	
	BOOL						bDebugBonusPunctual
	TXM_DEBUG_SKIP_STATES 		tDebugState = TXM_DSS_CHECK_FOR_BUTTON_PRESS
#ENDIF

//FUNCTIONS------------------------------------------------------------------------------------------------------------
/// PURPOSE: Do all your taxi specific requests and loading here. 
///   		 Loads our model, string table, and anims and whatever we need for taxi oddjobs
///   
///    

PROC REQUEST_TAXI_ODDJOB_CC_STREAMS_STAGE_01()
	//Load text and UI
	REQUEST_MODEL(myTaxiGroup.mPassengerModel[TAXI_GROUP_P0_REJINO])
	REQUEST_MODEL(myTaxiGroup.mPassengerModel[TAXI_GROUP_P1_FRANKIE])
	REQUEST_MODEL(myTaxiGroup.mPassengerModel[TAXI_GROUP_P2_PAULIE])
		
	TAXI_INIT_SHARED_STREAMS()
	CDEBUG1LN(DEBUG_OJ_TAXI,"INIT_TAXI_STREAMS - SUCCESS")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_CC_STREAMS_STAGE_01()

	//Load text and UI
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P0_REJINO])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P1_FRANKIE])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P2_PAULIE])
		
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM CC Stage 01 Assets Requested")
ENDPROC

PROC REQUEST_TAXI_ODDJOB_CC_STREAMS_HOT_BOX()
	//Request PFX - HotBox
	REQUEST_PTFX_ASSET()
	
	REQUEST_ANIM_DICT("oddjobs@taxi@gyn@cc@hotbox")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM CC Hot Box Assets Requested")
ENDPROC

PROC RELEASE_TAXI_ODDJOB_CC_STREAMS_HOT_BOX()
	//Request PFX - HotBox
	REMOVE_PTFX_ASSET()
	
	REMOVE_ANIM_DICT("oddjobs@taxi@gyn@cc@hotbox")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM CC Hot Box Assets Released")
ENDPROC

PROC REQUEST_TAXI_ODDJOB_CC_STREAMS_STAGE_Intro()

	REQUEST_MODEL(myTaxiGroup.mAmbPedModel[TAXI_AMB_P0_AMANDA])
	REQUEST_MODEL(myTaxiGroup.mAmbPedModel[TAXI_AMB_P1_BRENDA])
	REQUEST_MODEL(myTaxiGroup.mAmbPedModel[TAXI_AMB_P2_CANDY])
	REQUEST_MODEL(myTaxiGroup.mAmbPedModel[TAXI_AMB_P4_GUEST1])
	REQUEST_MODEL(myTaxiGroup.mAmbPedModel[TAXI_AMB_P6_GUEST3])	
	
	REQUEST_MODEL(STRETCH)	
	REQUEST_MODEL(SUPERD)	
	
	REQUEST_ANIM_DICT("oddjobs@taxi@gyn@cc@intro")
	REQUEST_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@exit")
	REQUEST_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@base")
	
	TaxiMidSize.siMovie = REQUEST_MG_MIDSIZED_MESSAGE()
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM CC Intro Stage Assets Requested")
ENDPROC


PROC RELEASE_TAXI_ODDJOB_CC_STREAMS_STAGE_Intro()

	SET_MODEL_AS_NO_LONGER_NEEDED(STRETCH)	
	SET_MODEL_AS_NO_LONGER_NEEDED(SUPERD)	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P0_AMANDA])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P1_BRENDA])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P2_CANDY])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P4_GUEST1])
	SET_MODEL_AS_NO_LONGER_NEEDED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P6_GUEST3])	
	
	REMOVE_ANIM_DICT("oddjobs@taxi@gyn@cc@intro")
	REMOVE_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@exit")
	REMOVE_ANIM_DICT("amb@world_human_stand_impatient@female@no_sign@base")
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TXM CC Intro Stage Assets Released")
ENDPROC
/// PURPOSE: Wait for job specific assets to be streamed in
///    
/// RETURNS: TRUE if everything is streamed and ready to rock. FALSE otherwise
///    
FUNC BOOL HAVE_TXM_CC_INTRO_STREAMS_LOADED()
//Ambient Peds
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P0_AMANDA])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TAXI_AMB_P0_AMANDA",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P1_BRENDA])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TAXI_AMB_P1_BRENDA",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P2_CANDY])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TAXI_AMB_P2_CANDY",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P4_GUEST1])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TAXI_AMB_P4_GUEST1",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mAmbPedModel[TAXI_AMB_P6_GUEST3])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TAXI_AMB_P6_GUEST3",iDebugThrottle)
		RETURN FALSE
	ENDIF

	IF NOT HAS_MODEL_LOADED(STRETCH)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading STRETCH",iDebugThrottle)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(SUPERD)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading SUPERD",iDebugThrottle)
		RETURN FALSE
	ENDIF		
	
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@taxi@gyn@cc@intro")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Anim Dicts oddjobs@taxi@gyn@cc@intro Loading...",iDebugThrottle)
    	RETURN FALSE
	ENDIF  
	
	IF NOT HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@female@no_sign@exit")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Anim Dicts amb@world_human_stand_impatient@female@no_sign@exit Loading...",iDebugThrottle)
    	RETURN FALSE
	ENDIF  
	
	IF NOT HAS_ANIM_DICT_LOADED("amb@world_human_stand_impatient@female@no_sign@base")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Anim Dicts amb@world_human_stand_impatient@female@no_sign@exit Loading...",iDebugThrottle)
    	RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_TXM_CC_HOT_BOX_STREAMS_LOADED()
	//Particle Requests
	IF NOT HAS_PTFX_ASSET_LOADED()
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading scr_oddjobtaxi",iDebugThrottle)
		RETURN FALSE

	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED("oddjobs@taxi@gyn@cc@hotbox")
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading oddjobs@taxi@gyn@cc@hotbox",iDebugThrottle)
		RETURN FALSE
	ENDIF 
		
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_TXM_CC_STAGE_01_STREAMS_LOADED()
		
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P0_REJINO])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_Y_BUSINESS_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P1_FRANKIE])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_Y_BUSINESS_02",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(myTaxiGroup.mPassengerModel[TAXI_GROUP_P2_PAULIE])
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading A_M_Y_GOLFER_01",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(TaxiMidSize.siMovie)
		#IF IS_DEBUG_BUILD
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Loading TaxiMidSize.siMovie",iDebugThrottle)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT TAXI_SHARED_ASSETS_STREAMED(iDebugThrottle)
		CDEBUG_MESSAGE_OJ_TAXI_PERIODIC("TAXI_ASSETS_STREAMED - Waiting for assets...",iDebugThrottle)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(taxiRideWidgets)
			DELETE_WIDGET_GROUP(taxiRideWidgets)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(ojTaxiWidgets_Hotbox)
			DELETE_WIDGET_GROUP(ojTaxiWidgets_Hotbox)
		ENDIF
		
		CLEANUP_TAXI_WIDGETS()
		
	#ENDIF
	
	//Clean up particles
	REMOVE_TAXI_OJ_PFX_GROUP_HOT_BOX(myTaxiGroup)
	
	RELEASE_TAXI_ODDJOB_CC_STREAMS_STAGE_Intro()
	RELEASE_TAXI_ODDJOB_CC_STREAMS_STAGE_01()
	RELEASE_TAXI_ODDJOB_CC_STREAMS_HOT_BOX()
	
	TERMINATE_THIS_THREAD()
ENDPROC

// Perform any special commands if the script fails
// NOTE: This can include WAITs prior to the Mission_Cleanup() call
PROC Script_Failed()
	TAXI_SCRIPT_FAILED(myTaxiData)
	Script_Cleanup()
ENDPROC

PROC INITIALIZE_SCRIPT_VARIABLES()

	TAXI_ODDJOB_GLOBAL_SETUP(myTaxiData,TXM_09_CLOWNCAR)
	
	// Set our initial state up. VIP missions don't start at TRS_FINDING_LOCATION.
	myTaxiData.tTaxiOJ_RideState = TRS_INIT_STREAM
	
	//Init j skip points------
	myTaxiData.vTaxiOJ_WarpPtPickup = << -1248.4220, 252.6221, 63.0799 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingPickup = 34.4
	
	myTaxiData.vTaxiOJ_WarpPtDropoff = << -231.2259, -889.5931, 28.8903 >>
	myTaxiData.fTaxiOJ_WarpPtHeadingDropoff = 251.9658
	
	//Set Models
	myTaxiGroup.mPassengerModel[TAXI_GROUP_P0_REJINO] = A_M_Y_BUSINESS_02
	myTaxiGroup.mPassengerModel[TAXI_GROUP_P1_FRANKIE] = A_M_Y_BUSINESS_02
	myTaxiGroup.mPassengerModel[TAXI_GROUP_P2_PAULIE] = A_M_Y_BUSINESS_02
	
	myTaxiGroup.mAmbPedModel[TAXI_AMB_P0_AMANDA] = A_F_Y_Beach_01
	myTaxiGroup.mAmbPedModel[TAXI_AMB_P1_BRENDA] = A_F_Y_Beach_01
	myTaxiGroup.mAmbPedModel[TAXI_AMB_P2_CANDY] = A_F_Y_Beach_01
	
	myTaxiGroup.mAmbPedModel[TAXI_AMB_P4_GUEST1] = A_F_Y_Beach_01
	myTaxiGroup.mAmbPedModel[TAXI_AMB_P6_GUEST3] = A_M_Y_BUSINESS_01
	
	//Predetermine fares here.
	SET_TAXI_TIP_CUTOFFS(myTaxiData, CONST_TAXI_OJ_TIP_AVERAGE_CUTOFF, CONST_TAXI_OJ_TIP_AMAZING_CUTOFF)
	
	#IF IS_DEBUG_BUILD
		#IF DEBUG_iTurnOnAllDXDebug
			ENABLE_ALL_DIALOGUE_DEBUG()
		#ENDIF
		fWindow1X = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_FRONT_X
		fWindow1Y = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_FRONT_Y
		fWindow1Z = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_FRONT_Z
		
		fWindow2X = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_REAR_X
		fWindow2Y = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_REAR_Y
		fWindow2Z = TAXI_OJ_CONST_PFX_WINDOW_DRIVER_REAR_Z
		
		fWindow3X = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_FRONT_X
		fWindow3Y = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_FRONT_Y
		fWindow3Z = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_FRONT_Z
		
		fWindow4X = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_REAR_X
		fWindow4Y = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_REAR_Y
		fWindow4Z = TAXI_OJ_CONST_PFX_WINDOW_PASSENGER_REAR_Z
		
		taxiRideWidgets = START_WIDGET_GROUP("Taxi Ride - Clown Car")
		
		ADD_WIDGET_STRING("Debug Taxi Bonus")
			ADD_WIDGET_BOOL(" Punctual",bDebugBonusPunctual)
				
		INIT_ODDJOB_TAXI_WIDGETS()
					
			//Excitement Widget
			ojTaxiWidgets_Hotbox = START_WIDGET_GROUP("Hotbox")
			
				//Driver Front Window
				ADD_WIDGET_FLOAT_SLIDER("Blue X DF= ", fWindow1X, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Blue Y DF= ", fWindow1Y, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Blue Z DF= ", fWindow1Z, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Blue Scale= ", fPFXScale[0],0.1,2.0,0.1)
				
				//Driver Rear Window
				ADD_WIDGET_FLOAT_SLIDER("Green X DR= ", fWindow2X, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Green Y DR= ", fWindow2Y, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Green Z DR= ", fWindow2Z, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Green Scale= ", fPFXScale[1],0.1,2.0,0.1)
				
				//Passegner Front Window
				ADD_WIDGET_FLOAT_SLIDER("Red X PF= ", fWindow3X, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Red Y PF= ", fWindow3Y, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Red Z PF= ", fWindow3Z, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Red Scale= ", fPFXScale[2],0.1,2.0,0.1)
				
				//Passenger Rear Window
				ADD_WIDGET_FLOAT_SLIDER("Purple X PR= ", fWindow4X, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Purple Y PR= ", fWindow4Y, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Purple Z PR= ", fWindow4Z, -2.0, 2.0,0.01)
				ADD_WIDGET_FLOAT_SLIDER("Purple Scale= ", fPFXScale[3],0.1,2.0,0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("Window Pt Range = ", fWindowPtRange, 0.05, 1.0,0.05)
				
				ADD_WIDGET_BOOL("Turn on PFX ", bTaxiOJHotBoxPFXEnable)
				ADD_WIDGET_BOOL("Turn off PFX ", bTaxiOJHotBoxPFXDisable)
				
				ADD_WIDGET_BOOL("Turn on Broken Window PFX ", bTaxiOJPFXBrokenWindowEnable)
				ADD_WIDGET_BOOL("Turn off Broken Window PFX ", bTaxiOJPFXBrokenWindowDisable)
				
				ADD_WIDGET_BOOL("Turn on Open Door PFX ", bTaxiOJPFXOpenDoorEnable)
				ADD_WIDGET_BOOL("Turn off Open Door PFX ", bTaxiOJPFXOpenDoorDisable)
				
				ADD_WIDGET_BOOL("Turn on Closed Door PFX ", bTaxiOJPFXClosedDoorEnable)
				ADD_WIDGET_BOOL("Turn off Closed Door PFX ", bTaxiOJPFXClosedDoorDisable)
				
				ADD_WIDGET_BOOL("Adjust PFX Speed Evolution", bTaxiOJHotBoxSpeedEvo)
				ADD_WIDGET_FLOAT_SLIDER("Speed Evolution", fSpeedEvo, 0.0, 1.0, 0.1)
				ADD_WIDGET_BOOL("Adjust PFX Smoke Evolution", bTaxiOJHotBoxSmokeEvo)
				ADD_WIDGET_FLOAT_SLIDER("Smoke Evolution", fSmokeEvo, 0.0, 1.0, 0.1)
				
				ADD_WIDGET_BOOL("Draw MPH", bDebugVelocity)
				
				ADD_WIDGET_BOOL("Debug Taxi Window Tint", bDebugTaxiWindowTint)
				
				ADD_WIDGET_INT_SLIDER("Window Tint Color Index", iTaxiWindowColor, 0, 10, 1)
				ADD_WIDGET_INT_READ_ONLY("Window Tint Index", iTaxiWindowColorRead)
				
				ADD_WIDGET_BOOL("Smash Windows", bSmashWindows)
				
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	#ENDIF
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~~~~~~~~~~~~ Oddjobs | Taxi | Clown Car ~~~~~~~~~~~~~~~~~~~~----------")	
	CDEBUG1LN(DEBUG_OJ_TAXI,"-----~~~~~-------------------------------~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~----")

ENDPROC

PROC TAXI_GET_WINDOW_SMOKE_OFFSETS()
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		IF IS_PLAYER_DRIVING_TAXI_OJ(myTaxiData)
			IF NOT bInitWindowOffset
				INT iBoneLF, iBoneLR, iBoneRF, iBoneRR
				iBoneLF = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_lf")
				iBoneLR = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_lr")
				iBoneRF = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_rf")
				iBoneRR = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_rr")
				
				vSmokeWindowDF = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneLF)
				vSmokeWindowDF = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vSmokeWindowDF)
				vSmokeWindowBrokenDF = vSmokeWindowDF
				vSmokeWindowDF.z += 0.15
				vSmokeWindowDF.y -= 0.15
				
				vSmokeWindowDR = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneLR)
				vSmokeWindowDR = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vSmokeWindowDR)
				vSmokeWindowBrokenDR = vSmokeWindowDR
				vSmokeWindowDR.z += 0.15
				
				vSmokeWindowPF = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneRF)
				vSmokeWindowPF = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vSmokeWindowPF)
				vSmokeWindowBrokenPF = vSmokeWindowPF
				vSmokeWindowPF.z += 0.15
				vSmokeWindowPF.y -= 0.15
				
				vSmokeWindowPR = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneRR)
				vSmokeWindowPR = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(myTaxiData.viTaxi, vSmokeWindowPR)
				vSmokeWindowBrokenPR = vSmokeWindowPR
				vSmokeWindowPR.z += 0.15
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"-------------------------------------------")
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowDF = ", vSmokeWindowDF)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowDR = ", vSmokeWindowDR)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowPF = ", vSmokeWindowPF)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowPR = ", vSmokeWindowPR)
				CDEBUG1LN(DEBUG_OJ_TAXI,"-------------------------------------------")
				
				CDEBUG1LN(DEBUG_OJ_TAXI,"-------------------------------------------")
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowBrokenDF = ", vSmokeWindowBrokenDF)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowBrokenDR = ", vSmokeWindowBrokenDR)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowBrokenPF = ", vSmokeWindowBrokenPF)
				CDEBUG1LN(DEBUG_OJ_TAXI,"| vSmokeWindowBrokenPR = ", vSmokeWindowBrokenPR)
				CDEBUG1LN(DEBUG_OJ_TAXI,"-------------------------------------------")
				
				bInitWindowOffset = TRUE
			ENDIF
		ELSE
			IF bInitWindowOffset
				bInitWindowOffset = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_WIDGETS()
	
	UPDATE_TAXI_WIDGETS(myTaxiData,tTaxiOJ_DQ_Data)
	
	IF bDebugBonusPunctual
		IF IS_TAXI_TIMER_STARTED(myTaxiData, TT_DROPOFF)
			sDebugString[0] = "Dropoff Timer "
			sDebugString[0] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP( GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF))
			DRAW_DEBUG_TEXT_2D(sDebugString[0],<<0.07,0.67,0.0>>)
		
			IF myTaxiGroup.iWhichDropoff < 2
					
				IF fGoodPaulieDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 0
					//Draw value in reg color
					sDebugString[1] = "Left to drop Paulie: "
					sDebugString[1] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(fGoodPaulieDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF))
					DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.69,0.0>>)
					
				ENDIF
			ELIF myTaxiGroup.iWhichDropoff >= 2
			AND myTaxiGroup.iWhichDropoff < 4
				//DEBUG SHOW IF PAULIE DROPPED OFF
				IF myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P2_PAULIE]
					sDebugString[1] = "Paulie dropped off timely. "
					DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.69,0.0>>)
				ENDIF
				
				IF fGoodFrankieDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 0
					//Draw value in reg color
					sDebugString[2] = "Left to drop Frankie: "
					sDebugString[2] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(fGoodPaulieDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF))
					DRAW_DEBUG_TEXT_2D(sDebugString[2],<<0.07,0.71,0.0>>)
					
				ENDIF
			ELIF myTaxiGroup.iWhichDropoff >= 4
				//DEBUG SHOW IF PAULIE DROPPED OFF
				IF myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P2_PAULIE]
					sDebugString[1] = "Paulie dropped off timely. "
					DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.69,0.0>>)
				ELSE
					sDebugString[1] = "Failed to drop Paulie off in time "
					DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.69,0.0>>,255)
				ENDIF
				
				//DEBUG SHOW IF PAULIE DROPPED OFF
				IF myTaxiGroup.bIsTimelyDropoff[TAXI_GROUP_P1_FRANKIE]
					sDebugString[2] = "Frankie dropped off timely. "
					DRAW_DEBUG_TEXT_2D(sDebugString[2],<<0.07,0.71,0.0>>)
				ELSE
					sDebugString[2] = "Failed to drop Frankie off in time "
					DRAW_DEBUG_TEXT_2D(sDebugString[2],<<0.07,0.71,0.0>>,255)
				ENDIF
				
				IF fGoodRejinoDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF) > 0
					//Draw value in reg color
					sDebugString[3] = "Left to drop Rejino: "
					sDebugString[3] += TAXI_UTILS_GET_STRING_FROM_FLOAT_SP(fGoodPaulieDropOffTime - GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DROPOFF))
					DRAW_DEBUG_TEXT_2D(sDebugString[3],<<0.07,0.73,0.0>>)
				ENDIF
			ENDIF
		ENDIF
							
		IF NOT HAS_TAXI_GROUP_BEEN_DROPPED_OFF_TIMELY(myTaxiGroup)
			sDebugString[1]= "Punctual Bonus LOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>,255,0,0)
			
		ELSE
			sDebugString[1]= "Punctual Bonus UnLOCKED "
			DRAW_DEBUG_TEXT_2D(sDebugString[1],<<0.07,0.77,0.0>>)
		ENDIF
	ENDIF
	
	IF bDebugDrawDistance
	//	TAXI_DEBUG_DRAW_DISTANCE(bDropOffFound,myTaxiData,h2i_distance)
		IF myTaxiGroup.iWhichDropoff = 0 OR  myTaxiGroup.iWhichDropoff = 1
			TAXI_DEBUG_DRAW_DISTANCE_GROUP(bDropOffFound,myTaxiData, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE], h2i_distance)
		ELIF myTaxiGroup.iWhichDropoff = 2 OR  myTaxiGroup.iWhichDropoff = 3
			TAXI_DEBUG_DRAW_DISTANCE_GROUP(bDropOffFound,myTaxiData, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE], h2i_distance)
		ELIF myTaxiGroup.iWhichDropoff = 4 OR  myTaxiGroup.iWhichDropoff = 5
			TAXI_DEBUG_DRAW_DISTANCE_GROUP(bDropOffFound,myTaxiData, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO], h2i_distance)
		ENDIF
		
	ENDIF	
	
	IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
		INT iBoneLF, iBoneLR, iBoneRF, iBoneRR
		iBoneLF = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_lf")
		iBoneLR = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_lr")
		iBoneRF = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_rf")
		iBoneRR = GET_ENTITY_BONE_INDEX_BY_NAME(myTaxiData.viTaxi, "window_rr")
		
		//Driver Rear in GREEN
		vWindowDF2 = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneLF)
		vWindowDF2.z += 0.15
		vWindowDF2.y -= 0.15
		DRAW_DEBUG_SPHERE(vWindowDF2,fWindowPtRange,0,255,0)
		
		//Driver Rear in GREEN
		vWindowDR2 = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneLR)
		vWindowDR2.z += 0.15
		DRAW_DEBUG_SPHERE(vWindowDR2,fWindowPtRange,0,255,0)
		
		//Passenger Front in RED
		vWindowPF2 = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneRF)
		vWindowPF2.z += 0.15
		vWindowPF2.y -= 0.15
		DRAW_DEBUG_SPHERE(vWindowPF2,fWindowPtRange,255,0,0)
		
		//Passenger Rear in PURPLE
		vWindowPR2 = GET_WORLD_POSITION_OF_ENTITY_BONE(myTaxiData.viTaxi, iBoneRR)
		vWindowPR2.z += 0.15
		DRAW_DEBUG_SPHERE(vWindowPR2,fWindowPtRange,123,104,238)
		
		//Driver Front in BLUE
		vWindowDF = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<fWindow1X,fWindow1Y, fWindow1Z >>)
		DRAW_DEBUG_SPHERE(vWindowDF,fWindowPtRange)
		
		//Driver Rear in GREEN
		vWindowDR = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<fWindow2X,fWindow2Y, fWindow2Z >>)
		DRAW_DEBUG_SPHERE(vWindowDR,fWindowPtRange,0,255,0)
		
		//Passenger Front in RED
		vWindowPF = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<fWindow3X,fWindow3Y, fWindow3Z >>)
		DRAW_DEBUG_SPHERE(vWindowPF,fWindowPtRange,255,0,0)
		
		//Passenger Rear in PURPLE
		vWindowPR = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(myTaxiData.viTaxi,<<fWindow4X,fWindow4Y, fWindow4Z >>)
		DRAW_DEBUG_SPHERE(vWindowPR,fWindowPtRange,123,104,238)
	ENDIF
	
	//Turn on pfx
	IF bTaxiOJHotBoxPFXEnable
		ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE(myTaxiData, myTaxiGroup)
		//ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX(myTaxiData, pFxTaxiWindowSmoke,vWindowDF,vWindowDR, vWindowPF, vWindowPR,fPFXScale)
		bTaxiOJHotBoxPFXEnable = FALSE
	ENDIF
	
	//Turn off pfx
	IF bTaxiOJHotBoxPFXDisable
		DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX(myTaxiGroup)
		bTaxiOJHotBoxPFXDisable = FALSE
	ENDIF
	
	//Turn on Broken window pfx
	IF bTaxiOJPFXBrokenWindowEnable
		ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_BROKEN_WINDOW(myTaxiData, myTaxiGroup)
		bTaxiOJPFXBrokenWindowEnable = FALSE
	ENDIF
	
	//Turn off Broken window pfx
	IF bTaxiOJPFXBrokenWindowDisable
		DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_BROKEN_WINDOW(myTaxiGroup)
		bTaxiOJPFXBrokenWindowDisable = FALSE
	ENDIF
	
	//Turn on Open Door pfx
	IF bTaxiOJPFXOpenDoorEnable
		ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_OPEN_DOOR(myTaxiData, myTaxiGroup)
		bTaxiOJPFXOpenDoorEnable = FALSE
	ENDIF
	
	//Turn off Open Door pfx
	IF bTaxiOJPFXOpenDoorDisable
		DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_OPEN_DOOR(myTaxiGroup)
		bTaxiOJPFXOpenDoorDisable = FALSE
	ENDIF
	
	//Turn on Open Door pfx
	IF bTaxiOJPFXClosedDoorEnable
		ENABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_HARDCODE_CLOSED_DOOR(myTaxiData, myTaxiGroup)
		bTaxiOJPFXClosedDoorEnable = FALSE
	ENDIF
	
	//Turn off Open Door pfx
	IF bTaxiOJPFXClosedDoorDisable
		DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX_CLOSED_DOOR(myTaxiGroup)
		bTaxiOJPFXClosedDoorDisable = FALSE
	ENDIF
	
	INT iTemp1, iTemp2
	
	//LM smoke debugs 12/2/11
	// Debug the smoke evolutions.  
	// In order for these to work you have comment out the equivalent PROC's in taxi_clowncar_lib
	IF bTaxiOJHotBoxSpeedEvo
		iTemp1 = 0
		REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp1
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp1], "speed", fSpeedEvo)
		ENDREPEAT
	ENDIF
	
	IF bTaxiOJHotBoxSmokeEvo
		iTemp2 = 0
		REPEAT TAXI_OJ_CONST_PFX_NUM_HOT_BOX_PARTICLES iTemp2
			SET_PARTICLE_FX_LOOPED_EVOLUTION(myTaxiGroup.pFxTaxiWindowSmoke[iTemp2], "smoke", fSmokeEvo)
		ENDREPEAT
	ENDIF
	
	IF bDebugTaxiWindowTint
		
		iTaxiWindowColorRead = GET_VEHICLE_WINDOW_TINT(myTaxiData.viTaxi)
		
		SET_VEHICLE_WINDOW_TINT(myTaxiData.viTaxi, iTaxiWindowColor)
		
	ENDIF
	
	IF bSmashWindows
		INT iWindow
		iWindow = GET_RANDOM_INT_IN_RANGE(0,3)
		
		SMASH_VEHICLE_WINDOW(myTaxiData.viTaxi, INT_TO_ENUM(SC_WINDOW_LIST, iWindow))
		bSmashWindows = FALSE
	ENDIF
ENDPROC
#ENDIF


PROC TAXI_OJ_CC_SET_TIPS_TO_CHECK()

	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_POS_DELIVERY_TIME)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_HIT_PED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_TOOK_DAMAGE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_ROLL_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LEFT_CAR)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_STOPPED)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_LIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_RADIO_STATION_DISLIKE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_FREEBIE)
	SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_TipsBitMask,TAXI_TIP_BIT_LOST_POLICE)
	
	//Turn off aggro bits
	SET_BITMASK_AS_ENUM(aggroArgs.iBitFieldDontCheck, EAggro_Wanted)
	
	
	CDEBUG1LN(DEBUG_OJ_TAXI,"TAXI_OJ_DL_SET_TIPS_TO_CHECK set tips to check on")
ENDPROC

/*
ooooooooo  ooooo      o      ooooo         ooooooo     ooooooo8  
 888    88o 888      888      888        o888   888o o888    88  
 888    888 888     8  88     888        888     888 888    oooo 
 888    888 888    8oooo88    888      o 888o   o888 888o    88  
o888ooo88  o888o o88o  o888o o888ooooo88   88ooo88    888ooo888  
*/
PROC TRIGGER_TAXI_QUEUE_CC_LINES()

	SET_TAXI_OJ_INTERRUPT_TIMER_OFF(myTaxiData)

	//Trigger lines
	IF IS_BANTER_SAFE_TO_PLAY(myTaxiData,tTaxiOJ_DQ_Data)
		
		SWITCH tTaxiOJ_DQ_Data.iCurrentDQLine
			
			CASE 0
				IF myTaxiData.tTaxiOJ_RideState =  TRS_DRIVING_PASSENGER
					
					IF NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
						IF SAFE_IS_THIS_PRINT_BEING_DISPLAYED("TAXI_OBJ_CC1")
						OR DOES_BLIP_EXIST(myTaxiGroup.blipTaxiDropOff[TAXI_GROUP_P2_PAULIE])
							CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO Maze Bank has been assigned")
							tTaxiOJ_DQ_Data.iCurrentDQLine++
							
						ELSE
							IF GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_OBJ_GIVE_MAIN
							
								SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_OBJ_GIVE_MAIN,TRUE,FALSE,TRUE)
								CDEBUG1LN(DEBUG_OJ_TAXI,"--------------------------[Taxi Oddjob Objective] GO TO Maze Bank  has been REassigned")

							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 1
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER,TRUE)
					
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 1")
					ENDIF
				
					//Start the mission timer here
					TAXI_START_TIMER(myTaxiData, TT_RIDETODEST)
					
				ENDIF
			BREAK
			
			CASE 2
				// Triggering Smoke anims
				IF NOT myTaxiGroup.bSmokeAnimStart
					myTaxiGroup.bSmokeAnimStart = TRUE
					bHotBoxReady = TRUE
				ENDIF
				
				IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 530
					tTaxiOJ_DQ_Data.iCurrentDQLine++
				ENDIF
				
				IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > 17
					OR (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 700)
				//AND tTaxiOJ_DQ_Data.iCurrentDQLine = 2
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BUILD_HOTBOX_ONE_LINE,TRUE)
					
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Build Hot Box one line")
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
//				IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 530
//					tTaxiOJ_DQ_Data.iCurrentDQLine++
//				ENDIF
				
				IF GET_TAXI_SPEECH_INDEX(myTaxiData) > TAXI_OBJ_GIVE_MAIN
				AND GET_TAXI_SPEECH_INDEX(myTaxiData) <> TAXI_DI_BANTER_2
				AND NOT IS_TAXI_OJ_INTERRUPTION_LINE_PLAYING(myTaxiData)
					
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BANTER_2,TRUE)
					
					IF g_bDebug
						SCRIPT_ASSERT("Triggering Banter 2")
					ENDIF
					
				ENDIF
			BREAK
			
			CASE 4 //3
				IF (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 530
					tTaxiOJ_DQ_Data.iCurrentDQLine++
				ENDIF
				
				IF (GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > 8
					OR (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 700)
				//AND tTaxiOJ_DQ_Data.iCurrentDQLine = 3
					
					//IF NOT bHotBoxEarly
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_BUILD_HOTBOX,TRUE)
						//bHotBoxReady = TRUE
						
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Build Hot Box")
						ENDIF
					//ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			BREAK
			
			CASE 5 //4
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) > 20
				OR (GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.viTaxi, myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE])) < 530
					
					IF NOT bWeedEffect
						//Player_Takes_Weed_Hit(PLAYER_PED_ID())
						CDEBUG1LN(DEBUG_OJ_TAXI,"Weed effect applied")
						bWeedEffect = TRUE
					ENDIF
					
					IF NOT bHotBoxWindowReady
						
						SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_INIT_HOTBOX,TRUE)
						bHotBoxWindowReady = TRUE
						
						IF g_bDebug
							SCRIPT_ASSERT("Triggering Banter HOTBOX")
						ENDIF
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() % 1000) < 50
						CDEBUG1LN(DEBUG_OJ_TAXI,"GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER) = ", GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_BANTER))
					ENDIF
				ENDIF
			
			BREAK 
			
			CASE 6 //5
				IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_DQUEUE) > GET_RANDOM_FLOAT_IN_RANGE(8.0,16.0)
					//Set Radio Station Check
//					IF NOT GET_TAXI_RADIO_CHECK_FLAG(myTaxiData)	
//						TAXI_RADIO_STATION_TURN_ON(myTaxiData)
//						IF g_bDebug
//							SCRIPT_ASSERT("The Taxi Radio Should Be Updating")
//						ENDIF
//					ENDIF
				ENDIF
			BREAK
		ENDSWITCH				
	ENDIF				
	
	PROCESS_IMPORTANT_DIALOGUE_Q(myTaxiData,tDialogueLine, tTaxiOJ_DQ_Data, g_bDebug)

ENDPROC

/*

oooo     oooo      o      ooooo oooo   oooo 
 8888o   888      888      888   8888o  88  
 88 888o8 88     8  88     888   88 888o88  
 88  888  88    8oooo88    888   88   8888  
o88o  8  o88o o88o  o888o o888o o88o    88  
                                           
*/

PROC MAIN_TAXI_OJ_CLOWN_CAR()	
	
	//Handles Fail Or No Taxi-------------------------------------------------------------------------------
	IF IS_TAXI_JOB_IN_FAIL_STATE(myTaxiData)
		
		TAXI_OJ_CLEAR_ALL_BLIPS(myTaxiData)
		
		IF myTaxiData.tTaxiOJ_RideState > TRS_INIT_STREAM
			IF TAXI_HANDLE_FAIL(myTaxiData)
				Script_Failed()
			ENDIF
		ELSE
		
			IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData, TT_CLEANUP) > 5.0
				CDEBUG1LN(DEBUG_OJ_TAXI,"Fail During State: "," ", myTaxiData.sTaxiOJ_RideState, " ", myTaxiData.sTaxiOJ_Reason4Fail)
				SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
				Script_Cleanup()
			ENDIF
		
		ENDIF
	
	//Proceed
	ELSE
		//Run Throughtout the Entire Mission--------------------------------------------------------------
		RUN_GLOBAL_TAXI_UPDATES(myTaxiData,aggroArgs)
		
		PROCESS_TAXI_EXCEPTIONS(myTaxiData)
		
		UPDATE_TAXI_OJ_TIP(myTaxiData,iTipIndex)

		IF myTaxiData.tTaxiOJ_RideState < TRS_DRIVING_PASSENGER
			TAXI_OJ_HANDLE_GROUP_AGGRO(myTaxiData,myTaxiGroup,aggroArgs)
		ENDIF
		
		//Find Dropoff Point
		IF myTaxiData.tTaxiOJ_RideState > TRS_MANAGE_PICKUP
		AND NOT bDropOffFound
			bDropOffFound = TRUE
			myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P0_REJINO]	=	<<27.36738, -1356.94824, 28.21810>> //<< 25.5080, -1357.8468, 28.2690 >>		//7-11
			myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P1_FRANKIE]	=	<<314.30136, -266.34949, 52.78319>> //<< 317.4944, -267.4910, 52.7820 >>		//bankinbetween
			myTaxiGroup.vTaxiOJDropoff[TAXI_GROUP_P2_PAULIE]	= 	<<-159.96420, -856.53094, 28.71328>> //<< -160.9156, -858.0977, 28.6290 >>  	//bank
			
			myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P0_REJINO]		=	<< 30.9791, -1347.3896, 28.4939 >> //<< 27.3107, -1342.5736, 28.4939 >>		//7-11
			myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P1_FRANKIE]	=	<< 313.8930, -279.0695, 53.1647 >>
			myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P2_PAULIE]		= 	<< -88.0743, -845.7833, 40.7970 >>  //<< 145.5663, -1044.2811, 28.3680 >>
			//bDropOffFound =  GET_TAXI_GROUP_DROPOFF_LOCATION_FROM_DISTANCE(txRegionsArray,myTaxiData,myTaxiGroup)	
		ENDIF
		
		//Dialogue & Objectives
		IF myTaxiData.tTaxiOJ_RideState > TRS_FINDING_LOCATION			
			IF NOT IS_TAXI_EMERGENCY_FAIL_SET(myTaxiData)
				
				IF NOT bAmbientIntroNoSetUp
					IF HAVE_TXM_CC_INTRO_STREAMS_LOADED()
						INIT_AMBIENT_CROWD_AT_CC_PICKUP(myTaxiGroup)	
						bAmbientIntroNoSetUp = TRUE
					ENDIF
				ENDIF
				
				
				TRIGGER_TAXI_QUEUE_CC_LINES()
			ELSE
				TAXI_SET_FAIL(myTaxiData,"Taxi Not Driveable",GET_TAXI_EMERGENCY_FAIL_STRING(myTaxiData))
			ENDIF			
					
		ENDIF
		
		INT k, k2
		SEQUENCE_INDEX tempIndex
		BOOL bHitPickupCSPed
		IF myTaxiData.tTaxiOJ_RideState > TRS_SPAWNING
			IF NOT bHitPickupCSPed
				IF IS_VEHICLE_DRIVEABLE(myTaxiData.viTaxi)
					REPEAT ENUM_TO_INT(TAXI_AMB_PED_NUM) k
						IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[INT_TO_ENUM(TAXI_AMB_PED_NAMES, k)])
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myTaxiGroup.piAmbPed[INT_TO_ENUM(TAXI_AMB_PED_NAMES, k)], myTaxiData.viTaxi)
								bHitPickupCSPed = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
			
			IF bHitPickupCSPed
				REPEAT ENUM_TO_INT(TAXI_AMB_PED_NUM) k2
					IF NOT IS_ENTITY_DEAD(myTaxiGroup.piAmbPed[INT_TO_ENUM(TAXI_AMB_PED_NAMES, k2)])
						IF NOT IS_PED_INJURED(myTaxiGroup.piAmbPed[k2])
							CLEAR_PED_TASKS(myTaxiGroup.piAmbPed[k2])
							CLEAR_SEQUENCE_TASK(tempIndex)
							OPEN_SEQUENCE_TASK(tempIndex)					
								 TASK_SMART_FLEE_PED(NULL,  myTaxiData.piTaxiPlayer, 1000.0, -1)
							CLOSE_SEQUENCE_TASK(tempIndex)
							TASK_PERFORM_SEQUENCE(myTaxiGroup.piAmbPed[k2], tempIndex)
							CLEAR_SEQUENCE_TASK(tempIndex)		
						ENDIF
					ENDIF
				ENDREPEAT			
			ENDIF
		ENDIF
		
		
		
		IF myTaxiData.tTaxiOJ_RideState = TRS_DRIVING_PASSENGER
			
			HANDLE_TAXI_EXCITEMENT(myTaxiData,FALSE,TRUE)
			
			//set amb peds at passenger pickup as no longer needed if player is 200 away
			TAXI_SET_INTRO_AMB_PEDS_AS_NOLONGER_NEEDED(myTaxiData, myTaxiGroup)

#IF IS_DEBUG_BUILD
		ELIF bDebugTurnOnFreeRide
			HANDLE_TAXI_EXCITEMENT(myTaxiData,TRUE,TRUE)
#ENDIF
		ENDIF			
		//================================================================================================
		
		SWITCH myTaxiData.tTaxiOJ_RideState
			
			//PRESTREAM and Check that the player and taxi are good to go
			CASE TRS_INIT_STREAM
				
				//Initialize Bonuses
				TAXI_INITIALIZE_BONUS_FIELD(bonusFieldClownCar[0], "TAXI_SC_BN_10", TAXI_CONST_BONUS_CASH_PUNCTUAL)
				TAXI_INITIALIZE_BONUS_INFO(myTaxiData, bonusFieldClownCar)
				
				//Request of our assets
				REQUEST_TAXI_ODDJOB_CC_STREAMS_STAGE_01()
				
				REQUEST_TAXI_ODDJOB_CC_STREAMS_STAGE_Intro()
				
				// TODO - temporary placement of this request.  Rmove after the new PFX are in
				REQUEST_TAXI_ODDJOB_CC_STREAMS_HOT_BOX()
				
				TAXI_RESET_TIMERS(myTaxiData, TT_DIALOGUE,TAXI_DX_DELAY)
				
				TAXI_SPAWN_GROUP_PASSENGERS(myTaxiData, vPassengerPt, vPassengerPickupPt, myTaxiGroup)
				TAXI_INIT_GROUP_PASSENGER_BLIP(myTaxiData)
					
				//Move on to the next stage
				TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_STREAMING)
				
			BREAK
			
			//Wait for streaming to finish
			CASE TRS_STREAMING

				IF HAVE_TXM_CC_STAGE_01_STREAMS_LOADED()
					
					INIT_ALL_TAXI_EXCITEMENT_VALUES()
					
					INITIALIZE_GENERIC_TAXI_EXCEPTIONS()
					
					//SET REACT BITS
					CLEAR_ALL_TAXI_PASSENGER_REACT_BITS(myTaxiData)
					
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_HITPED)	
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_TOOKDAMAGE)
					SET_BITMASK_AS_ENUM(myTaxiData.iTaxiOJ_PassengerReactBits, TAXI_DF_ROLL)
					
					myTaxiData.vTaxiOJPickup = vPassengerPt
					
					myTaxiData.iTaxiOJ_StatesGenIndex = 0
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SPAWNING)
				ENDIF
	
			BREAK
			
			CASE TRS_SPAWNING
				IF PROPERTY_VIP_INIT_READY(myTaxiData,3)
					
					//Give player obj to go pickup Passenger
					ENABLE_TAXI_SPEECH(myTaxiData)
					
					TAXI_OJ_CLEAR_AREA_OF_ALL_PEDS_AND_SCENARIOS(siHotel, vPassengerPt, 20.0)

					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
				ENDIF
			BREAK
			
			//Handles the passengers until you reach them.
			CASE TRS_MANAGE_PICKUP
				TAXI_GET_WINDOW_SMOKE_OFFSETS()
				
				IF TAXI_HANDLE_GROUP_PICKUP(myTaxiData, myTaxiGroup)
					
					STAT_GET_FLOAT(myTaxiData.sDrivingStat,myTaxiData.fTaxiOJ_InitialMileage)
					CDEBUG1LN(DEBUG_OJ_TAXI,"Initial Taxi Mileage = ", myTaxiData.fTaxiOJ_InitialMileage)
					
					// TODO - after the in car smoke effect is finished, reinstate this here!
					//REQUEST_TAXI_ODDJOB_CC_STREAMS_HOT_BOX()
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_WAIT_PARK)
				ENDIF
			BREAK
			
			//Waits for everyone to be seated
			CASE TRS_WAIT_PARK
				IF IS_GROUP_IN_TAXI(myTaxiData,myTaxiGroup)
					//Greet the player
					SET_NEXT_TAXI_SPEECH(myTaxiData,TAXI_DI_OBJ,TRUE)
				
					IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO])
						SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P0_REJINO],PCF_DontAllowToBeDraggedOutOfVehicle,TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE])
						SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P1_FRANKIE],PCF_DontAllowToBeDraggedOutOfVehicle,TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE])
					
						SET_PED_CONFIG_FLAG(myTaxiGroup.piPassenger[TAXI_GROUP_P2_PAULIE],PCF_DontAllowToBeDraggedOutOfVehicle,TRUE)
					
					ENDIF
					
					
					//CleanUp Pickup Lock & POI
					CLEANUP_TAXI_PICKUP_STOP(myTaxiData)
					
					//Passenger is in cab, do cleanup and add new blip for destination.
					TAXI_SET_DROPOFF_BLIP_ON(myTaxiData,FALSE)
					
					RELEASE_TAXI_ODDJOB_CC_STREAMS_STAGE_Intro()
					
					//SET_TAXI_OJ_GROUP_HOT_BOX(myTaxiData,pFxTaxiWindowSmoke )
					
					TAXI_START_TIMER(myTaxiData, TT_BANTER)
					TAXI_RESET_TIMERS(myTaxiData,  TT_BORING)
					TAXI_RESET_TIMERS(myTaxiData,  TT_DROPOFF)
					
					TAXI_OJ_CC_SET_TIPS_TO_CHECK()
					
					myTaxiData.fTaxiOJ_BanterDelay = GET_RANDOM_FLOAT_IN_RANGE(3.0,10.0)
					
					TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
					
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_DRIVING_PASSENGER)
					
				ENDIF
				
				// check to see if player dived, if so send back to prev state
				IF IS_VEHICLE_DRIVEABLE( myTaxiData.viTaxi)
					IF NOT IS_PED_IN_VEHICLE(myTaxiData.piTaxiPlayer, myTaxiData.viTaxi)
						TAXI_HANDLE_PLAYER_DIVE_GROUP(myTaxiData, myTaxiGroup)
						TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_MANAGE_PICKUP)
					ENDIF
				ENDIF
			BREAK

			
			CASE TRS_DRIVING_PASSENGER
				//Had to do this here, and I hate it cause it's the only case, because I had to give Clown Car it's own unique data
				IF NOT bFirstDropoffBlipEnabled
//					IF myTaxiData.bObjPrinted
//						TAXI_BLIP_GROUP_DROPOFF_POINT_SEQUENTIAL(myTaxiGroup)
//						bFirstDropoffBlipEnabled = TRUE
//					ENDIF
				ENDIF
				
				RUN_TAXIRADIO_UPDATE_WHEN_ENABLED(myTaxiData)
				
				IF HAS_TAXI_GROUP_REACHED_HIT_ALL_DROPOFFS(myTaxiData, myTaxiGroup, tTaxiOJ_DQ_Data, tDialogueLine)
					
					CLOSE_DIALOGUE_QUEUE(tTaxiOJ_DQ_Data,-1,TRUE)
					
					IF HAS_TAXI_GROUP_BEEN_DROPPED_OFF_TIMELY(myTaxiGroup)
						TAXI_SET_BONUS_AWARD(myTaxiData, 0)
					ENDIF
					
					DISABLE_TAXI_OJ_PFX_GROUP_HOT_BOX(myTaxiGroup)
					
					
					//CASH$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
					SET_TAXI_FARE_OFF_MILEAGE(myTaxiData)
					TAXI_OJ_RATE_OVERALL_TIP_LEVEL(myTaxiData)
					CONVERT_TAXI_TIP_TO_CASH(myTaxiData)
					
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_REGULAR_PAYMENT)

				ENDIF
			BREAK
			
			//Regular Payment
			CASE TRS_REGULAR_PAYMENT
				
				//Wait til passenger is done speaking/paying than tell him to leave vehicle
				IF HAS_TAXI_OJ_PASSENGER_BEEN_DROPPED_OFF(myTaxiData)
					
					CLEAR_SEQUENCE_TASK(tempIndex)
					OPEN_SEQUENCE_TASK(tempIndex)
						//TASK_GO_TO_COORD_ANY_MEANS(NULL, myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P0_REJINO], PEDMOVE_WALK, NULL)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, myTaxiGroup.vTaxiOJWalkTo[TAXI_GROUP_P0_REJINO], PEDMOVE_WALK)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						IF DOES_SCENARIO_EXIST_IN_AREA(myTaxiData.vTaxiOJ_PassengerGoToPt, 4, TRUE)
							TASK_USE_NEAREST_SCENARIO_TO_COORD(NULL, myTaxiData.vTaxiOJ_PassengerGoToPt, 4)
						ELSE
							TASK_ACHIEVE_HEADING(NULL, 350.8689)
							TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_WINDOW_SHOP_BROWSE")
//							TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_pleased", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//							TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_pleased", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//							TASK_PLAY_ANIM(NULL, "gestures@m@standing@casual", "gesture_pleased", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//							TASK_WANDER_STANDARD(NULL)
						ENDIF
					CLOSE_SEQUENCE_TASK(tempIndex)
					TASK_PERFORM_SEQUENCE(myTaxiData.piTaxiPassenger, tempIndex)
					
					SET_PED_KEEP_TASK(myTaxiData.piTaxiPassenger, TRUE)
						
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_SCORECARD_GRADE)				
		
				ENDIF			

			BREAK
			
			//Pop up the scorecard
			CASE TRS_SCORECARD_GRADE
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, <<29.112947,-1349.217651,29.612801>>) < 3.0
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_TEXT_2D("PRF_SearchForClosestDoor Flag Set", <<0.5, 0.2, 0.0>>)
						#ENDIF
					ENDIF
				ENDIF
				
				IF TAXI_CALC_SCORECARD(myTaxiData,TaxiMidSize)
					TAXI_MISSION_END(TRUE, myTaxiData, FALSE)	
					TAXI_RESET_TIMERS(myTaxiData,TT_CLEANUP)
					//Move on to the next stage
					TAXI_JOB_SET_NEXT_STATE(myTaxiData,TRS_CLEANUP)
				ENDIF
			BREAK
			
			CASE TRS_CLEANUP
				IF NOT IS_PED_INJURED(myTaxiData.piTaxiPassenger)
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(myTaxiData.piTaxiPassenger, <<29.112947,-1349.217651,29.612801>>) < 3.0
						SET_PED_RESET_FLAG(myTaxiData.piTaxiPassenger, PRF_SearchForClosestDoor, TRUE)
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_TEXT_2D("PRF_SearchForClosestDoor Flag Set", <<0.5, 0.2, 0.0>>)
						#ENDIF
					ENDIF
					
					IF IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<31.050192,-1347.284790,29.497032>>, <<1.0,1.0,1.0>>, FALSE, TRUE, TM_ON_FOOT)
					OR GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > 50
					OR GET_TAXI_TIMER_IN_SECONDS(myTaxiData,TT_CLEANUP) > 30
						IF IS_ENTITY_AT_COORD(myTaxiData.piTaxiPassenger, <<31.050192,-1347.284790,29.497032>>, <<1.0,1.0,1.0>>, FALSE, TRUE, TM_ON_FOOT)
							CDEBUG1LN(DEBUG_OJ_TAXI,"passenger is at the coord")
						ENDIF
						IF GET_PLAYER_DISTANCE_FROM_ENTITY(myTaxiData.piTaxiPassenger) > 50
							CDEBUG1LN(DEBUG_OJ_TAXI,"player is far from the passenger")
						ENDIF
						IF GET_TAXI_TIMER_IN_SECONDS(myTaxiData,TT_CLEANUP) > 30
							CDEBUG1LN(DEBUG_OJ_TAXI,"cleanup timer > 30")
						ENDIF
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(myTaxiData.piTaxiPassenger,FALSE)
	        			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, myTaxiData.relPassenger, RELGROUPHASH_PLAYER)
						SET_PED_AS_NO_LONGER_NEEDED(myTaxiData.piTaxiPassenger)
						
						Script_Cleanup()	
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_OJ_TAXI,"passenger is injured")
					Script_Cleanup()
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

SCRIPT

	// Handles the player being busted or arrested, or if the player
	// jumps into Multiplayer from Singleplayer – ensures the script
	// gets cleaned up properly under the correct circumstances
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_FAILURE)
		Script_Cleanup()
	ENDIF
	
	// Any initialisation (generally, only mission scripts should set
	// the mission flag to TRUE)
	SET_MISSION_FLAG(TRUE)
	
	INITIALIZE_SCRIPT_VARIABLES()
	
	// The script loop
	WHILE (TRUE)
		// Maintain the script – perform per-frame functionality
	
	//All skips and debug shortcuts-------------------------------
	#IF IS_DEBUG_BUILD
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			IF DOES_ENTITY_EXIST(myTaxiData.viTaxi)
				OUTPUT_DEBUG_CAM_RELATIVE_TO_ENTITY(myTaxiData.viTaxi)
			ENDIF
		ENDIF	
			
		PROCESS_TAXI_DEBUG_SKIP(myTaxiData,tDebugState)
		
		// Debug Key: Check for Pass (not for Minigmes)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			TAXI_ODDJOB_DEBUG_SKIP_TO_SCORECARD(myTaxiData)
		ENDIF

		// Debug Key: Check for Fail (not for Minigames)
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Script_Failed()
		ENDIF
		// Debug Key: set vehicle health to 0.0
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
			IF NOT IS_ENTITY_DEAD(myTaxiData.viTaxi)
				SET_VEHICLE_ENGINE_HEALTH(myTaxiData.viTaxi, 0.0)
			ENDIF
		ENDIF		
		PROCESS_WIDGETS()
	#ENDIF
	//END DEBUG----------------------------------------------------
				
		IF DOES_ENTITY_EXIST(myTaxiData.piTaxiPlayer)
			MAIN_TAXI_OJ_CLOWN_CAR()
		ELSE
			REASSIGN_TAXI_OJ_DRIVER(myTaxiData)
		ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

